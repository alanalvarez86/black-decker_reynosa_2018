unit FTressShell;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseShell, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxRibbonCustomizationForm, cxStyles, cxClasses, dxSkinsForm, Vcl.ExtCtrls,
  Vcl.ComCtrls, dxRibbon, dxStatusBar, dxRibbonStatusBar,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaMessages,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, System.Actions,
  Vcl.ActnList, dxBar, cxLocalization, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxGroupBox;

type
  TTressShell = class(TBaseShell)
    TabArchivo: TdxRibbonTab;
    DevEx_BarManager: TdxBarManager;
    AEmpresas: TdxBar;
    ASalir: TdxBar;
    TabArchivo_btnOtraEmpresa: TdxBarLargeButton;
    TabArchivo_btnImprimir: TdxBarButton;
    TabArchivo_btnImpresora: TdxBarButton;
    TabArchivo_btnSalir: TdxBarLargeButton;
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_SalirSistema: TAction;
    TabProcesos: TdxRibbonTab;
    IMSSIDSE: TdxBar;
    TabProcesos_btnValIDSE: TdxBarLargeButton;
    _IMSS_ValidaMovIDSE: TAction;
    DevEx_cxLocalizer: TcxLocalizer;
    gbDetalle: TcxGroupBox;
    memDetalle: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _IMSS_ValidaMovIDSEExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CargaTraducciones;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure WMWizardSilent(var Message: TMessage); message WM_WIZARD_SILENT;
    procedure RevisaDerechos;
  protected
    procedure DoOpenAll; override;
  public
    { Public declarations }
     procedure SetDataChange(const Entidades: array of TipoEntidad);
     procedure RefrescaNavegacionInfo;
     procedure CambiaEmpleadoActivos;
     procedure ReinitPeriodo;
     procedure SetAsistencia( const dValue: TDate );
     procedure RefrescaIMSS;
     procedure RefrescaEmpleado;
     procedure ReconectaFormaActiva;
     procedure CargaEmpleadoActivos;
     procedure CambiaPeriodoActivos;
     procedure CambiaSistemaActivos;
  end;

var
  TressShell: TTressShell;

implementation

{$R *.dfm}

uses
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     DCliente,
     DProcesos,
     DIMSS,
     DConsultas,
     ZAccesosTress,
     ZAccesosMgr;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin

end;

procedure TTressShell._A_OtraEmpresaExecute(Sender: TObject);
begin
  AbreEmpresa( False );
end;

procedure TTressShell._A_SalirSistemaExecute(Sender: TObject);
begin
    Close;

end;

procedure TTressShell._IMSS_ValidaMovIDSEExecute(Sender: TObject);
begin
     inherited;
     dmProcesos.MostrarDetalleProceso := FALSE;
     DProcesos.ShowWizard( prIMSSValidacionMovimientosIDSE );
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
    //PENDIENTE
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
    //PENDIENTE
end;

procedure TTressShell.ReinitPeriodo;
begin
    //PENDIENTE
end;

procedure TTressShell.SetAsistencia( const dValue: TDate );
begin
    //PENDIENTE
end;

procedure TTressShell.RefrescaIMSS;
begin
    //PENDIENTE
end;

procedure TTressShell.RefrescaEmpleado;
begin
   //PENDIENTE
end;

procedure TTressShell.ReconectaFormaActiva;
begin
    //PENDIENTE
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     //PENDIENTE
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
  dmCliente := TdmCliente.Create( Self );
  dmProcesos := TdmProcesos.Create( Self );
  dmIMSS := TdmIMSS.Create( Self );
  dmConsultas := TdmConsultas.Create( Self );
  inherited;
  DevEx_ShellRibbon.ShowTabHeaders := True;

  _IMSS_ValidaMovIDSE.Enabled := FALSE;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaTraducciones;

  FocusControl(gbDetalle);  
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     //PENDIENTE
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     //PENDIENTE
end;

procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
     end;

     // Detalle de ejecución de Wizard de validación de movimientos IDSE
     memDetalle.Lines.Text := dmProcesos.DetalleWizardIDSE;
end;
procedure TTressShell.WMWizardSilent(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessSilent( WParam, LParam );
     end;
end;

procedure TTressShell.RevisaDerechos;
begin
     _IMSS_ValidaMovIDSE.Enabled := Revisa( D_IMSS_PROC_VALID_MOV_IDSE ) and EmpresaAbierta;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        RevisaDerechos;
     except
           on Error : Exception do
           begin
                DoCloseAll;
           end;
     end;
end;


end.
