unit FImportarTressEmail;

interface

uses
  CheckLst, DB, ZetaRegistryServer, DZetaServerProvider, ZetaClientDataSet,
  WinSvc, FileCtrl, TressMorado2013, ComObj, ShellApi,

  // US #7328: Implementar Control de la ayuda en XE5
  // HtmlHelpViewer,
  FHelpManager, SysUtils,

  Forms, System.Classes, dxBevel, ZetaDialogo, ZetaRegistryCliente, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, dxSkinsdxBarPainter, Xml.xmldom, Xml.XMLIntf,
  Xml.Win.msxmldom, Xml.XMLDoc, cxLocalization, Vcl.ImgList, Vcl.Controls,
  dxBar, cxClasses, dxSkinsForm, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxGrid, Vcl.ExtCtrls,

  DImportacion, Vcl.Menus, Vcl.StdCtrls, cxButtons;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TFImportarTareasTressEmail = class(TForm)
    dxSkinController1: TdxSkinController;
    dxBarManagerbkbk: TdxBarManager;
    btnComparar: TdxBarLargeButton;
    dxBarManagerBar: TdxBar;
    cxLargeImageMenu: TcxImageList;
    DataSource: TDataSource;
    btnRefrescar: TdxBarLargeButton;
    pnlGrid: TPanel;
    cxLocalizer: TcxLocalizer;
    pnlSuperior: TPanel;
    XMLDoc: TXMLDocument;
    gridTareasEmail: TcxGrid;
    cxGridTareasEmailDBTableView: TcxGridDBTableView;
    NombreTarea: TcxGridDBColumn;
    cxGridTareasEmail: TcxGridLevel;
    Empresa: TcxGridDBColumn;
    Fecha: TcxGridDBColumn;
    Usuarios: TcxGridDBColumn;
    Frecuencia: TcxGridDBColumn;
    Reportes: TcxGridDBColumn;
    ReporteEmp: TcxGridDBColumn;
    panelInferior: TPanel;
    Descripcion: TcxGridDBColumn;
    ScheduleType: TcxGridDBColumn;
    HoraInicio: TcxGridDBColumn;
    FechaInicio: TcxGridDBColumn;
    Dias: TcxGridDBColumn;
    Meses: TcxGridDBColumn;
    btnGenerarCSV: TdxBarLargeButton;
    btnSalir: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCompararClick(Sender: TObject);
    procedure cxGridServidorDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    // US #7328: Implementar Control de la ayuda en XE5
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    procedure btnGenerarCSVClick(Sender: TObject);
    procedure btnRefrescarClick(Sender: TObject);
    procedure btnSalirClick(Sender: TObject);
    private
      { Private declarations }
      procedure ApplyMinWidth;
    public
    { Public declarations }
    protected
      // US #7328: Implementar Control de la ayuda en XE5
      FOldHelpEvent: THelpEvent;
      function HTMLHelpHook( Command: Word; Data: THelpEventData; var CallHelp: Boolean ): Boolean;

      procedure CargaTraducciones;
  end;

var
  FImportarTareasTressEmail: TFImportarTareasTressEmail;

implementation

uses
  Types, Windows, Graphics, ZetaCommonClasses,// ZetaWinAPITools,
  ZetaLicenseMgr, ZetaCommonTools, ZetaServerTools,
  MotorPatchUtils, DBClient, DSistema;


{$R *.dfm}

procedure TFImportarTareasTressEmail.FormCreate(Sender: TObject);
{var
  l: DWORD;}
begin
     HelpContext := 1;
     ZetaRegistryCliente.InitClientRegistry;

     WindowState := wsNormal;
     // US #7328: Implementar Control de la ayuda en XE5
     with Application do
     begin
          OnException := ManejaExcepcion;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
     end;
end;

// US #7328: Implementar Control de la ayuda en XE5
function TFImportarTareasTressEmail.HTMLHelpHook(Command: Word; Data: THelpEventData; var CallHelp: Boolean): Boolean;
begin
    Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

// US #7328: Implementar Control de la ayuda en XE5
procedure TFImportarTareasTressEmail.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TFImportarTareasTressEmail.FormShow(Sender: TObject);
begin
     // Traducciones
     CargaTraducciones;

     // dmSistema.cdsSistBaseDatos.Conectar;
     dmSistema.cdsTareasEmail.Conectar;

     // DataSource.DataSet := dmsistema.cdsSistBaseDatos;
     DataSource.DataSet := dmsistema.cdsTareasEmail;

     cxGridTareasEmailDBTableView.DataController.DataSource := DataSource;

     //Desactiva la posibilidad de agrupar
     cxGridTareasEmailDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     cxGridTareasEmailDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     cxGridTareasEmailDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     cxGridTareasEmailDBTableView.FilterBox.CustomizeDialog := TRUE;
     //DevEx: Para que ponga el MinWidth ideal al titulo de las columnas. Posteriormente se aplica el BestFit.
     ApplyMinWidth;
     cxGridTareasEmailDBTableView.ApplyBestFit();
end;

procedure TFImportarTareasTressEmail.btnCompararClick(Sender: TObject);
begin
     dmImportacion.ProcesaArchivoTXT;
     cxGridTareasEmailDBTableView.ApplyBestFit();
end;

procedure TFImportarTareasTressEmail.btnGenerarCSVClick(Sender: TObject);
begin
     // Leer tareas y escribirlas en archivo CSV.
     // ShellApi.ShellExecute (Application.MainForm.Handle, nil, 'cmd.exe', '/C schtasks /query /fo CSV /v > ImportarTareasTressEmail.csv', nil, SW_HIDE);
     ShellApi.ShellExecute (Application.MainForm.Handle, nil, 'cmd.exe', '/C schtasks /query /fo LIST /v > ImportarTareasTressEmail.txt', nil, SW_HIDE);
     // ShellApi.ShellExecute (Application.MainForm.Handle, nil, 'cmd.exe', '/C schtasks /query /fo TABLE /NH /V > ImportarTareasTressEmail.txt', nil, SW_HIDE);
end;

procedure TFImportarTareasTressEmail.btnRefrescarClick(Sender: TObject);
begin
     dmSistema.cdsTareasEmail.Refrescar;
end;

procedure TFImportarTareasTressEmail.btnSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TFImportarTareasTressEmail.CargaTraducciones;
begin
     cxLocalizer.Active := True;
     cxLocalizer.Locale := 2058;  // Cardinal para Espa�ol (Mexico)
end;

procedure TFImportarTareasTressEmail.cxGridServidorDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TFImportarTareasTressEmail.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 35;
begin
     with  cxGridTareasEmailDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
