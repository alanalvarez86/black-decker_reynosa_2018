unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, ZetaTipoEntidad, FileCtrl,
  cxGraphics, cxControls, cxLookAndFeels, cxLocalization,
  ZBaseImportaShell_DevEx, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, dxSkinsdxBarPainter, cxCheckBox, cxMaskEdit,
  cxDropDownEdit, ZetaCXStateComboBox, ImgList, dxBar, cxClasses,
  dxSkinsForm, ActnList, ExtCtrls, dxStatusBar,
  dxRibbonStatusBar, cxGroupBox, StdCtrls, cxButtons, cxTextEdit, cxMemo,
  cxPC, ComCtrls,ZetaCommonLists,ZetaDialogo, dxRibbonSkins,
  dxSkinsdxRibbonPainter, cxStyles, dxRibbon, dxSkinsdxStatusBarPainter,
  {uses para crear XML}  XMLIntf, cxProgressBar, dxBarBuiltInMenu, cxShellBrowserDialog {,Psock, NMsmtp};

type
  TTressShell = class(TBaseImportaShell_DevEx)
    _A_Configuracion: TAction;
    SaveDialog: TSaveDialog;
    dxBarSubItem1: TdxBarSubItem;
    TabArchivo_btnConfiguracion: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    PeriodoTipoLBL_DevEx: TLabel;
    PanelControlesNomina: TPanel;
    Panel2: TPanel;
    PanelRangoFechas: TPanel;
    PanelPeriodoStatus: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    PeriodoPrimero_DevEx: TcxButton;
    PeriodoAnterior_DevEx: TcxButton;
    PeriodoSiguiente_DevEx: TcxButton;
    PeriodoUltimo_DevEx: TcxButton;
    PeriodoNumeroCB_DevEx2: TcxStateComboBox;
    Panel5: TPanel;
    cxGroupBox2: TcxGroupBox;
    chkConCtaBanco: TcxCheckBox;
    chkSinCtaBanco: TcxCheckBox;
    PeriodoTipoCB_DevEx2: TcxStateComboBox;
    DevEx_cxLocalizer: TcxLocalizer;
    _Per_Primero: TAction;
    _Per_Anterior: TAction;
    _Per_Siguiente: TAction;
    _Per_Ultimo: TAction;
    Panel3: TPanel;
    StatusBarTimbrado: TdxStatusBar;
    StatusBarTimbradoContainer2: TdxStatusBarContainerControl;
    PanelTipoNomina: TPanel;
    StatusBarTimbradoContainer1: TdxStatusBarContainerControl;
    PanelAnio: TPanel;
    AnioLbl_DevEx: TLabel;
    grpAno: TcxGroupBox;
    btnYearBefore_DevEx: TcxButton;
    SistemaYear_DevEx: TcxTextEdit;
    btnYearNext_DevEx: TcxButton;
    Panel6: TPanel;
    Bar: TcxProgressBar;
    cxButton1: TcxButton;
    DirDestino: TEdit;
    Label2: TLabel;
    cxShellBrowserDialogDirectorio: TcxShellBrowserDialog;

    procedure _A_ConfiguracionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject); overload;
    procedure PeriodoTipoCB_DevEx2LookUp(Sender: TObject;
      var lOk: Boolean);
    procedure _Per_AnteriorUpdate(Sender: TObject);
    procedure _Per_PrimeroExecute(Sender: TObject);
    procedure btnYearBefore_DevExClick(Sender: TObject);
    procedure btnYearNext_DevExClick(Sender: TObject);
    procedure CalculaAnio ( Operacion:Integer );
    procedure HabilitaBotonesAnio;
    procedure RefrescaSistemaActivos;
    procedure SistemaYear_DevExPropertiesChange(Sender: TObject);
    procedure PeriodoNumeroCB_DevEx2LookUp(Sender: TObject;
      var lOk: Boolean);
    procedure _Per_AnteriorExecute(Sender: TObject);
    procedure _Per_SiguienteExecute(Sender: TObject);
    procedure _Per_SiguienteUpdate(Sender: TObject);
    procedure _Per_UltimoExecute(Sender: TObject);
    procedure _Per_UltimoUpdate(Sender: TObject);

  private
    { Private declarations }
     FTempPanelRangoFechas:String;
     BatchLog: TStrings;
     procedure SetNombreLogs;

  protected
     procedure DoOpenAll; override;
     procedure DoCloseAll; override;
     procedure ProcesarArchivo; override;
     procedure DoProcesar; override;
     procedure SetControlesBatch; override;
     procedure CargaTraducciones;
     procedure ActivaControles( const lProcesando: Boolean ); override;
     function  GetStrXml(TieneCuenta :String;var  iWD_Folio :Integer ):String;


  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
  procedure StrToFile(const FileName, SourceString : string);
  function FormaActivaNomina: Boolean;
  function SetNombreArchivo(TieneCuenta :String):String;
  procedure RefrescaEmpleado;
  procedure SetDataChange(const Entidades: array of TipoEntidad);
  procedure CambiaEmpleadoActivos;
  function FormaActiva: TBaseConsulta;
  procedure SetAsistencia( const dValue: TDate );
  procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
  procedure RefrescaIMSS;
  procedure ReinitPeriodo;
  procedure GetFechaLimiteShell;
  procedure CreaArbol( const lEjecutar: Boolean );
  procedure SetBloqueo;
  procedure AbreFormaSimulaciones(const TipoForma: Integer);
  procedure RefrescaNavegacionInfo;
  procedure ReconectaFormaActiva;
  procedure CargaEmpleadoActivos;
  procedure CambiaSistemaActivos;
  procedure CambiaPeriodoActivos;
  procedure RefrescaPeriodoActivos;
  procedure ChangeTimerInfo;
  procedure SplitStr(const Source, Delimiter: String; var DelimitedList: TStringList);
  procedure CambioPeriodoActivo;
  property  TempPanelRangoFechas: String  read FTempPanelRangoFechas write FTempPanelRangoFechas;
  procedure CambioValoresActivos(const Valor: TipoEstado);
  function  GetsURL(TieneCuenta: String;var sTipo_Conn:String):String;
  procedure EscribeBitacoraHora(const sMensaje: String);
  procedure EscribeErrorHora(const sMensaje: String);
  function  Enviar( TieneCuenta: String ):boolean;

  end;

const
     K_EXTENSION_ARCHIVO = '.xml';
     K_NO_TIENE_CUENTA= 'CHK';
     K_SEPARADOR = '_';
     K_NOMINA ='NOMINA';

     K_MANUAL = 0;
     K_ANTERIOR = 1;
     K_SIGUIENTE = 2;
     K_MAX_YEAR = 2100;
     K_MIN_YEAR = 1899;
     K_CARDINAL_LOCALIZACION_ES = 2058;     // Cardinal para Espanol (Mexico)


     K_PANEL_EMPLEADO = 1;
     K_PANEL_PERIODO = 2;
     K_PANEL_IMSS = 3;
     K_PANEL_NAVEGA = 4;
     K_PANEL_FECHA_SISTEMA = 5;
     K_PANEL_FECHA_ASISTENCIA = 6;


     K_EXTRA_WIDTH = 5;
     K_MIN_WIDTH = 405;


var
  TressShell: TTressShell;
  ErrorCount : Integer; 
implementation

uses
    ZAccesosMgr, ZaccesosTress, ZetaCommonClasses,
    ZetaCommonTools, ZetaClientTools, ZetaMessages,
    DInterfase, DSistema, DTablas, DCliente, DBasicoCliente, DB, DGlobal,DbaseGlobal,ZGlobalTress,RegExpr,StrUtils,
    dConsultas,Fconfiguracion,ZetaSystemWorking;

{$R *.dfm}
{$WRITEABLECONST ON}
    { Procedimientos y Funciones Publicos del Shell de Tress }
procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambioPeriodoActivo;
begin
     PeriodoNumeroCB_DevEx2.ValorEntero := dmCliente.PeriodoNumero;
     RefrescaPeriodoActivos;
     CambioValoresActivos( stPeriodo );

end;

procedure TTressShell.CambioValoresActivos(const Valor: TipoEstado);
begin

end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;


procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     // No Implementar
end;


procedure TTressShell.RefrescaNavegacionInfo;
begin
     // No Implementar
end;


procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     // No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     // No Implementar
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;


procedure TTressShell._A_ConfiguracionExecute(Sender: TObject);
var
   Configuracion: TConfiguracion;
   UserValido : boolean;
begin
  inherited;
    //@DC
     Configuracion :=  TConfiguracion.Create( Self );
     UserValido:=ZAccesosMgr.CheckDerecho( D_CAT_CONFI_GLOBALES, K_DERECHO_CAMBIO );
     if(UserValido=true) then
       Configuracion.ShowModal
     else
        ZetaDialogo.zInformation( 'Error', '! No Tiene Derecho a ver o modificar configuraci�n !', 0 );
        Configuracion.Free;
end;


procedure TTressShell.FormCreate(Sender: TObject);
begin
  inherited;

    ZAccesosMgr.LoadDerechos;
    dmInterfase := TdmInterfase.Create( Self );
    dmSistema := TdmSistema.Create( Self );
    dmTablas := TdmTablas.Create( Self );
    dmCliente :=  TdmCliente.Create( Self );
    dmConsultas := TdmConsultas.Create(self);
    dmSistema.cdsUsuarios.Conectar;
    //Para que le asigen el uso de vista nueva solamente
    dmCliente.ModoSuper := True;
    BatchLog := TStringList.Create;

end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
  inherited;
       FreeAndNil( dmInterfase );
       FreeAndNil( dmSistema );
       FreeAndNil( dmTablas );
       FreeAndNil( dmConsultas );
       BatchLog.Free;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaTraducciones;
end;

procedure TTressShell.DoOpenAll;
begin
     inherited;
     Global.Conectar;
     ActivaControles( FALSE );
     with dmcliente do
     begin
          InitActivosSistema;
          InitArrayTPeriodo;
          InitActivosAsistencia;
          InitActivosPeriodo;
          InitActivosIMSS;
          SistemaYear_DevEx.Text:= IntToStr(dmCliente.YearDefault);
    end;
     with PeriodoTipoCB_DevEx2 do
     begin
          ListaFija:=lfTipoPeriodoConfidencial;
          Llave:= IntToStr( Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) );
          RefrescaPeriodoActivos;
          SetNombreLogs;
          CambioPeriodoActivo;
             // Si viene vacio el valor le asigna 0
           if ( StrVacio ( Llave ) ) then
             begin
                  PeriodoTipoCB_DevEx2.Indice:= 0;
                  dmCliente.PeriodoTipo := eTipoPeriodo( StrToInt( PeriodoTipoCB_DevEx2.Llave ) );
                  CambioPeriodoActivo;
             end;
        end;
        chkSinCtaBanco.Checked:=True;
        chkConCtaBanco.Checked:=True;
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.ProcesarArchivo;
begin
     ActivaControles( TRUE );
     try
        InitBitacora;
          DoProcesar;
     finally
            EscribeBitacoras;
             ActivaControles( FALSE );
     end;
end;

procedure TTressShell.SplitStr(const Source, Delimiter: String; var DelimitedList: TStringList);
var
  s: PChar;
  DelimiterIndex: Integer;
  Item: String;
begin
  s:=PChar(Source);

  repeat
    DelimiterIndex:=Pos(Delimiter, s);
    if DelimiterIndex=0 then Break;

    Item:=Copy(s, 1, DelimiterIndex-1);

    DelimitedList.Add(Item);

    inc(s, DelimiterIndex + Length(Delimiter)-1);
  until DelimiterIndex = 0;
  DelimitedList.Add(s);
end;

procedure TTressShell.DoProcesar;
var
  lOk:Boolean;
begin
   lOk:=False;
   EscribeBitacoraHora('Iniciando proceso');
   if (chkConCtaBanco.Checked and chkSinCtaBanco.Checked)   then
        begin
           //Enviar archivo con cuenta
           lOk := not Enviar( K_GLOBAL_SI );
           //Enviar archivo sin cuenta
           if(lOk= False)  then
              lOk := not  Enviar( K_GLOBAL_NO)
           else //Aqui no se asigna el valor a lOk por que si hubo error en el proceso anterior y este proceso concluye correctamente se sobreescribiria la etiqueta de error y mostraria proceso exitoso cuando no es as�
               Enviar(K_GLOBAL_NO);
        end
   else
       if((chkConCtaBanco.Checked = TRUE ) and  (chkSinCtaBanco.Checked= FALSE)) then
          begin
             //Enviar archivo con cuenta
             lOk:= not  Enviar( K_GLOBAL_SI );
          end
       else
          if((chkConCtaBanco.Checked= FALSE) and ( chkSinCtaBanco.Checked = TRUE) ) then
              begin
                 //Enviar archivo sin cuenta
                 lOk:= not Enviar(K_GLOBAL_NO);
              end
          else
              begin
                ShowMessage('No seleccion�  tipo(s) de archivos de dispersi�n a generar');
              end;
   if(lOk = False) then
       EscribeBitacoraHora('Proceso Finalizado Exitosamente')
   else
       EscribeBitacoraHora('Proceso Finalizado con Errores');
end;

//sobre escrito para seleccionar un directorio
procedure TTressShell.ArchivoSeekClick(Sender: TObject);
begin
     cxShellBrowserDialogDirectorio.Path :=  DirDestino.Text;
     if cxShellBrowserDialogDirectorio.Execute then
     begin
         DirDestino.Text := cxShellBrowserDialogDirectorio.Path ;
     end;

    { if  DirectoryExists(DirDestino.Text) then
         FTimbramexHelper.SetRecibosPath( cxFolderRecibosTxt.Text );}

end;


procedure TTressShell.SetControlesBatch;
begin
     inherited;
     SetNombreLogs;
     EnviarCorreo := FALSE;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre, sArchBit, sArchErr: String;
begin
     with dmCliente do
     begin
          sNombre := FechaToStr( FechaDefault ) + FormatDateTime( 'hhmmss', Time ) + '.LOG';
          sArchBit := ZetaMessages.SetFileNameDefaultPath( 'BITACORA-' + sNombre );
          sArchErr := ZetaMessages.SetFileNameDefaultPath( 'ERRORES-' + sNombre );
     end;
     if StrVacio( ArchBitacora.Text )  then
          ArchBitacora.Text := sArchBit;

     if StrVacio( ArchErrores.Text ) then
          ArchErrores.Text := sArchErr;
end;


procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := K_CARDINAL_LOCALIZACION_ES;
end;



procedure TTressShell.ActivaControles( const lProcesando: Boolean );
begin
     inherited;

end;

procedure TTressShell.PeriodoTipoCB_DevEx2LookUp(Sender: TObject;
  var lOk: Boolean);
var
   oCursor: TCursor;
begin
  inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
      try
      lOk := dmCliente.SetPeriodoNumero( PeriodoNumeroCB_DevEx2.ValorEntero );
        if lOk then
        begin
             RefrescaPeriodoActivos;
             dmCliente.PeriodoTipo := eTipoPeriodo( StrToIntDef( PeriodoTipoCB_DevEx2.Llave, 0 ) );
             CambioPeriodoActivo;
             CambioValoresActivos( stPeriodo );
        end
        else
            ZetaDialogo.zInformation( 'Error', ' No existe el per�odo de n�mina !', 0 );
     finally
            Screen.Cursor := oCursor;
end;
end;

procedure TTressShell.RefrescaPeriodoActivos;
var
   FechaDescrip: String;
procedure LimpiaTextos;
begin
     PanelPeriodoStatus.Caption := K_VACIO;
     PanelRangoFechas.Caption := K_VACIO;
end;
begin

     BtnProcesar.Enabled := FALSE;
     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
          StatusBarMsg( GetPeriodoDescripcion, K_PANEL_PERIODO );
           begin
               LimpiaTextos;
               FechaDescrip := FechaCorta( Inicio );
               if  (( Length( FechaDescrip ) > 0 ) AND ( FechaDescrip <> K_ESPACIO )) then
               begin
                      TempPanelRangoFechas := K_ESPACIO + FechaDescrip + K_ESPACIO +'al'+ K_ESPACIO + FechaCorta( Fin )+K_ESPACIO;
                      //DevEx: Solo se muestra el panel si el width es mayor a 1165, para dar prioridad a que los valores de la nomina se vean de izq. a derecha
                      if (Self.Width > K_MIN_WIDTH) then
                         PanelRangoFechas.Caption := TempPanelRangoFechas;
               end;

               {***DevEx (by am): Se define el Status de la nomina.***}
               if ( Numero = K_SIN_DEFINIR ) then
               begin
                    with  PanelPeriodoStatus do
                    begin
                         Font.Color := K_STATUS_COLOR_SINDEFINIR;
                         Caption := K_ESPACIO+'Sin Definir'+K_ESPACIO;
                         Hint := 'Per�odos ' + PeriodoTipoCB_DevEx2.Text + ' no han sido definidos';
                    end;
               end
               else
               begin
                    with PanelPeriodoStatus,PanelPeriodoStatus.Font do
                    begin
                         if ( Ord( Status ) in [K_STATUS_NUEVA, K_STATUS_PRENOMINA_PARCIAL, K_STATUS_SIN_CALCULAR, K_STATUS_CALCULADA_PARCIAL,K_STATUS_CALCULADA_TOTAL]) then
                            Color := K_STATUS_COLOR_NUEVA
                         else if ( Ord( Status ) in [K_STATUS_AFECTADA_PARCIAL,K_STATUS_AFECTADA_TOTAL]) then
                              begin
                                   if ( StatusTimbrado >=  estiTimbradoParcial ) then
                                      Color := K_STATUS_COLOR_TIMBRADA
                                   else
                                       Color := K_STATUS_COLOR_AFECTADA;
                              end
                         else
                             Color := clWindowText;
                         Caption :=K_ESPACIO+ZetaCommonTools.GetDescripcionStatusPeriodo(Status, StatusTimbrado ) +K_ESPACIO;
                         //DevEx:  Si el panel del rango de fechas no esta visible se le pone esta informacion al hint del status
                         if (Self.Width > K_MIN_WIDTH) then
                            Hint := K_VACIO
                         else
                             Hint := TempPanelRangoFechas;

                          //@DC se agrego el codigo para habilitar y deshabilitar  el boton de enviar para que solo funcione con nominas afectadas
              if ( ord(Status)>= K_STATUS_AFECTADA_TOTAL ) then
                 begin
                   BtnProcesar.Enabled := ZAccesosMgr.CheckDerecho( D_NOM_PROC_AFECTAR, K_DERECHO_CAMBIO );
                   end
                else
                   BtnProcesar.Enabled:=False; 

                    end;
               end;
               {***DevEx(by am): Ajustar tamano del panel segun el caption
               Se le suma un width extra por el cambio de FONT al panel, ya que el metodo TextWidth toma en cuenta el font de la forma para el calculo***}
               PanelPeriodoStatus.Width := Self.Canvas.TextWidth(PanelPeriodoStatus.Caption) + K_EXTRA_WIDTH;
               PanelRangoFechas.Width := Self.Canvas.TextWidth(PanelRangoFechas.Caption) + K_EXTRA_WIDTH;


              // GetFechaLimiteShell; //cargar fecha asistencia StatusBar_DevEx
          end;
          end;
     end;
end;

procedure TTressShell._Per_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoPrimero then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoDownEnabled;
end;



procedure TTressShell.btnYearBefore_DevExClick(Sender: TObject);
begin
  inherited;
  CalculaAnio ( K_ANTERIOR );
end;

procedure TTressShell.btnYearNext_DevExClick(Sender: TObject);
begin
  inherited;
  CalculaAnio ( K_SIGUIENTE );
end;



procedure TTressShell.SistemaYear_DevExPropertiesChange(Sender: TObject);
begin
  inherited;
  if ( Length( SistemaYear_DevEx.Text ) = 4 ) then
  begin
        CalculaAnio ( K_MANUAL);
  end;
end;


procedure TTressShell.CalculaAnio ( Operacion:Integer );
var
   oCursor: TCursor;
   NewYearValue:Integer;
function ValidaNewYear ( NewYearValue:Integer ):Integer;
 begin
     if NewYearValue > K_MAX_YEAR then
     begin
        NewYearValue := K_MAX_YEAR;
        btnYearNext_DevEx.Enabled := FALSE;  //Deshabilita btnYearSiguiente
     end
     else if NewYearValue < K_MIN_YEAR then
     begin
        NewYearValue := K_MIN_YEAR;
        btnYearBefore_DevEx.Enabled := FALSE;//Deshabilita btnYearAnterior
     end
     else
     begin
        HabilitaBotonesAnio; //Habilita ambos botones
     end;
     Result := NewYearValue;
 end;
begin
     NewYearValue := dmCliente.YearDefault;
     case Operacion of
          K_ANTERIOR:
               NewYearValue := dmCliente.YearDefault -1;
          K_SIGUIENTE:
               NewYearValue := dmCliente.YearDefault +1;
          K_MANUAL:
               NewYearValue := StrToInt ( SistemaYear_DevEx.Text );
     end;

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
              YearDefault := ValidaNewYear ( NewYearValue );
              SistemaYear_DevEx.Text := IntToStr ( YearDefault );
        end;
        CambioPeriodoActivo;
        RefrescaSistemaActivos;
     finally
            Screen.Cursor := oCursor;
     end;

end;

procedure TTressShell.HabilitaBotonesAnio;
begin
     btnYearNext_DevEx.Enabled := TRUE;
     btnYearBefore_DevEx.Enabled := TRUE;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     SetNombreLogs;
end;



procedure TTressShell.PeriodoNumeroCB_DevEx2LookUp(Sender: TObject;
  var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
           lOk := dmCliente.SetPeriodoNumero( PeriodoNumeroCB_DevEx2.ValorEntero );
        if lOk then
        begin
             RefrescaPeriodoActivos;
             CambioValoresActivos( stPeriodo );
        end
        else
            ZetaDialogo.zInformation( 'Error', '! No Existe El Per�odo De N�mina !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoAnterior then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell._Per_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoSiguiente then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_SiguienteUpdate(Sender: TObject);
begin
  inherited;
  TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

function TTressShell.SetNombreArchivo(TieneCuenta :String):String;
var
   sNombre:String;
begin
    sNombre:= '';
    sNombre:= K_NOMINA+K_SEPARADOR+PeriodoTipoCB_DevEx2.Descripcion+K_SEPARADOR+IntToStr(dmCliente.getDatosPeriodoActivo.Numero)+K_SEPARADOR+FormatDateTime( 'hhnn' , now)+K_SEPARADOR+ FechaToStr(now);
     if(TieneCuenta = K_GLOBAL_SI )  then
        result:= sNombre + K_EXTENSION_ARCHIVO
     else
        result:= sNombre+K_SEPARADOR+K_NO_TIENE_CUENTA+ K_EXTENSION_ARCHIVO;
end;

procedure TTressShell._Per_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoUltimo then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell._Per_UltimoUpdate(Sender: TObject);
begin
  inherited;
   TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

function TTressShell.GetsURL(TieneCuenta: String;var sTipo_Conn:String):String;
var
  sServer,sDirectorio: String;
  iPuerto: Integer;

begin
     iPuerto     := Global.GetGlobalInteger(K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_1);
     sServer     := Global.GetGlobalString(K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_2);
     sDirectorio := Global.GetGlobalString(K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_5);
     {if(iPuerto = 21)  then
        begin
          sTipo_Conn:=K_CONEXION_FTP;
        end
      else
        begin }
           if(iPuerto=22) then
              begin
                 sTipo_Conn:=K_CONEXION_SFTP;
              end
           else
              begin
                 sTipo_Conn:= K_CONEXION_DEFAULT;
              end;
       { end ; }
    result:=sTipo_Conn+sServer+'/'+ sDirectorio +'/'+SetNombreArchivo(TieneCuenta);
end;


  //Genera un string con el XML si  existen empleados
function TTressShell.GetStrXml(TieneCuenta :String;var  iWD_Folio :Integer ):String;
var
 iEmp : Integer;
 sTemp, sXML , sMensaje : String;
begin
   sXML := K_HEADER_XML;

   if(TieneCuenta= K_GLOBAL_SI) then
      begin
         sMensaje:='con cuenta';
      end
   else
      begin
         sMensaje:='sin cuenta';
      end;


      iEmp := dmInterfase.GetEmpleados_cta( dmCliente.YearDefault,Ord( dmCliente.GetDatosPeriodoActivo.Tipo ),dmCliente.getDatosPeriodoActivo.Numero,TieneCuenta,dmCliente.Empresa);

      if( iEmp  > 0 )  then
         begin
            EscribeBitacoraHora('Se inici� el proceso de dispersi�n de n�mina para empleados '+sMensaje+' '+SetNombreArchivo(TieneCuenta)+'.' );
            sTemp := dmInterfase.Crear_XML(  dmCliente.YearDefault, Ord( dmCliente.GetDatosPeriodoActivo.Tipo ),dmCliente.getDatosPeriodoActivo.Numero , TieneCuenta , dmCliente.Empresa, dmCliente.Usuario,iWD_Folio  );
            if(sTemp='') then
               begin
                 sXML:='';
               end
            else
               begin
                    sXML:=sXML+sTemp;
               end;
         end
      else
         begin
            EscribeErrorHora('Error al generar archivo de empleados'+SetNombreArchivo(TieneCuenta) +': no existen empleados '+sMensaje+'.');
            sXML:='';
         end;
   result := sXML;
end;


procedure TTressShell.EscribeBitacoraHora(const sMensaje: String);
begin
     MemBitacora.Lines.Add(DateTimeToStr(Now)+':   '+ sMensaje );
end;

procedure TTressShell.EscribeErrorHora(const sMensaje: String);
begin
     MemErrores.Lines.Add(DateTimeToStr(Now)+':   '+ sMensaje );
end;



procedure TTressShell.StrToFile(const FileName, SourceString : string);
var
  Stream : TFileStream;
begin
  Stream:= TFileStream.Create(FileName, fmCreate);
  try
    Stream.WriteBuffer(Pointer(SourceString)^, Length(SourceString));
  finally
    Stream.Free;
  end;
end;

function TTressShell.Enviar( TieneCuenta: String ):boolean;
var
 sXML, sFileName, sFileNamePath :String;
 destFile : TextFile;
 lOk:Boolean;
 iWD_FOLIO:integer;
begin
   lOK := FALSE;
   sXML := GetStrXml(TieneCuenta, iWD_FOLIO);
   sFileName := SetNombreArchivo(TieneCuenta);

   if DirectoryExists(DirDestino.Text ) then
   begin
      sFileNamePath := ExtractFilePath( DirDestino.Text + '\' + sFileName );
      sFileName := sFileNamePath  +  sFileName;
   end
   else
   begin
     if strLleno(DirDestino.Text ) then 
         EscribeBitacoraHora('El directorio '+ DirDestino.Text +' no existe, los achivos se crear�n en la carpeta  '  +  GetCurrentDir );
   end;

   try
      AssignFile(destFile,sFileName );
      ReWrite(destFile);
      Write( destFile, sXML );
      CloseFile(destFile);
      if zStrToBool(TieneCuenta) then
           EscribeBitacoraHora('Se gener� el archivo de dispersi�n (Con cuenta bancaria): '+sFileName )
      else
           EscribeBitacoraHora('Se gener� el archivo de dispersi�n (Sin cuenta bancaria): '+sFileName );
      lOk := True;
   except
       on E : Exception do
          EscribeErrorHora('Error al intentar grabar el archivo de dispersi�n: '+sFileName  )
   end;


   result:=lOk;
end;

end.
