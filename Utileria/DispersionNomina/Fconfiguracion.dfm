inherited Configuracion: TConfiguracion
  Left = 563
  Top = 274
  Caption = 'Configuraci'#243'n de empresas'
  ClientHeight = 414
  ClientWidth = 666
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [0]
    Left = 27
    Top = 174
    Width = 49
    Height = 13
    Caption = 'Empresas:'
  end
  object Label10: TLabel [1]
    Left = 22
    Top = 67
    Width = 60
    Height = 13
    Caption = 'Contrase'#241'a :'
    Color = clBtnHighlight
    ParentColor = False
  end
  object Label11: TLabel [2]
    Left = 30
    Top = 75
    Width = 60
    Height = 13
    Caption = 'Contrase'#241'a :'
    Color = clBtnHighlight
    ParentColor = False
  end
  object Label12: TLabel [3]
    Left = 38
    Top = 83
    Width = 60
    Height = 13
    Caption = 'Contrase'#241'a :'
    Color = clBtnHighlight
    ParentColor = False
  end
  object Label37: TLabel [4]
    Left = 63
    Top = 183
    Width = 69
    Height = 13
    Caption = 'Llave publica :'
    Color = clBtnHighlight
    ParentColor = False
  end
  inherited PanelBotones: TPanel
    Top = 378
    Width = 666
    inherited OK_DevEx: TcxButton
      Left = 490
      ModalResult = 1
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 571
    end
  end
  object pgConfig: TcxPageControl [6]
    Left = 0
    Top = 0
    Width = 666
    Height = 378
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = TsBanco
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 376
    ClientRectLeft = 2
    ClientRectRight = 664
    ClientRectTop = 27
    object TsBanco: TcxTabSheet
      Caption = 'Banco'
      object cxGroupBox1: TcxGroupBox
        Left = 8
        Top = 6
        Caption = 'Cuenta HSBC :'
        TabOrder = 0
        Height = 105
        Width = 641
        object Label3: TLabel
          Left = 79
          Top = 19
          Width = 46
          Height = 13
          Caption = 'HSBC ID:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label4: TLabel
          Left = 23
          Top = 43
          Width = 102
          Height = 13
          Caption = 'Nombre de la cuenta:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label6: TLabel
          Left = 3
          Top = 67
          Width = 122
          Height = 13
          Caption = 'No. de cuenta ordenante:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object txtBank_id: TZetaEdit
          Left = 128
          Top = 16
          Width = 170
          Height = 21
          TabOrder = 0
        end
        object txtAcount_Name: TZetaEdit
          Left = 128
          Top = 40
          Width = 170
          Height = 21
          TabOrder = 1
        end
        object txtAcount_Number: TZetaEdit
          Left = 128
          Top = 64
          Width = 170
          Height = 21
          TabOrder = 2
        end
      end
      object cxGroupBox2: TcxGroupBox
        Left = 8
        Top = 113
        Caption = 'Empleados con cuenta bancaria'
        TabOrder = 1
        Height = 56
        Width = 641
        object Label2: TLabel
          Left = 36
          Top = 18
          Width = 91
          Height = 13
          Caption = 'Banco beneficiario:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object txtBanco_Con_Cuenta: TZetaEdit
          Left = 128
          Top = 15
          Width = 171
          Height = 21
          TabOrder = 0
        end
      end
      object cxGroupBox3: TcxGroupBox
        Left = 9
        Top = 170
        Caption = 'Empleados sin cuenta bancaria'
        TabOrder = 2
        Height = 79
        Width = 641
        object Label1: TLabel
          Left = 36
          Top = 21
          Width = 91
          Height = 13
          Caption = 'Banco beneficiario:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label21: TLabel
          Left = 43
          Top = 45
          Width = 84
          Height = 13
          Caption = 'Clave de servicio:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label19: TLabel
          Left = 313
          Top = 45
          Width = 83
          Height = 13
          Caption = 'Vigencia cheque:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label16: TLabel
          Left = 466
          Top = 45
          Width = 23
          Height = 13
          Caption = 'D'#237'as'
          Color = clBtnHighlight
          ParentColor = False
        end
        object txtBanco_Sin_Cuenta: TZetaEdit
          Left = 128
          Top = 18
          Width = 173
          Height = 21
          TabOrder = 0
        end
        object txtNum_Contrato: TZetaEdit
          Left = 128
          Top = 41
          Width = 173
          Height = 21
          TabOrder = 1
        end
        object txtParam4: TZetaNumero
          Left = 397
          Top = 41
          Width = 67
          Height = 21
          Mascara = mnDias
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Text = '0'
          OnExit = txtParam4Exit
        end
      end
      object cxGroupBox5: TcxGroupBox
        Left = 9
        Top = 256
        Caption = 'Par'#225'metros auxiliares'
        TabOrder = 3
        Height = 89
        Width = 641
        object Label17: TLabel
          Left = 27
          Top = 21
          Width = 100
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'a de pago (sumar):'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label18: TLabel
          Left = 328
          Top = 21
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora de pago:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label20: TLabel
          Left = 67
          Top = 45
          Width = 60
          Height = 13
          Caption = 'Par'#225'metro 3:'
          Color = clBtnHighlight
          ParentColor = False
        end
        object txtParam1: TZetaEdit
          Tag = -1
          Left = 128
          Top = 17
          Width = 170
          Height = 21
          TabOrder = 0
        end
        object txtParam2: TZetaEdit
          Tag = -1
          Left = 397
          Top = 17
          Width = 170
          Height = 21
          TabOrder = 1
        end
        object txtParam3: TZetaEdit
          Tag = -1
          Left = 128
          Top = 41
          Width = 170
          Height = 21
          TabOrder = 2
        end
      end
    end
    object tsConexion: TcxTabSheet
      Caption = 'Conexi'#243'n'
      ImageIndex = 1
      TabVisible = False
      object cxGroupBox4: TcxGroupBox
        Left = 88
        Top = 73
        Caption = 'Servidor y autentificaci'#243'n :'
        TabOrder = 0
        Height = 175
        Width = 489
        object Label7: TLabel
          Left = 70
          Top = 21
          Width = 45
          Height = 13
          Caption = 'Servidor :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label8: TLabel
          Left = 73
          Top = 44
          Width = 42
          Height = 13
          Caption = 'Usuario :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label9: TLabel
          Left = 53
          Top = 69
          Width = 61
          Height = 13
          Caption = 'Passphrase :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label13: TLabel
          Left = 29
          Top = 93
          Width = 86
          Height = 13
          Caption = 'Directorio remoto :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label14: TLabel
          Left = 45
          Top = 118
          Width = 69
          Height = 13
          Caption = 'Llave p'#250'blica :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label15: TLabel
          Left = 361
          Top = 20
          Width = 37
          Height = 13
          Caption = 'Puerto :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object Label22: TLabel
          Left = 44
          Top = 140
          Width = 70
          Height = 13
          Caption = 'Llave privada :'
          Color = clBtnHighlight
          ParentColor = False
        end
        object txtServidor: TZetaEdit
          Left = 121
          Top = 16
          Width = 216
          Height = 21
          TabOrder = 0
        end
        object txtUsuario: TZetaEdit
          Left = 121
          Top = 40
          Width = 216
          Height = 21
          TabOrder = 1
        end
        object txtDirectorio_Remoto: TZetaEdit
          Left = 121
          Top = 88
          Width = 336
          Height = 21
          TabOrder = 3
        end
        object btnLlaveArchivo: TcxButton
          Left = 460
          Top = 112
          Width = 21
          Height = 21
          Caption = 'Buscar...'
          TabOrder = 5
          OnClick = btnLlaveArchivoClick
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3
            FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3
            FAFFDDF3FAFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4EEFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
            F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFF92DDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFE8F6FAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF53CA
            EFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
            F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF7ED7F2FF7ED7
            F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
            FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 13
          PaintStyle = bpsGlyph
        end
        object txtPass: TMaskEdit
          Left = 121
          Top = 64
          Width = 216
          Height = 21
          PasswordChar = '*'
          TabOrder = 2
        end
        object txtPuerto: TZetaEdit
          Tag = -1
          Left = 400
          Top = 17
          Width = 57
          Height = 21
          TabOrder = 4
        end
        object txtLlave_publica: TZetaEdit
          Left = 121
          Top = 112
          Width = 336
          Height = 21
          TabOrder = 6
        end
        object txtLlave_privada: TZetaEdit
          Left = 121
          Top = 136
          Width = 336
          Height = 21
          TabOrder = 7
        end
        object cxButton1: TcxButton
          Left = 460
          Top = 136
          Width = 21
          Height = 21
          Caption = 'Buscar...'
          TabOrder = 8
          OnClick = cxButton1Click
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3
            FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3
            FAFFDDF3FAFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4EEFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
            F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFF92DDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFE8F6FAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF53CA
            EFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
            F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFC
            FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
            FCFFFCFCFCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF7ED7F2FF7ED7
            F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
            FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          OptionsImage.Spacing = 13
          PaintStyle = bpsGlyph
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 5767552
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '*.*'
    Filter = 'Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Importar'
    Left = 638
    Top = 314
  end
end
