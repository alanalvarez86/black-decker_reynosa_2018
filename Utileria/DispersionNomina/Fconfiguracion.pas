unit FConfiguracion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx,ZBaseGlobal, StdCtrls, Buttons, ExtCtrls, Mask, ZetaNumero, ZetaAsciiFile, IniFiles,
  ComCtrls, ZetaKeyLookup, ZetaEdit, ZBaseDlgModal, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, ImgList, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit, cxGroupBox, cxPC,
  cxTextEdit, cxMemo, ZBaseGlobal_DevEx,ZetaClientTools, dxBarBuiltInMenu;

type
  TTArchivosIni = class( TObject )
  private
    { Private declarations }
   // FiniFile: TiniFile;


  public
    { Public declarations }



  end;
  TConfiguracion = class(TBaseGlobal_DevEx)
    pgConfig: TcxPageControl;
    TsBanco: TcxTabSheet;
    tsConexion: TcxTabSheet;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    txtBank_id: TZetaEdit;
    Label4: TLabel;
    txtAcount_Name: TZetaEdit;
    Label5: TLabel;
    txtAcount_Number: TZetaEdit;
    Label6: TLabel;
    cxGroupBox2: TcxGroupBox;
    Label2: TLabel;
    txtBanco_Con_Cuenta: TZetaEdit;
    cxGroupBox3: TcxGroupBox;
    Label1: TLabel;
    txtBanco_Sin_Cuenta: TZetaEdit;
    cxGroupBox4: TcxGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    txtServidor: TZetaEdit;
    txtUsuario: TZetaEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    txtDirectorio_Remoto: TZetaEdit;
    Label13: TLabel;
    Label14: TLabel;
    btnLlaveArchivo: TcxButton;
    txtPass: TMaskEdit;
    cxGroupBox5: TcxGroupBox;
    Label17: TLabel;
    txtParam1: TZetaEdit;
    txtParam2: TZetaEdit;
    Label18: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    txtNum_Contrato: TZetaEdit;
    Label15: TLabel;
    txtPuerto: TZetaEdit;
    txtLlave_publica: TZetaEdit;
    Label37: TLabel;
    Label19: TLabel;
    txtParam3: TZetaEdit;
    Label16: TLabel;
    txtLlave_privada: TZetaEdit;
    Label22: TLabel;
    cxButton1: TcxButton;
    txtParam4: TZetaNumero;
    OpenDialog: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure txtParam4Exit(Sender: TObject);
    procedure btnLlaveArchivoClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);



  private
    { Private declarations }
    FIniValues: TTArchivosIni;
  public
    { Public declarations }
    property IniValues: TTArchivosIni read FIniValues;
  end;

var
  Configuracion: TConfiguracion;

implementation

{$R *.DFM}

uses  DInterfase,DGlobal,ZetaCommonTools, ZGlobalTress,ZetaServerTools,ZetaDialogo,ZAccesosMgr, ZAccesosTress, ZetaCommonClasses;

const
     K_EMPRESAS = 'Empresas';
     K_CAMPOS = 'CAMPOS';



procedure TConfiguracion.FormShow(Sender: TObject);
var
iDias:Integer;
s:string;
begin
     inherited;
     pgConfig.ActivePage := TsBanco;
     s:= Global.GetGlobalString(  K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_4 );
     if ( ZetaCommonTools.strLleno ( s ) )     then
     begin
          txtpass.text:= ZetaServerTools.Decrypt(s);
     end;

    //ShowMessage(s);
    // txtParam4.Text:=IntToStr(Global.GetGlobalInteger(K_GLOBAL_ESPECIAL_FECHA_GLOBAL_5));
     //iDias:= StrToInt(txtLlave_publica.Text);
    { iDias:=Global.GetGlobalInteger(K_GLOBAL_ESPECIAL_FECHA_GLOBAL_5);
     if( iDias= 0)   then
        begin
           iDias:=180;
        end;
      txtParam4.Text:= IntToStr(iDias); }

end;

procedure TConfiguracion.FormCreate(Sender: TObject);
begin


     inherited;
     ZAccesosMgr.LoadDerechos;
     IndexDerechos               :=  ZAccesosTress.D_CAT_CONFI_GLOBALES;
     txtPuerto.Tag               :=  K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_1;   //252
     txtServidor.Tag             :=  K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_2;  //253
     txtUsuario.Tag              :=  K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_3;  //254
     txtPass.Tag                 :=  K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_4;  //255
     txtDirectorio_Remoto.Tag    :=  K_GLOBAL_ESPECIAL_NUM_ENTERO_GLOBAL_5;  //256
     txtBank_id.Tag              :=  K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_1;   //257
     txtAcount_Name.Tag          :=  K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_2;   //258
     txtAcount_Number.Tag        :=  K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_3;   //259
     txtBanco_Con_Cuenta.Tag     :=  K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_4;   //260
     txtBanco_Sin_Cuenta.Tag     :=  K_GLOBAL_ESPECIAL_TEXTO_GLOBAL_5;   //261
     txtNum_Contrato.Tag         :=  K_GLOBAL_ESPECIAL_FECHA_GLOBAL_1;   //267
     txtParam1.Tag               :=  K_GLOBAL_ESPECIAL_FECHA_GLOBAL_2;   //268
     txtParam2.Tag               :=  K_GLOBAL_ESPECIAL_FECHA_GLOBAL_3;   //269
     txtParam3.Tag               :=  K_GLOBAL_ESPECIAL_FECHA_GLOBAL_4;   //270
     txtParam4.Tag               :=  K_GLOBAL_ESPECIAL_FECHA_GLOBAL_5;   //271
     txtLlave_publica.Tag        :=  K_GLOBAL_ESPECIAL_NUM_GLOBAL_4;
     txtLlave_privada.Tag        :=  K_GLOBAL_ESPECIAL_NUM_GLOBAL_5;

end;


procedure TConfiguracion.txtParam4Exit(Sender: TObject);
var
 iDias:Integer;
begin
   iDias:= 1;
  //iDias:=strToInt(txtParam4.Text);
  if(iDias>365) or (idias<=0) then
     begin
        ZetaDialogo.ZetaMessage(Caption, 'La vigencia del cheque no puede ser mayor a 365 d�as ni menor a un d�a', mtInformation, [mbOK], 0, mbOk);
        txtParam4.SetFocus;
     end
end;   
procedure TConfiguracion.btnLlaveArchivoClick(Sender: TObject);
begin
  inherited;
  txtLlave_publica.Text := ZetaClientTools.AbreDialogo( OpenDialog, txtLlave_publica.Text, '*.*' );
end;

procedure TConfiguracion.cxButton1Click(Sender: TObject);
begin
  inherited;
  txtLlave_privada.Text := ZetaClientTools.AbreDialogo( OpenDialog, txtLlave_privada.Text, '*.*' );
end;

procedure TConfiguracion.OK_DevExClick(Sender: TObject);
begin
  txtpass.Text:= ZetaServerTools.Encrypt( txtpass.Text );
  inherited;

end;

end.
