unit DispersionNomina_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 5/5/2015 4:39:27 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_30_Vsn_2015\Utileria\DispersionNomina\MTS\DispersionNomina.tlb (1)
// LIBID: {07D5803D-07A3-4F09-8CDF-90FC992EFBB2}
// LCID: 0
// Helpfile: 
// HelpString: DispersionNomina Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\SysWOW64\stdole2.tlb)
//   (2) v1.0 Midas, (C:\Windows\SysWOW64\midas.dll)
//   (3) v4.0 StdVCL, (C:\Windows\SysWOW64\stdvcl40.dll)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}

interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  DispersionNominaMajorVersion = 1;
  DispersionNominaMinorVersion = 0;

  LIBID_DispersionNomina: TGUID = '{07D5803D-07A3-4F09-8CDF-90FC992EFBB2}';

  IID_IdmDispersionNomina: TGUID = '{59A05069-FFD5-4AAA-B081-BFB331C8BB70}';
  CLASS_dmServerDispersionNomina: TGUID = '{37C5F3D9-DFB2-46C4-853B-D6E88E787F09}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmDispersionNomina = interface;
  IdmDispersionNominaDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerDispersionNomina = IdmDispersionNomina;


// *********************************************************************//
// Interface: IdmDispersionNomina
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {59A05069-FFD5-4AAA-B081-BFB331C8BB70}
// *********************************************************************//
  IdmDispersionNomina = interface(IAppServer)
    ['{59A05069-FFD5-4AAA-B081-BFB331C8BB70}']
    function GetUltimoPeriodo(Empresa: OleVariant; const Query: WideString): OleVariant; safecall;
    function Get_Empleados_cta(Year: Integer; Tipo: Integer; NominaNumero: Integer; 
                               const TieneCuenta: WideString; Empresa: OleVariant): Integer; safecall;
    function Genera_XML(Anio: Integer; Nomina_Tipo: Integer; Nomina_Numero: Integer; 
                        const TieneCuenta: WideString; Empresa: OleVariant; Usuario: Integer; 
                        out iWD_FOLIO: Integer): WideString; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmDispersionNominaDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {59A05069-FFD5-4AAA-B081-BFB331C8BB70}
// *********************************************************************//
  IdmDispersionNominaDisp = dispinterface
    ['{59A05069-FFD5-4AAA-B081-BFB331C8BB70}']
    function GetUltimoPeriodo(Empresa: OleVariant; const Query: WideString): OleVariant; dispid 301;
    function Get_Empleados_cta(Year: Integer; Tipo: Integer; NominaNumero: Integer; 
                               const TieneCuenta: WideString; Empresa: OleVariant): Integer; dispid 302;
    function Genera_XML(Anio: Integer; Nomina_Tipo: Integer; Nomina_Numero: Integer; 
                        const TieneCuenta: WideString; Empresa: OleVariant; Usuario: Integer; 
                        out iWD_FOLIO: Integer): WideString; dispid 303;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerDispersionNomina provides a Create and CreateRemote method to          
// create instances of the default interface IdmDispersionNomina exposed by              
// the CoClass dmServerDispersionNomina. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerDispersionNomina = class
    class function Create: IdmDispersionNomina;
    class function CreateRemote(const MachineName: string): IdmDispersionNomina;
  end;

implementation

uses ComObj;

class function CodmServerDispersionNomina.Create: IdmDispersionNomina;
begin
  Result := CreateComObject(CLASS_dmServerDispersionNomina) as IdmDispersionNomina;
end;

class function CodmServerDispersionNomina.CreateRemote(const MachineName: string): IdmDispersionNomina;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerDispersionNomina) as IdmDispersionNomina;
end;

end.
