unit DServerDispersionNomina;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl,
  DataBkr, DBClient, DB,
  MtsRdm, Mtx,
  Variants,
  DispersionNomina_TLB,
  DZetaServerProvider, ZetaServerDataSet;

type
  eTipoCatalogo = ( eTurnosGeneralizados,     {0}
                    eTurnosExcepciones );      {1}

  TdmServerDispersionNomina = class(TMtsDataModule, IdmDispersionNomina)
    procedure RemoteDatamoduleCreate(Sender: TObject);
    procedure RemoteDatamoduleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider : TdmZetaServerProvider;
    FErrorEjecucion : String;
    procedure SetTablaInfo();

  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    function GetUltimoPeriodo(Empresa: OleVariant; const Query: WideString): OleVariant;
          safecall;
    function Get_Empleados_cta(Year, Tipo, NominaNumero: Integer;
      const TieneCuenta: WideString; Empresa: OleVariant): Integer;
      safecall;
    function Genera_XML(Anio, Nomina_Tipo, Nomina_Numero: Integer; const TieneCuenta: WideString;
          Empresa: OleVariant; Usuario: Integer; out iWD_Folio: Integer): WideString;
          safecall;
    function Set_StatusNom(WD_FOLIO: Integer; WD_ENVIADO: WordBool; const WD_ARCHIVO,
          WD_OBSERVA: WideString; US_CODIGO: Integer; Empresa: OleVariant): WordBool;
          safecall;

  public
    function  GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
          safecall;

function GetReporteSap(Empresa: OleVariant): OleVariant; safecall;


  end;

var
  dmServerDispersionNomina: TdmServerDispersionNomina;

implementation

{$R *.DFM}


uses
    ZetaCommonTools, ZetaServerTools, ZetaCommonLists, zetacommonclasses;


class procedure TdmServerDispersionNomina.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerDispersionNomina.RemoteDatamoduleCreate(Sender: TObject);
begin
      oZetaProvider := TdmZetaServerProvider.Create( self );
end;

procedure TdmServerDispersionNomina.RemoteDatamoduleDestroy(Sender: TObject);
begin
     oZetaProvider.Free;
end;


procedure TdmServerDispersionNomina.SetTablaInfo();
begin
  with oZetaProvider.TablaInfo do
  begin
        SetInfo( 'IN_REP_SAP', 'RE_CODIGO,IR_PAYROLL,IR_PURPOSE,IR_PE_TIPO,US_CODIGO,US_FEC_MOD','RE_CODIGO');
        SetOrder('RE_CODIGO');
       // BeforeUpdateRecord := BeforeUpdateReporte;
     end;
  end;






function TdmServerDispersionNomina.GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant;
                           out ErrorCount: Integer): OleVariant;
var
    FTipoCatalogo : eTipoCatalogo;
begin
    { FTipoCatalogo := eTipoCatalogo( iCatalogo );
     settablainfo();
     FErrorEjecucion := '';
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitGlobales;
          Result := GrabaTabla( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
     //si existe error
     if ( FErrorEjecucion.Trim <> '') then
     begin
        DatabaseError( FErrorEjecucion ) ;
     end;   }
end;


//** Consultas **//



//** Actualizaciones **//



function TdmServerDispersionNomina.GetReporteSap(Empresa: OleVariant): OleVariant;
begin
end;

function TdmServerDispersionNomina.GetUltimoPeriodo(Empresa: OleVariant; const Query: WideString): OleVariant;
begin
 FErrorEjecucion := '';
     with oZetaProvider do
     begin
          try
             EmpresaActiva := Empresa;
             try
                Result := OpenSQL( Empresa, query, True );
             except
                   on Error: Exception do
                   begin
                        FErrorEjecucion := Error.message;
                   end;
             end;
          finally
               SetComplete;
          end;
     end;
end;

function TdmServerDispersionNomina.Get_Empleados_cta(Year, Tipo,
  NominaNumero: Integer; const TieneCuenta: WideString;
  Empresa: OleVariant): Integer;
  var
    query :WideString;
   // test : Integer;
    FDataset: TZetaCursor;
begin

    query:= 'select dbo.PE_NominaTieneCuentas('+IntToStr(Year) +','+IntToStr(Tipo)+','+IntToStr(NominaNumero) +','+TieneCuenta+') as Num_Nominas';
    FErrorEjecucion := '';
     with oZetaProvider do
     begin
          try
             EmpresaActiva := Empresa;
             try
                FDataset := CreateQuery( query);
                with FDataset do
                   begin
                    Active := True;
                     if EOF then
                        Result := 0
                     else
                      Result := FieldByName( 'Num_Nominas' ).AsInteger;
                  Active := False;
             end;
             except
                   on Error: Exception do
                   begin
                        FErrorEjecucion := Error.message;
                   end;
             end;
          finally
               SetComplete;
               FreeAndNil( FDataset );
       end;
     end;
end;

function TdmServerDispersionNomina.Genera_XML(Anio, Nomina_Tipo, Nomina_Numero: Integer;
          const TieneCuenta: WideString; Empresa: OleVariant; Usuario: Integer;
          out iWD_Folio: Integer): WideString;
const
   Q_GENERA_ARCHIVO_DISPERSION = 'EXECUTE PROCEDURE  PE_DispersionNomina_GenerarArchivo( :PE_YEAR, :PE_TIPO, :PE_NUMERO, :TIENE_CUENTA, :WD_FOLIO, :USUARIO, :EMPRESA)';
   Q_OBTIENE_ARCHIVO_DISPERSION = 'select WD_XML from WB_DISPERS where WD_FOLIO = %d ';

var
    test: Integer;
    FDataset, FXMLDataset: TZetaCursor;

begin

FErrorEjecucion := '';
Result := '';

  with oZetaProvider do
     begin
          try
             try
                EmpresaActiva := Empresa;
                FDataset := CreateQuery( Q_GENERA_ARCHIVO_DISPERSION );
                with FDataSet do
                begin
                     ParamAsInteger( FDataset, 'PE_YEAR' , Anio );
                     ParamAsInteger( FDataset, 'PE_TIPO' , Nomina_Tipo );
                     ParamAsInteger( FDataset, 'PE_NUMERO' , Nomina_Numero );
                     ParamAsVarChar( FDataset, 'TIENE_CUENTA' , TieneCuenta ,1 );
                     ParamAsString( FDataset, 'EMPRESA', CodigoEmpresaActiva );
                     ParamAsInteger( FDataset, 'USUARIO' , Usuario );
                     ParamSalida( FDataset, 'WD_FOLIO' );

                     Ejecuta( FDataset );
                     iWD_FOLIO :=  GetParametro( FDataset, 'WD_FOLIO' ).AsInteger ;

                     FXMLDataset := CreateQuery(  Format(  Q_OBTIENE_ARCHIVO_DISPERSION, [iWD_FOLIO] ) );
                     FXMLDataset.Active := True;

                     if ( not  FXMLDataset.Eof ) then
                     begin
                          Result :=  FXMLDataset.FieldByName( 'WD_XML' ).AsString;
                     end
                end;
             except
                   on Error: Exception do
                   begin
                        DatabaseError('Error al generar archivo de dispersion:'+ error.Message );
                   end;
             end;
          finally
               SetComplete;
               FreeAndNil( FDataset );
          end;
     end;
end;

function TdmServerDispersionNomina.Set_StatusNom(WD_FOLIO: Integer; WD_ENVIADO: WordBool; const WD_ARCHIVO, WD_OBSERVA: WideString; US_CODIGO: Integer;
          Empresa: OleVariant): WordBool;
const
   Q_GENERA_ARCHIVO_DISPERSION = 'EXECUTE PROCEDURE  PE_DispersionNomina_NotificarEstatus( :WD_FOLIO, :WD_ENVIADO, :WD_ARCHIVO, :WD_OBSERVA, :USUARIO)';
var
  FDataset, FXMLDataset: TZetaCursor;
  lOk:Boolean;
begin
  lOk:=False;
  with oZetaProvider do
     begin
          try
             try
                EmpresaActiva := Empresa;
                FDataset := CreateQuery( Q_GENERA_ARCHIVO_DISPERSION );
                with FDataSet do
                begin

                     ParamAsInteger( FDataset, 'WD_FOLIO' , WD_FOLIO );
                     ParamAsBoolean( FDataset, 'WD_ENVIADO' , WD_ENVIADO );
                     ParamAsString( FDataset, 'WD_ARCHIVO' , WD_ARCHIVO );
                     ParamAsString( FDataset, 'WD_OBSERVA' , WD_OBSERVA);
                     ParamAsInteger( FDataset, 'USUARIO' , US_CODIGO );

                     Ejecuta( FDataset );
                     lOk:=True;
                end;
             except
                   on Error: Exception do
                   begin
                        DatabaseError('Error al generar archivo de dispersion:'+ error.Message );
                        lOk:=False
                   end;
             end;
          finally
               SetComplete;
               FreeAndNil( FDataset );
               Result:=lOk;
          end;
     end;




end;

initialization
  TComponentFactory.Create(ComServer, TdmServerDispersionNomina,
    Class_dmServerDispersionNomina, ciMultiInstance, ZetaServerTools.GetThreadingModel);
end.