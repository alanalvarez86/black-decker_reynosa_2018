unit FTPClass;

interface

uses
 Curl,ZetaDialogo,ZetaCommonTools,ZetaServerTools,Classes,ZetaSystemWorking,cxProgressBar,ZBaseImportaShell_DevEx,SysUtils,StrUtils, IdCoder3to4, IdCoderMIME, IdBaseComponent,Dialogs;

function Subir(Usuario, Clave, URL, sXML,llave,llaveprivate,TieneCuenta,Conexion: AnsiString; LMostrarMensaje: Boolean; var sMensajeBitacora :String ): Boolean;
function progressCallback( UserData:pChar; DownloadTotal:double; DownloadNow:double;UploadTotal:double; UploadNow:double ): LongInt; cdecl;
function Bajar(Usuario, Clave, URL: AnsiString): Boolean;
function Mapeollave( sRuta:String ): String;
function ExisteArchivo(Usuario, Clave, URL: AnsiString): Boolean;
function DecodeBase46( base64Str : string ) : string;
function DebugFunc(Curl: TCURL; InfoType: Integer; Text: PAnsiChar; Size: Integer; Stream: TStream): Integer; cdecl;

type TCurlFTP = class(TObject)
    public
      RX: Int64;
      TX: Int64;
      DLNow, ULNow: double;
      FInProgress: Boolean;
end;




implementation



function ReadFromStream(Buffer: PAnsiChar; Size, Count: Integer;
  Stream: TStream): Integer; cdecl;
begin
  Result:= Stream.Read(Buffer^,Size*Count) div Size;
end;

function SaveToStream(Buffer: PAnsiChar; Size, Count: Integer;
  Stream: TStream): Integer; cdecl;
begin
  Result:= Stream.Write(Buffer^,Size*Count) div Size;
end;

function StringToStream(const AString: string): TStream;
begin
  Result := TStringStream.Create(AString);
end;

function StreamToString(aStream: TStream): string;
var
  SS: TStringStream;
begin
  if aStream <> nil then
  begin
    SS := TStringStream.Create('');
    try
      SS.CopyFrom(aStream, 0);  // No need to position at 0 nor provide size
      Result := SS.DataString;
    finally
      SS.Free;
    end;
  end else
  begin
    Result := '';
  end;
end;


function ExisteArchivo(Usuario, Clave, URL: AnsiString): Boolean;
var
  Curl: TCURL;
  Stream: TMemoryStream;
  CodigoCurl:CURLcode;
begin
  Result:= FALSE;
  Curl:= curl_easy_init;
  if Curl <> nil then
  try
    if curl_easy_setopt(Curl, CURLOPT_VERBOSE, TRUE) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_USE_SSL, CURLUSESSL_ALL) <> CURLE_OK then
      Exit;
    // No verificamos la identidad del servidor, suponemos que es quien dice ser
    if curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYPEER, FALSE) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_USERNAME, PAnsiChar(Usuario)) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_PASSWORD, PAnsiChar(Clave)) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_URL, PChar(URL)) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, @SaveToStream) <> CURLE_OK then
      Exit;
    Stream:= TMemoryStream.Create;
    try
      if curl_easy_setopt(Curl, CURLOPT_FILE, Stream) <> CURLE_OK then
        Exit;
      CodigoCurl:=curl_easy_perform(Curl);
     if (CodigoCurl  <> CURLE_OK ) then
                     begin
                        result:=False;
                     end
     else
        begin
           result:= True;
        end;
      // Con los datos del stream podemos hacer cualquier cosa
     // Stream.SaveToFile('C:\1.txt');
    finally
      Stream.Free;
    end;
  finally
    curl_easy_cleanup(Curl);
end;
end;

function progressCallback( UserData:pChar; DownloadTotal:double; DownloadNow:double;UploadTotal:double; UploadNow:double ): LongInt; cdecl;
begin
 result:=0;
end;

function Bajar(Usuario, Clave, URL: AnsiString): Boolean;
var
  Curl: TCURL;
  Stream: TMemoryStream;
begin
  Result:= FALSE;
  Curl:= curl_easy_init;
  if Curl <> nil then
  try
    if curl_easy_setopt(Curl, CURLOPT_VERBOSE, TRUE) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_USE_SSL, CURLUSESSL_ALL) <> CURLE_OK then
      Exit;
    // No verificamos la identidad del servidor, suponemos que es quien dice ser
    if curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYPEER, FALSE) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_USERNAME, PAnsiChar(Usuario)) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_PASSWORD, PAnsiChar(Clave)) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_URL, PChar(URL)) <> CURLE_OK then
      Exit;
    if curl_easy_setopt(Curl, CURLOPT_WRITEFUNCTION, @SaveToStream) <> CURLE_OK then
      Exit;
    Stream:= TMemoryStream.Create;
    try
      if curl_easy_setopt(Curl, CURLOPT_FILE, Stream) <> CURLE_OK then
        Exit;
      Result:= curl_easy_perform(Curl) = CURLE_OK;
      // Con los datos del stream podemos hacer cualquier cosa
      Stream.SaveToFile('C:\1.txt');
    finally
      Stream.Free;
    end;
  finally
    curl_easy_cleanup(Curl);
  end;
end;


function QuitarSaltosLinea(Strs: String; CharReplace:String):String;
 var
   Str:string;
 begin
   Str := StringReplace(Strs, #10, CharReplace,[rfReplaceAll]);
   Result := StringReplace(Str, #13, CharReplace,[rfReplaceAll] );
end;

function Mapeollave( sRuta:String  ): String;
Const
K_TIPO_Llave = 'ssh-rsa';
K_FIN_Llave = 'imported-openssh-key';
K_SEPARADOR ='|';
var
FDatos,FRenglon, FLlave : TStringList;
sDatos,sllave:String;
iPosIni,iCommentIni,iCommentFin,iContador,iPosExtension,iPosFormato: integer;
begin
     FDatos := TStringList.Create;
     FDatos.LoadFromFile(sRuta);
     iPosFormato:= Ansipos( K_FIN_Llave , FDatos.Text );
     if( iPosFormato = 0 )  then
     begin
          Fdatos.Text:= QuitarSaltosLinea( StringReplace ( Fdatos.text,'----','|',[rfReplaceAll] ) ,'' );
          if( Fdatos.Count > 0 ) then
          begin
               FRenglon := TStringList.Create;
               FLlave := TStringList.Create;
               try
               iContador:=0;
               sDatos:=FDatos[0];
               sDatos:= UTF8Decode( sDatos );
               sDatos := StringReplace(sDatos, 'Comment:','',[rfReplaceAll]);
               iPosIni:=AnsiPos('"',sDatos);
               While ( iPosIni <> 0 )  do
               begin
                    Delete(sDatos,iPosIni,1);
                    if(iContador = 0 )  then
                    begin
                         iCommentIni:= iPosIni;
                    end
                    else if(iContador=1) then
                         begin
                              iCommentFin:= iPosIni;
                         end;
                    iContador:= iContador + 1;
                    iPosIni:=AnsiPos('"',sDatos);
               end;
               Delete(sDatos,iCommentIni,iCommentFin-iCommentIni);
               FRenglon.Delimiter := K_SEPARADOR;
               sDatos := StrTransAll( sDatos, K_SEPARADOR , '"'+ K_SEPARADOR + '"' );
               FRenglon.DelimitedText := '"' + sDatos +'"';
               if( FRenglon.Count >  0 )  then
               begin
                    sllave:=trim( FRenglon[2] );
                    sllave := QuitarSaltosLinea(sllave,'');
                    //sllave:=DecodeBase46(sllave);
                    FRenglon[0]:='';
                    sDatos:='';
                    sDatos:=K_TIPO_LLAVE +' '+ sllave +' '+K_FIN_Llave;
                    sDatos := QuitarSaltosLinea(sDatos,'');     //sDatos.replaceAll('[\n\r]', '');
                    iPosExtension := LastDelimiter('.', sRuta);
                    Delete( sRuta , iPosExtension, length(sRuta) );
                    FLlave.Add(sDatos) ;
                    sRuta:= sRuta+'_OpenSSH.pub';
                    //sRuta:=StringReplace(sRuta,'.','_OpenSSH.pub',[rfReplaceAll]);
                    FLlave.SaveToFile(sRuta);
               end
               else
               begin
                     sRuta:='';
                    //  sMensajeBitacora:='Error al convertir llave pública';
               end;
               finally
                      FreeAndNil( FRenglon );
                      FreeAndNil( FLlave );
               end;
     end;
end;

   result:= sRuta;

end;

function DecodeBase46( base64Str : string ) : string;
     var
        Decoder: TIdDecoderMIME;

     begin
         REsult := '';
         try
           Decoder := TIdDecoderMIME.Create(nil);
           try
              Result := Decoder.DecodeString( base64Str)
           finally
             Decoder.Free;
           end;
         finally
    end;
end;


function DebugFunc(Curl: TCURL; InfoType: Integer; Text: PAnsiChar; Size: Integer; Stream: TStream): Integer; cdecl;
var
  Str: String;
begin
  Str:= #13 +DateTimeToStr(Now)+':   ';
  Str:= Str + Format(' %8.8x:%8.8x',[Integer(Curl),InfoType]) ;
  Stream.Write(PAnsiChar(Str)^,Length(Str));
  if AnsiPos('<?xml',Text)=0 then
  begin
        Stream.Write(Text^,Size);
  end;
  Result:= 0;
end;

          
function Subir(Usuario, Clave, URL, sXML,llave,llaveprivate,TieneCuenta,Conexion: AnsiString; LMostrarMensaje: Boolean; var  sMensajeBitacora :String ): Boolean;
var
  Curl: TCURL;
  Stream:TStream;
  DebugStream :TMemoryStream;
  CodigoCurl:CURLcode;
  lOk: Boolean;
  lExiste:Boolean;
  stemp: string;

begin
     lOk:= false;
     sTemp:='';
     Curl:= curl_easy_init;
     if(sXML='') then
     begin
          lOk:= false;
          sMensajeBitacora := 'No se pudo generar el archivo de dispersión de nómina '+ Url;
     end
     else
     begin
          if Curl <> nil then
          begin
                sTemp:=  mapeoLlave(llave);
                //lExiste:= ExisteArchivo(Usuario,Clave,URL);
                if(sTemp = '')  then
                begin
                     sMensajeBitacora:='Error en la llave publica verifique el formato';
                     lOk:=False;
                end
                else
                begin
                     try
                        llave:= sTemp;
                        Stream := StringToStream(sXML);
                        sMensajeBitacora:= 'Proceso Exitoso';
                        clave:= ZetaServerTools.Decrypt(clave);
                        ZetaSystemWorking.InitAnimation( 'Subiendo información...' );
                        //Configuración de Curl
                        if( curl_easy_setopt(Curl, CURLOPT_VERBOSE, TRUE) <> CURLE_OK)  or
                        // Le decimos que use SSL
                        (curl_easy_setopt(Curl, CURLOPT_USE_SSL, CURLUSESSL_ALL) <> CURLE_OK)  or
                        (curl_easy_setopt(Curl, CURLOPT_SSH_AUTH_TYPES, 1) <> CURLE_OK)  or
                        // No verificamos la identidad del servidor, suponemos que es quien dice ser.
                        (curl_easy_setopt(Curl, CURLOPT_SSL_VERIFYPEER, FALSE) <> CURLE_OK ) or
                        // asignamos la llave publica
                        (curl_easy_setopt(Curl, CURLOPT_SSH_PUBLIC_KEYFILE, PAnsiChar( llave ) ) <> CURLE_OK) or
                        (curl_easy_setopt(Curl, CURLOPT_SSH_PRIVATE_KEYFILE, PAnsiChar(llaveprivate)) <> CURLE_OK) or
                        //asignamos la llave privada
                        (curl_easy_setopt(Curl, CURLOPT_KEYPASSWD, PAnsiChar(Clave)) <> CURLE_OK) or
                        //Le indicamos que funcion debe usar para leer el stream
                        (curl_easy_setopt(Curl, CURLOPT_READFUNCTION, @ReadFromStream) <> CURLE_OK ) then
                        begin
                             lOk:=False;
                             sMensajeBitacora:='Error en la inicialización del objeto de conexión';
                        end
                        else
                            try
                               DebugStream:= TMemoryStream.Create;
                               curl_easy_setopt(Curl, CURLOPT_VERBOSE,true);
                               if curl_easy_setopt(Curl, CURLOPT_DEBUGFUNCTION, @DebugFunc) <> CURLE_OK then
                               Exit;
                               if curl_easy_setopt(Curl, CURLOPT_DEBUGDATA, DebugStream) <> CURLE_OK then
                               Exit;
                               // Indicamos el usuario
                               CodigoCurl:=curl_easy_setopt(Curl, CURLOPT_USERNAME, PAnsiChar(Usuario));
                               if(CodigoCurl<> CURLE_OK) then
                               begin
                                    sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                    lOk:= False;
                               end;
                               //Indicamos el password
                               CodigoCurl:=curl_easy_setopt(Curl, CURLOPT_PASSWORD, PAnsiChar(Clave)) ;
                               if(CodigoCurl<> CURLE_OK) then
                               begin
                                    sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                    lOk:= False;
                               end;
                               // La URL del tipo: sftp://servidor/rutadelfichero
                               CodigoCurl:=curl_easy_setopt(Curl, CURLOPT_URL, PAnsiChar(URL))  ;
                               if(CodigoCurl<> CURLE_OK) then
                               begin
                                    sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                    lOk:= False;
                               end;
                               // Aqui le indicamos el stream que debe subir
                               CodigoCurl:=curl_easy_setopt(Curl, CURLOPT_INFILE, Stream)  ;
                               if(CodigoCurl<> CURLE_OK) then
                               begin
                                    sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                    lOk:= False;
                               end;

                               // Aqui le indicamos que la operacion es de subida
                               CodigoCurl:= curl_easy_setopt(Curl, CURLOPT_UPLOAD, TRUE) ;

                               if(CodigoCurl<> CURLE_OK) then
                               begin
                                    sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                    lOk:= False;
                               end;
                               // Ejecutamos el comando
                               CodigoCurl:=curl_easy_perform( Curl );

                               if(CodigoCurl=CURLE_REMOTE_ACCESS_DENIED) then
                               begin
                                    sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                    lOk:= False;
                               end
                               else
                                   begin
                                        if(CodigoCurl=CURLE_COULDNT_CONNECT) then
                                        begin
                                             sMensajeBitacora:=GetCurlError(CodigoCurl) +' '+ URL +'  con el usuario: '+Usuario;
                                             lOk:= False;
                                        end
                                        else
                                        begin
                                             sMensajeBitacora:=GetCurlError(CodigoCurl);
                                             if ( sMensajeBitacora = 'Proceso Exitoso' ) then
                                             begin
                                                  lOk:= true;
                                             end
                                             else
                                             begin
                                                  lOk:= False;
                                             end;
                                        end;
                                   end;
                            finally
                            begin
                                 if (LMostrarMensaje)  then
                                 begin
                                       ZetaDialogo.zWarning('Advertencia', SMensajeBitacora, 0, mbCancel );
                                 end;
                                 sMensajeBitacora:= StreamToString( DebugStream );
                                 //DebugStream.SaveToFile('Test.txt'); // @DC   esta linea manda  la informacion generada por curl a un archivo para debugear.
                                 DebugStream.Free;
                                 ZetaSystemWorking.EndAnimation;
                                 Stream.Free;
                            end;
                       end;
                     finally
                            curl_easy_cleanup(Curl);
                     end;
                //result:= lOk;
                end;

          end; //end if Existe;
          result:= lOk;
     end;
end;

end.
