unit DInterfase;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Db, DBClient, ZetaClientDataSet, Variants, DZetaServerProvider,
    ZetaTipoEntidad,
    DispersionNomina_TLB,
    ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses, ZBaseEdicion, ZBaseEdicion_DevEx
    , XMLIntf;

type
  TdmInterfase = class(TDataModule)
    cdsDatosEmpleado: TZetaClientDataSet;
    cdsDatosConexion: TZetaClientDataSet;
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsDatosEmpleadoAlBorrar(Sender: TObject);
    procedure cdsDatosEmpleadoAlModificar(Sender: TObject);
    procedure cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
    procedure cdsDatosEmpleadoAlEnviarDatos(Sender: TObject);
    procedure cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsDatosConexionAlAdquirirDatos(Sender: TObject);



  private
    { Private declarations }
    FServerDispersion : IdmDispersionNominaDisp;

    oZetaProvider: TdmZetaServerProvider;
    FFechaInicial: TDate;
    FFechaFinal: TDate;
    FFechaReferencia: TDate;
    uSqlText : String;
    function GetServerDispersion: IdmDispersionNominaDisp;

  protected
    { Protected declarations }
    property ServerDispersion: IdmDispersionNominaDisp read GetServerDispersion;

  public
    { Public declarations }
    property FechaInicial: TDate read FFechaInicial write FFechaInicial;
    property FechaFinal: TDate read FFechaFinal write FFechaFinal;
    property FechaReferencia: TDate read FFechaReferencia write FFechaReferencia;
    property SQLText: String Read uSqlText write uSqlText;
    function GetEmpleados_cta(Year, Tipo, NominaNumero: Integer;
      const TieneCuenta: WideString; Empresa: OleVariant):Integer;
    function Crear_XML(Year, Tipo, NominaNumero: Integer;  const TieneCuenta: WideString; Empresa: OleVariant;Usuario: Integer;var  iWD_Folio :Integer ):WideString;
    function NotificarEstatus(FOLIO: Integer; ENVIADO: WordBool; const WD_ARCHIVO, WD_OBSERVA: WideString; US_CODIGO: Integer; Empresa: OleVariant): WordBool;
  end;

var
  dmInterfase: TdmInterfase;

const

     K_CONEXION_FTP='ftp://';
     K_CONEXION_SFTP='sftp://';
     K_CONEXION_DEFAULT='sftp://';
     K_EXTENSION_ARCHIVO = '.xml';
     K_NO_TIENE_CUENTA= 'CHK';
     K_SEPARADOR = '_';
     K_GLOBAL_SI ='S';
     K_GLOBAL_NO ='N';
     K_NOMINA ='NOMINA';
     K_ESPACIO = ' ';
     K_VACIO ='';
     K_SIN_DEFINIR = 0;
     K_STATUS_NUEVA = 0;
     K_STATUS_PRENOMINA_PARCIAL = 1;
     K_STATUS_SIN_CALCULAR = 2;
     K_STATUS_CALCULADA_PARCIAL = 3;
     K_STATUS_CALCULADA_TOTAL = 4;
     K_STATUS_AFECTADA_PARCIAL =5;
     K_STATUS_AFECTADA_TOTAL = 6;
     K_HEADER_XML ='<?xml version="1.0" encoding="UTF-8" ?>';

implementation

uses
     dCliente,
     DGlobal,
     ZGlobalTress,
     ZAsciiTools,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaMsgDlg,
     ZReconcile,
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress,
     DSistema,
     FTressShell, DBasicoCliente;

{$R *.DFM}

{ TdmInterfase }


function TdmInterfase.GetServerDispersion: IdmDispersionNominaDisp;
begin
    Result:= IdmDispersionNominaDisp( dmCliente.CreaServidor( CLASS_dmServerDispersionNomina, FServerDispersion ) );
end;



procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     FreeAndNil( FServerDispersion);
end;

procedure TdmInterfase.cdsDatosEmpleadoAlBorrar(Sender: TObject);
begin
inherited;
    //no implementar
end;





function TdmInterfase.Crear_XML(Year, Tipo, NominaNumero: Integer;  const TieneCuenta: WideString; Empresa: OleVariant;Usuario: Integer;var  iWD_Folio :Integer ):WideString;
begin
   result:= ServerDispersion.Genera_XML(Year,Tipo,NominaNumero,TieneCuenta,Empresa,Usuario,iWD_Folio);
end;

function TdmInterfase.NotificarEstatus(FOLIO: Integer; ENVIADO: WordBool; const WD_ARCHIVO, WD_OBSERVA: WideString; US_CODIGO: Integer; Empresa: OleVariant): WordBool;
begin
  //result:=ServerDispersion.Set_StatusNom(FOLIO,ENVIADO,WD_ARCHIVO,WD_OBSERVA,US_CODIGO,Empresa);
  Result := TRUE;
end;


function TdmInterfase.GetEmpleados_cta(Year, Tipo, NominaNumero: Integer;
      const TieneCuenta: WideString; Empresa: OleVariant):Integer;
begin
   result:=  ServerDispersion.Get_Empleados_cta(Year,Tipo,NominaNumero, QuotedStr(TieneCuenta),Empresa);
end;

procedure TdmInterfase.cdsDatosEmpleadoAlModificar(Sender: TObject);
begin
inherited;
    // ZBaseEdicion_DevEx.ShowFormaEdicion( EditReporteSap, TEditReporteSap);
end;

procedure TdmInterfase.cdsDatosEmpleadoAlAdquirirDatos(Sender: TObject);
begin
       //no implementar
end;

procedure TdmInterfase.cdsDatosEmpleadoAlEnviarDatos(Sender: TObject);

begin
      //no implementar
end;

procedure TdmInterfase.cdsDatosEmpleadoAlCrearCampos(Sender: TObject);
begin
   //no implementar
end;

procedure TdmInterfase.cdsDatosConexionAlAdquirirDatos(Sender: TObject);
begin
   //no implementar
end;

end.
