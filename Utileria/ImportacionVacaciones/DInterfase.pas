unit DInterfase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, ZetaCommonLists,
  {$ifndef VER130}
  Variants,
  {$endif}
  {$ifdef DOS_CAPAS}
  DServerRecursos,
  {$else}
  Recursos_TLB,
  {$endif}
  ZetaCommonClasses,
  ZetaServerDataSet,
  ZetaClientTools;

type
    eTipoImportacion = ( taHistorial, taSaldo );
    TdmInterfase = class(TDataModule)
    cdsTempAscii: TZetaClientDataSet;
      procedure DataModuleCreate(Sender: TObject);
      procedure DataModuleDestroy(Sender: TObject);

      private
      { Private declarations }
      FHuboCambios, FProcesandoThread : Boolean;
      FTipoImportacion : eTipoImportacion;
      FArchivo : String;
      FIgualaGozo: Boolean;
      FSaldoPrimaEnCero: Boolean;
      FFechaReferencia: TDate;
      FParameterList: TZetaParams;
      FSaldosVaca : OleVariant;
      FSaldarAniversario:Boolean;

      {$ifdef DOS_CAPAS}
      function GetServerRecursos : TdmServerRecursos;
      property ServerRecursos :TdmServerRecursos read GetServerRecursos;
      {$else}

      FServidor : IdmServerRecursosDisp;
      function GetServerRecursos : IdmServerRecursosDisp ;
      property ServerRecursos : IdmServerRecursosDisp read GetServerRecursos;
      {$endif}

      function cdsASCII: TZetaClientDataSet;
      function cdsDataSet: TZetaClientDataSet;
      function ImportaAscii( cdsDataSet : TZetaClientDataSet; var ErrorCount: Integer; lEsHistorial: Boolean ): OleVariant;
      function GetSaldoVacaFecha(const iValor, iEmpleado: Integer; dFecha: TDate ): TPesos;
      function GetHeaderTexto(const iEmpleado: Integer): String;

      procedure ProcesaHistorial;
      procedure ProcesaSaldo;
      procedure SetNuevosEventos;
      procedure cdsNewRecord(DataSet: TDataSet);
      {$ifdef VER150}
      procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
      {$else}
      procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
      {$endif}
      procedure EscribeBitacora(const sMensaje: String; const iEmpleado: Integer = 0 );
      procedure EscribeError(const sMensaje: String; const iEmpleado: Integer = 0 );
      procedure ReportaProcessDetail(const iFolio: Integer);
      procedure GetSaldosVacacion( const iEmpleado: Integer; dFecha: TDate );
      procedure ValidaEnviarVaca;
      procedure ReportaErroresASCII;

    public
      { Public declarations }
        property TipoImportacion: eTipoImportacion read FTipoImportacion write FTipoImportacion;
        property Archivo : String read FArchivo write FArchivo;
        property IgualaGozo: Boolean read FIgualaGozo write FIgualaGozo;
        property SaldoPrimaEnCero: Boolean read FSaldoPrimaEnCero write FSaldoPrimaEnCero;
        property FechaReferencia: TDate read FFechaReferencia write FFechaReferencia;
        property SaldarAniversario : Boolean read FSaldarAniversario write FSaldarAniversario;
        procedure Procesa;
        procedure HandleProcessEnd(const iIndice, iValor: Integer);
    end;

var
  dmInterfase: TdmInterfase;

implementation

uses dProcesos, dCliente, dRecursos, dTablas,
     FTressShell,
     ZAsciiTools,
     ZBaseThreads,
     ZetaCommonTools,
     DSuperAscii,
     DConsultas,
     FToolsRH;

{$R *.dfm}

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
      SetNuevosEventos;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     //
end;

{$ifdef DOS_CAPAS}
function TdmInterfase.GetServerRecursos: TdmServerRecursos;
begin
     Result:= DCliente.dmCliente.ServerRecursos;
end;
{$else}
function TdmInterfase.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidor ) );
end;
{$endif}

function TdmInterfase.cdsASCII: TZetaClientDataSet;
begin
     Result := dmProcesos.cdsASCII;
end;


function TdmInterfase.cdsDataSet: TZetaClientDataSet;
begin
     Result := dmProcesos.cdsDataSet;
end;


procedure TdmInterfase.Procesa;
var
   lProcesoThread : Boolean;
begin
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     EscribeBitacora( '******* Proceso Iniciado *******' );
     ZAsciiTools.FillcdsASCII( Self.cdsASCII, FArchivo );
     FParameterList := TZetaParams.Create;
     try
        if ( not Self.cdsASCII.IsEmpty ) then
        begin
             case FTipoImportacion of
                  taHistorial : ProcesaHistorial;
                  taSaldo : ProcesaSaldo;
             end;
        end;
     finally
            FreeAndNil( FParameterList );
     end;

     lProcesoThread := FProcesandoThread;

     Repeat                               // Mientras se maneja de otra forma el HandleProcessEnd
           Application.ProcessMessages;
     Until ( not FProcesandoThread );

     if ( not FHuboCambios ) then
        EscribeBitacora( 'No se Encontraron Cambios que Procesar.' )
     else if ( not lProcesoThread ) then                             // Si no corri� el Thread de los procesos
        EscribeBitacora( '******* Proceso Terminado *******' );
end;

{ Procesa Historial }

procedure TdmInterfase.ProcesaHistorial;
const
     K_MENSAJE_VACA = 'Agreg� Vacaciones del %s al %s';

var
   iEmpleado, ErrorCount: Integer;
   dFecha, dFechaFin: TDate;


   function RegistraHistorialVacacion: Boolean;
   begin
        Result := TRUE;
        if dmCliente.SetEmpleadoNumero( iEmpleado ) then   // Posicionar al Empleado
        begin
             try
                dmRecursos.ConectarKardexLookups;
                with dmRecursos.cdsHisVacacion do
                begin
                     Refrescar;
                     Append;
                     FieldByName( 'VA_FEC_INI' ).AsDateTime := dFecha;
                     FieldByName( 'VA_FEC_FIN' ).AsDateTime := dFechaFin;
                     FieldByName( 'VA_GOZO' ).AsFloat := cdsDataSet.FieldByName('DIASGOZO').AsFloat;
                     FieldByName( 'VA_PAGO' ).AsFloat := cdsDataSet.FieldByName('DIASPAGO').AsFloat;
                     FieldByName( 'VA_P_PRIMA' ).AsFloat := cdsDataSet.FieldByName('PRIMA').AsFloat;
                     FieldByName( 'VA_COMENTA' ).AsString := cdsDataSet.FieldByName('COMENTA').AsString;
                end;
                dmRecursos.CambioVacaciones := FALSE;
                ValidaEnviarVaca;
                if dmRecursos.CambioVacaciones then
                   EscribeBitacora( Format( K_MENSAJE_VACA, [ ZetaCommonTools.FechaCorta( dFecha ),
                                            ZetaCommonTools.FechaCorta( dFechaFin ) ] ), iEmpleado );
             except
                   on Error: Exception do
                   begin
                        EscribeError( Error.Message, iEmpleado );
                   end;
             end;
        end
        else
            EscribeError( 'No se Encontr� Empleado con ese N�mero -  No se Pueden Registrar Vacaciones', iEmpleado );
   end;



begin
     ErrorCount:= 0;

     with cdsDataSet do
     begin
          Data:= ImportaAscii( Self.cdsASCII, ErrorCount, TRUE );
          if ( ErrorCount = 0 ) then
          begin
               First;
               while not EOF do
               begin
                    iEmpleado:= FieldByName('EMPLEADO').AsInteger;
                    dFecha:= FieldByName('FECHAINI').AsDateTime;
                    dFechaFin:= FieldByName('FECHAFIN').AsDateTime;
                    if ( FIgualaGozo ) and ( FieldByName('DIASPAGO').AsFloat = 0 ) then
                    begin
                         if not ( State in [dsInsert, dsEdit] ) then
                            Edit;
                         cdsDataSet.FieldByName('DIASPAGO').AsFloat:= cdsDataSet.FieldByName('DIASGOZO').AsFloat;
                    end;
                    RegistraHistorialVacacion;
                    Next;
               end;
          end
          else
          begin
               ReportaErroresASCII;
          end
     end;
end;


procedure TdmInterfase.ProcesaSaldo;
const
     K_MENSAJE_VACA = 'Agreg� Ajuste al %s';
var
   dFecha: TDate;
   iEmpleado, ErrorCount: Integer;

     procedure BorraCierresPrevios;
     begin
          with dmRecursos.cdsHisVacacion do
          begin
               First;
               while not EOF do
               begin
                    if ( FieldByName('VA_TIPO').AsInteger = 0 ) {and ( FieldByName('VA_FEC_INI').AsDateTime <= dFecha )} then
                         Delete
                    else
                        Next;
               end;
          end;
     end;

     procedure BorraMovimienPrevios;
     begin
          with dmRecursos.cdsHisVacacion do
          begin
               if ( Locate( 'VA_FEC_INI;VA_FEC_FIN;VA_TIPO', VarArrayOf( [ FechaAsStr( dFecha ), FechaAsStr( dFecha ), 1 ] ), [] ) ) then
                    Delete;
               Enviar;
          end;
     end;

     function FechaUltimoAniversario(const iEmpleado:Integer):TDate;
     var
        iYear:Integer;
        dAniv:TDate;
     begin
          dAniv := dmCliente.cdsEmpleado.FieldByName('CB_FEC_ANT').AsDateTime;
          iYear :=  TheYear( dFecha );
          Result := DateTheYear( dAniv , iYear );
          if ( Result > dFecha ) then
                 Result := DateTheYear( Result, ( iYear - 1 ) );
     end;

     procedure CalculaRegistraAjuste(const lSaldarPrimaAniv:Boolean);
     var
        TotalAjusteGozo, TotalAjustePago, TotalAjustePrima: TPesos;

     begin
          with dmRecursos.cdsHisVacacion do
          begin
               TotalAjusteGozo := 0;
               TotalAjustePago := 0;
               if not lSaldarPrimaAniv then
               begin
                    TotalAjusteGozo:= GetSaldoVacaFecha(K_VACA_GOZO_ACTUAL,iEmpleado,dFecha) - cdsDataSet.FieldByName('DIASGOZO').AsFloat;
                    TotalAjustePago:=  GetSaldoVacaFecha( K_VACA_PAGO_ACTUAL,iEmpleado,dFecha)- cdsDataSet.FieldByName('DIASPAGO').AsFloat;
               end;
               TotalAjustePrima:= GetSaldoVacaFecha( K_VACA_DPRIMA_ACTUAL,iEmpleado,dFecha ) - cdsDataSet.FieldByName('PRIMA').AsFloat;
               Append;
               FieldByName( 'VA_FEC_INI' ).AsDateTime := dFecha;
               FieldByName( 'VA_FEC_FIN' ).AsDateTime := dFecha;
               FieldByName( 'VA_GOZO' ).AsFloat := TotalAjusteGozo;
               FieldByName( 'VA_PAGO' ).AsFloat := TotalAjustePago;
               FieldByName( 'CB_TABLASS' ).AsString := FSaldosVaca[ K_VACA_TABLA_PREST ];
               FieldByName( 'CB_SALARIO' ).AsFloat := FSaldosVaca[ K_VACA_SAL_DIARIO ];
               FieldByName( 'VA_TASA_PR' ).AsFloat := FSaldosVaca[ K_VACA_PRIMA_VACA ];

               if ( SaldoPrimaEnCero or lSaldarPrimaAniv ) then
                  FieldByName( 'VA_P_PRIMA' ).AsFloat := TotalAjustePrima
               else if not SaldarAniversario then
                   FToolsRH.SetDiasPrima( FSaldosVaca, dmRecursos.cdsHisVacacion );

               FieldByName( 'VA_COMENTA' ).AsString := cdsDataSet.FieldByName('COMENTA').AsString;
          end;
          dmRecursos.CambioVacaciones := FALSE;
          ValidaEnviarVaca;
          if dmRecursos.CambioVacaciones then
             EscribeBitacora( Format( K_MENSAJE_VACA, [ ZetaCommonTools.FechaCorta( dFecha )] ), iEmpleado );
     end;

     procedure RecalculaVacaciones;
     var
        oParameterList: TZetaParams;

        function GetRango: String;
        var
           sRango: String;
        begin
             with cdsDataSet do
             begin
                  First;
                  while not EOF do
                  begin
                       sRango:= ConcatString( sRango, FieldByName('EMPLEADO').AsString, ',') ;
                       Next;
                  end;
                  Result:= GetFiltroLista( '@TABLA.CB_CODIGO', sRango );
             end;
        end;

     begin
          oParameterList := TZetaParams.Create;     //Simula el wizard de recalculo de vacaciones
          try
             with dmCliente do
             begin
                  CargaActivosIMSS( oParameterList );
                  CargaActivosPeriodo( oParameterList );
                  CargaActivosSistema( oParameterList );
             end;
             with oParameterList do
             begin
                  AddDate( 'Fecha', dmCliente.FechaDefault );
                  AddString( 'RangoLista', GetRango );
                  AddString( 'Condicion', VACIO );
                  AddString( 'Filtro', VACIO );
             end;
             ServerRecursos.RecalculaSaldosVaca ( dmCliente.Empresa, oParameterList.VarValues );
          finally
                 FreeAndNil(oParameterList);
          end;
     end;



begin
     with cdsDataSet do
     begin
          Data:= ImportaAscii( Self.cdsASCII, ErrorCount, FALSE );
          if ( ErrorCount = 0 ) then
          begin
               First;
               while not EOF do
               begin
                    iEmpleado:= FieldByName('EMPLEADO').AsInteger;

                    if ( FieldByName('FECHA').AsDateTime = 0 ) then
                    begin
                         if ( FFechaReferencia <> 0 ) then
                            dFecha:= FFechaReferencia
                         else
                             Raise Exception.Create('La fecha de referencia no puede quedar vac�a');
                    end
                    else
                        dFecha:= FieldByName('FECHA').AsDateTime;

                    if ( FIgualaGozo ) and ( FieldByName('DIASPAGO').AsFloat = 0 ) then
                    begin
                         if not ( State in [dsInsert, dsEdit] ) then
                            Edit;
                         cdsDataSet.FieldByName('DIASPAGO').AsFloat:= cdsDataSet.FieldByName('DIASGOZO').AsFloat;
                    end;



                    if dmCliente.SetEmpleadoNumero( iEmpleado ) then   // Posicionar al Empleado
                    begin
                         try

                            dmRecursos.ConectarKardexLookups;
                            with dmRecursos.cdsHisVacacion do
                            begin
                                 Refrescar;
                                 BorraCierresPrevios;
                                 BorraMovimienPrevios;
                                 GetSaldosVacacion( iEmpleado, dFecha );
                                 CalculaRegistraAjuste(False);
                                 if SaldarAniversario then
                                 begin
                                      dFecha := FechaUltimoAniversario(iEmpleado);
                                      GetSaldosVacacion( iEmpleado, dFecha );
                                      CalculaRegistraAjuste(True);
                                 end
                            end;

                         except
                               on Error: Exception do
                               begin
                                    EscribeError( Error.Message, iEmpleado );
                               end;
                         end;
                    end
                    else
                        EscribeError( 'No se Encontr� Empleado con ese N�mero -  No se Pueden Registrar Vacaciones', iEmpleado );
                    Next;
               end;
               RecalculaVacaciones;
               Application.ProcessMessages;
               FProcesandoThread:= FALSE;
          end
          else
              ReportaErroresASCII;
     end;
end;

procedure TdmInterfase.GetSaldosVacacion( const iEmpleado: Integer; dFecha: TDate );
begin
      with dmCliente do
          FSaldosVaca := ServerRecursos.GetSaldosVacacion( Empresa, iEmpleado, dFecha );

end;

function TdmInterfase.GetSaldoVacaFecha(const iValor, iEmpleado: Integer; dFecha: TDate ): TPesos;
const
     Q_SUMA_SALDO_VACA = 'select SUM( %s ) SUMA from VACACION ';
     Q_FILTRO_VACA = 'where CB_CODIGO = %d and VA_FEC_INI > %s';
var
   sSQL: String;
   rSaldo: TPesos;
begin
     rSaldo:= 0;
     sSQL:= Format( Q_FILTRO_VACA, [iEmpleado,  EntreComillas(FechaAsStr( dFecha )) ]  );
     case iValor of
          K_VACA_GOZO_ACTUAL : dmConsultas.SQLText:= Format( Q_SUMA_SALDO_VACA, ['VA_GOZO' ]) + sSQL;
          K_VACA_PAGO_ACTUAL : dmConsultas.SQLText:= Format( Q_SUMA_SALDO_VACA, ['VA_PAGO']) + sSQL;
          K_VACA_DPRIMA_ACTUAL: dmConsultas.SQLText:= Format( Q_SUMA_SALDO_VACA, ['VA_P_PRIMA' ]) + sSQL;
     end;
     with dmConsultas.cdsQueryGeneral do
     begin
          Refrescar;
          if not IsEmpty then
             rSaldo:= FieldByName('SUMA').AsFloat;
          Result:= FSaldosVaca[ iValor ] + rSaldo;
     end;
end;

function TdmInterfase.ImportaAscii(cdsDataSet: TZetaClientDataSet; var ErrorCount: Integer; lEsHistorial: Boolean ): OleVariant;
var
   oSuperASCII:TdmSuperASCII;
begin
     oSuperASCII := TdmSuperASCII.Create( Self );
     try
        with oSuperASCII do
        begin
             Formato := faASCIIDel;
             FormatoImpFecha := ifDDMMYYYYs;
             UsaCommaText := False;
             AgregaColumna( 'EMPLEADO', tgNumero, K_ANCHO_NUMEROEMPLEADO , TRUE );
             AgregaColumna( 'DIASGOZO', tgFloat, K_ANCHO_PESOS, TRUE );
             AgregaColumna( 'DIASPAGO', tgFloat, K_ANCHO_PESOS, FALSE, 0, TRUE, '0.0' );
             AgregaColumna( 'PRIMA', tgFloat, K_ANCHO_PESOS, FALSE, 0, TRUE, '0.0'  );


             if  lEsHistorial then
             begin
                  AgregaColumna( 'FECHAINI', tgFecha, K_ANCHO_FECHA, TRUE );
                  AgregaColumna( 'FECHAFIN', tgFecha, K_ANCHO_FECHA, TRUE );
                  AgregaColumna( 'COMENTA', tgTexto, K_ANCHO_DESCRIPCION, FALSE, 0, TRUE, 'IMPORTACION' );
             end
             else
             begin
                 AgregaColumna( 'FECHA', tgFecha, K_ANCHO_FECHA, FALSE );
                 AgregaColumna( 'COMENTA', tgTexto, K_ANCHO_DESCRIPCION, FALSE, 0, TRUE, 'AJUSTE' );
             end;
             Result := Procesa( Self.cdsASCII.Data );
             ErrorCount := Errores;
        end;

     finally
            oSuperASCII.Free;
     end;

end;

procedure TdmInterfase.ReportaErroresASCII;
const
     K_MENSAJE_ERROR = 'Errores en ASCII: (%d) - %s';
begin
     ZAsciiTools.FiltraASCII( cdsDataSet, TRUE );
     try
        with cdsDataSet do
        begin
             First;
             while not EOF do
             begin
                  EscribeError( Format( K_MENSAJE_ERROR, [ FieldByName( 'NUM_ERROR' ).AsInteger,
                                                           FieldByName( 'DESC_ERROR' ).AsString ] ),
                                FieldByName( 'EMPLEADO' ).AsInteger );
                  Next;
             end;
        end;
     finally
            ZAsciiTools.FiltraASCII( cdsDataSet, FALSE );
     end;
end;

procedure TdmInterfase.ValidaEnviarVaca;
begin
     try
        dmRecursos.cdsHisVacacion.Enviar;
     except
           on Error: Exception do
           begin
                EscribeError( Error.Message, dmRecursos.cdsHisVacacion.FieldByName( 'CB_CODIGO' ).AsInteger );
                dmRecursos.cdsHisVacacion.CancelUpdates;
           end;
     end;
end;

{ Manejo de Fin de Proceso - Bit�cora }

procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
   oProcessData: TProcessInfo;

   function GetResultadoProceso: String;
   begin

        case oProcessData.Status of
             epsOK: Result := '******* Proceso Terminado *******';
             epsCancelado: Result := '******* Proceso Cancelado *******';
             epsError, epsCatastrofico: Result := '****** Proceso Con Errores ******';
        else
             Result := ZetaCommonClasses.VACIO;
        end;
   end;

begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        EscribeBitacora( GetResultadoProceso );
     finally
        FreeAndNil( oProcessData );
        FProcesandoThread := FALSE;      // Mientras se encuentra la forma de manejar los threads
     end;
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha Movimiento: %s - %s';
var
   sMensaje: String;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave : EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;


function TdmInterfase.GetHeaderTexto(const iEmpleado: Integer): String;
const
     K_HEADER_EMPLEADO = 'Empleado %d : ';
begin
     Result := VACIO;
     if ( iEmpleado > 0 ) then
        Result := Format( K_HEADER_EMPLEADO, [ iEmpleado ] )
     else
        Result := FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + ' : ';
end;

procedure TdmInterfase.EscribeBitacora(const sMensaje: String; const iEmpleado: Integer);
begin
     if ( not FHuboCambios ) and ( iEmpleado > 0 ) then       // Para saber si hay que poner un mensaje de que no hubo cambios
        FHuboCambios := TRUE;
     TressShell.EscribeBitacora( GetHeaderTexto( iEmpleado ) + sMensaje );
end;

procedure TdmInterfase.EscribeError(const sMensaje: String; const iEmpleado: Integer);
begin
      TressShell.EscribeError( GetHeaderTexto( 0 ) + GetHeaderTexto( iEmpleado ) + sMensaje );
end;

procedure TdmInterfase.SetNuevosEventos;
begin
     with dmRecursos.cdsHisVacacion do
     begin
          OnReconcileError := self.cdsReconcileError;
          OnNewRecord:= self.cdsNewRecord;;
          BeforeDelete := Nil;
          AlCrearCampos:= Nil;
     end;
end;
{$ifdef VER150}

procedure TdmInterfase.cdsNewRecord(DataSet: TDataSet);
begin
     with DataSet, dmCliente do
     begin
          FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
          FieldByName( 'VA_TIPO' ).AsInteger    := Ord( tvVacaciones );
          FieldByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     EscribeError( E.Message, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger );
     Action := raCancel;
end;
{$else}
procedure TdmInterfase.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     EscribeError( E.Message, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger );
     Action := raCancel;
end;
{$endif}





end.
