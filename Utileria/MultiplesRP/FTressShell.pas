unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, ZBaseConsulta,
  FWizIMSSExportarSuaMultiples;

type
  TTressShell = class(TBaseImportaShell)
    LblFecha: TLabel;
    SistemaFechaZF: TZetaFecha;
    LblFormatoFechas: TLabel;
    cbFFecha: TZetaKeyCombo;
    rgTipoImp: TRadioGroup;
    LblTipoImportacion: TLabel;
    Label2: TLabel;
    cbFormato: TZetaKeyCombo;
    StepPeriodo: TZetaNumero;
    UpDownPer: TUpDown;
    LblStep: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    { Private declarations }
    FContador : Integer;
    WizardSUA : TWizIMSSExportarSUAMultiples;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
  protected
    { Protected declarations }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;

  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure MostrarWizard;
    procedure GenerarArchivosBitacora;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
  end;

const
     K_PARAM_TIPO  = 5;
     K_PARAM_STEP = 6;
     K_PARAM_FORMATO_FECHA = 7;
     K_PARAM_FECHA = 8;
     K_STEP_DEFAULT = 2;

var
  TressShell: TTressShell;

implementation

uses DCliente, DGlobal, DSistema, DCatalogos, DTablas,
     ZetaCommonClasses, DProcesos, ZetaCommonTools,
     ZetaClientTools, 
     ZWizardBasico;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
     inherited;
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );

     FContador := 0;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     dmTablas.Free;
     dmProcesos.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     //WizardSUA.Free;
     inherited;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;
             InitActivosIMSS;
        end;
        InitBitacora;
        SetNombreLogs;
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( 'Error al Conectar', 'Empresa: ' +
                                  dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre: String;
begin
     with dmCliente do
     begin
          sNombre := GetDatosEmpresaActiva.Nombre + '-' +
                     FechaToStr( FechaDefault ) + '-' +
                     FormatDateTime( 'hhmmss' , Now ) + '.LOG';

          ArchBitacora.Text := ZetaMessages.SetFileNameDefaultPath( 'BIT-' + sNombre );
          ArchErrores.Text := ZetaMessages.SetFileNameDefaultPath( 'ERR-' + sNombre );
     end;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
          inc( FContador );
          if FContador = WizardSUA.CuantosPatrones then
          begin
               WizardSUA.GenerarConcentrados;
          end;
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.SetBloqueo;
begin
     { No se implementa }
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell.MostrarWizard;
begin
     dmCliente.FormsMoved := TRUE;
     //ZWizardBasico.ShowWizard( TWizIMSSExportarSUAMultiples );

     WizardSUA := TWizIMSSExportarSUAMultiples.Create( Application ) as TWizardBasico;

     WizardSUA.ShowModal;
     
end;

procedure TTressShell.GenerarArchivosBitacora;
begin
     //Almacenar Bitacoras en Archivos
     EscribeBitacoras;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
    //No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     //No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     //No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     //No Implementar
end;

end.
