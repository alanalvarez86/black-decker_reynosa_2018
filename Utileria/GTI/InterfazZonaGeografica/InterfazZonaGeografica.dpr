program InterfazZonaGeografica;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}
uses
  MidasLib,
  Forms,
  ComObj,
  ActiveX,
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseImportaShell_DevEx in '..\..\..\Tools\ZBaseImportaShell_DevEx.pas' {BaseImportaShell_DevEx};

{$R *.res}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     if ( ParamCount = 0 ) then
     Application.Title := 'Interfaz Zona Geográfica - Sistema TRESS';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
