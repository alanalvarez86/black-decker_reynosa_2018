unit DInterfase;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Db, DBClient, ZetaClientDataSet, Variants, DZetaServerProvider, ZetaTipoEntidad,
    ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses, Recursos_TLB, Sistema_TLB, //Workday_TLB,
    OleServer;

type
  { Tipos de Movimientos Validos }
  TdmInterfase = class(TDataModule)
    cdsIncapacidades: TZetaClientDataSet;
    cdsUsuarios: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErrorEnviar: Boolean;
    FEmpleado, FLinea: Integer;
    FArchivo, FInfo, FMensaje: string;
    FDatos, FRenglon: TStringList;
    oZetaProvider: TdmZetaServerProvider;
    FFormatoFecha: eFormatoImpFecha;
    FClaveInterfaz: string;
    FUsuarioInterfaz: string;
    FNombreArchivo: string;
    sProceso, sCodigoProceso: String;
    FFecha: TDate;
    FServerSistema: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    function GetHeaderTexto( const iEmpleado: Integer; const iLinea: Integer ): string;
    function ValidaBajaEmpleado( const sTipo, proceso: String ): Boolean;
    function decodificaUTF (sDatos: String): WideString;
    function ObtenFecha( sFecha, sCampo, sProceso: String ): TDate;
    procedure ReportaProcessDetail(const iFolio: Integer);
    procedure SetNuevosEventos;
    procedure ProcesaArchivo;
    procedure ValidaEnviar( DataSet: TZetaClientDataSet );
    procedure RenombraArchivo( sArchivo: string );
    procedure cdsEmpleadoBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure SetNuevoKardex(const sTipo: String);
    procedure CB_SEGSOCValidate(Sender: TField);
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure ProcesaSalario;
  protected
    { Protected declarations }
  public
    { Public declarations }
    function GetUsuarioAutomatiza: Boolean;
    procedure Procesa;
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = TRUE );
    procedure EscribeErrorExterno( const sMensaje: string );
    procedure EscribeBitacora(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0 );
    property FormatoFecha : eFormatoImpFecha read FFormatoFecha write FFormatoFecha;
    property Archivo : string read FArchivo write FArchivo;
    property Informacion: string read FInfo write FInfo;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    property UsuarioInterfaz : string read FUsuarioInterfaz write FUsuarioInterfaz;
    property ClaveInterfaz : string read FClaveInterfaz write FClaveInterfaz;
  end;

var
  dmInterfase: TdmInterfase;

implementation

uses dProcesos,
     dCliente,
     dRecursos,
     dTablas,
     dCatalogos,
     dConsultas,
     dGlobal,
     dSistema,
     dNomina,
     ZGlobalTress,
     FTressShell,
     ZAsciiTools,
     ZetaAsciiFile,
     ZBaseThreads,
     ZBaseSelectGrid,
     ZetaCommonTools,
     dSuperAscii,
     StrUtils,
     ZReconcile,
     FAutoClasses,
     ZAccesosMgr,
     ZetaDialogo,
     ZAccesosTress;

{$R *.DFM}

{ TdmInterfase }
procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     FArchivo      := VACIO;
     SetNuevosEventos;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

{ Manejo de Fin de Proceso - Bit�cora }
procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
   oProcessData: TProcessInfo;
   function GetResultadoProceso: string;
   begin
        case oProcessData.Status of
             epsOK: Result := '** Proceso terminado **';
             epsCancelado: Result := '** Proceso cancelado **';
             epsError, epsCatastrofico: Result := '** Proceso con errores **';
        else
             Result := ZetaCommonClasses.VACIO;
        end;
   end;
begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( '�ndice de lista de procesos fuera de rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        EscribeBitacora( GetResultadoProceso );
     finally
            FreeAndNil( oProcessData );
            FProcesandoThread := FALSE;      // Mientras se encuentra la forma de manejar los threads
     end;
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha movimiento: %s - %s - %s';
var
   sMensaje: string;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString, FieldByName( 'BI_DATA' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave: EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger, FLinea );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

{ Utilerias de Bit�cora }
function TdmInterfase.GetHeaderTexto( const iEmpleado: Integer;  const iLinea: Integer ): string;
const
     K_HEADER_EMPLEADO = '%sEmpleado #%d: ' ;
var
   sFechaHora: String;
begin
     Result := VACIO;
     sFechaHora := '(' + FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + '): ';
     if( iLinea > 0 )then
         sFechaHora := sFechaHora + Format( 'L�nea #%d, ', [ iLinea ] );

     if ( iEmpleado > 0  ) then
        Result := Format( K_HEADER_EMPLEADO, [ sFechaHora, iEmpleado ] )
     else
         Result := sFechaHora;
end;

// Manda mensaje a la bit�cora de mensajes.
procedure TdmInterfase.EscribeBitacora(const sMensaje: string; const iEmpleado: Integer = 0;  const iLinea: Integer = 0 );
begin
     { Se cambia la bandera FHuboCambios para indicar que existen cambios en la informaci�n de los empleados }
     if ( not FHuboCambios ) and ( iEmpleado > 0 ) then
        FHuboCambios := TRUE;
     TressShell.EscribeBitacora( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     //Application.ProcessMessages
end;

// Manda mensaje a la bit�cora de errores.
procedure TdmInterfase.EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = TRUE );
begin
     if lBandera then
     begin
          { Se cambia la bandera FHuboErrores para indicar existen errores en el proceso }
          if ( not FHuboErrores ) then
             FHuboErrores := TRUE;
     end;
     TressShell.EscribeError( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     //Application.ProcessMessages
end;

// Este prodecimiento nos sirve para invocarse de unidades externas a la interfaz
// el cual a su vez env�a el mensaje al EscribeErro utilizando el cliente actual.
procedure TdmInterfase.EscribeErrorExterno( const sMensaje: string );
begin
     EscribeError( sMensaje, FEmpleado, FLinea );
end;

procedure TdmInterfase.SetNuevosEventos;
begin
     with dmCliente.cdsEmpleado do
     begin
          BeforePost := self.cdsEmpleadoBeforePost;
          AlCrearCampos := self.cdsEmpleadoAlCrearCampos;
          OnReconcileError := self.cdsEmpleadoReconcileError;
     end;
     with dmRecursos.cdsEditHisKardex do
     begin
          OnReconcileError := self.cdsReconcileError;
     end;
     with dmRecursos.cdsHisVacacion do
     begin
          OnReconcileError := self.cdsReconcileError;
     end;
end;

procedure TdmInterfase.cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if PK_Violation( E ) and
        ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
        EscribeError( 'No se pudo registrar el alta, el n�mero de empleado ya existe', FEmpleado, FLinea )
     else
        EscribeError( E.Message, FEmpleado, FLinea );
     Action := raCancel;
end;

procedure TdmInterfase.cdsEmpleadoBeforePost(DataSet: TDataSet);
begin
     dmCliente.cdsEmpleadoBeforePost( DataSet );
end;

procedure TdmInterfase.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     dmCliente.cdsEmpleadoAlCrearCampos( Sender );
     dmCliente.cdsEmpleado.FieldByName( 'CB_SEGSOC' ).onValidate := self.CB_SEGSOCValidate;
end;

procedure TdmInterfase.CB_SEGSOCValidate(Sender: TField);
var
   sMensaje: String;
begin
     if not dmCliente.ValidaSEGSOC( Sender.AsString, sMensaje, FALSE ) or strLleno( sMensaje ) then
        EscribeError( sMensaje, FEmpleado, FLinea );
end;

procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     with E do
     begin
          if PK_VIOLATION( E ) then //if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
             sError := '� C�digo Ya Existe !'
          else
              sError := Message;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     EscribeError( sError, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger, FLinea );
     Action := raCancel;
end;

{ Renombra el archivo indicado con un formato de [nombre]_[fecha].bak }
procedure TdmInterfase.RenombraArchivo( sArchivo: string );
var
   sNombre, sRuta, sFecha, sArchivoTmp: string;
const
     BAK = '.bak';
     GUION_BAJO = '_';
begin
     if FileExists( sArchivo )then
     begin
          sNombre := ExtractFileName( sArchivo );
          sNombre := Copy( sNombre, 1, ansiPos( '.', sNombre ) - 1 );
          sRuta := ExtractFilePath( sArchivo );
          sRuta := VerificaDir( sRuta );
          sRuta := sRuta + 'Backups';
          if not DirectoryExists( sRuta ) then
             CreateDir( sRuta ) ;
          sRuta := VerificaDir( sRuta );
          sFecha := FormatDateTime( 'yyyymmdd', dmCliente.FechaDefault ) + FormatDateTime( 'hhmmss', Time );
          sArchivoTmp := sNombre + GUION_BAJO + sFecha + BAK;
          try
             RenameFile( sArchivo, sRuta + sArchivoTmp );
             EscribeBitacora( Format( 'Se renombr� el archivo %s a %s', [ sNombre, sArchivoTmp ] ) );
          except
                on Error: Exception do
                   EscribeError( Format( 'Se gener� un error al tratar de renombrar el archivo %s', [ sNombre ] ) );
          end;
     end
     else
         EscribeError( Format( 'El archivo ''%s'' no existe, no se renombra archivo.', [ ExtractFileName( sArchivo ) ] ) );
end;

procedure TdmInterfase.ValidaEnviar(DataSet: TZetaClientDataSet);
begin
     FHuboErrorEnviar := TRUE;
     try
        DataSet.Enviar;
        FHuboErrorEnviar := FALSE;
     except
           on Error: Exception do
           begin
                FHuboErrorEnviar := TRUE;
                FHuboErrores := TRUE;
                EscribeError( Error.Message, FEmpleado, FLinea );
                DataSet.CancelUpdates;
           end;
     end;
end;

function TdmInterfase.ValidaBajaEmpleado( const sTipo, proceso: String ): Boolean;
begin
     with dmCliente.GetDatosEmpleadoActivo do
     begin
          Result := ( not dmRecursos.EsKardexRequiereActivo( sTipo ) ) or Activo;
          if not Result then
             EscribeError( Format( 'El empleado ya fue dado de baja el ' + FechaCorta( Baja ) + ', no se puede realizar movimiento de %s.', [ proceso ] ), FEmpleado, FLinea );
     end;
end;

// Se hizo este m�todo de forma local, ya que el mismo, dentro de dRecursos, activa la
// bandera de "AgregandoKardex:= TRUE;" posterior a "ConectarKardexLookups". Y En el lugar de la activaci�n
// de el boleano mencionado, se sutituye por "cdsHisKardex.Refrescar".
// Esto se hace para que cada vez que se agregue un movimiento, no nos impida posteriormente, cargar a
// cdsEditHisKardex los datos de cdsHisKardex. Ya que si esta bandera estuviera activada, en lugar de los
// registros de cdsHisKardex, nos trae valores vac�os, como si fuera hacer un alta.
procedure TdmInterfase.SetNuevoKardex(const sTipo: String);
begin
     with dmRecursos do
     begin
          ConectarKardexLookups; // Se ocupa para el create simple lookup de cdsEditHisKardex
          cdsHisKardex.Refrescar;
          with cdsEditHisKardex do
          begin
               Refrescar;
               Append;
               FieldByName( 'CB_TIPO' ).AsString:= sTipo;
          end;
     end;
end;

// Procesa archivo.
procedure TdmInterfase.Procesa;
begin
     dmCliente.cdsTPeriodos.Refrescar;
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;
     EscribeBitacora( 'Iniciando proceso' );
     ProcesaArchivo;
     //RenombraArchivo( FArchivo );
     Application.ProcessMessages;
     if FHuboErrores then
        EscribeBitacora( 'Proceso finalizado con errores' )
     else
     begin
          if ( FHuboCambios ) then
               EscribeBitacora( 'Proceso finalizado' )
          else
              EscribeBitacora( 'Proceso finalizado sin cambios' );
     end;
end;

// Procesa el archivo ingresado
procedure TdmInterfase.ProcesaArchivo;
var
   sRuta: string;

begin
     FFormatoFecha := ifDDMMYYYYs;
     sRuta := FArchivo;
     if FileExists( sRuta ) then
     begin
          FLinea := 0;
          FNombreArchivo := sRuta;
          EscribeBitacora( Format( 'Procesando archivo: %s', [ sRuta ] ) );
          EscribeError ( Format( 'Procesando archivo: %s', [ sRuta ] ), 0, 0, FALSE);
          FArchivo := FNombreArchivo;
          Application.ProcessMessages;
          ProcesaSalario;
          RenombraArchivo( FNombreArchivo );
          Application.ProcessMessages;
     end
     else
     begin
          EscribeBitacora( 'Archivo no existe en el directorio indicado.' );
     end;
end;

procedure TdmInterfase.ProcesaSalario;
var
   sDatos, sTabulador, sLista, sZonaGeo: String;
   lContinuar: Boolean;
   dFechaEfectiva: TDateTime;
   i: integer;
   SalNuevo,SalAnterior : Currency;

   function GetSalarioTabulador: Currency;
   begin
        with dmCatalogos.cdsClasifi do
        begin
             Conectar;
             if Locate( 'TB_CODIGO', dmCliente.cdsEmpleado.FieldByName( 'CB_CLASIFI' ).AsString, [] ) then
                Result := FieldByName( 'TB_SALARIO' ).AsFloat
             else
                 Result := dmCliente.cdsEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat;
        end;
   end;

begin
     sProceso := 'Kardex';
     if not Autorizacion.EsDemo then
     begin
          if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_CAMBIO_SALARIO )then
          begin
               FDatos := TStringList.Create;
               try
                  FDatos.LoadFromFile( FArchivo );
                  if( FDatos.Count > 0 )then
                  begin
                       FRenglon := TStringList.Create;
                       try
                          FRenglon.Delimiter := ',';
                          for i := 0 to ( FDatos.Count - 1) do
                          begin
                               Inc( FLinea );
                               FRenglon.Clear;
                               lContinuar := TRUE;
                               sDatos := FDatos[ i ];
                               sDatos := decodificaUTF( sDatos );

                               if ( sDatos <> '' ) and ( Copy( sDatos, 1,1 ) = #$FEFF) then
                                  delete( sDatos, 1, 1);

                               sDatos := StrTransAll( sDatos, ',' , '","' );
                               sDatos := ansiUpperCase( sDatos );
                               FRenglon.DelimitedText := '"' + sDatos + '"';

                               FMensaje := VACIO;
                               FEmpleado := 0;
                               if(  FRenglon.Count >= 13 ) then  //validamos los datos minimos
                               begin
                                    if StrLleno( FRenglon[ 0 ] ) then
                                    begin
                                         if Length( FRenglon[ 0 ] ) <= 9 then
                                         begin
                                              if TryStrToInt( Trim( Copy( FRenglon[ 0 ], 1, 9 ) ), FEmpleado ) then
                                              begin
                                                   if ( FEmpleado > 0 ) then
                                                   begin
                                                        if dmCliente.SetEmpleadoNumero( FEmpleado ) then
                                                        begin
                                                             if ValidaBajaEmpleado( K_T_CAMBIO, sProceso ) then
                                                             begin
                                                                  // C�digo de operaci�n
                                                                  if StrLleno( FRenglon[ 1 ] ) then
                                                                  begin
                                                                       sCodigoProceso := FRenglon[ 1 ];
                                                                       if Length ( sCodigoProceso ) = 2 then
                                                                       begin
                                                                            if( sCodigoProceso = '01' ) then
                                                                            begin
                                                                                 { Fecha de movimiento - validar incapacitado y Estatus de n�mina }
                                                                                 dFechaEfectiva := ObtenFecha( FRenglon[ 2 ], 'Kardex', sProceso );
                                                                                 if ( dFechaEfectiva <> NullDateTime ) then
                                                                                 begin
                                                                                       if not dmCliente.GetDatosEmpleadoActivo.Activo then
                                                                                       begin
                                                                                            EscribeError( Format( 'Empleado no se encuentra activo en Sistema TRESS, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                            lContinuar := False;
                                                                                       end
                                                                                       else
                                                                                       begin
                                                                                            if strLleno( FMensaje )  then
                                                                                            begin
                                                                                                 EscribeError( FMensaje, FEmpleado, FLinea );
                                                                                                 lContinuar := FALSE;
                                                                                            end;
                                                                                       end;
                                                                                 end
                                                                                 else
                                                                                 begin
                                                                                      lContinuar := FALSE;
                                                                                 end;

                                                                                 //OBTENEMOS EL NUEVO SALARIO
                                                                                 SalAnterior := dmCliente.cdsEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat;
                                                                                 SalNuevo := 0;
                                                                                 if StrLLeno( FRenglon[ 4 ] ) then
                                                                                 begin
                                                                                      SalNuevo := StrToFloatDef( Copy( FRenglon[ 4 ], 1, 10 ), 0 );
                                                                                      SalNuevo := SalNuevo / 100;
                                                                                      sTabulador := Copy( FRenglon[ 10 ], 1, 1 );
                                                                                      if strVacio( sTabulador ) then
                                                                                      begin
                                                                                           sTabulador := K_GLOBAL_NO;
                                                                                      end
                                                                                      else
                                                                                      begin
                                                                                           if ( ( sTabulador <> K_GLOBAL_NO ) and
                                                                                                ( sTabulador <> K_GLOBAL_SI ) ) then
                                                                                           begin
                                                                                                sTabulador := K_GLOBAL_NO;
                                                                                           end;
                                                                                      end;
                                                                                      sLista := VACIO;
                                                                                      if strLleno( Copy( FRenglon[ 5 ], 1, 2 ) ) then
                                                                                      begin
                                                                                           sLista := ConcatString( sLista, Copy( FRenglon[ 5 ], 1, 2 ), ',' );
                                                                                           if strlleno( Copy( FRenglon[ 6 ], 1, 2 ) ) then
                                                                                           begin
                                                                                                sLista := ConcatString( sLista, Copy( FRenglon[ 6 ], 1, 2 ), ',' );
                                                                                                if strlleno( Copy( FRenglon[ 7 ], 1, 2 ) ) then
                                                                                                begin
                                                                                                     sLista := ConcatString( sLista, Copy( FRenglon[ 7 ], 1, 2 ), ',' );
                                                                                                     if strlleno( Copy( FRenglon[ 8 ], 1, 2 ) ) then
                                                                                                     begin
                                                                                                          sLista := ConcatString( sLista, Copy( FRenglon[ 8 ], 1, 2 ), ',' );
                                                                                                          if strlleno( Copy( FRenglon[ 9 ], 1, 2 ) ) then
                                                                                                          begin
                                                                                                               sLista := ConcatString( sLista, Copy( FRenglon[ 9 ], 1, 2 ), ',' );
                                                                                                          end;
                                                                                                     end;
                                                                                                end;
                                                                                           end;
                                                                                      end;
                                                                                      if StrLleno( sLista ) then
                                                                                         dmRecursos.ListaPercepFijas := sLista
                                                                                      else
                                                                                          dmRecursos.ListaPercepFijas := VACIO;
                                                                                      if ( SalNuevo > 0 ) then
                                                                                      begin
                                                                                           if ( SalNuevo < SalAnterior ) then
                                                                                           begin
                                                                                                EscribeError( Format( 'El nuevo salario debe ser mayor o igual actual, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                                lContinuar:=FALSE;
                                                                                           end;
                                                                                      end
                                                                                      else
                                                                                      begin
                                                                                           EscribeError( Format( 'El nuevo salario debe ser mayor a cero, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                           lContinuar:=FALSE;
                                                                                      end;

                                                                                 end
                                                                                 else
                                                                                 begin
                                                                                      EscribeError( Format( 'El campo salario se encuentra vac�o, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                      lContinuar:=FALSE;
                                                                                 end;
                                                                                 sZonaGeo := Copy( FRenglon[ 12 ], 1, 1 );
                                                                                 if StrLLeno( sZonaGeo ) then
                                                                                 begin
                                                                                      if ( ( sZonaGeo = 'A' ) or ( sZonaGeo = 'B' ) or ( sZonaGeo= 'C' ) ) then
                                                                                      begin

                                                                                      end
                                                                                      else
                                                                                      begin
                                                                                           EscribeError( Format( 'Valores v�lidos para campo de zona geogr�fica son [A, B, o C], no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                           lContinuar:=FALSE;
                                                                                      end;
                                                                                 end
                                                                                 else
                                                                                 begin
                                                                                      EscribeError( Format( 'El campo zona geogr�fica se encuentra vac�o, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                      lContinuar:=FALSE;
                                                                                 end;

                                                                                 if( lContinuar ) then
                                                                                 begin
                                                                                        try
                                                                                           Self.SetNuevoKardex( K_T_CAMBIO );
                                                                                           with dmRecursos.cdsEditHisKardex do
                                                                                           begin
                                                                                                FieldByName( 'CB_FECHA' ).AsDateTime := dFechaEfectiva;
                                                                                                FieldByName( 'CB_AUTOSAL' ).AsString := sTabulador;
                                                                                                if ( sTabulador = K_GLOBAL_NO ) then
                                                                                                   FieldByName( 'CB_SALARIO' ).AsFloat := SalNuevo
                                                                                                else
                                                                                                    FieldByName( 'CB_SALARIO' ).AsFloat := GetSalarioTabulador;
                                                                                                FieldByName( 'CB_ZONA_GE' ).AsString := sZonaGeo;
                                                                                                FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_SI;
                                                                                                FieldByName( 'CB_NOTA' ).AsString := Copy( FRenglon[ 11 ], 1, 50 );
                                                                                                FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                                                                                           end;
                                                                                           ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                                                           if dmRecursos.RefrescaHisKardex then
                                                                                           begin
                                                                                                FHuboCambios := TRUE;
                                                                                                EscribeBitacora( Format( 'Se registr� movimiento de Kardex. Fecha de movimiento: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                                                           end
                                                                                           else
                                                                                           begin
                                                                                                EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                           end;
                                                                                           dmRecursos.RefrescaKardex( K_T_CAMBIO );
                                                                                        except
                                                                                             on Error : Exception do
                                                                                             begin
                                                                                                  EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                                  EscribeError( Format( 'Error al procesar cambio de Kardex: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                                                                             end;
                                                                                        end;
                                                                                 end
                                                                                 else
                                                                                 begin
                                                                                      EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                 end;
                                                                            end
                                                                            else
                                                                            begin
                                                                                 EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                                                 EscribeError( Format( 'El c�digo de operaci�n es inv�lido [%s], no se puede realizar movimiento de %s.', [ sCodigoProceso, sProceso ] ), FEmpleado, FLinea );
                                                                            end;
                                                                       end
                                                                       else
                                                                       begin
                                                                            EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                                            EscribeError( Format( 'El c�digo de operaci�n no cumple con la longitud requerida [%s], no se puede realizar movimiento de %s.', [ sCodigoProceso, sProceso ] ), FEmpleado, FLinea );
                                                                       end;
                                                                  end
                                                                  else
                                                                  begin
                                                                       EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                                       EscribeError( Format( 'El c�digo de operaci�n se encuentra vac�o, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                                  end;
                                                             end
                                                             else
                                                             begin
                                                                  EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                             end;
                                                        end
                                                        else
                                                        begin
                                                             EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                             EscribeError( Format( 'El n�mero de empleado no existe en Sistema TRESS, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                        end;
                                                   end
                                                   else
                                                   begin
                                                        EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                        EscribeError( Format( 'El n�mero de empleado debe ser mayor a cero, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                   end;
                                              end
                                              else
                                              begin
                                                   EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                   EscribeError( Format( 'El n�mero de empleado no es un n�mero, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                                   //Application.ProcessMessages;
                                              end;
                                         end
                                         else
                                         begin
                                              if not TryStrToInt( Trim( Copy( FRenglon[ 0 ], 1, 9 ) ), FEmpleado ) then
                                              begin
                                                   EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                   EscribeError( Format( 'El n�mero de empleado inv�lido, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                              end
                                              else
                                              begin
                                                   EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                                   EscribeError( Format( 'El n�mero de empleado no cumple con la longitud requerida [%s], no se puede realizar movimiento de %s.', [ FRenglon[ 0 ], sProceso ] ), FEmpleado, FLinea );
                                                   //Application.ProcessMessages;
                                              end;
                                         end;
                                    end
                                    else
                                    begin
                                         EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                         EscribeError( Format( 'El n�mero de empleado se encuentra vac�o, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                                         //Application.ProcessMessages;
                                    end;
                               end
                               else
                               begin
                                    EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
                                    EscribeError( Format( 'La cantidad de campos es menor al m�nimo esperado, no se puede realizar movimiento de %s.', [ sProceso ] ), FEmpleado, FLinea );
                               end;
                          end;
                       finally
                              FreeAndNil( FRenglon );
                       end;
                  end;
               finally
                      FreeAndNil( FDatos );
               end;
          end
          else
          begin
               EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
               EscribeError( Format( 'Usuario sin derecho en Sistema TRESS para realizar %s.', [ sProceso ] ) );
          end;
     end
     else
     begin
          EscribeBitacora( Format( 'Errores en la informaci�n a importar, no se procesar� movimiento de %s. Revisar bit�cora de errores.', [ sProceso ] ), FEmpleado, FLinea );
          EscribeError( Format( 'Empresa en modo DEMO, no se puede realizar movimiento de %s.', [ sProceso ] ) );
     end;
end;

function TdmInterfase.GetUsuarioAutomatiza:Boolean;
var
   iUsuario, iLongitud : Integer;
   lBloqueoSistema: WordBool;

begin
     Result := False;
     //Se conecta a usuarios
     cdsUsuarios.Conectar;
     cdsUsuarios.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );

     //Trae la clave 11
     dmCliente.GetSeguridad;
     iUsuario :=  dmCliente.DatosSeguridad.UsuarioTareasAutomaticas;

     if not (iUsuario = 0) then
          if cdsUsuarios.Locate( 'US_CODIGO',iUsuario, [] ) then
          begin
               //Esta configurado el usuario correctamente
               Result := True;
               FUsuarioInterfaz := cdsUsuarios.FieldByName( 'US_CORTO' ).AsString;
               FClaveInterfaz := cdsUsuarios.FieldByName( 'US_PASSWRD' ).AsString;
          end
          else
               //Esta configurado un usuario que no existe
               EscribeError( Format ( 'Usuario: %d no est� registrado en Sistema Tress', [ iUsuario ] ) )
     else
          //No esta configurado ningun usuario
          EscribeError( 'No est� configurado Usuario para Tareas Autom�ticas!!' );
end;

function TdmInterfase.decodificaUTF (sDatos: String ): WideString;
var sUTF8: WideString;
begin
     Result := sDatos;

     sUTF8 := UTF8Decode( sDatos );
     if sUTF8 <> VACIO then
        Result := sUTF8;
end;

function TdmInterfase.ObtenFecha(sFecha, sCampo, sProceso: string): TDate;
begin
     Result := 0;
     if not Strvacio( sFecha ) then
     begin
          if Length( sFecha ) = 10 then
          begin
               //sFecha := Copy( Trim( sFecha, 1, 10 );
               FFecha := GetFechaImportada( Trim( Copy( sFecha, 1, 10 ) ),  FFormatoFecha );
               if FFecha <> NullDateTime then
               begin
                    Result := FFecha;
               end
               else
               begin
                    EscribeError( Format( 'La fecha de %s es inv�lida [%s], no se puede realizar movimiento de %s.', [ sCampo, sFecha, sProceso ] ), FEmpleado, FLinea );
               end;
          end
          else
          begin
               EscribeError( Format( 'La fecha de %s no cumple con la longitud requerida o posee un formato inv�lido [%s], no se puede realizar movimiento de %s.', [ sCampo, sFecha, sProceso ] ), FEmpleado, FLinea );
          end;
     end
     else
     begin
          EscribeError( Format( 'La fecha de %s se encuentra vac�a, no se puede realizar movimiento de %s.', [ sCampo, sProceso ] ), FEmpleado, FLinea );
     end;
end;

function TdmInterfase.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;

end.

