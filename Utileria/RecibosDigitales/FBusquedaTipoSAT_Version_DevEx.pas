unit FBusquedaTipoSAT_Version_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZetaClientDataSet, DB, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZetaBusqueda_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxClasses, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TBusquedaTipoSAT_Version_DevEx = class(TBusqueda_DevEx)
    DBGrid_DevExDBTableViewColumn1: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  BusquedaTipoSAT_Version_DevEx: TBusquedaTipoSAT_Version_DevEx;

  function BusquedaSATDevEx_ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String): Boolean;
implementation

{$R *.dfm}

function BusquedaSATDevEx_ShowSearchForm( LookupDataset: TZetaLookupDataset; const sFilter: String; var sKey, sDescription: String): Boolean;
var
   oBusqueda: TBusquedaTipoSAT_Version_DevEx;
begin
//   if ( Busqueda = nil ) then
//   Se agreg� un objeto TBusqueda para manejar multiples instancias
//   de la forma de busqueda

     oBusqueda := TBusquedaTipoSAT_Version_DevEx.Create( Application );
     try
        with oBusqueda do
        begin
             Dataset := LookupDataset;
             Filtro := sFilter;
             Codigo := sKey;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  sKey := Codigo;
                  sDescription := LookupDataset.GetDescription;
             end;
        end;
     finally
            oBusqueda.Free;
     end;
end;

end.
