unit WizardAuditarConceptosTimbrado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres, ZetaClientDataSet,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, ZetaCommonLists, cxHyperLinkEdit,
  cxRadioGroup, URLMon, cxShellBrowserDialog, WinInet, WininetUtils,
  ZetaEdit, ZetaCommonClasses, DBClient,     DCatalogos, ZetaKeyCombo,
  ZetaCXGrid, TressMorado2013,ZetaKeyLookup_DevEx,cxMemo, ZetaFilesTools,
  cxButtonEdit, ActnList, UManejadorPeticiones, UPeticionWS, UThreadStack, UThreadPolling, UInterfaces,ZetaDialogAbrirArchivo,
  cxEditRepositoryItems, ZetaNumero, System.Actions, ZetaCXStateComboBox,
  cxImage, dxGDIPlusClasses, ZBaseShell, Vcl.ImgList, ZetaDespConsulta,
  cxSpinEdit,  Vcl.Styles, Vcl.Themes,ActiveX, cxExtEditRepositoryItems,
  cxCheckListBox;



type
  TWizardAuditarConceptosTimbradoForm = class(TdxWizardControlForm, ILogBitacora, INotificarStatusAuditoriaConceptosTimbrado)
    dxWizardControl1: TdxWizardControl;
    WizardAuditoriaConceptoDescarga: TdxWizardControlPage;
    PanelGrid: TPanel;
    PanelFiltro: TPanel;
    DBEdit1: TDBEdit;
    cxGroupBox3: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    Label1: TLabel;
    PE_TIMBRO: TZetaDBTextBox;
    WizardAuditoriaConceptoRazonSocial: TdxWizardControlPage;
    Label10: TLabel;
    RS_CODIGO: TZetaKeyLookup_DevEx;
    Panel3: TPanel;
    cxGroupBox1: TcxGroupBox;
    cxCheckListReportes: TcxCheckListBox;
    cxImageListReportes: TcxImageList;
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure actActualizarTablaExecute(Sender: TObject);
    procedure RS_CODIGOValidLookup(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure dxWizardControl1PageChanged(Sender: TObject);
    procedure cxCheckListReportesDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);


  private
   FContinuaDownload : boolean;
   FEjecuto : boolean;
   FEsEmpleadoTransferido : Boolean;

   FTipoRango: eTipoRangoActivo;
   FParameterList: TZetaParams;
   FDescripciones: TZetaParams;

   //Progress Bar
   FTermino      : Boolean;
   FAvanzar      : Boolean;
   FConciliadoTimbrado : Boolean;
   Estado        : string;
   Mutex         : THandle;
   FCredencialesTimbradoNomina: String;
   FProcesandoThreadConciliacion: Boolean;
   FProcesoThreadConciliacionCancelado: Boolean;
   FRespuestaTimbramex : TRespuestaStruct;
   { Private declarations }

   //ProgressBar
  protected
   { Protected declarations }
   property ParameterList: TZetaParams read FParameterList;
   property Descripciones: TZetaParams read FDescripciones;
   function Verificar: Boolean; virtual;
   function EjecutarWizard: Boolean;
   procedure InitStatusReporte;
   function GetArchivoFromURL( sURL, sDestinoPath,  sFileName: string; var sError: String): string;
  public
      FTotalRead , FTotalSize : TPesos;
      FFileNameDescarga : String;
      property TipoRango: eTipoRangoActivo read FTipoRango;
      procedure EscribirBitacoraLog( sMensaje : string );
      procedure EscribirBitacoraError( sMensaje : string );
      procedure CargaPeriodoSeleccionado;
      function ExisteEnTimbrado: boolean;
      function ObtenerTotalDiferenciasConciliadas: Integer;
      procedure SetBotonesAtrasSiguiente(bHabilitarBotones: boolean);
      function NotificarStatusAuditoriaConceptosTimbrado(sTitulo, sDescripcion, sDetalle : string; lStatusOk : integer; lEnable: Boolean = TRUE  ) : boolean;
      function GetStatusReporteItemNo2(sTitulo : string) : integer;
      function LanzaPeticionWSAuditoriaConceptos( sXML: String; var sError: String; var respuestaTimbramex : TRespuestaStruct ): Boolean;
const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_NAVEGA = 4;
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
     K_MENSAJE_DIFERENCIAS_CONCILIACION = 'Ver diferencias';
     K_MENSAJE_DETALLE_CONCILIACION = 'Ver detalles';
end;

    function DescargaEnProgreso(FileName : string; ATotalSize,  ATotalRead, AStartTime: DWORD): Boolean;

var
  WizardAuditarConceptosTimbradoForm: TWizardAuditarConceptosTimbradoForm;




function ShowWizardTimbrado : boolean;

implementation

uses
 ZetaClientTools,
 ZetaTipoEntidad,
 DTablas, Dcliente,
 DProcesos,
 DGlobal,
 ZGlobalTress,
 ZAccesosMgr,
 DBasicoCliente,
 ZetaBuscador_DevEx,
 ZConstruyeFormula,
 ZConfirmaVerificacion,
 ZetaCommonTools,
 ZetaBuscaEmpleado_DevEx,
 ZBaseSelectGrid_DevEx,
 ZBasicoSelectGrid_DevEx,
 FEmpleadoTimbradoGridSelect,
 PDFMerge,
 ZetaBuscaEmpleado_DevExAvanzada, ZetaBuscaEmpleado_DevExTimbradoAvanzada,
 FDetalleConciliacionTimbrado_DevEx;


{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

procedure TWizardAuditarConceptosTimbradoForm.SetBotonesAtrasSiguiente(bHabilitarBotones: boolean);
begin
     dxWizardControl1.Buttons.Next.Enabled := bHabilitarBotones;
     dxWizardControl1.Buttons.Back.Enabled := bHabilitarBotones;
     dxWizardControl1.Buttons.Finish.Enabled := bHabilitarBotones;
     FProcesandoThreadConciliacion := not bHabilitarBotones;
end;


procedure TWizardAuditarConceptosTimbradoForm.actActualizarTablaExecute(Sender: TObject);
begin
end;

{*** FIN ***}
procedure TWizardAuditarConceptosTimbradoForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin
     if AKind = wcbkCancel then
     begin
          if ( FProcesandoThreadConciliacion ) then
          begin
                ModalResult := mrCancel;
                if ZetaDialogo.ZConfirm( self.Caption, '�Desea cancelar el proceso de auditoria de conceptos?', 0, mbNo ) then
                begin
                     FProcesandoThreadConciliacion := FALSE;
                     FProcesoThreadConciliacionCancelado := True;
                     dxWizardControl1.Buttons.Cancel.Enabled := False;
                     Application.ProcessMessages;
                end;
          end
          else
          begin
               if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
               begin
                     if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
                     begin
                          FProcesoThreadConciliacionCancelado := True;
                          ModalResult := mrCancel;
                     end;
               end
               else
               begin
                    ModalResult := mrCancel;
               end;
          end;
     end
     else if AKind = wcbkBack then
     begin
          dxWizardControl1.Buttons.Finish.Enabled := True;
          dxWizardControl1.Buttons.Cancel.Caption := 'Cancelar';
     end
     else if AKind = wcbkFinish then
     begin
          Ahandled := True;
          dxWizardControl1.Buttons.Finish.Enabled := true;
          SetBotonesAtrasSiguiente(false);
          Application.ProcessMessages;
          if ( EjecutarWizard ) then
          begin
               if not FProcesoThreadConciliacionCancelado then
               begin
                    with TDialogAbrirArchivo_DevEx.Create( Application ) do
                    begin
                         try
                            TituloVentana := Self.Caption;
                            ArchivoUrl :=  FRespuestaTimbramex.Resultado.ArchivoUrl;
                            ArchivoNombre1 := FRespuestaTimbramex.Resultado.ArchivoNombre1;
                            ShowDialogoAbrirArchivo( True );
                         finally
                                Free;
                         end;
                    end;
                    Application.ProcessMessages;
                    dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
               end;
          end;
          SetBotonesAtrasSiguiente(true);
          Application.ProcessMessages;
          dxWizardControl1.Buttons.Finish.Enabled := false;
     end;
end;

procedure TWizardAuditarConceptosTimbradoForm.FormCreate(Sender: TObject);
begin
     HelpContext:= H32143_AuditoriaConceptoTimbrado;
     FProcesandoThreadConciliacion := False;
     FProcesoThreadConciliacionCancelado := False;
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
          end;
     end;

     //Ordenamiento de Paginas.
     WizardAuditoriaConceptoRazonSocial.PageIndex := 0;
     WizardAuditoriaConceptoDescarga.PageIndex := 1;

     //Conexion Razon Social
     dmInterfase.cdsRSocial.Conectar;
     RS_CODIGO.LookupDataset := dmInterfase.cdsRSocial;
     FAvanzar := False;
     //FIN
end;

procedure TWizardAuditarConceptosTimbradoForm.Button1Click(Sender: TObject);
begin
     SHowMessage(format('%d - %d', [ Self.Height, Self.Width ]));
end;

procedure TWizardAuditarConceptosTimbradoForm.Button3Click(Sender: TObject);
begin
     ShowMessage(format('Ancho: %d',[ Self.Width ]));
end;

procedure TWizardAuditarConceptosTimbradoForm.CargaPeriodoSeleccionado;
begin

end;

procedure TWizardAuditarConceptosTimbradoForm.cxCheckListReportesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
   ACanvas: TcxCanvas;
   AText: string;
   ATextRect: TRect;
   AGlyphWidth: Integer;
   AListBox: TcxCheckListBox;
   ACheckBmp: TBitmap;
   ACanvasFont: TFont;
   AItemState: TcxCheckBoxState;
   AItemEnabled: Boolean;
begin
     AListBox := (Control as TcxCheckListBox);
     ACanvas := AListBox.InnerCheckListBox.Canvas;
     ACanvasFont := ACanvas.Font;
     AItemState := AListBox.Items[Index].State;
     AItemEnabled := AListBox.Items[Index].Enabled;

     ACanvasFont.Color := clBlack;

     case  AListBox.Items[Index].ImageIndex  of
        K_STATUS_REPORTE_IDLE :
         begin
           ACanvasFont.Color := clBlack;
         end;
       K_STATUS_REPORTE_OK :
         begin
           //ACanvasFont.Style := [fsBold];
           ACanvasFont.Color := clBlack;
         end;
       K_STATUS_REPORTE_ERROR :
         begin
           ACanvasFont.Color := clRed;
         end;
       K_STATUS_REPORTE_HOLD :
         begin
           ACanvasFont.Color := clBlack;
         end;
       K_STATUS_REPORTE_EXECUTING :
         begin
           ACanvasFont.Color := clBlack;
         end;
     end;

     if not AItemEnabled then
       ACanvasFont.Color := clGray;

     ACanvas.Brush.Color := clWhite;
     ACanvas.FillRect(Rect);

     AGlyphWidth := 0;
     AText := AListBox.Items[Index].Text;
     ATextRect := Rect;
     ATextRect.Left := ATextRect.Left + 3 + AGlyphWidth;
     ACanvas.Brush.Color :=  ACanvasFont.Color;
     ACanvas.DrawTexT(AText, ATextRect, 0);
end;

function TWizardAuditarConceptosTimbradoForm.ExisteEnTimbrado: boolean;
begin
   Result := (dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger > 0 );
end;

function TWizardAuditarConceptosTimbradoForm.ObtenerTotalDiferenciasConciliadas: Integer;
begin
end;



procedure TWizardAuditarConceptosTimbradoForm.dxWizardControl1PageChanged( Sender: TObject );
var
   lContinuar: Boolean;
begin
//     if ( dxWizardControl1.ActivePage = WizardAuditoriaConceptoRazonSocial ) then
//     begin
//          dxWizardControl1.Buttons.Next.Caption := 'Siguiente';
//          dxWizardControl1.Buttons.Finish.Enabled := False;
//     end;
//
//     if ( dxWizardControl1.ActivePage = WizardAuditoriaConceptoDescarga ) then
//     begin
//          dxWizardControl1.Buttons.Next.Caption := 'Siguiente';
//          dxWizardControl1.Buttons.Finish.Enabled := False;
//          Application.ProcessMessages;
//     end;
end;

procedure TWizardAuditarConceptosTimbradoForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
const
    K_TRUE = 'True';
    K_QUERY_FILTRO_PERIODOS_A_CONCILIAR = 'CONCILIAR = %s';
    K_QUERY_FILTRO_PERIODOS_CONCILIADOS = '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO = %2:s)';
  var
    iContadorNominasAProcesar : integer;
    lContinuar: Boolean;
begin
     if ( AnewPage = WizardAuditoriaConceptoRazonSocial ) then
     begin
          InitStatusReporte;
          AAllow := TRUE;
     end;

     if ( AnewPage = WizardAuditoriaConceptoDescarga ) then
     begin
          AAllow := TRUE;
          FConciliadoTimbrado := False;
          if StrVacio( RS_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'El contribuyente no puede quedar vac�o.', 0  );
               AAllow := FALSE;
          end;
          if ( AAllow ) then
          begin
               if not ExisteEnTimbrado then
               begin
                     AAllow := FALSE;
                     //Mostrar mensaje de Timbrado Nomina
                     zError( Self.Caption, 'El contribuyente no est� registrado, no es posible continuar.', 0  );
               end;
          end;
     end;
end;



procedure TWizardAuditarConceptosTimbradoForm.RS_CODIGOValidLookup(Sender: TObject);
begin
     try
        dmInterfase.cdsRSocial.Locate('RS_CODIGO', RS_CODIGO.Llave, []);
     Except
     end;
end;

procedure TWizardAuditarConceptosTimbradoForm.FormDestroy(Sender: TObject);
begin
     //FDescripciones.Free;
     //FParameterList.Free;
end;

function TWizardAuditarConceptosTimbradoForm.Verificar: Boolean;
begin
end;

procedure TWizardAuditarConceptosTimbradoForm.FormShow(Sender: TObject);
begin
     inherited;
     FEsEmpleadoTransferido := false;
     //inicializacion orden pestanas
     WizardAuditoriaConceptoRazonSocial.PageIndex := 0;
     WizardAuditoriaConceptoDescarga.PageIndex := 1;
     //Obtener Valor de Mes y Anio apartir del periodo
     RS_CODIGO.SetFocus;
end;

function TWizardAuditarConceptosTimbradoForm.GetStatusReporteItemNo2(sTitulo : string) : integer;
begin
     Result := -1;

     if ( sTitulo = K_RECOLECCION_INFORMACION) then  Result := 0
     else
     if ( sTitulo = K_ENVIO_CONFIGURACION) then  Result :=  2
     else
     if ( sTitulo = K_DESCARGA_AUDITORIA) then  Result :=  4;
end;

function TWizardAuditarConceptosTimbradoForm.NotificarStatusAuditoriaConceptosTimbrado(sTitulo, sDescripcion, sDetalle: string; lStatusOk: integer; lEnable: Boolean): boolean;
var
     itemNo2 : integer;
begin
     itemNo2 := GetStatusReporteItemNo2(sTitulo);
     if ( itemNo2 >=0 ) and ( itemNo2+1 < cxCheckListReportes.Items.Count ) then
     begin
           cxCheckListReportes.Items[itemNo2].Text := sTitulo;
           if StrVacio( sDescripcion ) then
              cxCheckListReportes.Items[itemNo2+1].Text := 'Estatus: '+ sDetalle
           else
               cxCheckListReportes.Items[itemNo2+1].Text := 'Estatus: '+ sDescripcion + ' ' + sDetalle;
           cxCheckListReportes.Items[itemNo2].Enabled := lEnable;
           cxCheckListReportes.Items[itemNo2+1].Enabled := lEnable;
           if (lEnable) then
           begin
               cxCheckListReportes.Items[itemNo2].ImageIndex := lStatusOk;
           end
           else
           begin
                cxCheckListReportes.Items[itemNo2].Text := sTitulo;
                cxCheckListReportes.Items[itemNo2+1].Text := VACIO;
                cxCheckListReportes.Items[itemNo2].ImageIndex := 0;
                cxCheckListReportes.Items[itemNo2].State := TcxCheckBoxState.cbsGrayed;
           end;
     end;


     Application.ProcessMessages;
end;

procedure TWizardAuditarConceptosTimbradoForm.EscribirBitacoraError(
  sMensaje: string);
begin
//
end;

procedure TWizardAuditarConceptosTimbradoForm.EscribirBitacoraLog(sMensaje: string);
begin
  //
end;

function TWizardAuditarConceptosTimbradoForm.LanzaPeticionWSAuditoriaConceptos( sXML: String; var sError: String; var respuestaTimbramex : TRespuestaStruct ): Boolean;
var
   oPeticionWS: TPeticionWS;
   timbresSoap : TimbresFiscalesSoap;
   sRespuesta:  string;
   broker : TManejadorPeticiones;

   procedure EsperarPeticion;
   var
      iCount : integer;
   begin
       iCount := 0;
       while oPeticionWS.lProceso do
       begin
         Inc( iCount );
         if ( iCount mod 4  = 0 ) then
            Application.ProcessMessages
         else
             DelayNPM( 200  );
       end;
       Application.ProcessMessages;
   end;
begin
     try
        Result := TRUE;
        sError := VACIO;

         if ( GetTimbradoServer <> '' ) then
         begin
              Application.ProcessMessages;
              timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
              if ( timbresSoap <> nil ) then
              begin
                   try
                      oPeticionWS := TPeticionWS.Create;
                      broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );
                      oPeticionWS.tipoPeticion := peticionAuditoriaConceptosTimbrado;
                      oPeticionWs.cnxSoap := timbresSoap;
                      oPeticionWS.sPeticion :=   dmInterfase.GetPeticionAuditoriaConceptoNube(sXML, sError);
                      oPeticionWS.sURL := GetTimbradoURL;
                      oPeticionWS.lProceso := TRUE;
                      broker.AgregarPeticionWS( oPeticionWS );
                      EsperarPeticion;
                      sRespuesta := oPeticionWS.sRespuesta;
                      if not StrVacio( sRespuesta ) then
                      begin
                           respuestaTimbramex := FTimbramexHelper.GetRecibosRespuesta(sRespuesta );
                           Application.ProcessMessages;

                           if respuestaTimbramex.Resultado.Errores then
                           begin
                                sError := respuestaTimbramex.Bitacora.Eventos.Text;
                                Result := False;
                           end;

                           if not respuestaTimbramex.Resultado.ErroresDS.IsEmpty then
                           begin
                                with respuestaTimbramex.Resultado.ErroresDS do
                                begin
                                     First;
                                     while not EOF do
                                     begin
                                          sError := FieldByName('DETALLE').AsString;
                                          if StrVacio( sError ) then
                                             sError := FieldByName('DESCRIPCION').AsString;
                                          Break;
                                     end;
                                end;
                                Result := False;
                           end;
                      end
                      else
                      begin
                           sError := TIMBRADO_SIN_CONECTIVIDAD;
                           Result := False;
                      end;
                   finally
                          FreeAndNil( oPeticionWs );
                          FreeAndNil( broker );
                   end;
              end
              else
              begin
                   Result := FALSE;
                   sError := TIMBRADO_SIN_CONECTIVIDAD + ' No hay servidor especificado';
              end;
         end
         else
         begin
              Result := FALSE;
              sError := TIMBRADO_SIN_CONECTIVIDAD + ' No hay servidor especificado';
         end;
     except on Error: Exception do
            begin
                 sError := Error.Message;
                 Result := FALSE;
            end;
     end;
end;

function TWizardAuditarConceptosTimbradoForm.EjecutarWizard: Boolean;
var
   sXML : String;
   sError : String;
   sArchivoListo: string;
   aDir: array[0..1024] of char;
   sRuta: String;
   function ProcesarDescarga( var sError: String; respuestaTimbramex : TRespuestaStruct ): Boolean;
   begin
        Result := FALSE;
        try
           with respuestaTimbramex.Resultado do
           begin
               if StrLleno( ArchivoUrl ) then
               begin
                    GetTempPath(1024, aDir);
                    sRuta := aDir;
                    sArchivoListo := GetArchivoFromURL(ArchivoUrl,  sRuta,  ArchivoNombre1, sError );
                    if not StrVacio( sArchivoListo ) then
                    begin
                         Result := TRUE;
                         FRespuestaTimbramex.Resultado.ArchivoUrl := sRuta;
                         FRespuestaTimbramex.Resultado.ArchivoNombre1 := sArchivoListo;
                    end;
               end;
           end;
        except on Error: Exception do
            begin
                 sError := Error.Message;
                 Result := FALSE;
            end;
        end;
   end;
begin
     Result := FALSE;
     sXML := VACIO;
     sError := VACIO;
     // 1.-Recoleccion de Informacion de sistema TRESS
     NotificarStatusAuditoriaConceptosTimbrado(K_RECOLECCION_INFORMACION, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
     if ( dmInterfase.ObtieneInformacionConceptosTimbrado(sXML, sError) and FProcesandoThreadConciliacion ) then
     begin
          NotificarStatusAuditoriaConceptosTimbrado(K_RECOLECCION_INFORMACION, 'Termin�', '' , K_STATUS_REPORTE_OK  );
          Result := TRUE;
          // 2.-Envio Configuracion de concepto administrador de TRESS
          NotificarStatusAuditoriaConceptosTimbrado(K_ENVIO_CONFIGURACION, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
          if ( LanzaPeticionWSAuditoriaConceptos( sXML, sError, FRespuestaTimbramex ) and FProcesandoThreadConciliacion ) then
          begin
               NotificarStatusAuditoriaConceptosTimbrado(K_ENVIO_CONFIGURACION, 'Termin�', '' , K_STATUS_REPORTE_OK  );
               Result := TRUE;
               // 3.-Descarga de auditoria
               NotificarStatusAuditoriaConceptosTimbrado(K_DESCARGA_AUDITORIA, 'Procesando...', '' , K_STATUS_REPORTE_EXECUTING  );
               if ( ProcesarDescarga( sError, FRespuestaTimbramex ) and FProcesandoThreadConciliacion ) then
               begin
                    NotificarStatusAuditoriaConceptosTimbrado(K_DESCARGA_AUDITORIA, 'Termin�', '' , K_STATUS_REPORTE_OK  );
               end
               else
               begin
                    NotificarStatusAuditoriaConceptosTimbrado(K_DESCARGA_AUDITORIA, '', sError , K_STATUS_REPORTE_ERROR  );
                    Result := FALSE;
               end;
          end
          else
          begin
               NotificarStatusAuditoriaConceptosTimbrado(K_ENVIO_CONFIGURACION, '', sError , K_STATUS_REPORTE_ERROR  );
               Result := FALSE;
          end;
     end
     else
     begin
          NotificarStatusAuditoriaConceptosTimbrado(K_RECOLECCION_INFORMACION, '', sError , K_STATUS_REPORTE_ERROR  );
          Result := FALSE;
     end;
end;

procedure TWizardAuditarConceptosTimbradoForm.InitStatusReporte;
begin
     NotificarStatusAuditoriaConceptosTimbrado( K_RECOLECCION_INFORMACION, '', '', 6, True);
     NotificarStatusAuditoriaConceptosTimbrado( K_ENVIO_CONFIGURACION, '', '', 6, TRUE);
     NotificarStatusAuditoriaConceptosTimbrado( K_DESCARGA_AUDITORIA, '', '', 6, TRUE);
end;


function TWizardAuditarConceptosTimbradoForm.GetArchivoFromURL( sURL, sDestinoPath,  sFileName: string; var sError: String ): string;
var
  SourceFile, LocalFile, sFileNamePath: string;
  func: TProgressCallback;
  brokerDownload : TManejadorPeticiones;
  oPeticionDownload : TPeticionWS;


    function GetTempFileDownload( const sPrefijo, sExt : string ): string;
    {$ifndef DOTNET}
    var
       aDir,aArchivo : array[0..1024] of char;
    {$endif}
    begin
         {$ifdef DOTNET}
         Result := '';
         {$else}
         GetTempPath(1024, aDir);
         GetTempFileName( aDir,PChar(sPrefijo), 0,aArchivo );
         Result := PChar(ChangeFileExt( aArchivo, '.'+ sExt ));
         DeleteFile( aArchivo );
         {$endif}
    end;


    function UntilNotExistsFileName : string;
    var
       iCount : integer;
       sDirPath,sDirPathEmpleadoNombre, sExtension  : string;
       sYear, sCodigoEmpleado, sNombreEmpleado : string;
       sNombre, sApellido : string;

       procedure crearDirectorio (sPath : string);
       begin
            if not DirectoryExists( sPath ) then
               CreateDir( sPath );
       end;
    begin
         iCount := 1;
         crearDirectorio(sDestinoPath);
         sDirPath := sDestinoPath;
         repeat
               //sFileNamePath := ExtractFilePath( sDirPath + '\' + sFileName );
               sFileNamePath := ExtractFilePath( sDirPath + sFileName );
               if (icount = 1 ) then
                sFileNamePath := sFileNamePath  +  sFileName
               else
                sFileNamePath := sFileNamePath  + Format('(%d) %s', [iCount, sFileName]);
                Inc(icount);
         until not FileExists( sFileNamePath );

         Result := sFileNamePath;
    end;

    procedure EsperarDescarga;
    var
        iCount : integer;
    begin
        iCount := 0;
        FTotalRead := 0;
        FTotalSize := 0;
        FFileNameDescarga := VACIO;

        while oPeticionDownload.lProceso do
        begin
          {if StrLleno( FFileNameDescarga ) then
             SetStatus( Format('Descargando %s (%.2f Mb de %.2f Mb )', [FFileNameDescarga, fTotalRead, fTotalSize ] ) );} //COMENTADO AHORITA

          Inc( iCount );
          //iShowBuzz := iCount mod 4;
          //cxProgressBarEnvio.Position := iShowBuzz * 20;  cxProgressBarEnvio.Update;

          if ( iCount mod 4  = 0 ) then
             Application.ProcessMessages
          else
             DelayNPM( 200 );
         end;
        Application.ProcessMessages;
    end;

    function  DescargarArchivo : boolean;
    begin
         Result := FALSE;
         try
            oPeticionDownload := TPeticionWS.Create;
            oPeticionDownload.tipoPeticion := peticionDownloadFile;
            oPeticionDownload.sArchivoDestino := sFileName;
            oPeticionDownload.sArchivoOrigen :=  SourceFile;
            oPeticionDownload.sLocalFile := LocalFile;
            oPeticionDownload.ACallback :=  DescargaEnProgreso;
            oPeticionDownload.lProceso := TRUE;
            brokerDownload.AgregarPeticionWS( oPeticionDownload );
            EsperarDescarga;
            Result := oPeticionDownload.lResultadoDownload;
         finally
               ///SetStatus( VACIO );
         end;
    end;
begin
     SourceFile := sURL;
     LocalFile := GetTempFileDownload('REC', 'zip');
     Result := VACIO;
     try
        try
           brokerDownload := TManejadorPeticiones.Create( ILogBitacora( Self ) );
           FContinuaDownload := TRUE;
           sFileNamePath := UntilNotExistsFileName;
           if DescargarArchivo then
           begin
             RenameFile(LocalFile, sFileNamePath);
             Result := sFileNamePath;
           end
           else
              sError := Format('Error al descargar archivo del sistema de timbrado: %s', [sFileName] );
        except on Error: Exception do
               begin
                    sError := Format('Error al descargar archivo del sistema de timbrado: %s', [sFileName] );
               end;
        end;
     finally
            FreeAndNil( oPeticionDownload );
            FreeAndNil( brokerDownload );
     end;
end;

function DescargaEnProgreso(FileName : string; ATotalSize,  ATotalRead, AStartTime: DWORD): Boolean;
begin
     Result := True;
end;

end.


