unit MensajesTimbradoWebServices;

interface

uses Windows,SysUtils,Classes,Timbres,FTimbramexHelper,ZetaCommonClasses,
     RespuestaXML,ExtCtrls,Dialogs;

type
    eTipoImagenMensaje = (rInformativo,rAdvertencia,rError, rErrorGrave);
    eTipoBotonDialogo  = (btnOK, btnCancel);
    //Se utilizara para iniciarlizar los parametros de la peticion Timbrado
    TParametrosPeticion = record
      CT_CODIGO: String;
      RS_CONTID: Integer;
    end;

    TInfoBotonOKDialogo = record
      Label_: UnicodeString;
      Caption: UnicodeString;
      Tipo: ShortInt;
      URL: UnicodeString;
    end;

    TInfoBotonCLOSEDialogo = record
      Label_: UnicodeString;
      Caption: UnicodeString;
      Tipo: ShortInt;
    end;

    TMensajesTimbradoWebServices = class
    private
      { private declarations here }
      FTimbreSoap: TimbresFiscalesSoap;
      FParametros: TParametrosPeticion;
      FInfoBotonOKDialogo: TInfoBotonOKDialogo;
      FInfoBotonCLOSEDialogo: TInfoBotonCLOSEDialogo;
      FPeticion: String;
      FRespuestaHTML: String;
      FRespuestaUI: String;
      FLinkTemp: String;
      FImagenTemp: String;
      FErrores: String;
      FPlantillaHTML: TStringList;
      FImagenTipoMensaje: TImage;
      FTipoError: Integer;
      FTituloDialogo: String;
      FBotonesDialogoUI: TMsgDlgButtons;
      function GetTempFileDownload( const sPrefijo, sExt : string ): string;
      function GrabarTexto( const sArchivo,  sTexto : string ) : boolean;
      function GetLinkTemp: String;
      procedure SetLinkTemp(const Value: String);
      procedure ObtenerPlantillaHTML( sImagen: String; sMensajeHTML: String );
      procedure ObtenerPlantillaHTMLDialogo( sImagen: String; sMensajeHTML: String );
      function GetImagenTemp: String;
      procedure SetImagenTemp(const Value: String);
      function GetPlantillaHTML: TStringList;
      procedure SetPlantillaHTML(const Value: TStringList);
      function GetTipoError: Integer;
      procedure SetTipoError(const Value: Integer);
      function GetTituloDialogo: String;
      procedure SetTituloDialogo(const Value: String);
      function GetBotonesDialogoUI: TMsgDlgButtons;
      function GetInfoBotonCLOSEDialogo: TInfoBotonCLOSEDialogo;
      function GetInfoBotonOKDialogo: TInfoBotonOKDialogo;
    protected
      { protected declarations here }
    public
      { public declarations here }
      constructor Create( oParametros: TParametrosPeticion );
      destructor Destroy;
      function SolicitarPeticion(var sErrores: String): Boolean;
      function GetParametros: TParametrosPeticion;
      function GetPeticion: String;
      procedure SetPeticion(Value: String);
      function GetRespuestaHTMl: String;
      procedure SetRespuestaHTML(Value: String);
      function GetRespuestaUI: String;
      procedure SetRespuestaUI(Value: String);
      procedure SetBotonesDialogoUI(oBotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK; oBotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE);
      procedure GenerarInfoBotonesDialogoUI(oBotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK; oBotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE);
      procedure FormarPeticionCartaTimbrado;
      function FormarHTMLMensaje(const iVerboID: Integer): Boolean;
      function FormarHTMLMensajeDialogo(const iVerboID: Integer): Boolean;
      function GetHTML( const sXML: String; const iVerboID: Integer ): String;
      procedure SetImagenTipoError;
      function GetTipoDialogoUI: TMsgDlgType;
      property TimbreSoap: TimbresFiscalesSoap read FTimbreSoap;
      property Parametros: TParametrosPeticion read GetParametros write FParametros;
      property Peticion: String read GetPeticion write SetPeticion;
      property RespuestaHTML: String read GetRespuestaHTML write SetRespuestaHTML;
      property RespuestaUI: String read GetRespuestaUI write SetRespuestaUI;
      property LinkTemp: String read GetLinkTemp write SetLinkTemp;
      property ImagenTemp: String read GetImagenTemp write SetImagenTemp;
      property PlantillaHTML: TStringList read GetPlantillaHTML write SetPlantillaHTML;
      property TipoError: Integer read GetTipoError write SetTipoError;
      property TituloDialogo: String read GetTituloDialogo write SetTituloDialogo;
      property TipoDialogoUI: TMsgDlgType read GetTipoDialogoUI;
      property BotonesDialogoUI: TMsgDlgButtons read GetBotonesDialogoUI write FBotonesDialogoUI;
      property InfoBotonOKDialogo: TInfoBotonOKDialogo read GetInfoBotonOKDialogo write FInfoBotonOKDialogo;
      property InfoBotonCLOSEDialogo: TInfoBotonCLOSEDialogo read GetInfoBotonCLOSEDialogo write FInfoBotonCLOSEDialogo;
    end;

implementation

const
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
     K_ERR_TIMBRAMEX = 'Notificación del Sistema de Timbrado ';

constructor TMensajesTimbradoWebServices.Create( oParametros: TParametrosPeticion );
begin
     FTimbreSoap := nil;
     FPeticion := VACIO;
     FRespuestaHTML := VACIO;
     FRespuestaUI := VACIO;
     FParametros := oParametros;
     FTimbreSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
     FLinkTemp := GetTempFileDownload( 'MENSAJESTIMBRADO', 'html');
     FImagenTemp := GetTempFileDownload( 'IMAGENNOTIFICACION', 'bmp');
     FPlantillaHTML := TStringList.Create;
     FImagenTipoMensaje := TImage.Create(nil);
     FTituloDialogo := 'Notificación del Sistema de Timbrado ';
end;

destructor TMensajesTimbradoWebServices.Destroy;
begin
     FreeAndNil( FTimbreSoap );
     FreeAndNil(FPlantillaHTML);
     FreeAndNil(FImagenTipoMensaje);
end;

function TMensajesTimbradoWebServices.GetParametros: TParametrosPeticion;
begin
     Result := FParametros;
end;

function TMensajesTimbradoWebServices.GetPeticion: String;
begin
     Result := FPeticion;
end;

function TMensajesTimbradoWebServices.GetPlantillaHTML: TStringList;
begin
     Result := FPlantillaHTML;
end;

function TMensajesTimbradoWebServices.GetRespuestaHTMl: String;
begin
     Result := FRespuestaHTML;
end;

function TMensajesTimbradoWebServices.GetRespuestaUI: String;
begin
     Result := FRespuestaUI;
end;

procedure TMensajesTimbradoWebServices.FormarPeticionCartaTimbrado;
var
   sPeticion: String;
begin
     sPeticion := '<?xml version="1.0" encoding="iso-8859-1" ?>';
     sPeticion := sPeticion + '<PROCESS> ';
     sPeticion := sPeticion + GetCredenciales( GetParametros.CT_CODIGO, GetParametros.RS_CONTID ) ;
     sPeticion := sPeticion + '<DATA></DATA>';
     sPeticion := sPeticion + '</PROCESS> ';
     SetPeticion( sPeticion );
end;

function TMensajesTimbradoWebServices.SolicitarPeticion(var sErrores: String): Boolean;
var
   sServer: String;
   sPeticion,sRespuesta: String;
   crypt : TWSTimbradoCrypt;
begin
     try
        Result := False;
        sServer := GetTimbradoServer;
        if ( sServer <> '' ) then
        begin
           if ( FtimbreSoap <> nil ) then
           begin
                sPeticion :=  GetPeticion;
				crypt := TWSTimbradoCrypt.Create;                				 
				sRespuesta := crypt.Desencriptar(  FtimbreSoap.ContribuyenteValidar( crypt.Encriptar( sPeticion ) ) );
                //sRespuesta := FtimbreSoap.ContribuyenteValidar( sPeticion );
                SetRespuestaHTML( sRespuesta );
                Result := True;
				FreeAndNil( crypt ); 
           end;
        end
        else
        begin
             sErrores := K_SIN_CONECTIVIDAD + ' No hay servidor ';
             Result := False;
        end;
     except on Error: Exception do
            begin
                 sErrores :=   K_SIN_CONECTIVIDAD + Error.Message;
                 Result := False;
            end;
     end;
end;

{***
    Tipo:
         0 - No mostrar
         1- Cerrar Ventanta
         2- Abrir Liga
***}
procedure TMensajesTimbradoWebServices.SetBotonesDialogoUI(
          oBotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK;
          oBotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE);
begin
     if ( oBotonDialogoOK.Tipo <> 0 ) and ( oBotonDialogoCLOSE.Tipo <> 0 ) then
        FBotonesDialogoUI := [ mbOk, mbCancel ]
     else if ( oBotonDialogoOK.Tipo <> 0 ) and ( oBotonDialogoCLOSE.Tipo = 0 ) then
          FBotonesDialogoUI := [ mbOK ]
     else if ( oBotonDialogoOK.Tipo = 0 ) and ( oBotonDialogoCLOSE.Tipo <> 0 ) then
          FBotonesDialogoUI := [ mbCancel ];
end;

procedure TMensajesTimbradoWebServices.SetImagenTemp(const Value: String);
begin
     FImagenTemp := Value;
end;

procedure TMensajesTimbradoWebServices.SetLinkTemp(const Value: String);
begin
     FLinkTemp := Value;
end;

procedure TMensajesTimbradoWebServices.SetPeticion(Value: String);
begin
     FPeticion := Value;
end;

procedure TMensajesTimbradoWebServices.SetPlantillaHTML(const Value: TStringList);
begin
     FPlantillaHTML := Value;
end;

procedure TMensajesTimbradoWebServices.SetRespuestaHTML(Value: String);
begin
     FRespuestaHTML := Value;
end;

procedure TMensajesTimbradoWebServices.SetRespuestaUI(Value: String);
begin
     FRespuestaUI := Value;
end;


procedure TMensajesTimbradoWebServices.SetTipoError(const Value: Integer);
begin
     FTipoError := Value;
end;

procedure TMensajesTimbradoWebServices.SetTituloDialogo(const Value: String);
begin
     FTituloDialogo := Value;
end;

/////HTML
function TMensajesTimbradoWebServices.GetTempFileDownload( const sPrefijo, sExt : string ): string;
{$ifndef DOTNET}
var
   aDir,aArchivo : array[0..1024] of char;
{$endif}
begin
     {$ifdef DOTNET}
     Result := '';
     {$else}
     GetTempPath(1024, aDir);
     GetTempFileName( aDir,PChar(sPrefijo), 0,aArchivo );
     Result := PChar(ChangeFileExt( aArchivo, '.'+ sExt ));
     DeleteFile( aArchivo );
     {$endif}
end;

function TMensajesTimbradoWebServices.GetTipoError: Integer;
begin
     Result := FTipoError;
end;

function TMensajesTimbradoWebServices.GetTituloDialogo: String;
begin
     Result := FTituloDialogo;
end;

function TMensajesTimbradoWebServices.GrabarTexto( const sArchivo,  sTexto : string ) : boolean;
var
   sl : TStringList;
begin
     sl := TStringList.Create;
     sl.Text := sTexto;
     sl.SaveToFile( sArchivo );
     FreeAndNil(sl);
end;

procedure TMensajesTimbradoWebServices.GenerarInfoBotonesDialogoUI(
          oBotonDialogoOK: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoOK;
          oBotonDialogoCLOSE: IXMLRESPUESTA_SALIDA_MENSAJE_BotonDialogoCLOSE);
begin
     FInfoBotonOKDialogo.Label_ := oBotonDialogoOK.Label_;
     FInfoBotonOKDialogo.Caption := oBotonDialogoOK.Caption;
     FInfoBotonOKDialogo.Tipo := oBotonDialogoOK.Tipo;
     FInfoBotonOKDialogo.URL := oBotonDialogoOK.URL;

     FInfoBotonCLOSEDialogo.Label_ := oBotonDialogoCLOSE.Label_;
     FInfoBotonCLOSEDialogo.Caption := oBotonDialogoCLOSE.Caption;
     FInfoBotonCLOSEDialogo.Tipo := oBotonDialogoCLOSE.Tipo;
end;

function TMensajesTimbradoWebServices.GetBotonesDialogoUI: TMsgDlgButtons;
begin
     Result := FBotonesDialogoUI;
end;

function TMensajesTimbradoWebServices.GetHTML( const sXML: String; const iVerboID: Integer ): String;
var
   sMensajeHTML : string;
   xRespuesta : IXMLRESPUESTA;
   xRespuestaVerboID: IXMLRESPUESTA_SALIDA_MENSAJE;
begin
     xRespuesta := RespuestaXML.LoadRESPUESTA(sXML);
     sMensajeHTML := VACIO;
     if HayErrorBitacora(xRespuesta ) then
     begin
          SMensajeHTML := GetErroresBitacora(xRespuesta);
          if SMensajeHTML <> VACIO then
          begin
               SetTipoError(Ord(eTipoImagenMensaje(rError)));
               SetImagenTipoError;
               FBotonesDialogoUI := [mbCancel];
          end;
     end
     else
     begin
          xRespuestaVerboID := GetMENSAJEVERBOID(xRespuesta, iVerboID);
          if xRespuestaVerboID <> nil then
          begin
               SetTituloDialogo( xRespuestaVerboID.Titulo );
               SetTipoError( Ord(eTipoImagenMensaje(xRespuestaVerboID.Tipo)));
               SetImagenTipoError;
               sMensajeHTML := xRespuestaVerboID.HTML;
               SetBotonesDialogoUI( xRespuestaVerboID.BotonDialogoOK, xRespuestaVerboID.BotonDialogoCLOSE);
               GenerarInfoBotonesDialogoUI(xRespuestaVerboID.BotonDialogoOK, xRespuestaVerboID.BotonDialogoCLOSE);
          end;
     end;
     Result := sMensajeHTML;
end;

function TMensajesTimbradoWebServices.GetImagenTemp: String;
begin
     Result := FImagenTemp;
end;

function TMensajesTimbradoWebServices.GetInfoBotonCLOSEDialogo: TInfoBotonCLOSEDialogo;
begin
     Result := FInfoBotonCLOSEDialogo;
end;

function TMensajesTimbradoWebServices.GetInfoBotonOKDialogo: TInfoBotonOKDialogo;
begin
     Result := FInfoBotonOKDialogo;
end;

function TMensajesTimbradoWebServices.GetLinkTemp: String;
begin
     Result := FLinkTemp;
end;

/////FIN

procedure TMensajesTimbradoWebServices.ObtenerPlantillaHTML( sImagen: String; sMensajeHTML: String );
var
   sPlantillaHTML: String;
   RSource:Tresourcestream;
begin
     try
        RSource := TResourceStream.Create(HInstance,'FirmadoContratoPlantilla', RT_RCDATA);
        GetPlantillaHTML.LoadFromStream(RSource);
        sPlantillaHTML := GetPlantillaHTML.Text;
        sPlantillaHTML := StringReplace( sPlantillaHTML, '##IMAGEN', sImagen , [rfReplaceAll] );
        sPlantillaHTML := StringReplace( sPlantillaHTML, '##MENSAJEHTML', sMensajeHTML, [rfReplaceAll] );
        GetPlantillaHTML.Text := sPlantillaHTML;
     except
          on Error: Exception do
          begin
               //ErrorLog( 'Se encontró un Error ' + CR_LF + Error.Message );
          end;
     end;
end;

procedure TMensajesTimbradoWebServices.ObtenerPlantillaHTMLDialogo( sImagen: String; sMensajeHTML: String );
var
   sPlantillaHTML: String;
   RSource:Tresourcestream;
begin
     try
        RSource := TResourceStream.Create(HInstance,'FirmadoContratoPlantillaDialogo', RT_RCDATA);
        GetPlantillaHTML.LoadFromStream(RSource);
        sPlantillaHTML := GetPlantillaHTML.Text;
        sPlantillaHTML := StringReplace( sPlantillaHTML, '##MENSAJEHTML', sMensajeHTML, [rfReplaceAll] );
        GetPlantillaHTML.Text := sPlantillaHTML;
     except
          on Error: Exception do
          begin
               //ErrorLog( 'Se encontró un Error ' + CR_LF + Error.Message );
          end;
     end;
end;

{***
    Debe colocar el icono segun Tipo
         0: Informativo:  i ,
         1: Advertencia:  Icono de ! ,
         2: Error: Icono de X
         3: Error Grave.: Icono de X
***}
procedure TMensajesTimbradoWebServices.SetImagenTipoError;
var
   TipoImagenMensaje : eTipoImagenMensaje;
begin
     TipoImagenMensaje := eTipoImagenMensaje(GetTipoError);
     case TipoImagenMensaje of
          rInformativo :
          begin
               FImagenTipoMensaje.Picture.Bitmap.LoadFromResourceID(hInstance, Ord(3));
          end;
          rAdvertencia :
          begin
               FImagenTipoMensaje.Picture.Bitmap.LoadFromResourceID(hInstance, Ord(1));
          end;
          rError :
          begin
               FImagenTipoMensaje.Picture.Bitmap.LoadFromResourceID(hInstance, Ord(2));
          end;
          rErrorGrave :
          begin
               FImagenTipoMensaje.Picture.Bitmap.LoadFromResourceID(hInstance, Ord(2));
          end;
     end;
     FImagenTipoMensaje.Picture.Bitmap.SaveToFile(GetImagenTemp);
end;


function TMensajesTimbradoWebServices.FormarHTMLMensaje( const iVerboID: Integer ) : Boolean;
var
   sHTML: String;
begin
     Result := False;
     sHTML := GetHTML( GetRespuestaHTMl, iVerboID);
     if sHTML <> VACIO then
     begin
          SetImagenTipoError;
          ObtenerPlantillaHTML( GetImagenTemp, sHTML );
          sHTML := GetPlantillaHTML.Text;
          GrabarTexto( GetLinkTemp, sHTML);
          Result := True;
     end;
end;

function TMensajesTimbradoWebServices.FormarHTMLMensajeDialogo( const iVerboID: Integer ) : Boolean;
var
   sHTML: String;
begin
     Result := False;
     sHTML := GetHTML( GetRespuestaHTMl, iVerboID);
     if sHTML <> VACIO then
     begin
          ObtenerPlantillaHTMLDialogo( GetImagenTemp, sHTML );
          sHTML := GetPlantillaHTML.Text;
          GrabarTexto( GetLinkTemp, sHTML);
          Result := True;
     end;
end;

function TMensajesTimbradoWebServices.GetTipoDialogoUI: TMsgDlgType;
var
   TipoImagenMensaje : eTipoImagenMensaje;
begin
     Result := mtInformation;
     TipoImagenMensaje := eTipoImagenMensaje(GetTipoError);
     case TipoImagenMensaje of
          rInformativo :
          begin
               Result := mtInformation;
          end;
          rAdvertencia :
          begin
               Result := mtWarning;
          end;
          rError :
          begin
               Result := mtError;
          end;
          rErrorGrave :
          begin
               Result := mtError;
          end;
     end;
end;

end.
