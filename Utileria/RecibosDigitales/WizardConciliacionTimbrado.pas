unit WizardConciliacionTimbrado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres, ZetaClientDataSet,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaDBTextBox, ExtCtrls, ZetaKeyLookup, Mask,
  DBCtrls,FTimbramexClasses, FTimbramexHelper, Grids, DBGrids, Buttons,
  ZetaSmartLists, FBaseReportes_DevEx, ZetaCommonLists, cxHyperLinkEdit,
  cxRadioGroup, URLMon, cxShellBrowserDialog, WinInet, WininetUtils,
  ZetaEdit, ZetaCommonClasses, DBClient,     DCatalogos, ZetaKeyCombo,
  ZetaCXGrid, TressMorado2013,ZetaKeyLookup_DevEx,cxMemo, ZetaFilesTools,
  cxButtonEdit, ActnList, UManejadorPeticiones, UPeticionWS, UThreadStack, UThreadPolling, UInterfaces,
  cxEditRepositoryItems, ZetaNumero, System.Actions, ZetaCXStateComboBox,
  cxImage, dxGDIPlusClasses, ZBaseShell, Vcl.ImgList, ZetaDespConsulta,
  cxSpinEdit,  Vcl.Styles, Vcl.Themes,ActiveX, cxExtEditRepositoryItems;



type
  TWizardConciliacionTimbradoForm = class(TdxWizardControlForm, ILogBitacora)
    dxWizardControl1: TdxWizardControl;
    dataSourceTimbrar: TDataSource;
    dataSourceContribuyente: TDataSource;
    DataSourceCuentas: TDataSource;
    DataSourceRecibos: TDataSource;
    DataSourceErrores: TDataSource;
    DatasetPeriodo: TDataSource;
    ActionListGrid: TActionList;
    Grid_AbrirArchivo: TAction;
    Grid_AbrirArchivoExplorer: TAction;
    cxEditRepositoryBotonesGrid: TcxEditRepository;
    ButtonsImpreso: TcxEditRepositoryButtonItem;
    ButtonsXML: TcxEditRepositoryButtonItem;
    DataSourceNominasXYear: TDataSource;
    DSNominasXYearAProcesar: TDataSource;
    ImageButtons: TcxImageList;
    WizardConciliacionPagina1: TdxWizardControlPage;
    WizardConciliacionPagina2: TdxWizardControlPage;
    WizardConciliacionPagina3: TdxWizardControlPage;
    PanelGrid: TPanel;
    GridPeriodosAConciliar: TZetaCXGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    PERIODO: TcxGridDBColumn;
    NOMINA: TcxGridDBColumn;
    STATUS_TIMBRADO: TcxGridDBColumn;
    TIMBRADO_EN_TRESS: TcxGridDBColumn;
    ESTATUS_CONCILIACION: TcxGridDBColumn;
    CONCILIAR: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    TIMBRADO_EN_SAT: TcxGridDBColumn;
    TOTAL: TcxGridDBColumn;
    Panel8: TPanel;
    cxLabel1: TcxLabel;
    cxLabel6: TcxLabel;
    Panel9: TPanel;
    GridPeriodosAProcesar: TZetaCXGrid;
    GridPeriodosProcesarDBTableView: TcxGridDBTableView;
    PERIODO_APLICA: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    CONCILIADOS: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    GridPeriodosProcesarDBTableViewColumn1: TcxGridDBColumn;
    GridPeriodosProcesarDBTableViewColumn2: TcxGridDBColumn;
    PanelFiltro: TPanel;
    Label11: TcxLabel;
    txtAnio: TcxSpinEdit;
    Label12: TcxLabel;
    zcboMes: TcxStateComboBox;
    DBEdit1: TDBEdit;
    cxGroupBox3: TcxGroupBox;
    PeriodoTipoLbl: TLabel;
    Label6: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label8: TLabel;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    iNumeroNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    iTipoNomina: TZetaTextBox;
    Label1: TLabel;
    PE_TIMBRO: TZetaDBTextBox;
    ActionList1: TActionList;
    actTodos: TAction;
    actNinguno: TAction;
    actIniciar: TAction;
    actSalir: TAction;
    actActualizarTabla: TAction;
    progressbarConciliacion: TcxEditRepositoryProgressBar;
    cxGridDBTableView1Column1: TcxGridDBColumn;
    cxGridDBTableView1Column2: TcxGridDBColumn;
    cxGridDBTableView1Column3: TcxGridDBColumn;
    WizardConciliacionPaginaContribuyente: TdxWizardControlPage;
    Label10: TLabel;
    RS_CODIGO: TZetaKeyLookup_DevEx;
    Panel1: TPanel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    Panel2: TPanel;
    GridPeriodosConciliados: TZetaCXGrid;
    GridDBTablePeriodosConciliados: TcxGridDBTableView;
    PE_YEAR: TcxGridDBColumn;
    PE_TIPO_APLICA: TcxGridDBColumn;
    PE_PERIODO: TcxGridDBColumn;
    NOMBRE_PERIODO: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridDBColumn18: TcxGridDBColumn;
    APLICAR: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    DSPeriodosConciliadosTimbrado: TDataSource;
    CONCILIA_PROCESO: TcxGridDBColumn;
    CONCILIA_STATUS: TcxGridDBColumn;
    ANIO: TcxGridDBColumn;
    PE_TIPO: TcxGridDBColumn;
    STATUS_CONCILIACION: TcxGridDBColumn;
    PORCENTAJE_CONCILIACION: TcxGridDBColumn;
    Panel3: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    txtMesPagina2: TZetaTextBox;
    txtAnioPagina2: TZetaTextBox;
    txtMesPagina3: TZetaTextBox;
    txtAnioPagina3: TZetaTextBox;
    LinksDetalleConciliacion: TcxEditRepositoryHyperLinkItem;
    DETALLE: TcxGridDBColumn;
    DETALLE_2: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure cambiarSeleccion (elegir: string);
    procedure RBListaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btREfrescaPeriodosClick(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure DataSourceNominasXYearUpdateData(Sender: TObject);
    procedure DataSourceErroresUpdateData(Sender: TObject);
    procedure DataSourceErroresDataChange(Sender: TObject; Field: TField);
    procedure zcboMesLookUp(Sender: TObject; var lOk: Boolean);
    procedure actActualizarTablaExecute(Sender: TObject);
    procedure DSNominasXYearAProcesarUpdateData(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure RS_CODIGOValidLookup(Sender: TObject);
    procedure DSPeriodosConciliadosTimbradoUpdateData(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure GridDBTablePeriodosConciliadosCellClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure GridDBTablePeriodosConciliadosMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure GridPeriodosProcesarDBTableViewCellClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure GridPeriodosProcesarDBTableViewMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure dxWizardControl1PageChanged(Sender: TObject);


  private
   FContinuaDownload : boolean;
   FEjecuto : boolean;
   FEsEmpleadoTransferido : Boolean;

   FTipoRango: eTipoRangoActivo;
   FParameterList: TZetaParams;
   FDescripciones: TZetaParams;

   //Progress Bar
   FTermino      : Boolean;
   FAvanzar      : Boolean;
   FConciliadoTimbrado : Boolean;
   Estado        : string;
   Mutex         : THandle;
   fAvances      : TList;
   FCredencialesTimbradoNomina: String;
   FProcesandoThreadConciliacion: Boolean;
   FProcesoThreadConciliacionCancelado: Boolean;
   { Private declarations }
   function GetNombreArchivoElegido : string;
   procedure CambioEmpleadoActivo;
   procedure RefrescarPeriodosPorMesYear;
   procedure RefrescarPeriodosPorMesYearFiltrados;
   procedure RefrescarPeriodosConciliadosPeridosAplica;

   //ProgressBar
   function actIniciarConciliacionExecute: Boolean;
  protected
   { Protected declarations }
   property ParameterList: TZetaParams read FParameterList;
   property Descripciones: TZetaParams read FDescripciones;
   function Verificar: Boolean; virtual;
   function EjecutarWizard: Boolean;
   procedure EnabledBotones( const eTipo: eTipoRangoActivo );
   procedure ApplyMinWidth;
   procedure RestaurarTamanioForma(iWidth: Integer = 0; iHeight: Integer = 0);
  public
      FTotalRead , FTotalSize : TPesos;
      FFileNameDescarga : String;
      property TipoRango: eTipoRangoActivo read FTipoRango;
      procedure EscribirBitacoraLog( sMensaje : string );
      procedure EscribirBitacoraError( sMensaje : string );
      procedure CargaPeriodoSeleccionado;
      function ExisteEnTimbrado: boolean;
      procedure ActualizacionGridUpdateConciliacion( sMensaje : string; iAvance: Integer; oDataSetUpdate: TZetaClientDataSet; oParams: TZetaParams );//Actualizacion de Grid
      procedure ActualizacionGridUpdateAplicacionConciliacion( sMensaje : string; iAvance: Integer; oDataSetUpdate: TZetaClientDataSet; oParams: TZetaParams );//Actualizacion de Grid
      function ObtenerTotalDiferenciasConciliadas: Integer;
      procedure SetBotonesAtrasSiguiente(bHabilitarBotones: boolean);
const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_NAVEGA = 4;
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
     K_MENSAJE_DIFERENCIAS_CONCILIACION = 'Ver diferencias';
     K_MENSAJE_DETALLE_CONCILIACION = 'Ver detalles';
end;

var
  WizardConciliacionTimbradoForm: TWizardConciliacionTimbradoForm;


function ShowWizardTimbrado : boolean;

implementation

uses
 ZetaClientTools,
 ZetaTipoEntidad,
 DTablas, Dcliente,
 DProcesos,
 DGlobal,
 ZGlobalTress,
 ZAccesosMgr,
 DBasicoCliente,
 ZetaBuscador_DevEx,
 ZConstruyeFormula,
 ZConfirmaVerificacion,
 ZetaCommonTools,
 ZetaBuscaEmpleado_DevEx,
 ZBaseSelectGrid_DevEx,
 ZBasicoSelectGrid_DevEx,
 FEmpleadoTimbradoGridSelect,
 PDFMerge,
 ZetaBuscaEmpleado_DevExAvanzada, ZetaBuscaEmpleado_DevExTimbradoAvanzada,
 FDetalleConciliacionTimbrado_DevEx;


{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin
    Result := FALSE;
end;

//FIN
function TWizardConciliacionTimbradoForm.actIniciarConciliacionExecute: Boolean;
var
  I, C     : Integer;
  bm       : TBookmark;
  Params   : TZetaParams;
  Param    : TParam;
  XMl, Error: WideString;
  sPeticion, sRespuesta: String;
  sXMl: String;
  respuestaConciacionEmpleadosNube : TRespuestaConciliacionTimbradoNomina;
  lbandera: Boolean;
  bProcesoConciliacion: Boolean;
  //Thread Conciliacion
  broker : TManejadorPeticiones ;
  oPeticionWS : TPeticionWS;
  procedure EsperarPeticion;
     var
        iCount : integer;
     begin
         iCount := 0;
         while oPeticionWS.lProceso do
         begin
           Inc( iCount );
           if not FProcesandoThreadConciliacion  then
           begin
                FProcesoThreadConciliacionCancelado := True;
                oPeticionWS.lCancelar := TRUE;
           end;
           if ( iCount mod 4  = 0 ) then
              Application.ProcessMessages
           else
               DelayNPM( 500  );
         end;
         Application.ProcessMessages;
     end;


begin
  inherited;
  Result := false;
  lbandera := false;
  bProcesoConciliacion := true;
  I := 0;
  Screen.Cursor := crHourGlass;
  //Limpriar dataset
  with DSNominasXYearAProcesar.DataSet do
  begin
       DisableControls;
       try
          try
            First;
            while not Eof do begin
              if FieldByName('CONCILIAR').AsBoolean then begin
                Edit;
                FieldByName('CONCILIA_PROCESO').AsInteger := 0;
                FieldByName('CONCILIA_STATUS').AsString   := '';
                Post;
              end;
              Next;
            end;
          except
          on e: Exception do
             begin
             end;
          end;
       except
            on e: Exception do
               begin
               end;
       end;
       EnableControls;
  end;
  //fin
  with DSNominasXYearAProcesar.DataSet do begin
    bm := Bookmark;
    Params := TZetaParams.Create;
    try
       try
          First;
          while not Eof do
          begin
               if FieldByName('CONCILIAR').AsBoolean then
               begin
                    Edit;
                    FieldByName('CONCILIA_PROCESO').AsInteger := 0;
                    FieldByName('CONCILIA_STATUS').AsString   := '';
                    Post;
                    Params.Clear;
                    Inc(I);
                    // Pasar la tupla a un TZetaParams
                    for C := 0 to Fields.Count - 1 do begin
                      Param          := Params.Add as TParam;
                      Param.Name     := Fields[C].FieldName;
                      Param.DataType := Fields[C].DataType;
                      Param.Size     := Fields[C].Size;
                      Param.Value    := Fields[C].Value;
                    end;
                    Params.AddString('RS_CODIGO', dmCliente.RazonSocialConciliacionTimbrado);
                    Params.AddInteger('USUARIO', dmCliente.Usuario);
                    Params.AddDate('US_FEC_MOD', dmCliente.FechaDefault);
                    Params.AddVariant('EMPRESA', dmCliente.Empresa);
                    //Fin de Agregar Parametros

                    //Lanzamos el Thread que estara procesando la informacion
                    broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );
                    oPeticionWS := TPeticionWS.Create;
                    oPeticionWS.tipoPeticion := peticionConciliacionNominaTimbrado;
                    oPeticionWS.ACallbackActualizacion := ActualizacionGridUpdateConciliacion;
                    oPeticionWS.oParamsConciliacion := Params;
                    if not FProcesandoThreadConciliacion then
                       oPeticionWS.lCancelar := TRUE;
                    oPeticionWS.lProceso := TRUE;
                    broker.AgregarPeticionWS( oPeticionWS );
                    EsperarPeticion; //Esperar la confirmacion de la finalizacion del Thread
                    //FIN
               end;
               if ( ( not lbandera ) and ( not bProcesoConciliacion ) ) then
                  lbandera := true;
                Next;
          end;

          //Procesar Multiple SHow
          if I = 0 then begin
            ZetaDialogo.ZetaMessage(Caption, 'Seleccionar un periodo.', mtInformation, [mbOK], 0, mbOk); // ToDo: Ayuda contextual
            lbandera := true;
          end;
       except
         on e: Exception do
            begin
                 lbandera := true;
                 ZetaDialogo.ZError(Caption, 'Ocurri� un error al conciliar. ' + sLineBreak + e.Message, 0); // ToDo: Ayuda contextual
            end;
       end;
    finally
           Result :=true;
           FreeAndNil(Params);
           Bookmark := bm;
           ApplyMinWidth;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TWizardConciliacionTimbradoForm.SetBotonesAtrasSiguiente(bHabilitarBotones: boolean);
begin
     dxWizardControl1.Buttons.Next.Enabled := bHabilitarBotones;
     dxWizardControl1.Buttons.Back.Enabled := bHabilitarBotones;
     dxWizardControl1.Buttons.Finish.Enabled := bHabilitarBotones;
     FProcesandoThreadConciliacion := not bHabilitarBotones;
end;


procedure TWizardConciliacionTimbradoForm.actActualizarTablaExecute(Sender: TObject);
begin
end;

{*** FIN ***}
procedure TWizardConciliacionTimbradoForm.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin
     if AKind = wcbkCancel then
     begin
          if ( FProcesandoThreadConciliacion ) then
          begin
                ModalResult := mrCancel;
                if ZetaDialogo.ZConfirm( self.Caption, '�Desea cancelar el proceso de conciliaci�n?', 0, mbNo ) then
                begin
                     FProcesandoThreadConciliacion := FALSE;
                     FProcesoThreadConciliacionCancelado := True;
                     dxWizardControl1.Buttons.Cancel.Enabled := False;
                     Application.ProcessMessages;
                end;
          end
          else
          begin
               if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
               begin
                     if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
                     begin
                          FProcesoThreadConciliacionCancelado := True;
                          ModalResult := mrCancel;
                     end;
               end
               else
               begin
                    ModalResult := mrCancel;
               end;
          end;
     end
     else if AKind = wcbkBack then
     begin
          dxWizardControl1.Buttons.Finish.Enabled := True;
          dxWizardControl1.Buttons.Cancel.Caption := 'Cancelar';
     end
     else if AKind = wcbkFinish then
     begin
          Ahandled := True;
          dxWizardControl1.Buttons.Finish.Enabled := true;
          SetBotonesAtrasSiguiente(false);
          Application.ProcessMessages;
          if ( EjecutarWizard ) then
          begin
               if not FProcesoThreadConciliacionCancelado then
               begin
                    APLICAR.Visible := False;
                    ZInformation('Proceso finalizado', 'Haga clic en el detalle para ver los cambios aplicados.', 0 );
                    dxWizardControl1.Buttons.Cancel.Caption := 'Salir';
               end;
               //Esconder Boton Aplicar una vez Aplicado la conciliacion
          end
          else
          begin
               zError( Self.Caption,'No se aplico la conciliaci�n en al menos un periodo',0);
          end;
          SetBotonesAtrasSiguiente(true);
          Application.ProcessMessages;
          dxWizardControl1.Buttons.Finish.Enabled := false;
     end;
end;

procedure TWizardConciliacionTimbradoForm.FormCreate(Sender: TObject);
    procedure InicializarFiltros;
    begin
         FParameterList := TZetaParams.Create;
         FDescripciones := TZetaParams.Create;
         ZBasicoSelectGrid_DevEx.ParametrosGrid := FDescripciones;
    end;
begin
     HelpContext:= H32142_ConciliadorTimbrado;
     InicializarFiltros;
     cxGridDBTableView1.ApplyBestFit();
     FProcesandoThreadConciliacion := False;
     FProcesoThreadConciliacionCancelado := False;
     with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iTipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
               FechaInicial.Caption := FormatDateTime( FormatSettings.LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( FormatSettings.LongDateFormat, Fin );
          end;
     end;
     DatasetPeriodo.DataSet := dmInterfase.cdsPeriodosAfectadosTotal;


     dmTablas.cdsEstado.Conectar;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
     dmCatalogos.InitActivosNavegacion;

     //Ordenamiento de Paginas.
     WizardConciliacionPagina1.PageIndex := 0;
     WizardConciliacionPagina2.PageIndex := 1;
     WizardConciliacionPagina3.PageIndex := 2;

     with dmCliente.GetDatosPeriodoActivo do
     begin
          txtAnio.Value  :=  Year;
          zcboMes.Indice :=  Mes;
          txtAnioPagina2.Caption := txtAnio.Text;
          txtMesPagina2.Caption := zcboMes.Text;
          txtAnioPagina3.Caption := txtAnio.Text;
          txtMesPagina3.Caption := zcboMes.Text;
          dmCliente.AnioConciliacionTimbrado := Year;
          dmCliente.MesConciliacionTimbrado := Mes;
     end;

     //Conexion Razon Social
     dmInterfase.cdsRSocial.Conectar;
     RS_CODIGO.LookupDataset := dmInterfase.cdsRSocial;
     FAvanzar := False;
     //FIN
end;

procedure TWizardConciliacionTimbradoForm.Button1Click(Sender: TObject);
begin
     //actIniciarConciliacionExecute;
     SHowMessage(format('%d - %d', [ Self.Height, Self.Width ]));
end;

procedure TWizardConciliacionTimbradoForm.Button2Click(Sender: TObject);
begin
     GridPeriodosProcesarDBTableView.ApplyBestFit();
end;

procedure TWizardConciliacionTimbradoForm.Button3Click(Sender: TObject);
begin
     ShowMessage(format('Ancho: %d',[ Self.Width ]));
end;

procedure TWizardConciliacionTimbradoForm.CargaPeriodoSeleccionado;
begin
     RefrescarPeriodosPorMesYear;
end;

function TWizardConciliacionTimbradoForm.ExisteEnTimbrado: boolean;
begin
   Result := (dmInterfase.cdsRSocial.FieldByName('RS_CONTID').AsInteger > 0 );
end;

function TWizardConciliacionTimbradoForm.ObtenerTotalDiferenciasConciliadas: Integer;
var
  AIndex, AGroupIndex: integer;
  AValue: variant;
  sValue: String;

  cdsClone: TZetaClientDataSet;
begin
     Result := 0;
     cdsClone := TZetaClientDataSet.Create(nil);
     try
        with cdsClone do
        begin
             CloneCursor( TZetaClientDataSet( GridPeriodosProcesarDBTableView.DataController.DataSet ), FALSE );    // Se conserva el filtro del dataset
             First;
             while ( not EOF ) do
             begin
                  Result  := Result + FieldByName('CONCILIADOS').AsInteger;
                  Next;
             end;
             Close;
        end;
     finally
            cdsClone.Free;
     end;
end;



procedure TWizardConciliacionTimbradoForm.dxWizardControl1PageChanged( Sender: TObject );
var
   lContinuar: Boolean;
begin
     if ( dxWizardControl1.ActivePage = WizardConciliacionPaginaContribuyente ) then
     begin
          dxWizardControl1.Buttons.Next.Caption := 'Siguiente';
          dxWizardControl1.Buttons.Finish.Enabled := False;
     end;

     if ( dxWizardControl1.ActivePage = WizardConciliacionPagina1 ) then
     begin
          dxWizardControl1.Buttons.Next.Caption := 'Conciliar';
          APLICAR.Visible := True;
          dxWizardControl1.Buttons.Finish.Enabled := True;
     end;

     if ( dxWizardControl1.ActivePage = WizardConciliacionPagina2 ) then
     begin
          dxWizardControl1.Buttons.Next.Caption := 'Siguiente';
          dxWizardControl1.Buttons.Finish.Enabled := False;
          Application.ProcessMessages;
     end;

     if ( dxWizardControl1.ActivePage = WizardConciliacionPagina3 ) then
     begin
          dxWizardControl1.Buttons.Next.Caption := 'Siguiente';
     end;

     if (dxWizardControl1.ActivePage = WizardConciliacionPagina2 ) then
     begin
          if not ( FConciliadoTimbrado ) then
           begin
                SetBotonesAtrasSiguiente(false);
                Application.ProcessMessages;
                if ( actIniciarConciliacionExecute ) then
                begin
                     RefrescarPeriodosConciliadosPeridosAplica;
                     DSPeriodosConciliadosTimbrado.DataSet := dmInterfase.cdsTimbradoConciliaGetPeriodos;
                     ApplyMinWidth;
                     GridDBTablePeriodosConciliados.ApplyBestFit();
                     RestaurarTamanioForma(1266,600);//Cambiar Tamanio de la forma dependiendo de cada paso.
                     if not FProcesoThreadConciliacionCancelado then
                     begin
                          if ( ObtenerTotalDiferenciasConciliadas > 0 ) then
                          begin
                               ZInformation('Proceso finalizado', 'Haga clic en el detalle para m�s informaci�n de la comparaci�n de cada periodo.', 0 );
                          end
                          else
                          begin
                               //Ini
                               lContinuar := true;
                               lContinuar := zWarningYesNoConfirm( Self.Caption, 'No se encontraron diferencias.' + CR_LF + '�Desea salir del proceso de conciliaci�n?', 0, mbOK  );
                               if lContinuar then
                               begin
                                    Close;
                               end;
                               //Fin

                          end;
                     end;
                     FAvanzar := TRUE;
                     FConciliadoTimbrado := TRUE;
                     SetBotonesAtrasSiguiente(true);
                     Application.ProcessMessages;
                end
                else
                begin
                     zError( Self.Caption,'Revisar no se proceso al menos una conciliaci�n',0);
                end;
           end;
     end;
end;

procedure TWizardConciliacionTimbradoForm.dxWizardControl1PageChanging(Sender: TObject;
  ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
const
    K_TRUE = 'True';
    K_QUERY_FILTRO_PERIODOS_A_CONCILIAR = 'CONCILIAR = %s';
    K_QUERY_FILTRO_PERIODOS_CONCILIADOS = '( PE_YEAR = %0:d ) and ( PE_TIPO = %1:d ) and ( PE_NUMERO = %2:s)';
  var
    iContadorNominasAProcesar : integer;
    lContinuar: Boolean;
begin
     if ( AnewPage = WizardConciliacionPaginaContribuyente ) then
     begin
          RestaurarTamanioForma(1266,600);//Cambiar Tamanio de la forma dependiendo de cada paso.
          AAllow := TRUE;
     end;

     if ( AnewPage = WizardConciliacionPagina1 ) then
     begin
          AAllow := TRUE;
          FConciliadoTimbrado := False;
          if StrVacio( RS_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'El contribuyente no puede quedar vac�o.', 0  );
               AAllow := FALSE;
          end;
          if ( AAllow ) then
          begin
               if not ExisteEnTimbrado then
               begin
                     AAllow := FALSE;
                     //Mostrar mensaje de Timbrado Nomina
                     zError( Self.Caption, 'El contribuyente no est� registrado, no se podr� proceder a conciliar.', 0  );
               end
               else
               begin
                    RestaurarTamanioForma(1266,600);//Cambiar Tamanio de la forma dependiendo de cada paso.
                    dmCliente.RazonSocialConciliacionTimbrado := RS_CODIGO.Llave;
                    RefrescarPeriodosPorMesYear;
                    ApplyMinWidth;
                    cambiarSeleccion('True');
                    cxGridDBTableView1.ApplyBestFit();
               end;
          end;
     end;

     if ( AnewPage = WizardConciliacionPagina2 ) then
     begin
          AAllow := TRUE;

          if ( dxWizardControl1.ActivePage = WizardConciliacionPagina1 ) then
          begin
               FConciliadoTimbrado := False;
               FAvanzar := False
          end;

          if not ( FConciliadoTimbrado ) then
          begin
               dmInterfase.cdsPeriodosXMesYearAProcesar.Data :=  dmInterfase.cdsPeriodosXMesYear.Data;
               with dmInterfase.cdsPeriodosXMesYearAProcesar do
               begin
                    Filter := Format( K_QUERY_FILTRO_PERIODOS_A_CONCILIAR, [ EntreComillas( K_TRUE ) ] );
                    Filtered := true;
                    iContadorNominasAProcesar := RecordCount;
                    if iContadorNominasAProcesar < 1  then
                    begin
                         zError( Self.Caption,'Seleccione al menos un periodo de n�mina a procesar.',0);
                         AAllow := FALSE;
                    end
                    else
                    begin
                         RestaurarTamanioForma(1266,600);//Cambiar Tamanio de la forma dependiendo de cada paso.
                         RefrescarPeriodosPorMesYearFiltrados;
                         ApplyMinWidth;
                         GridPeriodosProcesarDBTableView.ApplyBestFit();

                         //Aplicar conciliacion
                         //Preguntar si desea realizar la conciliacion si no, impedir el avance.
                         if not FAvanzar then
                         begin
                              lContinuar := true;
                              lContinuar := ZWarningConfirm( Self.Caption, 'Este proceso puede demorar unos minutos.' + CR_LF + '�Est� seguro que desea iniciar el proceso de conciliaci�n?', 0, mbOK  );
                              if not lContinuar then
                              begin
                                   AAllow := FALSE;
                              end
                              else
                              begin
                                   AAllow := TRUE;
                                   FAvanzar := True;
                              end;
                         end
                         else
                         begin
                              AAllow := TRUE;
                              FAvanzar := False;
                         end;
                         //FIN
                    end;
               end;
          end;
     end;
end;

procedure TWizardConciliacionTimbradoForm.CT_CODIGOValidLookup(Sender: TObject);
begin
      DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

procedure TWizardConciliacionTimbradoForm.btnDesmarcarTodasClick(Sender: TObject);
begin
     cambiarSeleccion('False');
end;

procedure TWizardConciliacionTimbradoForm.btnSeleccionarTodosClick(Sender: TObject);
begin
     cambiarSeleccion('True');
end;

procedure TWizardConciliacionTimbradoForm.cambiarSeleccion (elegir: string);
var sRegistroActual: String;
begin
     try
        with DataSourceNominasXYear.DataSet do
        begin
             DisableControls;
             //sRegistroActual := FieldByName('PE_NUMERO').AsString;
             cxGridDBTableView1.OptionsView.ScrollBars := ssNone;
             First;
             while not Eof do
             begin
                 Edit;
                 FieldByName ('CONCILIAR').AsString := elegir;
                 Post;
                 Next;
             end;
             First;
             cxGridDBTableView1.OptionsView.ScrollBars := ssBoth;
             //Locate('PE_NUMERO', sRegistroActual, []);
             EnableControls;
        end;
      except on Error: Exception do
             begin
                //ZWarning( 'Obtener Recibos Timbrados por Empleado','', 0, mbOK);
             end;
      end;
end;

procedure TWizardConciliacionTimbradoForm.btREfrescaPeriodosClick(Sender: TObject);
begin
     RefrescarPeriodosPorMesYear;
end;

procedure TWizardConciliacionTimbradoForm.RefrescarPeriodosPorMesYear;
var
  sError : String;
begin
     sError := VACIO;
     with dmInterfase do
     begin
          with dmCliente.GetDatosPeriodoActivo do
          begin
               dmCliente.AnioConciliacionTimbrado := txtAnio.Value;
               dmCliente.MesConciliacionTimbrado := zcboMes.Indice;
               txtAnioPagina2.Caption := txtAnio.Text;
               txtMesPagina2.Caption := zcboMes.Text;
               txtAnioPagina3.Caption := txtAnio.Text;
               txtMesPagina3.Caption := zcboMes.Text;
          end;
          dmInterfase.cdsPeriodosXMesYear.DisableControls;
          dmInterfase.cdsPeriodosXMesYear.Refrescar;
          dmInterfase.cdsPeriodosXMesYear.EnableControls;
          sError := VACIO;
          if ( cdsPeriodosXMesYear.IsEmpty ) or (sError <> VACIO) then
              begin
               if cxGridDBTableView1.DataController.RecordCount > 0 then
               begin
                    cxGridDBTableView1.DataController.RecordCount := 0;
               end;
               Refresh;
            end
            else
            begin
             DataSourceNominasXYear.DataSet := dmInterfase.cdsPeriodosXMesYear;
             Refresh;
             cxGridDBTableView1.DataController.FocusedRowIndex := 0;
            end;
     end;
end;


procedure TWizardConciliacionTimbradoForm.RefrescarPeriodosPorMesYearFiltrados;
var
  sError : String;
begin
     dmInterfase.cdsPeriodosXMesYearAProcesar.Refrescar;
     sError := VACIO;
     with dmInterfase do
     begin
          if ( cdsPeriodosXMesYearAProcesar.IsEmpty ) then
              begin
               if GridPeriodosProcesarDBTableView.DataController.RecordCount > 0 then
               begin
                    GridPeriodosProcesarDBTableView.DataController.RecordCount := 0;
               end;
               Refresh;
          end
          else
          begin
               DSNominasXYearAProcesar.DataSet := dmInterfase.cdsPeriodosXMesYearAProcesar;
               Refresh;
               GridPeriodosProcesarDBTableView.DataController.FocusedRowIndex := 0;
          end;
     end;
end;

procedure TWizardConciliacionTimbradoForm.RefrescarPeriodosConciliadosPeridosAplica;
var
  sError : String;
begin
     dmInterfase.cdsTimbradoConciliaGetPeriodos.Refrescar;
     sError := VACIO;
     with dmInterfase do
     begin
          if ( cdsTimbradoConciliaGetPeriodos.IsEmpty ) then
              begin
               if GridDBTablePeriodosConciliados.DataController.RecordCount > 0 then
               begin
                    GridDBTablePeriodosConciliados.DataController.RecordCount := 0;
               end;
               Refresh;
          end
          else
          begin
               DSPeriodosConciliadosTimbrado.DataSet := dmInterfase.cdsTimbradoConciliaGetPeriodos;
               Refresh;
               GridDBTablePeriodosConciliados.DataController.FocusedRowIndex := 0;
          end;
     end;
end;



procedure TWizardConciliacionTimbradoForm.RS_CODIGOValidLookup(Sender: TObject);
begin
     try
        dmInterfase.cdsRSocial.Locate('RS_CODIGO', RS_CODIGO.Llave, []);
     Except
     end;
end;

procedure TWizardConciliacionTimbradoForm.CambioEmpleadoActivo;
begin
     RefrescarPeriodosPorMesYear;
end;

procedure TWizardConciliacionTimbradoForm.FormDestroy(Sender: TObject);
begin
     //FDescripciones.Free;
     //FParameterList.Free;
end;

procedure TWizardConciliacionTimbradoForm.EnabledBotones(const eTipo: eTipoRangoActivo);
{var
   lEnabled: Boolean;}
begin
     {FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;}
end;


procedure TWizardConciliacionTimbradoForm.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TWizardConciliacionTimbradoForm.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TWizardConciliacionTimbradoForm.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

function TWizardConciliacionTimbradoForm.Verificar: Boolean;
begin

end;

procedure TWizardConciliacionTimbradoForm.zcboMesLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        CambioEmpleadoActivo;
        cambiarSeleccion('True');
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TWizardConciliacionTimbradoForm.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     Mutex := CreateMutex(nil, False, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ClassName + '.Execute'));
     if Mutex = 0 then
        RaiseLastOSError;
          cxGridDBTableView1.ApplyBestFit();
     cxGridDBTableView1.OptionsCustomize.ColumnFiltering := False;
     FEsEmpleadoTransferido := false;

     cxGridDBTableView1.applybestfit();

      {***Configuracion nuevo Grid***}
     //Desactiva la posibilidad de agrupar
     cxGridDBTableView1.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     cxGridDBTableView1.OptionsView.GroupByBox := False;
     //Para remover las opcioens de filtrado
     cxGridDBTableView1.OptionsCustomize.ColumnFiltering := False;
     //Para que nunca muestre el filterbox inferior
     cxGridDBTableView1.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     cxGridDBTableView1.FilterBox.CustomizeDialog := False;
     //FIN

     //Grid 2
     GridPeriodosProcesarDBTableView.applybestfit();
     {***Configuracion nuevo Grid***}
     //Desactiva la posibilidad de agrupar
     GridPeriodosProcesarDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     GridPeriodosProcesarDBTableView.OptionsView.GroupByBox := False;
     //Para remover las opcioens de filtrado
     GridPeriodosProcesarDBTableView.OptionsCustomize.ColumnFiltering := False;
     //Para que nunca muestre el filterbox inferior
     GridPeriodosProcesarDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     GridPeriodosProcesarDBTableView.FilterBox.CustomizeDialog := False;
     //FIN

     //Grid 3
     GridDBTablePeriodosConciliados.applybestfit();
     {***Configuracion nuevo Grid***}
     //Desactiva la posibilidad de agrupar
     GridDBTablePeriodosConciliados.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     GridDBTablePeriodosConciliados.OptionsView.GroupByBox := False;
     //Para remover las opcioens de filtrado
     GridDBTablePeriodosConciliados.OptionsCustomize.ColumnFiltering := False;
     //Para que nunca muestre el filterbox inferior
     GridDBTablePeriodosConciliados.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     GridDBTablePeriodosConciliados.FilterBox.CustomizeDialog := False;
     //FIN

     //inicializacion orden pestanas
     WizardConciliacionPaginaContribuyente.PageIndex := 0;
     WizardConciliacionPagina1.PageIndex := 1;
     WizardConciliacionPagina2.PageIndex := 2;
     WizardConciliacionPagina3.PageIndex := 3;

     //Obtener Valor de Mes y Anio apartir del periodo
     fAvances := TList.Create;
     RS_CODIGO.SetFocus;
end;

procedure TWizardConciliacionTimbradoForm.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =10;
begin
     with  cxGridDBTableView1 do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption) + widthColumnOptions;
          end;
          ApplyBestFit();
     end;

     with  GridPeriodosProcesarDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption) + widthColumnOptions;
          end;
          ApplyBestFit();
     end;

     with GridDBTablePeriodosConciliados do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption) + widthColumnOptions;
          end;
          ApplyBestFit();
     end;
end;

function TWizardConciliacionTimbradoForm.GetNombreArchivoElegido: string;
begin
 Result := VACIO;
 with dmInterfase.cdsArchivosDescargados do
 begin
    if not isEmpty then
     Result :=  FieldByName('ARCHIVO').AsString;
 end;
end;

procedure TWizardConciliacionTimbradoForm.GridDBTablePeriodosConciliadosCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
   iYear: Integer;
   iPeriodo: Integer;
   iTipoPeriodo: Integer;
begin
     if (ACellViewInfo.Item = DETALLE) and (AButton = mbLeft) then
     begin
          if (  ACellViewInfo.RecordViewInfo.GridRecord.Values[DETALLE.Index] = K_MENSAJE_DETALLE_CONCILIACION ) then
          begin
               iYear := ACellViewInfo.RecordViewInfo.GridRecord.Values[PE_YEAR.Index];
               iTipoPeriodo := ACellViewInfo.RecordViewInfo.GridRecord.Values[PE_TIPO_APLICA.Index];
               iPeriodo := ACellViewInfo.RecordViewInfo.GridRecord.Values[PE_PERIODO.Index];
               FDetalleConciliacionTimbrado_DevEx.GetDetalleConciliacionPeriodo(iYear,iTipoPeriodo,iPeriodo,zcboMes.Text, dmCliente.RazonSocialConciliacionTimbrado );
          end;
     end;
end;

procedure TWizardConciliacionTimbradoForm.GridDBTablePeriodosConciliadosMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   Site: TcxGridSite;
   AHitTest: TcxCustomGridHitTest;
   Item: TcxCustomGridTableItem;
   Rec: TcxCustomGridRecord;
   AColumn: TcxGridDBColumn;
begin
     AHitTest := (Sender as TcxGridSite).GridView. GetHitTest (Point (X, Y));
     if (AHitTest is TcxGridRecordCellHitTest) and ((AHitTest as TcxGridRecordCellHitTest).Item = DETALLE) then
     begin
          if PtInRect ((AHitTest as TcxGridRecordCellHitTest).ViewInfo. TextBounds, Point (X, Y)) then
             (Sender as TcxGridSite).Cursor := crHandPoint
          else
              (Sender as TcxGridSite).Cursor := crDefault;
     end;
end;

procedure TWizardConciliacionTimbradoForm.GridPeriodosProcesarDBTableViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
var
   iYear: Integer;
   iPeriodo: Integer;
   iTipoPeriodo: Integer;
begin
     if (ACellViewInfo.Item = DETALLE_2) and (AButton = mbLeft) then
     begin
          if (  ACellViewInfo.RecordViewInfo.GridRecord.Values[DETALLE_2.Index] = K_MENSAJE_DIFERENCIAS_CONCILIACION ) then
          begin
               iYear := ACellViewInfo.RecordViewInfo.GridRecord.Values[ANIO.Index];
               iTipoPeriodo := ACellViewInfo.RecordViewInfo.GridRecord.Values[PE_TIPO.Index];
               iPeriodo := ACellViewInfo.RecordViewInfo.GridRecord.Values[PERIODO_APLICA.Index];
               FDetalleConciliacionTimbrado_DevEx.GetDetalleConciliacionPeriodo(iYear,iTipoPeriodo,iPeriodo,zcboMes.Text, dmCliente.RazonSocialConciliacionTimbrado );
          end;
     end;
end;

procedure TWizardConciliacionTimbradoForm.GridPeriodosProcesarDBTableViewMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
   Site: TcxGridSite;
   AHitTest: TcxCustomGridHitTest;
   Item: TcxCustomGridTableItem;
   Rec: TcxCustomGridRecord;
   AColumn: TcxGridDBColumn;
begin
     AHitTest := (Sender as TcxGridSite).GridView. GetHitTest (Point (X, Y));
     if (AHitTest is TcxGridRecordCellHitTest) and ((AHitTest as TcxGridRecordCellHitTest).Item = DETALLE_2) then
     begin
          if PtInRect ((AHitTest as TcxGridRecordCellHitTest).ViewInfo. TextBounds, Point (X, Y)) then
             (Sender as TcxGridSite).Cursor := crHandPoint
          else
              (Sender as TcxGridSite).Cursor := crDefault;
     end;
end;

procedure TWizardConciliacionTimbradoForm.EscribirBitacoraError(
  sMensaje: string);
begin
//
end;

procedure TWizardConciliacionTimbradoForm.EscribirBitacoraLog(sMensaje: string);
begin
  //
end;


procedure TWizardConciliacionTimbradoForm.DataSourceErroresDataChange(Sender: TObject; Field: TField);
begin
     cxGridDBTableView1.ApplyBestFit();
end;

procedure TWizardConciliacionTimbradoForm.DataSourceErroresUpdateData(Sender: TObject);
begin
     cxGridDBTableView1.ApplyBestFit();
end;

procedure TWizardConciliacionTimbradoForm.DataSourceNominasXYearUpdateData( Sender: TObject);
begin
     cxGridDBTableView1.applybestfit();
end;

procedure TWizardConciliacionTimbradoForm.DSNominasXYearAProcesarUpdateData(
  Sender: TObject);
begin
     GridPeriodosProcesarDBTableView.applybestfit();
end;

procedure TWizardConciliacionTimbradoForm.DSPeriodosConciliadosTimbradoUpdateData(
  Sender: TObject);
begin
     GridDBTablePeriodosConciliados.applybestfit();
end;


function TWizardConciliacionTimbradoForm.EjecutarWizard: Boolean;
var
  I, C     : Integer;
  bm       : TBookmark;
  Params   : TZetaParams;
  Param    : TParam;
  XMl, Error: WideString;
  sPeticion, sRespuesta: String;
  sXMl: String;
  bProcesoConciliacion: Boolean;
  lbandera: Boolean;

  //Thread Conciliacion
  broker : TManejadorPeticiones ;
  oPeticionWS : TPeticionWS;


  procedure EsperarPeticion;
     var
        iCount : integer;
     begin
         iCount := 0;
         while oPeticionWS.lProceso do
         begin
           Inc( iCount );
           if not FProcesandoThreadConciliacion then
           begin
                oPeticionWS.lCancelar := TRUE;
                FProcesoThreadConciliacionCancelado := True;
           end;
           if ( iCount mod 4  = 0 ) then
              Application.ProcessMessages
           else
               DelayNPM( 500  );
         end;
         Application.ProcessMessages;
     end;


begin
  inherited;
  Result := false;
  lbandera := false;
  bProcesoConciliacion := true;
  I := 0;
  Screen.Cursor := crHourGlass;
  with DSPeriodosConciliadosTimbrado.DataSet do begin
    bm := Bookmark;
    Params := TZetaParams.Create;
    //DisableControls;
    WaitForSingleObject(Mutex, INFINITE);
    try
       try
         First;
         while not Eof do begin
           if FieldByName('CONCILIAR').AsBoolean then begin
             Params.Clear;
             Inc(I);
             // Pasar la tupla a un TZetaParams
             for C := 0 to Fields.Count - 1 do begin
               Param          := Params.Add as TParam;
               Param.Name     := Fields[C].FieldName;
               Param.DataType := Fields[C].DataType;
               Param.Size     := Fields[C].Size;
               Param.Value    := Fields[C].Value;
             end;

             //Agregar parametros
             Params.AddInteger('USUARIO', dmCliente.Usuario);
             Params.AddDate('US_FEC_MOD', dmCliente.FechaDefault);

             // RCM Here comes the magic
             FTermino := False;
             Estado := VACIO;
             try
                broker := TManejadorPeticiones.Create( ILogBitacora( Self ) );
                oPeticionWS := TPeticionWS.Create;
                oPeticionWS.tipoPeticion := peticionAplicacionConciliacionNominaTimbrado;
                oPeticionWS.ACallbackActualizacion := ActualizacionGridUpdateAplicacionConciliacion;
                oPeticionWS.oParamsConciliacion := Params;
                oPeticionWS.lProceso := TRUE;
                if not FProcesandoThreadConciliacion then
                   oPeticionWS.lCancelar := TRUE;
                broker.AgregarPeticionWS( oPeticionWS );
                EsperarPeticion; //Esperar la confirmacion de la finalizacion del Thread
             except
               on e: Exception do begin
                 Edit;
                 FieldByName('CONCILIA_PROCESO').AsInteger := 100;
                 FieldByName('CONCILIA_STATUS').AsString := e.Message;
                 Post;
                 Application.ProcessMessages;
                 bProcesoConciliacion := false;
               end;
             end
           end;
           if ( ( not lbandera ) and ( not bProcesoConciliacion ) ) then
              lbandera := true;
           Next;
         end;
         if I = 0 then begin
           ZetaDialogo.ZetaMessage(Caption, 'Seleccionar un periodo.', mtInformation, [mbOK], 0, mbOk); // ToDo: Ayuda contextual
           lbandera := true;
         end;
       except
         on e: Exception do
         begin
           ZetaDialogo.ZError(Caption, 'Ocurri� un error al aplicar conciliaci�n. ' + sLineBreak + e.Message, 0); // ToDo: Ayuda contextual
           lbandera := false;
         end;
       end;
    finally
           ReleaseMutex(Mutex);
           FreeAndNil(Params);
           Bookmark := bm;
           EnableControls;
           ApplyMinWidth;
           Result := true; //not lbandera;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TWizardConciliacionTimbradoForm.RestaurarTamanioForma( iWidth: Integer = 0; iHeight: Integer = 0 );
begin
     if ( iHeight > 0 ) then
        Self.ClientHeight := iHeight;

     if ( iWidth > 0 ) then
     Self.ClientWidth := iWidth;
end;

procedure TWizardConciliacionTimbradoForm.ActualizacionGridUpdateConciliacion( sMensaje : string; iAvance: Integer; oDataSetUpdate: TZetaClientDataSet; oParams: TZetaParams);//Actualizacion de Grid
var
   bm: TBookmark;
begin
     try
        with GridPeriodosProcesarDBTableView.DataController.DataSource.DataSet do
        begin
             DisableControls;
             bm := Bookmark;
             if Locate('PE_YEAR;PE_TIPO;PE_NUMERO',VarArrayOf( [ oParams.ParamByName('PE_YEAR').AsInteger, oParams.ParamByName('PE_TIPO').AsInteger, oParams.ParamByName('PE_NUMERO').AsInteger ] ), [] ) then
             try
                Edit;
                FieldByName('CONCILIA_PROCESO').AsInteger := iAvance;
                FieldByName('CONCILIA_STATUS').AsString   := sMensaje;

                //Diferencias concultar
                if ( oDataSetUpdate <> nil ) and not ( oDataSetUpdate.EOF ) then
                begin
                     with oDataSetUpdate do
                     begin
                          if Locate('PE_YEAR;PE_TIPO;PE_NUMERO',VarArrayOf( [ oParams.ParamByName('PE_YEAR').AsInteger, oParams.ParamByName('PE_TIPO').AsInteger, oParams.ParamByName('PE_NUMERO').AsInteger ] ), [] ) then
                          begin
                               GridPeriodosProcesarDBTableView.DataController.DataSource.DataSet.FieldByName('CONCILIADOS').AsInteger := oDataSetUpdate.FieldByName('CONCILIADOS').AsInteger;
                               GridPeriodosProcesarDBTableView.DataController.DataSource.DataSet.FieldByName('TIMBRADOS_SAT').AsInteger := oDataSetUpdate.FieldByName('TIMBRADOS_SAT').AsInteger;
                               GridPeriodosProcesarDBTableView.DataController.DataSource.DataSet.FieldByName('PORC_CONCILIADOS').AsFloat := oDataSetUpdate.FieldByName('PORC_CONCILIADOS').AsFloat;
                          end;
                     end
                end;

                if ( iAvance = 100)  and ( not oParams.ParamByName('ERROR_CONCILIACION').AsBoolean ) and ( FieldByName('CONCILIADOS').AsInteger > 0 ) then
                begin
                     FieldByName('DETALLE').AsString := 'Ver diferencias';
                end;
                Post;
             finally
                    Bookmark := bm;
                    EnableControls;
             end;
        end;
     finally
     end;
end;


procedure TWizardConciliacionTimbradoForm.ActualizacionGridUpdateAplicacionConciliacion( sMensaje : string; iAvance: Integer; oDataSetUpdate: TZetaClientDataSet; oParams: TZetaParams);//Actualizacion de Grid
var
   bm: TBookmark;
begin
     try
        with GridDBTablePeriodosConciliados.DataController.DataSource.DataSet do  //DSNominasXYearAProcesar.DataSet do
        begin
             DisableControls;
             bm := Bookmark;
             if Locate('PE_YEAR;PE_TIPO;PE_NUMERO',VarArrayOf( [ oParams.ParamByName('PE_YEAR').AsInteger, oParams.ParamByName('PE_TIPO').AsInteger, oParams.ParamByName('PE_NUMERO').AsInteger ] ), [] ) then
             try
                Edit;
                FieldByName('CONCILIA_PROCESO').AsInteger := iAvance;
                FieldByName('CONCILIA_STATUS').AsString   := sMensaje;

                //Diferencias concultar
                if ( oDataSetUpdate <> nil ) and not ( oDataSetUpdate.EOF ) then
                begin
                     with oDataSetUpdate do
                     begin
                          if Locate('PE_YEAR;PE_TIPO;PE_NUMERO',VarArrayOf( [ oParams.ParamByName('PE_YEAR').AsInteger, oParams.ParamByName('PE_TIPO').AsInteger, oParams.ParamByName('PE_NUMERO').AsInteger ] ), [] ) then
                          begin
                               GridDBTablePeriodosConciliados.DataController.DataSource.DataSet.FieldByName('CONCILIADOS').AsInteger := oDataSetUpdate.FieldByName('CONCILIADOS').AsInteger;
                          end;
                     end
                end;

                if ( iAvance = 100)  and ( not oParams.ParamByName('ERROR_CONCILIACION').AsBoolean ) then
                begin
                     FieldByName('DETALLE').AsString := 'Ver detalles';
                end;
                Post;
             finally
                    Bookmark := bm;
                    EnableControls;
             end;
        end;
     finally
     end;
end;


end.


