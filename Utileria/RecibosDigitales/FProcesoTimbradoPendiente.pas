unit FProcesoTimbradoPendiente;

interface

uses
    SysUtils,
    StrUtils,
    Classes,
    ZetaCommonClasses,
    ZetaClientDataSet,
    ZetaCommonLists,
    DInterfase;
type
  TProcesoTimbradoPendiente = class
  private
    { private declarations here }
    FParams: TZetaParams;
    FPaquetesXML : TStringList;
    FNumeroPaquetes: Integer;
    FXMLTotalEmpleados : WideString;
    FDataSetNominasResultadoDS: TZetaClientDataSet;
    FDataSetPeriodos: TZetaClientDataSet;
    FDataSetRazonesSociales: TZetaClientDataSet;
    FParamsTimbradoNomina: TZetaParams;
  protected
    { protected declarations here }
  public
    { public declarations here }
    constructor Create;
    destructor Destroy; override;
    procedure SetEstatusPendienteEmpleados;
    procedure SetEstatusPendienteEmpleadosCanceladosLimpiar;
    procedure SetEstatusPendienteEmpleadosTimbradosLimpiar;
    procedure SetEstatusCancelacionPendienteEmpleados;
    //procedure
  published
    { published declarations here }
    property Params : TZetaParams read FParams write FParams;
    property PaquetesXML : TStringList read FPaquetesXML write FPaquetesXML;
    property NumeroPaquetes : Integer read FNumeroPaquetes write FNumeroPaquetes;
    property XMLTotalEmpleados : WideString read FXMLTotalEmpleados write FXMLTotalEmpleados;
    property DataSetNominasResultadoDS: TZetaClientDataSet read FDataSetNominasResultadoDS write FDataSetNominasResultadoDS;
    property DataSetPeriodos: TZetaClientDataSet read FDataSetPeriodos write FDataSetPeriodos;
    property DataSetRazonesSociales: TZetaClientDataSet read FDataSetRazonesSociales write FDataSetRazonesSociales;
    property ParamsTimbradoNomina: TZetaParams read FParamsTimbradoNomina write FParamsTimbradoNomina;
end;

implementation
{ TProcesoTimbradoPendiente }

constructor TProcesoTimbradoPendiente.Create;
begin
     inherited Create;
     FParamsTimbradoNomina := TZetaParams.Create;
end;

destructor TProcesoTimbradoPendiente.Destroy;
begin
     if not ( FDataSetNominasResultadoDS = nil ) then
        FreeAndNil( FDataSetNominasResultadoDS );
     if not ( FParamsTimbradoNomina = nil ) then
        FreeAndNil( FParamsTimbradoNomina );
     inherited;
end;

procedure TProcesoTimbradoPendiente.SetEstatusCancelacionPendienteEmpleados;
begin
     dmInterfase.SetEstatusEmpleadoNomina( estiCancelacionPendiente, FParamsTimbradoNomina );
end;

procedure TProcesoTimbradoPendiente.SetEstatusPendienteEmpleados;
begin
     dmInterfase.SetEstatusEmpleadoNomina( estiTimbradoPendiente, FParamsTimbradoNomina );
end;

procedure TProcesoTimbradoPendiente.SetEstatusPendienteEmpleadosCanceladosLimpiar;
begin
     dmInterfase.SetEstatusEmpleadoNomina( estiCancelacionPendienteLimpiar, FParamsTimbradoNomina );
end;


procedure TProcesoTimbradoPendiente.SetEstatusPendienteEmpleadosTimbradosLimpiar;
begin
     dmInterfase.SetEstatusEmpleadoNomina( estiTimbradoPendienteLimpiar, FParamsTimbradoNomina );
end;

end.

