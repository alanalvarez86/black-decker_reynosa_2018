unit ZetaDialogAbrirArchivo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, ZBaseThreads,
     ZetaDBTextBox, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons,
     cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo, ShellAPI;

type
  TDialogAbrirArchivo_DevEx = class(TZetaDlgModal_DevEx)
    Imagen: TImage;
    cxImageList_ImagenMensaje: TcxImageList;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);

  private
    { Private declarations }
    FVerBitacora: Boolean;
    FArchivoUrl: String;
    FArchivoNombre1: String;
    FTituloVentana: String;
    procedure SetBotonDefault(const lOK: Boolean);
  public
    { Public declarations }
    property VerBitacora: Boolean read FVerBitacora write FVerBitacora default TRUE;
    property ArchivoUrl: String read FArchivoUrl write FArchivoUrl;
    property ArchivoNombre1: String read FArchivoNombre1 write FArchivoNombre1;
    property TituloVentana: String read FTituloVentana write FTituloVentana;
    function ShowDialogoAbrirArchivo(const lMostrar: Boolean): Integer;
  end;

implementation

uses ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

const
     K_ALTURA_FORMA = 315; //+15 Para corregir el problema en los Large Fonts

{$R *.DFM}

{ *********** TWizardResults ********* }

procedure TDialogAbrirArchivo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with Imagen do
     begin
          cxImageList_ImagenMensaje.GetBitmap( 0, Imagen.Picture.Bitmap);  //DevEx
     end;
     FVerBitacora := TRUE;
end;

procedure TDialogAbrirArchivo_DevEx.FormShow(Sender: TObject);
var
   dDuracion: TDateTime;

   procedure SetImagen( const pIcon: PChar );overload;
   begin
        Imagen.Picture.Icon.Handle := LoadIcon( 0, pIcon );
   end;

   procedure SetImagen( iStatus : Integer );overload;
   begin
        cxImageList_ImagenMensaje.GetBitmap( iStatus, Imagen.Picture.Bitmap);
   end;
begin
     inherited;
     Self.Caption := FTituloVentana;
     SetImagen( ord(epsEjecutando) );            //DevEx
     Cancelar_DevEx.Visible := TRUE;
     Beep;
     if not Application.Active then
        FlashWindow(Application.Handle,TRUE);
end;

procedure TDialogAbrirArchivo_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     ShellExecute(Handle, 'open','c:\windows\explorer.exe', {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ExtractFilePath(FArchivoNombre1)), nil, SW_SHOWNORMAL);
end;

procedure TDialogAbrirArchivo_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     try
        ExecuteFile( ArchivoNombre1, '', '', SW_SHOWDEFAULT );
     except
           On E:Exception do
              ZetaDialogo.ZError('Problemas al Abrir Documento', 'El Documento no Contiene Información', 0 );
     end;
end;

procedure TDialogAbrirArchivo_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     Action := caHide;
end;

procedure TDialogAbrirArchivo_DevEx.SetBotonDefault( const lOK: Boolean );
begin
     if lOk then
     begin
          OK_DevEx.Default := True;
          Cancelar_DevEx.Default := False;
          ActiveControl := OK_DevEx;
     end
     else
     begin
          if FVerBitacora then
          begin
               OK_DevEx.Default := False;
               Cancelar_DevEx.Default := True;
               ActiveControl := Cancelar_DevEx;
          end
          else
          begin
               OK_DevEx.Default := True;
               ActiveControl := OK_DevEx;
          end;
     end;
end;

function TDialogAbrirArchivo_DevEx.ShowDialogoAbrirArchivo( const lMostrar: Boolean ): Integer;
begin
     Result := 0;
     if lMostrar then
     begin
          Application.Restore;
          ShowModal;
          if ( ModalResult = mrYes ) then
             Result := 1;
     end;
end;


end.
