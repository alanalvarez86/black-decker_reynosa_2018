// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://revolutionsrv/timbres/Timbres.asmx?WSDL
// Encoding : utf-8
// Version  : 1.0
// (04/12/2013 03:36:22 p.m. - 1.33.2.5)
// ************************************************************************ //

unit Timbres;

interface

uses  
	InvokeRegistry, SOAPHTTPClient, SOAPHTTPTrans, OpConvertOptions, Types, XSBuiltIns,  IdHTTP, IdIOHandlerSocket, IdSSLOpenSSL, wininet,
    OPConvert,FTimbramexClasses;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:boolean         - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : http://www.timbramex.com.mx/
  // soapAction: http://www.timbramex.com.mx/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : TimbresFiscalesSoap
  // service   : TimbresFiscales
  // port      : TimbresFiscalesSoap
  // URL       : http://revolutionsrv/timbres/Timbres.asmx
  // ************************************************************************ //
  TimbresFiscalesSoap = interface(IInvokable)
  ['{27C2D6A2-0A07-0A80-620C-B0413EC385BB}']

   {Uso }
    function  VerificarVersion(const version: TXSDecimal): TXSDecimal; stdcall;
    function  ContribuyenteGetManifiesto(const xml: WideString ): WideString; stdcall;
    function  ContribuyenteAgregar(const xml: WideString): WideString; stdcall;
    function  ContribuyenteCancelar(const xml: WideString): WideString; stdcall;
    function  ContribuyenteConsultar(const xml: WideString): WideString; stdcall;
    function  ContribuyenteActualizar(const xml: WideString): WideString; stdcall;
    function  Timbrar(const xml: WideString): WideString; stdcall;
    function  CancelarTimbre(const xml: WideString): WideString; stdcall;
    function  PeriodoGetRecibosImpresos(const xml: WideString): WideString; stdcall;
    function  PeriodoGetRecibosXML(const xml: WideString): WideString; stdcall;
    function  PeriodoEnviarCorreos(const xml: WideString): WideString; stdcall;
    function  FIELFileTest(const certificado: WideString; const llavePrivada: WideString): Boolean; stdcall;
    function  HelloWorld: WideString; stdcall;
    function  Echo(const value: WideString): WideString; stdcall;
    function  CuentaConsultaSaldo(const xml: WideString): WideString; stdcall;
    function  FacturaGetUUID (const xml: WideString): WideString; stdcall;
    function  CancelarTimbreInit(const xml: WideString): WideString; stdcall;
    function  CancelarTimbreStatus(const xml: WideString): WideString; stdcall;
    function  ContribuyenteValidar( const xml: WideString ): WideString; stdcall;
    function  VerificarVersionXml(const version: TXSDecimal): WideString; stdcall;
    function  ConciliarNominas(const xml: WideString): WideString; stdcall;
    function  AuditarConceptosTRESS(const xml: WideString): WideString; stdcall;


    {Definicion
    function  VerificarVersion(const version: TXSDecimal): TXSDecimal; stdcall;
    function  ContribuyenteGetManifiesto(const xml: WideString): WideString; stdcall;
    function  ContribuyenteTieneCSD(const xml: WideString): WideString; stdcall;
    function  ContribuyenteAgregar(const xml: WideString): WideString; stdcall;
    function  ContribuyenteConsultar(const xml: WideString): WideString; stdcall;
    function  ContribuyenteCancelar(const xml: WideString): WideString; stdcall;
    function  ContribuyenteActualizar(const xml: WideString): WideString; stdcall;
    function  CuentaAgregar(const xml: WideString): WideString; stdcall;
    function  Timbrar(const xml: WideString): WideString; stdcall;
    function  CancelarTimbre(const xml: WideString): WideString; stdcall;
    function  PeriodoGetRecibosImpresos(const xml: WideString): WideString; stdcall;
    function  PeriodoGetRecibosXML(const xml: WideString): WideString; stdcall;
    function  FIELFileTest(const certificado: WideString; const llavePrivada: WideString): Boolean; stdcall;
    procedure Esperar(const segundos: Integer); stdcall;
    function  EsperarConTexto(const segundos: Integer; const datos: WideString): WideString; stdcall;
    function  HelloWorld: WideString; stdcall;
    function  Echo(const value: WideString): WideString; stdcall;
    }
  end;


  function GetTimbresFiscalesSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO = nil ): TimbresFiscalesSoap;
  function GetTimbresFiscalesSoap2(UseWSDL: Boolean; Addr: string;var wsConfig : TWebServiceConfig;  HTTPRIO: THTTPRIO = nil ): TimbresFiscalesSoap;

implementation

function GetTimbresFiscalesSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): TimbresFiscalesSoap;
const
  defWSDL = 'http://ws.recibodigital.mx/ReciboDigitalWS/timbres.asmx?WSDL';
  defURL  = 'http://ws.recibodigital.mx/ReciboDigitalWS/timbres.asmx';
  defSvc  = 'TimbresFiscales';
  defPrt  = 'TimbresFiscalesSoap';
var
  RIO: THTTPRIO;
  wsConfig : TWebServiceConfig;
begin
  Result := nil;
  wsConfig  :=   TWebServiceConfig.Create;

  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try

    RIO.HTTPWebNode.InvokeOptions := RIO.HTTPWebNode.InvokeOptions + [ soIgnoreInvalidCerts ];
    RIO.Converter.Options := RIO.Converter.Options + [soUTF8InHeader];
    RIO.HTTPWebNode.UseUTF8InHeader := True;

    Result := (RIO as TimbresFiscalesSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;




     //RIO.Converter.Options.soUTF8InHeader to True
    wsConfig.FHTTPRIO := RIO;
    RIO.OnBeforeExecute :=  wsConfig.HTTPRIOBeforeExecute;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;



function GetTimbresFiscalesSoap2(UseWSDL: Boolean; Addr: string;var wsConfig : TWebServiceConfig;  HTTPRIO: THTTPRIO = nil ): TimbresFiscalesSoap;
const
  defWSDL = 'http://ws.recibodigital.mx/ReciboDigitalWS/timbres.asmx?WSDL';
  defURL  = 'http://ws.recibodigital.mx/ReciboDigitalWS/timbres.asmx';
  defSvc  = 'TimbresFiscales';
  defPrt  = 'TimbresFiscalesSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  wsConfig  :=   TWebServiceConfig.Create;

  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try

    RIO.HTTPWebNode.InvokeOptions := RIO.HTTPWebNode.InvokeOptions + [ soIgnoreInvalidCerts ];
    RIO.Converter.Options := RIO.Converter.Options + [soUTF8InHeader];
    RIO.HTTPWebNode.UseUTF8InHeader := True;

    Result := (RIO as TimbresFiscalesSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;




     //RIO.Converter.Options.soUTF8InHeader to True
    wsConfig.FHTTPRIO := RIO;
    RIO.OnBeforeExecute :=  wsConfig.HTTPRIOBeforeExecute;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(TimbresFiscalesSoap), 'http://www.timbramex.com.mx/', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(TimbresFiscalesSoap), 'http://www.timbramex.com.mx/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(TimbresFiscalesSoap), ioDocument);


end. 