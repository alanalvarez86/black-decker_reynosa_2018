// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://devwebsrv01/recolector/entrada.asmx?WSDL
// Encoding : utf-8
// Version  : 1.0
// (9/30/2015 5:47:16 PM - 1.33.2.5)
// ************************************************************************ //

unit Recolector;

interface

uses InvokeRegistry, SOAPHTTPClient, SOAPHTTPTrans, Types, XSBuiltIns, OpConvertOptions, OPConvert, IdHTTP, IdIOHandlerSocket, IdSSLOpenSSL, wininet ,
FTimbramexClasses;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:decimal         - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : http://www.tress.com.mx/recolector
  // soapAction: http://www.tress.com.mx/recolector/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // binding   : EntradaSoap
  // service   : Entrada
  // port      : EntradaSoap
  // URL       : http://devwebsrv01/recolector/entrada.asmx
  // ************************************************************************ //
  EntradaSoap = interface(IInvokable)
  ['{A13D795E-AD84-EFE0-9C12-FC59AFECCE1D}']
    function  Autorizar(const xml: WideString): WideString; stdcall;
    function  Registrar(const xml: WideString): WideString; stdcall;
    function  HelloWorld: WideString; stdcall;
    function  Echo(const value: WideString): WideString; stdcall;
    function  VerificarVersion(const version: TXSDecimal): TXSDecimal; stdcall;
    procedure Esperar(const segundos: Integer); stdcall;
    function  EsperarConTexto(const segundos: Integer; const datos: WideString): WideString; stdcall;
  end;

function GetEntradaSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): EntradaSoap;


implementation

function GetEntradaSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): EntradaSoap;
const
  defWSDL = 'http://devwebsrv01/recolector/entrada.asmx?WSDL';
  defURL  = 'http://devwebsrv01/recolector/entrada.asmx';
  defSvc  = 'Entrada';
  defPrt  = 'EntradaSoap';
var
  RIO: THTTPRIO;
  wsConfig : TWebServiceConfig;
begin
  Result := nil;
  wsConfig  :=   TWebServiceConfig.Create;

  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try

    RIO.HTTPWebNode.InvokeOptions := RIO.HTTPWebNode.InvokeOptions + [ soIgnoreInvalidCerts ];
    RIO.Converter.Options := RIO.Converter.Options + [soUTF8InHeader];
    RIO.HTTPWebNode.UseUTF8InHeader := True;

    Result := (RIO as EntradaSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;




     //RIO.Converter.Options.soUTF8InHeader to True
    wsConfig.FHTTPRIO := RIO;
    RIO.OnBeforeExecute :=  wsConfig.HTTPRIOBeforeExecute;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(EntradaSoap), 'http://www.tress.com.mx/recolector', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(EntradaSoap), 'http://www.tress.com.mx/recolector/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(EntradaSoap), ioDocument);



end.