unit FEditTipoSAT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaNumero, Mask,
  ZetaEdit, ZetaSmartLists, ZetaKeyCombo, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  dxSkinsCore,  TressMorado2013,dxSkinsdxBarPainter;

type
  TEditTipoSAT = class(TBaseEdicion_DevEx)
    DBCodigoLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    DBDescripcionLBL: TLabel;
    DBInglesLBL: TLabel;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    Label2: TLabel;
    Label1: TLabel;
    TB_TEXTO: TDBEdit;
    Label3: TLabel;
    TB_SAT_CLA: TZetaDBKeyCombo;
    Label4: TLabel;
    TB_SAT_NUM: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }   
  protected
     procedure Connect; override;
     procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditTipoSAT: TEditTipoSAT;

implementation

uses DCatalogos,
     ZetaBuscador,
     ZetaCommonClasses,
     ZAccesosTress,
     dSistema, DCliente, ZetaCommonTools;

{$R *.DFM}

procedure TEditTipoSAT.FormCreate(Sender: TObject);
begin
     inherited;

     IndexDerechos := ZAccesosTress.D_TAB_OFI_SAT_TIPOS_CONCEPTO;
     HelpContext:= H70079_TiposConceptosSAT;
     FirstControl := TB_CODIGO;
end;

procedure TEditTipoSAT.Connect;
begin
     with dmCatalogos do
     begin
          DataSource.DataSet := cdsTiposSAT;
          Self.Caption := cdsTiposSAT.LookupName;
     end;
end;

procedure TEditTipoSAT.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', Self.Caption, 'TB_CODIGO',  dmCatalogos.cdsTiposSAT );
end;

end.
