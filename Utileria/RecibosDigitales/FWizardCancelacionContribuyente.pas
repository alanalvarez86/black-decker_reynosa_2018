unit FWizardCancelacionContribuyente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ExtCtrls, ZetaKeyLookup, ZetaDBTextBox, Buttons,
  IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent, ZetaCommonClasses, ZetaCommonTools, ZetaClientTools,
  Mask, DBCtrls, cxMemo, cxRichEdit, DTablas, FTimbramexClasses,
  TressMorado2013, ZetaKeyLookup_DevEx, dxGDIPlusClasses, cxImage;

type
  TWizardCancelacionContribuyente = class(TdxWizardControlForm)
    dataSourceContribuyente: TDataSource;
    OpenDialog: TOpenDialog;
    Label3: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    DataSourceCuentas: TDataSource;
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageRazonSocial: TdxWizardControlPage;
    dxWizardControlPageSeleccionRazonSocial: TdxWizardControlPage;
    Label1: TLabel;
    Label18: TLabel;
    Label6: TLabel;
    RS_RFC: TDBEdit;
    RS_LEGAL: TDBEdit;
    RS_NOMBRE: TDBEdit;
    cxGroupBox3: TcxGroupBox;
    Label13: TLabel;
    Label5: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label7: TLabel;
    RS_CALLE: TDBEdit;
    RS_NUMEXT: TDBEdit;
    RS_NUMINT: TDBEdit;
    RS_CIUDAD: TDBEdit;
    RS_CODPOST: TDBEdit;
    RS_ENTIDAD: TZetaDBKeyLookup_DevEx;
    dxWizardControlPageCuenta: TdxWizardControlPage;
    dxWizardControlPageRegistrar: TdxWizardControlPage;
    cxButton1: TcxButton;
    ZetaDBTextBox3: TZetaDBTextBox;
    lblDI_IP: TLabel;
    Panel1: TPanel;
    cxLabel1: TcxLabel;
    cxImage1: TcxImage;
    RS_CODIGO: TZetaKeyLookup_DevEx;
    Label10: TLabel;
    CT_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    Label9: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    cxProgressBarEnvio: TcxProgressBar;
    cxLabel4: TcxLabel;
    cxStatus: TcxLabel;
    RS_CURP: TDBEdit;
    Label4: TLabel;
    RS_RL_RFC: TDBEdit;
    Label14: TLabel;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CT_CODIGOValidLookup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxWizardControl1PageChanging(Sender: TObject;
      ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
  private
    { Private declarations }
   // procedure EnviarRecibos;r
     function CancelarContribuyente : boolean;
  public
    { Public declarations }

  end;


var
  WizardRegistroContribuyentec: TWizardCancelacionContribuyente;


implementation

uses
FTimbramexHelper,{ZHelpContext,}DCliente;

{$R *.dfm}

procedure TWizardCancelacionContribuyente.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin

     try

        timbresSoap := nil;

        if ( GetTimbradoServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
           if ( timbresSoap <> nil ) then
           begin
				crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) );
			  //sEchoResponse :=  					  timbresSoap.Echo(                 TESTING_MESSAGE    );
				FreeAndNil( crypt );				
                ZInformation( Self.Caption, 'Hay Conectividad con el Servidor de Timbrado' , 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, K_SIN_CONECTIVIDAD , 0);
     end;


end;

procedure TWizardCancelacionContribuyente.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
  begin
         if dxWizardControl1.Buttons.Cancel.Caption <> 'Salir'  then
         begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de cancelar y salir del proceso?', 0, mbNo ) then
               begin
                    ModalResult := mrCancel;
               end;
         end
         else
         begin
              ModalResult := mrCancel;
         end;
  end
  else if AKind = wcbkFinish then
  begin
               if ZetaDialogo.ZConfirm( self.Caption, '�Est� seguro de seguir con el proceso y cancelar el contribuyente?', 0, mbNo ) then
               begin
                    if CancelarContribuyente then
                    begin
                         ZInformation( Self.Caption, Format( 'Al contribuyente %s le fue Cancelado su Registro en el Sistema de Timbrado',
                                       [dmInterfase.cdsRSocial.FieldByName('RS_NOMBRE').AsString] ) , 0);
                         ModalResult := mrOk;
                    end;
               end;
  end;

end;


procedure TWizardCancelacionContribuyente.FormCreate(Sender: TObject);
const
     K_QUERY_FILTRO_RAZONES = 'RS_CONTID >  ';
var
     iSocial : integer;
     Valor : integer;
begin
     HelpContext:= H32131_CancelarContribuyente;

     dmInterfase.cdsRSocial.Conectar;
     Valor := 0;
     RS_CODIGO.LookupDataset := dmInterfase.cdsRSocial;

     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;

     dmTablas.cdsEstado.Conectar;
     RS_ENTIDAD.LookupDataSet := dmTablas.cdsEstado;
     CT_CODIGO.LookupDataset := dmInterfase.cdsCuentasTimbramex;
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
end;

procedure TWizardCancelacionContribuyente.CT_CODIGOValidLookup(
  Sender: TObject);
begin
     DataSourceCuentas.DataSet := dmInterfase.cdsCuentasTimbramex;
end;

function TWizardCancelacionContribuyente.CancelarContribuyente : boolean;


//Metodo que se iria a una Helper Class
  function   CancelarContribuyenteFromServer : boolean;
  const
       TESTING_MESSAGE = 'PRUEBA DE CONEXION';
       K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
       K_ERR_TIMBRAMEX = 'Notificaci�n del Sistema de Timbrado ';
       K_NOPOSIBLE_TIMBRAMEX = 'No fue posible cancelar registro de contribuyente: ';
  var
     sEchoResponse : string;
     timbresSoap : TimbresFiscalesSoap;
     sPeticion, sRespuesta:  string;
     oCursor: TCursor;
     sServer : string;
     respuestaTimbramex : TRespuestaStruct;
	 crypt : TWSTimbradoCrypt; 
  begin

     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     try


          timbresSoap := nil;

          cxProgressBarEnvio.Properties.Min := 0.00;
          cxProgressBarEnvio.Properties.Max := 100.00;  // puede ser por la cantidad de empleados en nomina
          cxProgressBarEnvio.Position := 0.00;
          cxProgressBarEnvio.Update;

          if ( GetTimbradoServer <> '' ) then
          begin
             cxStatus.Caption := 'Conectando a Servidor...';
             timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  GetTimbradoURL );
             if ( timbresSoap <> nil ) then
             begin
                  cxStatus.Caption := 'Cancelando Registro de Contribuyente...';
                  cxProgressBarEnvio.Position := 20.00;  cxProgressBarEnvio.Update;
                  sPeticion := dmInterfase.GetContribuyenteCancelar;

				  crypt := TWSTimbradoCrypt.Create;                				 
				  sRespuesta := crypt.Desencriptar(  timbresSoap.ContribuyenteCancelar( crypt.Encriptar( sPeticion ) ) );
                  //sRespuesta := timbresSoap.ContribuyenteCancelar( sPeticion );
				  FreeAndNil( crypt); 
				  

                  cxProgressBarEnvio.Position := 80.00;  cxProgressBarEnvio.Update;
                  respuestaTimbramex := FTimbramexHelper.GetAgregar( sRespuesta );

                  if respuestaTimbramex.Resultado.Errores then
                  begin
                     cxProgressBarEnvio.Position := 0.00;  cxProgressBarEnvio.Update;
                     zError( K_ERR_TIMBRAMEX , K_NOPOSIBLE_TIMBRAMEX  + char(10)+char(13)+ respuestaTimbramex.Bitacora.Eventos.Text, 0);
                     dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta  );
                  end
                  else
                  begin
                       with dmInterfase.cdsRSocial do
                       begin
                            dmInterfase.GrabaContribuyente( FieldByName('RS_CODIGO').AsString, CT_CODIGO.Llave,  0);
                       end;

                        cxProgressBarEnvio.Position := 100.00;  cxProgressBarEnvio.Update;
                       Result := TRUE;
                  end;
             end;
          end
          else
          begin
             zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
             dmInterfase.EscribeBitacoraTRESS(Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado ' );
          end;
       except on Error: Exception do
              begin
                 zError( Self.Caption, K_SIN_CONECTIVIDAD, 0);
                 dmInterfase.EscribeBitacoraTRESS(Self.Caption, TIMBRADO_NOPOSIBLE_TIMBRAMEX + ' '+Error.Message  +   char(10)+char(13)+ 'Env�o:' +char(10)+char(13)+  sPeticion   + 'Recepci�n:' +char(10)+char(13)+  sRespuesta  );
              end;
       end;


       Screen.Cursor := oCursor;

  end;

var
   oCursor: TCursor;
begin

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

     Result := CancelarContribuyenteFromServer;

     Screen.Cursor := oCursor;
end;


procedure TWizardCancelacionContribuyente.FormShow(Sender: TObject);
begin
     if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;
end;

procedure TWizardCancelacionContribuyente.dxWizardControl1PageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
var
   lContinuar : Boolean;
begin
     if(dmCliente.EsTRESSPruebas) then
     begin
          DxWizardControl1.buttons.Finish.Enabled := False;
     end;

     if ( AnewPage = dxWizardControlPageSeleccionRazonSocial ) { and  not ExisteEnTimbrado} then
     begin
          AAllow := TRUE;
     end;

     if ( AnewPage = dxWizardControlPageRazonSocial ) { and  not ExisteEnTimbrado} then
     begin
          AAllow := TRUE;
          if StrVacio( RS_CODIGO.Llave ) then
          begin
               zError( Self.Caption, 'El contribuyente no puede quedar vac�o.', 0  );
               AAllow := FALSE;
          end
          else
          begin
               dmInterfase.cdsRSocial.Locate('RS_CODIGO', RS_CODIGO.Llave, []);
          end;
     end;

     if ( AnewPage = dxWizardControlPageCuenta ) { and  not ExisteEnTimbrado} then
     begin
          AAllow := TRUE;
          lContinuar := true;
           if dmInterfase.GetCountTimbradasPorContribuyente > 0  then
           begin
                lContinuar := ZWarningConfirm( Self.Caption, 'La Raz�n Social '+ dmInterfase.cdsRsocial.FieldByName('RS_NOMBRE').AsString + ' tiene n�minas timbradas. '+ char(10) + char(13) + '  �Desea continuar con el proceso de Cancelaci�n de Contribuyente? ', 0, mbOK  )
           end;
           if not lContinuar then
           begin
                AAllow := false;
           end;
     end;


end;


end.
