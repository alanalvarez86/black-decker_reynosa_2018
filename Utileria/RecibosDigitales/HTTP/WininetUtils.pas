unit WininetUtils;

interface

uses Windows, WinInet,Classes, SysUtils, Variants, ZetaClientDataSet, ZetaCommonClasses;

type



TProgressCallback = function (FileName : string; ATotalSize, ATotalRead, AStartTime: DWORD): Boolean;
TProgressPaqueteCallback = procedure (sMensaje : string);
TProgressConciliacionTimbradoCallback = procedure (sMensaje : string; iAvance: Integer; oDataSetUpdate: TZetaClientDataSet; oParams: TZetaParams) of object;


function DownloadFileWininet(const URL: string; Path: string;  const ACallback: TProgressCallback = nil;  const AliasPath : string = '' ) : boolean;

implementation


function StrToIntDef(const S: string; Default: Integer): Integer;
var
  E: Integer;
begin
  Val(S, Result, E);
  if E <> 0 then Result := Default;
end;


  function _HttpQueryInfo(AFile: HINTERNET; AInfo: DWORD): string;
  var
    infoBuffer: PChar;
    dummy: DWORD;
    err, bufLen: DWORD;
    res: LongBool;
  begin
    Result := '';
    bufLen := 0;
    dummy := 0;
    infoBuffer := nil;
    res := HttpQueryInfo(AFile, AInfo, infoBuffer, bufLen, dummy);
    if not res then
    begin
      // Probably working offline, or no internet connection.
      err := GetLastError;
      if err = ERROR_HTTP_HEADER_NOT_FOUND then
      begin
        // No headers
      end else if err = ERROR_INSUFFICIENT_BUFFER then
      begin
        GetMem(infoBuffer, bufLen);
        try
          HttpQueryInfo(AFile, AInfo, infoBuffer, bufLen, dummy);
          Result := infoBuffer;
        finally
          FreeMem(infoBuffer);
        end;
      end;
    end;
  end;


function DownloadFileWininet(const URL: string; Path: string;  const ACallback: TProgressCallback = nil; const AliasPath : string = '') : boolean;
const
  BLOCK_SIZE = 1024;
var
  BytesRead, Totalbytes, Totalread, StartTime: DWORD;
  BytesWritten: DWORD;
  FileHandle: THandle;
  URLHandle: HINTERNET;
  InetHandle: HINTERNET;
  Buffer: array[0..BLOCK_SIZE - 1] of Byte;

begin
    Result := FALSE;
   Totalread := 0;

  InetHandle := InternetOpen(PChar(URL), INTERNET_OPEN_TYPE_PRECONFIG, nil,
    nil, 0);
  if not Assigned(InetHandle) then
    RaiseLastOSError;
  try
    StartTime := GetTickCount;

    URLHandle := InternetOpenUrl(InetHandle, PChar(URL), nil, 0,
      INTERNET_FLAG_NO_UI or INTERNET_FLAG_RELOAD, 0);
    if not Assigned(URLHandle) then
      RaiseLastOSError;
    try

        Totalbytes := StrToIntDef(_HttpQueryInfo(URLHandle, HTTP_QUERY_CONTENT_LENGTH), 0);

      FileHandle := CreateFile(PChar(Path), GENERIC_WRITE, FILE_SHARE_WRITE,
        nil, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
      if FileHandle = INVALID_HANDLE_VALUE then
        RaiseLastOSError;
      try



        repeat
          if not InternetReadFile(URLHandle, @Buffer, BLOCK_SIZE, BytesRead) or
            not WriteFile(FileHandle, Buffer[0], BytesRead, BytesWritten, nil)
          then
            RaiseLastOSError;

          Inc(Totalread, bytesRead);
          if Assigned(ACallback) then
          begin
            if not ACallback( AliasPath, TotalBytes, Totalread, StartTime) then Break;
          end;
          //Sleep(10); 
        until
          BytesRead = 0;

        Result := TRUE;
      finally
        CloseHandle(FileHandle);
      end;
    finally
      InternetCloseHandle(URLHandle);
    end;
  finally
    InternetCloseHandle(InetHandle);
  end;
end;


end.
