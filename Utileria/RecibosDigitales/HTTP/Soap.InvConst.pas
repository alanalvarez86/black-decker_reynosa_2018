{*******************************************************}
{                                                       }
{                Delphi Runtime Library                 }
{              Pascal Invoker Support                   }
{                                                       }
{ Copyright(c) 1995-2013 Embarcadero Technologies, Inc. }
{                                                       }
{*******************************************************}

unit Soap.InvConst;

interface

resourcestring
  SClassNotRegistered = 'Class not registered';
  SIntfNotRegistered = 'Interface %s not registered';

implementation

end.
