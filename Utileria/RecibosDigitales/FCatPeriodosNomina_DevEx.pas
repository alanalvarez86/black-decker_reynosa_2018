unit FCatPeriodosNomina_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions, ZetaDialogo;

type
  TCatPeriodosNomina_DevEx = class(TBaseGridLectura_DevEx)
    gridPeriodosDBTableView1Column10: TcxGridDBColumn;
    gridPeriodosDBTableView1Column9: TcxGridDBColumn;
    gridPeriodosDBTableView1Column2: TcxGridDBColumn;
    gridPeriodosDBTableView1Column3: TcxGridDBColumn;
    gridPeriodosDBTableView1Column6: TcxGridDBColumn;
    gridPeriodosDBTableView1Column7: TcxGridDBColumn;
    DataSourcePeriodos: TDataSource;
    PopupMenu2: TPopupMenu;
    imbrarNmina1: TMenuItem;
    omarRecibos1: TMenuItem;
    omarRecibosXEmpleado: TMenuItem;
    CancelarTimbrado1: TMenuItem;
    EnviarRecibos1: TMenuItem;
    ProcesosSmallImages: TcxImageList;
    _P_TimbrarNomina: TAction;
    _P_EnviarTimbrados: TAction;
    _P_CancelarTimbradoNomina: TAction;
    _P_GetTimbrados: TAction;
    _P_GetTimbradosXEmpleado: TAction;
    ZetaDBGridDBTableViewColumn1: TcxGridDBColumn;
    TIMBRADAS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure gridPeriodosDBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure DataSourcePeriodosDataChange(Sender: TObject; Field: TField);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ZetaDBGridDBTableViewFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
          procedure _P_TimbrarNominaExecute(Sender: TObject);
    procedure _P_CancelarTimbradoNominaExecute(Sender: TObject);
    procedure _P_GetTimbradosExecute(Sender: TObject);
    procedure _P_GetTimbradosXEmpleadoExecute(Sender: TObject);
    procedure _P_EnviarTimbradosExecute(Sender: TObject);
  private
    { Private declarations }
    procedure Refrescar;
    procedure AsignaValoresActivos;

  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatPeriodosNomina_DevEx: TCatPeriodosNomina_DevEx;

implementation

uses dCatalogos,
     dInterfase,
     dCliente,
     dProcesos,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaClientDataSet,
     ZGridModeTools,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatPeriodosNomina_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Refrescar;
     CanLookup := True;
     HelpContext:= H32138_PeriodosNomina;
end;

procedure TCatPeriodosNomina_DevEx.Refrescar;
begin
     dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
     DataSource.DataSet :=  dmInterfase.cdsPeriodosAfectadosTotal;
end;

procedure TCatPeriodosNomina_DevEx.Connect;
begin
     inherited;
     with dmInterfase do
     begin
          cdsPeriodosAfectadosTotal.Conectar;
          DataSource.DataSet:= cdsPeriodosAfectadosTotal;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPeriodosNomina_DevEx.gridPeriodosDBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
  inherited;
    try
 ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
    Except
    end;
end;

procedure TCatPeriodosNomina_DevEx.Refresh;
begin
     inherited;
     dmInterfase.cdsPeriodosAfectadosTotal.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatPeriodosNomina_DevEx.Agregar;
begin
     ZInformation('Periodos de N�mina','No se permite agregar en Periodos de N�mina.',0);
end;

procedure TCatPeriodosNomina_DevEx.AsignaValoresActivos;
var
   sTipoPeriodo : String;
   sNumeroPeriodo : String;
begin
  try
       with dmCliente do
       begin
            with GetDatosPeriodoActivo do
            begin
                  with ZetaDBGridDBTableView do
                  begin
                          if DataController.Values[DataController.FocusedRecordIndex,1] <> null then
                          begin
                               sNumeroPeriodo := DataController.Values[DataController.FocusedRecordIndex,1];
                          end;
                  end;
                 sTipoPeriodo := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );   //TIPO PERIODO
                 textoValorActivo1.Caption := sTipoPeriodo +': '+ sNumeroPeriodo;
            end;
       end;
  Except
  end;
end;

procedure TCatPeriodosNomina_DevEx.Borrar;
begin
ZInformation('Periodos de N�mina','No se permite borrar en Periodos de N�mina.',0);
end;

procedure TCatPeriodosNomina_DevEx.Modificar;
begin
     ZInformation('Periodos de N�mina','No se permite modificar en Periodos de N�mina.',0);
end;

procedure TCatPeriodosNomina_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  try
     ZetaDBGridDBTableView.ApplyBestFit(); //CAUSA ACCES VIOLATION AL FILTRAR
  Except

  end;

end;

procedure TCatPeriodosNomina_DevEx.DataSourcePeriodosDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TCatPeriodosNomina_DevEx.DoLookup;
begin
     inherited;
      ZetaBuscador_DevEx.BuscarCodigo( 'N�mero', 'Periodos Afectados', 'Numero', dmInterfase.cdsPeriodosAfectadosTotal );
end;


procedure TCatPeriodosNomina_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('PE_NUMERO'), K_SIN_TIPO , '', skCount);
  ZetaDBGridDBTableView.ApplyBestFit();
  textoValorActivo1.Visible := true;
  PanelIdentifica.Visible := true;
  ValorActivo1.Visible := true;
  AsignaValoresActivos;
end;

procedure TCatPeriodosNomina_DevEx.ZetaDBGridDBTableViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
    //dmCatalogos.cdsTiposSAT.Modificar;
end;

procedure TCatPeriodosNomina_DevEx.ZetaDBGridDBTableViewFocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
var
   sPeriodo : string;
   iPeriodo : integer;
   lOk : Boolean;
begin
  inherited;
  try
    AsignaValoresActivos;
      iPeriodo := dmInterfase.cdsPeriodosAfectadosTotal.FieldByName( 'PE_NUMERO' ).AsInteger;
      if iPeriodo > 0 then
      begin
           if  (dmInterfase.cdsPeriodosAfectadosTotal <> nil) and not( dmInterfase.cdsPeriodosAfectadosTotal.state in [dsInactive] ) then
            begin
                  sPeriodo := inttostr(iPeriodo);

                  lOk := dmCliente.SetPeriodoNumeroEspecialTimbra( iPeriodo );
                  if lOk then
                  begin
                       dmInterfase.CambioNumeroPeriodoDesdeGridShell(iPeriodo);
                       dmInterfase.RefrescaPeriodoActivosShell(true);
                  end;
            end;
      end;
  Except

  end;
end;

procedure TCatPeriodosNomina_DevEx._P_CancelarTimbradoNominaExecute(
  Sender: TObject);
begin
     dmInterfase._P_CancelarTimbradoNominaShell;
end;

procedure TCatPeriodosNomina_DevEx._P_EnviarTimbradosExecute(Sender: TObject);
begin
     dmInterfase._P_EnviarTimbradosShell;
end;

procedure TCatPeriodosNomina_DevEx._P_GetTimbradosExecute(Sender: TObject);
begin
     dmInterfase._P_GetTimbradosShell;
end;

procedure TCatPeriodosNomina_DevEx._P_GetTimbradosXEmpleadoExecute(
  Sender: TObject);
begin
     dmInterfase._P_GetTimbradosXEmpleadoShell;
end;

procedure TCatPeriodosNomina_DevEx._P_TimbrarNominaExecute(Sender: TObject);
begin
     dmInterfase._P_TimbrarNominaShell;
end;

end.
