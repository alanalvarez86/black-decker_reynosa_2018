unit FEditCatConceptos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Buttons, DBCtrls, StdCtrls, ExtCtrls, Mask, dbClient,
  ZetaKeyCombo, ComCtrls, ZetaNumero, ZetaCommonLists,
  ZetaCommonClasses, ZetaTipoEntidad, ZetaClientDataSet,
  ZetaDBTextBox, ZetaEdit, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, ZetaKeyLookup_DevEx, cxPC,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, dxBarBuiltInMenu;

type
  TEditCatConceptos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    CO_NUMEROLbl: TLabel;
    LblDescrip: TLabel;
    PageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    TabCalculo: TcxTabSheet;
    TabExento: TcxTabSheet;
    LblCondicion: TLabel;
    CO_ACTIVO: TDBCheckBox;
    CO_CALCULA: TDBCheckBox;
    LblTipo: TLabel;
    CO_TIPO: TZetaDBKeyCombo;
    TabIndividual: TcxTabSheet;
    LblFormulaCalc: TLabel;
    CO_FORMULA: TcxDBMemo;
    LblFormulaIndiv: TLabel;
    CO_IMP_CAL: TcxDBMemo;
    LblFormulaExento: TLabel;
    CO_X_ISPT: TcxDBMemo;
    CO_QUERY: TZetaDBKeyLookup_DevEx;
    GBPercepcion: TGroupBox;
    CO_MENSUAL: TDBCheckBox;
    CO_A_PTU: TDBCheckBox;
    CO_G_ISPT: TDBCheckBox;
    CO_G_IMSS: TDBCheckBox;
    GBImprimir: TGroupBox;
    TabRecibo: TcxTabSheet;
    LblFormulaRecibo: TLabel;
    CO_RECIBO: TcxDBMemo;
    CO_IMPRIME: TDBCheckBox;
    CO_LISTADO: TDBCheckBox;
    CO_NUMERO: TZetaDBNumero;
    CO_DESCRIP: TDBEdit;
    SBco_x_ispt: TcxButton;
    SBco_imp_cal: TcxButton;
    SBco_recibo: TcxButton;
    SBCO_FORMULA: TcxButton;
    Label12: TLabel;
    CO_SUB_CTA: TDBEdit;
    CO_CAMBIA: TDBCheckBox;
    TabNotas: TcxTabSheet;
    CO_NOTA: TcxDBMemo;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    btnAgregarDocumento: TcxButton;
    btnBorraDocumento: TcxButton;
    btnEditarDocumento: TcxButton;
    btnVerDocumento: TcxButton;
    lblDescripcionArchivo: TLabel;
    lblTipoArchivo: TLabel;
    TabSheet1: TcxTabSheet;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    GpoAcceso: TGroupBox;
    TodosUsuarios: TRadioButton;
    AlgunosUsuarios: TRadioButton;
    lblMonto1: TLabel;
    CO_LIM_INF: TZetaDBNumero;
    CO_LIM_SUP: TZetaDBNumero;
    lblMonto: TLabel;
    CO_VER_INF: TDBCheckBox;
    CO_VER_SUP: TDBCheckBox;
    btnElegirGrupos: TcxButton;
    CO_GPO_ACC: TcxDBMemo;
    CO_SUMRECI: TDBCheckBox;
    CO_ISN: TDBCheckBox;
    LblFormulaAlterna: TLabel;
    CO_FRM_ALT: TcxDBMemo;
    SBCO_FRM_ALT: TcxButton;
    LblUsoNomina: TLabel;
    CO_USO_NOM: TZetaDBKeyCombo;
    bImpSubCuentas: TdxBarButton;
    TabTimbrado: TcxTabSheet;
    GroupBox4: TGroupBox;
    Label2: TLabel;
    lblTipoPositivo: TLabel;
    CO_SAT_CLP: TZetaDBKeyCombo;
    CO_SAT_TPP: TZetaDBKeyLookup_DevEx;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    lblTipoNegativo: TLabel;
    CO_SAT_CLN: TZetaDBKeyCombo;
    CO_SAT_TPN: TZetaDBKeyLookup_DevEx;
    GroupBox6: TGroupBox;
    Label6: TLabel;
    lblConceptoExento: TLabel;
    CO_SAT_EXE: TZetaDBKeyCombo;
    CO_SAT_CON: TZetaDBKeyLookup_DevEx;
    CheckBoxCO_TIMBRA: TDBCheckBox;
    procedure SBco_FORMULAClick(Sender: TObject);
    procedure SBco_x_isptClick(Sender: TObject);
    procedure SBco_imp_calClick(Sender: TObject);
    procedure SBco_reciboClick(Sender: TObject);
    procedure CO_TIPOChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure bImpSubCuentasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AgregarDocumentoBtnClick(Sender: TObject);
    procedure BorrarDocumentoBtnClick(Sender: TObject);
    procedure EditarDocumentoBtnClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnVerDocumentoClick(Sender: TObject);
    procedure AlgunosUsuariosClick(Sender: TObject);
    procedure TodosUsuariosClick(Sender: TObject);
    procedure btnElegirGruposClick(Sender: TObject);
    procedure CO_VER_INFClick(Sender: TObject);
    procedure CO_VER_SUPClick(Sender: TObject);
    procedure SBCO_FRM_ALTClick(Sender: TObject);
    procedure CO_SAT_CLPChange(Sender: TObject);
    procedure CO_SAT_CLNChange(Sender: TObject);
    procedure CO_SAT_EXEChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure CO_SAT_EXESelect(Sender: TObject);
    procedure CheckBoxCO_TIMBRAClick(Sender: TObject);
  private
    FNavegando:Boolean;
    procedure ControlesGBPercepciones( iValor : Integer );
    procedure ControlesGBImprimir( iValor : Integer );
    procedure SetControls;
    procedure ActivarControlesConcepto;
    procedure EnabledControlDocumento;
    procedure AgregaDocumento;
    procedure DialogoAgregaDocumento(const lAgregando: Boolean);
    procedure SetControlesAcceso;
    procedure SetControlGposAcceso;
    procedure ControlesVerificar;
    procedure SetControlesTimbrado(Field: TField);
    procedure SetControlesTimbradoUX(iCOT_SAT_CLP, iCOT_SAT_CLN,  iCON_SAT_EXE : integer );
    procedure EnfocarControlesTimbrado;
    procedure AplicarFiltroComboConcepto;
    procedure ActivarCompConceptosTimb;

  protected
    procedure HabilitaControles; override;
    procedure Connect; override;
    procedure DoLookup; override;
    procedure ImprimirForma; override;
    procedure EscribirCambios;override;
   public
  end;

var
  EditCatConceptos_DevEx: TEditCatConceptos_DevEx;
  ComboValue : integer;

implementation

uses dCatalogos,
     ZImprimeForma,
     ZAccesosTress,
     ZConstruyeFormula,
     ZImportaSubCuentas,
     ZetaBuscaEntero_DevEx,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaFilesTools,
     FCatConGposAcceso_DevEx,
     FEditDocumento_DevEx;

{$R *.DFM}

procedure TEditCatConceptos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_NOMINA_CONCEPTOS;
     PageControl.ActivePage := TabGenerales;
     FirstControl := CO_NUMERO;
     HelpContext:= H60621_Catalogo_conceptos;
     CO_QUERY.LookupDataset := dmCatalogos.cdsCondiciones;
end;

procedure TEditCatConceptos_DevEx.CheckBoxCO_TIMBRAClick(Sender: TObject);
begin
  inherited;
          ActivarCompConceptosTimb;
end;

procedure TEditCatConceptos_DevEx.ActivarCompConceptosTimb;
begin
  inherited;
        try
            if (CheckBoxCO_TIMBRA.Checked ) then
            begin
                 GroupBox4.Enabled := true;
                 GroupBox5.Enabled := true;
                 GroupBox6.Enabled := true;
                 CO_SAT_CLP.Enabled := true;
                 CO_SAT_TPP.Enabled := true;
                 CO_SAT_CLN.Enabled := true;
                 CO_SAT_TPN.Enabled := true;
                 CO_SAT_EXE.Enabled := true;
                 CO_SAT_CON.Enabled := true;
            end
            else
            begin

                 if (dmCatalogos.cdsConceptos.FieldByName('CO_TIPO').AsInteger in [1,4] ) and ( (dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLN').AsInteger > 0 ) or (dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLP').AsInteger > 0 ) ) then
                 begin
                       CO_SAT_CLP.Valor := 0;
                       CO_SAT_CLN.Valor := 0;
                       //CO_SAT_EXE.Valor := 0;

                       CO_SAT_TPP.Valor := 0;
                       CO_SAT_TPN.Valor := 0;
                       //CO_SAT_CON.Valor := 0;

                       CO_SAT_TPP.Llave := VACIO;
                       CO_SAT_TPN.Llave := VACIO;
                       //CO_SAT_CON.Llave := VACIO;
                 end;

                 GroupBox4.Enabled := false;
                 GroupBox5.Enabled := false;
                 GroupBox6.Enabled := false;
                 CO_SAT_CLP.Enabled := false;
                 CO_SAT_TPP.Enabled := false;
                 CO_SAT_CLN.Enabled := false;
                 CO_SAT_TPN.Enabled := false;
                 CO_SAT_EXE.Enabled := false;
                 CO_SAT_CON.Enabled := false;
            end;
        Except
        end;

end;

procedure TEditCatConceptos_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCondiciones.Conectar;
          cdsTiposSAT.Conectar;
          // se requiere conectar cdsConceptosLookup antes de de que cdsConceptos entre en edici�n
          // si la transf. de Data se hace despu�s, cdsConceptos regresa a dsBrowse y provoca error
          // El LookUp conecta tambi�n a cdsConceptos

          cdsConceptosLookUp.Conectar;
          CO_SAT_TPP.LookupDataset := cdsTiposSAT;
          CO_SAT_TPN.LookupDataset := cdsTiposSAT;
          CO_SAT_CON.LookupDataset := cdsConceptosLookUp;

          DataSource.DataSet:= cdsConceptos;

     end;

     EnabledControlDocumento;
end;

procedure TEditCatConceptos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     SetControls;
     FNavegando := False;
     AplicarFiltroComboConcepto;
     //ActivarCompConceptosTimb;

end;

procedure TEditCatConceptos_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     ZetaFilesTools.BorraArchivosTemporales;
end;

procedure TEditCatConceptos_DevEx.SetControls;
var
   lAutoEdit: Boolean;
begin
     //Se deshabilitan los botones si el usuario no tiene derecho de modificar.
     lAutoEdit := DataSource.AutoEdit;
     bImpSubCuentas.Enabled := lAutoEdit;
     SBCO_FORMULA.Enabled := lAutoEdit;
     SBco_x_ispt.Enabled := lAutoEdit;
     SBco_imp_cal.Enabled := lAutoEdit;
     SBco_recibo.Enabled := lAutoEdit;
end;

procedure TEditCatConceptos_DevEx.CO_TIPOChange(Sender: TObject);
begin
     inherited;
     ControlesGBPercepciones( CO_TIPO.Valor );
     ControlesGBImprimir( CO_TIPO.Valor );
     if (CO_TIPO.Valor = 1) or (CO_TIPO.Valor = 4) then
     begin
          CheckBoxCO_TIMBRA.State := cbChecked;
     end
     else
     begin
          CheckBoxCO_TIMBRA.State := cbUnchecked;
     end;
end;

procedure TEditCatConceptos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) OR ( Field.FieldName = 'CO_TIPO' ) then
     begin
          if ( Field = nil ) then
             ActivarControlesConcepto
          else
              ControlesGBPercepciones( DataSource.DataSet.FieldByName( 'CO_TIPO' ).AsInteger );
     end;

     if ( ( Field = nil ) or ( ( Field.FieldName = 'CO_VER_ACC' ) or
                               ( Field.FieldName = 'CO_VER_INF' ) or
                               ( Field.FieldName = 'CO_VER_SUP' ) )  )then
     begin
          if ( ( Field = nil )and ( not( DataSource.DataSet.State in [dsEdit,dsInsert]) ) )then
          begin
               FNavegando := True;
               SetControlGposAcceso;
          end
          else
              ControlesVerificar;
     end;

     EnabledControlDocumento;

     SetControlesTimbrado( Field );
end;

procedure TEditCatConceptos_DevEx.SetControlGposAcceso;
begin
     TodosUsuarios.Checked := ( NOT zStrToBool( DataSource.DataSet.FieldByName('CO_VER_ACC').AsString ) );
     AlgunosUsuarios.Checked := (not TodosUsuarios.Checked);
     FNavegando := False;
end;

procedure TEditCatConceptos_DevEx.ActivarControlesConcepto;
var
   lEnabled : Boolean;
begin
     lEnabled := ( DataSource.DataSet.State <> dsInactive ) and
                 ( DataSource.DataSet.FieldByName( 'CO_NUMERO' ).AsInteger < 1000 );
     LblDescrip.Enabled := lEnabled;
     CO_DESCRIP.Enabled := lEnabled;
     LblTipo.Enabled := lEnabled;
     CO_TIPO.Enabled := lEnabled;
     CO_ACTIVO.Enabled := lEnabled;
     CO_CALCULA.Enabled := lEnabled;
     LblCondicion.Enabled := lEnabled;
     CO_QUERY.Enabled := lEnabled;

     GBImprimir.Enabled := lEnabled;
     CO_IMPRIME.Enabled := lEnabled;
     CO_LISTADO.Enabled := lEnabled;

     LblFormulaCalc.Enabled := lEnabled;
     CO_FORMULA.Enabled := lEnabled;
     SBCO_FORMULA.Enabled := lEnabled;

     LblFormulaExento.Enabled := lEnabled;
     CO_X_ISPT.Enabled := lEnabled;
     SBco_x_ispt.Enabled := lEnabled;

     LblFormulaIndiv.Enabled := lEnabled;
     CO_IMP_CAL.Enabled := lEnabled;
     SBco_imp_cal.Enabled := lEnabled;

     LblFormulaRecibo.Enabled := lEnabled;
     CO_RECIBO.Enabled := lEnabled;
     SBco_recibo.Enabled := lEnabled;
     CO_CAMBIA.Enabled := lEnabled;

     CO_VER_INF.Enabled := lEnabled;
     CO_VER_SUP.Enabled := lEnabled;

     AlgunosUsuarios.Enabled := lEnabled;
     TodosUsuarios.Enabled := lEnabled;

     GpoAcceso.Enabled := lEnabled;

     if lEnabled then
     begin
          ControlesGBPercepciones( DataSource.DataSet.FieldByName( 'CO_TIPO' ).AsInteger );
          SetControls;    // Checar AutoEdit en botones
     end
     else
     begin
          GBPercepcion.Enabled := lEnabled;
          CO_MENSUAL.Enabled := lEnabled;
          CO_A_PTU.Enabled   := lEnabled;
          CO_G_IMSS.Enabled  := lEnabled;
          CO_G_ISPT.Enabled  := lEnabled;
          CO_ISN.Enabled     := lEnabled;
     end;
end;

procedure TEditCatConceptos_DevEx.ControlesGBImprimir( iValor : Integer );
var
   lConceptosApoyo : Boolean;
begin
     if Inserting then
     begin
          lConceptosApoyo:= ( eTipoConcepto ( iValor ) in [ coCaptura,coResultados,coCalculo ] );
          with dmCatalogos.cdsConceptos do
          begin
               FieldByName( 'CO_IMPRIME' ).AsString := zBoolToStr( NOT lConceptosApoyo );
               FieldByName( 'CO_LISTADO' ).AsString := zBoolToStr( NOT lConceptosApoyo );
          end;
     end;
end;

procedure TEditCatConceptos_DevEx.ControlesGBPercepciones( iValor : Integer );
var
   lEnabled : Boolean;
begin
     with GBPercepcion do
     begin
          lEnabled:= ( iValor = Ord( coPercepcion ) );
          Enabled := lEnabled OR ( iValor = Ord( coPrestacion ) );
          CO_MENSUAL.Enabled := lEnabled;
          CO_A_PTU.Enabled   := lEnabled;
          CO_G_IMSS.Enabled  := Enabled;
          CO_G_ISPT.Enabled  := Enabled;
          CO_ISN.Enabled := Enabled;
     end;
end;

procedure TEditCatConceptos_DevEx.HabilitaControles;
begin
     CO_NUMERO.Enabled:= Inserting;
     CO_NUMEROLbl.Enabled:= Inserting;
     if Inserting then
        FirstControl:= CO_NUMERO
     else
        FirstControl:= CO_DESCRIP;

     inherited;

     bImpSubCuentas.Enabled := not Editing and DataSource.AutoEdit;
end;

procedure TEditCatConceptos_DevEx.SBCO_FORMULAClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_FORMULA').AsString := GetFormulaConst( enNomina , CO_FORMULA.Lines.Text, CO_FORMULA.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos_DevEx.SBco_x_isptClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_X_ISPT').AsString := GetFormulaConst( enNomina , CO_X_ISPT.Lines.Text , CO_X_ISPT.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos_DevEx.SBco_imp_calClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_IMP_CAL').AsString := GetFormulaConst( enNomina , CO_IMP_CAL.Lines.Text, CO_IMP_CAL.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos_DevEx.SBco_reciboClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_RECIBO').AsString := GetFormulaConst( enNomina , CO_RECIBO.Lines.Text, CO_RECIBO.SelStart, evNomina  );
     end;
end;

procedure TEditCatConceptos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'N�mero', 'Concepto de N�mina', 'CO_NUMERO', dmCatalogos.cdsConceptos );
end;

procedure TEditCatConceptos_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enConcepto, dmCatalogos.cdsConceptos );
end;

procedure TEditCatConceptos_DevEx.OK_DevExClick(Sender: TObject);
var
    oConceptoPuente: TClientDataSet;
    sNroConceptoPuente: string;
    sNombreConceptoPuente: string;
    sTipoConceptoPuente, sClaseConceptoPuente: string;
begin
     inherited;
     try
     oConceptoPuente := TZetaClientDataSet.Create( self );
     sNombreConceptoPuente := VACIO;
     sNroConceptoPuente := VACIO;

    if ( (dmCatalogos.cdsConceptos.FieldByName('CO_TIPO').AsInteger in [1,4] ) and (( dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLN').AsInteger = 0  ) and (dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLP').AsInteger = 0 ) ) and (dmCatalogos.cdsConceptos.FieldByName('CO_TIMBRA').AsString = 'S' )and (dmCatalogos.cdsConceptos.FieldByName('CO_ACTIVO').AsString = 'S' ))  then
    begin
         dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLP').FocusControl;
         ZWarning( 'Conceptos de N�mina','El concepto no tiene configuraci�n de timbrado, verifique la clave SAT y tratamiento de exento que le corresponde.' , 0, mbOK);
         EnfocarControlesTimbrado;
    end;

     //CO_SAT_EXE = 3 ES TRATAMIENTO "Exento por Concepto" y CO_TIPO ( TIPO ) = 1 ES PERCEPCION Y 4 PRESTACION
     if ( (dmCatalogos.cdsConceptos.FieldByName('CO_SAT_EXE').AsInteger = 3) and ((dmCatalogos.cdsConceptos.FieldByName('CO_TIPO').AsInteger = 1) or (dmCatalogos.cdsConceptos.FieldByName('CO_TIPO').AsInteger = 4))) then
     begin
          if (CO_SAT_CON.Valor > 0) then
          begin
               oConceptoPuente.Data := dmCatalogos.cdsConceptos.Data;
               oConceptoPuente.Locate('CO_NUMERO', CO_SAT_CON.Valor, [] );
               sNombreConceptoPuente := oConceptoPuente.FieldByName('CO_DESCRIP').AsString;
               sTipoConceptoPuente :=  oConceptoPuente.FieldByName('CO_TIPO').AsString;
               sClaseConceptoPuente :=  oConceptoPuente.FieldByName('CO_SAT_CLP').AsString;

               if((sTipoConceptoPuente <> '3') or (sClaseConceptoPuente <> '3')) then
               begin
                    sNroConceptoPuente :=IntToStr(CO_SAT_CON.Valor);
                    ZWarning( Caption, 'El concepto puente #'+ sNroConceptoPuente +' '+sNombreConceptoPuente+' debe configurarse propiamente para ser tomado como Exento' , 0, mbOK);
               end;
          end;
     end;
     finally
           FreeAndNil( oConceptoPuente );
     end;
end;
procedure TEditCatConceptos_DevEx.EnfocarControlesTimbrado;
begin
     try
         if (dmCatalogos.cdsConceptos.FieldByName('CO_TIPO').AsInteger in [1,4] ) and (dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLN').AsInteger = 0 ) and (dmCatalogos.cdsConceptos.FieldByName('CO_SAT_CLP').AsInteger = 0 ) then
         begin
              PageControl.ActivePageIndex := TabTimbrado.PageIndex;

              CO_SAT_CLP.SetFocus;
         end;
     except
     end;

end;

procedure TEditCatConceptos_DevEx.bImpSubCuentasClick(Sender: TObject);
begin
     inherited;
     ZImportaSubCuentas.ImportaArchivo( TZetaClientDataSet( DataSource.DataSet ), 'CO_NUMERO', 'CO_SUB_CTA' );
end;


procedure TEditCatConceptos_DevEx.AgregarDocumentoBtnClick(Sender: TObject);
begin
     AgregaDocumento;
end;

procedure TEditCatConceptos_DevEx.BorrarDocumentoBtnClick(Sender: TObject);
begin
     with dmCatalogos do
     begin
          if ( ZetaDialogo.ZConfirm(Caption, '� Desea Borrar el Documento ' + cdsConceptos.FieldByName('CO_D_NOM').AsString + ' ?', 0, mbNo ) ) then
          begin
               dmCatalogos.BorraDocumento;
               EnabledControlDocumento;
          end;
     end;
end;

procedure TEditCatConceptos_DevEx.EditarDocumentoBtnClick(Sender: TObject);
begin
     inherited;
     if StrLleno( dmCatalogos.cdsConceptos.FieldByName('CO_D_NOM').AsString ) then
     begin
          DialogoAgregaDocumento( FALSE );
     end
     else ZetaDialogo.ZError( Caption, 'El Documento No Contiene Informaci�n', 0 );
end;

procedure TEditCatConceptos_DevEx.btnVerDocumentoClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.AbreDocumento;

end;

procedure TEditCatConceptos_DevEx.EnabledControlDocumento;
 var lEnabled : Boolean;
     sDescripcion, sTipo : string;
     oColor : TColor;
begin
     with dmCatalogos.cdsConceptos do
     begin
          lEnabled := StrLleno( FieldByName('CO_D_NOM').AsString );

          if lEnabled then
          begin
               oColor := clNavy;
               sDescripcion := 'Archivo: '+ FieldByName('CO_D_NOM').AsString;
               sTipo := ZetaFilesTools.GetTipoDocumento( FieldByName('CO_D_EXT').AsString );
          end
          else
          begin
               with lblDescripcionArchivo do
               begin
                    oColor := clGreen;
                    sDescripcion := 'No hay Ning�n Documento Almacenado';
                    sTipo := VACIO;
               end;
          end;
     end;

     btnVerDocumento.Enabled := lEnabled ;
     btnBorraDocumento.Enabled := lEnabled;
     btnEditarDocumento.Enabled := lEnabled;

     with lblDescripcionArchivo do
     begin
          Font.Color:= oColor;
          Caption := sDescripcion;
     end;

     lblTipoArchivo.Caption := sTipo;

end;

procedure TEditCatConceptos_DevEx.AgregaDocumento;
 var sDocumento : string;
begin
     sDocumento := dmCatalogos.cdsConceptos.FieldByName('CO_D_NOM').AsString;
     if StrVacio( sDocumento ) or ZetaDialogo.ZConfirm( Caption, '� Desea Sustituir el Documento: ' + sDocumento + ' por uno Nuevo ?', 0, mbNo ) then
     begin
          DialogoAgregaDocumento( TRUE );
     end;

end;

procedure TEditCatConceptos_DevEx.DialogoAgregaDocumento( const lAgregando : Boolean );
begin
     with dmCatalogos.cdsConceptos do
     begin
          if FEditDocumento_DevEx.EditarDocumento( lAgregando, FieldByName('CO_D_NOM').AsString, H_DOCUMENTO_CONCEPTOS, dmCatalogos.CargaDocumento ) then
          begin
               EnabledControlDocumento;
               if state in [ dsinsert,dsedit ] then
                  Modo:= dsEdit;
          end;
     end;
end;

procedure TEditCatConceptos_DevEx.AlgunosUsuariosClick(Sender: TObject);
begin
     TodosUsuarios.Checked := False;
     SetControlesAcceso;
end;

procedure TEditCatConceptos_DevEx.TodosUsuariosClick(Sender: TObject);
begin
     inherited;
     AlgunosUsuarios.Checked := False;
     SetControlesAcceso;
end;

procedure TEditCatConceptos_DevEx.SetControlesAcceso;
begin
     if ( not FNavegando )then
     begin
          if not (DataSource.DataSet.State in [dsEdit,dsInsert])then
          begin
               DataSource.DataSet.Edit;
          end;
          DataSource.DataSet.FieldByName('CO_VER_ACC').AsString := zBoolToStr(not ( TodosUsuarios.Checked ) );
     end;

     btnElegirGrupos.Enabled := AlgunosUsuarios.Checked;
end;

procedure TEditCatConceptos_DevEx.EscribirCambios;
begin
     //SetGruposAcceso;
     inherited;

end;

procedure TEditCatConceptos_DevEx.btnElegirGruposClick(Sender: TObject);
begin
     inherited;
     try
        CatConGposAcceso_DevEx := TCatConGposAcceso_DevEx.Create(Self);
        CatConGposAcceso_DevEx.ShowModal;
     finally
            CatConGposAcceso_DevEx.Free;
     end;
end;

procedure TEditCatConceptos_DevEx.ControlesVerificar;
begin
     CO_LIM_INF.Enabled := CO_VER_INF.Checked;
     CO_LIM_SUP.Enabled := CO_VER_SUP.Checked;
     lblMonto1.Enabled  := CO_VER_INF.Checked;
     lblMonto.Enabled := CO_VER_SUP.Checked;
     CO_GPO_ACC.Enabled := AlgunosUsuarios.Checked;
     btnElegirGrupos.Enabled := AlgunosUsuarios.Checked;
end;

procedure TEditCatConceptos_DevEx.CO_VER_INFClick(Sender: TObject);
begin
     inherited;
     CO_LIM_INF.Enabled := CO_VER_INF.Checked;
     lblMonto1.Enabled  := CO_VER_INF.Checked;
end;

procedure TEditCatConceptos_DevEx.CO_VER_SUPClick(Sender: TObject);
begin
  inherited;
      CO_LIM_SUP.Enabled := CO_VER_SUP.Checked;                                     
      lblMonto.Enabled := CO_VER_SUP.Checked;
end;

procedure TEditCatConceptos_DevEx.SBCO_FRM_ALTClick(Sender: TObject);
begin
     with dmCatalogos.cdsConceptos do
     begin
          if not ( Editing or Inserting ) then
             Edit;
          FieldByName( 'CO_FRM_ALT').AsString := GetFormulaConst( enNomina , CO_FRM_ALT.Lines.Text, CO_FRM_ALT.SelStart, evNomina );
     end;
end;

procedure TEditCatConceptos_DevEx.SetControlesTimbrado(Field: TField);
begin

    with  DataSource.DataSet do
    begin
         SetControlesTimbradoUX(FieldByName('CO_SAT_CLP').AsInteger, FieldByName('CO_SAT_CLN').AsInteger,  FieldByName('CO_SAT_EXE').AsInteger );
    end;

end;

procedure TEditCatConceptos_DevEx.SetControlesTimbradoUX(iCOT_SAT_CLP, iCOT_SAT_CLN,  iCON_SAT_EXE : integer );
begin

    with  DataSource.DataSet do
    begin
         CO_SAT_TPP.Enabled := ( iCOT_SAT_CLP in [1,2,4] );
         CO_SAT_TPN.Enabled := ( iCOT_SAT_CLN in [1,2,4] );
         CO_SAT_CON.Enabled := ( iCON_SAT_EXE in [3,6] );

         lblTipoPositivo.Enabled := CO_SAT_TPP.Enabled ;
         lblTipoNegativo.Enabled := CO_SAT_TPN.Enabled ;
         lblConceptoExento.Enabled := CO_SAT_CON.Enabled;

         if ( CO_SAT_TPP.Enabled ) then
             CO_SAT_TPP.Filtro := Format( '(TB_SAT_CLA = %d)', [ iCOT_SAT_CLP] );


         if ( CO_SAT_TPN.Enabled ) then
             CO_SAT_TPN.Filtro := Format( '(TB_SAT_CLA = %d)', [ iCOT_SAT_CLN] );

    end;

end;



procedure TEditCatConceptos_DevEx.CO_SAT_CLPChange(Sender: TObject);
begin
  inherited;
  SetControlesTimbradoUX( CO_SAT_CLP.Valor, CO_SAT_CLN.Valor,  CO_SAT_EXE.Valor );
end;

procedure TEditCatConceptos_DevEx.CO_SAT_CLNChange(Sender: TObject);
begin
  inherited;
  SetControlesTimbradoUX( CO_SAT_CLP.Valor, CO_SAT_CLN.Valor,  CO_SAT_EXE.Valor );
end;

procedure TEditCatConceptos_DevEx.CO_SAT_EXEChange(Sender: TObject);
begin
  inherited;
  SetControlesTimbradoUX( CO_SAT_CLP.Valor, CO_SAT_CLN.Valor,  CO_SAT_EXE.Valor ); 
end;

procedure TEditCatConceptos_DevEx.CO_SAT_EXESelect(Sender: TObject);
begin
  inherited;
   AplicarFiltroComboConcepto;
end;

procedure TEditCatConceptos_DevEx.AplicarFiltroComboConcepto;
const
     K_FILTRO_CONCEPTO_PUENTE = '( CO_TIPO = 3 ) and ( CO_SAT_CLP = 3 )';
begin
     try
         IF ((CO_SAT_EXE.Valor = 6) or (CO_SAT_EXE.Valor = 3)) then
         begin
              CO_SAT_CON.Enabled := true;
         end
         else
         begin
              CO_SAT_CON.Enabled := false;
         end;

         IF (CO_SAT_EXE.Valor = 3) then // EL VALOR 3 EN EL COMBO ES EXENTO POR CONCEPTO
         begin
            ComboValue := 3;
         end
         else
         begin
            ComboValue := 0;
         end;
     except
     end;
end;
end.

