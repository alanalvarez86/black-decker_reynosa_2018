program TimbradoNomina_TestComplete;
{$define DEVEX}
uses
  Forms,
  ComObj,
  ActiveX,
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {c: TDataModule},
  FTimbramexHelper in 'FTimbramexHelper.pas',
  ZetaClientTools in '..\..\Componentes\ZetaClientTools.pas',
  FTimbramexClasses in 'FTimbramexClasses.pas',
  WininetUtils in 'HTTP\WininetUtils.pas',
  ZBasicoSelectGrid in '..\..\Tools\ZBasicoSelectGrid.pas' {BasicoGridSelect},
  ZBaseTimbradoSelectGrid in 'ZBaseTimbradoSelectGrid.pas' {BaseTimbradoGridSelect},
  ZBaseEdicion in '..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZetaBusqueda_DevEx in '..\..\Componentes\ZetaBusqueda_DevEx.pas' {Busqueda_DevEx},
  ZetaBusqueda in '..\..\Componentes\ZetaBusqueda.pas' {Busqueda},
  ZBaseDlgModal_DevEx in '..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBaseEdicion_DevEx in '..\..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  DCatalogos in 'dCatalogos.pas' {dmCatalogos: TDataModule},
  ZBaseUtileriaShell in '..\..\Tools\ZBaseUtileriaShell.pas' {BaseUtileriaShell};

{$R *.RES}
{$R WindowsXP.res}
{$R ..\Traducciones\Spanish.RES}

procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;



  Application.Title := 'Timbrado de N�mina';
  Application.HelpFile := 'TimbradoNomina.chm';
  Application.CreateForm(TTressShell, TressShell);
  {Application.CreateForm(TBasicoGridSelect, BasicoGridSelect);
  Application.CreateForm(TBaseTimbradoGridSelect, BaseTimbradoGridSelect);
  Application.CreateForm(TBaseEdicion, BaseEdicion);
  Application.CreateForm(TBusqueda_DevEx, Busqueda_DevEx);
  Application.CreateForm(TBusqueda, Busqueda);
  Application.CreateForm(TZetaDlgModal_DevEx, ZetaDlgModal_DevEx);
  Application.CreateForm(TBaseEdicion_DevEx, BaseEdicion_DevEx); }
  Application.CreateForm(TdmCatalogos, dmCatalogos);
  //Application.CreateForm(TBaseUtileriaShell, BaseUtileriaShell);
  with TressShell do
     begin

        if Login_DevEx( False ) then
        begin
             Show;
             Update;
             BeforeRun;
             Application.Run;
        end
        else
        begin
            // CierraSplash;
             Free;
        end;

     end;
end.
