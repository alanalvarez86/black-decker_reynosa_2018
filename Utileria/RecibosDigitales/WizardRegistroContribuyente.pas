unit WizardRegistroContribuyente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, dxSkinsCore,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxDropDownEdit,
  cxMaskEdit, cxTextEdit, cxProgressBar, cxLabel, cxGroupBox,
  ZetaDialogo,
  Timbres,
  DInterfase, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, DB, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ExtCtrls, ZetaKeyLookup, ZetaDBTextBox, Buttons,
  IdCoder, IdCoder3to4, IdCoderMIME, IdBaseComponent, ZetaCommonClasses, ZetaCommonTools, ZetaClientTools,
  TressMorado2013, ZetaKeyLookup_DevEx,cxMemo;

type
  TWizardRegistroContribuyente = class(TdxWizardControlForm)
    dxWizardControl1: TdxWizardControl;
    dxWizardControlPageConexion: TdxWizardControlPage;
    dxWizardControlPage1: TdxWizardControlPage;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxProgressBarEnvio: TcxProgressBar;
    cxStatus: TcxLabel;
    cxGroupBox2: TcxGroupBox;
    cxTextEditUsuario: TcxTextEdit;
    cxMaskEditPass: TcxMaskEdit;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxComboServer: TcxComboBox;
    cxButton1: TcxButton;
    dxWizardControlPage2: TdxWizardControlPage;
    dataSourceContribuyente: TDataSource;
    memoPreview: TcxMemo;
    Button1: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    CT_CODIGO: TZetaDBKeyLookup_DevEx;
    GroupBox1: TGroupBox;
    lbKey: TLabel;
    lbCert: TLabel;
    BuscarCertificado: TcxButton;
    BuscarLlavePrivada: TcxButton;
    ArchivoCertificado: TEdit;
    ArchivoLlavePrivada: TEdit;
    OpenDialog: TOpenDialog;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label3: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    Label4: TLabel;
    ZetaDBTextBox3: TZetaDBTextBox;
    procedure cxButton1Click(Sender: TObject);
    procedure dxWizardControl1ButtonClick(Sender: TObject;
      AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BuscarLlavePrivadaClick(Sender: TObject);
    procedure BuscarCertificadoClick(Sender: TObject);
  private
    { Private declarations }   
  public
    { Public declarations }

  end;


var
  WizardRegistroContribuyente: TWizardRegistroContribuyente;


function ShowWizardTimbrado : boolean;

implementation



{$R *.dfm}

function ShowWizardTimbrado : boolean;
begin

end;

procedure TWizardRegistroContribuyente.cxButton1Click(Sender: TObject);
const
     TESTING_MESSAGE = 'PRUEBA DE CONEXION';
     K_SIN_CONECTIVIDAD = 'No hay conectividad: ';
var
   sEchoResponse : string;
   sServer : string;
   timbresSoap : TimbresFiscalesSoap;
   crypt : TWSTimbradoCrypt;
begin

     try

        sServer := '';
        sServer := cxComboServer.Properties.Items[ cxComboServer.SelectedItem ];
        timbresSoap := nil;

        if ( sServer <> '' ) then
        begin
           timbresSoap := Timbres.GetTimbresFiscalesSoap(False,  sServer + '/timbres/Timbres.asmx' );
           if ( timbresSoap <> nil ) then
           begin
                crypt := TWSTimbradoCrypt.Create;
                sEchoResponse := crypt.Desencriptar(  timbresSoap.Echo( crypt.Encriptar(TESTING_MESSAGE) ) );
			  //sEchoResponse :=  					  timbresSoap.Echo(                 TESTING_MESSAGE    );
				FreeAndNil( crypt );
                ZInformation( Self.Caption, 'Hay Conectividad con '+ cxComboServer.Text +' Respuesta: ' + sEchoResponse , 0);
           end;
        end
        else
        begin
           zError( Self.Caption, K_SIN_CONECTIVIDAD + ' No hay servidor especificado', 0);
        end;
     except on Error: Exception do
               zError( Self.Caption, K_SIN_CONECTIVIDAD + Error.Message, 0);
     end;


end;

procedure TWizardRegistroContribuyente.dxWizardControl1ButtonClick(Sender: TObject;
  AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
begin

  if AKind = wcbkCancel then
    ModalResult := mrCancel
  else if AKind = wcbkFinish then
  begin
    ModalResult := mrOk;
  end;

end;

procedure TWizardRegistroContribuyente.FormCreate(Sender: TObject);
begin
     dataSourceContribuyente.DataSet :=  dmInterfase.cdsRSocial;
end;

procedure TWizardRegistroContribuyente.Button1Click(Sender: TObject);
begin
     memoPreview.Text :=  dmInterfase.GetContribuyente( ArchivoLlavePrivada.Text, ArchivoCertificado.Text );
end;


procedure TWizardRegistroContribuyente.BuscarLlavePrivadaClick(Sender: TObject);
begin
     inherited;
     with ArchivoLlavePrivada do
     begin
          OpenDialog.Filter := VACIO;
          Text := AbreDialogo( OpenDialog, Text, 'Llave Privada|*.key' )
     end;
end;

procedure TWizardRegistroContribuyente.BuscarCertificadoClick(Sender: TObject);
begin
     inherited;
     with ArchivoCertificado do
     begin
          OpenDialog.Filter := VACIO;
          Text := AbreDialogo( OpenDialog, Text,  'Certificado|*.cer' )
     end;

end;


end.
