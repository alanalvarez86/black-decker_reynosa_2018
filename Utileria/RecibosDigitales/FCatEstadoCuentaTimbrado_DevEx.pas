unit FCatEstadoCuentaTimbrado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions, ZetaKeyLookup_DevEx,ZetaDBTextBox,cxButtons,cxGroupBox,cxLabel, ZetaFecha,
  cxContainer, ZetaCommonTools, Vcl.Mask, ZetaDialogo, cxImage, dxGDIPlusClasses;

type
  TCatEstadoCuentaTimbrado_DevEx = class(TBaseGridLectura_DevEx)
    DataSourceEstadoCuentaDetalle: TDataSource;
    Panel4: TPanel;
    cxLabel1: TcxLabel;
    cxGroupBox1: TcxGroupBox;
    cxLabel2: TcxLabel;
    varFecIni: TZetaFecha;
    cxLabel3: TcxLabel;
    varFecFin: TZetaFecha;
    cxGroupBox2: TcxGroupBox;
    ZetaDBTextBox1: TZetaDBTextBox;
    ZetaDBTextBox2: TZetaDBTextBox;
    ZetaDBTextBox3: TZetaDBTextBox;
    ZetaDBTextBox4: TZetaDBTextBox;
    ZetaDBTextBox5: TZetaDBTextBox;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    DataSourceRSocial: TDataSource;
    DataSourceEstadoDeCuenta: TDataSource;
    varRazonSocial: TZetaKeyLookup_DevEx;
    EstatusLargeImages: TcxImageList;
    btConsultarEstadoCuenta: TcxButton;
    ImagenStatus: TcxImage;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
//    procedure cxTabEstadoCuentaShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btConsultarClick(Sender: TObject);
    procedure btConsultarEstadoCuentaClick(Sender: TObject);
    procedure DataSourceEstadoDeCuentaDataChange(Sender: TObject;
      Field: TField);
  private
    { Private declarations }
    procedure Refrescar;
    procedure ObtenerStatusCuenta;
    function CambiarStatusCuenta(var iIndex: integer):integer;
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
    procedure RefrescaRazonesSociales;
  end;

var
  CatEstadoCuentaTimbrado_DevEx: TCatEstadoCuentaTimbrado_DevEx;

implementation

uses dInterfase,
     dCliente,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.dfm}

procedure TCatEstadoCuentaTimbrado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     Refrescar;
     CanLookup := false;
     HelpContext:= H32139_EstadoCuentaTimbrado;

     varFecIni.valor := FirstDayOfMonth( dmCliente.FechaDefault );
     varFecFin.valor := LastDayOfMonth( dmCliente.FechaDefault );
     varRazonSocial.Llave := VACIO;

     ZetaDBTextBox1.Caption := VACIO;   //ESTATUS
     ObtenerStatusCuenta;



     ZetaDBTextBox2.Caption := VACIO;
     ZetaDBTextBox3.Caption := VACIO;
     ZetaDBTextBox4.Caption := VACIO;
     ZetaDBTextBox5.Caption := VACIO;
end;

//Catalogos
procedure TCatEstadoCuentaTimbrado_DevEx.RefrescaRazonesSociales;
begin
     dmInterfase.cdsRSocial.Refrescar;
     DataSourceRSocial.DataSet :=  dmInterfase.cdsRSocial;
     varRazonSocial.LookupDataset := dmInterfase.cdsRSocial;
     DataSourceEstadoDeCuenta.DataSet := dmInterfase.cdsSaldoTimbrado;
     DataSourceEstadoCuentaDetalle.DataSet := dmInterfase.cdsSaldoTimbradoDetalle;

     ZetaDBTextBox1.Caption := VACIO;   //ESTATUS
     ObtenerStatusCuenta;

     ZetaDBTextBox2.Caption := VACIO;
     ZetaDBTextBox3.Caption := VACIO;
     ZetaDBTextBox4.Caption := VACIO;
     ZetaDBTextBox5.Caption := VACIO;
end;


procedure TCatEstadoCuentaTimbrado_DevEx.Refrescar;
begin
     dmInterfase.cdsSaldoTimbradoDetalle.Refrescar;
     DataSourceEstadoCuentaDetalle.DataSet :=  dmInterfase.cdsSaldoTimbradoDetalle;
end;



procedure TCatEstadoCuentaTimbrado_DevEx.Connect;
begin
     inherited;
     with dmInterfase do
     begin
          cdsSaldoTimbradoDetalle.Conectar;
          DataSourceEstadoCuentaDetalle.DataSet:= cdsSaldoTimbradoDetalle;
     end;

     RefrescaRazonesSociales;

     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEstadoCuentaTimbrado_DevEx.Refresh;
begin
     inherited;
     dmInterfase.cdsSaldoTimbradoDetalle.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatEstadoCuentaTimbrado_DevEx.Agregar;
begin
     inherited;
     ZInformation('Estado de Cuenta de Timbrado','No se permite agregar en Estado de Cuenta de Timbrado.',0);
end;

procedure TCatEstadoCuentaTimbrado_DevEx.Borrar;
begin
     inherited;
     ZInformation('Estado de Cuenta de Timbrado','No se permite borrar en Estado de Cuenta de Timbrado.',0);
end;

procedure TCatEstadoCuentaTimbrado_DevEx.Modificar;
begin
     inherited;
     ZInformation('Estado de Cuenta de Timbrado','No se permite modificar en Estado de Cuenta de Timbrado.',0);
end;

procedure TCatEstadoCuentaTimbrado_DevEx.ObtenerStatusCuenta;
var
   iIndex : integer;
   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
  inherited;
            iIndex :=  -1;
            CambiarStatusCuenta(iIndex);
            ABitmap := TcxAlphaBitmap.CreateSize(32, 32);
            APngImage := nil;
            try
                   ABitmap.Clear;
                   EstatusLargeImages.GetBitmap(iIndex, ABitmap);
                   APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                   ImagenStatus.Picture.Graphic := APngImage;
                   //ImagenStatus_DevEx.Hint := sHint;
            finally
                   APngImage.Free;
                   ABitmap.Free;
            end;

end;

function TCatEstadoCuentaTimbrado_DevEx.CambiarStatusCuenta(
  var iIndex: integer): integer;
begin
  try
     if ZetaDBTextBox1.Caption = 'Activo' then
     begin
          iIndex := 0;
     end
     else if ZetaDBTextBox1.Caption = 'Cancelado' then
     begin
          iIndex := 1;
     end
     else
     begin
          iIndex := -1;
     end;
     Result := iIndex;
  Except
  end;
end;

procedure TCatEstadoCuentaTimbrado_DevEx.btConsultarClick(
  Sender: TObject);
begin
  inherited;
  dmInterfase.ConsultarSaldo( varRazonSocial.Llave, varFecIni.Valor, varFecFin.Valor ) ;
  ZetaDBGridDBTableView.applyBestFit();
end;

procedure TCatEstadoCuentaTimbrado_DevEx.btConsultarEstadoCuentaClick(
  Sender: TObject);
begin
  inherited;
  dmInterfase.ConsultarSaldo( varRazonSocial.Llave, varFecIni.Valor, varFecFin.Valor ) ;
  ZetaDBGridDBTableView.applyBestFit();
end;

procedure TCatEstadoCuentaTimbrado_DevEx.DataSourceEstadoDeCuentaDataChange(
  Sender: TObject; Field: TField);
begin
  inherited;
            ObtenerStatusCuenta;
end;

procedure TCatEstadoCuentaTimbrado_DevEx.DoLookup;
begin
     inherited;
end;


procedure TCatEstadoCuentaTimbrado_DevEx.FormShow(Sender: TObject);
begin
 ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('FechaHora'), K_SIN_TIPO , '', skCount);
  ZetaDBGridDBTableView.ApplyBestFit();

  if StrVacio ( varRazonSocial.Llave ) then
  begin
      if ( not  dmInterfase.cdsRSocial.isEmpty ) then
      begin
           varRazonSocial.Llave := dmInterfase.cdsRSocial.FieldbyName('RS_CODIGO' ).AsString;

      end;
  end;
end;

procedure TCatEstadoCuentaTimbrado_DevEx.ZetaDBGridDBTableViewCellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
    //dmInterfase.cdsTiposSAT.Modificar;
end;




end.
