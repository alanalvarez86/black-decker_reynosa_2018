object WizardCancelacionContribuyente: TWizardCancelacionContribuyente
  Left = 158
  Top = 167
  Caption = 'Cancelaci'#243'n de Registro de Contribuyente'
  ClientHeight = 472
  ClientWidth = 572
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 122
    Top = 14
    Width = 64
    Height = 13
    Caption = 'Razon social:'
  end
  object ZetaDBTextBox2: TZetaDBTextBox
    Left = 193
    Top = 14
    Width = 276
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox2'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object dxWizardControl1: TdxWizardControl
    Left = 0
    Top = 0
    Width = 572
    Height = 472
    AutoSize = True
    Buttons.Back.Caption = '&Atras'
    Buttons.Back.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F342370000008349444154384F63201674D77733F634A2
      602610DDDDD8C5DC55DF25015546186031088C810631020D62842A230C800685
      0035FE06E2FFC81868D075525D346A100130F80CEAADEF55C1661008030D3280
      2A231E00351560338C6497C100D50DECAEEFCC001A4079D8C100D522030646A6
      81F8CA33AA18C800C2400341490AA8828101008A004FA9E01121AC0000000049
      454E44AE426082}
    Buttons.Cancel.Caption = '&Cancelar'
    Buttons.Cancel.Glyph.Data = {
      89504E470D0A1A0A0000000D49484452000000100000001008060000001FF3FF
      610000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC7249000000BA49444154384F8D913112C220104529BC574AB07746BC
      85F7B0F018293C84BD92135968FE861F11D8357FE617C0BE97998D639E311CA6
      E82FF9A8063398CDC725B84831BCE6BEA7D3FE9AAF9BE00D33985D2525CCF624
      05CC2E9214FD583D484B4907CEF5A3BB0FC32E1DFDAD370050856706AC7CC192
      745BC2CC66490F66FE4A2C98D11766FF628905B3AA640BCC361215C63E949DAC
      120BC6C2ACC58AE411C3B979ACB6AD49C0CAC08FA482995AB2C28C481498A1E4
      0B3BF701FFE789DE4F9390EE0000000049454E44AE426082}
    Buttons.CustomButtons.Buttons = <>
    Buttons.Finish.Caption = '&Aplicar'
    Buttons.Finish.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F34237000001FB49444154384FCD93CF2B055114C767DE
      F3E4577EC702C90E4549598884B9C34A6225B2B050786FE6D988CDDB4A562C10
      FF00A5B0B03233AFC4C242922CD850F2A388A5ADCF7926F5123D4A39F5E9CE9C
      7BCEF79E7BCFBDDABF32CB515AC4513AF89E5F9AED29DD76CD0CCB35F320CD77
      FFCCACB8D22C4F056DCF2C44AC0DA121C64A7F3A75B31C43B6962EC9880DC226
      DFA730A685F7BA3EF8CE6CAA89789D019273486C8069BE0FE1096EA96E960015
      E0232B8163EA7E6E92D9F82DCF0C59AE2A21B11B9611BB607C8413DB55F36CBB
      51F65E8AA31D5A09C8A5822441CB33E490B359AC1A4688DB863BB8855D9A301E
      F58CAA49AF33281DE9C3B9096B2421A832C6E36D1CB2C19CC121AB22E65A2006
      07700FE7B08A782F39A510A4FA447BC34CDCC035CC410DD5A611C8B6CC0AFEFB
      6105CE402A12C1188B36939BCF8281280B270C074E73075EE018C28848A76A61
      0264EECA670B4611AA864C7293CF98C95CCA1C603C826770F89F61942AC5F700
      B2AD25E88132844272453E195504E85439415370092228A3542242FB20E24D6C
      BFE0FDB27EF174E89674349D807A1216410EF815E41C37406E7715F06C78835F
      098949D710D311CB21D1807538A6E205C60EAA2E8ED0ADC938DD4AC5A2AE9246
      C8ED966B60C23062755493250BF961A91B1508727ED988E533861277E7B7E60B
      4A95BA9DEAB6FEC634ED0DA90748A38751FD620000000049454E44AE426082}
    Buttons.Help.Visible = False
    Buttons.Next.Caption = '&Siguiente'
    Buttons.Next.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000013000000130806000000725036
      CC0000000467414D410000B18F0BFC6105000000097048597300000EBC00000E
      BC0195BC72490000001A74455874536F667477617265005061696E742E4E4554
      2076332E352E313147F34237000000A449444154384F630081AEFA2E89EEC62E
      869EC66E0CDC5DDFCD0856440A001AC88664202332061AC80C55463C80BAF03A
      D080FF68F837D0C010A832E2C1A8812806F6D477A64095110FF018F81F285700
      55463CA0BA811DF51D3240CDDFD10D036160183A40951106F5F5F52C3D8D5DAB
      B119D4D3D4DD0F5546188C1A44180C5A8398801A61E51890DFCD0EC41C408388
      2FCFA0068134A2148A60DCD40D554508303000009CD04F09670CD6D200000000
      49454E44AE426082}
    Buttons.Next.GlyphAlignment = blGlyphRight
    Buttons.Next.Width = 78
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC300000EC301C76F
      A86400000401494441545847BD574B681351148D1F545054D08588F843415444
      4451C1850B313351B48ADAC45FAD2D528B82A0A082F47512B5CDCCC4C4561051
      F08798E8C2853F50FCA0FDC4242E546ABBABD085050517225DB818EF7973D326
      93A4B538F5C085BCFBDEBBF7BE73EFBBF3E2F95B444574AA29F40391A0619841
      BD5917FA8988882CE0E991031C1B9A516F68FA0F5333AC2272FB92B8348997BB
      0B53988BC8C15787C362F29CB7B80B1876382A29941285B7B9033AFD7432FC5B
      3AB073BE014EA80E4E2125A801D61DB3D79821DEEA0EA2A261AE7D3AFD931062
      2CAB8B82EAE3674433AEF3D01D90D309304C410C995F5AD36D068D280FDD0351
      FD1014F3B0242880A7B8A23C7407A801C98030D7B1AA24646FA02078E80E0C61
      AC07FDE803AC2A091423D2C0437760F700FD150F07055822B6DEF2D03D500DB4
      9B22BC998745213B65D068A06B789955EE819C57A3050F760DB186E8FF4A6958
      CE2A778134503DECE0610178DECF43F741C697A219358AC659ACEA07AEDEDF34
      AA7F0605718468EE0113E890104384CF4087DFBC6C641116E195B22835A30F39
      27794A799FC1D3FF0F7AE8E2B211A71C28D68082D75BA6D525BA7AC5DD8E53E2
      55618BF8E787094E86C24253411B76169E763BB34ADCEBB2EA129D8FC59DAED1
      AC96C035447AF0FD40EF18364B889E0CE43D40608CA725C8E82862202E129D79
      81911E5FCECE817DFA5BE81EC4362D68AF522B327E25FEE6A8FA2C1350AADFD4
      A8F379DB00E48393AE53AEF37E6339779C4E7EC266A0ABB7B1F1EA42567BF060
      C9D9D31DD1CECF4C1E546BD2BB158B9CE7496AAF62A5FD4ADDBB9DBE01061D06
      F2844EF30D5F46ACAB8B7734210048C3D9D81CE8887A25677D1FFA46DB21B516
      CE5BF66F3B9EF17B3F649D93E3DE7440D925C701C57E3FD81F1D7E7A9590EC6B
      2778A36D85CDC067D9F71118CDF764D751CAAE3E3AEB9B9DDEA3580F6B2B6F36
      7F2C1BFFE2A43A0141C039399D6C5996275DEE5D6F07E45DEC911F921C67A504
      273582CD1389FE5FDAADF76B64009A7EDFB9A6AD5A3D408EACB6FD9B7B12D7CA
      A6347DDC3E3A13D8382EEB1CD4A7CB95C792857225245F3DB94606916E14AA88
      77841ACE35D19F947095730DBA22519C4811036041D24FCEE1B8C0B99D922F78
      4EF55338A484CC981E8C2DBB50AFCFC33575CEE3DAA6762B4F28080B3500DA93
      95CA946C009209474D20803C2343C86FFBF1215B72E13CCD25ABD4C3309E0AA8
      DF7369CF32E108223ADC00702B0A4EDE2FF49FA1B5529D07E3ADFBB654906ECC
      00EDDE0F2DB5DEF15498A352E5CA2A9B0175EDB003184CD08CD0015301A51E0E
      5E56EC2C23E74FF8B4B226927EDF6AFADD471247634331BB8E67A77D63D30135
      86DB50AC19913E8E54F0F291436A8F77C9EB9AAD5750EDB20F50CE937B7DEBB2
      45E9F1783C7F0077B0FDD3A18C5EB80000000049454E44AE426082}
    OptionsAnimate.TransitionEffect = wcteSlide
    ParentFont = False
    OnButtonClick = dxWizardControl1ButtonClick
    OnPageChanging = dxWizardControl1PageChanging
    object dxWizardControlPageSeleccionRazonSocial: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Seleccione el contribuyente previamente registrado en el Sistema' +
        ' de Timbrado. '
      Header.Title = 'Cancelaci'#243'n de Registro de Contribuyente'
      ParentFont = False
      object Label10: TLabel
        Left = 90
        Top = 151
        Width = 68
        Height = 13
        Caption = 'Contribuyente:'
        Color = clWhite
        ParentColor = False
      end
      object RS_CODIGO: TZetaKeyLookup_DevEx
        Left = 164
        Top = 147
        Width = 300
        Height = 21
        Filtro = 'RS_CONTID >  0'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
    end
    object dxWizardControlPageRazonSocial: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Verifique que la informaci'#243'n del contribuyente seleccionado sea ' +
        'correcta. '
      Header.Title = 'Datos del Contribuyente'
      ParentFont = False
      object Label1: TLabel
        Left = 112
        Top = 60
        Width = 64
        Height = 13
        Caption = 'Razon social:'
        Color = clWhite
        ParentColor = False
      end
      object Label18: TLabel
        Left = 141
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C.:'
        Color = clWhite
        ParentColor = False
      end
      object Label6: TLabel
        Left = 76
        Top = 85
        Width = 98
        Height = 13
        Caption = 'Representante legal:'
        Color = clWhite
        ParentColor = False
      end
      object Label4: TLabel
        Left = 143
        Top = 36
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'CURP:'
        Color = clWhite
        ParentColor = False
      end
      object Label14: TLabel
        Left = 39
        Top = 106
        Width = 135
        Height = 13
        Caption = 'R.F.C. Representante Legal:'
        Color = clWhite
        ParentColor = False
      end
      object RS_RFC: TDBEdit
        Left = 182
        Top = 9
        Width = 188
        Height = 21
        DataField = 'RS_RFC'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
      object RS_LEGAL: TDBEdit
        Left = 182
        Top = 81
        Width = 274
        Height = 21
        DataField = 'RS_RLEGAL'
        DataSource = dataSourceContribuyente
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object RS_NOMBRE: TDBEdit
        Left = 182
        Top = 57
        Width = 274
        Height = 21
        DataField = 'RS_NOMBRE'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 2
      end
      object cxGroupBox3: TcxGroupBox
        Left = 44
        Top = 127
        Caption = ' Domicilio fiscal: '
        TabOrder = 3
        Height = 143
        Width = 457
        object Label13: TLabel
          Left = 104
          Top = 19
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Calle:'
          Color = clWhite
          ParentColor = False
        end
        object Label5: TLabel
          Left = 82
          Top = 43
          Width = 48
          Height = 13
          Caption = '# Exterior:'
          Color = clWhite
          ParentColor = False
        end
        object Label16: TLabel
          Left = 94
          Top = 64
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ciudad:'
          Color = clWhite
          ParentColor = False
        end
        object Label15: TLabel
          Left = 94
          Top = 89
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estado:'
          Color = clWhite
          ParentColor = False
        end
        object Label17: TLabel
          Left = 63
          Top = 113
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'C'#243'digo postal:'
          Color = clWhite
          ParentColor = False
        end
        object Label7: TLabel
          Left = 212
          Top = 43
          Width = 45
          Height = 13
          Caption = '# Interior:'
        end
        object RS_CALLE: TDBEdit
          Left = 138
          Top = 15
          Width = 274
          Height = 21
          DataField = 'RS_CALLE'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 0
        end
        object RS_NUMEXT: TDBEdit
          Left = 138
          Top = 39
          Width = 70
          Height = 21
          DataField = 'RS_NUMEXT'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 1
        end
        object RS_NUMINT: TDBEdit
          Left = 263
          Top = 39
          Width = 70
          Height = 21
          DataField = 'RS_NUMINT'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 2
        end
        object RS_CIUDAD: TDBEdit
          Left = 138
          Top = 61
          Width = 274
          Height = 21
          DataField = 'RS_CIUDAD'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 3
        end
        object RS_CODPOST: TDBEdit
          Left = 138
          Top = 108
          Width = 188
          Height = 21
          DataField = 'RS_CODPOST'
          DataSource = dataSourceContribuyente
          Enabled = False
          ReadOnly = True
          TabOrder = 4
        end
        object RS_ENTIDAD: TZetaDBKeyLookup_DevEx
          Left = 138
          Top = 85
          Width = 300
          Height = 21
          Enabled = False
          ReadOnly = True
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 5
          TabStop = True
          WidthLlave = 60
          DataField = 'RS_ENTIDAD'
          DataSource = dataSourceContribuyente
        end
      end
      object RS_CURP: TDBEdit
        Left = 182
        Top = 33
        Width = 188
        Height = 21
        DataField = 'RS_CURP'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 4
      end
      object RS_RL_RFC: TDBEdit
        Left = 182
        Top = 105
        Width = 274
        Height = 21
        DataField = 'RS_RL_RFC'
        DataSource = dataSourceContribuyente
        Enabled = False
        ReadOnly = True
        TabOrder = 5
      end
    end
    object dxWizardControlPageCuenta: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Datos de la cuenta timbrado ligado al contribuyente que  se dese' +
        'a cancelar.'
      Header.Title = 'Cuenta de timbrado '
      ParentFont = False
      object ZetaDBTextBox3: TZetaDBTextBox
        Left = 92
        Top = 267
        Width = 273
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox3'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CT_PASSWRD'
        DataSource = DataSourceCuentas
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblDI_IP: TLabel
        Left = 91
        Top = 290
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Contrase'#241'a:'
        Color = clWhite
        ParentColor = False
        Visible = False
      end
      object Label2: TLabel
        Left = 68
        Top = 141
        Width = 80
        Height = 13
        Caption = 'Cuenta timbrado:'
        Color = clWhite
        ParentColor = False
      end
      object Label9: TLabel
        Left = 97
        Top = 167
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cuenta ID:'
        Color = clWhite
        ParentColor = False
      end
      object ZetaDBTextBox1: TZetaDBTextBox
        Left = 158
        Top = 165
        Width = 121
        Height = 17
        AutoSize = False
        Caption = 'ZetaDBTextBox1'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CT_ID'
        DataSource = DataSourceCuentas
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object cxButton1: TcxButton
        Left = 350
        Top = 190
        Width = 108
        Height = 26
        Hint = 'Verificar conectividad'
        Caption = 'Probar conexi'#243'n'
        LookAndFeel.Kind = lfFlat
        LookAndFeel.NativeStyle = False
        LookAndFeel.SkinName = 'TressMorado2013'
        OptionsImage.Glyph.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000005B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF77A5FFFFBFD4FFFFFFFFFFFFD1E0
          FFFF82ACFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFB4CDFFFFD6E4FFFFD6E4FFFFE0EAFFFFFFFFFFFFE8F0
          FFFFD6E4FFFFD6E4FFFFC6D9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFFD3E2FFFFC1D6FFFFC1D6FFFFC1D6FFFFC1D6
          FFFFC1D6FFFFCCDDFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFCCDDFFFF6095FFFFBFD4
          FFFF9BBDFFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF6599FFFF5B92FFFF6D9E
          FFFF6397FFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC4D8FFFF6397FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF70A0FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF84ADFFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFFEAF1FFFF87AFFFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF72A1FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF8EB4FFFF5E94FFFF5B92
          FFFF5B92FFFF5B92FFFFE3ECFFFFF5F8FFFFEAF1FFFFEAF1FFFFEAF1FFFFEAF1
          FFFFEAF1FFFFF2F6FFFFFAFCFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFD8E5FFFF6599FFFF5B92
          FFFF5B92FFFF5B92FFFF6397FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF70A0FFFF70A0FFFF6599FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF75A3FFFF84ADFFFF84ADFFFF84ADFFFF7AA7
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5E94FFFFF2F6FFFF75A3
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFFABC7FFFFFFFFFFFFBAD1FFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF689BFFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFD8E5FFFFEAF1FFFFEAF1FFFFF2F6FFFFFFFFFFFFF2F6FFFFEAF1
          FFFFEAF1FFFFDEE9FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFFC4D8FFFF6A9C
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFFB4CDFFFF99BBFFFF99BBFFFF99BBFFFF99BBFFFF99BB
          FFFFB4CDFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF9BBDFFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF6A9CFFFF8CB2FFFF5B92FFFF9BBDFFFF6095
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF7AA7FFFFB7CFFFFF5B92FFFFCCDDFFFF6D9E
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF84ADFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF84ADFFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFFFFFFFFF96B9FFFF70A0FFFF70A0FFFF70A0FFFF70A0FFFF70A0
          FFFF96B9FFFFFFFFFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFFDEE9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE5EEFFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92
          FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF5B92FFFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = cxButton1Click
      end
      object CT_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 158
        Top = 140
        Width = 300
        Height = 21
        Enabled = False
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidLookup = CT_CODIGOValidLookup
        DataField = 'CT_CODIGO'
        DataSource = dataSourceContribuyente
      end
    end
    object dxWizardControlPageRegistrar: TdxWizardControlPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Header.Description = 
        'Al iniciar el proceso se cerrar'#225' el asistente, al concluir recib' +
        'ir'#225' una notificaci'#243'n presentando la bit'#225'cora con los resultados ' +
        'del proceso'
      Header.Title = 'Presione Aplicar para iniciar el proceso'
      ParentFont = False
      object Panel1: TPanel
        Left = 0
        Top = 1
        Width = 547
        Height = 89
        Align = alCustom
        BevelOuter = bvNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxLabel1: TcxLabel
          Left = 66
          Top = 0
          Align = alClient
          AutoSize = False
          Caption = 
            'Al aplicar el proceso se cancelar'#225' el registro del contribuyente' +
            ' en el Sistema de Timbrado por lo que ya no estar'#225' disponible pa' +
            'ra los diferentes procesos de timbrado.'
          ParentColor = False
          ParentFont = False
          Style.BorderColor = clScrollBar
          Style.BorderStyle = ebsFlat
          Style.Color = clInfoBk
          Style.Edges = [bTop, bRight, bBottom]
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -12
          Style.Font.Name = 'Meiryo UI'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Properties.ShadowedColor = clInfoBk
          Properties.WordWrap = True
          Height = 89
          Width = 481
          AnchorY = 45
        end
        object cxImage1: TcxImage
          Left = 0
          Top = 0
          Align = alLeft
          AutoSize = True
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000040
            000000400806000000AA6971DE0000000467414D410000B18F0BFC6105000000
            097048597300000EBC00000EBC0195BC7249000004A149444154785EDD5B2D70
            1341148E40202A110804A202814020101515150804025181405454542011CC5D
            672A2A100844052282617A97834154545454202A2A2A1A72172A221011888A0A
            44057CDFE63549C94B73C9EDDE6DFBCD7CC94C76F7DECFFEBDDD7BA9B946B8BF
            7F2B8C7E2C85516B258CD3F5A0916D058DF6377C1F801DC3383BC4F74E90B43F
            B20EB81A6EB79E85F5CE6D79CCF542D868CEC1E0E5A0917E8271676123FB3B0B
            D1FE0F9D1536D257E1E7EC8E3CDE4F989E8EB397419CEE5271CDA0228423CF31
            4AF6C3B8B5E6DDC8E0708582479AE22E0859271C1522BE3A8449FAD8F48AA264
            19344EC7FA22EA9407CE47084F34A5AAA0E984EDF4BEA8E716E197EC416F08EA
            CA5445E8D4C5F782A8E90658E49E42D0CCABBA6B42B773676B431865AF7B0274
            E13E3188DB9BDC9544F5E280E11F34413E133A27569CC09ED70414659064DB88
            F85E9050B6AED5294A4496EFC58CD92073DEFEB047C02422FAE06F6ADDA24408
            2E22A683ACF6D6173C448ADF45C40858A6B52942D38149BA2822F241F679275B
            1DF6EC772266042CD3DA142542F3DF61DC9C17319301E39D05393C20899811B0
            4C6B6383B0E950C45C0D86B7DA036C118A9C865F7FDE13717DF03753A6B4B146
            2CB8226E3CCA88EDD91B5C63442416C0E6BCF94DA96B93907172E5D6682E2194
            86AE08671F935A9933E2382DE65E063D030F9576A4AD8AB0116786E69C983D80
            B37DD84746E95B317B00DEE4A8956F20616B2A66F7C021E1E21ACB6B0EC705BC
            C0542BDD68B6DE88F96E0390496494869DA0C36FADDC15FB21B9ACFEA55F72C0
            E80333F2645F3657DF4A3DB76CDEC5F0FFB1A417DA27B72033DAA2D613E3FD21
            54E2009E14F9A1165AA2B99DC1B17AD2A565150E08926C03FB7FBAAE155A63CE
            A368250E88B33A63FF2DADD01ABD7640BA0B07F0DD9B5EC11931EAC4EE3E2A1A
            01471C01075AA153FAE3802E1DD0D10A9DD21307903C03FCD20A9CD22F07B8BF
            8818A12F5300D127A7C08E56E894DE38203BAE99B414A5D029FD71C09EFB4048
            A32F0E6020046556B542A7F4C601EDCDD22F420D3D7180B92065C21156C3726F
            837C71C0C501ADF470D803079830F802A57BDF871130AC83BC0C2D2FFBC38B29
            D07E24A27BC09E585EBA5BC50EE0F947C40E60323095CA4E58BD03465FD19BDD
            A0ACF4B70A1D001B4FC7E61D97D60B558E0045F625707B501BDA64450E806DDD
            8989D6A55C936B0E88B2E76A5D9BCC9B30E57C47885ACB22AA0F6688A8752DD1
            AC6F79F30619229AE1A23CA828F1DCB1098CCCEBD3DA1425649E31F547C4E403
            1A2EA061F1E088899649BA6818B51ECAE3C7827506F52D25692A232E17AC2C4C
            39DF0B68304ED09E390D27ADFA93605E6D690FCE4BBE00C5949A89055FDB331D
            57CC981DE6ED71152F4F0A92AFBFD55CA059609CE06881724193886DCBF86160
            48AE5859185DB2E89C9F042E4C656773E4A1D9EA665DEDA74559999D79095D4E
            D031D3EDF33680E1C63F3B54F6072AC8EE725AE68EF05C80C279976094519474
            41C83AE55CF7EA1FA45C759981C924444D691BE44D0E2F33FCFF1F31D608E6E1
            F5F662DD98BC446F1F99DEFEFF0EEFBA80A96866FB4CB20D1853675A0A8D1A9E
            32DC55D0B3CC1CDFEBD56162556B8DD1A03CC6116AB57F058711FAC14F69E200
            00000049454E44AE426082}
          Style.BorderColor = clScrollBar
          Style.Edges = [bLeft, bTop, bBottom]
          TabOrder = 1
        end
      end
      object cxProgressBarEnvio: TcxProgressBar
        Left = 33
        Top = 197
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'MS Sans Serif'
        Style.Font.Style = []
        Style.IsFontAssigned = True
        TabOrder = 1
        Width = 481
      end
      object cxLabel4: TcxLabel
        Left = 33
        Top = 174
        Caption = 'Avance:'
        Transparent = True
      end
      object cxStatus: TcxLabel
        Left = 33
        Top = 221
        Transparent = True
      end
    end
  end
  object dataSourceContribuyente: TDataSource
    Left = 688
    Top = 48
  end
  object OpenDialog: TOpenDialog
    FileName = 'D:\3win_20_2010_Build_1\Sistema\Copy of New Text Document.cer'
    Filter = 'Llave Privada|*.key'
    Left = 474
    Top = 16
  end
  object DataSourceCuentas: TDataSource
    Left = 472
    Top = 18
  end
end
