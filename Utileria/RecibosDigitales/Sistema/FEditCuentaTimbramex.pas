unit FEditCuentaTimbramex;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, Mask, ZetaNumero, ZetaKeyCombo, ZetaEdit,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, cxControls,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinsCore,  TressMorado2013, dxSkinsDefaultPainters,
  dxSkinsdxBarPainter;

type
  TEditCuentaTimbramex = class(TBaseEdicion_DevEx)
    lblDI_NOMBRE: TLabel;
    lblDI_DESCRIP: TLabel;
    CT_DESCRIP: TDBEdit;
    CT_CODIGO: TZetaDBEdit;
    GroupBox1: TGroupBox;
    lblDI_IP: TLabel;
    CT_PASSWRD: TDBEdit;
    Label1: TLabel;
    ZetaDBNumero1: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
  { Protected declarations }
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditCuentaTimbramex: TEditCuentaTimbramex;

implementation

uses DInterfase,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.dfm}

{ TEditSistLstDispositivos }

procedure TEditCuentaTimbramex.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_NOMINA_PERIODOS;
     HelpContext := 0;
     FirstControl := CT_CODIGO;
end;

procedure TEditCuentaTimbramex.Connect;
begin
     inherited;
     with dmInterfase do
     begin
          cdsCuentasTimbramex.Conectar;
          DataSource.DataSet := cdsCuentasTimbramex;
     end;
end;

end.
