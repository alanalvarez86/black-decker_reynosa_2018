object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  Left = 436
  Top = 235
  Height = 479
  Width = 741
  object cdsCondiciones: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    Left = 48
    Top = 128
  end
  object cdsNomParamLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 240
    Top = 160
  end
  object cdsPuestos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    LookupActivoField = 'PU_ACTIVO'
    Left = 16
    Top = 8
  end
  object cdsTurnos: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Turnos'
    LookupDescriptionField = 'TU_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    LookupActivoField = 'TU_ACTIVO'
    Left = 88
    Top = 8
  end
  object cdsCursos: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    LookupName = 'Cat'#225'logo de Cursos'
    LookupDescriptionField = 'CU_DESCRIP'
    LookupKeyField = 'CU_CODIGO'
    LookupActivoField = 'CU_ACTIVO'
    Left = 160
    Top = 8
  end
  object cdsNomParam: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    UsaCache = True
    LookupName = 'Par'#225'metros de N'#243'mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    LookupActivoField = 'NP_ACTIVO'
    Left = 272
    Top = 128
  end
  object cdsClasifi: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Clasificaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    Left = 248
    Top = 8
  end
  object cdsConceptos: TZetaLookupDataSet
    Tag = 5
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    Left = 344
    Top = 8
  end
  object ZetaLookupDataSet1: TZetaLookupDataSet
    Tag = 6
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    UsaCache = True
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    Left = 424
    Top = 8
  end
  object cdsContratos: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Tipos de Contrato'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    Left = 16
    Top = 184
  end
  object cdsEventos: TZetaLookupDataSet
    Tag = 8
    Aggregates = <>
    IndexFieldNames = 'EV_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Eventos'
    LookupDescriptionField = 'EV_DESCRIP'
    LookupKeyField = 'EV_CODIGO'
    LookupActivoField = 'EV_ACTIVO'
    Left = 16
    Top = 72
  end
  object cdsCalendario: TZetaClientDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    Left = 368
    Top = 72
  end
  object cdsFestTurno: TZetaLookupDataSet
    Tag = 9
    Aggregates = <>
    IndexFieldNames = 'TU_CODIGO'
    Params = <>
    LookupName = 'Cat'#225'logo de Festivos por Turno'
    LookupDescriptionField = 'FE_DESCRIP'
    LookupKeyField = 'TU_CODIGO'
    Left = 96
    Top = 72
  end
  object cdsFolios: TZetaLookupDataSet
    Tag = 10
    Aggregates = <>
    IndexFieldNames = 'FL_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Folios'
    LookupDescriptionField = 'FL_DESCRIP'
    LookupKeyField = 'FL_CODIGO'
    Left = 160
    Top = 72
  end
  object cdsHorarios: TZetaLookupDataSet
    Tag = 11
    Aggregates = <>
    IndexFieldNames = 'HO_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Horarios'
    LookupDescriptionField = 'HO_DESCRIP'
    LookupKeyField = 'HO_CODIGO'
    LookupActivoField = 'HO_ACTIVO'
    Left = 296
    Top = 72
  end
  object cdsInvitadores: TZetaLookupDataSet
    Tag = 12
    Aggregates = <>
    IndexFieldNames = 'IV_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Invitadores'
    LookupDescriptionField = 'IV_NOMBRE'
    LookupKeyField = 'IV_CODIGO'
    LookupActivoField = 'IV_ACTIVO'
    Left = 96
    Top = 184
  end
  object cdsReglas: TZetaLookupDataSet
    Tag = 19
    Aggregates = <>
    IndexFieldNames = 'CL_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Reglas'
    LookupDescriptionField = 'CL_LETRERO'
    LookupKeyField = 'CL_CODIGO'
    LookupActivoField = 'CL_ACTIVO'
    Left = 432
    Top = 72
  end
  object cdsOtrasPer: TZetaLookupDataSet
    Tag = 16
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Percepciones Fijas'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 176
    Top = 240
  end
  object cdsMaestros: TZetaLookupDataSet
    Tag = 13
    Aggregates = <>
    IndexFieldNames = 'MA_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Maestros'
    LookupDescriptionField = 'MA_NOMBRE'
    LookupKeyField = 'MA_CODIGO'
    LookupActivoField = 'MA_ACTIVO'
    Left = 16
    Top = 128
  end
  object cdsMatrizCurso: TZetaClientDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    Left = 184
    Top = 128
  end
  object cdsOrdFolios: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'OF_POSICIO'
    Params = <>
    Left = 224
    Top = 72
  end
  object cdsEntNivel: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'ET_CODIGO'
    Params = <>
    Left = 96
    Top = 128
  end
  object cdsRPatron: TZetaLookupDataSet
    Tag = 20
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Registros Patronales'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    Left = 16
    Top = 240
  end
  object cdsSSocial: TZetaLookupDataSet
    Tag = 18
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Prestaciones'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    LookupActivoField = 'TB_ACTIVO'
    Left = 248
    Top = 184
  end
  object cdsPrestaci: TZetaLookupDataSet
    Tag = 18
    Aggregates = <>
    IndexFieldNames = 'PT_YEAR'
    Params = <>
    Left = 312
    Top = 184
  end
  object cdsPRiesgo: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RT_FECHA'
    Params = <>
    Left = 96
    Top = 240
  end
  object cdsPeriodo: TZetaLookupDataSet
    Tag = 17
    Aggregates = <>
    IndexFieldNames = 'PE_NUMERO'
    Params = <>
    LookupName = 'Periodos de N'#243'mina'
    LookupDescriptionField = 'PE_FEC_INI'
    LookupKeyField = 'PE_NUMERO'
    Left = 176
    Top = 192
  end
  object cdsConceptosLookUp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    LookupName = 'Cat'#225'logo de Conceptos'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    Left = 400
    Top = 184
  end
  object ZetaLookupDataSet2: TZetaLookupDataSet
    Tag = 21
    Aggregates = <>
    IndexFieldNames = 'NP_FOLIO'
    Params = <>
    LookupName = 'Par'#225'metros de N'#243'mina'
    LookupDescriptionField = 'NP_NOMBRE'
    LookupKeyField = 'NP_FOLIO'
    LookupActivoField = 'NP_ACTIVO'
    Left = 368
    Top = 128
  end
  object cdsTools: TZetaLookupDataSet
    Tag = 23
    Aggregates = <>
    IndexFieldNames = 'TO_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Herramientas'
    LookupDescriptionField = 'TO_DESCRIP'
    LookupKeyField = 'TO_CODIGO'
    Left = 248
    Top = 240
  end
  object cdsPercepFijas: TZetaClientDataSet
    Tag = 24
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    Left = 392
    Top = 240
  end
  object cdsAreas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Areas'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    Left = 16
    Top = 296
  end
  object cdsPuestosLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Puestos'
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    LookupActivoField = 'PU_ACTIVO'
    Left = 96
    Top = 296
  end
  object cdsPtoTools: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    Left = 184
    Top = 296
  end
  object cdsSesiones: TZetaLookupDataSet
    Tag = 26
    Aggregates = <>
    IndexFieldNames = 'SE_FOLIO'
    Params = <>
    Left = 248
    Top = 296
  end
  object cdsHisCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 312
    Top = 296
  end
  object cdsAccReglas: TZetaLookupDataSet
    Tag = 28
    Aggregates = <>
    IndexFieldNames = 'AE_CODIGO'
    Params = <>
    LookupName = 'Cat'#225'logo de Reglas de Acceso'
    LookupDescriptionField = 'AE_LETRERO'
    LookupKeyField = 'AE_CODIGO'
    LookupActivoField = 'AE_ACTIVO'
    Left = 400
    Top = 296
  end
  object cdsAulas: TZetaLookupDataSet
    Tag = 29
    Aggregates = <>
    IndexFieldNames = 'AL_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Aulas'
    LookupDescriptionField = 'AL_NOMBRE'
    LookupKeyField = 'AL_CODIGO'
    LookupActivoField = 'AL_ACTIVA'
    Left = 16
    Top = 352
  end
  object cdsPrerequisitosCurso: TZetaClientDataSet
    Tag = 30
    Aggregates = <>
    Params = <>
    Left = 96
    Top = 352
  end
  object cdsCertificaciones: TZetaLookupDataSet
    Tag = 31
    Aggregates = <>
    IndexFieldNames = 'CI_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Certificaciones'
    LookupDescriptionField = 'CI_NOMBRE'
    LookupKeyField = 'CI_CODIGO'
    LookupActivoField = 'CI_ACTIVO'
    Left = 32
    Top = 400
  end
  object cdsTiposPoliza: TZetaLookupDataSet
    Tag = 32
    Aggregates = <>
    IndexFieldNames = 'PT_CODIGO'
    Params = <>
    LookupName = 'Tipos de P'#243'liza'
    LookupDescriptionField = 'PT_NOMBRE'
    LookupKeyField = 'PT_CODIGO'
    Left = 112
    Top = 408
  end
  object cdsTPeriodos: TZetaLookupDataSet
    Tag = 33
    Aggregates = <>
    IndexFieldNames = 'TP_TIPO'
    Params = <>
    LookupName = 'Tipos de periodo'
    LookupDescriptionField = 'TP_DESCRIP'
    LookupKeyField = 'TP_TIPO'
    Left = 201
    Top = 354
  end
  object cdsCamposPerfil: TZetaClientDataSet
    Tag = 36
    Aggregates = <>
    Params = <>
    Left = 196
    Top = 416
  end
  object cdsSeccionesPerfil: TZetaLookupDataSet
    Tag = 35
    Aggregates = <>
    Params = <>
    LookupName = 'Secciones de Perfil'
    LookupDescriptionField = 'DT_NOMBRE'
    LookupKeyField = 'DT_CODIGO'
    Left = 278
    Top = 415
  end
  object cdsPerfilesPuesto: TZetaLookupDataSet
    Tag = 34
    Aggregates = <>
    IndexFieldNames = 'PU_CODIGO'
    Params = <>
    LookupName = 'Catalogo de Perfiles '
    LookupDescriptionField = 'PU_DESCRIP'
    LookupKeyField = 'PU_CODIGO'
    Left = 512
    Top = 398
  end
  object cdsPerfiles: TZetaClientDataSet
    Tag = 37
    Aggregates = <>
    Params = <>
    Left = 358
    Top = 400
  end
  object cdsDescPerfil: TZetaClientDataSet
    Tag = 38
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 344
  end
  object cdsProvCap: TZetaLookupDataSet
    Tag = 39
    Aggregates = <>
    IndexFieldNames = 'PC_CODIGO'
    Params = <>
    LookupName = 'Proveedores de Capacitaci'#243'n'
    LookupDescriptionField = 'PC_NOMBRE'
    LookupKeyField = 'PC_CODIGO'
    LookupActivoField = 'PC_ACTIVO'
    Left = 576
    Top = 302
  end
  object cdsValPlantilla: TZetaLookupDataSet
    Tag = 41
    Aggregates = <>
    Params = <>
    LookupName = 'Cat'#225'logo de Plantillas de Valuaci'#243'n'
    LookupDescriptionField = 'VL_NOMBRE'
    LookupKeyField = 'VL_CODIGO'
    Left = 472
    Top = 240
  end
  object cdsValFactores: TZetaLookupDataSet
    Tag = 43
    Aggregates = <>
    Params = <>
    LookupName = 'Cat'#225'logo de Factores de Valuaci'#243'n'
    LookupDescriptionField = 'VF_NOMBRE'
    LookupKeyField = 'VF_ORDEN'
    Left = 496
    Top = 184
  end
  object cdsSubfactores: TZetaLookupDataSet
    Tag = 45
    Aggregates = <>
    Params = <>
    LookupName = 'Cat'#225'logo de Subfactores de Valuaci'#243'n'
    LookupDescriptionField = 'VS_NOMBRE'
    LookupKeyField = 'VS_ORDEN'
    Left = 464
    Top = 128
  end
  object cdsValNiveles: TZetaLookupDataSet
    Tag = 46
    Aggregates = <>
    Params = <>
    LookupName = 'Escala de Subfactores'
    LookupDescriptionField = 'VN_NOMBRE'
    LookupKeyField = 'VN_ORDEN'
    Left = 568
    Top = 184
  end
  object cdsValuaciones: TZetaClientDataSet
    Tag = 47
    Aggregates = <>
    Params = <>
    Left = 560
    Top = 240
  end
  object cdsValPuntos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 480
    Top = 304
  end
  object cdsPuntosNivel: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Escala de Subfactores'
    LookupDescriptionField = 'VN_NOMBRE'
    LookupKeyField = 'VN_ORDEN'
    Left = 552
    Top = 128
  end
  object cdsPeriodoOtro: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'PE_NUMERO'
    Params = <>
    LookupName = 'Periodos de N'#243'mina'
    LookupDescriptionField = 'PE_FEC_INI'
    LookupKeyField = 'PE_NUMERO'
    Left = 312
    Top = 240
  end
  object cdsTemp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 264
    Top = 352
  end
  object cdsRSocial: TZetaLookupDataSet
    Tag = 49
    Aggregates = <>
    Params = <>
    LookupName = 'Cat'#225'logo de razones sociales'
    LookupDescriptionField = 'RS_NOMBRE'
    LookupKeyField = 'RS_CODIGO'
    Left = 512
    Top = 8
  end
  object cdsMatrizCertificPuesto: TZetaClientDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'CI_CODIGO'
    Params = <>
    Left = 576
    Top = 368
  end
  object cdsCerNivel: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CN_CODIGO'
    Params = <>
    Left = 352
    Top = 472
  end
  object cdsReglaPrestamo: TZetaLookupDataSet
    Tag = 52
    Aggregates = <>
    Params = <>
    LookupName = 'Reglas de Pr'#233'stamos'
    LookupDescriptionField = 'RP_LETRERO'
    LookupKeyField = 'RP_CODIGO'
    LookupActivoField = 'RP_ACTIVO'
    Left = 616
    Top = 8
  end
  object cdsPrestaXRegla: TZetaClientDataSet
    Tag = 53
    Aggregates = <>
    Params = <>
    Left = 616
    Top = 56
  end
  object cdsHistRev: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CH_FECHA'
    Params = <>
    Left = 504
    Top = 56
  end
  object cdsPeriodoAd: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 288
    Top = 472
  end
  object cdsEstablecimientos: TZetaLookupDataSet
    Tag = 55
    Aggregates = <>
    IndexFieldNames = 'ES_CODIGO'
    Params = <>
    LookupName = 'Establecimientos'
    LookupDescriptionField = 'ES_ELEMENT'
    LookupKeyField = 'ES_CODIGO'
    LookupActivoField = 'ES_ACTIVO'
    Left = 488
    Top = 368
  end
  object cdsTiposPension: TZetaLookupDataSet
    Tag = 56
    Aggregates = <>
    IndexFieldNames = 'TP_CODIGO'
    Params = <>
    LookupName = 'Tipos de Pensi'#243'n'
    LookupDescriptionField = 'TP_NOMBRE'
    LookupKeyField = 'TP_CODIGO'
    Left = 576
    Top = 432
  end
  object cdsSegGastosMed: TZetaLookupDataSet
    Tag = 57
    Aggregates = <>
    IndexFieldNames = 'PM_CODIGO'
    Params = <>
    LookupName = 'P'#243'lizas de SGM'
    LookupDescriptionField = 'PM_DESCRIP'
    LookupKeyField = 'PM_CODIGO'
    Left = 432
    Top = 432
  end
  object cdsVigenciasSGM: TZetaLookupDataSet
    Tag = 58
    Aggregates = <>
    IndexFieldNames = 'PM_CODIGO'
    Params = <>
    LookupName = 'Vigencias SGM'
    LookupDescriptionField = 'PV_REFEREN'
    Left = 672
    Top = 432
  end
  object cdsCosteoGrupos: TZetaLookupDataSet
    Tag = 60
    Aggregates = <>
    IndexFieldNames = 'GpoCostoCodigo'
    Params = <>
    LookupName = 'Grupos de Costeo'
    LookupDescriptionField = 'GpoCostoNombre'
    LookupKeyField = 'GpoCostoCodigo'
    LookupActivoField = 'GpoCostoActivo'
    Left = 32
    Top = 456
  end
  object cdsCosteoCriterios: TZetaLookupDataSet
    Tag = 61
    Aggregates = <>
    IndexFieldNames = 'CritCostoID'
    Params = <>
    LookupName = 'Criterios de Costeo'
    LookupDescriptionField = 'CritCostoNombre'
    LookupKeyField = 'CritCostoID'
    LookupActivoField = 'CritCostoActivo'
    Left = 32
    Top = 504
  end
  object cdsCosteoCriteriosPorConcepto: TZetaClientDataSet
    Tag = 62
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 560
  end
  object cdsConceptosDisponibles: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    LookupName = 'Conceptos Disponibles'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    LookupActivoField = 'CO_ACTIVO'
    Left = 192
    Top = 472
  end
  object cdsTablasAmortizacion: TZetaLookupDataSet
    Tag = 63
    Aggregates = <>
    Params = <>
    LookupName = 'Tablas de Cotizaci'#243'n SGM'
    LookupDescriptionField = 'AT_DESCRIP'
    LookupKeyField = 'AT_CODIGO'
    LookupActivoField = 'AT_ACTIVO'
    Left = 384
    Top = 544
  end
  object cdsCostosAmortizacion: TZetaLookupDataSet
    Tag = 64
    Aggregates = <>
    Params = <>
    Left = 384
    Top = 592
  end
  object cdsCompetencias: TZetaLookupDataSet
    Tag = 56
    Aggregates = <>
    IndexFieldNames = 'CC_CODIGO'
    Params = <>
    LookupName = 'Competencias'
    LookupDescriptionField = 'CC_ELEMENT'
    LookupKeyField = 'CC_CODIGO'
    LookupActivoField = 'CC_ACTIVO'
    Left = 176
    Top = 560
  end
  object cdsCompRevisiones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 264
    Top = 520
  end
  object cdsCompCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 264
    Top = 616
  end
  object cdsCatPerfiles: TZetaLookupDataSet
    Tag = 56
    Aggregates = <>
    IndexFieldNames = 'CP_CODIGO'
    Params = <>
    LookupName = 'Grupos de Competencias'
    LookupDescriptionField = 'CP_ELEMENT'
    LookupKeyField = 'CP_CODIGO'
    LookupActivoField = 'CP_ACTIVO'
    Left = 368
    Top = 560
  end
  object cdsRevPerfiles: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 432
    Top = 520
  end
  object cdsMatPerfilComps: TZetaClientDataSet
    Tag = 69
    Aggregates = <>
    Params = <>
    Left = 536
    Top = 560
  end
  object cdsCompNiveles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Niveles'
    LookupDescriptionField = 'NC_DESCRIP'
    LookupKeyField = 'NC_NIVEL'
    Left = 264
    Top = 568
  end
  object cdsGpoCompPuesto: TZetaClientDataSet
    Tag = 72
    Aggregates = <>
    Params = <>
    Left = 672
    Top = 528
  end
end
