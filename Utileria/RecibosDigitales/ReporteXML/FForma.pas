unit FForma;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, ComCtrls,
  Psock, NMsmtp;

type
  TForma = class(TForm)
    BitBtn1: TBitBtn;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    Edit2: TEdit;
    DataSource2: TDataSource;
    DBGrid2: TDBGrid;
    BitBtn2: TBitBtn;
    Button1: TButton;
    BitBtn3: TBitBtn;
    RichEdit1: TRichEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Forma: TForma;

implementation

uses DReportesMail, DCliente;

{$R *.DFM}

procedure TForma.BitBtn1Click(Sender: TObject);
begin
    if dmCliente.BuscaCompany( edit1.Text ) then
        dmReportesMail.GeneraReporte
    else
        ShowMessage( 'No Existe' );
    ShowMessage( 'Proceso Terminado' );
end;

procedure TForma.BitBtn2Click(Sender: TObject);
begin
    IF dmCliente.BuscaCompany( edit1.Text ) then
     dmReportesMail.GeneraMail;
end;

procedure TForma.Button1Click(Sender: TObject);
begin
    {f dmCliente.BuscaCompany( edit1.Text ) then
        dmReportesMail.GeneraUnReporteDummy(StrToInt(edit2.text))
    else
        ShowMessage( 'No Existe' );
    ShowMessage( 'Proceso Terminado' );}

end;

procedure TForma.BitBtn3Click(Sender: TObject);
begin
     with dmReportesMail.Email do
     begin
          if Not Connected then
          begin
               Host := '10.10.10.1';
               UserID := 'cveliz';
               Connect;
          end;
          if Connected then
          begin
               SubType := mtEnriched;
               PostMessage.FromAddress := 'cveliz@tress.com.mx';
               PostMessage.FromName := 'Ultimo Desarrollo';
               PostMessage.ToAddress.Clear;
               PostMessage.ToAddress.Add( 'cveliz@tress.com.mx' );
               PostMessage.Body.AddStrings(richEdit1.Lines);
               PostMessage.Date := DateToStr( Date );
               SendMail;
          end
          else
              ShowMessage('El Servidor de Email no est� conectado');
     end;
end;
end.
