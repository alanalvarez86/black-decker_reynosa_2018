object WizardTimbradoForm: TWizardTimbradoForm
  Left = 441
  Top = 205
  Width = 732
  Height = 453
  Caption = 'Timbrado de Recibos de N'#243'mina'
  Color = clBtnFace
  Constraints.MaxHeight = 453
  Constraints.MaxWidth = 732
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object dxWizardControl1: TdxWizardControl
    Left = 0
    Top = 0
    Width = 716
    Height = 415
    AutoSize = True
    Buttons.Back.Caption = '&Regresar'
    Buttons.Cancel.Caption = '&Cancelar'
    Buttons.Finish.Caption = '&Ejecutar'
    Buttons.Help.Visible = False
    Buttons.Next.Caption = '&Siguiente'
    OptionsAnimate.TransitionEffect = wcteSlide
    OnButtonClick = dxWizardControl1ButtonClick
    OnPageChanging = dxWizardControl1PageChanging
    object dxWizardControlPage2: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Se Enviar'#225' a Timbrar el siguiente Periodo de N'#243'mina'
      Header.Title = 'N'#243'mina a Timbrar Por Raz'#243'n Social'
      object Label3: TLabel
        Left = -5
        Top = 245
        Width = 66
        Height = 13
        Caption = 'Razon Social:'
        Visible = False
      end
      object Label1: TLabel
        Left = 20
        Top = 116
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object Label5: TLabel
        Left = 3
        Top = 151
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Raz'#243'n Social:'
      end
      object gridPeriodos: TcxGrid
        Left = 0
        Top = 199
        Width = 694
        Height = 80
        Align = alBottom
        TabOrder = 0
        object gridPeriodosDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dataSourceTimbrar
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnGrouping = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = 'No hay Periodos'
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object gridPeriodosDBTableView1Column10: TcxGridDBColumn
            Caption = 'Status Env'#237'o'
            DataBinding.FieldName = 'PE_T_STATUS'
            Visible = False
            Width = 85
          end
          object gridPeriodosDBTableView1Column8: TcxGridDBColumn
            Caption = 'Raz'#243'n Social'
            DataBinding.FieldName = 'RS_NOMBRE'
            Options.Editing = False
            Options.Filtering = False
            Width = 190
          end
          object gridPeriodosDBTableView1Column2: TcxGridDBColumn
            Caption = 'N'#243'mina'
            DataBinding.FieldName = 'TP_NOMBRE'
            Visible = False
            Options.Editing = False
            Width = 85
          end
          object gridPeriodosDBTableView1Column9: TcxGridDBColumn
            Caption = 'Periodo'
            DataBinding.FieldName = 'PE_NUMERO'
            Visible = False
            Width = 53
          end
          object gridPeriodosDBTableView1Column5: TcxGridDBColumn
            Caption = 'Empleados'
            DataBinding.FieldName = 'PE_NUM_EMP'
            Options.Editing = False
            Width = 80
          end
          object gridPeriodosDBTableView1Column7: TcxGridDBColumn
            Caption = 'Timbrados'
            DataBinding.FieldName = 'TIMBRADOS'
            Width = 80
          end
          object gridPeriodosDBTableView1Column11: TcxGridDBColumn
            Caption = 'Por Timbrar'
            DataBinding.FieldName = 'PORTIMBRAR'
            Width = 80
          end
          object gridPeriodosDBTableView1Column4: TcxGridDBColumn
            Caption = 'Monto Neto'
            DataBinding.FieldName = 'PE_TOT_NET'
            Visible = False
            Options.Editing = False
            Width = 79
          end
        end
        object gridPeriodosLevel1: TcxGridLevel
          GridView = gridPeriodosDBTableView1
        end
      end
      object DBEdit1: TDBEdit
        Left = 66
        Top = 243
        Width = 79
        Height = 21
        DataField = 'RS_NOMBRE'
        DataSource = dataSourceContribuyente
        ReadOnly = True
        TabOrder = 1
        Visible = False
      end
      object cxCheckBox1: TcxCheckBox
        Left = 560
        Top = 264
        Caption = 'Ver Recibos a Timbrar'
        TabOrder = 2
        Visible = False
        Width = 145
      end
      object cxGroupBox2: TcxGroupBox
        Left = 0
        Top = 0
        Align = alCustom
        Caption = ' Periodo: '
        TabOrder = 3
        Height = 145
        Width = 669
        object PeriodoTipoLbl: TLabel
          Left = 29
          Top = 22
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
        end
        object Label6: TLabel
          Left = 41
          Top = 93
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
        end
        object PeriodoNumeroLBL: TLabel
          Left = 13
          Top = 46
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label8: TLabel
          Left = 34
          Top = 69
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'Del:'
        end
        object FechaInicial: TZetaTextBox
          Left = 56
          Top = 67
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FechaFinal: TZetaTextBox
          Left = 56
          Top = 91
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object iNumeroNomina: TZetaTextBox
          Left = 56
          Top = 44
          Width = 65
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sDescripcion: TZetaTextBox
          Left = 130
          Top = 44
          Width = 255
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object iTipoNomina: TZetaTextBox
          Left = 56
          Top = 20
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object ZetaDBTextBox2: TZetaDBTextBox
          Left = 56
          Top = 114
          Width = 201
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'PE_TIMBRO'
          DataSource = DatasetPeriodo
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
    object dxWizardControlPageRecibos: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Lista de los recibos que se enviaran a timbrar'
      Header.Title = 'Recibos a Timbrar'
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 694
        Height = 279
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          FilterBox.CustomizeDialog = False
          DataController.DataSource = DataSourceRecibos
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnGrouping = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = 'No hay Recibos'
          OptionsView.GroupByBox = False
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = 'Empleado'
            DataBinding.FieldName = 'COLUMNA2'
            Options.Editing = False
            Width = 85
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = 'Nombre'
            DataBinding.FieldName = 'COLUMNA3'
            Width = 197
          end
          object cxGridDBColumn4: TcxGridDBColumn
            Caption = 'RFC'
            DataBinding.FieldName = 'COLUMNA5'
            Options.Editing = False
            Width = 109
          end
          object cxGridDBColumn3: TcxGridDBColumn
            Caption = 'Percepciones'
            DataBinding.FieldName = 'COLUMNA4'
            Options.Editing = False
            Width = 114
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
    object dxWizardControlPageCuentaTimbrado: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Seleccionar la Cuenta de Timbrado'
      Header.Title = 'Cuenta Sistema Timbrado'
      object cxGroupBox4: TcxGroupBox
        Left = 91
        Top = 21
        TabOrder = 0
        Height = 164
        Width = 465
        object Label2: TLabel
          Left = 24
          Top = 48
          Width = 84
          Height = 13
          Caption = 'Cuenta Timbrado:'
        end
        object ZetaDBTextBox1: TZetaDBTextBox
          Left = 112
          Top = 72
          Width = 121
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CT_ID'
          DataSource = DataSourceCuentas
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object ZetaDBTextBox3: TZetaDBTextBox
          Left = 112
          Top = 93
          Width = 273
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CT_PASSWRD'
          DataSource = DataSourceCuentas
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 57
          Top = 74
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cuenta ID:'
        end
        object lblDI_IP: TLabel
          Left = 51
          Top = 95
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Contrase'#241'a:'
        end
        object cxButton2: TcxButton
          Left = 436
          Top = 8
          Width = 29
          Height = 28
          Hint = 'Verificar conectividad'
          TabOrder = 0
          OnClick = cxButton1Click
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.NativeStyle = True
          LookAndFeel.SkinName = 'DevExpressDarkStyle'
          OptionsImage.Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000040C14140A1F
            3434246BB2B32877C6C70B213838050E18180000000000000000000000000000
            0000000000000000000000000000000000000000000000000000297FD2D32573
            BEBF2573BEBF2573BEBF2573BEBF2E8CEAEB0000000000000000000000000000
            00000000000000000000071624240A1D303013396060000000002573BEBF0000
            00000000000000000000000000002573BEBF0000000000000000000000000000
            00000000000000000000050E18180716242400000000000000002573BEBF0000
            00000000000000000000000000002573BEBF0000000000000000000000000000
            0000000000000000000002070C0C02070C0C0000000000000000297ACACB0D26
            40400D2640400D2640400D264040297FD2D30000000000000000000000000000
            00000000000000000000071624240C243C3C00000000000000000C243C3C1339
            60601339606013396060133960600E2B48480000000000000000000000000000
            00000000000006111C1C16416C6C16416C6C06111C1C00000000000000000000
            0000050E18180A1D303000000000000000000000000000000000000000000000
            00001A508687205F9E9F297FD2D32C83DADB205F9E9F1A508687000000000000
            0000050E18180818282800000000000000000000000000000000000000000000
            00002D8BE6E70D2640400D2640400D2640400D264040297FD2D3000000000000
            000002070C0C040C141400000000000000000000000000000000000000000000
            00002D86DEDF000000000000000000000000000000002573BEBF000000000E2B
            4848071624240C243C3C00000000000000000000000000000000000000000000
            00002D86DEDF000000000000000000000000000000002573BEBF000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000297BCECF2573BEBF2573BEBF2573BEBF2573BEBF297ACACB000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000}
        end
        object CT_CODIGO: TZetaKeyLookup
          Left = 112
          Top = 45
          Width = 300
          Height = 21
          Enabled = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          OnValidLookup = CT_CODIGOValidLookup
        end
      end
    end
    object dxWizardControlPageTimbrado: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Presione bot'#243'n Ejecutar para inciar el Proceso'
      Header.Title = 'Timbrado de Nomina'
      object cxGroupBox1: TcxGroupBox
        Left = 16
        Top = 8
        Caption = ' Env'#237'o y Procesamiento de Recibos '
        TabOrder = 0
        Height = 74
        Width = 649
        object cxLabel4: TcxLabel
          Left = 24
          Top = 19
          Caption = 'Avance:'
        end
        object cxProgressBarEnvio: TcxProgressBar
          Left = 32
          Top = 43
          TabOrder = 1
          Width = 593
        end
        object cxStatus: TcxLabel
          Left = 32
          Top = 72
        end
      end
      object cxGroupBoxResultados: TcxGroupBox
        Left = 16
        Top = 88
        Caption = ' Respuesta del Sistema de Timbrado'
        TabOrder = 1
        Height = 154
        Width = 649
        object ZetaSpeedButton1: TZetaSpeedButton
          Left = 13
          Top = 13
          Width = 25
          Height = 25
          Hint = 'Exportar'
          Flat = True
          Glyph.Data = {
            36030000424D3603000000000000360200002800000010000000100000000100
            08000000000000010000C40E0000C40E0000800000008000000049935F003A73
            4C00265D38002C52300055A45A0058966B005F9B7200336D470094C79C0066A0
            77006CA67C009DD0A700AAD6B20071AA810057825A007CB58A002C6642001E53
            3100E6F0E6004B964D00EDF5ED003D8C570032873500E4EFE400DCEADB00F9FB
            FA00417B570076AF850081BC9000EBF3EB00FDFEFD00DFECDF0004690400EFF6
            EF004D876200F4F9F400F1F7F100358C4F00FCFDFB00E2EEE300D8E7D800F6FA
            F600F3F8F3001A4D2A005CAA65006DB67500F5F9F500DCEADD001D781E00E9F2
            E90073BB7C0081AA8D00FEFEFE00558E67008AB7950064AC69006199640068B0
            6F00F8FAF800DEEBDE00F6F9F600FFFFFF00DAE9DA0047815D00E8F1E800E1ED
            E1007CC18500EAF1E900D9E8D90063B276004093470042874900FCFDFC00FEFF
            FE00579A5E000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000010101070707
            1010100202021111112B010A090906060505000000151525252B1A0A1D401217
            271F3B2F18442828252B1A0D141D431217271F3B18183E2815113F1B21141D31
            401733030303033E1511220F2A0403030303302D001302180002351C23240445
            043042001302332F00020536292E2A04161C00130238381F0002060819292E16
            082C040720122741051009082619460B2D2C47041620121705100A0B48130C32
            32000E390416201206070D0B343937374A3C2A0E0E0E0E3106071B0C341E1E26
            193A3C232421141D09010F0C3D3D491E26193A292324211409010F0C0C0B0B08
            0808361C0F1B0D0D0A011C0F1B0D0A0906053522223F1A1A0101}
          ParentShowHint = False
          ShowHint = True
          OnClick = ZetaSpeedButton1Click
        end
        object cxLabel3: TcxLabel
          Left = 32
          Top = 72
        end
        object DBGrid1: TDBGrid
          Left = 14
          Top = 40
          Width = 625
          Height = 104
          DataSource = DataSourceErrores
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NUMERO'
              Title.Caption = 'N'#250'mero'
              Width = 55
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ERRORID'
              Title.Caption = 'Error ID'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DESCRIPCION'
              Title.Caption = 'Descripci'#243'n'
              Width = 208
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DETALLE'
              Title.Caption = 'Detalle'
              Width = 391
              Visible = True
            end>
        end
      end
    end
  end
  object dataSourceTimbrar: TDataSource
    Left = 576
    Top = 56
  end
  object dataSourceContribuyente: TDataSource
    Left = 624
    Top = 56
  end
  object DataSourceCuentas: TDataSource
    Left = 552
    Top = 56
  end
  object DataSourceRecibos: TDataSource
    Left = 496
    Top = 56
  end
  object DataSourceErrores: TDataSource
    Left = 672
    Top = 56
  end
  object DatasetPeriodo: TDataSource
    Left = 392
    Top = 56
  end
end
