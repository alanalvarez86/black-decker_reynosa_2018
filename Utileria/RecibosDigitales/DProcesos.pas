unit DProcesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifndef VER130}Variants,{$endif}
{$ifdef DOS_CAPAS}
     DServerIMSS,
     DServerAsistencia,
     DServerNomina,
     DServerCalcNomina,
     DServerAnualNomina,
     DServerSistema,
     DServerRecursos,
{$ifdef TIMBRADO}
     DServerNominaTimbrado,
     DServerLogin,
{$endif}
     DServerCafeteria,
{$else}
     IMSS_TLB,
     Asistencia_TLB,
     DAnualNomina_TLB,
     DCalcNomina_TLB,
     Nomina_TLB,
{$ifdef TIMBRADO}
     TimbradoNomina_TLB,
     Login_TLB,
{$endif}
     Recursos_TLB,
     Sistema_TLB,
     Cafeteria_TLB,
{$endif}
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaAsciiFile,
     ZetaClientDataSet,
     ZBaseThreads;

type
  TdmProcesos = class(TDataModule)
    cdsDataSet: TZetaClientDataSet;
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    cdsDeclaraGlobales: TZetaClientDataSet;
    cdsDataSetTimbrados: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
{$ifndef DOS_CAPAS}
    FServidorIMSS: IdmServerIMSSDisp;
    FServidorAsistencia: IdmServerAsistenciaDisp;
    {$ifdef TIMBRADO}
    FServidorNomina: IdmServerNominaTimbradoDisp;
    FServidorLogin: IdmServerLoginDisp;
    {$else}
     FServidorNomina: IdmServerNominaDisp;
    {$endif}
    FServidorCalcNomina: IdmServerCalcNominaDisp;
    FServidorAnualNomina: IdmServerAnualNominaDisp;
    FServidorRecursos: IdmServerRecursosDisp;
    FServidorSistema: IdmServerSistemaDisp;
    FServidorCafeteria: IdmServerCafeteriaDisp;
{$endif}
    FErrorCount: Integer;
    FFechaLimiteMovSUA : TDate;
    function AgregaMensaje(const sMensaje, sValor: String): String;
    function Check(const Resultado: OleVariant): Boolean;
    function CheckSilencioso(const Resultado: OleVariant; const lNomina: Boolean ): Boolean;
    function CorreThreadAnual(const eProceso: Procesos; const Lista: OleVariant; const lCargarPeriodo: Boolean; Parametros: TZetaParams): Boolean;
    function CorreThreadAsistencia( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
    function CorreThreadCalcNomina(const eProceso: Procesos; const Lista: OleVariant; Parametros : TZetaParams): Boolean;
    function CorreThreadCalcNominaBDE(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadCalcNominaSilent(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadIMSS( const eProceso: Procesos; Parametros: TZetaParams ): Boolean; overload;
    function CorreThreadIMSS( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean; overload;
    function CorreThreadNomina(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadNominaSilent(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
    function CorreThreadNominaSilentConciliacion(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams; Ventana : HWND;SetResultado:TSetResultadoEspecial): Boolean;
    function CorreThreadRecursos(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadCafeteria( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
    function CorreThreadSistema(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadSistemaSilent(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams; Ventana : HWND;SetResultado:TSetResultadoEspecial): Boolean;
    function ExportarMovEnd(const Parametros, Datos: OleVariant): String;
    function ExportarSUAEnd(const Parametros, Empleados, Movimientos,Infonavit,Incapacidades: OleVariant): String;
    function CalcularPrimaRiesgoEnd(const Parametros, Datos: OleVariant): String;
    function RevisarDatosSTPSEnd(const Parametros, Datos: OleVariant): String;
    function GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
    function ExtraerChecadasBitTerminalesEnd(const Parametros, Datos: OleVariant): String;
    function RecuperaChecadasBitTerminalesEnd( const Parametros, Datos: OleVariant ): String;



{$ifdef TIMBRADO}
   {$ifdef DOS_CAPAS}
       function GetServerNomina: TdmServerNominaTimbrado;
       property ServerNomina: TdmServerNominaTimbrado read GetServerNomina;
       function GetServerLogin: IdmServerLogin;
   {$else}
       function GetServerNomina: IdmServerNominaTimbradoDisp;
       property ServerNomina: IdmServerNominaTimbradoDisp read GetServerNomina;
       function GetServerLogin: IdmServerLoginDisp;
   {$endif}

{$else}
   {$ifdef DOS_CAPAS}
       function GetServerNomina: TdmServerNomina;
       property ServerNomina: TdmServerNomina read GetServerNomina;
       function GetServerNomina: TdmServerNominaTimbrado;
   {$else}
       function GetServerNomina: IdmServerNominaDisp;
       property ServerNomina: IdmServerNominaDisp read GetServerNomina;
       function GetServerLogin: IdmServerLoginDisp;
   {$endif}
{$endif}

{$ifdef DOS_CAPAS}
    function GetServerIMSS: TdmServerIMSS;
    function GetServerAsistencia: TdmServerAsistencia;
    function GetServerCalcNomina: TdmServerCalcNomina;
    function GetServerAnualNomina: TdmServerAnualNomina;
    function GetServerRecursos: TdmServerRecursos;
    function GetServerSistema: TdmServerSistema;
    function GetServerCafeteria: TdmServerCafeteria;
{$else}
    function GetServerIMSS: IdmServerIMSSDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    function GetServerAnualNomina: IdmServerAnualNominaDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    function GetServerCafeteria: IdmServerCafeteriaDisp;
{$endif}
    function ProcesarResultado( Info: TProcessInfo ): String;
    function RastrearEnd(const Parametros, Rastreo: OleVariant): String;
    function NumeroTarjetaEnd( const Bitacora: OleVariant ): String;
    procedure ExportarMovEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    procedure ExportarSUAEmpleados( Sender: TAsciiExport; DataSet: TDataset );
    procedure ExportarSUAMovimientos( Sender: TAsciiExport; DataSet: TDataset );
    procedure ValidacionesAfterOpen(DataSet: TDataSet);
    procedure CB_CODIGOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ExportarSUADatosAfiliatorios(Sender: TAsciiExport;DataSet: TDataset);
    procedure ExportarSUAIncapacidades(Sender: TAsciiExport;DataSet: TDataset);
    procedure ExportarSUAInfonavit(Sender: TAsciiExport;DataSet: TDataset);
    function CrearAsciiDatosAfiliatorios(cdsDataSet: TZetaClientDataSet;const sArchivoDatosAfiliatorios: string): String;
    function CrearAsciiIncapacidades(cdsDataSet: TZetaClientDataSet;const sArchivoIncapacidades: string): String;
    function CrearAsciiMovimientos(cdsDataSet: TZetaClientDataSet;const sArchivoMovimientos: string): String;
    function CrearAsciiEmpleados (cdsDataSet: TZetaClientDataSet;const sArchivoEmpleados: string):String;
    function CrearAsciiInfonavit (cdsDataSet: TZetaClientDataSet;const sArchivoInfonavit: string):String;
    procedure LlenaParametrosCalculo( oParametros: TZetaParams; const lNomina: Boolean );
    procedure ExtraerChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    procedure RecuperaChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    function ImportaTerminalesEnd( const Bitacora: OleVariant ): String;

  protected
    { Protected declarations }
  public
    { Public declarations }
    FListaProcesos: TProcessList;
    property ErrorCount: Integer read FErrorCount;
{$ifdef DOS_CAPAS}
    property ServerIMSS: TdmServerIMSS read GetServerIMSS;
    property ServerAsistencia: TdmServerAsistencia  read GetServerAsistencia;

    property ServerCalcNomina: TdmServerCalcNomina  read GetServerCalcNomina;
    property ServerAnualNomina: TdmServerAnualNomina  read GetServerAnualNomina;
    property ServerRecursos: TdmServerRecursos  read GetServerRecursos;
    property ServerSistema: TdmServerSistema  read GetServerSistema;
    property ServerCafeteria: TdmServerCafeteria read GetServerCafeteria;
    property ServerLogin: TdmServerLogin read GetServerLogin;
{$else}
    property ServerIMSS: IdmServerIMSSDisp read GetServerIMSS;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
 
    property ServerCalcNomina: IdmServerCalcNominaDisp read GetServerCalcNomina;
    property ServerAnualNomina: IdmServerAnualNominaDisp read GetServerAnualNomina;
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    property ServerCafeteria: IdmServerCafeteriaDisp read GetServerCafeteria;
    property ServerLogin: IdmServerLoginDisp read GetServerLogin;
{$endif}
    function BorrarBajas(Parametros: TZetaParams): Boolean;
    function BorrarTarjetas(Parametros: TZetaParams): Boolean;
    function BorrarNominas(Parametros: TZetaParams): Boolean;
    function BorrarTimbradoNominas(Parametros: TZetaParams): Boolean;
    function BorrarPoll(Parametros: TZetaParams): Boolean;
    function BorrarBitacora(Parametros: TZetaParams): Boolean;
    function BorrarHerramientas(Parametros: TZetaParams): Boolean;
    function CalcularPagosIMSS(Parametros: TZetaParams): Boolean;
    function CalcularRecargosIMSS(Parametros: TZetaParams): Boolean;
    function CalcularTasaINFONAVIT(Parametros: TZetaParams): Boolean;
    function CalcularPrimaRiesgo(Parametros: TZetaParams): Boolean;
    function CerrarAhorros(Parametros: TZetaParams): Boolean;
    function CerrarPrestamos(Parametros: TZetaParams): Boolean;
    function RevisarSUA(Parametros: TZetaParams): Boolean;
    function AfectarNomina(Parametros: TZetaParams): Boolean;
    function DesafectarNomina(Parametros: TZetaParams): Boolean;
    function FoliarRecibos(Parametros: TZetaParams): Boolean;
    function LimpiarAcum(Parametros: TZetaParams): Boolean;
    function Rastrear(Parametros: TZetaParams): Boolean;
    function RecalculoAcumulado(Parametros: TZetaParams): Boolean;
    function ReFoliarRecibos(Parametros: TZetaParams): Boolean;
    function SalNetoBruto(Parametros: TZetaParams): Boolean;
    function DefinirPeriodos(Parametros: TZetaParams): Boolean;
    function PagarAguinaldo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function CalcularRepartoAhorro(Parametros: TZetaParams): Boolean;
    function PTUCalcular(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function PTUPago(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function DeclaraCredito( Parametros: TZetaParams ): Boolean;
    function ISPTAnual( Parametros: TZetaParams ): Boolean;
    function PagarRepartoAhorro( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    function ISPTAnualPago( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    function ExportarMov(Parametros: TZetaParams): Boolean;
    function ExportarSUA( Parametros: TZetaParams ): Boolean;
    function ValidarMovimientosIDSE( Parametros: TZetaParams ): Boolean;
    function NumeroTarjeta: Boolean;
    procedure AplicarTabuladorGetList( Parametros: TZetaParams );
    procedure PromediarVariablesGetLista(Parametros: TZetaParams);
    procedure PagarAguinaldoGetLista(Parametros: TZetaParams);
    procedure PagarRepartoAhorroGetLista(Parametros: TZetaParams);
    procedure PTUPagoGetLista(Parametros: TZetaParams);
    procedure ISPTAnualPagoGetLista(Parametros: TZetaParams);
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure HandleProcessSilent(const iIndice, iValor: Integer);
    function CreditoAplicado( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure CreditoAplicadoGetLista(Parametros: TZetaParams);
    procedure SalarioIntegradoGetLista( Parametros: TZetaParams );
    procedure CambioSalarioGetLista(Parametros: TZetaParams);
    procedure EventosGetLista(Parametros: TZetaParams);
    procedure ImportarKardexGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarKardexGetLista(Parametros: TZetaParams);
    // FWizEmpCambioTurno
    function CambioMasivoTurnos(Parametros: TZetaParams; const DataSet: TZetaClientDataSet; const lVerificacion: Boolean): Boolean;
    // WizImportarAltas
    function ImportarAltas(Parametros: TZetaParams): Boolean;
    procedure ImportarAltasGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarAltasGetASCIICatalogos(Parametros: TZetaParams);
    // WizCancelarKardex
    procedure CancelarKardexGetLista(Parametros: TZetaParams);
    // WizCancelarVacaciones
    function CancelarVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarVacacionesGetLista(Parametros: TZetaParams);
    // WizCierreGlobalVacaciones
    function CierreGlobalVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CierreGlobalVacacionesGetLista(Parametros: TZetaParams);
    // WizTransferencia
    function Transferencia(Parametros: TZetaParams; const EmpresaDestino: Variant): Boolean;
    // WizAutorizarHoras
    procedure AutorizarHorasGetLista( Parametros: TZetaParams );

    //WizAsistIntercambioFestivo
    procedure IntercambioFestivoGetLista( Parametros: TZetaParams );

    //WizAsistCancelarExcepcionesFestivos
    procedure CancelarExcepcionesFestivosGetLista( Parametros: TZetaParams );

    // WizNomImportarMovimientos
    function ImportarMov(Parametros: TZetaParams): Boolean;
    procedure ImportarMovGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    // WizNomImportarPagoRecibos
    function ImportarPagoRecibos(Parametros: TZetaParams): Boolean;
    procedure ImportarPagoRecibosGetListaASCII(Parametros: TZetaParams);
    procedure ImportarPagoRecibosGetLista(Parametros: TZetaParams);
    //CV
    procedure RecalculoDias( const iTipo: Integer );
    // WizNOMPagarPorFuera
    function PagarPorFuera(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure PagarPorFueraGetLista(Parametros: TZetaParams);
    // WizNOM Poliza
    function Poliza(Parametros: TZetaParams; aFiltros : OleVariant ): Boolean;
    // WizEmpLiquidacionGlobal
    function LiquidacionGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure LiquidacionGlobalLista( Parametros: TZetaParams );
    // WizNOMCalcularAguinaldo
    function CalcularAguinaldo(Parametros: TZetaParams): Boolean;
    procedure CalcularNominaEmpleado( const lNomina : Boolean = TRUE );
    procedure CalcularSimulacionEmpleado( const TipoPeriodo: eTipoPeriodo; const iPeriodo: Integer );
    function CalcularSimulacionGlobal(const iPeriodo: Integer): Boolean;
    function LiquidacionGlobalSimulacion( Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    // WizEmpEntHerr
    function EntregarHerramienta(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure EntregarHerramientaGetLista(Parametros: TZetaParams);
    // WizEmpRegresaHerr
    function RegresarHerramienta(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure RegresarHerramientaGetLista(Parametros: TZetaParams);
    // WizEmpCursoTomado
    procedure CursoTomadoGetLista(Parametros: TZetaParams);
    // WizEmpRenumera
    procedure RenumeraEmpleadosGetLista(Parametros: TZetaParams);
    procedure RenumeraVerificaListaASCII( Parametros: TZetaParams );
    // WizEmpPermisoGlobal
    function PermisoGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure PermisoGlobalGetLista(Parametros: TZetaParams);
    //WizCalculoRetroactivos
    function CalculaRetroactivos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CalculaRetroactivoGetLista(Parametros: TZetaParams);
    // WizEmpBorrarCursoTomado
    procedure BorrarCursoTomadoGetLista(Parametros: TZetaParams);

    //WizNomDeclaracionAnual
    function DeclaracionAnual(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure DeclaracionAnualGetLista(Parametros: TZetaParams);
    procedure GetDeclaracionAnualGlobales;
    procedure GrabaDeclaracionAnualGlobales;
    //WizNomDeclaracionAnualCierre
    function DeclaracionAnualCierre(Parametros: TZetaParams): Boolean;

    //WizComidasGrupales
    function ComidasGrupales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function ComidasGrupalesListaASCII( Parametros: TZetaParams ): Boolean;
    procedure ComidasGrupalesGetLista( Parametros: TZetaParams );
    //WizEmpCorregirFechasCafe
    function CorregirFechasGlobalesCafe( Parametros: TZetaParams ): Boolean;
    // WizImportarAsistenciaSesiones
    procedure ImportarAsistenciaSesionesGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarAsistenciaSesionesGetLista(Parametros: TZetaParams);
    //WizEmpCancelarCierreVacaciones
    function CancelarCierreVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarCierreVacacionesGetLista( Parametros: TZetaParams );
    //WizEmpCancelarPermisosGlobales
    function CancelarPermisosGlobales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarPermisosGlobalesGetLista( Parametros: TZetaParams );
    procedure PermisosAfterOpen(DataSet: TDataSet);
    procedure PM_CLASIFIOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    //WizEmpImportarAhorrosPrestamos
    procedure ImportarAhorrosPrestamosGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //WizEmpRecalculaSaldosVaca
    function RecalculaSaldosVaca(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure RecalculaSaldosVacaGetLista(Parametros: TZetaParams);
    //WizNomAjusteRetFonacot
    function AjusteRetFonacot( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AjusteRetFonacotGetLista(Parametros: TZetaParams);
    //WizNomCalcularPagoFonacot
    function CalcularPagoFonacot( Parametros: TZetaParams): Boolean;
    //WizSisEnrolarUsuarios
    function EnrolarUsuarios( Parametros: TZetaParams): Boolean;
    procedure EnrolarUsuariosGetLista( Parametros: TZetaParams);
    function ImportarEnrolamientoMasivo ( Parametros: TZetaParams): Boolean;
    procedure ImportarEnrolamientoMasivoGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //WizIMSSAjusteRetInfonavit
    function AjusteRetInfonavit( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AjusteRetInfonavitGetLista(Parametros: TZetaParams);
    // WizEmpProgCursoGlobal
    function CursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CursoProgGlobalGetLista(Parametros: TZetaParams);
    //WizEmpCancelarProgCursoGlobal
    function CancelarCursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarCursoProgGlobalGetLista(Parametros: TZetaParams);
    //WizFiniquitos
    function AplicarFiniquitosGlobales( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AplicarFiniquitosGlobalGetLista(Parametros: TZetaParams);
    //WizAsistAjustIncapaCal
    function AjusteIncapaCal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure AjusteIncapaCalGetLista(Parametros: TZetaParams);
    //Wizard Foliar Capacitaciones
    function FoliarCapacitaciones( Parametros: TZetaParams ): Boolean;
    procedure FoliarCapacitacionesSTPSGetLista( Parametros: TZetaParams );
    function FoliarCapacitacionesSTPS(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    //Wizard Autorizacion PreNomina
    function AutorizacionPreNomina( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AutorizacionPreNominaGetLista(Parametros: TZetaParams);

    //WizAsistCancelarExcepcionesFestivos
    procedure RegistrarExcepcionesFestivosGetLista( Parametros: TZetaParams );

    //WizNOTransferenciasCosteo
    function TransferenciasCosteo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure TransferenciasCosteoGetLista(Parametros: TZetaParams);
    
    //WizNoCalculaCosteo
    function CalculaCosteo(Parametros: TZetaParams): Boolean;

    //WizImportarClasificacionesTemporales
    procedure ImportarClasificacionesTemporalesGetListaASCII( oParametros: TZetaParams; const lFillASCII: Boolean = TRUE);
    function ImportarClasificacionesTemporales(Parametros: TZetaParams): Boolean;
    procedure ImportarClasificacionesTemporalesGetLista(Parametros: TZetaParams);
    //WizNomCancelarTransferenciasCosteo
    function CancelaTransferenciaCosteo(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
    procedure CancelaTransferenciasCosteoGetLista(Parametros: TZetaParams);

    //FWizNomPrevioISR
    function PrevioISR(Parametros: TZetaParams): Boolean;

    {$ifndef DOS_CAPAS}
    //WizDepuraBitBiometrico
    function DepuraBitacoraBio( Parametros:TZetaParams ): Boolean;
    //WizAsignaNumBiometricos
    function AsignaNumBiometricos( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    //WizAsignaGrupoTerminales
    function AsignaGrupoTerminales( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure EmpleadosConBiometricosGetLista( Parametros: TZetaParams );
    procedure EmpleadosSinBiometricosGetLista( Parametros: TZetaParams );
    function ImportaTerminales( Parametros: TZetaParams ): Boolean;
    {$endif}
    //WizSistImportarTablas
    function ImportarTablas( Parametros: TZetaParams ): Boolean;
    //WizSistImportarTablasCSV
    procedure ImportarTablaCSVGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
 // WizRHImportarSGM
    function ImportarSGM(Parametros: TZetaParams): Boolean;
    procedure ImportarSGMGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarSGMGetLista(Parametros: TZetaParams);

    procedure RenovacionSGMGetLista(Parametros: TZetaParams);
    function RenovacionSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;

    procedure BorrarSGMGetLista(Parametros: TZetaParams);
    function BorrarSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
  //WizImportaOrganigrama
   procedure ImportarOrganigramaGetListaASCII( oParametros: TZetaParams; const lFillASCII: Boolean = TRUE);
    function ImportarOrganigrama(Parametros: TZetaParams): Boolean;
    procedure ImportarOrganigramaGetLista(Parametros: TZetaParams);

    // WizEmpValidarDC4
    function RevisarDatosSTPS (Parametros: TZetaParams): Boolean;
 
   // Wizard Actualizar Bases de Datos
    function EjecutaMotorPatch(Parametros: TZetaParams;Ventana:HWND;SetResultado:TSetResultadoEspecial): Boolean;

    //Wizard ConsiliacionTimbradoNomina
    function IniciarConciliacionTimbradoPeriodos(Parametros: TZetaParams;Ventana:HWND;SetResultado:TSetResultadoEspecial): Boolean;  // Versi�n con hilo
    function AplicarConciliacionTimbradoPeriodos(Parametros: TZetaParams;Ventana:HWND;SetResultado:TSetResultadoEspecial): Boolean;  // Versi�n con hilo

    {$IFDEF TIMBRADO}
    //WizTimbrarNomina
    function TimbrarNomina( Parametros: TZetaParams ): Boolean;
    procedure TimbrarNominaGetLista(Parametros: TZetaParams);
    {$ENDIF}

    //Obtener datos para Hint timbrado
    function GetStatusTimbrado(Parametros: TZetaParams ): Olevariant;
  end;

const
     K_VERSION_SUA_2000 = 0;
     K_VERSION_SUA_2006 = 1;
     aVersionSUA: array[0..1] of String = ( 'SUA 2000 Windows', 'SUA 2006 Windows' );

var
  dmProcesos: TdmProcesos;

procedure ShowWizard( const eProceso: Procesos );
procedure SetDataChange( const Proceso: Procesos );

implementation

uses
     ZcxWizardBasico,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaClientTools,
     ZAsciiTools,
     ZetaHayProblemas,
     ZetaDialogo,
     ZetaRegistryCliente,
     ZetaMessages,
     ZetaWizardFeedBack_DevEx,
     ZetaSystemWorking,
     FTressShell,
     DThreads,
     DGlobal,
     DCliente,
     DRecursos,
     DConsultas,
{$IFDEF TRESS_DELPHIXE5_UP}
     DBorradorSUA,
{$ENDIF}
	 dIMSS,
     {$ifdef TIMBRADO}
     DInterfase
     {$endif}
     ;

const
     K_FORMATO = '#,###,###,###,##0';

{$R *.DFM}

{ ********** Funciones Globales ************* }

procedure SetDataChange( const Proceso: Procesos );
begin
     with TressShell do
     begin
          RefrescarShellPeriodos;
     end;
end;

function GetCxWizardClass( const eProceso: Procesos ): TCXWizardBasicoClass;
begin
     Result := Nil;
end;

procedure ShowWizard( const eProceso: Procesos );
var
   WizardCXClass:TcxWizardBasicoClass;
begin
     dmProcesos.cdsDataSet.Init;
     WizardCXClass := nil;
     WizardCXClass :=  GetCxWizardClass ( eProceso );
     ZCXWizardBasico.ShowWizard( WizardCXClass);
     
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function FechaSUA( const dValue: TDate ): String;
begin
     Result := FormatDateTime( 'ddmmyyyy', dValue );
end;

{ *********** TdmProcesos ************ }

procedure TdmProcesos.DataModuleCreate(Sender: TObject);
begin
     FListaProcesos := TProcessList.Create;
end;

procedure TdmProcesos.DataModuleDestroy(Sender: TObject);
begin
     FListaProcesos.Free;
end;

procedure TdmProcesos.HandleProcessSilent(const iIndice, iValor: Integer);
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             VerBitacora := FALSE;
             OcultarForma := dmCliente.FormsMoved;
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );
             ShowProcessInfo( True );
             SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure TdmProcesos.HandleProcessEnd( const iIndice, iValor: Integer );
begin
with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             OcultarForma := dmCliente.FormsMoved;
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );

             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );

             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function TdmProcesos.Check( const Resultado: OleVariant): Boolean;
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with ProcessData do
             begin
                  SetResultado( Resultado );
                  Result := ( Status in [ epsEjecutando, epsOK ] );
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
end;

function TdmProcesos.CheckSilencioso(const Resultado: OleVariant; const lNomina: Boolean ): Boolean;
begin
    if eProcessStatus( Resultado[ K_PROCESO_STATUS ] ) = epsOK then
    begin
         Result := TRUE;
         with TressShell do
         begin
              if lNomina then
                 SetDataChange( [ enPeriodo, enNomina, enMovimien ] )
              else
                 SetDataChange( [ enPeriodo, enNomina, enAusencia ] );
              if Global.GetGlobalBooleano( K_GLOBAL_PLAN_VACA_PRENOMINA ) then
                 SetDataChange( [ enEmpleado, enVacacion, enPlanVacacion ] );
         end;
    end
    else
        Result := Check( Resultado );
end;

function TdmProcesos.ProcesarResultado( Info: TProcessInfo ): String;
begin
     with Info do
     begin
          case Proceso of
               prIMSSExportarSua:
               begin
                    Result := ExportarSUAEnd( Cargo[ 0 ], Cargo[ 1 ], Cargo[ 2 ],Cargo[ 3 ],Cargo[ 4 ] );
               end;
               prIMSSCalculoPrima:
               begin
                    Result := CalcularPrimaRiesgoEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end; 
               prRHRevisarDatosSTPS:
               begin
                    Result := RevisarDatosSTPSEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prNOExportarMov:
               begin
                    Result := ExportarMovEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prNORastrearCalculo:
               begin
                    Result := RastrearEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prNODefinirPeriodos:
               begin
                    { Posicionar sobre el Per�odo 1 del Tipo Generado }
                    dmCliente.PeriodoTipo := eTipoPeriodo( Cargo );

                    Result := '';
               end;
               prSISTNumeroTarjeta:
               begin
                    Result := NumeroTarjetaEnd( Cargo );
               end;


               prSISTImportaTerminales:
               begin
                    Result := ImportaTerminalesEnd( Cargo );
               end;

          else
              Result := '';
          end;
     end;
end;

{$ifdef DOS_CAPAS}
function TdmProcesos.GetServerIMSS: TdmServerIMSS;
begin
     Result := DCliente.dmCliente.ServerIMSS;
end;

function TdmProcesos.GetServerAsistencia: TdmServerAsistencia;
begin
     Result := DCliente.dmCliente.ServerAsistencia;
end;


{$ifdef TIMBRADO}

function TdmProcesos.GetServerNomina: TdmServerNominaTimbrado;
begin
     Result := dmInterfase.ServerNominaTimbrado;
end;
{$else}

function TdmProcesos.GetServidorNomina: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;
{$endif}

function TdmProcesos.GetServerCalcNomina: TdmServerCalcNomina;
begin
     Result := DCliente.dmCliente.ServerCalcNomina;
end;

function TdmProcesos.GetServerAnualNomina: TdmServerAnualNomina;
begin
     Result := DCliente.dmCliente.ServerAnualNomina;
end;

function TdmProcesos.GetServerRecursos: TdmServerRecursos;
begin
     Result := DCliente.dmCliente.ServerRecursos;
end;

function TdmProcesos.GetServerSistema: TdmServerSistema;
begin
     Result := DCliente.dmCliente.ServerSistema;
end;

function TdmProcesos.GetServerCafeteria: TdmServerCafeteria;
begin
     Result := DCliente.dmCliente.ServerCafeteria;
end;

{$else}
function TdmProcesos.GetServerIMSS: IdmServerIMSSDisp;
begin
     Result := IdmServerIMSSDisp( dmCliente.CreaServidor( CLASS_dmServerIMSS, FServidorIMSS ) );
end;

function TdmProcesos.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServidorAsistencia ) );
end;

{$ifdef TIMBRADO}
function TdmProcesos.GetServerNomina: IdmServerNominaTimbradoDisp;
begin
     Result := IdmServerNominaTimbradoDisp( dmCliente.CreaServidor( CLASS_dmServerNominaTimbrado, FServidorNomina  ) );
end;

function TdmProcesos.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( dmCliente.CreaServidor( CLASS_dmServerLogin, FServidorLogin ) );
end;
{$else}
function TdmProcesos.GetServerNomina: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( dmCliente.CreaServidor( CLASS_dmServerNomina, FServidorNomina ) );
end;
{$endif}

function TdmProcesos.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( CLASS_dmServerCalcNomina, FServidorCalcNomina ) );
end;

function TdmProcesos.GetServerAnualNomina: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( dmCliente.CreaServidor( CLASS_dmServerAnualNomina, FServidorAnualNomina ) );
end;

function TdmProcesos.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result := IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidorRecursos ) );
end;

function TdmProcesos.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServidorSistema ) );
end;

function TdmProcesos.GetServerCafeteria: IdmServerCafeteriaDisp;
begin
     Result := IdmServerCafeteriaDisp( dmCliente.CreaServidor( CLASS_dmServerCafeteria, FServidorCafeteria ) );
end;
{$endif}

function TdmProcesos.GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

function TdmProcesos.AgregaMensaje( const sMensaje, sValor: String ): String;
begin
     Result := sMensaje + CR_LF + sValor;
end;

{ ********** Procesos de Asistencia ********** }

function TdmProcesos.CorreThreadAsistencia(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TAsistenciaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;


function TdmProcesos.BorrarTarjetas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prSISTBorrarTarjetas, NULL, Parametros );
end;

function TdmProcesos.BorrarPoll(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prSISTBorrarPOLL, NULL, Parametros );
end;

procedure TdmProcesos.AutorizarHorasGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.AutorizarHorasGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.RegistrarExcepcionesFestivosGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.RegistrarExcepcionesFestivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.CancelarExcepcionesFestivosGetLista( Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.CancelarExcepcionesFestivosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.IntercambioFestivoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.IntercambioFestivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.AjusteIncapaCal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASIAjustIncapaCal, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASIAjustIncapaCal, NULL, Parametros );
end;

procedure TdmProcesos.AjusteIncapaCalGetLista(Parametros: TZetaParams);
begin
     cdsDataset.Data := ServerAsistencia.AjustarIncapaCalGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AutorizacionPreNomina( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASISAutorizacionPreNomina, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASISAutorizacionPreNomina, NULL, Parametros );
end;

procedure TdmProcesos.AutorizacionPreNominaGetLista(Parametros: TZetaParams);
begin
    cdsDataset.Data := ServerAsistencia.AutorizacionPreNominaGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

{JB: 20111031 Importacion de Clasificaciones}

function TdmProcesos.ImportarClasificacionesTemporales(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prASISImportacionClasificaciones, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.ImportarClasificacionesTemporalesGetListaASCII( oParametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, oParametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerAsistencia.GetClasificacionTempLista( dmCliente.Empresa, oParametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;     
end;

procedure TdmProcesos.ImportarClasificacionesTemporalesGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then
          ClearErroresASCII( cdsDataset );
     with cdsDataset do
     begin
          Data := ServerAsistencia.GetClasificacionTempGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataset ) );
     end;
end;

{ *************** Procesos de CalcNomina ************* }

function TdmProcesos.CorreThreadCalcNomina(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TCalcNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.Rastrear(Parametros: TZetaParams ): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadCalcNomina( prNORastrearCalculo, NULL, Parametros );
end;

function TdmProcesos.RastrearEnd( const Parametros, Rastreo: OleVariant ): String;
var
   sArchivo: String;
begin
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sArchivo := ParamByName( 'Archivo' ).AsString;
          finally
                 Free;
          end;
     end;
     with TStringList.Create do
     begin
          try
             Text := Rastreo;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;

{ ************ Fin de Procesos de CalcNomina ********** }

{ *************** Procesos de CalcNominaBDE ************* }

function TdmProcesos.CorreThreadCalcNominaBDE(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TCalcNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadCalcNominaSilent(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TCalcNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          SilentExecute := TRUE;
          Resume;
     end;
     Result := True;
end;

{ ************ Fin de Procesos de CalcNominaBDE ********** }

{ *********** Procesos de IMSS ************* }

function TdmProcesos.CorreThreadIMSS(const eProceso: Procesos; Parametros: TZetaParams ): Boolean;
begin
     dmCliente.CargaActivosIMSS( Parametros );
     with TIMSSThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadIMSS(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     dmCliente.CargaActivosIMSS( Parametros );
     with TIMSSThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CalcularPagosIMSS(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadIMSS( prIMSSCalculoPagos, Parametros );
end;

function TdmProcesos.CalcularRecargosIMSS( Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSCalculoRecargos, Parametros );
end;

function TdmProcesos.CalcularTasaINFONAVIT(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSTasaINFONAVIT, Parametros );
end;

function TdmProcesos.RevisarSUA(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSRevisarSUA, Parametros );
end;

function TdmProcesos.ExportarSUA(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSExportarSUA, Parametros );
end;

function TdmProcesos.ValidarMovimientosIDSE(Parametros: TZetaParams): Boolean;
begin
       Result := CorreThreadIMSS( prIMSSValidacionMovimientosIDSE, GetLista( dmIMSS.cdsMovimientosIDSE ), Parametros );      
end;

procedure TdmProcesos.ExportarSUAEmpleados( Sender: TAsciiExport; DataSet: TDataset );
var
   rPrestamo: TPesos;
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'SUA_E_PAT' ).Datos := FieldByName( 'SUA_E_PAT' ).AsString;    { RegistroPatronal }
               ColumnByName( 'SUA_E_SS' ).Datos := FieldByName( 'SUA_E_SS' ).AsString;      { NumeroSeguro }
               ColumnByName( 'SUA_E_RFC' ).Datos := FieldByName( 'SUA_E_RFC' ).AsString;    { Registro Federal de Causantes }
               ColumnByName( 'SUA_E_CUR' ).Datos := FieldByName( 'SUA_E_CUR' ).AsString;    { # CURP }
               ColumnByName( 'SUA_E_NOMB' ).Datos := FieldByName( 'SUA_E_NOMB' ).AsString;  { Nombre del Asegurado }
               ColumnByName( 'SUA_E_MODP' ).Datos := IntToStr( FieldByName( 'SUA_E_MODP' ).AsInteger );   { M�dulo Patr�n }
               ColumnByName( 'SUA_E_JORN' ).Datos := IntToStr( FieldByName( 'SUA_E_JORN' ).AsInteger );   { Tipo Jornada }
               ColumnByName( 'SUA_E_INGR' ).Datos := FechaSUA( FieldByName( 'SUA_E_INGR' ).AsDateTime );  { Fecha Ingreso }
               ColumnByName( 'SUA_E_WAGE' ).Datos := ZetaCommonTools.StrToZero( 100 * FieldByName( 'SUA_E_WAGE' ).AsFloat, 7, 0 ); { Salario SUA }
               ColumnByName( 'SUA_E_ID' ).Datos := FieldByName( 'SUA_E_ID' ).AsString;      { Indentificaci�n }
               rPrestamo := FieldByName( 'SUA_E_LOAN' ).AsFloat;
               if ( rPrestamo > 0 ) then { Monto Pr�stamo INFONAVIT }
               begin
                    ColumnByName( 'SUA_E_CRED' ).Datos := FieldByName( 'SUA_E_CRED' ).AsString;               { # Cr�dito INFONAVIT }
                    ColumnByName( 'SUA_E_FE_P' ).Datos := FechaSUA( FieldByName( 'SUA_E_FE_P' ).AsDateTime ); { Fecha Pr�stamo INFONAVIT }
                    ColumnByName( 'SUA_E_TIPO' ).Datos := IntToStr( FieldByName( 'SUA_E_TIPO' ).AsInteger );  { Tipo Pr�stamo INFONAVIT }

                    if eTipoInfonavit( FieldByName( 'SUA_E_TIPO' ).AsInteger ) = tiCuotaFija then
                       ColumnByName( 'SUA_E_LOAN' ).Datos := ZetaCommonTools.StrToZero( Round( 1000 * rPrestamo ), 8, 0 ) { Monto Pr�stamo INFONAVIT de Cuota Fija en la Forma DDDDD.DD0 }
                    else
                        ColumnByName( 'SUA_E_LOAN' ).Datos := ZetaCommonTools.StrToZero( Round( 10000 * rPrestamo ), 8, 0 ); { Monto Pr�stamo INFONAVIT }
               end
               else
               begin
                    ColumnByName( 'SUA_E_CRED' ).Datos := Space( 10 );
                    ColumnByName( 'SUA_E_FE_P' ).Datos := Space( 8 );
                    ColumnByName( 'SUA_E_TIPO' ).Datos := Space( 1 );
                    ColumnByName( 'SUA_E_LOAN' ).Datos := ZetaCommonTools.StrToZero( 0, 8, 0 );
               end;
          end;
     end;
end;

function TdmProcesos.CrearAsciiEmpleados (cdsDataSet: TZetaClientDataSet;const sArchivoEmpleados:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoEmpleados, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  AgregaColumna( 'SUA_E_PAT', tgTexto, 11 );     { RegistroPatronal }
                  AgregaColumna( 'SUA_E_SS', tgTexto, 11 );      { NumeroSeguro }
                  AgregaColumna( 'SUA_E_RFC', tgTexto, 13 );     { Registro Federal de Causantes }
                  AgregaColumna( 'SUA_E_CUR', tgTexto, 18 );     { # CURP }
                  AgregaColumna( 'SUA_E_NOMB', tgTexto, 50 );    { Nombre del Asegurado }
                  AgregaColumna( 'SUA_E_MODP', tgNumero, 1 );    { M�dulo Patr�n }
                  AgregaColumna( 'SUA_E_JORN', tgNumero, 1 );    { Tipo Jornada }
                  AgregaColumna( 'SUA_E_INGR', tgTexto, 8 );     { Fecha Ingreso }
                  AgregaColumna( 'SUA_E_WAGE', tgFloat, 7 );     { Salario SUA }
                  AgregaColumna( 'SUA_E_ID', tgTexto, 17 );      { Indentificaci�n }
                  AgregaColumna( 'SUA_E_CRED', tgTexto, 10 );    { # Cr�dito INFONAVIT }
                  AgregaColumna( 'SUA_E_FE_P', tgTexto, 8 );     { Fecha Pr�stamo INFONAVIT }
                  AgregaColumna( 'SUA_E_TIPO', tgNumero, 1 );    { Tipo Pr�stamo INFONAVIT }
                  AgregaColumna( 'SUA_E_LOAN', tgTexto, 8 );     { Monto Pr�stamo INFONAVIT }
                  AlExportarColumnas := ExportarSUAEmpleados;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Empleados Exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUADatosAfiliatorios( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'SUA_E_PAT' ).Datos := FieldByName( 'SUA_E_PAT' ).AsString;                  { RegistroPatronal }
               ColumnByName( 'SUA_E_SS' ).Datos := FieldByName( 'SUA_E_SS' ).AsString;                    { NumeroSeguro }
               ColumnByName( 'SUA_E_CODP' ).Datos := FieldByName( 'SUA_E_CODP' ).AsString;                { C�digo Postal }
               ColumnByName( 'SUA_E_FE_N' ).Datos := FechaSUA( FieldByName( 'SUA_E_FE_N' ).AsDateTime);   { Fecha de Nacimiento  }
               ColumnByName( 'SUA_E_LNAC' ).Datos := FieldByName( 'SUA_E_LNAC' ).AsString;                { Lugar de Nacimiento }
               ColumnByName( 'SUA_E_CNAC' ).Datos := FieldByName( 'SUA_E_CNAC' ).AsString;                { Clave de Lugar de Nacimiento }
               ColumnByName( 'SUA_E_CLIN' ).Datos := FieldByName( 'SUA_E_CLIN' ).AsString;                { Cl�nica [Unidad de medicina Familiar UMF] }
               ColumnByName( 'SUA_E_OCUP' ).Datos := FieldByName( 'SUA_E_OCUP' ).AsString;                { Ocupaci�n 'Empleado' }
               ColumnByName( 'SUA_E_SEXO' ).Datos := FieldByName( 'SUA_E_SEXO' ).AsString;                { Sexo [M � F]}
               ColumnByName( 'SUA_E_T_SA' ).Datos := IntToStr( FieldByName( 'SUA_E_T_SA' ).AsInteger );     { Tipo de salario }
               ColumnByName( 'SUA_E_HORA' ).Datos := FieldByName( 'SUA_E_HORA' ).AsString;                { Hora }
          end;
     end;
end;

function TdmProcesos.CrearAsciiDatosAfiliatorios (cdsDataSet: TZetaClientDataSet;const sArchivoDatosAfiliatorios:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoDatosAfiliatorios, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;

                  AgregaColumna( 'SUA_E_PAT', tgTexto, 11 );   { RegistroPatronal }
                  AgregaColumna( 'SUA_E_SS', tgTexto, 11 );    { NumeroSeguro }
                  AgregaColumna( 'SUA_E_CODP',tgTexto,5 );    { C�digo Postal }
                  AgregaColumna( 'SUA_E_FE_N',tgTexto,8 );      { Fecha de Nacimiento }
                  AgregaColumna( 'SUA_E_LNAC',tgTexto,25 );    { Lugar de Nacimiento }
                  AgregaColumna( 'SUA_E_CNAC',tgTexto,2 );    { Clave de Lugar de Nacimiento }
                  AgregaColumna( 'SUA_E_CLIN',tgTexto,3 );    { Cl�nica [Unidad de medicina Familiar UMF] }
                  AgregaColumna( 'SUA_E_OCUP',tgTexto,12 );    { Ocupaci�n 'Empleado'}
                  AgregaColumna( 'SUA_E_SEXO',tgTexto,1 );     { Sexo [M � F] }
                  AgregaColumna( 'SUA_E_T_SA',tgNumero,1 );    { Tipo de salario }
                  AgregaColumna( 'SUA_E_HORA',tgTexto,1 );     { Hora  }

                  AlExportarColumnas := ExportarSUADatosAfiliatorios;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Datos afiliatorios exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUAInfonavit( Sender: TAsciiExport; DataSet: TDataset );
var sValorDescuento :string;
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'SUA_F_PAT' ).Datos := FieldByName( 'SUA_F_PAT' ).AsString;                  { RegistroPatronal }
               ColumnByName( 'SUA_F_SS' ).Datos := FieldByName( 'SUA_F_SS' ).AsString;                    { NumeroSeguro }

               ColumnByName( 'SUA_F_CRED' ).Datos := FieldByName( 'SUA_F_CRED' ).AsString;               { # Credito Infonavit }
               ColumnByName( 'SUA_F_TMOV' ).Datos := FieldByName( 'SUA_F_TMOV' ).AsString;      { Tipo de Movimiento  }
               ColumnByName( 'SUA_F_FMOV' ).Datos := FechaSUA( FieldByName( 'SUA_F_FMOV' ).AsDateTime );    { Fecha de Movimiento }
               ColumnByName( 'SUA_F_TDES' ).Datos := IntToStr( FieldByName( 'SUA_F_TDES' ).AsInteger );   { Tipo de Descuento }


                 if eTipoInfonavit( FieldByName( 'SUA_F_TDES' ).AsInteger ) = tiCuotaFija then
                 begin
                       //otra idea : ColumnByName( 'SUA_F_VDES' ).Datos := ZetaCommonTools.StrToZero( Round( 1000 * FieldByName( 'SUA_F_VDES' ).AsFloat ), 8, 0 )
                       { Monto Pr�stamo INFONAVIT de Cuota Fija en la Forma DDDDD.DD0 }
                       sValorDescuento := ZetaCommonTools.StrToZero( FieldByName( 'SUA_F_VDES' ).AsFloat, 9, 3 );  //9 = digitos + punto
                       ColumnByName( 'SUA_F_VDES' ).Datos := StrLeft(sValorDescuento,5)+StrRight(sValorDescuento,3); { Valor de Descuento }
                 end
                 else
                 begin
                      sValorDescuento := ZetaCommonTools.StrToZero( FieldByName( 'SUA_F_VDES' ).AsFloat, 9, 4 );
                      ColumnByName( 'SUA_F_VDES' ).Datos := StrLeft(sValorDescuento,4)+StrRight(sValorDescuento,4); { Valor de Descuento }
                 end;

               ColumnByName( 'SUA_F_DISM' ).Datos := FieldByName( 'SUA_F_DISM' ).AsString;                { Aplica Dism %  }
          end;
     end;
end;

function TdmProcesos.CrearAsciiInfonavit (cdsDataSet: TZetaClientDataSet;const sArchivoInfonavit:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoInfonavit, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;

                  AgregaColumna( 'SUA_F_PAT',tgTexto,11 );    { RegistroPatronal }
                  AgregaColumna( 'SUA_F_SS',tgTexto,11 );     { NumeroSeguro }
                  AgregaColumna( 'SUA_F_CRED',tgTexto,10 );  { # Credito Infonavit }
                  AgregaColumna( 'SUA_F_TMOV',tgTexto,2 );   { Tipo de Movimiento }
                  AgregaColumna( 'SUA_F_FMOV',tgTexto,8 );    { Fecha de Movimiento }
                  AgregaColumna( 'SUA_F_TDES',tgNumero,1 );   { Tipo de Descuento }
                  AgregaColumna( 'SUA_F_VDES',tgTexto,8 );   { Valor de Descuento }
                  AgregaColumna( 'SUA_F_DISM',tgTexto, 1 );   { Aplica Dism % }

                  AlExportarColumnas := ExportarSUAInfonavit;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Datos de Infonavit exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUAMovimientos( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               if ( FieldByName( 'SUA_M_DATE' ).AsDateTime >= FFechaLimiteMovSUA ) then
               begin
                    ColumnByName( 'SUA_M_PAT' ).Datos := FieldByName( 'SUA_M_PAT' ).AsString;                    { RegistroPatronal }
                    ColumnByName( 'SUA_M_SS' ).Datos := FieldByName( 'SUA_M_SS' ).AsString;                      { NumeroSeguro }
                    ColumnByName( 'SUA_M_TIPO' ).Datos := FieldByName( 'SUA_M_TIPO' ).AsString;                  { Tipo }
                    ColumnByName( 'SUA_M_DATE' ).Datos := FechaSUA( FieldByName( 'SUA_M_DATE' ).AsDateTime );    { Fecha };
                    ColumnByName( 'SUA_M_INCA' ).Datos := FieldByName( 'SUA_M_INCA' ).AsString;                  { Incapacidad }
                    ColumnByName( 'SUA_M_DIAS' ).Datos := ZetaCommonTools.StrToZero( FieldByName( 'SUA_M_DIAS' ).AsFloat, 2, 0 );       { Dias };
                    ColumnByName( 'SUA_M_MONT' ).Datos := ZetaCommonTools.StrToZero( 100 * FieldByName( 'SUA_M_MONT' ).AsFloat, 7, 0 ); { Monto };
               end
               else
                   Abort;   // Con una excepci�n no incluye el registro en el ASCII
          end;
     end;
end;

function TdmProcesos.CrearAsciiMovimientos (cdsDataSet: TZetaClientDataSet;const sArchivoMovimientos:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoMovimientos, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  AgregaColumna( 'SUA_M_PAT', tgTexto, 11 );     { RegistroPatronal }
                  AgregaColumna( 'SUA_M_SS', tgTexto, 11 );      { NumeroSeguro }
                  AgregaColumna( 'SUA_M_TIPO', tgTexto, 2 );     { Tipo }
                  AgregaColumna( 'SUA_M_DATE', tgTexto, 8 );     { Fecha };
                  AgregaColumna( 'SUA_M_INCA', tgTexto, 8 );     { Incapacidad }
                  AgregaColumna( 'SUA_M_DIAS', tgTexto, 2 );     { Dias }
                  AgregaColumna( 'SUA_M_MONT', tgTexto, 7 );     { Monto }
                  AlExportarColumnas := ExportarSUAMovimientos;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Movimientos exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUAIncapacidades( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               if ( FieldByName( 'SUA_I_FINI' ).AsDateTime >= FFechaLimiteMovSUA ) then
               begin
                    ColumnByName( 'SUA_I_PAT' ).Datos := FieldByName( 'SUA_I_PAT' ).AsString;                    { RegistroPatronal }
                    ColumnByName( 'SUA_I_SS' ).Datos := FieldByName( 'SUA_I_SS' ).AsString;                      { NumeroSeguro }
                    ColumnByName( 'SUA_I_TIPO' ).Datos := IntToStr( FieldByName( 'SUA_I_TIPO' ).AsInteger );      { Tipo }
                    ColumnByName( 'SUA_I_FINI' ).Datos := FechaSUA( FieldByName( 'SUA_I_FINI' ).AsDateTime );    { Fecha }
                    ColumnByName( 'SUA_I_FOLIO' ).Datos := FieldByName( 'SUA_I_FOLIO' ).AsString;                { Folio }
                    ColumnByName( 'SUA_I_DIAS' ).Datos := StrZero( IntToStr( FieldByName( 'SUA_I_DIAS' ).AsInteger ),3 );    { Dias }
                    ColumnByName( 'SUA_I_INCA' ).Datos := StrToZero( MiRound( FieldByName( 'SUA_I_INCA' ).AsFloat,0 ),3,0 );      { % Incapacidad si no tiene 0 }
                    ColumnByName( 'SUA_I_RAMA' ).Datos := IntToStr( FieldByName( 'SUA_I_RAMA' ).AsInteger );     { Rama de incapacidad }
                    ColumnByName( 'SUA_I_RSGO' ).Datos := IntToStr( FieldByName( 'SUA_I_RSGO' ).AsInteger );     { Tipo de Riesgo }
                    ColumnByName( 'SUA_I_SCLA' ).Datos := IntToStr( FieldByName( 'SUA_I_SCLA' ).AsInteger );     { Secuela o consecuencia }
                    ColumnByName( 'SUA_I_CTIN' ).Datos := IntToStr( FieldByName( 'SUA_I_CTIN' ).AsInteger );     { Control de incapacidad [Motivo] }
                    ColumnByName( 'SUA_I_FFIN' ).Datos := FechaSUA( FieldByName( 'SUA_I_FFIN' ).AsDateTime );    { Fecha Final }
               end
               else
                   Abort;   // Con una excepci�n no incluye el registro en el ASCII
          end;
     end;
end;

function TdmProcesos.CrearAsciiIncapacidades (cdsDataSet: TZetaClientDataSet;const sArchivoIncapacidades:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoIncapacidades, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  AgregaColumna( 'SUA_I_PAT', tgTexto,11 );    { RegistroPatronal }
                  AgregaColumna( 'SUA_I_SS', tgTexto,11 );     { NumeroSeguro }
                  AgregaColumna( 'SUA_I_TIPO',tgNumero,1 );    { Tipo }
                  AgregaColumna( 'SUA_I_FINI',tgTexto,8 );     { Fecha de Inicio }
                  AgregaColumna( 'SUA_I_FOLIO',tgTexto,8 );    { Folio }
                  AgregaColumna( 'SUA_I_DIAS',tgNumero,3 );    { Dias }
                  AgregaColumna( 'SUA_I_INCA',tgTexto,3 );     { % Incapacidad si no tiene 0 }
                  AgregaColumna( 'SUA_I_RAMA',tgNumero,1 );    { Rama de incapacidad }
                  AgregaColumna( 'SUA_I_RSGO',tgNumero,1 );    { Tipo de Riesgo }
                  AgregaColumna( 'SUA_I_SCLA',tgNumero,1 );    { Secuela o consecuencia }
                  AgregaColumna( 'SUA_I_CTIN',tgNumero,1 );    { Control de incapacidad [Motivo] }
                  AgregaColumna( 'SUA_I_FFIN',tgTexto,8 );     { Fecha Final }

                  AlExportarColumnas := ExportarSUAIncapacidades;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Datos de Incapacidades exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

function TdmProcesos.ExportarSUAEnd( const Parametros, Empleados, Movimientos,Infonavit,Incapacidades: OleVariant ): String;
var
   oCursor: TCursor;
   lEmpleados, lMovimientos, lBorrarEmpleados : Boolean;
   sPatron, sRegistro, sPath, sArchivoEmpleados,sArchivoDatosAfiliatorios,sArchivoInfonavit,sArchivoIncapacidades, sArchivoMovimientos : String;
   eBorrarMovimientos: eBorrarMovimSUA;
   iYear, iMes, iVersionSUA: Integer;

   //(@am): CAMBIO TEMPORAL
   procedure BorraSUA2000;
   begin
   end;

   procedure BorraSUA2006;
   begin
        {$IFDEF TRESS_DELPHIXE5_UP}
        dmBorradorSUA := TdmBorradorSUA.Create( Self );
        try
           with dmBorradorSUA do
           begin
                try
                   Empleados := lBorrarEmpleados;
                   BorrarMovimientos := eBorrarMovimientos;
                   Patron := sRegistro;
                   Path := sPath;
                   Year := iYear;
                   Mes  := iMes;
                   Borrar;
                except
                      on Error: Exception do
                      begin
                           Result := AgregaMensaje( Result, 'Error Al Borrar Archivos de SUA En' + CR_LF + sPath + CR_LF + Error.Message );
                      end;
                end;
           end;
        finally
               dmBorradorSUA.Free;
        end;
        {$ENDIF}
   end;

begin {ExportarSUAEnd}
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        InitAnimation( 'Exportando SUA' );
        with TZetaParams.Create do
        begin
             try
                VarValues := Parametros;
                sPatron := ParamByName( 'Patron' ).AsString;
                sRegistro := ParamByName( 'NumeroRegistro' ).AsString;
                sPath := Global.GetGlobalString( K_GLOBAL_DIR_DATOS_SUA );
                lEmpleados := ParamByName( 'EnviarEmpleados' ).AsBoolean;
                sArchivoEmpleados := ParamByName( 'ArchivoEmpleados' ).AsString;
                sArchivoDatosAfiliatorios :=  ParamByName('ArchivoDatosAfiliatorios').AsString;
                sArchivoInfonavit :=  ParamByName('ArchivoInfonavit').AsString;
                lBorrarEmpleados := ParamByName( 'BorrarEmpleados' ).AsBoolean;
                lMovimientos := ParamByName( 'EnviarMovimientos' ).AsBoolean;
                sArchivoIncapacidades :=  ParamByName('ArchivoIncapacidad').AsString;
                sArchivoMovimientos := ParamByName( 'ArchivoMovimientos' ).AsString;
                eBorrarMovimientos := eBorrarMovimSUA( ParamByName( 'BorrarMovimientos' ).AsInteger );
                iYear := ParamByName( 'Year' ).AsInteger;
                iMes := ParamByName( 'Mes' ).AsInteger;
                iVersionSUA := ParamByName( 'VersionSUA' ).AsInteger;
             finally
                    Free;
             end;
        end;
        Result := '';
        if lEmpleados then
        begin
             cdsDataSet.Data := Empleados;
             Result := AgregaMensaje( Result, CrearAsciiEmpleados(cdsDataSet,sArchivoEmpleados) );
             {$ifdef VER130}
             cdsDataSet.Data := Empleados;
             {$endif}
             Result := AgregaMensaje( Result, CrearAsciiDatosAfiliatorios(cdsDataSet,sArchivoDatosAfiliatorios) );
        end;
        if lMovimientos then
        begin
             with cdsDataSet do
             begin
                  Data := Movimientos;
                  if ( eBorrarMovimientos = bsNinguno ) then    // Si no se borra nada en SUA deben filtrarse las incapacidades que comenzaron el mes pasado
                     FFechaLimiteMovSUA := CodificaFecha( iYear, iMes, 1 )
                  else
                      FFechaLimiteMovSUA := NullDateTime;
             end;
             Result := CrearAsciiMovimientos (cdsDataSet,sArchivoMovimientos) ;
              cdsDataset.Data := Incapacidades;
             Result := AgregaMensaje( Result, CrearASCIIIncapacidades( cdsDataSet,sArchivoIncapacidades ) );

             cdsDataset.Data := Infonavit;
             Result := AgregaMensaje( Result, CrearAsciiInfonavit(cdsDataSet,sArchivoInfonavit) );
        end;
        if lBorrarEmpleados or ( eBorrarMovimientos <> bsNinguno ) then
        begin
             case iVersionSUA of
                  K_VERSION_SUA_2000 : BorraSUA2000;
                  K_VERSION_SUA_2006 : BorraSUA2006;
             else
                 Result := AgregaMensaje( Result, 'No se tiene implementado mecanismo de borrado para esta versi�n de SUA' );
             end;
        end;
     finally
            EndAnimation;
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

function TdmProcesos.CalcularPrimaRiesgo(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSCalculoPrima, Parametros );
end;

function TdmProcesos.CalcularPrimaRiesgoEnd(const Parametros, Datos: OleVariant): String;
var
   oCursor: TCursor;
   sArchivo: String;
begin
     Result := '';
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TZetaParams.Create do
        begin
             try
                VarValues := Parametros;
                sArchivo := ParamByName( 'Archivo' ).AsString;
             finally
                    Free;
             end;
        end;
        with TStringList.Create do
        begin
             try
                Text := Datos;
                SaveToFile( sArchivo );
             finally
                    Free;
             end;
        end;
        ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
     finally
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmProcesos.AjusteRetInfonavitGetLista(Parametros: TZetaParams);
begin
     cdsDataSet.Data:= ServerImss.AjusteRetInfonavitGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AjusteRetInfonavit(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadIMSS( prIMSSAjusteRetInfonavit, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadIMSS( prIMSSAjusteRetInfonavit, NULL, Parametros );
end;

{ ********** Fin de Procesos IMSS ************* }

{ ************ Procesos Anuales de N�mina *************** }
function TdmProcesos.CorreThreadAnual(const eProceso: Procesos; const Lista: OleVariant; const lCargarPeriodo: Boolean; Parametros: TZetaParams): Boolean;
begin
     if lCargarPeriodo then
     begin
          with dmCliente do
          begin
               CargaActivosPeriodo( Parametros );
          end;
     end;
     with TAnualThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CerrarAhorros(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prSISTCierreAhorros, NULL, False, Parametros );
end;

function TdmProcesos.CerrarPrestamos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prSISTCierrePrestamos, NULL, False, Parametros );
end;

function TdmProcesos.CalcularAguinaldo(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOCalculoAguinaldo, NULL, False, Parametros );
end;

function TdmProcesos.PagarAguinaldo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOPagoAguinaldo, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOPagoAguinaldo, NULL, True, Parametros );
end;

procedure TdmProcesos.PagarAguinaldoGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     with cdsDataset do
     begin
          Data := ServerAnualNomina.PagarAguinaldoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.PTUCalcular(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     Result := CorreThreadAnual( prNOCalculoPTU, NULL, False, Parametros );
end;

function TdmProcesos.PTUPago(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOPagoPTU, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOPagoPTU, NULL, True, Parametros );
end;

procedure TdmProcesos.PTUPagoGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.PTUPagoGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

function TdmProcesos.CalcularRepartoAhorro(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNORepartoAhorro, NULL, False, Parametros );
end;

function TdmProcesos.PagarRepartoAhorro(Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOPagoRepartoAhorro, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOPagoRepartoAhorro, NULL, True, Parametros );
end;

procedure TdmProcesos.PagarRepartoAhorroGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.PagarAhorroGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

function TdmProcesos.ISPTAnual(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOISPTAnual, NULL, False, Parametros );
end;

function TdmProcesos.ISPTAnualPago(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOISPTAnualPago, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOISPTAnualPago, NULL, True, Parametros );
end;

procedure TdmProcesos.ISPTAnualPagoGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.ISPTPagoGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

function TdmProcesos.DeclaraCredito(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOCreditoSalario, NULL, False, Parametros );
end;

function TdmProcesos.CalculaRetroactivos(Parametros: TZetaParams; const lVerificacion: Boolean):Boolean;
begin
     if lVerificacion then
        Result := CorreThreadCalcNominaBDE( prNOCalculaRetroactivos, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadCalcNominaBDE( prNOCalculaRetroactivos, NULL, Parametros );
end;

procedure TdmProcesos.CalculaRetroactivoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerCalcNomina.CalculaRetroactivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{ ******** Fin de Procesos Anuales de N�mina ************ }

{ ********** Procesos de Recursos Humanos ********* }

function TdmProcesos.CorreThreadRecursos( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TRecursosThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.BorrarBajas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prSISTBorrarBajas, NULL, Parametros );
end;

function TdmProcesos.BorrarHerramientas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prSISTBorrarHerramienta, NULL, Parametros );
end;

function TdmProcesos.SalNetoBruto(Parametros: TZetaParams): Boolean;
var
    NetoBruto: OleVariant;
begin
     NetoBruto := Parametros.VarValues;
     try
        ServerCalcNomina.CalculaNetoBruto( dmCliente.Empresa, NetoBruto );
        Parametros.VarValues := NetoBruto;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmProcesos.AplicarTabuladorGetList( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.AplicarTabuladorGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.PromediarVariablesGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.PromediarVariablesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.SalarioIntegradoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.SalarioIntegradoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.CambioSalarioGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CambioSalarioGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.EventosGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          IndexFieldNames := 'CB_CODIGO';
          Data := ServerRecursos.EventosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.ImportarKardexGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.ImportarKardexGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

procedure TdmProcesos.ImportarKardexGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarKardexGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames := 'CB_CODIGO';
     end;
end;

function TdmProcesos.CambioMasivoTurnos(Parametros: TZetaParams; const DataSet: TZetaClientDataSet; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prEmpCambioTurnoMasivo, GetLista( DataSet ), Parametros )
     else
        Result := CorreThreadRecursos( prEmpCambioTurnoMasivo, NULL, Parametros );
end;

procedure TdmProcesos.CancelarKardexGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarKardexGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CancelarVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancVacGlobales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancVacGlobales, NULL, Parametros );
end;

procedure TdmProcesos.CancelarVacacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarVacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CierreGlobalVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCierreVacacionGlobal, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCierreVacacionGlobal, NULL, Parametros );
end;

procedure TdmProcesos.CierreGlobalVacacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CierreGlobalVacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.EntregarHerramienta(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHEntregarHerramienta, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHEntregarHerramienta, NULL, Parametros );
end;

procedure TdmProcesos.EntregarHerramientaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.EntregarHerramientaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.RegresarHerramienta(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHRegresarHerramienta, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHRegresarHerramienta, NULL, Parametros );
end;

procedure TdmProcesos.RegresarHerramientaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.RegresarHerramientaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.CursoTomadoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CursoTomadoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.RenumeraEmpleadosGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.RenumeraGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.RenumeraVerificaListaASCII( Parametros: TZetaParams );
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.RenumeraValidaASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.PermisoGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHPermisoGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHPermisoGlobal, NULL, Parametros );
end;

procedure TdmProcesos.PermisoGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.PermisoGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;


procedure TdmProcesos.BorrarCursoTomadoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.BorrarCursoTomadoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{ACL. 24.Feb.09. Cursos Programados Global}
function TdmProcesos.CursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCursoProgGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHCursoProgGlobal, NULL, Parametros );
end;

procedure TdmProcesos.CursoProgGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CursoProgGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{ACL. 27.Feb.09. Cancelar Cursos Programados Global}
function TdmProcesos.CancelarCursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarCursoProgGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHCancelarCursoProgGlobal, NULL, Parametros );
end;

procedure TdmProcesos.CancelarCursoProgGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarCursoProgGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.ImportarAsistenciaSesionesGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarAsistenciaSesionesGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames := 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarAsistenciaSesionesGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.ImportarAsistenciaSesionesGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.CancelarCierreVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarCierreVacaciones, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancelarCierreVacaciones, NULL, Parametros );
end;

procedure TdmProcesos.CancelarCierreVacacionesGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarCierreVacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.CancelarPermisosGlobalesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          try
             AfterOpen := PermisosAfterOpen;
             Data := ServerRecursos.CancelarPermisosGlobalesGetLista( dmCliente.Empresa, Parametros.VarValues );
          finally
                 AfterOpen := Nil;
          end;
     end;
end;

function TdmProcesos.CancelarPermisosGlobales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarPermisosGlobales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancelarPermisosGlobales, NULL, Parametros );

end;

procedure TdmProcesos.PermisosAfterOpen(DataSet: TDataSet);
begin
     with cdsDataSet do
     begin
          with FieldByName('PM_CLASIFI') do
          begin
               Alignment := taLeftJustify;
               OnGetText := PM_CLASIFIOnGetText;
          end;
     end;
end;

procedure TdmProcesos.PM_CLASIFIOnGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
     Text := ObtieneElemento( lfTipoPermiso,cdsDataSet.FieldByName('PM_CLASIFI').AsInteger );
end;

procedure TdmProcesos.ImportarAhorrosPrestamosGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          with dmCliente do
          begin
               cdsDataset.Data := ServerRecursos.ImportarAhorrosPrestamosGetASCII( Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          end;
          Active := False;
     end;
end;

function TdmProcesos.RecalculaSaldosVaca(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHRecalculaSaldosVacaciones, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHRecalculaSaldosVacaciones, NULL, Parametros );

end;

procedure TdmProcesos.RecalculaSaldosVacaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.RecalculaSaldosVacaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarSGM(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadRecursos( prRHImportarSGM, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarSGMGetLista(Parametros: TZetaParams);
begin
      if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
          FErrorCount := 0;
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarSGMGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames := 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarSGMGetListaASCII(Parametros: TZetaParams;
  const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.ImportarSGMGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.RenovacionSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if not lVerificacion then
          cdsDataset.Data := ServerRecursos.RenovarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
     Result := CorreThreadRecursos( prRHRenovacionSGM, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.RenovacionSGMGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
          Data := ServerRecursos.RenovarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.BorrarSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if not lVerificacion then
          cdsDataset.Data := ServerRecursos.BorrarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
     Result := CorreThreadRecursos( prRHBorrarSGM, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.BorrarSGMGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
          Data := ServerRecursos.BorrarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.ImportarOrganigrama(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadRecursos( prRHImportarOrganigrama, GetLista( cdsDataset ), Parametros );

end;

procedure TdmProcesos.ImportarOrganigramaGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarOrganigramaGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames:= 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarOrganigramaGetListaASCII(oParametros: TZetaParams; const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, oParametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          with dmCliente do
          begin
               cdsDataset.Data := ServerRecursos.ImportarOrganigramaGetASCII(Empresa, oParametros.VarValues, GetLista( cdsASCII ), FErrorCount);
          end;
          Active := False;
     end;
end;

{ ********** Fin de Procesos de Recursos Humanos ********* }

{ *********** Procesos de Sistemas ********* }

function TdmProcesos.Transferencia(Parametros: TZetaParams; const EmpresaDestino: Variant): Boolean;
begin
     Result := CorreThreadCalcNomina( prRHTranferencia, EmpresaDestino, Parametros );
     if Result then
        dmCliente.PosicionaSiguienteEmpleado;
end;

function TdmProcesos.CorreThreadSistema(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadSistemaSilent(const eProceso: Procesos;const Lista: OleVariant; Parametros: TZetaParams ; Ventana : HWND;SetResultado:TSetResultadoEspecial): Boolean;
begin
      with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          ProcessInfo.FeedBack := False;
          ProcessInfo.SetResultadoEsp := SetResultado;
          CargaParametros( Parametros );
          SilentExecute := True;
          Resume;
     end;
     Result := True;
end;


function TdmProcesos.BorrarBitacora(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadSistema( prSISTDepurar, NULL, Parametros );
end;

function TdmProcesos.NumeroTarjeta: Boolean;
begin
    Result := CorreThreadSistema( prSISTNumeroTarjeta, NULL, NIL );
end;

function TdmProcesos.NumeroTarjetaEnd( const Bitacora: OleVariant ): String;
var
   sArchivo: String;
begin
     sArchivo := Format( '%sActualizarTarjeta.log', [ ExtractFilePath( Application.ExeName ) ] );
     with TStringList.Create do
     begin
          try
             Text := Bitacora;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;


function TdmProcesos.EnrolarUsuarios(Parametros: TZetaParams): Boolean;
begin
      Result := CorreThreadSistema( prSistEnrolarUsuario, NULL, parametros );
end;

procedure TdmProcesos.EnrolarUsuariosGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerSistema.EnrolamientoMasivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarEnrolamientoMasivo(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     Result := CorreThreadSistema( prSistImportarEnrolamientoMasivo, GetLista( cdsDataset ), Parametros );
end;


procedure TdmProcesos.ImportarEnrolamientoMasivoGetListaASCII(Parametros: TZetaParams;const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          with dmCliente do
          begin
               cdsDataset.Data := ServerSistema.ImportarEnrolamientoMasivoGetASCII(Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount,GetDatosUsuarioActivo.LoginAD );
          end;
          Active := False;
     end;
end;

{$ifndef DOS_CAPAS}
// SYNERGY
function TdmProcesos.DepuraBitacoraBio(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTDepuraBitBiometrico, NULL, Parametros );
end;

//SYNERGY
function TdmProcesos.AsignaNumBiometricos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadSistema( prSISTAsignaNumBiometricos, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadSistema( prSISTAsignaNumBiometricos, NULL, Parametros );
end;

// SYNERGY
function TdmProcesos.AsignaGrupoTerminales( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadSistema( prSISTAsignaGrupoTerminales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadSistema( prSISTAsignaGrupoTerminales, NULL, Parametros );
end;

// SYNERGY
procedure TdmProcesos.EmpleadosConBiometricosGetLista(Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerSistema.EmpleadosBiometricosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

// SYNERGY
procedure TdmProcesos.EmpleadosSinBiometricosGetLista(Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerSistema.EmpleadosSinBiometricosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

// SYNERGY
function TdmProcesos.ImportaTerminales( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadSistema( prSISTImportaTerminales, NULL, Parametros );
end;

function TdmProcesos.ImportaTerminalesEnd( const Bitacora: OleVariant ): String;
var
   sArchivo: String;
begin
     sArchivo := Format( '%sImportaTerminales.log', [ ExtractFilePath( Application.ExeName ) ] );
     with TStringList.Create do
     begin
          try
             Text := Bitacora;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;
{$endif}
function TdmProcesos.ImportarTablas( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadSistema( prSISTImportarTablas, NULL, Parametros );
end;

procedure TdmProcesos.ImportarTablaCSVGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;         
          cdsDataset.Data := ServerSistema.ImportarTablaCSVGetASCII( dmConsultas.BaseDatosImportacionXMLCSV, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.ExtraerChecadasBitTerminalesEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   sFileName: String;
begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sFileName := ParamByName( 'Archivo' ).AsString;
          finally
                 Free;
          end;
     end;         

     Archivo := TAsciiExport.Create;
     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin
                  AgregaColumna( 'WS_MIENT', tgTexto, 31 );
                  AlExportarColumnas := ExtraerChecadasBitTermEscribeASCII;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Checadas exportadas' );

                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;

// ----- -----

// @DC
function TdmProcesos.RecuperaChecadasBitTerminalesEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   // eFormato: eFormatoASCII;
   sFileName: String;
begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sFileName := ParamByName( 'RutaSalidaArchivo' ).AsString;
          finally
                 Free;
          end;
     end;
     Archivo := TAsciiExport.Create;
     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin
                  AgregaColumna( 'CHECADA_RELOJ', tgTexto, 31 );
                  AlExportarColumnas := RecuperaChecadasBitTermEscribeASCII;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Checadas exportadas' );

                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;



{ ********** Fin de Procesos de Sistemas ********* }

{ ********** Proceso de Importaci�n de Altas ****** }
function TdmProcesos.ImportarAltas(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     Result := CorreThreadRecursos( prRHImportarAltas, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarAltasGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             cdsDataSet.AfterOpen := ValidacionesAfterOpen;
             cdsDataset.Data := ServerRecursos.ImportarAltasGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          finally
                 cdsDataSet.AfterOpen := Nil;
          end;
          Active := False;
     end;
end;

procedure TdmProcesos.ValidacionesAfterOpen(DataSet: TDataSet);
begin
      with cdsDataSet do
      begin
           with FieldByName('CB_CODIGO') do
           begin
                Alignment := taLeftJustify;
                OnGetText := CB_CODIGOOnGetText;
           end;
      end;
end;

procedure TdmProcesos.CB_CODIGOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if (Sender.AsInteger = 0) then
                  Text := 'Por Asignar'
               else
                    Text := Sender.AsString;
          end;
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmProcesos.ImportarAltasGetASCIICatalogos(Parametros: TZetaParams);
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             //cdsDataSet.AfterOpen := ImportarAltasAfterOpen;
             cdsDataset.Data := ServerRecursos.ImportarAltasValidaCatalogos( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          finally
                 cdsDataSet.AfterOpen := Nil;
          end;
          Active := False;
     end;
end;

{ ********** Fin del Proceso de Importar Altas *********** }
{ *********** Procesos de N�minas *********** }

function TdmProcesos.CorreThreadNomina(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadNominaSilent(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          SilentExecute := TRUE;
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadNominaSilentConciliacion(const eProceso: Procesos;const Lista: OleVariant; Parametros: TZetaParams ; Ventana : HWND;SetResultado:TSetResultadoEspecial): Boolean;
begin
      with TNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          ProcessInfo.FeedBack := False;
          ProcessInfo.SetResultadoEsp := SetResultado;
          CargaParametros( Parametros );
          SilentExecute := True;
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.BorrarNominas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prSISTBorrarNominas, NULL, Parametros );
end;

function TdmProcesos.BorrarTimbradoNominas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prSISTBorrarTimbradoNominas, NULL, Parametros );
end;


procedure TdmProcesos.ImportarMovGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          with dmCliente do
          begin
               // (JB) Fix de Bug 631-2702
               CargaActivosPeriodo( Parametros );
               CargaActivosIMSS( Parametros );
               CargaActivosSistema( Parametros );
               cdsDataset.Data := ServerNomina.ImportarMovimientosGetASCII( Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          end;
          Active := False;
     end;
end;

function TdmProcesos.ImportarMov(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          // (JB) Fix de Bug 631-2702
          CargaActivosPeriodo( Parametros );
          CargaActivosIMSS( Parametros );
          CargaActivosSistema( Parametros );
     end;
     ClearErroresASCII( cdsDataset );
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadNomina( prNOImportarMov, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ExportarMovEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'CB_CODIGO' ).Datos := ZetaCommonTools.FormateaNumero( FieldByName( 'CB_CODIGO' ).AsInteger, 9, ' ' );
               ColumnByName( 'CO_NUMERO' ).Datos := ZetaCommonTools.FormateaNumero( FieldByName( 'CO_NUMERO' ).AsInteger, 5, ' ' );
               ColumnByName( 'MO_MONTO' ).Datos := FormatFloat( '000000000000000.00', FieldByName( 'MO_MONTO' ).AsFloat );
               ColumnByName( 'MO_REFEREN' ).Datos := FieldByName( 'MO_REFEREN' ).AsString;
          end;
     end;
end;

function TdmProcesos.ExportarMovEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   eFormato: eFormatoASCII;
   sFileName: String;
begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sFileName := ParamByName( 'Archivo' ).AsString;
             eFormato := eFormatoASCII( ParamByName( 'Formato' ).AsInteger );
          finally
                 Free;
          end;
     end;
     Archivo := TAsciiExport.Create;
     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin
                  AgregaColumna( 'CB_CODIGO', tgNumero, 9 );
                  AgregaColumna( 'CO_NUMERO', tgNumero, 5 );
                  AgregaColumna( 'MO_MONTO', tgFloat, 18 ); {30-Abr-2003: Se cambio de 17 a 18, para que coincida con la Importacion}
                  AgregaColumna( 'MO_REFEREN', tgTexto, 10 );
                  AlExportarColumnas := ExportarMovEscribeASCII;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( eFormato, cdsDataset ) ) + ' Movimientos Exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;

function TdmProcesos.ExportarMov(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOExportarMov, NULL, Parametros );
end;

function TdmProcesos.AfectarNomina(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOAfectar, NULL, Parametros );
end;

function TdmProcesos.DesafectarNomina(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNODesafectar, NULL, Parametros );
end;

function TdmProcesos.DefinirPeriodos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNODefinirPeriodos, NULL, Parametros );
end;

function TdmProcesos.LimpiarAcum(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOLimpiarAcum, NULL, Parametros );
end;

function TdmProcesos.RecalculoAcumulado(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNORecalcAcum, NULL, Parametros );
end;

procedure TdmProcesos.RecalculoDias( const iTipo: Integer );
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
             { El Tipo proviene del Combo Box y no necesariamente
             concide con el Valor Activo }
             Parametros.ParamByName( 'Tipo' ).AsInteger := iTipo;
        end;
        ServerNomina.RecalculoDias( dmCliente.Empresa, Parametros.VarValues );
     finally
            Parametros.Free;
     end;
end;

function TdmProcesos.FoliarRecibos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOFoliarRecibos, NULL, Parametros );
end;

function TdmProcesos.CreditoAplicado( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOCreditoAplicado, GetLista( cdsDataset ), FALSE, Parametros )
     else
         Result := CorreThreadAnual( prNOCreditoAplicado, NULL, FALSE, Parametros );
end;

procedure TdmProcesos.CreditoAplicadoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAnualNomina.CreditoAplicadoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ReFoliarRecibos(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOReFoliarRecibos, NULL, Parametros );
end;

function TdmProcesos.PagarPorFuera(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     if lVerificacion then
        Result := CorreThreadNomina( prNOPagosPorFuera, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadNomina( prNOPagosPorFuera, NULL, Parametros );
end;

procedure TdmProcesos.PagarPorFueraGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     with cdsDataset do
     begin
          Data := ServerNomina.PagarPorFueraGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.Poliza(Parametros: TZetaParams; aFiltros: OleVariant ): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOPolizaContable, aFiltros, Parametros );
end;

function TdmProcesos.LiquidacionGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     if lVerificacion then
        Result := CorreThreadNomina( prNOLiquidacion, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadNomina( prNOLiquidacion, NULL, Parametros );
end;

procedure TdmProcesos.LiquidacionGlobalLista( Parametros: TZetaParams );
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     with cdsDataset do
     begin
          Data := ServerNomina.LiquidacionGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarPagoRecibos(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOPagoRecibos, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarPagoRecibosGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerNomina.ImportarPagoRecibosGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames:= 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarPagoRecibosGetListaASCII(Parametros: TZetaParams);
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Open;
          cdsDataset.Data := ServerNomina.ImportarPagoRecibosGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Close;
     end;
end;

procedure TdmProcesos.LlenaParametrosCalculo( oParametros: TZetaParams; const lNomina: Boolean );
begin
     with dmCliente do
     begin
          CargaActivosIMSS( oParametros );
          CargaActivosPeriodo( oParametros );
          CargaActivosSistema( oParametros );
          with oParametros do
          begin
               AddString( 'RangoLista', '' );
               AddString( 'Condicion', '' );
               { El UNICO empleado que se calcula es el Empleado ACTIVO }
               AddString( 'Filtro', Format( 'CB_CODIGO = %d', [ Empleado ] ) );
               AddInteger( 'TipoCalculo', Ord( tcNueva ) );
               if lNomina then
               begin
                    AddBoolean( 'MuestraTiempos', False );
                    AddBoolean( 'MuestraScript', False );
               end
               else
               begin
                    AddBoolean( 'PuedeCambiarTarjeta', PuedeModificarTarjetasAnteriores );
               end;
          end;
     end;
end;

procedure TdmProcesos.ExtraerChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'WS_MIENT' ).Datos := FieldByName( 'WS_MIENT' ).AsString;
          end;
     end;
end;

procedure TdmProcesos.RecuperaChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );//@DC
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'CHECADA_RELOJ' ).Datos := FieldByName( 'CHECADA_RELOJ' ).AsString;
          end;
     end;
end;

procedure TdmProcesos.CalcularNominaEmpleado( const lNomina : Boolean );
var
   Parametros: TZetaParams;
   Resultado : OleVariant;
begin
     Parametros := TZetaParams.Create;
     try
        LlenaParametrosCalculo( Parametros, lNomina );
        if lNomina then
            Resultado := ServerCalcNomina.CalculaNomina( dmCliente.Empresa, Parametros.VarValues )
        else
            Resultado := ServerCalcNomina.CalculaPreNomina( dmCliente.Empresa, Parametros.VarValues );
        CheckSilencioso( Resultado, lNomina );
     finally
            Parametros.Free;
     end;
end;

procedure TdmProcesos.CalcularSimulacionEmpleado( const TipoPeriodo: eTipoPeriodo; const iPeriodo: Integer );
var
   Parametros: TZetaParams;
   Resultado : OleVariant;
begin
     Parametros := TZetaParams.Create;
     try
        LlenaParametrosCalculo( Parametros, TRUE );
        with Parametros do
        begin
             AddInteger( 'Tipo', Ord( TipoPeriodo ) );
             AddInteger( 'Numero', iPeriodo );
        end;
        Resultado := ServerCalcNomina.CalculaNomina( dmCliente.Empresa, Parametros.VarValues );
        CheckSilencioso( Resultado, TRUE );
     finally
            Parametros.Free;
     end;
end;

function TdmProcesos.CalcularSimulacionGlobal( const iPeriodo: Integer ): Boolean;
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        LlenaParametrosCalculo( Parametros, TRUE );
        with Parametros do
        begin
             AddInteger( 'Numero', iPeriodo );
             AddString( 'SimulacionCiclo', K_GLOBAL_SI)
        end;
        Result := CorreThreadCalcNominaSilent( prNOCalcular, NULL, Parametros );
     finally
            Parametros.Free;
     end;
end;

function TdmProcesos.LiquidacionGlobalSimulacion( Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNominaSilent( prNOLiquidacion, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadNominaSilent( prNOLiquidacion, NULL, Parametros );
end;

//WizNomDeclaracionAnual
function TdmProcesos.DeclaracionAnual(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNODeclaracionAnual, GetLista( cdsDataset ), FALSE, Parametros )
     else
         Result := CorreThreadAnual( prNODeclaracionAnual, NULL, FALSE, Parametros );
end;

procedure TdmProcesos.DeclaracionAnualGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.DeclaracionAnualGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

procedure TdmProcesos.GetDeclaracionAnualGlobales;
begin
     cdsDeclaraGlobales.Data := ServerAnualNomina.GetDeclaracionGlobales( dmCliente.Empresa );
end;

procedure TdmProcesos.GrabaDeclaracionAnualGlobales;
 var
    ErrorCount: integer;
begin
     ServerAnualNomina.GrabaDeclaracionGlobales( dmCliente.Empresa,
                                                 cdsDeclaraGlobales.Delta,
                                                 ErrorCount );
end;


//WizNomDeclaracionAnualCierre
function TdmProcesos.DeclaracionAnualCierre(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNODeclaracionAnualCierre, NULL, FALSE, Parametros );
end;

//WizNomAjusteRetFonacot

procedure TdmProcesos.AjusteRetFonacotGetLista(Parametros: TZetaParams);
begin
     cdsDataSet.Data:= ServerNomina.AjusteRetFonacotGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AjusteRetFonacot(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNomina( prNOAjusteRetFonacot, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadNomina( prNOAjusteRetFonacot, NULL, Parametros );
end;

//WizNomCalcularPagoFonacot


function TdmProcesos.CalcularPagoFonacot(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOCalcularPagoFonacot, NULL, Parametros );
end;

function TdmProcesos.AplicarFiniquitosGlobales(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNomina( prEmpAplicacionFiniGlobal , GetLista( cdsDataset ), Parametros )
     else
          Result := CorreThreadNomina( prEmpAplicacionFiniGlobal, NULL, Parametros );

end;

procedure TdmProcesos.AplicarFiniquitosGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerNomina.AplicacionGlobalFiniquitosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.TransferenciasCosteo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prNoTransferenciasCosteo, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prNoTransferenciasCosteo, NULL, Parametros );
end;

procedure TdmProcesos.TransferenciasCosteoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.TransferenciasCosteoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CalculaCosteo(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prNoCalculaCosteo, NULL, Parametros );
end;

function TdmProcesos.CancelaTransferenciaCosteo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prNoCancelaTransferencias, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prNoCancelaTransferencias, NULL, Parametros );
end;

procedure TdmProcesos.CancelaTransferenciasCosteoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.CancelaTransferenciasCosteoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.PrevioISR(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOPrevioISR, NULL, TRUE, Parametros );
end;

{ ******** Fin de Procesos de N�minas ******* }


{ *************** Procesos de Cafeteria ************* }
function TdmProcesos.CorreThreadCafeteria( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TCafeteriaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.ComidasGrupales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadCafeteria( prCAFEComidasGrupales, GetLista( cdsDataset ), Parametros )
     else
          Result := CorreThreadCafeteria( prCAFEComidasGrupales, NULL, Parametros );
end;

procedure TdmProcesos.ComidasGrupalesGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerCafeteria.ComidasGrupalesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ComidasGrupalesListaASCII( Parametros: TZetaParams ): Boolean;
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          Result := CorreThreadCafeteria( prCAFEComidasGrupales, GetLista( cdsASCII ), Parametros );
          Active := False;
     end;
end;

function TdmProcesos.CorregirFechasGlobalesCafe(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadCafeteria( prCAFECorregirFechas, NULL, Parametros );
end;

function TdmProcesos.FoliarCapacitaciones( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadRecursos( prRHFoliarCapacitaciones, NULL, Parametros );
end;

procedure TdmProcesos.FoliarCapacitacionesSTPSGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.FoliarCapacitacionesSTPSGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.FoliarCapacitacionesSTPS(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
var
   proceso : ZetaCommonLists.Procesos;
begin
   if  Parametros.ParamByName ('ReinicioFolio').AsBoolean then
       proceso := prRHReiniciarCapacitacionesSTPS
   else
       proceso := prRHFoliarCapacitaciones;
   if lVerificacion then
      Result := CorreThreadRecursos( proceso, GetLista( cdsDataset ), Parametros )
   else
       Result := CorreThreadRecursos( proceso, NULL, Parametros );
end;

// WizEmpValidarDC4
function TdmProcesos.RevisarDatosSTPS(Parametros: TZetaParams): Boolean;
begin
   Result := CorreThreadRecursos( prRHRevisarDatosSTPS, NULL, Parametros );
end;

function TdmProcesos.RevisarDatosSTPSEnd (const Parametros, Datos: OleVariant): String;
var
   oCursor: TCursor;
   sArchivo: String;
begin
     Result := '';
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TZetaParams.Create do
        begin
             try
                VarValues := Parametros;
                sArchivo := ParamByName( 'Archivo' ).AsString;
             finally
                    Free;
             end;
        end;
        with TStringList.Create do
        begin
             try
                Text := Datos;
                SaveToFile( sArchivo );
             finally
                    Free;
             end;
        end;
        if ZConfirm( 'Revisi�n de Datos para STPS', Format ('El archivo ''%s''', [ExtractFileName (sArchivo)]) + ' fu� Creado con �xito.' +
           CR_LF + '�Desea verlo?', 0, mbYes ) then
           ZetaClientTools.ExecuteFile( sArchivo, '', '', SW_SHOWDEFAULT );
     finally
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

{ ******** Fin de Procesos de Cafeteria ******* }

function TdmProcesos.EjecutaMotorPatch(Parametros: TZetaParams; Ventana: HWND; SetResultado: TSetResultadoEspecial): Boolean;
begin
  Result := CorreThreadSistemaSilent(prPatchCambioVersion, null, Parametros, Ventana, SetResultado);
end;

{$IFDEF TIMBRADO}

function TdmProcesos.TimbrarNomina(Parametros: TZetaParams): Boolean;
begin
 Result := CorreThreadNomina( prNoTimbrarNomina , GetLista( cdsDataset ), Parametros )
end;
procedure TdmProcesos.TimbrarNominaGetLista(Parametros: TZetaParams);
begin
      with cdsDataSetTimbrados do
     begin
          Data := ServerNomina.TimbrarNominasGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

//Conciliacion Timbrado de Nomina
function TdmProcesos.IniciarConciliacionTimbradoPeriodos(Parametros: TZetaParams; Ventana: HWND; SetResultado: TSetResultadoEspecial): Boolean;
begin
     Parametros.AddVariant('Empresa', dmCliente.Empresa);
     Result := CorreThreadNominaSilentConciliacion(prIniciarConciliacionTimbrado, null, Parametros, Ventana, SetResultado);
end;

function TdmProcesos.AplicarConciliacionTimbradoPeriodos(Parametros: TZetaParams; Ventana: HWND; SetResultado: TSetResultadoEspecial): Boolean;
begin
     Parametros.AddVariant('Empresa', dmCliente.Empresa);
     Result := CorreThreadNominaSilentConciliacion(prAplicarConciliacionTimbrado, null, Parametros, Ventana, SetResultado);
end;


{$ENDIF}
function TdmProcesos.GetStatusTimbrado(Parametros: TZetaParams ): Olevariant;
begin
     Result := ServerLogin.GetStatusTimbrado( dmCliente.Empresa, Parametros.VarValues );
end;

end.
