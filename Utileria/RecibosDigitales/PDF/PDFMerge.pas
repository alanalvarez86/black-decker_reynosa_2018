unit PDFMerge;

interface

uses
    Windows,
    Dialogs,
    Messages,
    Classes,
    SysUtils,
    DebenuPDFLibrary1115,
    PDFMergeThread;
type
   TPDFMerge = Class
     private
       sPathOrigen  : String;
       sPathDestino : String;
     published
       function GeneraPDFMerge(sBaseName: String) : Boolean;
       function ListarPDFs(sPathOrigen : String) : TStringList;
       function BorrarPDFTemporal(fichero: String): Boolean;
       function GeneraPDFAux(sArchivo, sArchivo2,sBaseName: String) : String;
       function GeneraPDFMergeThread(sBaseName: String) : Boolean;

       //Nuevas Funciones
       function MergeTotal( listPDF : TStringList; sBaseName : String) : Boolean;
       function Merge( listPDF : TStringList; iPaquetesControl : Integer) :  TStringList;
       function TestMerge( listPDF : TStringList; sBaseName : String) : Boolean;
       //

       procedure Debug(sMensaje : String; dt : TDateTime);
       procedure DebugText(sMensaje : String);
       procedure SetPathOrigen(Value: string);
       function  GetPathOrigen: string;
       procedure SetPathDestino(Value: string);
       function  GetPathDestino: string;
       function  PDFCalidad(sArchivo,sBaseName: String) : Boolean;
end;

type
    aLisTemp = record
      aList :  array of TStringList;
  end;

implementation

procedure TPDFMerge.SetPathOrigen(Value: string);
begin
  sPathOrigen := Value;
end;

function TPDFMerge.GetPathOrigen: string;
begin
  Result := sPathOrigen;
end;


procedure TPDFMerge.SetPathDestino(Value: string);
begin
  sPathDestino := Value;
end;

function TPDFMerge.GetPathDestino: string;
begin
  Result := sPathDestino;
end;

{
    PDF Optimiza
}

function  TPDFMerge.PDFCalidad(sArchivo,sBaseName: String) : Boolean;
var
 DPL : TDebenuPDFLibrary1115;
begin
     DPL  := TDebenuPDFLibrary1115.Create;
     DPL.UnlockKey('jy9p34jr83a5sr8517jj95b6y');
     DPL.Free;
     Result := True;
end;

{
    Genera PDF MERGE
}

function TPDFMerge.GeneraPDFMerge(sBaseName: String) : Boolean;
var
   lExito   : Boolean;
   aListPDF : TStringList;
   dt       : TDateTime;
   iPos     : Integer;
   sPFDAux   : String;
begin
     lExito := True;
     aListPDF := ListarPDFs(sPathOrigen);
     dt := now - dt;
     Debug('Inicio del Proceso',dt);
     sPFDAux := GeneraPDFAux(aListPDF[0], aListPDF[1], sBaseName);
     dt := now - dt;
     Debug(Format('PDF Merge %s <=> %s',[aListPDF[0],aListPDF[1]]), dt);
     for iPos := 2 to aListPDF.Count - 1 do
     begin
        sPFDAux := GeneraPDFAux(sPFDAux, aListPDF[iPos], sBaseName);
        dt := now - dt;
        Debug(Format('PDF Merge %s <=> %s',[sPFDAux,aListPDF[iPos]]), dt);
     end;
     FreeAndNil(aListPDF);
     dt := now - dt;
     Debug('Proceso Final en ', dt);
end;


{
    Nuevas Funciones
}

function TPDFMerge.TestMerge( listPDF : TStringList; sBaseName : String): Boolean;
var
   lExito   : Boolean;
   aListPDF : TStringList;
   dt       : TDateTime;
   iPos     : Integer;
   sPFDAux   : String;
begin
     lExito := True;
     lExito := MergeTotal(listPDF,sBaseName);
     Result := lExito;
end;




function TPDFMerge.MergeTotal( listPDF : TStringList; sBaseName : String) : Boolean;
var
   arregloTemp  : TStringList;
   lOk          : Boolean;
   lExito          : Boolean;
   iPos         : Integer;
   dt       : TDateTime;
   FPDFMergeThread: TPDFMergeThread;
   iPaquetesControl : Integer;
begin
     dt := now - dt;
     arregloTemp := listPDF;
     lOk := True;
     lExito := False;
     while ( lOk ) do
     begin
          if (arregloTemp.Count = 0 ) then
          begin
               lExito := False;
               lOk := False;
          end
          else
          if (arregloTemp.Count = 1 ) then
          begin
               lExito := TRUE;
               lOk := False;
               RenameFile(  listPDF[0], sPathDestino + sBaseName );
          end
          else
          if (arregloTemp.Count = 2 ) then
          begin
               FPDFMergeThread := TPDFMergeThread.Create(True,arregloTemp[0], arregloTemp[1],sBaseName,sPathDestino);
               FPDFMergeThread.Resume;
                  if not FPDFMergeThread.TerminateThread then
                  begin
                       FPDFMergeThread.WaitFor;
                       //FPDFMergeThread.Free;
                       lOk := False;
                       lExito := True;
                  end;
          end
          else
          begin
               arregloTemp := Merge( arregloTemp ,iPaquetesControl );
               //DebugText('===== ' + IntToStr(arregloTemp.Count));
               for iPos:= 0  to arregloTemp.Count - 1 do
               begin
                    //DebugText(arregloTemp[iPos]);
               end;
                //DebugText('=======');
          end;

          Inc(iPaquetesControl);
     end;

     Result := lExito;

end;


function TPDFMerge.Merge( listPDF: TStringList; iPaquetesControl : Integer ) :  TStringList;
var
   orden : Integer;
   tamanoNuevo : Integer;
   nuevoArreglo : TStringList;
   iPos         : Integer;
   primero      : Integer;
   segundo      : Integer;
   iThread      : Integer;
   nthreadsLanzados : Integer;
   iNumPDF, iThreadCheck       :  Integer;
   FPDFMergeThread: TPDFMergeThread;
   threads   : array[0..3] of TPDFMergeThread;
begin

     for iThreadCheck := 0  to 3 do
         threads[iThreadCheck] := nil;


     tamanoNuevo := (listPDF.Count div 2 );
     if ( listPDF.Count mod 2 <> 0 ) then
     begin
          tamanoNuevo := tamanoNuevo + 1;
     end;

     if (Odd(listPDF.Count)) and  (listPDF.Count <> 0) then
     begin
	     iNumPDF := listPDF.Count - 1;
     end
     else
     begin
          iNumPDF := listPDF.Count;
     end;

         nuevoArreglo := TStringList.Create;
         orden := 0;
         nthreadsLanzados := 0;
         nuevoArreglo := TStringList.Create;
	 for iPos := 0 to tamanoNuevo  do
	 begin

              if(iPos >= 0 ) then
              begin
                   primero := iPos * 2;
                   segundo := iPos  * 2 + 1;
              end;

              if ( segundo < iNumPDF )  then
              begin
	           //lanzaThread( orden, primero, segundo )
                   //EJecutar Threads
                    FPDFMergeThread := TPDFMergeThread.Create(True,listPDF[primero], listPDF[segundo],'Merge_Thread_' + IntToStr(orden) + '_' + IntToStr(iPaquetesControl) + '.pdf',sPathDestino);
                    threads[ nthreadsLanzados ] := FPDFMergeThread;

                    FPDFMergeThread.Resume;
                    nuevoArreglo.Add(sPathDestino + 'Merge_Thread_' + IntToStr(orden) + '_' + IntToStr(iPaquetesControl) + '.pdf');


                    ///DebugText('PDF CREATE Thread # ' + IntToStr(nthreadsLanzados));

                    if( nthreadsLanzados = 3) then
                    begin
                         for iThreadCheck := 0  to nThreadsLanzados do
                         begin
                            if threads[ iThreadCheck ] <> nil then
                            begin
                               if not threads[ iThreadCheck ] .TerminateThread then
                               begin
                                    threads[ iThreadCheck ] .WaitFor;
                                    threads[ iThreadCheck ] := nil;
                               end;
                            end;
                         end;
                         nthreadsLanzados := 0;
                         ///DebugText('PDF SE TERMINO DE EJECUTAR LOS 4 THREAD ');
                    end
                    else
                    begin
                         Inc(nthreadsLanzados);
                    end;





                   //nuevoArreglo.Add('lanzaThread Reult ' + listPDF[primero] + ' ---- ' + listPDF[segundo]);
		   Inc( orden );

              end
              else
              begin
	           break;
              end;
		 //FPDFMergeThread.WaitFor;
	 end;

         if (Odd(listPDF.Count)) and  (listPDF.Count <> 0) then
         begin
              nuevoArreglo.Add(listPDF[listPDF.Count - 1]);
         end;


         if(tamanoNuevo = 1) then
              nuevoArreglo := listPDF;


         for iThreadCheck := 0  to 3 do
         begin
            if threads[ iThreadCheck ] <> nil then
            begin
               if not threads[ iThreadCheck ].TerminateThread then
               begin
                    threads[ iThreadCheck ].WaitFor;
                    threads[ iThreadCheck ] := nil;
               end;
            end;
         end;

           Sleep(5000);

         Result := nuevoArreglo;

         //Result := nuevoArreglo;
end;

{
    FIN
}



{
    Genera PDF MERGE Thread
}

function TPDFMerge.GeneraPDFMergeThread(sBaseName: String) : Boolean;
var
   lExito   : Boolean;
   aListPDF : TStringList;
   aListPDFAux : TStringList;
   dt       : TDateTime;
   iPos     : Integer;
   iPosAux  : Integer;
   sPFDAux   : String;
   iNumThead : Integer;
   FPDFMergeThread, FPDFMergeThread2 : TPDFMergeThread;
   iPDF             : Integer;
   aLisTempA         :   aLisTemp;
begin
     lExito := True;
     aListPDF := ListarPDFs(sPathOrigen);
     dt := now - dt;
//     Debug('Inicio del Proceso',dt);
     dt := now - dt;



     if( aListPDF.Count mod 2 = 0) then
     begin
          iPDF := aListPDF.Count - 1;
     end
     else
     begin
          iPDF := aListPDF.Count - 2;
     end;
     iPosAux := 0;
     for iPos := 0 to iPDF do
     begin
          if((iPosAux = iPDF - 1 ) and (iPosAux mod 2 = 0) ) then
          begin

          end
          else
          begin

               //Creamos la tarea suspendida
               FPDFMergeThread := TPDFMergeThread.Create(True,aListPDF[iPosAux], aListPDF[iPosAux + 1],'Merge_Thread_' + IntToStr(iPos) + '.pdf',sPathDestino);
               FPDFMergeThread.Resume;
//               Debug('PDF CREATE #1 Thread', dt);

          end;

          iPosAux := iPosAux + 2;



        {FPDFMergeThread2 := TPDFMergeThread.Create(true,aListPDF[iPosAux+2], aListPDF[iPosAux+3],'Merge_Thread_' + IntToStr(iPos + 1) + '.pdf',sPathDestino);
        Debug('PDF CREATE #2 Thread', dt);
        iPosAux := iPosAux + 4;
        // Iniciar las tareas
        FPDFMergeThread.Resume;
        Debug('PDF RESUME #1', dt);
        FPDFMergeThread2.Resume;
        Debug('PDF RESUME #2', dt);
        // Esperar a que acaben
        if not FPDFMergeThread.TerminateThread then
        FPDFMergeThread.WaitFor;
        Debug('PDF TERMINATE #1', dt);
        if not FPDFMergeThread2.TerminateThread then
        FPDFMergeThread2.WaitFor;
        Debug('PDF TERMINATE #2', dt); }

        if(iPosAux = iPDF) then break;

     end;

     if not FPDFMergeThread.TerminateThread then
        FPDFMergeThread.WaitFor;

//     Debug('PDF TERMINATE #1', dt);


     FreeAndNil(aListPDF);
     dt := now - dt;
//     Debug('Proceso Final en ', dt);
end;




{
    Muestra Mensaje en Consola
}

procedure TPDFMerge.Debug(sMensaje : String; dt : TDateTime);
begin
     AllocConsole;
     Writeln(Format(sMensaje + ' %s',[FormatDateTime('hh:mm:ss.zzz',dt)]));
end;

procedure TPDFMerge.DebugText(sMensaje : String);
begin
     AllocConsole;
     Writeln(sMensaje);
end;


{
    Genera PDF AUXILIAR
}

function TPDFMerge.GeneraPDFAux(sArchivo, sArchivo2,sBaseName: String) : String;
var
    DPL : TDebenuPDFLibrary1115;
begin
     DPL  := TDebenuPDFLibrary1115.Create;
     DPL.UnlockKey('jy9p34jr83a5sr8517jj95b6y');
     DPL.AddToFileList('FilesToMerge', sArchivo);
     DPL.AddToFileList('FilesToMerge', sArchivo2);
     DPL.MergeFileListFast('FilesToMerge', sPathDestino + sBaseName);
     DPL.Free;
     Result := sPathDestino + sBaseName;
end;

{
 Listado de PDF's en un directorio Especificado
}
function TPDFMerge.ListarPDFs(sPathOrigen : String) : TStringList;
var
   searchResult: TSearchRec;
begin
    Result := TStringList.Create;
    if FindFirst(sPathOrigen + '*.pdf', faAnyFile, searchResult ) = 0 then
      repeat
        if (searchResult.Attr and faDirectory = 0) or (searchResult.Name <> '.')
          and (searchResult.Name <> '..') then
            Result.Add(sPathOrigen + searchResult.Name);
      until FindNext(searchResult) <> 0;
    FindClose(searchResult);
end;

{
    Borra el Archvivo PDF Merge
}

function TPDFMerge.BorrarPDFTemporal(fichero: String): Boolean;
begin
  if FileExists(sPathDestino + fichero) then begin
     if (DeleteFile(sPathDestino + fichero)) then
     begin
          Result := True;
     end
     else
     begin
          Result := False;
     end
  end
  else
  begin
       Result := False;
  end;
end;

end.
