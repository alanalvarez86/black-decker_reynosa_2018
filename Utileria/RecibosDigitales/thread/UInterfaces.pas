unit UInterfaces;

interface

uses  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Contnrs;

type
  ILogBitacora = interface
    procedure EscribirBitacoraLog( sMensaje : string );
    procedure EscribirBitacoraError( sMensaje : string );
  end;

procedure Delay(ms: Cardinal);
procedure DelayNPM(ms: Cardinal);


implementation

procedure Delay(ms: Cardinal);
var
  Start: Cardinal;
begin
  Start:= GetTickCount;

  while ms + Start > GetTickCount do
    Application.ProcessMessages;
end;


procedure DelayNPM(ms: Cardinal);
var
  Start: Cardinal;
begin

  Windows.Sleep(ms);

{  Start:= GetTickCount;


  while ms + Start > GetTickCount do;}
end;


end.
