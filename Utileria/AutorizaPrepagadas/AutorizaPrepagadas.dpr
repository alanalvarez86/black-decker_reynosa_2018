program AutorizaPrepagadas;

{$APPTYPE CONSOLE}

uses
  SysUtils,
  ZetaClientTools,
  DIntefase in 'DIntefase.pas' {dmInterfase: TDataModule},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule};

var
   sComando: String;
begin
     ZetaClientTools.InitDCOM;
     try
        dmInterfase := TdmInterfase.Create( nil );
        with dmInterfase do
        begin
             //Solamente necesita la empresa el default de fechas es 0
             if ( ParamCount >= 1 )then
                Procesar
             else
             begin
                  sComando := Format( '%s %s=<X> ', [ ParamStr( 0 ), P_EMPRESA ] );
                  DespliegaAyuda( sComando );
             end;
        end;
     finally
            FreeAndNil( dmInterfase );
     end;
end.
