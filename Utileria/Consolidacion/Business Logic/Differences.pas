unit Differences;

interface
	uses		
		  SysUtils, Classes, System.Generics.Collections, Comparision, Row, MetaData, Data, Diff, Writer;

	function Get(Tables:TStringList; BaseConnStr,SlaveConnStr:String):TObjectList<TComparision>;	
	function StringsToCompare(Tables:TStringList):TObjectList<TComparision>;

implementation



	function Get(Tables:TStringList; BaseConnStr,SlaveConnStr:String):TObjectList<TComparision>;	
		var			
			CompareList:TObjectList<TComparision>;
		begin
			CompareList:=StringsToCompare(Tables);
			CompareList:=MetaData.Get(CompareList, BaseConnStr);
			CompareList:=Data.Get(CompareList, BaseConnStr, SlaveConnStr);			
			CompareList:=Diff.Get(CompareList);
			result:=CompareList;	
		end;


	function StringsToCompare(Tables:TStringList):TObjectList<TComparision>;
		var
			s:String;
			CompareList:TObjectList<TComparision>;			
		begin
			CompareList:=TobjectList<TComparision>.Create;					
			try
				for s in Tables do begin
					CompareList.Add(TComparision.Create(s));
				end;
			except
				on E: Exception do
				begin
					
				end;
			end;		
			result:=CompareList;	
		end;
		
end.