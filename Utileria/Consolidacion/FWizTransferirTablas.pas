unit FWizTransferirTablas;

interface

uses
 ZetaCommonClasses, Forms, dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, ZetaKeyLookup, Data.DB, cxCheckListBox, Vcl.ImgList, ZetaClientDataSet, Dialogs,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxSkinscxPCPainter, ZetaKeyLookup_DevEx, Windows, Graphics, ZetaCXGrid;

const
    K_DEFAULT_BIT = 'TransfiereTablas.txt';

type
  TWizTransferirTablas = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    pgBasesDatos: TdxWizardControlPage;
    lblFuente: TcxLabel;
    lblDestino: TcxLabel;
    DataSource: TDataSource;
    pgTablasComparar: TdxWizardControlPage;
    btnSeleccionarTodos: TcxButton;
    btnDesmarcarTodas: TcxButton;
    ImageButtons: TcxImageList;
    pgComparacion: TdxWizardControlPage;
    gridTablasCatalogosDBTableView: TcxGridDBTableView;
    gridTablasCatalogosLevel: TcxGridLevel;
    gridTablasCatalogos: TcxGrid;
    lblBitacora: TLabel;
    txtBitacora: TcxTextEdit;
    btnBitacora: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    DataSourceGrid: TDataSource;
    ENTIDAD: TcxGridDBColumn;
    luBDDestino: TZetaKeyLookup_DevEx;
    luBDFuente: TZetaKeyLookup_DevEx;
    pgTablasATransferir: TdxWizardControlPage;
    gridTablasTransferir: TcxGrid;
    gridTablasTransferirDBTableView: TcxGridDBTableView;
    ENTIDAD2: TcxGridDBColumn;
    gridTablasTransferirLevel: TcxGridLevel;
    ESTATUS: TcxGridDBColumn;
    DESTINO: TcxGridDBColumn;
    Transferir: TcxGridDBColumn;
    btnTransferirTodos: TcxButton;
    btnDesmarcarTodos: TcxButton;
    DataSourceGridTransferir: TDataSource;
    MENSAJE: TcxGridDBColumn;
    DESCRIPCION2: TcxGridDBColumn;
    DESCRIPCION1: TcxGridDBColumn;
    lblBitacoraTransferencia: TLabel;
    txtBitacoraTransf: TcxTextEdit;
    btnBitacoraTrans: TcxButton;
    pgEjecucionComparacion: TdxWizardControlPage;
    lblEstatus: TcxLabel;
    pbDetalle: TcxProgressBar;
    lblEstatusHeader: TcxLabel;
    pbAvance: TcxProgressBar;
    pgEjecucionTransferencia: TdxWizardControlPage;
    lblEstatusHeaderTransfer: TcxLabel;
    pbAvanceTransfer: TcxProgressBar;
    pnlResumen: TPanel;
    lblResumen: TcxLabel;
    btnVerBitacoraTrans: TcxButton;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    lblErrores: TcxLabel;
    lblDiferencias: TcxLabel;
    lblProcesados: TcxLabel;
    gridTablas: TZetaCXGrid;
    gridTablasDBTableView: TcxGridDBTableView;
    Nombre: TcxGridDBColumn;
    cxGridDBColumn1: TcxGridDBColumn;
    Elegir: TcxGridDBColumn;
    gridTablasLevel: TcxGridLevel;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure btnBitacora_BKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure cambiarSeleccion (elegir: Boolean);
    procedure btnBitacoraClick(Sender: TObject);
    procedure btnBitacoraTransClick(Sender: TObject);
    procedure btnVerBitacoraTransClick(Sender: TObject);
    procedure gridTablasTransferirDBTableViewEditing(
      Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
      var AAllow: Boolean);
    procedure gridTablasTransferirDBTableViewCellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btnTransferirTodosClick(Sender: TObject);
    procedure btnDesmarcarTodosClick(Sender: TObject);
    procedure gridTablasDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure gridTablasCatalogosDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure gridTablasTransferirDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
  private
    { Private declarations }
    Mutex      : THandle;
    fAvances   : TList;
    oParametros: TZetaParams;
    // fDataBase  : string;
    function Processing: Boolean;
    // procedure HiloFinalizado(Sender: TObject);
    procedure HabilitaBotones;
    procedure agregarTodasTablasCatalogos;
    procedure CargarTablasCatalogosEnGrid;
    procedure CargarTablasParaTransferirEnGrid;
    function CompararTablasCatalogos(out mensaje: string): Boolean;
    function TransferirTablasCatalogos(out mensaje: string): Boolean;
    function CallMeBack( const sMensaje: String; iValor: Integer): Boolean;
    function CallMeBackDetalle( iValorMax, iValor: Integer): Boolean;
    function CallMeBackTransfer( const sMensaje: String; iValor: Integer): Boolean;
    procedure limitarTablasSeleccion;
    procedure limitarTablasTransferir;
    function Warning( const sError: String; oControl: TWinControl ): Boolean;
    function HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
    procedure ApplyMinWidth;
  public
    { Public declarations }
  end;

var
  WizTransferirTablas: TWizTransferirTablas;

implementation

uses
  SysUtils, DMotorPatch, MotorPatchUtils, MotorPatchThreads, DBClient, ZetaCommonLists, ShellAPI,
  ZetaRegistryServer, DSistema, DConsolida, ZetaDialogo, ZetaCommonTools, FDetalleComparacion;


const
     // K_ANCHO_LINEA = 79;
     K_ANCHO_LINEA = 126;
     K_CONDICION = '@WHERE';
     K_TODOS = 'TODOS';
     K_DELTA_VALUES = '{ %s } <-> { %s }';
     K_ANCHO_REFERENCIA = 8;
     K_VALOR_NULO = 'NULLNULL';
     K_CONCEPTOS_PK = 'CO_NUMERO;CB_CODIGO;MO_REFEREN';
     K_EMPLEADOS_PK = 'CB_CODIGO';
     VACIO = '';

{$R *.dfm}

{function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle, nil, StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ), StrPCopy( zDir, DefaultDir ), 10 );
end;}

function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[0..79] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;

{ TWizTransferirTablas }

procedure TWizTransferirTablas.FormCreate(Sender: TObject);
begin
  HelpContext := 3;
  Mutex := CreateMutex(nil, False, PChar(ClassName + '.Execute'));
  if Mutex = 0 then
    RaiseLastOSError;

  fAvances := TList.Create;
end;

procedure TWizTransferirTablas.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := not Processing;
end;

procedure TWizTransferirTablas.FormDestroy(Sender: TObject);
begin
  FreeAndNil(oParametros);
  FreeAndNil(fAvances);
  CloseHandle(Mutex);
end;

procedure TWizTransferirTablas.FormShow(Sender: TObject);
begin
    with dmSistema do
    begin
        cdsSistBaseDatos.Conectar;
        cdsSistBaseDatosLookup.Conectar;
        // DataSource.DataSet := cdsSistBaseDatos;
        luBDFuente.LookupDataset := cdsSistBaseDatosLookUp;
        luBDDestino.LookupDataset := cdsSistBaseDatosLookUp;
    end;

    txtBitacora.Text := Format( '%sComparaTablasTrans.txt', [ ExtractFilePath( Application.ExeName ) ] );
    txtBitacoraTransf.Text := Format( '%s' + K_DEFAULT_BIT, [ ExtractFilePath( Application.ExeName ) ] );

    //Desactiva la posibilidad de agrupar
    gridTablasDBTableView.OptionsCustomize.ColumnGrouping := False;
    //Esconde la caja de agrupamiento
    gridTablasDBTableView.OptionsView.GroupByBox := False;
    //Para que nunca muestre el filterbox inferior
    gridTablasDBTableView.FilterBox.Visible := fvNever;
    //Para que no aparezca el Custom Dialog
    gridTablasDBTableView.FilterBox.CustomizeDialog := False;
    //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
    // gridTablasDBTableView.ApplyBestFit();
end;

procedure TWizTransferirTablas.gridTablasCatalogosDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;
procedure TWizTransferirTablas.gridTablasDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TWizTransferirTablas.gridTablasTransferirDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
  var sMensaje: String;
      i: Integer;
begin

    if StrLleno (ACellViewInfo.GridRecord.DisplayTexts [MENSAJE.Index]) then
    begin
        sMensaje :=  // ( StringOfChar( '=', 79 ) ) + CR_LF +
                   ( Format( 'TABLA %s ( %s )', [ ACellViewInfo.GridRecord.DisplayTexts [ENTIDAD.Index], ACellViewInfo.GridRecord.DisplayTexts [DESCRIPCION2.Index] ] ) );
        with dmConsolida.TablasN[ACellViewInfo.GridRecord.DisplayTexts [ENTIDAD.Index]].Llaves do
        begin
              sMensaje := sMensaje + CR_LF + 'Llave Primaria: ';
              for i := 0 to ( Count - 1 ) do
              begin
                   sMensaje := sMensaje + Strings[ i ] + CR_LF;
                   sMensaje := sMensaje + StringOfChar( ' ', 20 );
              end;
        end;
        sMensaje := sMensaje + ACellViewInfo.GridRecord.DisplayTexts [MENSAJE.Index];
        FDetalleComparacion.MostrarComparacion(ACellViewInfo.GridRecord.DisplayTexts [ENTIDAD.Index], luBDDestino.Descripcion, luBDFuente.Descripcion, sMensaje);
    end;

end;

procedure TWizTransferirTablas.gridTablasTransferirDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TWizTransferirTablas.gridTablasTransferirDBTableViewEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  with Sender.DataController do
  begin
      if Pos ('Faltan', Values[FocusedRecordIndex, ESTATUS.Index]) > 0 then
        AAllow := TRUE
      else
        AAllow := FALSE;
  end;
end;

function TWizTransferirTablas.Processing: Boolean;
begin
    Result := ((lblEstatus.Caption <> 'Estatus') and (lblEstatus.Caption <> 'Proceso finalizado')) or
                ((lblEstatusHeaderTransfer.Caption <> 'Tablas y cat�logos')
                    and (lblEstatusHeaderTransfer.Caption <> 'Proceso finalizado'));
end;

procedure TWizTransferirTablas.HabilitaBotones;
begin
  with dxWizardControl.Buttons do
  begin
       Next.Enabled := not Processing;
       Back.Enabled := not Processing;
  end;
end;

procedure TWizTransferirTablas.btnBitacoraClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtBitacora.Text), 'ComparaTablasTrans.txt');
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );

      if cxOpenDialog.Execute then
        txtBitacora.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TWizTransferirTablas.btnBitacoraTransClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtBitacoraTransf.Text), K_DEFAULT_BIT);
      if cxOpenDialog.Execute then
        txtBitacoraTransf.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TWizTransferirTablas.btnBitacora_BKClick(Sender: TObject);
// var
   // oTexto: TStringList;
   // oCDS: TClientDataSet;
   // eTipo: eTipoBitacora;
   // sArchivo: string;
begin
  // oTexto := TStringList.Create;
  // oCDS := TClientDataSet.Create(Self);
  with oParametros do
  try
     {oTexto.Add(Format('********** Inicio: %s  **********',  [FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now)]));
     oTexto.Add(Format('      Sentencias: %d', [ParamByName('PRO_EXITOS').AsInteger]));
     oTexto.Add(Format('    Advertencias: %d', [ParamByName('PRO_ADVERTENCIAS').AsInteger]));
     oTexto.Add(Format('         Errores: %d', [ParamByName('PRO_ERRORES').AsInteger]));
     oTexto.Add('');
     // Lista de advertencias y errores
     oCDS.Data := dmDBConfig.GetBitacora(ParamByName('DB_PROCESO').AsInteger);
     while not oCDS.Eof do
     begin
          eTipo := eTipoBitacora(oCDS.FieldByName('BI_TIPO').AsInteger);
          if eTipo in [tbAdvertencia, tbError] then begin
             oTexto.Add(Format('%s: %s', [ObtieneElemento(lfTipoBitacora, Ord(eTipo)), oCDS.FieldByName('BI_TEXTO').AsString]));
             if eTipo = tbError then
                oTexto.Add(StringOfChar(' ', 7) + oCDS.FieldByName('BI_DATA').AsString);
          end;
          oCDS.Next;
     end;
     if not oCDS.IsEmpty then
        oTexto.Add('');

    if ParamByName('PRO_ERRORES').AsInteger > 0 then
       oTexto.Add('La actualizaci�n termin� con errores, favor de revisarlos y ejecutar el proceso de nuevo.')
    else
        oTexto.Add(lblResumen.Caption);
    oTexto.Add(Format('********** Final: %s **********',  [FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now)]));
    sArchivo := ExtractFilePath(ParamStr(0)) + 'COMPARTE_Actualiza_' + ParamByName('DB_APLICAVERSION').AsString + '.log';
    oTexto.SaveToFile(sArchivo);}

    // ShellExecute(0, nil, 'notepad', PChar(sArchivo), nil, SW_SHOWNORMAL);
    ShellExecute(0, nil, 'notepad', PChar(txtBitacora.Text), nil, SW_SHOWNORMAL);
  finally
      // FreeAndNil(oTexto)
  end;
end;

procedure TWizTransferirTablas.btnSeleccionarTodosClick(Sender: TObject);
begin
    cambiarSeleccion(TRUE);
end;

procedure TWizTransferirTablas.btnDesmarcarTodasClick(Sender: TObject);
begin
    cambiarSeleccion(FALSE);
end;

procedure TWizTransferirTablas.cambiarSeleccion (elegir: Boolean);
var sTablaActual: String;
begin
    with DataSource.DataSet do
    begin
        sTablaActual := FieldByName('Nombre').AsString;
        gridTablasDBTableView.OptionsView.ScrollBars := ssNone;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName ('Elegir').AsBoolean := elegir;
            Post;
            Next;
        end;
        First;
        gridTablasDBTableView.OptionsView.ScrollBars := ssBoth;
        Locate('Nombre', sTablaActual, []);
    end;
end;

procedure TWizTransferirTablas.btnDesmarcarTodosClick(Sender: TObject);
begin
      gridTablasTransferirDBTableView.OptionsView.ScrollBars := ssNone;
      DataSourceGridTransferir.DataSet.First;
      while not DataSourceGridTransferir.DataSet.Eof do
      begin
          DataSourceGridTransferir.DataSet.Edit;
          DataSourceGridTransferir.DataSet.FieldByName ('Transferir').AsBoolean := FALSE;
          DataSourceGridTransferir.DataSet.Post;
          DataSourceGridTransferir.DataSet.Next;
      end;
      DataSourceGridTransferir.DataSet.First;
      gridTablasTransferirDBTableView.OptionsView.ScrollBars := ssBoth;
end;

procedure TWizTransferirTablas.btnTransferirTodosClick(Sender: TObject);
begin
      gridTablasTransferirDBTableView.OptionsView.ScrollBars := ssNone;
      DataSourceGridTransferir.DataSet.First;
      // DataSourceGridTransferir.DataSet.Edit;
      while not DataSourceGridTransferir.DataSet.Eof do
      begin
          if  Pos ('Faltan', DataSourceGridTransferir.DataSet.FieldByName ('ESTATUS').AsString) > 0 then
          begin
              DataSourceGridTransferir.DataSet.Edit;
              DataSourceGridTransferir.DataSet.FieldByName ('Transferir').AsBoolean := TRUE;
              DataSourceGridTransferir.DataSet.Post;
          end;

          DataSourceGridTransferir.DataSet.Next;
      end;
      DataSourceGridTransferir.DataSet.First;
      gridTablasTransferirDBTableView.OptionsView.ScrollBars := ssBoth;
end;

procedure TWizTransferirTablas.btnVerBitacoraTransClick(Sender: TObject);
begin
    ShellExecute(0, nil, 'notepad', PChar(txtBitacoraTransf.Text), nil, SW_SHOWNORMAL);
end;

procedure TWizTransferirTablas.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;
  var AHandled: Boolean);
var
  mensaje : string;
  oCursor: TCursor;
begin
  case AKind of
    wcbkNext:
      begin

        if (dxWizardControl.ActivePage = pgBasesDatos) then
        begin
            if luBDFuente.Llave = VACIO then
            begin
                AHandled := Warning('El campo Fuente no puede quedar vac�o.', luBDFuente);
            end
            else if luBDDestino.Llave = VACIO then
            begin
                AHandled := Warning('El campo Destino no puede quedar vac�o.', luBDDestino);
            end
            else if luBDFuente.Llave = luBDDestino.Llave then
            begin
                AHandled := Warning('La base de datos Destino no puede ser igual a la base de datos Fuente.', luBDDestino);
            end
            else
            begin
                oCursor := Screen.Cursor;
                try
                    // Proceso de inicar empresas y tablas en DConsolida
                    dxWizardControl.Enabled := FALSE;
                    Screen.Cursor := crHourglass;

                    // Inicializar bases de datos seleccionadas
                    dmConsolida.InitEmpresas (luBDFuente.Llave, luBDDestino.Llave);
                    // Inicializar tablas
                    dmConsolida.InitTablas;
                    // Agregar las tablas en el grid
                    agregarTodasTablasCatalogos;
                finally
                    dxWizardControl.Enabled := TRUE;
                    Screen.Cursor := oCursor;
                end;
            end;

        end;

        if (dxWizardControl.ActivePage = pgTablasComparar) then
        begin
            if not HayTablaSeleccionada (DataSource.DataSet, 2) then
                AHandled := Warning('Debe seleccionar al menos una tabla.',  gridTablas)
            else
            begin
                dmConsolida.LimpiarDiferenciasErrores;
                CargarTablasCatalogosEnGrid;
            end;
        end;

        if (dxWizardControl.ActivePage = pgComparacion) then
        // if (dxWizardControl.ActivePage = pgEjecucionComparacion) then
        begin
            pbAvance.Position := 0;
            pbDetalle.Position := 0;
            dxWizardControl.ActivePage := pgEjecucionComparacion;
            oCursor := Screen.Cursor;
            try
                dxWizardControl.Enabled := FALSE;
                Screen.Cursor := crHourglass;
                // Ejecutar Proceso de Comparaci�n de Tablas y Cat�logos
                CompararTablasCatalogos(mensaje);
                // Fin de Proceso
            finally
                dxWizardControl.Enabled := TRUE;
                Screen.Cursor := oCursor;
                // Acciones despu�s de ejecutar la comparaci�n
                CargarTablasParaTransferirEnGrid;
                // pbAvanceTransfer.Position := 0;
            end;
        end;

        if (dxWizardControl.ActivePage = pgTablasATransferir) then
        begin
            if StrVacio (ExtractFileName (txtBitacoraTransf.Text)) then
                AHandled := Warning('Debe proporcionar un nombre de archivo de bit�cora.',  txtBitacoraTransf)
            else if not HayTablaSeleccionada (DataSourceGridTransferir.DataSet, 3) then
                AHandled := Warning('Debe seleccionar al menos una tabla con registros faltantes a transferir.',  gridTablasTransferir)
            else
            begin
                pnlResumen.Visible := FALSE;
                pbAvanceTransfer.Position := 0;
                Application.ProcessMessages;
                dxWizardControl.ActivePage := pgEjecucionTransferencia;
                oCursor := Screen.Cursor;
                try
                    dxWizardControl.Enabled := FALSE;
                    Screen.Cursor := crHourglass;
                    // Ejecutar Proceso de Transferencia de Tablas y Cat�logos
                    if TransferirTablasCatalogos(mensaje) then
                        pnlResumen.Visible := TRUE;
                    // Fin de Proceso
                finally
                    dxWizardControl.Enabled := TRUE;
                    Screen.Cursor := oCursor;

                    // Resultados del proceso
                    lblProcesados.Caption := IntToStr (dmConsolida.TablasCountSeleccionadas);
                    lblDiferencias.Caption := IntToStr (dmConsolida.TablasConDiferencias);
                    lblErrores.Caption := IntToStr (dmConsolida.TablasConError);
                end;
            end;
        end;

      end;
    wcbkCancel:
      begin
        if not Processing then
          ModalResult := mrCancel;
      end;

    wcbkFinish:
      ModalResult := mrOk;

  end;
  HabilitaBotones();
end;

procedure TWizTransferirTablas.dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage;
  var AAllow: Boolean);
begin
  if ANewPage = pgComparacion then
    dxWizardControl.Buttons.Next.Caption := 'Comparar'
  else if ANewPage = pgTablasATransferir then
    dxWizardControl.Buttons.Next.Caption := 'Transferir'
  else
    dxWizardControl.Buttons.Next.Caption := 'Siguiente';
end;

procedure TWizTransferirTablas.agregarTodasTablasCatalogos;
 var cdsTablasCatalogos : TZetaClientDataSet;
     i : Integer;
begin
     cdsTablasCatalogos := TZetaClientDataSet.Create(Self);
     with cdsTablasCatalogos do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('Nombre', 20);
          AddStringField('Descripcion', 45);
          AddBooleanField('Elegir');
          CreateTempDataset;
          Open;
     end;

     for i:=0 to dmConsolida.TablasCount-1 do
     begin
        cdsTablasCatalogos.Insert;
        cdsTablasCatalogos.FieldByName('Nombre').AsString := dmConsolida.Tablas[i].Nombre;
        cdsTablasCatalogos.FieldByName('Descripcion').AsString := dmConsolida.Tablas[i].Descripcion;
        cdsTablasCatalogos.FieldByName('Elegir').AsBoolean := FALSE;
        cdsTablasCatalogos.Post;
     end;

     DataSource.DataSet := cdsTablasCatalogos;
     gridTablasDBTableView.DataController.DataSource := DataSource;

     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     // gridTablasDBTableView.ApplyBestFit();
end;

procedure TWizTransferirTablas.CargarTablasCatalogosEnGrid;
 var cdsTablasCatalogos : TZetaClientDataSet;
begin
     cdsTablasCatalogos := TZetaClientDataSet.Create(Self);
     with cdsTablasCatalogos do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('Entidad', 20);
          AddStringField('Descripcion', 45);
          CreateTempDataset;
          Open;
     end;

     with DataSource.DataSet do
     begin
         First;
         while not Eof do
         begin
              if FieldByName('Elegir').AsBoolean then
              begin
                  cdsTablasCatalogos.Insert;
                  cdsTablasCatalogos.FieldByName('Entidad').AsString := FieldByName('Nombre').AsString;
                  cdsTablasCatalogos.FieldByName('Descripcion').AsString := FieldByName('Descripcion').AsString;
                  cdsTablasCatalogos.Post;
              end;
              Next;
         end;
     end;

     DataSourceGrid.DataSet := cdsTablasCatalogos;
     gridTablasCatalogosDBTableView.DataController.DataSource := DataSourceGrid;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     // gridTablasCatalogosDBTableView.ApplyBestFit();
end;

procedure TWizTransferirTablas.CargarTablasParaTransferirEnGrid;
 var cdsTablasCatalogos : TZetaClientDataSet;
begin
     cdsTablasCatalogos := TZetaClientDataSet.Create(Self);
     with cdsTablasCatalogos do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          AddStringField('Entidad', 20);
          AddStringField('Descripcion', 45);
          AddStringField('Estatus', 29);
          AddBooleanField('Transferir');
          // AddStringField('Mensaje', 150);
          AddMemoField('Mensaje', 250);
          CreateTempDataset;
          Open;
     end;


     // if dmConsolida.TablasConError > 0 then
        // MENSAJE.Visible := TRUE;
     // else
        MENSAJE.Visible := FALSE;

      DataSourceGrid.DataSet.First;
      while not DataSourceGrid.DataSet.Eof do
      begin
          cdsTablasCatalogos.Insert;
          cdsTablasCatalogos.FieldByName('Entidad').AsString := DataSourceGrid.DataSet.FieldByName('Entidad').AsString;
          cdsTablasCatalogos.FieldByName('Descripcion').AsString := dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].Descripcion;
          cdsTablasCatalogos.FieldByName('Transferir').AsBoolean := FALSE;

          if not dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].Error then
          begin
              if dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].Diferencias then
              begin
                  cdsTablasCatalogos.FieldByName('Estatus').AsString := 'Diferencias';

                  // Armar contenido de columna MENSAJE.
                  // Tendr� un formato parecido a como queda en ComparaTablas.txt
                  // cdsTablasCatalogos.FieldByName('MENSAJE').AsString := CR_LF + 'Registros Diferentes En Empresa Maestra ' + Format( K_DELTA_VALUES, [ '<Valor Maestro>', '<Valor Fuente>' ] ) + CR_LF
                  cdsTablasCatalogos.FieldByName('Mensaje').AsString := CR_LF + 'Registros diferentes en base de datos destino ' + Format( K_DELTA_VALUES, [ '<Valor destino>', '<Valor fuente>' ] ) + CR_LF
                      + StringOfChar( '-', K_ANCHO_LINEA );
                  //
                  cdsTablasCatalogos.FieldByName('Mensaje').AsString := cdsTablasCatalogos.FieldByName('Mensaje').AsString
                      + dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].DiferenciasTS.Text
                      + StringOfChar( '-', K_ANCHO_LINEA );

                  if dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].Registros then
                  begin
                      if StrLleno (cdsTablasCatalogos.FieldByName('Estatus').AsString) then
                        cdsTablasCatalogos.FieldByName('Estatus').AsString := cdsTablasCatalogos.FieldByName('Estatus').AsString + ', ';
                      cdsTablasCatalogos.FieldByName('Estatus').AsString := cdsTablasCatalogos.FieldByName('Estatus').AsString +
                      'Faltan registros';
                      cdsTablasCatalogos.FieldByName('Transferir').AsBoolean := TRUE;
                  end
              end
              else if dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].Registros then
              begin
                  cdsTablasCatalogos.FieldByName('Estatus').AsString := 'Faltan registros';
                  cdsTablasCatalogos.FieldByName('Transferir').AsBoolean := TRUE;
              end
              else
                  cdsTablasCatalogos.FieldByName('Estatus').AsString := 'Igual';

              // if StrLleno (dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('ENTIDAD').AsString].FaltantesTS.Text) then
              if dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].Registros then
              begin
                  cdsTablasCatalogos.FieldByName('Mensaje').AsString := cdsTablasCatalogos.FieldByName('Mensaje').AsString +
                          // CR_LF + 'Registros No Existentes En Empresa Maestra' + CR_LF +
                          CR_LF + 'Registros no existentes base de datos destino' + CR_LF +
                          StringOfChar( '-', K_ANCHO_LINEA ) + CR_LF +
                          dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].FaltantesTS.Text +
                          StringOfChar( '-', K_ANCHO_LINEA )
                  {cdsTablasCatalogos.FieldByName('MENSAJE').AsString := cdsTablasCatalogos.FieldByName('Mensaje').AsString +
                      // ' || ' +
                      CR_LF +
                      dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('ENTIDAD').AsString].FaltantesTS.Text;}
              end;
          end
          else
          begin
              cdsTablasCatalogos.FieldByName('Estatus').AsString := 'Error (incompatible)';
              cdsTablasCatalogos.FieldByName('Mensaje').AsString := CR_LF +
                dmConsolida.TablasN[DataSourceGrid.DataSet.FieldByName('Entidad').AsString].MensajeError.Text;
          end;

          // i := i + 1;
          DataSourceGrid.DataSet.Next;
      end;

      cdsTablasCatalogos.Post;

       // end;
     // end;

     DataSourceGridTransferir.DataSet := cdsTablasCatalogos;
     gridTablasTransferirDBTableView.DataController.DataSource := DataSourceGridTransferir;

     // Ajustar grid
     //Desactiva la posibilidad de agrupar
     gridTablasTransferirDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     gridTablasTransferirDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     gridTablasTransferirDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     gridTablasTransferirDBTableView.FilterBox.CustomizeDialog := False;
     //DevEx: Para que ponga el MinWidth ideal al titulo de las columnas. Posteriormente se aplica el BestFit.
     ApplyMinWidth;
     gridTablasTransferirDBTableView.ApplyBestFit();
end;

{procedure TWizTransferirTablas.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   FLog: TTransferLog;
   i: Integer;
begin
     FLog := TTransferLog.Create;
     try
        FLog.Start( LogFileName );
        ShowStatus( 'Empezando Comparaci�n' );
        with dmCompara do
        begin
             with ProgressBar do
             begin
                  Step := 1;
                  Max := TablasCount;
             end;
             if Transferir.Checked then
             begin
                  with Catalogos do
                  begin
                       for i := 0 to ( Items.Count - 1 ) do
                       begin
                            Tablas[ i ].Transfer := Checked[ i ];
                       end;
                  end;
             end;
             if not CompararTablas( CallMeBack, FLog ) then
                ZetaDialogo.zInformation( '� Buenas Noticias !', 'No Se Encontraron Diferencias', 0 );
        end;
        ShowStatus( 'Comparaci�n Terminada' );
        lOk := True;
     finally
            with FLog do
            begin
                 Close;
                 Free;
            end;
     end;
     if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
        ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;}

function TWizTransferirTablas.CompararTablasCatalogos(out mensaje: string): Boolean;
var
   lOk : Boolean;
   FLog: TTransferLog;
begin
     // lOk := FALSE;
     try

         // ZetaDialogo.ZInformation('Mensaje', 'Iniciando proceso', 0);
         // mensaje := '';

         FLog := TTransferLog.Create;
         try
            // FLog.Start( LogFileName );
            FLog.Start( txtBitacora.Text );

            // ShowStatus( 'Empezando Comparaci�n' );
            lblEstatus.Caption := 'Empezando Comparaci�n';
            // with dmCompara do
            with dmConsolida do
            begin
                 {if Transferir.Checked then
                 begin
                      with Catalogos do
                      begin
                           for i := 0 to ( Items.Count - 1 ) do
                           begin
                                Tablas[ i ].Transfer := Checked[ i ];
                           end;
                      end;
                 end;}

                 // Antes de CompararTablas, limitar selecci�n de tablas
                 limitarTablasSeleccion;

                 // Ajustar m�ximo de la barra de progreso a las tablas que ser�n comparadas.
                 pbAvance.Properties.Max := TablasCountSeleccionadas;

                 // bCompararTablas :=
                 CompararTablas( CallMeBack, FLog, CallMeBackDetalle );
                 // if not CompararTablas( CallMeBack, FLog, CallMeBackDetalle ) then
                 // begin
                    CallMeBack ('Proceso finalizado',  dmConsolida.TablasCountSeleccionadas);
                    // lblEstatus.Caption := 'Comparaci�n Terminada';

                 {if not bCompararTablas then
                    ZetaDialogo.zInformation( '� Buenas Noticias !', 'No Se Encontraron Diferencias', 0 );}
                 {end
                 else
                 begin
                    lblEstatus.Caption := 'Comparaci�n Terminada';
                    CallMeBack ('',  dmConsolida.TablasCountSeleccionadas);
                 end;}

            end;
            // ShowStatus( 'Comparaci�n Terminada' );
            // lblEstatus.Caption := 'Comparaci�n Terminada';
            lOk := True;
         finally
                with FLog do
                begin
                     Close;
                     Free;
                end;
         end;
         // if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
            // ExecuteFile ( 'NOTEPAD.EXE', ExtractFileName( txtBitacora.Text ), ExtractFilePath( txtBitacora.Text ), SW_SHOWDEFAULT );
     Except on e:exception do
     begin
          ZetaDialogo.ZError('Error', mensaje, 0);
          lOk := FALSE;
     end;
     end;

     Result := lOk;
end;

function TWizTransferirTablas.CallMeBack( const sMensaje: String; iValor: Integer): Boolean;
begin
     lblEstatus.Caption := sMensaje;
     pbAvance.Position := iValor;
     Application.ProcessMessages;
     Result := TRUE;
     // Result := not Wizard.Cancelado;
end;

function TWizTransferirTablas.CallMeBackDetalle( iValorMax, iValor: Integer ): Boolean;
begin
     // if iValorMax > 0 then
        pbDetalle.Properties.Max := iValorMax;
     pbDetalle.Position := iValor;
     Application.ProcessMessages;
     Result := TRUE;
end;

function TWizTransferirTablas.CallMeBackTransfer( const sMensaje: String; iValor: Integer): Boolean;
begin
     lblEstatusHeaderTransfer.Caption := sMensaje;
     pbAvanceTransfer.Position := iValor;
     Application.ProcessMessages;
     Result := TRUE;
end;

procedure TWizTransferirTablas.limitarTablasSeleccion;
var i: Integer;
begin
    gridTablasCatalogosDBTableView.OptionsView.ScrollBars := ssNone;
    for i := 0 to dmConsolida.TablasCount-1 do
    begin
        dmConsolida.Tablas[i].Seleccionada := FALSE;
        DataSourceGrid.DataSet.First;
        while (not DataSourceGrid.DataSet.Eof) and (not dmConsolida.Tablas[i].Seleccionada)do
        begin
            if DataSourceGrid.DataSet.FieldByName('ENTIDAD').AsString = dmConsolida.Tablas[i].Nombre then
            begin
                dmConsolida.Tablas[i].Seleccionada := TRUE;
            end;
            DataSourceGrid.DataSet.Next;
        end;
    end;
    gridTablasCatalogosDBTableView.OptionsView.ScrollBars := ssBoth;
end;

procedure TWizTransferirTablas.limitarTablasTransferir;
// var i: Integer;
begin
    gridTablasTransferirDBTableView.OptionsView.ScrollBars := ssNone;
    // for i := 0 to dmConsolida.TablasCount-1 do
    // begin
        // dmConsolida.Tablas[i].Transfer := FALSE;
        DataSourceGridTransferir.DataSet.First;
        while (not DataSourceGridTransferir.DataSet.Eof) do //  and (not dmConsolida.Tablas[i].Transfer)do
        begin
            // if DataSourceGridTransferir.DataSet.FieldByName('ENTIDAD').AsString = dmConsolida.Tablas[i].Nombre then
            // begin
                if DataSourceGridTransferir.DataSet.FieldByName('TRANSFERIR').AsBoolean then
                    dmConsolida.TablasN [DataSourceGridTransferir.DataSet.FieldByName('ENTIDAD').AsString].Transfer := TRUE
                else
                    dmConsolida.TablasN [DataSourceGridTransferir.DataSet.FieldByName('ENTIDAD').AsString].Transfer := FALSE;
            // end;
            DataSourceGridTransferir.DataSet.Next;
        end;
    // end;

    gridTablasTransferirDBTableView.OptionsView.ScrollBars := ssBoth;
end;

function TWizTransferirTablas.TransferirTablasCatalogos(out mensaje: string): Boolean;
var
   lOk : Boolean;
   FLog: TTransferLog;
begin
     // lOk := FALSE;
     try

         // ZetaDialogo.ZInformation('Mensaje', 'Iniciando proceso', 0);
         // mensaje := '';

         FLog := TTransferLog.Create;
         try
            // FLog.Start( LogFileName );
            if StrVacio(ExtractFileExt(txtBitacoraTransf.Text)) then
                FLog.Start( txtBitacoraTransf.Text + '.txt' )
            else
              FLog.Start( txtBitacoraTransf.Text );

            // ShowStatus( 'Empezando Comparaci�n' );
            // lblEstatus.Caption := 'Empezando Comparaci�n';
            lblEstatusHeaderTransfer.Caption := 'Empezando transferencia';
            // with dmCompara do
            with dmConsolida do
            begin
                 {if Transferir.Checked then
                 begin
                      with Catalogos do
                      begin
                           for i := 0 to ( Items.Count - 1 ) do
                           begin
                                Tablas[ i ].Transfer := Checked[ i ];
                           end;
                      end;
                 end;}

                 // Antes de CompararTablas, limitar selecci�n de tablas
                 // limitarTablasSeleccion;
                 limitarTablasTransferir;

                 // Ajustar m�ximo de la barra de progreso a las tablas que ser�n comparadas.
                 pbAvanceTransfer.Properties.Max := TablasTransferirCountSeleccionadas;

                 // bTransferirTablas :=
                 TransferirTablas( CallMeBackTransfer, FLog );
                    CallMeBackTransfer ('Proceso finalizado',  dmConsolida.TablasTransferirCountSeleccionadas);

                 // if not bCompararTablas then
                    // ZetaDialogo.zInformation( '� Buenas Noticias !', 'No Se Encontraron Diferencias', 0 );
            end;
            lOk := True;
         finally
                with FLog do
                begin
                     Close;
                     Free;
                end;
         end;
     Except on e:exception do
     begin
          ZetaDialogo.ZError('Error', mensaje, 0);
          lOk := FALSE;
     end;
     end;

     Result := lOk;
end;

function TWizTransferirTablas.Warning( const sError: String; oControl: TWinControl ): Boolean;
begin
     ZetaDialogo.ZWarning( Caption, sError, 0, mbOK );
     oControl.SetFocus;
     Result := TRUE;
end;

function TWizTransferirTablas.HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
begin
    Result := FALSE;
    with DataSet do
    begin
        First;
        while not Eof do
        begin
            if Fields[iColumna].AsBoolean then
            begin
                Result := TRUE;
                Break;
            end;
            Next;
        end;
    end;
end;

procedure TWizTransferirTablas.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 35;
begin
     with  gridTablasTransferirDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

end.
