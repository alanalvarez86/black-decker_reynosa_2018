unit FDiffDetail;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.Menus, cxContainer, cxLabel, Vcl.StdCtrls,
  cxButtons, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Vcl.ImgList;

type
  TDiffDetail = class(TForm)
    gridTableView: TcxGridDBTableView;
    grid: TcxGrid;
    btn_Accept: TcxButton;
    btn_Cancel: TcxButton;
    SlaveCode: TcxGridDBColumn;
    SlaveDesc: TcxGridDBColumn;
    BaseDesc: TcxGridDBColumn;
    BaseCode: TcxGridDBColumn;
    DataSource: TDataSource;
    btnSeleccionarTodos: TcxButton;
    btnDesmarcarTodas: TcxButton;
    ImageButtons: TcxImageList;
    procedure btn_CancelClick(Sender: TObject);
    procedure btn_AcceptClick(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
  private
    _Index:Integer;
    _IsInsertionView:Boolean;
		_TableName:String;
    procedure LoadForm(Index:Integer; IsInsertionView:Boolean);
    procedure CambiarSeleccion(elegir:Boolean);
  public
	 	Constructor Create(AOwner:TComponent); overload; override;
		Constructor Create(AOwner:TComponent; Index:Integer; IsInsertionView:Boolean); reintroduce; overload;

  end;

var
  DiffDetail: TDiffDetail;

implementation

{$R *.dfm}
	uses DConsolidacion;

	Constructor TDiffDetail.Create(AOwner : TComponent);
		begin
			inherited Create(AOwner);
		end;


  procedure TDiffDetail.CambiarSeleccion(elegir:Boolean);
		begin
      with DataSource.DataSet do begin
        gridTableView.OptionsView.ScrollBars := ssNone;
        First;
        while not Eof do begin
          Edit;
          FieldByName ('Elegir').AsBoolean := elegir;
          Post;
          Next;
        end;
        First;
        gridTableView.OptionsView.ScrollBars := ssBoth;
      end;
		end;

  procedure TDiffDetail.btnDesmarcarTodasClick(Sender: TObject);
    begin
      CambiarSeleccion(false);
    end;

procedure TDiffDetail.btnSeleccionarTodosClick(Sender: TObject);
    begin
      CambiarSeleccion(true);
    end;

procedure TDiffDetail.btn_AcceptClick(Sender: TObject);
    begin
      AddDiffDetail(DataSource.DataSet, _Index, _IsInsertionView);
      Close;
    end;

  procedure TDiffDetail.btn_CancelClick(Sender: TObject);
    begin
      Close;
    end;

Constructor TDiffDetail.Create(AOwner:TComponent; Index:Integer; IsInsertionView:Boolean);
		begin
			inherited Create(AOwner);

			gridTableView.OptionsCustomize.ColumnGrouping := False;
			gridTableView.OptionsView.GroupByBox := False;
			gridTableView.FilterBox.Visible := fvNever;
			gridTableView.FilterBox.CustomizeDialog := False;
			LoadForm(Index,  IsInsertionView);
		end;



	procedure TDiffDetail.LoadForm(Index:Integer;  IsInsertionView:Boolean);
		begin
			_Index:=Index;
      _IsInsertionView:=IsInsertionView;
			DataSource.DataSet := DConsolidacion.GetDiffDetails(Index, IsInsertionView, _TableName);
			
			if IsInsertionView then begin
				self.Caption:='Inserciones: ' + _TableName;
			end
			else begin
				self.Caption:='Modificaciones: ' + _TableName;
			end;

      
			gridTableView.DataController.DataSource := DataSource;															

		end;

end.