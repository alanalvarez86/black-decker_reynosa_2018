﻿unit FWizCompararTablas;

interface

uses
 ZetaCommonClasses, Forms, dxWizardControlForm, TressMorado2013, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
 dxSkinsCore, cxContainer, cxEdit, cxProgressBar, cxCheckBox, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLabel,
 dxCustomWizardControl, dxWizardControl, System.Classes, Vcl.Controls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Data.DB, cxCheckListBox, Vcl.ImgList, ZetaClientDataSet, Dialogs,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  dxSkinscxPCPainter, ZetaCXGrid, System.IOUtils;

const
    K_DEFAULT_BIT = 'ComparaTablas.txt';

type
  TWizCompararTablas = class(TdxWizardControlForm)
    dxWizardControl: TdxWizardControl;
    pgFinProceso: TdxWizardControlPage;
    pgBasesDatos: TdxWizardControlPage;
    lblFuente: TcxLabel;
    lblDestino: TcxLabel;
    DataSource: TDataSource;
    pgTablasComparar: TdxWizardControlPage;
    btnSeleccionarTodos: TcxButton;
    btnDesmarcarTodas: TcxButton;
    ImageButtons : TcxImageList;
    pgResumen: TdxWizardControlPage;
    gridTablasCatalogosDBTableView: TcxGridDBTableView;
    gridTablasCatalogosLevel: TcxGridLevel;
    gridTablasCatalogos: TcxGrid;
    lblBitacora: TLabel;
    txtBitacora: TcxTextEdit;
    btnBitacora: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    Entidad: TcxGridDBColumn;
    luBDDestino: TZetaKeyLookup_DevEx;
    luBDFuente: TZetaKeyLookup_DevEx;
    gridTablas: TZetaCXGrid;
    gridTablasDBTableView: TcxGridDBTableView;
    Nombre: TcxGridDBColumn;
    Descripcion1: TcxGridDBColumn;
    Elegir: TcxGridDBColumn;
    gridTablasLevel: TcxGridLevel;
    DataSourceGridTablas: TDataSource;
    Descripcion2: TcxGridDBColumn;
    pgSeleccion: TdxWizardControlPage;
    btn_Sel_SeleccionarTodos: TcxButton;
    btn_Sel_DesmarcarTodas: TcxButton;
    gridTablasDiff: TZetaCXGrid;
    gridTablasDiffView: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    Label1: TLabel;
    lbl_BasePath: TLabel;
    lbl_SlavePath: TLabel;
    txtBox_SlavePath: TcxTextEdit;
    txtBox_BasePath: TcxTextEdit;
    btn_SlavePath: TcxButton;
    btn_BasePath: TcxButton;
    btn_InsDetalle: TcxButton;
    btn_ModDetalle: TcxButton;
    DataSourceSelected: TDataSource;
    cxLabel3: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
    procedure dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind; var AHandled: Boolean);
    procedure dxWizardControlPageChanged(Sender: TObject);
    procedure btnBitacora_BKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDesmarcarTodasClick(Sender: TObject);
    procedure btnDesmarcarTodasDiffClick(Sender: TObject);
    procedure cambiarSeleccion (elegir: Boolean);
    procedure btnBitacoraClick(Sender: TObject);
    procedure gridTablasDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure gridTablasCatalogosDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure btn_SlavePathClick(Sender: TObject);
    procedure btn_BasePathClick(Sender: TObject);
    procedure btn_InsDetalleClick(Sender: TObject);
    procedure btn_ModDetalleClick(Sender: TObject);

  private
    Mutex      : THandle;
    fAvances   : TList;
    oParametros: TZetaParams;
    FDestino, FFuente: string;

    procedure HabilitaBotones;
    //function CompararTablasCatalogos(out mensaje: string): Boolean;
    function HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
		function pgBasesDatosValidation: Boolean;
		function pgTablasCompararValidation: Boolean;
		function pgSeleccionValidation: Boolean;
		function pgResumenValidation: Boolean;
  end;

var
  WizCompararTablas: TWizCompararTablas;

implementation

uses
  SysUtils, Windows, DMotorPatch, MotorPatchUtils, MotorPatchThreads, DBClient, ZetaCommonLists, ShellAPI,
  ZetaRegistryServer, DSistema, DConsolida, ZetaDialogo, ZetaCommonTools,
	DConsolidacion, FDiffDetail;

{$R *.dfm}


	function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
		var
			zFileName, zParams, zDir: array[0..79] of Char;
		begin
				Result := ShellExecute( Application.MainForm.Handle,
																nil,
																StrPCopy( zFileName, FileName ),
																StrPCopy( zParams, Params ),
																StrPCopy( zDir, DefaultDir ),
																ShowCmd );
		end;


	procedure TWizCompararTablas.FormCreate(Sender: TObject);
		begin
			HelpContext := 2;
			Mutex := CreateMutex(nil, False, PChar(ClassName + '.Execute'));
			if Mutex = 0 then
				RaiseLastOSError;

			fAvances := TList.Create;
		end;

	procedure TWizCompararTablas.FormDestroy(Sender: TObject);
		begin
			FreeAndNil(oParametros);
			FreeAndNil(fAvances);
			CloseHandle(Mutex);
		end;

	procedure TWizCompararTablas.FormShow(Sender: TObject);
		begin
				with dmSistema do
				begin
							cdsSistBaseDatos.Conectar;
							cdsSistBaseDatosLookup.Conectar;
							DataSource.DataSet := cdsSistBaseDatos;
							luBDFuente.LookupDataset := cdsSistBaseDatosLookUp;
							luBDDestino.LookupDataset := cdsSistBaseDatosLookUp;
				end;

				txtBitacora.Text := Format( '%s' + K_DEFAULT_BIT, [ ExtractFilePath( Application.ExeName ) ] );


				gridTablasDBTableView.OptionsCustomize.ColumnGrouping := False;
				gridTablasDBTableView.OptionsView.GroupByBox := False;
				gridTablasDBTableView.FilterBox.Visible := fvNever;
				gridTablasDBTableView.FilterBox.CustomizeDialog := False;


				gridTablasCatalogosDBTableView.OptionsCustomize.ColumnGrouping := False;
				gridTablasCatalogosDBTableView.OptionsView.GroupByBox := False;
				gridTablasCatalogosDBTableView.FilterBox.Visible := fvNever;
				gridTablasCatalogosDBTableView.FilterBox.CustomizeDialog := False;
				
				gridTablasDiffView.OptionsCustomize.ColumnGrouping := False;
				gridTablasDiffView.OptionsView.GroupByBox := False;
				gridTablasDiffView.FilterBox.Visible := fvNever;
				gridTablasDiffView.FilterBox.CustomizeDialog := False;
		end;

	procedure TWizCompararTablas.gridTablasCatalogosDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
		var
			AIndex: Integer;
		begin
			inherited;
			AIndex := AValueList.FindItemByKind(fviCustom);
				if AIndex <> -1 then
					AValueList.Delete(AIndex);
		end;

	procedure TWizCompararTablas.gridTablasDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
		var
			AIndex: Integer;
		begin
			inherited;
			AIndex := AValueList.FindItemByKind(fviCustom);
				if AIndex <> -1 then
					AValueList.Delete(AIndex);
		end;


	procedure TWizCompararTablas.HabilitaBotones;
		begin
			with dxWizardControl.Buttons do
			begin
					Next.Enabled :=true;
					Back.Enabled :=true;
			end;
		end;


	{* Generar un archivo de bitácora. @author Ricardo Carrillo Morales 2014-04-10}
	procedure TWizCompararTablas.btnBitacoraClick(Sender: TObject);
		var sFileName: String;
		begin
					sFileName := StrDef (ExtractFileName(txtBitacora.Text), K_DEFAULT_BIT);
					cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
					if cxOpenDialog.Execute then
						txtBitacora.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
		end;

	procedure TWizCompararTablas.btnBitacora_BKClick(Sender: TObject);
		begin
			with oParametros do
			try		
				ShellExecute(0, nil, 'notepad', PChar(txtBitacora.Text), nil, SW_SHOWNORMAL);
			finally
			end;
		end;

	procedure TWizCompararTablas.btnDesmarcarTodasClick(Sender: TObject);
		begin
				cambiarSeleccion(FALSE);
		end;

	procedure TWizCompararTablas.btnSeleccionarTodosClick(Sender: TObject);
		begin
				cambiarSeleccion(TRUE);
		end;

 	procedure TWizCompararTablas.btnDesmarcarTodasDiffClick(Sender: TObject);
		begin
			cambiarSeleccion(FALSE);
      DConsolidacion.DiffListToFalse;
		end;

	procedure TWizCompararTablas.btn_BasePathClick(Sender: TObject);
		var
      saveDialog:TSaveDialog;
    begin
			saveDialog:=TSaveDialog.Create(self);
			saveDialog.InitialDir:=TPath.GetDocumentsPath;
			saveDialog.Filter:='SQL file|*.sql|Text file|*.txt';
			saveDialog.DefaultExt:='sql';
			if saveDialog.Execute then 
				txtBox_BasePath.Text:=saveDialog.FileName;
			saveDialog.Free;
    end;

  procedure TWizCompararTablas.btn_InsDetalleClick(Sender: TObject);
		var
			detailForm:TDiffDetail;
      index:Integer;
    begin
      index:=DataSourceGridTablas.DataSet.FieldByName('Index').AsInteger;
			detailForm:=TDiffDetail.Create(nil, index, true);
			detailForm.ShowModal;
    end;

  procedure TWizCompararTablas.btn_ModDetalleClick(Sender: TObject);
		var
			detailForm:TDiffDetail;      index:Integer;
    begin
      index:=DataSourceGridTablas.DataSet.FieldByName('Index').AsInteger;
			detailForm:=TDiffDetail.Create(nil, index, false);
			detailForm.ShowModal;
    end;

	procedure TWizCompararTablas.btn_SlavePathClick(Sender: TObject);
		var		
      saveDialog:TSaveDialog;
    begin
			saveDialog:=TSaveDialog.Create(self);			
			saveDialog.InitialDir:=TPath.GetDocumentsPath;
			saveDialog.Filter:='SQL file|*.sql|Text file|*.txt';
			saveDialog.DefaultExt:='sql';
			if saveDialog.Execute then 
				txtBox_SlavePath.Text:=saveDialog.FileName;
			saveDialog.Free;
    end;

procedure TWizCompararTablas.cambiarSeleccion (elegir: Boolean);
		var sTablaActual: String;
		begin
				with DataSourceGridTablas.DataSet do
				begin
						sTablaActual := FieldByName('Nombre').AsString;
						gridTablasDBTableView.OptionsView.ScrollBars := ssNone;
						First;
						while not Eof do
						begin
								Edit;
								FieldByName ('Elegir').AsBoolean := elegir;
								Post;
								Next;
						end;
						First;
						gridTablasDBTableView.OptionsView.ScrollBars := ssBoth;
						Locate('Nombre', sTablaActual, []);
				end;
		end;

procedure TWizCompararTablas.dxWizardControlButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;	var AHandled: Boolean);
		begin
			case AKind of
				wcbkNext:
					begin
						if (dxWizardControl.ActivePage = pgBasesDatos) then
							AHandled:=pgBasesDatosValidation
						
						else if (dxWizardControl.ActivePage = pgTablasComparar) then
							AHandled:=pgTablasCompararValidation
						
						else if (dxWizardControl.ActivePage = pgSeleccion) then
							AHandled:=pgSeleccionValidation												
						
						else if (dxWizardControl.ActivePage = pgResumen) then
							AHandled:=pgResumenValidation;

					end;
				wcbkFinish:
					ModalResult := mrOk;

			end;
			HabilitaBotones();  
		end;

	procedure TWizCompararTablas.dxWizardControlPageChanged(Sender: TObject);
		var
			oCursor: TCursor;
		begin 
			if (dxWizardControl.ActivePage = pgFinProceso) then
			begin

					Application.ProcessMessages;
					oCursor := Screen.Cursor;
					try
							dxWizardControl.Enabled := FALSE;
							Screen.Cursor := crHourglass;
					finally
							dxWizardControl.Enabled := TRUE;
							Screen.Cursor := oCursor;

					end;
			end;	
		end;

	procedure TWizCompararTablas.dxWizardControlPageChanging(Sender: TObject; ANewPage: TdxWizardControlCustomPage; var AAllow: Boolean);
		begin
			if ANewPage = pgResumen then
				dxWizardControl.Buttons.Next.Caption := 'Aplicar'
			else
				dxWizardControl.Buttons.Next.Caption := 'Siguiente';
		end;

	function TWizCompararTablas.HayTablaSeleccionada (DataSet: TDataSet; iColumna: Integer): Boolean;
		begin
			result:=false;
			DataSet.First;
			while not DataSet.Eof do begin
				if DataSet.Fields[iColumna].AsBoolean then begin
					result:=true;
					Break;
				end;
				DataSet.Next;
			end;
		end;

	function TWizCompararTablas.pgBasesDatosValidation: Boolean;
		var
			oCursor: TCursor;
		begin
			result:=true;
			if luBDFuente.Llave = VACIO then begin
					ZetaDialogo.ZWarning( Caption, 'El campo Fuente no puede quedar vacío.', 0, mbOK );
			end
			else if luBDDestino.Llave = VACIO then begin
					ZetaDialogo.ZWarning( Caption, 'El campo Destino no puede quedar vacío.', 0, mbOK );
			end
			else if luBDFuente.Llave = luBDDestino.Llave then begin
					ZetaDialogo.ZWarning( Caption, 'La base de datos Destino no puede ser igual a la base de datos Fuente.', 0, mbOK );
			end
			else begin
				oCursor := Screen.Cursor;
				try
						Screen.Cursor := crHourglass;
						dxWizardControl.Enabled := FALSE;
      with dmSistema.cdsSistBaseDatosLookUp do
      begin
           if Locate( 'DB_CODIGO', luBDDestino.Llave, [] ) then
              FDestino := Copy( FieldByName( 'DB_DATOS' ).AsString, ansiPos( '.', FieldByName( 'DB_DATOS' ).AsString ) + 1, Length( FieldByName( 'DB_DATOS' ).AsString ) );
           if Locate( 'DB_CODIGO', luBDFuente.Llave, [] ) then
              FFuente := Copy( FieldByName( 'DB_DATOS' ).AsString, ansiPos( '.', FieldByName( 'DB_DATOS' ).AsString ) + 1, Length( FieldByName( 'DB_DATOS' ).AsString ) );
      end;
						DataSourceGridTablas.DataSet := DConsolidacion.GetTables(FDestino, FFuente);
						gridTablasDBTableView.DataController.DataSource := DataSourceGridTablas;
				finally
						Screen.Cursor := oCursor;
						dxWizardControl.Enabled := TRUE;
						result:=false;
				end;
			end;
		end;

	function TWizCompararTablas.pgTablasCompararValidation: Boolean;
		var
			oCursor: TCursor;
		begin
			if not HayTablaSeleccionada (DataSourceGridTablas.DataSet, 2) then begin
				ZetaDialogo.ZWarning( Caption, 'Debe seleccionar al menos una tabla.', 0, mbOK );
				result:=true
			end							
			else begin
				oCursor := Screen.Cursor;
				Screen.Cursor := crHourglass;
    with dmSistema.cdsSistBaseDatosLookUp do
    begin
         if Locate( 'DB_CODIGO', luBDDestino.Llave, [] ) then
            FDestino := Copy( FieldByName( 'DB_DATOS' ).AsString, ansiPos( '.', FieldByName( 'DB_DATOS' ).AsString ) + 1, Length( FieldByName( 'DB_DATOS' ).AsString ) );
         if Locate( 'DB_CODIGO', luBDFuente.Llave, [] ) then
            FFuente := Copy( FieldByName( 'DB_DATOS' ).AsString, ansiPos( '.', FieldByName( 'DB_DATOS' ).AsString ) + 1, Length( FieldByName( 'DB_DATOS' ).AsString ) );
    end;
				DataSourceGridTablas.DataSet := DConsolidacion.GetDifferences(DataSourceGridTablas.DataSet, FDestino, FFuente);
				gridTablasDiffView.DataController.DataSource := DataSourceGridTablas;
				Screen.Cursor := oCursor;
				dxWizardControl.Enabled := TRUE;



				result:=false;
			end;
		end;

	function TWizCompararTablas.pgSeleccionValidation: Boolean;
		var
			oCursor: TCursor;	
		begin
			if not DConsolidacion.DiffsWereSelected(DataSourceGridTablas.DataSet) then begin
				ZetaDialogo.ZWarning( Caption, 'Debe seleccionar al menos una tabla.', 0, mbOK );
				result:=true
			end							
			else begin
				oCursor := Screen.Cursor;
				Screen.Cursor := crHourglass;
				DataSourceSelected.DataSet := DConsolidacion.GetSelectedDiffs(DataSourceGridTablas.DataSet);
				gridTablasCatalogosDBTableView.DataController.DataSource := DataSourceSelected;

        txtBox_BasePath.Text:= ExtractFilePath( Application.ExeName ) + 'DestinoQRY.sql';
        txtBox_SlavePath.Text:=ExtractFilePath( Application.ExeName ) + 'FuenteQRY.sql';

				Screen.Cursor := oCursor;
				result:=false;
			end;
		end;
		
	function TWizCompararTablas.pgResumenValidation: Boolean;
		var
			IsBaseEmpty, IsSlaveEmpty, IsBitacoraEmpty:Boolean;
			oCursor: TCursor;	
		begin 
			IsBaseEmpty:= StrVacio (ExtractFileName (txtBox_BasePath.Text));
			IsSlaveEmpty:= StrVacio (ExtractFileName (txtBox_SlavePath.Text));
			IsBitacoraEmpty:= StrVacio (ExtractFileName (txtBitacora.Text));						
			 
			if IsBaseEmpty OR IsSlaveEmpty OR IsBitacoraEmpty then begin								
				ZetaDialogo.ZWarning( Caption, 'Proporcionar las rutas necesarias para finalizar el proceso de Consolidación', 0, mbOK );
				result:=true;
			end
			else begin
				oCursor := Screen.Cursor;
				Screen.Cursor := crHourglass;
				DConsolidacion.GenerateQuery(txtBox_BasePath.Text, txtBox_SlavePath.Text);
				result:=false;
				Screen.Cursor := oCursor;
			end;
		end;


end.
