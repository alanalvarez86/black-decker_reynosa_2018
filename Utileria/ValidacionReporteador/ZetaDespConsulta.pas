unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

{$define QUINCENALES}
type
 eFormaConsulta = ( efcNinguna,
                     efcEmpIdentifica,
                     efcEmpContratacion,
                     efcEmpArea,
                     efcEmpPercepcion,
                     efcEmpOtros,
                     efcEmpAdicionales,
                     efcEmpPersonal,
                     efcEmpFoto,
                     efcEmpDocumento, // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
                     efcEmpParientes,
                     efcEmpExperiencia,
                     efcEmpAntesCur,
                     efcEmpAntesPto,
                     efcHisKardex,
                     efcHisVacacion,
                     efcHisIncapaci,
                     efcHisPermisos,
                     efcHisCursos,
                     efcHisCursosProg,
                     efcHisPagosIMSS,
                     efcHisAhorros,
                     efcHisPrestamos,
                     efcHisAcumulados,
                     efcHisNominas,
                     efcCafDiarias,
                     efcCafPeriodo,
                     efcCafInvita,
                     efcRegEmpleado,
                     efcProcEmpleado,
                     efcAsisTarjeta,
                     efcNomDatosAsist,
                     efcAsisCalendario,
                     efcAutorizaciones,
                     efcRegAsistencia,
                     efcProcAsistencia,
                     efcNomTotales,
                     efcNomDiasHoras,
                     efcNomMontos,
                     efcNomDatosClasifi,
                     efcNomExcepciones,
                     efcNomExcepMontos,
                     efcNomExcepGlobales,
                     efcRegNomina,
                     efcProcNomina,
                     efcAnualNomina,
                     efcIMSSDatosTot,
                     efcIMSSDatosMensual,
                     efcIMSSDatosBimestral,
                     efcIMSSHisMensual,
                     efcIMSSHisBimestral,
                     efcRegIMSS,
                     efcProcIMSS,
                     efcReportes,
                     efcQueryGral,
                     efcSistBitacora,
                     efcCatPuestos,
                     efcCatClasifi,
                     efcCatOtrasPer,
                     efcCatTurnos,
                     efcCatHorarios,
                     efcCatPrestaciones,
                     efcCatContratos,
                     efcCatConceptos,
                     efcNomParams,
                     efcCatPeriodos,
                     efcCatFolios,
                     efcCatRSocial,
                     efcCatRPatron,
                     efcCatFestGrales,
                     efcCatFestTurno,
                     efcCatCondiciones,
                     efcCatEventos,
                     efcTOTipoCursos,
                     efcCatCursos,
                     efcCatMaestros,
                     efcCatProvCap,
                     efcCatMatrizCurso,
                     efcCatMatrizPuesto,
                     efcCatCalendario,
                     efcSistGlobales,
                     efcDiccion,
                     efcCatReglas,
                     efcCatInvitadores,
                     efcTOEstadoCivil,
                     efcTOHabitacion,
                     efcTOViveCon,
                     efcTOEstudios,
                     efcTOTransporte,
                     efcTOEstado,
                     efcTONivel1,
                     efcTONivel2,
                     efcTONivel3,
                     efcTONivel4,
                     efcTONivel5,
                     efcTONivel6,
                     efcTONivel7,
                     efcTONivel8,
                     efcTONivel9,
                     efcTOExtra1,
                     efcTOExtra2,
                     efcTOExtra3,
                     efcTOExtra4,
                     efcTOMovKardex,
                     efcTOMotivoBaja,
                     efcTOIncidencias,
                     efcTOMotAuto,
                     efcTOTAhorro,
                     efcTOTPresta,
                     efcMonedas,
                     efcSistSalMin,
                     efcSistLeyIMSS,
                     efcSistNumerica,
                     efcSistRiesgo,
                     efcTipoCambio,
                     efcSistEmpresas,
                     efcSistGrupos,
                     efcSistUsuarios,
                     efcSistPoll,
                     efcImpresoras,
                     efcRegSist,
                     efcProcSist,
                     efcAsisPreNomina,
                     efcSistProcesos,
                     efcSistNivel0,
                     efcCatTools,
                     efcTOTallas,
                     efcTOMotTool,
                     efcHisTools,
                     efcConteo,
                     efcTTOClasifiCurso,
                     efcOrganigrama,
                     efcCatSesiones,
                     efcCatAccReglas,
                     efcAsisClasifiTemp,
                     efcTTOColonia,
                     efcTOExtra5,
                     efcTOExtra6,
                     efcTOExtra7,
                     efcTOExtra8,
                     efcTOExtra9,
                     efcTOExtra10,
                     efcTOExtra11,
                     efcTOExtra12,
                     efcTOExtra13,
                     efcTOExtra14,
                     efcCatAulas,
                     efcReservas,
                     efcPrerequisitosCurso,
                     efcCertificaciones,
                     efcKarCertificaciones,
                     efcPolizas,
	                efcTiposPoliza,
{$ifdef QUINCENALES}
                     efcCatTPeriodos,
{$endif}
                     efcPlanVacacion,
                     efcCatPerfPuestos,
                     efcCatSeccPerf,
                     efcCatCamposPerf,
{$IFNDEF DOS_CAPAS}
                     efcCatValuaciones,
                     efcCatValPlantillas,
                     efcCatValFactores,
                     efcCatValSubfactores,
{$ENDIF}
                     efcSimulaMontos,
                     efcBitacoraSistema,
                     efcSistRoles,
                     efcEmpUsuario,
                     efcCatMatrizCertsPuesto,
                     efcCatMatrizPuestosCert,
                     efcKarCertificProgramadas,
                     efcNomFonacotTotales,
                     efcNomFonacotEmpleado,
                     efcHisCreInfonavit,
                     efcIMSSConciliaInfonavit
                     {$ifdef ACS}
                     ,efcTONivel10
                     ,efcTONivel11
                     ,efcTONivel12
                     {$endif}
                     ,efcReglasPrestamos
                     ,efcCasDiarias
                     ,efcSistDispositivos
                     ,efcSimulaMontoGlobal
                     ,efcMotChecaManual
                     ,efcTOcupaNac
                     ,efcTAreaTemCur
                     ,efcTOMunicipios
                     ,efcEstablecimientos
{$IFNDEF DOS_CAPAS}
                     ,efcCosteoTransferencias
                     ,efcCosteoMotTransfer
                     ,efcCatCosteoGrupos
                     ,efcCatCosteoCriterios
                     ,efcCatCosteoCriteriosPorConcepto
                     ,efcBitacoraBio // SYNERGY
                     ,efcSistListaGrupos // SYNERGY
                     ,efcSistTermPorGrupos // SYNERGY
{$ENDIF}
                     ,efcTiposPension
                     ,efcHisPensiones
                     ,efcCatSegurosGastosMedicos
                     ,efcHisSegurosGastosMedicos
                     ,efcTabTCompetencias
                     ,efcTabTPerfiles
                     ,efcCatCompetencias
                     ,efcCatPerfiles
                     ,efcCatMatPerfilComps
                     ,efcHisPlanCapacitacion
                     ,efcHisEvaluaCompeten
                     ,efcMtzHabilidades
                     ,efcCatNacCompetencias
{$IFNDEF DOS_CAPAS}
                     ,efcSistMensajes // SYNERGY
                     ,efcSistMensajesPorTerminal // SYNERGY
                     ,efcSistTerminales // SYNERGY
{$ENDIF}
                     ,efcCatTabAmortizacion
                     ,efcSistBaseDatos
                     ,efcSistActualizarBDs
                     ,efcBancos
                     );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

  function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
   //(@am): CAMBIO TEMPORAL
   Result := Consulta( 0, nil );
end;

end.
