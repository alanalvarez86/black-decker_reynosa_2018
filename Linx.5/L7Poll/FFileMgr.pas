unit FFileMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     LinxIni,
     LinxDll,
     ZBaseDlgModal;

type
  TFileManager = class(TZetaDlgModal)
    ArchivosGB: TGroupBox;
    Archivos: TListBox;
    Upload: TBitBtn;
    Download: TBitBtn;
    UploadDialog: TOpenDialog;
    DownloadDialog: TSaveDialog;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure UploadClick(Sender: TObject);
    procedure DownloadClick(Sender: TObject);
  private
    { Private declarations }
    FTerminalIndex: Integer;
    FLinxServer: TL7Server;
    function GetArchivoSeleccionado( out sArchivo: String ): Boolean;
  public
    { Public declarations }
    property TerminalIndex: Integer read FTerminalIndex write FTerminalIndex;
    property LinxServer: TL7Server read FLinxServer write FLinxServer;
    procedure CargarArchivos;
  end;

var
  FileManager: TFileManager;

implementation

{$R *.DFM}

uses ZetaDialogo;

procedure TFileManager.FormShow(Sender: TObject);
var
   FTerminal: TTerminal;
begin
     inherited;
     FTerminal := LinxServer.ListaTerminales.Terminal[ TerminalIndex ];
     CargarArchivos;
     if Assigned( FTerminal ) then
     begin
          ArchivosGB.Caption := Format( ' Archivos Cargados En Terminal: %s ', [ FTerminal.Description ] );
     end
     else
     begin
          ArchivosGB.Caption := ' Archivos Cargados En Terminal Desconocida ';
     end;
     ActiveControl := Archivos;
end;

procedure TFileManager.CargarArchivos;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Archivos do
        begin
             with Items do
             begin
                  Clear;
                  BeginUpdate;
                  try
                     LinxServer.CargarDirectorio( Self.TerminalIndex, Self.Archivos.Items );
                  finally
                         EndUpdate;
                  end;
             end;
             if ( Items.Count > 0 ) then
                ItemIndex := 0;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TFileManager.GetArchivoSeleccionado( out sArchivo: String ): Boolean;
var
   i: Integer;
begin
     Result := False;
     with Archivos do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               sArchivo := Items[ i ];
               Result := True;
          end
          else
          begin
               sArchivo := '';
               ZetaDialogo.zWarning( '� Atenci�n !', 'No se ha seleccionado ning�n archivo', 0, mbOK );
          end;
     end;
end;

procedure TFileManager.OKClick(Sender: TObject);
var
   sArchivo: String;
begin
     inherited;
     if GetArchivoSeleccionado( sArchivo ) then
     begin
          if ZetaDialogo.zConfirm( '� Atenci�n !', Format( '� Desea Borrar El Archivo %s ?', [ sArchivo ] ), 0, mbNo ) then
          begin
               if LinxServer.BorrarArchivo( TerminalIndex, sArchivo ) then
               begin
                    CargarArchivos;
               end
               else
                   ZetaDialogo.zError( '� Error !', Format( 'El Archivo %s No Pudo Ser Borrado', [ sArchivo ] ), 0 );
          end;
     end;
end;

procedure TFileManager.UploadClick(Sender: TObject);
var
   sSource, sTarget: String;
begin
     inherited;
     with UploadDialog do
     begin
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               sSource := FileName;
               sTarget := ExtractFileName( sSource );
               LinxServer.SendDataFile( Self.TerminalIndex, sSource, sTarget );
               CargarArchivos;
          end;
     end;
end;

procedure TFileManager.DownloadClick(Sender: TObject);
var
   sSource, sTarget: String;
begin
     inherited;
     if GetArchivoSeleccionado( sSource ) then
     begin
          with DownloadDialog do
          begin
               InitialDir := ExtractFilePath( Application.ExeName );
               FileName := ExtractFileName( sSource );
               if Execute then
               begin
                    sTarget := FileName;
                    if LinxServer.GetDataFile( Self.TerminalIndex, sTarget, sSource ) then
                       ZetaDialogo.ZInformation( '� Atenci�n !', Format( 'El archivo %s fu� bajado hacia %s', [ sSource, sTarget ] ), 0)
                    else
                        ZetaDialogo.ZError( '� Error !', Format( 'Hubo problemas para bajar el archivo %s', [ sSource ] ), 0);
               end;
          end;
     end;
end;

end.
