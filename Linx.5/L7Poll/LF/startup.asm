* CF52xx Startup Routine for Crossware C Compiler
* This startup routine is provided as an example only.  It must be modified to suit your
* target system.

* The following block will be maintained by the Embedded Development Studio
* DO NOT EDIT
*{{EDS_STARTUP(CF52XX.ASM)
__NoRom equ 0
__LowestRomLocation equ 0FFE00000H
__HighestRomLocation equ 0FFFFFFFFH
__LowestRamLocation equ 0H
__HighestRamLocation equ 07FFFFFH
__InterruptVectorOffset equ 0FFE00000H
*}}EDS_STARTUP
* END OF DO NOT EDIT
* This is the end of the block that will be maintained by the Embedded Development Studio

	xref	_main,__initiostreams,__init_cvars,__HP
	xdef	__cstart,_exit

******************************************************************************************

* These sections are required by the Crossware C Compiler:

	section	__STACK,4,data,high	Linker places this at highest available ram location
	ds.w	1			1 word minimum to create section

	section __HEAP,4,data,postpone	Linker places this after all ram sections except __STACK
	ds.w	1			1 word minimum to create section


******************************************************************************************

* Modify the code below to suit your particular target system and program requirements.


**** Start of startup code for separate code and data ****

**** This code will be included when __NoRom is zero. Ie when the
**** "Code and data in ram" option in the linker settings has not been checked

* We are assuming here that the code will go in rom.
* The address of the __STACK section is placed at address zero 
* and so becomes the supervisor stack as well as the working stack
* for the C compiler.  This means that the chip will remain in supervisor
* at all times.
* Execution immediately jumps to __cstart.
* The compiler generates a reference to '__cstart' to force the linker to include at least
* one module with this label. Therefore you should use '__cstart' in this file otherwise
* you will get an error message from the linker or a second startup module (extracted
* from the library) will be included in the final program.
	
	ifeq	__NoRom

	org     __LowestRomLocation
	xdef __START
__START		* give the linker the start address
	dc.l    __STACK		Initialise supervisor stack for C compiler
	dc.l    __cstart	Jump to __cstart on power up
        
	SECTION __CODE
       
__cstart
*	Add your own initialisation code here
	xref _MBAR,_SRAM,_SRAMSIZE,_init_chip_selects,_init_dram
	* chip selects must be initialised before __init_cvars called because
	* the memory of the C variables need to be accessible

	* initialize MBAR
	move.l	#_MBAR+1,D0
	movec	D0,MBAR
	* initialize SRAM
	move.l	#_SRAM+$21,D0
	movec	D0,RAMBAR

	* we need a temporary valid stack to call init_chip_selects, so we will use sram
	move.l	#_SRAM+_SRAMSIZE,A7
	jsr	_init_chip_selects
	jsr	_init_dram
	movea.l	#__STACK,a7	* the stack segment should now be valid
	move.l	#__InterruptVectorOffset,D0
	movec	d0,VBR

	move.l	#__HEAP,D0	Initialise heap pointer. (You must do this if you are using the heap.)
	move.l	D0,__HP
	jsr	__init_cvars	__init_cvars initialises all C variables and must always called
	jsr	__initiostreams	initialise stdout, stdin, etc
	move #$2000,SR		enable all interrupts
	jsr	_main
_exit

**** End of startup code for separate code and data ****

	elsec

**** Start of startup code for combined code and data ****

* This code will be included when __NoRom is non-zero. Ie when the
* "Code and data in ram" option in the linker settings has been checked.
* We are assuming here that your target board already has a monitor or debugger
* running on it and that you are loading your program and data into data space
* on that target board.
* We are also assuming that the stack being used by this monitor/debugger is 
* insufficient for the C compiler.  We therefore set up a different stack.

	org	__LowestRamLocation	Lowest user ram location
	even				Just in case an odd address has be put into the linker settings
	xdef __START
__START		* give the linker the start address
__cstart
*	Add your own initialisation code here


	movea.l	#__STACK,a7	Set user stack pointer.  The C compiler might need it's own stack.  This will set it up.
	move.l	#__HEAP,D0	Initialise heap pointer. (You must do this if you are using the heap (malloc(), realloc(), etc.).
	move.l	D0,__HP
	jsr	__init_cvars	__init_cvars initialises all C variables and must always called
	jsr	__initiostreams	initialise stdout, stdin, etc
	move.l	#__InterruptVectorOffset,D0
	movec	d0,VBR
	move #$2000,SR		enable all interrupts
	jsr	_main
_exit

* Where execution goes from here depends on your target system. 
* A simple rts might be sufficient to return control to your target systems monitor/debugger.
* The code below is for a target board that requires a trap 15 (ie a ColdFire evaluation board
* running dBug).

	move.l	#0,d0
	trap	#15		Call monitor reenter command
	
	endc
**** End of startup code for combined code and data ****
	
