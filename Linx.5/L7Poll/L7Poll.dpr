program L7Poll;

uses
  Forms,
  SysUtils,
  ZetaClientTools,
  FLinxBase in '..\LinxBase\FLinxBase.pas' {LinxBase},
  FAcercaDe in '..\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FLinx7Poll in 'FLinx7Poll.pas' {Linx7Poll},
  DPoll in '..\LinxBase\DPoll.pas' {dmPoll: TDataModule};

{$R *.RES}
{$R WindowsXP.res}

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'L7Poll';
  Application.HelpFile := '';
  Application.CreateForm(TLinx7Poll, Linx7Poll);
  case ParamCount of
          1:
          begin
               Linx7Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ) );
               FreeAndNil( Linx7Poll );
          end;
          2:
          begin
               Linx7Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ) );
               FreeAndNil( Linx7Poll );
          end;
     end;
     Application.Run;
end.
