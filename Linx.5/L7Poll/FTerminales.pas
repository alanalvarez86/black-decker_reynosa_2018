unit FTerminales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls, ShellAPI,
     LinxIni,
     LinxDLL,
     ZBaseDlgModal,
     ZetaNumero;

type
  TCapturarTerminales = class(TZetaDlgModal)
    PanelControles: TPanel;
    PanelLista: TPanel;
    TerminalList: TListBox;
    PanelABC: TPanel;
    Agregar: TBitBtn;
    Borrar: TBitBtn;
    Modificar: TBitBtn;
    OpenDialog: TOpenDialog;
    PanelDescripcion: TPanel;
    DescriptionLBL: TLabel;
    IdentificadorLBL: TLabel;
    Identificador: TEdit;
    Description: TEdit;
    gbConexion: TGroupBox;
    DireccionLBL: TLabel;
    DireccionIP: TMaskEdit;
    UsuarioLBL: TLabel;
    Usuario: TEdit;
    PasswordLBL: TLabel;
    Password: TEdit;
    gbArchivos: TGroupBox;
    ProgramaLBL: TLabel;
    Programa: TEdit;
    BuscarPrograma: TSpeedButton;
    BuscarAlarma: TSpeedButton;
    BuscarEmpleados: TSpeedButton;
    Empleados: TEdit;
    Alarma: TEdit;
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    Configurar: TBitBtn;
    ConfirmacionLBL: TLabel;
    Confirmacion: TEdit;
    LetreroLBL: TLabel;
    Letrero: TEdit;
    Puerto: TZetaNumero;
    PuertoLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AgregarClick(Sender: TObject);
    procedure BorrarClick(Sender: TObject);
    procedure ModificarClick(Sender: TObject);
    procedure UseTerminalListClick(Sender: TObject);
    procedure TerminalListClick(Sender: TObject);
    procedure HayCambios(Sender: TObject);
    procedure HayCambiosDescripcion(Sender: TObject);
    procedure HayCambiosPassword(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure IdentificadorEnter(Sender: TObject);
    procedure BuscarEmpleadosClick(Sender: TObject);
    procedure BuscarAlarmaClick(Sender: TObject);
    procedure BuscarProgramaClick(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
    procedure DireccionIPKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FRefreshing: Boolean;
    FLinxServer: TL7Server;
    function BuscaArchivo(const sFileName, sFilter, sTitle: String): String;
    function VerificaDatos( Lista: TStrings ): Boolean;
    procedure CargarTerminal( Terminal: TTerminal );
    procedure LimpiarLista;
    procedure SeleccionaPrimerTerminal;
    procedure ShowTerminalInfo;
  public
    { Public declarations }
    property LinxServer: TL7Server read FLinxServer write FLinxServer;
  end;

var
  CapturarTerminales: TCapturarTerminales;

implementation

uses ZetaCommonTools,
     {$ifdef FALSE}
     FConfigurar,
     {$endif}
     ZetaDialogo;

{$R *.DFM}

procedure TCapturarTerminales.FormCreate(Sender: TObject);
begin
     inherited;
     FRefreshing := False;
end;

procedure TCapturarTerminales.FormShow(Sender: TObject);
const
     K_CON_DETECTAR = 86;
     K_SIN_DETECTAR = 4;
begin
     inherited;
     with LinxIni.IniValues do
     begin
          ListaTerminales.Cargar( TerminalList.Items );
     end;
     UseTerminalListClick( Self );
     SeleccionaPrimerTerminal;
     ShowTerminalInfo;
end;

procedure TCapturarTerminales.FormDestroy(Sender: TObject);
begin
     inherited;
     LimpiarLista;
end;

procedure TCapturarTerminales.LimpiarLista;
begin
     with TerminalList do
     begin
          LinxIni.IniValues.ListaTerminales.Limpiar( Items );
     end;
end;

procedure TCapturarTerminales.SeleccionaPrimerTerminal;
begin
     with TerminalList do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
end;

procedure TCapturarTerminales.ShowTerminalInfo;
var
   i: Integer;
begin
     with TerminalList do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               FRefreshing := True;
               with TTerminal( Items.Objects[ i ] ) do
               begin
                    Self.DireccionIP.Text := Address;
                    Self.Puerto.Valor := Puerto;
                    Self.Usuario.Text := UserName;
                    Self.Password.Text := Password;
                    Self.Confirmacion.Text := Password;
                    Self.Description.Text := Description;
                    Self.Identificador.Text := Identificador;
                    Self.Letrero.Text := Letrero;
                    Self.Programa.Text := Programa;
                    Self.Alarma.Text := Alarma;
                    Self.Empleados.Text := Empleados;
               end;
               FRefreshing := False;
          end;
     end;
end;

function TCapturarTerminales.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

procedure TCapturarTerminales.CargarTerminal( Terminal: TTerminal );
begin
     with Terminal do
     begin
          Address := Self.DireccionIP.Text;
          Puerto := Self.Puerto.ValorEntero;
          UserName := Self.Usuario.Text;
          Password := Self.Password.Text;
          PasswordConfirm := Self.Confirmacion.Text;
          Description := Self.Description.Text;
          Identificador := Self.Identificador.Text;
          Letrero := Self.Letrero.Text;
          Programa := Self.Programa.Text;
          Alarma := Self.Alarma.Text;
          Empleados := Self.Empleados.Text;
     end;
end;

procedure TCapturarTerminales.HayCambios(Sender: TObject);
begin
     if not FRefreshing then
     begin
          with TerminalList do
          begin
               if ( ItemIndex >= 0 ) then
               begin
                    CargarTerminal( TTerminal( Items.Objects[ ItemIndex ] ) );
               end;
          end;
     end;
end;

procedure TCapturarTerminales.HayCambiosDescripcion(Sender: TObject);
begin
     HayCambios( Sender );
     if not FRefreshing then
     begin
          with TerminalList do
          begin
               if ( ItemIndex >= 0 ) then
               begin
                    Items.Strings[ ItemIndex ] := Description.Text;
               end;
          end;
     end;
end;

procedure TCapturarTerminales.HayCambiosPassword(Sender: TObject);
begin
     if not FRefreshing then
     begin
          Self.Confirmacion.Text := '';
     end;
     HayCambios( Sender );
end;

procedure TCapturarTerminales.UseTerminalListClick(Sender: TObject);
begin
     inherited;
     //SetTerminalControls;
end;

procedure TCapturarTerminales.TerminalListClick(Sender: TObject);
begin
     inherited;
     ShowTerminalInfo;
end;

procedure TCapturarTerminales.AgregarClick(Sender: TObject);
begin
     inherited;
     if VerificaDatos( TerminalList.Items ) then
     begin
          with TerminalList do
          begin
               Items.AddObject( 'TerminalNueva', TTerminal.Create( 'TerminalNueva' ) );
               ItemIndex := ( Items.Count - 1 );
          end;
          Modificar.Click;
     end;
end;

procedure TCapturarTerminales.BorrarClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with TerminalList do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               with Items do
               begin
                    TTerminal( Objects[ i ] ).Free;
                    Delete( i );
                    if ( i >= Count ) then
                       i := ( i - 1 );
               end;
               ItemIndex := i;
               ShowTerminalInfo;
          end;
     end;
end;

procedure TCapturarTerminales.ModificarClick(Sender: TObject);
begin
     inherited;
     ShowTerminalInfo;
     with DireccionIP do
     begin
          if CanFocus and Visible then
             SetFocus;
     end;
end;

function TCapturarTerminales.VerificaDatos( Lista: TStrings ): Boolean;
var
   i, j: Integer;
   FOrigen, FDestino: TTerminal;
begin
     Result := True;
     for i := 0 to ( Lista.Count - 1 ) do
     begin
          FOrigen := TTerminal( Lista.Objects[ i ] );
          with FOrigen do
          begin
               if ZetaCommonTools.StrLleno( Password ) and ( Password <> PasswordConfirm ) then
               begin
                    ZetaDialogo.ZError( 'Error al Grabar', 'La Terminal ' + FOrigen.IPAddress + ' tiene contrase�a err�nea', 0 );
                    Result := False;
               end;
          end;
          if Result then
          begin
               for j := ( i + 1 ) to ( Lista.Count - 1 ) do
               begin
                    FDestino := TTerminal( Lista.Objects[ j ] );//.GetInfo;
                    if ( FOrigen.IPAddress = FDestino.IPAddress ) then
                    begin
                         ZetaDialogo.ZError( 'Error al Grabar', 'La Terminal ' + FDestino.Description + ' tiene los mismos valores de direcci�n [ ' + FOrigen.IPAddress + ' ] que la Terminal ' + FOrigen.Description, 0 );
                         Result := False;
                         Break;
                    end
                    else
                    begin
                         if ( FOrigen.Identificador = FDestino.Identificador ) and ZetaCommonTools.StrLleno( FDestino.Identificador ) then
                         begin
                              ZetaDialogo.ZError( 'Error al Grabar', 'La Terminal ' + FDestino.IPAddress + ' tiene el mismo Identificador [ ' + FDestino.Identificador + ' ] que la Terminal ' + FOrigen.IPAddress, 0 );
                              Result := False;
                              Break;
                         end;
                    end;
               end;
          end;
          if not Result then
             Break;
     end;
end;

procedure TCapturarTerminales.IdentificadorEnter(Sender: TObject);
begin
     inherited;
     Identificador.SelectAll;
end;

procedure TCapturarTerminales.BuscarProgramaClick(Sender: TObject);
begin
     inherited;
     Programa.Text := BuscaArchivo( Programa.Text, 'Configuraci�n Linx7|*.lxe|Todos los Archivos|*.*', 'Archivo de Configuraci�n para Aplicaciones de Linx7' );
end;

procedure TCapturarTerminales.BuscarEmpleadosClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleado|*.val|Todos los Archivos|*.*', 'Archivo de Empleados para Aplicaciones de Linx7' );
end;

procedure TCapturarTerminales.BuscarAlarmaClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas|*.val|Todos los Archivos|*.*', 'Archivo de Alarma para Aplicaciones de Linx7' );
end;

procedure TCapturarTerminales.OKClick(Sender: TObject);
begin
     inherited;
     if VerificaDatos( TerminalList.Items ) then
        LinxIni.IniValues.ListaTerminales.Descargar( TerminalList.Items )
     else
     begin
          //ZetaDialogo.ZError( 'Error al Grabar', 'El Identificador de los relojes debe de ser �nico', 0 );
          ModalResult := mrNone;
     end;
end;

procedure TCapturarTerminales.ConfigurarClick(Sender: TObject);
var
   Terminal: TTerminal;
   {$ifndef FALSE}
   pURL: array[ 0..255 ] of Char;
   {$endif}
begin
     inherited;
     if ( TerminalList.ItemIndex >= 0 ) then
     begin
          Terminal := TTerminal.Create( '' );
          try
             Self.CargarTerminal( Terminal );
             {$ifdef FALSE}
             FConfigurar.ConfigurarTerminal( Terminal );
             {$else}
             ShellAPI.ShellExecute( Application.Handle, 'open', StrPCopy( pURL, Format( 'http://%s', [ Terminal.IPAddress ] ) ), nil, nil, SW_SHOWNORMAL );
             {$endif}
          finally
                 FreeAndNil( Terminal );
          end;
     end;
end;

procedure TCapturarTerminales.DireccionIPKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key <> Chr( VK_RETURN ) ) then { ENTER as TAB }
     begin
          case Key of
               #48..#57:{ Nada que hacer };
               '.':{ Nada que hacer };
               Chr( VK_BACK ):{ Nada que hacer };
          else
              begin
                   MessageBeep( MB_ICONEXCLAMATION );
                   Key := Chr( 0 );
              end;
          end;
     end;
     inherited;
end;

end.
