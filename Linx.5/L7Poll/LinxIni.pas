unit LinxIni;

interface

uses Classes, IniFiles, SysUtils, Forms, Dialogs,
     LinxIniBase;

const
     INI_L7POLL = 'L7Poll';
     K_LEN_SEGMENTO = 3;
     K_SEGMENTO_1 = 1;
     K_SEGMENTO_2 = 4;
     K_SEGMENTO_3 = 7;
     K_SEGMENTO_4 = 10;
     L5_Broadcast_True = 1;
     L5_Broadcast_False = 0;

type
  TTerminal = class( TObject )
  private
    { Private declarations }
    FAddress: TIPAddress;
    FDescription: String;
    FIdentificador: TIdentificaID;
    FPrograma: String;
    FEmpleados: String;
    FAlarma: String;
    FLetrero: String;
    FConectada: Boolean;
    FUserName: String;
    FPassword: String;
    FPasswordConfirm: String;
    FPuerto: Integer;
    FUltimoPoleo: TDateTime;
    function GetIPAddress: TIPAddress;
    function GetIdentificador: TIdentificaID;
    procedure SetConectada(const Value: Boolean);
    procedure SetIdentificador(const Value: TIdentificaID);
  public
    { Public declarations }
    constructor Create( const sData: String );
    property Address: TIPAddress read FAddress write FAddress;
    property IPAddress: TIPAddress read GetIPAddress;
    property Description: String read FDescription write FDescription;
    property Identificador: TIdentificaID read GetIdentificador write SetIdentificador;
    property Programa: String read FPrograma write FPrograma;
    property Alarma: String read FAlarma write FAlarma;
    property Empleados: String read FEmpleados write FEmpleados;
    property Conectada: Boolean read FConectada write SetConectada;
    property UserName: String read FUserName write FUserName;
    property Password: String read FPassword write FPassword;
    property PasswordConfirm: String read FPasswordConfirm write FPasswordConfirm;
    property Puerto: Integer read FPuerto write FPuerto;
    property Letrero: String read FLetrero write FLetrero;
    property UltimoPoleo: TDateTime read FUltimoPoleo write FUltimoPoleo;
    function GetInfo: String;
  end;
  TTerminals = class( TStringList )
  private
    { Private declarations }
    function GetTerminal( Index: Integer ): TTerminal;
    function GetTerminalFileName: String;
    procedure Init;
    procedure Write;
    function GetActiveCount: Integer;
  protected
    { protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Terminal[ Index: Integer ]: TTerminal read GetTerminal;
    property ActiveCount: Integer read GetActiveCount;
    function GetFormatedIPAddress( const sAddress: String ): String;
    function GetTerminalInfo( const sAddress: String ): TTerminal;
    function GetTerminalID( const sAddress: String ): String;
    procedure Cargar( Lista: TStrings );
    procedure Clear; override;
    procedure Descargar( Lista: TStrings );
    procedure Limpiar( Lista: TStrings );
  end;
  TL7IniValues = class( TIniValues )
  private
    { Private declarations }
    FTerminalList: TTerminals;
    function GetTerminales: LongInt;
    procedure PutTerminales( const Valor: LongInt );
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ListaTerminales: TTerminals read FTerminalList;
    property Terminales: LongInt read GetTerminales write PutTerminales;
  end;

var
   IniValues: TL7IniValues;

procedure IniValuesInit;
procedure IniValuesClear;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaServerTools,
     FLinxTools;

const
     K_LEN_DESCRIPTION = 20;
     K_LEN_ADDRESS = 12;
     K_LEN_UNIT_NUMBER = 2;
     K_LEN_IDENTIFICADOR = 4;
     K_CERO = '0';
     K_ARCHIVO_TOKEN = '|';
     K_ID_TOKEN = '=';
     TERMINAL_SEPARATOR = '|';
     L7Poll_Terminales = 'Terminales';

procedure IniValuesInit;
begin
     if not Assigned( IniValues ) then
     begin
          IniValues := TL7IniValues.Create;
     end;
end;

procedure IniValuesClear;
begin
     if Assigned( IniValues ) then
     begin
          FreeAndNil( IniValues );
     end;
end;

{ ***** TTerminal ******** }

constructor TTerminal.Create( const sData: String );
const
     DEFAULT_USERNAME = 'AnOnYmOuS';
     DEFAULT_PASSWORD = 'AnOnYmOuS';
     DEFAULT_PUERTO = 52440;
var
   iPos: Integer;
   sArchivos: String;
begin
     FConectada := False;
     FUserName := DEFAULT_USERNAME;
     FPassword := DEFAULT_PASSWORD;
     FPuerto := DEFAULT_PUERTO;
     iPos := Pos( K_ID_TOKEN, sData );
     if ( iPos > 0 ) then
     begin
          FDescription := Copy( sData, 1, ( iPos - 1 ) );
          FAddress := Copy( sData, ( iPos + 1 ), K_LEN_ADDRESS );
          FIdentificador := Copy( sData, ( iPos + 14 ), K_LEN_IDENTIFICADOR );
          sArchivos := Copy( sData, ( iPos + 19 ), ( Length( sData ) - ( iPos + 18 ) ) );
          FUserName := StrToken( sArchivos, K_ARCHIVO_TOKEN );
          FPassword := ZetaServerTools.Decrypt( StrToken( sArchivos, K_ARCHIVO_TOKEN ) );
          FPrograma := StrToken( sArchivos, K_ARCHIVO_TOKEN );
          FAlarma   := StrToken( sArchivos, K_ARCHIVO_TOKEN );
          FEmpleados := StrToken( sArchivos, K_ARCHIVO_TOKEN );
          FLetrero := StrToken( sArchivos, K_ARCHIVO_TOKEN );
          FPuerto := StrToIntDef( sArchivos, DEFAULT_PUERTO );
     end
     else
     begin
          FDescription := PadR( Copy( sData, 1, K_LEN_DESCRIPTION ), K_LEN_DESCRIPTION );
     end;
     FPasswordConfirm := FPassword;
     FUltimoPoleo := NullDateTime;
end;

function TTerminal.GetIdentificador: TIdentificaID;
begin
     Result := PadR( Trim( FIdentificador ), K_LEN_IDENTIFICADOR );
end;

procedure TTerminal.SetIdentificador(const Value: TIdentificaID);
const
     K_BLANCO = ' ';
var
   i: Integer;
begin
     FIdentificador := Trim( Value );
     for i := Length( Value ) to K_LEN_IDENTIFICADOR do
     begin
          FIdentificador := FIdentificador + K_BLANCO;
     end;
end;

function TTerminal.GetInfo: String;
const
     K_ESPACIO = ' ';
begin
     Result := PadR( Copy( FDescription, 1, K_LEN_DESCRIPTION ), K_LEN_DESCRIPTION ) +
               K_ID_TOKEN +
               StrPadChR( Trim( Copy( FAddress, 1, K_LEN_ADDRESS ) ), K_CERO, K_LEN_ADDRESS ) +
               K_ESPACIO +
               Identificador +
               K_ESPACIO +
               FUserName +
               K_ARCHIVO_TOKEN +
               ZetaServerTools.Encrypt( FPassword ) +
               K_ARCHIVO_TOKEN +
               FPrograma +
               K_ARCHIVO_TOKEN +
               FAlarma +
               K_ARCHIVO_TOKEN +
               FEmpleados +
               K_ARCHIVO_TOKEN +
               FLetrero +
               K_ARCHIVO_TOKEN +
               IntToStr( FPuerto );
end;

function TTerminal.GetIPAddress: TIPAddress;
begin
     Result := Format( '%d.%d.%d.%d', [
               strToIntDef( Copy( FAddress, K_SEGMENTO_1, K_LEN_SEGMENTO ), 0 ),
               strToIntDef( Copy( FAddress, K_SEGMENTO_2, K_LEN_SEGMENTO ), 0 ),
               strToIntDef( Copy( FAddress, K_SEGMENTO_3, K_LEN_SEGMENTO ), 0 ),
               strToIntDef( Copy( FAddress, K_SEGMENTO_4, K_LEN_SEGMENTO ), 0 ) ] );
end;

procedure TTerminal.SetConectada(const Value: Boolean);
begin
     if ( FConectada <> Value ) then
     begin
          FConectada := Value;
          FUltimoPoleo := NullDateTime;
     end;
end;

{ ***** TL5Terminals ******** }

constructor TTerminals.Create;
begin
     inherited Create;
end;

destructor TTerminals.Destroy;
begin
     Clear;
     inherited Destroy;
end;

function TTerminals.GetActiveCount: Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := ( Count - 1 ) downto 0 do
     begin
          if Terminal[ i ].Conectada then
             Inc( Result );
     end;
end;

function TTerminals.GetTerminal( Index: Integer ): TTerminal;
begin
     Result := TTerminal( Objects[ Index ] );
end;

procedure TTerminals.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Terminal[ i ].Free;
     end;
     inherited Clear;
end;

function TTerminals.GetTerminalFileName: String;
const
     TERMINAL_FILE_NAME = 'L7UNITS.INI';
begin
     Result := FLinxTools.SetFileNameDefaultPath( TERMINAL_FILE_NAME );
end;

procedure TTerminals.Init;
var
   UnitFile: TextFile;
   sData: String;
begin
     if FileExists( GetTerminalFileName ) then
     begin
          AssignFile( UnitFile, GetTerminalFileName );
          Reset( UnitFile );
          while not Eof( UnitFile ) do
          begin
               Readln( UnitFile, sData );
               AddObject( sData, TTerminal.Create( sData ) );
          end;
          CloseFile( UnitFile );
     end;
end;

function TTerminals.GetFormatedIPAddress( const sAddress: String ): String;
{$ifdef PRUEBAS}
var
   sTemp: String;
{$endif}
begin
     {$ifdef PRUEBAS}
     sTemp := sAddress + '.';
     Result := Format( '%3.3s.%3.3s.%3.3s.%3.3s', [ GetIPSubAddress( sTemp ),
                                                    GetIPSubAddress( sTemp ),
                                                    GetIPSubAddress( sTemp ),
                                                    GetIPSubAddress( sTemp ) ] );
     {$else}
     Result := sAddress;
     {$endif}
end;

function TTerminals.GetTerminalInfo( const sAddress: String ): TTerminal;
var
   i: Integer;
   sDireccion: String;
begin
     sDireccion := GetFormatedIPAddress( sAddress );
     Result := nil;
     for i := 0 to ( Self.Count - 1 ) do
     begin
          if ( sDireccion = Terminal[ i ].IPAddress ) then
          begin
               Result := Terminal[ i ];
               Break;
          end;
     end;
end;

function TTerminals.GetTerminalID( const sAddress: String ): String;
var
   Terminal: TTerminal;
begin
     Terminal := GetTerminalInfo( sAddress );
     if Assigned( Terminal ) then
     begin
          Result := Terminal.Identificador;
     end
     else
     begin
          Result := StrTransform( GetFormatedIPAddress( sAddress ), '.', '' );
          Result := Copy( Result, Length( Result ) - 4, 4 );
     end;
     Result := Format( '%4.4s00', [ Result ] );
end;

procedure TTerminals.Cargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  NewTerminal := TTerminal.Create( Terminal[ i ].GetInfo );
                  AddObject( NewTerminal.Description, NewTerminal );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminals.Descargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     try
        BeginUpdate;
        Clear;
        for i := 0 to ( Lista.Count - 1 ) do
        begin
             NewTerminal := TTerminal.Create( TTerminal( Lista.Objects[ i ] ).GetInfo );
             AddObject( NewTerminal.Description, NewTerminal );
        end;
     finally
            EndUpdate;
     end;
     Write;
end;

procedure TTerminals.Limpiar( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             for i := ( Count - 1 ) downto 0 do
             begin
                  TTerminal( Objects[ i ] ).Free;
             end;
             Clear;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminals.Write;
var
   i: Integer;
   UnitFile: TextFile;
begin
     AssignFile( UnitFile, GetTerminalFileName );
     Rewrite( UnitFile );
     for i := 0 to ( Count - 1 ) do
         Writeln( UnitFile, Terminal[ i ].GetInfo );
     CloseFile( UnitFile );
end;

{ ***** TL7IniValues ******** }

constructor TL7IniValues.Create;
const
     INI_FILE_NAME = 'L7POLL.INI';
begin
     inherited Create( INI_FILE_NAME );
     FTerminalList := TTerminals.Create;
     FTerminalList.Init;
end;

destructor TL7IniValues.Destroy;
begin
     FTerminalList.Free;
     inherited Destroy;
end;

{ *** M�todos Read / Write de las propiedades *** }

function TL7IniValues.GetTerminales: LongInt;
begin
     Result := 0; { IniFile.ReadInteger( INI_L7POLL, L5Poll_Terminales, 0 ); }
end;

procedure TL7IniValues.PutTerminales( const Valor: LongInt );
begin
     IniFile.WriteInteger( INI_L7POLL, L7Poll_Terminales, Valor );
end;

end.
