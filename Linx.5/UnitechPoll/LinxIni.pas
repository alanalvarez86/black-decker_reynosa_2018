unit LinxIni;

interface

uses Classes, IniFiles, SysUtils, Forms, Dialogs,
     LinxIniBase;

const
     K_ID_LEN = 4;
type
  eTerminalType = ( ttTCPIP, ttSerial, ttModem );
  eBaudRate = ( br300, br600, br1200, br2400, br4800, br9600, br19200, br38400 );
  eStopBits = ( sbOneStopBit, sbTwoStopBits );
  eParity = ( paOdd, paEven, paNone );
  TTerminal = class( TObject )
  private
    { Private declarations }
    FIsConnected: Boolean;
    FLetter: String;
    FDescription: String;
    FIdentificador: TIdentificaID;
    FTipo: eTerminalType;
    FCommData: Pointer;
    FTCPAddress1: Word;
    FTCPAddress2: Word;
    FTCPAddress3: Word;
    FTCPAddress4: Word;
    FBaudRate: eBaudRate;
    FStopBits: eStopBits;
    FParity: eParity;
    FSerialPort: Integer;
    FTCPPort: Integer;
    FTCPTimeOut: Integer;
    FEmpleados: String;
    FAlarma: String;
    FLecturas: Integer;
    function GetIdentificador: TIdentificaID;
    function GetIsConnected: Boolean;
    function GetData: String;
    function GetSerialPort: Integer;
    function GetTimeOut: Integer;
    function GetTCPAddress: String;
    function GetTCPPort: Integer;
    function GetTCPTimeOut: Integer;
    procedure SetData(const Value: String);
    procedure SetIdentificador(const Value: TIdentificaID);
    procedure SetIsConnected(const Value: Boolean);
    procedure SetTCPTimeOut( const iValue: Integer );
  protected
    { protected declarations }
    property Data: String read GetData write SetData;
  public
    { Public declarations }
    constructor Create;
    property Description: String read FDescription write FDescription;
    property Identificador: TIdentificaID read GetIdentificador write SetIdentificador;
    property Letter: String read FLetter write FLetter;
    property Tipo: eTerminalType read FTipo write FTipo;
    property CommData: Pointer read FCommData write FCommData;
    property IsConnected: Boolean read GetIsConnected write SetIsConnected;
    property TCPAddress: String read GetTCPAddress;
    property TCPAddress1: Word read FTCPAddress1 write FTCPAddress1;
    property TCPAddress2: Word read FTCPAddress2 write FTCPAddress2;
    property TCPAddress3: Word read FTCPAddress3 write FTCPAddress3;
    property TCPAddress4: Word read FTCPAddress4 write FTCPAddress4;
    property TCPPort: Integer read GetTCPPort write FTCPPort;
    property TCPTimeOut: Integer read GetTCPTimeOut write SetTCPTimeOut;
    property TimeOut: Integer read GetTimeout;
    property SerialPort: Integer read GetSerialPort write FSerialPort;
    property BaudRate: eBaudRate read FBaudRate write FBaudRate;
    property StopBits: eStopBits read FStopBits write FStopBits;
    property Parity: eParity read FParity write FParity;
    property Alarma: String read FAlarma write FAlarma;
    property Empleados: String read FEmpleados write FEmpleados;
    property Lecturas: Integer read FLecturas write FLecturas;
    function GetInfo: String;
    function BaudRateWord: Word;
    function LetterWord: Word;
    function ParityWord: Word;
    function StopBitsWord: Word;
    function Verificar(Otra: TTerminal; var sMensaje: String): Boolean;
  end;
  TTerminalList = class( TStringList )
  private
    { Private declarations }
    function GetTerminal( Index: Integer ): TTerminal;
    function GetTerminalFileName: String;
    procedure Init;
    procedure Write;
  protected
    { protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Terminal[ Index: Integer ]: TTerminal read GetTerminal;
    procedure Cargar( Lista: TStrings );
    procedure Clear; override;
    procedure Descargar( Lista: TStrings );
    procedure Limpiar( Lista: TStrings );
  end;
  TUnitekIniValues = class( TIniValues )
  private
    { Private declarations }
    FTerminalList: TTerminalList;
    function GetConfiguracion: String;
    procedure PutConfiguracion(const Value: String);
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ListaTerminales: TTerminalList read FTerminalList;
    property Configuracion: String read GetConfiguracion write PutConfiguracion;
  end;

var
   IniValues: TUnitekIniValues;

procedure IniValuesInit;
procedure IniValuesClear;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     FLinxTools;

const
     TERMINAL_SEPARATOR = '|';
     K_MAX_TIMEOUT = 5;
     K_MIN_TIMEOUT = 1;
     Archivos_Config = 'Configuracion';

procedure IniValuesInit;
begin
     if not Assigned( IniValues ) then
     begin
          IniValues := TUnitekIniValues.Create;
     end;
end;

procedure IniValuesClear;
begin
     if Assigned( IniValues ) then
     begin
          FreeAndNil( IniValues );
     end;
end;

{ ***** TTerminal ******** }

constructor TTerminal.Create;
begin
     FIsConnected := False;
     FTipo := ttTCPIP;
     FDescription := 'Terminal Nueva';
     FIdentificador := '';
     FLetter := 'A';
     FTCPAddress1 := 0;
     FTCPAddress2 := 0;
     FTCPAddress3 := 0;
     FTCPAddress4 := 0;
     FTCPPort := 9000;
     FTCPTimeOut := K_MAX_TIMEOUT;
     FSerialPort := 1;
     FBaudRate := eBaudRate( 0 );
     FStopBits := eStopBits( 0 );
     FParity := eParity( 0 );
     FLecturas := 0;
end;

function TTerminal.GetIsConnected: Boolean;
begin
     Result := FIsConnected;
end;

procedure TTerminal.SetIsConnected(const Value: Boolean);
begin
     FIsConnected := Value;
end;

function TTerminal.GetIdentificador: TIdentificaID;
begin
     Result := Trim( FIdentificador );
end;

function TTerminal.GetSerialPort: Integer;
begin
     Result := FSerialPort
end;

function TTerminal.GetTCPPort: Integer;
begin
     Result := FTCPPort;
end;

function TTerminal.GetTimeOut: Integer;
begin
     Result := K_MAX_TIMEOUT;
end;

function TTerminal.GetTCPTimeOut: Integer;
begin
     Result := ZetaCommonTools.iMax( ZetaCommonTools.iMin( FTCPTimeOut, K_MAX_TIMEOUT ), K_MIN_TIMEOUT );
end;

function TTerminal.GetData: String;
begin
     Result := Format( '%1.1d|%-15.15s|%-4.4s|%-1.1s', [ Ord( Tipo ), Description, Identificador, Letter ] );
     case Tipo of
          ttTCPIP: Result := Format( '%s|%3.3d|%3.3d|%3.3d|%3.3d|%5.5d|%2.2d|%s|%s', [ Result, TCPAddress1, TCPAddress2, TCPAddress3, TCPAddress4, TCPPort, TCPTimeOut, Alarma, Empleados ] );
          ttSerial: Result := Format( '%s|%1.1d|%1.1d|%1.1d|%1.1d|%s|%s', [ Result, SerialPort, Ord( BaudRate ), Ord( StopBits ), Ord( Parity ), Alarma, Empleados ] );
     end;
end;

procedure TTerminal.SetIdentificador(const Value: TIdentificaID);
begin
     FIdentificador := Copy( Trim( Value ), 1, K_ID_LEN );
end;

procedure TTerminal.SetTCPTimeOut( const iValue: Integer );
begin
     FTCPTimeOut := ZetaCommonTools.iMax( ZetaCommonTools.iMin( iValue, K_MAX_TIMEOUT ), K_MIN_TIMEOUT );
end;

procedure TTerminal.SetData(const Value: String);
var
   sArchivos: String;
begin
     FTipo := eTerminalType( StrToIntDef( Copy( Value, 1, 1 ), 0 ) );
     FDescription := Copy( Value, 3, 15 );
     FIdentificador := Copy( Value, 19, 4 );
     FLetter := Copy( Value, 24, 1 );
     case Tipo of
          ttTCPIP:
          begin
               FTCPAddress1 := StrToIntDef( Copy( Value, 26, 3 ), 0 );
               FTCPAddress2 := StrToIntDef( Copy( Value, 30, 3 ), 0 );
               FTCPAddress3 := StrToIntDef( Copy( Value, 34, 3 ), 0 );
               FTCPAddress4 := StrToIntDef( Copy( Value, 38, 3 ), 0 );
               FTCPPort := StrToIntDef( Copy( Value, 42, 5 ), 0 );
               FTCPTimeOut := StrToIntDef( Copy( Value, 48, 2 ), K_MAX_TIMEOUT );
               sArchivos := Copy( Value, 51, ( Length( Value ) - 50 ) );
               FSerialPort := 0;
               FBaudRate := eBaudRate( 0 );
               FStopBits := eStopBits( 0 );
               FParity := eParity( 0 );
          end;
          ttSerial:
          begin
               FTCPAddress1 := 0;
               FTCPAddress2 := 0;
               FTCPAddress3 := 0;
               FTCPAddress4 := 0;
               FTCPPort := 0;
               FTCPTimeOut := K_MAX_TIMEOUT;
               FSerialPort := StrToIntDef( Copy( Value, 26, 1 ), 0 );
               FBaudRate := eBaudRate( StrToIntDef( Copy( Value, 28, 1 ), 0 ) );
               FStopBits := eStopBits( StrToIntDef( Copy( Value, 30, 1 ), 0 ) );
               FParity := eParity( StrToIntDef( Copy( Value, 32, 1 ), 0 ) );
               sArchivos := Copy( Value, 34, ( Length( Value ) - 33 ) );
          end;
     end;
     FAlarma := StrToken( sArchivos, '|' );
     FEmpleados := sArchivos;
end;

function TTerminal.GetTCPAddress: String;
begin
     Result := Format( '%d.%d.%d.%d', [ TCPAddress1, TCPAddress2, TCPAddress3, TCPAddress4 ] );
end;

function TTerminal.GetInfo: String;
const
     aConectada: array[ False..True ] of pChar = ( 'OFF', 'OK ' );
begin
     Result := Trim( Format( '%s: %-15.15s', [ aConectada[ IsConnected ], Description  ] ) );
end;

function TTerminal.BaudRateWord: Word;
begin
     case BaudRate of
          br300: Result := Word( '2' );
          br600: Result := Word( '3' );
          br1200: Result := Word( '4' );
          br2400: Result := Word( '5' );
          br4800: Result := Word( '6' );
          br9600: Result := Word( '7' );
          br19200: Result := Word( '8' );
          br38400: Result := Word( '9' );
     else
         Result := Word( '1' );
     end;
end;

function TTerminal.LetterWord: Word;
begin
     if ZetaCommonTools.StrLleno( Letter ) then
        Result := Word( Letter[ 1 ] )
     else
         Result := Word( 'A' );
end;

function TTerminal.ParityWord: Word;
begin
     case StopBits of
          sbOneStopBit: Result := Word( '1' );
          sbTwoStopBits: Result := Word( '2' );
     else
         Result := Word( '1' );
     end;
end;

function TTerminal.StopBitsWord: Word;
begin
     case Parity of
          paOdd: Result := Word( 'O' );
          paEven: Result := Word( 'E' );
          paNone: Result := Word( 'N' );
     else
         Result := Word( 'N' );
     end;
end;

function TTerminal.Verificar( Otra: TTerminal; var sMensaje: String ): Boolean;
begin
     if ZetaCommonTools.StrLleno( Identificador ) and ZetaCommonTools.StrLleno( Otra.Identificador ) and ( Identificador = Otra.Identificador ) then
     begin
          Result := False;
          sMensaje := Format( 'La Terminal %s Tiene El Mismo Identificador (%s) %s Que La Terminal %s', [ Description, Identificador, CR_LF, Otra.Description ] );
     end
     else
         if ( Tipo = ttTCPIP ) and ( TCPAddress = Otra.TCPAddress ) then
         begin
              Result := False;
              sMensaje := Format( 'La Terminal %s Tiene La Misma Direcci�n TCP/IP (%s) %s Que La Terminal %s', [ Description, TCPAddress, CR_LF, Otra.Description ] );
         end
         else
             Result := True;
end;

{ ***** TTerminalList ******** }

constructor TTerminalList.Create;
begin
     inherited Create;
end;

destructor TTerminalList.Destroy;
begin
     Clear;
     inherited Destroy;
end;

function TTerminalList.GetTerminal( Index: Integer ): TTerminal;
begin
     Result := TTerminal( Objects[ Index ] );
end;

procedure TTerminalList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Terminal[ i ].Free;
     end;
     inherited Clear;
end;

function TTerminalList.GetTerminalFileName: String;
const
     TERMINAL_FILE_NAME = 'UTKUNITS.INI';
begin
     Result := FLinxTools.SetFileNameDefaultPath( TERMINAL_FILE_NAME );
end;

procedure TTerminalList.Init;
var
   UnitFile: TextFile;
   sData: String;
   Terminal: TTerminal;
begin
     if FileExists( GetTerminalFileName ) then
     begin
          Clear;
          try
             BeginUpdate;
             AssignFile( UnitFile, GetTerminalFileName );
             try
                Reset( UnitFile );
                while not Eof( UnitFile ) do
                begin
                     Readln( UnitFile, sData );
                     Terminal := TTerminal.Create;
                     Terminal.Data := sData;
                     AddObject( sData, Terminal );
                end;
             finally
                    CloseFile( UnitFile );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminalList.Cargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     with Lista do
     begin
          Clear;
          try
             BeginUpdate;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  NewTerminal := TTerminal.Create;
                  NewTerminal.Data := Terminal[ i ].Data;
                  AddObject( NewTerminal.Description, NewTerminal );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminalList.Descargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     Clear;
     try
        BeginUpdate;
        for i := 0 to ( Lista.Count - 1 ) do
        begin
             NewTerminal := TTerminal.Create;
             NewTerminal.Data := TTerminal( Lista.Objects[ i ] ).Data;
             AddObject( NewTerminal.Description, NewTerminal );
        end;
     finally
            EndUpdate;
     end;
     Write;
end;

procedure TTerminalList.Limpiar( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             for i := ( Count - 1 ) downto 0 do
             begin
                  TTerminal( Objects[ i ] ).Free;
             end;
             Clear;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminalList.Write;
var
   i: Integer;
   UnitFile: TextFile;
begin
     AssignFile( UnitFile, GetTerminalFileName );
     try
        Rewrite( UnitFile );
        for i := 0 to ( Count - 1 ) do
        begin
             Writeln( UnitFile, Terminal[ i ].Data );
        end;
     finally
            CloseFile( UnitFile );
     end;
end;

{ ***** TL5IniValues ******** }

constructor TUnitekIniValues.Create;
const
     INI_FILE_NAME = 'UNITEK.INI';
begin
     inherited Create( INI_FILE_NAME );
     FTerminalList := TTerminalList.Create;
     FTerminalList.Init;
end;

destructor TUnitekIniValues.Destroy;
begin
     FTerminalList.Free;
     inherited Destroy;
end;

{ *** M�todos Read / Write de las propiedades *** }

function TUnitekIniValues.GetConfiguracion: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Archivos_Config, '' );
end;

procedure TUnitekIniValues.PutConfiguracion(const Value: String);
begin
     IniFile.WriteString( INI_ARCHIVOS, Archivos_Config, Value );
end;

end.
