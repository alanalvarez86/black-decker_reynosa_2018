{
It is assumed that you have installed
Online Comm Manager in the folder of
C:\OnlineCommManager

Otherwise, you have to

1. Copy OnlineDll.Dll to Windows or Windows\System folder,
   and remove the path specification
or

2. Copy OnlineDll.Dll to the same folder of your Exe file,
   and remove the path specification

or

3. Manual modify the path in
   External 'C:\OnlineCommManager\OnlineDll.Dll'
}

{$define DESCONECTAR}
//{$undef DESCONECTAR}
unit FOnlineDll;

interface

uses Windows, SysUtils, Forms, Classes,
     LinxIni;

Const
     K_MILISECONDS = 1000;
     K_ONLINE_DLL_FILE = 'OnlineDll.dll';
    CmdOK        = 1; { Command OK }
    CmdEOT       = 2;
    CmdEOF       = 3;
    CmdACK       = 4;
    CmdNAK       = 5;
    CmdYES       = 6;
    CmdNO        = 7;
    CmdUSE       = 8;
    CmdNONE      = 9;
    CmdErr       = 128;
    CmdErrLen    = 129;
    CmdErrTime   = 130;
    CmdErrSocket = 131;

Type
    PBARCFG =^TBARCFG;
    TBARCFG = Record  { for ESC B Command }
        code39,       { 'N' = Enable  barcode decoding of Code 39 }
                      { 'F' = Disable barcode decoding of Code 39 }
        i2of5,        { 'N' = Enable  barcode decoding of Interleaved 2 of 5 }
                      { 'F' = Disable barcode decoding of Interleaved 2 of 5 }
        codabar,      { 'N' = Enable  barcode decoding of CODABAR }
                      { 'F' = Disable barcode decoding of CODABAR }
        ean_upc,      { 'N' = Enable  barcode decoding of UPC/EAN }
                      { 'F' = Disable barcode decoding of UPC/EAN }
        code128,      { 'N' = Enable  barcode decoding of Code 128}
                      { 'F' = Disable barcode decoding of Code 128}
        ean128,       { 'N' = Enable  barcode decoding of EAN 128 }
                      { 'F' = Enable  barcode decoding of EAN 128 }
        code93: Byte; { 'N' = Enable  barcode decoding of Code 93 }
                      { 'F' = Disable barcode decoding of Code 93 }
    End;

    PBARCFG5=^TBARCFG5;
    TBARCFG5= Record  { for ESC B Command on PT500 }
        code39,       { 'N' = Enable  barcode decoding of Code 39 }
                      { 'a'..'h'                                  }
                      { 'F' = Disable barcode decoding of Code 39 }
        i2of5,        { 'N' = Enable  barcode decoding of Interleaved 2 of 5 }
                      { 'a'..'h'                                             }
                      { 'F' = Disable barcode decoding of Interleaved 2 of 5 }
        codabar,      { 'N' = Enable  barcode decoding of CODABAR }
                      { 'a'..'h'                                  }
                      { 'F' = Disable barcode decoding of CODABAR }
        ean_upc,      { 'N' = Enable  barcode decoding of UPC/EAN }
                      { 'a'..'p'                                  }
                      { 'F' = Disable barcode decoding of UPC/EAN }
        code128,      { 'N' = Enable  barcode decoding of Code 128}
                      { 'F' = Disable barcode decoding of Code 128}
        ean128,       { 'N' = Enable  barcode decoding of EAN 128 }
                      {  msi, 'a'..'c'                            }
                      { 'F' = Enable  barcode decoding of EAN 128 }
        code93,       { 'N' = Enable  barcode decoding of Code 93 }
                      { 'F' = Disable barcode decoding of Code 93 }
        code32,       { 'N' = Enable  barcode decoding of Code 32 }
                      { 'a'..'d'                                  }
                      { 'F' = Disable barcode decoding of Code 32 }
        chnpost:Byte; { 'N' = Enable  barcode decoding of China Postal Code }
                      { 'a'..'h'                                            }
                      { 'F' = Disable barcode decoding of China Postal Code }
    End;

    PBARCFG6=^TBARCFG6;
    TBARCFG6= Record  { for ESC B Command on PT600 }
        code39,       { 'E' = Enable  barcode decoding of Code 39             }
                      { 'F' = Enable  barcode decoding of Code 39, Full ASCII }
                      { 'D' = Disable barcode decoding of Code 39             }
        i2of5,        { 'E' = Enable  barcode decoding of Interleaved 2 of 5 }
                      { 'D' = Disable barcode decoding of Interleaved 2 of 5 }
        codabar,      { 'E' = Enable  barcode decoding of CODABAR }
                      { 'D' = Disable barcode decoding of CODABAR }
        ean_upc,      { 'E' = Enable  barcode decoding of UPC/EAN }
                      { 'D' = Disable barcode decoding of UPC/EAN }
        code128,      { 'E' = Enable  barcode decoding of Code 128}
                      { 'D' = Disable barcode decoding of Code 128}
        ean128,       { 'E' = Enable  barcode decoding of EAN 128 }
                      { 'D' = Enable  barcode decoding of EAN 128 }
        code93: Byte; { 'E' = Enable  barcode decoding of Code 93 }
                      { 'D' = Disable barcode decoding of Code 93 }
    End;

    PCOMCFG =^TCOMCFG;
    TCOMCFG = Record  { for ESC C Command
                       Always set 'M' to Protocol,
                       '04' to TimeOut, and 'C' to FlowCtrl. }
        BaudRate,     { '0' =   110 bps }
                      { '1' =   150     }
                      { '2' =   300     }
                      { '3' =   600     }
                      { '4' =  1200     }
                      { '5' =  2400     }
                      { '6' =  4800     }
                      { '7' =  9600     }
                      { '8' = 19200     }
                      { '9' = 38400     }
                      { 'A' = 57600     }
        StopBits,     { '1' = 1 Stop Bit  }
                      { '2' = 2 Stop Bits }
        DataBits,     { '7' = 7 Data Bits }
                      { '8' = 8 Data Bits }
        Parity,       { 'N' = None Parity }
                      { 'O' = Odd  Parity }
                      { 'E' = Even Parity }
        Protocol,     { 'M' = Multi-point protocol }
                      { 'F' = None protocol        }
        Address:      { 'A'..'Y', or '0'..'6' }
                      Byte;
        TimeOut:      { '00' = reserved }
                      Array [0..1] Of Byte;
        FlowCtrl:     { 'N' = None     }
                      { 'X' = Xon/Xoff }
                      { 'C' = CTS/RTS (effect with RS232 only)}
                      Byte;
    End;

    PCOMCFG6=^TCOMCFG6;
    TCOMCFG6= Record  { for ESC C Command on PT600 }
        BaudRate,     { '0' =   110 bps }
                      { '1' =   150     }
                      { '2' =   300     }
                      { '3' =   600     }
                      { '4' =  1200     }
                      { '5' =  2400     }
                      { '6' =  4800     }
                      { '7' =  9600     }
                      { '8' = 19200     }
                      { '9' = 38400     }
                      { 'A' = 57600     }
        StopBits,     { '1' = 1 Stop Bit  }
                      { '2' = 2 Stop Bits }
        DataBits,     { '7' = 7 Data Bits }
                      { '8' = 8 Data Bits }
        Parity,       { 'N' = None Parity }
                      { 'O' = Odd  Parity }
                      { 'E' = Even Parity }
        FlowCtrl:     { 'N' = None     }
                      { 'X' = Xon/Xoff }
                      { 'C' = CTS/RTS  }
                      Byte;
        Protocol,     { 'M' = Multi-point protocol }
                      { 'F' = None protocol        }
        Address:      { 'A'..'Y', or '0'..'6' }
                      Byte;
        TimeOut:      { '000'= reserved }
                      Array [0..2] Of Byte;
    End;

    PTRMCFG =^TTRMCFG;
    TTRMCFG = Record  { for ESC T Command
                       ID is always 8 characters.
                       So Pad space character to ID
                       if its length less than 8.}
        ID:           { Terminal ID }
                      Array [0..7] Of Byte;
        Online,       { 'R' = Remote. Transmit the data to host port. }
                      { 'L' = Local.  No transmit.                    }
        Echo,         { 'N' = Display data on LCD. }
                      { 'F' = No Display.          }
        AutoLF,       { 'N' = Append a LF with Data. }
                      { 'F' = No Append.             }
        Mode,         { 'C' = Set to Character Mode. Transmit the data }
                      {       to the host port one character each time.}
                      { 'B' = Set to Block Mode.     Transmit the data }
                      {       to the host port one block each time.    }
        Block_Def,    { 'L' = Set to line block mode.               }
                      { 'P' = Set to page block mode.               }
                      { 'B' = Set to both line and page block modes.}
        Line,         { Line = end of line character. }
        Page:         { Page = end of page character. }
                      Byte;
    End;

    PDEVCFG2 =^TDEVCFG2;
    TDEVCFG2 = Record { for ESC V Command on MR320 }
        Scanner,      { 'A' = Auto Scanner }
        Badge,        { 'B' = Slot Reader  }
        LCDBkLt,      { 'N' = On }
                      { 'F' = Off}
        Buzzer,       { 'N' = On }
        BuzzerVol,    { '0'..'7', '7' = Off }
        Interval,     { 'N' = On }
                      { 'F' = Off}
        Reserved:     { '00'  Reserved }
                      Array [0..1] Of Byte;
    End;

    PDEVCFG5 =^TDEVCFG5;
    TDEVCFG5 = Record { for ESC V Command On MR350 }
        Scanner,      { 'N' = reserved.}
        Badge,        { 'B' = Enable  the badge. }
                      { 'D' = Disable the badge. }
        LCDBkLt,      { 'N' = reserved. }
        Buzzer,       { 'N' = Set buzzer on. }
                      { 'F' = Set Buzzer off.}
        KeyLock,      { 'N' = Set keyboard unlocked.}
                      { 'K' = Set keyboard locked.  }
                      { 'P' = Set keyboard partial locked. }
        BuzzerVol:    { '0' = Low volumn. }
                      { '5' = Mid volumn. }
                      { '9' = Hi  volumn. }
                      Byte;
    End;

    PDEVCFG8 =^TDEVCFG8;
    TDEVCFG8 = Record { for ESC V Command On MR380 }
        Ext_Scanner,  { 'P' = Pen.    }
                      { 'A' = Auto    }
                      { 'M' = MSR     }
                      { 'D' = Disable }
        Int_Scanner,  { 'B' = Bar code }
                      { 'M' = MSR      }
                      { 'D' = Disable  }
        LCDBkLt,      { 'N' = On }
                      { 'F' = Off}
        Buzzer,       { 'N' = Set buzzer on. }
                      { 'F' = Set Buzzer off.}
        KeyLock,      { 'N' = Set keyboard unlocked.      }
                      { 'K' = Set keyboard locked.        }
                      { 'P' = Set keyboard partial locked.}
        BuzzerVol:    { '0' = Low volumn.}
                      { '5' = Mid volumn.}
                      { '9' = Hi  volumn.}
                      Byte;
    End;

    PDEVCFG6 =^TDEVCFG6;
    TDEVCFG6 = Record { for ESC V Command On PT600 }
        Scanner,      { 'P' = Pen     }
                      { 'A' = Auto    }
                      { 'D' = Disable }
        LCDBkLt,      { 'N' = reserved. }
        Buzzer,       { 'N' = Set buzzer on. }
                      { 'F' = Set Buzzer off.}
        KeyLock,      { 'N' = reserved. }
        BuzzerVol:    { '0' = Low volumn. }
                      { '5' = Mid volumn. }
                      { '9' = Hi  volumn. }
                      Byte;
    End;
    TMakeUnitListEvent = procedure of object;
    TGaugeEvent = procedure( const sMsg: String; iValue: Integer ) of object;
    TFeedback = procedure( const sMensaje: String ) of object;
    TFeedBackError  = procedure( const sMensaje: String; Error: Exception ) of object;
    TFeedBackEvent = procedure( const sMensaje: String; iTerminal: Integer ) of object;
    TLecturaEvent = procedure( const sData: String ) of object;
    TOnlineMgr = class( TObject )
    private
      { Private declarations }
      FUnits: Integer;
      FTerminal: Integer;
      FOnError: TFeedBackError;
      FOnMessage: TFeedback;
      FOnFeedBack: TFeedBackEvent;
      FOnGaugeSetSize: TGaugeEvent;
      FOnGaugeFinish: TGaugeEvent;
      FOnGaugeAdvance: TGaugeEvent;
      FOnMakeUnitList: TMakeUnitListEvent;
      FOnDoLectura: TLecturaEvent;
      FValidTokens: String;
      FData: String;
      FPollData: TStrings;
      FPollPtr: Integer;
      FStatus: String;
      function Ack(const iResult: Word): Boolean;
      function ChangeFileNameToLocalDAT(const sArchivo: String): String;
      function Check(const iResult: Word): Boolean;
      function CommunicationConnect( const iTerminal: Integer ): Boolean;
      function CommunicationEnd( const iTerminal: Integer ): Boolean;
      function CommunicationStart( const iTerminal: Integer ): Boolean;
      function Yes(const iResult: Word): Boolean;
      function EnviarArchivo( const iTerminal: Integer; const sSource, sTarget: String ): Boolean;
      function EnviarArchivoParametros(const iTerminal: Integer; const sLongFileName, sShortFileName: String): Boolean;
      function GetReplyMsg( const iResult: Word ): String;
      function GetRunningProgram( const iTerminal: Integer; var sPrograma: String ): Boolean;
      function GetTerminalID: Integer;
      function GetTerminal(Index: Integer): TTerminal;
      function TransformarConfig( var sArchivo: String ): Boolean;
      procedure Acknowledge( const Tipo: eTerminalType; CommData: Pointer; LetterWord: Word );
      procedure CommunicationDelay( const iTerminal: Integer );
      procedure Esperar( const eTipo: eTerminalType );
      procedure ManejaError(const sMensaje: String; Error: Exception);
      procedure Mensaje(const sMensaje: String);
      procedure SetData(const Value: String);
    protected
      { Protected declarations }
      property Data: String read FData write SetData;
      procedure DoFeedBack( const iTerminal: Integer; sMsg: String );
      procedure DoGaugeSetSize( const sMsg: String; iSize: Integer );
      procedure DoGaugeAdvance( const sMsg: String; iStep: Integer );
      procedure DoGaugeFinish( const sMsg: String; iValue: Integer );
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      property TerminalID: Integer read GetTerminalID;
      property Terminal[ Index: Integer ]: TTerminal read GetTerminal;
      property OnError: TFeedBackError read FOnError write FOnError;
      property OnFeedBack: TFeedBackEvent read FOnFeedBack write FOnFeedBack;
      property OnGaugeSetSize: TGaugeEvent read FOnGaugeSetSize write FOnGaugeSetSize;
      property OnGaugeAdvance: TGaugeEvent read FOnGaugeAdvance write FOnGaugeAdvance;
      property OnGaugeFinish: TGaugeEvent read FOnGaugeFinish write FOnGaugeFinish;
      property OnMakeUnitList: TMakeUnitListEvent read FOnMakeUnitList write FOnMakeUnitList;
      property OnDoLectura: TLecturaEvent read FOnDoLectura write FOnDoLectura;
      property OnMessage: TFeedback read FOnMessage write FOnMessage;
      property ValidTokens: String read FValidTokens write FValidTokens;
      function BorrarArchivo(const iTerminal: Integer; const sArchivo: String): Boolean;
      function DatosEnviados: String;
      function Ejecutar( const iTerminal: Integer; const sArchivo: String ): Boolean;
      function EnviarArchivoAlarma(const iTerminal: Integer; const sFileName: String): Boolean;
      function EnviarArchivoEmpleados(const iTerminal: Integer; const sFileName: String): Boolean;
      function GetInfo(const iUnit: Integer): String;
      function GetTerminalVersion(const iTerminal: Integer): String;
      function Init: Boolean;
      function IsUnitOnLine( const iTerminal: Integer ): Boolean;
      function Read: Boolean;
      function RegistroLeido: String;
      function RegistroNormal: Boolean;
      function TransformarAlarma( var sArchivo: String ): Boolean;
      function TransformarEmpleado( var sArchivo: String ): Boolean;
      function Units: Integer;
      function SetChecadaInfo: Boolean;
      procedure CargaDirectorio( const iTerminal: Integer; Archivos: TStrings );
      procedure MakeNodeList;
      procedure RebootUnit( const iTerminal: Integer );
      procedure ResetUnit(const iTerminal: Integer);
      procedure SendAnyFile( const iTerminal: Integer; sFileName: String );
      procedure SendApplicationFile( const iTerminal: Integer; sFileName: String );
      procedure SendConfigFile( const iTerminal: Integer; sFileName: String );
      procedure SendEmployeeListFile( const iTerminal: Integer; sFileName: String );
      procedure SendParameterFile( const iTerminal: Integer; sFileName: String );
      procedure SetDateTime(const iTerminal: Integer; const dValue: TDateTime);
      procedure SetDateTimeAll( const dValue: TDateTime );
      procedure ShowVersion(const iTerminal: Integer);
      procedure Stop;
    end;

Function                 Online_Create
:                        Pointer
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_Alloc
(Var HGlobal: LongInt):  Pointer
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_Init
(CommData: Pointer):     Word
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_Destroy
(CommData: Pointer);
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_Done
(CommData: Pointer); 
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_Release
(Var HGlobal: LongInt);
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_Connect
(CommData: Pointer):     Word
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_Disconnect
(CommData: Pointer);
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_Delay
(CommData: Pointer; 
 Delay: LongInt):        Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_SetupEx
(CommData: Pointer;
 Port,
 BaudRate,
 StopBits,
 Parity: Word):          Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_Setup
(CommData: Pointer;
 Port,
 BaudRate,
 StopBits,
 Parity: Word):          Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_TcpIpEx
(CommData: Pointer; 
 Host,
 Port,
 Time: PChar):           Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_TcpIp  
(CommData: Pointer; 
 Host, 
 Port,
 Time: PChar):           Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ModemInit
(CommData: Pointer; 
 Delay: LongInt; 
 hList: HWND; 
 Msg:   PChar; 
 nMax:  LongInt; 
 Init:  PChar):          Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ModemDialEx
(CommData: Pointer;
 Delay: LongInt;
 hList: HWND; 
 Msg:   PChar; 
 nMax:  LongInt;
 Dial:  PChar;  
 Phone: PChar;
 Retry: Word):           Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ModemDial
(CommData: Pointer;
 Delay: LongInt; 
 hList: HWND; 
 Msg:   PChar; 
 nMax:  LongInt;
 Dial:  PChar;  
 Phone: PChar):          Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ModemHangUp
(CommData: Pointer;
 Delay:  LongInt; 
 hList:  HWND; 
 Msg:    PChar; 
 nMax:   LongInt;
 HangUp: PChar):         Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ModemWaitForCall
(CommData: Pointer;
 Delay: LongInt; 
 hList: HWND; 
 Msg:   PChar; 
 nMax:  LongInt):        Word
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_ModemATCmd
(CommData: Pointer;
 ATCmd:    PChar)
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ModemEcho
(CommData: Pointer; 
 Msg:  PChar; 
 nMax: LongInt):         Word;
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_GetVersion
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_ACK
(CommData: Pointer;
 tAdd: Word);
StdCall; External K_ONLINE_DLL_FILE;

Procedure                Online_NAK
(CommData: Pointer; 
 tAdd: Word);
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_A
(CommData: Pointer; 
 tAdd: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_H
(CommData: Pointer; 
 tAdd: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_0
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_POLL
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_BELL
(CommData: Pointer;
 tAdd: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_9
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_G
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_I
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_R
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_5
(CommData: Pointer; 
 tAdd: Word;
 nAdd: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_K
(CommData: Pointer; 
 tAdd: Word;
 Data: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_M
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar):           Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_N
(CommData: Pointer; 
 tAdd: Word;
 Data: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_O
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_P
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_B
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_C
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_T
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar;
 bLen, ISet: Word):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_V
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_FindFirst
(CommData: Pointer; 
 tAdd: Word;
 Info: PChar;
 Msg:  PChar;
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_FindNext
(CommData: Pointer; 
 tAdd: Word;
 Info: PChar;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_D
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_E
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar;
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_J
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar;
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_L
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_U
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_Y
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar; 
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_Z
(CommData: Pointer; 
 tAdd: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ESC_X
(CommData: Pointer; 
 tAdd: Word;
 Msg:  PChar; 
 bLen: Word):            Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_DBG
(CommData: Pointer;
 tAdd: Word;
 Msg:  PChar;
 Var   bLen: Byte):      Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_DnLoad
(CommData:  Pointer;
 tAdd:      Word;
 sName:     PChar;
 lName:     PChar;
 TimeOut:   LongInt;
 Retry:     Word):       Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_UpLoad
(CommData: Pointer;
 tAdd:      Word;
 lName:     PChar;
 sName:     PChar;
 TimeOut:   LongInt;
 Retry:     Word):       Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_StLoad
(CommData:  Pointer;
 Stop:      Word;
 Var sSize: LongInt;
 Var lSize: LongInt):    Word
StdCall; External K_ONLINE_DLL_FILE;

Function                 Online_ResetMR053
(Host,
 Port,
 Time: PChar):           Word
StdCall; External K_ONLINE_DLL_FILE;

implementation

uses ZetaAsciiFile,
     ZetaCommonClasses,
     ZetaCommonTools;
//     FUnitekPoll;

const
     K_DATE_FORMAT = 'yyyymmddhhnnss';
type
    aPaquete = array [ 0..255 ] Of Byte;

{ ******** TOnlineMgr ******** }

constructor TOnlineMgr.Create;
begin
     FValidTokens := '';
     FPollData := TStringList.Create;
     FPollPtr := 0;
end;

destructor TOnlineMgr.Destroy;
begin
     FreeAndNil( FPollData );
     Stop;
     inherited Destroy;
end;

procedure TOnlineMgr.ManejaError( const sMensaje: String; Error: Exception );
begin
     if Assigned( FOnError ) then
        FOnError( sMensaje, Error );
end;

procedure TOnlineMgr.Mensaje(const sMensaje: String);
begin
     if Assigned( FOnMessage ) then
        FOnMessage( sMensaje );
end;

function TOnlineMgr.GetReplyMsg( const iResult: Word ): String;
begin
     case iResult of
          CmdOK: Result := 'OK';
          CmdEOT: Result := 'End of Terminal';
          CmdEOF: Result := 'End of File';
          CmdACK: Result := 'Acknowledge';
          CmdNAK: Result := 'Not acknowledge';
          CmdYES: Result := 'Yes';
          CmdNO: Result := 'No';
          CmdUSE: Result := 'File in use';
          CmdNONE: Result := 'None';
          CmdErr: Result := 'Error';
          CmdErrLen: Result := 'Error Length';
          CmdErrTime: Result := 'Error Time';
          CmdErrSocket: Result := 'Error Socket';
     else
         Result := Format( 'Unknown %d', [ iResult ] );
     end;
end;

function TOnlineMgr.Check( const iResult: Word ): Boolean;
begin
     Result := ( iResult = CmdOK );
     FStatus := GetReplyMsg( iResult );
end;

function TOnlineMgr.Ack( const iResult: Word ): Boolean;
begin
     Result := ( iResult = CmdACK );
     FStatus := GetReplyMsg( iResult );
end;

function TOnlineMgr.Yes( const iResult: Word ): Boolean;
begin
     Result := ( iResult = CmdYES );
     FStatus := GetReplyMsg( iResult );
end;

procedure TOnlineMgr.SetData(const Value: String);
{$ifdef FALSE}
const
     K_GAFETE_LENGTH = 10;
     K_GAFETE_FORMAT = '%10.10d';
var
   iPos, iValue: Integer;
{$endif}
begin
     FData := Trim( Value );
     {$ifdef FALSE}
     iPos := Pos( TOKEN_PROXIMIDAD, FData );
     if ( iPos > 0 ) then
     begin
          iValue := StrToIntDef( '$' + Trim( Copy( FData, iPos + 1, K_GAFETE_LENGTH ) ), 0 );
          FData := Copy( FData, 1, iPos ) + Format( K_GAFETE_FORMAT, [ iValue ] ) + Copy( FData, iPos + K_GAFETE_LENGTH + 1, Length( FData ) - ( iPos + K_GAFETE_LENGTH ) );
     end;
     {$endif}
end;

procedure TOnlineMgr.DoFeedBack( const iTerminal: Integer; sMsg: String );
begin
     if Assigned( FOnFeedBack ) then
        FOnFeedBack( sMsg, iTerminal );
end;

procedure TOnlineMgr.DoGaugeSetSize( const sMsg: String; iSize: Integer );
begin
     if Assigned( FOnGaugeSetSize ) then
        FOnGaugeSetSize( sMsg, iSize );
end;

procedure TOnlineMgr.DoGaugeAdvance( const sMsg: String; iStep: Integer );
begin
     if Assigned( FOnGaugeAdvance ) then
        FOnGaugeAdvance( sMsg, iStep );
end;

procedure TOnlineMgr.DoGaugeFinish( const sMsg: String; iValue: Integer );
begin
     if Assigned( FOnGaugeFinish ) then
        FOnGaugeFinish( sMsg, iValue );
end;

procedure TOnlineMgr.Acknowledge( const Tipo: eTerminalType; CommData: Pointer; LetterWord: Word );
begin
     Online_ACK( CommData, LetterWord );
     Esperar( Tipo );
end;

procedure TOnlineMgr.CommunicationDelay( const iTerminal: Integer );
begin
     {
     Try to use OnLine_Delay() before every time you send a command to the
     network. Sometimes when there is a communication problem with a terminal,
     our DLL returns the Timeout setting to the default, causing future
     communications problems. Also, use the highest timeout or delay possible.

     Basically OnLine_Delay() and the TimeOut parameters of Online_TCPIP() are
     very similar in the operation, but the first one is to define a specific
     delay for the  next instruction, and the TimeOut parameters is meant to
     define precisely a timeout on the settings of the communication channel
     (either RS232 or TCP/IP)

     A good idea on the settings of both OnLine_Delay() and the TimeOut is
     about 5 Sec. or 5000 msec.

     Regards

     Carlos Allera
     Ventas/Soporte Latinoam�rica
     Unitech America, Inc.
     Tel. +52 (33) 3126-9665
     www.latin.unitech-adc.com
     }
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               with Terminal[ iTerminal ] do
               begin
                    case Tipo of
                         ttTCPIP: Online_Delay( CommData, K_MILISECONDS * TCPTimeOut );
                         ttSerial: Online_Delay( CommData, K_MILISECONDS * TimeOut );
                         ttModem: Online_Delay( CommData, K_MILISECONDS * TimeOut );
                    end;
               end;
          end;
     end;
end;

function TOnlineMgr.CommunicationConnect( const iTerminal: Integer ): Boolean;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               CommunicationDelay( iTerminal );
               with Terminal[ iTerminal ] do
               begin
                    Result := Check( Online_Connect( CommData ) );
                    Esperar( Tipo );
               end;
          end;
     end;
end;

function TOnlineMgr.CommunicationStart( const iTerminal: Integer ): Boolean;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               CommunicationDelay( iTerminal );
               with Terminal[ iTerminal ] do
               begin
                    {$ifdef DESCONECTAR}
                    if IsConnected then
                    begin
                         Result := CommunicationConnect( iTerminal );
                    end;
                    {$endif}
               end;
          end;
     end;
end;

function TOnlineMgr.CommunicationEnd( const iTerminal: Integer ): Boolean;
begin
     {$ifdef DESCONECTAR}
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               with Terminal[ iTerminal ] do
               begin
                    if IsConnected then
                    begin
                         Online_Disconnect( CommData );
                         Esperar( Tipo );
                         Result := True;
                    end;
               end;
          end;
     end;
     {$endif}
end;

procedure TOnlineMgr.Esperar( const eTipo: eTerminalType );
const
     K_10_MILISEGUNDOS = 10;
     K_8_MILISEGUNDOS = 8;
     K_75_MILISEGUNDOS = 75;
begin
     case eTipo of
          ttTCPIP: Sleep( K_10_MILISEGUNDOS );
          ttSerial: Sleep( K_8_MILISEGUNDOS );
          ttModem: Sleep( K_75_MILISEGUNDOS );
     end;
end;

function TOnlineMgr.Init: Boolean;
const
     K_DELAY = '01';
var
   i: Integer;
   lOK: Boolean;
begin
     FUnits := 0;
     FPollData.Clear;
     {$ifdef FALSE}
     FPollData.Add( '1=000A000000@~00007B02DE031014110' );
     {$endif}
     FPollPtr := 0;
     Result := True;
     try
        with IniValues.ListaTerminales do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  with Terminal[ i ] do
                  begin
                       CommData := Online_Create;
                       case Tipo of
                            ttTCPIP:
                            begin
                                 lOk := Check( Online_TcpIp( CommData, pChar( TCPAddress ), pChar( Format( '%d', [ TCPPort ] ) ), pChar( Format( '%d', [ TCPTimeOut ] ) ) ) );
                            end;
                            ttSerial:
                            begin
                                 lOk := Check( Online_SetupEx( CommData, SerialPort, BaudRateWord, StopBitsWord, ParityWord ) );
                            end;
                       else
                           lOk := False;
                       end;
                       if lOk then
                       begin
                            CommunicationDelay( i );
                            IsConnected := CommunicationConnect( i );
                            CommunicationEnd( i );
                       end;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ManejaError( 'Error Al Iniciar Recolecci�n', Error );
                Result := False;
           end;
     end;
end;

procedure TOnlineMgr.Stop;
var
   i: Integer;
begin
     try
        with IniValues.ListaTerminales do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  with Terminal[ i ] do
                  begin
                       if IsConnected then
                       begin
                            IsConnected := False;
                            Online_Disconnect( CommData );
                            Online_Destroy( CommData );
                            CommData := nil;
                       end;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ManejaError( 'Error Al Terminar Recolecci�n', Error );
           end;
     end;
     FUnits := 0;
end;

function TOnlineMgr.Units: Integer;
begin
     Result := IniValues.ListaTerminales.Count;
end;

function TOnlineMgr.GetInfo( const iUnit: Integer ): String;
begin
     with IniValues.ListaTerminales do
     begin
          if ( iUnit > 0 ) and ( iUnit <= Count ) then
          begin
               Result := Terminal[ iUnit - 1 ].GetInfo;
          end;
     end;
end;

procedure TOnlineMgr.MakeNodeList;
var
   iTerminals: Integer;
begin
     iTerminals := IniValues.ListaTerminales.Count;
     if ( FUnits <> iTerminals ) then
     begin
          FUnits := iTerminals;
          if Assigned( FOnMakeUnitList ) then
             FOnMakeUnitList;
     end;
end;

function TOnlineMgr.IsUnitOnLine( const iTerminal: Integer ): Boolean;
const
     K_LETRA_A = 64;
     K_TOPE = 10;
type
    aMensaje = array[ 0..K_TOPE + 1 ] of Byte;
var
   pOriginal, pRegresa: aMensaje;
   i, iLen: Byte;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    {$ifndef DESCONECTAR}
                    IsConnected := Check( Online_Connect( CommData ) );
                    {$endif}
                    if IsConnected then
                    begin
                         Result := Check( Online_BELL( CommData, LetterWord ) );
                         if Result then
                         begin
                              pOriginal[ 0 ] := K_TOPE;
                              pRegresa[ 0 ] := K_TOPE;
                              for i := 1 to K_TOPE do
                              begin
                                   pOriginal[ i ] := K_LETRA_A + i;
                                   pRegresa[ i ] := K_LETRA_A + i;
                              end;
                              pOriginal[ K_TOPE + 1 ] := 0;
                              pRegresa[ K_TOPE + 1 ] := 0;
                              if Check( Online_ESC_9(CommData, LetterWord, @pRegresa[ 1 ], iLen ) ) then
                              begin
                                   for i := 0 to ( K_TOPE + 1 ) do
                                   begin
                                        if ( pOriginal[ i ] <> pRegresa[ i ] ) then
                                        begin
                                             Result := False;
                                             Break;
                                        end;
                                   end;
                              end
                              else
                                  Result := False;
                         end;
                    end;
               end;
               {$ifdef DESCONECTAR}
               CommunicationEnd( iTerminal - 1 );
               {$endif}
          end;
     end;
end;

procedure TOnlineMgr.ResetUnit( const iTerminal: Integer );
var
   sMensaje: String;
begin
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         if Ack( Online_ESC_H( CommData, LetterWord ) ) then
                         begin
                              sMensaje := 'Ha Sido Reseteada';
                              {
                              Indicar que hay una terminal menos para que
                              se refresque la lista de terminales en MakeNodeList
                              }
                              {$ifdef DESCONECTAR}
                              IsConnected := CommunicationConnect( iTerminal - 1 );
                              {$else}
                              IsConnected := Check( Online_Connect( CommData ) );
                              {$endif}
                              Dec( FUnits );
                         end
                         else
                             sMensaje := Format( 'No Pudo Ser Reseteada ( %s )', [ FStatus ] );
                         DoFeedBack( iTerminal, sMensaje );
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

procedure TOnlineMgr.RebootUnit( const iTerminal: Integer );
var
   sMensaje: String;
begin
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         if Ack( Online_ESC_A( CommData, LetterWord ) ) then
                            sMensaje := 'Ha Sido Rebooteada'
                         else
                             sMensaje := Format( 'No Pudo Ser Rebooteada ( %s )', [ FStatus ] );
                         DoFeedBack( iTerminal, sMensaje );
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

procedure TOnlineMgr.SetDateTimeAll( const dValue: TDateTime );
var
   i: Integer;
begin
     with IniValues.ListaTerminales do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               CommunicationStart( i );
               with Terminal[ i ] do
               begin
                    if IsConnected then
                    begin
                         if not Yes( Online_ESC_M( CommData, LetterWord, pChar( FormatDateTime( K_DATE_FORMAT, dValue ) ) ) ) then
                         begin
                              Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Ser Sincronizada ( %s )', [ i, Self.GetInfo( i ), FStatus ] ) );
                         end;
                    end;
               end;
               CommunicationEnd( i );
          end;
     end;
end;

procedure TOnlineMgr.SetDateTime( const iTerminal: Integer; const dValue: TDateTime );
begin
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         if not Yes( Online_ESC_M( CommData, LetterWord, pChar( FormatDateTime( K_DATE_FORMAT, dValue ) ) ) ) then
                         begin
                              Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Ser Sincronizada ( %s )', [ iTerminal, Self.GetInfo( iTerminal ), FStatus ] ) );
                         end;
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.GetRunningProgram( const iTerminal: Integer; var sPrograma: String ): Boolean;
const
     K_LEN = 128;
var
   pMsg: pChar;
   pData: aPaquete;
   iLen: Byte;
   iResult: Word;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         iLen := K_LEN;
                         iResult := Online_ESC_I( CommData, LetterWord, @pData[ 1 ], iLen );
                         if ( iResult = CmdNONE ) then
                            sPrograma := 'Ninguno'
                         else
                             if Check( iResult ) then
                             begin
                                  pData[ 0 ] := iLen;
                                  pData[ iLen + 1] := 0;
                                  pMsg := @pData[ 1 ];
                                  sPrograma := pMsg;
                                  Result := True;
                             end;
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.GetTerminalVersion(const iTerminal: Integer): String;
const
     K_LEN = 128;
     K_MEMORY_SIZE = 3;
     K_UNKNOWN = '???';
var
   pMsg: pChar;
   pData: aPaquete;
   iLen, iMemory: Byte;
   sVersion, sID, sMemory, sPrograma: String;
begin
     Result := K_UNKNOWN;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         iLen := K_LEN;
                         if Check( Online_GetVersion( CommData, LetterWord, @pData[ 1 ], iLen ) ) then
                         begin
                              pData[ 0 ] := iLen;
                              pData[ iLen + 1] := 0;
                              pMsg := @pData[ 1 ];
                              sVersion := pMsg;
                         end
                         else
                             sVersion := K_UNKNOWN;
                         iLen := K_LEN;
                         if Check( Online_ESC_R( CommData, LetterWord, @pData[ 1 ], iLen ) ) then
                         begin
                              pData[ 0 ] := iLen;
                              pData[ iLen + 1] := 0;
                              pMsg := @pData[ 1 ];
                              sID := pMsg;
                         end
                         else
                             sID := K_UNKNOWN;
                         iMemory := K_LEN;
                         if Check( Online_ESC_G( CommData, LetterWord, @pData[ 1 ], iMemory ) ) then
                         begin
                              pData[ 0 ] := iMemory;
                              pData[ iMemory + 1] := 0;
                              pMsg := @pData[ 1 ];
                              sMemory := pMsg;
                              if ( ( iMemory mod K_MEMORY_SIZE ) = 0 ) then
                              begin
                                   iMemory := iMemory div K_MEMORY_SIZE;
                                   sMemory := Format( 'Total=%s, Ejecuci�n=%s, Libre=%s', [ Copy( sMemory, 1, iMemory ), Copy( sMemory, ( iMemory + 1 ), iMemory ), Copy( sMemory, ( 2 * iMemory + 1 ), iMemory ) ] );
                              end
                              else
                                  sMemory := K_UNKNOWN;
                         end
                         else
                             sMemory := K_UNKNOWN;
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
               if not GetRunningProgram( iTerminal, sPrograma ) then
                  sPrograma := K_UNKNOWN;
               Result := Format( '%s ( ID = %s ) Ejecuta %s ( Memoria: %s )', [ sVersion, sID, sPrograma, sMemory ] );
          end;
     end;
end;

procedure TOnlineMgr.ShowVersion( const iTerminal: Integer );
begin
     DoFeedBack( iTerminal, GetTerminalVersion( iTerminal ) );
end;

function TOnlineMgr.DatosEnviados: String;
begin
     Result := FData;
end;

function TOnlineMgr.RegistroLeido: String;
begin
     Result := DatosEnviados;
end;

function TOnlineMgr.RegistroNormal: Boolean;
begin
//     Result := True;  // ER: Siempre regresa True para dejarlas en archivo
     Result := ( Pos( FValidtokens, FData ) > 0 );
end;

function TOnlineMgr.SetChecadaInfo: Boolean;
var
   sData, sTerminal: String;
begin
     Result := ( FPollData.Count > 0 );
     if Result then
     begin
          with FPollData do
          begin
               sTerminal := Names[ 0 ];
               sData := Values[ sTerminal ];
               FTerminal := StrToIntDef( sTerminal, 0 );
               if Assigned( Terminal[ FTerminal ] ) then
               begin
                    with Terminal[ FTerminal ] do
                    begin
                         if ZetaCommonTools.StrLleno( Identificador ) then
                         begin
                              sData := Format( '%-' + Format( '%0:d.%0:d', [ K_ID_LEN ] ) + 's%s', [ Identificador, Copy( sData, K_ID_LEN + 1, ( Length( sData ) - K_ID_LEN ) ) ] );
                         end;
                    end;
               end;
               Data := sData;
               Delete( 0 );
          end;
     end
end;

function TOnlineMgr.Read: Boolean;
var
   j, iReply: Integer;
   iLen: Byte;
   aData: aPaquete;
   aDataStr: String[ 255 ] absolute aData;
   sData, sOldData, sDataLectura: String;
begin
//     Result := False;
     FTerminal := -1;
     Data := '';
     Result := SetChecadaInfo;
     if ( not Result ) then
     begin
          try
             with IniValues.ListaTerminales do
             begin
                  if ( FPollPtr >= Count ) then
                     FPollPtr := 0;
                  CommunicationStart( FPollPtr );
                  with Terminal[ FPollPtr ] do
                  begin
                       if IsConnected then
                       begin
                            { Inicializar }
                            repeat
                                  for j := Low( aPaquete ) to High( aPaquete ) do
                                  begin
                                       aData[ j ] := 0;
                                  end;
                                  sOldData := '';
                                  iReply := Online_POLL( CommData, LetterWord, @aData[ 1 ], iLen );
                                  case iReply of
                                       CmdOK:
                                       begin
                                            Acknowledge( Tipo, CommData, LetterWord );
                                            aData[ 0 ] := iLen;
                                            aData[ iLen + 1 ] := 0;
                                            sData := aDataStr;
                                            if ( sData <> sOldData ) then
                                            begin
                                                 sOldData := sData;
                                                 FPollData.Add( Format( '%d=%s', [ FPollPtr + 1, sData ] ) );
                                                 if Assigned( OnDoLectura ) then
                                                 begin
                                                      sDataLectura := sData;
                                                      if ZetaCommonTools.StrLleno( Identificador ) then
                                                         sDataLectura := Format( '%-' + Format( '%0:d.%0:d', [ K_ID_LEN ] ) + 's%s', [ Identificador, Copy( sDataLectura, K_ID_LEN + 1, ( Length( sDataLectura ) - K_ID_LEN ) ) ] );
                                                      OnDoLectura( Trim( sDataLectura ) );
                                                 end;
                                            end;
                                       end;
                                       CmdEOT: Acknowledge( Tipo, CommData, LetterWord );
                                  end;
                            until ( iReply <> CmdOK );
                            Lecturas := Lecturas + FPollData.Count;
                       end;
                  end;
                  CommunicationEnd( FPollPtr );
                  Inc( FPollPtr );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Recolectar', Error );
                end;
          end;
          Result := SetChecadaInfo;  // Si en la recolecci�n hubo checadas prepara la primera
     end;
end;

function TOnlineMgr.GetTerminalID: Integer;
begin
     Result := FTerminal;
end;

function TOnlineMgr.GetTerminal(Index: Integer): TTerminal;
begin
     with IniValues.ListaTerminales do
     begin
          if ( Index > 0 ) and ( Index <= Count ) then
          begin
               Result := Terminal[ Index - 1 ];
          end
          else
              Result := nil;
     end;
end;

procedure TOnlineMgr.CargaDirectorio( const iTerminal: Integer; Archivos: TStrings );
var
   FindStructure: array [ 0..279 ] of Byte;
   iResult: Word;
   iLen: Byte;
   aName: array [ 0..15 ] of Byte;
   aNameStr: String[ 15 ] absolute aName;
begin
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         with Archivos do
                         begin
                              Clear;
                              try
                                 BeginUpdate;
                                 iResult := Online_FindFirst( CommData, LetterWord, @FindStructure, @aName[ 1 ], iLen );
                                 while Check( iResult ) and ( iLen > 0 ) do
                                 begin
                                      aName[ 0 ] := iLen;
                                      Add( aNameStr );
                                      iResult := Online_FindNext( CommData, LetterWord, @FindStructure, @aName[ 1 ], iLen );
                                 end
                              finally
                                     EndUpdate;
                              end;
                         end;
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.BorrarArchivo( const iTerminal: Integer; const sArchivo: String ): Boolean;
var
   iLen: Byte;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         iLen := Length( sArchivo );
                         Result := Yes( Online_ESC_E( CommData, LetterWord, @sArchivo[ 1 ], iLen ) );
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.Ejecutar( const iTerminal: Integer; const sArchivo: String ): Boolean;
const
     K_EXE_SUFIX = '.EXE';
var
   iLen: Byte;
   sPrograma: String;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         sPrograma := sArchivo;
                         iLen := Pos( K_EXE_SUFIX, sPrograma );
                         if ( iLen > 0 ) then
                            sPrograma := Copy( sPrograma, 1, ( iLen - 1 ) );
                         iLen := Length( sPrograma );
                         Result := Yes( Online_ESC_X( CommData, LetterWord, @sPrograma[ 1 ], iLen ) );
                         if Result then
                            DoFeedBack( iTerminal, Format( 'Ejecuta %s', [ sPrograma ] ) );
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.ChangeFileNameToLocalDAT( const sArchivo: String ): String;
begin
     Result := ExtractFilePath( Application.ExeName ) + ExtractFileName( ChangeFileExt( sArchivo, '.dat' ) );
end;

function TOnlineMgr.TransformarAlarma( var sArchivo: String ): Boolean;
const
     K_COMENTARIO = '#';
     K_SEPARADOR = '|';
type
    TAlarma = record
       Hora: array[ 1..6 ] of Char;
       Modo: array[ 1..1 ] of Char;
    end;
var
   FEntrada: TAsciiServer;
   rAlarma: TAlarma;
   FSalida: file of TAlarma;
   sHora, sModo, sDatos, sSalida: String;
   i, iPos: Integer;
begin
     Result := False;
     if FileExists( sArchivo ) then
     begin
          try
             sSalida := ChangeFileNameToLocalDAT( sArchivo );
             FEntrada := TAsciiServer.Create;
             try
                AssignFile( FSalida, sSalida );
                try
                   Rewrite( FSalida );
                   with FEntrada do
                   begin
                        if Open( sArchivo ) then
                        begin
                             repeat
                                  sDatos := Trim( Data );
                                  if ZetaCommonTools.StrLleno( sDatos ) and ( sDatos[ 1 ] <> K_COMENTARIO ) then
                                  begin
                                       iPos := Pos( K_SEPARADOR, sDatos );
                                       if ( iPos > 0 ) then
                                       begin
                                            sHora := Trim( Copy( sDatos, 1, ( iPos - 1 ) ) );
                                            sModo := Trim( Copy( sDatos, ( iPos + 1 ), 1 ) );
                                            if ZetaCommonTools.StrLleno( sHora ) and ZetaCommonTools.StrLleno( sModo ) then
                                            begin
                                                 with rAlarma do
                                                 begin
                                                      for i := Low( Hora ) to High( Hora ) do
                                                      begin
                                                           if ( i > Length( sHora ) ) then
                                                              Hora[ i ] := Chr( 0 )
                                                           else
                                                               Hora[ i ] := sHora[ i ];
                                                      end;
                                                      Modo[ 1 ] := sModo[ 1 ];
                                                 end;
                                                 Write( FSalida, rAlarma );
                                            end;
                                       end;
                                  end;
                             until not Read;
                        end;
                        Close;
                   end;
                   Result := FileExists( sSalida );
                   if Result then
                      sArchivo := sSalida;
                finally
                       CloseFile( FSalida );
                end;
             finally
                    FreeAndNil( FEntrada );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Transformar Archivo De Alarmas', Error );
                end;
          end;
     end;
end;

function TOnlineMgr.TransformarConfig( var sArchivo: String ): Boolean;
const
     K_COMENTARIO = '#';
     K_SEPARADOR = '=';
     K_STR_SIZE = 18;
type
    TConfig = record
       Nombre: array[ 1..K_STR_SIZE ] of Char;
       Valor: array[ 1..K_STR_SIZE ] of Char;
    end;
var
   FEntrada: TAsciiServer;
   rConfig: TConfig;
   FSalida: file of TConfig;
   sNombre, sValor, sDatos, sSalida: String;
   i, iPos: Integer;
begin
     Result := False;
     if FileExists( sArchivo ) then
     begin
          try
             sSalida := ChangeFileNameToLocalDAT( sArchivo );
             FEntrada := TAsciiServer.Create;
             try
                AssignFile( FSalida, sSalida );
                try
                   Rewrite( FSalida );
                   with FEntrada do
                   begin
                        if Open( sArchivo ) then
                        begin
                             repeat
                                  sDatos := Trim( Data );
                                  if ZetaCommonTools.StrLleno( sDatos ) and ( sDatos[ 1 ] <> K_COMENTARIO ) then
                                  begin
                                       iPos := Pos( K_SEPARADOR, sDatos );
                                       if ( iPos > 0 ) then
                                       begin
                                            sNombre := Trim( Copy( sDatos, 1, ( iPos - 1 ) ) );
                                            sValor := Trim( Copy( sDatos, ( iPos + 1 ), ( Length( sDatos ) - iPos ) ) );
                                            if ZetaCommonTools.StrLleno( sNombre ) and ZetaCommonTools.StrLleno( sValor ) then
                                            begin
                                                 with rConfig do
                                                 begin
                                                      for i := Low( Nombre ) to High( Valor ) do
                                                      begin
                                                           if ( i > Length( sNombre ) ) then
                                                              Nombre[ i ] := Chr( 0 )
                                                           else
                                                               Nombre[ i ] := sNombre[ i ];
                                                      end;
                                                      for i := Low( Valor ) to High( Valor ) do
                                                      begin
                                                           if ( i > Length( sValor ) ) then
                                                              Valor[ i ] := Chr( 0 )
                                                           else
                                                               Valor[ i ] := sValor[ i ];
                                                      end;
                                                 end;
                                                 Write( FSalida, rConfig );
                                            end;
                                       end;
                                  end;
                             until not Read;
                        end;
                        Close;
                   end;
                   Result := FileExists( sSalida );
                   if Result then
                      sArchivo := sSalida;
                finally
                       CloseFile( FSalida );
                end;
             finally
                    FreeAndNil( FEntrada );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Transformar Archivo De Cofiguraci�n', Error );
                end;
          end;
     end;
end;

function TOnlineMgr.TransformarEmpleado( var sArchivo: String ): Boolean;
const
     K_COMENTARIO = '#';
     K_SEPARADOR = '|';
     C_BADGE_SIZE = 11; { GA: � xq tengo 12 ? }
     C_APELLIDO_SIZE = 16;
     C_NOMBRE_SIZE = 16;
type
    TEmpleado = record
       Gafete: array[ 1..C_BADGE_SIZE ] of Char;
       Nombre: array[ 1..C_NOMBRE_SIZE ] of Char;
       Apellido: array[ 1..C_APELLIDO_SIZE ] of Char;
    end;
var
   FEntrada: TAsciiServer;
   rEmpleado: TEmpleado;
   FSalida: file of TEmpleado;
   sGafete, sApellido, sNombre, sDatos, sSalida: String;
   i, iPos: Integer;
begin
     Result := False;
     if FileExists( sArchivo ) then
     begin
          try
             sSalida := ChangeFileNameToLocalDAT( sArchivo );
             FEntrada := TAsciiServer.Create;
             try
                AssignFile( FSalida, sSalida );
                try
                   Rewrite( FSalida );
                   with FEntrada do
                   begin
                        if Open( sArchivo ) then
                        begin
                             repeat
                                  sDatos := Trim( Data );
                                  if ZetaCommonTools.StrLleno( sDatos ) and ( sDatos[ 1 ] <> K_COMENTARIO ) then
                                  begin
                                       sApellido := '';
                                       sNombre := '';
                                       iPos := Pos( K_SEPARADOR, sDatos );
                                       if ( iPos > 0 ) then
                                       begin
                                            sGafete := Copy( sDatos, 1, ( iPos - 1 ) );
                                            sDatos := Trim( Copy( sDatos, ( iPos + 1 ), ( Length( sDatos ) - iPos ) ) );
                                            iPos := Pos( K_SEPARADOR, sDatos );
                                            if ( iPos > 0 ) then
                                            begin
                                                 sNombre := Copy( sDatos, 1, ( iPos - 1 ) );
                                                 sApellido := Trim( Copy( sDatos, ( iPos + 1 ), ( Length( sDatos ) - iPos ) ) );
                                            end
                                            else
                                            begin
                                                 sNombre := sDatos;
                                            end;
                                       end
                                       else
                                       begin
                                            sGafete := sDatos;
                                       end;
                                       if ZetaCommonTools.StrLleno( sGafete ) then
                                       begin
                                            with rEmpleado do
                                            begin
                                                 for i := Low( Gafete ) to High( Gafete ) do
                                                 begin
                                                      if ( i > Length( sGafete ) ){$ifdef FALSE}or ( i >= High( Gafete ) ){$endif} then
                                                         Gafete[ i ] := Chr( 0 )
                                                      else
                                                          Gafete[ i ] := sGafete[ i ];
                                                 end;
                                                 for i := Low( Nombre ) to High( Nombre ) do
                                                 begin
                                                      if ( i > Length( sNombre ) ){$ifdef FALSE} or ( i >= High( Nombre ) ){$endif} then
                                                         Nombre[ i ] := Chr( 0 )
                                                      else
                                                          Nombre[ i ] := sNombre[ i ];
                                                 end;
                                                 for i := Low( Apellido ) to High( Apellido ) do
                                                 begin
                                                      if ( i > Length( sApellido ) ){$ifdef FALSE} or ( i >= High( Apellido ) ){$endif} then
                                                         Apellido[ i ] := Chr( 0 )
                                                      else
                                                          Apellido[ i ] := sApellido[ i ];
                                                 end;
                                            end;
                                            Write( FSalida, rEmpleado );
                                       end;
                                  end;
                             until not Read;
                        end;
                        Close;
                   end;
                   Result := FileExists( sSalida );
                   if Result then
                      sArchivo := sSalida;
                finally
                       CloseFile( FSalida );
                end;
             finally
                    FreeAndNil( FEntrada );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Transformar Lista De Empleados', Error );
                end;
          end;
     end;
end;

function TOnlineMgr.EnviarArchivo( const iTerminal: Integer; const sSource, sTarget: String ): Boolean;
const
     K_MAX = 120;
var
   FSource: file of Byte;
   FData: array [ 1..K_MAX ] of Byte;
   iTextLen: Byte;
   iSize: Integer;
   lFinished: Boolean;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         if FileExists( sSource ) then
                         begin
                              AssignFile( FSource, sSource );
                              try
                                 Reset( FSource );
                                 iSize := Trunc( FileSize( FSource ) / K_MAX ) + 1;
                                 DoGaugeSetSize( 'Empezando Env�o De Archivo', iSize );
                                 if Ack( Online_ESC_L( CommData, LetterWord, @sTarget[ 1 ], Length( sTarget ) ) ) then
                                 begin
                                      Esperar( Tipo );
                                      lFinished := False;
                                      Result := True;
                                      repeat
                                            iTextLen := 0;
                                             while not Eof( FSource ) and ( iTextLen < K_MAX ) do
                                             begin
                                                  Inc( iTextLen );
                                                  System.Read( FSource, FData[ iTextLen ] );
                                             end;
                                             if ( iTextLen > 0 ) then
                                             begin
                                                  if not Ack( Online_ESC_Y( CommData, LetterWord, @FData, iTextLen ) ) then
                                                  begin
                                                       Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Continuar La Lectura Del Archivo %s ( %s )', [ iTerminal, Self.GetInfo( iTerminal ), sSource, FStatus ] ) );
                                                       lFinished := True;
                                                       Result := False;
                                                  end;
                                             end
                                             else
                                             begin
                                                  if not Ack( Online_ESC_Z( CommData, LetterWord ) ) then
                                                  begin
                                                       Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Terminar La Lectura Del Archivo %s ( %s )', [ iTerminal, Self.GetInfo( iTerminal ), sSource, FStatus ] ) );
                                                       Result := False;
                                                  end;
                                                  lFinished := True
                                             end;
                                             Acknowledge( Tipo, CommData, LetterWord );
                                             DoGaugeAdvance( 'Enviando Archivo', 1 );
                                      until lFinished;
                                 end
                                 else
                                     Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Iniciar La Lectura Del Archivo %s ( %s )', [ iTerminal, Self.GetInfo( iTerminal ), sSource, FStatus ] ) );
                                 DoGaugeFinish( 'Archivo Enviado', iSize );
                              finally
                                     CloseFile( FSource );
                              end;
                         end
                         else
                             Mensaje( Format( 'El Archivo %s No Existe ( Terminal # %d )', [ sSource, iTerminal ] ) );
                    end
                    else
                        Mensaje( Format( 'La Terminal %d ( %s ) Est� Desconectada', [ iTerminal, Self.GetInfo( iTerminal ) ] ) );
               end;
               CommunicationEnd( iTerminal - 1 );
          end
          else
              Mensaje( Format( 'La Terminal %d ( %s ) No Existe', [ iTerminal, GetInfo( iTerminal ) ] ) );
     end;
     if Result then
        DoFeedBack( iTerminal, Format( 'Archivo %s Fu� Enviado', [ sTarget ] ) );
end;

function TOnlineMgr.EnviarArchivoParametros(const iTerminal: Integer; const sLongFileName, sShortFileName: String): Boolean;
var
   lRunning: Boolean;
   sPrograma: String;
begin
     try
        lRunning := GetRunningProgram( iTerminal, sPrograma );
     except
           on Error: Exception do
           begin
                ManejaError( Format( 'Error Leyendo Programa Ejecutado Al Enviar Archivo %s', [ sLongFileName ] ), Error );
                lRunning := False;
           end;
     end;
     Sleep( 1 * K_MILISECONDS );
     try
        Result := EnviarArchivo( iTerminal, sLongFileName, sShortFileName );
     except
           on Error: Exception do
           begin
                ManejaError( Format( 'Error Enviando Archivo %s', [ sLongFileName ] ), Error );
                Result := False;
           end;
     end;
     if lRunning then
     begin
          Sleep( 1 * K_MILISECONDS );
          try
             if not Ejecutar( iTerminal, sPrograma ) then
             begin
                  Mensaje( Format( 'La Terminal %d ( %s ) No Pudo Reejecutar El Programa %s', [ iTerminal, GetInfo( iTerminal ), sPrograma ] ) );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( Format( 'Error Reejecutando Programa %s', [ sPrograma ] ), Error );
                end;
          end;
     end;
end;

function TOnlineMgr.EnviarArchivoEmpleados(const iTerminal: Integer; const sFileName: String): Boolean;
begin
     Result := EnviarArchivoParametros( iTerminal, sFileName, 'EMPLEADO.DAT' );
end;

function TOnlineMgr.EnviarArchivoAlarma(const iTerminal: Integer; const sFileName: String): Boolean;
begin
     Result := EnviarArchivoParametros( iTerminal, sFileName, 'ALARMA.DAT' );
end;

procedure TOnlineMgr.SendAnyFile(const iTerminal: Integer; sFileName: String);
begin
     EnviarArchivo( iTerminal, sFileName, ExtractFileName( sFileName ) );
end;

procedure TOnlineMgr.SendApplicationFile(const iTerminal: Integer; sFileName: String);
begin
     EnviarArchivo( iTerminal, sFileName, ExtractFileName( sFileName ) );
end;

procedure TOnlineMgr.SendConfigFile(const iTerminal: Integer; sFileName: String);
begin
     if TransformarConfig( sFileName ) then
     begin
          EnviarArchivoParametros( iTerminal, sFileName, ExtractFileName( sFileName ) );
     end;
end;

procedure TOnlineMgr.SendEmployeeListFile(const iTerminal: Integer; sFileName: String);
begin
     if TransformarEmpleado( sFileName ) then
     begin
          EnviarArchivoEmpleados( iTerminal, sFileName );
     end;
end;

procedure TOnlineMgr.SendParameterFile(const iTerminal: Integer; sFileName: String);
begin
     if TransformarAlarma( sFileName ) then
     begin
          EnviarArchivoAlarma( iTerminal, sFileName );
     end;
end;

end.

