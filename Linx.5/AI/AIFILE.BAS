' AiFile.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                       A i F i l e                     %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : File analysis module                     OS : PCDOS V3.3
'   Language : Microsoft Quick Basic V4.5               CS : IBM PC/AT
'
'   Purpose : Performs variable compression
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0     5 Jul 90    JHF     Original Creation
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiUtil.BI'
'$INCLUDE:'AiEnvPc.BI'
'$INCLUDE:'AiRead.BI'
'$INCLUDE:'AiTime.BI'
'$INCLUDE:'AiUser.BI'
'$INCLUDE:'AiExec.BI'
'$INCLUDE:'AiFile.BI'

DIM SHARED UvCnew
'
'****************************************************************************
'
'   FiIs : Set the use class of a variable
'
'   FiIs ( n, usage )
'
'****************************************************************************
SUB FiIs ( n%, t% )
  IF n% < SvUser THEN EXIT SUB
  i = t%
  IF UvNames(n%)<>"." AND i<uuVarN THEN i=uuVarN
  IF Uvu(n%) < i THEN Uvu(n%) = i ' Always use the greatest value
END SUB 'FiIs

SUB FiStr (n%) : FiIs n%, uuConstS : END SUB
SUB FiNum (n%) : FiIs n%, uuConstN : END SUB
SUB FiStrV(n%) : FiIs n%, uuVarS   : END SUB
SUB FiNumV(n%) : FiIs n%, uuVarN   : END SUB

'****************************************************************************
'
'   FiIx : Set a new index for a variable
'
'   FiIx ( n )
'
'****************************************************************************
SUB FiIx ( n% )
  IF n% < 1 OR n% > UvC THEN EXIT SUB
  i  = Uvi(n%)
  n% = i
END SUB 'FiIx
'
'****************************************************************************
'
'   FiClassOps : Classify variable usage for all opcodes
'
'****************************************************************************
SUB FiClassOps
  DIM cop AS OpCode
  
  '----- Reset the variable types -----
  FOR i = 1 TO UvC
    IF i <= VarAux THEN Uvu(i) = uuVarS ELSE Uvu(i) = uuNone
  NEXT i
  
  FOR iii = 1 TO OpC
    cop = Ops(iii)
    SELECT CASE cop.OpT
      
      CASE xxComment
        FiIs cop.Src, uuComment
        
      CASE xxLet
        dstS = FALSE : srcS = FALSE : bothS = FALSE
        SELECT CASE cop.SubT
          CASE xoMove     : bothS = TRUE
          CASE xoApp      : bothS = TRUE
          CASE xoAdd      :
          CASE xoSub      :
          CASE xoMul      :
          CASE xoDiv      :
          CASE xoNeg      :
          CASE xoNUM      : srcS = TRUE
          CASE xoLookH, xoLookL, xoLookB : FiStr cop.Lfile
          CASE xoLEN      : srcS = TRUE
          CASE xoLEFT     : FiNum cop.A
          CASE xoRIGHT    : FiNum cop.A
          CASE xoMID      : FiNum cop.A
            FiNum cop.B
          CASE xoTRIM     : bothS = TRUE
          CASE xoUCASE    : bothS = TRUE
          CASE xoASC      : bothS = TRUE
          CASE xoCHR      : bothS = TRUE
          CASE xoMOD      :
          CASE xoINT      :
        END SELECT
        IF dstS OR bothS THEN i = uuVarS ELSE i = uuVarN
        FiIs cop.Dst, i
        IF srcS OR bothS THEN i = uuConstS ELSE i = uuConstN
        FiIs cop.Src, i
        
      CASE xxTransfer
        FiStr cop.Dst
        IF cop.SubT <> xfDate AND cop.SubT <> xfTime THEN
          FiStr cop.Src
        END IF
        
      CASE xxBranch :
        
      CASE xxIf
        dstS = FALSE : srcS = FALSE : bothS = FALSE
        SELECT CASE cop.SubT
          CASE xiEQ    : bothS = TRUE
          CASE xiNE    : bothS = TRUE
          CASE xiLT    : bothS = TRUE
          CASE xiGT    : bothS = TRUE
          CASE xiSTART : bothS = TRUE
          CASE xiEND   : bothS = TRUE
          CASE xiIN    : bothS = TRUE : FiStr cop.A : FiStr cop.B
          CASE xiOUT   : bothS = TRUE : FiStr cop.A : FiStr cop.B
        END SELECT
        IF dstS OR bothS THEN i = uuConstS ELSE i = uuConstN
        FiIs cop.Dst, i
        IF srcS OR bothS THEN i = uuConstS ELSE i = uuConstN
        FiIs cop.Src, i
        
      CASE xxRead
        FiStrV cop.Dst
        i = cop.Options
        IF (i AND USETABLE)<>0 THEN FiStr cop.Ctab
        IF cop.A<>0 THEN FiStr cop.A
        IF cop.B<>0 THEN FiStr cop.B
        IF cop.LFile<>0 THEN FiStr cop.Lfile
        
      CASE xxSend : FiStr cop.Src
      CASE xxSound
      CASE xxPause
      CASE xxCls
      CASE xxGetDsr
      CASE xxGetSense: FiStr cop.Si
      CASE xxLoadFile : FiStr cop.Lfile
      CASE xxSetLine
      CASE xxUser
      CASE xxLabel : FiIs cop.Src, uuComment
      CASE xxReturn
      CASE xxOn
      CASE xxChain : FiStr cop.Lfile
      CASE xxDuration: FiStrV cop.Dst
      CASE xxTransition: FiStrV cop.Dst
    END SELECT
    IF cop.Lbl < 0 THEN FiIs -cop.Lbl, uuComment
  NEXT iii
  
  '----- Determine variable indexes for all variables -----
  j = 0
  FOR i = 1 TO UvC
    IF Uvu(i)=uuNone THEN Uvi(i)=0 ELSE j=j+1 : Uvi(i)=j
  NEXT i
  UvCnew = j
  
END SUB 'FiClassOps
'
'****************************************************************************
'
'   FiIndexOps : Reset the variable indexes for all opcodes
'
'****************************************************************************
SUB FiIndexOps
  DIM cop AS OpCode
  FOR iii = 1 TO OpC
    cop = Ops(iii)
    SELECT CASE cop.OpT
      
      CASE xxComment : FiIx cop.Src
        
      CASE xxLet
        FiIx cop.Src
        FiIx cop.Dst
        SELECT CASE cop.SubT
          CASE xoLookH, xoLookL, xoLookB : FiIx cop.Lfile
          CASE xoLEFT     : FiIx cop.A
          CASE xoRIGHT    : FiIx cop.A
          CASE xoMID      : FiIx cop.A
                            FiIx cop.B
        END SELECT
        
      CASE xxTransfer
        FiIx cop.Dst                                         'GMH93-9
        IF cop.SubT <> xfDate AND cop.SubT <> xfTime THEN
          FiIx cop.Src
        END IF
        
      CASE xxBranch
        
      CASE xxIf
        FiIx cop.Src
        FiIx cop.Dst
        SELECT CASE cop.SubT
          CASE xiIN    : FiIx cop.A : FiIx cop.B
          CASE xiOUT   : FiIx cop.A : FiIx cop.B
        END SELECT
        
      CASE xxRead
        FiIx cop.Dst
        i = cop.Options         'GMH93-2 was cop.Msk
        IF (i AND USETABLE)<>0 THEN FiIx cop.Ctab
        IF cop.A<>0 THEN FiIx cop.A
        IF cop.B<>0 THEN FiIx cop.B
        IF cop.LFile<>0 THEN FiIx cop.Lfile
        
      CASE xxSend : FiIx cop.Src
      CASE xxSound
      CASE xxPause
      CASE xxCls
      CASE xxGetDsr
      CASE xxGetSense
      CASE xxLoadFile : FiIx cop.Lfile
      CASE xxSetLine
      CASE xxUser
      CASE xxLabel : FiIx cop.Src
      CASE xxReturn
      CASE xxOn
      CASE xxChain : FiIx cop.Lfile
      CASE xxDuration: FiStrV cop.Dst
      CASE xxTransition: FiStrV cop.Dst
    END SELECT
    IF cop.Lbl < 0 THEN i = -cop.Lbl : FiIx i : cop.Lbl = -i
    Ops(iii) = cop
  NEXT iii
END SUB 'FiIndexOps
'
'****************************************************************************
'
'   FiCompressVars : Compress the variable table, omitting unused vars
'
'****************************************************************************
SUB FiCompressVars
  j = 1
  FOR i = VarAux+1 TO UvC
    ii = Uvi(i)
    IF ii>0 AND ii<i THEN
      Uv     (ii) = Uv (i)
      Uvs    (ii) = Uvs(i)
      Uve    (ii) = Uve(i)
      Uvu    (ii) = Uvu(i)
      Uvi    (ii) = Uvi(i)
      UvNames(ii) = UvNames(i)
    END IF
  NEXT i
  IF UvC <> UvCnew THEN
    AiStat "Eliminated"+STR$(Uvc-UvCnew)+" string(s)."
    SLEEP 1
    UvC = UvCnew
    UvLC = 0
    ExIs VarOut
    ExIs VarIn
  END IF
END SUB 'FiCompressVars

'****************************************************************************
'
'   FiCompressAll : Determine variable class for all variables
'
'****************************************************************************
SUB FiCompressAll
  AiStat "Compressing variable table..."
  FiClassOps                  ' Figure out what the variables are used for
  FiIndexOps                  ' Reset the variable indexes for all opcodes
  FiCompressVars              ' Squash the variable table
END SUB 'FiCompressAll

'End of Module AiFile.BAS

