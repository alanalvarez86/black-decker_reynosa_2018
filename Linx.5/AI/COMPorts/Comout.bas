' COMOUT.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                     C O M O U T                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LinxBasic                                Version : 1.0
'   Usage    : Low level LINX IO Checkout               OS : PCDOS V3.3
'   Language : LinxBASIC V1.0                           CS : IBM PC/AT
'
'   Purpose  : Outputs records to the COM port.  
'              Note! do not run on master LINX!
'   Running on a MASTER will result in a NOT A NORMAL TERMINAL ERROR.
'
'   Version     Date      Who     Short description of change
'     1.0    11 Aug 89    JHF     Original creation 
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z 
OPEN "COM" FOR OUTPUT AS #1
t$ = " > ABCDEFGHIJKLMNOPQRSTUVWXYZ "
t$ = t$+"abcdefghijklmnopqrstuvwxyz 1234567890"
i& = i& + 1
DO 
  PRINT #1,i&;t$
  i& = i&+1
LOOP
