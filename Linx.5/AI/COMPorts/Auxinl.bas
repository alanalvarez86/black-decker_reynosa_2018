' AUXINL.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                     A U X I N L                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LinxBasic                                Version : 1.0
'   Usage    : Low level LINX IO Checkout               OS : PCDOS V3.3
'   Language : LinxBASIC V1.0                           CS : IBM PC/AT
'
'   Purpose  : Line input from the AUX port with echo and host IO.
'   Requires : First test with AUXIN.bas and LCD.bas.
'
'   Version     Date      Who     Short description of change
'     1.0    11 Aug 89    JHF     Original creation 
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z 
c$   = "AUX"                    ' Port to open.
DIM SHARED ComBuf$              ' Buffer to contain incoming records.
DIM SHARED XCR$, XLF$, XBS$     ' Special characters for comm line.
XCR$ = CHR$(13)                 ' End of line value.
XLF$ = CHR$(10)                 ' Line feed value.
XBS$ = CHR$(08)                 ' Backspace value.

FUNCTION GetLine$ STATIC
DO
  DO WHILE LOC(#1)=0 : LOOP             ' Wait for any comm input.     
  ComBuf$ = ComBuf$+INPUT$(LOC(#1),#1)  ' Get the data from comm.
  i = INSTR(ComBuf$,XCR$)               ' Look for a carriage return.
  IF i<>0 THEN
    r$      = LEFT$(ComBuf$,i-1)        ' Isolate the record.
    ComBuf$ = MID$(ComBuf$,i+1)         ' Keep whats left over.  
    DO 
      i = INSTR(r$,XLF$)                ' Look for line feeds.
      IF i>0 THEN                       ' Strip line feed.
        r$ = MID$(r$,1,i-1)+MID$(r$,i+1)
      ELSE   
        i = INSTR(r$,XBS$)              ' Look for back spaces.
        IF i>0 THEN        
          IF i=1 THEN r$=MID$(r$,2) ELSE r$=MID$(r$,1,i-2)+MID$(r$,i+1)
        END IF
      END IF
    LOOP WHILE i>0
    GetLine$ = r$
    EXIT FUNCTION
  END IF
LOOP
END FUNCTION ' GetLine$

OPEN c$     FOR RANDOM AS #1            ' Open the comm line.
OPEN "HOST" FOR OUTPUT AS #2            ' Log the records to the host.
OPEN "LCD"  FOR OUTPUT AS #3              
IOCTL #2,"12"                           ' Set the record prefix to 12.
n  = LOF(#3) \ 2                        ' Get the display size.
i& = 0                                  ' Count the input records.
ComBuf$ = ""                            ' Clear the input buffer.
CLS  
PRINT "- Waiting for" : PRINT c$;" line input";
PRINT #1,"- Enter data lines."
DO 
  t$ = GetLine$                         ' Get a line.
  i& = i&+1                             ' Increment the counter.
  CLS                                   ' Display the line.
  PRINT c$+" line"+STR$(i&) : PRINT LEFT$(t$,n);
  s$ = c$+" line"+STR$(i&)+" <"+t$+">"
  PRINT #1, s$                          ' Echo the input line.
  PRINT #2, s$                          ' Send input line to host.
LOOP
