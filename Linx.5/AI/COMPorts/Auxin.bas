' AUXIN.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                     A U X I N                         %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LinxBasic                                Version : 1.0
'   Usage    : Low level LINX IO Checkout               OS : PCDOS V3.3
'   Language : LinxBASIC V1.0                           CS : IBM PC/AT
'
'   Purpose  : Inputs data from the AUX port and echos the results.
'   Requires : First test port with AUXOUT.bas.
'
'   Version     Date      Who     Short description of change
'     1.0    11 Aug 89    JHF     Original creation 
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1989, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z 
OPEN "AUX" FOR RANDOM AS #1
PRINT #1,"- Enter data."
DO 
  DO WHILE LOC(#1)=0 : LOOP             ' Wait for data.
  t$ = INPUT$(LOC(#1),#1)               ' Get the data.
  PRINT #1,"<"+t$+">"                   ' Display the data.
LOOP



  
