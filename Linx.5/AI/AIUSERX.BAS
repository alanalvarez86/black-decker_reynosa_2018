' AiUser.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                      A i U s e r                      %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : Applications Interface                   Version : 1.0
'   Usage    : User supplied AI operations              OS : PCDOS V3.3
'   Language : LinxBasic V1.0                           CS : IBM PC/AT
'
'   Purpose : This provides the interface between user written BASIC
'       subroutines, and AI.
'
'       ***** EXAMPLE ******  This illustrates the use of AiUser
'       to incorporate a user developed function into an AI
'       application.  To use this module :
'
'       COPY AIUSER.BAS AIUSER.SAV  'Save blank original module.
'       COPY AIUSERX.BAS AIUSER.BAS 'Modify AIUSERX.BAS for users needs.
'       BB AIUSER.BAS               'Compile AIUSER with Microsoft Compiler.
'       LAI                         'Link AI modules to include AIUSER.BAS
'       LAIGEN                      'Link AIGEN modules to include AIUSER.BAS
'
'       Note that there is a new option in the Other Operations
'       Menu (Additional opererations).  There are 2 new functions
'       available in this menu.  See below.
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0    21 Aug 90    JHF     Original creation
'
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

DEFINT A-Z
''##C !
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiUser.BI'
'
SUB AiUserInit
  UserName1  = "~Reverse"  ' Note the ~ will set R to be the hot key
  UserName2  = "~Siren"    ' Note the ~ will set S to be the hot key
  UserName3  = "?"
  UserName4  = "?"
  UserName5  = "?"
  UserName6  = "?"
  UserName7  = "?"
  UserName8  = "?"
  UserName9  = "?"
END SUB  'AiUserInit
''##E
'
'****************************************************************************
'
'   User1 : REVERSE - Reverses the characters of the IN variable
'
'****************************************************************************
SUB User1
  s$ = Uv(VarIn)
  t$ = ""
  FOR i = LEN(s$) TO 1 STEP -1
    t$ = t$+MID$(s$,i,1)
  NEXT i
  Uv(VarIn) = t$
END SUB 'User1

'****************************************************************************
'
'   User2 : SIREN - Makes a nasty whooping sound
'
'****************************************************************************
SUB User2
  FOR i = 1 TO 5
    FOR j = 600 TO 1200 STEP 100
      SOUND j, 2
      IF NOT PcMode THEN SLEEP 0.1      ' Note conditional to make
    NEXT j                              ' downloaded program sound
  NEXT i                                ' the same as under emulation.
END SUB 'User2

'****************************************************************************
SUB User3
END SUB 'User3

'****************************************************************************
SUB User4
END SUB 'User4

'****************************************************************************
SUB User5
END SUB 'User5

'****************************************************************************
SUB User6
END SUB 'User6

'****************************************************************************
SUB User7
END SUB 'User7

'****************************************************************************
SUB User8
END SUB 'User8

'****************************************************************************
SUB User9
END SUB 'User9

'****************************************************************************
' End of Module AiUser.bas






