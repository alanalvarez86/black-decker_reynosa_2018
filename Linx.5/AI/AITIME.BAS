' AiTime.bas
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%                       A i T i m e                     %%%%%%%%%%
'%%%%%%%%%%%                                                       %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'
'   Project  : LINX Support Environment                 Version : 2.0
'   Usage    : Formatted time and date                  OS : PCDOS V3.3
'   Language : LinxBasic V1.0                           CS : IBM PC/AT
'
'   Purpose : Formatted time and date functions
'   Requirements : Must be preceded by Env.bas, or EnvPC.bas
'
'-------------------------------- History -----------------------------------
'
'      Version     Date      Who     Short description of change
'        1.0    30 Jul 90    JHF     Original creation
'
'----------------------- Function and Procedure Index -----------------------
'
'   FormatDate$ : Formats a date string
'   FormatTime$ : Formats a time string
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'%%%%%%%%%%        CopyRight 1990, Linx Data Terminals, Inc.       %%%%%%%%%%
'%%%%%%%%%%                  All Rights Reserved.                  %%%%%%%%%%
'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
DEFINT A-Z
''##C @
'$INCLUDE:'AiVar.BI'
'$INCLUDE:'AiEnvPc.BI'
''##E
'
'****************************************************************************
'
'      FormatDate$ : Formats a date string
'
'      A string is returned which contains a representation of the
'      date in one of a number of different formats.
'
'      The len specifies the maximum returned string length and
'      determines the format which is returned.  These are :
'
'         len >= 18 : "September 30, 1988"
'         len >= 12 : "Sep 30, 1988"
'         len  = 11 : "30-Sep-1988"
'         len  = 10 : "10/30/1988" (this is the same as the input date)
'         len  = 9  : "30-Sep-88"
'         len  = 8  : "10/30/88"
'         len >= 6  : "881030"
'         len <  6  : "1030"
'
'      string = FormatDate$ ( GetDATE$, len )
'
'****************************************************************************
FUNCTION FormatDATE$ (da$, ln)
  IF ln = 10 THEN
    FormatDATE$ = da$
  ELSE
    month$ = LEFT$(da$, 2): day$ = MID$(da$, 4, 2)
    IF ln < 10 THEN year$ = RIGHT$(da$, 2) ELSE year$ = RIGHT$(da$, 4)
''##C D
    IF ln > 8 THEN
      SELECT CASE VAL(month$)
        CASE 2: mname$ = "February"
        CASE 3: mname$ = "March"
        CASE 4: mname$ = "April"
        CASE 5: mname$ = "May"
        CASE 6: mname$ = "June"
        CASE 7: mname$ = "July"
        CASE 8: mname$ = "August"
        CASE 9: mname$ = "September"
        CASE 10: mname$ = "October"
        CASE 11: mname$ = "November"
        CASE 12: mname$ = "December"
        CASE ELSE: mname$ = "January"
      END SELECT
      IF ln < 18 THEN mname$ = LEFT$(mname$, 3)
    END IF
''##E
    SELECT CASE ln
''##C D
      CASE IS > 11: FormatDATE$ = mname$ + " " + day$ + ", " + year$
      CASE IS > 8:   FormatDATE$ = day$ + "-" + mname$ + "-" + year$
''##E
      CASE 8:        FormatDATE$ = month$ + "/" + day$ + "/" + year$
      CASE IS > 5:   FormatDATE$ = year$ + month$ + day$
      CASE ELSE:     FormatDATE$ = month$ + day$
    END SELECT
  END IF
END FUNCTION 'FormatDATE$

'****************************************************************************
'
'      FormatTime$ : Formats a time string
'
'      A string is returned containing a representation of the time
'      in the specified format.
'
'      The len specifies the returned string format :
'
'         len >= 11 : "11:51:23 pm"
'         len >= 9  : "11:51 pm"
'         len  = 8  : "23:51:11"     military format (default input)
'         len >= 6  : "235111"
'         len  = 5  : "23:51"        military format
'         len <= 4  : "2351"         military format
'
'      string = FormatTIME$ ( GetTIME$, len );
'
'****************************************************************************
FUNCTION FormatTIME$ (ti$, ln)
  IF ln = 8 THEN
    FormatTIME$ = ti$
  ELSE
    hh$ = LEFT$(ti$, 2)
    mm$ = MID$(ti$, 4, 2)
    ss$ = RIGHT$(ti$, 2)
    IF ln > 8 THEN
      v = VAL(hh$)
      IF v > 11 THEN p$ = " pm": v = v - 12 ELSE p$ = " am"
      IF v = 0 THEN v = 12
      hh$ = RIGHT$(STR$(v + 100), 2)
      IF ln > 10 THEN p$ = ":" + ss$ + p$
      FormatTIME$ = hh$ + ":" + mm$ + p$
    ELSE
      SELECT CASE ln
        CASE IS > 5: FormatTIME$ = hh$ + mm$ + ss$
        CASE 5:        FormatTIME$ = hh$ + ":" + mm$
        CASE ELSE:     FormatTIME$ = hh$ + mm$
      END SELECT
    END IF
  END IF
END FUNCTION 'FormatTIME$
''##E

' End of Module AiTime.bas
