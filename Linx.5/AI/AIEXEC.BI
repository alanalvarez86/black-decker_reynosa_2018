' AiExec.BI - Basic Include file
'
DEFINT A-Z

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                          Constants and Types                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
' ---------- OpCodes ----------
CONST xxComment  = 1             ' Comment
CONST xxLet      = 2             ' Simple assigment or math operation
CONST xxTransfer = 3             ' Fundamental move data operator
CONST xxBranch   = 4             ' Branch instruction
CONST xxIf       = 5             ' Conditional
CONST xxRead     = 6             ' General read operation
CONST xxSend     = 7             ' General send operation
CONST xxSound    = 8             ' Make a sound
CONST xxPause    = 9             ' Pause
CONST xxCls      = 10            ' Clear screen
CONST xxGetDSR   = 11            ' Get DSR line to in variable
CONST xxGetSense = 12            ' Get sense port to in variable
CONST xxSetLine  = 13            ' Set a line to a position
CONST xxLoadFile = 14            ' Load a file to the LINX
CONST xxUser     = 15            ' User supplied operations
CONST xxLabel    = 16            ' Program label
CONST xxReturn   = 17            ' Return from subroutine
CONST xxOn       = 18            ' On CONDITION Goto
CONST xxChain    = 19            ' Chain to another program
CONST xxDuration = 20            ' Move sense line duration to a variable
CONST xxTransition=21            ' Move sense line transitions to a variable

'
CONST xoMove     =  1            ' Move to a variable
CONST xoApp      =  2            ' Append to a variable
CONST xoAdd      =  3            ' Add to a variable
CONST xoSub      =  4            ' Subtract from a variable
CONST xoMul      =  5            ' Multiply
CONST xoDiv      =  6            ' Divide
CONST xoNeg      =  7            ' Negate
CONST xoNUM      =  8            ' Make Numeric
CONST xoLookH    =  9            ' Lookup in a Host table/file
CONST xoLEN      =  10           ' Length of a variable
CONST xoLEFT     =  11           ' Left   part of a variable
CONST xoRIGHT    =  12           ' Right  part of a variable
CONST xoMID      =  13           ' Middle part of a variable
CONST xoTRIM     =  14           ' Trim spaces off a variable
CONST xoUCASE    =  15           ' Upper case a variable
CONST xoASC      =  16           ' Character to ASCII Value
CONST xoCHR      =  17           ' ASCII Value to Character
CONST xoMOD      =  18           ' Take Modulus of variable
CONST xoINT      =  19           ' Integer part of a variable
CONST xoLookL    =  20           ' Seq. lookup in a Linx table/file
CONST xoLookB    =  21           ' Binary lookup in a Linx table/file

'
CONST xfText     = 1             ' Text constant field
CONST xfVar      = 2             ' Variable field
CONST xfTime     = 3             ' Time field
CONST xfDate     = 4             ' Date field
CONST xfRead     = 5             ' Read field (all underscores)

CONST xlLED      = 1             ' LED line
CONST xlLINE     = 2             ' Control LINE
CONST xlRTS      = 3             ' RTS line on COM 1
CONST xlDTR      = 4             ' DTR line on COM 1

CONST xsQueue    = 1             ' Send to Queue
CONST xsHost     = 2             ' Send to Host
CONST xsCom      = 3             ' Send to Com
CONST xsAux      = 4             ' Send to Aux

CONST xiEQ       = 1             ' If equal to
CONST xiNE       = 2             ' If not equal to
CONST xiLT       = 3             ' If less than
CONST xiGT       = 4             ' If greater than
CONST xiIN       = 5             ' If in range
CONST xiOUT      = 6             ' if out of range
CONST xiSTART    = 7             ' If starts with
CONST xiEND      = 8             ' If ends with

CONST xnKey      = 1             ' On a key entered
CONST xnBdg      = 2             ' On read from badge reader
CONST xnCom      = 3             ' On read from comm port
CONST xnAux      = 4             ' On read from aux port
CONST xnHst      = 5             ' On read from host
CONST xnSns      = 6             ' On sensor input
CONST xnErr      = 7             ' On an error
CONST xnTmt      = 8             ' On a timeout

CONST xgGOTO     = 0             ' Default to GOTO subtype
CONST xgGOSUB    = 1             ' GOSUB branch instruction

'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                               Common Section                            @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
'
COMMON SHARED /AiExec/ UvLC AS INTEGER         ' # of User Variables in list
COMMON SHARED /AiExec/ OpC  AS INTEGER         ' # of opcodes in the program
COMMON SHARED /AiExec/ SOpC AS INTEGER         ' # of opcodes saved in stk

'$DYNAMIC
COMMON SHARED /AiExec/ UvL()       AS INTEGER  ' Display list of vars
'$STATIC


'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'@@                     Sub and Function Declarations                       @@
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'
DECLARE SUB      ExShowVars             ()
DECLARE SUB      ExIs                   (n%)
DECLARE SUB      ExShowOp               (p%, y%, rs$)
DECLARE SUB      ExShowCode             ()
DECLARE SUB      ExShowAllVars          ()
DECLARE FUNCTION ExGetLabel%            (s$)
DECLARE FUNCTION ExGetAbsLabel%         (s$)
DECLARE FUNCTION ExLineName$            (i%)
DECLARE FUNCTION ExLookup%              (ov%, ss$, ff%, pp%, nn%, rl%)
DECLARE SUB      ExecOp                 ()
DECLARE SUB      ExAddOp                (p AS OpCode, ed%)
DECLARE SUB      ExDelOp                ()
DECLARE SUB      ExSaveOp               ()
DECLARE SUB      ExRestoreOp            ()
DECLARE SUB      ExNewOp                (p AS OpCode, t%)
DECLARE SUB      ExDspField             (v%, s$, p%, n%, c%)
DECLARE FUNCTION ExAddField%            (cop AS OpCode, tt%, v%)
DECLARE FUNCTION ExSaveVars%            (n$)
DECLARE FUNCTION ExLoadVars             (n$)
DECLARE SUB      ExSaveGlobals          ()
DECLARE SUB      ExLoadGlobals          ()
DECLARE SUB      ExSave                 (s$)
DECLARE SUB      ExLoad                 (s$)
DECLARE FUNCTION ExSaved%               ()

'End AiExec.BI



