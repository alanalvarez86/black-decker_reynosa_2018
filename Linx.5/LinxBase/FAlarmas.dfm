inherited CapturarAlarmas: TCapturarAlarmas
  Left = 562
  Top = 238
  ActiveControl = Alarmas
  Caption = 'Capturar Archivos de Alarmas'
  ClientHeight = 245
  ClientWidth = 536
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 209
    Width = 536
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 368
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 453
    end
    object Agregar: TBitBtn
      Left = 4
      Top = 4
      Width = 70
      Height = 27
      Hint = 'Agregar Alarma Nuevo'
      Caption = '&Agregar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = AgregarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
    end
    object Borrar: TBitBtn
      Left = 76
      Top = 4
      Width = 70
      Height = 27
      Hint = 'Borrar El Alarma Seleccionado'
      Caption = '&Borrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BorrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
    end
    object Modificar: TBitBtn
      Left = 148
      Top = 4
      Width = 75
      Height = 27
      Hint = 'Modificar El Alarma Seleccionado'
      Caption = '&Modificar'
      TabOrder = 4
      OnClick = ModificarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
  end
  object PanelControles: TPanel
    Left = 224
    Top = 0
    Width = 312
    Height = 209
    Align = alRight
    TabOrder = 1
    object HoraLBL: TLabel
      Left = 18
      Top = 33
      Width = 26
      Height = 13
      Hint = 'Usar Formato 24 Horas'
      Alignment = taRightJustify
      Caption = '&Hora:'
      FocusControl = Hora
      ParentShowHint = False
      ShowHint = True
    end
    object ArchivoLBL: TLabel
      Left = 5
      Top = 9
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Archivo:'
      FocusControl = Archivo
    end
    object AlarmaSeek: TSpeedButton
      Left = 282
      Top = 4
      Width = 25
      Height = 25
      Hint = 'Buscar ALARMA.VAL'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = AlarmaSeekClick
    end
    object DiasLBL: TLabel
      Left = 18
      Top = 55
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = '&D�as:'
      FocusControl = Dias
    end
    object Archivo: TEdit
      Left = 47
      Top = 6
      Width = 231
      Height = 21
      TabOrder = 0
      OnChange = HayCambios
    end
    object Hora: TZetaHora
      Left = 47
      Top = 29
      Width = 40
      Height = 21
      Hint = 'Usar Formato 24 Horas'
      EditMask = '99:99;0'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Tope = 24
      OnChange = HayCambios
      OnExit = HayCambios
    end
    object Dias: TCheckListBox
      Left = 47
      Top = 52
      Width = 81
      Height = 97
      ItemHeight = 13
      Items.Strings = (
        'Domingo'
        'Lunes'
        'Martes'
        'Mi�rcoles'
        'Jueves'
        'Viernes'
        'S�bado')
      TabOrder = 2
      OnClick = HayCambios
    end
  end
  object PanelLista: TPanel
    Left = 0
    Top = 0
    Width = 224
    Height = 209
    Align = alClient
    TabOrder = 0
    object Alarmas: TListBox
      Left = 1
      Top = 1
      Width = 222
      Height = 207
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = AlarmasClick
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'val'
    Filter = 
      'Archivos VAL ( *.val )|*.val|Textos ( *.txt ) |*.txt|Todos ( *.*' +
      ' )|*.*'
    FilterIndex = 0
    Title = 'Especificar Un ALARMA.VAL'
    Left = 104
    Top = 16
  end
end
