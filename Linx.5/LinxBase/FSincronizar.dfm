inherited AskDateTime: TAskDateTime
  Left = 391
  Top = 281
  ActiveControl = Fecha
  Caption = 'Sincronizar'
  ClientHeight = 117
  ClientWidth = 183
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object FechaLBL: TLabel [0]
    Left = 13
    Top = 12
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = '&Fecha:'
    FocusControl = Fecha
  end
  object HoraLBL: TLabel [1]
    Left = 20
    Top = 36
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = '&Hora:'
    FocusControl = Hora
  end
  inherited PanelBotones: TPanel
    Top = 81
    Width = 183
    BevelOuter = bvNone
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 12
    end
    inherited Cancelar: TBitBtn
      Left = 95
    end
  end
  object Fecha: TZetaFecha
    Left = 48
    Top = 8
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 0
    Text = '09/May/98'
    Valor = 35924
  end
  object Hora: TZetaHora
    Left = 49
    Top = 32
    Width = 62
    Height = 21
    EditMask = '99:99:99;0'
    TabOrder = 1
    Tope = 24
    UseSeconds = True
  end
  object chHoraSist: TCheckBox
    Left = 22
    Top = 61
    Width = 141
    Height = 17
    Caption = 'Utilizar la hora del sistema'
    TabOrder = 3
    OnClick = chHoraSistClick
  end
end
