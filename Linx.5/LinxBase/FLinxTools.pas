unit FLinxTools;

interface

uses Windows, SysUtils, StdCtrls, Controls, Classes, ShellApi,
     FileCtrl, Forms, Dialogs, Messages, Graphics;

function SetFileNameDefaultPath( const sFile: String ): String;

implementation

function SetFileNamePath( const sPath, sFile: String ): String;
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile;
end;

function SetFileNameDefaultPath( const sFile: String ): String;
begin
     Result := SetFileNamePath( ExtractFileDir( Application.ExeName ), sFile );
end;

end.
