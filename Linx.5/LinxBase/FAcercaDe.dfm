object AcercaDe: TAcercaDe
  Left = 244
  Top = 175
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Acerca de'
  ClientHeight = 373
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Label1: TLabel
    Left = -7
    Top = 19
    Width = 64
    Height = 16
    Alignment = taRightJustify
    Caption = '# de Serie:'
  end
  object Label2: TLabel
    Left = 60
    Top = 19
    Width = 115
    Height = 17
    AutoSize = False
    Caption = 'SerialNumber'
    ShowAccelChar = False
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 368
    Height = 340
    ActivePage = AutoData
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object TabVersion: TTabSheet
      Caption = '&Versi'#243'n'
      object PanelDatos: TPanel
        Left = 0
        Top = 0
        Width = 360
        Height = 312
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 0
        object VersionBuild: TLabel
          Left = 1
          Top = 32
          Width = 330
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Versi'#243'n 2.0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          IsControl = True
        end
        object Copyright: TLabel
          Left = 17
          Top = 130
          Width = 289
          Height = 19
          Alignment = taCenter
          AutoSize = False
          Caption = 'Grupo Tress Internacional, S.A. de C.V.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          WordWrap = True
          IsControl = True
        end
        object Comments: TLabel
          Left = 73
          Top = 148
          Width = 177
          Height = 39
          Alignment = taCenter
          AutoSize = False
          Caption = 'Derechos Reservados 1996-2010'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          WordWrap = True
          IsControl = True
        end
        object ProductName: TLabel
          Left = 1
          Top = 12
          Width = 330
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Cafeter'#237'a'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          IsControl = True
        end
        object Logo: TImage
          Left = 145
          Top = 81
          Width = 32
          Height = 32
          AutoSize = True
          Picture.Data = {
            055449636F6E0000010001002020100000000000E80200001600000028000000
            2000000040000000010004000000000080020000000000000000000000000000
            0000000000000000000080000080000000808000800000008000800080800000
            C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
            FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB9B9B9B9BFFFFF9BFF
            FFFFFFFFFFFFFF9B9B9B9BFFFFFFF9FFFFFFFFFFFFFFFFB9B9B9BFF55555FFFF
            FFFFFFFFFFFFFFFB9B9B9FF55555FFFFFFFFFFFFFFFFFFF9B9B9BFF55555FFFF
            FFFFFFFFFFFFFFFB9BFFFFF5555555FFFFFFFFFFFFFFFFF9FFFFFFFF555555FF
            FFFFFFFFFFFFFFFFFF5555FF555555FFFFFFFFFFFFFFFFFFFF5555555555555F
            FFFFFFFFFFFFFFFFFF5555555555555FFFFFFFFFFFFFFFFFFF5555555555555F
            FFFFFFFFFFFFFFFFFFF555555555555FFFFFFFFFFFFFFFFFFFFF55555555555F
            FFFFFFFFFFFFFFFFF5FF55555555555FFFFFFFFFFFFFFFFFF555555555555555
            FFFFFFFFFFFFFFFFF555555555555555FFFFFFFFFFFFFFFFFF55555555555555
            FFFFFFFFFFFFFFFFFF55555555555555FFFFFFFFFFFFFFFFFF55555555555555
            FFFFFFFFFFFFFFFFFF55555555555555FFFFFFFFFFFFFFFFFF55555555555555
            FFFFFFFFFFFFFFFFFF555555555555555FFFFFFFFFFFFFFFFFF5555555555555
            5FFFFFFFFFFFFFFFFFF55555555555555FFFFFFFFFFFFFFFFFF5555555555555
            5FFFFFFFFFFFFFFFFFF55555555555555FFFFFFFFFFFFFFFFFF5555555555555
            5FFFFFFFFFFFFFFFFFF555555555555555FFFFFFFFFFFFFFFFFF555555555555
            55FFFFFFFFFFFFFFFFFF55555555555555FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFF00000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            00000000}
          Stretch = True
        end
        object LblBuild: TLabel
          Left = 1
          Top = 52
          Width = 330
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Build 2.0.0.0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          IsControl = True
        end
      end
    end
    object TabRecursos: TTabSheet
      Caption = '&Recursos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      object DiscoGB: TGroupBox
        Left = 0
        Top = 119
        Width = 360
        Height = 193
        Align = alClient
        Caption = ' Disco '
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object DiscoTotalLBL: TLabel
          Left = 42
          Top = 22
          Width = 65
          Height = 13
          Alignment = taRightJustify
          Caption = 'Capacidad:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DiscoDisponible: TLabel
          Left = 110
          Top = 44
          Width = 80
          Height = 13
          Caption = '9999 MegaBytes'
        end
        object DiscoTotal: TLabel
          Left = 110
          Top = 22
          Width = 80
          Height = 13
          Caption = '9999 MegaBytes'
        end
        object DiscoDisponibleLBL: TLabel
          Left = 25
          Top = 44
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Espacio Libre:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DiscoOcupadoLBL: TLabel
          Left = 41
          Top = 65
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ocupaci'#243'n:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DiscoOcupado: TLabel
          Left = 110
          Top = 65
          Width = 38
          Height = 13
          Caption = '99.99 %'
        end
      end
      object MemoriaGB: TGroupBox
        Left = 0
        Top = 0
        Width = 360
        Height = 119
        Align = alTop
        Caption = ' Memoria '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object MemoriaRAMLBL: TLabel
          Left = 78
          Top = 36
          Width = 32
          Height = 13
          Alignment = taRightJustify
          Caption = 'RAM:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object MemoriaRAM: TLabel
          Left = 114
          Top = 36
          Width = 68
          Height = 13
          Caption = '99 MegaBytes'
        end
        object RecursosLibresLBL: TLabel
          Left = 14
          Top = 17
          Width = 96
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recursos Libres:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object RecursosLibres: TLabel
          Left = 116
          Top = 17
          Width = 32
          Height = 13
          Caption = '80.5 %'
        end
        object MemoriaRAMDisponible: TLabel
          Left = 114
          Top = 55
          Width = 68
          Height = 13
          Caption = '99 MegaBytes'
        end
        object MemoriaRAMDisponibleLBL: TLabel
          Left = 15
          Top = 55
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'RAM Disponible:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object MemoriaVirtualLBL: TLabel
          Left = 69
          Top = 75
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = 'Virtual:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object MemoriaVirtual: TLabel
          Left = 114
          Top = 75
          Width = 68
          Height = 13
          Caption = '99 MegaBytes'
        end
        object MemoriaVirtualDisponible: TLabel
          Left = 114
          Top = 94
          Width = 68
          Height = 13
          Caption = '99 MegaBytes'
        end
        object MemoriaVirtualDisponibleLBL: TLabel
          Left = 6
          Top = 94
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Virtual Disponible:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object AutoData: TTabSheet
      Caption = '&Autorizaci'#243'n'
      ImageIndex = 2
      object PanelAuto: TPanel
        Left = 0
        Top = 0
        Width = 360
        Height = 97
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SerialNumberLBL: TLabel
          Left = 1
          Top = 19
          Width = 65
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Guardia:'
        end
        object EmpresaLBL: TLabel
          Left = 12
          Top = 36
          Width = 54
          Height = 13
          Alignment = taRightJustify
          Caption = '# Empresa:'
        end
        object EmpleadosLBL: TLabel
          Left = 11
          Top = 53
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = 'Empleados:'
        end
        object UsuariosLBL: TLabel
          Left = 22
          Top = 70
          Width = 44
          Height = 13
          Alignment = taRightJustify
          Caption = 'Usuarios:'
        end
        object Vencimiento: TLabel
          Left = 237
          Top = 35
          Width = 110
          Height = 17
          AutoSize = False
          Caption = 'Vencimiento'
          ShowAccelChar = False
        end
        object Usuarios: TLabel
          Left = 71
          Top = 70
          Width = 110
          Height = 17
          AutoSize = False
          Caption = 'Usuarios'
          ShowAccelChar = False
        end
        object Empresa: TLabel
          Left = 71
          Top = 36
          Width = 115
          Height = 17
          AutoSize = False
          Caption = 'Empresa'
          ShowAccelChar = False
        end
        object Empleados: TLabel
          Left = 71
          Top = 53
          Width = 133
          Height = 17
          AutoSize = False
          Caption = 'Empleados'
          ShowAccelChar = False
        end
        object SerialNumber: TLabel
          Left = 71
          Top = 19
          Width = 115
          Height = 17
          AutoSize = False
          Caption = 'SerialNumber'
          ShowAccelChar = False
        end
        object VencimientoLBL: TLabel
          Left = 174
          Top = 35
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vencimiento:'
        end
        object Version: TLabel
          Left = 237
          Top = 18
          Width = 110
          Height = 17
          AutoSize = False
          Caption = 'Versi'#243'n:'
          ShowAccelChar = False
        end
        object VersionLBL: TLabel
          Left = 197
          Top = 18
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Versi'#243'n:'
        end
        object Plataforma: TLabel
          Left = 13
          Top = 0
          Width = 53
          Height = 13
          Alignment = taRightJustify
          Caption = 'Plataforma:'
        end
        object lblPlataforma: TLabel
          Left = 71
          Top = 0
          Width = 109
          Height = 17
          AutoSize = False
          Caption = 'Plataforma'
          ShowAccelChar = False
        end
        object lblBaseDatos: TLabel
          Left = 238
          Top = 0
          Width = 115
          Height = 17
          AutoSize = False
          Caption = 'lblBaseDatos'
          ShowAccelChar = False
        end
        object Label4: TLabel
          Left = 162
          Top = 0
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Base de Datos:'
        end
        object EsKit: TCheckBox
          Left = 149
          Top = 52
          Width = 102
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Kit de Distribuidor:'
          Enabled = False
          TabOrder = 0
        end
      end
      object PanelModulos: TPanel
        Left = 0
        Top = 97
        Width = 360
        Height = 215
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object ModulosGB: TGroupBox
          Left = 0
          Top = 0
          Width = 177
          Height = 215
          Align = alLeft
          Caption = ' Autorizados '
          TabOrder = 0
          object Modulos: TCheckListBox
            Left = 2
            Top = 15
            Width = 173
            Height = 198
            Align = alClient
            BorderStyle = bsNone
            Color = clBtnFace
            Enabled = False
            ItemHeight = 13
            TabOrder = 0
          end
        end
        object Caducidad: TGroupBox
          Left = 177
          Top = 0
          Width = 183
          Height = 215
          Align = alClient
          Caption = ' Pr'#233'stamos '
          TabOrder = 1
          object Prestamos: TCheckListBox
            Left = 2
            Top = 15
            Width = 179
            Height = 198
            Align = alClient
            BorderStyle = bsNone
            Color = clBtnFace
            Enabled = False
            ItemHeight = 13
            TabOrder = 0
          end
        end
      end
    end
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 340
    Width = 368
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      368
      33)
    object OK: TBitBtn
      Left = 289
      Top = 5
      Width = 78
      Height = 26
      Anchors = [akTop, akRight]
      Caption = '&OK'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Kind = bkOK
      Margin = 2
      Spacing = -1
      IsControl = True
    end
  end
end
