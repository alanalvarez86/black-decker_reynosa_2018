unit DPoll;

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComObj, Db, DBClient,
     {$ifndef ZKPoll}
     LinxIni,
     {$endif}
     {$ifdef HTTP_CONNECTION}SConnect,{$endif}
     {$ifdef DOS_CAPAS}
     DServerCalcNomina,
     DServerLogin,
     {$else}
     DCalcNomina_TLB,
     Login_TLB,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaClientDataSet;

type
  TFeedback = procedure( const sMensaje: String ) of object;
  TFeedBackError  = procedure( const sMensaje: String; Error: Exception ) of object;
  TFeedBackStatus = procedure( const sMessage: String ) of object;
  TdmPoll = class(TDataModule)
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    cdsLog: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FConnected: Boolean;
    FOnMessage: TFeedback;
    FOnError: TFeedBackError;
    FOnStatus: TFeedBackStatus;

    {$ifdef DOS_CAPAS}
    FServerLogin: TdmServerLogin;
    FServerCalcNomina: TdmServerCalcNomina;
    function GetServerLogin: TdmServerLogin;
    function GetServerCalcNomina: TdmServerCalcNomina;
    {$else}
    {$ifdef HTTP_CONNECTION}
    FWebConnection: TWebConnection;
    {$endif}
    function CreateServer(const ClassID: TGUID): IDispatch;
    function GetServerLogin: IdmServerLoginDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    {$endif}

    function GetLog: TDataset;
    procedure Feedback( const sMensaje: String );
    procedure FeedbackError( const sMensaje: String; Error: Exception );
    procedure FeedbackStatus( const sMessage: String );

  protected
    property ServerLogin : {$ifdef DOS_CAPAS}TdmServerLogin{$else}IdmServerLoginDisp{$endif} read GetServerLogin;
    property ServerCalcNomina: {$ifdef DOS_CAPAS}TdmServerCalcNomina{$else}IdmServerCalcNominaDisp{$endif} read GetServerCalcNomina;

  public
    { Public declarations }
    property Log: TDataset read GetLog;
    property OnMessage: TFeedback read FOnMessage write FOnMessage;
    property OnError: TFeedBackError read FOnError write FOnError;
    property OnStatus: TFeedBackStatus read FOnStatus write FOnStatus;
    function DoPoll(const sArchivo, sOffLineBuffer: String; var iRecords: Integer): Boolean;
    function EnviarChecadas(const sArchivo: String; const lProcesar: Boolean; var iRecords: Integer): Boolean; overload;
    function EnviarChecadas(const sArchivo: String; var iRecords: Integer): Boolean; overload;
    function GetAuto: OleVariant;
    function ValidaRegistryWrite: Boolean;
    procedure ConfigurarServidor;
    procedure ShowLog;
  end;

var
  dmPoll: TdmPoll;

implementation

uses ZetaCommonTools,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     ZetaRegistryServerEditor,
     {$else}
     ZetaRegistryCliente,
     {$endif}
     FViewLog,
     ZAsciiTools,
     ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

procedure TdmPoll.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor.InitComparte;
     DZetaServerProvider.InitAll;
     FServerLogin := TdmServerLogin.Create( Self );
     FServerCalcNomina := TdmServerCalcNomina.Create( Self );
     {$else}
     ZetaRegistryCliente.InitClientRegistry;
     ClientRegistry.InitComputerName;
     {$endif}
     {$ifdef HTTP_CONNECTION}
     FWebConnection := TWebConnection.Create( Self );
     with FWebConnection do
     begin
          LoginPrompt := False;
          Agent := 'GTI';
     end;
     {$endif}
end;

procedure TdmPoll.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef HTTP_CONNECTION}
     FreeAndNil( FWebConnection );
     {$endif}
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerCalcNomina );
     FreeAndNil( FServerLogin );
     ZetaSQLBroker.FreeAll( True );
     DZetaServerProvider.FreeAll;
     {$else}
     ZetaRegistryCliente.ClearClientRegistry;
     {$endif}
end;

{$ifdef DOS_CAPAS}
function TdmPoll.GetServerLogin: TdmServerLogin;
begin
     Result := FServerLogin;
end;

function TdmPoll.GetServerCalcNomina: TdmServerCalcNomina;
begin
     Result := FServerCalcNomina;
end;
{$else}
function TdmPoll.CreateServer( const ClassID: TGUID ): IDispatch;
begin
     with ClientRegistry do
     begin
          {$ifdef HTTP_CONNECTION}
          case ClientRegistry.TipoConexion of
               conxHTTP:
               begin
                    with FWebConnection do
                    begin
                         Connected := False;
                         URL := ClientRegistry.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
                         Username := ClientRegistry.UserName;
                         Password := ClientRegistry.Password;
                         ServerGUID := GUIDToString( ClassID );
                         Connected := True;
                         Result := AppServer;
                    end;
               end
               else
               begin
                    if ( ComputerName = '' ) then
                       Result := ComObj.CreateComObject( ClassID ) as IDispatch
                    else
                        Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
               end;
          end;
          {$else}
          if ( ComputerName = '' ) then
             Result := ComObj.CreateComObject( ClassID ) as IDispatch
          else
              Result := ComObj.CreateRemoteComObject( ComputerName, ClassID ) as IDispatch;
          {$endif}
     end;
end;

function TdmPoll.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( CreateServer( CLASS_dmServerLogin ) );
end;

function TdmPoll.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( CreateServer( CLASS_dmServerCalcNomina ) );
end;
{$endif}

function TdmPoll.GetAuto: OleVariant;
begin
     Result := GetServerLogin.GetAuto;
end;

function TdmPoll.ValidaRegistryWrite: Boolean;
begin
     {
     Solo se checa en Corporativa ya que en Profesional
     es muy costoso: se tiene que crear una forma;
     sin embargo en Profesional aparecerá un ZetaDialogo
     indicando la imposibilidad de configurar }
     {$ifdef DOS_CAPAS}
     Result := True;
     {$else}
     Result := ClientRegistry.CanWrite;
     {$endif}
end;

procedure TdmPoll.ConfigurarServidor;
begin
     {$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor.EspecificaComparte;
     {$else}
     ClientRegistry.BuscaServidor;
     {$endif}
end;

procedure TdmPoll.ShowLog;
begin
     with cdsLog do
     begin
          if Active and not IsEmpty then
          begin
               with TViewLog.Create( Self ) do
               begin
                    try
                       LogDataset := cdsLog;
                       ShowModal;
                    finally
                           Free;
                    end;
               end;
          end
          else
              ZetaDialogo.zError( 'ˇ Error Al Enviar Checadas !', 'Hubo Problemas Al Enviar Checadas', 0 );
     end;
end;

function TdmPoll.EnviarChecadas( const sArchivo: String; const lProcesar: Boolean; var iRecords: Integer ): Boolean;
var
   {$ifdef DOS_CAPAS}
   Servidor: TdmServerCalcNomina;
   {$else}
   Servidor: IdmServerCalcNominaDisp;
   {$endif}
   Lista, Empresa: OleVariant;
   i, iLow, iHigh: Integer;
   sEmpresa: String;
   Parametros: TZetaParams;
begin
     if FileExists( sArchivo ) then
     begin
          ZAsciiTools.FillcdsASCII( cdsASCII, sArchivo );
          with cdsASCII do
          begin
               Active := True;
               MergeChangeLog;  // Elimina Registros Borrados //
               Lista := Data;
               iRecords := RecordCount;
               Active := False;
          end;
          Result := False;
          FConnected := False;
          if ( iRecords > 0 ) then
          begin
               Parametros := TZetaParams.Create;
               with Parametros do
               begin
                    {$ifndef ZKPoll}
                    with LinxIni.IniValues do
                    begin
                         AddString( 'Campo', Campo );
                         AddString( 'Empresa', Empresa );
                         AddBoolean( 'TransformarNumero', TransformarNumero );
                    end;
                    {$endif}
               end;
               try
                  try
                     Servidor := GetServerCalcNomina;
                     try
                        if Assigned( Servidor ) then
                        begin
                             with cdsLog do
                             begin
                                  Data := Servidor.PollTotal( Parametros.VarValues, Lista );
                                  Result := not Active or IsEmpty;
                                  FConnected := True;
                             end;
                        end
                        else
                            {$ifdef DOS_CAPAS}
                            FeedBack( 'No Hay Comunicación Con El Servidor' );
                            {$else}
                            FeedBack( Format( 'No Hay Comunicación Con El Servidor %s', [ ClientRegistry.ComputerName ] ) );
                            {$endif}
                     finally
                            {$ifndef DOS_CAPAS}
                            Servidor := nil;
                            {$endif}
                     end;
                     if FConnected and lProcesar and not VarIsNull( Lista ) then
                     begin
                          with Parametros do
                          begin
                               Clear;
                               AddBoolean( 'PuedeCambiarTarjeta', FALSE );
                          end;

                          iLow := VarArrayLowBound( Lista, 1 );
                          iHigh := VarArrayHighBound( Lista, 1 );
                          for i := iLow to iHigh do
                          begin
                               sEmpresa := Lista[ i, 1 ];
                               Empresa := Lista[ i, 2 ];
                               try
                                  FeedBackStatus( 'Procesando ' + sEmpresa );
                                  GetServerCalcNomina.ProcesarTarjetasPendientes( Empresa, Parametros.VarValues );
                               except
                                     on Error: Exception do
                                     begin
                                          if ( Error.HelpContext = 0 ) then
                                             FeedbackError( Format( 'Error Al Procesar %s', [ sEmpresa ] ), Error );
                                     end;
                               end;
                          end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             FeedBackError( 'Error Al Enviar Checadas', Error );
                        end;
                  end;
                  {$ifndef DOS_CAPAS}
                  Servidor := nil;
                  {$endif}
               finally
                      FreeAndNil( Parametros );
               end;
          end
          else
              Result := True;
     end
     else
     begin
          FeedBack( Format( 'Archivo %s no existe', [ sArchivo ] ) );
          Result := True;
     end;
end;

function TdmPoll.EnviarChecadas( const sArchivo: String; var iRecords: Integer ): Boolean;
begin

     Result :=  {$ifndef ZKPoll} EnviarChecadas( sArchivo, LinxIni.IniValues.ProcesarTarjetas, iRecords ){$else} False{$endif};
end;

function TdmPoll.DoPoll( const sArchivo, sOffLineBuffer: String; var iRecords: Integer ): Boolean;
begin
     Result := EnviarChecadas( sArchivo, iRecords );
     if not FConnected then
     begin
          if FileExists( sOffLineBuffer ) then
             SysUtils.DeleteFile( sOffLineBuffer );
          RenameFile( sArchivo, sOffLineBuffer );
     end;
end;

procedure TdmPoll.Feedback(const sMensaje: String);
begin
     if Assigned( FOnMessage ) then
        FOnMessage( sMensaje );
end;

procedure TdmPoll.FeedbackError(const sMensaje: String; Error: Exception);
begin
     if Assigned( FOnError ) then
        FOnError( sMensaje, Error );
end;

procedure TdmPoll.FeedbackStatus(const sMessage: String);
begin
     if Assigned( FOnStatus ) then
        FOnStatus( sMessage );
end;

function TdmPoll.GetLog: TDataset;
begin
     Result := cdsLog;
end;

end.
