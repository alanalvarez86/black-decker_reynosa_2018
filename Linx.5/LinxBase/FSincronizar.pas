unit FSincronizar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     ZBaseDlgModal,
     ZetaHora,
     ZetaFecha;

type
  TAskDateTime = class(TZetaDlgModal)
    FechaLBL: TLabel;
    HoraLBL: TLabel;
    Fecha: TZetaFecha;
    Hora: TZetaHora;
    chHoraSist: TCheckBox;
    procedure chHoraSistClick(Sender: TObject);
  private
    { Private declarations }
    function GetDateValue: TDateTime;
    procedure SetDateValue( const dValue: TDateTime );
  public
    { Public declarations }
    property DateValue: TDateTime read GetDateValue write SetDateValue;
  end;

function GetDateTime( var dValue: TDateTime; const lMuestraCheckbox: Boolean; out lUtilizaHoraSistema: Boolean ): Boolean;

var
  AskDateTime: TAskDateTime;

implementation

uses ZetaCommonTools;

{$R *.DFM}

function GetDateTime( var dValue: TDateTime; const lMuestraCheckbox: Boolean; out lUtilizaHoraSistema: Boolean ): Boolean;
begin
     with TAskDateTime.Create( Application ) do
     begin
          try
             DateValue := dValue;
             chHoraSist.Visible := lMuestraCheckbox;
             if lMuestraCheckbox then
                  Height := 151
             else
                 Height := 132;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
             begin
                  dValue := DateValue;
                  lUtilizaHoraSistema := chHoraSist.Checked;
             end;
          finally
                 Free;
          end;
     end;
end;

{ ************ TAskDateTime ************ }

function TAskDateTime.GetDateValue: TDateTime;
begin
     with Fecha do
     begin
          Result := EncodeDate( TheYear( Valor ), TheMonth( Valor ), TheDay( Valor ) );
     end;
     with Hora do
     begin
          Result := Result + EncodeTime( StrToInt( Copy( Valor, 1, 2 ) ), StrToInt( Copy( Valor, 3, 2 ) ), StrToInt( Copy( Valor, 5, 2 ) ), 0 );
     end;
end;

procedure TAskDateTime.SetDateValue( const dValue: TDateTime );
var
   iHour, iMin, iSec, iMSec: Word;
begin
     Fecha.Valor := dValue;
     DecodeTime( dValue, iHour, iMin, iSec, iMSec );
     Hora.Valor := Format( '%2.2d%2.2d%2.2d', [ iHour, iMin, iSec ] );
end;

procedure TAskDateTime.chHoraSistClick(Sender: TObject);
begin
     Fecha.Enabled := not chHoraSist.Checked;
     Hora.Enabled := not chHoraSist.Checked;
end;

end.
