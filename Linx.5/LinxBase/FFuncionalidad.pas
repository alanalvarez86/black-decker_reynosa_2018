unit FFuncionalidad;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     LinxIni,
     ZBaseDlgModal;

type
  TFuncionalidad = class(TZetaDlgModal)
    ProcesarTarjetas: TCheckBox;
    EnviarChecadas: TCheckBox;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    ArchivoSeek: TSpeedButton;
    OpenDialog: TOpenDialog;
    TransformarNumero: TCheckBox;
    EmpresaLBL: TLabel;
    Empresa: TEdit;
    CampoLBL: TLabel;
    Campo: TEdit;
    procedure FormShow(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure TransformarNumeroClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControls;
  public
    { Public declarations }
  end;

var
  Funcionalidad: TFuncionalidad;

implementation

{$R *.DFM}

procedure TFuncionalidad.FormShow(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Self.ProcesarTarjetas.Checked := ProcesarTarjetas;
          Self.EnviarChecadas.Checked := EnviarChecadas;
          Archivo.Text := ArchivoAdicional;
          Self.TransformarNumero.Checked := TransformarNumero;
          Self.Empresa.Text := Empresa;
          Self.Campo.Text := Campo;
     end;
     SetControls;
end;

procedure TFuncionalidad.SetControls;
begin
     with TransformarNumero do
     begin
          EmpresaLBL.Enabled := Checked;
          Empresa.Enabled := Checked;
          CampoLBL.Enabled := Checked;
          Campo.Enabled := Checked;
     end;
end;

procedure TFuncionalidad.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     with Archivo do
     begin
          with OpenDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TFuncionalidad.TransformarNumeroClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TFuncionalidad.OKClick(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          ProcesarTarjetas := Self.ProcesarTarjetas.Checked;
          EnviarChecadas := Self.EnviarChecadas.Checked;
          ArchivoAdicional := Archivo.Text;
          TransformarNumero := Self.TransformarNumero.Checked;
          Empresa := Self.Empresa.Text;
          Campo := Self.Campo.Text;
     end;
end;

end.
