{
It is assumed that you have installed
Online Comm Manager in the folder of
C:\OnlineCommManager

Otherwise, you have to

1. Copy OnlineDll.Dll to Windows or Windows\System folder,
   and remove the path specification
or

2. Copy OnlineDll.Dll to the same folder of your Exe file,
   and remove the path specification

or

3. Manual modify the path in
   External 'C:\OnlineCommManager\OnlineDll.Dll'
}

{$define DESCONECTAR}

unit FOnlineDll;

interface

uses Windows, SysUtils, Forms, Classes,
     LinxIni,LinxIniBase,  Sockets
     {$ifdef PROFILE},ZetaProfiler{$endif};

Const

{$ifdef PRUEBAS}
     TOPE_DEMO = 10;
{$else}
     TOPE_DEMO = 25;
{$endif}

     K_ESPERA_CLOSE_TCP = 1500;
     K_MILISECONDS = 1000;
     K_TRANSACTION_DELAY = 100;
     K_ONLINE_DLL_FILE = 'Syndll.dll';
    CmdOK        = 0; { Command OK }

    CmdDataNoRecv   = 0;
    CmdDataOK       = 1;

    {Heredados por unitech , AV: TODO BORRAR}
    CmdEOT       = 2;
    CmdEOF       = 3;
    CmdACK       = 4;
    CmdNAK       = 5;
    CmdYES       = 6;
    CmdNO        = 7;
    CmdUSE       = 8;
    CmdNONE      = 9;
    CmdErr       = 128;
    CmdErrLen    = 129;
    CmdErrTime   = 130;
    CmdErrSocket = 131;




    {Nuevos por Synel}
    ERR_GENERAL           = -2020;
    ERR_Exception         = -2016;
    ERR_Busy              = -2015;
    ERR_PortNotOpen       = -2001;
    ERR_PortAllreadyOpen  = -2002;
    ERR_CanNotOpenPort    = -2003;
    ERR_PortDisConnect    = -2004;
    ERR_TimeOut           = -2006;
    ERR_DataOnTerminal    = -3002;
    DataAAlreadyWrite     = -3004;



Type
    TMakeUnitListEvent = procedure of object;
    TGaugeEvent = procedure( const sMsg: String; iValue: Integer ) of object;
    TFeedback = procedure( const sMensaje: String ) of object;
    TFeedBackError  = procedure( const sMensaje: String; Error: Exception ) of object;
    TFeedBackEvent = procedure( const sMensaje: String; iTerminal: Integer ) of object;
    TLecturaEvent = procedure( const sData: String ) of object;
    TPuedeLeerChecadas =    procedure ( var lPuede : Boolean) of object;

    TStatusEquipo = class( TObject )
    protected
     MULTIDROP_ID      : string;
     MODEL_NO          : string;
     VERSION_NO        : string;
     STATION_ID        : string;
     CLOCK_YEAR        : string;
     CLOCK_MONTH       : string;
     CLOCK_DAY         : string;
     CLOCK_HOUR        : string;
     CLOCK_MINUTES     : string;
     ACTIVE_FUNCTION   : string;
     FULL_BUFFERS      : string;
     FAULTY_BUFFERS    : string;
     FULL_BUFFERS_SENT : string;
     EMPTY_BUFFERS     : string;
     MEMORY_SIZE_USED  : string;
     STATION_TYPE      : string;
     SELF_TEST         : string;
     CRC               : string;
     EOT               : string;
     FOk               : Boolean;
    public
       constructor Create( const sData : string);
       destructor Destroy; override;
    end;

    TOnlineMgr = class( TObject )
    private
      { Private declarations }
      FUnits: Integer;
      FTerminal: Integer;
      FOnError: TFeedBackError;
      FOnMessage: TFeedback;
      FOnFeedBack: TFeedBackEvent;
      FOnGaugeSetSize: TGaugeEvent;
      FOnGaugeFinish: TGaugeEvent;
      FOnGaugeAdvance: TGaugeEvent;
      FOnMakeUnitList: TMakeUnitListEvent;
      FOnDoLectura: TLecturaEvent;
      FPuedeLeerChecadas : TPuedeLeerChecadas;
      FValidTokens: String;
      FData: String;
      FPollData: TStrings;
      FPollPtr: Integer;
      FStatus: String;

      FEsDemo: boolean;
      FNumeroSentinel : integer;

      FLock :TRTLCriticalSection;
      FLockFuncionalidad :TRTLCriticalSection;


      //function Ack(const iResult: Word): Boolean;
      function ChangeFileNameToLocalDAT(const sArchivo: String): String;
      function Check(const iResult: smallint ): Boolean;
      function CheckData( const iResult: smallint ): Boolean;
      function CommunicationConnect( const iTerminal: Integer ): Boolean;
      function CommunicationEnd( const iTerminal: Integer ): Boolean;
      function CommunicationStart( const iTerminal: Integer ): Boolean;
      function GetNetworkInfo( const iTerminal: Integer ): string;
      //function Yes(const iResult: Word): Boolean;
      function EnviarArchivo( const iTerminal: Integer; const sSource, sTarget: String ): Boolean;
      function EnviarArchivoParametros(const iTerminal: Integer; const sLongFileName, sShortFileName: String): Boolean;
      function GetReplyMsg( const iResult: smallint): String;
      function GetRunningProgram( const iTerminal: Integer; var sPrograma: String ): Boolean;
      function GetTerminalID: Integer;
      function GetTerminal(Index: Integer): TTerminal;
      function TransformarConfig( var sArchivo: String ): Boolean;
      function GetStatusEquipo( const unidad: string ): TStatusEquipo;
      function GetStatusEquipoStr( const unidad: string ): string;
      //procedure Acknowledge( const Tipo: eTerminalType; CommData: Pointer; LetterWord: Word );
      procedure CommunicationDelay( const iTerminal: Integer );
      procedure Esperar( const eTipo: eTerminalType ); overload;
      procedure Esperar( _delayms : integer); overload;
      procedure ManejaError(const sMensaje: String; Error: Exception); overload;
      procedure ManejaError(const sMensaje: String); overload;
      procedure Mensaje(const sMensaje: String);
      procedure SetData(const Value: String);
    protected
      { Protected declarations }
      property Data: String read FData write SetData;
      procedure DoFeedBack( const iTerminal: Integer; sMsg: String );
      procedure DoGaugeSetSize( const sMsg: String; iSize: Integer );
      procedure DoGaugeAdvance( const sMsg: String; iStep: Integer );
      procedure DoGaugeFinish( const sMsg: String; iValue: Integer );
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      property TerminalID: Integer read GetTerminalID;
      property Terminal[ Index: Integer ]: TTerminal read GetTerminal;
      property OnError: TFeedBackError read FOnError write FOnError;
      property OnFeedBack: TFeedBackEvent read FOnFeedBack write FOnFeedBack;
      property OnGaugeSetSize: TGaugeEvent read FOnGaugeSetSize write FOnGaugeSetSize;
      property OnGaugeAdvance: TGaugeEvent read FOnGaugeAdvance write FOnGaugeAdvance;
      property OnGaugeFinish: TGaugeEvent read FOnGaugeFinish write FOnGaugeFinish;
      property OnMakeUnitList: TMakeUnitListEvent read FOnMakeUnitList write FOnMakeUnitList;
      property OnDoLectura: TLecturaEvent read FOnDoLectura write FOnDoLectura;
      property OnMessage: TFeedback read FOnMessage write FOnMessage;
      property ValidTokens: String read FValidTokens write FValidTokens;
      property OnPuedeLeerChecadas: TPuedeLeerChecadas read  FPuedeLeerChecadas write FPuedeLeerChecadas;

      property EsDemo : boolean read FEsDemo write FEsDemo;
      property NumeroSentinel : integer read FNumeroSentinel write FNumeroSentinel;

      function BorrarArchivo(const iTerminal: Integer; const sArchivo: String): Boolean;
      function DatosEnviados: String;
      function Ejecutar( const iTerminal: Integer; const sArchivo: String ): Boolean;
      function EnviarArchivoAlarma(const iTerminal: Integer; const sFileName: String): Boolean;
      function EnviarArchivoEmpleados(const iTerminal: Integer; const sFileName: String): Boolean;
      function GetInfo(const iUnit: Integer): String;
      function GetTerminalVersion(const iTerminal: Integer): String;
      function Init: Boolean;
      function IsUnitOnLine( const iTerminal: Integer ): Boolean;
      function RegistroLeido: String;
      function RegistroNormal: Boolean;
      function TransformarAlarma( var sArchivo: String ): Boolean;
      function TransformarEmpleado( var sArchivo: String ): Boolean;
      function Units: Integer;
      function SetChecadaInfo: Boolean;
      procedure Verify;
      procedure CargaDirectorio( const iTerminal: Integer; Archivos: TStrings );
      procedure MakeNodeList;

      //Funciones y procedimientos Thread Safe Locked  -->>>
      function Read: Boolean;
      function ProgramarTerminal( const iTerminal: Integer; const sDir1,sDir2,sDir3: String ): Boolean;
      procedure RebootUnit( const iTerminal: Integer );          // No aplica
      procedure ResetUnit( const iTerminal: Integer );
      function  ResetUnitFn(const iTerminal: Integer) : boolean;
      procedure SendAnyFile( const iTerminal: Integer; sFileName: String );
      procedure SendApplicationFile( const iTerminal: Integer; sFileName: String );
      procedure SendConfigFile( const iTerminal: Integer; sFileName: String );
      procedure SendEmployeeListFile( const iTerminal: Integer; sFileName: String );
      procedure SendParameterFile( const iTerminal: Integer; sFileName: String );
      procedure SetDateTime(const iTerminal: Integer; const dValue: TDateTime);
      procedure SetDateTimeAll( const dValue: TDateTime );
      procedure ShowVersion(const iTerminal: Integer);

      //<<<-----


      procedure Stop;
    end;

Function DLL_Init(Debug_comm_file, delet_file: word; Debug_File_Name: PChar ):  smallint StdCall; External K_ONLINE_DLL_FILE;
Function DLL_Free() : smallint StdCall; External K_ONLINE_DLL_FILE;
Function InitParam(recived_tcp_time_out, multi_thread: word) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SetTCPPort(Port: word; IP_address: PChar; Timeout, Retries: word): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SetTCPPortEx(Port: word; IP_address: PChar; Timeout, Retries, time_out_in_msec: word): smallint StdCall; External K_ONLINE_DLL_FILE;
Function CloseTCPPort(): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynGetStatus (Term_ID: word; status_buffer: PChar ): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynSetDateTime(Term_ID: word; date_buffer, time_buffer: PChar ): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynGetDataACK(Term_ID: word; data_buffer: PChar ): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynGetData(Term_ID: word; data_buffer: PChar ): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynGetDataClear(Term_ID: word; data_buffer: PChar ): smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynHalt(Term_ID: word) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynRun(Term_ID: word) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynDelAll(Term_ID: word) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynProg (Term_ID: word; tables_dir : PChar)  : smallint StdCall; External K_ONLINE_DLL_FILE;
Function Synprog_displayEx(Term_ID: word; tables_dir : PChar; x, y, width, height, display_type : word;  txt : PChar) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function Synprog_displayEx_ALL(Term_ID: word; t_file1, t_file2, t_file3 : PChar; x, y, width, height, display_type : word;  txt : PChar) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynOneTable (Term_ID: word; file_name : PChar)  : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynGetNetworkConfiguration(Term_ID: word; data_buffer: PChar  ): word StdCall; External K_ONLINE_DLL_FILE;
Function SynClear(Term_ID: word) : smallint StdCall; External K_ONLINE_DLL_FILE;
Function SynBufferReset(Term_ID: word) : smallint StdCall; External K_ONLINE_DLL_FILE;


implementation

uses ZetaAsciiFile,
     ZetaCommonClasses,
     ZetaCommonTools,
     RegExpr;
const
     K_DATE_FORMAT = 'dd/mm/yyyy';
     K_HOUR_FORMAT = 'hh:mm:ss';
type
    aPaquete = array [ 0..255 ] Of Byte;

{ ******** TOnlineMgr ******** }

constructor TOnlineMgr.Create;
begin
     FValidTokens := '';
     FPollData := TStringList.Create;
     FPollPtr := 0;
     InitializeCriticalSection(FLock);
     InitializeCriticalSection(FLockFuncionalidad);
     {$ifdef DEBUG}
     DLL_Init(1,0,'C:\TEMP\synel.log');
     {$else}
     DLL_Init(0,0,'');
     {$endif}
     InitParam(K_TRANSACTION_DELAY,0);
end;

destructor TOnlineMgr.Destroy;
begin
     FreeAndNil( FPollData );
     Stop;
     DeleteCriticalSection(FLock);
     DeleteCriticalSection(FLockFuncionalidad);
     DLL_Free();
     inherited Destroy;
end;

procedure TOnlineMgr.ManejaError( const sMensaje: String; Error: Exception );
begin
     if Assigned( FOnError ) then
        FOnError( sMensaje, Error );
end;

procedure TOnlineMgr.Mensaje(const sMensaje: String);
begin
     if Assigned( FOnMessage ) then
        FOnMessage( sMensaje );
end;

function TOnlineMgr.GetReplyMsg( const iResult: smallint ): String;
begin
     case iResult of
          CmdOK: Result := 'OK';

          CmdDataOK: Result := 'Data collection succeeded';
          CmdEOT: Result := 'End of Terminal';
          CmdEOF: Result := 'End of File';
          CmdACK: Result := 'Acknowledge';
          CmdNAK: Result := 'Not acknowledge';
          CmdYES: Result := 'Yes';
          CmdNO: Result := 'No';
          CmdUSE: Result := 'File in use';
          CmdNONE: Result := 'None';
          CmdErr: Result := 'Error';
          CmdErrLen: Result := 'Error Length';
          CmdErrTime: Result := 'Error Time';
          CmdErrSocket: Result := 'Error Socket';

          ERR_GENERAL           : Result := 'Error general ';
          ERR_Exception         : Result := 'Excepci�n desconocida ';
          ERR_Busy              : Result := 'El puerto est� ocupado ';
          ERR_PortNotOpen       : Result := 'El puerto no est� abierto ';
          ERR_PortAllreadyOpen  : Result := 'Todo los puertos est�n abiertos ';
          ERR_CanNotOpenPort    : Result := 'No puede abrir el puerto ';
          ERR_PortDisConnect    : Result := 'Puerto disconectado ';
          ERR_TimeOut           : Result := 'Timeout (Tiempo de espera excedido) ';
          ERR_DataOnTerminal    : Result := 'Existe informaci�n en la terminal ';
          DataAAlreadyWrite     : Result := 'La informaci�n ya se encuentra almacenada ';

     else
         Result := Format( 'Unknown %d', [ iResult ] );
     end;
end;

function TOnlineMgr.Check( const iResult: smallint ): Boolean;
begin
     Result := ( iResult = CmdOK );
     FStatus := GetReplyMsg( iResult );
end;

function TOnlineMgr.CheckData( const iResult: smallint ): Boolean;
begin
     Result := ( iResult >= CmdDataOK );

     if iResult = CmdDataNoRecv then
        FStatus :=  'No data was received'
     else
          FStatus := GetReplyMsg( iResult );
end;


procedure TOnlineMgr.SetData(const Value: String);
{$ifdef FALSE}
const
     K_GAFETE_LENGTH = 10;
     K_GAFETE_FORMAT = '%10.10d';
var
   iPos, iValue: Integer;
{$endif}
begin
     FData := Trim( Value );
     {$ifdef FALSE}
     iPos := Pos( TOKEN_PROXIMIDAD, FData );
     if ( iPos > 0 ) then
     begin
          iValue := StrToIntDef( '$' + Trim( Copy( FData, iPos + 1, K_GAFETE_LENGTH ) ), 0 );
          FData := Copy( FData, 1, iPos ) + Format( K_GAFETE_FORMAT, [ iValue ] ) + Copy( FData, iPos + K_GAFETE_LENGTH + 1, Length( FData ) - ( iPos + K_GAFETE_LENGTH ) );
     end;
     {$endif}
end;

procedure TOnlineMgr.DoFeedBack( const iTerminal: Integer; sMsg: String );
begin
     if Assigned( FOnFeedBack ) then
        FOnFeedBack( sMsg, iTerminal );
end;

procedure TOnlineMgr.DoGaugeSetSize( const sMsg: String; iSize: Integer );
begin
     if Assigned( FOnGaugeSetSize ) then
        FOnGaugeSetSize( sMsg, iSize );
end;

procedure TOnlineMgr.DoGaugeAdvance( const sMsg: String; iStep: Integer );
begin
     if Assigned( FOnGaugeAdvance ) then
        FOnGaugeAdvance( sMsg, iStep );
end;

procedure TOnlineMgr.DoGaugeFinish( const sMsg: String; iValue: Integer );
begin
     if Assigned( FOnGaugeFinish ) then
        FOnGaugeFinish( sMsg, iValue );
end;


//AV NO NEED
procedure TOnlineMgr.CommunicationDelay( const iTerminal: Integer );
begin
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               with Terminal[ iTerminal ] do
               begin
                    {case Tipo of
                         //ttTCPIP: Online_Delay( CommData, K_MILISECONDS * TCPTimeOut );
                         ////ttSerial: Online_Delay( CommData, K_MILISECONDS * TimeOut );
                         ///ttModem: Online_Delay( CommData, K_MILISECONDS * TimeOut );
                    end;}
               end;
          end;
     end;
end;

function TOnlineMgr.CommunicationConnect( const iTerminal: Integer ): Boolean;
const
   K_COMMUNICATION_ERROR  = 'Error al Conectar terminal %s : %s';
var
   sIP :AnsiString;
   IP_Address : PAnsiChar;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin

               with Terminal[ iTerminal ] do
               begin
                    try
                       EnterCriticalSection(FLock);
                       //Cerrar Siempre la Conexion Anterior
                       try
                          CloseTCPPort();
                          //Esperar( K_ESPERA_CLOSE_TCP );
                       except
                             on Error: Exception do
                             begin
                                  Result := True;
                             end;
                       end;

                       sIP := Format( '%d.%d.%d.%d', [TCPAddress1,TCPAddress2,TCPAddress3,TCPAddress4]);
                       IP_Address := PAnsiChar( sIP );
                       try
                          try
                             Result := Check( SetTCPPortEx( TCPPort, IP_Address, TCPTimeOut, 2, 1 ) );
                             InitParam( BufferTimeOut, 0);
                             if ( not Result ) then
                             begin
                                 ManejaError( Format(K_COMMUNICATION_ERROR, [Identificador, FStatus]) );
                             end;

                          except
                             on ErrorSocket: ESocketError do
                             begin
                                 ManejaError( Format(K_COMMUNICATION_ERROR, [Identificador, ErrorSocket.Message]) );
                                 Result := False;
                             end;
                          end;
                       except
                             on Error: Exception do
                             begin
                                  ManejaError( Format(K_COMMUNICATION_ERROR, [Identificador, Error.Message]) );
                                  Result := False;
                             end;
                       end;
                       Esperar( Tipo );
                    finally
                           LeaveCriticalSection(FLock);
                    end;
               end;
          end;
     end;
end;


function TOnlineMgr.GetNetworkInfo( const iTerminal: Integer ): string;
const
{Ejemplo de retorno de la terminal: S1HNF0000000EE301A4DA0A0A0AEA0A0A0AFE0A0A0A6DFFFFFF00037340373400030000
 En este caso solo interesa el MAC Address
 Analizado :
          S 1 H N F 0 00 0 [000EE301A4DA] 0A0A0AEA 0A0A0AFE 0A0A0A6DF FFFFF00 03734 03734 00030 000
System command S

Multidrop ID 1-9

Hardware configuration H

Network N

Network card A/B/D/E/F

Polling mode and MAC sending mode 0-3 0 - No Network polling Network MAC send Polling mode and Network MAC send

Polling interval n in seconds

Network Protocol 0/1 0 - TCP/IP

[ MAC address in Hex
format xx.xx.xx.xx.xx.xx     ]

IP address xx.xx.xx.xx

Gateway xx.xx.xx.xx

IP remote address xx.xx.xx.xx

Subnet mask xx.xx.xx.xx

Terminal port port number

Remote port port number

Diconnect time in seconds
DHCP 0/1 0 - Off 1 - On
Network card Firmware version
CRC
Hex EOT
}

   K_GET_MAC_NET_EXPR = '.{9}(.{12}).+';
   K_GET_MAC_NET_ERROR  = 'Error al Obtener Info. de la terminal %s : %s';
var
   dataBuffer : array[0..1024] of AnsiChar;
   netExp : TRegExpr;
begin
     Result := VACIO;
     netExp := TRegExpr.Create;
     netExp.Expression := K_GET_MAC_NET_EXPR;

     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin

               with Terminal[ iTerminal ] do
               begin
                    try
                       EnterCriticalSection(FLock);

                       try
                          try
                             if  Check( SynGetNetworkConfiguration( StrToIntDef( Unidad, 0 ), dataBuffer) ) then
                             begin
                                 Result := dataBuffer;
                                 if netExp.Exec( Result ) then
                                 begin
                                    Result := netExp.Match[1];
                                    Terminal[ iTerminal ].MACAddress := Result;
                                 end
                                 else
                                     Result := VACIO;
                             end
                             else
                             begin
                                  Result := VACIO;
                                  ManejaError( Format(K_GET_MAC_NET_ERROR, [Identificador, FStatus]) );
                             end;
                          except
                             on ErrorSocket: ESocketError do
                             begin
                                  Result := VACIO;
                                  ManejaError( Format(K_GET_MAC_NET_ERROR, [Identificador, ErrorSocket.Message]) );
                             end;
                          end;
                       except
                             on Error: Exception do
                             begin
                                  Result := VACIO;
                                  ManejaError( Format(K_GET_MAC_NET_ERROR, [Identificador, Error.Message]) );
                             end;
                       end;
                       Esperar( Tipo );
                    finally
                           LeaveCriticalSection(FLock);
                    end;
               end;
          end;
     end;

     FreeAndNil(netExp);
end;

function TOnlineMgr.CommunicationStart( const iTerminal: Integer ): Boolean;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               with Terminal[ iTerminal ] do
               begin
                    if IsConnected then
                    begin
                         //OK
                        Result := CommunicationConnect( iTerminal );
                    end;
               end;
          end;
     end;
end;

function TOnlineMgr.CommunicationEnd( const iTerminal: Integer ): Boolean;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal >= 0 ) and ( iTerminal < Count ) then
          begin
               with Terminal[ iTerminal ] do
               begin
                    EnterCriticalSection(FLock);
                    try
                       if IsConnected then
                       begin
                            try
                               CloseTCPPort();
                            except
                                  on Error: Exception do
                                  begin
                                       Result := True;
                                  end;
                            end;

                            Esperar( K_ESPERA_CLOSE_TCP );
                            Result := True;
                       end;
                    finally
                       LeaveCriticalSection(FLock);
                    end;
               end;
          end;
     end;
end;

procedure TOnlineMgr.Esperar( const eTipo: eTerminalType );
const
     K_200_MILISEGUNDOS = 200;
begin
     case eTipo of
          ttTCPIP: Windows.Sleep( K_200_MILISEGUNDOS );
     end;
end;

function TOnlineMgr.Init: Boolean;
const
     K_DELAY = '01';
var
   i: Integer;
   lOK: Boolean;
begin
     FUnits := 0;
     FPollData.Clear;
     {$ifdef FALSE}
     FPollData.Add( '1=000A000000@~00007B02DE031014110' );
     {$endif}
     FPollPtr := 0;
     Result := True;
     try
        with IniValues.ListaTerminales do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  with Terminal[ i ] do
                  begin
                       case Tipo of
                            ttTCPIP:
                            begin
                                 lOK := TRUE; //TODO  Check Status  Modificar

                            end;
                            ttSerial:
                            begin
                                 lOk := FALSE;//Siempre sera falso ( solo maneja TCP/IP)
                            end;
                       else
                           lOk := False;
                       end;

                       Result := lOK;

                       if lOk then
                       begin
                            CommunicationDelay( i );
                            IsConnected := CommunicationConnect( i );

                            GetNetworkInfo(i);


                            CommunicationEnd( i );
                       end;
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                ManejaError( 'Error Al Iniciar Recolecci�n', Error );
                Result := False;
           end;
     end;
end;

procedure TOnlineMgr.Stop;
var
   i: Integer;
begin
    EnterCriticalSection(FLock);
    try
        try
           with IniValues.ListaTerminales do
           begin
                for i := 0 to ( Count - 1 ) do
                begin
                     with Terminal[ i ] do
                     begin
                          if IsConnected then
                          begin
                               IsConnected := False;
                               CommData := nil;
                          end;
                     end;
                end;
           end;
        except
              on Error: Exception do
              begin
                   ManejaError( 'Error Al Terminar Recolecci�n', Error );
              end;
        end;
        FUnits := 0;
       

       try
          CloseTCPPort();
          Esperar(K_ESPERA_CLOSE_TCP);
       except
             on Error: Exception do
             begin
             end;
       end;

    finally
       LeaveCriticalSection(FLock);
    end;


end;

function TOnlineMgr.Units: Integer;
begin
     Result := IniValues.ListaTerminales.Count;
end;

function TOnlineMgr.GetInfo( const iUnit: Integer ): String;
begin
     with IniValues.ListaTerminales do
     begin
          if ( iUnit > 0 ) and ( iUnit <= Count ) then
          begin
               Result := Terminal[ iUnit - 1 ].GetInfo;
          end;
     end;
end;

procedure TOnlineMgr.MakeNodeList;
var
   iTerminals: Integer;
begin
     iTerminals := IniValues.ListaTerminales.Count;
     if ( FUnits <> iTerminals ) then
     begin
          FUnits := iTerminals;
          if Assigned( FOnMakeUnitList ) then
             FOnMakeUnitList;
     end;
end;

//private
function TOnlineMgr.GetStatusEquipo( const unidad: string ): TStatusEquipo;
var
   status_buffer : array[0..1000] of AnsiChar;
   status_string : string;
begin
   Result := nil;

   EnterCriticalSection(FLock);
   try
      if Check(SynGetStatus( strtointdef( unidad, 0) , status_buffer )) then
      begin
           status_string := Copy( status_buffer, 0, length(status_buffer));
           Result := TStatusEquipo.Create( status_string );
      end;
   finally
       LeaveCriticalSection(FLock);
   end;
end;

function TOnlineMgr.GetStatusEquipoStr( const unidad: string ): string;
var
   status_buffer : array[0..1000] of AnsiChar;
begin
   Result := VACIO;

   EnterCriticalSection(FLock);
   try
      if Check(SynGetStatus( strtointdef( unidad, 0) , status_buffer )) then
      begin
           Result := Copy( status_buffer, 0, length(status_buffer));
      end;
   finally
       LeaveCriticalSection(FLock);
   end;
end;


function TOnlineMgr.IsUnitOnLine( const iTerminal: Integer ): Boolean;
const
     K_LETRA_A = 64;
     K_TOPE = 10;
var
   oStatusEquipo :  TStatusEquipo;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin


                         oStatusEquipo := GetStatusEquipo(Unidad) ;
                         if ( oStatusEquipo <> nil ) then
                         begin
                            Result := oStatusEquipo.FOk;
                            FreeAndNil( oStatusEquipo );
                         end;

                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

procedure TOnlineMgr.ResetUnit( const iTerminal: Integer );
begin
     ResetUnitFn( iTerminal );
end;

function TOnlineMgr.ResetUnitFn( const iTerminal: Integer ) : boolean;

  function HaltRun( const unidad: string ) : boolean;
  var
     termID : integer;
  begin
       Result := False;
       try
          termID := StrToIntDef( unidad, 0 ) ;
          if  Check( SynHalt(termID)) then
          begin
              Esperar(ttTCPIP );
              Esperar(5000);
              Result := Check( SynRUN(termID) );
              Esperar(ttTCPIP );
          end;
       except
           on Error: Exception do
           begin
                ManejaError( 'Error Reiniciar la Unidad', Error );
                Result := False;
           end;
       end;
  end;
var
   sMensaje: String;
begin
    Result := False;
    EnterCriticalSection(FLockFuncionalidad);
    try
       with IniValues.ListaTerminales do
       begin
            if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
            begin
                 CommunicationStart( iTerminal - 1 );
                 with Terminal[ iTerminal - 1 ] do
                 begin
                      if IsConnected then
                      begin
                           //AV-TODO  Resetear la Unidad

                           if HaltRun( Unidad ) then
                           begin
                                sMensaje := 'Ha Sido Reseteada';
                                IsConnected := CommunicationConnect( iTerminal - 1 );
                                Result := IsConnected;
                                Dec( FUnits );
                           end
                           else
                               sMensaje := Format( 'No Pudo Ser Reseteada ( %s )', [ FStatus ] );


                           DoFeedBack( iTerminal, sMensaje );
                      end;
                 end;
                 CommunicationEnd( iTerminal - 1 );
            end;
       end;
    finally
           LeaveCriticalSection(FLockFuncionalidad);
    end;
end;

procedure TOnlineMgr.RebootUnit( const iTerminal: Integer );
var
   sMensaje: String;
begin
    EnterCriticalSection(FLockFuncionalidad);
    try
       with IniValues.ListaTerminales do
       begin
            if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
            begin
                 CommunicationStart( iTerminal - 1 );
                 with Terminal[ iTerminal - 1 ] do
                 begin
                      if IsConnected then
                      begin
                           sMensaje := 'No existe esa funcionalidad para este equipo' ;
                           DoFeedBack( iTerminal, sMensaje );
                      end;
                 end;
                 CommunicationEnd( iTerminal - 1 );
            end;
       end;
   finally
     LeaveCriticalSection(FLockFuncionalidad);
   end;
end;

procedure TOnlineMgr.SetDateTimeAll( const dValue: TDateTime );
var
   i: Integer;
begin
   EnterCriticalSection(FLockFuncionalidad);
   try
     with IniValues.ListaTerminales do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               CommunicationStart( i );
               with Terminal[ i ] do
               begin
                    if IsConnected then
                    begin
                         //Realiza limpieza de Buffer
                         {if not EsDemo then
                            Check( SynClear(StrToIntDef(Unidad,0)) );}

                         if not Check( SynSetDateTime( StrToIntDef(Unidad,0), pChar( FormatDateTime( K_DATE_FORMAT, dValue ) ), pChar( FormatDateTime( K_HOUR_FORMAT, dValue ) ) ) ) then
                         begin
                              Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Ser Sincronizada ( %s )', [ i, Self.GetInfo( i ), FStatus ] ) );
                         end;
                    end;
               end;
               CommunicationEnd( i );
          end;
     end;
   finally
     LeaveCriticalSection(FLockFuncionalidad);
   end;
end;



procedure TOnlineMgr.SetDateTime( const iTerminal: Integer; const dValue: TDateTime );
begin
   EnterCriticalSection(FLockFuncionalidad);
   try
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         if not Check( SynSetDateTime( StrToIntDef(Unidad,0), pChar( FormatDateTime( K_DATE_FORMAT, dValue ) ), pChar( FormatDateTime( K_HOUR_FORMAT, dValue ) ) ) ) then
                         begin
                              Mensaje( Format( 'La Terminal %d ( %s ) NO Pudo Ser Sincronizada ( %s )', [ iTerminal, Self.GetInfo( iTerminal ), FStatus ] ) );
                         end;
                    end;

               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
   finally
     LeaveCriticalSection(FLockFuncionalidad);
   end;
end;

function TOnlineMgr.GetRunningProgram( const iTerminal: Integer; var sPrograma: String ): Boolean;
const
     K_LEN = 128;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         sPrograma := 'N/A';
                         {AV  TODO  Get Programa con SyNEL ?
                         iResult := Online_ESC_I( CommData, LetterWord, @pData[ 1 ], iLen );
                         if ( iResult = CmdNONE ) then
                            sPrograma := 'Ninguno'
                         else
                             if Check( iResult ) then
                             begin
                                  pData[ 0 ] := iLen;
                                  pData[ iLen + 1] := 0;
                                  pMsg := @pData[ 1 ];
                                  sPrograma := pMsg;
                                  Result := True;
                             end;
                             }
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.GetTerminalVersion(const iTerminal: Integer): String;
const
     K_UNKNOWN = '???';
var
   sVersion, sID, sMemory, sPrograma, sStatus: String;

   {$ifdef ANTES}
   oStatusEquipo :  TStatusEquipo;
   {$endif}

begin
     Result := K_UNKNOWN;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         {$ifdef ANTES}
                         oStatusEquipo := GetStatusEquipo( Unidad );
                         if (oStatusEquipo <> nil) and (oStatusEquipo.FOk)  then
                         begin
                              sVersion := oStatusEquipo.VERSION_NO;
                              sID      := oStatusEquipo.STATION_ID;
                              sMemory  := oStatusEquipo.MEMORY_SIZE_USED;
                         end
                         else
                         begin
                             sVersion := K_UNKNOWN;
                             sID      := K_UNKNOWN;
                             sMemory  := K_UNKNOWN;
                         end;
                         {$else}
                         sStatus := GetStatusEquipoStr( Unidad );
                         if strLleno( sStatus )  then
                         begin
                              sVersion := copy( sStatus, 4, 1 );
                              sID      := Copy( sStatus, 5, 5 );
                              sMemory  := Copy( sStatus, 33, 5 );
                         end
                         else
                         begin
                             sVersion := K_UNKNOWN;
                             sID      := K_UNKNOWN;
                             sMemory  := K_UNKNOWN;
                         end;
                         {$endif}
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
               if not GetRunningProgram( iTerminal, sPrograma ) then
                  sPrograma := K_UNKNOWN;
               Result := Format( '%s ( ID = %s ) Ejecuta %s ( Memoria: %s )', [ sVersion, sID, sPrograma, sMemory ] );
          end;
     end;
end;

procedure TOnlineMgr.ShowVersion( const iTerminal: Integer );
begin
   EnterCriticalSection(FLockFuncionalidad);
   try
     DoFeedBack( iTerminal, GetTerminalVersion( iTerminal ) );
   finally
      LeaveCriticalSection(FLockFuncionalidad);
   end;
end;

function TOnlineMgr.DatosEnviados: String;
begin
     Result := FData;
end;

function TOnlineMgr.RegistroLeido: String;
begin
     Result := DatosEnviados;
end;

function TOnlineMgr.RegistroNormal: Boolean;
begin
//     Result := True;  // ER: Siempre regresa True para dejarlas en archivo
     Result := ( Pos( FValidtokens, FData ) > 0 );
end;

function TOnlineMgr.SetChecadaInfo: Boolean;
var
   sData, sTerminal: String;
begin
     Result := ( FPollData.Count > 0 );
     if Result then
     begin
          with FPollData do
          begin
               sTerminal := Names[ 0 ];
               sData := Values[ sTerminal ];
               FTerminal := StrToIntDef( sTerminal, 0 );
               if Assigned( Terminal[ FTerminal ] ) then
               begin
                    with Terminal[ FTerminal ] do
                    begin
                         if ZetaCommonTools.StrLleno( Identificador ) then
                         begin
                              sData := Format( '%-' + Format( '%0:d.%0:d', [ K_ID_LEN ] ) + 's%s', [ Identificador, Copy( sData, K_ID_LEN + 1, ( Length( sData ) - K_ID_LEN ) ) ] );
                         end;
                    end;
               end;
               Data := sData;
               Delete( 0 );
          end;
     end
end;

function TOnlineMgr.Read: Boolean;
const

//FIXED (TerminalID)(DSvF)(EPROM)(Date)Checadas
  K_HEAD_EXPR = 'd(.)(.)(.{5})(\d{4})(\d{2})(\d{2})(.+)';


//CHAR, I/O/C, TIPO Terminal (?) ,  Badge No,  (H=Barcode o M=Proximidad)   HH MM SS
//A        1        00               010772A H 15 36 39
  K_ITEMS_EXPR = '([A-Z])(\d)(\d{2})(.{11})([H|M])(\d{2})(\d{2})(\d{2})';

  K_TIPO_BARCODE = 'H';
  K_TIPO_PROXIMIDAD = 'M';

  K_INDICADOR_PROXIMIDAD = '~';

var
   HeadExpr,ItemsExpr : TRegExpr;
   sTipoFecha, sTerminalID, sEPROM, sChecadas : string;

   sTipo, sCaracter, sCheckInOut, sBadgeNo, sTipoLectura, sYear, sMonth, sDay, sHH, sMM  : string;
   iBadgeNo : integer;
   lMensajeNoAuth : boolean;

   aData: aPaquete;
   aDataStr: String[ 255 ] absolute aData;
   sData, sOldData, sDataLectura: String;

   dataBuffer : array[0..2096] of AnsiChar;

   termAuth : eTerminalAuth;
   {$ifdef PROFILE}
   o : TZetaProfiler;
  {$endif}

  function PuedeLeerChecadas : boolean;
  begin
       OnPuedeLeerChecadas( Result );
  end;

  function PasoTopeChecadas( lecturas : integer) : boolean;
  begin
       Result := EsDemo and  (lecturas >= TOPE_DEMO);
  end;

   procedure IniciarRecoleccion( unidad : string);
   begin
        try
          try
             CheckData( SynBufferReset( StrToIntDef( unidad, 0 ) ) );
          except
             on ErrorSocket: ESocketError do
             begin
             end;
          end;
       except
             on Error: Exception do
             begin
             end;
       end;
   end;


   function GetData( unidad : string)   : string;
   begin
       Result := VACIO;
       try
          try
             if  CheckData( SynGetDataClear( StrToIntDef( unidad, 0 ), dataBuffer) ) then
             begin
                 Result := dataBuffer;

             end;
          except
             on ErrorSocket: ESocketError do
             begin
                  Result := VACIO;
             end;
          end;
       except
             on Error: Exception do
             begin
                  Result := VACIO;
             end;
       end;
   end;


   function LimpiarBufferUnidad( unidad : string)   : boolean;
   begin
       Result := False;
       try
          try
             if  CheckData( SynClear( StrToIntDef( unidad, 0 ) ) ) then
             begin
                 Result := True;

             end;
          except
             on ErrorSocket: ESocketError do
             begin
                  Result := False;
             end;
          end;
       except
             on Error: Exception do
             begin
                  Result := False;
             end;
       end;

   end;





begin

{$ifdef PROFILE}   o :=  TZetaProfiler.Create;
   o.Start;
   Randomize;
{$endif}

if ( Units = 0 ) then
begin
   Result := False;
end
else
begin
{$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  EnterCriticalSection'); {$endif}
    EnterCriticalSection(FLockFuncionalidad);
{$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  EnterCriticalSection'); {$endif}
         FTerminal := -1;
         Data := '';
         Result := SetChecadaInfo;
         if ( not Result ) then
         begin
              HeadExpr := TRegExpr.Create;
              HeadExpr.Expression := K_HEAD_EXPR;
              ItemsExpr := TRegExpr.Create;
              ItemsExpr.Expression := K_ITEMS_EXPR;
              try
                 try
                    with IniValues.ListaTerminales do
                    begin
                         if ( FPollPtr >= Count ) then
                            FPollPtr := 0;

                         {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  CommunicationStart'); {$endif}
                         CommunicationStart( FPollPtr );
                         {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  CommunicationStart'); {$endif}
                         with Terminal[ FPollPtr ] do
                         begin
                             if IsConnected then
                             begin
                                {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  RevisoAutorizacion'); {$endif}
                                 lMensajeNoAuth :=  ( not Terminal[ FPollPtr ].RevisoAutorizacion );
                                 termAuth := Terminal[ FPollPtr ].IsAuthorized( NumeroSentinel );
                                {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  RevisoAutorizacion'); {$endif}

                                 //El tope de Checadas lo hace FSynelPoll
                                 if EsDemo then
                                    termAuth := termAuthorized;

                                 if ( termAuth = termNoAuthorized ) then
                                 begin
                                     if lMensajeNoAuth then
                                          Mensaje( Format( 'La Terminal %s NO est� Autorizada para lecturas' , [GetInfo]));
                                 end
                                 else
                                 if ( termAuth = termNoAuthorizedSentinel ) then
                                 begin
                                    if lMensajeNoAuth then
                                          Mensaje( Format( 'La Terminal %s NO est� Autorizada para lecturas con el Sentinel %d' , [GetInfo, NumeroSentinel]));
                                 end
                                 else
                                 if ( termAuth =  termAuthorized ) then
                                 begin
                                    {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  GetData'); {$endif}

                                      IniciarRecoleccion( Unidad );
                                      while PuedeLeerChecadas and ( not PasoTopeChecadas( Lecturas ) ) and  HeadExpr.Exec( GetData( Unidad ) ) do
                                      begin
                                            sTerminalID := HeadExpr.Match[1];
                                            sTipoFecha := HeadExpr.Match[2];
                                            sEPROM := HeadExpr.Match[3];
                                            sYear := HeadExpr.Match[4];
                                            sMonth := HeadExpr.Match[5];
                                            sDay := HeadExpr.Match[6];
                                            sChecadas := HeadExpr.Match[7];
                                            if ItemsExpr.Exec( sChecadas) then
                                            begin
                                                 repeat
                                                     //'1=([A-Z])2=(\d)3=(\d{2})4=(.{11})5=([H|M])6=(\d{2})7=(\d{2})8=(\d{2})';
                                                      sCaracter := ItemsExpr.Match[1];
                                                      sCheckInOut :=ItemsExpr.Match[2];
                                                      sTipo :=ItemsExpr.Match[3];
                                                      sBadgeNo  := Trim( ItemsExpr.Match[4]);
                                                      sTipoLectura := ItemsExpr.Match[5];

                                                      sHH  := ItemsExpr.Match[6];
                                                      sMM  := ItemsExpr.Match[7];
                                                      if ( length( sBadgeNo ) > 0 ) then
                                                      begin

                                                         if ( sTipoLectura = K_TIPO_BARCODE ) then
                                                         begin
                                                            if not UseCredentialsDefs then
                                                               sBadgeNo :=  sBadgeNo[1] + ZetaCommonTools.PadLCar( Copy( sBadgeNo, 2, length( sBadgeNo ) - 2), 9,'0')  + sBadgeNo[ length( sBadgeNo ) ]
                                                            else
                                                            begin
                                                               if strVacio( Digit ) then
                                                                sBadgeNo :=  sBadgeNo[1] +ZetaCommonTools.PadLCar( Copy( sBadgeNo, 1, length( sBadgeNo ) - 1), 9,'0')  + Letter
                                                               else
                                                                   sBadgeNo :=  Digit +ZetaCommonTools.PadLCar( sBadgeNo, 9,'0')  + Letter ;

                                                            end;
                                                         end
                                                         else
                                                         begin
                                                               iBadgeNo := StrToIntDef( sBadgeNo, 0);
                                                               sBadgeNo := K_INDICADOR_PROXIMIDAD + ZetaCommonTools.PadLCar( IntToHex( iBadgeNo, 6 ), 10,'0');

                                                         end;
                                                         //123456 XX YY @ E 123456789 C MM DD HH NN I
                                                         //Terminal[ FPollPtr ].TCPAddress4
                                                         sData := format( '%s000000@%s%s%s%s%s%s',
                                                                                [ ZetaCommonTools.PadLCar( Identificador, 4, '0' ),
                                                                                  sBadgeNo,
                                                                                  ZetaCommonTools.PadLCar( sMonth, 2, '0' ),
                                                                                  ZetaCommonTools.PadLCar( sDay, 2, '0' ),
                                                                                  ZetaCommonTools.PadLCar( sHH, 2, '0' ),
                                                                                  ZetaCommonTools.PadLCar( sMM, 2, '0' ),
                                                                                  sCheckInOut] );
                                                      end;
                                                      if ( sData <> sOldData ) then
                                                      begin
                                                           sOldData := sData;
                                                           FPollData.Add( Format( '%d=%s', [ FPollPtr + 1, sData ] ) );
                                                           //Mensaje( Format( 'FPollData.Count: %d', [ FPollData.Count ] ) );
                                                           if Assigned( OnDoLectura ) then
                                                           begin
                                                                sDataLectura := sData;
                                                                if ZetaCommonTools.StrLleno( Identificador ) then
                                                                   sDataLectura := Format( '%-' + Format( '%0:d.%0:d', [ K_ID_LEN ] ) + 's%s', [ Identificador, Copy( sDataLectura, K_ID_LEN + 1, ( Length( sDataLectura ) - K_ID_LEN ) ) ] );
                                                                {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  OnDoLectura'); {$endif}
                                                                OnDoLectura( Trim( sDataLectura ) );
                                                                Lecturas := Lecturas + 1;
                                                                {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  OnDoLectura'); {$endif}
                                                           end;
                                                      end;
                                                 until not ItemsExpr.ExecNext;
                                            end;  //ItemsExpr
                                       end; //while HeadExpr
                                      {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  GetData'); {$endif}

                                      {$ifdef FALSE}
                                      FreeandNil( oChecadas );
                                      {$endif}

                                      {if not EsDemo then
                                         LimpiarBufferUnidad( Unidad );}
                                 end; //if ( termAuth
                                 //Lecturas := Lecturas + FPollData.Count;
                             end;//if IsConnected
                            end;//with Terminal
                            {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  CommunicationEnd'); {$endif}
                            CommunicationEnd( FPollPtr );
                            Sleep(1000);
                            {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  CommunicationEnd'); {$endif}
                         end;//with IniValues.ListaTerminales
                         Inc( FPollPtr );
                 except
                       on Error: Exception do
                       begin
                            ManejaError( 'Error Al Recolectar', Error );
                       end;
                 end;
                 Result := SetChecadaInfo;  // Si en la recolecci�n hubo checadas prepara la primera
              finally
                  FreeAndNil( HeadExpr );
                  FreeAndNil( ItemsExpr );
                  {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  LeaveCriticalSection'); {$endif}
                  LeaveCriticalSection(FLockFuncionalidad);
                  {$ifdef PROFILE}o.Check('TTOnlineMgr.Read >>  LeaveCriticalSection'); {$endif}
              end;
         end;

    {$ifdef PROFILE}o.Report( Format( 'C:\TEMP\PROFILE_OnlinePoll_READ_%d_%d.LOG', [Random(1000),Random(100000)]) );
     FreeAndNil( o );
    {$endif}
end;

end;

function TOnlineMgr.GetTerminalID: Integer;
begin
     Result := FTerminal;
end;

function TOnlineMgr.GetTerminal(Index: Integer): TTerminal;
begin
     with IniValues.ListaTerminales do
     begin
          if ( Index > 0 ) and ( Index <= Count ) then
          begin
               Result := Terminal[ Index - 1 ];
          end
          else
              Result := nil;
     end;
end;


{AV: Que hace esta funcion ?? }
procedure TOnlineMgr.CargaDirectorio( const iTerminal: Integer; Archivos: TStrings );
var
   aName: array [ 0..15 ] of Byte;
   aNameStr: String[ 15 ] absolute aName;
begin
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         with Archivos do
                         begin
                              Clear;
{AV: TODO Averguar si carga los archivos de poleo o de configuraci�n??
                                 try
                                 BeginUpdate;
                                 iResult := Online_FindFirst( CommData, LetterWord, @FindStructure, @aName[ 1 ], iLen );
                                 while Check( iResult ) and ( iLen > 0 ) do
                                 begin
                                      aName[ 0 ] := iLen;
                                      Add( aNameStr );
                                      iResult := Online_FindNext( CommData, LetterWord, @FindStructure, @aName[ 1 ], iLen );
                                 end
                              finally
                                     EndUpdate;
                              end;}
                         end;
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.BorrarArchivo( const iTerminal: Integer; const sArchivo: String ): Boolean;
begin
     Result := False;
     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         {iLen := Length( sArchivo );}
                         {AV : TODO Borrar Archivo en la terminal????
                         Result := Yes( Online_ESC_E( CommData, LetterWord, @sArchivo[ 1 ], iLen ) );}
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;
end;

function TOnlineMgr.Ejecutar( const iTerminal: Integer; const sArchivo: String ): Boolean;
const
     K_EXE_SUFIX = '.EXE';
begin
     Result := False;
{     with IniValues.ListaTerminales do
     begin
          if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
          begin
               CommunicationStart( iTerminal - 1 );
               with Terminal[ iTerminal - 1 ] do
               begin
                    if IsConnected then
                    begin
                         sPrograma := sArchivo;
                         iLen := Pos( K_EXE_SUFIX, sPrograma );
                         if ( iLen > 0 ) then
                            sPrograma := Copy( sPrograma, 1, ( iLen - 1 ) );
                         iLen := Length( sPrograma );
                       ///AV : Creo que no aplica para Synel   Result := Yes( Online_ESC_X( CommData, LetterWord, @sPrograma[ 1 ], iLen ) );
                         if Result then
                            DoFeedBack( iTerminal, Format( 'Ejecuta %s', [ sPrograma ] ) );
                    end;
               end;
               CommunicationEnd( iTerminal - 1 );
          end;
     end;}
end;

function TOnlineMgr.ChangeFileNameToLocalDAT( const sArchivo: String ): String;
begin
     Result := ExtractFilePath( Application.ExeName ) + ExtractFileName( ChangeFileExt( sArchivo, '.dat' ) );
end;

function TOnlineMgr.TransformarAlarma( var sArchivo: String ): Boolean;
const
     K_COMENTARIO = '#';
     K_SEPARADOR = '|';
type
    TAlarma = record
       Hora: array[ 1..6 ] of Char;
       Modo: array[ 1..1 ] of Char;
    end;
var
   FEntrada: TAsciiServer;
   rAlarma: TAlarma;
   FSalida: file of TAlarma;
   sHora, sModo, sDatos, sSalida: String;
   i, iPos: Integer;
begin
     Result := False;
     if FileExists( sArchivo ) then
     begin
          try
             sSalida := ChangeFileNameToLocalDAT( sArchivo );
             FEntrada := TAsciiServer.Create;
             try
                AssignFile( FSalida, sSalida );
                try
                   Rewrite( FSalida );
                   with FEntrada do
                   begin
                        if Open( sArchivo ) then
                        begin
                             repeat
                                  sDatos := Trim( Data );
                                  if ZetaCommonTools.StrLleno( sDatos ) and ( sDatos[ 1 ] <> K_COMENTARIO ) then
                                  begin
                                       iPos := Pos( K_SEPARADOR, sDatos );
                                       if ( iPos > 0 ) then
                                       begin
                                            sHora := Trim( Copy( sDatos, 1, ( iPos - 1 ) ) );
                                            sModo := Trim( Copy( sDatos, ( iPos + 1 ), 1 ) );
                                            if ZetaCommonTools.StrLleno( sHora ) and ZetaCommonTools.StrLleno( sModo ) then
                                            begin
                                                 with rAlarma do
                                                 begin
                                                      for i := Low( Hora ) to High( Hora ) do
                                                      begin
                                                           if ( i > Length( sHora ) ) then
                                                              Hora[ i ] := Chr( 0 )
                                                           else
                                                               Hora[ i ] := sHora[ i ];
                                                      end;
                                                      Modo[ 1 ] := sModo[ 1 ];
                                                 end;
                                                 Write( FSalida, rAlarma );
                                            end;
                                       end;
                                  end;
                             until not Read;
                        end;
                        Close;
                   end;
                   Result := FileExists( sSalida );
                   if Result then
                      sArchivo := sSalida;
                finally
                       CloseFile( FSalida );
                end;
             finally
                    FreeAndNil( FEntrada );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Transformar Archivo De Alarmas', Error );
                end;
          end;
     end;
end;

{AV TODO :  Debe transformarse segun el formato de Synel , VERIFICAR SI APLICA!}
function TOnlineMgr.TransformarConfig( var sArchivo: String ): Boolean;
const
     K_COMENTARIO = '#';
     K_SEPARADOR = '=';
     K_STR_SIZE = 18;
type
    TConfig = record
       Nombre: array[ 1..K_STR_SIZE ] of Char;
       Valor: array[ 1..K_STR_SIZE ] of Char;
    end;
var
   FEntrada: TAsciiServer;
   rConfig: TConfig;
   FSalida: file of TConfig;
   sNombre, sValor, sDatos, sSalida: String;
   i, iPos: Integer;
begin
     Result := False;
     if FileExists( sArchivo ) then
     begin
          try
             sSalida := ChangeFileNameToLocalDAT( sArchivo );
             FEntrada := TAsciiServer.Create;
             try
                AssignFile( FSalida, sSalida );
                try
                   Rewrite( FSalida );
                   with FEntrada do
                   begin
                        if Open( sArchivo ) then
                        begin
                             repeat
                                  sDatos := Trim( Data );
                                  if ZetaCommonTools.StrLleno( sDatos ) and ( sDatos[ 1 ] <> K_COMENTARIO ) then
                                  begin
                                       iPos := Pos( K_SEPARADOR, sDatos );
                                       if ( iPos > 0 ) then
                                       begin
                                            sNombre := Trim( Copy( sDatos, 1, ( iPos - 1 ) ) );
                                            sValor := Trim( Copy( sDatos, ( iPos + 1 ), ( Length( sDatos ) - iPos ) ) );
                                            if ZetaCommonTools.StrLleno( sNombre ) and ZetaCommonTools.StrLleno( sValor ) then
                                            begin
                                                 with rConfig do
                                                 begin
                                                      for i := Low( Nombre ) to High( Valor ) do
                                                      begin
                                                           if ( i > Length( sNombre ) ) then
                                                              Nombre[ i ] := Chr( 0 )
                                                           else
                                                               Nombre[ i ] := sNombre[ i ];
                                                      end;
                                                      for i := Low( Valor ) to High( Valor ) do
                                                      begin
                                                           if ( i > Length( sValor ) ) then
                                                              Valor[ i ] := Chr( 0 )
                                                           else
                                                               Valor[ i ] := sValor[ i ];
                                                      end;
                                                 end;
                                                 Write( FSalida, rConfig );
                                            end;
                                       end;
                                  end;
                             until not Read;
                        end;
                        Close;
                   end;
                   Result := FileExists( sSalida );
                   if Result then
                      sArchivo := sSalida;
                finally
                       CloseFile( FSalida );
                end;
             finally
                    FreeAndNil( FEntrada );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Transformar Archivo De Cofiguraci�n', Error );
                end;
          end;
     end;
end;

{AV TODO :  Debe transformarse segun el formato de Synel }
function TOnlineMgr.TransformarEmpleado( var sArchivo: String ): Boolean;
const
     K_COMENTARIO = '#';
     K_SEPARADOR = '|';
     C_BADGE_SIZE = 11; { GA: � xq tengo 12 ? }
     C_APELLIDO_SIZE = 16;
     C_NOMBRE_SIZE = 16;
type
    TEmpleado = record
       Gafete: array[ 1..C_BADGE_SIZE ] of Char;
       Nombre: array[ 1..C_NOMBRE_SIZE ] of Char;
       Apellido: array[ 1..C_APELLIDO_SIZE ] of Char;
    end;
var
   FEntrada: TAsciiServer;
   rEmpleado: TEmpleado;
   FSalida: file of TEmpleado;
   sGafete, sApellido, sNombre, sDatos, sSalida: String;
   i, iPos: Integer;
begin
     Result := False;
     if FileExists( sArchivo ) then
     begin
          try
             sSalida := ChangeFileNameToLocalDAT( sArchivo );
             FEntrada := TAsciiServer.Create;
             try
                AssignFile( FSalida, sSalida );
                try
                   Rewrite( FSalida );
                   with FEntrada do
                   begin
                        if Open( sArchivo ) then
                        begin
                             repeat
                                  sDatos := Trim( Data );
                                  if ZetaCommonTools.StrLleno( sDatos ) and ( sDatos[ 1 ] <> K_COMENTARIO ) then
                                  begin
                                       sApellido := '';
                                       sNombre := '';
                                       iPos := Pos( K_SEPARADOR, sDatos );
                                       if ( iPos > 0 ) then
                                       begin
                                            sGafete := Copy( sDatos, 1, ( iPos - 1 ) );
                                            sDatos := Trim( Copy( sDatos, ( iPos + 1 ), ( Length( sDatos ) - iPos ) ) );
                                            iPos := Pos( K_SEPARADOR, sDatos );
                                            if ( iPos > 0 ) then
                                            begin
                                                 sNombre := Copy( sDatos, 1, ( iPos - 1 ) );
                                                 sApellido := Trim( Copy( sDatos, ( iPos + 1 ), ( Length( sDatos ) - iPos ) ) );
                                            end
                                            else
                                            begin
                                                 sNombre := sDatos;
                                            end;
                                       end
                                       else
                                       begin
                                            sGafete := sDatos;
                                       end;
                                       if ZetaCommonTools.StrLleno( sGafete ) then
                                       begin
                                            with rEmpleado do
                                            begin
                                                 for i := Low( Gafete ) to High( Gafete ) do
                                                 begin
                                                      if ( i > Length( sGafete ) ){$ifdef FALSE}or ( i >= High( Gafete ) ){$endif} then
                                                         Gafete[ i ] := Chr( 0 )
                                                      else
                                                          Gafete[ i ] := sGafete[ i ];
                                                 end;
                                                 for i := Low( Nombre ) to High( Nombre ) do
                                                 begin
                                                      if ( i > Length( sNombre ) ){$ifdef FALSE} or ( i >= High( Nombre ) ){$endif} then
                                                         Nombre[ i ] := Chr( 0 )
                                                      else
                                                          Nombre[ i ] := sNombre[ i ];
                                                 end;
                                                 for i := Low( Apellido ) to High( Apellido ) do
                                                 begin
                                                      if ( i > Length( sApellido ) ){$ifdef FALSE} or ( i >= High( Apellido ) ){$endif} then
                                                         Apellido[ i ] := Chr( 0 )
                                                      else
                                                          Apellido[ i ] := sApellido[ i ];
                                                 end;
                                            end;
                                            Write( FSalida, rEmpleado );
                                       end;
                                  end;
                             until not Read;
                        end;
                        Close;
                   end;
                   Result := FileExists( sSalida );
                   if Result then
                      sArchivo := sSalida;
                finally
                       CloseFile( FSalida );
                end;
             finally
                    FreeAndNil( FEntrada );
             end;
          except
                on Error: Exception do
                begin
                     ManejaError( 'Error Al Transformar Lista De Empleados', Error );
                end;
          end;
     end;
end;

function TOnlineMgr.EnviarArchivo( const iTerminal: Integer; const sSource, sTarget: String ): Boolean;
begin
  Result := False;
end;

function TOnlineMgr.ProgramarTerminal( const iTerminal: Integer; const sDir1,sDir2,sDir3: String ): Boolean;

const
     K_MAX = 120;
var
   termID : integer;
   ansiSource :AnsiString;
   pAnsiSource : PAnsiChar;
   intento : integer;


   {function EnviarArchivoProgDir( sSource : string ) : boolean;
   begin
        try
           ansiSource := sSource;
           pAnsiSource := PAnsiChar( ansiSource );
           Result := Check ( Synprog_displayEx( termID, pAnsiSource, 0,0, 500, 10, 2, 'Env�o de Archivos de Programaci�n y Configuraci�n Synel') );
        except
             on Error: Exception do
             begin
                  ManejaError( 'Error al Enviar Conjunto de Archivos:'+ sSource, Error );
                  Result := False;
             end;
         end;
   end;}

   function EnviarArchivosProgDir(): boolean;
   var
      dir1, dir2, dir3 :AnsiString;
   begin
        try
           dir1 := sDir1;
           dir2 := sDir2;
           dir3 := sDir3;

           pAnsiSource := PAnsiChar( ansiSource );
           Result := Check ( Synprog_displayEx_ALL( termID, PAnsiChar( dir1 ),PAnsiChar( dir2 ),PAnsiChar( dir3 ),0,0, 350, 100, 2, 'Env�o de Archivos de Programaci�n y Configuraci�n Synel') );

           if not Result then
                 ManejaError( 'Error al Enviar Conjunto de Archivos :'+ sDir1 + ', '+sDir2 + ', '+sDir3,  Exception.Create( FStatus ) );

        except
             on Error: Exception do
             begin
                  ManejaError( 'Error al Enviar Conjunto de Archivos :'+ sDir1 + ', '+sDir2 + ', '+sDir3, Error );
                  Result := False;
             end;
         end;
   end;


begin
     Result := False;
     EnterCriticalSection(FLockFuncionalidad);
     try
        with IniValues.ListaTerminales do
        begin
             if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
             begin
                  CommunicationStart( iTerminal - 1 );
                  with Terminal[ iTerminal - 1 ] do
                  begin
                       if IsConnected then
                       begin
                            if FileExists( sDir1 ) and FileExists( sDir2 ) and FileExists( sDir3) then
                            begin
                                 termID := StrToIntDef( Unidad, 0 ) ;
                                 intento := 1;
                                 while ( not Result ) and ( intento < 4) do
                                 begin
                                    if  Check( SynHalt(termID)) then
                                    begin
                                         if Check( SynDelAll(termID)) then
                                         begin
                                              Result := EnviarArchivosProgDir;
                                              Esperar(10000);
                                              Check( SynRUN(termID) );
                                         end;
                                    end;

                                    inc(intento);

                                    if ( Result ) then
                                       break
                                    else
                                       DoFeedBack( iTerminal, Format( 'Intento %d de programaci�n de terminal', [intento-1] )  );

                                 end;
                            end
                            else
                                Mensaje( Format( 'Uno de los Archivos No Existe ( Terminal # %d )', [ iTerminal ] ) );
                       end
                       else
                           Mensaje( Format( 'La Terminal %d ( %s ) Est� Desconectada', [ iTerminal, Self.GetInfo( iTerminal ) ] ) );
                  end;
                  CommunicationEnd( iTerminal - 1 );
             end
             else
                 Mensaje( Format( 'La Terminal %d ( %s ) No Existe', [ iTerminal, GetInfo( iTerminal ) ] ) );
        end;
        if Result then
        begin
           Mensaje( Format( 'La Terminal %d ( %s ) ha sido programada con �xito ', [ iTerminal,  Self.GetInfo( iTerminal) ] ) );
           DoFeedBack( iTerminal,  'La Terminal ha sido programada con �xito' );
        end
        else
           Mensaje( Format( 'No pudo programarse la  Terminal %d ( %s ) ', [ iTerminal,  Self.GetInfo( iTerminal) ] ) );


   finally
      LeaveCriticalSection(FLockFuncionalidad);
   end;
end;

function TOnlineMgr.EnviarArchivoParametros(const iTerminal: Integer; const sLongFileName, sShortFileName: String): Boolean;
var
   termID : integer;
   ansiSource :AnsiString;
   pAnsiSource : PAnsiChar;

   function EnviarArchivo( sSource : string ) : boolean;
   var
      sMensaje : string;
   begin
        Result := False;
        try
           ansiSource := sSource;
           pAnsiSource := PAnsiChar( ansiSource );
           if  Check( SynHalt(termID)) then
           begin
                  Result := Check ( SynOneTable( termID, pAnsiSource) );
                  if ( Result ) then
                     Esperar(1000)
                  else
                       sMensaje := Format( 'No Pudo Ser Enviado el Archivo %s ( %s )', [ sShortFileName, FStatus ] );

                  Check( SynRun(termID))  ;

           end
           else
               sMensaje := Format( 'No Pudo Ser Enviado el Archivo %s ( %s )', [ sShortFileName, FStatus ] );
        except
             on Error: Exception do
             begin
                  ManejaError( 'Error al enviar de Archivo:'+ sSource, Error );
                  Result := False;
             end;
         end;

        if strLleno( sMensaje ) then
        begin
                Mensaje( sMensaje );
                Result := False;
        end;

   end;


begin
//Si se conecta
     Result := False;
        with IniValues.ListaTerminales do
        begin
             if ( iTerminal > 0 ) and ( iTerminal <= Count ) then
             begin
                  CommunicationStart( iTerminal - 1 );
                  with Terminal[ iTerminal - 1 ] do
                  begin
                       if IsConnected then
                       begin
                            if FileExists( sLongFileName )  then
                            begin
                                  termID := StrToIntDef( Unidad, 0 ) ;
                                  Result := EnviarArchivo(sLongFileName);

                                  if ( not Result ) then
                                  begin
                                      DoFeedBack( iTerminal, Format( 'Re-intentando Enviar  Archivo (%s) a la terminal', [  sShortFileName ] ) );
                                      Result := EnviarArchivo(sLongFileName);
                                  end;

                                  if ( not Result ) then
                                  begin
                                      DoFeedBack( iTerminal, Format( 'Re-intentando Enviar  Archivo (%s) a la terminal', [  sShortFileName ] ) );
                                      Result := EnviarArchivo(sLongFileName);
                                  end;

                                  if ( not Result ) then
                                  begin
                                        DoFeedBack( iTerminal, Format( 'Fall� el Env�o de Archivo (%s) a la terminal', [  sShortFileName ] ) );
                                  end;

                            end
                            else
                                Mensaje( Format( 'El Archivo %s Existe ( Terminal # %d )', [sLongFileName, iTerminal ] ) );
                       end
                       else
                           Mensaje( Format( 'La Terminal %d ( %s ) Est� Desconectada', [ iTerminal, Self.GetInfo( iTerminal ) ] ) );
                  end;
                  CommunicationEnd( iTerminal - 1 );
             end
             else
                 Mensaje( Format( 'La Terminal %d ( %s ) No Existe', [ iTerminal, GetInfo( iTerminal ) ] ) );
        end;
        if Result then
           DoFeedBack( iTerminal, Format( 'Se envi� el Archivo (%s) a la terminal', [  sShortFileName ] ) )
        else
           DoFeedBack( iTerminal, Format( 'No fue posible enviar el Archivo (%s) a la terminal', [  sShortFileName ] ) );
end;

function TOnlineMgr.EnviarArchivoEmpleados(const iTerminal: Integer; const sFileName: String): Boolean;
begin
     Result := EnviarArchivoParametros( iTerminal, sFileName, 'VLD100.RDY' );
end;

function TOnlineMgr.EnviarArchivoAlarma(const iTerminal: Integer; const sFileName: String): Boolean;
begin
     Result := EnviarArchivoParametros( iTerminal, sFileName, 'FTS001.RDY' );
end;

procedure TOnlineMgr.SendAnyFile(const iTerminal: Integer; sFileName: String);
begin
    EnterCriticalSection(FLockFuncionalidad);
    try
       EnviarArchivo( iTerminal, sFileName, ExtractFileName( sFileName ) );
    finally
      LeaveCriticalSection(FLockFuncionalidad);
    end;
end;

procedure TOnlineMgr.SendApplicationFile(const iTerminal: Integer; sFileName: String);
begin
    EnterCriticalSection(FLockFuncionalidad);
    try
     EnviarArchivo( iTerminal, sFileName, ExtractFileName( sFileName ) );
    finally
      LeaveCriticalSection(FLockFuncionalidad);
    end;
end;

procedure TOnlineMgr.SendConfigFile(const iTerminal: Integer; sFileName: String);
begin
   if TransformarConfig( sFileName ) then
   begin
       EnterCriticalSection(FLockFuncionalidad);
       try
        EnviarArchivoParametros( iTerminal, sFileName, ExtractFileName( sFileName ) );
       finally
        LeaveCriticalSection(FLockFuncionalidad);
       end;

   end;
end;

procedure TOnlineMgr.SendEmployeeListFile(const iTerminal: Integer; sFileName: String);
begin
    EnterCriticalSection(FLockFuncionalidad);
    try
      EnviarArchivoEmpleados( iTerminal, sFileName );
    finally
      LeaveCriticalSection(FLockFuncionalidad);
    end;
end;

procedure TOnlineMgr.SendParameterFile(const iTerminal: Integer; sFileName: String);
begin
    EnterCriticalSection(FLockFuncionalidad);
    try
      EnviarArchivoAlarma( iTerminal, sFileName );
    finally
      LeaveCriticalSection(FLockFuncionalidad);
    end;
end;

{ TStatusEquipo }
constructor TStatusEquipo.Create(const sData: string);
const
//Datos del Status   (SynGetStatus)
    K_MULTIDROP_ID     = 1; //Multidrop I.D. 1 byte
    K_MODEL_NO         = 2; //Model no.(0...9) 1 byte
    K_VERSION_NO       = 3; //Version no. (0...9) 1 byte
    K_STATION_ID       = 4; //Station I.D. 5 bytes
    K_CLOCK_YEAR       = 5; //Year 2 bytes
    K_CLOCK_MONTH      = 6;  //Month 2
    K_CLOCK_DAY        = 7; //Day 2 bytes
    K_CLOCK_HOUR       = 8; // Hour 2 bytes
    K_CLOCK_MINUTES    = 9; // Minutes 2 bytes
    K_ACTIVE_FUNCTION  = 10;// Active function no. 1 bytes
    K_FULL_BUFFERS     = 11; //No. of full buffers 3 bytes
    K_FAULTY_BUFFERS   = 12;//No. of faulty buffers 3 bytes
    K_FULL_BUFFERS_SENT= 13;//No. of full buffers sent and not cleared3 bytes
    K_EMPTY_BUFFERS    = 14; //No. of empty buffers 3 bytes
    K_MEMORY_SIZE_USED = 15;//memory size used for the tables5 bytes
    K_STATION_TYPE     = 16; //Station Type 1 bytes
    K_SELF_TEST        = 17; //self test 1 bytes
    K_CRC              = 18; //CRC 4 bytes
    K_EOT              = 19; //EOT 1 (04 hex)
                            //Datos  + Fecha y Hora + Active Function + Buffers + memory size used for the tables + Station replay,Self test,CRC,EOT
    K_EXPRESION_REGULAR =  's(.)(.)(.)(.{5})'+'(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})'+'(.)'+ '(.{3})(.{3})(.{3})(.{3})'+'(.{5})'+'(.)(.)(.{4})(.)';
var
   DatosExpr : TRegExpr;
begin
   DatosExpr := TRegExpr.Create;
   try
      DatosExpr.Expression :=  K_EXPRESION_REGULAR;

      FOK := False;

      if DatosExpr.Exec( sData ) then
      begin
           MULTIDROP_ID      := DatosExpr.Match[K_MULTIDROP_ID     ];
           MODEL_NO          := DatosExpr.Match[K_MODEL_NO         ];
           VERSION_NO        := DatosExpr.Match[K_VERSION_NO       ];
           STATION_ID        := DatosExpr.Match[K_STATION_ID       ];
           CLOCK_YEAR        := DatosExpr.Match[K_CLOCK_YEAR       ];
           CLOCK_MONTH       := DatosExpr.Match[K_CLOCK_MONTH      ];
           CLOCK_DAY         := DatosExpr.Match[K_CLOCK_DAY        ];
           CLOCK_HOUR        := DatosExpr.Match[K_CLOCK_HOUR       ];
           CLOCK_MINUTES     := DatosExpr.Match[K_CLOCK_MINUTES    ];
           ACTIVE_FUNCTION   := DatosExpr.Match[K_ACTIVE_FUNCTION  ];
           FULL_BUFFERS      := DatosExpr.Match[K_FULL_BUFFERS     ];
           FAULTY_BUFFERS    := DatosExpr.Match[K_FAULTY_BUFFERS   ];
           FULL_BUFFERS_SENT := DatosExpr.Match[K_FULL_BUFFERS_SENT];
           EMPTY_BUFFERS     := DatosExpr.Match[K_EMPTY_BUFFERS    ];
           MEMORY_SIZE_USED  := DatosExpr.Match[K_MEMORY_SIZE_USED ];
           STATION_TYPE      := DatosExpr.Match[K_STATION_TYPE     ];
           SELF_TEST         := DatosExpr.Match[K_SELF_TEST        ];
           CRC               := DatosExpr.Match[K_CRC              ];
           EOT               := DatosExpr.Match[K_EOT              ];
           FOK := True;
      end;
   finally
          FreeAndNil( DatosExpr );
   end;

end;

destructor TStatusEquipo.Destroy;
begin
//
  inherited;
end;

procedure TOnlineMgr.Esperar( _delayms: integer);
begin
      Windows.Sleep(_delayms );
end;

procedure TOnlineMgr.Verify;
begin
     if ( Units = 0 ) then
        raise Exception.Create( 'No Se Detectaron Terminales' );
end;


procedure TOnlineMgr.ManejaError(const sMensaje: String);
var
  e : Exception;
begin
  e:= Exception.Create( VACIO );
  ManejaError(sMensaje, e ) ;
  FreeAndNil(e);
end;

end.

