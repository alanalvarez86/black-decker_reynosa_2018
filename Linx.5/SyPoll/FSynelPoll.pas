unit FSynelPoll;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FLinx5Poll.pas                             ::
  :: Descripci�n: Programa principal de L5Poll.exe           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, DBClient,
     Forms, Dialogs, ExtCtrls, Menus, StdCtrls, Buttons, ComCtrls, ComObj, ActiveX,
     LinxIni,
     FOnlineDLL,
     FLinxBase,
     DPoll;

type
  TSynelPoller = class(TLinxBase)
    N5: TMenuItem;
    N9: TMenuItem;
    TerminalesDirectorio: TMenuItem;
    TerminalEjecutar: TMenuItem;
    TerminalesConfiguracion: TMenuItem;
    ConfiguracionArchivos: TMenuItem;
    TerminalesEnviar: TMenuItem;
    btConectar: TBitBtn;
    Empleados: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConfiguracionClick(Sender: TObject);
    procedure ConfiguracionListaClick(Sender: TObject);
    procedure TerminalesProgramaClick(Sender: TObject);
    procedure TerminalesAlarmaClick(Sender: TObject);
    procedure TerminalesEmpleadosClick(Sender: TObject);
    procedure TerminalesConfiguracionClick(Sender: TObject);
    procedure TerminalesSincronizarClick(Sender: TObject);
    procedure TerminalesSincronizarTodasClick(Sender: TObject);
    procedure TerminalesResetearClick(Sender: TObject);
    procedure TerminalesRebootClick(Sender: TObject);
    procedure TerminalesVersionClick(Sender: TObject);
    procedure TerminalesDirectorioClick(Sender: TObject);
    procedure TerminalEjecutarClick(Sender: TObject);
    procedure ConfiguracionArchivosClick(Sender: TObject);
    procedure TerminalesEnviarClick(Sender: TObject);
    procedure btConectarClick(Sender: TObject);
    procedure ArchivoTerminarClick(Sender: TObject);
  private
    { Private declarations }
     FOnlineMgr: TOnlineMgr;
     FConectado : Boolean;
     FConectando : Boolean;

     //procedure Delay(ms: Cardinal);

  protected
    { Protected declarations }
    function DatosEnviados: String; override;
    function InitAllServers: Boolean; override;
    function Read: Boolean; override;
    function RegistroLeido: String; override;
    function RegistroNormal: Boolean; override;
    function Terminal: Integer; override;
    function VerifyAllServers: Boolean; override;
    function PuedeRecolectar: Boolean; override;
    procedure PuedeLeerChecadas ( var lPuede : Boolean);
    procedure AutoSincronizarTodas( const dValue: TDate ); override;
    procedure DownAllServers; override;
    procedure ExecuteCmd( Command: Executer );
    procedure MakeNodeList; override;
    procedure ProcesarLectura( var sData: String ); override;
    procedure SendPrograma( const sFileName, sFileName2, sFileName3 : String ); override;
    procedure SendAlarm( const sFileName: String ); override;
    procedure SendEmpleados( const sFileName: String ); override;
    procedure SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
    procedure SendRebootCmd; override;    
    procedure EscribeLecturaLinea( const sData: String );
    procedure EscribeAllData( const sData: String ); override;
    procedure SendProgDirs;
    procedure EnablePoll( const lEnabled: Boolean ); override;
    procedure EnableControls( const lRecolectando: Boolean ); override;
  public
    { Public declarations }
    function BatchModeEnabled: Boolean; override;
    procedure ProcesaParametro( const sParametro, sArchivo: String );  override;
    procedure ProcesaParametro( const sParametro, sArchivo, sArchivo2, sArchivo3: String );  overload;
    procedure MakeUnitList; override;
  end;

var
  SynelPoller: TSynelPoller;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FAutoClasses,
     FArchivos,
     FFileMgr,
     FEjecutador,
     FViewLog,
     FImportar,
     FSincronizar,
     FTerminales,
     FLinxTools;

{$R *.DFM}

{ ************* TUnitekPoller ************ }

procedure TSynelPoller.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     K_TITULO = '%s Profesional';
     {$else}
     K_TITULO = '%s';
     {$endif}
begin
     inherited;
     FOnlineMgr := TOnlineMgr.Create;
     with FOnlineMgr do
     begin
          OnError := ManejaError;
          OnMessage := ManejaBitacora;
          OnMakeUnitList := MakeUnitList;
          OnFeedBack := PostMessage;
          OnGaugeSetSize := GaugeSetSize;
          OnGaugeAdvance := GaugeAdvance;
          OnGaugeFinish := GaugeFinish;
          OnDoLectura := EscribeLecturaLinea;
          OnPuedeLeerChecadas := PuedeLeerChecadas;
          ValidTokens := TOKEN_ASISTENCIA;
          with Autorizacion do
          begin
                    NumeroSentinel := NumeroSerie;
                    FOnlineMgr.EsDemo :=  Autorizacion.EsDemo or not OkModulo( okL5Poll, True ) ;
          end;
     end;
     Self.Caption := Format( K_TITULO, [ Application.Title ] );
end;

procedure TSynelPoller.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FOnlineMgr );
     inherited;
end;

function TSynelPoller.BatchModeEnabled: Boolean;
begin
     Result := True;
end;

{ ********** Abrir Servidores ************ }

function TSynelPoller.InitAllServers: Boolean;
var
   i: Integer;
begin
     Result := inherited InitAllServers;
     if Result then
     begin
          Result := FOnlineMgr.Init;
          if Result then
          begin
               for i := 1 to FOnlineMgr.Units do
               begin
                    with FOnlineMgr.Terminal[ i ] do
                    begin
                         if not IsConnected then
                         begin
                              Bitacora.WriteText( Format( 'Terminal %s ( %s ) Fuera De L�nea', [ Description, Identificador ] ) );
                         end;
                    end;
               end;
          end;
     end;
end;


procedure TSynelPoller.ProcesaParametro( const sParametro, sArchivo: String );
begin
     ProcesaParametro( sParametro, sArchivo,  VACIO, VACIO);
end;

procedure TSynelPoller.ProcesaParametro(const sParametro, sArchivo,
  sArchivo2, sArchivo3: String);
const
     P_ASISTENCIA = '/X';
     P_ASISTENCIA_ALARMA = '/XA';
     P_ASISTENCIA_SINCRONIZAR = '/XS';
     P_IMPORTAR = '/I';
     P_EMPLEADOS = '/XE';
     P_FLASH = '/FLASH';
     P_SCRIPT = '/SCRIPT';
     P_PROGRAMAR = '/PROGRAMAR';
     P_REBOOT = '/REBOOT';
var
   iRecords: Integer;
   eOp: eBatchOp;
begin
     if BatchModeEnabled then
     begin
          if ( sParametro = P_ASISTENCIA ) then
             eOp := boAsistencia
          else
              if ( sParametro = P_ASISTENCIA_ALARMA ) then
                 eOp := boAlarma
              else
                  if ( sParametro = P_ASISTENCIA_SINCRONIZAR ) then
                     eOp := boSincronizar
                  else
                      if ( sParametro = P_IMPORTAR ) then
                         eOp := boImportar
                      else
                          if ( sParametro = P_EMPLEADOS ) then
                             eOp := boEmpleados
                          else
                            if ( sParametro = P_PROGRAMAR ) then
                               eOp := boProgramar
                            else
                             if ( sParametro = P_SCRIPT ) then
                                eOp := boScript
                             else
                              if ( sParametro = P_REBOOT ) then
                                 eOp := boReboot
                              else
                                   eOp := boUnknown;
          case eOp of
               boAsistencia: CicloBatch( boAsistencia, '' );
               boAlarma: CicloBatch( boAlarma, sArchivo );
               boSincronizar: CicloBatch( boSincronizar, sArchivo );
               boImportar:
               begin
                    BatchModeSet;
                    try
                       LogOpen( 'Procesamiento de Archivo' );
                       try
                          if FileExists( sArchivo ) then
                          begin
                               if dmPoll.EnviarChecadas( sArchivo, iRecords ) then
                                  ManejaMensaje( Format( 'El Archivo %s Fue Procesado Con %d Checadas', [ sArchivo, iRecords ] ) )
                               else
                                   ManejaMensaje( Format( 'El Archivo %s No Fue Procesado', [ sArchivo ] ) );
                          end
                          else
                              ManejaMensaje( Format( 'El Archivo %s No Existe', [ sArchivo ] ) );
                       finally
                              LogClose;
                       end;
                    finally
                           BatchModeReset;
                    end;
               end;
               boEmpleados: CicloBatch( boEmpleados, sArchivo );
               boFlash: CicloBatch( boFlash, sArchivo );
               boScript: CicloBatch( boScript, sArchivo );
               boReboot: CicloBatch( boReboot, '' );
               boProgramar : CicloBatch( boProgramar, sArchivo, sArchivo2, sArchivo3);
          else
              ZetaDialogo.zError( '� Error En Par�metro !',
                                  'Se Especific� el Par�metro ' + sParametro + CR_LF +
                                  CR_LF +
                                  'Los Par�metros V�lidos Son: ' + CR_LF +
                                  Format( '%s : Operaci�n Batch de Asistencia', [ P_ASISTENCIA ] ) + CR_LF +
                                  Format( '%s <FTS001.RDY> : Operaci�n Batch de Asistencia y Enviar Archivo de Alarmas <FTS001.rdy> � <FTS001.rdy Default>', [ P_ASISTENCIA_ALARMA ] ) + CR_LF +
                                  Format( '%s : Operaci�n Batch de Asistencia y Sincronizar Relojes a Hora del Sistema', [ P_ASISTENCIA_SINCRONIZAR ] ) + CR_LF +
                                  Format( '%s <HORA> : Operaci�n Batch de Asistencia y Sincronizar Relojes a <HORA>', [ P_ASISTENCIA_SINCRONIZAR ] ) + CR_LF +
                                  Format( '%s <ARCHIVO> : Procesar Archivo de Asistencia', [ P_IMPORTAR ] ) + CR_LF +
                                  Format( '%s <ARCHIVO> : Operaci�n Batch de Asistencia y Enviar Archivo de Empleados <VLD100.rdy> � <VLD100.rdy Default>', [ P_EMPLEADOS ] ) + CR_LF +
                                  //Format( '%s <ARCHIVO> : Enviar Archivo a Memoria Flash', [ P_FLASH ] ) + CR_LF +
                                  Format( '%s Enviar Archivos de Programa Predeterminados a Terminal Synel', [ P_PROGRAMAR ] ) + CR_LF +
                                  Format( '%s <ARCHIVO DIR001> <ARCHIVO DIR002> <ARCHIVO DIR003> : Enviar Archivos de Programa a Terminal Synel', [ P_PROGRAMAR ] ) + CR_LF +
                                  Format( '%s : Reiniciar la Terminal', [ P_REBOOT ] ), 0 );
          end;
     end
     else
         ZetaDialogo.zError( '� Modo Inv�lido !', 'No Se Puede Operar En Modo Batch', 0 );
end;


{ ********** Procesamiento ********** }

procedure TSynelPoller.ProcesarLectura( var sData: String );
begin
     inherited ProcesarLectura( sData );
end;

{ ********** Manejo de Terminales Synel  ********** }

function TSynelPoller.DatosEnviados: String;
begin
     Result := FOnlineMgr.DatosEnviados;
end;

{procedure TSynelPoller.Delay(ms: Cardinal);
var
  Start: Cardinal;
begin
  Start:= GetTickCount;

  while ms + Start > GetTickCount do
    Application.ProcessMessages;
end;}


function TSynelPoller.Read: Boolean;
const
     K_READ_SLEEPTIME = 1000;
begin
     Result := FOnlineMgr.Read;
end;

function TSynelPoller.RegistroLeido: String;
begin
     Result := DatosEnviados;
end;

function TSynelPoller.RegistroNormal: Boolean;
begin
     Result := FOnlineMgr.RegistroNormal;
end;

function TSynelPoller.Terminal: Integer;
begin
     Result := FOnlineMgr.TerminalID;
end;

function TSynelPoller.VerifyAllServers: Boolean;
begin
     MakeUnitList;

     try
        FOnlineMgr.Verify;
        Result := True;
     except
           on Error: Exception do
           begin
                ManejaError( 'Error Al Verificar Servidores', Error );
                Result := False;
           end;
     end;
end;

procedure TSynelPoller.AutoSincronizarTodas(const dValue: TDate);
begin
     FOnlineMgr.SetDateTimeAll( dValue );
end;

procedure TSynelPoller.MakeNodeList;
begin
     FOnlineMgr.MakeNodeList;
end;

procedure TSynelPoller.MakeUnitList;
var
   i: Integer;
begin
     with UnitList.Items do
     begin
          BeginUpdate;
          Clear;
          for i := 1 to FOnlineMgr.Units do
          begin
               Add( FOnlineMgr.GetInfo( i ) );
               if BatchMode then
               begin
                    Bitacora.WriteText( Format( '%s - [ %s ]', [ FOnlineMgr.GetInfo( i ), FOnlineMgr.GetTerminalVersion( i ) ] ) );
               end;
          end;
          EndUpdate;
     end;



end;

procedure TSynelPoller.ExecuteCmd( Command: Executer );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          SetCursorWait;
          for i := 0 to ( UnitList.Items.Count - 1 ) do
              if UnitList.Selected[ i ] then
              begin
                   Command( i + 1 );
              end;
          SetCursorNormal;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TSynelPoller.SendAlarm(const sFileName: String);
var
   i: Integer;
   sAlarmFile, sAlarmFileOld, sAlarmFileDAT: String;
begin
     with FOnlineMgr do
     begin
          sAlarmFileOld := VACIO;
          sAlarmFileDAT := VACIO;
          for i := 1 to Units do
          begin
               sAlarmFile := VACIO;
               if ZetaCommonTools.StrLleno( sFileName ) then
                  sAlarmFile := sFileName
               else
               begin
                    if Assigned( Terminal[ i ] ) then
                    begin
                         sAlarmFile := Terminal[ i ].Alarma;
                    end;
               end;
               if ZetaCommonTools.StrLleno( sAlarmFile ) and FileExists( sAlarmFile ) then
               begin
                    if EnviarArchivoAlarma( i, sAlarmFile ) then
                       Bitacora.WriteTimeStamp( Format( 'Alarma %s Enviado A %s', [ sAlarmFile, Terminal[ i ].Description ] ) + ' ( %s )' )
                    else
                        Bitacora.WriteTimeStamp( Format( 'Alarma %s NO FUE ENVIADO A %s', [ sAlarmFile, Terminal[ i ].Description ] ) + ' ( %s )' )
               end
               else
                   if ZetaCommonTools.StrLleno( sAlarmFile ) and not FileExists( sAlarmFile) then
                   begin
                        Bitacora.WriteTimeStamp( Format( 'Terminal %s: Archivo De Alarmas %s No Existe', [ Terminal[ i ].Description, sAlarmFile ] ) + ' ( %s )' );
                   end;
          end;
     end;
end;

procedure TSynelPoller.SendEmpleados(const sFileName: String);
var
   i: Integer;
   sEmployeeFile : String;
begin
     with FOnlineMgr do
     begin
          for i := 1 to Units do
          begin
               sEmployeeFile := VACIO;
               if ZetaCommonTools.StrLleno( sFileName ) then
                  sEmployeeFile := sFileName
               else
               begin
                    if Assigned( Terminal[ i ] ) then
                    begin
                         sEmployeeFile := Terminal[ i ].Empleados;
                    end;
               end;
               if ZetaCommonTools.StrLleno( sEmployeeFile ) and FileExists( sEmployeeFile ) then
               begin
                    if EnviarArchivoEmpleados( i, sEmployeeFile ) then
                       Bitacora.WriteTimeStamp( Format( 'Lista De Empleados %s Enviada A %s', [ sEmployeeFile, Terminal[ i ].Description ] ) + ' ( %s )' )
                    else
                        Bitacora.WriteTimeStamp( Format( 'Lista De Empleados %s No Fu� Enviada A %s', [ sEmployeeFile, Terminal[ i ].Description ] ) + ' ( %s )' )

               end
               else
                   if ZetaCommonTools.StrLleno( sEmployeeFile ) and not FileExists( sEmployeeFile) then
                   begin
                        Bitacora.WriteTimeStamp( Format( 'Terminal %s: Lista De Empleados %s No Existe', [ Terminal[ i ].Description, sEmployeeFile ] ) + ' ( %s )' );
                   end;
          end;
     end;
end;

procedure TSynelPoller.SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          if NOT lAskForFile OR GetFileName( sFileName ) then
          begin
               if FileExists( sFileName ) then
               begin
                    Application.ProcessMessages;
                    SetCursorWait;
                    for i := 0 to ( UnitList.Items.Count - 1 ) do
                        if UnitList.Selected[ i ] then
                           Command( ( i + 1 ), sFileName );
                    SetCursorNormal;
               end
               else
                   zWarning( '� Archivo No Existe !', sFileName, 0, mbOK );
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;


procedure TSynelPoller.SendProgDirs;
var
   i: Integer;
   sProgDir1, sProgDir2, sProgDIr3 : string;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          sProgDir1 := IniValues.ProgDir1;
          sProgDir2 := IniValues.ProgDir2;
          sProgDir3 := IniValues.ProgDir3;

          if ArchivosPrograma( sProgDir1 , sProgDir2 , sProgDir3) then
          begin
               if FileExists( sProgDir1 ) then
               begin
                  if FileExists( sProgDir2 ) then
                  begin
                       if FileExists( sProgDir3 ) then
                       begin
                           Application.ProcessMessages;
                           SetCursorWait;
                           for i := 0 to ( UnitList.Items.Count - 1 ) do
                               if UnitList.Selected[ i ] then
                               begin
                                    FOnlineMgr.ProgramarTerminal( i+1, sProgDir1, sProgDir2, sProgDir3 )
                               end;
                           SetCursorNormal;
                       end
                       else
                         zWarning( '� Archivo No Existe !', sProgDir3 , 0, mbOK );

                  end
                  else
                    zWarning( '� Archivo No Existe !', sProgDir2 , 0, mbOK );

               end
               else
                   zWarning( '� Archivo No Existe !', sProgDir1 , 0, mbOK );
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TSynelPoller.EscribeAllData( const sData: String );
begin
     // No hacer nada se agregar� con el evento onDoLectura
end;

procedure TSynelPoller.EscribeLecturaLinea( const sData: String );
begin
     AsciiServer.WriteAllData( sData );
end;

{ ********** Cerrar Servidores ********** }

procedure TSynelPoller.DownAllServers;
var
   i: Integer;
begin
     if BatchMode then
     begin
          for i := 1 to FOnlineMgr.Units do
          begin
               Bitacora.WriteText( Format( '%s - Lecturas [ %d ]', [ FOnlineMgr.GetInfo( i ), FOnLineMgr.Terminal[i].Lecturas ] ) );
          end;
     end;
     FOnlineMgr.Stop;
     inherited DownAllServers;
end;

{ ************ Eventos del Men� *********** }

procedure TSynelPoller.ConfiguracionClick(Sender: TObject);
begin
     inherited;
     ConfiguracionLista.Enabled := not Recolectando;
     ConfiguracionArchivos.Enabled := not Recolectando;
end;

procedure TSynelPoller.ConfiguracionArchivosClick(Sender: TObject);
begin
     inherited;
     with TArchivos.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TSynelPoller.ConfiguracionListaClick(Sender: TObject);
begin
     inherited;
     with TCapturarTerminales.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TSynelPoller.TerminalesProgramaClick(Sender: TObject);
begin
     inherited;

     SendProgDirs;

     //SendFile( FOnlineMgr.SendApplicationFile, IniValues.Programa, True );
end;

procedure TSynelPoller.TerminalesAlarmaClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendParameterFile, IniValues.Alarma, True );
end;

procedure TSynelPoller.TerminalesEmpleadosClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendEmployeeListFile, IniValues.Empleados, True );
end;

procedure TSynelPoller.TerminalesConfiguracionClick(Sender: TObject);
begin
     inherited;
//     SendFile( FOnlineMgr.SendConfigFile, IniValues.Configuracion, True );
end;

procedure TSynelPoller.TerminalesVersionClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( FOnlineMgr.ShowVersion );
end;

procedure TSynelPoller.TerminalesDirectorioClick(Sender: TObject);
var
   FArchivos: TStrings;
   i, iSelected: Integer;
begin
     inherited;
     if ( UnitList.SelCount = 0 ) then
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK )
     else
         if ( UnitList.SelCount > 1 ) then
            ZetaDialogo.zWarning( '� Demasiadas Terminales !', 'Se Debe Escoger Una Sola Terminal', 0, mbOK )
         else
         begin
              iSelected := -1;
              FArchivos := TStringList.Create;
              try
                 SetCursorWait;
                 for i := 0 to ( UnitList.Items.Count - 1 ) do
                 begin
                      if UnitList.Selected[ i ] then
                      begin
                           iSelected := ( i + 1 );
                           FOnlineMgr.CargaDirectorio( iSelected, FArchivos );
                           Break;
                      end;
                 end;
                 SetCursorNormal;
                 if ( FArchivos.Count > 0 ) then
                 begin
                      FileManager := TFileManager.Create( Self );
                      try
                         with FileManager do
                         begin
                              TerminalIndex := iSelected;
                              OnlineMgr := FOnlineMgr;
                              CargarArchivos( FArchivos );
                              ShowModal;
                         end;
                      finally
                             FreeAndNil( FileManager );
                      end;
                 end
                 else
                     ZetaDialogo.zWarning( '� No Hay Archivos !', Format( 'La Terminal %s NO Tiene Archivos', [ FOnlineMgr.Terminal[ iSelected ].Description ] ), 0, mbOK )
              finally
                     FreeAndNil( FArchivos );
              end;
         end;
end;

procedure TSynelPoller.TerminalEjecutarClick(Sender: TObject);
var
   FArchivos: TStrings;
   i, iSelected: Integer;
begin
     inherited;
     if ( UnitList.SelCount = 0 ) then
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK )
     else
         if ( UnitList.SelCount > 1 ) then
            ZetaDialogo.zWarning( '� Demasiadas Terminales !', 'Se Debe Escoger Una Sola Terminal', 0, mbOK )
         else
         begin
              iSelected := -1;
              FArchivos := TStringList.Create;
              try
                 SetCursorWait;
                 for i := 0 to ( UnitList.Items.Count - 1 ) do
                 begin
                      if UnitList.Selected[ i ] then
                      begin
                           iSelected := ( i + 1 );
                           FOnlineMgr.CargaDirectorio( iSelected, FArchivos );
                           Break;
                      end;
                 end;
                 SetCursorNormal;
                 if ( FArchivos.Count > 0 ) then
                 begin
                      ExecuteMgr := TExecuteMgr.Create( Self );
                      try
                         with ExecuteMgr do
                         begin
                              TerminalIndex := iSelected;
                              OnlineMgr := FOnlineMgr;
                              CargarArchivos( FArchivos );
                              ShowModal;
                         end;
                      finally
                             FreeAndNil( ExecuteMgr );
                      end;
                 end
                 else
                     ZetaDialogo.zWarning( '� No Hay Archivos !', Format( 'La Terminal %s NO Tiene Archivos', [ FOnlineMgr.Terminal[ iSelected ].Description ] ), 0, mbOK )
              finally
                     FreeAndNil( FArchivos );
              end;
         end;
end;

procedure TSynelPoller.TerminalesEnviarClick(Sender: TObject);
begin
     inherited;
    // SendFile( FOnlineMgr.SendAnyFile, '', True );
end;

procedure TSynelPoller.TerminalesSincronizarClick(Sender: TObject);
var
   i: Integer;
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     if ( UnitList.SelCount > 0 ) then
     begin
          dDateTime := Now;
          if FSincronizar.GetDateTime( dDateTime, TRUE, lHoraSistema ) then
          begin
               SetCursorWait;
               for i := 0 to ( UnitList.Items.Count - 1 ) do
               begin
                    if UnitList.Selected[ i ] then
                    begin
                         if lHoraSistema then
                            dDateTime := Now;
                         FOnlineMgr.SetDateTime( ( i + 1 ), dDateTime );
                    end;
               end;
               SetCursorNormal;
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TSynelPoller.TerminalesSincronizarTodasClick(Sender: TObject);
var
   i: Integer;
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     dDateTime := Now;
     if FSincronizar.GetDateTime( dDateTime, TRUE, lHoraSistema ) then
     begin
          SetCursorWait;
          for i := 0 to ( UnitList.Items.Count - 1 ) do
          begin
               if lHoraSistema then
                  dDateTime := Now;
               FOnlineMgr.SetDateTime( ( i + 1 ), dDateTime );
          end;
          SetCursorNormal;
     end;
end;

procedure TSynelPoller.TerminalesResetearClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Resetear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( FOnlineMgr.ResetUnit );
     end;
end;

procedure TSynelPoller.TerminalesRebootClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Rebootear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( FOnlineMgr.RebootUnit );
     end;
end;

procedure TSynelPoller.btConectarClick(Sender: TObject);
var
  dStart : TDateTime;
begin
  inherited;
     FConectado := True;

     FConectando := False;
     EnableControls( True );
     try
        EnablePoll( False );
        LogOpen;
        StartPoll;
        StatusMessage( 'Iniciando Conexi�n' );
        FLecturas := -1;
        while FConectado do
        begin
             dStart := Now;
             MakeNodeList;
             Relojito.Caption := TimeToStr( Now );
             Application.ProcessMessages;
             IniValues.ListaAlarmas.CheckInterval( dStart, SendAlarm );
        end;
        StatusMessage( 'Conexi�n Ha Terminado' );
        EndPoll;
        LogClose;
     finally
            FConectando := False;
            EnableControls( False );
     end;
end;

procedure TSynelPoller.ArchivoTerminarClick(Sender: TObject);
begin
  FConectado := False;
  inherited;

end;

procedure TSynelPoller.EnablePoll(const lEnabled: Boolean);
begin
  inherited EnablePoll(lEnabled);
  btConectar.Enabled := lEnabled;

end;

procedure TSynelPoller.EnableControls(const lRecolectando: Boolean);
begin
  inherited;
  Empleados.Enabled :=  lRecolectando;
end;



procedure TSynelPoller.SendPrograma(const sFileName, sFileName2,
  sFileName3: String);
var

  sProgDir1, sProgDir2, sProgDir3 : string;
  i : integer;
begin
   sProgDir1 := VACIO;
   sProgDir2 := VACIO;
   sProgDir3 := VACIO;

   if StrLleno( sFileName ) then
   begin
      sProgDir1 := sFileName;
      sProgDir2 := sFileName2;
      sProgDir3 := sFileName3
   end
   else
   begin
        sProgDir1 := IniValues.ProgDir1;
        sProgDir2 := IniValues.ProgDir2;
        sProgDir3 := IniValues.ProgDir3;
   end;

   if StrLleno( sProgDir1 ) and StrLleno( sProgDir2 ) and StrLleno( sProgDir3 ) then
   begin
        if FileExists( sProgDir1 ) then
        begin
           if FileExists( sProgDir2 ) then
           begin
                if FileExists( sProgDir3 ) then
                begin
                     with FOnlineMgr do
                     begin
                         for i := 1 to Units do
                         begin
                              if ProgramarTerminal(  i, sProgDir1, sProgDir2, sProgDir3  ) then
                                 Bitacora.WriteTimeStamp( Format( 'Se Envi� Los Archivos de Programa (%s,%s,%s) a la Terminal  %s', [ sProgDir1, sProgDir2, sProgDir3, Terminal[ i ].Description ] ) + ' ( %s )' )
                              else
                                  Bitacora.WriteTimeStamp( Format( 'No se pudieron enviar Los Archivos de Programa (%s,%s,%s) a la Terminal  %s', [ sProgDir1, sProgDir2, sProgDir3, Terminal[ i ].Description ] ) + ' ( %s )' )
                         end;
                     end;
                end
                else
                   Bitacora.WriteTimeStamp( Format( '� Archivo DIR003 No Existe ! %s ', [ sProgDir3 ] ) + ' ( %s )' );
           end
           else
               Bitacora.WriteTimeStamp( Format( '� Archivo DIR002 No Existe ! %s ', [ sProgDir2 ] ) + ' ( %s )' );
        end
        else
            Bitacora.WriteTimeStamp( Format( '� Archivo DIR001 No Existe ! %s ', [ sProgDir1 ] ) + ' ( %s )' );
   end
   else
       Bitacora.WriteTimeStamp('Los Archivos de Programa (Dir) NO est�n especificados' + ' ( %s )' );

end;

procedure TSynelPoller.SendRebootCmd;
var
 i : integer;
begin
     with FOnlineMgr do
     begin
          for i := 1 to Units do
          begin
               if ResetUnitFn( i ) then
                  Bitacora.WriteTimeStamp( Format( 'La Terminal %s fue Reiniciada', [ Terminal[ i ].Description ] ) + ' ( %s )' )
               else
                   Bitacora.WriteTimeStamp( Format( 'La Terminal %s NO pudo ser Reiniciada', [ Terminal[ i ].Description ] ) + ' ( %s )' )
          end;
     end;
end;


function TSynelPoller.PuedeRecolectar: Boolean;
begin
     Result := FOnlineMgr.Units > 0 ;
end;

procedure TSynelPoller.PuedeLeerChecadas( var lPuede : Boolean );
begin
     lPuede := ChecaTopeLecturas;
end;

end.
