unit FFileMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     LinxIni,
     FOnlineDll,
     ZBaseDlgModal;

type
  TFileManager = class(TZetaDlgModal)
    ArchivosGB: TGroupBox;
    Archivos: TListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FTerminalIndex: Integer;
    FOnlineMgr: TOnlineMgr;
  public
    { Public declarations }
    property TerminalIndex: Integer read FTerminalIndex write FTerminalIndex;
    property OnlineMgr: TOnlineMgr read FOnlineMgr write FOnlineMgr;
    procedure CargarArchivos( Lista: TStrings );
  end;

var
  FileManager: TFileManager;

implementation

{$R *.DFM}

uses ZetaDialogo;

procedure TFileManager.FormShow(Sender: TObject);
var
   FTerminal: TTerminal;
begin
     inherited;
     FTerminal := OnlineMgr.Terminal[ TerminalIndex ];
     if Assigned( FTerminal ) then
     begin
          ArchivosGB.Caption := Format( ' Archivos Cargados En Terminal: %s ', [ FTerminal.Description ] );
     end
     else
     begin
          ArchivosGB.Caption := ' Archivos Cargados En Terminal Desconocida ';
     end;
     ActiveControl := Archivos;
     with Archivos do
     begin
          if ( Items.Count > 0 ) then
             ItemIndex := 0;
     end;
end;

procedure TFileManager.CargarArchivos(Lista: TStrings);
begin
     with Archivos.Items do
     begin
          Clear;
          BeginUpdate;
          try
             Assign( Lista );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TFileManager.OKClick(Sender: TObject);
var
   sArchivo: String;
   i: Integer;
begin
     inherited;
     with Archivos do
     begin
          i := ItemIndex;
          if ( i >= 0 ) then
          begin
               sArchivo := Items[ i ];
               if ZetaDialogo.zConfirm( '� Atenci�n !', Format( '� Desea Borrar El Archivo %s ?', [ sArchivo ] ), 0, mbNo ) then
               begin
                    if OnlineMgr.BorrarArchivo( TerminalIndex, sArchivo ) then
                    begin
                         Items.Delete( i );
                    end
                    else
                        ZetaDialogo.zError( '� Error !', Format( 'El Archivo %s No Pudo Ser Borrado', [ sArchivo ] ), 0 );
               end;
          end;
     end;
end;

end.
