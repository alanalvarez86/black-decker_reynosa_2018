unit FArchivos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal,
     LinxIni;

type
  TArchivos = class(TZetaDlgModal)
    ConfigLBL: TLabel;
    ProgramaLBL: TLabel;
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    ProgDir1Seek: TSpeedButton;
    ProgDir2Seek: TSpeedButton;
    AlarmaSeek: TSpeedButton;
    ProgDir3Seek: TSpeedButton;
    ProgDir3: TEdit;
    Alarma: TEdit;
    ProgDir2: TEdit;
    ProgDir1: TEdit;
    OpenDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Empleados: TEdit;
    BuscarEmpleados: TSpeedButton;
    gbArchivosRecurrentes: TGroupBox;
    procedure FormShow(Sender: TObject);
    procedure ProgDir1SeekClick(Sender: TObject);
    procedure ProgDir2SeekClick(Sender: TObject);
    procedure AlarmaSeekClick(Sender: TObject);
    procedure ProgDir3SeekClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure BuscarEmpleadosClick(Sender: TObject);
  private
    { Private declarations }
    function BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
  public
    { Public declarations }
    ModoEjecucion : Boolean;
  end;

var
  Archivos: TArchivos;


function ArchivosPrograma(var sDir1, sDir2, sDir3 : string ): Boolean;

implementation

uses LinxIniBase;

{$R *.DFM}

function ArchivosPrograma(var sDir1, sDir2, sDir3 : string ): Boolean;
begin
     Result := False;
     if ( Archivos = nil ) then
        Archivos := TArchivos.Create( Application );
     if ( Archivos <> nil ) then
     begin

          with Archivos do
          begin
               ModoEjecucion := True;
               ShowModal;
               sDir1 := ProgDir1.Text;
               sDir2 := ProgDir2.Text;
               sDir3 := ProgDir3.Text;
               ModoEjecucion := False;
               Result := ( ModalResult = mrOk );
          end;
     end;
end;

procedure TArchivos.FormShow(Sender: TObject);
begin
     inherited;

     if not ModoEjecucion then
     begin
      Caption := 'Archivos Predeterminados de Programación de Terminales';
      Height := 242;
      gbArchivosRecurrentes.Enabled := True;
      gbArchivosRecurrentes.Visible := True;
     end
     else
     begin
      Caption := 'Archivos de Programación de Terminales';
      Height := 167;
      gbArchivosRecurrentes.Enabled := False;
      gbArchivosRecurrentes.Visible := False;

     end;


     with LinxIni.IniValues do
     begin
          Self.ProgDir1.Text := ProgDir1;
          Self.ProgDir2.Text := ProgDir2;
          Self.ProgDir3.Text := ProgDir3;
          Self.Alarma.Text := Alarma;
          Self.Empleados.Text := Empleados;
     end;
end;

function TArchivos.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

procedure TArchivos.ProgDir1SeekClick(Sender: TObject);
begin
     inherited;
     ProgDir1.Text := BuscaArchivo( ProgDir1.Text, 'Archivo Tipo RDY (*.RDY)|*.RDY|Todos los Archivos|*.*', ProgDir1Seek.Caption  );
end;

procedure TArchivos.ProgDir2SeekClick(Sender: TObject);
begin
     inherited;
     ProgDir2.Text := BuscaArchivo( ProgDir2.Text, 'Archivo Tipo RDY (*.RDY)|*.RDY|Todos los Archivos|*.*', ProgDir2Seek.Caption  );
end;

procedure TArchivos.ProgDir3SeekClick(Sender: TObject);
begin
     inherited;
     ProgDir3.Text := BuscaArchivo( ProgDir3.Text, 'Archivo Tipo RDY (*.RDY)|*.RDY|Todos los Archivos|*.*', ProgDir3Seek.Caption  );
end;


procedure TArchivos.AlarmaSeekClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas (*.RDY)|*.rdy|Todos los Archivos|*.*', 'Archivo de Alarma' );
end;


procedure TArchivos.OKClick(Sender: TObject);
begin
     inherited;

     if not ModoEjecucion then
     begin
        with LinxIni.IniValues do
        begin
             ProgDir1 := Self.ProgDir1.Text;
             ProgDir2 := Self.ProgDir2.Text;
             ProgDir3 := Self.ProgDir3.Text;
             Alarma := Self.Alarma.Text;
             Empleados := Self.Empleados.Text;
        end;
     end;
end;

procedure TArchivos.BuscarEmpleadosClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleados (*.RDY)|*.rdy|Todos los Archivos|*.*', 'Archivo de Empleados' );
end;

end.
