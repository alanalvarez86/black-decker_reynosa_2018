unit FUnitekPoll;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FLinx5Poll.pas                             ::
  :: Descripci�n: Programa principal de L5Poll.exe           ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Db, DBClient,
     Forms, Dialogs, ExtCtrls, Menus, StdCtrls, Buttons, ComCtrls, ComObj, ActiveX,
     LinxIni,
     FOnlineDLL,
     FLinxBase;

type
  TUnitechPoller = class(TLinxBase)
    N5: TMenuItem;
    N9: TMenuItem;
    TerminalesDirectorio: TMenuItem;
    TerminalEjecutar: TMenuItem;
    TerminalesConfiguracion: TMenuItem;
    ConfiguracionArchivos: TMenuItem;
    TerminalesEnviar: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConfiguracionClick(Sender: TObject);
    procedure ConfiguracionListaClick(Sender: TObject);
    procedure TerminalesProgramaClick(Sender: TObject);
    procedure TerminalesAlarmaClick(Sender: TObject);
    procedure TerminalesEmpleadosClick(Sender: TObject);
    procedure TerminalesConfiguracionClick(Sender: TObject);
    procedure TerminalesSincronizarClick(Sender: TObject);
    procedure TerminalesSincronizarTodasClick(Sender: TObject);
    procedure TerminalesResetearClick(Sender: TObject);
    procedure TerminalesRebootClick(Sender: TObject);
    procedure TerminalesVersionClick(Sender: TObject);
    procedure TerminalesDirectorioClick(Sender: TObject);
    procedure TerminalEjecutarClick(Sender: TObject);
    procedure ConfiguracionArchivosClick(Sender: TObject);
    procedure TerminalesEnviarClick(Sender: TObject);
  private
    { Private declarations }
     FOnlineMgr: TOnlineMgr;
  protected
    { Protected declarations }
    function DatosEnviados: String; override;
    function InitAllServers: Boolean; override;
    function Read: Boolean; override;
    function RegistroLeido: String; override;
    function RegistroNormal: Boolean; override;
    function Terminal: Integer; override;
    function VerifyAllServers: Boolean; override;
    procedure AutoSincronizarTodas( const dValue: TDate ); override;
    procedure DownAllServers; override;
    procedure ExecuteCmd( Command: Executer );
    procedure MakeNodeList; override;
    procedure ProcesarLectura( var sData: String ); override;
    procedure SendAlarm( const sFileName: String ); override;
    procedure SendEmpleados( const sFileName: String ); override;
    procedure SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
    procedure EscribeLecturaLinea( const sData: String );
    procedure EscribeAllData( const sData: String ); override;
  public
    { Public declarations }
    function BatchModeEnabled: Boolean; override;
    procedure MakeUnitList; override;
  end;

var
  UnitechPoller: TUnitechPoller;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FArchivos,
     FFileMgr,
     FEjecutador,
     FViewLog,
     FImportar,
     FSincronizar,
     FTerminales,
     FLinxTools;

{$R *.DFM}

{ ************* TUnitekPoller ************ }

procedure TUnitechPoller.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     K_TITULO = '%s Profesional';
     {$else}
     K_TITULO = '%s Corporativa';
     {$endif}
begin
     inherited;
     FOnlineMgr := TOnlineMgr.Create;
     with FOnlineMgr do
     begin
          OnError := ManejaError;
          OnMessage := ManejaBitacora;
          OnMakeUnitList := MakeUnitList;
          OnFeedBack := PostMessage;
          OnGaugeSetSize := GaugeSetSize;
          OnGaugeAdvance := GaugeAdvance;
          OnGaugeFinish := GaugeFinish;
          OnDoLectura := EscribeLecturaLinea;
          ValidTokens := TOKEN_ASISTENCIA;
     end;
     Self.Caption := Format( K_TITULO, [ Application.Title ] );
end;

procedure TUnitechPoller.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FOnlineMgr );
     inherited;
end;

function TUnitechPoller.BatchModeEnabled: Boolean;
begin
     Result := True;
end;

{ ********** Abrir Servidores ************ }

function TUnitechPoller.InitAllServers: Boolean;
var
   i: Integer;
begin
     Result := inherited InitAllServers;
     if Result then
     begin
          Result := FOnlineMgr.Init;
          if Result then
          begin
               for i := 1 to FOnlineMgr.Units do
               begin
                    with FOnlineMgr.Terminal[ i ] do
                    begin
                         if not IsConnected then
                         begin
                              Bitacora.WriteText( Format( 'Terminal %s ( %s ) Fuera De L�nea', [ Description, Identificador ] ) );
                         end;
                    end;
               end;
          end;
     end;
end;

{ ********** Procesamiento ********** }

procedure TUnitechPoller.ProcesarLectura( var sData: String );
begin
     inherited ProcesarLectura( sData );
end;

{ ********** Manejo de Terminales Unitek ********** }

function TUnitechPoller.DatosEnviados: String;
begin
     Result := FOnlineMgr.DatosEnviados;
end;

function TUnitechPoller.Read: Boolean;
begin
     Result := FOnlineMgr.Read;
end;

function TUnitechPoller.RegistroLeido: String;
begin
     Result := DatosEnviados;
end;

function TUnitechPoller.RegistroNormal: Boolean;
begin
     Result := FOnlineMgr.RegistroNormal;
end;

function TUnitechPoller.Terminal: Integer;
begin
     Result := FOnlineMgr.TerminalID;
end;

function TUnitechPoller.VerifyAllServers: Boolean;
begin
     MakeUnitList;
     Result := True;
end;

procedure TUnitechPoller.AutoSincronizarTodas(const dValue: TDate);
begin
     FOnlineMgr.SetDateTimeAll( dValue );
end;

procedure TUnitechPoller.MakeNodeList;
begin
     FOnlineMgr.MakeNodeList;
end;

procedure TUnitechPoller.MakeUnitList;
var
   i: Integer;
begin
     with UnitList.Items do
     begin
          BeginUpdate;
          Clear;
          for i := 1 to FOnlineMgr.Units do
          begin
               Add( FOnlineMgr.GetInfo( i ) );
               if BatchMode then
               begin
                    Bitacora.WriteText( Format( '%s - [ %s ]', [ FOnlineMgr.GetInfo( i ), FOnlineMgr.GetTerminalVersion( i ) ] ) );
               end;
          end;
          EndUpdate;
     end;
end;

procedure TUnitechPoller.ExecuteCmd( Command: Executer );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          SetCursorWait;
          for i := 0 to ( UnitList.Items.Count - 1 ) do
              if UnitList.Selected[ i ] then
              begin
                   Command( i + 1 );
              end;
          SetCursorNormal;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TUnitechPoller.SendAlarm(const sFileName: String);
var
   i: Integer;
   sAlarmFile, sAlarmFileOld, sAlarmFileDAT, sAlarmFileBUF: String;
   lOk: Boolean;
begin
     with FOnlineMgr do
     begin
          sAlarmFileOld := VACIO;
          sAlarmFileDAT := VACIO;
          for i := 1 to Units do
          begin
               sAlarmFile := VACIO;
               if ZetaCommonTools.StrLleno( sFileName ) then
                  sAlarmFile := sFileName
               else
               begin
                    if Assigned( Terminal[ i ] ) then
                    begin
                         sAlarmFile := Terminal[ i ].Alarma;
                    end;
               end;
               if ZetaCommonTools.StrLleno( sAlarmFile ) and FileExists( sAlarmFile ) then
               begin
                    lOk := True;
                    if ( sAlarmFile <> sAlarmFileOld ) then
                    begin
                         sAlarmFileBUF := sAlarmFile;
                         lOk := TransformarAlarma( sAlarmFileBUF );
                         if lOk then
                         begin
                              sAlarmFileOld := sAlarmFile;
                              sAlarmFileDAT := sAlarmFileBUF;
                         end
                         else
                         begin
                              Bitacora.WriteTimeStamp( Format( 'Terminal %s: Archivo De Alarmas %s No Pudo Ser Transformado a DAT', [ Terminal[ i ].Description, sAlarmFile ] ) + ' ( %s )' );
                         end;
                    end;
                    if lOk then
                    begin
                         if EnviarArchivoAlarma( i, sAlarmFileDAT ) then
                            Bitacora.WriteTimeStamp( Format( 'Alarma %s Enviado A %s', [ sAlarmFile, Terminal[ i ].Description ] ) + ' ( %s )' )
                         else
                             Bitacora.WriteTimeStamp( Format( 'Alarma %s NO FUE ENVIADO A %s', [ sAlarmFile, Terminal[ i ].Description ] ) + ' ( %s )' )
                    end;
               end
               else
                   if ZetaCommonTools.StrLleno( sAlarmFile ) and not FileExists( sAlarmFile) then
                   begin
                        Bitacora.WriteTimeStamp( Format( 'Terminal %s: Archivo De Alarmas %s No Existe', [ Terminal[ i ].Description, sAlarmFile ] ) + ' ( %s )' );
                   end;
          end;
     end;
end;

procedure TUnitechPoller.SendEmpleados(const sFileName: String);
var
   i: Integer;
   sEmployeeFile, sEmployeeFileOld, sEmployeeFileDAT, sEmployeeFileBUF: String;
   lOk: Boolean;
begin
     with FOnlineMgr do
     begin
          for i := 1 to Units do
          begin
               sEmployeeFile := VACIO;
               if ZetaCommonTools.StrLleno( sFileName ) then
                  sEmployeeFile := sFileName
               else
               begin
                    if Assigned( Terminal[ i ] ) then
                    begin
                         sEmployeeFile := Terminal[ i ].Empleados;
                    end;
               end;
               if ZetaCommonTools.StrLleno( sEmployeeFile ) and FileExists( sEmployeeFile ) then
               begin
                    lOk := True;
                    if ( sEmployeeFile <> sEmployeeFileOld ) then
                    begin
                         sEmployeeFileBUF := sEmployeeFile;
                         lOk := TransformarEmpleado( sEmployeeFileBUF );
                         if lOk then
                         begin
                              sEmployeeFileOld := sEmployeeFile;
                              sEmployeeFileDAT := sEmployeeFileBUF;
                         end
                         else
                         begin
                              Bitacora.WriteTimeStamp( Format( 'Terminal %s: Lista De Empleados %s No Pudo Ser Transformada a DAT', [ Terminal[ i ].Description, sEmployeeFile ] ) + ' ( %s )' );
                         end;
                    end;
                    if lOk then
                    begin
                         if EnviarArchivoEmpleados( i, sEmployeeFileDAT ) then
                            Bitacora.WriteTimeStamp( Format( 'Lista De Empleados %s Enviada A %s', [ sEmployeeFile, Terminal[ i ].Description ] ) + ' ( %s )' )
                         else
                             Bitacora.WriteTimeStamp( Format( 'Lista De Empleados %s No Fu� Enviada A %s', [ sEmployeeFile, Terminal[ i ].Description ] ) + ' ( %s )' )
                    end;
               end
               else
                   if ZetaCommonTools.StrLleno( sEmployeeFile ) and not FileExists( sEmployeeFile) then
                   begin
                        Bitacora.WriteTimeStamp( Format( 'Terminal %s: Lista De Empleados %s No Existe', [ Terminal[ i ].Description, sEmployeeFile ] ) + ' ( %s )' );
                   end;
          end;
     end;
end;

procedure TUnitechPoller.SendFile( Command: FileSender; sFileName: String; lAskForFile: Boolean );
var
   i: Integer;
begin
     if ( UnitList.SelCount > 0 ) then
     begin
          if NOT lAskForFile OR GetFileName( sFileName ) then
          begin
               if FileExists( sFileName ) then
               begin
                    Application.ProcessMessages;
                    SetCursorWait;
                    for i := 0 to ( UnitList.Items.Count - 1 ) do
                        if UnitList.Selected[ i ] then
                           Command( ( i + 1 ), sFileName );
                    SetCursorNormal;
               end
               else
                   zWarning( '� Archivo No Existe !', sFileName, 0, mbOK );
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TUnitechPoller.EscribeAllData( const sData: String );
begin
     // No hacer nada se agregar� con el evento onDoLectura
end;

procedure TUnitechPoller.EscribeLecturaLinea( const sData: String );
begin
     AsciiServer.WriteAllData( sData );
end;

{ ********** Cerrar Servidores ********** }

procedure TUnitechPoller.DownAllServers;
var
   i: Integer;
begin
     if BatchMode then
     begin
          for i := 1 to FOnlineMgr.Units do
          begin
               Bitacora.WriteText( Format( '%s - Lecturas [ %d ]', [ FOnlineMgr.GetInfo( i ), FOnLineMgr.Terminal[i].Lecturas ] ) );
          end;
     end;
     FOnlineMgr.Stop;
     inherited DownAllServers;
end;

{ ************ Eventos del Men� *********** }

procedure TUnitechPoller.ConfiguracionClick(Sender: TObject);
begin
     inherited;
     ConfiguracionLista.Enabled := not Recolectando;
     ConfiguracionArchivos.Enabled := not Recolectando;
end;

procedure TUnitechPoller.ConfiguracionArchivosClick(Sender: TObject);
begin
     inherited;
     with TArchivos.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TUnitechPoller.ConfiguracionListaClick(Sender: TObject);
begin
     inherited;
     with TCapturarTerminales.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TUnitechPoller.TerminalesProgramaClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendApplicationFile, IniValues.Programa, True );
end;

procedure TUnitechPoller.TerminalesAlarmaClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendParameterFile, IniValues.Alarma, True );
end;

procedure TUnitechPoller.TerminalesEmpleadosClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendEmployeeListFile, IniValues.Empleados, True );
end;

procedure TUnitechPoller.TerminalesConfiguracionClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendConfigFile, IniValues.Configuracion, True );
end;

procedure TUnitechPoller.TerminalesVersionClick(Sender: TObject);
begin
     inherited;
     ExecuteCmd( FOnlineMgr.ShowVersion );
end;

procedure TUnitechPoller.TerminalesDirectorioClick(Sender: TObject);
var
   FArchivos: TStrings;
   i, iSelected: Integer;
begin
     inherited;
     if ( UnitList.SelCount = 0 ) then
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK )
     else
         if ( UnitList.SelCount > 1 ) then
            ZetaDialogo.zWarning( '� Demasiadas Terminales !', 'Se Debe Escoger Una Sola Terminal', 0, mbOK )
         else
         begin
              iSelected := -1;
              FArchivos := TStringList.Create;
              try
                 SetCursorWait;
                 for i := 0 to ( UnitList.Items.Count - 1 ) do
                 begin
                      if UnitList.Selected[ i ] then
                      begin
                           iSelected := ( i + 1 );
                           FOnlineMgr.CargaDirectorio( iSelected, FArchivos );
                           Break;
                      end;
                 end;
                 SetCursorNormal;
                 if ( FArchivos.Count > 0 ) then
                 begin
                      FileManager := TFileManager.Create( Self );
                      try
                         with FileManager do
                         begin
                              TerminalIndex := iSelected;
                              OnlineMgr := FOnlineMgr;
                              CargarArchivos( FArchivos );
                              ShowModal;
                         end;
                      finally
                             FreeAndNil( FileManager );
                      end;
                 end
                 else
                     ZetaDialogo.zWarning( '� No Hay Archivos !', Format( 'La Terminal %s NO Tiene Archivos', [ FOnlineMgr.Terminal[ iSelected ].Description ] ), 0, mbOK )
              finally
                     FreeAndNil( FArchivos );
              end;
         end;
end;

procedure TUnitechPoller.TerminalEjecutarClick(Sender: TObject);
var
   FArchivos: TStrings;
   i, iSelected: Integer;
begin
     inherited;
     if ( UnitList.SelCount = 0 ) then
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK )
     else
         if ( UnitList.SelCount > 1 ) then
            ZetaDialogo.zWarning( '� Demasiadas Terminales !', 'Se Debe Escoger Una Sola Terminal', 0, mbOK )
         else
         begin
              iSelected := -1;
              FArchivos := TStringList.Create;
              try
                 SetCursorWait;
                 for i := 0 to ( UnitList.Items.Count - 1 ) do
                 begin
                      if UnitList.Selected[ i ] then
                      begin
                           iSelected := ( i + 1 );
                           FOnlineMgr.CargaDirectorio( iSelected, FArchivos );
                           Break;
                      end;
                 end;
                 SetCursorNormal;
                 if ( FArchivos.Count > 0 ) then
                 begin
                      ExecuteMgr := TExecuteMgr.Create( Self );
                      try
                         with ExecuteMgr do
                         begin
                              TerminalIndex := iSelected;
                              OnlineMgr := FOnlineMgr;
                              CargarArchivos( FArchivos );
                              ShowModal;
                         end;
                      finally
                             FreeAndNil( ExecuteMgr );
                      end;
                 end
                 else
                     ZetaDialogo.zWarning( '� No Hay Archivos !', Format( 'La Terminal %s NO Tiene Archivos', [ FOnlineMgr.Terminal[ iSelected ].Description ] ), 0, mbOK )
              finally
                     FreeAndNil( FArchivos );
              end;
         end;
end;

procedure TUnitechPoller.TerminalesEnviarClick(Sender: TObject);
begin
     inherited;
     SendFile( FOnlineMgr.SendAnyFile, '', True );
end;

procedure TUnitechPoller.TerminalesSincronizarClick(Sender: TObject);
var
   i: Integer;
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     if ( UnitList.SelCount > 0 ) then
     begin
          dDateTime := Now;
          if FSincronizar.GetDateTime( dDateTime, TRUE, lHoraSistema ) then
          begin
               SetCursorWait;
               for i := 0 to ( UnitList.Items.Count - 1 ) do
               begin
                    if UnitList.Selected[ i ] then
                    begin
                         if lHoraSistema then
                            dDateTime := Now;
                         FOnlineMgr.SetDateTime( ( i + 1 ), dDateTime );
                    end;
               end;
               SetCursorNormal;
          end;
     end
     else
         ZetaDialogo.zWarning( '� No Se Escogi� Ninguna Terminal !', 'No Hay Ninguna Terminal Seleccionada', 0, mbOK );
end;

procedure TUnitechPoller.TerminalesSincronizarTodasClick(Sender: TObject);
var
   i: Integer;
   dDateTime: TDateTime;
   lHoraSistema: Boolean;
begin
     inherited;
     dDateTime := Now;
     if FSincronizar.GetDateTime( dDateTime, TRUE, lHoraSistema ) then
     begin
          SetCursorWait;
          for i := 0 to ( UnitList.Items.Count - 1 ) do
          begin
               if lHoraSistema then
                  dDateTime := Now;
               FOnlineMgr.SetDateTime( ( i + 1 ), dDateTime );
          end;
          SetCursorNormal;
     end;
end;

procedure TUnitechPoller.TerminalesResetearClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Resetear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( FOnlineMgr.ResetUnit );
     end;
end;

procedure TUnitechPoller.TerminalesRebootClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( '� Atencion !', '� Desea Rebootear La Terminal Seleccionada?', 0, mbOK ) then
     begin
          ExecuteCmd( FOnlineMgr.RebootUnit );
     end;
end;

end.
