(*
BioPoll
============================================================================
This program automatically downloads Log file from selected networked
V-Stations (based on .INI file settings). Program may be triggered manually
or via Windows Task Manager.

Usage:
BioPoll /R[/D][/I][/V]

Available parameters are:
/R = Run (required parameter)
/D = Debug mode On
/I = Verify IP connectivity only (do not download Log)
/V = Visible mode On (otherwise program runs minimized)
============================================================================
Nov. 17, 2004. Ver 1.01 Released - E. Salazar - esalazar@adaconsultores.com
*)

unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Includes, ExtCtrls, FAutoServer, FAutoClasses;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Timer1: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FRangos: TStringList;
    FDigitosEmpleado: Integer;
    FLoop: integer;  // global loop
    FEsDEMO : boolean;
    FChecadasAcumuladas : integer;
    function ProgFilesOk(FName: string): boolean;
    function GetEmpresa( const iGafete: Integer ): String;
    procedure ExecuteProgram;
    procedure RangosCargar;
    procedure WriteOutput;
    procedure CheckParameters;
    procedure Connect(IP: string);
    procedure Disconnect;
    procedure DownloadTransactionLog;
    function DownloadOneTransaction : boolean;
    {$ifdef FALSE}
    procedure VerificarDEMO;
    {$endif}
    procedure LoadIni;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

const
  ProgCaption = 'BioPoll - %s';
  Copyright = '(c) Grupo TRESS';
  TOPE_DEMO = 25;

var
  SubNetMask, ConnectionTimeout, DevicePort, EraseLog: string;  // .INI Vars
  SubNetMaskL: cardinal;  // equivalent to longword 0..4294967295 unsigned 32-bit
  _D, _I, _R, _V: boolean;  // Program parameters
  ID_Strings: array[1..50] of string; // xxxx
  IP_Strings: array[1..50] of string; // xxx.xxx.xxx.xxx
  IDs, IPs: integer;
  NotRanBefore: boolean;
  fb: TextFile;  // BITACORA.TXT
  ConnectionOpen: boolean;
  TransactionCount: integer;
  MaxEntries: integer;
  rf: file of BII_Transaction_Log_Struct;
  myr: BII_Transaction_Log_Struct;

procedure TForm1.Connect(IP: string);
var
  x, netID: longint;

begin
  ConnectionOpen := False;
  if _D then
  begin
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add(FormatDateTime('hh:mm:ssAMPM',Now));
    Form1.Memo1.Lines.Add('Connecting to '+IP);
    Form1.Memo1.Lines.Add('Terminal ID='+ID_Strings[FLoop]);
  end;
  writeln(fb,'***** Connecting to '+IP+ '*****');
  writeln(fb,'Terminal ID='+ID_Strings[FLoop]);

  BII_Close_PC_Comm();  // also: BII_Close_PC_Comm

  // if BII_Is_Using_TCPIP = 0 then
  begin
    BII_Close_TCPIP_Communications();
    BII_Cleanup_Socket_Communications();
  end;

  netID := BII_Get_Current_NetID();
  {$ifdef ORIGINAL}
  x := BII_Set_Current_NetID(netID);  // Parameter is NetID
                                      // this one is set automatically
  {$else}
  BII_Set_Current_NetID(netID);  // Parameter is NetID
                                      // this one is set automatically
  {$endif}

  if _D and (BII_Set_Subnet_Mask(SubNetMaskL) <> 1) then
  begin
    Form1.Memo1.Lines.Add('Error setting subnet mask');
    writeln(fb,'Error setting subnet mask');
  end
  else
  begin
    {$ifdef ORIGINAL}
    x := BII_Set_TCP_Connection_Timeout(StrToInt(ConnectionTimeout));  // 5000
    {$else}
    if TryStrToInt( ConnectionTimeout, x ) then
       BII_Set_TCP_Connection_Timeout(x)  // 5000
    else
        BII_Set_TCP_Connection_Timeout(5000);  // 5000
    {$endif}
    x := BII_Initialize_Socket_Communications();
    if x <> 1 then
    begin
      Form1.Memo1.Lines.Add('Error initializing socket communications');
      writeln(fb,'Error initializing socket communications');
    end
    else
    begin
      // IP Sample Addresses:
      // 10.10.10.35
      // 10.10.10.40
      // DevicePort = 10001 (selected at random)
      x := BII_Open_TCPIP_Communications(PChar(IP), StrToInt(DevicePort));
      if x <> 1 then
      begin
        Form1.Memo1.Lines.Add('Error opening TCP Communications');
        writeln(fb,'Error opening TCP Communications');
      end
      else
      begin
        x := BII_Status;
        if x <> 0 then
        begin
          Form1.Memo1.Lines.Add('Unit is bussy');
          writeln(fb,'Unit is bussy');
        end
        else
        begin
          Form1.Memo1.Lines.Add('Unit responding Ok');
          writeln(fb,'Unit responding Ok');
          ConnectionOpen := True;
        end;

        if _D then
          Form1.Memo1.Lines.Add('Finalized connection procedure');
      end;
    end;
  end;
end;

procedure TForm1.DownloadTransactionLog;
var
  Number_Of_Entries: integer;
  ReadFlag, UpdateFlag, MaxFlag: integer;
  iResultado, i: integer;
  {$ifdef ORIGINAL}
  f: TextFile;
  PLog, PLogi: BII_Transaction_Log_Arr;
  {$else}
  PLog: BII_Transaction_Log_Arr;
  {$endif}
begin
     Plog := nil;
     //Number_Of_Entries := BII_Get_Num_Transaction_Log(0); //All entries in log
     Number_Of_Entries := BII_Get_Num_Transaction_Log(1); //Only un-read entries
     if _D then
       Form1.Memo1.Lines.Add('BII_Get_Num_Transaction_Log='+IntToStr(Number_Of_Entries));
     // -104: No Data was Received from V-Station

     if Number_Of_Entries < 0 then
     begin
       writeln(fb,'Error while getting transactions!');
       Form1.Memo1.Lines.Add('Error while getting transactions!');
     end//if
     else
     begin
          //Prueba del Sentinel
          if ( FEsDemo ) then
          begin
               Memo1.Lines.Add(Format( 'Se Recolectar�n Hasta %d Checadas Unicamente', [ TOPE_DEMO ] ));
               writeln(fb, Format( 'Se Recolectar�n Hasta %d Checadas Unicamente', [ TOPE_DEMO ] ) );
               //Limitar Lectura
               if Number_Of_Entries > TOPE_DEMO then
               begin
                    //Cortar Checadas Disponibles
                    MaxFlag := TOPE_DEMO;
               end
               else
               begin
                    MaxFlag := Number_Of_Entries;
               end;
          end//if ( EsDemo ) then
          else
          begin
               //MaxEntries := Number_Of_Entries;
               MaxFlag := Number_Of_Entries;
          end;//else

          //Asignar variable global
          MaxEntries := MaxFlag;
          TransactionCount := TransactionCount + MaxEntries;

          {$ifdef ORIGINAL}
          PLogi := PLog;
          {$else}
          {$endif}
          //PLog := AllocMem(SizeOf(BII_Transaction_Log_Struct)*MaxEntries);
          GetMem(PLog,SizeOf(BII_Transaction_Log_Struct)*MaxEntries);
          {$ifdef ORIGINAL}
          PLogi := @PLog;
          {$else}
          try
          {$endif}
             //ReadFlag := 0;  // read all entries (regardless of read status)
             ReadFlag := 1;  // Read only unread entries

             //UpdateFlag := 0;  // No updating will take place (Do not mark as read)
             UpdateFlag := 1;  // Mark as read

             //MaxFlag := -1;  // Read all transactions for the options set
             //MaxFlag := N;  // Read a maximum of N transactions

             //Lectura de Registros
             //=1 The command was successful.
             iResultado := BII_Read_Transaction_Log( ReadFlag, UpdateFlag, MaxFlag, PLog );

             // -601: Generic Exception Error
             {$ifdef ORIGINAL}
             if iResultado <> 1 then  // Error
               Number_Of_Entries := 0  // reset value; nothing will be downloaded
             else
             {$else}
             if ( iResultado = 1 ) then  // Exitoso
             {$endif}
             begin  // process records
               if _D then
                 Form1.Memo1.Lines.Add('# ID MONTH/DAY/YER HOUR:MIN:SEC');
               for i := (MaxEntries-1) downto 0 do  // inverse order
               begin
                 // NOTE: PLog[] holds records in the following order:
                 // from most recent to oldest. That's why we need to invert
                 if _D then
                   Form1.Memo1.Lines.Add(IntToStr(i)+' '+IntToStr(PLog[i].id)+' '+
                   IntToStr(Plog[i].month)+'/'+IntToStr(Plog[i].day)+'/'+IntToStr(Plog[i].year)+' '+
                   IntToStr(Plog[i].hour)+':'+IntToStr(Plog[i].min)+':'+IntToStr(Plog[i].sec));

                 myr := Plog[i];
                 write(rf,myr);
               end;
             end;
             //FreeMem(PLog,SizeOf(BII_Transaction_Log_Struct)*MaxEntries);
             {$ifdef ORIGINAL}
             FreeMem(Plog);
             {$else}
          finally
                 FreeMem( PLog );
          end;
          {$endif}
     end;//else if Number_Of_Entries < 0 then
end;

function TForm1.DownloadOneTransaction : boolean;
var
   Number_Of_Entries, iResultado, iChecadasPendientes, i : integer;
   PLog: BII_Transaction_Log_Arr;
begin
     PLog := nil;
     Result := False;

     AssignFile(rf,'BioPoll.rec');
     Rewrite(rf);

     Number_Of_Entries := BII_Get_Num_Transaction_Log(1); //Only un-read entries

     if Number_Of_Entries > 0 then
     begin
          MaxEntries := Number_Of_Entries;
          TransactionCount := TransactionCount + Number_Of_Entries;
          if ( FEsDemo ) then
          begin
               iChecadasPendientes := TOPE_DEMO - FChecadasAcumuladas;
          end
          else
          begin
               iChecadasPendientes := Number_Of_Entries;
          end;

          if iChecadasPendientes > 0 then
          begin
               GetMem( PLog, SizeOf(BII_Transaction_Log_Struct) * iChecadasPendientes ); //Reservar memoria
               try
                  iResultado := BII_Read_Transaction_Log( 1, 1, iChecadasPendientes, PLog ); //Leer registros pendientes
                  sleep(2000); //Esperar a que se refresquen los registros en el reloj.

                  if ( iResultado = 1 ) then
                  begin
                       if _D then Form1.Memo1.Lines.Add('# ID MONTH/DAY/YER HOUR:MIN:SEC');

                       for i := ( iChecadasPendientes - 1 ) downto 0 do  // inverse order
                       begin
                            // NOTE: PLog[] holds records in the following order:
                            // from most recent to oldest. That's why we need to invert
                            if _D then
                            begin
                                 Memo1.Lines.Add( IntToStr(i)+' '+IntToStr(PLog[i].id)+' '+
                                                  IntToStr(Plog[i].month)+'/'+IntToStr(Plog[i].day)+'/'+IntToStr(Plog[i].year)+' '+
                                                  IntToStr(Plog[i].hour)+':'+IntToStr(Plog[i].min)+':'+IntToStr(Plog[i].sec));
                            end;

                            myr := Plog[i];
                            write( rf, myr );

                            if ( ( myr.trans_code = 48 ) or //TL_VERIFY_ID
                                 ( myr.trans_code = 56 ) or //TL_IDENTIFY
                                 ( myr.trans_code = 49 ) ) and //SMART CARD
                                 ( myr.status = 1 ) then //Status = Success
                            begin
                                 //write( rf, myr );
                                 Result := True;
                            end;
                       end;//for
                  end//if ( iResultado = 1 ) then
                  else
                  begin
                       //Error
                       writeln(fb,'Error while getting transactions!');
                  end;
               finally
                      FreeMem( PLog );
               end;
          end;//if iChecadasPendientes > 0 then
     end;//if Number_Of_Entries > 0 then

     CloseFile(rf);
end;

procedure TForm1.WriteOutput;
var
  written: integer;
  Reloj, Empresa, Empleado: string;
  EmpresaN, EmpleadoN: integer;
  mes, dia, hora, min: string;
  mesi, diai, horai, mini: integer;
  f: TextFile;
begin
  AssignFile(f,'BioPoll.tmp');
  Append(f);  // this shouldn't fail
  Reset(rf);
  written := 0;
  repeat
        Read(rf, myr);

        Reloj := ID_Strings[FLoop];
        if Length(Reloj) > 4 then
          SetLength(Reloj, 4);
        while Length(Reloj) < 4 do
        Reloj := '0' + Reloj;

        EmpresaN := myr.id;
        {$ifdef ORIGINAL}
        if (EmpresaN >= 4000) and (EmpresaN < 6000) then
          Empresa := '0'
        else if (EmpresaN >= 1000) and (EmpresaN < 4000) then
          Empresa := '1'
        else if (EmpresaN >= 6000) and (EmpresaN < 9000) then
          Empresa := '2'
        else if (EmpresaN >= 9000) then
          Empresa := '3'
        else
          Empresa := '4';  // exception
        {$else}
        Empresa := GetEmpresa( EmpresaN );
        {$endif}

        EmpleadoN := myr.id;
        Empleado := IntToStr(EmpleadoN);
        {$ifdef ORIGINAL}
        if Length(Empleado) > 9 then
          SetLength(Empleado, 9);
        while Length(Empleado) < 9 do
          Empleado := '0' + Empleado;
        {$else}
        if ( Length(Empleado) > FDigitosEmpleado ) then
        begin
             Empleado := Copy( Empleado, ( Length( Empleado ) - FDigitosEmpleado + 1 ), FDigitosEmpleado );
        end;
        while ( Length(Empleado) < FDigitosEmpleado ) do
        begin
             Empleado := '0' + Empleado;
        end;
        {$endif}

        mesi := myr.month;
        mes := IntToStr(mesi);
        while Length(mes) < 2 do
          mes := '0' + mes;

        diai := myr.day;
        dia := IntToStr(diai);
        while Length(dia) < 2 do
          dia := '0' + dia;

        horai := myr.hour;
        hora := IntToStr(horai);
        while Length(hora) < 2 do
          hora := '0' + hora;

        mini := myr.min;
        min := IntToStr(mini);
        while Length(min) < 2 do
          min := '0' + min;

        // trans_code: Byte;  Action, 48=Verify ID and 56=Identify or 49=Smart Card
        // status: Byte;  CmdStatus, 1=Success, 0=Failure

        // Save only selected records
        if ( ( myr.trans_code = 48 ) or
             ( myr.trans_code = 56 ) or
             ( myr.trans_code = 49 ) ) and
             (myr.status = 1) then
        begin
             writeln(f,Reloj,'000000','@',Empresa,Empleado,'A',mes,dia,hora,min,'1');
             Inc(written);
        end;
  until EOF(rf);
  CloseFile(rf);
  CloseFile(f);

  FChecadasAcumuladas := FChecadasAcumuladas + written;

  //Verificar si es DEMO
  if FEsDEMO then
  begin
       if ( FChecadasAcumuladas < TOPE_DEMO ) then
       begin
            if DownloadOneTransaction then
            begin
                 WriteOutput;
            end;
       end;
  end;

  //writeln(fb,IntToStr( FChecadasAcumuladas ),' records written to RELOJ.DAT');
end;

procedure TForm1.Disconnect;
begin
  BII_Close_TCPIP_Communications();
  BII_Cleanup_Socket_Communications();

  Form1.Memo1.Lines.Add('Done disconnecting!');
  writeln(fb,'Done disconnecting!');
end;

procedure TForm1.LoadIni;
  var
    IniFile: TextFile;
    {$ifdef ORIGINAL}
    PText, PCaption: PChar;
    {$endif}
    s, s2: string;
    a, b, c, d: cardinal;
    myS: string;
    rc: integer;

  procedure Grab(var v: string; vn: string);
    var
      ss: string;
      p: integer;
  begin
    if Pos(vn, s) = 1 then
    begin
      p := Pos('=', s);
      ss := Copy(s, p+1, Length(s)-p);
      v := Trim(ss);
    end;
  end;

begin
  { Load INI file else create new }
  AssignFile(IniFile, 'BioPoll.ini');
  {$I-}
  Reset(IniFile);
  {$I+}
  if IOresult <> 0 then
  begin
    ShowMessage('Missing .INI file!'+#13+
      'Correct errror and try again!');
    Halt;
  end
  else
    CloseFile(IniFile);

  { assign default values }
  SubNetMask := '255.255.255.0';
  ConnectionTimeout := '5000';
  DevicePort := '10001';
  EraseLog := 'NO';
  { load INI file }
  IDs := 0;
  IPs := 0;
  Reset(IniFile);
  while not EOF(IniFile) do
  begin
    readln(IniFile,s);
    Grab(SubNetMask, 'SubNetMask');
    Grab(ConnectionTimeout, 'ConnectionTimeout');
    Grab(DevicePort, 'DevicePort');
    Grab(EraseLog, 'EraseLog');
    if Pos('ID',s) = 1 then
    begin
      Inc(IDs);
      Grab(s2, 'ID');
      ID_Strings[IDs] := s2;
    end;
    if Pos('IP',s) = 1 then
    begin
      Inc(IPs);
      Grab(s2, 'IP');
      IP_Strings[IPs] := s2;
    end;
  end;
  CloseFile(IniFile);

  // Convert SubNetMask string to SunNetMask numeric value
  myS := SubNetMask;
  s := Copy(myS,1,Pos('.',myS)-1);
  Val(s,a,rc);
  Delete(myS,1,Pos('.',myS));

  s := Copy(myS,1,Pos('.',myS)-1);
  Val(s,b,rc);
  Delete(myS,1,Pos('.',myS));

  s := Copy(myS,1,Pos('.',myS)-1);
  Val(s,c,rc);
  Delete(myS,1,Pos('.',myS));

  Val(myS,d,rc);

  // Default is: a := 255; b := 255; c := 255; d := 0;
  SubNetMaskL := (a*256*256*256) + (b*256*256) + (c*256) + d;
end;

procedure TForm1.ExecuteProgram;
var
  {$ifdef ORIGINAL}
  i, x: integer;
  {$else}
  i, iLoop: Integer;
  {$endif}
  f, f2: TextFile;
  s: string;

begin
  if _D or _V then
  begin
    Form1.Memo1.Clear;
    Form1.Memo1.Visible := True;
    if _D then
      Form1.Memo1.Lines.Add('Running in Debug Mode!')
    else
      Form1.Memo1.Lines.Add('Running in Visible Mode!');
    Form1.Memo1.Lines.Add('Program Name:');
    Form1.Memo1.Lines.Add(ParamStr(0));
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('Program Parameters:');
    for i := 1 to ParamCount do
      Form1.Memo1.Lines.Add(ParamStr(i));
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('IDs:');
    for i := 1 to IDs do
      Form1.Memo1.Lines.Add(ID_Strings[i]);
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('IPs:');
    for i := 1 to IPs do
      Form1.Memo1.Lines.Add(IP_Strings[i]);
  end;
  // #############################################################
  // #############################################################
  TransactionCount := 0;
  AssignFile(f,'BioPoll.tmp');   // prepare file; save this session here
  Rewrite(f);
  CloseFile(f);
  for iLoop := 1 to IPs do  // loop through all devices
  begin
    FLoop := iLoop;
    Connect(IP_Strings[FLoop]);
    if _I then  // verify IP connectivity only, do not download log!
    begin
      Form1.Memo1.Lines.Add('Transaction log not requested');
      writeln(fb,'Transaction log not requested');
    end
    else
    if ConnectionOpen then  // otherwise don't bother!
    begin
      AssignFile(rf,'BioPoll.rec');
      Rewrite(rf);
      writeln(fb,'Downloading transaction log');
      Form1.Memo1.Lines.Add('Downloading transaction log');
      DownloadTransactionLog;  // for current IP
      CloseFile(rf);
      if MaxEntries <> 0 then
      begin
        WriteOutput;
        Form1.Memo1.Lines.Add(IntToStr( TransactionCount )+' transactions downloaded');
        writeln(fb,(IntToStr( TransactionCount )+' transactions downloaded'));
        writeln(fb,IntToStr( FChecadasAcumuladas ),' records written to RELOJ.DAT');

        //TransactionCount := TransactionCount + MaxEntries;
        //Form1.Memo1.Lines.Add(IntToStr(MaxEntries)+' transactions downloaded');
        //writeln(fb,(IntToStr(MaxEntries)+' transactions downloaded'));

        if (UpperCase(EraseLog) = 'YES') or (UpperCase(EraseLog) = 'Y') then
        begin
          {$ifdef ORIGINAL}
          //x := BII_Erase_Transaction_Log(0, 0);  // Erase All=0, async=0
          x := BII_Erase_Transaction_Log(1, 0);  // Erase only read transactions=1, async=0
          {$else}
          //BII_Erase_Transaction_Log(0, 0);  // Erase All=0, async=0
          BII_Erase_Transaction_Log(1, 0);  // Erase only read transactions=1, async=0
          {$endif}
          //writeln(fb,'All transactions deleted from terminal!');
          writeln(fb,'All readed transactions deleted from terminal!');
          //Form1.Memo1.Lines.Add('All transactions deleted from terminal!');
          Memo1.Lines.Add('All readed transactions deleted from terminal!');
        end;
      end;
    end;
    Disconnect;  // close anhow (even if not open)
  end;
  // Start file processing
  if TransactionCount <> 0 then  // do file processing
  begin
    if _D then
      Form1.Memo1.Lines.Add('Start file handling process');
    // Append all recs. polled during this session to MIENT.MIE
    AssignFile(f,'BioPoll.tmp');
    Reset(f);  // worst case this file is empty
    AssignFile(f2,'MIENT.MIE');
    {$I-}
    Append(f2);
    {$I-}
    if IOResult <> 0 then
      Rewrite(f2);
    // Time-Stamp it!
    writeln(f2,'**********'+FormatDateTime('dd/mmm/yyyy hh:mm:ssAMPM',Now)+'**********');
    repeat
      readln(f,s);
      if length(s) <> 0 then
        writeln(f2,s);
    until EOF(f);
    CloseFile(f);
    CloseFile(f2);

    if FileExists('RELOJ.DAT') then
    begin
      // Rename files
      if FileExists('MIENT.003') then
        DeleteFile('MIENT.003');  // this one is gone
      if FileExists('MIENT.002') then
        RenameFile('MIENT.002','MIENT.003');  // Old, New; 002 becomes 003
      if FileExists('MIENT.001') then
        RenameFile('MIENT.001','MIENT.002');  // 001 becomes 002
      RenameFile('RELOJ.DAT','MIENT.001');  // .DAT becomes 001

      // now let's do: RELOJ.DAT = BioPoll.tmp
      AssignFile(f,'BioPoll.tmp');
      Reset(f);
      AssignFile(f2,'RELOJ.DAT');
      Rewrite(f2);
      repeat
        readln(f,s);
        if length(s) <> 0 then
          writeln(f2,s);
      until EOF(f);
      CloseFile(f);
      CloseFile(f2);

      DeleteFile('BioPoll.tmp');
    end
    else  // there's no RELOJ.DAT
    begin
      RenameFile('BioPoll.tmp','RELOJ.DAT');  // .TMP becomes .DAT
    end;
    if _D then
      Form1.Memo1.Lines.Add('Done with files!');
  end;
  // #############################################################
  // #############################################################
end;

procedure TForm1.CheckParameters;
var
  i: integer;
begin
  _D := False;
  //_D := True;  // ???????
  _I := False;
  _R := False;
  _V := False;
  for i := 1 to ParamCount do
  begin
    // Standard search
    if UpperCase(ParamStr(i)) = '/D' then _D := True;
    if UpperCase(ParamStr(i)) = '/I' then _I := True;
    if UpperCase(ParamStr(i)) = '/R' then _R := True;
    if UpperCase(ParamStr(i)) = '/V' then _V := True;
    // Alternate search (no spaces between parameters)
    if Pos('/D',UpperCase(ParamStr(i))) <> 0 then _D := True;
    if Pos('/I',UpperCase(ParamStr(i))) <> 0 then _I := True;
    if Pos('/R',UpperCase(ParamStr(i))) <> 0 then _R := True;
    if Pos('/V',UpperCase(ParamStr(i))) <> 0 then _V := True;
  end;
end;

{$ifdef FALSE}
procedure TForm1.VerificarDEMO;
var
   //Variables de la prueba de Sentinel
   FAutoServer: TAutoServer;
begin
     FEsDEMO := True;

     //Crear Sentinel
     FAutoServer := TAutoServer.Create;
     try
        //FAutoServer.OkModulo(okL5Poll);
        with FAutoServer do
        begin
             {$ifdef DOS_CAPAS}
             SQLType := engInterbase;
             AppType := atProfesional;
             {$endif}

             {$ifdef MSSQL}
             SQLType := engMSSQL;
             AppType := atCorporativa;
             {$endif}

             //Cargar Parametros de Sentinel
             Cargar;

             //Prueba del Sentinel
             //FEsDEMO := EsDemo;
             FEsDEMO := EsDemo or ( not OkModulo( okL5Poll, True ) );

        end;//with FAutoServer do
     finally
            FreeAndNil( FAutoServer );
     end;
end;
{$endif}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FChecadasAcumuladas := 0;
  //VerificarDEMO;    //Marco: Se estar� as� s�lo por ahora.
  FEsDEMO := FALSE;    //Marco: Quitar esto cuando se resuelva el defecto.
  FRangos := TStringList.Create;
  NotRanBefore := False;
  Caption := ' ' + Format( ProgCaption, [{$ifdef DOS_CAPAS}'Profesional'{$endif} {$ifdef MSSQL}'Corporativa'{$endif}] ) + ' ' + Copyright;
  Memo1.Clear;
  Memo1.ReadOnly := True;
  Memo1.Visible := False;
  CheckParameters;
  if not(_R) then
  begin
    Memo1.Visible := True;
    Form1.Memo1.Lines.Add('Usage:'); // Display Usage (help)
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('BioPoll /R[/D][/I][/V]');
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('Available parameters are:');
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('/R = Run (required parameter)');
    Form1.Memo1.Lines.Add('/D = Debug mode On');
    Form1.Memo1.Lines.Add('/I = Verify IP connectivity only (do not download Log)');
    Form1.Memo1.Lines.Add('/V = Visible mode On (otherwise program runs minimized)');
    Form1.Memo1.Lines.Add('');
    Form1.Memo1.Lines.Add('Please close window to terminate program!');
  end
  else
  begin
    if not(_V) then
      Form1.WindowState := wsMinimized;
    LoadIni;
    NotRanBefore := True;
  end;
  RangosCargar;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRangos );
end;

procedure TForm1.RangosCargar;
const
     K_DIGITOS_CREDENCIAL = 'Digitos';
     K_DIGITOS_CREDENCIAL_DEFAULT = 9;
var
   i: Integer;
   sRango, sEmpresa: String;
begin
     FDigitosEmpleado := K_DIGITOS_CREDENCIAL_DEFAULT;
     with FRangos do
     begin
          LoadFromFile( ExtractFilePath( Application.ExeName ) + 'BioPoll.rng' );
          Sort;
          for i := 0 to ( Count - 1 ) do
          begin
               if ( Names[ i ] = K_DIGITOS_CREDENCIAL ) then
                  FDigitosEmpleado := StrToIntDef( Values[ K_DIGITOS_CREDENCIAL ], K_DIGITOS_CREDENCIAL_DEFAULT )
               else
               begin
                    sRango := Names[ i ];
                    sEmpresa := Values[ sRango ];
                    Strings[ i ] := sEmpresa;
                    Objects[ i ] := TObject( StrToIntDef( sRango, 0 ) );
               end;
          end;
     end;
end;

function TForm1.GetEmpresa( const iGafete: Integer ): String;
const
     K_EMPRESA_DEFAULT = '0';
var
   i, iInferior: Integer;
begin
     Result := K_EMPRESA_DEFAULT;
     with FRangos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               iInferior := Integer( Objects[ i ] );
               if ( iGafete >= iInferior ) then
                  Result := Strings[ i ]
               else
                   Break;
          end;
     end;
end;

function TForm1.ProgFilesOk(FName: string): boolean;
var
  Ok: boolean;
  f: TextFile;
begin
  Ok := True;
  if FileExists(FName) then
  begin
    AssignFile(f,FName);
    {$I-}
    Reset(f);
    {$I-}
    if IOresult <> 0 then
      Ok := False
    else
      CloseFile(f);
  end;
  Result := Ok;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
  // Form1.Show;
  // Application.ProcessMessages;
  if NotRanBefore then
  begin
    NotRanBefore := False;
    Timer1.Enabled := False;  // not needed anymore
    if ProgFilesOk('BITACORA.TXT') and ProgFilesOk('RELOJ.DAT')
      and ProgFilesOk('MIENT.MIE') and ProgFilesOk('MIENT.001')
      and ProgFilesOk('MIENT.002') and ProgFilesOk('MIENT.003')
    then
    begin
      AssignFile(fb,'BITACORA.TXT');
      {$I-}
      Append(fb);
      {$I-}
      if IOResult <> 0 then
        Rewrite(fb);
      writeln(fb,'>>>>> Program starts '+FormatDateTime('dd/mmm/yyyy hh:mm:ssAMPM',Now)+' >>>>>');
      ExecuteProgram;
      writeln(fb,'<<<<< Program ends '+FormatDateTime('dd/mmm/yyyy hh:mm:ssAMPM',Now)+' <<<<<');
      CloseFile(fb);
      if _D and _V then
      begin
        Form1.Memo1.Lines.Add(FormatDateTime('hh:mm:ssAMPM',Now));
        Form1.Memo1.Lines.Add('Done!');
        Form1.Memo1.Lines.Add('');
        Form1.Memo1.Lines.Add('Please close window to terminate program!');
        ShowMessage('Done!');
      end
      else
        Halt(0);  // The End
    end
    else
      ShowMessage('An application file is locked! Program cannot run!');
  end;
end;

end.

