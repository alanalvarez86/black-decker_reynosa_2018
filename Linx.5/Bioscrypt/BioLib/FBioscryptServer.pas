unit FBioscryptServer;

interface

uses
{$ifdef WIN32}
  Windows;
  //BIITypes,
  //BIIDefines;
{$else}
  Wintypes,
  WinProcs;
{$endif}

const
     { Constantes BIIDEFINES}
     { Non Error Related Macros for MV1100 }
     NUM_CORE                  = 3;
     RAW_IMAGE_SIZE_X          = 300;
     RAW_IMAGE_SIZE_Y          = 300;
     PACKED_ARRAY_SIZE         = 5500;
     WEEKLY_SCHEDULE_SIZE      = 28;
     FIRST_USABLE_SCHEDULE     = 1;
     MAX_WEEKLY_SCHEDULES_USER = 58;
     TOTAL_WEEKLY_SCHEDULES    = 64;
     MAX_HOLIDAY_SCHEDULES     = 50;
     MAX_USABLE_HOLIDAYS       = 46;
     WEEKLY_SCHEDULE_NEVER     = 0;
     WEEKLY_SCHEDULE_ALWAYS    = 63;
     HOLIDAYS_OBSERVED_FLAG    = $80;
     UNDEFINED_DAY_SCHEDULE    = $FFFFFFF;
     ESI_UNUSED_BLOCK          = $00000000;
     ESI_TEMPLATE_1_BLOCK      = $00000001;
     ESI_TEMPLATE_2_BLOCK      = $00000002;
     ESI_DATA_BLOCK            = $00000004;
     ESI_ADMIN_BLOCK           = $00000005;
     ESI_LAYOUT_BLOCK          = $00000006;
     ESI_LAYOUT_POINTER_BLOCK  = $00000007;
     BII_FLOW_HOST_RECEPTION   = 1;
     BII_FLOW_MV1100_RECEPTION = 2;
     BII_XON_XOFF              = 1;
     BII_NO_FLOW               = 0;
     BII_XOFF_DEF              = char( 2 );
     BII_XON_DEF               = char( 3 );
     BII_TXCLEAR               = 1;
     BII_RXCLEAR               = 0;
     BII_FINGER_PRESENT        = 2;
     BII_AUTO_FINGER_DETECT_ENABLED_TEMP = $FFFFFFF;
     BII_AUTO_FINGER_DETECT_ENABLED      = 1;
     BII_AUTO_FINGER_DETECT_DISABLED     = 0;
     BII_HOST_PORT       = 0;
     BII_AUX_PORT        = 1;
     BII_RS232           = 0;
     BII_RS485           = 1;
     BII_LED_OFF         = 0;
     BII_LED_GREEN       = 1;
     BII_LED_RED         = 2;
     BII_LED_AMBER       = 3;
     BII_LED_FLASH_GREEN = -1;
     BII_LED_FLASH_RED   = -2;
     BII_LED_FLASH_AMBER = -3;

{ Tipos BIITYPES }
type
  BII_Template = record
    id: Cardinal;
    employee_id: Cardinal;
    password: Cardinal;
    sensor_version: Byte;
    template_version: Byte;
    name: Array[0..16-1] of Char;
    finger: Byte;
    admin_level: Byte;
    schedule: Byte;
    security_thresh: Byte;
    noise_level: Array[0..18-1] of Byte;
    corramb: Array[0..NUM_CORE-1] of Byte;
    ihcore: Array[0..NUM_CORE-1] of Byte;
    ivcore: Array[0..NUM_CORE-1] of Byte;
    temp_xoffset: Byte;
    temp_yoffset: Byte;
    index: Byte;
    inphase:Array[0..PACKED_ARRAY_SIZE-1] of Byte;
  end {BII_Template};

type
  BII_Uncomp_Img = record
  image:Array[0..9216] of Byte;
 end;

type
  BII_Raw = record
    nx_pix: Integer;
    ny_pix: Integer;
    bits_per_pixel: Integer;
    p_image: array of byte;
  end {BII_Raw};

type
  BII_Full_Raw = record
    nx_pix: Integer;
    ny_pix: Integer;
    bits_per_pixel: Integer;
    image:array[0..RAW_IMAGE_SIZE_Y*RAW_IMAGE_SIZE_X] of Byte;
  end {BII_Full_Raw};

{BII_Template_Admin_Type supplants old non-convention name TemplateAdminType) }
type
  TBII_Template_Admin_Struct = record
    ID: Cardinal;
    Index: Integer;
    Admin_Level: Integer;
  end {BII_Template_Admin_Struct};

type
 BII_Template_Admin_Struct = record
  Lista:Array[0..200] of TBII_Template_Admin_Struct;
  end;

{**///BII_Wiegand_Type supplants old non-convention name WiegandType)}
type
  BII_Wiegand_Struct = record
    Input_Wiegand_Format: Cardinal;
    Output_Wiegand_Format: Cardinal;
    Output_Action: Cardinal;
    Output_Fail_Code_0: Cardinal;
    Output_Fail_Code_1: Cardinal;
    Output_Site_Code: Cardinal;
    Output_Pulse_Width: Cardinal;
    Output_Pulse_Interval: Cardinal;
  end {BII_Wiegand_Struct};

{///Revision 1 sensor parameter structure }
type
  BII_Sensor_Param = record
    discharge_time: Cardinal;
    discharge_current: Cardinal;
    sensor: Integer;
  end {BII_Sensor_Param};


type
  BII_Flow_Control_Struct = record
    flowType: Char;
    flowDir: Char;
    transmitOn: Char;
    transmitOff: Char;
  end {BII_Flow_Control_Struct};


type
  BII_LED_State_Table = record
    state: Array[0..12-1] of Cardinal;
  end {BII_LED_State_Table};

type
  BII_LED_Table = record
    group: Array[0..13-1] of BII_LED_STATE_TABLE;
  end {BII_LED_Table};

type
  BII_Verify_Response_Struct = record
    host: Integer;
    aux: Integer;
    wiegand: Integer;
    line_trigger: Integer;
    reserved2: Integer;
  end {BII_Verify_Response_Struct};

type
  BII_Port_Enable_Struct = record
    auxState: Integer;
    auxPassword: Integer;
    wiegandState: Integer;
    wiegandRes: Integer;
    reserved1: Integer;
    reserved2: Integer;
    reserved3: Integer;
    reserved4: Integer;
  end {BII_Port_Enable_Struct};


{///For Veridicom Sensors only }
type
  BII_Calibrate_Res = record
    dischargTime: Integer;
    dischargeCurrent: Integer;
    signal: Integer;
  end {BII_Calibrate_Res};


type
  BII_Wiegand_Format = record
    totalLength: Integer;
    bitFormat: Integer;
    idStartBit: Integer;
    idLength: Integer;
    siteStartBit: Integer;
    siteLength: Integer;
    parityType: Integer;
    parityFlags: Cardinal;
    evenParityBit: Integer;
    oddParityBitTwo: Integer;
    evenParityBitTwo: Integer;
    oddParityBit: Integer;
    defOutPulseWidth: Cardinal;
    defOutPulseInterval: Cardinal;
    reserved_1: Cardinal;
    reserved_2: Cardinal;
    reserved_3: Cardinal;
    reserved_4: Cardinal;
  end {BII_Wiegand_Format};

type
  BII_Unit_Type = record
    comparison: Cardinal;
    product: Cardinal;
    reserved2: Cardinal;
  end {BII_Unit_Type};

type
  BII_AUTO_FDETECT = record
    currentState: Cardinal;
    storedState: Cardinal;
    reserved1: Cardinal;
    reserved2: Cardinal;
  end {BII_AUTO_FDETECT};


type
  BII_Wiegand_ID_List = record
    ID_1: Cardinal;
    Type_1: Cardinal;
    ID_2: Cardinal;
    Type_2: Cardinal;
    ID_3: Cardinal;
    Type_3: Cardinal;
    ID_4: Cardinal;
    Type_4: Cardinal;
  end {BII_Wiegand_ID_List};

type
  Product_Type_Struct = record
    Key: Cardinal;
    Mode: Cardinal;
    Finger_Detect: Cardinal;
    Search_Range: Cardinal;
    Typea: Cardinal;
    Res_1: Cardinal;
  end {Product_Type_Struct};

type
  BII_GPI_States = record
    GPI0_Current_State: Cardinal;
    GPI1_Current_State: Cardinal;
    GPI0_Latched_State: Cardinal;
    GPI1_Latched_State: Cardinal;
    reserved0: Cardinal;
    reserved1: Cardinal;
  end {BII_GPI_States};

type
  BII_Default_GPO_States = record
    GPO0_State: Cardinal;
    reserved0: Cardinal;
    reserved1: Cardinal;
    reserved2: Cardinal;
    GPO1_State: Cardinal;
    reserved3: Cardinal;
    reserved4: Cardinal;
    reserved5: Cardinal;
  end {BII_Default_GPO_States};

type
  BII_GPO_State = record
    Line: Cardinal;
    State: Cardinal;
    Duration: Cardinal;
    reserved0: Cardinal;
    reserved1: Cardinal;
    reserved2: Cardinal;
  end {BII_GPO_State};

type
  BII_Reset_GPO_Struct = record
    Line: Cardinal;
    reserved0: Cardinal;
    reserved1: Cardinal;
  end {BII_Reset_GPO_Struct};

type
  BII_GPO_State_All = record
    GPO0_State: Cardinal;
    GPO1_State: Cardinal;
    reserved0: Cardinal;
    reserved1: Cardinal;
  end {BII_GPO_State_All};

type
  BII_SetSerialNumber_Struct = record
    password: Cardinal;
    SerialNumber: Array[0..80-1] of Char;
  end {BII_SetSerialNumber_Struct};

type
  BII_GetSerialNumber_Struct = record
    SerialNumber: Array[0..80-1] of Char;
  end {BII_GetSerialNumber_Struct};

type
  BioAPI_BIR_VERSION = Byte;
type
  BioAPI_BIR_DATA_TYPE = Byte;
type
  BioAPI_QUAILTY = Char;
type
  BioAPI_BIR_PURPOSE = Byte;
type
  BioAPI_BIR_AUTH_FACTORS = Cardinal;

{type
  BII_tagBio_API_BIR_Header = record
    ulLength: Cardinal;
    HeaderVersion: BIOAPI_BIR_VERSION;
    ;: Integer;
    Type: BIOAPI_BIR_DATA_TYPE;
    uhwFormatOwner: Word;
    uhwFormatID: Word;
    Quality: BIOAPI_QUAILTY;
    Purpose: BIOAPI_BIR_PURPOSE;
    FactorsMask: BIOAPI_BIR_AUTH_FACTORS;
  end {BII_tagBio_API_BIR_Header;}

{+// New struct for ESI Smartcard Layouts*/ }

type
  BII_Smartcard_Layout_Struct = record
    BlockType: Array[0..48-1] of Cardinal;
  end {BII_Smartcard_Layout_Struct};

type
  BII_IClass_Layout_Struct = record
    PageUse: Array[0..16-1] of Byte;
  end {BII_IClass_Layout_Struct};

type
  BII_IClass_Smartcard_Info = record
    numApplicationAreas: Cardinal;
    pageLength: Cardinal;
    lockedAppAreas: Cardinal;
    fuseValue: Cardinal;
  end {BII_IClass_Smartcard_Info};

type
  BII_Smartcard_Manufacturer_Info = record
    serialNumber: Array[0..8-1] of Byte;
  end {BII_Smartcard_Manufacturer_Info};

type
  BII_Verification_Queue_Entry = record
    action: Cardinal;
    id: Cardinal;
    index: Cardinal;
    quality: Cardinal;
    content: Cardinal;
    score: Cardinal;
    wiegandID: Array[0..8-1] of Byte;
  end {BII_Verification_Queue_Entry};

type
  BII_Transaction_Log_Struct = record
    id: Cardinal;
    reserved_1: Byte;
    index: Byte;
    year: Word;
    month: Byte;
    day: Byte;
    hour: Byte;
    min: Byte;
    sec: Byte;
    trans_code: Byte;
    flag_port: Byte;
    trans_log_data_1: Byte;
    trans_log_data_2: Byte;
    trans_log_data_3: Byte;
    reserved_2: Byte;
    status: Byte;
  end {BII_Transaction_Log_Struct};

type
  BII_DateTime_Struct = record
    year: Word;
    month: Byte;
    day: Byte;
    hour: Byte;
    min: Byte;
    sec: Byte;
  end {BII_DateTime_Struct};


{/// DateTime Settings Struct for V-Station }
type
  BII_DateTimeSettings_Struct = record
    twentyFourHourFormat: Cardinal;
    observeDaylightSavings: Cardinal;
    timeFormat: Cardinal;
    dateFormat: Cardinal;
    GMTDifferential: Integer;
    reserved_one: Cardinal;
    reserved_two: Cardinal;
    reserved_three: Cardinal;
  end {BII_DateTimeSettings_Struct};

{/// Extended ID Struct }
type
  BII_Ext_Template_Admin_Struct = record
    ID: Cardinal;
    ExtendedID: Array[0..8-1] of Byte;
    Index: Byte;
    Admin_Level: Byte;
    reserved1: Byte;
    reserved2: Byte;
  end {BII_Ext_Template_Admin_Struct};

type
  BII_GPO_Table = record
    GPO_0_Action: Cardinal;
    GPO_1_Action: Cardinal;
    GPO_0_AND_1_Action: Cardinal;
    reserved_1: Cardinal;
    reserved_2: Cardinal;
    reserved_3: Cardinal;
  end {BII_GPO_Table};

type
  BII_GPI_Table = record
    GPI_0_Action: Cardinal;
    GPI_1_Action: Cardinal;
    GPI_0_AND_1_Action: Cardinal;
    reserved_1: Cardinal;
    reserved_2: Cardinal;
    reserved_3: Cardinal;
  end {BII_GPI_Table};


type
  BII_Holiday_Schedule_Struct = record
    year: Byte;
    month: Byte;
    day: Byte;
    reserved: Byte;
    schedule: Cardinal;
  end {BII_Holiday_Schedule_Struct};

const
     { Constantes DIMENSIONS }
     NXMAX    = 96;
     NYMAX    = 128;
     NUM_MP   = 36;

     //BII_CURRENT_PORT    = $FFFFFFF;

   {$ifndef _BiiError_h}
   {$define _BiiError_h}
     { Constantes BIIERROR }
     { *Error Codes Generated by the V1100* }
     { *(-1 to -99 reserved for V1100)* }
     BII_ERR_SENT_UNKNWN_COMMAND           = -1; {/* The Command ID Sent to the V1100 was not known*/}
     BII_ERR_SENT_CKSUM                    = -2; {/* The Chksum sent to the V1100 Did not match the data it received*/}
     BII_ERR_SENT_LEN                      = -3; {/* Invalid Packet Length specified in data sent to the V1100*/}
     BII_ERR_MV1100_BUSY                   = -4; {/* Not enough resources on V1100 To accept the command*/}
     BII_ERR_SENT_OVERFLOW                 = -4; {/* Former meaning of -4, superceded by BII_ERR_MV1100_BUSY*/}
     BII_ERR_SENT_UNKNOWN                  = -5; {/* An non-specified error occured on the V1100*/}
     BII_ERR_DOWNLOAD_TMO                  = -6;
     BII_ERR_INVAL_BAUD                    = -7;
     BII_ERR_INVAL_STOP_BITS               = -8;
     BII_ERR_INVAL_NETID                   = -9;
     BII_ERR_SENSOR_PARAM                  = -10;
     BII_ERR_INVAL_TEMP_ID                 = -11;
     BII_ERR_INVAL_TEMP_INDEX              = -12;
     BII_ERR_WRITING_DATA                  = -13;
     BII_ERR_STORE_EXHAUST                 = -14;
     BII_ERR_INVAL_THRESH                  = -15;
     BII_ERR_NO_FINGER_DETECT              = -16; {/* Expected user to places finger, but user did not*/}
     BII_ERR_READING_DATA                  = -17;
     BII_ERR_LIST_SIZE                     = -18; {/* A list sent to the device too large*/}
     BII_ERR_WIEGAND_ID_EXIST              = -19; {/* A Wiegand Card ID is already assigned a different use*/}
     BII_ERR_TABLE_FULL                    = -20; {/* Attempted to add an element to a full table*/}
     BII_ERR_MEM_ALLOC                     = -21; {/* On baord MV1100 Memory allocation error*/}
     BII_ERR_INVAL_TEMP_TYPE               = -22; {/* An invalid Template type was sent to the unit*/}
     BII_ERR_TMO                           = -23; {/* MV1100 timed out while waiting for user action (such as a card swipe)*/}
     BII_ERR_INVAL_OPTION                  = -24; {/* Invalid option specified on a command to the MV1100*/}
     BII_ERR_MAX_INDEX                     = -25; {/* Maximum number of indexes (255) have been exceeded*/}
     BII_ERR_INVAL_DATA_SIZE               = -26; {/* Data size that is downloaded to the unit is to large*/}
     BII_ERR_UPDATING_TEMPLATES            = -27;
     BII_ERR_ADMIN_LEVEL_DIFFERS           = -28; {/* Mismatch with the current template admin level and those stored under the same ID*/}
     BII_ERR_INVAL_WIEGAND_FORMAT          = -32; {// The requested wiegand format is invalid}
     BII_ERR_INVAL_SENSOR                  = -33; {// command not appropriate for current sensor type}
     BII_ERR_INVAL_KEY                     = -34; {// Key is not correct. Used for lockable functions}
     BII_ERR_INPUT_LOCKED                  = -35; {// The input option is locked, for wiegand struct (input format)}
     BII_ERR_OUTPUT_LOCKED                 = -36; {// The output option is locked. for wiegand struct (output format)}
     BII_ERR_INVAL_WGND_FMT_COMBO          = -37; {// The input and ooutput wiegand formats are not a valid combination}
     BII_ERR_INVAL_PROG                    = -38; {// An Invalid Loader File was sent to the MV1100}
     BII_ERR_IMAGE_ACQUIRE                 = -39; {// problem acquiring an image from the sensor}
     BII_ERR_PORT_LOCKED                   = -40; {// Port locked}
     BII_ERR_PORT_LOCKED_PASSWORD          = -41; {// Port locked need to supply password to unlock the port}
     BII_ERR_OPERATION_FAILED              = -42; {// Generic Operation Failed Error}
     BII_ERR_FINGER_ON_SENSOR              = -43; {// Finger was on the sensor when it should not be on sensor}
     BII_ERR_PIC_NOT_SUPPORT               = -44; {// The option is not supported due to the PIC firmware}
     BII_ERR_TEMP_NOT_IDENTIFY             = -45;
     BII_ERR_TEMP_NOT_ONE_TO_ONE           = -46;
     BII_ERR_INVALID_SECURITY              = -47; {// Invalid template security level}
     BII_ERR_INVALID_ENROLL_TYPE           = -49; {// The MV1100 received an invalid enroll wiegand type}
     BII_ERR_INVALID_DELETE_TYPE           = -50; {// The MV1100 received an invalid delete wiegand type}
     BII_ERR_UNSUPPORTED_SENSOR_TYPE       = -51; {// The MV1100 received a template with an unsupported template type}
     BII_ERR_MV1100_ESI_TIMEOUT            = -52; {// The ESI did not respond within the defined timeout}
     BII_ERR_ESI_INVALID_RESPONSE          = -53; {// The ESI response packet was invalid (could not understand packet)}
     BII_ERR_INVALID_V_SERIES_PORT_MODE    = -54;
     BII_ERR_INVALID_V_SERIES_GPO_MODE     = -55;
     BII_ERR_INVALID_PRODUCT_NOT_V_SERIES  = -56;
     BII_ERR_INVALID_V_SERIES_FUNCTION     = -57;
     BII_ERR_ESI_BOOT_UP_DELAY             = -58;
     BII_ERR_INVALID_V_SMART_PORT_MODE     = -59;
     BII_ERR_PROG_NOT_COMPAT_CPLD          = -60;
     BII_ERR_ESI_INVALID_HOST_MODE         = -61;
     BII_ERR_INVALID_WIEGAND_ACTION        = -62; {// some options are not availabled with pass-thru mode}
     BII_ERR_INVALID_ESI_TYPE              = -63; {// invalid ESI firmware used in programming}
     BII_ERR_FIRM_REVERT_NOT_ALLOWED       = -64; {// you may not update to this version of firmware}
     BII_SENSOR_ERROR                      = -65; {// error communicating with sensor}
     BII_ERR_FIRM_COMPONENT_UNKNWN         = -66; {// firmware component unknown}
     BII_ERR_FIRM_CHKSUM_INCORRECT         = -67; {// firmware checksum is incorrect}
     BII_ERR_UNSUPPORTED_PROCESSOR         = -68; {// firmware not supported on this processor}
     BII_ERR_INVAL_VSTATION_COMMAND        = -69; {// This command not supported by V-Station}
     BII_ERR_NOT_VSTATION_DEVICE           = -70; {// The command is valid only on a V-Station}
     BII_ERR_TEMPLATE_EXISTS               = -71; {// template already exists}
     BII_ERR_KIT_MICRO_TIMEOUT             = -72; {// the Kit timed out}
     BII_ERR_KIT_MICRO_INVALID_RESPONSE    = -73; {// the Kit returned an invalid response}
     BII_ERR_MICRO_TIMEOUT                 = -74; {// generic: a processor/micro timed out}
     BII_ERR_NOT_ALLOWED_ON_AUX            = -75; {// This command is not allowed over the AUX port}
     BII_ERR_AUX_PROG_BAUD_NOT_57600       = -76; {// May not reprogram over aux if not 57600}
     BII_ERR_DUPLICATE_ID_OR_EXT_ID        = -77; {// There is a duplicate template with the same ext. Id}
     BII_ERR_MISMATCH_EXTENED_ID_MODE      = -78; {// Cannot perform operation for this extended ID mode}
     BII_ERR_EXT_WIEGAND_CONFLICT          = -79; {// May not have both normal and extended templates on the unit}
     BII_ERR_INVALID_PRODUCT               = -80; {// Command is invalid for this product}
     BII_ERR_GPO_OPTION_CONFLICT           = -81; {// You can't set Verification and GPO actions which conflict}
     BII_ERR_INVALID_POLLING_MODE          = -82; {// Invalid polling mode specified}
     BII_ERR_NO_FREE_SCHED_IN_POOL         = -83; {// No more available schedules in pool}
     BII_ERR_PASSWORD_EXT_ID_CONFLICT      = -84; {// Password mode and Extended ID Wiegand formats cannot be used at the same time}
     BII_ERR_FEATURE_DISABLED_THIS_VER     = -85; {// Feature is disabled in this version of firmware}
     BII_ERR_PARAM_SCTR_CONV_FAIL          = -98; {// Upgrade to 'step' firmware failed to convert flash sector}
     BII_ERR_PARAM_TEMP_SECT_FAIL          = -99; {// Upgrade to 'step' firmware failed to move template sector}

     { *Generic Error Codes* }
     BII_ERR_REC_UNIDENTIFIED              = -100; {/* An Empty Error Packet was received*/}
     BII_ERR_ERROR                         = -101; {/* Generic Code*/}

     { *Communication Error Codes* }
     BII_ERR_PORTACCESS                    = -102; {/* Error Accessing PC's Comport (From Readfile)*/}
     BII_ERR_REC_TIMEOUT                   = -103; {/* Timeout occurred before all data was received*/}
     BII_ERR_NO_DATA_RECEIVED              = -104; {/* No Data Was received from MV1100/MV1200/ESI*/}
     BII_ERR_ACK_REC_NO_REQ                = -105; {/* Special Code used for Singe Threaded communication*/}

     { *Indicates that a command was acknowledged but the requested data* }
     { *was not sent out by the timeout period* }
     BII_ERR_WRITEPORT                     = -106; {/* Failed to write data to comport*/}
     BII_ERR_READPORT                      = -107; {/* failed to read data from port*/}
     BII_ERR_FLOWCONTROLCHANGE             = -108;
     BII_ERR_GETCOMMSTATE                  = -109; {/* Failed getting comm state*/}
     BII_ERR_SETCOMMSTATE                  = -110; {/* Failed setting Comm state*/}
     BII_ERR_PURGERX                       = -111; {/* Failed to Purge Receive Line on Commport*/}
     BII_ERR_PURGETX                       = -112; {/* Failed to Purge Transmit Line on Commport*/}
     BII_ERR_CLOSECOMM                     = -113; {/* Failed to Close CommPort*/}
     BII_ERR_FRAMING                       = -114; {/* The hardware detected a framing error*/}
     BII_ERR_IO                            = -115; {/* An I/O error occurred during communications with the device.*/}

     { *This error differs from READPORT in that it originates from ClearCommError* }
     BII_ERR_PORTMODE                      = -116; {/* Error from ClearCommError (Invalid port specified or invalid mode)*/}
     BII_ERR_UARTOVERUN                    = -117; {/* A character-buffer overrun has occurred in the UART.*/}
     BII_ERR_PORTOVERUN                    = -118; {/* A character overrun has occured in the device driver*/}
     BII_ERR_PARITY                        = -119; {/* The hardware detected a parity error.*/}
     BII_ERR_TXFULL                        = -120; {/* The transmit buffer is full*/}
     BII_ERR_PURGESOCKET                   = -121; {// Windows socket could not be purged.}
     BII_ERR_INVALIDSOCKET                 = -122; {// Windows socket was invalid}
     BII_ERR_CLOSESOCKET                   = -123; {// Error closing socket}
     BII_ERR_BINDSOCKET                    = -124; {// Error binding socket}
     BII_ERR_OPENSOCKET                    = -125; {// Error opening socket}
     BII_ERR_WRITESOCKET                   = -126; {// Unable to write to socket}
     BII_ERR_READSOCKET                    = -127; {// Unable to read from socket}
     BII_ERR_SOCKETCLEANUP                 = -128; {// Error from WSACleanup()}
     BII_ERR_NOT_USING_COMM_PORT           = -129; {// returned for comm port specific calls when in TCP/IP mode}
     BII_ERR_NOT_USING_SOCKET              = -130; {// returned for TCP/IP specific calls when in COMM port mode}
     BII_ERR_SOCKETOPTION                  = -131; {// error setting a socket option}
     BII_ERR_CONNECTION_CLOSED             = -132; {// the connection was closed at the other end}

     { *Data Integrity Errors For Data Received from V1100* }
     BII_ERR_COMMANDCHK                    = -201; {/* Command Echo'd Back does not match command sent*/}
     BII_ERR_REC_CKSUM                     = -202; {/* The Checksum received from the V1100 does not match data*/}
     BII_ERR_REC_ERRPACKET                 = -203; {/* Error Packet returned from unit*/}
     BII_ERR_ACK_LEN                       = -204; {/* Acknowledgement Packet has invalid length*/}
     BII_ERR_INVALID_SYNCWORD              = -205; {/* An Invalid SyncWord was received*/}
     BII_ERR_REC_INVALID                   = -206; {/* A Positive Error code was received from V1100*/}
     BII_ERR_COMMAND_MISMATCH              = -207; {/* Packet received does nto correspond to command sent*/}

     { *Memory Allocation Errors* }
     BII_ERR_MEM                           = -301; {/* Memory allocation error*/}
     BII_ERR_PCKT2LRG                      = -302; {// Response from unit contains too much data}
     BII_ERR_PCKT2SML                      = -303; {// Response from unit contains too little data}

     { *Invalid Parameter error codes* }
     BII_ERR_INVALIDPORT                   = -401; {/* An Invalid CommPort was specified*/}
     BII_ERR_INVALIDPORTHANDLE             = -402; {/* An invalid port Handle was accessed*/}
     BII_ERR_FLOWCONTROLMODE               = -403;
     BII_ERR_PARAMETER_ONE                 = -404; {/* This first paramter was invalid*/}
     BII_ERR_PARAMETER_TWO                 = -405; {/* This second paramter was invalid*/}
     BII_ERR_PARAMETER_THREE               = -406; {/* This third paramter was invalid*/}
     BII_ERR_PARAMETER_FOUR                = -407; {/* This fourth paramter was invalid*/}
     BII_ERR_TEMPLATEDATA                  = -408; {/* The template passed in Does not contain valid data*/}
     BII_ERR_RAWIMAGE                      = -409; {// Invalid image data size}
     BII_ERR_BAUDRATE                      = -410; {// Invalid baud rate}
     BII_ERR_N_DATABYTES                   = -411; {/* Too Large a value was specified for the number of data bytes*/}
     BII_ERR_SUPPORT_TEMPLATE              = -412; {/* The template passed in is not supported with the version*/}

     { *ESI Packet Errors* }
     BII_ERR_ESI_BAD_PACKET                = -451; {// The ESI returned an invalid packet of data}

     { *Special DLL State Errors (Ie the current state of the DLL willnot allow this)* }
     BII_ERR_DATA_MONITOR_RUN              = -501; {/* Data Monitor Thread Running*/}

     { *Cannot perform single-threaded data exchange* }
     { *Exception Error Codes* }
     BII_ERR_EXCEPTION                     = -601; {/* Generic Exception Error*/}

     { *ESI Error Codes* }
     BII_ERR_ESI_UNKNOWN_COMMAND           = -701;
     BII_ERR_ESI_INVALID_CHECKSUM          = -702;
     BII_ERR_ESI_INVALID_PACKETLENGTH      = -703;
     BII_ERR_ESI_UNKNOWN                   = -705;
     BII_ERR_ESI_INVALID_BAUD              = -707;
     BII_ERR_ESI_INVALID_TEMPLATE          = -711;
     BII_ERR_ESI_INVALID_INDEX             = -712;
     BII_ERR_ESI_ERROR_WRITING             = -713;
     BII_ERR_ESI_ERROR_STORAGE_FULL        = -714;
     BII_ERR_ESI_ERROR_READING             = -717;
     BII_ERR_ESI_TIMEOUT                   = -723;
     BII_ERR_ESI_INVALID_OPTION            = -724;
     BII_ERR_ESI_INVALID_KEY               = -734;
     BII_ERR_ESI_INVALID_PROGRAM           = -738;
     BII_ERR_ESI_NO_DATA                   = -749;
     BII_ERR_ESI_DEFAULT_KEY_USED          = -750;
     BII_ERR_ESI_INVALID_SITE_KEY          = -751;
     BII_ERR_ESI_INVALID_LAYOUT_PACKET     = -752;
     BII_ERR_ESI_UNRECOGNIZED_LAYOUT       = -753;
     BII_ERR_ESI_CARDNOTPRESENT            = -754; {// no card was detected}
     BII_ERR_ESI_READER_ERROR              = -755; {// generic card reader error}
     BII_ERR_ESI_ID_NOT_AVAILABLE          = -756; {// command id is reserved}
     BII_ERR_ESI_CARDCHECKSUM              = -757; {// the checksum of the card does not match data}
     BII_ERR_ESI_WRITEDISABLED             = -758; {// could not perform action because write is disabled (read-only mode)}
     BII_ERR_ESI_AUTO_KEY_UPDATE_DISABLED  = -759; {// auto-key update could not occur because it is disabled}

     { *V-Station Communincation Manager (CM, aka Rabbit) Specific Error Codes* }
     BII_ERR_CM_INVALIDCOMMANDID           = -801; {// Invalid command for the CM}
     BII_ERR_CM_INVALIDCHECKSUM            = -802; {// Checksum invalid with a CM command}
     BII_ERR_CM_INCORRECTPACKETLENGTH      = -803; {// Incorrect packet length on CM command}
     BII_ERR_CM_BUSY                       = -804; {// The CM was busy at time of command}
     BII_ERR_CM_DHCP_MODE                  = -805; {// Can't set IP address; V-Station is in DHCP mode}
     BII_ERR_CM_INVALID_IP_ADDRESS         = -806; {// Invalid IP address}
     BII_ERR_CM_IP_ADDRESS_IN_USE          = -807; {// IP address is in use!}
     BII_ERR_CM_INVALID_MENU_IN_MEM        = -808; {// Corrupt or uninitialized menu referenced in the CM memory}
     BII_ERR_CM_INVALID_MENU_FILE          = -809; {// Invalid V-Station Menu file}
     BII_ERR_CM_FLASH_STORE_FAIL           = -810; {// Error writing to CM flash memory}
     BII_ERR_CM_INVALID_DATA_VALUE         = -811; {// Invalid CM option}
     BII_ERR_CM_INVALID_MENU_LOCATION      = -812; {// Invalid V-Station Menu location}
     BII_ERR_CM_INVALID_SLOT_DESIGNATION   = -813; {// Invalid slot for uploading menu prompt}
     BII_ERR_CM_INVALIDOPTION              = -824; {// Invalid option for CM command}
     BII_ERR_CM_KIT_MICRO_TIMEOUT          = -872; {// The CM timed out waiting for the Kit}
     BII_ERR_CM_DHCP_FAIL                  = -898; {// DHCP failed on the CM}
     BII_ERR_CM_CHECKSUM_ERROR             = -899; {// The CM found a checksum error in the packet}

     { *Internal Errors Only (Do Not Expect to See these)* }
     BII_ERR_MINPACKETWORDS                = -10001; {/* The Total Number of packet words specified was less than the*/}

     { *smallest possible Packet Size* }
     BII_ERR_INVALIDPACKETLENGTH           = -10002;
     BII_ERR_PACKET_HEADER_MISMATCH        = -10003; {//the packet header cannot be correct for packet data specified}
     BII_ERR_PACKET_NOT_EMPTY              = -10004; {//The packet to be sent out was not initialized}
     BII_ERR_NO_CURRENT_COMMSESSION        = -10005;
   {$endif}


{ Declaración de Funciones Generales }
function BII_Calibrate_Finger_Detection(reserved_one: Cardinal; reserved_two: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Capture_DownSampled_Image(var image: BII_RAW): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Capture_DownSampled_ImageEx(var image: BII_RAW; var extended: Integer; extended_size: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Capture_Raw_Image(var image: BII_RAW; start_row: Integer; start_col: Integer; num_rows: Integer; num_cols: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Capture_Opti_Raw_Image(var image: BII_RAW; start_row: Integer; start_col: Integer; num_rows: Integer; num_cols: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Check_Template(id: Cardinal; index: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Close_PC_Comm: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Current_Port: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Enroll(TemplateID: Cardinal; var quality: Integer; var content: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Enroll_And_Transfer(TemplateID: Cardinal; var the_template: BII_TEMPLATE; var quality: Integer; var content: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Enroll_And_Transfer_Search(TemplateID: Cardinal; var the_template: BII_TEMPLATE; var quality: Integer; var content: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Erase_Templates(var template_id_list: Cardinal; var template_index_list: Integer; number_of_templates: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Finish_FPREnroll(var quality: Integer; var content: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Finish_FPREnroll_Transfer(var the_template: BII_TEMPLATE; var quality: Integer; var content: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Finish_FPREnroll_Transfer_Search(var the_template: BII_TEMPLATE; var quality: Integer; var content: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Finish_FPRIdentify(var TemplateID: Cardinal; var Index: Cardinal; var Reserved: Cardinal; var score: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Finish_FPRVerify(var score: Integer; num_templates: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Finish_FPRVerify_Template(var score: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Full_Version(dll_version: PChar; algorithm_version: PChar; kernel_version: PChar; pic_version: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Auto_Finger_Detect_State(var fdetect_state: BII_AUTO_FDETECT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Current_NetID: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Flash_Block(var buffer: Cardinal; offset: Integer; num_words: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Global_Thresh: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Host_Port_Type: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_LED_Table(var led_table: BII_LED_TABLE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Max_NTemplates: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Net_ID: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Num_Templates: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_PC_Baud_Rate: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Port_Comm_Params(port: Integer; var baud_rate: Integer; var stop_bits: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Port_Comm_ParamsEx(port: Integer; var baud_rate: Integer; var stop_bits: Integer; var flow_control: BII_FLOW_CONTROL_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Port_Enable(var value: BII_PORT_ENABLE_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Sensor_Parameters(var discharge_time_level: Cardinal; var discharge_current_level: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Sensor_ParametersEx(struct_revision: Integer; var sensor_parameters: BII_SENSOR_PARAM): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template(template_number: Cardinal; template_index: Integer; var the_template: BII_TEMPLATE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template_Admin_List(var list:BII_Template_Admin_Struct; option: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template_List(var template_list: array of char): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template_Nx_Ny(var the_template: BII_TEMPLATE; var nx: Integer; var ny: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template_Size(var the_template: BII_TEMPLATE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Unit_Type(var aunit: BII_UNIT_TYPE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_User_Interface(var value: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_V1100_Port: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Verify_Response_Port(var value: BII_VERIFY_RESPONSE_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Wiegand(var value: BII_WIEGAND_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Wiegand_Id(var Id: Cardinal; timeout: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Identify(reserved_for_future: Cardinal; var TemplateID: Cardinal; var Index: Cardinal; var Reserved: Cardinal; var score: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Is_Finger_Present: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Make_Enroll_WCard(var WCard_id: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Make_Erase_WCard(var WCard_id: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_N_Commands_Pending: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Open_PC_Comm(commport: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Program_Monitor(var the_program: Byte; n_data_bytes: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Purge_Port(line: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reindex_Templates(var value: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Auto_Finger_Detect_State(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Global_Thresh(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Host_Port_Type(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_LED_Table(mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Port_Comm_Params(port: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Port_Enable(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Sensor_Parameters(save_flag: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_User_Interface(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_V1100_Comm_Parameters(save_flag: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Verify_Response_Port(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Wiegand(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Wiegand_Locks(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Send_Flash_Block(var buffer: Cardinal; offset: Integer; num_words: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Send_Index_Template(var the_template: BII_TEMPLATE; option: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Send_Raw_Data(var the_data: Byte; nbytes: Integer; ack: Integer; req: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Send_Raw_Image(var image: BII_RAW; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Send_Template(var the_template: BII_TEMPLATE; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Sensor_Calibrate_Results(var res_array: BII_CALIBRATE_RES; var optional_image: BII_RAW): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Auto_Finger_Detect(fdetect: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Auto_Finger_Detect_State(var fdetect_state: BII_AUTO_FDETECT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Current_NetID(net_id: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Global_Thresh(thresh: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Host_Port_Type(port_type: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_LED(LEDSetting: Integer; rate: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_LED_Table(var led_table: BII_LED_TABLE; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Net_ID(net_id: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_PC_Baud_Rate(baud_rate: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_PC_Flow_Contol(var flow_control: BII_FLOW_CONTROL_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Port_Comm_Params(port: Integer; baud_rate: Integer; stop_bits: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Port_Comm_ParamsEx(port: Integer; baud_rate: Integer; stop_bits: Integer; var flow_control: BII_FLOW_CONTROL_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Port_Enable(var value: BII_PORT_ENABLE_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Sensor_Parameters(discharge_time_level: Cardinal; discharge_current_level: Cardinal; save_flag: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Sensor_ParametersEx(struct_revision: Integer; var sensor_parameters: BII_SENSOR_PARAM; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_User_Interface(thresh: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_V1100_Comm_Parameters(baud_rate: Integer; stop_bits: Integer; save_flag: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_V1100_Comm_ParametersEx(baud_rate: Integer; stop_bits: Integer; save_flag: Integer; var flow_control: BII_FLOW_CONTROL_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Verify_Response_Port(var value: BII_VERIFY_RESPONSE_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Wiegand(value: BII_WIEGAND_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Wiegand_Lock(wiegand_format: Integer; lock: Integer; var key: Cardinal; key_size: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Sleep(sleep: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_FPREnroll(TemplateID: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_FPREnroll_Transfer(TemplateID: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_FPREnroll_Transfer_Search(TemplateID: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_FPRIdentify(reserved_for_future: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_FPRVerify(TemplateID: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_FPRVerify_Template(var the_template: BII_TEMPLATE; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Start_Sensor_Calibrate(image_flag: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Status: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Uncompress(var inarray: BII_Template; var outarray: BII_Uncomp_Img): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Unpack(var outarray:array of Byte; var inarray: array of Byte; nxout: Integer; nyout: Integer; nbits: Integer; dirflag: Integer; var qtable: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Update_Template_Storage_Space(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Verify(TemplateID: Cardinal; var score: Integer; num_templates: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Verify_Template(var the_template: BII_TEMPLATE; var score: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Version(dll_version: PChar; algorithm_version: PChar; kernel_version: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Variable_Wiegand(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Variable_Wiegand(var Var_Format: BII_WIEGAND_FORMAT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Variable_Wiegand(var Var_Format: BII_WIEGAND_FORMAT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Convert_To_348(var the_template: BII_TEMPLATE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template_Name(ID: Cardinal; Index: Cardinal; template_name: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Wiegand_Enroll_ID(var Wiegand_Enroll_ID: BII_WIEGAND_ID_LIST; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Wiegand_Enroll_ID(var Wiegand_Enroll_ID: BII_WIEGAND_ID_LIST): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Clear_Wiegand_Enroll_ID(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Wiegand_Delete_ID(var Wiegand_Delete_ID: BII_WIEGAND_ID_LIST; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Wiegand_Delete_ID(var Wiegand_Delete_ID: BII_WIEGAND_ID_LIST): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Clear_Wiegand_Delete_ID(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Template_Security(id: Cardinal; index: Integer; security_level: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Template_Security(id: Cardinal; index: Integer; var security_level: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_LED_Idle(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Block_Template_List(var template_list: Cardinal; size_of_block: Integer; block_number: Integer): Integer {$ifdef WIN32} stdcall {$endif};
//function BII_Get_Block_Template_Admin_List(var template_list: TEMPLATEADMINTYPE;
  //                                         option: Integer;
  //                                         size_of_block: Integer;
  //                                         block_number: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Upload_Uncompressed_Template(id: Cardinal; index: Integer; var image: BII_RAW): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Download_Uncompressed_Template(var the_template: BII_TEMPLATE; var image: BII_RAW): Integer {$ifdef WIN32} stdcall {$endif};

{*MV1200 commands*}
function BII_Get_GPI_States(var GPI_struct: BII_GPI_STATES; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Default_GPO_States(GPO_Def_struct: BII_DEFAULT_GPO_STATES; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Default_GPO_States(var GPO_Def_struct: BII_DEFAULT_GPO_STATES; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Default_GPO_States(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPO_State(GPO_struct: BII_GPO_STATE; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPO_States(var GPO_struct_All: BII_GPO_STATE_ALL; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Serial_Number(var SerialNum: BII_GETSERIALNUMBER_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPO_State(GPO_Reset: BII_RESET_GPO_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Wiegand_String(var WiegandString: Cardinal; var NoWiegandBits: Cardinal; var Id: Cardinal; timeout: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Biometric_State(var State: Cardinal; var reserved_one: Cardinal; var reserved_two: Cardinal; var reserved_three: Cardinal; var reserved_four: Cardinal; var reserved_five: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Biometric_State(State: Cardinal; reserved_one: Cardinal; reserved_two: Cardinal; reserved_three: Cardinal; reserved_four: Cardinal; reserved_five: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Biometric_State(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Receive_Templates_Timeout(timeout: Integer; reserved_one: Integer; reserved_two: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Receive_Templates_Timeout(var timeout: Integer; var reserved_one: Integer; var reserved_two: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Receive_Templates_Timeout(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Verification_Entry(reserved: Cardinal; var entry: BII_VERIFICATION_QUEUE_ENTRY): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Verification_Queue(reserved: Cardinal; var queue: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Erase_Verification_Queue(reserved: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Custom_Wiegand_Format(var wiegandFile: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Custom_Wiegand_Format(formatName: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Upgrade_Firmware(var the_program: Byte; n_data_bytes: Cardinal; info: PChar; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Veri_Series_Ports(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Veri_Series_Ports(mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Veri_Series_Ports(var mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Product_Type(var data: PRODUCT_TYPE_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Finger_Detect_Timeout(var timer: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Finger_Detect_Timeout(timer: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Finger_Detect_Timeout(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
//function BII_Get_BioAPI_BIRHeader(var BioAPI_Header: BII_TAGBIO_API_BIR_HEADER;
  //                                async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Alt_Security_Behavior(setting1: Cardinal; setting2: Cardinal; setting3: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Alt_Security_Behavior(var setting1: Cardinal; var setting2: Cardinal; var setting3: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Alt_Security_Behavior(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Extended_ID_From_ID(templateID: Cardinal; var extendedID: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_ID_From_Extended_ID(var extendedID: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Extended_Template_Admin_List(var list: BII_EXT_TEMPLATE_ADMIN_STRUCT; option: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Extended_Wiegand_String(var ExtendedWiegandString: Byte; var NoWiegandBits: Cardinal; var ExtendedId: Byte; timeout: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Extended_Wiegand_Mode: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Error_String(error: Integer; reserved_one: Integer; errorString: Array of Char): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPO_Table(var GPOTable: BII_GPO_TABLE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPO_Table(var GPOTable: BII_GPO_TABLE; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPO_Table(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPO_Duration_Time: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPO_Duration_Time(duration: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPO_Duration_Time(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPI_Table(var GPITable: BII_GPI_TABLE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPI_Table(var GPITable: BII_GPI_TABLE; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPI_Table(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPI_Duration_Time: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPI_Duration_Time(duration: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPI_Duration_Time(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPI_Active_Edge: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPI_Active_Edge(activeEdge: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPI_Active_Edge(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPI_Double_Signal_Tolerance: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPI_Double_Signal_Tolerance(tolerance: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPI_Double_Signal_Tolerance(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_GPI_ID_Settings(var GPI_ID: Cardinal; var GPImaxIndex: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_GPI_ID_Settings(GPI_ID: Cardinal; GPImaxIndex: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_GPI_ID_Settings(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_MV1200_Step_Upgrade_Result: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Password_Mode(reserved: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Password_Mode(mode: Integer; reserved: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Password_Mode(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_MultiFinger_Params(reserved: Cardinal; var mode: Integer; var timeout: Integer; var reserved_out: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_MultiFinger_Params(mode: Integer; timeout: Integer; reserved: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_MultiFinger_Params(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Polling_Counter: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Polling_Data(buffer: PChar; var counter: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Clear_Polling_Data(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Polling_Mode(wiegandInput: Cardinal; keypadInput: Cardinal; reserved_one: Cardinal; reserved_two: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Polling_Mode(var wiegandInput: Cardinal; var keypadInput: Cardinal; var reserved_one: Cardinal; var reserved_two: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Polling_Mode(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Duress_Mode(var mode: Cardinal; var reserved: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Duress_Mode(mode: Cardinal; reserved: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Reset_Duress_Mode(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Extended_Wiegand_StringEx(timeout: Integer; var ExtendedWiegandString: Byte; var NoWiegandBits: Cardinal; var ExtendedId: Byte; var AlternateID: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};

{*ESI commands* }
function BII_ESI_Host_Mode: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Host_ModeEx(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Status: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Version(version: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Send_Index_Template(var primarykey: Byte; var the_template: BII_TEMPLATE; option: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Unit_Type(var aunit: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Template(var primarykey: Byte; template_number: Cardinal; template_index: Integer; var the_template: BII_TEMPLATE): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Erase_Templates(var primarykey: Byte; code: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Max_NTemplates: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Max_NTemplates(NumTemplates: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Num_Templates: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Template_List(var template_list: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Template_Admin_List(var template_list: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Flash_Block(var buffer: Cardinal; block: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Send_Flash_Block(var primarykey: Byte; var buffer: Cardinal; block: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Check_Template(id: Cardinal; index: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Is_SmartCard_Present: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Template_Name(ID: Cardinal; Index: Cardinal; template_name: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Template_Security(id: Cardinal; index: Integer; security_level: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Template_Security(id: Cardinal; index: Integer; var security_level: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Program_ESI_Controller(port: Cardinal; var programData: Byte; nBytes: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Smartcard_Password(var oldprimary: Byte; var newprimary: Byte; var secondary: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Check_Smartcard_Password(var tempkey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Smartcard_Layout(var SmartcardLayout: BII_SMARTCARD_LAYOUT_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Smartcard_Layout(var primarykey: Byte; SmartcardLayout: BII_SMARTCARD_LAYOUT_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Reset_Smartcard_Layout(var primarykey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Wiegand_String(templateNum: Integer; var WiegandString: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Wiegand_String(templateNum: Integer; var primarykey: Byte; var WiegandString: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Erase_Wiegand_String(templateNum: Integer; var primarykey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Store_Wiegand(StoreWiegand: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Store_Wiegand(var StoreWiegand: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Verification_Timeout(delay: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Verification_Timeout: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Unsecure_Smartcard(var primarykey: Byte; code: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Secure_Smartcard(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_SiteKey_Mode: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_SiteKey_Mode(var primarykey: Byte; mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Update_Smartcard_Password_Mode4(var PrimaryKey: Byte; var NewReadKey: Byte; var NewWriteKey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_ReadOnly_Mode(var PrimaryKey: Byte; mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_ReadOnly_Mode: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Reset_ReadOnly_Mode(var PrimaryKey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Set_Auto_SiteKey_Update(var PrimaryKey: Byte; mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Auto_SiteKey_Update: Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Reset_Auto_SiteKey_Update(var PrimaryKey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_Get_Smartcard_Manufacturer_Info(var info: BII_SMARTCARD_MANUFACTURER_INFO): Integer {$ifdef WIN32} stdcall {$endif};

{*ESI iClass commands*}
function BII_ESI_IClass_Get_Flash_Block(var buffer: Byte; block: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Send_Flash_Block(var primarykey: Byte; var buffer: Byte; block: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Set_Page_Layout(var primarykey: Byte; layout: BII_ICLASS_LAYOUT_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Get_Page_Layout(var layout: BII_ICLASS_LAYOUT_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Reset_Page_Layout(var primarykey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Set_Page_Offset(var primarykey: Byte; offset: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Get_Page_Offset(var offset: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Reset_Page_Offset(var primarykey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Get_Smartcard_Info(pageSelect: Cardinal; var info: BII_ICLASS_SMARTCARD_INFO): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Get_Smartcard_InfoEx(pageSelect: Cardinal; var info: BII_ICLASS_SMARTCARD_INFO): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Get_Default_Smartcard_Key_Description(var description: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Set_Default_Smartcard_Key(var primaryKey: Byte; var keys: Byte; var description: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_ESI_IClass_Reset_Default_Smartcard_Key(var primaryKey: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};

{*Undocumented Research and Testing Routines*}
//function BII_Verify_Test(TemplateID: Cardinal;
//                         var cand_info: CANDIDATE_INFO;
//                         async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
//function BII_Identify_Test(var cand_info: CANDIDATE_INFO;
//                         async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
//function BII_Enroll_Test(TemplateID: Cardinal;
//                       var temp_info: TEMPLATE_INFO;
//                       async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Capture_Dn_Raw_Image(var image: BII_RAW; start_row: Integer; start_col: Integer; num_rows: Integer; num_cols: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Test_Wiegand_In(test: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Wiegand_Test_Res(var result: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Send_Wiegand_Out(w_value: Word; site_code: Byte): Integer {$ifdef WIN32} stdcall {$endif};

{*TCP/IP functions*}
function BII_Initialize_Socket_Communications: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Open_TCPIP_Communications(host: PChar; port: Word): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Close_TCPIP_Communications: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Cleanup_Socket_Communications: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Current_TCP_Port: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Current_Host_Name(var hostname: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Socket_Buffer_Size(SendBuffer: Integer; RecvBuffer: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Socket_Buffer_Size(var SendBuffer: Integer; var RecvBuffer: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Local_IP_Address(var a1: Byte; var a2: Byte; var a3: Byte; var a4: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Is_Using_TCPIP: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Subnet_Mask(var subnetMask: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Subnet_Mask(subnetMask: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Window_Handle(handle: HWND): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_TCP_Connection_Timeout(timeout: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_TCP_Connection_Timeout: Integer {$ifdef WIN32} stdcall {$endif};
//function BII_Get_Product_Name_From_Type(type: Integer;
//                                      productName: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Product_Type_From_Name(productName: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_TCP_NoDelay: Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_TCP_NoDelay(noDelay: Integer): Integer {$ifdef WIN32} stdcall {$endif};

{*V-Station functions*}
function BII_Erase_Transaction_Log(option: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Read_Transaction_Log(option: Integer; updateFlag: Integer; maxEntries: Integer; var log: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Num_Transaction_Log(option: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_CM_Version(version: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Kit_Version(version: PChar): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Get_Date_Time(var dateTime: BII_DATETIME_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Date_Time(dateTime: BII_DATETIME_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_Set_Date_TimeEx(var dateTime: BII_DATETIME_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Key_Press(timeout: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Test_LCD: Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_MAC_Address(var address: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Date_Time_Settings(var settings: BII_DATETIMESETTINGS_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Date_Time_Settings(settings: BII_DATETIMESETTINGS_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Date_Time_SettingsEx(var settings: BII_DATETIMESETTINGS_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Reset_Date_Time_Settings(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Show_LCD_Text(displayTime: Integer; text: PChar; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_IP_Address(var address: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_IP_Address(address: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_DHCP_Mode: Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_DHCP_Mode(mode: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Current_IP_Address(var address: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Bio_Access_Schedule(reserved: Cardinal; var schedule: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Bio_Access_Schedule(reserved: Cardinal; var version: Cardinal; var schedule: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Reset_Bio_Access_Schedule(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Upload_Menu(menuLocation: Cardinal; reserved: Cardinal; var menuFile: Byte; menuFileSize: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Menu(whichMenu: Cardinal; reserved: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Menu_Info(var numMenus: Cardinal; var currentMenu: Cardinal; var reserved: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Menu_Name(whichMenu: Cardinal; var reserved: Cardinal; var menuName: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_LCD_Settings(contrast: Cardinal; reserved_one: Cardinal; reserved_two: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_LCD_Settings(var contrast: Cardinal; var reserved_one: Cardinal; var reserved_two: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Reset_LCD_Settings(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Erase_Menu(whichMenu: Cardinal; reserved: Cardinal; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Access_Schedule_Settings(enabled: Integer; tolerance: Integer; reserved: Cardinal;async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Access_Schedule_Settings(var enabled: Integer;var tolerance: Integer;var reserved: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Reset_Access_Schedule_Settings(async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Access_Schedule(scheduleIndex: Integer; var scheduleData: Byte; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Access_Schedule(scheduleIndex: Integer;var scheduleData: Byte): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Reset_Access_Schedule(scheduleIndex: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Num_Defined_Access_Schedules: Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Defined_Access_Schedule_List(var definedScheduleList: Cardinal): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Set_Holiday_Schedule(holidayIndex: Integer; var holidayData: BII_HOLIDAY_SCHEDULE_STRUCT; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Get_Holiday_Schedule(holidayIndex: Integer; var holidayData: BII_HOLIDAY_SCHEDULE_STRUCT): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Reset_Holiday_Schedule(holidayIndex: Integer; async: Integer): Integer {$ifdef WIN32} stdcall {$endif};
function BII_VS_Is_Template_Scheduled(templateId: Cardinal; templateIndex: Cardinal; templateSchedule: Byte): Integer {$ifdef WIN32} stdcall {$endif};

implementation

{ Implementación de las Funciones Generales }
function BII_Calibrate_Finger_Detection; external 'BII_V1100.dll';
function BII_Capture_DownSampled_Image; external 'BII_V1100.dll';
function BII_Capture_DownSampled_ImageEx; external 'BII_V1100.dll';
function BII_Capture_Raw_Image; external 'BII_V1100.dll';
function BII_Capture_Opti_Raw_Image; external 'BII_V1100.dll';
function BII_Check_Template; external 'BII_V1100.dll';
function BII_Close_PC_Comm; external 'BII_V1100.dll';
function BII_Current_Port; external 'BII_V1100.dll';
function BII_Enroll; external 'BII_V1100.dll';
function BII_Enroll_And_Transfer; external 'BII_V1100.dll';
function BII_Enroll_And_Transfer_Search; external 'BII_V1100.dll';
function BII_Erase_Templates; external 'BII_V1100.dll';
function BII_Finish_FPREnroll; external 'BII_V1100.dll';
function BII_Finish_FPREnroll_Transfer; external 'BII_V1100.dll';
function BII_Finish_FPREnroll_Transfer_Search; external 'BII_V1100.dll';
function BII_Finish_FPRIdentify; external 'BII_V1100.dll';
function BII_Finish_FPRVerify; external 'BII_V1100.dll';
function BII_Finish_FPRVerify_Template; external 'BII_V1100.dll';
function BII_Full_Version; external 'BII_V1100.dll';
function BII_Get_Auto_Finger_Detect_State; external 'BII_V1100.dll';
function BII_Get_Current_NetID; external 'BII_V1100.dll';
function BII_Get_Flash_Block; external 'BII_V1100.dll';
function BII_Get_Global_Thresh; external 'BII_V1100.dll';
function BII_Get_Host_Port_Type; external 'BII_V1100.dll';
function BII_Get_LED_Table; external 'BII_V1100.dll';
function BII_Get_Max_NTemplates; external 'BII_V1100.dll';
function BII_Get_Net_ID; external 'BII_V1100.dll';
function BII_Get_Num_Templates; external 'BII_V1100.dll';
function BII_Get_PC_Baud_Rate; external 'BII_V1100.dll';
function BII_Get_Port_Comm_Params; external 'BII_V1100.dll';
function BII_Get_Port_Comm_ParamsEx; external 'BII_V1100.dll';
function BII_Get_Port_Enable; external 'BII_V1100.dll';
function BII_Get_Sensor_Parameters; external 'BII_V1100.dll';
function BII_Get_Sensor_ParametersEx; external 'BII_V1100.dll';
function BII_Get_Template; external 'BII_V1100.dll';
function BII_Get_Template_Admin_List; external 'BII_V1100.dll';
function BII_Get_Template_List; external 'BII_V1100.dll';
function BII_Get_Template_Nx_Ny; external 'BII_V1100.dll';
function BII_Get_Template_Size; external 'BII_V1100.dll';
function BII_Get_Unit_Type; external 'BII_V1100.dll';
function BII_Get_User_Interface; external 'BII_V1100.dll';
function BII_Get_V1100_Port; external 'BII_V1100.dll';
function BII_Get_Verify_Response_Port; external 'BII_V1100.dll';
function BII_Get_Wiegand; external 'BII_V1100.dll';
function BII_Get_Wiegand_Id; external 'BII_V1100.dll';
function BII_Identify; external 'BII_V1100.dll';
function BII_Is_Finger_Present; external 'BII_V1100.dll';
function BII_Make_Enroll_WCard; external 'BII_V1100.dll';
function BII_Make_Erase_WCard; external 'BII_V1100.dll';
function BII_N_Commands_Pending; external 'BII_V1100.dll';
function BII_Open_PC_Comm; external 'BII_V1100.dll';
function BII_Program_Monitor; external 'BII_V1100.dll';
function BII_Purge_Port; external 'BII_V1100.dll';
function BII_Reindex_Templates; external 'BII_V1100.dll';
function BII_Reset_Auto_Finger_Detect_State; external 'BII_V1100.dll';
function BII_Reset_Global_Thresh; external 'BII_V1100.dll';
function BII_Reset_Host_Port_Type; external 'BII_V1100.dll';
function BII_Reset_LED_Table; external 'BII_V1100.dll';
function BII_Reset_Port_Comm_Params; external 'BII_V1100.dll';
function BII_Reset_Port_Enable; external 'BII_V1100.dll';
function BII_Reset_Sensor_Parameters; external 'BII_V1100.dll';
function BII_Reset_User_Interface; external 'BII_V1100.dll';
function BII_Reset_V1100_Comm_Parameters; external 'BII_V1100.dll';
function BII_Reset_Verify_Response_Port; external 'BII_V1100.dll';
function BII_Reset_Wiegand; external 'BII_V1100.dll';
function BII_Reset_Wiegand_Locks; external 'BII_V1100.dll';
function BII_Send_Flash_Block; external 'BII_V1100.dll';
function BII_Send_Index_Template; external 'BII_V1100.dll';
function BII_Send_Raw_Data; external 'BII_V1100.dll';
function BII_Send_Raw_Image; external 'BII_V1100.dll';
function BII_Send_Template; external 'BII_V1100.dll';
function BII_Sensor_Calibrate_Results; external 'BII_V1100.dll';
function BII_Set_Auto_Finger_Detect; external 'BII_V1100.dll';
function BII_Set_Auto_Finger_Detect_State; external 'BII_V1100.dll';
function BII_Set_Current_NetID; external 'BII_V1100.dll';
function BII_Set_Global_Thresh; external 'BII_V1100.dll';
function BII_Set_Host_Port_Type; external 'BII_V1100.dll';
function BII_Set_LED; external 'BII_V1100.dll';
function BII_Set_LED_Table; external 'BII_V1100.dll';
function BII_Set_Net_ID; external 'BII_V1100.dll';
function BII_Set_PC_Baud_Rate; external 'BII_V1100.dll';
function BII_Set_PC_Flow_Contol; external 'BII_V1100.dll';
function BII_Set_Port_Comm_Params; external 'BII_V1100.dll';
function BII_Set_Port_Comm_ParamsEx; external 'BII_V1100.dll';
function BII_Set_Port_Enable; external 'BII_V1100.dll';
function BII_Set_Sensor_Parameters; external 'BII_V1100.dll';
function BII_Set_Sensor_ParametersEx; external 'BII_V1100.dll';
function BII_Set_User_Interface; external 'BII_V1100.dll';
function BII_Set_V1100_Comm_Parameters; external 'BII_V1100.dll';
function BII_Set_V1100_Comm_ParametersEx; external 'BII_V1100.dll';
function BII_Set_Verify_Response_Port; external 'BII_V1100.dll';
function BII_Set_Wiegand; external 'BII_V1100.dll';
function BII_Set_Wiegand_Lock; external 'BII_V1100.dll';
function BII_Sleep; external 'BII_V1100.dll';
function BII_Start_FPREnroll; external 'BII_V1100.dll';
function BII_Start_FPREnroll_Transfer; external 'BII_V1100.dll';
function BII_Start_FPREnroll_Transfer_Search; external 'BII_V1100.dll';
function BII_Start_FPRIdentify; external 'BII_V1100.dll';
function BII_Start_FPRVerify; external 'BII_V1100.dll';
function BII_Start_FPRVerify_Template; external 'BII_V1100.dll';
function BII_Start_Sensor_Calibrate; external 'BII_V1100.dll';
function BII_Status; external 'BII_V1100.dll';
function BII_Uncompress; external 'BII_V1100.dll';
function BII_Unpack; external 'BII_V1100.dll';
function BII_Update_Template_Storage_Space; external 'BII_V1100.dll';
function BII_Verify; external 'BII_V1100.dll';
function BII_Verify_Template; external 'BII_V1100.dll';
function BII_Version; external 'BII_V1100.dll';
function BII_Reset_Variable_Wiegand; external 'BII_V1100.dll';
function BII_Get_Variable_Wiegand; external 'BII_V1100.dll';
function BII_Set_Variable_Wiegand; external 'BII_V1100.dll';
function BII_Convert_To_348; external 'BII_V1100.dll';
function BII_Get_Template_Name; external 'BII_V1100.dll';
function BII_Set_Wiegand_Enroll_ID; external 'BII_V1100.dll';
function BII_Get_Wiegand_Enroll_ID; external 'BII_V1100.dll';
function BII_Clear_Wiegand_Enroll_ID; external 'BII_V1100.dll';
function BII_Set_Wiegand_Delete_ID; external 'BII_V1100.dll';
function BII_Get_Wiegand_Delete_ID; external 'BII_V1100.dll';
function BII_Clear_Wiegand_Delete_ID; external 'BII_V1100.dll';
function BII_Set_Template_Security; external 'BII_V1100.dll';
function BII_Get_Template_Security; external 'BII_V1100.dll';
function BII_Reset_LED_Idle; external 'BII_V1100.dll';
function BII_Get_Block_Template_List; external 'BII_V1100.dll';
function BII_Get_Block_Template_Admin_List:Integer; external 'BII_V1100.dll';
function BII_Upload_Uncompressed_Template; external 'BII_V1100.dll';
function BII_Download_Uncompressed_Template; external 'BII_V1100.dll';
function BII_Get_GPI_States; external 'BII_V1100.dll';
function BII_Set_Default_GPO_States; external 'BII_V1100.dll';
function BII_Get_Default_GPO_States; external 'BII_V1100.dll';
function BII_Reset_Default_GPO_States; external 'BII_V1100.dll';
function BII_Set_GPO_State; external 'BII_V1100.dll';
function BII_Get_GPO_States; external 'BII_V1100.dll';
function BII_Get_Serial_Number; external 'BII_V1100.dll';
function BII_Reset_GPO_State; external 'BII_V1100.dll';
function BII_Get_Wiegand_String; external 'BII_V1100.dll';
function BII_Get_Biometric_State; external 'BII_V1100.dll';
function BII_Set_Biometric_State; external 'BII_V1100.dll';
function BII_Reset_Biometric_State; external 'BII_V1100.dll';
function BII_Set_Receive_Templates_Timeout; external 'BII_V1100.dll';
function BII_Get_Receive_Templates_Timeout; external 'BII_V1100.dll';
function BII_Reset_Receive_Templates_Timeout; external 'BII_V1100.dll';
function BII_Get_Verification_Entry; external 'BII_V1100.dll';
function BII_Get_Verification_Queue; external 'BII_V1100.dll';
function BII_Erase_Verification_Queue; external 'BII_V1100.dll';
function BII_Set_Custom_Wiegand_Format; external 'BII_V1100.dll';
function BII_Get_Custom_Wiegand_Format; external 'BII_V1100.dll';
function BII_Upgrade_Firmware; external 'BII_V1100.dll';
function BII_Reset_Veri_Series_Ports; external 'BII_V1100.dll';
function BII_Set_Veri_Series_Ports; external 'BII_V1100.dll';
function BII_Get_Veri_Series_Ports; external 'BII_V1100.dll';
function BII_Get_Product_Type; external 'BII_V1100.dll';
function BII_Get_Finger_Detect_Timeout; external 'BII_V1100.dll';
function BII_Set_Finger_Detect_Timeout; external 'BII_V1100.dll';
function BII_Reset_Finger_Detect_Timeout; external 'BII_V1100.dll';
function BII_Get_BioAPI_BIRHeader:Integer; external 'BII_V1100.dll';
function BII_Set_Alt_Security_Behavior; external 'BII_V1100.dll';
function BII_Get_Alt_Security_Behavior; external 'BII_V1100.dll';
function BII_Reset_Alt_Security_Behavior; external 'BII_V1100.dll';
function BII_Get_Extended_ID_From_ID; external 'BII_V1100.dll';
function BII_Get_ID_From_Extended_ID; external 'BII_V1100.dll';
function BII_Get_Extended_Template_Admin_List; external 'BII_V1100.dll';
function BII_Get_Extended_Wiegand_String; external 'BII_V1100.dll';
function BII_Get_Extended_Wiegand_Mode; external 'BII_V1100.dll';
function BII_Get_Error_String; external 'BII_V1100.dll';
function BII_Get_GPO_Table; external 'BII_V1100.dll';
function BII_Set_GPO_Table; external 'BII_V1100.dll';
function BII_Reset_GPO_Table; external 'BII_V1100.dll';
function BII_Get_GPO_Duration_Time; external 'BII_V1100.dll';
function BII_Set_GPO_Duration_Time; external 'BII_V1100.dll';
function BII_Reset_GPO_Duration_Time; external 'BII_V1100.dll';
function BII_Get_GPI_Table; external 'BII_V1100.dll';
function BII_Set_GPI_Table; external 'BII_V1100.dll';
function BII_Reset_GPI_Table; external 'BII_V1100.dll';
function BII_Get_GPI_Duration_Time; external 'BII_V1100.dll';
function BII_Set_GPI_Duration_Time; external 'BII_V1100.dll';
function BII_Reset_GPI_Duration_Time; external 'BII_V1100.dll';
function BII_Get_GPI_Active_Edge; external 'BII_V1100.dll';
function BII_Set_GPI_Active_Edge; external 'BII_V1100.dll';
function BII_Reset_GPI_Active_Edge; external 'BII_V1100.dll';
function BII_Get_GPI_Double_Signal_Tolerance; external 'BII_V1100.dll';
function BII_Set_GPI_Double_Signal_Tolerance; external 'BII_V1100.dll';
function BII_Reset_GPI_Double_Signal_Tolerance; external 'BII_V1100.dll';
function BII_Get_GPI_ID_Settings; external 'BII_V1100.dll';
function BII_Set_GPI_ID_Settings; external 'BII_V1100.dll';
function BII_Reset_GPI_ID_Settings; external 'BII_V1100.dll';
function BII_Get_MV1200_Step_Upgrade_Result; external 'BII_V1100.dll';
function BII_Get_Password_Mode; external 'BII_V1100.dll';
function BII_Set_Password_Mode; external 'BII_V1100.dll';
function BII_Reset_Password_Mode; external 'BII_V1100.dll';
function BII_Get_MultiFinger_Params; external 'BII_V1100.dll';
function BII_Set_MultiFinger_Params; external 'BII_V1100.dll';
function BII_Reset_MultiFinger_Params; external 'BII_V1100.dll';
function BII_Get_Polling_Counter; external 'BII_V1100.dll';
function BII_Get_Polling_Data; external 'BII_V1100.dll';
function BII_Clear_Polling_Data; external 'BII_V1100.dll';
function BII_Set_Polling_Mode; external 'BII_V1100.dll';
function BII_Get_Polling_Mode; external 'BII_V1100.dll';
function BII_Reset_Polling_Mode; external 'BII_V1100.dll';
function BII_Get_Duress_Mode; external 'BII_V1100.dll';
function BII_Set_Duress_Mode; external 'BII_V1100.dll';
function BII_Reset_Duress_Mode; external 'BII_V1100.dll';
function BII_Get_Extended_Wiegand_StringEx; external 'BII_V1100.dll';
function BII_ESI_Host_Mode; external 'BII_V1100.dll';
function BII_ESI_Host_ModeEx; external 'BII_V1100.dll';
function BII_ESI_Status; external 'BII_V1100.dll';
function BII_ESI_Version; external 'BII_V1100.dll';
function BII_ESI_Send_Index_Template; external 'BII_V1100.dll';
function BII_ESI_Get_Unit_Type; external 'BII_V1100.dll';
function BII_ESI_Get_Template; external 'BII_V1100.dll';
function BII_ESI_Erase_Templates; external 'BII_V1100.dll';
function BII_ESI_Get_Max_NTemplates; external 'BII_V1100.dll';
function BII_ESI_Set_Max_NTemplates; external 'BII_V1100.dll';
function BII_ESI_Get_Num_Templates; external 'BII_V1100.dll';
function BII_ESI_Get_Template_List; external 'BII_V1100.dll';
function BII_ESI_Get_Template_Admin_List; external 'BII_V1100.dll';
function BII_ESI_Get_Flash_Block; external 'BII_V1100.dll';
function BII_ESI_Send_Flash_Block; external 'BII_V1100.dll';
function BII_ESI_Check_Template; external 'BII_V1100.dll';
function BII_ESI_Is_SmartCard_Present; external 'BII_V1100.dll';
function BII_ESI_Get_Template_Name; external 'BII_V1100.dll';
function BII_ESI_Set_Template_Security; external 'BII_V1100.dll';
function BII_ESI_Get_Template_Security; external 'BII_V1100.dll';
function BII_Program_ESI_Controller; external 'BII_V1100.dll';
function BII_ESI_Set_Smartcard_Password; external 'BII_V1100.dll';
function BII_ESI_Check_Smartcard_Password; external 'BII_V1100.dll';
function BII_ESI_Get_Smartcard_Layout; external 'BII_V1100.dll';
function BII_ESI_Set_Smartcard_Layout; external 'BII_V1100.dll';
function BII_ESI_Reset_Smartcard_Layout; external 'BII_V1100.dll';
function BII_ESI_Get_Wiegand_String; external 'BII_V1100.dll';
function BII_ESI_Set_Wiegand_String; external 'BII_V1100.dll';
function BII_ESI_Erase_Wiegand_String; external 'BII_V1100.dll';
function BII_ESI_Set_Store_Wiegand; external 'BII_V1100.dll';
function BII_ESI_Get_Store_Wiegand; external 'BII_V1100.dll';
function BII_ESI_Set_Verification_Timeout; external 'BII_V1100.dll';
function BII_ESI_Get_Verification_Timeout; external 'BII_V1100.dll';
function BII_ESI_Unsecure_Smartcard; external 'BII_V1100.dll';
function BII_ESI_Secure_Smartcard; external 'BII_V1100.dll';
function BII_ESI_Get_SiteKey_Mode; external 'BII_V1100.dll';
function BII_ESI_Set_SiteKey_Mode; external 'BII_V1100.dll';
function BII_ESI_Update_Smartcard_Password_Mode4; external 'BII_V1100.dll';
function BII_ESI_Set_ReadOnly_Mode; external 'BII_V1100.dll';
function BII_ESI_Get_ReadOnly_Mode; external 'BII_V1100.dll';
function BII_ESI_Reset_ReadOnly_Mode; external 'BII_V1100.dll';
function BII_ESI_Set_Auto_SiteKey_Update; external 'BII_V1100.dll';
function BII_ESI_Get_Auto_SiteKey_Update; external 'BII_V1100.dll';
function BII_ESI_Reset_Auto_SiteKey_Update; external 'BII_V1100.dll';
function BII_ESI_Get_Smartcard_Manufacturer_Info; external 'BII_V1100.dll';
function BII_ESI_IClass_Get_Flash_Block; external 'BII_V1100.dll';
function BII_ESI_IClass_Send_Flash_Block; external 'BII_V1100.dll';
function BII_ESI_IClass_Set_Page_Layout; external 'BII_V1100.dll';
function BII_ESI_IClass_Get_Page_Layout; external 'BII_V1100.dll';
function BII_ESI_IClass_Reset_Page_Layout; external 'BII_V1100.dll';
function BII_ESI_IClass_Set_Page_Offset; external 'BII_V1100.dll';
function BII_ESI_IClass_Get_Page_Offset; external 'BII_V1100.dll';
function BII_ESI_IClass_Reset_Page_Offset; external 'BII_V1100.dll';
function BII_ESI_IClass_Get_Smartcard_Info; external 'BII_V1100.dll';
function BII_ESI_IClass_Get_Smartcard_InfoEx; external 'BII_V1100.dll';
function BII_ESI_IClass_Get_Default_Smartcard_Key_Description; external 'BII_V1100.dll';
function BII_ESI_IClass_Set_Default_Smartcard_Key; external 'BII_V1100.dll';
function BII_ESI_IClass_Reset_Default_Smartcard_Key; external 'BII_V1100.dll';
function BII_Verify_Test:Integer; external 'BII_V1100.dll';
function BII_Identify_Test:Integer; external 'BII_V1100.dll';
function BII_Enroll_Test:Integer; external 'BII_V1100.dll';
function BII_Capture_Dn_Raw_Image; external 'BII_V1100.dll';
function BII_Test_Wiegand_In; external 'BII_V1100.dll';
function BII_Get_Wiegand_Test_Res; external 'BII_V1100.dll';
function BII_Send_Wiegand_Out; external 'BII_V1100.dll';
function BII_Initialize_Socket_Communications; external 'BII_V1100.dll';
function BII_Open_TCPIP_Communications; external 'BII_V1100.dll';
function BII_Close_TCPIP_Communications; external 'BII_V1100.dll';
function BII_Cleanup_Socket_Communications; external 'BII_V1100.dll';
function BII_Get_Current_TCP_Port; external 'BII_V1100.dll';
function BII_Get_Current_Host_Name; external 'BII_V1100.dll';
function BII_Set_Socket_Buffer_Size; external 'BII_V1100.dll';
function BII_Get_Socket_Buffer_Size; external 'BII_V1100.dll';
function BII_Get_Local_IP_Address; external 'BII_V1100.dll';
function BII_Is_Using_TCPIP; external 'BII_V1100.dll';
function BII_Get_Subnet_Mask; external 'BII_V1100.dll';
function BII_Set_Subnet_Mask; external 'BII_V1100.dll';
function BII_Set_Window_Handle; external 'BII_V1100.dll';
function BII_Set_TCP_Connection_Timeout; external 'BII_V1100.dll';
function BII_Get_TCP_Connection_Timeout; external 'BII_V1100.dll';
function BII_Get_Product_Name_From_Type:Integer; external 'BII_V1100.dll';
function BII_Get_Product_Type_From_Name; external 'BII_V1100.dll';
function BII_Get_TCP_NoDelay; external 'BII_V1100.dll';
function BII_Set_TCP_NoDelay; external 'BII_V1100.dll';
function BII_Erase_Transaction_Log; external 'BII_V1100.dll';
function BII_Read_Transaction_Log; external 'BII_V1100.dll';
function BII_Get_Num_Transaction_Log; external 'BII_V1100.dll';
function BII_Get_CM_Version; external 'BII_V1100.dll';
function BII_Get_Kit_Version; external 'BII_V1100.dll';
function BII_Get_Date_Time; external 'BII_V1100.dll';
function BII_Set_Date_Time; external 'BII_V1100.dll';
function BII_Set_Date_TimeEx; external 'BII_V1100.dll';
function BII_VS_Get_Key_Press; external 'BII_V1100.dll';
function BII_VS_Test_LCD; external 'BII_V1100.dll';
function BII_VS_Get_MAC_Address; external 'BII_V1100.dll';
function BII_VS_Get_Date_Time_Settings; external 'BII_V1100.dll';
function BII_VS_Set_Date_Time_Settings; external 'BII_V1100.dll';
function BII_VS_Set_Date_Time_SettingsEx; external 'BII_V1100.dll';
function BII_VS_Reset_Date_Time_Settings; external 'BII_V1100.dll';
function BII_VS_Show_LCD_Text; external 'BII_V1100.dll';
function BII_VS_Get_IP_Address; external 'BII_V1100.dll';
function BII_VS_Set_IP_Address; external 'BII_V1100.dll';
function BII_VS_Get_DHCP_Mode; external 'BII_V1100.dll';
function BII_VS_Set_DHCP_Mode; external 'BII_V1100.dll';
function BII_VS_Get_Current_IP_Address; external 'BII_V1100.dll';
function BII_VS_Set_Bio_Access_Schedule; external 'BII_V1100.dll';
function BII_VS_Get_Bio_Access_Schedule; external 'BII_V1100.dll';
function BII_VS_Reset_Bio_Access_Schedule; external 'BII_V1100.dll';
function BII_VS_Upload_Menu; external 'BII_V1100.dll';
function BII_VS_Set_Menu; external 'BII_V1100.dll';
function BII_VS_Get_Menu_Info; external 'BII_V1100.dll';
function BII_VS_Get_Menu_Name; external 'BII_V1100.dll';
function BII_VS_Set_LCD_Settings; external 'BII_V1100.dll';
function BII_VS_Get_LCD_Settings; external 'BII_V1100.dll';
function BII_VS_Reset_LCD_Settings; external 'BII_V1100.dll';
function BII_VS_Erase_Menu; external 'BII_V1100.dll';
function BII_VS_Set_Access_Schedule_Settings; external 'BII_V1100.dll';
function BII_VS_Get_Access_Schedule_Settings; external 'BII_V1100.dll';
function BII_VS_Reset_Access_Schedule_Settings; external 'BII_V1100.dll';
function BII_VS_Set_Access_Schedule; external 'BII_V1100.dll';
function BII_VS_Get_Access_Schedule; external 'BII_V1100.dll';
function BII_VS_Reset_Access_Schedule; external 'BII_V1100.dll';
function BII_VS_Get_Num_Defined_Access_Schedules; external 'BII_V1100.dll';
function BII_VS_Get_Defined_Access_Schedule_List; external 'BII_V1100.dll';
function BII_VS_Set_Holiday_Schedule; external 'BII_V1100.dll';
function BII_VS_Get_Holiday_Schedule; external 'BII_V1100.dll';
function BII_VS_Reset_Holiday_Schedule; external 'BII_V1100.dll';
function BII_VS_Is_Template_Scheduled; external 'BII_V1100.dll';

end.
