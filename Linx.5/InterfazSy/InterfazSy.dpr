program InterfazSy;



uses
  Forms,
  ComObj,
  ActiveX,
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  ZBaseImportaShell in '..\..\Tools\ZBaseImportaShell.pas' {BaseImportaShell},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  FEditAlarmas in 'FEditAlarmas.pas' {EditAlarmas},
  FGeneral in 'FGeneral.pas' {General},
  FAlarmas in 'FAlarmas.pas' {Alarmas};

{$R *.RES}
{$R WindowsXP.res}

{procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     {if ( ParamCount = 0 ) then
        MuestraSplash;}
     Application.Title := 'Interfaz Sy';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    //CierraSplash;
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    //CierraSplash;
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
