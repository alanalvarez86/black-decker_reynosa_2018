unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell, Psock, NMsmtp, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, FileCTRL, ZBaseConsulta, ZetaKeyLookup;

type
  TTressShell = class(TBaseImportaShell)
    Panel2: TPanel;
    Cofiguracin1: TMenuItem;
    General1: TMenuItem;
    Alarmas1: TMenuItem;
    _ConfGeneral: TAction;
    _ConfAlarmas: TAction;
    GroupBox1: TGroupBox;
    cbTipoMovimiento: TComboBox;
    Label1: TLabel;
    cbReloj: TZetaKeyCombo;
    txtReloj: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
    procedure _ConfAlarmasExecute(Sender: TObject);
    procedure _ConfGeneralExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbTipoMovimientoChange(Sender: TObject);
  private
    { Private declarations }
    lModoBatch: Boolean;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure ActivaReloj;
  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure SetControlesBatch; override;
    procedure ProcesarArchivo; override;
  public
    { Public declarations }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
    procedure AgregarListaReloj;
    procedure CambiaSistemaActivos;
    procedure CambiaPeriodoActivos;
  end;

var
  TressShell: TTressShell;

implementation

uses DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DTablas,
     DRecursos,
     DConsultas,
     DProcesos,
     DInterfase,
     dReportes,
     ZAsciiTools,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseEdicion,
     FCalendario,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaDialogo,
     FGeneral,
     FAlarmas;

const
     K_MOVIMIENTO = 4;
     K_RELOJ      = 5;

{$R *.dfm}

procedure TTressShell.FormCreate(Sender: TObject);
const
     K_BATCH_SINTAXIS = '%s <EMPRESA> <USUARIO> <PASSWORD> <TIPO DE MOVIMIENTO [0-1]> <RELOJ>';
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     inherited;
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );

     EnviarCorreo := FALSE;
     SintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     lModoBatch := False;

end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmInterfase );
     FreeAndNil( dmReportes );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
{$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
{$endif}
     inherited;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;
        end;
        InitBitacora;
        CargaSistemaActivos;

        AgregarListaReloj;

     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( 'Error al conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
     ActivaControles( FALSE );
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmConsultas );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoProcesar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     SetNombreLogs;
     try
        with dmInterfase do
        begin
             TipoMovimiento := eTipoMovimiento( cbTipoMovimiento.ItemIndex );
             if( ModoBatch )then
                 Reloj := txtReloj.Text
             else
                 Reloj := Trim( cbReloj.Text );
                 
             ArchivoReporte := ArchOrigen.Text;
             Procesa;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
     with dmCliente do
     begin
          if ( not ModoBatch ) then
          begin
               if lProcesando and ( BtnDetener.CanFocus ) then
                  BtnDetener.SetFocus
               else if EmpresaAbierta and ( ArchOrigen.CanFocus ) then
                       ArchOrigen.SetFocus
                    else if BtnSalir.CanFocus then
                            BtnSalir.SetFocus;
          end;
     end;
end;

procedure TTressShell.SetControlesBatch;
begin
     inherited;
     EnviarCorreo := FALSE;
     SetNombreLogs;
     cbTipoMovimiento.ItemIndex := StrToIntDef( ParamStr( K_MOVIMIENTO ), Ord( tmEmpleados ) );
     if( cbTipoMovimiento.ItemIndex = 1 )then
         txtReloj.Text := Copy( ParamStr( K_RELOJ ), 1, 4 )
     else
         txtReloj.Text := VACIO;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     RefrescaSistemaActivos;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre, sArchBit, sArchErr: String;
begin
     with dmCliente do
     begin
          sNombre := FechaToStr( FechaDefault ) + FormatDateTime( 'hhmmss', Time ) + '.LOG';
          sArchBit := ZetaMessages.SetFileNameDefaultPath( 'BITACORA-' + sNombre );
          sArchErr := ZetaMessages.SetFileNameDefaultPath( 'ERRORES-' + sNombre );
     end;

     if StrVacio( ArchBitacora.Text )  then
          ArchBitacora.Text := sArchBit;

     if StrVacio( ArchErrores.Text ) then
          ArchErrores.Text := sArchErr;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.ProcesarArchivo;
begin
     ArchOrigen.Text := dmInterfase.ObtenRutaReporte( StrToIntDef( dmInterfase.IniValues.Reporte, 0 ) ) ;
     if strLleno( ArchOrigen.Text ) then
     begin
          if FileExists( ArchOrigen.Text ) then
          begin
               if( ( cbTipoMovimiento.ItemIndex = 0 )or
                   ( ( cbTipoMovimiento.ItemIndex = 1 )and( cbReloj.ItemIndex > 0 ) )or
                   ( ModoBatch and StrLleno( txtReloj.Text ) ) )then
               begin
                    ActivaControles( TRUE );
                    try
                       InitBitacora;
                       DoProcesar;
                    finally
                       EscribeBitacoras;
                       ActivaControles( FALSE );
                    end;
               end
               else
               begin
                    ReportaErrorDialogo( self.Caption, 'Para procesos de Alarmas es necesario especificar un Reloj' );
                    if not ModoBatch then
                       cbReloj.SetFocus;
               end;
          end
          else
          begin
               ReportaErrorDialogo( self.Caption, 'No se Encontr� el Archivo de Origen Especificado en el Reporte' );
               //ActiveControl := ArchOrigen;
          end;
     end
     else
     begin
          ReportaErrorDialogo( self.Caption, 'Falta Especificar Archivo de Origen en el Reporte' );
          //ActiveControl := ArchOrigen;
     end;
end;

procedure TTressShell.zFechaValidDate(Sender: TObject);
begin
     inherited;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     SetNombreLogs;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     // No Implementar
end;

procedure TTressShell._ConfAlarmasExecute(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( Alarmas, TAlarmas );
end;

procedure TTressShell._ConfGeneralExecute(Sender: TObject);
begin
     inherited;
     with TGeneral.Create( Self )do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TTressShell.ActivaReloj;
begin
     cbReloj.Enabled := ( cbTipoMovimiento.ItemIndex = 1 );
     if Not cbReloj.Enabled then
     begin
          if( cbReloj.Items.Count > 0 )then
              cbReloj.ItemIndex := 0;
     end;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     ActivaReloj;
end;

procedure TTressShell.cbTipoMovimientoChange(Sender: TObject);
begin
     inherited;
     ActivaReloj;
end;

procedure TTressShell.AgregarListaReloj;
var
   oReloj: TStrings;
begin
        oReloj := TStringList.Create;
        oReloj.Add( '-- Reloj --' );
        with dmInterfase.cdsReloj do
        begin
             Conectar;
             if( RecordCount > 0 )then
             begin
                  First;
                  while not EoF do
                  begin
                       oReloj.Add( Trim( FieldByName( 'RELOJ' ).AsString ) );
                       Next;
                  end;
             end;
        end;
        cbReloj.Items := oReloj;
        cbReloj.ItemIndex := 0;
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
   //{AV 5/Julio/2011} DUMMY Para adaptarse a los cambios que hubo en DRecursos.AgregaKardex en Version 2011
end;

procedure TTressShell.CambiaSistemaActivos;
begin
   //{AV 5/Julio/2011} DUMMY Para adaptarse a los cambios que hubo en DRecursos.AgregaKardex en Version 2011
end;

end.
