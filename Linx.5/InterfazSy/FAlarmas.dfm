inherited Alarmas: TAlarmas
  Caption = 'Alarmas'
  ClientHeight = 431
  ClientWidth = 606
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 395
    Width = 606
    inherited OK: TBitBtn
      Left = 438
    end
    inherited Cancelar: TBitBtn
      Left = 523
    end
  end
  inherited PanelSuperior: TPanel
    Width = 606
    inherited AgregarBtn: TSpeedButton
      Left = 6
    end
    inherited BorrarBtn: TSpeedButton
      Left = 30
    end
    inherited ModificarBtn: TSpeedButton
      Left = 54
      Width = 23
    end
    inherited BuscarBtn: TSpeedButton
      Left = 78
      Hint = 'Refrescar (F5)'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333338083333333333380083333
        3333333300833333333333380833333333333330033333333333333003330000
        0333333008333800033333380083800003333333000000080333333338000833
        0333333333333333333333333333333333333333333333333333}
    end
    inherited DBNavigator: TDBNavigator
      Left = 112
    end
  end
  inherited PanelIdentifica: TPanel
    Top = 85
    Width = 606
    inherited ValorActivo2: TPanel
      Width = 280
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 32
    Width = 606
    Height = 53
    Align = alTop
    TabOrder = 3
    object GroupBox1: TGroupBox
      Left = 4
      Top = 2
      Width = 174
      Height = 46
      Caption = ' Filtro '
      TabOrder = 0
      object Label1: TLabel
        Left = 11
        Top = 19
        Width = 27
        Height = 13
        Caption = 'Reloj:'
      end
      object txtReloj: TEdit
        Left = 39
        Top = 15
        Width = 44
        Height = 21
        CharCase = ecUpperCase
        MaxLength = 4
        TabOrder = 0
      end
      object BitBtn1: TBitBtn
        Left = 86
        Top = 12
        Width = 81
        Height = 25
        Caption = 'Refrescar'
        TabOrder = 1
        OnClick = BitBtn1Click
        Glyph.Data = {
          42020000424D4202000000000000420000002800000010000000100000000100
          1000030000000002000000000000000000000000000000000000007C0000E003
          00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
          1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
          00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C}
      end
    end
  end
  object grAlarmas: TZetaDBGrid [4]
    Left = 0
    Top = 104
    Width = 606
    Height = 291
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = grAlarmasDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'RELOJ'
        Title.Caption = 'Reloj'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HORA'
        Title.Caption = 'Hora'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LUNES'
        Title.Caption = 'Lun.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARTES'
        Title.Caption = 'Mar.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MIERCOLES'
        Title.Caption = 'Mie.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'JUEVES'
        Title.Caption = 'Jue.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VIERNES'
        Title.Caption = 'Vie.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'SABADO'
        Title.Caption = 'Sab.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DOMINGO'
        Title.Caption = 'Dom.'
        Width = 30
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TPO_OPERA'
        Title.Caption = 'Opeaci'#243'n'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RELAY'
        Title.Caption = 'Relay'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ACTIVO'
        Title.Caption = 'Activo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INACTIVO'
        Title.Caption = 'Inactivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Caption = 'Total'
        Width = 50
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 268
    Top = 57
  end
end
