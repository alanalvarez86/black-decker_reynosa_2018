unit FEditAlarmas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, Mask, ZetaNumero, ZetaEdit, ZetaHora;

type
  TEditAlarmas = class(TBaseEdicion)
    Label1: TLabel;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    LUNES: TCheckBox;
    MARTES: TCheckBox;
    MIERCOLES: TCheckBox;
    JUEVES: TCheckBox;
    SABADO: TCheckBox;
    DOMINGO: TCheckBox;
    TODOS: TCheckBox;
    TPO_OPERA: TRadioGroup;
    RELAY: TZetaDBNumero;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    ACTIVO: TZetaDBNumero;
    TOTAL: TZetaDBNumero;
    INACTIVO: TZetaDBNumero;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    VIERNES: TCheckBox;
    Label10: TLabel;
    RELOJ: TZetaDBEdit;
    Label11: TLabel;
    HORA: TZetaDBHora;
    procedure FormShow(Sender: TObject);
    procedure DIASClick(Sender: TObject);
    procedure TPO_OPERAClick(Sender: TObject);
    procedure TIEMPOChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FCambioInterno: boolean;
    procedure TodosLosDias( const lTodos: Boolean );
    procedure ActivaBotonTodos;
  protected
    { Private declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditAlarmas: TEditAlarmas;

implementation

uses DInterfase,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZAccesosTress;

const
     K_ABIERTO = 'R';
     K_CERRADO = 'S';
     K_PULSO   = 'P';

{$R *.dfm}

{ TEditAlarmas }

procedure TEditAlarmas.Connect;
begin
     with dmInterfase do
     begin
          cdsAlarmas.Conectar;
          DataSource.DataSet := cdsAlarmas;
     end;
end;

procedure TEditAlarmas.FormShow(Sender: TObject);
begin
     inherited;
     with dmInterfase.cdsAlarmas do
     begin
          if( State in [ dsEdit, dsBrowse ] )then
          begin
               // Asigna d�as.
               FCambioInterno := TRUE;
               LUNES.checked := ( FieldByName( 'LUNES' ).AsString = K_GLOBAL_SI );
               MARTES.checked := ( FieldByName( 'MARTES' ).AsString = K_GLOBAL_SI );
               MIERCOLES.checked := ( FieldByName( 'MIERCOLES' ).AsString = K_GLOBAL_SI );
               JUEVES.checked := ( FieldByName( 'JUEVES' ).AsString = K_GLOBAL_SI );
               VIERNES.checked := ( FieldByName( 'VIERNES' ).AsString = K_GLOBAL_SI );
               SABADO.checked := ( FieldByName( 'SABADO' ).AsString = K_GLOBAL_SI );
               DOMINGO.checked := ( FieldByName( 'DOMINGO' ).AsString = K_GLOBAL_SI );
               FCambioInterno := FALSE;

               ActivaBotonTodos;

               // Asigna tipo de operacion.
               if( FieldByName( 'TPO_OPERA' ).AsString = K_ABIERTO )then
                   TPO_OPERA.ItemIndex := 1
               else if( FieldByName( 'TPO_OPERA' ).AsString = K_CERRADO )then
                   TPO_OPERA.ItemIndex := 0
               else
                   TPO_OPERA.ItemIndex := 2;
          end
          else if( State = dsInsert )then
          begin
               FCambioInterno := TRUE;
               LUNES.Checked := FALSE;
               MARTES.Checked := FALSE;
               MIERCOLES.Checked := FALSE;
               JUEVES.Checked := FALSE;
               VIERNES.Checked := FALSE;
               SABADO.Checked := FALSE;
               DOMINGO.Checked := FALSE;
               TODOS.Checked := FALSE;
               FCambioInterno := FALSE;

               TPO_OPERA.ItemIndex := 0;
          end;
          RELOJ.SetFocus;
     end;

     Datasource.AutoEdit := TRUE;
end;

procedure TEditAlarmas.ActivaBotonTodos;
begin
     FCambioInterno := TRUE;
     TODOS.checked := ( LUNES.checked and MARTES.checked and MIERCOLES.checked and JUEVES.checked and
                        VIERNES.checked and SABADO.checked and DOMINGO.checked );
     FCambioInterno := FALSE;
end;

procedure TEditAlarmas.TodosLosDias( const lTodos: Boolean );
begin
     FCambioInterno := TRUE;
     if FCambioInterno then
     begin
          LUNES.checked := lTodos;
          MARTES.checked := lTodos;
          MIERCOLES.checked := lTodos;
          JUEVES.checked := lTodos;
          VIERNES.checked := lTodos;
          SABADO.checked := lTodos;
          DOMINGO.checked := lTodos;
     end;
     FCambioInterno := FALSE;
end;

procedure TEditAlarmas.DIASClick(Sender: TObject);
const
     K_TODOS = 'TODOS';
begin
     if not FCambioInterno then
     begin
          if( TCheckBox( Sender ).Name = K_TODOS )then
              TodosLosDias( TCheckBox( Sender ).Checked )
          else
              ActivaBotonTodos;
     end;
end;

procedure TEditAlarmas.TPO_OPERAClick(Sender: TObject);
begin
     inherited;
     if( TPO_OPERA.ItemIndex = 2 )then
     begin
          ACTIVO.Enabled := TRUE;
          INACTIVO.Enabled := TRUE;
     end
     else
     begin
          ACTIVO.Valor := 0;
          ACTIVO.Enabled := FALSE;
          INACTIVO.Valor := 0;
          INACTIVO.Enabled := FALSE;
          TOTAL.Valor := 0;
     end;
end;

procedure TEditAlarmas.TIEMPOChange(Sender: TObject);
begin
     TOTAL.Valor := ( ACTIVO.Valor + INACTIVO.Valor );
end;

procedure TEditAlarmas.FormCreate(Sender: TObject);
begin
     inherited;
     //IndexDerechos := D_EMP_DATOS_IDENTIFICA;
     FCambioInterno := FALSE;
end;

procedure TEditAlarmas.OKClick(Sender: TObject);
var
   sReloj, sHora: string;
begin
    // Asigna d�as.
    with dmInterfase.cdsAlarmas do
    begin
         if( LUNES.checked )then
             FieldByName( 'LUNES' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'LUNES' ).AsString := K_GLOBAL_NO;

         if( MARTES.checked )then
             FieldByName( 'MARTES' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'MARTES' ).AsString := K_GLOBAL_NO;

         if( MIERCOLES.checked )then
             FieldByName( 'MIERCOLES' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'MIERCOLES' ).AsString := K_GLOBAL_NO;

         if( JUEVES.checked )then
             FieldByName( 'JUEVES' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'JUEVES' ).AsString := K_GLOBAL_NO;

         if( VIERNES.checked )then
             FieldByName( 'VIERNES' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'VIERNES' ).AsString := K_GLOBAL_NO;

         if( SABADO.checked )then
             FieldByName( 'SABADO' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'SABADO' ).AsString := K_GLOBAL_NO;

         if( DOMINGO.checked )then
             FieldByName( 'DOMINGO' ).AsString := K_GLOBAL_SI
         else
             FieldByName( 'DOMINGO' ).AsString := K_GLOBAL_NO;

         // Asigna tipo de operacion.
         if( TPO_OPERA.ItemIndex = 1 )then
             FieldByName( 'TPO_OPERA' ).AsString := K_ABIERTO
         else if( TPO_OPERA.ItemIndex = 0 )then
              FieldByName( 'TPO_OPERA' ).AsString := K_CERRADO
         else
             FieldByName( 'TPO_OPERA' ).AsString := K_PULSO;
    end;

    inherited;

    with dmInterfase.cdsAlarmas do
    begin
         sReloj := FieldByName( 'RELOJ' ).AsString;
         sHora := FieldByName( 'HORA' ).AsString;
         Refrescar;
         Locate( 'RELOJ;HORA', VarArrayOf( [ sReloj, sHora ] ), [ loCaseInsensitive ] );
    end;

    // Refresca cat�logo de relojes
    dmInterfase.cdsReloj.Refrescar;

    Close;
end;

procedure TEditAlarmas.DBNavigatorClick(Sender: TObject;
  Button: TNavigateBtn);
begin
     inherited;
     with dmInterfase.cdsAlarmas do
     begin
          if( State = dsBrowse )then
          begin
               // Asigna d�as.
               FCambioInterno := TRUE;
               LUNES.checked := ( FieldByName( 'LUNES' ).AsString = K_GLOBAL_SI );
               MARTES.checked := ( FieldByName( 'MARTES' ).AsString = K_GLOBAL_SI );
               MIERCOLES.checked := ( FieldByName( 'MIERCOLES' ).AsString = K_GLOBAL_SI );
               JUEVES.checked := ( FieldByName( 'JUEVES' ).AsString = K_GLOBAL_SI );
               VIERNES.checked := ( FieldByName( 'VIERNES' ).AsString = K_GLOBAL_SI );
               SABADO.checked := ( FieldByName( 'SABADO' ).AsString = K_GLOBAL_SI );
               DOMINGO.checked := ( FieldByName( 'DOMINGO' ).AsString = K_GLOBAL_SI );
               FCambioInterno := FALSE;

               ActivaBotonTodos;

               // Asigna tipo de operacion.
               if( FieldByName( 'TPO_OPERA' ).AsString = K_ABIERTO )then
                   TPO_OPERA.ItemIndex := 1
               else if( FieldByName( 'TPO_OPERA' ).AsString = K_CERRADO )then
                   TPO_OPERA.ItemIndex := 0
               else
                   TPO_OPERA.ItemIndex := 2;
          end;
     end;
end;

procedure TEditAlarmas.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
