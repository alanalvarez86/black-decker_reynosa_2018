unit FGeneral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ZetaEdit, ZetaKeyLookup, ExtCtrls, FileCTRL;

type
  TGeneral = class(TForm)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    luReporte: TZetaKeyLookup;
    txtRutaEmpleado: TZetaEdit;
    Label1: TLabel;
    Label2: TLabel;
    ArchivoSeek: TSpeedButton;
    Label3: TLabel;
    txtRutaAlarma: TZetaEdit;
    SpeedButton1: TSpeedButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure GuardaConfiguracion;
  public
    { Public declarations }
  end;

var
  General: TGeneral;

implementation

uses dInterfase,
     ZetaCommonClasses,
     dReportes,
     ZetaDialogo;

{$R *.dfm}

procedure TGeneral.BitBtn1Click(Sender: TObject);
begin
     GuardaConfiguracion;
     inherited;
end;

procedure TGeneral.GuardaConfiguracion;
begin
     try
        dmInterfase.IniValues.RutaEmpleado := txtRutaEmpleado.Text;
        dmInterfase.IniValues.RutaAlarma := txtRutaAlarma.Text;
        dmInterfase.IniValues.Reporte := luReporte.Llave;
        ZetaDialogo.ZInformation( Self.Caption, 'Configuración almacenada', 0 );
     except
           on Error : Exception do
              zError( Self.Caption, Format( 'Se generó un error al almacenar configuración: %s', [ Error.Message ] ), 0 );
     end;
end;

procedure TGeneral.FormShow(Sender: TObject);
const
     K_FILTRO_REPO = '( RE_TIPO = 0 )';
begin
     dmReportes.cdsLookUpReportes.Refrescar;
     dmReportes.cdsLookUpReportes.Filter := K_FILTRO_REPO;
     dmReportes.cdsLookUpReportes.Filtered := TRUE;
     luReporte.LookUpDataSet := dmReportes.cdsLookUpReportes;

     txtRutaEmpleado.Text := dmInterfase.IniValues.RutaEmpleado;
     txtRutaAlarma.Text := dmInterfase.IniValues.RutaAlarma;
     luReporte.Llave := dmInterfase.IniValues.Reporte;
end;

procedure TGeneral.ArchivoSeekClick(Sender: TObject);
var
   sDirectory : string;
begin
  //inherited;
  sDirectory := txtRutaEmpleado.Text;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        txtRutaEmpleado.Text := sDirectory;
end;

procedure TGeneral.SpeedButton1Click(Sender: TObject);
var
   sDirectory : string;
begin
  //inherited;
  sDirectory := txtRutaAlarma.Text;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        txtRutaAlarma.Text := sDirectory;
end;

end.
