inherited TressShell: TTressShell
  Left = 356
  Top = 178
  Width = 575
  Height = 448
  Caption = 'Synel CFG'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 371
    Width = 559
  end
  inherited PageControl: TPageControl
    Top = 128
    Width = 559
    Height = 243
    TabOrder = 2
    inherited TabBitacora: TTabSheet
      Caption = '&Bit'#225'cora'
      inherited MemBitacora: TMemo
        Width = 551
        Height = 174
        Hint = 'Bit'#225'cora del proceso'
        Color = clBtnFace
        ParentShowHint = False
        ScrollBars = ssBoth
        ShowHint = True
      end
      inherited PanelBottom: TPanel
        Top = 174
        Width = 551
        inherited BitacoraSeek: TSpeedButton
          Left = 521
          Top = 8
          Hint = 'Buscar archivo para guardar la bit'#225'cora del proceso'
          Anchors = [akTop, akRight]
        end
        inherited LblGuardarBIT: TLabel
          Left = 7
          Top = 13
          Caption = '&Guardar En:'
          FocusControl = ArchBitacora
        end
        inherited ArchBitacora: TEdit
          Left = 66
          Top = 10
          Width = 455
          Hint = 'Indicar archivo para guardar la bit'#225'cora del proceso'
          Anchors = [akLeft, akTop, akRight]
          ParentShowHint = False
          ShowHint = True
        end
      end
    end
    inherited TabErrores: TTabSheet
      Caption = '&Errores'
      inherited Panel1: TPanel
        Top = 174
        Width = 551
        inherited ErroresSeek: TSpeedButton
          Left = 521
          Top = 8
          Hint = 'Buscar archivo para guardar la bit'#225'cora de errores'
        end
        inherited LblGuardarERR: TLabel
          Left = 7
          Top = 13
          Caption = '&Guardar En:'
          FocusControl = ArchErrores
        end
        inherited ArchErrores: TEdit
          Left = 66
          Top = 10
          Width = 455
          Hint = 'Indicar archivo para guardar la bit'#225'cora de errores'
          ParentShowHint = False
          ShowHint = True
        end
      end
      inherited MemErrores: TMemo
        Width = 551
        Height = 174
        Hint = 'Bit'#225'cora de errores'
        Color = clBtnFace
        ParentShowHint = False
        ScrollBars = ssBoth
        ShowHint = True
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 96
    Width = 559
    Height = 32
    TabOrder = 1
    inherited BtnProcesar: TBitBtn
      Top = 3
      Hint = 'Iniciar proceso'
      Caption = '&Procesar'
    end
    inherited BtnDetener: TBitBtn
      Left = 138
      Top = 3
      Hint = 'Cancelar proceso'
      Caption = '&Cancelar proceso'
      Visible = False
    end
    inherited BtnSalir: TBitBtn
      Left = 426
      Top = 3
      Hint = 'Salir de la aplicaci'#243'n'
    end
  end
  inherited PanelEncabezado: TPanel
    Width = 559
    Height = 33
    TabOrder = 0
    Visible = False
    inherited LblArchivo: TLabel
      Left = 7
      Top = 8
      Caption = '&Archivo Origen:'
    end
    inherited ArchivoSeek: TSpeedButton
      Left = 531
      Top = 3
      Hint = 'Buscar archivo origen'
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
    end
    inherited ArchOrigen: TEdit
      Left = 82
      Top = 5
      Width = 446
      Hint = 'Especificar archivo origen'
      Anchors = [akLeft, akTop, akRight]
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 33
    Width = 559
    Height = 63
    Align = alTop
    TabOrder = 4
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 557
      Height = 61
      Align = alClient
      Caption = ' Seleccionar tipo de archivo a generar: '
      TabOrder = 0
      object Label1: TLabel
        Left = 160
        Top = 27
        Width = 27
        Height = 13
        Caption = 'Reloj:'
      end
      object cbTipoMovimiento: TComboBox
        Left = 8
        Top = 24
        Width = 145
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        ItemIndex = 0
        TabOrder = 0
        Text = 'Lista de empleados'
        OnChange = cbTipoMovimientoChange
        Items.Strings = (
          'Lista de empleados'
          'Lista de alarmas')
      end
      object cbReloj: TZetaKeyCombo
        Left = 191
        Top = 23
        Width = 77
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object txtReloj: TEdit
        Left = 288
        Top = 24
        Width = 65
        Height = 21
        TabOrder = 2
        Visible = False
      end
    end
  end
  inherited LogoffTimer: TTimer
    Left = 232
    Top = 152
  end
  inherited ActionList: TActionList
    Left = 202
    Top = 152
    object _ConfGeneral: TAction
      Category = 'MenuArchivo'
      Caption = 'General'
      OnExecute = _ConfGeneralExecute
    end
    object _ConfAlarmas: TAction
      Category = 'MenuArchivo'
      Caption = 'Alarmas'
      OnExecute = _ConfAlarmasExecute
    end
  end
  inherited ShellMenu: TMainMenu
    Left = 176
    Top = 152
    inherited Archivo1: TMenuItem
      inherited ArchivoOtraEmpresa: TMenuItem
        Hint = 'Accesar otra empresa'
      end
      inherited ArchivoServidor: TMenuItem
        Hint = 'Especificar nombre del servidor remoto'
        ShortCut = 16467
      end
      inherited CancelarProceso1: TMenuItem
        Hint = 'Procesar archivo'
      end
      inherited CancelarProceso2: TMenuItem
        Visible = False
      end
      inherited ArchivoSalir: TMenuItem
        Hint = 'Salir de la aplicaci'#243'n'
      end
    end
    object Cofiguracin1: TMenuItem [1]
      Caption = 'Configuraci'#243'n'
      object General1: TMenuItem
        Action = _ConfGeneral
        ShortCut = 16455
      end
      object Alarmas1: TMenuItem
        Action = _ConfAlarmas
        ShortCut = 16449
      end
    end
  end
  inherited OpenDialog: TOpenDialog
    DefaultExt = 'TXT'
    Filter = 
      'Archivos de textos (*.txt)|*.txt|Archivos de delimitados (*.dat)' +
      '|*.dat|Todos (*.*)|*.*'
    Left = 262
    Top = 153
  end
  inherited ArbolImages: TImageList
    Left = 148
    Top = 152
  end
  inherited Email: TNMSMTP
    Left = 120
    Top = 152
  end
end
