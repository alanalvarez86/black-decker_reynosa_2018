inherited TressShell: TTressShell
  Left = 412
  Top = 205
  Width = 756
  Height = 557
  Caption = 'Interfaz PeopleSoft'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 492
    Width = 748
  end
  inherited PageControl: TPageControl
    Top = 359
    Width = 748
    Height = 100
    ActivePage = TabErrores
    TabOrder = 3
    inherited TabBitacora: TTabSheet
      Caption = '&Bit'#225'cora'
      inherited MemBitacora: TMemo
        Width = 740
        Height = 31
        Hint = 'Bit'#225'cora del proceso'
        Color = clBtnFace
        ParentShowHint = False
        ScrollBars = ssBoth
        ShowHint = True
      end
      inherited PanelBottom: TPanel
        Top = 31
        Width = 740
        Visible = False
        inherited BitacoraSeek: TSpeedButton
          Left = 503
          Top = 8
          Hint = 'Buscar archivo para guardar la bit'#225'cora del proceso'
        end
        inherited LblGuardarBIT: TLabel
          Left = 7
          Top = 13
          Caption = '&Guardar En:'
          FocusControl = ArchBitacora
        end
        inherited ArchBitacora: TEdit
          Left = 66
          Top = 10
          Width = 433
          Hint = 'Indicar archivo para guardar la bit'#225'cora del proceso'
          ParentShowHint = False
          ShowHint = True
        end
      end
    end
    inherited TabErrores: TTabSheet
      Caption = '&Errores'
      inherited Panel1: TPanel
        Top = 31
        Width = 740
        TabOrder = 1
        Visible = False
        inherited ErroresSeek: TSpeedButton
          Top = 8
          Hint = 'Buscar archivo para guardar la bit'#225'cora de errores'
        end
        inherited LblGuardarERR: TLabel
          Left = 7
          Top = 13
          Caption = '&Guardar En:'
          FocusControl = ArchErrores
        end
        inherited ArchErrores: TEdit
          Left = 66
          Top = 10
          Width = 309
          Hint = 'Indicar archivo para guardar la bit'#225'cora de errores'
          ParentShowHint = False
          ShowHint = True
        end
      end
      inherited MemErrores: TMemo
        Width = 740
        Height = 31
        Hint = 'Bit'#225'cora de errores'
        Color = clBtnFace
        ParentShowHint = False
        ScrollBars = ssBoth
        ShowHint = True
        TabOrder = 0
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 327
    Width = 748
    Height = 32
    inherited BtnProcesar: TBitBtn
      Left = 8
      Top = 3
      Width = 97
      Hint = 'Ejecutar movimiento seleccionado'
      Caption = '&Ejecutar'
      Kind = bkOK
    end
    inherited BtnDetener: TBitBtn
      Left = 144
      Top = 3
      Hint = 'Cancelar proceso'
      Caption = '&Cancelar proceso'
      Visible = False
    end
    inherited BtnSalir: TBitBtn
      Left = 642
      Top = 3
      Width = 97
      Hint = 'Salir de la aplicaci'#243'n'
    end
  end
  inherited PanelEncabezado: TPanel
    Top = 459
    Width = 748
    Height = 33
    Align = alBottom
    TabOrder = 4
    Visible = False
    inherited LblArchivo: TLabel
      Left = 7
      Top = 8
      Caption = '&Archivo Origen:'
    end
    inherited ArchivoSeek: TSpeedButton
      Left = 507
      Top = 3
      Hint = 'Buscar archivo de importaci'#243'n'
      ParentShowHint = False
      ShowHint = True
    end
    inherited ArchOrigen: TEdit
      Left = 82
      Top = 5
      Width = 421
      Hint = 'Especificar archivo de importaci'#243'n'
      ParentShowHint = False
      ShowHint = True
    end
  end
  object Panel3: TPanel [4]
    Left = 0
    Top = 270
    Width = 748
    Height = 57
    Align = alTop
    TabOrder = 1
    object rgTipoMovimiento: TRadioGroup
      Left = 1
      Top = 1
      Width = 746
      Height = 55
      Hint = 'Seleccionar el tipo de movimiento'
      Align = alClient
      Caption = ' Tipo de Movimiento '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Enviar informaci'#243'n a Terminal ZK'
        'Almacenar huellas en Sistema TRESS')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = rgTipoMovimientoClick
    end
  end
  object Panel2: TPanel [5]
    Left = 0
    Top = 0
    Width = 748
    Height = 270
    Align = alTop
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 746
      Height = 268
      ActivePage = Filtro
      Align = alClient
      TabOrder = 0
      object Terminales: TTabSheet
        Caption = 'Terminales'
        object GroupBox1: TGroupBox
          Left = 0
          Top = 0
          Width = 738
          Height = 240
          Hint = 'Seleccionar las terminales'
          Align = alClient
          Caption = 'Todas: '
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          object CheckBox1: TCheckBox
            Left = 8
            Top = 16
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 0
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox9: TCheckBox
            Left = 260
            Top = 16
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 8
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox17: TCheckBox
            Left = 512
            Top = 16
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 16
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox18: TCheckBox
            Left = 512
            Top = 31
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 17
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox19: TCheckBox
            Left = 512
            Top = 46
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 18
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox20: TCheckBox
            Left = 512
            Top = 61
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 19
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox21: TCheckBox
            Left = 512
            Top = 76
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 20
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox22: TCheckBox
            Left = 512
            Top = 91
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 21
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox23: TCheckBox
            Left = 512
            Top = 106
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 22
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox24: TCheckBox
            Left = 512
            Top = 121
            Width = 300
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 23
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox16: TCheckBox
            Left = 260
            Top = 121
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 15
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox15: TCheckBox
            Left = 260
            Top = 106
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 14
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox14: TCheckBox
            Left = 260
            Top = 91
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 13
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox13: TCheckBox
            Left = 260
            Top = 76
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 12
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox12: TCheckBox
            Left = 260
            Top = 61
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 11
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox11: TCheckBox
            Left = 260
            Top = 46
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 10
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox10: TCheckBox
            Left = 260
            Top = 31
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 9
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox2: TCheckBox
            Left = 8
            Top = 31
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 1
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox3: TCheckBox
            Left = 8
            Top = 46
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 2
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox4: TCheckBox
            Left = 8
            Top = 61
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 3
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox5: TCheckBox
            Left = 8
            Top = 76
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 4
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox6: TCheckBox
            Left = 8
            Top = 91
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 5
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox7: TCheckBox
            Left = 8
            Top = 106
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 6
            Visible = False
            OnClick = ActualizaCheckTodos
          end
          object CheckBox8: TCheckBox
            Left = 8
            Top = 121
            Width = 250
            Height = 17
            Caption = 'CheckBox1'
            TabOrder = 7
            Visible = False
            OnClick = ActualizaCheckTodos
          end
        end
        object cbxTodos: TCheckBox
          Left = 6
          Top = -1
          Width = 51
          Height = 17
          Hint = 'Seleccionar todas las terminales'
          Alignment = taLeftJustify
          Caption = 'Todas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = cbxTodosClick
        end
      end
      object Filtro: TTabSheet
        Caption = 'Filtro de Empleados'
        ImageIndex = 1
        object sCondicionLBl: TLabel
          Left = 2
          Top = 130
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Con&dici'#243'n:'
          FocusControl = ECondicion
        end
        object sFiltroLBL: TLabel
          Left = 27
          Top = 152
          Width = 25
          Height = 13
          Alignment = taRightJustify
          Caption = '&Filtro:'
          FocusControl = sFiltro
        end
        object GBRango: TGroupBox
          Left = 55
          Top = 2
          Width = 312
          Height = 118
          TabOrder = 0
          object lbInicial: TLabel
            Left = 24
            Top = 52
            Width = 30
            Height = 13
            Alignment = taRightJustify
            Caption = 'Inicial:'
            Enabled = False
          end
          object BInicial: TSpeedButton
            Left = 138
            Top = 46
            Width = 25
            Height = 25
            Hint = 'Invocar B'#250'squeda de Empleados'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BInicialClick
          end
          object lbFinal: TLabel
            Left = 171
            Top = 52
            Width = 25
            Height = 13
            Alignment = taRightJustify
            Caption = 'Final:'
            Enabled = False
          end
          object BFinal: TSpeedButton
            Left = 280
            Top = 46
            Width = 25
            Height = 25
            Hint = 'Invocar B'#250'squeda de Empleados'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BFinalClick
          end
          object BLista: TSpeedButton
            Left = 280
            Top = 85
            Width = 25
            Height = 25
            Hint = 'Invocar B'#250'squeda de Empleados'
            Enabled = False
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              0400000000000001000000000000000000001000000010000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33033333333333333F7F3333333333333000333333333333F777333333333333
              000333333333333F777333333333333000333333333333F77733333333333300
              033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
              33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
              3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
              33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
              333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
              333333773FF77333333333370007333333333333777333333333}
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            OnClick = BListaClick
          end
          object RBTodos: TRadioButton
            Left = 18
            Top = 13
            Width = 113
            Height = 17
            Caption = '&Todos'
            Checked = True
            TabOrder = 0
            TabStop = True
            OnClick = RBTodosClick
          end
          object RBRango: TRadioButton
            Left = 18
            Top = 29
            Width = 57
            Height = 17
            Caption = '&Rango'
            TabOrder = 1
            OnClick = RBRangoClick
          end
          object EInicial: TZetaEdit
            Left = 56
            Top = 47
            Width = 80
            Height = 21
            Enabled = False
            LookUpBtn = BInicial
            MaxLength = 9
            TabOrder = 2
          end
          object EFinal: TZetaEdit
            Left = 198
            Top = 48
            Width = 80
            Height = 21
            Enabled = False
            LookUpBtn = BFinal
            MaxLength = 9
            TabOrder = 3
          end
          object ELista: TZetaEdit
            Left = 37
            Top = 87
            Width = 239
            Height = 21
            Enabled = False
            LookUpBtn = BLista
            TabOrder = 4
          end
          object RBLista: TRadioButton
            Left = 18
            Top = 69
            Width = 73
            Height = 17
            Caption = '&Lista'
            TabOrder = 5
            OnClick = RBListaClick
          end
        end
        object ECondicion: TZetaKeyLookup
          Left = 55
          Top = 126
          Width = 338
          Height = 21
          TabOrder = 1
          TabStop = True
          WidthLlave = 82
        end
        object BAgregaCampo: TBitBtn
          Left = 370
          Top = 152
          Width = 25
          Height = 25
          Hint = 'Invocar al Constructor de F'#243'rmulas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BAgregaCampoClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
        end
        object sFiltro: TMemo
          Left = 55
          Top = 152
          Width = 312
          Height = 83
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 3
        end
      end
    end
  end
  inherited LogoffTimer: TTimer
    Left = 200
    Top = 344
  end
  inherited ActionList: TActionList
    Left = 170
    Top = 344
    object _ConfiguracionTerminales: TAction
      Caption = 'Lista de Terminales'
      ShortCut = 16460
      OnExecute = _ConfiguracionTerminalesExecute
    end
  end
  inherited ShellMenu: TMainMenu
    Left = 144
    Top = 344
    inherited Archivo1: TMenuItem
      inherited ArchivoOtraEmpresa: TMenuItem
        Hint = 'Accesar otra empresa'
      end
      inherited ArchivoServidor: TMenuItem
        Hint = 'Especificar nombre del servidor remoto'
        ShortCut = 16467
      end
      inherited CancelarProceso1: TMenuItem
        Hint = 'Procesar archivo'
        Visible = False
      end
      inherited CancelarProceso2: TMenuItem
        Visible = False
      end
      inherited N1: TMenuItem
        Visible = False
      end
      inherited ArchivoSalir: TMenuItem
        Hint = 'Salir de la aplicaci'#243'n'
      end
    end
    object Configuracin: TMenuItem [1]
      Caption = 'Configuraci'#243'n'
      object ListadeTerminales1: TMenuItem
        Action = _ConfiguracionTerminales
      end
    end
  end
  inherited OpenDialog: TOpenDialog
    DefaultExt = 'csv'
    Filter = 
      'Archivos de delimitados (*.csv)|*.csv|Archivos de textos (*.txt)' +
      '|*.txt|Todos (*.*)|*.*'
    Left = 230
    Top = 345
  end
  inherited ArbolImages: TImageList
    Left = 116
    Top = 344
  end
  inherited Email: TNMSMTP
    Left = 88
    Top = 344
  end
  object XPManifest1: TXPManifest
    Left = 489
    Top = 58
  end
end
