object ConfTerminales: TConfTerminales
  Left = 228
  Top = 213
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Configuraci'#243'n de Terminales'
  ClientHeight = 362
  ClientWidth = 436
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = -8
    Top = 0
    Width = 248
    Height = 326
    TabOrder = 2
    object Label3: TLabel
      Left = 130
      Top = 280
      Width = 114
      Height = 13
      Caption = '* M'#225'ximo 24 terminales'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
    end
    object lblTerminales: TLabel
      Left = 12
      Top = 280
      Width = 63
      Height = 13
      Caption = 'Terminales: 0'
    end
    object btnBorrar: TBitBtn
      Left = 92
      Top = 296
      Width = 77
      Height = 27
      Hint = 'Borrar terminal'
      Caption = '&Borrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = btnBorrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
        305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
        B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
        B0557777FF577777F7F500000E055550805577777F7555575755500000555555
        05555777775555557F5555000555555505555577755555557555}
      NumGlyphs = 2
    end
    object btnModificar: TBitBtn
      Left = 173
      Top = 296
      Width = 71
      Height = 27
      Hint = 'Modificar terminal'
      Caption = '&Modificar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnModificarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
    object btnNuevo: TBitBtn
      Left = 11
      Top = 296
      Width = 77
      Height = 27
      Hint = 'Nueva terminal'
      Caption = '&Agregar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = btnNuevoClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B3BB3888BB8888BB333B0000000000B333330FFFFFFFF08333330FFFFFFFF
        08333330FFFFFFFF08333330FFFFFFFF0833BBB0FFFFFFFF0BB33BB0FFFFFFFF
        0BBB3330FFFF000003333330FFFF0FF033333330FFFF0F0B33333330FFFF003B
        B33333B000000333BB333BB3333BB3333BB3B333333B3333333B}
    end
    object lbxTerminales: TListBox
      Left = 9
      Top = 1
      Width = 236
      Height = 276
      Hint = 'Lista de terminales'
      ItemHeight = 13
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
    end
  end
  object Panel3: TPanel
    Left = 240
    Top = 0
    Width = 194
    Height = 326
    TabOrder = 1
    object Label1: TLabel
      Left = 2
      Top = 33
      Width = 61
      Height = 13
      Caption = 'Direcci'#243'n IP:'
    end
    object Label2: TLabel
      Left = 4
      Top = 9
      Width = 59
      Height = 13
      Caption = 'Descripci'#243'n:'
    end
    object txtNombre: TEdit
      Left = 67
      Top = 5
      Width = 123
      Height = 21
      Hint = 'Nombre de la terminal'
      CharCase = ecUpperCase
      Enabled = False
      MaxLength = 10
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = txtNombreChange
      OnKeyPress = txtNombreKeyPress
    end
    object txtIP1: TZetaNumero
      Left = 67
      Top = 29
      Width = 30
      Height = 21
      Hint = 'Direcci'#243'n IP de la terminal'
      Enabled = False
      Mascara = mnDias
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '0'
      OnChange = txtIPChange
      OnKeyPress = txtIP1KeyPress
    end
    object txtIP2: TZetaNumero
      Left = 98
      Top = 29
      Width = 30
      Height = 21
      Hint = 'Direcci'#243'n IP de la terminal'
      Enabled = False
      Mascara = mnDias
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = '0'
      OnChange = txtIPChange
      OnKeyPress = txtIP2KeyPress
    end
    object txtIP3: TZetaNumero
      Left = 129
      Top = 29
      Width = 30
      Height = 21
      Hint = 'Direcci'#243'n IP de la terminal'
      Enabled = False
      Mascara = mnDias
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Text = '0'
      OnChange = txtIPChange
      OnKeyPress = txtIP3KeyPress
    end
    object txtIP4: TZetaNumero
      Left = 160
      Top = 29
      Width = 30
      Height = 21
      Hint = 'Direcci'#243'n IP de la terminal'
      Enabled = False
      Mascara = mnDias
      MaxLength = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      Text = '0'
      OnChange = txtIPChange
      OnKeyPress = txtIP4KeyPress
    end
    object btnEnviar: TBitBtn
      Left = 101
      Top = 54
      Width = 89
      Height = 25
      Hint = 'Enviar datos a lista'
      Caption = '&Enviar a lista'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = btnEnviarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 326
    Width = 434
    Height = 35
    TabOrder = 0
    object btnGuardar: TBitBtn
      Left = 276
      Top = 5
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      Enabled = False
      TabOrder = 0
      OnClick = btnGuardarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object btnSalir: TBitBtn
      Left = 355
      Top = 5
      Width = 75
      Height = 25
      TabOrder = 1
      OnClick = btnSalirClick
      Kind = bkClose
    end
  end
end
