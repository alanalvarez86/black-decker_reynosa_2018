unit FPoll;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, FileCtrl, Buttons, IniFiles, ComCtrls, ExtCtrls, ComObj, Mask,
     ZetaAsciiFile, FAsciiServer,
     ZetaNumero, OleCtrls, zkemkeeper_TLB;

type
  TTArchivosIni = class( TObject )
  private
    { Private declarations }
    FiniFile: TiniFile;
    function GetArchivoDestino: String;
    function GetArchivoFuente: String;
    function GetPrograma: String;
    function GetArchivoEncript: String;
    function GetParametros: String;
    function GetCredencialDEF: String;
    function GetCredencialFIN: Integer;
    function GetCredencialINI: Integer;
    function GetDiaFIN: Integer;
    function GetDiaINI: Integer;
    function GetEmpleadoFIN: Integer;
    function GetEmpleadoINI: Integer;
    function GetEmpresaDEF: String;
    function GetEmpresaFIN: Integer;
    function GetEmpresaINI: Integer;
    function GetHoraFIN: Integer;
    function GetHoraINI: Integer;
    function GetMesFIN: Integer;
    function GetMesINI: Integer;
    function GetMinutosFIN: Integer;
    function GetMinutosINI: Integer;
    function GetRelojDEF: String;
    function GetRelojFIN: Integer;
    function GetRelojINI: Integer;
    function GetTipoDEF: String;
    function GetTipoFIN: Integer;
    function GetTipoINI: Integer;
    procedure SetArchivoDestino( const Value: String );
    procedure SetArchivoFuente( const Value: String );
    procedure SetPrograma(const Value: String);
    procedure SetArchivoEncript(const Value: String);
    procedure SetParametros(const Value: String);
    procedure SetCredencialDEF(const Value: String);
    procedure SetCredencialFIN(const Value: Integer);
    procedure SetCredencialINI(const Value: Integer);
    procedure SetDiaFIN(const Value: Integer);
    procedure SetDiaINI(const Value: Integer);
    procedure SetEmpleadoFIN(const Value: Integer);
    procedure SetEmpleadoINI(const Value: Integer);
    procedure SetEmpresaDEF(const Value: String);
    procedure SetEmpresaFIN(const Value: Integer);
    procedure SetEmpresaINI(const Value: Integer);
    procedure SetHoraFIN(const Value: Integer);
    procedure SetHoraINI(const Value: Integer);
    procedure SetMesFIN(const Value: Integer);
    procedure SetMesINI(const Value: Integer);
    procedure SetMinutosFIN(const Value: Integer);
    procedure SetMinutosINI(const Value: Integer);
    procedure SetRelojDEF(const Value: String);
    procedure SetRelojFIN(const Value: Integer);
    procedure SetRelojINI(const Value: Integer);
    procedure SetTipoDEF(const Value: String);
    procedure SetTipoFIN(const Value: Integer);
    procedure SetTipoINI(const Value: Integer);
    function GetLongitudMinima: Integer;
    procedure SetLongitudMinima(const Value: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    { Archivos }
    property ArchivoFuente: String read GetArchivoFuente write SetArchivoFuente;
    property ArchivoDestino: String read GetArchivoDestino write SetArchivoDestino;
    property Parametros: String read GetParametros write SetParametros;
    property Programa: String read GetPrograma write SetPrograma;
    property ArchivoEncript: String read GetArchivoEncript write SetArchivoEncript;
    { Checadas }
    property EmpleadoINI: Integer read GetEmpleadoINI write SetEmpleadoINI;
    property EmpleadoFIN: Integer read GetEmpleadoFIN write SetEmpleadoFIN;
    property DiaINI: Integer read GetDiaINI write SetDiaINI;
    property DiaFIN: Integer read GetDiaFIN write SetDiaFIN;
    property MesINI: Integer read GetMesINI write SetMesINI;
    property MesFIN: Integer read GetMesFIN write SetMesFIN;
    property HoraINI: Integer read GetHoraINI write SetHoraINI;
    property HoraFIN: Integer read GetHoraFIN write SetHoraFIN;
    property MinutosINI: Integer read GetMinutosINI write SetMinutosINI;
    property MinutosFIN: Integer read GetMinutosFIN write SetMinutosFIN;
    property RelojINI: Integer read GetRelojINI write SetRelojINI;
    property RelojFIN: Integer read GetRelojFIN write SetRelojFIN;
    property RelojDEF: String read GetRelojDEF write SetRelojDEF;
    property TipoINI: Integer read GetTipoINI write SetTipoINI;
    property TipoFIN: Integer read GetTipoFIN write SetTipoFIN;
    property TipoDEF: String read GetTipoDEF write SetTipoDEF;
    property EmpresaINI: Integer read GetEmpresaINI write SetEmpresaINI;
    property EmpresaFIN: Integer read GetEmpresaFIN write SetEmpresaFIN;
    property EmpresaDEF: String read GetEmpresaDEF write SetEmpresaDEF;
    property CredencialINI: Integer read GetCredencialINI write SetCredencialINI;
    property CredencialFIN: Integer read GetCredencialFIN write SetCredencialFIN;
    property CredencialDEF: String read GetCredencialDEF write SetCredencialDEF;
    property LongitudMinima: Integer read GetLongitudMinima write SetLongitudMinima;
  end;
  TTArchivos = class( TForm )
    PanelInferior: TPanel;
    Guardar: TBitBtn;
    Procesar: TBitBtn;
    PageControl: TPageControl;
    Archivos: TTabSheet;
    Checadas: TTabSheet;
    ArchivoDestinoLBL: TLabel;
    ArchivoDestinoBTN: TSpeedButton;
    ProgramaLBL: TLabel;
    ProgramaBTN: TSpeedButton;
    ParametrosLBL: TLabel;
    ArchivoDestino: TEdit;
    Programa: TEdit;
    Parametros: TEdit;
    Ayuda: TSpeedButton;
    Salir: TBitBtn;
    RelojLBL: TLabel;
    RelojDEF: TEdit;
    TipoLBL: TLabel;
    TipoDEF: TEdit;
    EmpresaLBL: TLabel;
    EmpresaDEF: TEdit;
    CredencialDEF: TEdit;
    CredencialLBL: TLabel;
    OpenDialog: TOpenDialog;
    Estatus: TTabSheet;
    Memo1: TMemo;
    CZKEM1: TCZKEM;
    BitBtn1: TBitBtn;
    ArchivoEncritadoLBL: TLabel;
    ArchivoEncriptadoBTN: TSpeedButton;
    ArchivoEncriptado: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ArchivoDestinoBTNClick(Sender: TObject);
    procedure ArchivoDestinoChange(Sender: TObject);
    procedure GuardarClick( Sender: TObject );
    procedure ProgramaBTNClick(Sender: TObject);
    procedure AyudaClick(Sender: TObject);
    procedure ProcesarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ArchivoEncriptadoBTNClick(Sender: TObject);
  private
    { Private declarations }
    FSilent: Boolean;
    FShowing: Boolean;
    FAscii: TAsciiLog;
    FIniValues: TTArchivosIni;
    FEsDEMO : boolean;
    FlstRelojsAut: TStringList;
    FNumeroSentinel:Integer;
    FsLecturaReloj: TAsciiServer;

    function BuscaArchivo(const sArchivo: String): String;
    function Ejecutar(const sProgram, sParameters: String ): Boolean;
    function GetArchivoDestino: String;
    function GetArchivoFuente: String;
    function GetParametros: String;
    function GetPrograma: String;
    function Validar(const sSource, sDestino,sArchivoEncript,sPrograma, sParametros: String): Boolean;
    procedure LogEnd;
    procedure LogError( const sMensaje: String );
    procedure LogEvent(const sMensaje: String);
    procedure LogMensaje(const sMensaje: String);
    procedure LogStart;
    procedure LogTimeStamp( const sMensaje: String );
    procedure ProcesaArchivos( const sSource, sDestino,sEncriptado, sPrograma, sParametros: String );
    procedure ReadDeviceStatus;
    procedure AddInfo(const s: string);
    function RelojAutorizado( const sNumSerie:widestring;var Autorizado:Boolean ):Boolean;
    function GetArchivoEncriptado: String;
    procedure VerificarServerDEMO;
  protected
    { Protected declarations }
    property IniValues: TTArchivosIni read FIniValues;
    function ConvierteChecada( const sOriginal: String ): String; virtual;
    function Extrae( const sValue: String; const iEmpieza, iTermina: Integer ): String;
    function ExtraeDefault(const sValue, sDefault: String; const iEmpieza, iTermina: Integer): String;
    function TransformFiles( const sFuente, sDestino: String ): Boolean; virtual;
    function DownloadClockLogs( sArchivoDestino,sArchivoEncriptado : string ): Boolean;

  public
    { Public declarations }
    procedure RunSilent( const sParametro: String ); overload;
    procedure RunSilent( const sParametro, sFuente: String ); overload;

  end;

var
  TArchivos: TTArchivos;

implementation

uses FAyuda,
     ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaWinAPITools,
     ZetaServerTools,DPoll,FAutoClasses;

const
     K_ARCHIVOS = 'ARCHIVOS';
     K_DESTINO = 'Destino';
     K_FUENTE = 'Fuente';
     K_ENCRIPT = 'Encriptado';
     K_PARAMETROS = 'Parametros';
     K_PROGRAMA = 'Programa';
     K_CHECADAS = 'CHECADAS';
     K_EMPLEADO_INI = 'EmpleadoINI';
     K_EMPLEADO_FIN = 'EmpleadoFIN';
     K_DIA_INI = 'DiaINI';
     K_DIA_FIN = 'DiaFIN';
     K_MES_INI = 'MesINI';
     K_MES_FIN = 'MesFIN';
     K_HORA_INI = 'HoraINI';
     K_HORA_FIN = 'HoraFIN';
     K_MINUTOS_INI = 'MinutosINI';
     K_MINUTOS_FIN = 'MinutosFIN';
     K_RELOJ_INI = 'RelojINI';
     K_RELOJ_FIN = 'RelojFIN';
     K_RELOJ_DEF = 'RelojDEF';
     K_TIPO_INI = 'TipoINI';
     K_TIPO_FIN = 'TipoFIN';
     K_TIPO_DEF = 'TipoDEF';
     K_EMPRESA_INI = 'EmpresaINI';
     K_EMPRESA_FIN = 'EmpresaFIN';
     K_EMPRESA_DEF = 'EmpresaDEF';
     K_CREDENCIAL_INI = 'CredencialINI';
     K_CREDENCIAL_FIN = 'CredencialFIN';
     K_CREDENCIAL_DEF = 'CredencialDEF';
     K_LONGITUD_MIN = 'LongitudMinima';
     K_ARCHIVO_LISTA_RELOJES = 'IPList.dat';
     K_TOPE_DEMO = 25;
     K_MSG_N_AUT = 'Reloj %s (%s) N0 Autorizado';
     K_MSG_AUT = 'Reloj %s (%s) Autorizado al Sentinel: %s';
     K_MSG_DEMO = 'Modo DEMO , NO se recolectar�n checadas';
     K_MSG_N_AUT_SEN = 'Reloj %s (%s) N0 Autorizado al Sentinel: %s ';


{$R *.DFM}

function WinExecAndWait32( const Programa, Parametros: String; Visibility: Integer ): Integer;
var
   zAppName: array[ 0..512 ] of Char;
   zCurDir: array[ 0..255 ] of Char;
   WorkDir: String;
   StartupInfo: TStartupInfo;
   ProcessInfo: TProcessInformation;
   lpExitCode: DWORD;
begin
     if FileExists( Programa ) then
     begin
          if StrLleno( Parametros ) then
             StrPCopy( zAppName, Programa + ' ' + Parametros )
          else
              StrPCopy( zAppName, Programa );
          {
          ShowMessage( zAppName );
          }
          GetDir( 0, WorkDir );
          StrPCopy( zCurDir, WorkDir );
          FillChar( StartupInfo, Sizeof( StartupInfo ), #0 );
          with StartupInfo do
          begin
               cb := Sizeof( StartupInfo );
               dwFlags := STARTF_USESHOWWINDOW + STARTF_USEFILLATTRIBUTE;
               wShowWindow := Visibility;
               dwFillAttribute := FOREGROUND_BLUE + BACKGROUND_RED + BACKGROUND_GREEN + BACKGROUND_BLUE;
          end;
          if not CreateProcess( nil,
                                zAppName, { pointer to command line string }
                                nil,      { pointer to process security attributes }
                                nil,      { pointer to thread security attributes }
                                False,    { handle inheritance flag }
                                CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,          { creation flags }
                                nil,               { pointer to new environment block }
                                nil,               { pointer to current directory name }
                                StartupInfo,       { pointer to STARTUPINFO }
                                ProcessInfo ) then
          begin
               Result := -1; { pointer to PROCESS_INF / FRACASO}
          end
          else
          begin
               with ProcessInfo do
               begin
                    WaitForSingleObject( hProcess, INFINITE );
                    GetExitCodeProcess( hProcess, lpExitCode );
                    Result := lpExitCode; {SE EJECUTO CON EXITO}
               end;
          end;
          Application.ProcessMessages;
     end
     else
         Result := -2;//NO EXISTE EL ARCHIVO
end;

{********** TTArchivosIni ********** }

constructor TTArchivosIni.Create;
begin
     FIniFile := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ) );
end;

destructor TTArchivosIni.Destroy;
begin
     FreeAndNil( FIniFile );
     inherited;
end;

function TTArchivosIni.GetArchivoDestino: String;
begin
     Result := FiniFile.ReadString( K_ARCHIVOS, K_DESTINO, Format( '%sRELOJ.DAT', [ ExtractFilePath( Application.ExeName ) ] ) );
end;

function TTArchivosIni.GetArchivoEncript: String;
begin
     Result := FiniFile.ReadString( K_ARCHIVOS, K_ENCRIPT, Format( '%sTerminales.dat', [ ExtractFilePath( Application.ExeName ) ] ) );
end;

function TTArchivosIni.GetArchivoFuente: String;
begin
     Result := FIniFile.ReadString( K_ARCHIVOS, K_FUENTE, '' );
end;

function TTArchivosIni.GetCredencialDEF: String;
const
     K_CREDENCIAL_DEFAULT = 'A';
begin
     Result := FIniFile.ReadString( K_CHECADAS, K_CREDENCIAL_DEF, K_CREDENCIAL_DEFAULT );
     if ZetaCommonTools.StrVacio( Result ) then
        Result := K_CREDENCIAL_DEFAULT;
end;

function TTArchivosIni.GetCredencialFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_CREDENCIAL_FIN, 0 );
end;

function TTArchivosIni.GetCredencialINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_CREDENCIAL_INI, 0 );
end;

function TTArchivosIni.GetDiaFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_DIA_FIN, 0 );
end;

function TTArchivosIni.GetDiaINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_DIA_INI, 0 );
end;

function TTArchivosIni.GetEmpleadoFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_EMPLEADO_FIN, 0 );
end;

function TTArchivosIni.GetEmpleadoINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_EMPLEADO_INI, 0 );
end;

function TTArchivosIni.GetEmpresaDEF: String;
const
     K_EMPRESA_DEFAULT = '0';
begin
     Result := FIniFile.ReadString( K_CHECADAS, K_EMPRESA_DEF, K_EMPRESA_DEFAULT );
     if ZetaCommonTools.StrVacio( Result ) then
        Result := K_EMPRESA_DEFAULT;
end;

function TTArchivosIni.GetEmpresaFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_EMPRESA_FIN, 0 );
end;

function TTArchivosIni.GetEmpresaINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_EMPRESA_INI, 0 );
end;

function TTArchivosIni.GetHoraFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_HORA_FIN, 0 );
end;

function TTArchivosIni.GetHoraINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_HORA_INI, 0 );
end;

function TTArchivosIni.GetLongitudMinima: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_LONGITUD_MIN, 0 );
end;

function TTArchivosIni.GetMesFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_MES_FIN, 0 );
end;

function TTArchivosIni.GetMesINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_MES_INI, 0 );
end;

function TTArchivosIni.GetMinutosFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_MINUTOS_FIN, 0 );
end;

function TTArchivosIni.GetMinutosINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_MINUTOS_INI, 0 );
end;

function TTArchivosIni.GetParametros: String;
begin
     Result := FIniFile.ReadString( K_ARCHIVOS, K_PARAMETROS, Format( '/I "%s"', [ GetArchivoDestino ] ) );
end;

function TTArchivosIni.GetPrograma: String;
begin
     Result := FIniFile.ReadString( K_ARCHIVOS, K_PROGRAMA, 'C:\Program Files\Grupo Tress\L5Poll\L5Poll.exe' );
end;

function TTArchivosIni.GetRelojDEF: String;
const
     K_RELOJ_DEFAULT = '0000';
begin
     Result := FIniFile.ReadString( K_CHECADAS, K_RELOJ_DEF, K_RELOJ_DEFAULT );
     if ZetaCommonTools.StrVacio( Result ) then
        Result := K_RELOJ_DEFAULT;
end;

function TTArchivosIni.GetRelojFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_RELOJ_FIN, 0 );
end;

function TTArchivosIni.GetRelojINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_RELOJ_INI, 0 );
end;

function TTArchivosIni.GetTipoDEF: String;
const
     K_TIPO_DEFAULT = '1';
begin
     Result := FIniFile.ReadString( K_CHECADAS, K_TIPO_DEF, K_TIPO_DEFAULT );
     if ZetaCommonTools.StrVacio( Result ) then
        Result := K_TIPO_DEFAULT;
end;

function TTArchivosIni.GetTipoFIN: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_TIPO_FIN, 0 );
end;

function TTArchivosIni.GetTipoINI: Integer;
begin
     Result := FIniFile.ReadInteger( K_CHECADAS, K_TIPO_INI, 0 );
end;

procedure TTArchivosIni.SetArchivoDestino(const Value: String);
begin
     FIniFile.WriteString( K_ARCHIVOS, K_DESTINO, Value );
end;

procedure TTArchivosIni.SetArchivoEncript(const Value: String);
begin
     FIniFile.WriteString( K_ARCHIVOS, K_ENCRIPT, Value );
end;

procedure TTArchivosIni.SetArchivoFuente(const Value: String);
begin
     FIniFile.WriteString( K_ARCHIVOS, K_FUENTE, Value );
end;

procedure TTArchivosIni.SetCredencialDEF(const Value: String);
begin
     FIniFile.WriteString( K_CHECADAS, K_CREDENCIAL_DEF, Value );
end;

procedure TTArchivosIni.SetCredencialFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_CREDENCIAL_FIN, Value );
end;

procedure TTArchivosIni.SetCredencialINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_CREDENCIAL_INI, Value );
end;

procedure TTArchivosIni.SetDiaFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_DIA_FIN, Value );
end;

procedure TTArchivosIni.SetDiaINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_DIA_INI, Value );
end;

procedure TTArchivosIni.SetEmpleadoFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_EMPLEADO_FIN, Value );
end;

procedure TTArchivosIni.SetEmpleadoINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_EMPLEADO_INI, Value );
end;

procedure TTArchivosIni.SetEmpresaDEF(const Value: String);
begin
     FIniFile.WriteString( K_CHECADAS, K_EMPRESA_DEF, Value );
end;

procedure TTArchivosIni.SetEmpresaFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_EMPRESA_FIN, Value );
end;

procedure TTArchivosIni.SetEmpresaINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_EMPRESA_INI, Value );
end;

procedure TTArchivosIni.SetHoraFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_HORA_FIN, Value );
end;

procedure TTArchivosIni.SetHoraINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_HORA_INI, Value );
end;

procedure TTArchivosIni.SetLongitudMinima(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_LONGITUD_MIN, Value );
end;

procedure TTArchivosIni.SetMesFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_MES_FIN, Value );
end;

procedure TTArchivosIni.SetMesINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_MES_INI, Value );
end;

procedure TTArchivosIni.SetMinutosFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_MINUTOS_FIN, Value );
end;

procedure TTArchivosIni.SetMinutosINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_MINUTOS_INI, Value );
end;

procedure TTArchivosIni.SetParametros(const Value: String);
begin
     FIniFile.WriteString( K_ARCHIVOS, K_PARAMETROS, Value );
end;

procedure TTArchivosIni.SetPrograma(const Value: String);
begin
     FIniFile.WriteString( K_ARCHIVOS, K_PROGRAMA, Value );
end;

procedure TTArchivosIni.SetRelojDEF(const Value: String);
begin
     FIniFile.WriteString( K_CHECADAS, K_RELOJ_DEF, Value );
end;

procedure TTArchivosIni.SetRelojFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_RELOJ_FIN, Value );
end;

procedure TTArchivosIni.SetRelojINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_RELOJ_INI, Value );
end;

procedure TTArchivosIni.SetTipoDEF(const Value: String);
begin
     FIniFile.WriteString( K_CHECADAS, K_TIPO_DEF, Value );
end;

procedure TTArchivosIni.SetTipoFIN(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_TIPO_FIN, Value );
end;

procedure TTArchivosIni.SetTipoINI(const Value: Integer);
begin
     FIniFile.WriteInteger( K_CHECADAS, K_TIPO_INI, Value );
end;

{ ********  TTArchivos ********* }

procedure TTArchivos.FormCreate(Sender: TObject);
begin
     FSilent := False;
     FIniValues := TTArchivosIni.Create;
     FAscii := TAsciiLog.Create;
     FShowing := False;
     FlstRelojsAut := TStringList.Create;
     FNumeroSentinel := 0;
     dmPoll := TdmPoll.Create( Self );
     VerificarServerDEMO;
end;

procedure TTArchivos.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FAscii );
     FreeAndNil( FIniValues );
     FreeAndNil( FlstRelojsAut);
     FAutoClasses.ClearAuto;
     FreeAndNil( dmPoll );
end;



procedure TTArchivos.VerificarServerDEMO;
begin
     FAutoClasses.InitAuto;
     FEsDEMO := False;
     with Autorizacion do
     begin
          try
             Cargar( dmPoll.GetAuto );
          except
                on Error: Exception do
                begin
                     if ( ParamCount > 0  ) then
                     begin
                          AddInfo( 'Error al conectar servidor' );
                          AddInfo( Format('Error : %s' ,[Error.Message] ) );
                          LogEvent('Error al conectar servidor');
                          LogEvent(Format('Error : %s' ,[Error.Message] ) );
                     end
                     else
                         Application.HandleException( Error );
                end;
          end;
          FNumeroSentinel := NumeroSerie;
          FEsDemo := EsDemo or not OkModulo( okL5Poll, True );
     end;
end;



procedure TTArchivos.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := Estatus;
     FShowing := True;
     try
        with FIniValues do
        begin
             { Archivoss }
             Self.ArchivoDestino.Text := ArchivoDestino;
             Self.Programa.Text := Programa;
             Self.Parametros.Text := Parametros;
             Self.ArchivoEncriptado.Text := ArchivoEncript;  
             { Checadas }
             Self.RelojDEF.Text := RelojDEF;
             Self.TipoDEF.Text := TipoDEF;
             Self.EmpresaDEF.Text := EmpresaDEF;
             Self.CredencialDEF.Text := CredencialDEF;
             PageControl.ActivePage := Archivos; 
        end;
     finally
            FShowing := False;
     end;
end;




{ --- Funci�n que busca los archivos dat en el directorio indicado cono Fuente --- }

function TTArchivos.Extrae( const sValue: String; const iEmpieza, iTermina: Integer ): String;
begin
     Result := Copy( sValue, iEmpieza, ( iTermina - iEmpieza + 1 ) );
end;

function TTArchivos.ExtraeDefault( const sValue, sDefault: String; const iEmpieza, iTermina: Integer ): String;
begin
     if ( iEmpieza = 0 ) and ( iTermina = 0 ) then
        Result := sDefault
     else
         Result := Extrae( sValue, iEmpieza, iTermina );
end;

function TTArchivos.ConvierteChecada( const sOriginal: String ): String;
const
     K_EMPRESA = '0';
     K_CREDENCIAL = 'A';
     K_RELLENO = '00000*'; // Se agrega el asterisco para identificar el tipo de checada como generada por ZKPoll
var
   sRegistro, sReloj, sEmpleado, sMes: String;
   sDia, sHora, sMinutos, sTipo: String;
   aRegistro : TStringList;
   iTab : Integer;
begin
     sRegistro := sOriginal;
     Result := VACIO;
     with IniValues do
     begin
          aRegistro := TStringList.Create;
          try
             aRegistro.Clear;
             while pos ( Chr(9), sRegistro ) > 0 do
             begin
                if( pos ( Chr(9), sRegistro ) > 1) then
                    aRegistro.Add ( Trim( Copy ( sRegistro, 1, pos ( Chr (9), sRegistro ) -1 ) ) );
                Delete ( sRegistro, 1, pos ( Chr (9), sRegistro ) );
             end;

             if ( aRegistro.Count > 0 ) then
             begin
                 if Length ( sRegistro ) > 0 then
                    aRegistro.Add( sRegistro );
                 if( Length( aRegistro.Strings[0] ) > 9 ) then
                     aRegistro.Strings[0] := Copy( aRegistro.Strings[0], 0, 9 );
                 if( trystrtoint( aRegistro[0], iTab ) ) then
                 begin
                      sEmpleado := ZetaCommonTools.PadLCar( aRegistro.Strings[0], 9, '0' );
                      if( Length( aRegistro.Strings[1] ) > 19 ) then
                          aRegistro.Strings[1] := Copy( aRegistro.Strings[0], 0, 19 );
                      sMes := Copy( aRegistro.Strings[1], 6, 2 );
                      sDia := Copy( aRegistro.Strings[1], 9, 2 );
                      sHora := Copy( aRegistro.Strings[1], 12, 2 );
                      sMinutos := Copy( aRegistro.Strings[1], 15, 2 );
                      if ( aRegistro.Count > 2) then
                      begin
                           if( Length( aRegistro.Strings[2] ) > 1 ) then
                               aRegistro.Strings[2] := Copy( aRegistro.Strings[0], 0, 1 );
                           sTipo := aRegistro.Strings[2];
                      end
                      else
                          sTipo := TipoDEF;

                      if( aRegistro.Count > 3) then
                      begin
                           if( Length( aRegistro.Strings[3] ) > 4 ) then
                               aRegistro.Strings[3] := Copy( aRegistro.Strings[0], 0, 4 );
                               sReloj := ZetaCommontools.PadLCar( aRegistro.Strings[3], 4, '0' );
                           end
                           else
                               sReloj := ZetaCommonTools.PadLCar( RelojDEF, 4, '0' );
                      Result := sReloj + K_RELLENO + TOKEN_ASISTENCIA + EmpresaDEF + sEmpleado + CredencialDEF + sMes + sDia + sHora + sMinutos + sTipo;
                 end;
             end;
          finally
                 FreeAndNil(aRegistro);
          end;
     end;

end;

function TTArchivos.TransformFiles( const sFuente, sDestino: String ): Boolean;
var
   i : Integer;
   FFuente, FDestino : TStrings;
   sLinea : String;
begin
     Result := True;
     FFuente := TStringList.Create;
     try
        FDestino := TStringList.Create;
        try
           try
              with FFuente do
              begin
                   LoadFromFile( sFuente );
                   for i := 0 to ( Count - 1 ) do
                   begin
                        sLinea := ConvierteChecada( Trim( Strings[ i ] ) );
                        if( sLinea = '') then
                            LogEvent( Format( 'Se omiti� la l�nea '+ inttostr(i+1) + ' con el valor ['+ Strings[ i ] +']', [ sDestino ] ) )
                        else
                            FDestino.Add( sLinea );

                   end;
              end;
              FDestino.SaveToFile( sDestino );
           except
                 on Error: Exception do
                 begin
                      LogError( Error.Message );
                      Result := False;
                 end;
           end;
        finally
               FreeAndNil( FDestino );
        end;
     finally
            FreeAndNil( FFuente );
     end;
end;

function TTArchivos.DownloadClockLogs( sArchivoDestino,sArchivoEncriptado : string ): Boolean;
const
     K_BUFFER_RELOJ = 'Mient.mie';
var
   lstIPList, lstArchivoDestino : TStringList;
   sIDReloj, sIPReloj, sRegistroReloj : string;
   wsEnrollNumber,sNumSerie: widestring;
   i,dwVerifyMode: Integer;
   dwInOutMode,dwYear,dwMonth,dwDay,dwHour,dwMinute, dwSecond, dwWorkCode, ErrorCode: Integer;
   iAcumuladasDemo : integer;
   lAutorizado:Boolean;
begin
     Result := False;
     iAcumuladasDemo := 0;

     lstIPList := TStringList.Create;
     lstArchivoDestino := TStringList.Create;
     try
        if FileExists( ExtractFilePath( Application.ExeName ) + K_ARCHIVO_LISTA_RELOJES ) then
        begin
             lstIPList.LoadFromFile( ExtractFilePath( Application.ExeName ) + K_ARCHIVO_LISTA_RELOJES );
             Memo1.Lines.Clear;
             PageControl.ActivePage := Estatus;
             //Recorrer lista de relojes
             for i := 0 to ( lstIPList.Count - 1 ) do
             begin
                  //Obtener ID e IP del Reloj
                  sIPReloj := lstIPList.Strings[ i ];
                  sIDReloj := Trim( copy( sIPReloj, 1, pos( '=', sIPReloj ) - 1 ) );

                  Delete( sIPReloj, 1, pos( '=', sIPReloj ) );
                  sIPReloj := Trim( sIPReloj );

                  //Conectarse al reloj
                  AddInfo( Format( 'Intentando conexi�n con reloj %s (%s)...', [ sIDReloj, sIPReloj ] ) );
                  LogEvent( Format( 'Intentando conexi�n con reloj %s (%s)...', [ sIDReloj, sIPReloj ] ) );

                  if ( CZKEM1.Connect_net( sIPReloj, 4370 ) ) then
                  begin
                       //Reloj conectado con exito
                       AddInfo( Format( 'Reloj %s (%s) conectado con �xito.', [ sIDReloj, sIPReloj ] ) );
                       LogEvent( Format( 'Reloj %s (%s) conectado con �xito.', [ sIDReloj, sIPReloj ] ) );
                       //Validar con archivo encriptado
                       CZKEM1.GetSerialNumber(1,sNumSerie);
                       FlstRelojsAut.LoadFromFile(sArchivoEncriptado);
                       if RelojAutorizado( sNumSerie,lAutorizado )then
                       begin
                            if lAutorizado then
                            begin
                                 AddInfo( Format( K_MSG_AUT , [ sIDReloj, sIPReloj,IntToStr( FNumeroSentinel ) ] ) );
                                 LogEvent( Format( K_MSG_AUT, [ sIDReloj, sIPReloj,IntToStr( FNumeroSentinel ) ] ) );
                            end
                            else
                            begin
                                 AddInfo( Format( K_MSG_N_AUT_SEN , [ sIDReloj, sIPReloj,IntToStr( FNumeroSentinel ) ] ) );
                                 LogEvent( Format( K_MSG_N_AUT_SEN , [ sIDReloj, sIPReloj,IntToStr( FNumeroSentinel ) ] ) );
                            end
                       end
                       else
                       begin
                            AddInfo( Format( K_MSG_N_AUT , [ sIDReloj, sIPReloj ] ) );
                            LogEvent( Format( K_MSG_N_AUT , [ sIDReloj, sIPReloj ] ) );
                       end;
                       //Si no esta autorizado no procesa ninguna checada
                       if ( lAutorizado )and ( not FEsDemo ) then
                       begin
                            //Bajar los logs
                            FsLecturaReloj :=  TAsciiServer.Create;
                            FsLecturaReloj.Init;
                            try
                               if CZKEM1.ReadGeneralLogData( 1 ) then
                               begin                                      //GetGeneralLogData(  dwTMachine, dwEMachineNumber,
                                    while ( CZKEM1.SSR_GetGeneralLogData( 1, wsEnrollNumber, dwVerifyMode,
                                                                      dwInOutMode,dwYear,dwMonth,dwDay,dwHour,dwMinute, dwSecond, dwWorkCode ) ) and
                                          ( iAcumuladasDemo <= K_TOPE_DEMO ) do
                                    begin
                                         sRegistroReloj := format( '%s00000*@%s%s%s%s%s%s%s',
                                                                   [ ZetaCommonTools.PadLCar( sIDReloj, 4, '0' ),
                                                                     //FIniValues.EmpresaDEF,
                                                                     Copy( wsEnrollNumber, 1, 1 ) + ZetaCommonTools.PadLCar( Copy( wsEnrollNumber, 2, Length( wsEnrollNumber ) ), 9, '0' ),
                                                                     //ZetaCommonTools.PadLCar( wsEnrollNumber, 9, '0' ),
                                                                     //ZetaCommonTools.PadLCar( IntToStr( dwEnrollNumber ), 9, '0' ),
                                                                     FIniValues.CredencialDEF,
                                                                     ZetaCommonTools.PadLCar( IntToStr( dwMonth ), 2, '0' ),
                                                                     ZetaCommonTools.PadLCar( IntToStr( dwDay ), 2, '0' ),
                                                                     ZetaCommonTools.PadLCar( IntToStr( dwHour ), 2, '0' ),
                                                                     ZetaCommonTools.PadLCar( IntToStr( dwMinute ), 2, '0' ),
                                                                     FIniValues.TipoDEF ] );
                                         //Almacenar registro
                                         AddInfo( sRegistroReloj );
                                         FsLecturaReloj.Write( Trim( sRegistroReloj ) );
                                         lstArchivoDestino.Add( Trim(sRegistroReloj) );

                                         //Incrementar lecturas si es demo
                                         if FEsDEMO then
                                         begin
                                              Inc( iAcumuladasDemo );
                                         end;//if
                                    end;//while
                                    CZKEM1.ClearGLog(1);
                               end//if CZKEM1.ReadGeneralLogData( 1 ) then
                               else
                               begin
                                    AddInfo( Format( 'No existen registros de asistencia en reloj %s (%s)', [ sIDReloj, sIPReloj ] ) );
                                    LogEvent( Format( 'No existen registros de asistencia en reloj %s (%s)', [ sIDReloj, sIPReloj ] ) );
                                    AddInfo( VACIO );
                               end;
                            finally
                                   FsLecturaReloj.CloseAll;
                                   FreeAndNil( FsLecturaReloj );
                                   //Guardar Reloj.dat
                                   if ( lstArchivoDestino.Text <> '' ) then
                                   begin
                                        lstArchivoDestino.SaveToFile( sArchivoDestino );
                                        LogEvent( Format( 'El archivo "%s" fue almacenado con %d registro(s).', [ sArchivoDestino, lstArchivoDestino.Count ] ) );
                                        AddInfo( Format( 'El archivo "%s" fue almacenado con %d registro(s).', [ sArchivoDestino, lstArchivoDestino.Count ] ) );
                                        AddInfo( VACIO );
                                        Result := True;
                                   end;
                            end;//try
                       end;
                  end//if conectado
                  else
                  begin
                       //No se pudo conectar reloj
                       CZKEM1.GetLastError(ErrorCode);
                       LogEvent( Format( 'ERROR: El reloj %s (%s) no pudo ser conectado (Error #%d).', [ sIDReloj, sIPReloj, ErrorCode ] ) );
                       AddInfo( Format( 'ERROR: El reloj %s (%s) no pudo ser conectado (Error #%d).', [ sIDReloj, sIPReloj, ErrorCode ] ) );
                       AddInfo( VACIO );
                  end;// else - no conectado

             end;//for relojes
        end
        else
        begin
             LogEvent( Format( 'Archivo %s no existe, no se puede realizar la recolecci�n', [ ExtractFilePath( Application.ExeName ) + K_ARCHIVO_LISTA_RELOJES ] ) );
             AddInfo( Format( 'Archivo %s no existe, no se puede realizar la recolecci�n', [ ExtractFilePath( Application.ExeName ) + K_ARCHIVO_LISTA_RELOJES ] ) );
        end;
     finally
            lstIPList.Free;
            lstArchivoDestino.Free;
     end;
end;

{ ---------------- Metodo que llama a Ejecutar Programa ------------------ }

function TTArchivos.Ejecutar( const sProgram, sParameters: String ): Boolean;
const
     aShow: array[ False..True ] of Word = ( SW_HIDE, SW_SHOWDEFAULT );
var
   iResult: Integer;
begin
     Result := False;
     iResult := WinExecAndWait32( sProgram, sParameters, aShow[ not FSilent ] );
     case iResult of
          0: Result := True;
          -1: LogError( 'El Programa ' + sProgram + ' No Pudo Ser Invocado' );
          -2: LogError( 'El Programa ' + sProgram + ' No Existe' );
     else
         Result := False;
     end;
end;

{ ---------------- Metodo que llama a procesar los archivos ------------------ }

procedure TTArchivos.ProcesaArchivos(const sSource, sDestino,sEncriptado, sPrograma, sParametros: String);
begin
     LogStart;
     try

        LogTimeStamp( '*** Recolecci�n Iniciada %s ***' );
        if Validar( sSource, sDestino, sEncriptado,sPrograma, sParametros ) then
        begin
             { Verificar si es demo }
             if FEsDemo then
             begin
                  //Mostrar mensaje de demo
                  LogEvent( Format( K_MSG_DEMO , [ K_TOPE_DEMO ] ) );
                  if not FSilent then
                     ZetaDialogo.ZInformation( '�Atenci�n!', Format( K_MSG_DEMO , [ K_TOPE_DEMO ] ) , 0 );

                  //Deshabilitar boton de procesar
                  Procesar.Enabled := False;
             end//if FEsDemo then
             else
             begin
                  if DownloadClockLogs( sDestino,sEncriptado ) then
                  begin
                       //LogEvent( Format( 'El archivo' + CR_LF + '%s' + CR_LF + 'fu� generado', [ sDestino ] ) );
                       //AddInfo( Format( 'El archivo' + CR_LF + '%s' + CR_LF + 'fu� generado', [ sDestino ] ) );
                       if ZetaCommonTools.StrLleno( sPrograma ) then
                       begin
                            if Ejecutar( sPrograma, sParametros ) then
                            begin
                                 LogEvent( Format( 'El programa' + CR_LF + '%s' + CR_LF + 'fu� ejecutado con los par�metros' + CR_LF + '%s', [ sPrograma, sParametros ] ) );
                                 LogMensaje( 'Los archivos fueron procesados' );
                            end;
                       end
                       else
                       begin
                            if not FSilent and ZetaDialogo.zConfirm( Caption, 'El Archivo ' + sDestino + ' Fu� Creado' + CR_LF + '� Desea Verlo ?', 0, mbYes ) then
                            begin
                                 ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sDestino, ExtractFileDir( sDestino ), SW_SHOWDEFAULT );
                            end;
                       end;
                  end;
             end;
             //else
                 //LogError( 'Error en el proceso de conversi�n de archivos' );
        end;//if Validar
        LogTimeStamp( '*** Recolecci�n Terminada %s ***' );
     finally
            LogEnd;
     end;
end;

{ ------------------ Metodo que Maneja los Mensajes de Error ----------------- }

procedure TTArchivos.LogStart;
begin
     FAscii.Init( ChangeFileExt( Application.ExeName, '.LOG' ) );
end;

procedure TTArchivos.LogEnd;
begin
     FAscii.Close
end;

procedure TTArchivos.LogMensaje(const sMensaje: string);
var
   strAux : string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );
     if not FSilent then ZetaDialogo.ZInformation( '�Atenci�n!', sMensaje, 0 );
end;

procedure TTArchivos.LogError(const sMensaje: string);
var
   strAux: string;
begin
     strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
     FAscii.WriteTexto( strAux );
     if not FSilent then ZetaDialogo.ZError( '�Se encontr� un error!', sMensaje, 0 );
end;

procedure TTArchivos.LogEvent( const sMensaje: String );
var strAux : string;
begin
     //if FSilent then
     //begin
            strAux := DateTimeToStr( NOW ) + ' - ' + sMensaje;
            FAscii.WriteTexto( strAux );
     //end;
end;

procedure TTArchivos.LogTimeStamp( const sMensaje: String );
begin
     //if FSilent then
     //begin
            FAscii.WriteTimeStamp( StringOfChar( '*', 15 ) +  sMensaje + StringOfChar( '*', 15 ) );
     //end;
end;

{ ------ Metodo para escoger el Directorio Fuente ----- }

function TTArchivos.BuscaArchivo( const sArchivo: String ): String;
begin
     with OpenDialog do
     begin
          Title := 'Seleccione El Archivo A Transformar';
          DefaultExt := 'txt';
          Filter := 'Datos ( *.dat )|*.dat|Textos ( *.txt )|*.txt|Todos ( *.*. )|*.*';
          FilterIndex := 0;
          InitialDir := ExtractFilePath( sArchivo );
          FileName := ExtractFileName( sArchivo );
          if Execute then
             Result := FileName
          else
              Result := sArchivo;
     end;
end;

procedure TTArchivos.ArchivoDestinoBTNClick(Sender: TObject);
begin
     with ArchivoDestino do
     begin
          Text := BuscaArchivo( Text );
     end;
end;

procedure TTArchivos.ArchivoDestinoChange(Sender: TObject);
begin
     { Se usa para todos los controles, no nada m�s para Archivo Destino }
     if not FShowing then
        Guardar.Enabled := True;
end;

procedure TTArchivos.ProgramaBTNClick(Sender: TObject);
begin
     with Programa do
     begin
          with OpenDialog do
          begin
               Title := 'Seleccione El Programa A Ejecutar';
               DefaultExt := 'exe';
               Filter := 'Ejecutables ( *.exe )|*.exe|Batch ( *.bat )|*.bat|Todos ( *.*. )|*.*';
               FilterIndex := 0;
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

{ ----- Metodo de los botones para Guardar y Procesar ----- }

function TTArchivos.GetArchivoFuente: String;
begin
     //Result := ArchivoFuente.Text;
     Result := 'VACIO';
end;

function TTArchivos.GetArchivoDestino: String;
begin
     Result := ArchivoDestino.Text;
end;

function TTArchivos.GetArchivoEncriptado: String;
begin
     Result := ArchivoEncriptado.Text;
end;

function TTArchivos.GetPrograma: String;
begin
     Result := Programa.Text;
end;

function TTArchivos.GetParametros: String;
begin
     Result := Parametros.Text;
end;

function TTArchivos.Validar( const sSource, sDestino,sArchivoEncript, sPrograma, sParametros: String ): Boolean;
begin
     Result := False;
     if StrLleno(sArchivoEncript ) then
     begin
           if FileExists( sArchivoEncript ) then
           begin
                if ZetaCommonTools.StrVacio( sPrograma ) or FileExists( sPrograma ) then
                   Result := True
                else
                    LogError( Format( 'Programa ( %s ) No Existe', [ sPrograma ] ) );
           end
           else LogError( Format( 'Archivo Encriptado ( %s ) No Existe', [ sArchivoEncript ] ) );
     end
     else LogError( 'Archivo Encriptado no definido');

end;

procedure TTArchivos.GuardarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with FIniValues do
        begin
             { Archivos }
             ArchivoFuente := Self.GetArchivoFuente;
             ArchivoDestino := Self.GetArchivoDestino;
             Programa := Self.GetPrograma;
             Parametros := Self.GetParametros;
             ArchivoEncript := Self.GetArchivoEncriptado; 
             { Checadas }
             RelojDEF := Self.RelojDEF.Text;
             TipoDEF := Self.TipoDEF.Text;
             EmpresaDEF := Self.EmpresaDEF.Text;
             CredencialDEF := Self.CredencialDEF.Text;
        end;
        Guardar.Enabled := False;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTArchivos.RunSilent( const sParametro: String );
begin
     RunSilent( sParametro, FIniValues.ArchivoFuente );
end;

procedure TTArchivos.RunSilent( const sParametro, sFuente: String );
const
     P_IMPORTAR = '/I';
var
   sDestino, sPrograma,sEncriptado, sParametros: String;
begin
     if ( UpperCase( Trim( sParametro ) ) = P_IMPORTAR ) then
     begin
          FSilent := True;
          try
             with FIniValues do
             begin
                  sDestino := ArchivoDestino;
                  sPrograma := Programa;
                  sParametros := Parametros;
                  sEncriptado := ArchivoEncript; 
             end;
             ProcesaArchivos( sFuente, sDestino,sEncriptado, sPrograma, sParametros );
          finally
                 FSilent := False;
          end;
     end
     else
         ZetaDialogo.zError( '� Error En Par�metro !',
                             'Se Especific� el Par�metro ' + sParametro + CR_LF +
                             CR_LF +
                             'Los Par�metros V�lidos Son: ' + CR_LF +
                             Format( '%s <ARCHIVO> : Transformar Archivo', [ P_IMPORTAR ] ), 0 );
end;

procedure TTArchivos.AyudaClick(Sender: TObject);
var
   FAyuda: TShowAyuda;
begin
     FAyuda := TShowAyuda.Create( Self );
     try
        FAyuda.ShowModal;
     finally
            FreeAndNil( FAyuda );
     end;
end;

procedure TTArchivos.ProcesarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ProcesaArchivos( GetArchivoFuente, GetArchivoDestino,GetArchivoEncriptado,GetPrograma, GetParametros );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTArchivos.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TTArchivos.BitBtn1Click(Sender: TObject);
var
   ErrorCode: integer;
   lstIPList: TStringList;
   sNumSerie:widestring;
   sArchivo, sIPReloj, sIDReloj: string;
   i: integer;
   lAutorizado:Boolean;

begin
     lstIPList := TStringList.Create;
     try
        sArchivo := ExtractFilePath( Application.ExeName ) + K_ARCHIVO_LISTA_RELOJES;
        if FileExists( sArchivo ) then
        begin
             if FileExists( ArchivoEncriptado.Text )then
             begin
                  PageControl.ActivePage := Estatus;
                  lstIPList.LoadFromFile( sArchivo );
                  //Recorrer lista de relojes
                  for i := 0 to ( lstIPList.Count - 1 ) do
                  begin
                       //Obtener ID e IP del Reloj
                       sIPReloj := lstIPList.Strings[ i ];
                       sIDReloj := Trim( copy( sIPReloj, 1, pos( '=', sIPReloj ) - 1 ) );
                       Delete( sIPReloj, 1, pos( '=', sIPReloj ) );
                       sIPReloj := Trim( sIPReloj );

                       //Conectarse al reloj
                       AddInfo( Format( 'Conectando con reloj %s (%s)', [ sIDReloj, sIPReloj ] ) );

                       if ( CZKEM1.Connect_net( sIPReloj, 4370 ) ) then
                       begin
                            //Reloj conectado con exito
                            AddInfo( Format( 'Reloj %s (%s) conectado con �xito.', [ sIDReloj, sIPReloj ] ) );
                            FlstRelojsAut.LoadFromFile(ArchivoEncriptado.Text);
                            CZKEM1.GetSerialNumber(1,sNumSerie);
                            if FEsDemo then
                            begin
                                 AddInfo( K_MSG_DEMO );
                                 AddInfo( Format( K_MSG_N_AUT , [ sIDReloj, sIPReloj ] ) );
                            end
                            else
                            begin
                                 if RelojAutorizado( sNumSerie,lAutorizado )then
                                 begin
                                      if lAutorizado then
                                      begin
                                           AddInfo( Format( K_MSG_AUT , [ sIDReloj, sIPReloj,IntToStr( FNumeroSentinel ) ] ) )
                                      end
                                      else
                                          AddInfo( Format( K_MSG_N_AUT_SEN, [ sIDReloj, sIPReloj,IntToStr( FNumeroSentinel ) ] ) );
                                 end
                                 else
                                     AddInfo( Format( K_MSG_N_AUT, [ sIDReloj, sIPReloj ] ) );
                            end;

                            ReadDeviceStatus;
                            CZKEM1.RefreshData(1);
                            CZKEM1.EnableDevice(1, TRUE);
                            CZKEM1.Disconnect;
                       end//if conectado
                       else
                       begin
                            //No se pudo conectar reloj
                            CZKEM1.GetLastError(ErrorCode);
                            AddInfo( Format( 'ERROR: El reloj %s (%s) no pudo ser conectado (Error #%d).', [ sIDReloj, sIPReloj, ErrorCode ] ) );
                            AddInfo( VACIO );
                       end;
                  end;
             end
             else
                  AddInfo( Format( 'Archivo Encriptado %s no existe, no se puede realizar diagn�stico', [ ArchivoEncriptado.Text  ] ) );
        end
        else
            AddInfo( Format( 'Archivo %s no existe, no se puede realizar diagn�stico', [ sArchivo ] ) );
     finally
            FreeAndNil( lstIPList );
     end;
end;



function TTArchivos.RelojAutorizado( const sNumSerie:widestring;var Autorizado:Boolean ):Boolean;
var
   Reloj:TStrings;
   i:Integer;
begin
     Reloj := TStringList.Create;
     try
        Result := False;
        Autorizado := False;
        for i := 0 to ( FlstRelojsAut.Count - 1 ) do
        begin
             Reloj.Delimiter := '|';
             Reloj.DelimitedText := Decrypt( FlstRelojsAut.Strings[ i ] );
             Result :=  Reloj.Strings[1] = sNumSerie;
             if Result then
             begin
                  Autorizado := StrToInt( Reloj.Strings[0] ) = FNumeroSentinel;
                  break;
             end;
        end;
     finally
            FreeAndNil(Reloj);
     end;
end;

procedure TTArchivos.ReadDeviceStatus;
var
  s: widestring;
  ErrorCode, //Value, i,
  dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond: integer;
begin
     if CZKEM1.GetFirmwareVersion(1, s) then
        AddInfo('Versi�n de Firmware: '+s)
     else
     begin
          CZKEM1.GetLastError(ErrorCode);
          AddInfo(format('Error #(%d)',[ErrorCode]));
     end;
     if CZKEM1.GetSerialNumber(1, s) then
        AddInfo('N�mero de Serie: '+s)
     else
     begin
          CZKEM1.GetLastError(ErrorCode);
          AddInfo(format('Error #(%d)',[ErrorCode]));
     end;

     if CZKEM1.GetProductCode(1, s) then
        AddInfo('C�digo de producto: '+s)
     else
     begin
          CZKEM1.GetLastError(ErrorCode);
          AddInfo(format('Error #(%d)',[ErrorCode]));
     end;

     if CZKEM1.GetDeviceTime(1, dwYear, dwMonth, dwDay, dwHour, dwMinute, dwSecond) then
        AddInfo(Format('Fecha y Hora de la terminal: %s/%s/%s %s:%s:%s',[ ZetaCommonTools.PadLCar( IntToStr( dwDay ), 2, '0' ),
                                                                          ZetaCommonTools.PadLCar( IntToStr( dwMonth ), 2, '0' ),
                                                                          ZetaCommonTools.PadLCar( IntToStr( dwYear ), 4, '0' ),
                                                                          ZetaCommonTools.PadLCar( IntToStr( dwHour ), 2, '0' ),
                                                                          ZetaCommonTools.PadLCar( IntToStr( dwMinute ), 2, '0' ),
                                                                          ZetaCommonTools.PadLCar( IntToStr( dwSecond ), 2, '0' ) ] ) )
     else
     begin
          CZKEM1.GetLastError(ErrorCode);
          AddInfo(format('Error #(%d)',[ErrorCode]));
     end;

     AddInfo( '' );

     {
     for i:=1 to length(StatusNames) - 2 do
     begin
          if i = 4 then
          begin
               if CZKEM1.GetDeviceStatus(devid, 21, value) then
                  AddInfo(format('%s: %d', [StatusNames[13],value]))
               else
               begin
                    CZKEM1.GetLastError(ErrorCode);
                    AddInfo(format('! GetDeviceStatus(%d) ErrorNo.=%d',[21, ErrorCode]));
               end;
          end;
          if i = 8 then
          begin
               if CZKEM1.GetDeviceStatus(devid, 22, value) then
                  AddInfo(format('%s: %d', [StatusNames[14],value]))
                  else
                  begin
                  CZKEM1.GetLastError(ErrorCode);
                  AddInfo(format('! GetDeviceStatus(%d) ErrorNo.=%d',[22, ErrorCode]));
               end;
          end;
          if CZKEM1.GetDeviceStatus(devid, i, value) then
             AddInfo(format('%s: %d', [StatusNames[i],value]))
          else
          begin
               CZKEM1.GetLastError(ErrorCode);
               AddInfo(format('! GetDeviceStatus(%d) ErrorNo.=%d',[i, ErrorCode]));
          end;
     end;
     }
end;

procedure TTArchivos.AddInfo(const s: string);
begin
  memo1.Lines.Add(s);
  memo1.Refresh;
end;


procedure TTArchivos.ArchivoEncriptadoBTNClick(Sender: TObject);
begin
     with ArchivoEncriptado do
     begin
          with OpenDialog do
          begin
               Title := 'Seleccione El Archivo Encriptado de Terminales';
               DefaultExt := 'dat';
               Filter := 'Terminales ( *.dat )|*.dat|Todos ( *.*. )|*.*';
               FilterIndex := 0;
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;



end.
