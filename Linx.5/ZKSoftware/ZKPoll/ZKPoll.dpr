program ZKPoll;

uses
  Forms,
  SysUtils,
  FPoll in 'FPoll.pas' {TArchivos},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FAyuda in 'FAyuda.pas' {ShowAyuda};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'ZK Software Poll';
  Application.CreateForm(TTArchivos, TArchivos);
  case ParamCount of
       1:
       begin
            TArchivos.RunSilent( ParamStr( 1 ) );
            FreeAndNil( TArchivos );
       end;
       2:
       begin
            TArchivos.RunSilent( ParamStr( 1 ), ParamStr( 2 ) );
            FreeAndNil( TArchivos );
       end;
  end;
  Application.Run;
end.
