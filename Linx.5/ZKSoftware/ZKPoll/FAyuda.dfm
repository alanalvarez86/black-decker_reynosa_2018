inherited ShowAyuda: TShowAyuda
  Left = 280
  Top = 165
  Caption = 'Ayuda Sobre Transformador ....'
  ClientHeight = 411
  ClientWidth = 602
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 375
    Width = 602
    inherited OK: TBitBtn
      Left = 434
    end
    inherited Cancelar: TBitBtn
      Left = 519
    end
    object Exportar: TBitBtn
      Left = 5
      Top = 6
      Width = 75
      Height = 25
      Hint = 'Exportar Ayuda A Un Archivo Texto'
      Caption = 'Exportar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = ExportarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333303
        333333333333337FF3333333333333903333333333333377FF33333333333399
        03333FFFFFFFFF777FF3000000999999903377777777777777FF0FFFF0999999
        99037F3337777777777F0FFFF099999999907F3FF777777777770F00F0999999
        99037F773777777777730FFFF099999990337F3FF777777777330F00FFFFF099
        03337F773333377773330FFFFFFFF09033337F3FF3FFF77733330F00F0000003
        33337F773777777333330FFFF0FF033333337F3FF7F3733333330F08F0F03333
        33337F7737F7333333330FFFF003333333337FFFF77333333333000000333333
        3333777777333333333333333333333333333333333333333333}
      NumGlyphs = 2
    end
  end
  object Ayuda: TMemo
    Left = 0
    Top = 0
    Width = 602
    Height = 375
    Align = alClient
    BorderStyle = bsNone
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      
        'El programa ZKPoll se utiliza para la conexi'#243'n a las terminales ' +
        'ZK Software'
      
        'iClock 580. Este mismo programa sirva para la recolecci'#243'n de los' +
        ' registros de'
      
        'asistencia que se hayan realizado en dicha terminal. La forma de' +
        ' conectarse'
      'a estas terminales es por direcci'#243'n IP.'
      ''
      
        'El programa ZKPoll requiere que sea configurado para tomar un ar' +
        'chivo de texto'
      
        'con las direcciones IP de cada una de las terminales de donde es' +
        'tara realizando'
      
        'la recolecci'#243'n. El resultado final de este proceso sera un archi' +
        'vo con los '
      'registros'
      
        'de asistencia que haya n sido obtenidos de la terminal. Este arc' +
        'hivo tendra el'
      
        'formato requerido por Sistema TRESS para su importaci'#243'n hacia la' +
        ' asistencia'
      'de los empleados.'
      ''
      
        'Opcionalmente, se puede invocar un programa para procesar el arc' +
        'hivo Destino'
      'una vez que este fu'#233' generado:'
      ''
      'CONFIGURACION'
      '============='
      ''
      'Configuraci'#243'n inicial:'
      '----------------------'
      ''
      
        #183' Archivo Destino:      Indica el archivo generado con el format' +
        'o de Sistema TRESS'
      
        '                        a partir de los datos obtenidos de la te' +
        'rminal.'
      '                        Generalmente es <directorio>RELOJ.DAT.'
      
        #183' Programa:             Indica el programa con el cual se proces' +
        'a el archivo'
      '                        destino.'
      '                        generalmente es L5POLL.EXE.'
      ''
      
        '.Archivo Encriptado:    Archivo Generado en el Corportativo de G' +
        'rupo Tress'
      
        '                        Internacional al generar las claves para' +
        ' el sentinel'
      '                        adquirido.'
      ''
      
        #183' Par'#225'metros:           Indica los par'#225'metros de ejecuci'#243'n con e' +
        'l cual invocar'
      
        '                        el programa. Generalmente es /I <directo' +
        'rio>RELOJ.DAT.'
      
        '                        Default : Obtiene dato de "Archivo Desti' +
        'no"'
      ''
      'Registros de asistencia(Defaults):'
      '---------'
      ''
      
        #183' Credencial:           Indica el # de credencial usado por el e' +
        'mpleado para'
      
        '                        realizar el registro de asistencia. Si e' +
        'l default no se'
      '                        especifica, se usa '#39'A'#39'.'
      
        #183' Entrada/Salida:       Indica el tipo del registro de asistenci' +
        'a. Si el default'
      '                        no se especifica, se usa '#39'1'#39'.'
      ''
      'Estatus del proceso:'
      '---------'
      ''
      
        'Informaci'#243'n del proceso que se realizo. Todos los mensajes y/o e' +
        'rrrores que se'
      
        'produjeron en el momento del proceso aqui se podran observar. Se' +
        ' cuenta'
      
        'con un bot'#243'n de Diagnostivo para obtener la informacion de la te' +
        'rminal que se'
      'esta procesando.'
      ''
      'El bot'#243'n de "GUARDAR" permite grabar los valores configurados.'
      ''
      'OPERACION'
      '========='
      ''
      
        'El bot'#243'n de "PROCESAR" inicia el procesamiento de la informaci'#243'n' +
        ' configurada.'
      ''
      
        'Para permitir la ejecuci'#243'n autom'#225'tica, el programa ZKPoll.exe pu' +
        'ede ser'
      'invocado con par'#225'metros:'
      ''
      #183' ZKPoll.exe:           Se ejecuta la interfase visual.'
      
        #183' ZKPoll.exe /I:        Se ejecuta en modo batch procesando la c' +
        'onfiguraci'#243'n'
      '                        que se haya realizado.'
      ''
      
        'Al ejecutar la aplicaci'#243'n en modo batch (con el par'#225'metro /I) lo' +
        's resultados del'
      
        'procesamiento se almacenan en un archivo de bit'#225'cora en el mismo' +
        ' directorio '
      'donde se encuentra ZKPoll.exe. El archivo de bit'#225'cora se llama'
      'ZKPoll.LOG'
      '')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object SaveDialog: TSaveDialog
    Filter = 'Texto (*.txt)|*.txt|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Escoja El Archivo A Generar'
    Left = 88
    Top = 64
  end
end
