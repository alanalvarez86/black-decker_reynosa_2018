unit FArchivos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal,
     LinxIni;

type
  TArchivos = class(TZetaDlgModal)
    Linx5OSLBL: TLabel;
    Linx4OSLBL: TLabel;
    ProgramaLBL: TLabel;
    AlarmaLBL: TLabel;
    EmpleadosLBL: TLabel;
    Linx5OSSeek: TSpeedButton;
    Linx4OSSeek: TSpeedButton;
    ProgramaSeek: TSpeedButton;
    AlarmaSeek: TSpeedButton;
    EmpleadosSeek: TSpeedButton;
    Empleados: TEdit;
    Alarma: TEdit;
    Programa: TEdit;
    Linx4OS: TEdit;
    Linx5OS: TEdit;
    OpenDialog: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure Linx5OSSeekClick(Sender: TObject);
    procedure Linx4OSSeekClick(Sender: TObject);
    procedure ProgramaSeekClick(Sender: TObject);
    procedure AlarmaSeekClick(Sender: TObject);
    procedure EmpleadosSeekClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
  public
    { Public declarations }
  end;

var
  Archivos: TArchivos;

implementation

{$R *.DFM}

procedure TArchivos.FormShow(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Self.Linx5OS.Text := Linx5OS;
          Self.Linx4OS.Text := Linx4OS;
          Self.Programa.Text := Programa;
          Self.Alarma.Text := Alarma;
          Self.Empleados.Text := Empleados;
     end;
end;

function TArchivos.BuscaArchivo( const sFileName, sFilter, sTitle: String ): String;
begin
     with OpenDialog do
     begin
          Title := sTitle;
          FileName := sFileName;
          Filter := sFilter;
          FilterIndex := 0;
          if Execute then
             Result := FileName
          else
              Result := sFileName;
     end;
end;

procedure TArchivos.Linx5OSSeekClick(Sender: TObject);
begin
     inherited;
     Linx5OS.Text := BuscaArchivo( Linx5OS.Text, 'Linx 5 OS|L5*.OS|Linx OS|L*.OS|Todos los Archivos|*.*', 'Sistema Operativo De Linx 5' );
end;

procedure TArchivos.Linx4OSSeekClick(Sender: TObject);
begin
     inherited;
     Linx4OS.Text := BuscaArchivo( Linx4OS.Text, 'Linx 4 OS|L4*.OS|Linx OS|L*.OS|Todos los Archivos|*.*', 'Sistema Operativo De Linx 4' );
end;

procedure TArchivos.ProgramaSeekClick(Sender: TObject);
begin
     inherited;
     Programa.Text := BuscaArchivo( Programa.Text, 'Programas|*.lxe|Todos los Archivos|*.*', 'Aplicaciones de Linx' );
end;

procedure TArchivos.AlarmaSeekClick(Sender: TObject);
begin
     inherited;
     Alarma.Text := BuscaArchivo( Alarma.Text, 'Alarmas|*.val|Todos los Archivos|*.*', 'Archivo de Alarma para Aplicaciones de Linx' );
end;

procedure TArchivos.EmpleadosSeekClick(Sender: TObject);
begin
     inherited;
     Empleados.Text := BuscaArchivo( Empleados.Text, 'Empleado|*.val|Todos los Archivos|*.*', 'Archivo de Empleados para Aplicaciones de Linx' );
end;

procedure TArchivos.OKClick(Sender: TObject);
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Linx5OS := Self.Linx5OS.Text;
          Linx4OS := Self.Linx4OS.Text;
          Programa := Self.Programa.Text;
          Alarma := Self.Alarma.Text;
          Empleados := Self.Empleados.Text;
     end;
end;

end.
