unit FOptimizador;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, StdCtrls, Mask, Buttons, ExtCtrls,
     LinxIni,
     LinxIniBase,
     ZetaNumero,
     ZBaseDlgModal;

type
  TOptimizador = class(TZetaDlgModal)
    PanelTop: TPanel;
    IntervaloLBL: TLabel;
    Intervalo: TZetaNumero;
    Parametros: TGroupBox;
    Optimizador: TStringGrid;
    procedure OptimizadorGetEditText(Sender: TObject; ACol, ARow: Integer; var Value: String);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Optimizador: TOptimizador;

implementation

{$R *.DFM}

procedure TOptimizador.FormShow(Sender: TObject);
var
   L5Option: TL5Option;
   i: Integer;
begin
     inherited;
     with Optimizador do
     begin
          RowCount := 1;
          ColCount := 3;
          ColWidths[ 0 ] := 20;
          ColWidths[ 1 ] := 150;
          ColWidths[ 2 ] := 600;
          Cells[ 0, 0 ] := '#';
          Cells[ 1, 0 ] := 'Descripción';
          Cells[ 2, 0 ] := 'Valor';
          for i := 0 to ( LinxIni.IniValues.Options.Count - 1 ) do
          begin
               L5Option := LinxIni.IniValues.Options.Option[ i ];
               RowCount := RowCount + 1;
               Cells[ 0, i + 1 ] := IntToStr( L5Option.Number );
               Cells[ 1, i + 1 ] := LinxIni.IniValues.Options.Strings[ i ];
               Cells[ 2, i + 1 ] := StringValue( IntToStr( L5Option.Value ) );
               Objects[ 2, i + 1 ] := L5Option;
          end;
          FixedRows := 1;
          FixedCols := 2;
     end;
     Intervalo.Valor := LinxIni.IniValues.Intervalo;
end;

procedure TOptimizador.OptimizadorGetEditText(Sender: TObject; ACol, ARow: Integer; var Value: String);
begin
     Value := StringValue( Value );
end;

procedure TOptimizador.OKClick(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     with LinxIni.IniValues do
     begin
          Intervalo := Self.Intervalo.ValorEntero;
          with Options do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Option[ i ].Value := IntegerValue( Optimizador.Cells[ 2, i + 1 ] );
               end;
               Write;
          end;
     end;
end;

end.
