unit LinxIni;

interface

uses Classes, IniFiles, SysUtils, Forms, Dialogs,
     LinxIniBase;

const
     K_LEN_SEGMENTO = 3;
     K_SEGMENTO_1 = 1;
     K_SEGMENTO_2 = 4;
     K_SEGMENTO_3 = 7;
     K_SEGMENTO_4 = 10;
     Linx5OptNum0 = 0;   { L5_Reset : Reset to all default values }
     Linx5OptNum1 = 1;   { L5_ServerAvailCnt : Number of extra server avails to issue }
     Linx5OptNum2 = 2;   { L5_NotAvailCnt : Number of server not avails to issue }
     Linx5OptNum3 = 3;   { L5_SendRetryCnt : Number of data send retries to do }
     Linx5OptNum4 = 4;   { L5_ResponseWait : msec to wait for individual response }
     Linx5OptNum5 = 5;   { L5_BResponseWait : Broadcast Response Wait }
     Linx5OptNum6 = 6;   { L5_PollInterval : Time interval between online polls }
     Linx5OptNum7 = 7;   { L5_AvailInterval : Time between server avails }
     Linx5OptNum8 = 8;   { L5_AltInterval : Time to wait if an alternate }
     Linx5OptNum9 = 9;   { L5_Broadcast : 1 for broadcast, 0 for table only }
     Linx5OptNum10 = 10; { Ln_Compatibility: Level of LINX OS to support }
     Linx5OptNum11 = 11; { Ln_DebugMode: Debugging level }
     Linx5OptNum12 = 12; { Ln_RxBuffers: Number of receive buffers allocated }
     Linx5OptNum13 = 13; { Ln_NetBuffers: Number of network buffers allocated }
     Linx5OptNum14 = 14; { Ln_LocalPort: Port number on this computer (server) }
     Linx5OptNum15 = 15; { Ln_RemotePort: Port number of the Linx terminals }
     Linx5OptNum16 = 16; { Ln_MaxResponse: Maximum response time allowed before retry }
     Linx5OptNum17 = 17; { Ln_LogInterval: Statistics logging interval in milliseconds }
     Linx5OptNum18 = 18; { Ln_OpenNotAvailCnt: Number of initial server not avails to issue }
     Linx5OptDefNum = -1;
     Linx5OptDefault = 'DEFAULT';
     L5_Broadcast_True = 1;
     L5_Broadcast_False = 0;

type
  TTerminal = class( TObject )
  private
    { Private declarations }
    FAddress: TIPAddress;
    FDescription: String;
    FMasterID: TUnitID;
    FUnitID: TUnitID;
    FIdentifica: TIdentificaID;
    FEmpleados: String;
    FAlarma: String;
    function GetMasterID: TUnitID;
    function GetUnitID: TUnitID;
    function GetIPAddress: TIPAddress;
    function GetLinxAddress: String;
    function GetLookupAddress: String;
    function GetIdentificador: TIdentificaID;
    function FormatUnitNumber( const sUnitNumber: String ): TUnitID;
  public
    { Public declarations }
    constructor Create( const sData: String );
    property Address: TIPAddress read FAddress write FAddress;
    property LinxAddress: String read GetLinxAddress;
    property LookupAddress: String read GetLookupAddress;
    property IPAddress: TIPAddress read GetIPAddress;
    property Description: String read FDescription write FDescription;
    property MasterID: TUnitID read GetMasterID write FMasterID;
    property UnitID: TUnitID read GetUnitID write FUnitID;
    property Identificador: TIdentificaID read GetIdentificador write FIdentifica;
    property Alarma: String read FAlarma write FAlarma;
    property Empleados: String read FEmpleados write FEmpleados;
    function GetInfo: String;
  end;
  TTerminals = class( TStringList )
  private
    { Private declarations }
    function GetTerminal( Index: Integer ): TTerminal;
    function GetTerminalFileName: String;
    procedure Init;
    procedure Write;
  protected
    { protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Terminal[ Index: Integer ]: TTerminal read GetTerminal;
    function GetTerminalID(const sTerminal: String): String;
    function GetTerminalInfo( sAddress: String ): TTerminal;
    procedure Cargar( Lista: TStrings );
    procedure Clear; override;
    procedure Descargar( Lista: TStrings );
    procedure Limpiar( Lista: TStrings );
  end;
  TL5Option = class( TObject )
  private
    { Private declarations }
    FNumber: Integer;
    FValue: Integer;
    function GetIsDefault: Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( const Numero, Valor: Integer );
    property Number: Integer read FNumber;
    property Value: Integer read FValue write FValue;
    property IsDefault: Boolean read GetIsDefault;
  end;
  TL5Options = class( TStringList )
  private
    { Private declarations }
    FIniFile: TIniFile;
    function GetOption( Index: Integer ): TL5Option;
    procedure ReadOption( const sOption: String; iOption: Integer );
  protected
    { Protected declarations }
    property IniFile: TIniFile read FIniFile;
  public
    { Public declarations }
    constructor Create( ParentIniFile: TIniFile );
    destructor Destroy; override;
    property Option[ Index: Integer ]: TL5Option read GetOption;
    procedure Write;
  end;
  TL5IniValues = class( TIniValues )
  private
    { Private declarations }
    FOptions: TL5Options;
    FTerminalList: TTerminals;
    function GetLinx4OS: String;
    function GetLinx5OS: String;
    function GetSleep: LongInt;
    function GetTerminales: LongInt;
    function GetUsarListaTerminales: Boolean;
    procedure PutLinx4OS( const Valor: String );
    procedure PutLinx5OS( const Valor: String );
    procedure PutSleep(const Value: LongInt);
    procedure PutTerminales( const Valor: LongInt );
    procedure PutUsarListaTerminales( const Valor: Boolean );
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Options: TL5Options read FOptions;
    property ListaTerminales: TTerminals read FTerminalList;
    property Terminales: LongInt read GetTerminales write PutTerminales;
    property UsarListaTerminales: Boolean read GetUsarListaTerminales write PutUsarListaTerminales;
    property Sleep: LongInt read GetSleep write PutSleep;
    property Linx5OS: String read GetLinx5OS write PutLinx5OS;
    property Linx4OS: String read GetLinx4OS write PutLinx4OS;
  end;

var
   IniValues: TL5IniValues;

procedure IniValuesInit;
procedure IniValuesClear;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     FLinxTools;

const
     K_LEN_ADDRESS = 12;
     K_LEN_UNIT_NUMBER = 2;
     K_LEN_IDENTIFICADOR = 4;
     K_CERO = '0';
     K_ARCHIVO_TOKEN = '|';
     K_ID_TOKEN = '=';

     TERMINAL_SEPARATOR = '|';

     L5Poll_Terminales = 'Terminales';
     L5Poll_Usar_Lista_Terminales = 'Usar Lista de Terminales';
     L5Poll_Tipo = 'Tipo';
     L5Poll_Sleep = 'Sleep';

     Archivos_Linx5OS = 'Linx5 OS';
     Archivos_Linx4OS = 'Linx4 OS';

     INI_L5SETOPTIONS = 'L5SetOptions';
     Linx5Option0 = 'L5_Reset';
     Linx5Option1 = 'L5_ServerAvailCnt';
     Linx5Option2 = 'L5_NotAvailCnt';
     Linx5Option3 = 'L5_SendRetryCnt';
     Linx5Option4 = 'L5_ResponseWait';
     Linx5Option5 = 'L5_BResponseWait';
     Linx5Option6 = 'L5_PollInterval';
     Linx5Option7 = 'L5_AvailInterval';
     Linx5Option8 = 'L5_AltInterval';
     Linx5Option9 = 'L5_Broadcast';
     Linx5Option10 = 'Ln_Compatibility';
     Linx5Option11 = 'Ln_DebugMode';
     Linx5Option12 = 'Ln_RxBuffers';
     Linx5Option13 = 'Ln_NetBuffers';
     Linx5Option14 = 'Ln_LocalPort';
     Linx5Option15 = 'Ln_RemotePort';
     Linx5Option16 = 'Ln_MaxResponse';
     Linx5Option17 = 'Ln_LogInterval';
     Linx5Option18 = 'Ln_OpenNotAvailCnt';

procedure IniValuesInit;
begin
     if not Assigned( IniValues ) then
     begin
          IniValues := TL5IniValues.Create;
     end;
end;

procedure IniValuesClear;
begin
     if Assigned( IniValues ) then
     begin
          FreeAndNil( IniValues );
     end;
end;

{ ***** TTerminal ******** }

constructor TTerminal.Create( const sData: String );
var
   iPos: Integer;
   sArchivos: String;
begin
     iPos := Pos( K_ID_TOKEN, sData );
     if ( iPos > 0 ) then
     begin
          FDescription := Copy( sData, 1, ( iPos - 1 ) );
          FAddress := Copy( sData, ( iPos + 1 ), K_LEN_ADDRESS );
          FMasterID := Copy( sData, ( iPos + 14 ), K_LEN_UNIT_NUMBER );
          FUnitID := Copy( sData, ( iPos + 17 ), K_LEN_UNIT_NUMBER );
          FIdentifica := Copy( sData, ( iPos + 20 ), K_LEN_IDENTIFICADOR );
          sArchivos := Copy( sData, ( iPos + 25 ), ( Length( sData ) - ( iPos + 24 ) ) );
          FAlarma := StrToken( sArchivos, K_ARCHIVO_TOKEN );
          FEmpleados := sArchivos;
     end;
end;

function TTerminal.FormatUnitNumber( const sUnitNumber: String ): TUnitID;
begin
     Result := StrPadChL( Trim( Copy( Trim( sUnitNumber ), 1, K_LEN_UNIT_NUMBER ) ), K_CERO, K_LEN_UNIT_NUMBER );
end;

function TTerminal.GetMasterID: TUnitID;
begin
     Result := FormatUnitNumber( FMasterID );
end;

function TTerminal.GetUnitID: TUnitID;
begin
     Result := FormatUnitNumber( FUnitID );
end;

function TTerminal.GetIdentificador: TIdentificaID;
begin
     Result := PadR( Trim( FIdentifica ), K_LEN_IDENTIFICADOR );
end;

function TTerminal.GetInfo: String;
const
     K_LEN_DESCRIPTION = 20;
     K_ESPACIO = ' ';
begin
     Result := PadR( Copy( FDescription, 1, K_LEN_DESCRIPTION ), K_LEN_DESCRIPTION ) +
               K_ID_TOKEN +
               StrPadChR( Trim( Copy( FAddress, 1, K_LEN_ADDRESS ) ), K_CERO, K_LEN_ADDRESS ) +
               K_ESPACIO +
               MasterID +
               '-' +
               UnitID +
               K_ESPACIO +
               Identificador +
               K_ESPACIO +
               FAlarma +
               K_ARCHIVO_TOKEN +
               FEmpleados;
end;

function TTerminal.GetLinxAddress: String;
const
     PAD_FORMAT = '%3d';
begin
     Result := Trim( Format( PAD_FORMAT, [ StrToIntDef( Copy( FAddress, K_SEGMENTO_1, K_LEN_SEGMENTO ), 0 ) ] ) ) +
               '.' +
               Trim( Format( PAD_FORMAT, [ StrToIntDef( Copy( FAddress, K_SEGMENTO_2, K_LEN_SEGMENTO ), 0 ) ] ) ) +
               '.' +
               Trim( Format( PAD_FORMAT, [ StrToIntDef( Copy( FAddress, K_SEGMENTO_3, K_LEN_SEGMENTO ), 0 ) ] ) ) +
               '.' +
               Trim( Format( PAD_FORMAT, [ StrToIntDef( Copy( FAddress, K_SEGMENTO_4, K_LEN_SEGMENTO ), 0 ) ] ) ) +
               ' ' +
               MasterID +
               '-' +
               UnitID;
end;

function TTerminal.GetLookupAddress: String;
begin
     Result := Format( '%3.3d.%3.3d.%3.3d.%3.3d %s%s', [ StrToIntDef( Copy( FAddress, K_SEGMENTO_1, K_LEN_SEGMENTO ), 0 ),
                                                         StrToIntDef( Copy( FAddress, K_SEGMENTO_2, K_LEN_SEGMENTO ), 0 ),
                                                         StrToIntDef( Copy( FAddress, K_SEGMENTO_3, K_LEN_SEGMENTO ), 0 ),
                                                         StrToIntDef( Copy( FAddress, K_SEGMENTO_4, K_LEN_SEGMENTO ), 0 ),
                                                         MasterID,
                                                         UnitID ] );
end;

function TTerminal.GetIPAddress: TIPAddress;
begin
     Result := strTrimChL( Copy( FAddress, K_SEGMENTO_1, K_LEN_SEGMENTO ), K_CERO ) +
               '.' +
               strTrimChL( Copy( FAddress, K_SEGMENTO_2, K_LEN_SEGMENTO ), K_CERO ) +
               '.' +
               strTrimChL( Copy( FAddress, K_SEGMENTO_3, K_LEN_SEGMENTO ), K_CERO ) +
               '.' +
               strTrimChL( Copy( FAddress, K_SEGMENTO_4, K_LEN_SEGMENTO ), K_CERO ) +
               ' ' +
               MasterID +
               UnitID;
end;

{ ***** TL5Terminals ******** }

constructor TTerminals.Create;
begin
     inherited Create;
end;

destructor TTerminals.Destroy;
begin
     Clear;
     inherited Destroy;
end;

function TTerminals.GetTerminal( Index: Integer ): TTerminal;
begin
     Result := TTerminal( Objects[ Index ] );
end;

procedure TTerminals.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Terminal[ i ].Free;
     end;
     inherited Clear;
end;

function TTerminals.GetTerminalFileName: String;
const
     TERMINAL_FILE_NAME = 'L5UNITS.INI';
begin
     Result := FLinxTools.SetFileNameDefaultPath( TERMINAL_FILE_NAME );
end;

procedure TTerminals.Init;
var
   UnitFile: TextFile;
   sData: String;
begin
     if FileExists( GetTerminalFileName ) then
     begin
          AssignFile( UnitFile, GetTerminalFileName );
          Reset( UnitFile );
          while not Eof( UnitFile ) do
          begin
               Readln( UnitFile, sData );
               AddObject( sData, TTerminal.Create( sData ) );
          end;
          CloseFile( UnitFile );
     end;
end;

function TTerminals.GetTerminalInfo( sAddress: String ): TTerminal;
var
   i: Integer;
begin
     Result := nil;
     for i := 0 to ( Self.Count - 1 ) do
     begin
          if ( sAddress = Terminal[ i ].LookupAddress ) then
          begin
               Result := Terminal[ i ];
               Break;
          end;
     end;
end;

procedure TTerminals.Cargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  NewTerminal := TTerminal.Create( Terminal[ i ].GetInfo );
                  AddObject( NewTerminal.Description, NewTerminal );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminals.Descargar( Lista: TStrings );
var
   i: Integer;
   NewTerminal: TTerminal;
begin
     try
        BeginUpdate;
        Clear;
        for i := 0 to ( Lista.Count - 1 ) do
        begin
             NewTerminal := TTerminal.Create( TTerminal( Lista.Objects[ i ] ).GetInfo );
             AddObject( NewTerminal.Description, NewTerminal );
        end;
     finally
            EndUpdate;
     end;
     Write;
end;

procedure TTerminals.Limpiar( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          try
             BeginUpdate;
             for i := ( Count - 1 ) downto 0 do
             begin
                  TTerminal( Objects[ i ] ).Free;
             end;
             Clear;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TTerminals.Write;
var
   i: Integer;
   UnitFile: TextFile;
begin
     AssignFile( UnitFile, GetTerminalFileName );
     Rewrite( UnitFile );
     for i := 0 to ( Count - 1 ) do
         Writeln( UnitFile, Terminal[ i ].GetInfo );
     CloseFile( UnitFile );
end;

function TTerminals.GetTerminalID( const sTerminal: String ): String;
var
   i: Integer;
   sAddress: String;

   function ObtieneNetSubNet( sAddress: String ): String;
   var
      iLen, iLenIP, j: Integer;
      sIP: String;
   begin
        Result := VACIO;
        iLen := Length( sAddress );
        while ( iLen > 0 ) do
        begin
             if ( sAddress[ iLen ] = '.' ) then
                break;
             Dec( iLen );
        end;
        if ( iLen = 0 ) then
           Result := 'XXXX'
        else
        begin
             sIP := Trim( Copy( sAddress, (iLen + 1 ), 3 ) );
             iLenIP := Length( sIP );
             if ( iLenIP < 3 ) then
             begin
                  for j := 1 to ( 3 - iLenIP) do
                  begin
                       sIP := K_CERO + sIP;
                  end;
             end;
             Result := Copy( sAddress, (iLen - 1 ), 1 ) + sIP;
        end;
   end;

   function ObtieneInicioIP( const sIniIP: String ): String;
   const
        K_LONGITUD_IP = 6;
   var
      i, iLenIP, j: Integer;
      sIP, sOctetos: String;
   begin
        sIP := VACIO;
        sOctetos := VACIO;
        for i:= K_LONGITUD_IP downto 1 do
        begin
             if ( sIniIP[i] = '.' ) and( i <> K_LONGITUD_IP ) then
             begin
                  iLenIP := Length( sOctetos );
                  if ( iLenIP < 3 ) then
                  begin
                       for j := 1 to ( 3 - iLenIP) do
                       begin
                            sIP := K_CERO + sIP;
                       end;
                  end;
                  sOctetos := VACIO;
             end
             else
                 if ( sIniIP[i] <> '.' ) then
                 begin
                       sIP := sIniIP[i] + sIP;
                       sOctetos := sIniIP[i] + sOctetos;
                 end;
        end;
        Result := sIP;
        if ( Length( Result ) < K_LONGITUD_IP ) then
           Result := K_CERO + sIP;
   end;

begin
     Result := VACIO;
     for i := 0 to ( Count - 1 ) do
     begin
          with Terminal[ i ] do
          begin
               sAddress := Copy( LinxAddress, 1, 18 );
               if ( sAddress = sTerminal ) then
               begin
                    if ZetaCommonTools.StrLleno( Identificador ) then
                    begin
                         Result := Identificador;
                    end;
                    Break;
               end;
          end;
     end;
     if ZetaCommonTools.StrVacio( Result ) then
     begin
          Result := ObtieneNetSubNet( sTerminal );
     end;
     Result := Result + ObtieneInicioIP( sTerminal );
end;

{ ***** TL5Option ******** }

constructor TL5Option.Create( const Numero, Valor: Integer );
begin
     inherited Create;
     FNumber := Numero;
     FValue := Valor;
end;

function TL5Option.GetIsDefault: Boolean;
begin
     Result := ( Value = Linx5OptDefNum );
end;

{ ***** TL5Options ******** }

constructor TL5Options.Create( ParentIniFile: TIniFile );
begin
     inherited Create;
     FIniFile := ParentIniFile;
     ReadOption( Linx5Option0, Linx5OptNum0 );
     ReadOption( Linx5Option1, Linx5OptNum1 );
     ReadOption( Linx5Option2, Linx5OptNum2 );
     ReadOption( Linx5Option3, Linx5OptNum3 );
     ReadOption( Linx5Option4, Linx5OptNum4 );
     ReadOption( Linx5Option5, Linx5OptNum5 );
     ReadOption( Linx5Option6, Linx5OptNum6 );
     ReadOption( Linx5Option7, Linx5OptNum7 );
     ReadOption( Linx5Option8, Linx5OptNum8 );
     ReadOption( Linx5Option9, Linx5OptNum9 );
     ReadOption( Linx5Option10, Linx5OptNum10 );
     ReadOption( Linx5Option11, Linx5OptNum11 );
     ReadOption( Linx5Option12, Linx5OptNum12 );
     ReadOption( Linx5Option13, Linx5OptNum13 );
     ReadOption( Linx5Option14, Linx5OptNum14 );
     ReadOption( Linx5Option15, Linx5OptNum15 );
     ReadOption( Linx5Option16, Linx5OptNum16 );
     ReadOption( Linx5Option17, Linx5OptNum17 );
     ReadOption( Linx5Option18, Linx5OptNum18 );
end;

function TL5Options.GetOption( Index: Integer ): TL5Option;
begin
     Result := TL5Option( Objects[ Index ] );
end;

procedure TL5Options.ReadOption( const sOption: String; iOption: Integer );
var
   sValue: String;
   iValue, iCode: Integer;
begin
     sValue := IniFile.ReadString( INI_L5SETOPTIONS, sOption, Linx5OptDefault );
     if ( sValue = '' ) or ( sValue = Linx5OptDefault ) then
        iValue := Linx5OptDefNum
     else
        Val( sValue, iValue, iCode );
     AddObject( sOption, TL5Option.Create( iOption, iValue ) );
end;

procedure TL5Options.Write;
var
   sValue: String;
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if Option[ i ].IsDefault then
             sValue :=  Linx5OptDefault
          else
              sValue := IntToStr( Option[ i ].Value );
          IniFile.WriteString( INI_L5SETOPTIONS, Strings[ i ], sValue );
     end;
end;

destructor TL5Options.Destroy;
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          Option[ i ].Free;
     end;
     inherited Destroy;
end;

{ ***** TL5IniValues ******** }

constructor TL5IniValues.Create;
const
     INI_FILE_NAME = 'L5POLL.INI';
begin
     inherited Create( INI_FILE_NAME );
     FOptions := TL5Options.Create( IniFile );
     FTerminalList := TTerminals.Create;
     FTerminalList.Init;
end;

destructor TL5IniValues.Destroy;
begin
     FTerminalList.Free;
     FOptions.Free;
     inherited Destroy;
end;

{ *** M�todos Read / Write de las propiedades *** }

function TL5IniValues.GetSleep: LongInt;
begin
     Result := IniFile.ReadInteger( INI_L5POLL, L5Poll_Sleep, 500 );
end;

procedure TL5IniValues.PutSleep(const Value: LongInt);
begin
     IniFile.WriteInteger( INI_L5POLL, L5Poll_Sleep, Value );
end;

function TL5IniValues.GetTerminales: LongInt;
begin
     Result := 0; { IniFile.ReadInteger( INI_L5POLL, L5Poll_Terminales, 0 ); }
end;

procedure TL5IniValues.PutTerminales( const Valor: LongInt );
begin
     IniFile.WriteInteger( INI_L5POLL, L5Poll_Terminales, Valor );
end;

function TL5IniValues.GetUsarListaTerminales: Boolean;
begin
     Result := ReadBoolean( INI_L5POLL, L5Poll_Usar_Lista_Terminales, False );
end;

procedure TL5IniValues.PutUsarListaTerminales( const Valor: Boolean );
begin
     WriteBoolean( INI_L5POLL, L5Poll_Usar_Lista_Terminales, Valor );
end;

function TL5IniValues.GetLinx5OS: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Archivos_Linx5OS, '' );
end;

procedure TL5IniValues.PutLinx5OS( const Valor: String );
begin
     IniFile.WriteString( INI_ARCHIVOS, Archivos_Linx5OS, Valor );
end;

function TL5IniValues.GetLinx4OS: String;
begin
     Result := IniFile.ReadString( INI_ARCHIVOS, Archivos_Linx4OS, '' );
end;

procedure TL5IniValues.PutLinx4OS( const Valor: String );
begin
     IniFile.WriteString( INI_ARCHIVOS, Archivos_Linx4OS, Valor );
end;

end.
