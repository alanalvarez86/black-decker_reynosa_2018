inherited Optimizador: TOptimizador
  Left = 278
  Top = 422
  ActiveControl = Intervalo
  Caption = 'Parámetros de Optimización'
  ClientHeight = 245
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 209
    BevelOuter = bvNone
    TabOrder = 2
    inherited OK: TBitBtn
      OnClick = OKClick
    end
  end
  object PanelTop: TPanel
    Left = 0
    Top = 0
    Width = 375
    Height = 30
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object IntervaloLBL: TLabel
      Left = 9
      Top = 8
      Width = 122
      Height = 13
      Alignment = taRightJustify
      Caption = '&Intervalo de Recolección:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Intervalo: TZetaNumero
      Left = 134
      Top = 4
      Width = 38
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
    end
  end
  object Parametros: TGroupBox
    Left = 0
    Top = 30
    Width = 375
    Height = 179
    Align = alClient
    Caption = ' Parámetros del Servidor de Linx V '
    TabOrder = 1
    object Optimizador: TStringGrid
      Left = 2
      Top = 15
      Width = 371
      Height = 162
      Align = alClient
      ColCount = 3
      DefaultRowHeight = 21
      FixedCols = 2
      RowCount = 1
      FixedRows = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing, goTabs, goAlwaysShowEditor]
      TabOrder = 0
      OnGetEditText = OptimizadorGetEditText
    end
  end
end
