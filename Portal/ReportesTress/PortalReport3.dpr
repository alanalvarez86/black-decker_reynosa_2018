program PortalReport3;

uses
  Forms,
  ZetaClientTools,
  FPortalReportTress in 'FPortalReportTress.pas' {ReportesTress},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DReportesGenerador in '..\..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DReportesPortal in 'DReportesPortal.pas' {dmReportesPortal: TDataModule};

{$R *.RES}

begin
     ZetaClientTools.InitDCOM;
     Application.Initialize;
     Application.Title := 'Reportes Portal 3';
  Application.CreateForm(TReportesTress, ReportesTress);
  if ( ParamCount > 0 ) then
        ReportesTress.ProcesarBatch
     else
         Application.Run;
end.
