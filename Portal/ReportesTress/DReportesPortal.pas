unit DReportesPortal;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Registry, ComCtrls,
     FAutoClasses,
     DPortalCaller,
     {$ifdef DOS_CAPAS}
     DServerPortalTress,
     {$else}
     PortalTress_TLB,
     {$endif}
     DReportesGenerador,
     ZetaClientDataSet;

type
  TListaReportesPortal = class;
  TPortalReporte = class( TObject )
  private
    { Private declarations }
    FLista: TListaReportesPortal;
    FEmpresa: String;
    FNombre: String;
    FFolio: Integer;
    FReporte: Integer;
    FReporteTitulo: String;
  public
    { Public declarations }
    constructor Create( Lista: TListaReportesPortal );
    property Empresa: String read FEmpresa write FEmpresa;
    property Folio: Integer read FFolio write FFolio;
    property Lista: TListaReportesPortal read FLista;
    property Nombre: String read FNombre write FNombre;
    property Reporte: Integer read FReporte write FReporte;
    property ReporteTitulo: String read FReporteTitulo write FReporteTitulo;
    function GetTitulo: String;
    function TieneReporte: Boolean;
    procedure SetValues( const sEmpresa: String; const iReporte: Integer );
  end;
  TListaReportesPortal = class( TObject )
  private
    { Private declarations }
    FChanged: Boolean;
    FItems: TList;
    FRegistry: TRegIniFile;
    {$ifdef DOS_CAPAS}
    FServerPortalTress: TdmPortalTress;
    {$else}
    FServerPortalTress: IdmPortalTressDisp;
    function GetServerPortalTress: IdmPortalTressDisp;
    {$endif}
    function GetItem(Index: Integer): TPortalReporte;
    procedure Delete(const Index: Integer);
    function GetEmpresas: String;
    procedure SetEmpresas(const Value: String);
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerPortalTress: TdmPortalTress read FServerPortalTress;
    {$else}
    property ServerPortalTress: IdmPortalTressDisp read GetServerPortalTress;
    {$endif}
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Items[ Index: Integer ]: TPortalReporte read GetItem;
    property Changed: Boolean read FChanged;
    property Empresas: String read GetEmpresas write SetEmpresas;
    function AddItem(const iFolio: Integer ): TPortalReporte;
    function BuildList( Lista: TTreeNodes ): Boolean;
    function Count: Integer;
    function GetIndexOf(const iFolio: Integer): Integer;
    function Load( const lCheck: Boolean ): Boolean;
    procedure Borrar(const iFolio: Integer);
    procedure Clear;
    procedure SetChanged;
    procedure Unload;
  end;
  TdmReportesPortal = class(TdmReportGenerator)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FPortalReportes: TListaReportesPortal;
    FReporte: Integer;
    FEmpresa: String;
    FFolio: Integer;
    function ReportesPortalProcesarUno(const sEmpresa: String; const iReporte, iFolio: Integer): Boolean;
  protected
    { Protected declarations }
    procedure DoOnGetResultado( const lResultado: Boolean; const Error : string );override;
  public
    { Public declarations }
    procedure ReportesPortalProcesarTodos;
  end;

var
  dmReportesPortal: TdmReportesPortal;

implementation

{$R *.DFM}

uses DCliente,
     ZetaDialogo,
     ZetaBusqueda,
     ZReportTools,
     ZReportConst,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaWinAPITools,
     ZetaRegistryCliente;

const
     K_FOLIO = 0;
     K_FOLIO_ABIERTO = 1;

{ ******** TPortalReporte *********** }

constructor TPortalReporte.Create( Lista: TListaReportesPortal );
begin
     FLista := Lista;
end;

function TPortalReporte.GetTitulo: String;
begin
     if TieneReporte then
        Result := Format( '%d .- %s: Empresa %s, Reporte %d', [ Folio, Nombre, Empresa, Reporte ] )
     else
         Result := Format( '%d .- %s', [ Folio, Nombre ] );
end;

function TPortalReporte.TieneReporte: Boolean;
begin
     Result := ( Reporte > 0 ) and ZetaCommonTools.StrLleno( Empresa );
end;

procedure TPortalReporte.SetValues( const sEmpresa: String; const iReporte: Integer );
begin
     if ( Empresa <> sEmpresa ) then
     begin
          Empresa := sEmpresa;
          Lista.SetChanged;
     end;
     if ( Reporte <> iReporte ) then
     begin
          Reporte := iReporte;
          Lista.SetChanged;
     end;
end;

{ ******** TListaReportesPortal ********* }

constructor TListaReportesPortal.Create;
begin
     FItems := TList.Create;
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := ZetaRegistryCliente.ClientRegistry.RegistryRoot;
          LazyWrite := False;
          OpenKey( Format( '%sPortal\Reportes', [ ZetaWinAPITools.FormatDirectory( ZetaRegistryCliente.ClientRegistry.RegistryKey ) ] ), True );
     end;
     {$ifdef DOS_CAPAS}
     FServerPortalTress := TdmPortalTress.Create( nil );
     {$endif}
end;

destructor TListaReportesPortal.Destroy;
begin
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerPortalTress );
     {$endif}
     Clear;
     FreeAndNil( FRegistry );
     FreeAndNil( FItems );
     inherited Destroy;
end;

procedure TListaReportesPortal.Clear;
var
   i: Integer;
begin
     with FItems do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

procedure TListaReportesPortal.SetChanged;
begin
     FChanged := True;
end;

function TListaReportesPortal.Count: Integer;
begin
     Result := FItems.Count;
end;

function TListaReportesPortal.GetItem(Index: Integer): TPortalReporte;
begin
     with FItems do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TPortalReporte( Items[ Index ] )
          else
              Result := nil;
     end;
end;

{$ifndef DOS_CAPAS}
function TListaReportesPortal.GetServerPortalTress: IdmPortalTressDisp;
begin
     Result := IdmPortalTressDisp( dmCliente.CreaServidor( CLASS_dmPortalTress, FServerPortalTress ) );
end;
{$endif}

procedure TListaReportesPortal.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FItems.Delete( Index );
end;

function TListaReportesPortal.AddItem(const iFolio: Integer): TPortalReporte;
begin
     Result := TPortalReporte.Create( Self );
     try
        FItems.Add( Result );
        with Result do
        begin
             Folio := iFolio;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

function TListaReportesPortal.GetIndexOf(const iFolio: Integer): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Items[ i ].Folio = iFolio ) then
          begin
               Result := i;
               Break;
          end;
     end;
end;

procedure TListaReportesPortal.Borrar(const iFolio: Integer);
var
   i: Integer;
begin
     i := GetIndexOf( iFolio );
     if ( i >= 0 ) then
     begin
          Delete( i );
     end;
end;

procedure TListaReportesPortal.Unload;
var
   i: Integer;
   FEmpresas: TStrings;
begin
     if FChanged then
     begin
          FEmpresas := TStringList.Create;
          try
             with FRegistry do
             begin
                  ReadSections( FEmpresas );
                  for i := 0 to ( FEmpresas.Count - 1 ) do
                  begin
                       EraseSection( FEmpresas.Strings[ i ] );
                  end;
                  for i := 0 to ( Count - 1 ) do
                  begin
                       with Self.Items[ i ] do
                       begin
                            WriteString( Empresa, IntToStr( Folio ), IntToStr( Reporte ) );
                       end;
                  end;
             end;
             FChanged := False;
          finally
                 FreeAndNil( FEmpresas );
          end;
     end;
end;

function TListaReportesPortal.Load( const lCheck: Boolean ): Boolean;
var
   i, j, iPtr, iReporte, iFolio: Integer;
   FEmpresas, FReportes: TStrings;
   sEmpresa, sFolio: String;
begin
     if not lCheck then
     begin
          FChanged := False;
          Clear;
     end;
     FEmpresas := TStringList.Create;
     try
        FReportes := TStringList.Create;
        try
           with FRegistry do
           begin
                ReadSections( FEmpresas );
                for i := 0 to ( FEmpresas.Count - 1 ) do
                begin
                     sEmpresa := FEmpresas.Strings[ i ];
                     with FReportes do
                     begin
                          Clear;
                          ReadSectionValues( sEmpresa, FReportes );
                          for j := 0 to ( Count - 1 ) do
                          begin
                               sFolio := Names[ j ];
                               iFolio := StrToIntDef( sFolio, 0 );
                               iReporte := StrToIntDef( Values[ sFolio ], 0 );
                               if ( iFolio > 0 ) and ( iReporte > 0 ) then
                               begin
                                    iPtr := Self.GetIndexOf( iFolio );
                                    if lCheck then
                                    begin
                                         if ( iPtr >= 0 ) then  { Si existe, �nicamente hay que incorporar los datos }
                                         begin
                                              with Self.Items[ iPtr ] do
                                              begin
                                                   Reporte := iReporte;
                                                   Empresa := sEmpresa;
                                              end;
                                         end
                                         else
                                             FChanged := True;
                                    end
                                    else
                                        if ( iPtr < 0 ) then
                                        begin
                                             with Self.AddItem( iFolio ) do
                                             begin
                                                  Reporte := iReporte;
                                                  Empresa := sEmpresa;
                                             end;
                                        end;
                               end;
                          end;
                     end;
                end;
           end;
        finally
               FreeAndNil( FReportes );
        end;
     finally
            FreeAndNil( FEmpresas );
     end;
     Result := ( Count > 0 );
end;

function TListaReportesPortal.BuildList( Lista: TTreeNodes ): Boolean;
var
   i: Integer;
   Nodo: TTreeNode;
begin
     Clear;
     FChanged := False;
     { Primero hay que cargar los reportes }
     InitPortalCaller;
     try
        try
           with PortalCaller do
           begin
                if ReportsLoad then
                begin
                     with ReportsDataset do
                     begin
                          while not Eof do
                          begin
                               with AddItem( FieldByName( 'RP_FOLIO' ).AsInteger ) do
                               begin
                                    Nombre := FieldByName( 'RP_TITULO' ).AsString;
                               end;
                               Next;
                          end;
                     end;
                end;
           end;
        except
              on Error:Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            ClosePortalCaller;
     end;
     Lista.Clear;
     if Load( True ) then
     begin
          Nodo := nil;
          with Lista do
          begin
               Clear;
               BeginUpdate;
               try
                  for i := 0 to ( Self.Count - 1 ) do
                  begin
                       with Self.Items[ i ] do
                       begin
                            Nodo := AddObject( Nodo, GetTitulo, Self.Items[ i ] );
                       end;
                       with Nodo do
                       begin
                            ImageIndex := K_FOLIO;
                            SelectedIndex := K_FOLIO_ABIERTO;
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
     Result := ( Self.Count > 0 );
end;

function TListaReportesPortal.GetEmpresas: String;
begin
     Result := ServerPortalTress.GetEmpresas;
end;

procedure TListaReportesPortal.SetEmpresas(const Value: String);
begin
     ServerPortalTress.SetEmpresas( Value );
end;

{ *********** TdmReportGenerator1 ( Reportes para Portal ) ************* }

procedure TdmReportesPortal.DataModuleCreate(Sender: TObject);
const
     K_NOMBRE_BITACORA = 'ReportesPortal3.LOG';
begin
     SetLogFileName( ExtractFilePath( Application.ExeName ) + K_NOMBRE_BITACORA );
     inherited;
end;

procedure TdmReportesPortal.DoOnGetResultado( const lResultado: Boolean; const Error : string );
var
   i, iRow, iColumn: Integer;
   sMensaje: String;
begin
     with PortalCaller do
     begin
          BuildReportBegin;
          with cdsResultados do
          begin
               if IsEmpty then
               begin
                    sMensaje := Format( 'El Reporte %d de la Empresa %s No Tiene Datos Que Cumplan Con Los Filtros Especificados', [ FReporte, FEmpresa ] );
                    Log( sMensaje );
                    ReportIsEmpty( sMensaje );
               end
               else
               begin
                    AsignaListas;
                    {
                    iCountGrupos := oGrupos.Count - 1 - GetOffSet;
                    GeneraListaDetalle( iCountGrupos );
                    PreparaTotales( iCountGrupos, oListaTotales );
                    }
                    iColumn := 1;
                    with Campos do
                    begin
                         for i := 0 to ( Count - 1 ) do
                         begin
                              with TCampoListado( Objects[ i ] ) do
                              begin
                                   if ( Calculado <> K_RENGLON_NUEVO ) then
                                   begin
                                        if not SoloTotales or ( SQLColumna.Totalizacion <> ocNinguno ) then
                                        begin
                                             BuildAddColumn( FFolio, iColumn, Ancho, TipoImp, Titulo );
                                             Inc( iColumn );
                                        end;
                                   end;
                              end;
                         end;
                    end;
                    iRow := 1;
                    First;
                    while not Eof do
                    begin
                         BuildDataBegin;
                         iColumn := 1;
                         with Campos do
                         begin
                              for i := 0 to ( Count - 1 ) do
                              begin
                                   with TCampoListado( Objects[ i ] ) do
                                   begin
                                        if ( Calculado <> K_RENGLON_NUEVO ) then
                                        begin
                                             if not SoloTotales or ( SQLColumna.Totalizacion <> ocNinguno ) then
                                             begin
                                                  BuildDataAdd( iColumn, AsTexto( cdsResultados ) );
                                                  Inc( iColumn );
                                             end;
                                        end;
                                   end;
                              end;
                         end;
                         BuildDataEnd( FFolio, iRow );
                         Inc( iRow );
                         Next;
                    end;
               end;
               Active := False;
          end;
          BuildReportEnd( FFolio );
     end;
end;

function TdmReportesPortal.ReportesPortalProcesarUno( const sEmpresa: String; const iReporte, iFolio: Integer): Boolean;
begin
     FReporte := iReporte;
     FEmpresa := sEmpresa;
     FFolio := iFolio;
     Result := GetResultado( iReporte, False );
end;

procedure TdmReportesPortal.ReportesPortalProcesarTodos;
const
     K_DATE_TIME_FORMAT = 'dd/mmm/yyyy hh:mm:ss AM/PM';
var
   i: Integer;
   lOk: Boolean;
   sEmpresa: String;
begin
     if InitAutorizacion( okKiosko ) then
     begin
          Log( '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*' );
          Log( 'Proceso: Generaci�n de Reportes de Tress para Portal' );
          Log( 'Inicio: '+ FormatDateTime( K_DATE_TIME_FORMAT, Now ) );
          Log( '' );
          FPortalReportes := TListaReportesPortal.Create;
          try
             if FPortalReportes.Load( False ) then
             begin
                  InitPortalCaller;
                  try
                     try
                        with FPortalReportes do
                        begin
                             sEmpresa := '';
                             lOk := False;
                             for i := 0 to ( Count - 1 ) do
                             begin
                                  with Items[ i ] do
                                  begin
                                       if ( Empresa <> sEmpresa ) then
                                       begin
                                            sEmpresa := Empresa;
                                            if dmCliente.BuscaCompany( sEmpresa ) then
                                               lOk := True
                                            else
                                            begin
                                                 lOk := False;
                                                 Log( Format( 'Empresa %s No Es V�lida', [ sEmpresa ] ) );
                                            end;
                                       end;
                                       if lOk then
                                       begin
                                            if ReportesPortalProcesarUno( sEmpresa, Reporte, Folio ) then
                                               Log( Format( 'Reporte %d de Empresa %s Generado Hacia Portal Como El Reporte %d', [ Reporte, sEmpresa, Folio ] ) )
                                            else
                                                Log( Format( 'El Reporte %d de Empresa %s No Pudo Ser Generado', [ Reporte, sEmpresa ] ) );
                                       end;
                                  end;
                             end;
                        end
                     except
                           on Error:Exception do
                           begin
                                LogError( 'Error Al Generar Reportes Para Portal: ' + Error.Message, TRUE );
                           end;
                     end;
                  finally
                         ClosePortalCaller;
                  end;
             end
             else
             begin
                  Log( 'No Hay Reportes De Tress Configurados Para El Portal' );
                  Log( '' );
             end;
          finally
                 FreeAndNil( FPortalReportes );
          end;
          Log( 'Final: '+ FormatDateTime( K_DATE_TIME_FORMAT, Now ) );
          Log( '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*' );
     end
     else
     begin
          Log( 'El M�dulo De ' + Autorizacion.GetModuloStr( okKiosko ) + ' No Est� Autorizado' + CR_LF + 'Consulte A Su Distribuidor' );
          Log( '' );
     end;
end;

end.
