inherited dmCliente: TdmCliente
  OldCreateOrder = True
  Left = 285
  Top = 161
  object cdsPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 16
  end
  object cdsLookupReportes: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RE_NOMBRE'
    Params = <>
    AlAdquirirDatos = cdsLookupReportesAlAdquirirDatos
    LookupName = 'Reportes'
    LookupDescriptionField = 'RE_NOMBRE'
    LookupKeyField = 'RE_CODIGO'
    OnLookupSearch = cdsLookupReportesLookupSearch
    Left = 152
    Top = 72
  end
end
