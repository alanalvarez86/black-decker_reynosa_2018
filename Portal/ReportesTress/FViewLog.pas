unit FViewLog;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ShellAPI;

type
  TViewLog = class(TForm)
    PanelInferior: TPanel;
    Salir: TBitBtn;
    Bitacora: TListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FViewing: Boolean;
    FPortal: Boolean;
    FHTMLOnly: Boolean;
    procedure ShowMessage( const sText: String );
  public
    { Public declarations }
    property Portal: Boolean read FPortal write FPortal;
    property HTMLOnly: Boolean read FHTMLOnly write FHTMLOnly;
    procedure Execute;
  end;

var
  ViewLog: TViewLog;

implementation

uses ZetaDialogo,
     DReportesPortal;

{$R *.DFM}

function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

{ ********* TViewLog ********** }

procedure TViewLog.FormCreate(Sender: TObject);
begin
     FViewing := False;
     FPortal := False;
     FHTMLOnly := False;
end;

procedure TViewLog.FormActivate(Sender: TObject);
begin
     Execute;
end;

procedure TViewLog.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     CanClose := not FViewing;
end;

procedure TViewLog.Execute;
var
   oCursor: TCursor;
begin
     Salir.Enabled := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     FViewing := True;
     try
        with dmReportesPortal do
        begin
             OnLogCallBack := ShowMessage;
             ReportesPortalProcesarTodos;
        end;
     finally
            FViewing := False;
            Screen.Cursor := oCursor;
            Salir.Enabled := not FViewing;
     end;
end;

procedure TViewLog.ShowMessage( const sText: String );
begin
     with Bitacora do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  Add( sText );
               finally
                      EndUpdate;
               end;
               ItemIndex := ( Count - 1 );
          end;
     end;
     Application.ProcessMessages;
end;

end.
