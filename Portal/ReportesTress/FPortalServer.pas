unit FPortalServer;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     DReportesPortal,
     ZBaseDlgModal,
     ZetaNumero,
     ZetaKeyCombo;

type
  TPortalServer = class(TZetaDlgModal)
    ServerLBL: TLabel;
    Server: TEdit;
    ServerFind: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ServerFindClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function GetServerName: String;
    procedure SetServerName(const Value: String);
  public
    { Public declarations }
    property ServerName: String read GetServerName write SetServerName;
  end;

var
  PortalServer: TPortalServer;

implementation

{$R *.DFM}

uses ZetaNetworkBrowser,
     DPortalCaller;

procedure TPortalServer.FormCreate(Sender: TObject);
begin
     inherited;
     DPortalCaller.InitPortalCaller;
end;

procedure TPortalServer.FormShow(Sender: TObject);
begin
     inherited;
     ServerName := PortalCaller.PortalServer;
end;

procedure TPortalServer.FormDestroy(Sender: TObject);
begin
     inherited;
     DPortalCaller.ClosePortalCaller;
end;

function TPortalServer.GetServerName: String;
begin
     Result := Server.Text;
end;

procedure TPortalServer.SetServerName(const Value: String);
begin
     Server.Text := Value;
end;

procedure TPortalServer.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        PortalCaller.PortalServer := ServerName;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TPortalServer.ServerFindClick(Sender: TObject);
begin
     inherited;
     with Server do
     begin
          Text := ZetaNetworkBrowser.GetServer( Text );
     end;
end;

end.
