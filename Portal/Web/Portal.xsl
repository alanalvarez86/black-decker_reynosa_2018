<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<!-- TEMPLATE RAIZ -->

<xsl:template match="/">
<html> 
  <head>  	
    <link rel="stylesheet" href="Portal.css" type="text/css"/>
    <script language="JavaScript" SRC="Rolodex/CommonTools.js"/>
    <xsl:apply-templates select="RAIZ/DESCRIPCIONES/META"/>
    <title>
       <xsl:apply-templates select="RAIZ/DESCRIPCIONES/MODULO"/> -
	  <xsl:apply-templates select="RAIZ/DESCRIPCIONES/PANTALLA"/>
    </title>
  </head>
 <body onLoad="{RAIZ/DESCRIPCIONES/CARGAR}">
       <center>
	          <table width="90%" border="0" height="20" bgcolor="#08498C">
                      <tr>
                          <td height="20">
                              <font class="TITULO">
                                    <xsl:value-of select="RAIZ/DESCRIPCIONES/PANTALLA"/>
                              </font>
                          </td>
                      </tr>
			</table>
       </center>
       <xsl:apply-templates/>
 </body>
</html>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO RAIZ -->

<xsl:template match="RAIZ">
     <xsl:apply-templates/>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO ERRORES -->

<xsl:template match="ERRORES">
     <center>
       <font color="FF0000" face="Verdana"><h1>��� Se encontraron errores !!!</h1></font>
	  <hr width="90%"/>
	  <br/>
	  <h2><font face="Verdana">Mensajes de Error</font></h2>
	  <table width="90%" bgcolor="#E6E4E4" border="1" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff">
		<tr align="center">
			<td>
    			    <xsl:apply-templates/>
    		     </td>
    	     </tr>
       </table>
     </center>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO ERROR -->

<xsl:template match="ERROR">
     <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO WARNINGS -->

<xsl:template match="WARNINGS">
     <center>
      <!--<font color="FF0000" face="Verdana"><h1>Advertencia</h1></font>
	  <hr width="90%"/>
	  <br/>-->
	  <h1><font face="Verdana">��� Advertencia !!!</font></h1>
	  <table width="90%" bgcolor="#E6E4E4" border="1" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff">
		<tr align="center">
			<td>
    			<xsl:apply-templates/>
    		</td>
    	</tr>
      </table>
     </center>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO WARNING -->

<xsl:template match="WARNING">
     <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO DESCRIPCIONES -->

<xsl:template match="DESCRIPCIONES">
     <xsl:apply-templates select="modulo"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE MODULO -->

<xsl:template match="MODULO">
    <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE PARA AGREGAR META INFORMATION -->

<xsl:template match="META">
  <meta http-equiv="{@equiv}" content="{.}"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE PANTALLA -->

<xsl:template match="PANTALLA">
     <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE PARA DESPLIEGUE MASTER (MENU-TITULOS) -->

<xsl:template match="MASTER">
<!--     <hr width="90%"/>-->
     <table align="center" width="90%">
       	<xsl:apply-templates/>
     </table>
</xsl:template>

<!-- TEMPLATE PARA TABLA MASTER -->
<xsl:template match="TABLAMASTER">
     <tr>
     	<td>
         	<table border="0" align="{@alineacion}">
             	<xsl:apply-templates/>
         	</table>
         </td>
     </tr>
</xsl:template>

<!-- TEMPLATES PARA COLUMNAS DE TABLAS CONTENIDO MASTER -->
 <xsl:template match="CONTENIDO">
    <xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
           <xsl:if test="string-length(@clasefuente)!=0">
          	<td class="{@clasefuente}" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:when>
       <xsl:otherwise>
          <xsl:if test="string-length(@clasefuente)!=0">
               <td class="{@clasefuente}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td>
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:otherwise>
    </xsl:choose>
 </xsl:template>

<xsl:template match="LABEL">
   <font class="LETREROS">
      <xsl:value-of select="."/>
   </font>
</xsl:template>

<xsl:template match="FORMA">
   <form name="{@nombre}" method="{@metodo}" action="{@accion}" onSubmit="{@validar}" onReset="return deshacerCambios(this)" onLoad="{@cargar}">
       <xsl:apply-templates/>
   </form>
</xsl:template>

<!-- TEMPLATE DE LINEAS  -->
<xsl:template match="LINEA">
	<hr width="90%"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE ESPACIO -->

<xsl:template match="ESPACIO">
     <br/>
</xsl:template> 

<!-- TEMPLATE PARA MENU DE DESPLIEGUE MASTER -->

<xsl:template match="MENU">
    <input type="hidden" name="{@nombre}" value="{OPCIONSELECTED/@value}"/>
    <select onchange="{@alcambiar}" onfocus="MenuTemp(this,{@nombre})">
      <xsl:apply-templates select="OPCION|OPCIONSELECTED"/>
    </select>
</xsl:template>

<xsl:template match="COMBO">
    <select name="{@nombre}" onchange="{@alcambiar}">
      <xsl:apply-templates select="OPCION|OPCIONSELECTED"/>
    </select>
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE SUBMIT -->
<xsl:template match="BTNSUBMIT">
    <input type="submit" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE RESET -->
<xsl:template match="BTNRESET">
    <input type="reset" name="{@nombre}" value="{.}"/> 
</xsl:template>

<!-- TEMPLATE PARA BOTONES DE USO GENERAL -->
<xsl:template match="BOTON">
    <input type="button" name="{@nombre}" value="{.}" onclick="{@onclick}"/>
</xsl:template>

<!-- TEMPLATE PARA EDITS -->
<xsl:template match="EDIT">
    <input type="Text" size="{@tamano}" maxlength="{@longitud}" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA EDITS -->
<xsl:template match="EDITPSWD">
    <input type="password" size="{@tamano}" maxlength="{@longitud}" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA EDITS HIDDEN -->
<xsl:template match="HIDDEN">
    <input type="hidden" name="{@nombre}" value="{.}"/>
</xsl:template>

<!-- TEMPLATE PARA OPCION DE MENU -->
<xsl:template match="OPCION">
     <option value="{@value}"> <xsl:value-of select="@descripcion"/> </option>
</xsl:template>

<!-- TEMPLATE PARA OPCION DE MENU SELECTED--> 
<xsl:template match="OPCIONSELECTED">
      <option value="{@value}" selected="yes"> <xsl:value-of select="@descripcion"/> </option> 
</xsl:template>

<!-- TEMPLATE PARA LIGA HTML -->

<xsl:template match="LIGA">
  	<a href="{@href}" alt="{@hint}"><xsl:value-of select="."/></a>  
</xsl:template> 

<xsl:template match="LIGAIMG">
	<a href="{@href}"><img src="{@fuente}" border="0" alt="{@hint}"/></a>
</xsl:template>

<!-- TEMPLATE PARA LISTA HTML -->
<xsl:template match="LISTA">
<ul>  
  <xsl:apply-templates/>
</ul>
</xsl:template>

<!-- TEMPLATE PARA INCISO -->

<xsl:template match="INCISO">
  <li><xsl:apply-templates/></li>
</xsl:template>

<!-- TEMPLATE PARA DESPLIEGUE TABLA DETAIL -->

<xsl:template match="TABLA">
    <xsl:choose>
       <xsl:when test="@registros!=0">
          <table width="90%" align="center" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
              <font class="SUBTITULO"><xsl:value-of select="@titulo"/></font>
              <xsl:apply-templates/>
          </table>
       </xsl:when>
       <xsl:otherwise>
           <center>
           <font class="REG_ZERO">
              <b>* No hay datos *</b>
           </font>
           </center>
       </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="ENCABEZADO|RENGLON">
    <tr>
      <xsl:apply-templates/>
    </tr>
</xsl:template>

<xsl:template match="HEADER">
    <th class="TH_TEAL" font-class="xsmallblack">
        <xsl:apply-templates/>
    </th>
</xsl:template>

<xsl:template match="COLUMNA">
	<xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
           <xsl:if test="string-length(@clasefuente)!=0">
          	<td class="{@clasefuente}" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td class="TD_COLOR" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:when>
       <xsl:otherwise>
          <xsl:if test="string-length(@clasefuente)!=0">
               <td class="{@clasefuente}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td class="TD_COLOR">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="TEXTO">
    <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="IMAGEN">
    <IMG src="{@fuente}" alt="{@hint}"/>
</xsl:template>

<!-- TEMPLATE DE FOOTER(FECHAS/MENUS RETORNOS)  -->

<xsl:template match="FOOTER">
   <br/>           
   <hr width="90%"/>
   <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
   	<tr>
   		<td align="left">
      		<a href="javascript:window.history.back()">
          		<img src="imagenes/back.gif" border="0" alt="Pagina Anterior"/>
      		</a>
   		</td>
   		<!--<td align="center">
           <table border="0" cellpadding="0" width="90%">
               <xsl:apply-templates/>
           </table>
   		</td>-->
   		<td align="right">
      	<a href="inicio.asp">
          	<img src="Imagenes/inicio.gif" border="0" alt="Inicio"/>
      	</a>
   		</td>
   	</tr>
   </table>
   
   <center>
       <table border="0" cellpadding="0" width="90%">
            <xsl:apply-templates/>
       </table>
   </center>
</xsl:template>

<xsl:template match="LBLFOOTER">
   <font face="Arial" size="1">
      <xsl:apply-templates/>
   </font>
   <br/>
</xsl:template>

</xsl:stylesheet>


