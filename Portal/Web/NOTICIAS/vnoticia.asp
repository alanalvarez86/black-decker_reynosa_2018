<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="../errores.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false
	
	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim NoticiaXML	
	dim strNoticia
	dim xmlNoticia
	dim docXSLini
	dim docXSLmemo
	dim docXSLfin

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	set NoticiaXML = Server.CreateObject("Portal.dmServerPortal")
	
	strNoticia = NoticiaXML.GetNoticia("<RAIZ><PARAMETROS><FOLIO>"+Request("Folio")+"</FOLIO></PARAMETROS></RAIZ>")
    
	'Para Transformar el XML con el XSL tenemos que pasarl el string recibido  a documento XML
	set xmlNoticia =  server.CreateObject("Msxml2.DOMDocument.4.0")
	xmlNoticia.loadXML(strNoticia)	
	
	'Load the XSL para mostrar en pantalla los Combos
	set docXSLini = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSLini.async = false
	docXSLini.load(Server.MapPath("vnoticia.xsl"))	
	
	set docXSLmemo = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSLmemo.async = false
	docXSLmemo.load(Server.MapPath("memo.xsl"))	

	set docXSLfin = Server.CreateObject("Msxml2.DOMDocument.4.0")
	docXSLfin.async = false
	docXSLfin.load(Server.MapPath("finalnoticia.xsl"))
		
%>
<html>
<head>
	<meta http-equiv="Refresh" content="<%=Application("Tiempo")%>"; URL="vnoticia.asp">
</head>
<%	dim xmlResultado	
	dim docXSL
	dim Nodos
	
	set xmlResultado = CreateObject("Msxml2.DOMDocument.4.0")
	xmlResultado.loadXML(strNoticia)

	set Nodos = xmlResultado.documentElement.selectNodes( "//ERRORES" )
	if ( Nodos.Length = 0 ) then
		Response.Write(xmlNoticia.transformNode(docXSLini))	
	%>
   		<center>
	   	<table width="90%">
   		<font size="2" face="Verdana">
		<%Response.Write(xmlNoticia.transformNode(docXSLmemo))%>		
		</font>
		</table>
		</center>
		<%Response.Write(xmlNoticia.transformNode(docXSLfin))%>
	<%else
		set docXSL = Server.CreateObject("Msxml2.DOMDocument.4.0")
		docXSL.async = false
		docXSL.load(Server.MapPath("../errores.xsl"))
		Response.Write(xmlResultado.transformNode(docXSL))		
		'Response.Write( Nodos.Item( 0 ).Text )
 	end if
%>
</html>
