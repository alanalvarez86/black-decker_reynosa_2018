<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim PortalCOM
     dim ParametrosXML
	dim docXML
	dim docXSL
	dim strWrite

     'Documento XML para enviar Parametros
     CreaDocumentoXML ParametrosXML, true     
     AgregaParametro ParametrosXML, "PALABRA", Request("PALABRA")          
     SetTarjetaXML ParametrosXML
     'Response.write( ParametrosXML.XML )

	'Invocar al Componente COM+ y enviar el documento de XML generado
	set PortalCOM = Server.CreateObject("Portal.dmServerPortal")
     strWrite = PortalCOM.GrabaTarjeta( ParametrosXML.XML )
     'Response.write( strWrite )

     'Crear Documento para Cargar el XML Generado
     CreaDocumentoXML docXML, false
     docXML.loadXML( strWrite )

     'XML despues de Agregar
     '     docXML.load( Server.MapPath( "RolWriteAdd.xml" ) )
     'XML despues de Editar
     '     docXML.load( Server.MapPath( "RolWriteEdit.xml" ) )
     'XML despues de Borrar
     '     docXML.load( Server.MapPath( "RolWriteDel.xml" ) )
     'XML despues de marcar error al grabar
     '     docXML.load( Server.MapPath( "RolWriteError.xml" ) )

     'Response.write( docXML.XML )

     'Crear Documento para Cargar el XSL con que se presenta la información
     CreaDocumentoXML docXSL, false
	docXSL.load( Server.MapPath( "Rolodex.xsl" ) )

     Response.Write( docXML.transformNode( docXSL ) )

%>

