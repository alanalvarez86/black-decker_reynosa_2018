<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim PortalCOM
    dim ParametrosXML
	dim docXML
	dim docXSL
	dim strEdit

     'Documento XML para enviar Parametros
     CreaDocumentoXML ParametrosXML, true
     AgregaParametro ParametrosXML, "EMPRESA", Request("EMPRESA")          
     AgregaParametro ParametrosXML, "NOMBRES", Request("NOMBRES")    
     AgregaParametro ParametrosXML, "PALABRA", Request("PALABRA")          
     AgregaParametro ParametrosXML, "BTNAGREGAR", Request("BTNAGREGAR")          
	 AgregaParametro ParametrosXML, "ACCION", Request("ACCION")     
	 AgregaParametro ParametrosXML, "PAGINA", Request("PAGINA")     	 
     'Response.write( ParametrosXML.XML )

	 if ( Session("us_codigo")<>Application("UserDef") )then
		AgregaParametro ParametrosXML, "USUARIO", ""
	 else
		 AgregaParametro ParametrosXML, "USUARIO", "Invalido"
	 end if
	         
	 'Invocar al Componente COM+ y recibir el documento de XML generado
	 set PortalCOM = Server.CreateObject("Portal.dmServerPortal")
     strEdit = PortalCOM.GetTarjeta( ParametrosXML.XML )
     'Response.write( strEdit )

     'Crear Documento para Cargar el XML Generado
     CreaDocumentoXML docXML, false
     docXML.loadXML( strEdit )
	 'docXML.load( Server.MapPath( "RolEdit.xml" ) )
     'Response.write( docXML.XML )

     'Crear Documento para Cargar el XSL con que se presenta la información
     CreaDocumentoXML docXSL, false
	 docXSL.load( Server.MapPath( "Rolodex.xsl" ) )

     Response.Write( docXML.transformNode( docXSL ) )

%>

