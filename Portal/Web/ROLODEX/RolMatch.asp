<%@ Language=VBScript %>
<%option explicit%>
<!-- #include file="Tools.asp" -->
<%
	Response.Expires=0
	Response.Buffer=false

	'Declaramos Variables para Iniciar el COmponente y recibir el XML generado
	dim PortalCOM
    dim ParametrosXML
    dim WarningsXML
	dim docXML
	dim docXSL
	dim strMatch
	
     'Documento XML para enviar Parametros
     CreaDocumentoXML ParametrosXML, true
     AgregaParametro ParametrosXML, "PALABRA", Request("PALABRA")
     AgregaParametro ParametrosXML, "CAMPOORDEN", Request("CAMPOORDEN")
     AgregaParametro ParametrosXML, "ORDEN", Request("ORDEN")
     AgregaParametro ParametrosXML, "BTNBUSCAR", Request("BTNBUSCAR")
     AgregaParametro ParametrosXML, "PAGINA", Request("PAGINA")     
	 AgregaParametro ParametrosXML, "ACCION", Request("ACCION")          
	 AgregaParametro ParametrosXML, "EDICION", Request("EDICION")          	 
	 SetTarjetaXML ParametrosXML	 
	 	
	 if ( Session("us_codigo")<>Application("UserDef") )then
		AgregaParametro ParametrosXML, "USUARIO", ""
	 else
		 AgregaParametro ParametrosXML, "USUARIO", "Invalido"
	 end if	 	

     'Response.write( ParametrosXML.XML )
	 'Invocar al Componente COM+ y recibir el documento de XML generado
	 set PortalCOM = Server.CreateObject("Portal.dmServerPortal")
     strMatch = PortalCOM.GetMatchTarjetas( ParametrosXML.XML )
     'Response.write( strMatch )

     'Crear Documento para Cargar el XML Generado
     CreaDocumentoXML docXML, false
     docXML.loadXML( strMatch )

     'Crear Documento para Cargar el XSL con que se presenta la información
     CreaDocumentoXML docXSL, false
	 docXSL.load( Server.MapPath( "Rolodex.xsl" ) )
     Response.Write( docXML.transformNode( docXSL ) )
%>

