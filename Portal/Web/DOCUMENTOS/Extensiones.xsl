<?xml version='1.0' encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>

<!-- TEMPLATE RAIZ -->

<xsl:template match="/">
<html> 
  <head>  	
    <link rel="stylesheet" href="../Portal.css" type="text/css"/>
    <script language="JavaScript" SRC="CommonTools.js"/>
    <xsl:apply-templates select="RAIZ/DESCRIPCIONES/META"/>
    <title>
       <xsl:apply-templates select="RAIZ/DESCRIPCIONES/MODULO"/> -
	  <xsl:apply-templates select="RAIZ/DESCRIPCIONES/PANTALLA"/>
    </title>
  </head>
 <body onLoad="{RAIZ/DESCRIPCIONES/CARGAR}">
       <center>
	          <table width="90%" border="0" height="20" bgcolor="#08498C">
                      <tr>
                          <td height="20">
                              <font class="TITULO">
                                    <xsl:value-of select="RAIZ/DESCRIPCIONES/PANTALLA"/>
                              </font>
                          </td>
                      </tr>
			</table>
       </center>
       <xsl:apply-templates/>
 </body>
</html>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO RAIZ -->

<xsl:template match="RAIZ">
     <xsl:apply-templates/>
</xsl:template>

<!-- TEMPLATE DE ELEMENTO DESCRIPCIONES -->

<xsl:template match="DESCRIPCIONES">
     <xsl:apply-templates select="modulo"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE MODULO -->

<xsl:template match="MODULO">
    <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE PARA AGREGAR META INFORMATION -->

<xsl:template match="META">
  <meta http-equiv="{@equiv}" content="{.}"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE PANTALLA -->

<xsl:template match="PANTALLA">
     <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATES PARA COLUMNAS DE TABLAS CONTENIDO MASTER -->
 <xsl:template match="CONTENIDO">
    <xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
           <xsl:if test="string-length(@clasefuente)!=0">
          	<td class="{@clasefuente}" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:when>
       <xsl:otherwise>
          <xsl:if test="string-length(@clasefuente)!=0">
               <td class="{@clasefuente}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td>
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:otherwise>
    </xsl:choose>
 </xsl:template>

<xsl:template match="LABEL">
   <font class="LETREROS">
      <xsl:value-of select="."/>
   </font>
</xsl:template>

<!-- TEMPLATE DE LINEAS  -->
<xsl:template match="LINEA">
	<hr width="90%"/>
</xsl:template>

<!-- TEMPLATE DESPLIEGUE ESPACIO -->

<xsl:template match="ESPACIO">
     <br/>
</xsl:template> 

<!-- TEMPLATE PARA LIGA HTML -->

<xsl:template match="LIGA">
  	<a href="{@href}" alt="{@hint}"><xsl:value-of select="."/></a>  
</xsl:template> 

<xsl:template match="LIGAIMG">
	<a href="{@href}"><img src="{@fuente}" border="0" alt="{@hint}"/></a>
</xsl:template>


<!-- TEMPLATE PARA DESPLIEGUE TABLA DETAIL -->

<xsl:template match="TABLA">
    <xsl:choose>
       <xsl:when test="@registros!=0">
          <table width="90%" align="center" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
              <font class="SUBTITULO"><xsl:value-of select="@titulo"/></font>
              <xsl:apply-templates/>
          </table>
       </xsl:when>
       <xsl:otherwise>
           <center>
           <font class="REG_ZERO">
              <b>* No hay datos *</b>
           </font>
           </center>
       </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="ENCABEZADO|RENGLON">
    <tr>
      <xsl:apply-templates/>
    </tr>
</xsl:template>

<xsl:template match="HEADER">
    <th class="TH_TEAL" font-class="xsmallblack">
        <xsl:apply-templates/>
    </th>
</xsl:template>

<xsl:template match="COLUMNA">
	<xsl:choose>
       <xsl:when test="string-length(@alineacion)!=0">
           <xsl:if test="string-length(@clasefuente)!=0">
          	<td class="{@clasefuente}" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td class="TD_COLOR" align="{@alineacion}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:when>
       <xsl:otherwise>
          <xsl:if test="string-length(@clasefuente)!=0">
               <td class="{@clasefuente}">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
    		<xsl:if test="string-length(@clasefuente)=0">
    			<td class="TD_COLOR">
                   <xsl:apply-templates/>
               </td>
    		</xsl:if>
       </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template match="IMAGEN">
    <IMG src="{@fuente}" alt="{@hint}"/>
</xsl:template>

<xsl:template match="NOMBRE">
    <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="EXTENSION">
    <xsl:value-of select="."/>
</xsl:template>

<!-- TEMPLATE DE FOOTER(FECHAS/MENUS RETORNOS)  -->

<xsl:template match="FOOTER">
   <br/>           
   <hr width="90%"/>
   <table align="center" width="90%" border="0" cellspacing="0" cellpadding="0">
   	<tr>
   		<td align="left">
      		<a href="javascript:window.history.back()">
          		<img src="../imagenes/back.gif" border="0" alt="Pagina Anterior"/>
      		</a>
   		</td>
   		<td align="right">
      	<a href="../Inicio.asp">
          	<img src="../Imagenes/inicio.gif" border="0" alt="Inicio"/>
      	</a>
   		</td>
   	</tr>
   </table>   
   <center>
       <table border="0" cellpadding="0" width="90%">
            <xsl:apply-templates/>
       </table>
   </center>
</xsl:template>

<xsl:template match="LBLFOOTER">
   <font face="Arial" size="1">
      <xsl:apply-templates/>
   </font>
   <br/>
</xsl:template>

</xsl:stylesheet>


