<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

	
	<xsl:variable name="cTitular"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_TITULO"/></xsl:variable>
	<xsl:variable name="dFecha1"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_FEC_INI"/></xsl:variable>
	<xsl:variable name="dFecha2"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_FEC_FIN"/></xsl:variable>
	<xsl:variable name="cHora"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_HORA"/></xsl:variable>
	<xsl:variable name="cLugar"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_LUGAR"/></xsl:variable>
	<xsl:variable name="cMemo"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_MEMO"/></xsl:variable>
	<xsl:variable name="cImagenDelEvento"><xsl:value-of select="RAIZ/EVENTO/ROWS/ROW/@EV_IMAGEN"/></xsl:variable>
	
	<xsl:element name="body">
		<xsl:attribute name="vLink">#000000</xsl:attribute>
		<xsl:attribute name="aLink">#000000</xsl:attribute>
		<xsl:attribute name="link">#000000</xsl:attribute>
		<xsl:attribute name="bgColor">#FFFFFF</xsl:attribute>
   
   <center>
	   <table border="0" width="100%" >
  			<tr>
    			<td align="center" width="100%" bgcolor="#ffffff" valign="top">
					<xsl:if test="string-length($cImagenDelEvento)!=0">
						<table width="90%" border="1"  cellpadding="3" cellspacing="3" bgcolor="#08498C" >
							<tr>
								<td align="center" valign="top">
									<font face="Verdana" size="2" color="#FFCC00">
										<b><xsl:value-of select="$cTitular"/></b>
									</font>
								</td>
								<td  valign="top">
									<xsl:element name="img">
										<xsl:attribute name="border">0</xsl:attribute>
										<xsl:attribute name="src"><xsl:value-of select="$cImagenDelEvento"/></xsl:attribute>
										<xsl:attribute name="alt"><xsl:value-of select="$cImagenDelEvento"/></xsl:attribute>
									</xsl:element>
								</td>
							</tr>
						</table>		
					</xsl:if>
									
					<xsl:if test="string-length($cImagenDelEvento)=0">
						<table width="90%" border="0" cellpadding="3" cellspacing="3" bgcolor="#08498C">
							<tr>
								<td align="center">
									<font face="Verdana" size="2" color="#FFCC00">
										<b><xsl:value-of select="$cTitular"/></b>
									</font>
								</td>
							</tr>
						</table>					
					</xsl:if>
										
				</td>
			</tr>
		</table>
		<xsl:element name="br"/>
		<table width="90%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
			<tr>
				<td width="20%">
					<font size="1" face="verdana">
						<b>Fecha:</b>
					</font>
				</td>
				<td width="80%">
					<font size="2" face="verdana">
						<xsl:value-of select="$dFecha1"/> 
							<xsl:if test="$dFecha2 &gt; $dFecha1">
							 al <xsl:value-of select="$dFecha2"/>
							</xsl:if>
					</font> 
				</td>
			</tr>
			<tr>
    			<td width="20%">
    				<font size="1" face="verdana">
    					<b>Hora:</b>
    				</font>	
    			</td>
    			<td width="80%">
    				<font size="2" face="verdana">
    					<xsl:value-of select="$cHora"/>
    				</font>
    			</td>
  			</tr>
  			<tr>
    			<td width="20%">
    				<font size="1" face="verdana">
    					<b>Lugar</b>
    				</font>	
    			</td>
    			<td width="80%">
    				<font size="2" face="verdana">
    					<xsl:value-of select="$cLugar"/>
    				</font>
    			</td>
  			</tr>
		</table>
   <xsl:element name="br"/>
  </center>	
	<!--<table width="90%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
  		<tr>
   			<td width="100%">
   				<font size="2" face="Verdana">
   					<xsl:value-of select="$cMemo"/>
   				</font>
   			</td>
  		</tr>
  		<xsl:if test="string-length($cURL)!=0">
  			<tr>
  				<td width="100%" bgcolor="#ffffff">
  					<xsl:element name="br"/>
  					<font size="1" face="Verdana" class="Subtitulo">
  						<b>Para mas informacion visite:</b>   			
  							<xsl:element name="a"> 
  								<xsl:attribute name="href"><xsl:value-of select="$cURL"/></xsl:attribute>
  								<xsl:attribute name="target">_blank<xsl:value-of select="$cURL"/></xsl:attribute>
  								<xsl:value-of select="$cURL"/>
  							</xsl:element>	
  					</font>				
  				</td>
			</tr>
		</xsl:if>
	</table>
	
   
	<xsl:element name="br"/>

	<table width="90%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="left">
 				<xsl:element name="a">
 					<xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
 					<xsl:element name="img">
 						<xsl:attribute name="src">../IMAGENES/back.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Pagina Anterior</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
 			</td>
     		<td align="center">
     			<xsl:element name="a">
 					<xsl:attribute name="href">eventos100.asp</xsl:attribute>
					<xsl:element name="img">
 						<xsl:attribute name="src">../IMAGENES/maseventos.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Eventos Anteriores</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
			</td>
     		<td align="right">
     			<xsl:element name="a">
 					<xsl:attribute name="href">../inicio.asp</xsl:attribute>
					<xsl:element name="img">
 						<xsl:attribute name="src">../imagenes/inicio.gif</xsl:attribute> 
 						<xsl:attribute name="border">0</xsl:attribute>
 						<xsl:attribute name="alt">Inicio</xsl:attribute>
 					</xsl:element>
 				</xsl:element>	
			</td>
		</tr>
	</table>
	</center>
	<center>
  	<table border="0" cellpadding="2" width="90%">
    	<tr>
      		<td width="100%" align="center" valign="bottom">
      			<xsl:element name="br"/>
      			<xsl:element name="hr"/>
      			<xsl:element name="img">
      				<xsl:attribute name="src">../imagenes/tress_small.gif</xsl:attribute> 
 					<xsl:attribute name="border">0</xsl:attribute>
 				</xsl:element>
	  			<xsl:element name="br"/>
      			<font face="Arial" size="1">Portal Empresarial<xsl:element name="br"/>
      			<xsl:value-of select="RAIZ/GLOBAL/@G6"/> 	
      			<xsl:element name="a">
   						<xsl:attribute name="href"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 						<xsl:attribute name="target">"_new"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
 						<xsl:element name="br"/>
 						<xsl:value-of select="RAIZ/GLOBAL/@G19"/> 	
 				</xsl:element>		
      				
 				</font>	
			</td>			
    	</tr>
  	</table>
   </center>-->
	</xsl:element>
	
</xsl:template>

</xsl:stylesheet>