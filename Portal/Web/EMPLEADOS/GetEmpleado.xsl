<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
  <xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">../Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
	</xsl:element>

  <center>
	       <table border="0" width="90%" height="100%">
            <tr>
              <td valign="top">
              
                <!--<table bgcolor="#A5BACE" width="100%">-->
                <table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#A5BACE">
                  <tr>
                    <td>
                      <font face="Arial" size="2">
                        <b>Empleado: <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_CODIGO"/></b>
                      </font>
                    </td>
                  </tr>
                </table>
                
                
               <!--<table border="0" width="100%" cellpadding="2" cellspacing="1" bgcolor="#CECFCE">-->
               <table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
                  <tr>
                    <td valign="top">
                      <!--<table border="0" width="100%" cellpadding="2" cellspacing="1" bgcolor="#CECFCE">-->
                      <table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
                          <tr>
                            <td>Empleado:</td>
                            <td>
                              <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_APE_PAT"/><xsl:text>  </xsl:text>
                              <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_APE_MAT"/><xsl:text>, </xsl:text>
   		              		   <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_NOMBRES"/></td>
                          </tr>
	   					  <tr>
                            <td>Empresa:</td>
                            <td>
                              <xsl:value-of select="RAIZ/EMPLEADO/@EMPRESA"/>
                            </td>
                          </tr>
                          <tr>
                            <td>Turno:</td>
                            <td>
                              <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_TURNO"/>
                            </td>
                          </tr>
                          <tr>
                            <td>Puesto:</td>
                            <td>
                              <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_PUESTO"/>
                            </td>
                          </tr>
                          <tr>
                            <td>Clasificacion:</td>
                            <td>
                              <xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_CLASIFI"/>
                            </td>
                          </tr>
                          <xsl:for-each select="//NIVELES/NIVEL">
                            <tr>
                              <td><xsl:value-of select="@NOMBRE"/>:</td>
                              <td><xsl:value-of select="."/></td>
                            </tr>
                          </xsl:for-each>
                      </table>
                    </td>
                    <td align="right" valign="top">
                    <xsl:variable name="Foto"><xsl:value-of select="RAIZ/EMPLEADO/DATOS/@CB_FOTO"/></xsl:variable>
                 
                     <xsl:if test="string-length($Foto)!=0">
 							<img>
                        	<xsl:attribute name="src"><xsl:value-of select="$Foto"/></xsl:attribute>
							</img>
						</xsl:if>
                    </td>
                  </tr>
                </table>
<br>
</br>
<a><xsl:attribute name="href">javascript:window.history.back()</xsl:attribute>
<img>
<xsl:attribute name="src">../IMAGENES/back.gif</xsl:attribute>
<xsl:attribute name="border">0</xsl:attribute>
</img>
</a>

</td></tr>
</table>
 </center>
  </xsl:template>
</xsl:stylesheet>