<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
<xsl:element name="link">
		<xsl:attribute name="rel">stylesheet</xsl:attribute>
		<xsl:attribute name="href">Portal.css</xsl:attribute>
		<xsl:attribute name="type">text/css</xsl:attribute>
</xsl:element>

<xsl:element name="title">PORTAL-TRESS</xsl:element>

<xsl:variable name="lMostrarHeaderColl">True</xsl:variable>
<xsl:variable name="lMostrarHeaderCol">True</xsl:variable>
<!--<xsl:variable name="lMostrarTitulo1">True</xsl:variable>-->
<xsl:variable name="lMostrarTitulo2">1</xsl:variable>
<xsl:variable name="lMostrarTitulo3">1</xsl:variable>
<xsl:variable name="lMostrarTitulo4">1</xsl:variable>
<xsl:variable name="lMostrarTitulo5">1</xsl:variable>
<xsl:variable name="xColorBase">#FFFFFF</xsl:variable>
<xsl:variable name="xColorAlt">#B0C4DE</xsl:variable>
<xsl:variable name="xColor1"><xsl:value-of select="$xColorBase"/></xsl:variable>
<xsl:variable name="GL_FORMULA"><xsl:value-of select="$xColorBase"/></xsl:variable>

<xsl:element name="body">
	<xsl:attribute name="bgcolor">#FFFFFF</xsl:attribute>		
<center>
<table width="90%" border="0" cellpadding="0" cellspacing="0"> 
	<xsl:for-each select="//COLUMNA/SECCION">
		<xsl:variable name="TIPO"><xsl:value-of select="@CT_TIPO"/></xsl:variable>		
		<tr>
			<td>				
				
				<!-- *** REPORTE SECTION ***  -->
				<xsl:if test="$TIPO=0">
					<xsl:variable name="cTitulo"><xsl:value-of select="REPORTAL/@RP_TITULO"/></xsl:variable>
					<xsl:variable name="dFecha"><xsl:value-of select="REPORTAL/@RP_FECHA"/></xsl:variable>
					<xsl:variable name="dHora"><xsl:value-of select="REPORTAL/@RP_HORA"/></xsl:variable>
					<xsl:variable name="rFolio"><xsl:value-of select="REPORTAL/@RP_FOLIO"/></xsl:variable>
					<xsl:element name="br"/>
					<table bgcolor="#08498C" width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td align="left">
								<font face="Verdana" size="2" color="#FFCC00">
									<b><xsl:value-of select="$cTitulo"/></b>
								</font>
							</td>
							<td align="right">
								<font face="arial" size="1" color="#FFFFFF">
									<xsl:value-of select="$dFecha"/>, <xsl:value-of select="$dHora"/>
								</font>
							</td>
						</tr>
					</table>
					<!-- *** MOTOR DE REPORTES *** -->
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
  						<tr>
  							<td>
  								<p align="left"/>
			
								<xsl:element name="p"/>

								<xsl:variable name="COLS"><xsl:value-of select="REPORTAL/@RP_COLS"/></xsl:variable>
								<table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#acacac" bordercolorlight="#C5C3C3" bordercolordark="#ffffff" bgcolor="#E6E4E4">
								<xsl:for-each select="REP_DATA/LABELS">	
									<tr>		
										<xsl:if test="$COLS>=1">
											<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO1"/></b></font></td>
											<xsl:if test="$COLS>=2">
												<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO2"/></b></font></td>
												<xsl:if test="$COLS>=3">
													<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO3"/></b></font></td>
													<xsl:if test="$COLS>=4">						
														<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO4"/></b></font></td>
														<xsl:if test="$COLS>=5">
															<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO5"/></b></font></td>
															<xsl:if test="$COLS>=6">
																<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO6"/></b></font></td>
																<xsl:if test="$COLS>=7">
																	<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO7"/></b></font></td>
																	<xsl:if test="$COLS>=8">
																		<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO8"/></b></font></td>
																		<xsl:if test="$COLS>=9">
																			<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO9"/></b></font></td>
																			<xsl:if test="$COLS>=10">
																				<td align="center"><font size="2" face="Verdana"><b><xsl:value-of select="@RD_DATO10"/></b></font></td>
																			</xsl:if>												
																		</xsl:if>											
																	</xsl:if>
																</xsl:if>									
															</xsl:if>								
														</xsl:if>							
													</xsl:if>
												</xsl:if>					
											</xsl:if>								
										</xsl:if>		
									</tr>
								</xsl:for-each>
									
								<xsl:variable name="COLS2"><xsl:value-of select="REPORTAL/@RP_COLS"/></xsl:variable>

								<xsl:variable name="tipo1"><xsl:value-of select="REP_COLS/@RC_TIPO1"/></xsl:variable>					
								<xsl:variable name="tipo2"><xsl:value-of select="REP_COLS/@RC_TIPO2"/></xsl:variable>					
								<xsl:variable name="tipo3"><xsl:value-of select="REP_COLS/@RC_TIPO3"/></xsl:variable>					
								<xsl:variable name="tipo4"><xsl:value-of select="REP_COLS/@RC_TIPO4"/></xsl:variable>					
								<xsl:variable name="tipo5"><xsl:value-of select="REP_COLS/@RC_TIPO5"/></xsl:variable>					
								<xsl:variable name="tipo6"><xsl:value-of select="REP_COLS/@RC_TIPO6"/></xsl:variable>					
								<xsl:variable name="tipo7"><xsl:value-of select="REP_COLS/@RC_TIPO7"/></xsl:variable>					
								<xsl:variable name="tipo8"><xsl:value-of select="REP_COLS/@RC_TIPO8"/></xsl:variable>					
								<xsl:variable name="tipo9"><xsl:value-of select="REP_COLS/@RC_TIPO9"/></xsl:variable>					
								<xsl:variable name="tipo10"><xsl:value-of select="REP_COLS/@RC_TIPO10"/></xsl:variable>

								<xsl:variable name="varcorreo1"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO1"/></xsl:variable>
								<xsl:variable name="varcorreo2"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO2"/></xsl:variable>
								<xsl:variable name="varcorreo3"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO3"/></xsl:variable>
								<xsl:variable name="varcorreo4"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO4"/></xsl:variable>
								<xsl:variable name="varcorreo5"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO5"/></xsl:variable>
								<xsl:variable name="varcorreo6"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO6"/></xsl:variable>
								<xsl:variable name="varcorreo7"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO7"/></xsl:variable>
								<xsl:variable name="varcorreo8"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO8"/></xsl:variable>
								<xsl:variable name="varcorreo9"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO9"/></xsl:variable>
								<xsl:variable name="varcorreo10"><xsl:value-of select="REP_DATA/ROWS/ROW/@RD_DATO10"/></xsl:variable>
							
								<xsl:variable name="correo1"><xsl:value-of select="substring-after($varcorreo1, ':')"/></xsl:variable>
								<xsl:variable name="correo2"><xsl:value-of select="substring-after($varcorreo2, ':')"/></xsl:variable>
								<xsl:variable name="correo3"><xsl:value-of select="substring-after($varcorreo3, ':')"/></xsl:variable>				
								<xsl:variable name="correo4"><xsl:value-of select="substring-after($varcorreo4, ':')"/></xsl:variable>
								<xsl:variable name="correo5"><xsl:value-of select="substring-after($varcorreo5, ':')"/></xsl:variable>
								<xsl:variable name="correo6"><xsl:value-of select="substring-after($varcorreo6, ':')"/></xsl:variable>
								<xsl:variable name="correo7"><xsl:value-of select="substring-after($varcorreo7, ':')"/></xsl:variable>
								<xsl:variable name="correo8"><xsl:value-of select="substring-after($varcorreo8, ':')"/></xsl:variable>
								<xsl:variable name="correo9"><xsl:value-of select="substring-after($varcorreo9, ':')"/></xsl:variable>
								<xsl:variable name="correo10"><xsl:value-of select="substring-after($varcorreo10, ':')"/></xsl:variable>
			
								<xsl:for-each select="REP_DATA/ROWS/ROW">	
								<tr bgcolor="#ffffff">
									<xsl:if test="$COLS2>=1">
										<xsl:choose>
											<xsl:when test="$tipo1=1 or $tipo1=2">
												<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO1"/></font></td>
											</xsl:when>
											<xsl:when test="$tipo1=3">
												<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO1"/></font></td>						
											</xsl:when>
											<xsl:when test="$tipo1=7">
												<td align="left"><font size="1" face="Verdana">
													<xsl:element name="a">
														<xsl:attribute name="href"><xsl:value-of select="@RD_DATO1"/></xsl:attribute><xsl:value-of select="$correo1"/>
													</xsl:element></font>
												</td>						
											</xsl:when>
											<xsl:when test="$tipo1=8">
												<td align="left"><font size="1" face="Verdana">
													<xsl:element name="a">
														<xsl:attribute name="href"><xsl:value-of select="@RD_DATO1"/></xsl:attribute><xsl:value-of select="@RD_DATO1"/>
													</xsl:element></font>
												</td>						
											</xsl:when>						
											<xsl:otherwise>
												<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO1"/></font></td>												
											</xsl:otherwise>
										</xsl:choose>
								</xsl:if>
								<xsl:if test="$COLS2>=2">
									<xsl:choose>
										<xsl:when test="$tipo2=1 or $tipo2=2">
											<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO2"/></font></td>
										</xsl:when>
										<xsl:when test="$tipo2=3">
											<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO2"/></font></td>						
										</xsl:when>
										<xsl:when test="$tipo2=7">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO2"/></xsl:attribute><xsl:value-of select="$correo2"/>
												</xsl:element></font>
											</td>						
										</xsl:when>
										<xsl:when test="$tipo2=8">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO2"/></xsl:attribute><xsl:value-of select="@RD_DATO2"/>
												</xsl:element></font>
											</td>						
										</xsl:when>
						
										<xsl:otherwise>
											<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO2"/></font></td>												
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>		
								<xsl:if test="$COLS2>=3">
									<xsl:choose>
										<xsl:when test="$tipo3=1 or $tipo3=2">
											<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO3"/></font></td>
										</xsl:when>
										<xsl:when test="$tipo2=3">
											<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO3"/></font></td>						
										</xsl:when>
										<xsl:when test="$tipo3=7">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO3"/></xsl:attribute><xsl:value-of select="$correo3"/>
												</xsl:element></font>
											</td>						
										</xsl:when>
										<xsl:when test="$tipo3=8">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO3"/></xsl:attribute><xsl:value-of select="@RD_DATO3"/>
												</xsl:element></font>
											</td>						
										</xsl:when>						
										<xsl:otherwise>
											<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO3"/></font></td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="$COLS2>=4">
									<xsl:choose>
										<xsl:when test="$tipo4=1 or $tipo4=2">
											<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO4"/></font></td>
										</xsl:when>
										<xsl:when test="$tipo4=3">
											<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO4"/></font></td>
										</xsl:when>
										<xsl:when test="$tipo4=7">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO4"/></xsl:attribute><xsl:value-of select="$correo4"/>
												</xsl:element>
												</font>
											</td>						
										</xsl:when>
										<xsl:when test="$tipo4=8">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO4"/></xsl:attribute><xsl:value-of select="@RD_DATO4"/>								</xsl:element>
												</font>
											</td>						
										</xsl:when>						
										<xsl:otherwise>
											<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO4"/></font></td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="$COLS2>=5">
									<xsl:choose>
										<xsl:when test="$tipo5=1 or $tipo5=2">
											<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO5"/></font></td>
										</xsl:when>
										<xsl:when test="$tipo5=3">
											<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO5"/></font></td>						
										</xsl:when>
										<xsl:when test="$tipo5=7">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO5"/></xsl:attribute><xsl:value-of select="correo5"/>
												</xsl:element></font>
											</td>						
										</xsl:when>
										<xsl:when test="$tipo5=8">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO5"/></xsl:attribute><xsl:value-of select="@RD_DATO5"/>
												</xsl:element></font>
											</td>						
										</xsl:when>						
										<xsl:otherwise>
											<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO5"/></font></td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:if>
								<xsl:if test="$COLS2>=6">
									<xsl:choose>
										<xsl:when test="$tipo6=1 or $tipo6=2">
											<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO6"/></font></td>
										</xsl:when>
										<xsl:when test="$tipo6=3">
											<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO6"/></font></td>						
										</xsl:when>
										<xsl:when test="$tipo6=7">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO6"/></xsl:attribute><xsl:value-of select="$correo6"/>
												</xsl:element></font>
											</td>						
										</xsl:when>
										<xsl:when test="$tipo6=8">
											<td align="left"><font size="1" face="Verdana">
												<xsl:element name="a">
													<xsl:attribute name="href"><xsl:value-of select="@RD_DATO6"/></xsl:attribute><xsl:value-of select="@RD_DATO6"/>
												</xsl:element></font>
											</td>						
										</xsl:when>						
										<xsl:otherwise>
											<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO6"/></font></td>
										</xsl:otherwise>
									</xsl:choose>
							</xsl:if>
							<xsl:if test="$COLS2>=7">
								<xsl:choose>
									<xsl:when test="$tipo7=1 or $tipo7=2">
										<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO7"/></font></td>
									</xsl:when>
									<xsl:when test="$tipo7=3">
										<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO7"/></font></td>
									</xsl:when>
									<xsl:when test="$tipo7=7">
										<td align="left"><font size="1" face="Verdana">
											<xsl:element name="a">
												<xsl:attribute name="href"><xsl:value-of select="@RD_DATO7"/></xsl:attribute><xsl:value-of select="$correo7"/>
											</xsl:element></font>
										</td>						
									</xsl:when>
									<xsl:when test="$tipo7=8">
										<td align="left"><font size="1" face="Verdana">
											<xsl:element name="a">
												<xsl:attribute name="href"><xsl:value-of select="@RD_DATO7"/></xsl:attribute><xsl:value-of select="@RD_DATO7"/>
											</xsl:element></font>
										</td>						
									</xsl:when>						
									<xsl:otherwise>
										<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO7"/></font></td>
									</xsl:otherwise>
								</xsl:choose>
						</xsl:if>
						<xsl:if test="$COLS2>=8">
							<xsl:choose>
								<xsl:when test="$tipo8=1 or $tipo8=2">
									<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO8"/></font></td>
								</xsl:when>
								<xsl:when test="$tipo8=3">
									<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO8"/></font></td>
								</xsl:when>
								<xsl:when test="$tipo8=7">
									<td align="left"><font size="1" face="Verdana">
										<xsl:element name="a">
											<xsl:attribute name="href"><xsl:value-of select="@RD_DATO8"/></xsl:attribute><xsl:value-of select="correo8"/>
										</xsl:element></font>
									</td>						
								</xsl:when>
								<xsl:when test="$tipo8=8">
									<td align="left"><font size="1" face="Verdana">
										<xsl:element name="a">
											<xsl:attribute name="href"><xsl:value-of select="@RD_DATO8"/></xsl:attribute><xsl:value-of select="@RD_DATO8"/>
										</xsl:element></font>
									</td>						
								</xsl:when>						
								<xsl:otherwise>
									<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO8"/></font></td>
								</xsl:otherwise>
							</xsl:choose>
					</xsl:if>
					<xsl:if test="$COLS2>=9">
						<xsl:choose>
							<xsl:when test="$tipo9=1 or $tipo9=2">
								<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO9"/></font></td>
							</xsl:when>
							<xsl:when test="$tipo9=3">
								<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO9"/></font></td>						
							</xsl:when>
							<xsl:when test="$tipo9=7">
								<td align="left"><font size="1" face="Verdana">
									<xsl:element name="a">
										<xsl:attribute name="href"><xsl:value-of select="@RD_DATO9"/></xsl:attribute><xsl:value-of select="$correo9"/>
									</xsl:element></font>
								</td>						
							</xsl:when>
							<xsl:when test="$tipo9=8">
								<td align="left"><font size="1" face="Verdana">
									<xsl:element name="a">
										<xsl:attribute name="href"><xsl:value-of select="@RD_DATO9"/></xsl:attribute><xsl:value-of select="@RD_DATO9"/>
									</xsl:element></font>
								</td>						
							</xsl:when>						
							<xsl:otherwise>
								<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO9"/></font></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>
					<xsl:if test="$COLS2>=10">
						<xsl:choose>
							<xsl:when test="$tipo10=1 or $tipo10=2">
								<td align="right"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO10"/></font></td>
							</xsl:when>
							<xsl:when test="$tipo10=3">
								<td align="center"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO10"/></font></td>						
							</xsl:when>
							<xsl:when test="$tipo10=7">
								<td align="left"><font size="1" face="Verdana">
									<xsl:element name="a">
										<xsl:attribute name="href"><xsl:value-of select="@RD_DATO10"/></xsl:attribute><xsl:value-of select="$correo10"/>
									</xsl:element></font>
								</td>						
							</xsl:when>
							<xsl:when test="$tipo10=8">
								<td align="left"><font size="1" face="Verdana">
									<xsl:element name="a">
										<xsl:attribute name="href"><xsl:value-of select="@RD_DATO10"/></xsl:attribute><xsl:value-of select="@RD_DATO10"/>
									</xsl:element></font>
								</td>						
							</xsl:when>						
							<xsl:otherwise>
								<td align="left"><font size="1" face="Verdana"><xsl:value-of select="@RD_DATO10"/></font></td>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:if>		
					</tr>
					</xsl:for-each>
				</table>
  				</td>
			</tr>
		</table>
			
					<!-- *** FIN MOTOR *** -->
					<xsl:variable name="xCompleto"><xsl:value-of select="REPORTAL/@RP_COMPLETO"/></xsl:variable>
					<xsl:if test="$xCompleto=0">
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td align="right">
									<font face="Verdana" class="subtitulo">
										<i><xsl:element name="a">
												<xsl:attribute name="href">REPORTES/vreporte.asp?folio=<xsl:value-of select="$rFolio"/></xsl:attribute>
													Ver reporte completo...
											</xsl:element></i>
									</font>
								</td>
							</tr>
						</table>
					</xsl:if>
				</xsl:if>			
				<!-- *** END SECTION *** -->
				
				
				<!-- *** REPORTE GRAFICO ***  -->
				<xsl:if test="$TIPO=1">
					<xsl:if test="$lMostrarTitulo2=1">
						<table bgcolor="#08498C" width="100%" cellpadding="1" cellspacing="1">
							<tr>
								<td align="left">
									<xsl:element name="img">
									 	<xsl:attribute name="src">imagenes/reportegrafico.gif</xsl:attribute> 
									 	<xsl:attribute name="border">0</xsl:attribute>
									 </xsl:element>	
								</td>
							</tr>
						</table>
						<!--<xsl:value-of select="$lMostrarTitulo2=False"/>-->
					</xsl:if>	
				</xsl:if>
				<!-- *** END SECTION ***  -->
				
						
				<!-- *** TITULAR NOTE SECTION ***  -->
				<xsl:if test="$TIPO=2">
				
					<xsl:variable name="cMemo"><xsl:value-of select="TITULAR/@NT_CORTA"/></xsl:variable>
					<xsl:variable name="cTitular"><xsl:value-of select="TITULAR/@NT_TITULO"/></xsl:variable>
					<xsl:variable name="cAutor"><xsl:value-of select="TITULAR/@NT_AUTOR"/></xsl:variable>
					<xsl:variable name="cImagenDelTitular"><xsl:value-of select="TITULAR/@NT_IMAGEN"/></xsl:variable>
					<xsl:variable name="cFolio"><xsl:value-of select="TITULAR/@NT_FOLIO"/></xsl:variable>
					
					<xsl:if test="$lMostrarTitulo3=1">
						<table bgcolor="#08498C" width="100%" border="0">
							<tr>
								<td align="left">
									<font face="Verdana" size="2" color="White">
										<xsl:element name="a">
											<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
											<xsl:element name="img">
												<xsl:attribute name="src">imagenes/titular.gif</xsl:attribute>
												<xsl:attribute name="border">0</xsl:attribute>
											</xsl:element>
										</xsl:element>
										<!--<i><xsl:value-of select="$cSubTitular"/></i>-->
									</font>
								</td>
							</tr>
						</table>
					</xsl:if>
								
				<table width="100%" border="0" cellpadding="3" cellspacing="3">
					<tr>
						<xsl:if test="string-length($cImagenDelTitular)!=0">
						<td valign="top">
							<xsl:element name="a">
								<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
								<xsl:element name="img">
									<xsl:attribute name="border">0</xsl:attribute> 
									<xsl:attribute name="src"><xsl:value-of select="$cImagenDelTitular"/></xsl:attribute>
									<xsl:attribute name="ALT"><xsl:value-of select="$cImagenDelTitular"/></xsl:attribute>
								</xsl:element>
							</xsl:element>
						</td>
						</xsl:if>
						<td valign="top">
							<font size="2" face="Verdana" class="subtitulo">
								<xsl:element name="a">
									<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
									<b><xsl:value-of select="$cTitular"/></b>
								</xsl:element>
							</font>
							<xsl:element name="br"/>
							<font face="Verdana" size="2">
							<xsl:element name="p">
								<xsl:attribute name="align">justify</xsl:attribute>
							</xsl:element>	
							(<i>Nota de: <xsl:value-of select="$cAutor"/></i>)<xsl:value-of select="$cMemo"/> ... <font class="alfa">
								<xsl:element name="a">
									<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
									<b>Nota completa</b>
								</xsl:element>
								</font>
							</font>
						</td>
					</tr>
				</table>	
						
			 </xsl:if>
			 <!-- *** END SECTION ***  -->
			 
			 
			  <!-- *** NEWS SECTION ***  -->
			<xsl:if test="$TIPO=3">
 				<xsl:if test="$lMostrarTitulo4=1">
					<xsl:element name="br"/>
					<table bgcolor="#08498C" width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td align="left">
								<xsl:element name="a">
									<xsl:attribute name="href">NOTICIAS/noticias100.asp</xsl:attribute>
									<xsl:element name="img">
										<xsl:attribute name="src">IMAGENES/noticias.gif</xsl:attribute>
										<xsl:attribute name="border">0</xsl:attribute>
									</xsl:element>
								</xsl:element>
							</td>
						</tr>
					</table>
				</xsl:if>
										
				<table width="100%" cellpadding="1" cellspacing="1" border="0">
				
				<xsl:for-each select="NOTICIAS/ROWS/ROW">
					<xsl:variable name="cTitular"><xsl:value-of select="@NT_TITULO"/></xsl:variable>
					<xsl:variable name="dFecha"><xsl:value-of select="@NT_FECHA"/></xsl:variable>
					<xsl:variable name="cHora"><xsl:value-of select="@NT_HORA"/></xsl:variable>
              	<xsl:variable name="cAutor"><xsl:value-of select="@NT_AUTOR"/></xsl:variable>
              	<xsl:variable name="cNuevo"><xsl:value-of select="@HOT_NEWS"/></xsl:variable>
					<xsl:variable name="cFolio"><xsl:value-of select="@NT_FOLIO"/></xsl:variable>
					<xsl:variable name="xPar"><xsl:value-of select="@PAR"/></xsl:variable>
					
				 	<xsl:if test="$xPar=1">
						<tr bgcolor="#FFFFFF">
						<xsl:if test="$cNuevo=1">
							<td>
							<font face="Verdana" class="subtitulo">
								<b><xsl:element name="a">
										<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
										<xsl:element name="img">
											<xsl:attribute name="src">./imagenes/nueva.gif</xsl:attribute>
											<xsl:attribute name="border">0</xsl:attribute>
										</xsl:element>
									<xsl:value-of select="$cTitular"/>	
									</xsl:element></b></font></td>
						</xsl:if>
						<xsl:if test="$cNuevo=0">
							<td>
								<font face="Verdana" class="subtitulo">
									<b><xsl:element name="a">
										<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
										<xsl:value-of select="$cTitular"/>
									</xsl:element></b></font></td>			
						</xsl:if>

						</tr>
					</xsl:if>
					
					<xsl:if test="$xPar=0">	
						<tr bgcolor="#B0C4DE">
						<xsl:if test="$cNuevo=1">
							<td>
							<font face="Verdana" class="subtitulo">
								<b><xsl:element name="a">
										<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
										<xsl:element name="img">
											<xsl:attribute name="src">./imagenes/nueva.gif</xsl:attribute>
											<xsl:attribute name="border">0</xsl:attribute>
										</xsl:element>
									<xsl:value-of select="$cTitular"/>	
									</xsl:element></b></font></td>
						</xsl:if>						
						<xsl:if test="$cNuevo=0">
							<td>
								<font face="Verdana" class="subtitulo">
									<b><xsl:element name="a">
										<xsl:attribute name="href">noticias/vnoticia.asp?folio=<xsl:value-of select="$cFolio"/></xsl:attribute>
										<xsl:value-of select="$cTitular"/>
									</xsl:element></b></font></td>			
						</xsl:if>
						</tr>
					</xsl:if>	
					
				</xsl:for-each>
				</table>
				
			</xsl:if>
			<!-- *** END SECTION ***  -->
			
			<xsl:element name="br"/>
			 
			 <!-- *** EVENTS SECTION ***  -->
			<xsl:if test="$TIPO=4">
				<xsl:if test="$lMostrarTitulo5=1">
					<table bgcolor="#08498C" width="100%" cellpadding="1" cellspacing="1">
						<tr>
							<td align="left">
								<xsl:element name="a">
									<xsl:attribute name="href">EVENTOS/eventos100.asp</xsl:attribute>
									<xsl:element name="img">
										<xsl:attribute name="src">imagenes/calendariodeeventos.gif</xsl:attribute>
										<xsl:attribute name="border">0</xsl:attribute>
									</xsl:element>
								</xsl:element>	
							</td>
						</tr>
					</table>
				</xsl:if>
				
				<table width="100%" cellpadding="1" cellspacing="1" border="0">						
				<xsl:for-each select="EVENTOS/ROWS/ROW">
						<xsl:variable name="cTitular"><xsl:value-of select="@EV_TITULO"/></xsl:variable>
						<xsl:variable name="dFecha"><xsl:value-of select="@EV_FEC_INI"/></xsl:variable>
						<xsl:variable name="eFolio"><xsl:value-of select="@EV_FOLIO"/></xsl:variable>
						<xsl:variable name="xPar"><xsl:value-of select="@PAR"/></xsl:variable>
	
					<xsl:if test="$xPar=0">
						<tr bgcolor="#FFFFFF">	              		
						<td>
							<font face="Verdana" class="subtitulo" size="1">
								<b><xsl:element name="a">
										<xsl:attribute name="href">EVENTOS/veventos.asp?folio=<xsl:value-of select="$eFolio"/></xsl:attribute>
										<xsl:value-of select="$dFecha"/>:<xsl:text> </xsl:text><xsl:value-of select="$cTitular"/>
									</xsl:element>
								</b></font></td>
						</tr>
					</xsl:if>
					
					<xsl:if test="$xPar=1">
						<tr bgcolor="#B0C4DE">	              		
						<td>
							<font face="Verdana" class="subtitulo" size="1">
								<b><xsl:element name="a">
										<xsl:attribute name="href">EVENTOS/veventos.asp?folio=<xsl:value-of select="$eFolio"/></xsl:attribute>
										<xsl:value-of select="$dFecha"/>:<xsl:text> </xsl:text><xsl:value-of select="$cTitular"/>
									</xsl:element>
								</b></font></td>
						</tr>
					</xsl:if>

					
					</xsl:for-each>		
				</table>
			
			</xsl:if>
			<!-- *** END SECTION ***  -->			
				
			</td>
		</tr>
	</xsl:for-each>
</table>

	<table border="0" cellpadding="2" width="90%">
    	<tr>
      		<td width="100%" align="center" valign="bottom">
      			<xsl:element name="br"/>
      			<xsl:element name="hr"/>
      			<xsl:element name="img">
      				<!--<xsl:attribute name="src">imagenes/tress_small.gif</xsl:attribute>-->
      				<xsl:attribute name="src"><xsl:value-of select="RAIZ/GLOBAL/@G11"/></xsl:attribute> 
 					<xsl:attribute name="border">0</xsl:attribute>
 				</xsl:element>
	  			<xsl:element name="br"/>
      			<font face="Arial" size="1">Portal Empresarial<xsl:element name="br"/>
      					<!--Grupo Tress Internacional--><xsl:value-of select="RAIZ/GLOBAL/@G6"/><xsl:element name="br"/>
      				<xsl:element name="a">
	 					<!--<xsl:attribute name="href">http://www.tress.com.mx</xsl:attribute>-->
	 					<xsl:attribute name="href"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
	 					<!--<xsl:attribute name="target">"_new">http://www.tress.com.mx</xsl:attribute>
						http://www.tress.com.mx-->
						<xsl:attribute name="target">"_new"><xsl:value-of select="RAIZ/GLOBAL/@G19"/></xsl:attribute>
						<xsl:value-of select="RAIZ/GLOBAL/@G19"/>
 					</xsl:element>
 				</font>	
			</td>			
    	</tr>
  	</table>

</center>		
			
</xsl:element>
</xsl:template>
</xsl:stylesheet>

































