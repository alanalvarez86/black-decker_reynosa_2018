unit FUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  Buttons;

type
  TUsuarios = class(TBaseConsulta)
    PanelSuperior: TPanel;
    EditarContenido: TBitBtn;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure EditarContenidoClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
  end;

var
  Usuarios: TUsuarios;

implementation

uses DCliente,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaDialogo,
     ZetaBuscador;

{$R *.DFM}

{ TUsuarios }

procedure TUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
  //MA: Pendiente
  //HelpContext := H60612_Clasificaciones;
end;

procedure TUsuarios.Connect;
begin
  with dmCliente do
     begin
          cdsUsuarios.Conectar;
          DataSource.DataSet:= cdsUsuarios;
     end;
end;

procedure TUsuarios.Agregar;
begin
     dmCliente.cdsUsuarios.Agregar;
end;

procedure TUsuarios.Borrar;
begin
     dmCliente.cdsUsuarios.Borrar;
end;

procedure TUsuarios.Modificar;
begin
     dmCliente.cdsUsuarios.Modificar;
end;

procedure TUsuarios.Refresh;
begin
     dmCliente.cdsUsuarios.Refrescar;
end;

procedure TUsuarios.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Usuarios', 'US_CODIGO', dmCliente.cdsUsuarios );
end;

procedure TUsuarios.EditarContenidoClick(Sender: TObject);
begin
     inherited;
     if (  dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) then
        dmCliente.EditarContenidoUsuario
     else
         ZInformation('PortalCFG','No se tienen derechos para editar el contenido',0);
end;

procedure TUsuarios.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     EditarContenido.Enabled := not NoHayDatos;
end;

function TUsuarios.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar usuarios';
     Result := False;
end;

function TUsuarios.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar usuarios';
     Result := False;
end;

function TUsuarios.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar usuarios';
     Result := False;
end;

end.
