inherited EditUsuarios: TEditUsuarios
  Left = 364
  Top = 311
  Caption = 'Usuarios'
  ClientHeight = 173
  ClientWidth = 381
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 54
    Top = 44
    Width = 36
    Height = 13
    Caption = 'C�digo:'
  end
  object Label2: TLabel [1]
    Left = 50
    Top = 68
    Width = 40
    Height = 13
    Caption = 'Nombre:'
  end
  object Label3: TLabel [2]
    Left = 51
    Top = 91
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object Label4: TLabel [3]
    Left = 60
    Top = 115
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Clave:'
  end
  inherited PanelBotones: TPanel
    Top = 137
    Width = 381
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 213
    end
    inherited Cancelar: TBitBtn
      Left = 298
    end
  end
  inherited PanelSuperior: TPanel
    Width = 381
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 381
    TabOrder = 1
    Visible = False
    inherited Splitter: TSplitter
      Visible = False
    end
    inherited ValorActivo1: TPanel
      Visible = False
    end
    inherited ValorActivo2: TPanel
      Width = 55
      Visible = False
    end
  end
  object US_NOMBRE: TDBEdit [7]
    Left = 96
    Top = 64
    Width = 217
    Height = 21
    DataField = 'US_NOMBRE'
    DataSource = DataSource
    MaxLength = 30
    TabOrder = 3
  end
  object US_CODIGO: TZetaDBNumero [8]
    Left = 96
    Top = 40
    Width = 73
    Height = 21
    Mascara = mnDias
    TabOrder = 2
    Text = '0'
    ConfirmEdit = True
    ConfirmMsg = '� Desea Cambiar El N�mero De Este Usuario ?'
    DataField = 'US_CODIGO'
    DataSource = DataSource
  end
  object US_CORTO: TDBEdit [9]
    Left = 96
    Top = 87
    Width = 217
    Height = 21
    DataField = 'US_CORTO'
    DataSource = DataSource
    MaxLength = 15
    TabOrder = 4
  end
  object US_PASSWRD: TDBEdit [10]
    Left = 96
    Top = 111
    Width = 217
    Height = 21
    DataField = 'US_PASSWRD'
    DataSource = DataSource
    MaxLength = 15
    PasswordChar = '*'
    TabOrder = 5
  end
  inherited DataSource: TDataSource
    Left = 236
    Top = 57
  end
end
