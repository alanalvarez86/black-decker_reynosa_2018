unit FEditBuzon;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaEdit,
  ZetaHora, Mask, ZetaFecha, ZetaDBTextBox, ComCtrls, Psock, NMpop3, NMsmtp;

type
  TEditBuzon = class(TBaseEdicion)
    Titulo_Pn: TPanel;
    Label1: TLabel;
    BU_FOLIO: TZetaDBTextBox;
    Label4: TLabel;
    BU_TITULO: TZetaDBEdit;
    PageControl: TPageControl;
    Generales_Ts: TTabSheet;
    Label3: TLabel;
    Label5: TLabel;
    BU_USUARIO: TZetaDBTextBox;
    Label6: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    BU_HORA: TZetaDBHora;
    BU_AUTOR: TZetaDBEdit;
    BU_FECHA: TZetaDBFecha;
    BU_EMAIL: TZetaDBEdit;
    Descripcion_Ts: TTabSheet;
    BU_MEMO: TDBMemo;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditBuzon: TEditBuzon;

implementation

uses DCliente,DGlobal, ZetaBuscador, FDialogoImagenes;

{$R *.DFM}

{ TEditBuzon }
procedure TEditBuzon.FormCreate(Sender: TObject);
begin
  inherited;
  FirstControl := BU_TITULO;
  //MA:Pendiente
  //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditBuzon.Connect;
begin
     with dmCliente do
     begin
          cdsBuzon.Conectar;
          DataSource.DataSet:= cdsBuzon;
     end;
end;

procedure TEditBuzon.DoLookup;
begin
  inherited;
  ZetaBuscador.BuscarCodigo( 'Folio', 'Buz�n', 'BU_FOLIO', dmCliente.cdsBuzon );
end;

end.
