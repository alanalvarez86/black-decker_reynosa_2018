inherited Buzon: TBuzon
  Left = 310
  Top = 325
  Caption = 'Buz�n de Sugerencias'
  ClientWidth = 692
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 692
    inherited ValorActivo2: TPanel
      Width = 433
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 692
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'BU_FOLIO'
        Title.Caption = 'Folio'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BU_FECHA'
        Title.Caption = 'Fecha'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BU_HORA'
        Title.Caption = 'Hora'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BU_TITULO'
        Title.Caption = 'Titulo'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_NOMBRE'
        Title.Caption = 'Usuario'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BU_AUTOR'
        Title.Caption = 'Autor'
        Width = 150
        Visible = True
      end>
  end
end
