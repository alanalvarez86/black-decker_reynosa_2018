inherited ContenidoEditarGrafica: TContenidoEditarGrafica
  Left = 392
  Top = 336
  ActiveControl = Grafica
  Caption = 'Gr�fica'
  ClientWidth = 357
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GraficaLBL: TLabel [0]
    Left = 10
    Top = 22
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Gr�fica:'
  end
  inherited PanelBotones: TPanel
    Width = 357
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 189
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 274
    end
  end
  object Grafica: TZetaKeyLookup
    Left = 49
    Top = 19
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsReportes
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
  end
end
