unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBClient, Db, ComObj,
     {$ifndef VER130}Variants,MaskUtils,{$endif}
     DBasicoCliente,
     {$ifdef DOS_CAPAS}
     DServerPortal,
     {$else}
     Portal_TLB,
     {$endif}
     FCamposMgr,
     ZetaPortalClasses,
     ZetaClientDataSet,
     ZetaCommonLists;

const
     K_PRIMERA_PAGINA = 1;
     K_PRIMERA_COLUMNA = 1;
type
  eTipoReportal = ( eTipoReporte, eTipoGrafica );
  TdmCliente = class(TBasicoCliente)
    cdsNoticias: TZetaLookupDataSet;
    cdsTipoNoticias: TZetaLookupDataSet;
    cdsTipoEvento: TZetaLookupDataSet;
    cdsTipoDocumento: TZetaLookupDataSet;
    cdsUsuarios: TZetaLookupDataSet;
    cdsEventos: TZetaClientDataSet;
    cdsDocumentos: TZetaClientDataSet;
    cdsBuzon: TZetaClientDataSet;
    cdsContenidos: TZetaClientDataSet;
    cdsPaginas: TZetaClientDataSet;
    cdsReportes: TZetaLookupDataSet;
    cdsPantallas: TZetaClientDataSet;
    cdsSecuencias: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsTablasAlAdquirirDatos(Sender: TObject);
    procedure cdsNoticiasAlCrearCampos(Sender: TObject);
    procedure cdsTablaAlModificar(Sender: TObject);
    procedure cdsTablaBeforePost(DataSet: TDataSet);
    procedure cdsTablasAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure cdsTablasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsTablasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsTablasAfterDelete(DataSet: TDataSet);
    procedure cdsNoticiasNewRecord(DataSet: TDataSet);
    procedure cdsEventosNewRecord(DataSet: TDataSet);
    procedure cdsEventosAlCrearCampos(Sender: TObject);
    procedure cdsNoticiasAlAdquirirDatos(Sender: TObject);
    procedure cdsNoticiasBeforePost(DataSet: TDataSet);
    procedure cdsEventosAlAdquirirDatos(Sender: TObject);
    procedure cdsEventosBeforePost(DataSet: TDataSet);
    procedure cdsDocumentosAlCrearCampos(Sender: TObject);
    procedure cdsDocumentosAlAdquirirDatos(Sender: TObject);
    procedure cdsDocumentosBeforePost(DataSet: TDataSet);
    procedure cdsDocumentosNewRecord(DataSet: TDataSet);
    procedure cdsBuzonAlCrearCampos(Sender: TObject);
    procedure cdsBuzonAlAdquirirDatos(Sender: TObject);
    procedure cdsBuzonBeforePost(DataSet: TDataSet);
    procedure cdsBuzonNewRecord(DataSet: TDataSet);
    procedure cdsUsuariosBeforePost(DataSet: TDataSet);
    procedure cdsReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsReportesAlCrearCampos(Sender: TObject);
    procedure cdsReportesNewRecord(DataSet: TDataSet);
    procedure cdsReportesBeforePost(DataSet: TDataSet);
    procedure cdsReportesAfterOpen(DataSet: TDataSet);
    procedure cdsUsuariosLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsNoticiasAfterOpen(DataSet: TDataSet);
    procedure cdsNoticiasCalcFields(DataSet: TDataSet);
    procedure cdsReportesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FNoticias :Integer;
    FEventos :Integer;
    FDocumentos:Integer;
    FBuzon:Integer;
    FReportes :Integer;
    FListaContenidos: TListaContenidos;
    FRutaImagenGlobal: String;
    FFindBtnDocs: Integer;
    FFindBtnImagen: Integer;
    function ContenidoCargar(const iUsuario, iPagina,iColumna: Integer): Boolean;
    function DescargarPaginas: Boolean;
    function GetFechaDefault: TDate;
    function GetFolioMaximo(Dataset: TClientDataset; const sLlave: String): Integer;
    procedure SetFechaDefault(const Value: TDate);
    procedure ShowEdicion( const iTabla: Integer );
    procedure LeeTabla(Sender: TClientDataset);
    procedure EditarContenido( const iUsuario: Integer; const sNombre: String );
    procedure NT_TITULARGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  protected
    { Protected declarations }
    FFechaDefault: TDate;
    {$ifdef DOS_CAPAS}
    FServidor: TdmServerPortal;
    {$else}
    FServidor: IdmServerPortalDisp;
    function GetServidor: IdmServerPortalDisp;
    function CreateServer(const ClassID: TGUID): IDispatch;
    {$endif}
  public
    { Public declarations }
    {$ifdef DOS_CAPAS}
    property ServerPortal: TdmServerPortal read FServidor;
    {$else}
    property ServerPortal: IdmServerPortalDisp read GetServidor;
    {$endif}
    property ListaContenidos: TListaContenidos read FListaContenidos;
    property FechaDefault: TDate read GetFechaDefault write SetFechaDefault;
    property RutaImagenGlobal: String read FRutaImagenGlobal write FRutaImagenGlobal;
    property FindBtnImagen: Integer read FFindBtnImagen write FFindBtnImagen;
    property FindBtnDocs: Integer read FFindBtnDocs write FFindBtnDocs;
    function DescargarContenido: Boolean;
    function EditarPaginasUsuario: Boolean;
    function ContenidoRecargar(const iPagina,iColumna: Integer): Boolean;
    function GetAccesos: OleVariant;
    function GetValorActivoStr( const eTipo: TipoEstado ): String;override;
    function GetGrupoActivo: Integer;
    procedure EditarContenidoUsuario;
    procedure EditarContenidoDefault;
    procedure EditarGlobales;
    procedure SetTitular( const lSet: Boolean );
  end;

var
  dmCliente: TdmCliente;

implementation

{$R *.DFM}

uses ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaConnectionProblem,
     ZetaDialogo,
     ZetaMsgDlg,
     ZAccesosTress,
     ZBaseEdicion,
     ZReconcile,
     ZGlobalTress,
     ZetaRegistryCliente,
     DGlobal,
     FEditTablas,
     FEditNoticias,
     FEditEventos,
     FEditDocument,
     FEditBuzon,
     FEditUsuarios,
     FGlobalEdit,
     FContenidoCFG,
     FPaginaCFG,
     FUsuarios,
     FEditReportes;

const K_RP_T_DESC = 10;

var
  EditTipoNoticias : TTOEditTipoNoticias;
  EditTipoEvento : TTOEditTipoEvento;
  EditTipoDocumento : TTOEditTDocumento;

{ TdmPortal }

{$ifndef DOS_CAPAS}
function TdmCliente.CreateServer(const ClassID: TGUID): IDispatch;
var
   lLoop: Boolean;
begin
     lLoop := False;
     repeat
           Result := nil;
           try
              Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
           except
                 on Error: Exception do
                 begin
                      case ZetaConnectionProblem.ConnectionProblemDialog( Error ) of
                           mrRetry: lLoop := True;
                      else
                          lLoop := False;
                      end;
                 end;
           end;
     until not lLoop;
end;

function TdmCliente.GetServidor: IdmServerPortalDisp;
begin
     Result := IdmServerPortalDisp( CreateServer( CLASS_dmServerPortal ) );
end;
{$endif}

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     FFechaDefault := Date;
     FListaContenidos := TListaContenidos.Create;
     {$ifdef DOS_CAPAS}
     FServidor := TdmServerPortal.Create( Self );
     {$endif}
     cdsTipoNoticias.Tag := K_TAG_TIPO_NOTICIAS;
     cdsTipoEvento.Tag := K_TAG_TIPO_EVENTO;
     cdsTipoDocumento.Tag := K_TAG_TIPO_DOCUMENTO;
     cdsNoticias.Tag := K_TAG_NOTICIAS;
     cdsEventos.Tag := K_TAG_EVENTOS;
     cdsDocumentos.Tag := K_TAG_DOCUMENT;
     cdsBuzon.Tag := K_TAG_BUZON;
     cdsUsuarios.Tag := K_TAG_USUARIOS;
     cdsReportes.Tag := K_TAG_REPORTES;
     FindBtnImagen := 17;
     FindBtnDocs := 18;
     inherited;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServidor );
     {$endif}
     FreeAndNil( FListaContenidos );
end;

function TdmCliente.GetFechaDefault: TDate;
begin
     Result := Trunc( FFechaDefault );
end;

procedure TdmCliente.SetFechaDefault(const Value: TDate);
begin
     if ( FFechaDefault <> Value ) then
     begin
          FFechaDefault := Value;
     end;
end;

procedure TdmCliente.LeeTabla( Sender: TClientDataset );
begin
     with Sender do
     begin
          Data := ServerPortal.GetTabla( Tag );
     end;
end;

function TdmCliente.GetFolioMaximo( Dataset: TClientDataset; const sLlave: String ): Integer;
begin
     Result := 0;
     with Dataset do
     begin
          while not Eof do
          begin
               Result := ZetaCommonTools.iMax( Result, FieldByName( sLlave ).AsInteger );
               Next;
          end;
          First;
     end;
end;

{ Compatibilidad con formas de Tress }

function TdmCliente.GetAccesos: OleVariant;
begin
     ZetaCommonTools.SetOLEVariantToNull( Result );
end;

function TdmCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     Result := VACIO;
end;

function TdmCliente.GetGrupoActivo : Integer;
begin
     Result := D_GRUPO_SIN_RESTRICCION;
end;

{ **************** Manejo de Client DataSets ************************ }

procedure TdmCliente.ShowEdicion( const iTabla: Integer );
begin
     case iTabla of
          K_TAG_TIPO_NOTICIAS: ZBaseEdicion.ShowFormaEdicion( EditTipoNoticias, TTOEditTipoNoticias );
          K_TAG_TIPO_EVENTO: ZBaseEdicion.ShowFormaEdicion( EditTipoEvento, TTOEditTipoEvento );
          K_TAG_TIPO_DOCUMENTO: ZBaseEdicion.ShowFormaEdicion( EditTipoDocumento, TTOEditTDocumento );
          K_TAG_NOTICIAS: ZBaseEdicion.ShowFormaEdicion( EditNoticias, TEditNoticias );
          K_TAG_EVENTOS: ZBaseEdicion.ShowFormaEdicion( EditEventos, TEditEventos );
          K_TAG_DOCUMENT: ZBaseEdicion.ShowFormaEdicion( EditDocumento, TEditDocumento );
          K_TAG_BUZON: ZBaseEdicion.ShowFormaEdicion( EditBuzon, TEditBuzon );
          K_TAG_USUARIOS : ZBaseEdicion.ShowFormaEdicion( EditUsuarios, TEditUsuarios );
          K_TAG_REPORTES : ZBaseEdicion.ShowFormaEdicion( EditReportes, TEditReportes );
     end;
end;

{ ********** cdsTablas ********* }

procedure TdmCliente.cdsTablasAlAdquirirDatos(Sender: TObject);
begin
      LeeTabla( TZetaClientDataSet( Sender ) );
end;

procedure TdmCliente.cdsTablaAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmCliente.cdsTablaBeforePost(DataSet: TDataSet);
begin
     if DataSet.Fields[ 0 ].IsNull then
        DB.DatabaseError( 'Folio No Puede Quedar Vac�o' );
end;

procedure TdmCliente.cdsTablasAfterDelete(DataSet: TDataSet);
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          Enviar;
     end;
end;

procedure TdmCliente.cdsTablasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( Changecount > 0 ) then
             Reconcile( ServerPortal.GrabaTabla( Tag, Delta, ErrorCount ) );
     end;
end;

{$ifdef VER130}
procedure TdmCliente.cdsTablasReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$else}
procedure TdmCliente.cdsTablasReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;
{$endif}

{ cdsNoticias }

procedure TdmCliente.cdsNoticiasAlAdquirirDatos(Sender: TObject);
begin
     Global.Conectar;
     LeeTabla( cdsNoticias );
     FNoticias := GetFolioMaximo( cdsNoticias, 'NT_FOLIO' );
end;

procedure TdmCliente.cdsNoticiasAlCrearCampos(Sender: TObject);
begin
     cdsTipoNoticias.Conectar;
     cdsUsuarios.Conectar;
     With cdsNoticias do
     begin
          CreateCalculated( 'NT_TITULAR', ftBoolean, 0 );
          CreateSimpleLookup( cdsTipoNoticias, 'TB_ELEMENT', 'NT_CLASIFI' );
          CreateSimpleLookup( cdsUsuarios,'US_NOMBRE','NT_USUARIO');
     end;
end;

procedure TdmCliente.cdsNoticiasAfterOpen(DataSet: TDataSet);
begin
     with cdsNoticias do
     begin
          MaskFecha( 'NT_FECHA' );
          MaskTime( 'NT_HORA' );
          FieldByName( 'NT_TITULAR' ).OnGetText := NT_TITULARGetText;
     end;
end;

procedure TdmCliente.NT_TITULARGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Assigned( Sender ) then
          begin
               if Sender.Dataset.IsEmpty then
                  Text := ''
               else
                   if Sender.AsBoolean then
                        Text := 'Si'
                   else
                        Text := '';
          end;
     end;
end;

procedure TdmCliente.cdsNoticiasCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'NT_TITULAR' ).AsBoolean := ( Global.GetGlobalInteger( K_GLOBAL_TITULAR ) = FieldByName( 'NT_FOLIO' ).AsInteger );//( Global.GetGlobalInteger( K_GLOBAL_TITULAR ) = FieldByName( 'NT_FOLIO' ).AsInteger );
     end;
end;

procedure TdmCliente.cdsNoticiasNewRecord(DataSet: TDataSet);
begin
     With DataSet do
     begin
          FieldByName( 'NT_FOLIO' ).AsInteger := FNoticias + 1;
          FieldByName( 'NT_FECHA' ).AsDateTime := DATE;
          FieldByName( 'NT_HORA' ).AsString := FormatDateTime( 'hhmm', Now );
     end;
end;

procedure TdmCliente.cdsNoticiasBeforePost(DataSet: TDataSet);
begin
     if ( Dataset.State in [ dsInsert ] ) then
        Inc( FNoticias );
     cdsNoticias.FieldByName( 'NT_USUARIO' ).AsInteger := dmCliente.Usuario;
end;

procedure TdmCliente.SetTitular( const lSet: Boolean );
var
   iTitular, iNoticia: Integer;

procedure WriteTitular( const iFolio: Integer );
var
   FParametros: TZetaParams;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FParametros := TZetaParams.Create;
        try
           FParametros.AddString( Format( '%d', [ K_GLOBAL_TITULAR ] ), Format( '%d', [ iFolio ] ) );
           with Global do
           begin
                Descargar( FParametros, False );
                {
                Conectar;
                }
           end;
        finally
               FreeAndNil( FParametros );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

begin
     iNoticia := cdsNoticias.FieldByName( 'NT_FOLIO' ).AsInteger;
     with Global do
     begin
          iTitular := Global.GetGlobalInteger( K_GLOBAL_TITULAR );
          if lSet then
             WriteTitular( iNoticia )
          else
              if ( iNoticia = iTitular ) then
                 WriteTitular( 0 );
     end;
end;

{ ********* cdsEventos ******** }

procedure TdmCliente.cdsEventosNewRecord(DataSet: TDataSet);
begin
     With DataSet do
     begin
          FieldByName('EV_FOLIO').AsInteger := FEventos + 1;
          FieldByName('EV_FEC_INI').AsDateTime := DATE;
          FieldByName('EV_FEC_FIN').AsDateTime := DATE;
          FieldByName('EV_HORA').AsString := FormatDateTime( 'hhmm', Now );
     end;
end;

procedure TdmCliente.cdsEventosAlCrearCampos(Sender: TObject);
begin
     cdsUsuarios.Conectar;
     cdsTipoEvento.Conectar;
     With cdsEventos do
     begin
          MaskFecha( 'EV_FEC_INI' );
          MaskFecha( 'EV_FEC_FIN' );
          MaskTime( 'EV_HORA' );
          CreateSimpleLookup( cdsTipoEvento, 'TB_ELEMENT', 'EV_CLASIFI' );
          CreateSimpleLookup( cdsUsuarios,'US_NOMBRE','EV_USUARIO');
     end;
end;

procedure TdmCliente.cdsEventosAlAdquirirDatos(Sender: TObject);
begin
     Global.Conectar;
     LeeTabla( cdsEventos );
     FEventos := GetFolioMaximo( cdsEventos, 'EV_FOLIO' );
end;

procedure TdmCliente.cdsEventosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
           if ( FieldByName('EV_FEC_INI').AsDateTime > FieldByName('EV_FEC_FIN').AsDateTime ) then
                DatabaseError( 'La Fecha de Inicio del Evento es Mayor a la Fecha de Fin');
           if ( State in [ dsInsert ] ) then
              Inc( FEventos );
           FieldByName( 'EV_USUARIO' ).AsInteger := dmCliente.Usuario;
     end;
end;

{ ********* cdsDocumentos ********** }

procedure TdmCliente.cdsDocumentosAlCrearCampos(Sender: TObject);
begin
     cdsUsuarios.Conectar;
     cdsTipoDocumento.Conectar;
     With cdsDocumentos do
     begin
          MaskFecha( 'DC_FECHA' );
          MaskTime( 'DC_HORA' );
          CreateSimpleLookup( cdsTipoDocumento, 'TB_ELEMENT', 'DC_CLASIFI' );
          CreateSimpleLookup( cdsUsuarios, 'US_NOMBRE', 'DC_USUARIO' );
     end;
end;

procedure TdmCliente.cdsDocumentosAlAdquirirDatos(Sender: TObject);
begin
     Global.Conectar;
     LeeTabla( cdsDocumentos );
     FDocumentos := GetFolioMaximo( cdsDocumentos, 'DC_FOLIO' );
end;

procedure TdmCliente.cdsDocumentosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
           if ( Dataset.State in [ dsInsert ] ) then
              Inc( FDocumentos );
           FieldByName('DC_USUARIO').AsInteger := dmCliente.Usuario;
     end;

end;

procedure TdmCliente.cdsDocumentosNewRecord(DataSet: TDataSet);
begin
     With DataSet do
     begin
          FieldByName('DC_FOLIO').AsInteger := FDocumentos + 1;
          FieldByName('DC_FECHA').AsDateTime := DATE;
          FieldByName('DC_HORA').AsString := FormatDateTime( 'hhmm', Now );
     end;
end;

{ ******** cdsBuzon ******** }

procedure TdmCliente.cdsBuzonAlCrearCampos(Sender: TObject);
begin
     cdsUsuarios.Conectar;
     With cdsBuzon do
     begin
          MaskFecha( 'BU_FECHA' );
          MaskTime( 'BU_HORA' );
          CreateSimpleLookup( cdsUsuarios, 'US_NOMBRE', 'BU_USUARIO' );
     end;
end;

procedure TdmCliente.cdsBuzonAlAdquirirDatos(Sender: TObject);
begin
     Global.Conectar;
     LeeTabla( cdsBuzon );
     FBuzon := GetFolioMaximo( cdsBuzon, 'BU_FOLIO' );
end;

procedure TdmCliente.cdsBuzonBeforePost(DataSet: TDataSet);
begin
     if ( Dataset.State in [ dsInsert ] ) then
        Inc( FBuzon );
     DataSet.FieldByName('BU_USUARIO').AsInteger := dmCliente.Usuario;   
end;

procedure TdmCliente.cdsBuzonNewRecord(DataSet: TDataSet);
begin
     With DataSet do
     begin
          FieldByName('BU_FOLIO').AsInteger := FBuzon + 1;
          FieldByName('BU_FECHA').AsDateTime := DATE;
          FieldByName('BU_HORA').AsString := FormatDateTime( 'hhmm', Now );
     end;
end;

{ ******* Administrador de Contenidos ********** }

procedure TdmCliente.EditarContenido( const iUsuario: Integer; const sNombre: String );
begin
     if ContenidoCargar( iUsuario, K_PRIMERA_PAGINA, K_PRIMERA_COLUMNA ) then
     begin
          ContenidoCFG := TContenidoCFG.Create( Self );
          try
             with ContenidoCFG do
             begin
                  Caption := Format( 'Editar Contenido De %s', [ sNombre ] );
                  ShowModal;
             end;
          finally
                 FreeAndNil( ContenidoCFG );
          end;
     end;
end;

procedure TdmCliente.EditarContenidoUsuario;
begin
     with cdsUsuarios do
     begin
          EditarContenido( FieldByName( 'US_CODIGO' ).AsInteger, FieldByName( 'US_NOMBRE' ).AsString );
     end;
end;

procedure TdmCliente.EditarContenidoDefault;
begin
     Global.Conectar;     
     EditarContenido( Global.GetGlobalInteger( K_GLOBAL_USER_DEFAULT ), 'Pagina Default' );
end;

function TdmCliente.ContenidoCargar( const iUsuario, iPagina, iColumna: Integer ): Boolean;
var
   oPaginas: OleVariant;
   oCursor: TCursor;
begin
     Result := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with cdsContenidos do
        begin
             Data := ServerPortal.GetContenidos( iUsuario, iPagina, iColumna, oPaginas );
        end;
        with cdsPaginas do
        begin
             Data := oPaginas;
             with FListaContenidos.Paginas do
             begin
                  Clear;
                  if IsEmpty then
                  begin
                       AddPagina( 1, 1, 1, '' );
                  end
                  else
                  begin
                       while not Eof do
                       begin
                            AddPagina( FieldByName( 'PA_ORDEN' ).AsInteger,
                                       FieldByName( 'PA_ANTERIOR' ).AsInteger,
                                       FieldByName( 'PA_COLS' ).AsInteger,
                                       FieldByName( 'PA_TITULO' ).AsString );
                            Next;
                       end;
                  end;
             end;
        end;
        with cdsContenidos do
        begin
             if IsEmpty then
             begin
                  with FListaContenidos do
                  begin
                       Clear;
                       Usuario := iUsuario;
                       PaginaActiva := iPagina;
                       ColumnaActiva := iColumna;
                  end;
             end
             else
             begin
                  with FListaContenidos do
                  begin
                       Clear;
                       Usuario := iUsuario;
                       PaginaActiva := iPagina;
                       ColumnaActiva := iColumna;
                       while not Eof do
                       begin
                            AddContenido( eContenido( FieldByName( 'CT_TIPO' ).AsInteger ),
                                          FieldByName( 'CT_ORDEN' ).AsInteger,
                                          FieldByName( 'CT_MOSTRAR' ).AsInteger,
                                          FieldByName( 'CT_REPORTE' ).AsInteger,
                                          FieldByName( 'CT_CLASIFI' ).AsString,
                                          FieldByName( 'CT_DESCRIP' ).AsString );
                            Next;
                       end;
                  end;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmCliente.ContenidoRecargar( const iPagina, iColumna: Integer ): Boolean;
begin
     Result := ContenidoCargar( FListaContenidos.Usuario, iPagina, iColumna );
end;

function TdmCliente.DescargarContenido: Boolean;
var
   i, iErrores: Integer;
   oDelta: OleVariant;
begin
     Result := False;
     with cdsContenidos do
     begin
          EmptyDataset;
          MergeChangeLog;
          with FListaContenidos do
          begin
               Ordena;
               for i := 0 to ( Count - 1 ) do
               begin
                    Append;
                    with Contenido[ i ] do
                    begin
                         FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                         FieldByName( 'PA_ORDEN' ).AsInteger := PaginaActiva;
                         FieldByName( 'CT_COLUMNA' ).AsInteger := ColumnaActiva;
                         FieldByName( 'CT_TIPO' ).AsInteger := Ord( Tipo );
                         FieldByName( 'CT_ORDEN' ).AsInteger := Orden;
                         FieldByName( 'CT_MOSTRAR' ).AsInteger := Mostrar;
                         FieldByName( 'CT_REPORTE' ).AsInteger := Reporte;
                         FieldByName( 'CT_CLASIFI' ).AsString := Clase;
                    end;
                    Post;
               end;
               if ( ChangeCount > 0 ) then
                   oDelta := Delta
               else
                   oDelta := Null;
               Reconcile( ServerPortal.GrabaContenidos( oDelta, Usuario, PaginaActiva, ColumnaActiva, iErrores ) );
               if ( iErrores = 0 ) then
                  Result := True
               else
                   ContenidoRecargar( PaginaActiva, ColumnaActiva );
          end;
     end;
end;

function TdmCliente.DescargarPaginas: Boolean;
var
   i, iErrores: Integer;
begin
     Result := False;
     with cdsPaginas do
     begin
          EmptyDataset;
          MergeChangeLog;
          with FListaContenidos do
          begin
               with Paginas do
               begin
                    Ordena;
                    for i := 0 to ( Count - 1 ) do
                    begin
                         Append;
                         FieldByName( 'US_CODIGO' ).AsInteger := Usuario;
                         with Pagina[ i ] do
                         begin
                              FieldByName( 'PA_ORDEN' ).AsInteger := Orden;
                              FieldByName( 'PA_COLS' ).AsInteger := Columnas;
                              FieldByName( 'PA_TITULO' ).AsString := Titulo;
                              FieldByName( 'PA_ANTERIOR' ).AsInteger := Anterior;
                         end;
                         Post;
                    end;
               end;
               if ( ChangeCount > 0 ) then
               begin
                    Reconcile( ServerPortal.GrabaPaginas( Delta, Usuario, iErrores ) );
                    Result := ContenidoRecargar( K_PRIMERA_PAGINA, K_PRIMERA_COLUMNA );
                    if Result then
                       cdsUsuarios.Refrescar;
               end;
          end;
     end;
end;

function TdmCliente.EditarPaginasUsuario: Boolean;
var
   oCursor: TCursor;
begin
     Result := False;
     PaginaCFG := TPaginaCFG.Create( Self );
     try
        with PaginaCFG do
        begin
             Conectar;
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  oCursor := Screen.Cursor;
                  Screen.Cursor := crHourglass;
                  try
                     with FListaContenidos.Paginas do
                     begin
                          Merge( ListaPaginas );
                          Result := DescargarPaginas;
                     end;
                  finally
                         Screen.Cursor := oCursor;
                  end;
             end;
        end;
     finally
            FreeAndNil( PaginaCFG );
     end;
end;


{ ***** cdsUsuarios ******* }

procedure TdmCliente.cdsUsuariosLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     if ( StrtoIntDef( sKey, 0) = 0 ) then
     begin
          sDescription := 'Usuario Default';
          lOk := True;
     end
     else
     begin
          With Sender do
          begin
               OnLookupKey := nil;
               lOk := LookupKey( sKey, sFilter, sDescription );
               OnLookupKey := cdsUsuariosLookupKey;
          end;
     end;
end;

procedure TdmCliente.cdsUsuariosBeforePost(DataSet: TDataSet);
begin
     With Dataset do
     begin
          if ( FieldByName( 'US_CODIGO' ).AsInteger <= 0 ) then
             DB.DatabaseError( 'C�digo De Usuario Inv�lido' );
          if ( StrVacio( FieldByName( 'US_NOMBRE' ).AsString ) ) then
             DB.DatabaseError( 'El Nombre Del Usuario No Puede Estar Vac�o' );
     end;
end;

{ ******** cdsReportes ********* }

procedure TdmCliente.cdsReportesAlAdquirirDatos(Sender: TObject);
begin
     Global.Conectar;
     LeeTabla( cdsReportes );
     FReportes := GetFolioMaximo( cdsReportes, 'RP_FOLIO' );
end;

procedure TdmCliente.cdsReportesAlCrearCampos(Sender: TObject);
begin
     cdsUsuarios.Conectar;
     with cdsReportes do
     begin
          CreateSimpleLookup( cdsUsuarios, 'US_NOMBRE', 'RP_USUARIO' );
          CreateCalculated('RP_T_DESC', ftString, K_RP_T_DESC);
     end;     
end;

procedure TdmCliente.cdsReportesNewRecord(DataSet: TDataSet);
begin
     With cdsReportes do
     begin
          FieldByName('RP_TIPO').asInteger := Ord( eTipoReporte );
          FieldByName('RP_FOLIO').AsInteger := FReportes + 1;
     end;
end;

procedure TdmCliente.cdsReportesBeforePost(DataSet: TDataSet);
begin
     With DataSet do
     begin
          if ( StrVacio( FieldByName( 'RP_TITULO' ).AsString ) ) then
             DB.DatabaseError( 'El Titulo No Puede Quedar Vac�o' );
          if ( State in [ dsInsert ] ) then
             Inc( FReportes );
     end;
end;


procedure TdmCliente.cdsReportesAfterOpen(DataSet: TDataSet);
begin
     With cdsReportes do
     begin
          MaskFecha('RP_FECHA');
          MaskTime('RP_HORA');
     end;
end;

procedure TdmCliente.cdsReportesCalcFields(DataSet: TDataSet);
begin
     with DataSet do
     begin
          with FieldByName('RP_T_DESC') do
          begin
               Case eTipoReportal( FieldByName('RP_TIPO').AsInteger ) of
                    eTipoReporte: AsString := 'Reporte';
                    eTipoGrafica: AsString := 'Gr�fica';
               else
                   AsString := '?';     
               end;
          end;
     end;
end;

{ ********** Globales ********* }

procedure TdmCliente.EditarGlobales;
var
   Forma: TGlobalEdit;
begin
     Global.Conectar;
     Forma := TGlobalEdit.Create( Self );
     try
        with Forma do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( Forma );
     end;
end;

end.
