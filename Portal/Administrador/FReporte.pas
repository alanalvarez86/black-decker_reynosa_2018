unit FReporte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TReportes = class(TBaseConsulta)
    ReportesDBG: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Reportes: TReportes;

implementation
uses DCliente, ZetaBuscador;

{$R *.DFM}

{ TFReportes }
procedure TReportes.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     // PENDIENTE : HelpContext := H60612_Clasificaciones;
end;

procedure TReportes.Connect;
begin
     with dmCliente do
     begin
          cdsReportes.Conectar;
          DataSource.DataSet:= cdsReportes;
     end;
end;

procedure TReportes.Agregar;
begin
     dmCliente.cdsReportes.Agregar;
end;

procedure TReportes.Borrar;
begin
     dmCliente.cdsReportes.Borrar;
end;

procedure TReportes.Modificar;
begin
     dmCliente.cdsReportes.Modificar;
end;

procedure TReportes.Refresh;
begin
     dmCliente.cdsReportes.Refrescar;
end;

procedure TReportes.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Reportes', 'RP_FOLIO', dmCliente.cdsReportes );
end;

end.
