unit FEditEventos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyLookup,
  ZetaEdit, ZetaHora, ZetaFecha, Mask, ZetaNumero, ZetaDBTextBox, ExtDlgs,
  ComCtrls;

type
  TEditEventos = class(TBaseEdicion)
    Label5: TLabel;
    Titulo_Pn: TPanel;
    Label4: TLabel;
    EV_TITULO: TZetaDBEdit;
    Eventos_Pc: TPageControl;
    Generales_Ts: TTabSheet;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EV_USUARIO: TZetaDBTextBox;
    NT_IMAGEN_Find: TSpeedButton;
    Label2: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    EV_URL_Find: TSpeedButton;
    EV_HORA: TZetaDBHora;
    EV_AUTOR: TZetaDBEdit;
    EV_CLASIFI: TZetaDBKeyLookup;
    EV_IMAGEN: TZetaDBEdit;
    EV_LUGAR: TZetaDBEdit;
    EV_FEC_INI: TZetaDBFecha;
    EV_FEC_FIN: TZetaDBFecha;
    EV_URL: TZetaDBEdit;
    DescEv_TS: TTabSheet;
    EV_MEMO: TDBMemo;
    Panel1: TPanel;
    Find_Html_Btn: TSpeedButton;
    DlgFileOpen: TOpenDialog;
    Label1: TLabel;
    EV_FOLIO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure NT_IMAGE_FindClick(Sender: TObject);
    procedure EV_URL_FindClick(Sender: TObject);
    procedure Find_Html_BtnClick(Sender: TObject);
  private
      { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
    
  end;

var
  EditEventos: TEditEventos;

implementation

uses DCliente,DGlobal, ZetaBuscador, FDialogoImagenes, FTressShell ;

{$R *.DFM}

{ TEditEventos }
procedure TEditEventos.FormCreate(Sender: TObject);
begin
  inherited;
  FirstControl := EV_TITULO;
  EV_CLASIFI.LookupDataset := dmCliente.cdsTipoEvento;
  //MA:Pendiente
  //HelpContext:= H60617_Tipo_contrato;
end;

procedure TEditEventos.Connect;
begin
     with dmCliente do
        begin
             cdsEventos.Conectar;
             DataSource.DataSet:= cdsEventos;
        end;
end;

procedure TEditEventos.DoLookup;
begin
  inherited;
  ZetaBuscador.BuscarCodigo( 'Folio', 'Eventos', 'EV_FOLIO', dmCliente.cdsEventos );
end;

procedure TEditEventos.DataSourceStateChange(Sender: TObject);
begin
  inherited;
  if ( dmCliente.cdsEventos.State = dsInsert ) then
     EV_FOLIO.Enabled:=False;
end;

procedure TEditEventos.NT_IMAGE_FindClick(Sender: TObject);
begin
     with dmCliente do
     begin
          RutaImagenGlobal := Global.GetGlobalString( FindBtnImagen );
          with DialogoImagenes do
          begin
               LlamaDialogo(K_TIPO_IMAGEN,EV_IMAGEN.Text);
               if ( ModalResult = mrOk ) then
               begin
                    cdsEventos.Edit;
                    EV_IMAGEN.Text := ExtractFileName(ListaArchivos.FileName);
               end;
          end;
     end;
end;

procedure TEditEventos.EV_URL_FindClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        TressShell.ExecuteFile( EV_URL.Text, '', dmCliente.RutaImagenGlobal );
     finally
            Screen.Cursor := oCursor;
     end;       
end;

procedure TEditEventos.Find_Html_BtnClick(Sender: TObject);
begin
     with dlgFileOpen do
     begin
          with dmCliente do
          begin
               InitialDir := RutaImagenGlobal;
               if Execute then
               begin
                    with EV_MEMO do
                    begin
                         Clear;
                         cdsEventos.Edit;
                         Lines.LoadFromFile(dlgFileOpen.FileName);
                    end;
               end;
          end;
     end;
end;

end.
