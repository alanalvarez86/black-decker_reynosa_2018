unit ZetaDespConsulta;

interface
uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcTipoNoticia,
                     efcTipoEvento,
                     efcTipoDocumento,
                     efcUsuarios );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FTablas,
     FUsuarios,
     FEventos;


function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
          efcTipoNoticia  : Result := Consulta( K_SIN_RESTRICCION, TTOTipoNoticias );
          efcTipoEvento   : Result := Consulta( K_SIN_RESTRICCION, TTOTipoEvento );
          efcTipoDocumento: Result := Consulta( K_SIN_RESTRICCION, TTOTipoDocumento );
          efcUsuarios     : Result := Consulta( K_SIN_RESTRICCION, TUsuarios );
          else Result := Consulta( 0, nil );
     end;
end;

end.
