unit FContenidoEditarGrafica;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     FContenidoEditar, ZetaKeyLookup;

type
  TContenidoEditarGrafica = class(TContenidoEditar)
    GraficaLBL: TLabel;
    Grafica: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function GetFiltro: String;
  public
    { Public declarations }
  end;

var
  ContenidoEditarGrafica: TContenidoEditarGrafica;

implementation

uses DCliente,
     ZetaDialogo;

{$R *.DFM}

procedure TContenidoEditarGrafica.FormCreate(Sender: TObject);
begin
     inherited;         
     //HelpContext:= H00006_Catalogo_de_Diagnosticos;
end;

procedure TContenidoEditarGrafica.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := Grafica;
     Caption := GetCaption( 'Gr�fica' );
     with Contenido do
     begin
          with Self.Grafica do
          begin
               LookupDataset.Conectar;
               Filtro := GetFiltro;
               Valor := Reporte;
          end;
     end;
end;

function TContenidoEditarGrafica.GetFiltro: String;
begin
     Result := Format('( ( RP_USUARIO = 0 ) or ( RP_USUARIO = %d ) ) and ( RP_TIPO = %d )',[ Usuario, Ord( eTipoGrafica ) ] );
end;

procedure TContenidoEditarGrafica.OKClick(Sender: TObject);
begin
     inherited;
     if Grafica.Valor = 0 then
     begin
          ZetaDialogo.ZError( Caption, 'Gr�fica No Puede Quedar Vac�a', 0 );
          ActiveControl := Grafica;
     end
     else
     begin
          with Contenido do
          begin
               Reporte := Self.Grafica.Valor;
               Descripcion := Self.Grafica.Descripcion;
          end;
          ModalResult := mrOk;
     end;
end;

end.
