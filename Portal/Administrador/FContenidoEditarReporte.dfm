inherited ContenidoEditarReporte: TContenidoEditarReporte
  Left = 392
  Top = 336
  ActiveControl = Reporte
  Caption = 'Reporte'
  ClientWidth = 423
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ReporteLBL: TLabel [0]
    Left = 78
    Top = 11
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Reporte:'
  end
  object MostrarLBL: TLabel [1]
    Left = 8
    Top = 37
    Width = 111
    Height = 13
    Alignment = taRightJustify
    Caption = 'Renglones Por Mostrar:'
  end
  inherited PanelBotones: TPanel
    Width = 423
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 255
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 340
    end
  end
  object Reporte: TZetaKeyLookup
    Left = 122
    Top = 8
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsReportes
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
  end
  object Mostrar: TZetaNumero
    Left = 122
    Top = 32
    Width = 34
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
  end
end
