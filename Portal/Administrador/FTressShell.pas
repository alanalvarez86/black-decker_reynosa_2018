unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Buttons, ZetaSmartLists, ToolWin, Outlook, ExtCtrls, ZBaseConsulta,
  StdCtrls, Menus, ActnList, ShellAPI, ZBaseShell;

type
  TTressShell = class(TBaseShell)
    Panel5: TPanel;
    OutlookMenu: TOutlook;
    PanelPrincipal: TPanel;
    Panel3: TPanel;
    BarraBotones: TToolBar;
    ZetaSpeedButton1: TZetaSpeedButton;
    ZetaSpeedButton2: TZetaSpeedButton;
    ZetaSpeedButton3: TZetaSpeedButton;
    ToolButton1: TToolButton;
    ZetaSpeedButton5: TZetaSpeedButton;
    ToolButton2: TToolButton;
    Imprimir: TZetaSpeedButton;
    ToolButton3: TToolButton;
    PanelConsulta: TPanel;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _A_ImprimirForma: TAction;
    _A_ConfigurarImpresora: TAction;
    _A_Servidor: TAction;
    _A_SalirSistema: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    _E_Refrescar: TAction;
    _H_Contenido: TAction;
    _H_Glosario: TAction;
    _H_UsoTeclado: TAction;
    _H_ComoUsarAyuda: TAction;
    _H_AcercaDe: TAction;
    ShellMenu: TMainMenu;
    Archivo: TMenuItem;
    ArchivoImprimir: TMenuItem;
    ArchivoImprimirForma: TMenuItem;
    ArchivoImpresora: TMenuItem;
    N7: TMenuItem;
    ArchivoServidor: TMenuItem;
    N5: TMenuItem;
    MenuItem2: TMenuItem;
    Editar: TMenuItem;
    EditarAgregar: TMenuItem;
    EditarBorrar: TMenuItem;
    EditarModificar: TMenuItem;
    EditarSeparador2: TMenuItem;
    EditarRefrescar: TMenuItem;
    Ayuda: TMenuItem;
    MenuItem5: TMenuItem;
    AyudaGlosario: TMenuItem;
    AyudaCombinacionesTeclas: TMenuItem;
    AyudaUsandoAyuda: TMenuItem;
    AyudaSeparador2: TMenuItem;
    MenuItem6: TMenuItem;
    EscogeImpresora: TPrinterSetupDialog;
    Splitter2: TSplitter;
    Panel2: TPanel;
    LblFormato: TLabel;
    _E_BuscarCodigo: TAction;
    BuscarCodigoBtn: TZetaSpeedButton;
    N1: TMenuItem;
    EditarBuscarCodigo: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    procedure FormDestroy(Sender: TObject);
    procedure OutlookMenuItemClick(Sender: TObject; Item: String);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _A_ImprimirFormaExecute(Sender: TObject);
    procedure _A_ConfigurarImpresoraExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirSistemaExecute(Sender: TObject);
    procedure _H_ContenidoExecute(Sender: TObject);
    procedure _H_GlosarioExecute(Sender: TObject);
    procedure _H_UsoTecladoExecute(Sender: TObject);
    procedure _H_ComoUsarAyudaExecute(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _E_BuscarCodigoExecute(Sender: TObject);
  private
    { Private declarations }
    FFormaActiva: TBaseConsulta;
    function GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean ): TBaseConsulta;
    procedure SetFormaActiva(Forma: TBaseConsulta);
    procedure ActivaFormaPanel(InstanceClass: TBaseConsultaClass; const InstanceName: String );
    procedure SelectFormaOutLook(const sDescrip: String);
  public
    { Public declarations }
    function ExecuteFile(const FileName, Params,DefaultDir: String): THandle;
    property FormaActiva: TBaseConsulta read FFormaActiva;
  end;

var
  TressShell: TTressShell;

implementation

uses DCliente,
     DGlobal,
     ZAccesosMgr,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaRegistryCliente,
     ZetaDialogo,
     {$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor,
     {$else}
     FBuscaServidor,
     {$endif}
     FNoticias,
     FTablas,
     FUsuarios,
     FEventos,
     FDocumentos,
     FBuzon,
     FReporte,
     //FKiosko,
     FDialogoImagenes;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( self );
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     with Application do
     begin
          OnException := ManejaExcepcion;
          UpdateFormatSettings := FALSE;
     end;
     ZAccesosMgr.SetValidacion( FALSE );        // PENDIENTE: Cuando se programen derechos de acceso hay que poner esta bandera en TRUE;
     DialogoImagenes := TDialogoImagenes.Create( Self );
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     FreeAndNil( DialogoImagenes );
     dmCliente.Free;
     Global.Free;
     ZetaRegistryCliente.ClearClientRegistry;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
{$endif}
end;

procedure TTressShell.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZetaDialogo.ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

{ Activaci�n de Formas }

procedure TTressShell.OutlookMenuItemClick(Sender: TObject; Item: String);
begin
     LblFormato.Caption := Item;
     if ( Item = 'Globales' ) then
     begin
          if (  dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) then
             SelectFormaOutLook( Item )
          else
              ZInformation('PortalCFG','No se tienen derechos para editar el contenido',0);
     end
     else
         SelectFormaOutLook( Item );
end;

procedure TTressShell.SelectFormaOutLook( const sDescrip: String );
const
     K_FORM_NOTICIA = 'Noticias';
     K_FORM_EVENTO  = 'Eventos';
     K_FORM_DOCUMEN = 'Documentos';
     K_FORM_BUZON = 'Buz�n';
     K_FORM_REPORTES = 'Reportes';
     K_FORM_TIPO_NOTICIA = 'Tipo de Noticias';
     K_FORM_TIPO_EVENTO  = 'Tipo de Eventos';
     K_FORM_TIPO_DOCUMEN = 'Tipo de Documentos';
     K_FORM_USUARIOS = 'Usuarios';
     K_FOMR_USER_DEF = 'Usuario Default';
     K_FOMR_GLOBALES = 'Globales';
     K_FOMR_KIOSKO = 'Kiosco';
begin
     if ( sDescrip = K_FORM_NOTICIA ) then
        ActivaFormaPanel( TNoticias, 'Noticias' )
     else if ( sDescrip = K_FORM_TIPO_NOTICIA ) then
        ActivaFormaPanel( TTOTipoNoticias, 'TipoNoticias' )
     else if ( sDescrip = K_FORM_TIPO_EVENTO ) then
        ActivaFormaPanel( TTOTipoEvento, 'TipoEventos' )
     else if ( sDescrip = K_FORM_TIPO_DOCUMEN ) then
        ActivaFormaPanel( TTOTipoDocumento, 'TipoDocumentos' )
     else if ( sDescrip = K_FORM_EVENTO ) then
        ActivaFormaPanel( TEventos, 'Eventos' )
     else if ( sDescrip = K_FORM_DOCUMEN ) then
        ActivaFormaPanel( TDocumentos, 'Documentos' )
     else if ( sDescrip = K_FORM_BUZON ) then
        ActivaFormaPanel( TBuzon, 'Buzones' )
     else if ( sDescrip = K_FORM_REPORTES ) then
        ActivaFormaPanel( TReportes, 'Reportes' )
     else if ( sDescrip = K_FORM_USUARIOS ) then
        ActivaFormaPanel( TUsuarios, 'Usuarios' )
     else if ( sDescrip = K_FOMR_USER_DEF ) then
        dmCliente.EditarContenidoDefault
     else if ( sDescrip = K_FOMR_GLOBALES ) then
        dmCliente.EditarGlobales

     {else if ( sDescrip = K_FOMR_KIOSKO ) then
        ActivaFormaPanel( TKiosko, 'Kiosco' )}

end;

function TTressShell.GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean): TBaseConsulta;
begin
     lNueva := FALSE;
     Result := TBaseConsulta( FindComponent( InstanceName ) );
     if not Assigned( Result ) then
     begin
          try
             Result := InstanceClass.Create( Self ) as TBaseConsulta;
             with Result do
             begin
                  Name := InstanceName;
                  Parent := Self.PanelConsulta;
             end;
             lNueva := TRUE;
          except
             Result := nil;
          end;
     end;
end;

procedure TTressShell.ActivaFormaPanel(InstanceClass: TBaseConsultaClass; const InstanceName: String);
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     Forma := GetForma( InstanceClass, InstanceName, lNueva );
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               //IndexDerechos := ; PENDIENTE: Constante de Derecho de Acceso
               try
                  if lNueva then
                     DoConnect
                  else
                     Reconnect;
                  Show;
                  SetFormaActiva( Forma );
               except
                    Free;
               end;
          end;
     end
     else
          zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
end;

procedure TTressShell.SetFormaActiva(Forma: TBaseConsulta);
begin
     if ( FFormaActiva <> Forma ) then
     begin
          if ( Assigned( FFormaActiva ) ) then
             FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
          FFormaActiva := Forma;
          if ( Assigned( FFormaActiva ) ) then
          begin
               with FFormaActiva do
               begin
                    _E_Agregar.Enabled := CheckDerechos( K_DERECHO_ALTA );
                    _E_Borrar.Enabled := CheckDerechos( K_DERECHO_BAJA );
                    _E_Modificar.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
                    _A_Imprimir.Enabled := CheckDerechos( K_DERECHO_IMPRESION );
                    HelpContext:= FFormaActiva.HelpContext;
                    SetFocus;
               end;
          end
          else
          begin
               _E_Agregar.Enabled := FALSE;
               _E_Borrar.Enabled := FALSE;
               _E_Modificar.Enabled := FALSE;
               _A_Imprimir.Enabled := FALSE;
          end;
     end;
end;

{ Action List }

procedure TTressShell._A_ImprimirFormaExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._A_ConfigurarImpresoraExecute(Sender: TObject);
begin
     EscogeImpresora.Execute;
end;

procedure TTressShell._A_ServidorExecute(Sender: TObject);
var
   lOk: Boolean;
   {$ifndef DOS_CAPAS}
   sComputerName: String;
   {$endif}
begin
     inherited;
     {$ifdef DOS_CAPAS}
     lOk := ZetaRegistryServerEditor.EspecificaComparte;
     {$else}
     sComputerName := ClientRegistry.ComputerName;
     with TFindServidor.Create( Application ) do
     begin
          try
             Servidor := sComputerName;
             if ( ShowModal = mrOk ) then
             begin
                  lOk := True;
                  sComputerName := Servidor;
             end
             else
                 lOk := False;
          finally
                 Free;
          end;
     end;
     {$endif}
     if lOk then
     begin
          Application.ProcessMessages;
          {$ifndef DOS_CAPAS}
          ClientRegistry.ComputerName := sComputerName;
          {$endif}
          ZetaDialogo.ZInformation( '� Atenci�n !',
                                    'Para Conectarse Al Nuevo Servidor' +
                                    CR_LF +
                                   'Es Necesario Volver A Entrar Al Sistema', 0 );
          Close;
          Application.Terminate;
     end;
end;

procedure TTressShell._A_SalirSistemaExecute(Sender: TObject);
begin
     Close;
end;

procedure TTressShell._H_ContenidoExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_GlosarioExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_UsoTecladoExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_ComoUsarAyudaExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._H_AcercaDeExecute(Sender: TObject);
begin
     zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 );
end;

procedure TTressShell._E_AgregarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoInsert;
     end;
end;

procedure TTressShell._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoDelete;
     end;
end;

procedure TTressShell._E_ModificarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoEdit;
     end;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoRefresh;
     end;
end;

procedure TTressShell._A_ImprimirExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoPrint;
     end;
end;

procedure TTressShell._E_BuscarCodigoExecute(Sender: TObject);
begin
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoLookup;
     end;
end;

function TTressShell.ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;


end.
