unit FDocumentos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TDocumentos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  Documentos: TDocumentos;

implementation

uses DCliente, ZetaBuscador;

{$R *.DFM}

{ TDocumentos }
procedure TDocumentos.FormCreate(Sender: TObject);
begin
  inherited;
  CanLookup := True;
  // PENDIENTE : HelpContext := H60612_Clasificaciones;
end;

procedure TDocumentos.Agregar;
begin
     dmCliente.cdsDocumentos.Agregar;
end;

procedure TDocumentos.Borrar;
begin
     dmCliente.cdsDocumentos.Borrar;
end;

procedure TDocumentos.Connect;
begin
   with dmCliente do
     begin
          cdsTipoDocumento.Conectar;      
          cdsDocumentos.Conectar;
          DataSource.DataSet:= cdsDocumentos;
     end;
end;

procedure TDocumentos.Modificar;
begin
     dmCliente.cdsDocumentos.Modificar;
end;

procedure TDocumentos.Refresh;
begin
     dmCliente.cdsDocumentos.Refrescar;
end;

procedure TDocumentos.DoLookup;
begin
  inherited;
  ZetaBuscador.BuscarCodigo( 'Folio', 'Documentos', 'DC_FOLIO', dmCliente.cdsDocumentos );
end;

end.
