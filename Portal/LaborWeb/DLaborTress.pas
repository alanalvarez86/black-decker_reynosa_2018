unit DLaborTress;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,{$ifndef VER130}Variants,{$endif}
     DB, DBClient,
     DZetaServerProvider,
     ZCreator,
     ZetaCommonClasses,
     ZetaServerDataSet,
     DLaborCalculo;

type
  TdmLaborTress = class(TDataModule)
    cdsLista: TServerDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    oZetaCreator: TZetaCreator;
    procedure InitCreator;
  public
    { Public declarations }
    function GrabaDia(const Datos: WideString; out Mensaje: WideString): Boolean;
    procedure GrabaTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; Datos: OleVariant);
  end;

var
  dmLaborTress: TdmLaborTress;

implementation

uses DXMLTools,
     DQueries,
     ZetaSQLBroker,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonLists;

{$R *.dfm}

type
    eSQLScript = ( Q_TIEMPOS_BORRA,
                   Q_TIEMPOS_INSERTA,
                   Q_TIEMPOS_HAY_TARJETA );

{
alter table WORKS add WK_COMENTA Formula;
}

function GetSQLScript( const eScript: eSQLScript ): String;
begin
     case eScript of
          Q_TIEMPOS_BORRA: Result := 'delete from WORKS where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' )';
          Q_TIEMPOS_INSERTA: Result := 'insert into WORKS( ' +
                                       'CB_CODIGO, '+
                                       'OP_NUMBER, '+
                                       'WO_NUMBER, '+
                                       'AR_CODIGO, '+
                                       'CB_AREA,'+
                                       'WK_FECHA_R, '+
                                       'WK_HORA_R, '+
                                       'AU_FECHA, '+
                                       'WK_HORA_A, '+
                                       'WK_LINX_ID, '+
                                       'WK_TIPO, '+
                                       'WK_MOD_1, '+
                                       'WK_MOD_2, '+
                                       'WK_MOD_3, '+
                                       'WK_TMUERTO, '+
                                       'WK_MANUAL, '+
                                       'WK_PRE_CAL,'+
                                       'WK_COMENTA '+
                                       ') values ( '+
                                       ':CB_CODIGO, '+
                                       ':OP_NUMBER, '+
                                       ':WO_NUMBER, '+
                                       ':AR_CODIGO, '+
                                       ':CB_AREA, '+
                                       ':WK_FECHA_R, '+
                                       ':WK_HORA_R, '+
                                       ':AU_FECHA, '+
                                       ':WK_HORA_A, '+
                                       ':WK_LINX_ID, '+
                                       ':WK_TIPO, '+
                                       ':WK_MOD_1, '+
                                       ':WK_MOD_2, '+
                                       ':WK_MOD_3, '+
                                       ':WK_TMUERTO, '+
                                       ':WK_MANUAL, '+
                                       ':WK_PRE_CAL,'+
                                       ':WK_COMENTA )';
          Q_TIEMPOS_HAY_TARJETA: Result := 'select COUNT(*) CUANTOS from AUSENCIA where ( CB_CODIGO = %d ) and ( AU_FECHA = ''%s'' )';
     else
         Result := '';
     end;
end;

procedure TdmLaborTress.DataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmLaborTress.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmLaborTress.InitCreator;
begin
     if not Assigned( oZetaCreator ) then
        oZetaCreator := ZetaSQLBroker.GetZetaCreator( oZetaProvider );
end;

procedure TdmLaborTress.GrabaTiempos(Empresa: OleVariant; Empleado: Integer; Fecha: TDateTime; Datos: OleVariant);
var
   oLabor: TLCalculo;
   FDatosHorario: TDatosHorarioDiario;
   FDataset: TZetaCursor;
   sFecha: String;
   sHora: THoraLabor;
   iHora, iTiempo: Integer;
   lSinTarjeta: Boolean;
   TipoChecada: eTipoLectura;
begin
     sFecha := ZetaCommonTools.DateToStrSQL( Fecha );
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          InitCreator;
          InitGlobales;
          oLabor := TLCalculo.Create( oZetaCreator );
          try
             with oLabor do
             begin
                  CalcularTiemposBegin( Fecha, Fecha, cdsLista, FALSE );
                  try
                     FDatosHorario := GetDatosHorario( Empleado, Fecha );
                     with FDatosHorario do
                     begin
                          iHora := Entra24;
                     end;
                     FDataset := CreateQuery( Format( GetSQLScript( Q_TIEMPOS_HAY_TARJETA ), [ Empleado, sFecha ] ) );
                     try
                        with FDataset do
                        begin
                             Active := True;
                             lSinTarjeta := ( FieldByName( 'CUANTOS' ).AsInteger = 0 );
                             Active := False;
                        end;
                     finally
                            FreeAndNil( FDataset );
                     end;
                     if lSinTarjeta then { Hay que agregarle la tarjeta de Ausencia }
                     begin
                          InsertaTarjeta( Empleado, Fecha );
                     end;
                     FDataset := CreateQuery( GetSQLScript( Q_TIEMPOS_INSERTA ) );
                     try
                        EmpiezaTransaccion;
                        try
                           EjecutaAndFree( Format( GetSQLScript( Q_TIEMPOS_BORRA ), [ Empleado, sFecha ] ) );
                           with cdsLista do
                           begin
                                Lista := Datos;
                                while not Eof do
                                begin
                                     iTiempo := Trunc( FieldByName( 'WK_TIEMPO' ).AsInteger );
                                     iHora := iHora + iTiempo;
                                     sHora := DLaborCalculo.Minutes2Hour( iHora );
                                     if ( FindField( 'WK_TIPO' ) <> nil ) then
                                         TipoChecada := eTipoLectura( FieldByName( 'WK_TIPO' ).AsInteger )
                                     else
                                         TipoChecada := wtChecada;
                                     // Asignar Parametros del Query
                                     ParamAsInteger( FDataset, 'CB_CODIGO', Empleado );
                                     ParamAsDate( FDataset, 'AU_FECHA', Fecha );
                                     ParamAsChar( FDataset, 'WK_HORA_A', sHora, K_ANCHO_HH_MM );
                                     ParamAsDate( FDataset, 'WK_FECHA_R', Fecha );
                                     ParamAsChar( FDataset, 'WK_HORA_R', sHora, K_ANCHO_HH_MM );
                                     ParamAsVarChar( FDataset, 'OP_NUMBER', FieldByName( 'OP_NUMBER' ).AsString, K_ANCHO_OPERACION );
                                     ParamAsVarChar( FDataset, 'WO_NUMBER', FieldByName( 'WO_NUMBER' ).AsString, K_ANCHO_ORDEN );
                                     {$ifdef INTERRUPTORES}
                                     ParamAsVarChar( FDataset, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_PARTE20 );
                                     {$else}
                                     ParamAsVarChar( FDataset, 'AR_CODIGO', FieldByName( 'AR_CODIGO' ).AsString, K_ANCHO_PARTE );
                                     {$Endif}
                                     ParamAsVarChar( FDataset, 'WK_LINX_ID', 'OPERADOR', K_ANCHO_TERMINAL_ID );
                                     // Campo CB_AREA puede o no venir en cdsLista
                                     if ( FindField( 'CB_AREA' ) <> nil ) then
                                        ParamAsChar( FDataset, 'CB_AREA', FieldByName( 'CB_AREA' ).AsString, K_ANCHO_AREA )
                                     else
                                        ParamAsChar( FDataset, 'CB_AREA', ZetaCommonClasses.VACIO, K_ANCHO_AREA );
                                     ParamAsInteger( FDataset, 'WK_TIPO', Ord( TipoChecada ) );
                                     ParamAsChar( FDataset, 'WK_MOD_1', FieldByName( 'WK_MOD_1' ).AsString, K_ANCHO_MODULADOR );
                                     ParamAsChar( FDataset, 'WK_MOD_2', FieldByName( 'WK_MOD_2' ).AsString, K_ANCHO_MODULADOR );
                                     ParamAsChar( FDataset, 'WK_MOD_3', FieldByName( 'WK_MOD_3' ).AsString, K_ANCHO_MODULADOR );
                                     ParamAsChar( FDataset, 'WK_TMUERTO', FieldByName( 'WK_TMUERTO' ).AsString, K_ANCHO_TMUERTO );
                                     ParamAsBoolean( FDataset, 'WK_MANUAL', True );
                                     if ( TipoChecada <> wtChecada ) then
                                        ParamAsInteger( FDataset, 'WK_PRE_CAL', iTiempo )
                                     else
                                        ParamAsInteger( FDataset, 'WK_PRE_CAL', 0 );
                                     ParamAsVarChar( FDataset, 'WK_COMENTA', FieldByName( 'WK_COMENTA' ).AsString, K_ANCHO_FORMULA );
                                     Ejecuta( FDataset );
                                     Next;
                                end;
                           end;
                           CalcularTiemposEmpleado( Fecha, Empleado );
                           TerminaTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   //TerminaTransaccion( False );
                                   RollBackTransaccion;
                                   raise;
                              end;
                        end;
                     finally
                            FreeAndNil( FDataset );
                     end;
                  finally
                         CalcularTiemposEnd;
                  end;
             end;
          finally
                 FreeAndNil( oLabor );
          end;
     end;
end;

function TdmLaborTress.GrabaDia(const Datos: WideString; out Mensaje: WideString): Boolean;
const
     K_MINUTOS_HORA = 60;
var
   FXML: TdmXMLTools;
   Nodo: TZetaXMLNode;
   iEmpleado: Integer;
   dFecha: TDate;
   aEmpresa: OleVariant;
begin
     Result := FALSE;
     Mensaje := '';
     try
        with cdsLista do
        begin
             InitTempDataSet;
             AddIntegerField( 'CB_CODIGO' );
             AddStringField( 'OP_NUMBER', K_ANCHO_OPERACION );
             AddStringField( 'WO_NUMBER', K_ANCHO_ORDEN );
             {$ifdef INTERRUPTORES}
             AddStringField( 'AR_CODIGO', K_ANCHO_PARTE20 );
             {$else}
             AddStringField( 'AR_CODIGO', K_ANCHO_PARTE );
             {$Endif}
             AddStringField( 'CB_AREA', K_ANCHO_AREA );
             AddDateField( 'AU_FECHA' );
             AddIntegerField( 'WK_TIPO' );
             AddStringField( 'WK_TMUERTO', K_ANCHO_CODIGO3 );
             AddFloatField( 'WK_TIEMPO' );
             AddStringField( 'WK_MOD_1', K_ANCHO_CODIGO1 );
             AddStringField( 'WK_MOD_2', K_ANCHO_CODIGO1 );
             AddStringField( 'WK_MOD_3', K_ANCHO_CODIGO1 );
             AddStringField( 'WK_COMENTA', K_ANCHO_FORMULA );
             CreateTempDataset;
        end;
        FXML := TdmXMLTools.Create( Self );
        try
           with FXML do
           begin
                XMLAsText := Datos;
                iEmpleado := TagAsInteger( GetNode( 'EMPLEADO' ), 0 );
                dFecha := TagAsDate( GetNode( 'FECHA' ), NullDateTime );
                aEmpresa := GetEmpresa;
                Nodo := GetNode( 'ACTIVIDADES' );
                if HasChildren( Nodo ) then
                begin
                     Nodo := GetNode( 'ROWS', Nodo );
                     if HasChildren( Nodo ) then
                     begin
                          Nodo := GetFirstChild( Nodo );
                          while Assigned( Nodo ) do
                          begin
                               if HasAttributes( Nodo ) then
                               begin
                                    with cdsLista do
                                    begin
                                         Append;
                                         FieldByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                         FieldByName( 'AU_FECHA' ).AsDateTime := dFecha;
                                         FieldByName( 'WK_TIEMPO' ).AsFloat := ( K_MINUTOS_HORA * AttributeAsFloat( Nodo, 'WK_TIEMPO', 0.0 ) );
                                         FieldByName( 'WK_TIPO' ).AsInteger := AttributeAsInteger( Nodo, 'WK_TIPO', Ord( wtChecada ) );
                                         FieldByName( 'CB_AREA' ).AsString := AttributeAsString( Nodo, 'CB_AREA', ZetaCommonClasses.VACIO );
                                         FieldByName( 'OP_NUMBER' ).AsString := AttributeAsString( Nodo, 'OP_NUMBER', ZetaCommonClasses.VACIO );
                                         FieldByName( 'WK_TMUERTO' ).AsString := AttributeAsString( Nodo, 'WK_TMUERTO', ZetaCommonClasses.VACIO );
                                         FieldByName( 'WO_NUMBER' ).AsString := AttributeAsString( Nodo, 'WO_NUMBER', ZetaCommonClasses.VACIO );
                                         FieldByName( 'AR_CODIGO' ).AsString := ZetaCommonClasses.VACIO;
                                         FieldByName( 'WK_MOD_1' ).AsString := ZetaCommonClasses.VACIO;
                                         FieldByName( 'WK_MOD_2' ).AsString := ZetaCommonClasses.VACIO;
                                         FieldByName( 'WK_MOD_3' ).AsString := ZetaCommonClasses.VACIO;
                                         FieldByName( 'WK_COMENTA' ).AsString := AttributeAsString( Nodo, 'WK_COMENTA', ZetaCommonClasses.VACIO );
                                         Post;
                                    end;
                               end;
                               Nodo := GetNextSibling( Nodo );
                          end;
                     end
                     else
                         Mensaje := 'No Hay Datos';
                end
                else
                    Mensaje := 'No Hay Datos';
           end;
        finally
               FreeAndNil( FXML );
        end;
        InitCreator;
        try
           GrabaTiempos( aEmpresa, iEmpleado, dFecha, cdsLista.Data );
        finally
               ZetaSQLBroker.FreeZetaCreator( oZetaCreator );
        end;
        Result := TRUE;
     except
           on Error: Exception do
           begin
                Mensaje := 'Error Al Grabar Horas: ' + Error.Message;
           end;
     end;
end;

end.
