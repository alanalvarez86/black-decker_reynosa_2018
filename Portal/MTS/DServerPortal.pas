unit DServerPortal;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom,
     {$ifndef VER130}Variants,MaskUtils,{$endif}
     StdVcl, BdeMts, DataBkr, DBClient,Db, MtsRdm, Mtx,
     {$ifndef DOS_CAPAS}
     Portal_TLB,
     {$endif}
     {$ifdef CHILKAT}
     Login_TLB,
     ZetaXMLTools,
     {$endif}
     DSuperGlobal,
     DZetaServerprovider;

type
    eTipoTabla =( eTNoticia,
                  eTEvento,
                  eTDocumen,
                  eNoticia,
                  eEvento,
                  eDocument,
                  eBuzonSug,
                  eUsuarios,
                  ePagina,
                  eContiene,
                  eReportal,
                  eRepCols,
                  eRepData );
    eCampoOrden = ( eEmpresa,
                    eNombres,
                    eTelefonos );
    eOrden = ( eAscendente,
               eDescendente );
    eAccionTarjeta = ( eaAgregar,
                       eaEditar,
                       eaBorrar );

  TDatosColumna = record
    Titulo: String;
    Tipo: Integer;
    Ancho: Integer;
  end;

    TdmServerPortal = class(TMtsDataModule{$ifndef DOS_CAPAS}, IdmServerPortal{$endif})
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FContenidoBorra: TZetaCursor;
    FContenidoCambia: TZetaCursor;
    {$ifdef CHILKAT}
    FXML: TZetaXML;
    FLoginObj: IdmServerLogin;
    {$endif}
    {$ifdef CHILKAT}
    function GetNombreTabla( const eTabla: eTipoTabla ): String;
    function GetDocumentoXML: WideString;
    function AgregaReportal(const iFolio, iCuantos: Integer; oNodo: TZetaXMLNode): Integer;
    {$endif}
    procedure BeforeUpdatePaginas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
    procedure SetTablaInfo(const eTabla: eTipoTabla);
    {$ifdef CHILKAT}
    procedure TJ_EMPRESAOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure TJ_NOMBRESOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure TJ_TELEFONOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure INDICEOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure CampoHoraGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CampoImagenGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure RD_DATOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ResultadosAfterOpen(DataSet: TDataSet);
    procedure AgregaRepCols(const iFolio: Integer; oNodo: TZetaXMLNode);
    procedure AgregaRepData(const iFolio, iColumnas, iCuantos: Integer; oNodo: TZetaXMLNode);
    procedure AgregaNoticias( const iCuantos: Integer; const lShowHotNews: Boolean; oNodo: TZetaXMLNode );
    procedure AgregaEventos( const iCuantos: Integer; const dFecha: TDate; oNodo: TZetaXMLNode; const lInicio: Boolean = FALSE );
    procedure AgregaSeccGrafica(oNodo: TZetaXMLNode);
    procedure AgregaSeccReporte(const iMostrar, iCodigo: Integer; oNodo: TZetaXMLNode);
    procedure AgregaSeccTitular(oNodo: TZetaXMLNode);
    procedure SetFooterPortal( Datos: TZetaXMLNode );
    {$endif}
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    {$ifdef DOS_CAPAS}
  public
    { Public declarations }
    procedure CierraEmpresa;
    {$endif}
    function GetContenidos(Usuario, Pagina, Columna: Integer; out Paginas: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GetTabla(iTabla: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
    function GrabaTabla(iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaContenidos(Delta: OleVariant; Usuario, Pagina, Columna: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaPaginas(Delta: OleVariant; Usuario: Integer; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetGlobales: OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GrabaGlobales(Delta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetReportalMetadata(out RepData: OleVariant): OleVariant; {$ifndef DOS_CAPAS} safecall;{$endif}
    function GrabaReportal(Folio: Integer; var Columnas, Datos: OleVariant): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AgregaSugerencia(const Texto, Titulo, Autor, Correo: WideString; Usuario: Integer): Integer; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetInitPortal(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function UsuarioLogin(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function UsuarioCambiaPswd(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetInicio(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetNoticias100(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetNoticia(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEventos100(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetEvento(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetReporte(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDocumentos(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetDirectorios(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetMatchTarjetas(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetTarjeta(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function ExpiraLogin(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetBuscaTarjetas(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function GetTarjetaNET(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function AgregaTarjetaNET(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function BorraTarjetaNET(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    function ModificaTarjetaNET(const Parametros: WideString): WideString; {$ifndef DOS_CAPAS}safecall;{$endif}
    procedure EscribeArchivo(const Parametros: WideString);  {$ifndef DOS_CAPAS}safecall;{$endif}
  end;

var
   dmServerPortal: TdmServerPortal;
   {$ifdef CHILKAT}
   aColumnas:  array[1..10] of TDatosColumna;
   {$endif}

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaCommonLists,
     ZetaServerDataSet,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaPortalClasses;

const
     K_NOMBRE_MODULO = 'Portal';
     K_AGREGAR = 'Agregar';
     K_GRABAR = 'Grabar';
     K_BORRAR = 'Borrar';

     K_TABLE_TARJETA = 'TARJETA';

     K_OFFSET = 100;
     Q_CONTENIDO_LEE = 1;
     Q_CONTENIDO_BORRA = 2;
     Q_CONTENIDO_BORRA_TODO = 3;
     Q_CONTENIDO_CAMBIA_TODO = 4;
     Q_CONTENIDO_RESET = 5;
     Q_PAGINAS_LEE = 6;
     Q_PAGINAS_BORRA = 7;
     Q_PAGINAS_CUENTA = 8;
     Q_USUARIO_EXISTE = 9;
     Q_USUARIO_AGREGA = 10;
     Q_USUARIO_ACTUALIZA = 11;
     Q_REP_COLS_METADATA = 12;
     Q_REP_DATA_METADATA = 13;
     Q_REP_BORRA_COLUMNAS = 14;
     Q_REP_BORRA_DATOS = 15;
     Q_REP_CUENTA_COLUMNAS = 16;
     Q_REP_CUENTA_RENGLONES = 17;
     Q_REP_UPDATE_REPORTE = 18;
     Q_SUGERENCIA_FOLIO = 19;
     Q_SUGERENCIA_AGREGA = 20;
     Q_LISTA_NOTICIAS_100 = 21;
     Q_INFO_NOTICIA = 22;
     Q_LISTA_EVENTOS_100 = 23;
     Q_INFO_EVENTO = 24;
     Q_REP_PORTAL = 25;
     Q_REP_COLS = 26;
     Q_REP_DATA = 27;
     Q_CONTIENE = 28;
     Q_INFO_TITULAR = 29;
     Q_PAGINAS = 30;
     Q_DOCUMENTOS = 31;
     Q_COUNT_DOCUMENTOS = 32;
     Q_LOGIN_USUARIO = 33;
     Q_USUARIO_DEFAULT = 34;
     Q_OBTENER_INFO = 35;
     Q_LISTA_EVENTOS = 36;
     Q_INSERT_TARJETA =37;
     Q_UPDATE_TARJETA = 38;
     Q_DELETE_TARJETA = 39;
     Q_ROL_MATCH = 40;
     Q_ROL_MATCH_TOTAL = 41;
     Q_ROL_MATCH_COUNT = 42;
     Q_ACTUAL_PASSWORD = 43;
     Q_USUARIO_CAMBIA_PSWD = 44;
     Q_ROL_MATCH_NET = 45;
     Q_TARJETA_NET = 46;
     Q_BORRA_TARJETA_NET = 47;
     Q_AGREGA_TARJETA_NET = 48;
     Q_MODIFICA_TARJETA_NET = 49;

{$R *.DFM}

function GetNombreTabla( const eTabla: eTipoTabla ): String;
begin
     case eTabla of
          eTNoticia: Result := 'TNOTICIA';
          eTEvento: Result := 'TEVENTO';
          eTDocumen: Result := 'TDOCUMEN';
          eNoticia: Result := 'NOTICIA';
          eEvento: Result := 'EVENTO';
          eDocument: Result := 'DOCUMENT';
          eBuzonSug: Result := 'BUZONSUG';
          eUsuarios: Result := 'USUARIO';
          ePagina: Result := 'PAGINA';
          eContiene: Result := 'CONTIENE';
          eReportal: Result := 'REPORTAL';
          eRepCols: Result := 'REP_COLS';
          eRepData: Result := 'REP_DATA';
     else
         Result := '';
     end;
end;

function GetScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_CONTENIDO_LEE: Result := 'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'REPORTAL.RP_TITULO as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join REPORTAL on ( REPORTAL.RP_FOLIO = CONTIENE.CT_REPORTE ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO in ( %3:d, %4:d ) ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'CAST( ''Titular'' as VARCHAR( 70 ) ) as CT_DESCRIP '+
                                     'from CONTIENE where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %5:d ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'TNOTICIA.TB_ELEMENT as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join TNOTICIA on ( TNOTICIA.TB_CODIGO = CONTIENE.CT_CLASIFI ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %6:d ) '+
                                     'union '+
                                     'select PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, US_CODIGO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI, '+
                                     'TEVENTO.TB_ELEMENT as CT_DESCRIP '+
                                     'from CONTIENE '+
                                     'left outer join TEVENTO on ( TEVENTO.TB_CODIGO = CONTIENE.CT_CLASIFI ) '+
                                     'where '+
                                     '( US_CODIGO = %0:d ) and '+
                                     '( PA_ORDEN = %1:d ) and '+
                                     '( CT_COLUMNA = %2:d ) and '+
                                     '( CT_TIPO = %7:d ) ';
                                     //'order by PA_ORDEN, CT_COLUMNA, CT_ORDEN';
          Q_CONTENIDO_BORRA: Result := 'delete from CONTIENE where '+
                                       '( US_CODIGO = %d ) and '+
                                       '( PA_ORDEN = %d ) and '+
                                       '( CT_COLUMNA = %d )';
          Q_CONTENIDO_BORRA_TODO: Result := 'delete from CONTIENE where '+
                                            '( US_CODIGO = %d ) and '+
                                            '( PA_ORDEN = :PA_ORDEN )';
          Q_CONTENIDO_CAMBIA_TODO: Result := 'update CONTIENE set PA_ORDEN = :PA_ORDEN where ' +
                                             '( US_CODIGO = %d ) and '+
                                             '( PA_ORDEN = :OLD_PA_ORDEN )';
          Q_CONTENIDO_RESET: Result := 'update CONTIENE set PA_ORDEN = PA_ORDEN - %0:d where '+
                                       '( US_CODIGO = %1:d ) and '+
                                       '( PA_ORDEN >= %0:d )';
          Q_PAGINAS_LEE: Result := 'select US_CODIGO, PA_ORDEN, PA_TITULO, PA_COLS, '+
                                   'PA_ORDEN as PA_ANTERIOR '+
                                   'from PAGINA where ( US_CODIGO = %d ) order by PA_ORDEN';
          Q_PAGINAS_BORRA: Result := 'delete from PAGINA where ( US_CODIGO = %d )';
          Q_PAGINAS_CUENTA: Result := 'select COUNT(*) CUANTAS from PAGINA where ( US_CODIGO = %d )';
          Q_USUARIO_EXISTE: Result := 'select COUNT(*) CUANTOS from USUARIO where ( US_CODIGO = %d )';
          Q_USUARIO_AGREGA: Result := 'insert into USUARIO ( US_CODIGO, US_NOMBRE, US_PAGINAS ) values ( %d, ''%s'', %d )';
          Q_USUARIO_ACTUALIZA: Result := 'update USUARIO set US_PAGINAS = %d where ( US_CODIGO = %d )';
          Q_REP_COLS_METADATA: Result := 'select RP_FOLIO, RC_ORDEN, RC_TITULO, RC_TIPO, RC_ANCHO '+
                                         'from REP_COLS where ( RP_FOLIO < 0 )';
          Q_REP_DATA_METADATA: Result := 'select RP_FOLIO, RD_ORDEN, RD_DATO1, RD_DATO2, RD_DATO3, RD_DATO4, '+
                                         'RD_DATO5, RD_DATO6, RD_DATO7, RD_DATO8, RD_DATO9, RD_DATO10 '+
                                         'from REP_DATA where ( RP_FOLIO < 0 ) and ( RD_ORDEN < 0 )';
          Q_REP_BORRA_COLUMNAS: Result := 'delete from REP_COLS where ( RP_FOLIO = %d )';
          Q_REP_BORRA_DATOS: Result := 'delete from REP_DATA where ( RP_FOLIO = %d )';
          Q_REP_CUENTA_COLUMNAS: Result := 'select COUNT(*) CUANTOS from REP_COLS where ( RP_FOLIO = %d )';
          Q_REP_CUENTA_RENGLONES: Result := 'select COUNT(*) CUANTOS from REP_DATA where ( RP_FOLIO = %d )';
          Q_REP_UPDATE_REPORTE: Result := 'update REPORTAL set RP_FECHA = ''%s'', RP_HORA = ''%s'', RP_COLS = %d, RP_ROWS = %d where '+
                                          '( RP_FOLIO = %d )';
          Q_SUGERENCIA_FOLIO: Result := 'select MAX( BU_FOLIO ) MAXIMO from BUZONSUG';
          Q_SUGERENCIA_AGREGA: Result := 'insert into BUZONSUG( BU_FOLIO, BU_FECHA, BU_HORA, BU_MEMO, BU_TITULO, BU_USUARIO, BU_AUTOR, BU_EMAIL ) values ' +
                                         '( %d, ''%s'', ''%s'', :BU_MEMO, :BU_TITULO, :BU_USUARIO, :BU_AUTOR, :BU_EMAIL )';
          Q_LISTA_NOTICIAS_100: Result := 'select A.NT_FOLIO, A.NT_FECHA, A.NT_HORA, A.NT_TITULO%s from NOTICIA A order by A.NT_FECHA DESC';
          Q_INFO_NOTICIA: Result := 'select NT_FOLIO, NT_FECHA, NT_HORA, NT_TITULO, NT_MEMO, NT_USUARIO, NT_CLASIFI, NT_AUTOR, NT_CORTA, NT_SUB_TIT, NT_IMAGEN ' +
                                    'from NOTICIA ' +
                                    'where NT_FOLIO = %d';
          Q_LISTA_EVENTOS_100: Result := 'select EV_FOLIO, EV_FEC_INI, EV_TITULO from EVENTO order by EV_FEC_INI desc';
                                      //'select EV_FOLIO, EV_FEC_INI, EV_TITULO from EVENTO where EV_FEC_INI >= %s order by EV_FEC_INI asc';
          Q_INFO_EVENTO: Result := 'select EV_FOLIO, EV_FEC_INI, EV_FEC_FIN, EV_HORA, EV_LUGAR, EV_MEMO, EV_TITULO, EV_USUARIO, EV_CLASIFI, EV_AUTOR, EV_URL, EV_IMAGEN ' +
                                   'from EVENTO ' +
                                   'where EV_FOLIO = %d';
          Q_REP_PORTAL: Result := 'select RP_FOLIO, RP_TITULO, RP_DESCRIP, RP_FECHA, RP_HORA, RP_COLS, RP_ROWS from REPORTAL where RP_FOLIO = %d';
          Q_REP_COLS: Result := 'select RC_ORDEN, RC_TITULO, RC_TIPO, RC_ANCHO from REP_COLS where RP_FOLIO = %d order by RC_ORDEN';
          Q_REP_DATA: Result := 'select RD_ORDEN, %s from REP_DATA where RP_FOLIO = %d order by RD_ORDEN';
          Q_CONTIENE: Result := 'select US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI ' +
                                'from CONTIENE where US_CODIGO= %d and PA_ORDEN = %d' +
                                'order by CT_COLUMNA, CT_ORDEN';
          Q_INFO_TITULAR: Result := 'select NT_FOLIO, NT_TITULO, NT_AUTOR, NT_CORTA, NT_SUB_TIT, NT_IMAGEN ' +
                                    'from NOTICIA ' +
                                    'where NT_FOLIO = %d';
          Q_PAGINAS: Result := 'select PA_ORDEN, PA_TITULO, PA_COLS from PAGINA where US_CODIGO = %d';
          Q_DOCUMENTOS: Result := 'select DC_FOLIO, DC_TITULO, DC_AUTOR, DC_RUTA, DC_MEMO ' +
                                  'from DOCUMENT where UPPER(DC_PALABRA) like %s order by DC_TITULO';
          Q_COUNT_DOCUMENTOS: Result := 'select COUNT(*) CUANTOS from DOCUMENT where UPPER(DC_PALABRA) like %s';
          //Q_LOGIN_USUARIO: Result := 'select US_CODIGO, US_NOMBRE, US_PAGINAS, US_PASSWRD, US_BLOQUEA, CB_CODIGO, CM_CODIGO from USUARIO where ( UPPER( US_CORTO ) = UPPER( %s ) )';
          Q_LOGIN_USUARIO: Result := 'select US_PAGINAS, CB_CODIGO, CM_CODIGO from USUARIO where ( US_CODIGO = %d )';
          Q_USUARIO_DEFAULT: Result := 'select US_CORTO, US_PAGINAS, US_PASSWRD, CB_CODIGO, CM_CODIGO from USUARIO where US_CODIGO = %d';
          //Q_OBTENER_INFO: Result := 'select * from TARJETA where TJ_EMPRESA = %s and TJ_NOMBRES = %s';
          Q_OBTENER_INFO: Result := 'select * from TARJETA where TJ_EMPRESA = :TJ_EMPRESA and TJ_NOMBRES = :TJ_NOMBRES';
          Q_LISTA_EVENTOS: Result := 'select EV_FOLIO, EV_FEC_INI, EV_TITULO from EVENTO where EV_FEC_INI >= %s order by EV_FEC_INI asc';
          Q_INSERT_TARJETA: Result := 'insert into TARJETA ( TJ_EMPRESA, TJ_NOMBRES, TJ_TELEFON, TJ_DIRECC1, TJ_DIRECC2, TJ_COMENTA, TJ_EMAIL ) '+
                                      'values ( :TJ_EMPRESA, :TJ_NOMBRES, :TJ_TELEFON, :TJ_DIRECC1, :TJ_DIRECC2, :TJ_COMENTA, :TJ_EMAIL )';
          Q_UPDATE_TARJETA: Result := 'update TARJETA set TJ_EMPRESA = :TJ_EMPRESA, TJ_NOMBRES = :TJ_NOMBRES, TJ_TELEFON = :TJ_TELEFON, ' +
                                      'TJ_DIRECC1 = :TJ_DIRECC1, TJ_DIRECC2 = :TJ_DIRECC2, TJ_COMENTA = :TJ_COMENTA, TJ_EMAIL = :TJ_EMAIL ' +
                                      'where ( TJ_EMPRESA = :EMPRESA ) and ( TJ_NOMBRES = :NOMBRES )';
          Q_DELETE_TARJETA: Result := 'delete from TARJETA where ( TJ_EMPRESA = :EMPRESA ) and ( TJ_NOMBRES = :NOMBRES )';
          Q_ROL_MATCH: Result := 'select 0 INDICE, TJ_EMPRESA, TJ_NOMBRES, TJ_TELEFON from TARJETA where ( UPPER( TJ_EMPRESA ) like %0:s ) '+
                                 'OR ( UPPER( TJ_NOMBRES ) like %0:s ) OR ( UPPER( TJ_TELEFON ) like %0:s ) OR ( UPPER( TJ_COMENTA ) like %0:s ) '+
                                 'ORDER BY %1:s %2:s';
          Q_ROL_MATCH_TOTAL: Result := 'select 0 INDICE, TJ_EMPRESA, TJ_NOMBRES, TJ_TELEFON from TARJETA order by %0:s %1:s';
          Q_ROL_MATCH_COUNT: Result := 'select COUNT(*) CUANTOS from TARJETA where ( UPPER( TJ_EMPRESA ) like %0:s ) OR ( UPPER( TJ_NOMBRES ) like %0:s ) '+
                                       'OR ( UPPER( TJ_TELEFON ) like %0:s ) OR ( UPPER( TJ_COMENTA ) like %0:s )';
          Q_ACTUAL_PASSWORD: Result := 'select US_PASSWRD from USUARIO where ( US_CODIGO = %d )';
          Q_USUARIO_CAMBIA_PSWD: Result := 'update USUARIO set US_PASSWRD = ''%s'', US_CAMBIA = ''%s'', US_FEC_PWD = ''%s'' where ( US_CODIGO = %d )';
          // QUERYS DE .NET
          Q_ROL_MATCH_NET: Result := 'select TJ_EMPRESA, TJ_NOMBRES, TJ_TELEFON,'+
//                                     '''EMPRESA='' || TJ_EMPRESA || ''&'' || ''NOMBRES='' || TJ_NOMBRES as TJ_LIGA '+
                                     'cast( ''EMPRESA='' || TJ_EMPRESA || ''&'' || ''NOMBRES='' || TJ_NOMBRES as CHAR(100) ) TJ_LIGA '+
//                                     '''NOMBRES='' || TJ_NOMBRES as TJ_LIGA '+
                                     'from TARJETA where ( UPPER( TJ_EMPRESA ) like %0:s ) '+
                                     'OR ( UPPER( TJ_NOMBRES ) like %0:s ) OR ( UPPER( TJ_TELEFON ) like %0:s ) OR ( UPPER( TJ_COMENTA ) like %0:s ) '+
                                     'ORDER BY %1:d';
          Q_TARJETA_NET: Result := 'select TJ_EMPRESA,TJ_NOMBRES,TJ_TELEFON,TJ_DIRECC1,TJ_DIRECC2,TJ_COMENTA,TJ_EMAIL from TARJETA where TJ_EMPRESA = %s and TJ_NOMBRES = %s';
          Q_BORRA_TARJETA_NET: Result := 'delete from TARJETA where ( TJ_EMPRESA = %s ) and ( TJ_NOMBRES = %s )';
          Q_AGREGA_TARJETA_NET: Result := 'insert into TARJETA ( TJ_EMPRESA, TJ_NOMBRES, TJ_TELEFON, TJ_DIRECC1, TJ_DIRECC2, TJ_COMENTA, TJ_EMAIL ) '+
                                          'values ( %s, %s, %s, %s, %s, %s, %s )';
          Q_MODIFICA_TARJETA_NET: Result := 'update TARJETA set TJ_EMPRESA = %s, TJ_NOMBRES = %s, TJ_TELEFON = %s, ' +
                                            'TJ_DIRECC1 = %s, TJ_DIRECC2 = %s, TJ_COMENTA = %s, TJ_EMAIL = %s ' +
                                            'where ( TJ_EMPRESA = %s ) and ( TJ_NOMBRES = %s )';
     end;
end;

class procedure TdmServerPortal.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerPortal.MtsDataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerPortal.MtsDataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmServerPortal.SetTablaInfo(const eTabla: eTipoTabla);
begin
     with oZetaProvider.TablaInfo do
     begin
          case eTabla of
              eNoticia : SetInfo( 'NOTICIA','NT_FOLIO, NT_FECHA, NT_HORA, NT_IMAGEN, NT_MEMO, NT_TITULO, NT_USUARIO, NT_AUTOR, NT_CLASIFI,NT_CORTA,NT_SUB_TIT', 'NT_FOLIO' );
              eEvento  : SetInfo( 'EVENTO','EV_FOLIO, EV_FEC_INI, EV_FEC_FIN, EV_HORA, EV_IMAGEN, EV_LUGAR, EV_MEMO, EV_TITULO, EV_USUARIO, EV_AUTOR, EV_CLASIFI,EV_URL','EV_FOLIO' );
              eDocument: Setinfo( 'DOCUMENT', 'DC_FOLIO, DC_FECHA, DC_HORA, DC_RUTA, DC_IMAGEN, DC_TITULO, DC_MEMO, DC_USUARIO,DC_AUTOR, DC_PALABRA, DC_CLASIFI', 'DC_FOLIO');
              eBuzonSug: Setinfo( 'BUZONSUG', 'BU_FOLIO,BU_FECHA,BU_HORA,BU_MEMO,BU_TITULO,BU_USUARIO,BU_AUTOR,BU_EMAIL', 'BU_FOLIO');
              eUsuarios: Setinfo( 'USUARIO', 'US_CODIGO,US_NOMBRE,US_PAGINAS,US_PASSWRD,US_CORTO', 'US_CODIGO');
              ePagina  : Setinfo( 'PAGINA', 'US_CODIGO,PA_ORDEN,PA_TITULO, PA_COLS', 'US_CODIGO,PA_ORDEN');
              eContiene: SetInfo( 'CONTIENE', 'US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN, CT_TIPO, CT_MOSTRAR, CT_REPORTE, CT_CLASIFI', 'US_CODIGO, PA_ORDEN, CT_COLUMNA, CT_ORDEN' );
              eReportal: SetInfo( 'REPORTAL', 'RP_FOLIO,RP_TITULO, RP_DESCRIP, RP_FECHA, RP_HORA, RP_IMAGEN, RP_USUARIO, RP_TIPO, RP_CLASIFI, RP_COLS, RP_ROWS', 'RP_FOLIO');
              eRepCols: SetInfo( 'REP_COLS', 'RP_FOLIO,RC_ORDEN, RC_TITULO, RC_TIPO, RC_ANCHO', 'RP_FOLIO, RC_ORDEN' );
              eRepData: SetInfo( 'REP_DATA', 'RP_FOLIO,RD_ORDEN, RD_DATO1, RD_DATO2, RD_DATO3, RD_DATO4, RD_DATO5, RD_DATO6, RD_DATO7, RD_DATO8, RD_DATO9, RD_DATO10', 'RP_FOLIO, RD_ORDEN' );
          else
              SetInfo( GetNombreTabla( eTabla ), 'TB_CODIGO,TB_ELEMENT,TB_INGLES,TB_NUMERO,TB_TEXTO', 'TB_CODIGO' );
          end;
     end;
end;

{$ifdef DOS_CAPAS}
procedure TdmServerPortal.CierraEmpresa;
begin
end;
{$endif}

function TdmServerPortal.GetTabla( iTabla: Integer ): OleVariant;
var
   eTabla: eTipoTabla;
begin
     eTabla := eTipoTabla( iTabla );
     try
        SetTablaInfo( eTabla );
        with oZetaProvider do
        begin
             with TablaInfo do
             begin
                  case eTabla of
                       eUsuarios: Filtro := 'US_CODIGO <> 0';
                  end;
             end;
             Result := GetTabla( Comparte );
        end;
     except
           SetOLEVariantToNull( Result );
           raise;
     end;
     SetComplete;
end;

function TdmServerPortal.GrabaTabla( iTabla: Integer; oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     SetTablaInfo( eTipoTabla( iTabla ) );
     with oZetaProvider do
     begin
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     SetComplete;
end;

function TdmServerPortal.GetContenidos(Usuario, Pagina, Columna: Integer; out Paginas: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, Format( GetScript( Q_CONTENIDO_LEE ), [ Usuario, Pagina, Columna, Ord( ecReporte ), Ord( ecGrafica ), Ord( ecTitular ), Ord( ecNoticia ), Ord( ecEvento ) ] ), True );
          Paginas := OpenSQL( Comparte, Format( GetScript( Q_PAGINAS_LEE ), [ Usuario ] ), True );
     end;
     SetComplete;
end;

function TdmServerPortal.GrabaContenidos(Delta: OleVariant; Usuario, Pagina, Columna: Integer; out ErrorCount: Integer): OleVariant;
begin
     with oZetaProvider do
     begin
          ExecSQL( Comparte, Format( GetScript( Q_CONTENIDO_BORRA ), [ Usuario, Pagina, Columna ] ) );
          if VarIsNull( Delta ) then
             Result := Null
          else
          begin
               SetTablaInfo( eContiene );
               Result := GrabaTabla( Comparte, Delta, ErrorCount );
          end;
     end;
     SetComplete;
end;

function TdmServerPortal.GrabaPaginas(Delta: OleVariant; Usuario: Integer; out ErrorCount: Integer): OleVariant;
var
   FDataset: TZetaCursor;
   iPaginas: Integer;
   lExiste: Boolean;
   sNombre: String;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          ExecSQL( Comparte, Format( GetScript( Q_PAGINAS_BORRA ), [ Usuario ] ) );
          FContenidoBorra := CreateQuery( Format( GetScript( Q_CONTENIDO_BORRA_TODO ), [ Usuario ] ) );
          try
             FContenidoCambia := CreateQuery( Format( GetScript( Q_CONTENIDO_CAMBIA_TODO ), [ Usuario ] ) );
             try
                SetTablaInfo( ePagina );
                with TablaInfo do
                begin
                     BeforeUpdateRecord := BeforeUpdatePaginas;
                end;
                Result := GrabaTabla( Comparte, Delta, ErrorCount );
             finally
                    FreeAndNil( FContenidoCambia );
             end;
          finally
                 FreeAndNil( FContenidoBorra );
          end;
          { K_OFFSET es necesario para evitar que haya colisiones al }
          { cambiar los ordenes de dos paginas consecutivas }
          ExecSQL( Comparte, Format( GetScript( Q_CONTENIDO_RESET ), [ K_OFFSET, Usuario ] ) );
          FDataset := CreateQuery;
          try
             PreparaQuery( FDataset, Format( GetScript( Q_PAGINAS_CUENTA ), [ Usuario ] ) );
             with FDataset do
             begin
                  Active := True;
                  iPaginas := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
             PreparaQuery( FDataset, Format( GetScript( Q_USUARIO_EXISTE ), [ Usuario ] ) );
             with FDataset do
             begin
                  Active := True;
                  lExiste := ( Fields[ 0 ].AsInteger > 0 );
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          if lExiste then
             ExecSQL( Comparte, Format( GetScript( Q_USUARIO_ACTUALIZA ), [ iPaginas, Usuario ] ) )
          else
          begin
               if ( Usuario = 0 ) then
                  sNombre := 'Usuario Default'
               else
                   sNombre := Format( 'Usuario %d', [ Usuario ] );
               ExecSQL( Comparte, Format( GetScript( Q_USUARIO_AGREGA ), [ Usuario, sNombre, iPaginas ] ) )
          end;
     end;
     SetComplete;
end;

procedure TdmServerPortal.BeforeUpdatePaginas(Sender: TObject; SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind; var Applied: Boolean);
var
   iOrden, iAnterior: Integer;
begin
     with DeltaDS do
     begin
          iOrden := FieldByName( 'PA_ORDEN' ).AsInteger;
          iAnterior := FieldByName( 'PA_ANTERIOR' ).AsInteger;
          if ( iOrden = K_NULL_PAGINA ) then { Borrar Contenido de Pagina Borrada }
          begin
               with oZetaProvider do
               begin
                    EmpiezaTransaccion;
                    try
                       ParamAsInteger( FContenidoBorra, 'PA_ORDEN', iAnterior );
                       Ejecuta( FContenidoBorra );
                       TerminaTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               RollBackTransaccion;
                               raise;
                          end;
                    end;
               end;
               Applied := True;
          end
          else
              if ( iAnterior <> K_NULL_PAGINA ) and ( iOrden <> iAnterior ) then  { Cambiar Contenido }
              begin
                   { K_OFFSET es necesario para evitar que haya colisiones al }
                   { cambiar los ordenes de dos paginas consecutivas }
                   with oZetaProvider do
                   begin
                        EmpiezaTransaccion;
                        try
                           ParamAsInteger( FContenidoCambia, 'OLD_PA_ORDEN', iAnterior );
                           ParamAsInteger( FContenidoCambia, 'PA_ORDEN', iOrden + K_OFFSET );
                           Ejecuta( FContenidoCambia );
                           TerminaTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   RollBackTransaccion;
                                   raise;
                              end;
                        end;
                   end;
              end;
     end;
end;

function TdmServerPortal.GetGlobales: OleVariant;
var
   oSuperGlobal: TdmSuperGlobal;
begin
     oSuperGlobal := TdmSuperGlobal.Create;
     try
        try
           with oZetaProvider do
           begin
                EmpresaActiva := Comparte;
           end;
           with oSuperGlobal do
           begin
                Provider := oZetaProvider;
                Result := ProcesaLectura;
           end;
           SetComplete;
        except
              SetAbort;
              raise;
        end;
     finally
            FreeAndNil( oSuperGlobal );
     end;
end;

function TdmServerPortal.GrabaGlobales(Delta: OleVariant;out ErrorCount: Integer): OleVariant;
var
   oSuperGlobal: TdmSuperGlobal;
begin
     oSuperGlobal := TdmSuperGlobal.Create;
     try
        try
           with oZetaProvider do
           begin
                EmpresaActiva := Comparte;
           end;
           with oSuperGlobal do
           begin
                Provider := oZetaProvider;
                ActualizaDiccion := False;
                Result := ProcesaEscritura( Delta );
                ErrorCount := Errores;
           end;
           SetComplete;
        except
              SetAbort;
              raise;
        end;
     finally
            FreeAndNil( oSuperGlobal );
     end;
end;

function TdmServerPortal.GetReportalMetadata( out RepData: OleVariant): OleVariant;
begin
     with oZetaProvider do
     begin
          Result := OpenSQL( Comparte, GetScript( Q_REP_COLS_METADATA ), False );
          RepData := OpenSQL( Comparte, GetScript( Q_REP_DATA_METADATA ), False );
     end;
     SetComplete;
end;

function TdmServerPortal.GrabaReportal(Folio: Integer; var Columnas, Datos: OleVariant): Integer;
var
   iColErrors, iDataErrors: Integer;
   FDataset: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          ExecSQL( Comparte, Format( GetScript( Q_REP_BORRA_COLUMNAS ), [ Folio ] ) );
          ExecSQL( Comparte, Format( GetScript( Q_REP_BORRA_DATOS ), [ Folio ] ) );
          if ( ( Columnas = Null ) or ( Datos = Null ) ) then
          begin
               Result := 0;
          end
          else
          begin
               SetTablaInfo( eRepCols );
               Columnas := GrabaTabla( Comparte, Columnas, iColErrors );
               SetTablaInfo( eRepData );
               Datos := GrabaTabla( Comparte, Datos, iDataErrors );
               Result := ( K_ERROR_OFFSET * iColErrors + iDataErrors );
          end;
          FDataset := CreateQuery;
          try
             PreparaQuery( FDataset, Format( GetScript( Q_REP_CUENTA_COLUMNAS ), [ Folio ] ) );
             with FDataset do
             begin
                  Active := True;
                  iColErrors := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
             PreparaQuery( FDataset, Format( GetScript( Q_REP_CUENTA_RENGLONES ), [ Folio ] ) );
             with FDataset do
             begin
                  Active := True;
                  iDataErrors := Fields[ 0 ].AsInteger;
                  Active := False;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
          ExecSQL( Comparte, Format( GetScript( Q_REP_UPDATE_REPORTE ), [ ZetaCommonTools.DateToStrSQL( Now ), FormatDateTime( 'hhnn', Now ), iColErrors, iDataErrors, Folio ] ) );
     end;
     SetComplete;
end;

function TdmServerPortal.AgregaSugerencia(const Texto, Titulo, Autor, Correo: WideString; Usuario: Integer): Integer;
var
   FDataset: TZetaCursor;
   iFolio: Integer;
begin
     Result := 0;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FDataset := CreateQuery;
          try
             PreparaQuery( FDataset, GetScript( Q_SUGERENCIA_FOLIO ) );
             with FDataset do
             begin
                  Active := True;
                  iFolio := Fields[ 0 ].AsInteger + 1;
                  Active := False;
             end;
             if ( iFolio > 0 ) then
             begin
                  EmpiezaTransaccion;
                  try
                     PreparaQuery( FDataset, Format( GetScript( Q_SUGERENCIA_AGREGA ), [ iFolio, ZetaCommonTools.DateToStrSQL( Now ), FormatDateTime( 'hhnn', Now ) ] ) );
                     ParamAsBlob( FDataset, 'BU_MEMO', Texto );
                     ParamAsString( FDataset, 'BU_TITULO', Titulo );
                     ParamAsString( FDataset, 'BU_EMAIL', Correo );
                     ParamAsString( FDataset, 'BU_AUTOR', Autor );
                     ParamAsInteger( FDataset, 'BU_USUARIO', Usuario );
                     Ejecuta( FDataset );
                     TerminaTransaccion( True );
                     Result := iFolio;
                  except
                        on Error: Exception do
                        begin
                             RollBackTransaccion;
                             raise;
                        end;
                  end;
             end;
          finally
                 FreeAndNil( FDataset );
          end;
     end;
end;

{ Interaccion de XML con Clientes de WEB }

{$ifdef CHILKAT}
function TdmServerPortal.GetDocumentoXML: WideString;
var
   oNodoRaiz, oNodoGlobal : TZetaXMLNode;
begin
     with FXML do
     begin
          if HuboErrores then
             oNodoRaiz := DocErrores.GetRoot
          else
             oNodoRaiz := Documento.GetRoot;
          oNodoGlobal := oNodoRaiz.FindOrAddNewChild( 'GLOBAL' );
          with oZetaProvider do
          begin
               oNodoGlobal.AddAttribute( 'G6', GetGlobalString( K_GLOBAL_EMPRESA ) );
               oNodoGlobal.AddAttribute( 'G11', GetGlobalString( K_GLOBAL_LOGOSMALL ) );
               oNodoGlobal.AddAttribute( 'G19', GetGlobalString( K_GLOBAL_RUTA_PAGINA ) );
               oNodoGlobal.AddAttribute( 'G20', GetGlobalString( K_GLOBAL_RUTA_PORTAL ) );
          end;
          Result := GetDocXML;
     end;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.CampoHoraGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := Copy( Sender.AsString, 1, 2 ) + ':' + Copy( Sender.AsString, 3, 2 );
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.CampoImagenGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if strLleno( Sender.AsString ) then
        Text := oZetaProvider.GetGlobalString( K_GLOBAL_FOTOS ) + Sender.AsString
     else
        Text := Sender.AsString;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.SetFooterPortal( Datos: TZetaXMLNode );
var
   NodoFooter, NodoRenglon, NodoContenido: TZetaXMLNode;
   sPagina: String;

   procedure AgregaLabelFooter( const sContenido: String );
   const
        K_LBLFOOTER_TAG = 'LBLFOOTER';
   var
      NodoFooter: TZetaXMLNode;
   begin
        NodoFooter := NodoContenido.NewChild( K_LBLFOOTER_TAG, VACIO );
        FXML.SetNodoContenido( NodoFooter, sContenido );
   end;

begin
     { Debe haberse invocado antes a oZetaProvider.InitGlobales }
     with FXML do
     begin
          sPagina := oZetaProvider.GetGlobalString( K_GLOBAL_RUTA_PAGINA );
          NodoFooter := AgregaFooter( Datos, FALSE );
          NodoRenglon := SetRenglon( NodoFooter );
          NodoContenido := SetContenido( NodoRenglon, VACIO, taCenter );
          AgregaLabelFooter( GetTagImagen( oZetaProvider.GetGlobalString( K_GLOBAL_LOGOSMALL ) ) );
          AgregaLabelFooter( oZetaProvider.GetGlobalString( K_GLOBAL_NOMBRE ) );
          AgregaLabelFooter( oZetaProvider.GetGlobalString( K_GLOBAL_EMPRESA ) );
          AgregaLabelFooter( GetTagLiga( sPagina, sPagina ) );
     end;
end;
{$endif}

function TdmServerPortal.GetInitPortal(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   FCursor: TZetaCursor;
   oNodoRaiz: TZetaXMLNode;
   sNombre, sPassword, sEmpresa, sLogoSmall, sRutaPag, sRutaPortal, sCmCodigo : String;
   iTiempo, iPagina, iDefault, iCbCodigo: Integer;
{$endif}
begin
     {$ifdef CHILKAT}
     sNombre     := VACIO;
     sPassword   := VACIO;
     sEmpresa    := VACIO;
     sLogoSmall  := VACIO;
     sRutaPag    := VACIO;
     sRutaPortal := VACIO;
     sCmCodigo   := VACIO;
     iPagina   := 1;
     iCbCodigo := 0;
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             try
                with oZetaProvider do
                begin
                     EmpresaActiva := Comparte;
                     InitGlobales;
                     iTiempo := GetGlobalInteger( K_GLOBAL_PAGE_REFRESH );
                     sEmpresa := GetGlobalString( K_GLOBAL_EMPRESA );
                     sLogoSmall := GetGlobalString( K_GLOBAL_LOGOSMALL );
                     sRutaPag := GetGlobalString( K_GLOBAL_RUTA_PAGINA );
                     sRutaPortal := GetGlobalString( K_GLOBAL_RUTA_PORTAL );
                     iDefault := GetGlobalInteger( K_GLOBAL_USER_DEFAULT );
                end;
                FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_USUARIO_DEFAULT ), [ iDefault ] ) );
                try
                   try
                      with FCursor do
                      begin
                           Active := True;
                           if not IsEmpty then       // Si est� vacio se usuar�n los valores default asignados a las variables
                           begin
                                iPagina    := FieldByName( 'US_PAGINAS' ).AsInteger;
                                sNombre    := FieldByName( 'US_CORTO' ).AsString;
                                sPassword  := FieldByName( 'US_PASSWRD' ).AsString;
                                iCbCodigo  := FieldByName( 'CB_CODIGO' ).AsInteger;
                                sCmCodigo  := FieldByName( 'CM_CODIGO' ).AsString;
                           end;
                           Active := False;
                      end;
                   except
                         on Error: Exception do
                         begin
                              BuildErrorXML( 'Error Al Obtener Datos de Usuario Default: ' + Error.Message );
                         end;
                   end;
                finally
                       FreeAndNil( FCursor );
                end;
                oNodoRaiz := Documento.GetRoot;
                oNodoRaiz.NewChild( 'US_CORTO', sNombre );
                oNodoRaiz.NewChild( 'US_PASSWRD', sPassword );
                oNodoRaiz.NewChild( 'US_PAGINAS', IntToStr( iPagina ) );
                oNodoRaiz.NewChild( 'REFRESH', IntToStr( iTiempo ) );
                oNodoRaiz.NewChild( 'NOMBRE_EMP', sEmpresa );
                oNodoRaiz.NewChild( 'LOGO_SMALL', sLogoSmall );
                oNodoRaiz.NewChild( 'RUTAPAG', sRutaPag );
                oNodoRaiz.NewChild( 'RUTAPORTAL', sRutaPortal );
                oNodoRaiz.NewChild( 'US_CODIGO', InttoStr(iDefault) );
                oNodoRaiz.NewChild( 'CB_CODIGO', InttoStr(iCbCodigo) );
                oNodoRaiz.NewChild( 'CM_CODIGO', sCmCodigo );
             except
                on Error: Exception do
                begin
                     BuildErrorXML( 'Error Al Obtener Datos de Inicializaci�n: ' + Error.Message );
                end;
             end;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.UsuarioLogin(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
const
     K_NOMBRE_DOCUMENTO = 'Men� de Portal';
     K_ACCION_REGISTRAR = 'Registrar';
     K_ONLOAD_DOCUMENTO = 'ShowInicio()';
     K_TAG_MES = 'MES';
     K_TAG_ANNO = 'ANNO';
     K_TAG_RESULTADO = 'RESULTADO';
     K_TAG_TITULO = 'TITULO';
var
   oDatos, oSentinel: OleVariant;
   iUsuario, iEmpleado, iPagina, iPasswordLongitud, iPasswordLetras, iPasswordDigitos: Integer;
   sUsuario, sClave, sPassword, sEmpresa, sMensaje, sNombre : String;
   eReply: eLogReply;
   iIntentos: Integer;

   function CambiaClaveUsuario: Boolean;
   begin
        try
           with FXML do
                FLoginObj.UsuarioCambiaPswd( StrToIntDef( ParamAsString( 'USUARIO' ), 0 ),
                                             ZetaServerTools.Decrypt( ParamAsString( 'CLAVEACTUAL' ) ),
                                             ParamAsString( 'CLAVENUEVA' ) );
           Result := TRUE;
        except
           on Error: Exception do
           begin
                sMensaje := Error.Message;
                Result := FALSE;
           end;
        end;
   end;

   procedure AsignaValoresLogin;
   begin
        with FXML do
        begin
             sUsuario := ParamAsString( 'US_CORTO' );
             sClave := ParamAsString( 'US_PASSWRD' );
             if zStrToBool( ParamAsString( 'ENCRIPTADO' ) ) then
                sClave := Decrypt( sClave )
             else
                sClave := sClave;
        end;
   end;

   procedure GetDatosValues;
   var
      cdsUsuario : TServerDataSet;
   begin
        cdsUsuario := TServerDataSet.Create( self );
        try
           with cdsUsuario do
           begin
                Lista := oDatos;
                sNombre := FieldByName( 'US_NOMBRE' ).AsString;
                iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                sPassword := FieldByName( 'US_PASSWRD' ).AsString;
                iPasswordLongitud := FieldByName( 'LIMITEPASS' ).AsInteger;
                iPasswordLetras := FieldByName( 'LETRASPASS' ).AsInteger;
                iPasswordDigitos := FieldByName( 'DIGITOPASS' ).AsInteger;
           end;
        finally
           FreeAndNil( cdsUsuario );
        end;
   end;

   procedure GetPortalValues;
   var
      FCursor : TZetaCursor;
   begin
        with oZetaProvider do
        begin
             FCursor := CreateQuery;
             try
                AbreQueryScript( FCursor, Format( GetScript( Q_LOGIN_USUARIO ), [ iUsuario ] ) );
                with FCursor do
                begin
                     if ( not EOF ) then
                     begin
                          iPagina   := FieldByName( 'US_PAGINAS' ).AsInteger;
                          iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                          sEmpresa  := FieldByName( 'CM_CODIGO' ).AsString;
                     end
                     else       // No debiera llegar aqu�, sin embargo se valida
                     begin
                          iPagina   := 1;
                          iEmpleado := 0;
                          sEmpresa  := VACIO;
                     end;
                end;
             finally
                FreeAndNil( FCursor );
             end;
        end;
   end;

   procedure SetMensajesValue;
   begin
        case eReply of
             lrNotFound: sMensaje := '� No se Encontr� Usuario !';
             lrAccessDenied: sMensaje := '� Clave Incorrecta !';
             lrChangePassword: sMensaje := '� Debe Cambiar su Clave !';
             lrExpiredPassword: sMensaje := '� Clave Ha Expirado !';
             lrLockedOut, lrInactiveBlock: sMensaje := '� Usuario Bloqueado !';
             lrLoggedIn: sMensaje := '� Usuario Ya Entr� Al Sistema !';
             lrSystemBlock: sMensaje := '� Sistema Bloqueado !';
        end;
   end;

   procedure AgregaResultado( Datos: TZetaXMLNode; const sTag: String );
   const
        K_TAG_STATUS = 'STATUS';
        K_TAG_USERNAME = 'USERNAME';
        K_TAG_CLAVE = 'CLAVE';
        K_TAG_US_CODIGO = 'US_CODIGO';
        K_TAG_PAGINA = 'PAGINA';
        K_TAG_NOMBRE = 'NOMBRE';
        K_TAG_CB_CODIGO = 'CB_CODIGO';
        K_TAG_CM_EMPRESA = 'CM_EMPRESA';
   var
      Nodo: TZetaXMLNode;
   begin
        Nodo := Datos.NewChild( sTag, VACIO );
        with Nodo do
        begin
             Nodo.NewChild( K_TAG_STATUS, IntToStr( Ord( eReply ) ) );
             Nodo.NewChild( K_TAG_USERNAME, sUsuario );
             Nodo.NewChild( K_TAG_CLAVE, sPassword );
             Nodo.NewChild( K_TAG_US_CODIGO, IntToStr( iUsuario ) );
             Nodo.NewChild( K_TAG_PAGINA, IntToStr( iPagina ) );
             Nodo.NewChild( K_TAG_NOMBRE, sNombre );
             Nodo.NewChild( K_TAG_CB_CODIGO, IntToStr( iEmpleado ) );
             Nodo.NewChild( K_TAG_CM_EMPRESA, sEmpresa );
        end;
   end;

   procedure AgregaDescripciones;
   var
      NodoDescripcion: TZetaXMLNode;
   begin
        with FXML do
        begin
             NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, K_NOMBRE_DOCUMENTO );
             AgregaOnLoad( NodoDescripcion, K_ONLOAD_DOCUMENTO );
             NodoDescripcion.NewChild( K_TAG_MES, MesConLetraMes( TheMonth( Date ) ) );
             NodoDescripcion.NewChild( K_TAG_ANNO, IntToStr( TheYear( Date ) ) );
             AgregaResultado( NodoDescripcion, K_TAG_RESULTADO );
        end;
   end;

   procedure AgregaEncabezado;
   var
      NodoSeccion: TZetaXMLNode;
      sRutaPag: String;
   begin
        with FXML do
        begin
             NodoSeccion := SetSeccion( Documento.GetRoot );
             NodoSeccion.NewChild( K_TAG_TITULO, 'Bienvenido a' );
             NodoSeccion := SetSeccion( Documento.GetRoot );
             with oZetaProvider do
             begin
                  sRutaPag := GetGlobalString( K_GLOBAL_RUTA_PAGINA );
                  SetTagLigaImagen( NodoSeccion, sRutaPag, GetGlobalString( K_GLOBAL_LOGOBIG ), sRutaPag, 'new' );
             end;
             NodoSeccion := SetSeccion( Documento.GetRoot );
             if strLleno( sMensaje ) then
             begin
                  if ( eReply in [ lrOK, lrChangePassword, lrExpiredPassword ] ) then
                  begin
                       AgregaTexto( NodoSeccion, sNombre );
                       AgregaEspacio( NodoSeccion );
                  end;
                  AgregaTexto( NodoSeccion, sMensaje );
             end
             else
             begin
                  AgregaTexto( NodoSeccion, sNombre );
             end;
        end;
   end;

   procedure AgregaFormas;
   var
      NodoForma, NodoSeccion, NodoMaster, NodoRenglon: TZetaXMLNode;
   begin
        with FXML do
        begin
             SetForma( Documento.GetRoot, 'frminicio', 'inicio.asp', TRUE, VACIO, VACIO, VACIO, 'portal' );
             if ( eReply = lrOk ) and ( iUsuario <> oZetaProvider.GetGlobalInteger( K_GLOBAL_USER_DEFAULT ) ) then   // Login Correcto y no es Usuario Default
             begin
                  NodoForma := SetForma( Documento.GetRoot, 'frmFijo', 'fijo.asp', TRUE, VACIO, VACIO, VACIO, 'fijo' );
                  AgregaLinea( NodoForma );
                  NodoForma.NewChild( K_TAG_TITULO, 'Mis Aplicaciones' );
                  NodoSeccion := SetSeccion( NodoForma );
                  { Esta seccion debe ser configurable - Tipo Local Settings de Windows - Aplicaciones Generales e Individuales }
                    SetTagLiga( NodoSeccion, Format( 'http://www.tress.com.mx/Portal/Rolodex/Rolodex.aspx?Usuario=%d', [ iEmpleado ] ), 'Rolodex', VACIO, 'portal' );
                    //SetTagLiga( NodoSeccion, 'http://Dell-German/Rolodex/Rolodex.aspx', 'Rolodex', VACIO, 'portal' );
                    //SetTagLiga( NodoSeccion, 'http://dell-eduardo/Portal/Rolodex/RolMatch.asp?BTNBUSCAR=NoBuscar', 'Rolodex', VACIO, 'portal' );
                    AgregaEspacio( NodoSeccion );
                    //SetTagLiga( NodoSeccion, 'http://www.tress.com.mx/Portal/Rolodex/RolMatch.asp?BTNBUSCAR=NoBuscar', 'Rolodex', VACIO, 'portal' );
                    //SetTagLiga( NodoSeccion, 'http://dell-eduardo/Portal/Rolodex/RolMatch.asp?BTNBUSCAR=NoBuscar', 'Rolodex', VACIO, 'portal' );
                    //AgregaEspacio( NodoSeccion );
                    if ( ( iEmpleado <> 0 ) and ( sEmpresa <> VACIO ) ) then
                    begin
                         SetTagLiga( NodoSeccion, Format( 'http://www.tress.com.mx/PlanCarrera/Principal.asp?Empleado=%d&Empresa=%s&Usuario=%d&Nombre=%s',
                                                          [ iEmpleado, sEmpresa, iUsuario, sNombre ] ), 'Plan De Carrera', VACIO, 'new' );
                         AgregaEspacio( NodoSeccion );
                    end;
                    SetTagLiga( NodoSeccion, Format( 'http://www.tress.com.mx/LaborWeb/FVistaSemanal.aspx?Empleado=%d&Nombre=%s',
                                                     [ iEmpleado, sNombre ] ), 'Labor', VACIO, 'portal' );
                    AgregaEspacio( NodoSeccion );
                    SetTagLiga( NodoSeccion, 'http://Dell-Marcos/Celulares/Celulares.aspx', 'Mensajes Escritos', VACIO, 'portal' );
                    AgregaEspacio( NodoSeccion );
                    SetTagLiga( NodoSeccion, Format( 'http://www.tress.com.mx/Portal/CambioPswd.asp?USUARIO=%d&NOMBRE=%s',
                                                     [ iUsuario, sNombre ] ), 'Cambio de Clave', VACIO, 'portal' );
                    //SetTagLiga( NodoSeccion, Format( 'http://dell-eduardo/Portal/CambioPswd.asp?USUARIO=%d&NOMBRE=%s',
                    //                                 [ iUsuario, sNombre ] ), 'Cambio de Clave', VACIO, 'portal' );
                    AgregaEspacio( NodoSeccion );
                  SetBoton( NodoForma, 'Salir', 'btnAccion' );
                  AgregaFooter( NodoForma );
             end
             else if ( eReply in [ lrChangePassword, lrExpiredPassword ] ) then
             begin
                  NodoForma := SetForma( Documento.GetRoot, 'frmFijo', 'fijo.asp', TRUE, VACIO, 'return ValidaCambioClave(this)', 'PonerFocus( Clave )', 'fijo' );
                  AgregaHiddenEdit( NodoForma, 'MinLongitud', IntToStr( iPasswordLongitud ) );
                  AgregaHiddenEdit( NodoForma, 'MinLetras', IntToStr( iPasswordLetras ) );
                  AgregaHiddenEdit( NodoForma, 'MinDigitos', IntToStr( iPasswordDigitos ) );
                  AgregaHiddenEdit( NodoForma, 'hdUsuario', IntToStr( iUsuario ) );
                  AgregaHiddenEdit( NodoForma, 'hdClaveActual', sPassword );
                  NodoMaster := SetMaster( NodoForma );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagLbl( 'Nueva Clave:' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagEditPswd( VACIO, 'Clave', '20', '14' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagLbl( 'Confirmaci�n:' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagEditPswd( VACIO, 'Confirmacion', '20', '14' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagBoton( 'Registrar', 'btnAccion' ), taCenter );
                  AgregaFooter( NodoForma, FALSE );
             end
             else                                                // Login inv�lido o es usuario default
             begin
                  NodoForma := SetForma( Documento.GetRoot, 'frmFijo', 'fijo.asp', TRUE, VACIO, 'return ValidaLogin(this)', 'PonerFocus( Usuario )', 'fijo' );
                  NodoMaster := SetMaster( NodoForma );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagLbl( 'Usuario:' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagEdit( VACIO, 'Usuario', '20', '15' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagLbl( 'Clave:' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagEditPswd( VACIO, 'Clave', '20', '14' ) );
                  NodoRenglon := SetRenglon( NodoMaster );
                    SetContenido( NodoRenglon, GetTagBoton( 'Entrar', 'btnAccion' ), taCenter );
                  AgregaFooter( NodoForma, FALSE );
             end;
        end;
   end;
{$endif}

begin
     {$ifdef CHILKAT}
     sMensaje := VACIO;
     iUsuario := 0;           // Investigar Datos de Portal ( Paginas, Empleado, Empresa )
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          InitGlobales;
     end;
     FLoginObj := CreateComObject( CLASS_dmServerLogin ) as IdmServerLogin;
     try
        FXML := TZetaXML.Create( oZetaProvider );
        try
           with FXML do
           begin
                if LoadParametros( Parametros ) then
                begin
                     try
                        AsignaValoresLogin;
                        if ( ParamAsString( 'ACCION' ) = K_ACCION_REGISTRAR ) then        // Valida si hay que cambiar Password
                        begin
                             if CambiaClaveUsuario then
                                sClave := ParamAsString( 'CLAVENUEVA' );
                        end;
                        eReply := eLogReply( FLoginObj.UsuarioLogin( sUsuario, sClave, VACIO, True, oDatos, oSentinel, iIntentos ) );
                        if ( eReply in [ lrOK, lrChangePassword, lrExpiredPassword ] ) then
                        begin
                             GetDatosValues;
                             GetPortalValues;
                        end;
                        if strVacio( sMensaje ) then    // Si no se tiene un Mensaje de Error Anterior
                           SetMensajesValue;
                     except
                        on Error: Exception do
                        begin
                             eReply := lrFailure;
                             sMensaje := 'Error Al Validar Usuario: ' + Error.Message;
                        end;
                     end;
                     // Generar XML de Salida
                     AgregaDescripciones;
                     AgregaEncabezado;
                     AgregaFormas;
                end
                else
                    BuildErrorXML;
                Result := GetDocXML;
           end;
        finally
           FreeAndNil( FXML );
        end;
     finally
        FLoginObj := nil;
     end;
     {$endif}
     SetComplete;
end;

{
function TdmServerPortal.UsuarioLogin(const Parametros: WideString): WideString;
const
     K_MENSAJE = 'Usuario o Clave Incorrecto';
     K_LOGIN_OK = 4;
     K_LOGIN_DENIED = 1;
     K_LOGIN_BLOCKED = 3;
var
   oNodoRaiz: TZetaXMLNode;
   FCursor: TZetaCursor;
   iUsuario, iPagina, iCbCodigo, iResultado, iIntentos: Integer;
   sNombre, sMensaje, sRutaPag, sPathLogo, sUsuario, sPassword, sCmCodigo, sClave, sClaveDec: String;
   lEncriptado: Boolean;
   oDatos, oAutorizacion: OleVariant;

   procedure AsignaGlobales;
   begin
        with oZetaProvider do
        begin
             EmpresaActiva := Comparte;
             InitGlobales;
             iUsuario := GetGlobalInteger( K_GLOBAL_USER_DEFAULT );
             sPathLogo := GetGlobalString( K_GLOBAL_LOGOBIG );
             sRutaPag  := GetGlobalString( K_GLOBAL_RUTA_PAGINA );
        end;
   end;
begin
     iResultado:= 1;
     iUsuario := 0;
     iPagina  := 1;
     iCbCodigo:= 0;
     sNombre  := VACIO;
     sMensaje := VACIO;
     sRutaPag := VACIO;
     sPathLogo := VACIO;
     sCmCodigo := VACIO;
     sClave := VACIO;
     sUsuario := VACIO;
     FLoginObj := CreateComObject( CLASS_dmServerLogin ) as IdmServerLogin;
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     AsignaGlobales;
                     sUsuario := ParamAsString( 'US_CORTO' );
                     sClave := ParamAsString( 'US_PASSWRD' );
                     lEncriptado := zStrToBool(ParamAsString( 'ENCRIPTADO' ));
                     if lEncriptado then
                        sClaveDec := Decrypt( sClave )
                     else
                         sClaveDec := sClave;
                     iResultado := FLoginObj.UsuarioLogin( sUsuario, sClaveDec, VACIO, True, oDatos, oAutorizacion, iIntentos);
                     ShowMessage( IntToStr( iResultado ) );
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_LOGIN_USUARIO ), [ EntreComillas( ParamAsString( 'US_CORTO' ) ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                sPassword := FieldByName( 'US_PASSWRD' ).AsString;
                                if not lEncriptado then
                                   sPassword := Decrypt( sPassword );
                                if IsEmpty then
                                   //MV-23/Dic/2002: Se comento  este Mensaje para que se tenga el mismo comportamiento que en TRESS
                                   sMensaje := K_MENSAJE
                                else if ( sPassword <> ParamAsString( 'US_PASSWRD' ) ) then
                                   sMensaje := K_MENSAJE
                                else if( iResultado = K_LOGIN_BLOCKED )then
                                     sMensaje := Format( 'Usuario %s Bloqueado', [ sUsuario ] )
                                else
                                begin
                                     iUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
                                     iPagina  := FieldByName( 'US_PAGINAS' ).AsInteger;
                                     sNombre  := FieldByName( 'US_NOMBRE' ).AsString;
                                     iCbCodigo:= FieldByName( 'CB_CODIGO' ).AsInteger;
                                     sCmCodigo:= FieldByName( 'CM_CODIGO' ).AsString;
                                     sPassword:= FieldByName( 'US_PASSWRD' ).AsString;
                                end;
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Validar Usuario: ' + sUsuario + ' ' + Error.Message );
                              end;
                        end;
                     finally
                            FreeAndNil( FCursor );
                     end;
             except
                on Error: Exception do
                begin
                     BuildErrorXML( 'Error Al Conectar Base de Datos: ' + Error.Message );
                end;
             end;
                  oNodoRaiz := Documento.GetRoot;
                  oNodoRaiz.NewChild( 'US_CODIGO', IntToStr( iUsuario ) );
                  oNodoRaiz.NewChild( 'US_PAGINAS', IntToStr( iPagina ) );
                  oNodoRaiz.NewChild( 'US_NOMBRE', sNombre );
                  oNodoRaiz.NewChild( 'LOGOBIG', sPathLogo );
                  oNodoRaiz.NewChild( 'RUTAPAG', sRutaPag );
                  oNodoRaiz.NewChild( 'MENSAJE', sMensaje );
                  oNodoRaiz.NewChild( 'CB_CODIGO', inttoStr(iCbCodigo) );
                  oNodoRaiz.NewChild( 'CM_CODIGO', sCmCodigo );
                  oNodoRaiz.NewChild( 'CLAVE', sPassword );
                  oNodoRaiz.NewChild( 'StatusLogin', IntToStr( iResultado ) );
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
            FLoginObj := nil;
     end;
     SetComplete;
end;
}

function TdmServerPortal.GetInicio(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   FCursor: TZetaCursor;
   oPagina, oColumna, oSeccion, oOtrasPag, oNodoTemp: TZetaXMLNode;
   iCol, iTotCols, iTotSecc: Integer;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     // Paginas
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_PAGINAS ), [ ParamAsInteger( 'USUARIO' ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                if not IsEmpty then
                                begin
                                     oOtrasPag := Documento.GetRoot.FindOrAddNewChild( 'OTRAS_PAG' );
                                     while not EOF do
                                     begin
                                          if ( FieldByName( 'PA_ORDEN' ).AsInteger = ParamAsInteger( 'PAGINA' ) ) then
                                          begin
                                               oPagina := Documento.GetRoot.FindOrAddNewChild( 'PAGINA' );
                                               oNodoTemp := oPagina;
                                          end
                                          else
                                              oNodoTemp := oOtrasPag.NewChild( 'PAGINA', '' );
                                          with oNodoTemp do
                                          begin
                                               AddAttribute( 'US_CODIGO', ParamAsString( 'USUARIO' ) );
                                               AddAttribute( 'NUMERO', FieldByName( 'PA_ORDEN' ).AsString );
                                               AddAttribute( 'PA_TITULO', FieldByName( 'PA_TITULO' ).AsString );
                                          end;
                                          Next;
                                     end;
                                end;
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Obtener Informaci�n de P�ginas de Inicio: ' + Error.Message );
                              end;
                        end;
                     finally
                            FreeAndNil( FCursor );
                     end;
                     // Contenido
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_CONTIENE ), [ ParamAsInteger( 'USUARIO' ), ParamAsInteger( 'PAGINA' ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                if not IsEmpty then
                                begin
                                     iTotCols := 0;
                                     iCol := 0;
                                     iTotSecc := 0;
                                     while not EOF do
                                     begin
                                          if ( iCol <> FieldByName( 'CT_COLUMNA' ).AsInteger ) then
                                          begin
                                               if ( iTotSecc > 0 ) then
                                                  oColumna.AddAttribute( 'SECCIONES', IntToStr( iTotSecc ) );
                                               iCol := FieldByName( 'CT_COLUMNA' ).AsInteger;
                                               Inc( iTotCols );
                                               iTotSecc := 0;
                                               oColumna := oPagina.NewChild( 'COLUMNA', '' );
                                          end;
                                          oSeccion := oColumna.NewChild( 'SECCION', '' );
                                          oSeccion.AddAttribute( 'CT_TIPO', IntToStr( FieldByName( 'CT_TIPO' ).AsInteger ) );
                                          case FieldByName( 'CT_TIPO' ).AsInteger of
                                               0 : AgregaSeccReporte( FieldByName( 'CT_MOSTRAR' ).AsInteger, FieldByName( 'CT_REPORTE' ).AsInteger, oSeccion );
                                               1 : AgregaSeccGrafica( oSeccion );
                                               2 : AgregaSeccTitular( oSeccion );
                                               3 : AgregaNoticias( FieldByName( 'CT_MOSTRAR' ).AsInteger, TRUE, oSeccion );
                                               4 : AgregaEventos( FieldByName( 'CT_MOSTRAR' ).AsInteger, ParamAsDate( 'FECHAINI' ), oSeccion, TRUE );
                                          end;
                                          Inc(iTotSecc);
                                          Next;
                                     end;
                                     oPagina.AddAttribute( 'NUMCOLS', IntToStr( iTotCols ) );
                                end;
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Obtener Informaci�n de P�gina Inicial: ' + Error.Message );
                              end;
                        end;
                     finally
                            FreeAndNil( FCursor );
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Conectar Base de Datos: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocumentoXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaSeccReporte( const iMostrar, iCodigo: Integer; oNodo: TZetaXMLNode );
var
   iColumnas: Integer;
begin
     iColumnas := AgregaReportal( iCodigo, iMostrar, oNodo );
     AgregaRepCols( iCodigo, oNodo );
     AgregaRepData( iCodigo, iColumnas, iMostrar, oNodo );
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaSeccGrafica( oNodo: TZetaXMLNode );
begin
     // PENDIENTE;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaSeccTitular( oNodo: TZetaXMLNode );
var
   FCursor: TZetaCursor;
begin
     with FXML do
     begin
          try
             FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_INFO_TITULAR ), [ oZetaProvider.GetGlobalInteger( K_GLOBAL_TITULAR ) ] ) );
             try
                try
                   with FCursor do
                   begin
                        Active := True;
                        FieldByName( 'NT_IMAGEN' ).OnGetText := CampoImagenGetText;
                        AddRegistro( oNodo, 'TITULAR', FCursor );
                        Active := False;
                   end;
                except
                      on Error: Exception do
                      begin
                           BuildErrorXML( 'Error Al Obtener Informaci�n de Titular: ' + Error.Message );
                      end;
                end;
             finally
                    FreeAndNil( FCursor );
             end;
          except
                on Error: Exception do
                begin
                     BuildErrorXML( 'Error al Consultar Datos de Titular: ' + Error.Message );
                end;
          end;
     end;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaNoticias( const iCuantos: Integer; const lShowHotNews: Boolean; oNodo: TZetaXMLNode );
const
     K_HOT_NEWS_SCRIPT = ', ( SELECT 1 from NOTICIA B where B.NT_FECHA = %s and B.NT_FOLIO = A.NT_FOLIO ) as HOT_NEWS';
var
   FCursor: TZetaCursor;

   function GetShowHotNews: String;
   begin
        if lShowHotNews then
           Result := Format( K_HOT_NEWS_SCRIPT, [ DateToSTRSQLC( FXML.ParamAsDate( 'FECHAINI' ) ) ] )
        else
           Result := VACIO;
   end;

begin
     with FXML do
     begin
          try
             FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_LISTA_NOTICIAS_100 ), [ GetShowHotNews ] ) );
             try
                try
                   with FCursor do
                   begin
                        Active := True;
                        FieldByName( 'NT_FOLIO' ).DisplayLabel := 'Folio';
                        FieldByName( 'NT_FECHA' ).DisplayLabel := 'Fecha';
                        FieldByName( 'NT_HORA' ).DisplayLabel := 'Hora';
                        FieldByName( 'NT_TITULO' ).DisplayLabel := 'T�tulo';
                        AddDataset2( oNodo, 'NOTICIAS', FCursor, iCuantos );
                        Active := False;
                   end;
                except
                      on Error: Exception do
                      begin
                           BuildErrorXML( 'Error Al Crear Lista de Noticias: ' + Error.Message );
                      end;
                end;
             finally
                    FreeAndNil( FCursor );
             end;
          except
                on Error: Exception do
                begin
                     FXML.BuildErrorXML( 'Error al Consultar Datos de Noticias: ' + Error.Message );
                end;
          end;
     end;
end;
{$endif}

function TdmServerPortal.GetNoticias100(const Parametros: WideString): WideString;
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             try
                with oZetaProvider do
                begin
                     EmpresaActiva := Comparte;
                     InitGlobales;
                end;
                AgregaNoticias( 0, FALSE, Documento.GetRoot );
             except
                on Error: Exception do
                begin
                     BuildErrorXML( 'Error Al Conectar Base de Datos: ' + Error.Message );
                end;
             end;
             Result := GetDocumentoXML;
        end;
     finally
        FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetNoticia(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   FCursor: TZetaCursor;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_INFO_NOTICIA ), [ ParamAsInteger( 'FOLIO' ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                FieldByName( 'NT_HORA' ).OnGetText := CampoHoraGetText;
                                FieldByName( 'NT_IMAGEN' ).OnGetText := CampoImagenGetText;
                                AddDataset2( Documento.GetRoot, 'NOTICIA', FCursor );
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Obtener Informaci�n de Noticia ' + Error.Message );
                              end;
                        end;
                     finally
                            FreeAndNil( FCursor );
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Consultar Datos de Noticia: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocumentoXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaEventos( const iCuantos: Integer; const dFecha: TDate; oNodo: TZetaXMLNode; const lInicio: Boolean );
var
   FCursor: TZetaCursor;
   sQuery: String;
begin
     with FXML do
     begin
          try
             if lInicio then
                sQuery := Format( GetScript( Q_LISTA_EVENTOS ), [ DateToStrSQLC( dFecha ) ] )
             else
                 sQuery := GetScript( Q_LISTA_EVENTOS_100 );
             FCursor := oZetaProvider.CreateQuery( sQuery );
             try
                try
                   with FCursor do
                   begin
                        Active := True;
                        AddDataset2( oNodo, 'EVENTOS', FCursor, iCuantos );
                        Active := False;
                   end;
                except
                      on Error: Exception do
                      begin
                           BuildErrorXML( 'Error Al Crear Lista de Eventos ' + Error.Message );
                      end;
                end;
             finally
                    FreeAndNil( FCursor );
             end;
          except
                on Error: Exception do
                begin
                     BuildErrorXML( 'Error Al Consultar Datos de Eventos: ' + Error.Message );
                end;
          end;
     end;
end;
{$endif}

function TdmServerPortal.GetEventos100(const Parametros: WideString): WideString;
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     AgregaEventos( 0, ParamAsDate( 'FECHAINI' ), Documento.GetRoot, FALSE );
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Conectar Base de Datos: ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocumentoXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetEvento(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   FCursor: TZetaCursor;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_INFO_EVENTO ), [ ParamAsInteger( 'FOLIO' ) ] ) );
                     try
                        try
                           with FCursor do
                           begin
                                Active := True;
                                FieldByName( 'EV_HORA' ).OnGetText := CampoHoraGetText;
                                FieldByName( 'EV_IMAGEN' ).OnGetText := CampoImagenGetText;
                                AddDataset2( Documento.GetRoot, 'EVENTO', FCursor );
                                Active := False;
                           end;
                        except
                              on Error: Exception do
                              begin
                                   BuildErrorXML( 'Error Al Obtener Informaci�n de Evento ' + Error.Message );
                              end;
                        end;
                     finally
                            FreeAndNil( FCursor );
                     end;
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Obtener Datos de Evento: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocumentoXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetReporte(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   iColumnas: Integer;

   procedure AgregaGlobales;
   var
      NodoGlobal: TZetaXMLNode;
   begin
        NodoGlobal := FXML.Documento.GetRoot.FindOrAddNewChild( 'GLOBAL' );
        with oZetaProvider do
        begin
             NodoGlobal.AddAttribute( 'G1', GetGlobalString( 1 ) );
        end;
   end;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     AgregaGlobales;
                     iColumnas := AgregaReportal( ParamAsInteger( 'FOLIO' ), 0, Documento.GetRoot );
                     AgregaRepCols( ParamAsInteger( 'FOLIO' ), Documento.GetRoot );
                     AgregaRepData( ParamAsInteger( 'FOLIO' ), iColumnas, 0, Documento.GetRoot );
                  except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Obtener Datos de Reporte: ' + Error.Message );
                        end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocumentoXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

{$ifdef CHILKAT}
function TdmServerPortal.AgregaReportal( const iFolio, iCuantos: Integer; oNodo: TZetaXMLNode ): Integer;
var
   FCursor: TZetaCursor;
   oReportal : TZetaXMLNode;
begin
     Result := 0;
     with FXML do
     begin
          FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_REP_PORTAL ), [ iFolio ] ) );
          try
             try
                with FCursor do
                begin
                     Active := True;
                     Result := FieldByName( 'RP_COLS' ).AsInteger;
                     FieldByName( 'RP_HORA' ).OnGetText := CampoHoraGetText;
                     AddRegistro( oNodo, 'REPORTAL', FCursor );
                     if ( iCuantos > 0 ) then
                     begin
                          oReportal := oNodo.FindOrAddNewChild( 'REPORTAL' );
                          if ( FieldByName( 'RP_ROWS' ).AsInteger > iCuantos ) then
                             oReportal.AddAttribute( 'RP_COMPLETO', '0' )
                          else
                             oReportal.AddAttribute( 'RP_COMPLETO', '1' );
                     end;
                     Active := False;
                end;
             except
                   on Error: Exception do
                   begin
                        BuildErrorXML( 'Error Al Obtener Informaci�n de Reportal ' + Error.Message );
                   end;
             end;
          finally
                 FreeAndNil( FCursor );
          end;
     end;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaRepCols( const iFolio: Integer; oNodo: TZetaXMLNode );
var
   Registro: TZetaXMLNode;
   FCursor: TZetaCursor;
begin
     with FXML do
     begin
          FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_REP_COLS ), [ iFolio ] ) );
          try
             Registro := oNodo.FindOrAddNewChild( 'REP_COLS' );
             try
                with FCursor do
                begin
                     Active := True;
                     if not IsEmpty then
                     begin
                          while not EOF do
                          begin
                               with aColumnas[FieldByName( 'RC_ORDEN' ).AsInteger] do
                               begin
                                    Titulo := FieldByName( 'RC_TITULO' ).AsString;
                                    Tipo   := FieldByName( 'RC_TIPO' ).AsInteger;
                                    Ancho  := FieldByName( 'RC_ANCHO' ).AsInteger;
                               end;
                               Registro.AddAttribute( 'RC_TIPO' + IntToStr( FieldByName( 'RC_ORDEN' ).AsInteger ),
                                                      IntToStr( FieldByName( 'RC_TIPO' ).AsInteger ) );
                               Next;
                          end;
                     end;
                     Active := False;
                end;
             except
                   on Error: Exception do
                   begin
                        BuildErrorXML( 'Error Al Obtener Informaci�n de Columnas ' + Error.Message );
                   end;
             end;
          finally
                 FreeAndNil( FCursor );
          end;
     end;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.AgregaRepData( const iFolio, iColumnas, iCuantos: Integer; oNodo: TZetaXMLNode );
var
   FCursor: TZetaCursor;

   function GetListaCampos: String;
   var
      i: Integer;
   begin
        Result := VACIO;
        for i := 1 to iColumnas do
            Result := ConcatString( Result, 'RD_DATO' + IntToStr(i), ',' );
   end;

var
   i: Integer;
begin
     with FXML do
     begin
          FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_REP_DATA ), [ GetListaCampos, iFolio ] ) );
          try
             try
                with FCursor do
                begin
                     Active := True;
                     for i:= 1 to iColumnas do
                         with FieldByName( 'RD_DATO' + IntToStr(i) ) do
                         begin
                              OnGetText := RD_DATOGetText;
                              DisplayLabel := aColumnas[i].Titulo;
                         end;
                     AddDataset2( oNodo, 'REP_DATA', FCursor, iCuantos );
                     Active := False;
                end;
             except
                   on Error: Exception do
                   begin
                        BuildErrorXML( 'Error Al Obtener Datos del Reporte ' + Error.Message );
                   end;
             end;
          finally
                 FreeAndNil( FCursor );
          end;
     end;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.RD_DATOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
   iCol: Integer;
begin
     iCol := StrToIntDef( Copy( Sender.FieldName, 8, 1 ), 0 );
     case aColumnas[iCol].Tipo of
          7 : Text := 'mailto:' + Sender.AsString;
          1 : Text := FormatFloat( '$0.00', Sender.AsFloat )
     else
          Text := Sender.AsString;
     end;
end;
{$endif}

function TdmServerPortal.GetDocumentos(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   oPagina: TZetaXMLNode;
   FCursor: TZetaCursor;
   iCuantos, iInicial, iFinal, iTotal, iPagina, iPaginas, iPagAnt, iPagSig: Integer;

   procedure AgregaGlobal;
   var
      NodoGlobal: TZetaXMLNode;
   begin
        NodoGlobal := FXML.Documento.GetRoot.FindOrAddNewChild( 'GLOBAL' );
        NodoGlobal.AddAttribute( 'G3', oZetaProvider.GetGlobalString( 3 ) );
   end;

   function GetTotalRegistros: Integer;
   begin
        Result := 0;
        FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_COUNT_DOCUMENTOS ), [ EntreComillas( '%' + UpperCase(FXML.ParamAsString( 'PALABRA' )) + '%' ) ] ) );
        try
           try
              with FCursor do
              begin
                   Active := True;
                   Result := FieldByName( 'CUANTOS' ).AsInteger;
                   Active := False;
              end;
           except
                 on Error: Exception do
                 begin
                      FXML.BuildErrorXML( 'Error Al Obtener Cantidad de Documentos ' + Error.Message );
                 end;
           end;
        finally
               FreeAndNil( FCursor );
        end;
   end;

   procedure SetValores;
   begin
        iTotal   := GetTotalRegistros;
        iCuantos := oZetaProvider.GetGlobalInteger( 4 );
        if ( iTotal > 0 ) then
           iPagina  := iMax( FXML.ParamAsInteger( 'PAGINA' ), 1 )
        else
           iPagina := 0;
        iPagAnt  := iPagina - 1;
        iInicial := ( iPagAnt * iCuantos ) + 1;
        iFinal   := iMin( iTotal, ( iPagina * iCuantos ) );
        iPaginas := iMin( 1, ( iTotal Mod iCuantos ) ) + ( iTotal div iCuantos );
        iPagSig  := iPagina + 1;
        if ( iPagSig > iPaginas ) then
           iPagSig := 0;
   end;

   procedure SetPaginaHeader;
   begin
        with FXML do
        begin
             oPagina := Documento.GetRoot.FindOrAddNewChild( 'PAGINA' );
             with oPagina do
             begin
                  AddAttribute( 'NUMERO', IntToStr( iPagina ) );
                  AddAttribute( 'INICIAL', IntToStr( iInicial ) );
                  AddAttribute( 'FINAL', IntToStr( iFinal ) );
                  AddAttribute( 'TOTAL', IntToStr( iTotal ) );
                  AddAttribute( 'PALABRA', ParamAsString( 'PALABRA' ) );
             end;
        end;
   end;

   procedure AgregaOtrasPaginas;
   var
      oOtrasPag: TZetaXMLNode;
      i : Integer;
   begin
        oOtrasPag := FXML.Documento.GetRoot.FindOrAddNewChild( 'OTRAS_PAGINAS' );
        with oOtrasPag do
        begin
             AddAttribute( 'CUANTAS', IntToStr( iPaginas ) );
             AddAttribute( 'SIGUIENTE', IntToStr( iPagSig ) );
             AddAttribute( 'ANTERIOR', IntToStr( iPagAnt ) );
        end;
        for i := 1 to iPaginas do
            with oOtrasPag.NewChild( 'PAGE', '' ) do
                 AddAttribute( 'NUMERO', IntToStr( i ) );
   end;
{$endif}

begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     AgregaGlobal;
                     SetValores;
                     SetPaginaHeader;
                     if ( iPagina > 0 ) then
                     begin
                          FCursor := oZetaProvider.CreateQuery( Format( GetScript( Q_DOCUMENTOS ), [ EntreComillas( '%' + UpperCase(ParamAsString( 'PALABRA' )) + '%' ) ] ) );
                          try
                             try
                                with FCursor do
                                begin
                                     Active := True;
                                     AddDataSet2( oPagina, 'DOCUMENT', FCursor, iCuantos, iInicial );
                                     Active := False;
                                end;
                             except
                                   on Error: Exception do
                                   begin
                                        BuildErrorXML( 'Error Al Obtener Lista de Documentos ' + Error.Message );
                                   end;
                             end;
                          finally
                                 FreeAndNil( FCursor );
                          end;
                          AgregaOtrasPaginas;
                     end;
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Obtener Datos de Documentos ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocumentoXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetDirectorios(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   oNodoRaiz: TZetaXMLNode;
   sDirectorio, sDirVirtual: String;
{$endif}
begin
     {$ifdef CHILKAT}
     sDirectorio  := VACIO;
     sDirVirtual := VACIO;
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
              with oZetaProvider do
              begin
                   EmpresaActiva := Comparte;
                   InitGlobales;
                   sDirectorio := GetGlobalString( K_GLOBAL_DIR_EMPLEADOS );
                   sDirVirtual  := GetGlobalString( K_GLOBAL_RUTA_EMPLEADOS );
              end;
              oNodoRaiz := Documento.GetRoot;
              oNodoRaiz.NewChild( 'DIRECTORIO', sDirectorio );
              oNodoRaiz.NewChild( 'DIRVIRTUAL', sDirVirtual );
              Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetMatchTarjetas(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
const
     K_NOMBRE_DOCUMENTO = 'B�squeda de Rolodex';
     K_NOBUSCAR = 'NoBuscar';
     K_REGISTROS = 30;
     K_TARJETA = ' Tarjeta';
     K_LIGA = 'RolMatch.asp?PALABRA=%s&CAMPOORDEN=%s&ORDEN=%s&EDICION=edicion';
     K_MESS_OK = 'El Contacto Fu� %s en Rolodex';
     K_MESS_ERROR = 'Error';
     aCampoOrden: array[ eCampoOrden ] of PChar = ( 'TJ_EMPRESA', 'TJ_NOMBRES', 'TJ_TELEFON' );
     aOrden: array[ eOrden ] of PChar = ( 'ASC', 'DESC' );
     aDescripAccion: array[ eAccionTarjeta ] of PChar = ( 'Agregado','Modificado','Borrado' );
     aDescripAccionError: array[ eAccionTarjeta ] of PChar = ( 'Agregar','Editar','Borrar' );
var
   NodoRenglon, NodoDescripcion, DocTarjeta, NodoDescrip, NodoTarjeta: TZetaXMLNode;
   NodoTabla, NodoForma: TZetaXMLNode;
   Accion : eAccionTarjeta;
   FCursor: TZetaCursor;
   iCampoOrden, iOrden, iPagina: integer;
   sUsuario, sEdicion, sMensaje, sPalabra, sAccion, sBtnBuscar, sOrden, sCampoOrden: string;
   lOk: boolean;

   procedure SetMasterinfo;
   var
      NodoTabla, NodoForma, NodoRenglon, NodoContenido: TZetaXMLNode;

   begin
        with FXML do
        begin
             NodoTabla := SetTablaMaster( Documento.GetRoot );
             NodoForma := SetForma( NodoTabla, 'formBusqueda', 'RolMatch.asp', FALSE );
             AgregaHiddenEdit( NodoForma, 'CAMPOORDEN', sCampoOrden );
             AgregaHiddenEdit( NodoForma, 'ORDEN', sOrden );
             AgregaHiddenEdit( NodoForma, 'ACCION', sAccion );
             AgregaHiddenEdit( NodoForma, 'PAGINA', IntToStr( iPagina ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Nombre:' ), taRightJustify );
               NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sPalabra, 'PALABRA', '44', '40' ) );
               NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagBoton( 'Buscar', 'BTNBUSCAR' ) );
               SetContenido( NodoRenglon, GetTagBoton( 'Agregar', 'BTNAGREGAR', eBtnNormal, 'LlamaEdicion( formBusqueda )' ) );
        end;
   end;

   function GetLiga: string;
   begin
        Result := Format( K_LIGA, [ sPalabra, sCampoOrden, IntToStr( iOrden ) ] );
   end;

   function GetAccion( const sAccion: String ): eAccionTarjeta;
   begin
        if ( sAccion = K_AGREGAR ) then
           Result := eaAgregar
        else if ( sAccion = K_GRABAR ) then
           Result := eaEditar
        else
           Result := eaBorrar;
   end;

   function GetScriptAccion: integer;
   begin
        case Accion of
             eaAgregar : Result := Q_INSERT_TARJETA;
             eaEditar  : Result := Q_UPDATE_TARJETA;
             eaBorrar  : Result := Q_DELETE_TARJETA;
        else
             Result := 0;
        end;
   end;

   procedure SetValoresCursor;
   begin
        with FXML, oZetaProvider do
        begin
             ParamAsString( FCursor, 'TJ_EMPRESA', TagAsString( NodoTarjeta, 'EMPRESA', VACIO ) );
             ParamAsString( FCursor, 'TJ_NOMBRES', TagAsString( NodoTarjeta, 'NOMBRES', VACIO ) );
             ParamAsString( FCursor, 'TJ_TELEFON', TagAsString( NodoTarjeta, 'TELEFONOS', VACIO ) );
             ParamAsString( FCursor, 'TJ_DIRECC1', TagAsString( NodoTarjeta, 'DIRECCION', VACIO ) );
             ParamAsString( FCursor, 'TJ_DIRECC2', TagAsString( NodoTarjeta, 'CIUDAD', VACIO ) );
             ParamAsString( FCursor, 'TJ_COMENTA', TagAsString( NodoTarjeta, 'COMENTARIO', VACIO ) );
             ParamAsString( FCursor, 'TJ_EMAIL', TagAsString( NodoTarjeta, 'EMAIL', VACIO ) );
        end;
   end;

   procedure SetLlaveCursor( const sParam: String );
   var
      NodoLlave: TZetaXMLNode;
   begin
        with FXML do
        begin
             NodoLlave := GetNode( NodoTarjeta, sParam );
             oZetaProvider.ParamAsString( FCursor, sParam, NodoLlave.GetAttributeValue( 0 ) );
        end;
   end;

   function GrabaTarjeta: Boolean;
   begin
        with FXML, oZetaProvider do
        begin
             try
                NodoTarjeta := ParamAsNode( 'TARJETA' );
                Accion := GetAccion( sAccion );
                FCursor := CreateQuery( GetScript( GetScriptAccion ) );
                try
                   EmpiezaTransaccion;
                   try
                      if ( Accion in [ eaAgregar, eaEditar ] ) then
                         SetValoresCursor;
                      if ( Accion in [ eaEditar, eaBorrar ] ) then
                      begin
                           SetLlaveCursor( 'EMPRESA' );
                           SetLlaveCursor( 'NOMBRES' );
                      end;
                      Result := ( Ejecuta( FCursor ) > 0 );
                      if Result then
                         sMensaje := Format( K_MESS_OK, [ aDescripAccion[ Accion ] ] )
                      else
                         sMensaje := K_MESS_ERROR;
                      TerminaTransaccion( True );
                   except
                         on Error: Exception do
                         begin
                              RollBackTransaccion;
                              Result := FALSE;
                              sMensaje := K_MESS_ERROR;
                              if PK_Violation( Error ) then
                                 sMensaje := sMensaje + ': ' + 'Los datos del Contacto ya existen'
                              else
                                  sMensaje := sMensaje + ': ' + Error.Message;
                              BuildErrorXML( sMensaje );
                         end;
                   end;
                finally
                   FreeAndNil( FCursor );
                end;
             except
                   on Error: Exception do
                   begin
                        Result := FALSE;
                        sMensaje := K_MESS_ERROR;
                        BuildErrorXML( K_MESS_ERROR + ': ' + Error.Message );
                   end;
             end;
        end;
   end;

   procedure LlenaDataSet;
   var
      FCursor: TZetaCursor;

   begin
        if ( iPagina = 0 ) then
           iPagina := 1;
        FCursor := oZetaProvider.CreateQuery;
        try
           with FXML do
           begin
                //Tabla de Resultados
                FCursor.AfterOpen := ResultadosAfterOpen;
                if StrLleno( sPalabra ) then
                begin
                     sPalabra := UpperCase( sPalabra );
                     AgregaXMLDataSet( Documento, VACIO, Format( GetScript( Q_ROL_MATCH ), [ EntreComillas( '%' + sPalabra + '%' ), aCampoOrden[ eCampoOrden( iCampoOrden ) ], aOrden[ eOrden( iOrden ) ] ] ), FCursor, GetLiga, K_REGISTROS, iPagina );
                end
                else
                    AgregaXMLDataSet( Documento, VACIO, Format( GetScript( Q_ROL_MATCH_TOTAL ), [ aCampoOrden[ eCampoOrden( iCampoOrden ) ], aOrden[ eOrden( iOrden ) ] ] ), FCursor, GetLiga, K_REGISTROS, iPagina );
           end;
        finally
           FreeAndNil( FCursor );
        end;
   end;
{$endif}
begin
     {$ifdef CHILKAT}
     iCampoOrden := 0;
     iOrden := 0;
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  sPalabra := ParamAsString( 'PALABRA' );
                  iCampoOrden := ParamAsInteger( 'CAMPOORDEN' );
                  iOrden := ParamAsInteger( 'ORDEN' );
                  sBtnBuscar := ParamAsString( 'BTNBUSCAR' );
                  iPagina := ParamAsInteger( 'PAGINA' );
                  sAccion := ParamAsString( 'ACCION' );
                  sEdicion := ParamAsString( 'EDICION' );
                  sUsuario := ParamAsString( 'USUARIO' );
                  sOrden := IntToStr( iOrden );
                  sCampoOrden := IntToStr( iCampoOrden );
                  lOk := TRUE;
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                          if sUsuario <> 'Invalido' then
                          begin
                               if strLleno( sAccion ) then
                               begin
                                    lOk := GrabaTarjeta;
                                    sAccion := VACIO;
                               end;
                               if lOk then
                               begin
                                    NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, K_NOMBRE_DOCUMENTO );
                                    AgregaOnLoad( NodoDescripcion, 'PonerFocus( formBusqueda.PALABRA )' );
                                    if strVacio( sEdicion ) then
                                       iPagina := 0;
                                    SetMasterInfo;
                                    if ( sBtnBuscar <> K_NOBUSCAR ) then
                                    begin
                                         SetClaseFuentePar( 'TD_COLOR', 'TD_COLOR_PAR' );
                                         LlenaDataset;
                                    end;
                                    if HuboErrores then
                                       DocTarjeta := DocErrores
                                    else
                                        DocTarjeta := Documento;
                               end
                               else
                               begin
                                    DocTarjeta := DocErrores;
                                    NodoDescrip := AgregaDescripcionXML( DocTarjeta, K_NOMBRE_MODULO, aDescripAccionError[ Accion ] + ' ' + K_TARJETA );
                                    NodoTabla := SetTablaMaster( DocTarjeta.GetRoot );
                                    NodoForma := SetForma( NodoTabla, 'formGrabar', VACIO, FALSE );
                                    NodoRenglon := SetRenglon( NodoForma );
                                    SetContenido( NodoRenglon, GetTagBoton( 'Intentar de Nuevo', 'BTNREINTENTAR', eBtnNormal, 'history.go(-1)' ), taCenter );
                               end;
                               SetFooterPortal( DocTarjeta.GetRoot );
                          end
                          else
                          begin
                               NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, 'Rolodex' );
                               BuildWarningXML( 'Usuario Invalido para uso de Rolodex' );
                               SetFooterPortal( Documento );
                          end;
                     end;
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Buscar Contactos en Rolodex: ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

{$ifdef CHILKAT}
procedure TdmServerPortal.ResultadosAfterOpen(DataSet: TDataSet);
var
   iCampoOrden: integer;
begin
     with DataSet do
     begin
          with FXML do
          begin
               iCampoOrden := ParamAsInteger( 'CAMPOORDEN' );
               FieldByName( 'INDICE' ).DisplayLabel := '#';

               if ( iCampoOrden = 0 ) then
                  FieldByName( 'TJ_EMPRESA' ).DisplayLabel := GetTagLiga( 'RolMatch.asp?PALABRA='+ParamAsString( 'PALABRA' )+'&CAMPOORDEN=0&ORDEN='+IntToStr( ABS( ParamAsInteger( 'ORDEN' ) - 1 ) )+'&BTNBUSCAR=Buscar', 'Empresa' )
               else
                   FieldByName( 'TJ_EMPRESA' ).DisplayLabel := GetTagLiga( 'RolMatch.asp?PALABRA='+ParamAsString( 'PALABRA' )+'&CAMPOORDEN=0&ORDEN=0&BTNBUSCAR=Buscar', 'Empresa' );

               if ( iCampoOrden = 1 ) then
                  FieldByName( 'TJ_NOMBRES' ).DisplayLabel := GetTagLiga( 'RolMatch.asp?PALABRA='+ParamAsString( 'PALABRA' )+'&CAMPOORDEN=1&ORDEN='+IntToStr( ABS( ParamAsInteger( 'ORDEN' ) - 1 ) )+'&BTNBUSCAR=Buscar', 'Nombres' )
               else
                   FieldByName( 'TJ_NOMBRES' ).DisplayLabel := GetTagLiga( 'RolMatch.asp?PALABRA='+ParamAsString( 'PALABRA' )+'&CAMPOORDEN=1&ORDEN=0&BTNBUSCAR=Buscar', 'Nombres' );

               if ( iCampoOrden = 2 ) then
                  FieldByName( 'TJ_TELEFON' ).DisplayLabel := GetTagLiga( 'RolMatch.asp?PALABRA='+ParamAsString( 'PALABRA' )+'&CAMPOORDEN=2&ORDEN='+IntToStr( ABS( ParamAsInteger( 'ORDEN' ) - 1 ) )+'&BTNBUSCAR=Buscar', 'Tel�fonos' )
               else
                   FieldByName( 'TJ_TELEFON' ).DisplayLabel := GetTagLiga( 'RolMatch.asp?PALABRA='+ParamAsString( 'PALABRA' )+'&CAMPOORDEN=2&ORDEN=0&BTNBUSCAR=Buscar', 'Tel�fonos' );
               FieldByName( 'TJ_EMPRESA' ).OnGetText := TJ_EMPRESAOnGetText;
               FieldByName( 'TJ_NOMBRES' ).OnGetText := TJ_NOMBRESOnGetText;
               FieldByName( 'TJ_TELEFON' ).OnGetText := TJ_TELEFONOnGetText;
               FieldByName( 'INDICE' ).OnGetText := INDICEOnGetText;
          end;
     end;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.TJ_EMPRESAOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    with FXML do
                    begin
                         Text := GetTagLiga( 'RolEdit.asp?EMPRESA='+ ValidaHRef( FieldByName( 'TJ_EMPRESA' ).AsString, TRUE )+'&NOMBRES='+ ValidaHRef( FieldByName( 'TJ_NOMBRES' ).AsString, TRUE )+'&PALABRA='+ ValidaHRef( ParamAsString( 'PALABRA' ), TRUE )+'&ACCION=Grabar&PAGINA='+ParamAsString( 'PAGINA' ), Sender.AsString );
                    end;
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.TJ_NOMBRESOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    with FXML do
                    begin
                         Text := GetTagLiga( 'RolEdit.asp?EMPRESA='+ ValidaHRef( FieldByName( 'TJ_EMPRESA' ).AsString, TRUE )+'&NOMBRES='+ ValidaHRef( FieldByName( 'TJ_NOMBRES' ).AsString, TRUE )+'&PALABRA='+ ValidaHRef( ParamAsString( 'PALABRA' ), TRUE )+'&ACCION=Grabar&PAGINA='+ParamAsString( 'PAGINA' ), Sender.AsString );
                    end;
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.TJ_TELEFONOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    with FXML do
                    begin
                         Text := GetTagLiga( 'RolEdit.asp?EMPRESA='+ ValidaHRef( FieldByName( 'TJ_EMPRESA' ).AsString, TRUE )+'&NOMBRES='+ ValidaHRef( FieldByName( 'TJ_NOMBRES' ).AsString, TRUE )+'&PALABRA='+ ValidaHRef( ParamAsString( 'PALABRA' ), TRUE )+'&ACCION=Grabar&PAGINA='+ParamAsString( 'PAGINA' ), Sender.AsString );
                    end;
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;
{$endif}

{$ifdef CHILKAT}
procedure TdmServerPortal.INDICEOnGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if IsEmpty then
                  Text := VACIO
               else
               begin
                    with FXML do
                    begin
                         Text := GetTagLiga( 'RolEdit.asp?EMPRESA='+ ValidaHRef( FieldByName( 'TJ_EMPRESA' ).AsString, TRUE )+'&NOMBRES='+ ValidaHRef( FieldByName( 'TJ_NOMBRES' ).AsString, TRUE )+'&PALABRA='+ ValidaHRef( ParamAsString( 'PALABRA' ), TRUE )+'&ACCION=Grabar&PAGINA='+ParamAsString( 'PAGINA' ), IntToStr( FXML.RecNo ) );
                    end;
               end;
          end;
     end
     else
         Text:= Sender.AsString;
end;
{$endif}

function TdmServerPortal.GetTarjeta(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
const
     K_TARJETA = ' Tarjeta';
     K_USUARIO_DEF = 'Invalido';
var
   sUsuario, sAccion, sEmpresa, sNombres, sPalabra: String;
   iPagina, iOrden, iCampoOrden: Integer;
   sValEmpresa, sValNombres, sValTelefon, sValDirecc, sValCiudad, sValComenta, sValEmail, sCaption : String;
   NodoDescripcion: TZetaXMLNode;

   procedure GetValues;
   var
      FCursor: TZetaCursor;
   begin
        if ( sAccion = K_AGREGAR ) then
        begin
             sValEmpresa := VACIO;
             sValNombres := VACIO;
             sValTelefon := VACIO;
             sValDirecc  := VACIO;
             sValCiudad  := VACIO;
             sValComenta := VACIO;
             sValEmail   := VACIO;
             sCaption    := K_GRABAR;
        end
        else
        begin
             FCursor := oZetaProvider.CreateQuery( GetScript( Q_OBTENER_INFO ) );
             try
                with FCursor do
                begin
                     oZetaProvider.ParamAsString( FCursor, 'TJ_EMPRESA', sEmpresa );
                     oZetaProvider.ParamAsString( FCursor, 'TJ_NOMBRES', sNombres );
                     Active := True;
                     sValEmpresa := FieldByName( 'TJ_EMPRESA' ).AsString;
                     sValNombres := FieldByName( 'TJ_NOMBRES' ).AsString;
                     sValTelefon := FieldByName( 'TJ_TELEFON' ).AsString;
                     sValDirecc  := FieldByName( 'TJ_DIRECC1' ).AsString;
                     sValCiudad  := FieldByName( 'TJ_DIRECC2' ).AsString;
                     sValComenta := FieldByName( 'TJ_COMENTA' ).AsString;
                     sValEmail   := FieldByName( 'TJ_EMAIL' ).AsString;
                     sCaption    := K_GRABAR;
                     Active := False;
                end;
             finally
                    FreeAndNil( FCursor );
             end;
        end;
   end;

   procedure SetMasterInfo;
   var
      NodoTabla, NodoForma, NodoRenglon, NodoContenido: TZetaXMLNode;

   begin
        with FXML do
        begin
             NodoTabla := SetTablaMaster( Documento.GetRoot );
             NodoForma := SetForma( NodoTabla, 'formTarjeta', 'RolMatch.asp', FALSE, 'ResetTarjeta(this)', 'return ValidaTarjeta(this)' );
             AgregaHiddenEdit( NodoForma, 'PALABRA', sPalabra );
             AgregaHiddenEdit( NodoForma, 'ACCION', sAccion );
             AgregaHiddenEdit( NodoForma, 'BORRAR', 'Borrar' );
             AgregaHiddenEdit( NodoForma, 'PAGINA', IntToStr( iPagina ) );
             AgregaHiddenEdit( NodoForma, 'EDICION', 'Edicion' );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Empresa:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValEmpresa, 'EMPRESA', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'EMPRESA_ACTUAL', sValEmpresa );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Nombres:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValNombres, 'NOMBRES', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'NOMBRES_ACTUAL', sValNombres );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Tel�fonos:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValTelefon, 'TELEFONOS', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'TELEFONOS_ACTUAL', sValTelefon );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Direcci�n:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValDirecc, 'DIRECCION', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'DIRECCION_ACTUAL', sValDirecc );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Ciudad:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValCiudad, 'CIUDAD', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'CIUDAD_ACTUAL', sValCiudad );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Comentarios:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValComenta, 'COMENTARIO', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'COMENTARIO_ACTUAL', sValComenta );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'E-Mail:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sValEmail, 'EMAIL', '60', '40' ) );
               AgregaHiddenEdit( NodoRenglon, 'EMAIL_ACTUAL', sValEmail );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, VACIO, taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
             NodoContenido := SetContenido( NodoContenido, VACIO );
               SetBoton( NodoContenido, sCaption, 'BTNGRABAR' );
               SetBoton( NodoContenido, 'Deshacer Cambios', 'btnDeshacer', eBtnReset );
               if ( sAccion <> K_AGREGAR ) then
                  SetBoton( NodoContenido, 'Borrar', 'btnBorrar', eBtnNormal, 'Borra(this.form)'  );
            AgregaEspacio( NodoContenido );
            AgregaEspacio( NodoContenido );
            AgregaEspacio( NodoContenido );
        end;
   end;

   procedure Busqueda;
   var
      NodoTabla, NodoForma, NodoRenglon, NodoContenido: TZetaXMLNode;
   begin
        with FXML do
        begin
             NodoTabla := SetTablaMaster( Documento.GetRoot );
             NodoForma := SetForma( NodoTabla, 'formBusqueda', 'RolMatch.asp', FALSE );
             AgregaHiddenEdit( NodoForma, 'CAMPOORDEN', InttoStr( iCampoOrden ) );
             AgregaHiddenEdit( NodoForma, 'ORDEN', InttoStr( iOrden ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Nombre:' ) );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEdit( sPalabra, 'PALABRA', '44', '40' ) );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetBoton( NodoContenido, 'Buscar', 'BTNBUSCAR' );
        end;
   end;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                     begin
                          EmpresaActiva := Comparte;
                          InitGlobales;
                     end;
                     sEmpresa := ParamAsString( 'EMPRESA' );
                     sNombres := ParamAsString( 'NOMBRES' );
                     sPalabra := ParamAsString( 'PALABRA' );
                     iOrden := ParamAsInteger( 'ORDEN' );
                     sAccion := ParamASstring( 'ACCION' );
                     sUsuario := ParamAsString( 'USUARIO' );
                     iPagina := ParamAsInteger( 'PAGINA' );
                     iCampoorden := ParamAsInteger( 'CAMPOORDEN' );
                     if sUsuario <> K_USUARIO_DEF then
                     begin
                          if ( sAccion = K_AGREGAR ) then
                             NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, K_AGREGAR + K_TARJETA )
                          else
                             NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, 'Editar' + K_TARJETA );
                          AgregaOnLoad( NodoDescripcion, 'PonerFocus( formTarjeta.EMPRESA )' );
                          GetValues;
                          SetMasterInfo;
                          Busqueda;
                          if HuboErrores then
                             SetFooterPortal( DocErrores.GetRoot )
                          else
                              SetFooterPortal( Documento.GetRoot );
                     end
                     else
                     begin
                          NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, 'Rolodex' );
                          BuildWarningXML( 'Usuario Inv�lido para uso de Rolodex' );
                          SetFooterPortal( Documento );
                     end;
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Obtener Datos de Contacto en Rolodex: ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.UsuarioCambiaPswd(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
const
     K_NOMBRE_DOCUMENTO = 'Cambio de Clave para Portal';
     K_VALIDA_DEF = 'Invalido';
var
   sClaveActual, sClaveNueva, sUsuario, sNombre, sValida, sClaveConf: string;
   NodoRenglon, NodoDescripcion, NodoDescrip: TZetaXMLNode;
   NodoTabla, NodoForma: TZetaXMLNode;

   procedure SetMasterInfo;
   var
      NodoTabla, NodoForma, NodoRenglon, NodoContenido: TZetaXMLNode;
   begin
        with FXML do
        begin
             NodoTabla := SetTablaMaster( Documento.GetRoot );
             NodoForma := SetForma( NodoTabla, 'formCambio', 'CambioPswd.asp', FALSE, VACIO, 'return ValidaForma(this)' );
             AgregaHiddenEdit( NodoForma, 'USUARIO', sUsuario );
             AgregaHiddenEdit( NodoForma, 'NOMBRE', sNombre );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Usuario:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagLbl( sUsuario ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Nombre:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagLbl( sNombre ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Clave Actual:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEditPswd( sClaveActual, 'CLAVEACTUAL', '30', '15' ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Nueva Clave:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEditPswd( sClaveNueva, 'CLAVENUEVA', '30', '15' ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Confirmaci�n:' ), taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
               SetContenido( NodoRenglon, GetTagEditPswd( sClaveConf, 'CLAVECONF', '30', '15' ) );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, VACIO, taRightJustify );
             NodoContenido := SetContenido( NodoRenglon, VACIO );
             NodoContenido := SetContenido( NodoContenido, VACIO );
               SetBoton( NodoContenido, 'Aceptar', 'BTNACEPTAR' );
               setBoton( NodoContenido, 'Cancelar', 'BTNCANCELAR', eBtnNormal, 'Regresa(this.form)' );
            AgregaEspacio( NodoContenido );
        end;
   end;

   procedure SetFinal;
   var
      NodoTabla, NodoForma, NodoRenglon, NodoContenido: TZetaXMLNode;
   begin
        with FXML do
        begin
             NodoTabla := SetTablaMaster( Documento.GetRoot );
             NodoForma := SetForma( NodoTabla, 'formFinal', 'inicio.asp', FALSE );
             NodoRenglon := SetRenglon( NodoForma );
               SetContenido( NodoRenglon, GetTagLbl( 'Clave de Usuario Cambiada' ) );
             NodoContenido := SetContenido( NodoForma, VACIO, taCenter );
               SetBoton( NodoContenido, 'Aceptar', 'BTNACEPTAR' );
            AgregaEspacio( NodoContenido );
        end;
   end;
{$endif}
begin
     {$ifdef CHILKAT}
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          InitGlobales;
     end;
     FLoginObj := CreateComObject( CLASS_dmServerLogin ) as IdmServerLogin;
     try
        FXML := TZetaXML.Create( oZetaProvider );
        try
           with FXML do
           begin
                if LoadParametros( Parametros ) then
                begin
                     try
                        sClaveActual := ParamAsString( 'CLAVEACTUAL' );
                        sClaveNueva := ParamAsString( 'CLAVENUEVA' );
                        sClaveConf := ParamAsString( 'CLAVECONF' );
                        sUsuario := ParamAsString( 'USUARIO' );
                        sNombre := ParamAsString( 'NOMBRE' );
                        sValida := ParamAsString( 'VALIDA' );
                        if ( sValida <> K_VALIDA_DEF ) then
                        begin
                             if StrLleno( sClaveActual ) and StrLleno( sClaveNueva ) then           // Cuando se invoca la forma por primera vez vienen vacios
                             begin
                                  try
                                     FLoginObj.UsuarioCambiaPswd( StrToIntDef( sUsuario, 0 ), sClaveActual, sClaveNueva );
                                     NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, K_NOMBRE_DOCUMENTO );
                                     AgregaOnLoad( NodoDescripcion, 'PonerFocus( formFinal.BTNACEPTAR )' );
                                     SetFinal;
                                     SetFooterPortal( Documento.GetRoot );
                                  except
                                     on Error: Exception do
                                     begin
                                          BuildErrorXML( Error.Message );
                                          NodoDescrip := AgregaDescripcionXML( DocErrores, K_NOMBRE_MODULO, K_NOMBRE_DOCUMENTO );
                                          NodoTabla := SetTablaMaster( DocErrores.GetRoot );
                                          NodoForma := SetForma( NodoTabla, 'formGrabar', VACIO, FALSE );
                                          NodoRenglon := SetRenglon( NodoForma );
                                          SetContenido( NodoRenglon, GetTagBoton( 'Intentar de Nuevo', 'BTNREINTENTAR', eBtnNormal, 'history.go(-1)' ), taCenter );
                                          SetFooterPortal( DocErrores.GetRoot );
                                     end;
                                  end;
                             end
                             else
                             begin
                                  NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, K_NOMBRE_DOCUMENTO );
                                  AgregaOnLoad( NodoDescripcion, 'PonerFocus( formCambio.CLAVEACTUAL )' );
                                  SetMasterInfo;
                                  SetFooterPortal( Documento.GetRoot );
                             end;
                        end
                        else
                        begin
                             NodoDescripcion := AgregaDescripcionXML( Documento.GetRoot, K_NOMBRE_MODULO, K_NOMBRE_DOCUMENTO );
                             BuildWarningXML( 'Usuario Inv�lido para uso del Portal' );
                             SetFooterPortal( Documento.GetRoot );
                        end;
                     except
                        on Error: Exception do
                        begin
                             BuildErrorXML( 'Error Al Cambiar Clave de Usuario: ' + Error.Message );
                        end;
                     end;
                end
                else
                    BuildErrorXML;
                Result := GetDocXML;
           end;
        finally
           FreeAndNil( FXML );
        end;
     finally
        FLoginObj := nil;
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.ExpiraLogin(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   sClave, sClaveNueva, sClaveEncr, sConfirma: string;
   iUsuario: Integer;
   oNodoRaiz: TZetaXMLNode;
   FDataset: TZetaCursor;

   procedure NuevoHijo( const sPasa, sMensaje: String);
   const
        K_PASA = 'PASA';
        K_MENSAJE = 'MENSAJE';
   begin
        with FXML do
        begin
             oNodoRaiz := Documento.GetRoot;
             oNodoRaiz.NewChild( K_PASA, sPasa );
             oNodoRaiz.NewChild( K_MENSAJE, sMensaje );
        end;
   end;
{$endif}
begin
     {$ifdef CHILKAT}
     sClaveNueva := VACIO;
     sConfirma := VACIO;
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     sClaveNueva := ParamAsString( 'CLAVENUEVA' );
                     iUsuario := ParamAsInteger( 'USUARIO' );
                     sConfirma := ParamAsString( 'CONFIRMACION' );
                     if ( LowerCase(sConfirma) <> LowerCase(sClaveNueva) ) then
                     begin
                          NuevoHijo( 'N',  'Las Claves no son iguales.La Nueva Clave debe ser igual a la Confirmaci�n.');
                     end
                     else
                     begin
                          if strLleno(sClaveNueva) then
                          begin
                               with oZetaProvider do
                               begin
                                    EmpresaActiva := Comparte;
                                    FDataset := CreateQuery( Format( GetScript( Q_ACTUAL_PASSWORD ), [ iUsuario ] ) );
                                    try
                                       with FDataset do
                                       begin
                                            Active := True;
                                            sClave := FieldByName( 'US_PASSWRD' ).AsString;
                                            Active := False;
                                       end;
                                    finally
                                           FreeAndNil( FDataset );
                                    end;
                                    sClaveEncr := Decrypt(sClave);
                                    if(LowerCase(sClaveNueva)=LowerCase(sClaveEncr))then
                                    begin
                                         NuevoHijo( 'N', 'La Nueva Clave Debe Ser Diferente A La Clave Actual.' );
                                    end
                                    else
                                    begin
                                         EmpiezaTransaccion;
                                           try
                                              ExecSQL( Comparte,
                                                       Format( GetScript( Q_USUARIO_CAMBIA_PSWD ),
                                                               [ ZetaServerTools.Encrypt( sClaveNueva ),
                                                                 K_GLOBAL_NO,
                                                                 DateToStrSQL( Date ),
                                                                 iUsuario ] ) );
                                              TerminaTransaccion( True );
                                              NuevoHijo( 'S', VACIO );
                                           except
                                                 on Error: Exception do
                                                 begin
                                                      RollBackTransaccion;
                                                      BuildErrorXML( Error.Message );
                                                 end;
                                           end;
                                    end;
                               end;
                          end
                          else
                          begin
                               NuevoHijo( 'N', 'La Nueva Clave No Puede Quedar Vac�a.');
                          end;
                     end;
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Obtener Datos de Contacto en Rolodex: ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetBuscaTarjetas(const Parametros: WideString): WideString;
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                          EmpresaActiva := Comparte;
                     AgregaXMLQueryNET( Documento, K_TABLE_TARJETA, Format( GetScript( Q_ROL_MATCH_NET ), [
                                        EntreComillas( '%' + ParamAsString( 'PALABRA' ) + '%' ),
                                        ParamAsInteger( 'CAMPOORDEN' ) ] ) );
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Buscar Palabra en Rolodex: ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.GetTarjetaNET(const Parametros: WideString): WideString;
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  try
                     with oZetaProvider do
                          EmpresaActiva := Comparte;
                     AgregaXMLQueryNET( Documento, K_TABLE_TARJETA, Format( GetScript( Q_TARJETA_NET ), [
                                        EntreComillas( ParamAsString( 'EMPRESA' ) ),
                                        EntreComillas( ParamAsString( 'NOMBRES' ) ) ] ) );
                  except
                     on Error: Exception do
                     begin
                          BuildErrorXML( 'Error Al Buscar Contacto de Rolodex: ' + Error.Message );
                     end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.BorraTarjetaNET(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   iCuantos: Integer;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := Comparte;
                       try
                          iCuantos := EjecutaAndFree( Format( GetScript( Q_BORRA_TARJETA_NET ), [
                                                      EntreComillas( FXML.ParamAsString( 'EMPRESA' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'NOMBRES' ) ) ] ) );
                          if ( iCuantos > 0 ) then
                             BuildWarningXML( 'Registro Borrado' )
                          else
                             BuildErrorXML( 'No se Pudo Borrar Contacto de Rolodex' );
                       except
                             on Error: Exception do
                             begin
                                  BuildErrorXML( 'Error Al Borrar Contacto de Rolodex: ' + Error.Message );
                             end;
                       end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.AgregaTarjetaNET(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   iCuantos: Integer;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := Comparte;
                       try
                          iCuantos := EjecutaAndFree( Format( GetScript( Q_AGREGA_TARJETA_NET ), [
                                                      EntreComillas( FXML.ParamAsString( 'TJ_EMPRESA' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_NOMBRES' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_TELEFON' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_DIRECC1' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_DIRECC2' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_COMENTA' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_EMAIL' ) ) ] ) );
                          if ( iCuantos > 0 ) then
                             BuildWarningXML( 'Registro Agregado' )
                          else
                             BuildErrorXML( 'No se Pudo Agregar Contacto de Rolodex' );
                       except
                             on Error: Exception do
                             begin
                                  BuildErrorXML( 'Error Al Agregar Contacto de Rolodex: ' + Error.Message );
                             end;
                       end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

function TdmServerPortal.ModificaTarjetaNET(const Parametros: WideString): WideString;
{$ifdef CHILKAT}
var
   iCuantos: Integer;
{$endif}
begin
     {$ifdef CHILKAT}
     FXML := TZetaXML.Create( oZetaProvider );
     try
        with FXML do
        begin
             if LoadParametros( Parametros ) then
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := Comparte;
                       try
                          iCuantos := EjecutaAndFree( Format( GetScript( Q_MODIFICA_TARJETA_NET ), [
                                                      EntreComillas( FXML.ParamAsString( 'TJ_EMPRESA' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_NOMBRES' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_TELEFON' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_DIRECC1' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_DIRECC2' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_COMENTA' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'TJ_EMAIL' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'EMPRESA' ) ),
                                                      EntreComillas( FXML.ParamAsString( 'NOMBRES' ) ) ] ) );
                          if ( iCuantos > 0 ) then
                             BuildWarningXML( 'Registro Modificado' )
                          else
                             BuildErrorXML( 'No se Pudo Modificar Contacto de Rolodex' );
                       except
                             on Error: Exception do
                             begin
                                  BuildErrorXML( 'Error Al Modificar Contacto de Rolodex: ' + Error.Message );
                             end;
                       end;
                  end;
             end
             else
                 BuildErrorXML;
             Result := GetDocXML;
        end;
     finally
            FreeAndNil( FXML );
     end;
     {$endif}
     SetComplete;
end;

procedure TdmServerPortal.EscribeArchivo(const Parametros: WideString);
var
    oArchivo: TStrings;
begin
     oArchivo := TStringList.Create;
     try
        oArchivo.CommaText := Parametros;
        oArchivo.SaveToFile( Format( '\\CrmServer\Mensajes\Cel%s.txt', [ FormatDateTime( 'yymmddhhmmss', Now ) ] ) );
     finally
            FreeAndNil( oArchivo );
     end;
     SetComplete;
end;

{$ifndef DOS_CAPAS}
initialization
  TComponentFactory.Create(ComServer, TdmServerPortal,Class_dmServerPortal, ciMultiInstance, tmApartment);
{$endif}

end.

