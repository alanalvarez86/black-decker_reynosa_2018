inherited Salida: TSalida
  ActiveControl = Clave
  Caption = 'Salir Del Kiosko'
  ClientHeight = 91
  ClientWidth = 336
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ClaveLBL: TLabel [0]
    Left = 4
    Top = 24
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cla&ve De Seguridad:'
    FocusControl = Clave
  end
  inherited PanelBotones: TPanel
    Top = 55
    Width = 336
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 168
      Default = True
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 253
    end
  end
  object Clave: TEdit
    Left = 105
    Top = 21
    Width = 228
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
end
