{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: QuickReport 2.0 Delphi 1.0/2.0/3.0                      ::
  ::                                                         ::
  :: QRPREV - QuickReport standard preview form              ::
  ::                                                         ::
  :: Copyright (c) 1997 QuSoft AS                            ::
  :: All Rights Reserved                                     ::
  ::                                                         ::
  :: web: http://www.qusoft.no   mail: support@qusoft.no     ::
  ::                             fax: +47 22 41 74 91        ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

unit fPreview;

interface

{$define EZM}

uses
{$ifdef win32}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, QRPrntr, QR3const, QuickRpt;
{$else}
  Wintypes, WinProcs, Sysutils, Messages, Classes, Controls, StdCtrls, ExtCtrls,
  Buttons, QRPrntr, Graphics, Forms, Dialogs, QR2const, QuickRpt;
{$endif}

type
  TPreview = class(TForm)
    Toolbar: TPanel;
    StatusPanel: TPanel;
    ZoomToFit: TSpeedButton;
    ZoomTo100: TSpeedButton;
    ZoomToWidth: TSpeedButton;
    FirstPage: TSpeedButton;
    PrevPage: TSpeedButton;
    NextPage: TSpeedButton;
    LastPage: TSpeedButton;
    Print: TSpeedButton;
    Exit: TBitBtn;
    Panel1: TPanel;
    Status: TLabel;
    QRPreview: TQRPreview;
    Timer: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ZoomToFitClick(Sender: TObject);
    procedure ZoomTo100Click(Sender: TObject);
    procedure ZoomToWidthClick(Sender: TObject);
    procedure FirstPageClick(Sender: TObject);
    procedure PrevPageClick(Sender: TObject);
    procedure NextPageClick(Sender: TObject);
    procedure LastPageClick(Sender: TObject);
    procedure PrintClick(Sender: TObject);
    procedure ExitClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
{$ifdef EZM}
    procedure QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
{$endif}
  private
    FQRPrinter : TQRPrinter;
    FReporte : TQuickRep;
    FShowGrafica: Boolean;
    procedure TimerStart;
    procedure TimerStop;
    procedure CierraDialogosReporte;
  public
    constructor CreatePreview(AOwner : TComponent; aQRPrinter : TQRPrinter; oReport : TQuickRep ); virtual;
    procedure UpdateInfo;
    property QRPrinter : TQRPrinter read FQRPrinter write FQRPrinter;
    property Reporte : TQuickRep read FReporte write FReporte;
    property ShowGrafica: Boolean read FShowGrafica write FShowGrafica;
  end;

implementation

uses FRangoPaginas,
     FKioscoRegistry,
     DCliente,
     ZetaDialogo,
     ZetaCommonClasses;
{$R *.DFM}


procedure TPreview.FormCreate(Sender: TObject);
begin
     FirstPage.Caption := 'Primer' + CR_LF + 'P�gina';
     LastPage.Caption := 'Ultima' + CR_LF + 'P�gina';
     PrevPage.Caption := 'Anterior';
     NextPage.Caption := 'Siguiente';

     ZoomToFit.Caption := 'P�gina Completa';
     ZoomToWidth.Caption := 'Ancho P�gina';
     ZoomTo100.Caption := 'P�gina al 100%';
end;


constructor TPreview.CreatePreview(AOwner : TComponent; aQRPrinter : TQRPrinter; oReport : TQuickRep);
begin
     inherited Create(AOwner);
     QRPrinter := aQRPrinter;
     FReporte := oReport;
     QRPreview.QRPrinter := aQRPrinter;
     if QRPrinter <> nil then Caption := QRPrinter.Title;
     ZoomTo100.Down := True;
     WindowState := wsMaximized;
end;

procedure TPreview.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     TimerStop;
     Action := caFree;
end;

procedure TPreview.UpdateInfo;
begin
{$ifdef EZM}
     if FQrPrinter <> NIL then
        Status.Caption := 'P�gina ' + IntToStr(QRPreview.PageNumber) +
                          ' de ' + IntToStr(QRPreview.QRPrinter.PageCount);
{$else}
     Status.Caption := LoadStr(SqrPage) + ' ' + IntToStr(QRPreview.PageNumber) + ' ' +
                       LoadStr(SqrOf) + ' ' + IntToStr(QRPreview.QRPrinter.PageCount);
{$endif}
end;

procedure TPreview.ZoomToFitClick(Sender: TObject);
begin
  Application.ProcessMessages;
  QRPreview.ZoomToFit;
end;

procedure TPreview.ZoomTo100Click(Sender: TObject);
begin
  Application.ProcessMessages;
  QRPreview.Zoom := 100;
end;

procedure TPreview.ZoomToWidthClick(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.ZoomToWidth;
end;

procedure TPreview.FirstPageClick(Sender: TObject);
begin
  QRPreview.PageNumber := 1;
  UpdateInfo;
end;

procedure TPreview.PrevPageClick(Sender: TObject);
begin
  QRPreview.PageNumber := QRPreview.PageNumber - 1;
  UpdateInfo;
end;

procedure TPreview.NextPageClick(Sender: TObject);
begin
  QRPreview.PageNumber := QRPreview.PageNumber + 1;
  UpdateInfo;
end;

procedure TPreview.LastPageClick(Sender: TObject);
begin
  QRPreview.PageNumber := QRPrinter.PageCount;
  UpdateInfo;
end;

procedure TPreview.PrintClick(Sender: TObject);
begin
     try
        QrPreview.QrPrinter.Print;
        ZetaDialogo.ZInformation( Caption, 'El reporte ha sido impreso con �xito', 0 );
     except
           raise;
     end;
end;

procedure TPreview.ExitClick(Sender: TObject);
begin
  Close;
end;

procedure TPreview.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_Next : if Shift=[ssCtrl] then
                LastPageClick(Self)
              else
                NextPageClick(Self);
    VK_Prior : if Shift=[ssCtrl] then
                 FirstPageClick(Self)
               else
                 PrevPageClick(Self);
    VK_Home : FirstPageClick(Self);
    VK_End : LastPageClick(Self);
  end;
end;

procedure TPreview.QRPreviewPageAvailable(Sender: TObject;
  PageNum: Integer);
begin
  UpdateInfo;
end;

{$ifdef EZM}
procedure TPreview.QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     with QrPreview do
     begin
          case Button of
               mbLeft: if ( Zoom < 300 ) then Zoom := Zoom + 10;
               mbRight: if ( Zoom > 0 ) then Zoom := Zoom - 10;
          end;
     end;
end;
{$endif}

procedure TPreview.FormResize(Sender: TObject);
begin
     WindowState := wsMaximized;
end;

procedure TPreview.FormShow(Sender: TObject);
 const
     K_UN_SEGUNDO = 1000;
begin
     Timer.Interval := KioskoRegistry.PantallasTimeout * K_UN_SEGUNDO;
     TimerStart;
     QRPreview.Zoom := 100;
end;


procedure TPreview.TimerStart;
begin
     TimerStop;
     with Timer do
     begin
          Enabled := True;
     end;
end;

procedure TPreview.TimerStop;
begin
     with Timer do
     begin
          Enabled := False;
     end;
end;
procedure TPreview.TimerTimer(Sender: TObject);
begin
     CierraDialogosReporte;
     Close;
end;

procedure TPreview.CierraDialogosReporte;
var
   i : Integer;
begin
     with Screen do
     begin
          for i := FormCount-1 downto 0 do
          begin
               if ( Forms[i] <> self ) and
                  ( Forms[i].Visible ) and
                  ( Forms[i].Parent = nil ) and
                  ( ( fsModal in Forms[i].FormState ) or ( fsVisible in Forms[i].FormState ) ) and
                  ( Forms[i].Caption = self.Caption ) then
               try
                  Forms[i].Close;        // Esto hace que se cierre la Forma
               except
               end;
          end;
          Application.ProcessMessages;
     end;
end;

end.
