unit FFormaCursos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, Db, Grids, DBGrids,
     FFormaBase,
     ZetaDBGrid, StdCtrls;

type
  TFormaCursos = class(TFormaBase)
    dsHisCursos: TDataSource;
    PanelTomados: TPanel;
    LblTomados: TLabel;
    PanelProgramados: TPanel;
    LblProgramados: TLabel;
    ZetaDBGrid1: TZetaDBGrid;
    ZetaDBGrid2: TZetaDBGrid;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function HayCursosProgramados: Boolean;
    function HayCursosTomados: Boolean;
    procedure SetPanelInfo(oPanel: TPanel; const lHayDatos: Boolean; const sMensaje: String );
    procedure MessageClearPanel(oPanel: TPanel);
    procedure MessageShowPanel(oPanel: TPanel; const sMensaje: String);
    procedure SizeResetPanel(oPanel: TPanel);
    procedure SizeSetPanel(oPanel: TPanel; const iValue: Integer);
  protected
    { Protected declarations }
    procedure SetControls; override;
    procedure SetControlsEmpty; override;
    procedure SetControlsFull; override;
  public
    { Public declarations }
    procedure Connect; override;
  end;

var
  FormaCursos: TFormaCursos;

implementation

uses DCliente,
     FKioscoRegistry;

{$R *.DFM}

procedure TFormaCursos.FormShow(Sender: TObject);
begin
     inherited;
     with KioskoRegistry do
     begin
          with PanelProgramados do
          begin
               Color := PanelInferiorBkColor;
               Font := PanelInferior.Font;
          end;
          with PanelTomados do
          begin
               Color := PanelInferiorBkColor;
               Font := PanelInferior.Font;
          end;
          SetColorSubControles( PanelProgramados, PanelInferiorFontColor );
          SetColorSubControles( PanelTomados, PanelInferiorFontColor );
     end;
     SetTitulo( 'Cursos Programados y Tomados' );
     PanelProgramados.Height := Trunc( PanelInferior.Height / 2 );
     SetControls; // Lo mand� llamar DoConnect(), pero se requiere por que el Font de los Paneles a�n no estaba puesto, El FormShow solo se manda llamar una vez
end;

procedure TFormaCursos.Connect;
begin
     with dmCliente do
     begin
          AbreCursos;
          Datasource.Dataset := cdsCursos;
          dsHisCursos.Dataset := cdsHisCursos;
     end;
end;

function TFormaCursos.HayCursosProgramados: Boolean;
begin
     Result := Assigned( Dataset ) and ( not DataSetVacio );
end;

function TFormaCursos.HayCursosTomados: Boolean;
begin
     Result := Assigned( dsHisCursos.Dataset ) and ( not dsHisCursos.Dataset.IsEmpty );
end;

procedure TFormaCursos.SetControls;
begin
     if ( not HayCursosProgramados ) and ( not HayCursosTomados ) then
        SetControlsEmpty
     else
     begin
          SetControlsFull;
          SetPanelInfo( PanelProgramados, HayCursosProgramados, 'No Hay Cursos Programados' );
          SetPanelInfo( PanelTomados, HayCursosTomados, 'No Hay Cursos Tomados' );
     end;
end;

procedure TFormaCursos.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     ControlsHide( PanelInferior );
     MessageShow( 'No Hay Cursos Programados o Tomados' );
end;

procedure TFormaCursos.SetControlsFull;
begin
     inherited SetControlsFull;
     ControlsShow( PanelInferior );
     MessageClear;
end;

procedure TFormaCursos.SetPanelInfo( oPanel: TPanel; const lHayDatos: Boolean; const sMensaje: String );
begin
     if lHayDatos then
     begin
          MessageClearPanel( oPanel );
          ControlsShow( oPanel );
     end
     else
     begin
          ControlsHide( oPanel );
          MessageShowPanel( oPanel, sMensaje );
     end;
end;

procedure TFormaCursos.SizeSetPanel( oPanel: TPanel; const iValue: Integer );
begin
     with oPanel.Font do
     begin
          Size := iValue;
     end;
end;

procedure TFormaCursos.SizeResetPanel( oPanel: TPanel );
begin
     SizeSetPanel( oPanel, PanelInferior.Font.Size );
end;

procedure TFormaCursos.MessageShowPanel( oPanel: TPanel; const sMensaje: String );
begin
     SizeSetPanel( oPanel, K_FONT_SIZE_LETRERO );
     with oPanel do
     begin
          Caption := sMensaje;
     end;
end;

procedure TFormaCursos.MessageClearPanel( oPanel: TPanel );
begin
     SizeResetPanel( oPanel );
     with oPanel do
     begin
          Caption := '';
     end;
end;

end.
