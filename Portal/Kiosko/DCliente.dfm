inherited dmCliente: TdmCliente
  OldCreateOrder = True
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsEmpleadoCalcFields
    AlCrearCampos = cdsEmpleadoAlCrearCampos
    Left = 144
    Top = 56
  end
  object cdsCursos: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Descendente'
        Fields = 'KC_FEC_PRO'
        Options = [ixDescending]
      end>
    IndexName = 'Descendente'
    Params = <>
    StoreDefs = True
    AfterOpen = cdsCursosAfterOpen
    Left = 32
    Top = 128
  end
  object cdsVacHistorial: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsVacHistorialAfterOpen
    Left = 248
    Top = 56
  end
  object cdsVacTotal: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsVacTotalAfterOpen
    Left = 248
    Top = 104
  end
  object cdsAhorros: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsAhorrosAfterOpen
    Left = 336
    Top = 56
  end
  object cdsPrestamos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsPrestamosAfterOpen
    Left = 336
    Top = 104
  end
  object cdsCalendario: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsCalendarioAfterOpen
    OnCalcFields = cdsCalendarioCalcFields
    AlCrearCampos = cdsCalendarioAlCrearCampos
    Left = 144
    Top = 160
  end
  object cdsTarjeta: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsTarjetaAfterOpen
    Left = 248
    Top = 160
  end
  object cdsChecadas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsChecadasAfterOpen
    Left = 336
    Top = 160
  end
  object cdsNomina: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 216
  end
  object cdsConceptos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsConceptosAfterOpen
    Left = 248
    Top = 216
  end
  object cdsPrenomina: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 272
  end
  object cdsAusencia: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsAusenciaAfterOpen
    Left = 248
    Top = 272
  end
  object cdsCafPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsCafPeriodoAfterOpen
    Left = 144
    Top = 320
  end
  object cdsCafPeriodoTotales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 240
    Top = 320
  end
  object cdsPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 48
    Top = 320
  end
  object cdsHisCursos: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Descendente'
        Fields = 'KC_FEC_TOM'
        Options = [ixDescending]
      end>
    IndexName = 'Descendente'
    Params = <>
    StoreDefs = True
    AfterOpen = cdsHisCursosAfterOpen
    Left = 144
    Top = 104
  end
  object cdsReportes: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsChecadasAfterOpen
    Left = 336
    Top = 232
  end
  object cdsPeriodoActivo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 336
    Top = 320
  end
end
