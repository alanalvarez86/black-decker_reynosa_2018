inherited Empresas: TEmpresas
  Caption = 'Empresas Habilitadas Para Kiosco'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    inherited OK: TBitBtn
      Left = 214
      OnClick = OKClick
    end
  end
  object StringGrid: TStringGrid
    Left = 0
    Top = 17
    Width = 375
    Height = 147
    Align = alClient
    ColCount = 3
    DefaultRowHeight = 20
    FixedColor = clInfoBk
    FixedCols = 2
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goEditing, goAlwaysShowEditor]
    TabOrder = 1
    OnGetEditMask = StringGridGetEditMask
    ColWidths = (
      64
      229
      62)
  end
  object HeaderControl: THeaderControl
    Left = 0
    Top = 0
    Width = 375
    Height = 17
    DragReorder = False
    Sections = <
      item
        ImageIndex = -1
        Text = 'C�digo'
        Width = 67
      end
      item
        ImageIndex = -1
        Text = 'Nombre'
        Width = 230
      end
      item
        ImageIndex = -1
        Text = 'D�gito'
        Width = 64
      end>
  end
end
