unit FLetrero;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ExtCtrls,
     FFormaBase, Db;

type
  TFormaLetrero = class(TFormaBase)
  private
    { Private declarations }
    procedure SetLetrero( const sValue: String );
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Letrero: String write SetLetrero;
  end;

var
  FormaLetrero: TFormaLetrero;

implementation

{$R *.DFM}

procedure TFormaLetrero.SetLetrero( const sValue: String );
begin
     SetTitulo( sValue );
end;

end.
