�
 TPORTADA 0;  TPF0TPortadaPortadaLeft� Top� BorderStylebsNoneCaptionKiosco de TressClientHeight�ClientWidthQColorclBlackFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrder	Position	poDefault
PrintScalepoNoneWindowStatewsMaximized
OnActivateFormActivateOnCreate
FormCreate	OnDestroyFormDestroyOnDeactivateFormDeactivate
OnKeyPressFormKeyPressOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBoxBotonesGroupBoxLeftGTop+Width� Height&CaptionTRESS: KioscoColorclBlackFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold ParentColor
ParentFontTabOrder  TBitBtnIniciarLeft TopWidth� Height<HintIniciar Kiosco de TressCaption&IniciarDefault	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrder OnClickIniciarClick
Glyph.Data

    BM      v   (   (            �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333���������0        7wwwwwwww0&&&&��7�333330bbbb��7�3?�330&&  ��7�3ww330bb���7�33��0&&�   7��3wwws0` ��bb7�w33330 ���&&7����330`�   bb7��wwws330 ���&&&&7��37�3330`���bbbb7�����3330    &&&&7�www33330bbbbbbbb7�33333330��������7wwwwwwww0��������7��������0        7wwwwwwwws3333333333333333333333333333333333333333	NumGlyphs  TBitBtnSalirLeft Top� Width� Height<Cancel	Caption&SalirFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClick
SalirClick
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 8����w���������33?  DD@���DD������3��  33MP��33�����38��  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33E���3333���38�?  33M]��3333�x�38�?  33DDDDD3333������?  33333333333�����3?  333   333333?���3?  333
��333333����3?  333   333333����3?  	NumGlyphs  TBitBtn
ConfigurarLeft Top� Width� Height!Hint$Configurar Comportamiento Del KioscoCaption&ConfigurarFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrderOnClickConfigurarClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUP�YPU_�_uW��W �UYUwu�UW�up��UY3W_�UW�� �UY3w_UW�� UUY5wUU�WUU���uUuu_�Up�uPUW�U�wwuU U3UUwu_u�UUUUP35UU_��_uUU U3UUUw�u�UUPPP35UUWW�_uUU 3UUUw�UUU   5UWUwwwUUUU  UWUUww�UUU0 UWUUww��uUU3P  UUUUwWwwUUU	NumGlyphs  TBitBtnEmpresasLeft Top� Width� Height!Hint#Especificar Empresas Para El KioscoCaption	&EmpresasDefault	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrderOnClickEmpresasClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUU����UUUP   UUUWwww�UU	��� UUUuUUUw_UP����UW�UUUu� Uw�����_      wwwwww_uPp���Ww�UU�U ��� Uw�U�wWUP�p  UW_�wuwUU �� UUwuU�wUUP�p UUW_�wuUUU ��UUUwuU�UUUP�p UUUW_�wuUUUU UUUUUUwuUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU	NumGlyphs  TBitBtnServidorLeft TopiWidth� Height!HintEspecificar Servidor De DatosCaption	&ServidorFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrderOnClickServidorClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30     3?�wwwww?  �����ww?????s��3sssss7�������37�������    37wwwws��33337�����0    37�wwww��0����?������ �0�  �w7��ww���0���?����7� �0����w7�������0�  �3���ww7��0����77�������0    ���wwww   33333www333333333333333333333	NumGlyphs   	TGroupBoxGafeteGBLeftTop`Width"Height� Caption Por Favor Pase Su Gafete Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelEmpleadoLBLLeft	Top0Width\Height	AlignmenttaRightJustifyCaption	Empleado:ColorclWindowTextFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TEditEmpleadoLeftjTop,Width� Height Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TBitBtnRegresarLeftkTopXWidth� Height1HintRegresar Al CarrouselCancel	Caption	&RegresarFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style ModalResult
ParentFontParentShowHintShowHint	TabOrderOnClickRegresarClick
Glyph.Data
�   �   BM�       v   (               �                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� www   �wwww
�� �www
���www
��* www   � wwwwww  wwwwwww wwwwww wpwww  w
    � p�����* 
������p����� ww
    �wwpwwwwwwwwwwww   TTimerTimerEnabledInterval�OnTimer
TimerTimerLeft�Top�    