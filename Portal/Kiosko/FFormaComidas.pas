unit FFormaComidas;

interface

uses  Windows, Messages, SysUtils, Classes, Graphics, Controls,
      Forms, Dialogs, Db, Buttons, ExtCtrls,
      FFormaBasePeriodo, FFormaBase, Grids, DBGrids, ZetaDBGrid,
  ZetaDBTextBox, StdCtrls, DBCGrids;

type
  TFormaComidas = class(TFormaBasePeriodo)
    dsComidas: TDataSource;
    dsTotales: TDataSource;
    DBCtrlGrid: TDBCtrlGrid;
    TipoLbl: TLabel;
    CF_TIPO: TZetaDBTextBox;
    ComidasLbl: TLabel;
    CF_COMIDAS: TZetaDBTextBox;
    ExtrasLbl: TLabel;
    CF_EXTRAS: TZetaDBTextBox;
    ZetaDBGrid: TZetaDBGrid;
  private
    { Private declarations }
    procedure SetDatasets;
  protected
    { Protected declarations }
    procedure CargarInit; override;
    procedure CargarAnterior; override;
    procedure CargarSiguiente; override;
    procedure SetControlsFull; override;
    procedure SetControlsEmpty; override;
  public
    { Public declarations }
  end;

var
  FormaComidas: TFormaComidas;

implementation

uses DCliente,
     ZetaCommonTools;

{$R *.DFM}

{ ********** TFormaNomina ********* }

procedure TFormaComidas.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     with Titulo do
     begin
          Caption := 'Sin Comidas';
     end;
end;

procedure TFormaComidas.SetControlsFull;
var
   sTitulo: String;
begin
     inherited SetControlsFull;
     with Dataset do
     begin
          sTitulo := Format( 'Del %s Al %s', [ ZetaCommonTools.FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ),
                                               ZetaCommonTools.FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime ) ] );
     end;
     with dsComidas do
     begin
          if Assigned( Dataset ) then
          begin
               if Dataset.IsEmpty then
                  sTitulo := Format( 'Sin Comidas %s', [ sTitulo ] );
          end;
     end;
     Titulo.Caption := sTitulo;
end;

procedure TFormaComidas.SetDatasets;
begin
     with dmCliente do
     begin
          Datasource.Dataset := cdsPeriodo;
          dsComidas.Dataset := cdsCafPeriodo;
          dsTotales.Dataset := cdsCafPeriodoTotales;
     end;
end;

procedure TFormaComidas.CargarAnterior;
begin
     with dmCliente do
     begin
          AbreComidasAnterior( Year, Periodo );
     end;
     SetDatasets;
end;

procedure TFormaComidas.CargarInit;
begin
     with dmCliente do
     begin
          AbreComidas( Year );
     end;
     SetDatasets;
end;

procedure TFormaComidas.CargarSiguiente;
begin
     with dmCliente do
     begin
          AbreComidasSiguiente( Year, Periodo );
     end;
     SetDatasets;
end;

end.
