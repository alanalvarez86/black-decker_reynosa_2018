unit FFormaPersonales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Grids, DBGrids, Mask, DBCtrls, Db, ExtCtrls,
     FFormaBase,
     ZetaDBTextBox;

type
  TFormaPersonales = class(TFormaBase)
    PU_DESCRIPlbl: TLabel;
    PU_DESCRIP: TZetaDBTextBox;
    TB_ELEMENTlbl: TLabel;
    TU_DESCRIPlbl: TLabel;
    CB_CALLElbl: TLabel;
    CB_CALLE: TZetaDBTextBox;
    CB_COLONIAlbl: TLabel;
    CB_COLONIA: TZetaDBTextBox;
    CB_CODPOSTlbl: TLabel;
    CB_CODPOST: TZetaDBTextBox;
    CB_TELlbl: TLabel;
    CB_FEC_NAClbl: TLabel;
    CB_FEC_NAC: TZetaDBTextBox;
    CB_LUG_NAClbl: TLabel;
    CB_LUG_NAC: TZetaDBTextBox;
    CB_TEL: TZetaDBTextBox;
    TB_ELEMENT: TZetaDBTextBox;
    TU_DESCRIP: TZetaDBTextBox;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_ANTIGUOlbl: TLabel;
    CB_VENCElbl: TLabel;
    TB_ELEMENT1: TZetaDBTextBox;
    TB_ELEMENT4: TZetaDBTextBox;
    TB_ELEMENT5: TZetaDBTextBox;
    TB_ELEMENT6: TZetaDBTextBox;
    TB_ELEMENT7: TZetaDBTextBox;
    TB_ELEMENT8: TZetaDBTextBox;
    TB_ELEMENT9: TZetaDBTextBox;
    TB_ELEMENT2: TZetaDBTextBox;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL5: TZetaDBTextBox;
    CB_NIVEL6: TZetaDBTextBox;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL9: TZetaDBTextBox;
    TB_ELEMENT3: TZetaDBTextBox;
    CB_ANTIGUO: TZetaDBTextBox;
    CB_VENCE: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure SetNivel(const iNivel: Integer; oLabel: TLabel; oControl, oDescrip: TControl);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure SetControls; override;
  public
    { Public declarations }
  end;

var
  FormaPersonales: TFormaPersonales;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     DCliente;

{$R *.DFM}

procedure TFormaPersonales.FormShow(Sender: TObject);
begin
     inherited;
     Titulo.Color := PanelSuperior.Color;
     Titulo.Caption := 'Datos Personales';
end;

procedure TFormaPersonales.SetNivel( const iNivel: Integer; oLabel: TLabel; oControl, oDescrip: TControl );
var
   sNivel: String;
   lVisible: Boolean;
begin
     sNivel := Dataset.FieldByName( Format( 'GL_NIVEL%d', [ iNivel ] ) ).AsString;
     lVisible := ( sNivel <> K_TOKEN_NIL );
     with oLabel do
     begin
          Caption := sNivel + ':';
          Visible := lVisible;
     end;
     oControl.Visible := lVisible;
     oDescrip.Visible := lVisible;
end;

procedure TFormaPersonales.SetControls;
begin
     inherited SetControls;
     SetNivel( 1, CB_NIVEL1lbl, CB_NIVEL1, TB_ELEMENT1 );
     SetNivel( 2, CB_NIVEL2lbl, CB_NIVEL2, TB_ELEMENT2 );
     SetNivel( 3, CB_NIVEL3lbl, CB_NIVEL3, TB_ELEMENT3 );
     SetNivel( 4, CB_NIVEL4lbl, CB_NIVEL4, TB_ELEMENT4 );
     SetNivel( 5, CB_NIVEL5lbl, CB_NIVEL5, TB_ELEMENT5 );
     SetNivel( 6, CB_NIVEL6lbl, CB_NIVEL6, TB_ELEMENT6 );
     SetNivel( 7, CB_NIVEL7lbl, CB_NIVEL7, TB_ELEMENT7 );
     SetNivel( 8, CB_NIVEL8lbl, CB_NIVEL8, TB_ELEMENT8 );
     SetNivel( 9, CB_NIVEL9lbl, CB_NIVEL9, TB_ELEMENT9 );
end;

procedure TFormaPersonales.Connect;
begin
     with dmCliente do
     begin
          AbrePersonales;
          Datasource.Dataset := cdsEmpleado;
     end;
end;

end.
