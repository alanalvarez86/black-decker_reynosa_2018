�
 TFORMAVACACIONES 0S  TPF0�TFormaVacacionesFormaVacacionesLeft.Top� CaptionFormaVacacionesClientHeight�ColorclMaroonPixelsPerInch`
TextHeight �TPanelPanelSuperiorTop)Height� TabOrder 	TGroupBoxPagoGBLeftTopWidth� HeightAlignalLeftCaption Pago TabOrder  TLabelCB_DER_PAGlblLeft2TopWidth,Height	AlignmenttaRightJustifyCaptionDerecho:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelPago3Left	TopMWidthHeightCaption( + )  TLabelCB_PRO_PAGlblLeft TopNWidth>Height	AlignmenttaRightJustifyCaptionProporcional:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelPago1Left	Top&WidthHeightCaption( - )  TLabelCB_V_PAGOlblLeft1Top&Width-Height	AlignmenttaRightJustifyCaptionPagados:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelPago4Left	TopaWidthHeightCaption( = )  TLabelCB_TOT_PAGlblLeftTopbWidth?Height	AlignmenttaRightJustifyCaptionSaldo Actual:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TZetaDBTextBox
CB_DER_PAGLeftaTopWidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_DER_PAGColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_DER_PAG
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_V_PAGOLeftaTop$WidthSHeight	AlignmenttaRightJustifyAutoSizeCaption	CB_V_PAGOColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField	CB_V_PAGO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelPago2Left	Top9WidthHeightCaption( = )  TLabelCB_SUB_PAGlblLeft-Top:Width1Height	AlignmenttaRightJustifyCaption
Sub-Total:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TZetaDBTextBox
CB_SUB_PAGLeftaTop8WidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_SUB_PAGColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_SUB_PAG
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_PRO_PAGLeftaTopLWidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_PRO_PAGColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_PRO_PAG
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_TOT_PAGLeftaTop`WidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_TOT_PAGColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_TOT_PAG
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m   	TGroupBoxGozoGBLeft� TopWidth� HeightAlignalLeftCaption Gozo TabOrder TLabelCB_DER_GOZlblLeft2TopWidth,Height	AlignmenttaRightJustifyCaptionDerecho:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelGozo3Left	TopMWidthHeightCaption( + )  TLabelCB_PRO_GOZlblLeft TopMWidth>Height	AlignmenttaRightJustifyCaptionProporcional:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelGozo1Left	Top%WidthHeightCaption( - )  TLabelCB_V_GOZOlblLeft1Top%Width-Height	AlignmenttaRightJustifyCaptionGozados:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelGozo4Left	TopaWidthHeightCaption( = )  TLabelCB_TOT_GOZlblLeftTopaWidth?Height	AlignmenttaRightJustifyCaptionSaldo Actual:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TZetaDBTextBox
CB_DER_GOZLeftaTopWidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_DER_GOZColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_DER_GOZ
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox	CB_V_GOZOLeftaTop#WidthSHeight	AlignmenttaRightJustifyAutoSizeCaption	CB_V_GOZOColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField	CB_V_GOZO
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelGozo2Left	Top9WidthHeightCaption( = )  TLabelCB_SUB_GOZlblLeft-Top9Width1Height	AlignmenttaRightJustifyCaption
Sub-Total:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TZetaDBTextBox
CB_SUB_GOZLeftaTop7WidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_SUB_GOZColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_SUB_GOZ
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_PRO_GOZLeftaTopKWidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_PRO_GOZColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_PRO_GOZ
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TZetaDBTextBox
CB_TOT_GOZLeftaTop_WidthSHeight	AlignmenttaRightJustifyAutoSizeCaption
CB_TOT_GOZColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_TOT_GOZ
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m   	TGroupBox	GroupBox2LeftsTopWidth� HeightAlignalClientTabOrder TLabelCB_FEC_VAClblLeftTopWidth`Height	AlignmenttaRightJustifyCaptionUltimas Vacaciones:  TZetaDBTextBox
CB_FEC_VACLeftnTopWidthPHeightAutoSizeCaption
CB_FEC_VACColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_FEC_VAC
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelCB_DER_FEClblLeft.Top*Width>HeightCaptionUltimo Cierre:  TZetaDBTextBox
CB_DER_FECLeftnTop(WidthPHeightAutoSizeCaption
CB_DER_FECColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_DER_FEC
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m  TLabelCB_FEC_ANTlblLeft4Top@Width9Height	AlignmenttaRightJustifyCaptionAntig�edad:  TZetaDBTextBox
CB_FEC_ANTLeftnTop>WidthPHeightAutoSizeCaption
CB_FEC_ANTColorclWhiteParentColorShowAccelCharBrush.ColorclSilverBorder	DataField
CB_FEC_ANT
DataSource
DataSourceFormatFloat%14.2nFormatCurrency%m    �TPanelPanelInferiorTop� Height!TabOrder  TZetaDBGridDBGridLeftTopWidthkHeightAlignalClient
DataSourcedsVacaciones
FixedColorclInfoBkOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
VA_FEC_INITitle.CaptionSalidaWidthPVisible	 Expanded	FieldName
VA_FEC_FINTitle.CaptionRegresoWidthPVisible	 Expanded	FieldNameVA_GOZOTitle.CaptionD�as GozadosWidthXVisible	 Expanded	FieldNameVA_PAGOTitle.CaptionD�as PagadosWidthWVisible	 Expanded	FieldName
VA_PERIODOTitle.CaptionCorresponde aWidth� Visible	 Expanded	FieldName
VA_COMENTATitle.CaptionObservacionesWidth� Visible	     �TPanelTituloHeight)  TDataSourcedsVacacionesLeft� Top   