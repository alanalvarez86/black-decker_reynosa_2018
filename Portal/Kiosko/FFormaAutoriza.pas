unit FFormaAutoriza;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, StdCtrls, Mask, DBCtrls, Buttons, Grids, DBGrids, Db,
     FFormaBase,
     ZetaDBTextBox, ZetaDBGrid;

type
  TFormaAutoriza = class(TFormaBase)
    dsChecadas: TDataSource;
    DayBefore: TSpeedButton;
    DayAfter: TSpeedButton;
    GBChecadas: TGroupBox;
    ZDBGrid: TZetaDBGrid;
    PanelDatos: TPanel;
    GBHorario: TGroupBox;
    HO_HOR_IN: TZetaDBTextBox;
    HO_HOR_OU: TZetaDBTextBox;
    HO_HOR_INlbl: TLabel;
    HO_HOR_OUlbl: TLabel;
    HO_DESCRIPlbl: TLabel;
    HO_DESCRIP: TZetaDBTextBox;
    GBHoras: TGroupBox;
    AU_HORASCK: TZetaDBTextBox;
    AU_TARDES: TZetaDBTextBox;
    AU_HORASCKlbl: TLabel;
    AU_TARDESlbl: TLabel;
    GBHorasExtras: TGroupBox;
    SinExtras: TLabel;
    AU_AUT_EXTlbl: TLabel;
    AU_EXTRASlbl: TLabel;
    AU_AUT_EXT: TZetaDBTextBox;
    AU_EXTRAS: TZetaDBTextBox;
    AU_AUT_TRAlbl: TLabel;
    AU_AUT_TRA: TZetaDBTextBox;
    AU_DES_TRA: TZetaDBTextBox;
    AU_DES_TRAlbl: TLabel;
    GBPermisos: TGroupBox;
    SinPermisos: TLabel;
    AU_CG_ENTlbl: TLabel;
    AU_SG_ENTlbl: TLabel;
    AU_PER_CGlbl: TLabel;
    AU_PER_SGlbl: TLabel;
    AU_CG_ENT: TZetaDBTextBox;
    AU_SG_ENT: TZetaDBTextBox;
    AU_PER_CG: TZetaDBTextBox;
    AU_PER_SG: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
    procedure DayBeforeClick(Sender: TObject);
    procedure DayAfterClick(Sender: TObject);
  private
    { Private declarations }
    FDate: TDate;
    function GetDateText: String;
    procedure SetGroupBox(oGroupBox: TGroupBox; Leyenda: TLabel; const lHay: Boolean);
    procedure ShowGroupBoxes(const lHayAsistencia: Boolean);
  protected
    { Protected declarations }
    procedure SetControls; override;
    procedure SetControlsEmpty; override;
    procedure SetControlsFull; override;
  public
    { Public declarations }
    procedure Prepara; override;
    procedure Connect; override;
  end;

var
  FormaAutoriza: TFormaAutoriza;

implementation

uses DCliente,
     ZetaCommonTools,
     FKioscoRegistry;

{$R *.DFM}

procedure TFormaAutoriza.FormShow(Sender: TObject);
begin
     inherited;
     with KioskoRegistry do
     begin
          SetColorControl( GBHorasExtras, PanelInferiorFontColor );
          SetColorControl( GBPermisos, PanelInferiorFontColor );
          SetColorControl( GBChecadas, PanelInferiorFontColor );
     end;
end;

procedure TFormaAutoriza.SetGroupBox( oGroupBox: TGroupBox; Leyenda: TLabel; const lHay: Boolean );
var
   i: Integer;
begin
     for i := ( oGroupBox.ControlCount - 1 ) downto 0 do
     begin
          oGroupBox.Controls[ i ].Visible := lHay;
     end;
     Leyenda.Visible := not lHay;
end;

procedure TFormaAutoriza.ShowGroupBoxes( const lHayAsistencia: Boolean );
begin
     PanelDatos.Visible := lHayAsistencia;
end;

function TFormaAutoriza.GetDateText: String;
begin
     Result := GetDayAndDate( FDate );
end;

procedure TFormaAutoriza.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     ShowGroupBoxes( False );
     ControlsHide( PanelInferior );
     MessageShow( 'No Hay Registros De Asistencia' );
     SetTitulo( GetDateText );
end;

procedure TFormaAutoriza.SetControlsFull;
const
     K_CERO = 0.01;
var
   lHayPermisos, lHayExtras: Boolean;
begin
     inherited SetControlsFull;
     ControlsShow( PanelInferior );
     MessageClear;
     with Dataset do
     begin
          lHayPermisos := ( ( FieldByName( 'AU_CG_ENT' ).AsFloat +
                              FieldByName( 'AU_SG_ENT' ).AsFloat +
                              FieldByName( 'AU_PER_CG' ).AsFloat +
                              FieldByName( 'AU_PER_SG' ).AsFloat ) > K_CERO );
          lHayExtras := ( FieldByName( 'AU_EXTRAS' ).AsFloat > K_CERO );
          SetGroupBox( GBPermisos, SinPermisos, lHayPermisos );
          SetGroupBox( GBHorasExtras, SinExtras, lHayExtras );
          ShowGroupBoxes( True );
     end;
     SetTitulo( Format( 'Tarjeta De Asistencia De %s', [ GetDateText ] ) );
end;

procedure TFormaAutoriza.SetControls;
begin
     inherited SetControls;
end;

procedure TFormaAutoriza.Prepara;
begin
     inherited Prepara;
     FDate := Now;
end;

procedure TFormaAutoriza.Connect;
begin
     with dmCliente do
     begin
          AbreTarjeta( FDate );
          Datasource.Dataset := cdsTarjeta;
          dsChecadas.Dataset := cdsChecadas;
     end;
end;

procedure TFormaAutoriza.DayBeforeClick(Sender: TObject);
begin
     inherited;
     FDate := FDate - 1;
     DoConnect;
     TimerReset;
end;

procedure TFormaAutoriza.DayAfterClick(Sender: TObject);
begin
     inherited;
     FDate := FDate + 1;
     DoConnect;
     TimerReset;
end;

end.
