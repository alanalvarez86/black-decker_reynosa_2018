unit FFormaCalendario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, ExtCtrls, ComCtrls, StdCtrls, Buttons,
     ZetaDBGrid,
     FFormaBase;

type
  TFormaCalendario = class(TFormaBase)
    ZetaDBGrid: TZetaDBGrid;
    PanelMes: TPanel;
    MonthBefore: TSpeedButton;
    MonthAfter: TSpeedButton;
    PanelYear: TPanel;
    YearAfter: TSpeedButton;
    YearBefore: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure YearBeforeClick(Sender: TObject);
    procedure MonthBeforeClick(Sender: TObject);
    procedure MonthAfterClick(Sender: TObject);
    procedure YearAfterClick(Sender: TObject);
  private
    { Private declarations }
    FMonth: Word;
    FYear: Word;
    FYearInicial: Word;
    procedure MesSiguiente;
    procedure MesAnterior;
    procedure YearSiguiente;
    procedure YearAnterior;
  protected
    { Protected declarations }
    procedure SetControls; override;
    procedure SetControlsEmpty; override;
    procedure SetControlsFull; override;
  public
    { Public declarations }
    procedure Prepara; override;
    procedure Connect; override;
  end;

var
  FormaCalendario: TFormaCalendario;

implementation

uses DCliente;

{$R *.DFM}

const
     K_ENERO = 1;
     K_DICIEMBRE = 12;

procedure TFormaCalendario.FormShow(Sender: TObject);
begin
     inherited;
     with Titulo do
     begin
          PanelMes.Color := Color;
          PanelYear.Color := Color;
     end;
     SetTitulo( 'Calendario de Asistencia' );
end;

procedure TFormaCalendario.SetControls;
const
     K_LIMIT_BEFORE = 50;
     K_LIMIT_AFTER = 1;
begin
     inherited SetControls;
     PanelMes.Caption := LongMonthNames[ FMonth ];
     PanelYear.Caption := IntToStr( FYear );
     YearBefore.Enabled := ( FYear > ( FYearInicial - K_LIMIT_BEFORE ) );
     YearAfter.Enabled := ( FYear < ( FYearInicial + K_LIMIT_AFTER ) );
     MonthBefore.Enabled := ( FMonth > K_ENERO ) or ( ( FMonth = K_ENERO ) and YearBefore.Enabled );
     MonthAfter.Enabled := ( FMonth < K_DICIEMBRE ) or ( ( FMonth = K_DICIEMBRE ) and YearAfter.Enabled );
end;

procedure TFormaCalendario.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     ControlsHide( PanelInferior );
     MessageShow( 'No Hay Registros De Asistencia' );
end;

procedure TFormaCalendario.SetControlsFull;
begin
     inherited SetControlsFull;
     ControlsShow( PanelInferior );
     MessageClear;
end;

procedure TFormaCalendario.Prepara;
var
   iDia: Word;
begin
     inherited Prepara;
     DecodeDate( Date, FYear, FMonth, iDia );
     FYearInicial := FYear;
end;

procedure TFormaCalendario.Connect;
begin
     with dmCliente do
     begin
          AbreCalendario( FMonth, FYear );
          Datasource.Dataset := cdsCalendario;
     end;
end;

procedure TFormaCalendario.YearAnterior;
begin
     Dec( FYear );
     DoConnect;
end;

procedure TFormaCalendario.YearSiguiente;
begin
     Inc( FYear );
     DoConnect;
end;

procedure TFormaCalendario.MesAnterior;
begin
     if ( FMonth = K_ENERO ) then
     begin
          FMonth := K_DICIEMBRE;
          YearAnterior;
     end
     else
     begin
          Dec( FMonth );
          DoConnect;
     end;

end;

procedure TFormaCalendario.MesSiguiente;
begin
     if ( FMonth = K_DICIEMBRE ) then
     begin
          FMonth := K_ENERO;
          YearSiguiente;
     end
     else
     begin
          Inc( FMonth );
          DoConnect;
     end;
end;

procedure TFormaCalendario.YearBeforeClick(Sender: TObject);
begin
     inherited;
     YearAnterior;
     TimerReset;
end;

procedure TFormaCalendario.MonthBeforeClick(Sender: TObject);
begin
     inherited;
     MesAnterior;
     TimerReset;

end;

procedure TFormaCalendario.MonthAfterClick(Sender: TObject);
begin
     inherited;
     MesSiguiente;
     TimerReset;
end;

procedure TFormaCalendario.YearAfterClick(Sender: TObject);
begin
     inherited;
     YearSiguiente;
     TimerReset;
end;

end.
