unit FFormaReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FFormaBase, Grids, DBGrids, Db, ExtCtrls, ImgList, ComCtrls;

type
  TFormaReportes = class(TFormaBase)
    ListaReportes: TListView;
    ListaLargeImages: TImageList;
    procedure FormShow(Sender: TObject);
    procedure ListaReportesClick(Sender: TObject);
  private
    procedure InvocaReporte(const iCodigo: Integer);
    procedure LlenaListaReportes;
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
  end;

var
  FormaReportes: TFormaReportes;

implementation

uses FFormaEmpleado,
     DCliente;

{$R *.DFM}

procedure TFormaReportes.FormShow(Sender: TObject);
begin
     inherited;
     SetTitulo( 'Reportes' );
end;

procedure TFormaReportes.Connect;
begin
     with dmCliente do
     begin
          CargaReportes;
          Datasource.Dataset := cdsReportes;
     end;
     LlenaListaReportes;
end;

procedure TFormaReportes.LlenaListaReportes;
begin
     with ListaReportes.Items do
     begin
          BeginUpdate;
          try
             Clear;
             with dmCliente.cdsReportes do
             begin
                  if ( not IsEmpty ) then
                  begin
                       DisableControls;
                       try
                          First;
                          while ( not EOF ) do
                          begin
                               with Add do
                               begin
                                    Caption := Trim( FieldByName('RE_NOMBRE').AsString );
                                    SubItems.Add( IntToStr( FieldByName( 'RE_CODIGO' ).AsInteger ) );
                                    ImageIndex := FieldByName( 'RE_TIPO' ).AsInteger;
                               end;
                               Next;
                          end;
                       finally
                              EnableControls;
                       end;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TFormaReportes.ListaReportesClick(Sender: TObject);
begin
     inherited;
     with ListaReportes do
     begin
          if ( Selected <> nil ) then
          begin
               InvocaReporte( StrToIntDef( Selected.SubItems[0], 0 ) );
          end;
     end;
end;

procedure TFormaReportes.InvocaReporte( const iCodigo: Integer );
begin
     try
        FormaEmpleado.TimerStop;
        dmCliente.GeneraReporte( iCodigo );
     finally
            FormaEmpleado.TimerStart;
     end;
end;

end.
