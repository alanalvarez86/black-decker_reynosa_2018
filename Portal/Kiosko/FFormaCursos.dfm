�
 TFORMACURSOS 0�  TPF0�TFormaCursosFormaCursosLeft5Top]CaptionFormaCursosClientHeight5PixelsPerInch`
TextHeight �TPanelPanelSuperiorTop Height 
Font.ColorclBlackFont.Height�
Font.StylefsBold 
ParentFont  �TPanelPanelInferiorTop Height TPanelPanelTomadosLeftTop� WidthkHeight� AlignalClient
BevelOuterbvNoneColorclTealFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabel
LblTomadosLeft Top WidthkHeightAlignalTopCaptionCursos Tomados  TZetaDBGridZetaDBGrid2Left TopWidthkHeight� AlignalClient
DataSourcedsHisCursos
FixedColorclInfoBkOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
KC_FEC_TOMTitle.CaptionFechaWidthZVisible	 Expanded	FieldNameNOMBRETitle.CaptionCursoWidth� Visible	 Expanded	FieldName
KC_REVISIOTitle.CaptionRevisi�nWidthAVisible	 Expanded	FieldNameKC_HORASTitle.AlignmenttaRightJustifyTitle.CaptionHorasWidth0Visible	 Expanded	FieldName	KC_EVALUATitle.AlignmenttaRightJustifyTitle.Caption
Evaluaci�nWidthRVisible	 Expanded	FieldNameSE_FOLIOTitle.CaptionSesi�nWidthUVisible	     TPanelPanelProgramadosLeftTopWidthkHeight� AlignalTop
BevelOuterbvNoneColorclTealFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLblProgramadosLeft Top WidthkHeightAlignalTopCaptionCursos Programados  TZetaDBGridZetaDBGrid1Left TopWidthkHeightxAlignalClient
DataSource
DataSource
FixedColorclInfoBkOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclBlackTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
KC_FEC_PROTitle.Caption
ProgramadoWidthZVisible	 Expanded	FieldNameNOMBRETitle.CaptionCursoWidth� Visible	 Expanded	FieldName
KC_REVISIOTitle.CaptionRevisi�nWidthAVisible	 Expanded	FieldName
KC_FEC_TOMTitle.CaptionTomadoWidthKVisible	 Expanded	FieldName
KC_PROXIMOTitle.CaptionPr�ximoWidthKVisible	 Expanded	FieldNameKC_HORASTitle.AlignmenttaRightJustifyTitle.CaptionHorasWidth0Visible	 Expanded	FieldName	KC_EVALUATitle.AlignmenttaRightJustifyTitle.Caption
Evaluaci�nWidthRVisible	 Expanded	FieldName
EN_OPCIONATitle.CaptionOpcionalWidthAVisible	 Expanded	FieldName	MA_CODIGOTitle.CaptionMaestroVisible	 Expanded	FieldName
CP_INDIVIDTitle.Caption
IndividualWidthFVisible	      �TDataSource
DataSourceLeft� Top  TDataSourcedsHisCursosLeft�    