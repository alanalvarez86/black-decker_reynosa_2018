unit FPortada;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, ComCtrls, Buttons, ComObj, ActiveX;

type
  TPortada = class(TForm)
    BotonesGroupBox: TGroupBox;
    Iniciar: TBitBtn;
    Salir: TBitBtn;
    Configurar: TBitBtn;
    Empresas: TBitBtn;
    Servidor: TBitBtn;
    GafeteGB: TGroupBox;
    EmpleadoLBL: TLabel;
    Empleado: TEdit;
    Timer: TTimer;
    Regresar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure IniciarClick(Sender: TObject);
    procedure ServidorClick(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
    procedure EmpresasClick(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure RegresarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
  private
    { Private declarations }
    FHuboShow: Boolean;
    FEsShow: Boolean;
    FEmpleado: String;
    FCodigoBarra: String;
    procedure AjustarGroupBox(Grupo: TGroupBox);
    procedure CicloSecuencia;
    procedure MuestraFormaEmpleado( const sCodigo: String );
    procedure ShowBotones( const lPrende: Boolean );
    procedure TimerStart;
    procedure TimerStop;
  public
    { Public declarations }
  end;

function EntrarDirecto( var sEmpleado: String ): Boolean;

var
  Portada: TPortada;

implementation

uses FFormaEmpleado,
     FKioscoRegistry,
     FEmpresas,
     {$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor,
     {$else}
     ZetaRegistryCliente,
     FBuscaServidor,
     {$endif}
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.DFM}

function EntrarDirecto( var sEmpleado: String ): Boolean;
const
     K_SHOW = 'SHOW';
     K_SWITCHES = [ '-', '/' ];
begin
     sEmpleado := VACIO;
     Result := SysUtils.FindCmdLineSwitch( K_SHOW, K_SWITCHES, True );
     if not Result then
     begin
          if ( System.ParamCount = 1 ) then
          begin
               sEmpleado := System.ParamStr( 1 );
               Result := ZetaCommonTools.StrLleno( sEmpleado );
          end;
     end;
end;

{ ********* TPortada ********* }

procedure TPortada.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     FHuboShow := False;
     FormaEmpleado := TFormaEmpleado.Create( Self );
     FEsShow := EntrarDirecto( FEmpleado );
     FormaEmpleado.EsShow := FEsShow;
end;

procedure TPortada.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FormaEmpleado );
end;

procedure TPortada.FormShow(Sender: TObject);
const
     K_UN_SEGUNDO = 1000;
begin
     Configurar.Enabled := KioskoRegistry.CanWrite;
     Empresas.Enabled := Configurar.Enabled;
     BotonesGroupBox.Visible := not FEsShow;
     GafeteGB.Visible := FEsShow;
     Timer.Interval := KioskoRegistry.PantallasTimeout * K_UN_SEGUNDO;
end;

procedure TPortada.FormResize(Sender: TObject);
begin
     AjustarGroupBox( BotonesGroupBox );
     AjustarGroupBox( GafeteGB );
end;

procedure TPortada.FormKeyPress(Sender: TObject; var Key: Char);
const
     K_ESC = 27;
     K_LF = 13;
begin
     case Ord( Key ) of
          K_ESC: Regresar.Click;
          K_LF:
          begin
               FCodigoBarra := Empleado.Text;
               if ( Length( FCodigoBarra ) > 0 ) then
               begin
                    MuestraFormaEmpleado( FCodigoBarra );
                    FCodigoBarra := VACIO;
               end
               else
                   Regresar.Click;
           end;
     end;
end;

procedure TPortada.FormActivate(Sender: TObject);
begin
     if FEsShow then
     begin
          TimerStart;
     end;
end;

procedure TPortada.FormDeactivate(Sender: TObject);
begin
     if FEsShow then
     begin
          TimerStop;
     end;
end;

procedure TPortada.AjustarGroupBox( Grupo: TGroupBox );
begin
     with Grupo do
     begin
          Top := Trunc( ( Self.Height - Height ) / 2 );
          Left := Trunc( ( Self.Width - Width ) / 2 );
     end;
end;

procedure TPortada.TimerStart;
begin
     TimerStop;
     with Timer do
     begin
          Enabled := True;
     end;
end;

procedure TPortada.TimerStop;
begin
     with Timer do
     begin
          Enabled := False;
     end;
end;

procedure TPortada.CicloSecuencia;
var
   sCodigo: String;
begin
     {$ifdef FALSE}
     repeat
           with dmPantallas do
           begin
                Codigo := ShowSecuencia( Registry.InicioSecuencia, FALSE );
           end;
           if ZetaCommonTools.StrLleno( sCodigo ) then
           begin
                MuestraFormaEmpleado( sCodigo );
           end;
     until ZetaCommonTools.StrVacio( sCodigo );
     {$else}
     sCodigo := '';
     MuestraFormaEmpleado( sCodigo );
     {$endif}
end;

procedure TPortada.MuestraFormaEmpleado( const sCodigo: String );
begin
     if not FHuboShow then
     begin
        // Obliga a que se maximize en ese momento
        // para que obtenga los tama�os dependiendo
        // de la resoluci�n
        FormaEmpleado.Show;
        {
        FormaEmpleado.Posiciona( Codigo );
        FormaEmpleado.Hide;
        }
        FHuboShow := True;
     end;
     FormaEmpleado.ShowEmpleado( sCodigo );
end;

procedure TPortada.ShowBotones( const lPrende: Boolean );
begin
     BotonesGroupBox.Visible := lPrende;
     Application.ProcessMessages;
end;

procedure TPortada.IniciarClick(Sender: TObject);
begin
     ShowBotones( False );
     try
        CicloSecuencia;
     finally
            ShowBotones( True );
     end;
end;

procedure TPortada.ServidorClick(Sender: TObject);
begin
     ShowBotones( False );
     try
        {$ifdef DOS_CAPAS}
        ZetaRegistryServerEditor.EspecificaComparte;
        {$else}
        with TFindServidor.Create( Application ) do
        begin
             try
                Servidor := ClientRegistry.ComputerName;
                if ( ShowModal = mrOk ) then
                begin
                     ClientRegistry.ComputerName := Servidor;
                end;
             finally
                    Free;
             end;
        end;
        {$endif}
     finally
            ShowBotones( True );
     end;
end;

procedure TPortada.ConfigurarClick(Sender: TObject);
begin
     ShowBotones( False );
     try
        FKioscoRegistry.EditKioskoRegistry;
     finally
            ShowBotones( True );
     end;
end;

procedure TPortada.EmpresasClick(Sender: TObject);
begin
     ShowBotones( False );
     try
        FEmpresas.EditEmpresas;
     finally
            ShowBotones( True );
     end;
end;

procedure TPortada.TimerTimer(Sender: TObject);
begin
     RegresarClick( Sender );
end;

procedure TPortada.RegresarClick(Sender: TObject);
begin
     Close;
end;

procedure TPortada.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
