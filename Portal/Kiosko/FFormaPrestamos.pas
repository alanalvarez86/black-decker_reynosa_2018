unit FFormaPrestamos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls, Db, Mask, DBCtrls,
     FFormaBase, ZetaDBTextBox;

type
  TFormaPrestamos = class(TFormaBase)
    DBNavigator: TDBNavigator;
    PR_DESCRIPCION: TZetaDBTextBox;
    PR_DESCRIPCIONlbl: TLabel;
    PR_REFERENlbl: TLabel;
    PR_REFEREN: TZetaDBTextBox;
    PR_MONTOlbl: TLabel;
    PR_MONTO: TZetaDBTextBox;
    PR_CARGOSlbl: TLabel;
    PR_CARGOS: TZetaDBTextBox;
    PR_SALDOlbl: TLabel;
    PR_SALDO: TZetaDBTextBox;
    PR_FECHAlbl: TLabel;
    PR_FECHA: TZetaDBTextBox;
    PR_NUMEROlbl: TLabel;
    PR_NUMERO: TZetaDBTextBox;
    PR_STATUSlbl: TLabel;
    PR_STATUS: TZetaDBTextBox;
    PR_SALDO_Ilbl: TLabel;
    PR_SALDO_I: TZetaDBTextBox;
    PR_TOTALlbl: TLabel;
    PR_TOTAL: TZetaDBTextBox;
    PR_ABONOS: TZetaDBTextBox;
    PR_ABONOSlbl: TLabel;
    procedure FormShow(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure SetControlsEmpty; override;
    procedure SetControlsFull; override;
  public
    { Public declarations }
    procedure Connect; override;
  end;

var
  FormaPrestamos: TFormaPrestamos;

implementation

uses DCliente,
     FKioscoRegistry;

{$R *.DFM}

procedure TFormaPrestamos.FormShow(Sender: TObject);
begin
     inherited;
     SetTitulo( 'Préstamos' );
end;

procedure TFormaPrestamos.Connect;
begin
     with dmCliente do
     begin
          AbrePrestamos;
          Datasource.Dataset := cdsPrestamos;
     end;
end;

procedure TFormaPrestamos.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     inherited;
     TimerReset;
end;

procedure TFormaPrestamos.SetControlsEmpty;
begin
     inherited SetControlsEmpty;
     ControlsHide( PanelSuperior );
     ControlsHide( PanelInferior );
     MessageShow( 'No Hay Préstamos' );
end;

procedure TFormaPrestamos.SetControlsFull;
begin
     inherited SetControlsFull;
     ControlsShow( PanelSuperior );
     ControlsShow( PanelInferior );
     MessageClear;
end;

end.
