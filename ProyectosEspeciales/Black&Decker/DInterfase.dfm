object dmInterfase: TdmInterfase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 479
  Width = 741
  object cdsAsciiKardex: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 24
    object cdsAsciiKardexRenglon: TStringField
      DisplayWidth = 2048
      FieldName = 'Renglon'
      Size = 2048
    end
    object cdsAsciiKardexID: TIntegerField
      FieldName = 'ID'
    end
  end
end
