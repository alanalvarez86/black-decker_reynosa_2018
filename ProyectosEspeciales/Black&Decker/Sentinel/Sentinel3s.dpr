program Sentinel3s;

uses
  SvcMgr,
  FSentinelRegistry in '..\..\..\..\3Win_20\ProyectosEspeciales\Electrolux\Sentinel\FSentinelRegistry.pas',
  DSentinel in '..\..\..\..\3Win_20\ProyectosEspeciales\Electrolux\Sentinel\DSentinel.pas' {Sentinel3Service: TService};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Sentinel Corporativo';
  Application.CreateForm(TSentinel3Service, Sentinel3Service);
  Application.Run;
end.
