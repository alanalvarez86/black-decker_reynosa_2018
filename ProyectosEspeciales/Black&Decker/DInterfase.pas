unit DInterfase;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet,  variants,
  ZetaTipoEntidad,
  ZetaTipoEntidadTools,
  ZetaAsciiFile,
  ZetaCommonLists,
  ZetaCommonClasses;

const
     K_MAX_CAMPOS = 20;
type
  eTipoArchivo = ( taAltas, taKardex );
  TdmInterfase = class(TDataModule)
    cdsAsciiKardex: TZetaClientDataSet;
    cdsAsciiKardexRenglon: TStringField;
    cdsAsciiKardexID: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios, FProcesandoThread : Boolean;
    FFormato : eFormatoASCII;
    FFormatoFecha : eFormatoImpFecha;
    FTipoArchivo : eTipoArchivo;
    FArchivo : String;
    FParameterList: TZetaParams;
    FDato: TStrings;
    FCarga: TStrings;
    FDatoKardex: TStrings;
    FCampos: array[ 0..K_MAX_CAMPOS ] of string;
    Fempleado: integer;
    FRespaldo : TAsciiLog;
    function cdsASCII: TZetaClientDataSet;
    function GetHeaderTexto(const iEmpleado: Integer): String;
    function strToken( var sValue: String; const cSeparator: Char ): String;
    function CargaArchivo: boolean;
    function ValidaEnviar( DataSet: TZetaClientDataSet ): boolean;
    procedure LeeCampos( sLinea: string );
    procedure ProcesaLinea( sLinea: string );
    procedure LimpiaDataSets;
    procedure SetNuevosEventos;
    procedure EscribeBitacora(const sMensaje: String; const iEmpleado: Integer = 0 );
    procedure EscribeError(const sMensaje: String; const iEmpleado: Integer = 0 );
    procedure ReportaProcessDetail(const iFolio: Integer);
    procedure LimpiaCampos;
    procedure CreaDataSets;
    procedure RegistraEmpleado;
    procedure cdsHisCursosBeforePost(DataSet: TDataSet);
    {$ifdef VER150}
    procedure cdsHisCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsHisCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure CreaArchivoRespaldo;
    procedure EscribeRespaldo(const sRenglon: string);
  public
    { Public declarations }
    property Formato: eFormatoASCII read FFormato write FFormato;
    property FormatoFecha: eFormatoImpFecha read FFormatoFecha write FFormatoFecha;
    property TipoArchivo: eTipoArchivo read FTipoArchivo write FTipoArchivo;
    property Archivo: String read FArchivo write FArchivo;
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure EscribeErrorExterno( const sMensaje: string );
    procedure Procesa;
  end;

var
  dmInterfase: TdmInterfase;

implementation

uses dProcesos,
     dCliente,
     dRecursos,
     dAsistencia,
     dTablas,
     dConsultas,
     dGlobal,
     dCatalogos,
     ZGlobalTress,
     FTressShell,
     ZAsciiTools,
     ZBaseThreads,
     ZBaseSelectGrid,
     ZetaClientTools,
     FEmpVerificaTablasGridSelect_DevEx,
     ZetaCommonTools;

{$R *.DFM}

{ TdmInterfase }

procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     FDato := TStringList.Create;
     FDatoKardex := TStringList.Create;
     FCarga := TStringList.Create;
     FFormato := faASCIIDel;
     FFormatoFecha := ifMMDDYYYYs;
     FTipoArchivo := taAltas;
     FArchivo := ZetaCommonClasses.VACIO;
     SetNuevosEventos;
     CreaDataSets;
     CreaArchivoRespaldo;
end;

procedure TdmInterfase.CreaArchivoRespaldo;
begin
     if ( FRespaldo = NIL ) then
        FRespaldo := TAsciiLog.Create;
     FRespaldo.Init( ExtractFilePath( Application.ExeName ) + 'Respaldo.log' );
     FRespaldo.WriteTimeStamp( '********** %s **********' );
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FRespaldo );
     FreeAndNil( FCarga );
     FreeAndNil( FDatoKardex );
     FreeAndNil( FDato );
end;

procedure TdmInterfase.EscribeRespaldo( const sRenglon: string );
begin
     FRespaldo.WriteTexto( sRenglon );
end;

function TdmInterfase.cdsASCII: TZetaClientDataSet;
begin
     Result := dmProcesos.cdsASCII;
end;

{ Manejo de Fin de Proceso - Bit�cora }

procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
   oProcessData: TProcessInfo;

   function GetResultadoProceso: String;
   begin

        case oProcessData.Status of
             epsOK: Result := ZetaCommonClasses.VACIO; //Result := '******* Proceso Terminado *******';
             epsCancelado: Result := '*******  Proceso Cancelado  *******';
             epsError, epsCatastrofico: Result := '******* Proceso Con Errores *******';
        else
             Result := ZetaCommonClasses.VACIO;
        end;
   end;

begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        if strLleno( GetResultadoProceso ) then
           EscribeBitacora( GetResultadoProceso );
     finally
            FreeAndNil( oProcessData );
            FProcesandoThread := FALSE;      // Mientras se encuentra la forma de manejar los threads
     end;
end;

procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha Movimiento: %s - %s';
var
   sMensaje: String;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave : EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

{ Reporta Errores en el ASCII }

{ Utilerias de Bit�cora }

function TdmInterfase.GetHeaderTexto( const iEmpleado: Integer ): String;
const
     K_HEADER_EMPLEADO = 'Empleado No. %d : ';
begin
     Result := VACIO;
     if ( iEmpleado > 0 ) then
        Result := Format( K_HEADER_EMPLEADO, [ iEmpleado ] )
     else
        Result := FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + ' : ';
end;

procedure TdmInterfase.EscribeBitacora(const sMensaje: String; const iEmpleado: Integer = 0 );
begin
     if ( not FHuboCambios ) and ( iEmpleado > 0 ) then       // Para saber si hay que poner un mensaje de que no hubo cambios
        FHuboCambios := TRUE;
     TressShell.EscribeBitacora( GetHeaderTexto( iEmpleado ) + sMensaje );
end;

procedure TdmInterfase.EscribeError(const sMensaje: String; const iEmpleado: Integer = 0 );
begin
     TressShell.EscribeError( GetHeaderTexto( 0 ) + GetHeaderTexto( iEmpleado ) + sMensaje );
end;

{ Rutinas de lectura del archivo }
procedure TdmInterfase.SetNuevosEventos;
begin
    with dmCatalogos.cdsHisCursos do
     begin
          OnReconcileError := self.cdsHisCursosReconcileError;
          BeforePost := self.cdsHisCursosBeforePost;
     end;
end;

procedure TdmInterfase.CreaDataSets;
begin
     cdsAsciiKardex.CreateTempDataset;
end;

procedure TdmInterfase.LimpiaCampos;
var
   i: integer;
begin
     for i := 0 to K_MAX_CAMPOS do
     begin
          FCampos[ i ] := VACIO;
     end;
end;

{ Procesamiento de los datos }
procedure TdmInterfase.Procesa;
begin
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     EscribeBitacora( '*******  Proceso iniciado   *******' );
     dmCliente.cdsTPeriodos.Refrescar;     
     LimpiaCampos;
     ZAsciiTools.FillcdsASCII( cdsASCII, FArchivo );
     FParameterList := TZetaParams.Create;
     try
        if ( not cdsASCII.IsEmpty ) then
        begin
             EscribeBitacora( 'Procesando registros...' );        
             CargaArchivo;
        end;
     finally
            FreeAndNil( FParameterList );
     end;
     Application.ProcessMessages;

     if ( not FHuboCambios ) then
        EscribeBitacora( 'No se encontraron cambios que procesar' );
     if ( TressShell.MemErrores.Lines.Count <> 0 ) then
        EscribeBitacora( 'Hay errores en el proceso, revisar la bit�cora' );
     EscribeBitacora( '*******  Proceso terminado  *******' );
end;

function TdmInterfase.CargaArchivo: boolean;
var
   i: integer;

   procedure EscribeLineaXLinea;
   var
      x: integer;
   begin
        with FCarga do
        begin
             for x := 0 to ( Count - 1 ) do
             begin
                  EscribeRespaldo( Strings[ x ] ) ;
             end;
             FRespaldo.WriteTimeStamp( '********** %s **********' );             
        end;
   end;
begin
     Result := TRUE;
     if strLleno( FArchivo ) then
     begin
          LimpiaDataSets;
          FCarga.LoadFromFile( FArchivo );
          try
             EscribeLineaXLinea;
             with FCarga do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       ProcesaLinea( strings[ i ] );
                  end;
             end;
          except
                on Error: exception do
                begin
                     Result := FALSE;
                     EscribeError( Format( 'Error al cargar archivo ASCII [ %s ]', [ Error.message ] ) );
                end;
          end;
     end
     else
     begin
          Result := FALSE;
          EscribeError( '�No existe el archivo especificado!' );
     end;
end;

procedure TdmInterfase.ProcesaLinea( sLinea: string );
begin
     LeeCampos( sLinea );
     if ( FEmpleado <> 0 ) then
     begin
          EscribeBitacora( Format( 'Procesando # de empleado %d' , [ FEmpleado ] ) ); 
          if ( dmCliente.cdsEmpleadoLookup.GetDescripcion( IntToStr( FEmpleado ) ) <> VACIO ) then
             RegistraEmpleado
          else
             EscribeError( 'No existe el empleado #' + IntToStr( FEmpleado ) );
     end
     else
         EscribeError( '�N�mero de empleado es igual a cero!' );
end;

procedure TdmInterfase.RegistraEmpleado;
begin
     with dmCatalogos do
     begin
          FolioSesion := StrToIntDef( FCampos[ 1 ], 0 );
          with cdsSesiones do
          begin
               Refrescar;
               First;
          end;
          if cdsSesiones.Locate( 'SE_FOLIO', VarArrayof( [ FolioSesion ] ), [] ) then
          begin
               with cdsHisCursos do
               begin
                    Refrescar;
                    Insert;
                    FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleado;
                    FieldByName( 'KC_REVISIO' ).AsString := cdsSesiones.FieldByName( 'SE_REVISIO' ).AsString;
                    FieldByName( 'KC_HORAS' ).AsFloat := cdsSesiones.FieldByName( 'SE_HORAS' ).AsFloat;
                    FieldByName( 'MA_CODIGO' ).AsString := cdsSesiones.FieldByName( 'MA_CODIGO' ).AsString;
                    Post;
                    if ValidaEnviar( cdsSesiones ) then
                    begin
                         FHuboCambios := True;
                    end
                    else
                        EscribeError( 'Error al registar asistencia del empleado', FEmpleado );
               end;
          end
          else
          begin
               EscribeError( Format( 'El c�digo de la sesi�n [ %d ] No Existe en Base de Datos', [ FolioSesion ] ) );
          end;
     end;
end;

procedure TdmInterfase.LeeCampos( sLinea: string );
var
   i: integer;
begin
     for i := 0 to K_MAX_CAMPOS do
     begin
          FCampos[ i ] := Trim( strToken( sLinea, ',' ) );
     end;
     FEmpleado := StrToIntDef( FCampos[ 0 ], 0 );
end;

procedure TdmInterfase.LimpiaDataSets;
begin
     cdsAsciiKardex.EmptyDataSet;
end;

function TdmInterfase.strToken( var sValue: String; const cSeparator: Char ): String;
var
   i: Word;
begin
     i := Pos( cSeparator, sValue );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sValue, 1, ( i - 1 ) );
          System.Delete( sValue, 1, i );
     end
     else
     begin
          Result := sValue;
          sValue := VACIO;
     end;
end;

procedure TdmInterfase.cdsHisCursosBeforePost(DataSet: TDataSet);
begin
     with dmCatalogos do
     begin
          cdsHisCursos.FieldByName( 'KC_FEC_TOM' ).AsDateTime := cdsSesiones.FieldByName('SE_FEC_INI').AsDateTime;
          cdsHisCursos.FieldByName( 'KC_FEC_FIN' ).AsDateTime := cdsSesiones.FieldByName('SE_FEC_FIN').AsDateTime;
          if ( cdsHisCursos.FieldByName( 'CB_CODIGO' ).AsInteger = 0 ) then
               EscribeError( 'N�mero de empleado no puede quedar vac�o', FEmpleado );
          if( cdsHisCursos.FieldByName( 'KC_HORAS' ).AsFloat <= 0 )then
              EscribeError( 'Las horas deben ser mayores a cero', FEmpleado );
          if( cdsHisCursos.FieldByName( 'KC_EVALUA' ).AsFloat < 0 )then
              EscribeError( 'La evaluaci�n debe ser mayor o igual a cero', FEmpleado );
          cdsHisCursos.FieldByName( 'CU_CODIGO' ).AsString := cdsSesiones.FieldByName( 'CU_CODIGO' ).AsString;
          cdsHisCursos.FieldByName( 'SE_FOLIO' ).AsInteger := cdsSesiones.FieldByName( 'SE_FOLIO' ).AsInteger;
     end;
end;

{$ifdef VER150}
procedure TdmInterfase.cdsHisCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
    if PK_Violation( E ) then
    begin
         with DataSet do
         begin
              sError := Format( 'Curso ya fu� Registrado al Empleado. Fecha: %s', [ FechaCorta( FieldByName( 'KC_FEC_TOM' ).AsDateTime ) ] );
         end;
    end
    else
        sError := GetErrorDescription( E );
    //dmCatalogos.cdsHisCursos.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
    EscribeError( sError, FEmpleado );
    Action := raCancel;
end;
{$else}
procedure TdmInterfase.cdsHisCursosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError : String;
begin
    if PK_Violation( E ) then
    begin
         with DataSet do
         begin
              sError := Format( 'Curso ya fu� Registrado al Empleado. Fecha: %s', [ FechaCorta( FieldByName( 'KC_FEC_TOM' ).AsDateTime ) ] );
         end;
    end
    else
        sError := GetErrorDescription( E );
    //dmCatalogos.cdsHisCursos.ReconciliaError(DataSet,UpdateKind,E, 'CB_CODIGO', sError );
    EscribeError( sError, FEmpleado );
    Action := raCancel;
end;
{$endif}

function TdmInterfase.ValidaEnviar( DataSet: TZetaClientDataSet ): boolean;
begin
     Result := False;
     try
        DataSet.Enviar;
        Result := True;
     except
        on Error: Exception do
        begin
             EscribeError( Error.Message, FEmpleado );
             DataSet.CancelUpdates;
        end;
     end;
end;

// Este prodecimiento nos sirve para invocarse de unidades externas a la interfaz
// el cual a su vez env�a el mensaje al EscribeErro utilizando el cliente actual.
procedure TdmInterfase.EscribeErrorExterno( const sMensaje: string );
begin
     EscribeError( sMensaje, FEmpleado );
end;

end.
