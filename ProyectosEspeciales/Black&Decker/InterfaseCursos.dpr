program InterfaseCursos;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}

uses
  MidasLib,
  Forms,
  ComObj,
  ActiveX,
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in '..\..\Tools\ZetaSplash.pas' {SplashScreen},
  FConfiguracion in 'FConfiguracion.pas' {Configuracion},
  ZBaseImportaShell_DevEx in '..\..\Tools\ZBaseImportaShell_DevEx.pas' {BaseImportaShell_DevEx};

{$R *.RES}


begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     if ( ParamCount = 0 ) then
     Application.Title := 'Interfase para Cursos B&D';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
