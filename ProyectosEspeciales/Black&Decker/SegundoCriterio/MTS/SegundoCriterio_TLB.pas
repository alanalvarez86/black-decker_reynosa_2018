unit SegundoCriterio_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 12/4/2008 1:00:07 PM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3win_20\ProyectosEspeciales\B&D\SegundoCriterio\MTS\SegundoCriterio.tlb (1)
// LIBID: {B5AF7D5D-B85C-4F53-B381-4866F2527479}
// LCID: 0
// Helpfile: 
// HelpString: Tress Segundo Criterio
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\system32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  SegundoCriterioMajorVersion = 1;
  SegundoCriterioMinorVersion = 0;

  LIBID_SegundoCriterio: TGUID = '{B5AF7D5D-B85C-4F53-B381-4866F2527479}';

  IID_IdmServerSegundoCriterio: TGUID = '{B8FD746C-6143-4F00-B434-ADE76EB43F78}';
  CLASS_dmServerSegundoCriterio: TGUID = '{9A16E55F-38F0-4E74-B93C-0D35082E1B94}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IdmServerSegundoCriterio = interface;
  IdmServerSegundoCriterioDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  dmServerSegundoCriterio = IdmServerSegundoCriterio;


// *********************************************************************//
// Interface: IdmServerSegundoCriterio
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B8FD746C-6143-4F00-B434-ADE76EB43F78}
// *********************************************************************//
  IdmServerSegundoCriterio = interface(IAppServer)
    ['{B8FD746C-6143-4F00-B434-ADE76EB43F78}']
    function GetMatriz(Empresa: OleVariant; lCurso: WordBool; const sCodigo: WideString): OleVariant; safecall;
    function GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; 
                         out ErrorCount: Integer): OleVariant; safecall;
    function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; safecall;
    function ActualizaGlobal2Entrena(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerSegundoCriterioDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {B8FD746C-6143-4F00-B434-ADE76EB43F78}
// *********************************************************************//
  IdmServerSegundoCriterioDisp = dispinterface
    ['{B8FD746C-6143-4F00-B434-ADE76EB43F78}']
    function GetMatriz(Empresa: OleVariant; lCurso: WordBool; const sCodigo: WideString): OleVariant; dispid 301;
    function GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; 
                         out ErrorCount: Integer): OleVariant; dispid 302;
    function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer; oDelta: OleVariant; 
                           out ErrorCount: Integer): OleVariant; dispid 303;
    function ActualizaGlobal2Entrena(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 304;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CodmServerSegundoCriterio provides a Create and CreateRemote method to          
// create instances of the default interface IdmServerSegundoCriterio exposed by              
// the CoClass dmServerSegundoCriterio. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CodmServerSegundoCriterio = class
    class function Create: IdmServerSegundoCriterio;
    class function CreateRemote(const MachineName: string): IdmServerSegundoCriterio;
  end;

implementation

uses ComObj;

class function CodmServerSegundoCriterio.Create: IdmServerSegundoCriterio;
begin
  Result := CreateComObject(CLASS_dmServerSegundoCriterio) as IdmServerSegundoCriterio;
end;

class function CodmServerSegundoCriterio.CreateRemote(const MachineName: string): IdmServerSegundoCriterio;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerSegundoCriterio) as IdmServerSegundoCriterio;
end;

end.
