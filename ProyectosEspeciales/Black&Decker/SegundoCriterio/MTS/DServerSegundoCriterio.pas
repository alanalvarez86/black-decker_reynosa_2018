unit DServerSegundoCriterio;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComServ, ComObj, VCLCom, StdVcl, {bdemts,} DataBkr, DBClient,
  MtsRdm, Mtx, SegundoCriterio_TLB, DZetaServerProvider, DB,
  ZetaServerDataSet, ZetaCommonLists;

type
  eTipoCatalogo = ( eMatrizCurso, {0}
                    eMatrizPuesto {1}
                    );

  TdmServerSegundoCriterio = class(TMtsDataModule, IdmServerSegundoCriterio)
    cdsTemporal: TServerDataSet;
    procedure dmServerSegundoCriterioCreate(Sender: TObject);
    procedure dmServerSegundoCriterioDestroy(Sender: TObject);
  private
         oZetaProvider: TdmZetaServerProvider;
         FTipoCatalogo: eTipoCatalogo;
         procedure SetDetailInfo( eDetail : eTipoCatalogo );
         function GetMatrizCurso(const sMatrizCurso: string; const Empresa: OleVariant ) : OleVariant;
         function GetMatrizPuesto(const sMatrizPuesto: string; const Empresa: OleVariant ) : OleVariant;
         procedure SetTablaInfo( eCatalogo : eTipoCatalogo );

         procedure GrabaCambiosBitacora( Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind );

         {$ifndef DOS_CAPAS}
         procedure ServerActivate(Sender: TObject);
         procedure ServerDeactivate(Sender: TObject);
         {$endif}
    { Private declarations }
  protected
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
  public
        function GetMatriz(Empresa: OleVariant; lCurso: WordBool;const sCodigo: WideString): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
        function GrabaMatriz(Empresa: OleVariant; lCurso: WordBool; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
        function GrabaCatalogo(Empresa: OleVariant; iCatalogo: Integer;oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$ifndef DOS_CAPAS} safecall; {$endif}
        function ActualizaGlobal2Entrena(Empresa, Parametros: OleVariant): OleVariant;{$ifndef DOS_CAPAS} safecall; {$endif}
    { Public declarations }
  end;

var
  dmServerSegundoCriterio: TdmServerSegundoCriterio;

implementation

uses ZetaServerTools, ZetaCommonTools, ZetaCommonClasses;

{$R *.DFM}

class procedure TdmServerSegundoCriterio.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
  if Register then
  begin
    inherited UpdateRegistry(Register, ClassID, ProgID);
    EnableSocketTransport(ClassID);
    EnableWebTransport(ClassID);
  end else
  begin
    DisableSocketTransport(ClassID);
    DisableWebTransport(ClassID);
    inherited UpdateRegistry(Register, ClassID, ProgID);
  end;
end;

procedure TdmServerSegundoCriterio.dmServerSegundoCriterioCreate(Sender: TObject);
begin
     {$ifndef DOS_CAPAS}
     //OnActivate := ServerActivate;
     //OnDeactivate := ServerDeactivate;
     {$endif}
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmServerSegundoCriterio.dmServerSegundoCriterioDestroy(Sender: TObject);
begin
     dZetaServerProvider.FreeZetaProvider( oZetaProvider );
     //oZetaProvider.Free;
end;

{$ifdef DOS_CAPAS}
procedure TdmServerCatalogos.CierraEmpresa;
begin
end;
{$else}
procedure TdmServerSegundoCriterio.ServerActivate(Sender: TObject);
begin
     oZetaProvider.Activate;
end;

procedure TdmServerSegundoCriterio.ServerDeactivate(Sender: TObject);
begin
     oZetaProvider.Deactivate;
end;
{$endif}

procedure TdmServerSegundoCriterio.SetDetailInfo(eDetail: eTipoCatalogo);
begin
     with oZetaProvider.DetailInfo do
     begin
          case eDetail of
               eMatrizCurso, eMatrizPuesto :
                             SetInfoDetail( 'ENTNIVEL2', 'PU_CODIGO,CU_CODIGO,ET_CODIGO',
                                                        'PU_CODIGO,CU_CODIGO,ET_CODIGO', 'PU_CODIGO,CU_CODIGO' );                             
          end;
     end;
end;

function TdmServerSegundoCriterio.ActualizaGlobal2Entrena(Empresa, Parametros: OleVariant): OleVariant;
const
     K_ACTUALIZA_ENTRENA = 'update ENTRENA set GLOBAL2=%d, GLOBAL2DES=''%s''';
     K_ERROR = 'Error al actualizar el campo GLOBAL2 de la tabla ENTRENA';
var
   iGlobal2: integer;
   sGlobal2Des: String;
   FDataSet: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;
          AsignaParamList(Parametros);
          iGlobal2 := ParamList.ParamByName( 'GLOBAL2' ).AsInteger;
          sGlobal2Des := ParamList.ParamByName( 'GLOBAL2DES' ).AsString;

          FDataSet := CreateQuery;
          
          {Actualizar tabla}
          PreparaQuery( FDataset, Format( K_ACTUALIZA_ENTRENA, [ iGlobal2, sGlobal2Des ] ) );
          EmpiezaTransaccion;
          try
             Ejecuta( FDataset );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     Log.Excepcion( 0, K_ERROR , Error );
                end;
          end;
          Result := True;
     end;
     SetComplete;
end;


procedure TdmServerSegundoCriterio.GrabaCambiosBitacora(Sender: TObject; SourceDS: TDataSet; DeltaDS: TzProviderClientDataSet; UpdateKind: TUpdateKind);
var
   eClase: eClaseBitacora;
   sTitulo: String;

 procedure DatosBitacora( eClaseCatalogo:eClaseBitacora; sDescripcion, sKeyField : string );
  var
     oField : TField;
 begin
      eClase  := eClaseCatalogo;
      sTitulo := sDescripcion;

      if StrLleno(sKeyField) then
      begin
           oField := DeltaDS.FieldByName( sKeyField );

           if oField is TNumericField then
              sTitulo := sTitulo + ' ' + IntToStr( ZetaServerTools.CampoOldAsVar( oField ) )
           else
               sTitulo := sTitulo + ' ' + ZetaServerTools.CampoOldAsVar( oField );
      end;
 end;

begin
     if UpdateKind in [ ukModify, ukDelete ] then
     begin
          eClase := clbNinguno;

          case FTipoCatalogo of
                    eMatrizCurso : DatosBitacora( clbMatrizCurso, 'Curso: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ) +
                                                                  ' Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ), VACIO );
                    eMatrizPuesto: DatosBitacora( clbMatrizPuesto, 'Puesto: ' + ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'PU_CODIGO' ) ) +
                                                                   ' Curso: ' +  ZetaServerTools.CampoOldAsVar( DeltaDS.FieldByName( 'CU_CODIGO' ) ), VACIO );
                    else raise Exception.Create( 'No ha sido Integrado el Cat�logo al Case de <DServerCatalogos.GrabaCambiosBitacora>' )
          end;

          with oZetaProvider do
            if UpdateKind = ukModify then
                CambioCatalogo( sTitulo, eClase, DeltaDS )
            else
                BorraCatalogo( sTitulo, eClase, DeltaDS );
     end;
end;

function TdmServerSegundoCriterio.GetMatrizCurso(
  const sMatrizCurso: string; const Empresa: OleVariant): OleVariant;
begin
     SetTablaInfo( eMatrizCurso );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'PU_CODIGO' );
          TablaInfo.Filtro := Format( '( CU_CODIGO = ''%s'' )', [ sMatrizCurso ] );
          SetDetailInfo( eMatrizCurso );
          Result := GetMasterDetail( Empresa );
     end;
end;

function TdmServerSegundoCriterio.GetMatrizPuesto(
  const sMatrizPuesto: string; const Empresa: OleVariant): OleVariant;
const
     Q_GET_ENTRENA_JOIN_CURSO = 'select CURSO.CU_REVISIO, CU_CODIGO,PU_CODIGO,EN_DIAS,EN_OPCIONA,EN_LISTA,EN_LISTA2 '+
                                'from ENTRENA '+
                                'left outer join CURSO on ( CURSO.CU_CODIGO = ENTRENA.CU_CODIGO ) '+
                                'where PU_CODIGO = %s '+
                                'order by CU_CODIGO ';

begin
     SetTablaInfo( eMatrizPuesto );
     with oZetaProvider do
     begin
          TablaInfo.SetOrder( 'CU_CODIGO' );
          TablaInfo.Filtro := Format( '( PU_CODIGO = ''%s'' )', [ sMatrizPuesto ] );
          SetDetailInfo( eMatrizPuesto );
          Result := GetMasterDetail( Empresa );
     end;
end;

procedure TdmServerSegundoCriterio.SetTablaInfo(eCatalogo: eTipoCatalogo);
begin
  with oZetaProvider.TablaInfo do
     case eCatalogo of
          eMatrizPuesto: SetInfo( 'ENTRENA', 'CU_CODIGO,PU_CODIGO,EN_DIAS,EN_OPCIONA,EN_LISTA,EN_LISTA2','PU_CODIGO,CU_CODIGO' );
          eMatrizCurso : SetInfo( 'ENTRENA', 'CU_CODIGO,PU_CODIGO,EN_DIAS,EN_OPCIONA,EN_LISTA,EN_LISTA2','PU_CODIGO,CU_CODIGO' );
     end;
end;

function TdmServerSegundoCriterio.GetMatriz(Empresa: OleVariant;
  lCurso: WordBool; const sCodigo: WideString): OleVariant;
begin
     if ( lCurso ) then
        Result := GetMatrizCurso( sCodigo, Empresa )
     else
        Result := GetMatrizPuesto( sCodigo, Empresa );
     SetComplete;
end;

function TdmServerSegundoCriterio.GrabaMatriz(Empresa: OleVariant;
  lCurso: WordBool; oDelta: OleVariant;
  out ErrorCount: Integer): OleVariant;
begin
     if ( lCurso ) then
        Result := GrabaCatalogo( Empresa, Ord( eMatrizCurso ), oDelta, ErrorCount )
     else
        Result := GrabaCatalogo( Empresa, Ord( eMatrizPuesto ), oDelta, ErrorCount );
     SetComplete;
end;

function TdmServerSegundoCriterio.GrabaCatalogo(Empresa: OleVariant;
  iCatalogo: Integer; oDelta: OleVariant;
  out ErrorCount: Integer): OleVariant;
begin
     FTipoCatalogo := eTipoCatalogo( iCatalogo );

     SetTablaInfo( FTipoCatalogo );
     // Cat�logos que graban en bit�cora las modificaciones
     {CV: Este C�digo estaba hasta la version 2.3.96
     if ( FTipoCatalogo in CatalogosConBitacora ) then
     begin
        oZetaProvider.EmpresaActiva := Empresa;    // Lo requiere Bit�cora
        oZetaProvider.TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
     end;}


     {CV: A Partir de la version 2.4.97, TODOS los catalogos
     grabar�n a bitacoras sus cambios, por lo que hay que integrar el Cat�logo al CASE
     del Evento GrabaCambiosBitacora. }
     with oZetaProvider do
     begin
          EmpresaActiva := Empresa;    // Lo requiere Bit�cora
          InitGlobales;
          TablaInfo.AfterUpdateRecord := GrabaCambiosBitacora;
          SetDetailInfo( FTipoCatalogo );
          Result := GrabaMasterDetail( Empresa, oDelta, ErrorCount );
     end;
     SetComplete;
end;

{$ifndef DOS_CAPAS}

initialization
  TComponentFactory.Create(ComServer, TdmServerSegundoCriterio,
    Class_dmServerSegundoCriterio, ciMultiInstance, ZetaServerTools.GetThreadingModel );
    
{$endif}
end.