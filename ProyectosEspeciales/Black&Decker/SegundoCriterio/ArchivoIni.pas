unit ArchivoIni;

interface

uses
     SysUtils, Forms, Dialogs, IniFiles;

type
    TTArchivosIni = class( TObject )
    private
      { Private declarations }
      FiniFile: TiniFile;
      function GetSegundoCriterio: String;
      function GetSegundoCriterioDesc: String;
      procedure SetSegundoCriterio( const Value: String );
      procedure SetSegundoCriterioDesc( const Value: String );
    public
      { Public declarations }
      constructor Create;
      destructor Destroy; override;
      property SegundoCriterio: String read GetSegundoCriterio write SetSegundoCriterio;
      property SegundoCriterioDesc: String read GetSegundoCriterioDesc write SetSegundoCriterioDesc;
    end;

implementation

uses ZetaCommonClasses;
     
const
     K_CONF = 'CONFIGURACION';
     K_SEGUNDOCRITERIO = 'SEGUNDOCRITERIO';
     K_SEGUNDOCRITERIO_DESC = 'SEGUNDOCRITERIO_DESC';
     K_ARCH_CONF = 'CONFIGURACION';

{ TTArchivosIni }
constructor TTArchivosIni.Create;
begin
     FIniFile := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ) );
end;

destructor TTArchivosIni.Destroy;
begin
     FreeAndNil( FIniFile );
     inherited;
end;

function TTArchivosIni.GetSegundoCriterio: String;
begin
     Result := FIniFile.ReadString( K_CONF, K_SEGUNDOCRITERIO, VACIO );
end;

function TTArchivosIni.GetSegundoCriterioDesc: String;
begin
     Result := FIniFile.ReadString( K_CONF, K_SEGUNDOCRITERIO_DESC, VACIO );
end;

procedure TTArchivosIni.SetSegundoCriterio(const Value: String);
begin
     FIniFile.WriteString( K_CONF, K_SEGUNDOCRITERIO, Value );
end;

procedure TTArchivosIni.SetSegundoCriterioDesc(const Value: String);
begin
     FIniFile.WriteString( K_CONF, K_SEGUNDOCRITERIO_DESC, Value );
end;

end.
