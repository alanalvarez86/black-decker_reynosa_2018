inherited SegundoCriterioEdit_DevEx: TSegundoCriterioEdit_DevEx
  Left = 938
  Top = 324
  Caption = 'Segundo criterio de agrupaci'#243'n'
  ClientHeight = 122
  ClientWidth = 377
  Color = clBtnHighlight
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 86
    Width = 377
  end
  object GroupBox1: TcxGroupBox [1]
    Left = 8
    Top = 6
    Caption = 'Criterios adicionales de programaci'#243'n (segundo criterio)'
    TabOrder = 1
    Transparent = True
    Height = 71
    Width = 361
    object Label5: TLabel
      Left = 85
      Top = 32
      Width = 35
      Height = 13
      Caption = 'Cursos:'
      Transparent = True
    end
    object CriterioAdicional: TComboBox
      Left = 124
      Top = 28
      Width = 145
      Height = 21
      BevelInner = bvNone
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
