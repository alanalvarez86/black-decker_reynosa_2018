/*
 Descripci�n:
 Creaci�n y actualizaci�n de estructuras para el proyectos especial de Segundo criterio de agrupaci�n de cursos.
 
 Cliente:
 Black & Decker.
  
 Versi�n:
 2.13.114.8
 
 Fecha:
 Diciembre 2008
*/

/* Se crea una estructura semejante a la de ENTNIVEL para almacenar la lista seleccionada para el segundo criterio */
CREATE TABLE ENTNIVEL2 (
       PU_CODIGO            Codigo, /* C�digo del puesto */
       CU_CODIGO            Codigo, /* C�digo del curso */
       ET_CODIGO            Codigo /* Dellate del criterio seleccionado */
)
go

/* Llave primaria para ENTNIVEL2 */
ALTER TABLE ENTNIVEL2
       ADD CONSTRAINT PK_ENTNIVEL2 PRIMARY KEY (PU_CODIGO, CU_CODIGO, ET_CODIGO)

go

/* Se hace la relaci�n con las llaves foraneas de ENTNIVEL2 */
ALTER TABLE ENTNIVEL2
       ADD CONSTRAINT FK_EntNivel2_Entrena
       FOREIGN KEY (PU_CODIGO, CU_CODIGO)
       REFERENCES ENTRENA
       ON DELETE CASCADE
       ON UPDATE CASCADE;
go

/* Se agrega a ENTRENA 3 campos nuevos*/
ALTER TABLE ENTRENA 
	ADD
		EN_LISTA2 Booleano, /* Indicador de que se seleccion� algunos de los detalles del segundo criterio */
		GLOBAL2 FolioChico, /* Nos sirve para almacenar el segundo criterio seleccionado */
		GLOBAL2DES Descripcion /* La descripci�n del segundo criterio seleccionado */
go

/* Si existe la vista, lo elimina */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CUR_PROG]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[CUR_PROG]

go

CREATE view CUR_PROG (
  CB_CODIGO,
  CB_PUESTO,
  CU_CODIGO,
  KC_FEC_PRO,
  KC_EVALUA,
  KC_FEC_TOM,
  KC_HORAS,
  CU_HORAS,
  EN_OPCIONA,
  EN_LISTA,
  EN_LISTA2,
  MA_CODIGO,
  KC_PROXIMO,
  KC_REVISIO,
  CU_REVISIO,
  CP_MANUAL,
  EP_GLOBAL
) AS
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
 ( Select case when (KARCURSO.KC_FEC_TOM IS NULL )then DATEADD( DAY, E.EN_DIAS, C.CB_FEC_ING ) else KARCURSO.KC_FECPROG end )KC_FEC_PRO,
  KARCURSO.KC_EVALUA,
  KARCURSO.KC_FEC_TOM,
  KARCURSO.KC_HORAS,
  CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  E.EN_LISTA2,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  KARCURSO.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'N' as CHAR(1) ) CP_MANUAL,
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO on ( KARCURSO.CB_CODIGO = C.CB_CODIGO ) and ( KARCURSO.CU_CODIGO = E.CU_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = E.CU_CODIGO )
where ( CURSO.CU_ACTIVO = 'S' ) 
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CU.CU_CODIGO,
  ( select case when K.KC_FEC_TOM is null then NULL
		        when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'N' ) OR ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' )and ( K.KC_REVISIO = CU.CU_REVISIO ) ) )then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				when ( ( E.EN_RE_DIAS > 0 AND E.EN_REPROG = 'S' ) and ( K.KC_FEC_TOM > ( select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) ) ) and (K.KC_REVISIO <> CU.CU_REVISIO ))then DATEADD( DAY, E.EN_RE_DIAS, K.KC_FEC_TOM )
				else (select max(CH_FECHA) from CUR_REV where( ( CUR_REV.CU_CODIGO = CU.CU_CODIGO ) AND ( CUR_REV.CH_REVISIO = CU.CU_REVISIO ) ) )End ) KC_FEC_PRO,
  cast(0 as Decimal(15,2)) KC_EVALUA,
  cast('12/30/1899' as DateTime) KC_FEC_TOM,
  cast(0 as Decimal(15,2)) KC_HORAS,
  CU.CU_HORAS,
  E.EN_OPCIONA,
  E.EN_LISTA,
  E.EN_LISTA2,
  CU.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM  dbo.SESION WHERE (CU_CODIGO = E.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS DateTime))) KC_PROXIMO,
  cast(' ' as varchar(10))KC_REVISIO,               
  CU.CU_REVISIO,                  
  cast( 'N' as CHAR(1) ) CP_MANUAL,  
  cast( 'N' as CHAR(1) ) EP_GLOBAL
  from COLABORA C
  join ENTRENA E on ( E.PU_CODIGO = C.CB_PUESTO )
  left join KARCURSO K on ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CU_CODIGO = E.CU_CODIGO ) and ( K.KC_FEC_TOM = ( select MAX( KC_FEC_TOM ) from KARCURSO AS K1 where ( K1.CB_CODIGO = C.CB_CODIGO ) and ( K1.CU_CODIGO = E.CU_CODIGO ) ))
  left join CURSO CU on ( CU.CU_CODIGO = E.CU_CODIGO )
where ( Cu.CU_ACTIVO = 'S' ) and 
		( ( E.EN_RE_DIAS > 0 ) AND NOT ( K.KC_FEC_TOM = '12/30/1899') or
        ( ( E.EN_REPROG = 'S' ) and ( K.KC_REVISIO <> CU.CU_REVISIO ) and ( 
          ( select count(k2.KC_REVISIO) FROM karcurso k2 where k2.KC_REVISIO = CU.CU_REVISIO AND ( k2.CB_CODIGO = C.CB_CODIGO ) and ( k2.CU_CODIGO = E.CU_CODIGO ) ) = 0 ) ))
UNION ALL
select
  C.CB_CODIGO,
  C.CB_PUESTO,
  CURSO.CU_CODIGO,
  (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_ING + EP.EP_DIAS else EP.EP_FECHA end	)KC_FEC_PRO,
  K.KC_EVALUA,
  K.KC_FEC_TOM,
  K.KC_HORAS,
  CURSO.CU_HORAS,
  EP.EP_OPCIONA,
  cast( 'N' as CHAR(1) ) EN_LISTA, cast( 'N' as CHAR(1) ) EN_LISTA2,
  CURSO.MA_CODIGO,
  (SELECT MIN(SE_FEC_INI) AS Expr1 FROM dbo.SESION WHERE (CU_CODIGO = EP.CU_CODIGO) AND (SE_FEC_INI >= CAST(FLOOR(CAST(GETDATE() AS float)) AS Datetime))) AS KC_PROXIMO, 
  K.KC_REVISIO,
  CURSO.CU_REVISIO,
  cast( 'S' as CHAR(1) ) CP_MANUAL,	
  EP.EP_GLOBAL
  from COLABORA C
  join EMP_PROG EP on ( EP.CB_CODIGO = C.CB_CODIGO )
  left join CURSO on ( CURSO.CU_CODIGO = EP.CU_CODIGO )
  left join KARCURSO K on ( K.CU_CODIGO = EP.CU_CODIGO and EP.CB_CODIGO = K.CB_CODIGO and K.KC_FEC_TOM >= (select case when (EP.EP_PORDIAS = 'S' ) then C.CB_FEC_ING + EP.EP_DIAS else EP.EP_FECHA end	) )
where ( CURSO.CU_ACTIVO = 'S' )

go

/* Si existe el procedimiento, lo elimina */
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Get_CursosProgramados]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Get_CursosProgramados]

go

/* Se modifica el procedimiento para contemplar la nueva estructura: ENTNIVEL2*/
CREATE procedure Get_CursosProgramados 
@CB_CODIGO int
as

SELECT CP.CU_CODIGO, CP.KC_FEC_PRO, CP.KC_EVALUA, 
CP.KC_FEC_TOM, CP.KC_REVISIO, CP.KC_HORAS, 
CP.CU_HORAS, CP.EN_OPCIONA, CP.KC_PROXIMO, C.CU_NOMBRE
from CUR_PROG CP
left outer join CURSO C 
	on( CP.CU_CODIGO = C.CU_CODIGO ) 
left outer join COLABORA 
	on( CP.CB_CODIGO = COLABORA.CB_CODIGO ) 
where( CP.CB_CODIGO = @CB_CODIGO ) 
	and ((CP.EN_LISTA = 'N') 
	or (COLABORA.CB_CLASIFI in (select ET_CODIGO from ENTNIVEL 
					where (ENTNIVEL.PU_CODIGO = CP.CB_PUESTO) 
					and (ENTNIVEL.CU_CODIGO = CP.CU_CODIGO))))
	and ((CP.EN_LISTA2 = 'N') 
	or (COLABORA.CB_CLASIFI in (select ET_CODIGO from ENTNIVEL2 
					where (ENTNIVEL2.PU_CODIGO = CP.CB_PUESTO) 
					and (ENTNIVEL2.CU_CODIGO = CP.CU_CODIGO))))
	
					order by KC_FEC_PRO 