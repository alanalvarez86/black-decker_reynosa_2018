inherited CatMatrizPuesto: TCatMatrizPuesto
  Left = 272
  Top = 217
  Caption = 'Matriz de entrenamiento por puesto'
  ClientWidth = 528
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 528
    TabOrder = 2
    inherited Slider: TSplitter
      Left = 378
    end
    inherited ValorActivo1: TPanel
      Width = 362
    end
    inherited ValorActivo2: TPanel
      Left = 381
      Width = 147
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 65
    Width = 528
    Height = 208
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CU_CODIGO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CU_NOMBRE'
        Title.Caption = 'Curso'
        Width = 263
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CU_REVISIO'
        ReadOnly = True
        Title.Caption = 'Revisi'#243'n'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_DIAS'
        Title.Alignment = taRightJustify
        Title.Caption = 'D'#237'as de antig'#252'edad'
        Width = 104
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_OPCIONA'
        Title.Caption = 'Opcional'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_LISTA'
        Title.Caption = 'Lista'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_LISTA2'
        Title.Caption = 'Lista 2'
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 528
    Height = 46
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 37
      Top = 16
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object LookUpPuesto: TZetaKeyLookup
      Left = 80
      Top = 12
      Width = 300
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = LookUpPuestoValidKey
    end
  end
end
