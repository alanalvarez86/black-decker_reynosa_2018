inherited EditMatriz: TEditMatriz
  Left = 423
  Top = 202
  Caption = 'Matriz de entrenamiento'
  ClientHeight = 285
  ClientWidth = 445
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 40
    Top = 51
    Width = 30
    Height = 13
    Caption = 'Curso:'
  end
  object Label2: TLabel [1]
    Left = 34
    Top = 73
    Width = 36
    Height = 13
    Caption = 'Puesto:'
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 97
    Width = 57
    Height = 13
    Caption = 'Antig'#252'edad:'
  end
  object ZCurso: TZetaTextBox [3]
    Left = 80
    Top = 49
    Width = 273
    Height = 17
    AutoSize = False
    Caption = 'ZCurso'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label4: TLabel [4]
    Left = 152
    Top = 97
    Width = 23
    Height = 13
    Caption = 'D'#237'as'
  end
  inherited PanelBotones: TPanel
    Top = 249
    Width = 445
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 277
    end
    inherited Cancelar: TBitBtn
      Left = 362
    end
  end
  inherited PanelSuperior: TPanel
    Width = 445
    TabOrder = 5
  end
  inherited PanelIdentifica: TPanel
    Width = 445
    TabOrder = 6
    inherited ValorActivo2: TPanel
      Width = 119
    end
  end
  object PU_CODIGO: TZetaDBKeyLookup [8]
    Left = 80
    Top = 69
    Width = 300
    Height = 21
    LookupDataset = dmCatalogos.cdsPuestos
    Opcional = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'PU_CODIGO'
    DataSource = DataSource
  end
  object EN_OPCIONA: TDBCheckBox [9]
    Left = 23
    Top = 116
    Width = 70
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Opcional:'
    DataField = 'EN_OPCIONA'
    DataSource = DataSource
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object EN_DIAS: TZetaDBNumero [10]
    Left = 80
    Top = 93
    Width = 60
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
    DataField = 'EN_DIAS'
    DataSource = DataSource
  end
  object gbProgramacion: TGroupBox [11]
    Left = 80
    Top = 136
    Width = 265
    Height = 105
    TabOrder = 3
    object bModifica: TBitBtn
      Left = 136
      Top = 48
      Width = 105
      Height = 25
      Caption = '&Modificar Lista'
      TabOrder = 1
      OnClick = bModificaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
        333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
        300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
        333337F373F773333333303330033333333337F3377333333333303333333333
        333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
        333337777F337F33333330330BB00333333337F373F773333333303330033333
        333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
        333377777F77377733330BBB0333333333337F337F33333333330BB003333333
        333373F773333333333330033333333333333773333333333333}
      NumGlyphs = 2
    end
    object gbProgramarA: TDBRadioGroup
      Left = 16
      Top = 24
      Width = 97
      Height = 57
      Caption = 'Programar a:'
      DataField = 'EN_LISTA2'
      DataSource = DataSource
      Items.Strings = (
        'Todos'
        'Algunos')
      TabOrder = 0
      Values.Strings = (
        'N'
        'S')
      OnChange = gbProgramarAChange
    end
  end
  inherited DataSource: TDataSource
    Left = 164
    Top = 65534
  end
end
