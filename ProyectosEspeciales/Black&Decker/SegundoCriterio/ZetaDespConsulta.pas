unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcSistGlobales );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FReportes,
     FQueryGral;


function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     Result := Consulta( 0, nil );
     
end;

end.
