unit FCatEntNivel_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, Db, DBClient,
  ZetaClientDataSet, CheckLst, ArchivoIni,ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, ImgList, cxButtons;

type
  TCatEntNivel_DevEx = class(TZetaDlgModal_DevEx)
    Lista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FIniValues: TTArchivosIni;
    function  GetCriterioAdicional: TZetaLookupDataSet;
    function  YaExistia( const sCodigo : String ) : Boolean;
    procedure Inicializa;
    procedure GrabaSeleccionados;
  public
    { Public declarations }
  end;

var
  CatEntNivel_DevEx: TCatEntNivel_DevEx;
  sCodigo : String;

implementation

uses dCatalogos, dTablas, dGlobal, ZGlobalTress, dSegundoCriterio;

{$R *.DFM}

procedure TCatEntNivel_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Caption := 'Seleccione ' + FIniValues.SegundoCriterioDesc;
     Inicializa;
end;

procedure TCatEntNivel_DevEx.Inicializa;
var
   oDataSet : TZetaLookupDataset;
   iPosicion : integer;
begin
     iPosicion := 0;
     oDataSet := GetCriterioAdicional;
     if Assigned( oDataSet ) then
        with oDataSet do
        begin
             Conectar;
             First;
             Lista.Clear;
             while not Eof do
             begin
                  Lista.Items.Add( FieldByName( LookupKeyField ).AsString + '=' +
                                   FieldByName( LookupDescriptionField ).AsString );
                  sCodigo := FieldByName(LookupKeyField).AsString;
                  if YaExistia( sCodigo ) then
                     Lista.Checked[iPosicion] := TRUE;
                  iPosicion := iPosicion + 1;
                  Next;
             end;
        end;
end;


function TCatEntNivel_DevEx.GetCriterioAdicional: TZetaLookupDataSet;
begin
     Result := NIL;
     case StrToIntDef( FIniValues.SegundoCriterio, 0 )of
       1 : Result := dmCatalogos.cdsClasifi;
       2 : Result := dmCatalogos.cdsTurnos;
       3 : Result := dmTablas.cdsNivel1;
       4 : Result := dmTablas.cdsNivel2;
       5 : Result := dmTablas.cdsNivel3;
       6 : Result := dmTablas.cdsNivel4;
       7 : Result := dmTablas.cdsNivel5;
       8 : Result := dmTablas.cdsNivel6;
       9 : Result := dmTablas.cdsNivel7;
       10: Result := dmTablas.cdsNivel8;
       11: Result := dmTablas.cdsNivel9;
     end;
end;

procedure TCatEntNivel_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     GrabaSeleccionados;
end;

procedure TCatEntNivel_DevEx.GrabaSeleccionados;
var
   i : Integer;
   sCodigo : String;
begin
     with dmSegundoCriterio.cdsEntNivel2, Lista do
     begin
        for i := 0 to Items.Count-1 do
        begin
             sCodigo := Items.Names[ i ];
             if Checked[ i ] then                     // Si est� seleccionado
             begin
                  if not YaExistia( sCodigo ) then    // Y no existia, lo tengo que agregar
                  begin
                       Append;
                       FieldByName( 'ET_CODIGO' ).AsString := sCodigo;
                       Post;
                  end;
             end
             else                                     // No est� seleccionado, Si ya exist�a, lo tengo que borrar
                  if YaExistia( sCodigo ) then
                     Delete;
        end;
     end;
end;

function TCatEntNivel_DevEx.YaExistia(const sCodigo: String): Boolean;
begin
     Result := dmSegundoCriterio.cdsEntNivel2.Locate( 'ET_CODIGO', sCodigo, [loPartialKey] );
end;

procedure TCatEntNivel_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FIniValues := TTArchivosIni.Create;
end;

procedure TCatEntNivel_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FIniValues );
end;

end.
