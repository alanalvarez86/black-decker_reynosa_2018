unit FCatMatrizPuesto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZetaKeyLookup;

type
  TCatMatrizPuesto = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    LookUpPuesto: TZetaKeyLookup;
    procedure LookUpPuestoValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizPuesto: TCatMatrizPuesto;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses,
  dSegundoCriterio;

{$R *.DFM}

{ TCatMatrizPuesto }

procedure TCatMatrizPuesto.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60644_Matriz_puesto;
end;

procedure TCatMatrizPuesto.DisConnect;
begin
     inherited;
     LookUpPuesto.LookUpDataset := NIL;
end;

procedure TCatMatrizPuesto.Connect;
begin
     LookUpPuesto.LookUpDataset :=  dmCatalogos.cdsPuestos;
     with dmCatalogos do
     begin
          with cdsPuestos do
          begin
               Conectar;
               //First;
               LookUpPuesto.Llave := FieldByName('PU_CODIGO').AsString;
          end;
          dmSegundoCriterio.SetMatriz( FALSE, LookUpPuesto.Llave );
          cdsCursos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          DataSource.DataSet:= dmSegundoCriterio.cdsMatrizCurso;
          Refresh;
     end;
end;

procedure TCatMatrizPuesto.Refresh;
begin
     dmSegundoCriterio.cdsMatrizCurso.Refrescar;
     ZetaDBGrid1.Columns[ 0 ].FieldName := 'CU_CODIGO';
     ZetaDBGrid1.Columns[ 2 ].FieldName := 'CU_REVISIO';
     ZetaDBGrid1.Columns[ 3 ].FieldName := 'EN_DIAS';
     ZetaDBGrid1.Columns[ 4 ].FieldName := 'EN_OPCIONA';
     ZetaDBGrid1.Columns[ 5 ].FieldName := 'EN_LISTA';
     ZetaDBGrid1.Columns[ 6 ].FieldName := 'EN_LISTA2';
end;

procedure TCatMatrizPuesto.LookUpPuestoValidKey(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TCatMatrizPuesto.Agregar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Agregar;
end;

procedure TCatMatrizPuesto.Borrar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Borrar;
end;

procedure TCatMatrizPuesto.Modificar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Modificar;
end;

function TCatMatrizPuesto.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizPuesto.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TCatMatrizPuesto.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

end.
