program SegundoCriterio;

uses
  Forms,
  ZetaSplash,
  ZetaClientTools,
  ZetaCommonClasses,
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseCliente in '..\..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule},
  DCliente in '..\..\..\DataModules\DCliente.pas' {dmCliente: TDataModule},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  ZBaseConsulta in '..\..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseEdicion in '..\..\..\tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseEdicionRenglon in '..\..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZBaseWizard in '..\..\..\Tools\ZBaseWizard.pas' {BaseWizard},
  ZBaseWizardFiltro in '..\..\..\Tools\ZBaseWizardFiltro.pas' {BaseWizardFiltro},
  FWizNomBase_DevEx in '..\..\..\Wizards\FWizNomBase_DevEx.pas' {WizNomBase},
  ZWizardBasico in '..\..\..\Tools\ZWizardBasico.pas' {WizardBasico},
  ArchivoIni in 'ArchivoIni.pas',
  dSegundoCriterio in 'DataModule\dSegundoCriterio.pas' {dmSegundoCriterio: TDataModule},
  DCatalogos in '..\..\..\DataModules\dCatalogos.pas' {dmCatalogos: TDataModule};

{$R *.RES}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Segundo criterio de agrupación para cursos';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( FALSE ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            {
            OP
            if ( ConfiguracionDatos.IniValues.Configura = VACIO ) then
               ConfiguracionDatos.ShowModal;
            }
            Application.Run;
       end
       else
       begin
            CierraSplash;
            Free;
       end;
  end;
end.
