inherited EditMatrizPuesto: TEditMatrizPuesto
  Left = 197
  Top = 164
  Caption = 'Matriz por puesto'
  ClientHeight = 283
  ClientWidth = 433
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 40
    Top = 65
    Width = 30
    Height = 13
    Caption = 'Curso:'
  end
  object Label2: TLabel [1]
    Left = 34
    Top = 43
    Width = 36
    Height = 13
    Caption = 'Puesto:'
  end
  object Label3: TLabel [2]
    Left = 13
    Top = 89
    Width = 57
    Height = 13
    Caption = 'Antig'#252'edad:'
  end
  object ZPuesto: TZetaTextBox [3]
    Left = 80
    Top = 41
    Width = 307
    Height = 17
    AutoSize = False
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Label4: TLabel [4]
    Left = 152
    Top = 89
    Width = 23
    Height = 13
    Caption = 'D'#237'as'
  end
  inherited PanelBotones: TPanel
    Top = 247
    Width = 433
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 262
    end
    inherited Cancelar: TBitBtn
      Left = 347
    end
  end
  inherited PanelSuperior: TPanel
    Width = 433
    TabOrder = 5
  end
  inherited PanelIdentifica: TPanel
    Width = 433
    TabOrder = 6
    inherited ValorActivo2: TPanel
      Width = 107
    end
  end
  object EN_OPCIONA: TDBCheckBox [8]
    Left = 23
    Top = 109
    Width = 70
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Opcional:'
    DataField = 'EN_OPCIONA'
    DataSource = DataSource
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object EN_DIAS: TZetaDBNumero [9]
    Left = 80
    Top = 85
    Width = 68
    Height = 21
    Mascara = mnDias
    TabOrder = 1
    Text = '0'
    DataField = 'EN_DIAS'
    DataSource = DataSource
  end
  object CU_CODIGO: TZetaDBKeyLookup [10]
    Left = 80
    Top = 61
    Width = 332
    Height = 21
    LookupDataset = dmCatalogos.cdsCursos
    Opcional = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'CU_CODIGO'
    DataSource = DataSource
  end
  object gbProgramacion: TGroupBox [11]
    Left = 80
    Top = 127
    Width = 299
    Height = 105
    TabOrder = 3
    object bModifica: TBitBtn
      Left = 158
      Top = 48
      Width = 105
      Height = 25
      Caption = '&Modificar Lista'
      TabOrder = 1
      OnClick = bModificaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
        333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
        300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
        333337F373F773333333303330033333333337F3377333333333303333333333
        333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
        333337777F337F33333330330BB00333333337F373F773333333303330033333
        333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
        333377777F77377733330BBB0333333333337F337F33333333330BB003333333
        333373F773333333333330033333333333333773333333333333}
      NumGlyphs = 2
    end
    object gbProgramarA: TDBRadioGroup
      Left = 38
      Top = 24
      Width = 97
      Height = 57
      Caption = 'Programar a:'
      DataField = 'EN_LISTA2'
      DataSource = DataSource
      Items.Strings = (
        'Todos'
        'Algunos')
      TabOrder = 0
      Values.Strings = (
        'N'
        'S')
      OnChange = gbProgramarAChange
    end
  end
  inherited DataSource: TDataSource
    Left = 332
    Top = 1
  end
end
