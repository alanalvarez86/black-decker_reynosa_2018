unit FCatMatrizPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZetaKeyLookup,ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxTextEdit,
  ZetaKeyLookup_DevEx;

type
  TCatMatrizPuesto_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpPuesto: TZetaKeyLookup_DevEx;
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1CU_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableView1CU_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1CU_REVISIO: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_DIAS: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_OPCIONA: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_LISTA: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_LISTA2: TcxGridDBColumn;
    ZetaDBGridDBTableViewCU_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableViewCU_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableViewCU_REVISIO: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_DIAS: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_OPCIONA: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_LISTA: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_LISTA2: TcxGridDBColumn;
    procedure LookUpPuestoValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizPuesto_DevEx: TCatMatrizPuesto_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses,
  dSegundoCriterio;

{$R *.DFM}

{ TCatMatrizPuesto }

procedure TCatMatrizPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60644_Matriz_puesto;
end;

procedure TCatMatrizPuesto_DevEx.DisConnect;
begin
     inherited;
     LookUpPuesto.LookUpDataset := NIL;
end;

procedure TCatMatrizPuesto_DevEx.Connect;
begin
     LookUpPuesto.LookUpDataset :=  dmCatalogos.cdsPuestos;
     with dmCatalogos do
     begin
          with cdsPuestos do
          begin
               Conectar;
               //First;
               LookUpPuesto.Llave := FieldByName('PU_CODIGO').AsString;
          end;
          dmSegundoCriterio.SetMatriz( FALSE, LookUpPuesto.Llave );
          cdsCursos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          DataSource.DataSet:= dmSegundoCriterio.cdsMatrizCurso;
          Refresh;
     end;
end;

procedure TCatMatrizPuesto_DevEx.Refresh;
begin
     dmSegundoCriterio.cdsMatrizCurso.Refrescar;
    { ZetaDBGrid1.Columns[ 0 ].FieldName := 'CU_CODIGO';
     ZetaDBGrid1.Columns[ 2 ].FieldName := 'CU_REVISIO';
     ZetaDBGrid1.Columns[ 3 ].FieldName := 'EN_DIAS';
     ZetaDBGrid1.Columns[ 4 ].FieldName := 'EN_OPCIONA';
     ZetaDBGrid1.Columns[ 5 ].FieldName := 'EN_LISTA';
     ZetaDBGrid1.Columns[ 6 ].FieldName := 'EN_LISTA2';}
end;

procedure TCatMatrizPuesto_DevEx.LookUpPuestoValidKey(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TCatMatrizPuesto_DevEx.Agregar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Agregar;
end;

procedure TCatMatrizPuesto_DevEx.Borrar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Borrar;
end;

procedure TCatMatrizPuesto_DevEx.Modificar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Modificar;
end;

function TCatMatrizPuesto_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizPuesto_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TCatMatrizPuesto_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

procedure TCatMatrizPuesto_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

end;

end.
