unit dSegundoCriterio;

interface

uses
  SysUtils, Classes, DB, DBClient, ZetaClientDataSet,Catalogos_TLB,
     Labor_TLB, SegundoCriterio_TLB, ZetaCommonClasses;

type
  TdmSegundoCriterio = class(TDataModule)
    cdsEntNivel2: TZetaClientDataSet;
    cdsMatrizCurso: TZetaClientDataSet;
    procedure cdsEntNivel2AfterDelete(DataSet: TDataSet);
    procedure cdsEntNivel2NewRecord(DataSet: TDataSet);
    procedure cdsReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsMatrizCursoAfterCancel(DataSet: TDataSet);
    procedure cdsMatrizCursoAfterDelete(DataSet: TDataSet);
    procedure cdsMatrizCursoAfterOpen(DataSet: TDataSet);
    procedure cdsMatrizCursoAlAdquirirDatos(Sender: TObject);
    procedure cdsMatrizCursoAlCrearCampos(Sender: TObject);
    procedure cdsMatrizCursoAlEnviarDatos(Sender: TObject);
    procedure cdsMatrizCursoAlModificar(Sender: TObject);
    procedure cdsMatrizCursoBeforePost(DataSet: TDataSet);
    procedure cdsMatrizCursoNewRecord(DataSet: TDataSet);
    procedure cdsMatrizCursoPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
  private
         FMatrizPorCurso: Boolean;
         FServidor: IdmServerCatalogosDisp;
         FServidorSC: IdmServerSegundoCriterioDisp;
         FParametros: TZetaParams;

         function GetServerCatalogos: IdmServerCatalogosDisp;
         property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;

         function GetServerSC: IdmServerSegundoCriterioDisp;
         property ServerSC: IdmServerSegundoCriterioDisp read GetServerSC;

         procedure cdsBeforePost(DataSet: TDataSet; const sCampo: String);
    { Private declarations }
  public
        property MatrizPorCurso: Boolean read FMatrizPorCurso;
        procedure SetMatriz( const lCurso: Boolean; const sLlave: String );
        procedure ActualizaEntrena( iGlobal2: Integer; sGlobal2Des: String );
        procedure GuardaEntrenaPuestosProg( const sPuesto, sCurso : string ;const MatrizCursos:Boolean );
    { Public declarations }
  end;

var
  dmSegundoCriterio: TdmSegundoCriterio;

implementation

uses
    dCatalogos, dCliente, dRecursos, ZReconcile, FEditMatrizPuesto, FEditMatriz,
    ZBaseEdicion, ZetaDialogo, ZetaCommonTools;
{$R *.dfm}

procedure TdmSegundoCriterio.cdsBeforePost( DataSet: TDataSet; const sCampo: String );
begin
     with Dataset do
     begin
          if ZetaCommonTools.StrVacio( FieldByName( sCampo ).AsString ) then
             DataBaseError( 'El C�digo No Puede Quedar Vac�o' );
     end;
end;

function TdmSegundoCriterio.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServidor ) );
end;

function TdmSegundoCriterio.GetServerSC: IdmServerSegundoCriterioDisp;
begin
     Result := IdmServerSegundoCriterioDisp( dmCliente.CreaServidor( CLASS_dmServerSegundoCriterio, FServidorSC ) );
end;

procedure TdmSegundoCriterio.cdsEntNivel2AfterDelete(DataSet: TDataSet);
begin
     with cdsMatrizCurso do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmSegundoCriterio.cdsEntNivel2NewRecord(DataSet: TDataSet);
begin
     with cdsEntNivel2 do
     begin
          FieldByName( 'CU_CODIGO' ).AsString := cdsMatrizCurso.FieldByName( 'CU_CODIGO' ).AsString;
          FieldByName( 'PU_CODIGO' ).AsString := cdsMatrizCurso.FieldByName( 'PU_CODIGO' ).AsString;
     end;
end;

procedure TdmSegundoCriterio.cdsReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAfterCancel(DataSet: TDataSet);
begin
     dmSegundoCriterio.cdsEntNivel2.CancelUpdates;
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAfterDelete(DataSet: TDataSet);
begin
     cdsMatrizCurso.Enviar;
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAfterOpen(DataSet: TDataSet);
begin
     cdsEntNivel2.DataSetField := TDataSetField( cdsMatrizCurso.FieldByName( 'qryDetail' ) );
end;

procedure TdmSegundoCriterio.SetMatriz(const lCurso: Boolean; const sLlave: String);
begin
     FMatrizPorCurso := lCurso;
     {
     FMatrizCodigo := sLlave;
     }
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAlAdquirirDatos(
  Sender: TObject);
var
   sCodigo: String;
begin
     dmCatalogos.cdsCursos.Conectar;
     if ( FMatrizPorCurso ) then
        sCodigo := dmCatalogos.cdsCursos.FieldByName( 'CU_CODIGO' ).AsString
     else
         sCodigo := dmCatalogos.cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
     cdsMatrizCurso.Data := ServerSC.GetMatriz( dmCliente.Empresa, FMatrizPorCurso, sCodigo );
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAlCrearCampos(Sender: TObject);
begin
     with cdsMatrizCurso do
     begin
          CreateSimpleLookup( dmCatalogos.cdsPuestos, 'PU_NOMBRE', 'PU_CODIGO' );
          CreateSimpleLookup( dmCatalogos.cdsCursos, 'CU_NOMBRE', 'CU_CODIGO' );
          CreateLookup( dmCatalogos.cdsCursos, 'CU_REVISIO', 'CU_CODIGO', 'CU_CODIGO', 'CU_REVISIO' );
     end;
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsMatrizCurso do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerSC.GrabaMatriz( dmCliente.Empresa, FMatrizPorCurso, Delta, ErrorCount ) );
          end;
     end;
     dmRecursos.cdsHisCursosProg.Active := False; // para que se refresqueend;
end;

procedure TdmSegundoCriterio.cdsMatrizCursoAlModificar(Sender: TObject);
begin
     if FMatrizPorCurso then
        ZBaseEdicion.ShowFormaEdicion( EditMatriz, TEditMatriz )
     else
         ZBaseEdicion.ShowFormaEdicion( EditMatrizPuesto, TEditMatrizPuesto );
end;

procedure TdmSegundoCriterio.cdsMatrizCursoBeforePost(DataSet: TDataSet);
begin
     cdsBeforePost( Dataset, 'PU_CODIGO' );
     cdsBeforePost( DataSet, 'CU_CODIGO' );
end;

procedure TdmSegundoCriterio.cdsMatrizCursoNewRecord(DataSet: TDataSet);
begin
     with Dataset do
     begin
          if FMatrizPorCurso then
             FieldByName( 'CU_CODIGO' ).AsString := dmCatalogos.cdsCursos.FieldByName( 'CU_CODIGO' ).AsString
          else
              FieldByName( 'PU_CODIGO' ).AsString := dmCatalogos.cdsPuestos.FieldByName( 'PU_CODIGO' ).AsString;
          FieldByName( 'EN_OPCIONA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'EN_LISTA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'EN_LISTA2' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmSegundoCriterio.cdsMatrizCursoPostError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
     ZetaDialogo.ZError('Error', GetDBErrorDescription( E ), 0 );
     Action := daAbort;
end;

procedure TdmSegundoCriterio.ActualizaEntrena( iGlobal2: Integer; sGlobal2Des: String );
begin
     FParametros := TZetaParams.Create;
     FParametros.AddInteger( 'GLOBAL2', iGlobal2 );
     FParametros.AddString( 'GLOBAL2DES', sGlobal2Des );
     ServerSC.ActualizaGlobal2Entrena( dmCliente.Empresa, FParametros.VarValues );
end;

procedure TdmSegundoCriterio.GuardaEntrenaPuestosProg(const sPuesto, sCurso: string; const MatrizCursos:Boolean );
begin
     with cdsMatrizCurso do
     begin
          Append;
          FieldByName( 'CU_CODIGO' ).AsString := sCurso;
          FieldByName( 'PU_CODIGO' ).AsString := sPuesto;
          FieldByName( 'EN_DIAS' ).AsInteger := 0;
          FieldByName( 'EN_OPCIONA' ).AsString := K_GLOBAL_NO;
          Post;
     end;
end;

end.
