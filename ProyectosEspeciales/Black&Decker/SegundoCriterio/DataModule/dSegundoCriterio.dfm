object dmSegundoCriterio: TdmSegundoCriterio
  OldCreateOrder = False
  Left = 359
  Top = 214
  Height = 127
  Width = 241
  object cdsEntNivel2: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'ET_CODIGO'
    Params = <>
    AfterDelete = cdsEntNivel2AfterDelete
    OnNewRecord = cdsEntNivel2NewRecord
    OnReconcileError = cdsReconcileError
    Left = 48
    Top = 8
  end
  object cdsMatrizCurso: TZetaClientDataSet
    Tag = 14
    Aggregates = <>
    IndexFieldNames = 'CU_CODIGO'
    Params = <>
    AfterOpen = cdsMatrizCursoAfterOpen
    BeforePost = cdsMatrizCursoBeforePost
    AfterCancel = cdsMatrizCursoAfterCancel
    AfterDelete = cdsMatrizCursoAfterDelete
    OnNewRecord = cdsMatrizCursoNewRecord
    OnPostError = cdsMatrizCursoPostError
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsMatrizCursoAlAdquirirDatos
    AlEnviarDatos = cdsMatrizCursoAlEnviarDatos
    AlCrearCampos = cdsMatrizCursoAlCrearCampos
    AlModificar = cdsMatrizCursoAlModificar
    Left = 136
    Top = 8
  end
end
