unit FCatMatrizCurso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZetaKeyLookup, Buttons;

type
  TCatMatrizCurso = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    LookUpCursos: TZetaKeyLookup;
    BAgregaPuestos: TSpeedButton;
    procedure LookUpCursosValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BAgregaPuestosClick(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizCurso: TCatMatrizCurso;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses, FListaPuestos,
  dSegundoCriterio;

{$R *.DFM}

{ TCatMatrizCurso }

procedure TCatMatrizCurso.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60643_Matriz_curso;
end;

procedure TCatMatrizCurso.DisConnect;
begin
     LookUpCursos.LookUpDataSet := NIL;
end;

procedure TCatMatrizCurso.Connect;
begin
     LookUpCursos.LookUpDataSet := dmCatalogos.cdsCursos;
     with dmCatalogos do
     begin
          with cdsCursos do
          begin
               Conectar;
               LookUpCursos.Llave := FieldByName('CU_CODIGO').AsString;
          end;
          dmSegundoCriterio.SetMatriz( TRUE, LookUpCursos.Llave );
          cdsPuestos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          DataSource.DataSet:= dmSegundoCriterio.cdsMatrizCurso;
          Refresh;
     end;
end;

procedure TCatMatrizCurso.Refresh;
begin
     dmSegundoCriterio.cdsMatrizCurso.Refrescar;
     // No s� por qu�, pero se pierde al activar la otra Matriz de Cursos
     ZetaDBGrid1.Columns[ 0 ].FieldName := 'PU_CODIGO';
     ZetaDBGrid1.Columns[ 2 ].FieldName := 'EN_DIAS';
     ZetaDBGrid1.Columns[ 3 ].FieldName := 'EN_OPCIONA';
     ZetaDBGrid1.Columns[ 4 ].FieldName := 'EN_LISTA';
     ZetaDBGrid1.Columns[ 5 ].FieldName := 'EN_LISTA2';
end;

procedure TCatMatrizCurso.LookUpCursosValidKey(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TCatMatrizCurso.Agregar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Agregar;
end;

procedure TCatMatrizCurso.Borrar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Borrar;
end;

procedure TCatMatrizCurso.Modificar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Modificar;
end;

function TCatMatrizCurso.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizCurso.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
end;

function TCatMatrizCurso.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
end;

procedure TCatMatrizCurso.BAgregaPuestosClick(Sender: TObject);
begin
     inherited;
     if SetListaPuestos( LookUpCursos.Llave,True ) then
        DoRefresh;
end;

end.
