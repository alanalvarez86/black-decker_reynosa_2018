inherited CatMatrizCurso: TCatMatrizCurso
  Left = 319
  Top = 233
  Caption = 'Matriz de entrenamiento por curso'
  ClientWidth = 525
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 525
    TabOrder = 2
    inherited Slider: TSplitter
      Left = 366
    end
    inherited ValorActivo1: TPanel
      Width = 350
    end
    inherited ValorActivo2: TPanel
      Left = 369
      Width = 156
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 65
    Width = 525
    Height = 208
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'PU_CODIGO'
        Title.Caption = 'C'#243'digo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PU_NOMBRE'
        Title.Caption = 'Puesto'
        Width = 289
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_DIAS'
        Title.Alignment = taRightJustify
        Title.Caption = 'D'#237'as de antig'#252'edad'
        Width = 101
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_OPCIONA'
        Title.Caption = 'Opcional'
        Width = 49
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_LISTA'
        Title.Caption = 'Lista'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_LISTA2'
        Title.Caption = 'Lista 2'
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 525
    Height = 46
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 16
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Curso:'
    end
    object BAgregaPuestos: TSpeedButton
      Left = 347
      Top = 3
      Width = 85
      Height = 41
      Hint = 'Agregar Lista de Puestos'
      Caption = 'Agregar &Puestos'
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888800008888888888888888888800008888777777778888888800008800
        00000000788888880000880BFFFBFFF0777777880000880F444444F000000078
        0000880FFBFFFBF0FBFFF0780000880F444444F04444F0780000880BFFFBFFF0
        FFFBF0780000880F444444F04444F0780000880FFBFFFBF0FBFFF0780000880F
        44F000004477F0780000880BFFF0FFF0FF0007780000880F44F0FB00F70A0778
        0000880FFBF0F0FF000A00080000880000000F470AAAAA080000888888880FFB
        000A00080000888888880000770A088800008888888888888800088800008888
        88888888888888880000}
      Layout = blGlyphTop
      Spacing = 0
      OnClick = BAgregaPuestosClick
    end
    object LookUpCursos: TZetaKeyLookup
      Left = 39
      Top = 12
      Width = 305
      Height = 21
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = LookUpCursosValidKey
    end
  end
end
