unit FEditSegundoCriterio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGlobal, ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ArchivoIni,ZBaseDlgModal_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, ImgList, cxButtons, cxControls, cxContainer, cxEdit,
  cxGroupBox;

type
  TSegundoCriterioEdit_DevEx = class(TZetaDlgModal_DevEx)
    GroupBox1: TcxGroupBox;
    Label5: TLabel;
    CriterioAdicional: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
         FIniValues: TTArchivosIni;
         procedure LlenaCriterioAdicional;
         function GuardarEnArchivo: Boolean;
         procedure ObtenSegundoCriterio;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SegundoCriterioEdit_DevEx: TSegundoCriterioEdit_DevEx;

const
     K_CANCELAR = '&Cancelar';
     K_SALIR = '&Salir';
implementation

uses DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     dSegundoCriterio;
{$R *.dfm}

procedure TSegundoCriterioEdit_DevEx.LlenaCriterioAdicional;
var
   i: Integer;
begin
     with CriterioAdicional.Items do
     begin
          BeginUpdate;
          Clear;
          try
             Add( '<Indefinido>' );

             if( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) <> 1 )then
                 Add( 'Clasificación' );
             if( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) <> 2 )then
                 Add( 'Turno' );

             for i := 1 to 9 do
             begin
                  with Global do
                  begin
                       { Se le suma dos ya que se manejan dos valores iniciales
                         al agregar el criterio de agrupación ('Clasificación' y 'Turno').}
                       if ( GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) <> ( i + 2 ) )then
                       begin
                            if StrLleno( NombreNivel( i ) ) then
                               Add( NombreNivel( i ) )
                            else
                                Break;
                       end;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
     //cboCriterioCertific.Items := CriterioAdicional.Items;
end;

procedure TSegundoCriterioEdit_DevEx.FormCreate(Sender: TObject);
begin
     FIniValues := TTArchivosIni.Create;
     LlenaCriterioAdicional;
     ObtenSegundoCriterio;
     //HelpContext:= H65108_Capacitacion;
end;

{Guarda el segundo criterio de agrupación en archivo .INI}
function TSegundoCriterioEdit_DevEx.GuardarEnArchivo: Boolean;
begin
     if( CriterioAdicional.ItemIndex > 0 )then
     begin
         with FIniValues do
         begin
              if( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) <= CriterioAdicional.ItemIndex )then
                  SegundoCriterio := IntToStr( CriterioAdicional.ItemIndex + 1 )
              else
                  SegundoCriterio := IntToStr( CriterioAdicional.ItemIndex );
              SegundoCriterioDesc := CriterioAdicional.Text;
         end;
     end
     else
     begin
          with FIniValues do
          begin
               SegundoCriterio := IntToStr( CriterioAdicional.ItemIndex );
               SegundoCriterioDesc := VACIO;
          end;
     end;

     {Actualiza el campo GLOBAL2 en ENTRENA con el valor de la segunda agrupación}
     dmSegundoCriterio.ActualizaEntrena( StrToIntDef( FIniValues.SegundoCriterio, 0 ), FIniValues.SegundoCriterioDesc );
     Result := True;
end;

procedure TSegundoCriterioEdit_DevEx.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FIniValues );
     inherited;
end;

procedure TSegundoCriterioEdit_DevEx.OKClick(Sender: TObject);
begin
     GuardarEnArchivo;
end;

{Obtiene el valor del archivo (si existe).}
procedure TSegundoCriterioEdit_DevEx.ObtenSegundoCriterio;
begin
     with FIniValues do
     begin
          if( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) <= StrToIntDef( SegundoCriterio, 0 ) )then
              CriterioAdicional.ItemIndex := ( StrToIntDef( SegundoCriterio, 0 ) - 1 )
          else
              CriterioAdicional.ItemIndex := StrToIntDef( SegundoCriterio, 0 );
     end;
end;

end.
