unit FTressShell;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Buttons,
     Forms, Dialogs, ExtCtrls, ComCtrls, Db, Grids, DBGrids, StdCtrls,
     ZetaKeyCombo, ImgList, ActnList, Menus, ZetaStateComboBox, ZetaSmartLists,
     ZetaDBTextBox, Mask, ZetaNumero, ZetaFecha, ZetaDBGrid,TDMULTIP,
     ZBaseShell,
     ZetaMessages,
     ZetaCommonClasses,
     ZBaseConsulta,
     ZetaTipoEntidad,
     ToolWin,
     {$ifdef VER150}
     XPMan,
     {$endif}     
     fcOutlookList, fcButton, fcImgBtn, fcShapeBtn, fcClearPanel,
  fcButtonGroup, fcOutlookBar, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, dxStatusBar,
  dxRibbonStatusBar, cxButtons;

type
  TTressShell = class(TBaseShell)
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    ArbolImages: TImageList;
    Menu: TMainMenu;
    Archivo1: TMenuItem;
    OtraEmpresa1: TMenuItem;
    Servidor1: TMenuItem;
    Salir1: TMenuItem;
    Ayuda1: TMenuItem;
    Acercade1: TMenuItem;
    datasource: TDataSource;
    Paneloutlook: TPanel;
    Splitter1: TSplitter;
    PanelPrincipal: TPanel;
    PanelConsulta: TPanel;
    PanelTitulo: TPanel;
    LblFormato: TLabel;
    PGSuperior: TPageControl;
    EmpleadoTab: TTabSheet;
    EmpleadoPanel: TPanel;
    EmpleadoLBL: TLabel;
    EmpleadoBuscaBtn: TZetaSpeedButton;
    EmpleadoPrimero: TZetaSpeedButton;
    EmpleadoAnterior: TZetaSpeedButton;
    EmpleadoSiguiente: TZetaSpeedButton;
    EmpleadoUltimo: TZetaSpeedButton;
    EmpleadoPrettyName: TLabel;
    EmpleadoNumeroCB: TStateComboBox;
    EmpleadoActivoPanel: TPanel;
    PeriodoTab: TTabSheet;
    PeriodoPanel: TPanel;
    PeriodoNumeroLBL: TLabel;
    PeriodoTipoLbl: TLabel;
    PeriodoDelLBL: TLabel;
    PeriodoAlLBL: TLabel;
    PeriodoPrimero: TZetaSpeedButton;
    PeriodoAnterior: TZetaSpeedButton;
    PeriodoSiguiente: TZetaSpeedButton;
    PeriodoUltimo: TZetaSpeedButton;
    PeriodoFechaInicial: TZetaTextBox;
    PeriodoFechaFinal: TZetaTextBox;
    PeriodoTipoCB: TStateComboBox;
    PeriodoNumeroCB: TStateComboBox;
    PeriodoStatusPanel: TPanel;
    Sistema: TTabSheet;
    Panel2: TPanel;
    Label1: TLabel;
    Fecha: TZetaFecha;
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_Servidor: TAction;
    _A_Salir: TAction;
    _A_DatosConfiguracion: TAction;
    _H_AcercaDe: TAction;
    _P_Imprimir: TAction;
    _Per_Primero: TAction;
    _Per_Siguiente: TAction;
    _Per_Anterior: TAction;
    _Per_Ultimo: TAction;
    _A_BuscaEmpleado: TAction;
    _A_Primero: TAction;
    _A_Anterior: TAction;
    _A_Siguiente: TAction;
    _A_Ultimo: TAction;
    _E_Agregar: TAction;
    _E_Modificar: TAction;
    _E_Borrar: TAction;
    _A_Imprimir: TAction;
    Registro: TMenuItem;
    N3: TMenuItem;
    _A_Exportar: TAction;
    SistemaYearLBL: TLabel;
    SistemaYearCB: TStateComboBox;
    SistemaYearUpDown: TUpDown;
    Procesos1: TMenuItem;
    PanelTop: TPanel;
    Agregar: TcxButton;
    Borrar: TcxButton;
    Modificar: TcxButton;
    RefrescarBtn: TcxButton;
    Imprimir: TcxButton;
    Exportar: TcxButton;
    Imagenes: TImageList;
    fcOutlookBar: TfcOutlookBar;
    fcKiosco: TfcShapeBtn;
    fcSegundoCriterioList: TfcOutlookList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirExecute(Sender: TObject);
    procedure _A_BuscaEmpleadoExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure OutlookItemClick(Sender: TObject; Item: String);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _A_PrimeroExecute(Sender: TObject);
    procedure _A_AnteriorExecute(Sender: TObject);
    procedure _A_SiguienteExecute(Sender: TObject);
    procedure _A_UltimoExecute(Sender: TObject);
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _E_ExportarExecute(Sender: TObject);
    procedure _A_DatosConfiguracionExecute(Sender: TObject);
    procedure PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _Per_PrimeroExecute(Sender: TObject);
    procedure _Per_SiguienteExecute(Sender: TObject);
    procedure _Per_AnteriorExecute(Sender: TObject);
    procedure _Per_UltimoExecute(Sender: TObject);
    procedure PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure _Per_AnteriorUpdate(Sender: TObject);
    procedure _Per_SiguienteUpdate(Sender: TObject);
    procedure _A_ExportarExecute(Sender: TObject);
    procedure SistemaYearCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure SistemaYearUpDownClick(Sender: TObject; Button: TUDBtnType);
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
    procedure _A_R_ExcepcionesExecute(Sender: TObject);
    procedure _A_P_EnviarExcepcionesExecute(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure BuscarCodigoBtnClick(Sender: TObject);
    procedure RefrescarBtnClick(Sender: TObject);
    procedure fcSegundoCriterioListItemClick(
      OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
  private
    { Private declarations }
    FParameterList: TZetaParams;
    FFormaActiva: TBaseConsulta;
    FLastDescription: string;
    FCodigoEmpresa: string;
    function GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean ): TBaseConsulta;
    procedure ActivaFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: string; const iDerecho: integer );
    {procedure RefeshFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: string; const iDerecho: integer );}
    procedure SelectFormaOutLook( const sDescrip: String );
    procedure SetFormaRefresh( const sDescrip: String );
    procedure SetFormaActiva(Forma: TBaseConsulta);
    procedure CierraFormaActiva;
    procedure RefrescaEmpleadoActivos;
    procedure CambioEmpleadoActivo;
    procedure InicializaActions;
    procedure RefrescaPeriodoActivos;
    procedure RefrescarExcepciones;
    procedure CargaPeriodoActivos;
    procedure CargaSistemaActivos;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
  protected
    { Protected declarations }
    property ParameterList: TZetaParams read FParameterList;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
  public
    { Public declarations }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure SetBloqueo;
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure ChangeTimerInfo;
    procedure RefrescaEmpleado;
    procedure GetFechaLimiteShell;
    procedure CambiaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure CargaEmpleadoActivos;
    procedure CambioPeriodoActivo;
    procedure CreaArbol(const lEjecutar: Boolean);
    procedure SetAsistencia(const dValue: TDate);
    procedure BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
    property CodigoEmpresa: string read FCodigoEmpresa write FCodigoEmpresa;
    procedure RefrescaNavegacionInfo;
    procedure CambiaSistemaActivos;
    procedure CambiaPeriodoActivos;
  end;

var
  TressShell: TTressShell;

implementation

uses ZetaDialogo, ZetaCommonlists, ZetaCommonTools, ZetaWinApiTools,
     ZAccesosTress, ZGlobalTress, ZetaClientTools, ZetaBuscaEmpleado,
     ZetaAcercaDe, ZWizardBasico,ZetaSystemWorking,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     Dconsultas, DGlobal, DDiccionario, DSistema, DCatalogos, dReportes,
     DTablas, DRecursos, DCliente, DProcesos, DSegundoCriterio,
     FCatMatrizCurso,FCatMatrizCurso_DevEx, FCatMatrizPuesto, FCatMatrizPuesto_DevEx, FEditSegundoCriterio, FEditSegundoCriterio_DevEx;

{$R *.DFM}

const
     K_PANEL_EMPLEADO    = 1;
     K_PANEL_PERIODO     = 2;
     K_FORMA_MATRIZ_CURSO = 'Matriz por curso';
     K_FORMA_MATRIZ_PUESTO = 'Matriz por puesto';
     K_CODIGO_EMPRESA    = 'TRESS';
     K_FORMA_SEGUNDO_CRITERIO = 'Segundo criterio de agrupaci�n';

{ ***** Metodos de Creaci�n y destrucci�n del Shell ***** }
procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create ( Self );
     dmReportes := TdmReportes.Create( Self );
     dmProcesos := TdmProcesos.Create(Self);
     dmSegundoCriterio := TdmSegundoCriterio.Create( Self );
     FCodigoEmpresa := K_CODIGO_EMPRESA;
     inherited;
     FParameterList := TZetaParams.Create;
     WindowState:= wsMaximized;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( dmSegundoCriterio );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmReportes );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmCliente );
     Global.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll( True );
     DZetaServerProvider.FreeAll;
     {$endif}
     FParameterList.Free;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     //OutLook.ActiveTab := 1;
     FLastDescription := VACIO;
     InicializaActions;
     Fecha.Valor := Date;
     dmCliente.FechaAsistencia := Fecha.Valor;
end;

{ ***** M�todos del Action ***** }
procedure TTressShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     DoCloseAll;
     InicializaActions;
     AbreEmpresa( False );
end;

procedure TTressShell._A_ServidorExecute(Sender: TObject);
begin
     DoCloseAll;
     CambiaServidor;
end;

procedure TTressShell._A_SalirExecute(Sender: TObject);
begin
     Close;
end;

procedure TTressShell._A_BuscaEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        EmpleadoNumeroCB.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

{ ***** Metodos de la forma ***** }
procedure TTressShell.KeyPress(var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.DoOpenAll;
begin
     inherited DoOpenAll;
     Global.Conectar;
     with dmCliente do
     begin
          InitActivosSistema;
          InitActivosEmpleado;
          InitActivosPeriodo;
          FCodigoEmpresa := Compania;
     end;
     CargaEmpleadoActivos;
     CargaPeriodoActivos;
     CargaSistemaActivos;

     {En el caso que no haya un primer criterio, entonces no se puede agregar un segundo.}
     if ( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) = 0 )then
     begin
          fcSegundoCriterioList.Items[ 2 ].Enabled := False;
     end;
end;

procedure TTressShell.ActivaFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: String; const iDerecho: integer );
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     Forma := GetForma( InstanceClass, InstanceName, lNueva );
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               IndexDerechos := iDerecho;
               try
                  if lNueva then
                     DoConnect
                  else
                     Reconnect;
                  Show;
                  SetFormaActiva( Forma );
               except
                    Free;
               end;
          end;
     end
     else
         zError( '�Error Al Crear Forma!', '�Ventana No Pudo Ser Creada!', 0 );
end;

function TTressShell.GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean): TBaseConsulta;
begin
     lNueva := FALSE;
     Result := TBaseConsulta( FindComponent( InstanceName ) );
     if not Assigned( Result ) then
     begin
          try
             Result := InstanceClass.Create( Self ) as TBaseConsulta;
             with Result do
             begin
                  Name := InstanceName;
                  Parent := Self.PanelConsulta;
             end;
             lNueva := TRUE;
          except
             Result := nil;
          end;
     end;
end;

procedure TTressShell.SelectFormaOutLook( const sDescrip: String );
var
   oForma: TSegundoCriterioEdit;
   oForma_DevEx: TSegundoCriterioEdit_DevEx;
begin
     if( sDescrip = K_FORMA_MATRIZ_CURSO )then
          if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
              begin
               ActivaFormaPanel( TCatMatrizCurso_DevEx, 'CatMatrizCurso', D_CAT_CAPA_MATRIZ_CURSO )
             end
          else
            begin
               ActivaFormaPanel( TCatMatrizCurso, 'CatMatrizCurso', D_CAT_CAPA_MATRIZ_CURSO )
            end
     else if( sDescrip = K_FORMA_MATRIZ_PUESTO )then
              if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
                begin
                  ActivaFormaPanel( TCatMatrizPuesto_DevEx, 'CatMatrizPuesto', D_CAT_CAPA_MATRIZ_PUESTO )
                end
              else
                begin
                  ActivaFormaPanel( TCatMatrizPuesto, 'CatMatrizPuesto', D_CAT_CAPA_MATRIZ_PUESTO )
                end
     else if( sDescrip = K_FORMA_SEGUNDO_CRITERIO )then
                if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
                   begin
                     oForma_DevEx := TSegundoCriterioEdit_DevEX.Create( Self );
                    try
                     oForma_DevEx.ShowModal;
                    finally
                      FreeAndNil( oForma );
                   end;
                  end
                else
                  begin
                     oForma := TSegundoCriterioEdit.Create( Self );
                    try
                     oForma.ShowModal;
                    finally
                      FreeAndNil( oForma );
                  end;



            end;

     FLastDescription := sDescrip;
end;

procedure TTressShell.SetFormaActiva(Forma: TBaseConsulta);
begin
     if ( FFormaActiva <> Forma ) then
     begin
          if ( Assigned( FFormaActiva ) ) then
          begin
               FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
               FFormaActiva.Close;
          end;
          FFormaActiva := Forma;
          if ( Assigned( FFormaActiva ) ) then
          begin
               with FFormaActiva do
               begin
                    _E_Agregar.Enabled := CheckDerechos( K_DERECHO_ALTA );
                    _E_Borrar.Enabled := CheckDerechos( K_DERECHO_BAJA );
                    _E_Modificar.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
                    _A_Imprimir.Enabled := CheckDerechos( K_DERECHO_IMPRESION );
                    HelpContext:= FFormaActiva.HelpContext;
                    SetFocus;
               end;
          end
          else
              InicializaActions;
     end;
end;

procedure TTressShell.OutlookItemClick(Sender: TObject; Item: String);
begin
     PanelTitulo.Caption := Item;
     if ( Item = 'Globales' ) then
     begin
          if (  dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) then
             SelectFormaOutLook( Item )
          else
              ZInformation( 'Evaluaciones', 'No se tienen derechos para editar el contenido', 0);
     end
     else
         SelectFormaOutLook( Item );
end;

procedure TTressShell.DoCloseAll;
begin
     inherited DoCloseAll;
     CierraFormaActiva;
end;

procedure TTressShell.CierraFormaActiva;
begin
     if ( Assigned( FFormaActiva ) ) then
     begin
          FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
          FFormaActiva.Close;
     end;
     FLastDescription := VACIO;
     PanelTitulo.Caption := VACIO;
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     with dmCliente do
     begin
          EmpleadoNumeroCB.ValorEntero := Empleado;
     end;
     RefrescaEmpleadoActivos;
end;

procedure TTressShell._E_AgregarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoInsert;
     end;
end;

procedure TTressShell._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoDelete;
     end;
end;

procedure TTressShell._E_ModificarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoEdit;
     end;
end;

procedure TTressShell._A_ImprimirExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoPrint;
     end;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoRefresh;
     end;
end;

procedure TTressShell.RefrescaEmpleadoActivos;
begin
     with dmCliente do
     begin
          StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          with GetDatosEmpleadoActivo do
          begin
               EmpleadoPrettyName.Caption := Nombre;
               with EmpleadoActivoPanel do
               begin
                    if Activo then
                    begin
                         if ( Baja = NullDateTime ) then
                         begin
                              Caption := 'Activo';
                              Hint := VACIO;
                              Font.Color := clBlack;
                         end
                         else
                         begin
                              Caption := 'Reingreso';
                              Hint := 'Baja: ' + FechaCorta( Baja ) + ' Reingreso: ' + FechaCorta( Ingreso );
                              Font.Color := clGreen;
                         end;
                    end
                    else
                    begin
                         Caption := 'Baja';
                         Hint := 'Baja desde ' + FechaCorta( Baja );
                         Font.Color := clRed;
                    end;
               end;
          end;
          if strLleno( FLastDescription ) then
             SetFormaRefresh( FLastDescription );
     end;
end;

procedure TTressShell._A_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._A_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._A_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._A_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  EmpleadoNumeroCB.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB.ValorEntero );
        if lOk then
           CambioEmpleadoActivo
        else
            ZetaDialogo.zInformation( 'Error', '!Empleado No Encontrado!'+CR_LF+'Por Favor Verificar Sus Datos', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;


procedure TTressShell.SetFormaRefresh(const sDescrip: String);
begin
{
Esto no nos es �til. Borrar.
     if( sDescrip = K_FORMA_EXCEPCIONES )then
        RefeshFormaPanel( TExcepciones, 'Excepciones', D_NOM_EXCEP_MONTOS );
     else if ( sDescrip = K_FORMA_CONSULTA ) then
             RefeshFormaPanel( TConsultaGeneral, 'ConsultaGeneral', D_EMP_DATOS_CONTRATA );
     FLastDescription := sDescrip;
}
end;

{
procedure TTressShell.RefeshFormaPanel(InstanceClass: TBaseConsultaClass; const InstanceName: string; const iDerecho: integer);
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     Forma := GetForma( InstanceClass, InstanceName, lNueva );
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               IndexDerechos := iDerecho;
               try
                  if lNueva then
                     DoRefresh
                  else
                     DoRefresh;
                  Show;
                  SetFormaActiva( Forma );
               except
                    Free;
               end;
          end;
     end
     else
          zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
end;
}
procedure TTressShell._E_ExportarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoExportar;
     end;
end;

procedure TTressShell.InicializaActions;
begin
     _E_Agregar.Enabled := False;
     _E_Borrar.Enabled := False;
     _E_Modificar.Enabled := False;
     _A_Imprimir.Enabled := False;
end;

procedure TTressShell._A_DatosConfiguracionExecute(Sender: TObject);
begin
     //
end;

procedure TTressShell.PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          Application.ProcessMessages;
          try
             PeriodoTipo := eTipoPeriodo( PeriodoTipoCB.Indice );
             CambioPeriodoActivo;
             if ( GetDatosPeriodoActivo.Numero = 0 ) then
                ZetaDialogo.zInformation( 'Aviso', 'No Se Ha Definido Ning�n Per�odo ' + PeriodoTipoCB.Text, 0 );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TTressShell._Per_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoPrimero then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoSiguiente then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoAnterior then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoUltimo then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        lOk := dmCliente.SetPeriodoNumero( PeriodoNumeroCB.ValorEntero );
        if lOk then
        begin
             RefrescaPeriodoActivos;
        end
        else
            ZetaDialogo.zInformation( 'Error', '! No Existe El Per�odo De N�mina !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.RefrescarExcepciones;
begin
     //
end;

procedure TTressShell.CambioPeriodoActivo;
begin
     PeriodoNumeroCB.ValorEntero := dmCliente.PeriodoNumero;
     RefrescaPeriodoActivos;
     RefrescarExcepciones;
end;

procedure TTressShell.CargaPeriodoActivos;
begin
     with dmCliente do
     begin
          PeriodoTipoCB.Indice := Ord( PeriodoTipo );
          PeriodoNumeroCB.ValorEntero := PeriodoNumero;
     end;
     RefrescaPeriodoActivos;
end;

procedure TTressShell.RefrescaPeriodoActivos;
begin
     with dmCliente do
     begin
          StatusBarMsg( GetPeriodoDescripcion, K_PANEL_PERIODO );
          with GetDatosPeriodoActivo do
          begin
               PeriodoFechaInicial.Caption := FechaCorta( Inicio );
               PeriodoFechaFinal.Caption := FechaCorta( Fin );
               with PeriodoStatusPanel do
               begin
                    if ( Numero = 0 ) then
                    begin
                         Caption := 'Sin Definir';
                         Font.Color := clRed;
                         Hint := 'Per�odos ' + PeriodoTipoCB.Text + ' No Han Sido Definidos';
                    end
                    else
                    begin
                         Caption := ObtieneElemento( lfStatusPeriodo, Ord( Status ) );
                         Font.Color := clBlue;
                         Hint := '';
                    end;
               end;
          end;
          GetFechaLimiteShell;
          RefrescarExcepciones;
     end;
end;

procedure TTressShell._Per_AnteriorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoDownEnabled;
end;

procedure TTressShell._Per_SiguienteUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell._A_ExportarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoExportar;
     end;
end;

procedure TTressShell.SistemaYearCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             YearDefault := SistemaYearCB.ValorEntero;
        end;
        CambioPeriodoActivo;
        //RefrescaSistemaActivos;
        //CambioValoresActivos( stSistema );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.SistemaYearUpDownClick(Sender: TObject; Button: TUDBtnType);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with SistemaYearCB do
        begin
             case Button of
                  btNext: AsignaValorEntero( ValorEntero + 1 );
                  btPrev: AsignaValorEntero( ValorEntero - 1 );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     with dmCliente do
     begin
          SistemaYearCB.ValorEntero := YearDefault;
          Fecha.Valor := FechaDefault;
     end;
end;

procedure TTressShell._A_R_ExcepcionesExecute(Sender: TObject);
begin
//
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }
procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     { No se Hace Nada }
end;

procedure TTressShell._A_P_EnviarExcepcionesExecute(Sender: TObject);
begin
     //
end;

procedure TTressShell.ExportarClick(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoExportar;
     end;
end;

procedure TTressShell.BuscarCodigoBtnClick(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FormaActiva.DoLookup;
     end;
end;

procedure TTressShell.RefrescarBtnClick(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoRefresh;
     end;
end;

procedure TTressShell.fcSegundoCriterioListItemClick(
  OutlookList: TfcCustomOutlookList; Item: TfcOutlookListItem);
begin
     PanelTitulo.Caption := Item.Text;
     if ( Item.Text = 'Globales' ) then
     begin
          if (  dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) then
             SelectFormaOutLook( Item.Text )
          else
              ZInformation( 'Evaluaciones', 'No se tienen derechos para editar el contenido', 0);
     end
     else
         SelectFormaOutLook( Item.Text );
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
//
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     // No implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     // No implementar
end;

end.

