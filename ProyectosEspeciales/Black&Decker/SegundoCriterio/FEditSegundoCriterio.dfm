inherited SegundoCriterioEdit: TSegundoCriterioEdit
  Left = 714
  Top = 299
  Caption = 'Segundo criterio de agrupaci'#243'n'
  ClientHeight = 122
  ClientWidth = 377
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 86
    Width = 377
    inherited OK: TBitBtn
      Left = 209
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 294
    end
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 6
    Width = 361
    Height = 71
    Caption = 'Criterios adicionales de programaci'#243'n (segundo criterio)'
    TabOrder = 1
    object Label5: TLabel
      Left = 85
      Top = 32
      Width = 35
      Height = 13
      Caption = 'Cursos:'
    end
    object CriterioAdicional: TComboBox
      Left = 124
      Top = 28
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
    end
  end
end
