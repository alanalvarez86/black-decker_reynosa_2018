library SegundoCriterio;
{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}
uses
  midasLib,
  ComServ,
  DServerSegundoCriterio in '..\MTS\DServerSegundoCriterio.pas' {dmServerSegundoCriterio: TMtsDataModule} {dmServerSegundoCriterio: CoClass},
  SegundoCriterio_TLB in '..\MTS\SegundoCriterio_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ../MTS/SegundoCriterio.TLB}

{$R *.RES}

begin
end.
