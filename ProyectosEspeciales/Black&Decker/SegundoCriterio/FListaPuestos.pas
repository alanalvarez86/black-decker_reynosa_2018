unit FListaPuestos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, checklst, Buttons, ExtCtrls ,ZetaClientDataSet;

type
  TListaPuestos = class(TForm)
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Lista: TCheckListBox;
    PrendeBtn: TBitBtn;
    ApagaBtn: TBitBtn;
    procedure PrendeBtnClick(Sender: TObject);
    procedure ApagaBtnClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FCurso: String;
    FListaTemp: TStringList;
    FMatrizCursos : Boolean;
    procedure PrendeApaga( lState: Boolean );
    procedure AgregaListaPuestos;
    procedure FiltraPuestosProg;
    function GetDataSet: TZetaClientDataSet;

  public
    property Curso: String read FCurso write FCurso;
    property MatrizCursos :Boolean read FMatrizCursos write FMatrizCursos;
  end;

function SetListaPuestos( const sCurso: String ; const MatrizCurso:Boolean ): Boolean;

var
  ListaPuestos: TListaPuestos;

implementation

uses DCliente,dCatalogos, ZetaDialogo, dSegundoCriterio,ZetaCommonLists,FListaPuestos_DevEx;

function SetListaPuestos( const sCurso: String; const MatrizCurso:Boolean  ): Boolean;
begin
{if ( dmCliente.GetDatosUsuarioActivo.Vista = tvClasica )then
  begin
      if ( ListaPuestos = nil ) then
         ListaPuestos:= TListaPuestos.Create( Application );
      try
         with ListaPuestos do
         begin
              Curso := sCurso;
              MatrizCursos := MatrizCurso;
              ShowModal;
              Result := ( ModalResult = mrOk );
         end;
      finally
        FreeAndNil( ListaPuestos );
     end;
   end
else
  begin  }
    if ( ListaPuestos_DevEx = nil ) then
         ListaPuestos_DevEx:= TListaPuestos_DevEx.Create( Application );
      try
         with ListaPuestos_DevEx do
         begin
              Curso := sCurso;
              MatrizCursos := MatrizCurso;
              ShowModal;
              Result := ( ModalResult = mrOk );
         end;
      finally
        FreeAndNil( ListaPuestos_DevEx );
     end;
 // end;


end;

{$R *.DFM}

procedure TListaPuestos.FormCreate(Sender: TObject);
begin
     FListaTemp:= TStringList.Create;
     //HelpContext := H80815_Supervision_de_usuarios;
end;

procedure TListaPuestos.FormShow(Sender: TObject);
begin
     dmCatalogos.CargaListaPuestos( Lista.Items );
end;

procedure TListaPuestos.FormDestroy(Sender: TObject);
begin
     FListaTemp.Free;
end;

procedure TListaPuestos.AgregaListaPuestos;
var
   i: Integer;
begin
     FiltraPuestosProg;
     for i := 0 to Lista.Items.Count - 1 do
         if Lista.Checked[ i ] then
            dmSegundoCriterio.GuardaEntrenaPuestosProg( Lista.Items.Names[ i ], Curso,FMatrizCursos  );
end;

procedure TListaPuestos.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     with GetDataSet  do
     begin
          DisableControls;
          try
             with Screen do
             begin
                  oCursor := Cursor;
                  Cursor := crHourglass;
                  try
                     AgregaListaPuestos;
                     Enviar;
                  finally
                     Cursor := oCursor;
                  end;
             end;
             if ( ChangeCount > 0 ) then     // Algunos no se pudieron agregar
             begin
                  CancelUpdates;
                  ZError( self.Caption, 'No se Agregaron Todos los Puestos Seleccionados!', 0 );
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TListaPuestos.PrendeBtnClick(Sender: TObject);
begin
     PrendeApaga( True );
end;

procedure TListaPuestos.ApagaBtnClick(Sender: TObject);
begin
     PrendeApaga( False );
end;

procedure TListaPuestos.PrendeApaga( lState: Boolean );
var
   i:Integer;
begin
     Lista.Items.BeginUpdate;
     for i:=0 to Lista.Items.Count - 1 do
         Lista.Checked[i]:= lState;
     Lista.Items.EndUpdate;
end;

function TListaPuestos.GetDataSet:TZetaClientDataSet;
begin
     Result := dmSegundoCriterio.cdsMatrizCurso ;
end;

procedure TListaPuestos.FiltraPuestosProg;
var
   iPos: Integer;
begin
     with GetDataSet, Lista.Items do
     begin
          BeginUpdate;
          First;
          while not EOF do
          begin
               iPos := IndexOf( FieldByName( 'PU_CODIGO' ).AsString + '=' +
                                FieldByName( 'PU_NOMBRE' ).AsString );
               if ( iPos >= 0 ) then
                  Lista.Checked[iPos] := FALSE;
               Next;
          end;
          EndUpdate;
     end;
end;

end.
