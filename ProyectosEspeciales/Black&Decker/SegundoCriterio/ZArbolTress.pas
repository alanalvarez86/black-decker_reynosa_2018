unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB,
     ZArbolTools;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TTreeView );

implementation

uses DGlobal,
     DSistema,
     DCliente,
     ZetaDespConsulta,
     ZGlobalTress,
     ZetaCommonTools,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaTipoEntidad;

const
     {******** Administración *****}
     K_ADMINISTRACION                   = 900;
     K_RELACION_ORDENES                 = 901;
     K_PROCESOS_ORDENES                 = 902;
     K_TARIFA_EMPLEADO                  = 903;
     K_TARIFA_CATAGORIA                 = 904;
     K_PROCESOS                         = 905;

     {******** Consultas **********}
     K_CONSULTAS                        = 129;
     K_CONS_REPORTES                    = 130;
     K_CONS_SQL                         = 133;
     K_CONS_BITACORA                    = 161;
     K_CONS_PROCESOS                    = 163; { GA 22/Feb/2000:ANTES ERA UN HUECO 163 }

     {******** Catálogos **********}
     K_CATALOGOS                        = 134;

     {***** SISTEMA ****************** }
     K_SISTEMA                             = 21;
     K_SISTEMA_DATOS                       = 22;
     K_SISTEMA_DATOS_EMPRESAS              = 14;
     K_SISTEMA_DATOS_GRUPOS                = 23;
     K_SISTEMA_DATOS_ROLES                 = 24;
     K_SISTEMA_DATOS_USUARIOS              = 25;
     K_SISTEMA_DATOS_IMPRESORAS            = 26;

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     Result := Folder( '', 0 );
end;

procedure CreaArbolDefault( oArbolMgr : TArbolMgr );
begin
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     // CreaArbolUsuario
     with oArbolMgr do
     begin
          Configuracion := TRUE;
          oArbolMgr.NumDefault := K_ADMINISTRACION;
          CreaArbolDefault( oArbolMgr );
          Arbol.FullCollapse;
     end;
end;

procedure MuestraArbolOriginal( oArbol: TTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

end.
