inherited CatMatrizPuesto_DevEx: TCatMatrizPuesto_DevEx
  Left = 2000
  Top = 274
  Caption = 'Matriz de entrenamiento por puesto'
  ClientHeight = 364
  ClientWidth = 672
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 672
    TabOrder = 2
    inherited Slider: TSplitter
      Left = 378
    end
    inherited ValorActivo1: TPanel
      Width = 362
      inherited textoValorActivo1: TLabel
        Width = 356
      end
    end
    inherited ValorActivo2: TPanel
      Left = 381
      Width = 291
      inherited textoValorActivo2: TLabel
        Width = 285
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 672
    Height = 46
    Align = alTop
    Color = clBtnHighlight
    TabOrder = 0
    object Label1: TLabel
      Left = 37
      Top = 16
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Puesto:'
    end
    object LookUpPuesto: TZetaKeyLookup_DevEx
      Left = 80
      Top = 12
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = LookUpPuestoValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 65
    Width = 672
    Height = 299
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object ZetaDBGridDBTableViewCU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
      end
      object ZetaDBGridDBTableViewCU_NOMBRE: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_NOMBRE'
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
        Width = 196
      end
      object ZetaDBGridDBTableViewCU_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'CU_REVISIO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
      end
      object ZetaDBGridDBTableViewEN_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as de antig'#252'edad'
        DataBinding.FieldName = 'EN_DIAS'
        GroupSummaryAlignment = taCenter
        MinWidth = 15
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
        Width = 110
      end
      object ZetaDBGridDBTableViewEN_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'EN_OPCIONA'
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
        Width = 68
      end
      object ZetaDBGridDBTableViewEN_LISTA: TcxGridDBColumn
        Caption = 'Lista'
        DataBinding.FieldName = 'EN_LISTA'
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
      end
      object ZetaDBGridDBTableViewEN_LISTA2: TcxGridDBColumn
        Caption = 'Lista 2'
        DataBinding.FieldName = 'EN_LISTA2'
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
      end
    end
    object ZetaDBGridDBTableView1: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object ZetaDBGridDBTableView1CU_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CU_CODIGO'
      end
      object ZetaDBGridDBTableView1CU_NOMBRE: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_NOMBRE'
        Width = 263
      end
      object ZetaDBGridDBTableView1CU_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'CU_REVISIO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taLeftJustify
        Properties.ReadOnly = True
      end
      object ZetaDBGridDBTableView1EN_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as de antig'#252'edad'
        DataBinding.FieldName = 'EN_DIAS'
        HeaderAlignmentHorz = taRightJustify
        Width = 104
      end
      object ZetaDBGridDBTableView1EN_OPCIONA: TcxGridDBColumn
        Caption = 'Opcional'
        DataBinding.FieldName = 'EN_OPCIONA'
        Width = 52
      end
      object ZetaDBGridDBTableView1EN_LISTA: TcxGridDBColumn
        Caption = 'Lista'
        DataBinding.FieldName = 'EN_LISTA'
      end
      object ZetaDBGridDBTableView1EN_LISTA2: TcxGridDBColumn
        Caption = 'Lista 2'
        DataBinding.FieldName = 'EN_LISTA2'
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
