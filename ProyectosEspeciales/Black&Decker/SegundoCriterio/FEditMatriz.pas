unit FEditMatriz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaNumero, ZetaKeyLookup,
  ZetaDBTextBox, FCatEntNivel, ZetaSmartLists, ArchivoIni;

type
  TEditMatriz = class(TBaseEdicion)
    PU_CODIGO: TZetaDBKeyLookup;
    Label2: TLabel;
    Label3: TLabel;
    EN_OPCIONA: TDBCheckBox;
    EN_DIAS: TZetaDBNumero;
    Label1: TLabel;
    ZCurso: TZetaTextBox;
    Label4: TLabel;
    gbProgramacion: TGroupBox;
    bModifica: TBitBtn;
    gbProgramarA: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bModificaClick(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
         FIniValues: TTArchivosIni;
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatriz: TEditMatriz;

implementation

uses FTressShell, ZetaCommonClasses, dCatalogos, dGlobal, ZGlobalTress,
     ZBaseDlgModal, ZAccesosTress, ZetaClientTools, dSegundoCriterio;

{$R *.DFM}

procedure TEditMatriz.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_CURSO;
     HelpContext:= H60643_Matriz_curso;
     FirstControl := PU_CODIGO;
     PU_CODIGO.LookupDataset := dmCatalogos.cdsPuestos;
     FIniValues := TTArchivosIni.Create;
end;

procedure TEditMatriz.FormShow(Sender: TObject);
begin
     inherited;
     ZCurso.Caption := Datasource.DataSet.FieldByName( 'CU_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsCursos.FieldByName( 'CU_NOMBRE' ).AsString;
     gbProgramacion.Visible := ( ( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0 ) and ( StrToIntDef( FIniValues.SegundoCriterio, 0 ) > 0 ) ); //Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0;
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programación por ' + FIniValues.SegundoCriterioDesc; //Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CURSOS);
          Height := GetScaledHeight( 325 ); //+50 Se aumentó la altura debido a los Large Fonts;
     end
     else
         Height := GetScaledHeight( 208 ); //+50 Se aumentó la altura debido a los Large Fonts0;
end;

procedure TEditMatriz.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          Datasource.Dataset := dmSegundoCriterio.cdsMatrizCurso;
     end;
end;

procedure TEditMatriz.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatriz.bModificaClick(Sender: TObject);
begin
     inherited;
     ShowDlgModal( CatEntNivel, TCatEntNivel );
end;

procedure TEditMatriz.OKClick(Sender: TObject);
begin
     if ( gbProgramarA.Value = 'N' ) then
        with dmSegundoCriterio.cdsEntNivel2 do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;
end;

procedure TEditMatriz.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FIniValues );
end;

end.



