unit FEditMatriz_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls,
  Buttons, DBCtrls, StdCtrls, Mask, ZetaNumero, ZetaKeyLookup,
  ZetaDBTextBox, FCatEntNivel, ZetaSmartLists, ArchivoIni, ZBaseEdicion,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TEditMatriz_DevEx = class(TBaseEdicion_DevEx)
    PU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    Label3: TLabel;
    EN_OPCIONA: TDBCheckBox;
    EN_DIAS: TZetaDBNumero;
    Label1: TLabel;
    ZCurso: TZetaTextBox;
    Label4: TLabel;
    gbProgramacion: TGroupBox;
    bModifica: TcxButton;
    gbProgramarA: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bModificaClick(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
         FIniValues: TTArchivosIni;
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatriz_DevEx: TEditMatriz_DevEx;

implementation

uses FTressShell, ZetaCommonClasses, dCatalogos, dGlobal, ZGlobalTress,
     ZBaseDlgModal, ZAccesosTress, ZetaClientTools, dSegundoCriterio,
  DCliente;

{$R *.DFM}

procedure TEditMatriz_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_CURSO;
     HelpContext:= H60643_Matriz_curso;
     FirstControl := PU_CODIGO;
     PU_CODIGO.LookupDataset := dmCatalogos.cdsPuestos;
     FIniValues := TTArchivosIni.Create;
end;

procedure TEditMatriz_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZCurso.Caption := Datasource.DataSet.FieldByName( 'CU_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsCursos.FieldByName( 'CU_NOMBRE' ).AsString;
     gbProgramacion.Visible := ( ( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0 ) and ( StrToIntDef( FIniValues.SegundoCriterio, 0 ) > 0 ) ); //Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0;
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programación por ' + FIniValues.SegundoCriterioDesc; //Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CURSOS);
          Height := GetScaledHeight( 325 ); //+50 Se aumentó la altura debido a los Large Fonts;
     end
     else
         Height := GetScaledHeight( 208 ); //+50 Se aumentó la altura debido a los Large Fonts0;
end;

procedure TEditMatriz_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          Datasource.Dataset := dmSegundoCriterio.cdsMatrizCurso;
     end;
end;

procedure TEditMatriz_DevEx.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatriz_DevEx.bModificaClick(Sender: TObject);
begin
     inherited;

     ShowDlgModal( CatEntNivel, TCatEntNivel )

end;

procedure TEditMatriz_DevEx.OKClick(Sender: TObject);
begin
     if ( gbProgramarA.Value = 'N' ) then
        with dmSegundoCriterio.cdsEntNivel2 do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;
end;

procedure TEditMatriz_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FIniValues );
end;

end.



