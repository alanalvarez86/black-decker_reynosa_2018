unit FCatMatrizCurso_DevEx;

interface
                 
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, StdCtrls,
  ZetaKeyLookup, Buttons,ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZetaKeyLookup_DevEx, cxButtons;

type
  TCatMatrizCurso_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    LookUpCursos: TZetaKeyLookup_DevEx;
    BAgregaPuestos: TcxButton;
    ZetaDBGridDBTableView1: TcxGridDBTableView;
    ZetaDBGridDBTableView1PU_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableView1PU_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_DIAS: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_OPCIONA: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_LISTA: TcxGridDBColumn;
    ZetaDBGridDBTableView1EN_LISTA2: TcxGridDBColumn;
    ZetaDBGridDBTableViewPU_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableViewPU_NOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_DIAS: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_OPCIONA: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_LISTA: TcxGridDBColumn;
    ZetaDBGridDBTableViewEN_LISTA2: TcxGridDBColumn;
    procedure LookUpCursosValidKey(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BAgregaPuestosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure DisConnect;override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
  end;

var
  CatMatrizCurso_DevEx: TCatMatrizCurso_DevEx;

implementation

uses dCatalogos, dCliente, ZetaCommonClasses, FAutoClasses, FListaPuestos,
  dSegundoCriterio;

{$R *.DFM}

{ TCatMatrizCurso }

procedure TCatMatrizCurso_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H60643_Matriz_curso;
end;

procedure TCatMatrizCurso_DevEx.DisConnect;
begin
     LookUpCursos.LookUpDataSet := NIL;
end;

procedure TCatMatrizCurso_DevEx.Connect;
begin
     LookUpCursos.LookUpDataSet := dmCatalogos.cdsCursos;
     with dmCatalogos do
     begin
          with cdsCursos do
          begin
               Conectar;
               LookUpCursos.Llave := FieldByName('CU_CODIGO').AsString;
          end;
          dmSegundoCriterio.SetMatriz( TRUE, LookUpCursos.Llave );
          cdsPuestos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          DataSource.DataSet:= dmSegundoCriterio.cdsMatrizCurso;
          Refresh;
     end;
end;

procedure TCatMatrizCurso_DevEx.Refresh;
begin
     dmSegundoCriterio.cdsMatrizCurso.Refrescar;
     DoBestFit;
     // No s� por qu�, pero se pierde al activar la otra Matriz de Cursos
{
     ZetaDBGrid1.Columns[ 0 ].FieldName := 'PU_CODIGO';
     ZetaDBGrid1.Columns[ 2 ].FieldName := 'EN_DIAS';
     ZetaDBGrid1.Columns[ 3 ].FieldName := 'EN_OPCIONA';
     ZetaDBGrid1.Columns[ 4 ].FieldName := 'EN_LISTA';
     ZetaDBGrid1.Columns[ 5 ].FieldName := 'EN_LISTA2';
   }
end;

procedure TCatMatrizCurso_DevEx.LookUpCursosValidKey(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TCatMatrizCurso_DevEx.Agregar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Agregar;
     DoBestFit;
end;

procedure TCatMatrizCurso_DevEx.Borrar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Borrar;
     DoBestFit;
end;

procedure TCatMatrizCurso_DevEx.Modificar;
begin
     dmSegundoCriterio.cdsMatrizCurso.Modificar;
     DoBestFit;
end;

function TCatMatrizCurso_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeAgregar( sMensaje );
end;

function TCatMatrizCurso_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeModificar( sMensaje );
     DoBestFit;
end;

function TCatMatrizCurso_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= dmCliente.ModuloAutorizadoConsulta( okCursos, sMensaje );
     if Result then
        Result := inherited PuedeBorrar( sMensaje );
     DoBestFit;
end;

procedure TCatMatrizCurso_DevEx.BAgregaPuestosClick(Sender: TObject);
begin
     inherited;
     if SetListaPuestos( LookUpCursos.Llave,True ) then
        DoRefresh;
        DoBestFit;
end;

procedure TCatMatrizCurso_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

end;

end.
