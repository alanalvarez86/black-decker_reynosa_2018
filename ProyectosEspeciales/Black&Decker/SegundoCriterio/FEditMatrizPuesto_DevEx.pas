unit FEditMatrizPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, ZetaDBTextBox, StdCtrls, Mask, ZetaNumero, DBCtrls,
  ZetaKeyLookup, Db, ExtCtrls, FCatEntNivel,
  Buttons, ZetaSmartLists, ArchivoIni,ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TEditMatrizPuesto_DevEx = class(TBaseEdicion_DevEX)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EN_OPCIONA: TDBCheckBox;
    EN_DIAS: TZetaDBNumero;
    ZPuesto: TZetaTextBox;
    CU_CODIGO: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    gbProgramacion: TGroupBox;
    bModifica: TcxButton;
    gbProgramarA: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bModificaClick(Sender: TObject);
    procedure gbProgramarAChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
         FIniValues: TTArchivosIni;  
  protected
    procedure Connect; override;
  public
  end;

var
  EditMatrizPuesto_DevEx: TEditMatrizPuesto_DevEx;

implementation

uses FTressShell,ZetaCommonClasses, dCatalogos, dSegundoCriterio, dGlobal, ZBaseDlgModal,
     ZAccesosTress, ZGlobalTress, ZetaClientTools, DCliente, ZBaseDlgModal_DevEx,FCatEntNivel_DevEX;

{$R *.DFM}

{ TEditMatrizPuesto }

procedure TEditMatrizPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CAT_CAPA_MATRIZ_PUESTO;
     HelpContext:= H60644_Matriz_puesto;
     FirstControl := CU_CODIGO;
     CU_CODIGO.LookupDataset := dmCatalogos.cdsCursos;
     FIniValues := TTArchivosIni.Create;
end;

procedure TEditMatrizPuesto_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ZPuesto.Caption := Datasource.DataSet.FieldByName( 'PU_CODIGO' ).AsString + ' = ' + dmCatalogos.cdsPuestos.FieldByName( 'PU_DESCRIP' ).AsString;
     gbProgramacion.Visible := ( ( Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0 ) and ( StrToIntDef( FIniValues.SegundoCriterio, 0 ) > 0 ) ); //Global.GetGlobalInteger( K_NIVEL_PROGRAMACION_CURSOS ) > 0;
     if gbProgramacion.Visible then
     begin
          gbProgramacion.Caption := 'Programación por ' + FIniValues.SegundoCriterioDesc; //Global.GetDescNivelProg(K_NIVEL_PROGRAMACION_CURSOS);
          Height :=  GetScaledHeight( 325 );//+40 Se aumentó la altura debido a los Large Fonts;
     end
     else
          Height :=  GetScaledHeight( 208 );//+40 Se aumentó la altura debido a los Large Fonts;
end;

procedure TEditMatrizPuesto_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsCursos.Conectar;
          dmSegundoCriterio.cdsMatrizCurso.Conectar;
          Datasource.Dataset := dmSegundoCriterio.cdsMatrizCurso;
     end;
end;

procedure TEditMatrizPuesto_DevEx.bModificaClick(Sender: TObject);
begin
     inherited;

     ShowDlgModal( CatEntNivel_DevEx, TCatEntNivel_DevEX )


end;

procedure TEditMatrizPuesto_DevEx.gbProgramarAChange(Sender: TObject);
begin
     inherited;
     bModifica.Enabled := ( gbProgramarA.Value = 'S' );
end;

procedure TEditMatrizPuesto_DevEx.OKClick(Sender: TObject);
begin
     if ( gbProgramarA.Value = 'N' ) then
        with dmSegundoCriterio.cdsEntNivel2 do
        begin
             First;
             while not EOF do
                   Delete;
        end;
     inherited;
end;

procedure TEditMatrizPuesto_DevEx.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FIniValues );
end;

end.



