unit DInterfase;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Db, DBClient, ZetaClientDataSet, Variants, DZetaServerProvider, ZetaTipoEntidad,
    ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses, Recursos_TLB, Sistema_TLB,
    OleServer;

type
  eTipoMovimiento = ( tmAlta,tmReingreso,tmBaja,tmCambioSalario,TmCambioPuesto,TmCambioClasificacion,tmCambioTurno,tmCambioNiveles,tmCambiosGenerales );
  TdmInterfase = class(TDataModule)
    cdsUsuarios: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErrorEnviar: Boolean;
    FEmpleado, FLinea: Integer;
    FArchivo: string;
    FDatos, FRenglon: TStringList;
    oZetaProvider: TdmZetaServerProvider;
    FTipoMovimiento: eTipoMovimiento;
    FFormatoFecha: eFormatoImpFecha;
    FClaveInterfaz: string;
    FUsuarioInterfaz: string;
    FNombreArchivo: string;
    FServerSistema: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    function ObtenPeriodoBaja(dFecha: TDateTime; iTipo: Integer; var iYear: Integer; var iPeriodo: Integer ): Boolean;
    Function CounsultaMaxNumero:integer;
    function CounsultaMaxNumeroNivel4:integer;
    function GetHeaderTexto(const iEmpleado: Integer; const iLinea: Integer): string;
    function GetSQLScript( iConsulta: Integer ): String;
    function ValidaBajaEmpleado( const sTipo: String ): Boolean;
    function BuscaEmpleado( sNumero: string ): integer;
    function BuscaPuesto( sTexto: string): String;
    function BuscaNivel4( sTexto: string): String;
    function BuscaCodigo( sFiltro, sTexto, sCodigo, sTabla, sMensajeError: String ): String;
    function InsertaPuesto( stexto: string ): string;
    function InsertaNivel4( sTexto: string ): string;
    procedure ReportaProcessDetail(const iFolio: Integer);
    procedure SetNuevosEventos;
    procedure ProcesaArchivo;
    procedure ValidaEnviar( DataSet: TZetaClientDataSet );
    procedure RenombraArchivo( sArchivo: string );
    procedure cdsEmpleadoBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure SetNuevoKardex(const sTipo: String);
    procedure CB_SEGSOCValidate(Sender: TField);
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure ProcesaAlta;
    procedure ProcesaBaja;
    Procedure ProcesaRecontrata;
    Procedure ProcesaCambioDeSalario;
    procedure ProcesaCambioDePuesto;
    procedure ProcesaCambioDeClasificacion;
    procedure ProcesaCambioDeTurno;
    procedure ProcesaCambiodeNiveles;
    procedure ProcesaCambiosGenerales;
    {*** US 16239: (Octubre) Spectrum Brands (Black & Decker) ***}
    function EsTipoDePeriodoValidoInterfaz( iPeriodo: Integer ): Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    SaldarVacaciones : Boolean;
    function GetUsuarioAutomatiza: Boolean;
    procedure Procesa;
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = TRUE );
    procedure EscribeErrorExterno( const sMensaje: string );
    procedure EscribeBitacora(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0 );
    property FormatoFecha : eFormatoImpFecha read FFormatoFecha write FFormatoFecha;
    property Archivo : string read FArchivo write FArchivo;
    property TipoMovimiento: eTipoMovimiento read FTipoMovimiento write FTipoMovimiento;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    property UsuarioInterfaz : string read FUsuarioInterfaz write FUsuarioInterfaz;
    property ClaveInterfaz : string read FClaveInterfaz write FClaveInterfaz;
  end;

var
  dmInterfase: TdmInterfase;

const
     // Consultas SQL
     K_OBTEN_CODIGO             = 1;
     K_CONSULTA_PERIODOS        = 2;
     K_BUSCA_EMPLEADO           = 3;
     K_BUSCA_PUESTO             = 4;
     K_BUSCA_MAX_PUESTO         = 5;
     K_BUSCA_MAX_NIVEL4         = 6;
     K_BUSCA_NIVEL4             = 7;

implementation

uses dProcesos,
     dCliente,
     dRecursos,
     dTablas,
     dCatalogos,
     dConsultas,
     dGlobal,
     dSistema,
     dNomina,
     ZGlobalTress,
     FTressShell,
     ZAsciiTools,
     ZetaAsciiFile,
     ZBaseThreads,
     ZBaseSelectGrid,
     ZetaCommonTools,
     dSuperAscii,
     StrUtils,
     ZReconcile,
     FAutoClasses,
     ZAccesosMgr,
     ZAccesosTress;

{$R *.DFM}

{ TdmInterfase }
procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     FArchivo      := VACIO;
     SetNuevosEventos;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider ); 
end;

{ Manejo de Fin de Proceso - Bit�cora }
procedure TdmInterfase.HandleProcessEnd( const iIndice, iValor: Integer );
var
   oProcessData: TProcessInfo;
   function GetResultadoProceso: string;
   begin
        case oProcessData.Status of
             epsOK: Result := '** Proceso terminado **';
             epsCancelado: Result := '** Proceso cancelado **';
             epsError, epsCatastrofico: Result := '** Proceso con errores **';
        else
             Result := ZetaCommonClasses.VACIO;
        end;
   end;
begin
     oProcessData := TProcessInfo.Create( nil );
     try
        with dmProcesos do
        begin
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             oProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( '�ndice de lista de procesos fuera de rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             ReportaProcessDetail( oProcessData.Folio );
        end;
        EscribeBitacora( GetResultadoProceso );
     finally
            FreeAndNil( oProcessData );
            FProcesandoThread := FALSE;      // Mientras se encuentra la forma de manejar los threads
     end;
end;


procedure TdmInterfase.ReportaProcessDetail( const iFolio: Integer );
const
     K_MENSAJE_BIT = 'Fecha movimiento: %s - %s - %s';
var
   sMensaje: string;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_TEXTO' ).AsString, FieldByName( 'BI_DATA' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal, tbAdvertencia : EscribeBitacora( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger );
                              tbError, tbErrorGrave: EscribeError( sMensaje, FieldByName( 'CB_CODIGO' ).AsInteger, FLinea );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;

{ Utilerias de Bit�cora }
function TdmInterfase.GetHeaderTexto( const iEmpleado: Integer; const iLinea: Integer ): string;
const
     K_HEADER_EMPLEADO = '%sEmpleado #%d: ';
var
   sFechaHora: String;
begin
     Result := VACIO;
     sFechaHora := '(' + FechaCorta( date ) + '-' + FormatDateTime( 'hh:mm', Now ) + '): ';
     if( iLinea > 0 )then
         sFechaHora := sFechaHora + Format( 'L�nea #%d, ', [ iLinea ] );

     if ( iEmpleado > 0 ) then
        Result := Format( K_HEADER_EMPLEADO, [ sFechaHora, iEmpleado ] )
     else
         Result := sFechaHora;
end;

// Manda mensaje a la bit�cora de mensajes.
procedure TdmInterfase.EscribeBitacora(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0 );
begin
     { Se cambia la bandera FHuboCambios para indicar que existen cambios en la informaci�n de los empleados }
     if ( not FHuboCambios ) and ( iEmpleado > 0 ) then
        FHuboCambios := TRUE;
     TressShell.EscribeBitacora( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     Application.ProcessMessages;
end;

// Manda mensaje a la bit�cora de errores.
procedure TdmInterfase.EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = TRUE );
begin
     if lBandera then
     begin
          { Se cambia la bandera FHuboErrores para indicar existen errores en el proceso }
          if ( not FHuboErrores ) then
             FHuboErrores := TRUE;
     end;
     TressShell.EscribeError( GetHeaderTexto( iEmpleado, iLinea ) + sMensaje );
     Application.ProcessMessages;
end;

// Este prodecimiento nos sirve para invocarse de unidades externas a la interfaz
// el cual a su vez env�a el mensaje al EscribeErro utilizando el cliente actual.
procedure TdmInterfase.EscribeErrorExterno( const sMensaje: string );
begin
     EscribeError( sMensaje, FEmpleado, FLinea );
end;

procedure TdmInterfase.SetNuevosEventos;
begin
     with dmCliente.cdsEmpleado do
     begin
          BeforePost := self.cdsEmpleadoBeforePost;
          AlCrearCampos := self.cdsEmpleadoAlCrearCampos;
          OnReconcileError := self.cdsEmpleadoReconcileError;
     end;
     with dmRecursos.cdsEditHisKardex do
     begin
          OnReconcileError := self.cdsReconcileError;
     end;
end;

procedure TdmInterfase.cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if PK_Violation( E ) and
        ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
        EscribeError( 'No se pudo registrar el Alta, el n�mero de empleado ya existe.', FEmpleado, FLinea )
     else
        EscribeError( E.Message, FEmpleado, FLinea );
     Action := raCancel;
end;

procedure TdmInterfase.cdsEmpleadoBeforePost(DataSet: TDataSet);
begin
     dmCliente.cdsEmpleadoBeforePost( DataSet );
end;

procedure TdmInterfase.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     dmCliente.cdsEmpleadoAlCrearCampos( Sender );
     dmCliente.cdsEmpleado.FieldByName( 'CB_SEGSOC' ).onValidate := self.CB_SEGSOCValidate;
end;

procedure TdmInterfase.CB_SEGSOCValidate(Sender: TField);
var
   sMensaje: String;
begin
     if not dmCliente.ValidaSEGSOC( Sender.AsString, sMensaje, FALSE ) or strLleno( sMensaje ) then
        EscribeError( sMensaje, FEmpleado, FLinea );
end;

procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     EscribeError( E.Message, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger, FLinea );
     Action := raCancel;
end;

//Renombra el archivo indicado con un formato de [nombre]_[fecha].bak }
procedure TdmInterfase.RenombraArchivo( sArchivo: string );
var
   sNombre, sRuta, sFecha, sArchivoTmp: string;
const
     BAK = '.bak';
     GUION_BAJO = '_';
begin
     if FileExists( sArchivo )then
     begin
          sNombre := ExtractFileName( sArchivo );
          sNombre := Copy( sNombre, 1, ansiPos( '.', sNombre ) - 1 );
          sRuta := ExtractFilePath( sArchivo );
          sRuta := VerificaDir( sRuta );
          sRuta := sRuta + 'Backups';
          if not DirectoryExists( sRuta ) then
             CreateDir( sRuta ) ;
          sRuta := VerificaDir( sRuta );
          sFecha := FormatDateTime( 'yyyymmdd', dmCliente.FechaDefault ) + FormatDateTime( 'hhmmss', Time );
          sArchivoTmp := sNombre + GUION_BAJO + sFecha + BAK;
          try
             RenameFile( sArchivo, sRuta + sArchivoTmp );
             EscribeBitacora( Format( 'Se renombr� el archivo %s a %s.', [ sNombre, sArchivoTmp ] ) );
          except
                on Error: Exception do
                   EscribeError( Format( 'Se gener� un error al tratar de renombrar el archivo %s.', [ sNombre ] ) );
          end;
     end
     else
         EscribeError( Format( 'El archivo ''%s'' no existe. No se renombra archivo.', [ ExtractFileName( sArchivo ) ] ) );
end;

procedure TdmInterfase.ValidaEnviar(DataSet: TZetaClientDataSet);
begin
     FHuboErrorEnviar := TRUE;
     try
        DataSet.Enviar;
        FHuboErrorEnviar := FALSE;
     except
           on Error: Exception do
           begin
                FHuboErrores := TRUE;
                EscribeError( Error.Message, FEmpleado, FLinea );
                DataSet.CancelUpdates;
           end;
     end;
end;

function TdmInterfase.ValidaBajaEmpleado( const sTipo: String ): Boolean;
begin
     with dmCliente.GetDatosEmpleadoActivo do
     begin
          Result := (dmRecursos.EsKardexRequiereActivo( sTipo ) ) or Activo;
          if not Result then
             EscribeError( 'El Empleado ya fue dado de Baja el ' + FechaCorta( Baja ) + ' No se Pueden Realizar Movimientos de Kardex.', FEmpleado, FLinea );
     end;
end;

// Se hizo este m�todo de forma local, ya que el mismo, dentro de dRecursos, activa la
// bandera de "AgregandoKardex:= TRUE;" posterior a "ConectarKardexLookups". Y En el lugar de la activaci�n
// de el boleano mencionado, se sutituye por "cdsHisKardex.Refrescar".
// Esto se hace para que cada vez que se agregue un movimiento, no nos impida posteriormente, cargar a
// cdsEditHisKardex los datos de cdsHisKardex. Ya que si est� bandera estuviera activada, en lugar de los
// registros de cdsHisKardex, nos trae valores vac�os, como si fuera hacer un alta.
procedure TdmInterfase.SetNuevoKardex(const sTipo: String);
begin
     with dmRecursos do
     begin
          ConectarKardexLookups; // Se ocupa para el create simple lookup de cdsEditHisKardex
          cdsHisKardex.Refrescar;
          with cdsEditHisKardex do
          begin
               Conectar;
               Append;
               FieldByName( 'CB_TIPO' ).AsString:= sTipo;
          end;
     end;
end;

// Regresa el texto de la consulta indicada
function TdmInterfase.GetSQLScript(iConsulta: Integer): String;
begin
     case iConsulta of
          K_OBTEN_CODIGO: Result :=  'select %s from %s where %s = %d';
          K_CONSULTA_PERIODOS: Result := 'select PE_YEAR, PE_NUMERO from PERIODO '+
                                         'where '+
                                         '''%0:s'' between PE_FEC_INI and PE_FEC_FIN and PE_TIPO=%1:d and PE_NUMERO < %2:d order by PE_NUMERO';
          K_BUSCA_EMPLEADO: Result := 'select coalesce( MAX( CB_CODIGO ),0 ) + 1 as CB_CODIGO from COLABORA';
          K_BUSCA_PUESTO : Result := 'select PU_CODIGO from PUESTO where ( UPPER( PU_TEXTO ) = %s )';
          K_BUSCA_MAX_PUESTO : Result := 'select MAX( PU_NUMERO ) + 1 as PU_NUMERO from PUESTO';
          K_BUSCA_MAX_NIVEL4 : Result := 'select MAX( TB_NUMERO ) + 1 as TB_NUMERO from NIVEL4';
          K_BUSCA_NIVEL4 : result := 'select TB_CODIGO from NIVEL4 where ( UPPER( TB_TEXTO ) = %s )';
     else
          Result := VACIO;
     end;
end;

// Procesa archivo.
procedure TdmInterfase.Procesa;
begin
     dmCliente.cdsTPeriodos.Refrescar;
     FEmpleado := 0;
     FLinea := 0;
     FHuboErrores := FALSE;
     FHuboCambios := FALSE;
     FProcesandoThread := FALSE;
     FHuboErrorEnviar := FALSE;
     EscribeBitacora( 'Iniciando proceso.' );
     ProcesaArchivo;
     Application.ProcessMessages;
     if FHuboErrores then
        EscribeBitacora( 'Proceso finalizado con errores.' )
     else
     begin
          if ( FHuboCambios ) then
             EscribeBitacora( 'Proceso finalizado.' )
          else
              EscribeBitacora( 'Proceso finalizado sin cambios.' );
     end;
end;

// Procesa el archivo ingresado
procedure TdmInterfase.ProcesaArchivo;
var
   lArchivosXProcesar: boolean;
   oBusca: TSearchRec;
   sRuta: string;
begin
     FFormatoFecha := ifMMDDYYYYs;
     case FTipoMovimiento of
          tmAlta:
          begin
               EscribeBitacora( '*** Inicio proceso altas ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*NewHire*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaAlta;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
               EscribeBitacora( '*** Fin proceso altas ***' );
          end;
           tmbaja:
          begin
               EscribeBitacora( '*** Inicio proceso bajas ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*Termination*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaBaja;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
               EscribeBitacora( '*** Fin proceso Bajas ***' );
          end;
          tmReingreso:
          begin
               EscribeBitacora( '*** Inicio proceso reingreso ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.' , [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*ReHire*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaRecontrata;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
               EscribeBitacora( '*** Fin proceso reingreso ***' );
          end;
          TmCambioPuesto:
          begin
          EscribeBitacora( '*** Inicio proceso cambio de puesto ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*Position*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaCambioDePuesto;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
               EscribeBitacora( '*** Fin proceso cambio de puesto ***' );
          end;
          TmCambioClasificacion:
          begin
               EscribeBitacora( '*** Inicio proceso cambio de clasificaci�n ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*Classification*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaCambioDeClasificacion;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
                   EscribeBitacora( '*** Fin proceso cambio de clasificaci�n ***' );
          end;
          tmCambioTurno:
          begin
               EscribeBitacora( '*** Inicio proceso cambio de turno ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*Shift*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaCambioDeTurno;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
               EscribeBitacora( '*** Fin proceso cambio de turno ***' );
          end;
          tmCambioSalario:
          begin
               EscribeBitacora( '*** Inicio proceso cambio de salario ***' );
               EscribeBitacora( Format( 'Buscando archivos en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*Salary*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaCambioDeSalario;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
               EscribeBitacora( '*** Fin proceso cambio de salario ***' );
          end;
          tmCambioNiveles:
          begin
               EscribeBitacora( '*** Inicio proceso cambios de niveles ***' );
               EscribeBitacora( Format( 'Buscando archivos a procesar en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*level*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaCambiodeNiveles;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
                   EscribeBitacora( '*** Fin proceso cambios de niveles ***' );
          end;
          tmCambiosGenerales:
          begin
               EscribeBitacora( '*** Inicio proceso cambios de informaci�n del empleado ***' );
               EscribeBitacora( Format( 'Buscando archivos a procesar en directorio: %s.', [ VerificaDir( FArchivo ) ] ) );
               lArchivosXProcesar := FindFirst( VerificaDir( FArchivo ) + '*update*.txt', faArchive, oBusca ) = 0;
               if lArchivosXProcesar then
               begin
                    sRuta := FArchivo;
                    repeat
                          FLinea := 0;
                          FNombreArchivo := VerificaDir( sRuta ) + oBusca.Name;
                          EscribeBitacora( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Inicio proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          FArchivo := FNombreArchivo;
                          Application.ProcessMessages;
                          ProcesaCambiosGenerales;
                          EscribeBitacora( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ) );
                          EscribeError( Format( 'Fin proceso de archivo: %s.', [ oBusca.Name ] ), 0, 0, False );
                          RenombraArchivo( FNombreArchivo );
                          Application.ProcessMessages;
                    until FindNext( oBusca ) <> 0;
               end
               else
                   EscribeBitacora( 'No se encontraron archivos de la empresa seleccionada.' );
                   EscribeBitacora( '*** Fin proceso cambios de informaci�n del empleado  ***' );
          end;
     end;
end;

function tdmInterfase.BuscaPuesto( sTexto: string ): String;
begin
     Result := VACIO;
     if strLleno( sTexto ) then
     begin
          with dmConsultas do
          begin
               SQLText := Format( GetSQLScript( K_BUSCA_PUESTO ), [ EntreComillas( ansiUpperCase( sTexto ) ) ] );
               try
                  cdsQueryGeneral.Refrescar;
                  if ( cdsQueryGeneral.RecordCount > 0 ) then
                     Result := cdsQueryGeneral.FieldByName( 'PU_CODIGO' ).AsString;
               except
                     on Error : Exception do
                        EscribeError( Format( 'Error al consultar empleados en base de datos (Puestos). [%s]', [ Error.Message ] ), 0, FLinea );
               end;
          end;
     end;
end;

function tdmInterfase.BuscaNivel4( sTexto: string ): String;
begin
     Result := VACIO;
     if strLleno( sTexto ) then
     begin
          with dmConsultas do
          begin
               SQLText := Format( GetSQLScript( K_BUSCA_NIVEL4 ), [ EntreComillas( ansiUpperCase( sTexto ) ) ] );
               try
                  cdsQueryGeneral.Refrescar;
                  if ( cdsQueryGeneral.RecordCount > 0 ) then
                     Result := cdsQueryGeneral.FieldByName( 'TB_CODIGO' ).AsString;
               except
                     on Error : Exception do
                        EscribeError( Format( 'Error al consultar empleados en base de datos (Nivel #4). [%s]', [ Error.Message ] ), 0, FLinea );
               end;
          end;
     end;
end;

function tdmInterfase.CounsultaMaxNumero: integer;
begin
     Result := 0;
     with dmConsultas do
     begin
          SQLText := GetSQLScript( K_BUSCA_MAX_PUESTO );
          try
             cdsQueryGeneral.Refrescar;
             if ( cdsQueryGeneral.RecordCount > 0 ) then
                Result := cdsQueryGeneral.FieldByName( 'PU_NUMERO' ).AsInteger;
          except
                on Error : Exception do
                   EscribeError( Format( 'Error al consultar empleados en base de datos (Puestos). [%s]', [ Error.Message ] ), 0, FLinea );
          end;
     end;
end;

function tdmInterfase.CounsultaMaxNumeroNivel4: integer;
begin
     Result := 0;
     with dmConsultas do
     begin
          SQLText := GetSQLScript( K_BUSCA_MAX_NIVEL4 );
          try
             cdsQueryGeneral.Refrescar;
             if ( cdsQueryGeneral.RecordCount > 0 ) then
                Result := cdsQueryGeneral.FieldByName( 'TB_NUMERO' ).AsInteger;
          except
                on Error : Exception do
                   EscribeError( Format( 'Error al consultar empleados en base de datos (Nivel #4). [%s]', [ Error.Message ] ), 0, FLinea );
          end;
     end;
end;

function tdmInterfase.InsertaPuesto( sTexto: string ): string;
var
   iCounter : integer;
   sContador : string ;
begin
     iCounter := CounsultaMaxNumero;
     scontador := intToStr( iCounter );
     {$ifdef ANTES}
     result := Trim( copy( ( scontador + sTexto ),1, 6) );
     {$else}
     result := sContador;
     {$endif}
     with dmCatalogos.cdsPuestos do
     begin
          Refrescar;
          Append;
          If( icounter > 0 )then
          begin
               sContador := intToStr( iCounter );
          end;
          FieldByName( 'PU_CODIGO' ).AsString := result;
          FieldByName( 'PU_TEXTO' ).AsString := sTexto;
          fieldByName( 'PU_NUMERO' ).AsInteger := iCounter;
          Validaenviar( dmCatalogos.cdsPuestos );
     end;
end;

function tdmInterfase.InsertaNivel4( sTexto:String ): string;
var
   iCounter : integer;
   sContador : string ;
begin
     icounter := CounsultaMaxNumeroNivel4;
     scontador := intToStr(iCounter);
     {$ifdef ANTES}
     result := Trim( copy( ( scontador + sTexto ), 1, 6) );
     {$else}
     result := sContador;
     {$endif}
     with dmtablas.cdsNivel4 do
     begin
          Refrescar;
          Append;
          If( icounter > 0 )then
          begin
               sContador := intToStr( iCounter );
          end;
          FieldByName( 'TB_CODIGO' ).AsString := result;
          FieldByName( 'TB_TEXTO' ).AsString := sTexto;
          fieldByName( 'TB_NUMERO' ).AsInteger := icounter;
          Validaenviar( dmTablas.cdsNivel4 );
     end;
end;

function TdmInterfase.BuscaEmpleado( sNumero: string ): integer;
begin
     Result := 0;
     with dmConsultas do
     begin
          SQLText := Format( GetSQLScript( K_BUSCA_EMPLEADO ), [ EntreComillas( sNumero ) ] );
          try
             cdsQueryGeneral.Refrescar;
             if ( cdsQueryGeneral.RecordCount > 0 ) then
                Result := cdsQueryGeneral.FieldByName( 'CB_CODIGO' ).AsInteger;
          except
                on Error : Exception do
                   EscribeError( Format( 'Error al consultar empleados en base de datos. [%s]', [ Error.Message ] ), 0, FLinea );
          end;
     end;
end;

function TdmInterfase.BuscaCodigo( sFiltro, sTexto, sCodigo, sTabla, sMensajeError: string ): string;
var
   iEmpleado: integer;
begin
     Result := VACIO;
     if strLLeno( sTexto ) then
     begin
          if TryStrtoInt( sTexto ,iEmpleado ) then
          begin
               with dmConsultas do
               begin
                    SQLText := Format( GetSQLScript( K_OBTEN_CODIGO ), [ sCodigo,  sTabla, sFiltro, iEmpleado ] );
                    try
                       cdsQueryGeneral.Refrescar;
                       if ( cdsQueryGeneral.RecordCount > 0 ) then
                          Result := cdsQueryGeneral.FieldByName( sCodigo ).AsString;
                    except
                          on Error : Exception do
                             EscribeError( Format( 'Error al consultar %s en base de datos. [%s]', [ sTabla, Error.Message ] ), 0, FLinea );
                    end;
               end;
          end;
     end;
end;

procedure tdmInterfase.ProcesaCambiosGenerales;
var
   i: Integer;
   sDatos: String;
   bHuboCambio :Boolean;
begin
     bHuboCambio := false;
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     Inc( FLinea );
                     FRenglon.Clear;

                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos + '"';

                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe' ), 0 );
                     if ( dmCliente.SetEmpleadoNumero( FEmpleado ) ) then
                     begin
                          if ZAccesosMgr.RevisaCualquiera( D_EMP_DATOS ) then
                          begin
                               with (dmCliente.cdsEmpleado ) do
                               begin
                                    try
                                       if ( State = dsBrowse ) then
                                          edit;
                                       //boleanos
                                       if ( Trim( copy( FRenglon[ 146 ], 1, 1) ) <> VACIO )then
                                       begin
                                            if( ansiUpperCase(Trim( Copy( FRenglon[ 146 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                                FieldByName( 'CB_DISCAPA').AsString := K_GLOBAL_SI
                                            else
                                                FieldByName( 'CB_DISCAPA').AsString := K_GLOBAL_NO;
                                            EscribeBitacora( 'Se registr� cambio en campo CB_DISCAPA: ' + FieldByName( 'CB_DISCAPA' ).AsString+'.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 147 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 147 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                               FieldByName( 'CB_INDIGE' ).AsString := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_INDIGE').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_INDIGE: ' + FieldByName( 'CB_INDIGE' ).AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim( FRenglon[ 41 ] ) <> VACIO)then
                                       begin
                                            if ( ansiUpperCase( Trim( FRenglon[ 41 ] ) ) = 'HOURLY' ) then
                                            begin
                                                 FieldByName( 'CB_CHECA'  ).AsString := K_GLOBAL_SI;
                                                 EscribeBitacora( 'Se registr� cambio en campo CB_CHECA: ' + FieldByName( 'CB_CHECA' ).AsString+'.', FEmpleado, FLinea );
                                                 bHuboCambio := true;
                                            end
                                            else if ( ansiUpperCase( Trim( FRenglon[ 41 ] ) ) = 'SALARY' ) then
                                                 begin
                                                      FieldByName( 'CB_CHECA').AsString := K_GLOBAL_NO;
                                                      EscribeBitacora( 'Se registr� cambio en campo CB_CHECA: ' + FieldByName( 'CB_CHECA' ).AsString+'.', FEmpleado, FLinea );
                                                      bHuboCambio := true;
                                                 end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 67 ],1,1)) <> VACIO )then
                                       begin
                                            if ( ansiUpperCase(Trim( Copy( FRenglon[ 67 ], 1, 1 )) ) = K_GLOBAL_SI ) then
                                               FieldByName( 'CB_EST_HOY').AsString := K_GLOBAL_SI
                                            else
                                                FieldByName( 'CB_EST_HOY').AsString := K_GLOBAL_NO;
                                            EscribeBitacora( 'Se registr� cambio en campo CB_EST_HOY: ' + FieldByName( 'CB_EST_HOY').AsString+'.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 84 ],1,1)) <> VACIO)then
                                       begin
                                          if ( ansiUpperCase(Trim( Copy( FRenglon[ 84 ], 1, 1 )) ) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_1').AsString := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_1').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_1: ' + FieldByName( 'CB_G_LOG_1').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 85 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 85 ], 1, 1) ) ) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_2').AsString  := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_2').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_2: ' + FieldByName( 'CB_G_LOG_2').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 86 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 86 ], 1, 1 ) ) )= K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_3').AsString  := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_3').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_3: ' + FieldByName( 'CB_G_LOG_3').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 132 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 132 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_4').AsString  := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_4').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_4: ' + FieldByName( 'CB_G_LOG_4').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 133 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 133 ], 1, 1 )) ) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_5').AsString := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_5').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_5: ' + FieldByName( 'CB_G_LOG_5').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 134 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 134 ], 1, 1 )) ) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_6').AsString  := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_6').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_6: ' + FieldByName( 'CB_G_LOG_6').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 135 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 135 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_G_LOG_7').AsString := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_7').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_7: ' + FieldByName( 'CB_G_LOG_7').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 136 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 136 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                              FieldByName( 'CB_G_LOG_8').AsString  := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_G_LOG_8').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_LOG_8: ' + FieldByName( 'CB_G_LOG_8').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 49 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 49 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_PASAPOR').AsString  := K_GLOBAL_SI
                                          else
                                              FieldByName( 'CB_PASAPOR').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_PASAPOR: ' + FieldByName( 'CB_PASAPOR').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 149 ],1,1)) <> VACIO)then
                                       begin
                                          if(  ansiUpperCase(Trim( Copy( FRenglon[ 149 ], 1, 1 ) )) = K_GLOBAL_SI ) then
                                             FieldByName( 'CB_EMPLEO' ).AsString  := K_GLOBAL_SI
                                          else
                                             FieldByName( 'CB_EMPLEO').AsString := K_GLOBAL_NO;
                                          EscribeBitacora( 'Se registr� cambio en campo CB_EMPLEO: ' + FieldByName( 'CB_EMPLEO').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;

                                       //validar que sean numeros y pasarlos como numericos (el dato entero)
                                       //Campos Enteros
                                       if ( Trim(FRenglon[ 35 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_INFTIPO' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 35 ]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_INFTIPO: ' + FieldByName( 'CB_INFTIPO').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 37]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_INFTASA' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 37]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_INFTASA: ' + FieldByName( 'CB_INFTASA').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 38]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_INF_OLD' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 38]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_INF_OLD: ' + FieldByName( 'CB_INF_OLD').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 81 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_1' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 81]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_1: ' + FieldByName( 'CB_G_NUM_1').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 83 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_2' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 83]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_2: ' + FieldByName( 'CB_G_NUM_2').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 84 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_3' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 84]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_3: ' + FieldByName( 'CB_G_NUM_3').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 117 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_4' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 117]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_4: ' + FieldByName( 'CB_G_NUM_4').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 118 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_5' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 118]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_5: ' + FieldByName( 'CB_G_NUM_5').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 119 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_6' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 119]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_6: ' + FieldByName( 'CB_G_NUM_6').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 120 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_7' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 120]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_7: ' + FieldByName( 'CB_G_NUM_7').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 121 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_8' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 121]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_8: ' + FieldByName( 'CB_G_NUM_8').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       {if ( Trim(FRenglon[ 122 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM_9' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 122]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM_9: ' + FieldByName( 'CB_G_NUM_9').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;}
                                       if ( Trim(FRenglon[ 123 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM10' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 123]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM10: ' + FieldByName( 'CB_G_NUM10').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 124 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM11' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 124]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM11: ' + FieldByName( 'CB_G_NUM11').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 125 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM12' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 125]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM12: ' + FieldByName( 'CB_G_NUM12').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 126 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM13' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 126]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM13: ' + FieldByName( 'CB_G_NUM13').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 127 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM14' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 127]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM14: ' + FieldByName( 'CB_G_NUM14').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 128 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM15' ).AsFloat:= StrToFloatDef(Trim(FRenglon[ 128]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM15: ' + FieldByName( 'CB_G_NUM15').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 129 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM16' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 129]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM16: ' + FieldByName( 'CB_G_NUM16').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 130 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM17' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 130]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM17: ' + FieldByName( 'CB_G_NUM17').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 131 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_G_NUM18' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 131]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_G_NUM18: ' + FieldByName( 'CB_G_NUM18').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 160 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_TDISCAP' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 160]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_TDISCAP: ' + FieldByName( 'CB_TDISCAP').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 162 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_TESCUEL' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 162]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_TESCUEL: ' + FieldByName( 'CB_TESCUEL').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 163 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_TITULO' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 163]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_TITULO: ' + FieldByName( 'CB_TITULO').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;
                                       if ( Trim(FRenglon[ 164 ]) <> Vacio )then
                                       begin
                                          FieldByName( 'CB_YTITULO' ).AsFloat := StrToFloatDef(Trim(FRenglon[ 164]),0);
                                          EscribeBitacora( 'Se registr� cambio en campo CB_YTITULO: ' + FieldByName( 'CB_YTITULO').AsString+'.', FEmpleado, FLinea );
                                          bHuboCambio := true;
                                       end;

                                       //campos tipo fecha validar que el resultado de getfechaimportada no sea nulldatetime
                                       if ( Trim(copy(FRenglon[ 157 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 157 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_INF_ANT' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 157 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_INF_ANT: ' + FechaCorta(FieldByName( 'CB_INF_ANT').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 64 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 64 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_FEC_RES' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 64 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_FEC_RES: ' + FechaCorta(FieldByName( 'CB_FEC_RES').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 39 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 39 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_INF_INI' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 39 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_INF_INI: ' + FechaCorta(FieldByName( 'CB_INF_INI').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 87 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 87 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_1' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 87 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_1: ' + FechaCorta(FieldByName( 'CB_G_FEC_1').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 88 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 88 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_2' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 88 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_2: ' + FechaCorta(FieldByName( 'CB_G_FEC_2').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 89 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 89 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_3' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 89 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_3: ' + FechaCorta(FieldByName( 'CB_G_FEC_3').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 137 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 137 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                 FieldByName( 'CB_G_FEC_4' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 137 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_4: ' + FechaCorta(FieldByName( 'CB_G_FEC_4').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 138 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 138 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_5' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 138 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_5: ' + FechaCorta(FieldByName( 'CB_G_FEC_5').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 139 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 139 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_6' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 139 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_6: ' + FechaCorta(FieldByName( 'CB_G_FEC_6').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 140 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 140 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_7' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 140 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_7: ' + FechaCorta(FieldByName( 'CB_G_FEC_7').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;
                                       if ( Trim(copy(FRenglon[ 141 ],1,10)) <> Vacio )then
                                       begin
                                            if( GetFechaImportada( Trim( Copy( FRenglon[ 141 ], 1, 10 ) ), FFormatoFecha ) <> Nulldatetime) then
                                            begin
                                                FieldByName( 'CB_G_FEC_8' ).asDateTime :=  GetFechaImportada( Trim( Copy( FRenglon[ 141 ], 1, 10 ) ), FFormatoFecha );
                                                EscribeBitacora( 'Se registr� cambio en campo CB_G_FEC_8: ' + FechaCorta(FieldByName( 'CB_G_FEC_8').asDateTime)+'.', FEmpleado, FLinea );
                                                bHuboCambio := true;
                                            end;
                                       end;

                                       //campos adicionales
                                       if ( Trim(copy(FRenglon[ 165 ],1,30)) <> Vacio )then
                                       begin
                                            FieldByName( 'CB_CTA_GAS' ).AsString := Trim( Copy( FRenglon[ 165 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CTA_GAS: ' + FieldByName( 'CB_CTA_GAS').AsString+'.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 166 ],1,30)) <> Vacio )then
                                       begin
                                            FieldByName( 'CB_CTA_VAL' ).AsString := Trim( Copy( FRenglon[ 166 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CTA_VAL: ' + FieldByName( 'CB_CTA_VAL').AsString+'.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim(copy(FRenglon[ 167 ],1,6)) <> Vacio )then
                                       begin
                                            FieldByName( 'CB_MUNICIP' ).AsString := Trim( Copy( FRenglon[ 167 ], 1, 6  ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_MUNICIP: ' + FieldByName( 'CB_MUNICIP').AsString+'.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       {if (Frenglon.COunt > 168 ) then
                                       begin
                                            if ( Trim(FRenglon[ 168 ]) <> Vacio)  then
                                            begin
                                                 FieldByName( 'CB_ID_BIO' ).AsInteger := StrtointDef(Trim( Frenglon[ 168 ]),0 );
                                                 EscribeBitacora( 'Se registr� cambio en campo CB_ID_BIO: ' + FieldByName( 'CB_ID_BIO').AsString+'.', FEmpleado, FLinea );
                                                 bHuboCambio := true;
                                            end;
                                       end;}
                                       {if (Frenglon.COunt > 169 )then
                                       begin
                                          if ( Trim(copy(FRenglon[ 169 ],1,6)) <> Vacio)  then
                                          begin
                                               FieldByName( 'CB_GP_COD'  ).AsString := Trim( Copy( Frenglon[ 169 ], 1,6 ) );
                                               EscribeBitacora( 'Se registr� cambio en campo CB_GP_COD: ' + FieldByName( 'CB_GP_COD').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;}
                                       if (Frenglon.COunt > 170 )then
                                       begin
                                          if ( Trim(copy(FRenglon[ 170 ],1,30)) <> Vacio )  then
                                          begin
                                               FieldByName( 'CB_TSANGRE' ).AsString := Trim( Copy( Frenglon[ 170 ], 1,30 ) );
                                               EscribeBitacora( 'Se registr� cambio en campo CB_TSANGRE: ' + FieldByName( 'CB_TSANGRE').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;
                                       if (Frenglon.COunt > 171 )then
                                       begin
                                          if ( Trim(FRenglon[ 171 ]) <> Vacio )  then
                                          begin
                                               FieldByName( 'CB_ALERGIA' ).AsString := Trim( Copy( Frenglon[ 171 ], 1,255 ) );
                                               EscribeBitacora( 'Se registr� cambio en campo CB_ALERGIA: ' + FieldByName( 'CB_ALERGIA').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;
                                       {if (Frenglon.COunt > 172 ) then
                                       begin
                                          if ( Trim(FRenglon[ 172 ]) <> Vacio ) then
                                          begin
                                               FieldByName( 'CB_PLAZA'  ).asInteger := StrTointDef(Trim( Frenglon[ 172 ]),0 );
                                               EscribeBitacora( 'Se registr� cambio en campo CB_PLAZA: ' + FieldByName( 'CB_PLAZA').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;}
                                       if (Frenglon.COunt > 173 )then
                                       begin
                                          if ( Trim(copy(FRenglon[ 173 ],1,255)) <> Vacio ) then
                                          begin
                                               FieldByName( 'CB_E_MAIL'  ).AsString := Trim( Copy( Frenglon[ 173 ], 1,255 ) );
                                               EscribeBitacora( 'Se registr� cambio en campo CB_E_MAIL: ' + FieldByName( 'CB_E_MAIL').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;
                                       if (Frenglon.COunt > 174 ) then
                                       begin
                                          if ( Trim(copy(FRenglon[ 174 ],1,6)) <> Vacio )  then
                                          begin
                                               FieldByName( 'CB_BANCO'  ).AsString := Trim( Copy( Frenglon[ 174 ], 1,6 ) );
                                               EscribeBitacora( 'Se registr� cambio en campo  CB_BANCO: ' + FieldByName( 'CB_BANCO').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;
                                       if (Frenglon.COunt > 175 ) then
                                       begin
                                          if ( Trim(FRenglon[ 175 ]) <> Vacio ) then
                                          begin
                                               FieldByName( 'CB_REGIMEN').AsInteger := StrTointDef(Trim(Frenglon[ 175 ] ),0 );
                                               EscribeBitacora( 'Se registr� cambio en campo CB_REGIMEN: ' + FieldByName( 'CB_REGIMEN').AsString+'.', FEmpleado, FLinea );
                                               bHuboCambio := true;
                                          end;
                                       end;

                                       //Campos texto
                                       if ( Trim( copy( FRenglon[ 47 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_LUG_NAC'  ).AsString := Trim( copy( FRenglon[ 47 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_LUG_NAC: ' + FieldByName( 'CB_LUG_NAC').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 48 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_NACION'  ).AsString := Trim( copy( FRenglon[ 48 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_NACION: ' + FieldByName( 'CB_NACION').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 50 ], 1, 1 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_EDO_CIV'  ).AsString := Trim( copy( FRenglon[ 50 ], 1, 1 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_EDO_CIV: ' + FieldByName( 'CB_EDO_CIV').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 55 ], 1, 50 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_CALLE'  ).AsString := Trim( copy( FRenglon[ 55 ], 1, 50 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CALLE: ' + FieldByName( 'CB_CALLE' ).AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 56 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_COLONIA'  ).AsString := Trim( copy( FRenglon[ 56 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_COLONIA: ' + FieldByName( 'CB_COLONIA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 57 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_CIUDAD'  ).AsString := Trim( copy( FRenglon[ 57 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CIUDAD: ' + FieldByName( 'CB_CIUDAD').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 58 ], 1, 8 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_CODPOST'  ).AsString := Trim( copy( FRenglon[ 58 ], 1, 8 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CODPOST: ' + FieldByName( 'CB_CODPOST').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 59 ], 1, 2 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_ESTADO'  ).AsString := Trim( copy( FRenglon[ 59 ], 1, 2 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_ESTADO: ' + FieldByName( 'CB_ESTADO').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       {if ( Trim( copy( FRenglon[ 90 ], 1, 2 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_ENT_NAC'  ).AsString := Trim( copy( FRenglon[ 90 ], 1, 2 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_ENT_NAC: ' + FieldByName( 'CB_ENT_NAC').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;}

                                       if ( Trim( copy( FRenglon[ 61 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_TEL'  ).AsString := Trim( copy( FRenglon[ 61 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_TEL: ' + FieldByName( 'CB_TEL').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 73 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_1'  ).AsString := Trim( copy( FRenglon[ 73 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_1: ' + FieldByName( 'CB_G_TEX_1').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 74 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_2'  ).AsString := Trim( copy( FRenglon[ 74 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_2: ' + FieldByName( 'CB_G_TEX_2').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 75 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_3'  ).AsString := Trim( copy( FRenglon[ 75 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_3: ' + FieldByName( 'CB_G_TEX_3').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 76 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_4'  ).AsString := Trim( copy( FRenglon[ 76 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_4: ' + FieldByName( 'CB_G_TEX_4').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 92 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_5'  ).AsString := Trim( copy( FRenglon[ 92 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_5: ' + FieldByName( 'CB_G_TEX_5').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 93 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_6'  ).AsString := Trim( copy( FRenglon[ 93 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_6: ' + FieldByName( 'CB_G_TEX_6').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 94 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_7'  ).AsString := Trim( copy( FRenglon[ 94 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_7: ' + FieldByName( 'CB_G_TEX_7').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 95 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_8'  ).AsString := Trim( copy( FRenglon[ 95 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_8: ' + FieldByName( 'CB_G_TEX_8').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 96 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX_9'  ).AsString := Trim( copy( FRenglon[ 96 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX_9: ' + FieldByName( 'CB_G_TEX_9').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 97 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX10'  ).AsString := Trim( copy( FRenglon[ 97 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX10: ' + FieldByName( 'CB_G_TEX10').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 98 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX11'  ).AsString := Trim( copy( FRenglon[ 98 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX11: ' + FieldByName( 'CB_G_TEX11').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 99 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX12'  ).AsString := Trim( copy( FRenglon[ 99 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX12: ' + FieldByName( 'CB_G_TEX12').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 100 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX13'  ).AsString := Trim( copy( FRenglon[ 100 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX13: ' + FieldByName( 'CB_G_TEX13').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 101 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX14'  ).AsString := Trim( copy( FRenglon[ 101 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX14: ' + FieldByName( 'CB_G_TEX14').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 102 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX15'  ).AsString := Trim( copy( FRenglon[ 102 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX15: ' + FieldByName( 'CB_G_TEX15').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 103 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX16'  ).AsString := Trim( copy( FRenglon[ 103 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX16: ' + FieldByName( 'CB_G_TEX16').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 104 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX17'  ).AsString := Trim( copy( FRenglon[ 104 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX17: ' + FieldByName( 'CB_G_TEX17').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 105 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX18'  ).AsString := Trim( copy( FRenglon[ 105 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX18: ' + FieldByName( 'CB_G_TEX18').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 106 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX19'  ).AsString := Trim( copy( FRenglon[ 106 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX19: ' + FieldByName( 'CB_G_TEX19').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 150 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX20'  ).AsString := Trim( copy( FRenglon[ 150 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX20: ' + FieldByName( 'CB_G_TEX20').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 151 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX21'  ).AsString := Trim( copy( FRenglon[ 151 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX21: ' + FieldByName( 'CB_G_TEX21').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 152 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX22'  ).AsString := Trim( copy( FRenglon[ 152 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX22: ' + FieldByName( 'CB_G_TEX22').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 153 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX23'  ).AsString := Trim( copy( FRenglon[ 153 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX23: ' + FieldByName( 'CB_G_TEX23').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 154 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TEX24'  ).AsString := Trim( copy( FRenglon[ 154 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TEX24: ' + FieldByName( 'CB_G_TEX24').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 77 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_1'  ).AsString := Trim( copy( FRenglon[ 77 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_1: ' + FieldByName( 'CB_G_TAB_1').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 78 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_2'  ).AsString := Trim( copy( FRenglon[ 78 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_2: ' + FieldByName( 'CB_G_TAB_2').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 79 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_3'  ).AsString := Trim( copy( FRenglon[ 79 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_3: ' + FieldByName( 'CB_G_TAB_3').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;

                                       if ( Trim( copy( FRenglon[ 80 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_4'  ).AsString := Trim( copy( FRenglon[ 80 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_4: ' + FieldByName( 'CB_G_TAB_4').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 107 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_5'  ).AsString := Trim( copy( FRenglon[ 107 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_5: ' + FieldByName( 'CB_G_TAB_5').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 108 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_6'  ).AsString := Trim( copy( FRenglon[ 108 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_6: ' + FieldByName( 'CB_G_TAB_6').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 109 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_7'  ).AsString := Trim( copy( FRenglon[ 109 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_7: ' + FieldByName( 'CB_G_TAB_7').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 110 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_8'  ).AsString := Trim( copy( FRenglon[ 110 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_8: ' + FieldByName( 'CB_G_TAB_8').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 111 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB_9'  ).AsString := Trim( copy( FRenglon[ 111 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB_9: ' + FieldByName( 'CB_G_TAB_9').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 112 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB10'  ).AsString := Trim( copy( FRenglon[ 112 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB10: ' + FieldByName( 'CB_G_TAB10').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 113 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB11'  ).AsString := Trim( copy( FRenglon[ 113 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB11: ' + FieldByName( 'CB_G_TAB11').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 114 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB12'  ).AsString := Trim( copy( FRenglon[ 114 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB12: ' + FieldByName( 'CB_G_TAB12').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 115 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB13'  ).AsString := Trim( copy( FRenglon[ 115 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB13: ' + FieldByName( 'CB_G_TAB13').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 116 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_G_TAB14'  ).AsString := Trim( copy( FRenglon[ 116 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_G_TAB14: ' + FieldByName( 'CB_G_TAB14').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 158 ], 1, 10 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_NUM_EXT'  ).AsString := Trim( copy( FRenglon[ 158 ], 1, 10 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_NUM_EXT: ' + FieldByName( 'CB_NUM_EXT').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 54 ], 1, 1 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_MED_TRA'  ).AsString := Trim( copy( FRenglon[ 54 ], 1, 1 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_MED_TRA: ' + FieldByName( 'CB_MED_TRA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 62 ], 1, 1 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_VIVECON'  ).AsString := Trim( copy( FRenglon[ 62 ], 1, 1 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_VIVECON: ' + FieldByName( 'CB_VIVECON').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 63 ], 1, 2 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_VIVEEN'  ).AsString := Trim( copy( FRenglon[ 63 ], 1, 2 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_VIVEEN: ' + FieldByName( 'CB_VIVEEN').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 69 ], 1, 1 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_HABLA'  ).AsString := Trim( copy( FRenglon[ 69 ], 1, 1 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_HABLA: ' + FieldByName( 'CB_HABLA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 40 ], 1, 1 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_CREDENC'  ).AsString := Trim( copy( FRenglon[ 40 ], 1, 1 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CREDENC: ' + FieldByName( 'CB_CREDENC').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 32 ], 1, 1 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_ZONA_GE'  ).AsString := Trim( copy( FRenglon[ 32 ], 1, 1 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_ZONA_GE: ' + FieldByName( 'CB_ZONA_GE').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 65 ], 1, 2 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_ESTUDIO'  ).AsString := Trim( copy( FRenglon[ 65 ], 1, 2 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_ESTUDIO: ' + FieldByName( 'CB_ESTUDIO').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 43 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_NIVEL0'  ).AsString := Trim( copy( FRenglon[ 43 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_NIVEL0: ' + FieldByName( 'CB_NIVEL0').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 8 ], 1, 3 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_CLINICA'  ).AsString := Trim( copy( FRenglon[ 8 ], 1, 3 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CLINICA: ' + FieldByName( 'CB_CLINICA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 91 ], 1, 6 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_COD_COL'  ).AsString := Trim( copy( FRenglon[ 91 ], 1, 6 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_COD_COL: ' + FieldByName( 'CB_COD_COL').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 42 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_BAN_ELE'  ).AsString := Trim( copy( FRenglon[ 42 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_BAN_ELE: ' + FieldByName( 'CB_BAN_ELE').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 68 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_EST_HOR'  ).AsString := Trim( copy( FRenglon[ 68 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_EST_HOR: ' + FieldByName( 'CB_EST_HOR').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 70 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_IDIOMA'  ).AsString := Trim( copy( FRenglon[ 70 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_IDIOMA: ' + FieldByName( 'CB_IDIOMA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 51 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_LA_MAT'  ).AsString := Trim( copy( FRenglon[ 61 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_LA_MAT: ' + FieldByName( 'CB_LA_MAT').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 72 ], 1, 100 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_EXPERIE'  ).AsString := Trim( copy( FRenglon[ 72 ], 1, 100 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_EXPERIE: ' + FieldByName( 'CB_EXPERIE').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 71 ], 1, 100 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_MAQUINA'  ).AsString := Trim( copy( FRenglon[ 71 ], 1, 100 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_MAQUINA: ' + FieldByName( 'CB_MAQUINA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 66 ], 1, 40 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_CARRERA'  ).AsString := Trim( copy( FRenglon[ 66 ], 1, 40 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_CARRERA: ' + FieldByName( 'CB_CARRERA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 142 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_SUB_CTA'  ).AsString := Trim( copy( FRenglon[ 142 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_SUB_CTA: ' + FieldByName( 'CB_SUB_CTA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 36 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_INFCRED'  ).AsString := Trim( copy( FRenglon[ 36 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_INFCRED: ' + FieldByName( 'CB_INFCRED').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 148 ], 1, 30 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_FONACOT'  ).AsString := Trim( copy( FRenglon[ 148 ], 1, 30 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_FONACOT: ' + FieldByName( 'CB_FONACOT').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 159 ], 1, 10 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_NUM_INT'  ).AsString := Trim( copy( FRenglon[ 159 ], 1, 10 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_NUM_INT: ' + FieldByName( 'CB_NUM_INT').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       if ( Trim( copy( FRenglon[ 161 ], 1, 40 ) ) <> Vacio ) then
                                       begin
                                            FieldByName( 'CB_ESCUELA'  ).AsString := Trim( copy( FRenglon[ 161 ], 1, 40 ) );
                                            EscribeBitacora( 'Se registr� cambio en campo CB_ESCUELA: ' + FieldByName( 'CB_ESCUELA').AsString + '.', FEmpleado, FLinea );
                                            bHuboCambio := true;
                                       end;
                                       ValidaEnviar( dmCliente.cdsEmpleado );
                                       if ( bHuboCambio = TRUE )then
                                       begin
                                            FhuboCambios := true;
                                       end;
                                    except
                                          on Error : Exception do
                                             EscribeError( Format( 'Error al procesar cambios : %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                    end;
                               end;
                          end
                          else // Derechos de acceso
                          begin
                               EscribeError( 'Usuario sin derecho en Sistema TRESS para generar cambios de datos del empleado.', FEmpleado, FLinea );
                          end;
                     end
                     else // SetEmpleadoNumero
                     begin
                          EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                     end;
                end;
             finally
                    freeandnil(Frenglon);
             end;
        end;
     finally
             freeandnil(FDatos);
     end;
end;

procedure TdmInterfase.ProcesaCambiodeNiveles;
var
   i,VarValue,iMaxNivel:integer ;
   sComentarios, sNivel, sDatos, Nivel4Codigo, ErrorNivel: String;
   sValor, sTemp: string;
   dFechaEfectiva: TDate;
   ProcesoNIvel: boolean;
begin
     iMaxNivel := Global.NumNiveles;
     ProcesoNivel := True;
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     sNivel := VACIO;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     ProcesoNivel := True;
                     ErrorNivel := VACIO;

                     Inc( FLinea );
                     FRenglon.Clear;

                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos + '"';

                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El empleado no existe.' ), 0);
                     if ( dmCliente.SetEmpleadoNumero( FEmpleado ) ) then
                     begin
                          if strLleno( Trim( Copy( FRenglon[ 1 ] , 1, 2 ) ) ) then
                          begin
                               if ValidaBajaEmpleado( K_T_AREA ) then
                               begin
                                    if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_CAMBIO_AREA )then
                                    begin
                                         if TryStrtoInt( Trim(FRenglon[ 1 ] ), VarValue )then
                                         begin
                                              try
                                                 Self.SetNuevoKardex( K_T_AREA );
                                                 with dmRecursos.cdsEditHisKardex do
                                                 begin
                                                      dFechaEfectiva := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                                                      sComentarios :=  Trim( Copy( FRenglon[ 5 ], 1, 255 ) );
                                                      case strToInt( Trim( Copy( FRenglon[ 1 ] , 1, 2 ) ) ) of
                                                           11:
                                                           begin
                                                                if strLleno( Trim( Copy( FRenglon[ 3 ], 1, 2 ) ) ) then
                                                                   FieldByName( 'CB_NIVEl1' ).AsString := ansiUpperCase( Trim( Copy( FRenglon[ 3 ], 1, 2 ) ) )
                                                                else
                                                                begin
                                                                     ErrorNivel := 'C�digo de nivel #1 no puede quedar vac�o.';
                                                                     ProcesoNivel := False;
                                                                end;
                                                           end;
                                                           12:
                                                           begin
                                                                sValor := Trim( FRenglon[ 3 ] );
                                                                sNivel := VACIO;
                                                                if strLleno( sValor ) then
                                                                begin
                                                                     if Length( sValor ) > 8 then
                                                                     begin
                                                                          sTemp := Copy( sValor, 8, Length( sValor ) );
                                                                          if ( ansiPos( ' ', sTemp ) > 0 ) then
                                                                             sNivel := Trim( Copy( sTemp, 1, ansiPos( ' ', sTemp ) - 1 ) )
                                                                          else
                                                                              sNivel := Copy( sTemp, 1, 6 );
                                                                          sNivel := Copy( sNivel, 1, 6 );
                                                                     end
                                                                     else
                                                                     begin
                                                                          sTemp := VACIO;
                                                                     end;
                                                                end;
                                                                if strLleno( sNivel ) then
                                                                   FieldByName( 'CB_NIVEL2' ).AsString := sNivel
                                                                else
                                                                begin
                                                                     ErrorNivel := 'C�digo de nivel #2 no puede quedar vac�o.';
                                                                     ProcesoNivel := False;
                                                                end;
                                                           end;
                                                           13:
                                                           begin
                                                                if strLleno( Trim( Copy( FRenglon[ 3 ], 1, 6 ) ) ) then
                                                                   FieldByName( 'CB_NIVEl3' ).AsString := ansiUpperCase( Trim( Copy( FRenglon[ 3 ], 1, 6 ) ) )
                                                                else
                                                                begin
                                                                     ErrorNivel := 'C�digo de nivel #3 no puede quedar vac�o.';
                                                                     ProcesoNivel := False;
                                                                end;
                                                           end;
                                                           14:
                                                           begin
                                                                if strLleno( Trim( Copy( FRenglon[ 3 ], 1, 30 ) ) ) then
                                                                begin
                                                                     Nivel4Codigo := BuscaNivel4( Trim( Copy( FRenglon[ 3 ], 1, 30 ) ) );
                                                                     if ( Nivel4Codigo = '' ) then
                                                                     begin
                                                                          Nivel4Codigo := InsertaNivel4( Trim( Copy(FRenglon[ 3 ], 1, 30 ) ) );
                                                                     end;
                                                                     FieldByName( 'CB_NIVEL4' ).AsString := Nivel4Codigo;
                                                                end
                                                                else
                                                                begin
                                                                     ErrorNivel := 'C�digo de nivel #4 no puede quedar vac�o.';
                                                                     ProcesoNivel := False;
                                                                end;
                                                           end;
                                                           15:
                                                           begin
                                                                if ( imaxNivel > 4 ) then
                                                                begin
                                                                     if strLleno( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) ) then
                                                                     begin
                                                                          FieldByName( 'CB_NIVEl5' ).AsString := ansiUpperCase( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) );
                                                                     end
                                                                     else
                                                                     begin
                                                                          ErrorNivel := 'C�digo de nivel #5 no puede quedar vac�o.';
                                                                          ProcesoNivel := false;
                                                                     end;
                                                                end
                                                                else
                                                                begin
                                                                     ErrorNivel := 'No se puede realizar el proceso para el Nivel #5.';
                                                                     ProcesoNivel := false;
                                                                end;
                                                           end;
                                                           16:
                                                           begin
                                                                if ( imaxNivel > 5 ) then
                                                                begin
                                                                     if strLleno( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) ) then
                                                                     begin
                                                                          FieldByName( 'CB_NIVEL6' ).AsString := ansiUpperCase( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) );
                                                                     end
                                                                     else
                                                                     begin
                                                                          ErrorNivel := 'C�digo de nivel #6 no puede quedar vac�o.';
                                                                          ProcesoNivel := false;
                                                                     end;
                                                                end
                                                                else
                                                                begin
                                                                     ErrorNivel := 'No se puede realizar el proceso para el Nivel #6.';
                                                                     ProcesoNivel := false;
                                                                end;
                                                           end;
                                                           17:
                                                           begin
                                                                if ( imaxNivel > 6 ) then
                                                                begin
                                                                     if strLleno( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) ) then
                                                                     begin
                                                                          FieldByName( 'CB_NIVEL7' ).AsString := ansiUpperCase( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) );
                                                                     end
                                                                     else
                                                                     begin
                                                                          ErrorNivel := 'C�digo de nivel #7 no puede quedar vac�o.';
                                                                          ProcesoNivel := false;
                                                                     end;
                                                                end
                                                                else
                                                                begin
                                                                     ErrorNivel := 'No se puede realizar el proceso para el Nivel #7.';
                                                                     ProcesoNivel := false;
                                                                end;
                                                           end;
                                                           18:
                                                           begin
                                                                if ( imaxNivel > 7 ) then
                                                                begin
                                                                     if strLleno( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) ) then
                                                                     begin
                                                                          FieldByName( 'CB_NIVEL8' ).AsString := ansiUpperCase( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) );
                                                                     end
                                                                     else
                                                                     begin
                                                                          ErrorNivel := 'C�digo de nivel #8 no puede quedar vac�o.';
                                                                          ProcesoNivel := false;
                                                                     end;
                                                                end
                                                                else
                                                                begin
                                                                     ErrorNivel := 'No se puede realizar el proceso para el Nivel #8.';
                                                                     ProcesoNivel := false;
                                                                end;
                                                           end;
                                                           19:
                                                           begin
                                                                if ( imaxNivel > 8 ) then
                                                                begin
                                                                     if strLleno( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) ) then
                                                                     begin
                                                                          FieldByName( 'CB_NIVEL9' ).AsString := ansiUpperCase( Trim( Copy(FRenglon[ 3 ], 1, 6 ) ) );
                                                                     end
                                                                     else
                                                                     begin
                                                                          ErrorNivel := 'C�digo de nivel #9 no puede quedar vac�o.';
                                                                          ProcesoNivel := false;
                                                                     end;
                                                                end
                                                                else
                                                                begin
                                                                     ErrorNivel := 'No se puede realizar el proceso para el Nivel #9.';
                                                                     ProcesoNivel := false;
                                                                end;
                                                           end;
                                                      else
                                                           ProcesoNivel := false;
                                                           ErrorNivel := 'No se puede realizar el cambio de nivel. C�digos correctos [11-19].';
                                                      end;
                                                      FieldByName( 'CB_FECHA' ).AsDateTime := dFechaEfectiva;
                                                      FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_NO;
                                                      FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                      FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                                                 end;
                                                 if ( procesoNivel = True ) then
                                                 begin
                                                     ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                     if  dmRecursos.RefrescaHisKardex then
                                                     begin
                                                          FHuboCambios := TRUE;
                                                          EscribeBitacora( Format( 'Se registr� cambio de nivel, fecha: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                     end;
                                                     dmRecursos.RefrescaKardex( K_T_AREA );
                                                 end
                                                 else
                                                 begin
                                                      dmRecursos.cdsEditHisKardex.CancelUpdates;
                                                      if strLleno( ErrorNivel ) then
                                                         EscribeError( ErrorNivel,  Fempleado , FLinea );
                                                 end;
                                              except
                                                    on Error : Exception do
                                                    EscribeError( Format( 'Error al procesar cambio de nivel: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                              end;
                                         end
                                         else
                                         begin
                                             EscribeError( 'No se puede realizar el proceso. C�digos correctos [11-19].',  Fempleado , FLinea );
                                         end;
                                    end
                                    else
                                    begin
                                         EscribeBitacora( 'Usuario sin derecho en Sistema TRESS para generar cambio de Nivel.', FEmpleado, FLinea );
                                    end;
                               end;
                          end
                          else
                          begin
                               EscribeError( 'No se puede realizar el proceso, el c�digo de proceso est� vac�o.',  Fempleado , FLinea );
                          end;
                     end
                     else
                     begin
                          EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                     end;
                end;
             finally
                     freeandnil(Frenglon);
             end;
        end;
     finally
             FreeAndNIl(Fdatos);
     end;
end;

procedure TdmInterfase.ProcesaCambioDeTurno;
var
   i: Integer;
   sComentarios, sDatos: String;
   dFechaEfectiva: TDate;
begin
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                try
                   FRenglon.Delimiter := '|';
                   for i := 0 to ( FDatos.Count - 1 ) do
                   begin
                        Inc( FLinea );
                        FRenglon.Clear;

                        sDatos := FDatos[ i ];
                        sDatos := UTF8Decode( sDatos );
                        if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                           delete( sDatos, 1, 1);

                        sDatos := StrTransAll( sDatos, '|', '"|"' );
                        sDatos := ansiUpperCase( sDatos );
                        FRenglon.DelimitedText := '"' + sDatos + '"';

                        FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe.' ), 0);
                        if ( dmCliente.SetEmpleadoNumero(FEmpleado)) then
                        begin
                             if ( Copy( Trim( FRenglon[ 1 ] ), 1, 2 ) = '04' ) then
                             begin
                                  if ValidaBajaEmpleado( K_T_TURNO ) then
                                  begin
                                       if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_CAMBIO_TURNO )then
                                       begin
                                            if strLleno( Trim( FRenglon[ 3 ] ) )then
                                            begin
                                                 if ( ( ansiUpperCase( Trim( FRenglon[ 3 ] ) ) = 'T7' ) or
                                                      ( ansiUpperCase( Trim( FRenglon[ 3 ] ) ) = 'T8' ) or
                                                      ( ansiUpperCase( Trim( FRenglon[ 3 ] ) ) = 'SHIFT3' ) ) then
                                                 begin
                                                      try
                                                         dFechaEfectiva  := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                                                         sComentarios :=  Trim( Copy( FRenglon[ 5 ], 1, 255 ) );
                                                         Self.SetNuevoKardex( K_T_TURNO );
                                                         with dmRecursos.cdsEditHisKardex do
                                                         begin
                                                              FieldByName( 'CB_FECHA' ).AsDateTime := dFechaEfectiva;
                                                              FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_NO;
                                                              FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                              FieldByName( 'CB_TURNO' ).AsString := ansiUpperCase( Trim( Copy( FRenglon[ 3 ], 1, 6 ) ) );
                                                              FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                                                         end;
                                                         ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                         if dmRecursos.RefrescaHisKardex then
                                                         begin
                                                              FHuboCambios := TRUE;
                                                              EscribeBitacora( Format( 'Se registr� cambio de turno fecha: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                         end;
                                                         dmRecursos.RefrescaKardex( K_T_TURNO );
                                                      except
                                                           on Error : Exception do
                                                              EscribeError( Format( 'Error al procesar cambio de turno: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                                      end;
                                                 end
                                                 else
                                                     EscribeError( 'C�digo de turno inv�lido. No se procesa registro.', FEmpleado, FLinea );
                                            end
                                            else
                                            begin
                                                 EscribeError( 'C�digo de turno no puede quedar vac�o. No se procesa registro.', FEmpleado, FLinea );
                                            end;
                                       end
                                       else
                                           EscribeBitacora( 'Usuario sin derecho en Sistema TRESS para generar cambio de Turno.', FEmpleado, FLinea );
                                  end;
                             end
                             else
                             begin
                                  EscribeError( 'No se puede realizar el proceso. C�digo correcto [04].',  Fempleado , FLinea );
                             end;
                        end
                        else
                        begin
                             EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                        end;
                   end;
                except
                      on Error : Exception do
                         EscribeError( Format( 'Error al procesar cambio de turno: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                end;
             finally
                     freeandnil(Frenglon);
             end;
        end;
     finally
             FreeAndNIl(Fdatos);
     end;
end;

procedure TdmInterfase.ProcesaCambioDeClasificacion;
var
   i : Integer;
   sClasificacion, sComentarios, sDatos: String;
   dFechaEfectiva: TDate;
begin
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     Inc( FLinea );
                     FRenglon.Clear;

                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos + '"';

                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe' ), 0 );
                     if ( dmCliente.SetEmpleadoNumero(FEmpleado)) then
                     begin
                          if ( Copy( Trim( FRenglon[ 1 ] ), 1, 2 ) = '03' ) then
                          begin
                               if ValidaBajaEmpleado( K_T_PUESTO ) then
                               begin
                                    if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_CAMBIO_PUESTO )then
                                    begin
                                         if strLleno( Trim( FRenglon[ 3 ] ) )then
                                         begin
                                              if ( ansiUpperCase( Trim( FRenglon[ 3 ] ) ) <> 'U' ) then
                                              begin
                                                   sClasificacion := Trim( FRenglon[ 3 ] ) ;
                                                   if strLleno( sClasificacion )then
                                                   begin
                                                        try
                                                           dFechaEfectiva := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                                                           sComentarios := Trim( Copy( FRenglon[ 5 ], 1, 255 ) );
                                                           Self.SetNuevoKardex( K_T_PUESTO );
                                                           with dmRecursos.cdsEditHisKardex do
                                                           begin
                                                                FieldByName( 'CB_FECHA' ).AsDateTime := dFechaEfectiva;
                                                                FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_NO;
                                                                FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                                FieldByName( 'CB_CLASIFI' ).AsString := sClasificacion;
                                                                FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                                                           end;
                                                           ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                           if dmRecursos.RefrescaHisKardex then
                                                           begin
                                                                FHuboCambios := TRUE;
                                                                EscribeBitacora( Format( 'Se registr� cambio de clasificaci�n fecha: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                           end;
                                                           dmRecursos.RefrescaKardex( K_T_PUESTO );
                                                        except
                                                              on Error : Exception do
                                                                 EscribeError( Format( 'Error al procesar cambio de clasificaci�n: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                                        end;
                                                   end
                                                   else
                                                   begin
                                                        EscribeError( 'Clasificaci�n no puede quedar vac�o. No se procesa registro.', FEmpleado, FLinea );
                                                   end;
                                              end
                                              else
                                              begin
                                                   EscribeError( 'C�digo de clasificaci�n no puede ser U.', FEmpleado, FLinea );
                                              end;
                                         end
                                         else
                                         begin
                                              EscribeError( 'Clasificaci�n no puede quedar vac�o. No se procesa registro.', FEmpleado, FLinea );
                                         end;
                                    end
                                    else
                                        EscribeError( 'Usuario sin derecho en Sistema TRESS para generar cambio de clasificaci�n.', FEmpleado, FLinea );
                               end;
                          end
                          else
                          begin
                               EscribeError( 'No se puede realizar el proceso. C�digo correcto [03].',  FEmpleado , FLinea );
                          end;
                     end
                     else
                     begin
                          EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                     end;
                end;
             finally
                    freeandnil(Frenglon);
             end;
        end;
     finally
            FreeAndNIl(Fdatos);
     end;
end;

procedure TdmInterfase.ProcesaCambioDePuesto;
var
   i: Integer;
   sPuesto,  sComentarios, sDatos: String;
   dFechaEfectiva: TDate;
begin
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     Inc( FLinea );
                     FRenglon.Clear;

                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos + '"';

                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe' ), 0 );
                     if ( dmCliente.SetEmpleadoNumero(FEmpleado) ) then
                     begin
                          if ( Copy( Trim( FRenglon[ 1 ] ), 1, 2 ) = '02' ) then
                          begin
                               if ValidaBajaEmpleado( K_T_PUESTO ) then
                               begin
                                    if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_CAMBIO_PUESTO )then
                                    begin
                                         if strLleno( Trim(  FRenglon[ 3 ] ) )then
                                         begin
                                              try
                                                 sPuesto := BuscaPuesto( ansiUpperCase( Trim( Copy( FRenglon[ 3 ] , 1, 30 ) ) ) );
                                                 if (sPuesto = '') then
                                                 begin
                                                      sPuesto := InsertaPuesto( ansiUpperCase( Trim( Copy( FRenglon[ 3 ], 1, 30 ) ) ) );
                                                      EscribeBitacora( 'Se cre� nuevo c�digo en cat�logo de puestos: ' + sPuesto +'.' ,Fempleado,Flinea);
                                                 end;

                                                 dFechaEfectiva := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                                                 sComentarios := Trim( Copy( FRenglon[ 5 ], 1, 255 ) );
                                                 Self.SetNuevoKardex( K_T_PUESTO );
                                                 with dmRecursos.cdsEditHisKardex do
                                                 begin
                                                      FieldByName( 'CB_FECHA' ).AsDateTime := dFechaEfectiva;
                                                      FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_NO;
                                                      FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                      FieldByName( 'CB_PUESTO' ).AsString := sPuesto;
                                                      FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                                                 end;
                                                 ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                 if dmRecursos.RefrescaHisKardex then
                                                 begin
                                                      FHuboCambios := TRUE;
                                                      EscribeBitacora( Format( 'Se registr� cambio de puesto, fecha: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                 end;
                                                 dmRecursos.RefrescaKardex( K_T_PUESTO );
                                              except
                                                    on Error : Exception do
                                                       EscribeError( Format( 'Error al procesar el cambio de puesto: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                              end;
                                         end
                                         else
                                         begin
                                              EscribeError( 'Puesto no puede quedar vac�o. No se procesa registro.', FEmpleado, FLinea );
                                         end;
                                    end
                                    else
                                        EscribeError( 'Usuario sin derecho en Sistema TRESS para generar cambio de puesto.', FEmpleado, FLinea );
                               end;
                          end
                          else
                          begin
                               EscribeError( 'No se puede realizar el proceso. C�digo correcto [02].',  FEmpleado , FLinea );
                          end;
                     end
                     else
                     begin
                          EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                     end;
                end;
             finally
                     freeandnil(Frenglon);
             end;
        end;
     finally
             FreeAndNIl(Fdatos);
     end;
end;

Procedure TdmInterfase.ProcesaCambioDeSalario;
var
   i:Integer;
   sComentarios, sDatos: String;
   dFechaEfectiva, dFecha: TDate;
   SalNuevo,SalViejo : Currency;
begin
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     Inc( FLinea );
                     FRenglon.Clear;

                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos + '"';

                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe' ), 0 );
                     if ( dmCliente.SetEmpleadoNumero(FEmpleado)) then
                     begin
                          if ( Copy( Trim( FRenglon[ 1 ] ), 1, 2 ) = '01' ) then
                          begin
                               if ValidaBajaEmpleado( K_T_CAMBIO ) then
                               begin
                                    if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_CAMBIO_SALARIO )then
                                    begin
                                         SalViejo := dmCliente.cdsEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat;
                                         SalNuevo := StrToFloatDef( Trim(FRenglon[ 4 ]), 0 );
                                         sComentarios :=  Trim( Copy( FRenglon[ 5 ], 1, 255 ) );
                                         if( SalNuevo > 0 )then
                                         begin
                                              if ( SalNuevo > SalViejo) then
                                              begin
                                                   try
                                                      Self.SetNuevoKardex( K_T_CAMBIO );

                                                      with dmRecursos.cdsEditHisKardex do
                                                      begin
                                                           dFecha := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                                                           dFechaEfectiva := dFecha;
                                                           FieldByName( 'CB_FECHA' ).AsDateTime := dFechaEfectiva;
                                                           FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_NO;
                                                           FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                           FieldByName( 'CB_SALARIO' ).AsFloat := SalNuevo;
                                                           FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;
                                                           FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;

                                                      end;
                                                      dmRecursos.ListaPercepFijas := VACIO;
                                                      ValidaEnviar( dmRecursos.cdsEditHisKardex );

                                                      if dmRecursos.RefrescaHisKardex then
                                                      begin
                                                           FHuboCambios := TRUE;
                                                           EscribeBitacora( Format( 'Se registr� cambio de salario fecha: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                      end;
                                                      dmRecursos.RefrescaKardex( K_T_CAMBIO );
                                                   except
                                                         on Error : Exception do
                                                            EscribeError( Format( 'Error al procesar el cambio de salario: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                                   end;
                                              end
                                              else
                                              begin
                                                   EscribeError( 'Salario Nuevo debe ser mayor que Salario Actual. No se procesa registro.', FEmpleado, FLinea );
                                              end;
                                         end
                                         else
                                         begin
                                              EscribeError( 'Salario debe ser mayor que 0. No se procesa registro.', FEmpleado, FLinea );
                                         end;
                                    end
                                    else
                                        EscribeError( 'Usuario sin derecho en Sistema TRESS para generar cambio de salario.', FEmpleado, FLinea );
                               end;
                          end
                          else
                          begin
                               EscribeError( 'No se puede realizar el proceso. C�digo correcto [01].',  FEmpleado , FLinea );
                          end;
                     end
                     else
                     begin
                          EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                     end;
                end;
             finally
                    freeandnil(Frenglon);
             end;
        end;
     finally
            FreeAndNIl(Fdatos);
     end;
end;

function TdmInterfase.ObtenPeriodoBaja(dFecha: TDateTime; iTipo: Integer; var iYear: Integer; var iPeriodo: Integer ): Boolean;
begin
     Result := FALSE;
     with dmConsultas do
     begin
          SQLText := Format( GetSQLScript( K_CONSULTA_PERIODOS ), [ FechaAsStr( dFecha ), iTipo, Global.GetGlobalInteger( K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS ) ] );
          try
             cdsQueryGeneral.Refrescar;
             if( cdsQueryGeneral.RecordCount > 0 )then
             begin
                  iYear := ( cdsQueryGeneral.FieldByName( 'PE_YEAR' ).AsInteger );
                  iPeriodo := ( cdsQueryGeneral.FieldByName( 'PE_NUMERO' ).AsInteger );
                  Result := TRUE;
             end
             else
                 EscribeError( Format( 'No se encontr� un periodo de n�mina en la fecha de baja proporcionada. [%s]', [ FechaCorta( dFecha ) ] ), FEmpleado, FLinea );
          except
                on Error : Exception do
                   EscribeError( Format( 'Error al consultar el periodo de baja: %s.', [ Error.Message ] ), 0, FLinea );
          end;
     end;
end;

procedure TdmInterfase.ProcesaBaja;
const
     K_ALTA = 0;
var
   i,iTipo, iYear, iPeriodo: Integer;
   sComentarios, sDatos: String;
   dFechaEfectiva, dFecha, dFecha2: TDate;

   function GetMotivoBaja( sTextoArchivo: string ): string;
   begin
        if ( sTextoArchivo = 'ATTENDANCE' ) or
           ( sTextoArchivo = 'LEAVE OF ABSENCE' ) then
           Result := 'AUS'
        else if ( sTextoArchivo = 'INVOLUNTARY TERMINATION' ) or
                ( sTextoArchivo = 'REDUCTION IN FORCE' ) or
                ( sTextoArchivo = 'RESIGNATIONS' ) or
                ( sTextoArchivo = 'RESTRUCTURING' ) or
                ( sTextoArchivo = 'SEVERANCE' ) or
                ( sTextoArchivo = 'TERMINATION BY STRATEGY' ) then
                Result := 'EST'
             else if ( sTextoArchivo = 'DEATH' ) then
                     Result := 'DEF'
                  else if ( sTextoArchivo = 'INDISCIPLINE' ) or
                          ( sTextoArchivo = 'INSUBORDINATION' ) or
                          ( sTextoArchivo = 'JOB ABANDONMENT' ) or
                          ( sTextoArchivo = 'POLICY/ETHICS VIOLATION' ) or
                          ( sTextoArchivo = 'POSITIVE DRUG TEST' ) then
                          result := 'RC'
                       else if ( sTextoArchivo = 'CAREER ADVANCEMENT' ) or
                               ( sTextoArchivo = 'CHANGE OF RESIDENCE ( CITY)' ) or
                               ( sTextoArchivo = 'CHILD CARE' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH BENEFITS' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH FELLOW EMPLOYEES' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH PAY' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH PROMOTIONAL OPPORTUNITIES' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH SUPERVISION/LEADERSHIP' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH TYPE OF WORK' ) or
                               ( sTextoArchivo = 'DISSATISFIED WITH WORK CONDITIONS' ) or
                               ( sTextoArchivo = 'FAMILY HEALTH' ) or
                               ( sTextoArchivo = 'FAMILY REASONS' ) or
                               ( sTextoArchivo = 'HEALTH REASONS' ) or
                               ( sTextoArchivo = 'JOB TOO DIFFICULT' ) or
                               ( sTextoArchivo = 'LACK OF RECOGNITION' ) or
                               ( sTextoArchivo = 'MOVING' ) or
                               ( sTextoArchivo = 'OTHER' ) or
                               ( sTextoArchivo = 'PARTIAL/TOTAL DISABILITY' ) or
                               ( sTextoArchivo = 'PENSION' ) or
                               ( sTextoArchivo = 'PERSONAL' ) or
                               ( sTextoArchivo = 'PERSONAL HEALTH' ) or
                               ( sTextoArchivo = 'RETIREMENT' ) or
                               ( sTextoArchivo = 'RETURN TO SCHOOL' ) or
                               ( sTextoArchivo = 'SHIFT' ) or
                               ( sTextoArchivo = 'TRANSPORTATION PROBLEMS' ) or
                               ( sTextoArchivo = 'UNSATISFACTORY PERFORMANCE' ) or
                               ( sTextoArchivo = 'VOLUNTARY RESIGNATION' ) or
                               ( sTextoArchivo = 'VOLUNTARY TERMINATION' ) or
                               ( sTextoArchivo = 'WEDDING' ) then
                               result := 'RV'
                            else if ( sTextoArchivo = 'CONTRACT TERMINATION' ) or
                                    ( sTextoArchivo = 'END TEMPORARY EMPLOYMENT' ) or
                                    ( sTextoArchivo = 'TERMINATIONS' ) then
                                    Result := 'TC'
                                 else
                                     Result := VACIO;
   end;

begin
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     Inc( FLinea );
                     FRenglon.Clear;
                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos + '"';
                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe' ), 0 );
                     if ( dmCliente.SetEmpleadoNumero(FEmpleado)) then
                     begin
                          if ( Copy( Trim( FRenglon[ 1 ] ), 1, 2 ) = '09') then
                          begin
                              if ValidaBajaEmpleado( K_T_BAJA ) then
                              begin
                                   if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_BAJA )then
                                   begin
                                        try
                                           FFormatoFecha := ifMMDDYYYYs;
                                           iTipo := 1;
                                           dFecha := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                                           dFechaEfectiva := dFecha;
                                           dFecha2 := GetFechaImportada( Trim( Copy( FRenglon[ 4 ], 1, 10 ) ), FFormatoFecha );
                                           if ( dFecha2 = NullDateTime ) then
                                              dFecha2 := dFecha;
                                           sComentarios := Copy( FRenglon[ 5 ], 1, 255 );
                                           if ( ObtenPeriodoBaja( dFecha, iTipo, iYear, iPeriodo ) ) then
                                           begin
                                                SetNuevoKardex( K_T_BAJA );
                                                with dmRecursos.cdsEditHisKardex do
                                                begin
                                                     FieldByName( 'CB_FECHA' ).AsDateTime := dFecha;
                                                     FieldByName( 'CB_FECHA_2' ).AsDateTime := dFecha2;
                                                     FieldByName( 'CB_MOT_BAJ' ).AsString := GetMotivoBaja( AnsiUppercase( Trim( Copy( FRenglon[ 5 ], 1, 100 ) ) ) );
                                                     FieldByName( 'CB_GLOBAL').AsString := K_GLOBAL_NO;
                                                     FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                     FieldByName( 'CB_NOMYEAR').AsInteger := iYear;
                                                     FieldByName( 'CB_NOMTIPO').AsInteger := iTipo;
                                                     FieldByName( 'CB_NOMNUME').AsInteger :=  iPeriodo;
                                                     FieldByName( 'CB_RECONTR').AsString := K_GLOBAL_SI;
                                                     FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
                                                end;
                                                ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                if dmRecursos.RefrescaHisKardex then
                                                begin
                                                     FHuboCambios := TRUE;
                                                     EscribeBitacora( Format( 'Se registr� la baja del empleado el d�a %s.', [ FechaCorta( dFecha ) ] ), FEmpleado, FLinea );
                                                end;
                                                dmRecursos.RefrescaKardex( K_T_BAJA );
                                           end;
                                        except
                                              on Error : Exception do
                                              EscribeError( Format( 'Error al procesar la baja: %s.', [ Error.Message ] ), FEmpleado, FLinea );
                                        end;
                                   end
                                   else
                                   begin
                                        EscribeError( 'Usuario sin derecho en Sistema TRESS para dar de baja empleados.', FEmpleado, FLinea );
                                   end;
                              end;
                          end
                          else
                          begin
                               EscribeError( 'No se puede realizar el proceso. C�digo correcto [09].',  FEmpleado , FLinea );
                          end;
                     end
                     else
                     begin
                          EscribeError( 'El empleado no existe.', strToIntDef( Frenglon[0], 0 ), FLinea );
                     end;
                end;
             except
                   on Error : Exception do
                   EscribeError( Format( 'Error al procesar la baja: %s.', [ Error.Message ] ), FEmpleado, FLinea );
             end;
        end;
     finally
            freeandnil(Frenglon);
            FreeAndNIl(Fdatos);
     end;
end;

Procedure TdmInterfase.ProcesaRecontrata;
const
     K_ALTA = 0;
     K_REINGRESO = 1;
var
   i: Integer;
   sComentarios, sDatos : String;
   dFechaEfectiva, dFecha: TDate;
begin
     FDatos := TStringList.Create;
     SaldarVacaciones := TRUE;
     try
        FDatos.LoadFromFile( FArchivo );
        if( FDatos.Count > 0 )then
        begin
             FRenglon := TStringList.Create;
             try
                FRenglon.Delimiter := '|';
                for i := 0 to ( FDatos.Count - 1 ) do
                begin
                     Inc( FLinea );
                     FRenglon.Clear;
                     sDatos := FDatos[ i ];
                     sDatos := UTF8Decode( sDatos );
                     if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                        delete( sDatos, 1, 1);

                     sDatos := StrTransAll( sDatos, '|', '"|"' );
                     sDatos := ansiUpperCase( sDatos );
                     FRenglon.DelimitedText := '"' + sDatos +'"';
                     FEmpleado := strToIntDef( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 0 ] ), 'CB_CODIGO', 'COLABORA', 'El Empleado no existe' ), 0 );
                     try
                        dFecha := GetFechaImportada( Trim( Copy( FRenglon[ 2 ], 1, 10 ) ), FFormatoFecha );
                        dFechaEfectiva := dFecha;
                        if dmCliente.SetEmpleadoNumero(FEmpleado) then
                        begin
                             if ( Copy( Trim( FRenglon[ 1 ] ), 1, 2 ) = '08' ) then
                             begin
                                  if not dmCliente.GetDatosEmpleadoActivo.Activo then
                                  begin
                                       if ( dFechaEfectiva >= dmCliente.GetDatosEmpleadoActivo.Baja ) then
                                       begin
                                            if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_REINGRESO )then
                                            begin
                                                 FFormatoFecha := ifMMDDYYYYs;
                                                 sComentarios := Copy( FRenglon[ 5 ], 1, 255 );
                                                 SetNuevoKardex( K_T_ALTA );
                                                 with dmCliente , dmRecursos.cdsEditHisKardex do
                                                 begin
                                                      FieldByName( 'CB_FECHA' ).AsDateTime := dFecha;
                                                      FieldByName( 'CB_NOTA' ).AsString := sComentarios;
                                                      FieldByName( 'CB_GLOBAL' ).AsString := K_GLOBAL_NO;
                                                      FieldByName ( 'CB_PATRON' ).AsString := Trim( Copy( FRenglon[ 3 ], 1, 6 ) );
                                                      FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;

                                                      FieldByName( 'CB_FEC_ANT' ).AsDateTime := cdsEmpleado.FieldByName( 'CB_FEC_ANT' ).AsDateTime;
                                                      FieldByName( 'CB_AUTOSAL' ).AsString  := cdsEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString;
                                                      FieldByName( 'CB_CLASIFI' ).AsString  := cdsEmpleado.FieldByName( 'CB_CLASIFI' ).AsString;
                                                      FieldByName( 'CB_CONTRAT' ).AsString  := cdsEmpleado.FieldByName( 'CB_CONTRAT' ).AsString;
                                                      FieldByName( 'CB_TURNO' ).AsString    := cdsEmpleado.FieldByName( 'CB_TURNO' ).AsString;
                                                      FieldByName( 'CB_MOT_BAJ' ).AsString  := cdsEmpleado.FieldByName( 'CB_MOT_BAJ' ).AsString;
                                                      FieldByName( 'CB_PUESTO' ).AsString   := cdsEmpleado.FieldByName( 'CB_PUESTO' ).AsString;
                                                      FieldByName( 'CB_SALARIO' ).AsFloat   := cdsEmpleado.FieldByName( 'CB_SALARIO' ).AsFloat;
                                                      FieldByName( 'CB_TABLASS' ).AsString  := cdsEmpleado.FieldByName( 'CB_TABLASS' ).AsString;
                                                      FieldByName( 'CB_ZONA_GE' ).AsString  := cdsEmpleado.FieldByName( 'CB_ZONA_GE' ).AsString;;
                                                      FieldByName( 'CB_NIVEL1' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL1' ).AsString;
                                                      FieldByName( 'CB_NIVEL2' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL2' ).AsString;
                                                      FieldByName( 'CB_NIVEL3' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL3' ).AsString;
                                                      FieldByName( 'CB_NIVEL4' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL4' ).AsString;
                                                      FieldByName( 'CB_NIVEL5' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL5' ).AsString;
                                                      FieldByName( 'CB_NIVEL6' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL6' ).AsString;
                                                      FieldByName( 'CB_NIVEL7' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL7' ).AsString;
                                                      FieldByName( 'CB_NIVEL8' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL8' ).AsString;
                                                      FieldByName( 'CB_NIVEL9' ).AsString   := cdsEmpleado.FieldByName( 'CB_NIVEL9' ).AsString;
                                                      FieldByName( 'CB_NOMINA' ).AsInteger  := cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger
                                                 end;
                                                 ValidaEnviar( dmRecursos.cdsEditHisKardex );
                                                 if dmRecursos.RefrescaHisKardex then
                                                 begin
                                                      dmRecursos.RegistroAhorrosAutomaticos(K_REINGRESO);
                                                      FHuboCambios := TRUE;
                                                      EscribeBitacora( Format( 'Se registr� reingreso de empleado en la fecha: %s.', [ FechaCorta( dFechaEfectiva ) ] ), FEmpleado, FLinea );
                                                 end
                                                 else
                                                 begin
                                                      EscribeBitacora( 'Reingreso del empleado no fue registrado. Revisar Errores.', FEmpleado, FLinea );
                                                 end;
                                                 dmRecursos.RefrescaKardex( K_T_ALTA );
                                            end
                                            else
                                            begin
                                                EscribeError( 'Usuario sin derecho en Sistema TRESS para reingreso de empleados.', FEmpleado, FLinea );
                                            end;
                                       end
                                       else
                                       begin
                                            EscribeError( 'La fecha de reingreso ' + DateToStr( dFechaEfectiva ) +
                                                          ' debe ser mayor que ' + DateToStr( dmCliente.GetDatosEmpleadoActivo.Baja ) +
                                                          ' Fecha de baja del empleado', FEmpleado, FLinea, true );
                                       end;
                                  end
                                  else
                                  begin
                                       EscribeError( 'No se puede realizar el proceso, el empleado est� activo.', FEmpleado, FLinea );
                                  end;
                             end
                             else
                             begin
                                  EscribeError( 'No se puede realizar el proceso. C�digo correcto [08].',  FEmpleado , FLinea );
                             end;
                        end
                        else
                        begin
                             EscribeError( 'El empleado no existe.',  strToIntDef( FRenglon[ 0 ], 0 ) , FLinea );
                        end;
                     except
                           on Error : Exception do
                           EscribeError( Format( 'Error en la inserci�n, revise los datos y formato del archivo. [%s]', [ Error.Message ] ), 0, FLinea );
                     end;
                end;
             //except
                   //EscribeError( 'El empleado no existe.',  strtoint(FRenglon[ 0 ]) , FLinea );
             //end;
             finally
                    Freeandnil(Frenglon);
             end;
        end;
     finally
            FreeAndNIl(Fdatos);
     end;
end;

{*** US 16239: (Octubre) Spectrum Brands (Black & Decker) ***}
function TdmInterfase.EsTipoDePeriodoValidoInterfaz( iPeriodo: Integer ): Boolean;
begin
     Result := False;
     try
        dmCliente.InitArrayTPeriodo;
        with dmCliente do
        begin
             case GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad, iPeriodo ) of
                  tpDiario:                                 Result := True;
                  tpSemanal:                                Result := True;
                  tpCatorcenal,tpQuincenal:			            Result := False;
                  tpMensual:                                Result := False;
                  tpDecenal:                                Result := False;
             else
                  Result := False;
             end;
        end;
     except
           on Error : Exception do
           begin
                Result := False;
           end;
     end;
end;
{*** FIN ***}


procedure TdmInterfase.ProcesaAlta;
const
     K_ALTA = 0;
var
   i, iMaxNivel: Integer;
   sPadre, sMadre, sLista, sMensajeAdvertencia, sMensajeError,PU_CODIGO,Nivel4Codigo,sNivel1: String;
   sRFC, sCURP, sSexo, sDatos, sTemporal: string;
   sValor, sNivel, sTemp: string;
   lContinuar: Boolean;

   procedure LimpiaGlobales;
   begin
        with dmCliente.cdsEmpleado do
        begin
             // Datos Generales del empleado
             FieldByName( 'CB_CIUDAD' ).AsString   := VACIO;
             FieldByName( 'CB_ESTADO' ).AsString   := VACIO;
             FieldByName( 'CB_MUNICIP' ).AsString  := VACIO;
             FieldByName( 'CB_CHECA' ).AsString    := K_GLOBAL_NO;
             FieldByName( 'CB_NACION' ).AsString   := VACIO;
             FieldByName( 'CB_SEXO' ).AsString     := VACIO;
             FieldByName( 'CB_CREDENC' ).AsString  := 'A';
             FieldByName( 'CB_EDO_CIV' ).AsString  := VACIO;
             FieldByName( 'CB_VIVEEN' ).AsString   := VACIO;
             FieldByName( 'CB_VIVECON' ).AsString  := VACIO;
             FieldByName( 'CB_MED_TRA' ).AsString  := VACIO;
             FieldByName( 'CB_ESTUDIO' ).AsString  := VACIO;
             FieldByName( 'CB_AUTOSAL' ).AsString  := K_GLOBAL_NO;
             FieldByName( 'CB_ZONA_GE' ).AsString  := 'A';
             FieldByName( 'CB_PATRON' ).AsString   := VACIO;
             FieldByName( 'CB_SALARIO' ).AsFloat   := 0;
             FieldByName( 'CB_CONTRAT' ).AsString  := VACIO;
             FieldByName( 'CB_PUESTO' ).AsString   := VACIO;
             FieldByName( 'CB_CLASIFI' ).AsString  := VACIO;
             FieldByName( 'CB_TURNO' ).AsString    := VACIO;
             FieldByName( 'CB_TABLASS' ).AsString  := VACIO;
             FieldByName( 'CB_NIVEL1' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL2' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL3' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL4' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL5' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL6' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL7' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL8' ).AsString   := VACIO;
             FieldByName( 'CB_NIVEL9' ).AsString   := VACIO;
             FieldByName( 'CB_NOMINA' ).AsInteger  := Ord( tpSemanal );
             FieldByName( 'CB_TDISCAP' ).AsInteger  := Ord( disSinDefinir );

             // Adicionales de Texto
             FieldByName( 'CB_G_TEX_1' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_2' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_3' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_4' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_5' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_6' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_7' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_8' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX_9' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX10' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX11' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX12' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX13' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX14' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX15' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX16' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX17' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX18' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX19' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX20' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX21' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX22' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX23' ).AsString := VACIO;
             FieldByName( 'CB_G_TEX24' ).AsString := VACIO;

             // Adicionales de n�mero
             FieldByName( 'CB_G_NUM_1' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_2' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_3' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_4' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_5' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_6' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_7' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_8' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM_9' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM10' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM11' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM12' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM13' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM14' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM15' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM16' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM17' ).AsFloat := 0;
             FieldByName( 'CB_G_NUM18' ).AsFloat := 0;

             // Adicionales de Tablas
             FieldByName( 'CB_G_TAB_1' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_2' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_3' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_4' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_5' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_6' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_7' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_8' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB_9' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB10' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB11' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB12' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB13' ).AsString := VACIO;
             FieldByName( 'CB_G_TAB14' ).AsString := VACIO;

             // Adicionales logicos
             FieldByName( 'CB_G_LOG_1' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_2' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_3' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_4' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_5' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_6' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_7' ).AsString := K_GLOBAL_NO;
             FieldByName( 'CB_G_LOG_8' ).AsString := K_GLOBAL_NO;

             // Adicionales de Fecha
             FieldByName( 'CB_G_FEC_1' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_2' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_3' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_4' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_5' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_6' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_7' ).AsDateTime := NullDateTime;
             FieldByName( 'CB_G_FEC_8' ).AsDateTime := NullDateTime;
        end;
   end;

begin
     iMaxNivel := Global.NumNiveles;
     // Valida que Sistema TRESS no este en Modo DEMO
     if not Autorizacion.EsDemo then
     begin
          // Valida que el usuario tenga al menos el derecho de usuario para realizar un ALTA
          if ZAccesosMgr.RevisaCualquiera( D_EMP_REG_ALTA )then
          begin
               sMensajeAdvertencia := VACIO;
               sMensajeError := VACIO;

               dmCliente.EvaluarEmpleadosCliente( evOpAlta, sMensajeAdvertencia, sMensajeError );
               if StrLleno( sMensajeAdvertencia )then
                  EscribeError( sMensajeAdvertencia );

               if StrVacio( sMensajeError )then
               begin
                    FDatos := TStringList.Create;
                    try
                       FDatos.LoadFromFile( FArchivo );
                       if( FDatos.Count > 0 )then
                       begin
                            FRenglon := TStringList.Create;
                            try
                               FRenglon.Delimiter := '|';
                               for i := 0 to ( FDatos.Count - 1) do
                               begin
                                    Inc( FLinea );
                                    FRenglon.Clear;
                                    lContinuar := TRUE;
                                    sDatos := FDatos[ i ];
                                    sDatos := UTF8Decode( sDatos );
                                    if (sDatos <> '') and (Copy( sDatos, 1,1 ) = #$FEFF) then
                                       delete( sDatos, 1, 1);

                                    sDatos := StrTransAll( sDatos, '|', '"|"' );
                                    sDatos := ansiUpperCase( sDatos );
                                    FRenglon.DelimitedText := '"' + sDatos +'"';
                                    if StrVacio( BuscaCodigo( 'CB_G_NUM_9', Trim( FRenglon[ 122 ] ), 'CB_CODIGO', 'COLABORA', '' ) ) then
                                    begin
                                         FEmpleado := BuscaEmpleado( Trim( FRenglon[ 0 ] ) );
                                         if not dmCliente.SetEmpleadoNumero( FEmpleado ) then
                                         begin
                                              with dmCliente.cdsEmpleado do
                                              begin
                                                   Append;
                                                   LimpiaGlobales;
                                                   try
                                                      FieldByName( 'CB_APE_PAT' ).AsString := Trim( Copy( FRenglon[ 1 ], 1, 30 ));
                                                      FieldByName( 'CB_APE_MAT' ).AsString := Trim( Copy( FRenglon[ 2 ], 1, 30 ));
                                                      FieldByName( 'CB_NOMBRES' ).AsString := Trim( Copy( FRenglon[ 3 ], 1, 30 ));
                                                      sSexo := ansiUpperCase( Trim( FRenglon[ 4 ] ));
                                                      if ( sSexo = 'M' ) or ( sSexo = 'F' ) then
                                                         FieldByName( 'CB_SEXO' ).AsString := sSexo
                                                      else
                                                          FieldByName( 'CB_SEXO' ).AsString := VACIO;

                                                      FieldByName( 'CB_FEC_NAC' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 5 ], 1, 10 ) ), FFormatoFecha );

                                                      //RFC, # de IMSS y CURP Estan al final para que el resto de los datos ya se hayan leido.

                                                      //  FieldByName( 'CB_CLINICA' ).AsString := Trim( Copy(FRenglon[ 8 ], 1, 3) );
                                                      FieldByName( 'CB_FEC_ING' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 10 ], 1, 10 ) ), FFormatoFecha );
                                                      FieldByName( 'CB_FEC_ANT' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 11 ], 1, 10 ) ), FFormatoFecha );
                                                      { Fecha de Contrato }
                                                      FieldByName( 'CB_FEC_CON' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 12 ], 1, 10 ) ), FFormatoFecha );

                                                      {contrato}
                                                      FieldByName( 'CB_CONTRAT' ).AsInteger := 3;

                                                      { Puesto }
                                                      if strLleno( Trim( FRenglon[ 14 ] ) ) then
                                                      begin
                                                           PU_CODIGO := BuscaPuesto( Trim( Copy( FRenglon[ 14 ], 1, 30 ) ) );
                                                           if strVACIO( PU_CODIGO ) then
                                                           begin
                                                                PU_CODIGO := InsertaPuesto( Trim( Copy(FRenglon[ 14 ], 1, 30 ) ) );
                                                           end;
                                                               FieldByName( 'CB_PUESTO' ).AsString := PU_CODIGO;
                                                      end;

                                                      { clasificaci�n del empleado }
                                                      if strLleno( Trim( FRenglon[ 15 ] ) ) then
                                                      begin
                                                           if ( ( Trim( Copy(FRenglon[ 15 ],1,2)) = 'HI') or
                                                                ( Trim( Copy(FRenglon[ 15 ],1,2)) = 'HD') or
                                                                ( Trim( Copy(FRenglon[ 15 ],1,2)) = 'SE') ) then
                                                              FieldByName( 'CB_CLASIFI' ).AsString := Trim( Copy(FRenglon[ 15 ],1,2)); 
                                                      end
                                                      else
                                                          FieldByName( 'CB_CLASIFI' ).AsString :=  Vacio;


                                                      //cb_turno
                                                      if strLleno( Trim( FRenglon[ 16 ] ) ) then
                                                      begin
                                                           if ( ( ansiUpperCase( Trim( FRenglon[ 16 ] ) ) = 'T7' ) or
                                                                ( ansiUpperCase( Trim( FRenglon[ 16 ] ) ) = 'T8' ) or
                                                                ( ansiUpperCase( Trim( FRenglon[ 16 ] ) ) = 'SHIFT3' ) ) then
                                                              FieldByName( 'CB_TURNO' ).AsString := ansiUpperCase( Trim( FRenglon[ 16 ] ) );
                                                      end
                                                      else
                                                          FieldByName( 'CB_TURNO' ).AsString := VACIO;


                                                      { Registro Patronal }
                                                      FieldByName( 'CB_PATRON' ).asInteger := 1;

                                                      { Nivel #1 }
                                                      if strLleno( Trim( FRenglon[ 18 ] ) ) then
                                                      begin
                                                           sNivel1 :=  Trim(Copy(FRenglon[ 18 ],1,2) );
                                                           FieldByName( 'CB_NIVEL1' ).AsString := sNivel1;
                                                      end
                                                      else
                                                          FieldByName( 'CB_NIVEL1' ).AsString := VACIO ;

                                                      { Nivel #2 }
                                                      if strLleno( Trim( FRenglon[ 19 ] ) ) then
                                                      begin
                                                           {$ifdef ANTES}
                                                           if(sNivel1 = 'K2') then
                                                           begin
                                                                FieldByName( 'CB_NIVEL2' ).AsString := Trim(Copy(FRenglon[ 19 ],11,6) );
                                                           end
                                                           else
                                                           begin
                                                                FieldByName( 'CB_NIVEL2' ).AsString := Trim(Copy(FRenglon[ 19 ],11,4) );
                                                           end;
                                                           {$else}
                                                           sValor := Trim( FRenglon[ 19 ] );
                                                           sNivel := VACIO;
                                                           if strLleno( sValor ) then
                                                           begin
                                                                if ( Length( sValor ) > 8 ) then
                                                                begin
                                                                     sTemp := Copy( sValor, 8, Length( sValor ) );
                                                                     if ( ansiPos( ' ', sTemp ) > 0 ) then
                                                                        sNivel := Trim( Copy( sTemp, 1, ansiPos( ' ', sTemp ) - 1 ) )
                                                                     else
                                                                         sNivel := Copy( sTemp, 1, 6 );
                                                                     sNivel := Copy( sNivel, 1, 6 );
                                                                end
                                                                else
                                                                begin
                                                                     sTemp := VACIO;
                                                                end;
                                                           end;
                                                           if strLleno( sNivel ) then
                                                              FieldByName( 'CB_NIVEL2' ).AsString := sNivel
                                                           else
                                                               FieldByName( 'CB_NIVEL2' ).AsString := VACIO;
                                                           {$endif}
                                                      end;

                                                      { Nivel #3 }
                                                      FieldByName( 'CB_NIVEL3' ).AsString := '0';

                                                      { Nivel #4 }
                                                      if strLleno( Trim( FRenglon[ 21 ] ) ) then
                                                      begin
                                                           if strLleno( Trim( Copy( FRenglon[ 21 ], 1, 30 ) ) ) then
                                                           begin
                                                                Nivel4Codigo := BuscaNivel4( Trim( Copy( FRenglon[ 21 ], 1, 30 ) ) );
                                                                if ( Nivel4Codigo = '' ) then
                                                                begin
                                                                     Nivel4Codigo := InsertaNivel4( Trim( Copy( FRenglon[ 21 ], 1, 30) ) );
                                                                end;
                                                                FieldByName( 'CB_NIVEL4' ).AsString := Nivel4Codigo;
                                                           end;
                                                      end;

                                                      { Nivel #5 }
                                                      if ( ( FieldByName( 'CB_CLASIFI' ).AsString = 'HI' ) or
                                                           ( FieldByName( 'CB_CLASIFI' ).AsString = 'HD' ) ) then
                                                      begin
                                                           FieldByName( 'CB_NIVEL5' ).AsString := 'OP';
                                                      end
                                                      else if ( FieldByName( 'CB_CLASIFI' ).AsString = 'SE' ) then
                                                              FieldByName( 'CB_NIVEL5' ).AsString := '0'
                                                           else
                                                               FieldByName( 'CB_NIVEL5' ).AsString := VACIO;

                                                      { Nivel #6 }
                                                      FieldByName( 'CB_NIVEL6' ).AsString := 'TEGENE';

                                                      { Nivel #7 }
                                                      FieldByName( 'CB_NIVEL7' ).AsString := Global.GetGlobalString( K_GLOBAL_DEF_NIVEL_7 );

                                                      { Nivel #8 }
                                                      FieldByName( 'CB_NIVEL8' ).AsString := Global.GetGlobalString( K_GLOBAL_DEF_NIVEL_8 );

                                                      { Nivel #9 }
                                                      FieldByName( 'CB_NIVEL9' ).AsString := Global.GetGlobalString( K_GLOBAL_DEF_NIVEL_9 );

                                                      { Salario Por Tabulador }
                                                      FieldByName( 'CB_AUTOSAL' ).AsString := K_GLOBAL_NO;

                                                      { salario }
                                                      FieldByName('CB_SALARIO').AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 28 ], 1,9) ), 0 );

                                                      { Zona Geografica }
                                                      FieldByName( 'CB_ZONA_GE' ).AsString := 'A';

                                                      { Tabla de Prestaciones }
                                                      if ( ansiUpperCase( Trim( Copy( FRenglon[ 33 ], 1, 30 ) ) ) = 'HOURLY' ) then
                                                         FieldByName( 'CB_TABLASS' ).AsString := 'A'
                                                      else if ( ansiUpperCase( Trim( Copy( FRenglon[ 33 ], 1, 30 ) ) ) = 'SALARY' ) then
                                                              FieldByName( 'CB_TABLASS' ).AsString := 'B'
                                                           else
                                                               FieldByName( 'CB_TABLASS' ).AsString := VACIO;

                                                      { Tipo Nomina }
                                                      FieldByName( 'CB_NOMINA' ).AsInteger := 1;

                                                      { Apellido Paterno}
                                                      if StrVacio( FieldByName( 'CB_APE_PAT' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'El apellido paterno del empleado est� vac�o.', FEmpleado, FLinea );
                                                      end;

                                                      {Nombres}
                                                      if StrVacio( FieldByName( 'CB_NOMBRES' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'El Nombre del empleado est� vac�o.', FEmpleado, FLinea );
                                                      end;

                                                      {Sexo}
                                                      if StrVacio( FieldByName( 'CB_SEXO' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'El campo de sexo no contiene un valor v�lido [F � M].', FEmpleado, FLinea );
                                                      end;
                                                      if ( FieldByName( 'CB_FEC_NAC' ).AsDateTime = NullDateTime )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'La fecha de nacimiento del empleado es inv�lida [%s].', [ Copy( FRenglon[ 5 ], 1, 10 ) ] ), FEmpleado, FLinea );
                                                      end;
                                                      if ( FieldByName( 'CB_FEC_ING' ).AsDateTime = NullDateTime )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'La fecha de ingreso del empleado es inv�lida [%s].', [ Copy( FRenglon[ 10 ], 1, 10 ) ] ), FEmpleado, FLinea );
                                                      end;
                                                      if ( FieldByName( 'CB_FEC_ANT' ).AsDateTime = NullDateTime )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'La fecha de antig�edad del empleado es inv�lida [%s].', [ Copy( FRenglon[ 11 ], 1, 10 ) ] ), FEmpleado, FLinea );
                                                      end;
                                                      if ( FieldByName( 'CB_FEC_CON' ).AsDateTime = NullDateTime )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'La fecha de contrato del empleado es inv�lida [%s].', [ Copy( FRenglon[ 12 ], 1, 10 ) ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_CONTRAT' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format('Tipo de contrato del empleado inv�lido [%s].', [FRenglon[ 13 ]] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_PUESTO' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format('C�digo de puesto del empleado inv�lido [%s].', [FRenglon[ 14 ] ]), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_CLASIFI' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format('C�digo de clasificaci�n del empleado inv�lido [%s].', [ FRenglon [15] ]), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_TURNO' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'el turno del empleado inv�lido.', FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL1' ).AsString )and( iMaxNivel > 0 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format('El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 1 ), FRenglon[18] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL2' ).AsString )and( iMaxNivel > 1 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 2 ), FRenglon[19] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL3' ).AsString )and( iMaxNivel > 2 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 3 ), FRenglon[20] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL4' ).AsString )and( iMaxNivel > 3 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 4 ), FRenglon[21] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL5' ).AsString )and( iMaxNivel > 4 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 5 ), FRenglon[22] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL6' ).AsString )and( iMaxNivel > 5 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 6 ), FRenglon[23] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL7' ).AsString )and( iMaxNivel > 6 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 7 ), FRenglon[24] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL8' ).AsString )and( iMaxNivel > 7 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 8 ), FRenglon[25] ] ), FEmpleado, FLinea );
                                                      end;
                                                      if StrVacio( FieldByName( 'CB_NIVEL9' ).AsString )and( iMaxNivel > 8 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El nivel %s del empleado inv�lido. [%s].', [ Global.NombreNivel( 9 ), FRenglon[26] ] ), FEmpleado, FLinea );
                                                      end;

                                                      if ( FieldByName( 'CB_SALARIO' ).AsInteger <= 0 )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'El salario del empleado no puede ser menor o igual a cero.', FEmpleado, FLinea );
                                                      end;

                                                      if StrVacio( FieldByName( 'CB_ZONA_GE' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'La zona Geografica del empleado inv�lida.', FEmpleado, FLinea );
                                                      end;

                                                      if StrVacio( FieldByName( 'CB_TABLASS' ).AsString )then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( 'La tabla de prestaciones del empleado inv�lida.', FEmpleado, FLinea );
                                                      end;
                                                      //if ( (FieldByName( 'CB_NOMINA' ).AsInteger > 1) OR (FieldByName( 'CB_NOMINA' ).AsInteger > 12) )then old
                                                      if not ( EsTipoDePeriodoValidoInterfaz( FieldByName( 'CB_NOMINA' ).AsInteger ) ) then
                                                      begin
                                                           lContinuar := FALSE;
                                                           EscribeError( Format( 'El tipo de n�mina [%s] del empleado inv�lido.', [ Trim( FRenglon[ 145 ] ) ] ), FEmpleado, FLinea );
                                                      end;

                                                      if not lContinuar then
                                                      begin
                                                           EscribeBitacora( 'No se procesar� registro de alta del empleado. Revisar bit�cora de errores.', FEmpleado, FLinea );
                                                           CancelUpdates;
                                                      end
                                                      else
                                                      begin
                                                           { Segunda parte }
                                                           FieldByName( 'CB_PER_VAR' ).AsString := Vacio;
                                                           FieldByName( 'CB_INFTIPO' ).AsInteger := StrToIntDef( Trim( Copy(FRenglon[ 35 ], 1, 2) ), 0 );
                                                           FieldByName( 'CB_INFCRED' ).AsString := Trim( Copy(FRenglon[ 36 ],1, 30) );
                                                           FieldByName( 'CB_INFTASA' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 37 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_INF_OLD' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 38 ],1, 9) ), 0 );
                                                           FieldByName( 'CB_INF_INI' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 39 ], 1, 10 ) ), FFormatoFecha );
                                                           FieldByName( 'CB_CREDENC' ).AsString := 'A';

                                                           {CB_CHECA}
                                                           if ( ansiUpperCase( Trim( Copy( FRenglon[ 41 ], 1, 30 ) ) ) = 'HOURLY' ) then
                                                              FieldByName( 'CB_CHECA' ).AsString := K_GLOBAL_SI
                                                           else if ( ansiUpperCase( Trim( Copy( FRenglon[ 41 ], 1, 30 ) ) ) = 'SALARY' ) then
                                                                   FieldByName( 'CB_CHECA' ).AsString := K_GLOBAL_NO
                                                                else
                                                                    FieldByName( 'CB_CHECA' ).AsString := K_GLOBAL_NO;
                                                           FieldByName( 'CB_BAN_ELE' ).AsString := Trim( Copy( FRenglon[ 42 ], 1, 30 ) );
                                                           FieldByName( 'CB_NIVEL0' ).AsString := 'GENERA';
                                                           FieldByName( 'CB_LUG_NAC' ).AsString := Trim( Copy( FRenglon[ 47 ], 1, 30 ) );
                                                           FieldByName( 'CB_NACION' ).AsString := Trim( Copy( FRenglon[ 48 ], 1, 30 ) );

                                                           if ( Trim( Copy( FRenglon[ 49 ], 1, 1 ) ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_PASAPOR' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_PASAPOR' ).AsString := K_GLOBAL_NO;

                                                           if (strLleno(Trim( Copy( FRenglon[ 50 ], 1, 1 ) ))) then
                                                              FieldByName( 'CB_EDO_CIV' ).AsString := Trim( Copy( FRenglon[ 50 ], 1, 1 ) )
                                                           else
                                                               FieldByName( 'CB_EDO_CIV' ).AsString := Global.GetGlobalString( K_GLOBAL_DEF_ESTADO_CIVIL );

                                                           FieldByName( 'CB_LA_MAT' ).AsString := Trim( Copy( FRenglon[ 51 ], 1, 30 ) );
                                                           FieldByName( 'CB_MED_TRA' ).AsString := Trim( Copy( FRenglon[ 54 ], 1, 1 ) );
                                                           FieldByName( 'CB_CALLE' ).AsString := Trim( Copy( FRenglon[ 55 ], 1, 50 ) );
                                                           FieldByName( 'CB_COLONIA' ).AsString := Trim( Copy( FRenglon[ 56 ], 1, 30 ) );
                                                           FieldByName( 'CB_CIUDAD' ).AsString := Trim( Copy( FRenglon[ 57 ], 1, 30 ) );
                                                           FieldByName( 'CB_CODPOST' ).AsString := Trim( Copy( FRenglon[ 58 ], 1, 8 ) );

                                                           if strLleno( Trim( Copy( FRenglon[ 59 ], 1, 2 ) ) ) then
                                                              FieldByName( 'CB_ESTADO' ).AsString := '03'
                                                           else
                                                               FieldByName( 'CB_ESTADO' ).AsString := Global.GetGlobalString( K_GLOBAL_ENTIDAD_EMPRESA );

                                                           FieldByName( 'CB_ZONA' ).AsString := 'Mexicali';
                                                           FieldByName( 'CB_TEL' ).AsString := Trim( Copy( FRenglon[ 61 ], 1, 30 ) );
                                                           FieldByName( 'CB_VIVECON' ).AsString := Trim( Copy( FRenglon[ 62 ], 1, 2 ) );
                                                           FieldByName( 'CB_VIVEEN' ).AsString := Trim( Copy( FRenglon[ 63 ], 1, 2 ) );
                                                           if strLleno( Trim( Copy( FRenglon[ 64 ], 1, 10 ) ) ) then
                                                           begin
                                                                FieldByName( 'CB_FEC_RES' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 64 ], 1, 10 ) ), FFormatoFecha );
                                                           end;
                                                           FieldByName( 'CB_ESTUDIO' ).AsString := Trim( Copy( FRenglon[ 65 ], 1, 2 ) );
                                                           FieldByName( 'CB_CARRERA' ).AsString := Trim( Copy( FRenglon[ 66 ], 1, 40 ) );

                                                           if ( Trim( Copy( FRenglon[ 67 ], 1, 1 ) ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_EST_HOY' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_EST_HOY' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_EST_HOR' ).AsString := Trim( Copy( FRenglon[ 68 ], 1, 30 ) );

                                                           if ( Trim( Copy( FRenglon[ 69 ], 1, 1 ) ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_HABLA' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_HABLA' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_IDIOMA' ).AsString := Trim( Copy( FRenglon[ 70 ], 1, 30 ) );
                                                           FieldByName( 'CB_MAQUINA' ).AsString := Trim( Copy( FRenglon[ 71 ], 1, 100 ) );
                                                           FieldByName( 'CB_EXPERIE' ).AsString := Trim( Copy( FRenglon[ 72 ], 1, 100 ) );
                                                           FieldByName( 'CB_G_TEX_1' ).AsString := Trim( Copy( FRenglon[ 73 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_2' ).AsString := Trim( Copy( FRenglon[ 74 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_3' ).AsString := Trim( Copy( FRenglon[ 75 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_4' ).AsString := Trim( Copy( FRenglon[ 76 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TAB_1' ).AsString := Trim( Copy( FRenglon[ 77 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_2' ).AsString := Trim( Copy( FRenglon[ 78 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_3' ).AsString := Trim( Copy( FRenglon[ 79 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_4' ).AsString := Trim( Copy( FRenglon[ 80 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_NUM_1' ).AsFloat := StrToFloatDef( Trim( FRenglon[ 81 ] ), 0 );
                                                           FieldByName( 'CB_G_NUM_2' ).AsFloat := StrToFloatDef( Trim( FRenglon[ 82 ] ), 0 );
                                                           FieldByName( 'CB_G_NUM_3' ).AsFloat := StrToFloatDef( Trim( FRenglon[ 83 ] ), 0 );
                                                           if ( Trim( Copy( FRenglon[ 84 ], 1, 1 ) ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_1' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_1' ).AsString := K_GLOBAL_NO;

                                                           if ( Trim( Copy( FRenglon[ 85 ], 1, 1 ) ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_2' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_2' ).AsString := K_GLOBAL_NO;

                                                           if ( Trim( Copy( FRenglon[ 86 ], 1, 1 ) ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_3' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_3' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_G_FEC_1' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 87 ], 1, 10 ) ), FFormatoFecha );
                                                           FieldByName( 'CB_G_FEC_2' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 88 ], 1, 10 ) ), FFormatoFecha );
                                                           FieldByName( 'CB_G_FEC_3' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 89 ], 1, 10 ) ), FFormatoFecha );
                                                           FieldByName( 'CB_ENT_NAC' ).AsString := Trim( Copy( FRenglon[ 90 ], 1, 6 ) );

                                                           FieldByName( 'CB_COD_COL' ).AsString := Trim( Copy( FRenglon[ 91 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TEX_5' ).AsString := Trim( Copy( FRenglon[ 92 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_6' ).AsString := Trim( Copy( FRenglon[ 93 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_7' ).AsString := Trim( Copy( FRenglon[ 94 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_8' ).AsString := Trim( Copy( FRenglon[ 95 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX_9' ).AsString := Trim( Copy( FRenglon[ 96 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX10' ).AsString := Trim( Copy( FRenglon[ 97 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX11' ).AsString := Trim( Copy( FRenglon[ 98 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX12' ).AsString := Trim( Copy( FRenglon[ 99 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX13' ).AsString := Trim( Copy( FRenglon[ 100 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX14' ).AsString := Trim( Copy( FRenglon[ 101 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX15' ).AsString := Trim( Copy( FRenglon[ 102 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX16' ).AsString := Trim( Copy( FRenglon[ 103 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX17' ).AsString := Trim( Copy( FRenglon[ 104 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX18' ).AsString := Trim( Copy( FRenglon[ 105 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX19' ).AsString := Trim( Copy( FRenglon[ 106 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TAB_5' ).AsString := Trim( Copy( FRenglon[ 107 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_6' ).AsString := Trim( Copy( FRenglon[ 108 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_7' ).AsString := Trim( Copy( FRenglon[ 109 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_8' ).AsString := Trim( Copy( FRenglon[ 110 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB_9' ).AsString := Trim( Copy( FRenglon[ 111 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB10' ).AsString := Trim( Copy( FRenglon[ 112 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB11' ).AsString := Trim( Copy( FRenglon[ 113 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB12' ).AsString := Trim( Copy( FRenglon[ 114 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB13' ).AsString := Trim( Copy( FRenglon[ 115 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_TAB14' ).AsString := Trim( Copy( FRenglon[ 116 ], 1, 6 ) );
                                                           FieldByName( 'CB_G_NUM_4' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 117 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM_5' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 118 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM_6' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 119 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM_7' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 120 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM_8' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 121 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM_9' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 122 ], 1, 30) ), 0 );
                                                           FieldByName( 'CB_G_NUM10' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 123 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM11' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 124 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM12' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 125 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM13' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 126 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM14' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 127 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM15' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 128 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM16' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 129 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM17' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 130 ], 1, 9) ), 0 );
                                                           FieldByName( 'CB_G_NUM18' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 131 ], 1, 9) ), 0 );

                                                           if ( Copy( FRenglon[ 132 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_4' ).AsString := K_GLOBAL_SI
                                                           else
                                                              FieldByName( 'CB_G_LOG_4' ).AsString := K_GLOBAL_NO;

                                                           if ( Copy( FRenglon[ 133 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_5' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_5' ).AsString := K_GLOBAL_NO;

                                                           if ( Copy( FRenglon[ 134 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_6' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_6' ).AsString := K_GLOBAL_NO;

                                                           if ( Copy( FRenglon[ 135 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_7' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_7' ).AsString := K_GLOBAL_NO;

                                                           if ( Copy( FRenglon[ 136 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_G_LOG_8' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_G_LOG_8' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_G_FEC_4' ).AsDateTime := GetFechaImportada( Copy( FRenglon[ 137 ], 1, 10 ), FFormatoFecha );
                                                           FieldByName( 'CB_G_FEC_5' ).AsDateTime := GetFechaImportada( Copy( FRenglon[ 138 ], 1, 10 ), FFormatoFecha );
                                                           FieldByName( 'CB_G_FEC_6' ).AsDateTime := GetFechaImportada( Copy( FRenglon[ 139 ], 1, 10 ), FFormatoFecha );
                                                           FieldByName( 'CB_G_FEC_7' ).AsDateTime := GetFechaImportada( Copy( FRenglon[ 140 ], 1, 10 ), FFormatoFecha );
                                                           FieldByName( 'CB_G_FEC_8' ).AsDateTime := GetFechaImportada( Copy( FRenglon[ 141 ], 1, 10 ), FFormatoFecha );
                                                           FieldByName( 'CB_SUB_CTA' ).AsString := Trim( Copy( FRenglon[ 142 ], 1, 30 ) );
                                                           FieldByName( 'CB_NETO' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 143 ], 1,9) ), 0 );
                                                           FieldByName( 'CB_DER_PV' ).AsFloat := StrToFloatDef( Trim( Copy(FRenglon[ 144 ], 1, 9) ), 0 );

                                                           if ( Copy( FRenglon[ 146 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_DISCAPA' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_DISCAPA' ).AsString := K_GLOBAL_NO;

                                                           if ( Copy( FRenglon[ 147 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_INDIGE' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_INDIGE' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_FONACOT' ).AsString := Trim( Copy( FRenglon[ 148 ], 1, 30 ) );

                                                           if ( Copy( FRenglon[ 149 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_EMPLEO' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_EMPLEO' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_G_TEX20' ).AsString := Trim( Copy( FRenglon[ 150 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX21' ).AsString := Trim( Copy( FRenglon[ 151 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX22' ).AsString := Trim( Copy( FRenglon[ 152 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX23' ).AsString := Trim( Copy( FRenglon[ 153 ], 1, 30 ) );
                                                           FieldByName( 'CB_G_TEX24' ).AsString := Trim( Copy( FRenglon[ 154 ], 1, 30 ) );
                                                           FieldByName( 'CB_FEC_COV' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 155 ], 1, 10 ) ), FFormatoFecha );

                                                           if ( Copy( FRenglon[ 156 ], 1, 1 ) = K_GLOBAL_SI ) then
                                                              FieldByName( 'CB_INFDISM' ).AsString := K_GLOBAL_SI
                                                           else
                                                               FieldByName( 'CB_INFDISM' ).AsString := K_GLOBAL_NO;

                                                           FieldByName( 'CB_INF_ANT' ).AsDateTime := GetFechaImportada( Trim( Copy( FRenglon[ 157 ], 1, 10 ) ), FFormatoFecha );
                                                           FieldByName( 'CB_NUM_EXT' ).AsString := Trim( Copy( FRenglon[ 158 ], 1, 10 ) );
                                                           FieldByName( 'CB_NUM_INT' ).AsString := Trim( Copy( FRenglon[ 159 ], 1, 10 ) );
                                                           if strLleno( Copy( FRenglon[ 160 ], 1, 1 ) ) then
                                                              FieldByName( 'CB_TDISCAP' ).AsInteger := strToIntDef( Copy( FRenglon[ 160 ], 1, 1 ), 0 )
                                                           else
                                                               FieldByName( 'CB_TDISCAP' ).AsInteger := Ord( disSinDefinir );

                                                           FieldByName( 'CB_ESCUELA' ).AsString := Trim( Copy( FRenglon[ 161 ], 1, 40 ) );

                                                           if strLleno( Copy( FRenglon[ 162 ], 1, 1 ) ) then
                                                              FieldByName( 'CB_TESCUEL' ).AsInteger := StrToIntDef( Copy( FRenglon[ 162 ], 1, 1 ), 0 )
                                                           else
                                                               FieldByName( 'CB_TESCUEL' ).AsInteger := 0;

                                                           if strLleno( Copy( FRenglon[ 163 ], 1, 1 ) ) then
                                                              FieldByName( 'CB_TITULO' ).AsInteger := StrToIntDef( Copy( FRenglon[ 163 ], 1, 1 ), 0 )
                                                           else
                                                               FieldByName( 'CB_TITULO' ).AsInteger := 0;

                                                           FieldByName( 'CB_YTITULO' ).AsInteger := strToIntDef( Trim( Copy(FRenglon[ 164 ], 1, 2) ), 0 );
                                                           FieldByName( 'CB_CTA_GAS' ).AsString := Trim( Copy( FRenglon[ 165 ], 1, 30 ) );
                                                           FieldByName( 'CB_CTA_VAL' ).AsString := Trim( Copy( FRenglon[ 166 ], 1, 30 ) );
                                                           FieldByName( 'CB_MUNICIP' ).AsString := Trim( Copy( FRenglon[ 167 ], 1, 6  ) );


                                                           if (Frenglon.COunt > 168 ) then
                                                                 FieldByName( 'CB_ID_BIO' ).AsInteger := StrtointDef(Trim( Frenglon[ 168 ]),0 );

                                                           if (Frenglon.COunt > 169 ) then
                                                              FieldByName( 'CB_GP_COD'  ).AsString := Trim( Copy( Frenglon[ 169 ], 1,6 ) );

                                                           if (Frenglon.COunt > 170 ) then
                                                              FieldByName( 'CB_TSANGRE' ).AsString := Trim( Copy( Frenglon[ 170 ], 1,30 ) );

                                                           if (Frenglon.COunt > 171 ) then
                                                              FieldByName( 'CB_ALERGIA' ).AsString := Trim( Copy( Frenglon[ 171 ], 1,255 ) );

                                                           if (Frenglon.COunt > 172 ) then
                                                              FieldByName( 'CB_PLAZA'  ).asinteger := StrTointDef(Trim( Frenglon[ 172 ]),0 );

                                                           if (Frenglon.COunt > 173 ) then
                                                              FieldByName( 'CB_E_MAIL'  ).AsString := Trim( Copy( Frenglon[ 173 ], 1,255 ) );

                                                           if (Frenglon.COunt > 174 ) then
                                                              FieldByName( 'CB_BANCO'  ).AsString := Trim( Copy( Frenglon[ 174 ], 1,6 ) );

                                                           if (Frenglon.COunt > 175 ) then
                                                              FieldByName( 'CB_REGIMEN').Asinteger := StrTointDef(Trim(Frenglon[ 175 ] ),0 );


                                                           { C�lculo del RFC y Comparaci�n }
                                                           sTemporal := strTransAll( FRenglon[ 6 ], '-', '' );
                                                           FieldByName( 'CB_RFC' ).AsString := Copy( sTemporal, 1, 13 );
                                                           sRFC := ZetaCommonTools.CalcRFC( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                                            FieldByName( 'CB_APE_MAT' ).AsString,
                                                                                            FieldByName( 'CB_NOMBRES' ).AsString,
                                                                                            FieldByName( 'CB_FEC_NAC' ).AsDateTime );
                                                           if strLleno( sRFC ) and
                                                              strLleno( FieldByName( 'CB_RFC' ).AsString ) then
                                                           begin
                                                                if ( ansiUpperCase( sRFC ) <> ansiUpperCase( FieldByName( 'CB_RFC' ).AsString ) ) then
                                                                begin
                                                                     FieldByName( 'CB_RFC' ).AsString := sRFC;
                                                                     EscribeBitacora( 'RFC de empleado diferente al calculado por Sistema TRESS. Se actualizar� con el calculado.', FEmpleado, FLinea );
                                                                end;
                                                           end
                                                           else
                                                           begin
                                                                if strVacio( sRFC ) then
                                                                   EscribeError( 'RFC de empleado no pudo ser calculado por Sistema TRESS. Se utilizar� informaci�n enviada en el archivo.', 0, FLinea )
                                                                else
                                                                    FieldByName( 'CB_RFC' ).AsString := sRFC;
                                                           end;

                                                           { Validaci�n de d�gito de seguro social }
                                                           sTemporal := strTransAll( FRenglon[ 7 ], '-', '' );
                                                           FieldByName( 'CB_SEGSOC' ).AsString := Copy( sTemporal, 1, 11 );

                                                           { C�lculo del CURP y Comparaci�n }
                                                           sTemporal := strTransAll( FRenglon[ 9 ], '-', '' );
                                                           FieldByName( 'CB_CURP' ).AsString := Copy( sTemporal, 1, 18 );
                                                           sCURP := ZetaCommonTools.CalcCURP( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                                              FieldByName( 'CB_APE_MAT' ).AsString,
                                                                                              FieldByName( 'CB_NOMBRES' ).AsString,
                                                                                              FieldByName( 'CB_FEC_NAC' ).AsDateTime,
                                                                                              FieldByName( 'CB_SEXO' ).AsString,
                                                                                              dmTablas.GetEstadoCURP( FieldByName( 'CB_ENT_NAC' ).AsString ) );
                                                           if strLleno( sCURP ) and
                                                              strLleno( FieldByName( 'CB_CURP' ).AsString ) then
                                                           begin
                                                                if ( ansiUpperCase( sCURP ) <> ansiUpperCase( FieldByName( 'CB_CURP' ).AsString ) ) then
                                                                begin
                                                                     FieldByName( 'CB_CURP' ).AsString := sCURP;
                                                                     EscribeBitacora( 'CURP de empleado diferente al calculado por Sistema TRESS. Se actualizar� con el calculado.', FEmpleado, FLinea );
                                                                end;
                                                           end
                                                           else
                                                           begin
                                                                if strVacio( sCURP ) then
                                                                   EscribeError( 'CURP de empleado no pudo ser calculado por Sistema TRESS. Se utilizar� informaci�n enviada en el archivo.', 0, FLinea )
                                                                else
                                                                    FieldByName( 'CB_CURP' ).AsString := sCURP;
                                                           end;

                                                           sPadre := Copy( FRenglon[ 52 ], 1, 30 );
                                                           sMadre := Copy( FRenglon[ 53 ], 1, 30 );
                                                           dmRecursos.OtrosDatos := VarArrayOf( [ sPadre, sMadre ] );

                                                           if StrLleno( FRenglon[ 34 ] ) then
                                                           begin
                                                                sLista := Copy( FRenglon[ 34 ], 1, 2 );
                                                                if StrLleno( Copy( FRenglon[ 34 ], 3, 2 ) ) then
                                                                begin
                                                                     sLista := sLista + ',' + Copy( FRenglon[ 34 ], 3, 2 );
                                                                     if StrLleno( Copy( FRenglon[ 34 ], 5, 2 ) ) then
                                                                     begin
                                                                          sLista := sLista + ',' + Copy( FRenglon[ 34 ], 5, 2 );
                                                                          if StrLleno( Copy( FRenglon[ 34 ], 7, 2 ) ) then
                                                                          begin
                                                                               sLista := sLista + ',' + Copy( FRenglon[ 34 ], 7, 2 );
                                                                               if StrLleno( Copy( FRenglon[ 34 ], 9, 2 ) ) then
                                                                                  sLista := sLista + ',' + Copy( FRenglon[ 34 ], 9, 2 );
                                                                          end;
                                                                     end;
                                                                end;
                                                                dmRecursos.ListaPercepFijas := sLista;
                                                           end
                                                           else

                                                               dmRecursos.ListaPercepFijas := VACIO;
                                                           FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleado;
                                                           ValidaEnviar( dmCliente.cdsEmpleado );
                                                           if dmCliente.SetEmpleadoNumero( FEmpleado )then
                                                           begin
                                                                dmRecursos.RegistroAhorrosAutomaticos(K_ALTA);
                                                                FHuboCambios := TRUE;
                                                                EscribeBitacora( 'Se registr� exitosamente alta del empleado.', FEmpleado, FLinea );

                                                           end
                                                           else
                                                               EscribeBitacora( 'No se registr� alta de empleado. Revisar bit�cora de errores.', FEmpleado, FLinea );
                                                      end;
                                                   except
                                                         on Error : Exception do
                                                         begin
                                                              CancelUpdates;
                                                              EscribeError( Format( 'Error al procesar el alta: %s.', [ Error.Message ] ), 0, FLinea );
                                                         end;
                                                   end;
                                              end;
                                         end
                                         else
                                         begin
                                              EscribeError( 'El empleado ya existe, no se puede realizar alta', 0, FLinea );
                                              Application.ProcessMessages;
                                         end;
                                    end
                                    else
                                    begin
                                         EscribeError( 'El empleado ya existe, no se puede realizar alta', 0, FLinea );
                                         Application.ProcessMessages;
                                    end;
                               end;
                            finally
                                   FreeAndNil( FRenglon );
                            end;
                       end;
                    finally
                           FreeAndNil( FDatos );
                    end;
               end
               else
                   EscribeError( sMensajeError );
          end
          else
              EscribeError( 'No se cuenta con derecho para generar el alta, no se contin�a con el proceso.' );
     end
     else
         EscribeError( 'Empresa en modo DEMO, no se realiza el alta.' );
end;

function TdmInterfase.GetUsuarioAutomatiza:Boolean;
var
   iUsuario, iLongitud : Integer;
   lBloqueoSistema: WordBool;

begin
     Result := False;
     //Se conecta a usuarios
     cdsUsuarios.Conectar;
     cdsUsuarios.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );

     //Trae la clave 11
     dmCliente.GetSeguridad;
     iUsuario :=  dmCliente.DatosSeguridad.UsuarioTareasAutomaticas;

     if not (iUsuario = 0) then
     begin
          if cdsUsuarios.Locate( 'US_CODIGO',iUsuario, [] ) then
          begin
               //est� configurado el usuario correctamente
               Result := True;
               FUsuarioInterfaz := cdsUsuarios.FieldByName( 'US_CORTO' ).AsString;
               FClaveInterfaz := cdsUsuarios.FieldByName( 'US_PASSWRD' ).AsString;
          end
          else
               //est� configurado un usuario que no existe
               EscribeError( Format ( 'Usuario: %d no est� registrado en Sistema Tress', [ iUsuario ] ) )
     end
     else
     begin
          //No est� configurado ningun usuario
          EscribeError( 'No est� configurado Usuario para Tareas Autom�ticas!!' );
     end;
end;

function TdmInterfase.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;

end.

