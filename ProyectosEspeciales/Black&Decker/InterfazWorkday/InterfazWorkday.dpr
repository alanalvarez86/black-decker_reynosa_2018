program InterfazWorkday;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}
Uses  MidasLib,
  Forms,
  ComObj,
  ActiveX,
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseShell in '..\..\..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseImportaShell_DevEx in '..\..\..\Tools\ZBaseImportaShell_DevEx.pas' {BaseImportaShell_DevEx};

{$R *.RES}
{$R WindowsXP.res}
{$R ..\..\..\Traducciones\Spanish.res}

{procedure MuestraSplash;
begin
     SplashScreen := TSplashScreen.Create( Application );
     with SplashScreen do
     begin
          Show;
          Update;
     end;
end;

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     //if ( ParamCount = 0 ) then
        //MuestraSplash;
     Application.Title := 'Interfaz WorkDay - Sistema TRESS';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    //CierraSplash;
                    Show;                               
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    //CierraSplash;
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
