unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZBaseImportaShell,} ZBaseImportaShell_DevEx, {Psock, NMsmtp,} ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, FileCTRL, ZBaseConsulta, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, dxStatusBar, dxRibbonStatusBar, {ZBaseImportaShell,}
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBar, cxClasses, dxSkinsForm, cxGroupBox,
  cxButtons, cxTextEdit, cxMemo, cxPC, cxRadioGroup, cxShellBrowserDialog,
  dxBarBuiltInMenu, dxRibbonSkins, dxRibbonCustomizationForm, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdMessageClient, IdSMTPBase, IdSMTP, System.Actions, cxStyles, dxRibbon;

type
  TTressShell = class(TBaseImportaShell_DevEx)
    rgTipoMovimiento: TcxRadioGroup;
    BrowseDir: TcxShellBrowserDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    lModoBatch: Boolean;
    procedure SetNombreLogs;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure SetControlesBatch; override;
    procedure ProcesarArchivo; override;
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure RefrescaEmpleado;
    procedure CambiaEmpleadoActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaNavegacionInfo;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure ProcesarBatch; override;
  end;

var
  TressShell: TTressShell;

const
     K_PARAM_ARCHIVO = 2;
     K_PARAM_TIPO    = 3;

implementation

uses DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DTablas,
     DRecursos,
     DConsultas,
     DProcesos,
     DInterfase,
     DNomina,
     ZAsciiTools,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FCalendario,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaDialogo,
     ZetaServerTools;

{$R *.dfm}

procedure TTressShell.FormCreate(Sender: TObject);
const
     K_BATCH_SINTAXIS = '%s EMPRESA DIRECTORIO MOVIMIENTO';
begin
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmCliente.ModoSuper := True;
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmNomina   := TdmNomina.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
     inherited;
     EnviarCorreo := FALSE;
     SintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     lModoBatch := False;

end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmInterfase );
     FreeAndNil( dmNomina );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     FreeAndNil( Global );
     inherited;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;
        end;
        InitBitacora;
        CargaSistemaActivos;
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( 'Error al conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
     ActivaControles( FALSE );
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmConsultas );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoProcesar;
var
   oCursor: TCursor;
begin
     if( dmcliente.EsModoDemo ) then
     begin
           {if ModoBatch then
              AgregaBitacoraBatch ( 'No se puede realizar el proceso de importación en modo DEMO.' )
           else
               EscribeBitacora ( 'No se puede realizar el proceso de importación en modo DEMO.' );}
          EscribeError( 'No se puede realizar el proceso de importación en modo DEMO' );
     end
     else
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          SetNombreLogs;
          try
             with dmInterfase do
             begin
                  FormatoFecha := ifDDMMYYYYs;
                  Archivo := ArchOrigen.Text;
                  TipoMovimiento := eTipoMovimiento( rgTipoMovimiento.ItemIndex );
                  Procesa;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
     with dmCliente do
     begin
          if ( not ModoBatch ) then
          begin
               if lProcesando and ( BtnDetener.CanFocus ) then
                  BtnDetener.SetFocus
               else if EmpresaAbierta and ( ArchOrigen.CanFocus ) then
                       ArchOrigen.SetFocus
                    else if BtnSalir.CanFocus then
                            BtnSalir.SetFocus;
          end;
     end;
end;

procedure TTressShell.SetControlesBatch;
begin
     //inherited;
     ArchOrigen.Text := ParamStr( K_PARAM_ARCHIVO );
     rgTipoMovimiento.ItemIndex := StrToIntDef( ParamStr( K_PARAM_TIPO ), Ord( tmAlta ) );
     EnviarCorreo := FALSE;
     SetNombreLogs;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     RefrescaSistemaActivos;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre, sArchBit, sArchErr, sTipo: String;
begin
     with dmCliente do
     begin
          sTipo := rgTipoMovimiento.Properties.Items[ rgTipoMovimiento.ItemIndex ].Caption;
          sNombre := FechaToStr( FechaDefault ) + FormatDateTime( 'hhmmss', Time ) + '_' + sTipo + '.LOG';
          sArchBit := ZetaMessages.SetFileNameDefaultPath( 'BITACORA-' + sNombre );
          sArchErr := ZetaMessages.SetFileNameDefaultPath( 'ERRORES-' + sNombre );
     end;
     if StrVacio( ArchBitacora.Text )  then
        ArchBitacora.Text := sArchBit
     else if ( ArchBitacora.Text <> sArchBit ) then
          begin
               if ExtractFileDir( ArchBitacora.Text ) <> ExtractFileDir( sArchBit ) then
                  sArchBit := ZetaMessages.SetFileNamePath( ExtractFileDir( ArchBitacora.Text ), 'BITACORA-' + sNombre );
               ArchBitacora.Text := sArchBit;
          end;
     if StrVacio( ArchErrores.Text ) then
        ArchErrores.Text := sArchErr
     else if ( ArchErrores.Text <> sArchErr ) then
          begin
               if ExtractFileDir( ArchErrores.Text ) <> ExtractFileDir( sArchErr ) then
                  sArchErr := ZetaMessages.SetFileNamePath( ExtractFileDir( ArchErrores.Text ), 'ERRORES-' + sNombre );
               ArchErrores.Text := sArchErr;
          end;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell.zFechaValidDate(Sender: TObject);
begin
     inherited;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     SetNombreLogs;
end;

procedure TTressShell.ArchivoSeekClick(Sender: TObject);
var
   sDirectory : string;
begin
     sDirectory := ArchOrigen.Text;
     if strVacio(BrowseDir.Path) then
     begin
          BrowseDir.Path := ExtractFilePath( Application.ExeName );
     end;
     if BrowseDir.Execute then
     begin
          ArchOrigen.Text := BrowseDir.Path;
     end;
end;

procedure TTressShell.ProcesarBatch;
begin
     MinParamsBatch := 3;
     if dmInterfase.GetUsuarioAutomatiza then
     begin
          UsuarioInterfaz := dmInterfase.UsuarioInterfaz;
          ClaveInterfaz := ZetaServerTools.Decrypt( dmInterfase.ClaveInterfaz );
          inherited;
     end
     else
     begin
          ReportaErrorDialogo( VACIO, 'Usuario para tareas automáticas con error.' );
     end;
end;

procedure TTressShell.ProcesarArchivo;
begin
     if strLleno( ArchOrigen.Text ) then
     begin
          ActivaControles( TRUE );
          try
             InitBitacora;
             DoProcesar;
          finally
             EscribeBitacoras;
             ActivaControles( FALSE );
          end;
     end
     else
     begin
          ReportaErrorDialogo( self.Caption, 'Falta Especificar Ruta Origen' );
          ActiveControl := ArchOrigen;
     end;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }
procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     // No Implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     // No Implementar
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     // No Implementar
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
     //       Archivo_devEx.enabled := true;
   //  Ayuda_DevEx.Enabled := true;
end;


end.
