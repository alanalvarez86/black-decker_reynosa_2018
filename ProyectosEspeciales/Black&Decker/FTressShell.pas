unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseImportaShell_DevEx, ImgList, Menus, ActnList, ExtCtrls,
  StdCtrls, Buttons, ComCtrls, Mask, ZetaFecha, ZetaKeyCombo, ZetaTipoEntidad,
  ZetaMessages, ZetaNumero, IniFiles, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxRibbonCustomizationForm, System.Actions, cxStyles, cxClasses, dxSkinsForm,
  dxRibbon, dxStatusBar, dxRibbonStatusBar, dxBarBuiltInMenu, cxContainer,
  cxEdit, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdMessageClient, IdSMTPBase, IdSMTP, dxBar,
  cxGroupBox, cxButtons, cxTextEdit, cxMemo, cxPC;

type
  TTressShell = class(TBaseImportaShell_DevEx)
    LblFecha: TLabel;
    SistemaFechaZF: TZetaFecha;
    LblFormatoFechas: TLabel;
    cbFFecha: TZetaKeyCombo;
    Label2: TLabel;
    cbFormato: TZetaKeyCombo;
    _A_Configuracion: TAction;
    Valoresdeconfiguracin1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SistemaFechaZFValidDate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _A_ConfiguracionExecute(Sender: TObject);
  private
    { Private declarations }
    procedure CargaSistemaActivos;
    procedure RefrescaSistemaActivos;
    procedure SetNombreLogs;
    procedure GetPathArchivo;    
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
  protected
    { Protected declarations }
    procedure ActivaControles( const lProcesando: Boolean ); override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoProcesar; override;
    procedure SetControlesBatch; override;
  public
    { Public declarations }
    { Procedimientos y Funciones Publicos del Shell de Tress }
    function FormaActivaNomina: Boolean;
    procedure RefrescaEmpleado;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure CambiaEmpleadoActivos;
    procedure SetAsistencia( const dValue: TDate );
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure CreaArbol( const lEjecutar: Boolean );
    procedure CargaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure SetBloqueo;
    procedure RefrescaNavegacionInfo;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure CambiaSistemaActivos;
    procedure CambiaPeriodoActivos;    
  end;

const
     { Constantes de parametros de la interfase }
     K_PARAM_ARCHIVO       = 4;
     K_PARAM_FORMATO_FECHA = 5;
     K_PARAM_FECHA         = 6;

var
  TressShell: TTressShell;

implementation

uses DCliente, DGlobal, DSistema, DCatalogos, DTablas,
     DRecursos, DConsultas, DProcesos, DInterfase,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     FConfiguracion,
     FCalendario;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
const
     K_BATCH_SINTAXIS = '%s EMPRESA USUARIO PASSWORD [ARCHIVO] [FORMATO-FECHA] [FECHA-DEFAULT]';
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     inherited;
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmInterfase := TdmInterfase.Create( Self );
     Configuracion := TConfiguracion.Create ( Self );
     { Propiedades que se cambiaron de la base }
     MinParamsBatch := 3;
     EnviarCorreo := FALSE;
     SintaxisBatch := Format( K_BATCH_SINTAXIS, [ ExtractFileName( Application.ExeName ) ] );
     cbFormato.ItemIndex := Ord( faASCIIDel );
     cbFFecha.ItemIndex := Ord( ifMMDDYYYYs );
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     FreeAndNil( Configuracion );
     FreeAndNil( dmInterfase );
     FreeAndNil( dmProcesos );
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     inherited;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     GetPathArchivo;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        with dmCliente do
        begin
             InitActivosSistema;
             InitActivosAsistencia;
             InitActivosEmpleado;
             InitActivosPeriodo;
        end;
        InitBitacora;
        CargaSistemaActivos;
     except
        on Error : Exception do
        begin
             ReportaErrorDialogo( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message );
             DoCloseAll;
        end;
     end;
     ActivaControles( FALSE );
end;

procedure TTressShell.DoCloseAll;
begin
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          ZetaClientTools.CierraDatasets( dmProcesos );
          ZetaClientTools.CierraDatasets( dmRecursos );
          ZetaClientTools.CierraDatasets( dmConsultas );
          ZetaClientTools.CierraDatasets( dmTablas );
          ZetaClientTools.CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
end;

procedure TTressShell.DoProcesar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmInterfase do
        begin
             Formato         := eFormatoASCII( cbFormato.Valor );
             FormatoFecha    := eFormatoImpFecha( cbFFecha.Valor );
             Archivo         := ArchOrigen.Text;
             Procesa;
             DeleteFile( Archivo )
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.ActivaControles(const lProcesando: Boolean);
begin
     inherited;
     with dmCliente do
     begin
          LblFecha.Enabled       := EmpresaAbierta and ( not lProcesando );
          SistemaFechaZF.Enabled := EmpresaAbierta and ( not lProcesando );
          if ( not ModoBatch ) then
          begin
               if lProcesando and ( BtnDetener.CanFocus ) then
                  BtnDetener.SetFocus
               else if EmpresaAbierta and ( ArchOrigen.CanFocus ) then
                  ArchOrigen.SetFocus
               else if BtnSalir.CanFocus then
                  BtnSalir.SetFocus;
          end;
     end;
end;

procedure TTressShell.SetControlesBatch;
begin
     inherited;
     EnviarCorreo         := TRUE;
     cbFFecha.ItemIndex   := StrToIntDef( ParamStr( K_PARAM_FORMATO_FECHA ), Ord( ifMMDDYYYYs ) );
     SistemaFechaZF.Valor := dmCliente.FechaDefault + StrToIntDef( ParamStr( K_PARAM_FECHA ), 0 );
     if StrVacio( ParamStr( K_PARAM_ARCHIVO ) ) then
        GetPathArchivo;
     SetNombreLogs;
end;

procedure TTressShell.CargaSistemaActivos;
begin
     SistemaFechaZF.Valor := dmCliente.FechaDefault;
     RefrescaSistemaActivos;
end;

procedure TTressShell.SistemaFechaZFValidDate(Sender: TObject);
begin
     inherited;
     dmCliente.FechaDefault := SistemaFechaZF.Valor;
     RefrescaSistemaActivos;
end;

procedure TTressShell.RefrescaSistemaActivos;
begin
     with SistemaFechaZF do
     begin
          SistemaFechaDia.Caption := ZetaCommonTools.DiaSemana( Valor );
          FCalendario.SetDefaultDlgFecha( Valor ); { Refresca el Default del Lookup de ZetaFecha }
     end;
     SetNombreLogs;
end;

procedure TTressShell.SetNombreLogs;
var
   sNombre: String;
begin
     with dmCliente do
     begin
          sNombre := GetDatosEmpresaActiva.Nombre + '-' + FechaToStr( FechaDefault ) + FormatDateTime( 'hhmm', Time ) + '.LOG';
          ArchBitacora.Text := ZetaMessages.SetFileNameDefaultPath( 'BIT-' + sNombre );
          ArchErrores.Text := ZetaMessages.SetFileNameDefaultPath( 'ERR-' + sNombre );
     end;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.GetPathArchivo;
begin
     ArchOrigen.Text :=  Configuracion.IniValues.DirectorioSesiones + '\' + FormatDateTime( 'ddmmyyyy', Date ) + '.log';
end;

procedure TTressShell._A_ConfiguracionExecute(Sender: TObject);
var
   oForma: TConfiguracion;
begin
     oForma := Tconfiguracion.Create( Self );
     try
        with oForma do
        begin
             ShowModal;
             if ModalResult = mrOK then
                GetPathArchivo;
        end;
     finally
            FreeAndNil( oForma );
     end;
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmInterfase.HandleProcessEnd( WParam, LParam );
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }

procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;

function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
//
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
//
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     // No implementar
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     // No implementar
end;

end.
