unit FConfiguracion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, Mask, ZetaNumero, ZetaAsciiFile, IniFiles, FileCtrl;

type
  TTArchivosIni = class( TObject )
  private
    { Private declarations }
    FiniFile: TiniFile;
    function GetDirectorioSesiones: string;
    procedure SetDirectorioSesiones(const Value: string);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property DirectorioSesiones: string read GetDirectorioSesiones write SetDirectorioSesiones;
  end;
  TConfiguracion = class(TZetaDlgModal)
    Label1: TLabel;
    DirectorioSesiones: TEdit;
    Buscar: TSpeedButton;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
  private
    { Private declarations }
    FIniValues: TTArchivosIni;
  public
    { Public declarations }
    property IniValues: TTArchivosIni read FIniValues;
  end;

var
  Configuracion: TConfiguracion;

implementation

{$R *.DFM}

const
     { Constantes para el archivo de inicio }
     K_DIRECTORIO  = 'DIRECTORIO';
     K_PATH        = 'Path';


{ TTArchivosIni }

constructor TTArchivosIni.Create;
begin
     FIniFile := TIniFile.Create( ChangeFileExt( Application.ExeName, '.INI' ) );
end;

destructor TTArchivosIni.Destroy;
begin
     FreeAndNil( FIniFile );
     inherited;
end;

procedure TConfiguracion.FormCreate(Sender: TObject);
begin
     inherited;
     FIniValues := TTArchivosIni.Create;
end;

procedure TConfiguracion.FormShow(Sender: TObject);
begin
     inherited;
     Self.DirectorioSesiones.Text := FIniValues.DirectorioSesiones;
end;

procedure TConfiguracion.BuscarClick(Sender: TObject);
var
   sDirectory: String;
begin
     sDirectory := DirectorioSesiones.Text;

     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        DirectorioSesiones.Text := sDirectory;
end;

procedure TConfiguracion.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FIniValues.DirectorioSesiones := Self.DirectorioSesiones.Text;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TTArchivosIni.GetDirectorioSesiones: string;
begin
     Result := FIniFile.ReadString( K_DIRECTORIO, K_PATH, 'C:\Documents and Settings\TRESS\My Documents\Pocket_PC My Documents\' );
end;

procedure TTArchivosIni.SetDirectorioSesiones(const Value: string);
begin
     FIniFile.WriteString( K_DIRECTORIO, K_PATH, Value );
end;

end.
