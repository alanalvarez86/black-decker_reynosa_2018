unit FTipoPrestamoAhorro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, ExtCtrls, Buttons, ZetaKeyLookup;

type
  TTipoPrestamoAhorro = class(TZetaDlgModal)
    Label1: TLabel;
    lkTipoPrestamo: TZetaKeyLookup;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TipoPrestamoAhorro: TTipoPrestamoAhorro;

implementation

uses
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaDialogo,
    DCajaAhorro;

{$R *.dfm}

procedure TTipoPrestamoAhorro.OKClick(Sender: TObject);
var
   iTipoPrestamo: integer;
   lActivo: boolean;
begin
     inherited;
     if strLLeno( lkTipoPrestamo.Llave ) then
     begin
          iTipoPrestamo := dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger;
          lActivo := False;
          case iTipoPrestamo of
               1: lActivo := ( dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'AH_STATUS_CAJA' ).AsInteger = 0 );
               2: lActivo := ( dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'AH_STATUS_FONDO' ).AsInteger = 0 );
          end;
          if lActivo then
          begin
               if dmCajaAhorro.ExistenPrestamosActivos( iTipoPrestamo ) then
               begin
                    { Caja de Ahorro }
                    if ( iTipoPrestamo = 1 ) then
                       ZetaDialogo.ZError( 'Caja Ahorro', 'Ya existe un pr�stamo activo. No es posible registrar nuevo pr�stamo', 0 )
                    else { Fondo de Ahorro }
                        ZetaDialogo.ZError( 'Fondo Ahorro', 'Ya se le registro pr�stamo. No es posible registrar nuevo pr�stamo', 0 );
               end
               else
               begin
                    case iTipoPrestamo of
                         1:{ Caja de Ahorro }
                         begin
                              if ( dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat > 0 ) then
                                 dmCajaAhorro.cdsHisPrestamos.Agregar
                              else
                                  ZetaDialogo.ZError( 'Caja Ahorro', 'El monto total de los dos ahorros debe ser mayor a cero', 0 );
                         end;
                         2:{ Fondo de Ahorro }
                         begin
                              if ( dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'SubTotalFondoAhorro' ).AsFloat > 0 ) then
                                 dmCajaAhorro.cdsHisPrestamos.Agregar
                              else
                                  ZetaDialogo.ZError( 'Fondo Ahorro', 'El monto total del fondo de ahorro debe ser mayor a cero', 0 );
                         end;
                    end;
               end;
          end
          else
          begin
               case iTipoPrestamo of
                    1: ZetaDialogo.ZError( 'Caja Ahorro', 'Empleado no inscrito en Caja de Ahorro, No se le puede realizar el pr�stamo', 0 );
                    2: ZetaDialogo.ZError( 'Fondo Ahorro', 'Empleado no inscrito en Fondo de Ahorro, No se le puede realizar el pr�stamo', 0 );
               end;
          end;
     end
     else
         ZetaDialogo.ZError( 'Caja de Ahorro', 'No se puede dejar vac�o el c�digo del Ahorro', 0 );
end;

procedure TTipoPrestamoAhorro.FormCreate(Sender: TObject);
begin
     lkTipoPrestamo.LookupDataset := dmCajaAhorro.cdsTAhorro;
end;

procedure TTipoPrestamoAhorro.FormShow(Sender: TObject);
begin
     inherited;
     with dmCajaAhorro do
     begin
          FiltroCatalogo := ' TB_NUMERO = 1 or TB_NUMERO = 2 ';
          cdsTAhorro.Refrescar;
     end;
end;

end.
