inherited EditRepartirIntereses: TEditRepartirIntereses
  Left = 199
  Top = 200
  Width = 844
  Height = 311
  Caption = 'Repartici'#243'n de intereses'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 241
    Width = 836
    inherited OK: TBitBtn
      Left = 679
      Top = 7
      Anchors = [akRight, akBottom]
    end
    inherited Cancelar: TBitBtn
      Left = 757
      Top = 7
      Anchors = [akRight, akBottom]
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 836
    Height = 241
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'Empleado'
        Width = 66
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMBRE'
        Title.Caption = 'Nombre'
        Width = 154
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AHORRO_SEMANAL'
        Title.Caption = 'Ahorrado Sem.'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'AHORRO_ACUMULADO'
        Title.Caption = 'Ahorro Acum.'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INTERES_ACUMULADO'
        Title.Caption = 'Intereses Acum.'
        Width = 84
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COMISIONES_ACUMULADO'
        Title.Caption = 'Comisiones Acum.'
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CAPITAL'
        Title.Caption = 'Capital'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INTERESES_INV'
        Title.Caption = 'Int. S/Inversi'#243'n'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'INTERESES_PRESTAMO'
        Title.Caption = 'Int. S/Prestamos'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MONTO_COMISIONES'
        Title.Caption = 'Monto X Comisiones'
        Width = 108
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 432
    Top = 152
  end
end
