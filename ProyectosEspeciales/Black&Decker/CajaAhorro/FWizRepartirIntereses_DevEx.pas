unit FWizRepartirIntereses_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaKeyCombo, Mask, ZetaNumero,
  ZetaCommonClasses,ZetaCommonLists, Grids, DBGrids, ZetaDBGrid,
  ZetaDBTextBox, DB,ZcxBaseWizard,ZetaCxWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, {ZBaseWizardFiltro_DevEx,}ZetaKeyLookup_DevEx,
  ZCXBaseWizardFiltro, Menus, cxRadioGroup, cxTextEdit, cxMemo, cxButtons,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, ZetaCXGrid;
  
type
  TWizRepartirIntereses_DevEx = class(TBaseCXWizardFiltro)
    AH_AHORRO: TZetaKeyLookup_DevEx;
    InteresesXinversion: TZetaNumero;
    Label1: TLabel;
    Label2: TLabel;
    gbNomina_DevEx: TcxGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    PE_TIPO: TZetaKeyCombo;
    PE_YEAR: TZetaNumero;
    PE_NUMERO: TZetaKeyLookup_DevEx;
    Panel1: TPanel;
    ZetaDBGrid1: TZetaDBGrid;
    Panel2: TPanel;
    btnEditarRegistros_DevEx: TcxButton;
    Label3: TLabel;
    InteresSobreInversion: TZetaTextBox;
    Label4: TLabel;
    InteresesSobrePrestamos: TZetaTextBox;
    Label8: TLabel;
    CapitalTotal: TZetaTextBox;
    dsGrid: TDataSource;
    Label9: TLabel;
    Empleados: TZetaTextBox;
    Label10: TLabel;
    Nomina: TZetaTextBox;
    Label11: TLabel;
    TasaSobreInversion: TZetaTextBox;
    TasaSobrePrestamos: TZetaTextBox;
    Label12: TLabel;
    Label13: TLabel;
    MontoComisiones: TZetaNumero;
    Label14: TLabel;
    MontoXComision: TZetaTextBox;
    Label15: TLabel;
    TasaXComisiones: TZetaTextBox;
    Label16: TLabel;
    MontoXPrestamos: TZetaNumero;
    EditarRegistros: TdxWizardControlPage;
    ZetaCXGrid1: TZetaCXGrid;
    ZetaCXGrid1Level1: TcxGridLevel;
    ZetaCXGrid1DBTableView1: TcxGridDBTableView;
    CB_CODIGO: TcxGridDBColumn;
    NOMBRE: TcxGridDBColumn;
    AHORRO_ACUMULADO: TcxGridDBColumn;
    INTERES_ACUMULADO: TcxGridDBColumn;
    COMISIONES_ACUMULADO: TcxGridDBColumn;
    CAPITAL: TcxGridDBColumn;
    INTERESES_INV: TcxGridDBColumn;
    INTERESES_PRESTAMO: TcxGridDBColumn;
    MONTO_COMISIONES: TcxGridDBColumn;
    cxGroupBox2: TcxGroupBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure PE_YEARExit(Sender: TObject);
    procedure btnEditarRegistros_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CargaParametros;override;
  private
    { Private declarations }
    FParams : TZetaParams;
    procedure SetFiltroPeriodo(const iYear: Integer;const TipoPeriodo: eTipoPeriodo);
  protected
    { Public declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizRepartirIntereses_DevEx : TWizRepartirIntereses_DevEx;

implementation

uses
    DCliente,    
    DCajaAhorro,
    ZetaDialogo,
    ZBasicoSelectGrid_DevEx,
    ZetaCommonTools,
    FEditRepartirIntereses_DevEx;

{$R *.dfm}

procedure TWizRepartirIntereses_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FParams := TZetaParams.Create;
     PE_NUMERO.LookupDataSet := dmCajaAhorro.cdsPeriodoProcesos;
     AH_AHORRO.LookUpDataset := dmCajaAhorro.cdsTAhorro;
     dmCajaAhorro.cdsTAhorro.Conectar;
     dmCajaAhorro.cdsPeriodoProcesos.Conectar;

     with dmCliente.GetDatosPeriodoActivo do
     begin
          PE_YEAR.Valor := Year;
          PE_TIPO.Valor := Ord( Tipo );
          PE_NUMERO.Valor := Numero;
          SetFiltroPeriodo( Year, Tipo );
     end;
end;

procedure TWizRepartirIntereses_DevEx.WizardBeforeMove(Sender: TObject;var iNewPage: Integer; var CanMove: Boolean);
var
   oParams : TZetaParams;
   oCursor: TCursor;
   rTasaInversion, rTasaPrestamos, rTasaComisiones: Single;
begin
     inherited;
      if Wizard.Adelante then
     begin
          if WizardControl.ActivePage= Parametros  then
          begin
               FParams.AddInteger('Year', PE_YEAR.ValorEntero );
               FParams.AddInteger( 'Tipo', PE_TIPO.Valor );
               if strVacio( PE_NUMERO.Llave ) then
                  CanMove := Error( 'N�mero de n�mina no puede quedar vac�o', PE_NUMERO )
               else
               begin
                    FParams.AddInteger('Numero', PE_NUMERO.Valor );
                    if strVacio( AH_AHORRO.Llave ) then
                       CanMove :=  Error( 'Tipo de ahorro no puede quedar vac�o', AH_AHORRO )
                    else
                    begin
                         FParams.AddString( 'TipoAhorro', AH_AHORRO.Llave );
                         if ( InteresesXinversion.Valor = 0 ) then
                            CanMove := Error( 'Los intereses por inversi�n no pueden ser cero', InteresesXInversion )
                         else
                             FParams.AddFloat( 'InteresXinversion', InteresesXinversion.Valor );
                    end;
               end;
               if CanMove then
               begin
                    oCursor := Screen.Cursor;
                    Screen.Cursor := crHourglass;
                    try
                       oParams := TZetaParams.Create;
                       try
                          oParams.VarValues := dmCajaAhorro.GeTotalesRepartoIntereses( FParams );
                          InteresSobreInversion.Caption := FormatFloat( '#,##0.00', InteresesXinversion.Valor );
                          InteresesSobrePrestamos.Caption := FormatFloat( '#,##0.00', MontoXPrestamos.Valor );
                          MontoXComision.Caption := FormatFloat( '#,##0.00', MontoComisiones.Valor );
                          CapitalTotal.Caption := FormatFloat( '#,##0.00', oParams.ParamByName( 'Capital' ).AsFloat );
                          { Se Obtenien las Tasas de Inversi�n y de Prestamos }
                          if ( oParams.ParamByName( 'Capital' ).AsFloat > 0 ) then
                          begin
                               rTasaInversion := InteresesXinversion.Valor / oParams.ParamByName( 'Capital' ).AsFloat;
                               rTasaPrestamos := MontoXPrestamos.Valor / oParams.ParamByName( 'Capital' ).AsFloat;
                               rTasaComisiones := MontoComisiones.Valor / oParams.ParamByName( 'Capital' ).AsFloat;
                          end
                          else
                          begin
                               rTasaInversion := 0;
                               rTasaPrestamos := 0;
                               rTasaComisiones := 0;
                          end;
                          FParams.AddFloat( 'InteresesXPrestamos', MontoXPrestamos.Valor );
                          FParams.AddFloat( 'TasaInversion', rTasaInversion );
                          FParams.AddFloat( 'TasaPrestamos', rTasaPrestamos );
                          FParams.AddFloat( 'TasaComisiones', rTasaComisiones );
                          FParams.AddFloat( 'MontoInversion', InteresesXinversion.Valor );
                          FParams.AddFloat( 'MontoPrestamos', MontoXPrestamos.Valor );
                          FParams.AddFloat( 'MontoComisiones', MontoComisiones.Valor );
                          FParams.AddFloat( 'MontoCapital', oParams.ParamByName( 'Capital' ).AsFloat );
                          TasaSobreInversion.Caption := FormatFloat( '0.##### %', rTasaInversion );
                          TasaSobrePrestamos.Caption := FormatFloat( '0.##### %', rTasaPrestamos );
                          TasaXComisiones.Caption := FormatFloat( '0.##### %', rTasaComisiones );
                          with dmCajaAhorro do
                          begin
                               GetRepartoIntereses( FParams );
                               dsGrid.DataSet := cdsRepartoIntereses;
                               ZetaCxGrid1DBTableView1.ApplyBestFit();
                          end;
                          Empleados.Caption := IntToStr( dmCajaAhorro.cdsRepartoIntereses.RecordCount );
                          Nomina.Caption := ShowNomina( PE_YEAR.ValorEntero, PE_TIPO.Valor, PE_NUMERO.Valor )
                       finally
                              FreeAndNil(oParams)
                       end;
                    finally
                           Screen.Cursor := oCursor;
                    end;
               end;
          end
     end;
end;

procedure TWizRepartirIntereses_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FParams );
end;

procedure TWizRepartirIntereses_DevEx.SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodoProcesos.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodoProcesos.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodoProcesos.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodoProcesos( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodoProcesos( iYear, TipoPeriodo );

          with PE_NUMERO do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
                  Valor := dmCliente.GetDatosPeriodoActivo.Numero
               else
                   Valor := 1;
          end;
     end;
end;

procedure TWizRepartirIntereses_DevEx.PE_YEARExit(Sender: TObject);
begin
     inherited;
     SetFiltroPeriodo( PE_YEAR.ValorEntero, eTipoPeriodo( PE_TIPO.Valor ) );
end;

procedure TWizRepartirIntereses_DevEx.btnEditarRegistros_DevExClick(Sender: TObject);
begin
     inherited;
     ZBasicoSelectGrid_DevEx.GridSelectBasico( dmCajaAhorro.cdsRepartoIntereses,TEditRepartirIntereses_DevEx, FALSE );
end;

function TWizRepartirIntereses_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmCajaAhorro.GrabarIntereses( FParams );
     if Result then
        ZetaDialogo.ZInformation( '�Proceso Terminado!', 'Revisar Montos Calculados en el Ahorro de los Empleados', 0  )
     else
         ZetaDialogo.ZInformation( '�Proceso Terminado!', 'No hay empleados a procesar', 0 );
end;

procedure TWizRepartirIntereses_DevEx.FormShow(Sender: TObject);
begin

      inherited;
      Parametros.PageIndex := 0;
      EditarRegistros.PageIndex:=2;
      FiltrosCondiciones.PageIndex := 1;
      Ejecucion.PageIndex := 3;



      Advertencia.Caption:='Advertencia, los datos calculados para el reparto de intereses se aplicar�n en el ahorro de los empleados como un abono al mismo.';

     with dmCajaAhorro do
     begin
          FiltroCatalogo := ' TB_NUMERO = 1 or TB_NUMERO = 2 ';
          cdsTAhorro.Refrescar;
     end;
     AH_AHORRO.SetFocus;
end;

procedure TWizRepartirIntereses_DevEx.CargaParametros;
begin

     inherited;
     with ParameterList do
     begin
          AddInteger( 'Anio',PE_YEAR.ValorEntero );
          AddInteger( 'Tipo',  PE_TIPO.Valor );
          AddInteger('Numero', PE_NUMERO.Valor);
          AddString ( 'TipoAhorro', AH_AHORRO.Llave );
          AddFloat( 'MontoInversion', InteresesXinversion.Valor );
          AddFloat( 'MontoPrestamos', MontoXPrestamos.Valor );
          AddFloat( 'MontoComisiones', MontoComisiones.Valor );
     end;

     with Descripciones do
     begin
          AddInteger( 'A�o',PE_YEAR.ValorEntero );
          AddString( 'Tipo',  PE_TIPO.Text);
          AddInteger('N�mero', PE_NUMERO.Valor);
          AddString ( 'Tipo de ahorro', AH_AHORRO.Descripcion );
          AddFloat( 'Monto por inversi�n', InteresesXinversion.Valor );
          AddFloat( 'Monto por pr�stamos', MontoXPrestamos.Valor );
          AddFloat( 'Monto por comisiones', MontoComisiones.Valor );
     end;

end;



end.
