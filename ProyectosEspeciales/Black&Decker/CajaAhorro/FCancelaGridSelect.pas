unit FCancelaGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls;

type
  TCancelaGridSelect = class(TBaseGridSelect)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CancelaGridSelect: TCancelaGridSelect;

implementation

{$R *.DFM}

end.
