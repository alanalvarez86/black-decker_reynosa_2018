inherited SaldosTotalesEmpleado: TSaldosTotalesEmpleado
  Left = 215
  Top = 115
  Caption = 'Saldos Totales de Empleado'
  ClientHeight = 531
  ClientWidth = 904
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 904
    inherited ValorActivo2: TPanel
      Width = 645
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 904
    Height = 320
    Align = alTop
    TabOrder = 1
    object GroupBox1: TGroupBox
      Left = 304
      Top = 2
      Width = 292
      Height = 217
      Caption = ' Fondo de Ahorro '
      Color = clBtnFace
      ParentColor = False
      TabOrder = 0
      object Label18: TLabel
        Left = 104
        Top = 62
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ahorrado:'
      end
      object Label19: TLabel
        Left = 18
        Top = 150
        Width = 132
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de Pr'#233'stamos Capital:'
      end
      object AH_SALDO: TZetaDBTextBox
        Left = 154
        Top = 58
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_SALDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalFondoAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PR_SALDO: TZetaDBTextBox
        Left = 154
        Top = 146
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PR_SALDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosFondoCapital'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 22
        Top = 194
        Width = 128
        Height = 13
        Alignment = taRightJustify
        Caption = 'SubTotal Fondo de Ahorro:'
      end
      object SubTotalFondoAhorro: TZetaDBTextBox
        Left = 154
        Top = 190
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SubTotalFondoAhorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SubTotalFondoAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 117
        Top = 18
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object AH_STATUS_FONDO: TZetaDBTextBox
        Left = 154
        Top = 14
        Width = 115
        Height = 21
        AutoSize = False
        Caption = 'AH_STATUS_FONDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_STATUS_FONDO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 18
        Top = 172
        Width = 132
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de Pr'#233'stamos Inter'#233's:'
      end
      object SaldoPrestamosFondoInteres: TZetaDBTextBox
        Left = 154
        Top = 168
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoPrestamosFondoInteres'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosFondoInteres'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 52
        Top = 128
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aportaci'#243'n Empresa:'
      end
      object TotalFondoAhorro: TZetaDBTextBox
        Left = 154
        Top = 124
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'TotalFondoAhorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalFondoAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 270
        Top = 128
        Width = 16
        Height = 13
        Caption = '(+)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label13: TLabel
        Left = 270
        Top = 149
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 270
        Top = 171
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label16: TLabel
        Left = 270
        Top = 192
        Width = 16
        Height = 13
        Caption = '(=)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label20: TLabel
        Left = 58
        Top = 84
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Intereses Ganados:'
      end
      object InteresesFA: TZetaDBTextBox
        Left = 154
        Top = 80
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'InteresesFA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalInteresesFA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 44
        Top = 106
        Width = 106
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por comisiones:'
      end
      object ComisionesAcumFA: TZetaDBTextBox
        Left = 154
        Top = 102
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ComisionesAcumFA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'ComisionesAcumFA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 270
        Top = 106
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 270
        Top = 84
        Width = 16
        Height = 13
        Caption = '(+)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label29: TLabel
        Left = 74
        Top = 40
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Inicio:'
      end
      object AH_FECHA_INI_FONDO: TZetaDBTextBox
        Left = 154
        Top = 36
        Width = 115
        Height = 21
        AutoSize = False
        Caption = 'AH_FECHA_INI_FONDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_FECHA_INI_FONDO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object GroupBox2: TGroupBox
      Left = 5
      Top = 2
      Width = 293
      Height = 217
      Caption = ' Caja de Ahorro '
      Color = clBtnFace
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 104
        Top = 62
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ahorrado:'
      end
      object SALDO: TZetaDBTextBox
        Left = 154
        Top = 58
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SALDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalCajaAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 18
        Top = 128
        Width = 132
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de Pr'#233'stamos Capital:'
      end
      object SaldoPrestamosCajaCapital: TZetaDBTextBox
        Left = 154
        Top = 124
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoPrestamosCajaCapital'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosCajaCapital'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 31
        Top = 172
        Width = 119
        Height = 13
        Alignment = taRightJustify
        Caption = 'SubTotal Caja de Ahorro:'
      end
      object SubTotalCajaAhorro: TZetaDBTextBox
        Left = 154
        Top = 168
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SubTotalCajaAhorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SubTotalCajaAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 117
        Top = 18
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object AH_STATUS: TZetaDBTextBox
        Left = 154
        Top = 14
        Width = 115
        Height = 21
        AutoSize = False
        Caption = 'AH_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_STATUS_CAJA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label8: TLabel
        Left = 18
        Top = 150
        Width = 132
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de Pr'#233'stamos Inter'#233's:'
      end
      object SaldoPrestamosCajaInteres: TZetaDBTextBox
        Left = 154
        Top = 146
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoPrestamosCajaInteres'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosCajaInteres'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 270
        Top = 127
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 270
        Top = 149
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 270
        Top = 170
        Width = 16
        Height = 13
        Caption = '(=)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 58
        Top = 84
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Intereses Ganados:'
      end
      object InteresesCA: TZetaDBTextBox
        Left = 154
        Top = 80
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'InteresesCA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalInteresesCA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 44
        Top = 106
        Width = 106
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por comisiones:'
      end
      object ComisionesAcumCA: TZetaDBTextBox
        Left = 154
        Top = 102
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ComisionesAcumCA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'ComisionesAcumCA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 270
        Top = 106
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 270
        Top = 84
        Width = 16
        Height = 13
        Caption = '(+)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label28: TLabel
        Left = 74
        Top = 40
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Inicio:'
      end
      object AH_FECHA_INI_CAJA: TZetaDBTextBox
        Left = 154
        Top = 36
        Width = 115
        Height = 21
        AutoSize = False
        Caption = 'AH_FECHA_INI_CAJA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_FECHA_INI_CAJA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object GroupBox3: TGroupBox
      Left = 222
      Top = 266
      Width = 172
      Height = 50
      Caption = ' Saldo Disponible '
      TabOrder = 2
      object Label25: TLabel
        Left = 13
        Top = 24
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Neto:'
      end
      object AH_NETO: TZetaDBTextBox
        Left = 43
        Top = 20
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_NETO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Neto'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object gbAval: TGroupBox
      Left = 6
      Top = 221
      Width = 590
      Height = 42
      Caption = ' Aval '
      TabOrder = 3
      object Label30: TLabel
        Left = 330
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Monto:'
      end
      object MontoAval: TZetaDBTextBox
        Left = 366
        Top = 12
        Width = 85
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'MontoAval'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'MontoAval'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 11
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Empleado:'
      end
      object EmpleadoAval: TZetaDBTextBox
        Left = 64
        Top = 12
        Width = 262
        Height = 21
        AutoSize = False
        Caption = 'EmpleadoAval'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'EmpleadoAval'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 459
        Top = 16
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object FechaAval: TZetaDBTextBox
        Left = 496
        Top = 12
        Width = 85
        Height = 21
        AutoSize = False
        Caption = 'FechaAval'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'FechaAval'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object Panel2: TPanel [2]
    Left = 0
    Top = 339
    Width = 904
    Height = 192
    Align = alClient
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 902
      Height = 32
      Align = alTop
      TabOrder = 0
      object BBAgregar: TBitBtn
        Left = 8
        Top = 3
        Width = 122
        Height = 25
        Hint = 'Registrar un pr'#233'stamo'
        Caption = 'Prestar'
        TabOrder = 0
        OnClick = BBAgregarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BBModificar: TBitBtn
        Left = 131
        Top = 3
        Width = 122
        Height = 25
        Hint = 'Modificar el pr'#233'stamo seleccionado'
        Caption = 'Modificar pr'#233'stamo'
        TabOrder = 1
        OnClick = BBModificarClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
      end
      object BBBorrar: TBitBtn
        Left = 254
        Top = 3
        Width = 122
        Height = 25
        Hint = 'Borrar el pr'#233'stamo seleccionado'
        Caption = 'Borrar pr'#233'stamo'
        TabOrder = 2
        OnClick = BBBorrarClick
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55558888888585599958555555555550305555555550055BB0555555550FB000
          0055555550FB0BF0F05555550FBFBF0FB05555550BFBF0FB005555500FBFBFB0
          B055550E0BFBFB00B05550EEE0BFB0B0B055000EEE0BFBF0B0550000EEE00000
          B05500000E055550705550000055555505555500055555550555}
      end
    end
    object GridRenglones: TZetaDBGrid
      Left = 1
      Top = 33
      Width = 902
      Height = 158
      Align = alClient
      DataSource = dsRenglon
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'PR_TIPO'
          Title.Caption = 'C'#243'digo'
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TB_ELEMENT'
          Title.Caption = 'Descripci'#243'n'
          Width = 196
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_REFEREN'
          Title.Caption = 'Referencia'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_FECHA'
          Title.Caption = 'Fecha'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_MONTO'
          Title.Caption = 'Monto'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_PAGOS'
          Title.Caption = 'Pagos'
          Width = 74
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_SALDO'
          Title.Caption = 'Saldo'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PR_STATUS'
          Title.Caption = 'Estatus'
          Width = 86
          Visible = True
        end>
    end
  end
  inherited DataSource: TDataSource
    Left = 40
    Top = 48
  end
  object dsRenglon: TDataSource
    Left = 168
    Top = 331
  end
end
