inherited CancelaGridSelect: TCancelaGridSelect
  Left = 261
  Top = 208
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Empleados a Cancelar Reparto de Intereses'
  ClientHeight = 325
  ClientWidth = 655
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 289
    Width = 655
    inherited OK: TBitBtn
      Left = 498
      Anchors = [akRight, akBottom]
    end
    inherited Cancelar: TBitBtn
      Left = 576
      Anchors = [akRight, akBottom]
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 655
    Height = 254
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        ReadOnly = True
        Title.Caption = 'C'#243'digo'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Empleado'
        Width = 263
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Tipo de Ahorro'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CR_FECHA'
        Title.Caption = 'Fecha de Registro'
        Width = 103
        Visible = True
      end>
  end
  inherited PanelSuperior: TPanel
    Width = 655
  end
  inherited DataSource: TDataSource
    Left = 224
    Top = 56
  end
end
