unit FCancelaGridSelect_DevEx;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid_DevEx, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, ZBaseSelectGrid_DevEx,ZBasicoSelectGrid_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons,
  cxTextEdit;

type
  TCancelaGridSelect_DevEx = class(TBasicoGridSelect_DevEx)
    PanelSuperior: TPanel;
    PistaLBL: TLabel;
    Pista: TEdit;
    BtnFiltrar_DevEx: TcxButton;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    CR_FECHA: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CancelaGridSelect_DevEx: TCancelaGridSelect_DevEx;

implementation

{$R *.DFM}

end.
