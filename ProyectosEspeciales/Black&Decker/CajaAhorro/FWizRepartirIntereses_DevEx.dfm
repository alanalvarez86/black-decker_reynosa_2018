inherited WizRepartirIntereses_DevEx: TWizRepartirIntereses_DevEx
  Left = 2489
  Top = 234
  Caption = 'Repartici'#243'n de intereses'
  ClientHeight = 528
  ClientWidth = 845
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 845
    Height = 528
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Proceso que permite la repartici'#243'n de intereses.'
      Header.Title = 'Reparto de intereses.'
      object Label1: TLabel
        Left = 224
        Top = 170
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de ahorro:'
        Transparent = True
      end
      object Label2: TLabel
        Left = 200
        Top = 194
        Width = 96
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por inversi'#243'n:'
        Transparent = True
      end
      object Label13: TLabel
        Left = 193
        Top = 242
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto de comisiones:'
        Transparent = True
      end
      object Label16: TLabel
        Left = 194
        Top = 218
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por pr'#233'stamos:'
        Transparent = True
      end
      object AH_AHORRO: TZetaKeyLookup_DevEx
        Left = 298
        Top = 166
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object InteresesXinversion: TZetaNumero
        Left = 298
        Top = 190
        Width = 105
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
      end
      object gbNomina_DevEx: TcxGroupBox
        Left = 235
        Top = 86
        Caption = ' Per'#237'odo de n'#243'mina '
        TabOrder = 0
        Height = 75
        Width = 383
        object Label5: TLabel
          Left = 229
          Top = 20
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Transparent = True
          Visible = False
        end
        object Label6: TLabel
          Left = 20
          Top = 43
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
          Transparent = True
        end
        object Label7: TLabel
          Left = 38
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object PE_TIPO: TZetaKeyCombo
          Left = 256
          Top = 16
          Width = 105
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          Visible = False
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object PE_YEAR: TZetaNumero
          Left = 63
          Top = 16
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          OnExit = PE_YEARExit
        end
        object PE_NUMERO: TZetaKeyLookup_DevEx
          Left = 63
          Top = 39
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object MontoComisiones: TZetaNumero
        Left = 298
        Top = 238
        Width = 105
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
      end
      object MontoXPrestamos: TZetaNumero
        Left = 298
        Top = 214
        Width = 105
        Height = 21
        Mascara = mnPesos
        TabOrder = 3
        Text = '0.00'
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited cxGroupBox1: TcxGroupBox [0]
        Width = 823
        inherited Advertencia: TcxLabel
          Caption = ' Advertencia'
          Style.IsFontAssigned = True
          Width = 755
          AnchorY = 51
        end
      end
      object cxGroupBox2: TcxGroupBox [1]
        Left = 0
        Top = 95
        Align = alClient
        Caption = 'Par'#225'metros '
        TabOrder = 2
        Height = 293
        Width = 823
      end
      inherited GrupoParametros: TcxGroupBox [2]
        Left = 196
        Top = 111
        Align = alCustom
        Caption = ''
        Style.Edges = []
        Transparent = True
        Height = 270
        Width = 409
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.Description = 'Generar la lista de empleados que requiere aplicar el proceso.'
      Header.Title = 'Filtro de empleados.'
      inherited sCondicionLBl: TLabel
        Left = 209
      end
      inherited sFiltroLBL: TLabel
        Left = 234
      end
      inherited Seleccionar: TcxButton
        Left = 337
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 261
      end
      inherited sFiltro: TcxMemo
        Left = 261
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 261
      end
      inherited bAjusteISR: TcxButton
        Left = 575
      end
    end
    object EditarRegistros: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 'Proceso para el  reparto de intereses.'
      Header.Title = 'Reparto de intereses.'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 823
        Height = 81
        Align = alTop
        BevelOuter = bvNone
        Color = clBtnHighlight
        TabOrder = 0
        object Label3: TLabel
          Left = 26
          Top = 3
          Width = 120
          Height = 13
          Alignment = taRightJustify
          Caption = 'Intereses sobre inversi'#243'n:'
          Transparent = True
        end
        object InteresSobreInversion: TZetaTextBox
          Left = 149
          Top = 1
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label4: TLabel
          Left = 20
          Top = 22
          Width = 126
          Height = 13
          Alignment = taRightJustify
          Caption = 'Intereses sobre pr'#233'stamos:'
          Transparent = True
        end
        object InteresesSobrePrestamos: TZetaTextBox
          Left = 149
          Top = 20
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label8: TLabel
          Left = 70
          Top = 60
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total de capital:'
        end
        object CapitalTotal: TZetaTextBox
          Left = 149
          Top = 58
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label9: TLabel
          Left = 315
          Top = 60
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Empleados:'
          Transparent = True
        end
        object Empleados: TZetaTextBox
          Left = 397
          Top = 58
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label10: TLabel
          Left = 531
          Top = 3
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#243'mina:'
          Transparent = True
        end
        object Nomina: TZetaTextBox
          Left = 573
          Top = 1
          Width = 140
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label11: TLabel
          Left = 293
          Top = 3
          Width = 101
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa sobre inversi'#243'n:'
          Transparent = True
        end
        object TasaSobreInversion: TZetaTextBox
          Left = 397
          Top = 1
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object TasaSobrePrestamos: TZetaTextBox
          Left = 397
          Top = 20
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label12: TLabel
          Left = 287
          Top = 22
          Width = 107
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa sobre pr'#233'stamos:'
          Transparent = True
        end
        object Label14: TLabel
          Left = 40
          Top = 41
          Width = 106
          Height = 13
          Alignment = taRightJustify
          Caption = 'Monto por comisiones:'
          Transparent = True
        end
        object MontoXComision: TZetaTextBox
          Left = 149
          Top = 39
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label15: TLabel
          Left = 283
          Top = 41
          Width = 111
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa sobre comisiones:'
          Transparent = True
        end
        object TasaXComisiones: TZetaTextBox
          Left = 397
          Top = 39
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object ZetaDBGrid1: TZetaDBGrid
        Left = 0
        Top = 81
        Width = 817
        Height = 240
        Align = alCustom
        DataSource = dsGrid
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Empleado'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMBRE'
            Title.Caption = 'Nombre'
            Width = 172
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AHORRO_ACUMULADO'
            Title.Caption = 'Ahorro Acum.'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INTERES_ACUMULADO'
            Title.Caption = 'Intereses Acum.'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMISIONES_ACUMULADO'
            Title.Caption = 'Comisiones Acum.'
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAPITAL'
            Title.Caption = 'Capital'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INTERESES_INV'
            Title.Caption = 'Int. S/Inversi'#243'n'
            Width = 78
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INTERESES_PRESTAMO'
            Title.Caption = 'Int. S/Prestamos'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MONTO_COMISIONES'
            Title.Caption = 'Monto X Comisiones'
            Width = 103
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 358
        Width = 823
        Height = 30
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnEditarRegistros_DevEx: TcxButton
          Left = 345
          Top = 2
          Width = 112
          Height = 26
          Caption = 'Editar &registros'
          TabOrder = 0
          OnClick = btnEditarRegistros_DevExClick
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            200000000000000900000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
            FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
            FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
            EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
            E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
            F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
            FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
            F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
            EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
            F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
        end
      end
      object ZetaCXGrid1: TZetaCXGrid
        Left = 0
        Top = 81
        Width = 823
        Height = 277
        Align = alClient
        TabOrder = 3
        object ZetaCXGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsGrid
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.GroupBySorting = True
          OptionsCustomize.GroupRowSizing = True
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsSelection.CellSelect = False
          OptionsSelection.HideSelection = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object CB_CODIGO: TcxGridDBColumn
            Caption = 'Empleado'
            DataBinding.FieldName = 'CB_CODIGO'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.ReadOnly = False
            HeaderAlignmentHorz = taRightJustify
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 58
          end
          object NOMBRE: TcxGridDBColumn
            Caption = 'Nombre'
            DataBinding.FieldName = 'NOMBRE'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 49
          end
          object AHORRO_ACUMULADO: TcxGridDBColumn
            Caption = 'Ahorro Acum.'
            DataBinding.FieldName = 'AHORRO_ACUMULADO'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 69
          end
          object INTERES_ACUMULADO: TcxGridDBColumn
            Caption = 'Intereses Acum.'
            DataBinding.FieldName = 'INTERES_ACUMULADO'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 115
          end
          object COMISIONES_ACUMULADO: TcxGridDBColumn
            Caption = 'Comisiones Acum.'
            DataBinding.FieldName = 'COMISIONES_ACUMULADO'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 91
          end
          object CAPITAL: TcxGridDBColumn
            Caption = 'Capital'
            DataBinding.FieldName = 'CAPITAL'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 56
          end
          object INTERESES_INV: TcxGridDBColumn
            Caption = 'Int. S/Inversi'#243'n'
            DataBinding.FieldName = 'INTERESES_INV'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 83
          end
          object INTERESES_PRESTAMO: TcxGridDBColumn
            Caption = 'Int. S/Prestamos'
            DataBinding.FieldName = 'INTERESES_PRESTAMO'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 89
          end
          object MONTO_COMISIONES: TcxGridDBColumn
            Caption = 'Monto X Comisiones'
            DataBinding.FieldName = 'MONTO_COMISIONES'
            Options.Filtering = False
            Options.Grouping = False
            Options.Sorting = False
            Width = 103
          end
        end
        object ZetaCXGrid1Level1: TcxGridLevel
          GridView = ZetaCXGrid1DBTableView1
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 120
    Top = 372
  end
  object dsGrid: TDataSource
    Left = 356
    Top = 263
  end
end
