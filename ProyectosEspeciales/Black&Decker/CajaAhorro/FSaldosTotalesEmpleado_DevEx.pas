unit FSaldosTotalesEmpleado_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls, ZBaseEdicionRenglon,
  StdCtrls, Buttons, ZetaDBTextBox, ComCtrls, DBCtrls, ZetaSmartLists,
  Mask, ZetaNumero, ZetaKeyCombo, ZetaFecha, ZetaMessages, ZBaseDlgModal, ZBaseDlgModal_DevEx,
  ZBaseConsulta, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxButtons, cxContainer, cxGroupBox, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter;

   

type
  TSaldosTotalesEmpleado_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    dsRenglon: TDataSource;
    GroupBox1: TcxGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    AH_SALDO: TZetaDBTextBox;
    PR_SALDO: TZetaDBTextBox;
    Label4: TLabel;
    SubTotalFondoAhorro: TZetaDBTextBox;
    Label6: TLabel;
    AH_STATUS_FONDO: TZetaDBTextBox;
    Label7: TLabel;
    SaldoPrestamosFondoInteres: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label1: TLabel;
    SALDO: TZetaDBTextBox;
    Label3: TLabel;
    SaldoPrestamosCajaCapital: TZetaDBTextBox;
    Label5: TLabel;
    SubTotalCajaAhorro: TZetaDBTextBox;
    Label2: TLabel;
    AH_STATUS: TZetaDBTextBox;
    Label8: TLabel;
    SaldoPrestamosCajaInteres: TZetaDBTextBox;
    GroupBox3: TcxGroupBox;
    Label25: TLabel;
    AH_NETO: TZetaDBTextBox;
    Label9: TLabel;
    TotalFondoAhorro: TZetaDBTextBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    InteresesCA: TZetaDBTextBox;
    Label20: TLabel;
    InteresesFA: TZetaDBTextBox;
    Label21: TLabel;
    ComisionesAcumCA: TZetaDBTextBox;
    Label22: TLabel;
    ComisionesAcumFA: TZetaDBTextBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    AH_FECHA_INI_CAJA: TZetaDBTextBox;
    Label29: TLabel;
    AH_FECHA_INI_FONDO: TZetaDBTextBox;
    gbAval: TcxGroupBox;
    Label30: TLabel;
    MontoAval: TZetaDBTextBox;
    Label31: TLabel;
    EmpleadoAval: TZetaDBTextBox;
    Label32: TLabel;
    FechaAval: TZetaDBTextBox;
    PR_TIPO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    PR_REFEREN: TcxGridDBColumn;
    PR_FECHA: TcxGridDBColumn;
    PR_MONTO: TcxGridDBColumn;
    PR_PAGOS: TcxGridDBColumn;
    ZetaDBGridDBTableViewPR_SALDO: TcxGridDBColumn;
    PR_STATUS: TcxGridDBColumn;
    Panel2: TPanel;
    BBBorrar: TcxButton;
    BBAgregar_DevEx: TcxButton;
    BBModificar_DevEx: TcxButton;
    procedure BBAgregar_DevExClick(Sender: TObject);
    procedure BBModificar_DevExClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function PuedeModificarPresta: Boolean;
    function PuedeBorrarPresta: Boolean;
    function PuedeAgregarPresta: Boolean;
    procedure ModificaPrestamo;
  protected
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  SaldosTotalesEmpleado_DevEx: TSaldosTotalesEmpleado_DevEx;

implementation

uses
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaDialogo,
    DCajaAhorro,
    ZAccesosTress,
    FTipoPrestamoAhorro,
    FTipoPrestamoAhorro_DevEx,
    ZAccesosMgr, DCliente;

{$R *.dfm}

{ TSaldosTotalesEmpleado }

procedure TSaldosTotalesEmpleado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
   //  ZetaDBGrid.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= 0;
     IndexDerechos := D_AHORRO_SALDOS_X_EMP;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stNinguno;
  
end;

procedure TSaldosTotalesEmpleado_DevEx.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsHisAhorros.Refrescar;
          cdsTotalHisAhorros.Refrescar;
          Datasource.Dataset := cdsTotalHisAhorros;
          dsRenglon.DataSet := cdsTotalHisPrestamos;
          DoBestFit;
     end;
end;

procedure TSaldosTotalesEmpleado_DevEx.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Agregar;
     DoBestFit;
end;

procedure TSaldosTotalesEmpleado_DevEx.Borrar;
begin
     dmCajaAhorro.BorrarIncripcion;
     DoBestFit;
end;

procedure TSaldosTotalesEmpleado_DevEx.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Modificar;
     DoBestFit;
end;

procedure TSaldosTotalesEmpleado_DevEx.Refresh;
begin
     dmCajaAhorro.cdsTotalHisAhorros.Refrescar;
     DoBestFit;
end;

procedure TSaldosTotalesEmpleado_DevEx.BBAgregar_DevExClick(Sender: TObject);
begin
     inherited;
     if dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat > 0 then
     begin
          if PuedeAgregarPresta then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( TipoPrestamoAhorro_DevEx, TTipoPrestamoAhorro_DevEx);
               //TipoPrestamoAhorro_DevEx.Show;
               //dmCajaAhorro.cdsHisPrestamos.Agregar;
          end;
     end
    else
         ZetaDialogo.ZError( 'Prestamos', 'El Saldo disponible debe ser Mayor a Cero', 0 );
end;

procedure TSaldosTotalesEmpleado_DevEx.BBModificar_DevExClick(Sender: TObject);
begin
     inherited;
     ModificaPrestamo;
end;

procedure TSaldosTotalesEmpleado_DevEx.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if ( PuedeBorrarPresta ) then
     begin
          with dmCajaAhorro.cdsTotalHisPrestamos do
          begin
               if RecordCount = 0 then
                  ZInformation(Self.Caption,' No existen préstamos para borrar ',0)
               else
                   Borrar;
          end;
     end;
end;

procedure TSaldosTotalesEmpleado_DevEx.FormDblClick(Sender: TObject);
begin
     if ( ActiveControl = ZetaDBGrid)  then
         ModificaPrestamo
     else
         inherited;
end;

procedure TSaldosTotalesEmpleado_DevEx.WMExaminar(var Message: TMessage);
begin
     ModificaPrestamo;
end;

procedure TSaldosTotalesEmpleado_DevEx.ModificaPrestamo;
begin
     if PuedeModificarPresta then
        dmCajaAhorro.cdsTotalHisPrestamos.Modificar;
end;

function TSaldosTotalesEmpleado_DevEx.PuedeAgregarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS, K_DERECHO_ALTA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para registrar préstamos',0);
end;

function TSaldosTotalesEmpleado_DevEx.PuedeBorrarPresta: Boolean;
begin
     Result:= CheckDerecho( D_EMP_NOM_PRESTAMOS, K_DERECHO_BAJA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para borrar préstamos',0);
end;

function TSaldosTotalesEmpleado_DevEx.PuedeModificarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_CAMBIO);
     if not Result then
        ZInformation('Préstamos','No tiene permiso para modificar préstamos',0);
end;

function TSaldosTotalesEmpleado_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= False; //Revisa(D_AHORRO_REG_INSCRIP_EMP);
     if not Result then
        sMensaje:= 'No se pueden agregar datos';
end;

function TSaldosTotalesEmpleado_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= False; //Revisa(D_AHORRO_REG_LIQ_RET);
     if not Result then
        sMensaje:= 'No se pueden borrar datos';
end;

function TSaldosTotalesEmpleado_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= False; //Revisa(D_AHORRO_REG_LIQ_RET);
     if not Result then
        sMensaje:= 'No se pueden modificar datos';
end;

procedure TSaldosTotalesEmpleado_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;

end;

end.
