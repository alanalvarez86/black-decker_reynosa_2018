inherited WizCancelarIntereses: TWizCancelarIntereses
  Left = 371
  Top = 306
  Caption = 'Cancelaci'#243'n de Intereses Repartidos'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
  end
  inherited PageControl: TPageControl
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object Label1: TLabel
        Left = 11
        Top = 149
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de ahorro:'
      end
      object gbNomina: TGroupBox
        Left = 22
        Top = 73
        Width = 371
        Height = 67
        Caption = ' Per'#237'odo de n'#243'mina '
        TabOrder = 0
        object Label5: TLabel
          Left = 140
          Top = 20
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Visible = False
        end
        object Label6: TLabel
          Left = 20
          Top = 44
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label7: TLabel
          Left = 38
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object PE_TIPO: TZetaKeyCombo
          Left = 167
          Top = 16
          Width = 105
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 2
          Visible = False
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object PE_YEAR: TZetaNumero
          Left = 63
          Top = 16
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          OnExit = PE_YEARExit
        end
        object PE_NUMERO: TZetaKeyLookup
          Left = 63
          Top = 40
          Width = 300
          Height = 21
          Opcional = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object AH_AHORRO: TZetaKeyLookup
        Left = 86
        Top = 145
        Width = 300
        Height = 21
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        inherited Seleccionar: TBitBtn
          Visible = True
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Lines.Strings = (
          'Advertencia'
          ''
          'El siguiente proceso borrar'#225' los registros de Abono que se '
          'calcularon para los empleados en el proceso de '
          '"Repartir Intereses" '
          ''
          'Si ya se verifico la lista de los empleados'
          'Dar clic en el bot'#243'n de Ejecutar para iniciar el proceso.')
      end
    end
  end
end
