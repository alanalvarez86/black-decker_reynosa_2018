inherited TipoPrestamoAhorro: TTipoPrestamoAhorro
  Left = 315
  Top = 369
  Caption = 'Registro de Pr'#233'stamo de Ahorro'
  ClientHeight = 80
  ClientWidth = 417
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 16
    Width = 73
    Height = 13
    Caption = 'Tipo de Ahorro:'
  end
  inherited PanelBotones: TPanel
    Top = 44
    Width = 417
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 249
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 334
    end
  end
  object lkTipoPrestamo: TZetaKeyLookup
    Left = 84
    Top = 12
    Width = 317
    Height = 21
    Filtro = '( TB_NUMERO = 1 or TB_NUMERO = 2 )'
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
  end
end
