inherited TipoPrestamoAhorro_DevEx: TTipoPrestamoAhorro_DevEx
  Left = 2076
  Top = 460
  Caption = 'Registro de Pr'#233'stamo de Ahorro'
  ClientHeight = 80
  ClientWidth = 407
  Color = clBtnHighlight
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 8
    Top = 16
    Width = 73
    Height = 13
    Caption = 'Tipo de Ahorro:'
    Transparent = True
  end
  inherited PanelBotones: TPanel
    Top = 44
    Width = 407
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 238
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 318
      Cancel = True
      OnClick = Cancelar_DevExClick
    end
  end
  object lkTipoPrestamo: TZetaKeyLookup_DevEx [2]
    Left = 84
    Top = 12
    Width = 317
    Height = 21
    Filtro = '( TB_NUMERO = 1 or TB_NUMERO = 2 )'
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
