unit FWizCancelarIntereses_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaNumero,
  ZetaCommonClasses,ZetaCommonLists, ZetaKeyCombo,ZCXBaseWizardFiltro,ZetaCxWizard,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, Menus, cxRadioGroup,
  cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses,
  cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl,ZBasicoSelectGrid_DevEx,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, DB,
  cxDBData, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid;

type
  TWizCancelarIntereses_DevEx = class( TBaseCXWizardFiltro)
    gbNomina: TcxGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    PE_TIPO: TZetaKeyCombo;
    PE_YEAR: TZetaNumero;
    PE_NUMERO: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    AH_AHORRO: TZetaKeyLookup_DevEx;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);


  private
    { Private declarations }
    FParams : TZetaParams;
    procedure SetFiltroPeriodo(const iYear: Integer;const TipoPeriodo: eTipoPeriodo);
  public
    { Public declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  end;

var
  WizCancelarIntereses_DevEx: TWizCancelarIntereses_DevEx;

implementation

{$R *.dfm}

uses
    DCliente,
    DCajaAhorro,
    ZBaseSelectGrid,
    ZetaDialogo,
    ZBasicoSelectGrid,
    FCancelaGridSelect,
    ZetaCommonTools;

procedure TWizCancelarIntereses_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;

     if Wizard.Adelante then
     begin
          if WizardControl.ActivePageIndex= 0  then
          begin
               if PE_YEAR.ValorEntero = 0 then
                  CanMove :=  Error( 'A�o no puede ser cero', PE_YEAR )
               else
               begin
                   FParams.AddInteger( 'Year', PE_YEAR.ValorEntero );
                   if ( PE_NUMERO.Valor = 0 ) or
                      ( strVacio( PE_NUMERO.Llave ) ) then
                      CanMove :=  Error( 'N�mero de n�mina no puede quedar vac�o', PE_NUMERO )
                   else
                   begin
                        FParams.AddInteger( 'Numero', PE_NUMERO.Valor );
                        FParams.AddInteger( 'Tipo', PE_TIPO.Valor );
                        if strVacio( AH_AHORRO.Llave ) then
                           CanMove := Error( 'Tipo de ahorro no puede quedar vac�o', AH_AHORRO )
                        else
                            FParams.AddString( 'TipoAhorro', AH_AHORRO.Llave );
                   end;
               end;
          end
     end;
end;

procedure TWizCancelarIntereses_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FParams := TZetaParams.Create;
     PE_NUMERO.LookupDataSet := dmCajaAhorro.cdsPeriodoProcesos;
     AH_AHORRO.LookUpDataset := dmCajaAhorro.cdsTAhorro;
     dmCajaAhorro.cdsTAhorro.Conectar;
     dmCajaAhorro.cdsPeriodoProcesos.Conectar;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          PE_YEAR.Valor := Year;
          PE_TIPO.Valor := Ord( Tipo );
          PE_NUMERO.Valor := Numero;
          SetFiltroPeriodo( Year, Tipo );
     end;
end;

procedure TWizCancelarIntereses_DevEx.SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodoProcesos.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodoProcesos.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodoProcesos.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodoProcesos( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodoProcesos( iYear, TipoPeriodo );

          with PE_NUMERO do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
                  Valor := dmCliente.GetDatosPeriodoActivo.Numero
               else
                   Valor := 1;
          end;
     end;
end;

procedure TWizCancelarIntereses_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FParams );
end;



procedure TWizCancelarIntereses_DevEx.CargaListaVerificacion;
begin
     dmCajaAhorro.CancelaGetLista( FParams );
end;

procedure TWizCancelarIntereses_DevEx.CargaParametros;
begin

     inherited;

     with ParameterList do
     begin
          AddInteger( 'Year',PE_YEAR.ValorEntero );
          AddInteger( 'Tipo',  PE_TIPO.Valor );
          AddInteger('Numero', PE_NUMERO.Valor);
          AddString ( 'TipoAhorro', AH_AHORRO.Llave );

     end;

      with Descripciones do
     begin
          AddInteger( 'A�o',PE_YEAR.ValorEntero );
          AddString( 'Tipo',  PE_TIPO.Text);
          AddInteger('N�mero', PE_NUMERO.Valor);
          AddString ( 'Tipo de Ahorro', AH_AHORRO.Descripcion );
     end;

end;


function TWizCancelarIntereses_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmCajaAhorro.CancelaTodos( FParams, Verificacion );
     if Result then
        ZetaDialogo.ZInformation( '�Proceso Terminado!', 'Revisar Montos en el Ahorro de los Empleados', 0  )
     else
         ZetaDialogo.ZInformation( '�Proceso Terminado!', 'No hay empleados a procesar', 0  );
end;

function TWizCancelarIntereses_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmCajaAhorro.cdsDataset, TCancelaGridSelect );
end;

procedure TWizCancelarIntereses_DevEx.FormShow(Sender: TObject);
begin
      //ApplyMinWidth;
      inherited;
      Parametros.PageIndex := 0;
      FiltrosCondiciones.PageIndex := 1;
      Ejecucion.PageIndex := 2;
      Advertencia.Caption:='Advertencia, el siguiente proceso borrar� los registros de abono que se calcularon para los empleados en el proceso de repartir intereses.';



end;





end.
