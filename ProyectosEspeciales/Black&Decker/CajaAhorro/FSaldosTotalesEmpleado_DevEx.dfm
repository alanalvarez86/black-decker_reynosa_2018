inherited SaldosTotalesEmpleado_DevEx: TSaldosTotalesEmpleado_DevEx
  Left = 2459
  Top = 97
  Caption = 'Saldos Totales de Empleado'
  ClientHeight = 811
  ClientWidth = 1092
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1092
    inherited ValorActivo2: TPanel
      Width = 833
      inherited textoValorActivo2: TLabel
        Width = 827
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 1092
    Height = 326
    Align = alTop
    Color = clBtnHighlight
    ParentBackground = False
    TabOrder = 2
    object GroupBox1: TcxGroupBox
      Left = 304
      Top = 2
      Caption = ' Fondo de ahorro '
      ParentColor = False
      Style.Color = clBtnFace
      TabOrder = 0
      Height = 220
      Width = 292
      object Label18: TLabel
        Left = 104
        Top = 62
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ahorrado:'
        Transparent = True
      end
      object Label19: TLabel
        Left = 20
        Top = 150
        Width = 130
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de pr'#233'stamos capital:'
        Transparent = True
      end
      object AH_SALDO: TZetaDBTextBox
        Left = 154
        Top = 58
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_SALDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalFondoAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object PR_SALDO: TZetaDBTextBox
        Left = 154
        Top = 146
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'PR_SALDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosFondoCapital'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 30
        Top = 194
        Width = 120
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subtotal fondo de ahorro:'
        Transparent = True
      end
      object SubTotalFondoAhorro: TZetaDBTextBox
        Left = 154
        Top = 190
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SubTotalFondoAhorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SubTotalFondoAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 117
        Top = 18
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
        Transparent = True
      end
      object AH_STATUS_FONDO: TZetaDBTextBox
        Left = 154
        Top = 14
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_STATUS_FONDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_STATUS_FONDO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 20
        Top = 172
        Width = 130
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de pr'#233'stamos inter'#233's:'
        Transparent = True
      end
      object SaldoPrestamosFondoInteres: TZetaDBTextBox
        Left = 154
        Top = 168
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoPrestamosFondoInteres'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosFondoInteres'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 53
        Top = 128
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aportaci'#243'n empresa:'
        Transparent = True
      end
      object TotalFondoAhorro: TZetaDBTextBox
        Left = 154
        Top = 124
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'TotalFondoAhorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalFondoAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 270
        Top = 128
        Width = 16
        Height = 13
        Caption = '(+)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label13: TLabel
        Left = 270
        Top = 149
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label14: TLabel
        Left = 270
        Top = 171
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label16: TLabel
        Left = 270
        Top = 192
        Width = 16
        Height = 13
        Caption = '(=)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label20: TLabel
        Left = 60
        Top = 84
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Intereses ganados:'
        Transparent = True
      end
      object InteresesFA: TZetaDBTextBox
        Left = 154
        Top = 80
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'InteresesFA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalInteresesFA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 44
        Top = 106
        Width = 106
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por comisiones:'
        Transparent = True
      end
      object ComisionesAcumFA: TZetaDBTextBox
        Left = 154
        Top = 102
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ComisionesAcumFA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'ComisionesAcumFA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 270
        Top = 106
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label26: TLabel
        Left = 270
        Top = 84
        Width = 16
        Height = 13
        Caption = '(+)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label29: TLabel
        Left = 75
        Top = 40
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de inicio:'
        Transparent = True
      end
      object AH_FECHA_INI_FONDO: TZetaDBTextBox
        Left = 154
        Top = 36
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_FECHA_INI_FONDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_FECHA_INI_FONDO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object GroupBox2: TcxGroupBox
      Left = 5
      Top = 2
      Caption = ' Caja de ahorro '
      ParentColor = False
      Style.Color = clBtnFace
      TabOrder = 1
      Height = 220
      Width = 293
      object Label1: TLabel
        Left = 104
        Top = 62
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ahorrado:'
        Transparent = True
      end
      object SALDO: TZetaDBTextBox
        Left = 154
        Top = 58
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SALDO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalCajaAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 20
        Top = 128
        Width = 130
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de pr'#233'stamos capital:'
        Transparent = True
      end
      object SaldoPrestamosCajaCapital: TZetaDBTextBox
        Left = 154
        Top = 124
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoPrestamosCajaCapital'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosCajaCapital'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 37
        Top = 172
        Width = 113
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subtotal caja de ahorro:'
        Transparent = True
      end
      object SubTotalCajaAhorro: TZetaDBTextBox
        Left = 154
        Top = 168
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SubTotalCajaAhorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SubTotalCajaAhorro'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 117
        Top = 18
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
        Transparent = True
      end
      object AH_STATUS: TZetaDBTextBox
        Left = 154
        Top = 14
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_STATUS'
        ShowAccelChar = False
        Transparent = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_STATUS_CAJA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label8: TLabel
        Left = 20
        Top = 150
        Width = 130
        Height = 13
        Alignment = taRightJustify
        Caption = 'Saldo de pr'#233'stamos inter'#233's:'
        Transparent = True
      end
      object SaldoPrestamosCajaInteres: TZetaDBTextBox
        Left = 154
        Top = 146
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'SaldoPrestamosCajaInteres'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'SaldoPrestamosCajaInteres'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 270
        Top = 127
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label11: TLabel
        Left = 270
        Top = 149
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label15: TLabel
        Left = 270
        Top = 170
        Width = 16
        Height = 13
        Caption = '(=)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label17: TLabel
        Left = 60
        Top = 84
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Intereses ganados:'
        Transparent = True
      end
      object InteresesCA: TZetaDBTextBox
        Left = 154
        Top = 80
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'InteresesCA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TotalInteresesCA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 44
        Top = 106
        Width = 106
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por comisiones:'
        Transparent = True
      end
      object ComisionesAcumCA: TZetaDBTextBox
        Left = 154
        Top = 102
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'ComisionesAcumCA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'ComisionesAcumCA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 270
        Top = 106
        Width = 17
        Height = 13
        Caption = '(- )'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label27: TLabel
        Left = 270
        Top = 84
        Width = 16
        Height = 13
        Caption = '(+)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = True
      end
      object Label28: TLabel
        Left = 74
        Top = 40
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de Inicio:'
        Transparent = True
      end
      object AH_FECHA_INI_CAJA: TZetaDBTextBox
        Left = 154
        Top = 36
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_FECHA_INI_CAJA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'AH_FECHA_INI_CAJA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object GroupBox3: TcxGroupBox
      Left = 222
      Top = 266
      Caption = ' Saldo Disponible '
      TabOrder = 2
      Height = 56
      Width = 172
      object Label25: TLabel
        Left = 13
        Top = 24
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Neto:'
      end
      object AH_NETO: TZetaDBTextBox
        Left = 43
        Top = 20
        Width = 115
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'AH_NETO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Neto'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object gbAval: TcxGroupBox
      Left = 6
      Top = 225
      Caption = ' Aval '
      TabOrder = 3
      Height = 45
      Width = 590
      object Label30: TLabel
        Left = 330
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Monto:'
      end
      object MontoAval: TZetaDBTextBox
        Left = 366
        Top = 12
        Width = 85
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'MontoAval'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'MontoAval'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 11
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Empleado:'
      end
      object EmpleadoAval: TZetaDBTextBox
        Left = 64
        Top = 12
        Width = 262
        Height = 21
        AutoSize = False
        Caption = 'EmpleadoAval'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'EmpleadoAval'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 459
        Top = 16
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
      end
      object FechaAval: TZetaDBTextBox
        Left = 496
        Top = 12
        Width = 85
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'FechaAval'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'FechaAval'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 377
    Width = 1092
    Height = 434
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsRenglon
      OptionsBehavior.FocusCellOnTab = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object PR_TIPO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'PR_TIPO'
        Options.Filtering = False
        Options.Grouping = False
        Width = 69
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        Options.Filtering = False
        Options.Grouping = False
        Width = 196
      end
      object PR_REFEREN: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PR_REFEREN'
        Options.Filtering = False
        Options.Grouping = False
      end
      object PR_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'PR_MONTO'
        Options.Filtering = False
        Width = 72
      end
      object PR_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'PR_FECHA'
        Width = 77
      end
      object PR_PAGOS: TcxGridDBColumn
        Caption = 'Pagos'
        DataBinding.FieldName = 'PR_PAGOS'
        Options.Filtering = False
        Options.Grouping = False
        Width = 74
      end
      object ZetaDBGridDBTableViewPR_SALDO: TcxGridDBColumn
        Caption = 'Saldo'
        DataBinding.FieldName = 'PR_SALDO'
        Options.Filtering = False
        Options.Grouping = False
        Width = 76
      end
      object PR_STATUS: TcxGridDBColumn
        Caption = 'Estatus'
        DataBinding.FieldName = 'PR_STATUS'
        Width = 86
      end
    end
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 345
    Width = 1092
    Height = 32
    Align = alTop
    Color = clBtnHighlight
    TabOrder = 3
    object BBBorrar: TcxButton
      Left = 256
      Top = 3
      Width = 112
      Height = 26
      Hint = 'Borrar el pr'#233'stamo seleccionado'
      Caption = 'Borrar pr'#233'stamo'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BBBorrarClick
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000004858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFE8EAF9FFA4AC
        E6FFC6CBEFFFFFFFFFFFFFFFFFFFD1D5F2FFA4ACE6FFDDE0F5FFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFD1D5F2FF4858
        CCFF8D97DFFFFFFFFFFFFFFFFFFFA4ACE6FF4858CCFFBAC0ECFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF8D97DFFF8D97DFFF8D97DFFF8D97
        DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97DFFF8D97
        DFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
        CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
      OptionsImage.Margin = 1
    end
    object BBAgregar_DevEx: TcxButton
      Left = 9
      Top = 3
      Width = 112
      Height = 26
      Hint = 'Registrar un pr'#233'stamo'
      Caption = 'Prestar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BBAgregar_DevExClick
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FFC5F0DBFFDEF7EBFFCEF2E1FFBDEE
        D6FFADEACCFFA8E9C9FFA8E9C9FF8CE2B8FF5ED69BFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF87E0B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFDFFBDEED6FF69D8A2FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF58D498FFE9F9F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFEFDFFB2EB
        D0FF58D498FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFAFEBCEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFE
        FCFFC0EFD8FF97E4BFFF92E3BCFF92E3BCFFA5E8C8FFD6F4E6FFFCFEFDFFFFFF
        FFFFE9F9F1FF84DFB3FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF7FDEB0FFE6F9F0FFFFFFFFFFFFFFFFFFFFFFFFFFA2E7
        C6FF63D79FFF50D293FF50D293FF53D395FF8CE2B8FFB5ECD1FF8AE1B7FF9DE6
        C2FFD6F4E6FFCBF2DFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF74DBA9FFEFFBF5FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFAFEFCFFE1F7ECFFA5E8C8FF50D293FF66D8A1FFB8EDD3FF69D8
        A2FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF55D396FF8FE2BAFFAAE9CAFFBDEE
        D6FFBDEED6FFBAEDD5FFA8E9C9FFA5E8C8FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FF7CDDAEFF7CDD
        AEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDDAEFF7CDD
        AEFF7CDDAEFF7CDDAEFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFE4F8EEFFA5E8C8FFD0F3E2FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FFA8E9C9FFFFFFFFFFFFFF
        FFFFFFFFFFFFCBF2DFFF55D396FF97E4BFFF58D498FFA0E6C4FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FFD6F4E6FF63D79FFFA8E9C9FFFFFFFFFFFFFF
        FFFFFFFFFFFF8FE2BAFF50D293FF8AE1B7FF60D69DFF69D8A2FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FFE9F9F1FF66D8A1FFA8E9C9FFFFFFFFFFFFFF
        FFFFFFFFFFFFADEACCFF50D293FF9AE5C1FF5BD59AFF81DFB1FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FFE9F9F1FF66D8A1FFA8E9C9FFFFFFFFFFFFFF
        FFFFFFFFFFFFFCFEFDFFAFEBCEFF81DFB1FF9DE6C2FFEFFBF5FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FFE9F9F1FF66D8A1FF7CDDAEFFA8E9C9FFA8E9
        C9FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9C9FFA8E9
        C9FFA8E9C9FFA8E9C9FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FFE9F9F1FFA0E6C4FF92E3BCFF92E3BCFF92E3
        BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3BCFF92E3
        BCFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FFD6F4E6FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9
        F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9F1FFE9F9
        F1FFD6F4E6FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      OptionsImage.Spacing = 25
    end
    object BBModificar_DevEx: TcxButton
      Left = 126
      Top = 3
      Width = 125
      Height = 26
      Hint = 'Modificar el pr'#233'stamo seleccionado'
      Caption = 'Modificar pr'#233'stamo'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BBModificar_DevExClick
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000000B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
        FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
        FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
        EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
        E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
        F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
        FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
        F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
        EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
        F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
        F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
        E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
      OptionsImage.Margin = 1
    end
  end
  inherited DataSource: TDataSource
    Left = 40
    Top = 48
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsRenglon: TDataSource
    Left = 160
    Top = 299
  end
end
