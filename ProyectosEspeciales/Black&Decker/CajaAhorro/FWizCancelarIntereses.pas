unit FWizCancelarIntereses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, Mask, ZetaNumero,
  ZetaCommonClasses,ZetaCommonLists, ZetaKeyCombo;

type
  TWizCancelarIntereses = class(TBaseWizardFiltro)
    gbNomina: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    PE_TIPO: TZetaKeyCombo;
    PE_YEAR: TZetaNumero;
    PE_NUMERO: TZetaKeyLookup;
    Label1: TLabel;
    AH_AHORRO: TZetaKeyLookup;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PE_YEARExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParams : TZetaParams;
    procedure SetFiltroPeriodo(const iYear: Integer;const TipoPeriodo: eTipoPeriodo);
  public
    { Public declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
  end;

var
  WizCancelarIntereses: TWizCancelarIntereses;

implementation

{$R *.dfm}

uses
    DCliente,
    DCajaAhorro,
    ZBaseSelectGrid,
    ZetaDialogo,
    ZBasicoSelectGrid,
    FCancelaGridSelect,
    ZetaCommonTools;

procedure TWizCancelarIntereses.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros ) then
          begin
               if PE_YEAR.ValorEntero = 0 then
                  CanMove :=  Error( 'A�o no puede ser cero', PE_YEAR )
               else
               begin
                   FParams.AddInteger( 'Year', PE_YEAR.ValorEntero );
                   if ( PE_NUMERO.Valor = 0 ) or
                      ( strVacio( PE_NUMERO.Llave ) ) then
                      CanMove :=  Error( 'N�mero de n�mina no puede quedar vac�o', PE_NUMERO )
                   else
                   begin
                        FParams.AddInteger( 'Numero', PE_NUMERO.Valor );
                        FParams.AddInteger( 'Tipo', PE_TIPO.Valor );
                        if strVacio( AH_AHORRO.Llave ) then
                           CanMove := Error( 'Tipo de ahorro no puede quedar vac�o', AH_AHORRO )
                        else
                            FParams.AddString( 'TipoAhorro', AH_AHORRO.Llave );
                   end;
               end;
          end
     end;
end;

procedure TWizCancelarIntereses.FormCreate(Sender: TObject);
begin
     inherited;
     FParams := TZetaParams.Create;
     PE_NUMERO.LookupDataSet := dmCajaAhorro.cdsPeriodoProcesos;
     AH_AHORRO.LookUpDataset := dmCajaAhorro.cdsTAhorro;
     dmCajaAhorro.cdsTAhorro.Conectar;
     dmCajaAhorro.cdsPeriodoProcesos.Conectar;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          PE_YEAR.Valor := Year;
          PE_TIPO.Valor := Ord( Tipo );
          PE_NUMERO.Valor := Numero;
          SetFiltroPeriodo( Year, Tipo );
     end;
end;

procedure TWizCancelarIntereses.SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodoProcesos.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodoProcesos.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodoProcesos.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodoProcesos( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodoProcesos( iYear, TipoPeriodo );

          with PE_NUMERO do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
                  Valor := dmCliente.GetDatosPeriodoActivo.Numero
               else
                   Valor := 1;
          end;
     end;
end;

procedure TWizCancelarIntereses.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FParams );
end;

procedure TWizCancelarIntereses.PE_YEARExit(Sender: TObject);
begin
     inherited;
     SetFiltroPeriodo( PE_YEAR.ValorEntero, eTipoPeriodo( PE_TIPO.Valor ) );
end;

procedure TWizCancelarIntereses.CargaListaVerificacion;
begin
     dmCajaAhorro.CancelaGetLista( FParams );
end;

function TWizCancelarIntereses.EjecutarWizard: Boolean;
begin
     Result := dmCajaAhorro.CancelaTodos( FParams, Verificacion );
     if Result then
        ZetaDialogo.ZInformation( '�Proceso Terminado!', 'Revisar Montos en el Ahorro de los Empleados', 0  )
     else
         ZetaDialogo.ZInformation( '�Proceso Terminado!', 'No hay empleados a procesar', 0  );
end;

function TWizCancelarIntereses.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmCajaAhorro.cdsDataset, TCancelaGridSelect );
end;

procedure TWizCancelarIntereses.FormShow(Sender: TObject);
begin
     inherited;
     AH_AHORRO.SetFocus;
end;

end.
