unit FSaldosTotalesEmpleado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls, ZBaseEdicionRenglon,
  StdCtrls, Buttons, ZetaDBTextBox, ComCtrls, DBCtrls, ZetaSmartLists,
  Mask, ZetaNumero, ZetaKeyCombo, ZetaFecha, ZetaMessages, ZBaseDlgModal;

type
  TSaldosTotalesEmpleado = class(TBaseConsulta)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GridRenglones: TZetaDBGrid;
    dsRenglon: TDataSource;
    BBAgregar: TBitBtn;
    BBModificar: TBitBtn;
    BBBorrar: TBitBtn;
    GroupBox1: TGroupBox;
    Label18: TLabel;
    Label19: TLabel;
    AH_SALDO: TZetaDBTextBox;
    PR_SALDO: TZetaDBTextBox;
    Label4: TLabel;
    SubTotalFondoAhorro: TZetaDBTextBox;
    Label6: TLabel;
    AH_STATUS_FONDO: TZetaDBTextBox;
    Label7: TLabel;
    SaldoPrestamosFondoInteres: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    SALDO: TZetaDBTextBox;
    Label3: TLabel;
    SaldoPrestamosCajaCapital: TZetaDBTextBox;
    Label5: TLabel;
    SubTotalCajaAhorro: TZetaDBTextBox;
    Label2: TLabel;
    AH_STATUS: TZetaDBTextBox;
    Label8: TLabel;
    SaldoPrestamosCajaInteres: TZetaDBTextBox;
    GroupBox3: TGroupBox;
    Label25: TLabel;
    AH_NETO: TZetaDBTextBox;
    Label9: TLabel;
    TotalFondoAhorro: TZetaDBTextBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    InteresesCA: TZetaDBTextBox;
    Label20: TLabel;
    InteresesFA: TZetaDBTextBox;
    Label21: TLabel;
    ComisionesAcumCA: TZetaDBTextBox;
    Label22: TLabel;
    ComisionesAcumFA: TZetaDBTextBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    AH_FECHA_INI_CAJA: TZetaDBTextBox;
    Label29: TLabel;
    AH_FECHA_INI_FONDO: TZetaDBTextBox;
    gbAval: TGroupBox;
    Label30: TLabel;
    MontoAval: TZetaDBTextBox;
    Label31: TLabel;
    EmpleadoAval: TZetaDBTextBox;
    Label32: TLabel;
    FechaAval: TZetaDBTextBox;
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function PuedeModificarPresta: Boolean;
    function PuedeBorrarPresta: Boolean;
    function PuedeAgregarPresta: Boolean;
    procedure ModificaPrestamo;
  protected
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  end;

var
  SaldosTotalesEmpleado: TSaldosTotalesEmpleado;

implementation

uses
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaDialogo,
    DCajaAhorro,
    ZAccesosTress,
    FTipoPrestamoAhorro,
    ZAccesosMgr;

{$R *.dfm}

{ TSaldosTotalesEmpleado }

procedure TSaldosTotalesEmpleado.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= 0;
     IndexDerechos := D_AHORRO_SALDOS_X_EMP;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stNinguno;
end;

procedure TSaldosTotalesEmpleado.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsTAhorro.Conectar;
          cdsHisAhorros.Refrescar;
          cdsTotalHisAhorros.Refrescar;
          Datasource.Dataset := cdsTotalHisAhorros;
          dsRenglon.DataSet := cdsTotalHisPrestamos;
     end;
end;

procedure TSaldosTotalesEmpleado.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Agregar;
end;

procedure TSaldosTotalesEmpleado.Borrar;
begin
     dmCajaAhorro.BorrarIncripcion;
end;

procedure TSaldosTotalesEmpleado.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsHisAhorros.Modificar;
end;

procedure TSaldosTotalesEmpleado.Refresh;
begin
     dmCajaAhorro.cdsTotalHisAhorros.Refrescar;
end;

procedure TSaldosTotalesEmpleado.BBAgregarClick(Sender: TObject);
begin
     inherited;
     if dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat > 0 then
     begin
          if PuedeAgregarPresta then
          begin
               ShowDlgModal( TipoPrestamoAhorro, TTipoPrestamoAhorro );
               //TipoPrestamoAhorro.Show;
               //dmCajaAhorro.cdsHisPrestamos.Agregar;
          end;
     end
     else
         ZetaDialogo.ZError( 'Prestamos', 'El Saldo disponible debe ser Mayor a Cero', 0 );
end;

procedure TSaldosTotalesEmpleado.BBModificarClick(Sender: TObject);
begin
     inherited;
     ModificaPrestamo;
end;

procedure TSaldosTotalesEmpleado.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if ( PuedeBorrarPresta ) then
     begin
          with dmCajaAhorro.cdsTotalHisPrestamos do
          begin
               if RecordCount = 0 then
                  ZInformation(Self.Caption,' No existen préstamos para borrar ',0)
               else
                   Borrar;
          end;
     end;
end;

procedure TSaldosTotalesEmpleado.FormDblClick(Sender: TObject);
begin
     if ( ActiveControl = GridRenglones )  then
         ModificaPrestamo
     else
         inherited;
end;

procedure TSaldosTotalesEmpleado.WMExaminar(var Message: TMessage);
begin
     ModificaPrestamo;
end;

procedure TSaldosTotalesEmpleado.ModificaPrestamo;
begin
     if PuedeModificarPresta then
        dmCajaAhorro.cdsTotalHisPrestamos.Modificar;
end;

function TSaldosTotalesEmpleado.PuedeAgregarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS, K_DERECHO_ALTA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para registrar préstamos',0);
end;

function TSaldosTotalesEmpleado.PuedeBorrarPresta: Boolean;
begin
     Result:= CheckDerecho( D_EMP_NOM_PRESTAMOS, K_DERECHO_BAJA);
     if not Result then
       ZInformation('Préstamos','No tiene permiso para borrar préstamos',0);
end;

function TSaldosTotalesEmpleado.PuedeModificarPresta: Boolean;
begin
     Result:= CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_CAMBIO);
     if not Result then
        ZInformation('Préstamos','No tiene permiso para modificar préstamos',0);
end;

function TSaldosTotalesEmpleado.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= False; //Revisa(D_AHORRO_REG_INSCRIP_EMP);
     if not Result then
        sMensaje:= 'No se pueden agregar datos';
end;

function TSaldosTotalesEmpleado.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= False; //Revisa(D_AHORRO_REG_LIQ_RET);
     if not Result then
        sMensaje:= 'No se pueden borrar datos';
end;

function TSaldosTotalesEmpleado.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= False; //Revisa(D_AHORRO_REG_LIQ_RET);
     if not Result then
        sMensaje:= 'No se pueden modificar datos';
end;

end.
