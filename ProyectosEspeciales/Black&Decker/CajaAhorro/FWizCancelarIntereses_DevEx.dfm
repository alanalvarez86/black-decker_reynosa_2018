inherited WizCancelarIntereses_DevEx: TWizCancelarIntereses_DevEx
  Left = 1289
  Top = 278
  Caption = 'Cancelaci'#243'n de intereses repartidos'
  ClientHeight = 466
  ClientWidth = 518
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 518
    Height = 466
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Proceso para la cancelaci'#243'n de intereses repartidos.'
      Header.Title = 'Cancelaci'#243'n de intereses repartidos.'
      object Label1: TLabel
        Left = 65
        Top = 167
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de ahorro:'
        Transparent = True
      end
      object gbNomina: TcxGroupBox
        Left = 76
        Top = 79
        Caption = ' Per'#237'odo de n'#243'mina '
        TabOrder = 0
        Height = 75
        Width = 371
        object Label5: TLabel
          Left = 140
          Top = 20
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Transparent = True
          Visible = False
        end
        object Label6: TLabel
          Left = 20
          Top = 44
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
          Transparent = True
        end
        object Label7: TLabel
          Left = 38
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object PE_TIPO: TZetaKeyCombo
          Left = 223
          Top = 14
          Width = 105
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          Visible = False
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object PE_YEAR: TZetaNumero
          Left = 63
          Top = 16
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
        end
        object PE_NUMERO: TZetaKeyLookup_DevEx
          Left = 63
          Top = 40
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object AH_AHORRO: TZetaKeyLookup_DevEx
        Left = 139
        Top = 161
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 231
        Width = 496
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 496
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 428
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.Description = 'Generar la lista de empleados para los que aplicar'#225' el proceso.'
      Header.Title = 'Filtro de empleados'
      inherited sCondicionLBl: TLabel
        Left = 53
      end
      inherited sFiltroLBL: TLabel
        Left = 78
      end
      inherited Seleccionar: TcxButton
        Left = 181
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 105
      end
      inherited sFiltro: TcxMemo
        Left = 105
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 105
      end
      inherited bAjusteISR: TcxButton
        Left = 419
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 300
  end
end
