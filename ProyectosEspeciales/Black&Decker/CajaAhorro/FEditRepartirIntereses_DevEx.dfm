inherited EditRepartirIntereses_DevEx: TEditRepartirIntereses_DevEx
  Left = 758
  Top = 416
  Width = 833
  Height = 370
  Caption = 'Repartici'#243'n de intereses'
  OnCreate = nil
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 296
    Width = 817
    inherited OK_DevEx: TcxButton
      Left = 643
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 721
      Cancel = True
    end
    inherited Imprimir: TcxButton
      ParentShowHint = False
      ShowHint = True
    end
    inherited Exportar: TcxButton
      ParentShowHint = False
      ShowHint = True
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 817
    Height = 296
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object ZetaDBGridDBTableViewCB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Options.Filtering = False
        Options.Grouping = False
        Options.Sorting = False
        Width = 66
      end
      object ZetaDBGridDBTableViewNOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'NOMBRE'
        Width = 154
      end
      object ZetaDBGridDBTableViewAHORRO_SEMANAL: TcxGridDBColumn
        Caption = 'Ahorrado Sem.'
        DataBinding.FieldName = 'AHORRO_SEMANAL'
        Visible = False
        Width = 20
      end
      object ZetaDBGridDBTableViewAHORRO_ACUMULADO: TcxGridDBColumn
        Caption = 'Ahorro Acum.'
        DataBinding.FieldName = 'AHORRO_ACUMULADO'
        Width = 69
      end
      object ZetaDBGridDBTableViewINTERES_ACUMULADO: TcxGridDBColumn
        Caption = 'Intereses Acum.'
        DataBinding.FieldName = 'INTERES_ACUMULADO'
        Width = 84
      end
      object ZetaDBGridDBTableViewCOMISIONES_ACUMULADO: TcxGridDBColumn
        Caption = 'Comisiones Acum.'
        DataBinding.FieldName = 'COMISIONES_ACUMULADO'
        Width = 89
      end
      object ZetaDBGridDBTableViewCAPITAL: TcxGridDBColumn
        Caption = 'Capital'
        DataBinding.FieldName = 'CAPITAL'
        Width = 71
      end
      object ZetaDBGridDBTableViewINTERESES_INV: TcxGridDBColumn
        Caption = 'Int. S/Inversi'#243'n'
        DataBinding.FieldName = 'INTERESES_INV'
        Width = 81
      end
      object ZetaDBGridDBTableViewINTERESES_PRESTAMO: TcxGridDBColumn
        Caption = 'Int. S/Prestamos'
        DataBinding.FieldName = 'INTERESES_PRESTAMO'
        Width = 85
      end
      object ZetaDBGridDBTableViewMONTO_COMISIONES: TcxGridDBColumn
        Caption = 'Monto X Comisiones'
        DataBinding.FieldName = 'MONTO_COMISIONES'
        Width = 108
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 432
    Top = 152
  end
end
