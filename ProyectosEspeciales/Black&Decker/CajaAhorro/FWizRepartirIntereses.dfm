inherited WizRepartirIntereses: TWizRepartirIntereses
  Left = 2122
  Top = 277
  Caption = 'Repartici'#243'n de intereses'
  ClientHeight = 392
  ClientWidth = 792
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 356
    Width = 792
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 706
      Anchors = [akRight, akBottom]
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 623
      Anchors = [akRight, akBottom]
    end
  end
  inherited PageControl: TPageControl
    Width = 792
    Height = 356
    ActivePage = TabSheet1
    inherited Parametros: TTabSheet
      object Label1: TLabel
        Left = 233
        Top = 170
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de ahorro:'
      end
      object Label2: TLabel
        Left = 209
        Top = 194
        Width = 96
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por inversi'#243'n:'
      end
      object Label13: TLabel
        Left = 202
        Top = 242
        Width = 103
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto de comisiones:'
      end
      object Label16: TLabel
        Left = 203
        Top = 218
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto por pr'#233'stamos:'
      end
      object AH_AHORRO: TZetaKeyLookup
        Left = 307
        Top = 166
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object InteresesXinversion: TZetaNumero
        Left = 307
        Top = 190
        Width = 105
        Height = 21
        Mascara = mnPesos
        TabOrder = 2
        Text = '0.00'
      end
      object gbNomina: TGroupBox
        Left = 244
        Top = 88
        Width = 383
        Height = 65
        Caption = ' Per'#237'odo de n'#243'mina '
        TabOrder = 0
        object Label5: TLabel
          Left = 132
          Top = 20
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Visible = False
        end
        object Label6: TLabel
          Left = 20
          Top = 43
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label7: TLabel
          Left = 38
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object PE_TIPO: TZetaKeyCombo
          Left = 159
          Top = 16
          Width = 105
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          Visible = False
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object PE_YEAR: TZetaNumero
          Left = 63
          Top = 16
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          OnExit = PE_YEARExit
        end
        object PE_NUMERO: TZetaKeyLookup
          Left = 63
          Top = 39
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object MontoComisiones: TZetaNumero
        Left = 307
        Top = 238
        Width = 105
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
      end
      object MontoXPrestamos: TZetaNumero
        Left = 307
        Top = 214
        Width = 105
        Height = 21
        Mascara = mnPesos
        TabOrder = 3
        Text = '0.00'
      end
    end
    object TabSheet1: TTabSheet [1]
      Caption = 'Resumen'
      ImageIndex = 3
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 81
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label3: TLabel
          Left = 26
          Top = 3
          Width = 120
          Height = 13
          Alignment = taRightJustify
          Caption = 'Intereses sobre inversi'#243'n:'
        end
        object InteresSobreInversion: TZetaTextBox
          Left = 149
          Top = 1
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label4: TLabel
          Left = 20
          Top = 22
          Width = 126
          Height = 13
          Alignment = taRightJustify
          Caption = 'Intereses sobre prestamos:'
        end
        object InteresesSobrePrestamos: TZetaTextBox
          Left = 149
          Top = 20
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label8: TLabel
          Left = 69
          Top = 60
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total de Capital:'
        end
        object CapitalTotal: TZetaTextBox
          Left = 149
          Top = 58
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label9: TLabel
          Left = 315
          Top = 60
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = '# de Empleados:'
        end
        object Empleados: TZetaTextBox
          Left = 397
          Top = 58
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label10: TLabel
          Left = 531
          Top = 3
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#243'mina:'
        end
        object Nomina: TZetaTextBox
          Left = 573
          Top = 1
          Width = 140
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label11: TLabel
          Left = 290
          Top = 3
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa Sobre Inversi'#243'n:'
        end
        object TasaSobreInversion: TZetaTextBox
          Left = 397
          Top = 1
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object TasaSobrePrestamos: TZetaTextBox
          Left = 397
          Top = 20
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label12: TLabel
          Left = 287
          Top = 22
          Width = 107
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa sobre prestamos:'
        end
        object Label14: TLabel
          Left = 40
          Top = 41
          Width = 106
          Height = 13
          Alignment = taRightJustify
          Caption = 'Monto por comisiones:'
        end
        object MontoXComision: TZetaTextBox
          Left = 149
          Top = 39
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label15: TLabel
          Left = 282
          Top = 41
          Width = 112
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa sobre Comisiones:'
        end
        object TasaXComisiones: TZetaTextBox
          Left = 397
          Top = 39
          Width = 110
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object ZetaDBGrid1: TZetaDBGrid
        Left = 0
        Top = 81
        Width = 784
        Height = 234
        Align = alClient
        DataSource = dsGrid
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taRightJustify
            Expanded = False
            FieldName = 'CB_CODIGO'
            Title.Alignment = taRightJustify
            Title.Caption = 'Empleado'
            Width = 58
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMBRE'
            Title.Caption = 'Nombre'
            Width = 172
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'AHORRO_ACUMULADO'
            Title.Caption = 'Ahorro Acum.'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INTERES_ACUMULADO'
            Title.Caption = 'Intereses Acum.'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'COMISIONES_ACUMULADO'
            Title.Caption = 'Comisiones Acum.'
            Width = 86
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CAPITAL'
            Title.Caption = 'Capital'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INTERESES_INV'
            Title.Caption = 'Int. S/Inversi'#243'n'
            Width = 78
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'INTERESES_PRESTAMO'
            Title.Caption = 'Int. S/Prestamos'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MONTO_COMISIONES'
            Title.Caption = 'Monto X Comisiones'
            Width = 103
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 315
        Width = 784
        Height = 30
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object btnEditarRegistros: TBitBtn
          Left = 345
          Top = 2
          Width = 112
          Height = 27
          Caption = 'Editar &registros'
          TabOrder = 0
          OnClick = btnEditarRegistrosClick
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            77777000000070000000000007777000000070FFFFFFFFFF07777000000070FC
            CFCCCCCF07777000000070FFFFFFFFFF07777000000070FCCFCCCCCF07777000
            000070FFFFFFFFFF07777000000070FFFFFFF0FF07777000000070F00FFF0B0F
            07770000000070F0F0F0B0F000700000000070FF0B0B0F0FBF00000000007000
            00F0F0FBFBF0000000007777770B0FBFBFB00000000077777770FBFBFB000000
            0000777777770000007000000000777777777777777770000000777777777777
            777770000000}
        end
      end
    end
    inherited Ejecucion: TTabSheet [2]
      inherited Advertencia: TMemo
        Width = 784
        Lines.Strings = (
          'Advertencia'
          ''
          'Los datos calculados para el reparto de intereses se aplicaran '
          'en el Ahorro de los empleados como uno Abono al mismo.'
          ''
          
            'Si ya se verificaron los calculos se puede continuar con el proc' +
            'eso'
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 278
        Width = 784
      end
    end
    inherited FiltrosCondiciones: TTabSheet [3]
      Enabled = False
    end
  end
  object dsGrid: TDataSource
    Left = 28
    Top = 183
  end
end
