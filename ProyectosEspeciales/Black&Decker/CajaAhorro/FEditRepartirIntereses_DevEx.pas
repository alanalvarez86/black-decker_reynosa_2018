unit FEditRepartirIntereses_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBasicoSelectGrid_DevEx, DB, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  Buttons, ExtCtrls, ZBasicoSelectGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEditRepartirIntereses_DevEx = class(TBasicoGridSelect_DevEx)
    ZetaDBGridDBTableViewCB_CODIGO: TcxGridDBColumn;
    ZetaDBGridDBTableViewNOMBRE: TcxGridDBColumn;
    ZetaDBGridDBTableViewAHORRO_SEMANAL: TcxGridDBColumn;
    ZetaDBGridDBTableViewAHORRO_ACUMULADO: TcxGridDBColumn;
    ZetaDBGridDBTableViewINTERES_ACUMULADO: TcxGridDBColumn;
    ZetaDBGridDBTableViewCOMISIONES_ACUMULADO: TcxGridDBColumn;
    ZetaDBGridDBTableViewCAPITAL: TcxGridDBColumn;
    ZetaDBGridDBTableViewINTERESES_INV: TcxGridDBColumn;
    ZetaDBGridDBTableViewINTERESES_PRESTAMO: TcxGridDBColumn;
    ZetaDBGridDBTableViewMONTO_COMISIONES: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  EditRepartirIntereses_DevEx: TEditRepartirIntereses_DevEx;

implementation

{$R *.dfm}

end.
