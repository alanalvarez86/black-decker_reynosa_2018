unit FGridHorarioTemp_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls,
     ZetaCommonLists,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaDBGrid, ZetaSmartLists, ZBaseGridEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TGridHorarioTemp_DevEx = class(TBaseGridEdicion_DevEx)
    zCombo: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure zComboClick(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaCodigo;
    procedure ChangeStatus(const eTipo: eStatusAusencia);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Buscar; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

var
  GridHorarioTemp_DevEx: TGridHorarioTemp_DevEx;

implementation

uses DAsistencia,
     DCatalogos,
     ZetaCommonClasses;

const
     FIELD_STATUS = 'AU_STATUS';
     FIELD_HORARIO = 'HO_CODIGO';
     
{$R *.DFM}

{ TGridHorarioTemp }

procedure TGridHorarioTemp_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     PrimerColumna := 1;
     HelpContext := H20222_Horarios_temporales;
end;

procedure TGridHorarioTemp_DevEx.Connect;
begin
     dmCatalogos.cdsHorarios.Conectar;
     with dmAsistencia do
     begin
          cdsHorarioTemp.Refrescar;
          DataSource.DataSet:= cdsHorarioTemp;
     end;
end;

procedure TGridHorarioTemp_DevEx.Agregar;
begin
     { No hace nada }
end;

procedure TGridHorarioTemp_DevEx.Borrar;
begin
     { No hace nada }
end;

procedure TGridHorarioTemp_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = FIELD_HORARIO ) then
             BuscaCodigo;
     end;
end;

procedure TGridHorarioTemp_DevEx.BuscaCodigo;
var
   sHorario, sHorarioDesc: String;
begin
     with dmAsistencia.cdsHorarioTemp do
     begin
          sHorario := FieldByName( FIELD_HORARIO ).AsString;
          if ( dmCatalogos.cdsHorarios.Search_DevEx( '', sHorario, sHorarioDesc ) ) then
          begin
               if ( sHorario <> FieldByName( FIELD_HORARIO ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( FIELD_HORARIO ).AsString := sHorario;
               end;
          end;
     end;
end;

procedure TGridHorarioTemp_DevEx.ChangeStatus( const eTipo: eStatusAusencia );
begin
     with dmAsistencia.cdsHorarioTemp do
     begin
          if ( eTipo <> eStatusAusencia( FieldByName( FIELD_STATUS ).AsInteger ) ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( FIELD_STATUS ).AsInteger := Ord( eTipo );
          end;
     end;
end;

procedure TGridHorarioTemp_DevEx.KeyPress(var Key: Char);

procedure CambiaStatus( const Tipo: eStatusAusencia );
begin
     ChangeStatus( Tipo );
     Key := #0;
end;

begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = FIELD_STATUS ) then
               begin
                    zCombo.SetFocus;
                    SendMessage( zCombo.Handle, WM_Char, Word( Key ), 0 );
                    Key := #0;
               end
               else
                   if ( ZetaDBGrid.SelectedField.FieldName = FIELD_STATUS ) then
                   begin
                        case Key of
                             'H', 'h' : CambiaStatus( auHabil );
                             'S', 's' : CambiaStatus( auSabado );
                             'D', 'd' : CambiaStatus( auDescanso );
                        end;
                   end;
          end;
     end
     else
         if ( ActiveControl = zCombo ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SeleccionaPrimerColumna;
                   SeleccionaSiguienteRenglon;
              end;
         end;
end;

procedure TGridHorarioTemp_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = FIELD_STATUS ) and
             ( zCombo.Visible ) then
          begin
               zCombo.Visible := False;
          end;
     end;
end;

procedure TGridHorarioTemp_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = FIELD_STATUS ) then
          begin
               with zCombo do
               begin
                    Left := Rect.Left + ZetaDBGrid.Left;
                    Top := Rect.Top + ZetaDBGrid.Top;
                    Width := Column.Width + 2;
                    Visible := True;
                    Valor := Column.Field.AsInteger;
               end;
          end;
     end;
end;

procedure TGridHorarioTemp_DevEx.zComboClick(Sender: TObject);
begin
     inherited;
     ChangeStatus( eStatusAusencia( zCombo.Valor ) );
end;

end.
