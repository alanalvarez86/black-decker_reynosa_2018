unit FAutorizaciones_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseAsisConsulta_DevEx, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons, Db,
  ExtCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxTextEdit,
  cxButtons, Mask, ZetaFecha, FtressShell;

type
  TAutorizaciones_DevEx = class(TBaseAsisConsulta_DevEx)
    PanelBoton: TPanel;
    BtnModificarTodas_DevEx: TcxButton;
    Panel1: TPanel;
    AsistenciaFechaZF: TZetaFecha;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure BtnModificarTodasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AsistenciaFechaZFValidDate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    procedure SetColumnasGrid( const lVisible: boolean );
  protected
    { Protected declarations }
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Autorizaciones_DevEx: TAutorizaciones_DevEx;

implementation

uses dAsistencia, dSistema, dGlobal, ZetaCommonLists, ZetaCommonClasses, dTablas, dCliente, 
     ZAccesosMgr, ZAccesosTress, ZGlobalTress;

{$R *.DFM}

procedure TAutorizaciones_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
  
     HelpContext := H20221_Autorizar_extras_y_permisos;
     BtnModificarTodas_DevEx.Enabled := ZAccesosMgr.CheckDerecho( D_ASIS_REG_AUTO_EXTRAS, K_DERECHO_CONSULTA );
end;

procedure TAutorizaciones_DevEx.FormShow(Sender: TObject);
begin
 CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
      AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
       ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
       ZetaDBGridDBTableView.OptionsView.GroupByBox := true;
     SetColumnasGrid( dmAsistencia.AprobarPrendido );
end;

procedure TAutorizaciones_DevEx.Connect;
begin
     dmTablas.cdsMotAuto.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmAsistencia do
     begin
          cdsAutorizaciones.Conectar;
          DataSource.DataSet:= cdsAutorizaciones;
     end;
   // CreaColumaSumatoria('HORAS',skSum);
   // CreaColumaSumatoria('US_DESCRIP',skCount);

   end;

procedure TAutorizaciones_DevEx.Refresh;
begin
     dmAsistencia.cdsAutorizaciones.Refrescar;
     AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

procedure TAutorizaciones_DevEx.Agregar;
begin
     dmAsistencia.cdsAutorizaciones.Agregar;
end;

procedure TAutorizaciones_DevEx.Borrar;
begin
     dmAsistencia.cdsAutorizaciones.Borrar;
end;

procedure TAutorizaciones_DevEx.Modificar;
begin
     dmAsistencia.cdsAutorizaciones.Modificar;
end;

procedure TAutorizaciones_DevEx.BtnModificarTodasClick(Sender: TObject);
begin
     dmAsistencia.EditarGridAutorizaciones( False );
end;

procedure TAutorizaciones_DevEx.SetColumnasGrid( const lVisible: boolean );
const
     K_COLUMNA_APROBADAS = 7;
     K_COLUMNA_APROBADAS_POR = 8;
begin
     with ZetaDbGridDbTableview do
     begin
          Columns[ K_COLUMNA_APROBADAS ].Visible := lVisible;
          Columns[ K_COLUMNA_APROBADAS_POR ].Visible := lVisible;
     end;
end;

function TAutorizaciones_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := Inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          if dmAsistencia.AprobarPrendido then
          begin
               if ( dmAsistencia.cdsAutorizaciones.FieldByName( 'US_COD_OK' ).AsInteger > 0 ) then
               begin
                    Result := dmAsistencia.TieneDerechoAprobar;
                    if not Result then
                    begin
                         sMensaje := 'Usuario No Puede Borrar Autorización Ya Aprobada';
                    end;
               end;
          end;
     end;
end;

procedure TAutorizaciones_DevEx.AsistenciaFechaZFValidDate(
  Sender: TObject);
begin
  inherited;
  TressShell.AsistenciaFechavalid(AsistenciaFechaZF.Valor);
  dmAsistencia.cdsAutorizaciones.ResetDataChange;
  end;

procedure TAutorizaciones_DevEx.FormActivate(Sender: TObject);
begin
  inherited;
 AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

procedure TAutorizaciones_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
 AsistenciaFechaZF.Valor :=  dmCliente.FechaAsistencia;
end;

end.
