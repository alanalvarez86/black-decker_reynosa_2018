inherited AsisCalendario_DevEx: TAsisCalendario_DevEx
  Left = 201
  Top = 197
  Caption = 'Calendario de Asistencia'
  ClientHeight = 305
  ClientWidth = 615
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 615
    inherited Slider: TSplitter
      Left = 427
    end
    inherited ValorActivo1: TPanel
      Width = 411
      inherited textoValorActivo1: TLabel
        Width = 405
      end
    end
    inherited ValorActivo2: TPanel
      Left = 430
      Width = 185
      inherited textoValorActivo2: TLabel
        Width = 179
      end
    end
  end
  object PanelParam: TPanel [1]
    Left = 0
    Top = 19
    Width = 615
    Height = 38
    Align = alTop
    TabOrder = 2
    object lblIncidencia: TLabel
      Left = 228
      Top = 13
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = '&Incidencia:'
      Enabled = False
      Visible = False
    end
    object Label1: TLabel
      Left = 13
      Top = 11
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Inicial:'
    end
    object Label2: TLabel
      Left = 217
      Top = 11
      Width = 58
      Height = 13
      Caption = 'Fecha Final:'
    end
    object lblDia: TLabel
      Left = 55
      Top = 61
      Width = 21
      Height = 13
      Caption = 'D'#237'a:'
      Enabled = False
      Visible = False
    end
    object lblTipo: TLabel
      Left = 256
      Top = 37
      Width = 24
      Height = 13
      Caption = 'Tipo:'
      Enabled = False
      Visible = False
    end
    object lblRegistros: TLabel
      Left = 233
      Top = 62
      Width = 47
      Height = 13
      Caption = 'Registros:'
      Enabled = False
      Visible = False
    end
    object ztbRegistros: TZetaTextBox
      Left = 283
      Top = 60
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = '0'
      Enabled = False
      ShowAccelChar = False
      Visible = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ZFechaIni: TZetaFecha
      Left = 80
      Top = 8
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '26/ago/99'
      UseEnterKey = True
      Valor = 36398.000000000000000000
      OnValidDate = ZFechaValidDate
    end
    object AU_STATUS: TZetaKeyCombo
      Left = 80
      Top = 57
      Width = 115
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Enabled = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      Visible = False
      OnChange = AU_STATUSChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object AU_TIPODIA: TZetaKeyCombo
      Left = 283
      Top = 33
      Width = 115
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      Enabled = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 4
      Visible = False
      OnChange = AU_STATUSChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object AU_TIPO: TZetaKeyLookup_DevEx
      Left = 384
      Top = 9
      Width = 199
      Height = 21
      Enabled = False
      LookupDataset = dmTablas.cdsIncidencias
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 3
      TabStop = True
      Visible = False
      WidthLlave = 60
      OnValidKey = AU_STATUSChange
    end
    object ZFechaFin: TZetaFecha
      Tag = 1
      Left = 283
      Top = 8
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '26/ago/99'
      UseEnterKey = True
      Valor = 36398.000000000000000000
      OnValidDate = ZFechaValidDate
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 57
    Width = 615
    Height = 248
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object AU_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'AU_FECHA'
        Options.Grouping = False
        Width = 75
      end
      object AU_STATUS_GRID: TcxGridDBColumn
        Caption = 'D'#237'a'
        DataBinding.FieldName = 'AU_STATUS'
        Width = 61
      end
      object AU_TIPODIA_GRID: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'AU_TIPODIA'
        Width = 84
      end
      object AU_HORASCK: TcxGridDBColumn
        Caption = 'Ordinarias'
        DataBinding.FieldName = 'AU_HORASCK'
        HeaderAlignmentHorz = taRightJustify
        Options.Grouping = False
        Width = 53
      end
      object AU_HORAS: TcxGridDBColumn
        Caption = 'Por Pagar'
        DataBinding.FieldName = 'AU_HORAS'
        Options.Grouping = False
        Width = 52
      end
      object AU_EXTRAS: TcxGridDBColumn
        Caption = 'Extras'
        DataBinding.FieldName = 'AU_EXTRAS'
        HeaderAlignmentHorz = taRightJustify
        Options.Grouping = False
        Width = 37
      end
      object AU_PER_CG: TcxGridDBColumn
        Caption = 'Permiso C/G'
        DataBinding.FieldName = 'AU_PER_CG'
        HeaderAlignmentHorz = taRightJustify
        Options.Grouping = False
      end
      object AU_PER_SG: TcxGridDBColumn
        Caption = 'Permiso S/G'
        DataBinding.FieldName = 'AU_PER_SG'
        HeaderAlignmentHorz = taRightJustify
        Options.Grouping = False
      end
      object AU_DES_TRA: TcxGridDBColumn
        Caption = 'Descanso'
        DataBinding.FieldName = 'AU_DES_TRA'
        HeaderAlignmentHorz = taRightJustify
        Options.Grouping = False
        Width = 55
      end
      object AU_TARDES: TcxGridDBColumn
        Caption = 'Tardes'
        DataBinding.FieldName = 'AU_TARDES'
        HeaderAlignmentHorz = taRightJustify
        Options.Grouping = False
        Width = 40
      end
      object AU_TIPO_GRID: TcxGridDBColumn
        Caption = 'Incidencia'
        DataBinding.FieldName = 'AU_TIPO'
        Width = 60
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 56
    Top = 144
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = -523920
  end
  inherited ActionList: TActionList
    Left = 330
    Top = 65528
  end
  inherited PopupMenu1: TPopupMenu
    Left = 296
    Top = 65528
  end
end
