inherited ClasifTemporal_DevEx: TClasifTemporal_DevEx
  Left = 509
  Top = 365
  Caption = 'Clasificaciones Temporales'
  ClientHeight = 220
  ClientWidth = 619
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 619
    inherited ValorActivo2: TPanel
      Width = 360
      inherited textoValorActivo2: TLabel
        Width = 354
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 52
    Width = 619
    Height = 168
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        MinWidth = 70
        Options.Grouping = False
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 120
        Options.Grouping = False
      end
      object CB_PUESTO: TcxGridDBColumn
        Caption = 'Puesto'
        DataBinding.FieldName = 'CB_PUESTO'
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
      object CB_CLASIFI: TcxGridDBColumn
        Caption = 'Clasific.'
        DataBinding.FieldName = 'CB_CLASIFI'
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
      object CB_TURNO: TcxGridDBColumn
        Caption = 'Turno'
        DataBinding.FieldName = 'CB_TURNO'
        MinWidth = 70
        Options.Grouping = False
      end
    end
  end
  object PanelBoton: TPanel [2]
    Left = 0
    Top = 19
    Width = 619
    Height = 33
    Align = alTop
    TabOrder = 2
    DesignSize = (
      619
      33)
    object Panel1: TPanel
      Left = 312
      Top = 0
      Width = 307
      Height = 33
      Anchors = [akTop, akRight]
      BevelOuter = bvNone
      TabOrder = 0
      DesignSize = (
        307
        33)
      object Label1: TLabel
        Left = 102
        Top = 12
        Width = 69
        Height = 13
        Anchors = [akTop, akRight]
        Caption = 'Fecha Activa :'
      end
      object AsistenciaFechaZF: TZetaFecha
        Left = 182
        Top = 6
        Width = 115
        Height = 22
        Cursor = crArrow
        Hint = 'Fecha Default para Tarjetas de Asistencia'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '10/dic/97'
        UseEnterKey = True
        Valor = 35774.000000000000000000
        OnValidDate = AsistenciaFechaZFValidDate
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end