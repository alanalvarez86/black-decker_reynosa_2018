unit FGridAutorizacionesXAprobar_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridAutorizacionesPorAprobar_DevEx, Db, StdCtrls, Mask,
  ZetaFecha, ZetaKeyCombo, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ZetaClientDataSet,  
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, ZetaKeyLookup_DevEx, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TGridAutorizacionesXAprobar_DevEx = class(TBaseGridAutorizacionesPorAprobar_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure zFechaValidDate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function ChecaDerecho: boolean; override;
    procedure Connect; override;
    procedure InicializaControles; override;
  public
    { Public declarations }
  end;

var
  GridAutorizacionesXAprobar_DevEx: TGridAutorizacionesXAprobar_DevEx;

implementation

{$R *.DFM}

uses
    DCliente,
    DAsistencia,
    DTablas,
    DGlobal,
    ZGlobalTress,
    ZetaCommonTools,
    ZetaCommonClasses;

procedure TGridAutorizacionesXAprobar_DevEx.FormCreate(Sender: TObject);
begin
     Supervisores.LookupDataset := dmTablas.cdsSupervisores;
     AutoXAprobar := dmAsistencia.cdsAutoXAprobar;
     HelpContext := H20223_Autorizaciones_por_aprobar;     
     inherited;
end;

procedure TGridAutorizacionesXAprobar_DevEx.Connect;
begin
     dmTablas.cdsSupervisores.Conectar;
      {$ifndef CAJAAHORRO}
     dmAsistencia.FechaGridAuto := dmCliente.FechaAsistencia;
     {$endif}
     inherited;
end;

function TGridAutorizacionesXAprobar_DevEx.ChecaDerecho: boolean;
begin
     Result := dmAsistencia.TieneDerechoAprobar;
end;

procedure TGridAutorizacionesXAprobar_DevEx.InicializaControles;
begin
     inherited;
     zFecha.Valor := dmAsistencia.FechaGridAuto;
end;

procedure TGridAutorizacionesXAprobar_DevEx.zFechaValidDate(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     dmAsistencia.FechaGridAuto := zFecha.Valor;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        AutoXAprobar.Refrescar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
