unit FEditCalendario_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBCtrls, StdCtrls, ExtCtrls, Mask, ComCtrls,
     ZetaDBTextBox, ZetaCommonClasses, ZetaKeyCombo,
     ZetaKeyLookup_DevEx, ZetaNumero, ZetaFecha, ZBaseAsisEdicion_DevEx,
     cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
     ImgList, cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
     cxPCdxBarPopupMenu, cxPC;

type
  TEditCalendario_DevEx = class(TBaseAsisEdicion_DevEx)
    PageControl: TcxPageControl;
    ClasificacionTab: TcxTabSheet;
    HorasTab: TcxTabSheet;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Panel3: TPanel;
    NivelesTab: TcxTabSheet;
    CB_NIVEL1Lbl: TLabel;
    CB_NIVEL2Lbl: TLabel;
    CB_NIVEL3Lbl: TLabel;
    CB_NIVEL4Lbl: TLabel;
    CB_NIVEL5Lbl: TLabel;
    CB_NIVEL6Lbl: TLabel;
    CB_NIVEL7Lbl: TLabel;
    CB_NIVEL8Lbl: TLabel;
    CB_NIVEL9Lbl: TLabel;
    Label20: TLabel;
    Label10: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    AU_TIPO: TZetaDBKeyLookup_DevEx;
    HO_CODIGO: TZetaDBKeyLookup_DevEx;
    Label11: TLabel;
    Label12: TLabel;
    Label22: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    AU_STATUS: TZetaDBKeyCombo;
    AU_TIPODIA: TZetaDBKeyCombo;
    Label5: TLabel;
    AU_HORASCK: TZetaDBTextBox;
    AU_DOBLES: TZetaDBTextBox;
    AU_TRIPLES: TZetaDBTextBox;
    AU_EXTRAS: TZetaDBTextBox;
    AU_PER_CG: TZetaDBTextBox;
    AU_PER_SG: TZetaDBTextBox;
    AU_DES_TRA: TZetaDBTextBox;
    AU_TARDES: TZetaDBTextBox;
    AU_FECHA: TZetaDBFecha;
    Label6: TLabel;
    AU_HORAS: TZetaDBTextBox;
    Label7: TLabel;
    AU_HOR_MAN: TDBCheckBox;
    AU_DIA_SEM: TZetaDBTextBox;
    bbMostrarCalendario: TcxButton;
    CB_NIVEL10Lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    CB_NIVEL11Lbl: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    CB_NIVEL12Lbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    procedure ActivaClasificacion;
    procedure SetCamposNivel;
    {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
    procedure CambiosVisuales;
    {$ifend}
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  EditCalendario_DevEx: TEditCalendario_DevEx;

implementation

uses FTressShell,
     DAsistencia,
     DCatalogos,
     DTablas,
     DGlobal,
     DSistema,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAccesosTress;

{$R *.DFM}

procedure TEditCalendario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.LookupDataSet := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataSet := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataSet := dmTablas.cdsNivel12;
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}

     IndexDerechos := D_ASIS_DATOS_CALENDARIO;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     HelpContext := H22131_calendario_edicion; //H20213_Calendario;
     FirstControl := AU_STATUS;
     SetCamposNivel;

     with dmTablas do
     begin
          CB_NIVEL1.LookUpDataSet := cdsNivel1;
          CB_NIVEL2.LookUpDataSet := cdsNivel2;
          CB_NIVEL3.LookUpDataSet := cdsNivel3;
          CB_NIVEL4.LookUpDataSet := cdsNivel4;
          CB_NIVEL5.LookUpDataSet := cdsNivel5;
          CB_NIVEL6.LookUpDataSet := cdsNivel6;
          CB_NIVEL7.LookUpDataSet := cdsNivel7;
          CB_NIVEL8.LookUpDataSet := cdsNivel8;
          CB_NIVEL9.LookUpDataSet := cdsNivel9;
          AU_TIPO.LookUpDataSet := cdsIncidencias;
     end;

     with dmCatalogos do
     begin
          CB_TURNO.LookUpDataSet := cdsTurnos;
          CB_PUESTO.LookUpDataSet := cdsPuestos;
          CB_CLASIFI.LookUpDataSet := cdsClasifi;
          HO_CODIGO.LookUpDataSet := cdsHorarios;
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditCalendario_DevEx.FormShow(Sender: TObject);
begin
     ActivaClasificacion;
     inherited;
     {$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
      CambiosVisuales;
     {$ifend}
end;

procedure TEditCalendario_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          cdsClasifi.Conectar;
          cdsHorarios.Conectar;
     end;
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmAsistencia do
     begin
          cdsAsisCalendario.Conectar;
          DataSource.DataSet := cdsAsisCalendario;
     end;
end;

procedure TEditCalendario_DevEx.ActivaClasificacion;
begin
     PageControl.ActivePage := ClasificacionTab;
end;

procedure TEditCalendario_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     AU_FECHA.Enabled := Inserting;
     if Inserting then
        FirstControl := AU_FECHA;
end;

procedure TEditCalendario_DevEx.BorrarBtnClick(Sender: TObject);
 var
    sMensaje: string;
    lPuedeCerrar: Boolean;
begin
     //Borrar;
     //CV: Si se manda llamar el 'Borrar' no se validan los derechos de acceso.
     lPuedeCerrar := PuedeBorrar( sMensaje ) ;

     inherited dxBarButton_BorrarBtnClick( Sender );
     if lPuedeCerrar then
        Close;
end;

procedure TEditCalendario_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10Lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11Lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12Lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TEditCalendario_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     {$ifndef CAJAAHORRO}
      {$ifndef EVALUACION }
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
     {$endif}
     {$endif}
end;

procedure TEditCalendario_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmAsistencia.cdsAsisCalendario do
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;

{$IF Defined(ANCHO_NIVELES_8) or Defined(ANCHO_NIVELES_10) or Defined(ANCHO_NIVELES_12)}
procedure TEditCalendario_DevEx.CambiosVisuales;
var
  I : Integer;
begin
     for I := 0 to NivelesTab.ControlCount - 1 do
     begin
          if NivelesTab.Controls[I].ClassNameIs( 'TLabel' ) then
          begin
               NivelesTab.Controls[I].Left := K_WIDTH_FORMCALENDARIO -(NivelesTab.Controls[I].Width+2);
          end;
          if NivelesTab.Controls[I].ClassNameIs( 'TZetaDbKeyLookup_DevEx' ) then
          begin
               if ( NivelesTab.Controls[I].Visible ) then
               begin
                    NivelesTab.Controls[I].Width := K_WIDTH_LOOKUP;
                    TZetaDbKeyLookup_DevEx( NivelesTab.Controls[I]).WidthLlave := K_WIDTHLLAVE;
                    NivelesTab.Controls[I].Left := K_WIDTH_FORMCALENDARIO;
               end;
          end;
     end;
end;
{$ifend}

procedure TEditCalendario_DevEx.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
     CB_TURNO.EditarSoloActivos := TRUE;
     HO_CODIGO.EditarSoloActivos := TRUE;
     CB_NIVEL1.EditarSoloActivos := TRUE;
     CB_NIVEL2.EditarSoloActivos := TRUE;
     CB_NIVEL3.EditarSoloActivos := TRUE;
     CB_NIVEL4.EditarSoloActivos := TRUE;
     CB_NIVEL5.EditarSoloActivos := TRUE;
     CB_NIVEL6.EditarSoloActivos := TRUE;
     CB_NIVEL7.EditarSoloActivos := TRUE;
     CB_NIVEL8.EditarSoloActivos := TRUE;
     CB_NIVEL9.EditarSoloActivos := TRUE;
     CB_NIVEL10.EditarSoloActivos := TRUE;
     CB_NIVEL11.EditarSoloActivos := TRUE;
     CB_NIVEL12.EditarSoloActivos := TRUE;
end;

end.
