unit FAsisCalendario_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, ExtCtrls, StdCtrls, Mask,
     ZBaseAsisConsulta_DevEx,  ZetaCommonLists,
     ZetaFecha, ZetaDBTextBox, ZetaKeyLookup_DevEx, ZetaKeyCombo,
     cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
     cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
     cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
     Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
     cxGrid, ZetaCXGrid;

type
  TAsisCalendario_DevEx = class(TBaseAsisConsulta_DevEx)
    PanelParam: TPanel;
    ZFechaIni: TZetaFecha;
    Label1: TLabel;
    Label2: TLabel;
    ZFechaFin: TZetaFecha;
    lblDia: TLabel;
    lblTipo: TLabel;
    lblIncidencia: TLabel;
    AU_STATUS: TZetaKeyCombo;
    AU_TIPODIA: TZetaKeyCombo;
    AU_TIPO: TZetaKeyLookup_DevEx;
    lblRegistros: TLabel;
    ztbRegistros: TZetaTextBox;
    AU_FECHA: TcxGridDBColumn;
    AU_STATUS_GRID: TcxGridDBColumn;
    AU_TIPODIA_GRID: TcxGridDBColumn;
    AU_HORASCK: TcxGridDBColumn;
    AU_HORAS: TcxGridDBColumn;
    AU_EXTRAS: TcxGridDBColumn;
    AU_PER_CG: TcxGridDBColumn;
    AU_PER_SG: TcxGridDBColumn;
    AU_DES_TRA: TcxGridDBColumn;
    AU_TARDES: TcxGridDBColumn;
    AU_TIPO_GRID: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure ZFechaValidDate(Sender: TObject);
    procedure AU_STATUSChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure LlenaListas( oLista: TStrings; oLFijas: ListasFijas );
    procedure PreparaFiltro;
    procedure CuentaRegistros;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  AsisCalendario_DevEx: TAsisCalendario_DevEx;

implementation

{$R *.DFM}

uses DAsistencia,
     DSistema,
     DTablas,
     ZetaCommonTools,
     ZetaCommonClasses, ZBaseGridLectura_DevEx;

procedure TAsisCalendario_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     with dmAsistencia do
     begin
          SetCalendarioInicio;
          ZFechaIni.Valor := CalFechaIni;
          ZFechaFin.Valor := CalFechaFin;
     end;
     AU_TIPO.LookUpDataSet := dmTablas.cdsIncidencias;
     with AU_STATUS do
     begin
          LlenaListas( Lista, lfStatusAusencia );
          ItemIndex := 0;
     end;
     with AU_TIPODIA do
     begin
          LlenaListas( Lista, lfTipoDiaAusencia );
          ItemIndex := 0;
     end;
     CuentaRegistros;
     HelpContext:= H20213_Calendario;
end;

procedure TAsisCalendario_DevEx.LlenaListas( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eStatusAusencia;
   eValueTipo: eTipoDiaAusencia;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  if( oLFijas = lfStatusAusencia )then
                  begin
                       for eValueStatus := ( Low( eStatusAusencia ) ) to High( eStatusAusencia ) do
                       begin
                            AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
                       end;
                  end
                  else
                  begin
                       for eValueTipo := ( Low( eTipoDiaAusencia ) ) to High( eTipoDiaAusencia ) do
                       begin
                            AddObject( ObtieneElemento( oLFijas, Ord( eValueTipo ) ), TObject( Ord( eValueTipo ) ) );
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;

procedure TAsisCalendario_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmTablas.cdsIncidencias.Conectar;
     with dmAsistencia do
     begin
          cdsAsisCalendario.Conectar;
          DataSource.DataSet:= cdsAsisCalendario;
     end;
     PreparaFiltro;
end;

procedure TAsisCalendario_DevEx.Refresh;
begin
     dmAsistencia.cdsAsisCalendario.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TAsisCalendario_DevEx.ZFechaValidDate(Sender: TObject);
begin
     inherited;
     with Sender as TZetaFecha do
     begin
          case Tag of
               0: dmAsistencia.CalFechaIni := Valor;
               1: dmAsistencia.CalFechaFin := Valor;
          end;
     end;
     Self.Refresh;
     CuentaRegistros;
end;

procedure TAsisCalendario_DevEx.Agregar;
begin
     dmAsistencia.cdsAsisCalendario.Agregar;
end;

procedure TAsisCalendario_DevEx.Borrar;
begin
     dmAsistencia.cdsAsisCalendario.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TAsisCalendario_DevEx.Modificar;
begin
     dmAsistencia.cdsAsisCalendario.Modificar;
end;

procedure TAsisCalendario_DevEx.PreparaFiltro;
var
   sFiltro: String;
   function GetFiltro( sCampo: String; lLleno: Boolean):String;
   begin
        if( lLleno )then
            Result := sFiltro + ' and ' + sCampo
        else
            Result := sCampo;
   end;
begin
     sFiltro := VACIO;
     if( AU_STATUS.LlaveEntero <> 0 )then
         sFiltro := Format( 'AU_STATUS = %d', [AU_STATUS.LlaveEntero - 1] )
     else
         sFiltro := VACIO;
     if( AU_TIPODIA.LlaveEntero <> 0 )then
     begin
          sFiltro := GetFiltro( Format( 'AU_TIPODIA = %d', [AU_TIPODIA.LlaveEntero - 1] ), ( sFiltro <> VACIO ) );
     end;
     if( AU_TIPO.Llave <> VACIO )then
     begin
          sFiltro := GetFiltro( Format( 'AU_TIPO = %s ', [EntreComillas(AU_TIPO.Llave)] ), ( sFiltro <> VACIO ) );
     end;
     dmAsistencia.AplicaFiltros( sFiltro );
     CuentaRegistros;
end;

procedure TAsisCalendario_DevEx.CuentaRegistros;
begin
     ztbRegistros.Caption :=  InttoStr( dmAsistencia.cdsAsisCalendario.RecordCount );
end;

procedure TAsisCalendario_DevEx.AU_STATUSChange(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
end;

procedure TAsisCalendario_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     //agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('AU_FECHA'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
