inherited GridAjusteTarjetasPeriodo: TGridAjusteTarjetasPeriodo
  Left = 196
  Top = 150
  Width = 868
  Height = 374
  BorderStyle = bsSizeable
  Caption = 'Ajustes de Tarjetas de Per'#237'odo'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 281
    Width = 852
    inherited OK: TBitBtn
      Left = 692
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
    end
    inherited Cancelar: TBitBtn
      Left = 777
      ModalResult = 0
      Kind = bkCustom
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 852
    inherited ValorActivo2: TPanel
      Width = 526
    end
  end
  inherited PanelSuperior: TPanel
    Width = 852
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Hint = 'Excluir Registro (Shift+Del)'
      Visible = False
    end
    inherited ImprimirFormaBtn: TSpeedButton
      Left = 378
    end
    inherited UndoBtn: TSpeedButton
      Left = 233
    end
    object btnEntrar: TBitBtn
      Left = 796
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Agregar entrada puntual'
      Anchors = [akTop, akRight]
      Caption = 'E'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = TarjetaPuntualClick
      NumGlyphs = 2
    end
    object btnSalir: TBitBtn
      Tag = 1
      Left = 828
      Top = 4
      Width = 24
      Height = 24
      Hint = 'Agregar salida puntual'
      Anchors = [akTop, akRight]
      Caption = 'S'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = TarjetaPuntualClick
      NumGlyphs = 2
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 852
    Height = 230
    Ctl3D = False
    ParentCtl3D = False
    OnColExit = ZetaDBGridColExit
    OnDrawColumnCell = ZetaDBGridDrawColumnCell
    FixedCols = 2
    Columns = <
      item
        Expanded = False
        ReadOnly = True
        Title.Caption = 'N'#250'mero'
        Visible = False
      end
      item
        Expanded = False
        FieldName = 'AU_FECHA'
        ReadOnly = True
        Title.Caption = 'Fecha y D'#237'a'
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'IN_ELEMENT'
        ReadOnly = True
        Title.Caption = 'Status'
        Width = 58
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA1'
        Title.Caption = 'Ent. 1'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA1'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA2'
        Title.Caption = 'Sal. 1'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA2'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA3'
        Title.Caption = 'Ent. 2'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA3'
        Title.Caption = 'Motivo'
        Width = 38
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'CHECADA4'
        Title.Caption = 'Sal. 2'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MC_CHECADA4'
        Title.Caption = 'Motivo'
        Width = 37
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HO_CODIGO'
        Title.Caption = 'Horario'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'HO_DESCRIP'
        ReadOnly = True
        Title.Caption = 'Descripci'#243'n'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'AU_STATUS'
        Title.Caption = 'Tipo de D'#237'a'
        Width = 60
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_HORASCK'
        ReadOnly = True
        Title.Caption = 'Ordinarias'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_NUM_EXT'
        ReadOnly = True
        Title.Caption = 'Hrs. Extras'
        Width = 55
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'HRS_EXTRAS'
        Title.Caption = 'Extras Aut.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_HRS_EXTRAS'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRE_FUERA_JOR'
        Title.Caption = 'Prep. F/J'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PRE_FUERA_JOR'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRE_DENTRO_JOR'
        Title.Caption = 'Prep. D/J'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PRE_DENTRO_JOR'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'DESCANSO'
        Title.Caption = 'Desc. Trab.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_DESCANSO'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_CG'
        Title.Caption = 'Perm. C/G'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_CG'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_CG_ENT'
        Title.Caption = 'C/G Ent.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_CG_ENT'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_SG'
        Title.Caption = 'Perm. S/G'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_SG'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'PER_SG_ENT'
        Title.Caption = 'S/G Ent.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'M_PER_SG_ENT'
        Title.Caption = 'Motivo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_OUT2EAT'
        Title.Caption = 'Comi'#243
        Width = 160
        Visible = True
      end>
  end
  object zCombo: TZetaDBKeyCombo [4]
    Left = 272
    Top = 144
    Width = 83
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 4
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfStatusAusencia
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'AU_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object zComboComio: TZetaDBKeyCombo [5]
    Left = 272
    Top = 168
    Width = 100
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 5
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfOut2Eat
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'AU_OUT2EAT'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object StatusBar: TStatusBar [6]
    Left = 0
    Top = 317
    Width = 852
    Height = 19
    Panels = <
      item
        Text = 'dgfg'
        Width = 50
      end>
    SimplePanel = True
  end
  inherited DataSource: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 380
    Top = 145
  end
end
