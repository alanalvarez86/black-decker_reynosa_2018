unit FCafeteraServicioUI;

interface

uses
  FBaseServerCafeUI, Messages, Vcl.Controls, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls, Vcl.Buttons, System.Classes, Vcl.ImgList,
  System.Actions, Vcl.ActnList, Vcl.Menus, Vcl.StdActns, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, dxBarBuiltInMenu, cxControls,
  cxContainer, cxEdit, cxClasses, dxSkinsForm, cxTextEdit, cxMemo, cxPC,
  cxButtons;

type
  TFormaCafeteraServicioUI = class(TBaseServerCafeUI)
    actConfigConexion: TAction;
    lblUltima: TLabel;
    actActualizar: TAction;
    Conexion_DevEx: TcxButton;
    Actualizar_DevEx: TcxButton;
    procedure TimerTimer(Sender: TObject);
    procedure actConfigConexionExecute(Sender: TObject);
    procedure actConfigServidorExecute(Sender: TObject);
    procedure actActualizarExecute(Sender: TObject);
  private
    { Private declarations }
    procedure SeHaConectado(Sender: TObject);
  public
    { Public declarations }
    procedure Init; override;
  end;

var
  FormaCafeteraServicioUI: TFormaCafeteraServicioUI;

implementation

uses
  SysUtils, DB, Windows, Forms, DServerCafeUI, CafeteraConsts, CafeteraUtils, FConexion, ZetaDialogo, ZetaWinAPITools;

{$R *.dfm}

{ TServerCafeUI }

procedure TFormaCafeteraServicioUI.actConfigServidorExecute(Sender: TObject);
begin
  if not ServerCafeUI.ServerRunning then
    ZInformation('Configuraci�n de servidor', 'No est� conectado a un Servidor.', 0)
  else begin
    inherited;
    ServerCafeUI.Init
  end;
end;

procedure TFormaCafeteraServicioUI.Init;
begin
  Application.CreateForm(TServerCafeUI, ServerCafeUI);
  with ServerCafeUI do begin
    OnClientConnect      := SeHaConectado;
    OnClientDisConnect   := SeHaConectado;
    OnConnectTimer       := TimerTimer;
    Init;
  end;
end;

procedure TFormaCafeteraServicioUI.SeHaConectado(Sender: TObject);
var
   iConexion : Integer;
begin
  iConexion := K_CAFETERA_FUERA_LINEA_TEXT;
  if ServerCafeUI.Active then
     iConexion := K_CAFETERA_EN_LINEA_TEXT;

  StatusBar.Panels[1].Text := Format('Puerto %d', [ServerCafeUI.Puerto]);
  StatusBar.Panels[2].Text := Format('%s:%s (%s)', [ServerCafeUI.Direccion, ServerCafeUI.Instance, ESTADO_CONEXION[iConexion]]);
  if ServerCafeUI.Instance = '' then
    // Bitacora.Caption := 'Bit�cora'
    Bitacora_DevEx.Caption := 'Bit�cora'
  else
    // Bitacora.Caption := 'Bit�cora de ' + ServerCafeUI.Instance;
    Bitacora_DevEx.Caption := 'Bit�cora de ' + ServerCafeUI.Instance;
  actConfigServidor.Enabled := ServerCafeUI.ServerRunning;
end;

procedure TFormaCafeteraServicioUI.TimerTimer(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  // Pedir el estado del servicio periodicamente
  with ServerCafeUI do begin
    Params  := TParams.Create(Self);

    // Enviar peticion al servidor SOAP
    SOAPMsg := CreateSOAPMsg(K_OBTENER_LOG);
    if SendSOAPMsg(SOAPMsg, Params) then begin
      // Errores.Lines.Text := Params.ParamByName('LOG').AsString;
      Errores_DevEx.Lines.Text := Params.ParamByName('LOG').AsString;
      lblUltima.Caption  := '�ltima actualizaci�n: ' + FormatDateTime('ddd dd/mmm/yyyy hh:nn:ss', Now);
    end else
    FreeAndNil(SOAPMsg);
    FreeAndNil(Params);
  end;
  // SendMessage(Errores.Handle, EM_LINESCROLL, 0, Errores.Lines.Count);
  // SendMessage(Errores_DevEx.Handle, EM_LINESCROLL, 0, Errores_DevEx.Lines.Count);
  Errores_DevEx.InnerControl.Perform(EM_LINESCROLL, 0,  Errores_DevEx.Lines.Count);
  Screen.Cursor := crDefault;
end;

procedure TFormaCafeteraServicioUI.actActualizarExecute(Sender: TObject);
begin
  inherited;
  ServerCafeUI.Update;
end;

procedure TFormaCafeteraServicioUI.actConfigConexionExecute(Sender: TObject);
begin
  if TConexion.Execute then begin
    ;
  end;
end;

end.
