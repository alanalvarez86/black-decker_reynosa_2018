@echo off

echo 32 bits
xcopy "%~dp0Win32\CafeteraServicioUI.exe" "C:\Proyectos\Version\Proyectos Integrados\Version 2014\64bits\Entregables\Programas\CafeteraServicio\Win32\" /I /Y
echo.
echo 64 bits
xcopy "%~dp0Win64\CafeteraServicioUI.exe" "C:\Proyectos\Version\Proyectos Integrados\Version 2014\64bits\Entregables\Programas\CafeteraServicio\Win64\" /I /Y
echo.
pause