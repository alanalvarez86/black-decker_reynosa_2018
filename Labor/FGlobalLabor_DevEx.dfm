inherited GlobalLabor: TGlobalLabor
  Left = 301
  Top = 184
  Caption = 'Datos Globales'
  ClientHeight = 431
  ClientWidth = 385
  ExplicitWidth = 391
  ExplicitHeight = 459
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 395
    Width = 385
    ParentFont = False
    TabOrder = 1
    ExplicitTop = 395
    ExplicitWidth = 385
    inherited OK_DevEx: TcxButton
      Left = 210
      ParentFont = False
      ExplicitLeft = 210
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 290
      ExplicitLeft = 290
    end
  end
  object PageControl: TcxPageControl [1]
    Left = 0
    Top = 0
    Width = 385
    Height = 395
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Properties.ActivePage = Catalogo
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 393
    ClientRectLeft = 2
    ClientRectRight = 383
    ClientRectTop = 27
    object Generales: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 1
      object gbTextos: TGroupBox
        Left = 0
        Top = 196
        Width = 381
        Height = 69
        Align = alTop
        Caption = ' Textos '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        object TiempoLbl: TLabel
          Left = 33
          Top = 20
          Width = 86
          Height = 13
          Alignment = taRightJustify
          Caption = 'Unidad de &tiempo:'
          FocusControl = Tiempo
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object PiezasLbl: TLabel
          Left = 85
          Top = 44
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = '&Piezas:'
          FocusControl = Piezas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Tiempo: TEdit
          Tag = 25
          Left = 123
          Top = 16
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 0
        end
        object Piezas: TEdit
          Tag = 25
          Left = 123
          Top = 40
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 1
        end
      end
      object gbOperaciones: TGroupBox
        Left = 0
        Top = 46
        Width = 381
        Height = 50
        Align = alTop
        Caption = ' Operaciones '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object DefSteps: TcxCheckBox
          Left = 13
          Top = 20
          Caption = #191'Usar pasos (operaciones) p&redefinidos?'
          ParentBackground = False
          ParentColor = False
          Style.Color = clWindow
          TabOrder = 0
          Width = 225
        end
      end
      object gbOrdenTrabajo: TGroupBox
        Left = 0
        Top = 96
        Width = 381
        Height = 50
        Align = alTop
        Caption = ' Orden de trabajo '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object ValidaWOrder: TcxCheckBox
          Left = 13
          Top = 19
          Caption = #191'Se requiere &validaci'#243'n al dar de alta los pasos?'
          ParentBackground = False
          ParentColor = False
          Style.Color = clWindow
          TabOrder = 0
          Width = 285
        end
      end
      object GBNoRegistrado: TGroupBox
        Left = 0
        Top = 265
        Width = 381
        Height = 101
        Align = alClient
        Caption = ' %s - &No. registrado '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        object TMuertoNR: TZetaKeyLookup_DevEx
          Left = 13
          Top = 20
          Width = 329
          Height = 21
          LookupDataset = dmLabor.cdsTiempoMuerto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
        end
      end
      object gbMultilotes: TGroupBox
        Left = 0
        Top = 146
        Width = 381
        Height = 50
        Align = alTop
        Caption = ' Multilotes '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        object Multilotes: TcxCheckBox
          Left = 13
          Top = 20
          Caption = #191'Trabaja con &m'#250'ltiples Lotes?'
          ParentBackground = False
          ParentColor = False
          Style.Color = clWindow
          TabOrder = 0
          Width = 257
        end
      end
      object PanelFechaCorte: TPanel
        Left = 0
        Top = 0
        Width = 381
        Height = 46
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object gbFechaCorte: TGroupBox
          Left = 0
          Top = 0
          Width = 381
          Height = 46
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object FechaCorteLBL: TLabel
            Left = 30
            Top = 22
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Caption = '&Fecha:'
            Enabled = False
          end
          object FechaCorte: TZetaFecha
            Left = 65
            Top = 19
            Width = 115
            Height = 22
            Cursor = crArrow
            Enabled = False
            TabOrder = 1
            Text = '30-May-03'
            Valor = 37771.000000000000000000
          end
          object ckFechaCorte: TcxCheckBox
            Left = 13
            Top = 2
            Caption = #191'&Usar fecha de corte para cambios?'
            ParentBackground = False
            ParentColor = False
            Style.Color = clWindow
            TabOrder = 0
            OnClick = ckFechaCorteClick
            Width = 208
          end
        end
      end
    end
    object Catalogo: TcxTabSheet
      Caption = 'Cat'#225'logos'
      object GroupBox4: TGroupBox
        Left = 0
        Top = 241
        Width = 381
        Height = 125
        Align = alClient
        Caption = ' Nombres de moduladores '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Modula1Lbl: TLabel
          Left = 28
          Top = 20
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modulador #&1:'
          FocusControl = Modula1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Modula2Lbl: TLabel
          Left = 28
          Top = 44
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modulador #&2:'
          FocusControl = Modula2
        end
        object Modula3Lbl: TLabel
          Left = 28
          Top = 68
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modulador #&3:'
          FocusControl = Modula3
        end
        object Modula1: TEdit
          Tag = 27
          Left = 101
          Top = 16
          Width = 268
          Height = 21
          MaxLength = 25
          TabOrder = 0
        end
        object Modula2: TEdit
          Tag = 28
          Left = 101
          Top = 40
          Width = 268
          Height = 21
          MaxLength = 25
          TabOrder = 1
        end
        object Modula3: TEdit
          Tag = 28
          Left = 101
          Top = 64
          Width = 268
          Height = 21
          MaxLength = 25
          TabOrder = 2
        end
      end
      object Textos: TGroupBox
        Left = 0
        Top = 0
        Width = 381
        Height = 241
        Align = alTop
        Caption = ' Nombres de Cat'#225'logos '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object WOrderLbl: TLabel
          Left = 15
          Top = 41
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'Orden de &trabajo:'
          FocusControl = WOrder
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object ParteLbl: TLabel
          Left = 46
          Top = 65
          Width = 51
          Height = 13
          Alignment = taRightJustify
          Caption = '&Productos:'
          FocusControl = Parte
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object OperacionLbl: TLabel
          Left = 34
          Top = 89
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Op&eraciones:'
          FocusControl = Operacion
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object AreaLbl: TLabel
          Left = 67
          Top = 113
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = '&'#193'reas:'
          FocusControl = Area
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object TMuertoLbl: TLabel
          Left = 24
          Top = 206
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tiempo &muerto:'
          FocusControl = TMuerto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 101
          Top = 19
          Width = 47
          Height = 13
          Caption = 'Singular'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 237
          Top = 19
          Width = 33
          Height = 13
          Caption = 'Plural'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object TDefectoLbl: TLabel
          Left = 56
          Top = 137
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = '&Defecto:'
          FocusControl = TDefecto
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 48
          Top = 161
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#225'&quinas:'
          FocusControl = TMaquina
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 24
          Top = 184
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'P&lantillas prod.:'
          FocusControl = TPlantilla
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object WOrder: TEdit
          Tag = 25
          Left = 101
          Top = 37
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 0
          OnChange = ControlEditChange
        end
        object WOrders: TEdit
          Tag = 25
          Left = 237
          Top = 37
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 1
        end
        object Parte: TEdit
          Tag = 25
          Left = 101
          Top = 61
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 2
          OnChange = ControlEditChange
        end
        object Partes: TEdit
          Tag = 25
          Left = 237
          Top = 61
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 3
        end
        object Operacion: TEdit
          Tag = 25
          Left = 101
          Top = 85
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 4
          OnChange = ControlEditChange
        end
        object Operaciones: TEdit
          Tag = 25
          Left = 237
          Top = 85
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 5
        end
        object Area: TEdit
          Tag = 25
          Left = 101
          Top = 109
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 6
          OnChange = ControlEditChange
        end
        object Areas: TEdit
          Tag = 25
          Left = 237
          Top = 109
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 7
        end
        object TMuerto: TEdit
          Tag = 27
          Left = 101
          Top = 204
          Width = 268
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 25
          ParentFont = False
          TabOrder = 14
          OnExit = TMuertoExit
        end
        object TDefecto: TEdit
          Tag = 27
          Left = 101
          Top = 133
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 8
          OnChange = ControlEditChange
        end
        object TMaquina: TEdit
          Tag = 27
          Left = 101
          Top = 157
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 10
          OnChange = ControlEditChange
        end
        object TMaquinas: TEdit
          Tag = 27
          Left = 237
          Top = 157
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 11
        end
        object TDefectos: TEdit
          Tag = 27
          Left = 237
          Top = 133
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 9
          OnChange = ControlEditChange
        end
        object TPlantilla: TEdit
          Tag = 27
          Left = 101
          Top = 181
          Width = 133
          Height = 21
          MaxLength = 20
          TabOrder = 12
          OnChange = ControlEditChange
        end
        object TPlantillas: TEdit
          Tag = 27
          Left = 237
          Top = 181
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 13
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
end
