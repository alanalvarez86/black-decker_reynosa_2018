unit FSistGlobales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Db, ExtCtrls, ImgList, ComCtrls, ToolWin,
     ZBaseConsulta,
     ZetaDBTextBox;

type
  TSistGlobales = class(TBaseConsulta)
    Panel1: TPanel;
    Label1: TLabel;
    RazonLbl: TZetaDBTextBox;
    ImageList2: TImageList;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    WORDER: TZetaDBTextBox;
    OPERACION: TZetaDBTextBox;
    AREAS: TZetaDBTextBox;
    TMUERTO: TZetaDBTextBox;
    PARTE: TZetaDBTextBox;
    Label4: TLabel;
    TDefecto: TZetaDBTextBox;
    Label5: TLabel;
    Maquinas: TZetaDBTextBox;
    Label9: TLabel;
    Plantillas: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetGlobales;
  protected
    { Protected declarations }
    procedure Modificar; override;
    procedure Refresh; override;
    procedure Connect; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistGlobales: TSistGlobales;

implementation

uses DGlobal,
     ZetaCommonClasses,
     ZGlobalTress,
     FGlobalLabor,
     FTressShell;

{$R *.DFM}

procedure TSistGlobales.FormCreate(Sender: TObject);
begin
     inherited;
     SetGlobales;
     HelpContext := H9516_Catalogo_Globales;
end;

procedure TSistGlobales.SetGlobales;
begin
     with Global do
     begin
          RazonLbl.Caption := GetGlobalString( K_GLOBAL_RAZON_EMPRESA );
          WORDER.Caption := GetGlobalString( K_GLOBAL_LABOR_ORDEN );
          OPERACION.Caption := GetGlobalString( K_GLOBAL_LABOR_OPERACION );
          AREAS.Caption := GetGlobalString( K_GLOBAL_LABOR_AREA );
          TMUERTO.Caption := GetGlobalString( K_GLOBAL_LABOR_TMUERTO );
          PARTE.Caption := GetGlobalString( K_GLOBAL_LABOR_PARTE );
          TDEFECTO.Caption := GetGlobalString( K_GLOBAL_LABOR_TDEFECTO );
          Maquinas.Caption := GetGlobalString( K_GLOBAL_LABOR_MAQUINAS );
          Plantillas.Caption := GetGlobalString( K_GLOBAL_LABOR_PLANTILLAS );
     end;
end;

procedure TSistGlobales.Connect;
begin
     //No quitar, porque si no marca un error: 'Abstract Error'
end;

procedure TSistGlobales.Refresh;
begin
     //No quitar, porque si no marca un error: 'Abstract Error'
end;

procedure TSistGlobales.Modificar;
begin
     with TGlobalLabor.Create( Application ) do
     begin
          try
             ShowModal;
             if ( LastAction = K_EDICION_MODIFICACION ) then
             begin
                  SetGlobales;
                  with TressShell do
                  begin
                       SetCedulaInspeccionEnabled;
                       SetAfectarLaborEnabled;
                       CreaArbol( False );
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;

function TSistGlobales.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar Datos de ' + Caption;
end;

function TSistGlobales.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar Datos de ' + Caption;
end;

function TSistGlobales.PuedeModificar(var sMensaje: String): Boolean;
begin
     { GA: Para que el permiso de MOdificar se resuelva en la forma de edici�n }
     Result := True;
end;

end.
