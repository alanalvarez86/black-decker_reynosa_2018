unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     DBaseTressDiccionario,
     DBaseDiccionario,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaTipoEntidad ;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetLookupNames; override;
    procedure GetListaClasifiModulo(oLista: TStrings; const lMigracion: Boolean);override;
  end;

var
  dmDiccionario: TdmDiccionario;

implementation

uses dLabor, dTablas;

{$R *.DFM}

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     dmLabor.SetLookupNames;
     dmTablas.SetLookupNames;
end;


procedure TdmDiccionario.GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean );
begin
     inherited GetListaClasifiModulo( oLista, lMigracion );
     {$ifdef RDD}
     {$else}
     AgregaClasifi( oLista, crLabor );
     {$endif}
end;

end.
