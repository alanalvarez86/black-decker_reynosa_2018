unit FWizLaborImportarOrdenes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardImportar, ComCtrls, StdCtrls, ZetaKeyCombo, Buttons,
  ZetaWizard, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxButtons;

type
  TWizLaborImportarOrdenes = class(TTBaseWizardImportar)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizLaborImportarOrdenes: TWizLaborImportarOrdenes;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     DB;

{$R *.DFM}

procedure TWizLaborImportarOrdenes.FormCreate(Sender: TObject);
begin

     Programa.Tag := K_GLOBAL_DEF_LAB_IMP_ORD_PROGRAMA;
     Comandos.Tag := K_GLOBAL_DEF_LAB_IMP_ORD_PARAMETROS;
     Archivo.Tag := K_GLOBAL_DEF_LAB_IMP_ORD_ARCHIVO;
     HelpContext := H00006_Importar_ordenes;
     NombreCatalogo := Global.GetGlobalString( K_GLOBAL_LABOR_ORDENES );
     Parametros.Header.Title:='Importar '+ NombreCatalogo;
     inherited;
end;

function TWizLaborImportarOrdenes.EjecutarWizard: Boolean;
begin
     Result := dmLabor.ImportarOrdenes( ParameterList );
end;

end.
