unit FWizLaborImportarCedulas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardImportar, ComCtrls, StdCtrls, ZetaKeyCombo, Buttons,
  ZetaWizard, ExtCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, TressMorado2013, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxButtons;

type
  TWizLaborImportarCedulas = class(TTBaseWizardImportar)
    cbFFecha: TZetaKeyCombo;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizLaborImportarCedulas: TWizLaborImportarCedulas;

implementation

uses DLabor,
     ZGlobalTress,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

const
     K_TITULO_CEDULAS = 'c�dulas de captura';

procedure TWizLaborImportarCedulas.FormCreate(Sender: TObject);
begin
     Programa.Tag := K_GLOBAL_DEF_LAB_IMP_CED_PROGRAMA;
     Comandos.Tag := K_GLOBAL_DEF_LAB_IMP_CED_PARAMETROS;
     Archivo.Tag := K_GLOBAL_DEF_LAB_IMP_CED_ARCHIVO;
     cbFFecha.ItemIndex := Ord( ifDDMMYYYYs );
     NombreCatalogo := K_TITULO_CEDULAS;  // PENDIENTE: � Es correcto que en Advertencia.lines diga Catalogo de Cedulas de Captura ?
     HelpContext := H00010_Importacion_de_Cedulas_de_Produccion;
     inherited;
end;

function TWizLaborImportarCedulas.EjecutarWizard: Boolean;
begin
     Result := dmLabor.ImportarCedulas( ParameterList );
end;

procedure TWizLaborImportarCedulas.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
         AddInteger( 'FormatoImpFecha', cbFFecha.Valor );
     end;
     with Descripciones do
     begin
         AddString( 'Formato fechas', cbFFecha.Text );
     end;
end;

end.
