unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcEmpDatos,
                     efcEmpWorks,
                     efcEmpLecturas,
                     efcEmpWOFijas,
                     efcUnaOrden,
                     efcHistoriaOrden,
                     efcLabRegistro,
                     efcLabProceso,
                     efcPartes,
                     efcOperaciones,
                     efcTipoParte,
                     efcTipoOpera,
                     efcModula1,
                     efcModula2,
                     efcModula3,
                     efcTiempoMuerto,
                     efcArea,
                     efcKardexOrden,
                     efcReportes,
                     efcMultiLote,
                     efcSistProcesos,
                     efcSistGlobales,
                     efcCedulas,
                     efcBreaks,
                     efcKardexArea,
                     efcQueryGral,
                     efcTDefectos,
                     efcCedulasInspec,
                     efcCedulasScrap,
                     efcComponentes,
                     efcMotivosScrap,
                     efcMaquinas,
                     efcTMaquinas,
                     efcPlantillas,
                     efcKarMaqEmpleado,
                     efcKarEmpleadoMaq,
                     efcSistBitacora );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FTablas,
     FPartes,
     FOperaciones,
     FTParte,
     FTOpera,
     FRegLabor,
     FProcLabor,
     FConsultaEmpleados,
     FWorks,
     FLecturas,
     FWoFija,
     FWOrder,
     FHistorialWOrder,
     FKardexWOrder,
     FReportes_DevEx,
     FQueryGral_DevEx,
     FEditMultiLotes,
     FSistProcesos_DevEx,
     FSistGlobales,
     FCedulas,
     FBreaks,
     FKardexArea,
     FCedInspeccion,
     FCedScrap,
     FComponentes,
     FLayouts,
     FMaquinas;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
    // Result := Consulta( 0, nil );
     case Forma of
       efcModula1 : Result := Consulta( D_LAB_CAT_MODULA_1, TTOModula1 );
       efcModula2 : Result := Consulta( D_LAB_CAT_MODULA_2, TTOModula2 );
       efcModula3 : Result := Consulta( D_LAB_CAT_MODULA_3, TTOModula3 );
       efcTiempoMuerto : Result := Consulta( D_LAB_CAT_TIEMPO_MUERTO, TTOTiempoMuerto );
       efcPartes : Result := Consulta( D_LAB_CAT_PARTES, TPartes );
       efcOperaciones : Result := Consulta( D_LAB_CAT_OPERACIONES, TOperaciones );
       efcTipoParte : Result := Consulta( D_LAB_CAT_TIPO_PARTE, TTParte );
       efcTipoOpera : Result := Consulta( D_LAB_CAT_TIPO_OPERACION, TTOpera );
       efcArea : Result := Consulta( D_LAB_CAT_AREA, TArea );
  //:Todo     efcLabRegistro : Result := Consulta( D_LAB_REGISTRO, TRegLabor );
  //:Todo     efcLabProceso : Result := Consulta( D_LAB_PROCESO, TProcLabor );
       efcEmpDatos : Result := Consulta( D_LAB_EMP_DATOS, TConsultaEmpleados );
       efcEmpWorks : Result := Consulta( D_LAB_EMP_OPERACIONES, TWorks );
       efcEmpLecturas : Result := Consulta( K_SIN_RESTRICCION, TLecturas );
       efcEmpWOFijas : Result := Consulta( D_LAB_EMP_ORDENES_FIJAS, TWoFija );
       efcUnaOrden : Result := Consulta( D_LAB_WORK_ORDER, TWOrder );
       efcHistoriaOrden : Result := Consulta( D_LAB_HISTORIA_ORDENES, THistorialWOrder );
       efcKardexOrden  : Result := Consulta( D_LAB_KARDEX_ORDER, TKardexWOrder );
       efcReportes : Result := Consulta( D_REPORTES_LABOR, TReportes_DevEx );
       efcQueryGral : Result := Consulta( D_LAB_CONS_SQL, TQueryGral_DevEx );
       efcSistProcesos : Result := Consulta( D_LAB_CONS_PROCESOS, TSistProcesos_DevEx );
       efcSistGlobales : Result := Consulta( D_LAB_CAT_GLOBAL, TSistGlobales );
       efcCedulas : Result := Consulta( D_LAB_CEDULAS, TCedulas );
       efcBreaks: Result := Consulta( D_LAB_CAT_BREAKS, TBreaks );
       efcKardexArea: Result := Consulta( D_LAB_EMP_KARDEX_AREAS, TKardexArea );
       efcTDefectos: Result := Consulta( D_LAB_CAT_TDEFECTO, TTOTDefecto );
       efcCedulasInspec: Result := Consulta( D_LAB_CEDULAS_INSP, TFCedulaInspeccion );
       efcCedulasScrap: Result := Consulta( D_LAB_CEDULAS_SCRAP, TCedulaScrap );
       efcComponentes: Result := Consulta( D_LAB_CAT_COMPONENTES, TComponentes );
       efcMotivosScrap: Result := Consulta( D_LAB_CAT_MOT_SCRAP, TTOMotivoScrap );
       efcMaquinas: Result := Consulta( D_LAB_CAT_MAQUINAS, TMaquinas );
       efcTMaquinas: Result := Consulta( D_LAB_CAT_TMAQUINAS, TTMaquinas );
       efcPlantillas: Result := Consulta( D_LAB_PLANTILLAS, TLayouts );
     else
         Result := Consulta( 0, nil );
     end;
end;

end.
