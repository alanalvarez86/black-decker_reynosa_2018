object BuscaWorder_DevEx: TBuscaWorder_DevEx
  Left = 248
  Top = 161
  VertScrollBar.Visible = False
  ActiveControl = Parte.FLlave
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B'#250'squeda de Empleados'
  ClientHeight = 327
  ClientWidth = 611
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 611
    Height = 177
    Align = alTop
    TabOrder = 0
    object Status: TRadioGroup
      Left = 12
      Top = 11
      Width = 478
      Height = 32
      Caption = 'Buscar por Status'
      Columns = 3
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Completo'
        'Incompleto')
      ParentFont = False
      TabOrder = 0
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 52
      Width = 589
      Height = 113
      Caption = 'Aplicar m'#225's Opciones de B'#250'squeda'
      TabOrder = 1
      object lbInicio: TLabel
        Left = 124
        Top = 20
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = '&Inicio del:'
        Enabled = False
        FocusControl = FechaInicial
      end
      object lbFin: TLabel
        Left = 288
        Top = 20
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = 'Al:'
        Enabled = False
        FocusControl = FechaInicial
      end
      object Label1: TLabel
        Left = 112
        Top = 54
        Width = 57
        Height = 13
        Caption = 'N'#250'mero del:'
      end
      object Label5: TLabel
        Left = 288
        Top = 54
        Width = 12
        Height = 13
        Caption = 'Al:'
      end
      object lbParte: TLabel
        Left = 142
        Top = 76
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Parte:'
        FocusControl = Parte
      end
      object CBFecha: TCheckBox
        Left = 12
        Top = 18
        Width = 108
        Height = 17
        Caption = 'Filtrar por Fechas'
        TabOrder = 0
        OnClick = CBFechaClick
      end
      object FechaInicial: TZetaFecha
        Left = 172
        Top = 17
        Width = 115
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 1
        Text = '02/ene./98'
        Valor = 35797.000000000000000000
      end
      object FechaFinal: TZetaFecha
        Left = 302
        Top = 17
        Width = 115
        Height = 22
        Cursor = crArrow
        Enabled = False
        TabOrder = 2
        Text = '02/ene./98'
        Valor = 35797.000000000000000000
      end
      object WO_NUMBER_INI: TZetaEdit
        Left = 172
        Top = 48
        Width = 115
        Height = 21
        TabOrder = 3
      end
      object WO_NUMBER_FIN: TZetaEdit
        Left = 302
        Top = 48
        Width = 115
        Height = 21
        TabOrder = 4
      end
      object Parte: TZetaKeyLookup_DevEx
        Left = 172
        Top = 76
        Width = 397
        Height = 21
        LookupDataset = dmLabor.cdsPartes
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 100
      end
    end
    object BuscarBtn_DevEx: TcxButton
      Left = 498
      Top = 16
      Width = 26
      Height = 26
      Hint = 'Buscar por los Par'#225'metros Establecidos'
      Caption = '&Buscar'
      Default = True
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000000000000000000000000000000000008B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFCECA
        CEFF8D838EFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFB5AEB5FFFFFFFFFFFFFF
        FFFFE4E1E4FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF9F97A0FFFAF9FAFFFFFFFFFFFFFF
        FFFFD2CED2FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF908791FFEFEDEFFFFFFFFFFFFFFFFFFFE9E7
        E9FF8F8590FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFDBD7DBFFFFFFFFFFFFFFFFFFF8F7F8FF9A91
        9AFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFD9D5D9FFFDFD
        FDFFFFFFFFFFF4F3F4FFE4E1E4FFFFFFFFFFFFFFFFFFFFFFFFFFACA5ACFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFB1ABB2FFFDFDFDFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7C2C7FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF9B939CFFFDFDFDFFFFFFFFFFEDEBEDFFB5AE
        B5FFA8A1A9FFBEB8BFFFF4F3F4FFFFFFFFFFFFFFFFFF988F99FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFD3D0D4FFFFFFFFFFEBE9EBFF908791FF8B81
        8CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFCAC6CBFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFF0EFF1FFFFFFFFFFB3ACB4FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFC3BEC4FFFFFFFFFFE7E5E8FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFFFFFFFFFFFFFFFFFA69FA7FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFB7B0B7FFFFFFFFFFF0EFF1FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFEFEDEFFFFFFFFFFFB8B2B9FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFE4E1E4FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFF4F3F4FF968D97FF8B81
        8CFF8B818CFF8B818CFF9F97A0FFFBFBFBFFFFFFFFFFC2BCC2FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF988F99FFF4F3F4FFFFFFFFFFF4F3F4FFC2BC
        C2FFB7B0B7FFC7C2C7FFFAF9FAFFFFFFFFFFF2F1F2FF8F8590FF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFFA49DA5FFF8F7F8FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFEBE9EBFF988F99FF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF928993FFC5C0C6FFE2DF
        E2FFF0EFF1FFE0DDE0FFBCB6BDFF908791FF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
        8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
      PaintStyle = bpsGlyph
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BuscarBtn_DevExClick
    end
    object AceptarBtn_DevEx: TcxButton
      Left = 530
      Top = 16
      Width = 73
      Height = 26
      Hint = 'Aceptar y Escribir Cambios'
      Caption = '&Aceptar'
      Enabled = False
      ModalResult = 1
      OptionsImage.Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        200000000000000900000000000000000000000000000000000050D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8
        A1FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF8CE2B8FFE9F9
        F1FF55D396FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFF4FCF8FFFFFF
        FFFFADEACCFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF53D395FFD6F4E6FFFFFFFFFFFFFF
        FFFFFCFEFDFF71DAA7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFD9F5E7FF53D395FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF79DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF9AE5C1FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF5BD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
        D6FFFFFFFFFFFFFFFFFFF4FCF8FF63D79FFF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF50D293FF55D3
        96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF8AE1B7FFE6F9F0FF6BD9A4FF50D293FF50D293FF50D2
        93FF60D69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF53D395FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF66D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF69D8A2FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF6BD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF53D395FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF79DDACFFF7FDFAFFFFFFFFFFB8EDD3FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF71DAA7FFEFFBF5FFFFFFFFFFA0E6
        C4FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF66D8A1FFE1F7ECFFFFFF
        FFFF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF58D498FFBDEE
        D6FFFCFEFDFF8AE1B7FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF81DFB1FF8FE2BAFF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF50D2
        93FF50D293FF50D293FF50D293FF50D293FF50D293FF50D293FF}
      OptionsImage.Margin = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = AceptarBtn_DevExClick
    end
  end
  object ZetaDBGrid: TZetaCXGrid
    Left = 0
    Top = 177
    Width = 611
    Height = 150
    Align = alClient
    TabOrder = 1
    object ZetaDBGridDBTableView: TcxGridDBTableView
      OnDblClick = ZetaDBGridDBTableViewDblClick
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      DataController.DataSource = DataSource
      DataController.Filter.OnGetValueList = ZetaDBGridDBTableViewDataControllerFilterGetValueList
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnSortingChanged = ZetaDBGridDBTableViewDataControllerSortingChanged
      Filtering.ColumnFilteredItemsList = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OnColumnHeaderClick = ZetaDBGridDBTableViewColumnHeaderClick
      object WO_NUMBER: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'WO_NUMBER'
      end
      object WO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'WO_DESCRIP'
      end
      object WO_FEC_INI: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'WO_FEC_INI'
      end
      object WO_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'WO_STATUS'
      end
      object AR_NOMBRE: TcxGridDBColumn
        Caption = 'Parte'
        DataBinding.FieldName = 'AR_NOMBRE'
      end
    end
    object ZetaDBGridLevel: TcxGridLevel
      GridView = ZetaDBGridDBTableView
    end
  end
  object DataSource: TDataSource
    Left = 149
    Top = 215
  end
end
