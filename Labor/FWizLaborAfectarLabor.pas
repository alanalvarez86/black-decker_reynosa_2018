unit FWizLaborAfectarLabor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZcxBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaKeyCombo, ZetaNumero, Mask,
  ZetaFecha, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizLaborAfectarLabor = class(TcxBaseWizard)
    gbDatosPeriodo: TGroupBox;
    PeriodoLBL: TLabel;
    TipoLBL: TLabel;
    AnioLBL: TLabel;
    Numero: TZetaNumero;
    Tipo: TZetaKeyCombo;
    Year: TZetaNumero;
    FechaCorteLBL: TLabel;
    FechaCorte: TZetaFecha;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
   WizLaborAfectarLabor: TWizLaborAfectarLabor;

implementation

uses
    DCliente,
    DGlobal,
    DLabor,
    ZGlobalTress,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaRegistryCliente;

{$R *.DFM}

{ TWizLaborAfectarLabor }

procedure TWizLaborAfectarLabor.FormCreate(Sender: TObject);
begin
     inherited;
     Tipo.ItemIndex := ClientRegistry.TipoNomina;
     with dmCliente do
     begin
          Year.Valor := YearDefault;
          if GetPeriodoInicial( Year.ValorEntero, eTipoPeriodo(Tipo.ItemIndex)) then
             Numero.Valor   := cdsPeriodo.FieldByName( 'PE_NUMERO' ).AsInteger
          else
              Numero.Valor  := 1;
     end;
     FechaCorte.Tag := K_GLOBAL_LABOR_FECHACORTE;
     HelpContext := H00054_Afectar_labor;
     { MV: 24/Junio/2003
       Cuando se termine el funcionamiento de este Wizard se Regresaran
       los controles a la posici�n que se tienen en Dise�o y el GroupBox se pone visible }
     gbDatosPeriodo.Visible := False;
     FechaCorte.Top := 103;
     FechaCorteLBL.Top := 108;
end;

procedure TWizLaborAfectarLabor.FormShow(Sender: TObject);
begin
     inherited;
     with Global do
     begin
          if GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE ) then
             FechaCorte.Valor := GetGlobalDate( K_GLOBAL_LABOR_FECHACORTE )
          else
              FechaCorte.Valor := Now;
     end;
     FechaCorte.SetFocus;
end;

procedure TWizLaborAfectarLabor.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddDate( 'FechaCorte', FechaCorte.Valor );
          AddInteger( 'Year', Year.ValorEntero );
          AddInteger( 'Tipo', Tipo.Valor );
          AddInteger( 'Numero', Numero.ValorEntero );
     end;
     with Descripciones do
     begin
          AddDate( 'Fecha de corte', FechaCorte.Valor );
          {AddInteger( 'Year', Year.ValorEntero );
          AddInteger( 'Tipo', Tipo.Valor );
          AddInteger( 'Numero', Numero.ValorEntero );}
     end;
end;

procedure TWizLaborAfectarLabor.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if ( Year.ValorEntero = 0 ) then
                       CanMove := Error( '� No se Puede Dejar el A�o en Cero !', Year )
                    else if ( Numero.ValorEntero = 0 ) then
                            CanMove := Error( '� No se Puede Dejar el N�mero de Periodo en Cero !', Numero );
               end;
          end;
     end;
end;

function TWizLaborAfectarLabor.EjecutarWizard: Boolean;
begin
     Result := dmLabor.AfectarLabor( ParameterList );
end;

end.
