inherited KardexEmpMaquina: TKardexEmpMaquina
  Left = 243
  Top = 315
  Caption = 'Kadex empleados maquinas'
  ClientWidth = 736
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 736
    inherited ValorActivo2: TPanel
      Width = 477
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 736
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EM_FECHA'
        Title.Caption = 'Fecha'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EM_HORA'
        Title.Caption = 'Hora'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LY_CODIGO'
        Title.Caption = 'Layout'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LY_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 178
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MQ_CODIGO'
        Title.Caption = 'M'#225'quina'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MQ_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 181
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'Modific'#243
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 496
    Top = 64
  end
end
