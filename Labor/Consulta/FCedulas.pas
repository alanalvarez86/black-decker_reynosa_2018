unit FCedulas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, Grids, DBGrids, StdCtrls, Mask, Buttons,
     ZBaseGridLectura_DevEx,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaFecha,
     ZetaCommonClasses,
     ZetaCommonLists, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxGroupBox, cxRadioGroup, cxCheckBox;

type
  TCedulas = class(TBaseGridLectura_DevEx)
    PanelControles: TPanel;
    lbParte: TLabel;
    AR_CODIGO: TZetaKeyLookup_DevEx;
    Tipo: TcxRadioGroup;
    lbOpera: TLabel;
    OP_NUMBER: TZetaKeyLookup_DevEx;
    lbWorder: TLabel;
    WO_NUMBER: TZetaKeyLookup_DevEx;
    lbArea: TLabel;
    CE_AREA: TZetaKeyLookup_DevEx;
    Refrescar: TcxButton;
    CBFecha: TcxCheckBox;
    Filtros: TcxGroupBox;
    lbInicio: TLabel;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    FechaInicial: TZetaFecha;
    lbUsuario: TLabel;
    US_CODIGO: TZetaKeyLookup_DevEx;
    Status: TComboBox;
    Label2: TLabel;
    CE_FOLIO: TcxGridDBColumn;
    CE_FECHA: TcxGridDBColumn;
    CE_HORA: TcxGridDBColumn;
    CE_TIEMPO: TcxGridDBColumn;
    CE_TIPO: TcxGridDBColumn;
    VAR_CODIGO: TcxGridDBColumn;
    AR_NOMBRE: TcxGridDBColumn;
    CW_PIEZAS: TcxGridDBColumn;
    OP_NUMBER2: TcxGridDBColumn;
    CE_AREA2: TcxGridDBColumn;
    US_CODIGO2: TcxGridDBColumn;
    CE_TMUERTO: TcxGridDBColumn;
    CE_STATUS: TcxGridDBColumn;
    Modulo1: TcxGridDBColumn;
    Modulo2: TcxGridDBColumn;
    Modulo3: TcxGridDBColumn;
    CE_COMENTA: TcxGridDBColumn;
    WO_NAMBER2: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure CBFechaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    function GetTipoCedulaActivo: eTipoCedula;
    procedure AsignaColumnas;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect;override;
    procedure Refresh;override;
    procedure Modificar;override;
    procedure Borrar;override;
  public
    { Public declarations }
  end;

var
  Cedulas: TCedulas;

implementation

uses DLabor,
     DGlobal,
     DSistema,
     ZGlobalTress,
     ZetaLaborTools;

{$R *.DFM}

procedure TCedulas.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef HYUNDAI}
     FechaInicial.Valor := Date - 7;
     FechaFinal.Valor := Date;
     {$else}
     FechaInicial.Valor := Date;
     FechaFinal.Valor := Date;
     {$endif}
     FParametros := TZetaParams.Create;
     with dmLabor do
          if cdsCedulas.Active then
             cdsCedulas.Close;
     HelpContext := H00008_Cedulas_de_captura;

     AR_CODIGO.LookupDataset := dmLabor.cdsPartes;
     OP_NUMBER.LookupDataset := dmLabor.cdsOpera;
     WO_NUMBER.LookupDataset := dmLabor.cdsWOrderLookup;
     CE_AREA.LookupDataset := dmLabor.cdsArea;
     US_CODIGO.LookupDataset := dmSistema.cdsUsuarios;
     {$ifdef DOS_CAPAS}
     Grid.Columns[ Grid.GetFieldColumnIndex( 'VAR_CODIGO' ) ].FieldName := 'AR_CODIGO';
     {$endif}
end;

procedure TCedulas.FormDestroy(Sender: TObject);
begin
     inherited;
     FParametros.Free;
end;

procedure TCedulas.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     AsignaColumnas;
     {$ifdef HYUNDAI}
     CBFecha.checked := True;
     Status.ItemIndex := 1;
     {$endif}
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave := K_WIDTHLLAVE;
      US_CODIGO.WidthLlave := K_WIDTHLLAVE;
      CE_AREA.WidthLlave := K_WIDTHLLAVE;

      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
      US_CODIGO.Width := K_WIDTH_LOOKUP;
      CE_AREA.Width := K_WIDTH_LOOKUP;

      Refrescar.Left := K_LEFT_630;
     {$endif}

end;

procedure TCedulas.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmLabor do
     begin
          cdsPartes.Conectar;
          cdsOpera.Conectar;
          cdsArea.Conectar;
          cdsTiempoMuerto.Conectar;
          DataSource.DataSet := cdsCedulas;
     end;
end;

procedure TCedulas.Refresh;
var
   dInicio, dFin: TDate;
begin
     if CBFecha.Checked then
     begin
          dInicio := FechaInicial.Valor;
          dFin := FechaFinal.Valor;
     end
     else
     begin
          dInicio := NullDateTime;
          dFin := NullDateTime;
     end;

     with FParametros do
     begin
          AddInteger( 'Status', Status.ItemIndex - 1 );
          AddInteger( 'Tipo', Tipo.ItemIndex - 1 );
          AddString( 'Parte', AR_CODIGO.Llave );
          AddString( 'Operacion', OP_NUMBER.Llave );
          AddString( 'Orden', WO_NUMBER.Llave );
          AddString( 'Area', CE_AREA.Llave );
          AddString( 'Cedula', Vacio );
          AddDate( 'FechaInicial', dInicio );
          AddDate( 'FechaFinal', dFin );
          AddInteger( 'Usuario', US_CODIGO.Valor );
     end;
     dmLabor.RefrescarHistorialCedulas( FParametros );
end;

procedure TCedulas.RefrescarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ZetaDBGrid.SetFocus;
     DoBestFit;
end;

procedure TCedulas.CBFechaClick(Sender: TObject);
var
   lEnabled: Boolean;
begin
     inherited;
     lEnabled := CbFecha.Checked;
     lbInicio.Enabled := lEnabled;
     lbFin.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
end;

function TCedulas.GetTipoCedulaActivo: eTipoCedula;
begin
     if NoHayDatos then
        Result := tcOperacion       // Valor Default - No debe llegar a cumplirse esta condici�n por que no puede llegar aqu� si NoHayDatos
     else
        Result := eTipoCedula( DataSource.DataSet.FieldByName( 'CE_TIPO' ).AsInteger );
end;

function TCedulas.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No se pueden agregar datos a las ' + Caption;
end;

function TCedulas.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
        Result := ChecaTipoCedula( GetTipoCedulaActivo, ukModify, IndexDerechos, sMensaje );
end;

procedure TCedulas.Modificar;
begin
     dmLabor.cdsCedulas.Modificar;
end;

function TCedulas.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
        Result := ChecaTipoCedula( GetTipoCedulaActivo, ukDelete, IndexDerechos, sMensaje );
end;

procedure TCedulas.Borrar;
begin
     dmLabor.cdsCedulas.Borrar;
end;

procedure TCedulas.AsignaColumnas;
begin
     with ZetaDBGridDBTableView do
     begin
          AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
          AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
          AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CE_AREA );
          AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOpera, OP_NUMBER );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, ZetaDBGridDBTableView.Columns[5] );
          {$ifdef DOS_CAPAS}
//:todo          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, Columns[ GetColumnByFieldName( 'AR_CODIGO' ) ] );
          {$else}
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE,  ZetaDBGridDBTableView.Columns[6] );
          {$endif}
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PIEZAS, ZetaDBGridDBTableView.Columns[8] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, ZetaDBGridDBTableView.Columns[9] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, ZetaDBGridDBTableView.Columns[10] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_TMUERTO, ZetaDBGridDBTableView.Columns[12] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_MODULA1, ZetaDBGridDBTableView.Columns[14] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_MODULA2, ZetaDBGridDBTableView.Columns[15] );
          AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_MODULA3, ZetaDBGridDBTableView.Columns[16] );
     end;
end;

end.

