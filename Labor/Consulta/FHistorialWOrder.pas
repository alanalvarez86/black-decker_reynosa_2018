unit FHistorialWOrder;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, ExtCtrls, StdCtrls, Mask, DBCtrls, ComCtrls, Buttons,
     DLabor,
     ZBaseGridLectura_DevEx,
     ZetaCommonClasses,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaEdit, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, Vcl.Menus, ZetaKeyLookup_DevEx, cxButtons,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, ZetaCXGrid, cxContainer, cxCheckBox, cxGroupBox, cxRadioGroup;

type
  THistorialWOrder = class(TBaseGridLectura_DevEx)
    PanelFiltros: TPanel;
    lbParte: TLabel;
    Refrescar: TcxButton;
    Parte: TZetaKeyLookup_DevEx;
    BitacoraPG: TPageControl;
    lbInicio: TLabel;
    FechaInicial: TZetaFecha;
    lbFin: TLabel;
    FechaFinal: TZetaFecha;
    Label1: TLabel;
    Label5: TLabel;
    WO_NUMBER_INI: TZetaEdit;
    WO_NUMBER_FIN: TZetaEdit;
    Status: TcxRadioGroup;
    CBFecha: TcxCheckBox;
    WO_NUMBER: TcxGridDBColumn;
    WO_DESCRIP: TcxGridDBColumn;
    WO_FEC_INI: TcxGridDBColumn;
    WO_STATUS: TcxGridDBColumn;
    AR_CODIGO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
    procedure CBFechaClick(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
  public
    { Public declarations }
  end;

var
  HistorialWOrder: THistorialWOrder;

implementation

uses DGlobal,
     ZetaCommonLists,
     ZetaCommonTools,
     ZGlobalTress,
     ZAccesosTress,
     ZetaLaborTools;

{$R *.DFM}

procedure THistorialWOrder.FormCreate(Sender: TObject);
begin
     inherited;
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

     Caption := 'Historial de ' + Global.GetGlobalString(K_GLOBAL_LABOR_ORDENES);
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, Parte );
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE,ZetaDBGridDBTableView.Columns[4]);
     HelpContext := H9507_Historial_de_Ordenes_de_trabajo;
     FechaInicial.Valor := Date();
     FechaFinal.Valor := Date();
     Status.ItemIndex := 0;
     FParametros := TZetaParams.Create;
     dmLabor.cdsHistorialWOrder.Close;
     Parte.LookupDataset := dmLabor.cdsPartes;
end;

procedure THistorialWOrder.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
     inherited;
end;

procedure THistorialWOrder.Connect;
begin
     with dmLabor do
     begin
          cdsPartes.Conectar;
          DataSource.DataSet:= cdsHistorialWOrder;
          cdsWOrderSteps.Tag := D_LAB_HISTORIA_ORDENES;
     end;
end;

procedure THistorialWOrder.Refresh;
var
   dInicio, dFin: TDate;
begin
     if CBFecha.Checked then
     begin
          dInicio := FechaInicial.Valor;
          dFin := FechaFinal.Valor;
     end
     else
     begin
          dInicio := NullDateTime;
          dFin := NullDateTime;
     end;
     with FParametros do
     begin
          AddInteger( 'Tipo', Status.ItemIndex-1 );
          AddString( 'Parte', Parte.Llave );
          AddDate( 'FechaInicial', dInicio );
          AddDate( 'FechaFinal', dFin );
          AddString( 'OrderInicial', WO_NUMBER_INI.Valor );
          AddString( 'OrderFinal', WO_NUMBER_FIN.Valor );
     end;
     dmLabor.RefrescarHistorialWOrder( FParametros );
end;

procedure THistorialWOrder.RefrescarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ZetaDBGrid.SetFocus;
     DoBestFit;
end;

procedure THistorialWOrder.Modificar;
begin
     with dmLabor do
     begin
          FParametros.AddString( 'WO_NUMBER', cdsHistorialWOrder.FieldByName('WO_NUMBER').AsString );
          RefrescarWOrderSteps(FParametros);
          cdsWOrderSteps.Modificar;
     end;
end;

procedure THistorialWOrder.Agregar;
begin
     FParametros.AddString( 'WO_NUMBER', '' );
     with dmLabor do
     begin
          RefrescarWOrderSteps(FParametros);
          cdsWOrderSteps.Agregar;
     end;
end;

procedure THistorialWOrder.Borrar;
begin
     with dmLabor do
     begin
          FParametros.AddString( 'WO_NUMBER', cdsHistorialWOrder.FieldByName('WO_NUMBER').AsString );
          RefrescarWOrderSteps(FParametros);
          cdsWOrderSteps.Borrar;
          cdsHistorialWOrder.Delete;   // Para que se refleje en la lista
     end;
end;

procedure THistorialWOrder.CBFechaClick(Sender: TObject);
var
   lEnabled: Boolean;
begin
     inherited;
     lEnabled := CbFecha.Checked;
     lbInicio.Enabled := lEnabled;
     lbFin.Enabled := lEnabled;
     FechaInicial.Enabled := lEnabled;
     FechaFinal.Enabled := lEnabled;
end;

end.


