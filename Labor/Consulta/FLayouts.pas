unit FLayouts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,DLabor,
  Vcl.StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TLayouts = class(TBaseGridLectura_DevEx)
    LY_CODIGO: TcxGridDBColumn;
    LY_NOMBRE: TcxGridDBColumn;
    AR_CODIGO_TXT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Connect;override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Refresh; override;
  end;

var
  Layouts: TLayouts;


implementation

uses
    zAccesosTress,ZetaCommonLists,ZetaCommonClasses,DGlobal,ZGlobalTress,
  DBaseGlobal;


{$R *.dfm}


procedure TLayouts.Connect;
begin
     inherited;
     with dmLabor do
     begin
          cdsLayouts.Conectar;
          DataSource.DataSet := cdsLayouts;
     end;
end;

procedure TLayouts.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_LAB_PLANTILLAS;
     Caption := Global.GetGlobalString(K_GLOBAL_LABOR_PLANTILLA);
     HelpContext := H_LAB_CAT_LAYOUT;
end;


procedure TLayouts.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
  ZetaDBGridDBTableView.Columns[0].Width:=80;
end;

procedure TLayouts.Agregar;
begin
     inherited;
     dmLabor.cdsLayouts.Agregar;
end;


procedure TLayouts.Borrar;
begin
     inherited;
     dmLabor.cdsLayouts.Borrar;
end;

procedure TLayouts.Modificar;
begin
     inherited;
     dmLabor.cdsLayouts.Modificar;
end;

procedure TLayouts.Refresh;
begin
     inherited;
     dmLabor.cdsLayouts.Refrescar;
end;

end.
