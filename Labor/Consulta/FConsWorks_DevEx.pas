unit FConsWorks_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, ZetaKeyCombo, DBCtrls, ZetaHora, Mask,
  ZetaFecha, ZetaKeyLookup, Db, Buttons, ExtCtrls, ZetaEdit, ZetaNumero,
  ZetaDBTextBox, ZetaCommonLists, ComCtrls, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxPC, ImgList, cxButtons,
  ZetaKeyLookup_DevEx, dxBarBuiltInMenu;

type
  TConsWorks_DevEx = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    ValorActivo1: TPanel;
    ValorActivo2: TPanel;
    textoValorActivo1: TLabel;
    textoValorActivo2: TLabel;
    cxPageControl: TcxPageControl;
    Generales_DevEx: TcxTabSheet;
    Moduladores_DevEx: TcxTabSheet;
    Tiempos_DevEx: TcxTabSheet;
    WK_TMUERTO: TZetaDBKeyLookup_DevEx;
    CBMultiLote: TCheckBox;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    OP_NUMBER: TZetaDBKeyLookup_DevEx;
    CB_AREA: TZetaDBKeyLookup_DevEx;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    WK_PIEZAS: TZetaDBTextBox;
    WK_PRE_CAL: TZetaDBTextBox;
    WK_HORA_R: TZetaDBTextBox;
    lblMinutos: TLabel;
    LblTiempo: TLabel;
    WK_CEDULA: TZetaDBTextBox;
    Label2: TLabel;
    Label1: TLabel;
    lbTiempoMuerto: TLabel;
    WK_TIPO: TZetaDBTextBox;
    Label23: TLabel;
    lbOperacion: TLabel;
    Label21: TLabel;
    Label20: TLabel;
    lbArea: TLabel;
    lbl_hora_R: TLabel;
    lbWOrder: TLabel;
    lbParte: TLabel;
    WK_STATUS: TZetaDBKeyCombo;
    WK_MOD_3: TZetaDBKeyLookup_DevEx;
    WK_MOD_2: TZetaDBKeyLookup_DevEx;
    WK_MOD_1: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    lbModula3: TLabel;
    lbModula2: TLabel;
    lbModula1: TLabel;
    GroupBox3: TGroupBox;
    Label15: TLabel;
    WK_TIEMPOlbl: TLabel;
    Label17: TLabel;
    WK_HRS_ORD: TZetaDBTextBox;
    WK_HRS_2EX: TZetaDBTextBox;
    Label3: TLabel;
    WK_HRS_3EX: TZetaDBTextBox;
    WK_TIEMPO: TZetaDBTextBox;
    Label5: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    WK_FECHA_R: TZetaDBTextBox;
    WK_LINX_ID: TZetaDBTextBox;
    WK_HORA_A: TZetaDBTextBox;
    WK_MANUAL: TZetaDBTextBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FTipoValorActivo1: TipoEstado;
    FTipoValorActivo2: TipoEstado;
    procedure CambiaBotones;
    procedure DesabilitaControles;
    procedure SetControlesVisibles;
    procedure AsignaValoresActivos;
  protected
    { Private declarations }
    property TipoValorActivo1: TipoEstado read FTipoValorActivo1 write FTipoValorActivo1;
    property TipoValorActivo2: TipoEstado read FTipoValorActivo2 write FTipoValorActivo2;
  public
    { Public declarations }
    procedure Connect;
  end;

var
  ConsWorks_DevEx: TConsWorks_DevEx;

implementation

uses DCatalogos,
     DCliente,
     DGlobal,
     DLabor,
     ZetaLaborTools,
     ZetaCommonClasses,
     ZGlobalTress;



{$R *.DFM}

procedure TConsWorks_DevEx.Connect;
begin
     dmCatalogos.cdsPuestos.Conectar;
     with dmLabor do
     begin
          cdsArea.Conectar;
          cdsOpera.Conectar;
          cdsModula1.Conectar;
          cdsModula2.Conectar;
          cdsModula3.Conectar;
          cdsTiempoMuerto.Conectar;
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
          CBMultiLote.Checked := ( cdsWorks.FieldByName( 'WK_FOLIO' ).AsInteger > 0 );
     end;
end;

procedure TConsWorks_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     AsignaValoresActivos;
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
     {$endif}
end;

procedure TConsWorks_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWOrder, WO_NUMBER );
     AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOperacion, OP_NUMBER );
     AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CB_AREA );
     AsignaGlobal( K_GLOBAL_LABOR_TMUERTO, lbTiempoMuerto, WK_TMUERTO );
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, AR_CODIGO );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA1,lbModula1, WK_MOD_1 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA2, lbModula2, WK_MOD_2 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA3, lbModula3, WK_MOD_3 );
     SetControlesVisibles;
     cbMultilote.Visible := Global.GetGlobalBooleano( K_GLOBAL_LABOR_MULTILOTES );
     label20.Visible := cbMultilote.Visible;
     DesabilitaControles;
     CambiaBotones;

     WO_NUMBER.LookupDataset := dmLabor.cdsWOrderLookup;
     AR_CODIGO.LookupDataset := dmLabor.cdsPartes;
     CB_AREA.LookupDataset := dmLabor.cdsArea;
     OP_NUMBER.LookupDataset := dmLabor.cdsOpera;
     WK_TMUERTO.LookupDataset := dmLabor.cdsTiempoMuerto;
     WK_MOD_1.LookupDataset := dmLabor.cdsModula1;
     WK_MOD_2.LookupDataset := dmLabor.cdsModula2;
     WK_MOD_3.LookupDataset := dmLabor.cdsModula3;
     CB_PUESTO.LookUpDataSet := dmCatalogos.cdsPuestos;
end;

procedure TConsWorks_DevEx.SetControlesVisibles;

   procedure SetControlLookup( oControl: TZetaDBKeyLookup_DevEx; oLabel : TLabel );
   begin
        if not oControl.Enabled then
        begin
             oControl.Visible := FALSE;
             oLabel.Visible := FALSE;
        end;
   end;

begin
     // De momento solo funciona en moduladores
     SetControlLookup( WK_MOD_1, lbModula1 );
     SetControlLookup( WK_MOD_2, lbModula2 );
     SetControlLookup( WK_MOD_3, lbModula3 );
     // Si se tiene Orden no se puede capturar parte
     if WO_NUMBER.Enabled and AR_CODIGO.Enabled then
     begin
          lbParte.Enabled := FALSE;
          AR_CODIGO.Enabled := FALSE;
     end;
end;

procedure TConsWorks_DevEx.CambiaBotones;
begin
     Ok_DevEx.Enabled := False;
     with Cancelar_DevEx do
     begin
          OptionsImage.ImageIndex := 2;
          Caption := '&Salir';
          Hint := 'Salir de Consulta';
     end;
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
procedure TConsWorks_DevEx.AsignaValoresActivos;
begin
     with dmCliente do
     begin
          textoValorActivo1.Caption := GetValorActivoStr( TipoValorActivo1 );
          textoValorActivo2.Caption := GetValorActivoStr( TipoValorActivo2 )
     end;
end;

procedure TConsWorks_DevEx.DesabilitaControles;
begin
    WO_NUMBER.Enabled := False;
    AR_CODIGO.Enabled := False;
    CB_AREA.Enabled := False;
    OP_NUMBER.Enabled := False;
    CB_PUESTO.Enabled := False;
    WK_TMUERTO.Enabled := False;
    WK_MOD_1.Enabled := False;
    WK_MOD_2.Enabled := False;
    WK_MOD_3.Enabled := False;
    WK_STATUS.Enabled := False;
end;

end.

