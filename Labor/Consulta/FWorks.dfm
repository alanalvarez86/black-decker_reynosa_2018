inherited Works: TWorks
  Left = 266
  Top = 394
  Caption = 'Operaciones registradas'
  ClientHeight = 289
  ClientWidth = 707
  ExplicitWidth = 707
  ExplicitHeight = 289
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 707
    ExplicitWidth = 707
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 448
      ExplicitWidth = 448
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 442
        ExplicitLeft = 3
      end
    end
  end
  object Grid: TZetaDBGrid [1]
    Left = 0
    Top = 101
    Width = 707
    Height = 188
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'WK_HORA_R'
        Title.Caption = 'H. Real'
        Width = 40
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'WK_HORA_A'
        Title.Caption = 'H. Ajus.'
        Width = 40
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'WK_TIEMPO'
        Title.Caption = 'Duraci'#243'n'
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_TIPO'
        Title.Caption = 'Tipo'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_CEDULA'
        Title.Caption = 'C'#233'dula'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_NUMBER'
        Title.Caption = 'Orden'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OP_NOMBRE'
        Title.Caption = 'Operaci'#243'n'
        Width = 135
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AR_NOMBRE'
        Title.Caption = 'Parte'
        Width = 135
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_AREA'
        Title.Caption = 'Area'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_TMUERTO'
        Title.Caption = 'T. Muerto'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_MANUAL'
        Title.Caption = 'Manual'
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 707
    Height = 82
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 27
      Top = 7
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object AU_FECHA: TZetaDBTextBox
      Left = 64
      Top = 5
      Width = 75
      Height = 17
      AutoSize = False
      Caption = 'AU_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_FECHA'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AU_DIA_SEM: TZetaDBTextBox
      Left = 141
      Top = 5
      Width = 88
      Height = 17
      AutoSize = False
      Caption = 'AU_DIA_SEM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_DIA_SEM'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LblDia: TLabel
      Left = 39
      Top = 25
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'a:'
    end
    object AU_STATUS: TZetaDBTextBox
      Left = 64
      Top = 23
      Width = 165
      Height = 17
      AutoSize = False
      Caption = 'AU_STATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_STATUS'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AU_TIPODIA: TZetaDBTextBox
      Left = 64
      Top = 41
      Width = 165
      Height = 17
      AutoSize = False
      Caption = 'AU_TIPODIA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AU_TIPODIA'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 36
      Top = 43
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object HO_DESCRIP: TZetaDBTextBox
      Left = 132
      Top = 59
      Width = 180
      Height = 17
      AutoSize = False
      Caption = 'HO_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'HO_DESCRIP'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object HO_CODIGO: TZetaDBTextBox
      Left = 64
      Top = 59
      Width = 60
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'HO_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'HO_CODIGO'
      DataSource = dsAusencia
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label3: TLabel
      Left = 23
      Top = 61
      Width = 37
      Height = 13
      Caption = 'Horario:'
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 101
    Width = 707
    Height = 188
    ExplicitTop = 101
    ExplicitWidth = 707
    ExplicitHeight = 188
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object WK_HORA_R: TcxGridDBColumn
        Caption = 'H. Real'
        DataBinding.FieldName = 'WK_HORA_R'
        Options.Grouping = False
      end
      object WK_HORA_A: TcxGridDBColumn
        Caption = 'H. Ajus.'
        DataBinding.FieldName = 'WK_HORA_A'
        Options.Grouping = False
      end
      object WK_TIEMPO: TcxGridDBColumn
        Caption = 'Duraci'#243'n'
        DataBinding.FieldName = 'WK_TIEMPO'
        Options.Grouping = False
      end
      object WK_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'WK_TIPO'
        Options.Grouping = False
      end
      object WK_CEDULA: TcxGridDBColumn
        Caption = 'C'#233'dula'
        DataBinding.FieldName = 'WK_CEDULA'
        Options.Grouping = False
      end
      object WO_NUMBER: TcxGridDBColumn
        Caption = 'Orden trabajo'
        DataBinding.FieldName = 'WO_NUMBER'
        Options.Grouping = False
      end
      object OP_NOMBRE: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OP_NOMBRE'
        Options.Grouping = False
      end
      object AR_NOMBRE: TcxGridDBColumn
        Caption = 'Parte'
        DataBinding.FieldName = 'AR_NOMBRE'
        Options.Grouping = False
      end
      object CB_AREA: TcxGridDBColumn
        Caption = 'Area'
        DataBinding.FieldName = 'CB_AREA'
        Options.Grouping = False
      end
      object WK_TMUERTO: TcxGridDBColumn
        Caption = 'T. Muerto'
        DataBinding.FieldName = 'WK_TMUERTO'
        Options.Grouping = False
      end
      object WK_MANUAL: TcxGridDBColumn
        Caption = 'Manual'
        DataBinding.FieldName = 'WK_MANUAL'
        Options.Grouping = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 144
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsAusencia: TDataSource
    OnDataChange = DataSourceDataChange
    Left = 48
    Top = 144
  end
end
