inherited ConsWorks: TConsWorks
  Left = 107
  Top = 204
  Caption = 'Operaciones Registradas'
  ClientHeight = 313
  ClientWidth = 493
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 499
  ExplicitHeight = 341
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 277
    Width = 493
    ExplicitTop = 277
    ExplicitWidth = 498
    inherited OK: TcxButton
      Left = 315
      Top = 6
      OnClick = nil
      ExplicitLeft = 315
      ExplicitTop = 6
    end
    inherited Cancelar: TcxButton
      Left = 399
      Top = 6
      OnClick = nil
      ExplicitLeft = 399
      ExplicitTop = 6
    end
  end
  object PageControl: TPageControl [1]
    Left = 0
    Top = 19
    Width = 493
    Height = 258
    ActivePage = Generales
    Align = alBottom
    TabOrder = 1
    ExplicitWidth = 498
    object Generales: TTabSheet
      Caption = 'Generales'
      ExplicitWidth = 490
      object lbParte: TLabel
        Left = 96
        Top = 104
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Parte:'
      end
      object lbWOrder: TLabel
        Left = 38
        Top = 82
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Orden de Trabajo:'
      end
      object lbl_hora_R: TLabel
        Left = 73
        Top = 22
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora Real:'
      end
      object lbArea: TLabel
        Left = 99
        Top = 126
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Area:'
      end
      object Label20: TLabel
        Left = 76
        Top = 62
        Width = 49
        Height = 13
        Alignment = taRightJustify
        Caption = 'Multi-Lote:'
      end
      object Label21: TLabel
        Left = 101
        Top = 1
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo:'
      end
      object lbOperacion: TLabel
        Left = 72
        Top = 148
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Operaci'#243'n:'
      end
      object Label23: TLabel
        Left = 88
        Top = 170
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object WK_TIPO: TZetaDBTextBox
        Left = 128
        Top = -1
        Width = 99
        Height = 17
        AutoSize = False
        Caption = 'WK_TIPO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_TIPO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lbTiempoMuerto: TLabel
        Left = 50
        Top = 192
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tiempo Muerto:'
      end
      object Label1: TLabel
        Left = 90
        Top = 214
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Piezas:'
      end
      object Label2: TLabel
        Left = 233
        Top = 1
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#233'dula:'
      end
      object WK_CEDULA: TZetaDBTextBox
        Left = 273
        Top = -1
        Width = 99
        Height = 17
        AutoSize = False
        Caption = 'WK_CEDULA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_CEDULA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object LblTiempo: TLabel
        Left = 20
        Top = 42
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tiempo PreCalculado:'
      end
      object lblMinutos: TLabel
        Left = 180
        Top = 44
        Width = 37
        Height = 13
        Caption = 'Minutos'
      end
      object WK_HORA_R: TZetaDBTextBox
        Left = 128
        Top = 19
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'WK_HORA_R'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_HORA_R'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object WK_PRE_CAL: TZetaDBTextBox
        Left = 128
        Top = 40
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'WK_PRE_CAL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_PRE_CAL'
        DataSource = DataSource
        FormatFloat = '%2.2n'
        FormatCurrency = '%m'
      end
      object WK_PIEZAS: TZetaDBTextBox
        Left = 128
        Top = 210
        Width = 49
        Height = 17
        AutoSize = False
        Caption = 'WK_PIEZAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'WK_PIEZAS'
        DataSource = DataSource
        FormatFloat = '%2.2n'
        FormatCurrency = '%m'
      end
      object WO_NUMBER: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 78
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsWOrderLookup
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 100
        DataField = 'WO_NUMBER'
        DataSource = DataSource
      end
      object AR_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 100
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsPartes
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 100
        DataField = 'AR_CODIGO'
        DataSource = DataSource
      end
      object CB_AREA: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 122
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsArea
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 100
        DataField = 'CB_AREA'
        DataSource = DataSource
      end
      object OP_NUMBER: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 144
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsOpera
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 100
        DataField = 'OP_NUMBER'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 166
        Width = 350
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 100
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CBMultiLote: TcxCheckBox
        Left = 128
        Top = 59
        Enabled = False
        TabOrder = 0
        Width = 17
      end
      object WK_TMUERTO: TZetaDBKeyLookup_DevEx
        Left = 128
        Top = 188
        Width = 350
        Height = 21
        LookupDataset = dmLabor.cdsTiempoMuerto
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 100
        DataField = 'WK_TMUERTO'
        DataSource = DataSource
      end
    end
    object Moduladores: TTabSheet
      Caption = 'Moduladores'
      ImageIndex = 2
      ExplicitWidth = 490
      object lbModula1: TLabel
        Left = 41
        Top = 75
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modulador #1:'
      end
      object lbModula2: TLabel
        Left = 41
        Top = 99
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modulador #2:'
      end
      object lbModula3: TLabel
        Left = 41
        Top = 122
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modulador #3:'
      end
      object Label4: TLabel
        Left = 76
        Top = 52
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object WK_MOD_1: TZetaDBKeyLookup_DevEx
        Left = 113
        Top = 71
        Width = 300
        Height = 21
        LookupDataset = dmLabor.cdsModula1
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'WK_MOD_1'
        DataSource = DataSource
      end
      object WK_MOD_2: TZetaDBKeyLookup_DevEx
        Left = 113
        Top = 95
        Width = 300
        Height = 21
        LookupDataset = dmLabor.cdsModula2
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'WK_MOD_2'
        DataSource = DataSource
      end
      object WK_MOD_3: TZetaDBKeyLookup_DevEx
        Left = 113
        Top = 118
        Width = 300
        Height = 21
        LookupDataset = dmLabor.cdsModula3
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'WK_MOD_3'
        DataSource = DataSource
      end
      object WK_STATUS: TZetaDBKeyCombo
        Left = 113
        Top = 48
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfStatusLectura
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'WK_STATUS'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object Tiempos: TTabSheet
      Caption = 'Tiempos y Lecturas'
      ImageIndex = 1
      ExplicitWidth = 490
      object GroupBox1: TGroupBox
        Left = 68
        Top = 116
        Width = 353
        Height = 104
        Caption = ' Lecturas '
        TabOrder = 1
        object Label7: TLabel
          Left = 10
          Top = 59
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora Ajustada:'
        end
        object Label8: TLabel
          Left = 48
          Top = 17
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object Label9: TLabel
          Left = 38
          Top = 37
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Terminal:'
        end
        object Label10: TLabel
          Left = 42
          Top = 80
          Width = 38
          Height = 13
          Alignment = taRightJustify
          Caption = 'Manual:'
        end
        object WK_FECHA_R: TZetaDBTextBox
          Left = 83
          Top = 16
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_FECHA_R'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_FECHA_R'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_LINX_ID: TZetaDBTextBox
          Left = 83
          Top = 36
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_LINX_ID'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_LINX_ID'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_HORA_A: TZetaDBTextBox
          Left = 83
          Top = 57
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_HORA_A'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HORA_A'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_MANUAL: TZetaDBTextBox
          Left = 83
          Top = 78
          Width = 112
          Height = 17
          AutoSize = False
          Caption = 'WK_MANUAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_MANUAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox3: TGroupBox
        Left = 68
        Top = 10
        Width = 353
        Height = 103
        Caption = ' Tiempos '
        TabOrder = 0
        object Label15: TLabel
          Left = 12
          Top = 58
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Extras Dobles:'
        end
        object WK_TIEMPOlbl: TLabel
          Left = 35
          Top = 18
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Duraci'#243'n:'
        end
        object Label17: TLabel
          Left = 31
          Top = 38
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordinarias:'
        end
        object WK_HRS_ORD: TZetaDBTextBox
          Left = 83
          Top = 37
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_HRS_ORD'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HRS_ORD'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_HRS_2EX: TZetaDBTextBox
          Left = 83
          Top = 57
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_HRS_2EX'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HRS_2EX'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 14
          Top = 78
          Width = 66
          Height = 13
          Alignment = taRightJustify
          Caption = 'Extras Triples:'
        end
        object WK_HRS_3EX: TZetaDBTextBox
          Left = 83
          Top = 77
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_HRS_3EX'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_HRS_3EX'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object WK_TIEMPO: TZetaDBTextBox
          Left = 83
          Top = 16
          Width = 113
          Height = 17
          AutoSize = False
          Caption = 'WK_TIEMPO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'WK_TIEMPO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 198
          Top = 18
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object Label11: TLabel
          Left = 198
          Top = 39
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object Label12: TLabel
          Left = 198
          Top = 59
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
        object Label13: TLabel
          Left = 198
          Top = 79
          Width = 37
          Height = 13
          Caption = 'Minutos'
        end
      end
    end
  end
  object ValorActivo1: TPanel [2]
    Left = 0
    Top = 0
    Width = 323
    Height = 19
    Align = alLeft
    Alignment = taLeftJustify
    BorderWidth = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object textoValorActivo1: TLabel
      Left = 3
      Top = 3
      Width = 317
      Height = 13
      Align = alTop
      Caption = 'textoValorActivo1'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      ExplicitWidth = 83
    end
  end
  object ValorActivo2: TPanel [3]
    Left = 323
    Top = 0
    Width = 170
    Height = 19
    Align = alClient
    Alignment = taRightJustify
    BorderWidth = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    ExplicitWidth = 175
    object textoValorActivo2: TLabel
      Left = 3
      Top = 3
      Width = 164
      Height = 13
      Align = alTop
      Alignment = taRightJustify
      Caption = 'textoValorActivo2'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      ExplicitLeft = 89
      ExplicitWidth = 83
    end
  end
  object DataSource: TDataSource [4]
    AutoEdit = False
    Left = 8
    Top = 40
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
