unit FComponentes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridLectura_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TComponentes = class(TBaseGridLectura_DevEx)
    CN_CODIGO: TcxGridDBColumn;
    CN_NOMBRE: TcxGridDBColumn;
    CN_COSTO: TcxGridDBColumn;
    CN_UNIDAD: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
    procedure DoLookup;override;
  end;

var
  Componentes: TComponentes;

implementation
uses DLabor,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaBuscador_DevEx;

{$R *.DFM}

procedure TComponentes.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookUp := TRUE;
     HelpContext := H00056_Componentes;
end;

procedure TComponentes.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CN_CODIGO'), K_SIN_TIPO , '', skCount );
  //CN_CODIGO.Width := 84;
end;

procedure TComponentes.Connect;
begin
     with dmLabor do
     begin
          cdsComponentes.Conectar;
          DataSource.DataSet := cdsComponentes;
     end;
end;

procedure TComponentes.Refresh;
begin
     dmLabor.cdsComponentes.Refrescar;
end;

procedure TComponentes.Agregar;
begin
     dmLabor.cdsComponentes.Agregar;
end;

procedure TComponentes.Borrar;
begin
     dmLabor.cdsComponentes.Borrar;
end;

procedure TComponentes.Modificar;
begin
     dmLabor.cdsComponentes.Modificar;
end;

procedure TComponentes.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', 'Componentes', 'CN_CODIGO', dmLabor.cdsComponentes );
end;
end.
