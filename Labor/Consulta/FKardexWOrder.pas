unit FKardexWOrder;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, ExtCtrls, StdCtrls, Mask, DBCtrls, ComCtrls, Buttons,
     ZBaseGridLectura_DevEx,
     ZetaCommonClasses,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaSmartLists, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  ZetaKeyLookup_DevEx;

  type
  TKardexWOrder = class(TBaseGridLectura_DevEx)
    dsRenglon: TDataSource;
    PanelFiltros: TPanel;
    Label1: TLabel;
    lbParte: TLabel;
    Label2: TLabel;
    WO_QTY: TZetaDBTextBox;
    WO_STATUS: TZetaDBTextBox;
    AR_CODIGO: TZetaDBTextBox;
    AR_NOMBRE: TZetaDBTextBox;
    lbWOrder: TLabel;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    AU_FECHA: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    OP_NUMBER: TcxGridDBColumn;
    WK_TIEMPO: TcxGridDBColumn;
    WK_STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
    FLlave : string;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  KardexWOrder: TKardexWOrder;

implementation

{$R *.DFM}

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaBuscaWOrder,
     ZetaCommonLists,
     ZetaCommonTools;

{ TSistBitacora }

procedure TKardexWOrder.FormCreate(Sender: TObject);
begin
     inherited;
     Caption := 'Kardex de ' + Global.GetGlobalString(K_GLOBAL_LABOR_ORDEN);
     lbParte.Caption := Global.GetGlobalString(K_GLOBAL_LABOR_PARTE);
     AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
     AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION,ZetaDBGridDBTableView.Columns[3]);
     HelpContext := H9506_Kardex_de_Orden_de_trabajo;
     FParametros := TZetaParams.Create;
     AR_CODIGO.Caption := '';
     AR_NOMBRE.Caption := '';
     WO_STATUS.Caption := '';
     WO_QTY.Caption := '';
     with dmLabor do
     begin
          cdsKardexWOrder.Close;
          cdsWorksHijo.Close;
     end;
     WO_NUMBER.LookupDataset := dmLabor.cdsWOrderLookup;
end;

procedure TKardexWOrder.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
     inherited;
end;

procedure TKardexWOrder.FormShow(Sender: TObject);
begin
  DoBestFit;
  inherited;
  CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[0],0,'', skCount );
  ZetaDBGridDBTableView.Columns[0].width := 84;
end;

procedure TKardexWOrder.Connect;
begin
     with dmLabor do
     begin
          DataSource.DataSet:= cdsKardexWOrder;
          dsRenglon.DataSet := cdsWorksHijo;
//          cdsKardexWOrder.Close;
//          cdsWorksHijo.Close;
     end;

     //Refresh;
end;

procedure TKardexWOrder.Refresh;
begin
     if FLlave <> WO_NUMBER.Llave then
     begin
          FLlave := WO_NUMBER.Llave;
          if FLlave > '' then
          begin
               with FParametros do
                    AddString( 'WO_NUMBER', FLlave );
               dmLabor.RefrescarKardexWOrder( FParametros );
               ZetaDbGrid.SetFocus;
          end;
     end;
end;

procedure TKardexWOrder.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     DoRefresh;
     DoBestFit;
     ZetaDBGridDBTableView.Columns[0].Width:=84;
end;

function TKardexWOrder.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar el ' + Caption;
end;

function TKardexWOrder.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar Datos del ' + Caption;
end;

function TKardexWOrder.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar Datos al ' + Caption;
end;

end.


