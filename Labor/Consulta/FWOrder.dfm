inherited WOrder: TWOrder
  Left = 189
  Top = 281
  ActiveControl = WO_NUMBER
  Caption = 'Orden de Trabajo'
  ClientHeight = 354
  ClientWidth = 508
  OnDestroy = FormDestroy
  ExplicitWidth = 508
  ExplicitHeight = 354
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 508
    ExplicitWidth = 508
    inherited Slider: TSplitter
      Left = 339
      ExplicitLeft = 339
    end
    inherited ValorActivo1: TPanel
      Width = 323
      ExplicitWidth = 323
      inherited textoValorActivo1: TLabel
        Width = 317
      end
    end
    inherited ValorActivo2: TPanel
      Left = 342
      Width = 166
      ExplicitLeft = 342
      ExplicitWidth = 166
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 160
        ExplicitLeft = 80
      end
    end
  end
  object Panel: TPanel [1]
    Left = 0
    Top = 19
    Width = 508
    Height = 94
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 92
      Top = 52
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object lbParte: TLabel
      Left = 97
      Top = 32
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Parte:'
    end
    object Label2: TLabel
      Left = 80
      Top = 73
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cantidad:'
    end
    object WO_QTY: TZetaDBTextBox
      Left = 131
      Top = 71
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'WO_QTY'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WO_QTY'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WO_STATUS: TZetaDBTextBox
      Left = 131
      Top = 50
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'WO_STATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WO_STATUS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AR_CODIGO: TZetaDBTextBox
      Left = 131
      Top = 30
      Width = 100
      Height = 17
      AutoSize = False
      Caption = 'AR_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AR_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object AR_NOMBRE: TZetaDBTextBox
      Left = 233
      Top = 30
      Width = 245
      Height = 17
      AutoSize = False
      Caption = 'AR_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'AR_NOMBRE'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbWOrder: TLabel
      Left = 39
      Top = 10
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden de Trabajo:'
    end
    object WO_NUMBER: TZetaDBKeyLookup_DevEx
      Left = 131
      Top = 6
      Width = 350
      Height = 21
      LookupDataset = dmLabor.cdsWOrderLookup
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 100
      OnValidKey = WO_NUMBERValidKey
      DataField = 'WO_NUMBER'
      DataSource = DataSource
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 113
    Width = 508
    Height = 241
    ExplicitTop = 113
    ExplicitWidth = 508
    ExplicitHeight = 241
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsRenglon
      object ST_SEQUENC: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'ST_SEQUENC'
        Options.Grouping = False
      end
      object OP_NUMBER: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'OP_NUMBER'
        Options.Grouping = False
      end
      object OP_NOMBRE: TcxGridDBColumn
        Caption = 'Operaci'#243'n'
        DataBinding.FieldName = 'OP_NOMBRE'
        Options.Grouping = False
      end
      object ST_STD_HR: TcxGridDBColumn
        Caption = 'Standard'
        DataBinding.FieldName = 'ST_STD_HR'
        Options.Grouping = False
      end
      object ST_REAL_HR: TcxGridDBColumn
        Caption = 'Real'
        DataBinding.FieldName = 'ST_REAL_HR'
        Options.Grouping = False
      end
      object ST_QTY: TcxGridDBColumn
        Caption = 'Cantidad'
        DataBinding.FieldName = 'ST_QTY'
        Options.Grouping = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 344
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsRenglon: TDataSource
    Left = 376
    Top = 72
  end
end
