unit FTParte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseTablasConsulta_DevEx, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, FTablas,
  Vcl.StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TTParte = class(TTablaOpcion_DevEx)
    TT_CODIGO: TcxGridDBColumn;
    TT_DESCRIP: TcxGridDBColumn;
    TT_INGLES: TcxGridDBColumn;
    TT_NUMERO: TcxGridDBColumn;
    TT_TEXTO: TcxGridDBColumn;
  private
    { Private declarations }
  protected
    procedure AfterCreate; override;
  public
    { Public declarations }
    procedure DoLookup;override;
  end;

var
  TParte: TTParte;

implementation

uses Dlabor,
     DGlobal,
     ZetaBuscador_DevEx,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TTParte.AfterCreate;
begin
     ApplyMinWidth;
     CreaColumaSumatoria( ZetaDBGridDBTableView.Columns[5],0,'', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
   //  ZetaDBGridDBTableView.Columns[5].Width:=84;
     inherited AfterCreate;
     Caption := dmLabor.cdsTParte.LookupName;
     ZetaDataset := dmLabor.cdsTParte;
     HelpContext := H9512_Clasificacion_de_partes;
end;

procedure TTParte.DoLookup;
begin
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TT_CODIGO', ZetaDataset );
end;
end.
