unit ZetaLaborTools;

interface

uses StdCtrls, Forms, DBGrids, Controls, Extctrls, ComCtrls, dbClient, Classes, SysUtils, Db,
     ZetaKeyLookup,ZetaKeyLookup_DevEx, Windows, ShellAPI, ZetaCommonLists,
     cxGridCustomTableView,cxGridTableView, cxGridDBTableView,cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

function GetLaborLabel( const iGlobal: Integer ): String;
function AsignaGlobal( const iGlobal: Integer; oLabel: TLabel; oControl:TWinControl ): String;
function AsignaGlobalColumn( const iGlobal: Integer; oColumn: TColumn ): String;
function AsignaGlobalColumn_DevEx( const iGlobal: Integer; oColumn: TcxGridDBColumn ): String;
function GetAltura( oControl: TWinControl ): Integer;
function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
function ChecaTipoCedula( const TipoCedula: eTipoCedula; const eAccion: TUpdateKind; const iDerechos: Integer; var sMensaje: String ): Boolean;
procedure SetPanelLabel( oPanel: TPanel; oLabel: TLabel; const iGlobal: Integer );
procedure HabilitaPanel( oPanel: TPanel; const lEnabled: Boolean );
procedure SetPanelVisible( oPanel: TPanel; const lVisible: Boolean );
procedure InitcdsColectivo(cdsColectivo: TClientDataset);
procedure RevisaDataSet( const sCampo: String; FDataSet: TClientDataSet; Lista: TStrings );
procedure ReportaControlError( oControl : TWinControl; const sMensaje: String );
procedure ValidaControlLookup( oControl : TZetaKeyLookup; oLabel : TLabel );overload;
procedure ValidaControlLookup( oControl : TZetaKeyLookup_DevEx; oLabel : TLabel );overload;

const
     MIS_AREAS = 'MI_CODIGO <> '''' and MI_CODIGO IS NOT NULL';
     OTRAS_AREAS = 'MI_CODIGO = '''' or MI_CODIGO IS NULL';
     K_LABOR_NO_VALIDAR_LOOKUP = 1;

implementation

uses DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosMgr;

function GetDefaultString( const iGlobal: Integer ): String;
begin
     case iGlobal of
          K_GLOBAL_LABOR_ORDEN: Result := 'Orden de trabajo';
          K_GLOBAL_LABOR_ORDENES: Result := 'Ordenes de trabajo';
          K_GLOBAL_LABOR_PARTE: Result := 'Parte';
          K_GLOBAL_LABOR_PARTES:  Result := 'Partes';
          K_GLOBAL_LABOR_AREA: Result := 'Area';
          K_GLOBAL_LABOR_AREAS: Result := 'Areas';
          K_GLOBAL_LABOR_OPERACION: Result := 'Operaci�n';
          K_GLOBAL_LABOR_OPERACIONES: Result := 'Operaciones'
     else
         Result := '';
     end;
end;

function GetLaborLabel( const iGlobal: Integer ): String;
begin
     Result := Global.GetGlobalString( iGlobal );
     if StrVacio( Result ) then
        Result := GetDefaultString( iGlobal );
end;

function AsignaGlobal( const iGlobal: Integer; oLabel: TLabel; oControl: TWinControl ): String;
var
   sTexto: String;
begin
     Result := Global.GetGlobalString( iGlobal );
     oControl.Enabled := StrLleno( Result );
     with oLabel do
     begin
          sTexto := Result;
          if StrVacio( sTexto ) then
             sTexto := GetDefaultString( iGlobal );
          Caption := sTexto + ':';
          Enabled := oControl.Enabled;
     end;
end;

function AsignaGlobalColumn( const iGlobal: Integer; oColumn: TColumn ): String;
begin
     with Global do
     begin
          Result := GetGlobalString( iGlobal );
     end;
     with oColumn do
     begin
          with Title do
          begin
               Visible := StrLleno( Result );
               Caption := Result;
          end;
     end;
end;

//DevEx (by am): Metodo agregado para definir las columnas visibles con la clase de columnas de DevExpress
function AsignaGlobalColumn_DevEx( const iGlobal: Integer; oColumn: TcxGridDBColumn ): String;
begin
     with Global do
     begin
          Result := GetGlobalString( iGlobal );
     end;
     with oColumn do
     begin
          Visible := StrLleno( Result );
          Caption := Result;
     end;
end;

procedure SetPanelLabel( oPanel: TPanel; oLabel: TLabel; const iGlobal: Integer );
begin
     oLabel.Caption := Global.GetGlobalString( iGlobal );
     SetPanelVisible( oPanel, StrLleno( oLabel.Caption ) );
     oLabel.Caption := oLabel.Caption + ':';
end;

procedure SetPanelVisible( oPanel: TPanel; const lVisible: Boolean );
var
   i: Integer;
begin
     oPanel.Visible := lVisible;
     for i := 0 to ( oPanel.ControlCount - 1 ) do
     begin
          oPanel.Controls[ i ].Visible := lVisible;
     end;
end;

function GetAltura( oControl: TWinControl ): Integer;
var
   i: Integer;
begin
     Result := 0;
     for i := 0 to ( oControl.ControlCount - 1 ) do
     begin
          if ( oControl.Controls[ i ] is TPanel ) then
          begin
               with TPanel( oControl.Controls[ i ] ) do
               begin
                    if Visible then
                       Result := Result + Height;
               end;
          end;
     end;
end;

procedure HabilitaPanel( oPanel: TPanel; const lEnabled: Boolean );
var
   i: Integer;
begin
     for i := 0 to ( oPanel.ControlCount - 1 ) do
     begin
          oPanel.Controls[ i ].Enabled := lEnabled;
     end;
end;

procedure InitcdsColectivo(cdsColectivo: TClientDataset);
begin
     with cdsColectivo do
          if Active then
             EmptyDataSet
          else
             CreateDataSet;
end;

procedure RevisaDataSet( const sCampo: String; FDataSet: TClientDataSet; Lista: TStrings );
var
   sCodigo: String;
   lSalir: Boolean;
begin
     Lista.Clear;
     lSalir:= FALSE;
     with FDataSet do
     begin
          try
             DisableControls;
             First;
             while ( not EOF ) and ( not lSalir ) do
             begin
                  sCodigo:= FieldByName( sCampo ).AsString;
                  if ( Lista.Count > 0 ) and ( Lista.IndexOf( sCodigo ) > -1 ) then
                  begin
                       if ( RecNo = RecordCount ) then
                          lSalir := TRUE;
                       Delete;
                  end
                  else
                  begin
                       Lista.Add( sCodigo );
                       Next;
                  end;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure ReportaControlError( oControl : TWinControl; const sMensaje: String );
var
   PadreControl : TWinControl;

   function EnfocaControl: Boolean;
   begin
        with oControl do
        begin
             Result := CanFocus;
             if Result then
                SetFocus;
        end;
   end;

begin
     if ( not EnfocaControl ) then
     begin
          { Si no puede obtener Focus se intentar� activar el TabSheet en que se encuentra dicho control,
            posteriormente se volver� a intentar poner el Focus.
            Este mecanismo solo funciona si se tienen los controles separados mediante un PageControl, de
            otra forma no podr� poner el Focus }
          PadreControl := oControl.Parent;
          while ( PadreControl <> nil ) and ( not ( PadreControl is TTabSheet ) ) do
          begin
               PadreControl := PadreControl.Parent;
          end;
          if ( PadreControl <> nil ) and ( PadreControl is TTabSheet ) then
             TTabSheet( PadreControl ).PageControl.ActivePage := TTabSheet( PadreControl );
          EnfocaControl;
     end;
     DataBaseError( sMensaje );
end;

procedure ValidaControlLookup( oControl : TZetaKeyLookup; oLabel : TLabel );overload;
const
     K_LOOKUP_ERROR = 'Falta capturar el c�digo de %s';
begin
     with oControl do
     begin
          if ( Visible ) and strVacio( Llave ) and ( Tag <> K_LABOR_NO_VALIDAR_LOOKUP ) then
             ReportaControlError( oControl, Format( K_LOOKUP_ERROR, [ strTransAll( oLabel.Caption, ':', '' ) ] ) );
     end;
end;
procedure ValidaControlLookup( oControl : TZetaKeyLookup_DevEx; oLabel : TLabel );overload;
const
     K_LOOKUP_ERROR = 'Falta capturar el c�digo de %s';
begin
     with oControl do
     begin
          if ( Visible ) and strVacio( Llave ) and ( Tag <> K_LABOR_NO_VALIDAR_LOOKUP ) then
             ReportaControlError( oControl, Format( K_LOOKUP_ERROR, [ strTransAll( oLabel.Caption, ':', '' ) ] ) );
     end;
end;

function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

function ChecaTipoCedula( const TipoCedula: eTipoCedula; const eAccion: TUpdateKind; const iDerechos: Integer;
         var sMensaje: String ): Boolean;
const
     K_MESS_ERROR = 'No tiene permiso para %s c�dulas de %s';

   function GetPosDerecho: Integer;
   begin
        case TipoCedula of
             tcOperacion  : Result := K_DERECHO_BANCA;
             tcEspeciales : Result := K_DERECHO_CONFIGURA;
             tcTMuerto    : Result := K_DERECHO_NIVEL0;
        else
             Result := K_SIN_DERECHOS;
        end;
   end;

begin
     Result := ZAccesosMgr.CheckDerecho( iDerechos, GetPosDerecho );
     if not Result then
        sMensaje := Format( K_MESS_ERROR, [ ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord( eAccion ) ), ZetaCommonLists.ObtieneElemento( lfTipoCedula, Ord( TipoCedula ) ) ] );
end;

end.
