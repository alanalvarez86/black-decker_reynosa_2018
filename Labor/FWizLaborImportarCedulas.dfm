inherited WizLaborImportarCedulas: TWizLaborImportarCedulas
  Caption = 'Importar c'#233'dulas de captura'
  ClientHeight = 396
  ClientWidth = 441
  ExplicitWidth = 447
  ExplicitHeight = 425
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 441
    Height = 396
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC400000EC401952B
      0E1B000001B2494441545847C5964F4EC24014C629B12E5C28892B6FE1013071
      E9123C8147F00454CEC0252C24EC5D79004D8C7F62EB8A1318169A6802A57EDF
      3843A6E529B4B6E324BF0CF9E6CD7B1F33AF85469AA6FF8A28BA44145D228A2E
      11459788A24B44D125A24882E1CB61308CCEEA256E8BC5492F8CDF1090D6096A
      CCC4E244DA5007627182453F1F5C07AA981983870EF140A24D64827164774118
      5DFC85DE309ED839F3067C9012C1400203031D5A7AC0C0B59D776900450F4C71
      F0D91FDF3411E069D46718F0198F241ED862B2C284F194850DA6F8B62EBCD0F3
      37F75DCCC4D2145DE9844AC1E24D600A670DFC823270199F07A3E8B808AA8F72
      06ECC41B1BC0E61D2654475860F01AF206C84C27DED44082CD7C4C1FED646530
      06F6ACE4343157F7AF7AA0F36EAD694E5FF14DD88493E2441F9201030BF03492
      FEF8D674FFAE9EC113DF1144C597193091BD020E9310EC83394882D1330DCCC0
      C204F7C2E80AF77EA4119B6C1D2B4D680F6DA205A6553D66EBF8E955DCC2224F
      40DC5425CA8031C1599BE0CF318F5FDC54254B030663008B6DF94F448584F1C9
      8A01D788A24B44D125A2E81251748928BA4414DD9136BE006D68C4D6A9C3C25D
      0000000049454E44AE426082}
    ExplicitWidth = 441
    ExplicitHeight = 331
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Proceso que permite importar c'#233'dulas de captura desde un archivo' +
        ' de texto.'
      Header.Title = ' Importaci'#243'n de c'#233'dulas de captura'
      ExplicitWidth = 419
      ExplicitHeight = 254
      inherited ProgramaLBL: TLabel
        Left = 28
        ExplicitLeft = 28
      end
      inherited ComandosLBL: TLabel
        Left = 20
        ExplicitLeft = 20
      end
      inherited ArchivoLBL: TLabel
        Left = 37
        ExplicitLeft = 37
      end
      inherited Label8: TLabel
        Left = 35
        ExplicitLeft = 35
      end
      object Label4: TLabel [4]
        Left = 0
        Top = 126
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Formato fechas:'
        FocusControl = cbFFecha
      end
      inherited ProgramaSeek: TcxButton
        Left = 388
        ExplicitLeft = 388
      end
      inherited ArchivoSeek: TcxButton
        Left = 388
        ExplicitLeft = 388
      end
      inherited Programa: TEdit
        Left = 80
        ExplicitLeft = 80
      end
      inherited Comandos: TEdit
        Left = 80
        ExplicitLeft = 80
      end
      inherited Archivo: TEdit
        Left = 80
        ExplicitLeft = 80
      end
      inherited Formato: TZetaKeyCombo
        Left = 80
        ExplicitLeft = 80
      end
      object cbFFecha: TZetaKeyCombo
        Left = 80
        Top = 122
        Width = 160
        Height = 21
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 6
        ListaFija = lfFormatoImpFecha
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 419
      ExplicitHeight = 254
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 419
        ExplicitHeight = 94
        Height = 159
        Width = 419
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 419
        Width = 419
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar este proceso se importar'#225' toda la informaci'#243'n de las ' +
            'c'#233'dulas contenidas en el archivo indicado.'
          Style.IsFontAssigned = True
          ExplicitWidth = 347
          Width = 347
          AnchorY = 49
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 112
    Top = 256
  end
end
