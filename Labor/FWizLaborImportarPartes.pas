unit FWizLaborImportarPartes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizardImportar, ComCtrls, StdCtrls, ZetaKeyCombo, Buttons,
  ZetaWizard, ExtCtrls, ZCXBaseWizardFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, Vcl.Menus, ZetaCXWizard, ZetaEdit, cxRadioGroup,
  cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl;

type
  TWizLaborImportarPartes = class(TTBaseWizardImportar)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizLaborImportarPartes: TWizLaborImportarPartes;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizLaborImportarPartes.FormCreate(Sender: TObject);
begin
     Programa.Tag := K_GLOBAL_DEF_LAB_IMP_PAR_PROGRAMA;
     Comandos.Tag := K_GLOBAL_DEF_LAB_IMP_PAR_PARAMETROS;
     Archivo.Tag := K_GLOBAL_DEF_LAB_IMP_PAR_ARCHIVO;
     NombreCatalogo := Global.GetGlobalString( K_GLOBAL_LABOR_PARTES );
     HelpContext := H00007_Importar_Productos;
     Parametros.Header.Title:='Importar '+ NombreCatalogo;
     inherited;
end;

function TWizLaborImportarPartes.EjecutarWizard: Boolean;
begin
     Result := dmLabor.ImportarPartes( ParameterList );
end;

end.
