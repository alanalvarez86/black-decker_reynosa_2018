inherited WizLaborReCalcularTiempos: TWizLaborReCalcularTiempos
  Left = 379
  Top = 158
  Caption = 'Calcular tiempos'
  ClientHeight = 428
  ClientWidth = 494
  ExplicitWidth = 500
  ExplicitHeight = 457
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 494
    Height = 428
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvTitleFont]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F40000000467414D410000B18F0BFC6105000000097048597300000EC300000E
      C301C76FA864000002F0494441545847ED562B701441103D8140201008040281
      4044201008040281402022101108240281405035EC21B89BD9AB13A40A814020
      1011084404E2440402118140444644444444449C38DE9BEDDDEBD99DD94FA552
      185E5557DD4DBFFE4C4FF7EC8C866296B9CF79E65671B10BA15D1CFE27F0CF13
      C84D7EDF65EE533D38D6BE59631F09ED62911BFB2C92C05B519F1FD8C99631E6
      92FC6D606666B77C124A607347D45190D3E6B3423E76F362577627658063B836
      33937BB9993E71C66DF25890C0755137D0C7A7C79A588ADD11D5686EE6579D99
      BE41A97F861C2DF6773ECEDFE964623EA34960270F42A25BC1D14BEAB0D3E7F8
      7F54D7A7C465F604257FCD40BE42995B061C337DEC83D6C1802589BFE9A07DEC
      3A6577DB6C5FD149949B4A82843238CAB550CEBC60777F585670B6E07883C2DF
      52EAFD18BF4CA233B84664D6971CB5D6260210E805829ED66C77BBEC0248C92A
      07707880EC5BC74C636EDEDF845D500D2626EA7620D3CB0CA88C971C3951F706
      46F3366CCF949F234E92A8D3F0E7B9365ACDC6CE896A30E0EB95F6D5AB0A68BC
      AF9541668F5911510D06CF9D4DA892F821AA347403A1E9BECB7203EC6CEE5024
      790BD69AF94C96E3A053455EF15613558082875B6FCD3D4C9D2FCBAE782B36A8
      A89A90EEADC8BCEB451580EB010F82404F451D606AA67735AFB5A1EB09B02145
      15201FDB0F9AE72571BDC2C743CDC3FFF438B3693499B79BA83CFC8D868747C0
      81702D75D1B04734B7AD5F3C403A2CC968C83D59E6596E941DCD807C13D019D7
      851205BF7ECADF892CA781C6FBA80C4E276672432A83AFA15DF04C85DA093666
      615755E08BA8D2A89F59398A4CC41306A0FE254D356A038DAF209E53A2EA0D36
      65E003DF86549F34C04E8541F588E051709E45DD09703761735CDA53E073D88B
      193BE02BA87250885DB45D247C2BEAA6AB2471A17582860D674565F679C5B22A
      5E8AEB969FDFF0E9554877E3B5C13FBBE38E3B858F5871733E70E639FBB12071
      1936AEBDC14746F134B77B08545D5810CEFA2F1ED9B0C0A3D15FF017FFAC2746
      5A620000000049454E44AE426082}
    ParentFont = False
    ExplicitWidth = 494
    ExplicitHeight = 428
    inherited Parametros: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 
        'Este proceso te permite calcular el tiempo de cada empleado dedi' +
        'cado a las operaciones y '#225'reas definidas en Labor. Se requiere i' +
        'ndicar el rango de d'#237'as a revisar.'
      Header.Title = 'C'#225'lculo de tiempos'
      ExplicitWidth = 472
      ExplicitHeight = 288
      object Label1: TLabel
        Left = 135
        Top = 79
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha &inicial:'
        FocusControl = dInicial
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 142
        Top = 107
        Width = 55
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha &final:'
        FocusControl = dFinal
      end
      object dInicial: TZetaFecha
        Left = 201
        Top = 74
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '30/Jun/00'
        Valor = 36707.000000000000000000
      end
      object dFinal: TZetaFecha
        Left = 201
        Top = 102
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '30/Jun/00'
        Valor = 36707.000000000000000000
      end
      object ckRastrear: TcxCheckBox
        Left = 103
        Top = 128
        Caption = '  &Rastrear c'#225'lculos: '
        ParentBackground = False
        ParentColor = False
        Properties.Alignment = taRightJustify
        Style.Color = clWindow
        TabOrder = 2
        Width = 114
      end
      object CBLecturas: TcxCheckBox
        Left = 109
        Top = 149
        Caption = '&Procesar lecturas:    '
        ParentBackground = False
        ParentColor = False
        Properties.Alignment = taRightJustify
        Style.Color = clWindow
        TabOrder = 3
        Width = 108
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Title = 'Presione aplicar para iniciar el proceso.'
      ParentFont = False
      ExplicitWidth = 472
      ExplicitHeight = 288
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 472
        ExplicitHeight = 191
        Height = 193
        Width = 472
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 472
        Width = 472
        inherited Advertencia: TcxLabel
          Caption = 
            'Al aplicar el proceso se reflejar'#225'n las lecturas y '#243'rdenes de un' +
            'a operaci'#243'n en tiempos de las operaciones de los empleados indic' +
            'ados.'
          Style.IsFontAssigned = True
          ExplicitLeft = 70
          ExplicitWidth = 400
          Width = 404
          AnchorY = 57
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.TitleFont.Style = [fsBold]
      Header.Description = 'Generar la lista de empleados que requiere aplicar el proceso.'
      Header.Title = 'Filtro de empleados'
      ParentFont = False
      ExplicitWidth = 472
      ExplicitHeight = 288
      inherited sCondicionLBl: TLabel
        Left = 40
        Top = 137
        ExplicitLeft = 40
        ExplicitTop = 137
      end
      inherited sFiltroLBL: TLabel
        Left = 65
        Top = 186
        ExplicitLeft = 65
        ExplicitTop = 186
      end
      inherited Seleccionar: TcxButton
        Left = 168
        Top = 258
        Caption = ' Verificar lista de empleados'
        ParentFont = False
        ExplicitLeft = 168
        ExplicitTop = 258
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 92
        Top = 134
        ExplicitLeft = 92
        ExplicitTop = 134
      end
      inherited sFiltro: TcxMemo
        Left = 92
        Top = 165
        Style.IsFontAssigned = True
        ExplicitLeft = 92
        ExplicitTop = 165
      end
      inherited GBRango: TGroupBox
        Left = 92
        Top = 4
        ExplicitLeft = 92
        ExplicitTop = 4
      end
      inherited bAjusteISR: TcxButton
        Left = 407
        Top = 166
        ExplicitLeft = 407
        ExplicitTop = 166
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    Left = 432
    Top = 282
  end
end
