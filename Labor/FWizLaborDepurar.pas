unit FWizLaborDepurar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, ComCtrls, Buttons, StdCtrls, ExtCtrls,
     ZcxBaseWizard,
     ZetaFecha,
     ZetaWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizLaborDepurar = class(TcxBaseWizard)
    BorrarDatosGB: TGroupBox;
    FechaLBL: TLabel;
    CBKardex: TCheckBox;
    Fecha: TZetaFecha;
    CBCedulas: TCheckBox;
    CBCedInspeccion: TCheckBox;
    cbCedScrap: TCheckBox;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CBCedulasClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizLaborDepurar: TWizLaborDepurar;

implementation

uses DLabor,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizLaborDepurar.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H00005_Depuracion;
     ParametrosControl := Fecha;
     Fecha.Valor := Date - 365;
     CBCedulas.Checked := TRUE;
     CBKardex.Checked := TRUE;
end;

procedure TWizLaborDepurar.CargaParametros;
begin
  inherited;
  with ParameterList do
  begin
    AddDate( 'Fecha', Fecha.Valor );
    AddBoolean( 'BorrarKardex', CBKardex.Checked );
    AddBoolean( 'BorrarCedulas', CBCedulas.Checked );
    AddBoolean( 'BorrarCedulasInspeccion', CBCedInspeccion.Checked );
    AddBoolean( 'BorrarCedulasScrap', CBCedScrap.Checked );
    AddBoolean( 'BorrarLecturas', False );
  end;
  with Descripciones do
  begin
    AddDate( 'Fecha', Fecha.Valor );
    AddBoolean( 'Kardex de operaciones', CBKardex.Checked );
    AddBoolean( 'C�dulas de producci�n', CBCedulas.Checked );
    AddBoolean( 'C�dulas de inspecci�n', CBCedInspeccion.Checked );
    AddBoolean( 'C�dulas de scrap', CBCedScrap.Checked );
  end;
end;

function TWizLaborDepurar.EjecutarWizard: Boolean;
begin
     Result := dmLabor.Depuracion( ParameterList );
end;

procedure TWizLaborDepurar.CBCedulasClick(Sender: TObject);
begin
     inherited;
     if CBCedulas.Checked and ( not CBKardex.Checked ) then
        CBKardex.Checked := TRUE;
end;

end.
