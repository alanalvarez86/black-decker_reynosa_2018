inherited EditarTiempos: TEditarTiempos
  ActiveControl = OP_NUMBER
  Caption = 'Datos De La Actividad'
  ClientHeight = 148
  ClientWidth = 397
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object OrdenLBL: TLabel [0]
    Left = 56
    Top = 32
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Orden:'
  end
  object ParteLBL: TLabel [1]
    Left = 60
    Top = 55
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'Parte:'
  end
  object OperacionLBL: TLabel [2]
    Left = 36
    Top = 9
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Operación:'
  end
  object HorasLBL: TLabel [3]
    Left = 57
    Top = 77
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Horas:'
  end
  inherited PanelBotones: TPanel
    Top = 112
    Width = 397
    BevelOuter = bvNone
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 229
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 314
      OnClick = CancelarClick
    end
  end
  object WO_NUMBER: TZetaDBKeyLookup
    Left = 91
    Top = 28
    Width = 300
    Height = 21
    Filtro = 'WO_STATUS=0'
    LookupDataset = dmCliente.cdsWOrderLookup
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    OnValidKey = WO_NUMBERValidKey
    DataField = 'WO_NUMBER'
    DataSource = DataSource
  end
  object Horas: TZetaNumero
    Left = 91
    Top = 74
    Width = 59
    Height = 21
    Mascara = mnHoras
    TabOrder = 3
    Text = '0.00'
  end
  object AR_CODIGO: TZetaDBKeyLookup
    Left = 91
    Top = 51
    Width = 300
    Height = 22
    LookupDataset = dmCliente.cdsPartes
    TabOrder = 2
    TabStop = True
    WidthLlave = 60
    DataField = 'AR_CODIGO'
    DataSource = DataSource
  end
  object OP_NUMBER: TZetaDBKeyLookup
    Left = 91
    Top = 5
    Width = 300
    Height = 22
    LookupDataset = dmCliente.cdsOpera
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'OP_NUMBER'
    DataSource = DataSource
  end
  object DataSource: TDataSource
    Left = 208
    Top = 48
  end
end
