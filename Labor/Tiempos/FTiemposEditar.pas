unit FTiemposEditar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal, Db, Mask, ZetaNumero, ZetaKeyLookup;

type
  TEditarTiempos = class(TZetaDlgModal)
    OrdenLBL: TLabel;
    WO_NUMBER: TZetaDBKeyLookup;
    Horas: TZetaNumero;
    DataSource: TDataSource;
    ParteLBL: TLabel;
    AR_CODIGO: TZetaDBKeyLookup;
    OperacionLBL: TLabel;
    OP_NUMBER: TZetaDBKeyLookup;
    HorasLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Conectar;
    procedure Desconectar;
  end;

var
  EditarTiempos: TEditarTiempos;

procedure ShowFormaEdicion;

implementation

uses ZetaDialogo,
     DCliente;

procedure ShowFormaEdicion;
begin
     if not Assigned( EditarTiempos ) then
        EditarTiempos := TEditarTiempos.Create( Application );
     with EditarTiempos do
     begin
          Conectar;
          ShowModal;
          Desconectar;
     end;
end;

{$R *.DFM}

procedure TEditarTiempos.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          OrdenLBL.Caption := LabelOrden;
          ParteLBL.Caption := LabelParte;
          OperacionLBL.Caption := LabelOperacion;
     end;
end;

procedure TEditarTiempos.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := OP_NUMBER;
end;

procedure TEditarTiempos.Conectar;
begin
     with dmCliente do
     begin
          with Datasource do
          begin
               Dataset := cdsTiempos;
          end;
          Horas.Valor := cdsTiempos.FieldByName( 'WK_HORAS' ).AsFloat;
     end;
end;

procedure TEditarTiempos.Desconectar;
begin
     with Datasource do
     begin
          Dataset := nil;
     end;
end;

procedure TEditarTiempos.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     with AR_CODIGO do
     begin
          if ( Llave = '' ) and ( WO_NUMBER.Llave <> '' ) then
          begin
               with dmCliente do
               begin
                    if ( Sistema <> '' ) then
                    begin
                         Llave := Sistema;
                         ActiveControl := Self.Horas;
                    end;
               end;
          end;
     end;
end;

procedure TEditarTiempos.OKClick(Sender: TObject);
var
   rMinutos: Extended;
begin
     inherited;
     rMinutos := 60 * Horas.Valor;
     if ( rMinutos > 0 ) then
     begin
          with DCliente.dmCliente.cdsTiempos do
          begin
               FieldByName( 'WK_TIEMPO' ).AsFloat := rMinutos;
               Post;
          end;
          ModalResult := mrOk;
     end
     else
     begin
          ZetaDialogo.zError( Caption, 'Horas No Pueden Ser Cero', 0 );
          ActiveControl := Horas;
     end;
end;

procedure TEditarTiempos.CancelarClick(Sender: TObject);
begin
     inherited;
     DCliente.dmCliente.cdsTiempos.Cancel;
end;

end.
