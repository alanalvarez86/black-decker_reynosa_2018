program Tiempos;

uses
  Forms,
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FTiempos in 'FTiempos.pas' {TimeCount},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DBaseCliente in '..\..\DataModules\DBaseCliente.pas' {BaseCliente: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Captura de Tiempos';
  Application.CreateForm(TTimeCount, TimeCount);
  Application.Run;
end.
