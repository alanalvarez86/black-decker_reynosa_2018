unit FEditMultiLotes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Mask, StdCtrls, ComCtrls, ExtCtrls, DBCtrls, Buttons, Db, Grids, DBGrids,
     {$ifndef VER130}Variants,{$endif}     
     ZBaseEdicionRenglon_DevEx,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaEdit,
     ZetaDBGrid,
     ZetaSmartLists,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaHora,
     ZetaDBTextBox, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxPC, cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMultiLotes = class(TBaseEdicionRenglon_DevEx)
    Label6: TLabel;
    WK_HORA_R: TZetaDBHora;
    WK_STATUS: TZetaDBKeyCombo;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    OP_NUMBER: TZetaDBKeyLookup_DevEx;
    CB_AREA: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    Label23: TLabel;
    lbArea: TLabel;
    lbOperacion: TLabel;
    lbParte: TLabel;
    Label4: TLabel;
    Moduladores: TcxTabSheet;
    lbModula1: TLabel;
    WK_MOD_1: TZetaDBKeyLookup_DevEx;
    WK_MOD_2: TZetaDBKeyLookup_DevEx;
    WK_MOD_3: TZetaDBKeyLookup_DevEx;
    lbModula3: TLabel;
    lbModula2: TLabel;
    TabSheet2: TcxTabSheet;
    GroupBox3: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    WK_TIEMPO: TZetaDBNumero;
    WK_HRS_ORD: TZetaDBNumero;
    WK_HRS_2EX: TZetaDBNumero;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    WK_FECHA_R: TZetaDBFecha;
    WK_LINX_ID: TZetaDBEdit;
    WK_MANUAL: TDBCheckBox;
    WK_HORA_A: TZetaDBHora;
    lbTiempoMuerto: TLabel;
    WK_TMUERTO: TZetaDBKeyLookup_DevEx;
    Label1: TLabel;
    WK_TIPO: TZetaDBTextBox;
    Label2: TLabel;
    WK_HRS_3EX: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    function AgregarMultiLote : Boolean;
    procedure SetBuscaBtn;
  protected
    { Protected declarations }
    procedure KeyDown( var Key: Word; Shift: TShiftState );override;
    procedure HabilitaControles;override;
  public
    { Public declarations }
    procedure Connect;override;
    procedure Buscar;
  end;

var
  EditMultiLotes: TEditMultiLotes;

implementation

uses DLabor,
     DGlobal,
     DCatalogos,
     ZGlobalTress,
     ZetaDialogo,
     ZetaLaborTools,
     ZetaBuscador_DevEx,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZAccesosTress,
     FTressShell;

{$R *.DFM}

procedure TEditMultiLotes.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_LAB_REG_MULTILOTE;
     HelpContext := H9508_Multilote;
     FirstControl := WK_HORA_A;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stFecha;
     Moduladores.TabVisible := StrLleno( lbModula1.Caption );

     AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CB_AREA );
     AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbParte, AR_CODIGO );
     AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOperacion, OP_NUMBER );
     AsignaGlobal( K_GLOBAL_LABOR_TMUERTO, lbTiempoMuerto, WK_TMUERTO );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA1, lbModula1, WK_MOD_1 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA2, lbModula2, WK_MOD_2 );
     AsignaGlobal( K_GLOBAL_LABOR_MODULA3, lbModula3, WK_MOD_3 );
     AsignaGlobalColumn( K_GLOBAL_LABOR_ORDEN,GridRenglones.Columns[0]);

     AR_CODIGO.LookupDataset := dmLabor.cdsPartes;
     OP_NUMBER.LookupDataset := dmLabor.cdsOpera;
     CB_AREA.LookupDataset := dmLabor.cdsArea;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     WK_TMUERTO.LookupDataset := dmLabor.cdsTiempoMuerto;
     WK_MOD_1.LookupDataset := dmLabor.cdsModula1;
     WK_MOD_2.LookupDataset := dmLabor.cdsModula2;
     WK_MOD_3.LookupDataset := dmLabor.cdsModula3;

     //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditMultiLotes.Connect;
begin
     dmCatalogos.cdsPuestos.Conectar;
     with dmLabor do
     begin
          cdsArea.Conectar;
          cdsOpera.Conectar;
          cdsModula1.Conectar;
          cdsModula2.Conectar;
          cdsModula3.Conectar;
          cdsTiempoMuerto.Conectar;
          cdsWorks.Conectar;
          DataSource.DataSet := cdsWorks;
          dsRenglon.DataSet := cdsMultiLote;
          with cdsMultiLote do
          begin
               Active := FALSE;
               CreateDataSet;
               Active := TRUE;
          end;
          lbTiempoMuerto.Enabled := eTipoLectura(cdsWorks.FieldByName('WK_TIPO').AsInteger)=wtLibre;
          WK_TMUERTO.Enabled := lbTiempoMuerto.Enabled;
     end;
end;

procedure TEditMultiLotes.dxBarButton_BuscarBtnClick(Sender: TObject);
begin
  inherited;
  Buscar;
end;

procedure TEditMultiLotes.Buscar;
var
   sLlave, sDescripcion: string;
begin
     if TressShell.BuscaDialogo( enWorder, '', sLlave, sDescripcion ) then
     begin
          with dmLabor.cdsMultiLote do
          begin
               if State = dsBrowse then
                  Edit;
               FieldByName('WO_NUMBER').AsString := sLlave;
               FieldByName('WO_DESCRIP').AsString := sDescripcion;
          end;
     end;
end;

procedure TEditMultiLotes.PageControlChange(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
end;

procedure TEditMultiLotes.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if ( Key <> 0 ) AND
        ( ssCtrl in Shift )  AND { CTRL }
        ( Key = 66 )  then { Letra B = Buscar }
     begin
          Key := 0;
          Buscar;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TEditMultiLotes.HabilitaControles;
begin
     inherited HabilitaControles;
     SetBuscaBtn;
end;

procedure TEditMultiLotes.SetBuscaBtn;
begin
     //BuscarBtn.Enabled := PageControl.ActivePage = Tabla;
end;

procedure TEditMultiLotes.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;

function TEditMultiLotes.AgregarMultiLote: Boolean;
var
   i,j: Integer;
   vWorks: OleVariant;
begin
     j := 2;
     with dmLabor, cdsWorks do
     begin
          DisableControls;
          if cdsMultiLote.Active AND NOT cdsMultiLote.IsEmpty then
          begin
               cdsMultiLote.DisableControls;
               cdsMultiLote.First;
               Edit;
               FieldByName( 'WK_FOLIO' ).AsInteger := 1;
               FieldByName( 'WK_HORA_A' ).AsString := FieldByName( 'WK_HORA_R' ).AsString;
               FieldByName( 'WO_NUMBER' ).AsString := cdsMultiLote.Fields[ 0 ].AsString;
               Post;
               cdsMultiLote.Next;
               if not cdsMultiLote.Eof then
               begin
                    vWorks := VarArrayCreate( [ 0, Fieldcount - 1 ], varVariant );
                    for i := 0 to ( FieldCount - 1 ) do
                    begin
                         vWorks[ i ] := Fields[ i ].AsVariant;
                    end;
                    while not cdsMultiLote.Eof do
                    begin
                         Append;
                         for i := 0 to ( FieldCount - 1 ) do
                         begin
                              Fields[ i ].AsVariant := vWorks[ i ];
                         end;
                         //Pendiente en lo que se manda el MultiLote
                         FieldByName( 'WK_FOLIO' ).AsInteger := j;
                         FieldByName( 'WO_NUMBER' ).AsString := cdsMultiLote.Fields[ 0 ].AsString;
                         Post;
                         cdsMultiLote.Next;
                         Inc( j );
                    end;
               end;
               Result := TRUE;
               cdsMultiLote.EnableControls;
          end
          else
          begin
               Result := FALSE;
               ZetaDialogo.ZError(Caption, 'No se ha capturado ningun dato en: ' + Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN ), 0 );
               PageControl.ActivePage := Tabla;
          end;
          EnableControls;
     end;
end;

procedure TEditMultiLotes.OKClick(Sender: TObject);
begin
     if AgregarMultiLote then
     begin
   //       inherited OKClick(Sender);
          Close;
     end;
end;

procedure TEditMultiLotes.OK_DevExClick(Sender: TObject);
begin
  if AgregarMultiLote then
     begin
          inherited OK_DevExClick(Sender);
          Close;
     end;

end;

procedure TEditMultiLotes.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Datos;
end;

procedure TEditMultiLotes.GridRenglonesEnter(Sender: TObject);
begin
  inherited;
  dxBarButton_BuscarBtn.Enabled := true;
end;

procedure TEditMultiLotes.GridRenglonesExit(Sender: TObject);
begin
  inherited;
  dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TEditMultiLotes.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
end;

end.

