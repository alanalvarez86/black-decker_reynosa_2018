unit FCedulasTMuerto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FCedulasEspeciales, Db, StdCtrls, ZetaKeyCombo, Grids, DBGrids,
  ZetaDBGrid, ComCtrls, ZetaFecha, ZetaNumero, Mask, ZetaHora,
  ZetaKeyLookup, ExtCtrls, DBCtrls, Buttons, ZetaEdit, ImgList,
  ZetaDBTextBox, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ZetaKeyLookup_DevEx, cxPC, cxNavigator,
  cxDBNavigator, cxButtons;

type
  TCedulasTMuerto = class(TCedulasEspeciales)
    PanelMotivo: TPanel;
    LblMotivo: TLabel;
    CE_TMUERTO: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure InitLookupsDataSet; override;
    procedure Connect; override;
    procedure SetControlesVisibles; override;
    procedure SetControlesHabilitados; override;
    procedure ValidaDatosLabor; override;
  public
    { Public declarations }
  end;

var
  CedulasTMuerto: TCedulasTMuerto;

implementation

uses dLabor, dGlobal, ZetaLaborTools, ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.DFM}

procedure TCedulasTMuerto.FormCreate(Sender: TObject);
begin
     inherited;
     CedulaCaption := 'Tiempo Muerto';
end;

procedure TCedulasTMuerto.FormShow(Sender: TObject);
var
   sTMuerto: string;

begin
     inherited;
      { MV: Se les cambia el Tag a UNO para poderlos dejar vacios y no Sean checados como los otros controles }
     WO_NUMBER.Tag := ZetaLaborTools.K_LABOR_NO_VALIDAR_LOOKUP;
     AR_CODIGO.Tag := ZetaLaborTools.K_LABOR_NO_VALIDAR_LOOKUP;
     sTMuerto := GetLaborLabel( K_GLOBAL_LABOR_TIPO_TMUERTO );
     if ( sTMuerto <> ZetaCommonClasses.VACIO ) then
        CE_TMUERTO.Filtro := 'TB_CODIGO <> ' + EntreComillas( sTMuerto );
     dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TCedulasTMuerto.GridRenglonesEnter(Sender: TObject);
begin
  inherited;
  dxBarButton_BuscarBtn.Enabled := true;
end;

procedure TCedulasTMuerto.GridRenglonesExit(Sender: TObject);
begin
  inherited;
    dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TCedulasTMuerto.Connect;
begin
     inherited;
     dmLabor.cdsTiempoMuerto.Conectar;
end;

procedure TCedulasTMuerto.InitLookupsDataSet;
begin
     inherited;
     CE_TMUERTO.LookupDataSet := dmLabor.cdsTiempoMuerto;
end;

procedure TCedulasTMuerto.SetControlesVisibles;
begin
     inherited;
     ZetaLaborTools.SetPanelVisible( PanelOpera, FALSE );
     //PanelOpera.Visible := FALSE;
     LblCantidad.Visible:= FALSE;
     CE_PIEZAS.Visible  := FALSE;
end;

procedure TCedulasTMuerto.SetControlesHabilitados;
begin
     inherited;
     HabilitaPanel( PanelMotivo, TieneDerechoGenerales );
end;

procedure TCedulasTMuerto.ValidaDatosLabor;
begin
     inherited;
     ValidaControlLookup( CE_TMUERTO, LblMotivo );
end;

end.
