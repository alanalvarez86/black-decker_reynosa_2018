inherited EditKardexArea_DevEx: TEditKardexArea_DevEx
  Left = 526
  Top = 238
  Caption = 'Kardex de Areas'
  ClientHeight = 208
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 66
    Top = 67
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object Label6: TLabel [1]
    Left = 48
    Top = 93
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora Real:'
  end
  object lbArea: TLabel [2]
    Left = 74
    Top = 120
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Area:'
  end
  object Label2: TLabel [3]
    Left = 13
    Top = 146
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de Cambio:'
  end
  object Label3: TLabel [4]
    Left = 310
    Top = 146
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora de Cambio:'
  end
  object ZetaDBTextBox1: TZetaDBTextBox [5]
    Left = 104
    Top = 144
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'KA_FEC_MOV'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object ZetaDBTextBox2: TZetaDBTextBox [6]
    Left = 395
    Top = 144
    Width = 59
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox2'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'KA_HOR_MOV'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 172
    TabOrder = 5
  end
  inherited PanelIdentifica: TPanel
    TabOrder = 0
  end
  object KA_FECHA: TZetaDBFecha [10]
    Left = 104
    Top = 62
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '10/Dec/97'
    Valor = 35774.000000000000000000
    DataField = 'KA_FECHA'
    DataSource = DataSource
  end
  object KA_HORA: TZetaDBHora [11]
    Left = 104
    Top = 89
    Width = 50
    Height = 21
    EditMask = '99:99;0'
    TabOrder = 2
    Text = '    '
    Tope = 24
    Valor = '    '
    DataField = 'KA_HORA'
    DataSource = DataSource
  end
  object CB_AREA: TZetaDBKeyLookup_DevEx [12]
    Left = 104
    Top = 116
    Width = 350
    Height = 21
    LookupDataset = dmLabor.cdsArea
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 100
    DataField = 'CB_AREA'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 388
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
