unit FEditTPartes;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Mask, DBCtrls, StdCtrls, ExtCtrls, Buttons,
     ZBaseTablas_DevEx,
     ZetaEdit, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, ZetaNumero, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditTPartes = class(TEditTablas_DevEx)
  private
    { Private declarations }
  protected
    procedure AfterCreate; override;
  public
    { Public declarations }
    procedure DoLookup;override;
  end;

var
  EditTPartes: TEditTPartes;

implementation

uses DLabor,
     DGlobal,
     ZGlobalTress,
     ZetaCommonClasses,
     ZAccesosTress;
     //taBuscador;

{$R *.DFM}

procedure TEditTPartes.AfterCreate;
begin
     inherited AfterCreate;
     Caption := dmLabor.cdsTParte.LookupName;
     ZetaDataset := dmLabor.cdsTParte;
     HelpContext:= H9512_Clasificacion_de_partes;
     IndexDerechos := ZAccesosTress.D_LAB_CAT_TIPO_PARTE;
end;

procedure TEditTPartes.DoLookup;
begin
     inherited;
     //ZetaBuscador.BuscarCodigo( 'C�digo', Caption, 'TT_CODIGO', dmLabor.cdsTParte );
end;

end.
