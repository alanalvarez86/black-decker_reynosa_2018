unit FEditTablas;

interface

uses
  ZBaseEdicion_DevEx,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaKeyLookup, Mask, Db,
  ExtCtrls, Buttons, ZetaKeyCombo, ZetaEdit, ZetaClientDataSet, ZetaNumero,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
    TEditTablas = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    fZetalookupDataset : TZetaLookupDataSet;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
    procedure AfterCreate; virtual;
    property ZetaLookupDataset : TZetaLookupDataSet read fZetaLookupDataset write fZetaLookupDataset;
  public
  end;

type
  TTOEditModula1 = class( TEditTablas )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditModula2 = class( TEditTablas )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TTOEditModula3 = class( TEditTablas )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
    TTOEditTDefecto = class ( TEditTablas )
      protected
      Procedure AfterCreate;override;
    end;

type
   TEditMotivoScrap = class ( TEditTablas )
    protected
    Procedure AfterCreate;override;
  end;

TTOEditTiempoMuerto = class( TEditTablas )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;


const
     K_MAX_LENGTH_DESCRIP = 30;

var
   EditTDefecto : TTOEditTDefecto;
   EditMotivoScrap: TEditMotivoScrap;
   EditTiempoMuerto: TTOEditTiempoMuerto;
   EditModula1: TTOEditModula1;
   EditModula2: TTOEditModula2;
   EditModula3: TTOEditModula3;

implementation

uses DLabor,
     ZetaCommonClasses,
     ZAccesosTress,
     ZetaBuscador_DevEx,
     ZetaCommonLists;

{$R *.DFM}

procedure TEditTablas.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     AfterCreate;
     TB_ELEMENT.MaxLength := K_MAX_LENGTH_DESCRIP;
     TB_INGLES.MaxLength := K_MAX_LENGTH_DESCRIP;
     TB_TEXTO.MaxLength := K_MAX_LENGTH_DESCRIP;
end;


procedure TEditTablas.AfterCreate;
begin
end;

procedure TTOEditTiempoMuerto.AfterCreate;
begin
   inherited;
   ZetaLookupDataset := dmLabor.cdsTiempoMuerto;
   HelpContext:= H9519_TiempoMuerto;
   IndexDerechos := ZAccesosTress.D_LAB_CAT_TIEMPO_MUERTO;
end;

procedure TTOEditModula1.AfterCreate;
begin
   inherited AfterCreate;
   ZetaLookupDataset := dmLabor.cdsModula1;
   HelpContext:= H9515_Moduladores;
   IndexDerechos := ZAccesosTress.D_LAB_CAT_MODULA_1;
end;

procedure TTOEditModula2.AfterCreate;
begin
   inherited AfterCreate;
   ZetaLookupDataset := dmLabor.cdsModula2;
   HelpContext:= H9515_Moduladores;
   IndexDerechos := ZAccesosTress.D_LAB_CAT_MODULA_2;
end;

procedure TTOEditModula3.AfterCreate;
begin
   inherited AfterCreate;
   ZetaLookupDataset := dmLabor.cdsModula3;
   HelpContext:= H9515_Moduladores;
   IndexDerechos := ZAccesosTress.D_LAB_CAT_MODULA_3;
end;

procedure TEditTablas.Connect;
begin
     Caption := ZetaLookupDataset.LookupName;
     DataSource.DataSet := ZetaLookupDataset;
end;

procedure TEditTablas.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'CB_CODIGO', ZetaLookupdataset );
end;

procedure TTOEditTDefecto.AfterCreate;
begin
     inherited;
     ZetaLookupDataset := dmLabor.cdsTDefecto;
     HelpContext:= H00053_Clasificacion_de_Defectos;
     IndexDerechos := ZAccesosTress.D_LAB_CAT_TDEFECTO;
end;
procedure TEditMotivoScrap.AfterCreate;
begin
     inherited AfterCreate;
     ZetaLookupDataset := dmLabor.cdsMotivoScrap;
     HelpContext:= H00057_Motivos_de_scrap;
     IndexDerechos := ZAccesosTress.D_LAB_CAT_MOT_SCRAP;
end;

end.
