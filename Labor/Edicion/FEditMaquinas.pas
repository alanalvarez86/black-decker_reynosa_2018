unit FEditMaquinas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, ComCtrls, Db,
     ExtCtrls, Buttons, DBCtrls, StdCtrls,
     ZetaClientDataset,
     ZBaseEdicion_DevEx,
     ZetaBuscador_DevEx,
     DCatalogos,
     ZetaDBGrid,
     ZetaMessages, ZetaSmartLists, ExtDlgs, ZetaKeyLookup, ZetaNumero,
  Mask, ZetaEdit, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, ZetaKeyLookup_DevEx,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, imageenview, ieview,
  ieopensavedlg; //:TodoTDMULTIP;

type
  TEditMaquinas = class(TBaseEdicion_DevEx)
    PageControl: TcxPageControl;
    Datos: TcxTabSheet;
    Tabla: TcxTabSheet;
    GridRenglones: TZetaDBGrid;
    dsRenglon: TDataSource;
    Panel2: TPanel;
    BBAgregar: TcxButton;
    BBBorrar: TcxButton;
    BBModificar: TcxButton;
    MQ_CODIGO: TZetaDBEdit;
    DBCodigoLBL: TLabel;
    DBDescripcionLBL: TLabel;
    MQ_NOMBRE: TDBEdit;
    MQ_INGLES: TDBEdit;
    DBInglesLBL: TLabel;
    Label2: TLabel;
    MQ_NUMERO: TZetaDBNumero;
    MQ_TEXTO: TDBEdit;
    Label1: TLabel;
    Label3: TLabel;
    MQ_TMAQUIN: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    OpenPictureDialog1: TOpenPictureDialog;
    MQ_RESUMEN: TcxDBMemo;
    btnBuscarEmp: TSpeedButton;
    btnSelArchivo: TcxButton;
    MQ_MULTIP: TDBCheckBox;
    FOTO: TImageEnView;
    OpenImageEnDialog: TOpenImageEnDialog;
    SaveImageEnDialog: TSaveImageEnDialog;
    //:Todo IMAGEN: TPDBMultiImage;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure GridRenglonesTitleClick(Column: TColumn);
    procedure MQ_IMAGENDblClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnBuscarEmpClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    LimpiarImagen: Boolean;
    FPrimerColumna: Integer;
    FInsertando:Boolean;
    procedure HabilitaCertificaciones(const lHabilita:Boolean);
    procedure GrabaImagen;

  protected
    { Protected declarations }
    property PrimerColumna: Integer read FPrimerColumna write FPrimerColumna;
    function ClientDatasetHijo: TZetaClientDataset;
    function GridEnfocado: Boolean;
    function CheckDerechosPadre: Boolean;
    function PosicionaSiguienteColumna: Integer; Virtual;
     procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure HabilitaControles; override;
    procedure FocusFirstControl; override;
    procedure DoLookup; override;
    procedure DoCancelChanges; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure SeleccionaPrimerColumna;
    procedure Setok;

  public
    { Public declarations }
  end;

var
  EditMaquinas: TEditMaquinas;
  FEditImage: Boolean;
implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaClientTools,
     DLabor, //:Todo FToolsFoto,
     DCliente,
     FToolsImageEn;

{$R *.DFM}

procedure TEditMaquinas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_LAB_CAT_MAQUINAS;
     HelpContext:= H_LAB_CAT_EDIT_MAQUINAS;
      FirstControl := MQ_CODIGO;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs,
                                dgCancelOnExit ];
     FPrimerColumna := 0;
     with dmLabor do
     begin
          MQ_TMAQUIN.LookupDataset := cdsTMaquinas;
     end;

     FEditImage:= False;
end;

procedure TEditMaquinas.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Datos;
     FirstControl := MQ_CODIGO;
     FInsertando:= FALSE;
     HabilitaControles;
end;

procedure TEditMaquinas.HabilitaControles;
begin
     inherited HabilitaControles;
     { Impide que usuarios sin derechos de Modificación }
     { puedan llegar a modo de Edición simplemente cambiando }
     { el contenido de un control }
     dsRenglon.AutoEdit := Editing or CheckDerechos( K_DERECHO_CAMBIO );
		 HabilitaCertificaciones ( not( InsertInicial OR FInsertando ) );
end;


procedure TEditMaquinas.HabilitaCertificaciones(const lHabilita:Boolean);
begin
     BBAgregar.Enabled := lHabilita;
     BBBorrar.Enabled := lHabilita;
     BBModificar.Enabled := lHabilita;
     GridRenglones.Enabled := lHabilita;   
     PageControl.Pages[1].TabVisible := lHabilita;
end;

procedure TEditMaquinas.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if GridEnfocado and not ( ClientDatasetHijo.State in [ dsInsert, dsEdit ] ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                   end;
              end;
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        66:   { F }
                        begin
                             Key := 0;
                             btnBuscarEmpClick(Self);
                        end;
                   end;
              end;
     end;
     inherited KeyDown( Key, Shift );
end;

function TEditMaquinas.ClientDatasetHijo: TZetaClientDataset;
begin
     Result := TZetaClientDataset( dsRenglon.Dataset );
end;

function TEditMaquinas.CheckDerechosPadre: Boolean;
var
   sMensaje: String;
begin
     if Editing then
        Result := True
     else
         if PuedeModificar( sMensaje ) then
            Result := True
         else
         begin
              ZetaDialogo.zInformation( Caption, sMensaje, 0 );
              Result := False;
         end;
end;

procedure TEditMaquinas.GrabaImagen;
var
   sMsgError: String;
begin
    if ( not FToolsImageEn.ResizeImagen( FOTO, sMsgError ) ) then
       ZError( self.Caption, sMsgError, 0);
    FToolsImageEn.AsignaImagenABlob( FOTO, dmLabor.cdsMaquinas, 'MQ_IMAGEN' );
end;

function TEditMaquinas.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = GridRenglones );
end;

function TEditMaquinas.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
             Result := FPrimerColumna
          else
              Result := i;
     end;

end;

procedure TEditMaquinas.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ FPrimerColumna ].Field;
end;

procedure TEditMaquinas.Setok;
begin
     {if OK.Enabled then
        OK.SetFocus         //:Todo
     else
         Cancelar.SetFocus;}
end;

procedure TEditMaquinas.Agregar;
begin
     FInsertando:= True;
     if GridEnfocado then
        BBAgregar.Click
     else
         inherited Agregar;

end;

procedure TEditMaquinas.BBAgregarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               Append;
               Agregar;
          end;
          GridRenglones.SetFocus;
     end;

end;

procedure TEditMaquinas.Borrar;
begin
     if GridEnfocado then
        BBBorrar.Click
     else
         inherited Borrar;
end;

procedure TEditMaquinas.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               if not IsEmpty then
               begin
                    Delete;
                    Borrar;
               end;
          end;
     end;
end;

procedure TEditMaquinas.Modificar;
begin
     if GridEnfocado then
        BBModificar.Click
     else
         inherited Modificar;
end;

procedure TEditMaquinas.BBModificarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               if not IsEmpty then
               begin
                    Edit;
                    Modificar;
               end;
          end;
          GridRenglones.SetFocus;
     end;
end;

procedure TEditMaquinas.GridRenglonesTitleClick(Column: TColumn);
begin
     inherited;
     { QUEDA VACIA HASTA QUE ENCONTREMOS UNA SOLUCION }
     { PARA QUE NO APAREZCAN TODO EL HIJO }
end;

procedure TEditMaquinas.Connect;
begin
     with dmLabor do
     begin
          cdsMaqCertific.Conectar;
          DataSource.DataSet := cdsMaquinas;
          dsRenglon.DataSet := cdsMaqCertific;
     end;
end;

procedure TEditMaquinas.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( dmLabor.GetMaquinaGlobal, dmLabor.GetMaquinaGlobal, 'MQ_CODIGO', dmLabor.cdsMaquinas );
end;

procedure TEditMaquinas.FocusFirstControl;
begin
     PageControl.ActivePage :=  Datos;
     inherited FocusFirstControl;
end;

procedure TEditMaquinas.MQ_IMAGENDblClick(Sender: TObject);
begin
     inherited;
     {if Open.Execute then
     begin
          with dmLabor.cdsMaquinas do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
          //:Todo IMAGEN.LoadFromFile(OpenPictureDialog1.FileName  );

          FEditImage:= True;
     end; }
     if OpenImageEnDialog.Execute then
     begin
          // FOTO.Clear;
          FOTO.IO.LoadFromFile( OpenImageEnDialog.FileName );
          GrabaImagen;
     end;
end;

procedure TEditMaquinas.OKClick(Sender: TObject);
begin
     if ( FEditImage ) then
     begin
          {with IMAGEN do
          begin
               CutToClipboard;
               UpdateAsJPG:=True;
               PastefromClipboard;                          //:Todo
          end;
          if (IMAGEN.Picture.Width  > 300) OR (IMAGEN.Picture.Height  > 300) then
          begin
               FToolsFoto.ResizeImageBestFit( IMAGEN, 300, 300 );
               // FToolsFoto.ResizeImageKeepProportion ( MQ_IMAGEN, 300, false,MQ_IMAGEN.Picture.Width,MQ_IMAGEN.Picture.Height);
          end; }
          FEditImage:= False;
     end;
     inherited;
     dmLabor.cdsMaqCertific.Enviar;
     FInsertando := False;
     HabilitaControles; 
end;

procedure TEditMaquinas.OK_DevExClick(Sender: TObject);
begin
     inherited;
     dmLabor.cdsMaqCertific.Enviar;
     FInsertando := False;
     HabilitaControles;

end;

procedure TEditMaquinas.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'CI_CODIGO' ) and
                  ( btnBuscarEmp.Visible ) then
                  btnBuscarEmp.Visible:= False;
          end;
     end;
end;

procedure TEditMaquinas.GridRenglonesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if (( gdFocused in State ) and ( dmLabor.cdsMaqCertific.State in [dsEdit,dsInsert] ) )then
     begin
          if ( Column.FieldName = 'CI_CODIGO' ) then
          begin
               with btnBuscarEmp do
               begin
                    Left := Rect.Left + GridRenglones.Left + Column.Width - Width;
                    Top := Rect.Top;
                    Visible := True;
               end;
          end;
     end
     else if  (dmLabor.cdsMaqCertific.State in [dsBrowse ] )then
     begin
          btnBuscarEmp.Visible := False;
     end;
end;

procedure TEditMaquinas.btnBuscarEmpClick(Sender: TObject);
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Certificaciones', 'Certificaciones', 'CI_CODIGO', dmCatalogos.cdsCertificaciones );
     with dmLabor do
     begin
          cdsMaqCertific.Edit;
          cdsMaqCertific.FieldByName('CI_CODIGO').AsString := dmCatalogos.cdsCertificaciones.FieldByName('CI_CODIGO').AsString;
          cdsMaqCertific.FieldByName('MQ_CODIGO').AsString := cdsMaquinas.FieldByName('MQ_CODIGO').AsString;
          cdsMaqCertific.FieldByName('MC_FECHA').AsDateTime := Now;
          cdsMaqCertific.FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          cdsMaqCertific.Post;
     end;
end;

procedure TEditMaquinas.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     with dmLabor do
     begin
          if ( ( Field = nil ) and ( cdsMaquinas.State in [ dsBrowse, dsInactive,dsInsert ] ) and ( cdsMaqCertific.ChangeCount = 0 ) and ( cdsMaquinas.ChangeCount = 0 ) )then
               cdsMaqCertific.Refrescar;


     if (cdsMaquinas.State = dsInsert) and (LimpiarImagen) then
     begin
        FOTO.Clear;
        LimpiarImagen := FALSE;
     end
     else if ( Field = nil ) and ( cdsMaquinas.State = dsBrowse ) then     // Navegando
     begin
        FToolsImageEn.AsignaBlobAImagen( FOTO, cdsMaquinas, 'MQ_IMAGEN' );
        LimpiarImagen := TRUE;
     end;

     end;

end;

procedure TEditMaquinas.DoCancelChanges;
begin
     dmLabor.cdsMaqCertific.CancelUpdates;
     FInsertando:= FALSE;
     inherited DoCancelChanges;
end;


end.
