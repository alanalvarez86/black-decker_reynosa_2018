unit FEditCedScrap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicionRenglon_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaHora, Mask,
  ZetaFecha, ZetaKeyLookup, ComCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyCombo, ZetaDBTextBox, ZetaNumero, ZetaMessages,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxPC,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, dxBarBuiltInMenu;

type
  TEditCedScrap = class(TBaseEdicionRenglon_DevEx)
    PanelControles: TPanel;
    lbFecha: TLabel;
    CS_FECHA: TZetaDBFecha;
    Label2: TLabel;
    CI_FOLIO: TZetaDBTextBox;
    dsDefectos: TDataSource;
    lbHora: TLabel;
    CS_HORA: TZetaDBHora;
    WO_NUMBER: TZetaDBKeyLookup_DevEx;
    lbWorder: TLabel;
    AR_CODIGO: TZetaDBKeyLookup_DevEx;
    lbParte: TLabel;
    lbArea: TLabel;
    CS_AREA: TZetaDBKeyLookup_DevEx;
    CS_COMENTA: TDBEdit;
    lbComenta: TLabel;
    CS_TAMLOTE: TZetaDBNumero;
    lbTamLote: TLabel;
    CS_NUMERO1: TZetaDBNumero;
    CS_NUMERO2: TZetaDBNumero;
    CS_OBSERVA: TcxDBMemo;
    Label3: TLabel;
    lbNum2: TLabel;
    lbnum1: TLabel;
    Label4: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label1: TLabel;
    CS_FEC_FAB: TZetaDBFecha;
    lbOpera: TLabel;
    OP_NUMBER: TZetaDBKeyLookup_DevEx;
    Label5: TLabel;
    Label6: TLabel;
    CS_TEXTO1: TDBEdit;
    CS_TEXTO2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure GridRenglonesEnter(Sender: TObject);
    procedure WO_NUMBERValidKey(Sender: TObject);
    procedure GridRenglonesExit(Sender: TObject);
    //procedure CE_FOLIOValidKey(Sender: TObject);
  private
    { Private declarations }
    FPuedeModificar: Boolean;
    procedure AsignaColumnas;
    procedure SetBuscaBtn;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure SeleccionaSiguienteColumna;
    procedure HabilitaBusqueda(opc:boolean);
  protected
    procedure Connect; override;
    procedure DoLookup; override;
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function CheckDerechosPadre: Boolean;
    procedure KeyDown( var Key: Word; Shift: TShiftState );override;
    procedure KeyPress(var Key: Char);override;
    procedure EscribirCambios;override;
  public
    { Public declarations }
    procedure HabilitaControles;override;
  end;

var
  EditCedScrap: TEditCedScrap;

implementation

uses DLabor,
     DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaLaborTools,
     ZetaTipoEntidad,
     //ZAccesosMgr,
     ZetaDialogo,
     FTressShell ;

{$R *.DFM}

procedure TEditCedScrap.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs ];

     FirstControl := CS_FECHA;
     IndexDerechos := D_LAB_CEDULAS_SCRAP;
     HelpContext := H00055_Cedulas_de_scrap;

     with dmLabor do
     begin
          Ar_codigo.LookupDataset := cdsPartes;
          Cs_area.LookupDataset := cdsArea;
          Wo_number.LookupDataset := cdsWOrderLookup;
          Op_number.LookupDataset := cdsOpera;
     end;
end;


procedure TEditCedScrap.Connect;
begin
     with dmLabor do
     begin
          cdsOpera.Conectar;
          cdsArea.Conectar;
          cdsPartes.Conectar;

          cdsCedScrap.Refrescar;
          DataSource.DataSet := cdsCedScrap;
          dsRenglon.DataSet := cdsScrap;

          FPuedeModificar := PuedeModificarCedScrap;
     end;
end;

procedure TEditCedScrap.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Tabla;
     AsignaColumnas;
     dxBarButton_BuscarBtn.Visible:=ivAlways;
     HabilitaBusqueda(false);
     {$ifdef INTERRUPTORES}
      WO_NUMBER.WidthLlave := K_WIDTHLLAVE;
      AR_CODIGO.WidthLlave := K_WIDTHLLAVE;
      CS_AREA.WidthLlave := K_WIDTHLLAVE;
      OP_NUMBER.WidthLlave := K_WIDTHLLAVE;


      WO_NUMBER.Width := K_WIDTH_LOOKUP;
      AR_CODIGO.Width := K_WIDTH_LOOKUP;
      CS_AREA.Width := K_WIDTH_LOOKUP;
      OP_NUMBER.Width := K_WIDTH_LOOKUP;
      CS_COMENTA.Width := 475;

      EditCedScrap.Width := K_WIDTH_CEDULAS;
      {$endif}
end;

procedure TEditCedScrap.AsignaColumnas;
 {var lWorder : Boolean;}
begin
      AsignaGlobal( K_GLOBAL_LABOR_ORDEN, lbWorder, WO_NUMBER );
      AsignaGlobal( K_GLOBAL_LABOR_PARTE, lbparte, AR_CODIGO );
      AsignaGlobal( K_GLOBAL_LABOR_AREA, lbArea, CS_AREA );
      AsignaGlobal( K_GLOBAL_LABOR_OPERACION, lbOpera, OP_NUMBER );

      {lWorder := WO_NUMBER.Enabled;
      lbParte.Enabled := lbParte.Enabled AND NOT lWorder;
      AR_CODIGO.Enabled := AR_CODIGO.Enabled AND NOT lWorder;}
end;

procedure TEditCedScrap.DoLookup;
var
   sLlave, sDescripcion: String;
begin
     if ( ActiveControl = GridRenglones ) then
     begin
          with GridRenglones.SelectedField do
          begin
               if ( FieldName = 'CN_CODIGO' ) or ( FieldName = 'CN_NOMBRE' ) then
               begin
                    if TressShell.BuscaDialogo( enComponentes, '', sLlave, sDescripcion ) then
                    begin
                         with dmLabor.cdsScrap do
                         begin
                              if ( State = dsBrowse ) then
                                 Edit;
                              FieldByName( 'CN_CODIGO' ).AsString := sLlave;
                         end;
                    end;
               end;
               if ( FieldName = 'SC_MOTIVO' ) then
               begin
                    if TressShell.BuscaDialogo( enMotivoScrap, '', sLlave, sDescripcion ) then
                    begin
                         with dmLabor.cdsScrap do
                         begin
                              if ( State = dsBrowse ) then
                                 Edit;
                              FieldByName( 'SC_MOTIVO' ).AsString := sLlave;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TEditCedScrap.SetBuscaBtn;
begin
    //:Todo BuscarBtn.Enabled := (PageControl.ActivePage = Tabla) and ( ActiveControl = GridRenglones );
end;

procedure TEditCedScrap.PageControlChange(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
end;

procedure TEditCedScrap.HabilitaBusqueda(opc:boolean);
begin
  if opc = true then
     dxBarButton_BuscarBtn.Enabled := true
   else
     dxBarButton_BuscarBtn.Enabled := false;
end;

procedure TEditCedScrap.HabilitaControles;
begin
     inherited HabilitaControles;
    { DBNavigator.Visible := False;
     AgregarBtn.Visible := False;    //:Todo
     BorrarBtn.Visible := False;
     ModificarBtn.Visible := False; }
     SetBuscaBtn;
end;

procedure TEditCedScrap.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if ( Key <> 0 ) and
        ( ssCtrl in Shift ) and { CTRL }
        ( Key = 66 )  then      { Letra B = Buscar }
     begin
          Key := 0;
          DoLookup;
     end;
     inherited KeyDown( Key, Shift );
end;

function TEditCedScrap.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := False;
     if not Result then
     begin
          sMensaje := 'No se puede agregar el registro';
          Result := inherited PuedeAgregar( sMensaje );
     end;
end;

function TEditCedScrap.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := False;
     if not Result then
     begin
          sMensaje := 'No se puede borrar el registro';
          Result := inherited PuedeBorrar( sMensaje );
     end;
end;

function TEditCedScrap.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := False;
     if not Result then
     begin
          sMensaje := 'No se puede modificar el registro';
          Result := inherited PuedeModificar( sMensaje );
     end;
end;

procedure TEditCedScrap.BBAgregarClick(Sender: TObject);
begin
     with ClientDatasetHijo do
     begin
          Append;
     end;
     with GridRenglones do
     begin
          SelectedIndex := PrimerColumna;
          SetFocus;
     end;

end;

procedure TEditCedScrap.BBBorrarClick(Sender: TObject);
begin
     with ClientDatasetHijo do
     begin
          if not IsEmpty then
             Delete;
     end;
end;

function TEditCedScrap.CheckDerechosPadre: Boolean;
begin
     Result := TRUE;
end;


procedure TEditCedScrap.GridRenglonesEnter(Sender: TObject);
begin
     inherited;
     SetBuscaBtn;
     HabilitaBusqueda(true);
end;

procedure TEditCedScrap.GridRenglonesExit(Sender: TObject);
begin
  inherited;
  HabilitaBusqueda(false);
end;

procedure TEditCedScrap.WMExaminar(var Message: TMessage);
begin
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if GridEnfocado then
          begin
               with ClientDatasetHijo do
               begin
                    if StrVacio( FieldByName('CN_CODIGO').AsString ) then
                       SetOk
                    else
                    begin
                         SeleccionaSiguienteColumna;
                    end;
               end;
          end;
     end;
end;

procedure TEditCedScrap.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               SeleccionaSiguienteColumna;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   GridRenglones.selectedIndex := 0;
              end;
     end
     else
          inherited KeyPress( Key );
end;

procedure TEditCedScrap.SeleccionaSiguienteColumna;
begin
     with GridRenglones do
     begin
          SelectedIndex := PosicionaSiguienteColumna;
          if ( SelectedIndex = PrimerColumna ) then
          begin
               with ClientDatasetHijo do
               begin
                    Next;
                    if EOF then
                       Self.Agregar;
               end;
          end;
     end;
end;

procedure TEditCedScrap.EscribirCambios;
begin
     if FPuedeModificar then
        inherited EscribirCambios
     else
     begin
          dmLabor.DialogoErrorCedulaScrap
     end;
end;

procedure TEditCedScrap.WO_NUMBERValidKey(Sender: TObject);
begin
     inherited;
     if ( WO_NUMBER.Llave <> '' ) and ( AR_CODIGO.Visible ) then
     begin
          AR_CODIGO.Llave := WO_NUMBER.LookUpDataSet.FieldByName('AR_CODIGO').AsString;
     end;
end;

end.



