unit ZArbolTress;

interface

uses ComCtrls, Controls,Dialogs, SysUtils, DB,
     ZArbolTools,ZNavBarTools,cxtreeview;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
procedure MuestraArbolOriginal( oArbol: TTreeView );

//DevEx
procedure CreaNavBarUsuario ( oNavBarMgr: TNavBarMgr; const sFileName: String ; Arbolitos: array of TcxTreeView ; ArbolitoUsuario : TcxTreeView);

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZAccesosMgr,
     ZGlobalTress,
     DGlobal,
     DSistema,
     DCliente,
     ZetaTipoEntidad;

const
     K_EMPLEADOS                        = 326;
     K_EMP_DATOS                        = 327;
     K_EMP_OPERACIONES                  = 328;
//     K_EMP_LECTURAS                     = 504;
     K_EMP_ORDENES_FIJAS                = 329;
     K_EMP_KARDEX_AREAS                 = 330;
     K_LABOR                            = 331;
     K_LABOR_WORK_ORDER                 = 332;
     K_LABOR_HISTORIA_ORDENES           = 334;
     K_LABOR_REGISTRO                   = 335;
     K_LABOR_PROCESO                    = 337;

     K_CATALOGOS                        = 344;
     K_CAT_PARTES                       = 345;
     K_CAT_OPERACIONES                  = 346;
     K_CAT_TIPO_PARTE                   = 347;
     K_CAT_TIPO_OPERACION               = 348;
     K_CAT_TIEMPO_MUERTO                = 350;
     K_CAT_MODULA1                      = 352;
     K_CAT_MODULA2                      = 353;
     K_CAT_MODULA3                      = 354;
     K_CAT_AREA                         = 349;
     K_LABOR_KARDEX_ORDER               = 333;

     K_CONSULTAS                        = 561;//todo
     K_REPORTES                         = 562;//todo

     K_CAT_GLOBAL                       = 355;
     K_CAT_CEDULAS                      = 343;
     K_CAT_BREAKS                       = 351;
     K_LAB_CONS_SQL                     = 566;
     K_LAB_CONS_PROCESOS                = 567;



     K_CALIDAD                          = 364;
     K_LABOR_CEDULAS_INSP               = 363;
     {.$ifdef LABORSCRAP }
     K_LABOR_CEDULAS_SCRAP              = 365;
     K_CAT_TIPO_DEFECTO                 = 362;
     K_LABOR_COMPONENTES                = 377;
     K_LABOR_MOTIVOS_SCRAP              = 378;



     {.$endif}
     K_CAT_MAQUINAS                     = 625;
     K_CAT_TMAQUINAS                    = 626;
     K_CAT_LAYOUT                       = 624;
   {******** Cat�logos **********}
     K_NODO_X                                       =99;     //**PENDIENTE
     K_FORMA_X                        = 100;      //**PENDIENTE
     K_FULL_ARBOL_TRESS                 =9999;




function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;



function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;


//DevEx: Funcion para definir Grupos del NavBar
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     with GLobal do
     begin
          case iNodo of
               // Empleados
               K_EMPLEADOS               : Result := Folder('Empleados', iNodo );
               K_EMP_DATOS               : Result := Forma( 'Datos', efcEmpDatos );
               K_EMP_OPERACIONES         : Result := Forma( 'Operaciones', efcEmpWorks );
//               K_EMP_LECTURAS            : Result := Forma( 'Lecturas', efcEmpLecturas );
               K_EMP_ORDENES_FIJAS       : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_ORDENES ) + ' Fijas', efcEmpWOFijas);
               K_EMP_KARDEX_AREAS        : Result := Forma( 'Kardex de ' + GetGlobalString( K_GLOBAL_LABOR_AREAS ), efcKardexArea);

               // Labor
               K_LABOR                   : Result := Folder( 'Labor', iNodo );
               K_LABOR_WORK_ORDER        : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_ORDEN ), efcUnaOrden);
               K_LABOR_KARDEX_ORDER      : Result := Forma( 'Kardex de ' + GetGlobalString( K_GLOBAL_LABOR_ORDEN ), efcKardexOrden);
               K_LABOR_HISTORIA_ORDENES  : Result := Forma( 'Historial de ' + GetGlobalString( K_GLOBAL_LABOR_ORDENES ), efcHistoriaOrden);
               K_CAT_CEDULAS             : Result := Forma( 'C�dulas', efcCedulas );
             //  K_LABOR_REGISTRO          : Result := Forma( 'Registro', {efcLabRegistro}efcEmpDatos );
             //  K_LABOR_PROCESO           : Result := Forma( 'Proceso', {efcLabProceso}efcEmpDatos );

               //Calidad
               K_CALIDAD                 : Result := Folder( 'Calidad', iNodo );
               K_LABOR_CEDULAS_INSP      : Result := Forma( 'C�dulas de inspecci�n', efcCedulasInspec);
               {.$ifdef LABORSCRAP }
               K_LABOR_CEDULAS_SCRAP     : Result := Forma( 'C�dulas de scrap', efcCedulasScrap );
               K_LABOR_COMPONENTES       : Result := Forma( 'Componentes', efcComponentes );
               K_LABOR_MOTIVOS_SCRAP     : Result := Forma( 'Motivos de scrap', efcMotivosScrap );
               {.$endif}

               K_CAT_TIPO_DEFECTO        : Result := Forma( 'Clase de ' + GetGlobalString( K_GLOBAL_LABOR_TDEFECTOS ), efcTDefectos );

               // Consultas
               K_CONSULTAS               : Result := Folder( 'Consultas', iNodo );
               K_REPORTES                : Result := Forma( 'Reportes', {efcReportes}efcEmpDatos );
               K_LAB_CONS_SQL            : Result := Forma( 'SQL', {efcQueryGral}efcEmpDatos );
               K_LAB_CONS_PROCESOS       : Result := Forma( 'Procesos', {efcSistProcesos}efcEmpDatos );

               // Cat�logos
               K_CATALOGOS               : Result := Folder( 'Cat�logos', iNodo );
               K_CAT_PARTES              : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_PARTES ), efcPartes );
               K_CAT_OPERACIONES         : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_OPERACIONES ), efcOperaciones );
               K_CAT_TIPO_PARTE          : Result := Forma( 'Clases de ' + GetGlobalString( K_GLOBAL_LABOR_PARTES ), efcTipoParte );
               K_CAT_TIPO_OPERACION      : Result := Forma( 'Clases de ' + GetGlobalString( K_GLOBAL_LABOR_OPERACIONES ), efcTipoOpera);
               K_CAT_AREA                : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_AREAS ), efcArea);
               K_CAT_TIEMPO_MUERTO       : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_TMUERTO ), efcTiempoMuerto);
               K_CAT_MODULA1             : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_MODULA1 ), efcModula1);
               K_CAT_MODULA2             : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_MODULA2 ), efcModula2);
               K_CAT_MODULA3             : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_MODULA3 ), efcModula3);
               K_CAT_GLOBAL              : Result := Forma( 'Globales', efcSistGlobales);
               K_CAT_BREAKS              : Result := Forma( 'Breaks', efcBreaks );
               K_CAT_MAQUINAS            : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_MAQUINAS ), efcMaquinas);
               K_CAT_TMAQUINAS           : Result := Forma( Format('Clase de %s', [GetGlobalString( K_GLOBAL_LABOR_MAQUINAS )]), efcTMaquinas );
               K_CAT_LAYOUT              : Result := Forma( GetGlobalString( K_GLOBAL_LABOR_PLANTILLAS ), efcPlantillas );
            else
                Result := Folder( '', 0 );
            end;
     end;
end;

procedure CreaArbolDefault( oArbolMgr: TArbolMgr );

procedure NodoNivel( const iNivel, iNodo: Integer );
begin
     oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

var
   lOrden, lParte, lOperacion, lTDefecto,lMaquina,lLayouts: Boolean;
begin  // CreaArbolDefault

     lOrden := StrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN ) );
     lParte := StrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_PARTE ) );
     lOperacion := StrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_OPERACION ) );
     lTDefecto := STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_TDEFECTO ) );
     lMaquina := STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_MAQUINA ) );
     lLayouts := STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_PLANTILLA ) );

    // ********* Empleados ********
    NodoNivel( 0, K_EMPLEADOS );
      NodoNivel( 1, K_EMP_DATOS );
      NodoNivel( 1, K_EMP_OPERACIONES );
//      NodoNivel( 1, K_EMP_LECTURAS );
      if lOrden then
         NodoNivel( 1, K_EMP_ORDENES_FIJAS );
      NodoNivel( 1, K_EMP_KARDEX_AREAS );


    NodoNivel( 0, K_LABOR );
      if lOrden then
      begin
           NodoNivel( 1, K_LABOR_WORK_ORDER );
           NodoNivel( 1, K_LABOR_KARDEX_ORDER );
           NodoNivel( 1, K_LABOR_HISTORIA_ORDENES );
      end;
      NodoNivel( 1, K_CAT_CEDULAS );
      NodoNivel( 1, K_LABOR_REGISTRO );
      NodoNivel( 1, K_LABOR_PROCESO );
      if lLayouts then
         NodoNivel( 1, K_CAT_LAYOUT );
    if lTDefecto then
    begin
         NodoNivel( 0, K_CALIDAD );
            NodoNivel( 1, K_LABOR_CEDULAS_INSP );
            {.$ifdef LABORSCRAP }
            NodoNivel( 1, K_LABOR_CEDULAS_SCRAP );
            {.$endif}
            NodoNivel( 1, K_CAT_TIPO_DEFECTO );
            {.$ifdef LABORSCRAP }
            NodoNivel( 1, K_LABOR_COMPONENTES );
            NodoNivel( 1, K_LABOR_MOTIVOS_SCRAP );
            {.$endif}
    end;

   { NodoNivel( 0, K_CONSULTAS );
      NodoNivel( 1, K_REPORTES );
      NodoNivel( 1, K_LAB_CONS_SQL );
      NodoNivel( 1, K_LAB_CONS_PROCESOS );
    }
    NodoNivel( 0, K_CATALOGOS );
      if lParte then
         NodoNivel( 1, K_CAT_PARTES );
      if lOperacion then
         NodoNivel( 1, K_CAT_OPERACIONES );
      if lParte then
         NodoNivel( 1, K_CAT_TIPO_PARTE );
      if lOperacion then
         NodoNivel( 1, K_CAT_TIPO_OPERACION );
      NodoNivel( 1, K_CAT_AREA );
      if lMaquina then
         NodoNivel( 1, K_CAT_MAQUINAS );
      if lMaquina then
         NodoNivel( 1, K_CAT_TMAQUINAS );

      NodoNivel( 1, K_CAT_TIEMPO_MUERTO );
      NodoNivel( 1, K_CAT_BREAKS );

      if StrLleno(Global.GetGlobalString( K_GLOBAL_LABOR_MODULA1 ) ) then
         NodoNivel( 1, K_CAT_MODULA1 );
      if StrLleno(Global.GetGlobalString( K_GLOBAL_LABOR_MODULA2 ) )then
         NodoNivel( 1, K_CAT_MODULA2 );
      if StrLleno(Global.GetGlobalString( K_GLOBAL_LABOR_MODULA3 ) )then
         NodoNivel( 1, K_CAT_MODULA3 );
      NodoNivel( 1, K_CAT_GLOBAL );
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin   // CreaArbolUsuario
     with oArbolMgr do
     begin
     //MA: Linea activa cuando se implemente la forma expediente
         Configuracion := TRUE;
         oArbolMgr.NumDefault := K_LABOR;
         CreaArbolDefault( oArbolMgr );
         Arbol.FullExpand;
     end;
end;

procedure MuestraArbolOriginal( oArbol: TTreeView );
var
   oNodo: TTreeNode;
   iNodo: Integer;
begin
     oArbol.Items.BeginUpdate;
     oNodo := oArbol.Items.GetFirstNode;
     while ( oNodo <> NIL ) do
     begin
          iNodo := Integer( oNodo.Data );
          if ( iNodo > 0 ) then
             oNodo.Text := GetNodoInfo( iNodo ).Caption;
          oNodo := oNodo.GetNext;
     end;
     oArbol.Items.EndUpdate;
end;

//devex
//DevEx
procedure CreaNavBarUsuario (  oNavBarMgr: TNavBarMgr; const sFileName: String; Arbolitos: array of TcxtreeView ; ArbolitoUsuario : TcxTreeView);
var
   DataSet: TDataSet;
   NodoRaiz: TGrupoInfo;
//Edit by MP: Metodo Modificado para usar un TcxTreeview
procedure CreaArbolito( Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // Aqu� se agrega el arbol definido por el usuario
                  finally
                     EndUpdate;
                  end;
             end;
          finally
             Free;
          end;
     end;
end;
begin
    DataSet := dmCliente.cdsUsuario;


   //  GetDatosNavBarUsuario(oNavBarMgr, DataSet, sFileName );
     //CreaNavBarUsuario
     with oNavBarMgr.DatosUsuario do
     begin
          oNavBarMgr.NumDefault := NodoDefault;
          // Agrega el Nodo Raiz con el Nombre del Usuario
          if ( oNavBarMgr.Configuracion ) or ( TieneArbol ) then
          begin
             with NodoRaiz do
             begin
                      Caption := NombreNodoInicial;
                      IndexDerechos := K_SIN_RESTRICCION;
                    if(oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)) then
                      begin
                          CreaArbolito( ArbolitoUsuario ); //Pendiente cambiar el indice
                      end;

             end;
          end;
          // Agregar el Arbol de Sistema cuando:
          //  Se trate del Shell y
          //     -Usuario tiene su propio Arbol y decidi� Incluir Sistema, o
          //     -Usuario NO tiene su propio Arbol
          if ( not oNavBarMgr.Configuracion ) then
          begin
             if ( IncluirSistema or ( not TieneArbol )) then
             begin
                if (oNavBarMgr.NumDefault = 0 ) then
                    oNavBarMgr.NumDefault :=  K_NODO_X; //Por si solo se agregara el arbol del sistema

                CreaNavBarDefault(oNavBarMgr, Arbolitos );
             end;

             // Si por derechos no tiene nada, agregar grupo
             if ( oNavBarMgr.NavBar.Groups.Count = 0 ) then
             begin
                with NodoRaiz do
                begin
                     Caption := 'No tiene derechos';
                     //EsForma := FALSE;
                     IndexDerechos := K_SIN_RESTRICCION;
                     oNavBarMgr.NodoNivel(NodoRaiz,0, K_ARBOL_NODO_RAIZ)
                end;
             end;
          end;
     end;
end;

procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
     Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     with oArbolMgr do
                     begin
                          //Configuracion := TRUE;
                           begin
                            //oArbolMgr.NumDefault := K_GLOBAL ;
                            if(K_global = K_FULL_ARBOL_TRESS) then
                              ZArbolTress.CreaArbolDefault( oArbolMgr)
                            else
                              CreaArbolitoDefault( oArbolMgr, K_GLOBAL  )
                          end;
                               //Edit by MP: si el arbol es nil, utilizar el Arbol_Devex
                          if(arbol = nil) then
                            arbol_Devex.FullCollapse
                          else
                            Arbol.FullCollapse;
                          // Fin Edit
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Presupuestos *****************

     begin
        if( NodoNivel( K_EMPLEADOS, 1 )) then
        begin
          CreaArbolito ( K_EMPLEADOS, Arbolitos[1]);
        end;
        if( NodoNivel( K_LABOR, 2 )) then
        begin
          CreaArbolito ( K_LABOR, Arbolitos[2]);
        end;
        if( NodoNivel( K_CALIDAD, 3 ) and (STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_TDEFECTO ) ))) then
        begin
          CreaArbolito ( K_CALIDAD, Arbolitos[3]);
        end;
        if( NodoNivel( K_CATALOGOS, 4 )) then
        begin
          CreaArbolito ( K_CATALOGOS, Arbolitos[4]);
        end;
     end;
end;

//DevEx
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
    case iNodo of
      K_EMPLEADOS : Result := Grupo( 'Empleados', iNodo );
      K_LABOR : Result := Grupo( 'Labor', iNodo );
      K_CALIDAD : begin
       if STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_TDEFECTO ) ) then
        Result := Grupo( 'Calidad', iNodo );
      end;
      K_CATALOGOS : Result := Grupo( 'Cat�logos', iNodo );
    end;
end;


procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
var
   i, iNiveles: Integer;
   lOrden, lParte, lOperacion, lTDefecto,lMaquina,lLayouts: Boolean;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
  //**   if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then

          NodoNivel(1,iNodo);
end;

{procedure NodoNivel( const iNivel, iNodo: Integer );
begin
     oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;}


begin  // CreaArbolDefault

     lOrden := StrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_ORDEN ) );
     lParte := StrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_PARTE ) );
     lOperacion := StrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_OPERACION ) );
     lTDefecto := STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_TDEFECTO ) );
     lMaquina := STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_MAQUINA ) );
     lLayouts := STrLleno( Global.GetGlobalString( K_GLOBAL_LABOR_PLANTILLA ) );



    // ********* Empleados ********
    case iAccesoGlobal of
    K_EMPLEADOS:
    begin
    //NodoNivel( 0, K_EMPLEADOS );
      NodoNivel( 0, K_EMP_DATOS );
      NodoNivel( 0, K_EMP_OPERACIONES );
//      NodoNivel( 1, K_EMP_LECTURAS );
      if lOrden then
         NodoNivel( 0, K_EMP_ORDENES_FIJAS );
      NodoNivel( 0, K_EMP_KARDEX_AREAS );
    end;
    K_LABOR:
    begin
    //NodoNivel( 0, K_LABOR );
      if lOrden then
      begin
           NodoNivel( 0, K_LABOR_WORK_ORDER );
           NodoNivel( 0, K_LABOR_KARDEX_ORDER );
           NodoNivel( 0, K_LABOR_HISTORIA_ORDENES );
      end;
      NodoNivel( 0, K_CAT_CEDULAS );
      NodoNivel( 0, K_LABOR_REGISTRO );
      NodoNivel( 0, K_LABOR_PROCESO );
      if lLayouts then
         NodoNivel( 0, K_CAT_LAYOUT );
    end;
    K_CALIDAD: begin
    if lTDefecto then
    begin
            NodoNivel( 0, K_LABOR_CEDULAS_INSP );
            {.$ifdef LABORSCRAP }
            NodoNivel( 0, K_LABOR_CEDULAS_SCRAP );
            {.$endif}
            NodoNivel( 0, K_CAT_TIPO_DEFECTO );
            {.$ifdef LABORSCRAP }
            NodoNivel( 0, K_LABOR_COMPONENTES );
            NodoNivel( 0, K_LABOR_MOTIVOS_SCRAP );
            {.$endif}
    end;
    end;
    K_CONSULTAS: begin
      NodoNivel( 0, K_REPORTES );
      NodoNivel( 0, K_LAB_CONS_SQL );
      NodoNivel( 0, K_LAB_CONS_PROCESOS );
    end;
   K_CATALOGOS: begin
      if lParte then
         NodoNivel( 0, K_CAT_PARTES );
      if lOperacion then
         NodoNivel( 0, K_CAT_OPERACIONES );
      if lParte then
         NodoNivel( 0, K_CAT_TIPO_PARTE );
      if lOperacion then
         NodoNivel( 0, K_CAT_TIPO_OPERACION );
      NodoNivel( 0, K_CAT_AREA );
      if lMaquina then
         NodoNivel( 0, K_CAT_MAQUINAS );
      if lMaquina then
         NodoNivel( 0, K_CAT_TMAQUINAS );

      NodoNivel( 0, K_CAT_TIEMPO_MUERTO );
      NodoNivel( 0, K_CAT_BREAKS );

      if StrLleno(Global.GetGlobalString( K_GLOBAL_LABOR_MODULA1 ) ) then
         NodoNivel( 0, K_CAT_MODULA1 );
      if StrLleno(Global.GetGlobalString( K_GLOBAL_LABOR_MODULA2 ) )then
         NodoNivel( 0, K_CAT_MODULA2 );
      if StrLleno(Global.GetGlobalString( K_GLOBAL_LABOR_MODULA3 ) )then
         NodoNivel( 0, K_CAT_MODULA3 );
      NodoNivel( 0, K_CAT_GLOBAL );
    end;
    end;
end;
//end devex


end.
