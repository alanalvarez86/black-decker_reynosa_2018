unit QrTools;

interface

uses Windows, SysUtils, DB, DBTables, StdCtrls, Controls, Classes,
     Forms,
     ZetaCommonLists,
     ZetaEntidad,
     ZetaCommonClasses;

function GetValorCombo( oCombo : TComboBox ): string;
function GetValorList( oLista : TListBox; i : integer ) : string;

procedure SetIndiceCombo( oCombo : TCustomComboBox; iValor : integer );
procedure SetValorCombo( oCombo : TCustomComboBox; sValor : string );
procedure ClasificacionPrincipal( oLista : TStrings; iValor : integer );
procedure LimpiaLista( oLista : TStrings );
procedure GetLista( tqQuery : TQuery; iClasif: Integer; Lista: TStrings );

var
fClasifiReporte : Array[eClasifiReporte] of PChar = (
{EMPLEADOS}    '10;33;70;55;0;53;2;57;32;91;92',
{ASISTENCIA}   '5;8;23;81;',
{NOMINAS}      '50;1;40;89;90;',
{PAGOS IMSS}   '34;35;36;79;',
{CONSULTAS}    '61;7;',
{CATALOGOS}    '6;11;13;16;18;27;37;54;56;60;63;69;',
{TABLAS}       '9;12;14;15;17;19;20;21;22;24;30;38;39;41;42;43;44;45;46;47;48;49;51;59;62;64;65;68;71;72;82;',
{SUPERVISORES} '',
{CAFETERIA}    '');


implementation

uses {FEvBuilder, }ZetaFunciones;


function GetValorCombo( oCombo : TComboBox ): string;
begin
  with oCombo do
    if ( ItemIndex >= 0 ) AND ( Items.Objects[ItemIndex] <> NIL )then
       Result := TCampoMaster( Items.Objects[ ItemIndex ] ).Formula
    else Result := VACIO;
  if Result = kNINGUNO then Result := VACIO;
end;

function GetValorList( oLista : TListBox; i : integer ) : string;
begin
  with oLista do
     begin
     if i < 0 then i := ItemIndex;
     if i >= 0 then
        Result := TCampoMaster( Items.Objects[ i ] ).Formula
     else Result := VACIO;
     end;
end;

procedure SetIndiceCombo( oCombo : TCustomComboBox; iValor : integer );
begin
  if iValor < 0 then iValor := 0;
  oCombo.ItemIndex := iValor;
end;

procedure SetValorCombo( oCombo : TCustomComboBox; sValor : string );
begin
  SetIndiceCombo( oCombo, oCombo.Items.IndexOf( sValor ) );
end;


procedure LlenaLista( oLista : TStrings; sClasificaciones : string; eTipo : ListasFijas );
  var oListaNum : TStringList;
      oContainer : TCampoMaster;
      i, n : integer;
      sValor : string;
begin
  LimpiaLista( oLista );
  oListaNum := TSTringList.Create;
  DecodeString( sClasificaciones, oListaNum );
  for i:=0 to oListaNum.Count - 1 do
      begin
        n :=  StrToInt( oListaNum[ i ] );
        oContainer := TCampoMaster.Create;
        oContainer.Entidad := TipoEntidad( n );
        sValor := ObtieneElemento( eTipo, n );
        if StrLleno( sValor ) then
           oLista.AddObject( sValor, oContainer );
      end;
  oListaNum.Free;
end;

procedure ClasificacionPrincipal( oLista : TStrings; iValor : integer );
begin
     LlenaLista( oLista,fClasifiReporte[eClasifiReporte(iValor)], lfTipoEntidad );
end;

procedure LimpiaLista( oLista : TStrings );
begin
     oLista.Clear;
end;

procedure GetLista( tqQuery : TQuery; iClasif: Integer; Lista: TStrings );
 var oContainer : TCampoMaster;
begin
     tqQuery.Active := FALSE;
     tqQuery.Params[ 0 ].AsInteger := iClasif;
     tqQuery.Active := TRUE;

     with tqQuery do
     begin
          while not EOF do
          begin
               oContainer := TCampoMaster.Create;
               oContainer.Formula := Fields[ 1 ].AsString;
               //oContainer.Clasifica := eClasifiDiccion( iClasif );
               oContainer.Entidad := TipoEntidad( Fields[ 2 ].AsInteger );
               Lista.AddObject( Fields[ 0 ].AsString, oContainer );
               Next;
          end;
     end;
end;

end.
 