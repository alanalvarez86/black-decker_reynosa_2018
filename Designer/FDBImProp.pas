unit FDBImProp;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, QRDesign, QRDCtrls, ZetaCommonClasses;

type
  TDBImageForm = class(TForm)
    Button3: TButton;
    Button4: TButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    StretchCheckbox: TCheckBox;
    AlignmentComboBox: TComboBox;
    CenterCheckBox: TCheckBox;
    DataFieldComboBox: TComboBox;
    lbDataField: TLabel;
    Label3: TLabel;
    BandsComboBox: TComboBox;
    Label4: TLabel;
    CBRotacion: TComboBox;
    RGMuestraImagen: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EnableControles;
    procedure RGMuestraImagenClick(Sender: TObject);
  private
  public
    DatasourceList: TList;
  end;

var
  DBImageForm: TDBImageForm;

const
     K_ITEM_EMPLEADO = 0;
     K_ITEM_MAESTRO = 1;

function EditDBImageProps(QRD: TQRepDesigner; Owner: TWinControl; QRC: TQRDesignDBImage): Boolean;

implementation

uses
  DB;

{$R *.DFM}

{--------------------------------------------------------------------------------}

FUNCTION EditDBImageProps(QRD: TQRepDesigner; Owner: TWinControl; QRC: TQRDesignDBImage): Boolean;
VAR i : integer;
begin
  DBImageForm:=TDBImageForm.Create(Owner);
  With DBImageForm.AlignmentComboBox Do
    begin
      Items.Clear;
      Items.Add(QRD.RuntimeMessages.Values['ImgAlign None']);
      Items.Add(QRD.RuntimeMessages.Values['ImgAlign Top']);
      Items.Add(QRD.RuntimeMessages.Values['ImgAlign Bottom']);
      Items.Add(QRD.RuntimeMessages.Values['ImgAlign Left']);
      Items.Add(QRD.RuntimeMessages.Values['ImgAlign Right']);
    end;

  If QRC.Dataset<>NIL Then
    DBImageForm.Caption:=Format(QRD.RuntimeMessages.Values['GraphicFrom'],[QRC.Dataset.Name])
  Else
    DBImageForm.Caption:=Format(QRD.RuntimeMessages.Values['GraphicFrom'],['']);

  DBImageForm.DataFieldComboBox.ItemIndex := 0;

  {If QRC.BlockChange Then DBImageForm.DataFieldComboBox.Enabled:=FALSE; } //am: CAMBIO TEMPORAL

  For i:=0 to QRD.Owner.ComponentCount-1 Do
      If QRD.Owner.Components[ i ] is TQRDesignBand Then
         DBImageForm.BandsCombobox.Items.Add( QRD.Owner.Components[ i ].Name );

  if ( QRC.Parent = NIL ) AND ( QRD.QReport.Bands.HasDetail ) then
     with DBImageForm.BandsCombobox do
          ItemIndex := Items.IndexOf( QRD.QReport.Bands.DetailBand.Name )
  else if ( QRC.Parent <> NIL ) then
       with DBImageForm.BandsCombobox do
            ItemIndex := Items.IndexOf( QRC.Parent.Name );


  if DBImageForm.BandsCombobox.ItemIndex < 0 then
     DBImageForm.BandsCombobox.ItemIndex := 0;

  DBImageForm.StretchCheckBox.Checked:=QRC.Stretch;
  DBImageForm.CenterCheckBox.Checked:=QRC.Center;
  DBImageForm.CBRotacion.ItemIndex:=QRC.Tag;

  if ( QRC.DataField <> K_IMAGEN_MAESTRO ) then
  begin
       DBImageForm.DataFieldComboBox.Text:= QRC.DataField;
       DBImageForm.RGMuestraImagen.ItemIndex:= K_ITEM_EMPLEADO;
  end
  else
  begin
       DBImageForm.RGMuestraImagen.ItemIndex:= K_ITEM_MAESTRO;
       DBImageForm.EnableControles;
       DBImageForm.DataFieldComboBox.Text:= VACIO;
  end;

  Case QRC.Align of
    alNone          : DBImageForm.AlignmentComboBox.ItemIndex:=0;
    alTop           : DBImageForm.AlignmentComboBox.ItemIndex:=1;
    alBottom        : DBImageForm.AlignmentComboBox.ItemIndex:=2;
    alLeft          : DBImageForm.AlignmentComboBox.ItemIndex:=3;
    alRight         : DBImageForm.AlignmentComboBox.ItemIndex:=4;
    alClient        : DBImageForm.AlignmentComboBox.ItemIndex:=0;
  end;
  Result:=DBImageForm.ShowModal=mrOk;
  If Result Then
    begin
      QRC.Parent := TWinControl( QRD.Owner.FindComponent( DBImageForm.BandsComboBox.Text ) );
      QRC.Dataset := QRD.QReport.DataSet;
      if DBImageForm.RGMuestraImagen.ItemIndex = K_ITEM_EMPLEADO then
         QRC.DataField:= DBImageForm.DataFieldComboBox.Text
      else
          QRC.DataField:= K_IMAGEN_MAESTRO;

      QRC.Stretch:=DBImageForm.StretchCheckBox.Checked;
      QRC.Center:=DBImageForm.CenterCheckBox.Checked;
      QRC.Tag:=DBImageForm.CBRotacion.ItemIndex;

      Case DBImageForm.AlignmentComboBox.ItemIndex of
        0: QRC.Align:=alNone;
        1: QRC.Align:=alTop;
        2: QRC.Align:=alBottom;
        3: QRC.Align:=alLeft;
        4: QRC.Align:=alRight;
      end;
      QRC.Invalidate;
    end;
  DBImageForm.Free;
end;


procedure TDBImageForm.FormCreate(Sender: TObject);
begin
  {$IFNDEF AllowDatasetSelection}
  //DatasetCombobox.Visible:=False;
  {$ELSE}
  DatafieldCombobox.Left:=232;
  DatafieldCombobox.Width:=193;
  {$ENDIF}
  DatasourceList:=TList.Create;
end;

procedure TDBImageForm.FormDestroy(Sender: TObject);
begin
  DatasourceList.Destroy;
end;


procedure TDBImageForm.EnableControles;
begin
     DataFieldComboBox.Enabled:= (RGMuestraImagen.ItemIndex = K_ITEM_EMPLEADO);
     lbDataField.Enabled:= (RGMuestraImagen.ItemIndex = K_ITEM_EMPLEADO);
end;

procedure TDBImageForm.RGMuestraImagenClick(Sender: TObject);
begin
     EnableControles;
end;

end.
