unit DDiccionario;
{$INCLUDE DEFINES.INC}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}  
  DBaseTressDiccionario, Db, DBClient, ZetaClientDataSet, ZetaCommonLists,
  DBaseDiccionario;

type
  TdmDiccionario = class(TdmBaseTressDiccionario)
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    {$ifdef RDD}
    {$else}
    function GetNOClasificacion(const lFavoritos, lMigracion,lSuscripciones: Boolean): ListaClasificaciones;override;
    {$endif}
  public
    { Public declarations }
    procedure GetListaClasifiModulo(oLista: TStrings;const lMigracion: Boolean);override;

  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses
    ZetaCommonClasses,
    ZAccesosMgr,
    ZAccesosTress;

{$R *.DFM}

procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
     {$ifdef RDD}
     {$else}

     FClasificacion :=   VarArrayOf( [crFavoritos,
                          crSuscripciones,
                          crEmpleados ,
                          crCursos ,
                          crAsistencia ,
                          crNominas ,
                          crPagosIMSS ,
                          crConsultas ,
                          crCatalogos ,
                          crTablas ,
                          crSupervisor ,
                          crCafeteria ,
                          crLabor ,
                          crMedico ,
                          crCarrera ,
                          crKiosco ,
                          crAccesos ,
                          crEvaluacion ,
                          crCajaAhorro ,
                          crMigracion  ]
                         );
     {$endif}

end;



procedure TdmDiccionario.GetListaClasifiModulo( oLista : TStrings; const lMigracion : Boolean );
{$ifdef RDD}
{$else}
 var
    i: integer;
    Clasificaciones: ListaClasificaciones;
{$endif}
begin
       inherited GetListaClasifiModulo( oLista, lMigracion );

     {$ifdef RDD}
     {$else}
       with oLista do
       begin
            Clasificaciones := GetNOClasificacion( FALSE, lMigracion, FALSE );

            for i:= VarArrayLowBound( FClasificacion, 1 ) to VarArrayHighBound( FClasificacion, 1 ) do
            begin
                 if NOT ( eClasifiReporte( FClasificacion[i] ) in Clasificaciones ) then
                    AgregaClasifi( oLista, eClasifiReporte( FClasificacion[i] ), TRUE );
            end;
       end;
     {$endif}
end;

{$ifdef RDD}
{$else}
function TdmDiccionario.GetNOClasificacion( const lFavoritos, lMigracion, lSuscripciones : Boolean ): ListaClasificaciones;
begin
     Result := inherited GetNOClasificacion( lFavoritos, lMigracion, lSuscripciones );

     if NOT lMigracion then
        Result := Result + [crMigracion];

     Result := Result + [crEvaluacion];
end;
{$endif}

end.
