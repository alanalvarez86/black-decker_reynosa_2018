unit DCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient,FileCtrl,
  {$ifdef DOS_CAPAS}
     {$ifdef VISITANTES}
     DServerGlobalVisitantes,
     {$ELSE}
     DServerCatalogos,
     DServerGlobal,
     {$ENDIF}
 {$endif}
 {$ifdef VISITANTES}
     DBasicoCliente,
 {$else}
     DBaseCliente,
 {$endif}

  ZetaCommonLists,
  ZetaCommonClasses;

type
  {$ifdef VISITANTES}
  TBaseClass = TBasicoCliente;
  {$else}
  TBaseClass = TBaseCliente;
  {$endif}
  
  TdmCliente = class(TBaseClass)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FImssPatron: string;
    FIMSSYear: integer;
    FIMSSTipo: eTipoLiqIMSS;
    FIMSSMes: integer;
    { Private declarations }
  protected
  {$ifdef DOS_CAPAS}
    {$ifdef VISITANTES}
    FServerGlobal: TdmServerGlobalVisitantes;
    {$ELSE}
    FServerCatalogos: TdmServerCatalogos;
    FServerGlobal: TdmServerGlobal;
    {$ENDIF}
  {$endif}
  public
        procedure CargaActivosIMSS(oParams : TZetaParams);
        procedure CargaActivosPeriodo(oParams : TZetaParams);
        procedure CargaActivosSistema(oParams : TZetaParams);
        property IMSSPatron : string read FImssPatron write FImssPatron;
        property IMSSYear : integer read FIMSSYear write FIMSSYear;
        property IMSSMes : integer read FIMSSMes write FIMSSMes;
        property IMSSTipo : eTipoLiqIMSS read FIMSSTipo write FIMSSTipo;
        function GetDatosPeriodoActivo : TDatosPeriodo;
        function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
        procedure CargaActivosTodos(Parametros: TZetaParams);override;
    {$ifdef DOS_CAPAS}
        {$ifdef VISITANTES}
        property ServerGlobal: TdmServerGlobalVisitantes read FServerGlobal;
        {$ELSE}
        property ServerCatalogos: TdmServerCatalogos read FServerCatalogos;
        property ServerGlobal: TdmServerGlobal read FServerGlobal;
        {$ENDIF}
    {$endif}

end;

var
  dmCliente: TdmCliente;

implementation
uses ZetaRegistryCliente,
     ZetaDialogo;

{$R *.DFM}

{ TdmCliente }

procedure TdmCliente.CargaActivosIMSS(oParams: TZetaParams);
begin

end;

procedure TdmCliente.CargaActivosPeriodo(oParams: TZetaParams);
begin

end;

procedure TdmCliente.CargaActivosSistema(oParams: TZetaParams);
begin

end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', '' );
          AddInteger( 'IMSSYear', 0 );
          AddInteger( 'IMSSMes', 0 );
          AddInteger( 'IMSSTipo', 0 );
          AddInteger( 'Year', 0 );
          AddInteger( 'Tipo', 0 );
          AddInteger( 'Numero', 0 );
          AddDate( 'FechaAsistencia', Date );
          AddDate( 'FechaDefault', Date );
          AddInteger( 'YearDefault', 0 );
          AddInteger( 'EmpleadoActivo', 0 );
          AddString( 'NombreUsuario', '' );
          AddString( 'CodigoEmpresa', '' );
     end;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     Result :='';
end;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     inherited;
{$ifdef SELECCION}
     TipoCompany := tcRecluta;
     ModoSeleccion := TRUE;
{$endif}

{$ifdef VISITANTES}
     TipoCompany := tcVisitas;
     ModoVisitantes := TRUE;
{$endif}

{$ifdef ADUANAS}
     TipoCompany := tcComExt;
     //ModoAduanas := TRUE;
{$endif}

{$ifdef DOS_CAPAS}
     {$ifdef VISITANTES}
     FServerGlobal := TdmServerGlobalVisitantes.Create( Self );
     {$ELSE}
     FServerCatalogos := TdmServerCatalogos.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     {$ENDIF}
{$endif}

end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     FreeAndNil( FServerGlobal );
     {$IFNDEF VISITANTES}
     FreeAndNil( FServerCatalogos );
     {$ENDIF}
{$endif}
     inherited;

end;

end.



