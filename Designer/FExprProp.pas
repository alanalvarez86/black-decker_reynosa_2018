unit FExprProp;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, QRDesign, QRDCtrls,
  ZetaTipoEntidad,
  ZetaCommonLists, ZetaKeyCombo;

type
  TExpressionForm = class(TForm)
    FontDialog1: TFontDialog;
    ColorDialog1: TColorDialog;
    Button3: TButton;
    Button4: TButton;
    FrameGroupBox: TGroupBox;
    AutoSizeCheckbox: TCheckBox;
    TransparentCheckBox: TCheckBox;
    WordWrapCheckBox: TCheckBox;
    ExpressionGroupBox: TGroupBox;
    Label2: TLabel;
    Button5: TButton;
    ExpressionEdit: TEdit;
    StretchCheckBox: TCheckBox;
    ColorGroupBox: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    FormatGroupBox: TGroupBox;
    Label3: TLabel;
    FormatEdit: TEdit;
    ResetCheckBox: TCheckBox;
    GroupBox4: TGroupBox;
    Label5: TLabel;
    UnitLabel2: TLabel;
    DegreeEdit: TEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    AlignmentComboBox: TComboBox;
    SpeedButton19: TSpeedButton;
    SpeedButton20: TSpeedButton;
    Label6: TLabel;
    EAlias: TEdit;
    lbTipCampo: TLabel;
    cbTipoGlobal: TComboBox;
    procedure FontButtonClick(Sender: TObject);
    procedure ColorButtonClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure DegreeEditKeyPress(Sender: TObject; var Key: Char);
    procedure SpeedButton19Click(Sender: TObject);
    procedure SpeedButton20Click(Sender: TObject);
    procedure ExpressionEditExit(Sender: TObject);
  private
     fEntidadActiva : TipoEntidad;
  public
    { Public-Deklarationen }
    QRD: TComponent;
    TF: TFont;
    AColor: Longint;
    CompList: Array[0..30] of TComponent;

    property EntidadActiva : TipoEntidad read fEntidadActiva write fEntidadActiva;
    procedure SetControlsTipoGlobal;
  end;

var
  ExpressionForm: TExpressionForm;

function EditExpressionProps(QRD: TQRepDesigner; Owner: TWinControl; QRC: TQRDesignExpr): Boolean;

{-------------------------------------------------------------------------------------}
{-------------------------------------------------------------------------------------}
{-------------------------------------------------------------------------------------}

implementation

USES QuickRpt,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZReportTools,
     ZConstruyeFormula;

{$R *.DFM}

procedure TExpressionForm.FontButtonClick(Sender: TObject);
begin
  FontDialog1.Font.Assign(TF);
  If FontDialog1.Execute Then TF.Assign(FontDialog1.Font);
end;

procedure TExpressionForm.ColorButtonClick(Sender: TObject);
begin
  ColorDialog1.Color:=AColor;
  If ColorDialog1.Execute Then AColor:=ColorDialog1.Color;
end;

{--------------------------------------------------------------------------------}

function EditExpressionProps(QRD: TQRepDesigner; Owner: TWinControl; QRC: TQRDesignExpr): Boolean;
begin
     ExpressionForm:=TExpressionForm.Create(Owner);
     ExpressionForm.QRD:=QRD;
     with ExpressionForm do
     begin
          ExpressionEdit.Text:=QRC.Expression;
          AutoSizeCheckBox.Checked:=QRC.AutoSize;
          StretchCheckBox.Checked:=QRC.AutoStretch;
          TransparentCheckBox.Checked:=QRC.Transparent;
          WordWrapCheckBox.Checked:=QRC.WordWrap;
          ResetCheckbox.Checked:=QRC.ResetAfterPrint;

          Case QRC.Alignment of
               taRightJustify  : AlignmentComboBox.ItemIndex:=1;
               taLeftJustify   : AlignmentComboBox.ItemIndex:=0;
               taCenter        : AlignmentComboBox.ItemIndex:=2;
          end;//case
          TF:=TFont.Create;
          TF.Assign(QRC.Font);
          AColor:=QRC.Color;

          FormatEdit.Text:= QRC.Mask;
          EAlias.Text := QRC.Name;
          if Pos('RESULT', EAlias.Text ) > 0 then
             EAlias.Text := '#' + EAlias.Text;
          DegreeEdit.Text:=FormatFloat('',QRC.Degree/10);

          cbTipoGlobal.ItemIndex := QRC.Tag;
          SetControlsTipoGlobal;

          Result:=ShowModal=mrOk;

          If Result Then
          begin
               QRC.Mask := FormatEdit.Text;
               if Pos('#RESULT', EAlias.Text ) > 0 then
                  EAlias.Text := Copy(EAlias.Text,2,MaxInt);
               QRC.Name := EAlias.Text;
               QRC.Expression:= ExpressionEdit.Text;
               QRC.AutoStretch:= StretchCheckBox.Checked;
               QRC.ResetAfterPrint:= ResetCheckbox.Checked;
               QRC.WordWrap:= WordWrapCheckBox.Checked;
               QRC.AutoSize:= AutoSizeCheckBox.Checked;
               QRC.Transparent:= TransparentCheckBox.Checked;
               Case AlignmentComboBox.ItemIndex of
                    1: QRC.Alignment:=taRightJustify;
                    0: QRC.Alignment:=taLeftJustify;
                    2: QRC.Alignment:=taCenter;
               end;
               QRC.Font.Assign( TF );
               QRC.Color:= AColor;
               QRC.Tag := cbTipoGlobal.ItemIndex;
               Try
                  QRC.Degree:=Round( 10*StrToFloat( DegreeEdit.Text ) );
               Except
               End;
               QRC.Invalidate;
          END;
          TF.Free;
          Free;
      end;
END;


procedure TExpressionForm.Button5Click(Sender: TObject);
begin
     ExpressionEdit.Text := BorraCReturn( GetFormulaConst( TipoEntidad( StrToIntDef( TQRepDesigner(Qrd).QReport.ReportTitle, 10 ) ),
                                          ExpressionEdit.Text,
                                          ExpressionEdit.SelStart,
                                          evReporte ));

end;

procedure TExpressionForm.DegreeEditKeyPress(Sender: TObject;
  var Key: Char);
begin
  If (Key='.') or (Key=',') Then Key:= {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator;
  If Not (Key in ['0'..'9',{$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}DecimalSeparator]) Then Key:=#0;
end;

procedure TExpressionForm.SpeedButton19Click(Sender: TObject);
begin
  Try
    DegreeEdit.Text:=IntToStr(StrToInt(DegreeEdit.Text)+1);
  Except
  end;
end;

procedure TExpressionForm.SpeedButton20Click(Sender: TObject);
begin
  Try
    If StrToInt(DegreeEdit.Text)>1 Then
      DegreeEdit.Text:=IntToStr(StrToInt(DegreeEdit.Text)-1);
  Except
  end;
end;



procedure TExpressionForm.ExpressionEditExit(Sender: TObject);
begin
     SetControlsTipoGlobal;
end;

procedure TExpressionForm.SetControlsTipoGlobal;
begin
     cbTipoGlobal.Enabled := EsFormulaSQL(ExpressionEdit.Text);
     lbTipCampo.Enabled := cbTipoGlobal.Enabled;
end;

end.

