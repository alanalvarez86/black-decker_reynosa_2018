unit FEditMensajes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, DBCtrls,
  StdCtrls, Mask, ZetaNumero, ZetaEdit, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons;

type
  TEditMensajes_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DM_ORDEN: TZetaDBNumero;
    DM_SLEEP: TZetaDBNumero;
    Label6: TLabel;
    DM_ACTIVO: TDBCheckBox;
    ZetaDBEdit1: TZetaDBEdit;
    DM_CODIGO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  end;

var
  EditMensajes_DevEx: TEditMensajes_DevEx;

implementation

uses dSistema,
     ZAccesosTress;

{$R *.dfm}

{ TEditMensajes }

procedure TEditMensajes_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsMensajes.Conectar;
          DataSource.DataSet := cdsMensajes;
     end;
end;

procedure TEditMensajes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_SIST_MENSAJES ;
     FirstControl := DM_CODIGO;
end;

end.
