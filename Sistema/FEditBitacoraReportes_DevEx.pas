unit FEditBitacoraReportes_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBCtrls, StdCtrls, Buttons, ExtCtrls,
     ZetaDBTextBox, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxButtons, cxControls, cxNavigator, cxDBNavigator,
  cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit, ImgList;

type
  TEditBitacoraReportes_DevEx = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    Panel1: TPanel;
    FechaLBL: TLabel;
    BI_FECHA: TZetaDBTextBox;
    HoraLBL: TLabel;
    BI_HORA: TZetaDBTextBox;
    TipoLBL: TLabel;
    BI_TIPO: TZetaDBTextBox;
    MensajeLBL: TLabel;
    Imagen: TImage;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    BI_DATA: TcxDBMemo;
    CalendarioLBL: TLabel;
    CA_NOMBRE: TZetaDBTextBox;
    EmpresaLBL: TLabel;
    CM_CODIGO: TZetaDBTextBox;
    ReporteLBL: TLabel;
    CA_REPORT: TZetaDBTextBox;
    FrecuenciaLBL: TLabel;
    CA_FREC: TZetaDBTextBox;
    BI_TEXTO: TcxDBMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Conectar( Dataset: TDataset );
  end;
  TEditBitacoraReportesClass = class of TEditBitacoraReportes_DevEx;

var
  EditBitacoraReportes_DevEx: TEditBitacoraReportes_DevEx;

procedure ShowLogDetail( var Forma; EditBitacoraClass: TEditBitacoraReportesClass; Dataset: TDataset );

implementation
uses ZetaCommonClasses;
{$R *.DFM}

procedure ShowLogDetail( var Forma; EditBitacoraClass: TEditBitacoraReportesClass; Dataset: TDataset );
begin
     try
        TEditBitacoraReportes_DevEx( Forma ) := EditBitacoraClass.Create( Application ) as TEditBitacoraReportes_DevEx;
        with TEditBitacoraReportes_DevEx( Forma ) do
        begin
             Conectar( Dataset );
             ShowModal;
        end;
     finally
            TEditBitacoraReportes_DevEx( Forma ).Free;
     end;
end;

{ ********** TFormaBitacora ********** }

procedure TEditBitacoraReportes_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DataSource.DataSet := nil;
end;

procedure TEditBitacoraReportes_DevEx.Conectar(Dataset: TDataset);
begin
     DataSource.DataSet := Dataset;
end;

end.
