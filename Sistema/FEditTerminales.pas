unit FEditTerminales;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ComCtrls, Mask, ZetaNumero, ZetaEdit, ZetaFecha, ZetaKeyCombo,
  ZetaDBTextBox, DXMLTools;

type
  TEditTerminales = class(TBaseEdicion)
    PageControl: TPageControl;
    tbDatosGenerales: TTabSheet;
    tbConfiguracion: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    TE_CODIGO: TZetaDBNumero;
    TE_NUMERO: TZetaDBNumero;
    TE_ACTIVO: TDBCheckBox;
    TE_ZONA: TZetaKeyCombo;
    TE_BEAT: TZetaDBTextBox;
    TE_APP: TZetaKeyCombo;
    btnReiniciarTerminal: TButton;
    GroupBox5: TGroupBox;
    GroupBox10: TGroupBox;
    GroupBox11: TGroupBox;
    TabSheet3: TTabSheet;
    TabSheet6: TTabSheet;
    Label22: TLabel;
    Label23: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    TimeOut: TZetaNumero;
    HeartBeatTimer: TZetaNumero;
    MsgTime: TZetaNumero;
    RelayTime: TZetaNumero;
    Label9: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    ProxReader: TComboBox;
    BarCodeReader: TComboBox;
    Label12: TLabel;
    FingerVerify: TCheckBox;
    EnableOfflineRelay: TCheckBox;
    Label13: TLabel;
    Label15: TLabel;
    MenuAccessCode: TZetaNumero;
    Label16: TLabel;
    Volume: TZetaNumero;
    TabSheet1: TTabSheet;
    Label14: TLabel;
    Label44: TLabel;
    UseProxy: TCheckBox;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    UseProxyAuthentication: TCheckBox;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label18: TLabel;
    HID: TComboBox;
    Label21: TLabel;
    ErrorMsgTime: TZetaNumero;
    Label11: TLabel;
    TE_NOMBRE: TDBEdit;
    TE_TEXTO: TDBEdit;
    DateTimeFormat: TEdit;
    WebService: TEdit;
    Ip: TEdit;
    NetMask: TEdit;
    DefaultGateWay: TEdit;
    DNS: TEdit;
    Proxy: TEdit;
    ProxyPort: TEdit;
    ProxyUserName: TEdit;
    ProxyPassword: TEdit;
    FpuNotIdetifyMessage: TEdit;
    NotValidByServerMessage: TEdit;
    DuplicateMessage: TEdit;
    AutenticateMessage: TEdit;
    OfflineValidation: TEdit;
    Label10: TLabel;
    UpdateTimeByServer: TCheckBox;
    TE_MAC: TZetaTextBox;
    Label17: TLabel;
    OneMinuteValidate: TCheckBox;
    TabSheet2: TTabSheet;
    DBRichEdit1: TDBRichEdit;
    Label28: TLabel;
    ShowPhoto: TCheckBox;
    EnableKeyPad: TCheckBox;
    Label29: TLabel;
    Label30: TLabel;
    FingerPrint: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TE_ZONAChange(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure TE_APPChange(Sender: TObject);
    procedure btnReiniciarTerminalClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure XMLChange(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
    FXML: TdmXMLTools;
    FBrowsing: Boolean;
    function ObtenIndice( sZona: String ): Integer;
    function ObtenZona( iIndice: Integer ): String;
    function ObtenZonaES( iIndice: Integer ): String;
    function ActualizaTag( sTag, sValor, sXML: String ): String;
    procedure ActualizaXML;
    procedure LeeXML;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
  end;

var
  EditTerminales: TEditTerminales;

const
     K_WEBSERVICE = 'WebService';
     K_USEPROXY = 'UseProxy';
     K_UPDATETIMEBYSERVER = 'UpdateTimeByServer';
     K_PROXY = 'Proxy';
     K_PROXYPORT = 'ProxyPort';
     K_USEPORTAUTHENTICATION = 'UseProxyAuthentication';
     K_PROXYUSERNAME = 'ProxyUserName';
     K_PROXYPASSWORD = 'ProxyPassword';
     K_IP = 'Ip';
     K_NETMASK = 'NetMask';
     K_DEFAULTGATEWAY = 'DefaultGateWay';
     K_DNS = 'DNS';
     K_TIMEOUT = 'TimeOut';
     K_HEARTBEATTIMER = 'HeartBeatTimer';
     K_MSGTIME = 'MsgTime';
     K_ERRORMSGTIME = 'ErrorMsgTime';
     K_RELAYTIME = 'RelayTime';
     K_DATETIMEFORMAT = 'DateTimeFormat';
     K_VOLUME = 'Volume';
     K_PROXREADER = 'ProxReader';
     K_BARCODEREADER = 'BarCodeReader';
     K_HID = 'HID';
     K_MENUACCESSCODE = 'MenuAccessCode';
     K_FPUNOTIDETIFYMESSAGE = 'FpuNotIdetifyMessage';
     K_NOTVALIDBYSERVERMESSAGE = 'NotValidByServerMessage';
     K_DUPLICATEMESSAGE = 'DuplicateMessage';
     K_AUTENTICATEMESSAGE = 'AutenticateMessage';
     K_OFFLINEVALIDATION = 'OfflineValidation';
     K_FINGERVERIFY = 'FingerVerify';
     K_ENABLEOFFLINERELAY = 'EnableOfflineRelay';
     K_ONEMINUTEVALIDATE = 'OneMinuteValidate';
     K_IDENTIDAD = 'Identidad';
     K_VERSION = 'Version';
     K_FINGERPRINT = 'FingerPrint';
     K_ENABLEKEYPAD = 'EnableKeyPad';
     K_SHOWPHOTO = 'ShowPhoto';

implementation

uses dSistema,
     ZAccesosTress,
     ZetaServerTools,
     ZAccesosMgr,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.dfm}

procedure TEditTerminales.Connect;
begin
     inherited;
     FBrowsing := False;
     with dmSistema do
     begin
          cdsTerminales.Conectar;
          DataSource.DataSet := cdsTerminales;

          TE_ZONA.ItemIndex := ObtenIndice(cdsTerminales.FieldByName( 'TE_ZONA' ).AsString);
          TE_APP.ItemIndex := cdsTerminales.FieldByName( 'TE_APP' ).AsInteger - 1;
          if( cdsTerminales.State in [ dsBrowse ] )then
          begin
              TE_MAC.Caption := Decrypt( cdsTerminales.FieldByName( 'TE_MAC' ).AsString );
          end;

          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end;
end;

procedure TEditTerminales.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_TERMINALES_EDIC;
     IndexDerechos := ZAccesosTress.D_SIST_TERMINALES;
     btnReiniciarTerminal.Enabled := CheckDerecho( D_SIST_TERMINALES, K_DERECHO_BANCA );
     FirstControl := TE_NOMBRE;

     FXML := TdmXMLTools.Create( nil );
end;

procedure TEditTerminales.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := tbDatosGenerales;
     PageControl.SetFocus;
     {
     TE_NOMBRE.SetFocus;

     with dmSistema.cdsTerminales do
     begin
          if not( State in [ dsInsert, dsEdit ] )then
             Edit;
     end;
     }
end;

procedure TEditTerminales.TE_ZONAChange(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsTerminales do
     begin
          if not (State in [dsInsert, dsEdit])then
             Edit;

          FieldByName( 'TE_ZONA' ).AsString := ObtenZona(TE_ZONA.ItemIndex);
          FieldByName( 'TE_ZONA_ES' ).AsString := ObtenZonaES(TE_ZONA.ItemIndex);
     end;
end;

procedure TEditTerminales.DBNavigatorClick(Sender: TObject;
  Button: TNavigateBtn);
begin
     inherited;
     with dmSistema.cdsTerminales do
     begin
          TE_ZONA.ItemIndex := ObtenIndice(FieldByName( 'TE_ZONA' ).AsString);
          TE_APP.ItemIndex := FieldByName( 'TE_APP' ).AsInteger - 1;
          TE_MAC.caption := Decrypt( FieldByName( 'TE_MAC' ).AsString );
          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end;
end;

procedure TEditTerminales.Agregar;
begin
     dmSistema.cdsTerminales.Agregar;
end;

function TEditTerminales.ObtenIndice(sZona: String): Integer;
begin
     if( sZona = 'Eastern Standard Time' )then
         result := 0
     else if( sZona = 'Central Standard Time' )then
         result := 1
     else if( sZona = 'Central Standard Time (Mexico)' )then
         result := 2
     else if( sZona = 'US Mountain Standard Time' )then
         result := 3
     else if( sZona = 'Mountain Standard Time (Mexico)' )then
         result := 4
     else if( sZona = 'Mountain Standard Time' )then
         result := 5
     else if( sZona = 'Pacific Standard Time' )then
         result := 6
     else if( sZona = 'Pacific Standard Time (Mexico)' )then
         result := 7
     else
         result := 0;
end;

function TEditTerminales.ObtenZona(iIndice: Integer): String;
begin
     if( iIndice = 0 )then
         result := 'Eastern Standard Time'
     else if( iIndice = 1 )then
         result := 'Central Standard Time'
     else if( iIndice = 2 )then
         result := 'Central Standard Time (Mexico)'
     else if( iIndice = 3 )then
         result := 'US Mountain Standard Time'
     else if( iIndice = 4 )then
         result := 'Mountain Standard Time (Mexico)'
     else if( iIndice = 5 )then
         result := 'Mountain Standard Time'
     else if( iIndice = 6 )then
         result := 'Pacific Standard Time'
     else if( iIndice = 7 )then
         result := 'Pacific Standard Time (Mexico)'
     else
         result := 'Pacific Standard Time';
end;
function TEditTerminales.ObtenZonaES(iIndice: Integer): String;
begin
     if( iIndice = 0 )then
         result := 'Eastern Standard Time'
     else if( iIndice = 1 )then
         result := 'Central Standard Time'
     else if( iIndice = 2 )then
         result := 'Central Standard Time (Mexico)'
     else if( iIndice = 3 )then
         result := 'US Mountain Standard Time'
     else if( iIndice = 4 )then
         result := 'Mountain Standard Time (Mexico)'
     else if( iIndice = 5 )then
         result := 'Mountain Standard Time'
     else if( iIndice = 6 )then
         result := 'Pacific Standard Time'
     else if( iIndice = 7 )then
         result := 'Pacific Standard Time (Mexico)'
     else
         result := 'Pacific Standard Time';
end;

procedure TEditTerminales.TE_APPChange(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsTerminales do
     begin
          if not (State in [dsInsert, dsEdit])then
             Edit;
          FieldByName( 'TE_APP' ).AsInteger := (TE_APP.ItemIndex + 1);
     end;
end;

procedure TEditTerminales.btnReiniciarTerminalClick(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     if ZConfirm( 'Sincronizar huellas de la terminal', 'Esta acci�n sincronizar� las huellas de la terminal actual' + CR_LF + '�Desea continuar?', 0, mbYes ) then
     begin
          dmSistema.ReiniciaTerminales( DataSource.DataSet.FieldByName( 'TE_CODIGO' ).AsInteger );
          ZInformation( 'Sincronizar huellas de la terminal', 'Terminal sincronizada', 0);
     end;
     {$endif}
end;

procedure TEditTerminales.ActualizaXML;
var
   sTxtNuevo: String;
begin
     sTxtNuevo := ActualizaTag( K_WEBSERVICE, WebService.Text, dmSistema.cdsTerminales.FieldByName( 'TE_XML' ).AsString );
     sTxtNuevo := ActualizaTag( K_IP, Ip.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_NETMASK, NetMask.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DEFAULTGATEWAY, DefaultGateWay.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DNS, DNS.Text, sTxtNuevo );
     if( UseProxy.Checked )then
         sTxtNuevo := ActualizaTag( K_USEPROXY, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_USEPROXY, 'false', sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXY, Proxy.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXYPORT, ProxyPort.Text, sTxtNuevo );
     if( UseProxyAuthentication.Checked )then
         sTxtNuevo := ActualizaTag( K_USEPORTAUTHENTICATION, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_USEPORTAUTHENTICATION, 'false', sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXYUSERNAME, ProxyUserName.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXYPASSWORD, ProxyPassword.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_VOLUME, IntToStr( Volume.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_MENUACCESSCODE, IntToStr( MenuAccessCode.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DATETIMEFORMAT, DateTimeFormat.Text, sTxtNuevo );
     if( UpdateTimeByServer.Checked )then
         sTxtNuevo := ActualizaTag( K_UPDATETIMEBYSERVER, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_UPDATETIMEBYSERVER, 'false', sTxtNuevo );
     if( ProxReader.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_PROXREADER, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_PROXREADER, 'OFF', sTxtNuevo );
     if( BarCodeReader.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_BARCODEREADER, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_BARCODEREADER, 'OFF', sTxtNuevo );
     if( HID.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_HID, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_HID, 'OFF', sTxtNuevo );

     if( FingerPrint.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_FINGERPRINT, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_FINGERPRINT, 'OFF', sTxtNuevo );

     if( FingerVerify.Checked )then
         sTxtNuevo := ActualizaTag( K_FINGERVERIFY, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_FINGERVERIFY, 'false', sTxtNuevo );

     if( ShowPhoto.Checked )then
         sTxtNuevo := ActualizaTag( K_SHOWPHOTO, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_SHOWPHOTO, 'false', sTxtNuevo );

     if( EnableKeyPad.Checked )then
         sTxtNuevo := ActualizaTag( K_ENABLEKEYPAD, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_ENABLEKEYPAD, 'false', sTxtNuevo );

     if( EnableOfflineRelay.Checked )then
         sTxtNuevo := ActualizaTag( K_ENABLEOFFLINERELAY, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_ENABLEOFFLINERELAY, 'false', sTxtNuevo );
     if( OneMinuteValidate.Checked )then
         sTxtNuevo := ActualizaTag( K_ONEMINUTEVALIDATE, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_ONEMINUTEVALIDATE, 'false', sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_FPUNOTIDETIFYMESSAGE, FpuNotIdetifyMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_NOTVALIDBYSERVERMESSAGE, NotValidByServerMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DUPLICATEMESSAGE, DuplicateMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_AUTENTICATEMESSAGE, AutenticateMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_OFFLINEVALIDATION, OfflineValidation.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_TIMEOUT, IntToStr( TimeOut.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_HEARTBEATTIMER, IntToStr( HeartBeatTimer.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_MSGTIME, IntToStr( MsgTime.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_ERRORMSGTIME, IntToStr( ErrorMsgTime.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_RELAYTIME, IntToStr( RelayTime.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_IDENTIDAD, IntToStr( TE_APP.ItemIndex + 1 ), sTxtNuevo );

     dmSistema.cdsTerminales.FieldByName( 'TE_XML' ).AsString := sTxtNuevo;
end;

procedure TEditTerminales.LeeXML;
var
   iTimeOut,
   iHeartBeatTimer, iMsgTime, iErrorMsgTime,
   iRelayTime, iVolume, iMenuAccessCode: Integer;

   sWebService, sProxy, sProxyPort, sProxyUserName,
   sProxyPassword, sIp, sNetMask,
   sDefaultGateWay, sDNS, sDateTimeFormat,
   sFpuNotIdetifyMessage, sNotValidByServerMessage,
   sDuplicateMessage, sAutenticateMessage, sOfflineValidation,
   sProxReader, sBarCodeReader, sHID, sVersion, sFingerPrint: String;

   lUseProxy, lUseProxyAuthentication, lUpdateTimeByServer,
   lFingerVerify, lEnableOfflineRelay, lOneMinuteValidate, lShowPhoto, lEnableKeyPad: Boolean;
begin
     with FXML do
     begin
          XMLAsText := dmSistema.cdsTerminales.FieldByName( 'TE_XML' ).AsString;
          sWebService := TagAsString( GetNode( K_WEBSERVICE ), VACIO );
          lUseProxy := ( TagAsString( GetNode( K_USEPROXY ), 'false' ) = 'true' );
          lUpdateTimeByServer := ( TagAsString( GetNode( K_UPDATETIMEBYSERVER ), 'false' ) = 'true' );
          lShowPhoto := ( TagAsString( GetNode( K_SHOWPHOTO ), 'false' ) = 'true' );
          lEnableKeyPad := ( TagAsString( GetNode( K_ENABLEKEYPAD ), 'false' ) = 'true' );
          sProxy := TagAsString( GetNode( K_PROXY ), VACIO );
          sProxyPort := TagAsString( GetNode( K_PROXYPORT ), VACIO );
          lUseProxyAuthentication := ( TagAsString( GetNode( K_USEPORTAUTHENTICATION ), 'false' ) = 'true' );
          sProxyUserName := TagAsString( GetNode( K_PROXYUSERNAME ), VACIO );
          sProxyPassword := TagAsString( GetNode( K_PROXYPASSWORD ), VACIO );
          sIp := TagAsString( GetNode( K_IP ), VACIO );
          sNetMask := TagAsString( GetNode( K_NETMASK ), VACIO );
          sDefaultGateWay := TagAsString( GetNode( K_DEFAULTGATEWAY ), VACIO );
          sDNS := TagAsString( GetNode( K_DNS ), VACIO );
          iTimeOut := TagAsInteger( GetNode( K_TIMEOUT ), 0 );
          iHeartBeatTimer := TagAsInteger( GetNode( K_HEARTBEATTIMER ), 0 );
          iMsgTime := TagAsInteger( GetNode( K_MSGTIME ), 0 );
          iErrorMsgTime := TagAsInteger( GetNode( K_ERRORMSGTIME ), 0 );
          iRelayTime := TagAsInteger( GetNode( K_RELAYTIME ), 0 );
          sDateTimeFormat := TagAsString( GetNode( K_DATETIMEFORMAT ), VACIO );
          iVolume := TagAsInteger( GetNode( K_VOLUME ), 0 );
          sProxReader := TagAsString( GetNode( K_PROXREADER ), VACIO );
          sBarCodeReader := TagAsString( GetNode( K_BARCODEREADER ), VACIO );
          sHID := TagAsString( GetNode( K_HID ), VACIO );
          sFingerPrint := TagAsString( GetNode( K_FINGERPRINT ), VACIO );
          iMenuAccessCode := TagAsInteger( GetNode( K_MENUACCESSCODE ), 0 );
          sFpuNotIdetifyMessage := TagAsString( GetNode( K_FPUNOTIDETIFYMESSAGE ), VACIO );
          sNotValidByServerMessage := TagAsString( GetNode( K_NOTVALIDBYSERVERMESSAGE ), VACIO );
          sDuplicateMessage := TagAsString( GetNode( K_DUPLICATEMESSAGE ), VACIO );
          sAutenticateMessage := TagAsString( GetNode( K_AUTENTICATEMESSAGE ), VACIO );
          sOfflineValidation := TagAsString( GetNode( K_OFFLINEVALIDATION ), VACIO );
          lFingerVerify := ( TagAsString( GetNode( K_FINGERVERIFY ), 'false' ) = 'true' );
          lEnableOfflineRelay := ( TagAsString( GetNode( K_ENABLEOFFLINERELAY ), 'false' ) = 'true' );
          lOneMinuteValidate := ( TagAsString( GetNode( K_ONEMINUTEVALIDATE ), 'false' ) = 'true' );
          sVersion := TagAsString( GetNode( K_VERSION ), VACIO );

          WebService.Text := sWebService;
          UseProxy.Checked := lUseProxy;
          UpdateTimeByServer.Checked := lUpdateTimeByServer;
          Proxy.Text := sProxy;
          ProxyPort.Text := sProxyPort;
          UseProxyAuthentication.Checked := lUseProxyAuthentication;
          ShowPhoto.Checked := lShowPhoto;
          EnableKeyPad.Checked := lEnableKeyPad;
          ProxyUserName.Text := sProxyUserName;
          ProxyPassword.Text := sProxyPassword;
          Ip.Text := sIp;
          NetMask.Text := sNetMask;
          DefaultGateWay.Text := sDefaultGateWay;
          DNS.Text := sDNS;
          TimeOut.Valor := iTimeOut;
          HeartBeatTimer.Valor := iHeartBeatTimer;
          MsgTime.Valor := iMsgTime;
          ErrorMsgTime.Valor := iErrorMsgTime;
          RelayTime.Valor := iRelayTime;
          DateTimeFormat.Text := sDateTimeFormat;
          Volume.Valor := iVolume;

          if( sProxReader = 'ON' )then
              ProxReader.ItemIndex := 0
          else
              ProxReader.ItemIndex := 1;

          if( sBarCodeReader = 'ON' )then
              BarCodeReader.ItemIndex := 0
          else
              BarCodeReader.ItemIndex := 1;

          if( sHID = 'ON' )then
              HID.ItemIndex := 0
          else
              HID.ItemIndex := 1;

          if( sFingerPrint = 'ON' )then
              FingerPrint.ItemIndex := 0
          else
              FingerPrint.ItemIndex := 1;

          MenuAccessCode.Valor := iMenuAccessCode;
          FpuNotIdetifyMessage.Text := sFpuNotIdetifyMessage;
          NotValidByServerMessage.Text := sNotValidByServerMessage;
          DuplicateMessage.Text := sDuplicateMessage;
          AutenticateMessage.Text := sAutenticateMessage;
          OfflineValidation.Text := sOfflineValidation;
          FingerVerify.Checked := lFingerVerify;
          EnableOfflineRelay.Checked := lEnableOfflineRelay;
          OneMinuteValidate.Checked := lOneMinuteValidate;
     end;
end;

procedure TEditTerminales.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FXML );
end;

procedure TEditTerminales.OKClick(Sender: TObject);
begin
     ActualizaXML;
     inherited;
end;

function TEditTerminales.ActualizaTag( sTag, sValor, sXML: String ): String;
var
   iPosIni, iPosFin, iLong, iPosProperties: Integer;
begin
     iPosIni := Pos( '<' + sTag + '>', sXML );
     iPosFin := Pos( '</' + sTag + '>', sXML );
     Result := sXML;
     if( ( iPosIni > 0 )and( iPosFin > 0 ) )then
     begin
          iLong := Length( sTag );
          Result := Copy( sXML, 1, iPosIni + 1 + iLong ) + sValor + Copy( sXML, iPosFin, Length( sXML ) );
     end
     else
     begin
          if ( sTag = K_FINGERPRINT ) then
          begin
               iPosProperties := Pos( '</properties>', sXML );
               Result := Copy( sXML, 1, iPosProperties - 1 ) + '<' + K_FINGERPRINT +'>' + sValor + '</' + K_FINGERPRINT + '>' + Copy( sXML, iPosProperties, Length( sXML ) );
          end
          else if ( sTag = K_ENABLEKEYPAD ) then
               begin
                    iPosProperties := Pos( '</properties>', sXML );
                    Result := Copy( sXML, 1, iPosProperties - 1 ) + '<' + K_ENABLEKEYPAD + '>' + sValor + '</' + K_ENABLEKEYPAD + '>' + Copy( sXML, iPosProperties, Length( sXML ) );
               end;
     end;
end;

procedure TEditTerminales.XMLChange(Sender: TObject);
begin
     inherited;
     if not FBrowsing then
     begin
          with dmSistema.cdsTerminales do
          begin
               if not( State in [ dsInsert, dsEdit ] )then
                  Edit;
          end;
     end;
end;

procedure TEditTerminales.CancelarClick(Sender: TObject);
begin
     if( Cancelar.Caption = '&Cancelar' )then
     begin
          inherited;
          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end
     else
         inherited;
end;

end.
