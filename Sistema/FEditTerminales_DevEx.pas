unit FEditTerminales_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ComCtrls, Mask, ZetaNumero, ZetaEdit, ZetaFecha, ZetaKeyCombo,
  ZetaDBTextBox, DXMLTools, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer,
  cxEdit, cxTextEdit, cxMemo, cxDBEdit, dxBarBuiltInMenu;

type
  TEditTerminales_DevEx = class(TBaseEdicion_DevEx)
    PageControl_DevEx: TcxPageControl;
    tbDatosGenerales_DevEx: TcxTabSheet;
    tbConfiguracion_DevEx: TcxTabSheet;
    TabSheet1_DevEx: TcxTabSheet;
    TabSheet3_DevEx: TcxTabSheet;
    TabSheet6_DevEx: TcxTabSheet;
    TabSheet2_DevEx: TcxTabSheet;
    TE_TEXTO: TDBEdit;
    TE_NOMBRE: TDBEdit;
    TE_APP: TZetaKeyCombo;
    TE_ZONA: TZetaKeyCombo;
    TE_ACTIVO: TDBCheckBox;
    TE_NUMERO: TZetaDBNumero;
    TE_MAC: TZetaTextBox;
    TE_BEAT: TZetaDBTextBox;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    EnableKeyPad: TCheckBox;
    ShowPhoto: TCheckBox;
    DateTimeFormat: TEdit;
    Volume: TZetaNumero;
    MenuAccessCode: TZetaNumero;
    Label29: TLabel;
    Label28: TLabel;
    Label11: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    ProxyPassword: TEdit;
    ProxyUserName: TEdit;
    ProxyPort: TEdit;
    Proxy: TEdit;
    DNS: TEdit;
    DefaultGateWay: TEdit;
    NetMask: TEdit;
    Ip: TEdit;
    WebService: TEdit;
    UseProxyAuthentication: TCheckBox;
    UseProxy: TCheckBox;
    Label53: TLabel;
    Label52: TLabel;
    Label51: TLabel;
    Label50: TLabel;
    Label49: TLabel;
    Label48: TLabel;
    Label47: TLabel;
    Label46: TLabel;
    Label45: TLabel;
    Label44: TLabel;
    Label14: TLabel;
    FingerPrint: TComboBox;
    OneMinuteValidate: TCheckBox;
    OfflineValidation: TEdit;
    AutenticateMessage: TEdit;
    DuplicateMessage: TEdit;
    NotValidByServerMessage: TEdit;
    FpuNotIdetifyMessage: TEdit;
    HID: TComboBox;
    EnableOfflineRelay: TCheckBox;
    FingerVerify: TCheckBox;
    BarCodeReader: TComboBox;
    ProxReader: TComboBox;
    Label30: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label27: TLabel;
    Label26: TLabel;
    Label25: TLabel;
    Label24: TLabel;
    Label9: TLabel;
    Label23: TLabel;
    Label22: TLabel;
    ErrorMsgTime: TZetaNumero;
    RelayTime: TZetaNumero;
    MsgTime: TZetaNumero;
    HeartBeatTimer: TZetaNumero;
    TimeOut: TZetaNumero;
    Label21: TLabel;
    Label34: TLabel;
    Label33: TLabel;
    Label20: TLabel;
    Label19: TLabel;
    btnReiniciarTerminal_DevEx: TcxButton;
    GroupBox10: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox11: TGroupBox;
    cxDBMemo_XML: TcxDBMemo;
    TE_CODIGO: TZetaDBTextBox;
    Label31: TLabel;
    TE_VERSION: TZetaDBTextBox;
    Label32: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    Label35: TLabel;
    TE_IP: TZetaDBTextBox;
    Label36: TLabel;
    TE_HUELLAS: TZetaDBTextBox;
    Label37: TLabel;
    MiFare: TComboBox;
    Label38: TLabel;
    ZetaDBTextBox2: TZetaDBTextBox;
    DetalleBitacora: TCheckBox;
    Label10: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TE_ZONAChange(Sender: TObject);
    procedure TE_APPChange(Sender: TObject);
    procedure btnReiniciarTerminalClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure XMLChange(Sender: TObject);
    procedure btnReiniciarTerminal_DevExClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure DevEx_cxDBNavigatorEdicionButtonsButtonClick(Sender: TObject;
      AButtonIndex: Integer; var ADone: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    FXML: TdmXMLTools;
    FBrowsing: Boolean;
    FActualizaMAC: Boolean;
    Procedure ActualizaMAC;
    function ObtenIndice( sZona: String ): Integer;
    function ObtenZona( iIndice: Integer ): String;
    function ObtenZonaES( iIndice: Integer ): String;
    function ActualizaTag( sTag, sValor, sXML: String ): String;
    procedure ActualizaXML;
    procedure LeeXML;
    procedure HabilitarControles(lEnabled : Boolean);
    procedure ReconocimientoFacial;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
  end;

var
  EditTerminales_DevEx: TEditTerminales_DevEx;

const
     K_WEBSERVICE = 'WebService';
     K_USEPROXY = 'UseProxy';
     K_UPDATETIMEBYSERVER = 'UpdateTimeByServer';
     K_PROXY = 'Proxy';
     K_PROXYPORT = 'ProxyPort';
     K_USEPORTAUTHENTICATION = 'UseProxyAuthentication';
     K_PROXYUSERNAME = 'ProxyUserName';
     K_PROXYPASSWORD = 'ProxyPassword';
     K_IP = 'Ip';
     K_NETMASK = 'NetMask';
     K_DEFAULTGATEWAY = 'DefaultGateWay';
     K_DNS = 'DNS';
     K_TIMEOUT = 'TimeOut';
     K_HEARTBEATTIMER = 'HeartBeatTimer';
     K_MSGTIME = 'MsgTime';
     K_ERRORMSGTIME = 'ErrorMsgTime';
     K_RELAYTIME = 'RelayTime';
     K_DATETIMEFORMAT = 'DateTimeFormat';
     K_VOLUME = 'Volume';
     K_PROXREADER = 'ProxReader';
     K_BARCODEREADER = 'BarCodeReader';
     K_HID = 'HID';
     K_MENUACCESSCODE = 'MenuAccessCode';
     K_FPUNOTIDETIFYMESSAGE = 'FpuNotIdetifyMessage';
     K_NOTVALIDBYSERVERMESSAGE = 'NotValidByServerMessage';
     K_DUPLICATEMESSAGE = 'DuplicateMessage';
     K_AUTENTICATEMESSAGE = 'AutenticateMessage';
     K_OFFLINEVALIDATION = 'OfflineValidation';
     K_FINGERVERIFY = 'FingerVerify';
     K_ENABLEOFFLINERELAY = 'EnableOfflineRelay';
     K_ONEMINUTEVALIDATE = 'OneMinuteValidate';
     K_IDENTIDAD = 'Identidad';
     K_VERSION = 'Version';
     K_FINGERPRINT = 'FingerPrint';
     K_ENABLEKEYPAD = 'EnableKeyPad';
     K_SHOWPHOTO = 'ShowPhoto';
     K_MIFARE = 'MifareReader';
     K_DETALLEBITACORA = 'DetalleBitacora';

implementation

uses dSistema,
     ZAccesosTress,
     ZetaServerTools,
     ZAccesosMgr,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaDialogo;

{$R *.dfm}

procedure TEditTerminales_DevEx.Connect;
begin
     inherited;
     FBrowsing := False;
     FActualizaMAC := False; //DevEx (by am): Agregada como bandera para efectuarl el cambio de MAC al navegar
     with dmSistema do
     begin
          cdsTerminales.Conectar;
          DataSource.DataSet := cdsTerminales;

          TE_ZONA.ItemIndex := ObtenIndice(cdsTerminales.FieldByName( 'TE_ZONA' ).AsString);
          TE_APP.ItemIndex := cdsTerminales.FieldByName( 'TE_APP' ).AsInteger - 1;
          if( cdsTerminales.State in [ dsBrowse ] )then
          begin
              TE_MAC.Caption := Decrypt( cdsTerminales.FieldByName( 'TE_MAC' ).AsString );
          end;

          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end;
end;

procedure TEditTerminales_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_TERMINALES_EDIC;
     IndexDerechos := ZAccesosTress.D_SIST_TERMINALES;
     btnReiniciarTerminal_DevEx.Enabled := CheckDerecho( D_SIST_TERMINALES, K_DERECHO_BANCA );
     FirstControl := TE_NOMBRE;

     FXML := TdmXMLTools.Create( nil );
end;

procedure TEditTerminales_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     PageControl_DevEx.ActivePage := tbDatosGenerales_DevEx;
     PageControl_DevEx.SetFocus;
     {
     TE_NOMBRE.SetFocus;

     with dmSistema.cdsTerminales do
     begin
          if not( State in [ dsInsert, dsEdit ] )then
             Edit;
     end;
     }
     ReconocimientoFacial;
end;

procedure TEditTerminales_DevEx.TE_ZONAChange(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsTerminales do
     begin
          if not (State in [dsInsert, dsEdit])then
             Edit;

          FieldByName( 'TE_ZONA' ).AsString := ObtenZona(TE_ZONA.ItemIndex);
          FieldByName( 'TE_ZONA_ES' ).AsString := ObtenZonaES(TE_ZONA.ItemIndex);
     end;
end;

procedure TEditTerminales_DevEx.Agregar;
begin
     dmSistema.cdsTerminales.Agregar;
end;

function TEditTerminales_DevEx.ObtenIndice(sZona: String): Integer;
begin
     if( sZona = 'Eastern Standard Time' )then
         result := 0
     else if( sZona = 'Central Standard Time' )then
         result := 1
     else if( sZona = 'Central Standard Time (Mexico)' )then
         result := 2
     else if( sZona = 'US Mountain Standard Time' )then
         result := 3
     else if( sZona = 'Mountain Standard Time (Mexico)' )then
         result := 4
     else if( sZona = 'Mountain Standard Time' )then
         result := 5
     else if( sZona = 'Pacific Standard Time' )then
         result := 6
     else if( sZona = 'Pacific Standard Time (Mexico)' )then
         result := 7
     else
         result := 0;
end;

function TEditTerminales_DevEx.ObtenZona(iIndice: Integer): String;
begin
     if( iIndice = 0 )then
         result := 'Eastern Standard Time'
     else if( iIndice = 1 )then
         result := 'Central Standard Time'
     else if( iIndice = 2 )then
         result := 'Central Standard Time (Mexico)'
     else if( iIndice = 3 )then
         result := 'US Mountain Standard Time'
     else if( iIndice = 4 )then
         result := 'Mountain Standard Time (Mexico)'
     else if( iIndice = 5 )then
         result := 'Mountain Standard Time'
     else if( iIndice = 6 )then
         result := 'Pacific Standard Time'
     else if( iIndice = 7 )then
         result := 'Pacific Standard Time (Mexico)'
     else
         result := 'Pacific Standard Time';
end;

function TEditTerminales_DevEx.ObtenZonaES(iIndice: Integer): String;
begin
     if( iIndice = 0 )then
         result := 'Eastern Standard Time'
     else if( iIndice = 1 )then
         result := 'Central Standard Time'
     else if( iIndice = 2 )then
         result := 'Central Standard Time (Mexico)'
     else if( iIndice = 3 )then
         result := 'US Mountain Standard Time'
     else if( iIndice = 4 )then
         result := 'Mountain Standard Time (Mexico)'
     else if( iIndice = 5 )then
         result := 'Mountain Standard Time'
     else if( iIndice = 6 )then
         result := 'Pacific Standard Time'
     else if( iIndice = 7 )then
         result := 'Pacific Standard Time (Mexico)'
     else
         result := 'Pacific Standard Time';
end;

procedure TEditTerminales_DevEx.TE_APPChange(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsTerminales do
     begin
          if not (State in [dsInsert, dsEdit])then
             Edit;
          FieldByName( 'TE_APP' ).AsInteger := (TE_APP.ItemIndex + 1);
     end;
end;

procedure TEditTerminales_DevEx.btnReiniciarTerminalClick(Sender: TObject);
begin
     inherited;
     {$ifndef DOS_CAPAS}
     if ZConfirm( 'Sincronizar plantillas de la terminal', 'Esta acci�n sincronizar� las plantillas de la terminal actual' + CR_LF + '�Desea continuar?', 0, mbYes ) then
     begin
          dmSistema.ReiniciaTerminales( DataSource.DataSet.FieldByName( 'TE_CODIGO' ).AsInteger );
          ZInformation( 'Sincronizar plantillas de la terminal', 'Terminal sincronizada', 0);
     end;
     {$endif}
end;

procedure TEditTerminales_DevEx.ActualizaXML;
var
   sTxtNuevo: String;
begin
     sTxtNuevo := ActualizaTag( K_WEBSERVICE, WebService.Text, dmSistema.cdsTerminales.FieldByName( 'TE_XML' ).AsString );
     sTxtNuevo := ActualizaTag( K_IP, Ip.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_NETMASK, NetMask.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DEFAULTGATEWAY, DefaultGateWay.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DNS, DNS.Text, sTxtNuevo );
     if( UseProxy.Checked )then
         sTxtNuevo := ActualizaTag( K_USEPROXY, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_USEPROXY, 'false', sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXY, Proxy.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXYPORT, ProxyPort.Text, sTxtNuevo );
     if( UseProxyAuthentication.Checked )then
         sTxtNuevo := ActualizaTag( K_USEPORTAUTHENTICATION, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_USEPORTAUTHENTICATION, 'false', sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXYUSERNAME, ProxyUserName.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_PROXYPASSWORD, ProxyPassword.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_VOLUME, IntToStr( Volume.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_MENUACCESSCODE, IntToStr( MenuAccessCode.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DATETIMEFORMAT, DateTimeFormat.Text, sTxtNuevo );
     if( ProxReader.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_PROXREADER, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_PROXREADER, 'OFF', sTxtNuevo );
     if( BarCodeReader.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_BARCODEREADER, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_BARCODEREADER, 'OFF', sTxtNuevo );
     if( HID.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_HID, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_HID, 'OFF', sTxtNuevo );

     if( FingerPrint.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_FINGERPRINT, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_FINGERPRINT, 'OFF', sTxtNuevo );

     if( MiFare.ItemIndex = 0 )then
         sTxtNuevo := ActualizaTag( K_MIFARE, 'ON', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_MIFARE, 'OFF', sTxtNuevo );

     if( FingerVerify.Checked )then
         sTxtNuevo := ActualizaTag( K_FINGERVERIFY, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_FINGERVERIFY, 'false', sTxtNuevo );

     if( ShowPhoto.Checked )then
         sTxtNuevo := ActualizaTag( K_SHOWPHOTO, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_SHOWPHOTO, 'false', sTxtNuevo );

     if( EnableKeyPad.Checked )then
         sTxtNuevo := ActualizaTag( K_ENABLEKEYPAD, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_ENABLEKEYPAD, 'false', sTxtNuevo );

     if( EnableOfflineRelay.Checked )then
         sTxtNuevo := ActualizaTag( K_ENABLEOFFLINERELAY, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_ENABLEOFFLINERELAY, 'false', sTxtNuevo );
     if( OneMinuteValidate.Checked )then
         sTxtNuevo := ActualizaTag( K_ONEMINUTEVALIDATE, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_ONEMINUTEVALIDATE, 'false', sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_FPUNOTIDETIFYMESSAGE, FpuNotIdetifyMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_NOTVALIDBYSERVERMESSAGE, NotValidByServerMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_DUPLICATEMESSAGE, DuplicateMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_AUTENTICATEMESSAGE, AutenticateMessage.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_OFFLINEVALIDATION, OfflineValidation.Text, sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_TIMEOUT, IntToStr( TimeOut.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_HEARTBEATTIMER, IntToStr( HeartBeatTimer.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_MSGTIME, IntToStr( MsgTime.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_ERRORMSGTIME, IntToStr( ErrorMsgTime.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_RELAYTIME, IntToStr( RelayTime.ValorEntero ), sTxtNuevo );
     sTxtNuevo := ActualizaTag( K_IDENTIDAD, IntToStr( TE_APP.ItemIndex + 1 ), sTxtNuevo );

     if( DetalleBitacora.Checked )then
         sTxtNuevo := ActualizaTag( K_DETALLEBITACORA, 'true', sTxtNuevo )
     else
         sTxtNuevo := ActualizaTag( K_DETALLEBITACORA, 'false', sTxtNuevo );

     dmSistema.cdsTerminales.FieldByName( 'TE_XML' ).AsString := sTxtNuevo;
end;

procedure TEditTerminales_DevEx.ReconocimientoFacial;
var lEnabled : Boolean;
begin
     lEnabled := True;
     if dmSistema.cdsTerminales.FieldByName('TE_ID').AsInteger = ord (eitReconocimientoFacial) then
        lEnabled := False;

     HabilitarControles(lEnabled);
end;

procedure TEditTerminales_DevEx.HabilitarControles(lEnabled : Boolean);
begin
     //Informaci�n del reloj
     Label16.Enabled := lEnabled;
     Volume.Enabled := lEnabled;
     MenuAccesscode.Enabled := lEnabled;
     Label10.Enabled := lEnabled; // Prueba
     Label15.Enabled := lEnabled;
     DetalleBitacora.Enabled := lEnabled;
     Label28.Enabled := lEnabled;
     ShowPhoto.Enabled := lEnabled;

     //Configuraci�n de red
     Label44.Enabled := lEnabled;
     UseProxy.Enabled := lEnabled;
     Label45.Enabled := lEnabled;
     Proxy.Enabled := lEnabled;
     Label46.Enabled := lEnabled;
     ProxyPort.Enabled := lEnabled;
     Label47.Enabled := lEnabled;
     UseProxyAuthentication.Enabled := lEnabled;
     Label48.Enabled := lEnabled;
     ProxyUserName.Enabled := lEnabled;
     Label49.Enabled := lEnabled;
     ProxyPassword.Enabled := lEnabled;

     //Dispositivos, validaciones y mensajes
     Label23.Enabled := lEnabled;
     ProxReader.Enabled := lEnabled;
     Label22.Enabled := lEnabled;
     BarCodeReader.Enabled := lEnabled;
     Label18.Enabled := lEnabled;
     HID.Enabled := lEnabled;
     Label30.Enabled := lEnabled;
     FingerPrint.Enabled := lEnabled;
     Label37.Enabled := lEnabled;
     MiFare.Enabled := lEnabled;
     Label12.Enabled := lEnabled;
     FingerVerify.Enabled := lEnabled;
end;
procedure TEditTerminales_DevEx.LeeXML;
var
   iTimeOut,
   iHeartBeatTimer, iMsgTime, iErrorMsgTime,
   iRelayTime, iVolume, iMenuAccessCode: Integer;

   sWebService, sProxy, sProxyPort, sProxyUserName,
   sProxyPassword, sIp, sNetMask,
   sDefaultGateWay, sDNS, sDateTimeFormat,
   sFpuNotIdetifyMessage, sNotValidByServerMessage,
   sDuplicateMessage, sAutenticateMessage, sOfflineValidation,
   sProxReader, sBarCodeReader, sHID, sVersion, sFingerPrint, sMiFARE: String;

   lUseProxy, lUseProxyAuthentication, lUpdateTimeByServer,  lDetalleBitacora,
   lFingerVerify, lEnableOfflineRelay, lOneMinuteValidate, lShowPhoto, lEnableKeyPad: Boolean;
begin
     with FXML do
     begin
          XMLAsText := dmSistema.cdsTerminales.FieldByName( 'TE_XML' ).AsString;
          sWebService := TagAsString( GetNode( K_WEBSERVICE ), VACIO );
          lUseProxy := ( TagAsString( GetNode( K_USEPROXY ), 'false' ) = 'true' );
          lDetalleBitacora := ( TagAsString( GetNode( K_DETALLEBITACORA ), 'false' ) = 'true' );
          lShowPhoto := ( TagAsString( GetNode( K_SHOWPHOTO ), 'false' ) = 'true' );
          lEnableKeyPad := ( TagAsString( GetNode( K_ENABLEKEYPAD ), 'false' ) = 'true' );
          sProxy := TagAsString( GetNode( K_PROXY ), VACIO );
          sProxyPort := TagAsString( GetNode( K_PROXYPORT ), VACIO );
          lUseProxyAuthentication := ( TagAsString( GetNode( K_USEPORTAUTHENTICATION ), 'false' ) = 'true' );
          sProxyUserName := TagAsString( GetNode( K_PROXYUSERNAME ), VACIO );
          sProxyPassword := TagAsString( GetNode( K_PROXYPASSWORD ), VACIO );
          sIp := TagAsString( GetNode( K_IP ), VACIO );
          sNetMask := TagAsString( GetNode( K_NETMASK ), VACIO );
          sDefaultGateWay := TagAsString( GetNode( K_DEFAULTGATEWAY ), VACIO );
          sDNS := TagAsString( GetNode( K_DNS ), VACIO );
          iTimeOut := TagAsInteger( GetNode( K_TIMEOUT ), 0 );
          iHeartBeatTimer := TagAsInteger( GetNode( K_HEARTBEATTIMER ), 0 );
          iMsgTime := TagAsInteger( GetNode( K_MSGTIME ), 0 );
          iErrorMsgTime := TagAsInteger( GetNode( K_ERRORMSGTIME ), 0 );
          iRelayTime := TagAsInteger( GetNode( K_RELAYTIME ), 0 );
          sDateTimeFormat := TagAsString( GetNode( K_DATETIMEFORMAT ), VACIO );
          iVolume := TagAsInteger( GetNode( K_VOLUME ), 0 );
          sProxReader := TagAsString( GetNode( K_PROXREADER ), VACIO );
          sBarCodeReader := TagAsString( GetNode( K_BARCODEREADER ), VACIO );
          sHID := TagAsString( GetNode( K_HID ), VACIO );
          sFingerPrint := TagAsString( GetNode( K_FINGERPRINT ), VACIO );
          sMiFARE := TagAsString( GetNode( K_MIFARE ), VACIO );
          iMenuAccessCode := TagAsInteger( GetNode( K_MENUACCESSCODE ), 0 );
          sFpuNotIdetifyMessage := TagAsString( GetNode( K_FPUNOTIDETIFYMESSAGE ), VACIO );
          sNotValidByServerMessage := TagAsString( GetNode( K_NOTVALIDBYSERVERMESSAGE ), VACIO );
          sDuplicateMessage := TagAsString( GetNode( K_DUPLICATEMESSAGE ), VACIO );
          sAutenticateMessage := TagAsString( GetNode( K_AUTENTICATEMESSAGE ), VACIO );
          sOfflineValidation := TagAsString( GetNode( K_OFFLINEVALIDATION ), VACIO );
          lFingerVerify := ( TagAsString( GetNode( K_FINGERVERIFY ), 'false' ) = 'true' );
          lEnableOfflineRelay := ( TagAsString( GetNode( K_ENABLEOFFLINERELAY ), 'false' ) = 'true' );
          lOneMinuteValidate := ( TagAsString( GetNode( K_ONEMINUTEVALIDATE ), 'false' ) = 'true' );
          sVersion := TagAsString( GetNode( K_VERSION ), VACIO );

          WebService.Text := sWebService;
          UseProxy.Checked := lUseProxy;
          DetalleBitacora.Checked := lDetalleBitacora;
          Proxy.Text := sProxy;
          ProxyPort.Text := sProxyPort;
          UseProxyAuthentication.Checked := lUseProxyAuthentication;
          ShowPhoto.Checked := lShowPhoto;
          EnableKeyPad.Checked := lEnableKeyPad;
          ProxyUserName.Text := sProxyUserName;
          ProxyPassword.Text := sProxyPassword;
          Ip.Text := sIp;
          NetMask.Text := sNetMask;
          DefaultGateWay.Text := sDefaultGateWay;
          DNS.Text := sDNS;
          TimeOut.Valor := iTimeOut;
          HeartBeatTimer.Valor := iHeartBeatTimer;
          MsgTime.Valor := iMsgTime;
          ErrorMsgTime.Valor := iErrorMsgTime;
          RelayTime.Valor := iRelayTime;
          DateTimeFormat.Text := sDateTimeFormat;
          Volume.Valor := iVolume;

          if( sProxReader = 'ON' )then
              ProxReader.ItemIndex := 0
          else
              ProxReader.ItemIndex := 1;

          if( sBarCodeReader = 'ON' )then
              BarCodeReader.ItemIndex := 0
          else
              BarCodeReader.ItemIndex := 1;

          if( sHID = 'ON' )then
              HID.ItemIndex := 0
          else
              HID.ItemIndex := 1;

          if( sFingerPrint = 'ON' )then
              FingerPrint.ItemIndex := 0
          else
              FingerPrint.ItemIndex := 1;

          if( sMiFARE = 'ON' )then
              MiFARE.ItemIndex := 0
          else
              MiFARE.ItemIndex := 1;

          MenuAccessCode.Valor := iMenuAccessCode;
          FpuNotIdetifyMessage.Text := sFpuNotIdetifyMessage;
          NotValidByServerMessage.Text := sNotValidByServerMessage;
          DuplicateMessage.Text := sDuplicateMessage;
          AutenticateMessage.Text := sAutenticateMessage;
          OfflineValidation.Text := sOfflineValidation;
          FingerVerify.Checked := lFingerVerify;
          EnableOfflineRelay.Checked := lEnableOfflineRelay;
          OneMinuteValidate.Checked := lOneMinuteValidate;
     end;
end;

procedure TEditTerminales_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FXML );
end;

{procedure TEditTerminales_DevEx.OKClick(Sender: TObject);
begin
     ActualizaXML;
     inherited;
end;}

function TEditTerminales_DevEx.ActualizaTag( sTag, sValor, sXML: String ): String;
var
   iPosIni, iPosFin, iLong, iPosProperties: Integer;
begin
     iPosIni := Pos( '<' + sTag + '>', sXML );
     iPosFin := Pos( '</' + sTag + '>', sXML );
     Result := sXML;
     if( ( iPosIni > 0 )and( iPosFin > 0 ) )then
     begin
          iLong := Length( sTag );
          Result := Copy( sXML, 1, iPosIni + 1 + iLong ) + sValor + Copy( sXML, iPosFin, Length( sXML ) );
     end
     else
     begin
          if ( sTag = K_FINGERPRINT ) then
          begin
               iPosProperties := Pos( '</properties>', sXML );
               Result := Copy( sXML, 1, iPosProperties - 1 ) + '<' + K_FINGERPRINT +'>' + sValor + '</' + K_FINGERPRINT + '>' + Copy( sXML, iPosProperties, Length( sXML ) );
          end
          else if ( sTag = K_ENABLEKEYPAD ) then
               begin
                    iPosProperties := Pos( '</properties>', sXML );
                    Result := Copy( sXML, 1, iPosProperties - 1 ) + '<' + K_ENABLEKEYPAD + '>' + sValor + '</' + K_ENABLEKEYPAD + '>' + Copy( sXML, iPosProperties, Length( sXML ) );
               end;
     end;
end;

procedure TEditTerminales_DevEx.XMLChange(Sender: TObject);
begin
     inherited;
     if not FBrowsing then
     begin
          with dmSistema.cdsTerminales do
          begin
               if not( State in [ dsInsert, dsEdit ] )then
                  Edit;
          end;
     end;
end;

{procedure TEditTerminales_DevEx.CancelarClick(Sender: TObject);
begin
     if( Cancelar.Caption = '&Cancelar' )then
     begin
          inherited;
          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end
     else
         inherited;
end; }

procedure TEditTerminales_DevEx.btnReiniciarTerminal_DevExClick(
  Sender: TObject);
begin
  {$ifndef DOS_CAPAS}
     if ZConfirm( 'Sincronizar plantillas de la terminal', 'Esta acci�n sincronizar� las plantillas de la terminal actual' + CR_LF + '�Desea continuar?', 0, mbYes ) then
     begin
          dmSistema.ReiniciaTerminales( DataSource.DataSet.FieldByName( 'TE_CODIGO' ).AsInteger );
          ZInformation( 'Sincronizar plantillas de la terminal', 'Terminal sincronizada', 0);
     end;
  {$endif}

end;

procedure TEditTerminales_DevEx.OK_DevExClick(Sender: TObject);
begin
  ActualizaXML;
     inherited;
end;

procedure TEditTerminales_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  if( Cancelar_DevEx.Caption = '&Cancelar' )then
     begin
          inherited;
          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end
     else
         inherited;

end;

procedure TEditTerminales_DevEx.DevEx_cxDBNavigatorEdicionButtonsButtonClick(
  Sender: TObject; AButtonIndex: Integer; var ADone: Boolean);
begin
  inherited;
  FActualizaMAC:= True;
end;

Procedure TEditTerminales_DevEx.ActualizaMAC;
begin
     with dmSistema.cdsTerminales do
     begin
          TE_ZONA.ItemIndex := ObtenIndice(FieldByName( 'TE_ZONA' ).AsString);
          TE_APP.ItemIndex := FieldByName( 'TE_APP' ).AsInteger - 1;
          TE_MAC.caption := Decrypt( FieldByName( 'TE_MAC' ).AsString );
          FBrowsing := True;
          Self.LeeXML;
          FBrowsing := False;
     end;
end;

{***DevEx(by am): Anteriormente la actualizacion del campo TE_MAC se raelizaba directamente en el evento onBuutonClick del Navigator
                  para la nueva imagen se modifico pues el navegador cambia de registro (en el datasource) despuesde ejecutar el codigo
                  dentro del evento. Mientras que el codigo para el cambio de MAC debe ejecutarse despues de que el cds a cambiado de record. ***}
procedure TEditTerminales_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
  if FActualizaMAC then
  begin
       ActualizaMAC;
       FActualizaMAC := False;
  end;

  ReconocimientoFacial;
end;

end.
