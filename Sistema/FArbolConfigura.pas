unit FArbolConfigura;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Gauges,
     ComCtrls, ImgList, Menus, ActnList, ToolWin, DB,
     ZBaseDlgModal,
     FTressShell;

type
  TArbolConfigura = class(TZetaDlgModal)
    SistemaGB: TGroupBox;
    ArbolSistema: TTreeView;
    Panel2: TPanel;
    Gauge: TGauge;
    UsuarioGB: TGroupBox;
    ArbolUsuario: TTreeView;
    Panel3: TPanel;
    IncluirSistCB: TCheckBox;
    PopUsuaio: TPopupMenu;
    LimpiarUsuario: TMenuItem;
    BorraRama1: TMenuItem;
    NodoDefaultItem: TMenuItem;
    N1: TMenuItem;
    BuscarRama1: TMenuItem;
    PopSistema: TPopupMenu;
    BuscarRama2: TMenuItem;
    N2: TMenuItem;
    Renombrar1: TMenuItem;
    ArbolDefault: TTreeView;
    Label1: TLabel;
    ActionListArbol: TActionList;
    _BorraTodo: TAction;
    _BorraRama: TAction;
    _Renombrar: TAction;
    _NodoDefault: TAction;
    _BuscaRamaUsuario: TAction;
    _BuscaRamaSistema: TAction;
    _AgregaRama: TAction;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    ToolButton1: TToolButton;
    ToolBar2: TToolBar;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton5: TToolButton;
    ToolButton10: TToolButton;
    ToolButton7: TToolButton;
    ExportaDlg: TSaveDialog;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    _Exportar: TAction;
    _Importar: TAction;
    ToolButton13: TToolButton;
    ImportaDlg: TOpenDialog;
    AplicaVistaClasica: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArbolUsuarioDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ArbolUsuarioDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolUsuarioEdited(Sender: TObject; Node: TTreeNode; var S: String);
    procedure ArbolUsuarioMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ArbolDefaultDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolDefaultDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ArbolSistemaDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolSistemaDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure BorraRama( oNodo : TTreeNode );
    procedure OKClick(Sender: TObject);
    procedure IncluirSistCBClick(Sender: TObject);
    procedure _BorraTodoExecute(Sender: TObject);
    procedure _BorraRamaExecute(Sender: TObject);
    procedure _RenombrarExecute(Sender: TObject);
    procedure _BuscaRamaUsuarioExecute(Sender: TObject);
    procedure _BuscaRamaSistemaExecute(Sender: TObject);
    procedure _NodoDefaultExecute(Sender: TObject);
    procedure _AgregaRamaExecute(Sender: TObject);
    procedure _ExportarExecute(Sender: TObject);
    procedure _ImportarExecute(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios: Boolean;
    FUsuario: Integer;
    procedure CreaArbolSistema;
    procedure CreaArbolUsuario( const sFileName: String );
    procedure CopiaHijos(oNodoOrigen, oNodoDestino: TTreeNode);
    procedure HuboCambios;
    procedure CambiaBotones;
    procedure CopiaNodoDefault;
    procedure DescargaArbol;
    function  NumFormaDefault : Integer;
  public
    { Public declarations }
  end;

var
  ArbolConfigura: TArbolConfigura;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZArbolFind,
     ZArbolTools,
     ZArbolTress,
     ZetaCommonLists,
     DSistema,
     DCliente,
     ZBaseShell,ZetaRegistryCliente;

{$R *.DFM}

procedure TArbolConfigura.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80816_Usuarios_Preferencias;
end;

procedure TArbolConfigura.FormShow(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          FUsuario := dmSistema.UsuarioPosicionado;
          {
          IncluirSistCB.Checked := ConvierteBool( cdsUsuarios.FieldByName( 'US_ARBOL' ).AsString );
          }
          Caption  := 'Configuraci�n de Arbol: ' + cdsUsuarios.FieldByName( 'US_NOMBRE' ).AsString;

          //Para que se quede seleccionado el checkbox de la vista clasica en caso de que este configurada
           AplicaVistaClasica.Enabled :=  ClientRegistry.CanWrite;
           AplicaVistaClasica.Checked := ( cdsUsuarios.FieldByName( 'US_CLASICA' ).AsString = K_GLOBAL_SI );
           {$IFDEF PRESUPUESTOS}
                   AplicaVistaClasica.Visible := False;
           {$ENDIF}
     end;
     with TressShell do
     begin
          //Para que utilice los iconos de la vista clasica
          ArbolSistema.Images := ArbolImages;
          ArbolUsuario.Images := ArbolImages;
          ArbolDefault.Images := ArbolImages;
     end;
     CreaArbolSistema;
     CreaArbolUsuario( '' );
     ArbolSistema.Selected := ArbolSistema.Items.GetFirstNode;
     FHuboCambios := False;
     CambiaBotones;
end;

procedure TArbolConfigura.CreaArbolSistema;
var
   oArbolMgr: TArbolMgr;
begin
     oArbolMgr := TArbolMgr.Create( ArbolSistema );
     with oArbolMgr do
     begin
          try
             Configuracion := True;
             with Arbol.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // ****** Arbol de F�brica ********
                     // Aqu� se agrega el arbol definido por el usuario
                     // El arbol default se incluye de manera opcional
                     ZArbolTress.CreaArbolDefault( oArbolMgr );
                     // *********************************
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TArbolConfigura.CreaArbolUsuario( const sFileName: String );
var
   oArbolMgr: TArbolMgr;
begin
     oArbolMgr := TArbolMgr.Create( ArbolUsuario );
     with oArbolMgr do
     begin
          try
             Configuracion := True;
             with Arbol.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     ZArbolTress.CreaArbolUsuario( oArbolMgr, sFileName );
                     // Si no hubo selecci�n, selecciona el Primero y lo Abre
                     if ( Arbol.Selected = nil ) then
                         Arbol.Items[ 0 ].Selected := True;
                     if EsFolder( Arbol.Selected ) then
                        Arbol.Selected.Expand( False );
                     CopiaNodoDefault;
                     IncluirSistCB.Checked := oArbolMgr.DatosUsuario.IncluirSistema;
                     // Abre el folder del Usuario
                     // ArbolUsuario.Items[ 0 ].Expand( False );
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;


procedure TArbolConfigura.ArbolUsuarioDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   NodoFuente, NodoDestino, NewNodo: TTreeNode;
   oArbolFuente: TTreeView;
begin
     oArbolFuente := TTreeView( Source );
     NodoFuente := oArbolFuente.Selected;
     NodoDestino := ArbolUsuario.GetNodeAt( X, Y );
     if ( Source = ArbolUsuario ) then
     begin
          // Est� reorganizando nodos del arbol de Usuario
          if ( NodoDestino <> nil ) then
          begin
               ArbolUsuario.Items.BeginUpdate;
               if EsFolder( NodoDestino ) then
                  NodoFuente.MoveTo( NodoDestino, naAddChild )
               else
                   NodoFuente.MoveTo( NodoDestino, naInsert );
               ArbolUsuario.Items.EndUpdate;
               HuboCambios;
          end;
     end
     else
         if ( NodoFuente <> nil ) then
         begin
              // Si suelta en parte VACIA del Arbol,
              // supone que va bajo el folder Principal
              if ( NodoDestino = nil ) then
                 NodoDestino := ArbolUsuario.Items[ 0 ];
              with ArbolUsuario.Items do
              begin
                   // Si hacen un 'Drop' sobre un nodo que no es folder, busca
                   // el primer folder hacia arriba del arbol.
                   while ( NodoDestino <> nil ) and not EsFolder( NodoDestino ) do
                   begin
                        NodoDestino := NodoDestino.Parent;
                   end;
                   // Copia el nodo con todas sus propiedades e Hijos
                   BeginUpdate;
                   NewNodo := AddChild( NodoDestino, NodoFuente.Text );
                   if ( NewNodo <> nil ) then
                   begin
                        with NewNodo do
                        begin
                             ImageIndex := NodoFuente.ImageIndex;
                             SelectedIndex := NodoFuente.SelectedIndex;
                             StateIndex := NodoFuente.StateIndex;
                             Data := NodoFuente.Data;
                        end;
                        CopiaHijos( NodoFuente, NewNodo );
                        ArbolUsuario.Selected := NewNodo;
                   end;
                   EndUpdate;
                   HuboCambios;
              end;
         end;
end;

procedure TArbolConfigura.ArbolUsuarioDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
     inherited;
     Accept := ( Source = ArbolUsuario ) or ( Source = ArbolSistema );
end;

procedure TArbolConfigura.ArbolUsuarioEdited(Sender: TObject; Node: TTreeNode; var S: String);
begin
     inherited;
     HuboCambios;
end;

procedure TArbolConfigura.CopiaHijos( oNodoOrigen, oNodoDestino: TTreeNode );
var
   oNodo, ANode: TTreeNode;
   Items: TTreeNodes;
begin
     if ( oNodoOrigen.HasChildren ) then
     begin
          oNodo := oNodoOrigen.Item[ 0 ];
          ANode := oNodoDestino;
          Items := TTreeView( oNodoDestino.TreeView ).Items;
          repeat
                if ( oNodo <> nil ) then
                begin
                     if ( oNodo.Index = 0 ) then
                        ANode := Items.AddChild( ANode, oNodo.Text )
                     else
                         ANode := Items.Add( ANode, oNodo.Text );
                     with ANode do
                     begin
                          Text := oNodo.Text;
                          ImageIndex := oNodo.ImageIndex;
                          SelectedIndex := oNodo.SelectedIndex;
                          StateIndex := oNodo.StateIndex;
                          Data := oNodo.Data;
                     end;
                end;
                CopiaHijos( oNodo, ANode  );
                oNodo:= oNodo.GetNextChild( oNodo );
          until ( oNodo = nil );
      end;
end;


procedure TArbolConfigura.ArbolSistemaDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
     inherited;
     Accept := ( Source = ArbolUsuario );
end;

procedure TArbolConfigura.ArbolSistemaDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
     inherited;
     // Si regresa a ArbolSistema es que quiere borrar
     BorraRama( ArbolUsuario.Selected );
end;

procedure TArbolConfigura.BorraRama( oNodo: TTreeNode );
begin
     with ArbolUsuario.Items do
     begin
          if ( oNodo <> nil ) then
          begin
               BeginUpdate;
               if ( oNodo.AbsoluteIndex = 0 ) then
                  oNodo.DeleteChildren
               else
                   oNodo.Delete;
               EndUpdate;
               HuboCambios;
          end;
     end;
end;

procedure TArbolConfigura.HuboCambios;
begin
     // Para que s�lo haga el trabajo la primera vez
     if ( not FHuboCambios ) then
     begin
          FHuboCambios := True;
          CambiaBotones;
     end;
end;

procedure TArbolConfigura.CambiaBotones;
begin
     if FHuboCambios then
     begin
         OK.Enabled := True;
         with Cancelar do
         begin
               Kind := bkCancel;
               Cancel := False;
               Caption := '&Cancelar';
         end;
     end
     else
     begin
         OK.Enabled := False;
         with Cancelar do
         begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               SetFocus;
         end;
     end;
end;

procedure TArbolConfigura.OKClick(Sender: TObject);
var
   iForma: Integer;
   sVistaClasica : String;
function AplicaVistaUsuario: String;
begin
     with AplicaVistaClasica do
     begin
          if (Checked) then
             begin
                  Result := K_GLOBAL_SI;
             end
             else
             begin
                  Result := K_GLOBAL_NO;
             end;
     end;
end;
begin
          with dmSistema.cdsUsuarios do
          begin
               iForma := NumFormaDefault;
               if ( State = dsBrowse ) then
                  Edit;
               FieldByName( 'US_ARBOL' ).AsString := ZetaCommonTools.zBoolToStr( IncluirSistCB.Checked );
               FieldByName( 'US_FORMA' ).AsInteger := iForma;
               //Guardar la vista actual por si el usuario la cambia
               sVistaClasica := FieldByName( 'US_CLASICA' ).AsString;
               //Para guardar la vista seleccionada para el usuario
               FieldByName( 'US_CLASICA' ).AsString := AplicaVistaUsuario;

               //Si hubo un cambio de vista notificar al usuario
               if ( sVistaClasica <> FieldByName( 'US_CLASICA' ).AsString ) then
                  //ShowMessage ('El cambio de Estilo tomar� efecto la pr�xima vez que se firme a la aplicaci�n.');
                  ZInformation('Arbol de Usuario', 'El cambio de Estilo tomar� efecto la pr�xima vez que se firme a la aplicaci�n.', 0);
               dmSistema.ClaveEncriptada := FieldByName( 'US_PASSWRD' ).AsString;
               Post;
               Enviar;
          end;

          // PENDIENTE: Como hay 2 cds de Usuario, hay qu estarlos sincronizando
          if ( FUsuario = dmCliente.Usuario ) then
          begin
               with dmCliente.cdsUsuario do
               begin
                    Edit;
                    FieldByName( 'US_ARBOL' ).AsString := ZetaCommonTools.zBoolToStr( IncluirSistCB.Checked );
                    FieldByName( 'US_FORMA' ).AsInteger := iForma;
                    // Aplica Vista Usuario
                    //FieldByName( 'US_CLASICA' ).AsString := AplicaVistaUsuario;
                    Post;
               end;
          end;
          DescargaArbol;
          dmSistema.cdsArbol.Enviar;
          // Si el usuario que se modific� es el Activo, entonces se
          // refresca el arbol del Shell
          if ( FUsuario = dmCliente.Usuario ) then
          begin
               TressShell.CreaArbol( False )
          end;
end;

procedure TArbolConfigura.IncluirSistCBClick(Sender: TObject);
begin
     inherited;
     HuboCambios;
end;

procedure TArbolConfigura.ArbolUsuarioMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     if ( Button = mbRight ) then
        ArbolUsuario.Selected := ArbolUsuario.GetNodeAt( X, Y );
end;

procedure TArbolConfigura.CopiaNodoDefault;
var
   NodoFuente, NewNodo: TTreeNode;
begin
     with ArbolDefault.Items do
     begin
          BeginUpdate;
          Clear;
          NodoFuente := ArbolUsuario.Selected;
          if ( NodoFuente <> nil ) then
          begin
               NewNodo := AddChild( nil, NodoFuente.Text );
               with NewNodo do
               begin
                    ImageIndex := NodoFuente.ImageIndex;
                    // SelectedIndex := NodoFuente.SelectedIndex;
                    // Para que no cambie al estar seleccionado
                    SelectedIndex := ImageIndex;
                    StateIndex := NodoFuente.StateIndex;
                    Data := NodoFuente.Data;
               end;
               // CopiaHijos( NodoFuente, NewNodo  );
               // ArbolDefault.Selected:= NewNodo;
          end;
          EndUpdate;
     end;
end;

procedure TArbolConfigura.ArbolDefaultDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
     inherited;
     Accept := ( Source = ArbolUsuario );
end;

procedure TArbolConfigura.ArbolDefaultDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
     inherited;
     _NodoDefaultExecute( Sender );
end;

procedure TArbolConfigura._BorraTodoExecute(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.ZConfirm( 'Arbol de Usuario', '� Desea Borrar Todo el Arbol ?', 0, mbNo ) then
        BorraRama( ArbolUsuario.Items[ 0 ] );
end;

procedure TArbolConfigura._BorraRamaExecute(Sender: TObject);
begin
     inherited;
     if ( ActiveControl = ArbolUsuario ) then
        BorraRama( ArbolUsuario.Selected );
end;

procedure TArbolConfigura._RenombrarExecute(Sender: TObject);
begin
     inherited;
     if ( ActiveControl = ArbolUsuario ) and ( ArbolUsuario.Selected <> nil ) then
        ArbolUsuario.Selected.EditText;
end;

procedure TArbolConfigura._BuscaRamaUsuarioExecute(Sender: TObject);
begin
     inherited;
     ZArbolFind.BuscaNodoDialogo( ArbolUsuario, 'Arbol Usuario' );
end;

procedure TArbolConfigura._BuscaRamaSistemaExecute(Sender: TObject);
begin
     inherited;
     ZArbolFind.BuscaNodoDialogo( ArbolSistema, 'Arbol Sistema' );
end;

procedure TArbolConfigura._NodoDefaultExecute(Sender: TObject);
begin
     inherited;
     CopiaNodoDefault;
     HuboCambios;
end;

procedure TArbolConfigura._AgregaRamaExecute(Sender: TObject);
begin
     inherited;
     ArbolUsuarioDragDrop( ArbolUsuario, ArbolSistema, 0, 0 );
     if ( ArbolSistema.Selected <> nil ) then
        ArbolSistema.Selected := ArbolSistema.Selected.GetNextSibling;
end;

procedure TArbolConfigura.DescargaArbol;
var
   iOrden, iNivel: Integer;
   oNodo: TTreeNode;

  procedure AgregaRegistro( const iOrden, iNodo, iNivel: Integer; const sText: String );
  begin
       with dmSistema.cdsArbol do
       begin
            Append;
            FieldByName('US_CODIGO').AsInteger := FUsuario;
            FieldByName('AB_ORDEN').AsInteger := iOrden;
            FieldByName('AB_NODO').AsInteger := iNodo;
            FieldByName('AB_NIVEL').AsInteger := iNivel;
            FieldByName('AB_DESCRIP').AsString := sText;
            //Este campo se agrega para manejar el arbol de usuario por aplicacion (Exe)
            //FieldByName('AB_MODULO').AsInteger := dmSistema.FTipo_Exe_DevEx; //old
            FieldByName('AB_MODULO').AsInteger := dmCliente.GetAppNumber;
            Post;
       end;
  end;

begin     // DescargaArbol
     with dmSistema.cdsArbol do
     begin
          EmptyDataSet;
          Gauge.MaxValue := ( ArbolUsuario.Items.Count - 1 );
          Gauge.Progress := 0;
          Gauge.Visible := True;
          oNodo := ArbolUsuario.Items[ 0 ];
          // Se usa para Exportar/Importar estos 2 valores de USUARIO
          // Va en el primer registro
          oNodo.Data := Pointer( NumFormaDefault );
          if ( IncluirSistCB.Checked ) then
             iNivel := 1
          else
              iNivel := 0;
          with oNodo do
          begin
               AgregaRegistro( 0, NumFormaDefault, iNivel, oNodo.Text );
               oNodo := oNodo.GetNext;
          end;
          iOrden := 1;
          while ( oNodo <> nil ) do
          begin
               AgregaRegistro( iOrden, Integer( oNodo.Data ), oNodo.Level, oNodo.Text );
               Gauge.Progress := iOrden;
               Inc( iOrden );
               oNodo := oNodo.GetNext;
          end;
          Gauge.Visible:= False;
     end;
end;

procedure TArbolConfigura._ExportarExecute(Sender: TObject);
begin
     inherited;
     with ExportaDlg do
     begin
          FileName := 'Usuario' + IntToStr( FUsuario ) + '.arb';
          if Execute then
          begin
               with ArbolUsuario.Items.GetFirstNode do
               begin
               end;
               DescargaArbol;
               dmSistema.cdsArbol.SaveToFile( FileName );
          end;
     end;
end;

procedure TArbolConfigura._ImportarExecute(Sender: TObject);
begin
     inherited;
     with ImportaDlg do
     begin
          FileName := 'Usuario' + IntToStr( FUsuario ) + '.arb';
          if Execute then
          begin
               CreaArbolUsuario( FileName );
               HuboCambios;
          end;
     end;
end;

function TArbolConfigura.NumFormaDefault: Integer;
var
   oNodo: TTreeNode;
begin
     // Obtiene el # de Nodo de la Forma Default
     oNodo := ArbolDefault.Items.GetFirstNode;
     if ( oNodo <> nil ) then
        Result := Integer( oNodo.Data )
     else
         Result := 0;
end;

end.
