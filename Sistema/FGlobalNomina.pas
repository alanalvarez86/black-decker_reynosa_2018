unit FGlobalNomina;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ComCtrls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls,
     ZBaseGlobal,
     ZetaKeyLookup,
     ZetaKeyCombo,
     ZetaClientDataSet,
     ZetaNumero;

type
  TGlobalNomina = class(TBaseGlobal)
    FormatosGB: TGroupBox;
    PageControl: TPageControl;
    Configuracion: TTabSheet;
    Conceptos: TTabSheet;
    Label45: TLabel;
    RedondeoNeto: TZetaNumero;
    SaldoRedondeo: TCheckBox;
    PromediarCambios: TCheckBox;
    PromediarMinimos: TCheckBox;
    SepararCredito: TCheckBox;
    lblSubsidio: TLabel;
    SubsidioAcreditable: TZetaNumero;
    ISPTlbl: TLabel;
    ConceptoISPT: TZetaKeyLookup;
    ConceptoCreditoSalario: TZetaKeyLookup;
    AjsuteMonedaLBL: TLabel;
    ConceptoAjusteMoneda: TZetaKeyLookup;
    ConceptoDeduccionesIncobrables: TZetaKeyLookup;
    DeduccLbl: TLabel;
    Label31: TLabel;
    BancaElectronica: TZetaKeyCombo;
    TabSheet1: TTabSheet;
    RecibosPagados: TCheckBox;
    Recibos: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    MontoAdicional: TEdit;
    FormulaAdicional: TMemo;
    SBCO_FORMULA: TSpeedButton;
    Label4: TLabel;
    Aguinaldo: TTabSheet;
    FormulaFaltas: TMemo;
    Label5: TLabel;
    sbFaltas: TSpeedButton;
    Label6: TLabel;
    FormulaIncapacidades: TMemo;
    sbIncapacidades: TSpeedButton;
    rbPrimaDominical: TGroupBox;
    PrimaDominical: TCheckBox;
    PrimaDominicalTipoDia: TCheckBox;
    TabSheet2: TTabSheet;
    Label7: TLabel;
    ToleranciaPiramidacion: TZetaNumero;
    Label1: TLabel;
    ConceptoPiramidacion: TZetaKeyLookup;
    PiramidacionPrestamos: TZetaKeyCombo;
    Label9: TLabel;
    ConceptoTopePrestamos: TZetaKeyLookup;
    Label10: TLabel;
    FONACOT: TZetaKeyLookup;
    Label11: TLabel;
    Label12: TLabel;
    edtLimiteNominasOrdinarias: TZetaNumero;
    CreditoLbl: TLabel;
    GroupBox1: TGroupBox;
    SimulacionFiniquitos: TZetaNumero;
    Label8: TLabel;
    SimFiniquitosAprobacion: TCheckBox;
    Label14: TLabel;
    mAjusteISR: TMemo;
    bAjusteISR: TSpeedButton;
    lbConceptosTempEnRecibos: TLabel;
    cbConceptosTempEnRecibos: TCheckBox;
    Label13: TLabel;
    MetodoPago: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure SBCO_FORMULAClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sbFaltasClick(Sender: TObject);
    procedure sbIncapacidadesClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure bAjusteISRClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConceptoLookup( Control: TZetaKeyLookup; const iTag: Integer );
    procedure PrestamoLookup( Control: TZetaKeyLookup; const iTag: Integer );
    procedure ValidaFechaReformaFiscal; // Validaci�n por Reforma Fiscal 2008
  public
    { Public declarations }
  end;

var
  GlobalNomina: TGlobalNomina;

const
     K_LIMITE_INFERIOR_NOMINAS_ORDINARIAS = 100;
     K_LIMITE_SUPERIOR_NOMINAS_ORDINARIAS = 999;

implementation

uses DCatalogos,
     ZAccesosTress,
     DGlobal,ZGlobalTress,
     ZetaTipoEntidad,
     ZConstruyeFormula,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaCommonLists,
     dTablas,
     DCliente,
     ZToolsPE;


{$R *.DFM}

procedure TGlobalNomina.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos                      := ZAccesosTress.D_CAT_CONFI_NOMINA;
     RedondeoNeto.Tag                   := K_GLOBAL_REDONDEO_NETO;
     SaldoRedondeo.Tag                  := K_GLOBAL_LLEVAR_SALDO_REDONDEO;
     PromediarCambios.Tag               := K_GLOBAL_PROMEDIAR_SALARIOS;
     PromediarMinimos.Tag               := K_GLOBAL_PROM_SAL_MIN;
     SepararCredito.Tag                 := K_GLOBAL_SEPARAR_CRED_SAL;
     PrimaDominical.Tag                 := K_GLOBAL_PRIMA_DOMINICAL_COMPLETA;
     PrimaDominicalTipoDia.Tag          := K_GLOBAL_PRIMA_DOMINICAL_TIPO_DIA;
     SubsidioAcreditable.Tag            := K_GLOBAL_PORCEN_SUBSIDIO;
     ConceptoLookup( ConceptoISPT,                   K_GLOBAL_ISPT );
     ConceptoLookup( ConceptoCreditoSalario,         K_GLOBAL_CREDITO );
     ConceptoLookup( ConceptoAjusteMoneda,           K_GLOBAL_AJUSTE );
     ConceptoLookup( ConceptoDeduccionesIncobrables, K_GLOBAL_INCOBRABLES );
     ConceptoLookup( ConceptoPiramidacion,           K_GLOBAL_PIRAMIDA_CONCEPTO );
     ConceptoLookup( ConceptoTopePrestamos,          K_GLOBAL_TOPE_PRESTAMOS );
     RecibosPagados.Tag                 := K_DEFAULT_RECIBOS_PAGADOS;
     MontoAdicional.Tag                 := K_TITULO_MONTO_ADICIONAL_RECIBO;
     FormulaAdicional.Tag               := K_FORMULA_MONTO_ADICIONAL_RECIBO;
     FormulaFaltas.Tag                  := K_GLOBAL_DEF_AGUINALDO_FALTAS;
     FormulaIncapacidades.Tag           := K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES;
     ToleranciaPiramidacion.Tag         := K_GLOBAL_PIRAMIDA_TOLERANCIA;
     SimulacionFiniquitos.Tag           := K_GLOBAL_SIMULACION_FINIQUITOS;
     PiramidacionPrestamos.Tag          := K_GLOBAL_PIRAMIDA_PRESTAMOS;
     PrestamoLookup( FONACOT,          K_GLOBAL_FONACOT_PRESTAMO );
      // Validaci�n por Reforma Fiscal 2008
     ValidaFechaReformaFiscal;
     HelpContext                        := H65105_Nomina;
     Pagecontrol.ActivePage             := Configuracion;
     edtLimiteNominasOrdinarias.Tag     := K_GLOBAL_LIMITE_NOMINAS_ORDINARIAS;
     SimFiniquitosAprobacion.Tag        := K_GLOBAL_SIM_FINIQ_APROBACION;
     mAjusteISR.Tag                     := K_GLOBAL_EMPLEADO_CON_AJUSTE;
     cbConceptosTempEnRecibos.Tag       := K_GLOBAL_IMPRIMIR_CONCEPTOS_APOYO;
     MetodoPago.Tag                     := K_GLOBAL_NOMINA_METODO_DEFAULT;
end;

procedure TGlobalNomina.SBCO_FORMULAClick(Sender: TObject);
begin
     inherited;
     FormulaAdicional.Text := GetFormulaConst( enNomina, FormulaAdicional.Lines.Text, FormulaAdicional.SelStart, evBase  );
end;

procedure TGlobalNomina.FormShow(Sender: TObject);
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmTablas.cdsTPresta.Conectar;
     inherited;
end;

procedure TGlobalNomina.ConceptoLookup( Control: TZetaKeyLookup; const iTag: Integer );
begin
     with Control do
     begin
          LookupDataset := dmCatalogos.cdsConceptos;
          Tag := iTag;
     end;
end;

procedure TGlobalNomina.PrestamoLookup( Control: TZetaKeyLookup; const iTag: Integer );
begin
     with Control do
     begin
          LookupDataset := dmTablas.cdsTPresta;
          Tag := iTag;
     end;
end;


procedure TGlobalNomina.sbFaltasClick(Sender: TObject);
begin
     inherited;
     FormulaFaltas.Text:= GetFormulaConst( enEmpleado , FormulaFaltas.Text, FormulaFaltas.SelStart, evAguinaldo );
end;

procedure TGlobalNomina.sbIncapacidadesClick(Sender: TObject);
begin
     inherited;
     FormulaIncapacidades.Text:= GetFormulaConst( enEmpleado , FormulaIncapacidades.Text, FormulaIncapacidades.SelStart, evAguinaldo );
end;

procedure TGlobalNomina.OKClick(Sender: TObject);
begin
     if( ( SubsidioAcreditable.Valor > 1 ) or ( SubsidioAcreditable.Valor < 0 ) )then
     begin
          ZetaDialogo.ZError( Self.Caption, ' � La Proporci�n Del Subsidio '+
                                            'Acreditable Debe De Ser Un Valor Entre 0 - 1 !', 0 );
          PageControl.ActivePage := Configuracion;
          SubsidioAcreditable.SetFocus;
     end//if
     else
     begin
          //if( SimulacionFiniquitos.ValorEntero < K_LIMITE_NOM_NORMAL )then
          if( ( SimulacionFiniquitos.ValorEntero <> 0 ) and ( SimulacionFiniquitos.ValorEntero < edtLimiteNominasOrdinarias.ValorEntero ) )then {OP: 10/06/08}
          begin
               //ZetaDialogo.ZError( Self.Caption,Format( ' El n�mero de per�odo de simulaci�n de finiquitos debe ser mayor o igual a %d ',[ K_LIMITE_NOM_NORMAL ]),0);
               ZetaDialogo.ZError( Self.Caption,Format( ' El n�mero de per�odo de simulaci�n de finiquitos debe ser mayor o igual a %d ',[ edtLimiteNominasOrdinarias.ValorEntero ]),0);
               PageControl.ActivePage := Configuracion;
               SimulacionFiniquitos.SetFocus;
          end//if
          else
          begin
               if ( edtLimiteNominasOrdinarias.ValorEntero < K_LIMITE_INFERIOR_NOMINAS_ORDINARIAS ) or
                  ( edtLimiteNominasOrdinarias.ValorEntero > K_LIMITE_SUPERIOR_NOMINAS_ORDINARIAS ) then
               begin
                    ZetaDialogo.ZError( Self.Caption,Format( ' El l�mite de n�minas ordinarias debe estar en el rango de %0:d a %1:d',
                                                             [ K_LIMITE_INFERIOR_NOMINAS_ORDINARIAS , K_LIMITE_SUPERIOR_NOMINAS_ORDINARIAS ]),0);
                    PageControl.ActivePage := Configuracion;
                    edtLimiteNominasOrdinarias.SetFocus;
               end//if
               else
               begin
                    inherited;
               end;//else
          end;//else
     end;//else
end;


// Validaci�n por Reforma Fiscal 2008
procedure TGlobalNomina.ValidaFechaReformaFiscal;
begin
     if dmCliente.YearDefault >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 then
     begin
          SepararCredito.Caption := 'Separar Subsidio al Empleo:';
          SepararCredito.Left := 96;
          SepararCredito.Width := 149;
          ISPTlbl.Caption := 'I.S.R.:';
          CreditoLbl.Caption := 'Subsidio al Empleo:';
     end;
end;

procedure TGlobalNomina.bAjusteISRClick(Sender: TObject);
begin
     inherited;
     mAjusteISR.Lines.Text := GetFormulaConst( enEmpleado, mAjusteISR.Lines.Text, mAjusteISR.SelStart, evBase  );

end;

end.
