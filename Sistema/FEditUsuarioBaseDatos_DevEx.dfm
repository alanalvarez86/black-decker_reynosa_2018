inherited EditUsuarioBaseDatos_DevEx: TEditUsuarioBaseDatos_DevEx
  Left = 467
  Caption = 'Usuario de MSSQL'
  ClientHeight = 174
  ClientWidth = 288
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblUsuario: TLabel [0]
    Left = 60
    Top = 62
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usuario:'
  end
  object Label5: TLabel [1]
    Left = 69
    Top = 87
    Width = 30
    Height = 13
    Caption = 'Clave:'
  end
  object Label1: TLabel [2]
    Left = 35
    Top = 112
    Width = 64
    Height = 13
    Caption = 'Confirmaci'#243'n:'
  end
  inherited PanelBotones: TPanel
    Top = 138
    Width = 288
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 124
      Top = 5
      ModalResult = 0
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 203
      Top = 5
      Cancel = True
    end
  end
  object edUsuario: TEdit [4]
    Left = 102
    Top = 58
    Width = 123
    Height = 21
    Enabled = False
    TabOrder = 2
    OnChange = edUsuarioChange
  end
  object edPassword: TEdit [5]
    Left = 102
    Top = 83
    Width = 123
    Height = 21
    Enabled = False
    MaxLength = 30
    PasswordChar = '*'
    TabOrder = 3
  end
  object rbDefault: TcxRadioButton [6]
    Left = 16
    Top = 8
    Width = 265
    Height = 17
    Caption = 'Usuario y clave default (Configuraci'#243'n del Sistema)'
    Checked = True
    TabOrder = 0
    TabStop = True
    OnClick = rbDefaultClick
    Transparent = True
  end
  object rbOtro: TcxRadioButton [7]
    Left = 16
    Top = 32
    Width = 209
    Height = 17
    Caption = 'Utilizar otro usuario y clave de acceso:'
    TabOrder = 1
    OnClick = rbOtroClick
    Transparent = True
  end
  object edConfirmacion: TEdit [8]
    Left = 102
    Top = 108
    Width = 123
    Height = 21
    Enabled = False
    MaxLength = 30
    PasswordChar = '*'
    TabOrder = 4
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 2621688
  end
end
