inherited SistEditPoll_DevEx: TSistEditPoll_DevEx
  Left = 254
  Top = 146
  Caption = 'Poll Pendientes'
  ClientHeight = 208
  ClientWidth = 389
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 172
    Width = 389
    TabOrder = 2
    inherited OK_DevEx: TcxButton
      Left = 222
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 302
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 389
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 63
      inherited textoValorActivo2: TLabel
        Width = 57
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 7
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 389
    Height = 122
    Align = alClient
    TabOrder = 1
    object Label1: TLabel
      Left = 106
      Top = 8
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'gito Empresa:'
    end
    object Label2: TLabel
      Left = 77
      Top = 30
      Width = 105
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero de Empleado:'
    end
    object Label3: TLabel
      Left = 87
      Top = 52
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'Letra de Credencial:'
    end
    object Label4: TLabel
      Left = 88
      Top = 74
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha de Checada:'
    end
    object Label5: TLabel
      Left = 95
      Top = 96
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora de Checada:'
    end
    object Label6: TLabel
      Left = 108
      Top = 118
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reloj Checador:'
    end
    object PO_EMPRESA: TDBEdit
      Left = 192
      Top = 4
      Width = 58
      Height = 21
      DataField = 'PO_EMPRESA'
      DataSource = DataSource
      TabOrder = 0
    end
    object PO_NUMERO: TZetaDBNumero
      Left = 192
      Top = 26
      Width = 81
      Height = 21
      Mascara = mnEmpleado
      MaxLength = 9
      TabOrder = 1
      DataField = 'PO_NUMERO'
      DataSource = DataSource
    end
    object PO_LETRA: TDBEdit
      Left = 192
      Top = 48
      Width = 24
      Height = 21
      DataField = 'PO_LETRA'
      DataSource = DataSource
      TabOrder = 2
    end
    object PO_FECHA: TZetaDBFecha
      Left = 192
      Top = 70
      Width = 113
      Height = 22
      Cursor = crArrow
      TabOrder = 3
      Text = '15/dic/97'
      Valor = 35779.000000000000000000
      DataField = 'PO_FECHA'
      DataSource = DataSource
    end
    object PO_HORA: TZetaDBHora
      Left = 192
      Top = 92
      Width = 40
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 4
      Text = '    '
      Tope = 24
      Valor = '    '
      DataField = 'PO_HORA'
      DataSource = DataSource
    end
    object PO_LINX: TDBEdit
      Left = 192
      Top = 114
      Width = 110
      Height = 21
      DataField = 'PO_LINX'
      DataSource = DataSource
      TabOrder = 5
    end
  end
  inherited DataSource: TDataSource
    Left = 220
    Top = 73
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
