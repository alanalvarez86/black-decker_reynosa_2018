unit FSistGlobales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, ComCtrls, ToolWin, StdCtrls, Buttons, ImgList,
     ZBaseGlobal,
     ZBaseConsulta,
     ZetaDBTextBox;

type
  TSistGlobales = class(TBaseConsulta)
    PanelTitulos: TPanel;
    ToolBar: TToolBar;
    ImageList: TImageList;
    IdentificacionBtn: TToolButton;
    NivelesBtn: TToolButton;
    AdicionalesBtn: TToolButton;
    RecursosBtn: TToolButton;
    NominaBtn: TToolButton;
    AsistenciaBtn: TToolButton;
    ImssBtn: TToolButton;
    Capacitacionbtn: TToolButton;
    VariablesBtn: TToolButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    RfcLbl: TZetaDBTextBox;
    InfonavitLbl: TZetaDBTextBox;
    RazonLbl: TZetaDBTextBox;
    EmpleadosBtn: TToolButton;
    CafeteriaBtn: TToolButton;
    SupervisoresBtn: TToolButton;
    RepEmailBtn: TToolButton;
    SeguridadBtn: TToolButton;
    BitacoraBtn: TToolButton;
    AccesoTressBtn: TToolButton;
    BloqueoAsistenciaBtn: TToolButton;
    ListaDispositivosBtn: TToolButton;
    MisDatos: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure IdentificacionBtnClick(Sender: TObject);
    procedure NivelesBtnClick(Sender: TObject);
    procedure AdicionalesBtnClick(Sender: TObject);
    procedure RecursosBtnClick(Sender: TObject);
    procedure NominaBtnClick(Sender: TObject);
    procedure AsistenciaBtnClick(Sender: TObject);
    procedure ImssBtnClick(Sender: TObject);
    procedure CapacitacionbtnClick(Sender: TObject);
    procedure VariablesBtnClick(Sender: TObject);
    procedure EmpleadosBtnClick(Sender: TObject);
    procedure CafeteriaBtnClick(Sender: TObject);
    procedure SupervisoresBtnClick(Sender: TObject);
    procedure RepEmailBtnClick(Sender: TObject);
    procedure SeguridadBtnClick(Sender: TObject);
    procedure ToolBarDblClick(Sender: TObject);
    procedure BitacoraBtnClick(Sender: TObject);
    procedure AccesoTressBtnClick(Sender: TObject);
    procedure BloqueoAsistenciaBtnClick(Sender: TObject);
    procedure ListaDispositivosBtnClick(Sender: TObject);
    procedure MisDatosClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
    procedure AplicaGlobalDiccion;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  SistGlobales: TSistGlobales;

implementation

uses DGlobal,
     DCliente,
     DSistema,
     DDiccionario,
     ZGlobalTress,
     ZetaCommonClasses,
     FAutoClasses,
     FGlobalAsistencia,
     //FGlobalAdicionales,
     FGlobalCapacitacion,
     FGlobalImss,
     FGlobalIdentificacion,
     FGlobalNiveles,
     FGlobalNomina,
     FGlobalRecursos,
     FGlobalVariables,
     FGlobalEmpleados,
     FGlobalCafeteria,
     FGlobalSupervisores,
     FGlobalSeguridad,
     FGlobalReportesViaEmail,
     FGlobalBitacora,
     FGlobalBloqueo,
     FGlobalEnrolamientoUsuarios,
     FCamposCFG,
     FEmpAlta,
     FEditEmpAdicionales,
     FTressShell,
     ZetaDialogo,
     ZAccesosTress,
     ZAccesosMgr,
     FCamposMgr,
     FGlobalDispositivos,
     FGlobalMisDatos;
                      
{$R *.DFM}

{ ************* TSistGlobales *************** }

procedure TSistGlobales.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H60651_Globales_empresa;
{$ifdef DOS_CAPAS}
     MisDatos.Visible := False;
{$endif}
     MisDatos.Visible := not dmCliente.Sentinel_EsProfesional_MSSQL;
end;

procedure TSistGlobales.Connect;
begin
     with Global do
     begin
          RFCLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RFC_EMPRESA );
          InfonavitLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_INFONAVIT_EMPRESA );
          RazonLbl.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_RAZON_EMPRESA );
     end;
end;

procedure TSistGlobales.Refresh;
begin
end;

procedure TSistGlobales.AplicaGlobalDiccion;
begin
     TressShell.CreaArbol( False );
end;

function TSistGlobales.AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             if ZAccesosMgr.Revisa( IndexDerechos ) then
             begin
                  ShowModal;
                  Result := LastAction;
             end
             else
             begin
                  ZetaDialogo.ZInformation( self.Caption, 'No tiene derechos para consultar estos globales', 0 );
                  Result := K_EDICION_CANCELAR;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TSistGlobales.IdentificacionBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalIdentificacion ) = K_EDICION_MODIFICACION ) then
        Connect;
end;

procedure TSistGlobales.NivelesBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalNiveles ) = K_EDICION_MODIFICACION ) then
        AplicaGlobalDiccion;
end;

procedure TSistGlobales.AdicionalesBtnClick(Sender: TObject);
var
   oCamposAdic: TListaCampos;
   oClasificaciones: TListaClasificaciones;
   lDiferenteAdmin: Boolean;
begin
     inherited;
{
     if ( AbrirGlobal( TGlobalAdicionales ) = K_EDICION_MODIFICACION ) then
}
     lDiferenteAdmin:= ( dmCliente.GetGrupoActivo <> D_GRUPO_SIN_RESTRICCION );

     if ZAccesosMgr.Revisa( D_CAT_CONFI_ADICIONALES )then
     begin
          with dmSistema do
          begin
               { Aqui se valida el derecho de Modificar adicionales no el de consulta sobre clasificaciones }
               if ( lDiferenteAdmin ) then
                  ConectaGrupoAdAdmin ( dmCliente.Empresa );
               oCamposAdic := TListaCampos.Create;
               oClasificaciones := TListaClasificaciones.Create;
               CargaCamposAdicionales( oCamposAdic, oClasificaciones, FALSE );
               try
                  if FCamposCFG.ConfiguracionCampos( oCamposAdic, oClasificaciones ) then
                  begin
                       // Libera las formas de edici�n que puedan tener configurados los controles de adicionales
                       FreeAndNil( EmpAlta );
                       FreeAndNil( EditEmpAdicionales );
                       dmDiccionario.CambioGlobales;      // Para que refresque diccionario y LookupNames
                       AplicaGlobalDiccion;
                  end;
               finally
                      { Regresa el dataset para que valide derechos }
                      FreeAndNil( oClasificaciones );
                      FreeAndNil( oCamposAdic );
                      if ( lDiferenteAdmin ) then
                         cdsGruposAdic.Refrescar;
               end;
          end;
     end
     else
     begin
          ZetaDialogo.ZInformation( self.Caption, 'No tiene derechos para consultar estos globales', 0 );
     end;
end;
procedure TSistGlobales.RecursosBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okRecursos ) then
     begin
          AbrirGlobal( TGlobalRecursos );
     end;
end;

procedure TSistGlobales.NominaBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okNomina ) then
     begin
          AbrirGlobal( TGlobalNomina );
     end;
end;

procedure TSistGlobales.AsistenciaBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalAsistencia );
end;

procedure TSistGlobales.ImssBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okIMSS ) then
     begin
          AbrirGlobal( TGlobalImss );
     end;
end;

procedure TSistGlobales.CapacitacionbtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          AbrirGlobal( TGlobalCapacitacion );
     end;
end;

procedure TSistGlobales.VariablesBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalVariables );
end;

procedure TSistGlobales.EmpleadosBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalEmpleados );
end;

procedure TSistGlobales.CafeteriaBtnClick(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCafeteria ) then
     begin
          AbrirGlobal( TGlobalCafeteria );
     end;
end;

procedure TSistGlobales.SupervisoresBtnClick(Sender: TObject);
begin
     inherited;
     if ( Autorizacion.Plataforma = TPlataforma( 0 ) )then
     begin
          AbrirGlobal( TGlobalSupervisores );
     end
     else
     begin
          if ( dmCliente.ModuloAutorizado( okSupervisores ) )then
          begin
               AbrirGlobal( TGlobalSupervisores );
          end;
     end;
end;

procedure TSistGlobales.RepEmailBtnClick(Sender: TObject);
begin
     inherited;
     //pendiente
     if dmCliente.ModuloAutorizado( okReportesMail ) or dmCliente.ModuloAutorizado( okKiosko ) then
     begin
          AbrirGlobal( TGlobalReportesViaEmail );
     end;
end;

procedure TSistGlobales.SeguridadBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalSeguridad );
end;

procedure TSistGlobales.BitacoraBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalBitacora );
end;

procedure TSistGlobales.ToolBarDblClick(Sender: TObject);
begin
     inherited;
     IdentificacionBtnClick( Sender );
end;

procedure TSistGlobales.AccesoTressBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalEnrolamientoUsuarios );

end;

procedure TSistGlobales.BloqueoAsistenciaBtnClick(Sender: TObject);
begin
     inherited;
     if ( AbrirGlobal( TGlobalBloqueo ) = K_EDICION_MODIFICACION ) then
        TressShell.SetBloqueo;
end;

procedure TSistGlobales.ListaDispositivosBtnClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalDispositivos );
end;

procedure TSistGlobales.MisDatosClick(Sender: TObject);
begin
  	inherited;
     AbrirGlobal( TGlobalMisDatos );
end;

end.

