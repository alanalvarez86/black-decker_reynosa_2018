unit FSistEditGrupos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Mask, Db, ExtCtrls,
     Buttons, DBCtrls, ComCtrls, Menus, Grids, DBGrids,
     ZetaMessages,
     ZetaEdit,
     ZetaDBGrid,
     ZetaCommonClasses,
     ZetaNumero, ZetaSmartLists, ZBaseEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridCustomView, cxClasses, cxGridLevel, cxGrid, ZetaCXGrid,
  dxBarExtItems, dxBar, ImgList, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx;

type
  TSistEditGrupos_DevEx = class(TBaseEdicion_DevEx)
    Datos: TGroupBox;
    GR_CODIGOlbl: TLabel;
    GR_DESCRIPlbl: TLabel;
    GR_DESCRIP: TDBEdit;
    GR_CODIGO: TZetaDBNumero;
    Empresas: TGroupBox;
    dsEmpresas: TDataSource;
    Accesos: TcxButton;
    SUBORDINADOlbl: TLabel;
    GR_PADRE: TZetaDBKeyLookup_DevEx;
    GridEmpresas: TZetaCXGrid;
    GridEmpresasLevel1: TcxGridLevel;
    GridEmpresasDBTableView1: TcxGridDBTableView;
    procedure FormCreate(Sender: TObject);
    procedure AccesosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure GridEmpresasDBTableView1CellDblClick(Sender: TcxCustomGridTableView;ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
    procedure GridGetFilterValues(Sender: TcxCustomGridTableItem; AValueList: TcxDataFilterValueList);
  private
    { Private declarations }
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure ActualizaEmpresas;
    procedure ChecaGrupoActual;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  SistEditGrupos_DevEx: TSistEditGrupos_DevEx;

const
     K_ASIGNA_DERECHOS_OTROS_GRUPOS = ZetaCommonClasses.K_DERECHO_SIST_KARDEX;
     K_ASIGNA_DERECHOS_GRUPO_PROPIO = ZetaCommonClasses.K_DERECHO_BANCA;

implementation

uses DSistema,
     DCliente,
     DBaseSistema,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo,
     ZGridModeTools;

{$R *.DFM}

procedure TSistEditGrupos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_GRUPOS;

     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_EDIT_GPO_USR;
     {$else}
     HelpContext := H80812_Grupos_usuarios;
     {$endif}
     FirstControl := GR_CODIGO;
     with GR_PADRE do
     begin
          LookupDataset := dmSistema.cdsGruposLookup;
     end;
end;

procedure TSistEditGrupos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     GridEmpresasDbTableView1.ApplyBestFit();
end;

procedure TSistEditGrupos_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          cdsGruposLookUp.Conectar;
          Datasource.Dataset := cdsGrupos;
          ActualizaEmpresas;
     end;
end;

procedure TSistEditGrupos_DevEx.ActualizaEmpresas;
begin
     with dmSistema do
     begin
          with cdsEmpresasAccesos do
          begin
               Refrescar;
               Locate( 'CM_CODIGO', dmCliente.Compania, [ loCaseInsensitive ] );
          end;
          dsEmpresas.Dataset := cdsEmpresasAccesos;
          Accesos.Enabled := not dmSistema.cdsEmpresasAccesos.EOF;
     end;
end;

procedure TSistEditGrupos_DevEx.ChecaGrupoActual;
var
   sCodigo, sDescripcion: String;
begin
     with GR_PADRE do
     begin
          Enabled := ( dmSistema.cdsGrupos.FieldByName( 'GR_CODIGO' ).Asinteger <> dmCliente.GetGrupoActivo );
          if not Enabled then
          begin
               dmSistema.ObtieneDescripcionPadre( dmSistema.cdsGrupos.FieldByName( 'GR_PADRE' ).Asinteger, sCodigo, sDescripcion );
               SetLlaveDescripcion( sCodigo, sDescripcion );
          end;
          SUBORDINADOlbl.Enabled := Enabled;
     end;
end;

procedure TSistEditGrupos_DevEx.GridGetFilterValues(Sender: TcxCustomGridTableItem; AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
end;

procedure TSistEditGrupos_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     Accesos.Enabled := not Editing;
end;

procedure TSistEditGrupos_DevEx.WMExaminar(var Message: TMessage);
begin
     Accesos.Click;
end;

procedure TSistEditGrupos_DevEx.AccesosClick(Sender: TObject);
begin
     if not Inserting then
     begin
          // Llamar m�todo GrabaBaseAdicionales para registrar en BD los derechos que corresponden
		  // al �rbol de adicionales (Empleados/Datos) para el grupo y base de datos selecciondos.
	      // Se graban solamente los derechos faltantes en base de datos.
          // Correcci�n de bug #11680 ICU Medical: Comportamiento inadecuado al activar un derecho de acceso en el nodo de Adicionales.
          dmSistema.GrabaBaseAdicionales;

          if ( DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger <> D_GRUPO_SIN_RESTRICCION ) then
          begin
               if ( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, K_ASIGNA_DERECHOS_OTROS_GRUPOS )) and
                  ( dmCliente.GetGrupoActivo <> DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger ) then
               begin
                    DBaseSistema.CambiarAccesos;
               end
               else
               begin
                    if ( ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, K_ASIGNA_DERECHOS_GRUPO_PROPIO )) and
                       ( dmCliente.GetGrupoActivo = DataSource.Dataset.FieldByName( 'GR_CODIGO' ).AsInteger ) then
                    begin
                         DBaseSistema.CambiarAccesos;
                    end
                    else
                    begin
                         ZetaDialogo.zInformation( Self.Caption, '� No tiene Permiso para Modificar Accesos !', 0 );
                    end;
               end;
          end
          else
          begin
               ZetaDialogo.zInformation( Self.Caption, '� El Grupo 1 Tiene Todos Los Derechos: No Se Permite Cambiarle Accesos !', 0 );
          end;
      end;
end;

procedure TSistEditGrupos_DevEx.DBNavigatorClick(Sender: TObject; Button: TNavigateBtn);
begin
     inherited;
     ActualizaEmpresas;
end;

procedure TSistEditGrupos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          ChecaGrupoActual;
          ActualizaEmpresas;
     end;
end;

procedure TSistEditGrupos_DevEx.GridEmpresasDBTableView1CellDblClick( Sender: TcxCustomGridTableView;
          ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
     inherited;
     Accesos.click;
end;

end.
