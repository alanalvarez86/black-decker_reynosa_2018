unit FSistCalendarioReportes_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, {$ifdef TRESS_DELPHIXE5_UP}Actions,{$endif} cxButtons, ZetaKeyCombo, Mask,
  ZetaFecha, ZetaCommonLists, ZetaDialogo, ZFiltroSQLTools, cxContainer,
  cxTextEdit;

type
  TSistCalendarioReportes_DevEx = class(TBaseGridLectura_DevEx)
    PanelParam: TPanel;
    InscribirUsuarios: TcxButton;
    InscribirRoles: TcxButton;
    CA_NOMBRE: TcxGridDBColumn;
    CM_CODIGO: TcxGridDBColumn;
    CA_REPORT: TcxGridDBColumn;
    CA_ACTIVO: TcxGridDBColumn;
    CA_FREC: TcxGridDBColumn;
    CA_NX_FEC: TcxGridDBColumn;
    CA_US_CHG: TcxGridDBColumn;
    CA_ULT_FEC: TcxGridDBColumn;
    CA_FECHA: TcxGridDBColumn;
    CA_CAPTURA: TcxGridDBColumn;
    EjecutarEnvio: TcxButton;
    Label1: TLabel;
    EditBusca: TcxTextEdit;
    BuscaBtn: TcxButton;
    chkActivos: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure InscribirUsuariosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure InscribirRolesClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedRecordChanged( Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure EjecutarEnvioClick(Sender: TObject);
    procedure BuscaBtnClick(Sender: TObject);
    procedure chkActivosClick(Sender: TObject);
    procedure EditBuscaPropertiesChange(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
    procedure HabilitarControles;
    procedure Filtrar;
  protected
    { Protected declarations}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ReconnectGrid; override;
  public
    { Public declarations }
  end;

var
  SistCalendarioReportes_DevEx: TSistCalendarioReportes_DevEx;

implementation
uses
    DSistema,
    ZetaCommonClasses,
    ZAccesosMgr,
    ZAccesosTress,
    ZetaCommonTools,
    {$IFNDEF RDD}
    FWizSistCalendarioReportes_DevEx,
    {$endif}
    FSistSuscripcionUsuarios_DevEx,
    FSistSuscripcionRoles_DevEx,
    ZcxWizardBasico,
    ZBaseDlgModal_DevEx;

{$R *.dfm}

{ TLstDispositivos }

procedure TSistCalendarioReportes_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsUsuarios.Conectar;
          cdsEmpresasLookup.Conectar;
          cdsUsuariosLookUp.Conectar;
          cdsSistTareaCalendario.Conectar;
          DataSource.DataSet := cdsSistTareaCalendario;
     end;
     HabilitarControles;
end;

procedure TSistCalendarioReportes_DevEx.EditBuscaPropertiesChange(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistCalendarioReportes_DevEx.EjecutarEnvioClick(Sender: TObject);
var sError, sNombre : String;
begin
     inherited;
     sNombre := dmSistema.cdsSistTareaCalendario.FieldByName ('CA_NOMBRE').AsString;
     if ZConfirm(Self.Caption,'Se proceder� a ejecutar el env�o ' + sNombre + '.' + CR_LF + '�Seguro de ejecutar el env�o?',0,mbYes ) then
     begin
          sError := dmSistema.InsertarSolicitudEnvio(dmSistema.cdsSistTareaCalendario);
          if StrLleno(sError) then
             ZError(Self.Caption, sError, 0)
          else
              ZInformation(Self.Caption, 'Se ha ejecutado el env�o: ' + sNombre + '.' + CR_LF + 'Puede consultar el estatus en la pantalla de ' + CR_LF + 'solicitudes de env�os programados.', 0);
     end;
end;

procedure TSistCalendarioReportes_DevEx.ReconnectGrid;
begin
     inherited;
     Filtrar;
     ZetaDBGridDBTableView.DataController.Filter.Refresh;
end;


procedure TSistCalendarioReportes_DevEx.ZetaDBGridDBTableViewFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
const K_CERO = 0;
begin
  inherited;
     InscribirUsuarios.Enabled := (ZetaDBGridDBTableView.DataController.DataRowCount <> K_CERO)
                               and (dmSistema.cdsSistTareaCalendario.FieldByName ('CA_REPEMP').AsInteger = K_CERO);
     InscribirRoles.Enabled := InscribirUsuarios.Enabled;
     EjecutarEnvio.Enabled := ZetaDBGridDBTableView.DataController.DataRowCount <> K_CERO;
end;

procedure TSistCalendarioReportes_DevEx.Refresh;
begin
     dmSistema.cdsSistTareaCalendario.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
     HabilitarControles;
     Filtrar;
end;

procedure TSistCalendarioReportes_DevEx.Agregar;
begin
     dmSistema.TituloProceso := True;
     dmSistema.cdsSistTareaCalendario.Agregar;
     HabilitarControles;
end;

procedure TSistCalendarioReportes_DevEx.Modificar;
begin
     dmSistema.TituloProceso := True;
     dmSistema.cdsSistTareaCalendario.Modificar;
     HabilitarControles;
end;

procedure TSistCalendarioReportes_DevEx.Borrar;
begin
     dmSistema.cdsSistTareaCalendario.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
     HabilitarControles;
end;

procedure TSistCalendarioReportes_DevEx.BuscaBtnClick(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistCalendarioReportes_DevEx.chkActivosClick(Sender: TObject);
begin
     inherited;
     Filtrar;
end;

procedure TSistCalendarioReportes_DevEx.Filtrar;
const
     SIMBOLO = '%';
     K_FILTRO = 'CA_NOMBRE LIKE ''%s'' AND CA_ACTIVO = ''%s''';
     K_FILTRO_B = 'CA_NOMBRE LIKE ''%s''';
var
   sFiltro : string;
begin
     ZetaDBGridDBTableView.DataController.Filter.Clear;
     sFiltro := VACIO;
     sFiltro := FiltrarTextoSQL(EditBusca.Text);
     with DataSource.DataSet do
     begin
          Filtered := False;
          if chkActivos.Checked = TRUE then
             Filter := Format(K_FILTRO, [SIMBOLO+sFiltro+SIMBOLO, zBoolToStr(chkActivos.Checked) ])
          else
              Filter := Format(K_FILTRO_B, [SIMBOLO+sFiltro+SIMBOLO]);
          Filtered := True;
     end;
end;

procedure TSistCalendarioReportes_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := false;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := false;
end;

procedure TSistCalendarioReportes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     HelpContext := H5005406_Administrador_Envios_Programados;
end;

procedure TSistCalendarioReportes_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
     ConfigAgrupamiento;
     Filtrar;
end;

procedure TSistCalendarioReportes_DevEx.InscribirRolesClick(Sender: TObject);
begin
     inherited;
     dmSistema.EnvioProgramadoXEmpresa := false;
     if CheckDerecho( D_SIST_CALENDARIO_REPORTES, K_DERECHO_SIST_KARDEX ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( SistSuscripcionRoles_DevEx, TSistSuscripcionRoles_DevEx )
     else
         ZetaDialogo.zInformation( Caption, 'No tiene derechos para modificar suscripciones a roles.', 0 );
end;

procedure TSistCalendarioReportes_DevEx.InscribirUsuariosClick(Sender: TObject);
begin
     inherited;
     dmSistema.EnvioProgramadoXEmpresa := false;
     if CheckDerecho( D_SIST_CALENDARIO_REPORTES, K_DERECHO_BANCA ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( SistSuscripcionUsuarios_DevEx, TSistSuscripcionUsuarios_DevEx )
     else
         ZetaDialogo.zInformation( Caption, 'No tiene derechos para modificar suscripciones a usuarios.', 0 );
end;

procedure TSistCalendarioReportes_DevEx.HabilitarControles;
const K_CERO = 0;
var
   lHabilitar : Boolean;
begin
     if ZetaDBGridDBTableView.DataController.DataRowCount = 0 then
          lHabilitar := False
     else
          lHabilitar := True;

     EjecutarEnvio.Enabled := lHabilitar;
     lHabilitar := lHabilitar and (dmSistema.cdsSistTareaCalendario.FieldByName ('CA_REPEMP').AsInteger = K_CERO);
     InscribirUsuarios.Enabled := lHabilitar;
     InscribirRoles.Enabled := lHabilitar;
end;

end.


