unit FArbolConfigura_DevEx;
   
interface                                                                                                   

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Gauges,
     ComCtrls, ImgList, Menus, ActnList, ToolWin, DB,
     ZBaseDlgModal_DevEx,
     FTressShell, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
     dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBar,
     cxClasses, cxContainer, cxEdit, cxTreeView, cxButtons,
     dxSkinsDefaultPainters ;

type
  TArbolConfigura_DevEx = class(TZetaDlgModal_DevEx)
    SistemaGB: TGroupBox;
    ArbolSistema: TcxTreeView;
    Panel2: TPanel;
    Gauge: TGauge;
    UsuarioGB: TGroupBox;
    ArbolUsuario: TcxTreeView;
    Panel3: TPanel;
    IncluirSistCB: TCheckBox;
    PopUsuaio: TPopupMenu;
    LimpiarUsuario: TMenuItem;
    BorraRama1: TMenuItem;
    NodoDefaultItem: TMenuItem;
    N1: TMenuItem;
    BuscarRama1: TMenuItem;
    PopSistema: TPopupMenu;
    BuscarRama2: TMenuItem;
    N2: TMenuItem;
    Renombrar1: TMenuItem;
    ArbolDefault: TcxTreeView;
    Label1: TLabel;
    ActionListArbol: TActionList;
    _BorraTodo: TAction;
    _BorraRama: TAction;
    _Renombrar: TAction;
    _NodoDefault: TAction;
    _BuscaRamaUsuario: TAction;
    _BuscaRamaSistema: TAction;
    _AgregaRama: TAction;
    ExportaDlg: TSaveDialog;
    _Exportar: TAction;
    _Importar: TAction;
    ImportaDlg: TOpenDialog;
    DevEx_BarManager: TdxBarManager;
    BarArbolUsuario: TdxBar;
    dxBarButton_AgregaRama: TdxBarButton;
    dxBarButton_BuscaRamaUsuario: TdxBarButton;
    dxBarButton_BorraRama: TdxBarButton;
    dxBarButtonBorraTodo: TdxBarButton;
    dxBarButton_Renombrar: TdxBarButton;
    dxBarButton_NodoDefault: TdxBarButton;
    cxImageList16_BarArbol: TcxImageList;
    dxBarButton_Exportar: TdxBarButton;
    dxBarButton_Importar: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArbolUsuarioDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ArbolUsuarioDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolUsuarioEdited(Sender: TObject; Node: TTreeNode; var S: String);
    procedure ArbolUsuarioMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure ArbolDefaultDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolDefaultDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure ArbolSistemaDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ArbolSistemaDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure BorraRama( oNodo : TTreeNode );
    procedure IncluirSistCBClick(Sender: TObject);
    procedure _BorraTodoExecute(Sender: TObject);
    procedure _BorraRamaExecute(Sender: TObject);
    procedure _RenombrarExecute(Sender: TObject);
    procedure _BuscaRamaUsuarioExecute(Sender: TObject);
    procedure _BuscaRamaSistemaExecute(Sender: TObject);
    procedure _NodoDefaultExecute(Sender: TObject);
    procedure _AgregaRamaExecute(Sender: TObject);
    procedure _ExportarExecute(Sender: TObject);
    procedure _ImportarExecute(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure DevEx_BarManagerShowToolbarsPopup(Sender: TdxBarManager;
      PopupItemLinks: TdxBarItemLinks);
  private
    { Private declarations }
    FHuboCambios: Boolean;
    FUsuario: Integer;
    procedure CreaArbolSistema;
    procedure CreaArbolUsuario( const sFileName: String );
    procedure CopiaHijos(oNodoOrigen, oNodoDestino: TTreeNode);
    procedure HuboCambios;
    procedure CambiaBotones;
    procedure CopiaNodoDefault;
    procedure DescargaArbol;
    function  NumFormaDefault : Integer;
  public
    { Public declarations }
  end;

var
  ArbolConfigura_DevEx: TArbolConfigura_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     ZArbolFind_DevEx,
     ZArbolTools,
     ZArbolTress,
     ZetaCommonLists,
     DSistema,
     DCliente, ZBaseShell;

{$R *.DFM}

procedure TArbolConfigura_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80816_Usuarios_Preferencias;
end;

procedure TArbolConfigura_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          FUsuario := dmSistema.UsuarioPosicionado;
          {
          IncluirSistCB.Checked := ConvierteBool( cdsUsuarios.FieldByName( 'US_ARBOL' ).AsString );
          }
          Caption  := 'Configuraci�n de �rbol: ' + cdsUsuarios.FieldByName( 'US_NOMBRE' ).AsString;
     end;
     
     //Para que utilice los iconos de la vista Actual
     with TressShell do
     begin
          if  GetNavBarImageList <> nil then
          begin
               ArbolSistema.Images := GetNavBarImageList;
               ArbolUsuario.Images := GetNavBarImageList;
               ArbolDefault.Images := GetNavBarImageList;
          end;
     end;

     CreaArbolSistema;
     CreaArbolUsuario( '' );
     ArbolSistema.Selected := ArbolSistema.Items.GetFirstNode;
     FHuboCambios := False;
     CambiaBotones;
end;

procedure TArbolConfigura_DevEx.CreaArbolSistema;
var
   oArbolMgr: TArbolMgr;
begin
     oArbolMgr := TArbolMgr.Create( ArbolSistema );
     with oArbolMgr do
     begin
          try
             Configuracion := True;
             with Arbol_DevEx.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     // ****** Arbol de F�brica ********
                     // Aqu� se agrega el arbol definido por el usuario
                     // El arbol default se incluye de manera opcional
                     ZArbolTress.CreaArbolDefault( oArbolMgr );
                     // *********************************
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TArbolConfigura_DevEx.CreaArbolUsuario( const sFileName: String );
var
   oArbolMgr: TArbolMgr;
begin
     oArbolMgr := TArbolMgr.Create( ArbolUsuario );
     with oArbolMgr do
     begin
          try
             Configuracion := True;
             with Arbol_DevEx.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     ZArbolTress.CreaArbolUsuario( oArbolMgr, sFileName );
                     // Si no hubo selecci�n, selecciona el Primero y lo Abre
                     if ( Arbol_DevEx.Selected = nil ) then
                         Arbol_devEx.Items[ 0 ].Selected := True;
                     if EsFolder( Arbol_DevEx.Selected ) then
                        Arbol_DevEx.Selected.Expand( False );
                     CopiaNodoDefault;
                     IncluirSistCB.Checked := oArbolMgr.DatosUsuario.IncluirSistema;
                     // Abre el folder del Usuario
                     // ArbolUsuario.Items[ 0 ].Expand( False );
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;


procedure TArbolConfigura_DevEx.ArbolUsuarioDragDrop(Sender, Source: TObject; X, Y: Integer);
var
   NodoFuente, NodoDestino, NewNodo: TTreeNode;
   oArbolFuente: TcxTreeView;
begin
     oArbolFuente := TcxTreeView( tcxDragControlObject(Source).Control );
     NodoDestino := ArbolUsuario.GetNodeAt( X, Y );

     if ( oArbolFuente = ArbolUsuario ) then
     begin
     nodofuente := arbolusuario.selected;
          // Est� reorganizando nodos del arbol de Usuario
          if ( NodoDestino <> nil ) then
          begin
               ArbolUsuario.Items.BeginUpdate;
               if EsFolder( NodoDestino ) then
                  NodoFuente.MoveTo( NodoDestino, naAddChild )
               else
                   NodoFuente.MoveTo( NodoDestino, naInsert );
               ArbolUsuario.Items.EndUpdate;
               HuboCambios;
          end;
     end
     else
     begin
     NodoFuente := arbolSistema.selected;
         if ( NodoFuente <> nil ) then
         begin
              // Si suelta en parte VACIA del Arbol,
              // supone que va bajo el folder Principal
              if ( NodoDestino = nil ) then
                 NodoDestino := ArbolUsuario.Items[ 0 ];
              with ArbolUsuario.Items do
              begin
                   // Si hacen un 'Drop' sobre un nodo que no es folder, busca
                   // el primer folder hacia arriba del arbol.
                   while ( NodoDestino <> nil ) and not EsFolder( NodoDestino ) do
                   begin
                        NodoDestino := NodoDestino.Parent;
                   end;
                   // Copia el nodo con todas sus propiedades e Hijos
                   BeginUpdate;
                   NewNodo := AddChild( NodoDestino, NodoFuente.Text );
                   if ( NewNodo <> nil ) then
                   begin
                        with NewNodo do
                        begin
                             ImageIndex := NodoFuente.ImageIndex;
                             SelectedIndex := NodoFuente.SelectedIndex;
                             StateIndex := NodoFuente.StateIndex;
                             Data := NodoFuente.Data;
                        end;
                        CopiaHijos( NodoFuente, NewNodo );
                        ArbolUsuario.Selected := NewNodo;
                   end;
                   EndUpdate;
                   HuboCambios;
              end;
         end;
         end;
end;

procedure TArbolConfigura_DevEx.ArbolUsuarioDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
name:string;
begin
     inherited;
     if(source is tcxDragControlObject) then
               name := (source as tcxDragControlObject).Control.Name
     else
     name := 'No Fuente';
     Accept := ( name = ArbolUsuario.Name ) or ( name = ArbolSistema.Name );
end;

procedure TArbolConfigura_DevEx.ArbolUsuarioEdited(Sender: TObject; Node: TTreeNode; var S: String);
begin
     inherited;
     HuboCambios;
end;

procedure TArbolConfigura_DevEx.CopiaHijos( oNodoOrigen, oNodoDestino: TTreeNode );
var
   oNodo, ANode: TTreeNode;
   Items: TTreeNodes;
begin
     if ( oNodoOrigen.HasChildren ) then
     begin
          oNodo := oNodoOrigen.Item[ 0 ];
          ANode := oNodoDestino;
          Items := (oNodoDestino.TreeView as tTreeview).Items;
          repeat
                if ( oNodo <> nil ) then
                begin
                     if ( oNodo.Index = 0 ) then
                        ANode := Items.AddChild( ANode, oNodo.Text )
                     else
                         ANode := Items.Add( ANode, oNodo.Text );
                     with ANode do
                     begin
                          Text := oNodo.Text;
                          ImageIndex := oNodo.ImageIndex;
                          SelectedIndex := oNodo.SelectedIndex;
                          StateIndex := oNodo.StateIndex;
                          Data := oNodo.Data;
                     end;
                end;
                CopiaHijos( oNodo, ANode  );
                oNodo:= oNodo.GetNextChild( oNodo );
          until ( oNodo = nil );
      end;
end;


procedure TArbolConfigura_DevEx.ArbolSistemaDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
     inherited;
     if (Source is tcxDragControlObject) then
        Accept := ( (Source as tcxDragControlObject).Control = ArbolUsuario );
end;

procedure TArbolConfigura_DevEx.ArbolSistemaDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
     inherited;
     // Si regresa a ArbolSistema es que quiere borrar
     BorraRama( ArbolUsuario.Selected );
end;

procedure TArbolConfigura_DevEx.BorraRama( oNodo: TTreeNode );
begin
     with ArbolUsuario.Items do
     begin
          if ( oNodo <> nil ) then
          begin
               BeginUpdate;
               if ( oNodo.AbsoluteIndex = 0 ) then
                  oNodo.DeleteChildren
               else
                   oNodo.Delete;
               EndUpdate;
               HuboCambios;
          end;
     end;
end;

procedure TArbolConfigura_DevEx.HuboCambios;
begin
     // Para que s�lo haga el trabajo la primera vez
     if ( not FHuboCambios ) then
     begin
          FHuboCambios := True;
          CambiaBotones;
     end;
end;

procedure TArbolConfigura_DevEx.CambiaBotones;
begin
     if FHuboCambios then
     begin
         OK_DevEx.Enabled := True;
         with Cancelar_DevEx do
         begin
               //Kind := bkCancel;
               Cancel := False;
               //ModalResult := mrNone;
               Caption := '&Cancelar';
         end;
     end
     else
     begin
         OK_DevEx.Enabled := False;
         with Cancelar_DevEx do
         begin
               //Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               SetFocus;
         end;
     end;
end;

procedure TArbolConfigura_DevEx.IncluirSistCBClick(Sender: TObject);
begin
     inherited;
     HuboCambios;
end;

procedure TArbolConfigura_DevEx.ArbolUsuarioMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     if ( Button = mbRight ) then
        ArbolUsuario.Selected := ArbolUsuario.GetNodeAt( X, Y );
end;

procedure TArbolConfigura_DevEx.CopiaNodoDefault;
var
   NodoFuente, NewNodo: TTreeNode;
begin
     with ArbolDefault.Items do
     begin
          BeginUpdate;
          Clear;
          NodoFuente := ArbolUsuario.Selected;
          if ( NodoFuente <> nil ) then
          begin
               NewNodo := AddChild( nil, NodoFuente.Text );
               with NewNodo do
               begin
                    ImageIndex := NodoFuente.ImageIndex;
                    // SelectedIndex := NodoFuente.SelectedIndex;
                    // Para que no cambie al estar seleccionado
                    SelectedIndex := ImageIndex;
                    StateIndex := NodoFuente.StateIndex;
                    Data := NodoFuente.Data;
               end;
               // CopiaHijos( NodoFuente, NewNodo  );
               // ArbolDefault.Selected:= NewNodo;
          end;
          EndUpdate;
     end;
end;

procedure TArbolConfigura_DevEx.ArbolDefaultDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
     inherited;
     if (Source is tcxDragControlObject) then
        Accept := ( (Source as tcxDragControlObject).Control = ArbolUsuario );
end;

procedure TArbolConfigura_DevEx.ArbolDefaultDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
     inherited;
     _NodoDefaultExecute( Sender );
end;

procedure TArbolConfigura_DevEx._BorraTodoExecute(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.ZConfirm( '�rbol de Usuario', '� Desea Borrar Todo el �rbol ?', 0, mbNo ) then
        BorraRama( ArbolUsuario.Items[ 0 ] );
end;

procedure TArbolConfigura_DevEx._BorraRamaExecute(Sender: TObject);
begin
     inherited;
     if(activecontrol = ArbolUsuario.InnerTreeView) then
        BorraRama( ArbolUsuario.Selected );
end;

procedure TArbolConfigura_DevEx._RenombrarExecute(Sender: TObject);
begin
     inherited;
     if  (activecontrol = ArbolUsuario.InnerTreeView) and ( ArbolUsuario.Selected <> nil ) then
        ArbolUsuario.Selected.EditText;
end;

procedure TArbolConfigura_DevEx._BuscaRamaUsuarioExecute(Sender: TObject);
begin
     inherited;
     ZArbolFind_DevEx.BuscaNodoDialogo( ArbolUsuario, 'Arbol Usuario' );
end;

procedure TArbolConfigura_DevEx._BuscaRamaSistemaExecute(Sender: TObject);
begin
     inherited;
     ZArbolFind_DevEx.BuscaNodoDialogo( ArbolSistema, 'Arbol Sistema' );
end;

procedure TArbolConfigura_DevEx._NodoDefaultExecute(Sender: TObject);
begin
     inherited;
     CopiaNodoDefault;
     HuboCambios;
end;

procedure TArbolConfigura_DevEx._AgregaRamaExecute(Sender: TObject);
begin
     inherited;
      if(activecontrol = ArbolSistema.InnerTreeView) then
     ArbolUsuarioDragDrop( ArbolUsuario, ArbolSistema, 0, 0 );
     if ( ArbolSistema.Selected <> nil ) then
        ArbolSistema.Selected := ArbolSistema.Selected.GetNextSibling;
end;

procedure TArbolConfigura_DevEx.DescargaArbol;
var
   iOrden, iNivel: Integer;
   oNodo: TTreeNode;

  procedure AgregaRegistro( const iOrden, iNodo, iNivel: Integer; const sText: String );
  begin
       with dmSistema.cdsArbol do
       begin
            Append;
            FieldByName('US_CODIGO').AsInteger := FUsuario;
            FieldByName('AB_ORDEN').AsInteger := iOrden;
            FieldByName('AB_NODO').AsInteger := iNodo;
            FieldByName('AB_NIVEL').AsInteger := iNivel;
            FieldByName('AB_DESCRIP').AsString := sText;
            //Este campo se agrega para manejar el arbol de usuario por aplicacion (Exe)
            //FieldByName('AB_MODULO').AsInteger := dmSistema.FTipo_Exe_DevEx;  //old
            FieldByName('AB_MODULO').AsInteger := dmCliente.GetAppNumber;
            Post;
       end;
  end;

begin     // DescargaArbol
     with dmSistema.cdsArbol do
     begin
          EmptyDataSet;
          Gauge.MaxValue := ( ArbolUsuario.Items.Count - 1 );
          Gauge.Progress := 0;
          Gauge.Visible := True;
          oNodo := ArbolUsuario.Items[ 0 ];
          // Se usa para Exportar/Importar estos 2 valores de USUARIO
          // Va en el primer registro
          oNodo.Data := Pointer( NumFormaDefault );
          if ( IncluirSistCB.Checked ) then
             iNivel := 1
          else
              iNivel := 0;
          with oNodo do
          begin
               AgregaRegistro( 0, NumFormaDefault, iNivel, oNodo.Text );
               oNodo := oNodo.GetNext;
          end;
          iOrden := 1;
          while ( oNodo <> nil ) do
          begin
               AgregaRegistro( iOrden, Integer( oNodo.Data ), oNodo.Level, oNodo.Text );
               Gauge.Progress := iOrden;
               Inc( iOrden );
               oNodo := oNodo.GetNext;
          end;
          Gauge.Visible:= False;
     end;
end;

procedure TArbolConfigura_DevEx._ExportarExecute(Sender: TObject);
begin
     inherited;
     with ExportaDlg do
     begin
          FileName := 'Usuario' + IntToStr( FUsuario ) + '.arb';
          if Execute then
          begin
               with ArbolUsuario.Items.GetFirstNode do
               begin
               end;
               DescargaArbol;
               dmSistema.cdsArbol.SaveToFile( FileName );
          end;
     end;
end;

procedure TArbolConfigura_DevEx._ImportarExecute(Sender: TObject);
begin
     inherited;
     with ImportaDlg do
     begin
          FileName := 'Usuario' + IntToStr( FUsuario ) + '.arb';
          if Execute then
          begin
               CreaArbolUsuario( FileName );
               HuboCambios;
          end;
     end;
end;

function TArbolConfigura_DevEx.NumFormaDefault: Integer;
var
   oNodo: TTreeNode;
begin
     // Obtiene el # de Nodo de la Forma Default
     oNodo := ArbolDefault.Items.GetFirstNode;
     if ( oNodo <> nil ) then
        Result := Integer( oNodo.Data )
     else
         Result := 0;
end;

procedure TArbolConfigura_DevEx.OK_DevExClick(Sender: TObject);
var
   iForma: Integer;
begin
          with dmSistema.cdsUsuarios do
          begin
               iForma := NumFormaDefault;
               if ( State = dsBrowse ) then
                  Edit;
               FieldByName( 'US_ARBOL' ).AsString := ZetaCommonTools.zBoolToStr( IncluirSistCB.Checked );
               FieldByName( 'US_FORMA' ).AsInteger := iForma;
               dmSistema.ClaveEncriptada := FieldByName( 'US_PASSWRD' ).AsString;
               Post;
               Enviar;
          end;

          // PENDIENTE: Como hay 2 cds de Usuario, hay qu estarlos sincronizando
          if ( FUsuario = dmCliente.Usuario ) then
          begin
               with dmCliente.cdsUsuario do
               begin
                    Edit;
                    FieldByName( 'US_ARBOL' ).AsString := ZetaCommonTools.zBoolToStr( IncluirSistCB.Checked );
                    FieldByName( 'US_FORMA' ).AsInteger := iForma;
                    Post;
               end;
          end;
          DescargaArbol;
          dmSistema.cdsArbol.Enviar;
          // Si el usuario que se modific� es el Activo, entonces se
          // refresca el arbol del Shell
          if ( FUsuario = dmCliente.Usuario ) then
          begin
               //Limpiar navbar antes de volverla a crear
               {$ifndef TIMBRADO}
                     TressShell.LimpiaGruposNavBar;
                     TressShell.CreaNavBar;
               {$endif}
          end;
          {$ifndef TIMBRADO}
          TressShell.LLenaOpcionesGrupos;
          {$endif}
end;

procedure TArbolConfigura_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  //DevEx: Agregado porque la propiedad Cancel del boton de DevExpress no funciona igual que la de los TBitBtn
     Close;
end;

procedure TArbolConfigura_DevEx.DevEx_BarManagerShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;

end.
