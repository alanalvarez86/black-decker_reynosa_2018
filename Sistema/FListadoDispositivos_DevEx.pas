unit FListadoDispositivos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,             
  Dialogs, ZBaseDlgModal_DevEx, StdCtrls, CheckLst, Buttons, ZetaSmartLists,
  ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  cxContainer, cxEdit, cxCheckListBox;

type
  TListadoDispositivos_DevEx = class(TZetaDlgModal_DevEx)
    Prender_DevEx: TcxButton;
    Apagar_DevEx: TcxButton;
    Terminales_DevEx: TcxCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Prender_DevExClick(Sender: TObject);
    procedure Apagar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
  public
    { Public declarations }
  end;

var
  ListadoDispositivos_DevEx: TListadoDispositivos_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema;

{$R *.dfm}

procedure TListadoDispositivos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     ActiveControl := Terminales_DevEx;
end;

procedure TListadoDispositivos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TListadoDispositivos_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TListadoDispositivos_DevEx.Cargar;
{$ifndef DOS_CAPAS}
var
   i: integer;
{$endif}
begin
{$ifndef DOS_CAPAS}
          //DevEx(by am): Cargar opciones seleccionadas
          with dmSistema do
          begin
               CargaListaTerminales(FLista);
               with Terminales_DevEx do
               begin
                    Items.BeginUpdate;
                    try
                       Clear;
                       //DevEx(by am): Pasar la lista al CheckBoxList
                        with FLista do
                        begin
                             for i := 0 to ( Count - 1 ) do
                                  Items.Add.Text := FLista[i];
                        end;
                        //DevEx (by am): Cargar opciones selecionadas
                        for i := 0 to ( Items.Count - 1 ) do
                         Items[i].Checked := DentroListaTerminales( cdsListaGrupos.FieldByName( 'GP_CODIGO' ).AsString, Items[ i ].Text );
                    finally
                           Items.EndUpdate;
                    end;
               end;
          end;
{$endif}
end;

procedure TListadoDispositivos_DevEx.PrendeApaga(const lState: Boolean);
var
   i: Integer;
begin
     with Terminales_DevEx do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Items[ i ].Checked := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

procedure TListadoDispositivos_DevEx.OK_DevExClick(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   i: integer;
   sTerminales,sSinTerminales: string;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     // Guardar
     with dmSistema do
     begin
          for i := 0 to ( Terminales_DevEx.Items.Count - 1 ) do
          begin
               if Terminales_DevEx.Items[ i ].Checked then
                  sTerminales := ConcatString( sTerminales, Copy( Terminales_DevEx.Items[ i ].Text, 0, Pos( '=', Terminales_DevEx.Items[ i ].Text ) - 1 ), ',' )
               else
                   sSinTerminales := ConcatString( sSinTerminales, Copy( Terminales_DevEx.Items[ i ].Text, 0, Pos( '=', Terminales_DevEx.Items[ i ].Text ) - 1 ), ',' )
          end;

           GrabaListaTerminales( cdsListaGrupos.FieldByName( 'GP_CODIGO' ).AsString, sTerminales, sSinTerminales );

          cdsTermPorGrupo.Refrescar;
     end;
     {$endif}
end;

procedure TListadoDispositivos_DevEx.Prender_DevExClick(Sender: TObject);
begin
  inherited;
     PrendeApaga( True );
end;

procedure TListadoDispositivos_DevEx.Apagar_DevExClick(Sender: TObject);
begin
  inherited;
  PrendeApaga( False );
end;

end.
