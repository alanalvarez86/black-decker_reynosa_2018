unit FSistUsuariosSeguridad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseUsuarios, Db, StdCtrls, Buttons, Grids, DBGrids, ZetaDBGrid,
  ExtCtrls, ZetaKeyCombo, FSistBaseUsuariosSeguridad, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxButtons,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TSistUsuariosSeguridad = class(TSistBaseUsuariosSeguridad)
    Suspender: TcxButton;
    Activar: TcxButton;
    Supervisa: TcxButton;
    Areas: TcxButton;
    Bloquear: TcxButton;
    SistemaBloqueadoPanel: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    CCosto: TcxButton;
    CB_Codigo: TcxGridDBColumn;
    US_BLOQUEA: TcxGridDBColumn;
    US_DOMAIN: TcxGridDBColumn;
    procedure SuspenderClick(Sender: TObject);
    procedure ActivarClick(Sender: TObject);
    procedure SupervisaClick(Sender: TObject);
    procedure AreasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BloquearClick(Sender: TObject);
    procedure CCostoClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
    procedure EditPanelSistema;
  protected
    procedure Refresh; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;

  public
    { Public declarations }
  end;

var
  SistUsuariosSeguridad: TSistUsuariosSeguridad;

implementation

{$R *.DFM}

uses DSistema,
     DGlobal,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZBaseDlgModal_DevEx,
     ZetaDialogo,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     FSistEditUsuarioSupervisor_DevEx,
     FSistEditUsuarioArea_DevEx,
     FSistEditUsuarioCCosto_DevEx,
     DCliente,
     ZToolsPE;

procedure TSistUsuariosSeguridad.FormShow(Sender: TObject);
var
   sAreas: string;
begin
     inherited;
     sAreas := Global.GetGlobalString( K_GLOBAL_LABOR_AREAS );
     with Areas do
     begin
          //Caption := Format( '&%s', [ sAreas ] );
          Hint    := Format( 'Configurar %s A Su Cargo', [ sAreas ] );
          Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_AREAS, K_DERECHO_CONSULTA );
     end;


     with CCosto do
     begin
          Hint    := Format( 'Configurar %s A Su Cargo', [ Global.NombreCosteo ] );
          Enabled := dmCliente.ModuloAutorizadoCosteo;
     end;

     Activar.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_PRENDE_USUARIOS, K_DERECHO_CONSULTA );
     //Activar.Visible:=False; Se implementar� hasta la pr�xima versi�n AP: 5/Jun/2007
     Supervisa.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_SUPER, K_DERECHO_CONSULTA );
     Suspender.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_APAGA_USUARIOS, K_DERECHO_CONSULTA );
     //Suspender.Visible:=False; Se implementar� hasta la pr�xima versi�n AP: 5/Jun/2007
     Bloquear.Visible:= ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );
     EditPanelSistema;
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := true;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := true;
  //Para que nunca muestre el filterbox inferior
  Supervisa.Enabled := dmCliente.EmpresaAbierta;
     Areas.Enabled := dmCliente.EmpresaAbierta;
     CCosto.Enabled := dmCliente.EmpresaAbierta;
end;

procedure TSistUsuariosSeguridad.SuspenderClick(Sender: TObject);
begin
     dmSistema.SuspendeUsuarios;
end;

procedure TSistUsuariosSeguridad.ActivarClick(Sender: TObject);
begin
     dmSistema.ActivaUsuarios;
end;

procedure TSistUsuariosSeguridad.SupervisaClick(Sender: TObject);
begin
     //ZBaseDlgModal.ShowDlgModal( FSistEditUserSuper, TFSistEditUserSuper );
     if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
     begin
          ZBaseDlgModal_DevEx.ShowDlgModal( FSistEditUserSuper_DevEx, TFSistEditUserSuper_DevEx );
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
     end;
end;

procedure TSistUsuariosSeguridad.AreasClick(Sender: TObject);
begin
     //ZBaseDlgModal.ShowDlgModal( FSistEditUserArea, TFSistEditUserArea );
     if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
     begin
          ZBaseDlgModal_DevEx.ShowDlgModal( SistEditUsuarioCCosto_DevEx, TSistEditUsuarioCCosto_DevEx );
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
     end;
end;

procedure TSistUsuariosSeguridad.BloquearClick(Sender: TObject);
var
   lBlockSistema: Boolean;
begin
     lBlockSistema:= not dmSistema.BloqueoSistema ;
     dmCliente.GrabaBloqueoSistema(lBlockSistema);
     Refresh;
end;

procedure TSistUsuariosSeguridad.Refresh;
begin
     inherited;
     EditPanelSistema;
end;

procedure TSistUsuariosSeguridad.EditPanelSistema;
begin
     with SistemaBloqueadoPanel do
     begin
          if( dmSistema.BloqueoSistema ) then
          begin
               Caption := 'Sistema Bloqueado';
               Font.Color := clRed;
          end
          else
          begin
               Caption := 'Sistema Habilitado';
               Font.Color := clBlack;
          end;
     end;
end;

function TSistUsuariosSeguridad.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
          if (not Result) then
          begin
               sMensaje := 'No Tiene Permiso Para Modificar Registros';
          end;
     end;
end;

procedure TSistUsuariosSeguridad.CCostoClick(Sender: TObject);
begin
     if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
     begin
          ZBaseDlgModal_DevEx.ShowDlgModal( SistEditUsuarioCCosto_DevEx, TSistEditUsuarioCCosto_DevEx );
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
     end;
end;

procedure TSistUsuariosSeguridad.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
 inherited;
               with ACanvas do
               begin
                    if zStrToBool( AViewInfo.GridRecord.DisplayTexts[ZetaDbGridDBTableView.GetColumnByFieldName('US_DENTRO').index] ) then
                       Font.Color := clRed
               end;
         // DefaultDrawDataCell( Rect, Column.Field, State );
 end;

end.
