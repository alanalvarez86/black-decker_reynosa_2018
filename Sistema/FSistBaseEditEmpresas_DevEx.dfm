inherited SistBaseEditEmpresas_DevEx: TSistBaseEditEmpresas_DevEx
  Left = 218
  Top = 341
  Caption = 'Empresas'
  ClientHeight = 303
  ClientWidth = 509
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 267
    Width = 509
    TabOrder = 5
    DesignSize = (
      509
      36)
    inherited OK_DevEx: TcxButton
      Left = 345
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 424
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 509
    inherited ValorActivo2: TPanel
      Width = 183
      inherited textoValorActivo2: TLabel
        Width = 177
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 0
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 50
    Width = 509
    Height = 217
    Align = alClient
    TabOrder = 7
    object Label1: TLabel
      Left = 62
      Top = 19
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = '&C'#243'digo:'
      Enabled = False
      FocusControl = CM_CODIGO
    end
    object Label2: TLabel
      Left = 58
      Top = 44
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = '&Nombre:'
      FocusControl = CM_NOMBRE
    end
    object CM_NOMBRE: TDBEdit
      Left = 101
      Top = 40
      Width = 388
      Height = 21
      DataField = 'CM_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
    object CM_CODIGO: TZetaDBEdit
      Left = 101
      Top = 15
      Width = 143
      Height = 21
      CharCase = ecUpperCase
      Enabled = False
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'CM_CODIGO'
      DataSource = DataSource
    end
    object GroupBox1: TcxGroupBox
      Left = 1
      Top = 69
      Align = alBottom
      Caption = ' Base de Datos '
      Enabled = False
      TabOrder = 2
      Height = 147
      Width = 507
      object Label8: TLabel
        Left = 25
        Top = 42
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = '&Base de Datos:'
        Enabled = False
        FocusControl = CM_DATOS
        Transparent = True
      end
      object Label9: TLabel
        Left = 4
        Top = 65
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre de Usuar&io:'
        Enabled = False
        FocusControl = CM_USRNAME
        Transparent = True
      end
      object Label10: TLabel
        Left = 14
        Top = 88
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cla&ve de Acceso:'
        Enabled = False
        FocusControl = CM_PASSWRD
        Transparent = True
      end
      object Label5: TLabel
        Left = 62
        Top = 111
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Con&trol:'
        Enabled = False
        FocusControl = CM_CONTROL
        Transparent = True
      end
      object Label7: TLabel
        Left = 73
        Top = 19
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'A&lias:'
        Enabled = False
        FocusControl = CM_ALIAS
        Transparent = True
      end
      object CM_USRNAME: TDBEdit
        Left = 100
        Top = 61
        Width = 145
        Height = 21
        DataField = 'CM_USRNAME'
        DataSource = DataSource
        Enabled = False
        TabOrder = 2
      end
      object CM_PASSWRD: TDBEdit
        Left = 100
        Top = 84
        Width = 145
        Height = 21
        DataField = 'CM_PASSWRD'
        DataSource = DataSource
        Enabled = False
        MaxLength = 14
        PasswordChar = '*'
        TabOrder = 3
      end
      object CM_CONTROL: TDBEdit
        Left = 100
        Top = 107
        Width = 145
        Height = 21
        DataField = 'CM_CONTROL'
        DataSource = DataSource
        Enabled = False
        TabOrder = 4
      end
      object CM_DATOS: TDBEdit
        Left = 100
        Top = 38
        Width = 382
        Height = 21
        DataField = 'CM_DATOS'
        DataSource = DataSource
        Enabled = False
        TabOrder = 1
      end
      object CM_ALIAS: TDBEdit
        Left = 100
        Top = 15
        Width = 145
        Height = 21
        DataField = 'CM_ALIAS'
        DataSource = DataSource
        Enabled = False
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 1048928
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 432
    Top = 8
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 524688
  end
end
