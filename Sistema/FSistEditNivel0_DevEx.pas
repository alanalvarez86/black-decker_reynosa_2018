unit FSistEditNivel0_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Mask, DBCtrls, StdCtrls, ZetaEdit, Db, ExtCtrls, Buttons,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditNivel0_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    DBCodigoLBL: TLabel;
    TB_CODIGO: TZetaDBEdit;
    DBDescripcionLBL: TLabel;
    TB_ELEMENT: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditNivel0_DevEx: TEditNivel0_DevEx;

implementation

uses dSistema, ZAccesosTress, ZetaCommonClasses;

{$R *.DFM}

{ TEditNivel0 }

procedure TEditNivel0_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_NIVEL0;
     HelpContext := H80818_Confidencialidad;
     FirstControl := TB_CODIGO;
end;

procedure TEditNivel0_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsNivel0.Conectar;
          Datasource.Dataset := cdsNivel0;
     end;
end;

end.
