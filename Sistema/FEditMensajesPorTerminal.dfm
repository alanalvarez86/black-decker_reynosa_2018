inherited EditMensajesPorTerminal: TEditMensajesPorTerminal
  Caption = 'Mensajes por terminal'
  ClientHeight = 194
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 58
    Top = 43
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terminal:'
  end
  object Label2: TLabel [1]
    Left = 58
    Top = 67
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mensaje:'
  end
  object Label4: TLabel [2]
    Left = 12
    Top = 111
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Siguiente mensaje:'
  end
  object Label3: TLabel [3]
    Left = 65
    Top = 90
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Espera:'
  end
  object Label6: TLabel [4]
    Left = 171
    Top = 91
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = '(minutos)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object TM_NEXT: TZetaDBTextBox [5]
    Left = 105
    Top = 110
    Width = 233
    Height = 17
    AutoSize = False
    Caption = 'TM_NEXT'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'TM_NEXT'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 158
    Width = 419
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 251
    end
    inherited Cancelar: TBitBtn
      Left = 336
    end
  end
  inherited PanelSuperior: TPanel
    Width = 419
    TabOrder = 5
  end
  inherited PanelIdentifica: TPanel
    Width = 419
    TabOrder = 6
    inherited ValorActivo2: TPanel
      Width = 93
    end
  end
  object TE_CODIGO: TZetaDBKeyLookup [9]
    Left = 105
    Top = 40
    Width = 300
    Height = 21
    ReadOnly = True
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'TE_CODIGO'
    DataSource = DataSource
  end
  object DM_CODIGO: TZetaDBKeyLookup [10]
    Left = 105
    Top = 64
    Width = 300
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'DM_CODIGO'
    DataSource = DataSource
  end
  object TM_SLEEP: TZetaDBNumero [11]
    Left = 105
    Top = 87
    Width = 65
    Height = 21
    Mascara = mnDias
    TabOrder = 2
    Text = '0'
    DataField = 'TM_SLEEP'
    DataSource = DataSource
  end
  object btnAplicarMensaje: TButton [12]
    Left = 104
    Top = 129
    Width = 113
    Height = 25
    Caption = 'Aplicar mensaje'
    TabOrder = 3
    OnClick = btnAplicarMensajeClick
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
end
