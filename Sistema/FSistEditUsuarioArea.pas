unit FSistEditUsuarioArea;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal;

type
  TFSistEditUserArea = class(TZetaDlgModal)
    Areas: TCheckListBox;
    Prender: TBitBtn;
    Apagar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure AreasClickCheck(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  FSistEditUserArea: TFSistEditUserArea;

implementation

uses ZetaCommonClasses,
     ZGlobalTress,
     DGlobal,
     DSistema;

{$R *.DFM}

procedure TFSistEditUserArea.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
     HelpContext := H80839_Asignando_areas_de_Labor_a_los_usuarios;
end;

procedure TFSistEditUserArea.FormShow(Sender: TObject);
begin
     inherited;
     OK.Enabled := False;
     Cargar;
     ActiveControl := Areas;
     Caption :=  Format( 'Asignar %s A %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_AREAS ), dmSistema.cdsUsuarios.FieldByName( 'US_NOMBRE' ).AsString ] );
end;

procedure TFSistEditUserArea.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TFSistEditUserArea.Cargar;
var
   i: Integer;
   lState: boolean;
begin
     lState := FALSE;
     dmSistema.CargaListaAreas( FLista );
     with Areas do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       //Items.Add( Values[ Names[ i ] ] );
                       Items.Add( FLista[i] );
                       Checked[ i ] := ( Integer( Objects[ i ] ) > 0 );
                       if Checked[ i ] then
                          lState := TRUE;
                  end;
             end;
             if not lState then
                PrendeApaga( TRUE );
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TFSistEditUserArea.Descargar;
var
   i: Integer;
begin
     with Areas do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Checked[ i ] then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaAreas( FLista );
end;

procedure TFSistEditUserArea.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Areas do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
     AreasClickCheck( Areas );
end;

procedure TFSistEditUserArea.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TFSistEditUserArea.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TFSistEditUserArea.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUserArea.AreasClickCheck(Sender: TObject);
begin
     inherited;
     with OK do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

end.
