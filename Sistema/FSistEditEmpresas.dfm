inherited SistEditEmpresas: TSistEditEmpresas
  Left = 487
  Top = 227
  ClientHeight = 372
  ClientWidth = 438
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited Label1: TLabel
    Left = 78
    Enabled = True
  end
  inherited Label2: TLabel
    Left = 55
    Width = 59
    Caption = '&Descripci'#243'n:'
  end
  object Label3: TLabel [2]
    Left = 41
    Top = 95
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = '&Base de Datos:'
  end
  object DigitoGafetesLBL: TLabel [3]
    Left = 27
    Top = 117
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'D'#237'gito en &Gafetes:'
    FocusControl = CM_DIGITO
  end
  object lbReportesKiosco: TLabel [4]
    Left = 18
    Top = 169
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Reportes en Kiosco:'
    FocusControl = CM_KCLASIFI
  end
  inherited PanelBotones: TPanel
    Top = 336
    Width = 438
    TabOrder = 11
    inherited OK: TBitBtn
      Left = 272
    end
    inherited Cancelar: TBitBtn
      Left = 355
    end
  end
  inherited PanelSuperior: TPanel
    Width = 438
    TabOrder = 9
  end
  inherited PanelIdentifica: TPanel
    Width = 438
    TabOrder = 10
    inherited ValorActivo2: TPanel
      Width = 112
    end
  end
  inherited CM_NOMBRE: TDBEdit
    Left = 117
    Width = 316
  end
  inherited CM_CODIGO: TZetaDBEdit
    Left = 117
    Enabled = True
  end
  inherited GroupBox1: TGroupBox
    Left = 440
    Top = 190
    Width = 169
    Height = 91
    Align = alNone
    TabOrder = 8
    Visible = False
    inherited Label8: TLabel
      Left = 41
    end
    inherited Label9: TLabel
      Left = 20
    end
    inherited Label10: TLabel
      Left = 182
      Top = 40
    end
    inherited Label5: TLabel
      Left = 174
      Top = 23
    end
    inherited Label7: TLabel
      Left = 89
    end
    inherited CM_USRNAME: TDBEdit
      Left = 117
      Width = 52
    end
    inherited CM_PASSWRD: TDBEdit
      Left = 181
      Top = 60
      Width = 52
    end
    inherited CM_CONTROL: TDBEdit
      Left = 213
      Top = 19
      Width = 52
    end
    inherited CM_DATOS: TDBEdit
      Left = 117
      Width = 52
    end
    inherited CM_ALIAS: TDBEdit
      Left = 117
      Width = 52
    end
  end
  object CM_USACAFE: TDBCheckBox [11]
    Left = 43
    Top = 135
    Width = 87
    Height = 14
    Alignment = taLeftJustify
    Caption = '&Usa Cafeter'#237'a:'
    DataField = 'CM_USACAFE'
    DataSource = DataSource
    TabOrder = 4
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object CM_DIGITO: TDBEdit [12]
    Left = 117
    Top = 113
    Width = 23
    Height = 21
    DataField = 'CM_DIGITO'
    DataSource = DataSource
    MaxLength = 1
    TabOrder = 3
    OnKeyPress = CM_DIGITOKeyPress
  end
  object CM_KCLASIFI: TZetaKeyCombo [13]
    Left = 117
    Top = 165
    Width = 300
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 6
    OnChange = CM_KCLASIFIChange
    Items.Strings = (
      '')
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object gbConfidencialidad: TGroupBox [14]
    Left = 22
    Top = 186
    Width = 412
    Height = 143
    Caption = ' Conf&idencialidad: '
    TabOrder = 7
    object btSeleccionarConfiden: TSpeedButton
      Left = 380
      Top = 48
      Width = 23
      Height = 23
      Hint = 'Seleccionar Confidencialidad'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      OnClick = btSeleccionarConfidenClick
    end
    object listaConfidencialidad: TListBox
      Left = 94
      Top = 48
      Width = 285
      Height = 81
      TabStop = False
      ExtendedSelect = False
      ItemHeight = 13
      TabOrder = 2
    end
    object rbConfidenNinguna: TRadioButton
      Left = 94
      Top = 11
      Width = 123
      Height = 17
      Caption = 'Sin Confidencialidad'
      Checked = True
      TabOrder = 0
      OnClick = rbConfidenNingunaClick
    end
    object rbConfidenAlgunas: TRadioButton
      Left = 94
      Top = 28
      Width = 156
      Height = 17
      Caption = 'Aplica algunas'
      TabOrder = 1
      OnClick = rbConfidenAlgunasClick
    end
  end
  object CM_USACASE: TDBCheckBox [15]
    Left = 54
    Top = 150
    Width = 76
    Height = 14
    Alignment = taLeftJustify
    Caption = 'Usa Ca&seta:'
    DataField = 'CM_USACASE'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object DB_CODIGO: TZetaDBKeyLookup [16]
    Left = 117
    Top = 91
    Width = 316
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 85
    DataField = 'DB_CODIGO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
end
