inherited BitacoraRegBio: TBitacoraRegBio
  Left = 243
  Top = 176
  Caption = 'Bit'#225'cora Terminales GTI'
  ClientHeight = 472
  ClientWidth = 984
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 984
    inherited ValorActivo2: TPanel
      Width = 725
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 984
    Height = 113
    Align = alTop
    TabOrder = 1
    object GroupBox3: TGroupBox
      Left = 6
      Top = -1
      Width = 433
      Height = 111
      Caption = ' Filtros '
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 17
        Width = 44
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 11
        Top = 64
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Terminal:'
      end
      object Label5: TLabel
        Left = 11
        Top = 87
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Mensaje:'
      end
      object Label3: TLabel
        Left = 23
        Top = 40
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Inicio:'
      end
      object Label4: TLabel
        Left = 174
        Top = 40
        Width = 17
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fin:'
      end
      object luEmpresa: TZetaKeyLookup
        Left = 56
        Top = 13
        Width = 300
        Height = 21
        Hint = 'Empresas a mostrar en bit'#225'cora'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        ShowHint = True
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object BtnFiltrar: TBitBtn
        Left = 361
        Top = 32
        Width = 63
        Height = 49
        Hint = 'Refrescar b'#250'squeda con filtros'
        Caption = 'Refrescar'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtnFiltrarClick
        Glyph.Data = {
          42020000424D4202000000000000420000002800000010000000100000000100
          1000030000000002000000000000000000000000000000000000007C0000E003
          00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
          1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
          00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C}
        Layout = blGlyphTop
      end
      object zTerminales: TZetaKeyLookup
        Left = 56
        Top = 60
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object edtMensaje: TEdit
        Left = 56
        Top = 83
        Width = 273
        Height = 21
        TabOrder = 4
      end
      object dInicio: TZetaFecha
        Left = 56
        Top = 36
        Width = 115
        Height = 22
        Cursor = crArrow
        Hint = 'Fecha inicial'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '17/Jan/12'
        Valor = 40925.000000000000000000
      end
      object dFin: TZetaFecha
        Left = 195
        Top = 36
        Width = 115
        Height = 22
        Cursor = crArrow
        Hint = 'Fecha final'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = '17/Jan/12'
        Valor = 40925.000000000000000000
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [2]
    Left = 0
    Top = 132
    Width = 984
    Height = 340
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'Empresa'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'Empleado'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WS_FECHA'
        Title.Caption = 'Fecha'
        Width = 160
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WS_CHECADA'
        Title.Caption = 'Identificador'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CH_RELOJ'
        Title.Caption = 'Terminal'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WS_MENSAJE'
        Title.Caption = 'Mensaje'
        Width = 700
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 192
    Top = 280
  end
end
