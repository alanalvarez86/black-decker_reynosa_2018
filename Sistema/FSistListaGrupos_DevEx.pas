unit FSistListaGrupos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid;

type
  TSistListaGrupos_DevEx = class(TBaseGridLectura_DevEx)
    GP_CODIGO: TcxGridDBColumn;
    GP_DESCRIP: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  end;

var
  SistListaGrupos_DevEx: TSistListaGrupos_DevEx;

implementation

uses
     ZetaCommonClasses,
     dSistema;

{$R *.dfm}

{ TSistListaGrupos }

procedure TSistListaGrupos_DevEx.Agregar;
begin
     inherited;
      dmSistema.cdsListaGrupos.Agregar;
end;

procedure TSistListaGrupos_DevEx.Borrar;
begin
     inherited;
      dmSistema.cdsListaGrupos.Borrar;
end;

procedure TSistListaGrupos_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsListaGrupos.Conectar;
          DataSource.DataSet := cdsListaGrupos;
     end;
end;

procedure TSistListaGrupos_DevEx.Modificar;
begin
     inherited;
      dmSistema.cdsListaGrupos.Modificar;
end;

procedure TSistListaGrupos_DevEx.Refresh;
begin
     inherited;
      dmSistema.cdsListaGrupos.Refrescar;
end;

procedure TSistListaGrupos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_GRUPOS;
end;

procedure TSistListaGrupos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
end;

end.
