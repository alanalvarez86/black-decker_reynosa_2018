inherited SistEmpresas: TSistEmpresas
  Top = 119
  Caption = 'Empresas'
  ClientWidth = 821
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 821
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 495
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 821
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CM_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 250
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_CTRL_RL'
        Title.Caption = 'Tipo'
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_DIGITO'
        Title.Caption = 'D'#237'gito'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_USACAFE'
        Title.Caption = 'Cafeter'#237'a'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_USACASE'
        Title.Caption = 'Caseta'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CM_DATOS'
        Title.Caption = 'Base de Datos'
        Width = 346
        Visible = True
      end>
  end
end
