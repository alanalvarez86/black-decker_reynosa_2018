﻿unit FSistEditUsuarioArea_devEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
  cxContainer, cxEdit, cxCheckListBox;

type
  TFSistEditUserArea_DevEx = class(TZetaDlgModal_DevEx)
    Areas: TcxCheckListBox;
    Prender: TcxButton;
    Apagar: TcxButton;
    PanelBusqueda: TPanel;
    Label1: TLabel;
    AreaBuscaBtn: TcxButton;
    EditBusca: TEdit;
    MostrarActivos: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);                                                                                                
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure AreaCheck(Sender: TObject; AIndex: Integer; APrevState,
      ANewState: TcxCheckBoxState);
    procedure AreaBuscaBtnClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroArea: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  FSistEditUserArea_DevEx: TFSistEditUserArea_DevEx;

implementation

uses ZetaCommonClasses,
     ZGlobalTress,
     DGlobal,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema;

{$R *.DFM}

procedure TFSistEditUserArea_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
     HelpContext := H80839_Asignando_areas_de_Labor_a_los_usuarios;
end;

procedure TFSistEditUserArea_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     OK_DevEx.Enabled := False;
     Cargar;
     ActiveControl := Areas;
     Caption :=  Format( 'Asignar %s A %s', [ Global.GetGlobalString( K_GLOBAL_LABOR_AREAS ), dmSistema.cdsUsuarios.FieldByName( 'US_NOMBRE' ).AsString ] );
     EditBusca.Text := VACIO;
     FPosUltimo:= -1;
     FUltimoTexto:= VACIO;
end;

procedure TFSistEditUserArea_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TFSistEditUserArea_DevEx.Cargar;
var
   i: Integer;
   lState: boolean;
begin
     lState := FALSE;
     dmSistema.CargaListaAreas( FLista );
     with Areas do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       //Items.Add( Values[ Names[ i ] ] );
                       with Items.Add do
                       begin
                       Text :=  FLista[i] ;
                       Checked := ( Integer( Objects[ i ] ) > 0 );
                       end;
                       if items[i].Checked then
                          lState := TRUE;
                  end;
             end;
             if not lState then
                PrendeApaga( TRUE );
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TFSistEditUserArea_DevEx.Descargar;
var
   i: Integer;
begin
     with Areas do
     begin
      for i := 0 to ( Count - 1 ) do
          begin
            with Items[i] do
            begin
                if Checked then
                  FLista.Objects[ i ] := TObject( 1 )
                else
                  FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaAreas( FLista );
end;

procedure TFSistEditUserArea_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Areas do
     begin
        try
          Items.BeginUpdate;
          for i := 0 to ( Count - 1 ) do
            with Items[i] do
            begin
              Checked := lState;
            end;
        finally
          items.EndUpdate;
        end;
     end;
    // AreasClickCheck( AreaCheck );
end;

procedure TFSistEditUserArea_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
       with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TFSistEditUserArea_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
       with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TFSistEditUserArea_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUserArea_DevEx.OK_DevExClick(Sender: TObject);
begin
 inherited;
     Descargar;
end;

procedure TFSistEditUserArea_DevEx.AreaBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Areas.Count )then
     begin
          lEncontro:= EncontroArea;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Areas.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'Búsqueda de áreas', '! No hay otra área con esos datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'Búsqueda de áreas', '! No hay un área con esos datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;


function TFSistEditUserArea_DevEx.EncontroArea: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Areas do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].Text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TFSistEditUserArea_DevEx.AreaCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
   with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;

end;

end.
