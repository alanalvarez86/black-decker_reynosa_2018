unit FGlobalEnrolamientoUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, ZetaEdit,
  CheckLst, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxMemo, ImgList, cxButtons, cxCheckListBox,
  ZetaKeyLookup_DevEx;

type
  TGlobalEnrolamientoUsuarios_DevEx = class(TBaseGlobal_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    BtnFormulaUsuario: TcxButton;
    BtnFormulaPwd: TcxButton;
    BtnFormulaMail: TcxButton;
    BtnFormulaActiveDirectory: TcxButton;
    Label5: TLabel;
    AutoUser: TZetaKeyCombo;
    DominioAD: TZetaEdit;
    Label7: TLabel;
    GrupoDefault: TZetaKeyLookup_DevEx;
    Label8: TLabel;
    lblRoles: TLabel;
    CambiarPwd: TCheckBox;
    RolesDefault: TcxCheckListBox;
    edtRoles: TEdit;
    edtRoles2: TEdit;
    FormulaUsuario: TcxMemo;
    FormulaPwd: TcxMemo;
    FormulaMail: TcxMemo;
    FormulaActiveDirectory: TcxMemo;
    procedure BtnFormulaUsuarioClick(Sender: TObject);
    procedure BtnFormulaPwdClick(Sender: TObject);
    procedure BtnFormulaMailClick(Sender: TObject);
    procedure BtnFormulaActiveDirectoryClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Cargar; override;
    procedure Descargar; override;
  end;

var
  GlobalEnrolamientoUsuarios_DevEx: TGlobalEnrolamientoUsuarios_DevEx;

implementation

uses
    ZConstruyeFormula,
    ZetaCommonLists,
    ZetaCommonClasses,
    ZetaTipoEntidad,
    ZGlobalTress,
    ZAccesosTress,
    ZetaDialogo,
    DSistema;

{$R *.dfm}

procedure TGlobalEnrolamientoUsuarios_DevEx.BtnFormulaUsuarioClick(Sender: TObject);
begin
     inherited;
     FormulaUsuario.Text := GetFormulaConstRelaciones( enEmpleado, FormulaUsuario.Lines.Text, FormulaUsuario.SelStart, evBase, FALSE  );
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.BtnFormulaPwdClick(Sender: TObject);
begin
     inherited;
     FormulaPwd.Text := GetFormulaConstRelaciones( enEmpleado, FormulaPwd.Lines.Text, FormulaPwd.SelStart, evBase, FALSE  );
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.BtnFormulaMailClick(Sender: TObject);
begin
     inherited;
     FormulaMail.Text := GetFormulaConstRelaciones( enEmpleado, FormulaMail.Lines.Text, FormulaMail.SelStart, evBase, FALSE  );
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.BtnFormulaActiveDirectoryClick(
  Sender: TObject);
begin
     inherited;
     FormulaActiveDirectory.Text := GetFormulaConstRelaciones( enEmpleado, FormulaActiveDirectory.Lines.Text, FormulaActiveDirectory.SelStart, evBase, FALSE  );
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos                      := ZAccesosTress.D_CAT_CONFI_ENROLL;
     AutoUser.Tag                       := K_GLOBAL_ENROLL_AUTO_USER;
     DominioAD.Tag                      := K_GLOBAL_ENROLL_DOMINIO_AD;
     FormulaUsuario.Tag                 := K_GLOBAL_ENROLL_FORMULA_USER;
     FormulaPwd.Tag                     := K_GLOBAL_ENROLL_FORMULA_PWD;
     FormulaMail.Tag                    := K_GLOBAL_ENROLL_FORMULA_MAIL;
     FormulaActiveDirectory.Tag         := K_GLOBAL_ENROLL_FORMULA_AD;
     GrupoDefault.Tag                   := K_GLOBAL_ENROLL_GRUPO_DEFAULT;
     GrupoDefault.LookupDataset         := dmSistema.cdsGrupos;
     edtRoles.Tag                       := K_GLOBAL_ENROLL_ROLES_DEFAULT;
     edtRoles2.Tag                      := K_GLOBAL_ENROLL_ROLES_DEFAULT2;
     CambiarPwd.Tag                     := K_GLOBAL_ENROLL_CAMBIAR_PWD;

     HelpContext                        := H_GLOBAL_ENROLL;

     //Deshabilitar controles en la version de dos capas
     {$ifdef DOS_CAPAS}
     RolesDefault.Enabled := False;
     lblRoles.Enabled := False;
     {$endif}
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.FormShow(Sender: TObject);
begin
     dmSistema.cdsGrupos.Conectar;
     {$ifndef DOS_CAPAS}
     dmSistema.cdsRoles.Conectar;
     {$endif}
     inherited;
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.OKClick(Sender: TObject);
begin
     if GrupoDefault.Valor < 1 then
     begin
          //Mostrar Error
          ZetaDialogo.ZError( '', 'El Grupo Default no puede quedar vacio', 0 );
          GrupoDefault.SetFocus;
     end
     else
     begin
          inherited;
     end;
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.Cargar;
{$ifndef DOS_CAPAS}
var
   sCodigo, sNombre, sRegistro : string;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     //Cargar los roles en el CheckListBox
     with dmSistema.cdsRoles do
     begin
          First;
          RolesDefault.Clear;
          while not Eof do
          begin
               sCodigo := FieldByName('RO_CODIGO').AsString;
               sNombre := FieldByName('RO_NOMBRE').AsString;
               sRegistro := sCodigo + ' : ' + sNombre;
               with RolesDefault.Items.Add do
               begin
                    text := sRegistro ;
                    if ( pos( sCodigo + ',', edtRoles.Text + edtRoles2.Text ) > 0 ) then
                    begin
                      checked := True;
                    end;
                    Next;
              end
        end;
    end;
     {$endif}
end;

procedure TGlobalEnrolamientoUsuarios_DevEx.Descargar;
{$ifndef DOS_CAPAS}
var
   i : integer;
   sCodigo, sAcumulado : string;
{$endif}
begin
     {$ifndef DOS_CAPAS}
     //Almacenar los registros de roles
     sAcumulado := VACIO;
     edtRoles.Text := VACIO;
     edtRoles2.Text := VACIO;

     for i := 0 to RolesDefault.Items.Count - 1 do
     begin
          if RolesDefault.Items[i].Checked then
          begin
               sCodigo := RolesDefault.Items[i].text;
               sCodigo := Trim( copy( sCodigo, 1, pos( ' : ', sCodigo ) ) ) + ',';
               //sCodigo ya trae la coma

               //Acumular codigos
               sAcumulado := sAcumulado + sCodigo;
          end;
     end;//for i := 0 to RolesDefault.Items.Count - 1 do

     //Asignar acumulado al edit topado a 255 caracteres
     edtRoles.Text := copy( sAcumulado, 1, 255 );

     //Asignar el resto del acumulado al edit 2 topado a 255 caracteres
     edtRoles2.Text := copy( sAcumulado, 256, 510 );
     {$endif}
     inherited;
end;

end.
