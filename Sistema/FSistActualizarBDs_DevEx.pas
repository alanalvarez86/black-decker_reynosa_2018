{$HINTS OFF}
unit FSistActualizarBDs_DevEx;

interface

uses
  ZetaClientDataSet, ZetaCommonClasses, ZetaMessages, Windows, Messages, DB, Graphics, Dialogs, Forms, Classes,
  ActnList, Controls, StdCtrls, Grids, DBGrids, ZetaDBGrid, Buttons, ExtCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  ImgList, cxButtons,cxImage;

type

  TAvanceProceso = class(TThread)
  private
    { Private declarations }
    fMutex : THandle;
    fParams: TZetaParams;
    fDBGrid: TDBGrid;
    oCDS   : TZetaClientDataSet;
    fDS    : TDataSet;
   //bm     : TBookmarkStr;
   {$ifdef TRESS_DELPHIXE5_UP}
   bm     : TBookmark;
   {$else}
    bm     : TBookmarkStr;
   {$endif}
    procedure ActualizaGrid;
  protected
    procedure Execute; override;
  public
    constructor Create(Mutex: THandle; DBGrid: TDBGrid; Params: TZetaParams; AlTerminar: TNotifyEvent; Lista: TList);
    destructor Destroy; override;
  end;

  THackDBGrid = class(TDBGrid);

  TSistActualizarDBs_DevEx = class(TForm)
    DBGrid: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    Button1: TcxButton;
    Button2: TcxButton;
    ActionList1: TActionList;
    actTodos: TAction;
    actNinguno: TAction;
    Panel2: TPanel;
    btnProceso: TcxButton;
    actIniciar: TAction;
    BitBtn1: TcxButton;
    actSalir: TAction;
    cmbVersion: TComboBox;
    DataSource: TDataSource;
    Button3: TcxButton;
    actActualizarTabla: TAction;
    cxImageList1: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure DBGridCellClick(Column: TColumn);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure actTodosExecute(Sender: TObject);
    procedure actIniciarExecute(Sender: TObject);
    procedure actSalirExecute(Sender: TObject);
    procedure DBGridColExit(Sender: TObject);
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridColEnter(Sender: TObject);
    procedure cmbVersionClick(Sender: TObject);
    procedure actActualizarTablaExecute(Sender: TObject);
    procedure DBGridMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
    Mutex         : THandle;
    fEmpresa      : string;
    fUltimaVersion: Integer;
    fParams       : TZetaParams;
    FTermino      : Boolean;
    Estado        : string;
    fAvances      : TList;
    procedure HabilitaControles(const Enable: Boolean);
    procedure AlCambiarRegistro(DataSet: TDataSet);
    procedure MostrarCombo;
    function SiguienteColumna: Integer;
    procedure SetEmpresa(const Value: string);
    function GetUltimaVersion: Integer;
    procedure ActivaDB_APLICAPATCH(AField: TField);
    function GetDatosConexion: OleVariant;
    procedure SetResultadoEsp(Resultado: OleVariant);
    procedure ThreadActualizaFinaliza(Sender: TObject);
    function Processing: Boolean;
  protected
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    function EmpresaActualizada(const Empresa: string): Boolean;
    property Empresa: string read fEmpresa write SetEmpresa;
    property UltimaVersion: Integer read GetUltimaVersion;
  end;

var
  SistActualizarDBs_DevEx: TSistActualizarDBs_DevEx;

implementation

uses
  SysUtils, Variants, ZAccesosTress, ZetaWinAPITools, dSistema, ZetaDialogo, DProcesos, DCliente, ZetaCommonLists,
  FProcessLogShow_DevEx, DConsultas, ZetaCommonTools, ActiveX, Themes;

{$R *.dfm}

{* Dibujar una barra de progreso. @author http://stackoverflow.com/questions/7044125/delphi-draw-own-progress-bar-in-list-view}
procedure DrawStatus(DC: HDC; R: TRect; Font: TFont; const Txt: String; Progress: Single);
var
  TxtRect      : TRect;
  S            : string;
  Details      : TThemedElementDetails;
  SaveBrush    : HBRUSH;
  SavePen      : HPEN;
  TxtFont      : TFont;
  SaveFont     : HFONT;
  SaveTextColor: COLORREF;
begin
  FillRect(DC, R, 0);
  InflateRect(R, -1, -1);
  TxtFont := TFont.Create;
  TxtRect := R;
  SaveFont      := SelectObject(DC, TxtFont.Handle);
  SaveTextColor := SetTextColor(DC, GetSysColor(COLOR_GRAYTEXT));
  S       := Format('%s %.1f%%', [Txt, Progress * 100]);
  if ThemeServices.ThemesEnabled then begin
    Details := ThemeServices.GetElementDetails(tpBar);
    ThemeServices.DrawElement(DC, Details, R, nil);
    InflateRect(R, -2, -2);
    R.Right := R.Left + Trunc((R.Right - R.Left) * Progress);
    Details := ThemeServices.GetElementDetails(tpChunk);
    ThemeServices.DrawElement(DC, Details, R, nil);
  end else begin
    SavePen   := SelectObject(DC, CreatePen(PS_NULL, 0, 0));
    SaveBrush := SelectObject(DC, CreateSolidBrush($00EBEBEB));
    Inc(R.Right);
    Inc(R.Bottom);
    RoundRect(DC, R.Left, R.Top, R.Right, R.Bottom, 3, 3);
    R.Right := R.Left + Trunc((R.Right - R.Left) * Progress);
    DeleteObject(SelectObject(DC, CreateSolidBrush($00FFC184)));
    RoundRect(DC, R.Left, R.Top, R.Right, R.Bottom, 3, 3);
    if R.Right > R.Left + 3 then
      Rectangle(DC, R.Right - 3, R.Top, R.Right, R.Bottom);
    DeleteObject(SelectObject(DC, SaveBrush));
    DeleteObject(SelectObject(DC, SavePen));
  end;
  try
    TxtFont.Assign(Font);
    TxtFont.Height := TxtRect.Bottom - TxtRect.Top;
    TxtFont.Color  := clGrayText;
    SetBkMode(DC, TRANSPARENT);
    SaveFont      := SelectObject(DC, TxtFont.Handle);
    SaveTextColor := SetTextColor(DC, GetSysColor(COLOR_GRAYTEXT));
    DrawText(DC, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(S), -1, TxtRect, DT_SINGLELINE or DT_CENTER or DT_VCENTER or DT_END_ELLIPSIS or DT_NOPREFIX);
    SetBkMode(DC, TRANSPARENT);
  finally
    DeleteObject(SelectObject(DC, SaveFont));
    SetTextColor(DC, SaveTextColor);
    TxtFont.Free;
  end;
end;

{ TAvanceProceso }

constructor TAvanceProceso.Create(Mutex: THandle; DBGrid: TDBGrid; Params: TZetaParams; AlTerminar: TNotifyEvent; Lista: TList);
begin
  fMutex  := Mutex;
  fParams := TZetaParams.Create;
  fParams.Assign(Params);
  fDBGrid         := DBGrid;
  fDS             := fDBGrid.DataSource.DataSet;
  FreeOnTerminate := True;
  OnTerminate     := AlTerminar;
  oCDS            := TZetaClientDataSet.Create(nil);
  Lista.Add(Self);
  inherited Create(False);
end;

destructor TAvanceProceso.Destroy;
begin
  FreeAndNil(fParams);
  FreeAndNil(oCDS);
  inherited;
end;

procedure TAvanceProceso.ActualizaGrid;
begin
  bm := fDS.Bookmark;
  fDS.DisableControls;
  if fDS.Locate('DB_CODIGO', VarArrayOf([fParams.ParamByName('DB_CODIGO').AsString]), [loCaseInsensitive]) then
    try
      fDS.Edit;
      if fDS.FieldByName('DB_PROCESO').AsInteger = 0 then begin
        fDS.FieldByName('DB_PROCESO').AsInteger := oCDS.FieldByName('PC_NUMERO').AsInteger;
        fParams.ParamByName('DB_PROCESO').AsInteger := oCDS.FieldByName('PC_NUMERO').AsInteger;
      end;
      if oCDS.FieldByName('PC_MAXIMO').AsInteger = 0 then
        fDS.FieldByName('DB_AVANCE').AsFloat := 0
      else
        fDS.FieldByName('DB_AVANCE').AsFloat := oCDS.FieldByName('PC_PASO').AsInteger / oCDS.FieldByName('PC_MAXIMO').AsInteger * 100;
      fDS.Post;
    except
      fDS.Cancel;
    end;
  fDS.Bookmark := bm;
  fDS.EnableControls;
end;

procedure TAvanceProceso.Execute;
begin
  CoInitialize(nil);
  while not Terminated do begin
    WaitForSingleObject(fMutex, INFINITE);
    try
      oCDS.Data := dmSistema.GetMotorPatchAvance(fParams.VarValues);
      if not oCDS.IsEmpty then begin
        Synchronize(ActualizaGrid);
        if oCDS.FieldByName('PC_ERROR').AsString <> B_PROCESO_ABIERTO then
          Break; // Salir del m�todo con escala en el finally
      end;
    finally
      ReleaseMutex(fMutex);
    end;
    Sleep(500);  // Actualizar cada X milisegundos
  end;

  // Establecer a 100% para que no siga pintando el avance
  WaitForSingleObject(fMutex, INFINITE);
  bm := fDS.Bookmark;
  fDS.DisableControls;
  try
    if fDS.Locate('DB_CODIGO', VarArrayOf([fParams.ParamByName('DB_CODIGO').AsString]), [loCaseInsensitive]) then
      try
        fDS.Edit;
        fDS.FieldByName('DB_AVANCE').AsInteger := 100;
        fDS.Post;
      except
        fDS.Cancel;
      end;
  finally
    fDS.Bookmark := bm;
    fDS.EnableControls;
    ReleaseMutex(fMutex);
  end;
end;

{ TSistActualizarDBs }

procedure TSistActualizarDBs_DevEx.FormCreate(Sender: TObject);
begin
  inherited;

  Mutex := CreateMutex(nil, False, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ClassName + '.Execute'));
  if Mutex = 0 then
    RaiseLastOSError;

  fAvances := TList.Create;
  fEmpresa := '';
  HelpContext := H_SIST_ACTUALIZAR_BDS;
  //IndexDerechos := D_SIST_ACTUALIZAR_BDS;

  with dmSistema do begin
    cdsSistActualizarBDs.Conectar;
    cdsSistBaseDatos.Conectar;
    DataSource.DataSet := cdsSistActualizarBDs;
    DataSource.DataSet.AfterScroll := AlCambiarRegistro;
  end;
end;

procedure TSistActualizarDBs_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  inherited;
  CanClose := not Processing;
end;

procedure TSistActualizarDBs_DevEx.FormDestroy(Sender: TObject);
begin
  DataSource.DataSet.Close;
  DataSource.DataSet.AfterScroll := nil;
  DataSource.DataSet := nil;
  FreeAndNil(fParams);
  FreeAndNil(fAvances);
  CloseHandle(Mutex);
  inherited;
end;

{* Procesa las acciones hechas con el rat�n. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.DBGridCellClick(Column: TColumn);
begin
  inherited;
  if not Assigned(Column) then
    Exit;

  if (Column.Field.DataType = ftBoolean) then begin
    ActivaDB_APLICAPATCH(Column.Field)
  end else if (Column.FieldName = 'DB_PROCESO') and (Column.Field.AsInteger <> 0) and (not Processing) then begin
     with dmConsultas do
     begin
          BaseDatosImportacionXMLCSV := GetDatosConexion;
          LeerUnProceso(Column.Field.AsInteger,True);
     end;
  end;
end;

{* Construye la conexion de BD Seleccionada. @author: EZ}
function TSistActualizarDBs_DevEx.GetDatosConexion : OleVariant;
begin
     with dmSistema do
     begin
          cdsSistBaseDatos.Locate('DB_CODIGO',(DBGrid.DataSource.DataSet.FieldByName('DB_CODIGO').AsString),[]);
          Result := GetBaseDatosImportacion;
     end;
end;


{* Despliega en pantalla los valores de los campos. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
  CtrlState          : array[Boolean] of Integer       = (DFCS_BUTTONCHECK           , DFCS_BUTTONCHECK or DFCS_CHECKED);
  CtrlStateXP        : array[Boolean] of TThemedButton = (tbCheckBoxUncheckedNormal  , tbCheckBoxCheckedNormal);
  CtrlStateXPDisabled: array[Boolean] of TThemedButton = (tbCheckBoxUncheckedDisabled, tbCheckBoxCheckedDisabled);
var
  ClassicState: Integer;
  R           : TRect;
  bActivo     : Boolean;
  DrawState   : TGridDrawState absolute State;
  Details     : TThemedElementDetails;
begin
  inherited;                              
  R := Rect;
  with DBGrid.DataSource.DataSet do
    if Column.Field.DataType = ftBoolean then begin
      DBGrid.Canvas.FillRect(R);
      bActivo := not ( ((Column.FieldName <> 'DB_APLICAPATCH') and (not FieldByName('DB_APLICAPATCH').AsBoolean)) or
                       ((Column.FieldName = 'DB_APLICAESPECIALES') and (not ZStrToBool(FieldByName('DB_ESPC').AsString))) or
                       ((FieldByName('DB_VERSION').AsInteger >= FieldByName('DB_APLICAVERSION').AsInteger))
                     );
      if (Column.FieldName = 'DB_APLICAPATCH') and (fEmpresa <> '') then
        bActivo := bActivo and (UpperCase(FieldByName('DB_CODIGO').AsString) = UpperCase(fEmpresa));

      if ThemeServices.ThemesEnabled then begin
        if bActivo then
          Details := ThemeServices.GetElementDetails(CtrlStateXP[Column.Field.AsBoolean])
        else
          Details := ThemeServices.GetElementDetails(CtrlStateXPDisabled[Column.Field.AsBoolean]);
        ThemeServices.DrawElement(DBGrid.Canvas.Handle, Details, Rect);
      end else begin
        InflateRect(R, -2, -2);
        ClassicState := CtrlState[Column.Field.AsBoolean];
        if not bActivo then
          ClassicState := ClassicState or DFCS_INACTIVE;
        DrawFrameControl(DBGrid.Canvas.Handle, R, DFC_BUTTON, ClassicState);
      end;
    end else begin
      if (Column.FieldName = 'DB_PROCESO') and (Column.Field.AsInteger <> 0) and (not Processing) then begin
        DBGrid.Canvas.Font.Style := DBGrid.Canvas.Font.Style + [fsUnderLine];
        DBGrid.Canvas.Font.Color := Graphics.clBlue;
      end else begin
        DBGrid.Canvas.Font.Color := DBGrid.Font.Color;
      end;

      if (Column.FieldName = 'DB_STATUS') and (FieldByName('DB_AVANCE').AsFloat > 0)  and (FieldByName('DB_AVANCE').AsFloat < 100) then begin
        InflateRect(R, -1, -1);
        DrawStatus(DBGrid.Canvas.Handle, R, DBGrid.Font, '', FieldByName('DB_AVANCE').AsFloat / 100);
      end else
        DBGrid.DefaultDrawColumnCell(Rect, DataCol, Column, DrawState) ;
    end;
end;

{* Procesa las acciones hechas con el teclado. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.DBGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  with DBGrid do
    if ((SelectedField.DataType = ftBoolean) and (key = VK_SPACE)) then
      ActivaDB_APLICAPATCH(SelectedField);
end;

{* Marcar/Desmarcar todas las BDs para aplicar Patch. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.actTodosExecute(Sender: TObject);
var
  // bm: string;
  {$ifdef TRESS_DELPHIXE5_UP}
  bm: TBookmark;
  {$else}
  bm: string;
  {$endif}
  bOk: Boolean;
begin
  inherited;
  with DataSource.DataSet do begin
    DisableControls;
    bm := Bookmark;
    First;
    while not Eof do begin
      bOk := (Sender = actTodos) and (FieldByName('DB_APLICAVERSION').AsInteger > 0) and
             (FieldByName('DB_VERSION').AsInteger < FieldByName('DB_APLICAVERSION').AsInteger);
      if fEmpresa <> '' then
        bOk := bOk and (UpperCase(FieldByName('DB_CODIGO').AsString) = UpperCase(fEmpresa));
      Edit;
      FieldByName('DB_APLICAPATCH').AsBoolean  := bOk;
      Post;
      Next;
    end;
    Bookmark := bm;
    EnableControls;
  end
end;

{* Iniciar la aplicaci�n del Patch. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.actIniciarExecute(Sender: TObject);
var
  I, C     : Integer;
  //bm       : TBookmarkStr;
  {$ifdef TRESS_DELPHIXE5_UP}
  bm       : TBookmark;
  {$else}
  bm       : TBookmarkStr;
  {$endif}

  Params   : TZetaParams;
  Param    : TParam;
begin
  inherited;
  HabilitaControles(False);
  I := 0;
  Screen.Cursor := crHourGlass;
  with DBGrid.DataSource.DataSet do begin
    bm := Bookmark;
    Params := TZetaParams.Create;
    DisableControls;
    WaitForSingleObject(Mutex, INFINITE);
    try
       try
         First;
         while not Eof do begin
           if FieldByName('DB_APLICAPATCH').AsBoolean then begin
             Params.Clear;
             Inc(I);
             // Pasar la tupla a un TZetaParams
             for C := 0 to Fields.Count - 1 do begin
               Param          := Params.Add as TParam;
               Param.Name     := Fields[C].FieldName;
               Param.DataType := Fields[C].DataType;
               Param.Size     := Fields[C].Size;
               Param.Value    := Fields[C].Value;
             end;

             // RCM Here comes the magic
             FTermino := False;
             Estado := VACIO;
             try
               Edit;
               FieldByName('DB_PROCESO').AsInteger := 0;
               FieldByName('DB_STATUS').AsString   := 'Iniciando...';
               FieldByName('DB_AVANCE').AsInteger  := 0;
               Post;
               dmProcesos.EjecutaMotorPatch(Params, Self.Handle, SetResultadoEsp);  // Versi�n con hilo
               TAvanceProceso.Create(Mutex, DBGrid, Params, ThreadActualizaFinaliza, fAvances); // Hilo para actualizar avance
               //dmSistema.EjecutaMotorPatch(Params); // Versi�n sin hilo
             except
               on e: Exception do begin
                 Edit;
                 FieldByName('DB_STATUS').AsString := e.Message;
                 Post;
               end;
             end
           end;
           Next;
         end;
         if I = 0 then begin
           ZetaDialogo.ZetaMessage(Caption, 'Seleccionar al menos una Base de Datos', mtInformation, [mbOK], 0, mbOk); // ToDo: Ayuda contextual
           HabilitaControles(True);
         end
       except
         on e: Exception do
           ZetaDialogo.ZError(Caption, 'Ocurri� un error al aplicar el Patch. ' + sLineBreak + e.Message, 0); // ToDo: Ayuda contextual
       end;
    finally
           ReleaseMutex(Mutex);
           FreeAndNil(Params);
           Bookmark := bm;
           EnableControls;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TSistActualizarDBs_DevEx.SetResultadoEsp(Resultado : OleVariant);
var
   Params: TZetaParams;
begin
     WaitForSingleObject(Mutex, INFINITE);
     Params := TZetaParams.Create(Self);
     try
        Params.VarValues := Resultado;
        with DataSource.DataSet do
        begin
             if Locate('DB_CODIGO',(Params.ParamByName('DB_CODIGO').AsString), []) then
             try
                Estado:= 'Actualizada';
                if (Params.ParamValues['PRO_ERRORES'] = 0) and (Params.ParamValues['PRO_ADVERTENCIAS'] = 0) then
                  Estado := Format('%s correctamente.', [Estado]);
                if (Params.ParamByName('PRO_ERRORES').AsInteger <> 0) then
                  Estado := Format('%d error(es)', [Params.ParamByName('PRO_ERRORES').AsInteger]);
                if (Params.ParamByName('PRO_ADVERTENCIAS').AsInteger <> 0) then
                  Estado := Format('%s con %d advertencia(s)', [Estado, Params.ParamByName('PRO_ADVERTENCIAS').AsInteger]);
                Edit;
                FieldByName('DB_APLICAPATCH').AsBoolean := (FieldByName('DB_APLICAVERSION').AsInteger > 0) and
                                                           (FieldByName('DB_VERSION').AsInteger < FieldByName('DB_APLICAVERSION').AsInteger);
                FieldByName('DB_STATUS').AsString := Estado;
                FieldByName('DB_PROCESO').AsInteger := Params.ParamValues['DB_PROCESO'];
                Post;
             finally
             end;
        end;
     finally
            ReleaseMutex(Mutex);
            FreeAndNil(Params);
     end;
     actActualizarTabla.Execute;
end;


{* Habilitar/Inhabilitar los controles para la entrada al usuario. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.HabilitaControles(const Enable: Boolean);
begin
  //DBGrid.Enabled := Enable;
  actTodos.Enabled    := Enable;
  actNinguno.Enabled  := Enable;
  actSalir.Enabled    := Enable;
  actActualizarTabla.Enabled := Enable;
  actIniciar.Enabled  := Enable;
end;

{* Cerrar la ventana si no est� aplicando Patch. @author: Ricardo Carrillo}
procedure TSistActualizarDBs_DevEx.actSalirExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrOk
end;

procedure TSistActualizarDBs_DevEx.AlCambiarRegistro(DataSet: TDataSet);
begin
  // Llenar el combo con las versiones a actualizar, omitir las versiones menores
  if Assigned(DataSource.DataSet) then with DataSource.DataSet do begin
    cmbVersion.Items.Text := FieldByName('DB_VERSIONESPATCH').AsString;
    cmbVersion.ItemIndex := cmbVersion.Items.IndexOf(DataSet.FieldByName('DB_APLICAVERSION').AsString);
    MostrarCombo();
  end;
end;

procedure TSistActualizarDBs_DevEx.DBGridColExit(Sender: TObject);
begin
  inherited;
  with DBGrid do
    if Assigned(SelectedField) and (SelectedField.FieldName = 'DB_APLICAVERSION' ) then
      cmbVersion.Visible := False
end;

procedure TSistActualizarDBs_DevEx.DBGridKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if (Key = Chr(9)) then
    Exit;

  if Assigned(DBGrid.SelectedField) and (DBGrid.SelectedField.FieldName = 'DB_APLICAVERSION') then begin
    cmbVersion.SetFocus;
    SendMessage(cmbVersion.Handle, WM_CHAR, Word(Key), 0);
  end
end;

procedure TSistActualizarDBs_DevEx.DBGridColEnter(Sender: TObject);
begin
  inherited;
  MostrarCombo();
end;

procedure TSistActualizarDBs_DevEx.cmbVersionClick(Sender: TObject);
begin
  inherited;
  with DataSource.DataSet do
    if not IsEmpty then begin
      Edit;
      FieldByName('DB_APLICAVERSION').AsString := cmbVersion.Text;
      Post;
    end;
end;

procedure TSistActualizarDBs_DevEx.MostrarCombo;
var
  Col, CurrRow: Integer;
  R: TRect;
begin
  with DBGrid do begin
    cmbVersion.Visible := Assigned(SelectedField) and
                          (DataSource.DataSet.FieldByName('DB_APLICAPATCH').AsBoolean) and
                          (SelectedField.FieldName = 'DB_APLICAVERSION') and
                          (SelectedField.AsInteger > 0);
    if cmbVersion.Visible then begin
      CurrRow := THackDBGrid(DBGrid).Row;
      for Col := 0 to Columns.Count - 1 do
        if Columns[Col].Field.FieldName = 'DB_APLICAVERSION' then begin
          R := THackDBGrid(DBGrid).CellRect(Col + 1, CurrRow);
          cmbVersion.Top    := R.Top + DBGrid.Top;
          cmbVersion.Left   := R.Left + DBGrid.Left + 1;
          cmbVersion.Width  := R.Right - R.Left + 1;
          cmbVersion.Height := R.Bottom - R.Top;
        end;
    end;
  end;
end;

function TSistActualizarDBs_DevEx.SiguienteColumna: Integer;
var
  I: Integer;
begin
  with DBGrid do begin
    I := SelectedIndex + 1;
    while (I < Columns.Count) and (Columns[i].ReadOnly or not Columns[i].Visible) do begin
      Inc(I);
    end;
    if (I = Columns.Count) then
      Result := 0
    else
      Result := I;
  end;
end;

procedure TSistActualizarDBs_DevEx.KeyPress(var Key: Char);
begin
  inherited;
  if ActiveControl = DBGrid then begin
    if ((Key <> Chr(9)) and (Key <> #0)) then begin
      if (DBGrid.SelectedField.FieldName = 'DB_APLICAVERSION') then begin
        cmbVersion.SetFocus;
        SendMessage(cmbVersion.Handle, WM_CHAR, Word(Key), 0);
        Key := #0;
      end;
    end;
  end else if (ActiveControl = cmbVersion) and ((Key = Chr(VK_RETURN)) or (Key = Chr(VK_TAB))) then begin
    Key := #0;
    with DBGrid do begin
      SetFocus;
      SelectedIndex := SiguienteColumna;
    end;
  end;
end;

procedure TSistActualizarDBs_DevEx.SetEmpresa(const Value: string);
begin
  fEmpresa := Value;
  actTodos.Execute;
end;

function TSistActualizarDBs_DevEx.EmpresaActualizada(const Empresa: string): Boolean;
begin
  with DataSource.DataSet do
    Result := Locate('DB_CODIGO', VarArrayOf([Empresa]), [loCaseInsensitive]) and
              (FieldByName('DB_VERSION').AsInteger = UltimaVersion);
end;

function TSistActualizarDBs_DevEx.GetUltimaVersion: Integer;
begin
  if fUltimaVersion = 0 then
    fUltimaVersion := dmCliente.UltimaVersion;
  Result := fUltimaVersion
end;

procedure TSistActualizarDBs_DevEx.ActivaDB_APLICAPATCH(AField: TField);
var
  bValor: Boolean;
begin
  with DataSource.DataSet do begin
    Edit;
    if (fEmpresa = '') or (AField.FieldName <> 'DB_APLICAPATCH') then
      bValor := not AField.AsBoolean
    else
      bValor := (UpperCase(FieldByName('DB_CODIGO').AsString) = UpperCase(fEmpresa));
    AField.AsBoolean := bValor and (FieldByName('DB_VERSION').AsInteger < FieldByName('DB_APLICAVERSION').AsInteger);
    Post;
  end;
end;

procedure TSistActualizarDBs_DevEx.actActualizarTablaExecute(Sender: TObject);
var
  cdsTemp: TZetaLookupDataSet;
  //bm: string;
  {$ifdef TRESS_DELPHIXE5_UP}
  bm: TBookmark;
  {$else}
  bm: string;
  {$endif}

begin
  // Pseudo-update: Actualizar solamente algunas columnas
  cdsTemp := TZetaLookupDataSet.Create(self);
  with DataSource.DataSet do try
    cdsTemp.AlAdquirirDatos := dmSistema.cdsSistActualizarBDsAlAdquirirDatos;
    DisableControls;
    bm := Bookmark;
    First;
    cdsTemp.Conectar;
    while not cdsTemp.Eof do begin
      Edit;
      FieldByName('DB_VERSION').AsInteger       := cdsTemp.FieldByName('DB_VERSION').AsInteger;
      FieldByName('DB_APLICAVERSION').AsInteger := cdsTemp.FieldByName('DB_APLICAVERSION').AsInteger;
      FieldByName('DB_VERSIONESPATCH').AsString := cdsTemp.FieldByName('DB_VERSIONESPATCH').AsString;
      if FieldByName('DB_APLICAPATCH').AsBoolean then
        FieldByName('DB_APLICAPATCH').AsBoolean := (cdsTemp.FieldByName('DB_VERSION').AsInteger < cdsTemp.FieldByName('DB_APLICAVERSION').AsInteger);
      Post;
      Next;
      cdsTemp.Next
    end;
  finally
    FreeAndNil(cdsTemp);
    Bookmark := bm;
    EnableControls;
  end;
end;

procedure TSistActualizarDBs_DevEx.DBGridMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  pt: TGridcoord;
begin
  if Processing then
    Exit;
    
  with DBGrid do begin
    pt := MouseCoord(X, Y) ;
    if (pt.X > 0) and (pt.Y > 0) and (Columns[pt.X - 1].FieldName = 'DB_PROCESO') then
      Cursor := crHandPoint
    else
      Cursor := crDefault;
  end;
end;

procedure TSistActualizarDBs_DevEx.ThreadActualizaFinaliza(Sender: TObject);
var
  I: Integer;
begin
  I := fAvances.IndexOf(Sender);
  if I <> -1 then
    fAvances.Delete(I);
  if fAvances.Count = 0 then
  begin
       HabilitaControles(True);
       actActualizarTabla.Execute;
  end;
end;

function TSistActualizarDBs_DevEx.Processing: Boolean;
begin
  Result := (fAvances.Count > 0)
end;

end.
