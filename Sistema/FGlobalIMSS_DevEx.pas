unit FGlobalIMSS_DevEx;

{$define SUA_14}                                                                                                                          

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZetaFecha,
     ZetaNumero,
     ZetaKeyCombo, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ZetaKeyLookup_DevEx, ImgList,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxPC,
  dxBarBuiltInMenu;

type
  TGlobalIMSS_DevEx = class(TBaseGlobal_DevEx)
    OpenDialog: TOpenDialog;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label14: TLabel;
    FormulaPercepciones: TEdit;
    FormulaDias: TEdit;
    EnlaceSUA: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    LblPrimeraExportacion: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    BuscarArchivoEmpleados: TcxButton;
    BuscarArchivoMovimientos: TcxButton;
    BuscarDirectorioDatos: TcxButton;
    BuscarArchivoDatosAfiliatorios: TcxButton;
    BuscarArchivoInfonavit: TcxButton;
    BuscarArchivoIncapacidad: TcxButton;
    ArchivoEmpleados: TEdit;
    ArchivoMovimientos: TEdit;
    DirectorioDatos: TEdit;
    BorrarArchivos: TCheckBox;
    PrimeraExportacion: TZetaFecha;
    ArchivoDatosAfiliatorios: TEdit;
    ArchivoInfonavit: TEdit;
    ArchivoIncapacidad: TEdit;
    IncluirFI: TCheckBox;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    TopeAmortizacion: TZetaNumero;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    LblConceptoAmortizacion: TLabel;
    InicioAmortizacion: TZetaKeyCombo;
    AcumuladoAmortizacion: TCheckBox;
    ConceptoAmortizacion: TZetaKeyLookup_DevEx;
    GroupBox5: TGroupBox;
    LblConceptoInfonavit: TLabel;
    lblProvVaca: TLabel;
    ConceptosInf: TEdit;
    cxButton1: TcxButton;
    ProvisionVacaciones: TZetaKeyLookup_DevEx;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    cbTipoDescConciliaInfonavit: TZetaKeyCombo;
    CalculoAportaciones: TGroupBox;
    Label3: TLabel;
    ConceptoRetiro: TZetaKeyLookup_DevEx;
    Ajuste: TGroupBox;
    lblTPrestaAjus: TLabel;
    TComparaInfonavit: TRadioGroup;
    TPrestaInfonavit: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    CreditoUMA: TZetaNumero;
    rgTipoTopeAmortizacion: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuscarArchivoMovimientosClick(Sender: TObject);
    procedure BuscarArchivoEmpleadosClick(Sender: TObject);
    procedure BuscarDirectorioDatosClick(Sender: TObject);
    procedure BuscarArchivoDatosAfiliatoriosClick(Sender: TObject);
    procedure BuscarArchivoInfonavitClick(Sender: TObject);
    procedure BuscarArchivoIncapacidadClick(Sender: TObject);
    procedure AcumuladoAmortizacionClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure rgTipoTopeAmortizacionClick(Sender: TObject);
    procedure SetEditarSoloActivos;
  private
    { Private declarations }
    function BuscarArchivo( const DlgTitle, Value: String ): String;
    procedure HabilitaConceptoAcumulado( const lEnabled: Boolean );
    // DES US #13392 Global para indicar tipo de tope de amortización INFONAVIT
    procedure setMascaraTopeAmortizacion;
  public
    { Public declarations }
  end;

var
  GlobalIMSS_DevEx: TGlobalIMSS_DevEx;
Const
  FiltroBusca = 'CO_NUMERO < 1000';

implementation

uses DCatalogos,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DTablas;

{$R *.DFM}

procedure TGlobalIMSS_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos           := ZAccesosTress.D_CAT_CONFI_IMSS_INFO;
     with ConceptoRetiro do
     begin
          LookupDataset      := dmCatalogos.cdsConceptos;
          Tag                := K_GLOBAL_ADICIONAL_RETIRO;
     end;
     TopeAmortizacion.Tag    := K_GLOBAL_TOPE_INFONAVIT;
     FormulaPercepciones.Tag := K_GLOBAL_FORM_PERCEP;
     FormulaDias.Tag         := K_GLOBAL_FORM_DIAS;
     ArchivoEmpleados.Tag    := K_GLOBAL_FILE_EMP_SUA;
     ArchivoDatosAfiliatorios.Tag  := K_GLOBAL_FILE_AFIL_SUA;
     ArchivoInfonavit.Tag          := K_GLOBAL_FILE_INFO_SUA;
     ArchivoMovimientos.Tag        := K_GLOBAL_FILE_MOVS_SUA;
     ArchivoIncapacidad.Tag        := K_GLOBAL_FILE_INCA_SUA;
     DirectorioDatos.Tag     := K_GLOBAL_DIR_DATOS_SUA;
     PrimeraExportacion.Tag  := K_GLOBAL_FEC_1_EXP_SUA;
     BorrarArchivos.Tag      := K_GLOBAL_DELETE_FILE_SUA;
     InicioAmortizacion.Tag  := K_GLOBAL_INICIO_AMORTIZACION;
     AcumuladoAmortizacion.Tag  := K_GLOBAL_USAR_ACUM_AMORTIZACION;
     IncluirFI.Tag              := K_GLOBAL_INCLUYE_FI_SUA;
     cbTipoDescConciliaInfonavit.Tag := K_GLOBAL_INFO_DESCUENTO_INFONAVIT;
     CreditoUMA.Tag          := K_GLOBAL_CREDITO_UMA;
     with ConceptoAmortizacion do
     begin
          LookupDataset      := dmCatalogos.cdsConceptos;
          Tag                := K_GLOBAL_CONCEPTO_AMORTIZACION;
     end;
     ConceptosInf.Tag := K_GLOBAL_CONCEPTOS_INFONAVIT;

     TComparaInfonavit.Tag := K_GLOBAL_AJUSINFO_TCOMPARA;

     // US #13392 Global para indicar tipo de tope de amortización INFONAVIT
     rgTipoTopeAmortizacion.Tag := K_GLOBAL_TIPO_TOPE_AMORTIZACION_INFONAVIT;

     with TPrestaInfonavit do
     begin
          LookupDataset      := dmTablas.cdsTPresta;
          Tag                := K_GLOBAL_AJUSINFO_TPRESTA;
     end;

     with ProvisionVacaciones do
     begin
          LookupDataSet := dmCatalogos.cdsConceptos;
          Tag := K_GLOBAL_PROVISION_INFONAVIT;
     end;




     HelpContext             := H65107_IMSS_INFONAVIT;

{$ifndef SUA_14}
     AcumuladoAmortizacion.Visible := FALSE;
     ConceptoAmortizacion.Visible := FALSE;
{$endif}

     { ER: Versión 2.8: Estos controles ya no se requieren, este global no se utiliza }
     LblPrimeraExportacion.Visible := FALSE;
     PrimeraExportacion.Visible := FALSE;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TGlobalIMSS_DevEx.FormShow(Sender: TObject);
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmTablas.cdsTPresta.Conectar;
     inherited;
     with InicioAmortizacion do
     begin
          if ItemIndex < 0 then
             ItemIndex := 0;
     end;
     HabilitaConceptoAcumulado( AcumuladoAmortizacion.Checked );

     // DES US #13392 Global para indicar tipo de tope de amortización INFONAVIT
     setMascaraTopeAmortizacion;
end;

function TGlobalIMSS_DevEx.BuscarArchivo( const DlgTitle, Value: String ): String;
begin
     with OpenDialog do
     begin
          Title := DlgTitle;
          FileName := Value;
          if Execute then
             Result := FileName
          else
              Result := Value;
     end;
end;

procedure TGlobalIMSS_DevEx.BuscarArchivoEmpleadosClick(Sender: TObject);
begin
     inherited;
     with ArchivoEmpleados do
     begin
          Text := BuscarArchivo( BuscarArchivoEmpleados.Hint, Text );
     end;
end;

procedure TGlobalIMSS_DevEx.BuscarArchivoMovimientosClick(Sender: TObject);
begin
     inherited;
     with ArchivoMovimientos do
     begin
          Text := BuscarArchivo( BuscarArchivoMovimientos.Hint, Text );
     end;
end;

procedure TGlobalIMSS_DevEx.BuscarDirectorioDatosClick(Sender: TObject);
begin
     inherited;
     DirectorioDatos.Text := BuscarDirectorio( DirectorioDatos.Text );
end;

procedure TGlobalIMSS_DevEx.BuscarArchivoDatosAfiliatoriosClick(Sender: TObject);
begin
     inherited;
     with ArchivoDatosAfiliatorios do
     begin
          Text := BuscarArchivo( BuscarArchivoDatosAfiliatorios.Hint, Text );
     end;
end;

procedure TGlobalIMSS_DevEx.BuscarArchivoInfonavitClick(Sender: TObject);
begin
     inherited;
     with ArchivoInfonavit do
     begin
          Text := BuscarArchivo( BuscarArchivoInfonavit.Hint, Text );
     end;
end;

procedure TGlobalIMSS_DevEx.BuscarArchivoIncapacidadClick(Sender: TObject);
begin
     inherited;
     with ArchivoIncapacidad do
     begin
          Text := BuscarArchivo( BuscarArchivoIncapacidad.Hint, Text );
     end;
end;

procedure TGlobalIMSS_DevEx.AcumuladoAmortizacionClick(Sender: TObject);
begin
     inherited;
     HabilitaConceptoAcumulado( AcumuladoAmortizacion.Checked );
end;

procedure TGlobalIMSS_DevEx.HabilitaConceptoAcumulado( const lEnabled: Boolean );
begin
     LblConceptoAmortizacion.Enabled := lEnabled;
     ConceptoAmortizacion.Enabled := lEnabled;
end;

procedure TGlobalIMSS_DevEx.OKClick(Sender: TObject);
begin
     if ConceptoAmortizacion.Enabled and StrVacio( ConceptoAmortizacion.Llave ) then
     begin
          ZetaDialogo.zError( self.Caption, 'Falta especificar el Concepto de Amortización', 0 );
          ActiveControl := ConceptoAmortizacion;
     end
     else
     begin
          if StrVacio( ConceptosInf.Text ) or not ValidaListaNumeros( ConceptosInf.Text ) then
          begin
                ZetaDialogo.zError( self.Caption, 'La Lista de Acumulados no es válida en los Conceptos de Infonavit', 0 );
          end
          else
              inherited;
     end;
end;

procedure TGlobalIMSS_DevEx.OK_DevExClick(Sender: TObject);
begin
  if CreditoUMA.GetTextLen > 1 then
  begin
      ZetaDialogo.zError( self.Caption, 'El valor del tipo de crédito UMA tiene que ser de un solo dígito.', 0 )
  end
  else
      inherited;
end;

// DES US #13392 Global para indicar tipo de tope de amortización INFONAVIT
procedure TGlobalIMSS_DevEx.rgTipoTopeAmortizacionClick(Sender: TObject);
begin
     setMascaraTopeAmortizacion;
end;

procedure TGlobalIMSS_DevEx.cxButton1Click(Sender: TObject);
var SkNum,skey : string;
begin
  inherited;
  sKey := dmcatalogos.cdsConceptos.FieldByName( 'CO_NUMERO' ).AsString;
if(dmCatalogos.cdsConceptos.search_Devex(FiltroBusca,skey,SkNUm)) then
ConceptosInf.Text:=  ConcatString( ConceptosInf.Text, skey, ',' );
end;

// DES US #13392 Global para indicar tipo de tope de amortización INFONAVIT
procedure TGlobalIMSS_DevEx.setMascaraTopeAmortizacion;
begin
     if rgTipoTopeAmortizacion.ItemIndex = 0 then
        TopeAmortizacion.Mascara := mnVecesSMGDF
     else
         TopeAmortizacion.Mascara := mnVecesUMA;
end;

procedure TGlobalIMSS_DevEx.SetEditarSoloActivos;
begin
     ConceptoAmortizacion.EditarSoloActivos := TRUE;
     ProvisionVacaciones.EditarSoloActivos := TRUE;
     TPrestaInfonavit.EditarSoloActivos := TRUE;
     ConceptoRetiro.EditarSoloActivos := TRUE;
end;

end.
