unit FSistlstDispositivos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, 
{$ifdef TRESS_DELPHIXE5_UP}Actions,{$endif} ZBaseEdicion_DevEx;

type
  TSistLstDispositivos_DevEx = class(TBaseGridLectura_DevEx)
    DI_TIPO: TcxGridDBColumn;
    DI_NOMBRE: TcxGridDBColumn;
    DI_DESCRIP: TcxGridDBColumn;
    DI_IP: TcxGridDBColumn;
    DI_STATUS: TcxGridDBColumn;
    Panel1: TPanel;
    ConfiguracionCafeteria: TcxButton;
    mConfigurar: TMenuItem;
    _ConfigurarCafe: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure ConfiguracionCafeteriaClick(Sender: TObject);
  private
    { Private declarations }
    procedure ConfigAgrupamiento;
  protected
    { Protected declarations}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistLstDispositivos_DevEx: TSistLstDispositivos_DevEx;

implementation
uses
    DSistema,
    ZetaCommonClasses,
    ZAccesosMgr,
    ZAccesosTress,
    FConfiguraTress,
    ZetaDialogo;
{$R *.dfm}

{ TLstDispositivos }

procedure TSistLstDispositivos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_LISTA_DISPOSIT;
end;

procedure TSistLstDispositivos_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsSistLstDispositivos.Conectar;
          DataSource.DataSet := cdsSistLstDispositivos;
     end;
end;

procedure TSistLstDispositivos_DevEx.Refresh;
begin
     inherited;
     dmSistema.cdsSistLstDispositivos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TSistLstDispositivos_DevEx.ZetaDBGridDBTableViewFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
const
     K_TIPO_CAFE = 1;
begin
     inherited;
     ConfiguracionCafeteria.Enabled := False;
     mConfigurar.Enabled := False;
     if AFocusedRecord <> nil then
     begin
        if AFocusedRecord.Values[DI_TIPO.Index] = K_TIPO_CAFE then
        begin
             ConfiguracionCafeteria.Enabled := True;
             mConfigurar.Enabled := True;
        end;
     end;
end;

procedure TSistLstDispositivos_DevEx.Agregar;
begin
     inherited;
     dmSistema.cdsSistLstDispositivos.Agregar;
end;

procedure TSistLstDispositivos_DevEx.Modificar;
begin
     inherited;
     dmSistema.cdsSistLstDispositivos.Modificar;
end;

procedure TSistLstDispositivos_DevEx.Borrar;
begin
     inherited;
      dmSistema.cdsSistLstDispositivos.Borrar;
      ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TSistLstDispositivos_DevEx.ConfigAgrupamiento;
begin
     //Cambio de configuracion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE; //Desactiva la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Esconde la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     DI_NOMBRE.Options.Grouping:= FALSE;
     DI_DESCRIP.Options.Grouping:= FALSE;
     DI_IP.Options.Grouping:= FALSE;
end;

procedure TSistLstDispositivos_DevEx.ConfiguracionCafeteriaClick( Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          CafIdentificador := cdsSistLstDispositivos.FieldByName('DI_NOMBRE').AsString;
     end;
     if CheckDerecho( D_SIST_LST_DISPOSITIVOS, K_DERECHO_SIST_KARDEX ) then
        ZBaseEdicion_DevEx.ShowFormaEdicion( FormaConfigura, TFormaConfigura)
     else
         ZetaDialogo.zInformation( 'Lista de Dispositivos','No tiene derechos para modificar '+CR_LF+ 'configuraciones de cafeter�a.',0 );
end;

procedure TSistLstDispositivos_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;
  ConfigAgrupamiento;
end;

end.
