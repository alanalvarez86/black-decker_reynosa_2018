unit FSistBaseDatos_DevEx;

interface
{$define VALIDAEMPLEADOSGLOBAL}

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ExtCtrls, StdCtrls, 
  ZetaSmartLists, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxTextEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxMemo;

type
  TSistBaseDatos_DevEx = class(TBaseGridLectura_DevEx)
    pnlGrid: TPanel;
    pnlImportarExportar: TPanel;
    btImportar: TcxButton;
    btExportar: TcxButton;
    btImportarCSV: TcxButton;
    DB_CODIGO: TcxGridDBColumn;
    DB_DESCRIP: TcxGridDBColumn;
    DB_DATOS: TcxGridDBColumn;
    DB_TIPO: TcxGridDBColumn;
    DB_VERSION: TcxGridDBColumn;
    DB_EMPLEADOS: TcxGridDBColumn;
    DB_SIZE: TcxGridDBColumn;
    DB_ESPC: TcxGridDBColumn;
    DB_OK: TcxGridDBColumn;
    DB_ERROR: TcxGridDBColumn;
    memLicenciaDeUso: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure btImportarClick(Sender: TObject);
    procedure btImportarCSVClick(Sender: TObject);
    procedure btExportarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    {$IFDEF VALIDAEMPLEADOSGLOBAL}
    procedure LicenciaDeUso (EmpleadosConteo: Integer);
    {$ENDIF}
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;    
  public
    { Public declarations }      
  end;

var
  SistBaseDatos_DevEx: TSistBaseDatos_DevEx;

implementation

uses DSistema, ZetaCommonClasses, ZAccesosTress, DProcesos,
     ZetaCommonLists, ZAccesosMgr,
     ZetaCommonTools, ZetaLicenseClasses,FAutoClasses, FAutoServer;

{$R *.dfm}

const
     K_EXPORTAR_TABLAS_XML = ZetaCommonClasses.K_DERECHO_SIST_KARDEX;
     K_IMPORTAR_TABLAS_XML = ZetaCommonClasses.K_DERECHO_BANCA;
     K_IMPORTAR_TABLAS_CSV = ZetaCommonClasses.K_DERECHO_NIVEL0;

procedure TSistBaseDatos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_SIST_BASE_DATOS;
     IndexDerechos := D_SIST_BASE_DATOS;
end;

procedure TSistBaseDatos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     btExportar.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_BASE_DATOS, K_EXPORTAR_TABLAS_XML );
     btImportar.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_BASE_DATOS, K_IMPORTAR_TABLAS_XML );
     btImportarCSV.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_BASE_DATOS, K_IMPORTAR_TABLAS_CSV );

     {$IFDEF VALIDAEMPLEADOSGLOBAL}
     LicenciaDeUso (dmSistema.GetEmpleadosConteo);
     {$ENDIF}
end;

{$IFDEF VALIDAEMPLEADOSGLOBAL}
procedure TSistBaseDatos_DevEx.LicenciaDeUso (EmpleadosConteo: Integer);
var
   sMensajeAdvertencia, sMensajeError : string;
   oGlobalLicenseValues: TGlobalLicenseValues;
   oAutoServer: TAutoServer;
begin

     // US #11012 Opcion para obtener status de licencia de uso.
     pnlImportarExportar.Height := 34;
     memLicenciaDeUso.Text := VACIO;
     memLicenciaDeUso.Style.TextColor  := clWindowText;

     oAutoServer := TAutoServer.Create;
     oGlobalLicenseValues := TGlobalLicenseValues.Create(oAutoServer);
     try
        oGlobalLicenseValues.AutoServer.IniciaValidacion := Autorizacion.IniciaValidacion;
        oGlobalLicenseValues.AutoServer.Empleados := Autorizacion.Empleados;
        oGlobalLicenseValues.TotalGlobal := EmpleadosConteo;

        oGlobalLicenseValues.EvaluarEmpleadosMensajePantallaBD(sMensajeAdvertencia, sMensajeError);

        if StrLleno(sMensajeError) then
        begin
            memLicenciaDeUso.Text := sMensajeError;
            memLicenciaDeUso.Style.TextColor := clRed;
        end
        else if StrLleno(sMensajeAdvertencia) then
        begin
            memLicenciaDeUso.Text := sMensajeAdvertencia;
            memLicenciaDeUso.Style.TextColor  := clWindowText;
        end;

        memLicenciaDeUso.Visible := memLicenciaDeUso.Text <> VACIO;

        if memLicenciaDeUso.Visible then
            pnlImportarExportar.Height := 120;
     finally
            FreeAndNil (oGlobalLicenseValues);
            FreeAndNil (oAutoServer);
     end;
end;
{$ENDIF}

procedure TSistBaseDatos_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsSistBaseDatos.Conectar;
          cdsBasesDatos.Conectar;
          DataSource.DataSet := cdsSistBaseDatos;
     end;
end;

procedure TSistBaseDatos_DevEx.Refresh;
begin
     dmSistema.cdsSistBaseDatos.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
     {$IFDEF VALIDAEMPLEADOSGLOBAL}
     LicenciaDeUso (dmSistema.GetEmpleadosConteo);
     {$ENDIF}
end;

procedure TSistBaseDatos_DevEx.Agregar;
begin
     dmSistema.cdsSistBaseDatos.Agregar;
     {$IFDEF VALIDAEMPLEADOSGLOBAL}
     LicenciaDeUso (dmSistema.GetEmpleadosConteo);
     {$ENDIF}
end;

procedure TSistBaseDatos_DevEx.Borrar;
begin
     dmSistema.cdsSistBaseDatos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
     {$IFDEF VALIDAEMPLEADOSGLOBAL}
     LicenciaDeUso (dmSistema.GetEmpleadosConteo);
     {$ENDIF}
end;

procedure TSistBaseDatos_DevEx.Modificar;
begin
     dmSistema.cdsSistBaseDatos.Modificar;
     {$IFDEF VALIDAEMPLEADOSGLOBAL}
     LicenciaDeUso (dmSistema.GetEmpleadosConteo);
     {$ENDIF}
end;

procedure TSistBaseDatos_DevEx.btImportarClick(Sender: TObject);
begin
     ShowWizard( prSISTImportarTablas );
end;

procedure TSistBaseDatos_DevEx.btExportarClick(Sender: TObject);
begin
     dmSistema.InvocarExportarTablasXML
end;

procedure TSistBaseDatos_DevEx.btImportarCSVClick(Sender: TObject);
begin
     ShowWizard( prSISTImportarTablasCSV );
end;

end.
