unit FSistBitacoraSistema_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FBaseSistBitacora_DevEx, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  StdCtrls, Mask, ZetaFecha, Buttons, ZetaKeyCombo, ExtCtrls,
  ZetaCommonClasses, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, ActnList, ImgList, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, ZetaKeyLookup_DevEx;

type
  TSistBitacoraSistema_DevEx = class(TBaseSistBitacora_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    procedure Connect; override;
    procedure Modificar; override;
    procedure RefrescaDatos; override;
    procedure LlenaClaseBit; override;
  end;

var
  SistBitacoraSistema_DevEx: TSistBitacoraSistema_DevEx;

implementation

{$R *.dfm}

uses DSistema,
     DCliente,
     ZetaCommonLists,
     ZetaCommonTools;


procedure TSistBitacoraSistema_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_BITACORA_SISTEMA;
end;

procedure TSistBitacoraSistema_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          DataSource.DataSet := cdsBitacora;
     end;
end;

procedure TSistBitacoraSistema_DevEx.LlenaClaseBit;
var
   i : Integer;
begin
   with ClaseBit do
     begin
          Sorted := FALSE;
          for i:= 0 to Ord(High(eClaseSistBitacora)) do
              Items.AddObject( ObtieneElemento(lfClaseSistBitacora,i), TObject( i ) );
          ItemIndex := 0;
          Sorted := TRUE;
     end;
end;


procedure TSistBitacoraSistema_DevEx.Modificar;
begin
     with dmSistema do
     begin
          cdsBitacora.Modificar;
     end;
end;

procedure TSistBitacoraSistema_DevEx.RefrescaDatos;
begin
     dmSistema.RefrescarBitacora( FParametros );
end;


end.
