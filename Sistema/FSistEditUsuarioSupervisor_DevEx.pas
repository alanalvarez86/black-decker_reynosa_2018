unit FSistEditUsuarioSupervisor_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZetaSmartLists, ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters, ImgList,
  cxButtons, cxControls, cxContainer, cxEdit, cxCheckListBox;

type
  TFSistEditUserSuper_DevEx = class(TZetaDlgModal_DevEx)
    Niveles: TcxCheckListBox;
    Prender: TcxButton;
    Apagar: TcxButton;
    PanelBusqueda: TPanel;
    EmpleadoBuscaBtn: TcxButton;
    EditBusca: TEdit;
    Label1: TLabel;
    MostrarActivos: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure NivelesClickCheck2(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure MostrarActivosClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure NivelesClickCheck(Sender: TObject; AIndex: Integer;
      APrevState, ANewState: TcxCheckBoxState);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroSuper: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  FSistEditUserSuper_DevEx: TFSistEditUserSuper_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema;

{$R *.DFM}

procedure TFSistEditUserSuper_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TFSistEditUserSuper_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     OK_DevEx.Enabled := False;
     ActiveControl := Niveles;
     with MostrarActivos do
     begin
          Checked:= True;
          Enabled:= True;
     end;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
     EditBusca.Text := VACIO;
end;

procedure TFSistEditUserSuper_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TFSistEditUserSuper_DevEx.Cargar;
var
   i: Integer;
begin
     Caption := dmSistema.CargaListaSupervisores( FLista, MostrarActivos.Checked );
     with Niveles do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       //Items.Add( Values[ Names[ i ] ] );
                       with Items.Add do
                       begin
                          text := FLista[i] ;
                          Checked := ( Integer( Objects[ i ] ) > 0 );
                       end;
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TFSistEditUserSuper_DevEx.Descargar;
var
   i: Integer;
begin
     with Niveles do
     begin
     for i := 0 to ( Count - 1 ) do
     begin
          with Items[i] do
                    begin
                    if Checked then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaSupervisores( FLista );
end;

procedure TFSistEditUserSuper_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Niveles do
     begin
        try
          Niveles.Items.BeginUpdate;
          for i := 0 to ( Count - 1 ) do
          with Items[i] do
          begin

                      Checked := lState;
          end;
        finally
          Niveles.Items.EndUpdate;
        end;

     end;
     NivelesClickCheck2( Niveles );
end;

procedure TFSistEditUserSuper_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TFSistEditUserSuper_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TFSistEditUserSuper_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUserSuper_DevEx.NivelesClickCheck2(Sender: TObject);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
     MostrarActivos.Enabled:=False;
end;

procedure TFSistEditUserSuper_DevEx.EmpleadoBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Niveles.Count )then
     begin
          lEncontro:= EncontroSuper;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Niveles.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de Supervisores', '! No hay otro Supervisor con esos Datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de Supervisores', '! No hay un Supervisor con esos Datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

function TFSistEditUserSuper_DevEx.EncontroSuper: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Niveles do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].Text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TFSistEditUserSuper_DevEx.MostrarActivosClick(Sender: TObject);
begin
     Cargar;
end;

procedure TFSistEditUserSuper_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUserSuper_DevEx.NivelesClickCheck(Sender: TObject;
  AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
     MostrarActivos.Enabled:=False;

end;

end.
