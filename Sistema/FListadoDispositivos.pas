unit FListadoDispositivos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, CheckLst, Buttons, ZetaSmartLists,
  ExtCtrls;

type
  TListadoDispositivos = class(TZetaDlgModal)
    Prender: TBitBtn;
    Apagar: TBitBtn;
    Terminales: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
  public
    { Public declarations }
  end;

var
  ListadoDispositivos: TListadoDispositivos;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema;

{$R *.dfm}

procedure TListadoDispositivos.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     ActiveControl := Terminales;
end;

procedure TListadoDispositivos.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TListadoDispositivos.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TListadoDispositivos.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TListadoDispositivos.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TListadoDispositivos.OKClick(Sender: TObject);
{$ifndef DOS_CAPAS}
var
   i: integer;
   sTerminales, sSinTerminales: string;
{$endif}
begin
     inherited;
     {$ifndef DOS_CAPAS}
     // Guardar
     with dmSistema do
     begin
          for i := 0 to ( Terminales.Items.Count - 1 ) do
          begin
               if Terminales.Checked[ i ] then
                  sTerminales := ConcatString( sTerminales, Copy( Terminales.Items[ i ], 0, Pos( '=', Terminales.Items[ i ] ) - 1 ), ',' )
               else
                   sSinTerminales := ConcatString( sSinTerminales, Copy( Terminales.Items[ i ], 0, Pos( '=', Terminales.Items[ i ] ) - 1 ), ',' )
          end;
          GrabaListaTerminales( cdsListaGrupos.FieldByName( 'GP_CODIGO' ).AsString, sTerminales, sSinTerminales );
          cdsTermPorGrupo.Refrescar;
     end;
     {$endif}
end;

procedure TListadoDispositivos.Cargar;
{$ifndef DOS_CAPAS}
var
   i: integer;
{$endif}
begin
{$ifndef DOS_CAPAS}
     with dmSistema do
     begin
          CargaListaTerminales( Terminales.Items );

          with Terminales do
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    if DentroListaTerminales( cdsListaGrupos.FieldByName( 'GP_CODIGO' ).AsString, Items[ i ] )then
                       Checked[ i ] := True
                    else
                        Checked[ i ] := False;
               end;
          end;
     end;
{$endif}
end;

procedure TListadoDispositivos.PrendeApaga(const lState: Boolean);
var
   i: Integer;
begin
     with Terminales do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

end.
