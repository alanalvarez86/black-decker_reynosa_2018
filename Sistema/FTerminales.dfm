inherited Terminales: TTerminales
  Caption = 'Terminales'
  ClientWidth = 824
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 824
    inherited ValorActivo2: TPanel
      Width = 565
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 60
    Width = 824
    Height = 213
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TE_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_NOMBRE'
        Title.Caption = 'Descripci'#243'n'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_APP'
        Title.Caption = 'Aplicaci'#243'n'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_BEAT'
        Title.Caption = #218'ltimo latido'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_TER_HOR'
        Title.Caption = 'Fecha en la terminal'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_VERSION'
        Title.Caption = 'Versi'#243'n'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TE_IP'
        Title.Caption = 'Direcci'#243'n IP'
        Width = 150
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 824
    Height = 41
    Align = alTop
    TabOrder = 2
    object btnImportarTerminales: TButton
      Left = 8
      Top = 8
      Width = 210
      Height = 25
      Caption = 'Importar terminales'
      TabOrder = 0
      OnClick = btnImportarTerminalesClick
    end
    object btnReiniciaTerminales: TButton
      Left = 222
      Top = 8
      Width = 210
      Height = 25
      Caption = 'Sincronizar huellas de todas las terminales'
      Enabled = False
      TabOrder = 1
      OnClick = btnReiniciaTerminalesClick
    end
  end
end
