unit FBitacoraRegBio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, DB, ExtCtrls,
  ZetaKeyLookup, StdCtrls, Buttons, Mask, ZetaFecha, ZetaCommonClasses,
  ZetaKeyCombo;

type
  TBitacoraRegBio = class(TBaseConsulta)
    Panel1: TPanel;
    ZetaDBGrid1: TZetaDBGrid;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    luEmpresa: TZetaKeyLookup;
    BtnFiltrar: TBitBtn;
    Label2: TLabel;
    zTerminales: TZetaKeyLookup;
    edtMensaje: TEdit;
    Label5: TLabel;
    Label3: TLabel;
    dInicio: TZetaFecha;
    Label4: TLabel;
    dFin: TZetaFecha;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
  protected
    { Protected declarations }
    procedure Connect; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  BitacoraRegBio: TBitacoraRegBio;

implementation

uses
    dSistema,
    ZetaDialogo, DBaseSistema;

{$R *.dfm}

{ TBitacoraRegBio }

procedure TBitacoraRegBio.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsEmpresasLookUp.Conectar;
          cdsTerminales.Conectar;
          luEmpresa.LookupDataset := cdsEmpresasLookUp;
          zTerminales.LookUpDataSet := cdsTerminales;
     {$ifndef DOS_CAPAS}
          with FParametros do
          begin
               AddDate( 'FechaInicio', dInicio.Valor + 1 );
               AddDate( 'FechaFin', dFin.Valor + 2 );
               AddString( 'Empresa', luEmpresa.Llave );
               AddString( 'Terminal', zTerminales.Llave );
               AddString( 'Mensaje', edtMensaje.Text );
          end;
          dmSistema.CargaBitacora( FParametros );
     {$endif}
          DataSource.DataSet := cdsBitacoraBio;
     end;
end;

procedure TBitacoraRegBio.Refresh;
begin
     inherited;
     {$ifndef DOS_CAPAS}
     with FParametros do
     begin
          AddDate( 'FechaInicio', dInicio.Valor );
          AddDate( 'FechaFin', dFin.Valor );
          AddString( 'Empresa', luEmpresa.Llave );
          AddString( 'Terminal', zTerminales.Llave );
          AddString( 'Mensaje', edtMensaje.Text );
     end;
     dmSistema.CargaBitacora( FParametros );
     {$endif}
end;

procedure TBitacoraRegBio.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_BITACORATERMINALESGTI;
     FParametros := TZetaParams.Create;

 

end;

procedure TBitacoraRegBio.FormDestroy(Sender: TObject);
begin
     FParametros.Free;
     dmSistema.cdsBitacoraBio.EmptyDataSet;
     inherited;
end;

procedure TBitacoraRegBio.BtnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TBitacoraRegBio.FormShow(Sender: TObject);
begin
     inherited;
     dInicio.Valor := Now;
     dFin.Valor := Now;
end;

function TBitacoraRegBio.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'Esta acci�n no es permitida en la bit�cora';
end;

function TBitacoraRegBio.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'Esta acci�n no es permitida en la bit�cora';
end;

function TBitacoraRegBio.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := False;
     sMensaje := 'Esta acci�n no es permitida en la bit�cora';
end;

end.
