unit FEditUbicaBaseDatos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, Mask, DBCtrls, Buttons, StdCtrls, ExtCtrls, ZetaEdit,
  ZetaRegistryCliente, Registry, DB,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TCargaDocumento = function ( const sPath, sObservaciones : string; const lEncimarArchivo : Boolean ): Boolean of object;
  TEditUbicaBaseDatos_DevEx = class(TZetaDlgModal_DevEx)
    lblServidor: TLabel;
    edServidor: TEdit;
    Label5: TLabel;
    cbBasesDatos: TComboBox;
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbBasesDatosChange(Sender: TObject);
    procedure edServidorExit(Sender: TObject);

  private
    function GetPosicionBD (sBaseDatos: String): Integer;
    { Private declarations }
  public
    { Public declarations }
  end;

function EditarUbicacion (sServidor, sBaseDatos: String; const iHelpCtx: LongInt): Boolean;

var
  EditUbicaBaseDatos_DevEx: TEditUbicaBaseDatos_DevEx;

implementation

uses ZetaCommonClasses, dSistema, ZetaDialogo, ZetaServerTools, ZetaWinAPITools;

{$R *.DFM}

function EditarUbicacion (sServidor, sBaseDatos: String; const iHelpCtx: LongInt): Boolean;
begin
     with TEditUbicaBaseDatos_DevEx.Create( Application ) do
     begin
          try
             Result:= True; 
             HelpContext:= iHelpCtx;
             ShowModal;
          finally
            Free;
          end;
     end;
end;

procedure TEditUbicaBaseDatos_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     if Trim (cbBasesDatos.Text) <> '' then
     begin
          if not (dmSistema.cdsSistBaseDatos.state in [dsInsert, dsEdit ]) then
             dmSistema.cdsSistBaseDatos.Edit;

          dmSistema.cdsSistBaseDatos.FieldByName ('DB_DATOS').AsString := edServidor.Text + '.' + cbBasesDatos.Text;
          Close;
     end
     else
     begin
          ZetaDialogo.ZWarning ('Advertencia', 'Campo Base de Datos no puede quedar vac�o', 0, mbOK);
          cbBasesDatos.SetFocus;
     end;
end;

procedure TEditUbicaBaseDatos_DevEx.FormShow(Sender: TObject);
var    
   oCursor: TCursor;
   sUsuario, sPassword: String;
   indexBD: Integer;
begin
        inherited;
        sUsuario := '';
        sPassword := '';
        
        cbBasesDatos.SetFocus;
        
        with dmSistema.cdsSistBaseDatos do
        begin
             if (State in [dsInsert]) OR (Trim (FieldByName ('DB_DATOS').AsString) = '') then
             begin
                  edServidor.Text :=  dmSistema.GetServer;
             end
             else
             begin
                  edServidor.Text := Copy (FieldByName ('DB_DATOS').AsString, 0, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)-1));
             end;
                        
             oCursor := Screen.Cursor;
             Screen.Cursor := crHourglass;

             cbBasesDatos.Items.Clear;
             try
                if FieldByName ('DB_USRDFLT').AsString = K_GLOBAL_NO then
                begin
                     sUsuario := FieldByName ('DB_USRNAME').AsString;

                     if (State in [dsInsert]) or
                        (FieldByName ('DB_PASSWRD').NewValue <> FieldByName ('DB_PASSWRD').OldValue ) then
                        sPassword := ZetaServerTools.Encrypt( FieldByName ('DB_PASSWRD').AsString )
                     else
                         sPassword := FieldByName ('DB_PASSWRD').AsString;
                end;
                dmSistema.GetBasesDatos (Trim(edServidor.Text), sUsuario, sPassword);
                
             finally
                    Screen.Cursor := oCursor;
             end;

             while not dmSistema.cdsBasesDatos.Eof do
             begin
                  cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
                  dmSistema.cdsBasesDatos.Next;
             end;

             // cbBasesDatos.Text := Copy (FieldByName ('DB_DATOS').AsString, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1), Length (FieldByName ('DB_DATOS').AsString));

             indexBD := GetPosicionBD (Copy (FieldByName ('DB_DATOS').AsString, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1), Length (FieldByName ('DB_DATOS').AsString)));
             if indexBD >= 0 then
                cbBasesDatos.ItemIndex := indexBD
             else
                 DataBaseError (Format ('No se ha encontrado la Base de Datos ''%s'' en el servidor.', [Copy (FieldByName ('DB_DATOS').AsString, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1), Length (FieldByName ('DB_DATOS').AsString))]));
        end;
end;  

procedure TEditUbicaBaseDatos_DevEx.cbBasesDatosChange(Sender: TObject);
begin
      inherited;
      OK_DevEx.Enabled := TRUE;
end;

function TEditUbicaBaseDatos_DevEx.GetPosicionBD (sBaseDatos: String): Integer;
var
   index: Integer;
begin
     // index := 0;
     Result := -1;
     for index:= 0 to cbBasesDatos.Items.Count do
     begin
          if trim (sBaseDatos) = trim (cbBasesDatos.Items[index]) then
             Result := Index;
     end;
end;

procedure TEditUbicaBaseDatos_DevEx.edServidorExit(Sender: TObject);
var    
   oCursor: TCursor;   
   sUsuario, sPassword: String;
begin
     sUsuario := '';
     sPassword := '';
     
     if edServidor.Text <> VACIO then
     begin
         oCursor := Screen.Cursor;
         Screen.Cursor := crHourglass;
         
         with dmSistema.cdsSistBaseDatos do
         begin
             cbBasesDatos.Items.Clear;

             try
                if FieldByName ('DB_USRDFLT').AsString = K_GLOBAL_NO then
                begin
                     sUsuario := FieldByName ('DB_USRNAME').AsString;

                     if (State in [dsInsert]) or
                        (FieldByName ('DB_PASSWRD').NewValue <> FieldByName ('DB_PASSWRD').OldValue ) then
                        sPassword := ZetaServerTools.Encrypt( FieldByName ('DB_PASSWRD').AsString )
                     else
                         sPassword := FieldByName ('DB_PASSWRD').AsString;
                end;
                dmSistema.GetBasesDatos (Trim(edServidor.Text), sUsuario, sPassword);
             finally
                    Screen.Cursor := oCursor;
             end;

             while not dmSistema.cdsBasesDatos.Eof do
             begin
                  cbBasesDatos.AddItem(dmSistema.cdsBasesDatos.FieldByName ('NAME').AsString, nil);
                  dmSistema.cdsBasesDatos.Next;
             end;

             // cbBasesDatos.Text := Copy (FieldByName ('DB_DATOS').AsString, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1), Length (FieldByName ('DB_DATOS').AsString));
             // cbBasesDatos.ItemIndex := GetPosicionBD (Copy (FieldByName ('DB_DATOS').AsString, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1), Length (FieldByName ('DB_DATOS').AsString)));
             cbBasesDatos.ItemIndex := 0;
         end;
     end
     else
     begin
          // ZetaDialogo.zError ('Error En Servidor', 'Campo Servidor No Puede Estar Vac�o', 0);
          cbBasesDatos.Items.Clear;
          ZetaDialogo.ZWarning('Advertencia', 'Campo Servidor No Puede Estar Vac�o', 0, mbOK);
          edServidor.SetFocus;
     end;
end;

end.
