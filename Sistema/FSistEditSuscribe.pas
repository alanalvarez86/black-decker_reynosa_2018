unit FSistEditSuscribe;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, Db, ZetaEdit;

type
  TFormaEditSuscrip = class(TZetaDlgModal)
    Label1: TLabel;
    RE_NOMBRE: TZetaDBEdit;
    dsSuscrip: TDataSource;
    Label2: TLabel;
    SU_FRECUEN: TZetaDBKeyCombo;
    Label3: TLabel;
    SU_ENVIAR: TZetaDBKeyCombo;
    Label4: TLabel;
    SU_VACIO: TZetaDBKeyCombo;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormaEditSuscrip: TFormaEditSuscrip;

implementation

uses DReportes, dSistema;

{$R *.DFM}

procedure TFormaEditSuscrip.FormShow(Sender: TObject);
begin
  inherited;
  ActiveControl := SU_FRECUEN;
end;

procedure TFormaEditSuscrip.OKClick(Sender: TObject);
begin
  inherited;
  with dsSuscrip.DataSet do
    if State <> dsBrowse then
        Post;
end;

procedure TFormaEditSuscrip.CancelarClick(Sender: TObject);
begin
  inherited;
  with dsSuscrip.DataSet do
    if State <> dsBrowse then
        Cancel;
end;

end.
