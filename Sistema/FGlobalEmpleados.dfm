inherited GlobalEmpleados: TGlobalEmpleados
  Left = 430
  Top = 251
  Caption = 'Datos Globales de Empleados'
  ClientHeight = 358
  ClientWidth = 475
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 322
    Width = 475
    inherited OK: TBitBtn
      Left = 306
    end
    inherited Cancelar: TBitBtn
      Left = 386
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 475
    Height = 322
    ActivePage = Contratacion
    Align = alClient
    TabOrder = 1
    object Identificacion: TTabSheet
      Caption = 'Identificaci'#243'n'
      object LCiudad: TLabel
        Left = 99
        Top = 5
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object LEstado: TLabel
        Left = 99
        Top = 28
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
      end
      object LHabita: TLabel
        Left = 95
        Top = 163
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive En:'
      end
      object LViveCon: TLabel
        Left = 89
        Top = 185
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vive Con:'
      end
      object LEdoCivil: TLabel
        Left = 77
        Top = 117
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado Civil:'
      end
      object LTransporte: TLabel
        Left = 81
        Top = 207
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transporte:'
      end
      object LNacio: TLabel
        Left = 70
        Top = 73
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nacionalidad:'
      end
      object Label2: TLabel
        Left = 108
        Top = 95
        Width = 27
        Height = 13
        Caption = 'Sexo:'
      end
      object LEstudios: TLabel
        Left = 45
        Top = 229
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Grado de Estudios:'
      end
      object LTipoCreden: TLabel
        Left = 159
        Top = 140
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'Letra Credencial:'
      end
      object Label3: TLabel
        Left = 87
        Top = 50
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'Municipio:'
      end
      object CB_CIUDAD: TEdit
        Left = 138
        Top = 2
        Width = 145
        Height = 21
        TabOrder = 0
      end
      object CB_NACION: TEdit
        Left = 138
        Top = 69
        Width = 121
        Height = 21
        TabOrder = 3
      end
      object CB_ESTADO: TZetaKeyLookup
        Left = 138
        Top = 24
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        OnValidKey = CB_ESTADOValidKey
        OnValidLookup = CB_ESTADOValidLookup
      end
      object CB_EDO_CIV: TZetaKeyLookup
        Left = 138
        Top = 113
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
      end
      object CB_VIVEEN: TZetaKeyLookup
        Left = 138
        Top = 159
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
      end
      object CB_VIVECON: TZetaKeyLookup
        Left = 138
        Top = 181
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
      end
      object CB_MED_TRA: TZetaKeyLookup
        Left = 138
        Top = 203
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        WidthLlave = 60
      end
      object CB_ESTUDIO: TZetaKeyLookup
        Left = 138
        Top = 225
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        WidthLlave = 60
      end
      object CB_SEXO: TZetaKeyCombo
        Left = 138
        Top = 91
        Width = 121
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object CB_CHECA: TCheckBox
        Left = 65
        Top = 139
        Width = 87
        Height = 16
        Alignment = taLeftJustify
        Caption = 'Checa Tarjeta:'
        TabOrder = 6
      end
      object CB_CREDENC: TEdit
        Left = 242
        Top = 136
        Width = 29
        Height = 21
        TabOrder = 7
      end
      object NumEmpAuto: TCheckBox
        Left = 3
        Top = 248
        Width = 148
        Height = 17
        Alignment = taLeftJustify
        Caption = '# de Empleado Autom'#225'tico:'
        TabOrder = 12
      end
      object CB_MUNICIP: TZetaKeyLookup
        Left = 138
        Top = 46
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        OnValidKey = CB_MUNICIPValidKey
      end
    end
    object Contratacion: TTabSheet
      Caption = 'Contrataci'#243'n'
      object Label12: TLabel
        Left = 53
        Top = 96
        Width = 83
        Height = 13
        Caption = 'Zona Geogr'#225'fica:'
      end
      object Label36: TLabel
        Left = 52
        Top = 184
        Width = 84
        Height = 13
        Caption = 'Registro Patronal:'
      end
      object CB_HORARIOlbl: TLabel
        Left = 105
        Top = 162
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_CLASIFIlbl: TLabel
        Left = 74
        Top = 140
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object CB_PUESTOlbl: TLabel
        Left = 100
        Top = 118
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object CB_CONTRATlbl: TLabel
        Left = 51
        Top = 49
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Contrato: '
      end
      object CB_SALARIOlbl: TLabel
        Left = 71
        Top = 27
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Salario Diario:'
      end
      object Label17: TLabel
        Left = 27
        Top = 206
        Width = 109
        Height = 13
        Caption = 'Tabla de Prestaciones:'
      end
      object Label1: TLabel
        Left = 62
        Top = 228
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Motivo de Baja:'
      end
      object Label16: TLabel
        Left = 67
        Top = 72
        Width = 69
        Height = 13
        Caption = 'R'#233'gimen SAT:'
      end
      object lBanco: TLabel
        Left = 102
        Top = 251
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Banco:'
      end
      object CB_AUTOSAL: TCheckBox
        Left = 31
        Top = 4
        Width = 121
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Salario por Tabulador:'
        TabOrder = 0
      end
      object CB_SALARIO: TZetaNumero
        Left = 139
        Top = 23
        Width = 121
        Height = 21
        Mascara = mnHoras
        TabOrder = 1
        Text = '0.00'
      end
      object CB_CONTRAT: TZetaKeyLookup
        Left = 139
        Top = 45
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
      object CB_PUESTO: TZetaKeyLookup
        Left = 139
        Top = 114
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
      end
      object CB_CLASIFI: TZetaKeyLookup
        Left = 139
        Top = 136
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
      end
      object CB_TURNO: TZetaKeyLookup
        Left = 139
        Top = 158
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
      end
      object CB_PATRON: TZetaKeyLookup
        Left = 139
        Top = 180
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
      end
      object CB_TABLASS: TZetaKeyLookup
        Left = 139
        Top = 202
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
      end
      object CB_ZONA_GE: TMaskEdit
        Left = 139
        Top = 92
        Width = 30
        Height = 21
        TabOrder = 4
      end
      object CB_MOT_BAJ: TZetaKeyLookup
        Left = 139
        Top = 224
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        WidthLlave = 60
      end
      object CB_REGIMEN: TZetaKeyCombo
        Left = 139
        Top = 68
        Width = 300
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 3
        ListaFija = lfTipoRegimenesSAT
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object CB_BANCO: TZetaKeyLookup
        Left = 139
        Top = 247
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        WidthLlave = 60
      end
    end
    object Area: TTabSheet
      Caption = 'Area'
      object CB_NIVEL1lbl: TLabel
        Left = 104
        Top = 12
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 104
        Top = 34
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 104
        Top = 56
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 104
        Top = 78
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 104
        Top = 100
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 104
        Top = 121
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 104
        Top = 143
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 104
        Top = 165
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 104
        Top = 187
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object CB_NIVEL10lbl: TLabel
        Left = 98
        Top = 209
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 98
        Top = 231
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 98
        Top = 253
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
      object CB_NIVEL1: TZetaKeyLookup
        Left = 139
        Top = 8
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL2: TZetaKeyLookup
        Left = 139
        Top = 30
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL3: TZetaKeyLookup
        Left = 139
        Top = 52
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL4: TZetaKeyLookup
        Left = 139
        Top = 74
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL5: TZetaKeyLookup
        Left = 139
        Top = 96
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL6: TZetaKeyLookup
        Left = 139
        Top = 117
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL7: TZetaKeyLookup
        Left = 139
        Top = 139
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL8: TZetaKeyLookup
        Left = 139
        Top = 161
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL9: TZetaKeyLookup
        Left = 139
        Top = 183
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
      end
      object CB_NIVEL10: TZetaKeyLookup
        Left = 139
        Top = 205
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
      end
      object CB_NIVEL11: TZetaKeyLookup
        Left = 139
        Top = 227
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
      end
      object CB_NIVEL12: TZetaKeyLookup
        Left = 139
        Top = 249
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
      end
    end
  end
end
