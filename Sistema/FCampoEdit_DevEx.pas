unit FCampoEdit_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls,
     FCamposMgr,
     Mask, ZetaNumero, ZetaFecha,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ZetaKeyLookup_DevEx, ImgList, cxButtons;

type
  TCampoEdit_DevEx = class(TZetaDlgModal_DevEx)
    LetreroLBL: TLabel;
    edLetrero: TEdit;
    FormulaLBL: TLabel;
    FormulaTexto: TEdit;
    FormulaNumero: TZetaNumero;
    FormulaBool: TCheckBox;
    lblExcede: TLabel;
    FormulaFecha: TZetaFecha;
    FormulaTabla: TZetaKeyLookup_DevEx;
    chObligatorio: TCheckBox;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FCampo: TCampo;
    FExcedeAlto: Boolean;
    procedure SetCampo(const Value: TCampo);
    procedure SetExcedeAlto(const Value: Boolean);
    procedure EscribeCambios;
  public
    { Public declarations }
    property Campo: TCampo read FCampo write SetCampo;
    property ExcedeAlto : Boolean read FExcedeAlto write SetExcedeAlto;
  end;

var
  CampoEdit_DevEx: TCampoEdit_DevEx;

implementation

uses dTablas,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

{$R *.DFM}

{ TCampoEdit }
procedure TCampoEdit_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     edLetrero.MaxLength := K_ANCHO_DESCRIPCION;
end;

procedure TCampoEdit_DevEx.FormShow(Sender: TObject);
begin
     ActiveControl := edLetrero;
     inherited;
end;

procedure TCampoEdit_DevEx.SetCampo(const Value: TCampo);
begin
     FCampo := Value;
     if strLleno( FCampo.Letrero ) then
        edLetrero.Text := FCampo.Letrero
     else
        edLetrero.Text := FCampo.Nombre;  // Sugiere el nombre del campo
     chObligatorio.Checked := FCampo.Obligatorio;  
     with FormulaTexto do
     begin
          Visible := ( FCampo.Tipo = xctTexto );
          if Visible then
          begin
               Text := FCampo.ValorDefault;
               FormulaLBL.FocusControl := FormulaTexto;
          end;
     end;

     with FormulaNumero do
     begin
          Visible := ( FCampo.Tipo = xctNumero );
          if Visible then
          begin
               Valor := StrToReal(FCampo.ValorDefault);
               FormulaLBL.FocusControl := FormulaNumero;
          end;
     end;

     with FormulaBool do
     begin
          Visible := ( FCampo.Tipo in [xctBooleano,xctBoolStr] );
          if Visible then
          begin
               FormulaBool.Checked := zStrToBool( FCampo.ValorDefault );
               FormulaLBL.FocusControl := FormulaBool;
          end;
     end;

     with FormulaFecha do
     begin
          Visible := ( FCampo.Tipo = xctFecha );
          if Visible then
          begin
               Valor := StrAsFecha( FCampo.ValorDefault );
               FormulaLBL.FocusControl := FormulaFecha;
          end;
     end;

     with FormulaTabla do
     begin
          Visible := ( FCampo.Tipo = xctTabla );
          if Visible then
          begin
               LookupDataSet := dmTablas.GetEntidadAdicionalConnect( FCampo.Entidad );
               Llave := FCampo.ValorDefault;
               FormulaLBL.FocusControl := FormulaTabla;
          end;
     end;
end;

procedure TCampoEdit_DevEx.OKClick(Sender: TObject);
begin
     inherited;
        EscribeCambios;
end;

procedure TCampoEdit_DevEx.EscribeCambios;
begin
     if strLleno( edLetrero.Text ) then
     begin
          with FCampo do
          begin
               Letrero := edLetrero.Text;
               Obligatorio := chObligatorio.Checked;  
               Mostrar := arSiempre;
               case Tipo of
                    xctTexto: ValorDefault := FormulaTexto.Text;
                    xctNumero: ValorDefault := FormulaNumero.ValorAsText;
                    xctBooleano, xctBoolStr: ValorDefault := zBoolToStr( FormulaBool.Checked );
                    xctFecha: ValorDefault := FechaAsStr( FormulaFecha.Valor );
                    xctTabla: ValorDefault := FormulaTabla.Llave;
               end;
          end;
          ModalResult := mrOk;
     end
     else
     begin
          ZetaDialogo.ZError( self.Caption, 'Debe Especificarse Descripción del Campo', 0 );
          edLetrero.SetFocus;
     end;
end;

procedure TCampoEdit_DevEx.SetExcedeAlto(const Value: Boolean);
begin
     FExcedeAlto := Value;
     lblExcede.Visible := Value;
end;

procedure TCampoEdit_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
    EscribeCambios;
end;

end.
