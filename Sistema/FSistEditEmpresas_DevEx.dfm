inherited SistEditEmpresas_DevEx: TSistEditEmpresas_DevEx
  Left = 487
  Top = 227
  ClientHeight = 441
  ClientWidth = 455
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 405
    Width = 455
    TabOrder = 3
    inherited OK_DevEx: TcxButton
      Left = 291
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 370
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 455
    inherited ValorActivo2: TPanel
      Width = 129
      inherited textoValorActivo2: TLabel
        Width = 123
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 5
  end
  inherited Panel2: TPanel
    Width = 455
    Height = 355
    object DigitoGafetesLBL: TLabel [0]
      Left = 27
      Top = 101
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'gito en &Gafetes:'
      FocusControl = CM_DIGITO
    end
    object lbReportesKiosco: TLabel [1]
      Left = 18
      Top = 177
      Width = 96
      Height = 13
      Alignment = taRightJustify
      Caption = 'Reportes en Kiosco:'
      FocusControl = CM_KCLASIFI
    end
    object Label3: TLabel [2]
      Left = 41
      Top = 71
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = '&Base de Datos:'
    end
    inherited Label1: TLabel
      Left = 78
      Enabled = True
    end
    inherited Label2: TLabel
      Left = 55
      Width = 59
      Caption = '&Descripci'#243'n:'
    end
    object CM_DIGITO: TDBEdit [5]
      Left = 117
      Top = 97
      Width = 23
      Height = 21
      DataField = 'CM_DIGITO'
      DataSource = DataSource
      MaxLength = 1
      TabOrder = 3
      OnKeyPress = CM_DIGITOKeyPress
    end
    object CM_KCLASIFI: TZetaKeyCombo [6]
      Left = 117
      Top = 173
      Width = 300
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 6
      OnChange = CM_KCLASIFIChange
      Items.Strings = (
        '')
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object gbConfidencialidad: TcxGroupBox [7]
      Left = 22
      Top = 202
      Caption = ' Conf&idencialidad: '
      TabOrder = 7
      Height = 143
      Width = 410
      object btSeleccionarConfiden: TcxButton
        Left = 381
        Top = 48
        Width = 25
        Height = 25
        Hint = 'Seleccionar Confidencialidad'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = btSeleccionarConfidenClick
        OptionsImage.Glyph.Data = {
          7A080000424D7A08000000000000360000002800000017000000170000000100
          20000000000044080000C40E0000C40E0000000000000000000000B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF97E0F6FF97E0F6FF2CBFEDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2
          F7FFFFFFFFFFFFFFFFFFB7E9F9FF44C7EFFF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFFFFF
          FFFFFBFEFFFF9FE2F7FF18B9EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF9FE2F7FFAFE7F8FF24BD
          ECFF00B2E9FF0CB6EAFF68D1F2FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF10B7EAFF00B2E9FF14B8EBFF4CC9
          F0FFFFFFFFFFFFFFFFFF64D0F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFDFF5FCFFFFFFFFFFFFFF
          FFFFFFFFFFFFEBF9FDFF10B7EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FFA3E3F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFF8BDCF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF1CBAEBFFF7FDFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFBFEFFFF28BEECFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF80D9F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFB7E9F9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF08B4EAFFE3F7FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7E9
          F9FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF5CCEF1FFFFFFFFFFFFFFFFFFD7F3FCFF48C8EFFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FFB7E9F9FF6CD3F2FF04B3E9FF00B2E9FF1CBAEBFFA3E3F7FF8BDC
          F5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF08B4EAFF80D9F4FFF7FDFEFFFFFFFFFFA3E3F7FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF40C5EFFFE3F7FDFFFFFFFFFFF7FDFEFF7CD7F4FF08B4EAFF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF18B9
          EBFFBFECF9FFA7E4F7FF1CBAEBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
          E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
        OptionsImage.Margin = 1
      end
      object listaConfidencialidad: TListBox
        Left = 94
        Top = 48
        Width = 285
        Height = 81
        TabStop = False
        ExtendedSelect = False
        ItemHeight = 13
        TabOrder = 2
      end
      object rbConfidenNinguna: TcxRadioButton
        Left = 94
        Top = 11
        Width = 123
        Height = 17
        Caption = 'Sin Confidencialidad'
        Checked = True
        TabOrder = 0
        OnClick = rbConfidenNingunaClick
        Transparent = True
      end
      object rbConfidenAlgunas: TcxRadioButton
        Left = 94
        Top = 28
        Width = 156
        Height = 17
        Caption = 'Aplica algunas'
        TabOrder = 1
        OnClick = rbConfidenAlgunasClick
        Transparent = True
      end
    end
    object CM_USACASE: TDBCheckBox [8]
      Left = 54
      Top = 150
      Width = 76
      Height = 14
      Alignment = taLeftJustify
      Caption = 'Usa Ca&seta:'
      DataField = 'CM_USACASE'
      DataSource = DataSource
      TabOrder = 5
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object DB_CODIGO: TZetaDBKeyLookup_DevEx [9]
      Left = 117
      Top = 67
      Width = 316
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 85
      DataField = 'DB_CODIGO'
      DataSource = DataSource
    end
    object CM_USACAFE: TDBCheckBox [10]
      Left = 43
      Top = 127
      Width = 87
      Height = 14
      Alignment = taLeftJustify
      Caption = '&Usa Cafeter'#237'a:'
      DataField = 'CM_USACAFE'
      DataSource = DataSource
      TabOrder = 4
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    inherited CM_NOMBRE: TDBEdit
      Left = 117
      Width = 315
    end
    inherited CM_CODIGO: TZetaDBEdit
      Left = 117
      Enabled = True
    end
    inherited GroupBox1: TcxGroupBox
      Left = 457
      Top = 72
      Align = alNone
      TabOrder = 8
      Visible = False
      Height = 91
      Width = 169
      inherited Label8: TLabel
        Left = 41
      end
      inherited Label9: TLabel
        Left = 20
      end
      inherited Label10: TLabel
        Left = 182
        Top = 40
      end
      inherited Label5: TLabel
        Left = 174
        Top = 23
      end
      inherited Label7: TLabel
        Left = 89
      end
      inherited CM_USRNAME: TDBEdit
        Left = 117
        Width = 52
      end
      inherited CM_PASSWRD: TDBEdit
        Left = 181
        Top = 60
        Width = 52
      end
      inherited CM_CONTROL: TDBEdit
        Left = 213
        Top = 19
        Width = 52
      end
      inherited CM_DATOS: TDBEdit
        Left = 117
        Width = 52
      end
      inherited CM_ALIAS: TDBEdit
        Left = 117
        Width = 52
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 348
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 408
    Top = 0
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 376
  end
end
