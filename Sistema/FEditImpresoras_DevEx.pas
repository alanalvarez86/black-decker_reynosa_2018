unit FEditImpresoras_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, Buttons, DBCtrls, StdCtrls, ComCtrls, Mask,
     ZBaseEdicion_DevEx,
     ZetaEdit, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditImpresoras_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    PI_NOMBRE: TZetaDBEdit;
    PageControl_DevEx: TcxPageControl;
    GeneralesTab_DevEx: TcxTabSheet;
    EspecialesTab_DevEx: TcxTabSheet;
    PI_ITAL_OF: TDBEdit;
    PI_ITAL_ON: TDBEdit;
    PI_BOLD_OF: TDBEdit;
    PI_BOLD_ON: TDBEdit;
    PI_UNDE_OF: TDBEdit;
    PI_UNDE_ON: TDBEdit;
    PI_CHAR_17: TDBEdit;
    PI_CHAR_12: TDBEdit;
    PI_CHAR_10: TDBEdit;
    Label10: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    PI_EXTRA_2: TDBEdit;
    PI_EXTRA_1: TDBEdit;
    PI_LANDSCA: TDBEdit;
    PI_8_LINES: TDBEdit;
    PI_6_LINES: TDBEdit;
    PI_RESET: TDBEdit;
    PI_EJECT: TDBEdit;
    Label17: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditImpresoras_DevEx: TEditImpresoras_DevEx;

implementation

uses DSistema,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

{ ************ TEditImpresoras_DevEx ************ }

procedure TEditImpresoras_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_SIST_DATOS_IMPRESORAS;
     FirstControl := PI_NOMBRE;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_EDIT_IMPRESORAS;
     {$endif}     
end;

procedure TEditImpresoras_DevEx.FormShow(Sender: TObject);
begin
     PageControl_DevEx.ActivePage := GeneralesTab_DevEx;
     inherited;
end;

procedure TEditImpresoras_DevEx.Connect;
begin
     with dmSistema do
     begin
          Datasource.Dataset := cdsImpresoras;
     end;
end;

end.
