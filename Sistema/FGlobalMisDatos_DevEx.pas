unit FGlobalMisDatos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,
     {$ifndef VER130}Variants,{$endif}     
     ZetaNumero, ZetaCommonClasses, Grids,
  DBGrids, ZetaDBGrid, ZetaKeyCombo, DB, ZetaEdit, DBCtrls,
  ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxTextEdit, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid;

type
  TGlobalMisDatos_DevEx = class(TBaseGlobal_DevEx)
    Panel1: TPanel;
    lblUsuarioTareasAutomaticas: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    CM_CODIGO: TZetaDBKeyCombo;
    dsCarruselInfo: TDataSource;
    KW_TIM_DAT: TZetaDBNumero;
    dsCarrusel: TDataSource;
    Label3: TLabel;
    Global_URL: TZetaEdit;
    KW_GET_NIP: TDBCheckBox;
    GridRenglones: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    function ListaCompanies( Lista: TStrings ): Boolean;
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Cargar; override;
    procedure Descargar; override;
  end;

var
  GlobalMisDatos_DevEx: TGlobalMisDatos_DevEx;

implementation

uses ZetaDialogo,
     ZAccesosTress,
     DCliente, dSistema;

{$R *.DFM}

procedure TGlobalMisDatos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_MISDATOS;
     HelpContext := H_Globales_De_MisDatos;

end;

procedure TGlobalMisDatos_DevEx.Cargar;
begin
     ListaCompanies( CM_CODIGO.Lista );
     with dmSistema do
     begin
          with cdsCarruseles do
          begin
          		 Conectar;
               if not Locate('KW_CODIGO','MDATOS',[]) then
               begin
                    ZError('Configuración de Mis Datos','La configuración de Mis Datos no está inicializada , favor de inicializar Datos de Comparte en TressCFG',0);
               			Close;
               end
               else
               begin
                    dsCarrusel.DataSet := cdsCarruseles;
               		  cdsCarruselesInfo.Conectar;
     							  dsCarruselInfo.DataSet := cdsCarruselesInfo;
               end;
          end;
          Global_URL.Text := GetGlobalRaizKiosco;
     end;
end;

procedure TGlobalMisDatos_DevEx.DesCargar;
begin
     with dmSistema do
     begin
          cdsCarruseles.Enviar;
          GrabaGlobalRaizKiosco(Global_URL.Text);
     end;

     Close;
end;

procedure TGlobalMisDatos_DevEx.CancelarClick(Sender: TObject);
begin
      with dmSistema do
     begin
          cdsCarruseles.CancelUpdates;
          cdsCarruselesInfo.CancelUpdates; 
     end;
     inherited;

end;

function TGlobalMisDatos_DevEx.ListaCompanies( Lista: TStrings ): Boolean;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             with dmSistema.cdsEmpresas do
             begin
                  Conectar;
                  First;
                  while not Eof do
                  begin
                       Add( Format( '%s=%s', [ FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ) );
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;

procedure TGlobalMisDatos_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     with dmSistema do
     begin
          cdsCarruseles.CancelUpdates;
          cdsCarruselesInfo.CancelUpdates;
     end;
     inherited;

end;

end.
