unit FEditSistLstDispositivos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, Mask, ZetaNumero, ZetaKeyCombo, ZetaEdit,  
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZBaseGridLectura_DevEx, dxSkinsdxBarPainter, cxContainer, cxTextEdit,
  cxMemo, cxDBEdit, dxBarExtItems, dxBar, cxDBNavigator, cxButtons;

type
  TEditSistLstDispositivos_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    lblDI_TIPO: TLabel;
    lblDI_NOTA: TLabel;
    lblDI_NOMBRE: TLabel;
    lblDI_IP: TLabel;
    lblDI_DESCRIP: TLabel;
    DI_NOMBRE: TZetaDBEdit;
    DI_IP: TDBEdit;
    DI_DESCRIP: TDBEdit;
    cbDI_TIPO: TZetaDBKeyCombo;
    DI_NOTA: TcxDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
  { Protected declarations }
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditSistLstDispositivos_DevEx: TEditSistLstDispositivos_DevEx;

implementation

uses dSistema,
     ZAccesosTress,
     ZetaCommonClasses, ZetaCommonLists;

{$R *.dfm}

{ TEditSistLstDispositivos }

procedure TEditSistLstDispositivos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_SIST_LST_DISPOSITIVOS;
     HelpContext := H_EDIT_LISTA_DISPOSIT;
     FirstControl := DI_NOMBRE;
end;

procedure TEditSistLstDispositivos_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  with dmSistema do
  begin
       with cdsSistLstDispositivos do
            cbDI_TIPO.Enabled :=not ((FieldByName('DI_TIPO').AsInteger = Ord(dpCafeteria)) and
                                      (FieldByName('STATUS').AsInteger = Ord(tConfigurada)));
  end;
end;

procedure TEditSistLstDispositivos_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsSistLstDispositivos.Conectar;
          DataSource.DataSet := cdsSistLstDispositivos;
     end;
end;

end.
