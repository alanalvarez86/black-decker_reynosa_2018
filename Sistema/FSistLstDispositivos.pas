unit FSistLstDispositivos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls;

type
  TSistLstDispositivos = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistLstDispositivos: TSistLstDispositivos;

implementation
uses
    DSistema,
    ZetaCommonClasses,
    ZAccesosMgr,
    ZAccesosTress,
    ZBaseGridEdicion;
{$R *.dfm}

{ TLstDispositivos }

procedure TSistLstDispositivos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_LISTA_DISPOSIT;
end;

procedure TSistLstDispositivos.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsSistLstDispositivos.Conectar;
          DataSource.DataSet := cdsSistLstDispositivos;
     end;
end;

procedure TSistLstDispositivos.Refresh;
begin
     inherited;
     dmSistema.cdsSistLstDispositivos.Refrescar;

end;

procedure TSistLstDispositivos.Agregar;
begin
     inherited;
      dmSistema.cdsSistLstDispositivos.Agregar;
end;

procedure TSistLstDispositivos.Modificar;
begin
     inherited;
     dmSistema.cdsSistLstDispositivos.Modificar;

end;

procedure TSistLstDispositivos.Borrar;
begin
     inherited;
      dmSistema.cdsSistLstDispositivos.Borrar;     

end;

end.
