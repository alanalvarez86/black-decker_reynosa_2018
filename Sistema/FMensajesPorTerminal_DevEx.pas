unit FMensajesPorTerminal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, ZetaKeyLookup_DevEx, System.Actions;

type
  TMensajesPorTerminal_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    luTerminal: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    DM_CODIGO: TcxGridDBColumn;
    DM_MENSAJE: TcxGridDBColumn;
    TM_SLEEP: TcxGridDBColumn;
    TM_NEXT: TcxGridDBColumn;
    TM_ULTIMA: TcxGridDBColumn;
    TM_NEXTXHR: TcxGridDBColumn;
    TM_NEXTHOR: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure luTerminalValidKey(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CargaDatos;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh; override;
  end;

var
  MensajesPorTerminal_DevEx: TMensajesPorTerminal_DevEx;

implementation

uses dSistema,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo;

{$R *.dfm}

procedure TMensajesPorTerminal_DevEx.Agregar;
begin
     if StrLleno( luTerminal.Llave )then
     begin
          inherited;
          with dmSistema do
          begin
               TerminalSeleccionada := luTerminal.Llave;
               cdsMensajesPorTerminal.Agregar;
          end;
     end
     else
     begin
          luTerminal.SetFocus;
          ZetaDialogo.zError( Self.Caption, 'Es necesario especificar una terminal', 0 );
     end;
end;

procedure TMensajesPorTerminal_DevEx.Borrar;
begin
     inherited;
     dmSistema.cdsMensajesPorTerminal.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMensajesPorTerminal_DevEx.CargaDatos;
const
     K_FILTRO = '(TE_CODIGO=%s)';
begin
     with dmSistema do
     begin
          if StrVacio( luTerminal.Llave )then
             FiltroTerminal := Format( K_FILTRO, [ '-1' ] )
          else
              FiltroTerminal := Format( K_FILTRO, [ Trim( luTerminal.Llave ) ] );

          cdsMensajesPorTerminal.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMensajesPorTerminal_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          Self.CargaDatos;
          DataSource.DataSet := cdsMensajesPorTerminal;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMensajesPorTerminal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_MSGXTERMINAL;
     with dmSistema do
     begin
          cdsTerminales.Conectar;
          luTerminal.LookUpDataSet := cdsTerminales;
     end;
end;

procedure TMensajesPorTerminal_DevEx.Modificar;
begin
     inherited;
     with dmSistema do
     begin
          if StrLleno( cdsMensajesPorTerminal.FieldByName( 'TE_CODIGO' ).AsString )then
          begin
               TerminalSeleccionada := cdsMensajesPorTerminal.FieldByName( 'TE_CODIGO' ).AsString;
               cdsMensajesPorTerminal.Modificar;
          end
          else
              ZetaDialogo.zError( Self.Caption, 'Se gener� un error al asignar la terminal a modificar', 0 );
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMensajesPorTerminal_DevEx.Refresh;
begin
     inherited;
     Self.CargaDatos;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TMensajesPorTerminal_DevEx.luTerminalValidKey(Sender: TObject);
begin
     inherited;
     Self.CargaDatos;
end;

procedure TMensajesPorTerminal_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     inherited;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
