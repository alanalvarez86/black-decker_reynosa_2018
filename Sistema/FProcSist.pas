unit FProcSist;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, ToolWin, Db, ExtCtrls, ImgList,
     ZBaseConsulta,
     ZBaseConsultaBotones, StdCtrls;

type
  TProcSist = class(TBaseBotones)
    BorrarBajas: TToolButton;
    BorrarNominas: TToolButton;
    BorrarTarjetas: TToolButton;
    BorrarPOLL: TToolButton;
    BorrarBitacora: TToolButton;
    BorrarHerramienta: TToolButton;
    ToolButton1: TToolButton;
    btnEnrolamientoMasivo: TToolButton;
    btnImportarEnrolamiento: TToolButton;
    btnDepuraBitBio: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    AgregarBaseDatosSeleccion: TToolButton;
    AgregarBaseDatosVisitantes: TToolButton;
    AgregarBaseDatosPruebas: TToolButton;
    AgregarBaseDatosEmpleados: TToolButton;
    AgregarBaseDatosPresupuestos: TToolButton;
    ToolButton4: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ProcSist: TProcSist;

implementation

uses FTressShell,
     ZetaCommonClasses;

{$R *.DFM}

procedure TProcSist.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H80083_Procesos_Sistema;
end;


end.
