unit FSistUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseUsuarios, Db, StdCtrls, Buttons, Grids, DBGrids, ZetaDBGrid,
  ExtCtrls, ZetaKeyCombo;

type
  TSistUsuarios = class(TSistBaseUsuarios)
    Suspender: TBitBtn;
    Activar: TBitBtn;
    Supervisa: TBitBtn;
    Areas: TBitBtn;
    Bloquear: TBitBtn;
    SistemaBloqueadoPanel: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    CCosto: TBitBtn;
    procedure SuspenderClick(Sender: TObject);
    procedure ActivarClick(Sender: TObject);
    procedure SupervisaClick(Sender: TObject);
    procedure AreasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BloquearClick(Sender: TObject);
    procedure CCostoClick(Sender: TObject);
  private
    { Private declarations }
    procedure EditPanelSistema;
  protected
    procedure Refresh; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistUsuarios: TSistUsuarios;

implementation

{$R *.DFM}

uses DSistema,
     DGlobal,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZBaseDlgModal,
     ZetaDialogo,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     FSistEditUsuarioSupervisor,
     FSistEditUsuarioArea,
     FSistEditUsuarioCCosto, 
     DCliente,
     ZToolsPE;

procedure TSistUsuarios.FormShow(Sender: TObject);
var
   sAreas: string;
begin
     inherited;
     sAreas := Global.GetGlobalString( K_GLOBAL_LABOR_AREAS );
     with Areas do
     begin
          //Caption := Format( '&%s', [ sAreas ] );
          Hint    := Format( 'Configurar %s A Su Cargo', [ sAreas ] );
          Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_AREAS, K_DERECHO_CONSULTA );
     end;


     with CCosto do
     begin
          Hint    := Format( 'Configurar %s A Su Cargo', [ Global.NombreCosteo ] );
          Enabled := dmCliente.ModuloAutorizadoCosteo;
     end;

     Activar.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_PRENDE_USUARIOS, K_DERECHO_CONSULTA );
     //Activar.Visible:=False; Se implementar� hasta la pr�xima versi�n AP: 5/Jun/2007
     Supervisa.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_SUPER, K_DERECHO_CONSULTA );
     Suspender.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_APAGA_USUARIOS, K_DERECHO_CONSULTA );
     //Suspender.Visible:=False; Se implementar� hasta la pr�xima versi�n AP: 5/Jun/2007
     Bloquear.Visible:= ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );
     EditPanelSistema;

     Supervisa.Enabled := dmCliente.EmpresaAbierta;
     Areas.Enabled := dmCliente.EmpresaAbierta;
     CCosto.Enabled := dmCliente.EmpresaAbierta;
end;

procedure TSistUsuarios.SuspenderClick(Sender: TObject);
begin
     dmSistema.SuspendeUsuarios;
end;

procedure TSistUsuarios.ActivarClick(Sender: TObject);
begin
     dmSistema.ActivaUsuarios;
end;

procedure TSistUsuarios.SupervisaClick(Sender: TObject);
begin
     //ZBaseDlgModal.ShowDlgModal( FSistEditUserSuper, TFSistEditUserSuper );
     if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
     begin
          ZBaseDlgModal.ShowDlgModal( FSistEditUserSuper, TFSistEditUserSuper );
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
     end;
end;

procedure TSistUsuarios.AreasClick(Sender: TObject);
begin
     //ZBaseDlgModal.ShowDlgModal( FSistEditUserArea, TFSistEditUserArea );
     if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
     begin
          ZBaseDlgModal.ShowDlgModal( FSistEditUserArea, TFSistEditUserArea );
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
     end;
end;

procedure TSistUsuarios.BloquearClick(Sender: TObject);
var
   lBlockSistema: Boolean;
begin
     lBlockSistema:= not dmSistema.BloqueoSistema ;
     dmCliente.GrabaBloqueoSistema(lBlockSistema);
     Refresh;
end;

procedure TSistUsuarios.Refresh;
begin
     inherited;
     EditPanelSistema;
end;

procedure TSistUsuarios.EditPanelSistema;
begin
     with SistemaBloqueadoPanel do
     begin
          if( dmSistema.BloqueoSistema ) then
          begin
               Caption := 'Sistema Bloqueado';
               Font.Color := clRed;
          end
          else
          begin
               Caption := 'Sistema Habilitado';
               Font.Color := clBlack;
          end;
     end;
end;

function TSistUsuarios.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
          if (not Result) then
          begin
               sMensaje := 'No Tiene Permiso Para Modificar Registros';
          end;
     end;
end;

procedure TSistUsuarios.CCostoClick(Sender: TObject);
begin
     if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
     begin
          ZBaseDlgModal.ShowDlgModal( SistEditUsuarioCCosto, TSistEditUsuarioCCosto );
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
     end;
end;

end.
