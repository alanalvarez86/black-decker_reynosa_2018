unit FExportarTablas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, DB, ZetaEdit, StdCtrls, CheckLst, Buttons,
  ExtCtrls, ZetaDBTextBox, FileCtrl, ComCtrls, DBClient, ZetaClientDataSet;

type
  TExportarTablas = class(TZetaDlgModal)
    DirectorioGB: TGroupBox;
    PathLBL: TLabel;
    PathSeek: TSpeedButton;
    Directorio: TEdit;
    gbBaseDatos: TGroupBox;
    lblDescripcion: TLabel;
    lblCodigo: TLabel;
    lblTipo: TLabel;
    DataSource: TDataSource;
    DB_CODIGO: TZetaDBTextBox;
    DB_DESCRIP: TZetaDBTextBox;
    DB_TIPO: TZetaDBTextBox;
    PageControl: TPageControl;
    TabTablas: TTabSheet;
    TabBitacora: TTabSheet;
    ExportTables: TCheckListBox;
    MemBitacora: TMemo;
    cdsDataset: TZetaClientDataSet;
    procedure FormShow(Sender: TObject);
    procedure PathSeekClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure ExportTablesClickCheck(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    { Private declarations }
//    FListaTablas: TStrings;
    FEditing: Boolean;
    FExporting: Boolean;
    function ValidaDirectorio: Boolean;
//    function ValidaTablas( var iCuantas: Integer ): Boolean;
    function VerificaEditing: Boolean;
    procedure ReiniciaTablasSeleccionadas;
    procedure HabilitaControles;
    procedure InitBitacora;
    procedure EscribeBitacora(const sMensaje: String);
    procedure Exportar;
  public
    { Public declarations }
  end;

var
  ExportarTablas: TExportarTablas;

implementation

uses dSistema, ZetaMessages, ZetaDialogo, ZetaCommonClasses;

{$R *.dfm}

const
     K_TAB_TABLAS = 0;
     K_TAB_BITACORA = 1;

procedure TExportarTablas.FormShow(Sender: TObject);
var
   oCursor : TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourGlass;
        DataSource.DataSet := dmSistema.cdsSistBaseDatos;
        Directorio.Text := ZetaMessages.SetFileNameDefaultPath( '' );
        ExportTables.Items.Clear;
        dmSistema.GetListaTablas( ExportTables.Items );
        PageControl.TabIndex := K_TAB_TABLAS;
        HabilitaControles;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TExportarTablas.OKClick(Sender: TObject);
begin
     inherited;
     Exportar;
end;

procedure TExportarTablas.Exportar;
var
   i, iCuantos: Integer;
   sTablaName, sFileName: String;
begin
     if ValidaDirectorio then
     begin
          PageControl.TabIndex := K_TAB_BITACORA;
          FExporting := TRUE;
          try
             HabilitaControles;
             InitBitacora;
             iCuantos := 0;
             with ExportTables do
             begin
                  for i := 0 to ( Items.Count - 1 ) do
                  begin
                       if ( not FExporting ) then
                          Exit;
                       if Checked[ i ] then    //Exportar
                       begin
                            sTablaName := Items.Strings[ i ];
                            sFileName := SetFileNamePath( Directorio.Text, sTablaName + '.xml' );
                            if FileExists( sFileName ) and ZetaDialogo.ZConfirm( Self.Caption, Format( 'Archivo %s ya existe' + CR_LF + '�Desea remplazarlo? ', [ sTablaName + '.xml' ] ), 0, mbNo ) then
                            begin
                                 SysUtils.DeleteFile( sFileName );
                            end;
                            if ( not FileExists( sFileName ) ) then
                            begin
                                 dmSistema.GetTablaInfo( sTablaName, cdsDataset );
                                 cdsDataset.SaveToFile( sFileName, dfXML );
                                 Inc( iCuantos );
                                 EscribeBitacora( Format( 'Archivo: %s exportado con �xito', [ sFileName ] ) );
                                 Sleep(1000);   // Espera 1 segundo a ver si han cancelado el proceso
                            end;
                       end;
                       Application.ProcessMessages;
                  end;
             end;
          finally
                 FExporting := FALSE;
          end;
          if ( iCuantos > 0 ) then
          begin
               ZetaDialogo.zInformation( Self.Caption, Format( 'Se exportaron %d archivos: ', [ iCuantos ] ), 0 );
          end
          else
              ZetaDialogo.zInformation( Self.Caption, 'No se exportaron archivos', 0 );
          ReiniciaTablasSeleccionadas;
     end;
end;

function TExportarTablas.ValidaDirectorio: Boolean;
begin
     Result := DirectoryExists( Directorio.Text );
     if ( not Result ) then
     begin
          ZetaDialogo.zWarning( Self.Caption, 'Directorio indicado no existe', 0, mbOK );
          ActiveControl := Directorio;
     end;
end;

{
function TExportarTablas.ValidaTablas( var iCuantas: Integer ): Boolean;
var
   i: Integer;
begin
     iCuantas := 0;
     FListaTablas.Clear;
     with ExportTables do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               if Checked[ i ] then
               begin
                    FListaTablas.Add( Items.Strings[ i ] );
                    Inc( iCuantas );
               end;
          end;
     end;
     Result :=( iCuantas > 0 );
     if ( not Result ) then
     begin
          ZetaDialogo.zWarning( Self.Caption, 'Se debe especificar al menos una tabla a exportar', 0, mbOK );
          ActiveControl := ExportTables;
     end;
end;
}

procedure TExportarTablas.ReiniciaTablasSeleccionadas;
var
   i: Integer;
begin
     with ExportTables do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               Checked[ i ] := FALSE;
          end;
     end;
     FEditing := FALSE;
     HabilitaControles;
     PageControl.TabIndex := K_TAB_TABLAS;
end;

procedure TExportarTablas.HabilitaControles;
begin
     OK.Enabled := FEditing and ( not FExporting );
     if FEditing or FExporting then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar exportaci�n';
          end;
     end
     else
     begin
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                 SetFocus;
          end;
     end;
     Application.ProcessMessages;
end;

procedure TExportarTablas.ExportTablesClickCheck(Sender: TObject);
begin
     inherited;
     FEditing := VerificaEditing;
     HabilitaControles;
end;

function TExportarTablas.VerificaEditing: Boolean;
var
   i: Integer;
begin
     Result := FALSE;
     with ExportTables do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               Result := Checked[ i ];
               if Result then
                  Exit;
          end;
     end;
end;

procedure TExportarTablas.CancelarClick(Sender: TObject);
begin
     if FExporting then
     begin
          if ZetaDialogo.ZConfirm( Self.Caption, '�Seguro de cancelar el proceso?', 0, mbNo ) then
          begin
               FExporting := FALSE;
               HabilitaControles;
          end;
     end
     else if FEditing then
        ReiniciaTablasSeleccionadas
     else
         Close;
end;

procedure TExportarTablas.PathSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     inherited;
     with Directorio do
     begin
          sDirectory := Text;
          if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
             Text := sDirectory;
     end;
end;

procedure TExportarTablas.InitBitacora;
begin
     MemBitacora.Clear;
     Application.ProcessMessages;
end;

procedure TExportarTablas.EscribeBitacora( const sMensaje: String );
begin
     MemBitacora.Lines.Add( sMensaje );
end;

end.
