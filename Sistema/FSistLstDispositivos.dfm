inherited SistLstDispositivos: TSistLstDispositivos
  Left = 561
  Top = 289
  Caption = 'Lista de Dispositivos'
  PixelsPerInch = 96
  TextHeight = 13
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 429
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DI_TIPO'
        Title.Caption = 'Tipo'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_NOMBRE'
        Title.Caption = 'Identificador'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_IP'
        Title.Caption = 'IP/Computadora'
        Width = 105
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 368
    Top = 112
  end
end
