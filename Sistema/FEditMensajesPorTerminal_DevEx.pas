unit FEditMensajesPorTerminal_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaFecha, Mask, ZetaNumero, ZetaDBTextBox,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, ZetaHora, cxRadioGroup, cxGroupBox,
  cxCheckBox, cxDBEdit;

type
  TEditMensajesPorTerminal_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    TE_CODIGO: TZetaDBKeyLookup_DevEx;
    Label2: TLabel;
    DM_CODIGO: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    TM_NEXT: TZetaDBTextBox;
    btnAplicarMensaje_DevEx: TcxButton;
    Label3: TLabel;
    TM_SLEEP: TZetaDBNumero;
    Label6: TLabel;
    TM_NEXTXHR: TDBCheckBox;
    TM_NEXTHOR: TZetaDBHora;
    Label7: TLabel;
    procedure FormCreate(Sender: TObject);
    //procedure btnAplicarMensajeClick(Sender: TObject);
    procedure btnAplicarMensaje_DevExClick(Sender: TObject);
    procedure TM_NEXTXHRClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitarConfHoraMinutos;
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure HabilitaControles; override;
  end;

var
  EditMensajesPorTerminal_DevEx: TEditMensajesPorTerminal_DevEx;

implementation

uses dSistema,
     ZAccesosTress,
     ZetaDialogo,
     ZetaCommonClasses;

{$R *.dfm}

{ TEditMensajesPorTerminal }

procedure TEditMensajesPorTerminal_DevEx.Connect;
begin
     inherited;
     with dmSistema do
     begin
          cdsMensajesPorTerminal.Conectar;
          DataSource.DataSet := cdsMensajesPorTerminal;
     end;
end;

procedure TEditMensajesPorTerminal_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H_TRESS_SISTEMA_TERMINALESGTI_MSGXTERMINAL_EDIC;
     IndexDerechos := ZAccesosTress.D_SIST_MENSAJES_TERM;
     FirstControl := DM_CODIGO;

     with dmSistema do
     begin
          cdsTerminales.Conectar;
          cdsMensajes.Conectar;
          TE_CODIGO.LookUpDataSet := cdsTerminales;
          DM_CODIGO.LookUpDataSet := cdsMensajes;

          with cdsMensajesPorTerminal do
               btnAplicarMensaje_DevEx.Enabled := not( State in [dsInsert, dsEdit] );
     end;
end;

procedure TEditMensajesPorTerminal_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     HabilitarConfHoraMinutos;
     TE_CODIGO.SetFocus;
end;

procedure TEditMensajesPorTerminal_DevEx.HabilitaControles;
begin
     inherited;
     with dmSistema.cdsMensajesPorTerminal do
          btnAplicarMensaje_DevEx.Enabled := not( State in [dsInsert, dsEdit] );
end;

procedure TEditMensajesPorTerminal_DevEx.TM_NEXTXHRClick(Sender: TObject);
begin
    inherited;
    HabilitarConfHoraMinutos;
    {if TM_NEXTXHR.Checked then
        TM_NEXTHOR.SetFocus
    else
      TM_SLEEP.SetFocus;}
end;

procedure TEditMensajesPorTerminal_DevEx.btnAplicarMensaje_DevExClick(
  Sender: TObject);
var
   lAplicarMensaje: Boolean;
begin
     inherited;
     lAplicarMensaje := False;
     with dmSistema.cdsMensajesPorTerminal do
     begin
          if (State in [dsInsert])then
             ZInformation( Self.Caption, 'Es necesario grabar el nuevo registro para aplicar el mensaje', 0)
          else if (State in [dsEdit])then
          begin
               if ZConfirm( Self.Caption, 'Para aplicar el mensaje es necesario guardar los cambios' + CR_LF + '�Desea guardar en este momento?', 0, mbYes)then
               begin
                    Enviar;
                    lAplicarMensaje := True;
               end
          end
          else
              lAplicarMensaje := True;

          if lAplicarMensaje then
          begin
               if ZConfirm( Self.Caption, 'Se aplicar� el mensaje a la terminal actual' + CR_LF + '�Desea continuar?', 0, mbYes )then
               begin
                    Edit;
                    FieldByName( 'TM_NEXT' ).AsDateTime := NullDateTime;
                    Enviar;
               end;
          end;
     end;
end;

procedure TEditMensajesPorTerminal_DevEx.HabilitarConfHoraMinutos;
begin
    TM_SLEEP.Enabled := not TM_NEXTXHR.Checked;
    TM_NEXTHOR.Enabled := TM_NEXTXHR.Checked;
end;

end.
