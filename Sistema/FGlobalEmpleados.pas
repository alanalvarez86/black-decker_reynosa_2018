unit FGlobalEmpleados;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZBaseGlobal,
     ZetaKeyLookup,
     ZetaNumero,
     ZetaClientDataSet,
     ZetaKeyCombo;

type
  TGlobalEmpleados = class(TBaseGlobal)
    PageControl: TPageControl;
    Identificacion: TTabSheet;
    Contratacion: TTabSheet;
    Area: TTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    CB_NIVEL1: TZetaKeyLookup;
    CB_NIVEL2: TZetaKeyLookup;
    CB_NIVEL3: TZetaKeyLookup;
    CB_NIVEL4: TZetaKeyLookup;
    CB_NIVEL5: TZetaKeyLookup;
    CB_NIVEL6: TZetaKeyLookup;
    CB_NIVEL7: TZetaKeyLookup;
    CB_NIVEL8: TZetaKeyLookup;
    CB_NIVEL9: TZetaKeyLookup;
    LCiudad: TLabel;
    LEstado: TLabel;
    LHabita: TLabel;
    LViveCon: TLabel;
    LEdoCivil: TLabel;
    LTransporte: TLabel;
    LNacio: TLabel;
    Label2: TLabel;
    LEstudios: TLabel;
    CB_CIUDAD: TEdit;
    CB_NACION: TEdit;
    CB_ESTADO: TZetaKeyLookup;
    CB_EDO_CIV: TZetaKeyLookup;
    CB_VIVEEN: TZetaKeyLookup;
    CB_VIVECON: TZetaKeyLookup;
    CB_MED_TRA: TZetaKeyLookup;
    CB_ESTUDIO: TZetaKeyLookup;
    Label12: TLabel;
    Label36: TLabel;
    CB_HORARIOlbl: TLabel;
    CB_CLASIFIlbl: TLabel;
    CB_PUESTOlbl: TLabel;
    CB_CONTRATlbl: TLabel;
    CB_SALARIOlbl: TLabel;
    Label17: TLabel;
    CB_AUTOSAL: TCheckBox;
    CB_SALARIO: TZetaNumero;
    CB_CONTRAT: TZetaKeyLookup;
    CB_PUESTO: TZetaKeyLookup;
    CB_CLASIFI: TZetaKeyLookup;
    CB_TURNO: TZetaKeyLookup;
    CB_PATRON: TZetaKeyLookup;
    CB_TABLASS: TZetaKeyLookup;
    CB_ZONA_GE: TMaskEdit;
    CB_SEXO: TZetaKeyCombo;
    CB_MOT_BAJ: TZetaKeyLookup;
    Label1: TLabel;
    CB_CHECA: TCheckBox;
    LTipoCreden: TLabel;
    CB_CREDENC: TEdit;
    NumEmpAuto: TCheckBox;
    CB_NIVEL10: TZetaKeyLookup;
    CB_NIVEL10lbl: TLabel;
    CB_NIVEL11lbl: TLabel;
    CB_NIVEL11: TZetaKeyLookup;
    CB_NIVEL12: TZetaKeyLookup;
    CB_NIVEL12lbl: TLabel;
    CB_MUNICIP: TZetaKeyLookup;
    Label3: TLabel;
    Label16: TLabel;
    CB_REGIMEN: TZetaKeyCombo;
    lBanco: TLabel;
    CB_BANCO: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_ESTADOValidKey(Sender: TObject);
    procedure CB_ESTADOValidLookup(Sender: TObject);
    procedure CB_MUNICIPValidKey(Sender: TObject);
  private
    { Private declarations }
    procedure SetCamposNivel;
    procedure ConfigLookup(Control: TZetaKeyLookup; Dataset: TZetaLookupDataset; const iTag: Integer);
  public
     FCambio : Boolean;
    { Public declarations }
  end;

var
  GlobalEmpleados: TGlobalEmpleados;

implementation

uses DTablas,
     DCatalogos,
     DGlobal,
     ZAccesosTress,
     ZGlobalTress,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TGlobalEmpleados.FormCreate(Sender: TObject);
var
   i: Integer;
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_EMPLEADOS;
     SetCamposNivel;
     with CB_SEXO do
     begin
          LlenaLista( lfSexo, Lista );
          with Lista do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Strings[ i ] := ObtieneElemento( lfSexo, i ) + '=' + Strings[ i ];
               end;
          end;
     end;

     CB_CIUDAD.Tag      := K_GLOBAL_CIUDAD_EMPRESA;
     ConfigLookup( CB_ESTADO, dmTablas.cdsEstado, K_GLOBAL_ENTIDAD_EMPRESA );
     ConfigLookup( CB_MUNICIP, dmTablas.cdsMunicipios, K_GLOBAL_DEF_MUNICIPIO );
     CB_CHECA.Tag       := K_GLOBAL_DEF_CHECA_TARJETA;
     CB_NACION.Tag      := K_GLOBAL_DEF_NACIONALIDAD;
     CB_SEXO.Tag        := K_GLOBAL_DEF_SEXO;
     CB_CREDENC.Tag     := K_GLOBAL_DEF_LETRA_GAFETE;
     ConfigLookup( CB_EDO_CIV, dmTablas.cdsEstadoCivil, K_GLOBAL_DEF_ESTADO_CIVIL );
     ConfigLookup( CB_VIVEEN, dmTablas.cdsHabitacion, K_GLOBAL_DEF_VIVE_EN );
     ConfigLookup( CB_VIVECON, dmTablas.cdsViveCon, K_GLOBAL_DEF_VIVE_CON );
     ConfigLookup( CB_MED_TRA, dmTablas.cdsTransporte, K_GLOBAL_DEF_MEDIO_TRANSPORTE );
     ConfigLookup( CB_ESTUDIO, dmTablas.cdsEstudios, K_GLOBAL_DEF_ESTUDIOS );
     NumEmpAuto.Tag     := K_GLOBAL_NUM_EMP_AUTOMATICO;

     CB_AUTOSAL.Tag     := K_GLOBAL_DEF_SALARIO_TAB;
     CB_SALARIO.Tag     := K_GLOBAL_DEF_SALARIO_DIARIO;
     ConfigLookup( CB_CONTRAT, dmCatalogos.cdsContratos, K_GLOBAL_DEF_CONTRATO );
     CB_ZONA_GE.Tag     := K_GLOBAL_ZONAS_GEOGRAFICAS;
     ConfigLookup( CB_PUESTO, dmCatalogos.cdsPuestos, K_GLOBAL_DEF_PUESTO );
     ConfigLookup( CB_CLASIFI, dmCatalogos.cdsClasifi, K_GLOBAL_DEF_CLASIFICACION );
     ConfigLookup( CB_TURNO, dmCatalogos.cdsTurnos, K_GLOBAL_DEF_TURNO );
     ConfigLookup( CB_PATRON, dmCatalogos.cdsRPatron, K_GLOBAL_DEF_REGISTRO_PATRONAL );
     ConfigLookup( CB_TABLASS, dmCatalogos.cdsSSocial, K_GLOBAL_DEF_PRESTACIONES );
     ConfigLookup( CB_MOT_BAJ, dmTablas.cdsMotivoBaja, K_GLOBAL_DEF_MOTIVO_BAJA );
     ConfigLookup( CB_BANCO, dmTablas.cdsBancos, K_GLOBAL_DEF_BANCO );

     ConfigLookup( CB_NIVEL1, dmTablas.cdsNivel1, K_GLOBAL_DEF_NIVEL_1 );
     ConfigLookup( CB_NIVEL2, dmTablas.cdsNivel2, K_GLOBAL_DEF_NIVEL_2 );
     ConfigLookup( CB_NIVEL3, dmTablas.cdsNivel3, K_GLOBAL_DEF_NIVEL_3 );
     ConfigLookup( CB_NIVEL4, dmTablas.cdsNivel4, K_GLOBAL_DEF_NIVEL_4 );
     ConfigLookup( CB_NIVEL5, dmTablas.cdsNivel5, K_GLOBAL_DEF_NIVEL_5 );
     ConfigLookup( CB_NIVEL6, dmTablas.cdsNivel6, K_GLOBAL_DEF_NIVEL_6 );
     ConfigLookup( CB_NIVEL7, dmTablas.cdsNivel7, K_GLOBAL_DEF_NIVEL_7 );
     ConfigLookup( CB_NIVEL8, dmTablas.cdsNivel8, K_GLOBAL_DEF_NIVEL_8 );
     ConfigLookup( CB_NIVEL9, dmTablas.cdsNivel9, K_GLOBAL_DEF_NIVEL_9 );
     {$ifdef ACS}
     ConfigLookup( CB_NIVEL10, dmTablas.cdsNivel10, K_GLOBAL_DEF_NIVEL_10 );
     ConfigLookup( CB_NIVEL11, dmTablas.cdsNivel11, K_GLOBAL_DEF_NIVEL_11 );
     ConfigLookup( CB_NIVEL12, dmTablas.cdsNivel12, K_GLOBAL_DEF_NIVEL_12 );
     {$endif}
     CB_REGIMEN.Tag :=  K_GLOBAL_DEF_REGIMEN;

     HelpContext        := H65110_Empleados;
end;

procedure TGlobalEmpleados.FormShow(Sender: TObject);
begin
     with dmCatalogos do
     begin
          cdsContratos.Conectar;
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
          cdsSSocial.Conectar;
     end;
     with dmTablas do
     begin
          cdsEstado.Conectar;
          cdsMunicipios.Conectar;
          cdsEstadoCivil.Conectar;
          cdsHabitacion.Conectar;
          cdsViveCon.Conectar;
          cdsTransporte.Conectar;
          cdsEstudios.Conectar;
          cdsMotivoBaja.Conectar;
          cdsBancos.Conectar;
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     inherited;
     FCambio := False;
     CB_ESTADOValidKey(Self);
end;

procedure TGlobalEmpleados.ConfigLookup( Control: TZetaKeyLookup; Dataset: TZetaLookupDataset; const iTag: Integer );
begin
     with Control do
     begin
          LookupDataset := Dataset;
          Tag := iTag;
     end;
end;

procedure TGlobalEmpleados.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TGlobalEmpleados.CB_ESTADOValidKey(Sender: TObject);
begin
     inherited;
     if StrLleno(CB_ESTADO.Llave) then
     begin
          CB_MUNICIP.Filtro := Format('TB_ENTIDAD = ''%s''',[CB_ESTADO.Llave]);
     end
     else
         CB_MUNICIP.Filtro := VACIO;

     if FCambio then
        CB_MUNICIP.Llave := VACIO;
end;

procedure TGlobalEmpleados.CB_ESTADOValidLookup(Sender: TObject);
begin
     inherited;
     FCambio := True;
end;

procedure TGlobalEmpleados.CB_MUNICIPValidKey(Sender: TObject);
begin
     inherited;
     if CB_MUNICIP.Llave <> VACIO then
     begin
          FCambio := False;
          CB_ESTADO.LLave := dmTablas.cdsMunicipios.FieldByName('TB_ENTIDAD').AsString;
          CB_MUNICIP.Filtro := Format('TB_ENTIDAD = ''%s''',[CB_ESTADO.Llave]);
     end;
end;

end.
