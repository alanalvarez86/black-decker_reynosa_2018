unit FGlobalNiveles_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TGlobalNiveles_DevEx = class(TBaseGlobal_DevEx)
    Label12: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    NombreNivel1: TEdit;
    NombreNivel2: TEdit;
    NombreNivel3: TEdit;
    NombreNivel4: TEdit;
    NombreNivel5: TEdit;
    NombreNivel6: TEdit;
    NombreNivel7: TEdit;
    NombreNivel8: TEdit;
    NombreNivel9: TEdit;
    NIVEL12lbl: TLabel;
    NombreNivel12: TEdit;
    NIVEL11lbl: TLabel;
    NombreNivel11: TEdit;
    NIVEL10lbl: TLabel;
    NombreNivel10: TEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalNiveles_DevEx: TGlobalNiveles_DevEx;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalNiveles_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     NombreNivel10.Visible := True;
     NombreNivel11.Visible := True;
     NombreNivel12.Visible := True;
     NIVEL10lbl.Visible := True;
     NIVEL11lbl.Visible := True;
     NIVEL12lbl.Visible := True;
     {$endif}
     IndexDerechos    := ZAccesosTress.D_CAT_CONFI_AREAS;
     NombreNivel1.Tag := K_GLOBAL_NIVEL1;
     NombreNivel2.Tag := K_GLOBAL_NIVEL2;
     NombreNivel3.Tag := K_GLOBAL_NIVEL3;
     NombreNivel4.Tag := K_GLOBAL_NIVEL4;
     NombreNivel5.Tag := K_GLOBAL_NIVEL5;
     NombreNivel6.Tag := K_GLOBAL_NIVEL6;
     NombreNivel7.Tag := K_GLOBAL_NIVEL7;
     NombreNivel8.Tag := K_GLOBAL_NIVEL8;
     NombreNivel9.Tag := K_GLOBAL_NIVEL9;
     {$ifdef ACS}
     NombreNivel10.Tag := K_GLOBAL_NIVEL10;
     NombreNivel11.Tag := K_GLOBAL_NIVEL11;
     NombreNivel12.Tag := K_GLOBAL_NIVEL12;
     {$endif}
     HelpContext      := H65102_Niveles_organigrama;
     ActualizaDiccion := True;
end;

end.
