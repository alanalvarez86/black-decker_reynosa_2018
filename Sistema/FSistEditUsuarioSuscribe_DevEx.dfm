inherited FormaSuscribeUsuario_DevEx: TFormaSuscribeUsuario_DevEx
  Caption = 'Suscripci'#243'n a Reportes via e-mail'
  ClientHeight = 336
  ClientWidth = 426
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 432
  ExplicitHeight = 365
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 300
    Width = 426
    ExplicitTop = 300
    ExplicitWidth = 426
    inherited OK_DevEx: TcxButton
      Left = 257
      ExplicitLeft = 257
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 337
      ExplicitLeft = 337
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 0
    Width = 426
    Height = 33
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 7
      Width = 39
      Height = 13
      Caption = 'Usuario:'
    end
    object DBText1: TDBText
      Left = 64
      Top = 7
      Width = 25
      Height = 17
      DataField = 'US_CODIGO'
      DataSource = dsUsuario
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText2: TDBText
      Left = 90
      Top = 7
      Width = 244
      Height = 17
      DataField = 'US_NOMBRE'
      DataSource = dsUsuario
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object ZetaCXGrid1: TZetaCXGrid [2]
    Left = 0
    Top = 33
    Width = 426
    Height = 267
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object ZetaCXGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      FilterBox.Visible = fvNever
      DataController.DataSource = dsSuscrip
      DataController.Filter.OnGetValueList = ZetaCXGrid1DBTableView1DataControllerFilterGetValueList
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.GroupByBox = False
      object ZetaCXGrid1DBTableView1RE_NOMBRE: TcxGridDBColumn
        Caption = 'Calendario'
        DataBinding.FieldName = 'CA_NOMBRE'
        Width = 160
      end
      object ZetaCXGrid1DBTableView1REPORTE: TcxGridDBColumn
        Caption = 'Reporte'
        DataBinding.FieldName = 'RE_NOMBRE'
      end
      object ZetaCXGrid1DBTableView1CA_FREC: TcxGridDBColumn
        Caption = 'Frecuencia'
        DataBinding.FieldName = 'CA_FREC'
        Width = 80
      end
    end
    object ZetaCXGrid1Level1: TcxGridLevel
      GridView = ZetaCXGrid1DBTableView1
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object dsUsuario: TDataSource
    DataSet = dmSistema.cdsUsuarios
    Left = 16
    Top = 252
  end
  object dsSuscrip: TDataSource
    Left = 18
    Top = 188
  end
end
