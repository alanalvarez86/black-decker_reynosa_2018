inherited SistTablerosRoles_DevEx: TSistTablerosRoles_DevEx
  Left = 1785
  Top = 100
  ActiveControl = PanelParam
  Caption = 'Tableros de Inicio por Roles de Usuario'
  ClientHeight = 421
  ClientWidth = 1146
  OnDestroy = FormDestroy
  ExplicitWidth = 1146
  ExplicitHeight = 421
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1146
    TabOrder = 2
    Visible = False
    ExplicitWidth = 1146
    inherited Slider: TSplitter
      Left = 323
      Visible = False
      ExplicitLeft = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      Visible = False
      ExplicitWidth = 307
      inherited textoValorActivo1: TLabel
        Width = 301
        Visible = False
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 820
      Visible = False
      ExplicitLeft = 326
      ExplicitWidth = 820
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 814
        Visible = False
        ExplicitLeft = 734
      end
    end
  end
  object PanelParam: TPanel [1]
    Left = 0
    Top = 19
    Width = 1146
    Height = 118
    Align = alTop
    TabOrder = 0
    TabStop = True
    OnEnter = PanelParamEnter
    object Label1: TLabel
      Left = 8
      Top = 11
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa:'
    end
    object Label2: TLabel
      Left = 33
      Top = 65
      Width = 19
      Height = 13
      Alignment = taRightJustify
      Caption = 'Rol:'
    end
    object Label3: TLabel
      Left = 13
      Top = 36
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tablero:'
    end
    object Label4: TLabel
      Left = 16
      Top = 90
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Buscar:'
    end
    object Empresa: TZetaKeyCombo
      Left = 58
      Top = 6
      Width = 367
      Height = 21
      Hint = 'Empresa'
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnChange = EmpresaChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object fBuscar: TcxTextEdit
      Left = 57
      Top = 87
      Hint = 'Buscar por palabras claves'
      ParentShowHint = False
      Properties.OnChange = fBuscarPropertiesChange
      ShowHint = True
      TabOrder = 3
      Width = 300
    end
    object Roles: TZetaKeyLookup_DevEx
      Left = 58
      Top = 60
      Width = 245
      Height = 21
      Hint = 'Rol'
      LookupDataset = dmSistema.cdsRoles
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      ShowHint = True
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
      OnValidKey = RolesValidKey
    end
    object Tablero: TZetaKeyCombo
      Left = 58
      Top = 33
      Width = 244
      Height = 21
      Hint = 'Tablero'
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnChange = TableroChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 137
    Width = 1146
    Height = 284
    ExplicitTop = 137
    ExplicitWidth = 1146
    ExplicitHeight = 284
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CM_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CM_CODIGO'
      end
      object CM_NOMBRE: TcxGridDBColumn
        Caption = 'Empresa'
        DataBinding.FieldName = 'CM_NOMBRE'
      end
      object TableroNombre: TcxGridDBColumn
        Caption = 'Tablero'
        DataBinding.FieldName = 'TableroNombre'
      end
      object RO_CODIGO: TcxGridDBColumn
        Caption = 'Rol'
        DataBinding.FieldName = 'RO_CODIGO'
      end
      object RO_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'RO_NOMBRE'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 304
    Top = 216
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 4195200
  end
  inherited ActionList: TActionList
    Left = 946
  end
  inherited PopupMenu1: TPopupMenu
    Left = 840
    Top = 64
  end
end
