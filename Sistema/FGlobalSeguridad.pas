unit FGlobalSeguridad;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls,
     {$ifndef VER130}Variants,{$endif}     
     ZBaseGlobal, ZetaNumero, ZetaKeyLookup, ZetaCommonClasses;

type
  TGlobalSeguridad = class(TBaseGlobal)
    eExpiracion: TZetaNumero;
    eExpiracionLBL: TLabel;
    eExpiracionPFX: TLabel;
    eLimitePassLBL: TLabel;
    eLimitePass: TZetaNumero;
    eLimitePassPFX: TLabel;
    eIntentosLBL: TLabel;
    eIntentos: TZetaNumero;
    eIntentosPFX: TLabel;
    eDiasInactivosLBL: TLabel;
    eDiasInactivos: TZetaNumero;
    eDiasInactivosPFX: TLabel;
    eTiempoInactivoLBL: TLabel;
    eTiempoInactivo: TZetaNumero;
    eTiempoInactivoPFX: TLabel;
    eMinLetrasPassLBL: TLabel;
    eMinLetrasPassPFX: TLabel;
    eMinLetrasPass: TZetaNumero;
    eMinDigitosPassLBL: TLabel;
    eMinDigitosPass: TZetaNumero;
    eMinDigitosPassPFX: TLabel;
    eAlmacenarPassLBL: TLabel;
    eAlmacenarPass: TZetaNumero;
    eAlmacenarPassPFX: TLabel;
    lblUsuarioTareasAutomaticas: TLabel;       // (JB) Se agrega Configuracion de Tress Automatiza
    eUsuarioTareasAutomaticas: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Cargar; override;
    procedure Descargar; override;
  end;

var
  GlobalSeguridad: TGlobalSeguridad;

implementation

uses ZetaDialogo,
     ZAccesosTress,
     DCliente, dSistema;

const
     K_LIMITE_PASS = 14; {Longitud Maxima del Password}

{$R *.DFM}

procedure TGlobalSeguridad.FormCreate(Sender: TObject);
begin
     inherited;

     with dmSistema do
     begin
          cdsUsuarios.Conectar; // (JB) Se agrega Configuracion de Tress Automatiza
          eUsuarioTareasAutomaticas.LookupDataset  := cdsUsuarios;
     end;

     {$ifdef TRESS}
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_SEGURIDAD;
     {$else}
     IndexDerechos := ZAccesosTress.D_CAT_CONFI_GLOBALES;
     {$endif}
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_GE_SEGURIDAD;
     {$else}
     HelpContext := H65113_Globales_De_Seguridad;
     {$endif}
end;

procedure TGlobalSeguridad.Cargar;
begin
     with dmCliente do
     begin
          GetSeguridad;
          with DatosSeguridad do
          begin
               eExpiracion.Valor := PasswordExpiracion;
               eLimitePass.Valor := PasswordLongitud;
               eMinLetrasPass.Valor := PasswordLetras;
               eMinDigitosPass.Valor := PasswordDigitos;
               eAlmacenarPass.Valor := PasswordLog;
               eIntentos.Valor := PasswordIntentos;
               eDiasInactivos.Valor := Inactividad;
               eTiempoInactivo.Valor := TimeOut;
               eUsuarioTareasAutomaticas.Valor := UsuarioTareasAutomaticas; // (JB) Se agrega Configuracion de Tress Automatiza
          end;
     end;
end;

procedure TGlobalSeguridad.DesCargar;
var
     iLimite: Integer;
begin
     iLimite := eLimitePass.ValorEntero;
     if ( iLimite > K_LIMITE_PASS ) then
     begin
          ZetaDialogo.zError( Caption, Format( 'La Longitud M�xima De La Clave Es De %d Caracteres', [ K_LIMITE_PASS ] ), 0 );
          with eLimitePass do
          begin
               Valor := K_LIMITE_PASS;
               SetFocus;
          end;
     end
     else if ( iLimite < ( eMinLetrasPass.Valor + eMinDigitosPass.Valor ) ) then
     begin
          ZetaDialogo.zError( Caption, Format( 'La Cantidad de Letras y D�gitos debe Ser Igual o Menor que Longitud M�nima de la Clave: ', [ iLimite ] ), 0 );
          eMinLetrasPass.SetFocus;
     end
     else if eUsuarioTareasAutomaticas.Valor < 1 then
     begin
          ZetaDialogo.ZError( Caption, 'Usuario Tareas Autom�ticas no puede quedar vac�o.', 0 );
          eUsuarioTareasAutomaticas.SetFocus;
     end
     else
     begin
          try
             dmCliente.SetSeguridad( VarArrayOf( [ eExpiracion.Valor,                    //SEG_VENCIMIENTO
                                                   eLimitePass.Valor,                    //SEG_LIMITE_PASSWORD
                                                   eIntentos.Valor,                      //SEG_INTENTOS
                                                   eDiasInactivos.Valor,                 //SEG_DIAS_INACTIVOS
                                                   eTiempoInactivo.Valor,                //SEG_TIEMPO_INACTIVO
                                                   eMinLetrasPass.Valor,                 //SEG_MIN_LETRAS_PASSWORD
                                                   eMinDigitosPass.Valor,                //SEG_MIN_DIGITOS_PASSWORD
                                                   eAlmacenarPass.Valor,                 //SEG_MAX_LOG_PASSWORD
                                                   eUsuarioTareasAutomaticas.Valor ] ) ); //SEG_USUARIO_TRESSAUTOMATIZA // (JB) Se agrega Configuracion de Tress Automatiza
             LastAction := K_EDICION_MODIFICACION;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( Caption, 'Error al Escribir Valores De Seguridad', Error, 0 );
                end;
          end;
     end;
end;



end.
