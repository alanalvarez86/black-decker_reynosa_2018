unit FEditSistBaseDatos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, Mask, ZetaNumero, ZetaEdit, ZetaKeyCombo, ZetaBuscador;

type
  TEditSistBaseDatos = class(TBaseEdicion)
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    btCambiarUbicacion: TButton;
    btCambiarUsuario: TButton;
    Label5: TLabel;
    DB_DESCRIP: TDBEdit;
    DB_DATOS: TDBEdit;
    DB_USRNAME: TDBEdit;
    DB_TIPO: TZetaDBKeyCombo;
    DB_CODIGO: TZetaDBEdit;
    Label1: TLabel;
    lblProgramEspecial: TLabel;
    chbEspecial: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btCambiarUbicacionClick(Sender: TObject);
    procedure btCambiarUsuarioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure Connect; override;  
    procedure DoLookup; override;
  end;

var
  EditSistBaseDatos: TEditSistBaseDatos;

implementation

uses dSistema,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseDlgModal,
     FEditUsuarioBaseDatos,
     dCatalogos,
     FEditUbicaBaseDatos, ZetaClientDataSet;

{$R *.dfm}

{ TEditSistBaseDatos }

procedure TEditSistBaseDatos.Connect;
begin
     inherited;
     with dmSistema do
     begin
          DataSource.DataSet := cdsSistBaseDatos;
     end;
end;

procedure TEditSistBaseDatos.FormCreate(Sender: TObject);
var
   eCiclo: eTipoCompany;
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_SIST_BASE_DATOS;
     FirstControl := DB_CODIGO;

     with DB_TIPO.Lista do
     begin
          for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
          begin
               Add( Format( '%d=%s', [ Ord( eCiclo ) , ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo )  ) ] ) );
          end;
     end;

     HelpContext := H_SIST_BASE_DATOS;

end;

procedure TEditSistBaseDatos.btCambiarUbicacionClick(Sender: TObject);
var
   sServidor, sBaseDatos: String;
begin
     with dmSistema.cdsSistBaseDatos do
     begin
          sServidor :=  Copy (FieldByName ('DB_DATOS').AsString, 0, (AnsiPos('.', FieldByName ('DB_DATOS').AsString)-1));
          
          sBaseDatos := Copy (FieldByName ('DB_DATOS').AsString,
                     (AnsiPos('.', FieldByName ('DB_DATOS').AsString)+1),
                     Length (FieldByName ('DB_DATOS').AsString));

          if FEditUbicaBaseDatos.EditarUbicacion (sServidor, sBaseDatos, H_SIST_BASE_DATOS) then
          begin
                 HabilitaControles;
          end;
     end;
end;

procedure TEditSistBaseDatos.btCambiarUsuarioClick(Sender: TObject);
begin
     with dmSistema.cdsSistBaseDatos do
     begin
          if FEditUsuarioBaseDatos.EditarUsuario( FieldByName('DB_USRNAME').AsString, H_SIST_BASE_DATOS) then
          begin
                  HabilitaControles;
          end;
     end;
end;

procedure TEditSistBaseDatos.FormShow(Sender: TObject);
begin
      inherited;    
      if (DB_TIPO.Text = ObtieneElemento( lfTipoCompanyName, Ord( tc3Prueba ) )) then
         DB_TIPO.Enabled := FALSE
      else
         DB_TIPO.Enabled := TRUE;

      // Campo de Programaciones especiales
      chbEspecial.Checked := FALSE;
      if (dmSistema.cdsSistBaseDatos.FieldByName('DB_ESPC').AsString = K_GLOBAL_SI) then
         chbEspecial.Checked := TRUE;
end;

procedure TEditSistBaseDatos.DoLookup;
begin
     inherited;
end;

procedure TEditSistBaseDatos.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;

     with dmSistema.cdsSistBaseDatos do
     begin
          if ( State in [ dsInsert, dsEdit, dsBrowse ] ) then
          begin     
               chbEspecial.Checked := FALSE;
               if (FieldByName('DB_ESPC').AsString = K_GLOBAL_SI) then
                  chbEspecial.Checked := TRUE;
          end;

          if ( State in [ dsBrowse ] ) then
          begin
               if (DB_TIPO.Text = ObtieneElemento( lfTipoCompanyName, Ord( tc3Prueba ) )) then
                  DB_TIPO.Enabled := FALSE
               else
                  DB_TIPO.Enabled := TRUE;
          end
          else if ( Field = nil ) and (State = dsInsert) then
               DB_TIPO.Enabled := TRUE;
     end;
end;

procedure TEditSistBaseDatos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      if dmSistema.ActualizarBD then
      begin
         dmSistema.cdsSistBaseDatos.Refrescar;
         dmSistema.ActualizarBD := FALSE;
      end;
end;

end.
