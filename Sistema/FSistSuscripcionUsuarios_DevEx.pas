unit FSistSuscripcionUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
  ZetaCommonTools, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList, cxButtons, cxControls, cxContainer, cxEdit,
  cxCheckListBox, cxTextEdit;

type
  TSistSuscripcionUsuarios_DevEx = class(TZetaDlgModal_DevEx)
    Lista: TcxCheckListBox;
    Apagar: TcxButton;
    Prender: TcxButton;
    Label1: TLabel;
    BuscaBtn: TcxButton;
    EditBusca: TEdit;
    PanelBusqueda: TPanel;
    chkActivos: TCheckBox;
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    textoValorActivo1: TLabel;
    ValorActivo2: TPanel;
    textoValorActivo2: TLabel;
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CheckCLick;
    procedure ListaClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
    procedure BuscaBtnClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure chkActivosClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroSuper: Boolean;
  public
    { Public declarations }
  end;

var
  SistSuscripcionUsuarios_DevEx: TSistSuscripcionUsuarios_DevEx;

implementation

uses ZetaCommonClasses,
     ZetaDialogo,
     DSistema,
     dCatalogos,
     FSistCalendarioReportes_DevEx,
     DCliente;

{$R *.dfm}

{ TSeleccionarConfidencialidad }

procedure TSistSuscripcionUsuarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TSistSuscripcionUsuarios_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TSistSuscripcionUsuarios_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsSistTareaUsuario.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     chkActivos.Checked := True;
     Cargar;
     OK_DevEx.Enabled := False;
     chkActivos.Enabled := True;
     ActiveControl := Lista;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;
     if  dmSistema.EnvioProgramadoXEmpresa = true then
     begin
         textoValorActivo1.Caption := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CA_NOMBRE').AsString;
         textoValorActivo2.Caption := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName('CM_NOMBRE').AsString;
     end
     else
     begin
         textoValorActivo1.Caption := dmSistema.cdsSistTareaCalendario.FieldByName('CA_NOMBRE').AsString;
         textoValorActivo2.Caption := dmSistema.cdsSistTareaCalendario.FieldByName('CM_NOMBRE').AsString;
     end;
end;

procedure TSistSuscripcionUsuarios_DevEx.ListaClickCheck(Sender: TObject; AIndex: Integer; APrevState, ANewState: TcxCheckBoxState);
begin
     inherited;
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;

     if Ok_DevEx.Enabled = True then
        chkActivos.Enabled := False;
end;

procedure TSistSuscripcionUsuarios_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TSistSuscripcionUsuarios_DevEx.Cargar;
var
   i: Integer;
begin
     dmSistema.CargaListaUsuariosCalendario( FLista, chkActivos.Checked );
     dmSistema.cdsSistTareaUsuario.Refrescar;
     with Lista do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       with Items.Add do
                       begin
                            text := ( FLista[i] );
                            Checked := False;

                            dmSistema.cdsSistTareaUsuario.First;
                            while not dmSistema.cdsSistTareaUsuario.Eof do
                            begin
                                 if Trim(Names[i]) = dmSistema.cdsSistTareaUsuario.FieldByName('US_CODIGO').AsString then
                                      Checked := zStrToBool( dmSistema.cdsSistTareaUsuario.FieldByName('RU_ACTIVO').AsString );

                                 dmSistema.cdsSistTareaUsuario.Next;
                            end;
                       end
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;


procedure TSistSuscripcionUsuarios_DevEx.Descargar;
var
   i: Integer;
begin
     with Lista do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               with items[i] do
               begin
                    if Checked then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaUsuariosCalendario(FLista );
end;

function TSistSuscripcionUsuarios_DevEx.EncontroSuper: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Lista do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i].text ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TSistSuscripcionUsuarios_DevEx.BuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Lista.Count )then
     begin
          lEncontro:= EncontroSuper;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Lista.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de Usuarios', '� No hay otro usuario con esos datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de Usuarios', '� No hay un usuario con esos datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

procedure TSistSuscripcionUsuarios_DevEx.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Lista do
     begin
     items.BeginUpdate;
        try
            for i := 0 to (items.Count - 1 ) do
            begin
                with Items[i] do
                begin
                    Checked := lState;
                end;
            end;
        finally
            items.EndUpdate;
        end;
     end;
     CheckCLick;
end;

procedure TSistSuscripcionUsuarios_DevEx.CheckCLick;
begin
     with OK_DevEx do
     begin
          if not Enabled then
             Enabled := True;
     end;
end;

procedure TSistSuscripcionUsuarios_DevEx.chkActivosClick(Sender: TObject);
begin
     inherited;
     Cargar;
end;

procedure TSistSuscripcionUsuarios_DevEx.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TSistSuscripcionUsuarios_DevEx.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

end.
