inherited FormaEditSuscrip_DevEx: TFormaEditSuscrip_DevEx
  Left = 416
  Top = 240
  Caption = 'Propiedades de Suscripci'#243'n'
  ClientHeight = 203
  ClientWidth = 334
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 28
    Top = 24
    Width = 41
    Height = 13
    Caption = 'Reporte:'
  end
  object Label2: TLabel [1]
    Left = 13
    Top = 60
    Width = 56
    Height = 13
    Caption = 'Frecuencia:'
  end
  object Label3: TLabel [2]
    Left = 33
    Top = 96
    Width = 36
    Height = 13
    Caption = 'Recibir:'
  end
  object Label4: TLabel [3]
    Left = 22
    Top = 124
    Width = 47
    Height = 26
    Alignment = taRightJustify
    Caption = 'Si no hay datos:'
    WordWrap = True
  end
  inherited PanelBotones: TPanel
    Top = 167
    Width = 334
    inherited OK_DevEx: TcxButton
      Left = 166
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 246
      OnClick = Cancelar_DevExClick
    end
  end
  object RE_NOMBRE: TZetaDBEdit [5]
    Left = 76
    Top = 20
    Width = 249
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 1
    Text = 'RE_NOMBRE'
    DataField = 'RE_NOMBRE'
    DataSource = dsSuscrip
  end
  object SU_FRECUEN: TZetaDBKeyCombo [6]
    Left = 76
    Top = 56
    Width = 140
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 2
    ListaFija = lfEmailFrecuencia
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'SU_FRECUEN'
    DataSource = dsSuscrip
    LlaveNumerica = True
  end
  object SU_ENVIAR: TZetaDBKeyCombo [7]
    Left = 76
    Top = 92
    Width = 140
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 3
    Items.Strings = (
      'Listado'
      'Gr'#225'fica'
      'Listado y Gr'#225'fica')
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'SU_ENVIAR'
    DataSource = dsSuscrip
    LlaveNumerica = True
  end
  object SU_VACIO: TZetaDBKeyCombo [8]
    Left = 76
    Top = 128
    Width = 140
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 4
    Items.Strings = (
      'Enviar Notificaci'#243'n'
      'No Enviar Reporte')
    ListaFija = lfEmailNotificacion
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'SU_VACIO'
    DataSource = dsSuscrip
    LlaveNumerica = True
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object dsSuscrip: TDataSource
    DataSet = dmSistema.cdsSuscrip
    Left = 272
    Top = 52
  end
end
