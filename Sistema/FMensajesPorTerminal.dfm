inherited MensajesPorTerminal: TMensajesPorTerminal
  Left = 276
  Top = 153
  Caption = 'Mensajes por Terminal'
  ClientWidth = 851
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 851
    inherited ValorActivo2: TPanel
      Width = 592
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 851
    Height = 38
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 18
      Top = 12
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terminal:'
    end
    object luTerminal: TZetaKeyLookup
      Left = 65
      Top = 8
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = luTerminalValidKey
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [2]
    Left = 0
    Top = 57
    Width = 851
    Height = 216
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DM_CODIGO'
        Title.Caption = 'ID Mensaje'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DM_MENSAJE'
        Title.Caption = 'Mensaje'
        Width = 350
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TM_SLEEP'
        Title.Caption = 'Espera'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TM_NEXT'
        Title.Caption = 'Siguiente mensaje'
        Width = 150
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TM_ULTIMA'
        Title.Caption = #218'ltima Ejecuci'#243'n'
        Width = 150
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 296
    Top = 144
  end
end
