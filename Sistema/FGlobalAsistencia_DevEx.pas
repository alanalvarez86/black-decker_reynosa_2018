unit FGlobalAsistencia_DevEx;

interface                                                                                            

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, ComCtrls,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaDBTextBox, ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ZetaKeyLookup_DevEx, ImgList,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxPC;

{$define QUINCENALES}
{.$undefine QUINCENALES}
     
type
  TGlobalAsistencia_DevEx = class(TBaseGlobal_DevEx)
    PageControl: TcxPageControl;
    Generales: TcxTabSheet;
    ExtrasYFestivos: TcxTabSheet;
    Configuracion: TGroupBox;
    PrimerDia: TZetaKeyCombo;
    PrimerDiaLBL: TLabel;
    SumarJornadas: TCheckBox;
    GraciaRepetidas: TZetaNumero;
    GraciaRepetidasLBL: TLabel;
    NoChecan: TCheckBox;
    Redondeos: TGroupBox;
    RedondeoOrdinarias: TZetaKeyLookup_DevEx;
    RedondeoOrdinariasLBL: TLabel;
    RedondeoExtras: TZetaKeyLookup_DevEx;
    RedondeoExtrasLBL: TLabel;
    TratamientoHorasExtras: TGroupBox;
    HorasSabados: TZetaKeyCombo;
    HorasSabadosLBL: TLabel;
    HorasDescansos: TZetaKeyCombo;
    HorasDescansosLBL: TLabel;
    HorasFestivos: TZetaKeyCombo;
    HorasFestivosLBL: TLabel;
    NoChecanUsarJornadas: TCheckBox;
    CapturaObligatoriadeMotivo: TGroupBox;
    HrsExtras: TCheckBox;
    DescansoTrabajado: TCheckBox;
    PermisoCGoce: TCheckBox;
    PermisoCGentrada: TCheckBox;
    PermisoSGoce: TCheckBox;
    PermisoSGEntrada: TCheckBox;
    AutDesctoComidas: TCheckBox;
    Tarjetas: TcxTabSheet;
    ProximidadGB: TGroupBox;
    CodeSizeLBL: TLabel;
    FacilityCodeLBL: TLabel;
    CodeSize: TZetaNumero;
    FacilityCode: TEdit;
    Label1: TLabel;
    TopeDoblesSemanal: TCheckBox;
    TratamientoTELBL: TLabel;
    TratamientoTE: TZetaKeyCombo;
    AutorizacionExtrasYDescansos: TGroupBox;
    ExtraDescansoSab: TCheckBox;
    ExtraDescansoDes: TCheckBox;
    ExtraDescansoFes: TCheckBox;
    MotivodeCapturaManualChecadas: TGroupBox;
    PermitirCaptura: TCheckBox;
    CapturaObligatoria: TCheckBox;
    CredencialesInvalidas: TCheckBox;
    BloqueoPreNominas: TCheckBox;
    IncidenciaFestivoDescansos: TCheckBox;
    CBSalarioTemporal: TCheckBox;
    GroupBox1: TGroupBox;
    Regla3x3lbl: TLabel;
    Regla3x3: TZetaKeyCombo;
    DiasExento: TZetaKeyCombo;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    AplicarPlanVaca: TCheckBox;
    Label3: TLabel;
    Label4: TLabel;
    PagoPrimaVacacional: TCheckBox;
    CBDiasSinFaltasVac: TCheckBox;
    NumPerAnticipado: TZetaNumero;
    Panel1: TPanel;
    AutorizacionPago: TGroupBox;
    HorasExtras: TCheckBox;
    DiasFestivos: TCheckBox;
    GroupBox3: TGroupBox;
    AprobarAutorizacion: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DiasFestivosClick(Sender: TObject);
    procedure HorasExtrasClick(Sender: TObject);
    procedure PermitirCapturaClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalAsistencia_DevEx: TGlobalAsistencia_DevEx;

implementation

uses DTablas,
     DCliente,
     DGlobal,
     FAutoClasses,
     ZAccesosTress,
     ZGlobalTress,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists;

{$R *.DFM}

procedure TGlobalAsistencia_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos            := ZAccesosTress.D_CAT_CONFI_ASISTENCIA;
{
     ER: 2.4.97 - Ya no se utiliza este Global, se guarda en COMPANY.CM_DIGITO
     DigitoGafetes.Tag        := K_GLOBAL_DIGITO_EMPRESA;
}
     PrimerDia.Tag            := K_GLOBAL_PRIMER_DIA;
     GraciaRepetidas.Tag      := K_GLOBAL_GRACIA_2CHECK;
     {$ifdef QUINCENALES}
     TopeDoblesSemanal.Tag    := K_GLOBAL_TOPE_SEMANAL_DOBLES;
     SumarJornadas.Visible    := FALSE;
     {$else}
     SumarJornadas.Tag        := K_GLOBAL_SUMAR_JORNADAS;
     {$endif}
     
     HorasExtras.Tag          := K_GLOBAL_AUT_X;
     DiasFestivos.Tag         := K_GLOBAL_AUT_FESTIVO_TRAB;
     ExtraDescansoFes.Tag     := K_GLOBAL_EXTRA_DESCANSO;
     with RedondeoOrdinarias do
     begin
          LookupDataset       := dmTablas.cdsNumericas;
          Tag                 := K_GLOBAL_TAB_REDON_ORD;
     end;
     with RedondeoExtras do
     begin
          LookupDataset       := dmTablas.cdsNumericas;
          Tag                 := K_GLOBAL_TAB_REDON_X;
     end;
     HorasSabados.Tag         := K_GLOBAL_TRATO_X_SAB;
     HorasDescansos.Tag       := K_GLOBAL_TRATO_X_DESC;
     HorasFestivos.Tag        := K_GLOBAL_TRATO_X_FESTIVO;
     TratamientoTE.Tag        := K_GLOBAL_TRATAMIENTO_TIEMPO_EXTRA;
     NoChecan.Tag             := K_EXTRAS_NO_CHECAN;
     NoChecanUsarJornadas.Tag := K_GLOBAL_NO_CHECA_USAR_JORNADAS;
     Regla3x3.Tag             := K_GLOBAL_HRS_EXENTAS_IMSS;
     DiasExento.Tag           := K_GLOBAL_DIAS_APLICA_EXENTO;
     HrsExtras.Tag            := K_GLOBAL_VALIDA_HRS_EXTRAS;
     DescansoTrabajado.Tag    := K_GLOBAL_VALIDA_DESC_TRAB;
     PermisoCGoce.Tag         := K_GLOBAL_VALIDA_PERM_CGOCE;
     PermisoCGentrada.Tag     := K_GLOBAL_VALIDA_PERM_CGENT;
     PermisoSGoce.Tag         := K_GLOBAL_VALIDA_PERM_SGOCE;
     PermisoSGEntrada.Tag     := K_GLOBAL_VALIDA_PERM_SGENT;
     AutDesctoComidas.Tag     := K_GLOBAL_DESCONTAR_COMIDAS;
     AprobarAutorizacion.Tag  := K_GLOBAL_APROBAR_AUTORIZACIONES;
     AplicarPlanVaca.Tag      := K_GLOBAL_PLAN_VACA_PRENOMINA;
     PagoPrimaVacacional.Tag  := K_GLOBAL_PAGO_PRIMA_VACA_ANIV;

     CredencialesInvalidas.Tag:= K_GLOBAL_PROCESA_CRED_INVALIDAS;
     CBDiasSinFaltasVac.Tag   := K_GLOBAL_DIAS_COTIZADOS_FV;

     NumPerAnticipado.Tag     := K_GLOBAL_PAGO_PERIODO_ANTC;

     CodeSize.Tag             := K_GLOBAL_PROXIMITY_TOTAL_BYTES;
     FacilityCode.Tag         := K_GLOBAL_PROXIMITY_FACILITY_CODE;

     HelpContext              := H65106_Asistencia;

     ExtraDescansoSab.Tag     := K_GLOBAL_EXTRA_DESCANSO_SAB;
     ExtraDescansoDes.Tag     := K_GLOBAL_EXTRA_DESCANSO_DES;

     PermitirCaptura.Tag      := K_GLOBAL_PERMITIR_CAPTURA;
     CapturaObligatoria.Tag   := K_GLOBAL_CAPTURA_OBLIGATORIA;

     BloqueoPreNominas.Tag    := K_GLOBAL_BLOQUEO_PRENOM_AUT;

     {V2013 Mejoras configuraci�n conceptos de n�mina}
     IncidenciaFestivoDescansos.Tag := K_GLOBAL_FESTIVO_EN_DESCANSOS;
     CBSalarioTemporal.Tag := K_GLOBAL_SALARIO_TEMP;

end;

procedure TGlobalAsistencia_DevEx.FormShow(Sender: TObject);
var
   lAsistencia: Boolean;
begin
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          cdsNumericas.Conectar;
     end;
     inherited;
     PageControl.ActivePage := Generales;
     lAsistencia := dmCliente.ModuloAutorizado( okAsistencia );
     {
     DigitoGafetes.Enabled := lAsistencia;
     DigitoGafetesLBL.Enabled := lAsistencia;
     }
     GraciaRepetidas.Enabled := lAsistencia;
     GraciaRepetidasLBL.Enabled := lAsistencia;
     {$ifdef QUINCENALES}
     TopeDoblesSemanal.Enabled := lAsistencia;
     {$else}
     SumarJornadas.Enabled := lAsistencia;
     {$endif}
     HorasExtras.Enabled := lAsistencia;
     DiasFestivos.Enabled := lAsistencia;
     RedondeoOrdinarias.Enabled := lAsistencia;
     RedondeoOrdinariasLBL.Enabled := lAsistencia;
     RedondeoExtras.Enabled := lAsistencia;
     RedondeoExtrasLBL.Enabled := lAsistencia;
     HorasSabados.Enabled := lAsistencia;
     HorasSabadosLBL.Enabled := lAsistencia;
     HorasDescansos.Enabled := lAsistencia;
     HorasDescansosLBL.Enabled := lAsistencia;
     HorasFestivos.Enabled := lAsistencia;
     HorasFestivosLBL.Enabled := lAsistencia;
     TratamientoTE.Enabled := lAsistencia;
     TratamientoTELBL.Enabled := lAsistencia;
     NoChecan.Enabled := lAsistencia;
     NoChecanUsarJornadas.Enabled := lAsistencia;
     HrsExtras.Enabled := lAsistencia;
     DescansoTrabajado.Enabled := lAsistencia;
     PermisoCGoce.Enabled := lAsistencia;
     PermisoCGentrada.Enabled := lAsistencia;
     PermisoSGoce.Enabled := lAsistencia;
     PermisoSGEntrada.Enabled := lAsistencia;
     AutDesctoComidas.Enabled := lAsistencia;
     AprobarAutorizacion.Enabled := lAsistencia;
     ExtraDescansoFes.Enabled := ( lAsistencia and DiasFestivos.Checked );
     ExtraDescansoSab.Enabled := ( lAsistencia and HorasExtras.Checked );
     ExtraDescansoDes.Enabled := ExtraDescansoSab.Enabled;
     CapturaObligatoria.Enabled := PermitirCaptura.Checked;
     // (JB) Se agrega opcion de procesar credenciales Invalidas
     CredencialesInvalidas.Enabled := lAsistencia;
     CBDiasSinFaltasVac.Enabled := lAsistencia;
     BloqueoPreNominas.Enabled := lAsistencia;
     with CodeSize do
     begin
          if ( ValorEntero = 0 ) then
             Valor := 6;
     end;                
     {V2013 Mejoras configuraci�n conceptos de n�mina}
     IncidenciaFestivoDescansos.Enabled := lAsistencia;
     Pagecontrol.ActivePage:=Generales;
end;

procedure TGlobalAsistencia_DevEx.DiasFestivosClick(Sender: TObject);
begin
     inherited;
     ExtraDescansoFes.Enabled:= ( DiasFestivos.Enabled and DiasFestivos.Checked );
end;

procedure TGlobalAsistencia_DevEx.HorasExtrasClick(Sender: TObject);
begin
     inherited;
     ExtraDescansoSab.Enabled:= ( HorasExtras.Enabled and HorasExtras.Checked );
     ExtraDescansoDes.Enabled:= ( HorasExtras.Enabled and HorasExtras.Checked );
end;

procedure TGlobalAsistencia_DevEx.PermitirCapturaClick(Sender: TObject);
begin
     inherited;
     CapturaObligatoria.Enabled := PermitirCaptura.Checked;
     if NOT PermitirCaptura.Checked then     
        CapturaObligatoria.Checked := FALSE;
end;

procedure TGlobalAsistencia_DevEx.OK_DevExClick(Sender: TObject);
begin
 if ( Length( FacilityCode.Text ) > ( CodeSize.ValorEntero - 1 ) ) then
     begin
          ZetaDialogo.zError( '� Error En Prefijo !', 'La Longitud Del Prefijo Debe Ser Menor Que La Longitud Del C�digo', 0 );
          PageControl.ActivePage := Tarjetas;
          Application.ProcessMessages;
          ActiveControl := FacilityCode;
     end
     else
          if ( NumPerAnticipado.ValorEntero  < 0 ) then
          begin
               ZetaDialogo.zError( '� Error en aplicar plan de vacaciones!', 'El n�mero de per�odo anticipado para revisar plan de vacaciones, debe ser mayor o igual a cero', 0 );
               PageControl.ActivePage := Generales;
               Application.ProcessMessages;
               ActiveControl := NumPerAnticipado;
          end
          else
          inherited;

end;

end.
