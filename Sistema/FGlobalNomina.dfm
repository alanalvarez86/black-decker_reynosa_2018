inherited GlobalNomina: TGlobalNomina
  Left = 336
  Top = 173
  Caption = 'N'#243'mina'
  ClientHeight = 483
  ClientWidth = 543
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 447
    Width = 543
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 380
    end
    inherited Cancelar: TBitBtn
      Left = 460
    end
  end
  object FormatosGB: TGroupBox
    Left = 0
    Top = 5
    Width = 458
    Height = 61
    Caption = ' Formatos '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 543
    Height = 447
    ActivePage = Configuracion
    Align = alClient
    TabOrder = 0
    object Configuracion: TTabSheet
      Caption = 'Con&figuraci'#243'n'
      object Label45: TLabel
        Left = 150
        Top = 20
        Width = 79
        Height = 13
        Caption = 'Redondeo Neto:'
      end
      object lblSubsidio: TLabel
        Left = 61
        Top = 179
        Width = 168
        Height = 13
        Alignment = taRightJustify
        Caption = 'Porcentaje de Subsidio Acreditable:'
      end
      object Label31: TLabel
        Left = 139
        Top = 202
        Width = 90
        Height = 13
        Caption = 'Banca Electr'#243'nica:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 75
        Top = 285
        Width = 154
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de pr'#233'stamo de FONACOT:'
      end
      object Label12: TLabel
        Left = 48
        Top = 309
        Width = 181
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero L'#237'mite de N'#243'minas Ordinarias:'
      end
      object Label14: TLabel
        Left = 55
        Top = 327
        Width = 174
        Height = 13
        Alignment = taRightJustify
        Caption = 'Empleados con Ajuste de ISR Anual:'
      end
      object bAjusteISR: TSpeedButton
        Left = 488
        Top = 327
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = bAjusteISRClick
      end
      object Label13: TLabel
        Left = 147
        Top = 393
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'M'#233'todo de Pago:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object RedondeoNeto: TZetaNumero
        Tag = 39
        Left = 232
        Top = 16
        Width = 121
        Height = 21
        Mascara = mnPesosDiario
        TabOrder = 0
        Text = '0.00'
      end
      object SaldoRedondeo: TCheckBox
        Left = 97
        Top = 40
        Width = 148
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Llevar Saldo de Redondeo:'
        TabOrder = 1
      end
      object PromediarCambios: TCheckBox
        Left = 79
        Top = 58
        Width = 166
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Promediar Salarios en Cambios:'
        TabOrder = 2
      end
      object PromediarMinimos: TCheckBox
        Tag = 55
        Left = 95
        Top = 78
        Width = 150
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Promediar Salarios M'#237'nimos:'
        TabOrder = 3
      end
      object SepararCredito: TCheckBox
        Tag = 54
        Left = 106
        Top = 97
        Width = 139
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Separar Cr'#233'dito al Salario:'
        TabOrder = 4
      end
      object SubsidioAcreditable: TZetaNumero
        Tag = 43
        Left = 232
        Top = 175
        Width = 69
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 6
        Text = '0.00'
      end
      object BancaElectronica: TZetaKeyCombo
        Tag = 45
        Left = 232
        Top = 199
        Width = 193
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 7
        ListaFija = lfBancaElectronica
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object rbPrimaDominical: TGroupBox
        Left = 23
        Top = 113
        Width = 339
        Height = 57
        Caption = ' Prima Dominical '
        TabOrder = 5
        object PrimaDominical: TCheckBox
          Tag = 54
          Left = 37
          Top = 16
          Width = 184
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Considera S'#243'lo Horas de Domingo:'
          TabOrder = 0
        end
        object PrimaDominicalTipoDia: TCheckBox
          Tag = 54
          Left = 22
          Top = 34
          Width = 199
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Calcular Para Todos los Tipos de D'#237'a:'
          TabOrder = 1
        end
      end
      object FONACOT: TZetaKeyLookup
        Tag = 50
        Left = 232
        Top = 282
        Width = 279
        Height = 21
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        WidthLlave = 60
      end
      object edtLimiteNominasOrdinarias: TZetaNumero
        Left = 232
        Top = 304
        Width = 69
        Height = 21
        Mascara = mnMinutos
        TabOrder = 10
        Text = '0'
      end
      object GroupBox1: TGroupBox
        Left = 24
        Top = 222
        Width = 337
        Height = 57
        Caption = ' Finiquitos '
        TabOrder = 8
        object Label8: TLabel
          Left = 6
          Top = 14
          Width = 198
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero de per'#237'odo para simular finiquitos:'
        end
        object SimulacionFiniquitos: TZetaNumero
          Tag = 43
          Left = 209
          Top = 10
          Width = 69
          Height = 21
          Mascara = mnMinutos
          TabOrder = 0
          Text = '0'
        end
        object SimFiniquitosAprobacion: TCheckBox
          Tag = 242
          Left = 11
          Top = 34
          Width = 211
          Height = 17
          Alignment = taLeftJustify
          Caption = #191' Requiere Aprobaci'#243'n para Aplicarse ?:'
          TabOrder = 1
        end
      end
      object mAjusteISR: TMemo
        Left = 232
        Top = 327
        Width = 252
        Height = 59
        Lines.Strings = (
          ''
          'COLABORA.CB_FEC_ING <= FirstDayOfYear()')
        ScrollBars = ssVertical
        TabOrder = 11
      end
      object MetodoPago: TZetaKeyCombo
        Tag = 310
        Left = 232
        Top = 390
        Width = 193
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 12
        ListaFija = lfMetodoPagoSAT
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
    object Conceptos: TTabSheet
      Caption = '&Conceptos '
      object ISPTlbl: TLabel
        Left = 89
        Top = 17
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'I.S.P.T.:'
      end
      object CreditoLbl: TLabel
        Left = 46
        Top = 39
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cr'#233'dito al Salario:'
      end
      object AjsuteMonedaLBL: TLabel
        Left = 39
        Top = 61
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ajuste de Moneda:'
      end
      object DeduccLbl: TLabel
        Left = 4
        Top = 83
        Width = 124
        Height = 13
        Alignment = taRightJustify
        Caption = 'Deducciones Incobrables:'
      end
      object Label10: TLabel
        Left = 20
        Top = 104
        Width = 108
        Height = 33
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Tope para pr'#233'stamos y ahorros:'
        WordWrap = True
      end
      object ConceptoISPT: TZetaKeyLookup
        Tag = 50
        Left = 138
        Top = 13
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
      end
      object ConceptoCreditoSalario: TZetaKeyLookup
        Tag = 51
        Left = 138
        Top = 35
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object ConceptoAjusteMoneda: TZetaKeyLookup
        Tag = 52
        Left = 138
        Top = 57
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
      object ConceptoDeduccionesIncobrables: TZetaKeyLookup
        Tag = 53
        Left = 138
        Top = 79
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
      end
      object ConceptoTopePrestamos: TZetaKeyLookup
        Left = 138
        Top = 104
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'C&ontrol de Recibos'
      object Label2: TLabel
        Left = 140
        Top = 10
        Width = 163
        Height = 13
        Caption = 'Default: Recibos Fueron Pagados:'
      end
      object lbConceptosTempEnRecibos: TLabel
        Left = 8
        Top = 26
        Width = 295
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Imprimir Conceptos Tipo Resultados/Captura en Recibos:'
      end
      object RecibosPagados: TCheckBox
        Left = 306
        Top = 8
        Width = 17
        Height = 17
        TabOrder = 0
      end
      object Recibos: TGroupBox
        Left = 0
        Top = 255
        Width = 535
        Height = 164
        Align = alBottom
        Caption = ' Monto Adicional de Consulta '
        TabOrder = 2
        object Label3: TLabel
          Left = 15
          Top = 26
          Width = 31
          Height = 13
          Caption = 'T'#237'tulo:'
        end
        object SBCO_FORMULA: TSpeedButton
          Left = 432
          Top = 48
          Width = 25
          Height = 25
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          OnClick = SBCO_FORMULAClick
        end
        object Label4: TLabel
          Left = 6
          Top = 52
          Width = 40
          Height = 13
          Caption = 'F'#243'rmula:'
        end
        object MontoAdicional: TEdit
          Left = 48
          Top = 24
          Width = 121
          Height = 21
          TabOrder = 0
          Text = 'MontoAdicional'
        end
        object FormulaAdicional: TMemo
          Left = 48
          Top = 48
          Width = 380
          Height = 110
          ScrollBars = ssBoth
          TabOrder = 1
        end
      end
      object cbConceptosTempEnRecibos: TCheckBox
        Left = 306
        Top = 24
        Width = 17
        Height = 17
        Hint = 
          'Permite Incluir Conceptos de tipo "Resultados" y "De Captura" en' +
          ' Recibos y Listados de N'#243'mina'
        TabOrder = 1
      end
    end
    object Aguinaldo: TTabSheet
      Caption = '&Aguinaldo'
      ImageIndex = 3
      object Label5: TLabel
        Left = 42
        Top = 9
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descontar Faltas:'
      end
      object sbFaltas: TSpeedButton
        Left = 440
        Top = 5
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = sbFaltasClick
      end
      object Label6: TLabel
        Left = 0
        Top = 104
        Width = 125
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descontar Incapacidades:'
      end
      object sbIncapacidades: TSpeedButton
        Left = 440
        Top = 100
        Width = 25
        Height = 25
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        OnClick = sbIncapacidadesClick
      end
      object FormulaFaltas: TMemo
        Left = 128
        Top = 5
        Width = 308
        Height = 91
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object FormulaIncapacidades: TMemo
        Left = 128
        Top = 100
        Width = 308
        Height = 91
        ScrollBars = ssVertical
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&Piramidaci'#243'n'
      ImageIndex = 4
      object Label7: TLabel
        Left = 48
        Top = 42
        Width = 115
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tolerancia piramidaci'#243'n:'
      end
      object Label1: TLabel
        Left = 100
        Top = 19
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Piramidaci'#243'n:'
      end
      object Label9: TLabel
        Left = 11
        Top = 65
        Width = 152
        Height = 13
        Caption = 'C'#225'lculo de Pr'#233'stamos y Ahorros:'
      end
      object ToleranciaPiramidacion: TZetaNumero
        Tag = 43
        Left = 169
        Top = 38
        Width = 69
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 0
        Text = '0.00'
      end
      object ConceptoPiramidacion: TZetaKeyLookup
        Tag = 53
        Left = 170
        Top = 15
        Width = 300
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
      end
      object PiramidacionPrestamos: TZetaKeyCombo
        Left = 168
        Top = 61
        Width = 169
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 2
        ListaFija = lfPiramidacionPrestamos
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
    end
  end
end
