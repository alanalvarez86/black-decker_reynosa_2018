unit FGlobalReportesViaEmail;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     ZBaseGlobal,
     ZetaNumero, ZetaKeyCombo;

type
  TGlobalReportesViaEmail = class(TBaseGlobal)
    UserId: TEdit;
    Plantilla: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Buscar: TSpeedButton;
    Label6: TLabel;
    UserEmail: TEdit;
    Label7: TLabel;
    DisplayName: TEdit;
    Label8: TLabel;
    ErrorEmail: TEdit;
    Host: TEdit;
    OpenDialog: TOpenDialog;
    Label4: TLabel;
    EmailPswd: TEdit;
    Label5: TLabel;
    PuertoSMTP: TZetaNumero;
    PrimerDiaLBL: TLabel;
    Autenticacion: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalReportesViaEmail: TGlobalReportesViaEmail;

implementation

uses DGlobal,
     ZReportTools,
     ZReportToolsConsts,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalReportesViaEmail.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos  := ZAccesosTress.D_CAT_CONFI_REP_EMAIL;
     Host.Tag       := K_GLOBAL_EMAIL_HOST;
     PuertoSMTP.Tag := K_GLOBAL_EMAIL_PORT;
     UserId.Tag     := K_GLOBAL_EMAIL_USERID;
     EmailPswd.Tag  := K_GLOBAL_EMAIL_PSWD;
     UserEmail.Tag  := K_GLOBAL_EMAIL_FROMADRESS;
     DisplayName.Tag:= K_GLOBAL_EMAIL_FROMNAME;
     ErrorEMail.Tag  := K_GLOBAL_EMAIL_MSGERROR;
     Plantilla.Tag  := K_GLOBAL_EMAIL_PLANTILLA;
     Autenticacion.Tag := K_GLOBAL_EMAIL_AUTH;

     HelpContext := H65112_Globales_de_reportes_email;
end;

procedure TGlobalReportesViaEmail.BuscarClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          InitialDir := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
          FileName := StrDef(Plantilla.Text, K_TEMPLATE_HTM);
          if Execute then
          begin
               if UpperCase(ExtractFileDir(FileName)) = UpperCase(InitialDir) then
                  Plantilla.Text := ExtractFileName(FileName)
               else
                  Plantilla.Text := FileName;
          end;
     end;

end;

procedure TGlobalReportesViaEmail.FormShow(Sender: TObject);
begin
  inherited;
  with Autenticacion do
  begin
       if ItemIndex < 0 then
          ItemIndex := 0;
  end;
end;

end.
