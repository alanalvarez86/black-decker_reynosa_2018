unit FSistGrupos_DevEx;

interface                                       

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, Db, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, Buttons, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxButtons, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TSistGrupos_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    Grupos: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure GruposClick(Sender: TObject);
    procedure ZetaDBGridxDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
  private
    { Private declarations }
    //function ChecaGrupoSinRestriccion( const sAccion: string; var sMensaje: string ): boolean;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }

  end;

var
  SistGrupos_DevEx: TSistGrupos_DevEx;

implementation

{$R *.DFM}


uses DCliente,
     DSistema,
     ZetaCommonClasses,
     ZBaseDlgModal_DevEx,
     ZetaDialogo,
     FSistArbolGrupos_DevEx;

const
     K_MENSAJE_GRUPOS = 'ˇ Solo Usuarios del Grupo Administradores ' + CR_LF +
                        'Pueden %s Grupos de Usuarios !';
     K_MENSAJE_GRUPOS_RDD = 'No se puede %s Grupo de Usuarios desde esta Aplicación';
     K_MODIFICAR = 'Modificar';
     
{ ********** TSistGrupos ********* }

procedure TSistGrupos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef RDDAPP}
     Grupos.Visible := FALSE;
     {$else}
     {$endif}
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_CONS_GPO_USR;
     {$else}
     HelpContext := H80812_Grupos_usuarios;
     {$endif}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_SISTEMA_GPOUSUARIOS;
     {$endif}

end;

procedure TSistGrupos_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          DataSource.DataSet := cdsGrupos;
          Grupos.Enabled := Not ( cdsGrupos.IsEmpty );
     end;
end;

procedure TSistGrupos_DevEx.Refresh;
begin
     dmSistema.cdsGrupos.Refrescar;
end;

procedure TSistGrupos_DevEx.Agregar;
begin
     dmSistema.cdsGrupos.Agregar;
end;

procedure TSistGrupos_DevEx.Borrar;
begin
     dmSistema.cdsGrupos.Borrar
end;

procedure TSistGrupos_DevEx.Modificar;
begin
     dmSistema.cdsGrupos.Modificar;
end;

procedure TSistGrupos_DevEx.GruposClick(Sender: TObject);
var
   sOldIndex: string;
   Pos: TBookMark;
   sMensaje : string;
begin
     if PuedeModificar( sMensaje ) then
     begin
          with dmSistema.cdsGrupos do
          begin
               DisableControls;
               sOldIndex := IndexFieldNames;
               try
                  Pos := GetBookMark;
                  try
                     ZBaseDlgModal_DevEx.ShowDlgModal( SistArbolGrupos_DevEx, TSistArbolGrupos_DevEx )
                  finally
                         if ( Pos <> nil ) then
                         begin
                              GotoBookMark( Pos );
                              FreeBookMark( Pos );
                         end;
                  end;
               finally
                      IndexFieldNames:= sOldIndex;
                      EnableControls;
               end;
          end;//with dmSistema.cdsGrupos do
     end
     else
     begin
          zInformation( 'Mapa de Grupos', 'No tiene permiso para consultar o modificar el mapa de grupos.', 0 );
     end;

end;

procedure TSistGrupos_DevEx.ZetaDBGridxDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;

end;

{function TSistGrupos.ChecaGrupoSinRestriccion( const sAccion: string; var sMensaje: string ): boolean;
begin
     {$ifdef RDDAPP}
{     Result := sAccion = K_MODIFICAR;
     sMensaje := Format( K_MENSAJE_GRUPOS_RDD, [ sAccion ] );
     {$else}
{     Result := ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );
     if not Result then
        sMensaje := format( K_MENSAJE_GRUPOS, [ sAccion ] );
     {$endif}
{end; //}

procedure TSistGrupos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
     inherited;
     Grupos.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
end;

procedure TSistGrupos_DevEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
 with ZetaDBGrid do
     begin
          begin
               with ACanvas do
               begin

                    if (  dmCliente.GetGrupoActivo = AViewInfo.GridRecord.values[0]) then
                       Font.Color := clRed
                    else
                        Font.Color := ZetaDBGrid.Font.Color;
               end;
          end;
        
     end;
end;

end.

