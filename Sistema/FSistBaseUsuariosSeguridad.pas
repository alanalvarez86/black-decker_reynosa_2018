unit FSistBaseUsuariosSeguridad;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, Buttons, StdCtrls, ZetaKeyCombo, ZBaseGridLectura_DevEx,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, cxButtons,
  ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid;

type
  TSistBaseUsuariosSeguridad = class(TBaseGridLectura_DevEx)
    ZetaDBGrid: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    EBuscaNombre: TEdit;
    BBusca: TcxButton;
    Label2: TLabel;
    FiltroActivos: TZetaKeyCombo;
    lblGrupo: TLabel;
    FiltroGrupo: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridxDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EBuscaNombreChange(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure FiltroActivosChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FieldGroupChange(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    FFiltroNombres : string;
    FFiltroActivos : String;
    FFiltroGrupos : String;
    procedure FiltraUsuario;
    procedure FiltraActivo;
    procedure SetFiltros;
    procedure LimpiaGrid;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistBaseUsuariosSeguridad: TSistBaseUsuariosSeguridad;

const
     K_TODOS_ITEMS = '<Todos>';

implementation

{$R *.DFM}

uses DSistema,
     ZetaCommonTools,
     ZetaCommonClasses, DCliente, ZetaDialogo;

{ *********** TSistUsuarios *********** }

procedure TSistBaseUsuariosSeguridad.FormCreate(Sender: TObject);
     procedure LlenaCombo;
     begin
          dmSistema.LeeTodosGrupos( FiltroGrupo );
          with FiltroGrupo do
          begin
               Items.Add( K_TODOS_ITEMS );
               ItemIndex := 0;
          end;

     end;
begin
     inherited;
     HelpContext := H80813_Usuarios;
     FiltroActivos.ItemIndex := 0;
     FFiltroNombres := VACIO;
     FFiltroActivos := 'US_ACTIVO = ''S'' ';
     LlenaCombo;
end;

procedure TSistBaseUsuariosSeguridad.Connect;
begin
     with dmSistema do
     begin
          cdsGrupos.Conectar;
          cdsUsuariosSeguridad.Conectar;
          DataSource.DataSet:= cdsUsuariosSeguridad;
     end;
end;

procedure TSistBaseUsuariosSeguridad.Refresh;
begin
     dmSistema.cdsUsuariosSeguridad.Refrescar;
end;

procedure TSistBaseUsuariosSeguridad.ZetaDBGridxDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     with ZetaDBGrid do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if zStrToBool( DataSource.Dataset.FieldByName( 'US_DENTRO' ).AsString ) then
                       Font.Color := clRed
                    else
                        Font.Color := ZetaDBGrid.Font.Color;
               end;
          end;
          //DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

procedure TSistBaseUsuariosSeguridad.Agregar;
begin
     dmSistema.cdsUsuariosSeguridad.Agregar;
end;

procedure TSistBaseUsuariosSeguridad.Borrar;
begin
   dmSistema.cdsUsuariosSeguridad.Borrar;
end;

procedure TSistBaseUsuariosSeguridad.Modificar;
begin
     dmSistema.cdsUsuariosSeguridad.Modificar;
end;

procedure TSistBaseUsuariosSeguridad.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraUsuario;
end;

procedure TSistBaseUsuariosSeguridad.LimpiaGrid;
begin
     ZetadbGrid.SelectedRows.Clear;
end;

procedure TSistBaseUsuariosSeguridad.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     BBusca.Down := Length(EBuscaNombre.Text)>0;
     FiltraUsuario;
end;

procedure TSistBaseUsuariosSeguridad.FiltroActivosChange(Sender: TObject);
begin
     inherited;
     FiltraActivo;
end;

procedure TSistBaseUsuariosSeguridad.FiltraUsuario;
begin
     if BBusca.Down then
     begin
          FFiltroNombres := 'UPPER(US_NOMBRE) LIKE '+ chr(39) + '%'+ UpperCase(EBuscaNombre.Text) + '%' + chr(39);
     end
     else
     begin
          FFiltroNombres := VACIO;
     end;
     SetFiltros;
     LimpiaGrid;
end;

procedure TSistBaseUsuariosSeguridad.FiltraActivo;
begin
     case FiltroActivos.ItemIndex of
          0: FFiltroActivos := 'US_ACTIVO = ''S'' ';
          1: FFiltroActivos := 'US_ACTIVO = ''N'' ';
     else
         FFiltroActivos := VACIO;
     end;
     SetFiltros;
     LimpiaGrid;
end;

procedure TSistBaseUsuariosSeguridad.SetFiltros;
begin
     with dmSistema.cdsUsuariosSeguridad do
     begin
          Filter := ConcatFiltros( FFiltroNombres, FFiltroActivos );
          Filter := ConcatFiltros( Filter, FFiltroGrupos );
          Filtered := StrLleno( Filter );
     end;
end;

procedure TSistBaseUsuariosSeguridad.FormShow(Sender: TObject);
begin
     CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0, '' , SkCount );
     inherited;
     SetFiltros;
end;

procedure TSistBaseUsuariosSeguridad.FieldGroupChange(Sender: TObject);
begin
     inherited;
     with FiltroGrupo do
     begin
          if not ( Text = K_TODOS_ITEMS ) then
              FFiltroGrupos := 'GR_CODIGO = ''' + IntToStr( Integer( Items.Objects[ItemIndex] ) ) + ''''
          else
              FFiltroGrupos := VACIO;
     end;
     SetFiltros;
     LimpiaGrid;
end;

procedure TSistBaseUsuariosSeguridad.FormResize(Sender: TObject);
begin
     inherited;
     FiltroGrupo.Width := iMin( 250, iMax( 125, self.width - 663 ) );
end;

end.
