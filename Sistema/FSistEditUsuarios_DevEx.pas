unit FSistEditUsuarios_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons,
  ZetaSmartLists, FSistBaseEditUsuarios_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, ZetaKeyLookup_DevEx, cxPC, cxNavigator, cxDBNavigator, cxButtons;

type
  TSistEditUsuarios_DevEx = class(TSistBaseEditUsuarios_DevEx)
    Supervisa: TcxButton;
    Preferencias: TcxButton;
    Areas: TcxButton;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    CM_CODIGO: TZetaDBKeyLookup_DevEx;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    lblEmpleado: TLabel;
    BtnTemp: TButton;
    Label15: TLabel;
    US_DOMAIN: TDBEdit;
    US_ACTIVO: TDBCheckBox;
    US_PORTAL: TDBCheckBox;
    spbtnUsuarioRepetido: TcxButton;
    GroupBox3: TGroupBox;
    US_JEFE: TZetaDBKeyLookup_DevEx;
    Label16: TLabel;
    btnRoles: TcxButton;
    btnDominioUsuario: TcxButton;
    CCosto: TcxButton;
    Label17: TLabel;
    procedure SupervisaClick(Sender: TObject);
    procedure PreferenciasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AreasClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure CM_CODIGOValidKey(Sender: TObject);
    procedure BtnTempEnter(Sender: TObject);
    procedure CM_CODIGOExit(Sender: TObject);
    procedure btnRolesClick(Sender: TObject);
    procedure BuscarRepetidoBtnClick(Sender: TObject);
    procedure btnDominioUsuarioClick(Sender: TObject);
    procedure US_CORTOChange(Sender: TObject);
    procedure US_DOMAINChange(Sender: TObject);
    procedure DataSourceStateChange(Sender: TObject);
    procedure CCostoClick(Sender: TObject);
  private
    { Private declarations }
    sEmpresaOriginal: String;
    FCerrarForma: Boolean;
    procedure SetControls( const lEnabled: Boolean);
    procedure SetControlEmpleado;
  public
    { Public declarations }
    procedure Connect;override;
    procedure HabilitaControles; override;
  protected
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure EscribirCambios;override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    procedure Borrar;override;
  end;

var
  SistEditUsuarios_DevEx: TSistEditUsuarios_DevEx;

implementation
uses FSistEditUsuarioSupervisor_DevEx,
     FArbolConfigura_DevEx,
     FSistEditUsuarioArea_DevEx,
     ZetaCommonClasses,
     ZBaseDlgModal_DevEx,
     ZGlobalTress,
     ZAccesosMgr,
     ZAccesosTress,
     DGlobal,
     ZetaDialogo,
     ZetaCommonTools,
     DCliente,
     dSistema,
     DTablas,
     DBaseSistema,
     FSistEditUsuarioCCosto_DevEx,
     FSistEditUsuarioRoles_devEx,
     ZToolsPe;


{$R *.DFM}

procedure TSistEditUsuarios_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          CM_CODIGO.LookupDataset := cdsEmpresasPortal;
          CB_CODIGO.LookupDataset := cdsEmpleadosPortal;
          US_JEFE.LookupDataset := cdsUsuariosLookup;
     end;
     FCerrarForma:= FALSE;

     {$ifdef DOS_CAPAS}
     US_PORTAL.Visible := FALSE;
    {$endif}
end;

procedure TSistEditUsuarios_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsEmpresasPortal.Conectar;
          cdsEmpleadosPortal.Conectar;
     end;
     inherited; { Quedo asi porque se llama primero al metodo cdsEmpleadosPortalLookupSearch por lo que se dispara el
                  metodo BuildEmpresaPortal y trata de conectar la empresa con los datos de cdsEmpresasPortal pero como
                  no se ah Conectado truena }

     if NOT Navegacion then
        IndexDerechos := D_EMP_DATOS_USUARIO;
end;


procedure TSistEditUsuarios_DevEx.FormShow(Sender: TObject);
var

   sAreas, sCCosto, sMensaje,precaption,espacio: string;
   bUsaEnrolamientoSup : Boolean;
   i,txwidth,totalWidth,espacioWidth : integer;

begin
     inherited;
     sAreas := Global.GetGlobalString( K_GLOBAL_LABOR_AREAS );
     bUsaEnrolamientoSup := Global.GetGlobalBooleano( K_GLOBAL_ENROLAMIENTO_SUPERVISORES);

     with Areas do
     begin
          espacio :='';
          label17.Caption := '';
          espaciowidth := label17.width;
          txwidth := label17.Canvas.TextWidth( trim(Format( '&%s A Su Cargo', [ sAreas ] )));
          precaption := trim(Format( '&%s A Su Cargo', [ sAreas ] ));
          Hint    := Format( 'Configurar %s A Su Cargo', [ sAreas ] );
          Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_AREAS, K_DERECHO_CONSULTA );
          totalWidth := (107-(txwidth div 2))div espacioWidth;
          for i := 0 to ( totalWidth) do
          espacio  := espacio +' ';
          caption := espacio + precaption;
     end;
     Supervisa.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_SUPER, K_DERECHO_CONSULTA );
     btnRoles.Enabled := ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CONSULTA ) OR ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CAMBIO )
                         OR ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_ALTA );
     US_JEFE.Enabled := not bUsaEnrolamientoSup;

     with CCosto do
     begin
          espacio :='';
          sCCosto := Global.NombreCosteo;
          label17.Caption := '';
          espaciowidth := label17.width;
          txwidth := label17.Canvas.TextWidth(trim(Format( '&%s A Su Cargo', [ sCCosto ] )));
          preCaption := trim(Format( '&%s A Su Cargo', [ sCCosto ] ));
          Hint    := Format( 'Configurar %s A Su Cargo', [ sCCosto ] );
          Visible := dmCliente.ModuloAutorizadoCosteo and dmTablas.HayDataSetTransferencia( sMensaje );
          totalWidth := (107-(txwidth div 2))div espacioWidth;
          for i := 0 to ( totalWidth) do
          espacio  := espacio +' ';
          caption := espacio + precaption;
     end;
     btnSuscripciones.Enabled := dmCliente.EmpresaAbierta;
     Supervisa.Enabled := dmCliente.EmpresaAbierta;
     Areas.Enabled := dmCliente.EmpresaAbierta;
     CCosto.Enabled := dmCliente.EmpresaAbierta;
end;

procedure TSistEditUsuarios_DevEx.HabilitaControles;
begin
     inherited HabilitaControles;
     Areas.Enabled := dmCliente.EmpresaAbierta and not Editing;
     Supervisa.Enabled := dmCliente.EmpresaAbierta and not Editing;
     Preferencias.Enabled := not Editing;
     {$ifdef DOS_CAPAS}
     btnRoles.Enabled := FALSE;
     {$else}
     btnRoles.Enabled := not Editing and(ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CONSULTA ) OR ZAccesosMgr.CheckDerecho( D_CAT_ROLES, K_DERECHO_CAMBIO ) );
     {$endif}
     spbtnUsuarioRepetido.Enabled := Editing and StrLleno( US_CORTO.Text );
     btnDominioUsuario.Enabled := Editing and StrLleno( US_DOMAIN.Text );
     //US_BLOQUEA.Enabled := ZAccesosMgr.CheckDerecho( D_SIST_APAGA_USUARIOS, K_DERECHO_CONSULTA );
     btnSuscripciones.Enabled := dmCliente.EmpresaAbierta;
     Supervisa.Enabled := dmCliente.EmpresaAbierta;
     Areas.Enabled := dmCliente.EmpresaAbierta;
     CCosto.Enabled := dmCliente.EmpresaAbierta;
end;

procedure TSistEditUsuarios_DevEx.SupervisaClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
     begin
          //ZBaseDlgModal.ShowDlgModal( FSistEditUserSuper, TFSistEditUserSuper )

          if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( FSistEditUserSuper_DevEx, TFSistEditUserSuper_DevEx );
          end
          else
          begin
               ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

procedure TSistEditUsuarios_DevEx.PreferenciasClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( ArbolConfigura_DevEx, TArbolConfigura_DevEx )
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;


procedure TSistEditUsuarios_DevEx.AreasClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
     begin
          //ZBaseDlgModal.ShowDlgModal( FSistEditUserArea, TFSistEditUserArea )

          if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
          begin
               ZBaseDlgModal_DevEx.ShowDlgModal( FSistEditUserArea_DevEx, TFSistEditUserArea_DevEx );
          end
          else
          begin
               ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

procedure TSistEditUsuarios_DevEx.SetControls( const lEnabled: Boolean );
begin
     CB_CODIGO.Enabled := lEnabled;
     lblEmpleado.Enabled := lEnabled;
end;

procedure TSistEditUsuarios_DevEx.SetControlEmpleado;
begin
     if ( dmSistema.cdsUsuarios.FieldByName('CB_CODIGO').AsInteger <= 0 ) then
        CB_CODIGO.SetLlaveDescripcion( VACIO, VACIO );
end;

procedure TSistEditUsuarios_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = Nil ) then
     begin
          sEmpresaOriginal := dmSistema.cdsUsuarios.FieldByName('CM_CODIGO').AsString;
          DataSource.AutoEdit := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
           {OP:30.Abr.08}
          if ( dmSistema.cdsUsuarios.FieldByName( 'US_BLOQUEA' ).AsString = K_GLOBAL_SI ) and
             ( ZAccesosMgr.Revisa( D_SIST_PRENDE_USUARIOS ) ) then
             US_BLOQUEA.Enabled := True
          else if ( dmSistema.cdsUsuarios.FieldByName( 'US_BLOQUEA' ).AsString = K_GLOBAL_NO ) and
             ( ZAccesosMgr.Revisa( D_SIST_APAGA_USUARIOS ) ) then
             US_BLOQUEA.Enabled := True
          else
              US_BLOQUEA.Enabled := False;

     end;
     if (Field = Nil) or ( Field.FieldName = 'CM_CODIGO' ) then
     begin
          SetControls( strLleno( dmSistema.cdsUsuarios.FieldByName('CM_CODIGO').AsString ) );
     end;
     if (Field = Nil) or ( Field.FieldName = 'CB_CODIGO' ) then
     begin
          SetControlEmpleado;
     end;
end;

procedure TSistEditUsuarios_DevEx.CM_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     if not ( dmSistema.cdsUsuarios.State in [dsBrowse]) then
     begin
          if ( BtnTemp.Focused ) then
          begin
               if ( CB_CODIGO.Enabled ) then
                  CB_CODIGO.SetFocus
          end;
          if ( ActiveControl = Nil ) then
             US_BLOQUEA.SetFocus;
     end;
end;

procedure TSistEditUsuarios_DevEx.BtnTempEnter(Sender: TObject);
begin
     inherited;
     if ( CB_CODIGO.Enabled ) or strVacio( CM_CODIGO.Llave ) then
         US_BLOQUEA.SetFocus;
end;

procedure TSistEditUsuarios_DevEx.CM_CODIGOExit(Sender: TObject);
begin
     inherited;
     if ( sEmpresaOriginal <> CM_CODIGO.Llave ) then
     begin
          CB_CODIGO.Llave := VACIO;
          sEmpresaOriginal := CM_CODIGO.LLave;
     end;
end;

procedure TSistEditUsuarios_DevEx.btnRolesClick(Sender: TObject);
var sMensaje :string;
begin
     inherited;
      if PuedeModificar( sMensaje ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( SistEditUsuarioRoles_DevEx, TFSistEditUsuarioRoles_DevEx )
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TSistEditUsuarios_DevEx.BuscarRepetidoBtnClick(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          if ( UpperCase(US_CORTO.Text) <> UpperCase( cdsUsuarios.FieldByName('US_CORTO').AsString ) )then
          begin
               if (cdsUsuariosLookup.Locate('US_CORTO',UpperCase(US_CORTO.Text) ,[]))then
               begin
                    ZetaDialogo.zInformation( Caption, 'El Usuario: '+US_CORTO.Text+' est� repetido' , 0 )
               end
               else
               begin
                    ZetaDialogo.zInformation( Caption, 'No existe Usuario repetido' , 0 )
               end;
          end
          else
              ZetaDialogo.zInformation( Caption, 'No existe Usuario repetido' , 0 );
     end;
end;

procedure TSistEditUsuarios_DevEx.EscribirCambios;
{$ifndef DOS_CAPAS}
var
   Lista : TStrings;
   lNuevoRegistro:Boolean;
{$endif}
begin
     dmSistema.EditandoUsuarios := True; 
     {$ifndef DOS_CAPAS}
     lNuevoRegistro := Inserting;
     {$endif}

     inherited;
     
     {$ifndef DOS_CAPAS}
     if( lNuevoRegistro )then
     begin
          with dmSistema do
          begin
               if (cdsUsuarios.ChangeCount = 0) then
               begin
                    Lista := TStringList.Create;
                    CargaListaRolesDefault(Lista);
                    Lista.Free;
               end;
          end;
     end;
     {$endif}
end;

function TSistEditUsuarios_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := Navegacion ;
     if NOT Result then
        sMensaje := 'El Empleado ya tiene Acceso a Sistema Tress';
end;

procedure TSistEditUsuarios_DevEx.btnDominioUsuarioClick(Sender: TObject);
begin
     inherited;
     with dmSistema do
     begin
          if ( UpperCase(US_DOMAIN.Text) <> UpperCase( cdsUsuarios.FieldByName('US_DOMAIN').AsString ) )then
          begin
               if (cdsUsuariosLookup.Locate('US_DOMAIN',UpperCase(US_DOMAIN.Text),[]))then
               begin
                    ZetaDialogo.zInformation( Caption, 'El Usuario del Dominio: '+US_DOMAIN.Text+' est� repetido' , 0 )
               end
               else
               begin
                    ZetaDialogo.zInformation( Caption, 'No existe Usuario repetido' , 0 )
               end;
          end
          else
              ZetaDialogo.zInformation( Caption, 'No existe Usuario del Dominio repetido' , 0 );
     end;
end;

procedure TSistEditUsuarios_DevEx.Borrar;
const
   K_ADV_SUPERVISORES_EMPRESA = 'El Empleado puede estar como un Supervisor Enrolado en esta o en alguna otra de sus empresas. Si le quita el Acceso a Sistema TRESS puede quedar alg�n Supervisor sin enrolar.';
begin
     if Navegacion then
        inherited Borrar
     else
     begin
          if ZConfirm( Caption,K_ADV_SUPERVISORES_EMPRESA +'� Desea quitarle el Acceso a Sistema Tress al Empleado ?', 0, mbNo ) then
          begin
               dmSistema.BajaUsuario(dmCliente.cdsEmpleado.FieldByName('CB_CODIGO').AsInteger, TRUE);
               Close;
          end;
     end;
end;

procedure TSistEditUsuarios_DevEx.US_CORTOChange(Sender: TObject);
begin
     inherited;
     spbtnUsuarioRepetido.Enabled := Editing and StrLleno( US_CORTO.Text );

end;
procedure TSistEditUsuarios_DevEx.US_DOMAINChange(Sender: TObject);
begin
     inherited;
      btnDominioUsuario .Enabled := Editing and StrLleno( US_DOMAIN.Text );
end;

function TSistEditUsuarios_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
          if (not Result) then
          begin
               sMensaje := 'No Tiene Permiso Para Modificar Registros';
          end;
     end;
end;

procedure TSistEditUsuarios_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     Supervisa.Enabled := (not Editing) and (ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_SUPER, K_DERECHO_CONSULTA ));
     Areas.Enabled := (not Editing) and (ZAccesosMgr.CheckDerecho( D_SIST_ASIGNAR_AREAS, K_DERECHO_CONSULTA ));
     CCosto.Enabled := (not Editing) and dmCliente.ModuloAutorizadoCosteo;
     btnSuscripciones.Enabled := dmCliente.EmpresaAbierta;
     Supervisa.Enabled := dmCliente.EmpresaAbierta;
     Areas.Enabled := dmCliente.EmpresaAbierta;
     CCosto.Enabled := dmCliente.EmpresaAbierta;
end;

procedure TSistEditUsuarios_DevEx.CCostoClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificar( sMensaje ) then
     begin
          if dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset ) then
          begin
               if ( dmTablas.HayDataSetTransferencia( sMensaje ) ) then
                    ZBaseDlgModal_DevEx.ShowDlgModal( SistEditUsuarioCCosto_DevEx, TSistEditUsuarioCCosto_DevEx )
               else
                    ZInformation( 'Usuarios', sMensaje, 0 )
          end
          else
          begin
               ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar Registros', 0 );
          end;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, sMensaje, 0 );
     end;
end;

end.
