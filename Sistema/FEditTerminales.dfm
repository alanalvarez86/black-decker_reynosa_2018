inherited EditTerminales: TEditTerminales
  Left = 291
  Top = 190
  Caption = 'Terminales'
  ClientHeight = 391
  ClientWidth = 576
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 355
    Width = 576
    inherited OK: TBitBtn
      Left = 408
    end
    inherited Cancelar: TBitBtn
      Left = 493
    end
  end
  inherited PanelSuperior: TPanel
    Width = 576
    inherited DBNavigator: TDBNavigator
      OnClick = DBNavigatorClick
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 576
    inherited ValorActivo2: TPanel
      Width = 250
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 51
    Width = 576
    Height = 304
    ActivePage = tbDatosGenerales
    Align = alClient
    MultiLine = True
    TabOrder = 3
    object tbDatosGenerales: TTabSheet
      Caption = 'Datos Generales'
      object Label1: TLabel
        Left = 56
        Top = 9
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
      end
      object Label2: TLabel
        Left = 33
        Top = 31
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object Label3: TLabel
        Left = 52
        Top = 97
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'N'#250'mero:'
      end
      object Label4: TLabel
        Left = 62
        Top = 119
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Texto:'
      end
      object Label5: TLabel
        Left = 18
        Top = 139
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Direcci'#243'n MAC:'
      end
      object Label6: TLabel
        Left = 40
        Top = 53
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aplicaci'#243'n:'
      end
      object Label7: TLabel
        Left = 32
        Top = 157
        Width = 60
        Height = 13
        Alignment = taRightJustify
        Caption = #218'ltimo latido:'
      end
      object Label8: TLabel
        Left = 64
        Top = 75
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona:'
      end
      object TE_BEAT: TZetaDBTextBox
        Left = 95
        Top = 155
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_BEAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TE_BEAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TE_MAC: TZetaTextBox
        Left = 95
        Top = 137
        Width = 233
        Height = 17
        AutoSize = False
        Caption = 'TE_MAC'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object TE_CODIGO: TZetaDBNumero
        Left = 95
        Top = 5
        Width = 50
        Height = 21
        Enabled = False
        Mascara = mnDias
        TabOrder = 0
        Text = '0'
        DataField = 'TE_CODIGO'
        DataSource = DataSource
      end
      object TE_NUMERO: TZetaDBNumero
        Left = 95
        Top = 93
        Width = 113
        Height = 21
        Mascara = mnHoras
        TabOrder = 5
        Text = '0.00'
        DataField = 'TE_NUMERO'
        DataSource = DataSource
      end
      object TE_ACTIVO: TDBCheckBox
        Left = 278
        Top = 7
        Width = 50
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Activo'
        DataField = 'TE_ACTIVO'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object TE_ZONA: TZetaKeyCombo
        Left = 95
        Top = 71
        Width = 233
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 4
        OnChange = TE_ZONAChange
        Items.Strings = (
          '(GMT-05:00) Este (US & Canada)'
          '(GMT-06:00) Central (US & Canada)'
          '(GMT-06:00) Guadalajara, D. F., Monterrey'
          '(GMT-07:00) Arizona'
          '(GMT-07:00) Chihuahua, La Paz, Mazatl'#225'n'
          '(GMT-07:00) Monta'#241'a (US & Canada)'
          '(GMT-08:00) Pac'#237'fico (US & Canada)'
          '(GMT-08:00) Baja California')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object TE_APP: TZetaKeyCombo
        Left = 95
        Top = 49
        Width = 233
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnChange = TE_APPChange
        Items.Strings = (
          'Asistencia'
          'Cafeter'#237'a'
          'Acceso')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
      end
      object btnReiniciarTerminal: TButton
        Left = 95
        Top = 174
        Width = 179
        Height = 25
        Caption = 'Sincronizar huellas de la terminal'
        Enabled = False
        TabOrder = 7
        OnClick = btnReiniciarTerminalClick
      end
      object TE_NOMBRE: TDBEdit
        Left = 95
        Top = 27
        Width = 233
        Height = 21
        DataField = 'TE_NOMBRE'
        DataSource = DataSource
        TabOrder = 2
      end
      object TE_TEXTO: TDBEdit
        Left = 95
        Top = 115
        Width = 233
        Height = 21
        DataField = 'TE_TEXTO'
        DataSource = DataSource
        TabOrder = 6
      end
    end
    object tbConfiguracion: TTabSheet
      Caption = 'Informaci'#243'n del reloj'
      ImageIndex = 1
      object Label15: TLabel
        Left = 38
        Top = 32
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'MenuAccessCode:'
      end
      object Label16: TLabel
        Left = 90
        Top = 11
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Volume:'
      end
      object Label11: TLabel
        Left = 47
        Top = 53
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'DateTimeFormat:'
      end
      object Label10: TLabel
        Left = 24
        Top = 74
        Width = 104
        Height = 13
        Caption = 'UpdateTimeByServer:'
      end
      object Label28: TLabel
        Left = 70
        Top = 90
        Width = 58
        Height = 13
        Caption = 'ShowPhoto:'
      end
      object Label29: TLabel
        Left = 55
        Top = 106
        Width = 73
        Height = 13
        Caption = 'EnableKeyPad:'
      end
      object GroupBox5: TGroupBox
        Left = 8
        Top = 448
        Width = 257
        Height = 41
        Caption = ' Mensaje de autentificaci'#243'n: '
        TabOrder = 6
      end
      object GroupBox10: TGroupBox
        Left = 262
        Top = 352
        Width = 258
        Height = 105
        Caption = 'Validaciones:'
        TabOrder = 7
      end
      object GroupBox11: TGroupBox
        Left = 262
        Top = 456
        Width = 258
        Height = 57
        Caption = ' Versi'#243'n del software: '
        TabOrder = 8
      end
      object MenuAccessCode: TZetaNumero
        Left = 131
        Top = 29
        Width = 65
        Height = 21
        Hint = 'Contrase'#241'a para ingresar al men'#250' de la terminal {4...6 d'#237'gitos}'
        Mascara = mnDias
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '0'
        OnChange = XMLChange
      end
      object Volume: TZetaNumero
        Left = 131
        Top = 7
        Width = 65
        Height = 21
        Hint = 'Volumen de la terminal {00-100}'
        Mascara = mnDias
        MaxLength = 3
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '0'
        OnChange = XMLChange
      end
      object DateTimeFormat: TEdit
        Left = 131
        Top = 51
        Width = 238
        Height = 21
        Hint = 'Formato de fecha (ej: DD/MM hh:mm:ss)'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = XMLChange
      end
      object UpdateTimeByServer: TCheckBox
        Left = 131
        Top = 72
        Width = 25
        Height = 17
        Hint = 'Actualizar la hora por un servidor'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = XMLChange
      end
      object ShowPhoto: TCheckBox
        Left = 131
        Top = 88
        Width = 25
        Height = 17
        Hint = 'Presentar foto en la terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = XMLChange
      end
      object EnableKeyPad: TCheckBox
        Left = 131
        Top = 104
        Width = 25
        Height = 17
        Hint = 'Activar teclado de la terminal'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = XMLChange
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Configuraci'#243'n de red'
      ImageIndex = 4
      object Label14: TLabel
        Left = 65
        Top = 6
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'WebService:'
      end
      object Label44: TLabel
        Left = 79
        Top = 113
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'UseProxy:'
      end
      object Label45: TLabel
        Left = 98
        Top = 132
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'Proxy:'
      end
      object Label46: TLabel
        Left = 79
        Top = 154
        Width = 48
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxyPort:'
      end
      object Label47: TLabel
        Left = 11
        Top = 172
        Width = 116
        Height = 13
        Alignment = taRightJustify
        Caption = 'UseProxyAuthentication:'
      end
      object Label48: TLabel
        Left = 48
        Top = 190
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxyUserName:'
      end
      object Label49: TLabel
        Left = 52
        Top = 212
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxyPassword:'
      end
      object Label50: TLabel
        Left = 115
        Top = 28
        Width = 12
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ip:'
      end
      object Label51: TLabel
        Left = 81
        Top = 50
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'NetMask:'
      end
      object Label52: TLabel
        Left = 45
        Top = 72
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'DefaultGateWay:'
      end
      object Label53: TLabel
        Left = 101
        Top = 94
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'DNS:'
      end
      object UseProxy: TCheckBox
        Left = 131
        Top = 111
        Width = 17
        Height = 17
        Hint = 'Validaci'#243'n de uso de proxy'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = XMLChange
      end
      object UseProxyAuthentication: TCheckBox
        Left = 131
        Top = 170
        Width = 17
        Height = 17
        Hint = 'Utilizar autentificaci'#243'n del proxy'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = XMLChange
      end
      object WebService: TEdit
        Left = 131
        Top = 2
        Width = 350
        Height = 21
        Hint = 'URL del WS de checada'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnChange = XMLChange
      end
      object Ip: TEdit
        Left = 131
        Top = 24
        Width = 200
        Height = 21
        Hint = 'Ip de la terminal *Utilizar DHCP para obtener la ip din'#225'mica'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnChange = XMLChange
      end
      object NetMask: TEdit
        Left = 131
        Top = 46
        Width = 200
        Height = 21
        Hint = 'M'#225'scara de red'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnChange = XMLChange
      end
      object DefaultGateWay: TEdit
        Left = 131
        Top = 68
        Width = 200
        Height = 21
        Hint = 'Puerta de salida'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnChange = XMLChange
      end
      object DNS: TEdit
        Left = 131
        Top = 90
        Width = 200
        Height = 21
        Hint = 'DNS a utilizar'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnChange = XMLChange
      end
      object Proxy: TEdit
        Left = 131
        Top = 128
        Width = 200
        Height = 21
        Hint = 'Direcci'#243'n del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnChange = XMLChange
      end
      object ProxyPort: TEdit
        Left = 131
        Top = 150
        Width = 200
        Height = 21
        Hint = 'Puerto del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnChange = XMLChange
      end
      object ProxyUserName: TEdit
        Left = 131
        Top = 186
        Width = 200
        Height = 21
        Hint = 'Usuario del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnChange = XMLChange
      end
      object ProxyPassword: TEdit
        Left = 131
        Top = 208
        Width = 200
        Height = 21
        Hint = 'Contrase'#241'a del proxy'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnChange = XMLChange
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Dispositivos, validaciones y mensajes'
      ImageIndex = 4
      object Label22: TLabel
        Left = 58
        Top = 33
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'BarCodeReader:'
      end
      object Label23: TLabel
        Left = 78
        Top = 11
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'ProxReader:'
      end
      object Label9: TLabel
        Left = 22
        Top = 149
        Width = 115
        Height = 13
        Alignment = taRightJustify
        Caption = 'FpuNotIdentifyMessage:'
      end
      object Label24: TLabel
        Left = 8
        Top = 171
        Width = 129
        Height = 13
        Alignment = taRightJustify
        Caption = 'NotValidByServerMessage:'
      end
      object Label25: TLabel
        Left = 46
        Top = 193
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'DuplicateMessage:'
      end
      object Label26: TLabel
        Left = 37
        Top = 215
        Width = 100
        Height = 13
        Alignment = taRightJustify
        Caption = 'AutenticateMessage:'
      end
      object Label27: TLabel
        Left = 58
        Top = 237
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'OfflineValidation:'
      end
      object Label12: TLabel
        Left = 79
        Top = 97
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'FingerVerify:'
      end
      object Label13: TLabel
        Left = 44
        Top = 113
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Caption = 'EnableOfflineRelay:'
      end
      object Label18: TLabel
        Left = 115
        Top = 55
        Width = 22
        Height = 13
        Alignment = taRightJustify
        Caption = 'HID:'
      end
      object Label17: TLabel
        Left = 44
        Top = 129
        Width = 93
        Height = 13
        Alignment = taRightJustify
        Caption = 'OneMinuteValidate:'
      end
      object Label30: TLabel
        Left = 84
        Top = 77
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'FingerPrint:'
      end
      object ProxReader: TComboBox
        Left = 140
        Top = 7
        Width = 65
        Height = 21
        Hint = 'Proximidad'
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = 'ProxReader'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object BarCodeReader: TComboBox
        Left = 140
        Top = 29
        Width = 65
        Height = 21
        Hint = 'C'#243'digo de barras'
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = 'ComboBox1'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object FingerVerify: TCheckBox
        Left = 140
        Top = 95
        Width = 17
        Height = 17
        Hint = 'Segunda validaci'#243'n con huella'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = XMLChange
      end
      object EnableOfflineRelay: TCheckBox
        Left = 140
        Top = 111
        Width = 17
        Height = 17
        Hint = 
          'Indica si se desea la apertura del relay en modo acceso y offlin' +
          'e'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = XMLChange
      end
      object HID: TComboBox
        Left = 140
        Top = 51
        Width = 65
        Height = 21
        Hint = 'HID'
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = 'ComboBox1'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
      object FpuNotIdetifyMessage: TEdit
        Left = 140
        Top = 145
        Width = 350
        Height = 21
        Hint = 'Mensaje de huella inv'#225'lida'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnChange = XMLChange
      end
      object NotValidByServerMessage: TEdit
        Left = 140
        Top = 167
        Width = 350
        Height = 21
        Hint = 'Mensaje de error de validaci'#243'n del servidor'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnChange = XMLChange
      end
      object DuplicateMessage: TEdit
        Left = 140
        Top = 189
        Width = 350
        Height = 21
        Hint = 'Mensaje de checada duplicada'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnChange = XMLChange
      end
      object AutenticateMessage: TEdit
        Left = 140
        Top = 211
        Width = 350
        Height = 21
        Hint = 'Mensaje de autentificaci'#243'n v'#225'lida'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        OnChange = XMLChange
      end
      object OfflineValidation: TEdit
        Left = 140
        Top = 233
        Width = 350
        Height = 21
        Hint = 'Mensaje de checada fuera de l'#237'nea'
        MaxLength = 30
        ParentShowHint = False
        ShowHint = True
        TabOrder = 11
        OnChange = XMLChange
      end
      object OneMinuteValidate: TCheckBox
        Left = 140
        Top = 127
        Width = 17
        Height = 17
        Hint = 
          'Validar de no permitir registro del mismo empleado en el mismo m' +
          'inuto'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = XMLChange
      end
      object FingerPrint: TComboBox
        Left = 140
        Top = 73
        Width = 65
        Height = 21
        Hint = 'Lector de huella d'#237'gital'
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = 'FingerPrint'
        OnChange = XMLChange
        Items.Strings = (
          'ON'
          'OFF')
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Tiempos'
      ImageIndex = 7
      object Label19: TLabel
        Left = 85
        Top = 11
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'TimeOut:'
      end
      object Label20: TLabel
        Left = 51
        Top = 33
        Width = 77
        Height = 13
        Alignment = taRightJustify
        Caption = 'HeartBeatTimer:'
      end
      object Label33: TLabel
        Left = 82
        Top = 55
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'MsgTime:'
      end
      object Label34: TLabel
        Left = 75
        Top = 99
        Width = 53
        Height = 13
        Alignment = taRightJustify
        Caption = 'RelayTime:'
      end
      object Label21: TLabel
        Left = 60
        Top = 77
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'ErrorMsgTime:'
      end
      object TimeOut: TZetaNumero
        Left = 132
        Top = 7
        Width = 65
        Height = 21
        Hint = 'Time out de llamadas al WS {01-3600} '
        Mascara = mnDias
        MaxLength = 4
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '0'
        OnChange = XMLChange
      end
      object HeartBeatTimer: TZetaNumero
        Left = 132
        Top = 29
        Width = 65
        Height = 21
        Hint = 'Frecuencia de comunicaci'#243'n al WS {01-86400}'
        Mascara = mnDias
        MaxLength = 5
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '0'
        OnChange = XMLChange
      end
      object MsgTime: TZetaNumero
        Left = 132
        Top = 51
        Width = 65
        Height = 21
        Hint = 'Tiempo de despliegue de mensajes {01-06}'
        Mascara = mnDias
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = '0'
        OnChange = XMLChange
      end
      object RelayTime: TZetaNumero
        Left = 132
        Top = 95
        Width = 65
        Height = 21
        Hint = 'Duraci'#243'n de apertura del relevador {01-06}'
        Mascara = mnDias
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        Text = '0'
        OnChange = XMLChange
      end
      object ErrorMsgTime: TZetaNumero
        Left = 132
        Top = 73
        Width = 65
        Height = 21
        Hint = 'Tiempo de despliegue de mensajes de error {01-06}'
        Mascara = mnDias
        MaxLength = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = '0'
        OnChange = XMLChange
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Propiedades (XML)'
      ImageIndex = 5
      object DBRichEdit1: TDBRichEdit
        Left = 0
        Top = 0
        Width = 568
        Height = 258
        Align = alClient
        Color = clBtnFace
        DataField = 'TE_XML'
        DataSource = DataSource
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 396
    Top = 9
  end
end
