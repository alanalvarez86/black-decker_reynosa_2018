unit FSistEditUsuarioSupervisor;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls,
     ZBaseDlgModal, ZetaSmartLists;

type
  TFSistEditUserSuper = class(TZetaDlgModal)
    Niveles: TCheckListBox;
    Prender: TBitBtn;
    Apagar: TBitBtn;
    PanelBusqueda: TPanel;
    EmpleadoBuscaBtn: TZetaSpeedButton;
    EditBusca: TEdit;
    Label1: TLabel;
    MostrarActivos: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure ApagarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure NivelesClickCheck(Sender: TObject);
    procedure EmpleadoBuscaBtnClick(Sender: TObject);
    procedure MostrarActivosClick(Sender: TObject);
  private
    { Private declarations }
    FLista: TStrings;
    FUltimoTexto: String;
    FPosUltimo: Integer;
    function EncontroSuper: Boolean;
    procedure PrendeApaga( const lState: Boolean );
    procedure Cargar;
    procedure Descargar;
  public
    { Public declarations }
  end;

var
  FSistEditUserSuper: TFSistEditUserSuper;

implementation

uses ZetaCommonClasses,
     ZetaCommonTools,
     ZetaDialogo,
     DSistema;

{$R *.DFM}

procedure TFSistEditUserSuper.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TStringList.Create;
end;

procedure TFSistEditUserSuper.FormShow(Sender: TObject);
begin
     inherited;
     Cargar;
     OK.Enabled := False;
     ActiveControl := Niveles;
     with MostrarActivos do
     begin
          Checked:= True;
          Enabled:= True;
     end;
     FUltimoTexto:= VACIO;
     FPosUltimo:= -1;

end;

procedure TFSistEditUserSuper.FormDestroy(Sender: TObject);
begin
     inherited;
     FLista.Free;
end;

procedure TFSistEditUserSuper.Cargar;
var
   i: Integer;
begin
     Caption := dmSistema.CargaListaSupervisores( FLista, MostrarActivos.Checked );
     with Niveles do
     begin
          try
             Clear;
             Items.BeginUpdate;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       //Items.Add( Values[ Names[ i ] ] );
                       Items.Add( FLista[i] );
                       Checked[ i ] := ( Integer( Objects[ i ] ) > 0 );
                  end;
             end;
          finally
                 Items.EndUpdate;
          end;
     end;
end;

procedure TFSistEditUserSuper.Descargar;
var
   i: Integer;
begin
     with Niveles do
     begin
          with Items do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    if Checked[ i ] then
                       FLista.Objects[ i ] := TObject( 1 )
                    else
                        FLista.Objects[ i ] := TObject( 0 );
               end;
          end;
     end;
     dmSistema.DescargaListaSupervisores( FLista );
end;

procedure TFSistEditUserSuper.PrendeApaga( const lState: Boolean );
var
   i: Integer;
begin
     with Niveles do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                      EndUpdate;
               end;
          end;
     end;
     NivelesClickCheck( Niveles );
end;

procedure TFSistEditUserSuper.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( True );
end;

procedure TFSistEditUserSuper.ApagarClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( False );
end;

procedure TFSistEditUserSuper.OKClick(Sender: TObject);
begin
     inherited;
     Descargar;
end;

procedure TFSistEditUserSuper.NivelesClickCheck(Sender: TObject);
begin
     inherited;
     with OK do
     begin
          if not Enabled then
             Enabled := True;
     end;
     MostrarActivos.Enabled:=False;
end;

procedure TFSistEditUserSuper.EmpleadoBuscaBtnClick(Sender: TObject);
var
   lEncontro: Boolean;
begin
     if strLleno( EditBusca.Text ) and ( FPosUltimo <> Niveles.Count )then
     begin
          lEncontro:= EncontroSuper;
     end
     else
     begin
          lEncontro:= FALSE;
     end;
     if ( not lEncontro ) then
     begin
          Niveles.Selected[ FPosUltimo ]:= False;

          if( FPosUltimo > 0 ) then
              ZetaDialogo.ZInformation( 'B�squeda de Supervisores', '! No hay otro Supervisor con esos Datos !' ,0 )
          else
              ZetaDialogo.ZInformation( 'B�squeda de Supervisores', '! No hay un Supervisor con esos Datos !' ,0 );
          with EditBusca do
          begin
               SetFocus;
               SelectAll;
          end;

     FPosUltimo:=0;
     end;
end;

function TFSistEditUserSuper.EncontroSuper: Boolean;
var
   i : Integer;
   sTexto: String;
begin
     sTexto:= UpperCase( EditBusca.Text );
     with Niveles do
     begin
          if ( sTexto <> FUltimoTexto ) then
          begin
               FUltimoTexto := sTexto;
               FPosUltimo:= 0;
          end;
          Result:= False;
          for i:= FPosUltimo  to ( Count - 1 ) do
          begin
               if ( Pos( sTexto, UpperCase( Items[i] ) ) > 0 ) then
               begin
                    FUltimoTexto:= sTexto;
                    FPosUltimo:= i;
                    Selected[ FPosUltimo ]:= True;
                    FPosUltimo:= FPosUltimo + 1;
                    Result:= True;
                    Exit;
               end;
          end;
     end;
end;

procedure TFSistEditUserSuper.MostrarActivosClick(Sender: TObject);
begin
     Cargar;
end;

end.
