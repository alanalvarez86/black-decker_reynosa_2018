unit FeditFechaLimite_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaDBTextBox, StdCtrls, Mask, ZetaFecha, Buttons,
  {$ifndef VER130}Variants,{$endif}  
  ExtCtrls, Db, DBClient,
  ZetaCommonClasses,
  ZetaClientDataSet, DBCtrls, ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, ImgList, cxButtons;

type
  TEditFechaLimite_DevEx = class(TZetaDlgModal_DevEx)
    FechaLimiteLBL: TLabel;
    FechaLimite: TZetaFecha;
    GL_DESCRIP: TZetaTextBox;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    function SetDatos: boolean;
    procedure GetDatos;
    procedure Setcontroles;
  public
    { Public declarations }
  end;

var
  EditFechaLimite_DevEx: TEditFechaLimite_DevEx;

implementation

{$R *.DFM}

uses
    DAsistencia,
    DSistema,
    DGlobal,
    ZetaCommonTools,
    ZAccesosTress,
    ZAccesosMgr;

procedure TEditFechaLimite_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsUsuarios.Conectar;
     GetDatos;
     SetControles;
end;

procedure TEditFechaLimite_DevEx.GetDatos;
var
   sComentarios, sNombreUsuario, sDescripcion, sFecha: string;
   iUsuario: integer;
   oDatos: OleVariant;
   dFechaLimite: TDate;
begin
     oDatos := NULL;
     oDatos := Global.GetFechaLimite;
     //Variables Locales
     iUsuario := oDatos[ K_FECHA_LIMITE_USUARIO ];
     sComentarios := oDatos[ K_FECHA_LIMITE_DESCRIPCION ];
     sFecha := FechaCorta( oDatos[ K_FECHA_LIMITE_CAPTURA ] );

     //Se Asigna el valor de la fecha Limite
     dFechaLimite := StrToFecha( oDatos[ K_FECHA_LIMITE_FORMULA ] );

     //Si la Fecha es NULA no se construye el mensaje de Comentarios
     if ( dFechaLimite <> NullDateTime ) then
     begin
          FechaLimite.Valor := dFechaLimite;
          //Se Checa el Usuario que trae el Arreglo
          if ( iUsuario = 0 ) then
          begin
               sDescripcion := Format( 'Ajustada Autom�ticamente el D�a %s %sPor Cambio de Estatus de la %s', [ sFecha,
                                                                                                               CR_LF,
                                                                                                               sComentarios ] )
          end
          else
          begin
               sNombreUsuario := VACIO;
               with dmSistema.cdsUsuariosLookup do
               begin
                   if ( Locate( 'US_CODIGO', iUsuario, [] ) ) then
                      sNombreUsuario := FieldByName( 'US_NOMBRE' ).AsString;
               end;
               sDescripcion := Format( '%s el D�a %s %sPor %d=%s', [ sComentarios, sFecha,
                                                                      CR_LF, iUsuario,
                                                                      sNombreUsuario ] );
          end;
          GL_DESCRIP.Caption := sDescripcion;
     end
     else
         FechaLimite.Valor := Date;
end;

function TEditFechaLimite_DevEx.SetDatos: boolean;
begin
     Result := Global.SetFechaLimite( FechaLimite.Valor );
end;

procedure TEditFechaLimite_DevEx.Setcontroles;
var
   lEnabled: boolean;
begin
     lEnabled := ZAccesosMgr.CheckDerecho( D_ASIS_BLOQUEO_CAMBIOS, K_DERECHO_MODIFICAR_FECHA_LIMITE );
     with OK_DevEx do
     begin
          Enabled := lEnabled;
          Visible := lEnabled;
     end;
     FechaLimite.Enabled := lEnabled;
     with Cancelar_DevEx do
     begin
          if lEnabled then
          begin

               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end
          else
          begin
               
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
end;

procedure TEditFechaLimite_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     if ( SetDatos ) then
        ModalResult := mrOk;

end;

end.
