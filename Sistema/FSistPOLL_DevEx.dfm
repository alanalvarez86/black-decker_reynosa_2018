inherited SistPOLL_DevEx: TSistPOLL_DevEx
  Left = 202
  Top = 118
  Caption = 'Poll Pendientes'
  ClientWidth = 533
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 533
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 207
      inherited textoValorActivo2: TLabel
        Width = 201
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 533
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object PO_EMPRESA: TcxGridDBColumn
        Caption = 'Empresa'
        DataBinding.FieldName = 'PO_EMPRESA'
        MinWidth = 80
      end
      object PO_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'PO_NUMERO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 80
        Width = 100
      end
      object PO_LETRA: TcxGridDBColumn
        Caption = 'Letra'
        DataBinding.FieldName = 'PO_LETRA'
        MinWidth = 80
      end
      object PO_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'PO_FECHA'
        MinWidth = 80
        Width = 80
      end
      object PO_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'PO_HORA'
        MinWidth = 80
      end
      object PO_LINX: TcxGridDBColumn
        Caption = 'Reloj'
        DataBinding.FieldName = 'PO_LINX'
        MinWidth = 80
        Width = 181
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
