unit FEstadoCuentaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, ZetaKeyLookup, StdCtrls, Grids,
  DBGrids, ZetaDBGrid, ZetaDBTextBox, Mask, ZetaFecha, ZetaStateComboBox,
  Buttons;

type
  TEstadoCuentaBancaria = class(TBaseConsulta)
    Panel1: TPanel;
    grdMovimientos: TZetaDBGrid;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SALDO_INICIAL: TZetaTextBox;
    DEPOSITOS: TZetaTextBox;
    RETIROS: TZetaTextBox;
    Label5: TLabel;
    Label6: TLabel;
    SALDO_FINAL: TZetaTextBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    bbtnCalcular: TBitBtn;
    zstcmbMovimientos: TStateComboBox;
    zfchFechaFin: TZetaFecha;
    zfchFechaIni: TZetaFecha;
    zlkCuentas: TZetaKeyLookup;
    zlkTipoMovimiento: TZetaKeyLookup;
    Shape1: TShape;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbtnCalcularClick(Sender: TObject);
  protected
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;

  private
    procedure GetCuentasBancarias;
    { Private declarations }
  public
    function primerDiaDMes(Date: TDateTime):TDateTime;
    function ultimoDiaDMes(Date: TDateTime):TDateTime;
  end;

var
  EstadoCuentaBancaria: TEstadoCuentaBancaria;

implementation

uses DCajaAhorro,
     DCliente,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosTress;

{$R *.dfm}

//funcion del primer dia..
function TEstadoCuentaBancaria.primerDiaDMes(Date: TDateTime): TDateTime;
var
  Anio, Mes, Dia: Word;
begin
     DecodeDate(Date, Anio, Mes, Dia);
     Result := EncodeDate(Anio, Mes, 1);
end;

//funcion del ultimo dia
function TEstadoCuentaBancaria.ultimoDiaDMes(Date: TDateTime): TDateTime;
var
  Anio, Mes, Dia: Word;
begin
     DecodeDate(Date, Anio, Mes, Dia);
     Result := EncodeDate(Anio, Mes,MonthDays[IsLeapYear(Anio), Mes]);
end;

procedure TEstadoCuentaBancaria.FormCreate(Sender: TObject);
begin
     inherited;
     zlkCuentas.LookupDataset := dmCajaAhorro.cdsCtasBancarias;
     zlkTipoMovimiento.LookupDataset := dmCajaAhorro.cdsTiposDeposito;

     zstcmbMovimientos.ItemIndex := 0;
  
     zfchFechaIni.Valor := FirstDayOfMonth(dmCliente.FechaDefault);
     zfchFechaFin.Valor := LastDayOfMonth(dmCliente.FechaDefault);

     DataSource.DataSet := dmCajaAhorro.cdsCtasMovs;
     grdMovimientos.DataSource := DataSource;
end;

procedure TEstadoCuentaBancaria.FormShow(Sender: TObject);
begin
     inherited;
     grdMovimientos.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= H_ESTADO_CTA_BANCARIA;
     IndexDerechos:= D_AHORRO_EDO_CUENTA_BANC;
     TipoValorActivo1 := stTipoAhorro;
end;

procedure TEstadoCuentaBancaria.Connect;
begin
     inherited;
     with dmCajaAhorro do
     begin
          cdsCtasBancarias.Conectar;
          if( ( ( not cdsCtasMovs.Active ) or cdsCtasMovs.HayQueRefrescar ) ) then
          begin
               with cdsCtasBancarias do
               begin
                    try
                       Filtered := False;
                       Filter := Format( 'AH_TIPO = %s', [ EntreComillas( dmCliente.TipoAhorro ) ] );
                       Filtered := True;
                       First;
                       zlkCuentas.Llave := FieldByName( 'CT_CODIGO' ).AsString;
                    finally
                           Filtered := False;
                           Filter := VACIO;
                    end;
               end;
               zlkCuentas.Filtro := Format( 'AH_TIPO = %s', [ EntreComillas( dmCliente.TipoAhorro ) ] );
          end;
          DataSource.DataSet := cdsCtasMovs;
     end;
     Refresh;
end;

procedure TEstadoCuentaBancaria.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsCtasMovs.Agregar;
     Refresh;
end;

procedure TEstadoCuentaBancaria.Borrar;
//var sCuenta:string;
begin
     inherited;
     if ZetaDialogo.ZWarningConfirm( 'Estado de cuenta', '�Desea borrar el registro?', 0, mbCancel ) then
     begin
          //sCuenta := zlkCuentas.Llave;
          dmCajaAhorro.cdsCtasMovs.Borrar;
          //zlkCuentas.Llave := sCuenta;
          //Refresh;
     end;
end;

procedure TEstadoCuentaBancaria.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsCtasMovs.Modificar;
     Refresh;
end;

procedure TEstadoCuentaBancaria.Refresh;
 var
    oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        inherited;
        GetCuentasBancarias;
        with dmCajaAhorro do
        begin
             SALDO_INICIAL.Caption := FormatFloat( '#,0.00', SaldoInicial );
             DEPOSITOS.Caption := FormatFloat( '#,0.00', cdsCtasMovs.FieldByName('DEPOSITOS').AsFloat );
             RETIROS.Caption := FormatFloat( '#,0.00', cdsCtasMovs.FieldByName('RETIROS').AsFloat );
             SALDO_FINAL.Caption := FormatFloat( '#,0.00', cdsCtasMovs.FieldByName('SALDO').AsFloat );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEstadoCuentaBancaria.bbtnCalcularClick(Sender: TObject);
begin
     inherited;
     Refresh;
end;

procedure TEstadoCuentaBancaria.GetCuentasBancarias;
var
   oParams:TZetaParams;
begin
     oParams := TZetaParams.Create;
     with oParams do
     begin
          AddString('Cuenta',zlkCuentas.Llave);
          AddDate('FechaIni',zfchFechaIni.Valor);
          AddDate('FechaFin',zfchFechaFin.Valor);
          AddInteger('Status',zstcmbMovimientos.ItemIndex);
          AddString('Tipo',zlkTipoMovimiento.Llave);
     end;
     dmCajaAhorro.GetCuentasBancarias( oParams );
end;


end.
