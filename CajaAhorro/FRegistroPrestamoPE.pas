unit FRegistroPrestamoPE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, StdCtrls, DBCtrls, Mask, ZetaFecha, ZetaEdit,
  ZetaKeyLookup, DB, ExtCtrls, ZetaSmartLists, Buttons, ZetaNumero,
  ZetaDBTextBox, ZetaKeyCombo, ZetaCommonClasses,ZetaCommonLists;

type
  TRegistroPrestamoPE = class(TBaseEdicion)
    AH_TIPOLbl: TLabel;
    Label8: TLabel;
    PR_REFEREN: TZetaDBEdit;
    Label1: TLabel;
    PR_FECHA: TZetaDBFecha;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup;
    GBCondiciones: TGroupBox;
    LBL_Formula: TLabel;
    SBCO_FORMULA: TSpeedButton;
    Label7: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    PR_MESES: TZetaDBNumero;
    Label5: TLabel;
    PR_MONTO: TZetaDBNumero;
    PR_PAGOS: TZetaDBNumero;
    PR_PAG_PER: TZetaDBNumero;
    Label10: TLabel;
    PR_SALDO_I: TZetaDBNumero;
    GBCheque: TGroupBox;
    RegistrarCheque: TCheckBox;
    Label11: TLabel;
    CT_CODIGO: TZetaKeyLookup;
    Label12: TLabel;
    CM_CHEQUE: TZetaNumero;
    BtnSiguiente: TSpeedButton;
    Label13: TLabel;
    CM_DESCRIP: TEdit;
    ImprimeCheque: TCheckBox;
    Label14: TLabel;
    Label15: TLabel;
    CM_PRESTA: TZetaKeyCombo;
    TIPO_PRESTAMO: TZetaTextBox;
    PR_TIPO: TZetaDBTextBox;
    gbDescontar: TGroupBox;
    Label6: TLabel;
    Label9: TLabel;
    PE_YEAR: TZetaNumero;
    PE_NUMERO: TZetaKeyLookup;
    gbPagar: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    PE_YEAR2: TZetaNumero;
    PE_NUMERO2: TZetaKeyLookup;
    Label19: TLabel;
    Label20: TLabel;
    MontoAval: TZetaTextBox;
    Aval: TZetaKeyLookup;
    Label21: TLabel;
    Intereses: TZetaTextBox;
    MontoAvalReal: TZetaNumero;
    Label2: TLabel;
    SaldoDisponible: TZetaTextBox;
    TotalaDescontar: TZetaTextBox;
    lImprimeFormato: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BtnSiguienteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RegistrarChequeClick(Sender: TObject);
    procedure CT_CODIGOValidKey(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure AvalValidKey(Sender: TObject);
    procedure PE_NUMERO2ValidKey(Sender: TObject);
    procedure PR_PAGOSExit(Sender: TObject);
    procedure PE_NUMERO2ValidLookup(Sender: TObject);
    procedure MontoAvalRealExit(Sender: TObject);
  private
    { Private declarations }
    FTasa: extended;
    FInteres: currency;
    FDias: integer;
    FMontoAval: currency;
    FNetoDisponible: currency;
    procedure LlenaDatosPeriodo;
    function GetNumeroDescuento( dPago: TDate ): integer;
    procedure InitRegistrarCheque;
    procedure SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
    procedure SetFiltroPeriodo2(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
    procedure SiguienteCheque;
    procedure ImprimirReporte;
    procedure GetPrimerPeriodo;
  protected
    { Protected declarations }
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  RegistroPrestamoPE: TRegistroPrestamoPE;

implementation

uses dCajaAhorro,
     dGlobal,
     dCliente,
     dSistema,
     ZEtaDialogo,
     ZetaTipoEntidad,
     ZGlobalTress,
     ZImprimeForma,
     ZConstruyeFormula,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet,
     FTipoPrestamoAhorro,
     DReportes;

{$R *.dfm}

procedure TRegistroPrestamoPE.FormCreate(Sender: TObject);
const
     K_FILTRO_CTAS = '( AH_TIPO = %s and CT_STATUS = 0 )'; //S�lo cuentas activas
begin
     inherited;
     IndexDerechos := 0;
     HelpContext := H_REG_PRESTAMO;
     FirstControl := CB_CODIGO;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     with dmCajaAhorro do
     begin
          Aval.LookupDataset := cdsEmpleadosAval;
          PE_NUMERO.LookupDataSet := cdsPeriodo;
          PE_NUMERO2.LookupDataSet := cdsPeriodo2;
     end;
     CM_PRESTA.ItemIndex := 0;
     with CT_CODIGO do
     begin
          LookupDataset := dmCajaAhorro.cdsCtasBancarias;
          Filtro := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
     end;
end;

procedure TRegistroPrestamoPE.FormShow(Sender: TObject);
begin
     inherited;
     InitRegistrarCheque;
     LlenaDatosPeriodo;
     with dmCajaAhorro do
     begin
          TIPO_PRESTAMO.Caption := cdsTPresta.FieldByName( 'TB_ELEMENT' ).AsString;
     end;
     SaldoDisponible.Caption := VACIO;
     Intereses.Caption := VACIO;
     MontoAval.Caption := VACIO;
     MontoAvalReal.Valor := 0;
     Aval.SetLlaveDescripcion( VACIO, VACIO );
     with CT_CODIGO do
     begin
          LookupDataset := dmCajaAhorro.cdsCtasBancarias;
          dmCajaAhorro.cdsCtasBancarias.conectar;
          if dmCajaAhorro.cdsCtasBancarias.Locate( 'AH_TIPO', VarArrayOf( [ dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_CODIGO' ).AsString ] ), [] ) then
             Llave := dmCajaAhorro.cdsCtasBancarias.FieldByName( 'CT_CODIGO' ).AsString;
     end;
     SiguienteCheque;
     if ( dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger = 1 ) then
        FNetoDisponible := dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat
     else
     begin
          if ( dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat <
               dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'SubTotalFondoAhorro' ).AsFloat ) then
             FNetoDisponible := dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat
          else
              FNetoDisponible := dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'SubTotalFondoAhorro' ).AsFloat;
     end;
     SaldoDisponible.Caption := FormatFloat( '$##.##', FNetoDisponible );
     Intereses.Caption := FormatFloat( '$##.##', 0 );
     TotalADescontar.Caption := FormatFloat( '$##.##', 0 );
     lImprimeFormato.Checked := True;
end;

function TRegistroPrestamoPE.GetNumeroDescuento( dPago: TDate ): integer;
begin
     dmCajaAhorro.ConectarPeriodoBusqueda( dmCliente.GetDatosPeriodoActivo.Year, eTipoPeriodo( dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_NOMINA' ).AsInteger ) );
     with dmCajaAhorro.cdsPeriodoBusqueda do
     begin
          First;
          Result := FieldByName( 'PE_NUMERO' ).AsInteger;
          while not EOF do
          begin
               if ( FieldByName( 'PE_FEC_INI' ).AsDateTime <= dPago ) and
                  ( FieldByName( 'PE_FEC_FIN' ).AsDateTime >= dPago ) then
               begin
                    Result := FieldByName( 'PE_NUMERO' ).AsInteger;
                    Break;
               end;
               Next;
          end;
     end;
end;

procedure TRegistroPrestamoPE.Connect;
begin
     CB_CODIGO.ResetMemory;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCajaAhorro do
     begin
          cdsEmpleadosAval.Conectar;
          cdsCtasBancarias.Conectar;
          ConectarPeriodo( dmCliente.GetDatosPeriodoActivo.Year, eTipoPeriodo( dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ) );
          ConectarPeriodo2( dmCliente.GetDatosPeriodoActivo.Year, eTipoPeriodo( dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ) );
          Datasource.Dataset := cdsRegPrestamos;
     end;
     GetPrimerPeriodo;
end;

procedure TRegistroPrestamoPE.GetPrimerPeriodo;
begin
     with dmCajaAhorro.cdsPeriodo2 do
     begin
          First;
          PE_YEAR2.Valor := FieldByName( 'PE_YEAR' ).AsInteger;
          PE_NUMERO2.Valor := FieldByName( 'PE_NUMERO' ).AsInteger;
          PE_YEAR.Valor := FieldByName( 'PE_YEAR' ).AsInteger;
          PE_NUMERO.Valor := GetNumeroDescuento( FieldByName( 'PE_FEC_PAG' ).AsDateTime );
     end;
end;

procedure TRegistroPrestamoPE.EscribirCambios;
begin
     if RegistrarCheque.Checked then
     begin
          if ( CM_CHEQUE.ValorEntero > 0 ) then
          begin
               if not dmCajaAhorro.ValidarCheque( CT_CODIGO.Llave, CM_CHEQUE.ValorEntero ) then
               begin
                    with dmCajaAhorro do
                    begin
                         with cdsRegCtasMovs do
                         begin
                              PostData;
                              if ( not IsEmpty ) then
                              begin
                                   EmptyDataSet;
                              end;
                              Append;
                              FieldByName( 'CT_CODIGO' ).AsString := CT_CODIGO.Llave;
                              FieldByName( 'CM_TIPO' ).AsString := 'PRESTA';
                              FieldByName( 'CM_DEP_RET' ).AsString := 'R';
                              FieldByName( 'CM_DESCRIP' ).AsString := CM_DESCRIP.Text;
                              FieldByName( 'CM_CHEQUE' ).AsInteger := CM_CHEQUE.ValorEntero;
                              FieldByName( 'CM_STATUS' ).AsInteger := Ord( saActivo );
                              FieldByName( 'CM_PRESTA' ).AsInteger := CM_PRESTA.ItemIndex;
                              Post;
                         end;
                         RegImprimirCheque := self.ImprimeCheque.Checked;
                    end;
                    inherited EscribirCambios;
               end
               else
                   ZetaDialogo.ZError( 'Prestamos', 'N�mero de cheque repetido. Es necesario cambiarlo.', 0 );
          end
          else
          ZetaDialogo.ZError( 'Prestamos', 'Capturar n�mero de cheque', 0 );
     end
     else
     inherited EscribirCambios;
end;

procedure TRegistroPrestamoPE.LlenaDatosPeriodo;
begin
     {with dmCliente.GetDatosPeriodoActivo do
     begin
          Self.TipoNomina.Caption := ObtieneElemento( lfTipoPeriodo, Ord( Tipo ) );
          Self.NumeroNomina.Caption := IntToStr( Numero );
          Self.PeriodoFechaInicial.Caption := FechaCorta( Inicio );
          Self.PeriodoFechaFinal.Caption := FechaCorta( Fin );
     end;}
end;

procedure TRegistroPrestamoPE.Agregar;
begin
     { No hace nada }
end;

procedure TRegistroPrestamoPE.Borrar;
begin
     { No hace nada }
end;

procedure TRegistroPrestamoPE.InitRegistrarCheque;
var
   lPuedeRegistrarCheque, lPuedeImprimir: Boolean;
   sCtaBancaria : String;
begin
     sCtaBancaria := VACIO;
     lPuedeImprimir := FALSE;
     with dmCajaAhorro.cdsCtasBancarias do
     begin
          Filter := CT_CODIGO.Filtro;
          Filtered := TRUE;
          try
             lPuedeRegistrarCheque := ( not IsEmpty );
             if lPuedeRegistrarCheque then
             begin
                  First;
                  sCtaBancaria := FieldByName( 'CT_CODIGO' ).AsString;
                  lPuedeImprimir := ( FieldByName( 'CT_REP_CHK' ).AsInteger > 0 );
             end;
          finally
                 Filtered := FALSE;
                 Filter := VACIO;
          end;
     end;
     with RegistrarCheque do
     begin
          Enabled := lPuedeRegistrarCheque and False;
          Checked := lPuedeRegistrarCheque;
     end;
     CT_CODIGO.Llave := sCtaBancaria;
     CM_CHEQUE.Valor := 0;
     CM_DESCRIP.Text := VACIO;
     ImprimeCheque.Checked := lPuedeImprimir;
end;

{ Eventos de controles }

procedure TRegistroPrestamoPE.RegistrarChequeClick(Sender: TObject);
begin
     inherited;
     ZetaClientTools.SetEnabledControl( GBCheque, RegistrarCheque.Checked );
end;

procedure TRegistroPrestamoPE.SiguienteCheque;
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
        CM_CHEQUE.Valor := dmCajaAhorro.SiguienteCheque( CT_CODIGO.Llave );
end;

procedure TRegistroPrestamoPE.BtnSiguienteClick(Sender: TObject);
begin
     inherited;
     SiguienteCheque;
end;

procedure TRegistroPrestamoPE.CT_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
     begin
          with dmCajaAhorro.cdsCtasBancarias do
          begin
               Locate( 'CT_CODIGO', CT_CODIGO.Llave, [] );
               self.ImprimeCheque.Checked := ( FieldByName( 'CT_REP_CHK' ).AsInteger > 0 );
          end;
     end;
end;

procedure TRegistroPrestamoPE.OKClick(Sender: TObject);
var
   dNetoDisponible: currency;
   //Variables de campos de la tabla prestamo
   dPR_ABONOS,dPR_CARGOS,dPR_SALDO_I,dPR_TOTAL, rExcepcion: currency;
   dPR_PAG_PER : currency;
   sPR_FORMULA,sPR_REFEREN,sPR_TIPO,sPR_SUB_CTA: string;
   iPR_NUMERO,iPR_STATUS,iUS_CODIGO,iCB_CODIGO,iPR_MESES,iPR_PAGOS: integer;
   lOK: boolean;
   dFechaFinalAhorro, dFechaSemanas: Tdate;

   function GetFechaInicioPeriodo( const iNumeroPeriodo: integer ): TDate;
   begin
        with dmCajaAhorro do
        begin
             ConectarPeriodoBusqueda( YearEmpleado, eTipoPeriodo( TipoNominaEmpleado ) );
             cdsPeriodoBusqueda.Filter := Format( 'PE_TIPO = %d and PE_YEAR = %d and PE_NUMERO = %d', [ TipoNominaEmpleado,
                                                                                                        YearEmpleado,
                                                                                                        iNumeroPeriodo ] );
             cdsPeriodoBusqueda.Filtered := True;
             try
                Result := cdsPeriodoBusqueda.FieldByName( 'PE_FEC_INI' ).AsDateTime;
             finally
                    cdsPeriodoBusqueda.Filter := VACIO;
                    cdsPeriodoBusqueda.Filtered := False;
             end;
        end;
   end;

   function NumeroSemanas: boolean;
   begin
        Global.Conectar;
        if ( dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger = 1 ) then
           dFechaFinalAhorro := StrToFecha( Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL3 ) )           //Caja de Ahorro
        else
            dFechaFinalAhorro := StrToFecha( Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL2 ) );         //Fondo de Ahorro
        dFechaSemanas := GetFechaInicioPeriodo( PE_NUMERO.Valor ) + ( dmCajaAhorro.cdsRegPrestamos.FieldByName( 'PR_PAGOS' ).AsInteger * 7 );
        Result := ( dFechaSemanas < dFechaFinalAhorro );
   end;

begin
     if ( Self.PR_MONTO.Valor > 0 ) and
        ( Self.PR_PAGOS.Valor > 0 ) then
     begin
          { Validar que le monto sea menor al disponible }
          dNetoDisponible := dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'Neto' ).AsFloat;
          with dmCajaAhorro do
          begin
               PeriodoEmpleado := PE_NUMERO2.Valor;
               TipoNominaEmpleado := dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_NOMINA' ).AsInteger;
               YearEmpleado := dmCliente.GetDatosPeriodoActivo.Year;
          end;

          if NumeroSemanas then
          begin
               rExcepcion := PR_MONTO.Valor;
               lOK := True;
               { Si Hay Aval se les Suma el Monto }
               if strLleno( Aval.Llave ) then
               begin
                    if ( MontoAvalReal.Valor > 0 ) then
                       dNetoDisponible := dnetoDisponible + MontoAvalReal.Valor
                    else
                        lOK := False;
               end;
               if lOK then
               begin
                    { Caja de Ahorro }
                    if ( dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger = 1 ) then
                    begin
                         if ( PR_MONTO.Valor <= dNetoDisponible ) then
                         begin
                              if ( ( PR_MONTO.Valor + FInteres ) <= dNetoDisponible ) then
                              begin
                                   with dmCajaAhorro.cdsRegPrestamos do
                                   begin
                                        dPR_ABONOS := FieldByName( 'PR_ABONOS' ).AsFloat;
                                        dPR_CARGOS := FieldByName( 'PR_CARGOS' ).AsFloat;
                                        dPR_SALDO_I := FieldByName( 'PR_SALDO_I' ).AsFloat;
                                        dPR_TOTAL := FieldByName( 'PR_TOTAL' ).AsFloat;

                                        FieldByName( 'PR_SALDO' ).AsFloat := FieldByName( 'PR_MONTO' ).AsFloat;

                                        FieldByName( 'PR_TASA' ).AsFloat := FTasa;
                                        dPR_PAG_PER := FieldByName( 'PR_PAG_PER' ).AsFloat;

                                        FieldByName( 'PR_FECHA' ).AsDateTime := GetFechaInicioPeriodo( PE_NUMERO.Valor );
                                        FieldByName( 'PR_FORMULA' ).AsString := FloatToStr( FieldByName( 'PR_MONTO' ).AsFloat / FieldByName( 'PR_PAGOS' ).AsInteger );
                                        sPR_FORMULA := FieldByName( 'PR_FORMULA' ).AsString;
                                        sPR_REFEREN := PR_REFEREN.Valor;
                                        sPR_TIPO := FieldByName( 'PR_TIPO' ).AsString;
                                        sPR_SUB_CTA := FieldByName( 'PR_SUB_CTA' ).AsString;
                                        iPR_NUMERO := FieldByName( 'PR_NUMERO' ).AsInteger;
                                        iPR_STATUS := FieldByName( 'PR_STATUS' ).AsInteger;
                                        iUS_CODIGO := FieldByName( 'US_CODIGO' ).AsInteger;
                                        iCB_CODIGO := FieldByName( 'CB_CODIGO' ).AsInteger;
                                        iPR_MESES := FieldByName( 'PR_MESES' ).AsInteger;
                                        iPR_PAGOS := FieldByName( 'PR_PAGOS' ).AsInteger;

                                        { Quitar intereses, seran almacenados en un prestamo aparte }
                                        FieldByName( 'PR_INTERES' ).AsFloat := 0;
                                        FieldByName( 'PR_TASA' ).AsFloat := 0;
                                        if ( MontoAvalReal.Valor > 0 ) then
                                        begin
                                             FieldByName( 'PR_AVAL' ).AsInteger := StrToInt( Aval.Llave );
                                             FieldByName( 'PR_MONTO_A' ).AsFloat := MontoAvalReal.Valor;
                                             FieldByName( 'PR_FEC_AVA' ).AsDateTime := dmCliente.FechaDefault;
                                        end;
                                        Post;
                                   end;

                                   { Almacenar el segundo registro (intereses) }
                                   with dmCajaAhorro.cdsRegPrestamos do
                                   begin
                                        Append;
                                        //Cambiar Tipo de Prestamo a Intereses
                                        sPR_TIPO := dmCajaAhorro.GetTipoInteres( dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger );
                                        FieldByName( 'PR_TIPO' ).AsString := sPR_TIPO;
                                        //Cambiar el monto solicitado al monto de interes
                                        FieldByName( 'PR_MONTO_S' ).AsFloat := 0;
                                        FieldByName( 'PR_MONTO' ).AsFloat := FInteres;
                                        FieldByName( 'PR_SALDO' ).AsFloat := FieldByName( 'PR_MONTO' ).AsFloat;
                                        //Intereses en cero
                                        FieldByName( 'PR_INTERES' ).AsFloat := 0;
                                        FieldByName( 'PR_TASA' ).AsFloat := FTasa;

                                        FieldByName( 'PR_ABONOS' ).AsFloat := dPR_ABONOS;
                                        FieldByName( 'PR_CARGOS' ).AsFloat := dPR_CARGOS;
                                        FieldByName( 'PR_SALDO_I' ).AsFloat := dPR_SALDO_I;
                                        FieldByName( 'PR_TOTAL' ).AsFloat := dPR_TOTAL;
                                        FieldByName( 'PR_PAG_PER' ).AsFloat := dPR_PAG_PER;
                                        FieldByName( 'PR_FECHA' ).AsDateTime := GetFechaInicioPeriodo( PE_NUMERO.Valor );
                                        FieldByName( 'PR_PAGOS' ).AsInteger := iPR_PAGOS;
                                        FieldByName( 'PR_FORMULA' ).AsString := FloatToStr( FieldByName( 'PR_MONTO' ).AsFloat / FieldByName( 'PR_PAGOS' ).AsInteger );
                                        FieldByName( 'PR_REFEREN' ).AsString := sPR_REFEREN;
                                        FieldByName( 'PR_SUB_CTA' ).AsString := sPR_SUB_CTA;
                                        FieldByName( 'PR_NUMERO' ).AsInteger := iPR_NUMERO;
                                        FieldByName( 'PR_STATUS' ).AsInteger := iPR_STATUS;
                                        FieldByName( 'US_CODIGO' ).AsInteger := iUS_CODIGO;
                                        FieldByName( 'CB_CODIGO' ).AsInteger := iCB_CODIGO;
                                        FieldByName( 'PR_MESES' ).AsInteger := iPR_MESES;

                                        { Obtener el siguiente cheque }
                                        BtnSiguienteClick( nil );
                                        Post;
                                   end;

                                   with dmCajaAhorro.cdsExcepMontos do
                                   begin
                                        Refrescar;
                                        Append;
                                        FieldByName( 'CB_CODIGO' ).AsInteger := StrToInt( CB_CODIGO.Llave );
                                        dmCajaAhorro.cdsTPresta.Conectar;
                                        dmCajaAhorro.cdsTPresta.Locate( 'TB_CODIGO', sPR_TIPO, [] );
                                        FieldByName( 'CO_NUMERO' ).AsInteger := dmCajaAhorro.cdsTPresta.FieldByName( 'TB_PAGO' ).AsInteger;
                                        FieldByName( 'MO_PERCEPC' ).AsFloat := rExcepcion;
                                        Post;
                                        Enviar;
                                   end;

                                   inherited;

                                   if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
                                      ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then        //y no se tienen cambios pendientes
                                   begin
                                        ImprimirReporte;
                                        Close;
                                   end;
                              end
                              else
                              begin
                                   ZetaDialogo.ZError( 'Caja Ahorro', Format( 'Monto solicitado m�s intereses excede la suma disponible [$%s]', [ FormatFloat( '###,##0.00', dNetoDisponible ) ] ), 0 );
                              end;
                         end
                         else
                         begin
                              ZetaDialogo.ZError( 'Caja Ahorro', Format( 'Monto solicitado excede la suma disponible [$%s]', [ FormatFloat( '###,##0.00', dNetoDisponible ) ] ), 0 );
                         end;
                    end
                    else
                    begin
                         { Fondo de Ahorro }
                         { Solo valida que se tome en cuenta el Sub Total de Fondo de Ahorro }
                         if dNetoDisponible > dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'SubTotalFondoAhorro' ).AsFloat then
                            dNetoDisponible := dmCajaAhorro.cdsTotalHisAhorros.FieldByName( 'SubTotalFondoAhorro' ).AsFloat;
                         if ( PR_MONTO.Valor <= dNetoDisponible ) then
                         begin
                              if ( ( PR_MONTO.Valor + FInteres ) <= dNetoDisponible ) then
                              begin
                                   with dmCajaAhorro.cdsRegPrestamos do
                                   begin
                                        dPR_ABONOS := FieldByName( 'PR_ABONOS' ).AsFloat;
                                        dPR_CARGOS := FieldByName( 'PR_CARGOS' ).AsFloat;
                                        dPR_SALDO_I := FieldByName( 'PR_SALDO_I' ).AsFloat;
                                        dPR_TOTAL := FieldByName( 'PR_TOTAL' ).AsFloat;

                                        FieldByName( 'PR_SALDO' ).AsFloat := FieldByName( 'PR_MONTO' ).AsFloat;

                                        FieldByName( 'PR_TASA' ).AsFloat := FTasa;
                                        dPR_PAG_PER := FieldByName( 'PR_PAG_PER' ).AsFloat;

                                        FieldByName('PR_FECHA').AsDateTime := GetFechaInicioPeriodo( PE_NUMERO.Valor );
                                        FieldByName( 'PR_FORMULA' ).AsString := FloatToStr( FieldByName( 'PR_MONTO' ).AsFloat / FieldByName( 'PR_PAGOS' ).AsInteger );
                                        sPR_FORMULA := FieldByName( 'PR_FORMULA' ).AsString;
                                        sPR_REFEREN := PR_REFEREN.Valor;
                                        sPR_TIPO := FieldByName( 'PR_TIPO' ).AsString;
                                        sPR_SUB_CTA := FieldByName( 'PR_SUB_CTA' ).AsString;
                                        iPR_NUMERO := FieldByName( 'PR_NUMERO' ).AsInteger;
                                        iPR_STATUS := FieldByName( 'PR_STATUS' ).AsInteger;
                                        iUS_CODIGO := FieldByName( 'US_CODIGO' ).AsInteger;
                                        iCB_CODIGO := FieldByName( 'CB_CODIGO' ).AsInteger;
                                        iPR_MESES := FieldByName( 'PR_MESES' ).AsInteger;
                                        iPR_PAGOS := FieldByName( 'PR_PAGOS' ).AsInteger;

                                        { Quitar intereses, seran almacenados en un prestamo aparte }
                                        FieldByName( 'PR_INTERES' ).AsFloat := 0;
                                        FieldByName( 'PR_TASA' ).AsFloat := 0;
                                        if ( MontoAvalReal.Valor > 0 ) then
                                        begin
                                             FieldByName( 'PR_AVAL' ).AsInteger := StrToInt( Aval.Llave );
                                             FieldByName( 'PR_MONTO_A' ).AsFloat := MontoAvalReal.Valor;
                                             FieldByName( 'PR_FEC_AVA' ).AsDateTime := dmCliente.FechaDefault;
                                        end;

                                        Post;
                                   end;
                                   with dmCajaAhorro.cdsRegPrestamos do
                                   begin
                                        Append;
                                        //Cambiar Tipo de Prestamo a Intereses
                                        sPR_TIPO := dmCajaAhorro.GetTipoInteres( dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger );
                                        FieldByName( 'PR_TIPO' ).AsString := sPR_TIPO;
                                        //Cambiar el monto solicitado al monto de interes
                                        FieldByName( 'PR_MONTO_S' ).AsFloat := 0;
                                        FieldByName( 'PR_MONTO' ).AsFloat := FInteres;
                                        FieldByName( 'PR_SALDO' ).AsFloat := FieldByName( 'PR_MONTO' ).AsFloat;
                                        //Intereses en cero
                                        FieldByName( 'PR_INTERES' ).AsFloat := 0;
                                        FieldByName( 'PR_TASA' ).AsFloat := FTasa;

                                        FieldByName( 'PR_ABONOS' ).AsFloat := dPR_ABONOS;
                                        FieldByName( 'PR_CARGOS' ).AsFloat := dPR_CARGOS;
                                        FieldByName( 'PR_SALDO_I' ).AsFloat := dPR_SALDO_I;
                                        FieldByName( 'PR_TOTAL' ).AsFloat := dPR_TOTAL;
                                        FieldByName( 'PR_PAG_PER' ).AsFloat := dPR_PAG_PER;
                                        FieldByName( 'PR_FECHA' ).AsDateTime := GetFechaInicioPeriodo( PE_NUMERO.Valor );
                                        FieldByName( 'PR_PAGOS' ).AsInteger := iPR_PAGOS;
                                        FieldByName( 'PR_FORMULA' ).AsString := FloatToStr( FieldByName( 'PR_MONTO' ).AsFloat / FieldByName( 'PR_PAGOS' ).AsInteger );
                                        FieldByName( 'PR_REFEREN' ).AsString := sPR_REFEREN;
                                        FieldByName( 'PR_SUB_CTA' ).AsString := sPR_SUB_CTA;
                                        FieldByName( 'PR_NUMERO' ).AsInteger := iPR_NUMERO;
                                        FieldByName( 'PR_STATUS' ).AsInteger := iPR_STATUS;
                                        FieldByName( 'US_CODIGO' ).AsInteger := iUS_CODIGO;
                                        FieldByName( 'CB_CODIGO' ).AsInteger := iCB_CODIGO;
                                        FieldByName( 'PR_MESES' ).AsInteger := iPR_MESES;

                                        { Obtener el siguiente cheque }
                                        BtnSiguienteClick( nil );
                                        Post;
                                   end;

                                   with dmCajaAhorro.cdsExcepMontos do
                                   begin
                                        Refrescar;
                                        Append;
                                        FieldByName( 'CB_CODIGO' ).AsInteger := StrToInt( CB_CODIGO.Llave );
                                        dmCajaAhorro.cdsTPresta.Conectar;
                                        dmCajaAhorro.cdsTPresta.Locate( 'TB_CODIGO', sPR_TIPO, [] );
                                        FieldByName( 'CO_NUMERO' ).AsInteger := dmCajaAhorro.cdsTPresta.FieldByName( 'TB_PAGO' ).AsInteger;
                                        FieldByName( 'MO_PERCEPC' ).AsFloat := rExcepcion;
                                        Post;
                                        Enviar;
                                   end;

                                   { Finalizar proceso }
                                   inherited;

                                   if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
                                      ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then        //y no se tienen cambios pendientes
                                   begin
                                        ImprimirReporte;
                                        Close;
                                   end;
                              end
                              else
                                  ZetaDialogo.ZError( 'Fondo Ahorro', Format( 'Monto solicitado m�s intereses excede la suma disponible de fondo de ahorro [$%s]', [ FormatFloat( '###,##0.00', dNetoDisponible ) ] ), 0 );
                         end
                         else
                             ZetaDialogo.ZError( 'Fondo Ahorro', Format( 'Monto solicitado excede la suma disponible de fondo de ahorro [$%s]', [ FormatFloat( '###,##0.00', dNetoDisponible ) ] ), 0 );
                    end;
               end
               else
                   ZetaDialogo.ZError( 'Fondo Ahorro', 'Monto de aval solicitado deber ser mayor a cero', 0 );
          end
          else
              ZetaDialogo.ZError( 'Caja Ahorro', Format( 'La fecha final de Pago [%s] excede la fecha final del Ahorro [%s]', [ FechaCorta( dFechaSemanas ), FechaCorta( dFechaFinalAhorro ) ] ), 0 );
     end
     else
         ZetaDialogo.ZError( 'Caja Ahorro', 'Monto a prestar y n�mero de semanas no pueden ser cero', 0 );
end;

procedure TRegistroPrestamoPE.ImprimirReporte;
begin
     if lImprimeFormato.Checked then
     begin
          dmSistema.cdsusuarios.Conectar;
          with dmCajaAhorro.cdsTAhorro do
          begin
               if FieldByName( 'TB_REP_PRE' ).AsInteger <> 0 then
                  dmReportes.ImprimeUnaForma( VACIO, FieldByName( 'TB_REP_PRE' ).AsInteger );
          end;
     end;
end;

procedure TRegistroPrestamoPE.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enPrestamo, dmCajaAhorro.cdsRegPrestamos );
end;

function TRegistroPrestamoPE.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroPrestamoPE.SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodo.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodo( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodo( iYear, TipoPeriodo );

          with PE_NUMERO do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
               begin
                    if Ord( dmCliente.GetDatosPeriodoActivo.Status ) = 6 then
                       Valor := dmCliente.GetDatosPeriodoActivo.Numero + 1
                    else
                        Valor := dmCliente.GetDatosPeriodoActivo.Numero;
               end
               else
                   Valor := 1;
          end;
     end;
end;

procedure TRegistroPrestamoPE.SetFiltroPeriodo2(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodo2.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodo2.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodo2.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodo2( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodo2( iYear, TipoPeriodo );

          with PE_NUMERO2 do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
               begin
                    if Ord( dmCliente.GetDatosPeriodoActivo.Status ) = 6 then
                       Valor := dmCliente.GetDatosPeriodoActivo.Numero + 1
                    else
                        Valor := dmCliente.GetDatosPeriodoActivo.Numero;
               end
               else
                   Valor := 1;
          end;
     end;
end;

procedure TRegistroPrestamoPE.AvalValidKey(Sender: TObject);
begin
     inherited;
     if strLleno( Aval.Llave ) then
     begin
          FMontoAval := dmCajaAhorro.cdsEmpleadosAval.FieldByName( 'AH_SALDO' ).AsFloat;
          MontoAval.Caption := FloatToStr( FMontoAval );
     end
     else
     begin
          MontoAval.Caption := VACIO;
          FMontoAval := 0;
     end;
end;

procedure TRegistroPrestamoPE.PE_NUMERO2ValidKey(Sender: TObject);
begin
     inherited;
     with dmCajaAhorro.cdsPeriodo2 do
     begin
          PE_NUMERO.Valor := GetNumeroDescuento( FieldByName( 'PE_FEC_PAG' ).AsDateTime );
     end;
end;

procedure TRegistroPrestamoPE.PR_PAGOSExit(Sender: TObject);
var
   rTotalaDescontar: currency;
begin
     inherited;
     FTasa := ( 0.10 / 360 );
     FDias := ( PR_PAGOS.ValorEntero * 7 );
     FInteres := PR_MONTO.Valor * ( FDias * FTasa );
     Intereses.Caption := FormatFloat( '$##.##', FInteres );
     if ( PR_PAGOS.ValorEntero > 0 ) then
     begin
          rTotalaDescontar := ( FInteres / PR_PAGOS.ValorEntero );
          rTotalADescontar := rTotalADescontar + ( PR_MONTO.Valor / PR_PAGOS.ValorEntero );
     end
     else
         rTotalADescontar := 0;
     TotalADescontar.Caption := FormatFloat( '$##.##', rTotalADescontar );
end;

procedure TRegistroPrestamoPE.PE_NUMERO2ValidLookup(Sender: TObject);
begin
     inherited;
     with dmCajaAhorro.cdsPeriodo2 do
     begin
          PE_NUMERO.Valor := GetNumeroDescuento( FieldByName( 'PE_FEC_PAG' ).AsDateTime );
     end;
end;

procedure TRegistroPrestamoPE.MontoAvalRealExit(Sender: TObject);
begin
     inherited;
     if ( MontoAvalReal.Valor > 0 ) then
     begin
          if MontoAvalReal.Valor > dmCajaAhorro.cdsEmpleadosAval.FieldByName( 'AH_SALDO' ).AsFloat then
          begin
               ZetaDialogo.ZError( 'Caja Ahorro', 'Monto seleccionado no puede ser mayor a monto total de Aval', 0 );
               MontoAvalReal.Valor := dmCajaAhorro.cdsEmpleadosAval.FieldByName( 'AH_SALDO' ).AsFloat;
               MontoAvalReal.SetFocus;
          end;
     end;
end;

end.
