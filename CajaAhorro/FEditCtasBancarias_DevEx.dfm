inherited EditCtasBancarias_DevEx: TEditCtasBancarias_DevEx
  Left = 1179
  Top = 491
  Caption = 'Cuenta bancaria'
  ClientHeight = 360
  ClientWidth = 472
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 73
    Top = 41
    Width = 36
    Height = 13
    Caption = 'C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 69
    Top = 63
    Width = 40
    Height = 13
    Caption = 'Nombre:'
  end
  object Label3: TLabel [2]
    Left = 18
    Top = 86
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero de cuenta:'
  end
  object Label4: TLabel [3]
    Left = 75
    Top = 110
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Banco:'
  end
  object AH_TIPOLbl: TLabel [4]
    Left = 75
    Top = 134
    Width = 34
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ahorro:'
  end
  object Label8: TLabel [5]
    Left = 286
    Top = 41
    Width = 33
    Height = 13
    Caption = 'Status:'
  end
  inherited PanelBotones: TPanel
    Top = 324
    Width = 472
    inherited OK_DevEx: TcxButton
      Left = 305
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 384
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 472
    inherited ValorActivo2: TPanel
      Width = 146
      inherited textoValorActivo2: TLabel
        Width = 140
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 11
  end
  object CT_CODIGO: TZetaDBEdit [9]
    Left = 113
    Top = 37
    Width = 75
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
    ConfirmEdit = True
    DataField = 'CT_CODIGO'
    DataSource = DataSource
  end
  object CT_NOMBRE: TDBEdit [10]
    Left = 113
    Top = 59
    Width = 315
    Height = 21
    DataField = 'CT_NOMBRE'
    DataSource = DataSource
    TabOrder = 5
  end
  object CT_NUM_CTA: TDBEdit [11]
    Left = 113
    Top = 82
    Width = 315
    Height = 21
    DataField = 'CT_NUM_CTA'
    DataSource = DataSource
    TabOrder = 6
  end
  object CT_BANCO: TDBEdit [12]
    Left = 113
    Top = 106
    Width = 315
    Height = 21
    DataField = 'CT_BANCO'
    DataSource = DataSource
    TabOrder = 7
  end
  object AH_TIPO: TZetaDBKeyLookup_DevEx [13]
    Left = 113
    Top = 130
    Width = 340
    Height = 21
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 8
    TabStop = True
    WidthLlave = 60
    DataField = 'AH_TIPO'
    DataSource = DataSource
  end
  object GBFormatos: TcxGroupBox [14]
    Left = 8
    Top = 159
    Caption = ' Formatos de impresi'#243'n '
    TabOrder = 9
    Height = 81
    Width = 457
    object FormatoLBL: TLabel
      Left = 56
      Top = 22
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheques:'
      Transparent = True
    end
    object Label5: TLabel
      Left = 44
      Top = 46
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Liquidaci'#243'n:'
      Transparent = True
    end
    object CT_REP_CHK: TZetaDBKeyLookup_DevEx
      Left = 104
      Top = 18
      Width = 340
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'CT_REP_CHK'
      DataSource = DataSource
    end
    object CT_REP_LIQ: TZetaDBKeyLookup_DevEx
      Left = 104
      Top = 42
      Width = 340
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
      DataField = 'CT_REP_LIQ'
      DataSource = DataSource
    end
  end
  object GBAdicionales: TcxGroupBox [15]
    Left = 8
    Top = 240
    Caption = ' Adicionales '
    TabOrder = 10
    Height = 81
    Width = 457
    object Label6: TLabel
      Left = 61
      Top = 20
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
      Transparent = True
    end
    object Label7: TLabel
      Left = 71
      Top = 44
      Width = 30
      Height = 13
      Caption = 'Texto:'
      Transparent = True
    end
    object CT_NUMERO: TZetaDBNumero
      Left = 105
      Top = 16
      Width = 121
      Height = 21
      Mascara = mnNumeroGlobal
      TabOrder = 0
      Text = '0.00'
      UseEnterKey = True
      DataField = 'CT_NUMERO'
      DataSource = DataSource
    end
    object CT_TEXTO: TDBEdit
      Left = 105
      Top = 40
      Width = 274
      Height = 21
      DataField = 'CT_TEXTO'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object CT_STATUS: TZetaDBKeyCombo [16]
    Left = 323
    Top = 37
    Width = 105
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 4
    ListaFija = lfStatusAhorro
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'CT_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 105
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 440
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670032
  end
end
