unit FTipoAhorro;

interface

uses Controls, ExtCtrls, Buttons, Dialogs,
     ZBaseDlgModal,
     ZBaseTablasConsulta,
     ZAccesosTress;

type
  TipoTAhorro = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;


implementation

uses DTablas,
     DGlobal,
     ZetaBuscador,
     ZGlobalTress,
     DCajaAhorro,
     ZetaCommonClasses;


{ TipoTAhorro }
procedure TipoTAhorro.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmCajaAhorro.cdsTAhorro;
     HelpContext := H_CAT_TIPO_AHORRO;
     IndexDerechos:= D_TAB_NOM_TIPO_AHORRO;
end;

end.
