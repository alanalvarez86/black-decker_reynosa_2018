unit FTotalesCajaFondo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaDBTextBox, StdCtrls;

type
  TTotalesCajaFondo = class(TBaseConsulta)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SOCIOS_ACTIVOS: TZetaDBTextBox;
    TOTAL_AHORRADO: TZetaDBTextBox;
    PRESTAMOS_ACTIVOS: TZetaDBTextBox;
    SALDO_PRESTAMOS: TZetaDBTextBox;
    labels: TLabel;
    SALDO_BANCOS: TZetaDBTextBox;
    grdCuentas: TZetaDBGrid;
    dsCuentas: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Refresh;override;
  private
    { Private declarations }
  public
    { Public declarations }
    function PuedeImprimir( var sMensaje: String ): Boolean; override;
  end;

var
  TotalesCajaFondo: TTotalesCajaFondo;

implementation


uses ZetaCommonLists,
     ZetaCommonClasses,
     dCajaAhorro,
     ZAccesosTress,
     ZAccesosMgr,
     ZetaDialogo;

{$R *.dfm}

procedure TTotalesCajaFondo.Connect;
begin
     inherited;
     with dmCajaAhorro do
     begin
          cdsTotalesFondoCaja.Refrescar;
          Datasource.Dataset := cdsTotalesFondoCaja;
          dsCuentas.DataSet := cdsTotalesCuentas;
     end;
end;

procedure TTotalesCajaFondo.FormCreate(Sender: TObject);
begin
     inherited;
     grdCuentas.Options := [ dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect ];
     HelpContext:= H_SALDO_CAJA_AHORRO;
     TipoValorActivo1 := stTipoAhorro;
end;

procedure TTotalesCajaFondo.FormShow(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_AHORRO_CAT_CUENTAS_BANC;
end;

procedure TTotalesCajaFondo.Refresh;
begin
     inherited;
     dmCajaAhorro.cdsTotalesFondoCaja.Refrescar;
end;

procedure TTotalesCajaFondo.Agregar;
begin
     inherited;
     dmCajaAhorro.cdsTotalesCuentas.Agregar;
     Refresh;
end;

procedure TTotalesCajaFondo.Modificar;
begin
     inherited;
     dmCajaAhorro.cdsTotalesCuentas.Modificar;
     Refresh;
end;

procedure TTotalesCajaFondo.Borrar;
begin
     inherited;
     dmCajaAhorro.cdsTotalesCuentas.Borrar;
     Refresh;
end;

function TTotalesCajaFondo.PuedeImprimir(var sMensaje: String): Boolean;
begin
     Result:= CheckDerecho( D_AHORRO_TOT_CAJA_FONDO, K_DERECHO_IMPRESION);
     if not Result then
        sMensaje:='No tiene Permiso para Imprimir Registros';
end;



end.
