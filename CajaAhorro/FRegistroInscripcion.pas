unit FRegistroInscripcion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaNumero, Mask, ZetaFecha, ZetaKeyLookup;

type
  TRegistroInscripcion = class(TBaseEdicion)
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup;
    AH_FECHA: TZetaDBFecha;
    Label1: TLabel;
    Label18: TLabel;
    AH_SALDO_I: TZetaDBNumero;
    GroupBox1: TGroupBox;
    AH_FORMULA: TDBEdit;
    cbPorcentaje: TCheckBox;
    nPorcentaje: TZetaNumero;
    lbFormula: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure cbPorcentajeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    FCiclo: Boolean;
    procedure AplicarFiltro;
    { Private declarations }
  protected
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Connect;override;
    procedure EscribirCambios;override;
  public
    { Public declarations }
    property Ciclo: Boolean read FCiclo write FCiclo;
  end;

var
  RegistroInscripcion: TRegistroInscripcion;

implementation
uses
    DCajaAhorro,
    DCliente,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists
    ;

{$R *.dfm}

procedure TRegistroInscripcion.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0; // PENDIENTE
     HelpContext := H_INSCRIPCION; 
     FirstControl := AH_SALDO_I;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

procedure TRegistroInscripcion.FormShow(Sender: TObject);
begin
     inherited;
     nPorcentaje.Valor := 0;
     cbPOrcentaje.Checked := TRUE;
     cbPorcentajeClick( NIL );
end;

procedure TRegistroInscripcion.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCajaAhorro do
     begin
          Datasource.Dataset := cdsInscripcion;    // Ya debe estar agregado el registro al llegar a la forma
          AplicarFiltro;
          with cdsInscripcion do
          begin
               Refrescar;
               Append;
               FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
               AH_SALDO_I.SetFocus;
          end;
     end;
end;

//Se puede ciclar el registro de inscripci�n dependiendo de la variable FCiclo.
procedure TRegistroInscripcion.EscribirCambios;
 var
    lContinua : Boolean;
begin
     lContinua := TRUE;
     with DataSource.DataSet do
     begin
          if StrVacio( CB_CODIGO.Llave ) then
          begin
               CB_CODIGO.SetFocus;
               ZetaDialogo.ZError(Caption, 'El c�digo del empleado no puede quedar vac�o',0);
               lContinua := FALSE;
          end
          else
          if dmCajaAhorro.cdsAhorros.Locate('CB_CODIGO',CB_CODIGO.Valor,[])then
          begin
               ZetaDialogo.ZInformation( Caption, Format( 'El empleado: %d ya est� inscrito',[ CB_CODIGO.Valor ] ),0 );
               CB_CODIGO.SetFocus;
               lContinua := FALSE;
          end
          else
          if cbPorcentaje.Checked then
          begin
               if ( State = dsBrowse ) then
                  Edit;
               FieldByName('AH_FORMULA').AsString := 'SALARIO*'  + StrToZero( nPorcentaje.Valor/100,3,2);
               nPorcentaje.Valor := 0;
          end;
     end;
     if lContinua then
     begin
          inherited;
          if FCiclo then
          begin
               with DataSource.DataSet do
               begin
                    if ( State = dsBrowse ) then
                    begin
                         AplicarFiltro;
                         Append;
                         CB_CODIGO.SetFocus;
                    end;
               end;
          end
          else
          begin
               Close;
          end;
     end;
end;

procedure TRegistroInscripcion.cbPorcentajeClick(Sender: TObject);
begin
     inherited;
     AH_FORMULA.Visible := NOT cbPOrcentaje.Checked;
     nPorcentaje.Visible := cbPOrcentaje.Checked;
     
     if cbPOrcentaje.Checked then
     begin
          lbFormula.Caption := 'Porcentaje:'
     end
     else
     begin
          lbFormula.Caption := 'F�rmula:';
     end;
end;

function TRegistroInscripcion.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroInscripcion.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TRegistroInscripcion.AplicarFiltro;
begin
     with dmCajaAhorro do
     begin
          LlenaFiltroEmpleados( cdsAhorros );
          CB_CODIGO.Filtro := FiltroEmpleados;
     end;
end;

end.
