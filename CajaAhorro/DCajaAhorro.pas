unit DCajaAhorro;

interface

uses
  SysUtils, Classes, DB, DbClient, Forms, Dialogs,
  {$ifndef VER130}Variants,{$endif}
{$ifdef DOS_CAPAS}
     DServerCajaAhorro,
 {$ifdef CAJAAHORRO}
     DServerNomina,
 {$endif}
{$else}
     CajaAhorro_TLB,
 {$ifdef CAJAAHORRO}
     Nomina_TLB,
 {$endif}
{$endif}
     ZetaClientDataSet,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaTipoEntidad,
     ZBaseThreads,
     ZetaServerDataSet;

type
  TdmCajaAhorro = class(TDataModule)
    cdsTAhorro: TZetaLookupDataSet;
    cdsTPresta: TZetaLookupDataSet;
    cdsCtasBancarias: TZetaLookupDataSet;
    cdsTiposDeposito: TZetaLookupDataSet;
    cdsRegPrestamos: TZetaClientDataSet;
    cdsRegCtasMovs: TZetaClientDataSet;
    cdsLiquidacionAhorro: TZetaClientDataSet;
    cdsCtasMovsLiquida: TZetaClientDataSet;
    cdsCtasMovs: TZetaClientDataSet;
    cdsInscripcion: TZetaClientDataSet;
    cdsHisAhorros: TZetaClientDataSet;
    cdsACarabo: TZetaClientDataSet;
    cdsHisPrestamos: TZetaClientDataSet;
    cdsPCarAbo: TZetaClientDataSet;
    cdsTotalesFondoCaja: TZetaClientDataSet;
    cdsPeriodo: TZetaLookupDataSet;
    cdsAhorros: TZetaClientDataSet;
    cdsTotalesCuentas: TZetaClientDataSet;
    cdsTInteres: TZetaLookupDataSet;
    cdsTotalHisAhorros: TZetaClientDataSet;
    cdsTotalHisPrestamos: TZetaClientDataSet;
    cdsRepartoIntereses: TZetaClientDataSet;
    cdsDataSet: TZetaClientDataSet;
    cdsVerifica: TZetaClientDataSet;
    cdsPeriodo2: TZetaLookupDataSet;
    cdsEmpleadosAval: TZetaLookupDataSet;
    cdsExcepMontos: TZetaClientDataSet;
    cdsPeriodoBusqueda: TZetaLookupDataSet;
    cdsPeriodoProcesos: TZetaLookupDataSet;
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsCatalogoAlAdquirirDatos(Sender: TObject);
    procedure cdsCatalogoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsCatalogosAlEnviarDatos(Sender: TObject);
    procedure cdsCatalogosAfterDelete(DataSet: TDataSet);
    procedure cdsCatalogosAlModificar(Sender: TObject);
    procedure cdsCatalogosBeforePost(DataSet: TDataSet);
    procedure cdsTAhorroBeforePost(DataSet: TDataSet);
    procedure cdsCtasBancariasAlCrearCampos(Sender: TObject);
    procedure cdsCtasBancariasNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsRegPrestamosNewRecord(DataSet: TDataSet);
    procedure cdsRegPrestamosBeforePost(DataSet: TDataSet);
    procedure cdsRegPrestamosAlCrearCampos(Sender: TObject);
    procedure PR_MontosChange(Sender: TField);
    procedure PR_MESESChange(Sender: TField);
    procedure PR_INTERESChange(Sender: TField);
    procedure PR_PagosChange(Sender: TField);
    procedure cdsRegPrestamosAlEnviarDatos(Sender: TObject);
    procedure cdsRegCtasMovsBeforePost(DataSet: TDataSet);
    procedure cdsLiquidacionAhorroAlCrearCampos(Sender: TObject);
    procedure cdsCtasMovsLiquidaBeforePost(DataSet: TDataSet);
    procedure cdsCtasMovsLiquidaAlCrearCampos(Sender: TObject);
    procedure cdsCtasMovsAlAdquirirDatos(Sender: TObject);
    procedure cdsCtasMovsAlEnviarDatos(Sender: TObject);
    procedure cdsCtasMovsNewRecord(DataSet: TDataSet);
    procedure cdsCtasMovsBeforePost(DataSet: TDataSet);
    procedure cdsInscripcionNewRecord(DataSet: TDataSet);
    procedure cdsInscripcionBeforePost(DataSet: TDataSet);
    procedure cdsHisAhorrosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisAhorrosAlCrearCampos(Sender: TObject);
    procedure cdsHisAhorrosAlAgregar(Sender: TObject);
    procedure cdsHisAhorrosAlModificar(Sender: TObject);
    procedure cdsHisAhorrosBeforePost(DataSet: TDataSet);
    procedure cdsHisAhorrosAfterCancel(DataSet: TDataSet);
    procedure cdsACaraboAfterDelete(DataSet: TDataSet);
    procedure cdsACaraboAlCrearCampos(Sender: TObject);
    procedure cdsACaraboBeforePost(DataSet: TDataSet);
    procedure cdsACaraboNewRecord(DataSet: TDataSet);
    procedure cdsHisAhorrosAlEnviarDatos(Sender: TObject);
    procedure cdsInscripcionAlEnviarDatos(Sender: TObject);
    procedure cdsHisPrestamosAlCrearCampos(Sender: TObject);
    procedure cdsHisPrestamosAlAgregar(Sender: TObject);
    procedure cdsHisPrestamosAlModificar(Sender: TObject);
    procedure cdsPCarAboAfterDelete(DataSet: TDataSet);
    procedure cdsPCarAboNewRecord(DataSet: TDataSet);
    procedure cdsPCarAboAlAdquirirDatos(Sender: TObject);
    procedure cdsHisPrestamosAlEnviarDatos(Sender: TObject);
    procedure cdsHisPrestamosBeforePost(DataSet: TDataSet);
    procedure cdsTotalesFondoCajaAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsPeriodoLookupDescription(Sender: TZetaLookupDataSet;
      var sDescription: String);
    procedure cdsPeriodoLookupKey(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter, sKey: String;
      var sDescription: String);
    procedure cdsPeriodoLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsTotalesFondoCajaAlCrearCampos(Sender: TObject);
    procedure cdsCtasMovsAlCrearCampos(Sender: TObject);
    procedure cdsCtasMovsAfterOpen(DataSet: TDataSet);
    procedure cdsCtasMovsAlModificar(Sender: TObject);
    procedure cdsCtasMovsAlBorrar(Sender: TObject);
    procedure cdsHisAhorrosAfterDelete(DataSet: TDataSet);
    procedure cdsRegPrestamosReconcileError(DataSet: TCustomClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    procedure cdsTAhorroAlBorrar(Sender: TObject);
    procedure cdsTiposDepositoBeforePost(DataSet: TDataSet);
    procedure cdsHisPrestamosAfterDelete(DataSet: TDataSet);
    procedure cdsTiposDepositoBeforeDelete(DataSet: TDataSet);
    procedure cdsTiposDepositoNewRecord(DataSet: TDataSet);
    procedure cdsCtasBancariasBeforePost(DataSet: TDataSet);
    procedure cdsTotalesCuentasAlAgregar(Sender: TObject);
    procedure cdsTotalesCuentasAlModificar(Sender: TObject);
    procedure cdsTotalesCuentasAlBorrar(Sender: TObject);
    procedure cdsTotalesCuentasAlCrearCampos(Sender: TObject);
    procedure cdsCtasBancariasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTAhorroGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTPrestaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTiposDepositoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsHisPrestamosReconcileError(DataSet: TCustomClientDataSet;
      E: EReconcileError; UpdateKind: TUpdateKind;
      var Action: TReconcileAction);
    procedure cdsTAhorroNewRecord(DataSet: TDataSet);
    procedure cdsHisPrestamosCalcFields(DataSet: TDataSet);

    procedure cdsTotalHisAhorrosAlAdquirirDatos(Sender: TObject);
    procedure cdsTotalHisAhorrosAlCrearCampos(Sender: TObject);
    procedure cdsTotalHisPrestamosAfterDelete(DataSet: TDataSet);
    procedure cdsTotalHisPrestamosAlAgregar(Sender: TObject);
    procedure cdsTotalHisPrestamosAlCrearCampos(Sender: TObject);
    procedure cdsTotalHisPrestamosAlEnviarDatos(Sender: TObject);
    procedure cdsTotalHisPrestamosAlModificar(Sender: TObject);
    procedure cdsTotalHisPrestamosBeforePost(DataSet: TDataSet);
    procedure cdsRepartoInteresesAlCrearCampos(Sender: TObject);
    procedure cdsDataSetAlCrearCampos(Sender: TObject);
    procedure cdsPeriodo2AlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoBusquedaAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodo2LookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
    procedure cdsEmpleadosAvalAlAdquirirDatos(Sender: TObject);
    procedure cdsExcepMontosAfterEdit(DataSet: TDataSet);
    procedure cdsExcepMontosAlAdquirirDatos(Sender: TObject);
    procedure cdsExcepMontosAlBorrar(Sender: TObject);
    procedure cdsExcepMontosAlCrearCampos(Sender: TObject);
    procedure cdsExcepMontosAlEnviarDatos(Sender: TObject);
    procedure cdsExcepMontosNewRecord(DataSet: TDataSet);
    procedure cdsExcepMontosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsPeriodoProcesosAlAdquirirDatos(Sender: TObject);
    procedure cdsPeriodoProcesosLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
  private
    { Private declarations }
    FListaProcesos: TProcessList;
    FRegImprimirCheque: Boolean;
    FRegimprimirChequeLiq: boolean;
    FSaldoIni: Double;
    FFiltroEmpleados:string;
    FValidarReglas:Boolean;
    FForzarGuardarPrestamos:Boolean;
    FMensajePrestamo :WideString;
    FPeriodoEmpleado: integer;
    FTipoNominaEmpleado: integer;
    FYearEmpleado: integer;
    FTotalLiquidacion: currency;
    FFiltroCatalogo: string;
    FTipoAhorroLiquida: string;
{$ifdef DOS_CAPAS}
    function GetServerCajaAhorro: TdmServerCajaAhorro;
    function GetServerNomina: TdmServerNomina;
{$else}
    FServidor: IdmServerCajaAhorroDisp;
    FServidorNomina: IdmServerNominaDisp;
    function GetServerCajaAhorro: IdmServerCajaAhorroDisp;
    function GetServerNomina: IdmServerNominaDisp;
{$endif}
    function GetCantidadXMes: integer;
    procedure CM_DISPONIBLEChange(Sender: TField);
    procedure CM_FECHAOnChange(Sender: TField);
    procedure OnChangeAbono(Sender: TField);
    procedure OnChangeCargo(Sender: TField);


    procedure GetSaldos(DataSet: TDataSet; const sCargo, sAbono: string; var dCargo, dAbono: Real);
    //procedure RecalculaMontosAhorro(Sender: TField);
    procedure ShowEdicion(const iTabla: Integer);
    procedure InitDatasetsRegPrestamos;
    procedure ImprimirCheque( const sCtaBancaria: String; const iNumCheque: Integer; const lLiquida: boolean = False );
    procedure ValidaCargoAbono(Sender: TField; const sCampo: string);
    procedure CalculaEstadoCuenta;
    function GetLista(ZetaDataset: TZetaClientDataset): OLEVariant;
    function CancelaTodosVerifica(const Lista: OleVariant;Parametros: TZetaParams): boolean;
    function CancelaTodosNuevo(Parametros: TZetaParams): boolean;
    procedure GrabaExcepcionesMonto(Dataset: TZetaClientDataset);
    procedure GrabaExcepcionesMontoParams( Dataset: TZetaClientDataset; oParams: TZetaParams );
    procedure SetStatusPrestamo( DataSet: TDataSet );
  protected
    { Protected declarations }
  public
    { Public declarations }
    function SiguienteCheque( const sCuentaBancaria: String ): Integer;
    function GetTotalesRetenciones(oParams: TZetaParams): OleVariant;
    function GrabaRetenciones(oParams: TZetaParams): Boolean;
    function ValidarCheque( const sCuentaBancaria: String; iCheque: integer ): boolean;
    function GeTotalesRepartoIntereses(oParams: TZetaParams): OleVariant;
    function GrabarIntereses(Parametros: TZetaParams): Boolean;
    procedure GetRepartoIntereses(Parametros: TZetaParams);
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    procedure RegistrarPrestamos;
    procedure LiquidarAhorro;
	{$ifdef B&D}
    procedure RegistrarLiquidacion( iTipo: integer = 0 );
	{$else}
    //procedure RegistrarLiquidacion;
	{$endif}
    procedure RecalculaMontosAhorro;
    procedure RecalculaMontosAhorroLiquidacion;
    procedure RegistrarDepositoRetiro( const lAgregarNUevo: Boolean = TRUE );
    procedure RegistrarInscripcion( const lCiclo: Boolean = TRUE );
    procedure GetCuentasBancarias(oParams: TZetaParams);
    procedure ConectarPeriodo(const iYear: integer; const eTipo: eTipoPeriodo);
    procedure ConectarPeriodo2(const iYear: integer; const eTipo: eTipoPeriodo);
    procedure ConectarPeriodoBusqueda(const iYear: integer; const eTipo: eTipoPeriodo);
    procedure ConectarPeriodoProcesos(const iYear: integer; const eTipo: eTipoPeriodo);
    procedure BorrarIncripcion;
    {$ifdef DOS_CAPAS}
    property ServerCajaAhorro: TdmServerCajaAhorro read GetServerCajaAhorro;
    property ServerNomina: TdmServerNomina read GetServerNomina;
    {$else}
    property ServerCajaAhorro: IdmServerCajaAhorroDisp read GetServerCajaAhorro;
    property ServerNomina: IdmServerNominaDisp read GetServerNomina;
    {$endif}
    property RegImprimirCheque: Boolean read FRegImprimirCheque write FRegImprimirCheque;
    property RegImprimirChequeLiq: Boolean read FRegImprimirChequeLiq write FRegImprimirChequeLiq;
    property SaldoInicial: Double read FSaldoIni;
    property FiltroEmpleados:String read FFiltroEmpleados write FFiltroEmpleados;
    property FiltroCatalogo: String read FFiltroCatalogo write FFiltroCatalogo;
    property PeriodoEmpleado: integer read FPeriodoEmpleado write FPeriodoEmpleado;
    property TipoNominaEmpleado: integer read FTipoNominaEmpleado write FTipoNominaEmpleado;
    property YearEmpleado: integer read FYearEmpleado write FYearEmpleado;
    property TotalLiquidacion: currency read FTotalLiquidacion write FTotalLiquidacion;
    property TipoAhorroLiquida: string read FTipoAhorroLiquida write FTipoAhorroLiquida;
    procedure LlenaFiltroEmpleados(DataSet: TDataSet);
    procedure RefrescarLiquidacion(const iEmpleado: Integer);
    procedure GetAhorrosPorTipo(const sTipoAhorro:string);
    procedure ActualizarCodigoCuentaBancaria;
    function EsPrestamoFonacot( const sTipo: String ): Boolean;
    function ExistenPrestamosActivos( TipoPrestamo : integer ) : Boolean;
    function DiasLiquidacionPrestamo( TipoPrestamo : integer ) : integer;
    function GetTipoInteres( TipoPrestamo : integer ) : string;
    procedure CancelaGetLista(Parametros: TZetaParams);
    function CancelaTodos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
  end;

const
     K_TAHORRO = 0;
     K_TPRESTA = 1;
     K_CTABANCO = 2;
     K_T_DEPOSITO = 3;
     K_INSCRIPCION = 4;

var
  dmCajaAhorro: TdmCajaAhorro;

implementation

{$R *.dfm}

uses dcliente,
     dConsultas,
     dReportes,
     DSistema,
     DGlobal,
     ZetaCommonTools,
     ZcxWizardBasico,
     ZetaWizardFeedBack_DevEx,
     ZReconcile,
     ZetaMsgDlg,
     ZetaDialogo,
     ZBaseEdicion_DevEx,
     ZetaBuscaPeriodo_DevEx,
     FTressShell,
     FEditTablas_DevEx,
     FEditTablasCajaAhorro,
     FEditTAhorro_DevEx,FEditTAhorroPE_DevEx,
     FEditTPrestamo_DevEx,FEditTPrestamoPE_DevEx,                     //PE DevEx (by DACP)
     FEditCtasBancarias_DevEx,
     FEditHisAhorros_DevEx,
     FRegistroPrestamo_DevEx,FRegistroPrestamoPE_DevEx,           //PE DevEx (by DACP)
     FRegistroLiquidacion_DevEx,FRegistroLiquidacionPE_DevEx,   //PE DevEx (by DACP)
     FRegistroDepositoRetiro_DevEx,
     FRegistroInscripcion_DevEx,FRegistroInscripcionPE_DevEx,  //PE DevEx (by DACP)
     FEditHisPrestamos_DevEx,
     FEditTipoDepositoRetiro_DevEx,
     FTipoPrestamoAhorro,
     ZetaClientTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress;


procedure ShowWizard( const eProceso: Procesos );
var
   WizardClass: TcxWizardBasicoClass;
begin
     WizardClass := nil;
{
     case eProceso of
          prLabCalcularTiempos:     WizardClass := TWizLaborReCalcularTiempos;
     end;
}
     if ( WizardClass = nil ) then { Este IF debe ser Temporal }
        ZetaDialogo.zInformation( 'Estamos trabajando...', 'esta opci�n no ha sido implementada', 0 )
     else
         ZcxWizardBasico.ShowWizard( WizardClass );
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure SetDataChange( const Proceso: Procesos );
begin
{
     case Proceso of
          prLabCalcularTiempos: TressShell.SetDataChange( [ enWorks, enLecturas ] );
     end;
}
end;

{ ************** TdmCajaAhorro **************** }


procedure TdmCajaAhorro.DataModuleCreate(Sender: TObject);
begin
     dmReportes.cdsLookupReportes.OnLookupSearch := nil;
     FValidarReglas := False;
     FFiltroCatalogo := VACIO;
end;

{$ifdef DOS_CAPAS}
function TdmCajaAhorro.GetServerCajaAhorro: TdmServerCajaAhorro;
begin
     Result := DCliente.dmCliente.ServerCajaAhorro;
end;

function TdmCajaAhorro.GetServerNomina: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;
{$else}
function TdmCajaAhorro.GetServerCajaAhorro: IdmServerCajaAhorroDisp;
begin
     Result := IdmServerCajaAhorroDisp( dmCliente.CreaServidor( CLASS_dmServerCajaAhorro, FServidor ) );
end;

function TdmCajaAhorro.GetServerNomina: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( dmCliente.CreaServidor( CLASS_dmServerNomina, FServidorNomina ) );
end;
{$endif}

procedure TdmCajaAhorro.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

procedure TdmCajaAhorro.HandleProcessEnd( const iIndice, iValor: Integer );
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( '' );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure TdmCajaAhorro.ShowEdicion(const iTabla: Integer);
begin
     case iTabla of
         {$ifdef B&D}
          K_TAHORRO    : ZBaseEdicion_DevEx.ShowFormaEdicion( EditTAhorroPE_DevEx, TEditTAhorroPE_DevEx );
          K_TPRESTA    : ZBaseEdicion_DevEx.ShowFormaEdicion( EditTPrestamoPE_DevEx, TEditTPrestamoPE_DevEx );
          {$else}
            K_TAHORRO    : ZBaseEdicion_DevEx.ShowFormaEdicion( EditTAhorro_DevEx, TEditTAhorro_DevEx );
            K_TPRESTA    : ZBaseEdicion_DevEx.ShowFormaEdicion( EditTPrestamo_DevEx, TEditTPrestamo_DevEx );
          {$endif}
          K_CTABANCO   : ZBaseEdicion_DevEx.ShowFormaEdicion( EditCtasBancarias_DevEx, TEditCtasBancarias_DevEx );
          K_T_DEPOSITO : ZBaseEdicion_DevEx.ShowFormaEdicion( EditTipoDepositoRetiro_DevEx, TEditTipoDepositoRetiro_DevEx );
     end;
end;

procedure Error( const sError, sFieldName: string; DataSet : TDataSet );
begin
    ZetaDialogo.ZError( 'Mensaje', sError , 0 );
    Dataset.FieldByName(sFieldName).FocusControl;
end;

procedure TdmCajaAhorro.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
begin
     if ( Estado = stTipoAhorro ) then
     begin
          cdsHisAhorros.SetDataChange;
          cdsTotalesFondoCaja.SetDataChange;
          cdsCtasMovs.SetDataChange;
     end;

     if ( Estado = stEmpleado ) then
     begin
          cdsHisAhorros.SetDataChange;
     end;

     if Dentro( enAhorro, Entidades ) then
        cdsHisAhorros.SetDataChange;


end;

{ Catalogos }

procedure TdmCajaAhorro.cdsCatalogoAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerCajaAhorro.GetCatalogo( dmCliente.Empresa, Tag, FFiltroCatalogo );
          FFiltroCatalogo := VACIO;
     end;
end;

procedure TdmCajaAhorro.cdsCatalogoReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind;
          var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

procedure TdmCajaAhorro.cdsCatalogosAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmCajaAhorro.cdsCatalogosBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if StrVacio( FieldByName( 'TB_CODIGO' ).AsString ) then
             DB.DatabaseError( 'C�digo no puede quedar vac�o' );
     end;
end;

procedure TdmCajaAhorro.cdsCatalogosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          PostData;
          if ( Changecount > 0 ) then
             Reconcile( ServerCajaAhorro.GrabaCatalogo( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmCajaAhorro.cdsCatalogosAfterDelete(DataSet: TDataSet);
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          Enviar;
     end;
end;

{cdsTPresta}

procedure TdmCajaAhorro.cdsTPrestaGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_TAB_NOM_TIPO_PRESTA, iRight );
end;

{ cdsTAhorro }

procedure TdmCajaAhorro.cdsTAhorroBeforePost(DataSet: TDataSet);
begin
     cdsCatalogosBeforePost( DataSet );
     with DataSet do
     begin
          if strLleno( FieldByName( 'TB_CONCEPT' ).AsString ) and strLleno( FieldByName( 'TB_RELATIV' ).AsString ) then
          begin
               if FieldByName( 'TB_CONCEPT' ).AsInteger = FieldByName( 'TB_RELATIV' ).AsInteger then
                  DB.DatabaseError( 'Los Conceptos deducci�n y relativo no pueden ser los mismos' );
          end;
          if ( FieldByName( 'TB_TASA1' ).AsFloat < 0 ) or ( FieldByName( 'TB_TASA2' ).AsFloat < 0 ) or
             ( FieldByName( 'TB_TASA3' ).AsFloat < 0 ) then
             DB.DatabaseError( 'Tasas de int�res deben ser mayor o igual a cero' );
     end;
end;

procedure TdmCajaAhorro.cdsTAhorroAlBorrar(Sender: TObject);
begin
     with cdsTAhorro do
     begin
          GetAhorrosPorTipo( FieldByName('TB_CODIGO').AsString );
          if cdsAhorros.Locate( 'AH_TIPO',FieldByName('TB_CODIGO').AsString ,[] ) then
             DB.DatabaseError( Format (' No se puede borrar tipo de ahorro: %s ; tiene empleados inscritos en este ahorro',[FieldByName('TB_ELEMENT').AsString]) )
          else
          if ZetaMsgDlg.ConfirmaCambio( '� Desea borrar este registro ?' ) then
          begin
               Delete;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsTAhorroGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_TAB_NOM_TIPO_AHORRO, iRight );
end;


{ cdsCtasBancarias }

procedure TdmCajaAhorro.cdsCtasBancariasAlCrearCampos(Sender: TObject);
begin
     cdsTAhorro.Conectar;
     with cdsCtasBancarias do
     begin
          CreateSimpleLookUp( cdsTAhorro, 'TB_ELEMENT', 'AH_TIPO' );
          ListaFija( 'CT_STATUS', lfStatusAhorro );
          if ( FindField('SALDO_HOY') <> NIL ) then
             MaskPesos('SALDO_HOY');
     end;
end;

procedure TdmCajaAhorro.cdsCtasBancariasNewRecord(DataSet: TDataSet);
begin
     with cdsCtasBancarias do
     begin
          FieldByName( 'AH_TIPO' ).AsString := dmCliente.TipoAhorro;
          FieldByName( 'CT_STATUS' ).AsInteger := Ord( saActivo );
     end;
end;

procedure TdmCajaAhorro.cdsCtasBancariasBeforePost(DataSet: TDataSet);
begin
     with cdsCtasBancarias do
     begin
          if StrVacio( FieldByName( 'AH_TIPO' ).AsString )then
             DataBaseError('El tipo de ahorro no puede quedar vac�o');
          if StrVacio( FieldByName( 'CT_CODIGO' ).AsString )then
             DataBaseError('El c�digo de cuenta no puede quedar vac�o');
     end;
end;

procedure TdmCajaAhorro.cdsCtasBancariasGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_AHORRO_CAT_CUENTAS_BANC, iRight );
end;

{ cdsRegPrestamos }

procedure TdmCajaAhorro.InitDatasetsRegPrestamos;
var
   oCtasMovimientos: OleVariant;
begin
     if cdsRegPrestamos.Active and cdsRegCtasMovs.Active then
     begin
          cdsRegPrestamos.EmptyDataset;
          cdsRegCtasMovs.EmptyDataset;
     end
     else
     begin
          cdsRegPrestamos.Data := ServerCajaAhorro.InitRegPrestamos( dmCliente.Empresa, oCtasMovimientos );
          cdsRegCtasMovs.Data := oCtasMovimientos;
     end;
end;

procedure TdmCajaAhorro.ImprimirCheque( const sCtaBancaria: String; const iNumCheque: Integer; const lLiquida: boolean = False );
const
     K_FILTRO = '( CT_CODIGO = %s ) and ( CM_CHEQUE = %d )';
var
   iReporte: integer;
begin
     dmSistema.cdsusuarios.Conectar;
     with cdsCtasBancarias do
     begin
          if Locate( 'CT_CODIGO', sCtaBancaria, [] ) then
          begin
               if lLiquida then
                  iReporte := FieldByName( 'CT_REP_LIQ' ).AsInteger
               else
                   iReporte := FieldByName( 'CT_REP_CHK' ).AsInteger;
               if iReporte > 0 then
                  dmReportes.ImprimeUnaForma( Format( K_FILTRO, [ EntreComillas( sCtaBancaria ), iNumCheque ] ), iReporte );
          end;
     end;
end;

function TdmCajaAhorro.SiguienteCheque( const sCuentaBancaria: String ): Integer;
begin
     Result:= ServerCajaAhorro.GetSiguienteCheque( dmCliente.Empresa, sCuentaBancaria );
end;



procedure TdmCajaAhorro.RegistrarPrestamos;
var
   strTipoPrestamo : string;
begin
   cdsTAhorro.Conectar;
   cdsTotalHisAhorros.Conectar;
     with cdsTPresta do
     begin
          Conectar;
          if not Locate( 'TB_CODIGO', dmCliente.GetDatosAhorroActivo.TipoPrestamo, [] )then
             zInformation('Registrar Pr�stamo','El ahorro activo no tiene relacionado un tipo de pr�stamo',0)
          else
          begin
               InitDatasetsRegPrestamos;
               cdsRegPrestamos.Append;
               FRegImprimirCheque := FALSE;    // Dentro de la forma se pondr�a en TRUE y se evaluar� en evento AlEnviar()
               //Fijar el Tipo de Prestamo
               //pr_tipo = (select tb_presta from tahorro where tb_codigo = '1')
               strTipoPrestamo := ServerCajaAhorro.GetTipoPrestamo( dmCliente.Empresa, cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger );
               //strTipoPrestamo := ServerCajaAhorro.GetTipoPrestamo( dmCliente.Empresa, TipoPrestamoAhorro.lkTipoPrestamo.Valor );
               cdsRegPrestamos.FieldByName( 'PR_TIPO' ).AsString := strTipoPrestamo;
               cdsTPresta.Filter := Format ( 'TB_CODIGO = %s', [ EntreComillas( strTipoPrestamo ) ] );
               cdsTPresta.Filtered := True;
               try
               {$ifdef B&D}
                   ZBaseEdicion_DevEx.ShowFormaEdicion( RegistroPrestamoPE_DevEx,  TRegistroPrestamoPE_DevEx );
               {$else}
	               ZBaseEdicion_DevEx.ShowFormaEdicion( RegistroPrestamo_DevEx,  TRegistroPrestamo_DevEx );
               {$endif}
               finally
                      cdsTPresta.Filter := VACIO;
                      cdsTPresta.Filtered := False;
               end;
          end;
     end;

end;

procedure TdmCajaAhorro.cdsRegPrestamosAlCrearCampos(Sender: TObject);
begin
     with cdsRegPrestamos do
     begin
          FieldByName( 'PR_MONTO_S' ).OnChange := PR_MontosChange;
          FieldByName( 'PR_TASA' ).OnChange := PR_MontosChange;
          //FieldByName( 'PR_MESES' ).OnChange := PR_MESESChange; @DACP VERIFICAR
          //FieldByName( 'PR_INTERES' ).OnChange := PR_INTERESChange;
          //FieldByName( 'PR_MONTO' ).OnChange := PR_PagosChange;
          //FieldByName( 'PR_PAGOS' ).OnChange := PR_PagosChange;
     end;
end;

procedure TdmCajaAhorro.PR_MontosChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          FieldByName( 'PR_INTERES' ).AsFloat := ( FieldByName( 'PR_MONTO_S' ).AsFloat *
                                                   ( FieldByName( 'PR_TASA' ).AsFloat / 100 ) *
                                                   FieldByName( 'PR_MESES' ).AsFloat );
     end;
end;

function TdmCajaAhorro.GetCantidadXMes: integer;
const
     K_TREINTA = 30;
     K_CUATRO  = 4;
     K_DOS     = 2;
     K_UNO     = 1;
     K_TRES    = 3;
begin
     with dmCliente do
     begin
         case GetClasificacionPeriodo(FListaTiposPeriodoConfidencialidad ,cdsEmpleado.FieldByName('CB_NOMINA').AsInteger) of
              tpDiario:                                  Result := K_TREINTA;
              tpSemanal:                                 Result := K_CUATRO;
              tpCatorcenal,tpQuincenal:                  Result := K_DOS;
              tpMensual:                                 Result := K_UNO;
              tpDecenal:                                 Result := K_TRES;
         else
              Result := K_CUATRO;
         end;
     end;
end;

procedure TdmCajaAhorro.PR_MESESChange(Sender: TField);
const
     K_PAGOS_X_MES = 4;
begin
     self.PR_MontosChange( Sender );
     with Sender.DataSet do
     begin
          FieldByName( 'PR_PAGOS' ).AsInteger := Trunc( FieldByName( 'PR_MESES' ).AsFloat * GetCantidadXMes );
     end;
end;

procedure TdmCajaAhorro.PR_INTERESChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          FieldByName( 'PR_MONTO' ).AsFloat := ( FieldByName( 'PR_MONTO_S' ).AsFloat + FieldByName( 'PR_INTERES' ).AsFloat );
     end;
end;

procedure TdmCajaAhorro.PR_PagosChange(Sender: TField);
begin
     with Sender.DataSet do
     begin
          if ( FieldByName( 'PR_PAGOS' ).AsInteger > 0 ) then
          begin
               FieldByName( 'PR_PAG_PER' ).AsFloat := Redondea(( FieldByName( 'PR_MONTO' ).AsFloat / FieldByName( 'PR_PAGOS' ).AsInteger ) );
          end
          else
          begin
               FieldByName( 'PR_PAG_PER' ).AsFloat := 0;
          end;
          FieldByName( 'PR_FORMULA' ).AsString := FloatToStr( FieldByName( 'PR_PAG_PER' ).AsFloat );
     end;
end;

procedure TdmCajaAhorro.cdsRegPrestamosNewRecord(DataSet: TDataSet);
begin
     with cdsRegPrestamos do
     begin
          with dmCliente do
          begin
               FieldByName( 'PR_TIPO' ).AsString := GetDatosAhorroActivo.TipoPrestamo;
               FieldByName( 'CB_CODIGO' ).AsInteger := Empleado;
               FieldByName( 'PR_FECHA' ).AsDateTime := FechaDefault;
          end;
          FieldByName( 'PR_TASA' ).AsFloat := cdsTPresta.FieldByName( 'TB_TASA1' ).AsFloat;
          FieldByName( 'PR_REFEREN' ).AsString := '1';

          FieldByName('PR_CARGOS').AsFloat := 0;
          FieldByName('PR_ABONOS').AsFloat := 0;
          FieldByName('PR_NUMERO').AsInteger := 0;
          FieldByName('PR_SALDO_I').AsFloat := 0;
          FieldByName('PR_STATUS').AsInteger := Ord( spActivo );
          FieldByName('PR_TOTAL').AsFloat  := 0;
          FieldByName('PR_SALDO').AsFloat  := 0;
     end;
end;

procedure TdmCajaAhorro.cdsRegPrestamosBeforePost(DataSet: TDataSet);
begin
     with cdsRegPrestamos do
     begin
          { @DACP VERIFICAR
          if ( FieldByName( 'PR_MONTO_S' ).AsFloat <= 0 ) then
             DB.DataBaseError( 'Monto solicitado debe ser mayor a cero' );
          }
          if ( FieldByName( 'PR_MONTO' ).AsFloat <= 0 ) then
             DB.DataBaseError( 'Total a pagar debe ser mayor a cero' );
          {OP: 28/05/08}
          if ZetaCommonTools.StrVacio( FieldByName( 'PR_REFEREN' ).AsString ) then
             DB.DataBaseError( 'La Referencia no puede quedar vac�a' );
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmCajaAhorro.cdsRegPrestamosAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorCountMovs: Integer;
   oPrestamoResults, oCtaMovsResult: OleVariant;
   oParametros: TZetaParams;
begin
     ErrorCount := 0;
     ErrorCountMovs := 0;
     cdsRegCtasMovs.PostData;
     with cdsRegPrestamos do
     begin
          PostData;
          if ( ChangeCount > 0 ) then
          begin
               oParametros:= TZetaParams.Create;
               try
                  dmCliente.CargaActivosTodos( oParametros );
                  oPrestamoResults := ServerCajaAhorro.GrabaRegPrestamo( dmCliente.Empresa, Delta, cdsRegCtasMovs.DeltaNull, oParametros.VarValues, ErrorCount, ErrorCountMovs, oCtaMovsResult,FForzarGuardarPrestamos,FMensajePrestamo );
                  if Reconcile( oPrestamoResults ) and cdsRegCtasMovs.Reconcile( oCtaMovsResult ) then
                  begin
                       TressShell.SetDataChange( [ enPrestamo, enCtaMovs, enAhorro ] );
                       cdsHisAhorros.SetDataChange;

                       with cdsRegCtasMovs do
                       begin
                            if ( not IsEmpty ) and FRegImprimirCheque then
                               ImprimirCheque( FieldByName( 'CT_CODIGO' ).AsString, FieldByName( 'CM_CHEQUE' ).AsInteger );
                       end;
                  end
                  else
                  begin
                       if FValidarReglas then
                       begin
                            if CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_SIST_KARDEX )then
                            begin
                                 FForzarGuardarPrestamos := True;
                                 oPrestamoResults := ServerCajaAhorro.GrabaRegPrestamo( dmCliente.Empresa, Delta, cdsRegCtasMovs.DeltaNull, oParametros.VarValues, ErrorCount, ErrorCountMovs, oCtaMovsResult,FForzarGuardarPrestamos,FMensajePrestamo );
                                 if Reconcile( oPrestamoResults ) and cdsRegCtasMovs.Reconcile( oCtaMovsResult ) then
                                 begin
                                      TressShell.SetDataChange( [ enPrestamo, enCtaMovs, enAhorro ] );
                                      cdsHisAhorros.SetDataChange;
                                 end;
                            end;
                       end;
                  end;
               finally
                      FreeAndNil( oParametros );
               end;
          end;
          FValidarReglas := False;
          FForzarGuardarPrestamos := False;
     end;
end;

{ cdsRegCtasMovs }
procedure TdmCajaAhorro.cdsRegCtasMovsBeforePost(DataSet: TDataSet);
begin
     with cdsRegCtasMovs do
     begin
          if ( ServerCajaAhorro.ValidaCheque( dmCliente.Empresa, FieldByName( 'CT_CODIGO' ).AsString, FieldByName( 'CM_CHEQUE' ).AsInteger ) >= 1 ) then
             DB.DataBaseError( 'N�mero de cheque repetido. Es necesario cambiarlo.' );
     end;
     with cdsRegPrestamos do
     begin
          PostData;
          DataSet.FieldByName( 'CM_MONTO' ).AsFloat := FieldByName( 'PR_MONTO_S' ).AsFloat;
          DataSet.FieldByName( 'CM_FECHA' ).AsDateTime := FieldByName( 'PR_FECHA' ).AsDateTime;
          DataSet.FieldByName( 'PR_TIPO' ).AsString := FieldByName( 'PR_TIPO' ).AsString;
          DataSet.FieldByName( 'PR_REFEREN' ).AsString := FieldByName( 'PR_REFEREN' ).AsString;
     end;
     with dmCliente.cdsEmpleadoLookup do
     begin
          DataSet.FieldByName( 'CB_CODIGO' ).AsInteger := FieldByName( 'CB_CODIGO' ).AsInteger;
          DataSet.FieldByName( 'CM_BENEFI' ).AsString  := FieldByName( 'CB_NOMBRES' ).AsString + ' ' +
                                                          FieldByName( 'CB_APE_PAT' ).AsString + ' ' +
                                                          FieldByName( 'CB_APE_MAT' ).AsString;
     end;
end;


procedure TdmCajaAhorro.LiquidarAhorro;
begin
     with cdsTPresta do
     begin
          Conectar;
          Locate( 'TB_CODIGO', dmCliente.GetDatosAhorroActivo.TipoPrestamo, [] );
     end;
     cdsLiquidacionAhorro.Refrescar;
    // cdsLiquidacionAhorro.Edit;
     FRegImprimirCheque := FALSE;    // Dentro de la forma se pondr�a en TRUE y se evaluar� en evento AlEnviar()
     FRegimprimirChequeLiq := False;
     {$ifdef B&D}
           ZBaseEdicion_DevEx.ShowFormaEdicion( RegistroLiquidacionPE_DevEx,  TRegistroLiquidacionPE_DevEx );
     {$else}
           ZBaseEdicion_DevEx.ShowFormaEdicion( RegistroLiquidacion_DevEx,  TRegistroLiquidacion_DevEx );
     {$endif}
end;

procedure TdmCajaAhorro.cdsLiquidacionAhorroAlCrearCampos(Sender: TObject);
begin
     cdsTAhorro.Conectar;
     cdsCtasBancarias.Conectar;
     with cdsLiquidacionAhorro do
     begin
          //FieldByName('AH_FECHA').Onchange := CM_FECHAOnChange; //@DACP VERIFICAR
          //FieldByName('TASA').OnChange := RecalculaMontosAhorro;
          //FieldByName('DURACION').OnChange := RecalculaMontosAhorro;
     end;
end;

{$ifdef CAJAAHORRO}
procedure TdmCajaAhorro.RecalculaMontosAhorro;
{$else}
procedure TdmCajaAhorro.RecalculaMontosAhorro(Sender: TField);
{$endif}
begin
{ CM_TOT_AHO: TZetaNumero;
    CM_AFAVOR: TZetaNumero;
    CM_DISPONIBLE: TZetaNumero;
    CM_SAL_PRE: TZetaNumero;
    CM_INTERES: TZetaNumero;}
     with cdsCtasMovsLiquida do
     begin
          if dmCliente.GetDatosAhorroActivo.Relativo <> 0 then
          begin
               //Montos del Empleado
               FieldByName( 'CM_TOT_AHO' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'TotalFondoAhorro' ).AsFloat;
               FieldByName( 'CM_INTERES' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'TotalInteresesFA' ).AsFloat;
               FieldByName( 'CM_AFAVOR' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat;
               FieldByName( 'CM_SAL_PRE' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosFondoCapital' ).AsFloat +
                                                      cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosFondoInteres' ).AsFloat;
               FieldByName( 'CM_DISPONIBLE' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat +
                                                       FieldByName( 'CM_INTERES' ).AsFloat -
                                                       FieldByName( 'CM_SAL_PRE' ).AsFloat -
                                                       cdsLiquidacionAhorro.FieldByName( 'ComisionesAcumFA' ).AsFloat;
               FieldByName( 'CM_TOT_AHO_EMP' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat;
               FieldByName( 'CM_INTERES_EMP' ).AsFloat := FieldByName( 'CM_INTERES' ).AsFloat;
               FieldByName( 'CM_AFAVOR_EMP' ).AsFloat :=  FieldByName( 'CM_AFAVOR' ).AsFloat;
               FieldByName( 'CM_DISPONIBLE_EMP' ).AsFloat := FieldByName( 'CM_AFAVOR' ).AsFloat;
               FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat := FieldByName( 'CM_DISPONIBLE' ).AsFloat +
                                                               FieldByName( 'CM_DISPONIBLE_EMP' ).AsFloat;
               FieldByName( 'CM_MONTO' ).AsFloat := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               FTotalLiquidacion := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               if ( cdsLiquidacionAhorro.State = dsBrowse ) then
                  cdsLiquidacionAhorro.Edit;
               cdsLiquidacionAhorro.FieldByName( 'AH_TIPO' ).AsString := dmCliente.TipoAhorro;
               cdsLiquidacionAhorro.FieldByName( 'AH_STATUS' ).AsInteger := 1;
          end
          else
          begin
               //Montos del Empleado
               FieldByName( 'CM_TOT_AHO').AsFloat := cdsLiquidacionAhorro.FieldByName('TotalCajaAhorro').AsFloat;
               //FieldByName( 'CM_TOT_AHO' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'SALDO' ).AsFloat;
               FieldByName( 'CM_INTERES' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'TotalInteresesCA' ).AsFloat;
               FieldByName( 'CM_AFAVOR' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat +
                                                     FieldByName( 'CM_INTERES' ).AsFloat;
               FieldByName( 'CM_SAL_PRE' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosCajaCapital' ).AsFloat +
                                                    cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosCajaInteres' ).AsFloat;
               FieldByName( 'CM_DISPONIBLE' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat +
                                                       FieldByName( 'CM_INTERES' ).AsFloat -
                                                       FieldByName( 'CM_SAL_PRE' ).AsFloat -
                                                       cdsLiquidacionAhorro.FieldByName( 'ComisionesAcumCA' ).AsFloat;
               FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat := FieldByName( 'CM_DISPONIBLE' ).AsFloat;
               FieldByName( 'CM_MONTO' ).AsFloat := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               FTotalLiquidacion := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               if ( cdsLiquidacionAhorro.State = dsBrowse ) then
                  cdsLiquidacionAhorro.Edit;
               cdsLiquidacionAhorro.FieldByName( 'AH_TIPO' ).AsString := dmCliente.TipoAhorro;
               cdsLiquidacionAhorro.FieldByName( 'AH_STATUS' ).AsInteger := 1;
          end;
     end;
end;

procedure TdmCajaAhorro.RecalculaMontosAhorroLiquidacion;
begin
     with cdsCtasMovsLiquida do
     begin
          { Se toma alreves ya que lo que se esta tomando el Ahorro contrario }
          if ( dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger = 1 ) then
          begin
               //Montos del Empleado
               FieldByName('CM_TOT_AHO').AsFloat := cdsLiquidacionAhorro.FieldByName('TotalFondoAhorro').AsFloat;
               FieldByName( 'CM_INTERES' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'TotalInteresesFA' ).AsFloat;
               FieldByName( 'CM_AFAVOR' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat; //+FieldByName( 'CM_INTERES' ).AsFloat;
               FieldByName( 'CM_SAL_PRE' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosFondoCapital' ).AsFloat +
                                                      cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosFondoInteres' ).AsFloat;
               FieldByName( 'CM_DISPONIBLE' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat +
                                                         FieldByName( 'CM_INTERES' ).AsFloat -
                                                         FieldByName( 'CM_SAL_PRE' ).AsFloat -
                                                         cdsLiquidacionAhorro.FieldByName( 'ComisionesAcumFA' ).AsFloat;

               FieldByName( 'CM_TOT_AHO_EMP' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat;
               FieldByName( 'CM_INTERES_EMP' ).AsFloat := FieldByName( 'CM_INTERES' ).AsFloat;
               FieldByName( 'CM_AFAVOR_EMP' ).AsFloat :=  FieldByName( 'CM_AFAVOR' ).AsFloat;
               FieldByName( 'CM_DISPONIBLE_EMP' ).AsFloat := FieldByName( 'CM_AFAVOR' ).AsFloat;
               FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat := FieldByName( 'CM_DISPONIBLE' ).AsFloat +
                                                               FieldByName( 'CM_DISPONIBLE_EMP' ).AsFloat;

               FieldByName( 'CM_MONTO' ).AsFloat := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               //FTotalLiquidacion := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               if ( cdsLiquidacionAhorro.State = dsBrowse ) then
                  cdsLiquidacionAhorro.Edit;
               cdsLiquidacionAhorro.FieldByName( 'AH_TIPO' ).AsString := dmCliente.TipoAhorro;
               cdsLiquidacionAhorro.FieldByName( 'AH_STATUS' ).AsInteger := 1;
          end
          else
          begin
               //Montos del Empleado
               FieldByName( 'CM_TOT_AHO' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'TotalCajaAhorro' ).AsFloat;
               FieldByName( 'CM_INTERES' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'TotalInteresesCA' ).AsFloat;
               FieldByName( 'CM_AFAVOR' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat +
                                                     FieldByName( 'CM_INTERES' ).AsFloat;
               FieldByName( 'CM_SAL_PRE' ).AsFloat := cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosCajaCapital' ).AsFloat +
                                                      cdsLiquidacionAhorro.FieldByName( 'SaldoPrestamosCajaInteres' ).AsFloat;
               FieldByName( 'CM_DISPONIBLE' ).AsFloat := FieldByName( 'CM_TOT_AHO' ).AsFloat +
                                                       FieldByName( 'CM_INTERES' ).AsFloat -
                                                       FieldByName( 'CM_SAL_PRE' ).AsFloat -
                                                       cdsLiquidacionAhorro.FieldByName( 'ComisionesAcumCA' ).AsFloat;
               FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat := FieldByName( 'CM_DISPONIBLE' ).AsFloat;
               FieldByName( 'CM_MONTO' ).AsFloat := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               //FTotalLiquidacion := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
               if ( cdsLiquidacionAhorro.State = dsBrowse ) then
                  cdsLiquidacionAhorro.Edit;
               cdsLiquidacionAhorro.FieldByName( 'AH_TIPO' ).AsString := FTipoAhorroLiquida;
               cdsLiquidacionAhorro.FieldByName( 'AH_STATUS' ).AsInteger := 1;
          end;
     end;
end;


procedure TdmCajaAhorro.CM_DISPONIBLEChange(Sender: TField);
begin
     with cdsCtasMovsLiquida do
     begin
          if ( FieldByName( 'CM_MONTO' ).AsFloat > FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat ) then
               FieldByName( 'CM_MONTO' ).AsFloat := FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
     end;
end;

procedure TdmCajaAhorro.CM_FECHAOnChange( Sender: TField );

   function RedondeaD( const rMeses: TPesos ): TPesos;
   var
      rFraccion: TPesos;
   begin
        rFraccion := Frac(rMeses);
        if ( rFraccion = 0 ) or ( rFraccion < 0.25) then
           Result := Int( rMeses )
        else if ( rFraccion < 0.50 ) then
             Result := Int( rMeses ) + 0.25
        else if ( rFraccion < 0.75 ) then
             Result := Int( rMeses ) + 0.5
        else
             Result := Int( rMeses ) + 0.75
   end;

begin
     with cdsLiquidacionAhorro do
     begin
          {//@DACP VERIFICAR
          if ( cdsCtasMovsLiquida.FieldByName('CM_FECHA').AsDateTime < FieldByName('AH_FECHA').AsDateTime ) then
          begin
               cdsCtasMovsLiquida.FieldByName('CM_FECHA').FocusControl;
               raise Exception.Create('La fecha de retiro debe ser posterior o igual a la fecha de registro')
          end;
          FieldByName('DURACION').AsFloat := RedondeaD( Trunc( cdsCtasMovsLiquida.FieldByName('CM_FECHA').AsDateTime - FieldByName('AH_FECHA').AsDateTime + 1 ) / 30.4 )
          }
     end;
end;

procedure TdmCajaAhorro.RefrescarLiquidacion(const iEmpleado:Integer );
var
   oCtasMovimientos: OleVariant;
begin
     dmCliente.SetEmpleadoNumero( iEmpleado );
     with cdsLiquidacionAhorro do
     begin
          DisableControls;
          try
             Data := ServerCajaAhorro.GetLiquidaAhorro( dmCliente.Empresa, iEmpleado, dmCliente.TipoAhorro, oCtasMovimientos );
             cdsCtasMovsLiquida.Data := oCtasMovimientos;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TdmCajaAhorro.RegistrarLiquidacion( iTipo: integer );
begin
     cdsLiquidacionAhorro.PostData;
     cdsCtasMovsLiquida.PostData;

     ServerCajaAhorro.RegistrarLiquidacion( dmCliente.Empresa, cdsLiquidacionAhorro.Data, cdsCtasMovsLiquida.DeltaNull, iTipo );

     TressShell.SetDataChange( [ enPrestamo, enAhorro ] );
     cdsHisAhorros.SetDataChange;
     cdsHisPrestamos.SetDataChange;
     with cdsCtasMovsLiquida do
     begin
          if ( not IsEmpty ) and FRegImprimirCheque then
             ImprimirCheque( FieldByName( 'CT_CODIGO' ).AsString, FieldByName( 'CM_CHEQUE' ).AsInteger );
          if ( not IsEmpty ) and FRegImprimirChequeLiq then
             ImprimirCheque( FieldByName( 'CT_CODIGO' ).AsString, FieldByName( 'CM_CHEQUE' ).AsInteger, TRUE );
     end;
     
end;

procedure TdmCajaAhorro.cdsCtasMovsLiquidaBeforePost(DataSet: TDataSet);
begin
     with cdsCtasMovsLiquida do
     begin
          {@DACP VERIFICAR
          MARCO: Validar con el cliente si es esto lo que quiere.
          if NOT ( ( FieldByName('CM_MONTO').AsFloat > 0 ) AND
                   ( FieldByName('CM_MONTO').AsFloat <= FieldByName('CM_TOTAL_DISPONIBLE').AsFloat ) ) then
          begin
               Error('El monto debe ser mayor a Cero y menor al Saldo Disponible', 'CM_MONTO', cdsCtasMovsLiquida );
               Abort;
          end;
          }
          {
          if StrVacio( FieldByName('CT_CODIGO').AsString ) then
          begin
               Error('La Cuenta Bancaria no puede quedar vac�a', 'CT_CODIGO', cdsCtasMovsLiquida );
               Abort;
          end;
          }
          FieldByName('CM_DEP_RET').AsString := 'R';
          FieldByName('CM_STATUS').AsInteger := ord( saActivo );
          {
          if ( ServerCajaAhorro.ValidaCheque( dmCliente.Empresa, FieldByName( 'CT_CODIGO' ).AsString, FieldByName( 'CM_CHEQUE' ).AsInteger ) >= 1 ) then
             DB.DataBaseError( 'N�mero de cheque repetido. Es necesario cambiarlo.' );
          }
     end;

     with dmCliente.cdsEmpleadoLookup do
     begin
          cdsCtasMovsLiquida.FieldByName( 'CB_CODIGO' ).AsInteger := FieldByName( 'CB_CODIGO' ).AsInteger;
          cdsCtasMovsLiquida.FieldByName( 'CM_BENEFI' ).AsString  := FieldByName( 'CB_NOMBRES' ).AsString + ' ' +
                                                                     FieldByName( 'CB_APE_PAT' ).AsString + ' ' +
                                                                     FieldByName( 'CB_APE_MAT' ).AsString;
     end;

end;

procedure TdmCajaAhorro.cdsCtasMovsLiquidaAlCrearCampos(Sender: TObject);
begin
     with cdsCtasMovsLiquida do
     begin
          FieldByName('CM_DISPONIBLE').ONChange := CM_DISPONIBLEChange;
          FieldByName('CM_FECHA').Onchange := CM_FECHAOnChange;
     end;
end;

procedure TdmCajaAhorro.cdsCtasMovsAlAdquirirDatos(Sender: TObject);
begin
     cdsCtasMovs.Data := ServerCajaAhorro.GetCuentasMovtos( dmCliente.Empresa,'CM_FOLIO = 0' );
end;

procedure TdmCajaAhorro.RegistrarDepositoRetiro( const lAgregarNUevo: Boolean = TRUE );
begin
        if RegistroDepositoRetiro_DevEx = NIL then
            RegistroDepositoRetiro_DevEx := TRegistroDepositoRetiro_DevEx.Create(Application);

         with RegistroDepositoRetiro_DevEx do
         begin
              AgregarNuevo := lAgregarNuevo;
              ShowModal;
         end;
end;

procedure TdmCajaAhorro.cdsCtasMovsAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsCtasMovs do
     begin
          PostData;
          if ( Changecount > 0 ) then
          begin
               if Reconcile( ServerCajaAhorro.GrabaCuentasMovtos(dmCliente.Empresa, Delta, ErrorCount) ) then
                  TressShell.SetDataChange( [ enAhorro ] );
          end;
     end;
end;

procedure TdmCajaAhorro.cdsCtasMovsNewRecord(DataSet: TDataSet);
begin
     with cdsCtasMovs do
     begin
          ActualizarCodigoCuentaBancaria;
          FieldByName('CM_STATUS').AsInteger := ord( saActivo );
          FieldByName('CM_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldByName('CM_DEP_RET').AsString := 'D';
     end;
end;

procedure TdmCajaAhorro.cdsCtasMovsBeforePost(DataSet: TDataSet);
begin
     with cdsCtasMovs do
     begin
          if ( FieldByName('CM_MONTO').AsFloat = 0 ) then
          begin
               Error('El monto debe ser diferente de Cero', 'CM_MONTO', cdsCtasMovs );
               Abort;
          end;

          if StrVacio( FieldByName('CT_CODIGO').AsString ) then
          begin
               Error('La Cuenta Bancaria no puede quedar vac�a', 'CT_CODIGO', cdsCtasMovs );
               Abort;
          end;

          if ( FieldByName( 'CM_CHEQUE' ).AsInteger < 0 ) then
          begin
               Error('El n�mero de Cheque no puede ser menor que Cero', 'CM_CHEQUE', cdsCtasMovs );
               Abort;
          end;
          {
          if ( FieldByName( 'CM_CHEQUE' ).AsInteger > 0 ) then
          begin

               if ( FieldByName('CB_CODIGO').AsInteger = 0 ) then
               begin
                    Error('Se tiene que nombrar a un Beneficiario', 'CB_CODIGO', cdsCtasMovs );
                    Abort;
               end
               else
               begin
                    FieldByName( 'CM_BENEFI' ).AsString  := dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_NOMBRES' ).AsString + ' ' +
                                                            dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_APE_PAT' ).AsString + ' ' +
                                                            dmCliente.cdsEmpleadoLookup.FieldByName( 'CB_APE_MAT' ).AsString;
               end;
          end;}
          {
          if ( ServerCajaAhorro.ValidaCheque( dmCliente.Empresa, FieldByName( 'CT_CODIGO' ).AsString, FieldByName( 'CM_CHEQUE' ).AsInteger ) >= 1 ) then
             DB.DataBaseError( 'N�mero de cheque repetido. Es necesario cambiarlo.' );
          }
          if StrVacio( FieldByName('CM_TIPO').AsString )then
             DataBaseError('El tipo de dep�sito o retiro no puede quedar vac�o');
     end;
end;

procedure TdmCajaAhorro.cdsInscripcionNewRecord(DataSet: TDataSet);
begin
     with DataSet do
     begin
          FieldByName('AH_FECHA').AsDateTime := dmCliente.FechaDefault;
          FieldBynAME('AH_TIPO').AsString := dmCliente.TipoAhorro;
          FieldByName('US_CODIGO').AsInteger:= dmCliente.Usuario;
     end;
end;

procedure TdmCajaAhorro.RegistrarInscripcion( const lCiclo: Boolean = TRUE );
begin
 {$ifdef B&D}
     if ( RegistroInscripcionPE_DevEx = nil ) then
         RegistroInscripcionPE_DevEx := TRegistroInscripcionPE_DevEx.Create( Application );

     with RegistroInscripcionPE_DevEx do
     begin
          Ciclo := lCiclo;
          ShowModal;
     end;
 {$else}     
          if ( RegistroInscripcion_DevEx = nil ) then
             RegistroInscripcion_DevEx := TRegistroInscripcion_DevEx.Create( Application );

          with RegistroInscripcion_DevEx do
          begin
               Ciclo := lCiclo;
               ShowModal;
          end;
 {$endif}
end;

procedure TdmCajaAhorro.cdsInscripcionBeforePost(DataSet: TDataSet);
begin
     with cdsInscripcion do
     begin
          { MV(18/Jun/2008): Se quit� la validaci�n de poder dejar la formula Vac�a }
          {
          if StrVacio( FieldByName('AH_FORMULA').AsString ) then
          begin
               Error('La f�rmula no puede quedar vac�a', 'AH_FORMULA', cdsInscripcion );
               Abort;
          end;
          }
     end;
end;

procedure TdmCajaAhorro.cdsInscripcionAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsInscripcion do
     begin
          PostData;

          if ( Changecount > 0 ) then
             if Reconcile( ServerCajaAhorro.GrabaSaldosAhorro( dmCliente.Empresa,
                                                            Delta,
                                                            Null,
                                                            ErrorCount,
                                                            ErrorCount ) ) then
             begin
                  TressShell.SetDataChange( [ enAhorro ] );
             end;
     end;
end;

procedure TdmCajaAhorro.LlenaFiltroEmpleados( DataSet: TDataSet );
var
   sListaEmpleados: String;
begin
     FFiltroEmpleados := VACIO;
     sListaEmpleados := VACIO;
     with DataSet do
     begin
          GetAhorrosPorTipo( dmCliente.TipoAhorro );
          First;
          while ( not EOF ) do
          begin
               sListaEmpleados := ConcatString( sListaEmpleados, FieldByName( 'CB_CODIGO' ).AsString, ',' );
               Next;
          end;
          if strLleno( sListaEmpleados ) then
             FFiltroEmpleados := 'NOT ' + GetFiltroLista( 'CB_CODIGO', sListaEmpleados );
     end;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAlAdquirirDatos(Sender: TObject);
 var
    oPrestamos: OleVariant;
    oACarAbo: OleVariant;
begin
     with dmCliente do
     begin
          cdsHisAhorros.Data := ServerCajaAhorro.GetAhorro( Empresa, Empleado, TipoAhorro, oPrestamos, oACarAbo );
          cdsACarAbo.Data := oACarAbo;
          cdsHisPrestamos.Data := oPrestamos;
          cdsHisPrestamos.MergeChangeLog;
     end;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisAhorros do
     begin
          MaskPesos('AH_SALDO');
          MaskPesos('AH_NETO');
          MaskPesos('PR_SALDO');
          MaskFecha('AH_FECHA');
          ListaFija('AH_STATUS', lfStatusAhorro);
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAlAgregar(Sender: TObject);
begin
     RegistrarInscripcion( FALSE );
     cdsHisAhorros.Refrescar;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisAhorros_DevEx, TEditHisAhorros_DevEx );
end;

procedure TdmCajaAhorro.cdsHisAhorrosBeforePost(DataSet: TDataSet);
 var dCargo, dAbono : Real;
begin
     if ZetaCommonTools.StrVacio( cdsHisAhorros.FieldByName( 'AH_TIPO' ).AsString ) then
     begin
          cdsHisAhorros.FieldByName( 'AH_TIPO' ).FocusControl;
          DB.DatabaseError( 'El c�digo del ahorro no puede quedar vac�o' );
     end;
     GetSaldos( cdsACarAbo, 'CR_CARGO', 'CR_ABONO', dCargo, dAbono );
     with cdsHisAhorros do
     begin
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('AH_ABONOS').AsFloat := dAbono;
          FieldByName('AH_CARGOS').AsFloat := dCargo;
          FieldByName('AH_SALDO').AsFloat := FieldByName('AH_SALDO_I').AsFloat +
                                             FieldByName('AH_TOTAL').AsFloat +
                                             dAbono-dCargo ;
     end;
end;

procedure TdmCajaAhorro.GetSaldos( DataSet : TDataSet; const sCargo, sAbono : string;
                                   var dCargo, dAbono : Real );
begin
     dCargo := 0;
     dAbono := 0;

     with DataSet do
     begin
          DisableControls;
          First;
          while NOT EOF do
          begin
               dCargo := dCargo + FieldByName(sCargo).AsFloat;
               dAbono := dAbono + FieldByName(sAbono).AsFloat;
               Next;
          end;
          EnableControls;
     end;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAfterCancel(DataSet: TDataSet);
begin
     cdsACarAbo.CancelUpdates;
end;

procedure TdmCajaAhorro.cdsACaraboAfterDelete(DataSet: TDataSet);
begin
     if cdsHisAhorros.State = dsBrowse then
        cdsHisAhorros.Edit;
end;

procedure TdmCajaAhorro.cdsACaraboAlCrearCampos(Sender: TObject);
begin
     with TZetaClientDataSet(Sender) do
     begin
          MaskNumerico( 'CR_ABONO', '$#,0.00;-$#,0.00;#' );
          MaskNumerico( 'CR_CARGO', '$#,0.00;-$#,0.00;#' );
          FieldByName( 'CR_ABONO').OnChange := OnChangeAbono;
          FieldByName( 'CR_CARGO').OnChange := OnChangeCargo;
     end;
end;

procedure TdmCajaAhorro.OnChangeAbono(Sender: TField);
begin
     ValidaCargoAbono( Sender, 'CR_CARGO' );
end;

procedure TdmCajaAhorro.OnChangeCargo(Sender: TField);
begin
     ValidaCargoAbono( Sender, 'CR_ABONO' );
end;

procedure TdmCajaAhorro.ValidaCargoAbono( Sender : TField; const sCampo : string );
var
   AlCambiar : TFieldNotifyEvent;
begin
     with Sender do
     begin
          AlCambiar := DataSet.FieldByName(sCampo).OnChange;
          DataSet.FieldByName(sCampo).OnChange := NIL;

          if (AsFloat <=0 ) AND
             (DataSet.FieldByName(sCampo).AsFloat<=0) then
             DataBaseError( 'Cargo\abono debe ser mayor a cero')
          else if ( AsFloat > 0 ) then
               DataSet.FieldByName(sCampo).AsFloat:= 0;

          Sender.DataSet.FieldByName(sCampo).OnChange := AlCambiar;
     end;
end;

procedure TdmCajaAhorro.cdsACaraboBeforePost(DataSet: TDataSet);
begin
     with DataSet do
     begin
          if FieldByName('CR_FECHA').AsDateTime = 0 then
             DatabaseError( 'Fecha no puede quedar vac�a');

          if (FieldByName('CR_CARGO').AsFloat <=0 ) AND
             (FieldByName('CR_ABONO').AsFloat <=0 ) then
             DatabaseError( 'Cargo\abono debe ser mayor a cero')
     end;
end;

procedure TdmCajaAhorro.cdsACaraboNewRecord(DataSet: TDataSet);
begin
     with cdsACarAbo do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('CR_FECHA').AsDateTime:= dmCliente.FechaDefault;
          FieldByName('AH_TIPO').AsString  := cdsHisAhorros.FieldByName('AH_TIPO').AsString;
          FieldByName('CR_CAPTURA').AsDateTime:= dmCliente.FechaDefault;
     end;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorCountCarAbo: Integer;
begin
     with cdsHisAhorros do
     begin
          PostData;
          cdsACarAbo.PostData;

          if ( Changecount > 0 ) or (cdsACarAbo.ChangeCount > 0 ) then
          begin
               if Reconcile( ServerCajaAhorro.GrabaSaldosAhorro( dmCliente.Empresa,
                                                                 DeltaNull,
                                                                 cdsACarAbo.DeltaNull,
                                                                 ErrorCount,
                                                                 ErrorCountCarAbo ) ) then
               begin
                    cdsHisAhorros.Refrescar;
                    TressShell.SetDataChange( [ enAhorro ] );
               end;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsHisAhorrosAfterDelete(DataSet: TDataSet);
begin
     cdsHisAhorros.Enviar;
end;

procedure TdmCajaAhorro.cdsHisPrestamosAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsHisPrestamos do
     begin
          MaskPesos('PR_SALDO');
          MaskPesos('PR_MONTO');
          MaskPesos('PR_PAGOS');
          MaskFecha('PR_FECHA');
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          ListaFija( 'CM_PRESTA', lfTiposdePrestamo );
          FieldByName( 'PR_MONTO_S' ).OnChange := PR_MontosChange;
          FieldByName( 'PR_TASA' ).OnChange := PR_MontosChange;
          //FieldByName( 'PR_MESES' ).OnChange := PR_MESESChange;
          //FieldByName( 'PR_INTERES' ).OnChange := PR_INTERESChange;
          //FieldByName( 'PR_MONTO' ).OnChange := PR_PagosChange;
          //FieldByName( 'PR_PAGOS' ).OnChange := PR_PagosChange;
          CreateCalculated( 'PR_FON_MOT', ftInteger, 0 );
     end;
end;

procedure TdmCajaAhorro.cdsHisPrestamosAfterDelete(DataSet: TDataSet);
begin
     cdsHisPrestamos.Enviar;
end;

procedure TdmCajaAhorro.cdsHisPrestamosAlAgregar(Sender: TObject);
begin
     RegistrarPrestamos;
     cdsHisAhorros.Refrescar;
end;

procedure TdmCajaAhorro.cdsHisPrestamosAlModificar(Sender: TObject);
begin
     if cdsHisPrestamos.RecordCount > 0 then
     begin
          cdsPCarAbo.Refrescar;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisPrestamos_DevEx, TEditHisPrestamos_DevEx );
     end
     else
         ZetaDialogo.ZInformation('Pr�stamos',' No existen pr�stamos para modificar ',0);
end;

procedure TdmCajaAhorro.cdsPCarAboAfterDelete(DataSet: TDataSet);
begin
     {$ifdef B&D}
     if cdsTotalHisPrestamos.State = dsBrowse then
        cdsTotalHisPrestamos.Edit;
     {$else}
     if cdsHisPrestamos.State = dsBrowse then
        cdsHisPrestamos.Edit;
     {$endif}

end;

procedure TdmCajaAhorro.cdsPCarAboNewRecord(DataSet: TDataSet);
begin
     with cdsPCarAbo do
     begin
          FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
          FieldByName('CR_FECHA').AsDateTime:= dmCliente.FechaDefault;
          {$ifdef B&D}
          FieldByName('PR_TIPO').AsString  := cdsTotalHisPrestamos.FieldByName('PR_TIPO').AsString;
          FieldByName('PR_REFEREN').AsString  := cdsTotalHisPrestamos.FieldByName('PR_REFEREN').AsString;
          {$else}
          FieldByName('PR_TIPO').AsString  := cdsHisPrestamos.FieldByName('PR_TIPO').AsString;
          FieldByName('PR_REFEREN').AsString  := cdsHisPrestamos.FieldByName('PR_REFEREN').AsString;
          {$endif}
          FieldByName('CR_CAPTURA').AsDateTime:= dmCliente.FechaDefault;
     end;
end;

procedure TdmCajaAhorro.cdsPCarAboAlAdquirirDatos(Sender: TObject);
begin
     {$ifdef B&D}
     with cdsTotalHisPrestamos do
     {$else}
     with cdsHisPrestamos do
     {$endif}
     begin
          cdsPCarAbo.Data := ServerCajaAhorro.GetCatalogo( dmCliente.Empresa, cdsPCarAbo.Tag,
                                                           Format( 'CB_CODIGO = %d AND PR_TIPO = %s AND PR_REFEREN = %s' ,
                                                                   [ FieldByName('CB_CODIGO').AsInteger,
                                                                     EntreComillas( FieldByName('PR_TIPO').AsString ),
                                                                     EntreComillas( FieldByName('PR_REFEREN').AsString )] ) );
     end;
end;

procedure TdmCajaAhorro.cdsHisPrestamosAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorCountCarAbo: Integer;
   oParametros: TZetaParams;
begin
     with cdsHisPrestamos do
     begin
          PostData;
          cdsPCarAbo.PostData;

          if ( Changecount > 0 ) or (cdsPCarAbo.ChangeCount > 0 ) then
          begin
               oParametros:= TZetaParams.Create;
               try
                  dmCliente.CargaActivosTodos( oParametros );
                  //PENDEINTE: Reconcile no sirve, entra al If y aunque exista un error.
                  if Reconcile( ServerCajaAhorro.GrabaSaldosPrestamo( dmCliente.Empresa,
                                                                      DeltaNull,
                                                                      cdsPCarAbo.DeltaNull,
                                                                      oParametros.VarValues,
                                                                      ErrorCount,
                                                                      ErrorCountCarAbo,FForzarGuardarPrestamos,FMensajePrestamo ) ) then
                  begin
                       //cdsHisAhorros.Refrescar;    //Old code, comentado (by: am)
                       {***Correccion de bug (by: am):Agregado para corregir bug en VS 2013 que no permitia realizar
                                      mas de una operacion de guardar cambios en el grid de Cargos y Abonos. Para mas
                                      detalle ver bug 727 en proyecto NuevaImagenSistemaTress en TP}
                       cdsHisAhorros.Refrescar;
                       cdsPCarAbo.Refrescar;
                       cdsPCarAbo.MergeChangeLog;
                       {***}
                  end
                  else
                  begin
                       if FValidarReglas then
                       begin
                            FForzarGuardarPrestamos := True;
                            if Reconcile( ServerCajaAhorro.GrabaSaldosPrestamo( dmCliente.Empresa,
                                                                    DeltaNull,
                                                                    cdsPCarAbo.DeltaNull,
                                                                    oParametros.VarValues,
                                                                    ErrorCount,
                                                                    ErrorCountCarAbo,FForzarGuardarPrestamos,FMensajePrestamo ) ) then
                            begin
                                 //cdsHisAhorros.Refrescar;    //Old code, comentado (by: am)
                                {***Correccion de bug (by: am):Agregado para corregir bug en VS 2013 que no permitia realizar
                                      mas de una operacion de guardar cambios en el grid de Cargos y Abonos. Para mas
                                      detalle ver bug 727 en proyecto NuevaImagenSistemaTress en TP}
                                      cdsHisAhorros.Refrescar;
                                      cdsPCarAbo.Refrescar;
                                      cdsPCarAbo.MergeChangeLog;
                                {***}
                            end;
                       end;
                  end;
                  FForzarGuardarPrestamos := False;
                  FValidarReglas := False;
               finally
                      FreeAndNil( oParametros );
               end;
          end;
     end;

end;

procedure TdmCajaAhorro.cdsHisPrestamosBeforePost(DataSet: TDataSet);
 var dCargo, dAbono : Real;
begin
     if ZetaCommonTools.StrVacio( cdsHisPrestamos.FieldByName( 'PR_TIPO' ).AsString ) then
     begin
          cdsHisPrestamos.FieldByName( 'PR_TIPO' ).FocusControl;
          DB.DatabaseError( 'El c�digo del pr�stamo no puede quedar vac�o' );
     end;
     {OP: 02/06/08}
     if ZetaCommonTools.StrVacio( cdsHisPrestamos.FieldByName( 'PR_REFEREN' ).AsString ) then
     begin
          cdsHisPrestamos.FieldByName( 'PR_REFEREN' ).FocusControl;
          DB.DatabaseError( 'La referencia del pr�stamo no puede quedar vac�a' );{OP: 24/06/08}
     end;
     GetSaldos( cdsPCarAbo, 'CR_CARGO', 'CR_ABONO', dCargo, dAbono );
     with cdsHisPrestamos do
     begin
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('PR_ABONOS').AsFloat := dAbono;
          FieldByName('PR_CARGOS').AsFloat := dCargo;
     end;
end;

procedure TdmCajaAhorro.cdsHisPrestamosCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'PR_FON_MOT' ).AsInteger := 0;
     end;
end;

procedure TdmCajaAhorro.cdsTotalesFondoCajaAlAdquirirDatos( Sender: TObject);
 var
    oCuentas: OleVariant;
begin
     cdsTotalesFondoCaja.Data := ServerCajaAhorro.GetTotalesCajaFondo( dmCliente.Empresa, dmCliente.TipoAhorro, oCuentas );
     cdsTotalesCuentas.Data := oCuentas;
//     cdsCtasBancarias.SetDataChange;
end;


procedure TdmCajaAhorro.GetCuentasBancarias( oParams: TZetaParams );
begin
     cdsCtasMovs.Data := ServerCajaAhorro.GetCuentasBancarias( dmCliente.Empresa, oParams.VarValues, FSaldoIni );
     cdsCtasMovs.ResetDataChange;
end;


function TdmCajaAhorro.GetTotalesRetenciones( oParams: TZetaParams ): OleVariant;
begin
     Result := ServerCajaAhorro.GetTotalesRetenciones( dmCliente.Empresa, oParams.VarValues );
end;

function  TdmCajaAhorro.GrabaRetenciones( oParams: TZetaParams ): Boolean;
begin
     with cdsCtasMovs do
     begin
          Conectar;
          Append;
          with oParams do
          begin
               //Defaults
               FieldByName('CM_STATUS').AsInteger := ord( saActivo );
               FieldByName('CM_DEP_RET').AsString := 'D';
               FieldByName('CM_TIPO').AsString := 'NOMINA';

               //Capturados
               FieldByName('CT_CODIGO').AsString := ParamByName('Cuenta').AsString;
               FieldByName('CM_MONTO').AsFloat := ParamByName('Monto').AsFloat;
               FieldByName('CM_DESCRIP').AsString := ParamByName('Descripcion').AsString;
               FieldByName('CM_FECHA').AsDateTime := ParamByName('Fecha').AsDate;
               FieldByName('PE_YEAR').AsInteger := ParamByName('Year').AsInteger;
               FieldByName('PE_TIPO').AsInteger := ParamByName('Tipo').AsInteger;
               FieldByName('PE_NUMERO').AsInteger := ParamByName('Numero').AsInteger;
          end;
          Post;
          Enviar;
          Result := ChangeCount = 0; 
     end;
end;

procedure TdmCajaAhorro.cdsPeriodoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          ConectarPeriodo( YearDefault,ord( tpSemanal ) );
     end;
end;

procedure TdmCajaAhorro.ConectarPeriodo( const iYear: integer; const eTipo: eTipoPeriodo );
begin
     with cdsPeriodo do
     begin
          Data := ServerCajaAhorro.GetPeriodos( dmCliente.Empresa, iYear, Ord( eTipo ), 1 );
     end;
end;

procedure TdmCajaAhorro.ConectarPeriodoProcesos( const iYear: integer; const eTipo: eTipoPeriodo );
begin
     with cdsPeriodoProcesos do
     begin
          Data := ServerCajaAhorro.GetPeriodos( dmCliente.Empresa, iYear, Ord( eTipo ), 0 );
     end;
end;

procedure TdmCajaAhorro.cdsPeriodoGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := FALSE;
end;

procedure TdmCajaAhorro.cdsPeriodoLookupDescription( Sender: TZetaLookupDataSet; var sDescription: String);
begin
     with Sender do
     begin
          sDescription := 'De ' + FechaCorta( FieldByName( 'PE_FEC_INI' ).AsDateTime ) +
                          ' a ' + FechaCorta( FieldByName( 'PE_FEC_FIN' ).AsDateTime );
     end;
end;

procedure TdmCajaAhorro.cdsPeriodoLookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodo do
     begin
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsPeriodoLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
begin
     lOk :=    ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( Sender, sFilter, sKey, sDescription );
end;



procedure TdmCajaAhorro.cdsTotalesFondoCajaAlCrearCampos(Sender: TObject);
begin
     with cdsTotalesFondoCaja do
     begin
          //MaskPesos( 'Cuantos_Socios' );
          MaskPesos( 'Total_Ahorrado' );
          //MaskPesos( 'Num_Prestamos' );
          MaskPesos( 'Saldo_Prestamos' );
          MaskPesos( 'Saldo_Bancos' );
     end;
end;

procedure TdmCajaAhorro.CalculaEstadoCuenta;
var
   oBeforePost: TDataSetNotifyEvent;
   rDepositos, rRetiros,rSaldos : TPesos;
   sOldIndex: string;
begin
     rDepositos := 0;
     rRetiros := 0;
     rSaldos := FSaldoIni;

     with cdsCtasMovs do
     begin
          DisableControls;
          oBeforePost:= BeforePost;
          BeforePost := NIL;
          sOldIndex := IndexFieldNames;
          try
             First;
             {Edit;
             FieldByName('SALDO').AsFloat := FSaldoIni;
             Next;}

             while NOT EOF do
             begin
                  if ( eStatusAhorro( FieldByName('CM_STATUS').AsInteger ) = saActivo ) then
                  begin
                       Edit;
                       FieldByName('DEPOSITOS').AsFloat := rDepositos + FieldByName('CM_DEPOSIT' ).AsFloat;
                       FieldByName('RETIROS').AsFloat := rRetiros + FieldByName('CM_RETIRO' ).AsFloat;
                       FieldByName('SALDO').AsFloat := rSaldos + FieldByName('CM_DEPOSIT').AsFloat - FieldByName('CM_RETIRO').AsFloat;

                       rDepositos := FieldByName('DEPOSITOS').AsFloat ;
                       rRetiros := FieldByName('RETIROS').AsFloat ;
                       rSaldos := FieldByName('SALDO').AsFloat ;
                  end;
                  Next;                  
             end;
             MergeChangeLog;
          finally
                 BeforePost := oBeforePost;
                 IndexFieldNames := 'CM_FECHA;CM_DEP_RET';
                 EnableControls;
          end;
     end;
end;
procedure TdmCajaAhorro.cdsCtasMovsAlCrearCampos(Sender: TObject);
begin
     cdsTiposDeposito.Conectar;
     with cdsCtasMovs do
     begin
          MaskPesos( 'CM_MONTO' );
          MaskPesos( 'CM_RETIRO' );
          MaskPesos( 'CM_DEPOSIT' );
          MaskNumerico( 'CM_RETIRO', '#,0.00;-#,0.00;#' );
          MaskNumerico( 'CM_DEPOSIT', '#,0.00;-#,0.00;#' );
          //DevEx: Agregado para que en el nuevo grid la fecha salga con el formato habitual, por alguna razon no estaba y el
          //ZetaDBGrid si lo formateaba, sin embargo es mejor tenerlo asi.
          MaskFecha ('CM_FECHA' );
          if ( FindField('SALDO') <> NIL ) then
          begin
               MaskPesos( 'SALDO' );
          end;
          //CreateSimpleLookUp( cdsTiposDeposito, 'TB_ELEMENT', 'CT_CODIGO' );
          CreateSimpleLookUp( cdsTiposDeposito, 'TB_ELEMENT', 'CM_TIPO' );
     end;
end;

procedure TdmCajaAhorro.cdsCtasMovsAfterOpen(DataSet: TDataSet);
begin
     CalculaEstadoCuenta;
end;

procedure TdmCajaAhorro.cdsCtasMovsAlModificar(Sender: TObject);
begin
     RegistrarDepositoRetiro( FALSE );
     cdsCtasBancarias.SetDataChange;
     cdsCtasMovs.SetDataChange;
end;

procedure TdmCajaAhorro.cdsCtasMovsAlBorrar(Sender: TObject);
begin
     cdsCtasMovs.Delete;
     cdsCtasMovs.Enviar;
end;

function TdmCajaAhorro.ValidarCheque(const sCuentaBancaria: String; iCheque: integer): boolean;
begin
     Result := ( ServerCajaAhorro.ValidaCheque( dmCliente.Empresa, sCuentaBancaria, iCheque ) >= 1 );
end;

procedure TdmCajaAhorro.BorrarIncripcion;
begin
     with cdsHisAhorros do
     begin
          if ( FieldByName( 'AH_SALDO' ).AsFloat > 0 ) then
          begin
               if ZetaDialogo.ZWarningConfirm( 'Ahorros',
                                               Format( 'El empleado %d tiene un saldo ahorrado de %n.' +CR_LF+ '�Desea realizar la liquidaci�n?', [ dmCliente.Empleado,
                                                                                                                                                    FieldByName( 'AH_SALDO' ).AsFloat] ), 0, mbCancel ) then
                  LiquidarAhorro;
          end
          else
          begin
               if ZetaDialogo.ZWarningConfirm( 'Ahorros', '�Desea dar de baja del ahorro al empleado?', 0, mbCancel ) then
               begin
                    if ( State = dsBrowse ) then
                       Edit;
                    FieldByName( 'AH_STATUS' ).AsInteger := ord( saCancelado );
                    Enviar;
               end;
          end;
     end;
end;

procedure TdmCajaAhorro.GetAhorrosPorTipo(const sTipoAhorro: string);
const K_FILTRO = ' AH_TIPO = %s ';
begin
     cdsAhorros.Data := ServerCajaAhorro.GetCatalogo( dmCliente.Empresa,cdsAhorros.Tag,Format( K_FILTRO,[ EntreComillas( sTipoAhorro ) ]) );
end;

procedure TdmCajaAhorro.cdsRegPrestamosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;UpdateKind: TUpdateKind; var Action: TReconcileAction);
//var sError: string;
begin
     if not FValidarReglas then
     begin
          //FMensajePrestamo := E.Message;
         if not PK_VIOLATION( E ) then //if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
         begin
              if CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_SIST_KARDEX )then
              begin
                   Action := Zreconcile.HandleReconcilePrestamosWrng(DataSet,UpdateKind,E,FMensajePrestamo);
                   FValidarReglas := ( Action = raCorrect );
              end
              else
                   Action := Zreconcile.HandleReconcilePrestamosError(DataSet,UpdateKind,E,FMensajePrestamo);
         end
         else
              Action := Zreconcile.HandleReconcileError(DataSet,UpdateKind,E);
     end
     else
     begin
          Action := Zreconcile.HandleReconcileError(DataSet,UpdateKind,E);
     end;

end;

{cdsTiposDeposito}

procedure TdmCajaAhorro.cdsTiposDepositoBeforePost(DataSet: TDataSet);
const
     K_T_PRESTA = 'PRESTA';
     K_T_LIQUID = 'LIQUID';
     K_T_RETPAR = 'RETPAR';
     K_T_NOMINA = 'NOMINA';

function EsTipoSistema( const sTipo: String ) : Boolean;
   begin
        Result := ( sTipo = K_T_PRESTA ) or ( sTipo = K_T_LIQUID ) or ( sTipo = K_T_RETPAR ) or
                  ( sTipo = K_T_NOMINA );
   end;

begin
     cdsCatalogosBeforePost(DataSet);
     with DataSet do
     begin
          if StrVacio( FieldByName('TB_ELEMENT').AsString )then
             DataBaseError('La descripci�n no puede quedar vac�a');
              if ( State = dsInsert ) and  EsTipoSistema( FieldByName('TB_CODIGO').AsString ) then
             DatabaseError( 'El c�digo de tipo de dep�sito � retiro: (' + FieldByName('TB_CODIGO').AsString + ') est� reservado por el sistema' );
     end;
end;

procedure TdmCajaAhorro.cdsTiposDepositoBeforeDelete(DataSet: TDataSet);
begin
     if ZetaCommonTools.zStrToBool( cdsTiposDeposito.FieldByName( 'TB_SISTEMA' ).AsString ) then
        DB.DatabaseError( 'No se permite borrar tipos de dep�sito y retiro del sistema' )
end;

procedure TdmCajaAhorro.cdsTiposDepositoNewRecord(DataSet: TDataSet);
begin
       cdsTiposDeposito.FieldByName( 'TB_SISTEMA' ).AsString := K_GLOBAL_NO;
end;

procedure TdmCajaAhorro.cdsTiposDepositoGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_AHORRO_CAT_TIPO_DEPOSITO, iRight );
end;

{ cdsTotalesCuentas }

procedure TdmCajaAhorro.cdsTotalesCuentasAlCrearCampos(Sender: TObject);
begin
     cdsTotalesCuentas.MaskPesos('SALDO_HOY');
end;

procedure TdmCajaAhorro.cdsTotalesCuentasAlAgregar(Sender: TObject);
begin
     with cdsCtasBancarias do
     begin
          Conectar;
          Agregar;
     end;
end;

procedure TdmCajaAhorro.cdsTotalesCuentasAlModificar(Sender: TObject);
begin
     with cdsCtasBancarias do
     begin
          Conectar;
          if Locate( 'CT_CODIGO', cdsTotalesCuentas.FieldByName( 'CT_CODIGO' ).AsString, [] ) then
             Modificar;
     end;
end;

procedure TdmCajaAhorro.cdsTotalesCuentasAlBorrar(Sender: TObject);
begin
     with cdsCtasBancarias do
     begin
          Conectar;
          if Locate( 'CT_CODIGO', cdsTotalesCuentas.FieldByName( 'CT_CODIGO' ).AsString, [] ) then
             Borrar;
     end;
end;

function TdmCajaAhorro.EsPrestamoFonacot( const sTipo: String ): Boolean;
begin
     Result := ( Global.GetGlobalString(K_GLOBAL_FONACOT_PRESTAMO) = sTipo );
end;

procedure TdmCajaAhorro.cdsHisPrestamosReconcileError(
  DataSet: TCustomClientDataSet; E: EReconcileError;
  UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if not FValidarReglas then
     begin
          if not PK_VIOLATION( E ) then //if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
          begin
               if CheckDerecho(D_EMP_NOM_PRESTAMOS,K_DERECHO_SIST_KARDEX )then
               begin
                    Action := Zreconcile.HandleReconcilePrestamosWrng(DataSet,UpdateKind,E,FMensajePrestamo);
                    FValidarReglas := ( Action = raCorrect);
               end
               else
                   Action := Zreconcile.HandleReconcilePrestamosError(DataSet,UpdateKind,E,FMensajePrestamo);
          end
          else
              Action := Zreconcile.HandleReconcileError(DataSet,UpdateKind,E);
     end
     else
     begin
          Action := Zreconcile.HandleReconcileError(DataSet,UpdateKind,E);
     end;
end;

procedure TdmCajaAhorro.cdsTAhorroNewRecord(DataSet: TDataSet);
begin
     DataSet.FieldByName( 'TB_LIQUIDA' ).AsInteger := Ord( taAutomaticamente );
     if ( DataSet = cdsTAhorro ) then
     begin
          DataSet.FieldByName( 'TB_VAL_RAN' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmCajaAhorro.ActualizarCodigoCuentaBancaria;
const
     K_FILTRO_CTAS = '( AH_TIPO = %s )';
begin
     with dmCajaAhorro.cdsCtasBancarias do
     begin
          try
             Filtered := False;
             Filter := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
             Filtered := True;
             First;
             cdsCtasMovs.FieldByName('CT_CODIGO').AsString := cdsCtasBancarias.FieldByName('CT_CODIGO').AsString;
          finally
                 Filtered := False;
                 Filter := VACIO;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsTotalHisAhorrosAlAdquirirDatos(Sender: TObject);
var
   oPrestamos: OleVariant;
   oACarAbo: OleVariant;
begin
     with dmCliente do
     begin
          cdsTotalHisAhorros.Data := ServerCajaAhorro.GetTotalAhorro( Empresa, Empleado, '1', '2', oPrestamos, oACarAbo );
          cdsACarAbo.Data := oACarAbo;
          cdsTotalHisPrestamos.Data := oPrestamos;
          cdsTotalHisPrestamos.MergeChangeLog;
     end;
end;

procedure TdmCajaAhorro.cdsTotalHisAhorrosAlCrearCampos(Sender: TObject);
begin
     with cdsTotalHisAhorros do
     begin
          MaskPesos('TotalFondoAhorro');
          MaskPesos('TotalCajaAhorro');
          MaskPesos('SaldoPrestamosFondo');
          MaskPesos('SaldoPrestamosCaja');
          MaskPesos('SubTotalFondoAhorro');
          MaskPesos('SubTotalCajaAhorro');
          MaskPesos('Neto');
          MaskFecha('AH_FECHA_INI_CAJA');
          MaskFecha('AH_FECHA_INI_FONDO');
          ListaFija('AH_STATUS_CAJA', lfStatusAhorro);
          ListaFija('AH_STATUS_FONDO', lfStatusAhorro);
     end;
end;

function TdmCajaAhorro.ExistenPrestamosActivos(TipoPrestamo: integer): Boolean;
var
   sTipo: string;
begin
     { S�lo se valida el Tipo de Ahorro con TB_NUMERO = 2 }
     if ( TipoPrestamo = 2 ) then       //Fondo de Ahorro
     begin
          sTipo := ServerCajaAhorro.GetTipoPrestamo( dmCliente.Empresa, TipoPrestamo );
          with cdsTotalHisPrestamos do
          begin
               if strLleno( sTipo ) then
                  Filter := Format( 'PR_TIPO = %s', [ Entrecomillas( sTipo ) ] )
               else
                   Filter := 'PR_STATUS = 0';
               Filtered := True;
               try
                  Result := ( RecordCount > 0 );
               finally
                      Filtered := False;
               end;
          end;
     end
     else     //Caja de Ahorro
     begin
          sTipo := ServerCajaAhorro.GetTipoPrestamo( dmCliente.Empresa, TipoPrestamo );
          with cdsTotalHisPrestamos do
          begin
               if strLleno( sTipo ) then
                  Filter := Format( 'PR_STATUS = 0 and PR_TIPO = %s', [ Entrecomillas( sTipo ) ] )
               else
                   Filter := 'PR_STATUS = 0';
               Filtered := True;
               try
                  Result := ( RecordCount > 0 );
               finally
                      Filtered := False;
               end;
          end;
     end;
end;

function TdmCajaAhorro.DiasLiquidacionPrestamo(TipoPrestamo: integer): integer;
var
   iDias : integer;
   dFechaInicio, dFechaTemp : TDateTime;
   sTipoPrestamo: string;
begin
     Result := -1; //No hubo registros
     with cdsTotalHisPrestamos do
     begin
          //Filtrar prestamos activos del tipo seleccionado
          sTipoPrestamo := ServerCajaAhorro.GetTipoPrestamo( dmCliente.Empresa, TipoPrestamo );
          if strLleno( sTipoPrestamo ) then
          begin
               Filter := Format( 'PR_STATUS = 2 and PR_TIPO = %s', [ EntreComillas( sTipoPrestamo ) ] );
               Filtered := True;
               try
                  if ( RecordCount > 0 ) then
                  begin
                       //Buscar Fecha mas actual
                       First;
                       dFechaInicio := FieldByName( 'PR_FECHA' ).AsDateTime;
                       Next;
                       while not EOF do
                       begin
                            dFechaTemp := FieldByName( 'PR_FECHA' ).AsDateTime;
                            if ( dFechaTemp > dFechaInicio ) then
                               dFechaInicio := dFechaTemp;
                            Next;
                       end;
                       iDias := Trunc( Now - dFechaInicio );
                       Result := iDias;
                  end;
               finally
                      Filter := VACIO;
                      Filtered := False;
               end;
          end;
     end;
end;

function TdmCajaAhorro.GetTipoInteres(TipoPrestamo: integer): string;
begin
     Result := ServerCajaAhorro.GetTipoInteres( dmCliente.Empresa, TipoPrestamo );
end;

procedure TdmCajaAhorro.cdsTotalHisPrestamosAfterDelete(DataSet: TDataSet);
begin
     cdsTotalHisPrestamos.Enviar;
end;

procedure TdmCajaAhorro.cdsTotalHisPrestamosAlAgregar(Sender: TObject);
begin
     RegistrarPrestamos;
     cdsHisAhorros.Refrescar;
end;

procedure TdmCajaAhorro.cdsTotalHisPrestamosAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with cdsTotalHisPrestamos do
     begin
          MaskPesos( 'PR_SALDO' );
          MaskPesos( 'PR_MONTO' );
          MaskFecha( 'PR_FECHA' );
          ListaFija( 'PR_STATUS', lfStatusPrestamo );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
          ListaFija( 'CM_PRESTA', lfTiposdePrestamo );
          FieldByName( 'PR_MONTO_S' ).OnChange := PR_MontosChange;
          FieldByName( 'PR_TASA' ).OnChange := PR_MontosChange;
     end;
end;

procedure TdmCajaAhorro.cdsTotalHisPrestamosAlEnviarDatos(Sender: TObject);
var
   ErrorCount, ErrorCountCarAbo: Integer;
   oParametros: TZetaParams;
begin
     with cdsTotalHisPrestamos do
     begin
          PostData;
          cdsPCarAbo.PostData;

          if ( Changecount > 0 ) or (cdsPCarAbo.ChangeCount > 0 ) then
          begin
               oParametros:= TZetaParams.Create;
               try
                  if Reconcile( ServerCajaAhorro.GrabaSaldosPrestamo( dmCliente.Empresa,
                                                                      DeltaNull,
                                                                      cdsPCarAbo.DeltaNull,
                                                                      oParametros.VarValues,
                                                                      ErrorCount,
                                                                      ErrorCountCarAbo,FForzarGuardarPrestamos,FMensajePrestamo ) ) then
                  begin
                       cdsTotalHisAhorros.Refrescar;
                  end;
                  FForzarGuardarPrestamos := False;
                  FValidarReglas := False;
               finally
                      FreeAndNil( oParametros );
               end;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsTotalHisPrestamosAlModificar(Sender: TObject);
begin
     if cdsTotalHisPrestamos.RecordCount > 0 then
     begin
          cdsPCarAbo.Refrescar;
          ZBaseEdicion_DevEx.ShowFormaEdicion( EditHisPrestamos_DevEx, TEditHisPrestamos_DevEx )
     end
     else
         ZetaDialogo.ZInformation('Pr�stamos',' No existen pr�stamos para modificar ',0);
end;

procedure TdmCajaAhorro.cdsTotalHisPrestamosBeforePost(DataSet: TDataSet);
var
   dCargo, dAbono : Real;
begin
     if ZetaCommonTools.StrVacio( cdsTotalHisPrestamos.FieldByName( 'PR_TIPO' ).AsString ) then
     begin
          cdsTotalHisPrestamos.FieldByName( 'PR_TIPO' ).FocusControl;
          DB.DatabaseError( 'El c�digo del pr�stamo no puede quedar vac�o' );
     end;
     GetSaldos( cdsPCarAbo, 'CR_CARGO', 'CR_ABONO', dCargo, dAbono );
     with cdsTotalHisPrestamos do
     begin
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('PR_ABONOS').AsFloat := dAbono;
          FieldByName('PR_CARGOS').AsFloat := dCargo;
          FieldByName('PR_SALDO').AsFloat := FieldByName('PR_MONTO').AsFloat -
                                             FieldByName('PR_SALDO_I').AsFloat -
                                             FieldByName('PR_TOTAL').AsFloat -
                                             dAbono + dCargo;
     end;
     SetStatusPrestamo( cdsTotalHisPrestamos );
end;

procedure TdmCajaAhorro.SetStatusPrestamo( DataSet: TDataSet );
var
   OldStatus, NewStatus : eStatusPrestamo;
begin
     with DataSet do
     begin
          OldStatus:= eStatusPrestamo( FieldByName( 'PR_STATUS' ).AsInteger );
          NewStatus:= OldStatus;
          if ( OldStatus <> spCancelado ) then
          begin
               if ( FieldByName( 'PR_MONTO' ).AsFloat >= 0 ) then
               begin
                    if ( FieldByName( 'PR_SALDO' ).AsFloat <= 0 ) then
                       NewStatus:= spSaldado
                    else if ( OldStatus <> spCancelado ) then
                       NewStatus:= spActivo;
               end
               else
               begin
                    if ( FieldByName( 'PR_SALDO' ).AsFloat >= 0 ) then
                       NewStatus:= spSaldado
                    else if ( OldStatus <> spCancelado ) then
                       NewStatus:= spActivo;
               end;
               if ( OldStatus <> NewStatus ) then
               begin
                    FieldByName( 'PR_STATUS' ).AsInteger := Ord( NewStatus );
                    //ZInformation('Pr�stamos', 'El Status del Pr�stamo se Ajust� a : ' +
                                              //ObtieneElemento( lfStatusPrestamo, Ord( NewStatus ) ), 0 );
               end;
          end;
     end;
end;

function TdmCajaAhorro.GeTotalesRepartoIntereses( oParams: TZetaParams ): OleVariant;
begin
     Result := ServerCajaAhorro.GetTotalRepartoInteres( dmCliente.Empresa, oParams.VarValues );
end;

procedure TdmCajaAhorro.GetRepartoIntereses(Parametros: TZetaParams);
begin
     with cdsRepartoIntereses do
     begin
          Data := ServerCajaAhorro.GetDatosRepartoIntereses( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmCajaAhorro.cdsRepartoInteresesAlCrearCampos(Sender: TObject);
begin
     with cdsRepartoIntereses do
     begin
          MaskPesos( 'AHORRO_SEMANAL' );
          MaskPesos( 'AHORRO_ACUMULADO' );
          MaskPesos( 'INTERES_ACUMULADO' );
          MaskPesos( 'COMISIONES_ACUMULADO' );
          MaskPesos( 'CAPITAL' );
          {
          MaskTipoCambio( 'INTERESES_INV' );
          MaskTipoCambio( 'INTERESES_PRESTAMO' );
          MaskTipoCambio( 'MONTO_COMISIONES' );
          }
          MaskPesos( 'INTERESES_INV' );
          MaskPesos( 'INTERESES_PRESTAMO' );
          MaskPesos( 'MONTO_COMISIONES' );
     end;
end;

function TdmCajaAhorro.GrabarIntereses(Parametros: TZetaParams): Boolean;
begin
     Result := not cdsRepartoIntereses.IsEmpty;
     ServerCajaAhorro.GrabarIntereses( dmCliente.Empresa, cdsRepartoIntereses.Data, Parametros.VarValues );
end;

procedure TdmCajaAhorro.CancelaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerCajaAhorro.CancelaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmCajaAhorro.cdsDataSetAlCrearCampos(Sender: TObject);
begin
     cdsTAhorro.Conectar;
     with cdsDataSet do
     begin
          CreateSimpleLookUp( cdsTAhorro, 'TB_ELEMENT', 'AH_TIPO' );
          MaskFecha( 'CR_FECHA' );
     end;
end;

function TdmCajaAhorro.CancelaTodos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CancelaTodosVerifica( GetLista( cdsDataset ), Parametros )
     else
         Result := CancelaTodosNuevo( Parametros );
end;

function TdmCajaAhorro.GetLista(ZetaDataset: TZetaClientDataset): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

function TdmCajaAhorro.CancelaTodosVerifica( const Lista: OleVariant; Parametros: TZetaParams ): boolean;
begin
     cdsVerifica.Data := Lista;
     Result := not cdsVerifica.IsEmpty;
     ServerCajaAhorro.CancelaIntereses( dmCliente.Empresa, cdsVerifica.Data );
end;

function TdmCajaAhorro.CancelaTodosNuevo( Parametros: TZetaParams ): boolean;
begin
     CancelaGetLista( Parametros );
     Result := not cdsDataSet.IsEmpty;
     ServerCajaAhorro.CancelaIntereses( dmCliente.Empresa, cdsDataSet.Data );
end;

procedure TdmCajaAhorro.cdsPeriodo2AlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          ConectarPeriodo2( YearDefault, Ord( tpSemanal ) );
     end;
end;

procedure TdmCajaAhorro.cdsPeriodoBusquedaAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          ConectarPeriodoBusqueda( YearDefault, Ord( tpSemanal ) );
     end;
end;

procedure TdmCajaAhorro.ConectarPeriodo2( const iYear: integer; const eTipo: eTipoPeriodo );
begin
     with cdsPeriodo2 do
     begin
          Data := ServerCajaAhorro.GetPeriodos( dmCliente.Empresa, iYear, Ord( eTipo ), 1 );
     end;
end;

procedure TdmCajaAhorro.ConectarPeriodoBusqueda( const iYear: integer; const eTipo: eTipoPeriodo );
begin
     with cdsPeriodoBusqueda do
     begin
          Data := ServerCajaAhorro.GetPeriodos( dmCliente.Empresa, iYear, Ord( eTipo ), 1 );
     end;
end;

procedure TdmCajaAhorro.cdsPeriodo2LookupKey(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodo2 do
     begin
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsEmpleadosAvalAlAdquirirDatos(Sender: TObject);
begin
     with cdsEmpleadosAval do
     begin
          Data := ServerCajaAhorro.GetEmpleadosAval( dmCliente.Empresa, cdsTAhorro.FieldByName( 'TB_CODIGO' ).AsString );
     end;
end;

{ cdsExcepMontos }
procedure TdmCajaAhorro.cdsExcepMontosAfterEdit(DataSet: TDataSet);
begin
     with cdsExcepMontos do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmCajaAhorro.cdsExcepMontosAlAdquirirDatos(Sender: TObject);
begin
     cdsExcepMontos.Data:= ServerNomina.GetDatosExcepMontos( dmCliente.Empresa, FYearEmpleado, Ord( FTipoNominaEmpleado ), FPeriodoEmpleado, False );
end;

procedure TdmCajaAhorro.cdsExcepMontosAlBorrar(Sender: TObject);
begin
     with cdsExcepMontos do
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Esta Excepci�n ?' ) then
          begin
               Delete;
               Enviar;
          end;
     end;
end;

procedure TdmCajaAhorro.cdsExcepMontosAlCrearCampos(Sender: TObject);
begin
     {
     cdsTPresta.Conectar;
     with cdsExcepMontos do
     begin
          MaskPesos( 'MO_MONTO' );
          MaskPesos( 'MO_X_ISPT' );
          MaskPesos( 'MO_IMP_CAL' );
          ListaFija( 'CO_TIPO', lfTipoConcepto );
          FieldByName( 'CB_CODIGO' ).OnValidate:= ExcepcionesCB_CODIGOValidate;
          FieldByName( 'CB_CODIGO' ).OnChange:= cdsExcepMontosCB_CODIGOChange;
          FieldByName( 'CO_NUMERO' ).OnValidate:= cdsExcepMontosCO_NUMEROValidate;
          FieldByName( 'CO_NUMERO' ).OnChange:= cdsExcepMontosCO_NUMEROChange;
          FieldByName( 'MO_PERCEPC' ).OnGetText := cdsExcepMontosMO_PERCEPCGetText;
          FieldByName( 'MO_DEDUCCI' ).OnGetText := cdsExcepMontosMO_DEDUCCIGetText;
          FieldByName( 'MO_REFEREN' ).OnValidate:= cdsExcepMontosMO_REFERENValidate;
     end;
     }
end;

procedure TdmCajaAhorro.cdsExcepMontosAlEnviarDatos(Sender: TObject);
begin
     GrabaExcepcionesMonto( cdsExcepMontos );
end;

procedure TdmCajaAhorro.cdsExcepMontosNewRecord(DataSet: TDataSet);
begin
     with cdsExcepMontos do
     begin
          FieldByName( 'PE_YEAR' ).AsInteger := FYearEmpleado;
          FieldByName( 'PE_TIPO' ).AsInteger := Ord( FTipoNominaEmpleado );
          FieldByName( 'PE_NUMERO' ).AsInteger := FPeriodoEmpleado;
          FieldByName( 'US_CODIGO' ).AsInteger := dmCliente.Usuario;
          FieldByName( 'MO_ACTIVO' ).AsString := K_GLOBAL_SI;
          FieldByName( 'MO_PER_CAL' ).AsString := K_GLOBAL_NO;
          FieldByName( 'MO_REFEREN' ).AsString := VACIO;
          FieldByName( 'MO_PERCEPC' ).AsFloat := 0.0;
          FieldByName( 'MO_DEDUCCI' ).AsFloat := 0.0;
          FieldByName( 'MO_MONTO' ).AsFloat := 0.0;
     end;
end;

procedure TdmCajaAhorro.cdsExcepMontosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raCancel;
end;

procedure TdmCajaAhorro.GrabaExcepcionesMonto( Dataset: TZetaClientDataset );
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with Parametros do
        begin
             AddInteger( 'Year', FYearEmpleado );
             AddInteger( 'Tipo', Ord( FTipoNominaEmpleado ) );
             AddInteger( 'Numero', FPeriodoEmpleado );
        end;
        GrabaExcepcionesMontoParams( Dataset, Parametros );
     finally
            Parametros.Free;
     end;
end;

procedure TdmCajaAhorro.GrabaExcepcionesMontoParams( Dataset: TZetaClientDataset; oParams: TZetaParams );
var
   ErrorCount: Integer;
   ErrorVar, ErrorData: OleVariant;
begin
     ErrorCount := 0;
     with Dataset do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Repeat
                     oParams.AddInteger( 'Operacion', Ord( ocSustituir ) );
                     ErrorVar := ServerNomina.GrabaMovMontos( dmCliente.Empresa, oParams.VarValues, Delta, ErrorCount, ErrorData );
               Until ( Reconciliar( ErrorVar ) );
          end;
     end;
end;                               

procedure TdmCajaAhorro.cdsPeriodoProcesosAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          ConectarPeriodoProcesos( YearDefault, Ord( tpSemanal ) );
     end;
end;

procedure TdmCajaAhorro.cdsPeriodoProcesosLookupKey( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String);
begin
     with cdsPeriodoProcesos do
     begin
          lOk := Locate( 'PE_NUMERO', StrToIntDef( sKey, 0 ), [] );
          if lOk then
          begin
               sDescription := GetDescription;
          end;
     end;
end;

end.
