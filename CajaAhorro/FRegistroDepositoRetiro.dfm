inherited RegistroDepositoRetiro: TRegistroDepositoRetiro
  Left = 420
  Top = 246
  Caption = 'Tipo de dep'#243'sito / retiro'
  ClientHeight = 327
  ClientWidth = 471
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 68
    Top = 220
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Beneficiario:'
  end
  object Label8: TLabel [1]
    Left = 93
    Top = 126
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
  end
  object Label11: TLabel [2]
    Left = 45
    Top = 102
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cuenta bancaria:'
  end
  object Label3: TLabel [3]
    Left = 102
    Top = 172
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object lbMonto: TLabel [4]
    Left = 93
    Top = 148
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Monto:'
  end
  object lbCheque: TLabel [5]
    Left = 76
    Top = 196
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cheque #:'
    Enabled = False
  end
  object BtnSiguiente: TSpeedButton [6]
    Left = 248
    Top = 190
    Width = 25
    Height = 25
    Hint = 'Sugiere siguiente n'#250'mero de cheque'
    Enabled = False
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333FF3333333333333003333333333333F77F33333333333009033
      333333333F7737F333333333009990333333333F773337FFFFFF330099999000
      00003F773333377777770099999999999990773FF33333FFFFF7330099999000
      000033773FF33777777733330099903333333333773FF7F33333333333009033
      33333333337737F3333333333333003333333333333377333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = BtnSiguienteClick
  end
  object Label13: TLabel [7]
    Left = 67
    Top = 244
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object LBL_STATUS_MOV: TLabel [8]
    Left = 22
    Top = 268
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status de movimiento:'
  end
  inherited PanelBotones: TPanel
    Top = 291
    Width = 471
    TabOrder = 11
    inherited OK: TBitBtn
      Left = 303
    end
    inherited Cancelar: TBitBtn
      Left = 388
    end
  end
  inherited PanelSuperior: TPanel
    Width = 471
    TabOrder = 0
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 471
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 145
    end
  end
  object CM_FECHA: TZetaDBFecha [12]
    Left = 130
    Top = 121
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '17/Dic/97'
    Valor = 35781.000000000000000000
    DataField = 'CM_FECHA'
    DataSource = DataSource
  end
  object CT_CODIGO: TZetaDBKeyLookup [13]
    Left = 130
    Top = 98
    Width = 333
    Height = 21
    LookupDataset = dmCajaAhorro.cdsCtasBancarias
    Opcional = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 80
    DataField = 'CT_CODIGO'
    DataSource = DataSource
  end
  object CM_DEP_RET: TDBRadioGroup [14]
    Left = 130
    Top = 55
    Width = 185
    Height = 41
    Caption = ' Movimiento '
    Columns = 2
    DataField = 'CM_DEP_RET'
    DataSource = DataSource
    Items.Strings = (
      'Dep'#243'sito'
      'Retiro')
    TabOrder = 2
    Values.Strings = (
      'D'
      'R')
    OnClick = CM_DEP_RETClick
  end
  object CM_TIPO: TZetaDBKeyLookup [15]
    Left = 130
    Top = 169
    Width = 333
    Height = 21
    LookupDataset = dmCajaAhorro.cdsCtasBancarias
    Opcional = False
    TabOrder = 6
    TabStop = True
    WidthLlave = 80
    DataField = 'CM_TIPO'
    DataSource = DataSource
  end
  object CM_MONTO: TZetaDBNumero [16]
    Left = 130
    Top = 145
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 5
    Text = '0.00'
    DataField = 'CM_MONTO'
    DataSource = DataSource
  end
  object CM_CHEQUE: TZetaDBNumero [17]
    Left = 130
    Top = 192
    Width = 115
    Height = 21
    Enabled = False
    Mascara = mnDias
    TabOrder = 7
    Text = '0'
    DataField = 'CM_CHEQUE'
    DataSource = DataSource
  end
  object CM_DESCRIP: TDBEdit [18]
    Left = 130
    Top = 240
    Width = 331
    Height = 21
    DataField = 'CM_DESCRIP'
    DataSource = DataSource
    TabOrder = 9
  end
  object CM_BENEFI: TDBEdit [19]
    Left = 130
    Top = 216
    Width = 331
    Height = 21
    DataField = 'CM_BENEFI'
    DataSource = DataSource
    TabOrder = 8
  end
  object CM_STATUS: TZetaDBKeyCombo [20]
    Left = 130
    Top = 264
    Width = 115
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 10
    ListaFija = lfStatusAhorro
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'CM_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 8
    Top = 97
  end
end
