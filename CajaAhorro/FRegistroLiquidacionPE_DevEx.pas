unit FRegistroLiquidacionPE_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion_DevEx, StdCtrls, DBCtrls, Mask, ZetaFecha, ZetaEdit,
  ZetaKeyLookup, DB, ExtCtrls, ZetaSmartLists, Buttons, ZetaNumero,
  ZetaCommonLists,
  ZetaDBTextBox, ZBaseEdicion, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, cxContainer, cxEdit, cxGroupBox, dxSkinsCore,
  dxSkinsdxBarPainter;

type
  TRegistroLiquidacionPE_DevEx = class(TBaseEdicion_DevEx)
    EmpleadoLbl: TLabel;
    ZEmpleado: TZetaKeyLookup_DevEx;
    Label11: TLabel;
    Label12: TLabel;
    Label8: TLabel;
    CM_FECHA: TZetaDBFecha;
    gbCondiciones: TGroupBox;
    SpeedButton1: TSpeedButton;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label25: TLabel;
    gbTipoRetiro: TGroupBox;
    lbMonto: TLabel;
    CM_MONTO: TZetaDBNumero;
    DarBaja: TCheckBox;
    RetiroLiquidacion: TRadioButton;
    RetiroParcial: TRadioButton;
    dsCuentas: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    CM_TOT_AHO: TZetaDBTextBox;
    CM_INTERES: TZetaDBTextBox;
    CM_AFAVOR: TZetaDBTextBox;
    CM_SAL_PRE: TZetaDBTextBox;
    CM_DISPONIBLE: TZetaDBTextBox;
    Disponible_Pos: TZetaTextBox;
    CM_TOT_AHO_EMP: TZetaDBTextBox;
    CM_INTERES_EMP: TZetaDBTextBox;
    CM_AFAVOR_EMP: TZetaDBTextBox;
    CM_DISPONIBLE_EMP: TZetaDBTextBox;
    lbEmpleado: TLabel;
    lbEmpresa: TLabel;
    CM_TOTAL_DISPONIBLE: TZetaDBTextBox;
    lbSaldo: TLabel;
    gbCajaAhorro: TGroupBox;
    Label4: TLabel;
    SALDO: TZetaDBTextBox;
    Label5: TLabel;
    SaldoPrestamosCajaCapital: TZetaDBTextBox;
    Label6: TLabel;
    SubTotalCajaAhorro: TZetaDBTextBox;
    Label9: TLabel;
    SaldoPrestamosCajaInteres: TZetaDBTextBox;
    Label10: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    Label22: TLabel;
    InteresesCA: TZetaDBTextBox;
    Label23: TLabel;
    ComisionesAcumCA: TZetaDBTextBox;
    Label24: TLabel;
    Label27: TLabel;
    gbFondoAhorro: TcxGroupBox;
    Label26: TLabel;
    Label28: TLabel;
    AH_SALDO: TZetaDBTextBox;
    PR_SALDO: TZetaDBTextBox;
    Label29: TLabel;
    SubTotalFondoAhorro: TZetaDBTextBox;
    Label31: TLabel;
    SaldoPrestamosFondoInteres: TZetaDBTextBox;
    Label32: TLabel;
    TotalFondoAhorro: TZetaDBTextBox;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    InteresesFA: TZetaDBTextBox;
    Label38: TLabel;
    ComisionesAcumFA: TZetaDBTextBox;
    Label39: TLabel;
    Label40: TLabel;
    gbPagar: TcxGroupBox;
    PE_YEAR: TZetaNumero;
    PE_NUMERO: TZetaKeyLookup_DevEx;
    lImprimeFormato: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure BtnSiguienteClick(Sender: TObject);
    procedure CT_CODIGOValidKey(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure RetiroParcialClick(Sender: TObject);
    procedure RetiroLiquidacionClick(Sender: TObject);
    procedure CM_MONTOExit(Sender: TObject);
    procedure ZEmpleadoValidKey(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FTipoAhorro: string;
    procedure SetControlsTipoRetiro(const lParcial: Boolean);
    procedure SetControlsTotalesAhorro;
    procedure SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
    procedure ImprimirReporte( const stipo: string );
  protected
    { Protected declarations }
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  RegistroLiquidacionPE_DevEx: TRegistroLiquidacionPE_DevEx;

implementation

uses dCajaAhorro,
     dCliente,
     dSistema,
     ZetaTipoEntidad,
     ZetaDialogo,
     ZImprimeForma,
     ZConstruyeFormula,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet,
     DBasicoCliente, DReportes;

{$R *.dfm}

procedure TRegistroLiquidacionPE_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := ZEmpleado;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0; // PENDIENTE
     HelpContext := H_LIQUID_RET_PARCIAL;
     ZEmpleado.LookupDataSet := dmCliente.cdsEmpleadoLookUp;
     PE_NUMERO.LookupDataSet := dmCajaAhorro.cdsPeriodo;
end;

procedure TRegistroLiquidacionPE_DevEx.Connect;
const
     K_FILTRO_CTAS = '( AH_TIPO = %s and CT_STATUS = 0 )';  //S�lo cuentas activas
begin
     {
     with CT_CODIGO do
     begin
          LookupDataset := dmCajaAhorro.cdsCtasBancarias;
          Filtro := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
     end;
     }

     with dmCliente.cdsEmpleadoLookUp do
     begin
          ZEmpleado.ResetMemory;
          Conectar;
          //ZEmpleado.SetLlaveDescripcion( IntToStr( FieldByName('CB_CODIGO').AsInteger ) , FieldByName('PRETTYNAME').AsString);
     end;
     with dmCajaAhorro do
     begin
          cdsPeriodo.Conectar;
          RefrescarLiquidacion( dmCliente.Empleado );
          with cdsLiquidacionAhorro do
          begin
               if IsEmpty then
               begin
                    EmptyDataSet;
                    ZEmpleado.SetLlaveDescripcion( VACIO , VACIO );
               end
               else
                   ZEmpleado.Valor := dmCliente.Empleado;
          end;
          ConectarPeriodo( dmCliente.GetDatosPeriodoActivo.Year, eTipoPeriodo( dmCliente.cdsEmpleado.FieldByName( 'CB_NOMINA' ).AsInteger ) );
          with dmCajaAhorro.cdsPeriodo do
          begin
               First;
               PE_YEAR.Valor := FieldByName( 'PE_YEAR' ).AsInteger;
               PE_NUMERO.Valor := FieldByName( 'PE_NUMERO' ).AsInteger;
          end;
          {
          with dmCliente.GetDatosPeriodoActivo do
          begin
               PE_YEAR.Valor := Year;
               PE_NUMERO.Valor := Numero;
               SetFiltroPeriodo( Year, eTipoPeriodo( dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_NOMINA' ).AsInteger ) );
          end;
          }
          cdsTPresta.Conectar;
          cdsCtasBancarias.Conectar;
          Datasource.Dataset := cdsLiquidacionAhorro;    // Ya debe estar agregado el registro al llegar a la forma
          dsCuentas.Dataset := cdsCtasMovsLiquida;
     end;
     RetiroLiquidacion.Checked := True;
     SetControlsTipoRetiro( RetiroParcial.Checked );
     SetControlsTotalesAhorro;
     CM_MONTOExit(Self);
     lImprimeFormato.Checked := True;
end;


procedure TRegistroLiquidacionPE_DevEx.EscribirCambios;
var
   rMonto, rMonto2, rValida: currency;
   sTipoAhorro, sMensaje: string;
   lInscrito: boolean;
   iStatusCaja, iStatusFondo, iTipo: integer;
begin
     iStatusCaja :=  0;
     iStatusFondo:=0;
     dmCajaAhorro.cdsTAhorro.Conectar;
     { Tipo de Ahorro Activo }
     dmCajaAhorro.cdsTAhorro.Locate( 'TB_CODIGO', dmCliente.Tipoahorro, [] );
     { Eres Caja de Ahorro }
     iTipo := dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_NUMERO' ).AsInteger;
     if ( iTipo = 1 ) then
     begin
          rValida := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'TotalCajaAhorro' ).AsFloat;
          rMonto := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'SubTotalCajaAhorro' ).AsFloat;
          rMonto2 := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'SubTotalFondoAhorro' ).AsFloat;
          sTipoAhorro := dmCliente.Tipoahorro;
          iStatusCaja := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'AH_STATUS_CAJA' ).AsInteger;
     end
     else
     begin
          rValida := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'TotalFondoAhorro' ).AsFloat;
          rMonto := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'SubTotalFondoAhorro' ).AsFloat;
          rMonto2 := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'SubTotalCajaAhorro' ).AsFloat;
          dmCajaAhorro.cdsTAhorro.Locate( 'TB_NUMERO', 1, [] );
          sTipoAhorro := dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_CODIGO' ).AsString; //dmCliente.Tipoahorro;
          dmCajaAhorro.cdsTAhorro.Locate( 'TB_CODIGO', dmCliente.Tipoahorro, [] );
          iStatusFondo := dmCajaAhorro.cdsLiquidacionAhorro.FieldByName( 'AH_STATUS_FONDO' ).AsInteger;
     end;
     FTipoAhorro := dmCliente.Tipoahorro;// sTipoAhorro;
     if ( iTipo = 1 ) then
     begin
          if ( iStatusCaja = -1 ) then
          begin
               lInscrito := False;
               sMensaje := Format( 'Empleado #%d no inscrito, No se le puede realizar la liquidaci�n', [ zEmpleado.Valor ] );
          end
          else
          begin
               if ( iStatusCaja = 1 ) then
               begin
                    lInscrito := False;
                    sMensaje := Format( 'Empleado #%d Caja de Ahorro liquidada con anterioridad', [ zEmpleado.Valor ] );
               end
               else
               begin
                    lInscrito := TRUE;
                    sMensaje := VACIO;
               end;
          end;
     end
     else
     begin
          if ( iStatusFondo = -1 ) then
          begin
               lInscrito := False;
               sMensaje := Format( 'Empleado #%d no inscrito, No se le puede realizar la liquidaci�n', [ zEmpleado.Valor ] );
          end
          else
          begin
               if ( iStatusFondo = 1 ) then
               begin
                    lInscrito := False;
                    sMensaje := Format( 'Empleado #%d Fondo de Ahorro liquidado con anterioridad', [ zEmpleado.Valor ] );
               end
               else
               begin
                    lInscrito := TRUE;
                    sMensaje := VACIO;
               end;
          end;
     end;

     if lInscrito then
     begin
          //if ( rValida > 0 ) then
          if ( rMonto >= 0 ) then
          begin
               if ( dmCajaAhorro.cdsPeriodo.FieldByName( 'PE_STATUS' ).Asinteger <> 6 ) then
               begin
                    with dmCajaAhorro do
                    begin
                         with cdsCtasMovsLiquida do
                         begin
                              if ( State = dsBrowse ) then
                                 Edit;
                              FieldByName( 'CM_TIPO' ).AsString := 'LIQUID';
                         end;
                         RecalculaMontosAhorro;
                         RegistrarLiquidacion;
                    end;
                    {$ifdef ANTES}
                    with dmCajaAhorro do
                    begin
                         PeriodoEmpleado := PE_NUMERO.Valor;
                         TipoNominaEmpleado := dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_NOMINA' ).AsInteger;
                         YearEmpleado := dmCliente.GetDatosPeriodoActivo.Year;
                    end;
                    with dmCajaAhorro.cdsExcepMontos do
                    begin
                         Refrescar;
                         Append;
                         FieldByName( 'CB_CODIGO' ).AsInteger := zEmpleado.Valor;
                         dmCajaAhorro.cdsTAhorro.Conectar;
                         dmCajaAhorro.cdsTAhorro.Locate( 'TB_CODIGO', dmCliente.Tipoahorro, [] );
                         FieldByName( 'CO_NUMERO' ).AsInteger := dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_PAGO' ).AsInteger;
                         FieldByName( 'MO_PERCEPC' ).AsFloat := dmCajaAhorro.TotalLiquidacion;
                         FieldByName( 'MO_REFEREN' ).AsString := dmCliente.Tipoahorro;
                         Post;
                         Enviar;
                    end;
                    if ( rMonto2 < 0 ) then
                    begin
                         with dmCajaAhorro do
                         begin
                              with cdsCtasMovsLiquida do
                              begin
                                   if ( State = dsBrowse ) then
                                      Edit;
                                   FieldByName( 'CM_TIPO' ).AsString := 'RETPAR';
                              end;
                              TipoAhorroLiquida := sTipoAhorro;
                              RecalculaMontosAhorroLiquidacion;
                              RegistrarLiquidacion( 1 );
                         end;
                         with dmCajaAhorro do
                         begin
                              PeriodoEmpleado := PE_NUMERO.Valor;
                              TipoNominaEmpleado := dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_NOMINA' ).AsInteger;
                              YearEmpleado := dmCliente.GetDatosPeriodoActivo.Year;
                         end;
                         with dmCajaAhorro.cdsExcepMontos do
                         begin
                              Refrescar;
                              Append;
                              FieldByName( 'CB_CODIGO' ).AsInteger := zEmpleado.Valor;
                              dmCajaAhorro.cdsTAhorro.Conectar;
                              dmCajaAhorro.cdsTAhorro.Locate( 'TB_CODIGO', sTipoAhorro, [] );
                              FieldByName( 'CO_NUMERO' ).AsInteger := dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_PAGO' ).AsInteger;
                              FieldByName( 'MO_PERCEPC' ).AsFloat := dmCajaAhorro.TotalLiquidacion;
                              FieldByName( 'MO_REFEREN' ).AsString := sTipoAhorro;
                              Post;
                              Enviar;
                         end;
                    end;
                    {$else}
                    if ( rMonto2 < 0 ) then
                    begin
                         with dmCajaAhorro do
                         begin
                              with cdsCtasMovsLiquida do
                              begin
                                   if ( State = dsBrowse ) then
                                      Edit;
                                   FieldByName( 'CM_TIPO' ).AsString := 'RETPAR';
                              end;
                              TipoAhorroLiquida := sTipoAhorro;
                              RecalculaMontosAhorroLiquidacion;
                              RegistrarLiquidacion( 1 );
                         end;
                    end;
                    with dmCajaAhorro do
                    begin
                         PeriodoEmpleado := PE_NUMERO.Valor;
                         TipoNominaEmpleado := dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_NOMINA' ).AsInteger;
                         YearEmpleado := dmCliente.GetDatosPeriodoActivo.Year;
                    end;
                    with dmCajaAhorro.cdsExcepMontos do
                    begin
                         Refrescar;
                         Append;
                         FieldByName( 'CB_CODIGO' ).AsInteger := zEmpleado.Valor;
                         dmCajaAhorro.cdsTAhorro.Conectar;
                         dmCajaAhorro.cdsTAhorro.Locate( 'TB_CODIGO', dmCliente.Tipoahorro, [] );
                         FieldByName( 'CO_NUMERO' ).AsInteger := dmCajaAhorro.cdsTAhorro.FieldByName( 'TB_PAGO' ).AsInteger;
                         if ( rMonto2 < 0 ) then
                         begin
                              rMonto2 := rMonto2 * ( -1 );
                              FieldByName( 'MO_PERCEPC' ).AsFloat := dmCajaAhorro.TotalLiquidacion - rMonto2;
                         end
                         else
                         begin
                              FieldByName( 'MO_PERCEPC' ).AsFloat := dmCajaAhorro.TotalLiquidacion;
                         end;
                         FieldByName( 'MO_REFEREN' ).AsString := dmCliente.Tipoahorro;
                         Post;
                         Enviar;
                    end;
                    {$endif}
                    ImprimirReporte( FTipoAhorro ) ;
                    if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
                       ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then        //y no se tienen cambios pendientes
                    begin
                         Close;
                    end;
               end
               else
                   ZetaDialogo.Zerror( 'Liquidaci�n', 'No se puede registrar pago en un n�mina afectada', 0 );
          end
          else
              ZetaDialogo.Zerror( 'Liquidaci�n', 'No se puede liquidar empleado con saldo menor a cero', 0 );
     end
     else
     begin
          ZetaDialogo.ZInformation( Caption, sMensaje,0 );
          zEmpleado.SetFocus;
     end;
end;

procedure TRegistroLiquidacionPE_DevEx.ImprimirReporte( const stipo: string );
begin
     if lImprimeFormato.Checked then
     begin
          dmSistema.cdsusuarios.Conectar;
          with dmCajaAhorro.cdsTAhorro do
          begin
               Conectar;
               if Locate( 'TB_CODIGO', VarArrayOf( [ stipo ] ), [] ) then
               begin
                    if FieldByName( 'TB_REP_LIQ' ).AsInteger <> 0 then
                       dmReportes.ImprimeUnaForma( VACIO, FieldByName( 'TB_REP_LIQ' ).AsInteger );
               end;
          end;
     end;
end;


procedure TRegistroLiquidacionPE_DevEx.Agregar;
begin
     { No hace nada }
end;

procedure TRegistroLiquidacionPE_DevEx.Borrar;
begin
     { No hace nada }
end;

{ Eventos de controles }

procedure TRegistroLiquidacionPE_DevEx.BtnSiguienteClick(Sender: TObject);
begin
     inherited;
     {if strLleno( CT_CODIGO.Llave ) then
        CM_CHEQUE.Valor := dmCajaAhorro.SiguienteCheque( CT_CODIGO.Llave );}
end;

procedure TRegistroLiquidacionPE_DevEx.CT_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     {
     if strLleno( CT_CODIGO.Llave ) then
     begin
          with dmCajaAhorro.cdsCtasBancarias do
          begin
               Locate( 'CT_CODIGO', CT_CODIGO.Llave, [] );
               self.ImprimeCheque.Checked := ( FieldByName( 'CT_REP_CHK' ).AsInteger > 0 );
               self.ImprimeLiquidacion.Checked := ( FieldByName( 'CT_REP_LIQ' ).AsInteger > 0 );
          end;
     end;
     }
end;

procedure TRegistroLiquidacionPE_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     {
     if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
     }
end;

procedure TRegistroLiquidacionPE_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enPrestamo, dmCajaAhorro.cdsRegPrestamos );
end;


procedure TRegistroLiquidacionPE_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     //inherited;

end;

procedure TRegistroLiquidacionPE_DevEx.RetiroParcialClick(Sender: TObject);
begin
     inherited;
     RetiroLiquidacion.Checked := NOT RetiroParcial.Checked;
     SetControlsTipoRetiro( RetiroParcial.Checked );
end;

procedure TRegistroLiquidacionPE_DevEx.RetiroLiquidacionClick(Sender: TObject);
begin
     inherited;
     RetiroParcial.Checked := NOT RetiroLiquidacion.Checked;
     SetControlsTipoRetiro( RetiroParcial.Checked );
end;

procedure TRegistroLiquidacionPE_DevEx.SetControlsTipoRetiro( const lParcial: Boolean );
begin
     DarBaja.Checked := ( not lParcial );
     DarBaja.Enabled := lParcial;
     lbMonto.Enabled := lParcial;
     CM_MONTO.Valor := dmCajaAhorro.cdsCtasMovsLiquida.FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat;
     CM_MONTOExit(Self);
     CM_MONTO.Enabled := lParcial;
end;

procedure TRegistroLiquidacionPE_DevEx.SetControlsTotalesAhorro;
 var
    lVisible: Boolean;
begin
     lVisible := ( dmCliente.GetDatosAhorroActivo.Relativo <> 0 );
     gbFondoAhorro.Visible := lVisible;
     gbCajaAhorro.Visible := not gbfondoAhorro.Visible;
end;

function TRegistroLiquidacionPE_DevEx.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;



procedure TRegistroLiquidacionPE_DevEx.CM_MONTOExit(Sender: TObject);
begin
     inherited;
     Disponible_POS.Caption := FormatFloat('#,0.00;-#,0.00',dmCajaAhorro.cdsCtasMovsLiquida.FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat - CM_MONTO.Valor );
end;

procedure TRegistroLiquidacionPE_DevEx.ZEmpleadoValidKey(Sender: TObject);
const
     K_FILTRO_CTAS = '( AH_TIPO = %s and CT_STATUS = 0 )';//S�lo cuentas activas
var
   oCursor: TCursor;
begin
   //  inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
            // if not ( ( Sender is TBitBtn ) and ( TBitBtn(Sender).Name = 'Cancelar' ) ) then
             if Activecontrol.Name <> 'Cancelar' then
             begin
                  if ZEmpleado.Valor > 0 then
                  begin
                       with dmCajaAhorro do
                       begin
                            RefrescarLiquidacion(ZEmpleado.Valor);
                            if ( cdsLiquidacionAhorro.IsEmpty ) then
                            begin
                                 zError( Self.Caption,Format( 'El empleado %d no es socio de %s' , [ZEmpleado.Valor,GetDatosAhorroActivo.Descripcion] ),0 );
                                 ZEmpleado.SetFocus;
                            end
                            else


                              if ( eStatusAhorro( cdsLiquidacionAhorro.FieldByName('AH_STATUS').AsInteger ) = saCancelado ) then
                            begin
                                 zError( Self.Caption , Format( 'El empleado %d ya fue dado de baja de %s' , [ZEmpleado.Valor,GetDatosAhorroActivo.Descripcion] ),0 );
                                 ZEmpleado.SetFocus;
                            end
                            else


                            begin
                                 cdsLiquidacionAhorro.Edit;
                                 cdsLiquidacionAhorro.FieldByName( 'CB_CODIGO' ).AsInteger := zEmpleado.Valor;
                                 with cdsCtasMovsLiquida do
                                 begin
                                      Edit;
                                      FieldByName( 'CM_FECHA' ).AsDateTime := FechaDefault;

                                      cdsCtasBancarias.Filter := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
                                      cdsCtasBancarias.Filtered := TRUE;
                                      try
                                         FieldByName( 'CT_CODIGO' ).AsString := cdsCtasBancarias.FieldByName( 'CT_CODIGO' ).AsString;
                                      finally
                                             cdsCtasBancarias.Filtered := FALSE;
                                      end;
                                 end;
                                 //cdsLiquidacionAhorro.FieldByName('TASA').AsFloat := cdsTAhorro.FieldByName('TB_TASA1').AsFloat;
                                self.ActiveControl := CM_FECHA;
                            end;
                       end;
                  end
                  else
                  begin
                       ZetaDialogo.zError( 'Registrar liquidaci�n', '! Empleado no encontrado !', 0 );
                       ZEmpleado.SetFocus;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TRegistroLiquidacionPE_DevEx.SetFiltroPeriodo(const iYear: Integer; const TipoPeriodo: eTipoPeriodo);
begin
     with dmCajaAhorro do
     begin
          if ( not cdsPeriodo.IsEmpty ) then
          begin
               if ( ( iYear <> cdsPeriodo.FieldByName( 'PE_YEAR' ).AsInteger ) or
                    ( Ord( TipoPeriodo ) <> cdsPeriodo.FieldByName( 'PE_TIPO' ).AsInteger ) ) then
                  ConectarPeriodo( iYear, TipoPeriodo );
          end
          else
              ConectarPeriodo( iYear, TipoPeriodo );

          with PE_NUMERO do
          begin
               SetLlaveDescripcion( VACIO, VACIO );
               if ( dmCliente.GetDatosPeriodoActivo.Tipo = TipoPeriodo ) then
                  Valor := dmCliente.GetDatosPeriodoActivo.Numero
               else
                   Valor := 1;
          end;
     end;
end;


procedure TRegistroLiquidacionPE_DevEx.Cancelar_DevExClick(
  Sender: TObject);
begin
  Close;
  //inherited;

end;

end.
