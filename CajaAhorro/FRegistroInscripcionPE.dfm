inherited RegistroInscripcionPE: TRegistroInscripcionPE
  Left = 555
  Top = 302
  Caption = 'Inscribir en ahorro'
  ClientHeight = 201
  ClientWidth = 436
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 34
    Top = 60
    Width = 50
    Height = 13
    Caption = '&Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label1: TLabel [1]
    Left = 9
    Top = 85
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de inicio:'
  end
  object Label18: TLabel [2]
    Left = 425
    Top = 84
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo inicial:'
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 165
    Width = 436
    TabOrder = 5
    inherited OK: TBitBtn
      Left = 268
    end
    inherited Cancelar: TBitBtn
      Left = 353
    end
  end
  inherited PanelSuperior: TPanel
    Width = 436
    TabOrder = 0
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 436
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 110
      inherited textoValorActivo2: TLabel
        Width = 104
      end
    end
  end
  object CB_CODIGO: TZetaDBKeyLookup [6]
    Left = 86
    Top = 56
    Width = 340
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object AH_FECHA: TZetaDBFecha [7]
    Left = 86
    Top = 80
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '17/dic/97'
    Valor = 35781.000000000000000000
    DataField = 'AH_FECHA'
    DataSource = DataSource
  end
  object AH_SALDO_I: TZetaDBNumero [8]
    Left = 486
    Top = 80
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 6
    Text = '0.00'
    Visible = False
    DataField = 'AH_SALDO_I'
    DataSource = DataSource
  end
  object gbFormula: TGroupBox [9]
    Left = 0
    Top = 108
    Width = 436
    Height = 57
    Caption = ' F'#243'rmula: '
    TabOrder = 4
    object lbFormula: TLabel
      Left = 42
      Top = 28
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object AH_FORMULA: TDBEdit
      Left = 86
      Top = 24
      Width = 320
      Height = 21
      DataField = 'AH_FORMULA'
      DataSource = DataSource
      TabOrder = 2
    end
    object cbPorcentaje: TCheckBox
      Left = 54
      Top = -1
      Width = 74
      Height = 17
      Caption = 'Porcentaje'
      TabOrder = 0
      OnClick = cbPorcentajeClick
    end
    object nPorcentaje: TZetaNumero
      Left = 86
      Top = 24
      Width = 121
      Height = 21
      Mascara = mnTasa
      TabOrder = 1
      Text = '0.0 %'
      Visible = False
    end
    object cmbPorcentaje: TZetaKeyCombo
      Left = 86
      Top = 24
      Width = 62
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
      Items.Strings = (
        '3'
        '5'
        '10'
        '15')
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  object lImprimeFormato: TCheckBox [10]
    Left = 320
    Top = 83
    Width = 106
    Height = 17
    TabStop = False
    Caption = #191'Imprimir formato?'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 169
  end
end
