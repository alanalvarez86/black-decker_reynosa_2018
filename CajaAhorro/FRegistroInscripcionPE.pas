unit FRegistroInscripcionPE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, DB, ExtCtrls, DBCtrls, ZetaSmartLists, Buttons,
  StdCtrls, ZetaNumero, Mask, ZetaFecha, ZetaKeyLookup, ZetaKeyCombo;

type
  TRegistroInscripcionPE = class(TBaseEdicion)
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaDBKeyLookup;
    AH_FECHA: TZetaDBFecha;
    Label1: TLabel;
    Label18: TLabel;
    AH_SALDO_I: TZetaDBNumero;
    gbFormula: TGroupBox;
    AH_FORMULA: TDBEdit;
    cbPorcentaje: TCheckBox;
    nPorcentaje: TZetaNumero;
    lbFormula: TLabel;
    cmbPorcentaje: TZetaKeyCombo;
    lImprimeFormato: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure cbPorcentajeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    FCiclo: Boolean;
    procedure AplicarFiltro;
    procedure LlenaComboPorcentaje;
    procedure ImprimirReporte;
    { Private declarations }
  protected
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Connect;override;
    procedure EscribirCambios;override;
  public
    { Public declarations }
    property Ciclo: Boolean read FCiclo write FCiclo;
  end;

var
  RegistroInscripcionPE: TRegistroInscripcionPE;

implementation
uses
    DCajaAhorro,
    DCliente,
    DGlobal,
    DReportes,
    Dsistema,
    ZetaDialogo,
    ZGlobalTress,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists
    ;

{$R *.dfm}

procedure TRegistroInscripcionPE.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0;
     HelpContext := H_INSCRIPCION;
     FirstControl := CB_CODIGO;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

procedure TRegistroInscripcionPE.FormShow(Sender: TObject);
begin
     inherited;
     nPorcentaje.Valor := 0;
     cbPOrcentaje.Checked := TRUE;
     cbPorcentajeClick( NIL );
     LlenaComboPorcentaje;
     if ( cmbPorcentaje.Visible ) then
        cmbPorcentaje.ItemIndex := 0;
     lImprimeFormato.Checked := True;
     { Si es Fondo de Ahorro }
     if ( dmCliente.GetDatosAhorroActivo.Numero = 2 ) then
     begin
          gbFormula.Visible := False;
          cbPorcentaje.Visible := gbFormula.Visible;
          Self.Height := 171;
     end
     else
     begin
          gbFormula.Visible := True;
          cbPorcentaje.Visible := gbFormula.Visible;
          Self.Height := 228;
     end;
end;

procedure TRegistroInscripcionPE.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmCajaAhorro do
     begin
          FiltroCatalogo := Format( 'CB_CODIGO = %d', [ dmCliente.Empleado ] );
          Datasource.Dataset := cdsInscripcion;    
          with cdsInscripcion do
          begin
               Refrescar;
               Append;
               FieldByName('CB_CODIGO').AsInteger := dmCliente.Empleado;
               //AH_SALDO_I.SetFocus;
          end;
          FiltroCatalogo := VACIO;
     end;
end;

//Se puede ciclar el registro de inscripci�n dependiendo de la variable FCiclo.
procedure TRegistroInscripcionPE.LlenaComboPorcentaje;
begin
    with cmbPorcentaje.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( '3=3' );
             Add( '5=5' );
             Add( '10=10' );
             Add( '15=15' );
          finally
                 EndUpdate;
          end;
     end;
end;

{ Se puede ciclar el registro de inscripci�n dependiendo de la variable FCiclo }
procedure TRegistroInscripcionPE.EscribirCambios;
 var
    lContinua : Boolean;
begin
     lContinua := TRUE;
     with DataSource.DataSet do
     begin
          { Numero de empleado no puede quedar vacio }
          if StrVacio( CB_CODIGO.Llave ) then
          begin
               CB_CODIGO.SetFocus;
               ZetaDialogo.ZError(Caption, 'El c�digo del empleado no puede quedar vac�o',0);
               lContinua := FALSE;
          end
          else
          //if dmCajaAhorro.cdsAhorros.Locate('CB_CODIGO',CB_CODIGO.Valor,[])then
          begin
               { Posicionas el numero de empleado seleccionado como Activo de la aplicaci�n }
               dmcliente.SetEmpleadoNumero( CB_CODIGO.Valor );
               { Se filtra el Client DataSet solo para el empleado al que se quiere inscribir }
               with dmCajaAhorro do
               begin
                    FiltroCatalogo := Format( 'CB_CODIGO = %d', [ dmCliente.Empleado ] );
                    cdsHisAhorros.Refrescar;
                    FiltroCatalogo := VACIO;
               end;
               { Se Busca si el empleado ya esta inscrito con anterioridad }
               if dmCajaAhorro.cdsHisAhorros.Locate( 'CB_CODIGO', CB_CODIGO.Valor, [] )then
               begin
                    ZetaDialogo.ZInformation( Caption, Format( 'El empleado: %d ya est� inscrito',[ CB_CODIGO.Valor ] ),0 );
                    CB_CODIGO.SetFocus;
                    lContinua := FALSE;
               end
               else
               begin
                    //if dmCliente.cdsEmpleadoLookUp.FieldByName( 'CB_ACTIVO' ).AsString = K_GLOBAL_NO then
                    if not dmCliente.GetDatosEmpleadoActivo.Activo then
                    begin
                         ZetaDialogo.ZInformation( Caption, Format( 'El empleado: %d dado de baja, no se puede inscribir',[ CB_CODIGO.Valor ] ),0 );
                         CB_CODIGO.SetFocus;
                         lContinua := FALSE;
                    end
                    else
                    begin
                         { Si es Caja de Ahorro }
                         if ( dmCliente.GetDatosAhorroActivo.Numero = 1 ) then
                         begin
                              { Si es por porcentaje }
                              if cbPorcentaje.Checked then
                              begin
                                   { Editar el ClientDataSet }
                                   if ( State = dsBrowse ) then
                                      Edit;
                                   if strLleno( Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL1 ) ) then
                                      FieldByName( 'AH_FORMULA' ).AsString := Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL1 ) + StrToZero( StrToInt( cmbPorcentaje.Items.Strings[ cmbPorcentaje.ItemIndex ] ) / 100, 3, 2 )
                                   else
                                       FieldByName( 'AH_FORMULA' ).AsString := 'SALARIO*' + StrToZero( StrToInt( cmbPorcentaje.Items.Strings[ cmbPorcentaje.ItemIndex ] ) / 100, 3, 2 );
                              end
                              else
                              begin
                                   { Editar el ClientDataSet }
                                   if ( State = dsBrowse ) then
                                      Edit;
                                   if strLleno( Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL1 ) ) then
                                      FieldByName( 'AH_FORMULA' ).AsString := Global.GetGlobalString( K_GLOBAL_TEXT_GLOBAL1 ) + FieldByName( 'AH_FORMULA' ).AsString
                              end;
                         end
                         else{ Si es Fondo de Ahorro - Se pone la formula vacia para que toma la formula del concepto. }
                         begin
                              { Editar el ClientDataSet }
                              if ( State = dsBrowse ) then
                                 Edit;
                             FieldByName( 'AH_FORMULA' ).AsString := VACIO;
                         end;
                    end;
               end;
          end;
     end;
     FCiclo := False;
     if lContinua then
     begin
          inherited;
          ImprimirReporte;
          if FCiclo then
          begin
               with DataSource.DataSet do
               begin
                    if ( State = dsBrowse ) then
                    begin
                         AplicarFiltro;
                         Append;
                         CB_CODIGO.SetFocus;
                    end;
               end;
          end
          else
          begin
               Close;
          end;
     end;
end;

procedure TRegistroInscripcionPE.cbPorcentajeClick(Sender: TObject);
begin
     inherited;
     AH_FORMULA.Visible := not cbPOrcentaje.Checked;
     cmbPorcentaje.Visible := cbPOrcentaje.Checked;
     if cbPOrcentaje.Checked then
        lbFormula.Caption := 'Porcentaje:'
     else
         lbFormula.Caption := 'F�rmula:';
end;

function TRegistroInscripcionPE.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroInscripcionPE.CancelarClick(Sender: TObject);
begin
     inherited;
     Close;
end;

procedure TRegistroInscripcionPE.AplicarFiltro;
begin
     with dmCajaAhorro do
     begin
          LlenaFiltroEmpleados( cdsAhorros );
          CB_CODIGO.Filtro := FiltroEmpleados;
     end;
end;

procedure TRegistroInscripcionPE.ImprimirReporte;
begin
     if lImprimeFormato.Checked then
     begin
          dmSistema.cdsusuarios.Conectar;
          with dmCajaAhorro.cdsTAhorro do
          begin
               Conectar;
               if Locate( 'TB_CODIGO', VarArrayOf( [ dmCliente.GetDatosAhorroActivo.Codigo ] ), [] ) then
               begin
                    if FieldByName( 'TB_REP_INS' ).AsInteger <> 0 then
                       dmReportes.ImprimeUnaForma( VACIO, FieldByName( 'TB_REP_INS' ).AsInteger );
               end;
          end;
     end;
end;

end.
