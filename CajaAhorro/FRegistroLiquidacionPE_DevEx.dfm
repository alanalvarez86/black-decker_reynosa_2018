inherited RegistroLiquidacionPE_DevEx: TRegistroLiquidacionPE_DevEx
  Left = 702
  Top = 213
  Caption = 'Liquidaci'#243'n / retiro de ahorro'
  ClientHeight = 398
  ClientWidth = 478
  Color = clBtnHighlight
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 90
    Top = 60
    Width = 50
    Height = 13
    Caption = 'Empleado:'
    FocusControl = ZEmpleado
  end
  object Label8: TLabel [1]
    Left = 64
    Top = 85
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de retiro:'
  end
  object Label2: TLabel [2]
    Left = 675
    Top = 88
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo final disponible:'
  end
  object Label3: TLabel [3]
    Left = 900
    Top = 88
    Width = 134
    Height = 13
    Caption = '(Posterior al monto retirado)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Disponible_Pos: TZetaTextBox [4]
    Left = 782
    Top = 86
    Width = 115
    Height = 17
    AutoSize = False
    Caption = 'AH_FECHA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  inherited PanelBotones: TPanel
    Top = 362
    Width = 478
    TabOrder = 9
    inherited OK_DevEx: TcxButton
      Left = 313
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 392
      Cancel = True
      Caption = '&Salir'
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 478
    TabOrder = 7
    inherited ValorActivo2: TPanel
      Width = 152
      inherited textoValorActivo2: TLabel
        Width = 146
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 14
  end
  object ZEmpleado: TZetaKeyLookup_DevEx [8]
    Left = 142
    Top = 56
    Width = 333
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 80
    OnValidKey = ZEmpleadoValidKey
  end
  object CM_FECHA: TZetaDBFecha [9]
    Left = 142
    Top = 80
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '17/dic/97'
    Valor = 35781.000000000000000000
    DataField = 'CM_FECHA'
    DataSource = dsCuentas
  end
  object gbCondiciones: TGroupBox [10]
    Left = 584
    Top = 137
    Width = 401
    Height = 160
    Caption = ' Condiciones del ahorro  '
    TabOrder = 5
    object SpeedButton1: TSpeedButton
      Left = 424
      Top = 179
      Width = 25
      Height = 25
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
    object Label18: TLabel
      Left = 58
      Top = 29
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total ahorrado:'
    end
    object Label19: TLabel
      Left = 25
      Top = 49
      Width = 105
      Height = 13
      Alignment = taRightJustify
      Caption = '(+) Intereses ganados:'
    end
    object Label20: TLabel
      Left = 78
      Top = 69
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = '(=) A favor:'
    end
    object Label21: TLabel
      Left = 35
      Top = 109
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = '(=) Saldo disponible:'
    end
    object Label25: TLabel
      Left = 37
      Top = 89
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Caption = '(-) Saldo pr'#233'stamos:'
    end
    object CM_TOT_AHO: TZetaDBTextBox
      Left = 134
      Top = 27
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_TOT_AHO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_TOT_AHO'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_INTERES: TZetaDBTextBox
      Left = 134
      Top = 47
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_INTERES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_INTERES'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_AFAVOR: TZetaDBTextBox
      Left = 134
      Top = 67
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_AFAVOR'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_AFAVOR'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_SAL_PRE: TZetaDBTextBox
      Left = 134
      Top = 87
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_SAL_PRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_SAL_PRE'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_DISPONIBLE: TZetaDBTextBox
      Left = 134
      Top = 107
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_DISPONIBLE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_DISPONIBLE'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_TOT_AHO_EMP: TZetaDBTextBox
      Left = 262
      Top = 27
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_TOT_AHO_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_TOT_AHO_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_INTERES_EMP: TZetaDBTextBox
      Left = 262
      Top = 47
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_INTERES_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_INTERES_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_AFAVOR_EMP: TZetaDBTextBox
      Left = 262
      Top = 67
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_AFAVOR_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_AFAVOR_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_DISPONIBLE_EMP: TZetaDBTextBox
      Left = 262
      Top = 107
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_DISPONIBLE_EMP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_DISPONIBLE_EMP'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbEmpleado: TLabel
      Left = 168
      Top = 9
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado'
    end
    object lbEmpresa: TLabel
      Left = 299
      Top = 9
      Width = 41
      Height = 13
      Alignment = taCenter
      Caption = 'Empresa'
    end
    object CM_TOTAL_DISPONIBLE: TZetaDBTextBox
      Left = 262
      Top = 127
      Width = 115
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'CM_TOTAL_DISPONIBLE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_TOTAL_DISPONIBLE'
      DataSource = dsCuentas
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbSaldo: TLabel
      Left = 146
      Top = 129
      Width = 103
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo total disponible:'
    end
  end
  object gbTipoRetiro: TGroupBox [11]
    Left = 584
    Top = 301
    Width = 473
    Height = 73
    Caption = 
      ' Tipo de retiro:                                                ' +
      ' '
    TabOrder = 6
    object lbMonto: TLabel
      Left = 58
      Top = 47
      Width = 71
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto a retirar:'
    end
    object CM_MONTO: TZetaDBNumero
      Left = 134
      Top = 43
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      OnExit = CM_MONTOExit
      DataField = 'CM_MONTO'
      DataSource = dsCuentas
    end
    object DarBaja: TCheckBox
      Left = 35
      Top = 24
      Width = 112
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Dar baja de ahorro:'
      TabOrder = 2
    end
    object RetiroLiquidacion: TRadioButton
      Left = 148
      Top = -1
      Width = 80
      Height = 17
      Caption = 'Liquidaci'#243'n'
      Checked = True
      TabOrder = 1
      TabStop = True
      OnClick = RetiroLiquidacionClick
    end
    object RetiroParcial: TRadioButton
      Left = 84
      Top = -1
      Width = 56
      Height = 17
      Caption = 'Parcial'
      TabOrder = 0
      OnClick = RetiroParcialClick
    end
  end
  object gbCajaAhorro: TGroupBox [12]
    Left = 0
    Top = 109
    Width = 488
    Height = 175
    Caption = ' Caja de Ahorro '
    TabOrder = 2
    object Label4: TLabel
      Left = 104
      Top = 15
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ahorrado:'
    end
    object SALDO: TZetaDBTextBox
      Left = 154
      Top = 11
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SALDO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TotalCajaAhorro'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label5: TLabel
      Left = 18
      Top = 81
      Width = 132
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo de Pr'#233'stamos Capital:'
    end
    object SaldoPrestamosCajaCapital: TZetaDBTextBox
      Left = 154
      Top = 77
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SaldoPrestamosCajaCapital'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SaldoPrestamosCajaCapital'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label6: TLabel
      Left = 50
      Top = 125
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total Caja de Ahorro:'
    end
    object SubTotalCajaAhorro: TZetaDBTextBox
      Left = 154
      Top = 121
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SubTotalCajaAhorro'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SubTotalCajaAhorro'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label9: TLabel
      Left = 18
      Top = 103
      Width = 132
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo de Pr'#233'stamos Inter'#233's:'
    end
    object SaldoPrestamosCajaInteres: TZetaDBTextBox
      Left = 154
      Top = 99
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SaldoPrestamosCajaInteres'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SaldoPrestamosCajaInteres'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label10: TLabel
      Left = 270
      Top = 80
      Width = 17
      Height = 13
      Caption = '(- )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 270
      Top = 102
      Width = 17
      Height = 13
      Caption = '(- )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel
      Left = 270
      Top = 123
      Width = 16
      Height = 13
      Caption = '(=)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label22: TLabel
      Left = 58
      Top = 37
      Width = 92
      Height = 13
      Alignment = taRightJustify
      Caption = 'Intereses Ganados:'
    end
    object InteresesCA: TZetaDBTextBox
      Left = 154
      Top = 33
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'InteresesCA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TotalInteresesCA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label23: TLabel
      Left = 44
      Top = 59
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto por comisiones:'
    end
    object ComisionesAcumCA: TZetaDBTextBox
      Left = 154
      Top = 55
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ComisionesAcumCA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ComisionesAcumCA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label24: TLabel
      Left = 270
      Top = 59
      Width = 17
      Height = 13
      Caption = '(- )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label27: TLabel
      Left = 270
      Top = 37
      Width = 16
      Height = 13
      Caption = '(+)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object gbFondoAhorro: TcxGroupBox [13]
    Left = 0
    Top = 109
    Caption = ' Fondo de Ahorro '
    TabOrder = 3
    Visible = False
    Height = 178
    Width = 488
    object Label26: TLabel
      Left = 104
      Top = 15
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ahorrado:'
      Transparent = True
    end
    object Label28: TLabel
      Left = 20
      Top = 103
      Width = 130
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo de pr'#233'stamos capital:'
      Transparent = True
    end
    object AH_SALDO: TZetaDBTextBox
      Left = 154
      Top = 11
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'AH_SALDO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TotalFondoAhorro'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PR_SALDO: TZetaDBTextBox
      Left = 154
      Top = 99
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'PR_SALDO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SaldoPrestamosFondoCapital'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label29: TLabel
      Left = 45
      Top = 147
      Width = 105
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total fondo de ahorro:'
      Transparent = True
    end
    object SubTotalFondoAhorro: TZetaDBTextBox
      Left = 154
      Top = 143
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SubTotalFondoAhorro'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SubTotalFondoAhorro'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label31: TLabel
      Left = 20
      Top = 125
      Width = 130
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo de pr'#233'stamos inter'#233's:'
      Transparent = True
    end
    object SaldoPrestamosFondoInteres: TZetaDBTextBox
      Left = 154
      Top = 121
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SaldoPrestamosFondoInteres'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SaldoPrestamosFondoInteres'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label32: TLabel
      Left = 53
      Top = 81
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Aportaci'#243'n empresa:'
      Transparent = True
    end
    object TotalFondoAhorro: TZetaDBTextBox
      Left = 154
      Top = 77
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'TotalFondoAhorro'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TotalFondoAhorro'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label33: TLabel
      Left = 270
      Top = 81
      Width = 16
      Height = 13
      Caption = '(+)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label34: TLabel
      Left = 270
      Top = 102
      Width = 17
      Height = 13
      Caption = '(- )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label35: TLabel
      Left = 270
      Top = 124
      Width = 17
      Height = 13
      Caption = '(- )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label36: TLabel
      Left = 270
      Top = 145
      Width = 16
      Height = 13
      Caption = '(=)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label37: TLabel
      Left = 60
      Top = 37
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Intereses ganados:'
      Transparent = True
    end
    object InteresesFA: TZetaDBTextBox
      Left = 154
      Top = 33
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'InteresesFA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TotalInteresesFA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label38: TLabel
      Left = 44
      Top = 59
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto por comisiones:'
      Transparent = True
    end
    object ComisionesAcumFA: TZetaDBTextBox
      Left = 154
      Top = 55
      Width = 115
      Height = 21
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'ComisionesAcumFA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'ComisionesAcumFA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label39: TLabel
      Left = 270
      Top = 59
      Width = 17
      Height = 13
      Caption = '(- )'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label40: TLabel
      Left = 270
      Top = 37
      Width = 16
      Height = 13
      Caption = '(+)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object gbPagar: TcxGroupBox [14]
    Left = 0
    Top = 287
    Align = alBottom
    Caption = ' Dep'#243'sito del monto en n'#243'mina '
    TabOrder = 4
    Height = 75
    Width = 478
    object Label11: TLabel
      Left = 75
      Top = 43
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
    end
    object Label12: TLabel
      Left = 93
      Top = 20
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'A'#241'o:'
    end
    object PE_YEAR: TZetaNumero
      Left = 118
      Top = 16
      Width = 80
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
    end
    object PE_NUMERO: TZetaKeyLookup_DevEx
      Left = 118
      Top = 39
      Width = 333
      Height = 21
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 80
    end
  end
  object lImprimeFormato: TCheckBox [15]
    Left = 369
    Top = 83
    Width = 106
    Height = 17
    TabStop = False
    Caption = #191'Imprimir formato?'
    Checked = True
    State = cbChecked
    TabOrder = 10
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  object dsCuentas: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 44
    Top = 57
  end
end
