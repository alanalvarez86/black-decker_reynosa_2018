inherited WizDepositarRetenciones: TWizDepositarRetenciones
  Left = 414
  Top = 295
  Caption = 'Depositar retenciones'
  ClientHeight = 279
  ClientWidth = 448
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 243
    Width = 448
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Left = 7
      Enabled = True
    end
    inherited Siguiente: TZetaWizardButton
      Left = 86
    end
    inherited Salir: TZetaWizardButton
      Left = 361
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 278
      Enabled = True
    end
  end
  inherited PageControl: TPageControl
    Width = 448
    Height = 243
    inherited Parametros: TTabSheet
      object GroupBox3: TGroupBox
        Left = 8
        Top = 57
        Width = 425
        Height = 68
        Caption = ' Per'#237'odo de n'#243'mina '
        TabOrder = 0
        object Label5: TLabel
          Left = 212
          Top = 20
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo:'
          Visible = False
        end
        object Label6: TLabel
          Left = 41
          Top = 43
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label7: TLabel
          Left = 59
          Top = 20
          Width = 22
          Height = 13
          Alignment = taRightJustify
          Caption = 'A'#241'o:'
        end
        object PE_TIPO: TZetaKeyCombo
          Left = 239
          Top = 16
          Width = 105
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 2
          Visible = False
          OnChange = PE_YEARExit
          ListaFija = lfTipoPeriodo
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
        end
        object PE_YEAR: TZetaNumero
          Left = 83
          Top = 16
          Width = 60
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          OnExit = PE_YEARExit
        end
        object PE_NUMERO: TZetaKeyLookup
          Left = 83
          Top = 39
          Width = 300
          Height = 21
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
        end
      end
      object cbRetencionesPrestamos: TCheckBox
        Left = 16
        Top = 154
        Width = 146
        Height = 15
        Alignment = taLeftJustify
        Caption = 'Retenciones de pr'#233'stamos'
        TabOrder = 2
      end
      object cbRetencionesAhorro: TCheckBox
        Left = 34
        Top = 134
        Width = 128
        Height = 15
        Alignment = taLeftJustify
        Caption = 'Retenciones de ahorro'
        TabOrder = 1
      end
    end
    object Retenciones: TTabSheet [1]
      Caption = 'Retenciones'
      ImageIndex = 2
      TabVisible = False
      object Label1: TLabel
        Left = 120
        Top = 34
        Width = 139
        Height = 13
        AutoSize = False
        Caption = 'Retenciones de ahorro,       $'
      end
      object Num_ahorro: TZetaTextBox
        Left = 48
        Top = 32
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'Num_ahorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Suma_ahorro: TZetaTextBox
        Left = 261
        Top = 32
        Width = 130
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Suma_ahorro'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Num_Prestamos: TZetaTextBox
        Left = 48
        Top = 53
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'Num_Prestamos'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label2: TLabel
        Left = 120
        Top = 55
        Width = 138
        Height = 13
        Caption = 'Retenciones de pr'#233'stamos, $'
      end
      object Suma_prestamos: TZetaTextBox
        Left = 261
        Top = 53
        Width = 130
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Suma_prestamos'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label3: TLabel
        Left = 13
        Top = 108
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Monto a depositar:'
      end
      object Label4: TLabel
        Left = 2
        Top = 133
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Depositar en cuenta:'
      end
      object Label8: TLabel
        Left = 10
        Top = 159
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha de dep'#243'sito:'
      end
      object Label13: TLabel
        Left = 42
        Top = 183
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object CM_MONTO: TZetaTextBox
        Left = 104
        Top = 106
        Width = 121
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'CM_MONTO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object CT_CODIGO: TZetaKeyLookup
        Left = 104
        Top = 129
        Width = 333
        Height = 21
        LookupDataset = dmCajaAhorro.cdsCtasBancarias
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 80
      end
      object CM_FECHA: TZetaFecha
        Left = 104
        Top = 154
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 1
        Text = '27/feb/06'
        Valor = 38775.000000000000000000
      end
      object CM_DESCRIP: TEdit
        Left = 104
        Top = 179
        Width = 331
        Height = 21
        TabOrder = 2
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 440
        Lines.Strings = (
          ''
          ''
          ''
          ''
          
            'Se registrar'#225' un movimiento de retenci'#243'n en la cuenta selecciona' +
            'da.')
      end
      inherited ProgressPanel: TPanel
        Top = 166
        Width = 440
      end
    end
  end
end
