inherited TiposDeposito_DevEx: TTiposDeposito_DevEx
  Left = 399
  Top = 179
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
        MinWidth = 70
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 90
      end
      object TB_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TB_INGLES'
        MinWidth = 70
      end
      object TB_SISTEMA: TcxGridDBColumn
        Caption = 'Sistema'
        DataBinding.FieldName = 'TB_SISTEMA'
        MinWidth = 70
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
