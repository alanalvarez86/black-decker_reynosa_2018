inherited Diccion_DevEx: TDiccion_DevEx
  Left = 375
  Top = 165
  Caption = 'Diccionario de Datos'
  ClientWidth = 620
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 620
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 294
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 620
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 73
      Height = 13
      Caption = 'Tabla Principal:'
    end
    object CBEntidades: TComboBox
      Left = 95
      Top = 4
      Width = 258
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = CBEntidadesChange
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 49
    Width = 620
    Height = 175
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object DI_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'DI_NOMBRE'
        MinWidth = 75
      end
      object DI_TITULO: TcxGridDBColumn
        Caption = 'T'#237'tulo'
        DataBinding.FieldName = 'DI_TITULO'
        MinWidth = 75
      end
      object DI_ANCHO: TcxGridDBColumn
        Caption = 'Ancho'
        DataBinding.FieldName = 'DI_ANCHO'
        MinWidth = 75
      end
      object DI_MASCARA: TcxGridDBColumn
        Caption = 'M'#225'scara'
        DataBinding.FieldName = 'DI_MASCARA'
        MinWidth = 75
      end
      object DI_CONFI: TcxGridDBColumn
        Caption = 'Confidencial?'
        DataBinding.FieldName = 'DI_CONFI'
        MinWidth = 95
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 128
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670320
  end
  inherited ActionList: TActionList
    Left = 258
    Top = 48
  end
  inherited PopupMenu1: TPopupMenu
    Left = 224
    Top = 48
  end
end
