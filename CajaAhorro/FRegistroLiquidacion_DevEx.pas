unit FRegistroLiquidacion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBCtrls, Mask, ZetaFecha, ZetaEdit,
  ZetaKeyLookup_DevEx, DB, ExtCtrls, ZetaNumero,
  ZetaDBTextBox, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, ImgList,
  dxBarExtItems, dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons,
  Buttons;

type
  TRegistroLiquidacion_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    EmpleadoLbl: TLabel;
    ZEmpleado: TZetaKeyLookup_DevEx;
    GBCheque: TGroupBox;
    Label11: TLabel;
    CT_CODIGO: TZetaDBKeyLookup_DevEx;
    Label12: TLabel;
    CM_CHEQUE: TZetaDBNumero;
    Label13: TLabel;
    CM_DESCRIP: TDBEdit;
    ImprimeCheque: TCheckBox;
    Label8: TLabel;
    CM_FECHA: TZetaDBFecha;
    Label15: TLabel;
    Tasa: TZetaDBNumero;
    Label16: TLabel;
    Duracion: TZetaDBNumero;
    gbCondiciones: TGroupBox;
    SpeedButton1: TSpeedButton;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label25: TLabel;
    gbTipoRetiro: TGroupBox;
    lbMonto: TLabel;
    CM_MONTO: TZetaDBNumero;
    DarBaja: TCheckBox;
    ImprimeLiquidacion: TCheckBox;
    RetiroLiquidacion: TRadioButton;
    RetiroParcial: TRadioButton;
    dsCuentas: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    CM_TOT_AHO: TZetaDBTextBox;
    CM_INTERES: TZetaDBTextBox;
    CM_AFAVOR: TZetaDBTextBox;
    CM_SAL_PRE: TZetaDBTextBox;
    CM_DISPONIBLE: TZetaDBTextBox;
    AH_FECHA: TZetaDBTextBox;
    Disponible_Pos: TZetaTextBox;
    CM_TOT_AHO_EMP: TZetaDBTextBox;
    CM_INTERES_EMP: TZetaDBTextBox;
    CM_AFAVOR_EMP: TZetaDBTextBox;
    CM_DISPONIBLE_EMP: TZetaDBTextBox;
    lbEmpleado: TLabel;
    lbEmpresa: TLabel;
    CM_TOTAL_DISPONIBLE: TZetaDBTextBox;
    lbSaldo: TLabel;
    BtnSiguiente_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnSiguienteClick(Sender: TObject);
    procedure CT_CODIGOValidKey(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure RetiroParcialClick(Sender: TObject);
    procedure RetiroLiquidacionClick(Sender: TObject);
    procedure CM_MONTOExit(Sender: TObject);
    procedure ZEmpleadoValidKey(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure SetControlsTipoRetiro(const lParcial: Boolean);
    procedure SetControlsTotalesAhorro;
  protected
    { Protected declarations }
    function CheckDerechos(const iDerecho: Integer): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Connect;override;
    procedure EscribirCambios; override;
    procedure ImprimirForma;override;
  public
    { Public declarations }
  end;

var
  RegistroLiquidacion_DevEx: TRegistroLiquidacion_DevEx;

implementation

uses dCajaAhorro,
     dCliente,
     ZetaTipoEntidad,
     ZetaDialogo,
     ZImprimeForma,
     ZConstruyeFormula,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet,
     DBasicoCliente;

{$R *.dfm}

procedure TRegistroLiquidacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := ZEmpleado;
     TipoValorActivo1 := stTipoAhorro;
     IndexDerechos := 0; // PENDIENTE
     HelpContext := H_LIQUID_RET_PARCIAL;
     ZEmpleado.LookupDataSet := dmCliente.cdsEmpleadoLookUp;
end;

procedure TRegistroLiquidacion_DevEx.Connect;
const
     K_FILTRO_CTAS = '( AH_TIPO = %s and CT_STATUS = 0 )';//S�lo cuentas activas
begin
     with CT_CODIGO do
     begin
          LookupDataset := dmCajaAhorro.cdsCtasBancarias;
          Filtro := Format( K_FILTRO_CTAS, [ EntreComillas( dmCliente.TipoAhorro ) ] );
     end;
     with dmCliente.cdsEmpleadoLookUp do
     begin
          ZEmpleado.ResetMemory;
          Conectar;
          //ZEmpleado.SetLlaveDescripcion( IntToStr( FieldByName('CB_CODIGO').AsInteger ) , FieldByName('PRETTYNAME').AsString);
     end;
     with dmCajaAhorro do
     begin
          RefrescarLiquidacion( dmCliente.Empleado );
          with cdsLiquidacionAhorro do
          begin
               if IsEmpty or ( eStatusAhorro( FieldByName('AH_STATUS').AsInteger ) = saCancelado ) then
               begin
                    EmptyDataSet;
                    ZEmpleado.SetLlaveDescripcion( VACIO , VACIO );
               end
               else
                   ZEmpleado.Valor := dmCliente.Empleado;
          end;

          cdsTPresta.Conectar;
          cdsCtasBancarias.Conectar;
          Datasource.Dataset := cdsLiquidacionAhorro;    // Ya debe estar agregado el registro al llegar a la forma
          dsCuentas.Dataset := cdsCtasMovsLiquida;
     end;
     RetiroLiquidacion.Checked := True; 
     SetControlsTipoRetiro( RetiroParcial.Checked );
     SetControlsTotalesAhorro;
     CM_MONTOExit(Self);
end;


procedure TRegistroLiquidacion_DevEx.EscribirCambios;
begin
     if not dmCajaAhorro.ValidarCheque( CT_CODIGO.Llave, CM_CHEQUE.ValorEntero ) then
     begin
          if CM_CHEQUE.ValorEntero > 0 then
          begin
               with dmCajaAhorro do
               begin
                    with cdsCtasMovsLiquida do
                    begin
                         if ( State = dsBrowse ) then Edit;
                         if RetiroParcial.Checked then
                            FieldByName('CM_TIPO').AsString := 'RETPAR'
                         else
                            FieldByName('CM_TIPO').AsString := 'LIQUID';
                    end;

                    if ( DarBaja.Checked ) then
                    with cdsLiquidacionAhorro do
                    begin
                         if ( State = dsBrowse ) then Edit;
                         FieldByName('AH_STATUS').AsInteger := ord( saCancelado );
                    end;
                    RegimprimirCheque := ImprimeCheque.Checked;
                    RegimprimirChequeLiq := ImprimeLiquidacion.Checked;

                    dmCajaAhorro.RegistrarLiquidacion;
                    Close;
               end;
          end
          else
              ZetaDialogo.ZError( 'Liquidaci�n', 'N�mero de cheque debe ser mayor a cero', 0 );
     end
     else
         ZetaDialogo.ZError( 'Liquidaci�n', 'N�mero de cheque repetido. Es necesario cambiarlo.', 0 );
end;

procedure TRegistroLiquidacion_DevEx.Agregar;
begin
     { No hace nada }
end;

procedure TRegistroLiquidacion_DevEx.Borrar;
begin
     { No hace nada }
end;

{ Eventos de controles }

procedure TRegistroLiquidacion_DevEx.BtnSiguienteClick(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
        CM_CHEQUE.Valor := dmCajaAhorro.SiguienteCheque( CT_CODIGO.Llave );
end;

procedure TRegistroLiquidacion_DevEx.CT_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     if strLleno( CT_CODIGO.Llave ) then
     begin
          with dmCajaAhorro.cdsCtasBancarias do
          begin
               Locate( 'CT_CODIGO', CT_CODIGO.Llave, [] );
               self.ImprimeCheque.Checked := ( FieldByName( 'CT_REP_CHK' ).AsInteger > 0 );
               self.ImprimeLiquidacion.Checked := ( FieldByName( 'CT_REP_LIQ' ).AsInteger > 0 );
          end;
     end;
end;

procedure TRegistroLiquidacion_DevEx.ImprimirForma;
begin
     ZImprimeForma.ImprimeUnaForma( enPrestamo, dmCajaAhorro.cdsRegPrestamos );
end;


procedure TRegistroLiquidacion_DevEx.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     //inherited;

end;

procedure TRegistroLiquidacion_DevEx.RetiroParcialClick(Sender: TObject);
begin
     inherited;
     RetiroLiquidacion.Checked := NOT RetiroParcial.Checked;
     SetControlsTipoRetiro( RetiroParcial.Checked );
end;

procedure TRegistroLiquidacion_DevEx.RetiroLiquidacionClick(Sender: TObject);
begin
     inherited;
     RetiroParcial.Checked := NOT RetiroLiquidacion.Checked;
     SetControlsTipoRetiro( RetiroParcial.Checked );
end;

procedure TRegistroLiquidacion_DevEx.SetControlsTipoRetiro( const lParcial: Boolean );
begin
     DarBaja.Checked := ( not lParcial );
     DarBaja.Enabled := lParcial;
     lbMonto.Enabled := lParcial;
     CM_MONTO.Valor := dmCajaAhorro.cdsCtasMovsLiquida.FieldByName('CM_TOTAL_DISPONIBLE').AsFloat;
     CM_MONTOExit(Self);
     CM_MONTO.Enabled := lParcial;
end;

procedure TRegistroLiquidacion_DevEx.SetControlsTotalesAhorro;
 var
    lVisible: Boolean;
begin
     lVisible := (dmCliente.GetDatosAhorroActivo.Relativo <> 0);
     lbEmpleado.Visible := lVisible;
     lbEmpresa.Visible := lVisible;
     lbSaldo.Visible := lVisible;
     CM_TOT_AHO_EMP.Visible := lVisible;
     CM_INTERES_EMP.Visible := lVisible;
     CM_AFAVOR_EMP.Visible := lVisible;
     CM_DISPONIBLE_EMP.Visible := lVisible;
     CM_TOTAL_DISPONIBLE.Visible := lVisible;
end;

function TRegistroLiquidacion_DevEx.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := True;
end;

procedure TRegistroLiquidacion_DevEx.CM_MONTOExit(Sender: TObject);
begin
     inherited;
     Disponible_POS.Caption := FormatFloat('#,0.00;-#,0.00',dmCajaAhorro.cdsCtasMovsLiquida.FieldByName( 'CM_TOTAL_DISPONIBLE' ).AsFloat - CM_MONTO.Valor );
end;

procedure TRegistroLiquidacion_DevEx.ZEmpleadoValidKey(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
            // if not ( ( Sender is TBitBtn ) and ( TBitBtn(Sender).Name = 'Cancelar' ) ) then
             if Activecontrol.Name <> 'Cancelar_DevEx' then
             begin
                  if ZEmpleado.Valor > 0 then
                  begin
                       with dmCajaAhorro do
                       begin
                            RefrescarLiquidacion(ZEmpleado.Valor);
                            if ( cdsLiquidacionAhorro.IsEmpty ) then
                            begin
                                 zError( Self.Caption,Format( 'El empleado %d no es socio de %s' , [ZEmpleado.Valor,GetDatosAhorroActivo.Descripcion] ),0 );
                                 ZEmpleado.SetFocus;
                            end
                            else if ( eStatusAhorro( cdsLiquidacionAhorro.FieldByName('AH_STATUS').AsInteger ) = saCancelado ) then
                            begin
                                 zError( Self.Caption , Format( 'El empleado %d ya fue dado de baja de %s' , [ZEmpleado.Valor,GetDatosAhorroActivo.Descripcion] ),0 );
                                 ZEmpleado.SetFocus;
                            end
                            else
                            begin
                                 cdsLiquidacionAhorro.Edit;
                                 with cdsCtasMovsLiquida do
                                 begin
                                      Edit;
                                      FieldByName('CM_FECHA').AsDateTime := FechaDefault;

                                      cdsCtasBancarias.Filter := CT_CODIGO.Filtro;
                                      cdsCtasBancarias.Filtered := TRUE;
                                      try
                                         FieldByName('CT_CODIGO').AsString := cdsCtasBancarias.FieldByName('CT_CODIGO').AsString;
                                      finally
                                             cdsCtasBancarias.Filtered := FALSE;
                                      end;
                                 end;
                                 cdsLiquidacionAhorro.FieldByName('TASA').AsFloat := cdsTAhorro.FieldByName('TB_TASA1').AsFloat;
                                 self.ActiveControl := CM_FECHA;
                            end;
                       end;
                  end
                  else
                  begin
                       ZetaDialogo.zError( 'Registrar liquidaci�n', '! Empleado no encontrado !', 0 );
                       ZEmpleado.SetFocus;
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TRegistroLiquidacion_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  if ( TZetaClientDataSet( DataSource.DataSet ).ChangeCount = 0 ) and         //Si se aplicaron los cambios
        ( not ( DataSource.DataSet.State in [ dsEdit, dsInsert ] ) ) then    //y no se tienen cambios pendientes
        Close;
end;

procedure TRegistroLiquidacion_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  Close;
end;

end.
