inherited TiposDeposito: TTiposDeposito
  Left = 432
  Top = 464
  Caption = 'Tipo de dep'#243'sito/retiro'
  ClientWidth = 624
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 624
    inherited ValorActivo2: TPanel
      Width = 365
    end
  end
  object DBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 624
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TB_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_ELEMENT'
        Title.Caption = 'Descripci'#243'n'
        Width = 205
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_INGLES'
        Title.Caption = 'Ingl'#233's'
        Width = 210
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TB_SISTEMA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Sistema'
        Width = 50
        Visible = True
      end>
  end
end
