inherited RegistroPrestamo: TRegistroPrestamo
  Left = 2312
  Top = 124
  Caption = 'Solcitud y registro de pr'#233'stamo'
  ClientHeight = 531
  ClientWidth = 456
  PixelsPerInch = 96
  TextHeight = 13
  object AH_TIPOLbl: TLabel [0]
    Left = 68
    Top = 55
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pr'#233'stamo:'
  end
  object Label8: TLabel [1]
    Left = 60
    Top = 99
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Referencia:'
  end
  object Label1: TLabel [2]
    Left = 543
    Top = 131
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de inicio:'
    Visible = False
  end
  object EmpleadoLbl: TLabel [3]
    Left = 66
    Top = 76
    Width = 50
    Height = 13
    Caption = 'Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label15: TLabel [4]
    Left = 503
    Top = 99
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de pr'#233'stamo:'
    Visible = False
  end
  object TIPO_PRESTAMO: TZetaTextBox [5]
    Left = 201
    Top = 53
    Width = 248
    Height = 17
    AutoSize = False
    Caption = 'TIPO_PRESTAMO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object PR_TIPO: TZetaDBTextBox [6]
    Left = 119
    Top = 53
    Width = 80
    Height = 17
    AutoSize = False
    Caption = 'PR_TIPO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'PR_TIPO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 495
    Width = 456
    TabOrder = 9
    inherited OK: TBitBtn
      Left = 288
    end
    inherited Cancelar: TBitBtn
      Left = 373
    end
  end
  inherited PanelSuperior: TPanel
    Width = 456
    TabOrder = 10
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 456
    TabOrder = 11
    inherited ValorActivo2: TPanel
      Width = 130
      inherited textoValorActivo2: TLabel
        Width = 124
      end
    end
  end
  object PR_REFEREN: TZetaDBEdit [10]
    Left = 119
    Top = 95
    Width = 80
    Height = 21
    TabOrder = 1
    DataField = 'PR_REFEREN'
    DataSource = DataSource
  end
  object PR_FECHA: TZetaDBFecha [11]
    Left = 622
    Top = 126
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 8
    Text = '17/dic/97'
    Valor = 35781.000000000000000000
    Visible = False
    DataField = 'PR_FECHA'
    DataSource = DataSource
  end
  object CB_CODIGO: TZetaDBKeyLookup [12]
    Left = 119
    Top = 72
    Width = 333
    Height = 21
    Enabled = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object GBCondiciones: TGroupBox [13]
    Left = 0
    Top = 190
    Width = 456
    Height = 126
    Align = alBottom
    Caption = ' Condiciones del pr'#233'stamo '
    TabOrder = 4
    object LBL_Formula: TLabel
      Left = 74
      Top = 183
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
    end
    object SBCO_FORMULA: TSpeedButton
      Left = 424
      Top = 179
      Width = 25
      Height = 25
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
    object Label7: TLabel
      Left = 34
      Top = 41
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto solicitado:'
    end
    object Label4: TLabel
      Left = 67
      Top = 64
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Semanas:'
    end
    object Label10: TLabel
      Left = 242
      Top = 202
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Abonos anteriores:'
      Visible = False
    end
    object Label19: TLabel
      Left = 530
      Top = 68
      Width = 24
      Height = 13
      Caption = 'Aval:'
      Enabled = False
      Visible = False
    end
    object Label20: TLabel
      Left = 493
      Top = 89
      Width = 101
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto Total del Aval:'
      Visible = False
    end
    object MontoAval: TZetaTextBox
      Left = 558
      Top = 87
      Width = 80
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Visible = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label21: TLabel
      Left = 27
      Top = 85
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total de intereses:'
    end
    object Intereses: TZetaTextBox
      Left = 118
      Top = 83
      Width = 80
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label3: TLabel
      Left = 504
      Top = 110
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Caption = 'Monto Aval Solicitado:'
      Enabled = False
      Visible = False
    end
    object Label2: TLabel
      Left = 34
      Top = 20
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo disponible:'
    end
    object SaldoDisponible: TZetaTextBox
      Left = 118
      Top = 18
      Width = 80
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label5: TLabel
      Left = 9
      Top = 104
      Width = 105
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total a descontar:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object TotalaDescontar: TZetaTextBox
      Left = 118
      Top = 102
      Width = 80
      Height = 17
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label14: TLabel
      Left = 200
      Top = 104
      Width = 97
      Height = 13
      Caption = '(Semanalmente)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
    end
    object PR_MESES: TZetaDBNumero
      Left = 526
      Top = 64
      Width = 115
      Height = 21
      Mascara = mnDias
      TabOrder = 5
      Text = '0'
      DataField = 'PR_MESES'
      DataSource = DataSource
    end
    object PR_MONTO: TZetaDBNumero
      Left = 118
      Top = 37
      Width = 80
      Height = 21
      Mascara = mnPesos
      TabOrder = 0
      Text = '0.00'
      OnExit = PR_PAGOSExit
      DataField = 'PR_MONTO'
      DataSource = DataSource
    end
    object PR_PAGOS: TZetaDBNumero
      Left = 118
      Top = 60
      Width = 80
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
      OnExit = PR_PAGOSExit
      DataField = 'PR_PAGOS'
      DataSource = DataSource
    end
    object PR_PAG_PER: TZetaDBNumero
      Left = 118
      Top = 156
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 6
      Text = '0.00'
      DataField = 'PR_PAG_PER'
      DataSource = DataSource
    end
    object PR_SALDO_I: TZetaDBNumero
      Left = 334
      Top = 198
      Width = 115
      Height = 21
      Mascara = mnPesos
      TabOrder = 3
      Text = '0.00'
      Visible = False
      DataField = 'PR_SALDO_I'
      DataSource = DataSource
    end
    object Aval: TZetaKeyLookup
      Left = 558
      Top = 64
      Width = 333
      Height = 21
      Enabled = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      Visible = False
      WidthLlave = 80
      OnValidKey = AvalValidKey
    end
    object MontoAvalReal: TZetaNumero
      Left = 558
      Top = 106
      Width = 80
      Height = 21
      Enabled = False
      Mascara = mnPesos
      TabOrder = 4
      Text = '0.00'
      Visible = False
      OnExit = MontoAvalRealExit
    end
  end
  object GBCheque: TGroupBox [14]
    Left = 0
    Top = 385
    Width = 456
    Height = 110
    Align = alBottom
    TabOrder = 7
    object Label11: TLabel
      Left = 33
      Top = 21
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cuenta bancaria:'
      Enabled = False
    end
    object Label12: TLabel
      Left = 64
      Top = 44
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheque #:'
      Enabled = False
    end
    object BtnSiguiente: TSpeedButton
      Left = 236
      Top = 39
      Width = 25
      Height = 25
      Hint = 'Sugiere siguiente n'#250'mero de cheque'
      Enabled = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BtnSiguienteClick
    end
    object Label13: TLabel
      Left = 55
      Top = 67
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
      Enabled = False
    end
    object CT_CODIGO: TZetaKeyLookup
      Left = 118
      Top = 18
      Width = 333
      Height = 21
      Enabled = False
      LookupDataset = dmCajaAhorro.cdsCtasBancarias
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 80
      OnValidKey = CT_CODIGOValidKey
    end
    object CM_CHEQUE: TZetaNumero
      Left = 118
      Top = 41
      Width = 115
      Height = 21
      Enabled = False
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
    end
    object CM_DESCRIP: TEdit
      Left = 118
      Top = 64
      Width = 331
      Height = 21
      Enabled = False
      TabOrder = 2
    end
    object ImprimeCheque: TCheckBox
      Left = 35
      Top = 86
      Width = 96
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Imprimir cheque:'
      Checked = True
      Enabled = False
      State = cbChecked
      TabOrder = 3
    end
  end
  object RegistrarCheque: TCheckBox [15]
    Left = 10
    Top = 384
    Width = 105
    Height = 17
    Caption = 'Registrar cheque'
    Checked = True
    State = cbChecked
    TabOrder = 6
    OnClick = RegistrarChequeClick
  end
  object CM_PRESTA: TZetaKeyCombo [16]
    Left = 536
    Top = 95
    Width = 115
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 2
    ListaFija = lfTiposdePrestamo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
  object gbDescontar: TGroupBox [17]
    Left = 0
    Top = 316
    Width = 456
    Height = 69
    Align = alBottom
    Caption = ' Descontar a partir de '
    TabOrder = 5
    object Label6: TLabel
      Left = 76
      Top = 44
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
      Enabled = False
    end
    object Label9: TLabel
      Left = 94
      Top = 20
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'A'#241'o:'
      Enabled = False
    end
    object PE_YEAR: TZetaNumero
      Left = 119
      Top = 16
      Width = 80
      Height = 21
      Enabled = False
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
    end
    object PE_NUMERO: TZetaKeyLookup
      Left = 119
      Top = 40
      Width = 333
      Height = 21
      Enabled = False
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 80
    end
  end
  object gbPagar: TGroupBox [18]
    Left = 0
    Top = 122
    Width = 456
    Height = 68
    Align = alBottom
    Caption = ' Pago del pr'#233'stamo en n'#243'mina '
    TabOrder = 3
    object Label17: TLabel
      Left = 75
      Top = 43
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label18: TLabel
      Left = 93
      Top = 20
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'A'#241'o:'
    end
    object PeriodoFechaFinal: TZetaTextBox
      Tag = 1
      Left = 353
      Top = 17
      Width = 74
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PE_YEAR2: TZetaNumero
      Left = 118
      Top = 16
      Width = 80
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
    end
    object PE_NUMERO2: TZetaKeyLookup
      Left = 118
      Top = 39
      Width = 333
      Height = 21
      Filtro = 'PE_STATUS <> 6'
      Opcional = False
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 80
      OnValidKey = PE_NUMERO2ValidKey
      OnValidLookup = PE_NUMERO2ValidLookup
    end
  end
  object lImprimeFormato: TCheckBox [19]
    Left = 346
    Top = 97
    Width = 106
    Height = 17
    TabStop = False
    Caption = #191'Imprimir formato?'
    Checked = True
    State = cbChecked
    TabOrder = 12
  end
  inherited DataSource: TDataSource
    Left = 5
    Top = 56
  end
end
