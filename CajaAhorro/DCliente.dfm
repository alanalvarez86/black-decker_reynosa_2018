inherited dmCliente: TdmCliente
  Height = 265
  Width = 450
  inherited cdsEmpleadoLookUp: TZetaLookupDataSet
    LookupDescriptionField = 'PrettyName'
    Left = 192
    Top = 16
  end
  inherited cdsTPeriodos: TZetaLookupDataSet
    Left = 113
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpleadoAlAdquirirDatos
    Left = 118
    Top = 8
  end
  object cdsTipoAhorro: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 144
  end
  object cdsPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 190
    Top = 64
  end
end
