inherited RegistroInscripcionPE_DevEx: TRegistroInscripcionPE_DevEx
  Left = 949
  Top = 201
  Caption = 'Inscribir en ahorro'
  ClientHeight = 219
  ClientWidth = 433
  Color = clBtnHighlight
  PixelsPerInch = 96
  TextHeight = 13
  object EmpleadoLbl: TLabel [0]
    Left = 34
    Top = 60
    Width = 50
    Height = 13
    Caption = '&Empleado:'
    FocusControl = CB_CODIGO
    Transparent = True
  end
  object Label1: TLabel [1]
    Left = 9
    Top = 85
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha de inicio:'
    Transparent = True
  end
  object Label18: TLabel [2]
    Left = 425
    Top = 84
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo inicial:'
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 183
    Width = 433
    TabOrder = 5
    inherited OK_DevEx: TcxButton
      Left = 266
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 345
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 433
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 107
      inherited textoValorActivo2: TLabel
        Width = 101
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 11
  end
  object CB_CODIGO: TZetaDBKeyLookup_DevEx [6]
    Left = 86
    Top = 56
    Width = 340
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object AH_FECHA: TZetaDBFecha [7]
    Left = 86
    Top = 80
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '17/dic/97'
    Valor = 35781.000000000000000000
    DataField = 'AH_FECHA'
    DataSource = DataSource
  end
  object AH_SALDO_I: TZetaDBNumero [8]
    Left = 486
    Top = 80
    Width = 115
    Height = 21
    Mascara = mnPesos
    TabOrder = 6
    Text = '0.00'
    Visible = False
    DataField = 'AH_SALDO_I'
    DataSource = DataSource
  end
  object gbFormula: TcxGroupBox [9]
    Left = 8
    Top = 104
    Caption = '                       '
    TabOrder = 3
    Height = 73
    Width = 417
    object lbFormula: TLabel
      Left = 42
      Top = 28
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'F'#243'rmula:'
      Transparent = True
    end
    object AH_FORMULA: TDBEdit
      Left = 86
      Top = 24
      Width = 320
      Height = 21
      DataField = 'AH_FORMULA'
      DataSource = DataSource
      TabOrder = 1
    end
    object nPorcentaje: TZetaNumero
      Left = 86
      Top = 24
      Width = 121
      Height = 21
      Mascara = mnTasa
      TabOrder = 0
      Text = '0.0 %'
      Visible = False
    end
    object cmbPorcentaje: TZetaKeyCombo
      Left = 86
      Top = 24
      Width = 62
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      Items.Strings = (
        '3'
        '5'
        '10'
        '15')
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
  end
  object lImprimeFormato: TCheckBox [10]
    Left = 320
    Top = 83
    Width = 106
    Height = 17
    TabStop = False
    Caption = #191'Imprimir formato?'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object cbPorcentaje: TCheckBox [11]
    Left = 20
    Top = 107
    Width = 74
    Height = 17
    Caption = 'Porcentaje'
    TabOrder = 12
    OnClick = cbPorcentajeClick
  end
  inherited DataSource: TDataSource
    Left = 12
    Top = 169
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 4718816
  end
end
