unit FTiposDeposito_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  dxSkinsDefaultPainters,  dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  TressMorado2013, Menus, ActnList, ImgList,
  StdCtrls;

type
  TTiposDeposito_DevEx = class(TBaseGridLectura_DevEx)
    TB_CODIGO: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    TB_INGLES: TcxGridDBColumn;
    TB_SISTEMA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
   { Public declarations }
   procedure DoLookup; override;
  end;

var
  TiposDeposito_DevEx: TTiposDeposito_DevEx;

implementation

uses dCajaAhorro,
     ZetaCommonClasses,
     ZetaBuscador_DevEx,
     ZAccesosTress;

{$R *.dfm}

{ TCtasBancarias }

procedure TTiposDeposito_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CAT_TIPOS_DEP_RET;
     IndexDerechos:= D_AHORRO_CAT_TIPO_DEPOSITO;
end;

procedure TTiposDeposito_DevEx.Connect;
begin
     with dmCajaAhorro do
     begin
          cdsTiposDeposito.Conectar;
          DataSource.DataSet:= cdsTiposDeposito;
     end;
end;

procedure TTiposDeposito_DevEx.Refresh;
begin
     dmCajaAhorro.cdsTiposDeposito.Refrescar;
end;

procedure TTiposDeposito_DevEx.Agregar;
begin
     dmCajaAhorro.cdsTiposDeposito.Agregar;
end;

procedure TTiposDeposito_DevEx.Modificar;
begin
     dmCajaAhorro.cdsTiposDeposito.Modificar;
end;

procedure TTiposDeposito_DevEx.Borrar;
begin
     dmCajaAhorro.cdsTiposDeposito.Borrar;
     //Aplica el BestFit para las columnas despues de que se ha eliminado el registro
     DoBestFit;
end;

procedure TTiposDeposito_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Tipos de Dep�sito/Retiro', 'Tipos de Dep�sito/Retiro', 'TB_CODIGO', dmCajaAhorro.cdsTiposDeposito );
end;

procedure TTiposDeposito_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth; //DevEx: Se invoca despues de que todas las columnas del grid han sido creadas y antes de Aplicar el BestFit.
  inherited;
end;

end.
