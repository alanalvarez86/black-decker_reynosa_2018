unit FWizRDDImportar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, DbGrids,
     ZetaCommonLists,
     ZetaWizard,
     ZetaKeyLookup,
     ZetaEdit, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses,
  cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl,
  Vcl.Menus, cxButtons, dxSkinsCore, dxSkinsDefaultPainters,
  cxRadioGroup;

type
  TWizRDDImportar_DevEx = class(TcxBaseWizard)
    dlgImportar: TOpenDialog;
    Label5: TLabel;
    Archivo: TEdit;
    ArchivoSeek: TcxButton;
    rdgConflicto: TcxRadioGroup;
    lPrenderDerechos: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure ArchivoSeekClick(Sender: TObject);

  private
    { Private declarations }
    function GetConflicto:eConflictoImp;
   protected
    { Protected declarations }
     procedure CargaParametros;override;
     function EjecutarWizard: Boolean; override;
   public
    { Public declarations }
  end;

var
  WizRDDImportar_DevEx: TWizRDDImportar_DevEx;

implementation

uses FTressShell,
     DProcesos,
     DReportes,
     DXMLTools,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaCommonClasses;

{$R *.DFM}

{ ************ TBaseWizardFiltro ************ }

{ TWizRDDImportar }

function TWizRDDImportar_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarDiccionario(ParameterList);
end;

procedure TWizRDDImportar_DevEx.CargaParametros;
 var
    XML: TdmXMLTools;
begin

     with Descripciones do
     begin
          Clear;
          AddString( 'Archivo', Archivo.Text );
          {AddInteger( 'Respetar', Ord( GetConflicto ) );
          AddBoolean('Prender Derechos', lPrenderDerechos.Checked );}
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddInteger( 'Respetar', Ord( GetConflicto ) );
          AddBoolean( 'PrenderDerechos', lPrenderDerechos.Checked );
          if lPrenderDerechos.Checked then
          begin
               AddString( 'EmpresasConfidenciales', dmReportes.GetListaEmpresasConfidenciales );
          end;



          XML := TdmXMLTools.Create(NIL);
          with XML do
          begin
               XMLDocument.LoadFromFile( ParamByName('Archivo').AsString );
               AddString( 'XMLFile', XMLDocument.XML.Text );
               Free;
          end;
     end;
end;

function TWizRDDImportar_DevEx.GetConflicto: eConflictoImp;
begin
     Result := eConflictoImp( rdgConflicto.ItemIndex );
end;

procedure TWizRDDImportar_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if ZetaCommonTools.StrVacio( Archivo.Text ) then
                  CanMove := Error( 'Debe Especificarse un Archivo a Importar', Parametros )
               else
                  if not FileExists( Archivo.Text ) then
                     CanMove := Error( 'El Archivo ' + Archivo.Text + ' No Existe', Parametros );
          end;
     end;
end;


procedure TWizRDDImportar_DevEx.FormCreate(Sender: TObject);
begin
     Archivo.Tag := K_GLOBAL_DEF_RDD_ARCHIVO_IMPORTAR;
     inherited;
     HelpContext:= H_RDD_IMPORTAR_DICCION;  
end;

procedure TWizRDDImportar_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
      Archivo.Text := ZetaClientTools.AbreDialogo( dlgImportar , Archivo.Text, 'xml' );
end;

end.



