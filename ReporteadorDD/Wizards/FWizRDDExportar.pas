unit FWizRDDExportar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, Buttons, ComCtrls, ExtCtrls,
     ZetaCommonLists,
     ZBaseWizardFiltroRDD,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaWizard;

type
  TWizRDDExportar = class(TBaseWizardFiltroRDD)
    Archivo: TEdit;
    ArchivoSeek: TSpeedButton;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    edtVersionFinal: TZetaEdit;
    zedtListaVersion: TZetaEdit;
    edtVersionInicial: TZetaEdit;
    rdVersionLista: TRadioButton;
    rdVersionRango: TRadioButton;
    rdVersionTodas: TRadioButton;
    Label1: TLabel;
    Label2: TLabel;
    rdInformacionExportar: TRadioGroup;
    dlgExportar: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure VersionsClick(Sender: TObject);

  private
    { Private declarations }
    function GetInformacion:eTipoInfoExp;
    function GetNumVersion: eTipoRangoActivo;

  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizRDDExportar: TWizRDDExportar;

implementation

uses DCliente,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonTools,
     ZBaseSelectGrid,
     FWizRDDExportarGridSelect,
     DGlobal,
     ZGlobalTress,
     DProcesos;

{$R *.DFM}

procedure TWizRDDExportar.FormCreate(Sender: TObject);
begin
     Archivo.Tag := K_GLOBAL_DEF_RDD_ARCHIVO_EXPORTAR;
     inherited;
     HelpContext:= H_RDD_EXPORTAR_DICCION;
end;

procedure TWizRDDExportar.CargaParametros;
begin
     with Descripciones do
     begin
          AddString('Archivo',Archivo.Text );
          AddString('VersionInicial',edtVersionInicial.Text);
          AddString('VersionFinal',edtVersionFinal.Text);
          AddString('VersionLista',zedtListaVersion.Text);
          AddInteger('InformacionAExportar',Ord(GetInformacion));
          AddInteger('Version',Ord( GetNumVersion));

     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddString('Archivo',Archivo.Text );
          AddInteger('Version',Ord( GetNumVersion));
          AddString('VersionInicial',edtVersionInicial.Text);
          AddString('VersionFinal',edtVersionFinal.Text);
          AddString('VersionLista',zedtListaVersion.Text);
          AddInteger('InformacionAExportar',Ord(GetInformacion));
     end;

end;

procedure TWizRDDExportar.CargaListaVerificacion;
begin
     dmProcesos.TablasGetLista(ParameterList);
end;

function TWizRDDExportar.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TWizRDDExportarGridSelect );
end;


procedure TWizRDDExportar.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( PageControl.ActivePage = Parametros ) then
          begin
               if  not StrLleno(Archivo.Text ) then
                   CanMove := Error( 'Debe Especificarse un Archivo a Exportar', Parametros );
          end;
     end;
end;

function TWizRDDExportar.EjecutarWizard: Boolean;
begin
      Result := dmProcesos.ExportarDiccionario( ParameterList, Verificacion );
end;


procedure TWizRDDExportar.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     Archivo.Text := ZetaClientTools.AbreDialogo( dlgExportar , Archivo.Text, 'xml' );
end;

procedure TWizRDDExportar.FormShow(Sender: TObject);
begin
     inherited;
     zedtListaVersion.Text := IntToStr(Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD));//Global
end;


function TWizRDDExportar.GetInformacion: eTipoInfoExp;
begin
     Result:= eTipoInfoExp( rdInformacionExportar.ItemIndex );
end;

function TWizRDDExportar.GetNumVersion: eTipoRangoActivo;
begin
     Result := raTodos;
     if  rdVersionRango.Checked  then
         Result := raRango
     else if rdVersionLista.Checked then
          Result := raLista;
end;

procedure TWizRDDExportar.VersionsClick(Sender: TObject);
begin
     inherited;
     if (rdVersionTodas.Checked )then
     begin
          edtVersionInicial.Enabled := FALSE;
          edtVersionFinal.Enabled := FALSE;
          zedtListaVersion.Enabled := FALSE;
     end
     else if (rdVersionRango.Checked)then
     begin
          edtVersionInicial.Enabled := true;
          edtVersionFinal.Enabled := true;
          zedtListaVersion.Enabled := FALSE;
     end
     else if (rdVersionLista.Checked)then
     begin
          edtVersionInicial.Enabled := False;
          edtVersionFinal.Enabled := False;
          zedtListaVersion.Enabled := True;
     end
end;

end.
