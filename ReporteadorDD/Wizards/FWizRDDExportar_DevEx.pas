unit FWizRDDExportar_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, Buttons, ComCtrls, ExtCtrls,
     ZetaCommonLists,
     ZetaNumero,
     ZetaKeyCombo,
     ZetaFecha,
     ZetaKeyLookup,
     ZetaEdit,
     ZetaWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, cxButtons, dxCustomWizardControl, dxWizardControl,
  ZcxBaseWizardFiltroRDD, dxSkinsCore, dxSkinsDefaultPainters,
  cxRadioGroup;

type
  TWizRDDExportar_DevEx = class(TBaseWizardFiltroRDD)
    dlgExportar: TOpenDialog;
    Label5: TLabel;
    Archivo: TEdit;
    ArchivoSeek: TcxButton;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    edtVersionFinal: TZetaEdit;
    zedtListaVersion: TZetaEdit;
    edtVersionInicial: TZetaEdit;
    rdVersionLista: TRadioButton;
    rdVersionRango: TRadioButton;
    rdVersionTodas: TRadioButton;
    rdInformacionExportar: TcxRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure VersionsClick(Sender: TObject);

  private
    { Private declarations }
    function GetInformacion:eTipoInfoExp;
    function GetNumVersion: eTipoRangoActivo;

  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizRDDExportar_DevEx: TWizRDDExportar_DevEx;

implementation

uses DCliente,
     ZetaCommonClasses,
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonTools,
     ZBaseSelectGrid_DevEx,
     FWizRDDExportarGridSelect_DevEx,
     DGlobal,
     ZGlobalTress,
     DProcesos;

{$R *.DFM}

procedure TWizRDDExportar_DevEx.FormCreate(Sender: TObject);
begin
     Archivo.Tag := K_GLOBAL_DEF_RDD_ARCHIVO_EXPORTAR;
     inherited;
     HelpContext:= H_RDD_EXPORTAR_DICCION;
end;

procedure TWizRDDExportar_DevEx.CargaParametros;
begin
     with Descripciones do
     begin
          AddString('Archivo',Archivo.Text );
          {AddString('VersionInicial',edtVersionInicial.Text);
          AddString('VersionFinal',edtVersionFinal.Text);
          AddString('VersionLista',zedtListaVersion.Text);
          AddInteger('InformacionAExportar',Ord(GetInformacion)); }
          AddInteger('Version',Ord( GetNumVersion));

     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddString('Archivo',Archivo.Text );
          AddInteger('Version',Ord( GetNumVersion));
          AddString('VersionInicial',edtVersionInicial.Text);
          AddString('VersionFinal',edtVersionFinal.Text);
          AddString('VersionLista',zedtListaVersion.Text);
          AddInteger('InformacionAExportar',Ord(GetInformacion));
     end;

end;

procedure TWizRDDExportar_DevEx.CargaListaVerificacion;
begin
     dmProcesos.TablasGetLista(ParameterList);
end;

function TWizRDDExportar_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TWizRDDExportarGridSelect_DevEx );
end;


procedure TWizRDDExportar_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if  not StrLleno(Archivo.Text ) then
                   CanMove := Error( 'Debe Especificarse un Archivo a Exportar', Parametros );
          end;
     end;
end;

function TWizRDDExportar_DevEx.EjecutarWizard: Boolean;
begin
      Result := dmProcesos.ExportarDiccionario( ParameterList, Verificacion );
end;


procedure TWizRDDExportar_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     Archivo.Text := ZetaClientTools.AbreDialogo( dlgExportar , Archivo.Text, 'xml' );
end;

procedure TWizRDDExportar_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     zedtListaVersion.Text := IntToStr(Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD));//Global
end;


function TWizRDDExportar_DevEx.GetInformacion: eTipoInfoExp;
begin
     Result:= eTipoInfoExp( rdInformacionExportar.ItemIndex );
end;

function TWizRDDExportar_DevEx.GetNumVersion: eTipoRangoActivo;
begin
     Result := raTodos;
     if  rdVersionRango.Checked  then
         Result := raRango
     else if rdVersionLista.Checked then
          Result := raLista;
end;

procedure TWizRDDExportar_DevEx.VersionsClick(Sender: TObject);
begin
     inherited;
     if (rdVersionTodas.Checked )then
     begin
          edtVersionInicial.Enabled := FALSE;
          edtVersionFinal.Enabled := FALSE;
          zedtListaVersion.Enabled := FALSE;
     end
     else if (rdVersionRango.Checked)then
     begin
          edtVersionInicial.Enabled := true;
          edtVersionFinal.Enabled := true;
          zedtListaVersion.Enabled := FALSE;
     end
     else if (rdVersionLista.Checked)then
     begin
          edtVersionInicial.Enabled := False;
          edtVersionFinal.Enabled := False;
          zedtListaVersion.Enabled := True;
     end
end;

end.
