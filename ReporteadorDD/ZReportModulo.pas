unit ZReportModulo;

interface
uses Controls,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZReportTools;

function GetCampoDescripcionGrupos( const FEntidadActiva, FEntidadGrupo : TipoEntidad;
                                    const oDiccion : TDiccionRecord ) : string;
procedure GetFechasRango( const eTipo: eRangoFechas;
                          var dInicial,dFinal : TDate );

function GetEntidadPoliza(const i:integer ) :TipoEntidad;
function GetItemPoliza(const Entidad : TipoEntidad ) : integer;


implementation

function GetItemPoliza(const Entidad : TipoEntidad ) : integer;
begin
     Result := 0;
end;
function GetEntidadPoliza(const i:integer ) :TipoEntidad;
begin
     Result := enNinguno;
end;

procedure GetFechasRango( const eTipo: eRangoFechas;
                          var dInicial,dFinal : TDate );
begin
     dInicial := NULLDATETIME;
     dFinal := NULLDATETIME;
end;

function GetCampoDescripcionGrupos( const FEntidadActiva, FEntidadGrupo : TipoEntidad;
                                    const oDiccion : TDiccionRecord ) : string;
begin
     Result := GetCampoDescripcion( oDiccion );
end;

end.
