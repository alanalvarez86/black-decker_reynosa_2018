unit DReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient,
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaTipoEntidad,
  ZReportTools,
  DBaseReportes;

type
  TdmReportes = class(TdmBaseReportes)
    procedure cdsCampoRepAlCrearCampos(Sender: TObject);
  private
    { Private declarations }
    procedure GrabaFotos;
  protected
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList: OleVariant;
                        var sError: WideString): OleVariant;override;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;override;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;override;
    procedure GetFormaReporte( const eTipo: eTipoReporte );override;
    procedure AfterGeneraSQL;override;
  public
    { Public declarations }
    //function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
    procedure CancelaCuentasPoliza;
    procedure LlenaConceptos(eTiposConcepto: TiposConcepto);
    procedure BuscaConcepto;override;
    function DirectorioPlantillas: string;override;
    function GetListaEmpresasConfidenciales: string;
  end;

var
  dmReportes: TdmReportes;

implementation
uses DCliente,
     DCatalogos,
     DSistema,
     ZReportModulo,
     ZAccesosTress,
     ZetaCommonTools,
     {$IFDEF TRESS_DELPHIXE5_UP}
     FPolizaConcepto_DevEx,
     FPoliza_DevEx;//Forma poliza DevEx
     {$ELSE}
     FPolizaConcepto,
     FPoliza;//Forma poliza
     {$ENDIF}

{$R *.DFM}

function TdmReportes.GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ) : OleVariant;
begin
     Result := ServerReportes.GeneraSQL( dmCliente.Empresa, oSQLAgente, ParamList , sError  );
end;

procedure TdmReportes.AfterGeneraSQL;
begin
     GrabaFotos;
end;

procedure TdmReportes.GrabaFotos;
var
    FBlob : TBlobField;
    iColumna: integer;
    sTipo, sDirectorio,sDirectorio2 : string;
begin
     if ParamList.IndexOf('GRABA_IMAGEN_DISCO') <> -1 then
     begin
          try
             with ParamList do
             begin
                  iColumna := ParamByName('COL_IMAGEN_DISCO').AsInteger;
                  sTipo := ParamByName('TIPO_IMAGEN_DISCO').AsString;
                  sDirectorio := ParamByName('GRABA_IMAGEN_DISCO').AsString;
                  sDirectorio2 := StrTransAll( sDirectorio, '\', '\\');
             end;

             with cdsResultados do
             begin
                  DisableControls;
                  try
                     First;
                     while NOT EOF do
                     begin
                          Edit;
                          cdsBlobs.Data := ServerReportes.GetFoto( dmCliente.Empresa, Fields[iColumna].AsInteger,
                                                                      sTipo );
                          Fields[iColumna].AsString := sDirectorio2 + Fields[iColumna].AsString + '.jpg';
                          FBlob := TBlobField( cdsBlobs.FieldByName( 'CampoBlob' ) );
                          FBlob.SaveToFile( sDirectorio + Fields[0].AsString + '.jpg');
                          POst;
                          Next;
                     end;
                  finally
                         MergeChangeLog;
                         First;
                         EnableControls;
                  end;
             end;
          finally
          end;
     end;
end;

procedure TdmReportes.GetFormaReporte( const eTipo : eTipoReporte );
begin
     inherited GetFormaReporte( eTipo );
     {$IFDEF TRESS_DELPHIXE5_UP}
     case eTipo of
          trPoliza : ShowFormaReporte( Poliza_DevEx, TPoliza_DevEx, SoloLectura, SoloImpresion );
          trPolizaConcepto : ShowFormaReporte( PolizaConcepto_DevEx, TPolizaConcepto_DevEx, SoloLectura, SoloImpresion );
     end;
     {$ELSE}
     case eTipo of
          trPoliza : ShowFormaReporte( Poliza, TPoliza, SoloLectura, SoloImpresion );
          trPolizaConcepto : ShowFormaReporte( PolizaConcepto, TPolizaConcepto, SoloLectura, SoloImpresion );
     end;
     {$ENDIF}
end;

{$ifdef COMENTARIOS}

function TdmReportes.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     Result := ZReportModulo.GetDerechosClasifi(eClasifi);
     case eClasifi of
          crEmpleados  : Result:= D_REPORTES_EMPLEADOS;
          crCursos     : Result:= D_REPORTES_CURSOS;
          crAsistencia : Result:= D_REPORTES_ASISTENCIA;
          crNominas    : Result:= D_REPORTES_NOMINA;
          crPagosIMSS  : Result:= D_REPORTES_IMSS;
          crConsultas  : Result:= D_REPORTES_CONSULTAS;
          crCatalogos  : Result:= D_REPORTES_CATALOGOS;
          crTablas     : Result:= D_REPORTES_TABLAS;
          crSupervisor : Result:= D_REPORTES_SUPERVISORES;
          crCafeteria  : Result:= D_REPORTES_CAFETERIA;
          crLabor      : Result:= D_REPORTES_LABOR;
          crMedico     : Result:= D_REPORTES_MEDICOS;
          {$ifdef CARRERA}
          crCarrera    : Result:= D_REPORTES_CARRERA;
          {$endif}
          crMigracion  : Result:= D_REPORTES_MIGRACION

          else Result:= 0;
     end;
end;
{$ENDIF}


function TdmReportes.ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;
begin
     Result := ServerReportes.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParamList, sError );
end;

function TdmReportes.ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;
begin
     //CV: 17-junio-2005
     //La implementacion de esta parte se realizara cuando cambie la llamada de DDiccionario.PruebaFormula
     Result := FALSE;
end;



procedure TdmReportes.CancelaCuentasPoliza;
begin
     if ( cdsCampoRep.ChangeCount > 0 ) then
        cdsCampoRep.CancelUpdates;
end;

procedure TdmReportes.cdsCampoRepAlCrearCampos(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsConceptosLookup.Conectar;
     if dmCatalogos.cdsConceptosLookup.Active then
        cdsCampoRep.CreateSimpleLookup(dmCatalogos.cdsConceptosLookup, 'CO_DESCRIP','CR_CALC');
end;


procedure TdmReportes.LlenaConceptos( eTiposConcepto : TiposConcepto );
const
     K_CARGO = 0;
     K_ABONO = 1;
     aAsiento: array[ false..true ] of integer = ( K_CARGO, K_ABONO );
var
   eTipo: eTipoConcepto;
begin
     with dmCatalogos.cdsConceptosLookup do
     begin
          try
             Conectar;
             if Active then
             begin
                  DisableControls;
                  First;
                  cdsCampoRep.DisableControls;
                  while not Eof do
                  begin
                       eTipo := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                       if ( eTipo in eTiposConcepto ) then
                       begin
                            if NOT cdsCampoRep.Locate('CR_CALC', FieldByName('CO_NUMERO').AsInteger, []) then
                            begin
                                 cdsCampoRep.Append;
                                 cdsCampoRep.FieldByName( 'CR_CALC' ).AsInteger := FieldByName( 'CO_NUMERO' ).AsInteger;
                                 cdsCampoRep.FieldByName( 'CR_OPER' ).AsInteger := aAsiento[ ( eTipo in [coDeduccion,coObligacion] ) ];
                                 cdsCampoRep.Post;
                            end;
                       end;
                       Next;
                  end;
             end;
          finally
            cdsCampoRep.EnableControls;
            EnableControls;
          end;
     end;
end;

procedure TdmReportes.BuscaConcepto;
var
   sKey, sDescription: String;
begin
     if dmCatalogos.cdsConceptosLookUp.Search( VACIO, sKey, sDescription ) then
     begin
          with cdsCampoRep do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CR_CALC' ).AsString := sKey;
          end;
     end;
end;


function TdmReportes.DirectorioPlantillas: string;
begin
     with dmCliente do
          Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
          
end;

function TdmReportes.GetListaEmpresasConfidenciales: string;
begin
     with dmSistema.cdsEmpresas do
     begin
          Conectar;
          if Active then
          begin
               Filtered := FALSE;
               Filter := Format( 'upper(CM_DATOS) = ''%s''', [Uppercase(dmCliente.cdsCompany.FieldbyName('CM_DATOS').AsString)] );
               try
                  Filtered := TRUE;
                  First;
                  if RecordCount > 1 then
                  begin

                       while NOT EOF do
                       begin
                            Result := ConcatString(Result, FieldbyName('CM_CODIGO').AsString, ',');
                            Next;
                       end;
                  end
                  else
                  begin
                       Result := dmCliente.GetDatosEmpresaActiva.Codigo
                  end;
               finally
                      Filtered := FALSE;
               end;
          end
          else
          begin
               raise Exception.Create( 'Error al Obtener la Información de la Empresa Activa' );
          end
     end;
end;

end.
