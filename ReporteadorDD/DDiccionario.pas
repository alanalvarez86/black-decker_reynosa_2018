unit DDiccionario;

interface
{$INCLUDE DEFINES.INC}                  
uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,Variants,
     ZetaClientDataSet,
     DBaseDiccionario,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonLists;

type

  TdmDiccionario = class(TdmBaseDiccionario)
    cdsCamposDefault: TZetaClientDataSet;
    cdsFiltrosDefault: TZetaClientDataSet;
    cdsOrdenDefault: TZetaClientDataSet;
    cdsClasificacionesLookup: TZetaLookupDataSet;
    procedure cdsCamposDefaultAlCrearCampos(Sender: TObject);
    procedure cdsCamposDefaultBeforePost(DataSet: TDataSet);
    procedure cdsCamposDefaultNewRecord(DataSet: TDataSet);
    procedure cdsCamposPorTablaNewRecord(DataSet: TDataSet);
    procedure cdsCamposPorTablaAlCrearCampos(Sender: TObject);
    procedure cdsClasificacionesAlAdquirirDatos(Sender: TObject);
    procedure cdsDatasetAfterDelete(DataSet: TDataSet);
    procedure cdsDatasetAlBorrar(Sender: TObject);
    procedure cdsDatasetAlEnviarDatos(Sender: TObject);
    procedure cdsDatasetAlModificar(Sender: TObject);
    procedure cdsEntidadesPorModuloAlCrearCampos(Sender: TObject);
    procedure cdsEntidadesPorModuloBeforePost(DataSet: TDataSet);
    procedure cdsEntidadesPorModuloNewRecord(DataSet: TDataSet);
    procedure cdsFiltrosDefaultAlCrearCampos(Sender: TObject);
    procedure cdsFiltrosDefaultNewRecord(DataSet: TDataSet);
    procedure cdsOrdenDefaultAlCrearCampos(Sender: TObject);
    procedure cdsOrdenDefaultNewRecord(DataSet: TDataSet);
    procedure cdsRelacionesBeforePost(DataSet: TDataSet);
    procedure cdsRelacionesNewRecord(DataSet: TDataSet);
    procedure cdsTablasPorClasificacionAlCrearCampos(Sender: TObject);
    procedure cdsTablasPorClasificacionBeforePost(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsCamposDefaultCalcFields(DataSet: TDataSet);
    procedure cdsFiltrosDefaultCalcFields(DataSet: TDataSet);
    procedure cdsOrdenDefaultCalcFields(DataSet: TDataSet);
    procedure cdsListasFijasAfterDelete(DataSet: TDataSet);

    {$ifdef ver130}
    procedure cdsDataSetReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsDatasetReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsClasificacionesNewRecord(DataSet: TDataSet);
    procedure cdsListasFijasNewRecord(DataSet: TDataSet);
    procedure cdsListasFijasValoresNewRecord(DataSet: TDataSet);
    procedure cdsModulosNewRecord(DataSet: TDataSet);
    procedure UsuarioAfterOpen(DataSet: TDataSet);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure cdsListasFijasValoresBeforePost(DataSet: TDataSet);
    procedure UsuarioBeforePost(DataSet: TDataSet);
    procedure cdsListasFijasValoresAlModificar(Sender: TObject);
    procedure cdsClasificacionesReconcileError(
      DataSet: TCustomClientDataSet; E: EReconcileError;
      UpdateKind: TUpdateKind; var Action: TReconcileAction);

    {$endif}
  private
    { Private declarations }
    {$ifdef CAROLINA}
    oLista : TStrings;
    procedure GeneraScript(DataSet: TZetaClientDataset; const sTabla, sIgnoreFields: string; const lEncimaLog: Boolean; const lConectar: Boolean = FALSE );
    {$endif}
    procedure AT_TIPOChange(Sender: TField);
    procedure AT_TITULOChange(Sender: TField);
    procedure MoverRegistro(cdsDataset: TZetaClientDataset; const iDelta: Integer; const sCodigo, sOrden: string);
    procedure AgregaCampo( Dataset:TClientDataset; const sCampo, sPrefijo: string );
    procedure OnCalcDefaults(DataSet: TDataSet; sCampo: string);

  public
    { Public declarations }
    {$IFDEF FALSE}
    procedure CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
    {$ENDIF}
    procedure SetLookupNames;override;
    procedure ClasifiMover(const iDelta: Integer);
    procedure ModuloMover(const iDelta: Integer);
    procedure CamposDefaultMover(const iDelta: Integer);
    procedure FiltrosDefaultMover(const iDelta: Integer);
    procedure OrdenDefaultMover(const iDelta: Integer);
    procedure RelacionesMover(const iDelta: Integer);
    procedure TablasPorModuloMover(const iDelta: Integer);
    procedure TablasPorClasificacionMover(const iDelta: Integer);
    procedure GrabaListasFijas(const sValores: string);
    procedure ConectarDatasetEntidades(const iEntidad: integer );
    procedure AgregarCamposFaltantes;
    procedure AgregarCampoDefault( const sCampo: string; const eTipo: eTipoDiccionario );
    procedure AgregarDescripcionDefault( const sCampo: string );
    procedure AgregarRelacionesDefault;
    function GetPrimaryKey: string;
    function ClasificacionSistema(RC_CODIGO:Integer):Boolean;
    {$ifdef CAROLINA}
    procedure GenerarScripts;
    procedure RevisaTablas;
    procedure CreaReportes;
    {$endif}

  end;

var
  dmDiccionario: TdmDiccionario;

implementation
uses FEditDiccion,
     FEditClasifiReporte_DevEx,
     FEditModulos_DevEx,
     FEditListasFijas_DevEx,
     FEditEntidades_DevEx,
     FEditRelaciones_DevEx, //Diccionario de Datos: Forma de Edicion para Tab: 'Relaciones'
     FEditCampos_DevEx, //Diccionario de Datos: Forma de Edicion para Tab: 'Campos de la Tabla' {1}
     FEditClasifiEntidades_DevEx,  //Diccionario de Datos: Forma de Edicion para Tab: 'Clasificaciones'
     FEditModulosEntidades_DevEx, //Diccionario de Datos: Forma de Edicion para Tab: 'Modulos'
     FEditDefaults_DevEx, //Diccionario de Datos: Forma de Edicion para Tab: 'Campos Default' {2}
     ZDiccionTools,
     ZBaseEdicion_DevEx,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaMsgDlg,
     ZReconcile,
     DReportes,
     DCliente,
     DBasicoCliente,
     DGlobal,
     DSistema,
     ZReportConst,
     ZGlobalTress,
     ZetaClientTools;

{$ifdef RDD}
const
     K_BITACORA_ERRORES = 'c:\temp\Errores.txt';
{$endif}

{$R *.DFM}
{$IFDEF FALSE}
procedure TdmDiccionario.CamposPorEntidad( const oEntidad: TipoEntidad; const lTodos: Boolean; oLista: TStrings );
var
   oCampo: TObjetoString;
begin
     oLista.Clear;
     GetListaDatosTablas( oEntidad, lTodos );
     with cdsBuscaPorTabla do
     begin
          while NOT EOF do
          begin
               oCampo := TObjetoString.Create;
               {$ifdef RDD}
               oCampo.Campo := FieldByName( 'AT_CAMPO' ).AsString;
               oLista.AddObject( FieldByName( 'AT_TITULO' ).AsString, oCampo );
               {$else}
               oCampo.Campo := FieldByName( 'DI_NOMBRE' ).AsString;
               oLista.AddObject( FieldByName( 'DI_TITULO' ).AsString, oCampo );
               {$endif}
               Next;
          end;
     end;
end;
{$ENDIF}

procedure TdmDiccionario.SetLookupNames;
begin
     inherited;
     //dmSeleccion.SetLookupNames;
end;

procedure TdmDiccionario.cdsDatasetAlModificar(Sender: TObject);
begin
     case eTipoDiccionario( TZetaClientDataset( Sender ).Tag ) of
          eClasifi:ZBaseEdicion_DevEx.ShowFormaEdicion( EditClasifiReporte_DevEx, TEditClasifiReporte_DevEx ); //Catalogo de clasificaciones
          eEntidades:ZBaseEdicion_DevEx.ShowFormaEdicion( EditEntidades_DevEx, TEditEntidades_DevEx ); //Diccionario de Datos
          eListasFijas: ZBaseEdicion_DevEx.ShowFormaEdicion ( EditListasFijas_DevEx, TEditListasFijas_DevEx );
          eListasFijasValor:;
          eAccesosClasifi:;
          eAccesosEntidades:;
          eModulos: ZBaseEdicion_DevEx.ShowFormaEdicion ( EditModulos_DevEx, TEditModulos_DevEx ); //Catalogos de Modulos
          eModulosPorEntidad:
          begin
               if ( EditModulosEntidades_DevEx = nil ) then
                  EditModulosEntidades_DevEx := TEditModulosEntidades_DevEx.Create( Application );
                  with EditModulosEntidades_DevEx do
                  begin
                       ShowModal;
                  end;
          end;
          eCamposPorEntidad:ZBaseEdicion_DevEx.ShowFormaEdicion( EditCampos_DevEx, TEditCampos_DevEx ); //Diccionario de Datos {1}
          eClasifiPorEntidad:
          begin
               if ( EditClasifiEntidades_DevEx = nil ) then
                  EditClasifiEntidades_DevEx := TEditClasifiEntidades_DevEx.Create( Application );

               with EditClasifiEntidades_DevEx do
               begin
                    ShowModal;
               end;
          end;
          eRelacionesPorEntidad:ZBaseEdicion_DevEx.ShowFormaEdicion( EditRelaciones_DevEx, TEditRelaciones_DevEx ); //Diccioario de Datos: Tab Relaciones
          eCamposDefault:
          begin
               if ( EditDefaults_DevEx = nil ) then
                  EditDefaults_DevEx := TEditDefaults_DevEx.Create( Application );

               with EditDefaults_DevEx do
               begin
                    Dataset:= cdsCamposDefault;
                    CampoEntidad:= 'RD_ENTIDAD';
                    CampoVersion := 'RD_VERSION';
                    ShowModal;
               end;
          end;
          eFiltrosDefault:
          begin
               if ( EditDefaults_DevEx = nil ) then
                  EditDefaults_DevEx := TEditDefaults_DevEx.Create( Application );

               with EditDefaults_DevEx do
               begin
                    Dataset:= cdsFiltrosDefault;
                    CampoEntidad:= 'RF_ENTIDAD';
                    CampoVersion := 'RF_VERSION';
                    ShowModal;
               end;
          end;
          eOrdenDefault:
          begin
               if ( EditDefaults_DevEx = nil ) then
                  EditDefaults_DevEx := TEditDefaults_DevEx.Create( Application );

               with EditDefaults_DevEx do
               begin
                    Dataset:= cdsOrdenDefault;
                    CampoEntidad:= 'RO_ENTIDAD';
                    CampoVersion:= 'RO_VERSION';
                    ShowModal;
               end;  
          end;
     end;
end;

procedure TdmDiccionario.cdsClasificacionesAlAdquirirDatos( Sender: TObject);
begin
     inherited;
     cdsClasificacionesLookup.Data := cdsClasificaciones.Data
end;

procedure TdmDiccionario.cdsClasificacionesReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     inherited;
     if UK_Violation( E ) then
     begin
          with DataSet do
               sError := Format( '� Clasificaci�n ya registrada con Descripci�n: %s !', [ FieldByName('RC_NOMBRE').AsString ] )
     end
     else
         sError := GetErrorDescription( E );
    cdsClasificaciones.ReconciliaError(DataSet,UpdateKind,E,'RC_CODIGO', sError );
    Action := raCancel;
end;


procedure TdmDiccionario.cdsDatasetAlEnviarDatos(Sender: TObject);

var
   ErrorCount, iPosicion : Integer;
   iPos : string;
   procedure UpdatePosicion(Dataset:TZetaClientDataSet;const sCampo: string);
   begin
        with Dataset do
        begin
             DisableControls;
             try
                Edit;
                FieldByName(sCampo).AsInteger := iPosicion;
                Post;
                MergeChangeLog;
             finally
                    EnableControls;
             end;
        end;
   end;
begin
     inherited;
     ErrorCount := 0;
     with TZetaClientDataset(Sender) do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile ( ServerDiccionario.GrabaDataset( dmCliente.Empresa, DeltaNull, Tag, ErrorCount, iPosicion ) ) then
               begin
                    case eTipoDiccionario( Tag ) of
                         eEntidades:
                         cdsEntidadesLookup.Data := cdsEntidades.Data;
                         eClasifi:
                         begin
                              with cdsClasificaciones do
                              begin
                                   DisableControls;
                                   iPos := cdsClasificaciones.FieldByName('RC_CODIGO').asString;
                                   try
                                      Refrescar;
                                      if (cdsClasificaciones.recordcount > 0 ) then
                                        cdsClasificaciones.Locate('RC_CODIGO',iPos,[]);
                                      //Last;
                                   finally
                                      EnableControls;
                                   end;
                              end;
                         end;
                         eModulos:
                         begin
                              with cdsModulos do
                              begin
                                   DisableControls;
                                   iPos := cdsModulos.FieldByName('MO_CODIGO').asString;
                                   try
                                      Refrescar;
                                      if (cdsModulos.recordcount > 0 ) then
                                        cdsModulos.Locate('MO_CODIGO',iPos,[]);
                                      //Last;
                                   finally
                                          EnableControls;
                                   end;
                              end;
                         end;

                         eCamposDefault: UpdatePosicion(cdsCamposDefault, 'RD_ORDEN');
                         eFiltrosDefault: UpdatePosicion(cdsFiltrosDefault,'RF_ORDEN');
                         eOrdenDefault: UpdatePosicion(cdsOrdenDefault, 'RO_ORDEN');
                    end;
               end;
          end;
     end;

end;

procedure TdmDiccionario.MoverRegistro( cdsDataset: TZetaClientDataset; const iDelta: Integer; const sCodigo, sOrden: string );
var
   iCodigo: Integer;
   iOrden: Integer;
begin
     with cdsDataset do
     begin
          iCodigo := FieldByName( sCodigo ).AsInteger;
          iOrden := FieldByName( sOrden ).AsInteger;
          cdsDataSet.Data := ServerDiccionario.OrdenCambiar( dmCliente.Empresa, cdsDataset.Tag, iCodigo, iOrden, ( iOrden + iDelta ) );
          Locate( sOrden, VarArrayOf( [ iOrden + iDelta ] ), [] );
     end;
end;

procedure TdmDiccionario.RelacionesMover(const iDelta: Integer );
begin
     MoverRegistro( cdsRelaciones,  iDelta, 'EN_CODIGO', 'RL_ORDEN' );
end;

procedure TdmDiccionario.TablasPorModuloMover(const iDelta: Integer);
begin
     MoverRegistro( cdsEntidadesPorModulo,  iDelta, 'MO_CODIGO', 'ME_ORDEN' );
end;

procedure TdmDiccionario.TablasPorClasificacionMover(const iDelta: Integer);
begin
     MoverRegistro( cdsTablasPorClasificacion,  iDelta, 'RC_CODIGO', 'CE_ORDEN' );

end;


procedure TdmDiccionario.CamposDefaultMover(const iDelta: Integer );
begin
     MoverRegistro( cdsCamposDefault, iDelta, 'EN_CODIGO', 'RD_ORDEN' );
end;

procedure TdmDiccionario.OrdenDefaultMover(const iDelta: Integer );
begin
     MoverRegistro( cdsOrdenDefault, iDelta, 'EN_CODIGO', 'RO_ORDEN' );
end;

procedure TdmDiccionario.FiltrosDefaultMover(const iDelta: Integer );
begin
     MoverRegistro( cdsFiltrosDefault, iDelta, 'EN_CODIGO', 'RF_ORDEN' );
end;

procedure TdmDiccionario.ClasifiMover( const iDelta: Integer );
begin
     MoverRegistro( cdsClasificaciones, iDelta, 'RC_CODIGO', 'RC_ORDEN' );
end;

procedure TdmDiccionario.ModuloMover( const iDelta: Integer );
begin
     MoverRegistro(cdsModulos,iDelta,'MO_CODIGO','MO_ORDEN');
end;

procedure TdmDiccionario.GrabaListasFijas( const sValores: string );
var
   ErrorCount : Integer;
begin
     inherited;
     ErrorCount := 0;
     with cdsListasFijas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;

          if ( ChangeCount > 0 ) or StrLleno( sValores ) then
          begin
               if Reconcile ( ServerDiccionario.GrabaListasFijas( dmCliente.Empresa, DeltaNull, sValores, ErrorCount,cdsListasFijas.FieldByName('LV_VERSION').AsInteger,dmCliente.Usuario ) )  then
               begin
                    cdsListasFijasValores.Refrescar;
               end;
          end;
     end;
end;


procedure TdmDiccionario.cdsDatasetAlBorrar(Sender: TObject);
begin
     inherited;
     with TZetaLookupDataset(Sender) do
     begin
          if CodigoSistema(FieldByName(LookupKeyField).AsInteger) then
          begin
             if ZetaMsgDlg.ConfirmaCambio( 'Esta tabla es de Sistema' + CR_LF + '� Desea Borrar Este Registro ?' ) then
                Delete;
          end
          else
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?' ) then
          begin
               Delete;
          end;
     end;
end;

procedure TdmDiccionario.cdsDatasetAfterDelete(DataSet: TDataSet);
begin
     inherited;
     with TZetaClientDataset(DataSet) do
     begin
          Enviar;
     end;
end;

procedure TdmDiccionario.ConectarDatasetEntidades(const iEntidad: integer );
 var Campos, Relaciones, Modulos, Clasifi, CamposDef, OrdenDef, FiltrosDef: OleVariant;
begin
     ServerDiccionario.GetEditEntidades( dmCliente.Empresa, iEntidad,
                                         Campos, Relaciones, Modulos, Clasifi, CamposDef, OrdenDef, FiltrosDef );
     cdsTablasPorClasificacion.Data := Clasifi;
     cdsEntidadesPorModulo.Data := Modulos;
     cdsRelaciones.Data := Relaciones;
     cdsRelaciones.IndexFieldNames := 'RL_ORDEN';
     cdsCamposDefault.Data := CamposDef;
     cdsFiltrosDefault.Data := FiltrosDef;
     cdsOrdenDefault.Data := OrdenDef;

     cdsCamposPorTabla.Data := Campos;

end;
procedure TdmDiccionario.cdsEntidadesPorModuloAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsModulos.Conectar;
     with cdsEntidadesPorModulo do
          if ( FindField('MO_NOMBRE') = NIL ) then
             CreateSimpleLookup(cdsModulosLookup, 'MO_NOMBRE','MO_CODIGO');
end;

procedure TdmDiccionario.cdsTablasPorClasificacionAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsClasificaciones.Conectar;
     with cdsTablasPorClasificacion do
     begin
          CreateSimpleLookup(cdsClasificacionesLookup,'RC_NOMBRE','RC_CODIGO');
          
     end;
end;

procedure TdmDiccionario.cdsRelacionesNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsRelaciones do
     begin
          FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
          FieldByName('RL_INNER').AsString := K_GLOBAL_NO;
          FieldByName('RL_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('RL_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD); //Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;
end;

procedure TdmDiccionario.cdsRelacionesBeforePost(DataSet: TDataSet);
begin
     inherited;
     with cdsRelaciones do
     begin
          if StrVacio( FieldByName('RL_CAMPOS').AsString ) then
          begin
               ZError('Relaciones', 'Se deben especificar los campos de la relaci�n', 0);
               FieldByName('RL_CAMPOS').FocusControl;
               Abort;
          end;
          //FieldByName('RL_ORDEN').AsInteger := RecordCount;
          if ( FieldByName('RL_ENTIDAD').AsInteger <> cdsEntidadesLookup.FieldByName('EN_CODIGO').AsInteger ) then
             cdsEntidadesLookup.Locate('EN_CODIGO',FieldByName('RL_ENTIDAD').AsInteger,[]);
          FieldByName('EN_TITULO').AsString := cdsEntidadesLookup.FieldByName('EN_TITULO').AsString;
          FieldByName('EN_TABLA').AsString  := cdsEntidadesLookup.FieldByName('EN_TABLA').AsString;
     end;
     UsuarioBeforePost(DataSet);
end;

procedure TdmDiccionario.cdsEntidadesPorModuloNewRecord(DataSet: TDataSet);
begin
     inherited;
     with DataSet do
     begin
          FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
     end;

end;

procedure TdmDiccionario.cdsEntidadesPorModuloBeforePost(DataSet: TDataSet);
begin
     inherited;
     with cdsEntidadesPorModulo do
     begin
          FieldByName('ME_ORDEN').AsInteger := RecordCount + 1;
          FieldByName('ME_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;

procedure TdmDiccionario.cdsTablasPorClasificacionBeforePost(
  DataSet: TDataSet);
begin
     inherited;
     with cdsTablasPorClasificacion do
     begin
          FieldByName('CE_ORDEN').AsInteger := RecordCount + 1;
          FieldByName('CE_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
     end;
end;


procedure tdmDiccionario.AgregarCamposFaltantes;
 var
    oLista: TStrings;
    i: integer;

begin
     cdsCamposPorTabla.Data := ServerDiccionario.AgregarCamposFaltantes( dmCliente.Empresa,
                                                                         cdsCamposPorTabla.Data,
                                                                         cdsEntidades.FieldByName('EN_CODIGO').AsInteger );

     oLista:= TStringList.Create;
     oLista.Commatext := cdsEntidades.FieldByName('EN_PRIMARY').AsString;

     for i:=0 to oLista.Count -1 do
     begin
          AgregaCampo( cdsCamposDefault, oLista[i], 'RD' );
          AgregaCampo( cdsOrdenDefault, oLista[i], 'RO' );
          AgregaCampo( cdsFiltrosDefault, oLista[i], 'RF' );
     end;

     if StrLleno( cdsEntidades.FieldByName('EN_ATDESC').AsString ) then
        AgregaCampo( cdsCamposDefault, cdsEntidades.FieldByName('EN_ATDESC').AsString, 'RD' );

     cdsCamposDefault.Enviar;
     cdsOrdenDefault.Enviar;
     cdsFiltrosDefault.Enviar;
end;

procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     inherited;
     {eListasFijas,
                       eListasFijasValor,
                       eAccesosClasifi,
                       eAccesosEntidades,
                       ,
                       ,
                       ,}

     cdsClasificaciones.Tag := Ord( eClasifi );
     cdsModulos.Tag := Ord( eModulos );
     cdsEntidadesPorModulo.Tag := Ord( eModulosPorEntidad);
     cdsEntidades.Tag := Ord( eEntidades );
     cdsCamposDefault.Tag := Ord( eCamposDefault );
     cdsFiltrosDefault.Tag := Ord( eFiltrosDefault );
     cdsOrdenDefault.Tag := Ord( eOrdenDefault );
     cdsTablasPorClasificacion.Tag := Ord( eCamposPorEntidad );
     cdsRelaciones.Tag := Ord( eRelacionesPorEntidad);
     cdsTablasPorClasificacion.Tag := Ord( eClasifiPorEntidad );
     cdsCamposPorTabla.Tag := Ord(eCamposPorEntidad);
     cdsListasFijas.Tag := Ord(eListasFijas);

     {$ifdef CAROLINA}
     oLista := TStringlist.Create;
     {$endif}

end;

procedure TdmDiccionario.cdsCamposDefaultAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsEntidadesLookup.Conectar;
     with cdsCamposDefault do
     begin
          CreateSimpleLookup(cdsEntidadesLookup, 'EN_TITULO','RD_ENTIDAD');
          CreateCalculated( 'AT_TABLA', ftString, 255 );
     end;
end;

procedure TdmDiccionario.cdsFiltrosDefaultAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsEntidadesLookup.Conectar;
     with cdsFiltrosDefault do
     begin
          CreateSimpleLookup(cdsEntidadesLookup, 'EN_TITULO','RF_ENTIDAD');
          CreateCalculated( 'AT_TABLA', ftString, 255 );
     end;
end;

procedure TdmDiccionario.cdsOrdenDefaultAlCrearCampos(Sender: TObject);
begin
     inherited;
     cdsEntidadesLookup.Conectar;
     with cdsOrdenDefault do
     begin
          CreateSimpleLookup(cdsEntidadesLookup, 'EN_TITULO','RO_ENTIDAD');
          CreateCalculated( 'AT_TABLA', ftString, 255 );
     end;

end;

procedure TdmDiccionario.cdsFiltrosDefaultNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsFiltrosDefault do
     begin
          FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
          FieldByName('RF_ENTIDAD').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
           FieldByName('RF_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;

end;

procedure TdmDiccionario.cdsOrdenDefaultNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsOrdenDefault do
     begin
          FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
          FieldByName('RO_ENTIDAD').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
           FieldByName('RO_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;

end;

procedure TdmDiccionario.cdsCamposDefaultNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsCamposDefault do
     begin
          FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
          FieldByName('RD_ENTIDAD').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
          FieldByName('RD_VERSION').AsInteger :=  Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;
end;

procedure TdmDiccionario.cdsCamposDefaultBeforePost(DataSet: TDataSet);
begin
     inherited;
     with Dataset do
     begin

          if StrVacio( FieldByName('AT_CAMPO').AsString ) then
          begin
               ZError('Datos default', 'Falta especificar el nombre del campo', 0);
               FieldByName('AT_CAMPO').FocusControl;
               Abort;
          end;
     end;
     UsuarioBeforePost(DataSet);
end;

procedure TdmDiccionario.cdsCamposPorTablaAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsCamposPorTabla do
     begin
          FieldByName('AT_TIPO').OnChange := AT_TIPOChange;
          FieldByName('AT_TITULO').OnChange := AT_TITULOChange;
     end;
end;

procedure TdmDiccionario.AT_TIPOChange(Sender: TField);
begin
     with cdsCamposPortabla do
     begin
          if ( State = dsBrowse ) then
             Edit;
          try
             FieldByName('AT_TIPO').OnChange := NIL;
             DefaultDato(cdsCamposPortabla, eTipoGlobal(FieldByName('AT_TIPO').AsInteger),0);
          finally
                 FieldByName('AT_TIPO').OnChange := AT_TIPOChange;
          end;
     end;
end;

procedure TdmDiccionario.AT_TITULOChange(Sender: TField);

  function QuitaPalabras( sValue : String ) : String;
  begin
       Result := sValue;
       Result := UpperCase(QuitaAcentos(Result));
       Result := StrTransAll(Result, ' DE ', ' ' );
       Result := StrTransAll(Result, ' CON ', ' ' );
       Result := StrTransAll(Result, ' LAS ', ' ' );
       Result := StrTransAll(Result, ' QUE ', ' ' );
       Result := StrTransAll(Result, ' PARA ', ' ' );
       Result := StrTransAll(Result, ' DEL ', ' ' );
       Result := StrTransAll(Result, ' EL ', ' ' );
       Result := StrTransAll(Result, ' LOS ', ' ' );
       Result := StrTransAll(Result, ' LA ', ' ' );
       Result := StrTransAll(Result, ' EN ', ' ' );
       Result := StrTransAll(Result, '?', ' ' );
       Result := StrTransAll(Result, '�', ' ' );
       Result := StrTransAll(Result, '/', ' ' );
       Result := StrTransAll(Result, '#', ' ' );
       Result := StrTransAll(Result, ':', ' ' );
  end;

var sTitulo, sTCorto,sCampo, sDescrip, sClaves : string;
begin
     sTitulo := Sender.AsString;

     with Sender.Dataset do
     begin
          sTitulo := FieldByName('AT_TITULO').AsString;
          sTCorto := FieldByName('AT_TCORTO').AsString;
          sCampo := FieldByName('AT_CAMPO').AsString;
          sDescrip := FieldByName('AT_DESCRIP').AsString;
          sClaves := FieldByName('AT_CLAVES').AsString;

     end;

     if StrVacio( sTCorto ) or ( Trim(sTCorto) = Trim( sCampo ) ) then
     begin
          if sTitulo = 'C�digo de Tabla' then
             sTCorto := 'C�digo'
          else if sTitulo = 'Descripci�n en Ingl�s' then
               sTCorto := 'Ingl�s'
          else if sTitulo = 'N�mero de Empleado' then
               sTCorto := 'N�mero'
          else if sTitulo = 'N�mero de Expediente' then
               sTCorto := 'N�mero'
          else if sTitulo = 'N�mero de Uso General' then
               sTCorto := 'N�mero'
          else if sTitulo = 'Observaciones' then
               sTCorto := 'Observaciones'
          else if sTitulo = 'Texto de Uso General' then
               sTCorto := 'Texto'
          else if sTitulo = 'Usuario que Modific�' then
               sTCorto := 'Usuario'
          else sTCorto := sTitulo;
     end;
     if StrVacio( sClaves ) OR ( Trim(sClaves) = Trim( sCampo ) )then
        sClaves := QuitaPalabras( sTitulo );

     if StrVacio( sDescrip ) OR ( Trim(sDescrip) = Trim( sCampo ) )then
        sDescrip := sTitulo;

     with Sender.Dataset do
     begin
          FieldByName('AT_TCORTO').AsString := sTCorto;
          FieldByName('AT_DESCRIP').AsString := sDescrip;
          FieldByName('AT_CLAVES').AsString := sClaves;
     end;
end;


{$ifdef ver130}
procedure TdmDiccionario.cdsDataSetReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmDiccionario.cdsDatasetReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
begin
     {$ifdef CAROLINA}
     Action := raSkip;
     if oLista = nil THEN oLista :=  TStringList.Create;
     if FileExists( K_BITACORA_ERRORES ) then
        oLista.LoadFromFile(K_BITACORA_ERRORES);
     with cdsEntidades do
          oLista.Add(Format( 'Tabla # %d : %s', [FieldByName('EN_CODIGO').AsInteger,FieldByName('EN_TABLA').AsString] ) );
     oLista.Add( E.Message );
     oLista.SaveToFile(K_BITACORA_ERRORES);
     {$else}
        Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);

     {$endif}


end;


procedure TdmDiccionario.AgregaCampo( Dataset:TClientDataset; const sCampo, sPrefijo: string );
begin
     with Dataset do
     begin
          if NOT Locate( 'AT_CAMPO',sCampo, [] ) then
          begin
               Append;
               FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
               FieldByName(sPrefijo+'_ENTIDAD').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
               FieldByName('AT_CAMPO').AsString := sCampo;
               Post;
          end;
     end;
end;

procedure TdmDiccionario.AgregarDescripcionDefault( const sCampo: string );
begin
     with cdsCamposPorTabla do
     begin
          if ( eTipoRango( FieldByName('AT_TRANGO').AsInteger ) = rRangoEntidad ) and
              ( FieldByName('AT_ENTIDAD').AsInteger> 0 ) then
          begin
               if cdsEntidadesLookup.Locate('EN_CODIGO',FieldByName('AT_ENTIDAD').AsInteger,[]) and
                  StrLleno( cdsEntidadesLookup.FieldByName('EN_ATDESC').AsString ) then
               begin
                    if NOT cdsCamposDefault.Locate( 'AT_CAMPO',cdsEntidadesLookup.FieldByName('EN_ATDESC').AsString, [] ) then
                    begin
                         cdsCamposDefault.Append;
                         cdsCamposDefault.FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
                         cdsCamposDefault.FieldByName('RD_ENTIDAD').AsInteger := FieldByName('AT_ENTIDAD').AsInteger;
                         cdsCamposDefault.FieldByName('AT_CAMPO').AsString := cdsEntidadesLookup.FieldByName('EN_ATDESC').AsString;
                         cdsCamposDefault.Post;
                         cdsCamposDefault.Enviar;
                    end;


               end;
          end
     end;
end;

{$ifdef CAROLINA}
procedure TdmDiccionario.GenerarScripts;
 var iPos: integer;
begin
     //De uso general
     GeneraScript( cdsClasificaciones, 'R_CLASIFI', '', TRUE, TRUE );
     GeneraScript( cdsModulos, 'R_MODULO', '', TRUE, TRUE );
     GeneraScript( cdsListasFijas, 'R_LISTAVAL', 'LV_DUMMY', TRUE, TRUE );
     GeneraScript( cdsListasFijasValores, 'R_VALOR', '', TRUE, TRUE );
     ///Que dependen de las entidades.
     cdsEntidades.Conectar;
     //cdsEntidades.Filter := 'EN_CODIGO IN(273,274,275,276,277,278)';
     //cdsEntidades.Filtered := TRUE;

     GeneraScript( cdsEntidades, 'R_ENTIDAD', 'numtabla', TRUE, FALSE );
     with cdsEntidades do
     begin
          First;
          while NOT EOF do
          begin
               ConectarDatasetEntidades( FieldByName('EN_CODIGO').AsINteger );
               GeneraScript( cdsCamposPorTabla, 'R_ATRIBUTO', '', BOF);
               GeneraScript( cdsCamposDefault,'R_DEFAULT', 'EN_TITULO,AT_TABLA', BOF );
               GeneraScript( cdsFiltrosDefault, 'R_FILTRO', 'EN_TITULO,AT_TABLA', BOF );
               GeneraScript( cdsOrdenDefault, 'R_ORDEN', 'EN_TITULO,AT_TABLA', BOF );
               GeneraScript( cdsEntidadesPorModulo, 'R_MOD_ENT', 'MO_NOMBRE', BOF );
               GeneraScript( cdsTablasPorClasificacion, 'R_CLAS_ENT', 'RC_NOMBRE', BOF );
               cdsRelaciones.IndexFieldNames := 'EN_CODIGO;RL_ORDEN';
               {with cdsRelaciones do
               begin
                    First;
                    iPos := 1;
                    while NOT EOF do
                    begin
                         Edit;
                         cdsRelaciones.FieldByName('RL_ORDEN').AsInteger := iPos;
                         Post;
                         Inc(iPos);
                         Next;
                    end;
                    First;
               end;}
               GeneraScript( cdsRelaciones, 'R_RELACION', 'EN_TITULO,EN_TABLA', BOF );
               Next;
          end;
     end;

end;

procedure TdmDiccionario.GeneraScript( DataSet: TZetaClientDataset; const sTabla, sIgnoreFields: string; const lEncimaLog: Boolean; const lConectar: Boolean = FALSE );
 var
    oLista: TStrings;
    sFields,SValues, sArchivo : string;
    i: integer;
begin
     inherited;
     oLista:= TStringList.Create;
     sArchivo := Format( 'd:\temp\RDD\%s.sql',[sTabla] );

     if not lEncimaLog then
        if FileExists( sArchivo ) then
           oLista.LoadFromFile( sArchivo );

     with Dataset do
     begin
          if lConectar then
             Conectar;
          for i:= 0 to FieldCount - 1 do
          begin
               if Pos(Fields[i].FieldName , sIgnoreFields ) = 0 then
               begin
                    sFields := ConcatString( sFields, Fields[i].FieldName, ',' );
               end;
          end;
          //oLista.Add( '-- Tabla: ' + sTabla );
          //oLista.Add( '-- Registros agregados: ' + IntToStr(RecordCount) );

          First;
          while NOT EOF do
          begin
               for i:= 0 to FieldCount - 1 do
               begin
                    if Pos( Fields[i].FieldName , sIgnoreFields) = 0 then
                    begin
                         if Fields[i].DataType in [ftString	,ftMemo] then
                            sValues := ConcatString( sValues, EntreComillas( Fields[i].AsString), ',' )
                         else if Fields[i].DataType in [ftDateTime	,ftDate] then
                            sValues := ConcatString( sValues, DateToStrSQLC( Fields[i].AsDateTime), ',' )
                         else
                             sValues := ConcatString( sValues, Fields[i].AsString, ',' );
                    end;
               end;
               oLista.Add(Format( 'INSERT INTO %s( %s ) VALUES(%s)', [sTabla, sFields, sValues] ));
               //oLista.Add('GO');
               //oLista.Add('');
               sValues := VACIO;
               Next;
          end;
          oLista.SaveToFile( sArchivo );
     end;
end;

procedure TdmDiccionario.RevisaTablas;
procedure AgregaDescripcionTabla;
 begin
     with cdsEntidades do
          oLista.Add(Format( 'Tabla # %d : %s', [FieldByName('EN_CODIGO').AsInteger,FieldByName('EN_TABLA').AsString] ) );
 end;

 procedure RegistroCero(Dataset: TDAtaset; const sDes: string);
 begin
      if Dataset.RecordCount = 0 then
      begin
           AgregaDescripcionTabla;
           oLista.Add( sDes + ' 0 REGISTROS');
      end;
 end;

 var
   sPrimary: string;
begin
     oLista:= TStringList.Create;

     with cdsEntidades do
     begin
          Conectar;
          while NOT EOF do
          begin
               try
                  sPrimary := GetPrimaryKey;

                  if ( sPrimary <> FieldByName('EN_PRIMARY').AsString ) then
                  begin
                       Edit;
                       FieldByName('EN_PRIMARY').AsString := sPrimary;
                       Enviar;
                  end;

                  ConectarDatasetEntidades( FieldByName('EN_CODIGO').AsInteger );
                  AgregarCamposFaltantes;
                  AgregarRelacionesDefault;

                  RegistroCero( cdsTablasPorClasificacion, 'Clasificaciones');
                  RegistroCero( cdsEntidadesPorModulo, 'M�dulos' );
                  RegistroCero( cdsRelaciones, 'Relaciones' );
                  RegistroCero( cdsCamposDefault, 'Campos default' );
                  RegistroCero( cdsFiltrosDefault, 'Filtros default' );
                  RegistroCero( cdsOrdenDefault, 'Orden default' );

               except
                     On Error:Exception do
                     begin
                          AgregaDescripcionTabla;
                          oLista.Add(Error.Message);
                     end;
               end;
               Next;
               Application.ProcessMessages;
          end;
     end;
     oLista.SaveToFile(K_BITACORA_ERRORES);
end;

procedure TdmDiccionario.CreaReportes;
 var iPos, iSubPos: integer;
begin

     iPos := 0;
     iSubPos := 0;
     with cdsEntidades, dmReportes do
     begin
          Conectar;
          while NOT EOF do
          begin
               try
                  ConectarDatasetEntidades(FieldByName('EN_CODIGO').AsInteger);
                  cdsEditReporte.Conectar;
                  cdsEditReporte.Append;
                  cdsEditReporte.FieldByName('RE_TIPO').AsInteger := Ord(trListado);
                  cdsEditReporte.FieldByName('RE_ENTIDAD').AsInteger := FieldByName('EN_CODIGO').AsInteger;
                  cdsEditReporte.FieldByName('RE_CLASIFI').AsInteger := 26;
                  cdsEditReporte.FieldByName('RE_NOMBRE').AsString := PadL(FieldByName('EN_CODIGO').AsString,5) + ': '+ cdsEntidades.FieldByName('EN_TABLA').AsString;


                  cdsCampoRep.EmptyDataset;
                  cdsCampoRep.Conectar;

                  with cdsCamposDefault do
                  begin
                       while NOT EOF do
                       begin
                            cdsCampoRep.Append;
                            cdsCampoRep.FieldByName('CR_TIPO').AsInteger := Ord( tcCampos );
                            cdsCampoRep.FieldByName('CR_POSICIO').AsInteger := iPos;
                            cdsCampoRep.FieldByName('CR_SUBPOS').AsInteger := iSubPos;
                            cdsCampoRep.FieldByName('CR_TABLA').AsInteger := FieldByName('RD_ENTIDAD').AsInteger;
                            cdsCampoRep.FieldByName('CR_CLASIFI').AsInteger := FieldByName('RD_ENTIDAD').AsInteger;
                            if cdsEntidadesLookup.Locate( 'EN_CODIGO', FieldByName('RD_ENTIDAD').AsInteger, [] ) then
                               cdsCampoRep.FieldByName('CR_FORMULA').AsString := cdsEntidadesLookup.FieldByName('EN_TABLA').AsString + '.' + FieldByName('AT_CAMPO').AsString;
                            cdsCampoRep.FieldByName('CR_CALC').AsInteger := 0;
                            cdsCampoRep.FieldByName('CR_ANCHO').AsInteger := 10;
                            cdsCampoRep.FieldByName('CR_MASCARA').AsString := '';
                            cdsCampoRep.FieldByName('CR_TFIELD').AsInteger := 0;
                            cdsCampoRep.FieldByName('CR_OPER').AsInteger := 1;
                            cdsCampoRep.FieldByName('CR_REQUIER').AsString := '';
                            cdsCampoRep.FieldByName('CR_TITULO').AsString := FieldByName('AT_CAMPO').AsString;
                            cdsCampoRep.Post;
                            Inc( iPos);
                            Next;
                       end;
                  end;

                       cdsCampoRep.Append;
                       cdsCampoRep.FieldByName('CR_TIPO').AsInteger := Ord( tcGrupos );
                       cdsCampoRep.FieldByName('CR_POSICIO').AsInteger := 0;
                       cdsCampoRep.FieldByName('CR_SUBPOS').AsInteger := iSubPos;
                       cdsCampoRep.FieldByName('CR_TABLA').AsInteger := FieldByName('EN_CODIGO').AsInteger;
                       cdsCampoRep.FieldByName('CR_CLASIFI').AsInteger := FieldByName('EN_CODIGO').AsInteger;
                       cdsCampoRep.FieldByName('CR_FORMULA').AsString := '';
                       cdsCampoRep.FieldByName('CR_CALC').AsInteger := 0;
                       cdsCampoRep.FieldByName('CR_ANCHO').AsInteger := 10;
                       cdsCampoRep.FieldByName('CR_MASCARA').AsString := '';
                       cdsCampoRep.FieldByName('CR_TFIELD').AsInteger := 0;
                       cdsCampoRep.FieldByName('CR_OPER').AsInteger := 1;
                       cdsCampoRep.FieldByName('CR_REQUIER').AsString := '';
                       cdsCampoRep.FieldByName('CR_TITULO').AsString := 'Grupo Nivel Empresa';
                       cdsCampoRep.Post;

                  {with cdsFiltrosDefault do
                  begin
                       while NOT EOF do
                       begin
                            cdsCampoRep.Append;
                            cdsCampoRep.FieldByName('CR_TIPO').AsInteger := Ord( tcFiltro );
                            cdsCampoRep.FieldByName('CR_POSICIO').AsInteger := iPos;
                            cdsCampoRep.FieldByName('CR_SUBPOS').AsInteger := iSubPos;
                            cdsCampoRep.FieldByName('CR_TABLA').AsInteger := FieldByName('EN_CODIGO').AsInteger;
                            cdsCampoRep.FieldByName('CR_CLASIFI').AsInteger := FieldByName('EN_CODIGO').AsInteger;
                            cdsCampoRep.FieldByName('CR_FORMULA').AsString := cdsEntidades.FieldByName('EN_TABLA').AsString + '.' + FieldByName('AT_CAMPO').AsString;
                            cdsCampoRep.FieldByName('CR_CALC').AsInteger := 0;
                            cdsCampoRep.FieldByName('CR_ANCHO').AsInteger := 10;
                            cdsCampoRep.FieldByName('CR_MASCARA').AsString := '';
                            cdsCampoRep.FieldByName('CR_TFIELD').AsInteger := 0;
                            cdsCampoRep.FieldByName('CR_OPER').AsInteger := 1;
                            cdsCampoRep.FieldByName('CR_REQUIER').AsString := '';
                            cdsCampoRep.FieldByName('CR_TITULO').AsString := FieldByName('AT_CAMPO').AsString;
                            cdsCampoRep.Post;
                            Inc( iPos);
                            Next;
                       end;
                  end;}

                  with cdsOrdenDefault do
                  begin
                       while NOT EOF do
                       begin
                            cdsCampoRep.Append;
                            cdsCampoRep.FieldByName('CR_TIPO').AsInteger := Ord( tcOrden );
                            cdsCampoRep.FieldByName('CR_POSICIO').AsInteger := iPos;
                            cdsCampoRep.FieldByName('CR_SUBPOS').AsInteger := iSubPos;
                            cdsCampoRep.FieldByName('CR_TABLA').AsInteger := FieldByName('Ro_ENTIDAD').AsInteger;
                            cdsCampoRep.FieldByName('CR_CLASIFI').AsInteger := FieldByName('Ro_ENTIDAD').AsInteger;
                            if cdsEntidadesLookup.Locate( 'EN_CODIGO', FieldByName('RO_ENTIDAD').AsInteger, [] ) then
                               cdsCampoRep.FieldByName('CR_FORMULA').AsString := cdsEntidadesLookup.FieldByName('EN_TABLA').AsString + '.' + FieldByName('AT_CAMPO').AsString;
                            cdsCampoRep.FieldByName('CR_CALC').AsInteger := 0;
                            cdsCampoRep.FieldByName('CR_ANCHO').AsInteger := 10;
                            cdsCampoRep.FieldByName('CR_MASCARA').AsString := '';
                            cdsCampoRep.FieldByName('CR_TFIELD').AsInteger := 0;
                            cdsCampoRep.FieldByName('CR_OPER').AsInteger := 1;
                            cdsCampoRep.FieldByName('CR_REQUIER').AsString := '';
                            cdsCampoRep.FieldByName('CR_TITULO').AsString := FieldByName('AT_CAMPO').AsString;
                            cdsCampoRep.Post;
                            Inc( iPos);
                            Next;
                       end;
                  end;

                  cdsEditReporte.Enviar;


               except
                     On Error:Exception do
                     begin
                          oLista.Add(Error.Message);
                     end;
               end;
               Next;
               Application.ProcessMessages;
          end;
     end;
     oLista.SaveToFile(K_BITACORA_ERRORES);
end;
{$endif}

procedure TdmDiccionario.AgregarCampoDefault( const sCampo: string; const eTipo: eTipoDiccionario );
begin
     case eTipo of
          eCamposDefault:
          begin
               AgregaCampo( cdsCamposDefault, sCampo, 'RD' );
               cdsCamposDefault.Enviar;
          end;
          eFiltrosDefault:
          begin
               AgregaCampo( cdsFiltrosDefault, sCampo, 'RF' );
               cdsFiltrosDefault.Enviar;
          end;
          eOrdenDefault:
          begin
               AgregaCampo( cdsOrdenDefault, sCampo, 'RO' );
               cdsOrdenDefault.Enviar;
          end;
          eRelacionesPorEntidad:
          begin
               cdsRelaciones.Append;
               cdsRelaciones.FieldByName('RL_ENTIDAD').AsInteger := cdsCamposPorTabla.FieldByName('AT_ENTIDAD').AsInteger;
               cdsRelaciones.FieldByName('RL_CAMPOS').AsString := sCampo;
               cdsRelaciones.Modificar;
          end;
     end;
end;

procedure TdmDiccionario.AgregarRelacionesDefault;
begin
     with cdsCamposPOrTabla do
     begin
          try
             DisableControls;
             cdsRelaciones.DisableControls;
             First;
             while NOT eof do
             begin
                  if NOT cdsRelaciones.Locate('RL_ENTIDAD',cdsCamposPorTabla.FieldByName('AT_ENTIDAD').AsInteger, []) and
                     ( cdsCamposPorTabla.FieldByName('AT_ENTIDAD').AsInteger <> enEntidadVACIA ) and
                     ( cdsCamposPorTabla.FieldByName('AT_ENTIDAD').AsInteger <> cdsCamposPorTabla.FieldByName('EN_CODIGO').AsInteger) then
                  begin
                       cdsRelaciones.Append;
                       cdsRelaciones.FieldByName('RL_ENTIDAD').AsInteger := FieldByName('AT_ENTIDAD').AsInteger;
                       cdsRelaciones.FieldByName('RL_CAMPOS').AsString := FieldByName('AT_CAMPO').AsString;
                       cdsRelaciones.Post;
                  end;
                  Next;
             end;
             cdsRelaciones.Enviar;
          finally
                 EnableControls;
                 cdsRelaciones.EnableControls;
          end;
     end;
end;



procedure TdmDiccionario.cdsCamposPorTablaNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsCamposPorTabla do
     begin
          FieldByName('EN_CODIGO').AsInteger := cdsEntidades.FieldByName('EN_CODIGO').AsInteger;
          FieldByName('AT_CONFI').AsString :=  K_GLOBAL_NO;
          FieldByName('AT_SISTEMA').AsString :=  K_GLOBAL_NO;
          FieldByName('AT_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('AT_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;

end;

procedure TdmDiccionario.OnCalcDefaults(DataSet: TDataSet; sCampo: string);
begin
     with cdsEntidadesLookup do
     begin
          if Locate('EN_CODIGO',Dataset.FieldByName(sCampo).AsInteger, []) then
             Dataset.FieldByName('AT_TABLA').AsString :=  FieldByName('EN_TABLA').AsString;
     end;
end;

procedure TdmDiccionario.cdsCamposDefaultCalcFields(DataSet: TDataSet);
begin
     inherited;
     OnCalcDefaults(Dataset, 'RD_ENTIDAD');
end;

procedure TdmDiccionario.cdsFiltrosDefaultCalcFields(DataSet: TDataSet);
begin
     inherited;
     OnCalcDefaults(Dataset, 'RF_ENTIDAD');
end;

procedure TdmDiccionario.cdsOrdenDefaultCalcFields(DataSet: TDataSet);
begin
     inherited;
     OnCalcDefaults(Dataset, 'RO_ENTIDAD');
end;

procedure TdmDiccionario.cdsListasFijasAfterDelete(DataSet: TDataSet);
begin
     inherited;
     GrabaListasFijas(VACIO);

end;

function TdmDiccionario.GetPrimaryKey: string;
begin
     Result := ServerDiccionario.GetPrimaryKey(dmCliente.Empresa, cdsEntidades.FieldByName('EN_TABLA').AsString);
end;



procedure TdmDiccionario.cdsClasificacionesNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsClasificaciones do
     begin
          FieldByName ('RC_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('RC_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;
end;

procedure TdmDiccionario.cdsListasFijasNewRecord(DataSet: TDataSet);
begin
  inherited;
     with cdsListasFijas do
     begin
          FieldByName('LV_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('LV_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;
end;

procedure TdmDiccionario.cdsListasFijasValoresNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsListasFijasValores do
     begin
          FieldByName('VL_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;
end;

procedure TdmDiccionario.cdsModulosNewRecord(DataSet: TDataSet);
begin
     inherited;
     with cdsModulos do
     begin
          FieldByName('MO_ACTIVO').AsString := K_GLOBAL_SI;
          FieldByName('MO_VERSION').AsInteger := Global.GetGlobalInteger(K_GLOBAL_VERSION_RDD);//Global ReadOnly
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;//Usuario activo ReadOnly
     end;
end;

procedure TdmDiccionario.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;


procedure TdmDiccionario.UsuarioAfterOpen(DataSet: TDataSet);
begin
  inherited;
     dmSistema.cdsUsuarios.Conectar;         
     DataSet.FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
end;

function TdmDiccionario.ClasificacionSistema(RC_CODIGO:Integer):Boolean;
begin
     Result := ( ( RC_CODIGO = crSupervisor ) or ( RC_CODIGO = crClasificacionSistema ) );
end;


procedure TdmDiccionario.cdsListasFijasValoresBeforePost(
  DataSet: TDataSet);
begin
  inherited;
     DataSet.FieldByName('VL_VERSION').AsInteger := cdsListasFijas.FieldByName('LV_VERSION').AsInteger;
end;

procedure TdmDiccionario.UsuarioBeforePost(DataSet: TDataSet);
begin
     inherited;
     DataSet.FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
end;

procedure TdmDiccionario.cdsListasFijasValoresAlModificar(Sender: TObject);
{
var
   ErrorCount : Integer;
}
begin
     inherited;
     {ErrorCount := 0;
     with cdsListasFijas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;

          if ( ChangeCount > 0 ) then
          begin
               if Reconcile ( ServerDiccionario.GrabaListasFijas( dmCliente.Empresa, Delta, NULL, ErrorCount ) ) then
               begin
               end;
          end;
     end; }
end;



end.
