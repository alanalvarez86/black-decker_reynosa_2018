unit FClasifiReporte_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  Vcl.StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  dxSkinsCore,dxSkinsDefaultPainters, dxSkinscxPCPainter, cxButtons;

type
  TClasifiReporte_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    RC_ORDEN: TcxGridDBColumn;
    RC_NOMBRE: TcxGridDBColumn;
    PanelMover: TPanel;
    Subir: TcxButton;
    Bajar: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  ClasifiReporte_DevEx: TClasifiReporte_DevEx;

implementation

uses
    ZetaCommonClasses,
    DCliente,
    dDiccionario;
{$R *.dfm}

{ TClasifiReporte }

procedure TClasifiReporte_DevEx.Agregar;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Agregar;
end;

procedure TClasifiReporte_DevEx.Borrar;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Borrar;
end;

procedure TClasifiReporte_DevEx.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsClasificaciones.Conectar;
          DataSource.DataSet:= cdsClasificaciones;
     end;
end;

procedure TClasifiReporte_DevEx.Modificar;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Modificar;

end;

procedure TClasifiReporte_DevEx.Refresh;
begin
     inherited;
     dmDiccionario.cdsClasificaciones.Refrescar;
end;

procedure TClasifiReporte_DevEx.SubirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmDiccionario.ClasifiMover(TControl(Sender).Tag);
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TClasifiReporte_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext:= H_Catalogo_Clasificaciones;
  ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
end;

procedure TClasifiReporte_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.ApplyBestFit(); //ES NECESARIO?
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  inherited;
end;

end.
