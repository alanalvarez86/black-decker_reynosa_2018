inherited ListasFijas_DevEx: TListasFijas_DevEx
  Left = 482
  Top = 251
  Caption = 'Listas de valores'
  ClientHeight = 272
  ClientWidth = 410
  ExplicitWidth = 410
  ExplicitHeight = 272
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 410
    ExplicitWidth = 410
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 151
      ExplicitWidth = 151
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 145
        ExplicitLeft = 3
      end
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 410
    Height = 253
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'LV_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LV_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 259
        Visible = True
      end>
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 410
    Height = 253
    ExplicitWidth = 410
    ExplicitHeight = 253
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object LV_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'LV_CODIGO'
        Width = 85
      end
      object LV_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'LV_NOMBRE'
        Width = 242
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 56
    Top = 176
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
