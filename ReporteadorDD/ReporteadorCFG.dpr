program ReporteadorCFG;

uses
  MidasLib,
  Forms,
  ZetaClientTools,
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  FTressShell in 'FTressShell.pas' {TressShell},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DCliente in 'DCliente.pas' {dmCliente: TDataModule},
  dBaseGlobal in '..\DataModules\dBaseGlobal.pas',
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DBaseReportes in '..\DataModules\DBaseReportes.pas' {dmBaseReportes: TDataModule},
  DBaseSistema in '..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  FSistBaseEditEmpresas_DevEx in '..\Sistema\FSistBaseEditEmpresas_DevEx.pas' {SistBaseEditEmpresas_DevEx},
  ZBaseEditAccesos in '..\Tools\ZBaseEditAccesos.pas' {ZetaEditAccesos},
  FSistEditUsuarioSuscribe in '..\Sistema\FSistEditUsuarioSuscribe.pas' {FormaSuscribeUsuario},
  FEditDiccion in '..\Catalogos\FEditDiccion.pas' {EditDiccion},
  ZBaseConsulta in '..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseDlgModal_DevEx in '..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  DProcesos in 'DProcesos.pas' {dmProcesos: TDataModule},
  FDialogoLista_DevEx in 'FDialogoLista_DevEx.pas' {DialogoLista_DevEx},
  FEditModulosEntidades_DevEx in 'FEditModulosEntidades_DevEx.pas' {EditModulosEntidades_DevEx},
  FEditCampos_DevEx in 'FEditCampos_DevEx.pas' {EditCampos_DevEx},
  ZBaseEdicion_DevEx in '..\Tools\ZBaseEdicion_DevEx.pas' {BaseEdicion_DevEx},
  ZBasicoNavBarShell in '..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  FSistEditGrupos_DevEx in 'FSistEditGrupos_DevEx.pas' {SistEditGrupos_DevEx},
  ZBaseGridLectura_DevEx in '..\Tools\ZBaseGridLectura_DevEx.pas' {BaseGridLectura_DevEx},
  FWizRDDExportarGridSelect_DevEx in 'Wizards\FWizRDDExportarGridSelect_DevEx.pas',
  ZBaseSelectGrid_DevEx in '..\Tools\ZBaseSelectGrid_DevEx.pas',
  ZWizardBasico in '..\Tools\ZWizardBasico.pas' {WizardBasico},
  ZcxWizardBasico in '..\Tools\ZcxWizardBasico.pas' {CXWizardBasico},
  ZcxBaseWizard in '..\Tools\ZcxBaseWizard.pas' {cxBaseWizard},
  ZcxBaseWizardFiltroRDD in 'Wizards\ZcxBaseWizardFiltroRDD.pas' {BaseWizardFiltroRDD};

{$R *.RES}

{$R ..\Traducciones\Spanish.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.Title := 'Configurador de Reporteador';
  Application.HelpFile := 'ReporteadorCFG.chm';     //ReporteadorCFG.chm
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( False ) then
       begin
            Show;
            Update;
            WindowState := wsMaximized;  //Maximizar despues del update para que tome en cuenta que se escondio el Menu
            BeforeRun;
            Application.Run;
       end
       else
       begin
            Free;
       end;
  end;
end.

