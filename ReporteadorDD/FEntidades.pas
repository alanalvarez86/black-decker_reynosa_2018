unit FEntidades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  StdCtrls;

type
  TEntidades = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    eBuscar: TEdit;
    Label1: TLabel;
    BBusca: TSpeedButton;
    procedure eBuscarChange(Sender: TObject);
  private
    { Private declarations }
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Entidades: TEntidades;

implementation

uses
    dDiccionario;
{$R *.dfm}

{ TClasifiReporte }

procedure TEntidades.Agregar;
begin
     inherited;
     dmDiccionario.cdsEntidades.Agregar;
end;

procedure TEntidades.Borrar;
begin
     inherited;
     dmDiccionario.cdsEntidades.Borrar;
end;

procedure TEntidades.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsEntidades.Conectar;
          DataSource.DataSet:= cdsEntidades;
     end;
end;

procedure TEntidades.Modificar;
begin
     inherited;
     dmDiccionario.cdsEntidades.Modificar;

end;

procedure TEntidades.Refresh;
begin
     inherited;
     dmDiccionario.cdsEntidades.Refrescar;
end;

procedure TEntidades.eBuscarChange(Sender: TObject);
begin
     inherited;

     with dmDiccionario.cdsEntidades do
     begin
          BBusca.Down := Length(eBuscar.Text)>0;
          Filter := Format( '(NumTabla like ''%0:s'') or (UPPER(EN_TITULO) like ''%0:s'') or (UPPER(EN_TABLA) like ''%0:s'')', ['%' + UpperCase(eBuscar.Text) + '%']);
          Filtered := eBuscar.Text > '';
     end;
end;

end.
