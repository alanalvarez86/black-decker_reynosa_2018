inherited EditDefaults: TEditDefaults
  Left = 248
  Top = 272
  Caption = 'Defaults'
  ClientHeight = 171
  ClientWidth = 400
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 48
    Top = 67
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campo:'
    WordWrap = True
  end
  object Label1: TLabel [1]
    Left = 54
    Top = 44
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tabla:'
  end
  object Label12: TLabel [2]
    Left = 46
    Top = 91
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Versi'#243'n:'
    Enabled = False
  end
  object Label4: TLabel [3]
    Left = 41
    Top = 112
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
    Enabled = False
  end
  object US_CODIGO: TZetaDBTextBox [4]
    Left = 91
    Top = 111
    Width = 200
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 135
    Width = 400
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 225
    end
    inherited Cancelar: TBitBtn
      Left = 310
    end
  end
  inherited PanelSuperior: TPanel
    Width = 400
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 400
    TabOrder = 0
    inherited ValorActivo2: TPanel [1]
      Width = 74
    end
    inherited ValorActivo1: TPanel [2]
    end
  end
  object RD_ENTIDAD: TZetaDBKeyLookup [8]
    Left = 91
    Top = 40
    Width = 300
    Height = 21
    TabOrder = 2
    TabStop = True
    WidthLlave = 60
    DataField = 'RD_ENTIDAD'
    DataSource = DataSource
  end
  object AT_CAMPO: TDBEdit [9]
    Left = 91
    Top = 63
    Width = 110
    Height = 21
    CharCase = ecUpperCase
    DataField = 'AT_CAMPO'
    DataSource = DataSource
    TabOrder = 3
  end
  object RD_VERSION: TDBEdit [10]
    Left = 91
    Top = 87
    Width = 110
    Height = 21
    DataField = 'RD_VERSION'
    DataSource = DataSource
    MaxLength = 30
    ReadOnly = True
    TabOrder = 5
  end
  inherited DataSource: TDataSource
    Left = 332
    Top = 1
  end
end
