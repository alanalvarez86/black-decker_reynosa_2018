inherited dmDiccionario: TdmDiccionario
  OldCreateOrder = True
  Left = 528
  Top = 60
  Height = 678
  Width = 752
  inherited cdsDiccion: TZetaClientDataSet
    Top = 48
  end
  inherited cdsBuscaPorCampo: TZetaClientDataSet
    Left = 32
    Top = 160
  end
  inherited cdsBuscaPorTabla: TZetaClientDataSet
    Left = 32
    Top = 224
  end
  inherited cdsGlobal: TZetaClientDataSet
    Left = 32
    Top = 96
  end
  inherited cdsClasificaciones: TZetaLookupDataSet
    AfterOpen = UsuarioAfterOpen
    BeforePost = UsuarioBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnNewRecord = cdsClasificacionesNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 40
  end
  inherited cdsTablasPorClasificacion: TZetaClientDataSet
    BeforePost = cdsTablasPorClasificacionBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnNewRecord = cdsEntidadesPorModuloNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlCrearCampos = cdsTablasPorClasificacionAlCrearCampos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 344
  end
  inherited cdsRelaciones: TZetaClientDataSet
    AfterOpen = UsuarioAfterOpen
    BeforePost = cdsRelacionesBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnNewRecord = cdsRelacionesNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 392
  end
  inherited cdsCamposPorTabla: TZetaClientDataSet
    AfterOpen = UsuarioAfterOpen
    BeforePost = UsuarioBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnNewRecord = cdsCamposPorTablaNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 440
  end
  inherited cdsLookupGeneral: TZetaLookupDataSet
    Left = 164
    Top = 104
  end
  inherited cdsClasifiDerechos: TZetaClientDataSet
    Left = 276
    Top = 216
  end
  inherited cdsDatosDefault: TZetaClientDataSet
    Left = 164
    Top = 48
  end
  inherited cdsListasFijas: TZetaLookupDataSet
    AfterOpen = UsuarioAfterOpen
    BeforePost = UsuarioBeforePost
    AfterDelete = cdsListasFijasAfterDelete
    OnNewRecord = cdsListasFijasNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlBorrar = cdsDatasetAlBorrar
    AlModificar = cdsDatasetAlModificar
    Left = 276
    Top = 48
  end
  inherited cdsModulos: TZetaLookupDataSet
    AfterOpen = UsuarioAfterOpen
    BeforePost = UsuarioBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnNewRecord = cdsModulosNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlBorrar = cdsDatasetAlBorrar
    AlModificar = cdsDatasetAlModificar
    LookupName = 'M'#243'dulos'
    LookupDescriptionField = 'MO_NOMBRE'
    Left = 372
    Top = 112
  end
  inherited cdsListasFijasValores: TZetaClientDataSet
    BeforePost = cdsListasFijasValoresBeforePost
    OnNewRecord = cdsListasFijasValoresNewRecord
    AlModificar = cdsListasFijasValoresAlModificar
    Left = 276
    Top = 104
  end
  inherited cdsFunciones: TZetaClientDataSet
    Left = 168
    Top = 160
  end
  inherited cdsEntidades: TZetaLookupDataSet
    AfterOpen = UsuarioAfterOpen
    AfterDelete = cdsDatasetAfterDelete
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlBorrar = cdsDatasetAlBorrar
    AlModificar = cdsDatasetAlModificar
    Left = 156
    Top = 312
  end
  inherited cdsEntidadesLookup: TZetaLookupDataSet
    Left = 156
    Top = 368
  end
  inherited cdsEntidadesPorModulo: TZetaClientDataSet
    BeforePost = cdsEntidadesPorModuloBeforePost
    OnNewRecord = cdsEntidadesPorModuloNewRecord
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlCrearCampos = cdsEntidadesPorModuloAlCrearCampos
    AlModificar = cdsDatasetAlModificar
    Left = 372
  end
  object cdsCamposDefault: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = UsuarioAfterOpen
    BeforePost = cdsCamposDefaultBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnCalcFields = cdsCamposDefaultCalcFields
    OnNewRecord = cdsCamposDefaultNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlAdquirirDatos = cdsDiccionAlAdquirirDatos
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlCrearCampos = cdsCamposDefaultAlCrearCampos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 152
  end
  object cdsFiltrosDefault: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = UsuarioAfterOpen
    BeforePost = cdsCamposDefaultBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnCalcFields = cdsFiltrosDefaultCalcFields
    OnNewRecord = cdsFiltrosDefaultNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlCrearCampos = cdsFiltrosDefaultAlCrearCampos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 200
  end
  object cdsOrdenDefault: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = UsuarioAfterOpen
    BeforePost = cdsCamposDefaultBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnCalcFields = cdsOrdenDefaultCalcFields
    OnNewRecord = cdsOrdenDefaultNewRecord
    OnReconcileError = cdsDatasetReconcileError
    AlEnviarDatos = cdsDatasetAlEnviarDatos
    AlCrearCampos = cdsOrdenDefaultAlCrearCampos
    AlModificar = cdsDatasetAlModificar
    Left = 372
    Top = 248
  end
  object cdsClasificacionesLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RC_ORDEN'
    Params = <>
    LookupName = 'Clasificaciones'
    LookupDescriptionField = 'RC_NOMBRE'
    LookupKeyField = 'RC_CODIGO'
    Left = 476
    Top = 56
  end
end
