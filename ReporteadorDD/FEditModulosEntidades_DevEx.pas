unit FEditModulosEntidades_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FDialogoLista, StdCtrls, CheckLst, Buttons, ExtCtrls,
  FDialogoLista_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, Vcl.ImgList, cxButtons;

type
  TEditModulosEntidades_DevEx = class(TDialogoLista_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EditModulosEntidades_DevEx: TEditModulosEntidades_DevEx;

implementation
uses
    DDiccionario;
{$R *.dfm}

procedure TEditModulosEntidades_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     with dmDiccionario do
     begin
          DataSetChecked:= cdsEntidadesPorModulo;
          DataSetInicial:= cdsModulos;
     end;
end;

end.
