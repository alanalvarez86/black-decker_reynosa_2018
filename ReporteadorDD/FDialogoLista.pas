unit FDialogoLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, CheckLst, Buttons, ExtCtrls, ZetaClientDataset;

type
  TDialogoLista = class(TZetaDlgModal)
    cbLista: TCheckListBox;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FDataSetChecked: TZetaClientDataset;
    FDataSetInicial: TZetaLookupDataset;
  protected
    property DataSetChecked: TZetaClientDataset read FDataSetChecked write  FDataSetChecked;
    property DataSetInicial: TZetaLookupDataset read FDataSetInicial write  FDataSetInicial;
  public
    { Public declarations }
  end;

var
  DialogoLista: TDialogoLista;

implementation
uses
    ZetaDialogo,
    DDiccionario;

{$R *.dfm}

procedure TDialogoLista.FormShow(Sender: TObject);
begin
     inherited;
     cbLista.Items.Clear;
     with DataSetInicial do
     begin
          First;
          while NOT EOF do
          begin
               cbLista.Items.AddObject(FieldByName(LookupDescriptionField).AsString, TObject(FieldByName(LookupKeyField).AsInteger));
               cbLista.Checked[cbLista.Items.Count-1] := FDataSetChecked.Locate(LookupKeyField,FieldByName(LookupKeyField).AsInteger,[]);
               Next;
          end;
     end;

end;

procedure TDialogoLista.OKClick(Sender: TObject);
 var
    i: integer;
begin
     inherited;
     FDataSetChecked.EmptyDataSet;
     FDataSetChecked.MergeChangeLog;

     for i:=0 to cbLista.Items.Count - 1 do
     begin
          with FDataSetChecked do
          begin
               if cbLista.Checked[i] then
               begin
                    Append;
                    FieldByName(FDataSetInicial.LookupKeyField).AsInteger := Integer( cbLista.Items.Objects[i] );
                    Post;
               end;
          end;
     end;
     FDataSetChecked.Enviar;
end;

end.
