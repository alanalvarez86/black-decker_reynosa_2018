inherited ListasFijas: TListasFijas
  Left = 482
  Top = 251
  Caption = 'Listas de valores'
  ClientWidth = 405
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 405
    inherited ValorActivo2: TPanel
      Width = 146
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 405
    Height = 252
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'LV_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'C'#243'digo'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LV_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 259
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 56
    Top = 176
  end
end
