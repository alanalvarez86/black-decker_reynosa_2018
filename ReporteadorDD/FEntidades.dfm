inherited Entidades: TEntidades
  Left = 482
  Top = 251
  Caption = 'Diccionario de Datos'
  ClientWidth = 470
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 470
    inherited ValorActivo2: TPanel
      Width = 211
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 50
    Width = 470
    Height = 221
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'EN_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = '# Tabla'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_TITULO'
        Title.Caption = 'Nombre'
        Width = 259
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'EN_TABLA'
        Title.Caption = 'Tabla'
        Width = 103
        Visible = True
      end>
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 19
    Width = 470
    Height = 31
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 33
      Height = 13
      Caption = '&Buscar'
      FocusControl = eBuscar
    end
    object BBusca: TSpeedButton
      Left = 236
      Top = 1
      Width = 27
      Height = 27
      Hint = 'Buscar Reporte'
      AllowAllUp = True
      GroupIndex = 1
      Flat = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      OnClick = eBuscarChange
    end
    object eBuscar: TEdit
      Left = 48
      Top = 4
      Width = 185
      Height = 21
      TabOrder = 0
      OnChange = eBuscarChange
    end
  end
  inherited DataSource: TDataSource
    Left = 56
    Top = 176
  end
end
