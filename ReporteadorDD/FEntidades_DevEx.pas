unit FEntidades_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsulta, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid, Buttons,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  ZetaSmartLists, cxButtons, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter;

type
  TEntidades_DevEx = class(TBaseGridLectura_DevEx)
    ZetaDBGrid1: TZetaDBGrid;
    Panel1: TPanel;
    eBuscar: TEdit;
    Label1: TLabel;
    BBusca: TcxButton;
    EN_CODIGO: TcxGridDBColumn;
    EN_TITULO: TcxGridDBColumn;
    EN_TABLA: TcxGridDBColumn;
    procedure eBuscarChange(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
     procedure FiltraReporte;
  protected
           procedure Connect; override;
           procedure Refresh; override;
           procedure Agregar;override;
           procedure Borrar; override;
           procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Entidades_DevEx: TEntidades_DevEx;

implementation

uses
    dDiccionario, ZetaCommonClasses;
{$R *.dfm}

{ TClasifiReporte }

procedure TEntidades_DevEx.Agregar;
begin
     inherited;
     dmDiccionario.cdsEntidades.Agregar;
end;

procedure TEntidades_DevEx.BBuscaClick(Sender: TObject);
begin
  inherited;
  FiltraReporte;
end;

procedure TEntidades_DevEx.FiltraReporte;
var FilterString: String;
begin
  if (Length(eBuscar.Text)>0) and (BBusca.Down = False) then
    with ZetaDBGridDBTableView.DataController do
      begin
        BeginUpdate;
        Filter.Active := False;
        with Filter.Root do
        begin
          Clear;
        end;
        Filter.Active := True;
        EndUpdate;
      end
  else
    eBuscarChange(ZetaDBGridDBTableView);
end;

procedure TEntidades_DevEx.FormCreate(Sender: TObject);
begin
  inherited;
  HelpContext:= H_Diccionario_Datos;
  ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
end;

procedure TEntidades_DevEx.FormShow(Sender: TObject);
begin
  ApplyMinWidth;
  CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0], 0 ,'' , SkCount );
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  ZetaDBGridDBTableView.ApplyBestFit();
  inherited;
end;

procedure TEntidades_DevEx.Borrar;
begin
     inherited;
     dmDiccionario.cdsEntidades.Borrar;
end;

procedure TEntidades_DevEx.Connect;
begin
     inherited;
     with dmDiccionario do
     begin
          cdsEntidades.Conectar;
          DataSource.DataSet:= cdsEntidades;
     end;
end;

procedure TEntidades_DevEx.Modificar;
begin
     inherited;
     dmDiccionario.cdsEntidades.Modificar;

end;

procedure TEntidades_DevEx.Refresh;
begin
     inherited;
     dmDiccionario.cdsEntidades.Refrescar;
end;

procedure TEntidades_DevEx.eBuscarChange(Sender: TObject);
begin
     inherited;
     with dmDiccionario.cdsEntidades do
     begin
          BBusca.Down := Length(eBuscar.Text)>0;
          Filter := Format( '(NumTabla like ''%0:s'') or (UPPER(EN_TITULO) like ''%0:s'') or (UPPER(EN_TABLA) like ''%0:s'')', ['%' + UpperCase(eBuscar.Text) + '%']);
          Filtered := eBuscar.Text > '';
     end;
end;

end.
