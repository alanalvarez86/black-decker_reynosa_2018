{$HINTS OFF}
unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa principal de Tress                ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, Buttons, ExtCtrls, ToolWin, Menus, Mask, DBCtrls, Db, ImgList,
     StdCtrls, DBGrids, ActnList,
     ZBaseConsulta,
     ZBaseShell,
     ZetaClientDataset,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDespConsulta,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaMessages,
     ZetaTipoEntidad,
     ZBaseNavBarShell,
     dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, dxRibbonSkins,
  dxSkinsdxRibbonPainter, dxSkinsdxNavBarPainter, dxSkinsdxBarPainter,
  cxLocalization, dxBar, dxSkinsForm, dxNavBar, cxButtons, cxClasses,
  dxRibbon, cxPC, cxContainer, cxEdit, cxImage, dxGDIPlusClasses, cxLabel,
  cxTextEdit, cxGroupBox, dxBarExtItems, dxNavBarBase, dxNavBarCollns,
  cxTreeView, cxCustomData, cxStyles, cxTL, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxMaskEdit, cxDropDownEdit, ZetaCXStateComboBox,
  dxBarExtDBItems, dxStatusBar, dxRibbonStatusBar, cxBarEditItem, cxButtonEdit,
  ZetaStateComboBox, ZetaSmartLists, System.Actions, dxBarBuiltInMenu,
  dxRibbonCustomizationForm, dxCustomTileControl, dxTileControl, Registry, ShlObj, ComObj, ActiveX, StrUtils;

type
  TTressShell = class(TBaseNavBarShell)
    _Emp_Primero: TAction;
    _Emp_Anterior: TAction;
    _Emp_Siguiente: TAction;
    _Emp_Ultimo: TAction;
    _Emp_Alta: TAction;
    _Emp_Baja: TAction;
    _Emp_Reingreso: TAction;
    _Emp_Cambio_Salario: TAction;
    _Emp_Cambio_Turno: TAction;
    _Emp_Cambio_Puesto: TAction;
    _Emp_Cambio_Area: TAction;
    _Emp_Cambio_Contrato: TAction;
    _Emp_Cambio_Prestacion: TAction;
    _Emp_Cambios_Multiples: TAction;
    _Emp_Vacaciones: TAction;
    _Emp_Cerrar_Vacaciones: TAction;
    _Emp_Incapacidad: TAction;
    _Emp_Permiso: TAction;
    _Emp_Kardex: TAction;
    _Emp_Curso: TAction;
    _Emp_Comidas: TAction;
    _Emp_Caseta: TAction;
    _Emp_Recalcular_Integrados: TAction;
    _Emp_Promediar_Variables: TAction;
    _Emp_Salario_Global: TAction;
    _Emp_Aplicar_Tabulador: TAction;
    _Emp_Vacaciones_Globales: TAction;
    _Emp_Cancelar_Vacaciones: TAction;
    _Emp_CierreGlobal_Vacaciones: TAction;
    _Emp_Aplicar_Eventos: TAction;
    _Emp_Importar_Kardex: TAction;
    _Emp_Cancelar_Kardex: TAction;
    _Emp_Curso_Global: TAction;
    _Emp_Transferencia: TAction;
    _Asi_Autorizar: TAction;
    _Asi_Horarios_Temporales: TAction;
    _Asi_POLL: TAction;
    _Asi_Procesar_Tarjetas: TAction;
    _Asi_Prenomina: TAction;
    _Asi_ExtrasPermisosGlobales: TAction;
    _Asi_Registros_Automaticos: TAction;
    _Asi_Corregir_Fechas: TAction;
    _Asi_Recalcular_Tarjetas: TAction;
    _IMSS_PagoNuevo: TAction;
    _IMSS_BorrarPago: TAction;
    _IMSS_CalcularPago: TAction;
    _IMSS_CalcularRecargos: TAction;
    _IMSS_CalcularTasa: TAction;
    _IMSS_RevisarSUA: TAction;
    _IMSS_ExportarSUA: TAction;
    _IMSS_CalcularPrima: TAction;
    _Sist_EmpresaNueva: TAction;
    _Sist_BorrarBajas: TAction;
    _Sist_BorrarNominas: TAction;
    _Sist_Borrar_Tarjetas: TAction;
    _Sist_Borrar_POLL: TAction;
    _Sist_Borrar_Bitacora: TAction;
    _Per_Primero: TAction;
    _Per_Anterior: TAction;
    _Per_Siguiente: TAction;
    _Per_Ultimo: TAction;
    _Per_NominaNueva: TAction;
    _Per_BorrarNominaActiva: TAction;
    _Per_ExcepcionDiaHora: TAction;
    _Per_ExcepcionMonto: TAction;
    _Per_ExcepcionesGlobales: TAction;
    _Per_Liquidacion: TAction;
    _Per_PagoRecibos: TAction;
    _Per_CalcularNomina: TAction;
    _Per_AfectarNomina: TAction;
    _Per_FoliarRecibos: TAction;
    _Per_ImprimirListado: TAction;
    _Per_ImprimirRecibos: TAction;
    _Per_GenerarPoliza: TAction;
    _Per_ImprimirPoliza: TAction;
    _Per_ExportarPoliza: TAction;
    _Per_Desafectar: TAction;
    _Per_LimpiarAcumulados: TAction;
    _Per_RecalcularAcumulados: TAction;
    _Per_ImportarMovimientos: TAction;
    _Per_ExportarMovimientos: TAction;
    _Per_ImportarPagoRecibos: TAction;
    _Per_RefoliarRecibos: TAction;
    _Per_CopiarNomina: TAction;
    _Per_CalcularDiferencias: TAction;
    _Per_PagarFuera: TAction;
    _Per_CancelarPasadas: TAction;
    _Per_LiquidacionGlobal: TAction;
    _Per_CalcularNetoBruto: TAction;
    _Per_RastrearCalculo: TAction;
    _Per_DefinirPeriodos: TAction;
    _Per_CalcularAguinaldos: TAction;
    _Per_ImprimirAguinaldos: TAction;
    _Per_PagarAguinaldos: TAction;
    _Per_CalcularReparto: TAction;
    _Per_ImprimirReparto: TAction;
    _Per_PagarReparto: TAction;
    _Per_CierreAhorros: TAction;
    _Per_CierrePrestamos: TAction;
    _Per_CalcularPTU: TAction;
    _Per_ImprimirPTU: TAction;
    _Per_PagarPTU: TAction;
    _Per_CalcularDeclaracion: TAction;
    _Per_ImprimirDeclaracion: TAction;
    _Per_CalcularComparativo: TAction;
    _Per_ImprimirComparativo: TAction;
    _Per_PagarComparativo: TAction;
    _Emp_EntregaHerramienta: TAction;
    _Emp_EntregaGlobal: TAction;
    _Emp_RegresarHerramienta: TAction;
    _Emp_Entregar_Herramienta: TAction;
    _Emp_Regresar_Herramienta: TAction;
    _Sist_Borrar_Herramientas: TAction;
    _A_Preferencias: TAction;
    _E_BuscarEmpleado: TAction;
    _E_CambiarEmpleado: TAction;
    _V_ArbolOriginal: TAction;
    _Per_CreditoAplicadoMensual: TAction;
    _Emp_Importar_Altas: TAction;
    _Emp_Renumerar: TAction;
    _Emp_Banca_Elec: TAction;
    _Emp_Credito_Infonavit: TAction;
    _Emp_Permiso_Global: TAction;
    _Emp_Prestamos: TAction;
    _Per_CalculaRetroactivos: TAction;
    _Emp_Borrar_Curso_Global: TAction;
    _Per_DeclaracionAnual: TAction;
    _Per_ImprimirDeclaracionAnual: TAction;
    _Emp_Registro_Comidas_Grupales: TAction;
    _Emp_Corregir_Fechas_Caf: TAction;
    _Asi_AutorizacionesXAprobar: TAction;
    _Sist_Actualizar_NumeroTarjeta: TAction;
    _Asi_FechaLimite: TAction;
    _Emp_Importar_Asistencia_Sesiones: TAction;
    _Emp_Cancelar_Cierre_Vacaciones: TAction;
    _Emp_Certificaciones: TAction;
    _Asi_IntercambioFestivo: TAction;
    _Asi_CancelarExcepcionesFestivos: TAction;
    _Emp_Cancelar_Permisos_Globales: TAction;
    _Emp_Importar_Aho_Pre: TAction;
    _Emp_Recalcula_Saldos_Vaca: TAction;
    _Emp_Cambio_plaza: TAction;
    _Emp_Cambio_Tipo_Nomina: TAction;
    _Emp_Certificaciones_Empleado: TAction;
    _Emp_Empleados_Certificados: TAction;
    _Per_AjusteRetFonacot: TAction;
    _Per_CalcularPagoFonacot: TAction;
    _Sist_Enrolamiento_Masivo: TAction;
    _Sist_Importar_Enrolamiento_Masivo: TAction;
    _IMSS_AjusteRetInf: TAction;
    _Emp_Curso_Prog_Global: TAction;
    _Emp_Cancelar_Curso_Prog_Global: TAction;    
    _Asi_Ajust_Incapa_Cal: TAction;
    _Emp_Tarjetas_Gasolina: TAction;
    _Emp_Tarjetas_Despensa: TAction;
    _Emp_Foliar_capacitaciones: TAction;
    _Per_CierreDeclaracionAnual: TAction;
    _Asi_AutorizacionPreNomina: TAction;
    _Asi_RegistrarExcepcionesFestivo: TAction;
    _IMSS_ValidaMovIDSE: TAction;
    _Per_TransferenciasCosteo: TAction;
    _Emp_Importar_SGM: TAction;
    _Asi_ImportarClasificacionesTemporales: TAction;
    _Per_Calculo_Costeo: TAction;
    _Per_Cancelacion_Transferencias: TAction;
    _Sist_Depura_BitacoraBiometrico: TAction;
    _Sist_Asigna_NumBiometricos: TAction;
    _Emp_Renovacion_SGM: TAction;
    _Emp_Borrar_SGM: TAction;
    _Sist_Asigna_GrupoTerminales: TAction; // SYNERGY
    _Per_Previo_ISR: TAction;
    _Per_Imprimir_PrevioISR: TAction;
    _Emp_Revisar_Datos_STPS: TAction;
    _Emp_Reiniciar_Capacitaciones_STPS: TAction;
    _Emp_ImportarOrganigrama: TAction;
    Image24_StatusEmpleado: TcxImageList;
    EFechaSistema: TdxBar;
    dxBarControlContainerItem_FechaSistema: TdxBarControlContainerItem;
    PanelFecha: TPanel;
    SistemaFechaLBL_DevEx: TLabel;
    SistemaFechaDia_DevEx: TLabel;
    SistemaFechaZF_DevEx: TZetaFecha;
    TabArchivo_btnPreferencias: TdxBarLargeButton;
    PanelValorAEmpleado: TPanel;
    PanelValorAEmpleado_2: TPanel;
    EmpleadoPrettyName_DevEx: TLabel;
    PanelValorEmpleado_1: TPanel;
    EmpleadoLBL_DevEx: TLabel;
    btnBuscarEmp_DevEx: TcxButton;
    btnEmplPrimero_DevEx: TcxButton;
    btnEmpAnterior_DevEx: TcxButton;
    btnEmpSiguiente_DevEx: TcxButton;
    btnEmpUltimo_DevEx: TcxButton;
    dxBarControlContainerItem_AnoSistema: TdxBarControlContainerItem;
    PanelAnio: TPanel;
    grpAno: TcxGroupBox;
    AnioLbl_DevEx: TLabel;
    ImagenStatus_DevEx: TcxImage;
    EmpleadoNumeroCB_DevEx: TcxStateComboBox;
    NavegacionCB_DevEx: TcxStateComboBox;
    TabEmpleado: TdxRibbonTab;
    BarraEmpMovimientos: TdxBar;
    BarraEmpCambios: TdxBar;
    BarraEmpSalarios: TdxBar;
    BarraEmpTargetas: TdxBar;
    BarraEmpAdicionales: TdxBar;
    EmpMovAlta: TdxBarLargeButton;
    EmpMovBolBaja: TdxBarLargeButton;
    EmpMovReingreso: TdxBarLargeButton;
    EmpMovOtros: TdxBarSubItem;
    dxBarSubItem2: TdxBarSubItem;
    EmpImportarAltas: TdxBarButton;
    EmpTransferencia: TdxBarButton;
    Emp_Remunerar: TdxBarButton;
    EmpCambioTurno: TdxBarLargeButton;
    EmpCambioTipoNomina: TdxBarLargeButton;
    EmpKardesGral: TdxBarLargeButton;
    EmpMultiplesCambios: TdxBarLargeButton;
    EmpCambioContrato: TdxBarLargeButton;
    EmpCambioPuesto: TdxBarLargeButton;
    EmpCambioPrestaciones: TdxBarLargeButton;
    EmpcambioArea: TdxBarLargeButton;
    EmpMasivosSub: TdxBarSubItem;
    EmpAplicaEventosMasivos: TdxBarButton;
    empImportakardexMasivos: TdxBarButton;
    empCancelakardexMasivo: TdxBarButton;
    empCambioTurnoMasivos: TdxBarButton;
    EmpCambioSalario: TdxBarLargeButton;
    EmpGlobalesSub: TdxBarSubItem;
    EmpSalariosGlobales: TdxBarButton;
    EmpPromPercepVariables: TdxBarButton;
    EmpRecalcularIntegrados: TdxBarButton;
    EmpAplicarTabulador: TdxBarButton;
    EmpTarjetasSub: TdxBarSubItem;
    dxBarSubItem6: TdxBarSubItem;
    EmpBancaElectronica: TdxBarButton;
    EmpTarjDespensa: TdxBarButton;
    EmpTarjGasolina: TdxBarButton;
    dxBarSubItem7: TdxBarSubItem;
    dxBarLookupCombo1: TdxBarLookupCombo;
    dxBarSpinEdit1: TdxBarSpinEdit;
    dxBarListItem1: TdxBarListItem;
    dxBarContainerItem1: TdxBarContainerItem;
    dxBarToolbarsListItem1: TdxBarToolbarsListItem;
    dxBarListItem2: TdxBarListItem;
    HerramientasSubItem: TdxBarSubItem;
    EmpBeneficiosSub: TdxBarSubItem;
    EmpOrganigrama: TdxBarLargeButton;
    EmpEntregarHerramientas: TdxBarButton;
    EmpRegresaErramientaGlobal: TdxBarButton;
    EmpEntregaGlobal: TdxBarButton;
    EmpRegresaHerramienta: TdxBarButton;
    EmpEntregaHerramientaGlobal: TdxBarButton;
    EmpRenovarSGM: TdxBarButton;
    EmpImportarSGM: TdxBarButton;
    EmpBorrarSgm: TdxBarButton;
    EmpCambioPlaza: TdxBarLargeButton;
    TabExpediente: TdxRibbonTab;
    ToolBarVacaciones: TdxBar;
    ToolBarIncapacidades: TdxBar;
    ToolBarPermisos: TdxBar;
    ToolBarPrestamos: TdxBar;
    toolBarCefeteria: TdxBar;
    ToolBarCaseta: TdxBar;
    EmpVacacionesIndividual: TdxBarLargeButton;
    EmpVacacionesGlobal: TdxBarLargeButton;
    EmpINcapacidadesINcapacidades: TdxBarLargeButton;
    EmpCafeteriaIndividual: TdxBarLargeButton;
    EmpCafeteriaGlobal: TdxBarLargeButton;
    EmpCafeteriaCorregirFechasGlobales: TdxBarLargeButton;
    EmpPermisosIndividuales: TdxBarLargeButton;
    EmpPermisosGlobales: TdxBarLargeButton;
    EmpPermisosCancelarGlobales: TdxBarLargeButton;
    EmpPrestaPresta: TdxBarLargeButton;
    EmpImportaAhorroPresta: TdxBarLargeButton;
    EmpCasetaChecadas: TdxBarLargeButton;
    ExpOtrosSubitem: TdxBarSubItem;
    EmpCerrarVacaciones: TdxBarButton;
    EmpCancelarVacaciones: TdxBarButton;
    EmpCierreGlobalVacaciones: TdxBarButton;
    ExpRecalculaSaldosVaca: TdxBarButton;
    TabCapacitacion: TdxRibbonTab;
    ToolBarCursosTomados: TdxBar;
    ToolBarCapacitacion: TdxBar;
    ToolBarCertificaciones: TdxBar;
    CapaRegIndividual: TdxBarLargeButton;
    CapaRegGlobal: TdxBarLargeButton;
    CapaCurOtrosSub: TdxBarSubItem;
    CapaSTPSREvisar: TdxBarLargeButton;
    CapaSTPSFoliar: TdxBarLargeButton;
    CapaSTPSReiniciaFolios: TdxBarLargeButton;
    CapaCertActual: TdxBarLargeButton;
    CapaCertPorEmpleado: TdxBarLargeButton;
    CapaCertVerLista: TdxBarLargeButton;
    CapaCertCancelaCursoTomadoGLobal: TdxBarButton;
    CapaImportaCursos: TdxBarButton;
    CapaCancelCursoProgGlobal: TdxBarButton;
    EmpCancelarCierreGlobalVacaciones: TdxBarButton;
    CapaCursoPRogramadoGLobal: TdxBarButton;
    TabAsistencia: TdxRibbonTab;
    ToolbarAutorizaciones: TdxBar;
    ToolbarAjusteTarjetas: TdxBar;
    ToolbarFestivos: TdxBar;
    ToolBarPrenomina: TdxBar;
    ToolbarExtra: TdxBar;
    AsisExtraPermisos: TdxBarLargeButton;
    AsisExraPermisosglobales: TdxBarLargeButton;
    AsistAutoPorAprobar: TdxBarLargeButton;
    AsistImpClasifiTemporales: TdxBarLargeButton;
    AsistHorTemporales: TdxBarLargeButton;
    AsistRegistrosAutomaticos: TdxBarLargeButton;
    AsistCorregirFechasGlobalesTarjetas: TdxBarLargeButton;
    AsisAjusteRecalcularTarjetas: TdxBarLargeButton;
    AsisPrenominaCalcula: TdxBarLargeButton;
    AsisPrenominaAtorizar: TdxBarLargeButton;
    AsisPolleoSub: TdxBarSubItem;
    dxBarSubItem14: TdxBarSubItem;
    AsisAjustIncapacidad: TdxBarLargeButton;
    AsisPolRelojes: TdxBarLargeButton;
    AsisProcTarjetas: TdxBarLargeButton;
    TabNomina: TdxRibbonTab;
    ToolbarExcepciones: TdxBar;
    ToolbarNomina: TdxBar;
    ToolbarVacia: TdxBar;
    ToolbarFonacot: TdxBar;
    ToolbarPeriodos: TdxBar;
    ToolbarAnuales: TdxBar;
    nomExcepDiasHoras: TdxBarLargeButton;
    NomExepMonto: TdxBarLargeButton;
    MonExcepGlobales: TdxBarLargeButton;
    NomExcepMovImport: TdxBarSubItem;
    nomFiniquitosSub: TdxBarSubItem;
    NomDesafectar: TdxBarLargeButton;
    NomCalcular: TdxBarLargeButton;
    NomAfectar: TdxBarLargeButton;
    NomOtros: TdxBarSubItem;
    NomGeneraPolizasSub: TdxBarSubItem;
    NomCosteoNomSub: TdxBarSubItem;
    NomCalculoPrevioISRSub: TdxBarSubItem;
    dxBarSubItem21: TdxBarSubItem;
    NomHerramientasSub: TdxBarSubItem;
    NomRecibosSub: TdxBarSubItem;
    NomCalcularSub: TdxBarSubItem;
    NomDefinirSub: TdxBarSubItem;
    NomAnualesSub: TdxBarSubItem;
    dxBarSubItem27: TdxBarSubItem;
    NomExcepMovImpMovs: TdxBarButton;
    NomMovExportarMov: TdxBarButton;
    NomLiquidacionFiniquitos: TdxBarButton;
    NomLiquidacionGlobal: TdxBarButton;
    NomImprimirListado: TdxBarButton;
    NomBorraNomActiva: TdxBarButton;
    NomCopiarNom: TdxBarButton;
    NomCancelNomPasadas: TdxBarButton;
    NomCalRetroActivos: TdxBarButton;
    NomPagarPorFuera: TdxBarButton;
    NomCalDIferencias: TdxBarButton;
    NomImprimePolContable: TdxBarButton;
    NomExportaPolContable: TdxBarButton;
    NomImprimeRecibos: TdxBarButton;
    NomImportaPagoRecibos: TdxBarButton;
    NomFoliarRecibos: TdxBarButton;
    NomPagoRecibos: TdxBarButton;
    NomRefoliarRecibos: TdxBarButton;
    NomCalculaSalarioNetoBruto: TdxBarButton;
    NomRecalculaAcumulados: TdxBarButton;
    NomRastreaCalculo: TdxBarButton;
    NomLimpiaAcumulados: TdxBarButton;
    NomCreditoAplicadoMensual: TdxBarLargeButton;
    dxBarSubItem28: TdxBarSubItem;
    NomImprimirCalculoISR: TdxBarButton;
    NomCosteoTransferencias: TdxBarButton;
    NomCalculoCosteo: TdxBarButton;
    NomEliminarTransferencias: TdxBarButton;
    NomCalcAjusteRetencionFonacot: TdxBarButton;
    NomCalcularPagoFonacot: TdxBarButton;
    NomDefPeriodos: TdxBarButton;
    NomDefNomNueva: TdxBarButton;
    NomAguinaldoSep: TdxBarSeparator;
    NomCalcAguinaldo: TdxBarButton;
    NomImprimirAguinaldo: TdxBarButton;
    NomPagarAguinaldo: TdxBarButton;
    RepartoAhorrosSep: TdxBarSeparator;
    NomCalcularReparto: TdxBarButton;
    nomCierrePrestamos: TdxBarButton;
    NomImprimirReparto: TdxBarButton;
    NomPagarAhorros: TdxBarButton;
    nomCierreAhorros: TdxBarButton;
    NomCalcularRepartoUtilidades: TdxBarButton;
    NomPagarRepartoUtilidades: TdxBarButton;
    NomImprimirRepartoUtilidades: TdxBarButton;
    ComparativoAnualSep: TdxBarSeparator;
    NomComparaISPT: TdxBarButton;
    NomDIfISTPNom: TdxBarButton;
    nomImprimirISTPCompAnual: TdxBarButton;
    NomDeclaracionGastosSep: TdxBarSeparator;
    NomDeclaraSalario: TdxBarButton;
    NomImprimirDeclaraSalario: TdxBarButton;
    NomDeclaraSep: TdxBarSeparator;
    NomImprimirDeclaracion: TdxBarButton;
    NomCierreDeclaracion: TdxBarButton;
    NomConfDeclaracion: TdxBarButton;
    Excepciones: TdxBarLargeButton;
    CancelarExcepciones: TdxBarLargeButton;
    Intercambio: TdxBarLargeButton;
    RepartoUtilsSep: TdxBarSeparator;
    dxBarButton1: TdxBarButton;
    TabIMSS: TdxRibbonTab;
    TabSistema: TdxRibbonTab;
    IMSSIDSE: TdxBar;
    IMSSPAGOS: TdxBar;
    IMSSINFONAVIT: TdxBar;
    IMSSSUA: TdxBar;
    IMSSPRIMADERIESGO: TdxBar;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    SistemaInfo: TdxBar;
    SIstemaEnrolamiento: TdxBar;
    SistemaPRoximidad: TdxBar;
    SistemaBiometricos: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    ToolbarPoleo: TdxBar;
    ToolbarSUBEISR: TdxBar;
    dxBarSubItem1: TdxBarSubItem;
    PanelContenedor: TPanel;
    PanelValorNomina: TPanel;
    PanelControlesNomina: TPanel;
    PeriodoTipoLBL_DevEx: TLabel;
    PeriodoPrimero_DevEx: TcxButton;
    PeriodoAnterior_DevEx: TcxButton;
    PeriodoSiguiente_DevEx: TcxButton;
    PeriodoUltimo_DevEx: TcxButton;
    PeriodoTipoCB_DevEx2: TcxStateComboBox;
    PeriodoNumeroCB_DevEx2: TcxStateComboBox;
    PanelRangoFechas: TPanel;
    PanelPeriodoStatus: TPanel;
    NomGenerarPoliza: TdxBarButton;
    _Sist_Actualizar_BDs: TAction;
    _Emp_Importar_Imagenes: TAction;
    _Sist_Agrega_Base_Datos_Seleccion: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    BarraSistemaBaseDatos: TdxBar;
    BaseDatosSeleccion: TdxBarSubItem;
    BaseDatosVisitantes: TdxBarSubItem;
    dxBarSubItem3: TdxBarSubItem;
    btnBaseDatosSeleccion: TdxBarButton;
    dxBarButton2: TdxBarButton;
    SistAgregaBaseDatos: TdxBarSubItem;
    SistAgregaBaseDatosSeleccion: TdxBarButton;
    SistAgregaBaseDatosPruebas: TdxBarButton;
    SistAgregaBaseDatosVisitantes: TdxBarButton;
    SistAgregaBaseDatosPresupuestos: TdxBarButton;
    SistAgregaBaseDatosEmpleados: TdxBarButton;
    _Sist_Agrega_Base_Datos_Visitantes: TAction;
    _Sist_Agrega_Base_Datos_Pruebas: TAction;
    _Sist_Agrega_Base_Datos_Empleados: TAction;
    _Sist_Agrega_Base_Datos_Presupuestos: TAction;
    dxBarLargeButton23: TdxBarLargeButton;
    _Sist_BorrarTimbradoNominas: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    btnYearBefore_DevEx: TdxBarButton;
    btnYearNext_DevEx: TdxBarButton;
    dxStaticFechaSistema: TdxBarStatic;
    dxStaticFechaSistemaValor: TdxBarStatic;
    SistemaYear_DevEx: TdxBarEdit;
    dxBarStaticAnio: TdxBarStatic;
    dxBarButton3: TdxBarButton;
    dxStaticFechaAsistencia: TdxBarStatic;
    dxStaticFechaAsistenciaValor: TdxBarStatic;
    dxStaticSistemaFechaDia: TdxBarStatic;
    dxBarButtonEditarFecha: TdxBarButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    _Sist_Extraer_Checadas_Bit_TermGTI: TAction;
    _Sist_Importar_Checadas_OffLine_TermGTI: TAction;
    dxBarButton4: TdxBarButton;
    dxBarLargeButton27: TdxBarLargeButton;
    SistTerminalesGTIChecadas: TdxBarSubItem;
    dxBarLargeButton25: TdxBarLargeButton;
    bbExtraerChecadasBitTermGTI: TdxBarButton;
    bbImportarChecadas: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarSubItem5: TdxBarSubItem;
    dxBarSubItem8: TdxBarSubItem;
    dxBarButton5: TdxBarButton;
	dxBarSubItem9: TdxBarSubItem;
    NomCalcAjusteRetencionFonacotCancelacion: TdxBarButton;
    _Per_AjusteRetFonacotCancelacion: TAction;
    _Emp_Cambio_Masivos: TAction;
    _A_Inicio: TAction;
    TabArchivo_btnInicio: TdxBarLargeButton;
	{*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
    NomImportarAcumulados: TdxBarButton;
    _Per_ImportarAcumulados: TAction;
    NomRecalcularAhorros: TdxBarButton;
    _Per_RecalcularAhorros: TAction;
    BarraSistemaCafeteria: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    _Sist_ImportarCafeteria: TAction;
    NomRecalcularPrestamos: TdxBarButton;
    _Per_RecalcularPrestamos: TAction;
    BarraSistemaSolicitudEnvios: TdxBar;
    dxAdministradorDeEnvios: TdxBarLargeButton;
    _Sist_Administrador_Envios_Programados: TAction;
    NomImportarCedulasFonacot: TdxBarButton;
    _Per_ImportarCedulasFonacot: TAction;
    _Per_GenerarCedulaFonacot: TAction;
    dxBarButton7: TdxBarButton;
    _Per_EliminarCedulaFonacot: TAction;
    dxBarSubItem10: TdxBarSubItem;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarSubItem11: TdxBarSubItem;
    dxBarButton8: TdxBarButton;
    btnEliminarCedulaFonacot: TdxBarButton;
    TimbradoImages: TcxImageList;
    TimbradoStatus_DevEx: TcxImage;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
    procedure SistemaFechaZFValidDate(Sender: TObject);
    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure _A_PreferenciasExecute(Sender: TObject);
    procedure _E_BuscarEmpleadoExecute(Sender: TObject);
    procedure _E_CambiarEmpleadoExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _E_RefrescarUpdate(Sender: TObject);
    procedure _V_ArbolOriginalExecute(Sender: TObject);
    procedure _V_EmpleadosExecute(Sender: TObject);
    procedure _V_NominaExecute(Sender: TObject);
    procedure _V_SistemaExecute(Sender: TObject);
    procedure _Emp_PrimeroExecute(Sender: TObject);
    procedure _Emp_PrimeroUpdate(Sender: TObject);
    procedure _Emp_AnteriorExecute(Sender: TObject);
    procedure _Emp_SiguienteExecute(Sender: TObject);
    procedure _Emp_UltimoExecute(Sender: TObject);
    procedure _Emp_UltimoUpdate(Sender: TObject);
    procedure _Emp_AltaExecute(Sender: TObject);
    procedure _Emp_BajaExecute(Sender: TObject);
    procedure _Emp_Cambio_SalarioExecute(Sender: TObject);
    procedure _Emp_Cambio_TurnoExecute(Sender: TObject);
    procedure _Emp_ReingresoExecute(Sender: TObject);
    procedure _Emp_Cambio_PuestoExecute(Sender: TObject);
    procedure _Emp_Cambio_AreaExecute(Sender: TObject);
    procedure _Emp_Cambio_ContratoExecute(Sender: TObject);
    procedure _Emp_Cambio_PrestacionExecute(Sender: TObject);
    procedure _Emp_Cambios_MultiplesExecute(Sender: TObject);
    procedure _Emp_Cerrar_VacacionesExecute(Sender: TObject);
    procedure _Emp_IncapacidadExecute(Sender: TObject);
    procedure _Emp_PermisoExecute(Sender: TObject);
    procedure _Emp_KardexExecute(Sender: TObject);
    procedure _Emp_CursoExecute(Sender: TObject);
    procedure _Emp_ComidasExecute(Sender: TObject);
    procedure _Emp_Recalcular_IntegradosExecute(Sender: TObject);
    procedure _Emp_Promediar_VariablesExecute(Sender: TObject);
    procedure _Emp_Salario_GlobalExecute(Sender: TObject);
    procedure _Emp_Aplicar_TabuladorExecute(Sender: TObject);
    procedure _Emp_VacacionesExecute(Sender: TObject);
    procedure _Emp_Vacaciones_GlobalesExecute(Sender: TObject);
    procedure _Emp_Cancelar_VacacionesExecute(Sender: TObject);
    procedure _Emp_CierreGlobal_VacacionesExecute(Sender: TObject);
    procedure _Emp_Aplicar_EventosExecute(Sender: TObject);
    procedure _Emp_Importar_KardexExecute(Sender: TObject);
    procedure _Emp_Cancelar_KardexExecute(Sender: TObject);
    procedure _Emp_Curso_GlobalExecute(Sender: TObject);
    procedure _Emp_TransferenciaExecute(Sender: TObject);
    procedure _Asi_AutorizarExecute(Sender: TObject);
    procedure _Asi_Horarios_TemporalesExecute(Sender: TObject);
    procedure _Asi_POLLExecute(Sender: TObject);
    procedure _Asi_Procesar_TarjetasExecute(Sender: TObject);
    procedure _Asi_PrenominaExecute(Sender: TObject);
    procedure _Asi_ExtrasPermisosGlobalesExecute(Sender: TObject);
    procedure _Asi_Registros_AutomaticosExecute(Sender: TObject);
    procedure _Asi_Corregir_FechasExecute(Sender: TObject);
    procedure _Asi_Recalcular_TarjetasExecute(Sender: TObject);
    procedure _Per_PrimeroExecute(Sender: TObject);
    procedure _Per_AnteriorExecute(Sender: TObject);
    procedure _Per_AnteriorUpdate(Sender: TObject);
    procedure _Per_SiguienteExecute(Sender: TObject);
    procedure _Per_SiguienteUpdate(Sender: TObject);
    procedure _Per_UltimoExecute(Sender: TObject);
    procedure _Per_NominaNuevaExecute(Sender: TObject);
    procedure _Per_BorrarNominaActivaExecute(Sender: TObject);
    procedure _Per_ExcepcionDiaHoraExecute(Sender: TObject);
    procedure _Per_ExcepcionMontoExecute(Sender: TObject);
    procedure _Per_ExcepcionesGlobalesExecute(Sender: TObject);
    procedure _Per_LiquidacionExecute(Sender: TObject);
    procedure _Per_PagoRecibosExecute(Sender: TObject);
    procedure _Per_CalcularNominaExecute(Sender: TObject);
    procedure _Per_AfectarNominaExecute(Sender: TObject);
    procedure _Per_FoliarRecibosExecute(Sender: TObject);
    procedure _Per_ImprimirListadoExecute(Sender: TObject);
    procedure _Per_ImprimirRecibosExecute(Sender: TObject);
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
    procedure _Per_ImprimirPolizaExecute(Sender: TObject);
    procedure _Per_ExportarPolizaExecute(Sender: TObject);
    procedure _Per_DesafectarExecute(Sender: TObject);
    procedure _Per_LimpiarAcumuladosExecute(Sender: TObject);
    procedure _Per_RecalcularAcumuladosExecute(Sender: TObject);
    procedure _Per_ImportarMovimientosExecute(Sender: TObject);
    procedure _Per_ExportarMovimientosExecute(Sender: TObject);
    procedure _Per_ImportarPagoRecibosExecute(Sender: TObject);
    procedure _Per_RefoliarRecibosExecute(Sender: TObject);
    procedure _Per_CopiarNominaExecute(Sender: TObject);
    procedure _Per_CalcularDiferenciasExecute(Sender: TObject);
    procedure _Per_PagarFueraExecute(Sender: TObject);
    procedure _Per_CancelarPasadasExecute(Sender: TObject);
    procedure _Per_LiquidacionGlobalExecute(Sender: TObject);
    procedure _Per_CalcularNetoBrutoExecute(Sender: TObject);
    procedure _Per_RastrearCalculoExecute(Sender: TObject);
    procedure _Per_DefinirPeriodosExecute(Sender: TObject);
    procedure _Per_CalcularAguinaldosExecute(Sender: TObject);
    procedure _Per_ImprimirAguinaldosExecute(Sender: TObject);
    procedure _Per_PagarAguinaldosExecute(Sender: TObject);
    procedure _Per_CalcularRepartoExecute(Sender: TObject);
    procedure _Per_ImprimirRepartoExecute(Sender: TObject);
    procedure _Per_PagarRepartoExecute(Sender: TObject);
    procedure _Per_CierreAhorrosExecute(Sender: TObject);
    procedure _Per_CierrePrestamosExecute(Sender: TObject);
    procedure _Per_CalcularPTUExecute(Sender: TObject);
    procedure _Per_ImprimirPTUExecute(Sender: TObject);
    procedure _Per_PagarPTUExecute(Sender: TObject);
    procedure _Per_CalcularDeclaracionExecute(Sender: TObject);
    procedure _Per_ImprimirDeclaracionExecute(Sender: TObject);
    procedure _Per_CalcularComparativoExecute(Sender: TObject);
    procedure _Per_ImprimirComparativoExecute(Sender: TObject);
    procedure _Per_PagarComparativoExecute(Sender: TObject);
    procedure _IMSS_PagoNuevoExecute(Sender: TObject);
    procedure _IMSS_BorrarPagoExecute(Sender: TObject);
    procedure _IMSS_CalcularPagoExecute(Sender: TObject);
    procedure _IMSS_CalcularRecargosExecute(Sender: TObject);
    procedure _IMSS_CalcularTasaExecute(Sender: TObject);
    procedure _IMSS_RevisarSUAExecute(Sender: TObject);
    procedure _IMSS_ExportarSUAExecute(Sender: TObject);
    procedure _IMSS_CalcularPrimaExecute(Sender: TObject);
    procedure _Sist_EmpresaNuevaExecute(Sender: TObject);
    procedure _Sist_BorrarBajasExecute(Sender: TObject);
    procedure _Sist_BorrarNominasExecute(Sender: TObject);
    procedure _Sist_BorrarTimbradoNominasExecute(Sender: TObject);
    procedure _Sist_Borrar_TarjetasExecute(Sender: TObject);
    procedure _Sist_Borrar_POLLExecute(Sender: TObject);
    procedure _Sist_Borrar_BitacoraExecute(Sender: TObject);
    procedure _Emp_EntregaHerramientaExecute(Sender: TObject);
    procedure _Emp_EntregaGlobalExecute(Sender: TObject);
    procedure _Emp_RegresarHerramientaExecute(Sender: TObject);
    procedure _Emp_Entregar_HerramientaExecute(Sender: TObject);
    procedure _Emp_Regresar_HerramientaExecute(Sender: TObject);
    procedure _Sist_Borrar_HerramientasExecute(Sender: TObject);
    procedure _Per_CreditoAplicadoMensualExecute(Sender: TObject);
    procedure _Emp_Importar_AltasExecute(Sender: TObject);
    procedure _Emp_Banca_ElecExecute(Sender: TObject);
    procedure _Emp_RenumerarExecute(Sender: TObject);
    procedure _Emp_Credito_InfonavitExecute(Sender: TObject);
    procedure _Emp_Permiso_GlobalExecute(Sender: TObject);
    procedure _Emp_PrestamosExecute(Sender: TObject);
    procedure _Per_CalculaRetroactivosExecute(Sender: TObject);
    procedure _Emp_Borrar_Curso_GlobalExecute(Sender: TObject);
    procedure _Per_DeclaracionAnualExecute(Sender: TObject);
    procedure _Per_CierreDeclaracionAnualExecute(Sender: TObject);
    procedure _Per_ImprimirDeclaracionAnualExecute(Sender: TObject);
    procedure _Emp_Registro_Comidas_GrupalesExecute(Sender: TObject);
    procedure _Emp_Corregir_Fechas_CafExecute(Sender: TObject);
    procedure _Asi_AutorizacionesXAprobarExecute(Sender: TObject);
    procedure _Sist_Actualizar_NumeroTarjetaExecute(Sender: TObject);
    procedure _Asi_FechaLimiteExecute(Sender: TObject);
    procedure EmpleadoActivoPanelDblClick(Sender: TObject);
    procedure _Emp_Importar_Asistencia_SesionesExecute(Sender: TObject);
    procedure _Emp_Cancelar_Cierre_VacacionesExecute(Sender: TObject);
    procedure _Emp_CertificacionesExecute(Sender: TObject);
    procedure _Asi_IntercambioFestivoExecute(Sender: TObject);
    procedure _Asi_CancelarExcepcionesFestivosExecute(Sender: TObject);
    procedure _Emp_Cancelar_Permisos_GlobalesExecute(Sender: TObject);
    procedure _Emp_Importar_Aho_PreExecute(Sender: TObject);
    procedure _Emp_Recalcula_Saldos_VacaExecute(Sender: TObject);
    procedure _Emp_Cambio_plazaExecute(Sender: TObject);
    procedure _Emp_Cambio_Tipo_NominaExecute(Sender: TObject);
    procedure _Emp_Certificaciones_EmpleadoExecute(Sender: TObject);
    procedure _Emp_Empleados_CertificadosExecute(Sender: TObject);
    procedure _Per_AjusteRetFonacotExecute(Sender: TObject);
    procedure _Per_CalcularPagoFonacotExecute(Sender: TObject);
    procedure _Sist_Enrolamiento_MasivoExecute(Sender: TObject);
    procedure _Sist_Importar_Enrolamiento_MasivoExecute(Sender: TObject);
    procedure _IMSS_AjusteRetInfExecute(Sender: TObject);
    procedure _Emp_CasetaExecute(Sender: TObject);
    procedure NavegacionCBSelect(Sender: TObject);
    procedure ArbolitoDblClick(Sender: TObject); override; //DevEx (by am)
    procedure ArbolitoKeyPress(Sender: TObject; var Key: Char); override;
    procedure _V_CerrarExecute(Sender: TObject);
    procedure _Asi_Ajust_Incapa_CalExecute(Sender: TObject);
    procedure _Emp_Curso_Prog_GlobalExecute(Sender: TObject);
    procedure _Emp_Cancelar_Curso_Prog_GlobalExecute(Sender: TObject);
    procedure _Emp_Tarjetas_GasolinaExecute(Sender: TObject);
    procedure _Emp_Tarjetas_DespensaExecute(Sender: TObject);
    procedure _Emp_Foliar_capacitacionesExecute(Sender: TObject);
    procedure _Asi_AutorizacionPreNominaExecute(Sender: TObject);
    procedure _Asi_RegistrarExcepcionesFestivoExecute(Sender: TObject);
    procedure _IMSS_ValidaMovIDSEExecute(Sender: TObject);
    procedure _Per_TransferenciasCosteoExecute(Sender: TObject);
    procedure _Per_Previo_ISRExecute(Sender: TObject);
    procedure _Per_Imprimir_PrevioISRExecute(Sender: TObject);
    procedure _Emp_Importar_SGMExecute(Sender: TObject);
    procedure _Emp_Renovacion_SGMExecute(Sender: TObject);
    procedure _Emp_Borrar_SGMExecute(Sender: TObject);
    procedure _Asi_ImportarClasificacionesTemporalesExecute(Sender: TObject);
    procedure _Per_Calculo_CosteoExecute(Sender: TObject);
    procedure _Per_Cancelacion_TransferenciasExecute(Sender: TObject);
    procedure _Sist_Sincroniza_IDBiometricosExecute(Sender: TObject);
    procedure RegistroClick(Sender: TObject);
    procedure _Sist_Depura_BitacoraBiometricoExecute(Sender: TObject); // SYNERGY
    procedure _Sist_Asigna_NumBiometricosExecute(Sender: TObject); // SYNERGY
    procedure _Sist_Asigna_GrupoTerminalesExecute(Sender: TObject); // SYNERGY 
    procedure _Emp_Revisar_Datos_STPSExecute(Sender: TObject);
    procedure _Emp_Reiniciar_Capacitaciones_STPSExecute(Sender: TObject);
    procedure _Emp_ImportarOrganigramaExecute(Sender: TObject);
    procedure _Emp_Importar_ImagenesExecute(Sender: TObject);
    procedure _Sist_Actualizar_BDsExecute(Sender: TObject); // SYNERGY
    procedure ClientAreaPG_DevExChange(Sender: TObject);
    procedure ImagenStatus_DevExDblClick(Sender: TObject);
    procedure btnYearBefore_DevExClick(Sender: TObject);
    procedure btnYearNext_DevExClick(Sender: TObject);
    procedure SistemaYear_DevExPropertiesChange(Sender: TObject); // SYNERGY
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure ProcesosClick(Sender: TObject);
    //procedure RefrescaIMSSActivoPublico;
    procedure FormResize(Sender: TObject);
    procedure EscogeUnReporteSwitch( const sCaption: String; const Entity: TipoEntidad; const ReportType: eTipoReporte; const iDefault: Integer );
    procedure _Sist_Agrega_Base_Datos_SeleccionExecute(Sender: TObject);
    procedure _Sist_Agrega_Base_Datos_VisitantesExecute(Sender: TObject);
    procedure _Sist_Agrega_Base_Datos_PruebasExecute(Sender: TObject);
    procedure _Sist_Agrega_Base_Datos_EmpleadosExecute(Sender: TObject);
    procedure _Sist_Agrega_Base_Datos_PresupuestosExecute(Sender: TObject);
    procedure _Sist_Extraer_Checadas_Bit_TermGTIExecute(Sender: TObject);
    procedure _Sist_Importar_Checadas_OffLine_TermGTIExecute(Sender: TObject);
    procedure _Per_AjusteRetFonacotCancelacionExecute(Sender: TObject);
	procedure _A_InicioExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DevEx_BarManagerClickItem(Sender: TdxBarManager;
      ClickedItem: TdxBarItem);
    procedure _Emp_Cambio_MasivosExecute(Sender: TObject);
    procedure _Per_ImportarAcumuladosExecute(Sender: TObject);
    procedure _Per_RecalcularAhorrosExecute(Sender: TObject);
    procedure _Sist_ImportarCafeteriaExecute(Sender: TObject);
    procedure _Per_RecalcularPrestamosExecute(Sender: TObject);
    procedure _Sist_Administrador_Envios_ProgramadosExecute(Sender: TObject);
    procedure _Per_ImportarCedulasFonacotExecute(Sender: TObject);
    procedure _Per_GenerarCedulaFonacotExecute(Sender: TObject);
    procedure _Per_EliminarCedulaFonacotExecute(Sender: TObject);
  private
    { Private declarations }
    FTempPanelRangoFechas:String;
    FCambiandoAnioActivo: Boolean;
    function ValidaStatusNomina(const sProceso: String): Boolean;
    function RevisaDerechoForma( FormaConsulta: eFormaConsulta; const TipoDerecho: Integer ): Boolean;
    function CheckDerechosTress: Boolean;
    procedure CambioEmpleadoActivo;
    procedure CargaIMSSActivos;
    procedure CargaPeriodoActivos;
    procedure CargaSistemaActivos;
    procedure RefrescaPeriodoActivos;
    procedure RefrescaPeriodoInfo;
    procedure RefrescaIMSSActivo;
    procedure RefrescaSistemaActivos;
    procedure RefrescaEmpleadoActivos;
    procedure RefrescaEmpleadoInfo;
    procedure RevisaDerechos;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure WMWizardSilent(var Message: TMessage); message WM_WIZARD_SILENT;
    procedure ValidaFechaReformaFiscal; // Validacion por reforma fiscal del 2008
    procedure CargaNavegacion;
    procedure CalculaAnio ( Operacion:Integer );
    procedure HabilitaBotonesAnio;
    procedure AsignacionTabOrder_DevEx;
    procedure ElementosEmpresaAbierta;
    procedure AbrirAccesos;
    procedure SetAccesosRapidos(const index_acceso : integer);
    procedure RegistrarAccesos;
    procedure ObtenerAccesos;
    procedure CopiarAccesos;
    procedure RegistrarIconos;
    procedure CreateRoamingFolder;
    function GetSpecialFolder(const CSIDL: integer): string;
    function GetShortcutName(out LinkName: string): Boolean;
    procedure RedireccionarRutaAyuda(sNuevaRutaAyuda: string);
    procedure AddLlaveAyudaRegistro(sValorParaAppend, sRutaTargetShortcut  : string);
    procedure AddValorRegistro(sValor, sRuta  : string);
    function FileNameFromLink(const LinkName: String): String;
    function LastPos(SubStr, S: string): Integer;
    function LeerTomarValorLlaveRegistro(var sValorLlave: string; sRutaKey, sNameValueABuscar: string) : boolean;
    procedure CrearLlaveAyudaExento;

  protected
    { Protected declarations }
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); override;
    {$endif}
    procedure DoOpenAll; override;
    Procedure DoOpenSistema;override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    procedure BorrarModales;
    procedure CargaDerechos; override;
    //DevEx
    procedure CargaTraducciones; override;
    procedure MuestraDatosUsuarioActivo; override;
    function CheckVersionDatos:Boolean;override;

    //


  public
    { Public declarations }
    procedure PrendeApagaSubitem(SubMenu : TdxBarSubItem);
    procedure AsistenciaFechavalid(Valor:TDate); override;
    procedure LLenaOpcionesGrupos; override;
    function FormaActivaNomina: Boolean;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    procedure BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
    procedure CambiaEmpleadoActivos;
    procedure CambiaIMSSActivos;
    procedure CambiaPeriodoActivos;
    procedure CambioPeriodoActivo;
    procedure CambiaSistemaActivos;
    procedure CargaEmpleadoActivos;
    procedure RefrescaEmpleado;
    procedure RefrescaIMSS;
    procedure RefrescaPeriodo;
    procedure ReinitPeriodo;
    procedure SetAsistencia( const dValue: TDate );
    procedure SetBloqueo;
    procedure RefrescaNavegacionInfo;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad); override;
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades); override;
    {$endif}
    procedure ChangeTimerInfo;
    procedure GetFechaLimiteShell;
    procedure AbreFormaSimulaciones(const TipoForma: Integer);
    procedure RefrescaIMSSActivoPublico; override;
    //DevEx (by am): Metodo para cargar la configuracion de las vistas
    procedure CargaVista; override;
    property TempPanelRangoFechas: String  read FTempPanelRangoFechas write FTempPanelRangoFechas;
  end;

const
     K_PANEL_EMPLEADO = 1;
     K_PANEL_PERIODO = 2;
     K_PANEL_IMSS = 3;
     K_PANEL_NAVEGA = 4;
     K_PANEL_FECHA_SISTEMA = 5;
     K_PANEL_FECHA_ASISTENCIA = 6;

     K_MANUAL = 0;
     K_ANTERIOR = 1;
     K_SIGUIENTE = 2;
     K_MAX_YEAR = 2100;
     K_MIN_YEAR = 1899;

     K_EXTRA_WIDTH = 5;
     K_MIN_WIDTH = 1165;
     K_MIN_PANEL_FECHAS_PERIODO = 130;
var
  TressShell: TTressShell;
  //DevEx(by am): Arreglo para guardar los status del Empleado
  StatusEmpleado: array [0..3] of string = ('Activo', 'Reingreso','Baja','Antes ingreso');
  //DevEx(by Angie): Arreglo para guardar los status de la n�mina
  StatusTimbradoArray: array [0..5] of string = ('', 'Pendiente', 'Timbrada Parcial', 'Timbrada', 'Timbrado pendiente', 'Cancelaci�n pendiente' );

  lstAccesosDirectos  : TStringList;
  lstTopAccesosDirectos  : TStringList;

implementation

{$R *.DFM}

uses ZetaCommonClasses,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaDialogo,
     ZetaBuscaPeriodo_DevEx ,
     ZetaBuscaEmpleado_DevEx,
     ZetaBuscaDerechosAcceso_DevEx,
     ZetaRegistryCliente,
     ZAccesosMgr,
     ZAccesosTress,
     ZArbolTress,
     ZBaseDlgModal_DevEx,
     ZGlobalTress,
     ZToolsPE,
{$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
{$endif}
     TressHelp,
     DAsistencia,
     DCafeteria,
     DCatalogos,
     DCliente,
     DConsultas,
     DDiccionario,
     DGlobal,
     DIMSS,
     DNomina,
     DProcesos,
     DRecursos,
     DReportes,
     DSistema,
     DTablas,
     DConteo,
     FArbolConfigura_DevEx,
     FAutoClasses,
     FCalendario_DevEx,
     FEscogeReporte_DevEx,
     FEditFechaLimite_DevEx,
     FEditEmpAdicionales_DevEx,
     FEmpAlta_DevEx,
     DBasicoCliente,
     FSistActualizarBDs_DevEx,
     FActualizaBDConfirma_DevEx; //DevEx(by am): Pantalla agregada para la nueva Imagen.

{ ************** TTressShell ****************** }

procedure TTressShell.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  RegistrarAccesos;
end;

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmRecursos := TdmRecursos.Create( Self );
     dmAsistencia := TdmAsistencia.Create( Self );
     dmNomina := TdmNomina.Create( Self );
     dmIMSS := TdmIMSS.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmCafeteria := TdmCafeteria.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmProcesos := TdmProcesos.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmConteo := TdmConteo.Create( Self );
     inherited;
     HelpContext                           := H00001_Pantalla_principal;
     TabArchivo_btnConfigurarEmpresa.HelpContext  := H60651_Globales_empresa;
     btnBuscarEmp_DevEx.HelpContext              := H00012_Busqueda_empleados;
     FHelpGlosario := 'Tress.chm';

     {$ifndef DOS_CAPAS}
     _Sist_Depura_BitacoraBiometrico.Visible := True; // SYNERGY
     _Sist_Asigna_NumBiometricos.Visible := True; // SYNERGY
     _Sist_Asigna_GrupoTerminales.Visible := True; // SYNERGY
     {$endif}
     //DevEx (by am): Variable que indica que se trata del ejecutable de TRESS
     dmCliente.ModoTress_DevEx:=TRUE;
     ActionList.State := asSuspended;
     lstAccesosDirectos := TStringList.Create;
     lstTopAccesosDirectos := TStringList.Create;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
{$ifdef DOS_CAPAS}
var
   i : Integer;
{$endif}
begin
     //FFormManager.Free;
     inherited;
     dmConteo.Free;
     dmDiccionario.Free;
     dmCafeteria.Free;
     dmReportes.Free;
     dmIMSS.Free;
     dmNomina.Free;
     dmAsistencia.Free;
     dmRecursos.Free;
     dmTablas.Free;
     dmCatalogos.Free;
     dmSistema.Free;
     //inherited;
     dmCliente.Free;
     Global.Free;
     FreeAndNil(lstAccesosDirectos);
     FreeAndNil(lstTopAccesosDirectos);
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;

     with NavegacionCB do
     begin
          for i := 0 to Items.Count - 1 do
          begin
               TStringObject(Items.Objects[i]).Free;
               Items.Objects[i] := nil;
          end;
     end;

{$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     CargaVista;
     CargaTraducciones;
     ActionList.State := asNormal;
     RegistrarIconos;
     CrearLlaveAyudaExento;
end;


procedure TTressShell.CrearLlaveAyudaExento;
const
     NOMBREAYUDA = '\Tress.chm;file://;';
     ItssRestrictions = 'SOFTWARE\Microsoft\HTMLHelp\1.x\ItssRestrictions\';
     HHRestrictions = 'SOFTWARE\Microsoft\HTMLHelp\1.x\ItssRestrictions\';
var
     sValorLlave : string;
     iPosicion, i : Integer;
     iPuntoComaPosicion, iLongitudValorLlave : Integer;
     sRutaFolder : String;
     sRutaAyuda : String;
     sLink: string;
     sRutaShortcut : string;
     sRutaTargetShortcut : string;
begin
      try
        sLink := VACIO;
        sRutaTargetShortcut := VACIO;
        if GetShortcutName(sLink) then
        begin
              sRutaTargetShortcut := FileNameFromLink (sLink);
              iPosicion := LastPos('\' , sRutaTargetShortcut);
              System.Delete(sRutaTargetShortcut, iPosicion, length(sRutaTargetShortcut+NOMBREAYUDA));
              sValorLlave := VACIO;
              AddLlaveAyudaRegistro(VACIO, VACIO);
              if LeerTomarValorLlaveRegistro(sValorLlave, HHRestrictions, 'UrlAllowList') then
              begin
                   if sValorLlave = VACIO then
                   begin
                        AddLlaveAyudaRegistro(VACIO, sRutaTargetShortcut+NOMBREAYUDA );
                   end
                   else
                   begin
                         if sValorLlave.IndexOf(sRutaTargetShortcut+NOMBREAYUDA) = -1 then  //EN CASO DE QUE NO ESTE ES NECESARIO HACER EL APPEND DESDE EL COMIENZO
                         begin
                              iPuntoComaPosicion := 0;
                              iLongitudValorLlave := 0;
                              iPuntoComaPosicion := LastDelimiter(';', sValorLlave);
                              iLongitudValorLlave := Length(sValorLlave);
                              //ShowMessage('posicion'+IntToStr(iPuntoComaPosicion)+' length'+IntToStr(iLongitudValorLlave));
                              if iPuntoComaPosicion = iLongitudValorLlave then //Si encuentra el punto y coma al final entonces
                              begin
                                    //NO ES NECESARIO EL PUNTO Y COMA
                                    AddLlaveAyudaRegistro(sValorLlave, sRutaTargetShortcut+NOMBREAYUDA);
                              end
                              else
                              begin
                                    //AGREGAR EL PUNTO Y COMA DE SEPARACION
                                    AddLlaveAyudaRegistro(sValorLlave+';', sRutaTargetShortcut+NOMBREAYUDA);
                              end;

                              //EN CASO DE QUE NO ESTE ES NECESARIO HACER EL APPEND DESDE EL COMIENZO Y AGREGAR EL PUNTO Y COMA DE SEPARACION
                              //AddLlaveAyudaRegistro(sValorLlave+';', sRutaTargetShortcut );
                         end;
                   end;
              end
              else
              begin
                   CreateRoamingFolder;
              end;
        end;
      EXCEPT
          CreateRoamingFolder;
      end;
end;


function TTressShell.LeerTomarValorLlaveRegistro(var sValorLlave: string; sRutaKey, sNameValueABuscar: string) : boolean;
var
  oRegistro : TRegistry;
begin
     Result := False;
      try
              sValorLlave := VACIO;
              oRegistro:= TRegistry.Create(KEY_READ);
              oRegistro.RootKey:= HKEY_LOCAL_MACHINE;
              if (oRegistro.KeyExists(sRutaKey)) then
              begin
                    oRegistro.OpenKeyReadOnly(sRutaKey);
                    if (oRegistro.ValueExists(sNameValueABuscar)) then
                    begin
                         sValorLlave := oRegistro.ReadString(sNameValueABuscar);

                         if not AnsiContainsStr(sValorLlave,'\Tress.chm;file://;') then
                         begin
                              //Si no esta la excepcion se puede proceder a agregar
                              Result := True;
                         end;


                    end;
              end;
              oRegistro.CloseKey();
              oRegistro.Free;
      Except
            CreateRoamingFolder;
      end;
end;

procedure TTressShell.AddLlaveAyudaRegistro(sValorParaAppend, sRutaTargetShortcut : string);
const
     HHRestrictions   = 'Software\Microsoft\HTMLHelp\1.x\HHRestrictions\';
     ItssRestrictions = 'Software\Microsoft\HTMLHelp\1.x\ItssRestrictions\';
     HHRestrictionsWOW6432   = 'Software\Wow6432Node\Microsoft\HTMLHelp\1.x\HHRestrictions\';
     ItssRestrictionsWOW6432 = 'Software\Wow6432Node\Microsoft\HTMLHelp\1.x\ItssRestrictions\';
var
     oRegistro : TRegistry;
     sRutaFolder : String;
     sRutaAyuda : String;
     sRutaShortcut : string;
     sRutaRegistro: string;
     I: Integer;

begin
      for I := 1 to 4 do
      begin
           sRutaRegistro := VACIO;
           case I of
               1: sRutaRegistro := HHRestrictions;
               2: sRutaRegistro := ItssRestrictions;
               3: sRutaRegistro := HHRestrictionsWOW6432;
               4: sRutaRegistro := ItssRestrictionsWOW6432;
           end;
      AddValorRegistro(sValorParaAppend+sRutaTargetShortcut, sRutaRegistro);
      end;
end;

procedure TTressShell.AddValorRegistro(sValor, sRuta: string);
var
 oRegistro : TRegistry;
begin
    try
        oRegistro:= TRegistry.Create(KEY_READ);
        oRegistro.RootKey:= HKEY_LOCAL_MACHINE;
        if (not oRegistro.KeyExists(sRuta)) then
        begin
              oRegistro.Access:= KEY_WRITE or KEY_WOW64_64KEY;
              oRegistro.OpenKey(sRuta,True);
              if (oRegistro.ValueExists('UrlAllowList')) then
              begin
                   oRegistro.WriteString('UrlAllowList', sValor);
              end
              else
              begin

                   oRegistro.WriteString('UrlAllowList', sValor);
              end;
        end
        else
        begin
              oRegistro.Access:= KEY_WRITE or KEY_WOW64_64KEY;
              oRegistro.OpenKey(sRuta,True);
              if (oRegistro.ValueExists('UrlAllowList')) then
              begin
                   oRegistro.WriteString('UrlAllowList', sValor);
              end
              else
              begin
                   oRegistro.WriteString('UrlAllowList', sValor);
              end;
        end;
        oRegistro.CloseKey();
        oRegistro.Free;
    EXCEPT
          CreateRoamingFolder;
    end;
end;

function TTressShell.FileNameFromLink(const LinkName: String): String;
var
  AObject: IUnknown;
  ASLink: IShellLink;
  APFile: IPersistFile;
  WFileName: WideString;
  PFD: TWin32FindData;
begin
  AObject := CreateComObject(CLSID_ShellLink);
  ASLink := AObject as IShellLink;
  APFile := AObject as IPersistFile;
  WFileName := LinkName;
  APFile.Load(PWideChar(WFileName), 0);
  SetLength(Result, MAX_PATH);
  ASLink.GetPath(PChar(Result), MAX_PATH, PFD, 0);
end;

function TTressShell.LastPos(SubStr, S: string): Integer;
var
  Found, Len, Pos: integer;
begin
  Pos := Length(S);
  Len := Length(SubStr);
  Found := 0;
  while (Pos > 0) and (Found = 0) do
  begin
    if Copy(S, Pos, Len) = SubStr then
      Found := Pos;
    Dec(Pos);
  end;
  LastPos := Found;
end;

procedure TTressShell.CreateRoamingFolder;
var
     sRutaFolder : String;
     sRutaAyuda : String;
     sLink: string;
     iPosicion : Integer;
     sRutaShortcut : string;
     CreatedOrigen, AccessedOrigen, ModifiedOrigen: TDateTime;
     CreatedNuevo, AccessedNuevo, ModifiedNuevo: TDateTime;
begin
      try

          sRutaFolder := VACIO;
          //TOMA LA RUTA ESPECIAL C:\Users\username\AppData\Roaming
          sRutaFolder := GetSpecialFolder(CSIDL_APPDATA);
          //CREA EL DIRECTORIO SI NO EXISTE
          if not DirectoryExists(sRutaFolder+'\GTI') then
          begin
               CreateDir(sRutaFolder+'\GTI');
          end;
          //TOMA LA RUTA ACTUAL DE LA AYUDA
          sRutaAyuda := Application.HelpFile;
          //SI NO EXISTE AYUDA LA COPIA Y PEGA
          if not fileexists(sRutaFolder+'\GTI\Tress.chm') then
          begin
               CopyFile(PChar(sRutaAyuda),PChar(sRutaFolder+'\GTI\Tress.chm'),False);
          end
          else
          begin
               ZetaWinAPITools.GetInfoDate(sRutaAyuda, CreatedOrigen, AccessedOrigen, ModifiedOrigen);
               ZetaWinAPITools.GetInfoDate(sRutaFolder+'\GTI\Tress.chm', CreatedNuevo, AccessedNuevo, ModifiedNuevo);
               //SI ES MAS NUEVO EL ARCHIVO O MAYOR LA FECHA  (SE CREIA QUE DEBIA SER MENOR, PERO NO LO INTERPRETO AS� DELPHI)
                if CreatedOrigen > CreatedNuevo then
                begin
                    CopyFile(PChar(sRutaAyuda),PChar(sRutaFolder+'\GTI\Tress.chm'),False);
                end;
          end;
          //DETECTA LA RUTA DESDE DONDE SE ESTA EJECUTANDO Y SI ES UN SHORCUT ENTRA
          if GetShortcutName(sLink) then
          begin
                RedireccionarRutaAyuda(sRutaFolder+'\GTI\Tress.chm')
          end;
      Except
      end;
end;

procedure TTressShell.RedireccionarRutaAyuda(sNuevaRutaAyuda : string);
begin
      Application.HelpFile := sNuevaRutaAyuda;
end;

function TTressShell.GetShortcutName(out LinkName: string): Boolean;
const
  STARTF_TITLEISLINKNAME = $800;
var
  si: TStartupInfo;
begin
  Result := False;
  FillChar(si, SizeOf(TStartupInfo), 0);
  GetStartupInfo(si);
  if (si.dwFlags and STARTF_TITLEISLINKNAME) <> 0 then
  begin
    Result := True;
    LinkName := si.lpTitle;
  end;
end;

function TTressShell.GetSpecialFolder(const CSIDL: integer) : string;
var
  RecPath : PWideChar;
begin
  RecPath := StrAlloc(MAX_PATH);
    try
    FillChar(RecPath^, MAX_PATH, 0);
    if SHGetSpecialFolderPath(0, RecPath, CSIDL, false)
      then result := RecPath
      else result := '';
    finally
      StrDispose(RecPath);
    end;
end;


function TTressShell.CheckDerechosTress: Boolean;
begin
     Result := RevisaCualquiera( D_EMPLEADOS ) or
               RevisaCualquiera( D_ASISTENCIA ) or
               RevisaCualquiera( D_NOMINA ) or
               RevisaCualquiera( D_IMSS ) or
               RevisaCualquiera( D_CAPACITACION ) or
               RevisaCualquiera( D_CONSULTAS ) or
               RevisaCualquiera( D_CATALOGOS ) or
               RevisaCualquiera( D_TABLAS ) or
               RevisaCualquiera( D_SISTEMA );
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;

     if not EmpresaAbierta then
     begin
          StatusBarMsg( '', K_PANEL_EMPLEADO );
          StatusBarMsg( '', K_PANEL_PERIODO );
          StatusBarMsg( '', K_PANEL_IMSS );
          tabempleado.visible:=false;
          tabexpediente.visible:=false;
          tabcapacitacion.visible:=false;
          tabasistencia.visible:=false;
          tabnomina.visible:=false;
          tabimss.visible:=false;
          
          if dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION then
             tabSistema.visible:=true
          else
             tabSistema.visible:=false;

     end;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        if ( not CheckDerechosTress ) then
           db.DataBaseError( 'No tiene derechos para utilizar este ejecutable en esta Empresa' ); //DChavez : Se dejo esta validacion ya que no queremos que el usuario puede ver los valores activos
        dmSistema.LeeAdicionales;   // Carga Campos Adicionales - Se ocupa para cuando se conecta Global

        Global.Conectar;
        with dmCliente do
        begin

             InitActivosSistema;
             InitArrayTPeriodo;//acl
             InitActivosAsistencia;
             dmCatalogos.InitActivosNavegacion;
             CargaNavegacion;                         
             InitActivosEmpleado;
             InitActivosPeriodo;
             InitActivosIMSS;
             {*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
             RazonSocialAcumulado := VACIO;
             dmSistema.FTipo_Exe_DevEx := Ord( taTress );
             //LeeTiposNomina( self.PeriodoTipoCB.Lista ); {acl 7/10/08. El combo se cargara de la ListaFija}
             {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
             RespaldaValoresActivosGraficas;


             with PeriodoTipoCB_DevEx2 do
             begin
                  Enabled := dmCliente.GetDatosPeriodoActivo.Tipo > K_PERIODO_VACIO;
                  ListaFija:=lfTipoPeriodoConfidencial;
                  Llave:= IntToStr( Ord( dmCliente.GetDatosPeriodoActivo.Tipo ) );

                  // Si viene vacio el valor le asigna 0
                  if (StrVacio ( Llave )) and (dmCliente.GetDatosPeriodoActivo.Tipo > K_PERIODO_VACIO ) then
                  begin
                       PeriodoTipoCB_DevEx2.Indice:= 0;
                       dmCliente.PeriodoTipo := eTipoPeriodo( StrToInt( PeriodoTipoCB_DevEx2.Llave ) );
                       CambioPeriodoActivo;
                  end;
             end;
             SesionIDDashlet := 0;
        end;
        {***(@am): Se agrega bandera para que el cambio de a�o activo no se haga dos veces***}
        FCambiandoAnioActivo := False;
        {***}
        {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
        ActualizarGraficasInicio := True;
        {*** FIN ***}
        CargaEmpleadoActivos;
        CargaPeriodoActivos;
        CargaIMSSActivos;
        CargaSistemaActivos;

        SetArbolitosLength(12);
        CreaNavBar;
        RevisaDerechos;

        //** Ya no se valida, todas tienen vencimiento
        {if Autorizacion.TieneVencimiento then
             SistemaFechaZF_DevEx.Enabled := FALSE;
        }
        SetBloqueo;

        //Verificar constante de AVENT
        if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
        begin  
             //Hacer Tab Invisible
             TabIMSS.Visible := False; //**Deshabilitar nuevo tab
        end;//if

        {AV: 2010-11-16 Asigna El Global Usar Validacion Status Activo de Tablas y Cat�logos al ZetaClienteDataSet }
        ZetaClientDataSet.GlobalSoloKeysActivos := Global.GetGlobalBooleano( K_GLOBAL_USAR_VALIDACION_STATUS_TABLAS );
        // 2013-Feb-05: Mejora en Confidencialidad
        ZetaClientDataSet.GlobalListaConfidencialidad := dmCliente.Confidencialidad;

        ValidaFechaReformaFiscal;  // Validacion de Reforma Fiscal 2008
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
     LLenaOpcionesGrupos;

     ElementosEmpresaAbierta;

     //(@am): Accesos directos
     ObtenerAccesos;
     CopiarAccesos;
     AbrirAccesos;
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta)
  then
  begin
    for I:=0 to DevEx_NavBar.Groups.Count-1 do
    begin
      if (DevEx_NavBar.Groups[I].Caption = 'Empleados')    then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=1;
        DevEx_NavBar.Groups[I].SmallImageIndex:=1;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Asistencia') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=2;
        DevEx_NavBar.Groups[I].SmallImageIndex:=2;

      end;
      if (DevEx_NavBar.Groups[I].Caption = 'N�minas') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=3;
        DevEx_NavBar.Groups[I].SmallImageIndex:=3;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Pagos IMSS') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=4;
        DevEx_NavBar.Groups[I].SmallImageIndex:=4;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Capacitaci�n') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=5;
        DevEx_NavBar.Groups[I].SmallImageIndex:=5;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Consultas') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=6;
        DevEx_NavBar.Groups[I].SmallImageIndex:=6;
      end;
      if (DevEx_NavBar.Groups[I].Caption = 'Cat�logos') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=7;
        DevEx_NavBar.Groups[I].SmallImageIndex:=7;
      end;
      if (DevEx_NavBar.Groups[I].Caption ='Tablas') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=8;
        DevEx_NavBar.Groups[I].SmallImageIndex:=8;
      end;
      if (DevEx_NavBar.Groups[I].Caption ='Sistema') then
      begin
        DevEx_NavBar.Groups[I].LargeImageIndex:=9;
        DevEx_NavBar.Groups[I].SmallImageIndex:=9;
      end;
      if(DevEx_NavBar.Groups[I].Caption = '�rbol Cl�sico')  then
      begin
         if CheckDerechosTress then        //DChavez
         begin
              DevEx_NavBar.Groups[I].LargeImageIndex:=10;
              DevEx_NavBar.Groups[I].SmallImageIndex:=10;
         end
         else
         begin
              DevEx_NavBar.Groups[I].Caption := 'No tiene derechos' ;
              DevEx_NavBar.Groups[0].LargeImageIndex := 12;
              DevEx_NavBar.Groups[0].SmallImageIndex := 12;
         end;
      end;
      if (I = 0) then
      begin
            if(DevEx_NavBar.Groups.Count = 1) then
            begin
                 // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
                 if( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos' ) then
                 begin
                       DevEx_NavBar.Groups[0].LargeImageIndex := 12;
                       DevEx_NavBar.Groups[0].SmallImageIndex := 12;
                 end;
                 if( DevEx_NavBar.Groups[0].Caption = 'Sistema' ) then
                 begin
                      DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                      DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                 end;
            end
            else
            begin
                  if (DevEx_NavBar.Groups[1].Caption = 'Empleados')  then
                  begin
                      DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                      DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                  end;
            end;
      end;
      DevEx_NavBar.Groups[I].UseSmallImages:=false;
    end;
  end
  else
  begin
    if(DevEx_NavBar.Groups.Count >0) then
    begin
      DevEx_NavBar.Groups[0].UseSmallImages:=false;
      DevEx_NavBar.Groups[0].LargeImageIndex:=9;
      DevEx_NavBar.Groups[0].SmallImageIndex:=9;
    end;
  end;
end;
procedure TTressShell.CargaDerechos;
begin
     inherited CargaDerechos;
     { Pren�mina se utiliza tanto por Asistencia como por N�mina }
     { La forma obtiene sus derechos a partir de la primera ruta }
     { de acceso que es utilizada: si se abre primero por Asistencia }
     { se usa D_ASIS_DATOS_PRENOMINA; si se abre primero por N�mina }
     { se usa D_NOM_DATOS_PRENOMINA }
     ZAccesosMgr.DerechosIguales( D_ASIS_DATOS_PRENOMINA, D_NOM_DATOS_PRENOMINA );
end;

procedure TTressShell.RevisaDerechos;

{
function Revisa( const iPos: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( iPos, K_DERECHO_CONSULTA );
end;
}
 function EsPruebas : Boolean;
 begin
      Result := dmCliente.EsTRESSPruebas;
 end;

begin
     TabEmpleado.visible := (Revisa(D_EMP_REGISTRO) or  revisa (D_EMP_PROCESOS)) and dmcliente.EmpresaAbierta;
     TabExpediente.Visible := ( Revisa(D_EMP_REGISTRO) or  revisa (D_EMP_PROCESOS)) and dmcliente.EmpresaAbierta;
     TabCapacitacion.Visible := (Revisa(D_EMP_REGISTRO) or  revisa (D_EMP_PROCESOS)) and dmcliente.EmpresaAbierta ;
     TabAsistencia.Visible :=   (Revisa( D_ASIS_REGISTRO ) or Revisa( D_ASIS_PROCESOS )) and dmcliente.EmpresaAbierta ;
     TabNomina.Visible :=  (Revisa( D_NOM_REGISTRO ) or  Revisa( D_NOM_PROCESOS )) and dmcliente.EmpresaAbierta ;
     TabImss.Visible :=  (Revisa( D_IMSS_REGISTRO ) or Revisa( D_IMSS_PROCESOS )) and dmcliente.EmpresaAbierta ;
     TabSistema.Visible :=  Revisa( D_SIST_REGISTRO ) or Revisa( D_SIST_PROCESOS );



     _Emp_Alta.Enabled := Revisa( D_EMP_REG_ALTA ) and not EsPruebas;
     _Emp_Baja.Enabled := Revisa( D_EMP_REG_BAJA );
     _Emp_Reingreso.Enabled := Revisa( D_EMP_REG_REINGRESO ) and not EsPruebas;
     _Emp_Cambio_Salario.Enabled := Revisa( D_EMP_REG_CAMBIO_SALARIO );
     _Emp_Cambio_Plaza.enabled:= Revisa( D_EMP_REG_CAMBIO_PLAZA );
     _Emp_Importar_Imagenes.enabled:= Revisa( D_EMP_IMPORTAR_IMAGENES );
     _Emp_Cambio_Tipo_Nomina.ENabled := Revisa(D_EMP_REG_CAMBIO_TIPO_NOMINA);
     _Emp_Cambio_Turno.Enabled := Revisa( D_EMP_REG_CAMBIO_TURNO );
     _Emp_Cambio_Puesto.Enabled := Revisa( D_EMP_REG_CAMBIO_PUESTO );
     _Emp_Cambio_Area.Enabled := Revisa( D_EMP_REG_CAMBIO_AREA );
     _Emp_Cambio_Contrato.Enabled := Revisa( D_EMP_REG_CAMBIO_CONTRATO );
     _Emp_Cambio_Prestacion.Enabled := Revisa( D_EMP_REG_CAMBIO_PRESTA );
     _Emp_Cambios_Multiples.Enabled := Revisa( D_EMP_REG_CAMBIO_MULTIPLE );
     _Emp_Vacaciones.Enabled := Revisa( D_EMP_REG_VACACIONES );
     _Emp_Cerrar_Vacaciones.Enabled := Revisa( D_EMP_REG_CIERRE_VACA );
     _Emp_Incapacidad.Enabled := Revisa( D_EMP_REG_INCAPACIDAD );
     _Emp_Permiso.Enabled := Revisa( D_EMP_REG_PERMISO );
     _Emp_Kardex.Enabled := Revisa( D_EMP_REG_KARDEX );
     _Emp_Curso.Enabled := Revisa( D_EMP_REG_CURSO_TOMADO );
     _Emp_Comidas.Enabled := Revisa( D_EMP_REG_COMIDAS );
     _Emp_Caseta.Enabled := Revisa( D_EMP_REG_CASETA_DIARIAS );
     _Emp_Curso_Prog_Global.Enabled := Revisa(D_EMP_PROC_CURSO_PROG_GLOBAL);
     _Emp_Cancelar_Curso_Prog_Global.Enabled := Revisa(D_EMP_PROC_CANC_CUR_PROG_GLOB);          
     _Emp_Prestamos.Enabled := Revisa( D_EMP_REG_PRESTAMO );
     _Emp_Banca_Elec.Enabled := CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_BANCA );
     _Emp_Cambio_Masivos.Enabled := Revisa( D_EMP_PROG_CAMBIO_TURNO );
     _A_ExploradorReportes.Enabled := CheckDerecho(D_CONS_REPORTES , K_DERECHO_CONSULTA);
     _A_ListaProcesos.Enabled := CheckDerecho(GetPropConsulta(efcSistProcesos).IndexDerechos,K_DERECHO_CONSULTA);
     _A_SQL_DevEx.enabled := CheckDerecho(GetPropConsulta(efcQueryGral).IndexDerechos,K_DERECHO_CONSULTA);
     _A_Bitacora_DevEx.enabled := CheckDerecho(GetPropConsulta(efcSistBitacora).IndexDerechos,K_DERECHO_CONSULTA);
     _A_Inicio.Enabled := CheckDerecho( GetPropConsulta(efcInicio).IndexDerechos, K_DERECHO_CONSULTA );
     _Emp_EntregaHerramienta.Enabled := Revisa( D_EMP_REG_ENTREGA_TOOLS );
     _Emp_EntregaGlobal.Enabled := Revisa( D_EMP_REG_ENTREGA_GLOBAL );
     _Emp_RegresarHerramienta.Enabled := Revisa( D_EMP_REG_REGRESA_TOOLS );
     _Emp_Recalcular_Integrados.Enabled := Revisa( D_EMP_PROC_RECALC_INT );
     _Emp_Promediar_Variables.Enabled := Revisa( D_EMP_PROC_PROM_VARIABLES );
     _Emp_Salario_Global.Enabled := Revisa( D_EMP_PROC_SALARIO_GLOBAL );
     _Emp_Aplicar_Tabulador.Enabled := Revisa( D_EMP_PROC_TABULADOR );
     _Emp_Vacaciones_Globales.Enabled := Revisa( D_EMP_PROC_VACA_GLOBAL );
     _Emp_Cancelar_Vacaciones.Enabled := Revisa( D_EMP_PROC_CANCELAR_VACA );
     _Emp_CierreGlobal_Vacaciones.Enabled := Revisa( D_EMP_REG_CIERRE_VACA );
     _Emp_Aplicar_Eventos.Enabled := Revisa( D_EMP_PROC_APLICA_EVENTOS );
     _Emp_Importar_Kardex.Enabled := Revisa( D_EMP_PROC_IMPORTA_KARDEX );
     _Emp_Cancelar_Kardex.Enabled := Revisa( D_EMP_PROC_CANCELA_KARDEX );
     _Emp_Importar_Altas.Enabled := Revisa( D_EMP_PROC_IMPORTAR_ALTAS ) and not EsPruebas;
     _Emp_Curso_Global.Enabled := Revisa( D_EMP_PROC_CURSO_GLOBAL );
     _Emp_Renumerar.Enabled := Revisa( D_EMP_PROC_RENUMERA );
     _Emp_Permiso_Global.Enabled := Revisa( D_EMP_PROC_PERMISO_GLOBAL );
     _Emp_Transferencia.Enabled := Revisa( D_EMP_PROC_TRANSFERENCIA );
     _Emp_CierreGlobal_Vacaciones.Enabled := Revisa( D_EMP_PROC_CIERRE_VACACIONES );
     _Emp_Entregar_Herramienta.Enabled := Revisa( D_EMP_PROC_ENTREGAR_HERRAMIENTA );
     _Emp_Regresar_Herramienta.Enabled := Revisa( D_EMP_PROC_REGRESAR_HERRAMIENTA );
     //_Emp_Credito_Infonavit.Enabled := CheckDerecho( D_EMP_DATOS_OTROS, K_DERECHO_SIST_KARDEX );
     _Emp_Credito_Infonavit.Visible := FALSE;
     _Emp_Borrar_Curso_Global.Enabled := Revisa( D_EMP_PROC_BORRAR_CURSO_GLOBAL );
     _Emp_Registro_Comidas_Grupales.Enabled := Revisa( D_EMP_PROC_COMIDAS_GRUPALES );
     _Emp_Corregir_Fechas_Caf.Enabled := Revisa( D_EMP_PROC_CORREGIR_FECHAS_CAFE );
     _Asi_Autorizar.Enabled := Revisa( D_ASIS_REG_AUTO_EXTRAS );
     _Asi_Horarios_Temporales.Enabled := Revisa( D_ASIS_REG_HORARIO_TEMPO );
     _Emp_Certificaciones_Empleado.Enabled := Revisa( D_EMP_PROC_CERTIFIC_EMP );
     _Emp_Certificaciones.Enabled := CheckDerecho( D_EMP_EXP_CERTIFICACIONES, K_DERECHO_ALTA );
     _Emp_Empleados_Certificados.Enabled := Revisa( D_EMP_PROC_CERTIFIC_GLOBAL );
     //Grid de Aprobaciones
     with _Asi_AutorizacionesXAprobar do
     begin
          Visible := dmAsistencia.AprobarPrendido;
          if Visible then
             Enabled := Revisa( D_ASIS_REG_AUTORIZACIONES_APROBAR );
     end;
     _Asi_POLL.Enabled := Revisa( D_ASIS_PROC_POLL_RELOJES );
     _Asi_Procesar_Tarjetas.Enabled := Revisa( D_ASIS_PROC_PROC_TARJETAS );
//     _Asi_Prenomina.Enabled := Revisa( D_ASIS_PROC_CALC_PRENOMINA );
     _Asi_Prenomina.Enabled := RevisaCualquiera( D_ASIS_PROC_CALC_PRENOMINA );
     _Asi_ExtrasPermisosGlobales.Enabled := Revisa( D_ASIS_PROC_EXTRAS_GLOBAL );
     _Asi_Registros_Automaticos.Enabled := Revisa( D_ASIS_PROC_REG_AUTOMATICO );
     _Asi_Corregir_Fechas.Enabled := Revisa( D_ASIS_PROC_FECHAS_GLOBAL );
     _Asi_Recalcular_Tarjetas.Enabled := RevisaCualquiera( D_ASIS_PROC_RECALC_TARJETA );
     _Asi_IntercambioFestivo.Enabled := Revisa( D_ASIS_PROC_INTERCAMBIO_FESTIVOS );
     _Asi_CancelarExcepcionesFestivos.Enabled := Revisa ( D_ASIS_PROC_CANCELAR_EXCEP_FEST );
     _Asi_RegistrarExcepcionesFestivo.Enabled := Revisa ( D_ASIS_PROC_REGISTRAR_EXCEP_FEST );
     _Asi_Ajust_Incapa_Cal.Enabled := Revisa ( D_ASIS_AJUST_INCAPA_CAL ); {acl 6.Abril.09}
     _Asi_IntercambioFestivo.Enabled := Revisa( D_ASIS_PROC_INTERCAMBIO_FESTIVOS );
     _Asi_CancelarExcepcionesFestivos.Enabled := Revisa ( D_ASIS_PROC_CANCELAR_EXCEP_FEST );
     _Asi_Ajust_Incapa_Cal.Enabled := Revisa ( D_ASIS_AJUST_INCAPA_CAL ); {acl 6.Abril.09}
     _Asi_ImportarClasificacionesTemporales.Visible := Revisa( D_ASIS_PROC_IMPORTA_CLASIFICACIONES );
     _IMSS_PagoNuevo.Enabled := Revisa( D_IMSS_REG_NUEVO );
     _IMSS_BorrarPago.Enabled := Revisa( D_IMSS_REG_BORRAR_ACTIVO );
     _IMSS_CalcularPago.Enabled := Revisa( D_IMSS_PROC_CALCULAR );
     _IMSS_CalcularRecargos.Enabled := Revisa( D_IMSS_PROC_RECARGOS );
     { AP(23/May/2008): Ya no es necesario, lo hace SUA
     _IMSS_CalcularTasa.Enabled := Revisa( D_IMSS_PROC_CALC_TASA_INFO );}
      _IMSS_CalcularTasa.Visible := FALSE;
     _IMSS_RevisarSUA.Enabled := Revisa( D_IMSS_PROC_REVISAR_SUA );
     _IMSS_ExportarSUA.Enabled := Revisa( D_IMSS_PROC_EXP_SUA );
     _IMSS_CalcularPrima.Enabled := Revisa( D_IMSS_PROC_CALC_RIESGO );
     _IMSS_AjusteRetInf.Enabled:= Revisa( D_IMSS_PROC_AJUS_INFONAVIT );
     _IMSS_ValidaMovIDSE.Enabled :=Revisa( D_IMSS_PROC_VALID_MOV_IDSE );
     {$ifdef ANTES}
     _Sist_EmpresaNueva.Enabled := Revisa( D_SIST_REG_EMPRESA_NUEVA );
     {$else}
     _Sist_EmpresaNueva.Enabled := FALSE;
     {$endif}
     _Sist_BorrarBajas.Enabled := Revisa( D_SIST_PROC_BORRAR_BAJAS ) ;
     _Sist_BorrarNominas.Enabled := Revisa( D_SIST_PROC_BORRA_NOMINAS ) ;
     _Sist_BorrarTimbradoNominas.Enabled := Revisa( D_SIST_PROC_BORRA_TIMBRADO_NOMINAS ) and EsPruebas and dmcliente.EmpresaAbierta;
     _Sist_Borrar_Tarjetas.Enabled := Revisa( D_SIST_PROC_BORRAR_TARJETAS  ) ;
     _Sist_Borrar_POLL.Enabled := Revisa( D_SIST_BORRAR_POLL );
     _Sist_Borrar_Bitacora.Enabled := Revisa( D_SIST_BORRAR_BITACORA );
     _Sist_Borrar_Herramientas.Enabled := Revisa( D_SIST_BORRAR_HERRAMIENTA );
     _Sist_Actualizar_NumeroTarjeta.Enabled := Revisa( D_SIST_PROC_NUMERO_TARJETA ) ;
     _Sist_Depura_BitacoraBiometrico.Enabled := Revisa( D_SIST_PROC_DEPURA_BIT_BIO )  ; // SYNERGY
     _Sist_Asigna_NumBiometricos.Enabled := Revisa( D_SIST_PROC_ASIGNA_NUM_BIO )and dmcliente.EmpresaAbierta; // SYNERGY
     _Sist_Asigna_GrupoTerminales.Enabled := Revisa( D_SIST_PROC_ASIGNA_GRUPO_TERM )and dmcliente.EmpresaAbierta; // SYNERGY
     _Sist_Enrolamiento_Masivo.Enabled := Revisa( D_SIST_PROC_ENROLAMIENTO_MASIVO ) and dmcliente.EmpresaAbierta;
     _Sist_Importar_Enrolamiento_Masivo.Enabled := Revisa( D_SIST_PROC_IMP_ENROLAMIENTO_MAS) and dmcliente.EmpresaAbierta;
     _Sist_Actualizar_BDs.Enabled := Revisa( D_SIST_ACTUALIZAR_BDS);
     _Sist_Extraer_Checadas_Bit_TermGTI.Visible := Revisa (D_SIST_TERM_EXTRAER_CHECADAS) and dmcliente.EmpresaAbierta; // SYNERGY
     _Sist_Importar_Checadas_OffLine_TermGTI.Visible := Revisa (D_SIST_TERM_RECUPERAR_CHECADAS) and dmcliente.EmpresaAbierta; // SYNERGY

     _Per_NominaNueva.Enabled := Revisa( D_NOM_REG_NOMINA_NUEVA );
     _Per_BorrarNominaActiva.Enabled := Revisa( D_NOM_REG_BORRA_ACTIVA );
     _Per_ExcepcionDiaHora.Enabled := Revisa( D_NOM_REG_EXCEP_DIA_HORA );
     _Per_ExcepcionMonto.Enabled := Revisa( D_NOM_REG_EXCEP_MONTO );
     _Per_ExcepcionesGlobales.Enabled := Revisa( D_NOM_REG_EXCEP_MONTO_GLOBAL );
     _Per_Liquidacion.Enabled := Revisa( D_NOM_REG_LIQUIDACION );
     _Per_PagoRecibos.Enabled := Revisa( D_NOM_REG_PAGO_RECIBOS );
//     _Per_CalcularNomina.Enabled := Revisa( D_NOM_PROC_CALCULAR );
     _Per_CalcularNomina.Enabled := RevisaCualquiera( D_NOM_PROC_CALCULAR );
     _Per_AfectarNomina.Enabled := Revisa( D_NOM_PROC_AFECTAR );
     _Per_FoliarRecibos.Enabled := Revisa( D_NOM_PROC_FOLIAR_RECIBOS );
     _Per_ImprimirListado.Enabled := Revisa( D_NOM_PROC_IMP_LISTADO );
     _Per_ImprimirRecibos.Enabled := Revisa( D_NOM_PROC_IMP_RECIBOS );
     _Per_GenerarPoliza.Enabled := Revisa( D_NOM_PROC_CALC_POLIZA );
     _Per_ImprimirPoliza.Enabled := Revisa( D_NOM_PROC_IMP_POLIZA );
     _Per_ExportarPoliza.Enabled := Revisa( D_NOM_PROC_EXP_POLIZA );
     _Per_CreditoAplicadoMensual.Enabled := Revisa( D_NOM_PROC_CRED_APLICADO );
     _Per_Desafectar.Enabled := Revisa( D_NOM_PROC_DESAFECTAR );
     _Per_LimpiarAcumulados.Enabled := Revisa( D_NOM_PROC_LIMPIA_ACUM );
     _Per_RecalcularAcumulados.Enabled := Revisa( D_NOM_PROC_RECALC_ACUMULADOS );
     _Per_ImportarMovimientos.Enabled := RevisaCualquiera( D_NOM_PROC_IMP_MOVIMIEN );
     _Per_ExportarMovimientos.Enabled := Revisa( D_NOM_PROC_EXP_MOVIMIEN );
     _Per_ImportarPagoRecibos.Enabled := Revisa( D_NOM_PROC_IMP_PAGO_RECIBO );
     _Per_RefoliarRecibos.Enabled := Revisa( D_NOM_PROC_RE_FOLIAR );
     _Per_CalculaRetroactivos.Enabled := Revisa( D_NOM_PROC_RETROACTIVOS );
     _Per_CopiarNomina.Enabled := Revisa( D_NOM_PROC_COPIAR_NOMINA );
     _Per_CalcularDiferencias.Enabled := Revisa( D_NOM_PROC_CALC_DIFERENCIAS );
     _Per_PagarFuera.Enabled := Revisa( D_NOM_PROC_PAGAR_FUERA );
     _Per_CancelarPasadas.Enabled := Revisa( D_NOM_PROC_CANCELA_PASADA );
     _Per_LiquidacionGlobal.Enabled := Revisa( D_NOM_PROC_LIQUIDA_GLOBAL );
     _Per_CalcularNetoBruto.Enabled := Revisa( D_NOM_PROC_NETO_BRUTO );
     _Per_RastrearCalculo.Enabled := Revisa( D_NOM_PROC_RASTREO );
     _Per_AjusteRetFonacot.Enabled:= Revisa ( D_NOM_PROC_AJUSTE_RET_FONACOT );
     _Per_AjusteRetFonacotCancelacion.Enabled:= Revisa ( D_NOM_PROC_AJUSTE_RET_FONACOT_CANC );
     _Per_CalcularPagoFonacot.Enabled:= Revisa ( D_NOM_PROC_CALCULO_PAGO_FONACOT );
     _Per_ImportarCedulasFonacot.Enabled:= Revisa ( D_NOM_FONACOT_IMPORTAR_CEDULA);
     _Per_GenerarCedulaFonacot.Enabled:= Revisa ( D_NOM_FONACOT_GENERAR_CEDULA);
     _Per_EliminarCedulaFonacot.Enabled := Revisa ( D_NOM_FONACOT_ELIMINAR_CEDULA );
     _Per_DefinirPeriodos.Enabled := Revisa( D_NOM_ANUAL_PERIODOS );
     _Per_CalcularAguinaldos.Enabled := Revisa( D_NOM_ANUAL_CALC_AGUINALDO );
     _Per_ImprimirAguinaldos.Enabled := Revisa( D_NOM_ANUAL_IMP_AGUINALDO );
     _Per_PagarAguinaldos.Enabled := Revisa( D_NOM_ANUAL_PAGA_AGUINALDO );
     _Per_CalcularReparto.Enabled := Revisa( D_NOM_ANUAL_CALC_AHORRO );
     _Per_ImprimirReparto.Enabled := Revisa( D_NOM_ANUAL_IMP_AHORRO );
     _Per_PagarReparto.Enabled := Revisa( D_NOM_ANUAL_CALC_AHORROS ); { �Repetido con D_NOM_ANUAL_CALC_AHORRO (108)? }
     _Per_CierreAhorros.Enabled := Revisa( D_NOM_ANUAL_CIERRE_AHORRO );
     _Per_CierrePrestamos.Enabled := Revisa( D_NOM_ANUAL_CIERRE_PRESTA );
     _Per_CalcularPTU.Enabled := Revisa( D_NOM_ANUAL_CALC_PTU );
     _Per_ImprimirPTU.Enabled := Revisa( D_NOM_ANUAL_IMP_PTU );
     _Per_PagarPTU.Enabled := Revisa( D_NOM_ANUAL_PAGA_PTU );
     _Per_CalcularDeclaracion.Enabled := Revisa( D_NOM_ANUAL_CREDITO );
     _Per_ImprimirDeclaracion.Enabled := Revisa( D_NOM_ANUAL_IMP_CREDITO );
     _Per_CalcularComparativo.Enabled := Revisa( D_NOM_ANUAL_COMPARA_ISPT );
     _Per_ImprimirComparativo.Enabled := Revisa( D_NOM_ANUAL_IMP_COMPARA );
     _Per_PagarComparativo.Enabled := Revisa( D_NOM_ANUAL_DIF_COMPARA );
     _Per_DeclaracionAnual.Enabled := Revisa( D_NOM_ANUAL_DECLARACION_ANUAL );
     _Per_CierreDeclaracionAnual.Enabled := Revisa( D_NOM_ANUAL_DECLARACION_ANUAL_CIERRE );
     _Per_ImprimirDeclaracionAnual.Enabled := Revisa( D_NOM_ANUAL_IMP_DECLARACION_ANUAL );
     _Emp_Importar_Asistencia_Sesiones.Enabled := Revisa( D_EMP_PROC_IMP_ASISTENCIA_SESIONES );
     _Emp_Cancelar_Cierre_Vacaciones.Enabled := Revisa( D_EMP_PROC_CANCELA_CIERRE_VACA );
     _Emp_Cancelar_Permisos_Globales.Enabled := Revisa( D_EMP_PROC_CANCELA_PERMISO_GLOBAL );
     _Emp_Importar_Aho_Pre.Enabled := Revisa( D_EMP_PROC_IMP_AHORROS_PRESTAMOS );
     _Emp_Recalcula_Saldos_Vaca.Enabled := Revisa( D_EMP_PROC_RECALCULA_SALDOS_VACA );
     _Emp_Cambio_Plaza.Visible := dmCliente.UsaPlazas;
     _Emp_Cambio_Turno.Visible := not dmCliente.UsaPlazas;
     _Emp_Cambio_Area.Visible := not dmCliente.UsaPlazas;
     _Emp_Cambios_Multiples.Visible := not dmCliente.UsaPlazas; //Pendiente de implementar
     _Emp_Tarjetas_Gasolina.Visible := CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_ADICIONAL9 );
     _Emp_Tarjetas_Despensa.Visible := CheckDerecho( D_EMP_DATOS_OTROS_CUENTAS, K_DERECHO_CONFIGURA  );
     _Emp_Foliar_Capacitaciones.Enabled := Revisa( D_EMP_PROC_FOLIAR_CAPACITACIONES );
     _Emp_Revisar_Datos_STPS.Enabled := Revisa( D_EMP_PROC_REVISAR_CAPACITACIONES );
     _Emp_Reiniciar_Capacitaciones_STPS.Enabled := Revisa( D_EMP_PROC_REINICIAR_CAPACITACIONES );
     _Asi_AutorizacionPreNomina.Visible := RevisaCualquiera( D_ASIS_PROC_AUTORIZ_PRENOM);
     _Per_TransferenciasCosteo.Visible := dmCliente.ModuloAutorizadoCosteo and (Revisa( D_NOM_PROC_TRANSFERENCIAS_COSTEO )) and ( not dmCliente.Sentinel_EsProfesional_MSSQL ) ;
     _Per_Previo_ISR.Visible := Revisa ( D_NOM_PROC_PREVIO_ISR );
     _Per_Imprimir_PrevioISR.Visible := Revisa( D_NOM_PROC_PREVIO_ISR_IMPRIMIR );
     _Emp_Importar_SGM.Enabled := Revisa( D_EMP_PROC_IMP_SGM );
     _Emp_Renovacion_SGM.Enabled := Revisa( D_EMP_PROC_RENUEVA_SGM );
     _Emp_Borrar_SGM.Enabled := Revisa( D_EMP_PROC_BORRA_SGM  );

     _Per_Calculo_Costeo.Visible := dmCliente.ModuloAutorizadoCosteo and ( Revisa( D_NOM_PROC_CALCULO_COSTEO ) )and ( not dmCliente.Sentinel_EsProfesional_MSSQL );
     _Per_Cancelacion_Transferencias.Visible := dmCliente.ModuloAutorizadoCosteo and ( Revisa( D_NOM_PROC_CANCEL_TRANSF_COSTEO ) ) and ( not dmCliente.Sentinel_EsProfesional_MSSQL );
     _Emp_ImportarOrganigrama.Visible := Revisa( D_EMP_PROC_IMPORTA_ORG );
     _Sist_Agrega_Base_Datos_Seleccion.Visible := Revisa (D_SIST_PROC_BD_SELECCION);
     _Sist_Agrega_Base_Datos_Visitantes.Visible := Revisa (D_SIST_PROC_BD_VISITANTES);
     _Sist_Agrega_Base_Datos_Pruebas.Visible := Revisa (D_SIST_PROC_BD_PRUEBAS);
     _Sist_Agrega_Base_Datos_Empleados.Visible := Revisa (D_SIST_PROC_BD_TRESS);
     _Sist_Agrega_Base_Datos_Presupuestos.Visible := Revisa (D_SIST_PROC_BD_PRESUPUESTOS);
     _Sist_Administrador_Envios_Programados.Enabled := CheckDerecho( D_SIST_CALENDARIO_REPORTES, K_DERECHO_ALTA )  and dmcliente.EmpresaAbierta;

     {*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
     _Per_ImportarMovimientos.Enabled := dmcliente.EmpresaAbierta and CheckDerecho( D_NOM_PROC_IMP_MOVIMIEN, K_DERECHO_CONSULTA );
     _Per_ImportarAcumulados.Enabled  := CheckDerecho( D_NOM_PROC_IMP_MOVIMIEN, K_DERECHO_ALTA );
     {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
     _Per_RecalcularAhorros.Enabled := Revisa( D_NOM_PROC_RECALC_AHORROS );
     _Sist_ImportarCafeteria.Enabled := Revisa( D_SIST_IMPORTAR_CAFETERIA ) and dmcliente.EmpresaAbierta;
     {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
     _Per_RecalcularPrestamos.Enabled := Revisa( D_NOM_PROC_RECALC_PRESTAMOS );


     with EmpCambioPuesto do
     begin
            if dmCliente.UsaPlazas then
              begin
                  Caption := 'Clasificaci�n';
                  Hint := 'Cambiar clasificaci�n a este empleado';
              end
              else
              begin
                  Caption := 'P&uesto';
                  Hint := 'Cambiar puesto a este empleado';
              end;
     end;
            prendeapagasubitem(EmpMovOtros);
            prendeapagasubitem(EmpGlobalesSub);
            prendeapagasubitem(EmpMasivosSub);
            prendeapagasubitem(EmpTarjetasSub);
            prendeapagasubitem(HerramientasSubItem);
            prendeapagasubitem(EmpBeneficiosSub);
            prendeapagasubitem(ExpOtrosSubitem);
            prendeapagasubitem(CapaCurOtrosSub);
            prendeapagasubitem(AsisPolleoSub);
            prendeapagasubitem(NomExcepMovImport);
            prendeapagasubitem(nomFiniquitosSub);
            prendeapagasubitem(NomOtros);
            prendeapagasubitem(NomGeneraPolizasSub);
            prendeapagasubitem(NomRecibosSub);
            prendeapagasubitem(NomHerramientasSub);
            prendeapagasubitem(NomCalculoPrevioISRSub);
            prendeapagasubitem(NomCosteoNomSub);
            prendeapagasubitem(NomCalcularSub);
            prendeapagasubitem(NomDefinirSub);
            prendeapagasubitem(NomAnualesSub);
            PrendeApagaSubitem(SistAgregaBaseDatos);
            PrendeApagaSubitem(SistTerminalesGTIChecadas);
end;

procedure TTressShell.DevEx_BarManagerClickItem(Sender: TdxBarManager;
  ClickedItem: TdxBarItem);
begin
      inherited;
      if ClickedItem.Action <> nil then
      begin
          if ClickedItem.Tag <> -1 then
          begin
               SetAccesosRapidos(ClickedItem.Index);
          end;
      end;

end;

procedure TTressShell.DoCloseAll;
begin
     {*** US 14266: Las gr�ficas se refrescan cada vez que cambia el empleado activo ***}
     ActualizarGraficasInicio := True;
     {*** FIN ***}
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          BorrarModales;
          dmDiccionario.CierraEmpresa;
          dmCliente.CierraEmpresa;
          CierraDatasets( dmConteo );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmCafeteria );
          CierraDatasets( dmReportes );
          CierraDatasets( dmIMSS );
          CierraDatasets( dmNomina );
          CierraDatasets( dmAsistencia );
          CierraDatasets( dmRecursos );
          CierraDatasets( dmTablas );
          CierraDatasets( dmCatalogos );
          CierraDatasets( dmSistema );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.BorrarModales;
begin
      FreeAndNil(EditEmpAdicionales_DevEx);
     //Borra la pantalla de Alta  si fue creada durante la sesi�n de usuario
      FreeAndNil( EmpAlta_DevEx );
end;

function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;
begin
     {$ifdef MULTIPLES_ENTIDADES}
     Result := Dentro( eEntidad , [enEmpleado,enPeriodo, enAccArbol] );
     {$else}
     Result := ( eEntidad in [enEmpleado,enPeriodo, enAccArbol] );
     {$endif}

     if Result then
     begin
           case eEntidad of
              enEmpleado : lEncontrado := ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( VACIO, sKey, sDescription );
              enPeriodo : lEncontrado := ZetaBuscaPeriodo_DevEx.BuscaPeriodoDialogo( oLookUp, VACIO, sKey, sDescription );
              enAccArbol: lEncontrado := ZetaBuscaDerechosAcceso_DevEx.BuscaDerechosAccesoDialogo( oLookup, sKey, sDescription );

          end;
     end;
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enClasifi: Result := dmCatalogos.cdsClasifi;
          enEmpleado: Result := dmCliente.cdsEmpleadoLookUp;
          enConcepto: Result := dmCatalogos.cdsConceptos;
          enContrato: Result := dmCatalogos.cdsContratos;
          enCurso: Result := dmCatalogos.cdsCursos;
          enEdoCivil: Result := dmTablas.cdsEstadoCivil;
          enEntidad: Result := dmTablas.cdsEstado;
          enMunicipio: Result := dmTablas.cdsMunicipios;
          enEstudios: Result := dmTablas.cdsEstudios;
          enEvento: Result := dmCatalogos.cdsEventos;
          enExtra1: Result := dmTablas.cdsExtra1;
          enExtra2: Result := dmTablas.cdsExtra2;
          enExtra3: Result := dmTablas.cdsExtra3;
          enExtra4: Result := dmTablas.cdsExtra4;
          enExtra5: Result := dmTablas.cdsExtra5;
          enExtra6: Result := dmTablas.cdsExtra6;
          enExtra7: Result := dmTablas.cdsExtra7;
          enExtra8: Result := dmTablas.cdsExtra8;
          enExtra9: Result := dmTablas.cdsExtra9;
          enExtra10: Result := dmTablas.cdsExtra10;
          enExtra11: Result := dmTablas.cdsExtra11;
          enExtra12: Result := dmTablas.cdsExtra12;
          enExtra13: Result := dmTablas.cdsExtra13;
          enExtra14: Result := dmTablas.cdsExtra14;
          enFolio: Result := dmCatalogos.cdsFolios;
          enHorario: Result := dmCatalogos.cdsHorarios;
          enIncidencia: Result := dmTablas.cdsIncidencias;
          enMaestros: Result := dmCatalogos.cdsMaestros;
          enMoneda: Result := dmTablas.cdsMonedas;
          enMotBaja: Result := dmTablas.cdsMotivoBaja;
          enNivel1: Result := dmTablas.cdsNivel1;
          enNivel2: Result := dmTablas.cdsNivel2;
          enNivel3: Result := dmTablas.cdsNivel3;
          enNivel4: Result := dmTablas.cdsNivel4;
          enNivel5: Result := dmTablas.cdsNivel5;
          enNivel6: Result := dmTablas.cdsNivel6;
          enNivel7: Result := dmTablas.cdsNivel7;
          enNivel8: Result := dmTablas.cdsNivel8;
          enNivel9: Result := dmTablas.cdsNivel9;
          enOtrasPer: Result := dmCatalogos.cdsOtrasPer;
          enPeriodo: Result := dmCatalogos.cdsPeriodo;
          enPrestacion: Result := dmCatalogos.cdsPrestaci;
          enPRiesgo: Result := dmCatalogos.cdsPRiesgo;
          enPuesto: Result := dmCatalogos.cdsPuestos;
          enQuerys: Result := dmCatalogos.cdsCondiciones;
          enRPatron: Result := dmCatalogos.cdsRPatron;
          enSSocial: Result := dmCatalogos.cdsSSocial;
          enTAhorro: Result := dmTablas.cdsTAhorro;
          enTCurso: Result := dmTablas.cdsTipoCursos;
          enCCurso: Result := dmTablas.cdsClasifiCurso;
          enTKardex: Result := dmTablas.cdsMovKardex;
          enTPresta: Result := dmTablas.cdsTPresta;
          enTransporte: Result := dmTablas.cdsTransporte;
          enTurno: Result := dmCatalogos.cdsTurnos;
          enViveCon: Result := dmTablas.cdsViveCon;
          enViveEn: Result := dmTablas.cdsHabitacion;
{         ER - 2.5 : Se cambi� a TZetaClientDataSet por que no se ocupa un lookup hacia �l - La llave es Flotante: A80_LI
          enArt_80: Result := dmTablas.cdsArt80;
}
          enNumerica: Result := dmTablas.cdsNumericas;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enNomParam: Result := dmCatalogos.cdsNomParam;
          enOrdFolio: Result := dmCatalogos.cdsOrdFolios;
          enRegla: Result := dmCatalogos.cdsReglas;
          enInvitador: Result := dmCatalogos.cdsInvitadores;
          enMotAuto: Result :=  dmTablas.cdsMotAuto;
          enNivel0: Result := dmSistema.cdsNivel0;
          enTools: Result := dmCatalogos.cdsTools;
          enMotTool: Result := dmTablas.cdsMotTool;
          enTalla: Result := dmTablas.cdsTallas;
          enReporte:  Result := dmReportes.cdsLookupReportes;
          enArea: Result := dmCatalogos.cdsAreas;
          enAccRegla: Result := dmCatalogos.cdsAccReglas;
          enColonia: Result := dmTablas.cdsColonia;
          enGrupoAdicional: Result := dmSistema.cdsGruposAdic;
          enAula: Result := dmCatalogos.cdsAulas;
          enCertific: Result := dmCatalogos.cdsCertificaciones;
          enPolTipo:  Result := dmCatalogos.cdsTiposPoliza;
          enPlazas: Result := dmRecursos.cdsPlazasLookup;
          enSeccPerfil: Result := dmCatalogos.cdsSeccionesPerfil;
          enPerfilPuesto: Result := dmCatalogos.cdsPerfilesPuesto;
          enProvCap: Result:= dmCatalogos.cdsProvCap;
          enMotCheca: Result:= dmTablas.cdsMotCheca;
          enAccArbol: Result:= dmSistema.cdsAccArbol;
          enOcupaNac : Result := dmTablas.cdsOcupaNac;
          enAreaTematica : Result := dmTablas.cdsAreaTemCur;
          enEstablecimiento : Result := dmCatalogos.cdsEstablecimientos;
          {$ifndef DOS_CAPAS}
          enGruposCosteo : Result := dmCatalogos.cdsCosteoGrupos;
          enCriteriosCosteo : Result := dmCatalogos.cdsCosteoCriterios;
          {$endif}
          enBancos : Result := dmTablas.cdsBancos;
     else
         Result := nil;
     end;
end;

function TTressShell.ValidaStatusNomina( const sProceso: String ): Boolean;
begin
     if dmCliente.NominaYaAfectada then
     begin
          ZetaDialogo.zInformation( sProceso, 'La N�mina Ya Est� Afectada' + CR_LF + 'No Se Puede Realizar El Proceso', 0 );
          Result := False;
     end
     else
         Result := True;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
    Result := ( FormaActiva <> NIL ) and
              ( FormaActiva.TipoValorActivo1 = stEmpleado ) and
              ( FormaActiva.TipoValorActivo2 = stPeriodo );
end;

procedure TTressShell.ChangeTimerInfo;
begin
     SetTimerInfo;
end;

{ ********** Manejo de Valores Activos ************ }

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); 
{$endif}
begin
     dmRecursos.NotifyDataChange( Entidades, Estado );
     dmNomina.NotifyDataChange( Entidades, Estado );
     dmAsistencia.NotifyDataChange( Entidades, Estado );
     dmIMSS.NotifyDataChange( Entidades, Estado );
     dmCatalogos.NotifyDataChange( Entidades, Estado );
     dmSistema.NotifyDataChange( Entidades, Estado );
     dmCafeteria.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.ReinitPeriodo;
begin
     with dmCliente do
     begin
          if ( PeriodoNumero = 0 ) then
             InitActivosPeriodo;
     end;
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TTressShell.SetDataChange(const Entidades: ListaEntidades); 
{$endif}
begin
     { Cambios a Valores Activos }
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enPeriodo , Entidades ) then
     {$else}
     if ( enPeriodo in Entidades ) then
     {$endif}
     begin
          RefrescaPeriodoInfo;
          CargaPeriodoActivos;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enEmpleado , Entidades ) then
     {$else}
     if ( enEmpleado in Entidades ) then
     {$endif}
     begin
          RefrescaEmpleadoInfo;
          RefrescaEmpleadoActivos;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enQuerys , Entidades ) then
     {$else}
     if ( enQuerys in Entidades ) then
     {$endif}
     begin
          RefrescaNavegacionInfo;
     end;

     { Cambios a otros datasets }
     inherited SetDataChange( Entidades );
     //NotifyDataChange( Entidades, stNinguno );
end;

procedure TTressShell.EmpleadoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;

     try
        lOk := dmCliente.SetEmpleadoNumero( EmpleadoNumeroCB_DevEx.ValorEntero );
        if lOk then
           CambioEmpleadoActivo
        else
            ZetaDialogo.zInformation( 'Error', '! Empleado no encontrado !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;// end try
end;

procedure TTressShell.AsistenciaFechavalid(Valor:TDate);
begin
     dmCliente.FechaAsistencia :=Valor;
     CambioValoresActivos( stFecha );
end;

procedure TTressShell.PeriodoTipoCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     with dmCliente do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          Application.ProcessMessages;
          try
             {$ifdef ANTES}
             PeriodoTipo := eTipoPeriodo( PeriodoTipoCB.Indice );
             {$else}
             PeriodoTipo := eTipoPeriodo( StrToIntDef( PeriodoTipoCB_DevEx2.Llave, 0 ) );
             {$endif}
             CambioPeriodoActivo;
             if ( GetDatosPeriodoActivo.Numero = 0 ) then
                ZetaDialogo.zInformation( 'Aviso', 'No Se Ha Definido Ning�n Per�odo ' + PeriodoTipoCB_DevEx2.Text, 0 )
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TTressShell.PeriodoNumeroCBLookUp(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        //DevEx (by am): Se agrega la validacion de usuarios para utilizar el combo de numero de Periodo correspondiente.
        lOk := dmCliente.SetPeriodoNumero( PeriodoNumeroCB_DevEx2.ValorEntero );
        if lOk then
        begin
             RefrescaPeriodoActivos;
             CambioValoresActivos( stPeriodo );
        end
        else
            ZetaDialogo.zInformation( 'Error', '! No Existe El Per�odo De N�mina !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;
procedure TTressShell.SistemaFechaZFValidDate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          FechaDefault := SistemaFechaZF_DevEx.Valor;
     end;
     RefrescaSistemaActivos;
     CambioValoresActivos( stSistema );
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     with dmCliente do
     begin
          EmpleadoNumeroCB_DevEx.ValorEntero := Empleado; //DevEx
     end;
     RefrescaEmpleadoActivos;
end;

//DevEx (by am): Modificado para utilizar el combo correspondiente a la vista en la que se encuentre el usuario.
procedure TTressShell.CargaNavegacion;
begin
     with NavegacionCB_DevEx do
     begin
          ItemIndex := dmCatalogos.LlenaNavegacion( Properties.Items, dmCatalogos.Navegacion );
          if RevisaDerechoForma( efcCatCondiciones, K_DERECHO_ALTA) then
             Properties.Items.Add('Nuevo...');
          Properties.DropDownRows := iMin( Properties.Items.Count, 12 );   // M�ximo muestra 12 sin que aparezca el Scroll
     end;
     StatusBarMsg( dmCatalogos.GetNavegacionDescripcion, K_PANEL_NAVEGA );
end;

function TTressShell.RevisaDerechoForma( FormaConsulta: eFormaConsulta; const TipoDerecho: Integer ): Boolean;
Begin
     Result := False;
     if ZAccesosMgr.CheckDerecho( GetPropConsulta( FormaConsulta ).IndexDerechos, TipoDerecho ) then
        Result := True;
end;

procedure TTressShell.RefrescaNavegacionInfo;
begin
     CargaNavegacion;
end;

//DevEx (by am): Modificado para utilizar el combo correspondiente a la vista en la que se encuentre el usuario.
procedure TTressShell.NavegacionCBSelect(Sender: TObject);
var
   oTexto : TStringObject;
begin
     inherited;
          with NavegacionCB_DevEx do
          begin
               {***DevEx (by am): Se agrega la validacion de ItemIndex <> -1 porque los nuevos Combos no tiene evento OnSelect.
                                  Por lo tanto se ligo este metodo al evento OnChange***}
               if ItemIndex <> -1 then
               begin
                    if ItemIndex = 0 then
                    begin
                         StatusBarMsg( 'Navegaci�n: Todos', K_PANEL_NAVEGA );
                         dmCatalogos.Navegacion := VACIO;
                    end
                    else if ( RevisaDerechoForma( efcCatCondiciones, K_DERECHO_ALTA) ) and ( ItemIndex = ( Properties.Items.Count - 1 ) ) then
                         dmCatalogos.AbreFormaEdicionCondicion
                    else
                    begin
                         oTexto:= TStringObject( Properties.Items.Objects[ItemIndex] );
                         dmCatalogos.Navegacion := oTexto.Texto;
                         StatusBarMsg( dmCatalogos.GetNavegacionDescripcion, K_PANEL_NAVEGA );
                    end;
               end;
          end;
     dmCliente.InitNavegadorEmpleado;
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

//DevEx (by am): Se modifica este metodo para desplegar de distinta forma el nombre y status del empleado segun la vista en la que se encuentre.
procedure TTressShell.RefrescaEmpleadoActivos;
var
   oColor : TColor;
   i:Integer;
   sCaption, sHint : String;

   APngImage: TdxPNGImage;
   ABitmap: TcxAlphaBitmap;
begin
     {with dmCliente do
     begin
           StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
           with GetDatosEmpleadoActivo do
           begin
                EmpleadoPrettyName.Caption := Nombre;
                dmCliente.GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
                with EmpleadoActivoPanel do
                begin
                     Font.Color := oColor;
                     Caption := sCaption;
                     Hint := sHint;
                end;
           end;
     end;}   //OLD

     with dmCliente do
     begin
          StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          //MuestraDatosUsuarioActivo; //DevEx(by am): Se refresca el usuario y el grupo en el Panel de valores activos
          with GetDatosEmpleadoActivo do
          begin
               EmpleadoPrettyName_DevEx.Caption := Nombre;
               GetStatusEmpleado(GetDatosEmpleadoActivo, sCaption, sHint, oColor);
               //DevEx (by am): Inicializando Bitmat y PNG
               ABitmap := TcxAlphaBitmap.CreateSize(24, 24);
               APngImage := nil;
               for i := Low(StatusEmpleado) to High(StatusEmpleado) do
               begin
                    if (sCaption = StatusEmpleado[i]) then
                    begin
                         try
                               //DevEx (by am): Creando el Bitmap
                               ABitmap.Clear;
                               Image24_StatusEmpleado.GetBitmap(i, ABitmap);
                               //DevEx (by am): Creando el PNG
                               APngImage := TdxPNGImage.CreateFromBitmap(ABitmap);
                               ImagenStatus_DevEx.Picture.Graphic := APngImage;
                               ImagenStatus_DevEx.Hint := sHint;
                         finally
                                APngImage.Free;
                                ABitmap.Free;
                         end;
                         Break;
                    end;
               end;
          end;
     end;
end;

procedure TTressShell.RefrescaEmpleadoInfo;
begin
     dmCliente.RefrescaEmpleado;
end;

procedure TTressShell.RefrescaEmpleado;
begin
     RefrescaEmpleadoInfo;
     CambiaEmpleadoActivos;
end;

procedure TTressShell.SetAsistencia( const dValue: TDate );
begin
     with dmCliente do
     begin
          if ( dValue <> FechaAsistencia ) and ( dValue <> NullDateTime ) then
          begin
               FechaAsistencia := dValue;
               CambioValoresActivos( stFecha );
          end;
     end;
end;

procedure TTressShell.SetBloqueo;
 var
    lVisible: Boolean;
begin
     lVisible := not Global.GetGlobalBooleano( K_GLOBAL_BLOQUEO_X_STATUS );
     StatusBar_DevEx.Panels[K_PANEL_FECHA_ASISTENCIA].Visible := lVisible;
     dxBarButtonEditarFecha.Enabled :=  dmCliente.EmpresaAbierta;

end;

procedure TTressShell.CargaPeriodoActivos;
begin
     with dmCliente do
     begin
          {$ifdef ANTES}
          PeriodoTipoCB.Indice := Ord( PeriodoTipo );
          {$else}
          PeriodoTipoCB_DevEx2.Llave := IntToStr( Ord( PeriodoTipo ) );
          {$endif}
          PeriodoNumeroCB_DevEx2.ValorEntero := PeriodoNumero;
     end;
     RefrescaPeriodoActivos;
end;

procedure TTressShell.CambioPeriodoActivo;
begin
     //DevEx (by am): Se agrega la validacion de vista para utilizar el combo de numero de nomina correspondiente.
     PeriodoNumeroCB_DevEx2.ValorEntero := dmCliente.PeriodoNumero;
     RefrescaPeriodoActivos;
     CambioValoresActivos( stPeriodo );
end;

procedure TTressShell.RefrescaPeriodoActivos;
const
     K_ESPACIO = ' ';
     K_VACIO ='';
     K_SIN_DEFINIR = 0;
     K_STATUS_NUEVA = 0;
     K_STATUS_PRENOMINA_PARCIAL = 1;
     K_STATUS_SIN_CALCULAR = 2;
     K_STATUS_CALCULADA_PARCIAL = 3;
     K_STATUS_CALCULADA_TOTAL = 4;
     K_STATUS_AFECTADA_PARCIAL =5;
     K_STATUS_AFECTADA_TOTAL = 6;
var
   FechaDescrip, EstatusTimbradoTemporal: String;
   i, iTimbradas, iPendientes, iTotalEmpleados, iPendientesTimbradas, iCancelacionPendientesTimbradas:Integer;
   TPngImage: TdxPNGImage;
   Parametros, RecibeParametros: TZetaParams;
   TBitmap: TcxAlphaBitmap;
procedure LimpiaTextos;
begin
     PanelPeriodoStatus.Caption := K_VACIO;
     PanelRangoFechas.Caption := K_VACIO;
end;
begin
     with dmCliente do
     begin
          with GetDatosPeriodoActivo do
          begin
          StatusBarMsg( GetPeriodoDescripcion, K_PANEL_PERIODO );
          begin
               //DevEx(by Angie): Se asigna el �cono gris como default para timbrado.
               TBitmap := TcxAlphaBitmap.CreateSize(24, 24);
               TPngImage := nil;
               TimbradoImages.GetBitmap( 0, TBitmap );

               //DevEx (by am): Se define el Rango de Fechas de la nomina agregando el texto a la misma etiqueta donde se encuentra el status.

               LimpiaTextos;
               FechaDescrip := FechaCorta( Inicio );
               if  (( Length( FechaDescrip ) > 0 ) AND ( FechaDescrip <> K_ESPACIO )) then
               begin
                      TempPanelRangoFechas := K_ESPACIO + FechaDescrip + K_ESPACIO +'al'+ K_ESPACIO + FechaCorta( Fin )+K_ESPACIO;
                      //DevEx(by am): Solo se muestra el panel si el width es mayor a 1165, para dar prioridad a que los valores de la nomina se vean de izq. a derecha
                      if (Self.Width > K_MIN_WIDTH)then
                      begin
                           PanelRangoFechas.Caption := TempPanelRangoFechas;
                           if (Self.Canvas.TextWidth(PanelRangoFechas.Caption) >= K_MIN_PANEL_FECHAS_PERIODO) then
                           begin
                               PanelRangoFechas.Caption := K_VACIO;
                               PanelRangoFechas.Width := 1;
                           end;
                      end;

               end;

               {***DevEx (by am): Se define el Status de la nomina.***}
               if ( Numero = K_SIN_DEFINIR ) then
               begin
                    with  PanelPeriodoStatus do
                    begin
                         Font.Color := K_STATUS_COLOR_SINDEFINIR;
                         Caption := K_ESPACIO+'Sin Definir'+K_ESPACIO;
                         Hint := 'Per�odos ' + PeriodoTipoCB_DevEx2.Text + ' No Han Sido Definidos';
                         //DevEx(by Angie): En caso no tener definidos periodos se asigna icono gris y se borra el hint para timbrado
                         TPngImage := TdxPNGImage.CreateFromBitmap( TBitmap );
                         TimbradoStatus_DevEx.Picture.Graphic := TPngImage;
                         TimbradoStatus_DevEx.Hint := K_VACIO;
                    end;
               end
               else
               begin
                    with PanelPeriodoStatus,PanelPeriodoStatus.Font do
                    begin
                         if ( Ord( Status ) in [K_STATUS_NUEVA, K_STATUS_PRENOMINA_PARCIAL, K_STATUS_SIN_CALCULAR, K_STATUS_CALCULADA_PARCIAL,K_STATUS_CALCULADA_TOTAL]) then
                            Color := K_STATUS_COLOR_NUEVA
                         else if ( Ord( Status ) in [K_STATUS_AFECTADA_PARCIAL,K_STATUS_AFECTADA_TOTAL]) then
                              begin
                                   Color := K_STATUS_COLOR_AFECTADA;
                              end
                         else
                             Color := clWindowText;

                         //DevEx(by Angie): Agregar imagen de timbrado segun su estatus
                         EstatusTimbradoTemporal := ZetaCommonLists.ObtieneElemento( lfStatusTimbrado, ord( StatusTimbrado ) );
                         Caption :=K_ESPACIO+ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado ) +K_ESPACIO;
                         //DevEx (by Angie): Inicializando Bitmap y PNG
                         for i := Low( StatusTimbradoArray ) to High( StatusTimbradoArray ) do
                         begin
                              if ( EstatusTimbradoTemporal = StatusTimbradoArray[ i ] ) then
                              begin
                                   try
                                          TBitmap.Clear;
                                          if ( ( ord( StatusTimbrado ) = 0 ) and ( Ord( Status ) <= 4 ) ) then
                                          begin
                                               TimbradoImages.GetBitmap( 0, TBitmap );
                                          end
                                          else
                                              TimbradoImages.GetBitmap( i, TBitmap );

                                          TPngImage := TdxPNGImage.CreateFromBitmap( TBitmap );
                                          TimbradoStatus_DevEx.Picture.Graphic := TPngImage;
                                          try
                                             Parametros := TZetaParams.Create;
                                             with Parametros do
                                             begin
                                                  AddInteger('Anio', dmCliente.GetDatosPeriodoActivo.Year );
                                                  AddInteger('Tipo', dmCliente.GetDatosPeriodoActivo.Tipo );
                                                  AddInteger('Numero', dmCliente.GetDatosPeriodoActivo.Numero );
                                                  AddString('Nivel0', dmCliente.Confidencialidad );
                                             end;

                                             RecibeParametros := TZetaParams.Create;

                                             RecibeParametros.VarValues := dmProcesos.GetStatusTimbrado(Parametros);
                                             iTimbradas := RecibeParametros.ParamValues['Timbradas'];
                                             iPendientes := RecibeParametros.ParamValues['Pendientes'];
                                             iTotalEmpleados := iTimbradas + iPendientes;
                                             //Timbrado Pendiente
                                             iPendientesTimbradas := RecibeParametros.ParamValues['TimbradoPendientes'];
                                             iCancelacionPendientesTimbradas := RecibeParametros.ParamValues['CancelacionPendiente'];

                                          finally
                                                 Parametros.Free;
                                          end;

                                          //DevEx (by Angie): Asignando el mensaje al Hint dependendiendo de los estados
                                          if( ( ord( StatusTimbrado ) = 0 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                              TimbradoStatus_DevEx.Hint := 'Sin timbrar'
                                          else
                                              if( ( ord( StatusTimbrado ) = 1 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                  TimbradoStatus_DevEx.Hint := 'Timbrada Parcial ' + IntToStr(iTimbradas) + '/' + IntToStr(iTotalEmpleados)
                                              else
                                                  if( ( ord( StatusTimbrado ) = 2 ) and ( ( ord( Status ) = 6 ) ) ) then
                                                      TimbradoStatus_DevEx.Hint := 'Timbrada '
                                                      else
                                                          if( ( ord( StatusTimbrado ) = 3 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                               TimbradoStatus_DevEx.Hint := 'Timbrado pendiente ( existen ' + IntToStr(iPendientesTimbradas) + ' recibos que ocupan intentar timbrarse de nuevo )'
                                                           else
                                                               if( ( ord( StatusTimbrado ) = 4 ) and ( ( ord( Status ) = 5 ) or ( ord( Status ) = 6 ) ) ) then
                                                                   TimbradoStatus_DevEx.Hint := 'Cancelaci�n pendiente ( existen ' + IntToStr(iCancelacionPendientesTimbradas) + ' recibos que ocupan intentar cancelarse de nuevo )'
                                                               else
                                                                   TimbradoStatus_DevEx.Hint := VACIO;
                                   finally
                                          TPngImage.Free;
                                          TBitmap.Free;
                                   end;
                              end;
                         end;
                         //DevEx(by am): Si el panel del rango de fechas no esta visible se le pone esta informacion al hint del status
                         if ( Length( PanelRangoFechas.Caption ) <=0 ) then
                            Hint := TempPanelRangoFechas
                         else
                            Hint := K_VACIO;
                    end;
               end;
               {***DevEx(by am): Ajustar tamano del panel segun el caption
               Se le suma un width extra por el cambio de FONT al panel, ya que el metodo TextWidth toma en cuenta el font de la forma para el calculo***}
               //PanelPeriodoStatus.Width := Self.Canvas.TextWidth(PanelPeriodoStatus.Caption) + K_EXTRA_WIDTH;
               //PanelRangoFechas.Width := Self.Canvas.TextWidth(PanelRangoFechas.Caption) + K_EXTRA_WIDTH;

               GetFechaLimiteShell; //cargar fecha asistencia StatusBar_DevEx
          end;
          end;
     end;
end;

procedure TTressShell.RefrescaPeriodoInfo;
begin
     dmCliente.RefrescaPeriodo;
end;

procedure TTressShell.RefrescaPeriodo;
begin
     RefrescaPeriodoInfo;
     CambiaPeriodoActivos;
end;

procedure TTressShell.CargaIMSSActivos;
begin
     with dmCliente do
     begin
          StatusBarMsg( GetIMSSDescripcion, K_PANEL_IMSS );
     end;
end;

procedure TTressShell.RefrescaIMSSActivo;
begin
     CambioValoresActivos( stIMSS );
     CambioValoresActivos( stRazonSocial );
     StatusBarMsg( dmCliente.GetIMSSDescripcion, K_PANEL_IMSS );
end;

procedure TTressShell.RefrescaIMSS;
begin
     CambiaIMSSActivos;
end;

//DevEx ( by am ): Se agrega la validacion dela fecha para utilizar el nuevo componente del a�o
procedure TTressShell.CargaSistemaActivos;
begin
     with dmCliente do
     begin
          SistemaYear_DevEx.Text :=  IntToStr (YearDefault);
          SistemaFechaZF_DevEx.Valor := FechaDefault;
          //Valores del StatusBar_DevEx
          dxStaticFechaSistemaValor.Caption := DateToStr(FechaDefault);
     end;
     RefrescaSistemaActivos;
end;

//DevEx (by am): Se agrega la validacion de la fecha para tomar el componente correspondiente asi como la forma de calendario de la vista.
procedure TTressShell.RefrescaSistemaActivos;
begin
     with SistemaFechaZF_DevEx do
     begin
          SistemaFechaDia_DevEx.Caption := DiaSemana( Valor );
          FCalendario_DevEx.SetDefaultDlgFecha( Valor ); { Refresca el Default del Lookup de ZetaFecha }
          dxStaticSistemaFechaDia.Caption := DiaSemana( Valor );
     end;
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     CargaEmpleadoActivos;
     CambioValoresActivos( stEmpleado );
end;

procedure TTressShell.CambiaIMSSActivos;
begin
     CargaIMSSActivos;
     CambioValoresActivos( stIMSS );
end;

procedure TTressShell.CambiaPeriodoActivos;
begin
     CargaPeriodoActivos;
     CambioValoresActivos( stPeriodo );
end;

procedure TTressShell.BuscaNuevoPeriodo( const iTipo, iPeriodoBorrado: Integer );
begin
     with dmCliente do
     begin
          if ( iTipo = Ord( PeriodoTipo ) ) and ( iPeriodoBorrado = PeriodoNumero ) then
          begin
               if not GetPeriodoSiguiente then
                  GetPeriodoAnterior;
               {$ifdef ANTES}
               PeriodoTipo := eTipoPeriodo( PeriodoTipoCB.Indice );
               {$else}
               PeriodoTipo := eTipoPeriodo( StrToIntDef( PeriodoTipoCB_DevEx2.Llave, 0 ) );
               {$endif}
               CambioPeriodoActivo;
          end;
     end;
     if HayFormaAbierta then
        FormaActiva.ReConnect;
end;

procedure TTressShell.CambiaSistemaActivos;
begin
     CargaSistemaActivos;
     CambioValoresActivos( stSistema );
end;

{ ************ Eventos del ActionList *********** }

//DevEx (by am): Cambia el criterio para activar o descactivar la empresa pues los valores activos no dependeran de si hay una forma abierta
procedure TTressShell._E_RefrescarUpdate(Sender: TObject);
begin
     _E_Refrescar.Enabled := HayFormaAbierta;
end;

procedure TTressShell._A_InicioExecute(Sender: TObject);
begin
     inherited;
     if Revisa( D_INICIO ) then
        AbreFormaConsulta( efcInicio );
end;

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcSistUsuarios );
end;

procedure TTressShell._A_PreferenciasExecute(Sender: TObject);
begin
     inherited;
     // Ser�a mejor llamar a 'cdsArbol.Modificar'
     // y que ese m�todo se encargue de lo dem�s.
     dmSistema.PosicionaUsuarioActivo;
     ZBaseDlgModal_DevEx.ShowDlgModal( ArbolConfigura_DevEx, TArbolConfigura_DevEx );
      //LLenaOpcionesGrupos;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     with dmCliente do
     begin
          if (empresaAbierta) then
          begin
               RefrescaPeriodo;
               RefrescaPeriodoActivos;
          end ;
     end;
     inherited;
end;

//DevEx (by am): Se agrega la validacion de vistas para que se tome el combo indicado en este action
procedure TTressShell._E_BuscarEmpleadoExecute(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
        EmpleadoNumeroCB_DevEx.AsignaValorEntero( StrToIntDef( sKey, 0 ) );
end;

procedure TTressShell._E_CambiarEmpleadoExecute(Sender: TObject);
begin
     inherited;
     EmpleadoNumeroCB_DevEx.SetFocus;
end;

procedure TTressShell._V_ArbolOriginalExecute(Sender: TObject);
begin
     inherited;
     if ZConfirm( 'Edici�n Del Arbol De Formas', 'Este Cambio Es S�lo Temporal' + CR_LF + '� Desea Continuar ? ', 0, mbYes ) then
     begin
          {***DevEx(@am): El metodo ZArbolTress.MuestraArbolOriginal, renombra todo el arbol de sistema TRESS en vista la vista clasica,
                          lo cual no es necesario, pues los nombres del arbol del sistema no pueden ser cambiados, los unicos que puden cambiarse
                          son los del arbol del usuario, sin embargo como es Legacy se dejo tal y como esta ese codigo.
                          Para la nueva imagens olo se cambiaran los nombres para el primer grupo, que es donde estaria ubicado el arbol del usuario,
                          en caso de contar con uno***}
          
              if DevEx_NavBar.Groups.Count>0 then //Renoombrara el arbol del Grupo Cero, el arbol del usuario seimpre sea el cero.
                 ZArbolTress.MuestraArbolOriginal( DevEx_NavBar.Groups[0].Control.Controls[0] as TcxTreeView );
     end;
end;

procedure TTressShell._V_EmpleadosExecute(Sender: TObject);
begin
     inherited;
     _E_CambiarEmpleadoExecute( Sender );
end;

procedure TTressShell._V_NominaExecute(Sender: TObject);
begin
     inherited;
     PeriodoNumeroCB_DevEx2.SetFocus;
end;

procedure TTressShell._V_SistemaExecute(Sender: TObject);
begin
     inherited;
     SistemaYear_DevEx.SetFocus;
end;

//DevEx (by am): Se agrega la validacion de vistas a este action para utilizar el combo correspondiente a la vista.
procedure TTressShell._Emp_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_PrimeroUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoDownEnabled;
end;

//DevEx (by am): Se agrega la validacion de vistas a este action para utilizar el combo correspondiente a la vista.
procedure TTressShell._Emp_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

//DevEx (by am): Se agrega la validacion de vistas a este action para utilizar el combo correspondiente a la vista.
procedure TTressShell._Emp_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

//DevEx (by am): Se agrega la validacion de vistas a este action para utilizar el combo correspondiente a la vista.
procedure TTressShell._Emp_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  EmpleadoNumeroCB_DevEx.ValorEntero := Empleado;
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Emp_UltimoUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.EmpleadoUpEnabled;
end;

procedure TTressShell._Emp_AltaExecute(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsDatosEmpleado do
     begin
          Conectar;
          Agregar;
     end;
end;

procedure TTressShell._Emp_BajaExecute(Sender: TObject);
begin
     inherited;
     with dmRecursos.cdsDatosEmpleado do
     begin
          Refrescar;
          Borrar;
     end;
end;

procedure TTressShell._Emp_Cambio_SalarioExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_CAMBIO );
end;

procedure TTressShell._Emp_Cambio_TurnoExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_TURNO );
end;

procedure TTressShell._Emp_ReingresoExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.ProcesaReingreso;
end;

procedure TTressShell._Emp_Cambio_PuestoExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_PUESTO );
end;

procedure TTressShell._Emp_Cambio_AreaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_AREA );
end;

procedure TTressShell._Emp_Cambio_ContratoExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_RENOVA );
end;

procedure TTressShell._Emp_Cambio_MasivosExecute(Sender: TObject);
begin
  inherited;
  DProcesos.ShowWizard( prEmpCambioTurnoMasivo )
end;

procedure TTressShell._Emp_Cambio_PrestacionExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_PRESTA );
end;

procedure TTressShell._Emp_Cambios_MultiplesExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardexMultiple;
end;

procedure TTressShell._Emp_VacacionesExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_VACA );
end;

procedure TTressShell._Emp_Cerrar_VacacionesExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaCierreVacacion;
end;

procedure TTressShell._Emp_IncapacidadExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridIncapaci;
     //dmRecursos.AgregaKardex( K_T_INCA );
end;

procedure TTressShell._Emp_PermisoExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_PERM );
end;

procedure TTressShell._Emp_KardexExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( '' );
end;

procedure TTressShell._Emp_CursoExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        dmRecursos.EditarGridCursos;
end;

procedure TTressShell._Emp_ComidasExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCafeteria ) then
        dmCafeteria.EditarGridComidas( True );
end;

procedure TTressShell._Emp_Banca_ElecExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridBancaElectronica;
end;

procedure TTressShell._Emp_Credito_InfonavitExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridInfonavit;
end;

procedure TTressShell._Emp_EntregaHerramientaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridEmpTools;
end;

procedure TTressShell._Emp_EntregaGlobalExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridGlobalTools;
end;

procedure TTressShell._Emp_RegresarHerramientaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridRegresarTools;
end;

procedure TTressShell._Emp_PrestamosExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridPrestamos;
end;

procedure TTressShell._Emp_Recalcular_IntegradosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHSalarioIntegrado );
end;

procedure TTressShell._Emp_Promediar_VariablesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHPromediarVariables );
     { AP(23/May/2008): Proceso descontinuado
     ZetaDialogo.zInformation( 'Aviso', 'Se recomienda ejecutar el c�lculo de Tasa de INFONAVIT' + CR_LF +
                                        'al terminar de promediar Percepciones Variables', 0 );  }
end;

procedure TTressShell._Emp_Salario_GlobalExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHCambioSalario );
end;

procedure TTressShell._Emp_Aplicar_TabuladorExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHAplicarTabulador );
end;

procedure TTressShell._Emp_Vacaciones_GlobalesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHVacacionesGlobales );
end;

procedure TTressShell._Emp_Cancelar_VacacionesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHCancVacGlobales );
end;

procedure TTressShell._Emp_CierreGlobal_VacacionesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHCierreVacacionGlobal );
end;

procedure TTressShell._Emp_Entregar_HerramientaExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okHerramientas ) then
        DProcesos.ShowWizard( prRHEntregarHerramienta );
end;

procedure TTressShell._Emp_Regresar_HerramientaExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okHerramientas ) then
        DProcesos.ShowWizard( prRHRegresarHerramienta );
end;

procedure TTressShell._Emp_Aplicar_EventosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHAplicarEventos );
end;

procedure TTressShell._Emp_Importar_KardexExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHImportarKardex );
end;

procedure TTressShell._Emp_Cancelar_KardexExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHCancelarKardex );
end;

procedure TTressShell._Emp_Importar_Asistencia_SesionesExecute(Sender: TObject);
begin
     {***US 11759: Proteger modulo cursos por sentinel, est� abierta la captura de cursos tomados individual***}
     if dmCliente.ModuloAutorizado( okCursos ) then
     begin
          inherited;
          DProcesos.ShowWizard( prRHImportarAsistenciaSesiones );
     end;
end;

procedure TTressShell._Emp_Curso_GlobalExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard( prRHCursoTomado );
end;

procedure TTressShell._Emp_TransferenciaExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHTranferencia );
end;

procedure TTressShell._Emp_Importar_AltasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHImportarAltas );
end;

procedure TTressShell._Emp_RenumerarExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHRenumeraEmpleados );
end;

procedure TTressShell._Emp_Permiso_GlobalExecute(Sender: TObject);
begin
     inherited;
     dProcesos.ShowWizard( prRHPermisoGlobal );
end;

procedure TTressShell._Emp_Cancelar_Permisos_GlobalesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHCancelarPermisosGlobales );
end;

procedure TTressShell._Emp_Borrar_Curso_GlobalExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard( prRHBorrarCursoTomado );
end;

procedure TTressShell._Asi_AutorizarExecute(Sender: TObject);
begin
     inherited;
     dmAsistencia.EditarGridAutorizaciones( True );
end;

procedure TTressShell._Asi_Horarios_TemporalesExecute(Sender: TObject);
begin
     inherited;
     dmAsistencia.EditarGridHorarioTemp;
end;

procedure TTressShell._Asi_POLLExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okAsistencia ) then
        DProcesos.ShowWizard( prASISPollRelojes );
end;

procedure TTressShell._Asi_Procesar_TarjetasExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okAsistencia ) then
        DProcesos.ShowWizard( prASISProcesarTarjetas );
end;

procedure TTressShell._Asi_PrenominaExecute(Sender: TObject);
var
   sMensaje: String;

   function NoEsNominaEspecial: Boolean;
   begin
        Result := dmCliente.GetDatosPeriodoActivo.Uso <> upEspecial;
        if not Result then
           sMensaje:= 'Solo Se Permite Calcular Pre-N�minas De Uso Normal';
   end;

begin
     inherited;
     if ( dmCliente.ModuloAutorizadoConsulta( okAsistencia, sMensaje ) ) and
        ( dmNomina.ValidaAfectada( sMensaje, TRUE, 'Modificar' ) ) and
        ( NoEsNominaEspecial ) then
     begin
          DProcesos.ShowWizard( prASISCalculoPreNomina )
     end
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TTressShell._Asi_ExtrasPermisosGlobalesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prASISExtrasPer );
end;

procedure TTressShell._Asi_Registros_AutomaticosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prASISRegistrosAut );
end;

procedure TTressShell._Asi_Corregir_FechasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prASISCorregirFechas );
end;

procedure TTressShell._Asi_Recalcular_TarjetasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prASISRecalculoTarjetas );
end;

procedure TTressShell._Asi_FechaLimiteExecute(Sender: TObject);
begin
     inherited;
     ZBaseDlgModal_DevEx.ShowDlgModal( EditFechaLimite_DevEx, TEditFechaLimite_DevEx );
     GetFechaLimiteShell;
end;

procedure TTressShell._Asi_IntercambioFestivoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard(prASISIntercambioFestivo);
end;

procedure TTressShell._Asi_CancelarExcepcionesFestivosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard(prASISCancelarExcepcionesFestivos);
end;

procedure TTressShell._Asi_RegistrarExcepcionesFestivoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard(prASISRegistrarExcepcionesFestivos);
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     Global.Conectar;
     dxStaticFechaAsistenciaValor.Caption := FechaCorta( Global.GetGlobalDate( K_GLOBAL_FECHA_LIMITE ) );
end;

procedure TTressShell._Per_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoPrimero then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoAnterior then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_AnteriorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoDownEnabled;
end;

procedure TTressShell._Per_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoSiguiente then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_SiguienteUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

procedure TTressShell._Per_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetPeriodoUltimo then
             begin
                  CambioPeriodoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Per_NominaNuevaExecute(Sender: TObject);
begin
     inherited;
     dmCatalogos.AgregaPeriodo;
end;

procedure TTressShell._Per_BorrarNominaActivaExecute(Sender: TObject);
begin
     inherited;
     dmNomina.BorrarNominaActiva(True);
end;

procedure TTressShell._Per_ExcepcionDiaHoraExecute(Sender: TObject);
var
   sMensaje : String;
begin
     inherited;
     with dmNomina do
     begin
          if ValidaAfectada( sMensaje, TRUE, 'Agregar', True ) then
             EditarGridDiasHoras( True )
          else
              ZetaDialogo.zInformation( 'Excepciones de D�as/Horas', sMensaje, 0 );
     end;
end;

procedure TTressShell._Per_ExcepcionMontoExecute(Sender: TObject);
var
   sMensaje : String;
begin
     inherited;
     with dmNomina do
     begin
          if ValidaAfectada( sMensaje, TRUE, 'Agregar', True ) then
             EditarGridMontos( True )
          else
             ZetaDialogo.zInformation( 'Excepciones de Montos', sMensaje, 0 );
     end;
end;

procedure TTressShell._Per_ExcepcionesGlobalesExecute(Sender: TObject);
var
   sMensaje : String;
begin
     inherited;
     with dmNomina do
     begin
          if ValidaAfectada( sMensaje, TRUE, 'Agregar' ) then
             AgregaExcepGlobales
          else
              ZetaDialogo.zInformation( 'Excepciones Globales', sMensaje, 0 );
     end;
end;

procedure TTressShell._Per_LiquidacionExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.GetDatosPeriodoActivo.Tipo = K_PERIODO_VACIO then
        zError( 'Liquidaci�n Individual', 'El tipo de n�mina es invalido.', 0 )
     else
         if ( dmCliente.GetDatosPeriodoActivo.Numero = K_PERIODO_INICIAL ) then
                ZetaDialogo.zInformation( 'Aviso', 'No se ha definido ning�n per�odo ' + PeriodoTipoCB_DevEx2.Text, 0 )
         else
             dmNomina.LiquidaEmpleadoActivo;
end;

procedure TTressShell._Per_PagoRecibosExecute(Sender: TObject);
begin
     inherited;
     if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
        ZInformation( 'Pago de Recibos', 'La N�mina No Ha Sido Calculada Completamente', 0 )
     else
         dmNomina.EditarGridPagoRecibos;
end;

procedure TTressShell._Per_CalcularNominaExecute(Sender: TObject);
begin
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin          
          if ( Status >= spAfectadaTotal ) then
             ZetaDialogo.zInformation( 'Calcular N�mina',
                                       'La N�mina No Puede Ser Calculada' +
                                       CR_LF +
                                       'El Status De La N�mina Es ' + ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado ), 0 )
          else
              DProcesos.ShowWizard( prNOCalcular );
     end;
end;

procedure TTressShell._Per_AfectarNominaExecute(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          if not EsModoDemo then
          begin
               if ModuloAutorizado( okNomina ) then
               begin
                    with GetDatosPeriodoActivo do
                    begin
                         if Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) = Numero then
                            ZetaDialogo.zInformation('Afectar N�mina ','No se puede afectar este periodo especial de simulaci�n de finiquitos',0)
                         else
                             if ( Status < spCalculadaTotal ) then
                                ZetaDialogo.ZInformation( 'Afectar N�mina ', 'La N�mina No Ha Sido Calculada Completamente', 0 )
                             else
                                 if ( Status = spAfectadaTotal ) then
                                    ZetaDialogo.zInformation( 'Afectar N�mina ', 'La N�mina Ya Fu� Afectada', 0 )
                                 else
                                     DProcesos.ShowWizard( prNOAfectar );
                    end;
               end;
          end;
     end;
end;

procedure TTressShell._Per_FoliarRecibosExecute(Sender: TObject);
begin
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          if Status < spCalculadaTotal then
             ZInformation( 'Foliar Recibos de N�mina', 'La N�mina No Ha Sido Calculada Completamente', 0 )
          else
              DProcesos.ShowWizard( prNOFoliarRecibos );
     end;
end;
procedure TTressShell.EscogeUnReporteSwitch( const sCaption: String; const Entity: TipoEntidad; const ReportType: eTipoReporte; const iDefault: Integer );
begin
     FEscogeReporte_DevEx.EscogeUnReporte(  sCaption ,  Entity,  ReportType,  iDefault );
end ;

procedure TTressShell._Per_ImprimirListadoExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir Listado', enMovimienBalanza, trListado, K_GLOBAL_DEF_LISTADO_NOMINA );
end;

procedure TTressShell._Per_ImprimirRecibosExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir Recibos', enMovimienBalanza, trImpresionRapida, K_GLOBAL_DEF_RECIBOS_NOMINA );
end;

procedure TTressShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
        ZInformation( 'P�liza Contable', 'La N�mina No Ha Sido Calculada Completamente', 0 )
     else
     begin
         DProcesos.ShowWizard( prNOPolizaContable );
         //Se hace aqui, porque los Wizards,no hacen el dataChange si hubo errores.
         //En este caso, se quiere que se actualice la forma de POLHEAD en cualquier caso.
         SetDataChange( [ enPolHead ] )
     end;
end;

procedure TTressShell._Per_CreditoAplicadoMensualExecute(Sender: TObject);
begin
     inherited;
     if not dmCliente.EsModoDemo then
     begin
          DProcesos.ShowWizard( prNOCreditoAplicado );
     end;
end;

procedure TTressShell._Per_ImprimirPolizaExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir P�liza Contable', enPoliza, TRListado, K_GLOBAL_DEF_POLIZA );
end;

procedure TTressShell._Per_ExportarPolizaExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Exportar P�liza Contable', enPoliza, TRListado, K_GLOBAL_DEF_ASCII_POLIZA );
end;

procedure TTressShell._Per_DesafectarExecute(Sender: TObject);
begin
     inherited;
     if Global.GetGlobalInteger( K_GLOBAL_SIMULACION_FINIQUITOS ) = dmCliente.GetDatosPeriodoActivo.Numero then
             ZetaDialogo.zInformation('Desafectar N�mina ','No se puede desafectar este periodo especial de simulaci�n de finiquitos',0)
     else
     begin
          if dmCliente.NominaYaAfectada then
          begin
               DProcesos.ShowWizard( prNODesafectar )
          end
          else
              ZetaDialogo.zInformation( 'Desafectar N�mina ', 'La N�mina No Ha Sido Afectada', 0 );
     end;
end;

procedure TTressShell._Per_LimpiarAcumuladosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOLimpiarAcum );
end;

procedure TTressShell._Per_RecalcularAcumuladosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNORecalcAcum );
end;

{*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
procedure TTressShell._Per_RecalcularAhorrosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNORecalcAhorros );
end;
{*** FIN ***}

{*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
procedure TTressShell._Per_RecalcularPrestamosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNORecalcPrestamos );
end;

{*** FIN ***}

{*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
procedure TTressShell._Per_ImportarAcumuladosExecute(Sender: TObject);
begin
     inherited;
     //if ValidaStatusNomina( 'Importaci�n de Movimientos' ) then
     DProcesos.ShowWizard( prNOImportarAcumuladosRS );
end;

procedure TTressShell._Per_ImportarCedulasFonacotExecute(Sender: TObject);
begin
     inherited;
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
          begin
               DProcesos.ShowWizard (prNOFonacotImportarCedulas);
          end
          else
          begin
               ZInformation( 'Importar c�dula Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_ACTIVO, 0 )
          end;
     end
     else
     begin
          ZInformation( 'Importar c�dula Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )
     end;
end;

procedure TTressShell._Per_GenerarCedulaFonacotExecute(Sender: TObject);
begin
     inherited;
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
          begin
               DProcesos.ShowWizard (prNOFonacotGenerarCedula);
          end
          else
          begin
               ZInformation( 'Generar c�dula de pago Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_ACTIVO, 0 )
          end;
     end
     else
     begin
          ZInformation( 'Generar c�dula de pago Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )
     end;
end;

procedure TTressShell._Per_EliminarCedulaFonacotExecute(Sender: TObject);
begin
     inherited;
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          if Global.GetGlobalBooleano (K_GLOBAL_FONACOT_PRESTAMO_CONCILIA) then
          begin
               DProcesos.ShowWizard (prNOFonacotEliminarCedula);
          end
          else
          begin
               ZInformation( 'Eliminar registros de c�dula Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_ACTIVO, 0 )
          end;
     end
     else
     begin
          ZInformation( 'Eliminar registros de c�dula Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )
     end;
end;

procedure TTressShell._Per_ImportarMovimientosExecute(Sender: TObject);
begin
     inherited;
     if ValidaStatusNomina( 'Importaci�n de Movimientos' ) then
        DProcesos.ShowWizard( prNOImportarMov );
end;

procedure TTressShell._Per_ExportarMovimientosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOExportarMov );
end;

procedure TTressShell._Per_ImportarPagoRecibosExecute(Sender: TObject);
begin
     inherited;
     if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
        ZInformation( 'Importar Pago de Recibos', 'La N�mina No Ha Sido Calculada Completamente', 0 )
     else
         DProcesos.ShowWizard( prNOPagoRecibos );
end;

procedure TTressShell._Per_RefoliarRecibosExecute(Sender: TObject);
begin
     inherited;
     if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
        ZInformation( 'Re-Foliar Recibos de N�mina', 'La N�mina No Ha Sido Calculada Completamente', 0 )
     else
         DProcesos.ShowWizard( prNOReFoliarRecibos );
end;

procedure TTressShell._Per_CopiarNominaExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOCopiarNominas );
end;

procedure TTressShell._Per_CalcularDiferenciasExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.GetDatosPeriodoActivo.Tipo = K_PERIODO_VACIO then
        zError( 'Calcular Diferencias', 'El tipo de n�mina es invalido.', 0 )
     else
         if ( dmCliente.GetDatosPeriodoActivo.Numero = K_PERIODO_INICIAL ) then
                ZetaDialogo.zInformation( 'Aviso', 'No Se Ha Definido Ning�n Per�odo ' + PeriodoTipoCB_DevEx2.Text, 0 )
         else
             if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
                ZInformation( 'Calcular Diferencias', 'La N�mina No Ha Sido Calculada Completamente', 0 )
             else
                 DProcesos.ShowWizard( prNOCalcularDiferencias );
end;

procedure TTressShell._Per_PagarFueraExecute(Sender: TObject);
begin
     inherited;
     with dmCliente.GetDatosPeriodoActivo do
     begin
          if dmCliente.GetDatosPeriodoActivo.Tipo = K_PERIODO_VACIO then
            zError( 'Pagos Por Fuera', 'El tipo de n�mina es invalido.', 0 )
          else
              if ( dmCliente.GetDatosPeriodoActivo.Numero = K_PERIODO_INICIAL ) then
                    ZetaDialogo.zInformation( 'Aviso', 'No Se Ha Definido Ning�n Per�odo ' + PeriodoTipoCB_DevEx2.Text, 0 )
              else
                  if ( Status < spCalculadaParcial ) then
                    ZetaDialogo.zInformation( 'Pagos Por Fuera', 'La N�mina No Ha Sido Calculada', 0 )
                  else
                      if ( Status >= spAfectadaParcial ) then
                        ZetaDialogo.zInformation( 'Pagos Por Fuera',
                                                  'No Se Pueden Realizar Pagos X Fuera' +
                                                  CR_LF +
                                                  'El Status De La N�mina Es ' + ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado ) , 0 )
                      else
                         DProcesos.ShowWizard( prNOPagosPorFuera );
     end;
end;

procedure TTressShell._Per_CancelarPasadasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOCancelarPasadas );
end;

procedure TTressShell._Per_LiquidacionGlobalExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.GetDatosPeriodoActivo.Tipo = K_PERIODO_VACIO then
        zError( 'Liquidaci�n global', 'El tipo de n�mina es invalido.', 0 )
     else
         if ( dmCliente.GetDatosPeriodoActivo.Numero = K_PERIODO_INICIAL ) then
                ZetaDialogo.zInformation( 'Aviso', 'No se ha definido ning�n per�odo ' + PeriodoTipoCB_DevEx2.Text, 0 )
         else
             if ValidaStatusNomina( 'Liquidaci�n Global' ) then
                DProcesos.ShowWizard( prNOLiquidacion );
end;

procedure TTressShell._Per_CalcularNetoBrutoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOCalculoNetoBruto );
end;

procedure TTressShell._Per_RastrearCalculoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNORastrearCalculo );
end;

procedure TTressShell._Per_DefinirPeriodosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNODefinirPeriodos );
end;

procedure TTressShell._Per_CalcularAguinaldosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOCalculoAguinaldo );
end;

procedure TTressShell._Per_ImprimirAguinaldosExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir Aguinaldos', enAguinaldo, TRListado, K_GLOBAL_DEF_AGUINALDO_REPORTE );
end;

procedure TTressShell._Per_PagarAguinaldosExecute(Sender: TObject);
begin
     inherited;
     if ValidaStatusNomina( 'Pago de Aguinaldos' ) then
        DProcesos.ShowWizard( prNOPagoAguinaldo );
end;

procedure TTressShell._Per_CalcularRepartoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNORepartoAhorro );
end;

procedure TTressShell._Per_ImprimirRepartoExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir Reparto de Ahorros', enRepAhorro, TRListado, K_GLOBAL_DEF_REPARTIR_AHORROS_REPORTE );
end;

procedure TTressShell._Per_PagarRepartoExecute(Sender: TObject);
begin
     inherited;
     if ValidaStatusNomina( 'Reparto de Ahorros ' ) then
        DProcesos.ShowWizard( prNOPagoRepartoAhorro );
end;

procedure TTressShell._Per_CierreAhorrosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTCierreAhorros );
end;

procedure TTressShell._Per_CierrePrestamosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTCierrePrestamos );
end;

procedure TTressShell._Per_CalcularPTUExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOCalculoPTU );
end;

procedure TTressShell._Per_ImprimirPTUExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir PTU', enRepartoPTU, TRListado, K_GLOBAL_DEF_PTU_REPORTE);
end;

procedure TTressShell._Per_PagarPTUExecute(Sender: TObject);
begin
     inherited;
     if ValidaStatusNomina( 'Pago de PTU' ) then
        DProcesos.ShowWizard( prNOPagoPTU );
end;

procedure TTressShell._Per_CalcularDeclaracionExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOCreditoSalario );
end;

procedure TTressShell._Per_ImprimirDeclaracionExecute(Sender: TObject);
begin
     inherited;
     if ( dmCliente.YearDefault >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
        EscogeUnReporteSwitch( 'Imprimir Declaraci�n de Subsidio al Empleo', enDeclara, TRListado, K_GLOBAL_DEF_CRED_SAL_REPORTE)
     else
         EscogeUnReporteSwitch( 'Imprimir Declaraci�n de Cr�dito al Salario', enDeclara, TRListado, K_GLOBAL_DEF_CRED_SAL_REPORTE);
end;

procedure TTressShell._Per_CalcularComparativoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOISPTAnual );
end;

procedure TTressShell._Per_ImprimirComparativoExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir Comparativo Anual', enCompara, TRListado, K_GLOBAL_DEF_COMP_ANUAL_REPORTE );
end;

procedure TTressShell._Per_PagarComparativoExecute(Sender: TObject);
begin
     inherited;
     if ValidaStatusNomina( 'Pago de Comparativo Anual' ) then
        DProcesos.ShowWizard( prNOISPTAnualPago );
end;

procedure TTressShell._IMSS_PagoNuevoExecute(Sender: TObject);
begin
     inherited;
     with dmIMSS.cdsIMSSDatosTot do
     begin
          Refrescar;
          Agregar;
     end;
     //dmIMSS.AgregaPagoIMSS;
end;

procedure TTressShell._IMSS_BorrarPagoExecute(Sender: TObject);
begin
     inherited;
     with dmIMSS.cdsIMSSDatosTot do
     begin
          Refrescar;
          Borrar;
     end;
end;

procedure TTressShell._IMSS_CalcularPagoExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okIMSS ) then
        DProcesos.ShowWizard( prIMSSCalculoPagos );
end;

procedure TTressShell._IMSS_CalcularRecargosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prIMSSCalculoRecargos );
end;

procedure TTressShell._IMSS_CalcularTasaExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prIMSSTasaINFONAVIT );
end;

procedure TTressShell._IMSS_RevisarSUAExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prIMSSRevisarSUA );
end;

procedure TTressShell._IMSS_ExportarSUAExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okIMSS ) then
        DProcesos.ShowWizard( prIMSSExportarSUA );
end;

procedure TTressShell._IMSS_CalcularPrimaExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okIMSS ) then
        DProcesos.ShowWizard( prIMSSCalculoPrima );
end;

procedure TTressShell._Sist_EmpresaNuevaExecute(Sender: TObject);
begin
     inherited;
     dmSistema.AgregaEmpresa;
end;

procedure TTressShell._Sist_BorrarBajasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTBorrarBajas );
end;

procedure TTressShell._Sist_BorrarNominasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTBorrarNominas );
end;

procedure TTressShell._Sist_Borrar_TarjetasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTBorrarTarjetas );
end;

procedure TTressShell._Sist_Borrar_POLLExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTBorrarPOLL ); // � Falta Agregar a ZetaCommonLists ! //
end;

procedure TTressShell._Sist_Borrar_BitacoraExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTDepurar );
end;

procedure TTressShell._Sist_Borrar_HerramientasExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTBorrarHerramienta );
end;

procedure TTressShell._Per_CalculaRetroactivosExecute(Sender: TObject);
begin
     inherited;
     if ValidaStatusNomina( 'Calcular Retroactivos' ) then
         DProcesos.ShowWizard( prNOCalculaRetroactivos );
end;

{ ******* Mensajes de Wizards ejecutados con Threads ****** }
procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessEnd( WParam, LParam );
     end;
end;
procedure TTressShell.WMWizardSilent(var Message: TMessage);
begin
     with Message do
     begin
          dmProcesos.HandleProcessSilent( WParam, LParam );
     end;
end;

{* Cambio por reforma fiscal del 2008. Si es igual o mayor a la constante
 K_REFORMA_FISCAL_2008 se hacen cambio de texto en ciertos menus *}
procedure TTressShell.ValidaFechaReformaFiscal;
begin
     if ( dmCliente.YearDefault >= ZetaCommonClasses.K_REFORMA_FISCAL_2008 ) then
     begin
        _Per_CreditoAplicadoMensual.Caption := 'SUBE Aplicado Mensual';
        _Per_CalcularDeclaracion.Caption := 'Declaraci�n de Subsidio al Empleo';
        _Per_ImprimirDeclaracion.Caption := 'Imprimir Declaraci�n de Subsidio al Empleo';
     end
     else
     begin
        _Per_CreditoAplicadoMensual.Caption := 'Cr�dito Aplicado Mensual';
        _Per_CalcularDeclaracion.Caption := 'Declaraci�n de Cr�dito al Salario';
        _Per_ImprimirDeclaracion.Caption := 'Imprimir Declaraci�n de Cr�dito al Salario';
     end;
end;

procedure TTressShell._Per_DeclaracionAnualExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNoDeclaracionAnual );
end;

procedure TTressShell._Per_CierreDeclaracionAnualExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNoDeclaracionAnualCierre );
end;

procedure TTressShell._Per_ImprimirDeclaracionAnualExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir Declaraci�n Anual', enDeclaraAnual, TRListado, K_GLOBAL_DEF_DECL_ANUAL_REPORTE );
end;


procedure TTressShell._Emp_Registro_Comidas_GrupalesExecute(Sender: TObject);
begin
     inherited;
     if not dmCliente.EsModoDemo then
     begin
          if dmCliente.ModuloAutorizado( okCafeteria ) then
               DProcesos.ShowWizard( prCAFEComidasGrupales );
     end
end;

procedure TTressShell._Emp_Corregir_Fechas_CafExecute(Sender: TObject);
begin
     inherited;
     if not dmCliente.EsModoDemo then
     begin
          if dmCliente.ModuloAutorizado( okCafeteria ) then
             DProcesos.ShowWizard( prCAFECorregirFechas );
     end
end;

procedure TTressShell._Asi_AutorizacionesXAprobarExecute(Sender: TObject);
begin
     inherited;
     dmAsistencia.EditarGridAprobarAutorizaciones;
end;

procedure TTressShell._Sist_Actualizar_NumeroTarjetaExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTNumeroTarjeta );
end;

procedure TTressShell._Sist_Administrador_Envios_ProgramadosExecute(Sender: TObject);
begin
     inherited;
     with dmSistema.cdsSistTareaCalendario do
     begin
          dmSistema.TituloProceso := False;
          Conectar;
          Agregar;
     end;
end;

procedure TTressShell.EmpleadoActivoPanelDblClick(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcHisKardex );
end;

procedure TTressShell._Emp_Cancelar_Cierre_VacacionesExecute(
  Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHCancelarCierreVacaciones );
end;

procedure TTressShell._Emp_CertificacionesExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaCertificacion;
end;

procedure TTressShell._Emp_Importar_Aho_PreExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHImportarAhorrosPrestamos );
end;

procedure TTressShell._Emp_Recalcula_Saldos_VacaExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHRecalculaSaldosVacaciones );
end;

procedure TTressShell._Emp_Cambio_plazaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_PLAZA );
end;

procedure TTressShell._Emp_Cambio_Tipo_NominaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.AgregaKardex( K_T_TIPNOM );
end;

procedure TTressShell._Emp_Certificaciones_EmpleadoExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridEmpCertific;
end;

procedure TTressShell._Emp_Empleados_CertificadosExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridGlobalCertific;
end;

procedure TTressShell._Per_AjusteRetFonacotCancelacionExecute(Sender: TObject);
begin
  inherited;
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          DProcesos.ShowWizard( prNOAjusteRetFonacotCancelacion );
     end
     else
     begin
          ZInformation( 'Cancelar Ajuste de Retenci�n Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )
     end;
end;

procedure TTressShell._Per_AjusteRetFonacotExecute(Sender: TObject);
begin
     inherited;
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          DProcesos.ShowWizard( prNOAjusteRetFonacot );
     end
     else
     begin
          ZInformation( 'Ajuste de Retenci�n', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )
     end;
end;

procedure TTressShell._Per_CalcularPagoFonacotExecute(Sender: TObject);
begin
     inherited;
     if Global.GetGlobalString (K_GLOBAL_FONACOT_PRESTAMO) <> VACIO then
     begin
          DProcesos.ShowWizard( prNOCalcularPagoFonacot );
     end
     else
     begin
          ZInformation( 'Pago Fonacot', K_MENSAJE_GLOBAL_FONACOT_NO_TIPO_PRESTAMO, 0 )
     end;
end;

procedure TTressShell._Sist_Enrolamiento_MasivoExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSistEnrolarUsuario );
end;

procedure TTressShell._Sist_Extraer_Checadas_Bit_TermGTIExecute(
  Sender: TObject);
begin
  inherited;
  if dmCliente.ModuloAutorizado( okTressEnLinea ) then
    DProcesos.ShowWizard( prSISTExtraerChecadas );
end;

procedure TTressShell._Sist_ImportarCafeteriaExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard(prSISTImportarCafeteria);
end;

procedure TTressShell._Sist_Importar_Checadas_OffLine_TermGTIExecute(
  Sender: TObject);
begin
    inherited;
    if dmCliente.ModuloAutorizado( okTressEnLinea ) then
      DProcesos.ShowWizard( prSISTRecuperarChecadas );
end;

procedure TTressShell._Sist_Importar_Enrolamiento_MasivoExecute(
  Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSistImportarEnrolamientoMasivo );
end;

procedure TTressShell._IMSS_AjusteRetInfExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prIMSSAjusteRetInfonavit );
end;

procedure TTressShell._Emp_CasetaExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okAccesos ) then
        dmCafeteria.EditarGridCaseta( True );
end;

procedure TTressShell.ArbolitoDblClick(Sender: TObject);
begin
     inherited;
        NavegacionCB_DevEx.Enabled:= NOT FormaActivaNomina;
end;

procedure TTressShell.AbreFormaSimulaciones(const TipoForma: Integer);
begin
     if TipoForma = 1 then
        AbreFormaConsulta(efcSimulaMontos )
     else
        AbreFormaConsulta(efcSimulaMontoGlobal);
end;

procedure TTressShell._Emp_Curso_Prog_GlobalExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard( prRHCursoProgGlobal );
end;

procedure TTressShell._Emp_Cancelar_Curso_Prog_GlobalExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard( prRHCancelarCursoProgGlobal );
end;

procedure TTressShell.ArbolitoKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     NavegacionCB_DevEx.Enabled:= NOT FormaActivaNomina;
end;

procedure TTressShell._V_CerrarExecute(Sender: TObject);
begin
     inherited;
     NavegacionCB_DevEx.Enabled:= NOT FormaActivaNomina;
end;

procedure TTressShell._Asi_Ajust_Incapa_CalExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard(prASIAjustIncapaCal);
end;

procedure TTressShell._Emp_Tarjetas_GasolinaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridTarjetasGasolina;
end;

procedure TTressShell._Emp_Tarjetas_DespensaExecute(Sender: TObject);
begin
     inherited;
     dmRecursos.EditarGridTarjetasDespensa;
end;

procedure TTressShell._Emp_Foliar_capacitacionesExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard(prRHFoliarCapacitaciones);

end;
 
procedure TTressShell._Asi_AutorizacionPreNominaExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okAsistencia ) then
        DProcesos.ShowWizard(prASISAutorizacionPreNomina);
end;


procedure TTressShell._IMSS_ValidaMovIDSEExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prIMSSValidacionMovimientosIDSE );
end;

procedure TTressShell._Per_TransferenciasCosteoExecute(Sender: TObject);
var
   sMensaje : string;
begin
     inherited;
     if (dmTablas.HayDataSetTransferencia( sMensaje )) then
         DProcesos.ShowWizard( prNoTransferenciasCosteo )
     else
         ZInformation( 'Transferencias de Costeo', sMensaje, 0 );
end;

procedure TTressShell._Asi_ImportarClasificacionesTemporalesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prASISImportacionClasificaciones );
end;

procedure TTressShell._Emp_Importar_SGMExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHImportarSGM );
end;

procedure TTressShell._Emp_Renovacion_SGMExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHRenovacionSGM );
end;

procedure TTressShell._Emp_Borrar_SGMExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prRHBorrarSGM );
end;

procedure TTressShell._Per_Calculo_CosteoExecute(Sender: TObject);
begin
    inherited;
    if ( dmCliente.GetDatosPeriodoActivo.Status < spCalculadaTotal ) then
        ZInformation( 'C�lculo de Costeo', 'La N�mina No Ha Sido Calculada Completamente', 0 )
     else
         DProcesos.ShowWizard( prNoCalculaCosteo );
end;

procedure TTressShell._Per_Cancelacion_TransferenciasExecute( Sender: TObject);
var
   sMensaje : string;
begin
     inherited;
    if (dmTablas.HayDataSetTransferencia( sMensaje )) then
         DProcesos.ShowWizard( prNoCancelaTransferencias )
     else
         ZInformation( 'Eliminar Transferencias', sMensaje, 0 );
end;

// SYNERGY
procedure TTressShell._Sist_Sincroniza_IDBiometricosExecute(
  Sender: TObject);
begin
     inherited;
     //DProcesos.ShowWizard( prSISTSincBiometrico );
end;

procedure TTressShell.RegistroClick(Sender: TObject);
begin
  inherited;

end;

// SYNERGY
procedure TTressShell._Sist_Depura_BitacoraBiometricoExecute(
  Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTDepuraBitBiometrico );
end;

// SYNERGY
procedure TTressShell._Sist_Asigna_NumBiometricosExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTAsignaNumBiometricos );
end;

// SYNERGY
procedure TTressShell._Sist_Asigna_GrupoTerminalesExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prSISTAsignaGrupoTerminales );
end;

procedure TTressShell._Per_Previo_ISRExecute(Sender: TObject);
begin
     inherited;
     DProcesos.ShowWizard( prNOPrevioISR );
end;

procedure TTressShell._Per_Imprimir_PrevioISRExecute(Sender: TObject);
begin
     inherited;
     EscogeUnReporteSwitch( 'Imprimir C�lculo Previo de ISR', enPrevioISR, TRListado, K_GLOBAL_DEF_PREVIO_ISR_REPORTE );
end;

procedure TTressShell._Emp_ImportarOrganigramaExecute(Sender: TObject);
begin
     inherited;
      DProcesos.ShowWizard( prRHImportarOrganigrama );
end;

procedure TTressShell._Emp_Revisar_Datos_STPSExecute(Sender: TObject);
begin
     inherited;
     if dmCliente.ModuloAutorizado( okCursos ) then
        DProcesos.ShowWizard(prRHRevisarDatosSTPS);
end;

procedure TTressShell._Emp_Reiniciar_Capacitaciones_STPSExecute(
  Sender: TObject);
begin
  inherited;
  if dmCliente.ModuloAutorizado( okCursos ) then
     DProcesos.ShowWizard(prRHReiniciarCapacitacionesSTPS);
end;
procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
     DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=4;
end;

//**Carga vista actual
procedure TTressShell.CargaVista;
begin
     //Action Lists Locales
     _E_BuscarEmpleado.Caption := '';
     inherited;

     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Color := TColor($F9E6ED);
     DevEx_BandaValActivos.Visible := True;

     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;

     Application.ProcessMessages;
end;

//DevEx(by am): Metodo agregado para mostrar la informacion del usuario en el Panel de valores activos aun cuando no se elija una empresa.
procedure TTressShell.MuestraDatosUsuarioActivo;
begin
     //lblDatosUsuario.Caption := dmCliente.GetDatosUsuarioActivo.NombreCorto +'|'+ dmCliente.GetDatosUsuarioActivo.GrupoDescrip;
end;

//DevEx (by am): Evento agregadon  para activar y desactivar el combo tal y como sehace en la vista clasica.
procedure TTressShell.ClientAreaPG_DevExChange(Sender: TObject);
begin
  inherited;
  NavegacionCB_DevEx.Enabled:= NOT FormaActivaNomina
end;

//DevEx (by am): Metodo agregado para visualizar al KARDEX al dar doble clic sobre la imagen del status del empleado
procedure TTressShell.ImagenStatus_DevExDblClick(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcHisKardex );
end;

//DevEx (by am): Botones para Habilitar los botones de los componentes de mes y a�o
procedure TTressShell.HabilitaBotonesAnio;
begin
     btnYearNext_DevEx.Enabled := TRUE;
     btnYearBefore_DevEx.Enabled := TRUE;
end;


{
oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             YearDefault := SistemaYearCB.ValorEntero;
        end;
        CambioPeriodoActivo;
        RefrescaSistemaActivos;
        CambioValoresActivos( stSistema );
        ValidaFechaReformaFiscal;  // Validacion de Reforma Fiscal 2008

     finally
            Screen.Cursor := oCursor;
     end;
}

//DevEx (by am): Calcula el valor del a�o del Sistema con los nuevos componentes.
procedure TTressShell.CalculaAnio ( Operacion:Integer );
var
   oCursor: TCursor;
   NewYearValue:Integer;
function ValidaNewYear ( NewYearValue:Integer ):Integer;
 begin
     if NewYearValue > K_MAX_YEAR then
     begin
        NewYearValue := K_MAX_YEAR;
        btnYearNext_DevEx.Enabled := FALSE;  //Deshabilita btnYearSiguiente
     end
     else if NewYearValue < K_MIN_YEAR then
     begin
        NewYearValue := K_MIN_YEAR;
        btnYearBefore_DevEx.Enabled := FALSE;//Deshabilita btnYearAnterior
     end
     else
     begin
        HabilitaBotonesAnio; //Habilita ambos botones
     end;
     Result := NewYearValue;
 end;
begin
     NewYearValue := dmCliente.YearDefault;
     case Operacion of
          K_ANTERIOR:
               NewYearValue := dmCliente.YearDefault -1;
          K_SIGUIENTE:
               NewYearValue := dmCliente.YearDefault +1;
          K_MANUAL:
               NewYearValue := StrToInt ( SistemaYear_DevEx.Text );
     end;

     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
              YearDefault := ValidaNewYear ( NewYearValue );
              SistemaYear_DevEx.Text := IntToStr ( YearDefault );
        end;

        CambioPeriodoActivo;
        RefrescaSistemaActivos;
        CambioValoresActivos( stSistema );
        ValidaFechaReformaFiscal;  // Validacion de Reforma Fiscal 2008

     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.btnYearBefore_DevExClick(Sender: TObject);
begin
  inherited;
  FCambiandoAnioActivo := True;
  CalculaAnio ( K_ANTERIOR );
  FCambiandoAnioActivo := False;
end;

procedure TTressShell.btnYearNext_DevExClick(Sender: TObject);
begin
  inherited;
  FCambiandoAnioActivo := True;
  CalculaAnio ( K_SIGUIENTE );
  FCambiandoAnioActivo := False;
end;

procedure TTressShell.SistemaYear_DevExPropertiesChange(Sender: TObject);
begin
  inherited;
  {***(@am): Se agrega bandera FCambioAnioActivo para que el cambio de a�o activo no se haga dos veces***}
  if ( Length( Trim(SistemaYear_DevEx.Text) ) = 4 ) and (dmCliente.EmpresaAbierta) and (not FCambiandoAnioActivo) then
  begin
        CalculaAnio ( K_MANUAL);
  end;
end;

//DevEx (by mp): Evento para cerrar los tabs.
procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
  _V_Cerrar.Execute;
end;

procedure TTressShell.ProcesosClick(Sender: TObject);
begin
  inherited;

end;

//Edit by mp: Proceso para llamar publicamente al metodo privado de RefrescaIMMSActivo
procedure TTressShell.RefrescaIMSSActivoPublico;
begin
     RefrescaIMSSActivo;
end;

procedure TTressShell.PrendeApagaSubitem(SubMenu:tdxBarSubItem);
var
Prender : Boolean;
links : integer;
begin
prender := false;
for links:=0 to SubMenu.Itemlinks.Count -1 do
begin
  if(submenu.itemlinks.items[links].item is tdxbarbutton) then
  if((submenu.itemlinks.items[links].item.action as Taction).visible = true and (submenu.itemlinks.items[links].item.action as Taction).enabled )then prender := true;
end;
 SubMenu.Enabled := prender;
end;
//DevEx(by am): Se define este metodo para esconder y aparecer el panel del rango de fechas dependiendo del width de la forma.
procedure TTressShell.FormResize(Sender: TObject);
const
     K_VACIO ='';
begin
  inherited;
  if (Self.Width <= K_MIN_WIDTH) then
  begin
       if not ZetaCommonTools.StrLleno( TempPanelRangoFechas ) then  //Por si reduce mas de una vez
       begin
          TempPanelRangoFechas:= PanelRangoFechas.Caption;
          PanelPeriodoStatus.Hint := TempPanelRangoFechas;
       end;
       PanelRangoFechas.Caption:='';
       PanelRangoFechas.Width := 1;
  end
  else
  begin
      if ZetaCommonTools.StrLleno( TempPanelRangoFechas ) then  //Por si aumenta mas de una vez
      begin
           PanelRangoFechas.Caption:= TempPanelRangoFechas;
           TempPanelRangoFechas := '';

           {***Si el tama�o del texto es menor al minimo establecido para el texto mas largo, se asgina el width del minimo.
            En caso contrario, se asigna el width del texto.***}
           if (Self.Canvas.TextWidth(PanelRangoFechas.Caption)+K_EXTRA_WIDTH) < K_MIN_PANEL_FECHAS_PERIODO then
              PanelRangoFechas.Width := K_MIN_PANEL_FECHAS_PERIODO
           else
               PanelRangoFechas.Width := Self.Canvas.TextWidth(PanelRangoFechas.Caption)+K_EXTRA_WIDTH;

           PanelPeriodoStatus.Hint := K_VACIO;
      end;
  end;
end;

procedure TTressShell._Emp_Importar_ImagenesExecute(Sender: TObject);
begin
     inherited;
      DProcesos.ShowWizard( prRHImportarImagenes );
end;

procedure TTressShell._Sist_Actualizar_BDsExecute(Sender: TObject);
begin
     inherited;
     SistActualizarDBs_DevEx := TSistActualizarDBs_DevEx.Create(Self);
     SistActualizarDBs_DevEx.ShowModal;
     SistActualizarDBs_DevEx.Free;
end;
procedure TTressShell._Sist_Agrega_Base_Datos_SeleccionExecute(Sender: TObject);
begin
  inherited;
  DProcesos.ShowWizard(prSISTAgregarBaseDatosSeleccion);
end;

procedure TTressShell._Sist_Agrega_Base_Datos_VisitantesExecute(Sender: TObject);
begin
  inherited;
  DProcesos.ShowWizard(prSISTAgregarBaseDatosVisitantes);
end;

procedure TTressShell._Sist_Agrega_Base_Datos_PruebasExecute(Sender: TObject);
begin
  inherited;
  DProcesos.ShowWizard(prSISTAgregarBaseDatosPruebas);
end;

procedure TTressShell._Sist_Agrega_Base_Datos_EmpleadosExecute(Sender: TObject);
begin
  inherited;
  DProcesos.ShowWizard(prSISTAgregarBaseDatosEmpleados);
end;

procedure TTressShell._Sist_Agrega_Base_Datos_PresupuestosExecute(Sender: TObject);
begin
  inherited;
  DProcesos.ShowWizard(prSISTPrepararPresupuestos);
end;

procedure TTressShell._Sist_BorrarTimbradoNominasExecute(Sender: TObject);
begin
   DProcesos.ShowWizard( prSISTBorrarTimbradoNominas );
end;

function TTressShell.CheckVersionDatos:Boolean;
var
   ocDecision:eOperacionConflicto;
begin
     with dmCliente do
     begin
          Result := CheckVersion;
          //DevEx (@am): Se guarda el resultado de CheckVersion para utlizarlo despues en la validacion para mostrar la presentacion de cambios en version.
          EsVersionActualBD := Result;  
          if not Result then
          begin
          	 //**Validar que no se muestre forma de actualizacion a utilizar
             if (  dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION  ) then
             begin
                  ocDecision := FActualizaBDConfirma_DevEx.ShowDlgActualizaBDConfirma( cdsCompany.FieldByName('CM_NOMBRE').AsString,VersionDatos,UltimaVersion );
             end
             else
                   ocDecision := ocSumar;  //Por default continua sin actulizar empresa

               case  ocDecision of
                ocSustituir :  //Actualizar Empresa
                            begin
                                 SistActualizarDBs_DevEx := TSistActualizarDBs_DevEx.Create(Self);
                                 try
                                    SistActualizarDBs_DevEx.Empresa := cdsCompany.FieldByName('DB_CODIGO').AsString;
                                    SistActualizarDBs_DevEx.ShowModal;
                                    // Continuar s�lo si se actualiz�
                                    Result := SistActualizarDBs_DevEx.EmpresaActualizada(SistActualizarDBs_DevEx.Empresa);
                                 finally
                                        FreeAndNil(SistActualizarDBs_DevEx);
                                 end;
                            end;
                ocReportar  : Result := EscogeEmpresa(True); //Seleccionar otra empresa
                ocSumar     : Result := True;                //Continuar Sin Actualizar Empresa
                ocIgnorar   : Result := False;               //Configurar Sistema
                else
                    Result := False;                         //Continuar sin actualizar
               end;
          end;
     end;
end;

procedure TTressShell.DoOpenSistema;
begin
     inherited DoOpenSistema;
     if dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION then
     begin
         try
           if ( not CheckDerechosTress ) then
               db.DataBaseError( 'No tiene derechos para utilizar este ejecutable en esta Empresa' );

           SetArbolitosLength(1);
                 CreaNavBarSistema;

     		dmCliente.InitActivosSistema;
            CargaSistemaActivos;

            RevisaDerechos;
            //** Ya no se valida, todas tienen vencimiento
            {if Autorizacion.TieneVencimiento then
               SistemaFechaZF_DevEx.Enabled := FALSE;
            }
            SetBloqueo;

            //Verificar constante de AVENT
            if Global.GetGlobalBooleano( K_GLOBAL_AVENT ) then
            begin
                 //Hacer Tab Invisible
                 TabIMSS.Visible := False; //**Deshabilitar nuevo tab
            end;

         except
               on Error : Exception do
               begin
                    ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                    DoCloseAll;
               end;
         end;

         LLenaOpcionesGrupos;

         ElementosEmpresaAbierta;
     end
end;


procedure TTressShell.ElementosEmpresaAbierta;
begin 
     _Sist_BorrarBajas.Enabled := Revisa( D_SIST_PROC_BORRAR_BAJAS ) and EmpresaAbierta;
     _Sist_BorrarNominas.Enabled := Revisa( D_SIST_PROC_BORRA_NOMINAS ) and EmpresaAbierta ;
     _Sist_BorrarTimbradoNominas.Enabled := Revisa( D_SIST_PROC_BORRA_TIMBRADO_NOMINAS ) and dmCliente.EsTRESSPruebas and dmcliente.EmpresaAbierta;
     _Sist_Borrar_Tarjetas.Enabled := Revisa( D_SIST_PROC_BORRAR_TARJETAS  ) and EmpresaAbierta;
     _Sist_Borrar_POLL.Enabled := Revisa( D_SIST_BORRAR_POLL )and EmpresaAbierta;
     _Sist_Borrar_Bitacora.Enabled := Revisa( D_SIST_BORRAR_BITACORA )and EmpresaAbierta;
     _Sist_Borrar_Herramientas.Enabled := Revisa( D_SIST_BORRAR_HERRAMIENTA )and EmpresaAbierta;
     _Sist_Actualizar_NumeroTarjeta.Enabled := Revisa( D_SIST_PROC_NUMERO_TARJETA ) and EmpresaAbierta;

     btnYearBefore_DevEx.Enabled := EmpresaAbierta;
     btnYearNext_DevEx.Enabled := EmpresaAbierta;
     SistemaYear_DevEx.Enabled := EmpresaAbierta;

end;

// P A N E L - I N I C I O
procedure TTressShell.AbrirAccesos;
begin
    if EmpresaAbierta then
    begin
        if ClientRegistry.HomeSistema then
        begin
             if (CheckDerecho(GetPropConsulta(efcInicio).IndexDerechos,K_DERECHO_CONSULTA )) then
             begin
                  AbreFormaConsulta( efcInicio );
             end;
        end;
    end;
end;

procedure TTressShell.ObtenerAccesos;
const
     K_NO_EXISTE = -1;
var
  I: integer;
begin
    //@(am): Si la lista es mayor a cero, significa que el usuario cambio de empresa. Por lo tanto se deben guardar los accesos utilizados en esa empresa.
    if lstAccesosDirectos.Count>0 then
       RegistrarAccesos;
    //(@am): Es necesario limpiar la lista porque al cambiar de empresa se validan los derechos sobre las opciones amostrar de nuevo.
    lstAccesosDirectos.Clear;
    for I := 0 to DevEx_BarManager.ItemCount - 1 do
    begin
         if ClientRegistry.GetAccesoDirecto(I)> K_NO_EXISTE then
              if not( not (DevEx_BarManager.Items[I].Enabled) or not (DevEx_BarManager.Items[I].Visible = ivAlways)) then
                   lstAccesosDirectos.Add(IntToStr(I)+':'+IntToStr(ClientRegistry.GetAccesoDirecto(I))) ;
    end;
end;

procedure TTressShell.CopiarAccesos;
const
     K_MAX_ACCESOS = 12;
var
  I ,iAccesos: integer;
   procedure OrdenaLista;
   var
      strTemp: String;
      tmpA, tmpB: TStringList;
      i,j : Integer;
   begin
        try
           tmpA := TStringList.Create;
           tmpB := TStringList.Create;
           if (lstAccesosDirectos.Count > 0) then
           begin
                    for i:= lstAccesosDirectos.count-1 downto 1 do
                    begin
                         for j:= 0 to i-1 do
                         begin
                              tmpA.Clear;
                              tmpB.Clear;
                              SplitList(':', lstAccesosDirectos[j], tmpA);
                              SplitList(':', lstAccesosDirectos[j+1], tmpB);
                              if (StrToInt(tmpA[1]) < StrToInt(tmpB[1])) then
                              begin
                                   strTemp := lstAccesosDirectos[j+1];
                                   lstAccesosDirectos[j+1] := lstAccesosDirectos[j];
                                   lstAccesosDirectos[j] := strTemp;
                              end;

                         end;
                    end;
           end;
        finally
               FreeAndNil(tmpA);
               FreeAndNil(tmpB);
        end;
   end;

begin
    OrdenaLista;
    //(@am): Es necesario limpiar la lista porque al cambiar de empresa se validan los derechos sobre las opciones a mostrar de nuevo.
    if lstAccesosDirectos.Count > K_MAX_ACCESOS then
       iAccesos := K_MAX_ACCESOS
    else
        iAccesos := lstAccesosDirectos.Count;

    lstTopAccesosDirectos.Clear;
    for I := 0 to iAccesos - 1 do
    begin
         lstTopAccesosDirectos.Add(lstAccesosDirectos[I]);
    end;
end;

procedure TTressShell.RegistrarAccesos;
const
     K_NUM_SECCIONES = 2;
var
      i : Integer;
      AccesoSeccion    : TStringList;
begin
      AccesoSeccion := TStringList.Create;
      try
         for i := 0 to lstAccesosDirectos.Count-1 do
         begin
              AccesoSeccion.Clear;
              if SplitList(':', lstAccesosDirectos[i], AccesoSeccion) then
                 if AccesoSeccion.Count = K_NUM_SECCIONES then
                    ClientRegistry.SetAccesosDirecto(StrToInt(AccesoSeccion[0]), StrToInt( AccesoSeccion[1]));
         end;
      finally
        FreeAndNil(AccesoSeccion);
      end;
end;

procedure TTressShell.SetAccesosRapidos(const index_acceso : integer);

var
  AccesoSeccion           : TStringList;
  i, cont                 : integer;
  lExiste                  : Boolean;
begin
    lExiste := false;
    AccesoSeccion := TStringList.Create;
    try
       for i := 0 to lstAccesosDirectos.Count-1 do
       begin
           AccesoSeccion.Clear;
           if SplitList(':', lstAccesosDirectos[i], AccesoSeccion)then
           begin
                if (StrToInt(AccesoSeccion[0]) = index_acceso) then
                begin
                    lExiste := true;
                    cont := StrToInt(AccesoSeccion[1]) + 1;
                    lstAccesosDirectos[i] := AccesoSeccion[0] + ':' + IntToStr(cont);
                    break;
                end;
           end;
       end;
    finally
           FreeAndNil(AccesoSeccion);
    end;

    if not lExiste then
        lstAccesosDirectos.Add(IntToStr(index_acceso) + ':' + '1');

end;

procedure TTressShell.RegistrarIconos;
begin
      //Dejar en -1 si el bot�n no registrara accion en el INICIO

      //Archivo
      TabArchivo_btnOtraEmpresa.Tag := -1;
      TabArchivo_btnConfigurarEmpresa.Tag := 24;//OK
      TabArchivo_btnCatalogoUsuarios.Tag := 22; //OK
      TabArchivo_btnExplorardorReportes.Tag := 26;
      TabArchivo_btnSQL.Tag := 25; //OK
      TabArchivo_btnBitacora.Tag := 21; //OK
      TabArchivo_btnListaProcesos.Tag := 20; //OK
      TabArchivo_btnInicio.Tag := -1;
      TabArchivo_btnImprimir.Tag := -1;
      TabArchivo_btnImprimirForma.Tag := -1;
      TabArchivo_btnImpresora.Tag := -1;
      TabArchivo_btnClaveAcceso.Tag := -1;
      TabArchivo_btnPreferencias.Tag := -1;
      TabArchivo_btnServidor.Tag := 32;  //OK
      TabArchivo_btnSalir.Tag := -1;

      //Editar
      EOperaciones_btnAgregar.Tag := -1;
      EOperaciones_btnBorrar.Tag := -1;
      EOperaciones_btnModificar.Tag := -1;
      EOperaciones_btnExportar.Tag := -1;
      ERefrescar_btnRefrescar.Tag := -1;
      EOperaciones_btnBuscarCodigo.Tag := -1;

      //Empleados
      EmpMovAlta.Tag := 0; //OK
      EmpMovBolBaja.Tag := 1; //OK
      EmpMovReingreso.Tag := 2; //OK
      EmpImportarAltas.Tag := 3; //OK
      EmpTransferencia.Tag := 3; //OK
      Emp_Remunerar.Tag := 3; //OK
      dxBarButton3.Tag := 3; //_Emp_Importar_Imagenes OK
      EmpCambioPlaza.Tag := 19; //OK
      EmpCambioPuesto.Tag := 4; //OK
      EmpCambioTurno.Tag := 5; //OK
      EmpcambioArea.Tag := 6; //OK
      EmpCambioContrato.Tag := 10; //OK
      EmpCambioPrestaciones.Tag := 7; //OK
      EmpCambioTipoNomina.Tag := 15; //OK
      EmpMultiplesCambios.Tag := 14; //OK
      EmpKardesGral.Tag := 12; //OK
      EmpAplicaEventosMasivos.Tag := 13; //OK
      empImportakardexMasivos.Tag := 13; //OK
      empCancelakardexMasivo.Tag := 13; //OK
      empCambioTurnoMasivos.Tag := 13; //OK
      EmpCambioSalario.Tag := 16; //OK
      EmpSalariosGlobales.Tag := 11; //OK
      EmpRecalcularIntegrados.Tag := 11; //OK
      EmpPromPercepVariables.Tag := 11; //OK
      EmpAplicarTabulador.Tag := 11; //OK
      EmpBancaElectronica.Tag := 9; //OK
      EmpTarjGasolina.Tag := 9; //OK
      EmpTarjDespensa.Tag := 9; //OK
      EmpRenovarSGM.Tag := 17;  //OK
      EmpImportarSGM.Tag := 17; //OK
      EmpBorrarSgm.Tag := 17;  //OK
      EmpEntregarHerramientas.Tag := 8; //OK
      EmpEntregaGlobal.Tag := 8; //OK
      EmpRegresaHerramienta.Tag := 8; //OK
      EmpEntregaHerramientaGlobal.Tag := 8; //OK
      EmpRegresaErramientaGlobal.Tag := 8; //OK
      EmpOrganigrama.Tag := 18; //OK


      //Expediente
      EmpVacacionesIndividual.Tag := 42; //OK
      EmpVacacionesGlobal.Tag := 40; //OK
      EmpCerrarVacaciones.Tag := 37; //OK
      EmpCancelarVacaciones.Tag := 37; //OK
      EmpCierreGlobalVacaciones.Tag := 37; //OK
      EmpCancelarCierreGlobalVacaciones.Tag := 37; //OK
      ExpRecalculaSaldosVaca.Tag := 37; //OK
      EmpINcapacidadesINcapacidades.Tag := 44; //OK
      EmpPermisosIndividuales.Tag := 43; //OK
      EmpPermisosGlobales.Tag := 39; //OK
      EmpPermisosCancelarGlobales.Tag := 33; //OK
      EmpPrestaPresta.Tag := 38; //OK
      EmpImportaAhorroPresta.Tag := 36; //OK
      EmpCafeteriaIndividual.Tag := 41; //OK
      EmpCafeteriaGlobal.Tag := 45; //OK
      EmpCafeteriaCorregirFechasGlobales.Tag := 35; //OK
      EmpCasetaChecadas.Tag := 34; //OK

      //Capacitaci�n
      CapaRegIndividual.Tag := 50; //OK
      CapaRegGlobal.Tag := 48; //OK
      CapaCertCancelaCursoTomadoGLobal.Tag := 47; //OK
      CapaImportaCursos.Tag := 47; //OK
      CapaCursoPRogramadoGLobal.Tag := 47; //OK
      CapaCancelCursoProgGlobal.Tag := 47; //OK
      CapaSTPSREvisar.Tag := 54; //OK
      CapaSTPSFoliar.Tag := 46; //OK
      CapaSTPSReiniciaFolios.Tag := 53; //OK
      CapaCertActual.Tag := 49; //OK
      CapaCertPorEmpleado.Tag := 52; //OK
      CapaCertVerLista.Tag := 51; //OK

      //Asistencia
      AsisPrenominaCalcula.Tag := 57; //OK
      AsisPrenominaAtorizar.Tag := 56; //OK
      AsisExtraPermisos.Tag := 63; //OK
      AsisExraPermisosglobales.Tag := 62; //OK
      AsistAutoPorAprobar.Tag := 54; //OK
      AsistImpClasifiTemporales.Tag := 65; //OK
      AsistHorTemporales.Tag := 64; //OK
      AsistRegistrosAutomaticos.Tag := 70; //OK
      AsistCorregirFechasGlobalesTarjetas.Tag := 60; //OK
      AsisAjusteRecalcularTarjetas.Tag := 69; //OK
      Excepciones.Tag := 61; //OK
      CancelarExcepciones.Tag := 58; //OK
      Intercambio.Tag := 66; //OK
      AsisPolRelojes.Tag := 67; //OK
      AsisProcTarjetas.Tag := 68; //OK
      bbExtraerChecadasBitTermGTI.Tag := 59; //OK
      bbImportarChecadas.Tag := 59; //OK
      AsisAjustIncapacidad.Tag := 55; //OK

      //N�mina
      nomExcepDiasHoras.Tag := 79; //OK
      NomExepMonto.Tag := 81; //OK
      MonExcepGlobales.Tag := 82; //OK
      NomExcepMovImpMovs.Tag := 83; //OK
      NomMovExportarMov.Tag := 83; //OK
      NomCalcular.Tag := 74; //OK
      NomLiquidacionFiniquitos.Tag := 80; //OK
      NomLiquidacionGlobal.Tag := 80; //OK
      NomAfectar.Tag := 71; //OK
      NomDesafectar.Tag := 78; //OK
      NomImprimeRecibos.Tag := 86;  //OK
      NomPagoRecibos.Tag := 86; //OK
      NomFoliarRecibos.Tag := 86; //OK
      NomRefoliarRecibos.Tag := 86; //OK
      NomImportaPagoRecibos.Tag := 86; //OK
      NomGenerarPoliza.Tag := 85; //OK
      NomImprimePolContable.Tag := 85; //OK
      NomExportaPolContable.Tag := 85; //OK
      NomImprimirListado.Tag := 84;  //OK
      NomCalRetroActivos.Tag := 84; //OK
      NomCopiarNom.Tag := 84; //OK
      NomCalDIferencias.Tag := 84; //OK
      NomPagarPorFuera.Tag := 84; //OK
      NomCancelNomPasadas.Tag := 84; //OK
      NomBorraNomActiva.Tag := 84; //OK
      NomCalculaSalarioNetoBruto.Tag := 88; //OK
      NomRastreaCalculo.Tag := 88; //OK
      NomLimpiaAcumulados.Tag := 88; //OK
      NomRecalculaAcumulados.Tag := 88; //OK
      NomCreditoAplicadoMensual.Tag := 87;//OK
      dxBarButton1.Tag := 75; //OK //Calcuo Previo de ISR
      NomImprimirCalculoISR.Tag := 75; //OK
      NomCalcAjusteRetencionFonacot.Tag := 73;  //OK
      NomCalcAjusteRetencionFonacotCancelacion.Tag := 73; //OK
      NomCalcularPagoFonacot.Tag := 73; //OK
      NomCosteoTransferencias.Tag := 76; //OK
      NomEliminarTransferencias.Tag := 76; //OK
      NomCalculoCosteo.Tag := 76; //OK
      NomDefPeriodos.Tag := 77; //OK
      NomDefNomNueva.Tag := 77; //OK
      NomCalcAguinaldo.Tag := 72; //OK
      NomImprimirAguinaldo.Tag := 72; //OK
      NomPagarAguinaldo.Tag := 72; //OK
      NomCalcularReparto.Tag := 72; //OK
      NomImprimirReparto.Tag := 72; //OK
      NomPagarAhorros.Tag := 72; //OK
      nomCierreAhorros.Tag := 72; //OK
      nomCierrePrestamos.Tag := 72; //OK
      NomCalcularRepartoUtilidades.Tag := 72; //OK
      NomImprimirRepartoUtilidades.Tag := 72; //OK
      NomPagarRepartoUtilidades.Tag := 72; //OK
      NomComparaISPT.Tag := 72; //OK
      nomImprimirISTPCompAnual.Tag := 72; //OK
      NomDIfISTPNom.Tag := 72; //OK
      NomConfDeclaracion.Tag := 72; //OK
      NomImprimirDeclaracion.Tag := 72; //OK
      NomCierreDeclaracion.Tag := 72; //OK
      NomDeclaraSalario.Tag := 72; //OK
      NomImprimirDeclaraSalario.Tag := 72; //OK
      {*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
      NomImportarAcumulados.Tag := 88;//OK
      {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
      NomRecalcularAhorros.Tag := 88;
      {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
      NomRecalcularPrestamos.Tag := 88;
      //IMSS/INFONAVIT
      dxBarLargeButton9.Tag := 95; //OK //Nuevo Pago
      dxBarLargeButton1.Tag := 91; //OK //Calcular Pago
      dxBarLargeButton3.Tag := 93; //OK //Calcular Regargos
      dxBarLargeButton2.Tag := 90; //OK //Borrar Pago
      dxBarLargeButton4.Tag := 89; //OK //Ajuste de Retencion
      dxBarLargeButton5.Tag := 96; //OK //Revisar Datos
      dxBarLargeButton7.Tag := 94; //OK //Exportar Datos
      dxBarLargeButton6.Tag := 92; //OK //Calcular Prima de Riesgo
      dxBarLargeButton8.Tag := 97; //OK //Validacion de Movimientos

      //SISTEMA
      dxBarLargeButton10.Tag := 103; //Borrar Bajas
      dxBarLargeButton15.Tag := 107; //Borrar Nominas
      dxBarLargeButton14.Tag := 109; //Borrar Tarjetas
      dxBarLargeButton13.Tag := 108; //Borrar POLL
      dxBarLargeButton16.Tag := 105; //Borrar Bitacora
      dxBarLargeButton12.Tag := 106; //Borrar Herramientas
      dxBarLargeButton11.Tag := 104; //Borrar Biometricos
      dxBarLargeButton24.Tag := 110; //Borrar Timbrado
      dxBarLargeButton18.Tag := 111; //Enrolamiento Masivo
      dxBarLargeButton17.Tag := 112; //Importar Enrolamiento Masivo
      dxBarLargeButton19.Tag := 99; //Actualizar Numero de Tarjetas
      dxBarLargeButton20.Tag := 102; //Asignacion de Numeros
      dxBarLargeButton21.Tag := 101; //Asignacion de Grupos
      SistAgregaBaseDatosEmpleados.Tag := 100;
      SistAgregaBaseDatosPruebas.Tag := 100;
      SistAgregaBaseDatosSeleccion.Tag := 100;
      SistAgregaBaseDatosVisitantes.Tag := 100;
      SistAgregaBaseDatosPresupuestos.Tag := 100;
      dxBarLargeButton23.Tag := 98; //Actualizar BD
      dxAdministradorDeEnvios.Tag := 20;

      //VENTANA
      VCerrar_btnCerrar.Tag := -1;
      VCerrar_btnCerrarTodas.Tag := -1;
      VArbolForma_btnMostrarArbol.Tag := -1;
      VArbolForma_btnBuscarForma.Tag := -1;
      VArbolForma_btnExpanderArbol.Tag := -1;
      VArbolForma_btnCompactarArbol.Tag := -1;
      VCalculadora_btnCalculadora.Tag := -1; //Pendiente

      //AYUDA
      HContenido_btnContenido.Tag := -1;
      HContenido_NuevoEnTress.Tag := -1;
      HContenido_Glosario.Tag := -1;
      HContenido_btnUsoTeclado.Tag := -1;
      HContenido_btnComoUsarAyuda.Tag := -1;
      HAcerca_btnAcercaDe.Tag := -1;
end;
end.
