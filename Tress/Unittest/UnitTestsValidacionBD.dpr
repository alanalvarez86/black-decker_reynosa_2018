{$IFDEF LINUX}
{$DEFINE DUNIT_CLX}
{$ENDIF}
program UnitTestsValidacionBD;

uses
  TestFramework,
  GUITestRunner,
  DPruebasUnitariasValidacionBD in 'DPruebasUnitariasValidacionBD.pas' {DmPruebasUnitariasValidacionBD: TDataModule},
  ZetaLicenseClasses in '..\..\Servidor\ZetaLicenseClasses.pas',
  FAutoClasses in '..\..\..\Lib\Sentinel\Paquetes\Delphi5\FAutoClasses.pas';

{$R *.res}

begin
  TGUITestRunner.runRegisteredTests;
end.
