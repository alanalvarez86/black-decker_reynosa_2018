unit FReportesMailCFG;

interface

uses
 Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ImgList,
     Buttons, StdCtrls, Mask, ShellApi, CheckLst, ComObj, ActiveX, ExtCtrls, ComCtrls,
     IniFiles,
     ZBaseShell,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     {$endif}
     DReportes,
     ZetaFecha,
     ZetaHora,
     ZetaKeyCombo, ZetaKeyLookup, TressMorado2013, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxRibbonSkins,
  dxSkinsdxRibbonPainter, cxStyles, cxClasses, dxSkinsForm, dxRibbon,
  dxStatusBar, dxRibbonStatusBar, dxSkinsdxBarPainter, dxBar, Menus,
  cxButtons, cxRadioGroup, cxContainer, cxEdit, cxCheckListBox, cxGroupBox,
  cxLocalization{$ifdef TRESS_DELPHIXE5_UP}, dxRibbonCustomizationForm{$endif};

type
  TReportesMailTest = class(TBaseShell)
    FechaDia: TLabel;
    ImageList: TImageList;
    ParametrosGB: TcxGroupBox;
    Label7: TLabel;
    eEmpresa: TZetaKeyCombo;
    SchedulerGB: TcxGroupBox;
    BkupDaysLBL: TLabel;
    BkupTimeLBL: TLabel;
    HoraAMPM: TLabel;
    eLineaComando: TEdit;
    BCalendarizar: TcxButton;
    BkupDays: TcxCheckListBox;
    BkupTime: TZetaHora;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    BEmpresa: TdxBarLargeButton;
    bejecutar: TdxBarLargeButton;
    blinea: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    cxGroupBox1: TcxGroupBox;
    cbFrecuencia: TComboBox;
    Label2: TLabel;
    eFecha: TZetaFecha;
    Label5: TLabel;
    cxGroupBox2: TcxGroupBox;
    Label4: TLabel;
    DescReportes: TListBox;
    bReportes: TcxButton;
    btnRemoverReporte: TcxButton;
    eURLImagenes: TEdit;
    eDirImagenes: TEdit;
    Label1: TLabel;
    Label6: TLabel;
    cxGroupBox3: TcxGroupBox;
    GroupBox2: TcxGroupBox;
    Label8: TLabel;
    btnBuscarLista: TcxButton;
    DescripcionLista: TEdit;
    ReporteLista: TEdit;
    GroupBox1: TcxGroupBox;
    Label3: TLabel;
    PaintBox1: TPaintBox;
    eListaUsuarios: TEdit;
    bUsuarios: TcxButton;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    GbEnvio: TcxGroupBox;
    cxLocalizer1: TcxLocalizer;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnEjecutarClick(Sender: TObject);
    procedure bLineaClick(Sender: TObject);
    procedure BCalendarizarClick(Sender: TObject);
    procedure eFechaChange(Sender: TObject);
    procedure BkupDaysClick(Sender: TObject);
    procedure BkupTimeExit(Sender: TObject);
    procedure eLineaComandoExit(Sender: TObject);
    procedure ProbarClick(Sender: TObject);
    procedure bUsuariosClick(Sender: TObject);
    procedure bReportesClick(Sender: TObject);
    procedure bEmpresaClick(Sender: TObject);
    procedure btnBuscarListaClick(Sender: TObject);
    procedure btnRemoverReporteClick(Sender: TObject);
    procedure ReporteListaExit(Sender: TObject);
    procedure cxRadioButton1Click(Sender: TObject);
    procedure cxRadioButton2Click(Sender: TObject);
    procedure DescReportesExit(Sender: TObject);
    procedure CargaTraducciones;
    // US #7328: Implementar Control de la ayuda en XE5
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
  private
    { Private declarations}
    FImagenIni : TIniFile;
    function ConstruirParametros( Lista: TStrings ): Boolean;
    function GetAtCommand: String;
    function HayDiasMarcados: Boolean;
    procedure SetControls;
    procedure ShowError(const sMessage: String);
    procedure CreaImagenesINI;
    function GetDirectorioIni: string;
    function GetURLImagenes: String;
    function GetDirImagenes: String;

  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    procedure ProcesarBatch;
  end;

var
  ReportesMailTest: TReportesMailTest;
  StoredListItemIndex : Integer = -1;
implementation

uses FViewLog,
     FTestMyEMail,
     DCliente,
     ZetaNetworkBrowser_DevEx,
     ZetaWinAPITools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo,
     ZetaMessages;

{$R *.DFM}

const
     K_ARCHIVO_INI = 'REPORTES EMAIL.INI';
     K_CONFIGURACION = 'CONFIGURACION';
     K_URL = 'URL IMAGENES';
     K_DIR = 'DIR IMAGENES';
{$ifdef VISITANTES}
     K_TOP_LISTA_EMPLEADOS = 22;
     K_TOP_LISTA_DETALLE = 134;
{$endif}

{ ********** TReportesMailTest ******** }

procedure TReportesMailTest.FormCreate(Sender: TObject);
var
 l: DWORD;
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     // US #7328: Implementar Control de la ayuda en XE5.
     with Application do
     begin
          OnException := ManejaExcepcion;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
     end;

     dmCliente := TdmCliente.Create( Self );
     dmReportes := TdmReportes.Create( self  );

     FImagenIni := TIniFile.Create( GetDirectorioIni );

     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     BLinea.Caption := 'Construir l�nea de comandos';
     BEmpresa.Caption := 'Seleccionar empresa';
     BCalendarizar.Caption := 'Calendarizar tarea';
     Application.UpdateFormatSettings := FALSE;
     {$ifdef VISITANTES}
     Label8.Visible := False;
     ReporteLista.Visible := False;
     DescripcionLista.Visible := False;
     btnBuscarLista.Visible := False;
     {Label4.Top := Label4.Top + K_TOP_LISTA_EMPLEADOS;
     DescReportes.Top := DescReportes.Top + K_TOP_LISTA_EMPLEADOS;
     bReportes.Top := bReportes.Top + K_TOP_LISTA_EMPLEADOS;
     btnRemoverReporte.Top := btnRemoverReporte.Top + K_TOP_LISTA_EMPLEADOS;
     Label1.Top := Label1.Top + K_TOP_LISTA_EMPLEADOS;
     eURLImagenes.Top := eURLImagenes.Top + K_TOP_LISTA_EMPLEADOS;
     Label6.Top := Label6.Top - K_TOP_LISTA_EMPLEADOS;
     eDirImagenes.Top := eDirImagenes.Top + K_TOP_LISTA_EMPLEADOS;
     Self.Height := Self.Height - K_TOP_LISTA_EMPLEADOS; }
     ParametrosGB.Height := 250;
     //@(am): La segunda opcion no se utiliza.
     cxRadioButton2.Visible := False;
     {$endif}
     l := GetWindowLong(Self.Handle, GWL_STYLE);
     l := l and not (WS_MAXIMIZEBOX);
     l := SetWindowLong(Self.Handle, GWL_STYLE, l);

end;

// US #7328: Implementar Control de la ayuda en XE5
procedure TReportesMailTest.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TReportesMailTest.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FImagenIni );

     FreeAndNil( dmCliente );
     FreeAndNil( dmReportes );
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
end;
procedure TReportesMailTest.CargaTraducciones;
begin
      cxLocalizer1.Active := True;
      cxLocalizer1.Locale := 2058;
end;


procedure TReportesMailTest.FormShow(Sender: TObject);
var
   oCursor: TCursor;
   sCaption : string;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        eFecha.Valor := Date;
        cbFrecuencia.ItemIndex := 1;
        eListaUsuarios.Text := '';

      //  {$ifdef DOS_CAPAS}
     //   sCaption := 'Profesional';
     //   {$else}

        sCaption := dmCliente.TipoApplicacionStr;
     //   {$endif}


     {$ifdef TRESS}
     Caption := Format( 'Tress %s: Reportes Por E-mail', [sCaption] );
     {$endif}
     {$ifdef SELECCION}
     Caption := Format('Selecci�n de Personal %s: Reportes por E-mail', [sCaption] );
     {$endif}
     {$ifdef VISITANTES}
     Caption := Format('Visitantes %s: Reportes por E-mail', [sCaption] );
     {$endif}

        {with dmCliente do
        begin
             ListaCompanies( eEmpresa.Lista );
        end;
        ActiveControl := eEmpresa;}
        ActiveControl := eFecha;
        SetControls;
        //CargaDerechos;
        
     finally
            Screen.Cursor := oCursor;
     end;
     CargaTraducciones;
end;


procedure TReportesMailTest.ProcesarBatch;
var
   i: Integer;
begin
     //dmReportes := TdmReportes.Create( Self );
     try
        with dmReportes do
        begin
             if ( ParamCount > 0 ) then
             begin
                  AppParamClear;
                  for i := 1 to ParamCount do
                  begin
                       AppParamAdd( ParamStr( i ) );
                  end;
             end;
             URLImagenes := GetURLImagenes;
             DIRImagenes := GetDirImagenes;

             ExecuteReportsViaEMail( False );
        end;
     finally
    //        FreeAndNil( dmReportes );
     end;
end;

function TReportesMailTest.ConstruirParametros( Lista: TStrings ): Boolean;
var
   sEmpresa: String;

function GetTexto( const sTexto, sParametro: String): String;
begin
     if ( Trim( sTexto ) <> '' ) then
     begin
          Result := sParametro + sTexto;
     end;
end;

function GetReportes(const Lista: TStrings;const sParametro:string):string;
var i:Integer;
begin
		 for i := 0 to Lista.Count -1  do
     begin
          Result := ConcatString(Result,Lista.Names[i],',' );
     end;
     Result := GetTexto (Result, sParametro);
end;

begin
     Result := False;
     //sEmpresa := eEmpresa.Llave;
     sEmpresa := dmCliente.Compania;
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             if ZetaCommonTools.StrVacio( sEmpresa ) then
                ZetaDialogo.zError( '� Atenci�n !', 'El C�digo De La Empresa No Puede Quedar Vaci�', 0 )
             else
                 if ( cbFrecuencia.ItemIndex = 0 ) and
                    ( Trim( eListaUsuarios.Text ) = '' ) and
                    ( DescReportes.ItemIndex  = 0 ) then
                 begin
                      ZetaDialogo.zError( '� Atenci�n !', 'Se Debe Especificar La Frecuencia' + CR_LF + 'Si No Se Especifican Lista De Usuarios � De Reportes', 0 );
                 end
                 else
                 begin
                      Add( 'EMPRESA=' + sEmpresa );
                      if ( cbFrecuencia.ItemIndex > 0 ) then
                         Add( 'FREC=' + Copy( cbFrecuencia.Text, 1, 1 ) );
                      if ( eFecha.Valor <> Date ) then
                         Add( 'FECHA=' + FormatDateTime('dd/mm/yyyy', eFecha.Valor ) );
                      Add( GetTexto( eListaUsuarios.Text, 'USUARIOS=' ) );
                      Add( GetReportes( DescReportes.Items , 'REPORTES=' ) );
                      Add( GetTexto( ReporteLista.Text, 'REPORTE_EMP=' ) );
                      Result := ( Count > 0 );
                 end;
          finally
                 EndUpdate;
          end;
     end;
     CreaImagenesINI;
end;

function TReportesMailTest.GetDirectorioIni : string;
begin
     Result := ExtractFilePath( Application.ExeName ) + K_ARCHIVO_INI;
end;

function TReportesMailTest.GetURLImagenes : String;
begin
     Result := FImagenIni.ReadString( K_CONFIGURACION, K_URL, '' );
end;

function TReportesMailTest.GetDIRImagenes : String;
begin
     Result := FImagenIni.ReadString( K_CONFIGURACION, K_DIR, '' );
end;

procedure TReportesMailTest.CreaImagenesINI;
 var sDIRImagenes, sURLImagenes : string;
begin
     sURLImagenes := eURLImagenes.Text;
     sDirImagenes := eDIRImagenes.Text;

     {$ifdef ANTES}
     if StrLleno( sDirImagenes ) OR StrLleno( sURLImagenes )  then
     begin
          with FImagenIni do
          begin
               WriteString( K_CONFIGURACION, K_DIR, VerificaDir( sDirImagenes ) );
               WriteString( K_CONFIGURACION, K_URL, VerificaDirURL( sURLImagenes ) );
          end;
     end;
     {$else}
     with FImagenIni do
     begin
          WriteString( K_CONFIGURACION, K_DIR, VerificaDir( sDirImagenes ) );
          WriteString( K_CONFIGURACION, K_URL, VerificaDirURL( sURLImagenes ) );
end;
     {$endif}
end;

procedure TReportesMailTest.cxRadioButton1Click(Sender: TObject);
begin
  inherited;
  {
  cxradiobutton2.Checked := False;
  elistaUsuarios.Enabled := True;
  bUsuarios.Enabled := True;
  ReporteLista.Enabled :=False;
  DescripcionLista.Enabled :=False;
  btnBuscarLista.enabled := False;
  ReporteLista.Color := ClInactiveCaption;
  DescripcionLista.Color := ClInactiveCaption;
  elistaUsuarios.Color := ClWhite;
  label3.Font.Color := clWindowText;
  label8.Font.Color := clInactiveCaption;
  }
  groupBox1.visible := True;
  groupBox2.visible := false;
end;

procedure TReportesMailTest.cxRadioButton2Click(Sender: TObject);
begin
 inherited;
 groupBox2.visible := true;
 groupBox1.visible := false;
 {
  cxradiobutton1.Checked := False;
  elistaUsuarios.Enabled := false;
  bUsuarios.Enabled := false;
  ReporteLista.Enabled :=true;
  DescripcionLista.Enabled :=true;
  btnBuscarLista.enabled := True;
  ReporteLista.Color := clInfoBk;
  DescripcionLista.Color := clInfoBk;
  elistaUsuarios.Color := ClInactiveCaption;
  label3.Font.Color := clInactiveCaption;
  label8.Font.Color := clWindowText;
  }
end;

procedure TReportesMailTest.DescReportesExit(Sender: TObject);
begin
  inherited;
  StoredListItemIndex := DescReportes.itemIndex;
end;

procedure TReportesMailTest.eFechaChange(Sender: TObject);
begin
     FechaDia.Caption := ZetaCommonTools.DiaSemana( eFecha.Valor );
end;

procedure TReportesMailTest.BkupDaysClick(Sender: TObject);
begin
     SetControls;
end;

procedure TReportesMailTest.BkupTimeExit(Sender: TObject);
begin
     SetControls;
end;

procedure TReportesMailTest.eLineaComandoExit(Sender: TObject);
begin
     SetControls;
end;

procedure TReportesMailTest.BtnEjecutarClick(Sender: TObject);
var
   FViewer: TViewLog;
begin
     FViewer := TViewLog.Create( Self );
     try
        with dmReportes do
        begin
             if ConstruirParametros( AppParams ) then
             begin
                  URLImagenes := VerificaDirUrl( eURLImagenes.Text );
                  DIRImagenes := VerificaDir( eDirImagenes.Text );
                  with FViewer do
                  begin
                       ShowModal;
                  end;
             end;
        end;
     finally
            FreeAndNil( FViewer );
     end;
end;

procedure TReportesMailTest.bLineaClick(Sender: TObject);
var
   sParametros: String;
   FLista: TStrings;
   i: Integer;
begin
     sParametros := '';
     FLista := TStringList.Create;
     try
        if ConstruirParametros( FLista ) then
        begin
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       sParametros := Format( '%s %s', [ sParametros, Strings [ i ] ] );
                  end;
             end;
             eLineaComando.Text := Format( '"%s" %s', [ Application.ExeName, sParametros ] );
        end;
     finally
            FreeAndNil( FLista );
     end;
end;

procedure TReportesMailTest.ShowError( const sMessage: String );
begin
     Dialogs.ShowMessage( sMessage );
end;

function TReportesMailTest.GetAtCommand: String;
const
     K_AT = ' %s /every:%s ';
     K_DAYS: array[ 0..6 ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Su', 'M', 'T', 'W', 'Th', 'F', 'S' );
var
   i: Integer;
   sHora, sDays: String;
begin
     sDays := '';
     for i := 0 to 6 do
     begin
          if BkupDays.Items[i].Checked then
          begin
               if ( sDays <> '' ) then
                  sDays := sDays + ',';
               sDays := sDays + K_DAYS[ i ];
          end;
     end;
     with BkupTime do
     begin
          sHora := Format( '%s:%s', [ Copy( Valor, 1, 2 ), Copy( Valor, 3, 2 ) ] );
     end;
     Result := Format( K_AT, [ sHora, sDays ] );
end;

procedure TReportesMailTest.BCalendarizarClick(Sender: TObject);
var
   zFileName, zParams, zDir: array[ 0..255 ] of Char;
   iValue: DWord;
   FWindowsSystemFolder : string;
begin
     FWindowsSystemFolder := ZetaWinAPITools.GetWinSysDir;
     iValue := ShellApi.ShellExecute( 0,
                                      nil,
                                      StrPCopy( zFileName, Format( '%s\at.exe', [ FWindowsSystemFolder ] ) ),
                                      StrPCopy( zParams, Format( GetAtCommand + eLineaComando.Text, [] )),
                                      StrPCopy( zDir, FWindowsSystemFolder ),
                                      SW_HIDE );
     case iValue of
          {
          ERROR_FILE_NOT_FOUND: ShowError( 'The specified file was not found' );
          ERROR_PATH_NOT_FOUND: ShowError( 'The specified path was not found' );
          }
          ERROR_BAD_FORMAT: ShowError( 'The .EXE file is invalid (non-Win32 .EXE or error in .EXE image)' );
          SE_ERR_ACCESSDENIED: ShowError( 'The operating system denied access to the specified file' );
          SE_ERR_ASSOCINCOMPLETE: ShowError( 'The filename association is incomplete or invalid' );
          SE_ERR_DDEBUSY: ShowError( 'The DDE transaction could not be completed because other DDE transactions were being processed' );
          SE_ERR_DDEFAIL: ShowError( 'The DDE transaction failed' );
          SE_ERR_DDETIMEOUT: ShowError( 'The DDE transaction could not be completed because the request timed out' );
          SE_ERR_DLLNOTFOUND: ShowError( 'The specified dynamic-link library was not found' );
          SE_ERR_FNF: ShowError( 'The specified file was not found' );
          SE_ERR_NOASSOC: ShowError( 'There is no application associated with the given filename extension' );
          SE_ERR_OOM: ShowError( 'There was not enough memory to complete the operation' );
          SE_ERR_PNF: ShowError( 'The specified path was not found' );
          SE_ERR_SHARE: ShowError( 'A sharing violation occurred' )
          else
              ZetaDialogo.ZInformation(Caption, 'La Tarea Ha Sido Agregada', 0 );
     end;
end;

procedure TReportesMailTest.SetControls;
begin
     BCalendarizar.Enabled := ZetaCommonTools.StrLleno( eLineaComando.Text ) and
                              HayDiasMarcados and ZetaCommonTools.StrLleno( BkupTime.Valor );
end;

function TReportesMailTest.HayDiasMarcados: Boolean;
var
   i: Integer;
begin
     for i := 0 to 6 do
     begin
          Result := BkupDays.items[ i ].Checked;
          if Result then
             Break;
     end;
end;

procedure TReportesMailTest.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

procedure TReportesMailTest.ProbarClick(Sender: TObject);
begin
     EMailTest := TEMailTest.Create( Self );
     try
        EMailTest.ShowModal;
     finally
            FreeAndNil( EMailTest );
     end;
end;


procedure TReportesMailTest.bUsuariosClick(Sender: TObject);
 var
    sKey, sDescription: String;
begin
     dmReportes.cdsUsuariosLookup.Conectar;
     if ( dmReportes.cdsUsuariosLookup.Search_DevEx( VACIO, sKey, sDescription  ) ) then
     begin
          eListaUsuarios.Enabled := True;
     	  eListaUsuarios.Text := ConcatString( eListaUsuarios.Text,  sKey, ',' );
     	  ReporteLista.Text := VACIO;
          DescripcionLista.Text := VACIO;
          ReporteLista.Enabled := False;
     end;
end;

procedure TReportesMailTest.bReportesClick(Sender: TObject);
 var
    sKey, sDescription: String;
begin

     if ( dmReportes.cdsSuscripReportes.Search_DevEx( VACIO, sKey, sDescription ) ) then
     begin
          DescReportes.Items.Add( sKey +'='+sDescription );
          //eListaReportes.Text := ConcatString( eListaReportes.Text , sKey, ',' );
     end;
     
end;


procedure TReportesMailTest.bEmpresaClick(Sender: TObject);
begin
     inherited;
     AbreEmpresa( False );

end;

procedure TReportesMailTest.btnBuscarListaClick(Sender: TObject);
var
    sKey, sDescription: String;
begin
     if ( dmReportes.cdsSuscripReportes.Search_DevEx( VACIO, sKey, sDescription ) ) then
     begin
          eListaUsuarios.Text := VACIO;
					eListaUsuarios.Enabled := false;

         // ReporteLista.Enabled := True;
          ReporteLista.Text := sKey;
          DescripcionLista.Text := sDescription;

     end;
end;

procedure TReportesMailTest.btnRemoverReporteClick(Sender: TObject);
begin
     inherited;
     if (descreportes.Focused) then
        DescReportes.Items.Delete(DescReportes.ItemIndex)
     else
     DescReportes.Items.Delete(StoredListItemIndex);

     if( descreportes.Items.Count = 0) then
         StoredListItemIndex := -1;
     DescReportes.ItemIndex :=   StoredListItemIndex;
     StoredListItemIndex := DescReportes.ItemIndex;

end;

procedure TReportesMailTest.ReporteListaExit(Sender: TObject);
var
   sDescrip:string;
   lActive, lConfidential: boolean;
begin
  	 inherited;
     dmReportes.cdsSuscripReportes.LookupKey(ReporteLista.Text,VACIO,sDescrip,lActive, lConfidential);
		 DescripcionLista.Text := sDescrip;
end;

end.
