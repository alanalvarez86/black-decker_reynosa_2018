unit DReportes;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet, FileCtrl,
  {$ifndef VER130}
  Variants,
  {$endif}
  DReportesGenerador,
  DTotaliza,
  {$IFDEF TRESS_DELPHIXE5_UP}
  DEmailService,
  {$ELSE}
  DEmailServiceD7,
  {$ENDIF}
  ZetaCommonLists,
  {$ifdef DOS_CAPAS}
       DServerSistema,
       DZetaServerProvider,
  {$else}
       Sistema_TLB,
  {$endif}
  ZReportTools;

type
  eTipoPeriodoReportes = (tpDiario,tpSemanal,tpCatorcenal,tpQuincenal,tpMensual,tpDecenal,tpSemanalA,tpSemanalB,tpQuincenalA,tpQuincenalB,tpCatorcenalA,tpCatorcenalB);
  TdmReportes = class(TdmReportGenerator)
    cdsUsuariosLookup: TZetaLookupDataSet;
    cdsSuscripReportes: TZetaLookupDataSet;


    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsSuscripReportesLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
    procedure cdsUsuariosLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripReportesAlCrearCampos(Sender: TObject);
    procedure cdsUsuariosLookupGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);


  private
    { Private declarations }
    FArchivoHTML: String;
    //FHTMLDefault: String;
    FEmpresa: String;
    FBorraArchivo: Boolean;
    FFrecuencia: integer;
    FUsuarios: String;
    FReportes: String;
    FHost: String;
    FPort:Integer;
    FAuthMethod: eAuthTressEmail;
    FPasword:String;
    FUserID: String;
    FFromAddress: String;
    FFromName: String;
    FErrorMail: String;
    FSoloArchivos : String;
    FURLImagenes: String;
    FDIRImagenes: string;
    //FExportaPDF : Boolean;
    FUsuariosPlain: TStrings;
    FUsuariosHTML: TStrings;
    FErroresMail: TStrings;
    FListaReportes: TStrings;
    FClasifActivo: eClasifiReporte;
    FClasificaciones: TStrings;
    dmEmailService : TdmEmailService;
    FReporteEmpleados :Integer;
    cdsListaEmpleados : TZetaClientDataSet;


    {$ifdef DOS_CAPAS}
    FServerSistema: TdmServerSistema;
    {$else}
    FServerSistema: IdmServerSistemaDisp;


    function GetServerSistema: IdmServerSistemaDisp;
    {$endif}

    function ConectaGlobal: Boolean;
    function ConectaHost: Boolean;
    function GetSuscripciones: Boolean;
    procedure AppParamsPrepare;
    procedure ActualizaListaUsuarios( oLista: TStrings );
    procedure BorraArchivos;
    procedure ModificaListaCampos(Lista: TStrings);
    procedure ModificaListas;
    function GetSobreTotales: Boolean;
    procedure ValidaUsuarios(const sUsuarios: string);
    procedure EscribeMensaje(const sMensaje: string);
    procedure MueveTemporales;
    function GetClasificaciones(oClasificaciones: TStrings): string;
    procedure ObtieneEntidadGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    //procedure GetReportesLookup(const eClasifi: eClasifiReporte; oClasificaciones: TStrings);
    function SistemaBloqueado: Boolean;
  protected
    function GeneraReporte( const lHTMLOnly: Boolean ): Boolean;
    function GeneraUnReporte(const iReporte: Integer; const lHTMLOnly : Boolean ): Boolean;
    function GetExtensionDef : string;override;
    function GetSoloTotales: Boolean;
    function GeneraArchivoAscii: Boolean;
    function GetDirectorioImagenes: string;

    procedure DoOnGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoAfterGetResultado( const lResultado : Boolean; const Error : string );override;
    procedure DoOnGetReportes( const lResultado: Boolean );override;
    procedure DoOnGetDatosImpresion;override;
    procedure ProcesaReportesViaEmail( const lHTMLOnly: Boolean );
    procedure GeneraOutPut;
    procedure GeneraQRReporte;
    procedure GeneraMailError( const Error : string );
    procedure SendEmail(const Tipo: eEmailType; const lMsgNormal : Boolean; oUsuarios: TStrings);
    procedure SendEmailError;
    procedure LogError(const sTexto: String;const lEnviar: Boolean = FALSE);override;
    procedure ModificaImagenHtml;
    procedure ModificaArchivoImagen(const sArchivo: string);
    procedure AgregaAttachments( oLista : TStrings );
    procedure AgregaEmailUsuario( const iVacio: Integer );
    procedure EliminaReporte( const iReporte : integer );
    procedure GetNombresArchivos;

    function PreparaPlantilla( var sError : WideString ) : Boolean;override;
    procedure DesPreparaPlantilla;override;
    function GeneraReportesEmpleados( const lHTMLOnly: Boolean ): Boolean;
    procedure DoBeforePreparaAgente( var oParams:Olevariant );override;


    {$ifdef DOS_CAPAS}
    property ServerSistema: TdmServerSistema read FServerSistema;
    {$else}
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    {$endif}
  public
    { Public declarations }
    property Empresa: String read FEmpresa write FEmpresa;
    property Frecuencia: Integer read FFrecuencia write FFrecuencia;
    property ArchivoHTML: String read FArchivoHTML;
    property DIRImagenes: string read FDIRImagenes write FDIRImagenes;
    property URLImagenes: string read FURLImagenes write FURLImagenes;
    property ClasifActivo: eClasifiReporte read FClasifActivo write FClasifActivo;
    property Clasificaciones: TStrings read FClasificaciones write FClasificaciones;
    function ExecuteReportsViaEMail( const lHTMLOnly: Boolean ): Boolean;
    procedure AppParamClear;
    procedure AppParamAdd( const sValue: String );
  end;

var
  dmReportes: TdmReportes;

implementation
uses
    DCliente,
    DGlobal,
    DDiccionario,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaDialogo,
    ZGlobalTress,
    ZQrReporteListado,
    ZFuncsCliente,
    ZReportConst,
    ZReporteAscii,
    ZetaTipoEntidad,
    ZetaTipoEntidadTools,
    FLookupReporte_DevEx,
    FAutoClasses,
    FEmailWorkingFile,
    ZetaServerTools, DBasicoCliente, ZetaRegistryCliente;

{$R *.DFM}

const
     K_CUADROVACIO = '&nbsp';
     K_NOMBRE_BITACORA = 'ReportesEmail.LOG';
     K_MENSAJE_ERROR_HTML = '<HTML> '+
                            '<FONT face=Arial color=#800080 size=4> '+
                            '<HEAD> '+
                            {'<P><IMG height=60 alt="Logo de la Empresa" '+
                            'src= %s  width=100 align=right border=0> <!--Logotipo-->'+
                            '</P> '+}
                            '<STRONG> '+
                            '        <H1> %s <!--Titulo del Reporte--> </H1> '+
                            '        <P> Fecha: %s <!--Fecha del Reporte--></P> '+
                            '</STRONG></HEAD></FONT> '+
                            '<BODY> '+
                            '<DIV align=center> '+
                            '<FONT color=#FF0000 size=5>Notificaci�n Del M�dulo De Reportes V�a E-mail. '+
                            '<P>El Reporte </P> '+
                            '<P><STRONG>  %s <!--Nombre del Reporte--> </STRONG></P> '+
                            '<P>%s <!--Error Reportado--></P> '+
                            '</FONT></DIV></BODY></HTML>';
     K_MENSAJE_ERROR_PLAINTEXT = 'Notificaci�n Del M�dulo De Reportes V�a E-mail. ' +
                                 'El Reporte ' +
                                 '%s ' +
                                 '%s ';



procedure TdmReportes.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerSistema := TdmServerSistema.Create( self );
     {$endif}
     dmEmailService := TdmEmailService.Create( self );
     dmDiccionario := TdmDiccionario.Create( self );

     FUsuariosPlain := TStringList.Create;
     FUsuariosHTML := TStringList.Create;
     FErroresMail := TStringList.Create;
     FListaReportes := TStringList.Create;
     cdsListaEmpleados := TZetaClientDataSet.Create(Self);

     SetLogFileName( ExtractFilePath( Application.ExeName ) + K_NOMBRE_BITACORA );
     inherited;
end;

procedure TdmReportes.DataModuleDestroy(Sender: TObject);
begin
    {$ifdef DOS_CAPAS}
     FreeAndNil( FServerSistema );
    {$endif}
     FreeAndNil( FormaLookupReporte_DevEx );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmEmailService );
     FreeAndNil( FListaReportes );
     FreeAndNil( FErroresMail );
     FreeAndNil( FUsuariosHTML );
     FreeAndNil( FUsuariosPlain );
     FreeAndNil( cdsListaEmpleados );
     inherited;

end;

{$ifndef DOS_CAPAS}
function TdmReportES.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;
{$endif}


procedure TdmReportes.AppParamsPrepare;
const
     aTipo: array[ eTipoPeriodoReportes] of ZetaPChar = ( 'DI','S','C','Q','M','D','SA','SB','QA','QB','CA','CB');
     K_DIARIO = 'D';
     K_SEMANAL = 'S';
     K_MENSUAL = 'M';
     K_CADA_HORA = 'C';
     K_ESPECIAL = 'E';
var
   sFrecuencia, sTipoNomina: String;
   iTipoNomina: integer;
   i: eTipoPeriodoReportes;
begin
     with FAppParams do
     begin
          FEmpresa := Values[ 'EMPRESA' ];
          sFrecuencia := Values[ 'FREC' ];
          if ( sFrecuencia = K_DIARIO ) then
             FFrecuencia := 0
          else if ( sFrecuencia = K_SEMANAL ) then
               FFrecuencia := 1
          else if ( sFrecuencia = K_MENSUAL ) then
               FFrecuencia := 2
          else if ( sFrecuencia = K_CADA_HORA ) then
               FFrecuencia := 3
          else if ( sFrecuencia = K_ESPECIAL ) then
               FFrecuencia := 4
          else FFrecuencia := -1;

          {CV: Se requiere que el a�o default se mueva?}
          if ZetaCommonTools.StrLleno( Values[ 'FECHA' ] ) then
             dmCliente.FechaDefault := StrToDate( QuitaComillas( Values[ 'FECHA' ] ) );

          FUsuarios := Values[ 'USUARIOS' ];
          ValidaUsuarios(FUsuarios);

          FReportes := Values[ 'REPORTES' ];

          if StrLleno( FReportes ) then
             FListaReportes.CommaText := FReportes;

          //FExportaPDF := Values[ 'OUTPUT' ] = K_PDF;
          FSoloArchivos := Values[ 'SOLOARCHIVOS' ];

          sTipoNomina := UpperCase( Values['TIPO']);
          iTipoNomina := -1;

          if StrLLeno( sTipoNomina ) then
          begin
               for i:= Low( aTipo ) to High( aTipo ) do
                   if ( sTipoNomina = aTipo[i] ) then
                   begin
                        iTipoNomina := Ord( i );
                        Break;
                   end;
          end;

          with Params do
          begin
               AddInteger( 'Year', StrToIntDef( Values['YEAR'], -1 ) );
               AddInteger( 'Tipo', iTipoNomina );
          end;

          FReporteEmpleados :=  StrToIntDef( Values[ 'REPORTE_EMP' ],-1);
          dmCliente.GeneraListaEmpleados := False;

          if ( FFrecuencia = -1 ) and ZetaCommonTools.StrVacio( FUsuarios ) and ZetaCommonTools.StrVacio( FReportes ) then
             raise Exception.Create( 'No Se Ejecut� El Proceso Debido A Que No Se Especific� Frecuencia, Lista De Usuarios Ni Lista De Reportes ' );
     end;
end;

procedure TdmReportes.ValidaUsuarios( const sUsuarios : string );
 var oLista : TStrings;
     i : integer;
begin
     oLista := TStringList.Create;
     try
        oLista.CommaText := sUsuarios;
        for i:=0 to oLista.Count - 1 do
        begin
             if (StrToIntDef( oLista[i], -1 ) = -1) then
                raise Exception.Create( Format('"%s" No es un N�mero de Usuario V�lido',[ oLista[i] ]) );
        end;
     finally
            FreeAndNil(oLista);
     end;
end;

procedure TdmReportes.AppParamClear;
begin
     FAppParams.Clear;
end;

procedure TdmReportes.AppParamAdd( const sValue: String );
begin
     FAppParams.Add( UpperCase( sValue ) );
end;

function TdmReportes.ExecuteReportsViaEMail( const lHTMLOnly: Boolean ): Boolean;
begin
     Result := False;
     if ( FAppParams.Count > 0 ) then
     begin
          ProcesaReportesViaEmail( lHTMLOnly );
          Result := True;
     end
     else
     begin
          LogError( 'No Se Especific� Ning�n Par�metro' );
          Log( 'TRESSMAIL.EXE Nombre_Empresa Frecuencia' );
          Log( 'Par�metros:' );
          Log( 'Nombre_Empresa: C�digo de Empresa( CM_CODIGO )' );
          Log( 'Frecuencia: 0=Diario' )
     end;
end;

procedure TdmReportes.ProcesaReportesViaEmail( const lHTMLOnly: Boolean );
var
   lSalirTress: Boolean;
   sMensajeError: String;
   lLoop: Boolean;
   procedure validarHTTPEmail( var sMensajeError: String );
   begin
        sMensajeError := VACIO;
        if ClientRegistry.TipoConexion = conxHTTP then
        begin
             dmCliente.ValidarConexionHTTP( lLoop, lSalirTress, sMensajeError, True);
        end;
   end;
begin
     validarHTTPEmail( sMensajeError );
     if strVacio( sMensajeError ) then
     begin
          if InitAutorizacion( okReportesMail ) then
          begin
               Log( '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*' );
               Log( 'Proceso: Reportes V�a EMail' );
               Log( 'Inicio: '+ FormatDateTime('dd/mmm/yyyy hh:mm:ss', Now ) );
               Log( '' );
               try
                  if  ( ( dmCliente.Usuario = 0 )or ( dmCliente.GetGrupoActivo <> D_GRUPO_SIN_RESTRICCION ) ) and ( SistemaBloqueado ) then
                      LogError( 'El Administrador Ha Bloqueado el Acceso al Sistema', TRUE )
                  else
                  begin
                       AppParamsPrepare;
                       if dmCliente.BuscaCompany( FEmpresa ) then
                       begin
                            if FReporteEmpleados = -1 then
                               GeneraReporte( lHTMLOnly )
                            else
                                GeneraReportesEmpleados(lHTMLOnly );
                       end
                       else
                           LogError( 'La L�nea de Comandos Est� Mal Configurada: Empresa ' + FEmpresa + ' No Existe', TRUE );
                  end;
               except
                     on Error:Exception do
                     begin
                          LogError( 'Error Al Ejecutar Reportes V�a E-mail: ' + Error.Message, TRUE );
                     end;
               end;
               Log( 'Final: '+ FormatDateTime('dd/mmm/yyyy hh:mm:ss', Now ) );
               Log( '*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*' );
          end
          else
          begin
               Log( 'El M�dulo De ' + Autorizacion.GetModuloStr( okReportesMail ) + ' No Est� Autorizado' + CR_LF + 'Consulte A Su Distribuidor' );
               Log( '' );
          end;
     end
     else
     begin
          Log ( sMensajeError );
          Log( '' );
     end;
end;

procedure TdmReportes.LogError(const sTexto: String;const lEnviar: Boolean );
begin
     inherited LogError( sTexto, lEnviar );
     if lEnviar then
     begin
          FErroresMail.Add( '~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*' );
          FErroresMail.Add( 'ERROR en el Reporte '+
                            ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, FFrecuencia ) +
                            ' - ' +
                            NombreReporte );
          FErroresMail.Add( sTexto );
     end;
end;

procedure TdmReportes.AgregaEmailUsuario( const iVacio: Integer );
var
   sUsuario: String;
begin
     with cdsUsuarios do
     begin
          if Locate( 'US_CODIGO', VarArrayOF( [ cdsSuscripcion.FieldByName( 'US_CODIGO' ).AsInteger ] ), [] ) then
          begin
               sUsuario := FieldByName( 'US_EMAIL' ).AsString;
               if ( eEmailFormato( FieldByName( 'US_FORMATO' ).AsInteger ) = efHTML ) then
                  FUsuariosHTML.AddObject( sUsuario, TObject( iVacio ) )
               else
                   FUsuariosPlain.AddObject( sUsuario, TObject( iVacio ) );
          end;
     end;
end;

procedure TdmReportes.GetNombresArchivos;
begin
     FArchivoHTML := ZReportTools.DirPlantilla;
     {FHTMLDefault := Global.GetGlobalString( K_GLOBAL_EMAIL_PLANTILLA );
     if ZetaCommonTools.StrLleno( FHTMLDefault ) and ZetaCommonTools.StrVacio( ExtractFileDir( FHTMLDefault ) ) then
       FHTMLDefault := FArchivoHTML + FHTMLDefault;}
     FArchivoHTML := FArchivoHTML + 'Def' + FormatDateTime( 'hhmmss', Time ) + '.htm';
end;


procedure TdmReportes.EliminaReporte( const iReporte : integer );
 var sReporte : string;
     i : integer;
begin
     if FBorraArchivo then
        DeleteFile( FArchivoHTML );

     sReporte := IntToStr( iReporte );

     for i:= 0 to FListaReportes.Count -1 do
     begin
          if sReporte = FListaReportes[ i ] then
          begin
               FListaReportes.Delete( i );
               Break;
          end;
     end;
end;


function TdmReportes.GeneraReporte( const lHTMLOnly: Boolean ): Boolean;
 var i, iReporte, iOldReporte : integer;
     oCursor : TCursor;
     lEsSupervisor: Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Result := ConectaGlobal;
        if Result then
        begin
             //GetNombresArchivos;
             Result := GetSuscripciones;

             if Result then
             begin
                  iOldReporte := -1;
                  lEsSupervisor := FALSE;
                  with cdsSuscripcion do
                  begin
                       while not Eof do
                       begin
                            iReporte := FieldByName( 'RE_CODIGO' ).AsInteger;
                            FUsuariosHTML.Clear;
                            FUsuariosPlain.Clear;
                            AgregaEmailUsuario( FieldByName( 'SU_VACIO' ).AsInteger );

                            if ( ( FUsuariosHTML.Count + FUsuariosPlain.Count ) > 0 ) then
                            begin
                                 if ( iReporte <> iOldReporte ) or lEsSupervisor then
                                 begin
                                      FArchivoHTML := VACIO;

                                      if ( iOldReporte >=0 ) then
                                         EliminaReporte( iReporte );
                                      Result := GeneraUnReporte( iReporte, lHTMLOnly );

                                      {$ifdef TRESS}
                                      if cdsReporte.Active then
                                         lEsSupervisor := eClasifiReporte( cdsReporte.FieldByName( 'RE_CLASIFI' ).AsInteger ) = crSupervisor;
                                      {$else}
                                      lEsSupervisor := FALSE;
                                      {$endif}
                                      iOldReporte := iReporte;
                                 end;

                                 if NOT Result then
                                 begin
                                      ActualizaListaUsuarios( FUsuariosHTML );
                                      ActualizaListaUsuarios( FUsuariosPlain );
                                 end;                                 
                                 
                                 if ( DatosImpresion.Tipo in [tfHTML,tfHTM] ) AND UnSoloHTML then
                                    SendEmail( emtHtml, Result, FUsuariosHTML )
                                 else
                                     SendEmail( emtTexto, Result, FUsuariosHTML );
                                 SendEmail( emtTexto, Result, FUsuariosPlain );                                 

                            end;

                            Next;

                       end;
                  end;
             end;


             for i:= 0 to FListaReportes.Count - 1 do
             begin
                  FArchivoHTML := '';
                  iReporte := StrToIntDef( FListaReportes[ i ], 0 );
                  if iReporte > 0 then
                  begin
                       GeneraUnReporte( iReporte, lHTMLOnly );
                       Log( Format( 'El Reporte #%d Ha sido Generado en: ' + CR_LF + '" %s "',[iReporte, FArchivoHTML]));
                  end;
             end;

             if NOT Result AND ( FListaReportes.Count = 0 ) then
                SendEmailError;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TdmReportes.ConectaGlobal: Boolean;
begin
     with Global do
     begin
          Conectar;
          FHost := Global.GetGlobalString( K_GLOBAL_EMAIL_HOST );
          FPort := Global.GetGlobalInteger( K_GLOBAL_EMAIL_PORT );
          FAuthMethod :=  eAuthTressEmail( Global.GetGlobalInteger(K_GLOBAL_EMAIL_AUTH) - 1 );
          Result := ZetaCommonTools.StrLleno( FHost );
          if not Result then
             LogError( 'Datos Globales No Configurados' + CR_LF + 'Favor De Indicar El Servidor De Correos En El Cat�logo de Globales' );
          FUserID := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_USERID ), 'ReportesViaEmail' );
          FPasword := Global.GetGlobalString( K_GLOBAL_EMAIL_PSWD );
          FFromAddress := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMADRESS ), 'ReportesViaEmail@dominio.com' );
          FFromName := StrDef( Global.GetGlobalString( K_GLOBAL_EMAIL_FROMNAME ), 'Sistema TRESS-M�dulo Reportes V�a E-mail' );
          FErrorMail := Global.GetGlobalString( K_GLOBAL_EMAIL_MSGERROR );
     end;
end;

function TdmReportes.GetDirectorioImagenes: string;
begin
     //Result := 'http://WWW.TRESS.COM.MX/media/';
     Result := FURLImagenes;
end;

procedure TdmReportes.ModificaArchivoImagen( const sArchivo : string );
 const
      K_IMAGE_DIR = 'src="';
 var
    //i : integer;
    oLista : TStrings;
    //sImageName : string;
begin
     if FileExists( sArchivo ) then
     begin
          oLista := TStringList.Create;
          try
             oLista.LoadFromFile( sArchivo );

             {for i:= 0 to FNombresImagenes.Count - 1 do
             begin
                  sImageName := FNombresImagenes.Names[ i ];
                  oLista.Text := StrTransAll( oLista.Text,
                                              ZReportTools.DirPlantilla + sImageName,
                                              GetDirectorioImagenes + FNombresImagenes.Values[ sImageName ] );
             end;}
             {$ifdef ANTES}
             oLista.Text := StrTransAll( oLista.Text,
                                         K_IMAGE_DIR,
                                         K_IMAGE_DIR + FURLImagenes );
             {$else}
             if StrLleno( FURLImagenes ) then
                oLista.Text := StrTransAll( oLista.Text,
                                            K_IMAGE_DIR + VerificaDir( ExtractFileDir(sArchivo) ),
                                            K_IMAGE_DIR + VerificaDirURL( FURLImagenes ) );
             {$endif}
             oLista.SaveToFile( sArchivo );
          finally
                 FreeAndNil( oLista );
          end;
     end;
end;

procedure TdmReportes.ModificaImagenHtml;
 var
    iPageCount : integer;
    sArchivo, sDir, sExt : string;
    oFile: TSearchRec;
begin
     if ( DatosImpresion.Tipo in [tfHtml,tfHtm] ) then
     begin
          sArchivo := FArchivoHTML;
          iPageCount := 0;
          if UnSoloHTML then
          begin
               ModificaArchivoImagen( sArchivo );
          end
          else
          begin
               sExt := ExtractFileExt( sArchivo );
               sDir := ExtractFilePath( sArchivo );
               sArchivo := Copy( sArchivo, 1, Pos( sExt, sArchivo )-1 ) + '*' + sExt;

               if FindFirst( sArchivo, faArchive, oFile) = 0 then
               begin
                    ModificaArchivoImagen( sDir + oFile.Name );
                    Inc( iPageCount );
                    while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
                    begin
                         ModificaArchivoImagen( sDir + oFile.Name );
                         Inc( iPageCount );
                    end;
               end;
          end;
     end;
end;

procedure TdmReportes.ActualizaListaUsuarios( oLista: TStrings );
var
   i: Integer;
begin
     i := 0;
     with oLista do
     begin
          while ( i < Count ) do
          begin
               if ( Integer( Objects[ i ] ) > 0 ) then
                   Delete( i )
               else
                   Inc( i );
          end;
     end;
end;

procedure TdmReportes.GeneraMailError( const Error : string );
 const K_ARCHIVO = 'Error$Temp';
 var oArchivo : TStrings;
     sMensaje, sArchivo : string;
begin
     oArchivo := TStringList.Create;
     try
        if DatosImpresion.Tipo = tfHTML then
        begin
             sMensaje := Format ( K_MENSAJE_ERROR_HTML, [ {GetDirectorioImagenes,}
                                                          cdsReporte.FieldByName('RE_TITULO').AsString,
                                                          FormatDateTime('dd/mmm/yyyy', DATE),
                                                          NombreReporte,
                                                          Error] );

             sArchivo := ZReportTools.DirPlantilla + K_ARCHIVO + '.html';
        end
        else
        begin
             sMensaje := Format( K_MENSAJE_ERROR_PLAINTEXT, [ NombreReporte,
                                                              Error] );
             sArchivo := ZReportTools.DirPlantilla + K_ARCHIVO + '.txt';
        end;

        oArchivo.Text := sMensaje;
        oArchivo.SaveToFile( sArchivo  {$IFDEF TRESS_DELPHIXE5_UP}, TEncoding.Unicode   {$ENDIF});
     finally
            FArchivoHTML := sArchivo;
            FreeAndNil( oArchivo );
     end;

end;

procedure TdmReportes.DoOnGetResultado( const lResultado : Boolean; const Error : string );
begin
     if lResultado then
     begin
          if ( not GeneraListaEmpleados ) then
             GeneraOutPut;
     end
     else
     begin
          GeneraMailError( Error );
          //ActualizaListaUsuarios( FUsuariosHTML );
          //ActualizaListaUsuarios( FUsuariosPlain );
     end;
end;

procedure TdmReportes.DoAfterGetResultado( const lResultado : Boolean; const Error : string );
begin
     if lResultado and ( not GeneraListaEmpleados ) then
     begin
          FArchivoHTML := DatosImpresion.Exportacion;
          ModificaImagenHtml;
     end;
end;

procedure TdmReportes.DoOnGetReportes( const lResultado: Boolean );
begin
     EscribeMensaje( Format( 'Procesando Reporte%s', [CR_LF + NombreReporte] ) );
end;

function TdmReportes.GeneraUnReporte(const iReporte: Integer; const lHTMLOnly: Boolean ): Boolean;
begin
     Result := GetResultado( iReporte, TRUE );
     if Result AND lHTMLOnly then
        Log( Format( 'El Reporte Fu� Generado En El Archivo %s', [ FArchivoHTML ] ) );
end;

procedure TdmReportes.MueveTemporales;
 var
    sExt, sDir : string;
    oFile: TSearchRec;

begin
     if DirectoryExists( FDirImagenes ) then
     begin
          if DatosImpresion.Tipo IN [ tfHTML,tfHTM ] then
          begin
               sExt := '.JPG';
               sDir := ExtractFilePath( FArchivoHTML );

               if FindFirst( sDir + ChangeFileExt(ExtractFileName( FArchivoHTML ), VACIO) + '*_I' + sExt, faArchive, oFile) = 0 then
               begin
                    CopyFile( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( sDir + oFile.Name ),
                              {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( FDirImagenes +oFile.Name ),
                              FALSE );

                    while ( FindNext( oFile ) = 0 ) do
                    begin
                         CopyFile( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( sDir + oFile.Name ),
                                   {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( FDirImagenes +oFile.Name ),
                                   FALSE );
                    end;
               end;
          end;
     end;
end;

procedure TdmReportes.AgregaAttachments( oLista : TStrings );
 var
    oFile: TSearchRec;
    sExt, sDir : string;
    iPageCount : integer;
begin
     iPageCount := 0;

     if DatosImpresion.Tipo IN [ tfHTML,tfHTM,tfTXT,tfWMF,tfEMF,tfBMP,tfPNG,tfJPEG,tfSVG ] then
     begin
          sExt := ExtractFileExt( FArchivoHTML );
          sDir := ExtractFilePath( FArchivoHTML );
          if FindFirst( Copy( FArchivoHTML, 1, Pos( sExt, FArchivoHTML )-1 ) + '*' + sExt, faArchive, oFile) = 0 then
          begin
               oLista.Add( sDir + oFile.Name );
               Inc( iPageCount );
               while ( iPageCount <= FPageCount) AND ( FindNext( oFile ) = 0 ) do
               begin
                    oLista.Add( sDir + oFile.Name );
                    Inc( iPageCount );
               end;
          end;
     end
     else
         oLista.Add( FArchivoHTML );
end;

procedure TdmReportes.EscribeMensaje( const sMensaje: string );
begin
     if EmailWorkingFile <> NIL then
        EmailWorkingFile.WriteMensaje( sMensaje );
end;

procedure TdmReportes.SendEmail( const Tipo: eEmailType;
                                 const lMsgNormal : Boolean;
                                 oUsuarios: TStrings );
var
   sTipo: String;
   sErrorMsg : string;
begin
     if ( oUsuarios.Count > 0 ) then
     begin
          with dmEmailService do
          begin
               ConectaHost;

               SubType := Tipo;
               Subject := 'Reporte ' + ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, FFrecuencia ) +
                          ' - '+ NombreReporte;

               ToAddress.AddStrings( oUsuarios );

               if ( SubType = emtHtml ) AND FileExists( FArchivoHTML )  then
               begin
                    MessageText.LoadFromFile( FArchivoHTML );
               end
               else
               begin
                    MessageText.Add( 'Reporte Enviado: '+ Subject );
                    AgregaAttachments( Attachments );
               end;

               MueveTemporales;

               //PENDIENTE
               //PostMessage.Date := DateToStr( Date );

               EscribeMensaje( Format( 'Enviando Reporte%s', [CR_LF + NombreReporte] ) );
               if Validate( sErrorMsg ) then
               begin
                    if SendEmail( sErrorMsg ) then
                    begin
                         if ( Tipo = emtHtml ) then
                            sTipo := 'en Formato HTML '
                         else
                             sTipo := 'con Archivo Adjunto ';

                         if lMsgNormal then
                         begin
                         			if FReporteEmpleados = -1 then
																Log( Format( 'Reporte # %d Enviado %s A los Usuarios: %s', [ CodigoReporte, sTipo, CR_LF + oUsuarios.Text ] ) )
                            	else
                                Log(Format('Se envi� reporte #%d - %s a Empleado: %d Correo Electr�nico:%s',[CodigoReporte,NombreReporte,dmCliente.Empleado ,FUsuariosPlain.Text ]));
                         end;
                    end
                    else
                        LogError( 'Error al Tratar de Enviar los Correos ' + CR_LF + sErrorMsg );
               end
               else
                   LogError( 'Se encontr� un Error ' + CR_LF + sErrorMsg );
          end;
     end;
end;

procedure TdmReportes.SendEmailError;
 var sErrorMsg : string;
begin
     {$ifdef ANTES}
     if ZetaCommonTools.StrVacio( FErrorMail ) then
     begin
          LogError( 'No Se Ha Especificado La Direccion de E-mail Para Errores' + CR_LF +
                    'Revisar Dentro Del Sistema Tress En Globales Empresa\Reportes V�a E-mail La Configuraci�n' );
     end;
     if ( FErroresMail.Count > 0 ) and ZetaCommonTools.StrLleno( FErrorMail ) and ConectaHost then
     begin
          with Email do
          begin
               SubType := mtPlain;
               with PostMessage do
               begin
                    ToAddress.Clear;
                    Body.Clear;
                    Attachments.Clear;
                    Subject := 'ERROR al Enviar Mensaje -  M�dulo De Reportes V�a E-mail';

                    ToAddress.Add( FErrorMail );
                    Body.Add( 'Notificaci�n Del M�dulo De Reportes V�a E-mail' );
                    Body.AddStrings( FErroresMail );
               end;
               PostMessage.Date := DateToStr( Date );
               SendMail;
          end;
     end;
     {$else}
     if ZetaCommonTools.StrVacio( FErrorMail ) then
     begin
          LogError( 'No Se Ha Especificado La Direccion de E-mail Para Errores' + CR_LF +
                    'Revisar Dentro Del Sistema Tress En Globales Empresa\Reportes V�a E-mail La Configuraci�n' );
     end;
     if ( FErroresMail.Count > 0 ) and ZetaCommonTools.StrLleno( FErrorMail ) then
     begin
          with dmEmailService do
          begin
               ConectaHost;
               SubType := emtTextoError;

               Subject := 'ERROR al Enviar Mensaje -  M�dulo De Reportes V�a E-mail';
               ToAddress.Add( FErrorMail );
               MessageText.Add( 'Notificaci�n Del M�dulo De Reportes V�a E-mail' );
               MessageText.AddStrings( FErroresMail );

               if Validate( sErrorMsg ) then
               begin
                    if NOT SendEmail( sErrorMsg ) then
                       LogError( 'Error al Tratar de Enviar los Correos ' + CR_LF + sErrorMsg );
               end
               else
                   LogError( 'Se encontr� un Error ' + CR_LF + sErrorMsg );
          end;
     end;
     {$endif}
end;

procedure TdmReportes.DoOnGetDatosImpresion;
 {var sDir : string;}
var oDatosImpresion : TDatosImpresion;
begin
     oDatosImpresion := DatosImpresion;
     with oDatosImpresion do
     begin
          FBorraArchivo := Tipo in [tfImpresora];
          if FBorraArchivo then
          begin
               {.$ifdef TRESSEMAIL}
               {if FExportaPDF then
                  Tipo := tfPDF
               else}
               {.$endif}
               Tipo := tfHtml;

               {sDir := ExtractFilePath( Exportacion );
               if StrVacio( sDir ) then
                  Exportacion := VerificaDir( ExtractFilePath( Application.ExeName ) ) + Exportacion;
               if ( sDir = zReportTools.DirPlantilla ) then
                  Exportacion := StrTransAll( Exportacion, ExtractFilePath( Exportacion ), ExtractFilePath( Application.ExeName ) ) ;}
               Exportacion := VerificaDir( ExtractFilePath( Application.ExeName ) ) + 'Reporte.' + ObtieneElemento( lfExtFormato, Ord( Tipo ) ) ;

          end;
          DatosImpresion := oDatosImpresion;
     end;
end;

function TdmReportes.GetExtensionDef : string;
 var eTipo : eExtFormato;
begin
     {.$ifdef TRESSEMAIL}
     {if FExportaPDF then
        eTipo := tfPDF
     else}
     {.$endif}
     eTipo := tfHtml;
     Result := ObtieneElemento( lfExtFormato, Ord( eTipo ) );
end;

procedure TdmReportes.BorraArchivos;
 var
    iPage : integer;
    sExt, sNombre, sArchivo : string;
begin
     if FileExists( DatosImpresion.Exportacion ) then
     begin
          DeleteFile( DatosImpresion.Exportacion );
     end
     else
     begin
          iPage := 1;
          with DatosImpresion do
          begin
               sExt := ExtractFileExt( Exportacion );
               sNombre := Copy( Exportacion, 1, Pos( sExt, Exportacion )-1 );
          end;

          sArchivo := sNombre + IntToStr( iPage ) + sExt;

          while FileExists( sArchivo ) do
          begin
               DeleteFile( sArchivo );
               Inc( iPage );
               sArchivo := sNombre + IntToStr( iPage ) + sExt;
          end;
     end;
end;

function TdmReportes.GetSobreTotales: Boolean;
 var i : integer;
begin
     Result := FALSE;
     for i := 0 to Campos.Count - 1 do
     begin
          with TCampoListado( Campos.Objects[ i ] ) do
          begin
               Result := Result OR (Operacion = ocSobreTotales);
          end;
     end;
end;

procedure TdmReportes.GeneraQRReporte;
 var eTipo : eTipoReporte;
begin
     with cdsReporte do
     begin
          eTipo := eTipoReporte( FieldByName('RE_TIPO').AsInteger );
          if ( eTipo = trForma ) then
          begin
               QRReporteListado.GeneraForma( FieldByName('RE_NOMBRE').AsString,
                                             FALSE,
                                             cdsResultados )
          end
          else if ( eTipo = trListado ) then
          begin
               QRReporteListado.GeneraListado( FieldByName('RE_NOMBRE').AsString,
                                               FALSE,
                                               zStrToBool( FieldByName('RE_VERTICA').AsString ),
                                               zStrToBool( FieldByName('RE_SOLOT').AsString ),
                                               GetSobreTotales,
                                               cdsResultados,
                                               Campos,
                                               Grupos );
          end;
     end;
end;

procedure TdmReportes.ModificaListaCampos( Lista : TStrings );
 var i : integer;
begin
     for i:=0 to Lista.Count -1 do
        with TCampoOpciones(Lista.Objects[i]) do
             if PosAgente >= 0 then
             begin
                  SQLColumna := FSQLAgente.GetColumna(PosAgente);
                  with FSQLAgente.GetColumna(PosAgente) do
                  begin
                       TipoImp := TipoFormula;
                       OpImp := Totalizacion;
                  end;
             end;
end;

procedure TdmReportes.ModificaListas;
 var i : integer;
begin
     ModificaListaCampos( Campos );
     for i:= 0 to Grupos.Count - 1 do
     begin
          with TGrupoOpciones( Grupos.Objects[i]) do
          begin
               ModificaListaCampos( ListaEncabezado );
               ModificaListaCampos( ListaPie );
          end;
     end;
end;

procedure TdmReportes.GeneraOutPut;
begin
     ModificaListas;
     BorraArchivos;

     with cdsResultados do
     begin
          if Active AND Not IsEmpty then
          begin
               First;
               {$ifdef CAROLINA}
               SaveToFile('d:\ResEmail.cds');
               {$endif}

               ZFuncsCliente.RegistraFuncionesCliente;
               //Asignacion de Parametros del Reporte,
               //para poder evaluar la funcion PARAM() en el Cliente;
               ZFuncsCliente.ParametrosReporte := FSQLAgente.Parametros;

               case DatosImpresion.Tipo of
                    tfMailMerge,tfMailMergeXLS:;
                    tfASCIIFijo, tfASCIIDel, tfCSV : GeneraArchivoAscii
                    else GeneraQRReporte;
               end;
          end;
     end;
end;

function TdmReportes.GetSoloTotales: Boolean;
begin
     Result := zStrToBool( cdsReporte.FieldByName('RE_SOLOT').AsString );
end;

function TdmReportes.GeneraArchivoAscii: Boolean;
 var oReporteAscii : TReporteAscii;
begin
     oReporteAscii := TReporteAscii.Create;
     try
        Result := oReporteAscii.GeneraAscii( cdsResultados,
                                             DatosImpresion,
                                             Campos,
                                             Grupos,
                                             GetSoloTotales,
                                             FALSE );
     finally
            FreeAndNil( oReporteAscii );
     end;
end;

function TdmReportes.ConectaHost: Boolean;
begin
     with dmEmailService do
     begin
          NewEMail;
          MailServer := FHost;
          if FPort > 0 then
             Port := FPort;

          AuthMethod := FAuthMethod;
             
          User := FUserId;
          Password := FPasword;
          FromAddress := FFromAddress;
          FromName := FFromName;
          Result := TRUE;
     end;
end;

function TdmReportes.GetSuscripciones: Boolean;
var
   oUsuarios: Olevariant;
begin
     cdsSuscripcion.Data := ServerReportes.GetSuscripcion( dmCliente.Empresa, FFrecuencia, FUsuarios, FReportes, oUsuarios );
     cdsUsuarios.Data := oUsuarios;

     if StrVacio( FReportes ) then
     begin
          if cdsSuscripcion.IsEmpty then
             LogError( 'No Existe Ninguna Suscripci�n', TRUE );

          if cdsUsuarios.IsEmpty then
             LogError( 'No Existe Ning�n Usuario con Direcci�n de Correos', TRUE );
     end;

     FiltraUsuariosInactivos;

     Result := not ( cdsSuscripcion.IsEmpty OR cdsUsuarios.IsEmpty );
end;

function TdmReportes.PreparaPlantilla( var sError : WideString ) : Boolean;
 var
    lHayImagenes : Boolean;
    sDirectorio : string;
begin
     ZQRReporteListado.PreparaReporte;
     //DatosImpresion := GetDatosImpresion;

     sDirectorio := ExtractFilePath( DatosImpresion.Archivo );
     Result := DirectoryExists( sDirectorio );
     if NOT Result then
        sError := Format( 'El Directorio de Plantillas " %s " No Existe', [ sDirectorio ] );

     sDirectorio := ExtractFilePath( DatosImpresion.Exportacion );
     Result := DirectoryExists( sDirectorio );
     if Not Result then
        sError := Format( 'El Directorio del Archivo " %s " No Existe', [ sDirectorio ] );

     if Result then
     begin
          DoOnGetDatosImpresion;

          with QRReporteListado do
          begin
               Init( FSQLAgente,
                     DatosImpresion,
                     Parametros.Count,
                     lHayImagenes );

               AddNombreImagenes( FNombresImagenes );
          end;
     end;
end;

procedure TdmReportes.DesPreparaPlantilla;
begin
     with QRReporteListado do
     begin
          FPageCount := PageCount;
          FUnSoloHTML := UnSoloHTML;
     end;

     ZQrReporteListado.DesPreparaReporte;
end;



procedure TdmReportes.cdsSuscripReportesLookupSearch( Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;var sKey, sDescription: String);
begin
     inherited;
     with Sender do
     begin
          if FormaLookupReporte_DevEx = NIL then
             FormaLookupReporte_DevEx := TFormaLookupReporte_DevEx.Create( self );

          with FormaLookupReporte_devEx do
          begin
               lOK := ShowModal = mrOk;
               if lOk then
               begin
                    sKey := IntToStr( NumReporte );
                    sDescription := NombreReporte;
               end;
          end;
     end;
end;



procedure TdmReportes.cdsUsuariosLookupAlAdquirirDatos(Sender: TObject);
var
   iLongitud: Integer;
   lBloqueoSistema: WordBool;
begin
     inherited;
     cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBloqueoSistema );
end;

function TdmReportes.GetClasificaciones(oClasificaciones: TStrings): string;
var
   i: integer;
begin
     Result := VACIO;
     if not dmCliente.ModoTress then
     begin
          with oClasificaciones do                 
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Result := ConcatString( Result,
                                            IntToStr( Ord( eClasifiReporte( oclasificaciones.objects[ i ] ) ) ), ',' );
               end;
          end
     end;
end;

procedure TdmReportes.cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
begin
     with cdsSuscripReportes do
     begin
          IndexFieldNames:='';
          IndexName:='';
          Data := ServerReportes.GetReportes( dmCliente.Empresa,
                                              Ord( FClasifActivo ),
                                              Ord( crFavoritos ),
                                              Ord( crSuscripciones ), 
                                              GetClasificaciones(FClasificaciones),
                                              dmDiccionario.VerConfidencial);
          IndexFieldNames:='RE_CODIGO';
     end;
end;

procedure TdmReportes.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
{ var
    Pendientecaro: integer;}
begin

     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              {$ifdef RDD}
              Text := VACIO;
              {$else}
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
              {$endif}

     end
     else
         Text:= Sender.AsString;
end;

procedure TdmReportes.cdsSuscripReportesAlCrearCampos(Sender: TObject);
var
   oCampo: TField;
begin
     inherited;
     with cdsSuscripReportes do
     begin
          oCampo := FindField( 'RE_ENTIDAD' );
          if ( oCampo <> nil )  then
          begin
               with oCampo do
               begin
                    OnGetText := ObtieneEntidadGetText;
                    Alignment := taLeftJustify;
               end;
          end;
          ListaFija('RE_TIPO', lfTipoReporte);
     end;
end;

procedure TdmReportes.cdsUsuariosLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     inherited;
     lHasRights := FALSE;
end;

function TdmReportes.SistemaBloqueado: Boolean;
begin
     Result:= zstrToBool( ServerLogin.GetClave( ZetaServerTools.CLAVE_SISTEMA_BLOQUEADO ) );
end;


function TdmReportes.GeneraReportesEmpleados( const lHTMLOnly: Boolean ): Boolean;
var
   i, iReporte,iPosAgente : integer;
   oCursor : TCursor;

const
     K_CONST_EMPLEADO = 'EMPLEADO';
     K_CONST_EMAIL = 'EMAIL';
     K_CONST_NUMPER = 'NUM_PERIODO';
     K_CONST_TIPOPER = 'TIPO_PERIODO';
     K_CONST_YEARPER = 'ANIO_PERIODO';
     K_COLUMNA = 'COLUMNA';

   function GetFieldName:string;
   begin
        with FSQLAgente.GetColumna(iPosAgente) do
        begin
             if Length(Alias) > 0 then
                  Result := Alias
             else
                 Result := K_COLUMNA + IntToStr(iPosAgente);
        end;
   end;

   function GetValorEnteroColumna:Integer;
   begin
        if FSQLAgente.GetColumna(iPosAgente).Esconstante then
           Result := StrToInt( FSQLAgente.GetColumna(iPosAgente).ValorConstante)
        else
        begin
             Result := cdsListaEmpleados.FieldByname( GetFieldName ).AsInteger;
        end;
   end;

   function AsignaEnteroOpcional( const sCampo: String ): Integer;
   begin
        with cdsListaEmpleados do
        begin
             if Assigned( FindField( sCampo ) ) then
                Result := FieldByname( sCampo ).AsInteger
             else
             		 Result := 0;
        end;
   end;

   procedure ActualizaValoresActivos;
   var
      oDatosPeriodo:TDatosPeriodo;
   begin
        with cdsListaEmpleados do
        begin
             try
                FUsuariosPlain.Clear;
                FUsuariosPlain.Add( FieldByname( K_CONST_EMAIL ).AsString);
                dmCliente.Empleado := FieldByname( K_CONST_EMPLEADO ).AsInteger;
				oDatosPeriodo := dmCliente.PeriodoActivo ;
                with oDatosPeriodo do
                begin
                     Numero := AsignaEnteroOpcional( K_CONST_NUMPER );
                     Year := AsignaEnteroOpcional( K_CONST_YEARPER );
                     Tipo :=  Ord(eTipoPeriodoReportes( AsignaEnteroOpcional( K_CONST_TIPOPER ) ));

                     if Assigned( FindField( K_CONST_NUMPER ) ) then
                        Numero := FieldByname( K_CONST_NUMPER ).AsInteger;
                     if Assigned( FindField( K_CONST_YEARPER ) ) then
                        Year := FieldByname( K_CONST_YEARPER ).AsInteger;
                     if Assigned( FindField( K_CONST_TIPOPER ) ) then
                        Tipo := Ord(eTipoPeriodoReportes( FieldByname( K_CONST_TIPOPER ).AsInteger ));
                end;
                dmCliente.PeriodoActivo := oDatosPeriodo
             except
                  on Error:Exception do
                  begin
                       LogError(Format(' Al leer valores activos y par�metros , Mensaje:',[Error.Message] ) );
                  end;
            end;
        end;
   end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Result := ConectaGlobal;
        if Result then
        begin
             GeneraListaEmpleados := True;
             if GeneraUnReporte( FReporteEmpleados, FALSE )then
             begin
                  with cdsListaEmpleados do
                  begin
                       Data := cdsResultados.Data;
                       dmCliente.GeneraListaEmpleados := True;  			//Se prende nueva funcionalidad
                       while not Eof do
                       begin
                            ActualizaValoresActivos;   					//Se cambian valores activos por cada empleado
                            for i:= 0 to FListaReportes.Count - 1 do
                            begin
                                 iReporte := StrToIntDef( FListaReportes[ i ], 0 );
                                 if iReporte > 0 then
                                 begin
                                      GeneraListaEmpleados := False;
                                      Log('=================================');
                                      Log(Format('Generando Reporte:%d ,Empleado:%d',[iReporte,dmCliente.Empleado]));
                                      Result := GeneraUnReporte( iReporte, lHTMLOnly );
                                      try
                                         if Result then
                                         begin
                                              If ((FUsuariosPlain.Count > 0) and (FUsuariosPlain.Strings[0] <> VACIO)) then
                                              begin
                                                   SendEmail( emtTexto, Result, FUsuariosPlain )
                                              end
                                              else
                                              begin
                                                   LogError(Format(' No se configur� el correo electr�nico del empleado #: %d',[dmCliente.Empleado] ) );
                                              end;
                                         end;
                                      except
                                            on Error:Exception do
                                            begin
                                                 LogError(Format(' No se logro enviar correo a: %s Mensaje:%s',[FUsuariosPlain[0],Error.Message] ) );
                                            end;
                                      end;
                                      Log('=================================');
                                 end;
                            end;
                            Next;    //Siguiente empleado de la lista
                       end;
                  end;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
            dmCliente.GeneraListaEmpleados := False;
            //cdsListaEmpleados.Free;
     end;
end;

procedure TdmReportes.DoBeforePreparaAgente( var oParams:Olevariant );
const
     P_TITULO = 1;
     P_FORMULA = 2;
     P_TIPO = 3;
     K_PARAM = 'PARAM';
var
   i:Integer;
   sField:string;
   oParam:OleVariant;
   eTipo:eTipoGlobal;

   procedure SetParamValue( const iParametro: Integer;const sValor:string );
   begin
        if ( iParametro > 0 ) and ( iParametro <= K_MAX_PARAM ) then
        begin
             oParam := VarArrayCreate( [ P_TITULO, P_TIPO ], varVariant );
             oParam := oParams[ iParametro ];
             eTipo := oParam[P_TIPO];
             try
                case eTipo of
                     tgBooleano: oParam[ P_FORMULA ] := ZetaCommonTools.zStrToBool( Trim(sValor) );
                     tgFloat: oParam[ P_FORMULA ] := StrToFloat( Trim(sValor)  );
                     tgNumero: oParam[ P_FORMULA ] := StrToFloat( Trim(sValor)  );
                     tgFecha:
                                             begin
                                                              oParam[ P_FORMULA ] := cdsListaEmpleados.FieldByName(sField).AsDateTime;
                             end;
                     tgTexto: oParam[ P_FORMULA ] := Trim(sValor) ;
                end;
             except
                   on Error: Exception do
                   begin
                        Error.Message := Format( 'Error En Valor Del Par�metro %s # %d: %s', [ ZetaCommonLists.ObtieneElemento( lfTipoGlobal, Ord( eTipo ) ), i+1, Error.Message ] );
                        LogError(Error.Message);
                   end;
             end;
                           oParams[ iParametro ] := oParam;
        end;
   end;

begin
     for i := 0 to Parametros.Count -1  do
     begin
          sField := K_PARAM+IntToStr(i+1);
          if cdsListaEmpleados.FindField(sField) <> nil then
             SetParamValue(i+1,cdsListaEmpleados.FieldByName(sField).AsString);
     end;
end;


end.
