inherited EditIMSSDatosTot_DevEx: TEditIMSSDatosTot_DevEx
  Left = 198
  Top = 257
  Caption = 'Totales de IMSS'
  ClientHeight = 188
  ClientWidth = 406
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 152
    Width = 406
    inherited OK_DevEx: TcxButton
      Left = 240
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 320
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 406
    inherited ValorActivo2: TPanel
      Width = 80
      inherited textoValorActivo2: TLabel
        Width = 74
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 4
  end
  object GroupBox6: TGroupBox [3]
    Left = 0
    Top = 50
    Width = 406
    Height = 102
    Align = alClient
    Caption = 'Decreto 20%'
    TabOrder = 3
    object lblParcialidad: TLabel
      Left = 110
      Top = 68
      Width = 93
      Height = 13
      Caption = 'Importe Parcialidad:'
    end
    object LS_DES_20: TDBCheckBox
      Left = 105
      Top = 24
      Width = 114
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Aplic'#243' Decreto 20%:'
      DataField = 'LS_DES_20'
      DataSource = DataSource
      TabOrder = 0
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object LS_FINAN: TDBCheckBox
      Left = 105
      Top = 44
      Width = 114
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Con Financiamiento:'
      DataField = 'LS_FINAN'
      DataSource = DataSource
      TabOrder = 1
      ValueChecked = 'S'
      ValueUnchecked = 'N'
    end
    object LS_PARCIAL: TZetaDBNumero
      Left = 206
      Top = 64
      Width = 92
      Height = 21
      Mascara = mnHoras
      TabOrder = 2
      Text = '0.00'
      DataField = 'LS_PARCIAL'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 268
    Top = 73
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
