inherited IMSSDatosBimestral: TIMSSDatosBimestral
  Left = 125
  Top = 131
  Caption = 'IMSS: Datos Bimestrales por Empleado'
  ClientHeight = 266
  ClientWidth = 597
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 597
    inherited Slider: TSplitter
      Left = 313
    end
    inherited ValorActivo1: TPanel
      Width = 297
    end
    inherited ValorActivo2: TPanel
      Left = 316
      Width = 281
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 597
    Height = 112
    Align = alTop
    TabOrder = 1
    object Label8: TLabel
      Left = 239
      Top = 91
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D�as Cotizados:'
    end
    object LE_DIAS_BM: TZetaDBTextBox
      Left = 319
      Top = 89
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LE_DIAS_BM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LE_DIAS_BM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 4
      Width = 209
      Height = 100
      Caption = ' Cuenta Individual '
      TabOrder = 0
      object Label2: TLabel
        Left = 86
        Top = 18
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retiro:'
      end
      object LE_RETIRO: TZetaDBTextBox
        Left = 121
        Top = 16
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_RETIRO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_RETIRO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label1: TLabel
        Left = 34
        Top = 37
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cesant�a y Vejez:'
      end
      object LE_CES_VEJ: TZetaDBTextBox
        Left = 121
        Top = 35
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_CES_VEJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_CES_VEJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 65
        Top = 56
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total RCV:'
      end
      object LE_TOT_RET: TZetaDBTextBox
        Left = 121
        Top = 54
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_TOT_RET'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_TOT_RET'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label3: TLabel
        Left = 13
        Top = 75
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aportaci'#243'n Voluntaria:'
        Visible = False
      end
      object LE_APO_VOL: TZetaDBTextBox
        Left = 121
        Top = 73
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_APO_VOL'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_APO_VOL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object GroupBox2: TGroupBox
      Left = 218
      Top = 4
      Width = 192
      Height = 80
      Caption = ' Cuenta INFONAVIT '
      TabOrder = 1
      object Label6: TLabel
        Left = 55
        Top = 18
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Patronal:'
      end
      object LE_INF_PAT: TZetaDBTextBox
        Left = 101
        Top = 16
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_INF_PAT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_INF_PAT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 34
        Top = 37
        Width = 63
        Height = 13
        Alignment = taRightJustify
        Caption = 'Amortizaci�n:'
      end
      object LE_INF_AMO: TZetaDBTextBox
        Left = 101
        Top = 35
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_INF_AMO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_INF_AMO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 10
        Top = 56
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Total INFONAVIT:'
      end
      object LE_TOT_INF: TZetaDBTextBox
        Left = 101
        Top = 54
        Width = 80
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'LE_TOT_INF'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LE_TOT_INF'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object ZetaDBGrid2: TZetaDBGrid [2]
    Left = 0
    Top = 131
    Width = 597
    Height = 135
    Align = alClient
    DataSource = dsLiq_Mov
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'LM_FECHA'
        Title.Caption = 'Fecha'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_CLAVE'
        Title.Caption = 'Clave'
        Width = 35
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_DIAS'
        Title.Alignment = taRightJustify
        Title.Caption = 'D�as'
        Width = 29
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_INCAPAC'
        Title.Alignment = taRightJustify
        Title.Caption = 'INC'
        Width = 24
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_AUSENCI'
        Title.Alignment = taRightJustify
        Title.Caption = 'AUS'
        Width = 27
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_BASE'
        Title.Alignment = taRightJustify
        Title.Caption = 'Base'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_RETIRO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Retiro'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_CES_VEJ'
        Title.Alignment = taRightJustify
        Title.Caption = 'Cesant�a y Vejez'
        Width = 85
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_INF_PAT'
        Title.Alignment = taRightJustify
        Title.Caption = 'INFONAVIT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LM_INF_AMO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Amortizaci�n'
        Width = 86
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 464
    Top = 88
  end
  object dsLiq_Mov: TDataSource
    Left = 536
    Top = 80
  end
end
