unit FIMSSDatosBimestral;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, StdCtrls, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox;

type
  TIMSSDatosBimestral = class(TBaseConsulta)
    dsLiq_Mov: TDataSource;
    ZetaDBGrid2: TZetaDBGrid;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    LE_RETIRO: TZetaDBTextBox;
    Label1: TLabel;
    LE_CES_VEJ: TZetaDBTextBox;
    Label4: TLabel;
    LE_TOT_RET: TZetaDBTextBox;
    Label8: TLabel;
    LE_DIAS_BM: TZetaDBTextBox;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    LE_INF_PAT: TZetaDBTextBox;
    Label5: TLabel;
    LE_INF_AMO: TZetaDBTextBox;
    Label7: TLabel;
    LE_TOT_INF: TZetaDBTextBox;
    Label3: TLabel;
    LE_APO_VOL: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  IMSSDatosBimestral: TIMSSDatosBimestral;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

{ TIMSSDatosBimestral }

procedure TIMSSDatosBimestral.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40413_IMSS_bimestral_empleado;
end;

procedure TIMSSDatosBimestral.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosBim.Conectar;
          cdsMovBim.Conectar;
          DataSource.DataSet:= cdsIMSSDatosBim;
          dsLiq_Mov.DataSet:= cdsMovBim;
     end;
end;

procedure TIMSSDatosBimestral.Refresh;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosBim.Refrescar;
          cdsMovBim.Refrescar;
     end;
end;

function TIMSSDatosBimestral.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar A Datos Bimestrales IMSS';
end;

function TIMSSDatosBimestral.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Datos Bimestrales IMSS';
end;

function TIMSSDatosBimestral.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar En Datos Bimestrales IMSS';
end;

end.
