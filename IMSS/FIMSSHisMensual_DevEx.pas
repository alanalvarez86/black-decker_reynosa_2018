unit FIMSSHisMensual_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, Grids, DBGrids, StdCtrls, ExtCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  ZBaseGridLecturaImss_DevEx, ComCtrls, ZetaStateComboBox;

type
  TIMSSHisMensual_DevEx = class(TBaseGridLecturaImss_DevEx)
    dsLiq_Emp: TDataSource;
    Panel1: TPanel;
    Label6: TLabel;
    LS_TOT_IMS: TZetaDBTextBox;
    LS_NUM_TRA: TZetaDBTextBox;
    Label7: TLabel;
    Label8: TLabel;
    LS_DIAS_CO: TZetaDBTextBox;
    Label1: TLabel;
    LS_TOT_MES: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  end;

var
  IMSSHisMensual_DevEx: TIMSSHisMensual_DevEx;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

{ TIMSSHisMensual }

procedure TIMSSHisMensual_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40421_Hist_IMSS_mensual_empleado;
end;

procedure TIMSSHisMensual_DevEx.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSHisMen.Conectar;
          cdsMovHisMen.Conectar;
          DataSource.DataSet:= cdsIMSSHisMen;
          dsLiq_Emp.DataSet := cdsMovHisMen;
     end;
end;

procedure TIMSSHisMensual_DevEx.Refresh;
begin
     with dmIMSS do
     begin
          cdsIMSSHisMen.Refrescar;
          cdsMovHisMen.Refrescar;
     end;
end;

function TIMSSHisMensual_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar Al Historial De Pagos IMSS';
end;

function TIMSSHisMensual_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En El Historial De Pagos IMSS';
end;

function TIMSSHisMensual_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Modificar El Historial De Pagos IMSS';
end;

procedure TIMSSHisMensual_DevEx.FormShow(Sender: TObject);
begin
CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
  inherited;

end;

end.
