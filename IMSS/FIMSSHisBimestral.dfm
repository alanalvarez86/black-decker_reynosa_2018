inherited IMSSHisBimestral: TIMSSHisBimestral
  Left = 208
  Top = 139
  Caption = 'IMSS: Historia Bimestral por Empleado'
  ClientWidth = 581
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 581
    inherited Slider: TSplitter
      Left = 362
    end
    inherited ValorActivo1: TPanel
      Width = 346
    end
    inherited ValorActivo2: TPanel
      Left = 365
      Width = 216
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 581
    Height = 82
    Align = alTop
    TabOrder = 1
    object LS_TOT_RET: TZetaDBTextBox
      Left = 345
      Top = 12
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_RET'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_RET'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LS_TOT_INF: TZetaDBTextBox
      Left = 345
      Top = 31
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_INF'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_INF'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label3: TLabel
      Left = 255
      Top = 33
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total INFONAVIT:'
    end
    object Label1: TLabel
      Left = 290
      Top = 14
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total RCV:'
    end
    object LS_DIAS_BM: TZetaDBTextBox
      Left = 150
      Top = 50
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_DIAS_BM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_DIAS_BM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label4: TLabel
      Left = 72
      Top = 52
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D�as Cotizados:'
    end
    object Label2: TLabel
      Left = 88
      Top = 33
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Asegurados:'
    end
    object LS_NUM_BIM: TZetaDBTextBox
      Left = 150
      Top = 31
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_NUM_BIM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_NUM_BIM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LS_INF_NUM: TZetaDBTextBox
      Left = 150
      Top = 12
      Width = 80
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_INF_NUM'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_INF_NUM'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label5: TLabel
      Left = 88
      Top = 14
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Acreditados:'
    end
  end
  object ZetaDBGrid2: TZetaDBGrid [2]
    Left = 0
    Top = 101
    Width = 581
    Height = 172
    Align = alClient
    DataSource = dsLiq_Emp
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N�mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PrettyName'
        Title.Caption = 'Nombre'
        Width = 297
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_SEGSOC'
        Title.Caption = 'NSS'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LE_TOT_RET'
        Title.Alignment = taRightJustify
        Title.Caption = 'Total RCV'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LE_TOT_INF'
        Title.Alignment = taRightJustify
        Title.Caption = 'Total INFONAVIT'
        Width = 92
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LE_DIAS_BM'
        Title.Alignment = taRightJustify
        Title.Caption = 'D�as Cotizados'
        Width = 81
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 480
  end
  object dsLiq_Emp: TDataSource
    Left = 512
    Top = 56
  end
end
