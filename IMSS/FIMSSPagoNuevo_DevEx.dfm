inherited IMSSPagoNuevo_DevEx: TIMSSPagoNuevo_DevEx
  ActiveControl = LS_PATRON
  Caption = 'Pago de IMSS Nuevo'
  ClientHeight = 206
  ClientWidth = 451
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 12
    Top = 64
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = '&Registro Patronal:'
    FocusControl = LS_PATRON
  end
  object Label2: TLabel [1]
    Left = 74
    Top = 88
    Width = 22
    Height = 13
    Alignment = taRightJustify
    Caption = '&A'#241'o:'
    FocusControl = LS_YEAR
  end
  object Label5: TLabel [2]
    Left = 73
    Top = 111
    Width = 23
    Height = 13
    Alignment = taRightJustify
    Caption = '&Mes:'
    FocusControl = LS_MONTH
  end
  object Label6: TLabel [3]
    Left = 72
    Top = 135
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tipo:'
    FocusControl = LS_TIPO
  end
  inherited PanelBotones: TPanel
    Top = 170
    Width = 451
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 287
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 366
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 451
    TabOrder = 5
    inherited ValorActivo2: TPanel
      Width = 125
      inherited textoValorActivo2: TLabel
        Width = 119
      end
    end
  end
  object LS_PATRON: TZetaDBKeyLookup_DevEx [7]
    Left = 101
    Top = 60
    Width = 340
    Height = 21
    LookupDataset = dmCatalogos.cdsRPatron
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 0
    TabStop = True
    WidthLlave = 60
    DataField = 'LS_PATRON'
    DataSource = DataSource
  end
  object LS_MONTH: TZetaDBKeyCombo [8]
    Left = 101
    Top = 107
    Width = 100
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 2
    ListaFija = lfMeses
    ListaVariable = lvPuesto
    Offset = 1
    Opcional = False
    EsconderVacios = False
    DataField = 'LS_MONTH'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object LS_YEAR: TZetaDBKeyCombo [9]
    Left = 101
    Top = 84
    Width = 100
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 1
    ListaFija = lfYears
    ListaVariable = lvPuesto
    Offset = 1994
    Opcional = False
    EsconderVacios = False
    DataField = 'LS_YEAR'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object LS_TIPO: TZetaDBKeyCombo [10]
    Left = 101
    Top = 131
    Width = 100
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 3
    ListaFija = lfTipoLiqIMSS
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'LS_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 236
    Top = 89
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
