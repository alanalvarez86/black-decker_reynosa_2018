unit FCreditosNoRegistradosINFONAVIT;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  Vcl.StdCtrls, Vcl.ExtCtrls, cxCalendar;

type
  TCreditosNoRegistradosINFONAVIT = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    STATUS_EMP: TcxGridDBColumn;
    NO_CREDITO: TcxGridDBColumn;
    CB_INF_INI: TcxGridDBColumn;
    CB_FEC_ING: TcxGridDBColumn;
    CB_FEC_BAJ: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;


const
     K_NOMBRE_FORMA = 'cr�ditos en TRESS y no en INFONAVIT';

var
  CreditosNoRegistradosINFONAVIT: TCreditosNoRegistradosINFONAVIT;

implementation

{$R *.dfm}


uses
     FTressShell, ZetaCommonClasses;

procedure TCreditosNoRegistradosINFONAVIT.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsCreditosInactivosImss.DataSet;
end;


procedure TCreditosNoRegistradosINFONAVIT.FormCreate(Sender: TObject);
begin
    inherited;
    ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
    if ( TressShell.ConciliacionTerminada ) then
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n cr�dito en TRESS y que no est� en INFONAVIT'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

    ZetaDBGridDBTableView.OptionsData.Editing := False;
    HelpContext := H_CONCIL_CRE_REG_TRESS_NO_INF;
end;

procedure TCreditosNoRegistradosINFONAVIT.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     ZetaDBGridDBTableView.ApplyBestFit();
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TCreditosNoRegistradosINFONAVIT.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TCreditosNoRegistradosINFONAVIT.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TCreditosNoRegistradosINFONAVIT.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;


function TCreditosNoRegistradosINFONAVIT.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TCreditosNoRegistradosINFONAVIT.Refresh;
begin

end;

end.
