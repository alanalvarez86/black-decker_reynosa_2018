unit FCreditosNoRegistradosTRESS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxCalendar;

type
  TCreditosNoRegistradosTRESS = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    STATUS_EMP: TcxGridDBColumn;
    NO_CREDITO: TcxGridDBColumn;
    TDESCUENTO: TcxGridDBColumn;
    VDESCUENTO: TcxGridDBColumn;
    CB_INF_INI: TcxGridDBColumn;
    INF_AMO: TcxGridDBColumn;
    OBSERVA: TcxGridDBColumn;
    CB_FEC_ING: TcxGridDBColumn;
    CB_FEC_BAJ: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;

const
     K_NOMBRE_FORMA = 'cr�ditos no registrados en TRESS';

var
  CreditosNoRegistradosTRESS: TCreditosNoRegistradosTRESS;

implementation

{$R *.dfm}


uses
     FTressShell, ZetaCommonClasses;


procedure TCreditosNoRegistradosTRESS.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsCredNoActivosTress.DataSet;
end;


procedure TCreditosNoRegistradosTRESS.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
          ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n cr�dito no registrados en TRESS'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

     ZetaDBGridDBTableView.OptionsData.Editing := False;
     HelpContext := H_CONCIL_CRE_NO_TRESS_REG_INF;
end;

procedure TCreditosNoRegistradosTRESS.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount );
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     ZetaDBGridDBTableView.ApplyBestFit();
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TCreditosNoRegistradosTRESS.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TCreditosNoRegistradosTRESS.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TCreditosNoRegistradosTRESS.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TCreditosNoRegistradosTRESS.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TCreditosNoRegistradosTRESS.Refresh;
begin

end;

end.
