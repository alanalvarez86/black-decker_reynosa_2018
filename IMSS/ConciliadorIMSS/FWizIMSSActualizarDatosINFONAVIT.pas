unit FWizIMSSActualizarDatosINFONAVIT;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
    cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
    dxSkinsCore, TressMorado2013, cxContainer, cxEdit, ZetaNumero, Vcl.StdCtrls,
    ZetaKeyCombo, cxTextEdit, cxMaskEdit, cxDropDownEdit, ZetaCXStateComboBox,
    cxCheckBox, Vcl.Mask, ZetaFecha, ZetaCXWizard, dxGDIPlusClasses, cxImage,
    cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, cxSpinEdit,
    ZcxBaseWizard, ZetaClientDataSet, ZcxWizardBasico, DBClient;

type
  TWizIMSSActualizarDatosINFONAVIT = class(TcxBaseWizard)
    gbFiltrarFecha: TcxGroupBox;
    lblFechaFiltro: TcxLabel;
    zFechaFiltro: TZetaFecha;
    lFiltrarPorInicio: TcxCheckBox;
    cbDiferenciasInfonavit: TcxStateComboBox;
    lblDiferencias: TcxLabel;
    lblBimestreInfo: TcxLabel;
    lblYearInfo: TcxLabel;
    YearInfo: TcxSpinEdit;
    cbBimestre: TcxStateComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure cbDiferenciasInfonavitChange( Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);

  private
    { Private declarations }
    DataSet: TClientDataSet;
    procedure HabilitaContFiltrosInfo( const lSetVisibles: Boolean);

  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }

  end;

var
   WizIMSSActualizarDatosINFONAVIT: TWizIMSSActualizarDatosINFONAVIT;
   function ShowWizard( FiltrarDiferenciasInfonavit: Integer; FiltrarPorInicio: Boolean; FechaFiltro: TDate; aDataSet: TClientDataSet): Boolean;overload;
   function ShowWizard( aDataSet: TClientDataSet): Boolean;overload;
implementation

{$R *.dfm}

uses
    ZetaCommonTools, ZetaRegistryCliente, ZetaDialogo,
    DCliente, DConciliador, ZetaCommonClasses, ZetaCommonLists,
    ZBasicoSelectGrid_DevEx, FIMSSVerificaDiferenciasDatosINFONAVITGridSelect,
    FDiferenciasDatosINFONAVIT;

procedure TWizIMSSActualizarDatosINFONAVIT.FormCreate(Sender: TObject);
begin
     inherited;
     //Controles de la vista Infonavit
     //YearInfo.Valor:= ZetaCommonTools.TheYear( Date );
     YearInfo.Value := ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear);
     {SUA 3.2.5: INFONAVIT estar� revisando los cr�ditos que se otorgaron a partir del 01/Feb/2008
     y si el empleado tuvo cambios en su salario base, el factor de pago se ajustar� conforme a su nuevo salario}
     zFechaFiltro.Valor:= EncodeDate(2008,02,1);
     cbBimestre.Indice := NumeroBimestre( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes) ) + cbBimestre.Offset;
     if ( eBimestres( NumeroBimestre( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes) ) - 1 ) = bimNovDic ) then
     begin
          cbBimestre.Indice:= 1;
          YearInfo.Value :=  YearInfo.Value + 1;
     end;

     HelpContext := H_CONCIL_WIZ_ACTUALIZAR_DATOS_INFONAVIT;

     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;

end;

procedure TWizIMSSActualizarDatosINFONAVIT.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se actualizar� la informaci�n del expediente de INFONAVIT de los empleados indicados en los par�metros.'
end;

procedure TWizIMSSActualizarDatosINFONAVIT.WizardAlCancelar(Sender: TObject;
  var lOk: Boolean);
begin
     inherited;
     if ( lok ) then
        TClientDataSet(DataSet).CancelUpdates;
end;

procedure TWizIMSSActualizarDatosINFONAVIT.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);

begin
     inherited;
     with Wizard do
     begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
            begin
                  if ( YearInfo.Value = 0 ) then
                  begin
                       CanMove := Error( 'A�o Inv�lido', YearInfo);
                  end
                  else if ( cbBimestre.Indice =  ( cbBimestre.Offset -1 ) )  then
                     CanMove := Error( 'El bimestre no puede quedar vac�o', cbBimestre)
                  else if ( cbDiferenciasInfonavit.Indice =  (cbDiferenciasInfonavit.Offset - 1 ) )  then
                     CanMove := Error( 'El filtro de diferecias no puede quedar vac�o', cbDiferenciasInfonavit)
                  else
                  //si no hay registros a procesar despues de aplicar los filtros en la seleccion de par�metros, no avanzar
                  if ( DataSet.IsEmpty ) then
                  begin
                       ZetaDialogo.ZError( 'Error al actualizar datos Infonavit', 'No hay registros en la lista de empleados', 0 );
                       CanMove := False;
                  end
                  else
                  begin
                       CargaParametros;
                       //si hay registros a procesar, abrir el grid de empleados para filtrar
                       CanMove := ZBasicoSelectGrid_DevEx.GridSelectBasico(
                               TZetaClientDataSet( DataSet ), TIMSSVerificaDiferenciasDatosINFONAVITGridSelect, FALSE );
                       //si no hay registros a procesar, despues de filtrarlos por el grid de empleados, no avanzar
                       if ( DataSet.IsEmpty ) then
                       begin
                            ZetaDialogo.ZError( 'Error al actualizar datos Infonavit', 'No hay registros en la lista de empleados', 0 );
                            CanMove := False;
                            //cancelar los cambios realizados por el grid de empleados, cuando se eliminaron TODOS los registros a procesar
                            TClientDataSet( DataSet).CancelUpdates;
                       end;
                  end;

            end;
     end;
end;

function TWizIMSSActualizarDatosINFONAVIT.EjecutarWizard: Boolean;
begin
       try
            with dmCliente do
            begin
                 //ActualizarInfonavit( FirstDayOfBimestre( EncodeDate( YearInfo.ValorEntero, MesBimestre(cbBimestre.Valor), 1 ) ),
                 ActualizarInfonavit( FirstDayOfBimestre( EncodeDate( YearInfo.Value, MesBimestre(cbBimestre.Indice ), 1 ) ),
                                      TClientDataSet( DataSet ) );
            end;
       except
             on Error: Exception do
             begin
                  ZetaDialogo.zInformation( '', 'Error al validar par�metros del wizard.', 0 );
             end;
       end;
end;

procedure TWizIMSSActualizarDatosINFONAVIT.CargaParametros;
begin
     inherited;
     with Descripciones do
     begin
          Clear;
          AddString('Diferencias en', cbDiferenciasInfonavit.Properties.Items[ cbDiferenciasInfonavit.Indice ] );
          AddBoolean( 'Filtrar fecha otorgamiento', lFiltrarPorInicio.Checked );
          if ( lFiltrarPorInicio.Checked ) then
             AddDate( 'Fecha otorgamiento cr�dito', zFechaFiltro.Valor );
          AddString( 'Bimestre', ObtieneElemento( cbBimestre.ListaFija, cbBimestre.Indice - cbBimestre.Offset) );
          AddInteger( 'A�o', YearInfo.Value );
          if ( DataSet <> nil ) and (  DataSet.Active ) and ( Wizard.EsPaginaActual( Ejecucion ) ) then
             AddInteger ( 'Cantidad de empleados', DataSet.RecordCount);
     end;
end;

procedure TWizIMSSActualizarDatosINFONAVIT.cbDiferenciasInfonavitChange( Sender: TObject);
var
   sFiltro: String;
begin
     inherited;
     HabilitaContFiltrosInfo( FALSE );
     if ( DataSet <> NIL ) then
     begin
          with DataSet do
          begin
               if Active then
               begin
                    case cbDiferenciasInfonavit.ItemIndex of
                         0: sFiltro:= VACIO;
                         1: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_INICIO_CREDITO )] );
                         2: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_NUMERO_CREDITO ) ] );
                         3: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_TIPO_DESCUENTO ) ]);
                         4: sFiltro:= Format( 'DESCRIPCION = %s ', [EntreComillas( K_VALOR_DESCUENTO )] );
                    end;

                    Filtered := FALSE;
                    if lFiltrarPorInicio.Checked then
                       sFiltro:= ConcatFiltros(sFiltro, Format('CB_INF_INI >= %s',[EntreComillas( FechaAsStr(zFechaFiltro.Valor) ) ] ));
                    Filter := sFiltro;
                    Filtered := StrLleno(sFiltro);
               end;
          end;
     end;
end;

procedure TWizIMSSActualizarDatosINFONAVIT.HabilitaContFiltrosInfo( const lSetVisibles: Boolean);
var
   lHayRegistros: Boolean;
begin
     lHayRegistros:= ( not lSetVisibles ) or ( ( DataSet <> Nil ) and not ( DataSet.IsEmpty  ) );
     lblFechaFiltro.Enabled:= lFiltrarPorInicio.Checked and lHayRegistros;
     zFechaFiltro.Enabled:= lFiltrarPorInicio.Checked and lHayRegistros;

end;

//Para recibir de parametros los filtros iniciales del grid de empleados
function ShowWizard( FiltrarDiferenciasInfonavit: Integer; FiltrarPorInicio: Boolean; FechaFiltro: TDate; aDataSet: TClientDataSet): Boolean;overload;begin
     with TWizIMSSActualizarDatosINFONAVIT.Create( Application ) do
     begin
          try
             //recibe el TressShell.dsInfonavit.DataSet
             DataSet := aDataSet;
             cbDiferenciasInfonavit.Indice := FiltrarDiferenciasInfonavit;
             lFiltrarPorInicio.Checked := FiltrarPorInicio;
             zFechaFiltro.Valor := FechaFiltro;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

//Parametro: aDataset
function ShowWizard( aDataSet: TClientDataSet): Boolean;overload;
begin
     with TWizIMSSActualizarDatosINFONAVIT.Create( Application ) do
     begin
          try
             //recibe el TressShell.dsInfonavit.DataSet
             DataSet := aDataSet;
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

end.
