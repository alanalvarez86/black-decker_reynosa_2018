inherited CreditosSuspendidosTRESS: TCreditosSuspendidosTRESS
  Left = 0
  Top = 0
  Caption = 'Cr'#233'ditos suspendidos en TRESS'
  ClientHeight = 300
  ClientWidth = 635
  OldCreateOrder = False
  ExplicitWidth = 635
  ExplicitHeight = 300
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 635
    ExplicitWidth = 635
    inherited ValorActivo2: TPanel
      Width = 376
      ExplicitWidth = 376
      inherited textoValorActivo2: TLabel
        Width = 370
        ExplicitLeft = 290
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 635
    Height = 281
    ParentFont = False
    ExplicitTop = 22
    ExplicitWidth = 635
    ExplicitHeight = 281
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.Editing = True
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'CB_CODIGO'
        Options.Grouping = False
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        Options.Grouping = False
        Width = 64
      end
      object CB_SEGSOC: TcxGridDBColumn
        Caption = '# Seguro Social'
        DataBinding.FieldName = 'CB_SEGSOC'
        Options.Grouping = False
        Width = 64
      end
      object STATUS_EMP: TcxGridDBColumn
        Caption = 'Estatus'
        DataBinding.FieldName = 'STATUS_EMP'
        Width = 64
      end
      object NO_CREDITO: TcxGridDBColumn
        Caption = '# de Cr'#233'dito'
        DataBinding.FieldName = 'NO_CREDITO'
        Options.Grouping = False
        Width = 64
      end
      object TDESCUENTO: TcxGridDBColumn
        Caption = 'Tipo Descuento'
        DataBinding.FieldName = 'TDESCUENTO'
        Width = 64
      end
      object VDESCUENTO: TcxGridDBColumn
        Caption = 'Valor Descuento'
        DataBinding.FieldName = 'VDESCUENTO'
        Options.Grouping = False
        Width = 64
      end
      object CB_INF_INI: TcxGridDBColumn
        Caption = 'Fecha de Inicio'
        DataBinding.FieldName = 'CB_INF_INI'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyy'
        Options.Grouping = False
        Width = 64
      end
      object OBSERVA: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'OBSERVA'
        Options.Grouping = False
        Width = 64
      end
      object CB_FEC_ING: TcxGridDBColumn
        Caption = 'Fecha de Ingreso'
        DataBinding.FieldName = 'CB_FEC_ING'
        Options.Grouping = False
        Width = 64
      end
      object CB_FEC_BAJ: TcxGridDBColumn
        Caption = 'Fecha de Baja'
        DataBinding.FieldName = 'CB_FEC_BAJ'
        Options.Grouping = False
        Width = 64
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
