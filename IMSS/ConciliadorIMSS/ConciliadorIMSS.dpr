program ConciliadorIMSS;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


uses
  FTressShell in 'FTressShell.pas' {TressShell},
  MidasLib,
  Vcl.Forms,
  ZetaClientTools,
  System.SysUtils,
  ZBaseDlgModal_DevEx in '..\..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  DBasicoCliente in '..\..\Datamodules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseShell in '..\..\Tools\ZBaseShell.pas' {BaseShell},
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  DConciliador in 'DConciliador.pas' {dmConciliador: TDataModule},
  ZBasicoNavBarShell in '..\..\Tools\ZBasicoNavBarShell.pas' {BasicoNavBarShell},
  ZBaseGridLectura_DevEx in '..\..\Tools\ZBaseGridLectura_DevEx.pas',
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZcxWizardBasico in '..\..\Tools\ZcxWizardBasico.pas' {CXWizardBasico},
  ZcxBaseWizard in '..\..\Tools\ZcxBaseWizard.pas' {cxBaseWizard},
  ZetaCxWizard in '..\..\Componentes\ZetaCxWizard.pas' {$R *.RES},
  FWizIMSSActualizarDatosINFONAVIT in 'FWizIMSSActualizarDatosINFONAVIT.pas' {WizIMSSActualizarDatosINFONAVIT},
  FIMSSVerificaDiferenciasDatosINFONAVITGridSelect in 'FIMSSVerificaDiferenciasDatosINFONAVITGridSelect.pas' {IMSSVerificaDiferenciasDatosINFONAVITGridSelect},
  ZBasicoSelectGrid_DevEx in '..\..\Tools\ZBasicoSelectGrid_DevEx.pas' {BasicoGridSelect_DevEx},
  FWizIMSSImportarCreditos in 'FWizIMSSImportarCreditos.pas' {WizIMSSImportarCreditos},
  FWizIMSSImportarReinicioDescuento in 'FWizIMSSImportarReinicioDescuento.pas' {ImportarReinicioDescuento},
  FWizIMSSImportarSuspensiones in 'FWizIMSSImportarSuspensiones.pas' {ImportarSuspensiones},
  FIMSSVerificaReinicioDescuentosGridSelect in 'FIMSSVerificaReinicioDescuentosGridSelect.pas' {IMSSVerificaReinicioDescuentosGridSelect},
  FIMSSVerificaImportarCreditosGridSelect in 'FIMSSVerificaImportarCreditosGridSelect.pas' {IMSSVerificaImportarCreditosGridSelect},
  FWizIMSSConciliarBase in 'FWizIMSSConciliarBase.pas' {WizIMSSConciliarBase};

{$R *.RES}
{$R WindowsXP.res}
{$R ..\Traducciones\Spanish.RES}


begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;

  Application.Title := 'Conciliador IMSS';
  Application.HelpFile := 'Conciliador IMSS.chm';
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login ( False ) then
       begin
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
           Free;
  end;

end.
