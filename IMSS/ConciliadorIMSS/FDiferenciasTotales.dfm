inherited DiferenciasTotales: TDiferenciasTotales
  Left = 757
  Top = 339
  Caption = 'Diferencias en totales'
  ClientHeight = 511
  ClientWidth = 748
  OldCreateOrder = False
  Visible = True
  ExplicitWidth = 748
  ExplicitHeight = 511
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 748
    ExplicitWidth = 748
    inherited ValorActivo2: TPanel
      Width = 489
      ExplicitWidth = 489
      inherited textoValorActivo2: TLabel
        Width = 483
        ExplicitLeft = 403
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 748
    Height = 492
    ExplicitWidth = 748
    ExplicitHeight = 492
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      OptionsBehavior.FocusCellOnTab = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsCustomize.ColumnHorzSizing = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.CancelOnExit = True
      OptionsSelection.HideSelection = True
      OptionsView.NoDataToDisplayInfoText = 'No se encontraron diferencias'
      OptionsView.Indicator = True
      object DESCRIPCION: TcxGridDBColumn
        Caption = 'Rama'
        DataBinding.FieldName = 'DESCRIPCION'
        MinWidth = 150
        Width = 150
      end
      object TRESS: TcxGridDBColumn
        Caption = 'Tress'
        DataBinding.FieldName = 'TRESS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 150
        Width = 150
      end
      object CD_IMSS: TcxGridDBColumn
        Caption = 'IMSS'
        DataBinding.FieldName = 'CD_IMSS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 150
        Width = 150
      end
      object DIFERENCIA: TcxGridDBColumn
        Caption = 'Diferencia'
        DataBinding.FieldName = 'DIFERENCIA'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 150
        Width = 150
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 80
    Top = 168
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
