unit FWizIMSSConciliarBase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZcxBaseWizard, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus, cxClasses,
  cxShellBrowserDialog, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, Vcl.StdCtrls,
  ZetaKeyCombo, Vcl.Mask, ZetaNumero, cxTextEdit, cxButtons, cxMaskEdit,
  cxDropDownEdit, ZetaCXStateComboBox, cxSpinEdit, cxRadioGroup,
  DCliente, Vcl.ComCtrls, cxProgressBar;

type
  TWizIMSSConciliarBase = class(TcxBaseWizard)
    ValoresActivosgb: TcxGroupBox;
    YearLBL: TcxLabel;
    MesLBL: TcxLabel;
    TipoLBL: TcxLabel;
    PatronLBL: TcxLabel;
    Mes: TcxStateComboBox;
    Tipo: TcxStateComboBox;
    Patron: TZetaKeyLookup_DevEx;
    Year: TcxSpinEdit;
    EmisionLBL: TcxLabel;
    EmisionMensual: TcxRadioButton;
    EmisionBimestral: TcxRadioButton;
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;

    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure SetControls;
    procedure EmisionMensualClick(Sender: TObject);
    procedure EmisionBimestralClick(Sender: TObject);
    procedure MesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  protected
    procedure CargaParametros;override;
    function GetEmision: eEmisionClase;
    procedure SetEmision( ClaseEmision: eEmisionClase);
  end;
var
  WizIMSSConciliarBase: TWizIMSSConciliarBase;


implementation

{$R *.dfm}

uses
    ZetaRegistryCliente, ZetaCommonTools, ZetaCommonLists,
    ZetaDialogo, FTressShell;

procedure TWizIMSSConciliarBase.FormCreate(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          GetIMSSInicial;
          with Patron do
          begin
               LookupDataset := cdsRPatron;
               LLave := ClientRegistry.ConciliadorIMSSPatron;
               if ( Llave.IsEmpty )  then
                  Llave := LookupDataset.FieldByName( LookupDataset.LookupKeyField ).AsString;
          end;
     end;

     try
        //anio
        Year.Value := ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear);
        if ( Year.Value = 0 ) then
           Year.Value := TheYear(dmCliente.FechaDefault);
        //mes
        Mes.Indice := ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes);
        if ( Mes.Indice = 0 ) then
           Mes.Indice := TheMonth(dmCliente.FechaDefault);
        //tipo
        if ( Trim(ClientRegistry.ConciliadorIMSSTipo) = '' )  then
          // Tipo.Indice := 0
           Tipo.Indice := Ord( tlOrdinaria )
        else
            Tipo.Indice :=  ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSTipo);

        SetEmision( eEmisionClase ( ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSClaseEmision) ));

     except
           on Error: Exception do
           begin
                ZetaDialogo.zInformation( '', 'Error al leer configuraci�n de valores activos.', 0 );
           end;
     end;
     //DevEx
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;

     SetControls;
end;



procedure TWizIMSSConciliarBase.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Patron', Patron.Llave);
          AddInteger( 'Year', Year.Value );
          AddInteger( 'Mes', Mes.Indice );
          AddInteger( 'Tipo', tipo.Indice );
          AddString( 'Periodo', aEmisionClase[ eEmisionClase(GetEmision)] );

     end;

     with Descripciones do
     begin
          AddString( 'Patr�n', Patron.Llave + ': ' + Patron.Descripcion );
          AddInteger( 'A�o', Year.Value );
          AddString( 'Mes', ObtieneElemento( lfMeses,  (Mes.Indice - Mes.Offset) ) );
          AddString( 'Tipo', ObtieneElemento( lfTipoLiqIMSS, Tipo.Indice ) );
          AddString( 'Periodo', aEmisionClase[ eEmisionClase(GetEmision)] );
     end;

end;

procedure TWizIMSSConciliarBase.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
  var CanMove: Boolean);
begin
  inherited;
      with Wizard do
      begin
            if CanMove and Adelante and EsPaginaActual( Parametros ) then
             begin
                  if ( Patron.Llave = '' ) then
                  begin
                     CanMove := Error( 'El patr�n no puede quedar vac�o', Patron)
                  end
                  else if ( Year.Value = 0 ) then
                  begin
                     CanMove := Error( 'El a�o no debe quedar vac�o', Year)
                  end
                  else if ( Mes.Indice = 0 ) then
                  begin
                     CanMove := Error( 'El mes no debe quedar vac�o', Year)
                  end
                  else if ( Tipo.Indice < 0 ) then
                  begin
                     CanMove := Error( 'El tipo de conciliaci�n no debe quedar vac�o', Year)
                   end;

             end;

     end;

end;

function TWizIMSSConciliarBase.GetEmision: eEmisionClase;
begin
     if EmisionMensual.Checked then
        Result := eeCMensual
     else
         Result := eeCBimestral;
end;

procedure TWizIMSSConciliarBase.SetEmision( ClaseEmision: eEmisionClase);
begin
     if ( ClaseEmision = eeCMensual ) then
     begin
        EmisionMensual.Checked := True;
        EmisionBimestral.Checked := False;
     end
     else if ( ClaseEmision = eeCBimestral ) then
     begin
          EmisionBimestral.Checked := True;
          EmisionMensual.Checked := False;
     end;
end;


procedure TWizIMSSConciliarBase.SetControls;
var
   lEsBimestre: Boolean;
begin
     lEsBimestre := ZetaCommonTools.EsBimestre( Mes.Indice );
     if not lEsBimestre then
     begin
          EmisionMensual.Checked := True;
          EmisionBimestral.Checked := False;
     end;
     EmisionBimestral.Enabled := lEsBimestre;
     EmisionLBL.Enabled := lEsBimestre;
end;

procedure TWizIMSSConciliarBase.EmisionMensualClick(Sender: TObject);
begin
     inherited;
     EmisionBimestral.Checked := not EmisionMensual.Checked;
end;

procedure TWizIMSSConciliarBase.EmisionBimestralClick(Sender: TObject);
begin
     inherited;
     EmisionMensual.Checked := not EmisionBimestral.Checked;
end;


procedure TWizIMSSConciliarBase.MesClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;



end.
