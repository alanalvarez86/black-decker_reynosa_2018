unit DSUA2006;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs,  Db,
     //*ODSI, OVCL, OCL,
     DBClient, Provider, FileCtrl,
     {$ifndef VER130}
     Variants,
     {$endif}
     DCliente,
     DConciliador,
     ZetaCommonClasses,
     ZetaClientDataSet, ZetaServerDataSet,
     Data.Win.ADODB;

type
  TdmSUA2006 = class(TdmConciliador)
    prvUnico: TDataSetProvider;
    ADOQuery: TADOQuery;
    ADOConn: TADOConnection;
    procedure cdsEmpleadosAlCrearCampos(Sender: TObject);
    procedure cdsEmpleadosCalcFields(DataSet: TDataSet);
    procedure cdsEmpleadosAfterOpen(DataSet: TDataSet);
    procedure cdsMovimientosAfterOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    ConnString : String;
    function ConectarMovimiento( const sAfiliacion: String ): Boolean;
    function ConectarTabla(Tabla: TClientDataset; const sQuery: string; const sTabla: String = VACIO): Boolean;
    function GetPatronID: Boolean;
    function GetSQLScript( const eTipo: eEmisionTipo ): String;
    function OpenSQL(const sSQL: String; const lDatos: Boolean): OleVariant;
    procedure ConciliarEmpleados;
    procedure ConciliarListaEmpleados(Original, Datos: TDataset; const lTress: Boolean; Log: TStrings);
    procedure ConciliarListaMovimientos( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings  );
    procedure ConciliarMovimientos( const iEmpleado: TNumEmp; const sAfiliacion: String; Log: TStrings  );
    procedure ConciliarPatrones;
  protected
    { Protected declarations }
    function DataAsString(const sData: String; const iStart, iLength: Integer; const lPurosCeros: Boolean = false ): String;
    function DataAsInteger(const sData: String; const iStart, iLength: Integer): Integer;
    function DataAsFloat(const sData: String; const iStart, iLength: Integer): Extended;
    function DataAsDateDay(const sData: String; const iStart, iLength: Integer): TDate;
    function DataAsDateMonth(const sData: String; const iStart, iLength: Integer): TDate;
    function DataAsDateYear(const sData: String; const iStart, iLength: Integer): TDate;
    //function GetFileName( const eTipo: eEmisionTipo ): String;
    function GetConciliaDescripcion: string;override;
    procedure ResetBitacoras;override;
  public
    { Public declarations }
    function ConectarBaseDeDatos( const sFileName: String ): Boolean;override;
    procedure DesconectaBasedeDatos;override;
    function ConectarTablas: Boolean;override;
    procedure Conciliar;override;
  end;

var
  dmSUA2006: TdmSUA2006;

implementation

uses FTressShell,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonTools;

{$R *.DFM}


var
   K_MAESTRA_INICIAL: string;

const
     K_ANCHO_COLUMNA = 25;
     K_ANCHO = 90;

     K_ANCHO_REGISTRO_PATRONAL = 11;

     K_ESPACIO = ' ';
     K_INFONAVIT_INI = 'FEC_INF_INI';
     K_AFILIACION = 'AFILIACION';
     K_NUM_CRED_INFONAVIT = 'NO_CRED_INF';
     K_CIENTO = 100;

     K_MAESTRA_EAMP_ALTA = '01';
     K_MAESTRA_EAMP_ALTA2 = '08';
     K_MAESTRA_EAMP_BAJA = '02';
     K_MAESTRA_EAMP_CAMBIO_COTIZACION = '07';
     K_MAESTRA_EAMP_RESTABLECIMIENTO = '03';
     K_MAESTRA_INICIAL_IMSS = '09';
     K_MAESTRA_INICIAL_TRESS = '00';
     K_MAESTRA_INICIAL_SUA = '13';


function GetScript( const eValor: eScript ): String;
begin
     case eValor of
          esPatronMensual: Result := 'select '+
                                     'REG_PATR as TB_NUMREG, '+   { RPATRON.TB_NUMREG }
                                     'PRIM_RT as PRIMA_RT, '+     { PRIESGO.RT_PRIMA on PRIESGO.RT_FECHA }
                                     'TOT_ACR as LS_NUM_TRA, '+   { LIQ_IMSS.LS_NUM_TRA }
                                     'CTA_FIJ as LS_EYM_FIJ, '+   { LIQ_IMSS.LS_EYM_FIJ }
                                     'CTA_EXC as LS_EYM_EXC, '+   { LIQ_IMSS.LS_EYM_EXC }
                                     'PRE_DIN as LS_EYM_DIN, '+   { LIQ_IMSS.LS_EYM_DIN }
                                     'PRE_ESP as LS_EYM_ESP, '+   { LIQ_IMSS.LS_EYM_ESP }
                                     'INV_VID as LS_INV_VID, '+   { LIQ_IMSS.LS_INV_VID }
                                     'RIE_TRA as LS_RIESGOS, '+   { LIQ_IMSS.LS_RIESGOS }
                                     'GUA_DER as LS_GUARDER, '+   { LIQ_IMSS.LS_GUARDER }
                                     'TOT_DIA as LS_DIAS_CO '+    { LIQ_IMSS.LS_DIAS_CO }
                                     'from RESUMEN '+
                                     'where ( REG_PATR = ''%s'' )';
          esPatronBimestral: Result := 'select '+
                                       'REG_PATR as TB_NUMREG, '+                 { RPATRON.TB_NUMREG }
                                       '0 as LS_NUM_BIM, '+                       { LIQ_IMSS.LS_NUM_BIM }
                                       'RET_SAR as LS_RETIRO, '+                  { LIQ_IMSS.LS_RETIRO }
                                       'CEN_VEJPat + Cen_VEJObr as LS_CES_VEJ, '+ { LIQ_IMSS.LS_CES_VEJ }
                                       'TOT_DIAB as LS_DIAS_BM, '+                { LIQ_IMSS.LS_DIAS_BM }
                                       'TOT_ACR as LS_INF_NUM, '+                 { LIQ_IMSS.LS_INF_NUM }
                                       'VIV_SIN	as LS_INF_NAC, '+                 { LIQ_IMSS.LS_INF_NAC }
                                       'VIV_CON	as LS_INF_ACR, '+                 { LIQ_IMSS.LS_INF_ACR }
                                       'AMO_INF	as LS_INF_AMO '+                  { LIQ_IMSS.LS_INF_AMO }
                                       'from RESUMEN '+
                                       'where ( REG_PATR = ''%s'' )';
          esEmpleadoMensual: Result := 'select '+
                                       '-1 as CB_CODIGO, '+
                                       '0 as ' + K_FOUND + ', '+
                                       'NSS as ' + K_AFILIACION + ', '+   { COLABORA.CB_SEGSOC }
                                       'NSS as ' + K_NUM_SS + ', '+       { Dummy para Afiliacion }
                                       'Trabajador as TB_MODULO, '+             { RPATRON.TB_MODULO }
                                       'TMP_NOM as NOMBRE_SS, '+              { GetNombreSUA( COLABORA.CB_APE_PAT ) + '$' + GetNombreSUA( COLABORA.CB_APE_MAT ) + '$' + GetNombreSUA( COLABORA.CB_NOMBRES ) }
                                       'Jornada_Semana_Reducida as TU_TIP_JOR, '+          { TURNO.TU_TIP_JOR - 1 where ( TURNO.TU_CODIGO = COLABORA.CB_TURNO ) }
                                       'RFC_CURP as CB_RFC, '+                    { COLABORA.CB_CURP }
                                       'ASEGURA.CURP as CB_CURP, '+                    { COLABORA.CB_CURP }
//                                       ''''' as ' + K_INFONAVIT_INI + ', '+
                                       '0 as CB_INF_INI, ' +
                                       'Dias_Cotizados_Mes as LE_DIAS_CO, '+
                                       //Dias_Incapacidad
                                       //Dias_Ausentismo
                                       'Cuota_Fija	as LE_EYM_FIJ, '+
                                       'Cuota_Excedente	as LE_EYM_EXC, '+
                                       'Prestaciones_Dinero	+ Cuota_PdObr	as LE_EYM_DIN, '+
                                       'Gastos_Medicos_Pensionados	+ Cuota_GmpObr as LE_EYM_ESP, '+
                                       'Riesgo_Trabajo	as LE_RIESGOS, '+
                                       'Invalidez_Vida	as LE_INV_VID, '+
                                       'Guarderias	as		LE_GUARDER, '+
                                       'Retiro	as	LE_RETIRO, '+
                                       'Cesantia_Vejez_Patronal + Cesantia_Vejez_Obrera as LE_CES_VEJ, '+
                                       'Aportacion_Voluntaria	as	LE_APO_VOL ' +
                                       'from REGISTRO_03 '+
                                       'INNER JOIN ASEGURA '+
                                       'ON ( ASEGURA.NUM_AFIL = REGISTRO_03.NSS AND ASEGURA.REG_PATR = ''%s'' )'+
                                       'order by NSS';
          esEmpleadoBimestral: Result := 'select '+
                                         '-1 as CB_CODIGO, '+
                                         '0 as ' + K_FOUND + ', '+
                                         'NSS as ' + K_AFILIACION + ', '+            { COLABORA.CB_SEGSOC }
                                         'NSS as ' + K_NUM_SS + ', '+                { Dummy para Afiliacion }
                                         'Trabajador as TB_MODULO, '+             { RPATRON.TB_MODULO }
                                         'TMP_NOM as NOMBRE_SS, '+              { GetNombreSUA( COLABORA.CB_APE_PAT ) + '$' + GetNombreSUA( COLABORA.CB_APE_MAT ) + '$' + GetNombreSUA( COLABORA.CB_NOMBRES ) }
                                         'Jornada_Semana_Reducida as TU_TIP_JOR, '+          { TURNO.TU_TIP_JOR - 1 where ( TURNO.TU_CODIGO = COLABORA.CB_TURNO ) }
                                         'RFC_CURP as CB_RFC, '+                    { COLABORA.CB_CURP }
                                         'ASEGURA.CURP as CB_CURP, '+                    { COLABORA.CB_CURP }
                                         'PAG_INFO as CB_INFCRED, '+                     { COLABORA.CB_INFCRED }
                                         '0 as CB_INFTIPO, '+                 { COLABORA.CB_INFTIPO }
                                         'TIP_DSC as CB_INFTIPO_TMP, '+
                                         'val_dsc as CB_INFTASA, '+                       { COLABORA.CB_INFTASA }
//                                         'fec_dsc as ' + K_INFONAVIT_INI + ' '+           { ZetaCommonTools.FechaToStr() = COLABORA.CB_INF_INI }
                                         'fec_dsc as CB_INF_INI '+           { ZetaCommonTools.FechaToStr() = COLABORA.CB_INF_INI }
                                         'from REGISTRO_03 '+
                                         'INNER JOIN ASEGURA '+
                                         'ON ( ASEGURA.NUM_AFIL = REGISTRO_03.NSS AND ASEGURA.REG_PATR = ''%s'' )'+
                                         'order by NSS';
          esMovimientoMensual: Result := 'select '+
                                         '0 as ' + K_ORIGEN + ', '+
                                         'Tip_Mov as ' + K_TIPO_MOV + ', '+   { TipoMov3ToIMSS( LIQ_MOV.LM_KAR_TIP ) - ver CD: MAESTRA.EAMP }
                                         'Fec_Mov as LM_FECHA, '+             { LIQ_MOV.LM_FECHA }
                                         'Dia_Cot as LM_DIAS, '+              { LIQ_MOV.LM_DIAS }
                                         'Dia_Inc	as	LM_INCAPAC, '+           { LIQ_MOV.LM_INCAPACI }
                                         'Dia_Aus	as LM_AUSENCI, '+           { LIQ_MOV.LM_AUSENCI }
                                         'Sal_Dia as LM_BASE, '+              { LIQ_MOV.LM_BASE }
                                         'CF as LM_EYM_FIJ, '+                { LIQ_MOV.LM_EYM_FIJ }
                                         'EX as LM_EYM_EXC, '+                { LIQ_MOV.LM_EYM_EXC }
                                         'PD as LM_EYM_DIN, '+                { LIQ_MOV.LM_EYM_DIN }
                                         'GMP as LM_EYM_ESP, '+               { LIQ_MOV.LM_EYM_ESP }
                                         'IV as LM_INV_VID, '+                { LIQ_MOV.LM_INV_VID - Parece que debiera corresponder a EMIM.IMP_IV, pero el programa del CD lo tiene asignado a esta columna ( � bug ? ) }
                                         'RT as LM_RIESGOS, '+                { LIQ_MOV.LM_RIESGOS - Parece que debiera corresponder a EMIM.IMP_RT, pero el programa del CD lo tiene asignado a esta columna ( � bug ? ) }
                                         'GPS as LM_GUARDER, '+               { LIQ_MOV.LM_GUARDER }
                                         'EXPA	as LM_EYMEXCP, '+
                                         'PDP	as LM_EYMDINP, '+
                                         'GMPP	as LM_EYMESPP, '+
                                         'IVP	as LM_INVVIDP '+
                                         'from RELTRA '+
                                         'where ( Num_Afi = ''%s'' ) '+
//                                         'where ( Num_Afi = ''%s'' ) and ( Periodo <> '''') '+
                                         'order by Fec_Mov, Tip_Mov';
          esMovimientoBimestral: Result := 'select '+
                                           '0 as ' + K_ORIGEN + ', '+
                                           'Tip_Mov as ' + K_TIPO_MOV + ', '+   { TipoMov3ToIMSS( LIQ_MOV.LM_KAR_TIP ) - ver CD: MAESTRA.EAMP }
                                           'Fec_Mov as LM_FECHA, '+             { LIQ_MOV.LM_FECHA }
                                           'Dia_Cot as LM_DIAS, '+              { LIQ_MOV.LM_DIAS }
                                           'Dia_Inc	as	LM_INCAPAC, '+
                                           'Dia_Aus	as LM_AUSENCI, '+
                                           'Sal_Dia as LM_BASE, '+              { LIQ_MOV.LM_BASE }
                                           'Retiro as LM_RETIRO, '+             { LIQ_MOV.LM_RETIRO }
                                           'CYV as LM_CES_VEJ, '+               { LIQ_MOV.LM_CES_VEJ }
                                           'CYVP as LM_CESVEJP, '+              { LIQ_MOV.LM_CESVEJP }
                                           'APORTACC + APORTASC as LM_INF_PAT, '+                  { LIQ_MOV.LM_INF_PAT }
                                           'AMORTIZA as LM_INF_AMO '+                   { LIQ_MOV.LM_INF_AMO }
                                           'from RELTRABIM '+
                                           'where ( Num_Afi = ''%s'' ) '+
                                           'order by Fec_Mov, Tip_Mov';
          esPatronID: Result := 'select REG_PATR as PATRON_ID '+
                                ' from RESUMEN where ( REG_PATR = ''%s'' ) AND ( MES_ANO = ''%s'')';
     else
         Result := VACIO;
     end;
end;

{ ************ TdmSUA2006 ************* }

procedure TdmSUA2006.DataModuleCreate(Sender: TObject);
begin
     inherited;
     K_MAESTRA_INICIAL := K_MAESTRA_INICIAL_SUA;
end;

procedure TdmSUA2006.DesconectaBasedeDatos;
begin
     cdsPatron.Close;
     cdsEmpleados.Close;
     cdsMovimientos.Close;
     cdsMovConc.Close;

     try
        if ( ADOConn.Connected ) then
           ADOConn.Close;
     except on E:exception do
            begin
                 ZetaDialogo.zExcepcion( '� Error !', 'Error al cerrar conexi�n ADO de las tablas de SUA', E, 0 );
            end;
     end;

end;

function TdmSUA2006.ConectarBaseDeDatos( const sFileName: String ): Boolean;
begin
     Result := False;

     cdsEmpleados.AfterOpen := cdsEmpleadosAfterOpen;
//     cdsEmpleados.OnCalcFields := cdsEmpleadosCalcFields;
//     cdsEmpleados.AlCrearCampos := cdsEmpleadosAlCrearCampos;

     cdsMovimientos.AfterOpen := cdsMovimientosAfterOpen;

     try
        if ZetaCommonTools.StrVacio( sFileName ) then
        begin
             ZetaDialogo.zError( '� Atenci�n !', 'No Se Especific� El Archivo De Base de Datos Del SUA', 0 );
        end
        else
        begin
             if FileExists( sFileName ) then
             begin
             {
                  with Hdbc do
                  begin
                       Connected := False;
                       Driver := 'Microsoft Access Driver (*.mdb)';
                       with Attributes do
                       begin
                            Clear;
                            Add( Format( 'DBQ=%s', [ sFileName ] ) );
                            Add( Format( 'ReadOnly=%d', [ 1 ] ) );
                       end;
                       Password := Contrasena;
                       Connected := True;
                  end;
                  }
                  try
                     ADOConn := TADOConnection.Create(Self);
                  except on E:exception do    
                         begin
                              ZetaDialogo.zExcepcion( '� Error !', 'Error al crear conexion ADO', E, 0 );
                         end;
                  end;
                  {$IFDEF WIN32}
                  ConnString := 'Driver={Microsoft Access Driver (*.mdb)};' +
                  'DBQ=%s;Persist Security Info=False;password=%s';
                  {$ELSE}
                  ConnString := 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};' +
                  'DBQ=%s;Persist Security Info=False;password=%s';
                  {$ENDIF}

                  ConnString := Format( ConnString, [sFileName, Contrasena ]);
                  ADOConn.ConnectionString := ConnString;
                  ADOConn.LoginPrompt := False;

                  try
                     ADOConn.Connected := True;
                     Result := True;
                  except on E:exception do
                         begin
                              ZetaDialogo.zExcepcion( '� Error !', 'Error al abrir conexi�n ADO de las tablas de SUA', E, 0 );
                              Result := False;
                         end;
                  end;
             end
             else
             begin
                  ZetaDialogo.zError( '� Atenci�n !', 'El Archivo De Base de Datos Del SUA No Existe', 0 );
             end;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', 'Error Al Abrir Base De Datos Del SUA', Error, 0 );
           end;
     end;
end;

procedure TdmSUA2006.cdsEmpleadosAlCrearCampos(Sender: TObject);
begin
     with cdsEmpleados do
     begin
          CreateCalculated( 'CB_INF_INI', ftDateTime, 0 );
     end;
end;

procedure TdmSUA2006.cdsEmpleadosCalcFields(DataSet: TDataSet);
begin
     with cdsEmpleados do
     begin
          with FieldByName( K_INFONAVIT_INI ) do
          begin
               if StrLleno( AsString ) then
                  FieldByName( 'CB_INF_INI' ).AsDateTime := ZetaCommonTools.StrToFecha( AsString  )
               else
                   FieldByName( 'CB_INF_INI' ).AsDateTime := NullDateTime;
          end;
     end;
end;

procedure TdmSUA2006.cdsEmpleadosAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          while not Eof do
          begin
               Edit;
               try
                  {with FieldByName( K_AFILIACION ) do
                  begin
                       FieldByName( K_NUM_SS ).AsString := Copy( AsString, 2, ( Length( AsString ) - 1 ) );
                  end;}
                  if ( Clase = eecBimestral ) then
                  begin
                       FieldByName('CB_INFTIPO').AsInteger := StrAsInteger( FieldByName('CB_INFTIPO_TMP').AsString );
{
                       with FieldByName( K_NUM_CRED_INFONAVIT ) do
                       begin
                            FieldByName( 'CB_INFCRED' ).AsString := Copy( AsString, 3, ( Length( AsString ) - 2 ) );
                       end;
                       with FieldByName( 'CB_INFCRED' ) do
                       begin
                            if ( AsString = StringOfChar( '0',Length( AsString ) ) ) then
                            begin
                                 AsString := VACIO;
                            end;
                       end;
}
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          First;
     end;
end;

procedure TdmSUA2006.cdsMovimientosAfterOpen(DataSet: TDataSet);
begin
   with Dataset do
     begin
          while not Eof do
          begin
               Edit;

               try
                  with FieldByName( K_TIPO_MOV ) do
                  begin
                       if ( AsString = K_MAESTRA_INICIAL ) then
                       begin
                            AsString := K_MAESTRA_INICIAL_TRESS;
                            with FieldByName( 'LM_FECHA' ) do
                            begin
                                 AsDateTime := dmCliente.FechaInicial;
                            end;
                       end;

                       if ( AsString = K_MAESTRA_EAMP_ALTA2 ) then
                          AsString := K_MAESTRA_EAMP_ALTA;
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          First;
     end;
end;

function TdmSUA2006.OpenSQL( const sSQL: String; const lDatos: Boolean): OleVariant;
var
   iRecords: Integer;
begin
{
     with qryMaster do
     begin
          Active := False;
          SQL.Text := sSQL;
          FRecsOut := 0;
          if ( lDatos ) then
             iRecords := -1
          else
              iRecords := 0;
          Result := prvUnico.GetRecords( iRecords, FRecsOut, Ord( grMetaData ) + Ord( grReset ) )
     end;
 }
       try
          ADOQuery := TADOQuery.Create(Self);
          ADOQuery.Connection := ADOConn;
          ADOQuery.SQL.Add(sSQL);
          ADOQuery.Active := True;
          prvUnico.DataSet := ADOQuery;
          FRecsOut := 0;
          if ( lDatos ) then
             iRecords := -1
          else
              iRecords := 0;
          Result := prvUnico.GetRecords( iRecords, FRecsOut, Ord( grMetaData ) + Ord( grReset ) );
          ADOQuery.Close;
          ADOQuery.Free;
       except
       on e: EADOError do
       begin
            ZetaDialogo.zExcepcion( '� Error !', 'Error al en la consulta a las tablas de SUA', E, 0 );
       end;
  end;

end;

function TdmSUA2006.ConectarTabla( Tabla: TClientDataset; const sQuery: string; const sTabla: String = VACIO ): Boolean;
begin
     try
        with Tabla do
        begin
             DisableControls;
             try
                Active := False;
                Data := OpenSQL( Format( sQuery, [ FPatronID ] ), True );
                if not Active then
                   Active := True;
             finally
                    EnableControls;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Abrir Tabla %s De SUA', [ sTabla ] ), Error, 0 );
                Result := False;
           end;
     end;
end;

function TdmSUA2006.GetSQLScript( const eTipo: eEmisionTipo ): String;
begin
     case Clase of
          eeCMensual:
          begin
               case eTipo of
                    eeTPatron: Result := GetScript( esPatronMensual );
                    eeTEmpleados: Result := GetScript( esEmpleadoMensual );
                    eeTMovimientos: Result := GetScript( esMovimientoMensual );
               end;
           end;
           eeCBimestral:
           begin
                case eTipo of
                     eeTPatron: Result := GetScript( esPatronBimestral );
                     eeTEmpleados: Result := GetScript( esEmpleadoBimestral );
                     eeTMovimientos: Result := GetScript( esMovimientoBimestral );
                end;
           end;

     end;
end;


function TdmSUA2006.GetPatronID: Boolean;
 var
    iMes: integer;
begin
     Result := False;
     FPatronID := VACIO;

     if ( Clase = eeCMensual ) then
        iMes := Mes
     else
         iMes := Mes * 2;

     with cdsPatron do
     begin
          Active := False;
          Data := OpenSQL( Format( GetScript( esPatronID ), [ dmCliente.IMSSRegistro,
                                                              IntToStr(Year) +
                                                              PadLCar( IntToStr( iMes ),2, '0' ) ] ), True );
          if IsEmpty then
          begin
               ZetaDialogo.zError( '� Error !', Format( 'El SUA no Contiene Informaci�n para' + CR_LF + 'Registro Patronal %s, A�o %d, Mes %s' + CR_LF,
                                                        [ dmCliente.IMSSRegistro,
                                                          Year,
                                                          ObtieneElemento( lfMeses,  dmCliente.IMSSMes - 1  ) ] ), 0 );
          end
          else
          begin
               FPatronID := FieldByName( 'PATRON_ID' ).AsString;
               Result := True;
          end;
          Active := False;
     end;
end;

function TdmSUA2006.ConectarMovimiento( const sAfiliacion: String ): Boolean;
var
   eQuery: eScript;
   Datos: Variant;
begin
     Result := False;
     case Clase of
          eeCMensual: eQuery := esMovimientoMensual;
     else
         eQuery := esMovimientoBimestral;
     end;
     try
//        if ( sAfiliacion = '25976907771' ) then
//        if ( sAfiliacion = '21058737541' ) then
//           ShowMessage('cosa');
        Datos := OpenSQL( Format( GetScript( eQuery ), [ sAfiliacion ] ), True );
        with cdsMovimientos do
        begin
             Active := False;
             Data := Datos;
        end;
        with cdsMovConc do
        begin
             Active := False;
             Data := Datos;
             EmptyDataset;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Movimientos de IMSS # %s', [ sAfiliacion ] ), Error, 0 );
           end;
     end;
end;

function TdmSUA2006.DataAsString( const sData: String; const iStart, iLength: Integer; const lPurosCeros: Boolean = false  ): String;
begin
     if ( Length( sData ) < ( iStart + iLength - 1 ) ) then
        raise Exception.Create( Format( 'Datos Insuficientes: Substr( "%s", %d, %d )', [ sData, iStart, iLength ] ) )
     else
     begin
          Result := Copy( sData, iStart, iLength );
          if lPurosCeros then
          begin
               if ( Result = StringOfChar( '0', Length( Result ) ) ) then
                  Result := '';
          end;
     end;
end;

function TdmSUA2006.DataAsInteger( const sData: String; const iStart, iLength: Integer ): Integer;
begin
     Result := StrToIntDef( DataAsString( sData, iStart, iLength ), 0 );
end;

function TdmSUA2006.DataAsFloat( const sData: String; const iStart, iLength: Integer ): Extended;
 var
    sValor: string;
begin
     sValor := DataAsString( sData, iStart, iLength );
     Result := ZetaCommonTools.StrToReal( sValor );
     if Pos( '.', sValor ) = 0 then
        Result := Result/ K_CIENTO;
end;

function TdmSUA2006.DataAsDateYear( const sData: String; const iStart, iLength: Integer ): TDate;
begin
     Result := ZetaCommonTools.GetImportedDateYear( DataAsString( sData, iStart, iLength ) );
end;

function TdmSUA2006.DataAsDateMonth( const sData: String; const iStart, iLength: Integer ): TDate;
begin
     Result := ZetaCommonTools.GetImportedDateMes( DataAsString( sData, iStart, iLength ) );
end;

function TdmSUA2006.DataAsDateDay( const sData: String; const iStart, iLength: Integer ): TDate;
begin
     Result := ZetaCommonTools.GetImportedDateDia( DataAsString( sData, iStart, iLength ) );
end;

function TdmSUA2006.ConectarTablas: Boolean;
begin
     try
        Result := GetPatronID;
        if Result then
        begin
             with cdsEmpleados do
             begin
                  AfterOpen := cdsEmpleadosAfterOpen;
//                  OnCalcFields := cdsEmpleadosCalcFields;
//                  AlCrearCampos := cdsEmpleadosAlCrearCampos;
             end;
             cdsMovimientos.AfterOpen := cdsMovimientosAfterOpen;
             Result := ConectarTabla( cdsPatron, GetSQLScript( eetPatron ) ) and
                       ConectarTabla( cdsEmpleados, GetSQLScript( eetEmpleados ) );
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', 'Error Al Abrir Tablas De SUA', Error, 0 );
                Result := False;
           end;
     end;
end;

{ *********** Conciliaci�n ************ }


procedure TdmSUA2006.ConciliarListaMovimientos( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings );

var
   dFecha: TDate;
   sFecha: String;
   sTipo: string;
   lEncontrado,lValidaCambiosSalario: Boolean;


{$IFDEF FROMEX}
     procedure CampoEmpleadoGet( const sNombre,sCampo: String );
     const
         K_DIFERENTES = 'Diferentes';
     var
          oTress, oIMSS: TField;
          sTress, sIMSS, sDiferencia: String;
     begin
          oTress := Original.FieldByName( sCampo );
          oIMSS := Datos.FieldByName( sCampo );

          sDiferencia := VACIO;

          if Assigned( oTress ) and Assigned( oIMSS ) then
          begin
               sTress := GetCampoAsString( oTress );
               sIMSS := GetCampoAsString( oIMSS );
               if TienenMismoTipo( oTress, oIMSS ) then
               begin
                    case oTress.DataType of
                         ftString: sDiferencia := GetDiferencias( ( oTress.AsString = oIMSS.AsString ), sDiferencia );
                         ftSmallint: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                         ftInteger: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                         ftWord: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                         ftBoolean: sDiferencia := GetDiferencias( ( oTress.AsBoolean = oIMSS.AsBoolean ), sDiferencia );
                         ftFloat: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsFloat, oIMSS.AsFloat, FPrecision ), GetFloatAsStr( oTress.AsFloat - oIMSS.AsFloat ) );
                         ftCurrency: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsCurrency, oIMSS.AsCurrency, FPrecision ), GetFloatAsStr( oTress.AsCurrency - oIMSS.AsCurrency ) );
                         ftDate: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
                         ftDateTime: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
                    else
                        sDiferencia := 'YYY';
                    end;
               end
               else
                    sDiferencia := 'XXX';
          end
          else
          begin
               sDiferencia := VACIO;
          end;



          if lTress then
          begin
               AgregaRegistro( cdsDatosEmpleadosTodos, [ dmCliente.cdsLiqEmp.FieldByName('CB_SEGSOC').AsString,
                                                         PrettyName( dmCliente.cdsLiqEmp.FieldByName('CB_APE_PAT').AsString,
                                                         dmCliente.cdsLiqEmp.FieldByName('CB_APE_MAT').AsString,
                                                         dmCliente.cdsLiqEmp.FieldByName('CB_NOMBRES').AsString ),
                                                         dmCliente.cdsLiqEmp.FieldByName('CB_CODIGO').AsString,
                                                         sNombre,
                                                         oTress.AsString,
                                                         oImss.AsString,
                                                         '0.00' ] )
          end
          else
          begin
               AgregaRegistro( cdsDatosEmpleadosTodos, [ cdsEmpleados.FieldByName(K_NUM_SS).AsString,
                                                         cdsEmpleados.FieldByName('NOMBRE_SS').AsString,
                                                         '0',
                                                         sNombre,
                                                         oTress.AsString,
                                                         oImss.AsString,
                                                         '0.00' ] );
          end;
     end;

{$ENDIF}
 procedure CampoEmpleado( const sNombre,sCampo: String );
 const
     K_DIFERENTES = 'Diferentes';
 var
   oTress, oIMSS: TField;
   sTress, sIMSS, sDiferencia: String;
 begin
     oTress := Original.FieldByName( sCampo );
     oIMSS := Datos.FieldByName( sCampo );

     sDiferencia := VACIO;

     if Assigned( oTress ) and Assigned( oIMSS ) then
     begin
          sTress := GetCampoAsString( oTress );
          sIMSS := GetCampoAsString( oIMSS );
          if TienenMismoTipo( oTress, oIMSS ) then
          begin
               case oTress.DataType of
                    ftString: sDiferencia := GetDiferencias( ( oTress.AsString = oIMSS.AsString ), sDiferencia );
                    ftSmallint: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftInteger: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftWord: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftBoolean: sDiferencia := GetDiferencias( ( oTress.AsBoolean = oIMSS.AsBoolean ), sDiferencia );
                    ftFloat: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsFloat, oIMSS.AsFloat, FPrecision ), GetFloatAsStr( oTress.AsFloat - oIMSS.AsFloat ) );
                    ftCurrency: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsCurrency, oIMSS.AsCurrency, FPrecision ), GetFloatAsStr( oTress.AsCurrency - oIMSS.AsCurrency ) );
                    ftDate: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
                    ftDateTime: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
               else
                   sDiferencia := 'YYY';
               end;
          end
          else
               sDiferencia := 'XXX';
     end
     else
     begin
          sDiferencia := VACIO;
     end;

     if ( sDiferencia <> K_IGUALES ) and StrLleno(sDiferencia) then
     begin
          if lTress then
          begin
               AgregaRegistro( cdsDatosEmpleados, [ dmCliente.cdsLiqEmp.FieldByName('CB_SEGSOC').AsString,
                                                   PrettyName( dmCliente.cdsLiqEmp.FieldByName('CB_APE_PAT').AsString,
                                                               dmCliente.cdsLiqEmp.FieldByName('CB_APE_MAT').AsString,
                                                               dmCliente.cdsLiqEmp.FieldByName('CB_NOMBRES').AsString ),
                                                   dmCliente.cdsLiqEmp.FieldByName('CB_CODIGO').AsString,
                                                   sNombre,oTress.AsString,oImss.AsString,sDiferencia ] )
          end
          else
          begin
               AgregaRegistro( cdsDatosEmpleados, [ cdsEmpleados.FieldByName(K_NUM_SS).AsString,
                                                   cdsEmpleados.FieldByName('NOMBRE_SS').AsString,
                                                   '0',
                                                   sNombre,oTress.AsString,oImss.AsString,sDiferencia ] );
          end;
     end;

 end;


begin
     {Si se hace opcional esta validacion, esta variable va en funcion al control que se ponga en interfase de usuario}
     lValidaCambiosSalario := TRUE;

     with Original do
     begin                                                    
          case Clase of                                       
               eeCMensual:
               begin
                    FieldByName( 'LM_EYM_FIJ' ).Tag := 1;
                    FieldByName( 'LM_EYM_EXC' ).Tag := 1;
                    FieldByName( 'LM_EYM_DIN' ).Tag := 1;
                    FieldByName( 'LM_EYM_ESP' ).Tag := 1;
                    FieldByName( 'LM_INV_VID' ).Tag := 1;
                    FieldByName( 'LM_RIESGOS' ).Tag := 1;     
                    FieldByName( 'LM_GUARDER' ).Tag := 1;
                    FieldByName( 'LM_EYMEXCP' ).Tag := 1;
                    FieldByName( 'LM_EYMDINP' ).Tag := 1;     
                    FieldByName( 'LM_EYMESPP' ).Tag := 1;
                    FieldByName( 'LM_INVVIDP' ).Tag := 1;

               end;
               eeCBimestral:
               begin
                    FieldByName( 'LM_RETIRO' ).Tag := 1;
                    FieldByName( 'LM_CES_VEJ').Tag := 1;     
                    FieldByName( 'LM_CESVEJP').Tag := 1;
                    FieldByName( 'LM_INF_PAT').Tag := 1;
                    FieldByName( 'LM_INF_AMO').Tag := 1;
               end;
          end;

          FieldByName( 'LM_DIAS' ).Tag := 1;
          FieldByName( 'LM_BASE' ).Tag := 1;
          FieldByName( 'LM_INCAPAC' ).Tag := 1;
          FieldByName( 'LM_AUSENCI' ).Tag := 1;



          First;
          while not Eof do
          begin
               dFecha := FieldByName( 'LM_FECHA' ).AsDateTime;
               sFecha := ZetaCommonTools.FechaAsStr( dFecha );
               sTipo := FieldByName( K_TIPO_MOV ).AsString;
               if lTress then sTipo := PadLCar( sTipo,2, '0' );
                  
               lEncontrado := Datos.Locate( Format( 'LM_FECHA;%s', [ K_TIPO_MOV ] ), VarArrayOf( [ sFecha, sTipo ] ), [] );

               if lTress AND lValidaCambiosSalario AND (NOT lEncontrado) AND (sTipo = K_MAESTRA_EAMP_CAMBIO_COTIZACION) then
               begin
                    lEncontrado := Datos.Locate( Format( 'LM_FECHA;%s', [ K_TIPO_MOV ] ), VarArrayOf( [ sFecha, K_MAESTRA_INICIAL_TRESS ] ), [] );
               end;

               if lEncontrado then
               begin
{$ifdef FROMEX}
                 case Clase of
                              eeCMensual:
                              begin
                                   CampoEmpleadoGet(  K_CUOTA_FIJA, 'LM_EYM_FIJ' );
                                   CampoEmpleadoGet(  K_Excedente, 'LM_EYM_EXC' );
                                   CampoEmpleadoGet(  K_Dinero, 'LM_EYM_DIN' );
                                   CampoEmpleadoGet(  K_Especie , 'LM_EYM_ESP' );
                                   CampoEmpleadoGet(  K_Riesgos , 'LM_RIESGOS' );
                                   CampoEmpleadoGet(  K_Invalidez , 'LM_INV_VID' );
                                   CampoEmpleadoGet(  K_Guarderias, 'LM_GUARDER' );
                                   CampoEmpleadoGet(  K_ExcedenteP, 'LM_EYMEXCP' );
                                   CampoEmpleadoGet(  K_DineroP, 'LM_EYMDINP' );
                                   CampoEmpleadoGet(  K_EspecieP, 'LM_EYMESPP' );
                                   CampoEmpleadoGet(  K_InvalidezP, 'LM_INVVIDP' );
                              end;
                              eeCBimestral:
                              begin
                                   CampoEmpleadoGet( K_RETIRO, 'LM_RETIRO' );
                                   CampoEmpleadoGet( K_Cesantia, 'LM_CES_VEJ'  );
                                   CampoEmpleadoGet( K_CesantiaP, 'LM_CESVEJP' );
                                   CampoEmpleadoGet( K_Infonavit, 'LM_INF_PAT'  );
                                   CampoEmpleadoGet( k_amortiza, 'LM_INF_AMO' );
                              end;
                         end;
{$endif}

                    if CamposSonDiferentes( Original, Datos ) then
                    begin
                         SetDiferencias;
                         AddMovimiento( Original, lTress );
                         AddMovimiento( Datos, not lTress );

                         CampoEmpleado( K_Dias, 'LM_DIAS' );
                         CampoEmpleado( K_Integrado, 'LM_BASE' );
                         CampoEmpleado( K_Incapacidad, 'LM_INCAPAC' );
                         CampoEmpleado( K_Ausentismo, 'LM_AUSENCI' );

                         case Clase of
                              eeCMensual:
                              begin
                                   CampoEmpleado(  K_CUOTA_FIJA, 'LM_EYM_FIJ' );
                                   CampoEmpleado(  K_Excedente, 'LM_EYM_EXC' );
                                   CampoEmpleado(  K_Dinero, 'LM_EYM_DIN' );
                                   CampoEmpleado(  K_Especie , 'LM_EYM_ESP' );
                                   CampoEmpleado(  K_Riesgos , 'LM_RIESGOS' );
                                   CampoEmpleado(  K_Invalidez , 'LM_INV_VID' );
                                   CampoEmpleado(  K_Guarderias, 'LM_GUARDER' );
                                   CampoEmpleado(  K_ExcedenteP, 'LM_EYMEXCP' );
                                   CampoEmpleado(  K_DineroP, 'LM_EYMDINP' );
                                   CampoEmpleado(  K_EspecieP, 'LM_EYMESPP' );
                                   CampoEmpleado(  K_InvalidezP, 'LM_INVVIDP' );
                              end;
                              eeCBimestral:
                              begin
                                   CampoEmpleado( K_RETIRO, 'LM_RETIRO' );
                                   CampoEmpleado( K_Cesantia, 'LM_CES_VEJ'  );
                                   CampoEmpleado( K_CesantiaP, 'LM_CESVEJP' );
                                   CampoEmpleado( K_Infonavit, 'LM_INF_PAT'  );
                                   CampoEmpleado( k_amortiza, 'LM_INF_AMO' );
                              end;
                         end;
                    end;
                    { Evita que se vuelva a procesar }
                    Datos.Delete;
               end
               else
               begin
                    SetDiferencias;
                    AddMovimiento( Original, lTress );

                    if lTress then
                       RegistraBitacora( cdsMovtosTress, Original, lTress )
                    else
                        RegistraBitacora( cdsMovtosImss, Original, lTress );

               end;
               Next;
          end;
     end;
end;



procedure TdmSUA2006.ConciliarMovimientos( const iEmpleado: TNumEmp; const sAfiliacion: String; Log: TStrings  );
begin
     if dmCliente.ConectarMovimientos( Clase, iEmpleado ) and ConectarMovimiento( sAfiliacion ) then
     begin
          ConciliarListaMovimientos( dmCliente.cdsLiqMov, cdsMovimientos, True, Log );
          ConciliarListaMovimientos( cdsMovimientos, dmCliente.cdsLiqMov, False, Log );
     end;
end;

procedure TdmSUA2006.ConciliarListaEmpleados( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings  );
var
   sSeguroSocial: String;
   iEmpleado: TNumEmp;

 procedure CampoEmpleado( const sNombre, sCampo: String );
 begin
      ConciliarCampoEmpleado( sNOmbre, sCampo, FEmpleados, cdsDatosEmpleados, FALSE );
 end;
   
begin
     FEmpNotFound.Clear;
     with Original do
     begin
          First;
          while not Eof and TressShell.ConciliarStep do
          begin
               if ( FieldByName( K_FOUND ).AsInteger = 0 ) then
               begin
                    sSeguroSocial := FieldByName( K_NUM_SS ).AsString;
                    if Datos.Locate( K_NUM_SS, sSeguroSocial, [] ) then
                    begin
                         iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                         SetEmployeeFound( Original );
                         SetEmployeeFound( Datos );

                         ConciliarCampoEmpleado( 'M�dulo', 'TB_MODULO', FEmpleados, cdsDatosEmpleados );
                         ConciliarCampoEmpleado( 'Jornada', 'TU_TIP_JOR', FEmpleados, cdsDatosEmpleados );
                         ConciliarCampoEmpleado( 'CURP', 'CB_CURP', FEmpleados, cdsCurp );
                         ConciliarCampoEmpleado( 'RFC', 'CB_RFC', FEmpleados, cdsCurp );

                         case Clase of
                              eeCBimestral:
                              begin
//                                   if ( FieldByName( 'CB_CODIGO' ).AsInteger = 258 ) then
//                                      ShowMessage('cosa');
                                   ConciliarCampoEmpleado( K_NUMERO_CREDITO, 'CB_INFCRED', FEmpleados, cdsInfonavit, Ord( DifInfNoCredito ));
                                   ConciliarCampoEmpleado( K_TIPO_DESCUENTO, 'CB_INFTIPO', FEmpleados, cdsInfonavit, Ord( DifInfTipoDescuento) );
                                   ConciliarCampoEmpleado( K_VALOR_DESCUENTO, 'CB_INFTASA', FEmpleados, cdsInfonavit, Ord( DifInfValorDesc ) );
                                   ConciliarCampoEmpleado( K_INICIO_CREDITO , 'CB_INF_INI', FEmpleados, cdsInfonavit, Ord( DifInfOtorgamiento ) );

                              end;
                         end;

                         if ( iEmpleado > 0 ) then
                         begin
                              ConciliarMovimientos( iEmpleado, sSeguroSocial, FMovimientos );
                         end;
                    end
                    else
                    begin
                         SetDiferencias;
                         if lTress then
                            AgregaRegistro( cdsEmpleadosTress, [ sSeguroSocial,
                                                                 PrettyName( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                             FieldByName( 'CB_APE_MAT' ).AsString,
                                                                             FieldByName( 'CB_NOMBRES' ).AsString),
                                                                FieldByName( 'CB_CODIGO' ).AsString ] )
                         else
                             AgregaRegistro( cdsEmpleadosIMSS, [ sSeguroSocial, FieldByName( 'NOMBRE_SS' ).AsString ] )
                    end;
               end;
               Next;
          end;
     end;
end;

procedure TdmSUA2006.ConciliarEmpleados;
var
   {Se requiere por compatibilidad para las otras conciliaciones}
   FDatos: TStrings;
begin
     FDatos := TStringList.Create;
     try
        ConciliarListaEmpleados( dmCliente.cdsLiqEmp, cdsEmpleados, True, FDatos );
        ConciliarListaEmpleados( cdsEmpleados, dmCliente.cdsLiqEmp, False, FDatos );
        if ( Clase = eecBimestral )  then
        begin
             SetCreditosNoActivos(cdsInactivosTress, ImssSITressNO {TRUE});
             SetCreditosNoActivos(cdsInactivosImss, ImssNOTressSI{FALSE});
             SetCreditosNoActivos(cdsSuspendidosTress,  ImssSITressSuspendido );
        end;
     finally
            FreeAndNil( FDatos );
     end;
end;

procedure TdmSUA2006.ConciliarPatrones;
begin
     case Clase of
          eeCMensual:
          begin
               ConciliarCampoPatron( 'Prima de Riesgo', 'PRIMA_RT' );
               ConciliarCampoPatron( 'EYM: Cuota Fija', 'LS_EYM_FIJ' );    
               ConciliarCampoPatron( 'EYM: Excedente', 'LS_EYM_EXC' );     
               ConciliarCampoPatron( 'EYM: Dinero', 'LS_EYM_DIN' );        
               ConciliarCampoPatron( 'EYM: Especie', 'LS_EYM_ESP' );       
               ConciliarCampoPatron( 'Guarder�as', 'LS_GUARDER' );         
               ConciliarCampoPatron( 'Riesgos de Trabajo', 'LS_RIESGOS' ); 
               ConciliarCampoPatron( 'Invalidez y Vida', 'LS_INV_VID' );   
{
               CV/ER ( 20/Feb/2006 ) : SUA 2006 no almacena estos datos en ninguna tabla
               ConciliarCampoPatron( 'Asegurados', 'LS_NUM_TRA' );
               ConciliarCampoPatron( 'D�as Cotizados', 'LS_DIAS_CO' );
}
          end;
          eeCBimestral:
          begin
               ConciliarCampoPatron( 'Retiro', 'LS_RETIRO' );
               ConciliarCampoPatron( 'Cesant�a y Vejez', 'LS_CES_VEJ' );
               ConciliarCampoPatron( 'No. Acreditados', 'LS_INF_NAC' );
               ConciliarCampoPatron( 'Acreditados', 'LS_INF_ACR' );
               ConciliarCampoPatron( 'Amortizaciones', 'LS_INF_AMO' );
               ConciliarCampoPatron( 'Acreditados Bimestre', 'LS_INF_NUM' );
{
               CV/ER ( 20/Feb/2006 ) : SUA 2006 no almacena estos datos en ninguna tabla
               ConciliarCampoPatron( 'D�as Cotizados', 'LS_DIAS_BM' );
}
          end;                                                               
     end;
     TressShell.ConciliarStep;
end;

procedure TdmSUA2006.ResetBitacoras;
begin
     inherited;
     cdsDatosEmpleados.Filtered := FALSE;
     cdsDatosEmpleados.Filter := VACIO;
     {cdsMontosTotales.EmptyDataset;
     cdsEmpleadosTress.EmptyDataSet;
     cdsEmpleadosIMSS.EmptyDataSet;
     cdsDatosEmpleados.EmptyDataset;
     cdsCurp.EmptyDataset;
     cdsInfonavit.EmptyDataset;
     cdsMovtosTress.EmptyDataset;
     cdsMovtosImss.EmptyDataset;   }
end;

procedure TdmSUA2006.Conciliar;
var
   iSteps: Integer;
begin
     FDiferencias := 0;
     {
     iSteps := cdsEmpleados.RecordCount + dmCliente.cdsLiqEmp.RecordCount + 1;
     }
     //iSteps := 100;
     iSteps := cdsEmpleados.RecordCount;

     TressShell.ConciliarStart( iSteps );
     try
        ResetBitacoras;
        ConciliarPatrones;
        ConciliarEmpleados;
        FinishBitacoras;
     finally
            if TressShell.CancelaConciliacion then
            begin
                 ListaEspacio;
                 ListaFiller;
                 ListaFiller( 'Proceso Cancelado por el Usuario' );
                 ListaFiller;
            end;
            TressShell.ConciliarEnd;
     end;
end;


function TdmSUA2006.GetConciliaDescripcion: string;
begin
     Result := 'SUA';
end;

end.
