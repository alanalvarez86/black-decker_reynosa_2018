unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}


uses

  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseShell, ZBasicoNavBarShell, ZetaCommonClasses,
  cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  Vcl.ExtCtrls, dxStatusBar, dxRibbonStatusBar, Vcl.ComCtrls, Vcl.StdCtrls,
  dxSkinsdxNavBarPainter, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  dxSkinsdxBarPainter, dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbon, dxBar,
  dxSkinsForm, cxPC, cxContainer, cxEdit, cxTreeView, dxNavBarCollns, cxClasses,
  dxNavBarBase, dxNavBar, Vcl.Menus, cxLocalization, Vcl.ImgList,
  System.Actions, Vcl.ActnList, cxButtons, cxStyles, ZetaEdit_DevEx, cxTextEdit,
  HtmlHelpViewer, Data.DB, Vcl.Grids, Vcl.DBGrids, cxProgressBar,
  DBClient,
  FileCtrl,
  FAutoClasses,
  DCliente,
  ZetaServerDataSet, Vcl.Mask, ZetaNumero,
  ZetaFecha, ZetaEdit,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, cxDBData,
  cxGridLevel, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid,
  DConciliador,
  ZetaClientDataSet, dxBarBuiltInMenu, dxRibbonCustomizationForm;

type
  TTressShell = class(TBasicoNavBarShell)
    TabConciliacion: TdxRibbonTab;
    CConciliar: TdxBar;
    CConciliar_btnConciliarSUA: TdxBarLargeButton;
    CINFONAVIT: TdxBar;
    CINFONAVIT_btnActualizaDatosINFONAVIT: TdxBarLargeButton;
    CINFONAVIT_btnImportarCreditos: TdxBarLargeButton;
    CINFONAVIT_btnImportarSuspensiones: TdxBarLargeButton;
    CINFONAVIT_btnImportarReinicioDescuento: TdxBarLargeButton;
    CConciliar_btnConciliarIMSS: TdxBarLargeButton;
    PatronLBL: TLabel;
    MesLBL: TLabel;
    TipoLBL: TLabel;
    YearLBL: TLabel;
    LLavePatron: TcxTextEdit;
    Patron: TcxTextEdit;
    Mes: TcxTextEdit;
    Tipo: TcxTextEdit;
    Year: TcxTextEdit;
    TipoConciliacionLBL: TLabel;
    TipoConciliacion: TcxTextEdit;
    cxStyleValoresActivos: TcxStyle;
    EOperaciones_btnExportarTexto: TdxBarLargeButton;


    dsMontosTotales: TDataSource;
    dsMovtosImss: TDataSource;
    dsEmpleadosTress: TDataSource;
    dsDatosEmpleados: TDataSource;
    dsEmpleadosIMSS: TDataSource;
    dsCurp: TDataSource;
    dsInfonavit: TDataSource;
    dsMovtosTress: TDataSource;
    dsDatosEmpleadosTodos: TDataSource;
    dsCredSuspendidosTress: TDataSource;
    dsCreditosInactivosImss: TDataSource;
    dsCredNoActivosTress: TDataSource;
    _C_ConciliarIMSS: TAction;
    ePrecision: TZetaNumero;
    Emision: TcxTextEdit;
    EmisionLBL: TLabel;
    SaveDialog: TSaveDialog;
    _C_ActualizarDatosINFONAVIT: TAction;
    _C_ImportarCreditos: TAction;
    _C_ImportarSuspensiones: TAction;
    _C_ImportarReinicioDescuento: TAction;


    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure ConciliarProceso( iYear, iMes, iTipo: Integer; sPatron, sRuta :String;
              TipoConciliacion : eTipoConciliacion; ClaseEmision: eEmisionClase);
    function ConciliarPorClaseEmision(iYear, iMes, iTipo: Integer; sPatron, sRuta: String;
             TipoConciliacion : eTipoConciliacion; ClaseEmision: eEmisionClase): Boolean;
    procedure _C_ConciliarIMSSExecute(Sender: TObject);
    procedure _C_ConciliarSUAExecute(Sender: TObject);
    procedure _E_ExportarTextoExecute(Sender: TObject);
    procedure ClientAreaPG_DevExChange(Sender: TObject);
    procedure _C_ActualizarDatosINFONAVITExecute(Sender: TObject);
    procedure _C_ImportarCreditosExecute(Sender: TObject);
    procedure _C_ImportarSuspensionesExecute(Sender: TObject);
    procedure _C_ImportarReinicioDescuentoExecute(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure _HayFormaAbierta(Sender: TObject);

  private
    { Private declarations }
    Resultados: TStrings;
    FCancelaConciliacion : Boolean;
    FConciliacionTerminada : Boolean;
    ProgressBar: TcxProgressBar;

    
    FDiferenciasFiltro: Integer;
    FDiferenciasFiltrarFecha: Boolean;
    FDiferenciasFechaFiltro: TDate;


    function EsFormaINFONAVIT( Clase: TClass ): Boolean;
    function GetConciliaDesc: string;
    function GetLiquidacionDesc: string;
  protected
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure DoOpenSistema; override;
    procedure CargaTraducciones; override;
    procedure HabilitarBotonesRibbon( TipoConciliacion: eTipoConciliacion; RealizoConciliacion: Boolean; const EsEmisionBimestral: Boolean = False);

     
  public
    { Public declarations }
    {$IFDEF FROMEX}
    ResultadosMontosSUA: TStrings;
    {$ENDIF}
    procedure RefrescaEmpleado;
    procedure CargaEmpleadoActivos;
    procedure CambiaPeriodoActivos;
    procedure CambiaSistemaActivos;
    procedure CambiaEmpleadoActivos;
    procedure LLenaOpcionesGrupos; override;
    procedure RefrescaConfiguracionActivos;
    function ConciliarStep: Boolean;
    property CancelaConciliacion: Boolean read FCancelaConciliacion;
    property ConciliacionTerminada: Boolean read FConciliacionTerminada write FConciliacionTerminada;
    procedure ConciliarStart(const iMaxSteps: Integer);
    procedure ConciliarEnd;
    function GetEncabezadoForma: string;
    procedure SetProgressbar( BarraProgreso: TcxProgressBar);
    property DiferenciasFiltro: Integer read FDiferenciasFiltro write FDiferenciasFiltro;
    property DiferenciasFiltrarFecha: Boolean read FDiferenciasFiltrarFecha write FDiferenciasFiltrarFecha;
    property DiferenciasFechaFiltro: TDate read FDiferenciasFechaFiltro write FDiferenciasFechaFiltro;
    {$IFDEF FROMEX}
    procedure GenerarResultadosMontosSUA;
   {$ENDIF}

  end;


  const
     K_PANEL_BASE_DATOS_SUA = 1;
     K_PANEL_DIRECTORIO_IMSS = 2;
     K_ETIQUETA_SUA = 'SUA: ';
     K_ETIQUETA_IMSS = 'IMSS: ';

     K_SUA_ACCESS_CODE = 'S5@N52V49'; //Apartir del complemento 3.2.5 cambio la clave de 'NUEVOSUA' a 'S5@N52V49'


     K_CARDINAL_ESPANOL = 2058;  // Cardinal para Espanol (Mexico)
  var
     TressShell: TTressShell;

implementation

{$R *.dfm}

 uses
     ZAccesosMgr,
     ZetaDialogo,
     ZetaRegistryCliente,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaClientTools,
     ZAccesosTress,
     DDiccionario,
     DGlobal,
     DCDIMSS,
     DSUA2006,
     ZcxWizardBasico,
     ZetaDespConsulta,
     FDiferenciasTotales,
     FCreditosSuspendidosTRESS,
     FDiferenciasDatosINFONAVIT,
     FCreditosNoRegistradosTRESS,
     {$IFDEF FROMEX}
      FEditMontosConceptos_DevEx,
     {$ENDIF}
     FWizIMSSConciliarIMSS,
     FWizIMSSConciliarSUA,
     FWizIMSSActualizarDatosINFONAVIT,
     FWizIMSSImportarCreditos,
     FWizIMSSImportarSuspensiones,
     FWizIMSSImportarReinicioDescuento;



procedure TTressShell.FormClick(Sender: TObject);
begin
     inherited;
     FCancelaConciliacion := TRUE;
end;

procedure TTressShell.FormCreate(Sender: TObject);
const
     K_IMAGE_INFO = 1;
     K_SUA_ACCESS_CODE = 'S5@N52V49'; //Apartir del complemento 3.2.5 cambio la clave de 'NUEVOSUA' a 'S5@N52V49'
 var
    i: integer;

begin
     dmCliente := TdmCliente.Create( Self );
     //dmCDIMSS := TdmCDIMSS.Create( Self );
     //dmSUA := TdmSUA.Create( Self )
	   // dmSUA2006 := TdmSUA2006.Create( Self );
     dmDiccionario := TdmDiccionario.Create( self );
	    Global := TdmGlobal.Create;
     Resultados := TStringList.Create;
{$IFDEF FROMEX}
     ResultadosMontosSUA := TStringList.Create;     
{$ENDIF}

     inherited;

     FHelpGlosario := Application.HelpFile;
     HelpContext := H_CONCIL_COMENZAR;
     ConciliacionTerminada := False;

     FDiferenciasFiltro := 0;
     FDiferenciasFiltrarFecha := False;
     FDiferenciasFechaFiltro := 0;
end;


procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;

     FreeAndNil( Resultados );

     FreeAndNil( Global );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmSUA2006 );
     //**FreeAndNil( dmSUA );
      FreeAndNil( dmCDIMSS );
     FreeAndNil( dmCliente );
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
  inherited;
  CargaTraducciones;
  HelpContext := H_CONCIL_COMENZAR;
  DevEx_ShellRibbon.ActiveTab := TabConciliacion;
  EOperaciones_btnExportarTexto.Enabled := False;

end;

procedure TTressShell.DoOpenAll;
begin
     try
       inherited DoOpenAll;
        SetArbolitosLength(12);
        CreaNavBar;
        LLenaOpcionesGrupos;

         with dmCliente do
         begin
              GetIMSSInicial;
         end;
     RefrescaConfiguracionActivos;
     dmCDIMSS := TdmCDIMSS.Create( Self );
     dmSUA2006 := TdmSUA2006.Create( Self );
     Global.Conectar;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
                DoCloseAll;
           end;
     end;
     ConciliacionTerminada := False;
     HabilitarBotonesRibbon( tcIDSE, False);
end;


procedure TTressShell._E_ExportarTextoExecute(Sender: TObject);
  var
   sFileName: String;
   oCursor: TCursor;
begin
     inherited;
     sFileName := Format( '%sP%s-%d-%d-%d%d.txt', [ ExtractFilePath( Application.ExeName ), ClientRegistry.ConciliadorIMSSPatron,
                          ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSYear), ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSMes),
                          ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSTipo),
                          ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSClaseEmision)] );

     with SaveDialog do
     begin
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
          begin
               sFileName := FileName;
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  Resultados.SaveToFile( sFileName );

               finally
                      Screen.Cursor := oCursor;
               end;
               if ZetaDialogo.zConfirm( Caption, 'El archivo ' + sFileName + ' fu� creado' + CR_LF + '� Desea verlo ?', 0, mbYes ) then
               begin
                    ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sFileName, ExtractFileDir( sFileName ), SW_SHOWDEFAULT );
               end;
          end;
     end;
end;

procedure TTressShell._HayFormaAbierta(Sender: TObject);
begin
     inherited;
     TabArchivo_btnImprimir.Enabled := ConciliacionTerminada;
     EOperaciones_btnExportar.Enabled := ConciliacionTerminada;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          CierraDatasets(dmSUA2006);
          CierraDatasets(dmCDIMSS);
     end;
     inherited DoCloseAll;
end;

procedure TTressShell._C_ConciliarIMSSExecute(Sender: TObject);
begin
     inherited;
     dmCliente.AnaliticaContabilizarUso('ConciliadorIMSS', H_CONCIL_IMSS , 1);
     ZcxWizardBasico.ShowWizard(TWizIMSSConciliarIMSS);
end;

procedure TTressShell._C_ConciliarSUAExecute(Sender: TObject);
begin
     inherited;
     dmCliente.AnaliticaContabilizarUso('ConciliadorIMSS', H_CONCIL_SUA , 1);
     ZcxWizardBasico.ShowWizard(TWizIMSSConciliarSUA);
end;

procedure TTressShell._C_ActualizarDatosINFONAVITExecute(Sender: TObject);
begin
     inherited;
     //si no hay registros a procesar no abrir el wizard
     if ( TressShell.dsInfonavit.DataSet.IsEmpty ) then
          ZetaDialogo.ZError( 'Actualizar datos de INFONAVIT', 'No se encontraron diferencias entre Tress e INFONAVIT', 0 )
     else
     begin
          with TDiferenciasDatosINFONAVIT (FormaActiva) do
          begin
               FWizIMSSActualizarDatosINFONAVIT.ShowWizard( DiferenciasFiltro,
                                                            DiferenciasFiltrarFecha, DiferenciasFechaFiltro, TClientDataSet(TressShell.dsInfonavit.DataSet) );
          end;

     end;
end;

procedure TTressShell._C_ImportarCreditosExecute(Sender: TObject);
begin
     inherited;
     if ( TressShell.dsCredNoActivosTress.DataSet.IsEmpty ) then
         ZetaDialogo.zInformation( 'Importar cr�ditos', 'No se encontraron cr�ditos a importar', 0 )
     else
         FWizIMSSImportarCreditos.ShowWizard( TClientDataSet(TressShell.dsCredNoActivosTress.DataSet) );
end;

procedure TTressShell._C_ImportarReinicioDescuentoExecute(Sender: TObject);
begin
     inherited;
     //si no hay registros a procesar no abrir el wizard
     if ( TressShell.dsCredSuspendidosTress.DataSet.IsEmpty ) then
        ZetaDialogo.ZError( 'Importar reinicio de descuento', 'No se encontraron reinicios de descuentos de cr�ditos a importar', 0 )
     else
         FWizIMSSImportarReinicioDescuento.ShowWizard( TClientDataSet(TressShell.dsCredSuspendidosTress.DataSet) );
end;

procedure TTressShell._C_ImportarSuspensionesExecute(Sender: TObject);
begin
     inherited;
     if ( TressShell.dsCreditosInactivosImss.DataSet.IsEmpty ) then
        ZetaDialogo.ZError( 'Importar suspensiones de cr�ditos INFONAVIT', 'No se encontraron suspensiones de descuentos de cr�ditos a importar', 0 )
     else
         FWizIMSSImportarSuspensiones.ShowWizard( TClientDataSet(TressShell.dsCreditosInactivosImss.DataSet) );
end;

procedure TTressShell.RefrescaConfiguracionActivos;
var
   sDescrip : String;
begin
     StatusBarMsg( K_ETIQUETA_SUA + ClientRegistry.ConciliadorIMSSBaseDatosSUA,  K_PANEL_BASE_DATOS_SUA );
     StatusBarMsg( K_ETIQUETA_IMSS + ClientRegistry.ConciliadorIMSSDirectorioIMSS, K_PANEL_DIRECTORIO_IMSS);

     LLavePatron.Text := ClientRegistry.ConciliadorIMSSPatron;
     Patron.Text := dmCliente.cdsRPatron.GetDescripcion(LLavePatron.Text);
     Tipo.Text := ClientRegistry.ConciliadorIMSSTipo;
     if ( Tipo.Text <> '') then
        Tipo.Text := ObtieneElemento( lfTipoLiqIMSS, ZetaCommonTools.StrAsInteger((Tipo.Text)));
     Year.Text := ClientRegistry.ConciliadorIMSSYear;
     Mes.Text := ClientRegistry.ConciliadorIMSSMes;
     if ( Mes.Text <> '' ) then
        Mes.Text := ObtieneElemento( lfMeses, ZetaCommonTools.StrAsInteger((Mes.Text)) - 1 ) ;
     TipoConciliacion.Text := ClientRegistry.ConciliadorIMSSTipoConciliacion;
     if ( TipoConciliacion.Text <> '' ) then
        TipoConciliacion.Text := aTipoConciliacion [ eTipoConciliacion( ZetaCommonTools.StrAsInteger(TipoConciliacion.Text) ) ];
     Emision.Text := Trim(ClientRegistry.ConciliadorIMSSClaseEmision);
     if (Emision.Text <> '' ) then
        Emision.Text := aEmisionClase [ eEmisionClase( ZetaCommonTools.StrAsInteger(Emision.Text) ) ];

end;


procedure TTressShell.RefrescaEmpleado;
begin

end;


//DevEx (by am): Este metodo solo se ejecuta en el DoOpenAll asi que cargamos los combos de ambas vistas.
procedure TTressShell.CargaEmpleadoActivos;
begin

end;


procedure TTressShell.CambiaPeriodoActivos;
begin

end;

procedure TTressShell.CambiaSistemaActivos;
begin

end;



procedure TTressShell.CambiaEmpleadoActivos;
begin

end;

procedure TTressShell.LLenaOpcionesGrupos;
var
   iIndice : Integer;

begin
     if ( dmCliente.EmpresaAbierta ) then
     begin
          // @DChavez : Cuando no tiene derecho se asigna los iconos que corresponden al navbar por el index fijo en la lista
          if( ( DevEx_NavBar.Groups.Count = 1 ) and ( DevEx_NavBar.Groups[0].Caption = 'No tiene derechos'  ) )   then
          begin
               DevEx_NavBar.Groups[0].LargeImageIndex := 3;
               DevEx_NavBar.Groups[0].SmallImageIndex := 3;
               DevEx_NavBar.Groups[0].UseSmallImages := False;
          end
          else
              //recorre los grupos del navbar, le asigna su imagen por su nombre
              for iIndice := 0 to (DevEx_NavBar.Groups.Count - 1)  do
              begin
                   DevEx_NavBar.Groups[iIndice].LargeImageIndex := iIndice;
                   DevEx_NavBar.Groups[iIndice].SmallImageIndex := iIndice;
                   //para que use las imagenes grandes
                   DevEx_NavBar.Groups[iIndice].UseSmallImages := False;
              end;
     end;
end;



procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := K_CARDINAL_ESPANOL;  // Cardinal para Espanol (Mexico)
end;


procedure TTressShell.ClientAreaPG_DevExChange(Sender: TObject);
var iControles: integer;
begin
     inherited;
     //activar el tab de conciliacion para los procesos de INFONAVIT
     try
        if ( ClientAreaPG_DevEx.ActivePage <> nil ) then
        begin
             for  iControles:= 0 to (ClientAreaPG_DevEx.ActivePage.ControlCount - 1 )  do
             begin
                  if EsFormaINFONAVIT( ClientAreaPG_DevEx.ActivePage.Controls[iControles].ClassType ) then
                  begin
                       DevEx_ShellRibbon.ActiveTab := TabConciliacion;
                  end;
             end;
        end;
    except
    end;

end;

function TTressShell.EsFormaINFONAVIT( Clase: TClass ): Boolean;
begin
     Result := False;
     if ( Clase = TCreditosSuspendidosTRESS ) or ( Clase = TCreditosNoRegistradosTRESS ) or ( Clase = TDiferenciasDatosINFONAVIT ) then
        Result:= True
     else
         Result := False;
end;


procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
      _V_Cerrar.Execute;
end;


procedure TTressShell.ConciliarProceso(iYear, iMes, iTipo: Integer; sPatron, sRuta: String;
          TipoConciliacion : eTipoConciliacion; ClaseEmision: eEmisionClase);
var
   bFinalizo: Boolean;
begin
     bFinalizo := False;
     ConciliacionTerminada := False;
     try
          CierraFormaTodas;
          bFinalizo := ConciliarPorClaseEmision(iYear, iMes, iTipo, sPatron, sRuta, TipoConciliacion, ClaseEmision);
      except
           on Error: Exception do
           begin
                 ZetaDialogo.zInformation( '', Error.Message, 0);
           end;
     end;
     if (bFinalizo) then
     begin
          //guarda valores de ejecucion en el Registry
          try
                  ClientRegistry.ConciliadorIMSSYear := IntToStr( iYear );
                  ClientRegistry.ConciliadorIMSSMes := IntToStr( iMes );
                  ClientRegistry.ConciliadorIMSSTipo := IntToStr( iTipo );
                  ClientRegistry.ConciliadorIMSSTipoConciliacion :=  IntToStr( Ord(TipoConciliacion) );
                  ClientRegistry.ConciliadorIMSSPatron := sPatron;
                  if ( ( TipoConciliacion = tcIDSE ) or ( TipoConciliacion = tcCDImss )) then
                  begin
                       ClientRegistry.ConciliadorIMSSDirectorioIMSS := sRuta;
                  end;
                  if ( TipoConciliacion = ( tcSUA2006))  then
                  begin
                       ClientRegistry.ConciliadorIMSSBaseDatosSUA := sRuta;
                  end;
                  ClientRegistry.ConciliadorIMSSClaseEmision := IntToStr( Ord(ClaseEmision) );

          except
                on Error: Exception do
                begin
                     ZetaDialogo.zInformation( '', 'No se tienen los permisos requeridos para guardar la configuraci�n '
                                               + 'de la conciliaci�n, firmarse con otro usuario.', 0 );
                end;
          end;
          //refresca valores activos en shell
          RefrescaConfiguracionActivos;
          AbreFormaConsulta( efcDiferenciasTotales );
          ConciliacionTerminada := True;
          //si es conciliacion mensual limpia los datasets que se usan en las formas de INFONAVIT
          if ClaseEmision = eeCMensual then
          begin
               DevEx_NavBar.ActiveGroupIndex := 0;
               DevEx_NavBar.Groups[1].Visible := False;
          end
          else
          begin
               DevEx_NavBar.Groups[1].Visible := True;
          end;

     end;
end;

function TTressShell.ConciliarPorClaseEmision(iYear, iMes, iTipo: Integer; sPatron, sRuta: String;
         TipoConciliacion : eTipoConciliacion; ClaseEmision: eEmisionClase): Boolean;
   procedure SetReadOnlyDatasets( const lReadOnly : Boolean );
      var
         i :integer;
     begin
          for i:= 0 to ComponentCount - 1 do
          begin
{$IFDEF FROMEX}
               try
{$ENDIF}
                  if Components[i] is TDbGrid then
                     TClientDataset( TDbGrid( Components[i] ).DataSource.Dataset ).ReadOnly := lReadOnly;
{$IFDEF FROMEX}
               except
               end;
{$ENDIF}
          end;
     end;

const
     Alinea: array[ FALSE..TRUE ] of TAlign = ( alNone, alClient );
     K_PASOS = 5000;
var
     oCursor: TCursor;
     lOk, lModoDemo: Boolean;
     i: integer;
     sBaseDeDatos: string;
     dmConcilia: TdmConciliador;
begin
     inherited;
     Result := False;  //inicializar bandera que indica si el proceso termino
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     FCancelaConciliacion := FALSE;
     try
        lModoDemo := Autorizacion.EsDemo;
        case TipoConciliacion of
             tcSUA:
             begin
                  //**  dmConcilia := dmSUA;
                  //**  sBaseDeDatos := DBSUAAccess.Text;
             end;
             tcSUA2006:
             begin
                   dmConcilia := dmSUA2006;
                   sBaseDeDatos := sRuta;
             end;
             tcCDImss, tcIDSE:
             begin
                   dmConcilia := dmCDImss;
                   sBaseDeDatos := sRuta;
             end;
             else dmConcilia := NIL;
        end;

        if ( dmConcilia <> NIL ) then
        begin
             dsMontosTotales.Dataset := dmConcilia.cdsMontosTotales;
             dsEmpleadosTress.Dataset := dmConcilia.cdsEmpleadosTress;
             dsEmpleadosIMSS.DataSet := dmConcilia.cdsEmpleadosIMSS;
             dsDatosEmpleados.DataSet := dmConcilia.cdsDatosEmpleados;

             //test
              dsDatosEmpleadosTodos.DataSet := dmConcilia.cdsDatosEmpleadosTodos;

             dsInfonavit.DataSet := dmConcilia.cdsInfonavit;
             dsCredNoActivosTress.DataSet:= dmConcilia.cdsInactivosTress;
             dsCurp.DataSet := dmConcilia.cdsCurp;
             dsMovtosTress.DataSet := dmConcilia.cdsMovtosTress;
             dsMovtosImss.DataSet := dmConcilia.cdsMovtosIMSS;
             dsCreditosInactivosImss.DataSet:= dmConcilia.cdsInactivosImss;
             dsCredSuspendidosTress.DataSet := dmConcilia.cdsSuspendidosTress;
             SetReadOnlyDatasets( FALSE );

             if ( not Autorizacion.OkModulo( okIMSS )) then
             begin
                  ZetaDialogo.zInformation( '', 'M�dulo de ' + Autorizacion.GetModuloStr( okIMSS ) + ' no est� autorizado,' + CR_LF + 'consulte a su distribuidor para adquirirlo.' , 0);
             end
             else if lModoDemo then
             begin
                  ZetaDialogo.zInformation( '', 'Autorizaci�n en modo demo, consulte a su distribuidor para adquirir el m�dulo.', 0 );
                  ConciliarStart( 5000 );
                  try
                     for i := 0 to K_PASOS do
                        ConciliarStep;
                  finally
                         ConciliarEnd;
                  end;
             end
             else
             begin
                  with dmCliente do
                  begin
                       Clase := ClaseEmision;
                       IMSSPatron := sPatron;
                       IMSSYear := iYear;
                       IMSSTipo := eTipoLiqIMSS( iTipo );
                       IMSSMes := iMes;

                  end;
                  with dmConcilia do
                  begin
                       Clase := ClaseEmision;
                       Year := iYear;
                       Mes := iMes;
                       Precision := ePrecision.Valor;
                       if ( TipoConciliacion = tcSUA2006 ) then
                          Contrasena := K_SUA_ACCESS_CODE;


                       lOk := ConectarBaseDeDatos( sBaseDeDatos ) and ConectarTablas;
                  end;

                  if lOk then
                  begin
                       with dmCliente do
                       begin
                            ConciliaSua := TipoConciliacion;
                         //**   lOk := ConectarLiquidaciones( GetEmision, ( PageControl.ActivePage = tsSUA2006 ) );
                          lOk := ConectarLiquidaciones( ClaseEmision, (TipoConciliacion = tcSUA2006) );
                       end;
                       if lOk then
                       begin
                            with dmConcilia do
                            begin
                                 {$IFDEF ANTERIOR}
                                 with Resultados.Lines do
                                 begin
                                      Clear;
                                      BeginUpdate;
                                      try
                                         Lista := Resultados.Lines;
                                         Conciliar;
                                      finally
                                             DesconectaBasedeDatos;
                                             EndUpdate;
                                      end;
                                 end;
                                 {$ENDIF}
                                 with Resultados do
                                 begin
                                      Clear;
                                      BeginUpdate;
                                      try
                                         Lista := Resultados;
                                         Conciliar;
                                      finally
                                             SetReadOnlyDatasets( TRUE );
                                             DesconectaBasedeDatos;
                                             EndUpdate;
                                      end;
                                 end;
                                 Result := True;     //Bandera para indicar proceso finalizado
                                 if HayDiferencia then
                                 begin
                                      //Validar ds abierto
                                  {    GridCreditosNoActivos.DataSource.DataSet :=  cdsMovimientos.DataSource.DataSet;
                                      if ( GridCreditosNoActivos.DataSource.DataSet.Active ) then
                                          iMaxInfonavit := GridCreditosNoActivos.DataSource.DataSet.RecordCount;
                                          }
                                      ZetaDialogo.zWarning( '� Atenci�n !', Format( 'Se encontraron %d diferencias entre Tress e IMSS', [ Diferencias ] ), 0, mbOk );
                                 end
                                 else
                                 begin
                                      ZetaDialogo.zInformation( '� Exito !', 'No se encontraron diferencias entre Tress e IMSS', 0 );
                                 end;
                            end;
                       end
                  end
                  else
                  begin
                       dmConcilia.DesconectaBasedeDatos;
                  end;
                  HabilitarBotonesRibbon( TipoConciliacion, lOk, ( ClaseEmision = eecBimestral ) );
             end;
        end;
     finally
            FCancelaConciliacion := FALSE;
            Screen.Cursor := oCursor;
     end;
end;
procedure TTressShell.HabilitarBotonesRibbon( TipoConciliacion: eTipoConciliacion; RealizoConciliacion: Boolean; const EsEmisionBimestral: Boolean = False);
var
   HabilitarProceso: Boolean;
   HabilitarConciliacion : Boolean;
begin

     HabilitarProceso := False;
     HabilitarConciliacion := False;
     if ( dmCliente.ChecaAccesoGrabaInfonavit and RealizoConciliacion ) then
        HabilitarProceso := True;

     HabilitarConciliacion := ZAccesosMgr.CheckDerecho( D_IMSS, K_DERECHO_CONSULTA );
     CConciliar_btnConciliarSUA.Enabled := HabilitarConciliacion;
     CConciliar_btnConciliarIMSS.Enabled := HabilitarConciliacion;

     if ( TipoConciliacion = tcIDSE ) then
     begin
          CINFONAVIT_btnActualizaDatosINFONAVIT.Enabled := HabilitarProceso and EsEmisionBimestral;
          CINFONAVIT_btnImportarReinicioDescuento.Enabled := HabilitarProceso and EsEmisionBimestral;
          CINFONAVIT_btnImportarSuspensiones.Enabled := HabilitarProceso and EsEmisionBimestral;
          CINFONAVIT_btnImportarCreditos.Enabled := HabilitarProceso and EsEmisionBimestral;
     end;
     if ( RealizoConciliacion and ( TipoConciliacion = tcSUA2006 ) ) then
     begin
          CINFONAVIT_btnActualizaDatosINFONAVIT.Enabled := False;
          CINFONAVIT_btnImportarReinicioDescuento.Enabled := HabilitarProceso and EsEmisionBimestral;
          CINFONAVIT_btnImportarSuspensiones.Enabled := HabilitarProceso and EsEmisionBimestral;
          CINFONAVIT_btnImportarCreditos.Enabled := False;
     end;

     EOperaciones_btnExportarTexto.Enabled := HabilitarConciliacion and RealizoConciliacion;
end;


function TTressShell.ConciliarStep: Boolean;
begin
     Result := True;
     with ProgressBar do
      begin
          Position := Position + 1;
     end;
     Result := NOT FCancelaConciliacion;
     Application.ProcessMessages;

end;


procedure TTressShell.ConciliarStart( const iMaxSteps: Integer );
begin

     with ProgressBar do
     begin
          Position := 0;
          Properties.Max := iMaxSteps;
     end;
     Application.ProcessMessages;

end;

procedure TTressShell.ConciliarEnd;
begin
     with ProgressBar do
     begin
          Position := Properties.Max;
     end;
     Application.ProcessMessages;

end;



function TTressShell.GetConciliaDesc: string;
begin
     case ZetaCommonTools.StrAsInteger(ClientRegistry.ConciliadorIMSSTipoConciliacion) of
          Ord( tcSUA2006 ) : Result := 'Conciliaci�n Tress vs SUA';
      //    tcCDImss: Result := 'Conciliaci�n Tress vs CD IMSS';
          Ord( tcIDSE ): Result := 'Conciliaci�n Tress vs IDSE';
          Ord( tcNinguno ): Result := 'Conciliaci�n inv�lida'
     end;
end;

function TTressShell.GetLiquidacionDesc: string;
begin
     Result := 'Patr�n: ' + LLavePatron.Text + '=' + Patron.Text + CR_LF +
               'Liquidaci�n: ' + Tipo.Text +
                ', '+ Mes.Text + ' del ' + Year.Text;

end;


function TTressShell.GetEncabezadoForma: string;
begin
     Result := GetConciliaDesc + CR_LF + GetLiquidacionDesc;
end;

procedure TTressShell.SetProgressbar( BarraProgreso: TcxProgressBar);
begin
     Self.ProgressBar := BarraProgreso;
end;

procedure TTressShell.DoOpenSistema;
begin

end;

{$IFDEF FROMEX}
procedure TTressShell.GenerarResultadosMontosSUA;
var
   cdsEmpleadosTodos : TZetaClientDataSet;

   iEmpleado, iEmpleadoAnterior, i : integer;
   rMonto : TPesos;

   sDescrip : String;


   function  GetMontoConceptoSUA : TPesos;
   begin
        Result :=0.00;
        with cdsEmpleadosTodos do
        begin
             sDescrip := FieldByName('DESCRIPCION').AsString;

             if ( sDescrip =  DConciliador.K_Riesgos ) or
                  ( sDescrip =  DConciliador.K_Guarderias ) or
                  ( sDescrip =  DConciliador.K_Invalidez ) or
                  ( sDescrip =  DConciliador.K_Especie ) or
                  ( sDescrip =  DConciliador.K_Dinero ) or
                  ( sDescrip =  DConciliador.K_Excedente ) or
                  ( sDescrip =  DConciliador.K_CUOTA_FIJA ) then
                      Result := FieldByName('CD_IMSS').AsFloat;
        end;
   end;
   
   function  GetConceptoMonto( sConcepto : String ) : String;
   begin
        if ( sConcepto = DConciliador.k_Amortiza ) then Result := dmcliente.Amortizacion;
        if ( sConcepto = DConciliador.K_Infonavit ) then Result := dmcliente.InfonavitPatronal;
        if ( sConcepto = DConciliador.K_Cesantia ) then Result := dmcliente.CesantiaYVejez;
        if ( sConcepto = DConciliador.K_RETIRO ) then Result := dmcliente.Retiro;
   end;

begin
     iEmpleadoAnterior := 0;
     rMonto := 0.00;
     i:=0;

    cdsEmpleadosTodos := TZetaClientDataSet(  dsDatosEmpleadosTodos.DataSet );
    dmCliente.CargaMontosConceptos;
    if ( not cdsEmpleadosTodos.isEmpty )then
    begin
         with cdsEmpleadosTodos do
         begin
              DisableControls;
              OrdenarPor( cdsEmpleadosTodos, 'CB_CODIGO' );
              ResultadosMontosSUA.Clear;

              First;
              while not EOF do
              begin
                   Inc(i);
                   iEmpleado := FieldByName('CB_CODIGO').AsInteger;

                   if dmcliente.clase = eeCMensual then  //DIEGO
                   begin

                         if ( iEmpleado <> iEmpleadoAnterior ) or (i=RecordCount )  then
                         begin
                              //se agrega a la lista
                              if (i=RecordCount ) then
                              begin
                                 iEmpleadoAnterior := iEmpleado;
                                 rMonto := rMonto + GetMontoConceptoSUA;
                              end;

                              if ( iEmpleadoAnterior > 0 ) then
                                 ResultadosMontosSUA.Add(Format('%d,%d,%f', [iEmpleadoAnterior, StrToIntDef(dmCliente.ImssSubtotal, 0), rMonto]));

                              rMonto := 0;
                              iEmpleadoAnterior := iEmpleado;
                         end;

                         rMonto := rMonto + GetMontoConceptoSUA;
                   end
                   else   //bimestral
                   begin
                        sDescrip := FieldByName('DESCRIPCION').AsString;
                        if ( sDescrip = DConciliador.k_Amortiza ) or
                           ( sDescrip = DConciliador.K_Infonavit ) or
                           ( sDescrip = DConciliador.K_Cesantia ) or
                           ( sDescrip = DConciliador.K_RETIRO ) then
                           ResultadosMontosSUA.Add( Format('%d,%s,%f', [ iEmpleado,
                                                                         GetConceptoMonto( sDescrip ),
                                                                         FieldByName('CD_IMSS').AsFloat ] ) );
                   end;
                   Next;
              end;
              EnableControls;
         end;
    end;
end;
{$endif}


end.
