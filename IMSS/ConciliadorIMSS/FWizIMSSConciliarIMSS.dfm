inherited WiZIMSSConciliarIMSS: TWiZIMSSConciliarIMSS
  Caption = 'Conciliar IMSS'
  ClientHeight = 436
  ClientWidth = 517
  ExplicitWidth = 523
  ExplicitHeight = 464
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 517
    Height = 436
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000B0C00000B0C013F40
      22C800000457494441545847C596DD6F145518C607BDF0D2BFC02BB9D44B8C31
      26DC89F811675612135089684910E4C35489068476B1C06E53DBB25F2DFD04A4
      8AB1C6AF1B21314131050ABBAD8937DEF84778693BFE9E33E7E0ECE959D9ABE5
      4D7E796766E7BCCF33EF397376A2344D1F28C18BBD2478B19744C303E56DE593
      A509F88AE32B30CDF10FE42F2C5FC25310E54383CFADC65BE12A2CC22C9C1795
      D5F872AD155FA9AEC4D7397EDD08750AC4CA144FEFC3A7604764A1A2086CABAC
      C429A241104F65E27F0D50584F6E84306370C7EE3A4C821D91858A22BE4F2212
      432895199DE74DD59BC969B2866CB2FCD77E05429F39214F34CF0464036C5803
      7D145F776221EAAD78841C4D2D278F63F27BD8D26680C24339A14E8C4136C086
      31B01ABF4BF17F9C58683A269A71851C4DDE359D48591B036D0678EAAA27B601
      7B4F36C0860AD49BF1218AAEA9B0C4350DEE58D90A56C951A39954F43B1D196C
      33503A71B6E60BFA6060989C0DB0A102143CE8843A811967A02663D3CB49D98A
      3F620A51BCE10B0608768082077C411F4C0E93654046D2D9DBC9B8EB80318281
      CB9E588845B0D2595803EFE4C542349AF1E7E468E26E66E0FC9DA4A1B1AC892D
      FC76428BF0BB9C5027B45758E92C548435D09F1713F9F91774608E1C31F7E3FA
      6DE6766660663919D2B93A30EF898598032B9D858AF05447F26221DC1490C7C8
      EB176E16463496A9E8D79E2103173CB110C18D88A2DA077CC1B6F3DA4A5C243B
      036B17970A458D9DBF99BCE50CDCF7350499B4D25958031F3B41BFF58E5A2B19
      24EBDE51F2DAA5A5C2298D652D1CD3EF5A03DAE73788622C7F1E7C0D29B0DB09
      F3BE6F101708BD478EF8BDC813AF3105C73476EE56B2D3AC010A9772429DE8F4
      1A9A45A856E645F370CF1EB20CA813EB081FD4D885DF0ADB8D019EB49B7D400B
      D54A67610D682774421BC581D67FC0B116AC99730CECD758DE863D9A3E756013
      3C23D8159F8517397E015E829D3074F693334F90AD74162A42E1C7E01B3847F1
      12799C3C6558891779EACAC28DC26619602AFA6480C5B7CB1A78D319E89A7C58
      035DC39E61B6EDF95BC92B1A3B792739EA1BD80BFA6B3E05FBECF91B70188EC0
      283C07BE811DD596BE80126D345568F0E453149F8419CE2F71CF02595F4E291B
      D0F31A4B47FAB966FF9108D6C212C543F37F0FA6471F2FE67E15B126FE54E16E
      600AFE266FD638BE0F0E694A4C31058577401123BBE0357815FAE065D87DE6F8
      E97E0C6C75065C50F0699EA44E2E0B8E072C1F0ABAA1BDE268BD954CB129691A
      8C710C8CF07B9B81AEC987EB44B738034CC1FB7E07F2027A339E84519EFE6DD0
      DBA0B5A17571984EECE5DA7E8EDD3A30F04407287A8DE31FE15B5800AD8169AE
      CFD08959BE0716F963BA48477E6FEB80170F51BC8548702D88E2478375EE331F
      990A3D1505FF40CC9F7B7D3306BF1BB9FF2F3BBC3D540C11ED034DF8057E861B
      F013FC8AB1EB74E138B73E9C8DB8F7566C07FDFF6B83AAF3C423BC7E2518632D
      E8FAD77442DF05EA8ACE1F35031F24C18BBD2478B197042FF68E34FA17DF9B19
      41862977D40000000049454E44AE426082}
    ExplicitWidth = 517
    ExplicitHeight = 436
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso le permite verificar las diferencias que existen en' +
        ' Sistema TRESS y los archivos de IDSE.'
      Header.Title = 'Conciliaci'#243'n contra IMSS'
      ExplicitWidth = 495
      ExplicitHeight = 296
      inherited ValoresActivosgb: TcxGroupBox
        Top = 73
        Align = alClient
        TabOrder = 1
        ExplicitTop = 73
        ExplicitWidth = 495
        ExplicitHeight = 223
        Height = 223
        Width = 495
        inherited EmisionLBL: TcxLabel
          Left = 24
          ExplicitLeft = 24
        end
      end
      object gbDatosIMSS: TcxGroupBox
        Left = 0
        Top = 0
        Hint = ''
        Align = alTop
        Caption = 'Archivos de IMSS'
        TabOrder = 0
        Height = 73
        Width = 495
        object cxLabel2: TcxLabel
          Left = 10
          Top = 26
          Hint = ''
          Caption = 'Directorio:'
          Transparent = True
        end
        object btnDirectorioIMSS: TcxButton
          Left = 455
          Top = 24
          Width = 21
          Height = 21
          Hint = 'Seleccionar directorio de archivos IMSS'
          OptionsImage.Glyph.Data = {
            DA050000424DDA05000000000000360000002800000013000000130000000100
            200000000000A40500000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
            FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
            FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
            F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
            F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
            F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
            FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.Margin = 1
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = btnDirectorioIMSSClick
        end
        object DirectorioIMSS: TcxTextEdit
          Left = 70
          Top = 24
          Hint = ''
          TabStop = False
          Enabled = False
          TabOrder = 0
          Width = 380
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 495
      ExplicitHeight = 296
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 495
        ExplicitHeight = 160
        Height = 160
        Width = 495
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 495
        Width = 495
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 427
          ExplicitHeight = 74
          Width = 427
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
      inherited PanelProgressBar: TPanel
        Top = 255
        Width = 495
        ExplicitTop = 255
        ExplicitWidth = 495
        inherited ProgressBar: TcxProgressBar
          Left = 15
          ExplicitLeft = 15
          ExplicitHeight = 19
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Left = 312
    Top = 8
  end
  object ShellBrowserDialogDirectorioIMSS: TcxShellBrowserDialog
    FolderLabelCaption = 'Directorio IMSS'
    Title = 'Directorio IMSS'
    Left = 379
    Top = 128
  end
end
