object dmConciliador: TdmConciliador
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 466
  Width = 692
  object cdsPatron: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 152
  end
  object cdsEmpleados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 200
  end
  object cdsMovimientos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 249
  end
  object cdsMovConc: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 305
  end
  object cdsMontosTotales: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'FOLIO'
    Params = <>
    Left = 608
    Top = 40
  end
  object cdsEmpleadosTress: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'FOLIO'
    Params = <>
    Left = 608
    Top = 96
  end
  object cdsEmpleadosIMSS: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'FOLIO'
    Params = <>
    Left = 608
    Top = 168
  end
  object cdsDatosEmpleados: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO'
    Params = <>
    Left = 608
    Top = 224
  end
  object cdsCurp: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'FOLIO'
    Params = <>
    Left = 608
    Top = 280
  end
  object cdsInfonavit: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'FOLIO'
    Params = <>
    Left = 608
    Top = 336
  end
  object cdsMovtosTress: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'FOLIO'
    Params = <>
    Left = 496
    Top = 96
  end
  object cdsMovtosIMSS: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Indice'
      end>
    IndexFieldNames = 'FOLIO'
    Params = <>
    StoreDefs = True
    Left = 496
    Top = 168
  end
  object cdsInactivosTress: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 128
  end
  object cdsInactivosImss: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 192
  end
  object cdsSuspendidosTress: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 256
  end
  object cdsInactivosImssSuspendidosTress: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 296
    Top = 320
  end
  object cdsDatosEmpleadosTodos: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO'
    Params = <>
    Left = 496
    Top = 240
  end
end
