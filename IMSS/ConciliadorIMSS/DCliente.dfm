inherited dmCliente: TdmCliente
  OldCreateOrder = True
  object cdsRPatron: TZetaLookupDataSet
    Tag = 20
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    UsaCache = True
    LookupName = 'Cat'#225'logo de Registros Patronales'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnLookupDescription = cdsRPatronLookupDescription
    OnGetRights = cdsRPatronGetRights
    Left = 32
    Top = 136
  end
  object cdsLiqIMSS: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 16
  end
  object cdsLiqEmp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 144
    Top = 64
  end
  object cdsLiqMov: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsLiqMovAfterOpen
    Left = 144
    Top = 113
  end
  object cdsEmpleadoLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Empleados'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    Left = 312
    Top = 16
  end
  object cdsImportInfonavit: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 192
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosAfterOpen
    OnCalcFields = cdsProcesosCalcFields
    AlCrearCampos = cdsProcesosAlCrearCampos
    AlModificar = cdsProcesosAlModificar
    Left = 296
    Top = 128
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsLogDetailAfterOpen
    AlModificar = cdsLogDetailAlModificar
    Left = 376
    Top = 128
  end
  object cdsConceptos: TZetaLookupDataSet
    Tag = 17
    Aggregates = <>
    IndexFieldNames = 'CO_NUMERO'
    Params = <>
    AlAdquirirDatos = cdsConceptosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Conceptos de N'#243'mina'
    LookupDescriptionField = 'CO_DESCRIP'
    LookupKeyField = 'CO_NUMERO'
    Left = 144
    Top = 168
  end
end
