unit FMovimientosNoRegistradosIMSS;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseGridLectura_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxTextEdit, cxClasses, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Vcl.Menus, System.Actions, Vcl.ActnList,
  Vcl.ImgList, cxGridLevel, cxGridCustomView, cxGrid, ZetaCXGrid, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxCalendar;

type
  TMovimientosNoRegistradosIMSS = class(TBaseGridLectura_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    LM_FECHA: TcxGridDBColumn;
    LM_CLAVE: TcxGridDBColumn;
    LM_DIAS: TcxGridDBColumn;
    LM_INCAPAC: TcxGridDBColumn;
    LM_AUSENCI: TcxGridDBColumn;
    LM_BASE: TcxGridDBColumn;
    LM_EYM_FIJ: TcxGridDBColumn;
    LM_EYM_EXC: TcxGridDBColumn;
    LM_EYM_DIN: TcxGridDBColumn;
    LM_EYM_ESP: TcxGridDBColumn;
    LM_RIESGOS: TcxGridDBColumn;
    LM_INV_VID: TcxGridDBColumn;
    LM_GUARDER: TcxGridDBColumn;
    LM_RETIRO: TcxGridDBColumn;
    LM_CES_VEJ: TcxGridDBColumn;
    LM_INF_PAT: TcxGridDBColumn;
    LM_INF_AMO: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
    function PuedeImprimir( var sMensaje: String ): Boolean;override;
    procedure Refresh;override;
    procedure Connect; override;

  public
    { Public declarations }
  end;

const
     K_NOMBRE_FORMA = 'movimientos no registrados en IMSS';

var
  MovimientosNoRegistradosIMSS: TMovimientosNoRegistradosIMSS;

implementation

{$R *.dfm}

uses
     FTressShell, ZetaCommonClasses, ZetaRegistryCliente, ZetaCommonTools,
     DCliente;


procedure TMovimientosNoRegistradosIMSS.Connect;
begin
     inherited;
     DataSource.DataSet := TressShell.dsMovtosTress.DataSet;
end;

procedure TMovimientosNoRegistradosIMSS.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode := False;
     if ( TressShell.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n movimiento que no est� registrado en IMSS'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
     ZetaDBGridDBTableView.OptionsData.Editing := False;
     HelpContext := H_CONCIL_MOV_NO_REG_IMSS;
end;

procedure TMovimientosNoRegistradosIMSS.FormShow(Sender: TObject);
 const
       K_MENSUAL_I = 9;
       K_MENSUAL_F = 15;
       K_BIMESTRAL_I = 16;
       K_BIMESTRAL_F = 19;
       K_INF_AMO_COL = 8;
 var
    lMensual: Boolean;
    i: integer;
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('CB_CODIGO'), K_SIN_TIPO , '', skCount );
     ZetaDBGridDBTableView.ApplyBestFit();
     //si la conciliacion realizada fue mensual, se ocultan algunos registros
     lMensual := ( ZetaCommonTools.StrAsInteger( ClientRegistry.ConciliadorIMSSClaseEmision ) = Ord(eeCMensual) );
     begin
          with ZetaDBGridDBTableView do
          begin
               for i:= K_MENSUAL_I to K_MENSUAL_F do
                   Columns[i].Visible := lMensual;
               for i:= K_BIMESTRAL_I to K_BIMESTRAL_F do
                   Columns[i].Visible := not lMensual;
          end;
     end;
     TextoValorActivo1.Caption := TressShell.GetEncabezadoForma;
     TextoValorActivo2.Caption := Self.Caption;
end;

function TMovimientosNoRegistradosIMSS.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede agregar a ' + K_NOMBRE_FORMA;
end;

function TMovimientosNoRegistradosIMSS.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede borrar a ' + K_NOMBRE_FORMA;
end;

function TMovimientosNoRegistradosIMSS.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No se puede modificar a ' + K_NOMBRE_FORMA;
end;

function TMovimientosNoRegistradosIMSS.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result:= True;
end;

procedure TMovimientosNoRegistradosIMSS.Refresh;
begin

end;


end.
