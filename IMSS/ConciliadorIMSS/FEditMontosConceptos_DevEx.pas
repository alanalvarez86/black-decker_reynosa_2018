unit FEditMontosConceptos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaKeyLookup, DCliente,
  DB, ZetaMessages, cxGraphics, cxLookAndFeels,ZBaseConsulta,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, Vcl.ImgList,
  cxButtons, ZetaKeyLookup_DevEx;

type
  TEditMontosConceptos_DevEx = class(TZetaDlgModal_DevEx)
    GbMensual: TGroupBox;
    GbBimestral: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    IMSSSubtotalLookup: TZetaKeyLookup_DevEx;
    RetiroLookup: TZetaKeyLookup_DevEx;
    CesantiaYVejezLookup: TZetaKeyLookup_DevEx;
    InfonavitPatronalLookup: TZetaKeyLookup_DevEx;
    AmortizacionLookup: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private

  protected

  public
  end;

var
  EditMontosConceptos_DevEx: TEditMontosConceptos_DevEx;

implementation

uses
    ZetaDialogo;

{$R *.dfm}

procedure TEditMontosConceptos_DevEx.FormCreate(Sender: TObject);
var
   sCompania : String;
begin
     inherited;
     sCompania := dmCliente.Compania;
     with dmCliente do
     begin
          cdsConceptos.Conectar;

          IMSSSubtotalLookup.LookupDataset := cdsConceptos;
          RetiroLookup.LookupDataset := cdsConceptos;
          CesantiaYVejezLookup.LookupDataset := cdsConceptos;
          InfonavitPatronalLookup.LookupDataset := cdsConceptos;
          AmortizacionLookup.LookupDataset := cdsConceptos;

          try
             CargaMontosConceptos;
             IMSSSubtotalLookup.Llave := IMSSSubtotal;
             RetiroLookup.Llave := Retiro;
             CesantiaYVejezLookup.Llave := CesantiaYVejez;
             InfonavitPatronalLookup.Llave := InfonavitPatronal;
             AmortizacionLookup.Llave := Amortizacion;
          except

          end;
     end;
end;

procedure TEditMontosConceptos_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     //Guarda la configuracion de la empresa.
     with dmCliente do
     begin
          ImssSubtotal := ImssSubtotalLookup.Llave;
          Retiro := RetiroLookup.Llave;
          CesantiaYVejez := CesantiaYVejezLookup.Llave;
          InfonavitPatronal := InfonavitPatronalLookup.Llave;
          Amortizacion := AmortizacionLookup.Llave;
          try
             GuardaMontosConceptos;
          except
                ZetaDialogo.ZError('Error','No se puede escribir el archivo de configuración.',0);
          end;
     end;
end;

end.
