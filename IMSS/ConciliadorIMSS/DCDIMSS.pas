unit DCDIMSS;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs,  Db,
     //**ODSI, OVCL, OCL,
     DBClient, Provider, FileCtrl,
     {$ifndef VER130}
     Variants,
     {$endif}
     DCliente,
     DConciliador,
     ZetaCommonClasses,
     ZetaClientDataSet, ZetaServerDataSet,
      Data.Win.ADODB;

type
  TdmCDIMSS = class(TdmConciliador)
    //**Hdbc: THdbc;
    //**OECatalog: TOECatalog;
    prvUnico: TDataSetProvider;
    ADOQuery: TADOQuery;
    ADOConn: TADOConnection;
    //**qryMaster: TOEQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsEmpleadosAlCrearCampos(Sender: TObject);
    procedure cdsEmpleadosCalcFields(DataSet: TDataSet);
    procedure cdsEmpleadosAfterOpen(DataSet: TDataSet);
    procedure cdsMovimientosAfterOpen(DataSet: TDataSet);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FEsCDImss: Boolean;
    FComparaMontos: Boolean;
    FPath: String;
    FDatos: TStrings;
    ConnString : String;
    function ConectarMovimiento( const sAfiliacion: String ): Boolean;
    function ConectarTabla(Tabla: TClientDataset; const sQuery, sTabla: String): Boolean;
    function GetDescTipoMov(const sCampo: String): String;
    function GetPatronID: Boolean;
    function GetSQLScript( const eTipo: eEmisionTipo ): String;
    function GetTableName( const eTipo: eEmisionTipo ): String;
    function OpenSQL(const sSQL: String; const lDatos: Boolean): OleVariant;
    procedure ConciliarEmpleados;
    procedure ConciliarListaEmpleados(Original, Datos: TDataset; const lTress: Boolean; Log: TStrings);
    procedure ConciliarListaMovimientos( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings );
    procedure ConciliarMovimientos( const iEmpleado: TNumEmp; const sAfiliacion: String; Log: TStrings  );
    procedure ConciliarPatrones;
    function CargarEmpleados(const eClase: eEmisionClase; const sFileName: String): Boolean;
    function CargarMovimientos(const sAfiliacion: String; const eClase: eEmisionClase; const sFileName: String): Boolean;
    function CargarPatron(const eClase: eEmisionClase; const sFileName: String): Boolean;
  protected
    { Protected declarations }
    function DataAsString(const sData: String; const iStart, iLength: Integer; const lPurosCeros: Boolean = false ): String;
    function DataAsInteger(const sData: String; const iStart, iLength: Integer): Integer;
    function DataAsFloat(const sData: String; const iStart, iLength: Integer): Extended;
    function DataAsDateDay(const sData: String; const iStart, iLength: Integer): TDate;
    function DataAsDateMonth(const sData: String; const iStart, iLength: Integer): TDate;
    function DataAsDateYear(const sData: String; const iStart, iLength: Integer): TDate;
    function GetFileName( const eTipo: eEmisionTipo ): String;
    function GetConciliaDescripcion: string;override;
  public
    { Public declarations }
    function ConectarBaseDeDatos( const sFileName: String ): Boolean;override;
    procedure DesconectaBasedeDatos;override;
    function ConectarTablas: Boolean;override;
    procedure Conciliar;override;
    property EsCDImss : Boolean read FEsCDImss write FEsCDImss;
    property ComparaMontos : Boolean read FComparaMontos write FComparaMontos default TRUE;

  end;

var
  dmCDIMSS: TdmCDIMSS;

implementation

uses FTressShell,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaCommonTools;

{$R *.DFM}

const
     K_ANCHO_COLUMNA = 25;
     K_ANCHO = 90;

     K_ANCHO_REGISTRO_PATRONAL = 11;

     K_ESPACIO = ' ';
     K_INFONAVIT_INI = 'FEC_INF_INI';
     K_AFILIACION = 'AFILIACION';
     K_NUM_CRED_INFONAVIT = 'NO_CRED_INF';
     K_CIENTO = 100;

function GetScript( const eValor: eScript ): String;
begin
     case eValor of
          esPatronMensual: Result := 'select '+
                                     'EMIP_PATRON as TB_NUMREG, '+          { RPATRON.TB_NUMREG }
                                     'EMIP_PRIMA_RT as PRIMA_RT, '+         { PRIESGO.RT_PRIMA on PRIESGO.RT_FECHA }
                                     'EMIP_NUM_TRAB_COT as LS_NUM_TRA, '+   { LIQ_IMSS.LS_NUM_TRA }
                                     'EMIP_IMP_EYM_FIJA as LS_EYM_FIJ, '+   { LIQ_IMSS.LS_EYM_FIJ }
                                     'EMIP_IMP_EYM_EXCE as LS_EYM_EXC, '+   { LIQ_IMSS.LS_EYM_EXC }
                                     'EMIP_IMP_EYM_PRED as LS_EYM_DIN, '+   { LIQ_IMSS.LS_EYM_DIN }
                                     'EMIP_IMP_EYM_PREE as LS_EYM_ESP, '+   { LIQ_IMSS.LS_EYM_ESP }
                                     'EMIP_IMP_INV_VIDA as LS_INV_VID, '+   { LIQ_IMSS.LS_INV_VID }
                                     'EMIP_IMP_RIES_TRA as LS_RIESGOS, '+   { LIQ_IMSS.LS_RIESGOS }
                                     'EMIP_IMP_GUAR as LS_GUARDER, '+       { LIQ_IMSS.LS_GUARDER }
                                     'EMIP_DIAS as LS_DIAS_CO '+            { LIQ_IMSS.LS_DIAS_CO }
                                     'from %s '+
                                     'where ( EMIP_CVE_UNIT = ''%s'' )';
          esPatronBimestral: Result := 'select '+
                                       'EBIP_PATRON as TB_NUMREG, '+        { RPATRON.TB_NUMREG }
                                       'EBIP_NUM_TRAB_COT as LS_NUM_BIM, '+ { LIQ_IMSS.LS_NUM_BIM }
                                       'EBIP_IMP_RETIRO as LS_RETIRO, '+    { LIQ_IMSS.LS_RETIRO }
                                       'EBIP_IMP_CYV as LS_CES_VEJ, '+      { LIQ_IMSS.LS_CES_VEJ }
                                       'EBIP_DIAS as LS_DIAS_BM, '+         { LIQ_IMSS.LS_DIAS_BM }
                                       'EBIP_SUMA as LS_SUB_RET, '+         { LIQ_IMSS.LS_SUB_RET - No considera LS_APO_VOL, LS_ACT_RET, LS_REC_RET_, etc }
                                       'EBIP_TOT_CRED as LS_INF_NUM, '+     { LIQ_IMSS.LS_INF_NUM }
                                       'EBIP_IMPOR_INFO as LS_INF_TOT, '+   { LS_INF_TOT = ( LIQ_IMSS.LS_INF_NAC + LIQ_IMSS.LS_INF_ACR ) }
                                       'EBIP_IMPOR_AMOR as LS_INF_AMO, '+   { LIQ_IMSS.LS_INF_AMO }
                                       'EBIP_SUMA_INFO as LS_SUB_INF, '+    { LIQ_IMSS.LS_SUB_INF }
                                       'EBIP_IMPOR_TOT as LS_TOTAL '+       { LS_TOTAL = ( LIQ_IMSS.LS_SUB_RET + LIQ_IMSS.LS_SUB_INF ) }
                                       'from %s '+
                                       'where ( EBIP_CVE_UNIT = ''%s'' )';
          esEmpleadoMensual: Result := 'select '+
                                       '-1 as CB_CODIGO, '+
                                       '0 as ' + K_FOUND + ', '+
                                       'EMIA_NUM_AFIL as ' + K_AFILIACION + ', '+   { COLABORA.CB_SEGSOC }
                                       'EMIA_NUM_AFIL as ' + K_NUM_SS + ', '+       { Dummy para Afiliacion }
                                       'EMIA_TIPO_TRAB as TB_MODULO, '+             { RPATRON.TB_MODULO }
                                       'EMIA_NOM_TRAB as NOMBRE_SS, '+              { GetNombreSUA( COLABORA.CB_APE_PAT ) + '$' + GetNombreSUA( COLABORA.CB_APE_MAT ) + '$' + GetNombreSUA( COLABORA.CB_NOMBRES ) }
                                       'EMIA_JOR_SEM_RED as TU_TIP_JOR, '+          { TURNO.TU_TIP_JOR - 1 where ( TURNO.TU_CODIGO = COLABORA.CB_TURNO ) }
                                       'EMIA_CURP as CB_CURP, '+                    { COLABORA.CB_CURP }
                                       '0 as ' + K_INFONAVIT_INI + ' '+             { ZetaCommonTools.FechaToStr() = COLABORA.CB_INF_INI }
                                       'from %s '+
                                       'where ( EMIA_CVE_UNIT = ''%s'' ) '+
                                       'order by EMIA_NUM_AFIL';
          esEmpleadoBimestral: Result := 'select '+
                                         '-1 as CB_CODIGO, '+
                                         '0 as ' + K_FOUND + ', '+
                                         'EBIA_NUM_AFIL as ' + K_AFILIACION + ', '+            { COLABORA.CB_SEGSOC }
                                         'EBIA_NUM_AFIL as ' + K_NUM_SS + ', '+                { Dummy para Afiliacion }
                                         'EBIA_TIPO_TRAB as TB_MODULO, '+                      { RPATRON.TB_MODULO }
                                         'EBIA_NOM_TRAB as NOMBRE_SS, '+                       { GetNombreSUA( COLABORA.CB_APE_PAT ) + '$' + GetNombreSUA( COLABORA.CB_APE_MAT ) + '$' + GetNombreSUA( COLABORA.CB_NOMBRES ) }
                                         'EBIA_JOR_SEM_RED as TU_TIP_JOR, '+                   { TURNO.TU_TIP_JOR - 1 where ( TURNO.TU_CODIGO = COLABORA.CB_TURNO ) }
                                         'EBIA_CURP as CB_CURP, '+                             { COLABORA.CB_CURP }
                                         'EBIA_CRED_INFO as ' + K_NUM_CRED_INFONAVIT + ', ' +  { COLABORA.CB_INFCRED }
                                         'EBIA_CRED_INFO as CB_INFCRED, '+                     { COLABORA.CB_INFCRED }
                                         'EBIA_TIP_AMOR_INFO as CB_INFTIPO, '+                 { COLABORA.CB_INFTIPO }
                                         'EBIA_POR_VSM as CB_INFTASA, '+                       { COLABORA.CB_INFTASA }
                                         'EBIA_FEC_INF as ' + K_INFONAVIT_INI + ' '+           { ZetaCommonTools.FechaToStr() = COLABORA.CB_INF_INI }
                                         'from %s '+
                                         'where ( EBIA_CVE_UNIT = ''%s'' ) '+
                                         'order by EBIA_NUM_AFIL';
          esMovimientoMensual: Result := 'select '+
                                         '0 as ' + K_ORIGEN + ', '+
                                         'EMIM_TIPO_MOV as ' + K_TIPO_MOV + ', '+   { TipoMov3ToIMSS( LIQ_MOV.LM_KAR_TIP ) - ver CD: MAESTRA.EAMP }
                                         'EMIM_FECHA_MOV as LM_FECHA, '+            { LIQ_MOV.LM_FECHA }
                                         'EMIM_DIAS_COBRO as LM_DIAS, '+            { LIQ_MOV.LM_DIAS }
                                         'EMIM_SAL_BASE as LM_BASE, '+              { LIQ_MOV.LM_BASE }
                                         'EMIM_IMP_EYM_FIJA as LM_EYM_FIJ, '+       { LIQ_MOV.LM_EYM_FIJ }
                                         'EMIM_IMP_EYM_EXCE as LM_EYM_EXC, '+       { LIQ_MOV.LM_EYM_EXC }
                                         'EMIM_IMP_EYM_PRES as LM_EYM_DIN, '+       { LIQ_MOV.LM_EYM_DIN }
                                         'EMIM_IMP_EYM_ESPE as LM_EYM_ESP, '+       { LIQ_MOV.LM_EYM_ESP }
                                         'EMIM_IMP_RT as LM_INV_VID, '+             { LIQ_MOV.LM_INV_VID - Parece que debiera corresponder a EMIM.IMP_IV, pero el programa del CD lo tiene asignado a esta columna ( � bug ? ) }
                                         'EMIM_IMP_IV as LM_RIESGOS, '+             { LIQ_MOV.LM_RIESGOS - Parece que debiera corresponder a EMIM.IMP_RT, pero el programa del CD lo tiene asignado a esta columna ( � bug ? ) }
                                         'EMIM_IMP_GUAR as LM_GUARDER '+            { LIQ_MOV.LM_GUARDER }
                                         'from %s '+
                                         'where '+
                                         '( EMIM_CVE_UNIT = ''%s'' ) and '+
                                         '( EMIM_NUM_AFIL like ''%s'' ) '+
                                         'order by EMIM_FECHA_MOV, EMIM_TIPO_MOV';
          esMovimientoBimestral: Result := 'select '+
                                           '0 as ' + K_ORIGEN + ', '+
                                           'EBIM_TIPO_MOV as ' + K_TIPO_MOV + ', '+   { TipoMov3ToIMSS( LIQ_MOV.LM_KAR_TIP ) - ver CD: MAESTRA.EAMP }
                                           'EBIM_FECHA_MOV as LM_FECHA, '+            { LIQ_MOV.LM_FECHA }
                                           'EBIM_DIAS_COBRO as LM_DIAS, '+            { LIQ_MOV.LM_DIAS }
                                           'EBIM_SAL_BASE as LM_BASE, '+              { LIQ_MOV.LM_BASE }
                                           'EBIM_IMP_RETIRO as LM_RETIRO, '+          { LIQ_MOV.LM_RETIRO }
                                           'EBIM_IMP_CYV as LM_CES_VEJ, '+            { LIQ_MOV.LM_CES_VEJ }
                                           'EBIM_IMP_INF as LM_INF_PAT, '+            { LIQ_MOV.LM_INF_PAT }
                                           'EBIM_IMP_AMOR as LM_INF_AMO '+            { LIQ_MOV.LM_INF_AMO }
                                           'from %s '+
                                           'where '+
                                           '( EBIM_CVE_UNIT = ''%s'' ) and '+
                                           '( EBIM_NUM_AFIL like ''%s'' ) '+
                                           'order by EBIM_FECHA_MOV, EBIM_TIPO_MOV';
          esPatronIDMensual: Result := 'select EMIP_CVE_UNIT as PATRON_ID from %s where ( EMIP_PATRON like ''%s'' )';
          esPatronIDBimestral: Result := 'select EBIP_CVE_UNIT as PATRON_ID from %s where ( EBIP_PATRON like ''%s'' )';
     else
         Result := VACIO;
     end;
end;

{ ************ TdmCDIMSS ************* }

procedure TdmCDIMSS.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FDatos := TStringList.Create;
     FComparaMontos := TRUE;
end;

procedure TdmCDIMSS.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FDatos );

end;

procedure TdmCDIMSS.DesconectaBasedeDatos;
begin
     cdsPatron.Close;
     cdsEmpleados.Close;
     cdsMovimientos.Close;
     cdsMovConc.Close;
     try
        if ( ADOConn.Connected) then
           ADOConn.Close;
     except on E:exception do
            begin
                 ZetaDialogo.zExcepcion( '� Error !', 'Error al cerrar conexi�n ADO de las tablas de IMSS', E, 0 );
            end;
     end;
end;

function TdmCDIMSS.ConectarBaseDeDatos( const sFileName: String ): Boolean;
begin
     Result := False;

     cdsEmpleados.AfterOpen := cdsEmpleadosAfterOpen;
     cdsEmpleados.OnCalcFields := cdsEmpleadosCalcFields;
     cdsEmpleados.AlCrearCampos := cdsEmpleadosAlCrearCampos;

     if FEsCDImss then
     begin

          cdsMovimientos.AfterOpen := cdsMovimientosAfterOpen;

          try
             if ZetaCommonTools.StrVacio( sFileName ) then
             begin
                  ZetaDialogo.zError( '� Atenci�n !', 'No Se Especific� El Archivo De Datos Del CD IMSS', 0 );
             end
             else
             begin
                  if FileExists( sFileName ) then
                  begin
                  {
                       with Hdbc do
                       begin
                            Connected := False;
                            Driver := 'Microsoft Access Driver (*.mdb)';
                            with Attributes do
                            begin
                                 Clear;
                                 Add( Format( 'DBQ=%s', [ sFileName ] ) );
                                 Add( Format( 'ReadOnly=%d', [ 1 ] ) );
                            end;
                            Connected := True;
                       end;
                       Result := True;

                  }

                  try
                     ADOConn := TADOConnection.Create(Self);
                  except on E:exception do    
                         begin
                              ZetaDialogo.zExcepcion( '� Error !', 'Error al crear conexion ADO', E, 0 );
                         end;
                  end;
                  {$IFDEF WIN32}
                  ConnString := 'Driver={Microsoft Access Driver (*.mdb)};' +
                  'DBQ=%s;Persist Security Info=False;password=%s';
                  {$ELSE}
                  ConnString := 'Driver={Microsoft Access Driver (*.mdb, *.accdb)};' +
                  'DBQ=%s;Persist Security Info=False;password=%s';
                  {$ENDIF}

                  ConnString := Format( ConnString, [sFileName, Contrasena ]);
                  ADOConn.ConnectionString := ConnString;
                  ADOConn.LoginPrompt := False;
                  try
                     ADOConn.Connected := True;
					 Result := True;
                  except on E:exception do
                         begin
                              ZetaDialogo.zExcepcion( '� Error !', 'Error al abrir conexi�n ADO de las tablas de IMSS', E, 0 );
                              Result := False;
                         end;
                  end;                 
                  end
                  else
                  begin
                       ZetaDialogo.zError( '� Atenci�n !', 'El Archivo De Datos Del CD IMSS No Existe', 0 );
                  end;
             end;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( '� Error !', 'Error Al Abrir Base De Datos De CD IMSS', Error, 0 );
                end;
          end;
     end
     else
     begin
          if FileCtrl.DirectoryExists( sFileName ) then
          begin
               FPath := ZetaCommonTools.VerificaDir( sFileName );
               Result := True;
          end
          else
          begin
               ZetaDialogo.zError( '� Error !', Format( 'El Directorio %s No Existe', [ sFileName ] ), 0 );
               Result := False;
          end;
     end;

end;

procedure TdmCDIMSS.cdsEmpleadosAlCrearCampos(Sender: TObject);
begin
     with cdsEmpleados do
     begin
          CreateCalculated( 'CB_INF_INI', ftDateTime, 0 );
     end;
end;

procedure TdmCDIMSS.cdsEmpleadosCalcFields(DataSet: TDataSet);
begin
     with cdsEmpleados do
     begin
          FieldByName( 'CB_INF_INI' ).AsDateTime := ZetaCommonTools.StrToFecha( Format( '%d', [ FieldByName( K_INFONAVIT_INI ).AsInteger ] ) );
     end;
end;

procedure TdmCDIMSS.cdsEmpleadosAfterOpen(DataSet: TDataSet);
begin
     with Dataset do
     begin
          while not Eof do
          begin
               Edit;
               try
                  with FieldByName( K_AFILIACION ) do
                  begin
                       FieldByName( K_NUM_SS ).AsString := Copy( AsString, 2, ( Length( AsString ) - 1 ) );
                  end;
                  if ( Clase = eecBimestral ) then
                  begin
                       with FieldByName( K_NUM_CRED_INFONAVIT ) do
                       begin
                            FieldByName( 'CB_INFCRED' ).AsString := Copy( AsString, 3, ( Length( AsString ) - 2 ) );
                       end;
                       with FieldByName( 'CB_INFCRED' ) do
                       begin
                            if ( AsString = StringOfChar( '0',Length( AsString ) ) ) then
                            begin
                                 AsString := VACIO;
                            end;
                       end;
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          First;
     end;
end;

procedure TdmCDIMSS.cdsMovimientosAfterOpen(DataSet: TDataSet);
begin
   with Dataset do
     begin
          if not FEsCDImss then
             First;
          while not Eof do
          begin
               Edit;

               try
                  with FieldByName( K_TIPO_MOV ) do
                  begin
                       if ( AsInteger = K_MAESTRA_INICIAL ) then
                       begin
                            with FieldByName( 'LM_FECHA' ) do
                            begin
                                 AsDateTime := dmCliente.FechaInicial;
                            end;
                       end;

                       if ( AsInteger = K_MAESTRA_EAMP_ALTA2 ) then
                          AsInteger := K_MAESTRA_EAMP_ALTA;
                  end;
                  Post;
               except
                     on Error: Exception do
                     begin
                          Cancel;
                     end;
               end;
               Next;
          end;
          First;
     end;
end;

function TdmCDIMSS.OpenSQL( const sSQL: String; const lDatos: Boolean): OleVariant;
var
   iRecords: Integer;
begin
 //**
 {    with qryMaster do
     begin
          Active := False;
          SQL.Text := sSQL;
          FRecsOut := 0;
          if ( lDatos ) then
             iRecords := -1
          else
              iRecords := 0;
          Result := prvUnico.GetRecords( iRecords, FRecsOut, Ord( grMetaData ) + Ord( grReset ) )
     end;
     } 
      try
          ADOQuery := TADOQuery.Create(Self);
          ADOQuery.Connection := ADOConn;
          ADOQuery.SQL.Add(sSQL);
          ADOQuery.Active := True;
          prvUnico.DataSet := ADOQuery;
          FRecsOut := 0;
          if ( lDatos ) then
             iRecords := -1
          else
              iRecords := 0;
          Result := prvUnico.GetRecords( iRecords, FRecsOut, Ord( grMetaData ) + Ord( grReset ) );
          ADOQuery.Close;
          ADOQuery.Free;
       except
       on e: EADOError do
       begin
            ZetaDialogo.zExcepcion( '� Error !', 'Error al en la consulta a las tablas de IMSS', E, 0 );
       end;
  end;


end;

function TdmCDIMSS.ConectarTabla( Tabla: TClientDataset; const sQuery, sTabla: String ): Boolean;
begin
     try
        with Tabla do
        begin
             DisableControls;
             try
                Active := False;
                Data := OpenSQL( Format( sQuery, [ sTabla, FPatronID ] ), True );
                if not Active then
                   Active := True;
             finally
                    EnableControls;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Abrir Tabla %s De CD IMSS', [ sTabla ] ), Error, 0 );
                Result := False;
           end;
     end;
end;

function TdmCDIMSS.GetTableName( const eTipo: eEmisionTipo ): String;
const
     aEmisionTipo: array[ eEmisionTipo ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'PA', 'AS', 'MO' );
     aEmisionClase: array[ eEmisionClase ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'M', 'B' );
begin
     Result := Format( 'CDE%s%s%2.2d%4.4d', [ aEmisionClase[ Clase ], aEmisionTipo[ eTipo ], FNumero, FYear ] );
end;

function TdmCDIMSS.GetSQLScript( const eTipo: eEmisionTipo ): String;
begin
     case Clase of
          eeCMensual:
          begin
               case eTipo of
                    eeTPatron: Result := GetScript( esPatronMensual );
                    eeTEmpleados: Result := GetScript( esEmpleadoMensual );
                    eeTMovimientos: Result := GetScript( esMovimientoMensual );
               end;
           end;
           eeCBimestral:
           begin
                case eTipo of
                     eeTPatron: Result := GetScript( esPatronBimestral );
                     eeTEmpleados: Result := GetScript( esEmpleadoBimestral );
                     eeTMovimientos: Result := GetScript( esMovimientoBimestral );
                end;
           end;

     end;
end;


function TdmCDIMSS.GetPatronID: Boolean;
var
   eQuery: eScript;
begin
     Result := False;
     FPatronID := VACIO;
     case Clase of
          eeCMensual: eQuery := esPatronIDMensual;
     else
         eQuery := esPatronIDBimestral;
     end;
     try
        with cdsPatron do
        begin
             Active := False;
             Data := OpenSQL( Format( GetScript( eQuery ), [ GetTableName( eetPatron ), '%' + dmCliente.IMSSRegistro + '%' ] ), True );
             if IsEmpty then
             begin
                  ZetaDialogo.zError( '� Error !', Format( 'El CD IMSS No Tiene Datos Para El Registro Patronal %s', [ dmCliente.IMSSRegistro ] ), 0 );
             end
             else
             begin
                  FPatronID := FieldByName( 'PATRON_ID' ).AsString;
                  Result := True;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'El CDImss no Contiene Informaci�n para' + CR_LF + 'Registro Patronal %s, A�o %d, Mes %s' + CR_LF,
                                                             [ dmCliente.IMSSRegistro,
                                                               Year,
                                                               ObtieneElemento( lfMeses, Ord( Mes -1 ) ) ] )
                                                 , Error, 0 );
           end;
     end;
end;

function TdmCDIMSS.ConectarMovimiento( const sAfiliacion: String ): Boolean;
var
   eQuery: eScript;
   Datos: Variant;
begin
     if FEsCDImss then
     begin
          Result := False;
          case Clase of
               eeCMensual: eQuery := esMovimientoMensual;
          else
              eQuery := esMovimientoBimestral;
          end;
          try
             Datos := OpenSQL( Format( GetScript( eQuery ), [ GetTableName( eetMovimientos ), FPatronID, '%' + sAfiliacion + '%' ] ), True );
             with cdsMovimientos do
             begin
                  Active := False;
                  Data := Datos;
             end;
             with cdsMovConc do
             begin
                  Active := False;
                  Data := Datos;
                  EmptyDataset;
             end;
             Result := True;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Movimientos de IMSS # %s', [ sAfiliacion ] ), Error, 0 );
                end;
          end;
     end
     else
     begin
          Result := CargarMovimientos( sAfiliacion, Clase, GetFileName( eetMovimientos ) );
     end;
end;

function TdmCDIMSS.DataAsString( const sData: String; const iStart, iLength: Integer; const lPurosCeros: Boolean = false  ): String;
begin
     if ( Length( sData ) < ( iStart + iLength - 1 ) ) then
        raise Exception.Create( Format( 'Datos Insuficientes: Substr( "%s", %d, %d )', [ sData, iStart, iLength ] ) )
     else
     begin
          Result := Copy( sData, iStart, iLength );
          if lPurosCeros then
          begin
               if ( Result = StringOfChar( '0', Length( Result ) ) ) then
                  Result := '';
          end;
     end;
end;

function TdmCDIMSS.DataAsInteger( const sData: String; const iStart, iLength: Integer ): Integer;
begin
     Result := StrToIntDef( DataAsString( sData, iStart, iLength ), 0 );
end;

function TdmCDIMSS.DataAsFloat( const sData: String; const iStart, iLength: Integer ): Extended;
 var
    sValor: string;
begin
     sValor := DataAsString( sData, iStart, iLength );
     Result := ZetaCommonTools.StrToReal( sValor );
     if Pos( '.', sValor ) = 0 then
        Result := Result/ K_CIENTO;
end;

function TdmCDIMSS.DataAsDateYear( const sData: String; const iStart, iLength: Integer ): TDate;
begin
     Result := ZetaCommonTools.GetImportedDateYear( DataAsString( sData, iStart, iLength ) );
end;

function TdmCDIMSS.DataAsDateMonth( const sData: String; const iStart, iLength: Integer ): TDate;
begin
     Result := ZetaCommonTools.GetImportedDateMes( DataAsString( sData, iStart, iLength ) );
end;

function TdmCDIMSS.DataAsDateDay( const sData: String; const iStart, iLength: Integer ): TDate;
begin
     Result := ZetaCommonTools.GetImportedDateDia( DataAsString( sData, iStart, iLength ) );
end;

function TdmCDIMSS.CargarPatron( const eClase: eEmisionClase; const sFileName: String ): Boolean;
var
   sDatos: String;
begin
     Result := False;
     try
        with cdsPatron do
        begin
             Init;
             case eClase of
                  eeCMensual:
                  begin
                       AddStringField( 'TB_NUMREG', K_ANCHO_REGISTRO_PATRONAL );
                       AddFloatField( 'PRIMA_RT' );
                       AddIntegerField( 'LS_NUM_TRA' );
                       AddFloatField( 'LS_EYM_FIJ' );
                       AddFloatField( 'LS_EYM_EXC' );
                       AddFloatField( 'LS_EYM_DIN' );
                       AddFloatField( 'LS_EYM_ESP' );
                       AddFloatField( 'LS_INV_VID' );
                       AddFloatField( 'LS_RIESGOS' );
                       AddFloatField( 'LS_GUARDER' );
                       AddIntegerField( 'LS_DIAS_CO' );
                  end;
                  eeCBimestral:
                  begin
                       AddStringField( 'TB_NUMREG', K_ANCHO_REGISTRO_PATRONAL );
                       AddIntegerField( 'LS_NUM_BIM' );
                       AddFloatField( 'LS_RETIRO' );
                       AddFloatField( 'LS_CES_VEJ' );
                       AddIntegerField( 'LS_DIAS_BM' );
                       AddFloatField( 'LS_SUB_RET' );
                       AddIntegerField( 'LS_INF_NUM' );
                       AddFloatField( 'LS_INF_TOT' );
                       AddFloatField( 'LS_INF_AMO' );
                       AddFloatField( 'LS_SUB_INF' );
                       AddFloatField( 'LS_TOTAL' );

                  end;
             end;
             CreateTempDataset;
        end;
        with FDatos do
        begin
             Clear;
             LoadFromFile( sFileName );
             case Count of
                  0: ZetaDialogo.zError( '� Error !', Format( 'Archivo De Datos del Patr�n ( %s ) Est� Vac�o', [ sFileName ] ), 0 );
                  1:
                  begin
                       sDatos := Strings[ 0 ];
                       with cdsPatron do
                       begin
                            Append;
                            case eClase of
                                 eeCMensual:
                                 begin
                                      FieldByName( 'TB_NUMREG' ).AsString := DataAsString( sDatos, 1, 10 );     { RPATRON.TB_NUMREG }
                                      FieldByName( 'PRIMA_RT' ).AsFloat := DataAsFloat( sDatos, 102, 8 );       { PRIESGO.RT_PRIMA on PRIESGO.RT_FECHA }
                                      //FieldByName( 'LS_NUM_TRA' ).AsFloat := 0;                                 { LIQ_IMSS.LS_NUM_TRA : � Inferir de la cantidad de Asegurados - cdsEmpleados.RecordCount ? }
                                      FieldByName( 'LS_NUM_TRA' ).AsInteger := DataAsInteger( sDatos, 113, 8 );  { LIQ_IMSS.LS_NUM_TRA : � Inferir de la cantidad de Asegurados - cdsEmpleados.RecordCount ? }
                                      FieldByName( 'LS_EYM_FIJ' ).AsFloat := DataAsFloat( sDatos, 121, 12 );    { LIQ_IMSS.LS_EYM_FIJ }
                                      FieldByName( 'LS_EYM_EXC' ).AsFloat := DataAsFloat( sDatos, 133, 12 ) + DataAsFloat( sDatos, 145, 12 );    { LIQ_IMSS.LS_EYM_EXC }
                                      FieldByName( 'LS_EYM_DIN' ).AsFloat := DataAsFloat( sDatos, 157, 12 ) + DataAsFloat( sDatos, 169, 12 );    { LIQ_IMSS.LS_EYM_DIN }
                                      FieldByName( 'LS_EYM_ESP' ).AsFloat := DataAsFloat( sDatos, 181, 12 ) + DataAsFloat( sDatos, 193, 12 );    { LIQ_IMSS.LS_EYM_ESP }
                                      FieldByName( 'LS_INV_VID' ).AsFloat := DataAsFloat( sDatos, 205, 12 ) + DataAsFloat( sDatos, 217, 12 );    { LIQ_IMSS.LS_INV_VID } { OJO ! Puede corresponder a LS_RIESGOS }
                                      FieldByName( 'LS_RIESGOS' ).AsFloat := DataAsFloat( sDatos, 229, 12 );    { LIQ_IMSS.LS_RIESGOS } { OJO ! Puede corresponder a LS_INV_VID }
                                      FieldByName( 'LS_GUARDER' ).AsFloat := DataAsFloat( sDatos, 241, 12 );    { LIQ_IMSS.LS_GUARDER }
//                                      FieldByName( 'LS_DIAS_CO' ).AsInteger := DataAsInteger( sDatos, 399, 7 ); { LIQ_IMSS.LS_DIAS_CO }
                                        FieldByName( 'LS_DIAS_CO' ).AsInteger := DataAsInteger( sDatos, 399, 8 ); { LIQ_IMSS.LS_DIAS_CO }
                                 end;
                                 eeCBimestral:
                                 begin
                                      FieldByName( 'TB_NUMREG' ).AsString := DataAsString( sDatos, 1, 10 );     { RPATRON.TB_NUMREG }
                                      //FieldByName( 'LS_NUM_BIM' ).AsInteger := 0;                               { LIQ_IMSS.LS_NUM_BIM }
                                      FieldByName( 'LS_NUM_BIM' ).AsInteger := DataAsInteger( sDatos, 111, 8 );                               { LIQ_IMSS.LS_NUM_BIM }
                                      FieldByName( 'LS_RETIRO' ).AsFloat := DataAsFloat( sDatos, 119, 12 );     { LIQ_IMSS.LS_RETIRO }
                                      FieldByName( 'LS_CES_VEJ' ).AsFloat := DataAsFloat( sDatos, 131, 12 ) + DataAsFloat( sDatos, 143, 12 );    { LIQ_IMSS.LS_CES_VEJ }
                                      FieldByName( 'LS_DIAS_BM' ).AsInteger := DataAsInteger( sDatos, 301, 8 );   { LIQ_IMSS.LS_DIAS_BM }
                                      FieldByName( 'LS_SUB_RET' ).AsFloat := DataAsFloat( sDatos, 339, 13 );    { LIQ_IMSS.LS_SUB_RET - No considera LS_APO_VOL, LS_ACT_RET, LS_REC_RET_, etc }
                                      FieldByName( 'LS_INF_NUM' ).AsInteger := DataAsInteger( sDatos, 352, 8 ); { LIQ_IMSS.LS_INF_NUM }
                                      FieldByName( 'LS_INF_TOT' ).AsFloat := DataAsFloat( sDatos, 360, 13 );    { LS_INF_TOT = ( LIQ_IMSS.LS_INF_NAC + LIQ_IMSS.LS_INF_ACR ) }
                                      FieldByName( 'LS_INF_AMO' ).AsFloat := DataAsFloat( sDatos, 373, 13 );    { LIQ_IMSS.LS_INF_AMO }
                                      FieldByName( 'LS_SUB_INF' ).AsFloat := DataAsFloat( sDatos, 386, 13 );    { LIQ_IMSS.LS_SUB_INF }
                                      FieldByName( 'LS_TOTAL' ).AsFloat := DataAsFloat( sDatos, 399, 13 );      { LS_TOTAL = ( LIQ_IMSS.LS_SUB_RET + LIQ_IMSS.LS_SUB_INF ) }
                                 end;
                            end;
                            Post;
                       end;
                  end;
             else
                 begin
                      ZetaDialogo.zError( '� Error !', Format( 'Archivo De Datos del Patr�n ( %s ) Es Inv�lido', [ sFileName ] ), 0 );
                 end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Datos del Patr�n ( Archivo %s )', [ sFileName ] ), Error, 0 );
           end;
     end;
end;

function TdmCDIMSS.CargarEmpleados( const eClase: eEmisionClase; const sFileName: String ): Boolean;
var
   i: Integer;
   sDatos: String;
begin
     Result := False;
     try
        with cdsEmpleados do
        begin
             Init;
             case eClase of
                  eeCMensual:
                  begin
                       AddIntegerField( 'CB_CODIGO' );
                       AddIntegerField( K_FOUND );
                       AddStringField( K_AFILIACION, 30 );
                       AddStringField( K_NUM_SS, 30 );
                       AddIntegerField( 'TB_MODULO' );
                       AddStringField( 'NOMBRE_SS', 255 );
                       AddIntegerField( 'TU_TIP_JOR' );
                       AddStringField( 'CB_CURP', 30 );
                       AddDateField( K_INFONAVIT_INI );
                  end;
                  eeCBimestral:
                  begin
                       AddIntegerField( 'CB_CODIGO' );
                       AddIntegerField( K_FOUND );
                       AddStringField( K_AFILIACION, 30 );
                       AddStringField( K_NUM_SS, 30 );
                       AddIntegerField( 'TB_MODULO' );
                       AddStringField( 'NOMBRE_SS', 255 );
                       AddIntegerField( 'TU_TIP_JOR' );
                       AddStringField( 'CB_CURP', 30 );
                       AddStringField( K_NUM_CRED_INFONAVIT, 30 );
                       AddStringField( 'CB_INFCRED', 30 );
                       AddIntegerField( 'CB_INFTIPO' );
                       AddFloatField( 'CB_INFTASA' );
                       AddDateField( K_INFONAVIT_INI );
                       AddDateField( 'CB_INF_INI' );
                       AddStringField( 'CB_INFACT', 1 );
                  end;
             end;
             CreateTempDataset;

        end;
        with FDatos do
        begin
             Clear;
             LoadFromFile( sFileName );
             for i := 0 to ( Count - 1 ) do
             begin
                  sDatos := Strings[ i ];
                  with cdsEmpleados do
                  begin
                       Append;
                       case eClase of
                            eeCMensual:
                            begin
                                 FieldByName( 'CB_CODIGO' ).AsInteger := -1;
                                 FieldByName( K_FOUND ).AsInteger := 0;
                                 FieldByName( K_AFILIACION ).AsString := DataAsString( sDatos, 24, 11 );
                                 FieldByName( K_NUM_SS ).AsString := FieldByName( K_AFILIACION ).AsString;
                                 FieldByName( 'TB_MODULO' ).AsInteger := DataAsInteger( sDatos, 35, 1 );
                                 FieldByName( 'NOMBRE_SS' ).AsString := DataAsString( sDatos, 36, 50 );
                                 FieldByName( 'TU_TIP_JOR' ).AsInteger := DataAsInteger( sDatos, 86, 1 );
                                 FieldByName( 'CB_CURP' ).AsString := DataAsString( sDatos, 89, 18 );
                                 FieldByName( K_INFONAVIT_INI ).AsDateTime := ZetaCommonClasses.NullDateTime;
                            end;
                            eeCBimestral:
                            begin
                                 FieldByName( 'CB_CODIGO' ).AsInteger := -1;
                                 FieldByName( K_FOUND ).AsInteger := 0;
                                 FieldByName( K_AFILIACION ).AsString := DataAsString( sDatos, 22, 11 );
                                 FieldByName( K_NUM_SS ).AsString := FieldByName( K_AFILIACION ).AsString;
                                 FieldByName( 'TB_MODULO' ).AsInteger := DataAsInteger( sDatos, 33 ,1 );
                                 FieldByName( 'NOMBRE_SS' ).AsString := DataAsString( sDatos, 34, 50 );
                                 FieldByName( 'TU_TIP_JOR' ).AsInteger := DataAsInteger( sDatos, 84 ,1 );
                                 FieldByName( 'CB_CURP' ).AsString := DataAsString( sDatos, 87, 18 );
                                 FieldByName( K_NUM_CRED_INFONAVIT ).AsString := DataAsString( sDatos, 109, 11, True );
                                 with FieldByName( K_NUM_CRED_INFONAVIT ) do
                                 begin
                                      FieldByName( 'CB_INFCRED' ).AsString := Copy( AsString, 2, ( Length( AsString ) ) );
                                 end;

                                 FieldByName( 'CB_INFTIPO' ).AsInteger := DataAsInteger( sDatos, 120 ,1 );
                                 FieldByName( 'CB_INFTASA' ).AsFloat := DataAsFloat( sDatos, 121, 9 );
                                 FieldByName( K_INFONAVIT_INI ).AsDateTime := DataAsDateYear( sDatos, 130, 8 );

                                 if FieldByName( K_INFONAVIT_INI ).AsDateTime = ( NullDateTime + 2 ) then
                                    FieldByName( 'CB_INF_INI' ).AsDateTime := NullDateTime
                                 else
                                     FieldByName( 'CB_INF_INI' ).AsDateTime := FieldByName( K_INFONAVIT_INI ).AsDateTime;
                                 //FieldByName( 'CB_INF_INI' ).AsDateTime := ZetaCommonTools.StrToFecha( Format( '%d', [ FieldByName( K_INFONAVIT_INI ).AsInteger ] ) );

                                 if ( FieldByName( 'CB_INFTIPO' ).AsInteger in [1,2,3] ) then
                                    FieldByName( 'CB_INFACT' ).AsString := 'S'
                                 else
                                     FieldByName( 'CB_INFACT' ).AsString := 'N';


                            end;
                       end;
                       Post;
                  end;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Empleados ( Archivo %s )', [ sFileName ] ), Error, 0 );
           end;
     end;
end;

function TdmCDIMSS.CargarMovimientos( const sAfiliacion: String; const eClase: eEmisionClase; const sFileName: String ): Boolean;
const
     aAfiliacion: array[ eEmisionClase ] of Integer = ( 24, 22 );
     aTipoMovto: array[ eEmisionClase ] of Integer = ( 36, 34 );
     aFecha: array[ eEmisionClase ] of Integer = ( 37, 35 );
     K_FECHA_NULA = 2;
var
   i: Integer;
   sDatos: String;
   FDatosL :TStrings;

 function EsCambioSalarioValido: Boolean;
 begin
      Result := TRUE;
      {Result := NOT ( DataAsInteger( sDatos, aTipoMovto[ eClase ], 1 ) = K_MAESTRA_INICIAL_IMSS ) and
                    ( DataAsDateDay( sDatos, aFecha[ eClase ], 10 ) = 2 ) ;}
 end;

 function SetFecha( const dFecha: TDate ): TDate;
 begin
      Result := dFecha;
      if ( Result = K_FECHA_NULA ) then
      begin
           Result := CodificaFecha( Year, Mes, 1 );
           if ( eClase =  eeCMensual ) then
           begin
                Result := FirstDayOfMonth( Result );
           end
           else
           begin
                Result := FirstDayOfBimestre( Result );

           end;
      end;
 end;

begin
     Result := False;
     FDatosL := TStringList.Create;
     try
        try
        with cdsMovimientos do
        begin
             if NOT Active then
             begin
                  Init;
                  case eClase of
                       eeCMensual:
                       begin
                            AddIntegerField( K_ORIGEN );
                            AddIntegerField( 'LM_TIPO_MOV' );
                            AddDateField( 'LM_FECHA' );
                            AddIntegerField( 'LM_DIAS' );
                            AddFloatField( 'LM_BASE' );
                            AddFloatField( 'LM_EYM_FIJ' );
                            AddFloatField( 'LM_EYM_EXC' );
                            AddFloatField( 'LM_EYM_DIN' );
                            AddFloatField( 'LM_EYM_ESP' );
                            AddFloatField( 'LM_INV_VID' );
                            AddFloatField( 'LM_RIESGOS' );
                            AddFloatField( 'LM_GUARDER' );
                       end;
                       eeCBimestral:
                       begin
                            AddIntegerField( K_ORIGEN );
                            AddIntegerField( 'LM_TIPO_MOV' );
                            AddDateField( 'LM_FECHA' );
                            AddIntegerField( 'LM_DIAS' );
                            AddFloatField( 'LM_BASE' );
                            AddFloatField( 'LM_RETIRO' );
                            AddFloatField( 'LM_CES_VEJ' );
                            AddFloatField( 'LM_INF_PAT' );
                            AddFloatField( 'LM_INF_AMO' );
                       end;
                  end;
                  CreateTempDataset;
                  cdsMovConc.Data := Data;
             end
             else
             begin
                  EmptyDataSet;
             end;
        end;


        cdsMovConc.EmptyDataset;

        with FDatosL do
        begin
             LoadFromFile( sFileName );
             for i := 0 to ( Count - 1 ) do
             begin
                  sDatos := Strings[ i ];
                  if ( DataAsString( sDatos, aAfiliacion[ eClase ], 11 ) = sAfiliacion ) then
                  begin
                       with cdsMovimientos do
                       begin
                            Append;
                            case eClase of
                                 eeCMensual:
                                 begin
                                      FieldByName( K_ORIGEN ).AsInteger := DataAsInteger( sDatos, 35, 1 );
                                      FieldByName( 'LM_TIPO_MOV' ).AsInteger := DataAsInteger( sDatos, 36, 1 );
                                      FieldByName( 'LM_FECHA' ).AsDateTime := SetFecha( DataAsDateDay( sDatos, 37, 10 ) );
                                      FieldByName( 'LM_DIAS' ).AsInteger := DataAsInteger( sDatos, 47, 2 );
                                      FieldByName( 'LM_BASE' ).AsFloat := DataAsFloat( sDatos, 49, 6 );
                                      FieldByName( 'LM_EYM_FIJ' ).AsFloat := DataAsFloat( sDatos, 55, 8 );
                                      FieldByName( 'LM_EYM_EXC' ).AsFloat := DataAsFloat( sDatos, 63, 8 ) + DataAsFloat( sDatos, 71, 8 );
                                      FieldByName( 'LM_EYM_DIN' ).AsFloat := DataAsFloat( sDatos, 79, 8 ) + DataAsFloat( sDatos, 87, 8 );
                                      FieldByName( 'LM_EYM_ESP' ).AsFloat := DataAsFloat( sDatos, 95, 8 ) + DataAsFloat( sDatos, 103, 8 );
                                      FieldByName( 'LM_INV_VID' ).AsFloat := DataAsFloat( sDatos, 119, 8 ) + DataAsFloat( sDatos, 127, 8 );
                                      FieldByName( 'LM_RIESGOS' ).AsFloat := DataAsFloat( sDatos, 111, 8 );
                                      FieldByName( 'LM_GUARDER' ).AsFloat := DataAsFloat( sDatos, 135, 8 );
                                 end;
                                 eeCBimestral:
                                 begin
                                      FieldByName( K_ORIGEN ).AsInteger := DataAsInteger( sDatos, 33, 1 );
                                      FieldByName( 'LM_TIPO_MOV' ).AsInteger := DataAsInteger( sDatos, 34, 1 );
                                      FieldByName( 'LM_FECHA' ).AsDateTime := SetFecha( DataAsDateDay( sDatos, 35, 10 ) );
                                      FieldByName( 'LM_DIAS' ).AsInteger := DataAsInteger( sDatos, 45, 2 );
                                      FieldByName( 'LM_BASE' ).AsFloat := DataAsFloat( sDatos, 47, 6 );
                                      FieldByName( 'LM_RETIRO' ).AsFloat := DataAsFloat( sDatos, 53, 8 );
                                      FieldByName( 'LM_CES_VEJ' ).AsFloat := DataAsFloat( sDatos, 61, 8 ) + DataAsFloat( sDatos, 69, 8 );
                                      FieldByName( 'LM_INF_PAT' ).AsFloat := DataAsFloat( sDatos, 85, 8 );
                                      FieldByName( 'LM_INF_AMO' ).AsFloat := DataAsFloat( sDatos, 93, 8 );
                                 end;
                            end;
                            Post;
                       end;
                  end;
             end;
        end;
        //cdsMovConc.Data := cdsMovimientos.Data;
        cdsMovimientosAfterOpen(cdsMovimientos);
     finally
            FreeAndNil(FDatosL);
     end;
        Result := True;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', Format( 'Error Al Leer Movimientos ( Archivo %s )', [ sFileName ] ), Error, 0 );
           end;
     end;
end;


function TdmCDIMSS.GetFileName( const eTipo: eEmisionTipo ): String;
const
     aEmisionTipo: array[ eEmisionTipo ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'PA', 'AS', 'MO' );
     aEmisionClase: array[ eEmisionClase ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'M', 'B' );
begin
     Result := Format( '%sCDE%s%s99.txt', [ FPath, aEmisionClase[ Clase ], aEmisionTipo[ eTipo ] ] );
end;

function TdmCDIMSS.ConectarTablas: Boolean;
begin
     try
        if FEsCDImss then
        begin
             Result := GetPatronID;
             if Result then
             begin
                  with cdsEmpleados do
                  begin
                       AfterOpen := cdsEmpleadosAfterOpen;
                       OnCalcFields := cdsEmpleadosCalcFields;
                       AlCrearCampos := cdsEmpleadosAlCrearCampos;
                  end;
                  cdsMovimientos.AfterOpen := cdsMovimientosAfterOpen;
                  Result := ConectarTabla( cdsPatron, GetSQLScript( eetPatron ), GetTableName( eetPatron ) ) and
                            ConectarTabla( cdsEmpleados, GetSQLScript( eetEmpleados ), GetTableName( eetEmpleados ) );
             end;
        end
        else
        begin
             with cdsEmpleados do
             begin
                  AfterOpen := nil;
                  OnCalcFields := nil;
                  AlCrearCampos := nil;
             end;
             cdsMovimientos.AfterOpen := nil;

             Result := CargarPatron( Clase, GetFileName( eetPatron  ) ) and
                       CargarEmpleados( Clase, GetFileName( eetEmpleados ) ); {and
                       CargarMovimientos( Clase, GetFileName( eetMovimientos ) );}
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error !', 'Error Al Abrir Tablas De CD IMSS', Error, 0 );
                Result := False;
           end;
     end;
end;

{ *********** Conciliaci�n ************ }




function TdmCDIMSS.GetDescTipoMov( const sCampo: String ): String;
var
   Campo: TField;
begin
     Result := 'Indefinido';
     Campo := cdsMovConc.FieldByName( sCampo );
     if Assigned( Campo ) then
     begin
          Case Campo.AsInteger of
               K_MAESTRA_EAMP_ALTA : Result := 'Alta (%d)';
               K_MAESTRA_EAMP_ALTA2 : Result := 'Alta (%d)';
               K_MAESTRA_EAMP_BAJA : Result := 'Baja (%d)';
               K_MAESTRA_EAMP_CAMBIO_COTIZACION : Result := 'Cambio (%d)';
               K_MAESTRA_EAMP_RESTABLECIMIENTO : Result := 'Reingreso (%d)';
               K_MAESTRA_INICIAL_IMSS : Result := '';
               //K_MAESTRA_INICIAL_SUA : Result := '';
          end;
          if StrLleno( Result ) then
             Result := Format( Result, [Campo.AsInteger] );
     end;
end;



procedure TdmCDIMSS.ConciliarListaMovimientos( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings );

var
   dFecha: TDate;
   sFecha: String;
   iTipo: integer;
   lEncontrado,lValidaCambiosSalario: Boolean;


 procedure CampoEmpleado( const sNombre,sCampo: String );
 const
     K_DIFERENTES = 'Diferentes';
 var
   oTress, oIMSS: TField;
   sTress, sIMSS, sDiferencia: String;
 begin
     oTress := Original.FieldByName( sCampo );
     oIMSS := Datos.FieldByName( sCampo );

     sDiferencia := VACIO;

     if Assigned( oTress ) and Assigned( oIMSS ) then
     begin
          sTress := GetCampoAsString( oTress );
          sIMSS := GetCampoAsString( oIMSS );
          if TienenMismoTipo( oTress, oIMSS ) then
          begin
               case oTress.DataType of
                    ftString: sDiferencia := GetDiferencias( ( oTress.AsString = oIMSS.AsString ), sDiferencia );
                    ftSmallint: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftInteger: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftWord: sDiferencia := GetDiferencias( ( oTress.AsInteger = oIMSS.AsInteger ), GetIntegerAsStr( oTress.AsInteger - oIMSS.AsInteger ) );
                    ftBoolean: sDiferencia := GetDiferencias( ( oTress.AsBoolean = oIMSS.AsBoolean ), sDiferencia );
                    ftFloat: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsFloat, oIMSS.AsFloat, FPrecision ), GetFloatAsStr( oTress.AsFloat - oIMSS.AsFloat ) );
                    ftCurrency: sDiferencia := GetDiferencias( ZetaCommonTools.PesosIgualesP( oTress.AsCurrency, oIMSS.AsCurrency, FPrecision ), GetFloatAsStr( oTress.AsCurrency - oIMSS.AsCurrency ) );
                    ftDate: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
                    ftDateTime: sDiferencia := GetDiferencias( ( oTress.AsDateTime = oIMSS.AsDateTime ), sDiferencia );
               else
                   sDiferencia := 'YYY';
               end;
          end
          else
               sDiferencia := 'XXX';
     end
     else
     begin
          sDiferencia := VACIO;
     end;

     if ( sDiferencia <> K_IGUALES ) and StrLleno(sDiferencia) then
     begin
          if lTress then
          begin
               AgregaRegistro( cdsDatosEmpleados, [ dmCliente.cdsLiqEmp.FieldByName('CB_SEGSOC').AsString,
                                                   PrettyName( dmCliente.cdsLiqEmp.FieldByName('CB_APE_PAT').AsString,
                                                               dmCliente.cdsLiqEmp.FieldByName('CB_APE_MAT').AsString,
                                                               dmCliente.cdsLiqEmp.FieldByName('CB_NOMBRES').AsString ),
                                                   dmCliente.cdsLiqEmp.FieldByName('CB_CODIGO').AsString,
                                                   sNombre,oTress.AsString,oImss.AsString,sDiferencia ] )
          end
          else
          begin
               AgregaRegistro( cdsDatosEmpleados, [ cdsEmpleados.FieldByName(K_NUM_SS).AsString,
                                                   cdsEmpleados.FieldByName('NOMBRE_SS').AsString,
                                                   '0',
                                                   sNombre,oTress.AsString,oImss.AsString,sDiferencia ] );
          end;
     end;

 end;


begin
     {Si se hace opcional esta validacion, esta variable va en funcion al contro que se ponga en interfase de usuario}
     lValidaCambiosSalario := TRUE;



     with Original do
     begin
          if FEsCDImss OR ( not FEsCDImss AND FComparaMontos ) then
          begin
               case Clase of
                    eeCMensual:
                    begin
                         FieldByName( 'LM_EYM_FIJ' ).Tag := 1;
                         FieldByName( 'LM_EYM_EXC' ).Tag := 1;
                         FieldByName( 'LM_EYM_DIN' ).Tag := 1;
                         FieldByName( 'LM_EYM_ESP' ).Tag := 1;
                         FieldByName( 'LM_INV_VID' ).Tag := 1;
                         FieldByName( 'LM_RIESGOS' ).Tag := 1;
                         FieldByName( 'LM_GUARDER' ).Tag := 1;
                    end;
                    eeCBimestral:
                    begin
                         FieldByName( 'LM_RETIRO' ).Tag := 1;
                         FieldByName( 'LM_CES_VEJ').Tag := 1;
                         FieldByName( 'LM_INF_PAT').Tag := 1;
                         FieldByName( 'LM_INF_AMO').Tag := 1;
                    end;
               end;
          end;

          FieldByName( 'LM_DIAS' ).Tag := 1;
          FieldByName( 'LM_BASE' ).Tag := 1;

          First;
          while not Eof do
          begin
               dFecha := FieldByName( 'LM_FECHA' ).AsDateTime;
               sFecha := ZetaCommonTools.FechaAsStr( dFecha );
               iTipo := FieldByName( K_TIPO_MOV ).AsInteger;

               lEncontrado := Datos.Locate( Format( 'LM_FECHA;%s', [ K_TIPO_MOV ] ), VarArrayOf( [ sFecha, iTipo ] ), [] );

               if lTress AND lValidaCambiosSalario AND (NOT lEncontrado) AND (iTipo = K_MAESTRA_EAMP_CAMBIO_COTIZACION) then
               begin
                    lEncontrado := Datos.Locate( Format( 'LM_FECHA;%s', [ K_TIPO_MOV ] ), VarArrayOf( [ sFecha, K_MAESTRA_INICIAL ] ), [] );
               end;



               if lEncontrado then
               begin
                    if CamposSonDiferentes( Original, Datos ) then
                    begin
                         SetDiferencias;
                         AddMovimiento( Original, lTress );
                         AddMovimiento( Datos, not lTress );

                         CampoEmpleado( K_Dias, 'LM_DIAS' );
                         CampoEmpleado( K_Integrado, 'LM_BASE');

                         case Clase of
                              eeCMensual:
                              begin
                                   CampoEmpleado(  K_CUOTA_FIJA, 'LM_EYM_FIJ' );
                                   CampoEmpleado(  K_Excedente, 'LM_EYM_EXC' );
                                   CampoEmpleado(  K_Dinero, 'LM_EYM_DIN' );
                                   CampoEmpleado(  K_Especie , 'LM_EYM_ESP' );
                                   CampoEmpleado(  K_Riesgos , 'LM_RIESGOS' );
                                   CampoEmpleado(  K_Invalidez , 'LM_INV_VID' );
                                   CampoEmpleado(  K_Guarderias, 'LM_GUARDER'  );
                              end;
                              eeCBimestral:
                              begin
                                   CampoEmpleado( K_RETIRO, 'LM_RETIRO' );
                                   CampoEmpleado( K_Cesantia, 'LM_CES_VEJ'  );
                                   CampoEmpleado( K_Infonavit, 'LM_INF_PAT'  );
                                   CampoEmpleado( k_amortiza, 'LM_INF_AMO' );
                              end;
                         end;

                         //Movimientos para la bitacora
                         //RegistraBitacora( cdsMovtos, Original, lTress );
                         //RegistraBitacora( cdsMovtos, Datos, not lTress );


                         //RegistraBitacora( Original, Datos, lTress );
                    end;
                    { Evita que se vuelva a procesar }
                    Datos.Delete;
               end
               else
               begin
                    SetDiferencias;
                    AddMovimiento( Original, lTress );

                    if lTress then
                       RegistraBitacora( cdsMovtosTress, Original, lTress )
                    else
                        RegistraBitacora( cdsMovtosImss, Original, lTress );

               end;
               Next;
          end;
     end;
end;



procedure TdmCDIMSS.ConciliarMovimientos( const iEmpleado: TNumEmp; const sAfiliacion: String; Log: TStrings  );
const
     K_ANCHO_COL_MOV = 15;
     K_FILLER = '-';
var
   sOrigen, sTexto: String;
   i, iTope: Integer;



   function GetDescripcionTipoMov( const iTipoMov: integer ): String;
   begin
     Result := 'Indefinido';

          Case iTipoMov of
               K_MAESTRA_EAMP_ALTA : Result := 'Alta (%d)';
               K_MAESTRA_EAMP_ALTA2 : Result := 'Alta (%d)';
               K_MAESTRA_EAMP_BAJA : Result := 'Baja (%d)';
               K_MAESTRA_EAMP_CAMBIO_COTIZACION : Result := 'Cambio (%d)';
               K_MAESTRA_EAMP_RESTABLECIMIENTO : Result := 'Reingreso (%d)';
               K_MAESTRA_INICIAL_IMSS : Result := 'Maestra inicial IMSS (%d)';
               //K_MAESTRA_INICIAL_SUA : Result := '';
          end;
          if StrLleno( Result ) then
             Result := Format( Result, [iTipoMov] );

     end;



   procedure AgruparAmortizaciones;
   var
      iTipo : integer;
   begin

        case Clase of
                   eeCMensual:
                   begin
                   end;
                   eeCBimestral:
                   begin
                        with  cdsMovimientos do
                        begin
                             First;
                             while not Eof do
                             begin
                                  iTipo :=   FieldByName( K_TIPO_MOV ).AsInteger;
                                  {
                                     AddIntegerField('FOLIO');
                                     AddStringField('CB_SEGSOC',30);
                                     AddStringField('PRETTYNAME',255);
                                     AddIntegerField('CB_CODIGO');
                                     AddStringField( 'DESCRIPCION', 100 );
                                     AddFloatField( 'TRESS' );
                                     AddFloatField( 'CD_IMSS' );
                                     AddFloatField('DIFERENCIA');
                                  }
                                  AgregaRegistro( cdsDatosEmpleadosTodos, [ sAfiliacion,
                                                  GetDescripcionTipoMov( iTipo) ,
                                                  IntToStr(iEmpleado),
                                                  '',
                                                  '0.00',
                                                  FieldByName('LM_INF_AMO').AsString,
                                                  '0.00' ] );
                                  Next;
                             end;
                             First;
                        end;


                   end;
               end;
   end;

begin
     Log.Clear;
     if dmCliente.ConectarMovimientos( Clase, iEmpleado ) and ConectarMovimiento( sAfiliacion ) then
     begin
          AgruparAmortizaciones;
          ConciliarListaMovimientos( dmCliente.cdsLiqMov, cdsMovimientos, True, Log );
          ConciliarListaMovimientos( cdsMovimientos, dmCliente.cdsLiqMov, False, Log );
          with cdsMovConc do
          begin
               if ( RecordCount > 0 ) then
               begin
                    { Agregar Nombres de Campos }
                    case Clase of
                         eeCMensual:
                         begin
                              sTexto := ZetaCommonTools.PadL( 'EYM Fijo', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'EYM Excede', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'EYM Dinero', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'EYM Especie', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'Inv. y Vida', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'Riesgos', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'Guarder�as', K_ANCHO_COL_MOV ) + K_ESPACIO;
                         end;
                         eeCBimestral:
                         begin
                              sTexto := ZetaCommonTools.PadL( 'Retiro', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'Cesant�a/Vejez', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'INFONAVIT', K_ANCHO_COL_MOV ) + K_ESPACIO +
                                        ZetaCommonTools.PadL( 'Amortizaci�n', K_ANCHO_COL_MOV ) + K_ESPACIO;

                         end;
                    end;
                    Log.Add( VACIO );
                    Log.Add( ZetaCommonTools.PadR( 'Origen', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Fecha', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Tipo', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'D�as', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             ZetaCommonTools.PadL( 'Salario Base', K_ANCHO_COL_MOV ) + K_ESPACIO +
                             sTexto );
                    { Agregar L�neas de Encabezado }
                    sTexto := VACIO;
                    case Clase of
                         eeCMensual: iTope := 12;
                    else
                        iTope := 9;
                    end;
                    for i := 1 to iTope do
                    begin
                         sTexto := sTexto + StringOfChar( K_FILLER, K_ANCHO_COL_MOV ) + K_ESPACIO;
                    end;
                    Log.Add( sTexto );
                    { Agregar Datos }
                    First;
                    while not Eof do
                    begin
                         case eOrigen( FieldByName( K_ORIGEN ).AsInteger ) of
                            eoTress: sOrigen := 'Tress'
                         else
                             begin
                                  if FEsCDImss then
                                     sOrigen := 'CD IMSS'
                                  else
                                      sOrigen := 'IDSE';

                             end;
                         end;
                         case Clase of
                              eeCMensual:
                              begin
                                   sTexto := ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_EYM_FIJ' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_EYM_EXC' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_EYM_DIN' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_EYM_ESP' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_INV_VID' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_RIESGOS' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_GUARDER' ), K_ANCHO_COL_MOV ) + K_ESPACIO;
                              end;
                              eeCBimestral:
                              begin
                                   sTexto := ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_RETIRO' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_CES_VEJ' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_INF_PAT' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                             ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_INF_AMO' ), K_ANCHO_COL_MOV ) + K_ESPACIO;

                              end;
                         end;
                         Log.Add( ZetaCommonTools.PadR( sOrigen, K_ANCHO_COL_MOV ) + K_ESPACIO +
                                  ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_FECHA' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                  ZetaCommonTools.PadL( GetDescTipoMov( K_TIPO_MOV ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                  ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_DIAS' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                  ZetaCommonTools.PadL( GetCampoMovimiento( 'LM_BASE' ), K_ANCHO_COL_MOV ) + K_ESPACIO +
                                  sTexto );
                         Next;
                    end;
               end;
          end;
     end;
end;

procedure TdmCDIMSS.ConciliarListaEmpleados( Original, Datos: TDataset; const lTress: Boolean; Log: TStrings  );
const
     K_FILLER = '=';
     K_FILLER_ANCHO = 60;
var
   sSeguroSocial, sTexto: String;
   iEmpleado: TNumEmp;


 procedure CampoEmpleado( const sNombre, sCampo: String );
 begin
      ConciliarCampoEmpleado( sNOmbre, sCampo, FEmpleados, cdsDatosEmpleados, FALSE );
 end;

begin
     FEmpNotFound.Clear;
     with Original do
     begin
          First;
          while not Eof and TressShell.ConciliarStep do
          begin
               if ( FieldByName( K_FOUND ).AsInteger = 0 ) then
               begin
                    sSeguroSocial := Trim(FieldByName( K_NUM_SS ).AsString);//Error al tener seguro social con espacios
                    if lTress then
                    begin
                         sTexto := Format( '# IMSS %s : %s %s, %s ( # Tress %d )', [ sSeguroSocial, FieldByName( 'CB_APE_PAT' ).AsString, FieldByName( 'CB_APE_MAT' ).AsString, FieldByName( 'CB_NOMBRES' ).AsString, FieldByName( 'CB_CODIGO' ).AsInteger ] );
                    end
                    else
                    begin
                         sTexto := Format( '# IMSS %s : %s', [ sSeguroSocial, FieldByName( 'NOMBRE_SS' ).AsString ] );
                    end;
                    if Datos.Locate( K_NUM_SS, sSeguroSocial, [] ) then
                    begin
                         iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
                         SetEmployeeFound( Original );
                         SetEmployeeFound( Datos );
                         FEmpleados.Clear;
                         case Clase of
                              eeCMensual:
                              begin
                                   ConciliarCampoEmpleado( 'M�dulo', 'TB_MODULO', FEmpleados, cdsDatosEmpleados );
                                   ConciliarCampoEmpleado( 'Jornada', 'TU_TIP_JOR', FEmpleados, cdsDatosEmpleados );
                                   //if RevisaCurp then
                                      ConciliarCampoEmpleado( 'CURP', 'CB_CURP', FEmpleados, cdsCurp );
                              end;
                              eeCBimestral:
                              begin
                                      ConciliarCampoEmpleado( 'M�dulo', 'TB_MODULO', FEmpleados, cdsDatosEmpleados );
                                      ConciliarCampoEmpleado( 'Jornada', 'TU_TIP_JOR', FEmpleados, cdsDatosEmpleados );
                                      //if RevisaCurp then
                                      ConciliarCampoEmpleado( 'CURP', 'CB_CURP', FEmpleados, cdsCurp );
                                      if EsTipoDescuentoValidoEmpleado then
                                      begin
                                          ConciliarCampoEmpleado( K_NUMERO_CREDITO, 'CB_INFCRED', FEmpleados, cdsInfonavit, Ord( DifInfNoCredito ) );
                                          ConciliarCampoEmpleado( K_TIPO_DESCUENTO, 'CB_INFTIPO', FEmpleados, cdsInfonavit,Ord( DifInfTipoDescuento) );
                                          if ( not zStrToBool( Original.FieldByName('CB_INFDISM').AsString ) or   //Si aplica disminuci�n de tasa no se concilia.
                                             ( ( iEmpleado = cdsInfonavit.FieldByName('CB_CODIGO').AsInteger ) and  //o su ultima diferencia es T.Desc.
                                               ( eTipoDifInfonavit( cdsInfonavit.FieldByName('DIFERENCIA').AsInteger ) = DifInfTipoDescuento ) ) )  then
                                          begin
                                               ConciliarCampoEmpleado( K_VALOR_DESCUENTO, 'CB_INFTASA', FEmpleados, cdsInfonavit, Ord( DifInfValorDesc) );
                                          end;
                                          //if RevisaInfonavit then
                                             ConciliarCampoEmpleado( K_INICIO_CREDITO, 'CB_INF_INI', FEmpleados, cdsInfonavit, Ord(DifInfOtorgamiento) );

                                          ConciliarCampoEmpleado( K_REINICIO_CREDITO, 'CB_INFACT', FEmpleados, cdsInfonavit, Ord( DifReinicioDescuento ) );
                                      end;
                              end;
                         end;

                         if ( iEmpleado > 0 ) then
                         begin
                              ConciliarMovimientos( iEmpleado, sSeguroSocial, FMovimientos );
                         end;
                         if ( FEmpleados.Count > 0 ) or ( FMovimientos.Count > 0 ) then
                         begin
                              ListaEspacio;
                              ListaDelimitador( '_', '', K_ANCHO );
                              ListaDelimitador( '_', '', K_ANCHO );
                              ListaEspacio;
                              ListaSubFiller( sTexto );
                              ListaEspacio;
                         end;
                         if ( FEmpleados.Count > 0 ) then
                         begin
                              ListaEncabezados;
                              ListaAgregar( FEmpleados, FLista );
                         end;
                         if ( FMovimientos.Count > 0 ) then
                         begin
                              ListaAgregar( FMovimientos, FLista );
                         end;
                    end
                    else
                    begin
                         SetDiferencias;
                         FEmpNotFound.Add( sTexto );
                         if lTress then
                            AgregaRegistro( cdsEmpleadosTress, [ sSeguroSocial,
                                                                 PrettyName( FieldByName( 'CB_APE_PAT' ).AsString,
                                                                             FieldByName( 'CB_APE_MAT' ).AsString,
                                                                             FieldByName( 'CB_NOMBRES' ).AsString),
                                                                FieldByName( 'CB_CODIGO' ).AsString ] )
                         else
                             AgregaRegistro( cdsEmpleadosIMSS, [ sSeguroSocial, FieldByName( 'NOMBRE_SS' ).AsString ] )
                    end;
               end;
               Next;
          end;
     end;
     if ( FEmpNotFound.Count > 0 ) then
     begin
          with Log do
          begin
               Add( VACIO );
               Add( StringOfChar( K_FILLER, K_FILLER_ANCHO ) );
               if lTress then
                  Add( 'Empleados de Tress Que No Aparecen En CD IMSS' )
               else
                   Add( 'Empleados En CD IMSS Que No Aparecen En Tress' );
               Add( StringOfChar( K_FILLER, K_FILLER_ANCHO ) );
          end;
          ListaAgregar( FEmpNotFound, Log );
     end;
end;

procedure TdmCDIMSS.ConciliarEmpleados;
var
   FDatos: TStrings;
begin
     ListaEspacio;
     ListaEspacio;
     ListaFiller;
     ListaFiller( 'EMPLEADOS' );
     ListaFiller;
     ListaEspacio;
     FDatos := TStringList.Create;
     try
        ConciliarListaEmpleados( dmCliente.cdsLiqEmp, cdsEmpleados, True, FDatos );
        ConciliarListaEmpleados( cdsEmpleados, dmCliente.cdsLiqEmp, False, FDatos );
        if ( FDatos.Count > 0 ) then
        begin
             ListaAgregar( FDatos, Lista );
        end;
        if ( Clase = eecBimestral )  then
        begin
             SetCreditosNoActivos(cdsInactivosTress, ImssSITressNO {TRUE} );
             SetCreditosNoActivos(cdsInactivosImss,  ImssNOTressSI {FALSE});
             SetCreditosNoActivos(cdsInactivosImssSuspendidosTress,  ImssNOTressSuspendido );
             SetCreditosNoActivos(cdsSuspendidosTress,  ImssSITressSuspendido );

        end;
     finally
            FreeAndNil( FDatos );
     end;
end;

procedure TdmCDIMSS.ConciliarPatrones;
 const
      aEmisionClase: array[ eEmisionClase ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Mensual', 'Bimestral' );
 var
    rTotalTress, rTotalCdImss: TPesos;
    
begin
     with dmCliente do
     begin
          ListaEspacio;
          ListaTexto( Format( 'Conciliaci�n Del Patr�n: %s ( # %s )', [ IMSSRegistro, IMSSPatron ] ) );
          ListaTexto( Format( '                    A�o: %d', [ IMSSYear ] ) );
          ListaTexto( Format( '                    Mes: %s', [ ZetaCommonLists.ObtieneElemento( lfMeses, IMSSMes - 1 ) ] ) );
          ListaTexto( Format( '                   Tipo: %s', [ ZetaCommonLists.ObtieneElemento( lfTipoLiqIMSS, Ord( IMSSTipo ) ) ] ) );
          ListaTexto( Format( '                Periodo: %s', [ aEmisionClase[ Clase ] ] ) );
     end;
     ListaEspacio;
     ListaEspacio;    
     ListaFiller;
     ListaFiller( 'TOTALES' );
     ListaFiller;
     ListaEspacio;
     ListaEncabezados;
     case Clase of
          eeCMensual:
          begin
               ConciliarCampoPatron( 'Asegurados', 'LS_NUM_TRA' );
               ConciliarCampoPatron( 'D�as Cotizados', 'LS_DIAS_CO' );
               ConciliarCampoPatron( 'Prima de Riesgo', 'PRIMA_RT' );
               ConciliarCampoPatron( 'EYM: Cuota Fija', 'LS_EYM_FIJ' );
               ConciliarCampoPatron( 'EYM: Excedente', 'LS_EYM_EXC' );
               ConciliarCampoPatron( 'EYM: Dinero', 'LS_EYM_DIN' );
               ConciliarCampoPatron( 'EYM: Especie', 'LS_EYM_ESP' );
               ConciliarCampoPatron( 'Guarder�as', 'LS_GUARDER' );
               ConciliarCampoPatron( 'Riesgo de Trabajo', 'LS_RIESGOS' );
               ConciliarCampoPatron( 'Invalidez y Vida', 'LS_INV_VID' );

          end;
          eeCBimestral:
          begin
               ConciliarCampoPatron( 'Asegurados', 'LS_NUM_BIM' );
               ConciliarCampoPatron( 'D�as Cotizados', 'LS_DIAS_BM' );
               ConciliarCampoPatron( 'Acreditados', 'LS_INF_NUM' );
               ConciliarCampoPatron( 'Retiro', 'LS_RETIRO' );
               ConciliarCampoPatron( 'Cesant�a y Vejez', 'LS_CES_VEJ' );
               ConciliarCampoPatron( 'SubTotal RCV        (+)', 'LS_SUB_RET' );
               ConciliarCampoPatron( '5% INFONAVIT', 'LS_INF_TOT' );
               ConciliarCampoPatron( 'Amortizaciones', 'LS_INF_AMO' );
               ConciliarCampoPatron( 'SubTotal INFONAVIT  (+)', 'LS_SUB_INF' );

               with dmCliente.cdsLiqIMSS do
                    rTotalTress := FieldByName('LS_SUB_RET').AsFloat +
                                   FieldByName('LS_SUB_INF').AsFloat;

               with cdsPatron do
                    rTotalCdImss := FieldByName('LS_SUB_RET').AsFloat +
                                    FieldByName('LS_SUB_INF').AsFloat;

               if NOT ZetaCommonTools.PesosIguales( rTotalTress, rTotalCdImss ) then
               begin
                    ListaEspacio;
                    ListaTexto( ZetaCommonTools.PadR( 'Total               (=)', K_ANCHO_COLUMNA ) + K_ESPACIO +
                                ZetaCommonTools.PadL( GetFloatAsStr( rTotalTress ), K_ANCHO_COLUMNA ) + K_ESPACIO +
                                ZetaCommonTools.PadL( GetFloatAsStr( rTotalCdImss ), K_ANCHO_COLUMNA ) + K_ESPACIO +
                                ZetaCommonTools.PadL( GetFloatAsStr(  rTotalTress - rTotalCdImss ), K_ANCHO_COLUMNA ) );
               end;

          end;
     end;
     TressShell.ConciliarStep;
end;

procedure TdmCDIMSS.Conciliar;
var
   iSteps: Integer;
begin
     FDiferencias := 0;
     iSteps := cdsEmpleados.RecordCount + dmCliente.cdsLiqEmp.RecordCount + 1;
     TressShell.ConciliarStart( iSteps );
     try
        ResetBitacoras;
        ConciliarPatrones;
        ConciliarEmpleados;
        FinishBitacoras;
     finally
            if TressShell.CancelaConciliacion then
            begin
                 ListaEspacio;
                 ListaFiller;
                 ListaFiller( 'Proceso Cancelado por el Usuario' );
                 ListaFiller;
            end;
            TressShell.ConciliarEnd;
     end;
end;


function TdmCDIMSS.GetConciliaDescripcion: string;
begin
     Result := 'CD Imss';
end;



end.
