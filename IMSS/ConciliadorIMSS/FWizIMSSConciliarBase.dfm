inherited WizIMSSConciliarBase: TWizIMSSConciliarBase
  Left = 0
  Top = 0
  Caption = 'WizIMSSConciliarBase'
  ClientHeight = 440
  ClientWidth = 611
  Font.Name = 'Tahoma'
  OldCreateOrder = False
  ExplicitWidth = 617
  ExplicitHeight = 468
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 611
    Height = 440
    ExplicitWidth = 611
    ExplicitHeight = 440
    inherited Parametros: TdxWizardControlPage
      ExplicitWidth = 589
      ExplicitHeight = 300
      object ValoresActivosgb: TcxGroupBox
        Left = 0
        Top = 122
        Hint = ''
        Align = alBottom
        Caption = 'Liquidaci'#243'n a conciliar '
        TabOrder = 0
        Height = 178
        Width = 589
        object EmisionLBL: TcxLabel
          Left = 23
          Top = 140
          Hint = ''
          Caption = 'Periodo:'
          Transparent = True
        end
        object YearLBL: TcxLabel
          Left = 41
          Top = 60
          Hint = ''
          Caption = 'A'#241'o:'
          Transparent = True
        end
        object MesLBL: TcxLabel
          Left = 41
          Top = 87
          Hint = ''
          Caption = 'Mes:'
          Transparent = True
        end
        object TipoLBL: TcxLabel
          Left = 40
          Top = 115
          Hint = ''
          Caption = 'Tipo:'
          Transparent = True
        end
        object PatronLBL: TcxLabel
          Left = 28
          Top = 34
          Hint = ''
          Caption = 'Patr'#243'n:'
          Transparent = True
        end
        object Mes: TcxStateComboBox
          Left = 70
          Top = 86
          Hint = ''
          AutoSize = False
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediateDropDownWhenKeyPressed = False
          Properties.IncrementalSearch = False
          TabOrder = 2
          OnClick = MesClick
          ListaFija = lfMeses
          ListaVariable = lvPuesto
          EsconderVacios = False
          LlaveNumerica = True
          MaxItems = 10
          Offset = 1
          Height = 21
          Width = 90
        end
        object Tipo: TcxStateComboBox
          Left = 70
          Top = 113
          Hint = ''
          AutoSize = False
          Properties.DropDownListStyle = lsFixedList
          Properties.ImmediateDropDownWhenKeyPressed = False
          Properties.IncrementalSearch = False
          TabOrder = 3
          ListaFija = lfTipoLiqIMSS
          ListaVariable = lvPuesto
          EsconderVacios = False
          LlaveNumerica = True
          MaxItems = 10
          Offset = 0
          Height = 21
          Width = 120
        end
        object Patron: TZetaKeyLookup_DevEx
          Left = 71
          Top = 32
          Width = 317
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 40
        end
        object Year: TcxSpinEdit
          Left = 70
          Top = 58
          Hint = 'Seleccionar el a'#241'o'
          ParentShowHint = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taBottomJustify
          Properties.AssignedValues.DisplayFormat = True
          Properties.MaxValue = 2050.000000000000000000
          Properties.MinValue = 1899.000000000000000000
          Properties.SpinButtons.Position = sbpHorzLeftRight
          ShowHint = True
          TabOrder = 1
          Value = 1899
          Width = 90
        end
        object EmisionMensual: TcxRadioButton
          Left = 70
          Top = 140
          Width = 70
          Height = 17
          Caption = 'Mensual'
          Checked = True
          TabOrder = 4
          TabStop = True
          OnClick = EmisionMensualClick
          Transparent = True
        end
        object EmisionBimestral: TcxRadioButton
          Left = 146
          Top = 140
          Width = 63
          Height = 17
          Caption = 'Bimestral'
          TabOrder = 5
          OnClick = EmisionBimestralClick
          Transparent = True
        end
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      ExplicitWidth = 589
      ExplicitHeight = 300
      inherited GrupoParametros: TcxGroupBox
        ExplicitWidth = 589
        ExplicitHeight = 164
        Height = 164
        Width = 589
      end
      inherited cxGroupBox1: TcxGroupBox
        ExplicitWidth = 589
        Width = 589
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          ExplicitLeft = 67
          ExplicitTop = 20
          ExplicitWidth = 521
          ExplicitHeight = 74
          Width = 521
          AnchorY = 57
        end
        inherited cxImage1: TcxImage
          ExplicitLeft = 1
          ExplicitTop = 20
          ExplicitHeight = 74
        end
      end
      object PanelProgressBar: TPanel
        Left = 0
        Top = 259
        Width = 589
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object ProgressBar: TcxProgressBar
          Left = 62
          Top = 11
          Hint = ''
          TabOrder = 0
          Width = 465
        end
      end
    end
  end
end
