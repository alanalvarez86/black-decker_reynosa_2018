inherited IMSSDatosTot: TIMSSDatosTot
  Left = 173
  Top = 134
  Caption = 'Totales de IMSS'
  ClientHeight = 391
  ClientWidth = 625
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 625
    Height = 18
    inherited ImgNoRecord: TImage
      Height = 18
    end
    inherited Slider: TSplitter
      Left = 323
      Height = 18
    end
    inherited ValorActivo1: TPanel
      Width = 307
      Height = 18
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 299
      Height = 18
    end
  end
  object PageControl1: TPageControl [1]
    Left = 0
    Top = 18
    Width = 625
    Height = 373
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    TabPosition = tpBottom
    OnMouseDown = GenericoMouseDown
    object TabSheet1: TTabSheet
      Caption = 'Mensuales'
      OnMouseDown = GenericoMouseDown
      object GBResumenMensual: TGroupBox
        Left = 229
        Top = 0
        Width = 232
        Height = 225
        Caption = ' Resumen Mensual '
        TabOrder = 0
        OnDblClick = GBDblClick
        object Label20: TLabel
          Left = 62
          Top = 19
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total Mensual:'
        end
        object LS_TOT_MES: TZetaDBTextBox
          Left = 138
          Top = 18
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_MES'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_MES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label19: TLabel
          Left = 73
          Top = 37
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Asegurados:'
        end
        object LS_NUM_TRA: TZetaDBTextBox
          Left = 138
          Top = 36
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_NUM_TRA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_NUM_TRA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_DIAS_CO: TZetaDBTextBox
          Left = 138
          Top = 54
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_DIAS_CO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_DIAS_CO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 57
          Top = 55
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as Cotizados:'
        end
        object LS_FAC_ACT: TZetaDBTextBox
          Left = 138
          Top = 72
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_FAC_ACT'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_FAC_ACT'
          DataSource = DataSource
          FormatFloat = '%14.5n'
          FormatCurrency = '%m'
        end
        object Label24: TLabel
          Left = 7
          Top = 74
          Width = 125
          Height = 13
          Alignment = taRightJustify
          Caption = 'Factor de Actualizaciones:'
        end
        object Label18: TLabel
          Left = 41
          Top = 92
          Width = 91
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa de Recargos:'
        end
        object LS_FAC_REC: TZetaDBTextBox
          Left = 138
          Top = 90
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_FAC_REC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_FAC_REC'
          DataSource = DataSource
          FormatFloat = '%14.3n'
          FormatCurrency = '%m'
        end
        object Label34: TLabel
          Left = 68
          Top = 131
          Width = 64
          Height = 13
          Caption = 'IMSS Obrero:'
        end
        object Label35: TLabel
          Left = 61
          Top = 149
          Width = 71
          Height = 13
          Caption = 'IMSS Patronal:'
        end
        object LS_IMSS_OB: TZetaDBTextBox
          Left = 138
          Top = 129
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_IMSS_OB'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_IMSS_OB'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_IMSS_PA: TZetaDBTextBox
          Left = 138
          Top = 147
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_IMSS_PA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_IMSS_PA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBCuentaImss: TGroupBox
        Left = 8
        Top = 0
        Width = 217
        Height = 224
        Caption = ' Cuenta de IMSS '
        TabOrder = 1
        OnDblClick = GBDblClick
        object Label1: TLabel
          Left = 41
          Top = 21
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Cuota Fija:'
        end
        object LS_EYM_FIJ: TZetaDBTextBox
          Left = 126
          Top = 19
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_FIJ'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_FIJ'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label2: TLabel
          Left = 37
          Top = 39
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Excedente:'
        end
        object LS_EYM_EXC: TZetaDBTextBox
          Left = 126
          Top = 37
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_EXC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_EXC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label3: TLabel
          Left = 57
          Top = 57
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Dinero:'
        end
        object LS_EYM_DIN: TZetaDBTextBox
          Left = 126
          Top = 55
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_DIN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_DIN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_EYM_ESP: TZetaDBTextBox
          Left = 126
          Top = 74
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_EYM_ESP'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_EYM_ESP'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label4: TLabel
          Left = 50
          Top = 76
          Width = 70
          Height = 13
          Alignment = taRightJustify
          Caption = 'EYM: Especie:'
        end
        object Label7: TLabel
          Left = 64
          Top = 94
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Guarder'#237'as:'
        end
        object LS_GUARDER: TZetaDBTextBox
          Left = 126
          Top = 92
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_GUARDER'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_GUARDER'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label10: TLabel
          Left = 25
          Top = 113
          Width = 95
          Height = 13
          Alignment = taRightJustify
          Caption = 'Riesgos de Trabajo:'
        end
        object LS_RIESGOS: TZetaDBTextBox
          Left = 126
          Top = 111
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_RIESGOS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_RIESGOS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label11: TLabel
          Left = 43
          Top = 131
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Invalidez y Vida:'
        end
        object LS_INV_VID: TZetaDBTextBox
          Left = 126
          Top = 129
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INV_VID'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INV_VID'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label12: TLabel
          Left = 45
          Top = 149
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubTotal IMSS:'
        end
        object LS_SUB_IMS: TZetaDBTextBox
          Left = 126
          Top = 147
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_SUB_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_SUB_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label13: TLabel
          Left = 43
          Top = 167
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actualizaciones:'
        end
        object LS_ACT_IMS: TZetaDBTextBox
          Left = 126
          Top = 165
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_ACT_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_ACT_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label15: TLabel
          Left = 71
          Top = 186
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recargos:'
        end
        object LS_REC_IMS: TZetaDBTextBox
          Left = 126
          Top = 184
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_REC_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_REC_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label17: TLabel
          Left = 64
          Top = 204
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total IMSS:'
        end
        object LS_TOT_IMS: TZetaDBTextBox
          Left = 126
          Top = 202
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_IMS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_IMS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBDecreto20: TGroupBox
        Left = 8
        Top = 227
        Width = 217
        Height = 89
        Caption = 'Decreto 20%'
        TabOrder = 2
        OnDblClick = GBDblClick
        object lblParcialidad: TLabel
          Left = 28
          Top = 66
          Width = 93
          Height = 13
          Caption = 'Importe Parcialidad:'
        end
        object LS_PARCIAL: TZetaDBTextBox
          Left = 126
          Top = 63
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_PARCIAL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_PARCIAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_DES_20: TDBCheckBox
          Left = 23
          Top = 24
          Width = 115
          Height = 17
          TabStop = False
          Alignment = taLeftJustify
          Caption = 'Aplic'#243' Decreto 20%:'
          DataField = 'LS_DES_20'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 0
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object LS_FINAN: TDBCheckBox
          Left = 23
          Top = 43
          Width = 115
          Height = 17
          TabStop = False
          Alignment = taLeftJustify
          Caption = 'Con Financiamiento:'
          DataField = 'LS_FINAN'
          DataSource = DataSource
          ReadOnly = True
          TabOrder = 1
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Bimestrales'
      OnMouseDown = GenericoMouseDown
      object GBCuentaInd: TGroupBox
        Left = 8
        Top = 0
        Width = 207
        Height = 150
        Caption = ' Cuenta Individual '
        TabOrder = 0
        OnDblClick = GBDblClick
        object Label22: TLabel
          Left = 86
          Top = 19
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Retiro:'
        end
        object LS_RETIRO: TZetaDBTextBox
          Left = 121
          Top = 17
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_RETIRO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_RETIRO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label14: TLabel
          Left = 34
          Top = 36
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cesant'#237'a y Vejez:'
        end
        object LS_CES_VEJ: TZetaDBTextBox
          Left = 121
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_CES_VEJ'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_CES_VEJ'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LS_SUB_RET: TZetaDBTextBox
          Left = 121
          Top = 53
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_SUB_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_SUB_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label21: TLabel
          Left = 46
          Top = 54
          Width = 71
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubTotal RCV:'
        end
        object Label26: TLabel
          Left = 40
          Top = 72
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actualizaciones:'
        end
        object LS_ACT_RET: TZetaDBTextBox
          Left = 121
          Top = 71
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_ACT_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_ACT_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label23: TLabel
          Left = 68
          Top = 90
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recargos:'
        end
        object LS_REC_RET: TZetaDBTextBox
          Left = 121
          Top = 89
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_REC_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_REC_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label25: TLabel
          Left = 13
          Top = 108
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aportaci'#243'n Voluntaria:'
        end
        object LS_APO_VOL: TZetaDBTextBox
          Left = 121
          Top = 107
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_APO_VOL'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_APO_VOL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label33: TLabel
          Left = 65
          Top = 127
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total RCV:'
        end
        object LS_TOT_RET: TZetaDBTextBox
          Left = 121
          Top = 125
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_RET'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_RET'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBCuentaInfonavit: TGroupBox
        Left = 221
        Top = 0
        Width = 212
        Height = 150
        Caption = ' Cuenta INFONAVIT '
        TabOrder = 1
        OnDblClick = GBDblClick
        object Label6: TLabel
          Left = 39
          Top = 19
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'No Acreditados:'
        end
        object LS_INF_NAC: TZetaDBTextBox
          Left = 119
          Top = 17
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_NAC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_NAC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label8: TLabel
          Left = 56
          Top = 37
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acreditados:'
        end
        object LS_INF_ACR: TZetaDBTextBox
          Left = 119
          Top = 35
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_ACR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_ACR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 41
          Top = 55
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Amortizaciones:'
        end
        object LS_INF_AMO: TZetaDBTextBox
          Left = 119
          Top = 53
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_AMO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_AMO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label29: TLabel
          Left = 9
          Top = 73
          Width = 106
          Height = 13
          Alignment = taRightJustify
          Caption = 'SubTotal INFONAVIT:'
        end
        object LS_SUB_INF: TZetaDBTextBox
          Left = 119
          Top = 71
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_SUB_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_SUB_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label30: TLabel
          Left = 38
          Top = 91
          Width = 77
          Height = 13
          Alignment = taRightJustify
          Caption = 'Actualizaciones:'
        end
        object LS_ACT_INF: TZetaDBTextBox
          Left = 119
          Top = 89
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_ACT_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_ACT_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label31: TLabel
          Left = 66
          Top = 109
          Width = 49
          Height = 13
          Alignment = taRightJustify
          Caption = 'Recargos:'
        end
        object LS_REC_INF: TZetaDBTextBox
          Left = 119
          Top = 107
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_REC_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_REC_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label32: TLabel
          Left = 28
          Top = 127
          Width = 87
          Height = 13
          Alignment = taRightJustify
          Caption = 'Total INFONAVIT:'
        end
        object LS_TOT_INF: TZetaDBTextBox
          Left = 119
          Top = 125
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_TOT_INF'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_TOT_INF'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GBResumenBimestral: TGroupBox
        Left = 8
        Top = 152
        Width = 207
        Height = 82
        Caption = ' Resumen Bimestral '
        TabOrder = 2
        OnDblClick = GBDblClick
        object LS_INF_NUM: TZetaDBTextBox
          Left = 121
          Top = 15
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_INF_NUM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_INF_NUM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label16: TLabel
          Left = 58
          Top = 17
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acreditados:'
        end
        object LS_NUM_BIM: TZetaDBTextBox
          Left = 121
          Top = 34
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_NUM_BIM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_NUM_BIM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label27: TLabel
          Left = 58
          Top = 36
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Asegurados:'
        end
        object LS_DIAS_BM: TZetaDBTextBox
          Left = 121
          Top = 53
          Width = 80
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'LS_DIAS_BM'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LS_DIAS_BM'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label28: TLabel
          Left = 42
          Top = 55
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'D'#237'as Cotizados:'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 240
    Top = 40
  end
end
