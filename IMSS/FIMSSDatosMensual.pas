unit FIMSSDatosMensual;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, ExtCtrls,
     Controls, Forms, Dialogs, Db, Grids, DBGrids, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaDBTextBox;

type
  TIMSSDatosMensual = class(TBaseConsulta)
    dsLiq_Mov: TDataSource;
    ZetaDBGrid2: TZetaDBGrid;
    Panel1: TPanel;
    LE_INV_VID: TZetaDBTextBox;
    LE_RIESGOS: TZetaDBTextBox;
    LE_GUARDER: TZetaDBTextBox;
    LE_TOT_IMS: TZetaDBTextBox;
    LE_DIAS_CO: TZetaDBTextBox;
    Label1: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    LE_IMSS_OB: TZetaDBTextBox;
    Label11: TLabel;
    LE_IMSS_PA: TZetaDBTextBox;
    LE_APO_VOL: TZetaDBTextBox;
    Label12: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean;override;
    function PuedeBorrar( var sMensaje: String ): Boolean;override;
    function PuedeModificar( var sMensaje: String ): Boolean;override;
  end;

var
  IMSSDatosMensual: TIMSSDatosMensual;

implementation

uses DIMSS,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

{ TIMSSDatosMensual }

procedure TIMSSDatosMensual.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stIMSS;
     HelpContext := H40412_IMSS_mensual_empleado;
end;

procedure TIMSSDatosMensual.Connect;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosMen.Conectar;
          cdsMovMen.Conectar;
          DataSource.DataSet:= cdsIMSSDatosMen;
          dsLiq_Mov.DataSet:= cdsMovMen;
     end;
end;

procedure TIMSSDatosMensual.Refresh;
begin
     with dmIMSS do
     begin
          cdsIMSSDatosMen.Refrescar;
          cdsMovMen.Refrescar;
     end;
end;

function TIMSSDatosMensual.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Agregar A Datos Mensuales IMSS';
end;

function TIMSSDatosMensual.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Borrar En Datos Mensuales IMSS';
end;

function TIMSSDatosMensual.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result:= False;
     sMensaje := 'No Se Puede Modificar En Datos Mensuales IMSS';
end;

end.
