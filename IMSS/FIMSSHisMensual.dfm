inherited IMSSHisMensual: TIMSSHisMensual
  Left = 156
  Top = 167
  Caption = 'IMSS: Historia Mensual por Empleado'
  ClientHeight = 264
  ClientWidth = 580
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 580
    inherited ValorActivo2: TPanel
      Width = 321
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 580
    Height = 65
    Align = alTop
    TabOrder = 1
    object Label6: TLabel
      Left = 266
      Top = 16
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total IMSS:'
    end
    object LS_TOT_IMS: TZetaDBTextBox
      Left = 326
      Top = 14
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_IMS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_IMS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object LS_NUM_TRA: TZetaDBTextBox
      Left = 150
      Top = 14
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_NUM_TRA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_NUM_TRA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label7: TLabel
      Left = 87
      Top = 16
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Asegurados:'
    end
    object Label8: TLabel
      Left = 71
      Top = 34
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'D�as Cotizados:'
    end
    object LS_DIAS_CO: TZetaDBTextBox
      Left = 150
      Top = 32
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_DIAS_CO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_DIAS_CO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 252
      Top = 34
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total Mensual:'
    end
    object LS_TOT_MES: TZetaDBTextBox
      Left = 326
      Top = 32
      Width = 90
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'LS_TOT_MES'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'LS_TOT_MES'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
  end
  object ZetaDBGrid2: TZetaDBGrid [2]
    Left = 0
    Top = 84
    Width = 580
    Height = 180
    Align = alClient
    DataSource = dsLiq_Emp
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'N�mero'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PrettyName'
        Title.Caption = 'Nombre'
        Width = 333
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_SEGSOC'
        Title.Caption = 'NSS'
        Width = 91
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LE_TOT_IMS'
        Title.Alignment = taRightJustify
        Title.Caption = 'Total IMSS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LE_DIAS_CO'
        Title.Alignment = taRightJustify
        Title.Caption = 'D�as Cotizados'
        Width = 90
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 488
    Top = 8
  end
  object dsLiq_Emp: TDataSource
    Left = 536
    Top = 40
  end
end
