unit ZFuncsPendientes;

interface



implementation

  TZetaStoredProc = class( TZetaFunc )
  public
    { Public Declarations }
    function Calculate: TQREvResult; override;
  end;

function TZetaStoredProc.Calculate: TQREvResult;
var
   i, iElementos, iPosResult: Integer;
begin
     Result.Kind := resError; // Supone error, por si no hay match //
     iElementos := ArgList.Count - 1;
     if ( iElementos >= 1 ) and ( Argument( 0 ).Kind = resString ) then
     begin
          with dmQuerys.StoredProcedure do
          begin
               StoredProcName := Argument( 0 ).StrResult;
               Prepare;
               iPosResult := -1;
               for i := 0 to ( ParamCount - 1 ) do
               begin
                    with Params[ i ] do
                    begin
                         if ( ParamType in [ ptInput, ptInputOutput ] ) then  // Asigna valor a parámetros de ENTRADA //
                         begin
                              if ( ( i + 1 ) <= iElementos ) then
                              begin
                                   with Argument( i + 1 ) do
                                   begin
                                        case Kind of
                                             resString: AsString := strResult;
                                             resDouble: AsFloat := dblResult;
                                             resBool: AsBoolean := booResult;
                                             resInt: AsInteger := intResult;
                                        end;
                                   end;
                              end;
                         end;
                         if ParamType in [ ptOutput, ptInputOutput, ptResult ] then // Obtiene posición de PARAMETRO de Salida/Resultado //
                            iPosResult := i;
                    end;
               end;
               try
                  ExecProc;
               except
                     iPosResult := -1;
               end;
               if ( iPosResult >= 0 ) then
               begin
                    with Params[ iPosResult ], Result do
                    begin
                         case DataType of
                              ftString:
                              begin
                                   Kind := resString;
                                   strResult := AsString;
                              end;
                              ftSmallint, ftInteger, ftWord:
                              begin
                                   Kind := resInt;
                                   intResult := AsInteger;
                              end;
                              ftBoolean:
                              begin
                                   Kind := resBool;
                                   booResult := AsBoolean;
                              end;
                              ftFloat, ftCurrency, ftBCD, ftDate, ftTime, ftDateTime:
                              begin
                                   Kind := resDouble;
                                   dblResult := AsFloat;
                              end;
                         end;
                    end;
               end;
          end;
     end;
end;


end.
