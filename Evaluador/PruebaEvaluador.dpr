program PruebaEvaluador;

uses
  Forms,
  FPruebaEvaluador in 'FPruebaEvaluador.pas' {FormaEvaluador},
  ZEvaluador in 'ZEvaluador.pas',
  ZFuncsGenerales in 'ZFuncsGenerales.pas',
  DSuperReporte in '..\Servidor\DSuperReporte.pas' {dmSuperReporte: TDataModule},
  FPruebaReporte in 'FPruebaReporte.pas' {FormReporte},
  ZSuperEvaluador in 'ZSuperEvaluador.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TFormaEvaluador, FormaEvaluador);
  Application.CreateForm(TFormReporte, FormReporte);
  Application.Run;
end.
