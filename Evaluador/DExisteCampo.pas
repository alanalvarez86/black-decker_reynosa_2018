unit DExisteCampo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBaseExisteCampo,
  DNomParam,
  ZetaTipoEntidad,
  ZetaCommonLists;

type
  TdmExisteCampo = class(TdmBaseExisteCampo)
    procedure dmExisteCampoDestroy(Sender: TObject);
  private
    { Private declarations }
    FNomParam: TdmNomParam;
    property dmNomParam: TdmNomParam read FNomParam;
    procedure CreateNomParam;
  protected
    function EntidadEspecial(const oEntidad: TipoEntidad;var sLookup: string): Boolean;override;
  public
    { Public declarations }
    function ExisteParam( const sCampo : String;
                          var   sFormula : String;
                          var   iTipo  : Integer ) : Boolean;
    function ExisteCampo( var oTipoEntidad: TipoEntidad;
                          const sCampo: string;
                          var sFormula: string;
                          var iTipo: integer;
                          var lNomParam : Boolean): Boolean;override;
  end;

var
  dmExisteCampo: TdmExisteCampo;

implementation

{$R *.DFM}

procedure TdmExisteCampo.dmExisteCampoDestroy(Sender: TObject);
begin
     FreeAndNil( FNomParam );
     inherited;
end;

procedure TdmExisteCampo.CreateNomParam;
begin
     if ( FNomParam = nil ) then
        FNomParam := TdmNomParam.Create( oZetaProvider );
end;

function TdmExisteCampo.ExisteParam(const sCampo: String; var sFormula: String; var iTipo: Integer ): Boolean;
begin
     CreateNomParam;
     Result := dmNomParam.EsNomParam( sCampo, sFormula, iTipo );
end;

function TdmExisteCampo.ExisteCampo( var oTipoEntidad: TipoEntidad;
                                     const sCampo: string;
                                     var sFormula: string;
                                     var iTipo: integer;
                                     var lNomParam : Boolean): Boolean;
begin
     Result := inherited ExisteCampo( oTipoEntidad,sCampo,sFormula,iTipo,lNomParam );

     //NOMPARAM ES PROPIO DEL TRESS
     if (Not Result) AND (Length(sCampo)<=K_ANCHO_NOMBRECAMPO) AND
        (oTipoEntidad in NomParamEntidades) then
     begin
          CreateNomParam;
          Result := dmNomParam.EsNomParam( sCampo, sFormula, iTipo );
          lNomParam := Result;
     end;
end;

function TdmExisteCampo.EntidadEspecial(const oEntidad: TipoEntidad; var sLookup:string ) : Boolean;
begin
     Result := oEntidad = enEmpleado;
     sLookUp := 'COLABORA.CB_APE_PAT||'' ''||COLABORA.CB_APE_MAT||'', ''||COLABORA.CB_NOMBRES';
end;

end.
