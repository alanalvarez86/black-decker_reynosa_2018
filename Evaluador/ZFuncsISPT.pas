unit ZFuncsISPT;

interface

uses Classes, Sysutils, DB, Math, Controls,
     ZetaQRExpr,
     ZEvaluador,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses DTablasISPT,
     DZetaServerProvider,
     DSalMin,
     DCalculoNomina,
     ZGlobalTress,
     ZFuncsTools,
     ZAccesosTress,
     ZetaSQLBroker,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonLists;

const
     aCreditoSalarioUpper: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'CREDITO AL SALARIO', 'SUBSIDIO AL EMPLEO' );
     aCreditoSalarioLower: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'Cr�dito', '   SUBE' );


function GetFactorSube( const rFactorSUBE:TTasa; const Year: integer ): TTasa;
begin
     if ( Year >= K_REFORMA_FISCAL_2008 ) then
        Result := rFactorSUBE
     else
         Result := K_FACTOR_MENSUAL;
end;

{*********************** TZetaTABLA *********************************}
{ TABLA( Tabla, Valor [, Regresa][, Fecha] )}
type
  TZetaTabla = class( TZetaFunc )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaTabla.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     ParametrosFijos( 2 );
     TipoRegresa := resDouble;
     oZetaProvider.GetDatosActivos;
     ZetaEvaluador.oZetaCreator.PreparaTablasISPT;
end;

function TZetaTabla.Calculate: TQREvResult; // Sustituye la variable TABLA de TRESS //
begin
    with Result do
    begin
         Kind := resDouble;
         with ZetaEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( ParamInteger( 0 ), ParamFloat( 1 ), DefaultDate( 3, oZetaProvider.FechaDefault ) ) do
         begin
              case DefaultInteger( 2, 1 ) of
                   1: dblResult := Cuota;
                   2: dblResult := Tasa / 100;
                   3: dblResult := Inferior;
              else
                    dblResult := 0;
              end;
         end;
    end;
end;

{*********************** TZetaCalcImp *********************************}
{Calc_imp( Valor, Subsidio, Factor, Sal_minimo, T1, T2, T3, Fecha )}
type
  TZetaCalcImp = class( TZetaFunc )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  Calculate: TQREvResult; override;
  end;

procedure TZetaCalcImp.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;

     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
          PreparaSalMin;
     end;
     with oZetaProvider do
     begin
          InitGlobales;
          GetDatosActivos;
     end;
end;

function CalcImpuesto( oEvaluador : TZetaEvaluador; const nValor, nValorSube: TPesos; nFactorSubsidio: TPesos; const nFactor, nFactorSUBE, nSalMin : TPesos;
                       nT1, nT2, nT3 : Integer; const dFecha: TDate; var nCredito : TPesos ) : TPesos;
var
   oRenglon, oRenglon2 : TRenglonNumerica;
   lMarginalSubsidio, lExcedenteSubsidio : Boolean;
   lReformaFiscal2008: Boolean;
   nExcedenteSubsidio, nMarginalSubsidio : TPesos;
   nExcedente, nMarginal, nImpuesto : TPesos;
   nSubsidio : TPesos;
begin

     lReformaFiscal2008 := oEvaluador.oZetaProvider.DatosPeriodo.Year >= K_REFORMA_FISCAL_2008;

     nCredito := 0;

     // Si valor <= 0, ni pierdo tiempo investigando y calculando
     if ( nValor <= 0 ) then
     begin
          with oEvaluador do
          begin
               if Rastreando or RastreandoNomina then
               begin
                    with Rastreador do
                    begin
                         RastreoHeader( 'CONSULTA DE TABLA' );
                         RastreoPesos( ' Monto Gravado : ', nvalor );
                         RastreoPesos( 'ISPT Calculado : ', 0 );
                    end;
               end;
          end;
          Result := 0;
          Exit;
     end;

     // Impuesto Art. 80
     oRenglon := oEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT1, nValor, dFecha );
     with oRenglon do
     begin
          nExcedente := nValor - Inferior;
          nMarginal := nExcedente * ( Tasa/100 );
          nImpuesto := Cuota + nMarginal;
     end;

     // Cr�dito al Salario: Art. 80-B
     if ( nT3 = 0 ) then
        nCredito := nSalMin / 10 * nFactorSUBE
     else if ( nT3 < 0 ) then
        nCredito := 0
     else
        nCredito := oEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT3, nValorSUBE, dFecha ).Cuota;

     // Subsidio: Art. 80-A
     if lReformaFiscal2008 or ( ( nFactorSubsidio > 0 ) AND ( nT2 <> 0 ) ) then
     begin
           lExcedenteSubsidio := FALSE;
           // Si la tabla 2 es negativa, vuelve a calcular el impuesto marginal
           if ( nT2 < 0 ) then
           begin
                lMarginalSubsidio := TRUE;
                nT2 := -nT2;
                if ( nT2 > 1000 ) then
                begin
                     lExcedenteSubsidio := TRUE;
                     nT2 := nT2 - 1000;
                end;
           end
           else
               lMarginalSubsidio := FALSE;
          nExcedenteSubsidio := 0;

          //CV: Reforma Fiscal 2008
          if ( lReformaFiscal2008 ) then
          begin
               with oRenglon2 do
               begin
                    Inferior := 0;
                    Cuota := 0;
                    Tasa := 0;
               end;
               nFactorSubsidio := 0;
          end
          {CV: Antes de la Reforma Fiscal 2008, se mantiene por compatibilidad la l�gica}
          else
              oRenglon2 := oEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT2, nValor, dFecha );

          with oRenglon2 do
          begin
               if ( lMarginalSubsidio ) then
               begin
                  nExcedenteSubsidio := nValor - Inferior;
                  if ( lExcedenteSubsidio ) then
                        nMarginalSubsidio := nExcedenteSubsidio
                  else
                        nMarginalSubsidio  := nExcedenteSubsidio * ( oRenglon.Tasa/100 );
               end
               else
                   nMarginalSubsidio := nMarginal;
               nSubsidio := (( nMarginalSubsidio * ( Tasa/100 ) ) +  Cuota ) * nFactorSubsidio;
               // Lo topa para que no sea mayor que el impuesto
               if lExcedenteSubsidio and ( nSubsidio > nImpuesto ) then
                 nSubsidio := nImpuesto;
          end;
          with oEvaluador do
          begin
               if Rastreando or RastreandoNomina then
               begin
                    with Rastreador do
                    begin
                         RastreoHeader( 'CONSULTA DE TABLA' );
                         RastreoPesos( '           Monto Gravado : ', nValor );
                         RastreoPesos( '(-)      L�mite Inferior : ', oRenglon.Inferior );
                         RastreoPesos( '(=)            Excedente : ', nExcedente );
                         RastreoFactor( '(X)             Tasa (%) : ', oRenglon.Tasa );
                         RastreoPesos( '(=)    Impuesto Marginal : ', nMarginal );
                         RastreoPesos( '(+)           Cuota Fija : ', oRenglon.Cuota );
                         RastreoPesos( '(=)    ISPT sin Subsidio : ', nImpuesto );
                         RastreoEspacio;
                         // Rastreo de Subsidio de Impuesto Marginal
                         if (not lReformaFiscal2008) then
                         begin
                              if lMarginalSubsidio then
                              begin
                                   if lExcedenteSubsidio then   // Procedimiento #3
                                   begin
                                        RastreoPesos( '           Monto Gravado : ', nValor );
                                        RastreoPesos( '(-)      L�mite Inferior : ', oRenglon2.Inferior );
                                        RastreoPesos( '(=)            Excedente : ', nMarginalSubsidio );
                                        RastreoFactor('(X)    Tasa subsidio (%) : ', oRenglon2.Tasa );
                                        RastreoPesos( '(=) Subsidio I. Marginal : ', nMarginalSubsidio * ( oRenglon2.Tasa/100 ) );
                                   end
                                   else                         // Procedimiento #2
                                   begin
                                        RastreoPesos( '           Monto Gravado : ', nValor );
                                        RastreoPesos( '(-)      L�mite Inferior : ', oRenglon2.Inferior );
                                        RastreoPesos( '(=)            Excedente : ', nExcedenteSubsidio );
                                        RastreoFactor('(X)             Tasa (%) : ', oRenglon.Tasa );
                                        RastreoPesos( '(=)    Impuesto Marginal : ', nMarginalSubsidio );
                                        RastreoFactor('(X)    Tasa subsidio (%) : ', oRenglon2.Tasa );
                                        RastreoPesos( '(=) Subsidio I. Marginal : ', nMarginalSubsidio * ( oRenglon2.Tasa/100 ) );
                                   end;
                              end
                              else                              // Procedimiento #1
                              begin
                                   RastreoPesos( '       Impuesto Marginal : ', nMarginalSubsidio );
                                   RastreoFactor('(X)    Tasa subsidio (%) : ', oRenglon2.Tasa );
                                   RastreoPesos( '(=) Subsidio I. Marginal : ', nMarginalSubsidio * ( oRenglon2.Tasa/100 ) );
                              end;
                         end;

                         if (not lReformaFiscal2008) then
                         begin
                              RastreoEspacio;
                              RastreoPesos( '     Subsidio Cuota Fija : ', oRenglon2.Cuota );
                              RastreoPesos( '(+) Subsidio I. Marginal : ', nMarginalSubsidio * ( oRenglon2.Tasa/100 ) );
                              RastreoPesos( '(=)    Suma de Subsidios : ', oRenglon2.Cuota + nMarginalSubsidio * ( oRenglon2.Tasa/100 ) );
                              RastreoPesos( '(X) % Subsidio Acreditar : ', nFactorSubsidio * 100 );
                              RastreoPesos( '(=)        Subsidio Neto : ', nSubsidio );
                              RastreoEspacio;
                         end;
                         if ( nT3 >= 0 ) then
                            RastreoPesos( '       ISPT sin Subsidio : ', nImpuesto );

                         if (not lReformaFiscal2008) then
                            RastreoPesos( '(-)        Subsidio Neto : ', nSubsidio );

                         if lReformaFiscal2008 then
                         begin
                              if ( nT3 >= 0 ) then
                                 RastreoPesos( '(-)   Subsidio al Empleo : ', nCredito );
                         end
                         else
                             RastreoPesos( '(-)   Cr�dito al Salario : ', nCredito );

                         if ( ( nImpuesto - nSubsidio - nCredito ) < 0 ) then
                            RastreoPesos( Format( '(=)    %s Calculado : ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), nImpuesto - nSubsidio - nCredito )
                         else
                             RastreoPesos( '(=)       ISPT Calculado : ', nImpuesto - nSubsidio - nCredito );
                    end;
               end;
          end;
     end
     else
     begin
          nSubsidio := 0;
          with oEvaluador do
          begin
               if Rastreando or RastreandoNomina then
               begin
                    with Rastreador do
                    begin
                         RastreoHeader(    'CONSULTA DE TABLA SIN SUBSIDIO' );
                         RastreoPesos(     '         Monto Gravado : ', nValor );
                         RastreoPesos(     '(-)    L�mite Inferior : ', oRenglon.Inferior );
                         RastreoPesos(     '(=)          Excedente : ', nExcedente );
                         RastreoPesos(     '(X)           Tasa (%) : ', oRenglon.Tasa );
                         RastreoPesos(     '(=)  Impuesto Marginal : ', nMarginal );
                         RastreoPesos(     '(+)         Cuota Fija : ', oRenglon.Cuota );
                         RastreoPesos(     '(=)  ISPT sin Subsidio : ', nImpuesto );
                         RastreoPesos(     '(-) Cr�dito al Salario : ', nCredito );
                         if ( ( nImpuesto - nSubsidio - nCredito ) < 0 ) then
                            RastreoPesos(  '(=)  Cr�dito Calculado : ', nImpuesto - nSubsidio - nCredito )
                         else
                             RastreoPesos( '(=)     ISPT Calculado : ', nImpuesto - nSubsidio - nCredito );
                    end;
               end;
          end;
     end;

     // Pendiente: CREDITO APLICADO

     // Impuesto NETO
     Result := Redondea( nImpuesto - nSubsidio - nCredito );
     nCredito := Redondea ( nCredito );

     // Compatiblidad con Ley de 1991
     if ( nT3 <= 0 ) AND ( Result < 0 ) then
        Result := 0;
end;

function CalcImp( oEvaluador : TZetaEvaluador; const nValor, nValorSUBE, nFactorSubsidio, nFactor, nFactorSUBE, nSalMin : TPesos; nT1, nT2, nT3 : Integer; const dFecha: TDate ) : TPesos;
var
   rCredito : TPesos;
begin
     rCredito := 0;
     Result := CalcImpuesto( oEvaluador, nValor, nValorSUBE, nFactorSubsidio, nFactor, nFactorSUBE, nSalMin,  nT1, nT2, nT3, dFecha, rCredito );
end;

function TZetaCalcImp.Calculate: TQREvResult;
var
   rValor, rFactorSubsidio, rFactor, rSalMin: TPesos;
   iT1, iT2, iT3: Integer;
   dFecha : TDate;
   TipoRegresa : TQREvResultType;
begin
     if ( ZetaEvaluador.oZetaCreator.dmTablasISPT = NIL ) then
        GetRequeridos(TipoRegresa);
   
     // Valor
     if ParamVacio( 0 ) then
        rValor := 0
     else
         rValor := ParamFloat( 0 );
     // Subsidio
     if ParamVacio( 1 ) then
        rFactorSubsidio := oZetaProvider.GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )
     else
         rFactorSubsidio := ParamTasa( 1 );
     // Factor
     if ParamVacio( 2 ) then
        rFactor := K_FACTOR_MENSUAL
     else
         rFactor := ParamTasa( 2 );
     // Fecha
     if ParamVacio( 7 ) then
     begin
          if ( ZetaEvaluador.CalculandoNomina ) then
             dFecha := oZetaProvider.DatosPeriodo.Inicio      // Al Calcular N�mina DatosPeriodo Ya Tiene Informaci�n
          else
             dFecha := oZetaProvider.FechaDefault;
     end
     else
         dFecha := ParamDate( 7 );
     // Sal_minimo
     if ParamVacio( 3 ) then
     begin
        if ( ZetaEvaluador.CalculandoNomina ) then
           rSalMin := TCalcNomina(ZetaEvaluador.CalcNomina).nEmpleadoSalMin
        else
            // EM: Puse la zona como constante porque es muy
            // complicado obtener la zona real y este par�metro
            // ya no se usa.
            rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( dFecha, 'A' );
            { ER: 2.5 - Se Cambi� Date por el nuevo param�tro Fecha
              rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( Date, 'A' );
            }
     end
     else
         rSalMin := ParamFloat( 3 );
     // T1
     if ParamVacio( 4 ) then
        iT1 := 1        // ART.80 //
     else
         iT1 := ParamInteger( 4 );
     // T2
     if ParamVacio( 5 ) then
        iT2 := 2        // ART.80-A //
     else
         iT2 := ParamInteger( 5 );
     // T3
     if ParamVacio( 6 ) then
        iT3 := 3        // ART.80-B //
     else
         iT3 := ParamInteger( 6 );
     with Result do
     begin
          Kind      := resDouble;
          dblResult := CalcImp( ZetaEvaluador,
                                rValor,
                                rValor,//Monto para el SUBE
                                rFactorSubsidio,
                                rFactor,
                                rFactor,
                                rSalMin,
                                iT1,
                                iT2,
                                iT3,
                                dFecha );
     end;
end;

{*********************** TZetaCalcISPT *********************************}
type
  TZetaRegresaImpuestos = class( TZetaRegresaFloat )
   private
     function TotalNomina(const eTotal: TipoTotalNomina): TPesos;
  end;

function TZetaRegresaImpuestos.TotalNomina(const eTotal: TipoTotalNomina): TPesos;
begin
     if ZetaEvaluador.CalculandoNomina then
        Result := TCalcNomina(ZetaEvaluador.CalcNomina).TotNomina( eTotal )
     else
     begin
       with DataSetEvaluador do
          case eTotal of
               ttnPercepciones : Result := FieldByName('NO_PERCEPC').AsFloat;
               ttnDeducciones  : Result := FieldByName('NO_DEDUCCI').AsFloat;
               ttnNeto         : Result := FieldByName('NO_NETO').AsFloat;
               ttnMensuales    : Result := FieldByName('NO_PER_MEN').AsFloat;
               ttnExentas      : Result := FieldByName('NO_X_ISPT').AsFloat;
               ttnPerCal       : Result := FieldByName('NO_PER_CAL').AsFloat;
               ttnImpCal       : Result := FieldByName('NO_IMP_CAL').AsFloat;
               ttnExentoCal    : Result := FieldByName('NO_X_CAL').AsFloat;
               ttnExentoMensual: Result := FieldByName('NO_X_MENS').AsFloat;
               ttnPrestaciones : Result := FieldByName('NO_TOT_PRE').AsFloat;
               ttnGravadoPrev  : Result := FieldByName('NO_PREV_GR').AsFloat;
               ttnGravadoISN   : Result := FieldByName('NO_PER_ISN').AsFloat;

               else              Result := 0;
          end;
     end;
end;

{ISPT( [Subsidio, Factor, DiasISPT, Sal_Min, T1, T2, T3, Per_Grava, Per_Mensual ,Suaviza, Ya_Calc, Fecha, FactorSube] )}
//ISPT( , , , , , , , ,  ,, , , 35 )
type
TZetaCalcISPT = class( TZetaRegresaImpuestos )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaCalcISPT.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     inherited;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
          PreparaSalMin;
     end;

     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
     end;

     if ParamVacio( 2 ) then
     begin
          AgregaParam( 'DIAS_VACA' );
          AgregaParam( 'DIAS' );
     end;
     if not ZetaEvaluador.CalculandoNomina then
     begin
          if ParamVacio( 7 ) then
          begin
               AgregaRequerido( 'NOMINA.NO_PERCEPC', enNomina );
               AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
               AgregaRequerido( 'NOMINA.NO_X_ISPT',  enNomina );
               AgregaRequerido( 'NOMINA.NO_PER_CAL', enNomina );
               AgregaRequerido( 'NOMINA.NO_PREV_GR', enNomina );
          end;
          if ParamVacio( 8 ) then
          begin
               AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
               AgregaRequerido( 'NOMINA.NO_X_MENS', enNomina );
          end;
          if ParamVacio( 10 ) then
             AgregaRequerido( 'NOMINA.NO_IMP_CAL', enNomina );
     end;
end;

function CalcISPT( oEvaluador : TZetaEvaluador;
                   nSubsidio, nFactor, nFactorSUBE, nDiasISPT, nSalMin : TPesos;
                   const nT1, nT2, nT3 : Integer;
                   const nGravado, nMensual : TPesos;
                   const lSuaviza, lAjustaSUBE : Boolean;
                   const nYaCalculado : TPesos;
                   const dFecha : TDate;
                   const lRegistraCredito : Boolean = FALSE ) : TPesos;
var
   nPercProm, nValorSube, nISPTPeriod, nISPTMensual, nISPTPropo, nISPTCalc, nISPTNeto : TPesos;
   nCredito, nCreditoTabla : TPesos;
   lReformaFiscal2008: Boolean;
begin
     with oEvaluador do
     begin
          lReformaFiscal2008 := oZetaProvider.DatosPeriodo.Year >= K_REFORMA_FISCAL_2008;
          if CalculandoNomina then
          begin
               with TCalcNomina(CalcNomina) do
               begin
                    RegistraISPT( 0 );
                    RegistraCredito( 0 );

                    if lRegistraCredito then
                    begin
                         RegistraCreditoMensual( 0 );
                         RegistraCreditoPeriodo( 0 );
                    end;
                    RegistraSubeMensual( 0 );
               end;
          end;
     end;
     if ( nDiasISPT <= 0 ) then
     begin
          if ( nGravado > 0 ) OR ( nMensual > 0 ) then
             nDiasISPT := 1
          else
          begin
               Result := nYaCalculado;
               with oEvaluador do
               begin
                    if CalculandoNomina then
                       TCalcNomina(CalcNomina).RegistraISPT( Result );
               end;
               Exit;
          end;
     end;

     if ( nFactor <= 0 ) then
        raise Exception.Create( 'Factor Debe Ser Mayor que Cero' );

     if ( nFactorSUBE <= 0 ) then
        raise Exception.Create( 'Factor SUBE Debe Ser Mayor que Cero' );

     // Percepciones promedio del per�odo ( Elevar al mes)
     nPercProm := nGravado / nDiasISPT * nFactor;
     
     if lAjustaSUBE then
        nValorSube := nGravado
     else
         nValorSube := nGravado / nDiasISPT * nFactorSUBE;

     with oEvaluador do
     begin
          if Rastreando or RastreandoNomina then
          begin
               with Rastreador do
               begin
                    RastreoHeader( 'CONVIERTE PERCEPCIONES DEL PERIODO A MENSUALES' );
                    RastreoPesos( '     Percepciones Gravadas : ', nGravado );
                    RastreoPesos( '(/)      D�as Considerados : ', nDiasISPT );
                    RastreoPesos( '(X)      Factor de Per�odo : ', nFactor );
                    RastreoPesos( '(=)    Percepci�n Promedio : ', nPercProm );
               end;
          end;
     end;

     // Si hay percepciones tipo 'Mensual'
     with oEvaluador do
     begin
          if ( nMensual > 0 ) then
          begin
               if Rastreando or RastreandoNomina then
               begin
                    with Rastreador do
                    begin
                         RastreoHeader( 'SUMA PERCEPCIONES PERIODICAS MAS MENSUALES' );
                         RastreoPesos( '     Percepci�n Promedio del Per�odo : ', nPercProm );
                         RastreoPesos( '(+)        Percepciones Tipo Mensual : ', nMensual );
                         RastreoPesos( '(=)    Total de Percepciones del Mes : ', nPercProm + nMensual );
                    end;
               end;
               if ( lSuaviza ) then  // Suavizar mensual
               begin
                    if Rastreando or RastreandoNomina then
                       Rastreador.RastreoHeader( 'CALCULA ISPT DE PERIODICAS MAS MENSUALES (SUAVE)' );
                    nPercProm := nPercProm + nMensual;
                    {Change request 1303}
                    {CV: cambio para que el nValorSube sume las percepciones mensuales}
                    if NOT lAjustaSUBE then
                       nValorSube := nValorSube + nMensual;
               end
               else
               begin
                    if Rastreando or RastreandoNomina then
                       Rastreador.RastreoHeader( 'CALCULA ISPT DE PERCEPCION PROMEDIO' );
                    nISPTPeriod := CalcImpuesto( oEvaluador, nPercProm, nValorSUBE, nSubsidio, nFactor, nFactorSUBE, nSalMin, nT1, nT2, nT3, dFecha, nCreditoTabla );
                    nCredito := Redondea( nCreditoTabla / nFactor * nDiasISPT );
                    if Rastreando or RastreandoNomina then
                    begin
                         with Rastreador do
                         begin
                              if ( nISPTPeriod < 0  ) then
                                 RastreoMsg( '*** EL ISPT DE PERCEPCION PROMEDIO SE TOMARA COMO 0 ***' );
                              RastreoHeader( 'CALCULO ISPT: SUMA DE PERIODICAS MAS MENSUALES' );
                         end;
                    end;

                    nISPTMensual:= CalcImp( oEvaluador, nPercProm+nMensual, nValorSube+nMensual, nSubsidio, nFactor, nFactorSUBE, nSalMin, nT1, nT2, nT3, dFecha );
                    if ( Rastreando or RastreandoNomina ) and ( nISPTMensual < 0 ) then
                       Rastreador.RastreoMsg( '*** EL ISPT DE LA SUMA SE TOMARA COMO 0 ***' );
                    nISPTPropo  := nISPTPeriod / nFactor * nDiasISPT;
                    nISPTCalc   := rMax( nISPTPeriod, 0 );
                    nISPTMensual:= rMax( nISPTMensual, 0 );
                    if Rastreando or RastreandoNomina then
                    begin
                         with Rastreador do
                         begin
                              RastreoHeader( 'CALCULA ISPT DE PERCEPCIONES TIPO MENSUAL' );
                              RastreoPesos( '          ISPT Calculado : ', nISPTPeriod );
                              RastreoPesos( '(/)    Factor de Per�odo : ', nFactor );
                              RastreoPesos( '(X)    D�as Considerados : ', nDiasISPT );
                              RastreoPesos( '(=)      ISPT de Per�odo : ', nISPTPropo );
                              RastreoEspacio;
                              RastreoPesos( '         ISPT de Per�odo : ', nISPTPropo );
                              RastreoPesos( '(+) ISPT Mensual+Per�odo : ', nISPTMensual );
                              RastreoPesos( '(-)       ISPT Calculado : ', nISPTCalc );
                              RastreoPesos( '(+)    ISPT Individuales : ', nYaCalculado );
                              RastreoPesos( '(=)       ISPT a Retener : ', nYaCalculado + nISPTPropo + nISPTMensual - nISPTCalc );

                              if lRegistraCredito then
                              begin
                                   RastreoEspacio;
                                   RastreoHeader( Format('%s AL SALARIO APLICADO',[UpperCase(aCreditoSalarioLower[lReformaFiscal2008])]) );
                                   RastreoPesos( Format('        %s de Tabla : ',[aCreditoSalarioLower[lReformaFiscal2008]]), nCreditoTabla );
                                   RastreoPesos( '(/)    Factor de Per�odo : ', nFactor );
                                   RastreoPesos( '(X)    D�as Considerados : ', nDiasISPT );
                                   RastreoPesos( Format('(=)   %s de Per�odo : ',[aCreditoSalarioLower[lReformaFiscal2008]]), nCredito );
                                   RastreoEspacio;
                              end;
                         end;
                    end;
                    Result := Redondea( nYaCalculado + nISPTPropo + nISPTMensual - nISPTCalc );
                    if CalculandoNomina then
                    begin
                         with TCalcNomina( CalcNomina ) do
                         begin
                              if ( Result >= 0 ) then
                                 RegistraISPT( Result )
                              else
                                  RegistraCredito( Result );
                              if lRegistraCredito then
                              begin
                                   RegistraCreditoMensual( nCreditoTabla );
                                   RegistraCreditoPeriodo( nCredito );
                              end;
                              RegistraSubeMensual( nCreditoTabla );
                         end;
                    end;
                    Exit;
               end;
          end;
     end;
     nISPTPeriod := CalcImpuesto( oEvaluador, nPercProm, nValorSUBE, nSubsidio, nFactor, nFactorSUBE, nSalMin, nT1, nT2, nT3, dFecha, nCreditoTabla );
     { EZM : Enero 2000. Agregu� Redondea para que concuerde con Rastreo }
     nISPTNeto   := Redondea( nISPTPeriod / nFactor * nDiasISPT );
     nCredito   := Redondea( (nCreditoTabla / nFactor) * nDiasISPT );
     with oEvaluador do
     begin
          if Rastreando or RastreandoNomina then
          begin
               with Rastreador do
               begin
                    RastreoHeader( 'CONVIERTE EL IMPUESTO MENSUAL A PERIODICO' );
                    if ( nISPTPeriod < 0 ) then
                       RastreoPesos( Format( '     %s Calculado : ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), nISPTPeriod )
                    else
                       RastreoPesos( '        ISPT Calculado : ', nISPTPeriod );
                    RastreoPesos( '(/)  Factor de Per�odo : ', nFactor );
                    RastreoPesos( '(X)  D�as Considerados : ', nDiasISPT );
                    RastreoEspacio;
                    if ( nISPTNeto < 0 ) then
                       RastreoPesos( Format( '(=) %s a Entregar : ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), nISPTNeto )
                    else
                        RastreoPesos( '(=)     ISPT a Retener : ', nISPTNeto );
                    if ( nYaCalculado <> 0 ) then
                    begin
                         RastreoEspacio;
                         RastreoPesos( '(+)  ISPT Individuales : ', nYaCalculado );
                         if ( nISPTNeto + nYaCalculado < 0 ) then
                            RastreoPesos( Format( '(=) %s a Entregar : ', [ aCreditoSalarioLower[lReformaFiscal2008] ] ), nISPTNeto + nYaCalculado )
                         else
                             RastreoPesos( '(=)     ISPT a Retener : ', nISPTNeto + nYaCalculado );
                    end;
                    if lRegistraCredito then
                    begin
                         RastreoEspacio;
                         RastreoHeader(  aCreditoSalarioUpper[lReformaFiscal2008] + ' APLICADO' );
                         RastreoPesos( Format('        %s de Tabla : ',[aCreditoSalarioLower[lReformaFiscal2008]]), nCreditoTabla );
                         RastreoPesos( '(/)    Factor de Per�odo : ', nFactor );
                         RastreoPesos( '(X)    D�as Considerados : ', nDiasISPT );
                         RastreoPesos( Format('(=)   %s de Per�odo : ',[aCreditoSalarioLower[lReformaFiscal2008]]), nCredito );
                         RastreoEspacio;
                    end;
               end;
          end;
          Result := Redondea( nYaCalculado + nISPTNeto );
          if CalculandoNomina then
          begin
               with TCalcNomina(CalcNomina) do
               begin
                    if ( Result >= 0 ) then
                       RegistraISPT( Result )
                    else
                        RegistraCredito( Result );

                    if lRegistraCredito then
                    begin
                         RegistraCreditoMensual( nCreditoTabla );
                         RegistraCreditoPeriodo( nCredito );
                    end;
                    RegistraSubeMensual( nCreditoTabla );
               end;
          end;
     end;
end;

function TZetaCalcISPT.Calculate: TQREvResult;
var
   rDiasISPT, rGravado, rSalMin : TPesos;
   rMensual, rYaCalculado : TPesos;
   dFecha : TDate;
   TipoRegresa : TQREvResultType;
begin
     if ( ZetaEvaluador.oZetaCreator.dmTablasISPT = NIL ) then
        GetRequeridos(TipoRegresa);

     // DiasISPT
     if ParamVacio( 2 ) then
     begin
          rDiasISPT := DataSetEvaluador.FieldByName( 'DIAS_VACA' ).AsFloat *7/6;
          if ( DataSetEvaluador.FieldByName( 'DIAS' ).AsFloat > 0 ) then
             rDiasISPT := rDiasISPT + oZetaProvider.DatosPeriodo.Dias;  //  Nomina.tPeriodo.FieldByName( 'PE_DIAS' ).AsInteger;
     end
     else
         rDiasISPT := ParamFloat( 2 );
     // Fecha
     if ParamVacio( 11 ) then
     begin
          if ( ZetaEvaluador.CalculandoNomina ) then
             dFecha := oZetaProvider.DatosPeriodo.Inicio      // Al Calcular N�mina DatosPeriodo Ya Tiene Informaci�n
          else
             dFecha := oZetaProvider.FechaDefault;
     end
     else
         dFecha := ParamDate( 11 );
     // Sal_Min
     if ParamVacio( 3 ) then
     begin
        if ( ZetaEvaluador.CalculandoNomina ) then
           rSalMin := TCalcNomina(ZetaEvaluador.CalcNomina).nEmpleadoSalMin
        else
            // EM: Puse la zona como constante porque es muy
            // complicado obtener la zona real y este par�metro
            // ya no se usa.
            rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( dFecha, 'A' );
            { ER: 2.5 - Se Cambi� Date por el nuevo param�tro Fecha
              rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( Date, 'A' );
            }
     end
     else
         rSalMin := ParamFloat( 3 );
     // Per_Grava
     if ParamVacio( 7 ) then
        rGravado := ( TotalNomina( ttnPercepciones ) -
                      TotalNomina( ttnMensuales ) -
                      TotalNomina( ttnExentas  ) -
                      TotalNomina( ttnPerCal )  ) +
                      TotalNomina( ttnGravadoPrev )
     else
         rGravado := ParamFloat( 7 );
     // Per_Mensual
     if ParamVacio( 8 ) then
        rMensual := TotalNomina( ttnMensuales ) - TotalNomina( ttnExentoMensual )
     else
         rMensual := ParamFloat( 8 );
     // Ya_Calc
     if ParamVacio( 10 ) then
        rYaCalculado := TotalNomina( ttnImpCal )
     else
         rYaCalculado := ParamFloat( 10 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := CalcISPT(ZetaEvaluador, DefaultTasa( 0, ZetaEvaluador.oZetaCreator.dmTablasISPT.GetFactorSubsidio ),  // rSubsidio
                                DefaultTasa( 1, K_FACTOR_MENSUAL ), // rFactor
                                GetFactorSube( DefaultTasa( 12, K_FACTOR_MENSUAL ), oZetaProvider.DatosPeriodo.Year ), // rFactorSUBE
                                rDiasISPT,
                                DefaultFloat( 3, rSalMin ),
                                DefaultInteger( 4, 1 ), // ART.80
                                DefaultInteger( 5, 2 ), // ART.80-A //
                                DefaultInteger( 6, 3 ),  // ART.80-B //
                                rGravado,
                                rMensual,
                                DefaultBoolean( 9, FALSE ) , //lSuaviza,
                                FALSE, //Solo se usa para Ajusta_ISPT
                                rYaCalculado,
                                dFecha,
                                TRUE  ); //Es TRUE para que SI quede registrado el Credito APLICADO.
                                         //El credito aplicado nada mas se graba cuando se usa la f(x) ISPT
     end;
end;

{*********************** TZetaAjustaISPT *********************************}
{AJUSTA_ISPT( [Tipo, //Para la Reforma Fiscal 2008, se agreg� el tipo 6 (Ajusta al final del mes, topa el SUBE)
              Subsidio, Factor, Dias_Ispt, Sal_Minimo, T1, T2, T3,
              Per_Gravable, Per_Mensual, Ac_Gravable, AC_Mensual,
              Ret_Mensual,Cre_Mensual, Ac_Dias, Ya_Calc, Fecha,
              FactorSUBE //Este parametro se agrego para la Reforma Fiscal 2008
              ] )}


type
  TZetaAjustaISPT = class( TZetaRegresaImpuestos )
  private
    procedure AgregaAcumulado( const iConcepto : Integer; const sAlias : String);
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaAjustaISPT.AgregaAcumulado( const iConcepto : Integer; const sAlias : String);
var
    sScript : STring;
begin
     with oZetaProvider.DatosPeriodo do
{$ifdef INTERBASE}
        sScript := Format( '( select RESULTADO from sp_as( %d, %d, %d, %d, NOMINA.CB_CODIGO )) %s',
	                        [ iConcepto, Mes, Mes, Year, sAlias ] );
{$else}
        sScript := Format( 'DBO.SP_AS( %d, %d, %d, %d, NOMINA.CB_CODIGO ) %s',
	                        [ iConcepto, Mes, Mes, Year, sAlias ] );
{$endif}
     AgregaRequerido( sScript, enNomina );
end;

procedure TZetaAjustaISPT.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
          PreparaSalMin;
     end;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
          InitGlobales;
     end;

     if ParamVacio( 3 ) then
     begin
          AgregaParam( 'DIAS_VACA' );
          AgregaParam( 'DIAS' );
     end;
     AgregaAcumulado( 1000, 'AT0' );
     AgregaAcumulado( 1003, 'ATX');
     AgregaAcumulado( 1005, 'ATM');
     AgregaAcumulado( 1006, 'ATY');
     AgregaAcumulado( 1007, 'AT2');
     AgregaAcumulado( 1008, 'AT3');
     AgregaAcumulado( 1011, 'PREV_GR');
     AgregaAcumulado( oZetaProvider.GetGlobalInteger( K_GLOBAL_ISPT ), 'AISPT');
     AgregaAcumulado( oZetaProvider.GetGlobalInteger( K_GLOBAL_CREDITO) , 'ACREDITO');
     if not ZetaEvaluador.CalculandoNomina then
     begin
          if ParamVacio( 8 ) then
          begin
              AgregaRequerido( 'NOMINA.NO_PERCEPC', enNomina );
              AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
              AgregaRequerido( 'NOMINA.NO_X_ISPT',  enNomina );
              AgregaRequerido( 'NOMINA.NO_PER_CAL', enNomina );
              AgregaRequerido( 'NOMINA.NO_PREV_GR', enNomina );
          end;
          if ParamVacio( 9 ) then
          begin
              AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
              AgregaRequerido( 'NOMINA.NO_X_MENS', enNomina );
          end;
          if ParamVacio( 15 ) then
              AgregaRequerido( 'NOMINA.NO_IMP_CAL', enNomina );
     end;
end;

function AjustaISPT( oEvaluador : TZetaEvaluador;
                     Opciones : TTiposAjuste; nSubsidio, nFactor, nFactorSUBE, nDiasISPT, nSalMin : TPesos;
                     nT1, nT2, nT3 : Integer;
                     nGravado, nMensual, nAcGravado, nAcMensual, nRetMensual, nCredMensual,
                     nAcDias, nYaCalculado, nAcYaCalculado : TPesos;
                     const dFecha: TDate ) : TPesos;
var
   nMesISPT, nMesCredito : TPesos;
   nISPTNeto, nCredNeto : TPesos;
   lHazAjuste, lSuaviza, lAjustaSUBE : Boolean;
   nImpuesto : TPesos;
   iHubo: Word;
   lReformaFiscal2008: Boolean;
begin
     lReformaFiscal2008 := oEvaluador.oZetaProvider.DatosPeriodo.Year >= K_REFORMA_FISCAL_2008;

     nMesISPT := 0;
     nMesCredito := 0;
     lSuaviza := ( etaSuaviza in Opciones );
//     lAjustaSUBE := FALSE;
     with oEvaluador do
     begin
          if CalculandoNomina then
          begin
               with TCalcNomina(CalcNomina) do
               begin
                    RegistraISPT( 0 );
                    RegistraCredito( 0 );
               end;
          end;
          // Si siempre ajusta o es la Ultima del Mes, siempre y cuando no sea la primera del mes
          with oZetaProvider.DatosPeriodo do
          begin
               lHazAjuste := (( etaAjustaSiempre in Opciones ) OR ( PosMes = PerMes )) AND ( PosMes > 1 );
               lAjustaSUBE := ( ( etaTopaSUBE in Opciones ) and ( PosMes = PerMes ) );
          end;
          if ( Rastreando or RastreandoNomina ) then
          begin
               with Rastreador do
               begin
                    RastreoHeader( 'CALCULO DE ISPT AJUSTADO AL MES' );
                    if ( oZetaProvider.DatosPeriodo.PosMes = 1 ) then
                       RastreoMsg( '*** ES LA PRIMERA NOMINA DEL MES ***' );
                    if ( oZetaProvider.DatosPeriodo.PosMes = oZetaProvider.DatosPeriodo.PerMes ) then
                    begin
                         RastreoMsg( '*** ES LA ULTIMA NOMINA DEL MES ***' );
                    end;
               end;
          end;
     end;
     if NOT lHazAjuste then
     begin
          with oEvaluador do
          begin
               if Rastreando or RastreandoNomina then
                  Rastreador.RastreoMsg( '*** NO SE HACE AJUSTE. CALCULO DE ISPT NORMAL ***' );
          end;
          nImpuesto := CalcISPT( oEvaluador, nSubsidio, nFactor, nFactorSUBE, nDiasISPT, nSalMin, nT1, nT2, nT3, nGravado, nMensual, lSuaviza, lAjustaSUBE, nYaCalculado, dFecha );
          if ( etaEntregaCredito in Opciones ) then
             Result := nImpuesto
          else
          begin
               with oEvaluador do
               begin
                    if ( Rastreando or RastreandoNomina ) and ( nImpuesto < 0 ) then
                       Rastreador.RastreoMsg( Format( '*** NO SE ENTREGA %s: ISPT SE HACE 0 ***', [ aCreditoSalarioUpper[lReformaFiscal2008] ] ) );
                    if CalculandoNomina then
                       TCalcNomina(CalcNomina).RegistraCredito( 0 );
               end;
               Result := rMax( nImpuesto, 0 );
          end;
     end
     else
     begin
          with oEvaluador do
          begin
               if Rastreando or RastreandoNomina then
               begin
                    with Rastreador do
                    begin
                         RastreoPesos( '              Percepciones Gravadas : ', nGravado );
                         RastreoPesos( '(+)     Acumulado Peri�dico Gravado : ', nAcGravado );
                         RastreoPesos( '(=)         TOTAL Peri�dico Gravado : ', nGravado + nAcGravado );
                         RastreoEspacio;
                         RastreoPesos( '    Percepciones Mensuales Gravadas : ', nMensual );
                         RastreoPesos( '(+)       Acumulado Mensual Gravado : ', nAcMensual );
                         RastreoPesos( '(=)           TOTAL Mensual Gravado : ', nMensual + nAcMensual );
                         RastreoEspacio;
                         RastreoPesos( 'D�as Acumulados en el Mes : ', nAcDias );
                    end;
               end;
          end;
     	    nImpuesto := CalcISPT( oEvaluador, nSubsidio, nfactor, nFactorSUBE, nAcDias, nSalMin, nT1, nT2, nT3,
			                           nGravado+nAcGravado, nMensual+nAcMensual, lSuaviza, lAjustaSUBE, 0, dFecha );
          if ( nImpuesto >= 0 ) then
             nMesISPT := nImpuesto
          else
              nMesCredito := nImpuesto;
          nRetMensual := Redondea( nRetMensual - nAcYaCalculado );
          nISPTNeto := Redondea( nMesISPT - nRetMensual + nYaCalculado );

          with TCalcNomina(oEvaluador.CalcNomina) do
          begin
               if oEvaluador.CalculandoNomina then
                  RegistraISPT( nISPTNeto );

               nCredNeto := Redondea( nMesCredito - nCredMensual );

               if oEvaluador.CalculandoNomina then
                  RegistraCredito( nCredNeto );
          end;
          Result := nISPTNeto + nCredNeto;
          with oEvaluador do
          begin
               if Rastreando or RastreandoNomina then
               begin
                    iHubo := 0;
                    with Rastreador do
                    begin
                         if ( nMesISPT <> 0 ) or ( nRetMensual <> 0 ) or ( nYaCalculado <> 0 ) then
                         begin
                              iHubo := iHubo + 1;
                              RastreoHeader( 'AJUSTE DE ISPT CONTRA RETENIDO' );
                              RastreoPesos( '        ISPT Calculado Acumulado : ', nMesISPT );
                              RastreoPesos( '(-) Acumulado ISPT Retenido Neto : ', nRetMensual );
                              if ( nYaCalculado <> 0 ) then
                                 RastreoPesos( '(+)    ISPT Individual Peri�dico : ', nYaCalculado );
                              if ( nISPTNeto < 0 ) then
                                 RastreoPesos( '(=)    ISPT en Exceso a Regresar : ', nISPTNeto )
                              else
                                  RastreoPesos( '(=)      ISPT Ajustado a Retener : ', nISPTNeto );
                         end;
                         if ( nMesCredito <> 0 ) or ( nCredMensual <> 0 ) then
                         begin
                              iHubo := iHubo + 1;
                              RastreoHeader( Format('AJUSTE DE %s CONTRA RETENIDO',[ Trim( UpperCase(aCreditoSalarioLower[lReformaFiscal2008]) ) ] ));
                              RastreoPesos( Format('               %s Calculado : ',[aCreditoSalarioLower[lReformaFiscal2008]]), nMesCredito );
                              RastreoPesos( Format('(-)  Acumulado %s Entregado : ',[ Trim( aCreditoSalarioLower[lReformaFiscal2008] ) ]), nCredMensual );
                              if ( nCredNeto < 0 ) then
                                 RastreoPesos( Format('(=)  Saldo de %s a Entregar : ',[Trim( aCreditoSalarioLower[lReformaFiscal2008] ) ]), nCredNeto )
                              else
                                  RastreoPesos( Format('(=)  %s en Exceso a Retener : ',[Trim( aCreditoSalarioLower[lReformaFiscal2008] ) ]), nCredNeto );
                         end;
                         if ( iHubo = 2 ) then // Hubo los dos componentes, RastreoPesos neteo //
                         begin
                              RastreoHeader( Format('EFECTO NETO DE ISPT Y %s',[UpperCase(aCreditoSalarioLower[lReformaFiscal2008])]) );
                              RastreoPesos( '         ISPT            : ', nISPTNeto );
                              RastreoPesos( Format('(+)   %s al Salario : ',[aCreditoSalarioLower[lReformaFiscal2008]]), nCredNeto );
                              RastreoPesos( '(=)   ISPT Ajustado Neto : ', Result );
                         end;
                    end;
               end;
          end;
     end;
end;

function TZetaAjustaISPT.Calculate: TQREvResult;
var
   rDiasISPT, rGravado, rMensual, rSalMin, rYaCalculado : TPesos;
   Opciones: TTiposAjuste;
   dFecha : TDate;
begin
     with DataSetEvaluador, oZetaProvider do
     begin
          // Tipo
          if ParamVacio( 0 ) then
             Opciones := [ etaAjustaSiempre ]
          else
          begin
               case ParamInteger( 0 ) of
                    1: Opciones := [];
                    2: Opciones := [ etaEntregaCredito ];
                    3: Opciones := [ etaAjustaSiempre ];
                    4: Opciones := [ etaEntregaCredito, etaAjustaSiempre ];
                    5: Opciones := [ etaEntregaCredito, etaAjustaSiempre, etaSuaviza ];
               else
               begin
                    if ( DatosPeriodo.Year >= K_REFORMA_FISCAL_2008 ) then
                       Opciones := [ etaEntregaCredito, etaAjustaSiempre, etaTopaSUBE ]
                    else
                        Opciones := [ etaAjustaSiempre ];
               end;
               end;
          end;
          // Dias_Ispt
          if ParamVacio( 3 ) then
          begin
               rDiasISPT := FieldByName( 'DIAS_VACA' ).AsFloat* 7 / 6;
               if ( FieldByName( 'DIAS' ).AsFloat > 0 ) then
                  rDiasISPT := rDiasISPT + DatosPeriodo.Dias;
          end
          else
              rDiasISPT := ParamFloat( 3 );
          // Fecha
          if ParamVacio( 16 ) then
          begin
               if ( ZetaEvaluador.CalculandoNomina ) then
                  dFecha := DatosPeriodo.Inicio      // Al Calcular N�mina DatosPeriodo Ya Tiene Informaci�n
               else
                  dFecha := FechaDefault;
          end
          else
              dFecha := ParamDate( 16 );
          // Sal_Minimo
          if ParamVacio( 4 ) then
          begin
               if ( ZetaEvaluador.CalculandoNomina ) then
                  rSalMin := TCalcNomina(ZetaEvaluador.CalcNomina).nEmpleadoSalMin
               else
                   // EM: Puse la zona como constante porque es muy
                   // complicado obtener la zona real y este par�metro
                   // ya no se usa.
                   rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( dFecha, 'A' );
                   { ER: 2.5 - Se Cambi� Date por el nuevo param�tro Fecha
                     rSalMin := ZetaEvaluador.oZetaCreator.dmSalMin.GetSalMin( Date, 'A' );
                   }
          end
          else
              rSalMin := ParamFloat( 4 );
          // Per_Gravable
          if ParamVacio( 8 ) then
              rGravado := ( TotalNomina( ttnPercepciones ) -
                            TotalNomina( ttnMensuales ) -
                            TotalNomina( ttnExentas  ) -
                            TotalNomina( ttnPerCal ) ) +
                            TotalNomina( ttnGravadoPrev )
          else
              rGravado := ParamFloat( 8 );
          // Per_Mensual
          if ParamVacio( 9 ) then
             rMensual := TotalNomina( ttnMensuales ) - TotalNomina( ttnExentoMensual )
          else
              rMensual := ParamFloat( 9 );
          // Ya_Calc
          if ParamVacio( 15 ) then
             rYaCalculado := TotalNomina( ttnImpCal )
          else
              rYaCalculado := ParamFloat( 15 );

          Result.Kind      := resDouble;
          Result.dblResult := AjustaISPT( ZetaEvaluador,
                                          Opciones,
                                          DefaultTasa( 1, GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )),  // rSubsidio
                                          DefaultTasa( 2, K_FACTOR_MENSUAL ), // rFactor
                                          GetFactorSube( DefaultTasa( 17, K_FACTOR_MENSUAL ), oZetaProvider.DatosPeriodo.Year ), // FactorSUBE
                                          rDiasISPT,
                                          DefaultFloat( 4, rSalMin ),
                                          DefaultInteger( 5, 1 ), // ART.80
                                          DefaultInteger( 6, 2 ), // ART.80-A //
                                          DefaultInteger( 7, 3 ),  // ART.80-B //
                                          rGravado,
                                          rMensual,
                                          DefaultFloat( 10, FieldByName( 'AT0' ).AsFloat -  FieldByName( 'ATX' ).AsFloat -
                                                            FieldByName( 'ATM' ).AsFloat - FieldByName( 'AT2' ).AsFloat + FieldByName( 'PREV_GR' ).AsFloat ),  // racGravado
                                          DefaultFloat( 11, FieldByName( 'ATM' ).AsFloat -  FieldByName( 'ATY' ).AsFloat ), // raAcMensual
                                          DefaultFloat( 12, FieldByName( 'AISPT' ).AsFloat ), // rRetMensual
                                          DefaultFloat( 13, FieldByName( 'ACREDITO' ).AsFloat ), // rCredMensual
                                          DefaultFloat( 14, DatosPeriodo.DiasAcumula ), // rAcDias � temporal ? //
                                          rYaCalculado,
                                          DefaultFloat( 18, FieldByName( 'AT3' ).AsFloat ),  // rAcYaCalculado
                                          dFecha
                                          );   // Fecha
     end;
end;

{*********************** TZetaCalcSub *********************************}
{Calc_Sub( Valor [, Subsidio] [,T1] [, T2] [,Fecha] )}

type
  TZetaCalcSub = class( TZetaRegresa )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  Calculate: TQREvResult; override;
  end;

procedure TZetaCalcSub.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     ZetaEvaluador.oZetaCreator.PreparaTablasISPT;
     oZetaProvider.GetDatosActivos;

     if ( oZetaProvider.DatosPeriodo.Year = K_REFORMA_FISCAL_2008 ) then
        AgregaScript( '0.0', TipoRegresa, FALSE );
end;

function CalcSubsidio( oEvaluador : TZetaEvaluador; nValor : TPesos; nSubsidio : TTasa;
         nT1, nT2 : Integer; const dFecha: TDate ) : TPesos;
var
   nSubCuota, nSubMarginal : TPesos;
   nExcedente, nMarginal : TPesos;
   oRenglon : TRenglonNumerica;
   oRenglon2 : TRenglonNumerica;
   nExcedenteSubsidio, nMarginalSubsidio : TPesos;
   lExcedenteSubsidio, lMarginalSubsidio : Boolean;
begin
     if ( nValor <= 0 ) OR ( nSubsidio <= 0 ) then
     begin
          Result := 0;
          Exit;
     end;

     // Impuesto Art. 80
     oRenglon := oEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT1, nValor, dFecha );
     with oRenglon do
     begin
          nExcedente := nValor - Inferior;
          nMarginal := nExcedente * ( Tasa/100 );
     end;

     lExcedenteSubsidio := FALSE;
     // Si la tabla 2 es negativa, vuelve a calcular el impuesto marginal
     if ( nT2 < 0 ) then
     begin
          lMarginalSubsidio := TRUE;
          nT2 := -nT2;
          if ( nT2 > 1000 ) then
          begin
               lExcedenteSubsidio := TRUE;
               nT2 := nT2 - 1000;
          end;
     end
     else
         lMarginalSubsidio := FALSE;

     oRenglon2 := oEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT2, nValor, dFecha );
     with oRenglon2 do
     begin
          nSubCuota := Cuota;
          if ( lMarginalSubsidio ) then
          begin
               nExcedenteSubsidio := ( nValor - Inferior );
               if ( lExcedenteSubsidio ) then
                  nMarginalSubsidio := nExcedenteSubsidio
               else
                   nMarginalSubsidio  := nExcedenteSubsidio * ( oRenglon.Tasa / 100 );
          end
          else
              nMarginalSubsidio := nMarginal;

          nSubMarginal := ( nMarginalSubsidio * ( Tasa / 100 ) );
     end;

     Result := Redondea( ( nSubCuota + nSubMarginal ) * nSubsidio );
end;

function TZetaCalcSub.Calculate: TQREvResult;
var
   rValor: TPesos;
   rSubsidio: TTasa;
   iT1, iT2: Integer;
begin
     if ParamVacio( 0 ) then
        rValor := 0
     else
         rValor := ParamFloat( 0 );
     if ParamVacio( 1 ) then
        rSubsidio := oZetaProvider.GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )
     else
         rSubsidio := ParamTasa( 1 );
     if ParamVacio( 2 ) then
        iT1 := 1        // ART.80 //
     else
         iT1 := ParamInteger( 2 );
     if ParamVacio( 3 ) then
        iT2 := 2        // ART.80-A //
     else
         iT2 := ParamInteger( 3 );
     with Result do
     begin
          Kind := resDouble;
          dblResult := CalcSubsidio( ZetaEvaluador,
                                     rValor,
                                     rSubsidio,
                                     iT1,
                                     iT2,
                                     DefaultDate( 4, oZetaProvider.FechaDefault ) );
     end;
end;

{*********************** TZetaSubsidio *********************************}
{Subsidio( Subsidio, Factor, Dias_Ispt, T1, T2, Per_gravable, Per_mensual, Fecha )}

type
  TZetaSubsidio = class( TZetaRegresaImpuestos )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  Calculate: TQREvResult; override;
  end;

procedure TZetaSubsidio.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;
     oZetaProvider.GetDatosActivos;
     ZetaEvaluador.oZetaCreator.PreparaTablasISPT;
     if ParamVacio( 2 ) then
     begin
          AgregaParam( 'DIAS' );
          AgregaParam( 'DIAS_VACA' );
     end;
     if not ZetaEvaluador.CalculandoNomina then
     begin
          if ParamVacio( 5 ) then
          begin
              AgregaRequerido( 'NOMINA.NO_PERCEPC', enNomina );
              AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
              AgregaRequerido( 'NOMINA.NO_X_ISPT',  enNomina );
              AgregaRequerido( 'NOMINA.NO_PER_CAL', enNomina );
              AgregaRequerido( 'NOMINA.NO_PREV_GR', enNomina );
          end;
          if ParamVacio( 6 ) then
          begin
              AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
              AgregaRequerido( 'NOMINA.NO_X_MENS', enNomina );
          end;
     end;
end;

function Subsidio( oEvaluador : TZetaEvaluador; nSubsidio, nFactor, nDiasISPT: TPesos;
                   nT1, nT2 : Integer; const dFecha: TDate; nGravado, nMensual : TPesos ) : TPesos;
var
   nPercProm : TPesos;
   nSubPeriod, nSubMensual, nSubPropo : TPesos;
begin
     if ( nDiasISPT <= 0 ) then
        if ( nGravado > 0 ) OR ( nMensual > 0 ) then
           nDiasISPT := 1
        else
        begin
             Result := 0;
             Exit;
        end;

     nPercProm := nGravado / nDiasISPT * nFactor;
     if ( nMensual > 0 ) then
     begin
          nSubPeriod  := CalcSubsidio( oEvaluador, nPercProm, nSubsidio, nT1, nT2, dFecha );
          nSubMensual := CalcSubsidio( oEvaluador,nPercProm+nMensual, nSubsidio, nT1, nT2, dFecha );
          nSubPropo   := nSubPeriod/nFactor*nDiasISPT;
          Result := Redondea( nSubPropo + nSubMensual - nSubPeriod );
     end
     else
          Result := Redondea( CalcSubsidio( oEvaluador,nPercProm, nSubsidio, nT1, nT2, dFecha ) / nFactor * nDiasISPT );
end;

function TZetaSubsidio.Calculate: TQREvResult;
var
   rSubsidio, rFactor, rDiasISPT, rGravado, rMensual: TPesos;
   iT1, iT2: Integer;
   dFecha : TDate;
begin
     with oZetaProvider, TCalcNomina(ZetaEvaluador.CalcNomina) do
     begin
          // Subsidio
          if ParamVacio( 0 ) then
             rSubsidio := GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )
          else
              rSubsidio := ParamTasa( 0 );
          // Factor
          if ParamVacio( 1 ) then
             rFactor := K_FACTOR_MENSUAL
          else
              rFactor := ParamTasa( 1 );
          // Dias_Ispt
          if ParamVacio( 2 ) then
          begin
               rDiasISPT := DataSetEvaluador.FieldByName( 'DIAS_VACA' ).AsFloat * 7 / 6;
               if ( DataSetEvaluador.FieldByName( 'DIAS' ).AsFloat > 0 ) then
                  rDiasISPT := rDiasISPT + DatosPeriodo.Dias;
          end
          else
              rDiasISPT := ParamFloat( 2 );
          // T1
          if ParamVacio( 3 ) then
             iT1 := 1        // ART.80 //
          else
              iT1 := ParamInteger( 3 );
          // T2
          if ParamVacio( 4 ) then
             iT2 := 2        // ART.80-A //
          else
              iT2 := ParamInteger( 4 );
          // Per_gravable
          if ParamVacio( 5 ) then
              rGravado := ( TotalNomina( ttnPercepciones ) -
                            TotalNomina( ttnMensuales ) -
                            TotalNomina( ttnExentas  ) -
                            TotalNomina( ttnPerCal ) )+
                            TotalNomina( ttnGravadoPrev )
          else
              rGravado := ParamFloat( 5 );
          // Per_mensual
          if ParamVacio( 6 ) then
             rMensual := TotalNomina( ttnMensuales ) - TotalNomina( ttnExentoMensual )
          else
              rMensual := ParamFloat( 6 );
          // Fecha
          if ParamVacio( 7 ) then
          begin
               if ( ZetaEvaluador.CalculandoNomina ) then
                  dFecha := DatosPeriodo.Inicio      // Al Calcular N�mina DatosPeriodo Ya Tiene Informaci�n
               else
                  dFecha := FechaDefault;
          end
          else
              dFecha := ParamDate( 7 );

          with Result do
          begin
               Kind := resDouble;
               dblResult := Subsidio( ZetaEvaluador,
                                      rSubsidio,
                                      rFactor,
                                      rDiasISPT,
                                      iT1,
                                      iT2,
                                      dFecha,
                                      rGravado,
                                      rMensual );
          end;
     end;
end;

{*********************** TZetaCALC_CREDITO *********************************}
type
  TZetaCALC_CREDITO = class ( TZetaRegresaFloat )
  public
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;

  end;

procedure TZetaCALC_CREDITO.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
end;

function TZetaCALC_CREDITO.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).nRegistraCredito;
end;

{*********************** TZetaCALC_ISPT *********************************}
type
  TZetaCALC_ISPT = class ( TZetaRegresaFloat )
  public
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
end;

procedure TZetaCALC_ISPT.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
end;

function TZetaCALC_ISPT.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).nRegistraISPT;
end;

{*********************** TZetaART_86 *********************************}
{Art_86( Monto [, subsidio, Exento, Sal_mensual, Factor, Sal_minimo, T1, T2, T3, Periodico, DiasBase, Fecha] )}

type
  TZetaArt86 = class( TZetaRegresaImpuestos )
  public
    procedure ListaRequeridos;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

procedure TZetaArt86.ListaRequeridos;
begin
     AgregaParam( 'DIAS' );
     AgregaParam( 'DIAS_VACA' );
     AgregaParam( 'SALARIO' );
     AgregaRequerido( 'NOMINA.NO_PERCEPC', enNomina );
     AgregaRequerido( 'NOMINA.NO_PER_MEN', enNomina );
     AgregaRequerido( 'NOMINA.NO_X_ISPT',  enNomina );
     AgregaRequerido( 'NOMINA.NO_PER_CAL', enNomina );
     AgregaRequerido( 'NOMINA.NO_IMP_CAL', enNomina );
     AgregaRequerido( 'NOMINA.NO_X_MENS',  enNomina );
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
          PreparaSalMin;
     end;
     oZetaProvider.GetDatosPeriodo;
end;

procedure TZetaArt86.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
     inherited;
     ListaRequeridos;
     ParametrosFijos( 1 );
end;

function ART86( oEvaluador : TZetaEvaluador;
                nAguinaldo, nSubsidio, nExento, nSalMensual,  nFactor, nFactorSUBE, nSalMin : TPesos;
                iT1, iT2, iT3 : Integer; const dFecha: TDate; lPeriodico : Boolean;
                nDiasBase, nGravado, nYaCalculado, nMensual, nDiasISPT : TPesos  ) : TPesos;
var
   nIspt86, nAguiMensual, nPromMensual, nIspt1, nIspt2, nAguiNeto, nTasaAplicar, nIspt86Calc: TPesos;
begin
     // Pendiente :  aCreditoAplicado[ K_CREDITO_ART86 ] := 0

     nAguiNeto := rMax( nAguinaldo - nExento, 0 );
     if ( nAguiNeto <= 0 ) then
         nIspt86 := 0
     else
     begin
         nAguiMensual := nAguiNeto/nDiasBase*30.4;
         nPromMensual := nAguiMensual + nSalMensual ;
         with oEvaluador do
         begin
              if Rastreando or RastreandoNomina then
              begin
                   with Rastreador do
                   begin
                        RastreoHeader( 'CALCULO DE PROMEDIO MENSUAL' );
                        RastreoPesos( '          Monto Bruto : ', nAguinaldo );
                        RastreoPesos( '(-)      Parte Exenta : ', nExento );
                        RastreoPesos( '(=)    Monto Gravable : ', nAguiNeto );
                        RastreoEspacio;
                        RastreoPesos( '       Monto Gravable : ', nAguiNeto );
                        RastreoPesos( '(/)         D�as Base : ', nDiasBase );
                        RastreoPesos( '(*)              30.4 : ', 30.4 );
                        RastreoPesos( '(=)  Gravable Mensual : ', nAguiMensual );
                        RastreoEspacio;
                        RastreoPesos( '     Gravable Mensual : ', nAguiMensual );
                        RastreoPesos( '(+) 1 Salario Mensual : ', nSalMensual  );
                        RastreoPesos( '(=)  Promedio Mensual : ', nPromMensual );
                   end;
              end;
         end;

         nIspt1	 := rMax( CalcImp( oEvaluador, nPromMensual, nPromMensual, nSubsidio, nFactor, nFactorSUBE, nSalMin, iT1, iT2, iT3, dFecha ), 0 );
         nIspt2	 := rMax( CalcImp( oEvaluador, nSalMensual, nSalMensual, nSubsidio, nFactor, nFactorSUBE, nSalMin, iT1, iT2, iT3, dFecha ), 0 );
         nIspt86 := rMax( nIspt1 - nIspt2, 0 );

         if ( nAguiMensual <= 0.00  )then
            nTasaAplicar := 0.00
         else
             nTasaAplicar := nIspt86/nAguiMensual;

         nIspt86Calc  := nTasaAplicar * nAguiNeto;

         with oEvaluador do
         begin
              if Rastreando or RastreandoNomina then
              begin
                   with Rastreador do
                   begin
                        RastreoHeader( 'ARTICULO 86 DEL REGLAMENTO ISR' );
                        RastreoPesos( '  Gratificaci�n Art86 : ' , nAguinaldo);
                        RastreoPesos( '(-)      Parte Exenta : ' , nExento);
                        RastreoPesos( '(=)     Parte Gravada : ' , nAguiNeto);
                        RastreoEspacio;
                        RastreoPesos( '      Salario Mensual : ' , nSalMensual);
                        RastreoPesos( '(+) Gratific. Mensual : ' , nAguiMensual );
                        RastreoPesos( '(=)  Promedio Mensual : ' , nPromMensual );
                        RastreoEspacio;
                        RastreoPesos( '        ISPT Promedio : ' , nIspt1);
                        RastreoPesos( '(-) ISPT Sal. Mensual : ' , nIspt2);
                        RastreoPesos( '(=) ISPT a Considerar : ' , nIspt86);
                        RastreoEspacio;
                        RastreoPesos( '    ISPT a Considerar : ' , nIspt86);
                        RastreoPesos( '(/) Gratific. Mensual : ' , nAguiMensual);
                        RastreoPesos( '(=)    Tasa a Aplicar : ' , nTasaAplicar * 100 );
                        RastreoPesos( '(*) Gratific. Gravada : ' , nAguiNeto);
                        RastreoPesos( '(=)      ISPT Art. 86 : ' , nIspt86Calc);
                   end;
              end;
         end;

         nIspt86  := Redondea( nIspt86Calc );
     end;

     if lPeriodico then
         Result := nIspt86 + CalcISPT( oEvaluador,
                                       nSubsidio, nFactor, nFactorSUBE, nDiasISPT, nSalMin,
                                       iT1, iT2, iT3, nGravado,
                                       rMax( nMensual - nAguiNeto, 0 ),
                                       FALSE,
                                       FALSE,
                                       nYaCalculado,
                                       dFecha )
     else
         Result := nIspt86;
end;


function TZetaArt86.Calculate: TQREvResult;
var
   nGravado, nYaCalculado, nMensual, nDiasISPT : TPesos;
begin
    //with TCalcNomina(ZetaEvaluador.CalcNomina) do
    //begin
    nGravado := ( TotalNomina( ttnPercepciones ) -
                  TotalNomina( ttnMensuales ) -
                  TotalNomina( ttnExentas  ) -
                  TotalNomina( ttnPerCal ) ) +
                  TotalNomina( ttnGravadoPrev );
    nYaCalculado := TotalNomina( ttnImpCal );
    nMensual     := TotalNomina( ttnMensuales ) - TotalNomina( ttnExentoMensual );
    //end;
    with DataSetEvaluador do
    begin
         nDiasISPT := FieldByName( 'DIAS_VACA' ).AsFloat*7/6;
         if ( FieldByName( 'DIAS' ).AsFloat > 0 ) then
            nDiasISPT := nDiasISPT + oZetaProvider.DatosPeriodo.Dias;

    end;
    with Result, TCalcNomina(ZetaEvaluador.CalcNomina) do
    begin
         Kind := resDouble;
         dblResult := Art86( ZetaEvaluador,
                             ParamFloat( 0 ), // nAguinaldo
                             DefaultTasa( 1, oZetaProvider.GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )), // nSubsidio,
                             DefaultFloat( 2, 30 * nEmpleadoSalMin ), // nExento,
                             DefaultFloat( 3, K_FACTOR_MENSUAL * DataSetEvaluador.FieldByName( 'SALARIO' ).AsFloat ),// Mensual
                             DefaultFloat( 4, K_FACTOR_MENSUAL ),// Factor
                             DefaultFloat( 4, K_FACTOR_MENSUAL ),// Factor
                             DefaultFloat( 5, nEmpleadoSalMin), // SalMin
                             DefaultInteger( 6, 1 ), //iT1
                             DefaultInteger( 7, 2 ), //iT2
                             DefaultInteger( 8, 3 ), //iT3,
                             DefaultDate( 11, oZetaProvider.DatosPeriodo.Inicio ),
                             DefaultBoolean( 9, TRUE ), // lPeriodico
                             DefaultFloat( 10, 365 ),
                             nGravado,
                             nYaCalculado,
                             nMensual,
                             nDiasISPT );
    end;
end;

{*********************** TZetaART_86I *********************************}
{Art86_I( [ Monto, Subsidio, Exento, Sal_mensual, Factor, Sal_minimo, T1, T2, T3, DiasBase, Fecha ] )}
type
  TZetaArt86I = class( TZetaArt86 )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  Calculate: TQREvResult; override;
  end;

procedure TZetaArt86I.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
     TipoRegresa := resDouble;
     ListaRequeridos;
end;

function TZetaArt86I.Calculate: TQREvResult;
var
   nGravado, nYaCalculado, nMensual : TPesos;
begin
    //with TCalcNomina(ZetaEvaluador.CalcNomina) do
   // begin
     nGravado := ( TotalNomina( ttnPercepciones ) -
                   TotalNomina( ttnMensuales ) -
                   TotalNomina( ttnExentas  ) -
                   TotalNomina( ttnPerCal ) ) +
                   TotalNomina( ttnGravadoPrev );
     nYaCalculado := TotalNomina( ttnImpCal );
     nMensual     := TotalNomina( ttnMensuales ) - TotalNomina( ttnExentoMensual );
    //end;
     with Result, TCalcNomina(ZetaEvaluador.CalcNomina) do
     begin
          Kind      := resDouble;
          dblResult := Art86( ZetaEvaluador,
                              DefaultFloat(  0, nMontoFormula ),// Base
                              DefaultTasa( 1, oZetaProvider.GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )), // Subsidio
                              DefaultFloat( 2, nExentoFormula ), // Exento
                              DefaultFloat( 3, K_FACTOR_MENSUAL * DataSetEvaluador.FieldByName( 'SALARIO' ).AsFloat ),// Mensual
                              DefaultFloat( 4, K_FACTOR_MENSUAL ),// Factor
                              DefaultFloat( 4, K_FACTOR_MENSUAL ),// Factor
                              DefaultFloat( 5, nEmpleadoSalMin), // SalMin
                              DefaultInteger( 6, 1 ), //iT1
                              DefaultInteger( 7, 2 ), //iT2
                              DefaultInteger( 8, 3 ), //iT3,
                              DefaultDate( 10, oZetaProvider.DatosPeriodo.Inicio ),  // Fecha
                              FALSE, // lPeriodico,
                              DefaultFloat( 9, 365 ),
                              nGravado,
                              nYaCalculado,
                              nMensual,
                              0 ); // DiasBase
     end;
end;

{*********************** TZetaART80Separa *********************************}
{Art80_Separa( Base [, Subsidio, Exento, Sal_mensual, Factor, Sal_minimo, T1, T2, T3, Fecha] )}
type
  TZetaArt80Separa = class( TZetaRegresaFloat )
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  Calculate: TQREvResult; override;
  end;

procedure TZetaArt80Separa.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
     TipoRegresa := resDouble;
     AgregaParam( 'SALARIO' );
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
          PreparaSalMin;
     end;
end;

function ART80Separa( oEvaluador:TZetaEvaluador; nBase, nSubsidio, nExento, nSalMensual, nFactor, nFactorSUBE, nSalMin : TPesos;
         iT1, iT2, iT3 : Integer; const dFecha: TDate ) : TPesos;
var
   nSepNeto, nIsptSepara, nIsptMensual : TPesos;
   lReformaFiscal2008: Boolean;
begin
     // aCreditoAplicado[ K_CREDITO_ART80SEP ] := 0
     lReformaFiscal2008 := ( oEvaluador.oZetaProvider.DatosPeriodo.Year >= K_REFORMA_FISCAL_2008 );
     if lReformaFiscal2008 then
        iT3 := Abs( iT3 ) * -1;

     nSepNeto := rMax( nBase - nExento, 0 );
     with oEvaluador do
     begin
          if Rastreando or RastreandoNomina then
          begin
               with Rastreador do
               begin
                    RastreoHeader( 'ART.80: PAGOS POR SEPARACION');
                    RastreoPesos( '          Monto Bruto : ', nBase );
                    RastreoPesos( '(-)      Parte Exenta : ', nExento );
                    RastreoPesos( '(=)    Monto Gravable : ', nSepNeto );
                    RastreoEspacio;
                    RastreoPesos( '      Salario Mensual : ', nSalMensual );
                    RastreoEspacio;
               end;
          end;
     end;

     if ( nSepNeto <= 0 ) then
        nIsptSepara := 0
     else
     begin
         if ( nSepNeto < nSalMensual ) then
         begin
              with oEvaluador do
              begin
                   if Rastreando or RastreandoNomina then
                      Rastreador.RastreoTexto( '*** Monto Gravable es menor que un Salario Mensual ***', '' );
              end;
              nIsptSepara := CalcImp( oEvaluador,nSepNeto, nSepNeto, nSubsidio, nFactor, nFactorSUBE, nSalMin, iT1, iT2, iT3, dFecha );
              // Pendiente :      aCreditoAplicado[ K_CREDITO_ART80SEP ] := nCreditoAplicado
         end
         else
         begin
              with oEvaluador do
              begin
                   if Rastreando or RastreandoNomina then
                      Rastreador.RastreoTexto( '*** Monto Gravable es MAYOR que un Salario Mensual ***','' );
                   nIsptMensual := rMax( CalcImp( oEvaluador,nSalMensual,nSalMensual,  nSubsidio, nFactor, nFactorSUBE, nSalMin, iT1, iT2, iT3, dFecha ), 0 );
                   nIsptSepara  := Redondea( nIsptMensual / nSalMensual * nSepNeto );
                   // Pendiente  :  aCreditoAplicado[ K_CREDITO_ART80SEP ] := nCreditoAplicado
                   if Rastreando or RastreandoNomina then
                   begin
                        with Rastreador do
                        begin
                             RastreoHeader( ' Art. 80: Percepciones por SEPARACION' );
                             RastreoPesos( '    ISPT Sal. Mensual : ' , nIsptMensual );
                             RastreoPesos( '(/)  Salario Mensual  : ' , nSalMensual );
                             RastreoPesos( '(=)    Tasa a Aplicar : ' , nIsptMensual/ nSalMensual*100);
                             RastreoPesos( '(*)    Monto Gravable : ' , nSepNeto);
                             RastreoPesos( '(=) ISPT Art. 80 Sep. : ' , nIsptSepara );
                        end;
                   end;
              end;
         end;
     end;
     Result := nIsptSepara;
end;

function TZetaArt80Separa.Calculate: TQREvResult;
begin
     with Result, TCalcNomina(ZetaEvaluador.CalcNomina) do
     begin
          Kind := resDouble;
          dblResult := Art80Separa( ZetaEvaluador,
                                    DefaultFloat(  0, nMontoFormula ),// Base
                                    DefaultTasa( 1, oZetaProvider.GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO )), // Subsidio
                                    DefaultFloat( 2, nExentoFormula ), // Exento
                                    DefaultFloat( 3, K_FACTOR_MENSUAL * DataSetEvaluador.FieldByName( 'SALARIO' ).AsFloat ),// Mensual
                                    DefaultFloat( 4, K_FACTOR_MENSUAL ),// Factor
                                    DefaultFloat( 4, K_FACTOR_MENSUAL ),// Factor
                                    DefaultFloat( 5, nEmpleadoSalMin), // SalMin
                                    DefaultInteger( 6, 1 ), //iT1
                                    DefaultInteger( 7, 2 ), //iT2
                                    DefaultInteger( 8, 3 ), //iT3,
                                    DefaultDate( 9, oZetaProvider.DatosPeriodo.Inicio ) ); // Fecha

          dblResult := Max( 0, dblResult );
     end;
end;

{*********************** TZetaCreditoAplicado *********************************}
{Credito_Ap( [nPeriodicidad] )}
type
  TZetaCreditoAplicado = class ( TZetaRegresaFloat )
  public
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaCreditoAplicado.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
end;

function TZetaCreditoAplicado.Calculate: TQREvResult;
begin
     Result.Kind      := resDouble;
     Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).CreditoAplicado( DefaultInteger( 0, 1 ) = 1 );
end;

{*********************** TZetaSUBEAplicado *********************************}
{SUBE_AP( [lTodosPeriodos] )}
type
  TZetaSUBEAplicado = class ( TZetaRegresaFloat )
  public
   function Calculate: TQREvResult; override;
   procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

procedure TZetaSUBEAplicado.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );
end;

function TZetaSUBEAplicado.Calculate: TQREvResult;
var
   lRegresaTodos: Boolean;
begin
     Result.Kind := resDouble;
     lRegresaTodos := ( DefaultInteger( 0, 0 ) = 1 );
     with oZetaProvider.DatosPeriodo do
     begin
          if lRegresaTodos or ( PosMes = PerMes ) then   // Si es mensual se revisa si es el �ltimo periodo del mes o aplica para todos los periodos
             Result.dblResult := TCalcNomina(ZetaEvaluador.CalcNomina).SubeAplicado
          else
              Result.dblResult := 0;
     end;
end;

{*********************** TZetaCALC_IMP_ANUAL *********************************}
const //Constantes para la funcion AJUSTA_ISR()
      K_AJUSTA_CADA_PERIODO = 0;
      K_PAGA_SUBE_A_FIN_MES = 1;
      K_TOPA_SUBE_A_FIN_MES = 2;
      K_SUAVIZA_PERCEPCIONES_MENSUALES = 3;
      K_FACTOR_DIAS_MENSUALES = 4;
      K_FACTOR_SUBE_DIAS_MENSUALES = 5;
      K_DIAS_TRABAJADOS = 6;
      K_DIAS_TRABAJADOS_ACUMULADOS = 7;
      K_T1 = 8;
      K_T3 = 9;
      K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO = 10;
      K_PERCEPCIONES_GRAVABLES_ACUMULADAS = 11;
      K_PERCEPCIONES_TIPO_MENSUAL_DEL_PERIODO_ACTIVO = 12;
      K_PERCEPCIONES_TIPO_MENSUAL_ACUMULADAS = 13;
      K_IMPUESTO_RETENIDO_ACUMULADO = 14;
      K_SUBE_RETENIDO_ACUMULADO = 15;
      K_SUBE_APLICADO_ACUMULADO = 16;
      K_IMPUESTO_INDIVIDUAL = 17;
      K_IMPUESTO_INDIVIDUAL_ACUMULADO = 18;
      K_FECHA = 19;
      K_MESFINAL = 20;
      K_ALIAS_ANUAL = '_ANNUAL';
      K_CONCEPTO_SUBE_APLICADO = 153;

type
TZetaCALC_IMP_ANUAL = class( TZetaRegresaImpuestos )
  protected
      Opciones: TTiposAjuste;
      rACDias, rACSubeAplicado,
      rGravadoMensualSUBE,rTotPercepcionesGravadas: TPesos;
      rFactorSUBE : TPesos;
      nT1, nT3: integer;
      FMesInicial, FMesFinal: integer;
      dFecha: TDate;
      FLowerSUBE: string;
      function CalcImpuestoISR: TPesos;
  public
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function  Calculate: TQREvResult; override;
end;

procedure TZetaCALC_IMP_ANUAL.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
     end;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
          InitGlobales;
     end;
     { **** ******************************************)
     { **** Parametro -- Valor: este par�metro es requerido }
     ParametrosFijos( 1 );
end;

function  TZetaCALC_IMP_ANUAL.Calculate: TQREvResult;
begin
     { **** ******************************************)
     { **** Parametro -- Valor: este par�metro es requerido }
     rTotPercepcionesGravadas := ParamFloat( 0 );
     
     { **** ******************************************)
     { **** Parametro -- T1 }
     nT1 := DefaultInteger( K_T1, 1 );
     nT3 := 0; //Quiero asegurarme que no calcula SUBE.

     with oZetaProvider,DatosPeriodo do
     begin
          { **** ******************************************)
          { **** Parametro -- Fecha }
          if ParamVacio( 2 ) then
          begin
               if ( ZetaEvaluador.CalculandoNomina ) then
                  dFecha := Inicio      // Al Calcular N�mina DatosPeriodo Ya Tiene Informaci�n
               else
                  dFecha := FechaDefault;
          end
          else
          begin
              dFecha := ParamDate( 2 );
          end;

          { **** ******************************************)
          { **** Parametro -- MES }
          FMesInicial:= 1;
          if ZetaEvaluador.CalculandoNomina then
          begin
               //En el calculo de la nomina no aplica el parametro MES, porque no se pueden calcular unos empleados con un mes y otros con otro mes.
               FMesFinal := Mes;
          end
          else
          begin
               FMesFinal:= DefaultInteger( 3, Mes );
          end;
     end;

     Result.Kind := resDouble;
     Result.dblResult := CalcImpuestoISR;
end;

function TZetaCALC_IMP_ANUAL.CalcImpuestoISR: TPesos;
var
   oRenglon: TRenglonNumerica;
   //rGravadoSUBE,
   nExcedente, nMarginal, nImpuesto, nSube : TPesos;
begin
     //rGravadoSUBE := 0;
     // Si valor <= 0, no se requiere investigar y calcular
     if ( rTotPercepcionesGravadas <= 0 ) then
     begin
          with ZetaEvaluador  do
          begin
               if Rastreando or RastreandoNomina then
               begin
                    with Rastreador do
                    begin
                         RastreoHeader( 'CONSULTA DE TABLA' );
                         RastreoPesos( ' Monto Gravado : ', rTotPercepcionesGravadas );
                         RastreoPesos( ' ISR Calculado : ', 0 );
                    end;
               end;
          end;
          Result := 0;
          Exit;
     end;

     // Impuesto Art. 80
     with oZetaProvider do
     begin
          oRenglon := ZetaEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT1, rTotPercepcionesGravadas / MesDefault, dFecha );
          with oRenglon do
          begin
               if ( Inferior > 0.01 ) then          // Si es 0.01 o menor se deja el valor ya que es el primer renglon y debe empezar en 0.01
                  Inferior := Inferior * MesDefault;
               Cuota := Cuota * MesDefault;
          end;
     end;

     with oRenglon do
     begin
          nExcedente := rTotPercepcionesGravadas - Inferior;
          nMarginal := nExcedente * ( Tasa/100 );
          nImpuesto := Cuota + nMarginal;
     end;

     // Cr�dito al Salario: Art. 80-B
     if ( nT3 <= 0 ) then
        nSube := 0
     else
         nSube := TCalcNomina(ZetaEvaluador.CalcNomina).SubeAplicado;   // Se invoc� anteriormente el procedimiento AJUSTA_ISPT
     {
     begin
          if not ( etaTopaSUBE in Opciones ) then
             rGravadoSUBE := rGravadoMensualSUBE / rACDias * rFactorSUBE
          else
              rGravadoSUBE := rGravadoMensualSUBE;

          if rGravadoSUBE <> 0 then
             nSube := ZetaEvaluador.oZetaCreator.dmTablasISPT.ConsultaTablaNumerica( nT3, rGravadoSUBE, dFecha ).Cuota
          else
              nSube := 0;
     end;
     }

     // Subsidio: Art. 80-A
     with ZetaEvaluador do
     begin
          if Rastreando or RastreandoNomina then
          begin
               with Rastreador do
               begin
                    RastreoHeader( 'CONSULTA DE TABLA ' );
                    RastreoPesos ( '           Monto Gravado : ', rTotPercepcionesGravadas );
                    RastreoPesos ( '(-)      L�mite Inferior : ', oRenglon.Inferior );
                    RastreoPesos ( '(=)            Excedente : ', nExcedente );
                    RastreoFactor( '(X)             Tasa (%) : ', oRenglon.Tasa );
                    RastreoPesos ( '(=)    Impuesto Marginal : ', nMarginal );
                    RastreoPesos ( '(+)           Cuota Fija : ', oRenglon.Cuota );
                    RastreoPesos ( '(=)     ISR sin Subsidio : ', nImpuesto );
                    RastreoEspacio;

                    if ( nT3 > 0 ) then
                    begin
                         RastreoHeader( 'SUBSIDIO AL EMPLEO APLICADO' );
                         {
                         RastreoPesos( '              Percepciones Gravadas : ', rGravadoMensualSUBE );
                         if ( not ( etaTopaSUBE in Opciones ) ) then
                         begin
                              RastreoPesos( '     (/)          D�as Considerados : ', rACDias  );
                              RastreoPesos( '     (X)                Factor SUBE : ', rFactorSUBE );
                              RastreoPesos( '     (=) Percepciones Gravadas SUBE : ', rGravadoSUBE );
                         end;
                         }
                         RastreoPesos( '                      SUBE Aplicado : ', nSube );
                         {
                         if not ( etaTopaSUBE in Opciones ) then
                         begin
                              nSube := nSUBE / rFactorSUBE * rACDias;
                              RastreoPesos( '     (/)                Factor SUBE : ', rFactorSUBE );
                              RastreoPesos( '     (X)          D�as Considerados : ', rACDias  );
                              RastreoPesos( '                      SUBE Aplicado : ', nSUBE );
                         end;
                         }
                         if ( oZetaProvider.DatosPeriodo.Mes > 1 ) then
                         begin
                              nSube := rACSubeAplicado + nSube;
                              RastreoEspacio;
                              RastreoPesos( '     (+)  Acumulado SUBE Aplicado : ', rACSubeAplicado );
                              RastreoPesos( '     (=)      Total SUBE Aplicado : ', nSube );
                         end;
                    end;
                    RastreoHeader( 'DE ACUERDO A FRACCIONES II y III DEL ARTICULO 116' );
                    RastreoPesos( '                     Excedente ISR Anual: ', nImpuesto );
                    RastreoPesos( '(-)                  SUBE Anual Aplicado: ', nSube );

{
                    RastreoEspacio;
                    RastreoPesos( '            ISR sin Subsidio : ', nImpuesto );
                    RastreoPesos( '    (-)  Total SUBE Aplicado : ', nSube );

                    if ( ( nImpuesto - nSUBE ) < 0 ) then
                       RastreoPesos( Format( '(=)      %s Calculado : ', [ FLowerSUBE ] ), nImpuesto - nSube )
                    else
                        RastreoPesos( '(=)        ISR Calculado : ', nImpuesto - nSube );
}
               end;
          end;
     end;
     // Impuesto NETO
     Result := Redondea( nImpuesto - nSube );
end;

type
TZetaAjustaISR = class( TZetaCALC_IMP_ANUAL )
  private
    rDias,
    rGravado, rACGravado, rTotGravado,
    rMensual, rACMensual, rTotMensual,
    rRetenido, rSUBE,
    rIsptIndividual, rACIsptIndividual: TPesos;
    function SetAnual( const sCampo: string ): string;
    function AjustaEmpleado : Boolean;
    { ***** Funciones hermanas de AjustaISPT, CalcISPT y CalcImpuesto ***** }
    function CalculaAjusteISPT: TPesos;
    function AjustaISR: TPesos;
    procedure PreparaEmpleadosConAjuste;
    procedure AgregaAcumulado( const iConcepto : Integer; const sAlias : String; const lAgregaACAnualSiempre: Boolean = FALSE );
    procedure AgregaAcAnual( const iConcepto : Integer; const sAlias : String);
    procedure AgregaCampoReq( const sCampo : string; const iConceptoAcumulado : Integer );
    procedure SetOpciones;
    procedure SetParamDias;
    procedure SetParamTablas;
    procedure SetParamFecha;
    procedure SetAcumuladosISPT;
    procedure SetAcumuladosISR;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
  end;

constructor TZetaAjustaISR.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     PreparaEmpleadosConAjuste;
end;

procedure TZetaAjustaISR.PreparaEmpleadosConAjuste;
begin
     if ( oZetaProvider.EmpleadosConAjuste = nil ) then
        oZetaProvider.EmpleadosConAjuste := TEmpleadosConAjuste.Create(ZetaEvaluador.oZetaCreator);
end;

function TZetaAjustaISR.AjustaEmpleado: Boolean;
begin
     Result := FALSE;
     if ( not ZetaEvaluador.CalculandoNomina ) or ( oZetaProvider.DatosPeriodo.EsUltimaNomina ) then
        Result := TEmpleadosConAjuste(oZetaProvider.EmpleadosConAjuste).AplicaAjuste(ZetaEvaluador.GetEmpleado);
end;

procedure TZetaAjustaISR.AgregaAcumulado( const iConcepto : Integer; const sAlias : String; const lAgregaACAnualSiempre: Boolean );
var
    sScript : STring;
begin
     with oZetaProvider.DatosPeriodo do
{$ifdef INTERBASE}
        sScript := Format( '( select RESULTADO from sp_as( %d, %d, %d, %d, NOMINA.CB_CODIGO )) %s',
	                        [ iConcepto, FMesFinal, FMesFinal, Year, sAlias ] );
{$else}
        sScript := Format( 'DBO.SP_AS( %d, %d, %d, %d, NOMINA.CB_CODIGO ) %s',
	                        [ iConcepto, FMesFinal, FMesFinal, Year, sAlias ] );
{$endif}
     AgregaRequerido( sScript, enNomina );
     if oZetaProvider.DatosPeriodo.EsUltimaNomina or lAgregaACAnualSiempre then
        AgregaAcAnual( iConcepto, sAlias + K_ALIAS_ANUAL );
end;

procedure TZetaAjustaISR.AgregaAcAnual( const iConcepto : Integer; const sAlias : String);
var
    sScript : STring;
begin
     with oZetaProvider.DatosPeriodo do
{$ifdef INTERBASE}
        sScript := Format( '( select RESULTADO from sp_as( %d, %d, %d, %d, NOMINA.CB_CODIGO )) %s',
	                        [ iConcepto, FMesInicial, FMesFinal, Year, sAlias ] );
{$else}
        sScript := Format( 'DBO.SP_AS( %d, %d, %d, %d, NOMINA.CB_CODIGO ) %s',
	                        [ iConcepto, FMesInicial, FMesFinal, Year, sAlias ] );
{$endif}
     AgregaRequerido( sScript, enNomina );
end;

procedure TZetaAjustaISR.AgregaCampoReq( const sCampo : string; const iConceptoAcumulado : Integer );
begin
     AgregaRequerido( 'NOMINA.'+ sCampo, enNomina );
     if oZetaProvider.DatosPeriodo.EsUltimaNomina then
        AgregaAcAnual( iConceptoAcumulado, sCampo + K_ALIAS_ANUAL );
end;

procedure TZetaAjustaISR.GetRequeridos( var TipoRegresa : TQREvResultType );
var
   iConcepto: integer;
begin
     if NOT ZetaEvaluador.CalculandoNomina then
        ErrorFuncion( 'Solamente se Puede Utilizar en C�lculo de N�mina', 'Funci�n: ' + Expresion );

     TipoRegresa := resDouble;
     with ZetaEvaluador.oZetaCreator do
     begin
          PreparaTablasISPT;
     end;
     with oZetaProvider do
     begin
          GetDatosActivos;
          GetDatosPeriodo;
          InitGlobales;
     end;

     with oZetaProvider.DatosPeriodo do
     begin
          { **** Parametro -- K_MESFINAL }
          if ZetaEvaluador.CalculandoNomina then
          begin
               //En el calculo de la nomina no aplica el parametro MES, porque no se pueden calcular unos empleados con un mes y otros con otro mes.
               FMesFinal := Mes;
               if EsUltimaNomina then
                  FMesInicial:= 1
               else
                   FMesInicial:= FMesFinal;
          end
          else
          begin
               FMesFinal:= DefaultInteger( K_MESFINAL, Mes );
               if EsUltimaNomina then
                  FMesInicial:= 1
               else
                   FMesInicial:= FMesFinal;
          end;

          if ParamVacio( K_DIAS_TRABAJADOS ) then
          begin
               AgregaParam( 'DIAS' );
               AgregaParam( 'DIAS_VACA' );
               AgregaParam( 'DIAS_AJUST' );
          end;
          if  ( ( ParamVacio( K_AJUSTA_CADA_PERIODO ) or ( ParamInteger( K_AJUSTA_CADA_PERIODO ) = 0 ) ) and
                ( PosMes > 1 ) ) or
                ( EsUltimaNomina ) then
          begin
               AgregaAcumulado( 1107, 'AC_DIAS' );
               AgregaAcumulado( 1114, 'AC_DIAS_VACA' );
               AgregaAcumulado( 1116, 'AC_DIAS_AJUST' );
          end;
     end;

     AgregaAcumulado( 1000, 'AT0' );
     AgregaAcumulado( 1003, 'ATX');
     AgregaAcumulado( 1005, 'ATM');
     AgregaAcumulado( 1006, 'ATY');
     AgregaAcumulado( 1007, 'AT2');
     AgregaAcumulado( 1008, 'AT3');
     AgregaAcumulado( 1009, 'ATZ');
     AgregaAcumulado( 1011, 'PREV_GR');
     AgregaAcumulado( oZetaProvider.GetGlobalInteger( K_GLOBAL_ISPT ), 'AISPT');
     AgregaAcumulado( oZetaProvider.GetGlobalInteger( K_GLOBAL_CREDITO) , 'ACREDITO');

     if oZetaProvider.DatosPeriodo.EsUltimaNomina and
        ParamVacio( K_SUBE_APLICADO_ACUMULADO ) then
     begin
          iConcepto := oZetaProvider.GetGlobalInteger( K_GLOBAL_DEF_NOM_CREDITO_AP_CONCEPTO );
          if iConcepto = 0 then iConcepto := K_CONCEPTO_SUBE_APLICADO;
          AgregaAcAnual( iConcepto, 'SUBE_APLICADO')
     end;

     if not ZetaEvaluador.CalculandoNomina then
     begin
          if ParamVacio( K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO ) then
          begin
              AgregaCampoReq( 'NO_PERCEPC', 1000 );
              AgregaCampoReq( 'NO_PER_MEN', 1005 );
              AgregaCampoReq( 'NO_X_ISPT',  1003 );
              AgregaCampoReq( 'NO_PER_CAL', 1007 );
              AgregaCampoReq( 'NO_X_CAL',   1009 );
              AgregaCampoReq( 'NO_PREV_GR', 1011 );
          end;
          if ParamVacio( K_PERCEPCIONES_GRAVABLES_ACUMULADAS ) then
          begin
              AgregaCampoReq( 'NO_PER_MEN', 1005 );
              AgregaCampoReq( 'NO_X_MENS', 1006 );
          end;
          if ParamVacio( K_IMPUESTO_INDIVIDUAL ) then
              AgregaCampoReq( 'NO_IMP_CAL', 1008 );
     end;
end;

function TZetaAjustaISR.SetAnual( const sCampo: string ): string;
begin
     Result := sCampo + K_ALIAS_ANUAL;
end;

procedure TZetaAjustaISR.SetOpciones;
begin
     // eTipoAjuste = (etaEntregaCredito, etaAjustaSiempre, etaSuaviza, etaTopaSUBE );
     Opciones := [];

     { **** Parametro -- K_AJUSTA_CADA_PERIODO }
     if ParamVacio( K_AJUSTA_CADA_PERIODO ) or
        ( ParamInteger( K_AJUSTA_CADA_PERIODO ) = 0 ) then
        Opciones := Opciones + [etaAjustaSiempre];

     { **** Parametro -- K_PAGA_SUBE_A_FIN_MES }
     if NOT ParamVacio( K_PAGA_SUBE_A_FIN_MES ) and
        ( ParamInteger( K_PAGA_SUBE_A_FIN_MES ) = 1 ) then
        Opciones := Opciones + [etaEntregaCredito];

     { **** Parametro -- K_TOPA_SUBE_A_FIN_MES }
     if ( oZetaProvider.DatosPeriodo.Year >= K_REFORMA_FISCAL_2008 ) and
        NOT ParamVacio( K_TOPA_SUBE_A_FIN_MES  ) and
        ( ParamInteger( K_TOPA_SUBE_A_FIN_MES ) = 1 ) then
        Opciones := Opciones + [etaTopaSUBE];

     { **** Parametro -- K_SUAVIZA_PERCEPCIONES_MENSUALES }
     if NOT ParamVacio( K_SUAVIZA_PERCEPCIONES_MENSUALES ) and
          ( ParamInteger( K_SUAVIZA_PERCEPCIONES_MENSUALES ) = 1 ) then
          Opciones := Opciones + [etaSuaviza];
end;

procedure TZetaAjustaISR.SetParamDias;
begin
     with DataSetEvaluador, oZetaProvider do
     begin
          { **** Parametro -- K_DIAS_TRABAJADOS }
          if ParamVacio( K_DIAS_TRABAJADOS ) then
             rDias := FieldByName( 'DIAS' ).AsFloat + ( FieldByName( 'DIAS_VACA' ).AsFloat * 7/6 ) + FieldByName( 'DIAS_AJUST' ).AsFloat
          else
              rDias := ParamFloat( K_DIAS_TRABAJADOS );
          { **** Parametro -- K_DIAS_TRABAJADOS_ACUMULADOS }
          //Se suman los dias de la nomina actual + suma de los dias acumulados.
          if DatosPeriodo.EsUltimaNomina or ( ( etaAjustaSiempre in Opciones ) and (DatosPeriodo.PosMes > 1 ) ) then
          begin
               if ParamVacio( K_DIAS_TRABAJADOS_ACUMULADOS ) then
                  rACDias := rDias + FieldByName( 'AC_DIAS'  ).AsFloat + ( FieldByName( 'AC_DIAS_VACA' ).AsFloat * 7/6 ) +
                             FieldByName( 'AC_DIAS_AJUST' ).AsFloat
               else
                   rACDias := ParamFloat( K_DIAS_TRABAJADOS_ACUMULADOS );
          end
          else
          begin
               rACDias := 0;
          end;
     end;
     { **** Parametro -- K_FACTOR_SUBE_DIAS_MENSUALES }
     rFactorSUBE := GetFactorSube( DefaultTasa( K_FACTOR_SUBE_DIAS_MENSUALES, K_FACTOR_MENSUAL ), oZetaProvider.DatosPeriodo.Year );
end;

procedure TZetaAjustaISR.SetParamTablas;
begin
     { **** Parametro -- K_T1 }
     nT1 := DefaultInteger( K_T1, 1 );
     { **** Parametro -- K_T3 }
     nT3 := DefaultInteger( K_T3, 3 );
end;

procedure TZetaAjustaISR.SetParamFecha;
begin
     with DataSetEvaluador, oZetaProvider do
     begin
          { **** Parametro -- K_FECHA }
          if ParamVacio( K_FECHA ) then
          begin
               if ( ZetaEvaluador.CalculandoNomina ) then
                  dFecha := DatosPeriodo.Inicio      // Al Calcular N�mina DatosPeriodo Ya Tiene Informaci�n
               else
                  dFecha := FechaDefault;
          end
          else
          begin
              dFecha := ParamDate( K_FECHA );
          end;
     end;
end;

procedure TZetaAjustaISR.SetAcumuladosISPT;
begin
     with DataSetEvaluador, oZetaProvider do
     begin
          { **** Parametro -- K_PERCEPCIONES_TIPO_MENSUAL_DEL_PERIODO_ACTIVO }
          if ParamVacio( K_PERCEPCIONES_TIPO_MENSUAL_DEL_PERIODO_ACTIVO ) then
             rMensual := TotalNomina( ttnMensuales ) - TotalNomina( ttnExentoMensual )
          else
              rMensual := ParamFloat( K_PERCEPCIONES_TIPO_MENSUAL_DEL_PERIODO_ACTIVO );
          { **** Parametro -- K_PERCEPCIONES_TIPO_MENSUAL_ACUMULADAS }
          rACMensual := DefaultFloat( K_PERCEPCIONES_TIPO_MENSUAL_ACUMULADAS, FieldByName( 'ATM' ).AsFloat -  FieldByName( 'ATY' ).AsFloat );
          { **** Parametro -- K_IMPUESTO_RETENIDO_ACUMULADO }
          rRetenido := DefaultFloat( K_IMPUESTO_RETENIDO_ACUMULADO, FieldByName( 'AISPT' ).AsFloat );
          { **** Parametro -- K_SUBE_RETENIDO_ACUMULADO }
          rSUBE := DefaultFloat( K_SUBE_RETENIDO_ACUMULADO, FieldByName( 'ACREDITO' ).AsFloat );
          { **** Parametro -- K_IMPUESTO_INDIVIDUAL }
          if ParamVacio( K_IMPUESTO_INDIVIDUAL ) then
             rIsptIndividual := TotalNomina( ttnImpCal )
          else
              rIsptIndividual := ParamFloat( K_IMPUESTO_INDIVIDUAL );
          { **** Parametro -- K_IMPUESTO_INDIVIDUAL_ACUMULADO }
          rACIsptIndividual := DefaultFloat( K_IMPUESTO_INDIVIDUAL_ACUMULADO, FieldByName( 'AT3' ).AsFloat );
          { **** Parametro -- K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO }
          if ParamVacio( K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO ) then
             rGravado := ( TotalNomina( ttnPercepciones ) -
                           TotalNomina( ttnMensuales ) -
                           TotalNomina( ttnExentas  ) -
                           TotalNomina( ttnPerCal ) ) +
                           TotalNomina( ttnGravadoPrev )
          else
              rGravado := ParamFloat( K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO );
          { **** Parametro -- K_PERCEPCIONES_GRAVABLES_ACUMULADAS }
          if ParamVacio( K_PERCEPCIONES_GRAVABLES_ACUMULADAS ) then
             rACGravado := FieldByName( 'AT0' ).AsFloat -
                           FieldByName( 'ATX' ).AsFloat -
                           FieldByName( 'ATM' ).AsFloat -
                           FieldByName( 'ATZ' ).AsFloat +
                           FieldByName( 'PREV_GR' ).AsFloat
          else
              rACGravado := ParamFloat( K_PERCEPCIONES_GRAVABLES_ACUMULADAS );
     end;
end;

procedure TZetaAjustaISR.SetAcumuladosISR;
begin
     with DataSetEvaluador, oZetaProvider do
     begin
          { **** Parametro -- K_PERCEPCIONES_TIPO_MENSUAL_ACUMULADAS }
          rACMensual := DefaultFloat( K_PERCEPCIONES_TIPO_MENSUAL_ACUMULADAS, FieldByName( SetAnual( 'ATM' ) ).AsFloat -  FieldByName( 'ATY' ).AsFloat );
          { **** Parametro -- K_IMPUESTO_RETENIDO_ACUMULADO }
          rRetenido := DefaultFloat( K_IMPUESTO_RETENIDO_ACUMULADO, FieldByName( SetAnual( 'AISPT' ) ).AsFloat );
          { **** Parametro -- K_SUBE_RETENIDO_ACUMULADO }
          rSUBE := DefaultFloat( K_SUBE_RETENIDO_ACUMULADO, FieldByName( SetAnual( 'ACREDITO' ) ).AsFloat );
          { **** Parametro -- K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO }
          if ParamVacio( K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO ) then
             rGravado := ( TotalNomina( ttnPercepciones ) -
                           TotalNomina( ttnMensuales ) -
                           TotalNomina( ttnExentas  ) -
                           TotalNomina( ttnExentoCal ) ) +
                           TotalNomina( ttnGravadoPrev )
          else
              rGravado := ParamFloat( K_PERCEPCIONES_GRAVABLES_DEL_PERIODO_ACTIVO );
          { **** Parametro -- K_PERCEPCIONES_GRAVABLES_ACUMULADAS }
          if ParamVacio( K_PERCEPCIONES_GRAVABLES_ACUMULADAS ) then
             rACGravado := FieldByName( SetAnual( 'AT0' ) ).AsFloat -
                           FieldByName( SetAnual( 'ATX' ) ).AsFloat -
                           FieldByName( SetAnual( 'ATM' ) ).AsFloat -
                           FieldByName( SetAnual( 'AT2' ) ).AsFloat +
                           FieldByName( SetAnual( 'PREV_GR' ) ).AsFloat
          else
              rACGravado := ParamFloat( K_PERCEPCIONES_GRAVABLES_ACUMULADAS );
          { **** Parametro -- K_SUBE_APLICADO_ACUMULADO }
          if ParamVacio( K_SUBE_APLICADO_ACUMULADO ) then
             rACSubeAplicado := FieldByName( 'SUBE_APLICADO' ).AsFloat
          else
              rACSubeAplicado := ParamFloat( K_SUBE_APLICADO_ACUMULADO );
          //Monto Gravado de este mes.
          rGravadoMensualSUBE := ( TotalNomina( ttnPercepciones ) -
                                   TotalNomina( ttnExentas  ) -
                                   TotalNomina( ttnPerCal )) +
                                   TotalNomina( ttnGravadoPrev )  +
                                   FieldByName( 'AT0' ).AsFloat -           //Total de Perccepciones A(1000)
                                   FieldByName( 'ATX' ).AsFloat -           //Total de Perccepciones exentas A(1003)
                                   FieldByName( 'AT2' ).AsFloat +           //Total de Perccepciones c/ispt Individual A(1007)
                                   FieldByName( 'PREV_GR' ).AsFloat;        //Total de Prestaciones con parte gravada A(1011)
          rTotGravado := rGravado + rACGravado;
          rTotMensual := rMensual + rACMensual;
          rTotPercepcionesGravadas := rTotGravado + rTotMensual; //Incluye todas las percepciones menos los exentos.
     end;
end;

function TZetaAjustaISR.Calculate: TQREvResult;
begin
     Result.Kind  := resDouble;
     SetOpciones;
     SetParamDias;
     SetParamTablas;
     SetParamFecha;
     // Primero se aplica impuesto del periodo - Procedimiento anterior
     SetAcumuladosISPT;
     Result.dblResult := CalculaAjusteISPT;
     // Si aplica ajusta impuesto anual
     if AjustaEmpleado then
     begin
          FLowerSUBE := aCreditoSalarioLower[TRUE];
          SetAcumuladosISR;
          Result.dblResult := AjustaISR;
     end;
end;

function TZetaAjustaISR.CalculaAjusteISPT: TPesos;
begin
     Result := AjustaISPT( ZetaEvaluador,
                           Opciones,
                           0, //Subsidio, ya no se utiliza.
                           DefaultTasa( K_FACTOR_DIAS_MENSUALES, K_FACTOR_MENSUAL ), { **** Parametro -- K_FACTOR_DIAS_MENSUALES }
                           rFactorSUBE,
                           rDias,
                           0, //Salario minimo, ya no se utiliza.
                           nT1,
                           0,
                           nT3,
                           rGravado,
                           rMensual,
                           rACGravado,
                           rACMensual,
                           rRetenido,
                           rSUBE,
                           rACDias,
                           rIsptIndividual,
                           rACIsptIndividual,
                           dFecha );
end;

function TZetaAjustaISR.AjustaISR : TPesos;
var
   nImpuesto, nMesISPT, nISPTNeto, nSUBE: TPesos;
   sMesFinal: string;
begin
     with ZetaEvaluador do
     begin
          if CalculandoNomina then
          begin
               TCalcNomina(CalcNomina).RegistraISPT( 0 );
               nSUBE := TCalcNomina(CalcNomina).nRegistraCredito;   // SUBE calculado en AJUSTA_ISPT
          end
          else
              nSUBE := 0;

          if ( Rastreando or RastreandoNomina ) then
          begin
               with Rastreador do
               begin
                    RastreoEspacio;
                    with oZetaProvider.DatosPeriodo do
                    begin
                         if Mes = 1 then
                            RastreoHeader( 'CALCULO DE ISR AJUSTADO DE ENERO' )
                         else
                         begin
                              sMesFinal := ObtieneElemento( lfMeses, Mes - GetOffSet( lfMeses ) );
                              RastreoHeader( Format( '      CALCULO DE ISR AJUSTADO DE ENERO a %s', [ UpperCase(sMesFinal) ] ) );
                         end;
                         RastreoMsg( '*** ES LA ULTIMA NOMINA DEL MES ***' );
                         RastreoTextoLibre( Format( 'Los Montos Acumulados con (*) abarcan los periodos 1 al %d y los especiales', [Numero-1] ) ); //falta agregar la nomina especial

                    end;
                    RastreoEspacio;
                    RastreoPesos( '                Percepciones del Periodo : ', rGravado );
                    RastreoPesos( '(+)             *Percepciones Acumuladas : ', rACGravado );
                    RastreoPesos( '(+)   Percepciones Mensuales del Periodo : ', rMensual );
                    RastreoPesos( '(+)   *Percepciones Mensuales Acumuladas : ', rACMensual );
                    RastreoPesos( '(=)              TOTAL Acumulado Gravado : ', rTotPercepcionesGravadas );
                    RastreoEspacio;
               end;
          end;
     end;

     nImpuesto := CalcImpuestoISR;
     nMesISPT := ZetaCommonTools.rMax( 0, nImpuesto );    // Topar ISR a Cargo a 0 � la diferencia entre Impuesto y SUBE
     nISPTNeto := Redondea( nMesISPT - rRetenido );

     with ZetaEvaluador do
     begin
          if ( Rastreando or RastreandoNomina ) then
          begin
               with Rastreador do
               begin
                    if ( nImpuesto < 0 ) then
                    begin
                         RastreoMsg( 'ARTICULO 116, FRACCION III:' );
                    end;
                    RastreoPesos( '(=)                     Impuesto a Cargo : ', nMesISPT );
                    RastreoPesos( '(-)              *Acumulado ISR Retenido : ', rRetenido );

                    if ( nISPTNeto > 0 ) then
                       RastreoPesos( '(=)                          ISR A Cargo : ', nISPTNeto )
                    else
                       RastreoPesos( '(=)                          ISR A Favor : ', nISPTNeto *-1 );
                    RastreoHeader( '' );
                    RastreoPesos( '                                     ISR : ', nISPTNeto );
                    RastreoPesos( '(+)                      SUBE a entregar : ', nSUBE );
               end;
          end;
          if CalculandoNomina then
          begin
{
               if ( nISPTNeto = 0 ) then           // Si no hay impuesto no se refleja SUBE a menos que se asigne como impuesto
                  nISPTNeto := nSUBE;
}
               TCalcNomina(CalcNomina).RegistraISPT( nISPTNeto );
          end;
     end;
     Result := nISPTNeto + nSUBE;
end;

{********************** FUNCIONES PUBLICAS ***************************}

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          //FUNCIONES PARA CALCULOS DE ISPT
          RegisterFunction( TZetaTabla,        'TABLA', 'Info Tablas Num�ricas N�mina' );
          RegisterFunction( TZetaCalcImp,      'CALC_IMP', 'C�lculo de Impuesto Ispt' );
          RegisterFunction( TZetaCalcISPT,     'ISPT', 'C�lculo de impuesto Ispt' );
          RegisterFunction( TZetaAjustaISPT,   'AJUSTA_ISP', 'Impuesto ISPT' );
          RegisterFunction( TZetaCalcSub,      'CALC_SUB', 'C�lculo de Subsidio' );
          RegisterFunction( TZetaSubsidio,     'SUBSIDIO', 'C�lculo de Subsidio por N�mina' );
          RegisterFunction( TZetaCALC_CREDITO, 'CALC_CREDI', 'Cr�dito/SUBE al Salario Sin Netear' );
          RegisterFunction( TZetaCALC_ISPT,    'CALC_ISPT', 'C�lculo de Impuesto Sin Netear' );
          RegisterFunction( TZetaArt86,        'ART86', 'Art�culo 86' );
          RegisterFunction( TZetaArt86I,       'ART86_I', 'Art�culo 86 Individual' );
          RegisterFunction( TZetaArt80Separa,  'ART80_SEPA', 'Articulo 80 Separaci�n' );
          RegisterFunction( TZetaCreditoAplicado, 'CREDITO_AP','Cr�dito/SUBE Aplicado' );
          RegisterFunction( TZetaSUBEAplicado, 'SUBE_AP','SUBE Aplicado' );
          //RegisterFunction( TZetaArt80Separa, 'ART79_I');
          RegisterFunction( TZetaAjustaISR,      'AJUSTA_ISR', 'Ajusta ISR' );
          RegisterFunction( TZetaCALC_IMP_ANUAL, 'CALC_ISR', 'C�lculo de Impuesto Anual' );
     end;
end;

end.

