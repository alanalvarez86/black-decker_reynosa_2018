unit ZFuncsReporteNomina;

interface
uses Controls, SysUtils, ZetaQRExpr;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
procedure RegistraFuncionesBalanza( FunctionLibrary: TQRFunctionLibrary );

implementation

uses ZFuncsTress,
     ZEvaluador,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonTools,
     DZetaServerProvider;

const
     //Constantes para la funcion NOMM, que se utiliza en el recibo de mensual
      K_PRIMER_DIA_ORDINARIO = 1;
      K_ULTIMO_DIA_ORDINARIO = 2;
      K_PRIMER_PERIODO_ORDINARIO = 3;
      K_ULTIMO_PERIODO_ORDINARIO = 4;
      K_PRIMER_TIPO_PERIODO_ORDINARIO = 5;
      K_ULTIMO_TIPO_PERIODO_ORDINARIO = 6;
      K_TOTAL_ORDINARIOS = 7;
      K_TOTAL_ESPECIALES = 8;
      K_TOTAL_PERIODOS = 9;
      K_PRIMER_MES = 10;
      K_ULTIMO_MES = 11;

{ ************* TZetaReferenciaRecibo ************* }

type
  TZetaNOMM = class( TZetaRegresa )
  private
    FDatasetNOMM: TZetaCursor;
    FDatasetNOMMTipo: TZetaCursor;
    FTotalesNOMM: TZetaCursor;
    FKardexNOMM: TZetaCursor;
  public
    function Calculate: TQREvResult; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    constructor Create(oEvaluator:TQrEvaluator); override;
    destructor Destroy; override;
  end;

constructor TZetaNOMM.Create(oEvaluator:TQrEvaluator);
 {$ifdef DOS_CAPAS}
 const K_TIPONOMINA= 'select CB_NOMINA from SP_FECHA_KARDEX( :Fecha, :Empleado )';
 {$else}
 const K_TIPONOMINA= 'select dbo.SP_KARDEX_CB_NOMINA( :Fecha, :Empleado ) CB_NOMINA';
 {$endif}
begin
     inherited Create(oEvaluator);
     with oZetaProvider do
     begin
          with DatosNomm do
          begin
               if StrLleno( ScriptNOMM ) then
               begin
                    FDataSetNOMM :=     CreateQuery( ScriptNOMM );
                    FDataSetNOMMTipo := CreateQuery( ScriptNOMMTipo );
                    FTotalesNOMM :=     CreateQuery( TotalesNOMM );
               end;
          end;
          FKardexNOMM := CreateQuery( K_TIPONOMINA );
     end;
end;

destructor TZetaNOMM.Destroy;
begin
     FreeAndNil( FDataSetNOMM );
     FreeAndNil( FDataSetNOMMTipo );
     FreeAndNil( FTotalesNOMM );
     FreeAndNil( FKardexNOMM );
     inherited Destroy;
end;

procedure TZetaNOMM.GetRequeridos( var TipoRegresa : TQREvResultType );
 var iTipo: integer;
begin
     inherited;
     ParametrosFijos( 1 );

     iTipo := ParamInteger( 0 );
     if (iTipo < K_PRIMER_DIA_ORDINARIO) or (iTipo > K_ULTIMO_MES) then iTipo := K_PRIMER_DIA_ORDINARIO;

     if iTipo in [K_PRIMER_DIA_ORDINARIO,K_ULTIMO_DIA_ORDINARIO] then
     begin
          TipoRegresa := resDouble;
          esFecha := TRUE;
     end
     else
          TipoRegresa := resInt;

     with ZetaEvaluador do
          AgregaRequerido( oZetaCreator.dmEntidadesTress.NombreEntidad( Entidad ) + '.CB_CODIGO', Entidad );
end;

function TZetaNOMM.Calculate: TQREvResult;
var
   iTipo, iEmpleado :integer;
   iCount: integer;

    procedure AbreDataset( oDataset: TZetaCursor; const eTipo: integer; const lTipo: Boolean= FALSE );
    begin
         with oDataset do
         begin
              if lTipo then
                 Close;

              if NOT Active OR (FieldByName('CB_CODIGO').AsInteger <> iEmpleado) then
              begin
                   Close;
                   if lTipo then
                      oZetaProvider.ParamAsInteger( oDataset, 'Tipo', eTipo );
                   oZetaProvider.ParamAsInteger( oDataset, 'Empleado', iEmpleado );
                   Open;
                   //First;
              end;
         end;
    end;

begin
     iTipo := ParamInteger( 0 );
     if (iTipo < K_PRIMER_DIA_ORDINARIO) or (iTipo > K_ULTIMO_MES) then iTipo := K_PRIMER_DIA_ORDINARIO;

     with Result do
     begin
          if iTipo in [ K_PRIMER_DIA_ORDINARIO, K_ULTIMO_DIA_ORDINARIO] then
          begin
               Kind := resDouble;
               esFecha := TRUE;
          end
          else
          begin
               Kind := resInt;
          end;

          if StrLleno( oZetaProvider.DatosNomm.ScriptNOMM ) then
          begin
               iEmpleado := ZetaEvaluador.GetEmpleado;
               if iTipo in [ K_TOTAL_ORDINARIOS, K_TOTAL_ESPECIALES, K_TOTAL_PERIODOS, K_PRIMER_MES, K_ULTIMO_MES ] then
               begin
                    AbreDataset( FTotalesNOMM, 0 );
                    with FTotalesNOMM do
                         case iTipo of
                              K_TOTAL_ORDINARIOS             : intResult := FieldByName('Total_Ordinarios').AsInteger;
                              K_TOTAL_ESPECIALES             : intResult := FieldByName('Total_Especiales').AsInteger;
                              K_TOTAL_PERIODOS               : intResult := FieldByName('Total_Periodos').AsInteger;
                              K_PRIMER_MES                   : intResult := FieldByName('Primer_mes').AsInteger;
                              K_ULTIMO_MES                   : intResult := FieldByName('Ultimo_mes').AsInteger;
                         end;
               end
               else
               begin
                    if ( iTipo in [ K_ULTIMO_DIA_ORDINARIO, K_ULTIMO_PERIODO_ORDINARIO, K_ULTIMO_TIPO_PERIODO_ORDINARIO ] ) then
                    begin
                         AbreDataset( FDatasetNOMM,0 );

                         if  ( iTipo in [ K_ULTIMO_DIA_ORDINARIO, K_ULTIMO_PERIODO_ORDINARIO ] ) then
                         begin
                              AbreDataset( FDatasetNOMMTipo, FDatasetNOMM.FieldByName('ULTIMO_TIPO').AsInteger, TRUE );
                              iCount := 1;
                              while (iCount < FDatasetNOMMTipo.RecordCount) and  not FDatasetNOMMTipo.eof do
                              begin
                                   FDatasetNOMMTipo.Next;
                                   Inc(iCount);
                              end;
                         end;

                         case iTipo of
                              K_ULTIMO_DIA_ORDINARIO         : dblResult := FDatasetNOMMTipo.FieldByName('PE_FEC_FIN').AsDateTime;
                              K_ULTIMO_PERIODO_ORDINARIO     : intResult := FDatasetNOMMTipo.FieldByName('PE_NUMERO').AsInteger;
                              K_ULTIMO_TIPO_PERIODO_ORDINARIO: intResult := FDatasetNOMM.FieldByName('ULTIMO_TIPO').AsInteger;
                         end;
                    end
                    else
                    begin
                         AbreDataset( FDatasetNOMM,0 );

                         FKardexNOMM.Close;
                         oZetaProvider.ParamAsDAte( FKardexNOMM, 'Fecha', FDatasetNOMM.FieldByName('PE_FEC_INI').AsDateTime );
                         oZetaProvider.ParamAsInteger( FKardexNOMM, 'Empleado', iEmpleado );
                         FKardexNOMM.Open;

                         if  ( iTipo in [ K_PRIMER_DIA_ORDINARIO, K_PRIMER_PERIODO_ORDINARIO] ) then
                         begin
                              AbreDataset( FDatasetNOMMTipo, FKardexNOMM.Fields[0].AsInteger, TRUE );
                              //FDatasetNOMMTipo.First;
                         end;

                         case iTipo of
                              K_PRIMER_PERIODO_ORDINARIO     : intResult := FDatasetNOMMTipo.FieldByName('PE_NUMERO').AsInteger;
                              K_PRIMER_TIPO_PERIODO_ORDINARIO: intResult := FKardexNOMM.Fields[0].AsInteger;
                              else                             dblResult := FDatasetNOMMTipo.FieldByName('PE_FEC_INI').AsDateTime;
                         end;
                    end;
               end;
          end
          else
          begin
               with oZetaProvider.DatosPeriodo do
               case iTipo of
                    K_PRIMER_DIA_ORDINARIO          :     dblResult := Inicio;
                    K_ULTIMO_DIA_ORDINARIO          :     dblResult := Fin;
                    K_PRIMER_PERIODO_ORDINARIO,
                    K_ULTIMO_PERIODO_ORDINARIO      :     intResult := Numero;
                    K_PRIMER_TIPO_PERIODO_ORDINARIO,
                    K_ULTIMO_TIPO_PERIODO_ORDINARIO :     intResult := Ord(Tipo);
                    K_TOTAL_ORDINARIOS,
                    K_TOTAL_ESPECIALES,
                    K_TOTAL_PERIODOS                :     intResult := 1;
                    K_PRIMER_MES,
                    K_ULTIMO_MES                    :     intResult := Mes;    
               end;
          end;
     end;
end;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaCONCEPTO, 'CC','Concepto Calculado'  );
     end;
end;

procedure RegistraFuncionesBalanza( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( TZetaNOMM, 'NOMM','Info Periodo del Recibo Mensual'  );
     end;
end;

end.
