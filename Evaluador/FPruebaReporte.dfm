object FormReporte: TFormReporte
  Left = 189
  Top = 111
  Width = 724
  Height = 603
  Caption = 'FormReporte'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 325
    Width = 716
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 185
    Width = 716
    Height = 140
    Align = alTop
    DataSource = dsReporte
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 550
    Width = 716
    Height = 19
    Panels = <>
    SimplePanel = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 716
    Height = 185
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 36
      Top = 156
      Width = 43
      Height = 13
      Caption = 'Order by:'
    end
    object Label2: TLabel
      Left = 244
      Top = 156
      Width = 25
      Height = 13
      Caption = 'Filtro:'
    end
    object memoColumnas: TMemo
      Left = 20
      Top = 0
      Width = 457
      Height = 145
      Lines.Strings = (
        'CB_PUESTO'
        'CB_APE_PAT + '#39' '#39' + CB_APE_MAT + '#39', '#39' + CB_NOMBRES'
        'CB_SALARIO'
        'IF( cb_salario > 40, if( cb_salario > 50, 200, 200 ), 100 )'
        'CB_FEC_ING'
        'RESULT(3)/1'
        'CB_CHECA = '#39'S'#39
        
          '@select im_blob from IMAGEN where cb_codigo=colabora.cb_codigo a' +
          'nd im_tipo='#39'FOTO'#39)
      TabOrder = 0
    end
    object CreaReporte: TButton
      Left = 492
      Top = 44
      Width = 123
      Height = 41
      Caption = 'CreaReporte'
      TabOrder = 1
      OnClick = CreaReporteClick
    end
    object editOrden: TEdit
      Left = 92
      Top = 152
      Width = 133
      Height = 21
      TabOrder = 2
      Text = 'CB_PUESTO'
    end
    object editFiltro: TEdit
      Left = 272
      Top = 152
      Width = 285
      Height = 21
      TabOrder = 3
    end
    object SoloTotales: TCheckBox
      Left = 492
      Top = 88
      Width = 81
      Height = 17
      Caption = 'SoloTotales'
      TabOrder = 4
    end
    object Grupos: TEdit
      Left = 492
      Top = 104
      Width = 37
      Height = 21
      TabOrder = 5
      Text = '1'
    end
    object Reabre: TButton
      Left = 580
      Top = 152
      Width = 75
      Height = 25
      Caption = 'Reabre'
      TabOrder = 6
      OnClick = ReabreClick
    end
    object ComboEntidad: TComboBox
      Left = 492
      Top = 12
      Width = 177
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 7
    end
    object Button1: TButton
      Left = 620
      Top = 44
      Width = 75
      Height = 25
      Caption = 'Acumulados'
      TabOrder = 8
      OnClick = Button1Click
    end
  end
  object Memo1: TMemo
    Left = 0
    Top = 328
    Width = 716
    Height = 222
    Align = alClient
    Lines.Strings = (
      'Memo1')
    TabOrder = 3
  end
  object DBMemo1: TDBMemo
    Left = 580
    Top = 96
    Width = 129
    Height = 53
    DataSource = dsReporte
    TabOrder = 4
  end
  object dsReporte: TDataSource
    DataSet = cdsReporte
    Left = 600
    Top = 176
  end
  object cdsReporte: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 644
    Top = 180
  end
end
