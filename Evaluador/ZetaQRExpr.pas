{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: QuickReport 2.0 for Delphi 1.0/2.0/3.0                  ::
  ::                                                         ::
  :: QRPRNTR - QRPrinter and other low level classes         ::
  ::                                                         ::
  :: Copyright (c) 1997 QuSoft AS                            ::
  :: All Rights Reserved                                     ::
  ::                                                         ::
  :: web: http://www.qusoft.no    mail: support@qusoft.no    ::
  ::                              fax: +47 22 41 74 91       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

unit ZetaQRExpr;

{$R-}
{$T-} { We don't need (nor want) this type checking! }
{$B-} { QuickReport source assumes boolean expression short-circuit }

interface

uses
  SysUtils, Classes, DB, QR2Const, ZetaCommonClasses, ZetaCommonTools, DZetaServerProvider,
  ZetaCommonLists;

type
  EErrorEvaluador = Class(Exception);

type
  TQRErrorString    = String[100];
  TQRErrorEvaluador = (eeDesconocido, eeSintaxis, eeConstante, eeTipos, eeLogicos,
    eeFuncionPendiente, eeFuncionNoExiste, eeVariableNoExiste, eeParentesis, eeFuncion,
    eeFuncionParam);

type
  RecErrorEvaluador = record
    ErrTipo    : TQRErrorEvaluador;
    ErrDescrip : String;
    ErrMsg     : String;
    ErrExp     : String;
    ErrCompleto: String;
  end;

const
  cQRErrorDescrip: array [eeDesconocido .. eeFuncionParam] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('Desconocido', 'Sintaxis',
    'Constante', 'Tipos', 'Faltan Par�ntesis al Comparar', 'Funci�n Pendiente', 'Funci�n No Existe',
    'Campo No Existe', 'Par�ntesis', 'Calculando Funci�n', 'Par�metros de Funci�n');

  { **************** Fin EZM ****************** }

type
  TQREvElementFunction = class;
  TQREvElement         = class;

  { TQRLibraryEntry }
  TQRLibraryItemClass = class of TObject;

  TQRLibraryEntry = class
  private
    FDescription: string;
    FData       : string;
    FItem       : TQRLibraryItemClass;
    FName       : string;
    FVendor     : string;
  public
    property Data       : string read FData write FData;
    property Description: string read FDescription write FDescription;
    property Name       : string read FName write FName;
    property Vendor     : string read FVendor write FVendor;
    property Item       : TQRLibraryItemClass read FItem write FItem;
  end;

  { TQRLibrary }
  TQRLibrary = class
  private
    Entries: TStrings;
  protected
    function GetEntry(Index: integer): TQRLibraryEntry; virtual;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(aItem: TQRLibraryItemClass; AName, ADescription, AVendor, AData: string);
    property EntryList: TStrings read Entries write Entries;
    property Entry[Index: integer]: TQRLibraryEntry read GetEntry;
  end;

  { CV TQREvaluator - Lo requiere desde TQrFunction }
  TQREvaluator = class;

  { TQRFunctionLibrary }
  TQRFunctionLibrary = class(TQRLibrary)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    // CV
    function GetFunction(Name: string; oEvaluador: TQREvaluator): TQREvElement;
    procedure RegisterFunction(FunctionClass: TQRLibraryItemClass; const Name: String;
      const Description: string = '');
    procedure RegisterFunctionDP(FunctionClass: TQRLibraryItemClass; const Name: String;
      const Description: string = '');
  end;

  { TQREvaluator related declarations }
  { EZM: opDentro, opNegar }
  TQREvOperator = (opLess, opLessOrEqual, opGreater, opGreaterOrEqual, opEqual, opUnequal, opPlus,
    opMinus, opOr, opMul, opDiv, opAnd, opDentro, opNegar, opModulo);

  TQREvResultClass = class
  public
    EvResult: TQREvResult;
    { EZM }
    Element: TQREvElement;
  end;

  TQRFiFo = class
  private
    FAggreg  : boolean;
    FiFo     : TList;
    FNextItem: integer;
    function GetFiFoCount: integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Put(Value: TObject);
    procedure Start;
    function Get: TObject;
    function GetAndFree: TObject;
    procedure Delete(nPos: integer);
    property Aggreg: boolean read FAggreg write FAggreg;
    property NextItem: integer read FNextItem write FNextItem;
    property Count: integer read GetFiFoCount;
  end;

  TQREvElement = class
  private
    FIsAggreg: boolean;
  public
    constructor Create(oEvaluador: TQREvaluator = NIL); virtual;
    function Value(FiFo: TQRFiFo): TQREvResult; virtual;
    procedure Reset; virtual;
    property IsAggreg: boolean read FIsAggreg write FIsAggreg;
    { EZM }
    function GetExpresion(FiFo: TQRFiFo): String; virtual;
  end;

  { TQREvElementOperator }
  TQREvElementOperator = class(TQREvElement)
  private
    FOpCode: TQREvOperator;
    procedure ConverTQREvResults(var Res1: TQREvResult; var Res2: TQREvResult);
    { EZM }
    function ErrorTipos(Texto: TQRErrorString): TQREvResult;
  public
    constructor CreateOperator(OpCode: TQREvOperator);
    function Value(FiFo: TQRFiFo): TQREvResult; override;
    { EZM }
    function GetExpresion(FiFo: TQRFiFo): String; override;
  end;

  TQREvElementFunction = class(TQREvElement)
  private
  protected
    ArgList: TList;
    { EZM }
    Nombre           : String[10];
    FEvaluador       : TQREvaluator;
    FExpresion       : String;
    FEsFecha         : boolean;
    FRegresaConstante: boolean;
    {$IFDEF MSSQL}
    FCalculando: boolean;
    {$ENDIF}
    function ArgumentOK(Value: TQREvElement): boolean;
    function Argument(Index: integer): TQREvResult;
    procedure FreeArguments;
    procedure GetArguments(FiFo: TQRFiFo);
    function GetArgumentsConstantes(FiFo: TQRFiFo): boolean;
    procedure Aggregate; virtual;
    function Calculate: TQREvResult; virtual;
    procedure ParametrosFijos(const Numero: integer);
    {$IFDEF MSSQL}
    property Calculando: boolean read FCalculando write FCalculando;
    {$ENDIF}
  public
    // cv
    constructor Create(oEvaluador: TQREvaluator); override;
    destructor Destroy; override;
    function Value(FiFo: TQRFiFo): TQREvResult; override;
    { EZM }
    function ValueConstante(FiFo: TQRFiFo): TQREvResult;
    // function GetRequeridos: String; virtual;
    procedure GetRequeridos(var TipoRegresa: TQREvResultType); virtual;
    property Evaluador: TQREvaluator read FEvaluador write FEvaluador;
    function ArgumentElement(const Index: integer): TQREvElement;
    function ParamEsCampo(const iParam: Word): boolean;
    function ParamNombreCampo(const iParam: Word): String;
    property Expresion: String read FExpresion write FExpresion;
    function ParametrosConstantes: boolean;
    property EsFecha: boolean read FEsFecha write FEsFecha;
    property RegresaConstante: boolean read FRegresaConstante write FRegresaConstante;
  end;

  TQREvElementFunctionClass = class of TQREvElementFunction;

  TQREvElementArgumentEnd = class(TQREvElement);

  TQREvElementDataField = class(TQREvElement)
  private
    FDataSet: TZetaCursor;
    FFieldNo: integer;
    FField  : TZetaField;
  public
    constructor CreateField(aField: TZetaField); virtual;
    function Value(FiFo: TQRFiFo): TQREvResult; override;
  end;

  {$WARNINGS off}

  TQREvElementError = class(TQREvElement)
  private
    FErrorMessage: string;
  public
    constructor Create(ErrorMessage: string);
    function Value(FiFo: TQRFiFo): TQREvResult; override;
  end;
  {$WARNINGS on}
  { EZM
  TQREvElementError = class(TQREvElement)
  public
    function Value(FiFo : TQRFiFo) : TQREvResult; override;
  end;
  }

  { TQREvaluator class }

  TQREvaluator = class(TObject)
  private
    FLibrary : TQRFunctionLibrary;
    FDataSets: TList;
    FiFo     : TQRFiFo;
    FPrepared: boolean;
    function EvalFunctionExpr(const strFunc: string): TQREvResult;
    function EvalSimpleExpr(const strSimplExpr: string): TQREvResult;
    { EZM }
    function EvalOrExpr(const strSimplExpr: string): TQREvResult;
    { EZM }
    function EvalAndExpr(const strSimplExpr: string): TQREvResult;
    function EvalTerm(const strTermExpr: string): TQREvResult;
    function EvalFactor(strFactorExpr: string): TQREvResult;
    function EvalString(const strString: string): TQREvResult;
    function EvalConstant(const strConstant: string): TQREvResult;
    function GetAggregate: boolean;
    { EZM }
    procedure ResuelveComillas(c: AnsiChar; var cLastComilla: AnsiChar; var booString: boolean);
    // function  NegateResult(const Res : TQREvResult) : TQREvResult;
    function Evaluate(const strExpr: string): TQREvResult;
    procedure FindDelimiter(strArg: string; var Pos: integer);
    procedure FindOp1(const strExpr: string; var Op: TQREvOperator; var Pos, Len: integer);
    procedure FindOp2(const strExpr: string; var Op: TQREvOperator; var Pos, Len: integer);
    procedure FindOp3(const strExpr: string; var Op: TQREvOperator; var Pos, Len: integer);
    { EZM }
    procedure FindOpLogico(const sOperador, strExpr: string; var Op: TQREvOperator;
      var Pos, Len: integer);
    procedure SetAggregate(Value: boolean);
    procedure TrimString(var strString: string);
  protected
    { EZM }
    FBuscandoRequeridos     : boolean;
    LastFunction            : TQREvElement;
    property FunctionLibrary: TQRFunctionLibrary read FLibrary write FLibrary;
    function EvalFunction(strFunc: string; const strArg: string): TQREvResult; virtual;
    function EvalVariable(strVariable: string): TQREvResult; virtual;
    function GetIsAggreg: boolean;
  public
    constructor Create;
    destructor Destroy; override;
    function Calculate(const strExpr: string): TQREvResult;
    function Value: TQREvResult;
    procedure Prepare(const strExpr: string);
    procedure Reset;
    procedure UnPrepare;
    property IsAggreg: boolean read GetIsAggreg;
    property Aggregate: boolean read GetAggregate write SetAggregate;
    property DataSets: TList read FDataSets write FDataSets;
    property Prepared: boolean read FPrepared write FPrepared;
    { EZM }
    property BuscandoRequeridos: boolean read FBuscandoRequeridos write FBuscandoRequeridos;
    procedure CreaCampo(Tipo: TQREvResultType; const Exp: String; const lEsFecha: boolean);
    procedure IncluyeCampos(strCampos: string); virtual;
    function SQLValido(const Exp: String; var sSQL: String): boolean;
    function EsConstante: boolean;
  end;

  { EZM }
  function ErrorEvaluador(const Numero: TQRErrorEvaluador; const Msg: TQRErrorString;
    const Exp: String): String;
  function ErrorCreate(Value: string): TQREvResult;
  function HuboErrorEvaluador(const Res: TQREvResult; var Numero: integer;
    var Descrip, Msg, Exp: String): boolean;
  function DesarmaErrorEvaluador(sError: String): RecErrorEvaluador;

const
  { EZM }
  cQROpDescrip: array [opLess .. opModulo] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('<', '<=', '>', '>=', '=', '<>', '+', '-',
    'OR', '*', '/', 'AND', '$', 'NOT', '%');

implementation

const
  ArgSeparator = ',';

function Dentro(sSearch, sTarget: String): boolean;

  function NextToken(var sTarget: String): String;
  var
    i: integer;
  begin
    i := Pos(',', sTarget);
    if (i <> 0) then begin
      Result := System.Copy(sTarget, 1, (i - 1));
      System.Delete(sTarget, 1, i);
    end else begin
      Result  := sTarget;
      sTarget := '';
    end;
  end;

begin
  Result := FALSE;
  while not Result and (Length(Trim(sTarget)) > 0) do
    Result := (sSearch = NextToken(sTarget));
end;

{ TQRLibrary }

constructor TQRLibrary.Create;
begin
  inherited Create;
  Entries := TStringList.Create;
end;

destructor TQRLibrary.Destroy;
var
  i: integer;
begin
  for i := 0 to Entries.Count - 1 do
    Entries.Objects[i].Free;
  Entries.Free;
  inherited Destroy;
end;

procedure TQRLibrary.Add(aItem: TQRLibraryItemClass; AName, ADescription, AVendor, AData: string);
var
  aLibraryEntry: TQRLibraryEntry;
begin
  aLibraryEntry := TQRLibraryEntry.Create;
  with aLibraryEntry do begin
    Name        := AName;
    Description := ADescription;
    Vendor      := AVendor;
    Data        := AData;
    Item        := aItem;
  end;
  Entries.AddObject(AName, aLibraryEntry);
end;

function TQRLibrary.GetEntry(Index: integer): TQRLibraryEntry;
begin
  if Index <= Entries.Count then
    Result := TQRLibraryEntry(Entries.Objects[Index])
  else
    Result := nil;
end;

{ TQRFunctionLibrary }

function TQRFunctionLibrary.GetFunction(Name: string; oEvaluador: TQREvaluator): TQREvElement;
var
  i            : integer;
  AObject      : TQREvElementFunctionClass;
  aLibraryEntry: TQRLibraryEntry;
begin
  { EZM }
  if Length(Name) > 10 then
    Name := Copy(Name, 1, 10);

  i := Entries.IndexOf(Name);
  if i >= 0 then begin
    aLibraryEntry := TQRLibraryEntry(Entry[i]);
    AObject       := TQREvElementFunctionClass(aLibraryEntry.Item);
    Result        := AObject.Create(oEvaluador);
    { EZM }
    TQREvElementFunction(Result).Nombre := Name;
  end else
    Result := TQREvElementError.Create(ErrorEvaluador(eeFuncionNoExiste, '', Name));
end;

procedure TQRFunctionLibrary.RegisterFunction(FunctionClass: TQRLibraryItemClass;
  const Name: String; const Description: string = '');
begin
  Add(FunctionClass, Copy(Name, 1, 10), Description, '', '');
end;

procedure TQRFunctionLibrary.RegisterFunctionDP(FunctionClass: TQRLibraryItemClass;
  const Name: String; const Description: string = '');
var
  iPosicion: integer;
begin
  with EntryList do begin
    iPosicion := IndexOf(Name);
    if (iPosicion >= 0) then
      Delete(iPosicion);
  end;
  RegisterFunction(FunctionClass, Name);
end;

{ TQREvaluator }

constructor TQRFiFo.Create;
begin
  FiFo      := TList.Create;
  FAggreg   := FALSE;
  FNextItem := 0;
end;

destructor TQRFiFo.Destroy;
var
  i: integer;
begin
  for i := 0 to FiFo.Count - 1 do
    TObject(FiFo[i]).Free;
  FiFo.Free;
  inherited Destroy;
end;

procedure TQRFiFo.Start;
begin
  FNextItem := 0;
end;

procedure TQRFiFo.Put(Value: TObject);
begin
  FiFo.Add(Value);
end;

function TQRFiFo.GetAndFree: TObject;
begin
  if FiFo.Count > 0 then begin
    Result := FiFo[0];
    FiFo.Delete(0);
  end else
    Result := nil;
end;

function TQRFiFo.Get: TObject;
begin
  if FNextItem < FiFo.Count then begin
    Result := FiFo[FNextItem];
    inc(FNextItem);
  end else
    Result := nil;
end;

function TQRFiFo.GetFiFoCount: integer;
begin
  Result := FiFo.Count;
end;

procedure TQRFiFo.Delete(nPos: integer);
begin
  FiFo.Delete(nPos);
end;

{ ******************* EZM ******************** }
function ErrorCreate(Value: string): TQREvResult;
begin
  Result.Kind      := resError;
  Result.strResult := Value;
end;

function ErrorEvaluador(const Numero: TQRErrorEvaluador; const Msg: TQRErrorString;
  const Exp: String): String;
begin
  Result := '[' + IntToStr(Ord(Numero)) + ']' + Msg + '|' + Exp;
end;

function DesarmaErrorEvaluador(sError: String): RecErrorEvaluador;
var
  nPos: integer;
begin
  with Result do begin
    if (Copy(sError, 1, 1) = '[') then begin
      // Obtiene el N�mero
      nPos    := Pos(']', sError);
      ErrTipo := TQRErrorEvaluador(StrToInt(Copy(sError, 2, nPos - 2)));
      sError  := Copy(sError, nPos + 1, MAXINT);
      // Obtiene Msg
      nPos   := Pos('|', sError);
      ErrMsg := Copy(sError, 1, nPos - 1);
      // Obtiene Exp
      ErrExp := Copy(sError, nPos + 1, MAXINT);
    end else begin
      ErrTipo := eeDesconocido;
      ErrMsg  := sError;
      ErrExp  := '';
    end;
    ErrDescrip := cQRErrorDescrip[ErrTipo];
    ErrCompleto := 'Error N�mero: ' + IntToStr(Ord(ErrTipo)) + CR_LF + ErrDescrip + CR_LF + ErrMsg +
      CR_LF + ErrExp;
  end;
end;

function HuboErrorEvaluador(const Res: TQREvResult; var Numero: integer;
  var Descrip, Msg, Exp: String): boolean;
begin
  Result := (Res.Kind = resError);
  if (Result) then
    with DesarmaErrorEvaluador(Res.strResult) do begin
      Numero  := Ord(ErrTipo);
      Descrip := ErrDescrip;
      Msg     := ErrMsg;
      Exp     := ErrExp;
    end;
end;
{ ******************* FIN DE EZM ************** }

{ TQREvElement }

constructor TQREvElement.Create(oEvaluador: TQREvaluator);
begin
  inherited Create;
  FIsAggreg := FALSE;
end;

function TQREvElement.Value(FiFo: TQRFiFo): TQREvResult;
begin
end;

procedure TQREvElement.Reset;
begin
end;

constructor TQREvElementOperator.CreateOperator(OpCode: TQREvOperator);
begin
  inherited Create;
  FOpCode := OpCode;
end;

{ EZM }
function TQREvElementOperator.ErrorTipos(Texto: TQRErrorString): TQREvResult;
begin
  Result := ErrorCreate(ErrorEvaluador(eeTipos, Texto, 'Operador ' + cQROpDescrip[FOpCode]));
end;

procedure TQREvElementOperator.ConverTQREvResults(var Res1: TQREvResult; var Res2: TQREvResult);
begin
  if (Res1.Kind <> resError) and (Res2.Kind <> resError) then
    if Res1.Kind <> Res2.Kind then begin
      if ((Res1.Kind = resInt) and (Res2.Kind = resDouble) and (not Res2.resFecha)) then begin
        Res1.Kind      := resDouble;
        Res1.dblResult := Res1.intResult;
      end else if ((Res1.Kind = resDouble) and (Res2.Kind = resInt)) then begin
        Res2.Kind      := resDouble;
        Res2.dblResult := Res2.intResult;
      end else if (FOpCode = opDentro) and (Res1.Kind = resInt) and (Res2.Kind = resString) then begin
        Res1.Kind      := resString;
        Res1.strResult := IntToStr(Res1.intResult);
      end else if ((FOpCode = opNegar) and (Res1.Kind = resInt) and (Res2.Kind = resBool)) then begin
        { EZM }
        Res1.Kind := resBool;
      end else begin
        { EZM }
        Res1 := ErrorTipos('Tipos Diferentes');
      end;
    end;
end;

function TQREvElementOperator.Value(FiFo: TQRFiFo): TQREvResult;
var
  Res1, Res2: TQREvResult;

  procedure ComparaBooleanos;
  const
    K_BOOLEANOS = ['s', 'n'];
  begin
    // Para comparar campos 'Booleanos', ignorando min�sculas
    // Ej. CB_CHECA = 's' es igual que CB_CHECA = 'S'
    if (Length(Res1.strResult) = 1) and (Length(Res2.strResult) = 1) and
      ((Res1.strResult[1] in K_BOOLEANOS) or (Res2.strResult[1] in K_BOOLEANOS)) then begin
      Res1.strResult := UpperCase(Res1.strResult);
      Res2.strResult := UpperCase(Res2.strResult);
    end;
  end;

begin { Value }
  Res1 := TQREvElement(FiFo.Get).Value(FiFo);
  Res2 := TQREvElement(FiFo.Get).Value(FiFo);
  ConverTQREvResults(Res1, Res2);
  Result.Kind := Res1.Kind;
  { EZM }
  Result.resFecha := FALSE;
  if Res2.Kind = resError then
    Result.Kind := Res2.Kind;
  if Result.Kind <> resError then begin
    case FOpCode of
      opPlus:
        case Result.Kind of
          resInt:
            Result.intResult := Res1.intResult + Res2.intResult;
          resDouble: begin
              if (Res1.resFecha) then
                Result.resFecha := TRUE;
              Result.dblResult  := Res1.dblResult + Res2.dblResult;
            end;
          resString:
            Result.strResult := Res1.strResult + Res2.strResult; { EZM }
          //resBool:
          //  result.Kind := resError;
          resBool:
            Result := ErrorTipos('No pueden ser L�gicos');
        end;
      opMinus:
        case Result.Kind of
          resInt:
            Result.intResult := Res1.intResult - Res2.intResult;
          resDouble: begin
              // La resta de 2 fechas es un Entero
              if (Res1.resFecha) then begin
                if (Res2.resFecha) then begin
                  Result.Kind      := resInt;
                  Result.intResult := Trunc(Res1.dblResult - Res2.dblResult);
                end else begin
                  // Fecha - Dias es una Fecha
                  Result.resFecha  := TRUE;
                  Result.dblResult := Res1.dblResult - Res2.dblResult;
                end;
              end else
                Result.dblResult := Res1.dblResult - Res2.dblResult;
            end;
          { EZM }
          //resString:
          //  result.Kind := resError;
          //resBool:
          //  result.Kind := resError;
          resString:
            Result := ErrorTipos('No pueden ser Textos');
          resBool:
            Result := ErrorTipos('No pueden ser L�gicos');
        end;
      { EZM : opNegar Permite la NEGACION con NOT }
      opNegar:
        if Result.Kind = resBool then
          Result.booResult := not Res2.booResult
        else
          Result := ErrorTipos('No es L�gico');
      { EZM : opDentro }
      opDentro:
        if Result.Kind = resString then begin
          Result.Kind      := resBool;
          Result.booResult := Dentro(Res1.strResult, Res2.strResult);
        end else
          Result := ErrorTipos('No son caracter');
      opMul:
        case Result.Kind of
          resInt:
            Result.intResult := Res1.intResult * Res2.intResult;
          resDouble:
            Result.dblResult := Res1.dblResult * Res2.dblResult; { EZM }
          resString, resBool:
            Result := ErrorTipos('No son N�meros');
        end;
      opDiv:
        case Result.Kind of
          resInt:
            if Res2.intResult <> 0 then begin
              Result.dblResult := Res1.intResult / Res2.intResult;
              Result.Kind      := resDouble;
            end else begin
              { EZM : Divisi�n etre 0 = 0 }
              Result.dblResult := 0;
              Result.Kind      := resDouble;
            end;
          resDouble:
            if Res2.dblResult <> 0 then
              Result.dblResult := Res1.dblResult / Res2.dblResult
            else
              { EZM : Divisi�n etre 0 = 0 }
              Result.dblResult := 0;
          // result.Kind := resError;
          { EZM }
          resString, resBool:
            Result := ErrorTipos('No son N�meros');
        end;
      opModulo:
        case Result.Kind of
          resInt:
            if Res2.intResult <> 0 then
              Result.intResult := Res1.intResult mod Res2.intResult
            else
              Result.intResult := 0; { Divisi�n entre 0 }
          resDouble:
            Result := ErrorTipos('S�lo se permiten n�meros enteros');
          else
            Result := ErrorTipos('No son n�meros');
        end;
      opGreater:
        begin
          Result.Kind := resBool;
          case Res1.Kind of
            resInt:
              Result.booResult := Res1.intResult > Res2.intResult;
            resDouble:
              Result.booResult := Res1.dblResult > Res2.dblResult;
            resString:
              Result.booResult := Res1.strResult > Res2.strResult; { EZM }
            resBool:
              Result := ErrorTipos('No pueden ser L�gicos');
          end;
        end;
      opGreaterOrEqual:
        begin
          Result.Kind := resBool;
          case Res1.Kind of
            resInt:
              Result.booResult := Res1.intResult >= Res2.intResult;
            resDouble:
              Result.booResult := Res1.dblResult >= Res2.dblResult;
            resString:
              Result.booResult := Res1.strResult >= Res2.strResult; { EZM }
            resBool:
              Result := ErrorTipos('No pueden ser L�gicos');
          end;
        end;
      opLess: begin
          Result.Kind := resBool;
          case Res1.Kind of
            resInt:
              Result.booResult := Res1.intResult < Res2.intResult;
            resDouble:
              Result.booResult := Res1.dblResult < Res2.dblResult;
            resString:
              Result.booResult := Res1.strResult < Res2.strResult; { EZM }
            resBool:
              Result := ErrorTipos('No pueden ser L�gicos');
          end;
        end;
      opLessOrEqual: begin
          Result.Kind := resBool;
          case Res1.Kind of
            resInt:
              Result.booResult := Res1.intResult <= Res2.intResult;
            resDouble:
              Result.booResult := Res1.dblResult <= Res2.dblResult;
            resString:
              Result.booResult := Res1.strResult <= Res2.strResult; { EZM }
            resBool:
              Result := ErrorTipos('No pueden ser L�gicos');
          end;
        end;
      opEqual: begin
          Result.Kind := resBool;
          case Res1.Kind of
            resInt:
              Result.booResult := Res1.intResult = Res2.intResult;
            resDouble:
              Result.booResult := Res1.dblResult = Res2.dblResult;
            resString:
              begin
                ComparaBooleanos;
                Result.booResult := Res1.strResult = Res2.strResult;
              end;
            resBool:
              Result.booResult := Res1.booResult = Res2.booResult;
          end;
        end;
      opUnequal:
        begin
          Result.Kind := resBool;
          case Res1.Kind of
            resInt:
              Result.booResult := Res1.intResult <> Res2.intResult;
            resDouble:
              Result.booResult := Res1.dblResult <> Res2.dblResult;
            resString:
              begin
                ComparaBooleanos;
                Result.booResult := Res1.strResult <> Res2.strResult;
              end;
            resBool:
              Result.booResult := Res1.booResult <> Res2.booResult;
          end;
        end;
      opOr:
        begin
          Result.Kind := resBool;
          case Res1.Kind of
            { EZM : No tiene caso }
            resInt, resDouble, resString:
              Result := ErrorTipos('No son L�gicos');
            resBool:
              Result.booResult := Res1.booResult or Res2.booResult;
          end;
        end;
      opAnd:
        begin
          Result.Kind := resBool;
          case Res1.Kind of
            { EZM : No tiene caso }
            resInt, resDouble, resString:
              Result := ErrorTipos('No son L�gicos');
            resBool:
              Result.booResult := Res1.booResult and Res2.booResult;
          end;
        end;
    end;
    { EZM: Aqu� es la propagaci�n de los errores }
  end else if Res1.Kind = resError then
    Result := Res1
  else
    Result := Res2;
end;

{ TQREvElementConstant }

type
  TQREvElementConstant = class(TQREvElement)
  private
    FValue: TQREvResult;
    { EZM }
    FExpresion: String;
  public
    constructor CreateConstant(Value: TQREvResult);
    function Value(FiFo: TQRFiFo): TQREvResult; override;
    { EZM }
    property Expresion: String read FExpresion write FExpresion;
    function GetExpresion(FiFo: TQRFiFo): String; override;
  end;

constructor TQREvElementConstant.CreateConstant(Value: TQREvResult);
begin
  inherited Create;
  FValue := Value;
  { EZM }
  FExpresion := '';
end;

function TQREvElementConstant.Value(FiFo: TQRFiFo): TQREvResult;
begin
  Result := FValue;
end;

{ EZM }
function TQREvElementConstant.GetExpresion(FiFo: TQRFiFo): String;
begin
  if FExpresion > '' then
    Result := FExpresion
  else
    case FValue.Kind of
      resString:
        Result := '''' + FValue.strResult + '''';
      resInt:
        Result := IntToStr(FValue.intResult);
      resDouble:
        if (FValue.resFecha) then
          Result := '''' + FormatDateTime('mm/dd/yyyy', FValue.dblResult) + ''''
        else
          Result := FloatToStr(FValue.dblResult);
      resBool:
        if FValue.booResult then
          Result := 'TRUE'
        else
          Result := 'FALSE';
    end;
end;

{ TQREvElementString }

type
  TQREvElementString = class(TQREvElement)
  private
    FValue: string;
  public
    constructor CreateString(Value: string);
    function Value(FiFo: TQRFiFo): TQREvResult; override;
    { EZM }
    function GetExpresion(FiFo: TQRFiFo): String; override;
  end;

constructor TQREvElementString.CreateString(Value: string);
begin
  inherited Create;
  FValue := Value;
end;

function TQREvElementString.Value(FiFo: TQRFiFo): TQREvResult;
begin
  { EZM }
  Result.resFecha  := FALSE;
  Result.Kind      := resString;
  Result.strResult := FValue;
end;

function TQREvElementString.GetExpresion(FiFo: TQRFiFo): String;
begin
  Result := '''' + FValue + '''';
end;

{ TQREvElementFunction }

constructor TQREvElementFunction.Create(oEvaluador: TQREvaluator);
begin
  inherited Create;
  Evaluador := oEvaluador;
  ArgList   := TList.Create;
  { EZM }
  FEsFecha          := FALSE;
  FRegresaConstante := FALSE;
  {$IFDEF MSSQL}
  Calculando := TRUE;
  {$ENDIF}
end;

destructor TQREvElementFunction.Destroy;
begin
  ArgList.Free;
  inherited Destroy;
end;

procedure TQREvElementFunction.GetArguments(FiFo: TQRFiFo);
var
  aArgument: TQREvElement;
  AResult  : TQREvResultClass;
begin
  repeat
    aArgument := TQREvElement(FiFo.Get);
    if not(aArgument is TQREvElementArgumentEnd) then begin
      AResult          := TQREvResultClass.Create;
      AResult.EvResult := aArgument.Value(FiFo);
      { EZM }
      AResult.Element := aArgument;
      ArgList.Add(AResult);
    end;
  until aArgument is TQREvElementArgumentEnd;
end;

procedure TQREvElementFunction.FreeArguments;
var
  i: integer;
begin
  for i := 0 to ArgList.Count - 1 do
    TQREvElement(ArgList.Items[i]).Free;
  ArgList.Clear;
end;

function TQREvElementFunction.Argument(Index: integer): TQREvResult;
begin
  if Index <= ArgList.Count then
    Result := TQREvResultClass(ArgList[Index]).EvResult;
end;

function TQREvElementFunction.ArgumentElement(const Index: integer): TQREvElement;
begin
  Result := TQREvResultClass(ArgList[Index]).Element;
end;

function TQREvElementFunction.Value(FiFo: TQRFiFo): TQREvResult;
{ EZM }
var
  i          : integer;
  TipoRegresa: TQREvResultType;
begin
  GetArguments(FiFo);
  { EZM }
  for i := 0 to ArgList.Count - 1 do
    if (Argument(i).Kind = resError) and (Argument(i).strResult <> 'NIL') then begin
      Result := Argument(i);
      FreeArguments;
      Exit;
    end;
  try
    if FiFo.Aggreg then
      Aggregate;

    if Evaluador.BuscandoRequeridos then begin
      TipoRegresa := resError;
      {$IFDEF MSSQL}
      Calculando := FALSE;
      {$ENDIF}
      GetRequeridos(TipoRegresa);
      {$IFDEF MSSQL}
      Calculando := TRUE;
      {$ENDIF}
      // Puede ser que GetRequeridos determine el Tipo de Regresa
      // En ese caso, ya no es necesario llamar a Calculate
      if (TipoRegresa <> resError) then begin
        Result.Kind := TipoRegresa;
        // Evita 'basura' en numericos y string
        case (TipoRegresa) of
          resInt:
            Result.intResult := 1;
          resDouble:
            Result.dblResult := 1;
          resString:
            Result.strResult := '';
        end;
      end
      else
        Result := Calculate;
    end
    else
      Result := Calculate;
    { EZM }
    Result.resFecha := EsFecha;
  except
    on E: EErrorEvaluador do begin
      Result.Kind      := resError;
      Result.strResult := E.Message;
    end;
    on E: Exception do begin
      Result.Kind      := resError;
      Result.strResult := ErrorEvaluador(eeFuncion, E.Message, 'Funci�n:' + Nombre);
    end;
  end;
  FreeArguments;
end;

function TQREvElementFunction.GetArgumentsConstantes(FiFo: TQRFiFo): boolean;
var
  aArgument: TQREvElement;
  AResult  : TQREvResultClass;
begin
  Result := TRUE;
  repeat
    aArgument := TQREvElement(FiFo.Get);
    if not(aArgument is TQREvElementArgumentEnd) then begin
      // Si el Par�metro es un CAMPO, aunque sea 'TQREvElementConstant'
      // no se debe considerar para que las funciones 'RegresenConsante'
      // Nos dabmos cuenta que es un campo por 'Expresion' no vac�a
      if not(((aArgument is TQREvElementConstant) and
            (Length(TQREvElementConstant(aArgument).Expresion) = 0)) or
          (aArgument is TQREvElementString)) then begin
        Result := FALSE;
        Exit;
      end;

      AResult          := TQREvResultClass.Create;
      AResult.EvResult := aArgument.Value(FiFo);
      { EZM }
      AResult.Element := aArgument;
      ArgList.Add(AResult);
    end;
  until aArgument is TQREvElementArgumentEnd;
end;

function TQREvElementFunction.ValueConstante(FiFo: TQRFiFo): TQREvResult;
begin
  if GetArgumentsConstantes(FiFo) then begin
    try
      Result          := Calculate;
      Result.resFecha := EsFecha;
    except
      Result.Kind := resError;
    end;
  end
  else
    Result.Kind := resError;
  FreeArguments;
end;

{ EZM }
procedure TQREvElementFunction.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
end;

function TQREvElementFunction.ArgumentOK(Value: TQREvElement): boolean;
begin
  Result := not(Value is TQREvElementArgumentEnd) and not(Value is TQREvElementError);
end;

procedure TQREvElementFunction.Aggregate;
begin
end;

function TQREvElementFunction.Calculate: TQREvResult;
begin
  Result := ErrorCreate(ErrorEvaluador(eeFuncionPendiente, '', Nombre));
end;

procedure TQREvElementFunction.ParametrosFijos(const Numero: integer);
begin
  if (ArgList.Count < Numero) then
    raise EErrorEvaluador.Create(ErrorEvaluador(eeFuncionParam, 'Faltan Par�metros Fijos:' +
          IntToStr(Numero - ArgList.Count), 'Funci�n: ' + Nombre + '()'));
end;

{ TQREvSumFunction }

type
  TQREvSumFunction = class(TQREvElementFunction)
  private
    SumResult  : TQREvResult;
    ResAssigned: boolean;
  public
    constructor Create(oEvaluador: TQREvaluator); override;
    procedure Aggregate; override;
    function Calculate: TQREvResult; override;
    procedure Reset; override;
    procedure GetRequeridos(var TipoRegresa: TQREvResultType); override;
  end;

constructor TQREvSumFunction.Create(oEvaluador: TQREvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := FALSE;
  IsAggreg    := TRUE;
end;

procedure TQREvSumFunction.Reset;
begin
  ResAssigned := FALSE;
end;

procedure TQREvSumFunction.Aggregate;
var
  aValue: TQREvResult;
begin
  if ArgList.Count = 1 then begin
    aValue := Argument(0);
    if ResAssigned then begin
      case aValue.Kind of
        resInt:
          SumResult.dblResult := SumResult.dblResult + aValue.intResult;
        resDouble:
          SumResult.dblResult := SumResult.dblResult + aValue.dblResult;
        else
          SumResult.Kind := resError
      end;
    end
    else begin
      SumResult.Kind := resDouble;
      case aValue.Kind of
        resInt:
          SumResult.dblResult := aValue.intResult;
        resDouble:
          SumResult.dblResult := aValue.dblResult;
        else
          SumResult.Kind := resError;
      end;
    end;
    ResAssigned := TRUE;
    if (SumResult.Kind = resError) then
      SumResult.strResult := ErrorEvaluador(eeTipos, 'S�lo se permiten N�meros', 'Funci�n SUM()');
  end
  else
    SumResult := ErrorCreate(ErrorEvaluador(eeFuncionParam, 'Falta par�metro Num�rico',
        'Funci�n SUM()'));
end;

function TQREvSumFunction.Calculate: TQREvResult;
begin
  if ResAssigned then
    Result := SumResult
  else
    Result := ErrorCreate(ErrorEvaluador(eeFuncion, 'No ha sido inicializado', 'Funci�n SUM()'));
end;

procedure TQREvSumFunction.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
  ParametrosFijos(1);
  TipoRegresa := resDouble;
end;

{ TQREvAverageFunction }

type
  TQREvAverageFunction = class(TQREvSumFunction)
  private
    Count  : longint;
    AResult: TQREvResult;
  public
    function Calculate: TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
  end;

procedure TQREvAverageFunction.Reset;
begin
  inherited Reset;
  AResult  := ErrorCreate(ErrorEvaluador(eeFuncion, 'No hay registros', 'Funci�n AVERAGE()'));
  Count    := 0;
  IsAggreg := TRUE;
end;

procedure TQREvAverageFunction.Aggregate;
var
  aValue: TQREvResult;
begin
  inherited Aggregate;
  inc(Count);
  aValue       := inherited Calculate;
  AResult.Kind := resDouble;
  case aValue.Kind of
    resInt:
      AResult.dblResult := aValue.intResult / Count;
    resDouble:
      AResult.dblResult := aValue.dblResult / Count;
    else
      AResult := aValue; // Recibe el error
  end;
end;

function TQREvAverageFunction.Calculate: TQREvResult;
begin
  if ResAssigned then
    Result := AResult
  else
    Result := ErrorCreate(ErrorEvaluador(eeFuncion, 'No ha sido inicializado',
        'Funci�n AVERAGE()'));
end;

{ TQREvMaxFunction }

type
  TQREvMaxFunction = class(TQREvElementFunction)
  private
    MaxResult  : TQREvResult;
    ResAssigned: boolean;
  public
    constructor Create(oEvaluador: TQREvaluator); override;
    function Calculate: TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
    procedure GetRequeridos(var TipoRegresa: TQREvResultType); override;
  end;

constructor TQREvMaxFunction.Create(oEvaluador: TQREvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := FALSE;
  IsAggreg    := TRUE;
end;

procedure TQREvMaxFunction.Reset;
begin
  ResAssigned := FALSE;
end;

procedure TQREvMaxFunction.Aggregate;
var
  aValue: TQREvResult;
begin
  if ArgList.Count = 1 then begin
    aValue := Argument(0);
    if ResAssigned then begin
      case aValue.Kind of
        resInt:
          if aValue.intResult > MaxResult.dblResult then
            MaxResult.dblResult := aValue.intResult;
        resDouble:
          if aValue.dblResult > MaxResult.dblResult then
            MaxResult.dblResult := aValue.dblResult;
        resString:
          if aValue.strResult > MaxResult.strResult then
            MaxResult.strResult := aValue.strResult;
        resBool:
          if aValue.booResult > MaxResult.booResult then
            MaxResult.booResult := aValue.booResult;
        else
          MaxResult := aValue; // Pasa el Error
      end
    end
    else begin
      // Obliga a DOUBLE
      if (aValue.Kind = resInt) then begin
        MaxResult.Kind      := resDouble;
        MaxResult.dblResult := aValue.intResult;
      end
      else
        MaxResult := aValue;
      ResAssigned := TRUE;
    end
  end
  else
    MaxResult.Kind := resError;
end;

function TQREvMaxFunction.Calculate: TQREvResult;
begin
  if ResAssigned then
    Result := MaxResult
  else
    Result := ErrorCreate(ErrorEvaluador(eeFuncion, 'No ha sido inicializado', 'Funci�n MAXIMO()'));

end;

procedure TQREvMaxFunction.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
  ParametrosFijos(1);
  TipoRegresa := Argument(0).Kind;
end;

{ TQREvMinFunction }

type
  TQREvMinFunction = class(TQREvElementFunction)
  private
    MinResult  : TQREvResult;
    ResAssigned: boolean;
  public
    constructor Create(oEvaluador: TQREvaluator); override;
    function Calculate: TQREvResult; override;
    procedure Aggregate; override;
    procedure Reset; override;
    procedure GetRequeridos(var TipoRegresa: TQREvResultType); override;
  end;

constructor TQREvMinFunction.Create(oEvaluador: TQREvaluator);
begin
  inherited Create(oEvaluador);
  ResAssigned := FALSE;
  IsAggreg    := TRUE;
end;

procedure TQREvMinFunction.Reset;
begin
  ResAssigned := FALSE;
end;

procedure TQREvMinFunction.Aggregate;
var
  aValue: TQREvResult;
begin
  if ArgList.Count = 1 then begin
    aValue := Argument(0);
    if ResAssigned then begin
      case aValue.Kind of
        resInt:
          if aValue.intResult < MinResult.dblResult then
            MinResult.dblResult := aValue.intResult;
        resDouble:
          if aValue.dblResult < MinResult.dblResult then
            MinResult.dblResult := aValue.dblResult;
        resString:
          if aValue.strResult < MinResult.strResult then
            MinResult.strResult := aValue.strResult;
        resBool:
          if aValue.booResult > MinResult.booResult then
            MinResult.booResult := aValue.booResult;
        else
          MinResult := aValue; // Pasa El error
      end
    end
    else begin
      ResAssigned := TRUE;
      // Obliga a DOUBLE
      if (aValue.Kind = resInt) then begin
        MinResult.Kind      := resDouble;
        MinResult.dblResult := aValue.intResult;
      end
      else
        MinResult := aValue;
    end
  end
  else
    MinResult.Kind := resError;
end;

function TQREvMinFunction.Calculate: TQREvResult;
begin
  if ResAssigned then
    Result := MinResult
  else
    Result := ErrorCreate(ErrorEvaluador(eeFuncion, 'No ha sido inicializado', 'Funci�n MINIMO()'));
end;

procedure TQREvMinFunction.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
  ParametrosFijos(1);
  TipoRegresa := Argument(0).Kind;
end;

{ TQREvCountFunction }

type
  TQREvCountFunction = class(TQREvElementFunction)
  private
    FCount: integer;
  public
    constructor Create(oEvaluador: TQREvaluator); override;
    function Value(FiFo: TQRFiFo): TQREvResult; override;
    procedure Reset; override;
    procedure GetRequeridos(var TipoRegresa: TQREvResultType); override;
  end;

constructor TQREvCountFunction.Create(oEvaluador: TQREvaluator);
begin
  inherited Create(oEvaluador);
  FCount   := 0;
  IsAggreg := TRUE;
end;

function TQREvCountFunction.Value(FiFo: TQRFiFo): TQREvResult;
begin
  GetArguments(FiFo);
  { EZM }
  Result.resFecha := FALSE;
  if FiFo.Aggreg then
    inc(FCount);
  Result.Kind      := resInt;
  Result.intResult := FCount;
  FreeArguments;
end;

procedure TQREvCountFunction.Reset;
begin
  FCount := 0;
end;

procedure TQREvCountFunction.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
  TipoRegresa := resInt;
end;

{ TQREvElementDataField }

constructor TQREvElementDataField.CreateField(aField: TZetaField);
begin
  inherited Create;
  // FDataSet := aField.DataSet;
  FDataSet := CursorDelCampo(aField);
  FFieldNo := aField.Index;
  FField   := aField;
end;

function TQREvElementDataField.Value(FiFo: TQRFiFo): TQREvResult;
begin
  // IBO if FDataSet.DefaultFields then
  FField := FDataSet.Fields[FFieldNo];
  { EZM }
  Result.resFecha := FALSE;
  case TipoCampo(FField) of
    tgTexto: begin
        Result.Kind := resString;
        { EZM: Puse el Trim para CHAR <> VARCHAR en ADO }
        Result.strResult := Trim(FField.AsString);
      end;
    tgNumero: begin
        Result.Kind      := resInt;
        Result.intResult := FField.AsInteger;
      end;
    tgFloat: begin
        Result.Kind      := resDouble;
        Result.dblResult := FField.AsFloat;
      end;
    tgBooleano: begin
        Result.Kind      := resBool;
        Result.booResult := FField.AsBoolean;
      end;
    tgFecha: begin
        Result.resFecha  := TRUE;
        Result.Kind      := resDouble;
        Result.dblResult := Trunc(FField.AsDateTime);
        if Result.dblResult < 0 then // IBO Regresa fechas < 0
          Result.dblResult := 0;
      end;
    tgMemo: begin
        Result.resFecha  := FALSE;
        Result.Kind      := resString;
        Result.strResult := TMemoField(FField).AsString;
      end
    else begin { EZM }
        Result := ErrorCreate(ErrorEvaluador(eeTipos, 'Tipo de Campo No Existe ' + FieldTypeNames
              [FField.DataType], FField.FieldName));
      end;
  end;
end;

constructor TQREvElementError.Create(ErrorMessage: string);
begin
  FErrorMessage := ErrorMessage;
end;

function TQREvElementError.Value(FiFo: TQRFiFo): TQREvResult;
begin
  { EZM }
  Result.resFecha  := FALSE;
  Result.Kind      := resError;
  Result.strResult := FErrorMessage;
end;

function flip(aString: string; a, b: char): string;
var
  ParLevel: integer;
  isString: boolean;
  i       : integer;
  aChar   : string;
begin
  ParLevel := 0;
  isString := FALSE;
  i        := 1;
  while i <= Length(aString) do begin
    aChar := Copy(aString, i, 1);
    if aChar = '''' then
      isString := not isString
    else if not isString then begin
      if aChar = '(' then
        inc(ParLevel)
      else if aChar = ')' then
        dec(ParLevel)
      else if ParLevel = 0 then
        if aChar = a then
          aString[i] := b
        else if aChar = b then
          aString[i] := a;
    end;
    inc(i);
  end;
  Result := aString;
end;

{ TQREvaluator }
constructor TQREvaluator.Create;
begin
  Prepared := FALSE;
  { EZM }
  FBuscandoRequeridos := FALSE;
end;

destructor TQREvaluator.Destroy;
begin
  if Prepared then
    UnPrepare;
  inherited Destroy;
end;

{ ************** EZM ********************* }
function TQREvElementFunction.ParametrosConstantes: boolean;
var
  i       : integer;
  Elemento: TQREvElement;
begin
  Result     := TRUE;
  for i      := 0 to ArgList.Count - 1 do begin
    Elemento := ArgumentElement(i);
    if not((Elemento is TQREvElementConstant) or (Elemento is TQREvElementString)) then begin
      Result := FALSE;
      Break;
    end;
  end;
end;

function TQREvElementFunction.ParamEsCampo(const iParam: Word): boolean;
var
  Elemento: TQREvElement;
begin
  Result := FALSE;
  if ArgList.Count > iParam then begin
    Elemento := ArgumentElement(iParam);
    if (Elemento is TQREvElementConstant) then
      with Elemento as TQREvElementConstant do
        Result := Length(FExpresion) > 0;
  end;
end;

function TQREvElementFunction.ParamNombreCampo(const iParam: Word): String;
begin
  with ArgumentElement(iParam) as TQREvElementConstant do
    Result := FExpresion;
end;

function TQREvElement.GetExpresion(FiFo: TQRFiFo): String;
begin
  Result := '@@Error';
end;

{ EZM }
function TQREvElementOperator.GetExpresion(FiFo: TQRFiFo): String;
var
  Elemento1           : TQREvElement;
  Operando1, Operando2: String;
  Operador            : String;
  oLista              : TStringList;
  i                   : integer;
  lEntero             : boolean;
begin
  Elemento1 := TQREvElement(FiFo.Get);
  Operando1 := Elemento1.GetExpresion(FiFo);
  lEntero   := (Elemento1 is TQREvElementConstant) and (Elemento1.Value(FiFo).Kind = resInt);
  Operando2 := TQREvElement(FiFo.Get).GetExpresion(FiFo);
  if (FOpCode = opDentro) then begin
    Operador := 'IN';
    oLista   := TStringList.Create;
    with oLista do begin
      CommaText := Copy(Operando2, 2, Length(Operando2) - 2);
      Operando2 := '';
      for i     := 0 to Count - 1 do begin
        if i > 0 then
          Operando2 := Operando2 + ', ';
        if (lEntero) then
          Operando2 := Operando2 + Trim(Strings[i])
        else
          Operando2 := Operando2 + '''' + Trim(Strings[i]) + '''';
      end;
      Operando2 := '(' + Operando2 + ')';
    end;
    oLista.Free;
  end
  else if (FOpCode = opNegar) then begin
    Operando1 := '';
    Operador  := cQROpDescrip[FOpCode];
  end
  else
    Operador := cQROpDescrip[FOpCode];
  Result     := '(' + Operando1 + ' ' + Operador + ' ' + Operando2 + ')';
end;

function TQREvaluator.SQLValido(const Exp: String; var sSQL: String): boolean;
var
  F  : TObject;
  Res: TQREvResult;
begin
  FBuscandoRequeridos := TRUE;
  Prepare(Exp);

  FiFo.Start;
  F      := FiFo.Get;
  Result := FALSE;
  if F <> nil then begin
    Res := TQREvElement(F).Value(FiFo);
    if Res.Kind <> resError then begin
      FiFo.Start;
      F      := FiFo.Get;
      sSQL   := TQREvElement(F).GetExpresion(FiFo);
      Result := (Pos('@@Error', sSQL) = 0);
    end;
  end;
  UnPrepare;
  FBuscandoRequeridos := FALSE;
end;

procedure TQREvaluator.IncluyeCampos(strCampos: string);
begin
end;

procedure TQREvaluator.CreaCampo(Tipo: TQREvResultType; const Exp: String; const lEsFecha: boolean);
var
  Valor     : TQREvResult;
  oConstante: TQREvElementConstant;
begin
  Valor.resFecha := lEsFecha;
  Valor.Kind     := Tipo;
  case Tipo of
    resInt:
      Valor.intResult := 0;
    resDouble:
      Valor.dblResult := 0;
    resString:
      Valor.strResult := '';
    resBool:
      Valor.booResult := FALSE;
  end;

  oConstante           := TQREvElementConstant.CreateConstant(Valor);
  oConstante.Expresion := Exp;
  FiFo.Put(oConstante)
end;

{ ******************** FIN EZM ************************ }

procedure TQREvaluator.TrimString(var strString: string);
var
  intStart, intEnd: integer;
begin
  intStart := 1;
  intEnd   := Length(strString);
  while (Copy(strString, intStart, 1) = ' ') and (intStart < intEnd) do
    inc(intStart);
  while (Copy(strString, intEnd, 1) = ' ') and (intEnd > intStart) do
    dec(intEnd);
  strString := Copy(strString, intStart, intEnd - intStart + 1);
end;

procedure TQREvaluator.FindDelimiter(strArg: string; var Pos: integer);
var
  n            : integer;
  FoundDelim   : boolean;
  booString    : boolean;
  intParenteses: integer;
  cLastComilla : AnsiChar;
begin
  if strArg = '' then
    Pos := 0
  else begin
    FoundDelim    := FALSE;
    booString     := FALSE;
    intParenteses := 0;
    n             := 1;
    { EZM }
    cLastComilla := Chr(0);

    // while (N<length(strArg)) and not FoundDelim do
    while (n <= Length(strArg)) and not FoundDelim do begin
      case strArg[n] of
        '(':
          if not booString then
            inc(intParenteses);
        ')':
          if not booString then
            dec(intParenteses);
        { EZM }
        '''', '"', '[', ']':
          ResuelveComillas(AnsiChar(strArg[n]), AnsiChar(cLastComilla), booString);
        // '''' : booString := not booString;
      end;
      if (intParenteses = 0) and not booString then
        if strArg[n] = ArgSeparator then begin
          FoundDelim := TRUE;
          Break;
        end;
      inc(n);
    end;
    if FoundDelim then
      Pos := n
    else
      Pos := 0;
  end;
end;

{ EZM }
procedure TQREvaluator.ResuelveComillas(c: AnsiChar; var cLastComilla: AnsiChar;
  var booString: boolean);
begin
  case c of
    '''', '"':
      if (not booString) then begin
        booString    := TRUE;
        cLastComilla := c;
      end
      else if (c = cLastComilla) then
        booString := FALSE;
    '[':
      if (not booString) then begin
        booString    := TRUE;
        cLastComilla := c;
      end;
    ']':
      if (booString AND (cLastComilla = '[')) then
        booString := FALSE;
  end;
end;

procedure TQREvaluator.FindOp1(const strExpr: string; var Op: TQREvOperator; var Pos, Len: integer);
var
  n            : integer;
  booFound     : boolean;
  intParenteses: integer;
  aString      : string[255];
  booString    : boolean;
  { EZM }
  cLastComilla: AnsiChar;
begin
  n             := 1;
  intParenteses := 0;
  booFound      := FALSE;
  Len           := 1;
  aString       := strExpr;
  booString     := FALSE;
  cLastComilla  := Chr(0);
  while (n < Length(strExpr)) and (not booFound) do begin
    booFound := TRUE;
    case aString[n] of
      '(':
        if not booString then
          inc(intParenteses);
      ')':
        if not booString then
          dec(intParenteses);
      { EZM }
      '''', '"', '[', ']':
        ResuelveComillas(AnsiChar(aString[n]), cLastComilla, booString);
    end;
    if (intParenteses = 0) and (n > 1) and not booString then
      case aString[n] of
        '<': begin
            if aString[n + 1] = '>' then begin
              Op  := opUnequal;
              Len := 2;
            end
            else if aString[n + 1] = '=' then begin
              Op  := opLessOrEqual;
              Len := 2;
            end
            else
              Op := opLess;
          end;
        '>':
          if aString[n + 1] = '=' then begin
            Op  := opGreaterOrEqual;
            Len := 2;
          end
          else
            Op := opGreater;
        '=':
          Op := opEqual; { EZM }
        '$':
          Op := opDentro;
        else
          booFound := FALSE;
      end
    else
      booFound := FALSE;
    inc(n);
  end;
  if booFound then
    Pos := n - 1
  else
    Pos := -1;
end;

procedure TQREvaluator.FindOp2(const strExpr: string; var Op: TQREvOperator; var Pos, Len: integer);
var
  n            : integer;
  booFound     : boolean;
  intParenteses: integer;
  booString    : boolean;
  aString      : string[255];
  { EZM }
  aTempo      : string[255];
  cLastComilla: AnsiChar;
begin
  n             := 1;
  intParenteses := 0;
  booFound      := FALSE;
  booString     := FALSE;
  aString       := strExpr;
  Len           := 1;
  while (n < Length(strExpr)) and (not booFound) do begin
    booFound := TRUE;
    case aString[n] of
      '(':
        if not booString then
          inc(intParenteses);
      ')':
        if not booString then
          dec(intParenteses);
      { EZM }
      '''', '"', '[', ']':
        ResuelveComillas(AnsiChar(aString[n]), cLastComilla, booString);
    end;
    if (intParenteses = 0) and (not booString) and (n > 1) then
      case aString[n] of
        '+':
          Op := opPlus; { EZM }
        '-': begin
            // Permite operaciones con '-1'
            aTempo := Trim(Copy(strExpr, 1, n - 1));
            aTempo := Copy(aTempo, Length(aTempo), 1);
            if (aTempo = '') or (aTempo = '*') or (aTempo = '/') then begin
              booFound := FALSE;
              Break;
            end
            else
              Op := opMinus;
          end;
        { EZM }
        '!':
          Op := opNegar; { EZM
          ' ' : if (AnsiLowercase(copy(strExpr, N + 1, 3)) = 'or ') then
          begin
          Op := opOr;
          Len := 2;
          inc(N);
          end else
          booFound := false;
        }
        else
          booFound := FALSE;
      end
    else
      booFound := FALSE;
    inc(n);
  end;
  if booFound then
    Pos := n - 1
  else
    Pos := -1;
end;

procedure TQREvaluator.FindOp3(const strExpr: string; var Op: TQREvOperator; var Pos, Len: integer);
var
  n            : integer;
  booFound     : boolean;
  intParenteses: integer;
  booString    : boolean;
  aString      : string[255];
  { EZM }
  cLastComilla: AnsiChar;
begin
  n             := 1;
  intParenteses := 0;
  booFound      := FALSE;
  booString     := FALSE;
  Len           := 1;
  aString       := strExpr;
  cLastComilla  := Chr(0);
  while (n < Length(strExpr)) and (not booFound) do begin
    booFound := TRUE;
    case aString[n] of
      '(':
        if not booString then
          inc(intParenteses);
      ')':
        if not booString then
          dec(intParenteses);
      { EZM }
      '''', '"', '[', ']':
        ResuelveComillas(AnsiChar(aString[n]), cLastComilla, booString);
      // '''': booString := not booString;
    end;
    if (intParenteses = 0) and (not booString) and (n > 1) then begin
      case aString[n] of
        '*':
          Op := opMul;
        '/':
          Op := opDiv;
        '%':
          Op := opModulo; { EZM
          ' ' : if (AnsiLowercase(copy(strExpr, n + 1, 4)) = 'and ') then
          begin
          Op := opAnd;
          Len := 3;
          inc(N);
          end else
          booFound := false;
        }
        else
          booFound := FALSE;
      end;
    end
    else
      booFound := FALSE;
    inc(n);
  end;
  if booFound then
    Pos := n - 1
  else
    Pos := -1;
end;

procedure TQREvaluator.FindOpLogico(const sOperador, strExpr: string; var Op: TQREvOperator;
  var Pos, Len: integer);
var
  n            : integer;
  booFound     : boolean;
  intParenteses: integer;
  booString    : boolean;
  aString      : string[255];
  { EZM }
  cLastComilla: AnsiChar;
  nLenOp      : integer;
begin
  n             := 1;
  intParenteses := 0;
  booFound      := FALSE;
  booString     := FALSE;
  Len           := 1;
  aString       := strExpr;
  cLastComilla  := Chr(0);
  nLenOp        := Length(sOperador);
  while (n < Length(strExpr)) and (not booFound) do begin
    booFound := TRUE;
    case aString[n] of
      '(':
        if not booString then
          inc(intParenteses);
      ')':
        if not booString then
          dec(intParenteses);
      '''', '"', '[', ']':
        ResuelveComillas(AnsiChar(aString[n]), cLastComilla, booString);
    end;
    if (intParenteses = 0) and (not booString) and (n > 1) then begin
      case aString[n] of
        ' ':
          if (AnsiLowercase(Copy(strExpr, n + 1, nLenOp)) = sOperador) then begin
            if (sOperador = 'or ') then
              Op := opOr
            else
              Op := opAnd;
            Len  := nLenOp - 1;
            inc(n);
          end
          else
            booFound := FALSE;
        else
          booFound := FALSE;
      end;
    end
    else
      booFound := FALSE;
    inc(n);
  end;
  if booFound then
    Pos := n - 1
  else
    Pos := -1;
end;

{ ********** EZM *********
  function TQREvaluator.NegateResult(const Res : TQREvResult) : TQREvResult;
  begin
  result.Kind := Res.Kind;
  case Res.Kind of
  resInt: result.intResult := - Res.intResult;
  resDouble: result.dblResult := -Res.dblResult;
  resBool: result.booResult := not Res.booResult;
  else
  result.Kind := resError;
  end;
  end;
}

function TQREvaluator.EvalVariable(strVariable: string): TQREvResult;
var
  SeparatorPos: integer;
  FieldName   : string;
  aDataSet    : TZetaCursor;
  aField      : TZetaField;
  i           : integer;
begin
  if assigned(FDataSets) then begin
    SeparatorPos := AnsiPos('.', strVariable);
    FieldName    := AnsiUpperCase(Copy(strVariable, SeparatorPos + 1,
        Length(strVariable) - SeparatorPos));
    aField     := nil;
    for i      := 0 to FDataSets.Count - 1 do begin
      aDataSet := TZetaCursor(FDataSets[i]);
      aField   := aDataSet.FindField(FieldName);
      if aField <> nil then
        Break;
    end;

    if aField <> nil then
      FiFo.Put(TQREvElementDataField.CreateField(aField))
    else
      FiFo.Put(TQREvElementError.Create(ErrorEvaluador(eeVariableNoExiste, '', strVariable)));
  end
  else
    FiFo.Put(TQREvElementError.Create(ErrorEvaluador(eeVariableNoExiste, 'No hay Dataset',
          strVariable)));
end;

function TQREvaluator.EvalString(const strString: string): TQREvResult;
begin
  Result.Kind      := resString;
  Result.strResult := strString;
  FiFo.Put(TQREvElementString.CreateString(Result.strResult));
end;

function TQREvaluator.EvalFunction(strFunc: string; const strArg: string): TQREvResult;
var
  DelimPos  : integer;
  aString   : string;
  Res       : TQREvResult;
  aFunc     : TQREvElement;
  lFuncionOK: boolean;
  nPosFunc  : integer;
begin
  strFunc := AnsiUpperCase(strFunc);
  { EZM }
  LastFunction := NIL;
  { Con 'RegresaConstante', ya no se necesita
    if ( StrFunc = 'DATE' ) then
    begin
    Res.Kind := resDouble;
    Res.resFecha := TRUE;
    Res.dblResult := DATE;
    FiFo.Put(TQREvElementConstant.CreateConstant(Res));
    Exit;
    end;
  }

  aFunc := FunctionLibrary.GetFunction(strFunc, Self);
  if aFunc is TQREvElementError then begin
    if strArg = '' then begin
      aFunc.Free;
      EvalVariable(strFunc)
    end
    else begin
      FiFo.Put(aFunc);
    end
  end
  else begin
    { EZM }
    // TQREvElementFunction( AFunc ).Evaluador := Self;
    FiFo.Put(aFunc);
    nPosFunc   := FiFo.Count - 1;
    lFuncionOK := not(aFunc is TQREvElementError);
    if lFuncionOK then begin
      aString := strArg;
      repeat
        { EZM: Para encontrar ",," al principio }
        aString := Trim(aString);
        FindDelimiter(aString, DelimPos);
        if DelimPos > 0 then begin
          { EZM: Evaluar funciones con ",," o ",NIL," }
          if (DelimPos = 1) or (Trim(AnsiLowercase(Copy(aString, 1, DelimPos - 1))) = 'nil') then
          begin
            { EZM }
            Res.resFecha  := FALSE;
            Res.Kind      := resError;
            Res.strResult := 'NIL';
            FiFo.Put(TQREvElementConstant.CreateConstant(Res));
            // Res := Evaluate( ' ' )
          end
          else
            Res := Evaluate(Copy(aString, 1, DelimPos - 1));
        end
        else if Length(aString) > 0 then
          Res := Evaluate(aString);
        Delete(aString, 1, DelimPos);
      until DelimPos = 0;
    end;
    FiFo.Put(TQREvElementArgumentEnd.Create);
    LastFunction := aFunc;
    if (lFuncionOK) and TQREvElementFunction(aFunc).RegresaConstante then
      with TQREvElementFunction(aFunc) do begin
        FiFo.NextItem := nPosFunc + 1;
        Res           := ValueConstante(FiFo);
        // La funci�n SI se pudo resolver como constante
        // Quita funci�n y sus par�metros de FiFo
        // En su lugar, agrega una Constante
        if (Res.Kind <> resError) then begin
          FiFo.NextItem := nPosFunc;
          while (nPosFunc < FiFo.Count) do
            FiFo.Delete(FiFo.Count - 1);
          FiFo.Put(TQREvElementConstant.CreateConstant(Res));
          LastFunction := NIL;
        end;
      end;
  end;
end;

function TQREvaluator.EvalConstant(const strConstant: string): TQREvResult;
var
  n      : integer;
  aString: string[255];
  { EZM }
  Op                : TQREvOperator;
  Posicion, Longitud: integer;
  nFactor           : integer;
begin
  n := 1;
  { EZM }
  if (strConstant[1] = '-') then begin
    aString := Copy(strConstant, 2, MAXINT);
    nFactor := -1;
  end
  else begin
    aString := strConstant;
    nFactor := 1;
  end;

  while (n <= Length(aString)) and (aString[n] in ['0' .. '9']) do
    inc(n);
  Result.Kind := resInt;
  while ((n <= Length(aString)) and (aString[n] in ['0' .. '9', '.', 'e', 'E', '+', '-'])) do begin
    inc(n);
    Result.Kind := resDouble;
  end;
  if n - 1 <> Length(aString) then begin
    { EZM : Busca operadores L�gicos }
    if (AnsiUpperCase(strConstant) = '.T.') then begin
      Result.Kind      := resBool;
      Result.booResult := TRUE;
    end
    else if (AnsiUpperCase(strConstant) = '.F.') then begin
      Result.Kind      := resBool;
      Result.booResult := FALSE;
    end
    else begin
      FindOp1(strConstant, Op, Posicion, Longitud);
      if Posicion > 0 then
        Result := ErrorCreate(ErrorEvaluador(eeLogicos, '', strConstant))
      else
        Result := ErrorCreate(ErrorEvaluador(eeSintaxis, 'Letras en N�mero', strConstant));
    end;
  end
  else begin
    { EZM: Puse el try..except }
    try
      if Result.Kind = resInt then
        try
          Result.intResult := StrToInt(aString) * nFactor;
        except
          { EZM: Si es un entero muy grande, lo convierte a Double }
          Result.Kind := resDouble;
        end;
      if Result.Kind = resDouble then begin
        if FormatSettings.DecimalSeparator <> '.' then begin
          while Pos('.', aString) > 0 do
            aString[Pos('.', aString)] := AnsiChar(FormatSettings.DecimalSeparator);
        end;
        Result.dblResult := StrToFloat(aString) * nFactor;
      end;
    except
      Result := ErrorCreate(ErrorEvaluador(eeConstante, 'Constante Num�rica', aString));
    end;
  end;
  { EZM }
  Result.resFecha := FALSE;
  if Result.Kind = resError then
    FiFo.Put(TQREvElementError.Create(Result.strResult))
  else
    FiFo.Put(TQREvElementConstant.CreateConstant(Result));
end;

function TQREvaluator.EvalFunctionExpr(const strFunc: string): TQREvResult;
var
  argRes: TQREvResult;
  po    : integer;
  { EZM }
  strArg: String;
begin
  po := AnsiPos('(', strFunc);
  if po > 0 then begin
    if strFunc[Length(strFunc)] = ')' then begin
      // Verifica "FUNCION()", sin espacio entre los parentesis
      if (po = Length(strFunc) - 1) then
        strArg := ' ' // Obliga un espacio
      else
        strArg := Copy(strFunc, po + 1, Length(strFunc) - po - 1);
      // Trim permite dejar espacios antes del primer paren
      Result := EvalFunction(Trim(Copy(strFunc, 1, po - 1)), strArg);
      { EZM }
      if (LastFunction <> NIL) and BuscandoRequeridos then begin
        TQREvElementFunction(LastFunction).Expresion := strFunc;
      end;
    end
    else begin
      { EZM }
      // result := EvalFunction( strFunc, '');
      Result := ErrorCreate(ErrorEvaluador(eeParentesis, 'Falt� Cerrar', strFunc));
      FiFo.Put(TQREvElementError.Create(Result.strResult));
    end;
  end
  else begin
    argRes.Kind := resError;
    Result      := EvalFunction(strFunc, '');
    if (LastFunction <> NIL) and (BuscandoRequeridos) then begin
      TQREvElementFunction(LastFunction).Expresion := strFunc;
    end;
  end;
end;

function TQREvaluator.EvalFactor(strFactorExpr: string): TQREvResult;
var
  aString: string[255];
begin
  TrimString(strFactorExpr);
  aString := strFactorExpr;
  // if (AnsiLowerCase(Copy(strFactorExpr, 1, 3)) = 'not') then
  { EZM }
  // result := EvalSimpleExpr('0!' + Copy(strFactorExpr, 4, Length(strFactorExpr)))
  // result := NegateResult(EvalFactor(Copy(strFactorExpr, 4, Length(strFactorExpr))))
  // else
  case aString[1] of
    '(':
      if strFactorExpr[Length(strFactorExpr)] = ')' then
        Result := Evaluate(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
      else begin
        { EZM }
        Result := ErrorCreate(ErrorEvaluador(eeParentesis, '', strFactorExpr));
        FiFo.Put(TQREvElementError.Create(Result.strResult));
      end;
    // '-' : result := EvalSimpleExpr('0-' + Copy(strFactorExpr, 2, Length(strFactorExpr)));
    '-':
      if (Length(strFactorExpr) >= 2) and (strFactorExpr[2] in ['0' .. '9']) then
        Result := EvalConstant(strFactorExpr)
      else
        Result := EvalSimpleExpr('0-' + Copy(strFactorExpr, 2, Length(strFactorExpr)));
    { EZM
      '-' : result := NegateResult(EvalFactor(Copy(strFactorExpr, 2, Length(strFactorExpr))));
    }
    '+':
      Result := EvalFactor(Copy(strFactorExpr, 2, Length(strFactorExpr))); { EZM }
    '0' .. '9', '.':
      Result := EvalConstant(strFactorExpr);
    '''':
      if aString[Length(strFactorExpr)] = '''' then
        Result := EvalString(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
      else begin
        { EZM }
        Result := ErrorCreate(ErrorEvaluador(eeSintaxis, 'Comillas sin Terminar', strFactorExpr));
        FiFo.Put(TQREvElementError.Create(Result.strResult));
      end;
    { EZM: Cambio de Corchetes para que sean equivalentes a comillas }
    '[':
      if aString[Length(strFactorExpr)] = ']' then
        Result := EvalString(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
      else begin
        { EZM }
        Result := ErrorCreate(ErrorEvaluador(eeSintaxis, 'Comillas sin Terminar', strFactorExpr));
        FiFo.Put(TQREvElementError.Create(Result.strResult));
      end;
    '"':
      if aString[Length(strFactorExpr)] = '"' then
        Result := EvalString(Copy(strFactorExpr, 2, Length(strFactorExpr) - 2))
      else begin
        { EZM }
        Result := ErrorCreate(ErrorEvaluador(eeSintaxis, 'Comillas sin Terminar', strFactorExpr));
        FiFo.Put(TQREvElementError.Create(Result.strResult));
      end;
    'A' .. 'Z', 'a' .. 'z':
      Result := EvalFunctionExpr(strFactorExpr);
    else begin
        Result := ErrorCreate(ErrorEvaluador(eeSintaxis, 'Caracteres Raros', aString));
        FiFo.Put(TQREvElementError.Create(Result.strResult));
      end;
  end;
end;

function TQREvaluator.EvalSimpleExpr(const strSimplExpr: string): TQREvResult;
var
  Op              : TQREvOperator;
  intStart, intLen: integer;
  Res1, Res2      : TQREvResult;
begin
  FindOp2(strSimplExpr, Op, intStart, intLen);
  if intStart > 0 then begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    Res1 := EvalTerm(Copy(strSimplExpr, 1, intStart - 1));
    if Op = opMinus then
      Res2 := EvalSimpleExpr(flip(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)),
          '+', '-'))
    else
      Res2 := EvalSimpleExpr(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)))
  end
  else
    Result := EvalTerm(strSimplExpr);
end;

function TQREvaluator.EvalOrExpr(const strSimplExpr: string): TQREvResult;
var
  Op              : TQREvOperator;
  intStart, intLen: integer;
  Res1, Res2      : TQREvResult;
begin
  FindOpLogico('and ', strSimplExpr, Op, intStart, intLen);
  if intStart > 0 then begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    Res1 := EvalAndExpr(Copy(strSimplExpr, 1, intStart - 1));
    Res2 := Evaluate(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)))
  end
  else
    Result := EvalAndExpr(strSimplExpr);
end;

function TQREvaluator.EvalAndExpr(const strSimplExpr: string): TQREvResult;
var
  Op              : TQREvOperator;
  intStart, intLen: integer;
  Res1, Res2      : TQREvResult;
begin
  if (AnsiLowercase(Copy(Trim(strSimplExpr), 1, 3)) = 'not') then
    { EZM }
    Result := EvalAndExpr('0!(' + Copy(Trim(strSimplExpr), 4, MAXINT) + ')')
  else begin
    FindOp1(strSimplExpr, Op, intStart, intLen);
    if intStart > 0 then begin
      FiFo.Put(TQREvElementOperator.CreateOperator(Op));
      Res1 := EvalSimpleExpr(Copy(strSimplExpr, 1, intStart - 1));
      Res2 := EvalSimpleExpr(Copy(strSimplExpr, intStart + intLen, Length(strSimplExpr)))
    end
    else
      Result := EvalSimpleExpr(strSimplExpr);
  end;
end;

function TQREvaluator.EvalTerm(const strTermExpr: string): TQREvResult;
var
  Op              : TQREvOperator;
  intStart, intLen: integer;
  Res1, Res2      : TQREvResult;
begin
  FindOp3(strTermExpr, Op, intStart, intLen);
  if intStart > 0 then begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    Res1 := EvalFactor(Copy(strTermExpr, 1, intStart - 1));
    if Op = opDiv then
      Res2 := EvalTerm(flip(Copy(strTermExpr, intStart + intLen, Length(strTermExpr)), '*', '/'))
    else
      Res2 := EvalTerm(Copy(strTermExpr, intStart + intLen, Length(strTermExpr)));
  end
  else
    Result := EvalFactor(strTermExpr);
end;

function TQREvaluator.Evaluate(const strExpr: string): TQREvResult;
var
  Op              : TQREvOperator;
  intStart, intLen: integer;
  Res1, Res2      : TQREvResult;
begin
  // FindOp1(strExpr, op, intStart, intLen);
  FindOpLogico('or ', strExpr, Op, intStart, intLen);
  if intStart > 0 then begin
    FiFo.Put(TQREvElementOperator.CreateOperator(Op));
    { EZM }
    Res1 := EvalOrExpr(Copy(strExpr, 1, intStart - 1));
    Res2 := Evaluate(Copy(strExpr, intStart + intLen, Length(strExpr)));
  end
  else
    Result := EvalOrExpr(strExpr);
end;

procedure TQREvaluator.Prepare(const strExpr: string);
var
  Value: TQREvResult;
begin
  if Prepared then
    UnPrepare;
  FiFo := TQRFiFo.Create;
  if strExpr = '' then
    Value := Evaluate(''' ''')
  else
    Value  := Evaluate(strExpr);
  Prepared := TRUE;
end;

procedure TQREvaluator.UnPrepare;
begin
  FiFo.Free;
  Prepared := FALSE;
end;

procedure TQREvaluator.Reset;
var
  i: integer;
begin
  for i := 0 to FiFo.FiFo.Count - 1 do
    TQREvElement(FiFo.FiFo[i]).Reset;
end;

function TQREvaluator.Value: TQREvResult;
var
  F: TObject;
begin
  FiFo.Start;
  F := FiFo.Get;
  if F = nil then
    { EZM }
    Result := ErrorCreate(ErrorEvaluador(eeDesconocido, 'Vac�o', ''))
  else
    Result := TQREvElement(F).Value(FiFo);
end;

function TQREvaluator.GetIsAggreg: boolean;
var
  i: integer;
begin
  Result   := FALSE;
  for i    := 0 to FiFo.FiFo.Count - 1 do
    Result := Result or TQREvElement(FiFo.FiFo[i]).IsAggreg;
end;

function TQREvaluator.GetAggregate: boolean;
begin
  Result := FiFo.Aggreg;
end;

procedure TQREvaluator.SetAggregate(Value: boolean);
begin
  FiFo.Aggreg := Value;
end;

function TQREvaluator.Calculate(const strExpr: string): TQREvResult;
begin
  Prepare(strExpr);
  Result := Value;
  UnPrepare;
end;

function TQREvaluator.EsConstante: boolean;
var
  Elemento: TQREvElement;
begin
  if FiFo.GetFiFoCount = 1 then begin
    Elemento := TQREvElement(FiFo.FiFo[0]);
    Result   := (Elemento is TQREvElementString) or
      ((Elemento is TQREvElementConstant) and (TQREvElementConstant(Elemento).Expresion = ''));
  end
  else
    Result := FALSE;
end;

{ EZM }
{
  procedure InitQRLibrary;
  begin
  // Borra la Lista, y agrega las b�sicas //
  QRFunctionLibrary.Entries.Clear;
  RegisterQRFunction(TQREvSumFunction, 'SUM', 'SUM(<X>)|' + LoadStr(SqrSumDesc), LoadStr(SqrQSD), '3N');
  RegisterQRFunction(TQREvCountFunction, 'COUNT', 'COUNT|'+ LoadStr(SqrCountDesc), LoadStr(SqrQSD), '3');
  RegisterQRFunction(TQREvMaxFunction, 'MAXIMO', 'MAXIMO(<X>)|' + LoadStr(SqrMaxDesc), LoadStr(SqrQSD), '3V');
  RegisterQRFunction(TQREvMinFunction, 'MINIMO', 'MINIMO(<X>)|' + LoadStr(SqrMinDesc), LoadStr(SqrQSD), '3V');
  RegisterQRFunction(TQREvAverageFunction, 'AVERAGE', 'AVERAGE(<X>)|' + LoadStr(SqrAverageDesc), LoadStr(SqrQSD), '3N');
  end;
}

end.
