unit DNomParam;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
  ZetaServerDataSet,
  DZetaServerProvider;

type
  TdmNomParam = class(TDataModule)
    cdsNomParam: TServerDataSet;
  private
    { Private declarations }
    FEsNomParam: TZetaCursor;
    function GetZetaProvider: TdmZetaServerProvider;
    property oZetaProvider: TdmZetaServerProvider read GetZetaProvider;
  public
    { Public declarations }
    function EsNomParam( const sCampo: String; var sFormula: String; var eTipo: Integer ): Boolean;
  end;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZEvaluador;

{$R *.DFM}

{ TdmNomParam }

function TdmNomParam.GetZetaProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

function TdmNomParam.EsNomParam( const sCampo: string;
                                 var sFormula: string;
                                 var eTipo : Integer {TIENE QUE SER ETIPOGLOBAL}): Boolean;
 const Q_PARAM = 'SELECT NP_NOMBRE, NP_FORMULA, NP_TIPO FROM NOMPARAM '+
                 'WHERE NP_NOMBRE =:Nombre';

begin
     if FEsNomParam = NIL then
        FEsNomParam := oZetaProvider.CreateQuery(Q_PARAM);

     with FEsNomParam do
     begin
          Active := FALSE;
          oZetaProvider.ParamAsVarChar( FEsNomParam, 'NOMBRE', sCampo, 10 );
          Active := TRUE;
          RESULT := NOT EOF;
          if RESULT then
          begin
               sFormula := FieldByName('NP_FORMULA').AsString; //Se le hace una transformacion con ConvierteFechas
               {.$IFDEF RETROACTIVO}
               if ( oZetaProvider.EvaluacionTipo = teRetroactiva ) AND
                  not EsFormulaSQL( sFormula ) then
               begin
                    sFormula := ZetaServerTools.CambiaCamposRetroactivos( sFormula );
               end;
               {.$endif}
               eTipo := FieldByName('NP_TIPO').AsInteger;
          end;
          Active := FALSE;
     end;
end;

end.
