unit DInterfase;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    Db, DBClient, ZetaClientDataSet, Variants, DZetaServerProvider, ZetaTipoEntidad,
    ZetaTipoEntidadTools, ZetaCommonLists, ZetaCommonClasses, Recursos_TLB, Sistema_TLB,
    Consultas_TLB, OleServer, RegExpr, cxCheckListBox, SFE, SZCodeBaseX;

type
  TdmInterfase = class(TDataModule)
    cdsUsuarios: TZetaClientDataSet;
    cdsHistCursos: TZetaClientDataSet;
    cdsEmpresas: TZetaClientDataSet;
    cdsTempHistorial: TClientDataSet;
    cdsTempHistorialCB_CODIGO: TIntegerField;
    cdsTempHistorialPRETTYNAME: TStringField;
    cdsListaCursos: TZetaClientDataSet;
    cdsHistorial: TZetaClientDataSet;
    cdsHistorialCB_CODIGO: TIntegerField;
    cdsHistorialPRETTYNAME: TStringField;
    cdsSesionesCursos: TZetaLookupDataSet;
    cdsGruposLista: TZetaClientDataSet;
    cdsTemp: TZetaClientDataSet;
    cdsHuellasCurso: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsHistCursosAlCrearCampos(Sender: TObject);
    procedure cdsListaCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsSesionesCursosAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposListaAlAdquirirDatos(Sender: TObject);
    procedure cdsSesionesCursosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErrorEnviar, Calculo: Boolean;
    FEmpleado, FLinea: Integer;
    FEmpleadoOpteam: String;
    FDatos, FRenglon: TStringList;
    oZetaProvider: TdmZetaServerProvider;
    FNombreArchivo: string;
    FEmpresa: string;
    FNombreEmpleado: string;
    FTurnoEmpleado: string;
    FPuestoEmpleado: string;
    FFechaIniGrupo : string;
    FDrivers   : boolean;
    FAbrioSFE  : boolean;
    FServerSistema: IdmServerSistemaDisp;
    FServerConsultas: IdmServerConsultasDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    function GetSQLScript( iConsulta: Integer ): String;
    function getDatosConfiguracion(sLlave: String): String;
    function getDatosConfiguracionFloat(sLlave: String): double;
    function separarCursosMultiples(sCursos: String): String;
    function getStrValorMultiple(sCurso,sTipoRsp: String): String;
    procedure separarCursosMultiplesCompleto(sCursos: String);
    procedure SetNuevosEventos;
    procedure ValidaEnviar( DataSet: TZetaClientDataSet );
    procedure cdsEmpleadoBeforePost(DataSet: TDataSet);
    procedure cdsEmpleadoAlCrearCampos(Sender: TObject);
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
  protected
    { Protected declarations }
  public
    { Public declarations }
    //function GetHistorialCursos( const sCodigo: string; dFecha: TDateTime ): Boolean;
    function GetHistorialCursos( const dFechaRegistro: TDate ): Boolean;
    function GrabaCursoTomado( const iEmpleado: integer; const dFechaRegistro: TDate; const sCurso: string; sTipo: string ): Boolean;
    function ConectaEmpresa( const sDigito: String ): boolean;
    function AdquiereHuellas: Boolean;
    function GetChecadaEmpleado( const ID: integer ): string;
    function GetChecadaEmpleadoProx( const ID: String ): String;
    function GetDescripcion(sCurso: String): String;
    //function GetFechaIniGrupo( const fecha: String ): String;
    //function SincronizaHuellas: Boolean;
    procedure CargaListaCursos;
    procedure EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = TRUE);
    procedure EscribeErrorExterno( const sMensaje: string );
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    property ServerConsultas : IdmServerConsultasDisp read GetServerConsultas;
    property NombreEmpleado : string read FNombreEmpleado write FNombreEmpleado;
    property TurnoEmpleado : string read FTurnoEmpleado write FTurnoEmpleado;
    property PuestoEmpleado : string read FPuestoEmpleado write FPuestoEmpleado;
    property FechaIniGrupo : string read FFechaIniGrupo write FFechaIniGrupo;
  end;

var
  dmInterfase: TdmInterfase;
  lCursosMultiple: tstringlist;

const
     // Consultas SQL
     K_GET_REGISTROS_CURSO = 1;
     K_GET_EMPRESA         = 2;
     K_GET_INFO_EMPLEADO   = 3;
     K_GET_LISTA_CURSOS    = 4;
     K_GET_GRUPOS_CURSOS   = 5;
     K_GET_CURSO_GRUPO     = 6;
     K_GET_RESERVA         = 7;
     K_GET_CURSO_ASIS      = 8;
     K_GET_APROBADO        = 9;
     K_GET_SESION          = 10;
     K_GET_HUELLAS_CURSOS  = 11;
     K_GET_CHECADA_EMPLEADO = 12;
     K_GET_INSCRITOS       = 13;
     K_GET_CHECADA_PROX    = 14;
     K_GET_LISTA_GRUPOS    = 15;

implementation

uses IniFiles,
     dProcesos,
     dCliente,
     dRecursos,
     dTablas,
     dCatalogos,
     dConsultas,
     dGlobal,
     dSistema,
     dNomina,
     dAsistencia,
     ZGlobalTress,
     FTressShell,
     ZAsciiTools,
     ZetaAsciiFile,
     ZBaseThreads,
     ZBaseSelectGrid,
     ZetaCommonTools,
     dSuperAscii,
     StrUtils,
     ZReconcile,
     FAutoClasses,
     ZAccesosMgr,
     ZetaDialogo,
     ZAccesosTress;

{$R *.DFM}

{ TdmInterfase }
procedure TdmInterfase.DataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     SetNuevosEventos;
     FEmpresa := VACIO;
     cdsHistorial.CreateTempDataset;
     FDrivers  := LoadDLL;
     FAbrioSFE := FALSE;
end;

procedure TdmInterfase.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

function TdmInterfase.GetHistorialCursos(const dFechaRegistro: TDate): Boolean;
begin
     //cdsHistCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_REGISTROS_CURSO ), [ EntreComillas( sCodigo ), EntreComillas( FechaToStr( dFecha ) ) ] ) );
     cdsHistCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_INFO_EMPLEADO ), [ FEmpleado ] ) );
     if zStrToBool(cdsHistCursos.FieldByName( 'CB_ACTIVO' ).AsString) then
     begin
         FNombreEmpleado := cdsHistCursos.FieldByName( 'PRETTYNAME' ).AsString;
         FTurnoEmpleado := cdsHistCursos.FieldByName( 'TU_DESCRIP' ).AsString;
         FPuestoEmpleado := cdsHistCursos.FieldByName( 'PU_DESCRIP' ).AsString;
         if not cdsHistorial.Locate( 'CB_CODIGO;PRETTYNAME', VarArrayOf( [ cdsHistCursos.FieldByName( 'CB_CODIGO' ).AsInteger,cdsHistCursos.FieldByName( 'PRETTYNAME' ).AsString ] ), [] ) then
         begin
              cdsHistorial.Append;
              cdsHistorial.FieldByName( 'CB_CODIGO' ).AsInteger := cdsHistCursos.FieldByName( 'CB_CODIGO' ).AsInteger;
              cdsHistorial.FieldByName( 'PRETTYNAME' ).AsString := cdsHistCursos.FieldByName( 'PRETTYNAME' ).AsString;
              cdsHistorial.Post;
         end;
         Result := TRUE;
     end
     else
     begin
        if (cdsHistCursos.FieldByName( 'CB_FEC_BAJ' ).AsDateTime >= dFechaRegistro) then
        begin
            FNombreEmpleado := cdsHistCursos.FieldByName( 'PRETTYNAME' ).AsString;
            FTurnoEmpleado := cdsHistCursos.FieldByName( 'TU_DESCRIP' ).AsString;
            FPuestoEmpleado := cdsHistCursos.FieldByName( 'PU_DESCRIP' ).AsString;
            if not cdsHistorial.Locate( 'CB_CODIGO;PRETTYNAME', VarArrayOf( [ cdsHistCursos.FieldByName( 'CB_CODIGO' ).AsInteger,cdsHistCursos.FieldByName( 'PRETTYNAME' ).AsString ] ), [] ) then
            begin
                cdsHistorial.Append;
                cdsHistorial.FieldByName( 'CB_CODIGO' ).AsInteger := cdsHistCursos.FieldByName( 'CB_CODIGO' ).AsInteger;
                cdsHistorial.FieldByName( 'PRETTYNAME' ).AsString := cdsHistCursos.FieldByName( 'PRETTYNAME' ).AsString;
                cdsHistorial.Post;
            end;
            Result := TRUE;
        end
        else
        begin
            Result := FALSE;
        end;
     end;
end;

// Manda mensaje a la bit�cora de errores.
procedure TdmInterfase.EscribeError(const sMensaje: string; const iEmpleado: Integer = 0; const iLinea: Integer = 0; lBandera: Boolean = TRUE);
begin
     if lBandera then
     begin
          { Se cambia la bandera FHuboErrores para indicar existen errores en el proceso }
          if ( not FHuboErrores ) then
             FHuboErrores := TRUE;
     end;
     Application.ProcessMessages
end;

// Este prodecimiento nos sirve para invocarse de unidades externas a la interfaz
// el cual a su vez env�a el mensaje al EscribeErro utilizando el cliente actual.
procedure TdmInterfase.EscribeErrorExterno( const sMensaje: string );
begin
     EscribeError( sMensaje, FEmpleado, FLinea );
end;

function TdmInterfase.GetChecadaEmpleado(const ID: integer): string;
begin
     cdsTemp.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_CHECADA_EMPLEADO ), [ ID ] ) );
     Result := cdsTemp.FieldByName( 'CHECADA' ).AsString;
end;

function TdmInterfase.GetChecadaEmpleadoProx(const ID: String): String;
begin
    cdsTemp.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_CHECADA_PROX ), [ EntreComillas(ID) ] ) );
    Result := cdsTemp.FieldByName( 'CHECADA' ).AsString;
end;

function TdmInterfase.GetDescripcion(sCurso: String): String;
begin
    cdsListaCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa,
                                                                       Format( GetSQLScript( K_GET_LISTA_CURSOS ),
                                                                       [ EntreComillas( sCurso ) ] ) );
    Result := cdsListaCursos.FieldByName( 'CU_NOMBRE' ).AsString;
end;

function TdmInterfase.getDatosConfiguracion(sLlave: String): String;
var
   appINI : TIniFile;
begin
     appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
     try
        Result := appINI.ReadString('DATOS', sLlave, VACIO);
     finally
            appINI.Free;
     end;
end;

function TdmInterfase.getDatosConfiguracionFloat(sLlave: String): double;
var
   appINI : TIniFile;
begin
     appINI := TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini')) ;
     try
        Result := appINI.ReadFloat('DATOS', sLlave, 0.0);
     finally
            appINI.Free;
     end;
end;

procedure TdmInterfase.SetNuevosEventos;
begin
     with dmCliente.cdsEmpleado do
     begin
          BeforePost := self.cdsEmpleadoBeforePost;
          AlCrearCampos := self.cdsEmpleadoAlCrearCampos;
          OnReconcileError := self.cdsEmpleadoReconcileError;
     end;
end;

procedure TdmInterfase.cdsEmpleadoReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if PK_Violation( E ) and
        ( UpdateKind = ukInsert ) then // Solo en la Alta del Empleado //
        EscribeError( 'No se pudo registrar el alta, el n�mero de empleado ya existe', FEmpleado, FLinea )
     else
        EscribeError( E.Message, FEmpleado, FLinea );
     Action := raCancel;
end;

procedure TdmInterfase.cdsGruposListaAlAdquirirDatos(Sender: TObject);
begin
     cdsGruposLista.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, GetSQLScript( K_GET_CURSO_GRUPO ) );
end;

procedure TdmInterfase.cdsHistCursosAlCrearCampos(Sender: TObject);
begin
     with cdsHistCursos do
     begin
          MaskFecha( 'KC_FEC_TOM' );
     end;
end;

procedure TdmInterfase.cdsListaCursosAlAdquirirDatos(Sender: TObject);
const
     K_MENSAJE = '%s = %s ' + CR_LF + 'Maestro: %s' + CR_LF + 'Revisi�n: %s';
var
   sCursos, sSQL, sDato, sMaestro, sRevision, sGrupo, sMaestroId: string;
   charArray : Array[0..0] of Char;
   strArray : TArray<System.string>;
   i : integer;
begin
     dmCatalogos.cdsMaestros.Conectar;
     if zStrToBool( getDatosConfiguracion( 'INDIVIDUAL' ) ) then
     begin
          sCursos := getDatosConfiguracion( 'CURSO' );
          if strLleno( sCursos ) then
             cdsListaCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa,
                                                                       Format( GetSQLScript( K_GET_LISTA_CURSOS ),
                                                                       [ EntreComillas( sCursos ) ] ) );
          if not cdsListaCursos.IsEmpty then
          begin
               while not cdsListaCursos.EOF do
               begin
                    dmCatalogos.cdsMaestros.Locate( 'MA_CODIGO', VarArrayOf( [ getDatosConfiguracion( 'MAESTRO' ) ] ), [] );
                    sMaestro := dmCatalogos.cdsMaestros.FieldByName( 'MA_NOMBRE' ).AsString;
                    sDato := Format( K_MENSAJE, [ cdsListaCursos.FieldByName( 'CU_CODIGO' ).AsString,
                                                  cdsListaCursos.FieldByName( 'CU_NOMBRE' ).AsString,
                                                  sMaestro, getDatosConfiguracion( 'REVISION' ) ] );
                    if not ( cdsListaCursos.State in [ dsEdit, dsInsert ] ) then
                       cdsListaCursos.Edit;
                    cdsListaCursos.FieldByName( 'RESULTADO' ).AsString := sDato;
                    cdsListaCursos.Post;
                    cdsListaCursos.Next
               end;
          end;
     end;
     if zStrToBool( getDatosConfiguracion( 'MULTIPLE' ) ) then
     begin
          sCursos := getDatosConfiguracion( 'CURSOMULTIPLE' );
          if zStrToBool( getDatosConfiguracion( 'VALORESGLOBALES' ) ) then
          begin                                                   // si esta habilitado el global
            sCursos := separarCursosMultiples( sCursos );
            if strLleno( sCursos ) then
            begin
                 sCursos := StrTransAll( sCursos, ',', ''',''' );
                 sCursos := ansiUpperCase( sCursos );
                 sCursos := EntreComillas( sCursos );
                 sSQL := Format( GetSQLScript( K_GET_LISTA_CURSOS ), [ sCursos ] );
                 cdsListaCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, sSQL );

                 if strLleno( getDatosConfiguracion( 'MAESTROMULTIPLE' ) ) then
                 begin
                    dmCatalogos.cdsMaestros.Locate( 'MA_CODIGO', VarArrayOf( [ getDatosConfiguracion( 'MAESTROMULTIPLE' ) ] ), [] );
                    sMaestro := dmCatalogos.cdsMaestros.FieldByName( 'MA_NOMBRE' ).AsString;
                 end
                 else
                    sMaestro := VACIO;
                 sRevision := getDatosConfiguracion( 'REVISIONMULTIPLE' );

                 while not cdsListaCursos.EOF do
                 begin
                   if not cdsListaCursos.IsEmpty then
                   begin
                     //sMaestro := cdsListaCursos.FieldByName( 'MA_NOMBRE' ).AsString;
                     //sRevision := cdsListaCursos.FieldByName( 'CU_REVISIO' ).AsString;
                     sDato := Format( K_MENSAJE, [ cdsListaCursos.FieldByName( 'CU_CODIGO' ).AsString,
                                                   cdsListaCursos.FieldByName( 'CU_NOMBRE' ).AsString,
                                                   sMaestro, sRevision ] );
                     if not ( cdsListaCursos.State in [ dsEdit, dsInsert ] ) then
                        cdsListaCursos.Edit;
                     cdsListaCursos.FieldByName( 'RESULTADO' ).AsString := sDato;
                     cdsListaCursos.Post;
                     cdsListaCursos.Next
                   end;
                 end;
            end;
          end
          else
          begin
            separarCursosMultiplesCompleto( sCursos );
            sCursos := separarCursosMultiples( sCursos );
            sCursos := StrTransAll( sCursos, ',', ''',''' );
            sCursos := ansiUpperCase( sCursos );
            sCursos := EntreComillas( sCursos );
            sSQL := Format( GetSQLScript( K_GET_LISTA_CURSOS ), [ sCursos ] );
            cdsListaCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, sSQL );
            if not cdsListaCursos.IsEmpty then
            begin
                while not cdsListaCursos.EOF do
                begin
                     sMaestro := cdsListaCursos.FieldByName( 'MA_NOMBRE' ).AsString;
                     sRevision := cdsListaCursos.FieldByName( 'CU_REVISIO' ).AsString;
                     sDato := Format( K_MENSAJE, [ cdsListaCursos.FieldByName( 'CU_CODIGO' ).AsString,
                                                   cdsListaCursos.FieldByName( 'CU_NOMBRE' ).AsString,
                                                   sMaestro, sRevision ] );
                     if not ( cdsListaCursos.State in [ dsEdit, dsInsert ] ) then
                        cdsListaCursos.Edit;
                     cdsListaCursos.FieldByName( 'RESULTADO' ).AsString := sDato;
                     cdsListaCursos.Post;
                     cdsListaCursos.Next
                end;
            end;
          end;
     end;
     if zStrToBool( getDatosConfiguracion( 'GRUPOS' ) ) then
     begin
          sGrupo := getDatosConfiguracion( 'GRUPO' );
          if strLleno( sGrupo ) then
          begin
              cdsGruposLista.Conectar;
              if cdsGruposLista.Locate( 'SE_FOLIO', VarArrayOf( [ sGrupo ] ), [] ) then
              begin
                cdsListaCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa,Format( GetSQLScript( K_GET_LISTA_GRUPOS ), [ EntreComillas( sGrupo ) ] ) );
              end;
          end;
          if not cdsListaCursos.IsEmpty then
          begin
               while not cdsListaCursos.EOF do
               begin
                    FFechaIniGrupo := FechaToStr( cdsListaCursos.FieldByName( 'SE_FEC_INI' ).AsDateTime );
                    sDato := Format( K_MENSAJE, [ cdsListaCursos.FieldByName( 'SE_FOLIO' ).AsString,
                                                  cdsListaCursos.FieldByName( 'CU_NOMBRE' ).AsString,
                                                  cdsListaCursos.FieldByName( 'MA_NOMBRE' ).AsString,
                                                  cdsListaCursos.FieldByName( 'SE_REVISIO' ).AsString] );
                    if not ( cdsListaCursos.State in [ dsEdit, dsInsert ] ) then
                       cdsListaCursos.Edit;
                    cdsListaCursos.FieldByName( 'RESULTADO' ).AsString := sDato;
                    cdsListaCursos.Post;
                    cdsListaCursos.Next
               end;
          end;
     end;
end;

procedure TdmInterfase.cdsEmpleadoBeforePost(DataSet: TDataSet);
begin
     dmCliente.cdsEmpleadoBeforePost( DataSet );
end;

procedure TdmInterfase.CargaListaCursos;
begin
     cdsListaCursos.Refrescar;
end;

procedure TdmInterfase.cdsEmpleadoAlCrearCampos(Sender: TObject);
begin
     dmCliente.cdsEmpleadoAlCrearCampos( Sender );
end;

procedure TdmInterfase.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError;
          UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sError: String;
begin
     with E do
     begin
          if PK_VIOLATION( E ) then //if ( ErrorCode = 3604 ) or ( Pos( 'PRIMARY', Message ) > 0 ) then
             sError := '� C�digo Ya Existe !'
          else
              sError := Message;
          if ( Context <> '' ) then
              sError := sError + CR_LF + Context;
     end;
     EscribeError( sError, DataSet.FieldByName( 'CB_CODIGO' ).AsInteger, FLinea );
     Action := raCancel;
end;

procedure TdmInterfase.cdsSesionesCursosAlAdquirirDatos(Sender: TObject);
begin
  cdsSesionesCursos.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, GetSQLScript( K_GET_GRUPOS_CURSOS ) );
end;


procedure TdmInterfase.cdsSesionesCursosGetRights(Sender: TZetaClientDataSet;
  const iRight: Integer; var lHasRights: Boolean);
begin
      lHasRights := false;
end;

procedure TdmInterfase.ValidaEnviar(DataSet: TZetaClientDataSet);
begin
     try
        DataSet.Enviar;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Captura de cursos', Format( 'Error al grabar informaci�n de curso tomado para empleado [%d] .', [ FEmpleado ] ), 0 );
                DataSet.CancelUpdates;
           end;
     end;
end;

// Regresa el texto de la consulta indicada
function TdmInterfase.GetSQLScript(iConsulta: Integer): String;
begin
     case iConsulta of
          K_GET_REGISTROS_CURSO: Result := 'select KC.CB_CODIGO, PRETTYNAME, KC_FEC_TOM, KC.CU_CODIGO, C.CU_NOMBRE, KC_REVISIO, KC_HORAS, KC_EVALUA, SE_FOLIO from KARCURSO KC '+
                                           'left outer join CURSO C on C.CU_CODIGO = KC.CU_CODIGO '+
                                           'left outer join COLABORA CO on CO.CB_CODIGO = KC.CB_CODIGO '+
                                           'where ( KC.CU_CODIGO in ( %s ) ) and ( KC.KC_FEC_TOM = %s )';
          K_GET_EMPRESA: Result := 'select CM_CODIGO from V_COMPANY where ( CM_DIGITO = %s )';
          K_GET_INFO_EMPLEADO: Result := 'select C.CB_CODIGO, C.PRETTYNAME, C.CB_TURNO, C.CB_PUESTO, TU.TU_DESCRIP, PU.PU_DESCRIP, C.CB_ACTIVO, C.CB_FEC_BAJ,IM.IM_BLOB '+
                                         'from COLABORA C '+
                                         'left outer join TURNO TU on C.CB_TURNO = TU.TU_CODIGO '+
                                         'left outer join PUESTO PU on C.CB_PUESTO = PU.PU_CODIGO '+
                                         'left outer join IMAGEN IM on C.CB_CODIGO = IM.CB_CODIGO and IM.IM_TIPO = ''FOTO''' +
                                         'where ( C.CB_CODIGO = %d )';
          K_GET_LISTA_CURSOS: Result := 'select  CAST( '''' as varchar(500) ) as RESULTADO, C.CU_CODIGO, C.CU_NOMBRE, C.MA_CODIGO, MA.MA_NOMBRE, C.CU_REVISIO from CURSO C '+
                                        'left outer join  MAESTRO MA on C.MA_CODIGO = MA.MA_CODIGO  ' +
                                        'where ( C.CU_CODIGO in ( %s ) )';
          K_GET_GRUPOS_CURSOS: Result := 'select SE.SE_FOLIO, SE.CU_CODIGO, CU.CU_NOMBRE from SESION SE '+
                                         'left outer join CURSO CU on CU.CU_CODIGO = SE.CU_CODIGO';
          K_GET_CURSO_GRUPO: Result := 'select SE.SE_FOLIO, SE.CU_CODIGO, SE.MA_CODIGO, SE.SE_HORAS, CU.CU_NOMBRE, CU.CU_REVISIO from SESION SE '+
                                        'left outer join CURSO CU on CU.CU_CODIGO = SE.CU_CODIGO';
          K_GET_RESERVA: Result := 'select * from RESERVA where ( SE_FOLIO = %s )';
          K_GET_CURSO_ASIS: Result := 'select * from CUR_ASIS where ( SE_FOLIO = %s ) and ( RV_FOLIO = %s )';
          K_GET_APROBADO: Result := 'select KA.CB_CODIGO, KA.CU_CODIGO, KA.CB_CLASIFI, KA.CB_TURNO, KA.KC_EVALUA, KA.KC_FEC_TOM, '
                                    + 'KA.KC_FEC_FIN, KA.KC_HORAS, KA.CB_PUESTO, KA.CB_NIVEL1, KA.CB_NIVEL2, KA.CB_NIVEL3, KA.CB_NIVEL4, '
                                    + 'KA.CB_NIVEL5, KA.CB_NIVEL6, KA.CB_NIVEL7, KA.CB_NIVEL8, KA.CB_NIVEL9, KA.MA_CODIGO, KA.KC_REVISIO,KA.KC_EST, '
                                    + 'KA.SE_FOLIO, 0 as OPERACION,  ' + K_PRETTYNAME + ' as PRETTYNAME '
                                    + 'from KARCURSO KA LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = KA.CB_CODIGO ' + 'WHERE KA.SE_FOLIO = %s ';
          K_GET_SESION: Result := 'select SE.SE_FOLIO, SE.CU_CODIGO, SE.MA_CODIGO, SE.SE_LUGAR, SE.SE_FEC_INI, SE.SE_HOR_INI, SE.SE_FEC_FIN, SE.SE_HOR_FIN, '
                                    + 'SE.SE_HORAS, SE.SE_CUPO, SE.SE_COMENTA, SE.SE_REVISIO, SE.SE_COSTO1, SE.SE_COSTO2, SE.SE_COSTO3, SE.SE_STATUS, SE.US_CODIGO, SE.SE_EST, '
                                    + '( select count(*) from KARCURSO where SE_FOLIO = SE.SE_FOLIO ) as SE_APROBADO, '
                                    + '( select count(*) from INSCRITO where SE_FOLIO = SE.SE_FOLIO ) as SE_INSCRITO '
                                    + 'from SESION SE where ( SE_FOLIO = %s )';
          K_GET_HUELLAS_CURSOS: Result := 'select ID_NUMERO, HU_INDICE, HUELLA, CM_CODIGO, CB_CODIGO from VHGTI order by ID_NUMERO';
          K_GET_CHECADA_EMPLEADO: Result := 'select Top 1 COALESCE( CHECADA,  '''' ) CHECADA from VHGTI where ( ID_NUMERO = %d )';
          K_GET_INSCRITOS: Result := 'select I.SE_FOLIO, I.CB_CODIGO, I.IC_STATUS, I.IC_FEC_BAJ, I.IC_COMENTA, I.IC_EVA_1, I.IC_EVA_2, I.IC_EVA_3, I.IC_EVA_FIN, '
                                    + 'I.US_CODIGO, I.IC_FEC_INS, I.IC_HOR_INS, ' + K_PRETTYNAME + ' as PRETTYNAME '
                                    + 'FROM INSCRITO I LEFT OUTER JOIN COLABORA ON COLABORA.CB_CODIGO = I.CB_CODIGO WHERE I.SE_FOLIO = %s ';
          K_GET_CHECADA_PROX: Result := 'select Top 1 COALESCE( CHECADA, '''' ) CHECADA from VPGTI where (ID_NUMERO = %s)';
          K_GET_LISTA_GRUPOS: Result := 'select CAST( '''' as varchar(400) ) as RESULTADO, SE.SE_FOLIO, CU.CU_CODIGO, CU.CU_NOMBRE, SE.MA_CODIGO, MA.MA_NOMBRE, SE.SE_REVISIO, SE.SE_FEC_INI from SESION SE '
                                    + 'left outer join MAESTRO MA on SE.MA_CODIGO = MA.MA_CODIGO '
                                    + 'left outer join CURSO CU on SE.CU_CODIGO = CU.CU_CODIGO where ( SE.SE_FOLIO in ( %s ) )'
     else
          Result := VACIO;
     end;
end;

function TdmInterfase.GrabaCursoTomado(const iEmpleado: integer; const dFechaRegistro: TDate; const sCurso: string; sTipo: string ): Boolean;
var
   FRenglon: TStringList;
   i,iReserva: integer;
   sRevision, sMaestro, sResevacion, sGrupo: string;
   rHoras: currency;
   strArray : TArray<System.string>;
   charArray  : Array[0..0] of Char;
begin
     Result := TRUE;
     FEmpleado := iEmpleado;
     dmCatalogos.cdsCursos.Conectar;
     dmCatalogos.cdsMaestros.Conectar;
     if dmCliente.SetEmpleadoNumero( FEmpleado ) then
     begin
          with dmRecursos.cdsHisCursos do
          begin
               Refrescar;
               if (sTipo = 'I') then
               begin
                    if not Locate( 'CB_CODIGO;CU_CODIGO;KC_FEC_TOM', VarArrayOf( [ FEmpleado, sCurso, dFechaRegistro ] ), [] ) then
                    begin
                        Append;
                        FieldByName('CU_CODIGO').AsString := sCurso;
                        FieldByName( 'KC_FEC_TOM' ).AsDateTime := dFechaRegistro;
                        FieldByName( 'KC_REVISIO' ).AsString := getDatosConfiguracion( 'REVISION' );;
                        FieldByName( 'KC_HORAS' ).AsFloat := getDatosConfiguracionFloat( 'HORAS' );
                        FieldByName( 'MA_CODIGO' ).AsString := getDatosConfiguracion( 'MAESTRO' );
                        ValidaEnviar( dmRecursos.cdsHisCursos );
                        if not GetHistorialCursos( dFechaRegistro ) then
                            Result := FALSE;
                    end
                    else
                    begin
                        ZetaDialogo.ZError( 'Captura de cursos', format( 'El empleado ya cuenta con el curso [%s] tomado.',[sCurso]), 0 );
                        Result := FALSE;
                    end;
               end;
               if (sTipo = 'M') then
               begin
                    FRenglon := TStringList.Create;
                    charArray[0] := '|';
                    try
                       FRenglon.CommaText := sCurso; //separa por ,
                       for i := 0 to ( FRenglon.Count - 1) do
                       begin
                            strArray := FRenglon[i].Split(charArray);    ////
                            if not Locate( 'CB_CODIGO;CU_CODIGO;KC_FEC_TOM', VarArrayOf( [ FEmpleado, strArray[ 0 ], dFechaRegistro ] ), [] ) then
                            begin
                                 dmCatalogos.cdsCursos.Locate( 'CU_CODIGO', VarArrayOf( [ strArray[ 0 ] ] ), [] );

                                 if zStrToBool(getDatosConfiguracion('VALORESGLOBALES')) then
                                 begin
                                    sRevision := getDatosConfiguracion( 'REVISIONMULTIPLE' );
                                    rHoras := getDatosConfiguracionFloat( 'HORASMULTIPLE' );
                                    sMaestro := getDatosConfiguracion( 'MAESTROMULTIPLE' );
                                 end
                                 else
                                 begin
                                    sRevision := dmCatalogos.cdsCursos.FieldByName( 'CU_REVISIO' ).AsString;
                                    rHoras := dmCatalogos.cdsCursos.FieldByName('CU_HORAS').AsFloat;
                                    sMaestro := dmCatalogos.cdsCursos.FieldByName( 'MA_CODIGO' ).AsString;
                                 end;

                                 Append;
                                 FieldByName('CU_CODIGO').AsString  := strArray[ 0 ];
                                 FieldByName( 'KC_FEC_TOM' ).AsDateTime := dFechaRegistro;
                                 FieldByName( 'KC_REVISIO').AsString := sRevision;
                                 FieldByName( 'KC_HORAS' ).AsFloat := rHoras;
                                 FieldByName( 'MA_CODIGO' ).AsString := sMaestro;
                                 ValidaEnviar( dmRecursos.cdsHisCursos );
                                 if not GetHistorialCursos( dFechaRegistro ) then
                                    Result := FALSE;
                            end
                            else
                            begin
                                ZetaDialogo.ZError( 'Captura de cursos', format( 'El empleado ya cuenta con el curso [%s] tomado.',[strArray[ 0 ]]), 0 );
                                Result := FALSE;
                            end;
                       end;
                    finally
                           FreeAndNil( FRenglon );
                    end;
               end;
          end;
          if (sTipo = 'G') then
          begin
               sGrupo := sCurso;
               if zStrToBool(getDatosConfiguracion( 'ASISTENCIA' )) then
               begin
                      cdsTemp.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_RESERVA ), [ EntreComillas( sGrupo ) ] ) );
                      iReserva := cdsTemp.FieldByName( 'RV_FOLIO' ).AsInteger;
                      sResevacion := cdsTemp.FieldByName( 'RV_FOLIO' ).AsString;
                      if strLleno( sResevacion ) then
                      begin
                          dmRecursos.cdsListaAsistencia.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa,Format( GetSQLScript( K_GET_CURSO_ASIS ),
                                                                    [ EntreComillas( sGrupo ) , EntreComillas( sResevacion ) ] ) );


                          dmRecursos.cdsSesion.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_SESION ), [ EntreComillas( sGrupo ) ] ) );
                          dmRecursos.GetListaAsistencia(iReserva,TRUE);

                          if not dmRecursos.cdsListaAsistencia.IsEmpty then
                          begin
                              if dmRecursos.cdsListaAsistencia.Locate( 'CB_CODIGO;SE_FOLIO', VarArrayOf( [ FEmpleado, sGrupo ] ), [] ) then
                              begin
                                if not zStrToBool(dmRecursos.cdsListaAsistencia.FieldByName('CS_ASISTIO').AsString) then
                                begin
                                    if not ( dmRecursos.cdsListaAsistencia.State in [ dsEdit, dsInsert ] ) then
                                        dmRecursos.cdsListaAsistencia.Edit;
                                    dmRecursos.cdsListaAsistencia.FieldByName( 'CS_ASISTIO' ).AsString := K_GLOBAL_SI;
                                    ValidaEnviar( dmRecursos.cdsListaAsistencia );
                                    if not GetHistorialCursos( dFechaRegistro ) then
                                        Result := FALSE;
                                end
                                else
                                begin
                                     ZetaDialogo.ZError( 'Captura de cursos', Format( 'Ya est� marcada la asistencia del empleado [%d] a este grupo.', [ FEmpleado ] ), 0 );
                                     Result := FALSE;
                                end;
                              end
                              else
                              begin
                                ZetaDialogo.ZError( 'Captura de cursos', Format( 'El empleado [%d] no est� inscrito a este grupo.', [ FEmpleado ] ), 0 );
                                Result := FALSE;
                              end;
                          end
                          else
                          begin
                             ZetaDialogo.ZError( 'Captura de cursos',  'No existen empleados inscritos a este grupo.', 0 );
                             Result := FALSE;
                          end;
                      end
                      else
                      begin
                          ZetaDialogo.ZError( 'Captura de cursos', 'No existe reservacion para el grupo selecionado.', 0 );
                          Result := FALSE;
                      end;
               end;
               if zStrToBool(getDatosConfiguracion( 'APROBADOS' )) then
               begin
                  dmRecursos.cdsAprobados.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format(GetSQLScript( K_GET_APROBADO ),[EntreComillas(sGrupo)]));
                  with dmRecursos.cdsAprobados do
                  begin
                     if not Locate( 'CB_CODIGO;SE_FOLIO', VarArrayOf( [ FEmpleado, sGrupo ] ), [] ) then
                     begin
                        dmRecursos.cdsSesion.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_SESION ), [ EntreComillas( sGrupo ) ] ) );
                        //FFechaIniGrupo := dmRecursos.cdsSesion.FieldByName('SE_FEC_INI').AsString;
                        Append;
                        FieldByName('CB_CODIGO').AsInteger := FEmpleado;
                        FieldByName('KC_EVALUA').AsFloat := 100;
                        ValidaEnviar( dmRecursos.cdsAprobados );
                        if not GetHistorialCursos( dFechaRegistro ) then
                            Result := FALSE;
                     end
                     else
                     begin
                         ZetaDialogo.ZError( 'Captura de cursos', Format( 'El empleado [%d] ya fue aprobado con anterioridad.',[FEmpleado] ), 0 );
                         Result := FALSE;
                     end;
                  end;
               end;
               if zStrToBool(getDatosConfiguracion( 'INSCRITOS' )) then
               begin
                  dmRecursos.cdsSesion.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_SESION ), [ EntreComillas( sGrupo ) ] ) );
                  with dmRecursos.cdsInscritos do
                  begin
                      Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format(GetSQLScript( K_GET_INSCRITOS ),[EntreComillas(sGrupo)] ) );
                      if not Locate( 'CB_CODIGO;SE_FOLIO', VarArrayOf( [ FEmpleado, sGrupo ] ), [] ) then
                      begin
                          if not ( State in [ dsEdit, dsInsert ] ) then
                              Edit;
                          Append;
                          FieldByName( 'CB_CODIGO' ).AsInteger := FEmpleado;
                          FieldByName( 'IC_STATUS' ).AsInteger := Ord( siActivo );
                          FieldByName( 'IC_FEC_INS' ).AsDateTime := dFechaRegistro;
                          FieldByName( 'IC_HOR_INS' ).AsString := FormatDateTime( 'hhnn', Now );
                          ValidaEnviar( dmRecursos.cdsInscritos );
                          if not GetHistorialCursos( dFechaRegistro ) then
                              Result := FALSE;
                      end
                      else
                      begin
                        Result := FALSE;
                      end;
                  end;
                  cdsTemp.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_RESERVA ), [ EntreComillas( sGrupo ) ] ) );
                  iReserva := cdsTemp.FieldByName( 'RV_FOLIO' ).AsInteger;
                  sResevacion := cdsTemp.FieldByName( 'RV_FOLIO' ).AsString;
                  if strLleno( sResevacion ) then
                  begin
                      dmRecursos.cdsListaAsistencia.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa,Format( GetSQLScript( K_GET_CURSO_ASIS ),
                                                                    [ EntreComillas( sGrupo ) , EntreComillas( sResevacion ) ] ) );
                      //recargar lista asistencia
                      dmRecursos.GetListaAsistencia(iReserva,TRUE);
                      //poner asistencia
                      if dmRecursos.cdsListaAsistencia.Locate( 'CB_CODIGO;SE_FOLIO', VarArrayOf( [ FEmpleado, sGrupo ] ), [] ) then
                      begin
                        if not zStrToBool(dmRecursos.cdsListaAsistencia.FieldByName('CS_ASISTIO').AsString) then
                        begin
                            if not ( dmRecursos.cdsListaAsistencia.State in [ dsEdit, dsInsert ] ) then
                                dmRecursos.cdsListaAsistencia.Edit;
                            dmRecursos.cdsListaAsistencia.FieldByName( 'CS_ASISTIO' ).AsString := K_GLOBAL_SI;
                            ValidaEnviar( dmRecursos.cdsListaAsistencia );
                            if not GetHistorialCursos( dFechaRegistro ) then
                                Result := FALSE;
                        end
                        else
                        begin
                             ZetaDialogo.ZError( 'Captura de cursos', Format( 'Ya est� marcada la asistencia del empleado [%d] a este grupo.', [ FEmpleado ] ), 0 );
                             Result := FALSE;
                        end;
                      end
                      else
                      begin
                        ZetaDialogo.ZError( 'Captura de cursos', Format( 'El empleado [%d] no est� inscrito a este grupo.', [ FEmpleado ] ), 0 );
                        Result := FALSE;
                      end;
                  end
                  else
                  begin
                      ZetaDialogo.ZError( 'Captura de cursos', 'No existe reservacion para el grupo selecionado.', 0 );
                  end;
               end;
          end;
     end
     else
     begin
          ZetaDialogo.ZError( 'Captura de cursos', Format( 'Empleado [%d] no existe en sistema TRESS.', [ FEmpleado ] ), 0 );
          Result := FALSE;
     end;
end;

function TdmInterfase.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;

function TdmInterfase.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmInterfase.ConectaEmpresa( const sDigito: String ): boolean;
var
   Empresa : String;
begin
     cdsEmpresas.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, Format( GetSQLScript( K_GET_EMPRESA ), [ EntreComillas( sDigito ) ] ) );
     //cdsEmpresas.Data := ServerConsultas.GetQueryGralTodos( oZetaProvider.Comparte, Format( GetSQLScript( K_GET_EMPRESA ), [ EntreComillas( sDigito ) ] ) );
     Result := Not cdsEmpresas.IsEmpty;
     if Result then
     begin
          Empresa := cdsEmpresas.FieldByName( 'CM_CODIGO' ).AsString;
          if strLleno(Empresa) then
          begin
               if dmCliente.FindCompany( Empresa ) then
               begin
                   if strVacio( FEmpresa ) or ( FEmpresa <> Empresa ) then
                   begin
                        FEmpresa := Empresa;
                        dmCliente.SetCompany;
                        dmCliente.SetCacheLookup;
                        dmCliente.InitActivosEmpleado;
                        dmCliente.InitArrayTPeriodo;
                        dmCliente.InitActivosPeriodo;
                        Global.Conectar;
                   end;
               end;
          end;
     end;
end;

function TdmInterfase.AdquiereHuellas : Boolean;
begin
     cdsHuellasCurso.Data := ServerConsultas.GetQueryGralTodos( dmCliente.Empresa, GetSQLScript( K_GET_HUELLAS_CURSOS ) );
     Result := Not cdsHuellasCurso.IsEmpty;
end;

function TdmInterfase.separarCursosMultiples(sCursos: String): String;
var
    strArray1, strArray2 : TArray<System.string>;
    charArray1, charArray2  : Array[0..0] of Char;
    sNewCursos : String;
    i : integer;
begin
    charArray1[0] := ',';
    charArray2[0] := '|';
    strArray1 := sCursos.Split(charArray1);
    for i := 0 to Length(strArray1) - 1 do
    begin
      strArray2 := strArray1[i].Split(charArray2);
      sNewCursos := ConcatString(sNewCursos, strArray2[0], ',');
    end;
    Result := sNewCursos;
end;

function TdmInterfase.getStrValorMultiple(sCurso,sTipoRsp: String): String;
var
    strArray : TArray<System.string>;
    charArray  : Array[0..0] of Char;
    sRes : String;
    i : integer;
begin
    charArray[0] := '|';
    for i := 0 to lCursosMultiple.Count-1 do
    begin
      if ContainsText( lCursosMultiple[i], sCurso ) then
      begin
        strArray := lCursosMultiple[i].Split(charArray);
        if (sTipoRsp = 'M') then     //maestroId
          sRes := strArray[3]
        else
          sRes := strArray[1];
        Break;
      end;
    end;
    Result := sRes;
end;

procedure TdmInterfase.separarCursosMultiplesCompleto(sCursos: String);
var
    strArray1 : TArray<System.string>;
    charArray1  : Array[0..0] of Char;
    i : integer;
begin
    lCursosMultiple := TStringList.Create;
    charArray1[0] := ',';
    strArray1 := sCursos.Split(charArray1);
    for i := 0 to Length(strArray1) - 1 do
        lCursosMultiple.Add(strArray1[i]);   //   cursoId|rev|hr|maestroId
end;

end.

