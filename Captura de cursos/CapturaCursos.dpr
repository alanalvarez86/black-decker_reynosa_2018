program CapturaCursos;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}
uses
  MidasLib,
  Forms,
  ComObj,
  ActiveX,
  FTressShell in 'FTressShell.pas' {TressShell},
  DInterfase in 'DInterfase.pas' {dmInterfase: TDataModule},
  ZBaseDlgModal_DevEx in '..\Tools\ZBaseDlgModal_DevEx.pas' {ZetaDlgModal_DevEx},
  ZBaseShell in '..\Tools\ZBaseShell.pas' {BaseShell},
  ZBaseImportaShell_DevEx in '..\Tools\ZBaseImportaShell_DevEx.pas' {BaseImportaShell_DevEx};

{$R *.RES}

begin
     ComObj.CoInitFlags := COINIT_APARTMENTTHREADED;
     Application.Initialize;
     Application.Title := 'Captura de cursos - Sistema TRESS';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
     begin
          if ( ParamCount = 0 ) then
          begin
               if Login( False ) then
               begin
                    Show;
                    Update;
                    BeforeRun;
                    Application.Run;
               end
               else
               begin
                    Free;
               end;
          end
          else
          begin
               try
                  ProcesarBatch;
               finally
                  Free;
               end;
          end;
     end;
end.
