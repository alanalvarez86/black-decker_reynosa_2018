object dmInterfase: TdmInterfase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 242
  Width = 345
  object cdsUsuarios: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 32
    Top = 16
  end
  object cdsHistCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsHistCursosAlCrearCampos
    Left = 104
    Top = 16
  end
  object cdsEmpresas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsHistCursosAlCrearCampos
    Left = 32
    Top = 64
  end
  object cdsTempHistorial: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 74
    object cdsTempHistorialCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsTempHistorialPRETTYNAME: TStringField
      FieldName = 'PRETTYNAME'
      Size = 120
    end
  end
  object cdsListaCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsListaCursosAlAdquirirDatos
    Left = 192
    Top = 24
  end
  object cdsHistorial: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 192
    Top = 80
    object cdsHistorialCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsHistorialPRETTYNAME: TStringField
      FieldName = 'PRETTYNAME'
      Size = 160
    end
  end
  object cdsSesionesCursos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'SE_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsSesionesCursosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de Sesiones'
    LookupDescriptionField = 'CU_NOMBRE'
    LookupKeyField = 'SE_FOLIO'
    OnGetRights = cdsSesionesCursosGetRights
    Left = 40
    Top = 144
  end
  object cdsGruposLista: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsGruposListaAlAdquirirDatos
    Left = 144
    Top = 144
  end
  object cdsTemp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 216
    Top = 144
  end
  object cdsHuellasCurso: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 264
    Top = 88
  end
end
