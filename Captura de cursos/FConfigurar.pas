unit FConfigurar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ExtCtrls,
     ZBaseDlgModal, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, dxSkinsDefaultPainters,
     dxSkinWhiteprint, cxButtons, ImgList, DB, DBClient, ZetaClientDataSet,
  ZetaKeyLookup, Vcl.ComCtrls, Vcl.Mask, ZetaNumero, dxBarBuiltInMenu,
  cxControls, cxPC, ZetaKeyLookup_DevEx, ZetaKeyCombo, ZetaDBTextBox,
  cxContainer, cxEdit, cxTextEdit,ZetaCommonLists, SFE, SZCodeBaseX, cxCheckBox;

type
  TConfigurar = class(TZetaDlgModal_DevEx)
    PanelRelleno: TPanel;
    imgListTipoDispositivo: TcxImageList;
    cbTipoGafete: TZetaKeyCombo;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    cxRutaLocal: TcxTextEdit;
    btnBrowseDirBio_DevEx: TcxButton;
    cxImageList19_PanelBotones: TcxImageList;
    GroupBox2: TGroupBox;
    btnRecargarHuellas_DevEx: TcxButton;
    OpenDialog: TOpenDialog;
    ProgresoHuellas: TProgressBar;
    lblMensaje: TLabel;
    cbMostrarFoto: TcxCheckBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    cxLetraCredencial: TcxTextEdit;
    cxDigitoEmpresa: TcxTextEdit;
    gbReporte: TGroupBox;
    Label5: TLabel;
    zReporte: TZetaKeyLookup_DevEx;
    procedure OK_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbTipoGafeteChange(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure btnRecargarHuellas_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FDrivers   : boolean;
    FAbrioSFE  : boolean;
    procedure ToggleBiometrico(lBiometrico: Boolean);
    procedure getDatos;
    procedure setDatos;
    function SincronizaHuellas: Boolean;
    function LeeArchivo: Boolean;
    procedure CrearArchivoHuellas(sFile : String);
    procedure ValidaArchivoHuellas;
    procedure setRutaHuella;

  public
    { Public declarations }

  end;

var
  Configurar: TConfigurar;

implementation

uses IniFiles,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     DConsultas,
     DCliente,
     DCatalogos,
     DReportes,
     DRecursos,
     DInterfase,
     Variants,
     ZetaDialogo,
     ZetaBusqueda_DevEx,
     ZetaWinApiTools;

{$R *.DFM}

var
  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  addrOfTemplate    : Pbyte; //Direccion de memoria del template
  addrOfImageBytes  : Pbyte; //Direccion de memoria de la imagen a mostrar
  gHintStopPos      : Integer;
  gWorking          : Boolean; //Bandera de lector en procesos
  iDedo             : Integer;

const
  FINGER_BITMAP_WIDTH   : Integer = 256; //Ancho de la imagen de la huella
  FINGER_BITMAP_HEIGHT  : Integer = 256; //Alto de la imagen de la huella
  ColorNColor           : Integer = 3;

{ TConfiguraBDSAP }
procedure TConfigurar.btnRecargarHuellas_DevExClick(Sender: TObject);
begin
     inherited;
     {$ifdef ANTES}
     addrOfImageBytes := Integer(addr(gImageBytes));
     addrOfTemplate := Integer(addr(gTemplate));
     {$else}
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
     {$endif}
     gHintStopPos := 0;
     if FDrivers then
     begin
          FAbrioSFE := LeeArchivo;
          if ( FAbrioSFE ) then
             SincronizaHuellas;
     end;
end;

function TConfigurar.LeeArchivo: Boolean;
var
   sRuta : String;
   nRet : Integer;
begin
     sRuta := cxRutaLocal.Text;
     lblMensaje.Hint := cxRutaLocal.Text;
     nRet := 0;
     try
        if FileExists( cxRutaLocal.Text ) then
        begin
           //DeleteFile( cxRutaLocal.Text );
            if ( FDrivers ) then
               nRet := SFE.Open( PWideChar( cxRutaLocal.Text ), K_TIPO_BIOMETRICO, 0 );
            if ( nRet = 0 ) then
            begin
                 lblMensaje.Caption := Format( 'Plantillas encontradas: %s.', [ IntToStr( SFE.GetEnrollCount() ) ] );
                 Result := True;
            end
            else
            begin
                 lblMensaje.Caption := Format( 'Error al abrir archivo %s ...', [ sRuta.Substring(0,29) ] );
                 Result := False;
            end;
        end
        else
        begin
            lblMensaje.Caption := Format( 'Error no existe archivo %s ...', [ sRuta.Substring(0,29) ] );
            Result := False;
        end;
     except
           on Error: Exception do
           begin
                DeleteFile( cxRutaLocal.Text );
                Result := False;
           end;
     end;
end;

procedure TConfigurar.cbTipoGafeteChange(Sender: TObject);
var
  lSeleccionBioemtrico: Boolean;
begin
  inherited;
  lSeleccionBioemtrico := (cbTipoGafete.ItemIndex = Ord(egBiometrico));
  ToggleBiometrico(lSeleccionBioemtrico);
end;

procedure TConfigurar.FormCreate(Sender: TObject);
var
  sFiltroCta : String;
begin
     inherited;
     dmInterfase.cdsSesionesCursos.Conectar;
     FDrivers  := LoadDLL;
     FAbrioSFE := FALSE;
     dmReportes.cdsLookupReportes.Refrescar;
     sFiltroCta := 'RE_TIPO IN( 0, 1 )';
     with zReporte do
     begin
        LookupDataset := dmReportes.cdsLookupReportes;
        Filtro := sFiltroCta;
     end;
end;

procedure TConfigurar.FormShow(Sender: TObject);
begin
     inherited;
     dmReportes.cdsLookupReportes.Conectar;
     getDatos;
     ToggleBiometrico((cbTipoGafete.ItemIndex = Ord(egBiometrico)));
     if cbTipoGafete.ItemIndex = Ord(egBiometrico) then
        ValidaArchivoHuellas;
end;

procedure TConfigurar.OK_DevExClick(Sender: TObject);
var
  fCampoVacio: Boolean;
  sExt: String;
begin
     inherited;
     fCampoVacio := FALSE;

     if ( cbTipoGafete.ItemIndex = Ord(egBiometrico) ) and ( cxRutaLocal.Text = VACIO ) then
     begin
        fCampoVacio := TRUE;
        ZetaDialogo.ZError( 'Captura de cursos', 'Se debe seleccionar la ruta del archivo de huellas.', 0 );
     end;

     if not( cxRutaLocal.Text = VACIO ) then
     begin
        sExt := ExtractFileExt(cxRutaLocal.Text);
        if ( not (sExt.Equals('.dat') or sExt.Equals('.DAT')) ) then
        begin
          fCampoVacio := TRUE;
          ZetaDialogo.ZError( 'Captura de cursos', 'La extencion del archivo debe ser .dat', 0 );
        end;
     end;

     if (not fCampoVacio) then
     begin
        ModalResult := mrOk;
        setDatos;
     end;
end;

procedure TConfigurar.setRutaHuella;
var
   appINI : TIniFile;
begin
    appINI := TIniFile.Create( VerificaDir( ExtractFilePath( Application.ExeName ) ) + 'Configuracion.ini' );
    try
      Self.cxRutaLocal.Text := appINI.ReadString( 'CONFIGURACION', 'RUTAHUELLA', VACIO );
    finally
      appINI.Free;
    end;
end;

procedure TConfigurar.getDatos;
var
   appINI : TIniFile;
   sRuta : String;
begin
     sRuta := ExtractFilePath( Application.ExeName );
     sRuta := VerificaDir( sRuta );
     sRuta := sRuta + 'Configuracion.ini';
     appINI := TIniFile.Create( sRuta ) ;
     try
        Self.cbTipoGafete.ItemIndex := appINI.ReadInteger( 'CONFIGURACION', 'TIPOGAFETE', 0 );
        Self.cxRutaLocal.Text := appINI.ReadString( 'CONFIGURACION', 'RUTAHUELLA', VACIO );
        Self.cbMostrarFoto.Checked := zStrToBool( appINI.ReadString( 'CONFIGURACION', 'MUESTRAFOTO', K_GLOBAL_NO ) );
        Self.cxLetraCredencial.Text := appINI.ReadString( 'CONFIGURACION', 'LETRA', VACIO );
        Self.cxDigitoEmpresa.Text := appINI.ReadString( 'CONFIGURACION', 'DIGITO', VACIO );
        Self.zReporte.Llave := appINI.ReadString( 'CONFIGURACION', 'REPORTE', VACIO );
    finally
        appINI.Free;
    end;
end;

procedure TConfigurar.setDatos;
var
   appINI : TIniFile;
   sRuta : String;
begin
     sRuta := ExtractFilePath( Application.ExeName );
     sRuta := VerificaDir( sRuta );
     sRuta := sRuta + 'Configuracion.ini';
     appINI := TIniFile.Create( sRuta ) ;
     try
        appINI.WriteInteger( 'CONFIGURACION', 'TIPOGAFETE', Self.cbTipoGafete.ItemIndex );
        appINI.WriteString( 'CONFIGURACION', 'RUTAHUELLA', Self.cxRutaLocal.Text );
        appINI.WriteString( 'CONFIGURACION', 'MUESTRAFOTO', zBoolToStr( Self.cbMostrarFoto.Checked ) );
        appINI.WriteString( 'CONFIGURACION', 'LETRA', Self.cxLetraCredencial.Text );
        appINI.WriteString( 'CONFIGURACION', 'DIGITO', Self.cxDigitoEmpresa.Text );
        appINI.WriteString( 'CONFIGURACION', 'REPORTE', Self.zReporte.Llave );
    finally
        appINI.Free;
    end;
end;

procedure TConfigurar.ToggleBiometrico(lBiometrico: Boolean);
begin
    GroupBox1.Enabled := lBiometrico;
    cxRutaLocal.Enabled := lBiometrico;
    btnBrowseDirBio_DevEx.Enabled := lBiometrico;
    btnRecargarHuellas_DevEx.Enabled := lBiometrico;
end;

procedure TConfigurar.ArchivoSeekClick(Sender: TObject);
var
  sFile, oEdit : String;
  oDialog : TOpenDialog;
begin
  if (cxRutaLocal.Text = VACIO) then
  begin
      oEdit := VerificaDir(ExtractFilePath( Application.ExeName ));
      sFile := 'CURSOS.DAT';
  end
  else
  begin
      oEdit := VerificaDir(cxRutaLocal.Text);
      oEdit := oEdit.Substring(0,oEdit.Length-1);
      sFile := ExtractFileName(oEdit);
  end;
  oDialog := OpenDialog;
  with oDialog do begin
    FileName := sFile;
    InitialDir := ExtractFilePath(oEdit);
    if strVacio(InitialDir) then
      InitialDir := ZetaWinApiTools.GetWinSysDir;

    if Execute then
    begin
      cxRutaLocal.Text := FileName;
      CrearArchivoHuellas(cxRutaLocal.Text);
    end;
  end;
end;

procedure TConfigurar.CrearArchivoHuellas(sFile : String);
var
  fileHandle : THandle;
begin
  if not FileExists(sFile) then
  begin
    fileHandle := NativeInt(FileCreate(sFile));
    FileClose(fileHandle);
  end;
end;

procedure TConfigurar.ValidaArchivoHuellas;
begin
  if not (cxRutaLocal.Text = VACIO) then
  begin
    if not FileExists(cxRutaLocal.Text) then
    begin
      setRutaHuella;
      cxRutaLocal.Text := VACIO;
    end;
  end;
end;

function TConfigurar.SincronizaHuellas: Boolean;
var
   nRet, iId, iDedo, iValor, iEmpleado: Integer;
   iContador: Extended;
   sFilename, sRuta: string;
   ss: TStringStream;
   ms: TMemoryStream;
   oCursor: TCursor;
begin
     iId := 0;
     iDedo := 0;
     iContador := 0;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     sRuta := ExtractFilePath( Self.cxRutaLocal.Text );
     sRuta := VerificaDir( sRuta );

     //switchWorking(true);
     try
          lblMensaje.Caption := 'Inicializando dispositivo biométrico. Espere un momento...';
          if dmInterfase.AdquiereHuellas then
          begin
               with dmInterfase.cdsHuellasCurso do
               begin
                    First;
                    ms := TMemoryStream.Create;
                    while not EOF do
                    begin
                         iContador := iContador + 1;
                         iValor := Round( 100 * iContador / RecordCount );
                         ProgresoHuellas.Position := iValor;
                         ProgresoHuellas.Visible := true;
                         Application.ProcessMessages();
                         try
                            iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                            iDedo := FieldByName( 'HU_INDICE' ).AsInteger;
                            iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;

                            nRet := SFE.CheckFingerNum(iId, iDedo);
                            if ( nRet <> 0 ) then
                            begin
                                 ss := TStringStream.Create( FieldByName( 'HUELLA' ).AsString );
                                 ms.Position := 0;
                                 SZDecodeBase64( ss, ms );

                                 sFilename := sRuta + Format( '%s_%s.tem', [ IntToStr( iId ), IntToStr( iDedo ) ] );
                                 ms.SaveToFile( sFilename );

                                 nRet := SFE.LoadTemplateFromFile( PWideChar( sFilename ), addrOfTemplate );

                                 if ( nRet < 0 ) then
                                 begin
                                      lblMensaje.Caption := Format( 'Error con plantilla Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] );
                                 end
                                 else
                                 begin
                                      nRet := SFE.TemplateEnroll( iId , iDedo, 0, addrOfTemplate );
                                      if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                                      begin
                                           lblMensaje.Caption := Format( 'Error al enrolar Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] );
                                      end
                                      else
                                      begin
                                           lblMensaje.Caption := Format( 'Enrolando Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] );
                                      end;
                                 end;
                                 DeleteFile( sFilename );
                            end;
                         except
                               on Error: Exception do
                               begin
                                    lblMensaje.Caption := Format( 'Error proceso de obtener plantillas Empleado: %d Id: %d Indice: %d', [ iEmpleado, iId, iDedo ] );
                               end;
                         end;
                         Next;
                    end;
               end;
          end
          else
          begin
               lblMensaje.Caption := 'No se tienen plantillas por enrolar';
          end;
     finally
            Screen.Cursor := oCursor;
     end;
     lblMensaje.Caption := 'Empleados Enrolados con Exito';
     //ProgresoHuellas.Visible := false;
     ms.Clear;
end;

end.
