unit FTressProcesos;

interface

uses SysUtils, Classes, ComObj, ComServ, ActiveX, StdVcl, Dialogs,
     ZetaCommonClasses,
     TressProcesos_TLB;

type
  TTressProcesos_ = class(TAutoObject, ITressProcesos)
  private
    { Private declarations }
    FStatus: String;
    procedure ManejaError( Error: Exception ); overload;
    procedure ManejaError( Error: Exception; const sMensaje: String ); overload;
  protected
    { Protected declarations }
    procedure Start;
    procedure Stop;
  public
    { Public declarations }
    destructor Destroy; override;
  protected
    { Protected declarations }
  end;

implementation

uses DCliente,
     ZetaCommonTools,
     ZetaClientTools;

{ *********** TTressProcesos_ ************ }

destructor TTressProcesos_.Destroy;
begin
     Stop;
     inherited Destroy;
end;

procedure TTressProcesos_.Start;
begin
     FStatus := VACIO;
     if not Assigned( dmCliente ) then
        dmCliente := TdmCliente.Create( nil );
end;

procedure TTressProcesos_.Stop;
begin
     FreeAndNil( dmCliente );
end;

procedure TTressProcesos_.ManejaError( Error: Exception );
begin
     ManejaError( Error, 'Se Encontr� Un Error' );
end;

procedure TTressProcesos_.ManejaError( Error: Exception; const sMensaje: String );
begin
     FStatus := Error.Message;
     {
     ZetaDialogo.zExcepcion( '� Atenci�n !', sMensaje, Error, 0 );
     }
end;

{ ********** LLamadas COM ********** }


initialization
  {
  ZetaClientTools.InitDCOM;
  }
  TAutoObjectFactory.Create(ComServer, TTressProcesos_, Class_TressProcesos_, ciMultiInstance, tmApartment);
end.
