library TressProcesos;

uses
  ComServ,
  TressProcesos_TLB in 'TressProcesos_TLB.pas',
  FTressProcesos in 'FTressProcesos.pas' {TressProcesos: CoClass},
  DBasicoCliente in '..\..\3Win_20\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
