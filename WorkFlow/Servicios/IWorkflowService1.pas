// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/motor/Workflowservice.dll/wsdl/IWorkflowService
// Encoding : utf-8
// Version  : 1.0
// (22/12/2006 11:25:31 a.m. - 1.33.2.5)
// ************************************************************************ //

unit IWorkflowService1;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns, SOAPHTTPTrans;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"


  // ************************************************************************ //
  // Namespace : urn:WorkflowServiceIntf-IWorkflowService
  // soapAction: urn:WorkflowServiceIntf-IWorkflowService#TressGrabar
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : IWorkflowServicebinding
  // service   : IWorkflowServiceservice
  // port      : IWorkflowServicePort
  // URL       : http://localhost/motor/Workflowservice.dll/soap/IWorkflowService
  // ************************************************************************ //
  IWorkflowService = interface(IInvokable)
  ['{E409548F-12F8-BB9C-0866-5B78407BE8BB}']
    function  TressGrabar(const Data: WideString; const UsuarioMotor: Integer): WideString; stdcall;
  end;

function GetIWorkflowService(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): IWorkflowService;


implementation

function GetIWorkflowService(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): IWorkflowService;
const
  defWSDL = 'http://localhost/motor/Workflowservice.dll/wsdl/IWorkflowService';
  defURL  = 'http://localhost/motor/Workflowservice.dll/soap/IWorkflowService';
  defSvc  = 'IWorkflowServiceservice';
  defPrt  = 'IWorkflowServicePort';
var
  RIO: THTTPRIO;
  HTTPReqResp1:  THTTPReqResp;
begin
  HTTPReqResp1 := THTTPReqResp.create(nil);
  HTTPReqResp1.Password := 'tgomez';
  HTTPReqResp1.username := 'mibebe';
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IWorkflowService);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
    RIO.HTTPWebNode := HTTPReqResp1;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(IWorkflowService), 'urn:WorkflowServiceIntf-IWorkflowService', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IWorkflowService), 'urn:WorkflowServiceIntf-IWorkflowService#TressGrabar');

end. 