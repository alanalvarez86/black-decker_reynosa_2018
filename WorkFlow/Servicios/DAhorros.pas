unit DAhorros;

{$define LEONI}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,{$ifndef VER130}Variants,{$endif}
     Recursos_TLB,
     Asistencia_TLB,
     Comparte_TLB,
     Login_TLB,
     Consultas_TLB,
     Nomina_TLB,
     Tablas_TLB,
     DZetaServerProvider,
     DBasicoCliente,
     DXMLTools,
     //ZetaCommonTypes,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZBaseThreads,
     ZetaClientDataSet, ZetaServerDataSet;
const
     K_NO_AUTORIZADO = 99;
     K_AUTORIZADO = -1;
     K_MAX_VAL_USO_DEMO = 5;
     K_TW_AHORRO_AGREGAR = 'AHORRO_AGREGAR';
type

  TdmAhorros = class(TBasicoCliente)
    cdsLista: TZetaClientDataSet;
    cdsComparte: TZetaClientDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsProcesos: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsPassword: TZetaClientDataSet;
    cdsHisAhorros: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);

    procedure cdsHisAhorrosAlAdquirirDatos(Sender: TObject);
    procedure cdsHisAhorrosAlEnviarDatos(Sender: TObject);
  private
    { Private declarations }
    FXMLTools: TdmXMLTools;
    FStatus: String;
    FErrores: TStrings;
    FProcessData: TProcessInfo;
    FEmpleado: TNumEmp;
    FAhorroAjustarFecha: integer;
    cdsAhorros : TZetaClientDataSet;
    FCancelaAjuste : Boolean;
    FHuboCambios, FProcesandoThread, FHuboErrores, FHuboErroresInternos, FHuboMensajes, FHuboCambioEmpresa, FEmpresaValida, FErrorAhorro : Boolean;
    FServerRecursos: IdmServerRecursosDisp;
    FServerComparte: IdmServerComparteDisp;
    FServerLogin: IdmServerLoginDisp;
    FServerConsultas: IdmServerConsultasDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerComparte: IdmServerComparteDisp;
    function GetServerLogin: IdmServerLoginDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function LogAsText: String;
    function GetAuto: OleVariant;
    procedure LogInit;
    procedure LogError( const iEmpleado: Integer; const sTexto: String );
    procedure ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    function ObtenValUso( const sTipo: String ): Integer;
    procedure GrabaValUso( const sTipo: String; const iUso: Integer );
    function SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
    function ValidaEnviar( DataSet : TZetaClientDataSet ): boolean;
    function GetDatosEmpleadoActivo: Boolean;


  protected
    { Protected declarations }

    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerComparte: IdmServerComparteDisp read GetServerComparte;
    property ServerLogin : IdmServerLoginDisp read GetServerLogin;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property Status: String read FStatus write FStatus;
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    function TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
  public
    { Public declarations }
    property ProcessData: TProcessInfo read FProcessData;
    property ErrorAhorro : Boolean read FErrorAhorro write FErrorAhorro;
    property AhorroAjustarFecha : integer read FAhorroAjustarFecha write FAhorroAjustarFecha;
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
    function AgregarAhorro(  const iUsuario:integer ) : Boolean;
    procedure CargarAutorizacion;
    procedure AsignarDataTemporal;
  end;

var
  dmAhorros: TdmAhorros;

implementation

uses FAutoClasses,
     ZetaCommonTools,
     ZetaClientTools,
     FToolsRH,
     ZetaRegistryCliente,
     ZetaServerTools,
     ZGlobalTress, DateUtils, XMLIntf, Masks;
{$R *.DFM}

const
     K_EMPLEADO_NULO = 0;
     K_ESTADO_AHORRO_ACTIVO = 0;

constructor TdmAhorros.Create(AOwner: TComponent);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     inherited Create( AOwner );
end;

procedure TdmAhorros.DataModuleCreate(Sender: TObject);
begin
     FXMLTools := TdmXMLTools.Create( Self );
     FErrores := TStringList.Create;
     inherited;
end;

destructor TdmAhorros.Destroy;
begin
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmAhorros.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FErrores );
     FreeAndNil( FXMLTools );
end;
function TdmAhorros.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( Self.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;

function TdmAhorros.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( Self.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmAhorros.GetServerComparte: IdmServerComparteDisp;
begin
     Result:= IdmServerComparteDisp( Self.CreaServidor( CLASS_dmServerComparte, FServerComparte ) );
end;
function TdmAhorros.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( Self.CreaServidor( CLASS_dmServerLogin,FServerLogin ) );
end;

function TdmAhorros.GetAuto: OleVariant;
begin
     Result := GetServerLogin.GetAuto;
end;

{ ****** Manejos Internos ****** }

function TdmAhorros.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmAhorros.SetEmpresaActiva(const sCodigo: String): Boolean;
begin
     InitCompanies;
     Result := FindCompany( sCodigo );
     if Result then
     begin
          SetCompany;
     end;
end;

procedure TdmAhorros.ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     Status := E.Message;
end;

procedure TdmAhorros.LogInit;
begin
     with FErrores do
     begin
          Clear;
     end;
end;

procedure TdmAhorros.LogError( const iEmpleado: Integer; const sTexto: String );
begin
      with FErrores do
      begin
           BeginUpdate;
           try
              if( iEmpleado > 0 ) then
                Add( Format( 'Empleado #%d|%s' + CR_LF, [ iEmpleado, sTexto ] ) )
             else
              Add( sTexto );
           finally
                  EndUpdate;
           end;
      end;
end;

function TdmAhorros.LogAsText: String;
begin
     with FErrores do
     begin
          Result := Text;
     end;
end;

{ ***** Metodos Bases ***** }
{ ***** Cargado de XML y Determinaci�n del Proceso A Ejecutar }

function TdmAhorros.TressGrabar(const sXML: String; const iUsuario: Integer; {$ifdef LEONI} const iFolio: integer; {$endif} var sLog: String): Boolean;
var
   sEmpresa, sTipo: String;
begin
     Result := FALSE;
     sLog := '';
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
             sTipo := TagAsString( GetNode( 'MOV3' ), '' );
        end;
        if ZetaCommonTools.StrVacio( sEmpresa ) then
           sLog := 'Empresa No Especificada'
        else
        begin
             Usuario := iUsuario;    // Asigna el usuario activo a dmAhorros - No se hace login ni se validan derechos de acceso
             if SetEmpresaActiva( sEmpresa ) then
             begin
                  LogInit;
                  Result := TressInvocar( sTipo, iUsuario );
                  sLog := LogAsText;
             end
             else
                 sLog := Format( 'Empresa %s No Existe', [ sEmpresa ] );
        end;
     except
           on Error: Exception do
           begin
                sLog := 'Error Al Examinar Documento: ' + Error.Message;
           end;
     end;
end;

function TdmAhorros.TressInvocar( const sTipo: String; const iUsuario: Integer ): Boolean;
var
   iUso: Integer;
begin
     Status := '';
     iUso := ObtenValUso( sTipo );
     if( iUso < K_MAX_VAL_USO_DEMO )then
     begin
          if ( sTipo = K_TW_AHORRO_AGREGAR ) then // Agregar Ahorro
             Result := AgregarAhorro(iUsuario)
          else
          begin
               Result := False;
               LogError( 0, Format( 'Tipo de Movimiento Indeterminado ( Nodo TIPO ''%s'' )', [ sTipo ] ) );
          end;

          if Result then
          begin
               if( iUso > K_AUTORIZADO )then
                   GrabaValUso( sTipo, ( iUso + 1 ) );
          end;
     end
     else
     begin
          Result := False;
          if( iUso = K_NO_AUTORIZADO )then
              LogError( 0, 'M�dulo No Autorizado' )
          else
              LogError( 0, 'Modo DEMO' );
     end;
end;

function TdmAhorros.ObtenValUso( const sTipo: String ): Integer;
var
   sUso: String;
   iUso: Integer;
   oDataSet : TZetaClientDataSet;
begin
     Result := 0;
     Autorizacion.Cargar( GetAuto );

     {$ifdef WF_NUBE}
     Result := K_AUTORIZADO;
     {$else}

     if( Autorizacion.OkModulo( okWorkFlow ) )then // Modulo autorizado
     begin
          if( Autorizacion.EsDemo )then
          begin
               oDataSet := TZetaClientDataSet.Create( self );
               try
                  with oDataSet do
                  begin
                       Data := ServerComparte.GetValidacionUso( sTipo );
                       if Not IsEmpty then
                       begin
                            First;
                            sUso := Decrypt( FieldByName( 'USO' ).AsString );
                            iUso := StrToIntDef( sUso, 0 );
                            Result := iUso;
                       end;
                  end;
               finally
                      FreeAndNil( oDataSet );
               end;
          end
          else
              Result := K_AUTORIZADO;
     end
     else
         Result := K_NO_AUTORIZADO;
     {$endif}
end;

procedure TdmAhorros.GrabaValUso( const sTipo: String; const iUso: Integer );
var
   sUso: String;
begin
     sUso := Encrypt( Format( '%d', [ iUso ] ) );
     ServerComparte.GrabaValidacionUso( sTipo, sUso );
end;

procedure TdmAhorros.CargarAutorizacion;
begin
     Autorizacion.Cargar( GetAuto );
end;


function TdmAhorros.SetEmpleadoNumero( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
var
   oDatos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
     if Result then
     begin
          with oDataSet do
          begin
               Data := oDatos;
               FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
     end;
end;


function TdmAhorros.AgregarAhorro( const iUsuario:integer ) : Boolean;
var
   Registros, Registro: TZetaXMLNode;
   sNombreCompany: string;
   sEmpresa: string;

   iEmpNumero : integer;

   oParametros: TZetaParams;

   lOk, lOkAux, lOkProcess, lOkProcessAux, lRegistro: Boolean;
   iNumero : Integer;
   sMensaje : string;

   procedure CargaParametros( Registro: TZetaXMLNode; var Parametros : TZetaParams);
   begin
        with FXMLTools do
         begin
              while Assigned( Registro ) do
              begin
                 with Parametros do
                 begin
                      AddInteger('iEmpNumero', AttributeAsInteger(Registro, 'EmpNumero',  K_EMPLEADO_NULO ) );
                      AddString('sTipoAhorro', AttributeAsString(Registro, 'AHTipoCodigo', '' ) );
                      AddDate('dFechaAhorro', AttributeAsDateTime( Registro, 'AHFecha', 0 ) );
                      AddFloat('fSaldoInicial', AttributeAsFloat( Registro, 'AHSaldoInicial', 0 ));
                      AddString('sFormula', AttributeAsString(Registro, 'AHFormula', '' ) );
                      AddString('sSubCta', AttributeAsString(Registro, 'AHSubCta', '' ) );
                      AddInteger('iStatus', AttributeAsInteger(Registro, 'AHStatus',  K_ESTADO_AHORRO_ACTIVO ) );
                      //US_CODIGO (US_CODIGO)?
                 end;
                   break;
              end;
         end;
   end;

   procedure AgregarDatosDatasetAhorro(var Parametros : TZetaParams);
   const
        VACIO = '';
   var
      iCodigo, iNumAhorro : integer;
      sTipo : string;
      bEncontroClon : Boolean;
   begin
       sTipo := Parametros.ParamByName('sTipoAhorro').AsString;
       iCodigo := Parametros.ParamByName( 'iEmpNumero' ).AsInteger;
       iNumAhorro := 0; //SERA NECESARIO QUE ASIGNEN UN NUMERO DE AHORRO

        with cdsHisAhorros do
        begin
             Refrescar;
             bEncontroClon := Locate('CB_CODIGO; AH_TIPO', VarArrayOf([iCodigo, sTipo]),[]);
             if bEncontroClon then
             begin
                DB.DataBaseError( 'Ya existe un Ahorro de tipo '+sTipo+' para el empleado: '+ iCodigo.ToString());
             end
             else
             begin
                  Append;
                  FieldByName('CB_CODIGO').AsInteger := Parametros.ParamByName( 'iEmpNumero' ).AsInteger;
                  FieldByName('AH_TIPO').AsString := Parametros.ParamByName('sTipoAhorro').AsString;
                  FieldByName('AH_FECHA').AsDateTime := Parametros.ParamByName('dFechaAhorro').AsDateTime;
                  FieldByName('AH_SALDO_I').AsFloat :=  Parametros.ParamByName('fSaldoInicial').AsFloat;
                  FieldByName('AH_FORMULA').AsString := Parametros.ParamByName('sFormula').AsString;
                  FieldByName('AH_SUB_CTA').AsString := Parametros.ParamByName('sSubCta').AsString;
                  FieldByName('AH_STATUS').AsInteger := Parametros.ParamByName( 'iStatus' ).AsInteger;
                  FieldByName('AH_NUMERO').AsFloat :=  iNumAhorro;
                  FieldByName('US_CODIGO').AsFloat :=  Usuario;
             end;
        end;
   end;

    function ObtenerTipoAhorro( TipoAhorro: string ): boolean;
    const
         K_CONSULTA_TAHORRO = 'SELECT  TB_CODIGO, TB_ELEMENT  FROM TAHORRO where TB_CODIGO = ''%s''';
    var
       sTipoAhorro: string;
       sQuery: string;
       oConsultatipoAhorro: TClientDataSet;
       bRespuesta : boolean;
    begin
         oConsultatipoAhorro := TZetaClientDataSet.Create( self );
         sTipoAhorro := '';
         sQuery := '';
         bRespuesta := False;
         sQuery := Format( K_CONSULTA_TAHORRO, [ TipoAhorro ] );
         oConsultatipoAhorro.Data := ServerConsultas.GetQueryGralTodos( Empresa, sQuery );
         if ( not oConsultatipoAhorro.Eof ) then
         begin
            sTipoAhorro := oConsultatipoAhorro.FieldByName( 'TB_CODIGO' ).AsString;
         end;

         if (sTipoAhorro <> VACIO) then
         begin
               bRespuesta := true;
         end;
         Result := bRespuesta;
    end;

   function ValidaDatos: Boolean;
   var
      lValido : Boolean;
      bTipoAhorro : Boolean;
   begin
        lValido := True;
        bTipoAhorro := false;
        with cdsHisAhorros do
        begin
          if ( FieldByName( 'CB_CODIGO' ).AsInteger <= 0 ) then
          begin
             DB.DataBaseError( 'Es necesario un n�mero de empleado mayor a cero' );
             lValido := False;
          end;

          if ZetaCommonTools.StrVacio( FieldByName( 'AH_TIPO' ).AsString ) then
          begin
             DB.DataBaseError( 'El C�digo del ahorro no puede quedar vacio' );
             lValido := False;
          end
          else
          begin
                bTipoAhorro := ObtenerTipoAhorro( FieldByName( 'AH_TIPO' ).AsString );
                if ( bTipoAhorro <> true) then
                begin
                   DB.DataBaseError( 'Es necesario un c�digo de ahorro existente' );
                   lValido := False;
                end;
          end;

        end;

        Result := lValido;
   end;

begin
     lOk := False;
     lOkAux := False;
     try
        try
           with FXMLTools do
           begin
                Registros := GetNode( 'REGISTROS' );
                sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
                if Assigned ( Registros ) then
                begin
                     LogError( 0, Format( '|Empresa %s|', [ sEmpresa ] ) );
                     if Assigned( Registros ) then
                     begin
                          Registro := GetNode( 'REGISTRO', Registros );
                          while Assigned( Registro ) do
                          begin
                               oParametros:= TZetaParams.Create;
                               CargaParametros( Registro, oParametros);
                               lRegistro := True;
                               iEmpNumero := oParametros.ParamByName( 'iEmpNumero' ).AsInteger;
                               if SetEmpleadoNumero( cdsEmpleado, oParametros.ParamByName( 'iEmpNumero' ).AsInteger ) then
                               begin
                                    cdsHisAhorros.Conectar;
                                    AsignarDataTemporal;
                                    AgregarDatosDatasetAhorro(oParametros);
                                    if( ValidaDatos ) then
                                    begin
                                        if ( ( not ValidaEnviar( cdsHisAhorros) ) or ( ErrorAhorro ) ) then
                                           lOk := True;
                                    end
                                    else
                                    begin
                                        cdsHisAhorros.CancelUpdates;
                                        lOk := True;
                                    end;
                                    if ( ( lOk ) and ( lOkAux = False ) ) then lOkAux := True;
                               end
                               else
                               begin
                                    LogError( 0, Format( 'Empleado #%d no existe en la empresa %s:%s', [ iEmpNumero, sEmpresa, sNombreCompany ] ) );
                                    lOk := true;
                               end;
                               Registro := GetNextSibling( Registro );
                          end;
                          if not ( lRegistro ) then
                          begin
                               LogError( 0, 'Error al agregar Ahorro: Nodo <REGISTRO> no especificado' );
                               lOk := True;
                          end;
                     end;
                end
                else
                begin
                     lOk := true;
                     LogError( 0, 'Error al agregar Ahorro: Nodo <REGISTROS> no especificado' );
                end;
           end;
        except
              on Error: Exception do
              begin
                   lOk := true;
                   LogError( 0, 'Error al agregar Ahorro: ' + Error.Message );
              end;
        end;
        if ( lOkAux ) then lOk := True;
        if not ( lOk ) then FErrores.Clear;
        Result := lOk;
     finally
            FreeAndNil( cdsHisAhorros );
     end;
end;

function TdmAhorros.ValidaEnviar ( DataSet : TZetaClientDataSet) : boolean;
begin
     Result := True;
     try
          DataSet.Enviar;
     except
          on Error : Exception do
          begin
               FHuboErrores := True;
               LogError( FEmpleado, Format( '%s', [ Error.Message ] ) );
               DataSet.CancelUpdates;
               Result := False;
          end;
     end;
end;

procedure TdmAhorros.AsignarDataTemporal;
begin
     if cdsAhorros = Nil then
     begin
          cdsAhorros := TZetaClientDataSet.Create(Self);
     end;
     cdsAhorros.Data := cdsHisAhorros.Data;
end;

function TdmAhorros.GetDatosEmpleadoActivo: Boolean;
begin
     with cdsEmpleado do
     begin
          Result := zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
     end;
end;

procedure TdmAhorros.cdsHisAhorrosAlAdquirirDatos(Sender: TObject);
begin
      cdsHisAhorros.Data := ServerRecursos.GetHisAhorros( Empresa, FEmpleado );
end;

procedure TdmAhorros.cdsHisAhorrosAlEnviarDatos(Sender: TObject);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsHisAhorros do
     begin
          PostData;
          if (Changecount > 0 ) then
          begin
              Reconcile ( ServerRecursos.GrabaHistorial( Empresa, Tag, Delta, ErrorCount ) );
          end;
     end;
end;


end.
