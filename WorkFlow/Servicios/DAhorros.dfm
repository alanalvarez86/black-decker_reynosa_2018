inherited dmAhorros: TdmAhorros
  OldCreateOrder = True
  Height = 277
  Width = 443
  inherited cdsCompany: TZetaClientDataSet
    Left = 40
  end
  object cdsLista: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 16
  end
  object cdsComparte: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CM_NOMBRE'
    Params = <>
    Left = 136
    Top = 72
  end
  object cdsEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 232
    Top = 16
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 16
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 232
    Top = 72
  end
  object cdsPassword: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 80
  end
  object cdsHisAhorros: TZetaClientDataSet
    Tag = 8
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Ascendente'
        Fields = 'PR_FECHA'
      end
      item
        Name = 'Descendente'
        Fields = 'PR_FECHA'
        Options = [ixDescending]
      end>
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsHisAhorrosAlAdquirirDatos
    AlEnviarDatos = cdsHisAhorrosAlEnviarDatos
    Left = 40
    Top = 136
  end
end
