program WorkFlowEMailApp;

uses
  MidasLib,
  Forms,
  FEMailTest in 'FEMailTest.pas' {EMailTester},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal};

{$R *.RES}
{$R WINDOWSXP.RES}

begin
  Application.Initialize;
  Application.Title := 'WorkFlow EMail';
  Application.CreateForm(TEMailTester, EMailTester);
  Application.CreateForm(TZetaDlgModal, ZetaDlgModal);
  Application.Run;
end.
