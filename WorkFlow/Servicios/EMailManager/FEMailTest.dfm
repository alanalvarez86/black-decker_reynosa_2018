object EMailTester: TEMailTester
  Left = 342
  Top = 176
  Width = 637
  Height = 511
  Caption = 'Administrador De Correos De WorkFlow'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LogGB: TGroupBox
    Left = 0
    Top = 0
    Width = 629
    Height = 436
    Align = alClient
    Caption = ' Bit'#225'cora '
    TabOrder = 0
    object Log: TMemo
      Left = 2
      Top = 15
      Width = 625
      Height = 419
      Align = alClient
      BorderStyle = bsNone
      Color = clBtnFace
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 436
    Width = 629
    Height = 41
    Align = alBottom
    TabOrder = 1
    DesignSize = (
      629
      41)
    object Mensajes: TZetaTextBox
      Left = 355
      Top = 12
      Width = 97
      Height = 17
      AutoSize = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object GreenLED: TSpeedButton
      Left = 264
      Top = 4
      Width = 32
      Height = 32
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333000003333333333330000033333333008877700
        333333330088777003333308870007770333333088700077703330880FAFAF07
        703333088000000077033080F00000F070333308000000000703087F00A2200F
        77033087000222000770080A0A2A220A070330800022222000700F0F0AAAA20F
        070330F00022222000700F0A0FAA2A0A080330F00082222000800F7F00FFA00F
        780330F70008820007803080F00000F08033330800000000080330F80FAFAF08
        8033330F800000008803330F8700078803333330F8700078803333300FFF8800
        3333333300FFF880033333333000003333333333330000033333}
      NumGlyphs = 2
    end
    object Start: TBitBtn
      Left = 463
      Top = 6
      Width = 75
      Height = 30
      Hint = 'Iniciar Env'#237'o De Correos'
      Anchors = [akTop, akRight]
      Caption = 'Iniciar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = StartClick
      Kind = bkOK
    end
    object Stop: TBitBtn
      Left = 549
      Top = 6
      Width = 75
      Height = 30
      Hint = 'Detener Env'#237'o De Correos'
      Anchors = [akTop, akRight]
      Caption = 'Terminar'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = StopClick
      Kind = bkCancel
    end
    object MensajesLBL: TStaticText
      Left = 308
      Top = 13
      Width = 43
      Height = 17
      Alignment = taRightJustify
      Caption = 'Correos:'
      TabOrder = 2
    end
    object Configurar: TBitBtn
      Left = 6
      Top = 5
      Width = 97
      Height = 31
      Hint = 'Configurar Motor De WorkFlow'
      Caption = 'Configurar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = ConfigurarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
    end
  end
end
