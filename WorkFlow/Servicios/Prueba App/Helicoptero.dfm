object Helicopteros: THelicopteros
  Left = 240
  Top = 174
  Width = 703
  Height = 468
  Caption = 'Helicopteros COTEMAR'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 46
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Tipo personal: '
  end
  object Label2: TLabel
    Left = 320
    Top = 232
    Width = 32
    Height = 13
    Caption = 'Label2'
  end
  object tipoPersonal: TComboBox
    Left = 120
    Top = 5
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'Interno'
      'Externo')
  end
  object gbCargo: TGroupBox
    Left = 8
    Top = 32
    Width = 681
    Height = 89
    Caption = ' Con cargo a: '
    TabOrder = 1
    object Label3: TLabel
      Left = 58
      Top = 16
      Width = 50
      Height = 13
      Caption = 'Compa'#241'ia:'
    end
    object Label4: TLabel
      Left = 8
      Top = 40
      Width = 100
      Height = 13
      Caption = 'Unidad de Negocios:'
    end
    object Label5: TLabel
      Left = 29
      Top = 64
      Width = 78
      Height = 13
      Caption = 'Tipo de servicio:'
    end
    object Label6: TLabel
      Left = 411
      Top = 16
      Width = 25
      Height = 13
      Caption = 'Area:'
    end
    object Label7: TLabel
      Left = 391
      Top = 40
      Width = 45
      Height = 13
      Caption = 'Proyecto:'
    end
    object Compania: TEdit
      Left = 112
      Top = 12
      Width = 225
      Height = 21
      TabOrder = 0
    end
    object UnidadNegocio: TEdit
      Left = 112
      Top = 36
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object Area: TEdit
      Left = 440
      Top = 12
      Width = 121
      Height = 21
      TabOrder = 1
    end
    object Proyecto: TEdit
      Left = 440
      Top = 36
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object TipoServicio: TEdit
      Left = 112
      Top = 60
      Width = 121
      Height = 21
      TabOrder = 4
    end
  end
  object gbPasajero: TGroupBox
    Left = 7
    Top = 120
    Width = 682
    Height = 137
    Caption = ' Datos pasajero:  '
    TabOrder = 2
    object Label8: TLabel
      Left = 58
      Top = 16
      Width = 50
      Height = 13
      Caption = 'Compa'#241'ia:'
    end
    object Label9: TLabel
      Left = 66
      Top = 40
      Width = 40
      Height = 13
      Caption = 'Nombre:'
    end
    object Label10: TLabel
      Left = 80
      Top = 88
      Width = 24
      Height = 13
      Caption = 'RFC:'
    end
    object Label11: TLabel
      Left = 80
      Top = 112
      Width = 24
      Height = 13
      Caption = 'FM3:'
    end
    object Label12: TLabel
      Left = 379
      Top = 40
      Width = 59
      Height = 13
      Caption = 'Ap. Paterno:'
    end
    object Label13: TLabel
      Left = 44
      Top = 64
      Width = 61
      Height = 13
      Caption = 'Ap. Materno:'
    end
    object Label14: TLabel
      Left = 373
      Top = 64
      Width = 65
      Height = 13
      Caption = 'Nacionalidad:'
    end
    object Label15: TLabel
      Left = 361
      Top = 88
      Width = 71
      Height = 13
      Caption = 'No. Pasaporte:'
    end
    object CompaniaEmp: TEdit
      Left = 112
      Top = 12
      Width = 225
      Height = 21
      TabOrder = 0
    end
    object Nombre: TEdit
      Left = 112
      Top = 36
      Width = 225
      Height = 21
      TabOrder = 1
    end
    object RFC: TEdit
      Left = 112
      Top = 84
      Width = 225
      Height = 21
      TabOrder = 5
    end
    object FM3: TEdit
      Left = 112
      Top = 108
      Width = 225
      Height = 21
      TabOrder = 7
    end
    object ApPaterno: TEdit
      Left = 440
      Top = 36
      Width = 225
      Height = 21
      TabOrder = 2
    end
    object ApMaterno: TEdit
      Left = 112
      Top = 60
      Width = 225
      Height = 21
      TabOrder = 3
    end
    object Pasaporte: TEdit
      Left = 440
      Top = 84
      Width = 225
      Height = 21
      TabOrder = 6
    end
    object Nacionalidad: TComboBox
      Left = 440
      Top = 60
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 4
      Items.Strings = (
        'Mexicana')
    end
  end
  object gbServicio: TGroupBox
    Left = 7
    Top = 256
    Width = 682
    Height = 121
    Caption = ' Servicio solicitado: '
    TabOrder = 3
    object Label16: TLabel
      Left = 47
      Top = 24
      Width = 57
      Height = 13
      Caption = 'Movimiento:'
    end
    object Label17: TLabel
      Left = 71
      Top = 48
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object Label18: TLabel
      Left = 70
      Top = 72
      Width = 34
      Height = 13
      Caption = 'Origen:'
    end
    object Label19: TLabel
      Left = 373
      Top = 24
      Width = 65
      Height = 13
      Caption = 'Tipo solicitud:'
    end
    object Label20: TLabel
      Left = 399
      Top = 72
      Width = 39
      Height = 13
      Caption = 'Destino:'
    end
    object Label21: TLabel
      Left = 412
      Top = 48
      Width = 26
      Height = 13
      Caption = 'Hora:'
    end
    object Movimiento: TComboBox
      Left = 112
      Top = 20
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Embarque')
    end
    object Fecha: TDateTimePicker
      Left = 112
      Top = 44
      Width = 89
      Height = 21
      Date = 39085.482964768520000000
      Time = 39085.482964768520000000
      TabOrder = 2
    end
    object Origen: TComboBox
      Left = 112
      Top = 68
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 4
      Items.Strings = (
        'Cd. Carmen')
    end
    object TipoSolicitud: TComboBox
      Left = 440
      Top = 20
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'Por espacio')
    end
    object Destino: TComboBox
      Left = 440
      Top = 68
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 5
      Items.Strings = (
        'SAFE BRITANIA')
    end
    object Hora: TZetaHora
      Left = 440
      Top = 44
      Width = 40
      Height = 21
      EditMask = '99:99;0'
      TabOrder = 3
      Tope = 24
    end
  end
  object Panel1: TPanel
    Left = 8
    Top = 384
    Width = 681
    Height = 41
    TabOrder = 4
    object BitBtn1: TBitBtn
      Left = 251
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Solicitar'
      TabOrder = 0
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Left = 355
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Salir'
      TabOrder = 1
    end
  end
end
