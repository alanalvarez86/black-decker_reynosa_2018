unit dHelicopteros;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
{$ifndef VER130}
     Variants,
{$endif}
     ZBaseEdicion,
     FCamposMgr,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZetaCommonClasses, xmldom, Provider, Xmlxform, XMLIntf, msxmldom,
  XMLDoc;

{$define QUINCENALES}
{.$undefine QUINCENALES}

type
  TdmHelicopteros = class(TDataModule)
    cdsPendientes: TZetaClientDataSet;
    XMLTransformProvider1: TXMLTransformProvider;
    cdsDataTarea: TZetaClientDataSet;
    XMLTransformProvider2: TXMLTransformProvider;
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsPendientesAlAdquirirDatos(Sender: TObject);
    procedure cdsDataTareaAfterRowRequest(Sender: TObject;
      var OwnerData: OleVariant);
    procedure cdsDataTareaAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
         FTarea : string;
  protected
      { Protected declarations }
  public
    { Public declarations }
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    property Tarea : string read FTarea write FTarea;
  end;

var
  dmHelicopteros: TdmHelicopteros;

implementation

uses DCliente,
     DSistema,
     ZAccesosMgr,
     zAccesosTress,
     ZGlobalTress,
     ZReconcile,
     ZReportTools,
     ZetaDataSetTools,
     ZetaCommonTools,
     ZetaDialogo,
     ZetaMsgDlg,
     ZetaClientTools,
     ZBaseDlgModal,
     FTressShell,
     WS_Workflow;



{$R *.DFM}

{ TdmHelicopteros }

{$ifdef VER130}
procedure TdmHelicopteros.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmHelicopteros.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}


{$ifdef MULTIPLES_ENTIDADES}
procedure TdmHelicopteros.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmWebservice.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
end;


procedure TdmHelicopteros.cdsPendientesAlAdquirirDatos(Sender: TObject);
var
   ws : WS_WorkflowSOAP;
begin
      ws := GetWS_WorkflowSOAP();
      XMLTransformProvider1.TransformRead.SourceXml := ws.ProcesosPendientes(dmCliente.Usuario);
      cdsPendientes.Close;
      cdsPendientes.Open;
      //ZetaDialogo.ZInformation('Helicopteros Cotemar', sXML, 0 );
end;




procedure TdmHelicopteros.cdsDataTareaAfterRowRequest(Sender: TObject;
  var OwnerData: OleVariant);
begin

end;

procedure TdmHelicopteros.cdsDataTareaAlAdquirirDatos(Sender: TObject);
var
   ws : WS_WorkflowSOAP;
   sXML : STring;
begin
     ws := GetWS_WorkflowSOAP();
     sXML := ws.DatosTarea(Tarea, dmCliente.Usuario, 'HELI');
     XMLTransformProvider2.TransformRead.SourceXml := sXML;
     cdsDataTarea.Close;
     cdsDataTarea.Open;
end;

end.



