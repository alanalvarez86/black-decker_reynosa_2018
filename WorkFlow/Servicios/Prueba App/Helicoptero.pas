unit Helicoptero;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ZetaHora, ComCtrls,WS_Workflow;

type
  THelicopteros = class(TForm)
    Label1: TLabel;
    tipoPersonal: TComboBox;
    gbCargo: TGroupBox;
    gbPasajero: TGroupBox;
    gbServicio: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Compania: TEdit;
    UnidadNegocio: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    Area: TEdit;
    Proyecto: TEdit;
    Label8: TLabel;
    CompaniaEmp: TEdit;
    Label9: TLabel;
    Nombre: TEdit;
    Label10: TLabel;
    RFC: TEdit;
    Label11: TLabel;
    FM3: TEdit;
    ApPaterno: TEdit;
    Label12: TLabel;
    ApMaterno: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Pasaporte: TEdit;
    Nacionalidad: TComboBox;
    Label16: TLabel;
    Movimiento: TComboBox;
    Label17: TLabel;
    Fecha: TDateTimePicker;
    Origen: TComboBox;
    Label18: TLabel;
    TipoSolicitud: TComboBox;
    Label19: TLabel;
    Destino: TComboBox;
    Label20: TLabel;
    Label21: TLabel;
    Hora: TZetaHora;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    TipoServicio: TEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    ws : WS_WorkflowSOAP;
    FActualizar : boolean;
    procedure SetControlValues;
  public
    { Public declarations }
    property Actualizar : boolean read FActualizar write FActualizar;
  end;

var
  Helicopteros: THelicopteros;

implementation
 uses
     ZetaDialogo,
     ZetaCommonTools,
     dHelicopteros,
     DCliente;

{$R *.dfm}
const
K_XML_DATA = '<DATOS>'+
                       '<TIPOPERSONAL Titulo="Tipo personal:" Texto="%0:s">%20:d</TIPOPERSONAL>' +

                       '<COMPANIA Titulo="Compa�ia:" Tipo="T" Texto="%1:s">%1:s</COMPANIA>'+
                       '<AREA Titulo="Area:" Texto="%2:s">%2:s</AREA>'+
                       '<UNIDADNEGOCIOS Titulo="Unidad de negocios:" Tipo="T" Texto="%3:s">%3:s</UNIDADNEGOCIOS>'+
                       '<PROYECTO Titulo="Proyecto:" Tipo="T" Texto="%4:s">%4:s</PROYECTO>'+
                       '<TIPOSERVICIO Titulo="Tipo servicio:" Tipo="1" Texto="%5:s">%5:s</TIPOSERVICIO>'+


                       '<COMPANIAEMP Titulo="Compa�ia Empleado:" Tipo="T" Texto="%6:s">%6:s</COMPANIAEMP>'+
                       '<NOMBRE Titulo="Nombre:" Texto="%7:s">%7:s</NOMBRE>'+
                       '<APPATERNO Titulo="Ap. Paterno:" Tipo="T" Texto="%8:s">%8:s</APPATERNO>'+
                       '<APMATERNO Titulo="Ap. MAterno:" Tipo="T" Texto="%9:s">%9:s</APMATERNO>'+
                       '<NACIONALIDAD Titulo="Nacionalidad:" Tipo="I" Texto="%10:s">%21:d</NACIONALIDAD>'+
                       '<RFC Titulo="RFC:" Tipo="T" Texto="%11:s">%11:s</RFC>'+
                       '<PASAPORTE Titulo="No. Pasaporte:" Tipo="T" Texto="%12:s">%12:s</PASAPORTE>'+
                       '<FM3 Titulo="FM3:" Tipo="T" Texto="%13:s">%13:s</FM3>'+


                       '<MOVIMIENTO Titulo="Movimiento:" Tipo="T" Texto="%14:s">%22:d</MOVIMIENTO>'+
                       '<TIPOSOLICITUD Titulo="TipoSolicitud" Texto="%15:s">%23:d</TIPOSOLICITUD>'+
                       '<FECHA Titulo="Fecha:" Tipo="T" Texto="%16:s">%16:s</FECHA>'+
                       '<HORA Titulo="Hora:" Tipo="T" Texto="%17:s">%17:s</HORA>'+
                       '<ORIGEN Titulo="Origen:" Tipo="T" Texto="%18:s" Clase="">%24:d</ORIGEN>'+
                       '<DESTINO Titulo="Destino:" Tipo="T" Texto="%19:s" Clase="">%25:d</DESTINO>'+

             '</DATOS>';

procedure THelicopteros.BitBtn1Click(Sender: TObject);
var

   ReturnValue, sXML : String;
begin

     sXML  := Format(K_XML_DATA, [tipoPersonal.Text, Compania.Text, Area.Text, UnidadNegocio.Text, Proyecto.Text, TipoServicio.Text,
                                  CompaniaEmp.Text, Nombre.Text, ApPaterno.Text, ApMAterno.Text, Nacionalidad.Text, RFC.Text, Pasaporte.Text, FM3.Text,
                                  Movimiento.Text, TipoSolicitud.Text,ZetaCommonTools.DateToStrSql(Fecha.DateTime),Hora.Text, Origen.Text, Destino.Text,
                                  tipoPersonal.ItemIndex, Nacionalidad.ItemIndex, Movimiento.ItemIndex, TipoSolicitud.ItemIndex, Origen.ItemIndex, Destino.ItemIndex]);
     if not FActualizar then
     begin

          ReturnValue := ws.CrearProceso('HELI', dmCliente.Usuario, dmCliente.Usuario, sXML, 'Solicitud con WS');
     end
     else
         ReturnValue := ws.ActualizarProceso(dmHelicopteros.Tarea, dmCliente.Usuario, dmCliente.Usuario, '1', sXML, 'Actualizacion con WS');

     ZetaDialogo.ZInformation('Helicopteros Cotemar', ReturnValue, 0 );
     Self.Close;
end;

procedure THelicopteros.FormShow(Sender: TObject);
begin
     if (not Assigned (ws ) ) then
        ws := GetWS_WorkflowSOAP();
     if FActualizar then
     begin
          SetControlValues;
     end;

end;

procedure THelicopteros.SetControlValues;
begin
      with dmHelicopteros.cdsDataTarea do
      begin
           Conectar;
           if RecordCount > 0 then
           begin
                tipoPersonal.ItemIndex := FieldByName( 'TIPOPERSONAL' ).AsInteger;
                Compania.Text := FieldByName( 'COMPANIA' ).AsSTring;
                Area.Text := FieldByName( 'AREA' ).AsString;
                UnidadNegocio.Text := FieldByName( 'UNIDADNEGOCIOS' ).AsString;
                Proyecto.Text := FieldByName( 'PROYECTO' ).AsString;
                TipoServicio.Text := FieldByName( 'TIPOSERVICIO' ).AsString;
                CompaniaEmp.Text := FieldByName( 'COMPANIAEMP' ).AsString;
                Nombre.Text := FieldByName( 'NOMBRE' ).AsString;
                ApPaterno.Text := FieldByName( 'APPATERNO' ).AsSTring;
                ApMAterno.Text := FieldByName( 'APMATERNO' ).AsString;
                Nacionalidad.ItemIndex := FieldByName( 'NACIONALIDAD' ).AsInteger;
                RFC.Text := FieldByName( 'RFC' ).AsString;
                Pasaporte.Text := FieldByName( 'PASAPORTE' ).AsString;
                FM3.Text := FieldByName( 'FM3' ).AsString;
                Movimiento.ItemIndex := FieldByName( 'MOVIMIENTO' ).AsInteger;
                TipoSolicitud.ItemIndex := FieldByName( 'TIPOSOLICITUD' ).AsInteger;
                //Fecha.DateTime := StrToDate(FieldByName( 'FECHA' ).AsString);
                Hora.Text := FieldByName( 'HORA' ).AsString;
                Origen.ItemIndex := FieldByName( 'ORIGEN' ).AsInteger;
                Destino.ItemIndex := FieldByName( 'DESTINO' ).AsInteger;
           end;
      end;
end;

end.
