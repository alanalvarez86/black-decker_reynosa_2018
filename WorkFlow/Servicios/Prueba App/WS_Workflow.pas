// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/workflow5/WS_Workflow.asmx?WSDL
// Encoding : utf-8
// Version  : 1.0
// (08/01/2007 11:24:43 a.m. - 1.33.2.5)
// ************************************************************************ //

unit WS_Workflow;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"



  // ************************************************************************ //
  // Namespace : http://tempuri.org/
  // soapAction: http://tempuri.org/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : WS_WorkFlowSoap
  // service   : WS_WorkFlow
  // port      : WS_WorkFlowSoap
  // URL       : http://localhost/workflow5/WS_Workflow.asmx
  // ************************************************************************ //
  WS_WorkFlowSoap = interface(IInvokable)
  ['{A17F8775-FE02-934A-6106-B081FD2FE8E8}']
    function  CrearProceso(const Modelo: WideString; const UsuarioInicial: Integer; const UsuarioDestino: Integer; const XMLData: WideString; const Notas: WideString): WideString; stdcall;
    function  ActualizarProceso(const IdTarea: WideString; const UsuarioInicial: Integer; const UsuarioDestino: Integer; const Accion: WideString; const XMLData: WideString; const Notas: WideString): WideString; stdcall;
    function  DatosTarea(const IdTarea: WideString; const Usuario: Integer; const Modelo: WideString): WideString; stdcall;
    function  DatosProceso(const Folio: Integer; const Modelo: WideString): WideString; stdcall;
    function  ProcesosPendientes(const Usuario: Integer): WideString; stdcall;
  end;

function GetWS_WorkFlowSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): WS_WorkFlowSoap;


implementation

function GetWS_WorkFlowSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): WS_WorkFlowSoap;
const
  defWSDL = 'http://localhost/workflow/WS_Workflow.asmx?WSDL';
  defURL  = 'http://localhost/workflow/WS_Workflow.asmx';
  defSvc  = 'WS_WorkFlow';
  defPrt  = 'WS_WorkFlowSoap';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as WS_WorkFlowSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(WS_WorkFlowSoap), 'http://tempuri.org/', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(WS_WorkFlowSoap), 'http://tempuri.org/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(WS_WorkFlowSoap), ioDocument);

end. 