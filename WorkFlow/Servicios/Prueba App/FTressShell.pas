unit FTressShell;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Buttons,
     Forms, Dialogs, ExtCtrls, ComCtrls, Db, Grids, DBGrids, StdCtrls,
     ZetaKeyCombo, ImgList, ActnList, Menus, ZetaStateComboBox, ZetaSmartLists,
     ZetaDBTextBox, Mask, ZetaNumero, ZetaFecha, ZetaDBGrid,TDMULTIP,
     ZBaseShell,
     ZetaMessages,
     ZetaCommonClasses,
     ZBaseConsulta,
     ZetaTipoEntidad,
     ToolWin,
     {$ifdef VER150}
     XPMan,
     {$endif}     
     Outlook;

type
  TTressShell = class(TBaseShell)
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    ArbolImages: TImageList;
    Menu: TMainMenu;
    Archivo1: TMenuItem;
    OtraEmpresa1: TMenuItem;
    Servidor1: TMenuItem;
    Salir1: TMenuItem;
    Ayuda1: TMenuItem;
    Acercade1: TMenuItem;
    datasource: TDataSource;
    Paneloutlook: TPanel;
    Splitter1: TSplitter;
    PanelPrincipal: TPanel;
    PanelTop: TPanel;
    BarraBotones: TToolBar;
    Agregar: TZetaSpeedButton;
    Borrar: TZetaSpeedButton;
    Modificar: TZetaSpeedButton;
    BuscarCodigoBtn: TZetaSpeedButton;
    ToolButton1: TToolButton;
    Refrescar: TZetaSpeedButton;
    ToolButton2: TToolButton;
    Imprimir: TZetaSpeedButton;
    ToolButton3: TToolButton;
    PanelConsulta: TPanel;
    Outlook: TOutlook;
    PanelTitulo: TPanel;
    LblFormato: TLabel;
    ZetaSpeedButton1: TZetaSpeedButton;
    Exportar: TZetaSpeedButton;
    ActionList: TActionList;
    _A_OtraEmpresa: TAction;
    _A_Servidor: TAction;
    _A_Salir: TAction;
    _H_AcercaDe: TAction;
    _P_Imprimir: TAction;
    _Per_Primero: TAction;
    _Per_Siguiente: TAction;
    _Per_Anterior: TAction;
    _Per_Ultimo: TAction;
    _A_BuscaEmpleado: TAction;
    _A_Primero: TAction;
    _A_Anterior: TAction;
    _A_Siguiente: TAction;
    _A_Ultimo: TAction;
    _E_Agregar: TAction;
    _E_Modificar: TAction;
    _E_Borrar: TAction;
    _A_Imprimir: TAction;
    Registro: TMenuItem;
    N3: TMenuItem;
    _A_Exportar: TAction;
    _P_Obtener_: TAction;
    Procesos1: TMenuItem;
    N1: TMenuItem;
    ConfiguracindelSistema1: TMenuItem;
    _P_CambioRegistroPatronal: TAction;
    _R_MisPendientes: TAction;
    _R_Helicopteros: TAction;
    Obtenerdatosbasededatos1: TMenuItem;
    SolicitudHelicoptero1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _A_OtraEmpresaExecute(Sender: TObject);
    procedure _A_ServidorExecute(Sender: TObject);
    procedure _A_SalirExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _H_AcercaDeExecute(Sender: TObject);
    procedure OutlookItemClick(Sender: TObject; Item: String);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_RefrescarExecute(Sender: TObject);
    procedure _A_PrimeroExecute(Sender: TObject);
    procedure _A_AnteriorExecute(Sender: TObject);
    procedure _A_SiguienteExecute(Sender: TObject);
    procedure _A_UltimoExecute(Sender: TObject);
    procedure _E_ExportarExecute(Sender: TObject);
    procedure _Per_AnteriorUpdate(Sender: TObject);
    procedure _Per_SiguienteUpdate(Sender: TObject);
    procedure _A_ExportarExecute(Sender: TObject);
    procedure _R_HelicopterosExecute(Sender: TObject);
    procedure _R_MisPendientesExecute(Sender: TObject);
    procedure _Per_GenerarPolizaExecute(Sender: TObject);
  private
    { Private declarations }
    FParameterList: TZetaParams;
    FFormaActiva: TBaseConsulta;
    FLastDescription: string;
    FCodigoEmpresa: string;
    function GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean ): TBaseConsulta;
    procedure ActivaFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: string; const iDerecho: integer );
    procedure RefeshFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: string; const iDerecho: integer );
    procedure SelectFormaOutLook( const sDescrip: String );
    procedure SetFormaRefresh( const sDescrip: String );
    procedure SetFormaActiva(Forma: TBaseConsulta);
    procedure CierraFormaActiva;
    procedure RefrescaEmpleadoActivos;
    procedure CambioEmpleadoActivo;
    procedure InicializaActions;



    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
  protected
    { Protected declarations }
    property ParameterList: TZetaParams read FParameterList;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
  public
    { Public declarations }
    function FormaActivaNomina: Boolean;
    function FormaActiva: TBaseConsulta;
    procedure SetDataChange(const Entidades: array of TipoEntidad);
    procedure SetBloqueo;
    procedure RefrescaIMSS;
    procedure ReinitPeriodo;
    procedure ChangeTimerInfo;
    procedure RefrescaEmpleado;
    procedure GetFechaLimiteShell;
    procedure CambiaEmpleadoActivos;
    procedure ReconectaFormaActiva;
    procedure CargaEmpleadoActivos;
    procedure CreaArbol(const lEjecutar: Boolean);
    procedure SetAsistencia(const dValue: TDate);
    procedure BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
    property CodigoEmpresa: string read FCodigoEmpresa write FCodigoEmpresa;
  end;

var
  TressShell: TTressShell;

implementation

uses ZetaDialogo, ZetaCommonlists, ZetaCommonTools, ZetaWinApiTools,
     ZAccesosTress, ZGlobalTress, ZetaClientTools, ZetaBuscaEmpleado,
     ZetaAcercaDe, ZWizardBasico,ZetaSystemWorking, ZAccesosMgr,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     Dconsultas, DGlobal, DDiccionario, DSistema,
     DTablas, DRecursos, DCliente, Helicoptero, FCatPendientes, dHelicopteros;

{$R *.DFM}

const
     K_PANEL_EMPLEADO  = 1;
     K_PANEL_PERIODO   = 2;
     K_FORMA_HELICOPTEROS = 'Helicopteros';
     K_FORMA_MIS_PENDIENTES = 'Mis pendientes';
     //K_FORMA_CONSULTA  = 'Consulta General';
     K_CODIGO_EMPRESA  = 'TRESS';


{ ***** Metodos de Creaci�n y destrucci�n del Shell ***** }
procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmTablas := TdmTablas.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmRecursos := TdmRecursos.Create ( Self );
     dmHelicopteros := TdmHelicopteros.Create( Self );
     FCodigoEmpresa := K_CODIGO_EMPRESA;
     inherited;
     FParameterList := TZetaParams.Create;
     WindowState:= wsMaximized;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( dmRecursos );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmTablas );
     FreeAndNil( dmSistema );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmCliente );
     FreeAndNil( dmHelicopteros );
     Global.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll( True );
     DZetaServerProvider.FreeAll;
     {$endif}
     FParameterList.Free;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     OutLook.ActiveTab := 1;
     FLastDescription := VACIO;
     InicializaActions;
     dmCliente.FechaAsistencia := Date;

end;

{ ***** M�todos del Action ***** }
procedure TTressShell._A_OtraEmpresaExecute(Sender: TObject);
begin
     DoCloseAll;
     InicializaActions;
     AbreEmpresa( False );
end;

procedure TTressShell._A_ServidorExecute(Sender: TObject);
begin
     DoCloseAll;
     CambiaServidor;
end;

procedure TTressShell._A_SalirExecute(Sender: TObject);
begin
     Close;
end;

procedure TTressShell._H_AcercaDeExecute(Sender: TObject);
begin
     with TZAcercaDe.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

{ ***** Metodos de la forma ***** }
procedure TTressShell.KeyPress(var Key: Char);
begin
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

function TTressShell.FormaActivaNomina: Boolean;
begin
     Result := FALSE;
end;

procedure TTressShell.DoOpenAll;
begin
     inherited DoOpenAll;
     Global.Conectar;
     with dmCliente do
     begin
          InitActivosSistema;
          InitActivosEmpleado;
          InitActivosPeriodo;
          FCodigoEmpresa := Compania;
     end;
     CargaEmpleadoActivos;
end;

procedure TTressShell.ActivaFormaPanel( InstanceClass: TBaseConsultaClass; const InstanceName: String; const iDerecho: integer );
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     Forma := GetForma( InstanceClass, InstanceName, lNueva );
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               IndexDerechos := iDerecho;
               try
                  if lNueva then
                     DoConnect
                  else
                     Reconnect;
                  Show;
                  SetFormaActiva( Forma );
               except
                    Free;
               end;
          end;
     end
     else
         zError( '�Error Al Crear Forma!', '�Ventana No Pudo Ser Creada!', 0 );
end;

function TTressShell.GetForma(InstanceClass: TBaseConsultaClass; const InstanceName: String; var lNueva: Boolean): TBaseConsulta;
begin
     lNueva := FALSE;
     Result := TBaseConsulta( FindComponent( InstanceName ) );
     if not Assigned( Result ) then
     begin
          try
             Result := InstanceClass.Create( Self ) as TBaseConsulta;
             with Result do
             begin
                  Name := InstanceName;
                  Parent := Self.PanelConsulta;
             end;
             lNueva := TRUE;
          except
             Result := nil;
          end;
     end;
end;

procedure TTressShell.SelectFormaOutLook( const sDescrip: String );
begin
     if ( sDescrip = K_FORMA_MIS_PENDIENTES ) then
     begin
          ActivaFormaPanel( TCatPendientes, 'Pendientes', 0 ) ;
     end;
     FLastDescription := sDescrip;
end;

procedure TTressShell.SetFormaActiva(Forma: TBaseConsulta);
begin
     if ( FFormaActiva <> Forma ) then
     begin
          if ( Assigned( FFormaActiva ) ) then
          begin
               FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
               FFormaActiva.Close;
          end;
          FFormaActiva := Forma;
          if ( Assigned( FFormaActiva ) ) then
          begin
               with FFormaActiva do
               begin
                    _E_Agregar.Enabled := CheckDerechos( K_DERECHO_ALTA );
                    _E_Borrar.Enabled := CheckDerechos( K_DERECHO_BAJA );
                    _E_Modificar.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
                    _A_Imprimir.Enabled := CheckDerechos( K_DERECHO_IMPRESION );
                    HelpContext:= FFormaActiva.HelpContext;
                    SetFocus;
               end;
          end
          else
              InicializaActions;
     end;
end;

procedure TTressShell.OutlookItemClick(Sender: TObject; Item: String);
begin
     PanelTitulo.Caption := Item;
     if ( Item = 'Globales' ) then
     begin
          if (  dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION ) then
             SelectFormaOutLook( Item )
          else
              ZInformation( 'Evaluaciones', 'No se tienen derechos para editar el contenido', 0);
     end
     else
         SelectFormaOutLook( Item );
end;

procedure TTressShell.DoCloseAll;
begin
     inherited DoCloseAll;
     CierraFormaActiva;
end;

procedure TTressShell.CierraFormaActiva;
begin
     if ( Assigned( FFormaActiva ) ) then
     begin
          FFormaActiva.DoDisconnect;  { Cerrar Datos de Forma Anterior }
          FFormaActiva.Close;
     end;
     FLastDescription := VACIO;
     PanelTitulo.Caption := VACIO;
end;

procedure TTressShell.CargaEmpleadoActivos;
begin
     RefrescaEmpleadoActivos;
end;

procedure TTressShell._E_AgregarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoInsert;
     end;
end;

procedure TTressShell._E_BorrarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoDelete;
     end;
end;

procedure TTressShell._E_ModificarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoEdit;
     end;
end;

procedure TTressShell._A_ImprimirExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoPrint;
     end;
end;

procedure TTressShell._E_RefrescarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoRefresh;
     end;
end;

procedure TTressShell.RefrescaEmpleadoActivos;
begin
     with dmCliente do
     begin
          StatusBarMsg( 'Empleado: '+ IntToStr( Empleado ), K_PANEL_EMPLEADO );
          if strLleno( FLastDescription ) then
             SetFormaRefresh( FLastDescription );
     end;
end;

procedure TTressShell._A_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoPrimero then
             begin
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._A_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoAnterior then
             begin
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._A_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoSiguiente then
             begin
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._A_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmCliente do
        begin
             if GetEmpleadoUltimo then
             begin
                  CambioEmpleadoActivo;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CambioEmpleadoActivo;
begin
     RefrescaEmpleadoActivos;
end;

procedure TTressShell.SetFormaRefresh(const sDescrip: String);
begin
     if ( sDescrip = K_FORMA_MIS_PENDIENTES ) then
     begin
          ActivaFormaPanel( TCatPendientes, 'Pendientes', 0 ) ;
     end;
     FLastDescription := sDescrip;
end;

procedure TTressShell.RefeshFormaPanel(InstanceClass: TBaseConsultaClass; const InstanceName: string; const iDerecho: integer);
var
   Forma: TBaseConsulta;
   lNueva : Boolean;
begin
     Forma := GetForma( InstanceClass, InstanceName, lNueva );
     if Assigned( Forma ) then
     begin
          with Forma do
          begin
               IndexDerechos := iDerecho;
               try
                  if lNueva then
                     DoRefresh
                  else
                     DoRefresh;
                  Show;
                  SetFormaActiva( Forma );
               except
                    Free;
               end;
          end;
     end
     else
          zError( '� Error Al Crear Forma !', '� Ventana No Pudo Ser Creada !', 0 );
end;

procedure TTressShell._E_ExportarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoExportar;
     end;
end;

procedure TTressShell.InicializaActions;
begin
     _E_Agregar.Enabled := False;
     _E_Borrar.Enabled := False;
     _E_Modificar.Enabled := False;
     _A_Imprimir.Enabled := False;
end;

procedure TTressShell._Per_AnteriorUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoDownEnabled;
end;

procedure TTressShell._Per_SiguienteUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmCliente.PeriodoUpEnabled;
end;

procedure TTressShell.WMWizardEnd(var Message: TMessage);
begin
     with Message do
     begin
          //dmProcesos.HandleProcessEnd( WParam, LParam );
     end;
end;

procedure TTressShell._A_ExportarExecute(Sender: TObject);
begin
     inherited;
     if Assigned( FFormaActiva ) then
     begin
          FFormaActiva.DoExportar;
     end;
end;

{ Procedimientos y Funciones Publicos del Shell de Tress }
procedure TTressShell.RefrescaEmpleado;
begin
     // No Implementar
end;

procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
begin
     // No Implementar
end;

procedure TTressShell.CambiaEmpleadoActivos;
begin
     // No Implementar
end;

procedure TTressShell.SetAsistencia(const dValue: TDate);
begin
     // No Implementar
end;

procedure TTressShell.BuscaNuevoPeriodo(const iTipo, iPeriodoBorrado: Integer);
begin
     // No Implementar
end;

procedure TTressShell.RefrescaIMSS;
begin
     // No Implementar
end;

procedure TTressShell.ReinitPeriodo;
begin
     // No Implementar
end;

procedure TTressShell.CreaArbol(const lEjecutar: Boolean);
begin
     // No Implementar
end;

procedure TTressShell.ReconectaFormaActiva;
begin
     // No Implementar
end;

procedure TTressShell.ChangeTimerInfo;
begin
     // No Implementar
end;

procedure TTressShell.GetFechaLimiteShell;
begin
     // No Implementar
end;

procedure TTressShell.SetBloqueo;
begin
     // No Implementar
end;



function TTressShell.FormaActiva: TBaseConsulta;
begin
     Result := Nil;
end;

procedure TTressShell._R_HelicopterosExecute(Sender: TObject);
var
   oHelicoptero : THelicopteros;
begin
     try
        oHelicoptero := THelicopteros.Create(Application);
        with oHelicoptero do
        begin
             Actualizar := False;
             ShowModal;
        end;
     finally
            FreeAndNil(oHelicoptero);
     end;

end;

procedure TTressShell._R_MisPendientesExecute(Sender: TObject);
begin
     PanelTitulo.Caption := K_FORMA_MIS_PENDIENTES;
     SelectFormaOutLook( K_FORMA_MIS_PENDIENTES );
end;

procedure TTressShell._Per_GenerarPolizaExecute(Sender: TObject);
begin
     inherited;
     { No se Hace Nada }
end;

end.

