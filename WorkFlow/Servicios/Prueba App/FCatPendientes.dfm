inherited CatPendientes: TCatPendientes
  Left = 141
  Top = 325
  Caption = 'Mis Pendientes'
  ClientHeight = 257
  ClientWidth = 849
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 849
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 489
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 849
    Height = 238
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = ZetaDBGridDblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'WP_FOLIO'
        Title.Caption = 'Folio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WP_NOMBRE'
        Title.Caption = 'Proceso'
        Width = 180
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PASOACTUAL'
        Title.Caption = 'Paso'
        Width = 115
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WS_NOMBRE'
        Title.Caption = 'Descripci'#243'n'
        Width = 226
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 152
    Top = 8
  end
end
