program HelicopteroApp;

uses
  Forms,
  Helicoptero in 'Helicoptero.pas' {Helicopteros},
  FTressShell in 'FTressShell.pas' {TressShell},
  WS_Workflow in 'WS_Workflow.pas',
  ZBaseShell in 'D:\3Win_20\Tools\ZBaseShell.pas' {BaseShell},
  ZetaSplash in 'D:\3Win_20\Tools\ZetaSplash.pas' {SplashScreen};

{$R *.res}
procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;

begin
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'Helicopteros COTEMAR';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( True ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
