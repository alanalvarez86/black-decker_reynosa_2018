// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : http://localhost/motor2/WorkflowService.asmx?WSDL
// Encoding : utf-8
// Version  : 1.0
// (03/01/2007 08:35:04 a.m. - 1.33.2.5)
// ************************************************************************ //

unit WorkflowService;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns, SOAPHTTPTrans;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"
  // !:int             - "http://www.w3.org/2001/XMLSchema"


  // ************************************************************************ //
  // Namespace : urn:WorkflowServiceIntf-IWorkflowService
  // soapAction: urn:WorkflowServiceIntf-IWorkflowService/ComunicaTress
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : IWorkflowServiceserviceSoap
  // service   : IWorkflowServiceservice
  // port      : IWorkflowServiceserviceSoap
  // URL       : http://localhost/motor2/WorkflowService.asmx
  // ************************************************************************ //
  IWorkflowServiceserviceSoap = interface(IInvokable)
  ['{BC81B082-FB45-B423-6348-1D27A1404027}']
    function  ComunicaTress(const Data: WideString; const UsuarioMotor: Integer): WideString; stdcall;
  end;

function GetIWorkflowServiceserviceSoap(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil; Username: string  = ''; Password: string = ''): IWorkflowServiceserviceSoap;


implementation

function GetIWorkflowServiceserviceSoap(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO; Username: string; Password: string): IWorkflowServiceserviceSoap;
const
  defWSDL = 'http://localhost/motor2/WorkflowService.asmx?WSDL';
  defURL  = 'http://localhost/motor2/WorkflowService.asmx';
  defSvc  = 'IWorkflowServiceservice';
  defPrt  = 'IWorkflowServiceserviceSoap';
var
  RIO: THTTPRIO;
  HTTPReqResp1:  THTTPReqResp;
begin
  HTTPReqResp1 := THTTPReqResp.create(nil);
  HTTPReqResp1.Username := UserName;
  HTTPReqResp1.Password := Password;
  HTTPReqResp1.UseUTF8InHeader := True;
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as IWorkflowServiceserviceSoap);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
    RIO.HTTPWebNode := HTTPReqResp1;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(IWorkflowServiceserviceSoap), 'urn:WorkflowServiceIntf-IWorkflowService', 'utf-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(IWorkflowServiceserviceSoap), 'urn:WorkflowServiceIntf-IWorkflowService/ComunicaTress');

end. 