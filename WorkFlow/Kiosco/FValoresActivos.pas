unit FValoresActivos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, SHDocVw, ExtCtrls, fcImage, fcimageform, fcProgressBar, fcLabel;

type
  TValoresActivos = class(TForm)
    ValoresActivosURL: TWebBrowser;
    TimerSalida: TTimer;
    fcProgressBar1: TfcProgressBar;
    fcImageForm1: TfcImageForm;
    fcLabel1: TfcLabel;
    procedure ValoresActivosURLNavigateComplete2(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure FormShow(Sender: TObject);
    procedure TimerSalidaTimer(Sender: TObject);
    procedure ValoresActivosURLProgressChange(Sender: TObject; Progress,
      ProgressMax: Integer);
  private
    { Private declarations }
    FURL: String;
    //FLoop: Boolean;
    //FTimeOut: Integer;
    procedure ResetTimerSalida;
  public
    { Public declarations }
    property URL: String read FURL write FURL;
    //property TimeOut: Integer read FTimeOut write FTimeOut;
    //function Enviar: Boolean;
  end;

function EnviaValoresActivos( const sURL: String; var sProblema: String ): Boolean;

implementation

uses DCliente;

var
  ValoresActivos: TValoresActivos;

{$R *.DFM}

function EnviaValoresActivos( const sURL: String; var sProblema: String ): Boolean;
begin
     if not Assigned( ValoresActivos ) then
        ValoresActivos := TValoresActivos.Create( Application );
     Result := False;
     try
        with ValoresActivos do
        begin
             URL := sURL;
             //TimeOut := iTimeOut;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if not Result then
                sProblema := 'Demasiada tardanza al enviar valores activos';
        end;
     except
           on Error: Exception do
           begin
                sProblema := 'Error al enviar valores activos: ' + Error.Message;
           end;
     end;
end;

{ ******** TValoresActivos ********* }

{function TValoresActivos.Enviar: Boolean;
var
   oCursor: TCursor;
   dStart, dTimeOut: TDateTime;
begin
     FLoop := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ValoresActivosURL.Navigate( URL );
        dStart := Now;
        dTimeOut := EncodeTime( 0, 0, 0, FTimeOut );
        Result := True;
        while FLoop do
        begin
             if ( ( Now - dStart ) >= dTimeOut ) then
             begin
                  FLoop := False;
                  Result := False;
             end
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;}

procedure TValoresActivos.FormShow(Sender: TObject);
begin
     ResetTimerSalida;
     ValoresActivosURL.Navigate( URL );
end;

procedure TValoresActivos.ValoresActivosURLNavigateComplete2( Sender: TObject;
          const pDisp: IDispatch; var URL: OleVariant);
begin
     ModalResult := mrOk;
end;

procedure TValoresActivos.ResetTimerSalida;
begin
     with TimerSalida do
     begin
          Enabled := False;
          Interval := dmCliente.TimeoutDatos;
          Enabled := True;
     end;
end;

procedure TValoresActivos.TimerSalidaTimer(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

procedure TValoresActivos.ValoresActivosURLProgressChange(Sender: TObject;
  Progress, ProgressMax: Integer);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        fcProgressBar1.Progress := fcProgressBar1.Step + Progress;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
