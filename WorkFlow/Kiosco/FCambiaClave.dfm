inherited CambiaClave: TCambiaClave
  Left = 459
  Top = 89
  Caption = 'CambiaClave'
  ClientHeight = 389
  ClientWidth = 217
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited fcImagenFondo: TfcImager
    Width = 217
    Height = 389
  end
  inherited fcPanel2: TfcPanel
    Width = 217
    Height = 389
    inherited fcPanel1: TfcPanel
      Width = 217
      DesignSize = (
        217
        112)
      inherited PanelMensaje: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited PanelClave: TfcShapeBtn
        NumGlyphs = 0
      end
    end
    inherited PanelTeclado: TfcPanel
      Width = 217
      Height = 277
      inherited fcNumero1: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero2: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero3: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero4: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero5: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero6: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero7: TfcShapeBtn
        NumGlyphs = 0
      end
      inherited fcNumero8: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero9: TfcShapeBtn
        NumGlyphs = 1
      end
      inherited fcNumero0: TfcShapeBtn
        NumGlyphs = 1
      end
    end
  end
  object TimerSalida: TTimer
    Enabled = False
    OnTimer = TimerSalidaTimer
    Left = 7
    Top = 12
  end
end
