unit FConfiguraKiosco;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Mask, ExtCtrls,
     fcCombo, fcColorCombo, fcImage, fcImageForm, fcPanel,
     fcButton, fcImgBtn, fcShapeBtn,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaKeyCombo,
     FKioscoRegistry, CheckLst;

type
  TConfiguraKiosco = class(TForm)
    fcImageForm: TfcImageForm;
    KioscoLBL: TLabel;
    IntentosLBL: TLabel;
    edIntentos: TZetaNumero;
    btnEscribir: TfcShapeBtn;
    btnCancelar: TfcShapeBtn;
    cbKiosco: TZetaKeyCombo;
    ErrorText: TLabel;
    gbAdministrador: TfcGroupBox;
    ClaveLBL: TLabel;
    edClave: TEdit;
    edConfirmacion: TEdit;
    ConfirmacionLBL: TLabel;
    GafeteAdminLBL: TLabel;
    edGafeteAdmin: TEdit;
    fcAutoLogon: TfcGroupBox;
    DomainDefaultLBL: TLabel;
    DomainDefault: TEdit;
    UsuarioDefault: TEdit;
    UsuarioDefaultLBL: TLabel;
    PasswordDefaultLBL: TLabel;
    PasswordDefault: TEdit;
    PasswordConfirm: TEdit;
    PasswordConfirmLBL: TLabel;
    LogonAutoAdminLBL: TLabel;
    LogonAutoAdmin: TCheckBox;
    fcDefaultsGafete: TfcGroupBox;
    DefaultLetraCredencialLBL: TLabel;
    DefaultDigitoEmpresaLBL: TLabel;
    DefaultLetraCredencial: TEdit;
    DefaultDigitoEmpresa: TEdit;
    fcGroupBox1: TfcGroupBox;
    lbEmpresaAutorizadas: TCheckListBox;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEscribirClick(Sender: TObject);
    procedure CambioEnDatos(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LogonAutoAdminClick(Sender: TObject);
  private
    function GetListaEmpresasAutorizadas: string;
    procedure SetListaEmpresasAutorizadas(const Value: string);

  private
    { Private declarations }
    FRegistry: TLogonRegistry;
    procedure CargaListaKioscos;
    procedure CargaListaEmpresas;
    procedure LeeValoresRegistry;
    procedure EscribeRegistry;
    procedure EscribeError(const sError: String);
    procedure ResetError;
    procedure SetControls;
    property ListaEmpresasAutorizadas : string read GetListaEmpresasAutorizadas write SetListaEmpresasAutorizadas;
  public
    { Public declarations }
    function Init: Boolean;
  end;

function ConfiguraKiosco: Boolean;

implementation

uses DCliente,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo;

function ConfiguraKiosco: Boolean;
var
   oConfiguraKiosco: TConfiguraKiosco;
begin
     Result := False;
     oConfiguraKiosco := TConfiguraKiosco.Create( Application );
     try
        with oConfiguraKiosco do
        begin
             if Init then
             begin
                  ShowModal;
                  Result := ( ModalResult = mrOk );
             end;
        end;
     finally
            FreeAndNil( oConfiguraKiosco );
     end;
end;

{$R *.DFM}

{ ***** TConfiguraKiosco ***** }

procedure TConfiguraKiosco.FormCreate(Sender: TObject);
begin
     FRegistry := TLogonRegistry.Create;
end;

procedure TConfiguraKiosco.FormShow(Sender: TObject);
begin
     SetControls;
end;

procedure TConfiguraKiosco.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
end;

function TConfiguraKiosco.Init: Boolean;
var
   oCursor: TCursor;
begin
     Result := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           CargaListaKioscos;
           CargaListaEmpresas;
           LeeValoresRegistry;
           Result := True;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfiguraKiosco.CargaListaKioscos;
begin
     with dmCliente.cdsKioscos do
     begin
          while not EOF do
          begin
               cbKiosco.Lista.Add( FieldByName('KW_CODIGO').AsString +'='+ FieldByName('KW_NOMBRE').AsString );
               Next;
          end;
     end;
end;

procedure TConfiguraKiosco.CargaListaEmpresas;
 var
    sDigito:Char;
begin
     with lbEmpresaAutorizadas.Items do
     begin
          BeginUpdate;
          try
             Clear;
             with dmCliente.cdsCompany do
             begin
                  Data := dmCliente.Servidor.GetCompanys( 0, Ord( tc3Datos ) );
                  while not Eof do
                  begin
                       if StrLleno( FieldByName( 'CM_DIGITO' ).AsString ) then
                       begin
                            sDigito := FieldByName( 'CM_DIGITO' ).AsString[1];
                            AddObject( Format( '(%s) %s:%s', [ FieldByName( 'CM_DIGITO' ).AsString, FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ), TObject( ORD( sDigito ) ) );
                       end;
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;     
end;



procedure TConfiguraKiosco.LeeValoresRegistry;
begin
     with KioskoRegistry do
     begin
          cbKiosco.Llave := CodigoKiosco;
          edGafeteAdmin.Text := GafeteAdministrador;
          edClave.Text := PasswordSalida;
          edIntentos.Valor := Intentos;
          DefaultLetraCredencial.Text := LetraGafete;
          DefaultDigitoEmpresa.Text := DigitoEmpresa;
          ListaEmpresasAutorizadas := EmpresasAutorizadas;
     end;
     with FRegistry do
     begin
          DomainDefault.Text := DefaultDomainName;
          UsuarioDefault.Text := DefaultUserName;
          PasswordDefault.Text := DefaultPassword;
          PasswordConfirm.Text := DefaultPassword;
          LogonAutoAdmin.Checked := AutoAdminLogon;
     end;
end;

procedure TConfiguraKiosco.EscribeRegistry;
begin
     with KioskoRegistry do
     begin
          CodigoKiosco := cbKiosco.Llave;
          GafeteAdministrador := edGafeteAdmin.Text;
          PasswordSalida := edClave.Text;
          Intentos := edIntentos.ValorEntero;
          LetraGafete := DefaultLetraCredencial.Text;
          DigitoEmpresa := DefaultDigitoEmpresa.Text;
          EmpresasAutorizadas := ListaEmpresasAutorizadas;
     end;
     if LogonAutoAdmin.Checked then
     begin
          with FRegistry do
          begin
               DefaultDomainName := DomainDefault.Text;
               DefaultUserName := UsuarioDefault.Text;
               DefaultPassword := PasswordDefault.Text;
               AutoAdminLogon := LogonAutoAdmin.Checked;
          end;
     end
     else
     begin
          with FRegistry do
          begin
               DefaultDomainName := VACIO;
               DefaultUserName := VACIO;
               DefaultPassword := VACIO;
               AutoAdminLogon := FALSE;
          end;
     end;

end;

procedure TConfiguraKiosco.ResetError;
begin
     with ErrorText do
     begin
          Visible := False;
     end;
end;

procedure TConfiguraKiosco.EscribeError( const sError: String );
begin
     with ErrorText do
     begin
          Caption := sError;
          Visible := True;
     end;
end;

procedure TConfiguraKiosco.CambioEnDatos(Sender: TObject);
begin
     ResetError;
end;

procedure TConfiguraKiosco.SetControls;
begin
     DomainDefault.Enabled := LogonAutoAdmin.Checked;
     DomainDefaultLBL.Enabled := LogonAutoAdmin.Checked;
     UsuarioDefault.Enabled := LogonAutoAdmin.Checked;
     UsuarioDefaultLBL.Enabled := LogonAutoAdmin.Checked;
     PasswordDefault.Enabled := LogonAutoAdmin.Checked;
     PasswordDefaultLBL.Enabled := LogonAutoAdmin.Checked;
     PasswordConfirm.Enabled := LogonAutoAdmin.Checked;
     PasswordConfirmLBL.Enabled := LogonAutoAdmin.Checked;
end;

procedure TConfiguraKiosco.LogonAutoAdminClick(Sender: TObject);
begin
     SetControls;
     CambioEnDatos( Sender );
end;

procedure TConfiguraKiosco.btnEscribirClick(Sender: TObject);
var
   lOk: Boolean;
begin
     lOk := False;
     ModalResult := mrNone;
     
     if StrVacio( cbKiosco.Llave ) then
     begin
          EscribeError( '� C�digo de Kiosco inv�lido !' );
          ActiveControl := cbKiosco;
     end
     else
         if StrVacio( edGafeteAdmin.Text ) then
         begin
              EscribeError( '� Gafete del Administrador no fu� capturado !' );
              ActiveControl := edGafeteAdmin;
         end
         else
             if StrVacio( edClave.Text ) then
             begin
                  EscribeError( '� Clave del Administrador no fu� capturada !' );
                  ActiveControl := edClave;
             end
             else
                 if ( edClave.Text <> edConfirmacion.Text ) then
                 begin
                      EscribeError( '� Clave del Administrador no fu� confirmada correctamente !' );
                      ActiveControl := edClave;
                 end
                 else if (ListaEmpresasAutorizadas = VACIO) then
                 begin
                      EscribeError( '� Debe de autorizarse por lo menos una empresa !' );
                      ActiveControl := lbEmpresaAutorizadas;
                 end
                 else
                     lOk := True;
     if lOk then
     begin
          EscribeRegistry;
          ModalResult := mrOk;
     end;
end;

procedure TConfiguraKiosco.btnCancelarClick(Sender: TObject);
begin
     ModalResult := mrCancel;
end;


function TConfiguraKiosco.GetListaEmpresasAutorizadas: string;
 var
    i: integer;
    sDigito: string;
begin
     Result := VACIO;
     with lbEmpresaAutorizadas do
          for i:=0 to Items.Count - 1 do
          begin
               if Checked[i] then
               begin
                    sDigito := Chr( Integer( Items.Objects[i] ) );
                    Result := ConcatString( Result,  sDigito, ',' );
               end;
          end;
end;

procedure TConfiguraKiosco.SetListaEmpresasAutorizadas( const Value: string);
 var
    oLista: TStrings;
    i, iPos, iDigito: integer;
begin
     with lbEmpresaAutorizadas do
     begin
          for i:= 0 to Items.Count -1 do
              Checked[i] :=  FALSE;
     end;

     oLista := TStringList.Create;
     try
        oLista.Commatext := Value;
        for i:=0 to oLista.Count -1 do
        begin
             iDigito := Ord( oLista[i][1] );
             iPos := lbEmpresaAutorizadas.Items.IndexOfObject(TObject(iDigito));
             if iPos >= 0 then
                lbEmpresaAutorizadas.Checked[iPos] := TRUE;
        end;
     finally
            FreeAndNil(oLista);
     end;
end;

end.
