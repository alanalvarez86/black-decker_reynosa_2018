unit FBaseKiosco;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     OleCtrls, SHDocVw, ExtCtrls, StdCtrls,
     fcImager, fcpanel, fcButton, fcImgBtn, fcText,
     fcShapeBtn, fcClearPanel, {fcButtonGroup,}fcBitmap,
     ZetaCommonLists, QRPrntr, Buttons;

{.$DEFINE USE_SHAPE_BUTTONS}
type
  eTipoPanel = (tpBrowser, tpReportes, tpNip);
  TEstiloBoton = class(TObject)
  private
    { Private declarations }
    FFontItalic: Boolean;
    FFontBold: Boolean;
    FFontUnder: Boolean;
    FFontSize: Integer;
    FFontName: String;
    FCodigo: String;
    FColorBoton: TColor;
    FFontColor: TColor;
    FNombre: String;
    FBitmap: string;
    {$ifndef USE_SHAPE_BUTTONS}
    FButtonExt: TfcImageBtn;
    {$endif}
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property ColorBoton: TColor read FColorBoton write FColorBoton;
    property FontName: String read FFontName write FFontName;
    property FontSize: Integer read FFontSize write FFontSize;
    property FontColor: TColor read FFontColor write FFontColor;
    property FontBold: Boolean read FFontBold write FFontBold;
    property FontItalic: Boolean read FFontItalic write FFontItalic;
    property FontUnder: Boolean read FFontUnder write FFontUnder;
    property Nombre: String read FNombre write FNombre;
    property Bitmap: string read FBitmap write FBitmap;
    {$ifndef USE_SHAPE_BUTTONS}
    property ButtonExt: TfcImageBtn read FButtonExt write FButtonExt;
    {$endif}
  end;
  TBaseKiosco = class;
  TListaBotones = class;
  TBotonKiosco = class( TObject )
  private
    { Private declarations }
    FLista: TListaBotones;
    FLetrero: String;
    FURL: String;
    FPantalla: String;
    FOrden: Integer;
    FAccion: eAccionBoton;
    FAltura: Integer;
    FLugar: eLugarBoton;
    FSeparacion: Integer;
    FEstilo: TEstiloBoton;
    FPosicion: integer;
    FShapeButton: {$ifdef USE_SHAPE_BUTTONS}TfcShapeBtn{$else}TfcImageBtn{$endif};
    //FShapeButton : TfcCustomBitBtn;
    //FButtonClass: TfcCustomBitBtnClass;
    FFormaPantalla: TBaseKiosco;
    FReporte: integer;
    FPosIcono: ePosicionIcono;
    FIcono: string;
    procedure SetAltura( const Value: Integer );
    procedure SetEstilo( const Value: TEstiloBoton );
  public
    { Public declarations }
    constructor Create( Lista: TListaBotones );
    destructor Destroy; override;
    property Accion: eAccionBoton read FAccion write FAccion;
    property Altura: Integer read FAltura write SetAltura;
    property Letrero: String read FLetrero write FLetrero;
    property Lugar: eLugarBoton read FLugar write FLugar;
    property Separacion: Integer read FSeparacion write FSeparacion;
    property Orden: Integer read FOrden write FOrden;
    property Pantalla: String read FPantalla write FPantalla;
    property URL: String read FURL write FURL;
    property Estilo: TEstiloBoton read FEstilo write SetEstilo;
    property Posicion: integer read FPosicion write FPosicion;
    property Reporte: integer read FReporte write FReporte;
    property Icono: string read FIcono write FIcono;
    property PosIcono: ePosicionIcono read FPosIcono write FPosIcono;
    property ShapeButton: {$ifdef USE_SHAPE_BUTTONS}TfcShapeBtn{$else}TfcImageBtn{$endif} read FShapeButton write FShapeButton;
    //property ShapeButton: TfcCustomBitBtn read FShapeButton write FShapeButton;

    procedure DoClick(Sender: TObject);
    procedure MuestraPantalla;
    procedure Prepara;
  end;
  TListaBotones = class( TObject )
  private
    { Private declarations }
    FForma: TBaseKiosco;
    FBotones: TList;
    //FButtonGroup: TfcButtonGroup;
    FButtonGroup: TfcPanel;
    FEstiloNormal: TEstiloBoton;
    FEstiloSelect: TEstiloBoton;
    FAltura: Integer;
    FSeparacion: Integer;
    FAncho: integer;
    FAlineacion: TAlign;
    FColore: TColor;
    function GetBoton(Index: Integer): TBotonKiosco;
    procedure Delete(const Index: Integer);
    procedure SeleccionaBoton(const Index: Integer);
  public
    { Public declarations }
    constructor Create( Forma: TBaseKiosco );
    destructor Destroy; override;
    property Altura: Integer read FAltura write FAltura;
    property Boton[ Index: Integer ]: TBotonKiosco read GetBoton;
    //property ButtonGroup: TfcButtonGroup read FButtonGroup write FButtonGroup;
    property ButtonGroup: TfcPanel read FButtonGroup write FButtonGroup;
    property EstiloNormal: TEstiloBoton read FEstiloNormal;
    property EstiloSelect: TEstiloBoton read FEstiloSelect;
    property Separacion: Integer read FSeparacion write FSeparacion;
    property Colore : TColor read FColore write FColore;
    property Alineacion : TAlign read FAlineacion write FAlineacion;
    property Ancho : integer read FAncho write FAncho;
    function Add: TBotonKiosco;
    function Count: Integer;
    procedure Clear;
    procedure DoClick( Boton: TBotonKiosco );
    procedure Prepara;
    procedure Show;

  end;
  TBaseKiosco = class(TForm)
    PanelFijo: TPanel;
    WebBrowser: TWebBrowser;
    Timer: TTimer;
    fcPanelBotones: TfcPanel;
    fcImagenFondo: TfcImager;
    PanelReportes: TPanel;
    Toolbar: TPanel;
    Print: TSpeedButton;
    ZoomToFit: TSpeedButton;
    ZoomTo100: TSpeedButton;
    ZoomToWidth: TSpeedButton;
    FirstPage: TSpeedButton;
    PrevPage: TSpeedButton;
    NextPage: TSpeedButton;
    LastPage: TSpeedButton;
    QRPreview: TQRPreview;
    StatusPanel: TPanel;
    Panel1: TPanel;
    Status: TLabel;
    PanelGeneral: TPanel;
    ImageBlanca: TImage;
    Imagen: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
    procedure PrintClick(Sender: TObject);
    procedure ZoomToFitClick(Sender: TObject);
    procedure ZoomToWidthClick(Sender: TObject);
    procedure ZoomTo100Click(Sender: TObject);
    procedure FirstPageClick(Sender: TObject);
    procedure PrevPageClick(Sender: TObject);
    procedure NextPageClick(Sender: TObject);
    procedure LastPageClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
    procedure QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FListaBotones: TListaBotones;
    FBotonNormal: TEstiloBoton;
    FBotonSelect: TEstiloBoton;
    //FBotonActivo: Integer;
    FURLContenido: String;
    FURLPanel: String;
    FTimeout: Integer;
    FBrowserPanel : TWebBrowser;
    //procedure ManejaMensaje(var Msg: TMsg; var Handled: Boolean);
    procedure DoClick( Boton: TBotonKiosco );
    procedure GetEstilo( const sEstilo: String; oEstilo: TEstiloBoton );
    procedure GetContenido( const sURL: String );
    procedure Regresar;
    procedure PrendeBoton( const iBoton: Integer );
    procedure ShowPanelFijo;
    procedure CreaControles;
    procedure CreaControlesPreview;
    procedure ResetTimer( const iTimeout: Integer );
    procedure ShowPanel( const eTipo : eTipoPanel );
    procedure CierraPreviewReportes;
    procedure UpdateInfo;
    procedure LimpiaPanelFijo;
    procedure DisableControls;

    //procedure PintaBotonSeleccionado( const oBotonSelec: TfcCustomBitBtn );
  public
    { Public declarations }
    property  Timeout: Integer read FTimeout write FTimeout;
    procedure ShowContenido;
    procedure ShowPrimerBoton;
    procedure SetURLContenido( const sURL: String );
    procedure CleanPantallaEmpAnterior;
  end;

implementation

uses DCliente,
     DReportes,
     FPreview,
     //FCAmbiaNip,
     FDlgInformativo,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.DFM}

const
     K_BLANK_URL = 'about:blank';
     K_ESPERE_UN_MOMENTO = 'http://kiosco/Kiosco2/Reporte.aspx?Pagina=EspereUnMomento.htm';

{******* TBaseKiosco *********}

procedure TBaseKiosco.FormCreate(Sender: TObject);
begin
     fcImagenFondo.Align := alClient;
     //fcButtonGroup.Align := alClient;

     FListaBotones := TListaBotones.Create( Self );
     //FListaBotones.ButtonGroup := fcButtonGroup;
     FListaBotones.ButtonGroup := fcPanelBotones;
     FBotonNormal := TEstiloBoton.Create;
     FBotonSelect := TEstiloBoton.Create;
     //FBotonActivo := 0;
     FTimeout     := dmCliente.TimeoutDatos;
     CreaControles;
     CreaControlesPreview;
end;

procedure TBaseKiosco.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FBotonNormal );
     FreeAndNil( FBotonSelect );
     FreeAndNil( FListaBotones );
     FreeAndNil( FBrowserPanel );
end;

procedure TBaseKiosco.CreaControles;
var
   oBoton: TBotonKiosco;
   sImagen: string;
begin
     with dmCliente.cdsPantalla do
     begin
          with PanelFijo do
          begin
               {$ifdef FALSE}
               Color := FieldByName( 'KS_PF_COLR' ).AsInteger;
               Alignment := TAlignment( FieldByName( 'KS_PF_ALIN' ).AsInteger );
               {$endif}
               Height := FieldByName( 'KS_PF_SIZE' ).AsInteger;
               { WebBrowser del panel fijo }
               FURLPanel := dmCliente.GetURLKiosco( FieldByName( 'KS_PF_URL' ).AsString );
               if ( FURLPanel <> '' ) then
               begin
                    FreeAndNil( FBrowserPanel );
                    FBrowserPanel := TWebBrowser.Create( Self );
                    TWinControl( FBrowserPanel ).Parent := PanelFijo;
                    with FBrowserPanel do
                    begin
                         AddressBar := False;
                         MenuBar := False;
                         Silent := True;
                         StatusBar := False;
                         Offline := False;
                         
                         Align := alClient;
                         FullScreen := True;
                         OnBeforeNavigate2 := AntesDeNavegar;
                    end;
               end;
          end;
          {********************************************************************}
          {*********************** AQUI se fijan las **************************}
          {*********************** propiedades de la **************************}
          {*********************** lista de botones ***************************}
          {********************************************************************}

          with fcPanelBotones do
          begin
               Color := FieldByName( 'KS_PB_COLR' ).AsInteger;
               if ( eKJustificacion( FieldByName( 'KS_PB_ALIN' ).AsInteger ) =  ekIzquierda ) then
                  Align := alLeft
               else
                   Align := alRight;

               Width := FieldByName( 'KS_PB_SIZE' ).AsInteger;
               sImagen := FieldByName('KS_BITMAP').AsString;



               fcImagenFondo.Visible := StrLleno( sImagen ) and FileExists( sImagen );

               if fcImagenFondo.Visible then
                  fcImagenFondo.Picture.LoadFromFile( sImagen );

               {$ifdef USE_SHAPE_BUTTONS}
               //pnSeparaTop.Color := Color;
               //pnSeparaBottom.Color := Color;
               {$endif}
          end;

          with FListaBotones do
          begin
               GetEstilo( FieldByName( 'KS_EST_NOR' ).AsString, EstiloNormal );
               GetEstilo( FieldByName( 'KS_EST_SEL' ).AsString, EstiloSelect );
               Separacion := FieldByName( 'KS_BTN_SEP' ).AsInteger;
               Altura := FieldByName( 'KS_BTN_ALT' ).AsInteger;
               Ancho := FieldByName( 'KS_PB_SIZE' ).AsInteger;

          end;
          dmCliente.GetBotones( FieldByName( 'KS_CODIGO' ).AsString );

          {********************************************************************}

     end;
     FListaBotones.Clear;
     with dmCliente.cdsBotones do
     begin
          while not Eof do
          begin
               oBoton := FListaBotones.Add;
               with oBoton do
               begin
                    Altura := FieldByName( 'KB_ALTURA' ).AsInteger;
                    Lugar := eLugarBoton( FieldByName( 'KB_LUGAR' ).AsInteger );
                    Separacion := FieldByName( 'KB_SEPARA' ).AsInteger;
                    Accion := eAccionBoton( FieldByName( 'KB_ACCION' ).AsInteger );
                    URL := dmCliente.GetURLKiosco( FieldByName( 'KB_URL' ).AsString, Accion );
                    Pantalla := FieldByName( 'KB_SCREEN' ).AsString;
                    Orden := FieldByName( 'KB_ORDEN' ).AsInteger;
                    Letrero := FieldByName( 'KB_TEXTO' ).AsString;
                    Reporte := FieldByName( 'KB_REPORTE' ).AsInteger;
                    Icono := FieldByName( 'KB_BITMAP' ).AsString;
                    PosIcono := ePosicionIcono( FieldByName( 'KB_POS_BIT' ).AsInteger );
                    Posicion := FListaBotones.Count;
               end;
               Next;
          end;
     end;
     {
     Tiene que ser en una segunda pasada para soportar la "recursividad"
     de un bot�n que llama a una forma con otros botones.
     }

     with FListaBotones do
     begin
          Prepara;
          Show;
     end;
end;

// Nota: Se puede optimizar teniendo un TStringList con los estilos
// porque se van a estar reutilizando.
// Un solo query donde te traes todos los estilos. Entonces se hace un ciclo para
// llenar la lista con todos los estilos. Luego se buscan con el CODIGO
// y en el objeto de la lista se tiene el TEstiloBoton
procedure TBaseKiosco.GetEstilo( const sEstilo: String; oEstilo: TEstiloBoton);
begin
     with dmCliente do
     begin
          GetEstilo( sEstilo );
          with oEstilo do
          begin
               Codigo := sEstilo;
               with cdsEstilo do
               begin
                    Nombre      := FieldByName( 'KE_NOMBRE' ).AsString;
                    ColorBoton  := FieldByName( 'KE_COLOR' ).AsInteger;
                    FontName    := FieldByName( 'KE_F_NAME' ).AsString;
                    FontSize    := FieldByName( 'KE_F_SIZE' ).AsInteger;
                    FontColor   := FieldByName( 'KE_F_COLR' ).AsInteger;
                    FontBold    := ZetaCommonTools.zStrToBool( FieldByName( 'KE_F_BOLD' ).AsString );
                    FontItalic  := ZetaCommonTools.zStrToBool( FieldByName( 'KE_F_ITAL' ).AsString );
                    FontUnder   := ZetaCommonTools.zStrToBool( FieldByName( 'KE_F_SUBR' ).AsString );
                    Bitmap      := FieldByName( 'KE_BITMAP' ).AsString;
                    {$ifndef USE_SHAPE_BUTTONS}
                    ButtonExt  := TfcImageBtn.Create(self);
                    if StrLleno( Bitmap ) and FileExists( Bitmap ) then
                    begin
                         Imagen.Picture.LoadFromFile( Bitmap );
                         ButtonExt.Image.LoadFromBitmap( Imagen.Picture.Bitmap );
                         ButtonExt.ImageDown.LoadFromBitmap( Imagen.Picture.Bitmap );
                         Imagen.Picture := NIL;
                    end
                    else
                    begin
                         ButtonExt.Image.LoadFromBitmap( ImageBlanca.Picture.Bitmap );
                         ButtonExt.ImageDown.LoadFromBitmap( ImageBlanca.Picture.Bitmap );
                         ButtonExt.TransparentColor := clGray;
                         ButtonExt.Color := ColorBoton;
                    end;
                    {$endif}
              end;
          end;
     end;
end;

procedure TBaseKiosco.DisableControls;
begin
     PanelGeneral.Visible := FALSE;
     WebBrowser.Navigate( K_BLANK_URL );
     CierraPreviewReportes;
end;

procedure TBaseKiosco.DoClick( Boton: TBotonKiosco );
begin
     Timer.Enabled := FALSE;
     //DisableControls;
     ShowPanel( tpBrowser );

     with Boton do
     begin
          //FBotonActivo := Orden;
          case Accion of
             abContenido:
             begin
                  GetContenido( URL );
                  ResetTimer( Self.Timeout );
             end;
             abPantalla:
             begin
                  MuestraPantalla;
                  { De regreso de la otra pantalla }
                  PrendeBoton( 1 );
                  ResetTimer( self.Timeout );
             end;
             abMisDatos:
             begin
                  GetContenido( URL );
                  ResetTimer( Self.Timeout );
             end;
             abRegresar:
             begin
                  {
                  Para que cuando regrese de nuevo X empleado no vea los datos del empleado
                  anterior mientras se carga su infomaci�n

                  AP(17/06/2008): Se cambi� a DCliente para que tambi�n llegara en el timeout
                  LimpiaPanelFijo;
                  WebBrowser.Navigate( K_BLANK_URL );
                  }
                  Regresar;
             end;
             abReporte:
             begin
                  ShowPanel( tpReportes );
                  Self.QrPreview.Visible := FALSE;
                  dmCliente.QRPreview := Self.QrPreview;
                  Print.Tag := Reporte;
                  if dmCliente.GeneraReporte( Print.Tag ) then
                     Self.QrPreview.Visible := TRUE;
                  ResetTimer( self.Timeout );
             end;
             abCambiaNip:
             begin
                  ShowPanel( tpNip );
                  dmCliente.CambiaNIPEmpleado( PanelGeneral );
                  ResetTimer( self.Timeout );
             end;
         end;
     end;
end;



procedure TBaseKiosco.CierraPreviewReportes;
 var
    i: integer;
    oControl : TControl;
begin

     if ( PanelReportes <> NIL ) then
     begin
          PanelReportes.Visible := FALSE;

          if ( PanelReportes.ControlCount <> 0 ) then
          begin
               for i:= 0 to PanelReportes.ControlCount - 1 do
               begin
                    oControl := PanelReportes.Controls[i];
                    if ( oControl is TPreview ) then
                    begin
                         TPreview( oControl ).Parent := NIL;
                         TPreview( oControl ).CierraPreview;
                         Break;
                    end;
               end;
          end;
     end;
end;

procedure TBaseKiosco.ShowPanel( const eTipo : eTipoPanel );


   procedure Alinea( oControl: TWinControl; aAlinea: TAlign );
   begin
        oControl.Align := aAlinea;
        oControl.Visible := aAlinea = alClient;
   end;

begin
     Alinea( WebBrowser, alNone );
     Alinea( PanelReportes, alNone );
     Alinea( PanelGeneral, alNone );

     case eTipo of
          tpBrowser:  Alinea( WebBrowser, alClient );
          tpReportes: Alinea( PanelReportes, alClient );
          tpNip:      Alinea( PanelGeneral, alClient );
     end;
end;

procedure TBaseKiosco.GetContenido( const sURL: String );
begin
     with WebBrowser do
     begin
          Navigate( sURL );
     end;
end;

procedure TBaseKiosco.Regresar;
begin
     ModalResult := mrOK;
end;

procedure TBaseKiosco.ShowContenido;
begin
     ResetTimer( self.Timeout );
     ShowModal;
end;

procedure TBaseKiosco.PrendeBoton( const iBoton: Integer );
begin
     with FListaBotones do
     begin
          if Count > iBoton then
          begin
               with Boton[iBoton - 1].ShapeButton do
               begin
                    Click;
                    Down := True;
               end;
          end
          else
              FDlgInformativo.MuestraDialogoInf( 'No existe la lista de botones' );
     end;

end;

procedure TBaseKiosco.ShowPrimerBoton;
begin
     ShowPanelFijo;
     ResetTimer( Self.Timeout );
     PrendeBoton( 1 );
     ShowModal;
end;

procedure TBaseKiosco.SetURLContenido( const sURL: String);
begin
     FURLContenido := sURL;
     GetContenido( sURL );
end;

procedure TBaseKiosco.ShowPanelFijo;
begin
     if ( FBrowserPanel <> NIL ) then
     begin
          with FBrowserPanel do
          begin
               Navigate( K_BLANK_URL );
               Navigate( FURLPanel );
          end;
     end;
end;

procedure TBaseKiosco.LimpiaPanelFijo;
begin
     if ( FBrowserPanel <> NIL ) then
     begin
          with FBrowserPanel do
          begin
               Navigate( K_BLANK_URL );
          end;
     end;
end;

procedure TBaseKiosco.CleanPantallaEmpAnterior;
begin
     {Para que cuando regrese de nuevo X empleado no vea los datos del empleado
                  anterior mientras se carga su infomaci�n }
     LimpiaPanelFijo;
     WebBrowser.Navigate( K_BLANK_URL );
end;

procedure TBaseKiosco.TimerTimer(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

procedure TBaseKiosco.ResetTimer( const iTimeout: Integer );
begin
     Timer.Enabled  := FALSE;
     Timer.Interval := iTimeout;
     Timer.Enabled  := TRUE;
end;

procedure TBaseKiosco.AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; var URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
begin
     ResetTimer( Self.Timeout );
end;

{ ********* TBotonKiosco ****** }

constructor TBotonKiosco.Create( Lista: TListaBotones );
begin
     FLista := Lista;
end;

destructor TBotonKiosco.Destroy;
begin
     inherited Destroy;
end;

procedure TBotonKiosco.SetAltura( const Value: Integer );
begin
     if ( Value <= 0 ) then
        FAltura := FLista.Altura
     else
         FAltura := Value;
end;

procedure TBotonKiosco.SetEstilo( const Value: TEstiloBoton );
begin
     if FEstilo <> Value then
     begin
          FEstilo := Value;

           with FEstilo do
           begin
                with FShapeButton do
                begin
                     {$ifndef USE_SHAPE_BUTTONS}
                     TransparentColor := FButtonExt.TransparentColor;
                     Color := FButtonExt.Color;
                     ExtImage := FButtonExt;
                     ExtImageDown := FButtonExt;
                     {$endif}
                     with Font do
                     begin
                          Name := FontName;
                          Color := FontColor;
                          Size := FontSize;
                          Style := [];
                          if FontBold then
                             Style := Style + [ fsBold ];
                          if FontItalic then
                             Style := Style + [ fsItalic ];
                          if FontUnder then
                             Style := Style + [ fsUnderline ];
                     end;
                end;
           end;
     end;
end;

procedure TBotonKiosco.Prepara;
begin
     if ( Accion = abPantalla ) then
        FFormaPantalla := dmCliente.GetPantalla( Pantalla );
end;

procedure TBotonKiosco.MuestraPantalla;
begin
     if Assigned( FFormaPantalla ) then
     begin
          FFormaPantalla.ShowPrimerBoton;
     end;
end;

procedure TBotonKiosco.DoClick(Sender: TObject);
begin
     FLista.DoClick( Self );
end;

{ ******* TListaBotones ******* }

constructor TListaBotones.Create( Forma: TBaseKiosco );
begin
     FForma := Forma;
     FBotones := TList.Create;
     FEstiloNormal := TEstiloBoton.Create;
     FEstiloSelect := TEstiloBoton.Create;
end;

destructor TListaBotones.Destroy;
begin
     Clear;
     FreeAndNil( FEstiloSelect );
     FreeAndNil( FEstiloNormal );
     FreeAndNil( FBotones );
     inherited Destroy;
end;

function TListaBotones.Add: TBotonKiosco;
begin
     Result := TBotonKiosco.Create( Self );
     try
        FBotones.Add( Result );
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TListaBotones.Clear;
var
   i: Integer;
begin
     with FBotones do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaBotones.Count: Integer;
begin
     Result := FBotones.Count;
end;

procedure TListaBotones.Delete(const Index: Integer);
begin
     Boton[ Index ].Free;
     FBotones.Delete( Index );
end;

function TListaBotones.GetBoton(Index: Integer): TBotonKiosco;
begin
     with FBotones do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TBotonKiosco( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaBotones.SeleccionaBoton(const Index: Integer);
 var
    i: integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          Boton[ i ].Estilo := EstiloNormal;
     end;

     i := Index- 1;
     if (Count > i) then
        Boton[ Index- 1 ].Estilo := EstiloSelect;
end;


procedure TListaBotones.Prepara;
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          Boton[ i ].Prepara;
     end;
end;
{$ifdef BOTONES_ANTERIOR}
procedure TListaBotones.Show;
var
   i: Integer;
   {$ifdef USE_SHAPE_BUTTONS}
   Button: TfcShapeBtn;
   {$else}
   Button: TfcImageBtn;
   {$endif}
begin
     with FButtonGroup do
     begin
          ClickStyle := bcsRadioGroup;
          //AutoBold := True;
          {$ifdef USE_SHAPE_BUTTONS}
          ButtonClassName := 'TfcShapeBtn';
          {$else}
          ButtonClassName := 'TfcImageBtn';
          {$endif}
          ControlSpacing := Separacion;
          ShowDownAsUp := False;
          MaxControlSize := Altura;

          with ButtonItems do
          begin
               Clear;
               BeginUpdate;
               try
                  for i := 0 to ( Self.Count - 1 ) do
                  begin
                       {$ifdef USE_SHAPE_BUTTONS}
                       Button := TfcShapeBtn( Add.Button );
                       {$else}
                       Button := TfcImageBtn( Add.Button );
                       {$endif}
                       Boton[ i ].ShapeButton := Button;
                       with Button do
                       begin
                            Height := Altura;
                            with Self.Boton[ i ] do
                            begin
                                 Caption := Letrero;
                                 OnClick := DoClick;
                            end;

                            if StrLleno( Boton[ i ].Icono ) and FileExists( Boton[ i ].Icono ) then
                            begin
                                 Glyph.LoadFromFile( Boton[ i ].Icono );
                                 Layout := Boton[ i ].PosIcono;
                            end;
                            Boton[i].Estilo := EstiloNormal;

                            {$ifdef USE_SHAPE_BUTTONS}
                            Shape := bsRoundRect;
                            {$endif}
                            with TextOptions do
                            begin
                                 WordWrap := True;
                                 Options := [ toShowEllipsis ];
                            end;
                            Tag := i;
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;
{$ELSE}
procedure TListaBotones.Show;
var
   i, nTop, nSepara, nAltura: Integer;

   {$ifdef USE_SHAPE_BUTTONS}
   Button: TfcShapeBtn;
   {$else}
   Button: TfcImageBtn;
   {$endif}
begin
     //fcPanelBotones
     nTop := 0;

     for i := 0 to ( Self.Count - 1 ) do
     begin
          {$ifdef USE_SHAPE_BUTTONS}
          Button := TfcShapeBtn( Add.Button );
          {$else}
          Button := TfcImageBtn.Create(FButtonGroup);
          {$endif}
          Button.Parent := FButtonGroup;
          Boton[ i ].ShapeButton := Button;

          case Boton[ i ].Lugar of
               lbDefault   :  nSepara := Separacion;
               lbEspaciado :  nSepara := Boton[ i ].Separacion;
               else
                   nSepara := 0;
          end;

          nAltura := Boton[ i ].Altura;
          if ( nAltura < 0 ) then nAltura := Altura;

          if ( Boton[ i ].Lugar = lbAbajo ) then
          begin
               Button.Top   := FButtonGroup.Height - nAltura - 5;
               Button.Anchors := [akLeft,akBottom];    // Para soportar el 'Maximize' de la forma
          end
          else
          begin
               nTop := nTop + nSepara;
               Button.Top := nTop;
               nTop := nTop + nAltura;
          end;


          with Button do
          begin
               Width := Ancho;
               Height := nAltura;
               with Self.Boton[ i ] do
               begin
                    Caption := Letrero;
                    OnClick := DoClick;
               end;

               if StrLleno( Boton[ i ].Icono ) and FileExists( Boton[ i ].Icono ) then
               begin
                    Glyph.LoadFromFile( Boton[ i ].Icono );
                    Layout := Boton[ i ].PosIcono;
               end;
               Boton[i].Estilo := EstiloNormal;

               {$ifdef USE_SHAPE_BUTTONS}
               Shape := bsRoundRect;
               {$endif}
               with TextOptions do
               begin
                    WordWrap := True;
                    Options := [ toShowEllipsis ];
               end;
               BringToFront;
               Tag := i;
          end;
     end;
end;
{$ENDIF}



procedure TListaBotones.DoClick( Boton: TBotonKiosco );
begin
     SeleccionaBoton( Boton.Posicion );
     FForma.DoClick( Boton );
end;

{ ************ Metodos y eventos para el Preview ***************** }
procedure TBaseKiosco.PrintClick(Sender: TObject);
begin
     try
        Timer.Enabled := FALSE;
        //QrPreview.QrPrinter.Print;
        dmCliente.GeneraReporte( Print.Tag, FALSE );
        FDlgInformativo.MuestraDialogoInf( 'El reporte ha sido impreso con �xito' );
        ResetTimer( self.Timeout );
     except
           raise;
     end;
end;

procedure TBaseKiosco.ZoomToFitClick(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.ZoomToFit;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.ZoomTo100Click(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.Zoom := 100;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.ZoomToWidthClick(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.ZoomToWidth;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.FirstPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := 1;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.PrevPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPreview.PageNumber - 1;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.NextPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPreview.PageNumber + 1;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.LastPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPrinter.PageCount;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;



procedure TBaseKiosco.CreaControlesPreview;
begin
     FirstPage.Caption := 'Primer' + CR_LF + 'P�gina';
     LastPage.Caption := 'Ultima' + CR_LF + 'P�gina';
     PrevPage.Caption := 'Anterior';
     NextPage.Caption := 'Siguiente';

     ZoomToFit.Caption := 'P�gina Completa';
     ZoomToWidth.Caption := 'Ancho P�gina';
     ZoomTo100.Caption := 'P�gina al 100%';
end;

procedure TBaseKiosco.UpdateInfo;
begin
     if QrPreview.QrPrinter <> NIL then
        Status.Caption := 'P�gina ' + IntToStr(QRPreview.PageNumber) +
                          ' de ' + IntToStr(QRPreview.QRPrinter.PageCount);
end;


procedure TBaseKiosco.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if PanelReportes.Visible then
     begin
          case Key of
               VK_Next : if Shift=[ssCtrl] then
                            LastPageClick(Self)
                         else
                             NextPageClick(Self);
               VK_Prior : if Shift=[ssCtrl] then
                             FirstPageClick(Self)
                          else
                              PrevPageClick(Self);
               VK_Home : FirstPageClick(Self);
               VK_End : LastPageClick(Self);
          end;
     end
     else if PanelGeneral.Visible then
     begin
          ResetTimer( Self.Timeout );
     end;
end;

procedure TBaseKiosco.QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
begin
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     with QrPreview do
     begin
          case Button of
               mbLeft: if ( Zoom < 300 ) then Zoom := Zoom + 10;
               mbRight: if ( Zoom > 0 ) then Zoom := Zoom - 10;
          end;
     end;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     DisableControls;
end;



end.

