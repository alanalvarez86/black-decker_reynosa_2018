CREATE TABLE KSHOW (
	KW_CODIGO    Codigo,
	KW_NOMBRE    Descripcion,
	KW_SCR_INF   Codigo,
	KW_SCR_DAT   Codigo,
	KW_TIM_INF   FolioChico,
	KW_TIM_DAT   FolioChico,
	KW_GET_NIP   Booleano
)
GO

ALTER TABLE KSHOW
      ADD CONSTRAINT PK_KSHOW PRIMARY KEY( KW_CODIGO )
GO

CREATE TABLE KSHOWINF (
	KW_CODIGO     Codigo,
	KI_ORDEN      FolioChico,
	KI_TIMEOUT    FolioChico,
	KI_URL        Ruta_URL
)
GO

ALTER TABLE KSHOWINF
      ADD CONSTRAINT PK_KSHOWINF PRIMARY KEY( KW_CODIGO, KI_ORDEN )
GO

CREATE TABLE KSCREEN (
	KS_CODIGO    Codigo,
	KS_NOMBRE    Descripcion,
	KS_PF_SIZE   FolioChico,
	KS_PF_COLR   FolioGrande,
	KS_PF_ALIN   Status,
	KS_PF_URL    Formula,
	KS_PB_SIZE   FolioChico,
	KS_PB_COLR   FolioGrande,
	KS_PB_ALIN   Status,
	KS_BTN_ALT   FolioChico,
	KS_BTN_SEP   FolioChico,
	KS_EST_NOR   Codigo,
	KS_EST_SEL   Codigo
)
GO

ALTER TABLE KSCREEN
      ADD CONSTRAINT PK_KSCREEN PRIMARY KEY( KS_CODIGO )
GO

CREATE TABLE KBOTON (
	KS_CODIGO    Codigo,
	KB_ORDEN     FolioChico,
	KB_TEXTO     Observaciones,
	KB_BITMAP    Ruta_URL,
	KB_ACCION    Status,
	KB_URL       Ruta_URL,
	KB_SCREEN    Codigo,
	KB_ALTURA    FolioChico,
	KB_LUGAR     Status,
	KB_SEPARA    FolioChico,
 KB_REPORTE   FolioChico,
 KB_POS_BIT   Status
)
GO

ALTER TABLE KBOTON
      ADD CONSTRAINT PK_KBOTON PRIMARY KEY( KS_CODIGO, KB_ORDEN )
GO

CREATE TABLE KESTILO (
	KE_CODIGO    Codigo,
	KE_NOMBRE    Descripcion,
	KE_COLOR     FolioGrande,
	KE_F_NAME    Descripcion,
	KE_F_SIZE    FolioChico,
	KE_F_COLR    FolioGrande,
	KE_F_BOLD    Booleano,
	KE_F_ITAL    Booleano,
	KE_F_SUBR    Booleano
)
GO

ALTER TABLE KESTILO
      ADD CONSTRAINT PK_KESTILO PRIMARY KEY( KE_CODIGO )
GO

ALTER TABLE KSHOWINF
       ADD CONSTRAINT FK_KSHOWINF_KSHOW
       FOREIGN KEY (KS_CODIGO)
       REFERENCES KSHOW
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

ALTER TABLE KBOTON
       ADD CONSTRAINT FK_KBOTON_KSCREEN
       FOREIGN KEY (KS_CODIGO)
       REFERENCES KSCREEN
       ON DELETE CASCADE
       ON UPDATE CASCADE;
GO

CREATE TRIGGER TD_KSCREEN ON KSCREEN AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldCodigo Codigo;
     select @OldCodigo = KS_CODIGO from Deleted;
     update KSHOW SET KW_SCR_INF = '' WHERE ( KW_SCR_INF = @OldCodigo );
     update KSHOW SET KW_SCR_DAT = '' WHERE ( KW_SCR_DAT = @OldCodigo );
     update KBOTON set KB_SCREEN = '' WHERE ( KB_SCREEN = @OldCodigo );
END
GO

CREATE TRIGGER TU_KSCREEN ON KSCREEN AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( KS_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = KS_CODIGO from Inserted;
        select @OldCodigo = KS_CODIGO from Deleted;

        update KSHOW SET KW_SCR_INF = @NewCodigo WHERE ( KW_SCR_INF = @OldCodigo );
        update KSHOW SET KW_SCR_DAT = @NewCodigo WHERE ( KW_SCR_DAT = @OldCodigo );
        update KBOTON SET KB_SCREEN = @NewCodigo WHERE ( KB_SCREEN = @OldCodigo );
  END
END
GO

CREATE TRIGGER TD_KESTILO ON KESTILO AFTER DELETE AS
BEGIN
     SET NOCOUNT ON;
     declare @OldCodigo Codigo;
     select @OldCodigo = KE_CODIGO from Deleted;
     update KSHOW SET KS_EST_NOR = '' WHERE ( KS_EST_NOR = @OldCodigo );
     update KSHOW SET KS_EST_SEL = '' WHERE ( KS_EST_SEL = @OldCodigo );
END
GO

CREATE TRIGGER TU_KESTILO ON KESTILO AFTER UPDATE AS
BEGIN
  SET NOCOUNT ON;
  IF UPDATE( KE_CODIGO )
  BEGIN
	declare @NewCodigo Codigo, @OldCodigo Codigo;

	select @NewCodigo = KE_CODIGO from Inserted;
        select @OldCodigo = KE_CODIGO from Deleted;

        update KSHOW SET KS_EST_NOR = @NewCodigo WHERE ( KS_EST_NOR = @OldCodigo );
        update KSHOW SET KS_EST_SEL = @NewCodigo WHERE ( KS_EST_SEL = @OldCodigo );
  END
END
GO







