inherited HistTareas: THistTareas
  Left = 240
  Top = 216
  ActiveControl = Status
  Caption = 'Tareas'
  ClientHeight = 257
  ClientWidth = 881
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 881
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 521
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 105
    Width = 881
    Height = 111
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'WP_FOLIO'
        Title.Caption = 'Folio'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WP_NOMBRE'
        Title.Caption = 'Nombre de Proceso'
        Width = 154
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WP_NOM_INI'
        Title.Caption = 'Solicitante'
        Width = 162
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PASOACTUAL'
        Title.Caption = 'Pasos'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WS_NOMBRE'
        Title.Caption = 'Paso Actual'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'STATUSTAREA'
        Title.Caption = 'Status'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WT_NOM_DES'
        Title.Caption = 'Responsable'
        Width = 99
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WT_FEC_INI'
        Title.Caption = 'Inicio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WT_FEC_FIN'
        Title.Caption = 'Ultima Actividad'
        Width = 84
        Visible = True
      end>
  end
  object Panel: TPanel [2]
    Left = 0
    Top = 19
    Width = 881
    Height = 86
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object StatusLBL: TLabel
      Left = 84
      Top = 11
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Status:'
      FocusControl = Status
    end
    object ModeloLBL: TLabel
      Left = 79
      Top = 35
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = '&Modelo:'
      FocusControl = Modelo
    end
    object ProcesoLBL: TLabel
      Left = 17
      Top = 59
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Folio de Proceso, &de:'
      FocusControl = FolioInicial
    end
    object SolicitanteLBL: TLabel
      Left = 403
      Top = 11
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'S&olicitante:'
      FocusControl = Solicitante
    end
    object ResponsableLBL: TLabel
      Left = 390
      Top = 35
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = '&Responsable:'
      FocusControl = Responsable
    end
    object FechaInicioLBL: TLabel
      Left = 347
      Top = 59
      Width = 108
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fe&cha De Inicio, entre:'
      FocusControl = FechaInicio
    end
    object FolioFinalLBL: TLabel
      Left = 214
      Top = 59
      Width = 9
      Height = 13
      Alignment = taRightJustify
      Caption = '&a:'
      FocusControl = FolioFinal
    end
    object FechaFinalLBL: TLabel
      Left = 579
      Top = 59
      Width = 8
      Height = 13
      Alignment = taRightJustify
      Caption = '&y:'
      FocusControl = FechaFinal
    end
    object Status: TZetaKeyCombo
      Left = 120
      Top = 7
      Width = 217
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object Modelo: TZetaKeyCombo
      Left = 120
      Top = 31
      Width = 217
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
    end
    object FolioInicial: TZetaNumero
      Left = 120
      Top = 55
      Width = 90
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 2
    end
    object FolioFinal: TZetaNumero
      Left = 225
      Top = 55
      Width = 90
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 3
    end
    object FechaInicio: TZetaFecha
      Left = 458
      Top = 54
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 6
      Text = '13/Sep/04'
      Valor = 38243.000000000000000000
    end
    object Responsable: TZetaKeyLookup
      Left = 458
      Top = 31
      Width = 300
      Height = 21
      TabOrder = 5
      TabStop = True
      WidthLlave = 60
    end
    object Solicitante: TZetaKeyLookup
      Left = 458
      Top = 7
      Width = 300
      Height = 21
      TabOrder = 4
      TabStop = True
      WidthLlave = 60
    end
    object Filtrar: TBitBtn
      Left = 762
      Top = 8
      Width = 75
      Height = 66
      Hint = 'Aplicar El Filtro Para Obtener Los Procesos Deseados'
      Caption = '&Filtrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      OnClick = FiltrarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object FechaFinal: TZetaFecha
      Left = 593
      Top = 54
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 7
      Text = '13/Sep/04'
      Valor = 38243.000000000000000000
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 216
    Width = 881
    Height = 41
    Align = alBottom
    TabOrder = 3
    object Reasignar: TBitBtn
      Left = 9
      Top = 5
      Width = 88
      Height = 31
      Hint = 'Reasignar el proceso seleccionado'
      Cancel = True
      Caption = 'Reasi&gnar'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = ReasignarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FFFFFFFFFFF
        FFFF33333333333FFFFF3FFFFFFFFF00000F333333333377777F33FFFFFFFF09
        990F33333333337F337F333FFFFFFF09990F33333333337F337F3333FFFFFF09
        990F33333333337FFF7F33333FFFFF00000F3333333333777773333333FFFFFF
        FFFF3333333333333F333333333FFFFF0FFF3333333333337FF333333333FFF0
        00FF33333333333777FF333333333F00000F33FFFFF33777777F300000333000
        0000377777F33777777730EEE033333000FF37F337F3333777F330EEE0333330
        00FF37F337F3333777F330EEE033333000FF37FFF7F333F77733300000333000
        03FF3777773337777333333333333333333F3333333333333333}
      NumGlyphs = 2
    end
  end
  inherited DataSource: TDataSource
    Left = 16
    Top = 32
  end
end
