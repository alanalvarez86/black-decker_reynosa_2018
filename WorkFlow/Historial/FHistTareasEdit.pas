unit FHistTareasEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaSmartLists;

type
  THistTareasEdit = class(TBaseEdicion)
    PanelDatos: TPanel;
    WP_FOLIOlbl: TLabel;
    PASOACTUALlbl: TLabel;
    STATUSTAREAlbl: TLabel;
    WT_NOM_ORIlbl: TLabel;
    WT_NOTASlbl: TLabel;
    WT_FEC_INIlbl: TLabel;
    WP_FOLIO: TZetaDBTextBox;
    WM_CODIGO: TZetaDBTextBox;
    WT_NOM_ORI: TZetaDBTextBox;
    WT_FEC_INI: TZetaDBTextBox;
    WP_NOMBRE: TZetaDBTextBox;
    WM_CODIGOlbl: TLabel;
    PASOACTUAL: TZetaDBTextBox;
    WS_NOMBRE: TZetaDBTextBox;
    STATUSTAREA: TZetaDBTextBox;
    WT_AVANCE: TZetaDBTextBox;
    WT_NOM_DESlbl: TLabel;
    WT_NOM_DES: TZetaDBTextBox;
    WT_FEC_FINlbl: TLabel;
    WT_FEC_FIN: TZetaDBTextBox;
    WT_NOTAS: TDBMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  HistTareasEdit: THistTareasEdit;

implementation

uses ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses,
     DWorkFlow;

{$R *.DFM}

{ THistCorreosEdit }

procedure THistTareasEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_HISTORIAL_TAREAS_EDIC;
     IndexDerechos := D_HISTORIAL_TAREAS;
     AgregarBtn.Visible := False;
     BorrarBtn.Visible := False;
     ModificarBtn.Visible := False;
     CortarBtn.Visible := False;
     CopiarBtn.Visible := False;
     PegarBtn.Visible := False;
     UndoBtn.Visible := False;
     OK.Visible := False;
end;

procedure THistTareasEdit.Connect;
begin
     Datasource.Dataset := dmWorkflow.cdsTareas;
end;

function THistTareasEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Tareas';
end;

function THistTareasEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Tareas';
end;

function THistTareasEdit.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Cambio debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Tareas';
end;

function THistTareasEdit.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Modificar Tareas';
end;

end.
