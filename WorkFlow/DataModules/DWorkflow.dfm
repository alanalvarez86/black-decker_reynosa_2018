object dmWorkflow: TdmWorkflow
  OldCreateOrder = True
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 258
  Top = 310
  Height = 477
  Width = 768
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosAfterOpen
    AlModificar = cdsProcesosAlModificar
    Left = 40
    Top = 16
  end
  object cdsCorreos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsCorreosAfterOpen
    OnCalcFields = cdsCorreosCalcFields
    AlCrearCampos = cdsCorreosAlCrearCampos
    AlModificar = cdsCorreosAlModificar
    Left = 232
    Top = 16
  end
  object cdsTareas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsTareasAfterOpen
    AlModificar = cdsTareasAlModificar
    Left = 173
    Top = 16
  end
  object cdsProcesosPasos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosPasosAfterOpen
    AlCrearCampos = cdsProcesosPasosAlCrearCampos
    Left = 40
    Top = 64
  end
  object cdsProcesosCorreos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosCorreosAfterOpen
    AlCrearCampos = cdsProcesosCorreosAlCrearCampos
    Left = 40
    Top = 119
  end
  object cdsProcesosArchivos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 175
  end
  object cdsProcesosMensajes: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsProcesosMensajesAfterOpen
    Left = 40
    Top = 231
  end
  object cdsModelos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsModelosAfterOpen
    BeforePost = cdsModelosBeforePost
    AfterCancel = cdsModelosAfterCancel
    AfterScroll = cdsModelosAfterScroll
    OnNewRecord = cdsModelosNewRecord
    AlAdquirirDatos = cdsModelosAlAdquirirDatos
    AlEnviarDatos = cdsModelosAlEnviarDatos
    AlModificar = cdsModelosAlModificar
    LookupName = 'Modelos'
    LookupDescriptionField = 'WM_NOMBRE'
    LookupKeyField = 'WM_CODIGO'
    OnGetRights = cdsModelosGetRights
    Left = 306
    Top = 16
  end
  object cdsPasosModelo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsPasosModeloAfterOpen
    BeforePost = cdsPasosModeloBeforePost
    AfterDelete = cdsDatasetAfterDelete
    OnNewRecord = cdsPasosModeloNewRecord
    AlAdquirirDatos = cdsPasosModeloAlAdquirirDatos
    AlEnviarDatos = cdsPasosModeloAlEnviarDatos
    AlBorrar = cdsPasosModeloAlBorrar
    AlModificar = cdsPasosModeloAlModificar
    Left = 378
    Top = 16
  end
  object cdsAcciones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'WA_CODIGO'
    Params = <>
    AfterOpen = cdsAccionesAfterOpen
    AlAdquirirDatos = cdsAccionesAlAdquirirDatos
    AlEnviarDatos = cdsAccionesAlEnviarDatos
    AlModificar = cdsAccionesAlModificar
    LookupName = 'Acciones'
    LookupDescriptionField = 'WA_NOMBRE'
    LookupKeyField = 'WA_CODIGO'
    OnGetRights = cdsAccionesGetRights
    Left = 508
    Top = 16
  end
  object cdsRolUsuarios: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RO_CODIGO;US_CODIGO'
    Params = <>
    Left = 444
    Top = 119
  end
  object cdsReasignarTareaUsuarios: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 173
    Top = 64
  end
  object cdsEventLog: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsEventLogAlCrearCampos
    Left = 106
    Top = 16
  end
  object cdsRolModelos: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'RO_CODIGO;WM_CODIGO'
    Params = <>
    Left = 444
    Top = 64
  end
  object cdsModeloRoles: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'WM_CODIGO;RO_CODIGO'
    Params = <>
    Left = 306
    Top = 64
  end
  object cdsNotificaciones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsNotificacionesAfterOpen
    AfterInsert = cdsNotificacionesAfterCambios
    AfterEdit = cdsNotificacionesAfterCambios
    AfterDelete = cdsNotificacionesAfterCambios
    OnCalcFields = cdsNotificacionesCalcFields
    OnNewRecord = cdsNotificacionesNewRecord
    AlAdquirirDatos = cdsNotificacionesAlAdquirirDatos
    AlCrearCampos = cdsNotificacionesAlCrearCampos
    AlBorrar = cdsNotificacionesAlBorrar
    AlModificar = cdsNotificacionesAlModificar
    Left = 368
    Top = 208
  end
  object cdsNivelRoles: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RO_NIVEL'
    Params = <>
    AlAdquirirDatos = cdsNivelRolesAlAdquirirDatos
    LookupName = 'Niveles de roles'
    LookupDescriptionField = 'RO_NOMBRE'
    LookupKeyField = 'RO_NIVEL'
    OnGetRights = cdsNivelRolesGetRights
    Left = 468
    Top = 216
  end
  object cdsConexiones: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'WX_FOLIO'
    Params = <>
    AfterOpen = cdsConexionesAfterOpen
    OnNewRecord = cdsConexionesNewRecord
    AlAdquirirDatos = cdsConexionesAlAdquirirDatos
    AlEnviarDatos = cdsConexionesAlEnviarDatos
    AlModificar = cdsConexionesAlModificar
    LookupName = 'Conexiones'
    LookupDescriptionField = 'WX_NOMBRE'
    LookupKeyField = 'WX_FOLIO'
    Left = 516
    Top = 64
  end
end
