unit DGlobal;

interface

uses Forms, Controls, DB, Classes, StdCtrls, SysUtils,
     {$ifdef DOS_CAPAS}
     DServerComparte,
     {$else}
     Comparte_TLB,
     {$endif}
     DBaseGlobal;

type
  TdmGlobal = class( TdmBaseGlobal )
  private
    { Private declarations }
    {$ifdef DOS_CAPAS}
    function GetServer: TdmServerComparte;
    {$else}
    function GetServer: IdmServerComparteDisp;
    {$endif}
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property Server: TdmServerComparte read GetServer;
    {$else}
    property Server: IdmServerComparteDisp read GetServer;
    {$endif}
    function GetGlobales: Variant; override;
    procedure GrabaGlobales( const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer ); override;
  public
    { Public declarations }
  end;

var
   Global: TdmGlobal;

implementation

uses DCliente,
     ZetaCommonLists,
     ZGlobalTress;

{********* TdmGlobal ******** }

{$ifdef DOS_CAPAS}
function TdmGlobal.GetServer: TdmServerComparte;
{$else}
function TdmGlobal.GetServer: IdmServerComparteDisp;
{$endif}
begin
     Result := dmCliente.ServerComparte;
end;

{ER: Si se requiere declarar GetGlobales y GrabaGlobales, porque no se puede declarar el SERVER en la clase base}

function TdmGlobal.GetGlobales: Variant;
begin
     Result := Server.GetGlobales;
end;

procedure TdmGlobal.GrabaGlobales(const aGlobalServer: Variant; const lActualizaDiccion: Boolean; var ErrorCount: Integer);
begin
     Server.GrabaGlobales( aGlobalServer, ErrorCount );
end;

end.
