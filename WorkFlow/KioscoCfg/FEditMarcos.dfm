inherited EdicionMarcos: TEdicionMarcos
  Left = 563
  Top = 48
  Caption = 'Marcos de informaci'#243'n'
  ClientHeight = 500
  ClientWidth = 440
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 464
    Width = 440
    inherited OK: TBitBtn
      Left = 282
    end
    inherited Cancelar: TBitBtn
      Left = 361
    end
  end
  inherited PanelSuperior: TPanel
    Width = 440
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 440
    inherited ValorActivo2: TPanel
      Width = 114
    end
  end
  inherited Panel1: TPanel
    Width = 440
    Height = 54
    object Label1: TLabel
      Left = 78
      Top = 8
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 74
      Top = 31
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
    end
    object KS_CODIGO: TZetaDBEdit
      Left = 118
      Top = 4
      Width = 73
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'KS_CODIGO'
      ConfirmEdit = True
      DataField = 'KS_CODIGO'
      DataSource = DataSource
    end
    object KS_NOMBRE: TDBEdit
      Left = 118
      Top = 27
      Width = 177
      Height = 21
      DataField = 'KS_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  inherited PageControl: TPageControl
    Top = 105
    Width = 440
    Height = 359
    inherited Datos: TTabSheet
      object GroupBox1: TGroupBox
        Left = 10
        Top = 3
        Width = 412
        Height = 73
        Caption = ' Encabezado '
        TabOrder = 0
        object Label3: TLabel
          Left = 71
          Top = 24
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Altura:'
        end
        object Label4: TLabel
          Left = 42
          Top = 47
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'P'#225'gina web:'
        end
        object KS_PF_SIZE: TZetaDBNumero
          Left = 104
          Top = 20
          Width = 73
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'KS_PF_SIZE'
          DataSource = DataSource
        end
        object KS_PF_URL: TDBEdit
          Left = 104
          Top = 43
          Width = 300
          Height = 21
          DataField = 'KS_PF_URL'
          DataSource = DataSource
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 10
        Top = 83
        Width = 412
        Height = 117
        Caption = 'Panel de botones '
        TabOrder = 1
        object Label5: TLabel
          Left = 58
          Top = 24
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Anchura:'
        end
        object Label7: TLabel
          Left = 74
          Top = 70
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'Color:'
        end
        object Label6: TLabel
          Left = 58
          Top = 93
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Posici'#243'n:'
        end
        object Label12: TLabel
          Left = 14
          Top = 47
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Imagen del fondo:'
          FocusControl = KS_BITMAP
        end
        object bBuscaImagen: TSpeedButton
          Left = 380
          Top = 42
          Width = 22
          Height = 22
          Glyph.Data = {
            36050000424D3605000000000000360400002800000010000000100000000100
            0800000000000001000000000000000000000001000000010000000000000101
            0100020202000303030004040400050505000606060007070700080808000909
            09000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F0F00101010001111
            1100121212001313130014141400151515001616160017171700181818001919
            19001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F1F00202020002121
            2100222222002323230024242400252525002626260027272700282828002929
            29002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F2F00303030003131
            3100323232003333330034343400353535003636360037373700383838003939
            39003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F3F00404040004141
            4100424242004343430044444400454545004646460047474700484848004949
            49004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F4F00505050005151
            5100525252005353530054545400555555005656560057575700585858005959
            59005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F5F00606060006161
            6100626262006363630064646400656565006666660067676700686868006969
            69006A6A6A0074647400944D9400BB32BB00E315E300F408F400FB03FB00FD01
            FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
            FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
            FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
            FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
            FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00FE00
            FE00FE00FE00FE00FE00FD01FE00FC03FE00FB05FE00F908FE00F50EFD00F015
            FC00E821FA00DD32F600D046F100BF61E900AF7EDF00A78DD500A19ECA00A1A6
            BF00A2AEB300AFAFAF00B5B5B500BBBBBB00C0C0C000C4C4C400C8C9C900CED0
            D000D3D6D600D6DDDD00D9E4E500DCE9EB00E0EFF000E3F2F400E5F5F600E5F6
            F800E6F7F900E5F8FA00E4F8FA00E1F8FA00DDF8FB00DAF8FB00D7F7FB00D4F7
            FB00CFF6FA00CBF6FA00C8F6FB00C4F6FB00BFF7FB00B6F8FC00B1F8FD00ACF8
            FD00A7F9FD00A4F9FE00A1F9FE009FF8FE009EF7FD009DF6FD009CF5FD009AF4
            FD0099F3FC0098F1FC0096F0FC0095ECFB0093EBFB0092EAFB008FE9FB008EE8
            FB008CE7FB008BE6FB0089E4FB0086E3FB0083E1FB0080DFFB007EDEFB007CDD
            FA007ADBFA0078DAFA0076D9FA0074D7F90072D6F90070D5F9006ED4F9006CD2
            F80069D0F70066CDF40062CAF0005FC8EE0059C4EA0053C0E6004CBBE30049B8
            E00043B5DD003FB1DB003AAED80031A8D30025A0CD001495C4000B8FC000088D
            BE00088DBE00078DBE00078DBE00078DBE00078DBE00078DBE0086FCFCFCFCFC
            FCFCFCFCFCFCFC868686FCF6EDDEEBEBEBEBEBEBEBEBF4F78686FCF0F5D5E8E8
            E8E8E8E8E8E8F3C4FC86FCE6FCC9E2E2E2E2E2E2E2E2F2C4FC86FCE1F7D5D7DD
            DEDEDDDEDEDDF1C4F786FCDEF2EEC9D8D8D8D8D8D8D8F0C4C4FCFCDAE3F6BBC4
            C4C4C4C4C4C4D5BBC2FCFCD4D4F7FCFCFCFCFCFCFCFCFCFCFCFCFCD0D1D1D0CF
            D1D0D1D0D1D1F9868686FCBBCCCCCCCCCCCCCCCCCCCCF986868686FCBBCBCBCB
            FCFCFCFCFCFC868686868686FCFCFCFC86868686868686868686868686868686
            8686868686868686868686868686868686868686868686868686868686868686
            8686868686868686868686868686868686868686868686868686}
          OnClick = bBuscaImagenClick
        end
        object KS_PB_SIZE: TZetaDBNumero
          Left = 104
          Top = 20
          Width = 73
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'KS_PB_SIZE'
          DataSource = DataSource
        end
        object KS_PB_COLR: TfcColorCombo
          Left = 104
          Top = 66
          Width = 179
          Height = 21
          DataField = 'KS_PB_COLR'
          DataSource = DataSource
          AutoDropDown = True
          ColorDialog = ColorDialog
          ColorDialogOptions = [cdoEnabled, cdoPreventFullOpen, cdoSolidColor, cdoAnyColor]
          ColorListOptions.Font.Charset = DEFAULT_CHARSET
          ColorListOptions.Font.Color = clWindowText
          ColorListOptions.Font.Height = -11
          ColorListOptions.Font.Name = 'MS Sans Serif'
          ColorListOptions.Font.Style = []
          ColorListOptions.Options = [ccoShowSystemColors, ccoShowCustomColors, ccoShowStandardColors, ccoShowColorNames]
          DropDownCount = 8
          ReadOnly = False
          SelectedColor = 268435455
          TabOrder = 2
        end
        object KS_PB_ALIN: TZetaDBKeyCombo
          Left = 104
          Top = 89
          Width = 179
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 3
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          DataField = 'KS_PB_ALIN'
          DataSource = DataSource
          LlaveNumerica = True
        end
        object KS_BITMAP: TDBEdit
          Left = 104
          Top = 43
          Width = 273
          Height = 21
          DataField = 'KS_BITMAP'
          DataSource = DataSource
          TabOrder = 1
        end
      end
      object GroupBox3: TGroupBox
        Left = 10
        Top = 206
        Width = 412
        Height = 121
        Caption = ' Botones '
        TabOrder = 2
        object Label8: TLabel
          Left = 71
          Top = 24
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Altura:'
        end
        object Label9: TLabel
          Left = 44
          Top = 46
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Caption = 'Separaci'#243'n:'
        end
        object Label10: TLabel
          Left = 39
          Top = 69
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estilo normal:'
        end
        object Label11: TLabel
          Left = 7
          Top = 92
          Width = 94
          Height = 13
          Alignment = taRightJustify
          Caption = 'Estilo seleccionado:'
        end
        object KS_BTN_ALT: TZetaDBNumero
          Left = 104
          Top = 20
          Width = 73
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'KS_BTN_ALT'
          DataSource = DataSource
        end
        object KS_BTN_SEP: TZetaDBNumero
          Left = 104
          Top = 42
          Width = 73
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          DataField = 'KS_BTN_SEP'
          DataSource = DataSource
        end
        object KS_EST_NOR: TZetaDBKeyLookup
          Left = 104
          Top = 65
          Width = 300
          Height = 21
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'KS_EST_NOR'
          DataSource = DataSource
        end
        object KS_EST_SEL: TZetaDBKeyLookup
          Left = 104
          Top = 88
          Width = 300
          Height = 21
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
          DataField = 'KS_EST_SEL'
          DataSource = DataSource
        end
      end
    end
    inherited Tabla: TTabSheet
      inherited GridRenglones: TZetaDBGrid
        Width = 403
        Height = 302
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect]
        OnDblClick = BBModificarClick
        Columns = <
          item
            Expanded = False
            FieldName = 'KB_ORDEN'
            Title.Caption = 'Orden'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KB_TEXTO'
            Title.Caption = 'Texto'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KB_ACCION'
            Title.Caption = 'Acci'#243'n'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'KB_SCREEN'
            Title.Caption = 'Marco'
            Width = 44
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 432
        inherited BBAgregar: TBitBtn
          Left = 11
          Caption = 'Agregar Bot'#243'n'
        end
        inherited BBBorrar: TBitBtn
          Left = 151
          Caption = 'Borrar Bot'#243'n'
        end
        inherited BBModificar: TBitBtn
          Left = 291
          Caption = 'Modificar Bot'#243'n'
        end
      end
      object Panel3: TPanel
        Left = 403
        Top = 29
        Width = 29
        Height = 302
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object bArriba: TZetaSmartListsButton
          Left = 3
          Top = 116
          Width = 25
          Height = 25
          Hint = 'Subir Bot'#243'n'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333090333333333333309033333333333330903333333333333090333
            3333333333090333333333300009000033333330999999903333333309999903
            3333333309999903333333333099903333333333309990333333333333090333
            3333333333090333333333333330333333333333333033333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = bArribaClick
          Tipo = bsSubir
        end
        object bAbajo: TZetaSmartListsButton
          Left = 3
          Top = 140
          Width = 25
          Height = 25
          Hint = 'Bajar Boton'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            3333333333303333333333333309033333333333330903333333333330999033
            3333333330999033333333330999990333333333099999033333333099999990
            3333333000090000333333333309033333333333330903333333333333090333
            3333333333090333333333333309033333333333330003333333}
          ParentShowHint = False
          ShowHint = True
          OnClick = bAbajoClick
          Tipo = bsBajar
        end
      end
    end
  end
  object ColorDialog: TColorDialog
    Left = 216
    Top = 43
  end
end
