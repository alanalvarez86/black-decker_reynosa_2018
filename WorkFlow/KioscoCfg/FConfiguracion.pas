unit FConfiguracion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseConsultaBotones, ImgList, DB, ToolWin, ComCtrls, ExtCtrls;

type
  TConfiguracion = class(TBaseBotones)
    bReseteaPass: TToolButton;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Configuracion: TConfiguracion;

implementation
uses
    FTressShell;

{$R *.dfm}

procedure TConfiguracion.FormCreate(Sender: TObject);
begin
     inherited;
     bReseteaPass.Action := TressShell.ReseteaPassword;

end;

end.
