inherited ReseteaPass: TReseteaPass
  Left = 692
  Top = 373
  Caption = 'Borra NIP'
  ClientHeight = 98
  ClientWidth = 365
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel [0]
    Left = 14
    Top = 12
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa:'
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 36
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleado:'
  end
  inherited PanelBotones: TPanel
    Top = 62
    Width = 365
    inherited OK: TBitBtn
      Left = 197
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 282
    end
  end
  object eEmpresa: TZetaKeyCombo
    Left = 59
    Top = 8
    Width = 250
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
    OnChange = eEmpresaChange
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
  end
  object eEmpleado: TZetaKeyLookup
    Left = 59
    Top = 32
    Width = 300
    Height = 21
    TabOrder = 2
    TabStop = True
    WidthLlave = 60
  end
end
