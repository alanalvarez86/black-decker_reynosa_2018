unit FEditEstilosBotones;

interface
//KE_CODIGO,KE_NOMBRE,KE_COLOR,KE_F_NAME,KE_F_SIZE,KE_F_COLR,KE_F_BOLD,KE_F_ITAL,KE_F_SUBR

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Mask, DBCtrls, StdCtrls, Db, ExtCtrls, Buttons,
     ZBaseEdicion,
     ZetaNumero,
     ZetaEdit,
     ZetaSmartLists,
     ZetaKeyLookup,
     ZetaDBTextBox, fcCombo, fcColorCombo, fctreecombo, fcFontCombo;

type
  TTOEditEstilosBotones = class(TBaseEdicion)
    TB_CODIGOlbl: TLabel;
    KE_CODIGO: TZetaDBEdit;
    KE_NOMBRE: TDBEdit;
    TB_ELEMENTlbl: TLabel;
    KE_COLOR: TfcColorCombo;
    Label1: TLabel;
    ColorDialog: TColorDialog;
    FontDialog: TFontDialog;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    KE_F_NAME: TfcFontCombo;
    SpeedButton1: TSpeedButton;
    Label3: TLabel;
    KE_F_SIZE: TZetaDBNumero;
    Label4: TLabel;
    KE_F_COLR: TfcColorCombo;
    KE_F_BOLD: TDBCheckBox;
    KE_F_ITAL: TDBCheckBox;
    KE_F_SUBR: TDBCheckBox;
    Label12: TLabel;
    KE_BITMAP: TDBEdit;
    bBuscaImagen: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bBuscaImagenClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  EditTipoNoticias: TTOEditEstilosBotones;

implementation

uses DPortal,
     DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaBuscador,
     ZetaCommonTools;

{$R *.DFM}

procedure TTOEditEstilosBotones.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 0;
     IndexDerechos := 0; //D_PORTAL_CAT_NOTICIAS_TIPO;
     with dmPortal do
     begin
          Self.Caption := cdsEstilosBotones.LookupName;
     end;
end;

procedure TTOEditEstilosBotones.Connect;
begin
     with dmPortal do
     begin
          cdsEstilosBotones.Conectar;
          DataSource.DataSet := cdsEstilosBotones;
     end;

     KE_F_NAME.Datasource := DataSource;
     KE_F_NAME.DataField := 'KE_F_NAME';
end;

procedure TTOEditEstilosBotones.DoLookup;
begin
     inherited;
     with dmPortal do
     begin
          ZetaBuscador.BuscarCodigo( 'C�digo', Caption, cdsEstilosBotones.LookupKeyField, cdsEstilosBotones );
     end;
end;

procedure TTOEditEstilosBotones.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     with FontDialog do
     begin
          Font.Name := KE_F_NAME.SelectedFont;
          Font.Size := KE_F_SIZE.ValorEntero;
          Font.Style := [];
          Font.Color := KE_F_COLR.SelectedColor;
          if KE_F_BOLD.Checked then
             Font.Style := [fsBold];
          if KE_F_ITAL.Checked then
             Font.Style := Font.Style + [fsItalic];
          if KE_F_SUBR.Checked then
             Font.Style := Font.Style + [fsUnderline];

          if Execute then
          begin
               with dmPortal.cdsEstilosBotones do
               begin
                    if ( State = dsBrowse ) then Edit;
                    FieldByName( 'KE_F_NAME' ).AsString := Font.Name;
                    FieldByName( 'KE_F_SIZE' ).AsInteger := Font.Size;
                    FieldByName( 'KE_F_COLR' ).AsInteger := Font.Color;

                    FieldByName( 'KE_F_BOLD' ).AsString := zBoolToStr( fsBold in Font.Style );
                    FieldByName( 'KE_F_ITAL' ).AsString := zBoolToStr( fsItalic in Font.Style );
                    FieldByName( 'KE_F_SUBR' ).AsString := zBoolToStr( fsUnderline in Font.Style );
               end;
          end;
     end;
end;

procedure TTOEditEstilosBotones.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := KE_CODIGO;

end;

procedure TTOEditEstilosBotones.bBuscaImagenClick(Sender: TObject);
begin
     inherited;
     with ClientDataset do
     begin
          if ( State = dsBrowse ) then Edit;
             with FieldByName('KE_BITMAP') do
                  AsString := OpenDialogImagen( AsString, 'Bitmaps (*.bmp)|*.bmp|Bitmaps (*.bmp)|*.bmp', '' );


     end;
end;

end.
