unit FMarcos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TTOMarcos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  TOMarcos: TTOMarcos;

implementation

uses dPortal,
     ZetaBuscador;
     {,
     DSistema,
     DGlobal,
     ZHelpContext,
     ZAccesosTress,
     ;}

{$R *.DFM}

{ TCarruseles }

procedure TTOMarcos.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := 0;
     IndexDerechos := 0;//D_PORTAL_INFO_DOCUMENTOS;
end;

procedure TTOMarcos.Connect;
begin
     with dmPortal do
     begin
          cdsMarcosInformacion.Conectar;
          DataSource.DataSet:= cdsMarcosInformacion;
     end;
end;

procedure TTOMarcos.Agregar;
begin
     dmPortal.cdsMarcosInformacion.Agregar;
end;

procedure TTOMarcos.Borrar;
begin
     dmPortal.cdsMarcosInformacion.Borrar;
end;

procedure TTOMarcos.Modificar;
begin
     dmPortal.cdsMarcosInformacion.Modificar;
end;

procedure TTOMarcos.Refresh;
begin
     dmPortal.cdsMarcosInformacion.Refrescar;
end;

procedure TTOMarcos.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Folio', 'Marcos de información', 'KS_CODIGO', dmPortal.cdsMarcosInformacion );
end;

end.
