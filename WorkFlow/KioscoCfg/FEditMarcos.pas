unit FEditMarcos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicionRenglon, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls, ZetaEdit,
  ZetaKeyCombo, fcCombo, fcColorCombo, ZetaNumero, Mask, ZetaKeyLookup,
  ZetaCommonLists,
  DPortal;

type
  TEdicionMarcos = class(TBaseEdicionRenglon)
    Label1: TLabel;
    KS_CODIGO: TZetaDBEdit;
    KS_NOMBRE: TDBEdit;
    Label2: TLabel;
    ColorDialog: TColorDialog;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    KS_PF_SIZE: TZetaDBNumero;
    KS_PF_URL: TDBEdit;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    KS_PB_SIZE: TZetaDBNumero;
    KS_PB_COLR: TfcColorCombo;
    KS_PB_ALIN: TZetaDBKeyCombo;
    GroupBox3: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    KS_BTN_ALT: TZetaDBNumero;
    KS_BTN_SEP: TZetaDBNumero;
    KS_EST_NOR: TZetaDBKeyLookup;
    KS_EST_SEL: TZetaDBKeyLookup;
    Panel3: TPanel;
    bArriba: TZetaSmartListsButton;
    bAbajo: TZetaSmartListsButton;
    Label12: TLabel;
    KS_BITMAP: TDBEdit;
    bBuscaImagen: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure bArribaClick(Sender: TObject);
    procedure bAbajoClick(Sender: TObject);
    procedure bBuscaImagenClick(Sender: TObject);
  private
    { Private declarations }
    procedure SeleccionaPrimerColumna;
    procedure SeleccionaSiguienteRenglon;
    procedure SubirBajarBoton(eAccion: eSubeBaja);
  protected
    { Protected declarations }
    procedure Connect;override;
    procedure DoCancelChanges;override;
    procedure KeyPress(var Key: Char);override;
    function PosicionaSiguienteColumna: Integer;override;
  public
    { Public declarations }
  end;

var
  EdicionMarcos: TEdicionMarcos;

implementation

uses
    ZetaDialogo;

{$R *.dfm}

const
     PRIMER_COLUMNA = 0;

procedure TEdicionMarcos.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := 0; //D_CAT_CAPA_SESIONES;
     HelpContext:= 0;//H60647_Sesiones;
     FirstControl := KS_CODIGO;
     with dmPortal do
     begin
          KS_EST_NOR.LookupDataset := cdsEstilosBotones;
          KS_EST_SEL.LookupDataset := cdsEstilosBotones;
     end;
     GridRenglones.Options := [dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect];
     KS_PB_ALIN.ListaFija := lfKJustificacion;
end;

procedure TEdicionMarcos.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePAge := Datos;
end;

procedure TEdicionMarcos.Connect;
begin
     with dmPortal do
     begin
          DataSource.DataSet:= cdsMarcosInformacion;

          cdsBotones.Refrescar;
          dsRenglon.DataSet := cdsBotones;
     end;
end;

procedure TEdicionMarcos.DoCancelChanges;
begin
     dmPortal.cdsMarcosInformacion.CancelUpdates;
     inherited DoCancelChanges;
end;

procedure TEdicionMarcos.SeleccionaSiguienteRenglon;
begin
     with dmPortal.cdsMarcosInformacion do
     begin
          Next;
          if Eof then
          begin
               SeleccionaPrimerColumna;
               Agregar;
          end
          else
              GridRenglones.SelectedIndex := 0;
     end;
end;


procedure TEdicionMarcos.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
               if ( GridRenglones.SelectedIndex = 0 ) then
                  SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   SeleccionaPrimerColumna;
              end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

function TEdicionMarcos.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
               Result := 0
          else
              Result := i;
     end;
end;

procedure TEdicionMarcos.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ PRIMER_COLUMNA ].Field;   // Posicionar en la primer columna
end;

procedure TEdicionMarcos.BBAgregarClick(Sender: TObject);
begin
     //inherited;
     dmPortal.cdsBotones.Agregar;
end;

procedure TEdicionMarcos.BBModificarClick(Sender: TObject);
begin
     inherited;
     dmPortal.cdsBotones.Modificar;
end;

procedure TEdicionMarcos.bArribaClick(Sender: TObject);
begin
     inherited;
     SubirBajarBoton(eSubir);
end;

procedure TEdicionMarcos.bAbajoClick(Sender: TObject);
begin
     inherited;
     SubirBajarBoton(eBajar);
end;

procedure TEdicionMarcos.SubirBajarBoton(eAccion: eSubeBaja);
const
     K_PRIMERO = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmPortal do
        begin
             iPosActual := dmPortal.cdsBotones.RecNo;
             case( eAccion ) of
                   eSubir: begin
                                if( iPosActual = K_PRIMERO )then
                                    ZetaDialogo.ZError( self.Caption, '� Es el inicio de la lista de botones !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual - 1;
                                     CambiaOrdenBotones( iPosActual, iNuevaPos );
                                     //RefrescaGrid( iNuevaPos );
                                end;
                           end;
                   eBajar: begin
                                if( iPosActual = cdsBotones.RecordCount  )then
                                    ZetaDialogo.ZError( self.Caption, '� Es el final de la lista de botones !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual + 1;
                                     CambiaOrdenBotones( iPosActual, iNuevaPos );
                                     //RefrescaGrid( iNuevaPos );
                                end;
                           end;
             else
                 ZetaDialogo.ZError( self.Caption, '� Operaci�n no v�lida !', 0 )
             end;
        end;
     finally
             Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;
procedure TEdicionMarcos.bBuscaImagenClick(Sender: TObject);
begin
     inherited;
     with ClientDataset do
     begin
          if ( State = dsBrowse ) then Edit;
             with FieldByName('KS_BITMAP') do
                  AsString := OpenDialogImagen( AsString, 'Todas (*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf|JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf|Metafiles (*.wmf)|*.wmf ', '' );


     end;
end;

end.
