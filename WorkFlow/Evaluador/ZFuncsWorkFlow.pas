unit ZFuncsWorkFlow;

interface

uses Classes, Sysutils, DB, DBClient,
     ZetaQRExpr,
     ZEvaluador,
     DZetaServerProvider,
     ZetaCommonClasses,
     ZetaCommonTools;
procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses
    ZetaTipoEntidad,
    ZFuncsGlobal;

type
  TZetaUsuario = class( TZetaFunc )
  private
    FDataSet : TClientDataSet;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaUsuario.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FDataSet := TClientDataSet.Create( oZetaProvider );
end;

destructor TZetaUsuario.Destroy;
begin
     FDataSet.Free;
     inherited Destroy;
end;

procedure TZetaUsuario.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     oZetaProvider.GetDatosActivos;
end;

function TZetaUsuario.Calculate: TQREvResult;
const
     sSQL = 'select US_CODIGO, US_NOMBRE from USUARIO where US_CODIGO = %s';
var
   iUsuario: Integer;
begin
     with oZetaProvider, Result do
     begin
          iUsuario:= DefaultInteger( 0, UsuarioActivo );
          Kind := resString;
          if ( iUsuario = UsuarioActivo ) then
             strResult := NombreUsuario
          else
          begin
               with FDataSet do
               begin
                    if ( not Active ) or ( FieldByName( 'US_CODIGO' ).AsInteger <> iUsuario ) then
                       Data := OpenSQL( Comparte, Format( sSQL, [ IntToStr( iUsuario ) ] ), TRUE );
                    strResult:= FieldByName( 'US_NOMBRE' ).AsString;
               end;
          end;
     end;
end;


{ ************ TZetaXMLInfo ******************}
type TZetaXMLInfo = class( TZetaFunc )
  private
         oDataset: TZetaCursor;
  protected
    function GetTabla : string; virtual;
    function GetCampo : string; virtual;
    function GetCampoLlave : string; virtual;
    function GetLlave : integer; virtual;
    function GetPath : string; virtual;
    function GetAtributo : string; virtual;
    function GetPos : integer; virtual;
    function GetTipo( var lEsFecha : boolean )  : TQREvResultType; virtual;
    //resInt, resDouble, resString, resBool, resError;
  public
    constructor Create(oEvaluator:TQrEvaluator); override;
    destructor Destroy;override;
    procedure GetRequeridos(var TipoRegresa: TQREvResultType);override;
    function Calculate: TQREvResult; override;
end;

constructor TZetaXMLInfo.Create(oEvaluator:TQrEvaluator);
begin
     inherited Create(oEvaluator);
end;

destructor TZetaXMLInfo.Destroy;
begin
     FreeAndNil(oDataset);
end;

procedure TZetaXMLInfo.GetRequeridos(var TipoRegresa: TQREvResultType);
var
   lEsFecha : boolean;
begin
     TipoRegresa := GetTipo( lEsFecha );
     EsFecha := lEsFecha;
     ParametrosFijos( 1 );
end;


function TZetaXMLInfo.GetCampo: string;
begin
     Result := VACIO;
end;

function TZetaXMLInfo.GetCampoLlave: string;
begin
   Result := 'LLAVE';
end;

function TZetaXMLInfo.GetLlave: integer;
begin
     Result := DataSetEvaluador.FieldByName(GetCampoLlave).AsInteger;
end;

function TZetaXMLInfo.GetPath: string;
begin
     Result := ParamString(0);
end;



//resInt, resDouble, resString, resBool, resError
function TZetaXMLInfo.GetTipo( var lEsFecha : boolean )  : TQREvResultType;
var
   iTipo : integer;
begin
{
    1 = Texto
    2 = Entero
    3 = Numero con Decimales
    4 = Fecha
}
    iTipo := DefaultInteger( 1, 1 );
    lEsFecha := FALSE;
    case iTipo of
         1 :  Result := resString;
         2 :  Result := resInt;
         3 :  Result := resDouble;
         4 :  begin Result := resDouble;  lEsFecha := TRUE; end;
    else
        Result := resString;
    end;
end;

function TZetaXMLInfo.GetPos: integer;
begin
     Result := DefaultInteger( 2, 1 );
end;

function TZetaXMLInfo.GetAtributo: string;
begin
    Result := DefaultString( 3, VACIO );
end;


function TZetaXMLInfo.GetTabla: string;
begin
     Result := VACIO;
end;

function TZetaXMLInfo.Calculate: TQREvResult;
const
      Q_GET_XML_INFO = '{CALL SP_GET_XML_INFO(:Tabla, :Campo, :CampoLlave, :Llave, :Path, :Resultado, :Atributo, :Pos)}';
var
   sValor : string;
   lEsFecha : boolean;
begin
     with Result do
     begin
          intResult := 0;
          dblResult := 0;
          Result.booResult := FALSE;
          strResult := '';
          Kind := GetTipo( lEsFecha );
          resFecha := lEsFecha;
          EsFecha := lEsFecha;
     end;

     with oZetaPRovider do
     begin
          oDataset := CreateQuery( Q_GET_XML_INFO );
          try
             try
                 with oDataset do
                 begin
                      ParamAsString(oDataset, 'Tabla', Self.GetTabla );
                      ParamAsString(oDataset, 'Campo', GetCampo );
                      ParamAsString(oDataset, 'CampoLlave', GetCampoLlave );
                      ParamAsString(oDataset, 'Path', GetPath );
                      ParamAsString(oDataset, 'Atributo', GetAtributo );
                      ParamAsInteger( oDataset, 'Llave', GetLlave );
                      ParamAsInteger( oDataset, 'Pos', GetPos );
                      ParamSalida( oDataset, 'Resultado' );
                      Ejecuta( oDataset );
                      sValor :=GetParametro( oDataset, 'Resultado' ).AsString;
                      Result.strResult := sValor;


                      case Result.Kind of
                           resInt :Result.intResult := StrToIntDef( sValor, 0  );
                           resDouble :
                           begin
                                      if ( Result.resFecha ) then
                                        Result.dblResult :=  StrToFecha( sValor )
                                      else
                                          Result.dblResult := StrToFloatDef( sValor, 0.00 );
                           end;
                           resString : Result.strResult := sValor;
                           resBool : Result.booResult := StrToBoolDef( Result.strResult, FALSE );
                      else
                          Result.strResult := sValor;
                      end;
                 end;
             except
                   on Error: Exception do
                   begin
                   end;
             end;
          finally
                 FreeAndNil( oDataSet );
          end;
     end;
end;
{ *************************}

{ ************ TZetaWFXMLInfo ******************}
type TZetaWFXMLInfo = class( TZetaXMLInfo )
  private
  protected
    function GetTabla : string; override;
    function GetCampo : string; override;
    function GetCampoLlave : string; override;
  public
    procedure GetRequeridos(var TipoRegresa: TQREvResultType);override;
end;


function TZetaWFXMLInfo.GetCampo: string;
begin
     Result := 'WP_XMLDATA';
end;

function TZetaWFXMLInfo.GetCampoLlave: string;
begin
     Result := 'WP_FOLIO';
end;


function TZetaWFXMLInfo.GetTabla: string;
begin
      Result := 'V_PROCESO';
end;


procedure TZetaWFXMLInfo.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;

     AgregaRequerido( GetTabla + '.'+ GetCampoLlave, enProceso);
end;

{ ************ TZetaWFXMLAInfo ******************}
type TZetaWFXMLInfoEsp = class( TZetaXMLInfo )
  private
  protected
    function GetTabla : string; override;
    function GetCampo : string; override;
    function GetCampoLlave : string; override;
    function GetAtributo : string; override;
    function GetPath : string; override;
    function GetPos : integer; override;
  public
    procedure GetRequeridos(var TipoRegresa: TQREvResultType);override;
end;


function TZetaWFXMLInfoEsp.GetCampo: string;
begin
     Result := 'WP_XMLADD';
end;

function TZetaWFXMLInfoEsp.GetCampoLlave: string;
begin
     Result := 'WP_FOLIO';
end;


function TZetaWFXMLInfoEsp.GetTabla: string;
begin
      Result := 'V_PROCESO';
end;


procedure TZetaWFXMLInfoEsp.GetRequeridos(var TipoRegresa: TQREvResultType);
begin
     inherited;
     AgregaRequerido( GetTabla + '.'+ GetCampoLlave, enProceso);
end;

{ *************************}


procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( ZFuncsGlobal.TGetGlobal, 'GLOBAL' );
          RegisterFunction( ZFuncsGlobal.TZetaActivo, 'ACTIVO' );
          RegisterFunction( TZetaUSUARIO, 'USUARIO' );
          RegisterFunction( TZetaWFXMLInfo, 'WFINFOXML','Obtenci�n de Info. de XML de Workflow' );
          RegisterFunction( TZetaWFXMLInfoEsp, 'WFINFOESP','Obtenci�n de Info. de XML Especial de Workflow' );
     end;
end;

function TZetaWFXMLInfoEsp.GetAtributo: string;
begin
     Result := VACIO;
end;

function TZetaWFXMLInfoEsp.GetPos: integer;
begin
     Result := DefaultInteger( 0, 1 );
end;


function TZetaWFXMLInfoEsp.GetPath: string;
begin
     Result := '//CAMPO/VALUE';
end;

end.
