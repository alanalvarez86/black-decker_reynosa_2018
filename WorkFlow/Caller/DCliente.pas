unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,{$ifndef VER130}Variants,{$endif}
     {$ifdef DOS_CAPAS}
     DServerRecursos,
     DZetaServerProvider,
     ZetaSQLBroker,
     {$else}
     Recursos_TLB,
     {$endif}
     DBasicoCliente,
     DXMLTools,
     //ZetaCommonTypes,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    cdsLista: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FChanged: Boolean;
    FXMLTools: TdmXMLTools;
    FStatus: String;
    FEmpleado: TNumEmp;
    {$ifdef DOS_CAPAS}
    function GetServerRecursos: TdmServerRecursos;
    {$else}
    FServerRecursos: IdmServerRecursosDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    {$endif}
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    procedure ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure XMLToDateField( Datos: TDataset; const sTag, sField: String ); overload;
    procedure XMLToDateField( Datos: TDataset; const sTag: String ); overload;
    procedure XMLToStringField( Datos: TDataset; const sTag, sField: String ); overload;
    procedure XMLToStringField( Datos: TDataset; const sTag: String ); overload;
  protected
    { Protected declarations }
    property Changed: Boolean read FChanged write FChanged;
    property Empleado: TNumEmp read FEmpleado write FEmpleado;
    {$ifdef DOS_CAPAS}
    property ServerRecursos :TdmServerRecursos read GetServerRecursos;
    {$else}
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    {$endif}
    property Status: String read FStatus write FStatus;
    function LoadEmpleado: Boolean;
    function LoadEmpresa: Boolean;
    function LoadUsuario( const iUsuario: Integer ): Boolean;
    function LoadXML( const sXML: String ): Boolean;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function EmpleadoCambiarDatos( const sXML: String; var sLog: WideString ): Boolean;
  end;

var
  dmCliente: TdmCliente;

implementation

uses FAutoClasses,
     ZetaCommonTools,
     ZetaRegistryCliente;

{$R *.DFM}

{ ********* TdmCliente ********** }

constructor TdmCliente.Create(AOwner: TComponent);
begin
     FChanged := False;
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     inherited Create( AOwner );
end;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     FXMLTools := TdmXMLTools.Create( Self );
     {$ifdef DOS_CAPAS}
     FServerRecursos := TdmServerRecursos.Create( Self );
     {$endif}
     inherited;
end;

destructor TdmCliente.Destroy;
begin
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FXMLTools );
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerRecursos );
     {$endif}
end;

{$ifdef DOS_CAPAS}
function TdmCliente.GetServerRecursos: TdmServerRecursos;
begin
     Result:= FServerRecursos;
end;
{$else}
function TdmCliente.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( Self.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;
{$endif}

{ ****** Manejos Internos ****** }

function TdmCliente.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmCliente.SetEmpresaActiva(const sCodigo: String): Boolean;
begin
     InitCompanies;
     Result := FindCompany( sCodigo );
     if Result then
     begin
          SetCompany
     end;
end;

procedure TdmCliente.ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raCancel;
     Status := E.Message;
end;

procedure TdmCliente.XMLToStringField( Datos: TDataset; const sTag, sField: String );
var
   sNewValue, sOldValue: String;
begin
     with Datos.FieldByName( sField ) do
     begin
          sOldValue := AsString;
          with FXMLTools do
          begin
               sNewValue := TagAsString( GetNode( sTag ), sOldValue );
          end;
          if ( sNewValue <> sOldValue ) then
          begin
               Changed := True;
               AsString := sNewValue;
          end;
     end;
end;

procedure TdmCliente.XMLToStringField( Datos: TDataset; const sTag: String );
begin
     XMLToStringField( Datos, sTag, sTag );
end;

procedure TdmCliente.XMLToDateField( Datos: TDataset; const sTag, sField: String );
var
   dNewValue, dOldValue: TDate;
begin
     with Datos.FieldByName( sField ) do
     begin
          dOldValue := AsDateTime;
          with FXMLTools do
          begin
               dNewValue := TagAsDate( GetNode( sTag ), dOldValue );
          end;
          if ( dNewValue <> dOldValue ) then
          begin
               Changed := True;
               AsDateTime := dNewValue;
          end;
     end;
end;

procedure TdmCliente.XMLToDateField( Datos: TDataset; const sTag: String );
begin
     XMLToDateField( Datos, sTag, sTag );
end;

{ ***** Cargado de XML y Valores Activos }

function TdmCliente.LoadXML( const sXML: String ): Boolean;
begin
     Result := FALSE;
     Status := '';
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Status := 'Error Al Cargar Documento: ' + Error.Message;
           end;
     end;
end;

function TdmCliente.LoadUsuario( const iUsuario: Integer ): Boolean;
begin
     Result := FALSE;
     try
        Self.Usuario := iUsuario;
        Result := True;
     except
           on Error: Exception do
           begin
                Status := Format( 'Error Al Cargar Usuario %d: %s', [ iUsuario, Error.Message ] );
           end;
     end;
end;

function TdmCliente.LoadEmpleado: Boolean;
begin
     Result := FALSE;
     try
        with FXMLTools do
        begin
             Self.Empleado := TagAsInteger( GetNode( 'EMPLEADO' ), 0 );
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Status := Format( 'Error Al Cargar Empleado: %s', [ Error.Message ] );
           end;
     end;
end;

function TdmCliente.LoadEmpresa: Boolean;
var
   sEmpresa: String;
begin
     Result := FALSE;
     Status := '';
     try
        with FXMLTools do
        begin
             sEmpresa := TagAsString( GetNode( 'EMPRESA' ), '' );
        end;
        if ZetaCommonTools.StrVacio( sEmpresa ) then
           Status := 'Empresa No Especificada'
        else
        begin
             if SetEmpresaActiva( sEmpresa ) then
                Result := True
             else
                 Status := Format( 'Empresa %s No Existe', [ sEmpresa ] );
        end;
     except
           on Error: Exception do
           begin
                Status := 'Error Al Examinar Documento: ' + Error.Message;
           end;
     end;
end;

{ ***** Implementaciones de Llamadas al Servidor de Procesos ***** }

function TdmCliente.EmpleadoCambiarDatos( const sXML: String; var sLog: WideString ): Boolean;
var
   iErrores: Integer;
   Datos: OleVariant;
begin
     Result := False;
     Changed := False;
     if LoadXML( sXML ) and LoadEmpresa and LoadEmpleado then
     begin
          try
             if Servidor.GetEmpleado( Empresa, Self.Empleado, Datos ) then
             begin
                  with cdsLista do
                  begin
                       Data := Datos;
                       Edit;
                       XMLToStringField( cdsLista, 'CB_PASAPOR' );
                       XMLToStringField( cdsLista, 'CB_VIVECON' );
                       XMLToStringField( cdsLista, 'CB_VIVEEN' );
                       XMLToStringField( cdsLista, 'CB_MED_TRA' );
                       XMLToStringField( cdsLista, 'CB_CALLE' );
                       XMLToStringField( cdsLista, 'CB_COLONIA' );
                       XMLToStringField( cdsLista, 'CB_CIUDAD' );
                       XMLToStringField( cdsLista, 'CB_CODPOST' );
                       XMLToStringField( cdsLista, 'CB_ESTADO' );
                       XMLToStringField( cdsLista, 'CB_ZONA' );
                       XMLToStringField( cdsLista, 'CB_TEL' );
                       XMLToDateField( cdsLista, 'CB_FEC_RES' );
                       Post;
                       OnReconcileError := ReconcileError;
                       if Changed then
                          Result := Reconciliar( ServerRecursos.GrabaDatosEmpleado( Empresa, Delta, Null, '', iErrores ) )
                       else
                           Result := True;
                  end;
             end
             else
             begin
                  Status := Format( 'No Hay Datos Para El Empleado %d', [ Self.Empleado ] );
             end;
          except
                on Error: Exception do
                begin
                     Status := Format( 'Error Al Cambiar Datos Del Empleado: %s', [ Error.Message ] );
                end;
          end;
     end;
     sLog := Status;
end;

end.
