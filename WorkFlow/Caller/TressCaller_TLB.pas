unit TressCaller_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 17/11/2004 11:40:21 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: D:\3WorkFlow\Caller\TressCaller.tlb (1)
// LIBID: {82C1FFAC-D1D0-4F06-A498-DCB2F200B300}
// LCID: 0
// Helpfile: 
// HelpString: TressCaller Library
// DepndLst: 
//   (1) v1.0 Midas, (C:\WINDOWS\System32\midas.dll)
//   (2) v2.0 stdole, (C:\WINDOWS\System32\STDOLE2.TLB)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, Midas, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TressCallerMajorVersion = 1;
  TressCallerMinorVersion = 0;

  LIBID_TressCaller: TGUID = '{82C1FFAC-D1D0-4F06-A498-DCB2F200B300}';

  IID_ITressProcessCalls: TGUID = '{49EA784D-FDFA-4BB2-8D83-C6D6AEDF5C37}';
  CLASS_TressProcessCalls: TGUID = '{51CDE67B-4584-4DD0-A450-3B775F834889}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ITressProcessCalls = interface;
  ITressProcessCallsDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  TressProcessCalls = ITressProcessCalls;


// *********************************************************************//
// Interface: ITressProcessCalls
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49EA784D-FDFA-4BB2-8D83-C6D6AEDF5C37}
// *********************************************************************//
  ITressProcessCalls = interface(IAppServer)
    ['{49EA784D-FDFA-4BB2-8D83-C6D6AEDF5C37}']
    function EmpleadoDatosGrabar(const Datos: WideString; out Mensaje: WideString): WordBool; safecall;
  end;

// *********************************************************************//
// DispIntf:  ITressProcessCallsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {49EA784D-FDFA-4BB2-8D83-C6D6AEDF5C37}
// *********************************************************************//
  ITressProcessCallsDisp = dispinterface
    ['{49EA784D-FDFA-4BB2-8D83-C6D6AEDF5C37}']
    function EmpleadoDatosGrabar(const Datos: WideString; out Mensaje: WideString): WordBool; dispid 301;
    function AS_ApplyUpdates(const ProviderName: WideString; Delta: OleVariant; MaxErrors: Integer; 
                             out ErrorCount: Integer; var OwnerData: OleVariant): OleVariant; dispid 20000000;
    function AS_GetRecords(const ProviderName: WideString; Count: Integer; out RecsOut: Integer; 
                           Options: Integer; const CommandText: WideString; var Params: OleVariant; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000001;
    function AS_DataRequest(const ProviderName: WideString; Data: OleVariant): OleVariant; dispid 20000002;
    function AS_GetProviderNames: OleVariant; dispid 20000003;
    function AS_GetParams(const ProviderName: WideString; var OwnerData: OleVariant): OleVariant; dispid 20000004;
    function AS_RowRequest(const ProviderName: WideString; Row: OleVariant; RequestType: Integer; 
                           var OwnerData: OleVariant): OleVariant; dispid 20000005;
    procedure AS_Execute(const ProviderName: WideString; const CommandText: WideString; 
                         var Params: OleVariant; var OwnerData: OleVariant); dispid 20000006;
  end;

// *********************************************************************//
// The Class CoTressProcessCalls provides a Create and CreateRemote method to          
// create instances of the default interface ITressProcessCalls exposed by              
// the CoClass TressProcessCalls. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTressProcessCalls = class
    class function Create: ITressProcessCalls;
    class function CreateRemote(const MachineName: string): ITressProcessCalls;
  end;

implementation

uses ComObj;

class function CoTressProcessCalls.Create: ITressProcessCalls;
begin
  Result := CreateComObject(CLASS_TressProcessCalls) as ITressProcessCalls;
end;

class function CoTressProcessCalls.CreateRemote(const MachineName: string): ITressProcessCalls;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_TressProcessCalls) as ITressProcessCalls;
end;

end.
