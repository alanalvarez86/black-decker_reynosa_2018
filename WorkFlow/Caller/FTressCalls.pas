unit FTressCalls;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComServ, ComObj, VCLCom, StdVcl, BDEMts, DataBkr, DBClient, MtsRdm, Mtx,
     DCliente,
     TressCaller_TLB;

type
  TTressProcessCalls = class(TMtsDataModule, ITressProcessCalls)
    procedure MtsDataModuleCreate(Sender: TObject);
    procedure MtsDataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FdmCliente: TdmCliente;
  protected
    { Protected declarations }
    class procedure UpdateRegistry(Register: Boolean; const ClassID, ProgID: string); override;
    property dmCliente: TdmCliente read FdmCliente;
    function EmpleadoDatosGrabar(const Datos: WideString; out Mensaje: WideString): WordBool; safecall;
  public
    { Public declarations }
  end;

var
  TressProcessCalls: TTressProcessCalls;

implementation

{$R *.DFM}

class procedure TTressProcessCalls.UpdateRegistry(Register: Boolean; const ClassID, ProgID: string);
begin
     if Register then
     begin
       inherited UpdateRegistry(Register, ClassID, ProgID);
       EnableSocketTransport(ClassID);
       EnableWebTransport(ClassID);
     end else
     begin
       DisableSocketTransport(ClassID);
       DisableWebTransport(ClassID);
       inherited UpdateRegistry(Register, ClassID, ProgID);
     end;
end;

procedure TTressProcessCalls.MtsDataModuleCreate(Sender: TObject);
begin
     FdmCliente := TdmCliente.Create( Self );
end;

procedure TTressProcessCalls.MtsDataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FdmCliente );
end;

function TTressProcessCalls.EmpleadoDatosGrabar(const Datos: WideString; out Mensaje: WideString): WordBool;
begin
     try
        Result := dmCliente.EmpleadoCambiarDatos( Datos, Mensaje );
        SetComplete;
     except
           SetAbort;
           raise;
     end;
end;

initialization
  TComponentFactory.Create(ComServer, TTressProcessCalls, Class_TressProcessCalls, ciMultiInstance, tmApartment);
end.