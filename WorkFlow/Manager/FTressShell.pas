unit FTressShell;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     WorkFlow                                   ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FTressShell.pas                            ::
  :: Descripci�n: Programa p211`w11123rincipal de Tress                ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls, Buttons, WinSvc,
     ZetaClientDataset,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZBaseArbolShell,
     ZetaSmartLists,
     ZBaseShell, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, cxStyles, cxClasses, dxSkinsForm,
  dxRibbon, dxStatusBar, dxRibbonStatusBar;

type                                                            
  TTressShell = class(TBaseArbolShell)
    ServicioWorkFlow: TSpeedButton;
    ServicioEmail: TSpeedButton;
    ManagerServicesStatus: TAction;
    EmailServiceStatus: TAction;
    _P_CopiarModelo: TAction;
    Copiarmodelo1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure ServicioWorkFlowClick(Sender: TObject);
    procedure ManagerServicesStatusExecute(Sender: TObject);
    procedure EmailServiceStatusExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure _P_CopiarModeloExecute(Sender: TObject);
  private
    { Private declarations }
    procedure RevisaDerechos;
    //procedure ServicesStatus(Servicio: TSpeedButton; ServicioAction: TAction; const sServiceStatus, sServiceMsg: string);
  protected
    { Protected declarations }
    procedure AbreEmpresa(const lDefault: Boolean);override;
    procedure CargaDerechos; override;
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure HabilitaControles; override;
    //procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);override;
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override;
  public
    { Public declarations }
    function GetLookUpDataSet( const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
  end;

var
  TressShell: TTressShell;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZetaDespConsulta,
     ZHelpContext,
     FAutoClasses,
     DWorkflowConst,
     DWorkflow,
     DCliente,
     DGlobal,
     DSistema,
     DCatalogos,
     DConsultas,
     DReportes,
     DDiccionario,
     ZWizardBasico,
     FWizCopiarModelo;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmWorkFlow := TdmWorkFlow.Create( Self );
     inherited;

     HelpContext := H_WORKFLOWCFG_INTERFAZADMIN;
     ArchivoConfigurarEmpresa.HelpContext  := H_WORKFLOWCFG_INTERFAZADMIN;
     Arbol.HelpContext := H_WORKFLOWCFG_INTERFAZADMIN;
     FHelpGlosario := 'Workflow.chm';
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( dmWorkFlow );
     FreeAndNil( dmConsultas );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmReportes );
     FreeAndNil( dmCatalogos );
     FreeAndNil( dmSistema );
     FreeAndNil( dmCliente );
     FreeAndNil( Global );
end;

procedure TTressShell.AbreEmpresa(const lDefault: Boolean);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmCliente do
        begin
             InitComparteCompany;
             SetCompany;
        end;
        DoOpenAll;
        SetTimerInfo;
     finally
        Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TTressShell.DoOpenAll;
begin
     try
        inherited DoOpenAll;
        {***US 11757: Proteccion HTTP SkinController = True
        Evita que el backgound de algunos componentes de la aplicacion
        sea de color blanco
        ***}
        DevEx_SkinController.NativeStyle := TRUE;
        Global.Conectar;
        CreaArbol( True );
        RevisaDerechos;
     except
           on Error : Exception do
           begin
                ZetaDialogo.zError( 'Error Al Abrir Datos', Error.Message, 0 );
                DoCloseAll;
           end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     inherited DoCloseAll;
end;

 //Para Compilar en 2.8
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
//procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
begin
     dmCliente.NotifyDataChange( Entidades, Estado );
     dmCatalogos.NotifyDataChange( Entidades, Estado );
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell.CargaDerechos;
begin
     inherited CargaDerechos;
end;

procedure TTressShell.RevisaDerechos;
begin
     Tag := 0;
end;

procedure TTressShell.HabilitaControles;
begin
     inherited HabilitaControles;
     Editar.Enabled := EmpresaAbierta;
     Registro.Enabled := EmpresaAbierta;
     Procesos.Enabled := EmpresaAbierta;
     Ventana.Enabled := EmpresaAbierta;
     ArchivoConfigurarEmpresa.Enabled := EmpresaAbierta;
     ArchivoExplorardorReportes.Enabled := EmpresaAbierta;
     ArchivoCatalogoUsuarios.Enabled := EmpresaAbierta;
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     case eEntidad of
          enCompanys: Result := dmSistema.cdsEmpresas;
          enGrupo: Result := dmSistema.cdsGrupos;
          enUsuarios: Result := dmSistema.cdsUsuarios;
          enQuerys: Result := dmCatalogos.cdsCondiciones;
          enReporte: Result := dmReportes.cdsLookupReportes;
          enRol: Result := dmSistema.cdsRoles;
          enAccion: Result:= dmWorkflow.cdsAcciones;
          enModelo: Result:= dmWorkflow.cdsModelos
     else
         Result := NIL;
     end;
end;

{ **** Eventos del Shell **** }

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     inherited;
     AbreFormaConsulta( efcSistUsuarios );
end;

procedure TTressShell.ServicioWorkFlowClick(Sender: TObject);
var
   Parametros: TZetaParams;
   oCursor : TCursor;
begin
     inherited;
     Parametros := TZetaParams.Create;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        dmWorkFlow.ServicesStatus( Parametros );
     finally
            Screen.Cursor := oCursor;
            FreeAndNil( Parametros );
     end;
end;

{procedure TTressShell.ServicesStatus( Servicio: TSpeedButton;
                                      ServicioAction: TAction;
                                      const sServiceStatus, sServiceMsg: string );
const
     K_MENSAJE = 'Servicio de %s' + CR_LF + 'Status: %s';
     STATUS_VERDE = 32;
     STATUS_ROJO = 28;
     STATUS_AMARILLO = 30;
     STATUS_GRIS = 26;
var
   Parametros: TZetaParams;
   oCursor: TCursor;
   iImageIndex: integer;
   oBitmap: TBitmap;
   sHint: String;
begin
     inherited;
     Parametros := TZetaParams.Create;
     oBitmap := TBitmap.Create;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        sHint := 'Problema al leer status de servicios';
        iImageIndex := STATUS_GRIS;
        try
           dmWorkFlow.ServicesStatus( Parametros );
           with Parametros do
           begin
                sHint := ParamByName( sServiceMsg ).AsString + CR_LF + 'Presione el bot�n para revisar el Status';
                case ParamByName( sServiceStatus ).AsInteger of
                     SERVICE_RUNNING          : iImageIndex := STATUS_VERDE;
                     SERVICE_STOPPED          : iImageIndex := STATUS_ROJO;
                     SERVICE_START_PENDING    : iImageIndex := STATUS_AMARILLO;
                     SERVICE_STOP_PENDING     : iImageIndex := STATUS_ROJO;
                     SERVICE_CONTINUE_PENDING : iImageIndex := STATUS_AMARILLO;
                     SERVICE_PAUSE_PENDING    : iImageIndex := STATUS_AMARILLO;
                     SERVICE_PAUSED           : iImageIndex := STATUS_AMARILLO
                else
                    iImageIndex := STATUS_GRIS;
                    sHint := 'Consulta no disponible';
                end;
           end;
        except
              on Error: Exception do
              begin
                   ZetaDialogo.zExcepcion( '� Error !', 'Error al leer status de Servicios', Error, 0 );
              end;
        end;
        ServicioAction.Hint := sHint;
        Servicio.Glyph.FreeImage;
        ArbolImages.GetBitmap( iImageIndex, oBitmap );
        Servicio.Glyph := oBitmap;
     finally
            Screen.Cursor := oCursor;
            FreeAndNil( oBitmap );
            FreeAndNil( Parametros );
     end;
end;}

procedure TTressShell.EmailServiceStatusExecute(Sender: TObject);
begin
     inherited;
     //ServicesStatus( ServicioEmail, EmailServiceStatus, K_EMAIL_SERVICE_STATUS, K_EMAIL_SERVICE_MSG  ); OP: 8.Diciembre.2008
end;

procedure TTressShell.ManagerServicesStatusExecute(Sender: TObject);
begin
     inherited;
     //ServicesStatus( ServicioWorkFlow, ManagerServicesStatus, K_MANAGER_SERVICE_STATUS, K_MANAGER_SERVICE_MSG  );OP: 8.Diciembre.2008
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     //EmailServiceStatusExecute( Sender );
     //ManagerServicesStatusExecute( Sender );
     _P_CopiarModelo.Visible := Autorizacion.OkModulo(okWorkflow,True); 
end;

procedure TTressShell._P_CopiarModeloExecute(Sender: TObject);
begin
     inherited;
     ZWizardBasico.ShowWizard( TWizCopiarModelo );
end;

end.
