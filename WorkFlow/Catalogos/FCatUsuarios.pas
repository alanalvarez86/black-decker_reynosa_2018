unit FCatUsuarios;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatUsuarios = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    EBuscaNombre: TEdit;
    BBusca: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
  private
    { Private declarations }
    procedure FiltraUsuario;
    procedure LimpiaGrid;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatUsuarios: TCatUsuarios;

implementation

{$R *.DFM}

uses DSistema,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{ *********** TSistUsuarios *********** }

procedure TCatUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_USUARIOS;
     IndexDerechos := D_CAT_USUARIOS;
end;

procedure TCatUsuarios.Connect;
begin
     with dmSistema do
     begin
          ConectarUsuariosLookup;
          cdsUsuariosRol.Conectar;
          DataSource.DataSet:= cdsUsuariosRol;
     end;
end;

procedure TCatUsuarios.Refresh;
begin
     dmSistema.cdsUsuariosRol.Refrescar;
end;

function TCatUsuarios.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Permite Agregar Usuarios En Esta Pantalla';
end;

function TCatUsuarios.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Permite Borrar Usuarios En Esta Pantalla';
end;

procedure TCatUsuarios.Modificar;
begin
     dmSistema.cdsUsuariosRol.Modificar;
end;

procedure TCatUsuarios.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     BBusca.Down := Length(EBuscaNombre.Text)>0;
     FiltraUsuario;
end;

procedure TCatUsuarios.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraUsuario;
end;

procedure TCatUsuarios.FiltraUsuario;
begin
     with dmSistema.cdsUsuariosRol do
     begin
          Filtered := BBusca.Down;
          Filter := 'UPPER(US_NOMBRE) like '+ Chr(39) + '%' + UpperCase( EBuscaNombre.Text ) + '%' + Chr(39);
     end;
     LimpiaGrid;
end;

procedure TCatUsuarios.LimpiaGrid;
begin
     ZetadbGrid.SelectedRows.Clear;
end;

end.


