inherited CatPasosModeloEdit: TCatPasosModeloEdit
  Left = 85
  Top = 200
  Caption = 'Pasos de Modelo'
  ClientHeight = 372
  ClientWidth = 752
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 336
    Width = 752
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 584
    end
    inherited Cancelar: TBitBtn
      Left = 669
    end
  end
  inherited PanelSuperior: TPanel
    Width = 752
  end
  inherited PanelIdentifica: TPanel
    Width = 752
    inherited ValorActivo2: TPanel
      Width = 426
    end
  end
  inherited Panel1: TPanel
    Width = 752
    Height = 86
    object Label4: TLabel
      Left = 294
      Top = 59
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'Minutos'
    end
    object Label3: TLabel
      Left = 213
      Top = 59
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Horas'
    end
    object Label2: TLabel
      Left = 137
      Top = 59
      Width = 23
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#237'as'
    end
    object Label1: TLabel
      Left = 10
      Top = 58
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'L'#237'mite de tiempo:'
    end
    object WE_ORDENlbl: TLabel
      Left = 59
      Top = 33
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Orden:'
    end
    object WE_ORDEN: TZetaDBTextBox
      Left = 93
      Top = 32
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'WE_ORDEN'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WE_ORDEN'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WM_CODIGOlbl: TLabel
      Left = 53
      Top = 10
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Modelo:'
    end
    object WM_CODIGO: TZetaDBTextBox
      Left = 93
      Top = 8
      Width = 89
      Height = 17
      AutoSize = False
      Caption = 'WM_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WM_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WM_NOMBRE: TZetaDBTextBox
      Left = 185
      Top = 8
      Width = 273
      Height = 17
      AutoSize = False
      Caption = 'WM_NOMBRE'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WM_NOMBRE'
      DataSource = dsModelo
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object minutosVencimiento: TZetaNumero
      Left = 251
      Top = 56
      Width = 33
      Height = 21
      Mascara = mnDias
      TabOrder = 2
      Text = '0'
      OnExit = minutosVencimientoExit
    end
    object horasVencimiento: TZetaNumero
      Left = 170
      Top = 56
      Width = 33
      Height = 21
      Mascara = mnDias
      TabOrder = 1
      Text = '0'
      OnExit = horasVencimientoExit
    end
    object diasVencimiento: TZetaNumero
      Left = 93
      Top = 56
      Width = 33
      Height = 21
      Mascara = mnDias
      TabOrder = 0
      Text = '0'
      OnExit = diasVencimientoExit
    end
  end
  inherited PageControl: TPageControl
    Top = 137
    Width = 752
    Height = 199
    ActivePage = Formatos
    inherited Datos: TTabSheet
      Caption = 'Acci'#243'n'
      object WE_TIPDESTlbl: TLabel
        Left = 21
        Top = 146
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = '&Responsable:'
        FocusControl = WE_TIPDEST
      end
      object WE_DESCRIPlbl: TLabel
        Left = 27
        Top = 54
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = '&Descripci'#243'n:'
        FocusControl = WE_DESCRIP
      end
      object WE_NOMBRElb: TLabel
        Left = 0
        Top = 30
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = '&Nombre Del Paso:'
        FocusControl = WE_NOMBRE
      end
      object WA_CODIGOlbl: TLabel
        Left = 50
        Top = 7
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = '&Acci'#243'n:'
        FocusControl = WA_CODIGO
      end
      object WE_USRDESTFind: TSpeedButton
        Left = 430
        Top = 142
        Width = 23
        Height = 22
        Hint = 'Buscar Elemento'
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
          DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
          8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
          C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
          7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
        ParentShowHint = False
        ShowHint = True
        OnClick = WE_USRDESTFindClick
      end
      object WE_TIPDEST: TZetaDBKeyCombo
        Left = 89
        Top = 142
        Width = 144
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        OnChange = WE_TIPDESTChange
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'WE_TIPDEST'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object WE_USRDEST: TZetaDBEdit
        Left = 236
        Top = 142
        Width = 190
        Height = 21
        TabOrder = 4
        Text = 'WE_USRDEST'
        DataField = 'WE_USRDEST'
        DataSource = DataSource
      end
      object WE_DESCRIP: TDBMemo
        Left = 89
        Top = 49
        Width = 365
        Height = 89
        DataField = 'WE_DESCRIP'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 2
      end
      object WE_NOMBRE: TZetaDBEdit
        Left = 89
        Top = 26
        Width = 365
        Height = 21
        TabOrder = 1
        Text = 'WE_NOMBRE'
        DataField = 'WE_NOMBRE'
        DataSource = DataSource
      end
      object WA_CODIGO: TZetaDBKeyLookup
        Left = 89
        Top = 3
        Width = 365
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 80
        OnValidKey = WA_CODIGOValidKey
        DataField = 'WA_CODIGO'
        DataSource = DataSource
      end
    end
    inherited Tabla: TTabSheet
      Caption = 'Notificaciones'
      inherited GridRenglones: TZetaDBGrid
        Width = 744
        Height = 142
        ReadOnly = True
        Columns = <
          item
            Expanded = False
            FieldName = 'WY_QUIEN'
            Title.Caption = 'A quien'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_USR_NOT'
            Title.Caption = 'Responsable'
            Width = 109
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_CUANDO'
            Title.Caption = 'Cuando'
            Width = 57
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_QUE'
            Title.Caption = 'Acci'#243'n'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_TEM_STR'
            Title.Caption = 'Tiempo'
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_CAD_STR'
            Title.Caption = 'Intervalo'
            Width = 145
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_VECES'
            Title.Caption = 'Cuantas veces'
            Width = 81
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_TEXTO'
            Title.Caption = 'Observaciones'
            Width = 172
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'WY_XSL'
            Title.Caption = 'Plantilla'
            Width = 78
            Visible = True
          end>
      end
      inherited Panel2: TPanel
        Width = 744
      end
    end
    object PaginasWeb: TTabSheet
      Caption = 'P'#225'ginas Web'
      ImageIndex = 2
      object WM_URL_OKlbl: TLabel
        Left = 16
        Top = 17
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = '&Revisar:'
        FocusControl = WE_URL_OK
      end
      object WE_URL_OK: TZetaDBEdit
        Left = 58
        Top = 14
        Width = 403
        Height = 21
        TabOrder = 0
        Text = 'WE_URL_OK'
        DataField = 'WE_URL_OK'
        DataSource = DataSource
      end
    end
    object Formatos: TTabSheet
      Caption = 'Formatos'
      ImageIndex = 2
      object FormatosAvisosGB: TGroupBox
        Left = 0
        Top = 0
        Width = 744
        Height = 57
        Align = alTop
        Caption = ' Correos De Avisos '
        TabOrder = 0
        object WE_XSL_TARlbl: TLabel
          Left = 35
          Top = 24
          Width = 53
          Height = 13
          Alignment = taRightJustify
          Caption = 'Acti&vaci'#243'n:'
          FocusControl = WE_XSL_TAR
        end
        object WE_XSL_TAR: TZetaDBEdit
          Left = 91
          Top = 21
          Width = 379
          Height = 21
          TabOrder = 0
          Text = 'WE_XSL_TAR'
          DataField = 'WE_XSL_TAR'
          DataSource = DataSource
        end
      end
    end
    object Conexiones: TTabSheet
      Caption = 'Conexiones'
      ImageIndex = 4
      TabVisible = False
      object Label5: TLabel
        Left = 39
        Top = 7
        Width = 47
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cone&xi'#243'n:'
        FocusControl = WX_FOLIO
      end
      object WX_FOLIO: TZetaDBKeyLookup
        Left = 89
        Top = 3
        Width = 365
        Height = 21
        TabOrder = 0
        TabStop = True
        WidthLlave = 80
        DataField = 'WX_FOLIO'
        DataSource = DataSource
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
  object dsModelo: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 20
    Top = 65
  end
end
