inherited CatAccionesEdit: TCatAccionesEdit
  Caption = 'Acci'#243'n'
  PixelsPerInch = 96
  TextHeight = 13
  object WA_CODIGOlbl: TLabel [0]
    Left = 42
    Top = 50
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object WA_NOMBRElbl: TLabel [1]
    Left = 38
    Top = 72
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
    FocusControl = WA_NOMBRE
  end
  object WA_DESCRIPlbl: TLabel [2]
    Left = 19
    Top = 94
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object WA_CODIGO: TZetaDBTextBox [3]
    Left = 80
    Top = 48
    Width = 113
    Height = 17
    AutoSize = False
    Caption = 'WA_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'WA_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    BevelOuter = bvNone
  end
  object WA_NOMBRE: TDBEdit [7]
    Left = 80
    Top = 68
    Width = 364
    Height = 21
    DataField = 'WA_NOMBRE'
    DataSource = DataSource
    TabOrder = 3
  end
  object WA_DESCRIP: TDBMemo [8]
    Left = 80
    Top = 91
    Width = 363
    Height = 89
    DataField = 'WA_DESCRIP'
    DataSource = DataSource
    ScrollBars = ssVertical
    TabOrder = 4
  end
  inherited DataSource: TDataSource
    Left = 28
    Top = 113
  end
end
