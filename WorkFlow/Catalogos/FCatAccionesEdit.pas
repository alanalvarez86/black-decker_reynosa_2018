unit FCatAccionesEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask,
     ZBaseEdicion,
     ZetaDBTextBox, ZetaSmartLists;

type
  TCatAccionesEdit = class(TBaseEdicion)
    WA_CODIGOlbl: TLabel;
    WA_NOMBRE: TDBEdit;
    WA_NOMBRElbl: TLabel;
    WA_DESCRIPlbl: TLabel;
    WA_CODIGO: TZetaDBTextBox;
    WA_DESCRIP: TDBMemo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeImprimir(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure DoLookup; override;
  public
    { Public declarations }
  end;

var
  CatAccionesEdit: TCatAccionesEdit;

implementation

uses DWorkflow,
     ZHelpContext,
     ZetaCommonClasses,
     ZetaBuscador,
     ZAccesosTress;

{$R *.DFM}

procedure TCatAccionesEdit.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_ACCIONES_EDIC;
     IndexDerechos := D_CAT_ACCIONES;
     FirstControl := WA_NOMBRE;
end;

procedure TCatAccionesEdit.Connect;
begin
     Datasource.Dataset := dmWorkflow.cdsAcciones;
end;

procedure TCatAccionesEdit.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'Acci�n', 'Acci�n', 'WA_CODIGO', dmWorkflow.cdsAcciones );
end;

function TCatAccionesEdit.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Agregar Acciones';
end;

function TCatAccionesEdit.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Es Posible Borrar Acciones';
end;

function TCatAccionesEdit.PuedeModificar(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Alta debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_CAMBIO ); //K_DERECHO_ALTA );
     if not Result then
        sMensaje := 'No Est� Permitido Modificar Acciones';
end;

function TCatAccionesEdit.PuedeImprimir(var sMensaje: String): Boolean;
begin
     {
     Se verifica El Derecho De Baja debido a
     que en el �rbol de Derecho no Aparecen
     Los Derechos De Alta y De Baja
     }
     Result := CheckDerechos( K_DERECHO_IMPRESION ); //K_DERECHO_BAJA );
     if not Result then
        sMensaje := 'No Est� Permitido Imprimir Acciones';
end;

end.
