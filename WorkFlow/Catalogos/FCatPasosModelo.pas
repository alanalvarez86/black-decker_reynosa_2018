unit FCatPasosModelo;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids, Buttons, StdCtrls,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaKeyLookup,
     ZetaKeyCombo;

type
  TCatPasosModelo = class(TBaseConsulta)
    PanelDetalle: TPanel;
    ZetaDBGrid: TZetaDBGrid;
    PanelCodigo: TPanel;
    ModeloLBL: TLabel;
    Modelo: TZetaKeyCombo;
    PanelMover: TPanel;
    Subir: TSpeedButton;
    Bajar: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure ModeloClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
  private
    { Private declarations }
    procedure LlenarListaModelos;
  protected
    { Protected declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CatPasosModelo: TCatPasosModelo;

implementation

{$R *.DFM}

uses DWorkflow,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaCommonClasses;

{ *********** TSistUsuarios *********** }

procedure TCatPasosModelo.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_PASOSMODELOS;
     IndexDerechos := D_CAT_PASOS_MODELOS;
end;

procedure TCatPasosModelo.FormShow(Sender: TObject);
begin
     inherited;
     Subir.Enabled := CheckDerechos( K_DERECHO_CAMBIO );
     Bajar.Enabled := Subir.Enabled;
end;

procedure TCatPasosModelo.Connect;
begin
     LlenarListaModelos;
     with dmWorkflow do
     begin
          with cdsPasosModelo do
          begin
               SetDataChange;
               Conectar;
          end;
          DataSource.DataSet:= cdsPasosModelo;
     end;
end;

procedure TCatPasosModelo.ModeloClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmWorkflow.Modelo := Self.Modelo.Llave;
        dmWorkflow.cdsModelos.Locate('WM_CODIGO', Self.Modelo.Llave,[]);
        Refresh;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatPasosModelo.LlenarListaModelos;
begin
     with Self.Modelo do
     begin
          dmWorkflow.ModeloLlenarLista( Lista );
          ItemIndex := Lista.IndexOfName( dmWorkflow.Modelo );
     end;
 end;

procedure TCatPasosModelo.Refresh;
begin
     dmWorkflow.cdsPasosModelo.Refrescar;
end;

procedure TCatPasosModelo.Agregar;
begin
     dmWorkflow.cdsPasosModelo.Agregar;
end;

procedure TCatPasosModelo.Borrar;
begin
     dmWorkflow.cdsPasosModelo.Borrar;
end;

procedure TCatPasosModelo.Modificar;
begin
     dmWorkflow.cdsPasosModelo.Modificar;
end;

procedure TCatPasosModelo.SubirClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmWorkflow.DerechoSubir;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatPasosModelo.BajarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmWorkflow.DerechoBajar;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TCatPasosModelo.ZetaDBGridTitleClick(Column: TColumn);
begin
     inherited;
     with Column do
     begin
          if ( FieldName <> 'WE_DESCRIP' ) then
             dmWorkFlow.cdsPasosModelo.IndexFieldNames := FieldName;
     end;

end;

end.
