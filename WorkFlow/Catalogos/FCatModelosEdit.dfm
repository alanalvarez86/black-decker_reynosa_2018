inherited CatModelosEdit: TCatModelosEdit
  Left = 354
  Top = 210
  Caption = 'Modelo'
  ClientHeight = 454
  ClientWidth = 494
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 418
    Width = 494
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 326
    end
    inherited Cancelar: TBitBtn
      Left = 411
    end
  end
  inherited PanelSuperior: TPanel
    Width = 494
  end
  inherited PanelIdentifica: TPanel
    Width = 494
    inherited ValorActivo2: TPanel
      Width = 168
    end
  end
  object PanelMaster: TPanel [3]
    Left = 0
    Top = 51
    Width = 494
    Height = 172
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object WM_CODIGOlbl: TLabel
      Left = 50
      Top = 9
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = '&C'#243'digo:'
      FocusControl = WM_CODIGO
    end
    object WM_NOMBRElbl: TLabel
      Left = 46
      Top = 33
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = '&Nombre:'
      FocusControl = WM_NOMBRE
    end
    object WM_DESCRIPlbl: TLabel
      Left = 27
      Top = 54
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = '&Descripci'#243'n:'
    end
    object WM_STATUSlbl: TLabel
      Left = 53
      Top = 148
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = '&Status:'
    end
    object WM_STATUS: TZetaDBTextBox
      Left = 89
      Top = 147
      Width = 121
      Height = 17
      AutoSize = False
      Caption = 'WM_STATUS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'WM_STATUS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object WM_CODIGO: TZetaDBEdit
      Left = 89
      Top = 6
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'WM_CODIGO'
      ConfirmEdit = True
      DataField = 'WM_CODIGO'
      DataSource = DataSource
    end
    object WM_NOMBRE: TDBEdit
      Left = 89
      Top = 29
      Width = 364
      Height = 21
      DataField = 'WM_NOMBRE'
      DataSource = DataSource
      TabOrder = 1
    end
    object WM_DESCRIP: TDBMemo
      Left = 88
      Top = 52
      Width = 365
      Height = 89
      DataField = 'WM_DESCRIP'
      DataSource = DataSource
      ScrollBars = ssVertical
      TabOrder = 2
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 223
    Width = 494
    Height = 195
    ActivePage = Roles
    Align = alClient
    TabOrder = 4
    object Paginas: TTabSheet
      Caption = 'P'#225'ginas Web'
      ImageIndex = 1
      object WM_URLlbl: TLabel
        Left = 24
        Top = 11
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = '&Iniciar:'
      end
      object WM_URL_OKlbl: TLabel
        Left = 16
        Top = 33
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = '&Revisar:'
      end
      object WM_URL_EXAlbl: TLabel
        Left = 9
        Top = 57
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = '&Examinar:'
      end
      object WM_URL_DELlbl: TLabel
        Left = 15
        Top = 80
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dele&gar:'
      end
      object WM_URL_DEL: TDBEdit
        Left = 58
        Top = 76
        Width = 403
        Height = 21
        DataField = 'WM_URL_DEL'
        DataSource = DataSource
        TabOrder = 3
      end
      object WM_URL_EXA: TDBEdit
        Left = 58
        Top = 53
        Width = 403
        Height = 21
        DataField = 'WM_URL_EXA'
        DataSource = DataSource
        TabOrder = 2
      end
      object WM_URL_OK: TDBEdit
        Left = 58
        Top = 30
        Width = 403
        Height = 21
        DataField = 'WM_URL_OK'
        DataSource = DataSource
        TabOrder = 1
      end
      object WM_URL: TDBEdit
        Left = 58
        Top = 7
        Width = 403
        Height = 21
        DataField = 'WM_URL'
        DataSource = DataSource
        TabOrder = 0
      end
    end
    object Formatos: TTabSheet
      Caption = 'Formatos'
      ImageIndex = 2
      object ModelosGB: TGroupBox
        Left = 0
        Top = 0
        Width = 486
        Height = 167
        Align = alClient
        TabOrder = 0
        object WM_FORMATOlbl: TLabel
          Left = 12
          Top = 22
          Width = 75
          Height = 13
          Alignment = taRightJustify
          Caption = 'Archivo Fuente:'
        end
        object WM_FORMATO: TDBEdit
          Left = 90
          Top = 19
          Width = 378
          Height = 21
          DataField = 'WM_FORMATO'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
    object Roles: TTabSheet
      Caption = 'Roles'
      ImageIndex = 3
      object PanelRoles: TPanel
        Left = 446
        Top = 0
        Width = 40
        Height = 167
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          40
          167)
        object BorrarRol: TSpeedButton
          Left = 4
          Top = 39
          Width = 34
          Height = 31
          Hint = 'Remover Al Rol Del Modelo'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            33333333FF33333333FF333993333333300033377F3333333777333993333333
            300033F77FFF3333377739999993333333333777777F3333333F399999933333
            33003777777333333377333993333333330033377F3333333377333993333333
            3333333773333333333F333333333333330033333333F33333773333333C3333
            330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
            333333333337733333FF3333333C333330003333333733333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarRolClick
        end
        object AgregarRol: TSpeedButton
          Left = 3
          Top = 3
          Width = 34
          Height = 31
          Hint = 'Agregar Un Rol al Modelo'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarRolClick
        end
        object AgregarRolesTodos: TSpeedButton
          Left = 4
          Top = 100
          Width = 34
          Height = 31
          Hint = 'Asignar Todos Los Roles A Este Modelo'
          Anchors = [akLeft, akBottom]
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FFFFFFFFFFF
            FFFF33333333333FFFFF3FFFFFFFFF00000F333333333377777F33FFFFFFFF09
            990F33333333337F337F333FFFFFFF09990F33333333337F337F3333FFFFFF09
            990F33333333337FFF7F33333FFFFF00000F3333333333777773333333FFFFFF
            FFFF3FFFFF3333333F330000033FFFFF0FFF77777F3333337FF30EEE0333FFF0
            00FF7F337FFF333777FF0EEE00033F00000F7F33777F3777777F0EEE0E033000
            00007FFF7F7FF777777700000E00033000FF777773777F3777F3330EEE0E0330
            00FF337FFF7F7F3777F33300000E033000FF337777737F37773333330EEE0300
            03FF33337FFF77777333333300000333333F3333777773333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = AgregarRolesTodosClick
        end
        object BorrarRolesTodos: TSpeedButton
          Left = 4
          Top = 135
          Width = 34
          Height = 31
          Hint = 'Borrar Todos Los Roles Asignados A Este Modelo'
          Anchors = [akLeft, akBottom]
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
            555557777F777555F55500000000555055557777777755F75555005500055055
            555577F5777F57555555005550055555555577FF577F5FF55555500550050055
            5555577FF77577FF555555005050110555555577F757777FF555555505099910
            555555FF75777777FF555005550999910555577F5F77777775F5500505509990
            3055577F75F77777575F55005055090B030555775755777575755555555550B0
            B03055555F555757575755550555550B0B335555755555757555555555555550
            BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
            50BB555555555555575F555555555555550B5555555555555575}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BorrarRolesTodosClick
        end
      end
      object gridModelos: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 446
        Height = 167
        Align = alClient
        DataSource = dsRoles
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'RO_CODIGO'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RO_NOMBRE'
            Title.Caption = 'Nombre'
            Width = 302
            Visible = True
          end>
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 28
    Top = 113
  end
  object dsRoles: TDataSource
    OnDataChange = dsRolesDataChange
    Left = 24
    Top = 155
  end
end
