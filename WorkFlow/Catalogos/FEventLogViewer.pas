unit FEventLogViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, Grids, DBGrids, DBClient,Mask,
  ExtCtrls, DBCtrls, ZBaseConsulta, Buttons,
  ZetaCommonClasses;

type
  TEventLogViewer = class(TBaseConsulta)
    Panel1: TPanel;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    eNombreComputadora: TEdit;
    Label1: TLabel;
    bRevisar: TSpeedButton;
    cbTodos: TCheckBox;
    procedure bRevisarClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
  public
    { Public declarations }
    procedure Connect;override;
  end;

var
  EventLogViewer: TEventLogViewer;

implementation

uses
    DCliente,
    DWorkFlow;
{$R *.DFM}



{ TEventLogViewer}

procedure TEventLogViewer.Connect;
begin
     DataSource.Dataset := dmWorkFlow.cdsEventLog;
     eNombreComputadora.Text := dmCliente.ComputerName;
     bRevisar.Hint := 'Mostrar Eventos de Windows' + CR_LF +
                      '(Windows Application Event Log)' 
end;

procedure TEventLogViewer.bRevisarClick(Sender: TObject);
begin
     inherited;
     if FParametros= NIL then
        FParametros:= TZetaParams.Create;

     with FParametros do
     begin
          AddString('ComputerName', eNombreComputadora.Text );
          AddBoolean('Todos', cbTodos.Checked );
     end;

     dmWorkFlow.GetWindowsEventLog( FParametros );

end;

procedure TEventLogViewer.FormDestroy(Sender: TObject);
begin
     inherited;
     if ( FParametros <> NIL ) then
        FreeAndNil( FParametros ); 
end;

end.



