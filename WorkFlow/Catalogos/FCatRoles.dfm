inherited CatRoles: TCatRoles
  Left = 327
  Top = 273
  Caption = 'Roles'
  ClientHeight = 257
  ClientWidth = 849
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 849
    inherited Slider: TSplitter
      Left = 357
    end
    inherited ValorActivo1: TPanel
      Width = 341
      inherited textoValorActivo1: TLabel
        Width = 335
      end
    end
    inherited ValorActivo2: TPanel
      Left = 360
      Width = 489
      inherited textoValorActivo2: TLabel
        Width = 483
      end
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 849
    Height = 238
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'RO_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RO_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 214
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RO_DESCRIP'
        Title.Caption = 'Descripci'#243'n'
        Width = 341
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RO_NIVEL'
        Title.Caption = 'Nivel'
        Width = 94
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 280
    Top = 8
  end
end
