unit FGlobalEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ComCtrls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, FileCtrl,
     ZBaseGlobal,
     ZetaKeyLookup,
     ZetaNumero,
     {$IFDEF TRESS_DELPHIXE5_UP}
     DEmailService,
     {$ELSE}
     DEmailServiceD7,
     {$ENDIF}
     ZBaseDlgModal,
     DZetaServerProvider;

type
  TGlobalEdit = class(TBaseGlobal)
    PageControl: TPageControl;
    Generales: TTabSheet;
    GeneralesGB: TGroupBox;
    EmpresaLBL: TLabel;
    DireccionLBL: TLabel;
    CiudadLBL: TLabel;
    EstadoLBL: TLabel;
    Empresa: TEdit;
    Direccion: TEdit;
    Ciudad: TEdit;
    Estado: TEdit;
    ReporteadorGB: TGroupBox;
    Label3: TLabel;
    Buscar: TSpeedButton;
    Plantillas: TEdit;
    Correos: TTabSheet;
    InterfaseWEB: TTabSheet;
    PageControlCorreos: TPageControl;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    RootDirURL: TEdit;
    GroupBox2: TGroupBox;
    UsuarioTress: TZetaKeyLookup;
    UsuarioTressLBL: TLabel;
    eMailServer: TTabSheet;
    Estilos: TTabSheet;
    TimeOut: TZetaNumero;
    Puerto: TZetaNumero;
    Passsword: TEdit;
    Usuario: TEdit;
    Servidor: TEdit;
    ServidorLBL: TLabel;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    PuertoLBL: TLabel;
    TimeOutLBL: TLabel;
    XSLTareaGB: TGroupBox;
    XSLTareaLBL: TLabel;
    XSLTarea: TEdit;
    PanelEstilos: TPanel;
    XSLPathLBL: TLabel;
    XSLPath: TEdit;
    PasosProcesoGB: TGroupBox;
    XSLPasoProcesoActivarLBL: TLabel;
    XSLPasoProcesoTerminarLBL: TLabel;
    XSLPasoProcesoCancelarLBL: TLabel;
    XSLPasoProcesoActivar: TEdit;
    XSLPasoProcesoTerminar: TEdit;
    XSLPasoProcesoCancelar: TEdit;
    ProbarEMail: TBitBtn;
    XSLModificarPasos: TEdit;
    Label2: TLabel;
    XSLDespuesIniciar: TEdit;
    Label4: TLabel;
    XSLAntesLimite: TEdit;
    Label5: TLabel;
    XSLDespuesLimite: TEdit;
    Label6: TLabel;
    ConfirmacionGB: TGroupBox;
    ConfirmacionNombreLBL: TLabel;
    ConfirmacionEMailLBL: TLabel;
    ConfirmacionEMail: TEdit;
    ConfirmacionNombre: TEdit;
    RemitenteGB: TGroupBox;
    RemitenteLBL: TLabel;
    RemitenteDireccionLBL: TLabel;
    RemitenteDireccion: TEdit;
    Remitente: TEdit;
    MotorGB: TGroupBox;
    AdministradorLBL: TLabel;
    AdministradorEMailLBL: TLabel;
    AdministradorEMail: TEdit;
    Administrador: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
    procedure ProbarEMailClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FEmailMgr: TdmEmailService;
  public
    { Public declarations }
    procedure Descargar; override;
    property EmailMgr: TdmEmailService read FEmailMgr write FEmailMgr;
  end;

var
  GlobalEdit: TGlobalEdit;

implementation

uses DCliente,
     DSistema,
     ZetaCommonTools,
     ZHelpContext,
     ZAccesosTress,
     ZGlobalTress,
     ZetaCommonLists,
     FEMailSendTest,
     ZetaDialogo,
     ZetaCommonClasses;
const
     K_WORKFLOW_DIRECTORIO_RAIZ_DEFAULT = 'http://NombreServidor//DirectorioVirtual/';

{$R *.DFM}

procedure TGlobalEdit.FormCreate(Sender: TObject);
begin
     //Proyecto.Tag := K_GLOBAL_NOMBRE;
     Empresa.Tag := K_GLOBAL_EMPRESA;
     Direccion.Tag := K_GLOBAL_DIRECCION;
     Ciudad.Tag := K_GLOBAL_CIUDAD;
     Estado.Tag := K_GLOBAL_ESTADO;
     Plantillas.Tag := K_GLOBAL_DIR_PLANT;
     UsuarioTress.Tag := K_WORKFLOW_USER;
     RootDirURL.Tag := K_WORKFLOW_DIRECTORIO_RAIZ;
     //ArchivosAnexosDirectory.Tag := K_WORKFLOW_FILE_PATH;
     //ArchivosAnexosURL.Tag := K_WORKFLOW_FILE_URL;
     Administrador.Tag := K_WORKFLOW_MANAGER_NAME;
     AdministradorEMail.Tag := K_WORKFLOW_MANAGER_EMAIL;
     Remitente.Tag := K_WORKFLOW_SENDER_NAME;
     RemitenteDireccion.Tag := K_WORKFLOW_SENDER_EMAIL;
     ConfirmacionNombre.Tag := K_WORKFLOW_RECEIPT_NAME;
     ConfirmacionEMail.Tag := K_WORKFLOW_RECEIPT_EMAIL;
     Servidor.Tag := K_WORKFLOW_EMAIL_SERVER;
     Usuario.Tag := K_WORKFLOW_EMAIL_USER;
     Passsword.Tag := K_WORKFLOW_EMAIL_PASSWORD;
     Puerto.Tag := K_WORKFLOW_EMAIL_PORT;
     TimeOut.Tag := K_WORKFLOW_EMAIL_TIMEOUT;
     XSLPath.Tag := K_WORKFLOW_XSL_PATH;
     XSLTarea.Tag := K_WORKFLOW_XSL_TAREA;
     //XSLProcesoActivar.Tag := K_WORKFLOW_XSL_PROCESO_ACTIVAR;
     //XSLProcesoTerminar.Tag := K_WORKFLOW_XSL_PROCESO_TERMINAR;
     //XSLProcesoCancelar.Tag := K_WORKFLOW_XSL_PROCESO_CANCELAR;
     XSLPasoProcesoActivar.Tag := K_WORKFLOW_XSL_PASO_ACTIVAR;
     XSLPasoProcesoTerminar.Tag := K_WORKFLOW_XSL_PASO_TERMINAR;
     XSLPasoProcesoCancelar.Tag := K_WORKFLOW_XSL_PASO_CANCELAR;
     XSLModificarPasos.Tag := K_WORKFLOW_XSL_PASO_MODIFICAR;
     XSLDespuesIniciar.Tag := K_WORKFLOW_XSL_PASO_DESPUES_INICIAR;
     XSLAntesLimite.Tag := K_WORKFLOW_XSL_PASO_ANTES_LIMITE;
     XSLDespuesLimite.Tag := K_WORKFLOW_XSL_PASO_DESPUES_LIMITE;
     inherited;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_CONFIGURACION_GLOBAL;
     IndexDerechos := D_CAT_CONFI_GLOBALES;
     PageControl.ActivePage := Generales;
     //PageControlCorreos.ActivePage := Direcciones;
     UsuarioTress.LookupDataset := dmSistema.cdsUsuariosLookup;

     FEmailMgr := TdmEmailService.Create( nil );
end;

procedure TGlobalEdit.FormShow(Sender: TObject);
begin
     with dmSistema do
     begin
          ConectarUsuariosLookup;
     end;
     inherited;
     if ZetaCommonTools.StrVacio(RootDirURL.Text) then
        RootDirURL.Text := K_WORKFLOW_DIRECTORIO_RAIZ_DEFAULT;
     Empresa.SetFocus;
     ProbarEMail.Visible := Assigned( EMailMgr );
end;

procedure TGlobalEdit.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FEmailMgr );
end;

procedure TGlobalEdit.Descargar;

procedure TrimEdit( Control: TEdit );
begin
     with Control do
     begin
          Text := Trim( Text );
     end;
end;

begin
     //TrimEdit( Proyecto );
     TrimEdit( Empresa );
     TrimEdit( Direccion );
     TrimEdit( Ciudad );
     TrimEdit( Estado );
     TrimEdit( Plantillas );
     inherited Descargar;
end;

procedure TGlobalEdit.BuscarClick(Sender: TObject);
begin
     inherited;
     Plantillas.Text := BuscarDirectorio( Plantillas.Text );
end;

procedure TGlobalEdit.ProbarEMailClick(Sender: TObject);
begin
     inherited;
     with EMailMgr do
     begin
          NewEMail;
          MailServer := Servidor.Text;
          Port := Puerto.ValorEntero;
          User := Usuario.Text;
          PassWord := Passsword.Text;
          FromName := Administrador.Text;
          FromAddress := AdministradorEMail.Text;
          TimeOut := Self.TimeOut.ValorEntero;
          SubType := emtTexto;
     end;
     FEMailSendTest.ProbarEnvioCorreos( EMailMgr );
end;



end.
