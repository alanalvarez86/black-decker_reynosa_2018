unit FSistGlobales;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, ComCtrls, ToolWin, StdCtrls, Buttons, ImgList,
     ZBaseGlobal,
     ZBaseConsulta,
     ZetaDBTextBox;

type
  TSistGlobales = class(TBaseConsulta)
    PanelTitulos: TPanel;
    Empresa: TZetaDBTextBox;
    Direccion: TZetaDBTextBox;
    EmpresaLBL: TLabel;
    DireccionLBL: TLabel;
    CiudadLBL: TLabel;
    EstadoLBL: TLabel;
    Ciudad: TZetaDBTextBox;
    Estado: TZetaDBTextBox;
    Label1: TLabel;
    ProyectoLBL: TLabel;
    Proyecto: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure PanelTitulosDblClick(Sender: TObject);
  private
    { Private declarations }
    function AbrirGlobal(GlobalClass: TBaseGlobalClass): Integer;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  SistGlobales: TSistGlobales;

implementation

uses DGlobal,
     DCliente,
     ZGlobalTress,
     ZHelpContext,
     ZAccesosTress,
     ZetaCommonClasses,
     FGlobalEdit,
     FTressShell;

{$R *.DFM}

{ ************* TSistGlobales *************** }

procedure TSistGlobales.FormCreate(Sender: TObject);
begin
     inherited;
     EsConsulta := False;
     HelpContext := H_WORKFLOWCFG_CATALOGOS_CONFIGURACION;
     IndexDerechos := D_CAT_CONFI_GLOBALES;
end;

procedure TSistGlobales.Connect;
begin
     with Global do
     begin
          Proyecto.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_NOMBRE );
          Empresa.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_EMPRESA );
          Direccion.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_DIRECCION );
          Ciudad.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_CIUDAD );
          Estado.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_ESTADO );
     end;
end;

procedure TSistGlobales.Refresh;
begin
     with Global do
     begin
          Proyecto.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_NOMBRE );
          Empresa.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_EMPRESA );
          Direccion.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_DIRECCION );
          Ciudad.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_CIUDAD );
          Estado.Caption := GetGlobalString( ZGlobalTress.K_GLOBAL_ESTADO );
     end;
end;

function TSistGlobales.AbrirGlobal( GlobalClass: TBaseGlobalClass ): Integer;
var
   Forma: TBaseGlobal;
begin
     Forma := GlobalClass.Create( Application );
     with Forma do
     begin
          try
             ShowModal;
             Result := LastAction;
          finally
                 Free;
          end;
     end;
end;

procedure TSistGlobales.PanelTitulosDblClick(Sender: TObject);
begin
     inherited;
     AbrirGlobal( TGlobalEdit );
end;

end.

