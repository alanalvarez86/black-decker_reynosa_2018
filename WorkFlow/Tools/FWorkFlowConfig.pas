unit FWorkFlowConfig;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, Mask, ComCtrls, ShellApi, FileCtrl,
     DZetaServerProvider,
     {$ifdef EMAILTEST}
     DEmailService,
     {$else}
     DWorkFlow,
     {$endif}
     DWorkFlowTypes,
     ZBaseDlgModal,
     ZetaNumero;

type
  TWorkFlowCFGValues = class(TZetaDlgModal)
    PageControl: TPageControl;
    Direcciones: TTabSheet;
    EMailServer: TTabSheet;
    ServidorLBL: TLabel;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    Servidor: TEdit;
    Usuario: TEdit;
    Passsword: TEdit;
    PuertoLBL: TLabel;
    Puerto: TZetaNumero;
    TimeOutLBL: TLabel;
    TimeOut: TZetaNumero;
    MotorGB: TGroupBox;
    AdministradorLBL: TLabel;
    AdministradorEMailLBL: TLabel;
    AdministradorEMail: TEdit;
    Administrador: TEdit;
    RemitenteGB: TGroupBox;
    RemitenteLBL: TLabel;
    RemitenteDireccionLBL: TLabel;
    RemitenteDireccion: TEdit;
    Remitente: TEdit;
    ConfirmacionGB: TGroupBox;
    ConfirmacionEMailLBL: TLabel;
    ConfirmacionEMail: TEdit;
    ConfirmacionNombre: TEdit;
    ConfirmacionNombreLBL: TLabel;
    XSL: TTabSheet;
    XSLTareaGB: TGroupBox;
    XSLTareaLBL: TLabel;
    XSLTarea: TEdit;
    XSLTareaSB: TSpeedButton;
    XSLProcesosGB: TGroupBox;
    XSLProcesoActivarLBL: TLabel;
    XSLProcesoActivarSB: TSpeedButton;
    XSLProcesoActivar: TEdit;
    XSLProcesoTerminarLBL: TLabel;
    XSLProcesoTerminar: TEdit;
    XSLProcesoTerminarSB: TSpeedButton;
    XSLProcesoCancelarLBL: TLabel;
    XSLProcesoCancelar: TEdit;
    XSLProcesoCancelarSB: TSpeedButton;
    XSLPasosProcesoGB: TGroupBox;
    XSLPasoProcesoActivarLBL: TLabel;
    XSLPasoProcesoActivarSB: TSpeedButton;
    XSLPasoProcesoTerminarLBL: TLabel;
    XSLPasoProcesoTerminarSB: TSpeedButton;
    XSLPasoProcesoCancelarLBL: TLabel;
    XSLPasoProcesoActivar: TEdit;
    XSLPasoProcesoTerminar: TEdit;
    XSLPasoProcesoCancelar: TEdit;
    OpenXSLDialog: TOpenDialog;
    XSLTareaTestSB: TSpeedButton;
    XSLProcesoActivarTest: TSpeedButton;
    XSLProcesoTerminarTest: TSpeedButton;
    XSLProcesoCancelarTest: TSpeedButton;
    XSLPasoProcesoActivarTest: TSpeedButton;
    XSLPasoProcesoTerminarTest: TSpeedButton;
    XSLPasoProcesoCancelarTest: TSpeedButton;
    ProbarEMail: TBitBtn;
    PanelDefaults: TPanel;
    XSLPathLBL: TLabel;
    XSLPath: TEdit;
    XSLPasoProcesoCancelarSB: TSpeedButton;
    XSLPathSeek: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure XSLTareaSBClick(Sender: TObject);
    procedure XSLProcesoActivarSBClick(Sender: TObject);
    procedure XSLProcesoTerminarSBClick(Sender: TObject);
    procedure XSLProcesoCancelarSBClick(Sender: TObject);
    procedure XSLPasoProcesoActivarSBClick(Sender: TObject);
    procedure XSLPasoProcesoTerminarSBClick(Sender: TObject);
    procedure XSLPasoProcesoCancelarSBClick(Sender: TObject);
    procedure XSLTareaTestSBClick(Sender: TObject);
    procedure XSLProcesoActivarTestClick(Sender: TObject);
    procedure XSLProcesoTerminarTestClick(Sender: TObject);
    procedure XSLProcesoCancelarTestClick(Sender: TObject);
    procedure XSLPasoProcesoActivarTestClick(Sender: TObject);
    procedure XSLPasoProcesoTerminarTestClick(Sender: TObject);
    procedure XSLPasoProcesoCancelarTestClick(Sender: TObject);
    procedure ProbarEMailClick(Sender: TObject);
    procedure XSLPathSeekClick(Sender: TObject);
  private
    { Private declarations }
    FZetaProvider: TdmZetaWorkFlowProvider;
    FRegistry: TWFRegistry;
    {$ifdef EMAILTEST}
    FEmailMgr: TdmEmailService;
    {$else}
    FWorkFlowMgr: TWorkFlowManager;
    {$endif}
    function GetRegistry: TWFRegistry;
    procedure BuscarArchivoXSL( Edit: TEdit );
    procedure Load;
    procedure Unload;
    procedure ProbarArchivoXSL(const sFileName: String);
  protected
    { Protected declarations }
    property Registry: TWFRegistry read GetRegistry;
  public
    { Public declarations }
    property oZetaProvider: TdmZetaWorkFlowProvider read FZetaProvider write FZetaProvider;
    {$ifdef EMAILTEST}
    property EmailMgr: TdmEmailService read FEmailMgr write FEmailMgr;
    {$else}
    property WorkFlowMgr: TWorkFlowManager read FWorkFlowMgr write FWorkFlowMgr;
    {$endif}
  end;

{$ifdef EMAILTEST}
function UpdateWorkFlowRegistry( Manager: TdmEMailService; Provider: TdmZetaWorkFlowProvider ): Boolean;
{$else}
function UpdateWorkFlowRegistry( Manager: TWorkFlowManager ): Boolean;
{$endif}

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     {$ifdef EMAILTEST}
     FEMailSendTest,
     {$endif}
     ZetaDialogo;

{$R *.dfm}

{$ifdef EMAILTEST}
function UpdateWorkFlowRegistry( Manager: TdmEMailService; Provider: TdmZetaWorkFlowProvider ): Boolean;
{$else}
function UpdateWorkFlowRegistry( Manager: TWorkFlowManager ): Boolean;
{$endif}
var
   WorkFlowCFGValues: TWorkFlowCFGValues;
begin
     Result := False;
     try
        WorkFlowCFGValues := TWorkFlowCFGValues.Create( Application );
        try
           with WorkFlowCFGValues do
           begin
                {$ifdef EMAILTEST}
                oZetaProvider := Provider;
                EMailMgr := Manager;
                {$else}
                oZetaProvider := Manager.oZetaProvider;
                WorkFlowMgr := Manager;
                {$endif}
                try
                   with oZetaProvider do
                   begin
                        Start;
                        ShowModal;
                        Stop;
                   end;
                   Result := ( ModalResult = mrOk );
                except
                      on Error: Exception do
                      begin
                           Application.HandleException( Error );
                      end;
                end;
           end;
        finally
               FreeAndNil( WorkFlowCFGValues );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

{ ********* TWorkFlowCFGValues ********* }

procedure TWorkFlowCFGValues.FormCreate(Sender: TObject);
begin
     inherited;
     FRegistry := TWFRegistry.Create( True ); { Read-Write }
end;

procedure TWorkFlowCFGValues.FormShow(Sender: TObject);
begin
     Assert( Assigned( oZetaProvider ), 'oZetaProvider Sin Asignar' );
     Registry.oZetaProvider := oZetaProvider;
     inherited;
     Load;
     PageControl.ActivePage := Direcciones;
     ActiveControl := Administrador;
     XSL.TabVisible := {$ifdef EMAILTEST}False{$else}Assigned( WorkFlowMgr ){$endif};
     ProbarEMail.Visible := {$ifdef EMAILTEST}Assigned( EMailMgr ){$else}False{$endif};
end;

procedure TWorkFlowCFGValues.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
     inherited;
end;

function TWorkFlowCFGValues.GetRegistry: TWFRegistry;
begin
     Result := FRegistry;
end;

procedure TWorkFlowCFGValues.BuscarArchivoXSL(Edit: TEdit);
begin
     with Edit do
     begin
          with OpenXSLDialog do
          begin
               InitialDir := ExtractFilePath( Text );
               FileName := ExtractFileName( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TWorkFlowCFGValues.ProbarArchivoXSL( const sFileName: String );
{$ifndef EMAILTEST}
var
   sOutput, sTempHTML, sTempXML: String;
   FOutput: TStrings;
   oCursor: TCursor;
{$endif}
begin
     inherited;
     {$ifdef EMAILTEST}
     Tag := 0;
     {$else}
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        sTempHTML := Format( '%sTESTXSL.HTM', [ ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ) ] );
        sTempXML := Format( '%sTESTXSL.XML', [ ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ) ] );
        if WorkFlowMgr.ProbarXSL( sFileName, sTempXML, sOutput ) then
        begin
             FOutput := TStringList.Create;
             try
                FOutput.Text := sOutput;
                FOutput.SaveToFile( sTempHTML );
             finally
                    FreeAndNil( FOutput );
             end;
             if SysUtils.FileExists( sTempHTML ) then
             begin
                  ExecuteFile( sTempHTML, '', ExtractFilePath( sTempHTML ) );
             end
             else
             begin
                  ZetaDialogo.ZError( '� Error En Archivo !', Format( 'El Archivo %s No Fu� Creado', [ sTempHTML ] ), 0 );
             end;
        end
        else
        begin
             ZetaDialogo.ZError( '� Error Al Transformar ! ', Format( 'Se Produjo Un Error: %s', [ sOutput ] ), 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     {$endif}
end;

procedure TWorkFlowCFGValues.ProbarEMailClick(Sender: TObject);
begin
     inherited;
     {$ifdef EMAILTEST}
     with EMailMgr do
     begin
          NewEMail;
          MailServer := Servidor.Text;
          Port := Puerto.ValorEntero;
          User := Usuario.Text;
          PassWord := Passsword.Text;
          FromName := Administrador.Text;
          FromAddress := AdministradorEMail.Text;
          TimeOut := Self.TimeOut.ValorEntero;
          SubType := emtTexto;
     end;
     FEMailSendTest.ProbarEnvioCorreos( EMailMgr );
     {$else}
     Tag := 0;
     {$endif}
end;

procedure TWorkFlowCFGValues.Load;
begin
     with Registry do
     begin
          Servidor.Text := EMailServer;
          Usuario.Text := EMailUser;
          Passsword.Text := EMailPassword;
          Puerto.Valor := EMailPort;
          TimeOut.Valor := EMailTimeOut;
          AdministradorEMail.Text := WorkFlowEMailAddress;
          Administrador.Text := WorkFlowEMailName;
          RemitenteDireccion.Text := EMailReplyToAddress;
          Remitente.Text := EMailReplyToName;
          ConfirmacionEMail.Text := EMailReceiptRecipientAddress;
          ConfirmacionNombre.Text := EMailReceiptRecipientName;
          XSLPath.Text := DirectorioXSL;
          XSLTarea.Text := TareaXSL;
          XSLProcesoActivar.Text := ModeloXSLActivo;
          XSLProcesoTerminar.Text := ModeloXSLTerminado;
          XSLProcesoCancelar.Text := ModeloXSLCancelado;
          XSLPasoProcesoActivar.Text := PasoModeloXSLActivo;
          XSLPasoProcesoTerminar.Text := PasoModeloXSLTerminado;
          XSLPasoProcesoCancelar.Text := PasoModeloXSLCancelado;
     end;
end;

procedure TWorkFlowCFGValues.Unload;
begin
     with Registry do
     begin
          EMailServer := Servidor.Text;
          EMailUser := Usuario.Text;
          EMailPassword := Passsword.Text;
          EMailPort := Puerto.ValorEntero;
          EMailTimeOut := TimeOut.ValorEntero;
          WorkFlowEMailAddress := AdministradorEMail.Text;
          WorkFlowEMailName := Administrador.Text;
          EMailReplyToAddress := RemitenteDireccion.Text;
          EMailReplyToName := Remitente.Text;
          EMailReceiptRecipientAddress := ConfirmacionEMail.Text;
          EMailReceiptRecipientName := ConfirmacionNombre.Text;
          DirectorioXSL := XSLPath.Text;
          TareaXSL := XSLTarea.Text;
          ModeloXSLActivo := XSLProcesoActivar.Text;
          ModeloXSLTerminado := XSLProcesoTerminar.Text;
          ModeloXSLCancelado := XSLProcesoCancelar.Text;
          PasoModeloXSLActivo := XSLPasoProcesoActivar.Text;
          PasoModeloXSLTerminado := XSLPasoProcesoTerminar.Text;
          PasoModeloXSLCancelado := XSLPasoProcesoCancelar.Text;
     end;
end;

procedure TWorkFlowCFGValues.XSLPathSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     inherited;
     sDirectory := XSLPath.Text;
     if FileCtrl.SelectDirectory( sDirectory, [], 0 ) then
     begin
          XSLPath.Text := sDirectory;
     end;
end;

procedure TWorkFlowCFGValues.XSLTareaSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLTarea );
end;

procedure TWorkFlowCFGValues.XSLProcesoActivarSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLProcesoActivar );
end;

procedure TWorkFlowCFGValues.XSLProcesoTerminarSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLProcesoTerminar );
end;

procedure TWorkFlowCFGValues.XSLProcesoCancelarSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLProcesoCancelar );
end;

procedure TWorkFlowCFGValues.XSLPasoProcesoActivarSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLPasoProcesoActivar );
end;

procedure TWorkFlowCFGValues.XSLPasoProcesoTerminarSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLPasoProcesoTerminar );
end;

procedure TWorkFlowCFGValues.XSLPasoProcesoCancelarSBClick(Sender: TObject);
begin
     inherited;
     BuscarArchivoXSL( XSLPasoProcesoCancelar );
end;

procedure TWorkFlowCFGValues.XSLTareaTestSBClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLTarea.Text );
end;

procedure TWorkFlowCFGValues.XSLProcesoActivarTestClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLProcesoActivar.Text );
end;

procedure TWorkFlowCFGValues.XSLProcesoTerminarTestClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLProcesoTerminar.Text );
end;

procedure TWorkFlowCFGValues.XSLProcesoCancelarTestClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLProcesoCancelar.Text );
end;

procedure TWorkFlowCFGValues.XSLPasoProcesoActivarTestClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLPasoProcesoActivar.Text );
end;

procedure TWorkFlowCFGValues.XSLPasoProcesoTerminarTestClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLPasoProcesoTerminar.Text );
end;

procedure TWorkFlowCFGValues.XSLPasoProcesoCancelarTestClick(Sender: TObject);
begin
     inherited;
     Self.ProbarArchivoXSL( XSLPasoProcesoCancelar.Text );
end;

procedure TWorkFlowCFGValues.OKClick(Sender: TObject);
begin
     try
        Unload;
        ModalResult := mrOk;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
     inherited;
end;

end.
