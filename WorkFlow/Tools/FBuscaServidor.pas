unit FBuscaServidor;

interface

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Buttons, StdCtrls, ExtCtrls,
     {$ifdef HTTP_CONNECTION}
     ZetaRegistryCliente,
     {$endif}
     ZBaseDlgModal;

type
  TFindServidor = class(TZetaDlgModal)
    DCOMgb: TGroupBox;
    RemoteServerLBL: TLabel;
    RemoteServer: TEdit;
    ServidorSeek: TSpeedButton;
    UsarDCOM: TRadioButton;
    PanelRelleno: TPanel;
    HTTPgb: TGroupBox;
    PanelEnMedio: TPanel;
    UsarHTTP: TRadioButton;
    SitioWebLBL: TLabel;
    UserNameLBL: TLabel;
    PasswordLBL: TLabel;
    SitioWeb: TEdit;
    UserName: TEdit;
    Password: TEdit;
    procedure ServidorSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure UsarDCOMClick(Sender: TObject);
    procedure UsarHTTPClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    function GetServidor: String;
    {$ifdef HTTP_CONNECTION}
    function GetWebPassword: String;
    function GetConnectionType: TTipoConexion;
    function GetWebPage: String;
    function GetWebUser: String;
    procedure SetWebPassword(const Value: String);
    procedure SetConnectionType(const Value: TTipoConexion);
    procedure SetWebPage(const Value: String);
    procedure SetWebUser(const Value: String);
    procedure SetControls;
    {$endif}
    procedure SetServidor(const Value: String);
  public
    { Public declarations }
    property Servidor: String read GetServidor write SetServidor;
    {$ifdef HTTP_CONNECTION}
    property ConnectionType: TTipoConexion read GetConnectionType write SetConnectionType;
    property WebPage: String read GetWebPage write SetWebPage;
    property WebUser: String read GetWebUser write SetWebUser;
    property WebPassword: String read GetWebPassword write SetWebPassword;
    {$endif}
  end;

var
  FindServidor: TFindServidor;

implementation

uses ZetaNetworkBrowser,
     ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

{ TFindServidor }

procedure TFindServidor.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80084_Especificar_Servidor;
end;

procedure TFindServidor.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef HTTP_CONNECTION}
     SetControls;
     {$endif}
end;

function TFindServidor.GetServidor: String;
begin
     Result := RemoteServer.Text;
end;

procedure TFindServidor.SetServidor(const Value: String);
begin
     RemoteServer.Text := Value;
end;

{$ifdef HTTP_CONNECTION}
function TFindServidor.GetWebPassword: String;
begin
     Result := Self.Password.Text;
end;

function TFindServidor.GetConnectionType: TTipoConexion;
begin
     if Self.UsarHTTP.Checked then
        Result := conxHTTP
     else
         Result := conxDCOM;
end;

function TFindServidor.GetWebPage: String;
begin
     Result := Self.SitioWeb.Text;
end;

function TFindServidor.GetWebUser: String;
begin
     Result := Self.UserName.Text;
end;

procedure TFindServidor.SetWebPassword(const Value: String);
begin
     Self.Password.Text := Value;
end;

procedure TFindServidor.SetConnectionType(const Value: TTipoConexion);
begin
     case Value of
          conxHTTP:
          begin
               Self.UsarDCOM.Checked := False;
               Self.UsarHTTP.Checked := True;
          end
          else
          begin
               Self.UsarDCOM.Checked := True;
               Self.UsarHTTP.Checked := False;
          end;
     end;
end;

procedure TFindServidor.SetWebPage(const Value: String);
begin
     Self.SitioWeb.Text := Value;
end;

procedure TFindServidor.SetWebUser(const Value: String);
begin
     Self.UserName.Text := Value;
end;

procedure TFindServidor.SetControls;
begin
     RemoteServerLBL.Enabled := UsarDCOM.Checked;
     RemoteServer.Enabled := UsarDCOM.Checked;
     ServidorSeek.Enabled := UsarDCOM.Checked;
     SitioWebLBL.Enabled := UsarHTTP.Checked;
     SitioWeb.Enabled := UsarHTTP.Checked;
     UserNameLBL.Enabled := UsarHTTP.Checked;
     UserName.Enabled := UsarHTTP.Checked;
     PasswordLBL.Enabled := UsarHTTP.Checked;
     Password.Enabled := UsarHTTP.Checked;
     if UsarDCOM.Checked then
        ActiveControl := RemoteServer;
     if UsarHTTp.Checked then
        ActiveControl := SitioWeb;
end;
{$endif}

procedure TFindServidor.ServidorSeekClick(Sender: TObject);
begin
     inherited;
     Servidor := ZetaNetworkBrowser.GetServer( RemoteServer.Text );
end;


procedure TFindServidor.UsarDCOMClick(Sender: TObject);
begin
     inherited;
     {$ifdef HTTP_CONNECTION}
     Self.UsarHTTP.Checked := not Self.UsarDCOM.Checked;
     SetControls;
     {$endif}
end;

procedure TFindServidor.UsarHTTPClick(Sender: TObject);
begin
     inherited;
     {$ifdef HTTP_CONNECTION}
     Self.UsarDCOM.Checked := not Self.UsarHTTP.Checked;
     SetControls;
     {$endif}
end;


procedure TFindServidor.OKClick(Sender: TObject);
{$ifdef HTTP_CONNECTION}
const
     K_WININET = 'WinInet.dll';
var
   sArchivo: string;
{$endif}
begin

     inherited;
     {$ifdef HTTP_CONNECTION}
     if ( ConnectionType = conxHTTP ) then
     begin
          sArchivo := VerificaDir( ZetaWinAPITools.GetWinSysDir ) + K_WININET;
          if FileExists( sArchivo ) then
             ModalResult := mrOk
          else
             ZError(Caption, Format( 'No se encontr� instalado el archivo  "%s". ' + CR_LF +
                                     'Consulte a su Administrador', [sArchivo] ), 0);
     end
     else
     {$endif}
         ModalResult := mrOk;
end;

end.
