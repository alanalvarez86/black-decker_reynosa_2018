@echo off
cls

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Configuraciones
set usuario=Administrator
set contrasena=Administrator
set Servidor=DEV-SERVER
set puerto=49201
set ruta=Proyectos\Version 2011\Entregables
@echo off
cls

:: Para pruebas en cuanto a checkout puede tomarse esta linea:
:: call bco -p "Administrator:Administrator@DEV-SERVER:49201/Tress Windows 2.2/Proyectos/Version/Proyectos Integrados/Version 2011/Entregables/Tress20" -is -vb -o -ts "*"

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Configuraciones
::Configuraciones del cliente
set branch=Version 2011
set ruta=Proyectos\Version 2011\Entregables
set folder=Proyectos/Version 2011/Entregables


::Configuraciones adicionales
set vista=Tress Windows 2.2

set folderBatch=TestComplete/Batch
set folderdestino01=D:\3win_20\TestComplete\Programas\Clientes\
set folderdestino02=D:\3win_20\TestComplete\
set raizborrar=C:\%ruta%\
set folderinverso=%ruta%\Version
set folderDLLS=%folder%/Version/Dlls
set folderEXES=%folder%/Version/Ejecutables
set folderScript=%folder%/Scripts
set archivos=*

::Configuraciones de software utilizado
set iexplore=C:\Program Files\Internet Explorer\IEXPLORE.EXE
set componentservices=C:\windows\system32\Com\comexp.msc
set testcomplete=C:\Progra~1\Automa~1\TestCo~1\TestCo~1.exe

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Ejecucion
echo. Vista: %vista%  Cliente: %branch% 
pause
cls

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Revision de Fecha y Hora
::Si quiere verificar la fecha y hora de la maquina
echo.Verifique que la fecha y hora es la correcta para activacion de sentinel
::Fecha
date 
::Hora
time
cls

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Revision de Sentinel
:: Si quiere confirmar el Serial del Sentinela
echo.Es necesario que verifique que est�n correctamente las claves de Sentinel!
START /WAIT notepad "%folderdestino02%Tools\CommonClasses.sd"

del "%raizborrar%" /S /Q
cls

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Checkout de la version 2011
call bco -p "Administrator:Administrator@DEV-SERVER:49201/Tress Windows 2.2/Version 2011/Proyectos/Version/Proyectos Integrados/Version 2011/Entregables/Tress20" -is -vb -o -ts -exclude "/Documentacion" -exclude "/Documentos" -exclude "/Net" -exclude "PlantillasWF" "*"

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Checkout Cliente

::Copia Archivos a carpeta de ejecucion
xcopy "C:\Proyectos\Version\Proyectos Integrados\Version 2011\Entregables\Tress20" "D:\Programas de Tress\Tress20\" /s /f /y
copy "C:\Proyectos\Version\Proyectos Integrados\Version 2011\Entregables\Tress20\TestComplete\Clientes\*.*" "D:\3win_20\TestComplete\Programas\Clientes" /Y
copy "C:\Proyectos\Version\Proyectos Integrados\Version 2011\Entregables\Tress20\TestComplete\Profesional\*.*" "D:\3win_20\TestComplete\Programas\DosCapas" /Y

::echo Copiando el TestPowerQuery de la vista respectiva.
:: NO QUITAR, de lo contrario dejar el test power query vacio.
copy "D:\3win_20\TestComplete\Batch\TestPowerQuery.bat" "d:\3win_20\TestComplete\Programas\Batch\" /y 
cls
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Validacion de DLL's
::Si quiere validar los dll que se usaran
echo.Es necesario que revise que los DLL correponden a la programacion que se probara por Test Complete
START explorer "D:\Programas de Tress\Tress20\Servidor\SQLServer\"
%componentservices%
cls
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Eliminacion de Resultados Anteriores
:: Para borrar los resultados de ejecuciones anteriores es necesario activar las siguientes lineas
rd "D:\3win_20\TestComplete\Asistencia\Results\" /S /Q
rd "D:\3win_20\TestComplete\Cafeteria\Results\" /S /Q
rd "D:\3win_20\TestComplete\Imss\Results\" /S /Q
rd "D:\3win_20\TestComplete\L5Poll\Results\" /S /Q
rd "D:\3win_20\TestComplete\Nomina_I\Results\" /S /Q
rd "D:\3win_20\TestComplete\Nomina_II\Results\" /S /Q
rd "D:\3win_20\TestComplete\Rec_Hum_I\Results\" /S /Q
rd "D:\3win_20\TestComplete\Rec_Hum_II\Results\" /S /Q
rd "D:\3win_20\TestComplete\TressEmail\Results\" /S /Q
rd "D:\3win_20\TestComplete\Reportes_I\Results\" /S /Q
::se agrega cls para poder omitir mensajes de directorios no encontrados
cls

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Ejecucion de Casos automaticos
::Se puede comentar los casos no necesarios

Title Asistencia
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Asistencia\Asistencia.mds
xcopy "D:\3win_20\TestComplete\Asistencia\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Cafeteria
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Cafeteria\Cafeteria.mds
xcopy "D:\3win_20\TestComplete\Cafeteria\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Imss
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Imss\Imss.mds
xcopy "D:\3win_20\TestComplete\Imss\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title L5Poll
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\L5Poll\L5Poll.mds
xcopy "D:\3win_20\TestComplete\L5Poll\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Nomina I
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Nomina_I\Nomina_I.mds
xcopy "D:\3win_20\TestComplete\Nomina_I\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Nomina II
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Nomina_II\Nomina_II.mds
xcopy "D:\3win_20\TestComplete\Nomina_II\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Reportes I
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Reportes_I\Reportes_I.mds
xcopy "D:\3win_20\TestComplete\Reportes_I\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Rec Hum I
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Rec_Hum_I\Rec_Hum_I.mds
xcopy "D:\3win_20\TestComplete\Rec_Hum_I\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title Rec Hum II
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\Rec_Hum_II\Rec_Hum_II.mds
xcopy "D:\3win_20\TestComplete\Rec_Hum_II\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
Title TressEmail
START /W %testcomplete% /ns /RunAndExit /SilentMode %folderdestino02%\TressEmail\TressEmail.mds
xcopy "D:\3win_20\TestComplete\TressEmail\Results" "C:\Documents and Settings\Administrator\Desktop\Resultados\Version" /S /Y
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Ejecucion de Casos Automaticos de Cliente
::Aqui se agregan los casos automaticos de cada cliente. 
::Ejemplo:  START /W "C:\Progra~1\Automa~1\TestCo~1\TestCo~1.exe" /ns /RunAndExit /SilentMode D:\3win_20\TestComplete\<archivo.mds>

::Se deja un mensaje final de que termino
cls
echo.FIN
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::Fin