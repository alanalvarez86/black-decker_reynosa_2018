program TressCafeteria;

uses
  Forms,
  FCafeteria in '..\..\..\TressCafe\FCafeteria.pas' {FormaCafe},
  DCafeCliente in '..\..\..\TressCafe\DCafeCliente.pas' {dmCafeCliente: TDataModule},
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FConfigura in '..\..\..\TressCafe\FConfigura.pas' {FormaConfigura},
  FPreguntaComida in '..\..\..\TressCafe\FPreguntaComida.pas' {PreguntaComida},
  TCClient in 'C:\Program Files\Automated QA\TestComplete\Clients\Delphi&BCB\TCClient.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Cliente de Cafeter�a';
  Application.HelpFile := 'TressCafeteria.chm';
  Application.CreateForm(TFormaCafe, FormaCafe);
  Application.CreateForm(TdmCafeCliente, dmCafeCliente);
  Application.Run;
end.
