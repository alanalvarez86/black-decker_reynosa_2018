unit DMonitorTress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$IFDEF TRESS_DELPHIXE5_UP}
  DEmailService,
  {$ELSE}
  DEmailServiceD7,
  {$ENDIF}
  DMonitorRegistry;

type
    eTests = ( eEspacio,
               {$ifdef INTERBASE}
               eDirectorioTemp,
               {$endif}
               eProcAbiertos,
               eProcesos,
               eProcesosError,
               ePollRelojes,
               {$ifdef INTERBASE}
               eInterbaseServices,
               eInterbaseLog,
               eSweepInterval,
               eForcedWrites,
               eBuffersData,
               ePageSize,
               eDBArchivos,
               eArchivosGBK,
               {$endif}
               eConnectionPooling,
               eTracing,
               eSentinelSuperPro,
               eLicenciaUso );

    eResultado = ( eOk,
                   eWarning,
                   eError );

    eIdioma = ( eEspaniol,
                eIngles );

  TdmMonitorTress = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FRegistry: TMonitorRegistry;
    FEmailService: TdmEmailService;
    function ConectaHost: Boolean;
    function GetIdioma: boolean;
    { Private declarations }
  public
    { Public declarations }
    function ObtieneElemento( const Index: Integer ): String;
    function ObtieneResultado( const Index: Integer ): String;
    function GetComentario( eTipo: eResultado; ePrueba: eTests ): string;
    function ObtieneEtiquetas: String;    
    procedure SendEmail( const Archivo: TStrings );
    property Idioma: boolean Read GetIdioma;
  end;

var
  dmMonitorTress: TdmMonitorTress;

implementation

uses
    DMonitor,
    ZetaCommonLists,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaDialogo;

const
     { ******** Encabezados del Mensaje ************** }
     aTests: array[ eTests, eIdioma ] of PChar = ( ( 'Espacio en Discos Duros', 'Hard Disk Space' ),
                                                   {$ifdef INTERBASE}
                                                   ( 'Directorios Temporales de InterBase', 'InterBase Temporal Directories' ),
                                                   {$endif}
                                                   ( 'Procesos Abiertos de Tress', 'Open Tress Processes' ),
                                                   ( 'Procesos Generales de Tress', 'General Tress Processes' ),
                                                   ( 'Procesos En Error de Tress', 'Error Tress Processes' ),
                                                   ( 'Poll de Tarjetas de Tress', 'Asist Poll' ),
                                                   {$ifdef INTERBASE}
                                                   ( 'Servicios de InterBase', 'InterBase Services' ),
                                                   ( 'InterBase.Log', 'InterBase.Log' ),
                                                   ( 'Sweep Interval de Base de Datos', 'Sweep Interval Property' ),
                                                   ( 'Escrituras Directas a Disco', 'Enabled Forced Writes' ),
                                                   ( 'Buffers de Datos', 'Data Buffers' ),
                                                   ( 'Tama�o de Paginaci�n', 'DataBase Page Size' ),
                                                   ( 'Tama�o de los Archivos DB', 'DataBase Size Files' ),
                                                   ( 'Archivos de Respaldo GBK', 'Bakup Files GBK' ),
                                                   {$endif}
                                                   ( 'Propiedad Connection Pooling', 'Connection Pooling Property' ),
                                                   ( 'Propiedad SQL Tracing', 'SQL Tracing Property' ),
                                                   ( 'Sentinel SuperPro', 'Sentinnel SuperPro' ),
                                                   ( 'Licencia de Uso de Tress', 'License Use' ) );

     { ******** Comentarios de los Mensajes ************** }
     aComentarios: array[ eTests, eResultado, eIdioma ] of PChar = (
                                                                     ( ( 'Espacio en el Drive %s:\ esta Correcto', 'Hard Disk Space of Drive %s:\ is Correct' ),
                                                                       ( 'Espacio en el Drive %s:\ Arriba del %s %s', 'Hard Disk of Drive %s:\ is Above the %s %s' ),
                                                                       ( 'Espacio en el Drive %s:\ Arriba del %s %s', 'Hard Disk of Drive %s:\ is Above the %s %s' ) ),
                                                                     {$ifdef INTERBASE}
                                                                     ( ( 'Espacio para los Archivos Temporales de InterBase es el Correcto', 'Space for the InterBase Temporary Files Correct' ),
                                                                       ( 'Espacio en el Drive %s:\ Arriba del %s %s', 'Space for The Interbase Temporary Files is in Warning' ),
                                                                       ( 'Espacio Para los Archivos Temporales de InterBase no es el Suficiente Error', 'Space for the InterBase Temporary Files is not Enough Error' ) ),
                                                                     {$endif}
                                                                     ( ( 'No Existen Procesos Abiertos en Tress, Empresa: %s', 'No Open Processes at Tress, Company: %s' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'Existen Procesos Abiertos de Tress, Empresa: %s', 'There are Open Processes at Tress, Company: %s' ) ),
                                                                     ( ( VACIO, VACIO ),
                                                                       ( VACIO, VACIO ),
                                                                       ( VACIO, VACIO ) ),
                                                                     ( ( 'No Existen Procesos de Tress con Problemas, Empresa: %s', 'No Processes With Problems at Tress, Company: %s' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'Existen Procesos de Tress con Problemas, Empresa: %s', 'There are Tress Processes With Problems, Company: %s' ) ),
                                                                     ( ( 'El Proceso de Tarjetas en la Empresa: %s esta Correcto', 'Poll Process at Company: %s is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'El Proceso de Tarjetas en la Empresa: %s tiene Error', 'Poll Process at Company: %s with Error' ) ),
                                                                     {$ifdef INTERBASE}
                                                                     ( ( 'Los Servicios de InterBase estan Correctos', 'Then InterBase Services are Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'Error en los Servicios de InterBase', 'There is an Error with the InterBase Services' ) ),
                                                                     ( ( 'No fue Encontrada Ninguna Inconsistencua en la Base de Datos', 'No Inconsistency was Found in the DataBase' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'Se Encontro una Inconsistencia en la Base de Datos o No Existe el Archivo InterBase.log', 'An Inconsistency in the DataBase was Found or Does not Exists the File InterBase.log' ) ),
                                                                     ( ( 'El Valor de la Propiedad Sweep Interval en la Empresa: %s esta Correcto', 'The Sweep Interval Value at Company: %s is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'EL Valor de la Propiedad Sweep Interval en la Empresa: %s esta Incorrecto', 'The Sweep Interval Value At Company: %s  is Not Correct' ) ),
                                                                     ( ( 'La Propiedad Forced Writes en la Empresa: %s esta Correcta', 'The Property Forced Writes at Company: %s is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'La Propiedad Forced Writes en la Empresa: %s esta Incorrecta', 'The Property Forced Writes at Company: %s is Not Correct' ) ),
                                                                     ( ( 'El Valor de los Buffers de Datos en la Empresa: %s esta Correcto ', 'The Data Buffers Value at Company: %s is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'EL Valor de los Buffers de Datos en la Empresa: %s esta Incorrecto', 'The Data Buffers Value at Company: %s is Not Correct' ) ),
                                                                     ( ( 'El Valor de la Propiedad Page Size en la Empresa: %s esta Correcto', 'The Value of the property Page Size at Company: %s is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'El Valor de la Propiedad Page Size en la Empresa: %s esta Incorrecto', 'The Value of the Property Page Size at Company: %s is Not Correct' ) ),
                                                                     ( ( 'El Tama�o del Archivo de interBase en la Empresa: %s esta Correcto', 'The Interbase File Size at Company: %s is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'El Tama�o del Archivo de InterBase esta Arriba del %d %s en la Empresa: %s', 'The Interbase File Size is Above %d %s at Company: %s' ) ),
                                                                     ( ( 'La Fecha de los Archivos de Respaldo de InterBase esta Correcta', 'The InterBase Backup Files Date is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'La Fecha de los Archivos de Respaldo de InterBase esta Incorrecta', 'The InterBase Backup Files Date is Not Correct' ) ),
                                                                     {$endif}
                                                                     ( ( 'El Valor de la Propiedad Connection Pooling esta Correcto', 'The Value of Property Connection Pooling is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'El Valor de la Propiedad Connection Pooling esta Incorrecto', 'The Value of Property Connection Pooling is Not Correct' ) ),
                                                                     ( ( 'El Valor de la Propiedad SQL Tracing esta Correcto', 'The value of Property SQL Tracing is Correct' ),
                                                                       ( VACIO, VACIO ),
                                                                       ( 'El Valor de la Propiedad SQL Tracing esta Incorrecto', 'The Value of Property SQL Tracing is Not Correct' ) ),
                                                                     ( ( 'El Sentinel Super Pro de Tress esta Funcionando Correctamente', 'The Tress Sentinnel Super Pro is Working Fine' ),
                                                                       ( 'El Sentinel Virtual ser� Evaluado ya que No Existe Sentinel Super Pro F�sico V�lido', 'The Virtual Sentinel will be Evaluated as there is no valid Sentinel Super Pro Attached' ),
                                                                       ( 'Existe un Error en el Sentinel Super Pro de Tress', 'There is an Error with Tress Sentinnel Super Pro' ) ),
                                                                     ( ( 'La Licencia de Uso del Sistema Tress esta Correcta', 'The Tress Use License is Correct' ),
                                                                       ( 'La Licencia de Uso del sistema esta Proxima a Vencerse ', 'The Tress Use License is Going to Expired' ),
                                                                       ( 'La Licencia de Uso del sistema esta Vencida ', 'The Tress Use License Just Expired' ) )
                                                                   );

     { ************* Resultados de las Pruebas ******************* }
     aColores: array[ eResultado, eIdioma ] of PChar = ( ( 'OK', 'OK' ),
                                                         ( 'ADVERTENCIA', 'WARNING' ),
                                                         ( 'PROBLEMA', 'PROBLEM' ) );

     { ************* Etiquetas de las Pruebas ******************* }
     aEtiquetas: array[ eIdioma ] of PChar = ( 'Prueba: %s' + CR_LF + 'Fecha: %s  Hora: %s' + CR_LF + 'Resultado: %s' + CR_LF + 'Descripci�n: %s' + CR_LF,
                                               'Test: %s' + CR_LF + 'Date: %s  Time: %s'  + CR_LF + 'Result: %s' + CR_LF + 'Description: %s' + CR_LF );

{$R *.DFM}

{ TdmMonitorTress }

procedure TdmMonitorTress.DataModuleCreate(Sender: TObject);
begin
     FRegistry := TMonitorRegistry.Create;
     FEmailService := TdmEmailService.Create( self );
end;

procedure TdmMonitorTress.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FEmailService );
     FreeAndNil( FRegistry );
end;

function TdmMonitorTress.GetIdioma: boolean;
begin
     Result := FRegistry.Idioma;
end;

function TdmMonitorTress.ObtieneElemento( const Index: Integer ): String;
begin
     Result := aTests[ eTests( Index ), eIdioma( Idioma ) ];
end;

function TdmMonitorTress.ObtieneResultado(const Index: Integer): String;
begin
     Result := aColores[ eResultado( Index ), eIdioma( Idioma ) ];
end;

function TdmMonitorTress.GetComentario( eTipo: eResultado; ePrueba: eTests ): string;
begin
     Result := aComentarios[ ePrueba, eTipo, eIdioma( Idioma ) ];
end;

function TdmMonitorTress.ObtieneEtiquetas: String;
begin
     Result := aEtiquetas[ eIdioma( Idioma ) ];
end;

procedure TdmMonitorTress.SendEmail( const Archivo: TStrings );
const
     K_MAIL = '@tress.com.mx';
var
   Lista: TStrings;

   function EscribeCuerpoEmail: boolean;
   const
        aTitulo: array[ eIdioma ] of PChar = ( 'Resultado de las Pruebas Monitor de Tress',
                                               'Tress Monitor Tests Results' );

        aTituloPrueba: array[ eIdioma ] of PChar = ( 'Resultado de las Pruebas Monitor de Tress',
                                                     'Tress Monitor Tests Results' );

        aCuerpoInicio: array[ eIdioma ] of PChar = ( 'El Monitor de Tress ha Finalizado de Correr las Pruebas Seleccionadas' + CR_LF + 'Estos Fueron los Resultados:' + CR_LF + CR_LF + '**************** Inicio de Pruebas ****************',
                                                     'Tress Monitor Finished Running The Tests You Selected' + CR_LF + 'These are the Results:' + CR_LF + CR_LF + '**************** Begin Tests ****************' );

        aCuerpoFinal: array[ eIdioma ] of PChar = ( '****************   Fin de Pruebas  ****************' + CR_LF +'Atte.' + CR_LF + 'Monitor de Tress',
                                                    '****************   End Tests  ****************' + CR_LF + 'Atte.' + CR_LF + 'Tress Monitor' );

        aCuerpoPrueba: array[ eIdioma ] of PChar = ( 'Esta Fue Una Prueba de Correo Electr�nico del Monitor de Tress',
                                                     'This is an Email Test From Tress Monitor' );

   begin
        Result := True;
        with FEmailService do
        begin
             if ( Archivo.Count > 0 ) then
             begin
                  if ( Trim( Archivo.Text ) <> 'Prueba de Correo' ) then
                  begin
                       Subject := aTitulo[ eIdioma( Idioma ) ];
                       with MessageText  do
                       begin
                            Add( aCuerpoInicio[ eIdioma( Idioma ) ] );
                            Add( Archivo.Text );
                            Add( aCuerpoFinal[ eIdioma( Idioma ) ] );
                       end;
                  end
                  else
                  begin
                       Subject := aTituloPrueba[ eIdioma( Idioma ) ];
                       MessageText.Add( aCuerpoPrueba[ eIdioma( Idioma ) ] );
                  end;
             end
             else
                 Result := False;
        end;
   end;

 var sMsgError: string;
begin
     {$ifdef ANTES}
     Lista := TStringList.Create;
     try
        lista.CommaText := FRegistry.ListaCorreos;
        with Email do
        begin
             ConectaHost;
             if Connected then
             begin
                  if ( Lista.Count > 0 ) then
                  begin
                       if ( pos( K_MAIL, Lista.commatext ) > 0 ) then
                       begin
                            NewBodyMessage;
                            SubType := mtPlain;
                            with PostMessage do
                            begin
                                 ToAddress.Clear;
                                 Body.Clear;
                                 Attachments.Clear;
                                 ToAddress.AddStrings( Lista );
                            end;
                            PostMessage.Date := DateToStr( Date );
                            if EscribeCuerpoEmail then
                               SendMail;
                       end
                       else
                           ZetaDialogo.ZError( 'Monitor Tress', 'La Lista de Correos debe contener al menos ' + CR_LF +
                                                                'Una direcci�n de Correo de Grupo Tress ' + CR_LF +
                                                                '(@tress.com.mx)', 0 );
                  end
                  else
                      ZetaDialogo.ZError( 'Monitor', 'Es necesario tener al menos una Direcci�n de Correo Electr�nico,' + CR_LF +
                                                     'para poder ser enviado', 0 );
             end
             else
                 ZetaDialogo.ZError( 'Monitor', 'El Servidor %s De Email No Se Pudo Conectar' + Host, 0 );
        end;
     finally
            FreeAndNil( Lista );
     end;
     {$ELSE}
     Lista := TStringList.Create;
     try
        Lista.CommaText := FRegistry.ListaCorreos;
        if ConectaHost then
        begin
             if ( Lista.Count > 0 ) then
             begin
                  if ( pos( K_MAIL, Lista.commatext ) > 0 ) then
                  begin
                       with FEmailService do
                       begin
                            SubType := emtTexto;
                            ToAddress.AddStrings( Lista );
                            if EscribeCuerpoEmail then
                               if NOT SendEMail( sMsgError ) then
                                  ZetaDialogo.ZError( 'Monitor Tress', 'Se encontr� un Error al Tratar de Enviar el Correo ' + CR_LF + sMsgError, 0 );
                       end;
                  end
                  else
                      ZetaDialogo.ZError( 'Monitor Tress', 'La Lista de Correos debe contener al menos ' + CR_LF +
                                                           'Una direcci�n de Correo de Grupo Tress ' + CR_LF +
                                                           '(@tress.com.mx)', 0 );
             end
             else
                 ZetaDialogo.ZError( 'Monitor', 'Es necesario tener al menos una Direcci�n de Correo Electr�nico,' + CR_LF +
                                                'para poder ser enviado', 0 );
        end
        else
            ZetaDialogo.ZError( 'Monitor', Format( 'El Servidor %s De Email No Se Pudo Conectar' ,[ FEmailService.MailServer ]), 0 );
     finally
            FreeAndNil( Lista );
     end;
     {$ENDIF}
end;

function TdmMonitorTress.ConectaHost: Boolean;
begin
     {$ifdef ANTES}
     with Email do
     begin
          if not Connected then
          begin
               Host := FRegistry.Host;
               Port := FRegistry.Port;
               UserID := FRegistry.UserID;
               PostMessage.FromAddress := FRegistry.Remitente;
               PostMessage.FromName := 'Monitor Tress';
               Connect;
          end;
          Result := Connected;
     end;
     {$else}
     with FEmailService do
     begin
          NewEMail;
          MailServer := FRegistry.Host;
          Port := FRegistry.Port;
          User := FRegistry.UserId;
          FromAddress := FRegistry.Remitente;
          FromName := 'Monitor Tress';
          Result := TRUE;
     end;
     {$endif}
end;

end.
