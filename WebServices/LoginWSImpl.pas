{ Invokable implementation File for TProcesosWS which implements IProcesosWS }

unit LoginWSImpl;

interface

uses InvokeRegistry, Types, XSBuiltIns, LoginWSIntf, SysUtils,
     DClienteLogin,ZetaCommonLists;

type
  { TProcesosWS }
  TLoginWS = class(TInvokableClass, ILoginWS)
  private
    { Private declarations }
    dmCliente : TdmClienteLogin;
    procedure Init;
    procedure Release;
  public
    { Public declarations }
    function Echo( const Valores: WideString ): WideString; stdcall;
    function UsuarioLogin(const Parametros: WideString): WideString; stdcall;
    function CambiaClaveUsuario(const Parametros: string): WideString; stdcall;
  end;

implementation

uses DXMLTools;

procedure TLoginWS.Init;
begin
     dmCliente := TdmClienteLogin.Create( nil );
end;

procedure TLoginWS.Release;
begin
     FreeAndNil( dmCliente );
end;

function TLoginWS.Echo( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
begin
     Result := Format( '%s ( %s ) asdasdasasds', [ Valores, FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) ] );
end;

function TLoginWS.UsuarioLogin(const Parametros: WideString): WideString;   stdcall;
begin
     Init;
     try
        Result := dmCliente.UsuarioLogin(Parametros);
     finally
            Release;
     end;
end;

function TLoginWS.CambiaClaveUsuario(const Parametros: string): WideString;    stdcall;
begin
     Result := '';
     Init;
     try
        Result := dmCliente.CambiaClaveUsuario(Parametros);
     finally
            Release;
     end;
end;

initialization
  { Invokable classes must be registered }
  InvRegistry.RegisterInvokableClass(TLoginWS);

end.
