{ Invokable implementation File for TRecursosWS which implements IRecursosWS }

unit RecursosWSImpl;

interface

uses InvokeRegistry, Types, XSBuiltIns, RecursosWSIntf,
     SysUtils,
     DRecursos,
     ZetaCommonLists;

type

  { TRecursosWS }
  TRecursosWS = class(TInvokableClass, IRecursosWS)
  private
    dmRecursos : TdmRecursos;
    procedure RecursosInit;
    procedure RecursosRelease;
  public
    function AltaEmpleado( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoReingreso( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoBaja( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoIdentificacion( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoPersonales( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoOtros( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoAdicionales( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoExperiencia( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoParientes( const Parametros: WideString ): WideString; stdcall;
    function KardexCambio( const Parametros: WideString ): WideString; stdcall;
    function KardexVacaciones( const Parametros: WideString ): WideString; stdcall;
    function KardexPermisos( const Parametros: WideString ): WideString; stdcall;
    function KardexIncapacidades( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaTarjetaDiaria( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaTarjetaDiariaDefaults( const Parametros: WideString ): WideString; stdcall;
    function AsistenciaAutorizacionAlta( const Parametros: WideString ): WideString; stdcall;
    function NominaExcepcionMonto( const Parametros: WideString ): WideString; stdcall;
    function NominaExcepcionDias( const Parametros: WideString ): WideString; stdcall;
    function NominaExcepcionHoras( const Parametros: WideString ): WideString; stdcall;
    function Echo( const Parametros: WideString ): WideString; stdcall;
    function CatalogoPuestos( const Parametros: WideString ): WideString; stdcall;
    function CatalogoTexto(const Parametros: WideString): WideString; stdcall;
    function CatalogoTurnos( const Parametros: WideString ): WideString; stdcall;
    function EmpleadoPrestamos (const Parametros: WideString ): WideString; stdcall;
    function EmpleadoPrestaAcarAbo(const Parametros: WideString ): WideString; stdcall;
    function EmpleadoSaldoVacaciones(const Parametros: WideString ): WideString; stdcall;
    function GetVacaFechaRegreso(const Parametros: WideString ): WideString; stdcall;
    function GetVacaDiasGozados(const Parametros: WideString ): WideString; stdcall;
    function GetPermisoFechaRegreso(const Parametros: WideString ): WideString; stdcall;
    function GetPermisoDiasHabiles (const Parametros: WideString ): WideString; stdcall;
    
  end;

implementation

uses DCliente,
     DXMLTools,
     ZetaCommonTools;

procedure TRecursosWS.RecursosInit;
begin
     dmRecursos := TdmRecursos.Create( nil );
end;

procedure TRecursosWS.RecursosRelease;
begin
     FreeAndNil( dmRecursos );
end;

function TRecursosWS.Echo( const Parametros: WideString ): WideString; stdcall;
begin
     Result := Format( '%s ( %s )', [ Parametros, FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) ] );
end;

{-----------------------------
    Alta del empleado
------------------------------}
function TRecursosWS.AltaEmpleado( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaAlta( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                {Result := DXMLTools.ExcepcionAsXML( Error );}
                Result := 'Excepci�n';

           end;
     end;
end;

{-----------------------------
    Reingreso del empleado
------------------------------}
function TRecursosWS.EmpleadoReingreso( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaEmpleadoReingreso ( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Baja del Empleado
------------------------------}
function TRecursosWS.EmpleadoBaja( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaEmpleadoBaja( Parametros );

        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edici�n de la identificaci�n del empleado
------------------------------}
function TRecursosWS.EmpleadoIdentificacion( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaDatosEmpleado( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edicion de la identificaci�n del empleado
------------------------------}
function TRecursosWS.EmpleadoPersonales( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaDatosEmpleado( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edicion de Otros datos del empleado
------------------------------}
function TRecursosWS.EmpleadoOtros( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaDatosEmpleado( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{-----------------------------
    Edicion de datos adicionales del empleado
------------------------------}
function TRecursosWS.EmpleadoAdicionales( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaDatosEmpleado( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{-----------------------------
    Edicion de los datos de experiencia del empleado
------------------------------}
function TRecursosWS.EmpleadoExperiencia( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaDatosEmpleado( Parametros );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edici�n del cambio de Salario del Empleado
    Edici�n del cambio de Puesto
    Edici�n del cambio de Turno
    Edici�n del cambio de Niveles
    Edici�n del cambio de TipoN�mina
------------------------------}
function TRecursosWS.KardexCambio( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaCambioKardex(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edicion del cambio de vacaciones
------------------------------}
function TRecursosWS.KardexVacaciones( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaKardexVacaciones(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{-----------------------------
    Edicion del cambio de Permisos
------------------------------}
function TRecursosWS.KardexPermisos( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaKardexPermisos(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{-----------------------------
    Edicion del cambio de Incapacidaes
------------------------------}
function TRecursosWS.KardexIncapacidades( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
            Result := dmRecursos.ProcesaKardexIncapacidades(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edicion de la asistencia tarjeta diaria
------------------------------}
function TRecursosWS.AsistenciaTarjetaDiaria( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
            Result := dmRecursos.ProcesaAsistenciaTarjetaDiaria(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Obtener los valores 'default' de una tarjeta diaria
------------------------------}
function TRecursosWS.AsistenciaTarjetaDiariaDefaults( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
            Result := dmRecursos.ObtieneDefaultsAsistenciaTarjetaDiaria(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
                Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Registro de las autorizaciones
------------------------------}
function TRecursosWS.AsistenciaAutorizacionAlta( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
            Result := dmRecursos.ProcesaAsistenciaAutorizacion(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{-----------------------------
    Edici�n de las excepciones de monto de la n�mina
------------------------------}
function TRecursosWS.NominaExcepcionMonto( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaNominaExcepcionMonto(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edici�n de las excepciones de d�as de la n�mina
------------------------------}
function TRecursosWS.NominaExcepcionDias( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaNominaExcepcionDiasHoras(Parametros, False );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{-----------------------------
    Edici�n de las excepciones de horas en la n�mina
------------------------------}
function TRecursosWS.NominaExcepcionHoras( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaNominaExcepcionDiasHoras(Parametros, True );
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{------------------------------------------------------------
      Llamada a ABC de parientes
--------------------------------------------------------------}

function TRecursosWS.EmpleadoParientes( const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaEmpleadoParientes(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;


{------------------------------------------------------------
      Llamada al catalogo de Areas por Nivel
--------------------------------------------------------------}
function TRecursosWS.CatalogoTexto(const Parametros: WideString): WideString;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaCatalogoTexto(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{------------------------------------------------------------
      Llamada al cat�logo de Puestos
--------------------------------------------------------------}
function TRecursosWS.CatalogoPuestos(const Parametros: WideString): WideString;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaCatalogoPuestos(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{------------------------------------------------------------
      Llamada al cat�logos de Turnos
--------------------------------------------------------------}
function TRecursosWS.CatalogoTurnos( const Parametros: WideString): WideString;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaCatalogoTurnos(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{------------------------------------------------------------
      Llamada al ABC de Pr�stamos
--------------------------------------------------------------}
function TRecursosWS.EmpleadoPrestamos(const Parametros: WideString): WideString;
begin
      try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaPrestamos(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

{------------------------------------------------------------
      Llamada al ABC de Cargos y abonos de Pr�stamos
--------------------------------------------------------------}
function TRecursosWS.EmpleadoPrestaAcarAbo(const Parametros: WideString): WideString;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaPrestaAcarAbo(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;



function TRecursosWS.EmpleadoSaldoVacaciones(const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.ProcesaSaldosVacaciones(Parametros);
        finally
               RecursosRelease;
        end;
      except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

function TRecursosWS.GetVacaFechaRegreso(const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.GetVacaFechaRegreso( Parametros );
        finally
               RecursosRelease;
        end;
     except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

function TRecursosWS.GetVacaDiasGozados(const Parametros: WideString ): WideString; stdcall;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.GetVacaDiasGozados( Parametros );
        finally
               RecursosRelease;
        end;
     except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

function TRecursosWS.GetPermisoDiasHabiles(
  const Parametros: WideString): WideString;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.GetPermisoDiasHabiles (Parametros );
        finally
               RecursosRelease;
        end;
     except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

function TRecursosWS.GetPermisoFechaRegreso(
  const Parametros: WideString): WideString;
begin
     try
        RecursosInit;
        try
           Result := dmRecursos.GetPermisoFechaRegreso(Parametros)
        finally
               RecursosRelease;
        end;
     except
           On Error: Exception do
           begin
               Result := DXMLTools.ExcepcionAsXML( Error );
           end;
     end;
end;

initialization
  { Invokable classes must be registered }
  InvRegistry.RegisterInvokableClass(TRecursosWS);

end.


