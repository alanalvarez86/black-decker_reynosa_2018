program TressWebServicesRecursosTester;



uses
  Forms,
  ZetaClientTools,
  FWebServicesRecursosTest in 'FWebServicesRecursosTest.pas' {TestWebServicesRecursos},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  DCliente in '..\DCliente.pas' {dmCliente: TDataModule},
  DRecursos in '..\DRecursos.pas' {dmRecursos: TDataModule};

{$R *.RES}

begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  Application.CreateForm(TTestWebServicesRecursos, TestWebServicesRecursos);
  Application.Run;
end.
