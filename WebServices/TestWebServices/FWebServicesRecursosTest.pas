unit FWebServicesRecursosTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, Buttons, Mask, SOAPHTTPClient,
     ZetaNumero,
     IRecursosWSProxy,
     DRecursos, ComCtrls;

type
  TTestWebServicesRecursos = class(TForm)
    Paso2GB: TGroupBox;
    EmpresaLBL: TLabel;
    Archivo: TEdit;
    BtnProcesar: TButton;
    ArchivoSeek: TSpeedButton;
    OpenDialog: TOpenDialog;
    MemoResult: TMemo;
    Label2: TLabel;
    cbTipo: TComboBox;
    Iteracciones: TZetaNumero;
    IteraccionesLBL: TLabel;
    UsarWebService: TCheckBox;
    ProgressBar: TProgressBar;
    URLlbl: TLabel;
    URL: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnProcesarClick(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure UsarWebServiceClick(Sender: TObject);
  private
    { Private declarations }
    FWebServiceProxy: THTTPRIO;
    FdmRecursos: TdmRecursos;
    function EjecutaLocalmente(const sTipo, sDatos: String): String;
    function EjecutaWebService( const sTipo, sDatos: String ): String;
    function Ejecutar(const sTipo: String): String;
    function GetWebService: IRecursosWS;
    procedure ServicioClear;
    procedure ServicioInit;
  protected
    { Protected declarations }
    property dmRecursos: TdmRecursos read FdmRecursos;
  public
    { Public declarations }
  end;

var
  TestWebServicesRecursos: TTestWebServicesRecursos;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo;

{$R *.DFM}

const
     K_XML_ALTA_EMP = 'Alta empleado';
     K_XML_REINGRESO_EMP = 'Reingreso Empleado';
     K_XML_BAJA_EMP = 'Baja Empleado';
     K_XML_IDENT_EMP = 'Identificacion Empleado';
     K_XML_PERSO_EMP = 'Personales Empleado';
     K_XML_OTROS_EMP = 'Otros Empleado';
     K_XML_ADICI_EMP = 'Adicionales Empleado';
     K_XML_EXPER_EMP = 'Experiencia Empleado';
     K_XML_CAMBI_SAL_EMP = 'Cambio salario';
     K_XML_CAMBI_PUE_EMP = 'Cambio puesto';
     K_XML_CAMBI_TUR_EMP = 'Cambio turno';
     K_XML_CAMBI_NIV_EMP = 'Cambio niveles';
     K_XML_CAMBI_VAC_EMP = 'Registro vacaciones';
     K_XML_CAMBI_PER_EMP = 'Registro permisos';
     K_XML_CAMBI_INC_EMP = 'Registro incapacidades';
     K_XML_CAMBI_TAR_EMP = 'Registro Tarjeta';
     K_XML_DEFAULTS_TAR_EMP = 'Registro Tarjeta Defaults';
     K_XML_REGIS_AUT_EMP = 'Registro autorizacion';
     K_XML_CAMBI_EMO_EMP = 'Excep Monto';
     K_XML_CAMBI_EDI_EMP = 'Excep Dias';
     K_XML_CAMBI_EHO_EMP = 'Excep Horas';
     K_XML_PARIE_EMP = 'Parientes Empleado';
     K_XML_CATAL_TXT   = 'Cat�logo de Texto';
     K_XML_CATAL_AREA2 = 'Cat�logo de Areas2';
     K_XML_CATAL_TURNO = 'Cat�logo de Turnos';
     K_XML_CATAL_PUEST = 'Cat�logo de Puestos';
     K_XML_PRESTAMOS = 'Pr�stamos ';
     K_XML_PCARABO = 'Pr�stamos Cargos Abonos';
     K_XML_KAR_TIP_NOM = 'KardexTipoNomina';


     K_XML_ECHO = 'Echo';


procedure TTestWebServicesRecursos.FormCreate(Sender: TObject);
begin
     Iteracciones.Valor := 1;
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     with cbTipo do
     begin
          with Items do
          begin
               Clear;
               BeginUpdate;
               try
                  Add( K_XML_ALTA_EMP );
                  Add( K_XML_REINGRESO_EMP );
                  Add( K_XML_BAJA_EMP );
                  Add( K_XML_IDENT_EMP );
                  Add( K_XML_PERSO_EMP );
                  Add( K_XML_OTROS_EMP );
                  Add( K_XML_ADICI_EMP );
                  Add( K_XML_EXPER_EMP );
                  Add( K_XML_CAMBI_SAL_EMP );
                  Add( K_XML_CAMBI_PUE_EMP );
                  Add( K_XML_CAMBI_TUR_EMP );
                  Add( K_XML_CAMBI_NIV_EMP );
                  Add( K_XML_CAMBI_VAC_EMP );
                  Add( K_XML_CAMBI_PER_EMP );
                  Add( K_XML_CAMBI_INC_EMP );
                  Add( K_XML_CAMBI_TAR_EMP );
                  Add( K_XML_DEFAULTS_TAR_EMP );
                  Add( K_XML_REGIS_AUT_EMP );
                  Add( K_XML_CAMBI_EMO_EMP );
                  Add( K_XML_CAMBI_EDI_EMP );
                  Add( K_XML_CAMBI_EHO_EMP );
                  Add( K_XML_PARIE_EMP );
                  Add( K_XML_CATAL_TXT );
                  Add( K_XML_CATAL_AREA2 );
                  Add( K_XML_CATAL_TURNO );
                  Add( K_XML_CATAL_PUEST );
                  Add( K_XML_PRESTAMOS );
                  Add( K_XML_PCARABO);
                  Add( K_XML_KAR_TIP_NOM );
                  Add( K_XML_ECHO );

               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
end;

procedure TTestWebServicesRecursos.FormDestroy(Sender: TObject);
begin
     ZetaRegistryCliente.ClearClientRegistry;
end;

{ ********* Metodos para inicializar web-service ************** }

function TTestWebServicesRecursos.GetWebService: IRecursosWS;
begin
     Result := IRecursosWSProxy.GetIRecursosWS( False, Self.URL.Text, Self.FWebServiceProxy );
end;

procedure TTestWebServicesRecursos.ServicioInit;
begin
     if not Assigned( FdmRecursos ) then
     begin
          FdmRecursos := TdmRecursos.Create( nil );
     end;
end;

procedure TTestWebServicesRecursos.ServicioClear;
begin
     FreeAndNil(FdmRecursos);
end;

{ ********* Metodos para invocar web-service *************** }

function TTestWebServicesRecursos.EjecutaWebService( const sTipo, sDatos: String ): String;
begin
     Result := '';
     try
        try
           if ( sTipo = K_XML_ALTA_EMP ) then
              Result := GetWebService.AltaEmpleado( sDatos )
           else if ( sTipo = K_XML_REINGRESO_EMP ) then
                Result := GetWebService.EmpleadoReingreso( sDatos )
           else if ( sTipo = K_XML_BAJA_EMP ) then
                Result := GetWebService.EmpleadoBaja( sDatos )
           else if ( sTipo = K_XML_IDENT_EMP  ) then
                Result := GetWebService.EmpleadoIdentificacion( sDatos )
           else if ( sTipo = K_XML_PERSO_EMP  ) then
                Result := GetWebService.EmpleadoPersonales( sDatos )
           else if ( sTipo = K_XML_OTROS_EMP  ) then
                Result := GetWebService.EmpleadoOtros( sDatos )
           else if ( sTipo = K_XML_ADICI_EMP ) then
                Result := GetWebService.EmpleadoAdicionales( sDatos )
           else if ( sTipo = K_XML_EXPER_EMP ) then
                Result := GetWebService.EmpleadoExperiencia( sDatos )
           else if ( sTipo = K_XML_CAMBI_SAL_EMP ) then
                Result := GetWebService.KardexCambio( sDatos )
           else if ( sTipo = K_XML_CAMBI_PUE_EMP ) then
                Result := GetWebService.KardexCambio( sDatos )
           else if ( sTipo = K_XML_CAMBI_TUR_EMP ) then
                Result := GetWebService.KardexCambio( sDatos )
           else if ( sTipo = K_XML_CAMBI_NIV_EMP ) then
                Result := GetWebService.KardexCambio( sDatos )
           else if ( sTipo = K_XML_CAMBI_VAC_EMP ) then
                Result := GetWebService.KardexVacaciones( sDatos )
           else if ( sTipo = K_XML_CAMBI_PER_EMP ) then
                Result := GetWebService.KardexPermisos( sDatos )
           else if ( sTipo = K_XML_CAMBI_INC_EMP ) then
                Result := GetWebService.KardexIncapacidades( sDatos )
           else if ( sTipo = K_XML_CAMBI_TAR_EMP ) then
                Result := GetWebService.AsistenciaTarjetaDiaria( sDatos )
           else if ( sTipo = K_XML_DEFAULTS_TAR_EMP ) then
                Result := GetWebService.AsistenciaTarjetaDiariaDefaults( sDatos )
           else if ( sTipo = K_XML_REGIS_AUT_EMP ) then
                Result := GetWebService.AsistenciaAutorizacionAlta( sDatos)
           else if ( sTipo = K_XML_CAMBI_EMO_EMP ) then
                Result := GetWebService.NominaExcepcionMonto( sDatos )
           else if ( sTipo = K_XML_CAMBI_EDI_EMP ) then
                Result := GetWebService.NominaExcepcionDias( sDatos )
           else if ( sTipo = K_XML_CAMBI_EHO_EMP ) then
                Result := GetWebService.NominaExcepcionHoras( sDatos )
           else if ( sTipo = K_XML_PARIE_EMP ) then
                Result := GetWebService.EmpleadoParientes( sDatos)
           else if ( sTipo = K_XML_CATAL_TXT ) then
                Result := GetWebService.CatalogoTexto( sDatos)
           else if ( sTipo = K_XML_CATAL_TURNO ) then
                Result := GetWebService.CatalogoTurnos( sDatos )
           else if ( sTipo = K_XML_CATAL_PUEST ) then
                Result := GetWebService.CatalogoPuestos( sDatos )
           else if ( sTipo = K_XML_PRESTAMOS ) then
                Result := GetWebService.EmpleadoPrestamos( sDatos )
           else if ( sTipo = K_XML_PCARABO ) then
                Result := GetWebService.EmpleadoPrestaAcarAbo( sDatos )
           else if ( sTipo = K_XML_KAR_TIP_NOM ) then
                Result := GetWebService.KardexCambio( sDatos )

           else if ( sTipo = K_XML_ECHO ) then
                Result := GetWebService.Echo( sDatos );
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
     end;
end;

function TTestWebServicesRecursos.EjecutaLocalmente( const sTipo, sDatos: String ): String;
begin
     Result := '';
     ServicioInit;
     try
        try
           if ( sTipo = K_XML_ALTA_EMP ) then
              Result := dmRecursos.ProcesaAlta( sDatos )
           else if ( sTipo = K_XML_REINGRESO_EMP ) then
                Result := dmRecursos.ProcesaEmpleadoReingreso( sDatos )
           else if ( sTipo = K_XML_BAJA_EMP ) then
                Result := dmRecursos.ProcesaEmpleadoBaja( sDatos )
           else if ( sTipo = K_XML_IDENT_EMP  ) then
                Result := dmRecursos.ProcesaDatosEmpleado( sDatos )
           else if ( sTipo = K_XML_PERSO_EMP  ) then
                Result := dmRecursos.ProcesaDatosEmpleado( sDatos )
           else if ( sTipo = K_XML_OTROS_EMP  ) then
                Result := dmRecursos.ProcesaDatosEmpleado( sDatos )
           else if ( sTipo = K_XML_ADICI_EMP ) then
                Result := dmRecursos.ProcesaDatosEmpleado( sDatos )
           else if ( sTipo = K_XML_EXPER_EMP ) then
                Result := dmRecursos.ProcesaDatosEmpleado( sDatos )
           else if ( sTipo = K_XML_CAMBI_SAL_EMP ) then
                Result := dmRecursos.ProcesaCambioKardex( sDatos )
           else if ( sTipo = K_XML_CAMBI_PUE_EMP ) then
                Result := dmRecursos.ProcesaCambioKardex( sDatos )
           else if ( sTipo = K_XML_CAMBI_TUR_EMP ) then
                Result := dmRecursos.ProcesaCambioKardex( sDatos )
           else if ( sTipo = K_XML_CAMBI_NIV_EMP ) then
                Result := dmRecursos.ProcesaCambioKardex( sDatos )
           else if ( sTipo = K_XML_CAMBI_VAC_EMP ) then
                Result := dmRecursos.ProcesaKardexVacaciones( sDatos )
           else if ( sTipo = K_XML_CAMBI_PER_EMP ) then
                Result := dmRecursos.ProcesaKardexPermisos( sDatos )
           else if ( sTipo = K_XML_CAMBI_INC_EMP ) then
                Result := dmRecursos.ProcesaKardexIncapacidades( sDatos )
           else if ( sTipo = K_XML_CAMBI_TAR_EMP ) then
                Result := dmRecursos.ProcesaAsistenciaTarjetaDiaria( sDatos )
           else if ( sTipo = K_XML_DEFAULTS_TAR_EMP ) then
                Result := dmRecursos.ObtieneDefaultsAsistenciaTarjetaDiaria( sDatos )
           else if ( sTipo = K_XML_REGIS_AUT_EMP ) then
                Result := dmRecursos.ProcesaAsistenciaAutorizacion( sDatos)
           else if ( sTipo = K_XML_CAMBI_EMO_EMP ) then
                Result := dmRecursos.ProcesaNominaExcepcionMonto( sDatos )
           else if ( sTipo = K_XML_CAMBI_EDI_EMP ) then
                Result := dmRecursos.ProcesaNominaExcepcionDiasHoras( sDatos, False )
           else if ( sTipo = K_XML_CAMBI_EHO_EMP ) then
                Result := dmRecursos.ProcesaNominaExcepcionDiasHoras( sDatos, True )
           else if ( sTipo = K_XML_PARIE_EMP ) then
                Result := dmRecursos.ProcesaEmpleadoParientes( sDatos)
           else if ( sTipo = K_XML_CATAL_TXT ) then
                Result := dmRecursos.ProcesaCatalogoTexto( sDatos)
           else if ( sTipo = K_XML_CATAL_TURNO ) then
                Result := dmRecursos.ProcesaCatalogoTurnos( sDatos )
           else if ( sTipo = K_XML_CATAL_PUEST ) then
                Result := dmRecursos.ProcesaCatalogoPuestos( sDatos )
           else if ( sTipo = K_XML_PRESTAMOS ) then
                Result := dmRecursos.ProcesaPrestamos( sDatos )
           else if ( sTipo = K_XML_PCARABO ) then
                Result := dmRecursos.ProcesaPrestaAcarAbo( sDatos )
           else if ( sTipo = K_XML_KAR_TIP_NOM ) then
                Result := dmRecursos.ProcesaCambioKardex( sDatos )
           else if ( sTipo = K_XML_CATAL_PUEST ) then
                Result :=  GetWebService.Echo( sDatos );

        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
        end;
     finally
            ServicioClear;
     end;
end;

function TTestWebServicesRecursos.Ejecutar( const sTipo: String ): String;
var
   i, iTop: Integer;
   Datos, Resultado, Acumulado: String;
begin
     iTop := Iteracciones.ValorEntero;
     with ProgressBar do
     begin
          Max := iTop;
          Step := 1;
          Position := 0;
          Visible := ( iTop > 1 );
     end;
     with MemoResult do
     begin
          with Lines do
          begin
               Clear;
               try
                  BeginUpdate;
                  LoadFromFile( Archivo.Text );
               finally
                      EndUpdate;
               end;
               Datos := Text;
          end;
     end;
     Acumulado := '';
     for i := 1 to iTop do
     begin
          if UsarWebService.Checked then
          begin
               Resultado := EjecutaWebService( cbTipo.Text, Format( Datos, [ IntToStr( i ) ] ) );
          end
          else
          begin
               Resultado := EjecutaLocalmente( cbTipo.Text, Format( Datos, [ IntToStr( i ) ] ) );
          end;
          Acumulado := Acumulado + CR_LF + StringOfChar( '=', 18 ) + CR_LF + Format( 'Iteracci�n # %d', [ i ] ) + CR_LF + StringOfChar( '=', 18 ) + CR_LF + Resultado;
          if ( iTop > 1 ) then
          begin
               ProgressBar.StepIt;
          end;
     end;
     if ( iTop > 1 ) then
     begin
          ProgressBar.Visible := False;
     end;
     with MemoResult do
     begin
          with Lines do
          begin
               try
                  BeginUpdate;
                  Text := Datos + Acumulado;
               finally
                      EndUpdate;
               end;
               //SaveToFile( Format( '%sResultado.xml', [ ExtractFilePath( Application.ExeName ) ] ) );
          end;
     end;
end;

{ *********** Eventos de botones *************** }

procedure TTestWebServicesRecursos.BtnProcesarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Ejecutar( cbTipo.Text );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTestWebServicesRecursos.ArchivoSeekClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          FileName := ExtractFileName( Archivo.Text );
          InitialDir := ExtractFilePath( Archivo.Text );
          if Execute then
             Archivo.Text := FileName;
     end;
end;

procedure TTestWebServicesRecursos.UsarWebServiceClick(Sender: TObject);
begin
     URLlbl.Enabled := UsarWebService.Checked;
     URL.Enabled := UsarWebService.Checked;
end;

end.
