object TestWebServicesRecursos: TTestWebServicesRecursos
  Left = 303
  Top = 208
  Width = 696
  Height = 571
  Caption = 'Probar web services para edici'#243'n de datos '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Paso2GB: TGroupBox
    Left = 0
    Top = 0
    Width = 688
    Height = 130
    Align = alTop
    Caption = ' Par'#225'metros '
    TabOrder = 0
    object EmpresaLBL: TLabel
      Left = 57
      Top = 16
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Archivo:'
    end
    object ArchivoSeek: TSpeedButton
      Left = 654
      Top = 13
      Width = 25
      Height = 25
      Hint = 'Buscar Archivo de Importaci'#243'n'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333FFF333333333333000333333333
        3333777FFF3FFFFF33330B000300000333337F777F777773F333000E00BFBFB0
        3333777F773333F7F333000E0BFBF0003333777F7F3337773F33000E0FBFBFBF
        0333777F7F3333FF7FFF000E0BFBF0000003777F7F3337777773000E0FBFBFBF
        BFB0777F7F33FFFFFFF7000E0BF000000003777F7FF777777773000000BFB033
        33337777773FF733333333333300033333333333337773333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = ArchivoSeekClick
    end
    object Label2: TLabel
      Left = 72
      Top = 38
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object IteraccionesLBL: TLabel
      Left = 37
      Top = 60
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Iteracciones:'
    end
    object URLlbl: TLabel
      Left = 72
      Top = 107
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'URL:'
    end
    object Archivo: TEdit
      Left = 100
      Top = 14
      Width = 550
      Height = 21
      TabOrder = 0
    end
    object BtnProcesar: TButton
      Left = 254
      Top = 41
      Width = 66
      Height = 57
      Caption = 'Procesar XML'
      TabOrder = 5
      WordWrap = True
      OnClick = BtnProcesarClick
    end
    object cbTipo: TComboBox
      Left = 100
      Top = 35
      Width = 145
      Height = 21
      Style = csDropDownList
      DropDownCount = 20
      ItemHeight = 13
      TabOrder = 1
    end
    object Iteracciones: TZetaNumero
      Left = 100
      Top = 56
      Width = 38
      Height = 21
      Mascara = mnEmpleado
      TabOrder = 2
    end
    object UsarWebService: TCheckBox
      Left = 7
      Top = 83
      Width = 106
      Height = 15
      Alignment = taLeftJustify
      Caption = 'Usar WebService:'
      TabOrder = 3
      OnClick = UsarWebServiceClick
    end
    object ProgressBar: TProgressBar
      Left = 334
      Top = 64
      Width = 342
      Height = 17
      TabOrder = 6
      Visible = False
    end
    object URL: TEdit
      Left = 100
      Top = 103
      Width = 576
      Height = 21
      TabOrder = 4
      Text = 
        'http://DELL-APACHE01/TressRecursos/RecursosWS.dll/soap/IRecursos' +
        'WS'
    end
  end
  object MemoResult: TMemo
    Left = 0
    Top = 130
    Width = 688
    Height = 407
    Align = alClient
    Lines.Strings = (
      '')
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'xml'
    Filter = 'Archivos de XML (*.dat)|*.xml|Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo a Procesar'
    Left = 366
    Top = 74
  end
end
