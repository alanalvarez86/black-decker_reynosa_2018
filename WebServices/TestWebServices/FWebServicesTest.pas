unit FWebServicesTest;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, StdCtrls, Buttons, Mask, ZetaNumero,SOAPHTTPClient,
     IProcesosWSProxy,DProcesos;

type
  TTestWebServices = class(TForm)
    Paso2GB: TGroupBox;
    EmpresaLBL: TLabel;
    Archivo: TEdit;
    BtnProcesar: TButton;
    ArchivoSeek: TSpeedButton;
    OpenDialog: TOpenDialog;
    MemoResult: TMemo;
    Label2: TLabel;
    cbTipo: TComboBox;
    txtURL: TEdit;
    Label1: TLabel;
    chWs: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtnProcesarClick(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
  private
    { Private declarations }
    FWebServiceProxy: THTTPRIO;
    function EjecutaWebService( sTipo: String ): String;
    function EjecutaLocalmente(sTipo: String): String;
  public
    { Public declarations }
    function GetWebService: IProcesosWS;
  end;

var
  TestWebServices: TTestWebServices;
  dmProcesos : TdmProcesos ;

implementation

uses ZetaCommonClasses,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaDialogo;

{$R *.DFM}

const
     K_XML_SAL_INTEG = 'WIZ_SAL_INTEG';
     K_XML_RECAL_TAR = 'WIZ_RECAL_TAR';
     K_XML_CAL_PRENO = 'WIZ_CAL_PRENO';
     K_XML_CAL_NOMIN = 'WIZ_CAL_NOMIN';
     K_XML_AFE_NOMIN = 'WIZ_AFE_NOMIN';
     K_XML_DESA_NOMI = 'WIZ_DESA_NOMI';
     K_XML_PROM_VARI = 'WIZ_PROM_VARI';
     K_XML_PROC_TARS = 'PROC_TAR_SIMPLE';

     K_XML_ASISTENCIA_AJUSTE_COLECTIVO_LISTA = 'AJUSTE_LISTA';
     K_XML_ASISTENCIA_AJUSTE_COLECTIVO_ESCRIBIR = 'AJUSTE_ESCRIBIR';

procedure TTestWebServices.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmProcesos := TdmProcesos.Create( Self );
     with cbTipo do
     begin
          with Items do
          begin
               Clear;
               BeginUpdate;
               try
                  Add( K_XML_PROC_TARS );
                  Add( K_XML_SAL_INTEG );
                  Add( K_XML_RECAL_TAR );
                  Add( K_XML_CAL_PRENO );
                  Add( K_XML_CAL_NOMIN );
                  Add( K_XML_AFE_NOMIN );
                  Add( K_XML_DESA_NOMI );
                  Add( K_XML_PROM_VARI );
                  Add( K_XML_ASISTENCIA_AJUSTE_COLECTIVO_LISTA );
                  Add( K_XML_ASISTENCIA_AJUSTE_COLECTIVO_ESCRIBIR );
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
end;

procedure TTestWebServices.FormDestroy(Sender: TObject);
begin
     FreeAndNil( dmProcesos );
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
end;

function TTestWebServices.EjecutaLocalmente( sTipo: String ): String;
begin
     MemoResult.Lines.LoadFromFile( Archivo.Text );

     if (sTipo = K_XML_SAL_INTEG)then
        Result := dmProcesos.ProcesaWizard( prRHSalarioIntegrado, MemoResult.Text )
     else if (sTipo = K_XML_RECAL_TAR)then
          Result := dmProcesos.ProcesaWizard( prASISRecalculoTarjetas , MemoResult.Text )
     else if (sTipo = K_XML_CAL_PRENO)then
          Result := dmProcesos.ProcesaWizard( prASISCalculoPreNomina , MemoResult.Text )
     else if (sTipo = K_XML_CAL_NOMIN)then
          Result := dmProcesos.ProcesaWizard( prNOCalcular , MemoResult.Text )
     else if (sTipo = K_XML_AFE_NOMIN)then
          Result := dmProcesos.ProcesaWizard( prNOAfectar , MemoResult.Text )
     else if (sTipo = K_XML_DESA_NOMI)then
          Result := dmProcesos.ProcesaWizard( prNODesafectar, MemoResult.Text )
     else if (sTipo = K_XML_PROM_VARI)then
          Result := dmProcesos.ProcesaWizard( prRHPromediarVariables, MemoResult.Text )
     else if (sTipo = K_XML_ASISTENCIA_AJUSTE_COLECTIVO_LISTA)then
          Result := dmProcesos.AjusteColectivoLista( MemoResult.Text )
     else if (sTipo = K_XML_ASISTENCIA_AJUSTE_COLECTIVO_ESCRIBIR)then
          Result := dmProcesos.AjusteColectivoEscribir( MemoResult.Text )
     else if (sTipo = K_XML_PROC_TARS)then
          Result := dmProcesos.ProcesaWizard( prASISProcesarTarjetas, MemoResult.Text )
     else
         Result := '';
end;

function TTestWebServices.EjecutaWebService( sTipo: String ): String;
begin
     MemoResult.Lines.LoadFromFile( Archivo.Text );
      Result := '';

     if (sTipo = K_XML_SAL_INTEG)then
        Result := GetWebService.SalarioIntegrado(MemoResult.Text)
     else if (sTipo = K_XML_RECAL_TAR)then
          Result := GetWebService.AsistenciaRecalcularTarjetas(MemoResult.Text)
     else if (sTipo = K_XML_CAL_PRENO)then
          Result := GetWebService.AsistenciaPrenomina( MemoResult.Text )
     else if (sTipo = K_XML_CAL_NOMIN)then
          Result := GetWebservice.NominaCalcular( MemoResult.Text )
     else if (sTipo = K_XML_AFE_NOMIN)then
          Result := GetWebservice.NominaAfectar( MemoResult.Text )
     else if (sTipo = K_XML_DESA_NOMI)then
          Result := GetWebservice.NominaDesafectar( MemoResult.Text )
     else if (sTipo = K_XML_PROM_VARI)then
          Result := GetWebservice.PromediarPercepcionesVariables(MemoResult.Text)
     else if (sTipo = K_XML_ASISTENCIA_AJUSTE_COLECTIVO_LISTA)then
          Result := GetWebService.AsistenciaAjusteColectivoDataset( MemoResult.Text )
     else if (sTipo = K_XML_ASISTENCIA_AJUSTE_COLECTIVO_ESCRIBIR)then
          Result := GetWebSERVICE.AsistenciaAjusteColectivoEscribir( MemoResult.Text )
     else if (sTipo = K_XML_PROC_TARS)then
          Result := GetWebSERVICE.AsistenciaProcesarTarjetasSimple(MemoResult.Text)
     else
         Result := 'No definido';

end;

function TTestWebServices.GetWebService: IProcesosWS;
begin
     Result := IProcesosWSProxy.GetIProcesosWS( False, Self.txtURL.Text, Self.FWebServiceProxy );
end;

procedure TTestWebServices.BtnProcesarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if chWs.Checked then
           MemoResult.Lines.Text := EjecutaWebService( cbTipo.Text )
        else
            MemoResult.Lines.Text := EjecutaLocalmente( cbTipo.Text );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTestWebServices.ArchivoSeekClick(Sender: TObject);
begin
     with OpenDialog do
     begin
          FileName := ExtractFileName( Archivo.Text );
          InitialDir := ExtractFilePath( Archivo.Text );
          if Execute then
             Archivo.Text := FileName;
     end;
end;

end.
