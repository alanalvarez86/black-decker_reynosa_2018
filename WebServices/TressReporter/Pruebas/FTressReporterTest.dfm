object ReportCaller: TReportCaller
  Left = 303
  Top = 210
  Width = 814
  Height = 553
  Caption = 'TressReporter Test'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 806
    Height = 32
    Align = alTop
    TabOrder = 0
    object URLlbl: TLabel
      Left = 4
      Top = 8
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = 'URL:'
    end
    object URL: TEdit
      Left = 32
      Top = 4
      Width = 617
      Height = 21
      TabOrder = 0
      Text = 
        'http://localhost/TressReporter/TressReporter.dll/Reportes/ITress' +
        'Reporter'
    end
    object UsarWebService: TCheckBox
      Left = 654
      Top = 6
      Width = 119
      Height = 17
      Caption = 'Usar WebService'
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 678
    Top = 32
    Width = 128
    Height = 494
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object btnReporteXML: TButton
      Left = 8
      Top = 21
      Width = 113
      Height = 41
      Caption = 'Generar Reporte XML'
      TabOrder = 0
      WordWrap = True
      OnClick = btnReporteXMLClick
    end
    object btnReportePDF: TButton
      Left = 8
      Top = 69
      Width = 113
      Height = 41
      Caption = 'Generar Reporte PDF'
      TabOrder = 1
      WordWrap = True
      OnClick = btnReportePDFClick
    end
    object btnParametros: TButton
      Left = 7
      Top = 165
      Width = 113
      Height = 41
      Caption = 'Obtener defaults de Parametros'
      TabOrder = 3
      WordWrap = True
      OnClick = btnParametrosClick
    end
    object btnBorrarDatos: TButton
      Left = 8
      Top = 368
      Width = 113
      Height = 41
      Caption = 'Borrar Datos'
      TabOrder = 5
      OnClick = btnBorrarDatosClick
    end
    object btnCargar: TButton
      Left = 8
      Top = 319
      Width = 113
      Height = 41
      Caption = 'Cargar Datos'
      TabOrder = 4
      OnClick = btnCargarClick
    end
    object btnReporteASCII: TButton
      Left = 8
      Top = 115
      Width = 113
      Height = 41
      Caption = 'Generar Archivo ASCII'
      TabOrder = 2
      WordWrap = True
      OnClick = btnReporteASCIIClick
    end
    object btnEcho: TButton
      Left = 7
      Top = 219
      Width = 113
      Height = 41
      Caption = 'Echo'
      TabOrder = 6
      WordWrap = True
      OnClick = btnEchoClick
    end
    object btnGetDataSet: TButton
      Left = 7
      Top = 267
      Width = 113
      Height = 41
      Caption = 'btnGetDataSet'
      TabOrder = 7
      WordWrap = True
      OnClick = btnGetDataSetClick
    end
    object btnValidaFormula: TButton
      Left = 7
      Top = 416
      Width = 113
      Height = 41
      Caption = 'Validar f'#243'rmula'
      TabOrder = 8
      OnClick = btnValidaFormulaClick
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 32
    Width = 678
    Height = 494
    ActivePage = TabDatos
    Align = alClient
    TabOrder = 2
    object TabDatos: TTabSheet
      Caption = 'Datos'
      ImageIndex = 1
      object XMLData: TMemo
        Left = 0
        Top = 0
        Width = 670
        Height = 466
        Align = alClient
        Lines.Strings = (
          
            '<DATOS><TIPO>0</TIPO><CODIGO>99</CODIGO><AUTOR>1</AUTOR><CLASIFI' +
            'CACION>0</CLASIFICACION><ENTIDAD>10</ENTIDAD><SOLOAUTOR>N</SOLOA' +
            'UTOR><MODIFICADO>1998/12/23</MODIFICADO><NOMBRE>Cat'#225'logo de Empl' +
            'eados</NOMBRE><TITULO>Cat'#225'logo de Empleados</TITULO><PLANTILLA>D' +
            'EFAULT</PLANTILLA><IMPRESORA>IMPRESORA DEFAULT:</IMPRESORA><SALI' +
            'DA /><FILTROS><FILTRO /><CONDICION /><LISTA><ELEMENTO DESCRIPCIO' +
            'N="Status Activo?" FORMULA="COLABORA.CB_ACTIVO" TIPOELEMENTO="5"' +
            ' ENTIDAD="10" CALCULADO="0" TIPO="0" FILTROTIPO="4" APLICACION="' +
            '0" /></LISTA></FILTROS><ORDENES><ORDEN DESCRIPCION="N'#250'mero de Em' +
            'pleado" FORMULA="COLABORA.CB_CODIGO" TIPOELEMENTO="2" ENTIDAD="1' +
            '0" CALCULADO="0" ORIENTACION="0" /></ORDENES><PARAMETROS /><FORM' +
            'ATO>0</FORMATO><SOLOTOTALES>N</SOLOTOTALES><COLUMNASHORIZONTALES' +
            '>N</COLUMNASHORIZONTALES><GRUPOS><GRUPO DESCRIPCION="Grupo Nivel' +
            ' Empresa" FORMULA="" TIPOELEMENTO="1" ENTIDAD="10" CALCULADO="0"' +
            ' SALTO="N" TOTALIZAR="S" MOSTRARENCABEZADO="S" MOSTRARPIE="S" CE' +
            'NTRAR="N"><COLUMNASENCABEZADO /><COLUMNASPIE /></GRUPO></GRUPOS>'
          
            '<COLUMNAS><COLUMNA DESCRIPCION="N'#250'mero" FORMULA="COLABORA.CB_COD' +
            'IGO" TIPOELEMENTO="0" ENTIDAD="10" CALCULADO="0" TIPO="2" MASCAR' +
            'A="#0;-#0" ANCHO="7" CAMPO="" COMOTOTALIZAR="1" /><COLUMNA DESCR' +
            'IPCION="Nombre" FORMULA="PRETTY_NAME()" TIPOELEMENTO="0" ENTIDAD' +
            '="10" CALCULADO="-1" TIPO="4" MASCARA="" ANCHO="35" CAMPO="" COM' +
            'OTOTALIZAR="0" /><COLUMNA DESCRIPCION="Calle" FORMULA="COLABORA.' +
            'CB_CALLE" TIPOELEMENTO="0" ENTIDAD="10" CALCULADO="0" TIPO="4" M' +
            'ASCARA="" ANCHO="30" CAMPO="" COMOTOTALIZAR="0" /><COLUMNA DESCR' +
            'IPCION="Colonia" FORMULA="COLABORA.CB_COLONIA" TIPOELEMENTO="0" ' +
            'ENTIDAD="10" CALCULADO="0" TIPO="4" MASCARA="" ANCHO="30" CAMPO=' +
            '"" COMOTOTALIZAR="0" /><COLUMNA DESCRIPCION="Tel'#233'fono" FORMULA="' +
            'COLABORA.CB_TEL" TIPOELEMENTO="0" ENTIDAD="10" CALCULADO="0" TIP' +
            'O="4" MASCARA="" ANCHO="20" CAMPO="" COMOTOTALIZAR="0" /></COLUM' +
            'NAS><VALORES><TRESSUSERNAME>tress</TRESSUSERNAME><TRESSPASSWORD>' +
            'gti</TRESSPASSWORD><EMPRESA>DEMO</EMPRESA><USUARIO>1</USUARIO><V' +
            'ALORESACTIVOS><YEAR>2007</YEAR><FECHADEFAULT>2007/11/29</FECHADE'
          
            'FAULT><FECHAASISTENCIA>2007/11/29</FECHAASISTENCIA><EMPLEADO>579' +
            '9</EMPLEADO><PERIODO><YEAR>2007</YEAR><TIPO>1</TIPO><NUMERO>0</N' +
            'UMERO></PERIODO><IMSS><PATRON /><YEAR>2007</YEAR><TIPO>0</TIPO><' +
            'MES>11</MES></IMSS></VALORESACTIVOS></VALORES></DATOS>')
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
    object TabResultados: TTabSheet
      Caption = 'Resultados'
      object XMLResult: TMemo
        Left = 0
        Top = 0
        Width = 642
        Height = 386
        Align = alClient
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  object XMLOpenDialog: TOpenDialog
    DefaultExt = 'xml'
    Filter = 'Archivos XML (*.xml)|*.xml|Todos (*.*)|*.*'
    FilterIndex = 0
    Title = 'Escoja el archivo con los datos en XML'
    Left = 356
    Top = 176
  end
end
