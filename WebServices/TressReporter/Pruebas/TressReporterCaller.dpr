program TressReporterCaller;

uses
  Forms,
  FTressReporterTest in 'FTressReporterTest.pas' {ReportCaller},
  DBasicoCliente in '..\..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DReportesGenerador in '..\..\..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DReportesWebBase in '..\DReportesWebBase.pas' {dmReportWebGeneratorBase: TDataModule},
  DReportesASCII in '..\DReportesASCII.pas' {dmReportesASCII: TDataModule},
  ITressReporterProxy in 'ITressReporterProxy.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TReportCaller, ReportCaller);
  Application.Run;
end.
