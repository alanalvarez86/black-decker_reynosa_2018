unit DReportesDataset;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Db, DBClient,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZAgenteSQLClient,
     ZReportTools,
     ZQRReporteListado,
     DXMLTools,
     DReportesGenerador,
     DReportesWebBase;

type
  TdmReporteDataset = class(TdmReportWebGeneratorBase)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FXMLTools: DXMLTools.TdmXMLTools;
    FResultado: TZetaClientDataSet;
  protected
    { Protected declarations }
    procedure DoOnGetResultado( const lResultado: Boolean; const Error : string );override;
  public
    { Public declarations }
    property Resultado: TZetaClientDataSet read FResultado write FResultado;
    property XMLTools: DXMLTools.TdmXMLTools read FXMLTools write FXMLTools;
    function Procesar: Integer;
  end;

implementation

uses DCliente,
     DGlobal,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZFuncsCliente,
     ZAccesosTress;

{$R *.dfm}

procedure TdmReporteDataset.DataModuleCreate(Sender: TObject);
begin
     Self.FMostrarError := False;
     inherited;
end;

procedure TdmReporteDataset.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     if Assigned( FResultado ) then
     begin
          FResultado := nil;
     end;
end;

procedure TdmReporteDataset.DoOnGetResultado(const lResultado: Boolean; const Error: string);
begin
     inherited DoOnGetResultado( lResultado, Error );
     Self.ReporteInfo.LoadFromAgente( Self.SQLAgente );
     with Self.XMLTools do
     begin
          WriteStartElement( 'AGENTE' );
          Self.ReporteInfo.SaveToXML(Self.XMLTools);
          WriteEndElement;
          XMLBuildDataSet( cdsResultados, 'DATA' );
          {TODO : Realizar un nuevo m�todo para escribir las columnas que quedan como constantes }
     end;
end;

function TdmReporteDataset.Procesar: Integer;
begin
     Global.Conectar;
     Empresa := dmCliente.ValoresActivos.EmpresaActiva;
     if GenerarReporte( True ) then
     begin
          Result := cdsResultados.RecordCount;
     end
     else
     begin
          Result := 0;
     end;
end;

end.
