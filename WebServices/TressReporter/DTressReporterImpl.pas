{ Invokable implementation File for TTressReporter which implements ITressReporter }

unit DTressReporterImpl;

interface

uses Types, SysUtils, Classes,StrUtils,Dialogs,
     {$ifndef TEST}
     InvokeRegistry, XSBuiltIns,
     DTressReporterIntf,
     {$endif}
     gtCstPDFEng,
     gtExPDFEng,
     gtPDFEng,
     DCliente,
     DXMLTools,
     DReportesASCII,
     DReportesPDF,
     DReportesXML,
     DReportesDataSet,
     DDiccionario,
     DValoresActivos;

type
  TWebServiceStringReply = class( TObject )
  private
    { Private declarations }
    FErrorWaterMark: string;
    FDatos: string;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    property Datos: string read FDatos write FDatos;
    function ErrorIs( const Valor: String ): Boolean;
    function ErrorGet( const Valor: String ): String;
    procedure ErrorSet(const Valor: string);
  end;
  TWebServiceBinaryStream = class( TMemoryStream )
  private
    { Private declarations }
    FErrorWaterMark: array[ 0..4 ] of Byte;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    function ErrorIs( const Valor: TByteDynArray ): Boolean;
    function ErrorGet( const Valor: TByteDynArray ): String;
    function SaveToBytes: TByteDynArray;
    procedure ErrorSet(const Valor: string);
  end;
  { TTressReporter }
  TTressReporter = class( {$ifdef TEST}TObject{$else}TInvokableClass, ITressReporter{$endif})
  private
    { Private declarations }
    FReportesASCII: TdmReportesASCII;
    FReportesDataset: TdmReporteDataset;
    FReportesPDF: TdmReportesPDF;
    FReportesXML: TdmReportesXML;
    FStream: TWebServiceBinaryStream;
    FBitacora:TStrings;
    function GetXMLWriter: DXMLTools.TdmXMLTools;
    function GetXMLReader: DXMLTools.TdmXMLTools;
    function ErrorAsXML(const Error: String; Excepcion: Exception): WideString;overload;
    function ErrorAsXML( const Error: String ): WideString;overload;
    procedure WriteLogToFile(const Datos: string);
  protected
    { Protected declarations }
    property XMLWriter: TdmXMLTools read GetXMLWriter;
    property XMLReader: TdmXMLTools read GetXMLReader;
    property ReportesASCII: TdmReportesASCII read FReportesASCII;
    property ReportesPDF: TdmReportesPDF read FReportesPDF;
    property ReportesDataset: TdmReporteDataset read FReportesDataset;
    property ReportesXML: TdmReportesXML read FReportesXML;
    function Setup: Boolean;
    function TestPDFStream: TByteDynArray;
    procedure ReportesXMLInit;
    procedure ReportesASCIIInit;
    procedure ReportesDatasetInit;
    procedure ReportesPDFInit; overload;
    procedure ReportesPDFInit( Stream: TStream); overload;
  public
    { Public declarations }
    constructor Create; {$ifndef TEST}override;{$endif}
    destructor Destroy; override;
    property Bitacora: TStrings read FBitacora;
    function Echo( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
    function ReportAsASCII( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
    function ReportAsPDF( const Valores: WideString ): TByteDynArray; {$ifndef TEST}stdcall;{$endif}
    function ReportAsXML( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
    function ReportDataset( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
    function ReportParametersGetDefaults( const Valores: WideString ): WideString;{$ifndef TEST}stdcall;{$endif}
    function ValidaFormula( const Valores: WideString ): WideString;{$ifndef TEST}stdcall;{$endif}
  end;

implementation

uses ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

{ ************ TWebServiceStringReply *************** }

constructor TWebServiceStringReply.Create;
begin
     inherited Create;
     FErrorWaterMark := '11235';
end;

function TWebServiceStringReply.ErrorGet( const Valor: String ): String;
begin
     Result := StringReplace( Valor, FErrorWaterMark, '', [] );
end;

function TWebServiceStringReply.ErrorIs( const Valor: String ): Boolean;
begin
     Result := Pos( FErrorWaterMark, Valor ) > 0;
end;

procedure TWebServiceStringReply.ErrorSet(const Valor: string);
begin
     Datos := Format( '%s%s', [ FErrorWaterMark, Valor ] );
end;

{ ************ TWebServiceBinaryStream *************** }

constructor TWebServiceBinaryStream.Create;
begin
     inherited Create;
     FErrorWaterMark[ 0 ] := 1;
     FErrorWaterMark[ 1 ] := 1;
     FErrorWaterMark[ 2 ] := 2;
     FErrorWaterMark[ 3 ] := 3;
     FErrorWaterMark[ 4 ] := 5;
end;

function TWebServiceBinaryStream.ErrorGet( const Valor: TByteDynArray ): String;
var
   i: Integer;
begin
     Result := '';
     for i := ( Length( FErrorWaterMark ) + 1 ) to Length( Valor ) do
     begin
          Result := Result + Chr( Valor[ i ] );
     end;
end;

function TWebServiceBinaryStream.ErrorIs( const Valor: TByteDynArray ): Boolean;
var
   i: Integer;
begin
     Result := ( Length( Valor ) > Length( FErrorWaterMark ) );
     if Result then
     begin
          for i := 0 to ( Length( FErrorWaterMark ) - 1 ) do
          begin
               Result := ( Valor[ i + 1 ] = FErrorWaterMark[ i ] );
               if not Result then
               begin
                    Break;
               end;
        end;
     end;
end;

procedure TWebServiceBinaryStream.ErrorSet(const Valor: string);
const
     K_DOS_ESPACIOS = '  ';
var
   i: Integer;
begin
     Self.Clear;
     for i := 0 to ( Length( FErrorWaterMark ) - 1 ) do
     begin
          Self.WriteBuffer( FErrorWaterMark[ i ], 1 );
     end;
     for i := 1 to ( Length( Valor ) ) do
     begin
          Self.WriteBuffer( Valor[ i ], 1 );
     end;
     Self.WriteBuffer( K_DOS_ESPACIOS, 2 );
end;

function TWebServiceBinaryStream.SaveToBytes: TByteDynArray;
var
   Data: Byte;
   i, iSize: integer;
begin
     iSize := Self.Size;
     if ( iSize > 0 ) then
     begin
          SetLength( Result, iSize );
          Self.Position := 0;
          for i := 0 to ( iSize ) do
          begin
               Self.ReadBuffer( Data, 1 );
               Result[ Self.Position ] := Data;
               Self.Position := i;
          end;
     end;
end;

{ Constructores / Destructores }

constructor TTressReporter.Create;
begin
     inherited Create;
     if not Assigned( DCliente.dmCliente ) then
     begin
          DCliente.dmCliente := TdmCliente.Create( nil );
     end;
     FBitacora := TStringList.Create;
end;

destructor TTressReporter.Destroy;
begin
     if Assigned( DCliente.dmCliente ) then
     begin
          FreeAndNil( DCliente.dmCliente );
     end;
     if Assigned( FReportesASCII ) then
     begin
          FreeAndNil( FReportesASCII );
     end;
     if Assigned( FReportesDataset ) then
     begin
          FreeAndNil( FReportesDataset );
     end;
     if Assigned( FReportesPDF ) then
     begin
          FreeAndNil( FReportesPDF );
     end;
     if Assigned( FReportesXML ) then
     begin
          FreeAndNil( FReportesXML );
     end;
     FreeAndNil(FBitacora);
     inherited Destroy;
end;

{ Metodos privados }

function TTressReporter.GetXMLReader: DXMLTools.TdmXMLTools;
begin
     Result := dmCliente.XMLReader;
end;

function TTressReporter.GetXMLWriter: DXMLTools.TdmXMLTools;
begin
     Result := dmCliente.XMLResult;
end;

procedure TTressReporter.ReportesXMLInit;
begin
     if not Assigned( FReportesXML ) then
     begin
          FReportesXML := TdmReportesXML.Create( nil );
     end;
end;

function TTressReporter.ErrorAsXML( const Error: String ): WideString;
begin
     Result := Format( '<DATOS><ERROR>%s</ERROR></DATOS>', [ Error ] );
end;

function TTressReporter.ErrorAsXML( const Error: String; Excepcion: Exception ): WideString;
begin
     Result := ErrorAsXML(  Format( '%s: %s', [ Error, Excepcion.Message ] ) );
end;

procedure TTressReporter.ReportesASCIIInit;
begin
     if not Assigned( ReportesASCII ) then
     begin
          FReportesASCII := TdmReportesASCII.Create( nil );
     end;
end;

procedure TTressReporter.ReportesDatasetInit;
begin
     if not Assigned( FReportesDataset ) then
     begin
          FReportesDataset := TdmReporteDataset.Create( nil );
     end;
end;

procedure TTressReporter.ReportesPDFInit( Stream: TStream );
begin
     ReportesPDFInit;
     if Assigned( Self.ReportesPDF ) then
     begin
          Self.ReportesPDF.DefaultStream := Stream;
     end;
end;

procedure TTressReporter.ReportesPDFInit;
begin
     if not Assigned( FReportesPDF ) then
     begin
          FReportesPDF := TdmReportesPDF.Create( nil );
     end;
end;

function TTressReporter.Setup: Boolean;
begin
     Result := False;
     with dmCliente do
     begin
          if UsuarioLogin() then
          begin
               if SetEmpresaActiva() then
               begin
                    Result := True;
               end
               else
               begin
                    XMLWriter.XMLError( Format( 'Empresa "%s" no existe', [ ValoresActivos.EmpresaActiva ] ) );
                    //LogError(Format( 'Empresa "%s" no existe', [ ValoresActivos.EmpresaActiva ] ) );
               end;
          end
          else
          begin
               XMLWriter.XMLError( 'Credenciales de Sistema TRESS incorrectas' );
          end;
     end;
end;

function TTressReporter.TestPDFStream: TByteDynArray;
var
   mStream: TMemoryStream;
   gtPDFEngine: TgtPDFEngine;
   aData: TByteDynArray;
   Data: Byte;
   i, iSize: integer;
begin
     gtPDFEngine := TgtPDFEngine.Create(nil);
     with gtPDFEngine do
     begin
          mStream:= TMemoryStream.Create;
          with Preferences do
          begin
               OutputToUserStream := True;
               ShowSetupDialog := FALSE;
               OpenAfterCreate := FALSE;
          end;
          UserStream := mStream;
          BeginDoc;
          TextOut(2, 2,'HOOOOOOLA! DE PARTE DE CAROLINA <8-) ' );
          TextOut(2, 3,Format( ' Hoy es %s, y son las %s', [FormatDateTime('dd/mmm/yyyy', Date), FormatDateTime('hh:nn:ss', Now) ]) );
          EndDoc;
          {****** Hasta aqui la creacion del PDF}

          SetLength( aData, mStream.Size );
          iSize := mStream.Size;
          mStream.Position :=0;
          for i := 0 to ( iSize - 1 ) do
          begin
               mStream.ReadBuffer( Data, 1 );
               aData[mStream.Position] := Data;
               mStream.Position :=i;
          end;
          mStream.Free;
          Result := aData;
     end;
end;

{ ************** Interfases ************** }

function TTressReporter.Echo( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
begin
     Result := Format( '%s ( %s )', [ Valores, FormatDateTime( 'dd/mmm/yyyy hh:nn AM/PM', Now ) ] );
end;

function TTressReporter.ReportAsXML( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
var
   sNombre: String;
begin
     try
        ReportesXMLInit;
        with dmCliente do
        begin
             LogInit;
        end;
        try
           with dmCliente do
           begin
                CargarValoresActivos( Valores );
           end;
           if Setup() then
           begin
                with ReportesXML do
                begin
                     LoadReportFromXML( XMLReader );
                     sNombre := ReporteInfo.Nombre;
                     XMLTools := Self.XMLWriter;
                     Resultado := dmCliente.cdsResultado;
                     if ( Procesar( sNombre ) > 0 ) then
                     begin
                          XMLWriter.XMLBuildDataSet( Resultado, 'REPORTE' );
                     end
                     else
                     begin
                          XMLWriter.XMLError( Format( 'Reporte %d - %s: No Hay Datos', [ ReporteInfo.Codigo, sNombre ] ) );
                     end;
                end;
           end;
        except
              on Error: Exception do
              begin
                   XMLWriter.XMLError( Error.Message );
              end;
        end;
        with dmCliente do
        begin
             LogEnd;
             Result := LogAsText;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

function TTressReporter.ReportAsPDF( const Valores: WideString ): TByteDynArray;
var
   sBitacora:string;
begin

     FStream := TWebServiceBinaryStream.Create;
     try
        try
           ReportesPDFInit( FStream );
           with dmCliente do
           begin
                LogInit;
                CargarValoresActivos( Valores );
                //FStream.LoadFromStream(ReportesPDF.DefaultStream);
                if Setup() then
                begin
                     with ReportesPDF do
                     begin
                          LoadReportFromXML( XMLReader );
                          if ( Procesar > 0 ) then
                          begin
                               if ( FStream.Size <= 0 ) then
                               begin
                                    XMLWriter.XMLError( Format( 'El reporte #%d: %s no contiene informaci�n', [ ReporteInfo.Codigo, ReporteInfo.Nombre ] ) );
                               end;
                          end
                          else
                          begin
                               if LogHayErrores then
                               begin
                                    XMLWriter.XMLError( ReportesPDF.LogText );
                               end
                               else
                               begin
                                    if StrLleno( ReporteInfo.Nombre ) then
                                    begin
                                         //XMLWriter.XMLError( 'No hay registros que cumplan con los filtros especificados' );
                                    end
                                    else
                                    begin
                                         XMLWriter.XMLError( Format( 'Reporte %d no existe', [ ReporteInfo.Codigo ] ) );
                                    end;
                               end;
                          end;
                     end;
                end;
                LogEnd;
                sBitacora := dmCliente.XMLResult.XMLAsText;
                WriteLogToFile(sBitacora);
           end;
        except
              on Error: Exception do
              begin
                   FStream.ErrorSet( 'Error encontrado ' + Error.Message );
              end;
        end;
        Result := FStream.SaveToBytes;
     finally
            FreeAndNil( FStream );
     end;
end;

function TTressReporter.ReportAsASCII( const Valores: WideString ): WideString;
var
   FReply: TWebServiceStringReply;
begin
     Result := '';
     FReply := TWebServiceStringReply.Create;
     try
        try
           ReportesASCIIInit;
           with dmCliente do
           begin
                CargarValoresActivos( Valores );
                if Setup() then
                begin
                     with ReportesASCII do
                     begin
                          LoadReportFromXML( XMLReader );
                          if ( Procesar > 0 ) then
                          begin
                               FReply.Datos := Output;
                               if ( Length( FReply.Datos ) <= 0 ) then
                               begin
                                    FReply.ErrorSet( Format( 'El reporte #%d: %s no contiene informaci�n', [ ReporteInfo.Codigo, ReporteInfo.Nombre ] ) );
                               end;
                          end
                          else
                          begin
                               if LogHayErrores then
                               begin
                                    FReply.ErrorSet( ReportesASCII.LogText );
                               end
                               else
                               begin
                                    if StrLleno( ReporteInfo.Nombre ) then
                                    begin
                                         FReply.ErrorSet( 'No hay registros que cumplan con los filtros especificados' );
                                    end
                                    else
                                    begin
                                         FReply.ErrorSet( Format( 'Reporte %d no existe', [ ReporteInfo.Codigo ] ) );
                                    end;
                               end;
                          end;
                     end;
                end;
           end;
           Result := FReply.Datos;
        except
              on Error: Exception do
              begin
                   FReply.ErrorSet( 'Error encontrado ' + Error.Message );
              end;
        end;
     finally
            FreeAndNil( FReply );
     end;
end;

function TTressReporter.ReportParametersGetDefaults( const Valores: WideString ): WideString;{$ifndef TEST}stdcall;{$endif}
begin
     try
        ReportesPDFInit;
        with dmCliente do
        begin
             LogInit;
        end;
        try
           with dmCliente do
           begin
                CargarValoresActivos( Valores );
           end;
           if Setup() then
           begin
                with ReportesPDF do
                begin
                     ReporteInfo.ReporteParametros.AddFromXML(XMLReader, True);
                     if ( ReporteInfo.ReporteParametros.Count > 0 ) then
                     begin
                          EvaluarParametros;
                          if ( LogHayErrores ) then
                          begin
                               XMLWriter.XMLError( LogText );
                          end
                          else
                          begin
                               ReporteInfo.ReporteParametros.SaveToXML( XMLWriter );
                          end;
                     end
                     else
                     begin
                          XMLWriter.XMLError( 'No hay parametros definidos en los datos recibidos' );
                     end;
                end;
           end;
        except
              on Error: Exception do
              begin
                   XMLWriter.XMLError( Error.Message );
              end;
        end;
        with dmCliente do
        begin
             LogEnd;
             Result := LogAsText;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

function TTressReporter.ReportDataset( const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
begin
     try
        with dmCliente do
        begin
             LogInit;
        end;
        ReportesDatasetInit;
        with dmCliente do
        begin
             CargarValoresActivos( Valores );
             if Setup() then
             begin
                  with ReportesDataset do
                  begin
                       LoadReportFromXML( XMLReader );
                       XMLTools := Self.XMLWriter;
                       Resultado := dmCliente.cdsResultado;
                       if ( Procesar > 0 ) then
                       begin
                       end
                       else
                       begin
                            if LogHayErrores then
                            begin
                                 XMLWriter.XMLError( Format( 'Error en Reporte %d - %s: %s', [ ReporteInfo.Codigo, ReporteInfo.Nombre, ReportesDataset.LogText ] ) );
                            end
                            else
                            begin
                            end;
                       end;
                  end;
             end;
        end;
        with dmCliente do
        begin
             LogEnd;
             Result := LogAsText;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

{-----------------------------------------------------------------------------
  M�todo para validar la f�rmula
-----------------------------------------------------------------------------}
function TTressReporter.ValidaFormula( const Valores: WideString ): WideString;{$ifndef TEST}stdcall;{$endif}
var sFormula,sMensaje :string;
begin
     try
        ReportesXMLInit;
        with dmCliente do
        begin
             LogInit;
        end;
        try
           with dmCliente do
           begin
                CargarValoresActivos( Valores );
           end;
           if Setup() then
           begin
                with ReportesXML do
                begin
                     //Leer la formula a validar
                     with XMLReader do
                     begin
                           XMLAsText := Valores;
                           sFormula := TagAsString( GetNode( 'TEXTO', GetNode('FORMULA') ), VACIO );
                     end;
                     sMensaje := ReportesXML.ValidarFormula(sFormula);
                     if ( sMensaje <> VACIO ) then
                     begin
                          //Regresar el Error
                          XMLWriter.XMLError( sMensaje )
                     end
                     else
                     begin
                          XMLWriter.WriteStartElement('INFO');
                          XMLWriter.WriteString('F�rmula OK');
                          XMLWriter.WriteEndElement();
                     end;
                end;
           end;
        except
              on Error: Exception do
              begin
                   XMLWriter.XMLError( Error.Message );
              end;
        end;
        with dmCliente do
        begin
             LogEnd;
             Result := LogAsText;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

procedure TTressReporter.WriteLogToFile( const Datos: string );
begin
     if ( Length( Datos ) > 0 ) then
     begin
          try
             FBitacora.Clear;
             FBitacora.Add(Datos);
          except
                on Error: Exception do
                begin
                     //Application.HandleException( Error );
                end;
          end;
     end;
end;

{$ifndef TEST}
initialization
  { Invokable classes must be registered }
  InvRegistry.RegisterInvokableClass(TTressReporter);
{$endif}
end.





