unit DGeneraReporte;

interface

uses
  SysUtils, Classes,DTressReporterImpl,Windows, Messages,Controls, Forms,ZetaMigrar,ZetaCommonClasses,
     ZetaCommonLists,Types;

type
  TdmGeneraReporte = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FPathReporteXml:string;
    FData:string;
    FListaParametros:TStringList;
    FServicio: TTressReporter;

    procedure GetListaParametros;
    procedure ServicioInit;
    procedure ServicioClear;
    procedure CargarXml(FileName:string);
    procedure WriteStreamToFile(const sFileName: String;
      const Datos: TByteDynArray);
    function LoadFileToStr(const FileName: TFileName): String;

  public
    { Public declarations }
    procedure ParametrosPrepara;
    procedure ReporteAsPDF;
    procedure ProcessStreamOutput(const sFileName: String; const Datos: TByteDynArray);
  end;

var
  dmGeneraReporte: TdmGeneraReporte;

implementation

uses
    ZetaCommonTools;

const
     P_REPORTE    = 'REPORTE';

{$R *.dfm}


function TdmGeneraReporte.LoadFileToStr(const FileName: TFileName): String;
var LStrings: TStringList;
begin  
    LStrings := TStringList.Create;
    try
      LStrings.Loadfromfile(FileName);
      Result := LStrings.text;  
    finally  
      FreeAndNil(LStrings);
    end;
end;

procedure TdmGeneraReporte.CargarXml(FileName: string);

begin
     if FileExists( FileName ) then
     begin
         FData := LoadFileToStr(FileName);
     end;
end;



procedure TdmGeneraReporte.DataModuleCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     FListaParametros := TStringList.Create;
     GetListaParametros;
end;

procedure TdmGeneraReporte.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaPArametros );
end;

procedure TdmGeneraReporte.GetListaParametros;
var
   i: integer;
begin
     with FListaParametros do
     begin
          Clear;
          for i := 1 to ParamCount do
          begin
               // se modifico uppercase los parametros respetan minusculas
               Add( ParamStr( i ) );
          end;
     end;
end;

procedure TdmGeneraReporte.ReporteAsPDF;
var
   aData: TByteDynArray;
   sFileName :string;
   Dif :Integer;
begin
     try
        ParametrosPrepara;
        ServicioInit;
        try
           CargarXml(ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) )+FPathReporteXml );
           aData := FServicio.ReportAsPDF( FData );

           Dif := Length(FPathReporteXml) - Pos('.',FPathReporteXml);
           sFileName := Copy(FPathReporteXml,1,Length(FPathReporteXml)- Dif-1 );
           if Length( aData ) > 0 then
              ProcessStreamOutput( Format('%s%s.pdf',[ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ),sFileName ] ), aData )
           else
           begin
                //Grabar a Bitacora
                FServicio.Bitacora.SaveToFile(Format('%s%s.log',[ZetaCommonTools.VerificaDir( ExtractFilePath( Application.ExeName ) ),sFileName ] ));
           end;
        finally
               ServicioClear;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TdmGeneraReporte.ServicioClear;
begin
      FreeAndNil(FServicio);
end;

procedure TdmGeneraReporte.ServicioInit;
begin
     if not Assigned( FServicio ) then
     begin
          FServicio := TTressReporter.Create;
     end;
end;

procedure TdmGeneraReporte.ParametrosPrepara;
begin
     with FListaParametros do
     begin
          FPathReporteXml  := Values[ P_REPORTE ];
     end;
end;

procedure TdmGeneraReporte.ProcessStreamOutput(const sFileName: String;
  const Datos: TByteDynArray);
var
   FWebStream: TWebServiceBinaryStream;
begin
     FWebStream := TWebServiceBinaryStream.Create;
     try
        if FWebStream.ErrorIs( Datos ) then
        begin
             //Self.ShowResult( FWebStream.ErrorGet( Datos ) );
        end
        else
        begin
             Self.WriteStreamToFile( sFileName, Datos );
             if FileExists( sFileName ) then
             begin
                  //ExecuteFile( sFileName );
             end;
        end;
     finally
            FreeAndNil( FWebStream );
     end;
end;


procedure TdmGeneraReporte.WriteStreamToFile( const sFileName: String; const Datos: TByteDynArray );
var
   FStream: TFileStream;
   i: Integer;
begin
     if ( Length( Datos ) > 0 ) then
     begin
          try
             if FileExists( sFileName ) then
             begin
                  //DeleteFile( sFileName );
             end;
             FStream := TFileStream.Create( sFileName, fmCreate );
             try
                FStream.Position := 0;
                for i := 0 to ( Length( Datos ) ) do
                begin
                     FStream.WriteBuffer( Datos[ i ], 1 );
                end;
             finally
                    FStream.Free;
             end;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end
     else
     begin
          //ShowMessage( 'No Hay Datos' );
     end;
end;



end.
