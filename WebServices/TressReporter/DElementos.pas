unit DElementos;

interface

uses Windows, Messages, SysUtils, Variants, Classes,
     DB, DBClient, Controls,
     DXMLTools,
     ZetaCommonLists,
     ZFuncsCliente,
     ZAgenteSQLClient,
     ZetaTipoEntidad,
     ZReportTools;

type
  TCampoMasterList = class( TObject )
  private
    { Private declarations }
    FCampos: TList;
    function GetCampo(Index: Integer): TCampoMaster;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Campo[ Index: Integer ]: TCampoMaster read GetCampo;
    function Count: Integer;
    function Add( Item: TCampoMaster ): Integer;
    procedure Clear;
  end;
  TParametrosList = class( TCampoMasterList )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure AddFromXML( Reader: DXMLTools.TdmXMLTools; ParaEvaluar: Boolean = False );
    procedure SaveToXML( Writer: TdmXMLTools );
    function AsVariant: OleVariant;
    procedure LoadFromAgente(Agente: TSQLAgenteClient);
  end;
  TCampoListadoList = class( TObject )
  private
    { Private declarations }
    FCampos: TStrings;
    function GetCampo(Index: Integer): TCampoListado;
  protected
    { Protected declarations }
    procedure AddItemFromXML( Reader: TdmXMLTools; Nodo: TZetaXMLNode );
  public
    { Public declarations }
    constructor Create( Lista: TStrings );
    destructor Destroy; override;
    property Campo[ Index: Integer ]: TCampoListado read GetCampo;
    function Count: Integer;
    function Add( Item: TCampoListado ): Integer;
    procedure AddItemsToList( Lista: TStrings );
    procedure Clear;
    procedure LoadFromAgente(Agente: TSQLAgenteClient);
  end;
  TColumnas = class( TCampoListadoList )
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
    procedure AddFromXML( Reader: DXMLTools.TdmXMLTools );
    procedure SaveToXML( Writer: TdmXMLTools );
  end;
  TGrupoOpcionesList = class( TObject )
  private
    { Private declarations }
    FGrupos: TStrings;
    function GetGrupo(Index: Integer): TGrupoOpciones;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( Lista: TStrings );
    destructor Destroy; override;
    property Grupo[ Index: Integer ]: TGrupoOpciones read GetGrupo;
    function Count: Integer;
    function Add(Item: TGrupoOpciones): Integer;
    procedure AddFromXML(Reader: TdmXMLTools);
    procedure Clear;
    procedure LoadFromAgente(Agente: TSQLAgenteClient);
    procedure SaveToXML( Writer: TdmXMLTools );
  end;
  TOrdenOpcionesList = class( TObject )
  private
    { Private declarations }
    FOrdenes: TStrings;
    function GetOrden(Index: Integer): TOrdenOpciones;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( Lista: TStrings );
    destructor Destroy; override;
    property Orden[ Index: Integer ]: TOrdenOpciones read GetOrden;
    function Count: Integer;
    function Add(Item: TOrdenOpciones): Integer;
    procedure AddFromXML(Reader: TdmXMLTools);
    procedure Clear;
  end;
  TFiltroOpcionesList = class( TObject )
  private
    { Private declarations }
    FFiltros: TStrings;
    function GetFiltro(Index: Integer): TFiltroOpciones;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( Lista: TStrings );
    destructor Destroy; override;
    property Filtro[ Index: Integer ]: TFiltroOpciones read GetFiltro;
    function Count: Integer;
    function Add( Item: TFiltroOpciones ): Integer;
    procedure AddFromXML(Reader: TdmXMLTools; RootNode: TZetaXMLNode);
    procedure Clear;
  end;
  { ///////////////////////////////
  Clase de expresion a evaluar
  //////////////////////////// }
  TExpresion = class( TObject )
  private
    { Private declarations }
    FFormula: string;
    FPosicion: Integer;
    FBanda: string;
    FCampo : string;
    FPosAgente:Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Formula : string read FFormula write FFormula;
    property Posicion : Integer read FPosicion write FPosicion;
    property Banda : string read FBanda write FBanda;
    property Campo : string read FCampo write FCampo;
    property PosAgente : Integer read FPosAgente write FPosAgente default -1;
    constructor Create(  );
    destructor Destroy; override;
    procedure SaveToXML( Writer: TdmXMLTools );
  end;

  { ///////////////////////////////
  Clase lista de expresiones a evaluar
  //////////////////////////// }
  TExpresionesList = class( TObject )
  private
    { Private declarations }
    FExpresiones : TStrings;
    function GetExpresion(Index: Integer): TExpresion;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( Lista:TStrings );
    destructor Destroy; override;
    property Expresion[ Index: Integer ]: TExpresion read GetExpresion;
    function Count: Integer;
    function Add( Item: TExpresion ): Integer;
    procedure AddFromXML(Reader: TdmXMLTools);
    procedure Clear;
    procedure SaveToXML( Writer: TdmXMLTools );
    procedure LoadFromAgente (Agente: TSQLAgenteClient);
  end;


  TReporteData = class( TObject )
  private
    { Private declarations }
    FFormato: eTipoFormato;
    FTipo: eTipoReporte;
    FEntidad: Integer;
    FCodigo: Integer;
    FAutor: Integer;
    FClasificacion: Integer;
    FTitulo: String;
    FNombre: String;
    FCondicion: String;
    FFiltro: String;
    FSoloTotales: Boolean;
    FColumnasHorizontales: Boolean;
    FReporteParametros: TParametrosList;
    FReporteColumnas: TColumnas;
    FReporteGrupos: TGrupoOpcionesList;
    FReporteOrdenes: TOrdenOpcionesList;
    FReporteFiltros: TFiltroOpcionesList;
    FExpresiones:TExpresionesList;
    function GetEntidadTipo: TipoEntidad;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create( Grupos, Ordenes, Filtros, Columnas,Expresiones: TStrings );
    destructor Destroy; override;
    property Codigo: Integer read FCodigo write FCodigo;
    property Tipo: eTipoReporte read FTipo write FTipo;
    property Formato: eTipoFormato read FFormato write FFormato;
    property Clasificacion: Integer read FClasificacion write FClasificacion;
    property Nombre: String read FNombre write FNombre;
    property Titulo: String read FTitulo write FTitulo;
    property Autor: Integer read FAutor write FAutor;
    property Entidad: Integer read FEntidad write FEntidad;
    property EntidadTipo: TipoEntidad read GetEntidadTipo;
    property Condicion: String read FCondicion write FCondicion;
    property Filtro: String read FFiltro write FFiltro;
    property SoloTotales: Boolean read FSoloTotales write FSoloTotales;
    property ColumnasHorizontales: Boolean read FColumnasHorizontales write FColumnasHorizontales;
    property ReporteParametros: TParametrosList read FReporteParametros;
    property ReporteColumnas: TColumnas read FReporteColumnas;
    property ReporteGrupos: TGrupoOpcionesList read FReporteGrupos;
    property ReporteOrdenes: TOrdenOpcionesList read FReporteOrdenes;
    property ReporteFiltros: TFiltroOpcionesList read FReporteFiltros;
    property Expresiones: TExpresionesList read FExpresiones;
    procedure LoadFromAgente(Agente: TSQLAgenteClient);
    procedure LoadFromXML(Reader: TdmXMLTools);
    procedure SaveToXML( Writer: TdmXMLTools );
  end;


implementation

uses ZetaCommonClasses,
     ZetaCommonTools;

const
     TAG_DESCRIPCION = 'DESCRIPCION';
     TAG_FORMULA = 'FORMULA';
     TAG_VALOR = 'VALOR';
     TAG_TIPOELEMENTO = 'TIPOELEMENTO';
     TAG_ENTIDAD = 'ENTIDAD';
     TAG_CALCULADO = 'CALCULADO';
     TAG_TIPO = 'TIPO';
     TAG_PARAMETRO = 'PARAMETRO';
     TAG_PARAMETROS = 'PARAMETROS';
     TAG_COMO_TOTALIZAR = 'COMOTOTALIZAR';
     TAG_MASCARA = 'MASCARA';
     TAG_ANCHO = 'ANCHO';
     TAG_COLUMNAS = 'COLUMNAS';
     TAG_COLUMNA = 'COLUMNA';
     TAG_GRUPO = 'GRUPO';
     TAG_GRUPOS = 'GRUPOS';
     TAG_SALTO_PAGINA = 'SALTO';
     TAG_TOTALIZAR = 'TOTALIZAR';
     TAG_MOSTRAR_ENCABEZADO = 'MOSTRARENCABEZADO';
     TAG_MOSTRAR_PIE = 'MOSTRARPIE';
     TAG_CENTRAR = 'CENTRAR';
     TAG_COLUMNAS_ENCABEZADO = 'COLUMNASENCABEZADO';
     TAG_COLUMNAS_PIE = 'COLUMNASPIE';
     TAG_CAMPO = 'CAMPO';
     TAG_EXPRESIONES = 'EXPRESIONES';
     TAG_EXPRESION = 'EXPRESION';


function BoolToInt( const Value: Boolean ): Integer;
const
     aCalculado: array[ false..true ] of Integer = ( 0, 1 );
begin
     Result := aCalculado[ Value ];
end;

function BoolToFormula( const Value: Boolean ): String;
const
     aCalculado: array[ false..true ] of String = ( 'False', 'True' );
begin
     Result := aCalculado[ Value ];
end;

function IntToBool( const Value: Integer ):Boolean;
begin
     Result := ( Value <> 0 );
end;

function QuitaCaracteresRaros( const Valor: String ): String;
const
     LF = Chr( 10 );
begin
     Result := ZetaCommonTools.BorraCReturn( Valor );
     Result := StrTransAll( Result, LF, ' ' );
end;

function CreateItemFromXML( Reader: TdmXMLTools; Nodo: TZetaXMLNode ): TCampoListado;
begin
     if Assigned( Nodo ) then
     begin
          Result := TCampoListado.Create;
          with Reader do
          begin
               Result.Entidad := TipoEntidad( AttributeAsInteger( Nodo, TAG_ENTIDAD, Ord( Result.Entidad )));
               Result.Formula := QuitaCaracteresRaros( AttributeAsString( Nodo, TAG_FORMULA, Result.Formula ) );
               Result.Titulo := AttributeAsString( Nodo, TAG_DESCRIPCION, ZReportTools.SINTITULO );
               Result.TipoCampo := eTipoGlobal( AttributeAsInteger( Nodo, TAG_TIPO, Ord( Result.TipoCampo ) ));
               Result.Calculado := AttributeAsInteger( Nodo, TAG_CALCULADO, Result.Calculado );
               Result.Operacion := eTipoOperacionCampo(AttributeAsInteger( Nodo, TAG_COMO_TOTALIZAR, Ord( Result.Operacion )));
               Result.Mascara := AttributeAsString( Nodo, TAG_MASCARA, Result.Mascara );
               Result.Ancho := AttributeAsInteger( Nodo, TAG_ANCHO, Result.Ancho );
               {
               Requiere := FieldByName('CR_REQUIER').AsString;
               }
          end;
     end
     else
     begin
          Result := nil;
     end;
end;

procedure SaveItemToXML( Writer: TdmXMLTools; Item: TCampoOpciones );
const
     TAG_TIPOIMP= 'TIPOIMP';
     TAG_POSAGENTE = 'POSAGENTE';
begin
     with Writer do
     begin
          WriteAttributeInteger( TAG_ENTIDAD, Ord( Item.Entidad ));
          WriteAttributeString( TAG_FORMULA, Item.Formula );
          WriteAttributeString( TAG_CAMPO, Item.NombreCampo );
          WriteAttributeString( TAG_DESCRIPCION, Item.Titulo );
          WriteAttributeInteger( TAG_TIPO, Ord( Item.TipoCampo ) );
          WriteAttributeInteger( TAG_CALCULADO, Item.Calculado );
          WriteAttributeInteger( TAG_COMO_TOTALIZAR, Ord( Item.Operacion ) );
          WriteAttributeString( TAG_MASCARA, Item.Mascara );
          WriteAttributeInteger( TAG_ANCHO, Item.Ancho );
          WriteAttributeInteger( TAG_TIPOIMP, Ord(Item.TipoImp));
          WriteAttributeInteger( TAG_POSAGENTE, Item.PosAgente );
     end;
end;

procedure SaveGrupoToXML( Writer: TdmXMLTools; Grupo: TGrupoOpciones );
var
   i:Integer;
begin
     with Writer do
     begin
          WriteStartElement(TAG_GRUPO);
          WriteAttributeString( TAG_DESCRIPCION, Grupo.Titulo );
          WriteAttributeString( TAG_FORMULA, Grupo.Formula );
          WriteAttributeString( TAG_CAMPO, Grupo.NombreCampo );
          WriteAttributeInteger( TAG_ENTIDAD, Ord( Grupo.Entidad ));
          WriteAttributeInteger( TAG_CALCULADO, Grupo.Calculado );
          WriteAttributeBoolean ( TAG_SALTO_PAGINA , Grupo.SaltoPagina  );
          WriteAttributeBoolean ( TAG_TOTALIZAR , Grupo.Totalizar );
          WriteAttributeBoolean ( TAG_MOSTRAR_ENCABEZADO  , Grupo.Encabezado   );
          WriteAttributeBoolean ( TAG_MOSTRAR_PIE  , Grupo.PieGrupo  );
          WriteAttributeBoolean ( TAG_CENTRAR   , Grupo.Master );
          WriteAttributeInteger ( TAG_TIPO,Ord(Grupo.TipoCampo));

          //Lista del encabezado
          WriteStartElement(TAG_COLUMNAS_ENCABEZADO);
          for i:=0 to Grupo.ListaEncabezado.Count-1 do
          begin
               WriteStartElement(TAG_COLUMNA);
               SaveItemToXML(Writer,TCampoOpciones(Grupo.ListaEncabezado.Objects[i]));
               WriteEndElement() 
          end;
          WriteEndElement();
          // Lista del Pie
          WriteStartElement(TAG_COLUMNAS_PIE );
          for i:=0 to Grupo.ListaPie.Count-1 do
          begin
               SaveItemToXML(Writer,TCampoOpciones(Grupo.ListaPie.Objects[i]))
          end;
          WriteEndElement();

          WriteEndElement();

     end;
end;
{ **************** TReporteData ******************** }

constructor TReporteData.Create( Grupos, Ordenes, Filtros, Columnas,Expresiones: TStrings );
begin
     FReporteParametros := TParametrosList.Create;
     FReporteColumnas := TColumnas.Create( Columnas );
     FReporteGrupos := TGrupoOpcionesList.Create( Grupos );
     FReporteOrdenes := TOrdenOpcionesList.Create( Ordenes );
     FReporteFiltros := TFiltroOpcionesList.Create( Filtros );
     FExpresiones:= TExpresionesList.Create(Expresiones);
end;

destructor TReporteData.Destroy;
begin
     FreeAndNil( FReporteFiltros );
     FreeAndNil( FReporteOrdenes );
     FreeAndNil( FReporteGrupos );
     FreeAndNil( FReporteColumnas );
     FreeAndNil( FReporteParametros );
     FreeAndNil( FExpresiones );
     inherited Destroy;
end;

function TReporteData.GetEntidadTipo: TipoEntidad;
begin
     Result := TipoEntidad( Entidad );
end;

procedure TReporteData.LoadFromXML(Reader: DXMLTools.TdmXMLTools);
const
     TAG_TIPO = 'TIPO';
     TAG_FORMATO = 'FORMATO';
     TAG_CODIGO = 'CODIGO';
     TAG_AUTOR = 'USUARIO';
     TAG_CLASIFICACION = 'CLASIFICACION';
     TAG_ENTIDAD = 'ENTIDAD';
     TAG_SOLOAUTOR = 'SOLOAUTOR';
     TAG_MODIFICADO = 'MODIFICADO';
     TAG_NOMBRE = 'NOMBRE';
     TAG_TITULO = 'TITULO';
     TAG_FILTROS = 'FILTROS';
     TAG_FILTRO = 'FILTRO';
     TAG_CONDICION = 'CONDICION';
     TAG_SOLO_TOTALES = 'SOLOTOTALES';
     TAG_COLUMNAS_HORIZONTALES = 'COLUMNASHORIZONTALES';
var
   Nodo: TZetaXMLNode;
begin
     with Reader do
     begin
          Self.Tipo := eTipoReporte( TagAsInteger( GetNode( TAG_TIPO ), Ord( Self.Tipo ) ) );
          Self.Formato := eTipoFormato( TagAsInteger( GetNode( TAG_FORMATO ), Ord( Self.Formato ) ) );
          Self.Codigo := TagAsInteger( GetNode( TAG_CODIGO ), Self.Codigo );
          Self.Autor := TagAsInteger( GetNode( TAG_AUTOR ), Self.Autor );
          Self.Clasificacion := TagAsInteger( GetNode( TAG_CLASIFICACION ), Self.Clasificacion );
          Self.Entidad := TagAsInteger( GetNode( TAG_ENTIDAD ), Self.Entidad );
          Self.Nombre := TagAsString( GetNode( TAG_NOMBRE ), Self.Nombre );
          Self.Titulo := TagAsString( GetNode( TAG_TITULO ), Self.Titulo );
          Self.SoloTotales := TagAsBoolean( GetNode( TAG_SOLO_TOTALES ), Self.SoloTotales );
          Self.ColumnasHorizontales := TagAsBoolean( GetNode( TAG_COLUMNAS_HORIZONTALES ), Self.ColumnasHorizontales );
          Nodo := GetNode( TAG_FILTROS );
          if Assigned( Nodo ) then
          begin
               Self.Condicion := TagAsString( GetNode( TAG_CONDICION, Nodo ), Self.Condicion );
               Self.Filtro := TagAsString( GetNode( TAG_FILTRO, Nodo ), Self.Filtro );
               Self.ReporteFiltros.AddFromXML(Reader, Nodo );
          end;
          if Self.EntidadTipo in [ enMovimienLista, enListadoNomina ] then
          begin
               ZFuncsCliente.EsSoloTotales := Self.SoloTotales;
               Self.SoloTotales := FALSE;
               Self.ColumnasHorizontales := FALSE;
          end;
     end;
     Self.ReporteParametros.AddFromXML(Reader);
     Self.ReporteGrupos.AddFromXML(Reader);
     Self.ReporteColumnas.AddFromXML(Reader);
     Self.ReporteOrdenes.AddFromXML(Reader);
     Self.Expresiones.AddFromXml(Reader);
end;

procedure TReporteData.LoadFromAgente(Agente: TSQLAgenteClient);
begin
     Self.ReporteParametros.LoadFromAgente(Agente);
     Self.ReporteGrupos.LoadFromAgente(Agente);
     Self.ReporteColumnas.LoadFromAgente(Agente);
     Self.Expresiones.LoadFromAgente(Agente);   
end;

procedure TReporteData.SaveToXML( Writer: TdmXMLTools );
begin
     Self.ReporteParametros.SaveToXML(Writer);
     Self.ReporteGrupos.SaveToXML(Writer);
     Self.ReporteColumnas.SaveToXML(Writer);
     Self.Expresiones.SaveToXML(Writer);  
     {Checar si las columnas ya estan escribiendo las expresiones }
end;

{ ************* TCampoMasterList *********** }

constructor TCampoMasterList.Create;
begin
     FCampos := TList.Create;
end;

destructor TCampoMasterList.Destroy;
begin
     Clear;
     FreeAndNil( FCampos );
     inherited Destroy;
end;

function TCampoMasterList.Add( Item: TCampoMaster): Integer;
begin
     FCampos.Add( Item );
     Result := Count;
end;

procedure TCampoMasterList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Campo[ i ].Free;
     end;
end;

function TCampoMasterList.Count: Integer;
begin
     Result := FCampos.Count;
end;

function TCampoMasterList.GetCampo(Index: Integer): TCampoMaster;
begin
     Result := TCampoMaster( FCampos.Items[ Index ] );
end;

{ **************** TParametrosList ******************** }

procedure TParametrosList.AddFromXML(Reader: DXMLTools.TdmXMLTools; ParaEvaluar: Boolean = False);
var
   NodoParametros, NodoParametro: TZetaXMLNode;
   Parametro: TCampoMaster;
begin
     with Reader do
     begin
          NodoParametros := Reader.GetNode( TAG_PARAMETROS );
          if Assigned( NodoParametros ) then
          begin
               NodoParametro := Reader.GetNode( TAG_PARAMETRO, NodoParametros );
               while Assigned( NodoParametro ) do
               begin
                    Parametro := TCampoMaster.Create;
                    Parametro.Entidad := TipoEntidad( AttributeAsInteger( NodoParametro, TAG_ENTIDAD, Ord( Parametro.Entidad )));
                    Parametro.TipoCampo := eTipoGlobal( AttributeAsInteger( NodoParametro, TAG_TIPO, Ord( Parametro.TipoCampo ) ));
                    if ParaEvaluar then
                    begin
                         Parametro.Formula := AttributeAsString( NodoParametro, TAG_FORMULA, Parametro.Formula );
                    end
                    else
                    begin
                         case Parametro.TipoCampo of
                              tgBooleano: Parametro.Formula := BoolToFormula( ZetaCommonTools.zStrToBool( AttributeAsString( NodoParametro, TAG_VALOR, Parametro.Formula ) ) );
                              tgFecha: Parametro.Formula := FloatToStr( AttributeAsDate( NodoParametro, TAG_VALOR, NullDateTime ) );
                         else
                             Parametro.Formula := AttributeAsString( NodoParametro, TAG_VALOR, Parametro.Formula );
                         end;
                    end;
                    Parametro.Titulo := AttributeAsString( NodoParametro, TAG_DESCRIPCION, ZReportTools.SINTITULO );
                    Parametro.Calculado := AttributeAsInteger( NodoParametro, TAG_CALCULADO, Parametro.Calculado );
                    Self.Add( Parametro );
                    NodoParametro := Reader.GetNextSibling( NodoParametro );
               end;
          end;
     end;
end;

function TParametrosList.AsVariant: OleVariant;
const
     P_TITULO = 1;
     P_FORMULA = 2;
     P_TIPO = 3;
var
   ParametrosVar: OleVariant;
   ParametroVar: OleVariant;
   i: Integer;
begin
     ParametrosVar := VarArrayCreate( [ 1, K_MAX_PARAM ], varVariant );
     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               ParametroVar := VarArrayCreate( [ P_TITULO, P_TIPO ], varVariant );
               ParametroVar[ P_TITULO ] := Titulo;
               ParametroVar[ P_FORMULA ] := Formula;
               ParametroVar[ P_TIPO ] := TipoCampo;
          end;
          ParametrosVar[ i + 1 ] := ParametroVar;
     end;
     Result := ParametrosVar;
end;

procedure TParametrosList.SaveToXML( Writer: TdmXMLTools );
const
     TAG_TIPO_ELEMENTO = 'TIPOELEMENTO';
var
   i: Integer;
   sValor: String;
begin
     Writer.WriteStartElement( TAG_PARAMETROS );
     for i := 0 to ( Count - 1 ) do
     begin
          try
             case Campo[ i ].TipoCampo of
                  tgFecha: sValor := Writer.DateXML( StrToDate( Campo[ i ].Formula ), False );
                  tgBooleano: sValor := ZetaCommonTools.zBoolToStr(ZetaCommonTools.zStrToBool( Campo[ i ].Formula ));
             else
                 sValor := Campo[ i ].Formula;
             end;
          except
                on Error: Exception do
                begin
                     sValor := Format( 'Parametro "%s": %s', [ Campo[ i ].Titulo, Error.Message ] );
                end;
          end;
          Writer.WriteStartElement( TAG_PARAMETRO );
          Writer.WriteAttributeString( TAG_DESCRIPCION, Campo[ i ].Titulo );
          Writer.WriteAttributeString( TAG_FORMULA, sValor );
          Writer.WriteAttributeString( TAG_VALOR, sValor );
          Writer.WriteAttributeInteger( TAG_CALCULADO, Campo[ i ].Calculado );
          Writer.WriteAttributeInteger( TAG_TIPO, Ord( Campo[ i ].TipoCampo ) );
          Writer.WriteAttributeInteger( TAG_ENTIDAD, Ord( Campo[ i ].Entidad ));
          Writer.WriteEndElement;
     end;
     Writer.WriteEndElement;
end;

procedure TParametrosList.LoadFromAgente(Agente: TSQLAgenteClient);
var
   i: Integer;
   oColumna: TSQLColumna;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               if ( PosAgente >= 0 ) then
               begin
                    oColumna := Agente.GetColumna( PosAgente );
                    TipoCampo := oColumna.TipoFormula;
                    Formula := oColumna.Formula;
               end;
          end;
     end;
end;

{ ********* TCampoListadoList ********* }

constructor TCampoListadoList.Create( Lista: TStrings );
begin
     FCampos := Lista;
end;

destructor TCampoListadoList.Destroy;
begin
     {
     Clear;
     FreeAndNil( FCampos );
     }
     inherited Destroy;
end;

function TCampoListadoList.Add( Item: TCampoListado): Integer;
begin
     with FCampos do
     begin
          BeginUpdate;
          try
             AddObject( Item.Titulo, Item );
          finally
                 EndUpdate;
          end;
     end;
     Result := Count;
end;

procedure TCampoListadoList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Campo[ i ].Free;
     end;
     FCampos.Clear;
end;

function TCampoListadoList.Count: Integer;
begin
     Result := FCampos.Count;
end;

function TCampoListadoList.GetCampo(Index: Integer): TCampoListado;
begin
     Result := TCampoListado( FCampos.Objects[ Index ] );
end;

procedure TCampoListadoList.AddItemFromXML( Reader: TdmXMLTools; Nodo: TZetaXMLNode );
var
   Item: TCampoListado;
begin
     Item := CreateItemFromXML( Reader, Nodo );
     if Assigned( Item ) then
     begin
          Self.Add( Item );
     end;
end;

procedure TCampoListadoList.AddItemsToList( Lista: TStrings );
var
   i: Integer;
begin
     with Lista do
     begin
          Clear;
          BeginUpdate;
          try
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  AddObject( Self.Campo[ i ].Titulo, Self.Campo[ i ] );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TCampoListadoList.LoadFromAgente(Agente: TSQLAgenteClient);
var
   i: Integer;
   oColumna: TSQLColumna;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          with Campo[ i ] do
          begin
               if ( PosAgente >= 0 ) then
               begin
                    oColumna := Agente.GetColumna( PosAgente );
                    TipoCampo := oColumna.TipoFormula;
                    Formula := oColumna.Formula;
               end;
          end;
     end;
end;

{ *************** TColumnas ********************** }

procedure TColumnas.AddFromXML(Reader: TdmXMLTools);
var
   NodoPadre, Nodo: TZetaXMLNode;
begin
     with Reader do
     begin
          NodoPadre := GetNode( TAG_COLUMNAS );
          if Assigned( NodoPadre ) then
          begin
               Nodo := GetNode( TAG_COLUMNA, NodoPadre );
               while Assigned( Nodo ) do
               begin
                    AddItemFromXML( Reader, Nodo );
                    Nodo := GetNextSibling( Nodo );
               end;
          end;
     end;
end;

procedure TColumnas.SaveToXML( Writer: TdmXMLTools );
var
   i: Integer;
begin
     Writer.WriteStartElement( TAG_COLUMNAS );
     for i := 0 to ( Count - 1 ) do
     begin
          Writer.WriteStartElement( TAG_COLUMNA );
          SaveItemToXML( Writer, Campo[ i ] );
          Writer.WriteEndElement;
     end;
     Writer.WriteEndElement;
end;

{ ********* TGrupoOpcionesList ********* }

constructor TGrupoOpcionesList.Create( Lista: TStrings );
begin
     FGrupos := Lista;
end;

destructor TGrupoOpcionesList.Destroy;
begin
     {
     Clear;
     FreeAndNil( FGrupos );
     }
     inherited Destroy;
end;

function TGrupoOpcionesList.Add(Item: TGrupoOpciones): Integer;
begin
     FGrupos.AddObject( Item.Titulo, Item );
     Result := Count;
end;

procedure TGrupoOpcionesList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Grupo[ i ].Free;
     end;
     FGrupos.Clear;
end;

function TGrupoOpcionesList.Count: Integer;
begin
     Result := FGrupos.Count;
end;

function TGrupoOpcionesList.GetGrupo(Index: Integer): TGrupoOpciones;
begin
     Result := TGrupoOpciones( FGrupos.Objects[ Index ] );
end;

procedure TGrupoOpcionesList.AddFromXML(Reader: TdmXMLTools);
var
   NodoPadre, Nodo, NodoColumnas, NodoColumna: TZetaXMLNode;
   Item: TGrupoOpciones;
   Columna: TCampoListado;
begin
     with Reader do
     begin
          NodoPadre := Reader.GetNode( TAG_GRUPOS );
          if Assigned( NodoPadre ) then
          begin
               Nodo := Reader.GetNode( TAG_GRUPO, NodoPadre );
               while Assigned( Nodo ) do
               begin
                    Item := TGrupoOpciones.Create;
                    Item.Entidad := TipoEntidad( AttributeAsInteger( Nodo, TAG_ENTIDAD, Ord( Item.Entidad )));
                    Item.Formula := AttributeAsString( Nodo, TAG_FORMULA, Item.Formula );
                    Item.Titulo := AttributeAsString( Nodo, TAG_DESCRIPCION, ZReportTools.SINTITULO );
                    Item.Calculado := AttributeAsInteger( Nodo, TAG_CALCULADO, Item.Calculado );
                    Item.Encabezado := AttributeAsBoolean( Nodo, TAG_MOSTRAR_ENCABEZADO, Item.Encabezado );
                    Item.PieGrupo := AttributeAsBoolean( Nodo, TAG_MOSTRAR_PIE, Item.PieGrupo );
                    Item.SaltoPagina := AttributeAsBoolean( Nodo, TAG_SALTO_PAGINA, Item.SaltoPagina );
                    Item.Totalizar := AttributeAsBoolean( Nodo, TAG_TOTALIZAR, Item.Totalizar );
                    Item.Master := AttributeAsBoolean( Nodo, TAG_CENTRAR, Item.Master );
                    {
                    Item.TipoCampo := eTipoGlobal( AttributeAsInteger( Nodo, TAG_TIPO, Ord( Parametro.TipoCampo ) ));
                    }
                    Self.Add( Item );
                    NodoColumnas := Reader.GetNode( TAG_COLUMNAS_ENCABEZADO, Nodo );
                    if Assigned( NodoColumnas ) then
                    begin
                         NodoColumna := Reader.GetNode( TAG_COLUMNA, NodoColumnas );
                         while Assigned( NodoColumna ) do
                         begin
                              Columna := CreateItemFromXML( Reader, NodoColumna );
                              if Assigned( Columna ) then
                              begin
                                   Item.ListaEncabezado.AddObject(Columna.Titulo, Columna );
                              end;
                              NodoColumna := Reader.GetNextSibling( NodoColumna );
                         end;
                    end;
                    NodoColumnas := Reader.GetNode( TAG_COLUMNAS_PIE, Nodo );
                    if Assigned( NodoColumnas ) then
                    begin
                         NodoColumna := Reader.GetNode( TAG_COLUMNA, NodoColumnas );
                         while Assigned( NodoColumna ) do
                         begin
                              Columna := CreateItemFromXML( Reader, NodoColumna );
                              if Assigned( Columna ) then
                              begin
                                   Item.ListaPie.AddObject(Columna.Titulo, Columna );
                              end;
                              NodoColumna := Reader.GetNextSibling( NodoColumna );
                         end;
                    end;
                    Nodo := Reader.GetNextSibling( Nodo );
               end;
          end;
     end;
end;

procedure TGrupoOpcionesList.SaveToXML( Writer: TdmXMLTools );
var
   i:Integer;
begin
     Writer.WriteStartElement( TAG_GRUPOS  );
     for i := 0 to ( Count - 1 ) do
     begin
          SaveGrupoToXML ( Writer, Grupo[ i ] );
     end;
     Writer.WriteEndElement;
end;

procedure TGrupoOpcionesList.LoadFromAgente(Agente: TSQLAgenteClient);
var
   i, j: Integer;
   oColumna: TSQLColumna;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          with Grupo[ i ] do
          begin
               with ListaEncabezado do
               begin
                    for j := 0 to ( Count - 1 ) do
                    begin
                         with TCampoOpciones( Objects[ j ] ) do
                         begin
                              if ( PosAgente >= 0 ) then
                              begin
                                   oColumna := Agente.GetColumna( PosAgente );
                                   TipoCampo := oColumna.TipoFormula;
                                   Formula := oColumna.Formula;
                              end;
                         end;
                    end;
               end;
               with ListaPie do
               begin
                    for j := 0 to ( Count - 1 ) do
                    begin
                         with TCampoOpciones( Objects[ j ] ) do
                         begin
                              if ( PosAgente >= 0 ) then
                              begin
                                   oColumna := Agente.GetColumna( PosAgente );
                                   TipoCampo := oColumna.TipoFormula;
                                   Formula := oColumna.Formula;
                              end;
                         end;
                    end;
               end;
          end;
     end;
end;

{ ************ TOrdenOpcionesList *********** }

constructor TOrdenOpcionesList.Create( Lista: TStrings );
begin
     FOrdenes := Lista;
end;

destructor TOrdenOpcionesList.Destroy;
begin
     {
     Clear;
     FreeAndNil( FOrdenes );
     }
     inherited Destroy;
end;

function TOrdenOpcionesList.Add(Item: TOrdenOpciones): Integer;
begin
     FOrdenes.AddObject( Item.Titulo, Item );
     Result := Count;
end;

procedure TOrdenOpcionesList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Orden[ i ].Free;
     end;
     FOrdenes.Clear;
end;

function TOrdenOpcionesList.Count: Integer;
begin
     Result := FOrdenes.Count;
end;

function TOrdenOpcionesList.GetOrden(Index: Integer): TOrdenOpciones;
begin
     Result := TOrdenOpciones( FOrdenes.Objects[ Index ] );
end;

procedure TOrdenOpcionesList.AddFromXML(Reader: TdmXMLTools);
const
     TAG_ORDEN = 'ORDEN';
     TAG_ORDENES = 'ORDENES';
     TAG_ORIENTACION = 'ORIENTACION';
var
   NodoPadre, Nodo: TZetaXMLNode;
   Item: TOrdenOpciones;
begin
     with Reader do
     begin
          NodoPadre := Reader.GetNode( TAG_ORDENES );
          if Assigned( NodoPadre ) then
          begin
               Nodo := Reader.GetNode( TAG_ORDEN, NodoPadre );
               while Assigned( Nodo ) do
               begin
                    Item := TOrdenOpciones.Create;
                    Item.Entidad := TipoEntidad( AttributeAsInteger( Nodo, TAG_ENTIDAD, Ord( Item.Entidad )));
                    Item.Formula := AttributeAsString( Nodo, TAG_FORMULA, Item.Formula );
                    Item.Titulo := AttributeAsString( Nodo, TAG_DESCRIPCION, ZReportTools.SINTITULO );
                    Item.Calculado := AttributeAsInteger( Nodo, TAG_CALCULADO, Item.Calculado );
                    Item.DireccionOrden := AttributeAsInteger( Nodo, TAG_ORIENTACION, Item.DireccionOrden );
                    Self.Add( Item );
                    Nodo := Reader.GetNextSibling( Nodo );
               end;
          end;
     end;
end;

{ *************** TFiltroOpcionesList ************ }

constructor TFiltroOpcionesList.Create( Lista: TStrings );
begin
     FFiltros := Lista;
end;

destructor TFiltroOpcionesList.Destroy;
begin
     {
     Clear;
     FreeAndNil( FFiltros );
     }
     inherited Destroy;
end;

function TFiltroOpcionesList.Add(Item: TFiltroOpciones): Integer;
begin
     FFiltros.AddObject( Item.Titulo, Item );
     Result := Count;
end;

procedure TFiltroOpcionesList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Filtro[ i ].Free;
     end;
     FFiltros.Clear;
end;

function TFiltroOpcionesList.Count: Integer;
begin
     Result := FFiltros.Count;
end;

function TFiltroOpcionesList.GetFiltro(Index: Integer): TFiltroOpciones;
begin
     Result := TFiltroOpciones( FFiltros.Objects[ Index ] );
end;

procedure TFiltroOpcionesList.AddFromXML(Reader: TdmXMLTools; RootNode: TZetaXMLNode);
const
     TAG_FILTROS = 'LISTA';
     TAG_FILTRO = 'ELEMENTO';
     TAG_TIPO_FILTRO ='FILTROTIPO';
     TAG_RANGO_TIPO ='RANGOTIPO';
     TAG_RANGO_LISTA = 'RANGOLISTA';
     TAG_RANGO_INICIAL = 'RANGOINICIAL';
     TAG_RANGO_FINAL = 'RANGOFINAL';
     TAG_LOOKUP = 'LOOKUP';
     TAG_VALOR_ACTIVO = 'VALORACTIVO';
     TAG_VALOR = 'VALOR';
     TAG_APLICACION = 'APLICACION';
     TAG_CRITERIO = 'CRITERIO';
var
   NodoPadre, Nodo: TZetaXMLNode;
   Item: TFiltroOpciones;
   dInicial, dFinal: TDate;
begin
     with Reader do
     begin
          NodoPadre := Reader.GetNode( TAG_FILTROS, RootNode );
          if Assigned( NodoPadre ) then
          begin
               Nodo := Reader.GetNode( TAG_FILTRO, NodoPadre );
               while Assigned( Nodo ) do
               begin
                    Item := TFiltroOpciones.Create;
                    Item.Entidad := TipoEntidad( AttributeAsInteger( Nodo, TAG_ENTIDAD, Ord( Item.Entidad )));
                    Item.Formula := AttributeAsString( Nodo, TAG_FORMULA, Item.Formula );
                    Item.Titulo := AttributeAsString( Nodo, TAG_DESCRIPCION, ZReportTools.SINTITULO );
                    Item.Calculado := AttributeAsInteger( Nodo, TAG_CALCULADO, Item.Calculado );
                    Item.TipoRango := eTipoRango(AttributeAsInteger( Nodo, TAG_TIPO_FILTRO, Ord( Item.TipoRango )));
                    Item.TipoCampo := eTipoGlobal( AttributeAsInteger( Nodo, TAG_TIPO, Ord( Item.TipoCampo )));
                    Item.ValorActivo := eRDDValorActivo( AttributeAsInteger( Nodo, TAG_VALOR_ACTIVO, Ord( Item.ValorActivo )) );
                    Item.TipoRangoActivo := eTipoRangoActivo( AttributeAsInteger( Nodo, TAG_RANGO_TIPO, Ord( Item.TipoRangoActivo )));
                    //if ( StrVacio( Item.ValorActivo ) ) and ( Item.TipoRangoActivo = raActivo ) then
                    if ( Item.ValorActivo = vaSinValor ) and ( Item.TipoRangoActivo = raActivo ) then
                    begin
                         Item.TipoRangoActivo := raLista;
                    end;
                    Item.Numero := AttributeAsInteger( Nodo, TAG_LOOKUP, Item.Numero );
                    case Item.TipoRango of
                         rNinguno: Item.RangoInicial := AttributeAsString( Nodo, TAG_VALOR, Item.RangoInicial );
                         rFechas:
                         begin
                              Item.RangoFechas := eRangoFechas(AttributeAsInteger( Nodo, TAG_CRITERIO, Ord( Item.RangoFechas )));
                              ZReportTools.GetFechasRango( Item.RangoFechas, dInicial, dFinal );
                              Item.FechaInicial := dInicial;
                              Item.FechaFinal := dFinal;
                         end;
                         rBool: Item.Posicion := AttributeAsInteger( Nodo, TAG_APLICACION, Item.Posicion );
                    else
                        begin
                             case Item.TipoRangoActivo of
                                  raRango:
                                  begin
                                       Item.RangoInicial := AttributeAsString( Nodo, TAG_RANGO_INICIAL, Item.RangoInicial );
                                       Item.RangoFinal := AttributeAsString( Nodo, TAG_RANGO_FINAL, Item.RangoFinal );
                                  end;
                                  raLista: Item.Lista := AttributeAsString( Nodo, TAG_RANGO_LISTA, Item.Lista );
                             end;
                        end;
                    end;
                    Self.Add( Item );
                    Nodo := Reader.GetNextSibling( Nodo );
               end;
          end;
     end;
end;

{ TExpresion }

constructor TExpresion.Create;
begin

end;

destructor TExpresion.Destroy;
begin

  inherited;
end;

procedure TExpresion.SaveToXML(Writer: TdmXMLTools);
begin
     //WriteStartElement(TAG_EXPRESION);
     //WriteAttributeString( TAG_DESCRIPCION, Grupo.Titulo );
end;

{/************************** TExpresionesList /*********************************}

function TExpresionesList.Add(Item: TExpresion): Integer;
begin
     FExpresiones.AddObject( Item.Campo, Item );
     Result := Count;
end;

procedure TExpresionesList.AddFromXML(Reader: TdmXMLTools);
const
     TAG_EXPRESION = 'EXPRESION';
     TAG_EXPRESIONES = 'EXPRESIONES';
     TAG_POSICION = 'POSICION';
     TAG_BANDA = 'BANDA';
var
   NodoPadre, Nodo: TZetaXMLNode;
   Item: TExpresion;
begin
     with Reader do
     begin
          NodoPadre := Reader.GetNode( TAG_EXPRESIONES );
          if Assigned( NodoPadre ) then
          begin
               Nodo := Reader.GetNode( TAG_EXPRESION, NodoPadre );
               while Assigned( Nodo ) do
               begin
                    Item := TExpresion.Create;
                    Item.Posicion := AttributeAsInteger( Nodo, TAG_POSICION, Item.Posicion );
                    Item.Formula := AttributeAsString( Nodo, TAG_FORMULA, Item.Formula );
                    Item.Banda := AttributeAsString( Nodo, TAG_BANDA, Item.Banda );
                    Item.Campo := AttributeAsString( Nodo, TAG_CAMPO, Item.Campo );

                    Self.Add( Item );
                    Nodo := Reader.GetNextSibling( Nodo );
               end;
          end;
     end;
end;

procedure TExpresionesList.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
     begin
          Self.Expresion[ i ].Free;
     end;
     FExpresiones.Clear;

end;

function TExpresionesList.Count: Integer;
begin
      Result := FExpresiones.Count;
end;

constructor TExpresionesList.Create( Lista:TStrings );
begin
     FExpresiones := Lista;
end;

destructor TExpresionesList.Destroy;
begin

  inherited;
end;

function TExpresionesList.GetExpresion(Index: Integer): TExpresion;
begin
      Result := TExpresion( FExpresiones.Objects[ Index ] );
end;

procedure TExpresionesList.LoadFromAgente(Agente: TSQLAgenteClient);
var
   i: Integer;
   oColumna: TSQLColumna;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          with Expresion[ i ] do
          begin
               if ( PosAgente >= 0 ) then
               begin
                    oColumna := Agente.GetColumna( PosAgente );
                    Formula := oColumna.Formula;
                    Campo := oColumna.Alias;
               end;
          end;
     end;
end;

procedure TExpresionesList.SaveToXML(Writer: TdmXMLTools);
CONST
     TAG_EXPRESION = 'EXPRESION';
     TAG_EXPRESIONES = 'EXPRESIONES';
     TAG_POSICION = 'POSICION';
     TAG_BANDA = 'BANDA';
var
     i:Integer;
begin
      Writer.WriteStartElement( TAG_EXPRESIONES );
     for i := 0 to ( Count - 1 ) do
     begin
          Writer.WriteStartElement( TAG_EXPRESION );
          Writer.WriteAttributeString( TAG_FORMULA, Expresion[ i ].Formula );
          Writer.WriteAttributeInteger( TAG_POSICION, Expresion[ i ].Posicion );
          Writer.WriteAttributeString( TAG_BANDA, Expresion[ i ].Banda );
          Writer.WriteAttributeString( TAG_CAMPO, Expresion[ i ].Campo );
          Writer.WriteEndElement;
     end;
     Writer.WriteEndElement;
end;

end.
