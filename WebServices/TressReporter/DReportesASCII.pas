unit DReportesASCII;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
     Dialogs, DB, DBClient,
     DReportesWebBase,
     ZetaCommonLists,
     ZetaAsciiFile,
     ZetaClientDataSet;

type
  TdmReportesASCII = class(TdmReportWebGeneratorBase)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FDatos: TStrings;
    FResultado: TZetaClientDataSet;
    function GetOutput: string;
  protected
    { Protected declarations }
    property Resultado: TZetaClientDataSet read FResultado write FResultado;
    procedure DoOnGetReportes(const lResultado: Boolean); override;
    procedure DoOnGetResultado(const lResultado: Boolean; const Error: string); override;
  public
    { Public declarations }
    property Datos: TStrings read FDatos;
    property Output: string read GetOutput;
    function Procesar: Integer;
  end;

implementation

uses DCliente,
     DGlobal,
     DDiccionario,
     FAutoClasses,
     ZAccesosMgr,
     ZetaDialogo,
     ZFuncsCliente,     
     ZReportTools,
     ZReportConst,
     ZReporteASCII,
     ZetaCommonClasses,
     ZetaCommonTools;

{$R *.dfm}

procedure TdmReportesASCII.DataModuleCreate(Sender: TObject);
begin
     FDatos := TStringList.Create;
     inherited;
end;

procedure TdmReportesASCII.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FDatos );
     inherited;
end;

procedure TdmReportesASCII.DoOnGetReportes( const lResultado: Boolean );
begin
     if lResultado then
     begin
          with cdsReporte do
          begin
               Self.ReporteInfo.Nombre := FieldByName( 'RE_NOMBRE' ).AsString;
               if (eTipoReporte( FieldByName( 'RE_TIPO' ).AsInteger ) in [ trListado ] ) then
               begin
                    Self.GetDatosImpresion;
               end
               else
               begin
                    raise Exception.Create( 'Solamente se Permite Obtener Listados' );
               end;
          end;
     end;
end;

procedure TdmReportesASCII.DoOnGetResultado( const lResultado: Boolean; const Error: string );
var
   FReporteAscii: TReporteAscii;
begin
     with Self.cdsResultados do
     begin
          if lResultado and Active and not IsEmpty then
          begin
               First;
               ModificaListas;
               ZFuncsCliente.RegistraFuncionesCliente;
               //Asignacion de Parametros del Reporte, para poder evaluar la funcion PARAM() en el Cliente
               ZFuncsCliente.ParametrosReporte := Self.SQLAgente.Parametros;
               FReporteAscii := TReporteAscii.Create;
               try
                  with FReporteAscii do
                  begin
                       Stream := Self.Datos;
                       GeneraAscii( Self.cdsResultados, Self.DatosImpresion, Self.Campos, Self.Grupos, Self.ReporteInfo.SoloTotales, False );
                  end;
               finally
                      FreeAndNil( FReporteAscii );
               end;
          end;
     end;
end;

function TdmReportesASCII.GetOutput: string;
var
   i: Integer;
begin
     Result := '';
     for i := 0 to ( Self.Datos.Count - 1 ) do
     begin
          Result := Result + Self.Datos[ i ] + CR_LF;
     end;
end;

function TdmReportesASCII.Procesar: Integer;
begin
     Datos.Clear;
     Global.Conectar;
     Empresa := dmCliente.ValoresActivos.EmpresaActiva;
     //if GetResultado( ReporteInfo.Codigo, TRUE ) then
     if GenerarReporte( True ) then
     begin
          Result := cdsResultados.RecordCount;
     end
     else
     begin
          Result := 0;
     end;
end;

end.
