unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Controls,
     Db, DBClient,{$ifndef VER130}Variants,{$endif}
     {$ifdef DOS_CAPAS}
     DServerRecursos,
     DServerAsistencia,
     DServerNomina,
     DServerCalcNomina,
     DServerCatalogos,
     DServerSuper,
     DZetaServerProvider,
     DServerConsultas,
     ZetaSQLBroker,
     DServerTablas,
     {$else}
     Recursos_TLB,
     Asistencia_TLB,
     Catalogos_TLB,
     Nomina_TLB,
     DCalcNomina_TLB,
     Consultas_TLB,
     Tablas_TLB,
     Super_TLB,
     {$endif}
     DBasicoCliente,
     DXMLTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

type
  TPeriodo = class(TObject)
  private
    { Private declarations }
     _Anio:Integer;
     _Tipo:Integer;
     _Numero:Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Anio: Integer read _Anio write _Anio;
    property Tipo: Integer read _Tipo write _Tipo;
    property Numero: Integer read _Numero write _Numero;
    constructor Create;
  end;

  TProcesaNodoData = procedure( oNodo: TZetaXMLNode; oDataSet: TZetaClientDataSet ) of object;

  TdmCliente = class(TBasicoCliente)
    cdsLista: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FLogHayError: Boolean;
    FXMLTools: TdmXMLTools;
    FXMLResult: TdmXMLTools;
    FStatus: String;
    FProcesaNodoData: TProcesaNodoData;
    {$ifdef DOS_CAPAS}
    function GetServerRecursos: TdmServerRecursos;
    function GetServerAsistencia: TdmServerAsistencia;
    function GetServerCatalogos: TdmServerCatalogos;
    function GetServerNomina: TdmServerNomina;
    function GetServerCalcNomina: TdmServerCalcNomina;
    function GetServerConsultas: TdmServerConsultas;
    function GetServerTablas: TdmServerTablas;
    function GetServerSupervisores: TdmServerSuper;
    {$else}
    FServerRecursos: IdmServerRecursosDisp;
    FServerAsistencia: IdmServerAsistenciaDisp;
    FServerCatalogos: IdmServerCatalogosDisp;
    FServerNomina: IdmServerNominaDisp;
    FServerCalcNomina: IdmServerCalcNominaDisp;
    FServerConsultas: IdmServerConsultasDisp;
    FServerTablas :IdmServerTablasDisp;
    FServerSupervisores: IdmServerSuperDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp;
    function GetServerNomina: IdmServerNominaDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    function GetServerTablas: IdmServerTablasDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerSupervisores: IdmServerSuperDisp;
    {$endif}
  protected
    { Protected declarations }
    property LogHayError: Boolean read FLogHayError;
    property XMLTools: TdmXMLTools read FXMLTools;
    property XMLResult: TdmXMLTools read FXMLResult;
    property Status: String read FStatus write FStatus;
    property ProcesaNodoData: TProcesaNodoData read FProcesaNodoData write FProcesaNodoData;
    {$ifdef DOS_CAPAS}
    property ServerRecursos: TdmServerRecursos read GetServerRecursos;
    property ServerAsistencia: TdmServerAsistencia read GetServerAsistencia;
    property ServerCatalogos: TdmServerCatalogos read GetServerCatalogos;
    property ServerNomina: TdmServerNomina read GetServerNomina;
    property ServerCalcNomina: TdmServerCalcNomina read GetServerCalcNomina;
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
    property ServerTablas: TdmServerTablas read GetServerTablas;
    property ServerSupervisores: TdmServerSuper read GetServerSupervisores;
    {$else}
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
    property ServerCatalogos: IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerNomina: IdmServerNominaDisp read GetServerNomina;
    property ServerCalcNomina: IdmServerCalcNominaDisp read GetServerCalcNomina;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerTablas: IdmServerTablasDisp read GetServerTablas;
    property ServerSupervisores: IdmServerSuperDisp read GetServerSupervisores;
    {$endif}
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    function ConectaEmpresa(const sXML: String; lConectaCOM : Boolean = TRUE): Boolean;
    function GetNumEmpleado(const sXML: String ;const sNodoPadre: String): Integer;
    function GetFechaRegTarjeta(const sXML: String ;const sNodoPadre: String): TDate;
    function GetPeriodo (const sXML: String;const sNodoPadre: String ):TPeriodo;
    function GetOperacion():string;
    function GetFechaKardex():TDate;
    function GetTipoKardex():string;
    function GetFechaFromXml(nodoHijo:string;nodoPadre:string):TDate;
    function LogAsText: String;
    function FetchEmployee( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
    procedure ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure UpdateNodeInfo( oDataSet: TZetaClientDataSet; const sField: String );
    procedure TransfiereXMLCampo( oNodo: TZetaXMLNode; oDataSet: TZetaClientDataSet );
    procedure TransfiereXMLDataSet( const sTabla: String; oDataSet: TZetaClientDataSet );
    procedure LogInit;
    procedure LogEnd;
    procedure LogInfo( const sTexto: String; const iEmpleado: Integer = 0 );
    procedure LogError( const sTexto: String; const iEmpleado: Integer = 0 );
    procedure LlenaValoresActivos( Parametros: TZetaParams );
    procedure DataSetToXML( const sTabla: String; oDataSet: TZetaClientDataSet );

  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function GetGrupoActivo : Integer; override;
  end;

implementation

uses FAutoClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaRegistryCliente;

const
     K_TAG_DATOS = 'DATOS';
     XML_UTF_8_ENCODING = 'UTF-8';

{$R *.DFM}

{ ********* TdmCliente ********** }

constructor TdmCliente.Create(AOwner: TComponent);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     FLogHayError := False;
     inherited Create( AOwner );
end;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     FXMLTools := TdmXMLTools.Create( nil );
     FXMLResult := TdmXMLTools.Create( nil );
     {$ifdef DOS_CAPAS}
     FServerRecursos := TdmServerRecursos.Create( Self );
     FServerAsistencia := TdmServerAsistencia.Create( self );
     FServerCatalogos := TdmServerCatalogos.Create( self );
     FServerNomina := TdmServerNomina.Create( self );
     FServerCalcNomina := TdmServerCalcNomina.Create( self );
     {$endif}
     inherited;
end;

destructor TdmCliente.Destroy;
begin
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FXMLTools );
     FreeAndNil( FXMLResult );
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerRecursos );
     FreeAndNil( FServerAsistencia );
     FreeAndNil( FServerCatalogos );
     FreeAndNil( FServerNomina );
     FreeAndNil( FServerCalcNomina );
     FreeAndNil( FServerTablas );
     {$endif}
end;

{$ifdef DOS_CAPAS}
function TdmCliente.GetServerRecursos: TdmServerRecursos;
begin
     Result:= FServerRecursos;
end;
function TdmCliente.GetServerAsistencia: TdmServerAsistencia;
begin
     Result:= FServerAsistencia;
end;
function TdmCliente.GetServerCatalogos: TdmServerCatalogos;
begin
     Result:= FServerCatalogos;
end;
function TdmCliente.ServerNomina: TdmServerNomina;
begin
     Result:= FServerNomina;
end;
function TdmCliente.ServerCalcNomina: TdmServerCalcNomina;
begin
     Result:= FServerCalcNomina;
end;
function TdmCliente.GetServerConsultas: TdmServerConsultas;
begin
     Result:= FServerConsultas;
end;
function TdmCliente.GetServerTablas: TdmServerTablas;
begin
     Result:= FServerTablas;
end;
function TdmCliente.GetServerSupervisores: TdmServerSuper;
begin
     Result:= FServerSupervisores;
end;
{$else}
function TdmCliente.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result:= IdmServerRecursosDisp( Self.CreaServidor( CLASS_dmServerRecursos, FServerRecursos ) );
end;
function TdmCliente.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result:= IdmServerAsistenciaDisp( Self.CreaServidor( CLASS_dmServerAsistencia, FServerAsistencia ) );
end;
function TdmCliente.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result:= IdmServerCatalogosDisp( Self.CreaServidor( CLASS_dmServerCatalogos, FServerCatalogos ) );
end;
function TdmCliente.GetServerNomina: IdmServerNominaDisp;
begin
      Result:= IdmServerNominaDisp( Self.CreaServidor( CLASS_dmServerNomina, FServerNomina ) );
end;
function TdmCliente.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result:= IdmServerCalcNominaDisp( Self.CreaServidor( CLASS_dmServerCalcNomina, FServerCalcNomina ) );
end;
function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result:= IdmServerConsultasDisp( Self.CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;
function TdmCliente.GetServerTablas: IdmServerTablasDisp;
begin
     Result:= IdmServerTablasDisp( Self.CreaServidor( CLASS_dmServerTablas, FServerTablas ) );
end;
function TdmCliente.GetServerSupervisores: IdmServerSuperDisp;
begin
     Result:= IdmServerSuperDisp( Self.CreaServidor( CLASS_dmServerSuper, FServerSupervisores ) );
end;
{$endif}

{ ****** Manejos Internos ****** }

function TdmCliente.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmCliente.SetEmpresaActiva(const sCodigo: String): Boolean;
begin
     InitCompanies;
     Result := FindCompany( sCodigo );
     if Result then
     begin
          SetCompany;
     end;
end;

procedure TdmCliente.ReconcileError(DataSet: {$ifdef VER130}TClientDataSet{$else}TCustomClientDataSet{$endif}; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := raAbort;  // Aborta el reconcile para que regrese FALSE
     Status := E.Message;
end;

function TdmCliente.FetchEmployee( oDataSet: TZetaClientDataSet; const iEmpleado: TNumEmp ): Boolean;
var
   oDatos: OleVariant;
begin
     Result := Servidor.GetEmpleado( Empresa, iEmpleado, oDatos );
     if Result then
     begin
          with oDataSet do
          begin
               Data := oDatos;
          end;
     end;
end;

procedure TdmCliente.UpdateNodeInfo( oDataSet: TZetaClientDataSet; const sField: String );
{ Si el XML no incluye el nodo no se requiere procesar nada }
var
   oNodo: TZetaXMLNode;
   oCampo: TField;
begin
     oNodo := FXMLTools.GetNode( sField );
     if Assigned( oNodo ) then
     begin
          with oDataSet do
          begin
               oCampo := FindField( sField );
               if Assigned( oCampo ) then
               begin
                    if not ( state in [ dsEdit, dsInsert ] ) then
                       Edit;
                    case oCampo.DataType of
                         ftInteger, ftSmallInt, ftWord : oCampo.AsInteger := FXMLTools.TagAsInteger( oNodo, 0 );
                         ftFloat, ftCurrency, ftBCD : oCampo.AsFloat := FXMLTools.TagAsFloat( oNodo, 0 );
                         ftDate, ftTime, ftDateTime : oCampo.AsDateTime := FXMLTools.TagAsDate( oNodo, NullDateTime );
                    else
                         oCampo.AsString := FXMLTools.TagAsString( oNodo, VACIO );
                    end;
               end
               else
               begin
                    DataBaseError( 'No se encontr� el campo: ' + sField );
               end;
          end;
     end;
end;

procedure TdmCliente.LogInit;
begin
     FLogHayError := False;
     {
     GA: El encoding se fija a nivel interfase de
     webservice, en los componentes de SOAP
     }
     {$ifdef FALSE}
     FXMLResult.XMLEncoding := XML_UTF_8_ENCODING;
     {$endif}
     FXMLResult.XMLInit( K_TAG_DATOS );
end;

procedure TdmCliente.LogEnd;
begin
     FXMLResult.XMLEnd;
end;

procedure TdmCliente.LogInfo( const sTexto: String; const iEmpleado: Integer = 0 );
const
     K_INFO = 'INFO';
var
   sInfo: String;
begin
     sInfo := sTexto;
     if ( iEmpleado > 0 ) then
        sInfo:= Format( 'Empleado #%d = %s' + CR_LF, [ iEmpleado, sInfo ] );
     FXMLResult.WriteValueString( K_INFO, sInfo );
end;

procedure TdmCliente.LogError( const sTexto: String; const iEmpleado: Integer = 0 );
begin
     with FXMLResult do
     begin
          if ( iEmpleado > 0 ) then
             XMLError( Format( 'Empleado #%d = %s' + CR_LF, [ iEmpleado, sTexto ] ) )
          else
              XMLError( sTexto );
     end;
     FLogHayError := true;
end;

function TdmCliente.LogAsText: String;
begin
     with FXMLResult do
     begin
          {$ifdef FALSE}
          if ( XMLEncoding = XML_UTF_8_ENCODING ) then
          begin
               Result := ZetaCommonTools.QuitaAcentos( XMLAsText );
          end
          else
          begin
               Result := XMLAsText;
          end;
          {$else}
          Result := XMLAsText;
          {$endif}
     end;
end;

{ ***** Cargado de XML y conexi�n de empresa }

function TdmCliente.ConectaEmpresa(const sXML: String; lConectaCOM : Boolean): Boolean;
var
   sEmpresa, sDBServer, sDBUser, sDBPassword: String;
   iUsuario: Integer;
   Empresa: TZetaXMLNode;
begin
     Result := FALSE;
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
             Empresa := GetNode( 'EMPRESA' );
             sEmpresa := TagAsString( Empresa, '' );
             sDBServer := FXMLTools.AttributeAsString( Empresa, 'Database', '' );
             sDBUser := FXMLTools.AttributeAsString( Empresa, 'Username', '' );
             sDBPassword := FXMLTools.AttributeAsString( Empresa, 'Password', '' );
             iUsuario := TagAsInteger( GetNode( 'USUARIO' ), 0 );
        end;
        if ZetaCommonTools.StrVacio( sEmpresa ) then
        begin
             LogError( 'Empresa No Especificada' );
        end
        else
        begin
             if ( iUsuario = 0 ) then
             begin
                  LogError( 'Usuario no especificado' );
             end
             else
             begin
                  Usuario := 0;    // Asigna el usuario activo a dmCliente en CERO  - para obtener TODAS  las empresas
                  if ( ZetaCommonTools.StrLleno( sDBServer ) and ZetaCommonTools.StrLleno( sDBUser ) and ZetaCommonTools.StrLleno( sDBPassword ) ) then
                  begin
                       Result := False;
                       if (lConectaCOM )then
                       begin
                           InitCompanies;
                           with cdsCompany do
                           begin
                                LogChanges := False;
                                EmptyDataset;
                                Insert;
                                FieldByName( 'CM_CODIGO' ).AsString := sEmpresa;
                                FieldByName( 'CM_DATOS' ).AsString := sDBServer;
                                FieldByName( 'CM_USRNAME' ).AsString := sDBUser;
                                FieldByName( 'CM_PASSWRD' ).AsString := sDBPassword;
                                FieldByName( 'CM_NIVEL0' ).AsString := '';
                                Post;
                           end;
                           SetCompany;
                           Result := True;
                      end
                      else
                      begin
                           SetCompanySinCOM( sEmpresa, sDBServer, sDBUser, sDBPassword );
                           Result := TRUE;
                      end;
                  end
                  else
                  begin
                       Result := SetEmpresaActiva( sEmpresa );
                  end;
                  Usuario := iUsuario;
                  if ( not Result ) then
                  begin
                       LogError( Format( 'Empresa %s No Existe', [ sEmpresa ] ) );
                  end;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
end;
{
    Obtiene el numero del empleado del Xml del nodo Padre : ie.PERIODO
    Busca el tags de CB_CODIGO como entero
    Se considera 0 como valor vac�o
}

function TdmCliente.GetNumEmpleado(const sXML: String;const sNodoPadre: String ): Integer;
var
   iEmpleado: Integer;
begin
     iEmpleado := 0;
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
             iEmpleado := TagAsInteger(GetNode('CB_CODIGO',GetNode(sNodoPadre)),0);
        end;
        if ( iEmpleado = 0 ) then
           LogError( 'N�mero de empleado no especificado' );
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := iEmpleado;
end;

{
    Obtiene la fecha  Padre : ie.TARJETA
    Busca los tags de AU_FECHA como Date
    Se considera NullDateTime como valor vac�o
}

function TdmCliente.GetFechaRegTarjeta(const sXML: String;const sNodoPadre: String ): TDate;
var
   dFecha: TDate;
begin
     dFecha := NullDateTime;
     try
        with FXMLTools do
        begin
             XMLAsText := sXML;
             dFecha := TagAsDateTime(GetNode('AU_FECHA',GetNode(sNodoPadre)),0);
        end;
        if ( dFecha = NullDateTime ) then
           LogError( 'Fecha de Registro no especificada' );
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := dFecha;
end;


procedure TdmCliente.TransfiereXMLCampo( oNodo: TZetaXMLNode; oDataSet: TZetaClientDataSet );
var
   sField: String;
   oCampo: TField;
begin
     sField := oNodo.NodeName;
     with oDataSet do
     begin
          oCampo := FindField( sField );
          if Assigned( oCampo ) then
          begin
               if not ( state in [ dsEdit, dsInsert ] ) then
                  Edit;
               case oCampo.DataType of
                    ftInteger, ftSmallInt, ftWord : oCampo.AsInteger := FXMLTools.TagAsInteger( oNodo, 0 );
                    ftFloat, ftCurrency, ftBCD : oCampo.AsFloat := FXMLTools.TagAsFloat( oNodo, 0 );
                    ftDate, ftTime, ftDateTime : oCampo.AsDateTime := FXMLTools.TagAsDate( oNodo, NullDateTime );
               else
                    oCampo.AsString := FXMLTools.TagAsString( oNodo, VACIO );
               end;
          end
          else if Assigned( FProcesaNodoData ) then
          begin
               FProcesaNodoData( oNodo, oDataSet );
          end
          else
          begin
               LogError( 'No se encontr� el campo: ' + sField );
          end;
     end;
end;

procedure TdmCliente.TransfiereXMLDataSet( const sTabla: String; oDataSet: TZetaClientDataSet );
var
   oNodo, oItem: TZetaXMLNode;
begin
     with FXMLTools do
     begin
          oNodo := GetNode( sTabla );
          if Assigned( oNodo ) and HasChildren( oNodo ) then               // Viene seccion de la tabla
          begin
               oItem := GetFirstChild( oNodo );
               while Assigned( oItem ) do
               begin
                    TransfiereXMLCampo( oItem, oDataSet );
                    oItem := GetNextSibling( oItem );
               end;
          end;
     end;
end;

procedure TdmCliente.DataSetToXML( const sTabla: String; oDataSet: TZetaClientDataSet );
begin
     with FXMLTools do
     begin
          XMLBuildDataRow(oDataSet,sTabla);
     end;
end;


{
    Obtiene el periodo del Xml del nodo Padre : ie.PERIODO
    Busca los tags de PE_YEAR,PE_TIPO,PE_NUMERO como enteros cada uno
    Se considera 0 como valor vac�o
}

function TdmCliente.GetPeriodo (const sXML: String;const sNodoPadre: String ):TPeriodo;
var
   Periodo:TPeriodo;
begin
     Periodo := TPeriodo.Create();
     try
        with Periodo do
        begin
             with FXMLTools do
             begin
                  XMLAsText := sXML;
                  Anio  := TagAsInteger(GetNode('PE_YEAR',GetNode(sNodoPadre)),0);
                  Tipo := TagAsInteger(GetNode('PE_TIPO',GetNode(sNodoPadre)),0);
                  Numero := TagAsInteger(GetNode('PE_NUMERO',GetNode(sNodoPadre)),0);
             end;

             if ( Anio = 0 ) then
                LogError( 'A�o de periodo no especificado' );
             if ( Tipo = 0 ) then
                LogError( 'Tipo de periodo no especificado' );
             if ( Numero = 0 ) then
                LogError( 'N�mero de periodo no especificado' );
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := Periodo;
end;

function TdmCliente.GetFechaKardex():TDate;
var
   dFecha:TDate;
begin
     dFecha := NullDateTime;
     try
        with FXMLTools do
        begin
              dFecha := TagAsDate(GetNode('CB_FECHA',GetNode('KARDEX')),0);
              if dFecha = 0 then
                 LogError( 'Fecha de kardex no especificada' );
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := dFecha;
end;

function TdmCliente.GetOperacion():string;
var
   sOperacion:string;
begin
     try
        with FXMLTools do
        begin
              sOperacion := TagAsString(GetNode('OPERACION'),'');
              if sOperacion = VACIO then
                 LogError( 'Tipo de operaci�n no especificada' );
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := sOperacion;
end;

function TdmCliente.GetTipoKardex():string;
var
   sTipoKar:string;
begin
     try
        with FXMLTools do
        begin
              sTipoKar := TagAsString(GetNode('CB_TIPO',GetNode('KARDEX')),'');
              if sTipoKar = VACIO then
                 LogError( 'Tipo de kardex no especificado' );
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := sTipoKar;
end;

function TdmCliente.GetFechaFromXml(nodoHijo:string;nodoPadre:string):TDate;
var
   Fecha:TDate;
begin
     Fecha := NullDateTime;
     try
        with FXMLTools do
        begin
              Fecha := TagAsDate(GetNode(nodoHijo,GetNode(nodoPadre)),NullDateTime);
              if Fecha = NullDateTime then
                 LogError( 'Fecha no especificada' );
        end;
     except
           on Error: Exception do
           begin
                LogError( 'Error Al Examinar Documento: ' + Error.Message );
           end;
     end;
     Result := Fecha;
end;

procedure TdmCliente.LlenaValoresActivos( Parametros: TZetaParams );
const
     NODO_VALORESACTIVOS = 'VALORESACTIVOS';
var
   oNodo, oItem: TZetaXMLNode;
begin
     with XMLTools do
     begin
          oNodo := GetNode( NODO_VALORESACTIVOS );
          if Assigned( oNodo ) then               // Viene seccion de valores activos
          begin
               with Parametros do
               begin
                    // Activos Sistema
                    AddDate( 'FechaAsistencia', TagAsDate( GetNode( 'FECHAASISTENCIA', oNodo ), NullDateTime ) );
                    AddDate( 'FechaDefault', TagAsDate( GetNode( 'FECHADEFAULT', oNodo ), NullDateTime ) );
                    AddInteger( 'YearDefault', TagAsInteger( GetNode( 'YEAR', oNodo ), 0 ) );
                    AddInteger( 'EmpleadoActivo', TagAsInteger( GetNode( 'EMPLEADO', oNodo ), 0 ) );
                    AddString( 'NombreUsuario', VACIO );
                    AddString( 'CodigoEmpresa', VACIO );
                    // Activos de Periodo
                    oItem := GetNode( 'PERIODO', oNodo );
                    AddInteger( 'Year', TagAsInteger( GetNode( 'YEAR', oItem ), 0 ) );
                    AddInteger( 'Tipo', TagAsInteger( GetNode( 'TIPO', oItem ), 0 ) );
                    AddInteger( 'Numero', TagAsInteger( GetNode( 'NUMERO', oItem ), 0 ) );
                    // Activos de IMSS
                    oItem := GetNode( 'IMSS', oNodo );
                    AddString( 'RegistroPatronal', TagAsString( GetNode( 'PATRON', oItem ), VACIO ) );
                    AddInteger( 'IMSSYear', TagAsInteger( GetNode( 'YEAR', oItem ), 0 ) );
                    AddInteger( 'IMSSMes', TagAsInteger( GetNode( 'TIPO', oItem ), 0 ) );
                    AddInteger( 'IMSSTipo', TagAsInteger( GetNode( 'NUMERO', oItem ), 0 ) );
               end;
          end;
     end;
end;

function TdmCliente.GetGrupoActivo : Integer;
begin
     result := D_GRUPO_SIN_RESTRICCION;
end;

//************ TPeriodo ****************//

constructor TPeriodo.Create();
begin
     Anio := 0;
     Tipo := 0;
     Numero := 0;
end;


end.

