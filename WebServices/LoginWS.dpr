library LoginWS;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ActiveX,
  ComObj,
  WebBroker,
  ISAPIThreadPool,
  ISAPIApp,
  LoginWSImpl in 'LoginWSImpl.pas',
  LoginWSIntf in 'LoginWSIntf.pas',
  DLoginWS in 'DLoginWS.pas' {dmLoginWS: TWebModule},
  DClienteLogin in 'DClienteLogin.pas' {dmClienteLogin: TDataModule};

{$R *.res}

exports
  GetExtensionVersion,
  HttpExtensionProc,
  TerminateExtension;

begin
  CoInitFlags := COINIT_MULTITHREADED;
  Application.Initialize;
  Application.CreateForm(TdmLoginWS, dmLoginWS);
  Application.Run;
end.
