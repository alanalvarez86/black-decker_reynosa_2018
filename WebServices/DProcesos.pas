unit DProcesos;

interface

uses Windows, Messages, SysUtils, Variants, Classes, Controls, DB, DBClient,
     Nomina_TLB,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     DTablas,
     DCatalogos,
     DCliente,
     DXMLTools,
     DServerCalcNomina;

type
  TdmProcesos = class(TdmCliente)
    cdsAjusteColectivo: TZetaClientDataSet;
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsAjusteColectivoAlCrearCampos(Sender: TObject);
  private
    { Private declarations }
    FdmTablas: TdmTablas;
    FdmCatalogos: TdmCatalogos;
    function GetCondicion( oNodo: TZetaXMLNode ): String;
    function GetFiltro( oNodo: TZetaXMLNode ): String;
    function GetRango( oNodo: TZetaXMLNode; AliasEmpleado: String = ARROBA_TABLA ): String;
    procedure InitTablas;
    procedure InitCatalogos;
    procedure AgregaParametro( oNodo: TZetaXMLNode; Parametros: TZetaParams );
    procedure AgregaResultadoWizard( const eProceso: Procesos; const oResultado: OleVariant );
    procedure LlenaFiltros( Parametros: TZetaParams; AliasEmpleado: String = ARROBA_TABLA );
    procedure LlenaParametros( Parametros: TZetaParams );
  protected
    { Protected declarations }
    property dmTablas: TdmTablas read FdmTablas;
    property dmCatalogos: TdmCatalogos Read FdmCatalogos;
    procedure AjusteColectivoListaGetDataset(const Parametros: String);
  public
    { Public declarations }
    function ProcesaWizard( const eProceso: Procesos; const Parametros: String ): String;
    function AjusteColectivoLista( const Parametros: String ): String;
    function AjusteColectivoEscribir( const Parametros: String ): String;
  end;

implementation

{$R *.dfm}

uses ZetaClientTools,
     ZetaCommonTools;

procedure TdmProcesos.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     if Assigned( FdmTablas ) then
     begin
          FreeAndNil( FdmTablas );
     end;
     if Assigned( FdmCatalogos ) then
     begin
          FreeAndNil( FdmCatalogos );
     end;
end;

procedure TdmProcesos.InitTablas;
begin
     if not Assigned( FdmTablas ) then
     begin
          FdmTablas := TdmTablas.Create( nil );
          FdmTablas.dmCliente := Self;
     end;
end;

procedure TdmProcesos.InitCatalogos;
begin
     if not Assigned( FdmCatalogos ) then
     begin
          FdmCatalogos := TdmCatalogos.Create( nil );
          FdmCatalogos.dmCliente := Self;
     end;
end;

function TdmProcesos.GetRango( oNodo: TZetaXMLNode; AliasEmpleado: String = ARROBA_TABLA ): String;
begin
     with XMLTools do
     begin
          case eTipoRangoActivo( TagAsInteger( oNodo, 0 ) ) of
               raRango: Result := ZetaClientTools.GetFiltroRango( Format( '%s.CB_CODIGO', [ AliasEmpleado ] ),
                                                                  Trim( XMLTools.AttributeAsString( oNodo, 'INICIAL', VACIO ) ),
                                                                  Trim( XMLTools.AttributeAsString( oNodo, 'FINAL', VACIO ) ) );
               raLista: Result := GetFiltroLista( Format( '%s.CB_CODIGO', [ AliasEmpleado ] ),
                                                  Trim( XMLTools.AttributeAsString( oNodo, 'LISTA', VACIO ) ) );
          else
              Result := '';
          end;
     end;
end;

function TdmProcesos.GetCondicion( oNodo: TZetaXMLNode ): String;
var
   sCondicion: String;
begin
     sCondicion := XMLTools.TagAsString( oNodo, VACIO );
     if StrLleno( sCondicion ) then
     begin
          with cdsLista do
          begin
               Data := ServerCatalogos.GetCondiciones( Empresa );
               if Locate( 'QU_CODIGO', sCondicion, [] ) then
                  Result := Trim( FieldByName( 'QU_FILTRO' ).AsString )
               else
                   Result := VACIO;
          end;
     end
     else
         Result := VACIO;
end;

function TdmProcesos.GetFiltro( oNodo: TZetaXMLNode ): String;
begin
     Result := Trim( XMLTools.TagAsString( oNodo, VACIO ) );
end;

procedure TdmProcesos.LlenaFiltros( Parametros: TZetaParams; AliasEmpleado: String = ARROBA_TABLA );
const
     NODO_FILTROS = 'FILTROS';
     K_RANGO = 'RANGO';
     K_CONDICION = 'CONDICION';
     K_FILTRO = 'FILTRO';
var
   oNodo: TZetaXMLNode;
begin
     with XMLTools do
     begin
          oNodo := GetNode( NODO_FILTROS );
          if Assigned( oNodo ) then               // Viene seccion de filtros
          begin
               with Parametros do
               begin
                    AddString( 'RangoLista', GetRango( GetNode( K_RANGO, oNodo ), AliasEmpleado ) );
                    AddString( 'Condicion', GetCondicion( GetNode( K_CONDICION, oNodo ) ) );
                    AddString( 'Filtro', GetFiltro( GetNode( K_FILTRO, oNodo ) ) );
               end;
          end;
     end;
end;

procedure TdmProcesos.AgregaParametro( oNodo: TZetaXMLNode; Parametros: TZetaParams );
var
   sNameParam: String;
begin
     sNameParam := oNodo.NodeName;
     with XMLTools, Parametros do
     begin
          case eTipoGlobal( AttributeAsInteger( oNodo, 'Tipo', 0 ) ) of
               tgBooleano: AddBoolean( sNameParam, TagAsBoolean( oNodo, FALSE ) );
               tgFloat: AddFloat( sNameParam, TagAsFloat( oNodo, 0 ) );
               tgNumero: AddInteger( sNameParam, TagAsInteger( oNodo, 0 ) );
               tgFecha: AddDate( sNameParam, TagAsDate( oNodo, NullDateTime ) );
               tgTexto: AddString( sNameParam, TagAsString( oNodo, VACIO ) );
               tgMemo:  AddMemo( sNameParam, TagAsString( oNodo, VACIO ) );
          end;
     end;
end;

procedure TdmProcesos.LlenaParametros( Parametros: TZetaParams );
const
     NODO_PARAMETROS = 'PARAMETROS';
var
   oNodo, oItem: TZetaXMLNode;
begin
     with XMLTools do
     begin
          oNodo := GetNode( NODO_PARAMETROS );
          if Assigned( oNodo ) and HasChildren( oNodo ) then               // Viene seccion de parametros
          begin
               oItem := GetFirstChild( oNodo );
               while Assigned( oItem ) do
               begin
                    AgregaParametro( oItem, Parametros );
                    oItem := GetNextSibling( oItem );
               end;
          end;
     end;
end;

procedure TdmProcesos.AgregaResultadoWizard( const eProceso: Procesos; const oResultado: OleVariant );
begin
     with XMLResult do
     begin
          WriteStartElement( 'PROCESO' );
          try
             WriteValueInteger( 'ID', Ord( eProceso ) );
             WriteValueString( 'DESCRIPCION', ZetaCommonLists.ObtieneElemento( lfProcesos, Ord( eProceso ) ) );
             WriteValueInteger( 'FOLIO', oResultado[ K_PROCESO_FOLIO ] );
             WriteValueInteger( 'STATUS', oResultado[ K_PROCESO_STATUS ] );
             WriteValueInteger( 'MAXIMO', oResultado[ K_PROCESO_MAXIMO ] );
             WriteValueInteger( 'PROCESADOS', oResultado[ K_PROCESO_PROCESADOS ] );
             WriteValueInteger( 'ULTIMO_EMPLEADO', oResultado[ K_PROCESO_ULTIMO_EMPLEADO ] );
             WriteValueDateTime( 'INICIO', oResultado[ K_PROCESO_INICIO ] );
             WriteValueDateTime( 'FIN', oResultado[ K_PROCESO_FIN ] );
             WriteValueInteger( 'ERRORES', oResultado[ K_PROCESO_ERRORES ] );
             WriteValueInteger( 'ADVERTENCIAS', oResultado[ K_PROCESO_ADVERTENCIAS ] );
             WriteValueInteger( 'EVENTOS', oResultado[ K_PROCESO_EVENTOS ] );
          finally
                 WriteEndElement;
          end;
     end;
end;

{ ***** Implementaciones de Llamadas al Servidor de Procesos ***** }

function TdmProcesos.ProcesaWizard( const eProceso: Procesos; const Parametros: String ): String;
var
   oParams: TZetaParams;
   oResultado: OleVariant;
   sResultado:string;
   FServerCalc: TdmServerCalcNomina;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Parametros , eProceso <> prASISProcesarTarjetas  ) then
        begin
             try
                sResultado := VACIO;
                oParams := TZetaParams.Create( self );
                try
                   LlenaValoresActivos( oParams );
                   LlenaFiltros( oParams );
                   LlenaParametros( oParams );
                   case eProceso of
                        prRHSalarioIntegrado:    oResultado := ServerRecursos.SalarioIntegrado( Empresa, oParams.VarValues );
                        prASISRecalculoTarjetas: oResultado := ServerCalcNomina.RecalcularTarjetas( Empresa, oParams.VarValues );
                        prASISCalculoPreNomina:  oResultado := ServerCalcNomina.CalculaPreNomina( Empresa, oParams.VarValues );
                        prNOCalcular:            oResultado := ServerCalcNomina.CalculaNomina( Empresa, oParams.VarValues );
                        prNOAfectar:             oResultado := ServerNomina.AfectarNomina( Empresa, oParams.VarValues );
                        prNODesafectar:          oResultado := ServerNomina.DesafectarNomina( Empresa, oParams.VarValues);
                        prRHPromediarVariables : oResultado := ServerRecursos.PromediarVariables( Empresa, oParams.VarValues);
                        prASISProcesarTarjetas :
                        begin

                             try
                                FServerCalc := TdmServerCalcNomina.Create( self );

                                if ( FServerCalc <> nil ) then
                                     sResultado := FServerCalc.ProcesarTarjetasSimple( Empresa, oParams.VarValues)
                                else
                                    LogError( 'Error al ejecutar proceso: No fue posible crear TdmServerCalcNomina');

                             finally
                                    FreeAndNil( FServerCalc );
                             end;

                        end;
                   else
                       LogError( Format( 'El proceso %s no ha sido implementado', [ ZetaCommonLists.ObtieneElemento( lfProcesos, Ord( eProceso ) ) ] ) );
                   end;
                   if StrVacio(sResultado) then
                      AgregaResultadoWizard( eProceso, oResultado );
                finally
                       FreeAndNil( oParams );
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( 'Error al ejecutar proceso: ' + Error.Message );
                   end;
             end;
        end;
     finally
            LogEnd;
     end;
     if StrLleno(sResultado) then
        Result := sResultado
     else
         Result := LogAsText;
end;

procedure TdmProcesos.AjusteColectivoListaGetDataset(const Parametros: String);
var
   oParams: TZetaParams;
begin
     InitCatalogos;
     with dmCatalogos do
     begin
          cdsHorarios.Conectar;
     end;
     InitTablas;
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          cdsMotAuto.Conectar;
     end;
     oParams := TZetaParams.Create( self );
     try
        LlenaValoresActivos( oParams );
        LlenaFiltros( oParams, 'COLABORA' );
        LlenaParametros( oParams );
        with cdsAjusteColectivo do
        begin
             Data := Self.ServerSupervisores.AjusteColectivoGetLista( Self.Empresa, oParams.VarValues );
        end;
     finally
            FreeAndNil( oParams );
     end;
end;

procedure TdmProcesos.cdsAjusteColectivoAlCrearCampos(Sender: TObject);
begin
     inherited;
     with cdsAjusteColectivo do
     begin
          with dmCatalogos do
          begin
               CreateSimpleLookup( cdsHorarios, 'HO_DESCRIP', 'HO_CODIGO' );
          end;
          with dmTablas do
          begin
               CreateSimpleLookup( cdsIncidencias, 'IN_ELEMENT', 'AU_TIPO' );
          end;
     end;
end;

function TdmProcesos.AjusteColectivoLista(const Parametros: String): String;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Parametros ) then
        begin
             try
                AjusteColectivoListaGetDataset( Parametros );
                if cdsAjusteColectivo.IsEmpty then
                begin
                     LogError( 'No hay empleados que cumplan con los filtros indicados' );
                end
                else
                begin
                     XMLResult.XMLBuildDataSet( cdsAjusteColectivo, 'LISTA' );
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( 'Error al obtener lista de empleados: ' + Error.Message );
                   end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

function TdmProcesos.AjusteColectivoEscribir(const Parametros: String): String;
var
   oParams: TZetaParams;
begin
     Result := VACIO;
     LogInit;
     try
        if ConectaEmpresa( Parametros ) then
        begin
             try
                AjusteColectivoListaGetDataset( Parametros );
                if cdsAjusteColectivo.IsEmpty then
                begin
                     LogError( 'No hay empleados que cumplan con los filtros indicados' );
                end
                else
                begin
                     XMLTools.XMLToDataSet( cdsLista, 'DATASET' );
                     oParams := TZetaParams.Create( self );
                     try
                        LlenaValoresActivos( oParams );
                        {
                        LlenaFiltros( oParams );
                        LlenaParametros( oParams );
                        GA: Invocar D:\3Win_20\MTS\DServerSUper.GrabaListaAsistencia()
                        }
                     finally
                            FreeAndNil( oParams );
                     end;
                end;
             except
                   on Error: Exception do
                   begin
                        LogError( 'Error al escribir ajustes: ' + Error.Message );
                   end;
             end;
        end;
     finally
            LogEnd;
     end;
     Result := LogAsText;
end;

end.
