unit DClienteLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBasicoCliente, DB, DBClient,
  DXMLTools,
  ZetaClientDataSet,
  ZetaCommonLists;

type
  TdmClienteLogin = class(TBasicoCliente)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FXMLTools: TdmXMLTools;
    FXMLResult : TdmXMLTools;

  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    function UsuarioLogin( const sXML: string ): string;
    function CambiaClaveUsuario(const sXML: string): string;
  end;



implementation

uses XMLDoc,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaRegistryCliente;

{$R *.dfm}

function TdmClienteLogin.UsuarioLogin( const sXML: string ): string;
var
   oUsuario: TZetaXMLNode;
   iReplyLog: eLogReply;
   iIntentos: integer;
   sMensaje, sNombre, sClave, sComputadora: string;
begin
     with FXMLTools do
     begin
          XMLAsText := sXML;
          oUsuario := XMLDocument.DocumentElement;
          if oUsuario = NIL then
             Result := '<ERROR>Archivo no es XML</ERROR>'
          else
          begin

               if ( oUsuario.NodeName <> 'UserData' ) then
                  Result := '<ERROR>Archivo XML no tiene la estructura correcta</ERROR>'
               else
               begin
                    oUsuario := GetNode('Usuario');

                    if Self.Sentinel_EsProfesional_MSSQL then
                    begin
                         iReplyLog := lrSystemBlock;
                         sMensaje := 'M�dulo No Disponible para licencia Profesional de Sistema TRESS';
                         WriteAttributeInteger('ReplyLog',Ord(iReplyLog),oUsuario);
                         WriteAttributeString('Mensaje',sMensaje,oUsuario);
                    end
                    else
                    begin
                         sNombre:= AttributeAsString( oUsuario, 'Nombre', '' );
                         sClave := AttributeAsString( oUsuario, 'Clave', '' );
                         sComputadora := AttributeAsString( oUsuario, 'Computadora', '' );
                         iIntentos := AttributeAsInteger(oUsuario,'Intentos',0);

                         iReplyLog := GetUsuario( sNombre, sClave, sComputadora, TRUE );

                         if NOT ( iReplyLog in [lrOK , lrNotFound, lrChangePassword, lrExpiredPassword ] ) and
                             ( iIntentos >= DatosSeguridad.PasswordIntentos ) then
                         begin
                              UsuarioBloquea( sNombre, iIntentos );
                              iReplyLog := lrLockedOut
                         end;

                         case iReplyLog of
                              lrOK: sMensaje := '';
                              lrLoggedIn: sMensaje :='�Usuario ya entr� al sistema!';
                              lrLockedOut: sMensaje :='� Usuario bloqueado !';
                              lrChangePassword: sMensaje :='� Debe cambiar clave de usuario !';
                              lrExpiredPassword: sMensaje :='� Clave de usuario vencida !';
                              lrInactiveBlock: sMensaje :='� Usuario bloqueado !' ;
                              lrNotFound: sMensaje :='� No se encontr� el usuario !' ;
                              lrAccessDenied: sMensaje :='� Password incorrecto de usuario !';
                              lrSystemBlock: sMensaje :='� Sistema Bloqueado !' ;
                              lrDBServerInvalid : sMensaje :='� La licencia de Profesional de Sistema TRESS requiere la edici�n de MSSQL Server Express !' ;
                         else
                             sMensaje :='�Error general al verificar usuario!' ;
                         end;

                         WriteAttributeInteger('ReplyLog',Ord(iReplyLog),oUsuario);
                         WriteAttributeString('Mensaje',sMensaje,oUsuario);

                         with cdsUsuario do
                         begin
                              if Active then
                              begin
                                   WriteAttributeInteger('NumeroUsuario',FieldByName('US_CODIGO').AsInteger,oUsuario);
                                   WriteAttributeString('Empresa',FieldByName('CM_CODIGO').AsString,oUsuario);
                                   WriteAttributeInteger('Empleado',FieldByName('CB_CODIGO').AsInteger,oUsuario);
                                   WriteAttributeString('Nombre',FieldByName('US_NOMBRE').AsString,oUsuario);
                                   WriteAttributeString('EsActivo',FieldByName('US_ACTIVO').AsString,oUsuario);
                                   WriteAttributeString('EsPortal',FieldByName('US_PORTAL').AsString,oUsuario);
                                   WriteAttributeString('Email',FieldByName('US_EMAIL').AsString,oUsuario);
                                   WriteAttributeString('Domain',FieldByName('US_DOMAIN').AsString,oUsuario);
                                   WriteAttributeString('EstaBloqueado',FieldByName('US_BLOQUEA').AsString,oUsuario);
                                   WriteAttributeString('CambiaPass',FieldByName('US_CAMBIA').AsString,oUsuario);
                              end;
                         end;
                    end;
                    Result := XMLDocument.XML.Text;
               end;
          end;
     end;
end;

function TdmClienteLogin.CambiaClaveUsuario( const sXML: string ): string;
 var
    sAnterior, sClave: String;
    oUsuario: TZetaXMLNode;
    Error: eLogReply;
begin
     Result := UsuarioLogin( sXML );
     Error := lrOK;
     with FXMLTools do
     begin
          XMLAsText := Result;
          oUsuario := XMLDocument.DocumentElement;

          if oUsuario <> NIL then
          begin
               if ( oUsuario.NodeName = 'UserData' ) then
               begin
                    oUsuario := GetNode('Usuario');
                    sAnterior:= AttributeAsString( oUsuario, 'Clave', '' );
                    sClave := AttributeAsString( oUsuario, 'ClaveNueva', '' );
                    WriteAttributeString('Mensaje','',oUsuario);

                    try
                       ActualizaClaveUsuario( sAnterior, sClave );
                    except
                          On E:Exception do
                          begin
                               Error:= lrChangePassword;
                               WriteAttributeString('Mensaje',E.Message,oUsuario);
                          end;
                    end;
                    if Error = lrOK then
                    begin
                         WriteAttributeString('CambiaPass',K_GLOBAL_NO);
                         WriteAttributeString('Clave',PasswordUsuario,oUsuario);
                    end;
                    WriteAttributeInteger('ReplyLog',Ord(Error),oUsuario);

               end;
               Result := XMLDocument.XML.Text;
          end;
     end;
end;



constructor TdmClienteLogin.Create(AOwner: TComponent);
begin
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}

     inherited Create( AOwner );
end;

destructor TdmClienteLogin.Destroy;
begin
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmClienteLogin.DataModuleCreate(Sender: TObject);
begin
     inherited;
     FXMLTools := TdmXMLTools.Create( nil );
     FXMLResult := TdmXMLTools.Create( nil );
end;

procedure TdmClienteLogin.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FXMLTools );
     FreeAndNil( FXMLResult );
end;

end.
