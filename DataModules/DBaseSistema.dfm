object dmBaseSistema: TdmBaseSistema
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 469
  Width = 741
  object cdsEmpresas: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    BeforeInsert = cdsEmpresasBeforeInsert
    BeforePost = cdsEmpresasBeforePost
    AfterDelete = cdsEmpresasAfterDelete
    OnNewRecord = cdsEmpresasNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsEmpresasAlAdquirirDatos
    AlEnviarDatos = cdsEmpresasAlEnviarDatos
    AlModificar = cdsEmpresasAlModificar
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    OnGetRights = cdsEmpresasGetRights
    Left = 104
    Top = 8
  end
  object cdsGrupos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        AggregateName = 'MaxCodigo'
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
    AggregatesActive = True
    Params = <>
    BeforeInsert = cdsGruposBeforeInsert
    BeforePost = cdsGruposBeforePost
    AfterDelete = cdsGruposAfterDelete
    OnNewRecord = cdsGruposNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsGruposAlAdquirirDatos
    AlEnviarDatos = cdsGruposAlEnviarDatos
    AlModificar = cdsGruposAlModificar
    UsaCache = True
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    OnGetRights = cdsGruposGetRights
    Left = 176
    Top = 8
  end
  object cdsUsuarios: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsUsuariosAfterOpen
    BeforePost = cdsUsuariosBeforePost
    AfterDelete = cdsUsuariosAfterDelete
    OnNewRecord = cdsUsuariosNewRecord
    OnReconcileError = cdsUsuariosReconcileError
    AlAdquirirDatos = cdsUsuariosAlAdquirirDatos
    AlEnviarDatos = cdsUsuariosAlEnviarDatos
    AlCrearCampos = cdsUsuariosAlCrearCampos
    AlModificar = cdsUsuariosAlModificar
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnGetRights = cdsUsuariosGetRights
    Left = 24
    Top = 8
  end
  object cdsImpresoras: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterDelete = cdsImpresorasAfterDelete
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsImpresorasAlAdquirirDatos
    AlEnviarDatos = cdsImpresorasAlEnviarDatos
    AlModificar = cdsImpresorasAlModificar
    UsaCache = True
    Left = 104
    Top = 64
  end
  object cdsAccesosBase: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    AlEnviarDatos = cdsAccesosBaseAlEnviarDatos
    Left = 176
    Top = 64
  end
  object cdsEmpresasLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasLookUpAlAdquirirDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    OnGetRights = cdsEmpresasLookUpGetRights
    Left = 24
    Top = 64
  end
  object cdsSuscrip: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsSuscripBeforePost
    OnNewRecord = cdsSuscripNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsSuscripAlAdquirirDatos
    AlEnviarDatos = cdsSuscripAlEnviarDatos
    AlCrearCampos = cdsSuscripAlCrearCampos
    Left = 248
    Top = 8
  end
  object cdsCopiarAccesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 328
    Top = 8
  end
  object cdsGruposLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsGruposLookupAlAdquirirDatos
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    Left = 264
    Top = 72
  end
  object cdsEmpresasAccesos: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasAccesosAlAdquirirDatos
    AlCrearCampos = cdsEmpresasAccesosAlCrearCampos
    Left = 408
    Top = 5
  end
  object cdsDerechos: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 24
    Top = 136
  end
  object cdsUsuariosLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    LookupActivoField = 'US_ACTIVO'
    OnGetRights = cdsUsuariosLookupGetRights
    Left = 432
    Top = 8
  end
  object cdsClasifiRepEmp: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'RC_ORDEN'
    Params = <>
    LookupName = 'Clasificaciones'
    LookupDescriptionField = 'RC_NOMBRE'
    LookupKeyField = 'RC_CODIGO'
    Left = 112
    Top = 136
  end
  object cdsRoles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = GenericReconcileError
    Left = 216
    Top = 136
  end
  object cdsUserRoles: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    Left = 304
    Top = 136
  end
  object cdsGruposTodos: TZetaLookupDataSet
    Aggregates = <
      item
        Active = True
        AggregateName = 'MaxCodigo'
        Expression = 'Max( GR_CODIGO ) + 0'
        Visible = False
      end>
    AggregatesActive = True
    Params = <>
    BeforeInsert = cdsGruposBeforeInsert
    BeforePost = cdsGruposBeforePost
    AfterDelete = cdsGruposAfterDelete
    OnNewRecord = cdsGruposNewRecord
    OnReconcileError = GenericReconcileError
    AlAdquirirDatos = cdsGruposTodosAlAdquirirDatos
    AlEnviarDatos = cdsGruposAlEnviarDatos
    AlModificar = cdsGruposAlModificar
    UsaCache = True
    LookupName = 'Grupos de Usuarios'
    LookupDescriptionField = 'GR_DESCRIP'
    LookupKeyField = 'GR_CODIGO'
    OnGetRights = cdsGruposGetRights
    Left = 24
    Top = 192
  end
  object cdsSistBaseDatos: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'DB_CODIGO'
    Params = <>
    BeforePost = cdsSistBaseDatosBeforePost
    AfterDelete = cdsSistBaseDatosAfterDelete
    OnNewRecord = cdsSistBaseDatosNewRecord
    OnReconcileError = cdsSistBaseDatosReconcileError
    AlAdquirirDatos = cdsSistBaseDatosAlAdquirirDatos
    AlEnviarDatos = cdsSistBaseDatosAlEnviarDatos
    AlCrearCampos = cdsSistBaseDatosAlCrearCampos
    AlModificar = cdsSistBaseDatosAlModificar
    LookupName = 'Bases de Datos'
    LookupDescriptionField = 'DB_DESCRIP'
    LookupKeyField = 'DB_CODIGO'
    OnGetRights = cdsSistBaseDatosGetRights
    Left = 128
    Top = 192
  end
  object cdsSistBaseDatosLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSistBaseDatosLookUpAlAdquirirDatos
    LookupName = 'Base de Datos'
    LookupDescriptionField = 'DB_DESCRIP'
    LookupKeyField = 'DB_CODIGO'
    OnLookupSearch = cdsSistBaseDatosLookUpLookupSearch
    Left = 296
    Top = 141
  end
  object cdsUsuariosSeguridad: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsUsuariosSeguridadAfterOpen
    BeforePost = cdsUsuariosSeguridadBeforePost
    AfterDelete = cdsUsuariosSeguridadAfterDelete
    OnNewRecord = cdsUsuariosSeguridadNewRecord
    OnReconcileError = cdsUsuariosSeguridadReconcileError
    AlAdquirirDatos = cdsUsuariosSeguridadAlAdquirirDatos
    AlEnviarDatos = cdsUsuariosSeguridadAlEnviarDatos
    AlCrearCampos = cdsUsuariosSeguridadAlCrearCampos
    AlModificar = cdsUsuariosSeguridadAlModificar
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    OnGetRights = cdsUsuariosSeguridadGetRights
    Left = 40
    Top = 256
  end
  object cdsUsuariosSeguridadLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Usuarios'
    LookupDescriptionField = 'US_NOMBRE'
    LookupKeyField = 'US_CODIGO'
    LookupActivoField = 'US_ACTIVO'
    OnGetRights = cdsUsuariosSeguridadLookupGetRights
    Left = 192
    Top = 256
  end
  object cdsBasesDatos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 512
    Top = 14
  end
  object cdsSuscripUsuarioCalendario: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSuscripUsuarioCalendarioAlAdquirirDatos
    AlCrearCampos = cdsSuscripUsuarioCalendarioAlCrearCampos
    Left = 24
    Top = 309
  end
  object cdsSistTareaUsuario: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CA_FOLIO'
    Params = <>
    AlAdquirirDatos = cdsSistTareaUsuarioAlAdquirirDatos
    AlEnviarDatos = cdsSistTareaUsuarioAlEnviarDatos
    Left = 40
    Top = 260
  end
  object cdsSistTareaRoles: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'CA_FOLIO'
    Params = <>
    StoreDefs = True
    AlAdquirirDatos = cdsSistTareaRolesAlAdquirirDatos
    AlEnviarDatos = cdsSistTareaRolesAlEnviarDatos
    Left = 336
    Top = 228
  end
  object cdsSistTareaCalendario: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CA_FOLIO'
    Params = <>
    AfterCancel = cdsSistTareaCalendarioAfterCancel
    AfterDelete = cdsSistTareaCalendarioAfterDelete
    OnNewRecord = cdsSistTareaCalendarioNewRecord
    AlAdquirirDatos = cdsSistTareaCalendarioAlAdquirirDatos
    AlEnviarDatos = cdsSistTareaCalendarioAlEnviarDatos
    AlCrearCampos = cdsSistTareaCalendarioAlCrearCampos
    AlModificar = cdsSistTareaCalendarioAlModificar
    Left = 488
    Top = 308
  end
  object cdsSuscripCalendarioReportes: TZetaClientDataSet
    Aggregates = <>
    AutoCalcFields = False
    IndexFieldNames = 'ORDEN'
    Params = <>
    AlAdquirirDatos = cdsSuscripCalendarioReportesAlAdquirirDatos
    AlCrearCampos = cdsSuscripCalendarioReportesAlCrearCampos
    Left = 328
    Top = 372
  end
  object cdsEnviosProgramadosEmpresa: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CA_FOLIO'
    Params = <>
    AfterCancel = cdsEnviosProgramadosEmpresaAfterCancel
    AfterDelete = cdsEnviosProgramadosEmpresaAfterDelete
    OnNewRecord = cdsEnviosProgramadosEmpresaNewRecord
    AlAdquirirDatos = cdsEnviosProgramadosEmpresaAlAdquirirDatos
    AlEnviarDatos = cdsEnviosProgramadosEmpresaAlEnviarDatos
    AlCrearCampos = cdsEnviosProgramadosEmpresaAlCrearCampos
    AlModificar = cdsEnviosProgramadosEmpresaAlModificar
    Left = 336
    Top = 327
  end
  object cdsSistTareaCalendarioLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSistTareaCalendarioLookupAlAdquirirDatos
    LookupName = 'Calendario Tareas'
    LookupDescriptionField = 'CA_NOMBRE'
    LookupKeyField = 'CA_FOLIO'
    Left = 544
    Top = 215
  end
  object cdsEmpresasBuildLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasBuildLookupAlAdquirirDatos
    LookupName = 'Empresas'
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    Left = 584
    Top = 16
  end
  object cdsSistSolicitudEnvios: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsSistSolicitudEnviosAlCrearCampos
    AlModificar = cdsSistSolicitudEnviosAlModificar
    Left = 544
    Top = 480
  end
  object cdsSistEnvioEmail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSistEnvioEmailAlAdquirirDatos
    AlCrearCampos = cdsSistEnvioEmailAlCrearCampos
    AlModificar = cdsSistEnvioEmailAlModificar
    Left = 160
    Top = 448
  end
  object cdsBitacoraReportes: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsBitacoraReportesAfterOpen
    AlCrearCampos = cdsBitacoraReportesAlCrearCampos
    AlModificar = cdsBitacoraReportesAlModificar
    Left = 608
    Top = 152
  end
end
