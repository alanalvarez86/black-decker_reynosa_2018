unit DBasicoCliente;

interface

{$INCLUDE DEFINES.INC}

{$ifndef DOS_CAPAS}
{$define HTTP_CONNECTION}
{$endif}

{$define VALIDAEMPLEADOSGLOBAL}

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DBClient, ComOBJ, FileCtrl,
     {$ifdef HTTP_CONNECTION}SConnect,{$endif}
     {$ifndef VER130}Variants,{$endif}
{$ifdef DOS_CAPAS}
     DServerLogin,
{$else}
     Login_TLB,
{$endif}
     FAutoClasses,
     ZetaLicenseClasses,
     ZetaTipoEntidad,
     ZetaClientDataSet,
     ZetaCommonLists, ZetaServerTools, Math,
     {$ifdef INTERFAZ_NOVVARCO_KRONOS}
     DInterfase,
     {$endif}
     ZetaCommonClasses, cxClasses, cxLookAndFeels, dxSkinsForm;

type
  TInfoUsuario = record
    Codigo: Integer;
    NombreCorto: String;
    Nombre: String;
    Grupo: Integer;
    Nivel: eNivelUsuario;
    TieneArbol: Boolean;
    LoginAD: Boolean;
    GrupoDescrip: String;
    UltimaEntrada: String;
  end;
  TBasicoCliente = class(TDataModule)
    cdsUsuario: TZetaClientDataSet;
    cdsCompany: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    {$ifdef HTTP_CONNECTION}
    FWebConnection: TWebConnection;
    {$endif}
    FEmpresaAbierta: Boolean;
    FEmpresa: Variant;
    FUsuario: Integer;
    FPasswordUsuario: String;
    FCaptionLogoff : String;
    FFormsMoved : Boolean;
    FModoSuper: Boolean;
    FModoLabor: Boolean;
    FModoServicioMedico: Boolean;
    FModoPlanCarrera: Boolean;
    FModoSeleccion: Boolean;
    FModoVisitantes: Boolean;
    FModoEvaluacion: Boolean;
    FModoCajaAhorro: Boolean;
    FTipoCompany: eTipoCompany;
    FDataCache: TStringList;
    FServerSupportsCOMPlus: Boolean;
    FDatosSeguridad: TDatosSeguridad;
    {***US:13038 Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
    FDatosNotificacionAdvertencia: TDatosNotificacionAdvertencia;
    FTipoLogin : eTipoLogin;
    FApplicationID: Integer;
    {***DevEx (by am):Variable booleana para definir si se esta utilizando el cliente de sistema Tress
              Es distinta del metodo GetModoTress. Se agrego como necesidad del proyecto NuevaInterfaz2014,
              por lo tanto lleva el prefijo DevEx al igual que la propieddad para accesarla ModoTress_DeVex.
    ***}
    FModoTress_DevEx: Boolean;
    // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
    FDatosServicioReportesCorreos: TDatosServicioReportesCorreos;

{$ifdef VALIDAEMPLEADOSGLOBAL}
    FDatosSeguridadEmpresa : TClientGlobalLicenseValues;
{$endif}
    FVersionDatos:Integer;
    FUltimaVersion:Integer;
    function GetCompania: String;
    function GetEsModoDemo: Boolean;
    function GetFechaDefault: TDate;
    procedure SetFechaDefault(const Value: TDate);
    procedure SetDatosSeguridad( const Valores: OleVariant );
    function GetModoTress: Boolean;
    function GetApplicationID: Integer;
    procedure SetApplicationID( const Value: Integer );
    procedure SetDatosNotificacionAdvertencia( const Valores: OleVariant );
    // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
    procedure SetDatosServicioReportesCorreos( const Valores: OleVariant );
    function GetClaveAleatoria : string;

  protected
    FFechaDefault: TDate;
    {$ifdef DOS_CAPAS}
    FServidor: TdmServerLogin;
    {$else}
    FServidor: IdmServerLoginDisp;
    {$endif}
    property ServerSupportsCOMPlus: Boolean read FServerSupportsCOMPlus;
    {$ifndef DOS_CAPAS}
    function GetServidor: IdmServerLoginDisp;
    function CreateServer(const ClassID: TGUID): IDispatch;
    {$endif}
    function GetConfidencialidad: String;
    function GetConfidencialidadDefault : String;
    function GetConfidencialidadListaIN : string;

    procedure InitCacheModules; virtual;
    procedure SetUsuario( const iValue: Integer );
    {***DevEx(@am): Metodos virtuales para poder compilar unidades utilizadas en varios proyectos ***}
    function GetSkinController: TdxSkinController; virtual;
    {***}
  public
    { Public declarations }
    property Compania: String read GetCompania;
    property Empresa: Variant read FEmpresa;
    property EmpresaAbierta: Boolean read FEmpresaAbierta write FEmpresaAbierta;
    property EsModoDemo: Boolean read GetEsModoDemo;
    property FechaDefault: TDate read GetFechaDefault write SetFechaDefault;
{$ifdef DOS_CAPAS}
    property Servidor: TdmServerLogin read FServidor;
{$else}
    property Servidor: IdmServerLoginDisp read GetServidor;
{$endif}
    property ModoSuper: Boolean read FModoSuper write FModoSuper default FALSE;
    property ModoLabor: Boolean read FModoLabor write FModoLabor default FALSE;
    property ModoServicioMedico: Boolean read FModoServicioMedico write FModoServicioMedico default FALSE;
    property ModoPlanCarrera: Boolean read FModoPlanCarrera write FModoPlanCarrera default FALSE;
    property ModoSeleccion: Boolean read FModoSeleccion write FModoSeleccion default FALSE;
    property ModoVisitantes: Boolean read FModoVisitantes write FModoVisitantes default FALSE;
    property ModoEvaluacion: Boolean read FModoEvaluacion write FModoEvaluacion default FALSE;
    property ModoCajaAhorro: Boolean read FModoCajaAhorro write FModoCajaAhorro default FALSE;
    property ModoTress: Boolean read GetModoTress;
    {***DevEx(by am): ModoTress_DevEx es una Propiedad agregada para definir globalmente se esta utilizando el cliente de TRESS, es inicializada
        con el valor de TRUE en el DCliente de la aplicacion. Para las demas aplicaciones su
        valor por default es FALSE.
    ***}
    property ModoTress_DevEx: Boolean read FModoTress_DevEx write FModoTress_DevEx default FALSE;
    property Confidencialidad : String read GetConfidencialidad;
    property ConfidencialidadDefault : String read GetConfidencialidadDefault;
    property ConfidencialidadListaIN : String read GetConfidencialidadListaIN;
    property Usuario: Integer read FUsuario write SetUsuario;
    property PasswordUsuario: String read FPasswordUsuario;
    property CaptionLogoff : String read FCaptionLogoff write FCaptionLogoff;
    property FormsMoved : Boolean read FFormsMoved write FFormsMoved;
    property TipoCompany : eTipoCompany read FTipoCompany write FTipoCompany;
    property DataCache : TStringList read FDataCache write FDataCache;
    property DatosSeguridad: TDatosSeguridad read FDatosSeguridad;
    {***US:13038 Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
    property DatosNotificacionAdvertencia: TDatosNotificacionAdvertencia read FDatosNotificacionAdvertencia;
    property VersionDatos : Integer read FVersionDatos write FVersionDatos;
    property UltimaVersion : Integer read FUltimaVersion write FUltimaVersion;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    property DatosSeguridadEmpresa : TClientGlobalLicenseValues read FDatosSeguridadEmpresa;
{$endif}
    property TipoLogin : eTipoLogin read FTipoLogin write FTipoLogin;
    property ApplicationID : Integer read GetApplicationID write SetApplicationID;

    //DevEx(@am): Propiedad para acceder a los valores del componentes SkinController en el shell
    property SkinController:TdxSkinController read GetSkinController;
    // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
    property DatosServicioReportesCorreos: TDatosServicioReportesCorreos read FDatosServicioReportesCorreos;

{$ifndef DOS_CAPAS}
    function CreaServidor( const ClassID: TGUID; var Servidor; const lStateLess: Boolean = TRUE ): IDispatch;
    procedure CreateFixedServer(const ClassID: TGUID; var Servidor);
    procedure DestroyFixedServer(var Servidor);
{$endif}
    function FindCompany( const Value: String ): Boolean;
    function BuildEmpresa(DataSet: TDataSet): OleVariant;
    function GetAccesos: OleVariant;
    function GetDatosEmpresaActiva: TDatosCompania;
    function GetDatosUsuarioActivo: TInfoUsuario;
    function GetGrupoActivo : Integer; virtual;
    function GetSeguridad: Boolean;
    function GetNotificacionAdvertencia: Boolean;
    // US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.
    function GetServicioReportesCorreos: Boolean;

    function EsMultipleConfidencialidad : Boolean;
{$ifdef VALIDAEMPLEADOSGLOBAL}
    function GetSeguridadEmpresa: Boolean;
{$endif}
    function GetSegundosTimeout: Integer; virtual;
    function GetUsuario( const sNombre, sClave: String; const lAllowReentry: Boolean ): eLogReply;overload;
    function GetUsuario( const sNombre, sClave, sComputer: String; const lAllowReentry: Boolean ): eLogReply;overload;
    function InitCompany: Integer;
    function SetCompany : OLEVariant;
    function ModuloAutorizado( const eModulo: TModulos ): Boolean;
    function ModuloAutorizadoConsulta( const eModulo: TModulos; var sMensaje: String ): Boolean;
    function ModuloAutorizadoReportes( const Entidad: TipoEntidad ): Boolean;virtual;
    function ModuloAutorizadoDerechos (const eModulo: TModulos ) : Boolean;
    function GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String; virtual;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; virtual;
    function GetCacheCount: Integer;
    function ConstruyeListaEmpleados( oDataset : TDataset; const sFiltro : string ) : string;
    function ValidaCaracteresPassword( const sClave: String; var sMensaje: String ): Boolean;

{$ifdef VALIDAEMPLEADOSGLOBAL}
    function EvaluarEmpleadosCliente(operacion : eEvaluaOperacion;  var sMensajeAdvertencia, sMensajeError : string ) : eEvaluaTotalEmpleados;
    function ValidaLicenciaEmpleados(operacion : eEvaluaOperacion) : boolean ;
    function EsTRESSPruebas : boolean;
{$endif}


{$ifdef WS_PROCESOS}
    function SetCompanySinCOM( sEmpresa, sDBServer, sDBUser, sDBPassword  : string): OLEVariant;
{$endif}
    procedure CargaActivosTodos(Parametros: TZetaParams); virtual;
    procedure SetCacheLookup;
    procedure ActualizaClaveUsuario( const sAnterior, sClave: String ); virtual;
    procedure CierraEmpresa; virtual;
    procedure SetSeguridad( const Valores: OleVariant ); virtual;
    {***US:13038 Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
    procedure SetNotificacionAdvertencia( const Valores: OleVariant ); virtual;
    {***US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.***}
    procedure SetServicioReportesCorreos( const Valores: OleVariant ); virtual;
    procedure SetPasswordLongitud( const iLongitud: Integer );
    procedure UsuarioBloquea( const sNombre: String; const iIntentos: Word );
    procedure UsuarioLogout;
    procedure InitDataCache;
    function LoginActiveDirectory(const lAllowReentry: Boolean;var Tipo:Integer):eLogReply;
    function LoggedOnUserNameEx: string;

    function EsDeConfidencialidad( sNivel0 : string ) : Boolean;
    //DevEx (by am): Funcion que devuelve el numero asignado a cada aplicacion.
    function GetAppNumber: Integer;
    //
    function CheckVersion : Boolean;

    function CheckDBServer:Boolean;
    function Sentinel_EsProfesional_MSSQL:Boolean;
    function TipoApplicacionStr:String;
    //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
    function ValidarConexionHTTP( var lLoop: Boolean; var lSalirTress: Boolean; var sMensajeError: String; lModoBatch: Boolean = False): Boolean;
    //FIN
    function MandarClaveTemporal(sUsuario, sCorreoValidar: string):Boolean;
    function GetEmpresas: OleVariant; 
  end;

var
  BasicoCliente: TBasicoCliente;

implementation

uses {DDiccionario,}
     ZetaWinAPITools,
     ZetaCommonTools,
{$ifndef TRESS_REPORTES}
     ZetaConnectionProblem_DevEx,
     ZetaDialogo,
{$endif}
     ZetaMsgDlg,
     ZetaRegistryCliente;

{$R *.DFM}

{ ***************** TdmCliente ******************* }

procedure TBasicoCliente.DataModuleCreate(Sender: TObject);
begin
     FAutoClasses.InitAuto;
     FCaptionLogoff := VACIO;
     FormsMoved := FALSE;
     FFechaDefault := Date;
     FTipoCompany := tc3Datos;
     FServerSupportsCOMPlus := False;
     SetApplicationID( 0 );
{$ifdef TRESSEXCEL}
     FServidor := NIL;
{$endif}

{$ifdef TRESS_REPORTES}
     FServidor := NIL;
{$endif}

{$ifdef DOS_CAPAS}
     FServidor := TdmServerLogin.Create( Self );
{$endif}
     {$ifdef HTTP_CONNECTION}
    // FWebConnection := TWebConnection.Create( Self );
    // with FWebConnection do
    // begin

   //       LoginPrompt := False;
   //       Agent := 'GTI';
   //  end;
     {$endif}
end;

procedure TBasicoCliente.DataModuleDestroy(Sender: TObject);
begin
     {$ifdef HTTP_CONNECTION}
     FreeAndNil( FWebConnection );
     {$endif}
     FAutoClasses.ClearAuto;
     FreeAndNil( FDataCache );
{$ifdef DOS_CAPAS}
     FreeAndNil( FServidor );
{$endif}
end;

{$ifndef DOS_CAPAS}
function TBasicoCliente.CreateServer( const ClassID: TGUID ): IDispatch;
var
   lLoop: Boolean;
begin
     lLoop := False;

     {$IFDEF TRESS_REPORTES}
     CoInitializeEx (nil, 0);
     {$ENDIF}

     repeat
           Result := nil;
           try
              {$ifdef HTTP_CONNECTION}
              case ClientRegistry.TipoConexion of
                   conxHTTP:
                   begin
                        if (not Assigned (FWebConnection)) then
                        begin
                             FWebConnection := TWebConnection.Create( Self );
                             FWebConnection.LoginPrompt := False;
                             FWebConnection.Agent :=  'GTI';
                        end;
                        with FWebConnection do
                        begin
                             Connected := False;
                             URL := ClientRegistry.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
                             Username := ClientRegistry.UserName;
                             Password := ClientRegistry.Password;
                             ServerGUID := GUIDToString( ClassID );
                             Connected := True;
                             Result := AppServer;
                        end;
                   end
                   else
                   begin
                        Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
                   end;
              end;
              {$else}
              Result := CreateRemoteComObject( ClientRegistry.ComputerName, ClassID ) as IDispatch;
              {$endif}
              //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
              lLoop := False;
           except
                 on Error: Exception do
                 begin
                      {$ifndef XMLREPORT}
                      {$ifndef TRESS_REPORTES}
                      case ZetaConnectionProblem_DevEx.ConnectionProblemDialog( Error ) of    //DevEx(by am)
                           mrRetry: lLoop := True;
                      else
                          lLoop := False;
                      end;
                      {$ELSE}
                      {$else}
                         lLoop := False;
                      {$endif}
                      {$ENDIF}
                 end;
           end;
     until not lLoop;
end;

function TBasicoCliente.CreaServidor( const ClassID: TGUID; var Servidor; const lStateLess: Boolean = TRUE ): IDispatch;
begin
     if lStateless then
        Result := CreateServer( ClassID )
     else
     begin
          if ( IDispatch( Servidor ) = nil ) then
             IDispatch( Servidor ) := CreateServer( ClassID );
          Result := IDispatch( Servidor );
     end;
end;

procedure TBasicoCliente.CreateFixedServer( const ClassID: TGUID; var Servidor );
begin
     if not ServerSupportsCOMPlus then
        CreaServidor( ClassID, Servidor, FALSE );
end;

procedure TBasicoCliente.DestroyFixedServer( var Servidor );
begin
     if not ServerSupportsCOMPlus then
        IDispatch( Servidor ) := Nil;
end;

function TBasicoCliente.GetServidor: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( CreaServidor( CLASS_dmServerLogin, FServidor ) );
end;
{$endif}

procedure TBasicoCliente.CierraEmpresa;
begin
     {$ifdef DOS_CAPAS}
     Servidor.CierraEmpresa;
     {$endif}
end;

procedure TBasicoCliente.InitDataCache;
begin
     if not Assigned( FDataCache ) then
     begin
          FDataCache := TStringList.Create;
          InitCacheModules;
     end;
end;

procedure TBasicoCliente.InitCacheModules;
begin
     // Programar en cada Cliente
end;

function TBasicoCliente.GetCacheCount: Integer;
var
   i : Integer;
begin
     Result := 0;
     if Assigned( FDataCache ) then
        for i := 0 to FDataCache.Count - 1 do
            Result := Result + TDataModule( FDataCache.Objects[i] ).ComponentCount;
end;

{ ********** Asignaci�n de Valores Activos ************* }

function TBasicoCliente.GetFechaDefault: TDate;
begin
     Result := Trunc( FFechaDefault );
end;

procedure TBasicoCliente.SetFechaDefault(const Value: TDate);
begin
     if ( FFechaDefault <> Value ) then
     begin
          FFechaDefault := Value;
     end;
end;

function TBasicoCliente.GetCompania : String;
begin
    Result := cdsCompany.FieldByName( 'CM_CODIGO' ).AsString;
end;

function TBasicoCliente.GetAccesos: OleVariant;
begin
     Result := Servidor.GetAccesos( GetGrupoActivo, Compania );
end;

function TBasicoCliente.GetApplicationID: Integer;
begin
     Result := FApplicationID;
end;

procedure TBasicoCliente.SetApplicationID( const Value: Integer );
begin
     FApplicationID := Value;
end;

{ ************** Usuarios ************* }

function TBasicoCliente.GetUsuario( const sNombre, sClave: String; const lAllowReentry: Boolean ): eLogReply;
begin
     Result := GetUsuario( sNombre, sClave, ZetaWinAPITools.GetComputerName, lAllowReentry );
end;

function TBasicoCliente.GetUsuario( const sNombre, sClave, sComputer: String; const lAllowReentry: Boolean ): eLogReply;
var
   Datos, Sentinel: OleVariant;
   iIntentos : Integer;
   {$ifdef DOS_CAPAS}
   LoginServer: TdmServerLogin;
   lLoop : Boolean;
   {$else}
   LoginServer: IdmServerLoginDisp;
   {$endif}
begin
     try
        LoginServer := Servidor;
        if Assigned( LoginServer ) then
        begin
             {$ifdef DOS_CAPAS}
             repeat
                   lLoop := FALSE;
             {$endif}
                   Result := lrNotFound;
                   try
                      Result := eLogReply( LoginServer.UsuarioLogin( sNombre, Encrypt (sClave), sComputer, lAllowReentry, Datos, Sentinel, iIntentos ) )
                   except
                         on Error: Exception do
                         begin
                              {$ifdef DOS_CAPAS}
                              {$ifndef TRESS_REPORTES}
                              case ZetaConnectionProblem_DevEx.ConnectionProblemDialog( Error ) of   //DevEx(by am)
                                   mrRetry: lLoop := True;
                              end;
                              {$endif}
                              {$else}
                              Application.HandleException( Error );
                              {$endif}
                         end;
                   end;
             {$ifdef DOS_CAPAS}
             until not lLoop;
             {$endif}
        end
        else
            Result := lrNotFound;
     finally
            {$ifndef DOS_CAPAS}
            LoginServer := nil;
            {$endif}
     end;
     FDatosSeguridad.PasswordIntentos := iIntentos;
     if ( Result in [ lrOK, lrChangePassword, lrExpiredPassword ] ) then
     begin
          with cdsUsuario do
          begin
               Data := Datos;
               FUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
               with FDatosSeguridad do
               begin
                    TimeOut := FieldByName( 'TIMEOUT' ).AsInteger;
                    PasswordLongitud := FieldByName( 'LIMITEPASS' ).AsInteger;
                    PasswordLetras := FieldByName( 'LETRASPASS' ).AsInteger;
                    PasswordDigitos := FieldByName( 'DIGITOPASS' ).AsInteger;
               end;
          end;
          FPasswordUsuario := sClave;
          Autorizacion.Cargar( Sentinel );

     end
     else
         {$ifdef INTERFAZ_NOVVARCO_KRONOS}
         Autorizacion.Cargar( Sentinel );
         {$else}
         FUsuario := 0;
         {$endif}
end;

procedure TBasicoCliente.SetUsuario( const iValue: Integer );
begin
     FUsuario := iValue;
end;

function TBasicoCliente.GetDatosUsuarioActivo: TInfoUsuario;
begin
     if Assigned(cdsUsuario.findfield('US_CODIGO')) then
     begin
           with Result do
           begin
                with cdsUsuario do
                begin
                     Codigo := FieldByName( 'US_CODIGO' ).AsInteger;
                     NombreCorto := FieldByName( 'US_CORTO' ).AsString;
                     Nombre := FieldByName( 'US_NOMBRE' ).AsString;
                     Grupo := FieldByName( 'GR_CODIGO' ).AsInteger;
                     Nivel := eNivelUsuario( FieldByName( 'US_NIVEL' ).AsInteger );
                     TieneArbol := zStrToBool( FieldByName( 'US_ARBOL' ).AsString );
                     LoginAD := zStrToBool( FieldByName( 'LOGIN_AD' ).AsString );
                     GrupoDescrip := FieldByName( 'GRUPODESCRIP' ).AsString;
                     UltimaEntrada := FieldByName( 'US_FEC_IN' ).AsString;
                end;
           end;
        end
        else
        begin
             with Result do
             begin
                     Codigo := 0;
                     NombreCorto := VACIO;
                     Nombre := VACIO;
                     Grupo := 0;
                     Nivel := eNivelUsuario(0);
                     TieneArbol := false;
                     LoginAD := False;
             end;
        end;
end;

function TBasicoCliente.GetGrupoActivo : Integer;
begin
     {$ifdef KIOSCO2}
     //Dentro de Kiosco no hay usuario firmado.
     //Se supone que los reportes que se muestran ahi, se puede ver cualquier dato.
     Result := D_GRUPO_SIN_RESTRICCION;
     {$else}
        {$ifdef KIOSCO1}
         Result := D_GRUPO_SIN_RESTRICCION;
        {$else}
        {$ifdef TRESSEMAIL}
        Result := D_GRUPO_SIN_RESTRICCION;
        {$else}
        {$ifdef TRESSIMPORTER}
        Result := D_GRUPO_SIN_RESTRICCION;
        {$else}
        if cdsUsuario.Active then
           Result := cdsUsuario.FieldByName( 'GR_CODIGO' ).AsInteger
        else
            Result := D_GRUPO_SIN_RESTRICCION;
        {$endif}
        {$endif}
        {$endif}
     {$endif}
end;

procedure TBasicoCliente.ActualizaClaveUsuario( const sAnterior, sClave: String );
var
   sMensaje: String;
begin
     with FDatosSeguridad do
     begin
          if ( PasswordLongitud > 0 ) and ( Length( sClave ) < PasswordLongitud ) then
             DataBaseError( Format( 'Clave De Acceso Debe Tener Por Lo Menos %d Caracteres', [ PasswordLongitud ] ) )
          else if not ValidaCaracteresPassword( sClave, sMensaje ) then
             DataBaseError( sMensaje );
     end;

     Servidor.UsuarioCambiaPswd( FUsuario, Encrypt(sAnterior), Encrypt(sClave));
     FPasswordUsuario := sClave;
end;

procedure TBasicoCliente.UsuarioBloquea( const sNombre: String; const iIntentos: Word );
begin
     Servidor.UsuarioBloquea( sNombre, ZetaWinAPITools.GetComputerName, iIntentos );
end;

procedure TBasicoCliente.UsuarioLogout;
begin
     if ( FUsuario > 0 ) then
     begin
          Servidor.UsuarioLogout( FUsuario );
          cdsCompany.Close;
          cdsUsuario.Close;
          FUsuario := 0;
     end;
end;

//Metodo para obtener el nombre de usuario logeado al Network
procedure GetUserNameEx(NameFormat: DWORD;lpNameBuffer: LPSTR; nSize: PULONG); stdcall;external 'secur32.dll' Name 'GetUserNameExA';

function TBasicoCliente.LoggedOnUserNameEx: string;
CONST
     NameUnknown = 0;             // Unknown name type.
     NameFullyQualifiedDN = 1;    // Fully qualified distinguished name
     NameSamCompatible = 2;       // Windows NT� 4.0 account name
     NameDisplay = 3;             // A "friendly" display name
     NameUniqueId = 6;            // GUID string that the IIDFromString function returns
     NameCanonical = 7;           // Complete canonical name
     NameUserPrincipal = 8;       // User principal name
     NameCanonicalEx = 9;
     NameServicePrincipal = 10;   // Generalized service principal name
     DNSDomainName = 11;          // DNS domain name, plus the user name
var
     UserName: array[0..250] of {$ifdef TRESS_DELPHIXE5_UP}AnsiChar{$else}char{$endif};
     Size: DWORD;
begin
     Size := 250;
     GetUserNameEx(NameSamCompatible, @UserName, @Size);
     Result := {$ifdef TRESS_DELPHIXE5_UP}String({$endif}UserName{$ifdef TRESS_DELPHIXE5_UP}){$endif};
end;

function TBasicoCliente.LoginActiveDirectory(const lAllowReentry: Boolean;var Tipo:Integer):eLogReply;
var
   Datos, Sentinel: OleVariant;
   {$ifdef DOS_CAPAS}
   LoginServer: TdmServerLogin;
   lLoop : Boolean;
   {$else}
   LoginServer: IdmServerLoginDisp;
   {$endif}
begin
     try
        LoginServer := Servidor;
        if Assigned( LoginServer ) then
        begin
             {$ifdef DOS_CAPAS}
             repeat
                   lLoop := FALSE;
             {$endif}
                   Result := lrNotFound;
                   try
                      Result := eLogReply( LoginServer.LoginAD ( ZetaWinAPITools.GetComputerName, lAllowReentry,Tipo, Datos, Sentinel,LoggedOnUserNameEx ) );
                      FTipoLogin := eTipoLogin(Tipo);
                   except
                         on Error: Exception do
                         begin
                              {$ifdef DOS_CAPAS}
                              case ZetaConnectionProblem_DevEx.ConnectionProblemDialog( Error ) of   //DevEx(by am)
                                   mrRetry: lLoop := True;
                              end;
                              {$else}
                              Application.HandleException( Error );
                              {$endif}
                         end;
                   end;
             {$ifdef DOS_CAPAS}
             until not lLoop;
             {$endif}
        end
        else
            Result := lrNotFound;
     finally
            {$ifndef DOS_CAPAS}
            LoginServer := nil;
            {$endif}
     end;
     if ( Result in [ lrOK  ] ) then
     begin
          with cdsUsuario do
          begin
               Data := Datos;
               FUsuario := FieldByName( 'US_CODIGO' ).AsInteger;
               with FDatosSeguridad do
               begin
                    TimeOut := FieldByName( 'TIMEOUT' ).AsInteger;
                    PasswordLongitud := FieldByName( 'LIMITEPASS' ).AsInteger;
                    PasswordLetras := FieldByName( 'LETRASPASS' ).AsInteger;
                    PasswordDigitos := FieldByName( 'DIGITOPASS' ).AsInteger;
               end;
          end;
          Autorizacion.Cargar( Sentinel );
     end
     else
         FUsuario := 0;
end;



{ *************** Compa��as *************** }

function TBasicoCliente.InitCompany: Integer;
var
   sSelectedCompany : String;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( FUsuario, Ord( FTipoCompany ) );
               ResetDataChange;
          end;
          {***DevEx: Agregado por el filtro de empresas incluido en el nuevo LogIn.
                     Es necesario incluir esta linea aqui para limpiar el filtro del
                     grid antes de que se haga el conteo de empresas.
                     No se valida vista porque el nuevo LogIn lo hicimos para
                     ambas vistas.
                     Al cambiar la propiedad filtered de los cds perdemos
                     el record seleccionado, esto afecta la compania seleccionada en el grid,
                     por lo tanto debemos guardar el valor antes de apagar el filtro y restaurarlo
                     despues de hacerlo.***}
          //1. Guardar compania seleccionada en el cdsCompany antes de quitarle el filtro
          sSelectedCompany := GetCompania;
          //2. Quitar el filtro
          Filtered := False;
          //3. Restaurar la seleccion del record a la compania despues de apagar el filtro
          FindCompany ( sSelectedCompany );
          Result := RecordCount;
     end;
end;

function TBasicoCliente.GetDatosEmpresaActiva: TDatosCompania;
begin
     with Result do
     begin
          with cdsCompany do
          begin
               Codigo := FieldByName( 'CM_CODIGO' ).AsString;
               Nombre := FieldByName( 'CM_NOMBRE' ).AsString;
          end;
          // Ahora se hace desde el Shell FCompania := Codigo;
     end;
end;

function TBasicoCliente.FindCompany( const Value: String ): Boolean;
begin
     Result := cdsCompany.Locate( 'CM_CODIGO', Value, [] );
end;

function TBasicoCliente.BuildEmpresa(DataSet : TDataSet):OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
P_GRUPO = 6;
P_APPID = 7;
}
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  Self.Usuario,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString,
                                  GetGrupoActivo,
                                  GetApplicationID ] );
     end;
end;



function TBasicoCliente.SetCompany: OLEVariant;
begin
     Result := BuildEmpresa(cdsCompany);
     FEmpresa := Result;
{$ifdef VALIDAEMPLEADOSGLOBAL}
     GetSeguridadEmpresa;
{$endif}
end;

{$ifdef WS_PROCESOS}
function TBasicoCliente.SetCompanySinCOM( sEmpresa, sDBServer, sDBUser, sDBPassword  : string): OLEVariant;
begin
      Result := VarArrayOf(       [sDBServer,
                                  sDBUser,
                                  sDBPassword,
                                  Self.Usuario,
                                  '',
                                  sEmpresa,
                                  D_GRUPO_SIN_RESTRICCION,
                                  0 ] );

     FEmpresa := Result;

end;
{$endif}

{ ********** Cache de Client Datasets *********** }

procedure TBasicoCliente.SetCacheLookup;
var
   sError: String;
   lOk: Boolean;

function VerificaDirectorio( const sFolder: String ): Boolean;
begin
     Result := DirectoryExists( sFolder );
     if not Result then
        sError := 'No Se Encontr� El Directorio: ' + sFolder;
end;

begin
     with ClientRegistry do
     begin
          Compania := GetCompania;
          if CacheActive then
          begin
               lOk := ( not CacheSistemaActive ) or ( VerificaDirectorio( CacheSistemaFolder ) );
               if not lOk then
               begin
                    CacheSistemaActive:= FALSE;
                    {$ifndef TRESS_REPORTES}
                    ZetaDialogo.zError( 'Tablas Temporales', sError + CR_LF + 'Se Desactivaran Las Tablas Temporales de Sistema', 0 );
                    {$endif}
               end;
               lOk := VerificaDirectorio( CacheFolder );
               if not lOk then
               begin
                    CacheActive:= FALSE;
                    {$ifndef TRESS_REPORTES}
                    ZetaDialogo.zError( 'Tablas Temporales', sError + CR_LF + 'Se Desactivaran Las Tablas Temporales', 0 );
                    {$endif}
               end;
          end;
          ZetaClientDataSet.CacheActive := CacheActive;
          ZetaClientDataSet.CacheFolder := CacheFolder;
          ZetaClientDataSet.CacheSistemaActive := CacheSistemaActive;
          ZetaClientDataSet.CacheSistemaFolder := CacheSistemaFolder;
     end;
end;

{ Autorizacion de Sentinela }

function TBasicoCliente.GetEsModoDemo: Boolean;
begin
     Result := Autorizacion.EsDemo;
	 {$IFNDEF INTERFAZ_NOVVARCO_KRONOS}
     {$ifndef TRESS_REPORTES}
     {$ifndef INTERFAZ_TRESS}
     if Result then
        ZetaDialogo.zInformation( 'Versi�n Demostraci�n', 'Operaci�n no permitida en versi�n Demo', 0 );
     {$endif}
     {$endif}
     {$ELSE}
     {if Result then
        dmInterfase.EscribeErrorExterno('Operaci�n No Permitida en Versi�n Demo');}
     {$ENDIF}
end;

function TBasicoCliente.ModuloAutorizadoConsulta( const eModulo: TModulos; var sMensaje: String ): Boolean;
begin
     with Autorizacion do
     begin
          Result := OkModulo( eModulo );
          {$ifndef TRESS_REPORTES}
          if not Result then
             sMensaje := 'El m�dulo de ' + GetModuloStr( eModulo ) + ' no est� autorizado.' + CR_LF + 'Consulte a Grupo Tress Internacional o a su distribuidor autorizado.';
          {$endif}
     end;
end;

function TBasicoCliente.ModuloAutorizadoDerechos( const eModulo: TModulos ): Boolean;
begin
     with Autorizacion do
     begin
          Result := OkModulo( eModulo );
     end;
end;

function TBasicoCliente.GetClaveAleatoria: string;
const
    LETRAS_MI = 'abcdefghijklmnopqrstuvwxyz';
    NUMEROS = '0123456789';
    NUMEROS_CASE = '12345678';
var
    local: string;
    i, iAleatorio : integer;
    sCaracterGenerado : string;
begin
      TRY
        SetLength(local, 8);
        for i := 1 to 8 do
        begin
             iAleatorio := RandomRange(1, 4);//Random(NUMEROS_CASE);
             case iAleatorio of
                  1:
                    sCaracterGenerado := letras_mi[Random(Length(LETRAS_MI)) + 1];
                  2:
                    sCaracterGenerado := UpCase(letras_mi[Random(Length(LETRAS_MI)) + 1]);
                  3:
                    sCaracterGenerado := NUMEROS[Random(Length(NUMEROS)) + 1];
                  4:
                    sCaracterGenerado := NUMEROS[Random(Length(NUMEROS)) + 1];
             end;
             local[i] := sCaracterGenerado[1];
             sCaracterGenerado := '';
        end;
        Result := local;
      EXCEPT
      END;
end;

function TBasicoCliente.MandarClaveTemporal(sUsuario, sCorreoValidar: string): Boolean;
var
   {$ifdef DOS_CAPAS}
   LoginServer: TdmServerLogin;
   lLoop : Boolean;
   {$else}
   LoginServer: IdmServerLoginDisp;
   {$endif}
   sClaveTemporal, sCorreoRemitente, sDescripRemitente: string;

begin
     try
        sClaveTemporal := VACIO;
        sCorreoRemitente := VACIO;
        sDescripRemitente := VACIO;
        LoginServer := Servidor;
        if Assigned( LoginServer ) then
        begin
                   try
                      sClaveTemporal := ZetaServerTools.Encrypt(GetClaveAleatoria);
                      sCorreoRemitente := 'SistemaTRESS@dominio.com';
                      sDescripRemitente := 'Sistema TRESS-CambiarClave';
                      Result := LoginServer.EnviaClaveTemporal(sUsuario, sClaveTemporal,sCorreoRemitente,sDescripRemitente, sCorreoValidar);
                   except
                         on Error: Exception do
                         begin
                              {$ifdef DOS_CAPAS}
                              case ZetaConnectionProblem_DevEx.ConnectionProblemDialog( Error ) of   //DevEx(by am)
                                   mrRetry: lLoop := True;
                              end;
                              {$else}
                              Application.HandleException( Error );
                              Result := False;
                              {$endif}
                         end;
                   end;
        end;
     finally
            {$ifndef DOS_CAPAS}
            LoginServer := nil;
            {$endif}
     end;
end;

function TBasicoCliente.ModuloAutorizado( const eModulo: TModulos ): Boolean;
var
   sMensaje: String;
begin
     Result := ModuloAutorizadoConsulta( eModulo, sMensaje );
     {$ifndef TRESS_REPORTES}
     if not Result then
        ZetaDialogo.ZInformation( 'M�dulo no autorizado', sMensaje, 0 );
     {$endif}
end;

function TBasicoCliente.ModuloAutorizadoReportes( const Entidad: TipoEntidad ): Boolean;
begin
     Result := TRUE;
     {CV: El codigo que se encontraba aqui, lo puse en dmCliente,
     ya que son validaciones exclusivas del modulo de
     Reporteador de Tress}
end;

procedure TBasicoCliente.SetDatosSeguridad( const Valores: OleVariant );
begin
     with FDatosSeguridad do
     begin
          PasswordExpiracion := Valores[ SEG_VENCIMIENTO ];
          PasswordLongitud := Valores[ SEG_LIMITE_PASSWORD ];
          PasswordLetras := Valores[ SEG_MIN_LETRAS_PASSWORD ];
          PasswordDigitos := Valores[ SEG_MIN_DIGITOS_PASSWORD ];
          PasswordLog := Valores[ SEG_MAX_LOG_PASSWORD ];
          PasswordIntentos := Valores[ SEG_INTENTOS ];
          Inactividad := Valores[ SEG_DIAS_INACTIVOS ];
          { El timeout se enmasacara en GetSegundosTimeout de acuerdo a lo asignado en LoginAD }
          TimeOut := Valores[ SEG_TIEMPO_INACTIVO ];
          UsuarioTareasAutomaticas := Valores[ SEG_USUARIO_TRESSAUTOMATIZA ]; // (JB) Se agrega Configuracion de Tress Automatiza
     end;
end;

{***US:13038 Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
procedure TBasicoCliente.SetDatosNotificacionAdvertencia( const Valores: OleVariant );
begin
     with FDatosNotificacionAdvertencia do
     begin
          AdvertenciaLicenciaEmpleados := Valores[ SEG_MOSTRAR_ADV_LICENCIA ]; // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
          // US #11820 Advertir uso de licencia al grupo de administradores y grupo en espec�fico por Correo
          ServidorCorreos := Valores[ SEG_SERVIDOR_CORREOS ];
          PuertoSMTP := Valores[ SEG_PUERTO_SMTP ];
          AutentificacionCorreo := Valores[ SEG_AUTENTIFICACION_CORREO ];
          UserID := Valores[ SEG_USER_ID ];
          EmailPSWD := Valores[ SEG_EMAIL_PSWD ];
          RecibirAdvertenciaPorEmail := Valores[ SEG_RECIBIR_ADV_CORREO ];
          GrupoUsuariosEmail := Valores[ SEG_GRUPO_USUARIO_EMAIL ];
     end;
end;

function TBasicoCliente.GetSeguridad: Boolean;
begin
     try
        SetDatosSeguridad( Servidor.GetSeguridad );
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TBasicoCliente.SetSeguridad( const Valores: OleVariant );
begin
     try
        Servidor.SetSeguridad( Empresa, Valores );
        SetDatosSeguridad( Valores );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

{***US:13038 Timbrado - Enviar notificaci�n a un grupo de usuarios cuando una nomina afectada tiene pendiente el timbrado***}
function TBasicoCliente.GetNotificacionAdvertencia: Boolean;
begin
     try
        SetDatosNotificacionAdvertencia( Servidor.GetNotificacionAdvertencia );
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TBasicoCliente.SetNotificacionAdvertencia( const Valores: OleVariant );
begin
     try
        Servidor.SetNotificacionAdvertencia( Empresa, Valores );
        SetDatosNotificacionAdvertencia( Valores );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

{***US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.***}
procedure TBasicoCliente.SetDatosServicioReportesCorreos( const Valores: OleVariant );
begin
     with FDatosServicioReportesCorreos do
     begin
          DirectorioReportes := Valores [K_DIRECTORIO_SERVICIO_REPORTES];
          DirectorioImagenes := Valores [K_DIRECTORIO_IMAGENES];
          URLImagenes := Valores [K_URL_IMAGENES];
          ExpiracionImagenes := Valores [K_EXPIRACION_IMAGENES];
          ExpiracionImagenes := Valores [K_EXPIRACION_IMAGENES];
          Recuperarpwsserviciocorreos := Valores [K_RECUPERAR_PWD_SERVICIO_CORREOS];
     end;
end;

{***US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.***}
function TBasicoCliente.GetServicioReportesCorreos: Boolean;
begin
     try
        SetDatosServicioReportesCorreos( Servidor.GetServicioReportesCorreos );
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

{***US #14275: Servicio de env�o de correos - Modificaci�n a pantallas de globales.***}
procedure TBasicoCliente.SetServicioReportesCorreos( const Valores: OleVariant );
begin
     try
        Servidor.SetServicioReportesCorreos( Empresa, Valores );
        SetDatosServicioReportesCorreos( Valores );
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;

procedure TBasicoCliente.SetPasswordLongitud( const iLongitud: Integer );
begin
     FDatosSeguridad.PasswordLongitud := iLongitud;
end;

function TBasicoCliente.GetSegundosTimeout: Integer;
begin
     if( NOT GetDatosUsuarioActivo.LoginAD)then
     begin
         Result := FDatosSeguridad.TimeOut * 60      // El Resultado debe convertirse a Segundos
     end
     else
         Result := 0;
end;

function TBasicoCliente.ValidaCaracteresPassword( const sClave: String; var sMensaje: String ): Boolean;
const
     K_MESS_ERROR = 'Clave De Acceso Debe Incluir Por Lo Menos %d %s';
var
   i, iDigitos, iLetras : Integer;
   lCheckLetras, lCheckDigitos: Boolean;

   procedure RevisaChar( const Caracter: Char );
   begin
        if ZetaCommonTools.EsDigito( Caracter ) then
           Inc( iDigitos )
        else if ZetaCommonTools.EsLetra( Caracter ) then
           Inc( iLetras );
   end;

begin
     with FDatosSeguridad do
     begin
          lCheckLetras := ( PasswordLetras > 0 );
          lCheckDigitos := ( PasswordDigitos > 0 );
          Result := ( not lCheckLetras ) and ( not lCheckDigitos );
          if not Result then
          begin
               iDigitos := 0;
               iLetras  := 0;
               for i := 1 to Length( sClave ) do
               begin
                    RevisaChar( sClave[i] );
               end;
               if lCheckLetras and ( iLetras < PasswordLetras ) then
                  sMensaje := Format( K_MESS_ERROR, [ PasswordLetras, 'Letras' ] )
               else
                   if lCheckDigitos and ( iDigitos < PasswordDigitos ) then
                      sMensaje := Format( K_MESS_ERROR, [ PasswordDigitos, 'D�gitos' ] )
                   else
                       Result := TRUE;
          end;
     end;
end;

function TBasicoCliente.GetValorActivo( const eValor : {$ifdef RDD}eRDDValorActivo{$else}string{$endif} ): String;
begin
     Result := Vacio;
end;

function TBasicoCliente.GetValorActivoStr( const eTipo: TipoEstado ): String;
begin
     Result := '';
end;

procedure TBasicoCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
end;

function TBasicoCliente.GetConfidencialidad: String;
begin
    Result := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;
end;

function TBasicoCliente.GetConfidencialidadListaIN : string;
var
   sConfiden : string;
begin
     Result := VACIO;
     sConfiden := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;

     if strLleno( sConfiden ) then
        Result := ListaComas2InQueryList( sConfiden );

end;

function TBasicoCliente.GetConfidencialidadDefault: String;
var
   lista : TStringList;
   sConfiden : string;
begin
    sConfiden := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;

    Result :=VACIO;

    if strLleno( sConfiden )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := sConfiden;
         if lista.Count = 1 then
                  Result := lista[0]
         else
             Result := VACIO;
         FreeAndNil ( lista );
    end;
end;

function TBasicoCliente.EsMultipleConfidencialidad : Boolean;
var
   lista : TStringList;
begin
    Result := FALSE;

    if strLleno( GetConfidencialidad )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := GetConfidencialidad;
         Result := ( lista.Count > 1);
         FreeAndNil ( lista );
    end;

end;



function TBasicoCliente.EsDeConfidencialidad( sNivel0 : string ) : Boolean;
var
   lista : TStringList;
   sConfiden : string;
   idx : integer;
begin
    sConfiden := cdsCompany.FieldByName( 'CM_NIVEL0' ).AsString;

    Result := TRUE ;

    if strLleno( sConfiden )  then
    begin
         lista :=   TStringList.Create;
         lista.CommaText := sConfiden;
         Result := lista.Find(sNivel0, idx);
         FreeAndNil ( lista );
    end;
end;

function TBasicoCliente.ConstruyeListaEmpleados( oDataset: TDataset; const sFiltro : string ): string;

 const K_LIMITE_OBJETOS_FB = 1000; {Corrige defecto 113}
 var
    iCodigo : integer;
    sFiltroTemporal: string;
begin
     Result := '';
     with oDataSet do
     begin
          DisableControls;
          try
             if IsEmpty then
                Raise Exception.Create('Supervisor no Tiene Lista de Empleados');
             iCodigo := FieldByName('CB_CODIGO').AsInteger;
             First;
             while NOT EOF do
             begin
                  if ( RecNo MOD K_LIMITE_OBJETOS_FB ) = 0 then
                  begin
                       sFiltroTemporal := ConcatString( sFiltroTemporal, sFiltro + ' IN ('+Result+')', ' OR ' );
                       Result := VACIO;
                  end;

                  Result := ConcatString( Result, FieldByName('CB_CODIGO').AsString, ',' );
                  Next;
             end;

             if StrLleno(Result) or StrLLeno(sFiltroTemporal) then
             begin
                  Result := sFiltro + ' IN ('+Result+')';
                  Result := ConcatString( sFiltroTemporal, Result, ' OR ')
             end;
             Locate('CB_CODIGO', VarArrayOf([iCodigo]), [] );
          finally
                 EnableControls;
          end;
     end;
end;

function TBasicoCliente.GetModoTress: Boolean;
begin
     Result:=  not (
                     //ModoLabor or
                     //ModoServicioMedico or
                     //ModoPlanCarrera or
                     ModoSuper or
                     ModoSeleccion or
                     ModoVisitantes
                     //ModoEvaluacion or
                     //ModoCajaAhorro
                     );
end;

function TBasicoCliente.GetAppNumber: Integer;
var
   NumApp:TipoApp;
begin
    /// TipoApp     = ( taTress, taCajaAhorro, taSuper);
   if modoTress_DevEx then
      NumApp := taTress
   else if modoCajaAhorro then
        NumApp := taCajaAhorro
   else if modoSuper then
        NumApp :=taSuper
   else
       NumApp := taOtherApp;

   Result := Ord(NumApp);
end;
{$ifdef VALIDAEMPLEADOSGLOBAL}
function TBasicoCliente.GetSeguridadEmpresa: Boolean;
begin
     try
        FDatosSeguridadEmpresa := TClientGlobalLicenseValues.Create(  Servidor.GetSeguridadEmpresa( Empresa ) );

        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TBasicoCliente.EvaluarEmpleadosCliente(  operacion : eEvaluaOperacion; var sMensajeAdvertencia, sMensajeError : string ) : eEvaluaTotalEmpleados;
begin
     sMensajeAdvertencia := VACIO;
     sMensajeError := VACIO;
     Result := evIgnorar;
     if ( DatosSeguridadEmpresa <> nil ) then
     begin
          if DatosSeguridadEmpresa.TotalGlobal <= 0 then
             GetSeguridadEmpresa;
     end
     else
          GetSeguridadEmpresa;

     if ( DatosSeguridadEmpresa <> nil ) then
     begin
           // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
           GetSeguridad;
           DatosSeguridadEmpresa.MostrarAdvertencia := DatosSeguridad.AdvertenciaLicenciaEmpleados;

           Result := DatosSeguridadEmpresa.EvaluarEmpleadosCliente(  operacion , sMensajeAdvertencia, sMensajeError );
     end;
end;

function  TBasicoCliente.ValidaLicenciaEmpleados( operacion : eEvaluaOperacion) : boolean ;
var
   sMsgAdv, sMsgError : string;
   eEvalua : eEvaluaTotalEmpleados;
begin

      eEvalua := EvaluarEmpleadosCliente( operacion, sMsgAdv, sMsgError );

      if ( eEvalua= evRebasaRestringeDemo ) then
      begin
           sMsgAdv := sMsgError;
           sMsgError := VACIO;
      end;

      {$ifndef TRESS_REPORTES}
      if StrLleno(sMsgAdv) then
         ZWarning(  'Advertencia de Uso de Licencia', sMsgAdv, 0, mbOK );

      if StrLleno(sMsgError) then
         ZError(  'Restricci�n de Uso de Licencia', sMsgError, 0 );
      {$endif}

      Result := ( eEvalua <> evRebasaRestringe );
end;

function TBasicoCliente.EsTRESSPruebas : boolean;
begin
     Result := TRUE;

     if ( DatosSeguridadEmpresa <> nil ) then
     begin
           Result := ( DatosSeguridadEmpresa.TipoCompany = tc3Prueba )  and DatosSeguridadEmpresa.DebeValidar;
     end;

     {$ifdef TIMBRADO}{$ifdef PROFILE}Result := FALSE;{$endif} {$endif}

end;


{$endif}

function TBasicoCliente.CheckVersion:Boolean;
var
   Parametros:TZetaParams;
   procedure CargaParametros;
   begin
        Parametros := TZetaParams.Create;
        try
        try
           Parametros.VarValues := Servidor.CheckVersionDatos(SetCompany);
           with Parametros do
           begin
                FVersionDatos := ParamByName('VersionActual').AsInteger;
                FUltimaVersion := ParamByName('VersionUltima').AsInteger;
           end;
        except
              on Error: Exception do
              begin
                   FVersionDatos := -1;
                   FUltimaVersion := 0;
                   //ZError(  'Error', 'No se pudo conectar la empresa , entrar a configurar sistema para corregir la conexi�n', 0 );
              end;
        end;
        finally
               Parametros.Free;
        end;
   end;
begin
     CargaParametros;
     {**(@am): Se modifica la expresion a >= para que no salga la advertencia de actualizacion, cuando la version de la bd
               sea mayor al parche instalado***}
     Result := FVersionDatos >= FUltimaVersion;
end;

function TBasicoCliente.CheckDBServer:Boolean;
begin
     Result := Servidor.CheckDBServer(Empresa) = 1;
end;

function TBasicoCliente.Sentinel_EsProfesional_MSSQL:Boolean;
begin
     with Autorizacion do
     begin
          Cargar(Servidor.GetAuto);
          Result := ( (not EsDemo) and (Plataforma = ptProfesional) and (SQLEngine = engMSSQL));
     end;
end;

function TBasicoCliente.TipoApplicacionStr:String;
begin
     with Autorizacion do
     begin
          Result := GetPlataformaStr 
     end;
end;

function TBasicoCliente.GetSkinController: TdxSkinController;
begin
     Result:=nil;
end;

{***US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
    Valores de Regreso
     lLoop (TRUE): Indica si se debe reintentar la conexion muestra [ Connection Problem Dialog ]
     lSalirTress: Indica que el usuario decide Salir del Proceso
     sMensajeError: Mensaje de Error al realizar la conexion COM o HTTP
     lModoBatch: Indica que la aplicacion se ejecua en Linea de comando windows (cmd)
                 Por lo cual no debe mostrar [ Connection Problem Dialog ] u otros componentes visuales.
***}
function TBasicoCliente.ValidarConexionHTTP( var lLoop: Boolean; var lSalirTress: Boolean; var sMensajeError: String; lModoBatch: Boolean = False ): Boolean;
var
   lAutorizado: Boolean;
   sMensaje: String;

   function TipoConexionHTTP: Boolean;
   begin
        Result := False;
        if ClientRegistry.TipoConexion = conxHTTP then
           Result := True;
   end;
begin
     lSalirTress    := False;
     //Result := False;
     lAutorizado := False;
     try
        {$ifdef HTTP_CONNECTION}
        if not lModoBatch then
        begin
             case ClientRegistry.TipoConexion of
                  conxHTTP:
                  begin
                       if (not Assigned (FWebConnection)) then
                       begin
                            FWebConnection := TWebConnection.Create( Self );
                            FWebConnection.LoginPrompt := False;
                            FWebConnection.Agent :=  'GTI';
                       end;
                       with FWebConnection do
                       begin
                            Connected := False;
                            URL := ClientRegistry.URL; //'http://www.tress.com.mx/TressRemoto/httpsrvr.dll';
                            Username := ClientRegistry.UserName;
                            Password := ClientRegistry.Password;
                            ServerGUID := GUIDToString( CLASS_dmServerLogin );
                            Connected := True;
                       end;
                       lAutorizado := ModuloAutorizadoConsulta( okTressHTTP, sMensaje );
                       if not lAutorizado then
                          raise Exception.Create( sMensaje );
                  end
                  else
                  begin
                       lLoop := False;
                  end;
             end;
        end
        else
            lAutorizado := ModuloAutorizadoConsulta( okTressHTTP, sMensaje );
        {$else}
        lLoop := False;
        {$endif}
        lLoop := False;
        if lModoBatch and not lAutorizado then
             sMensajeError := sMensaje;
     except
           on Error: Exception do
           begin
                if lModoBatch then
                begin
                     lLoop := False;
                     lSalirTress := True;
                     sMensajeError := Error.Message;
                end
                else
                begin
                     UsuarioLogout;//US 12375: Al momento de mostrar ventana de validaci�n de licencia, deja marcado al usuario como que est� dentro del Sistema
                     {$ifndef TRESS_REPORTES}
                     case ZetaConnectionProblem_DevEx.ConnectionProblemDialog( Error, TipoConexionHTTP ) of    //DevEx(by am)
                          mrRetry:
                          begin
                               lLoop := True;
                          end
                     else
                         begin
                              lLoop := False;
                              if TipoConexionHTTP then
                                 lSalirTress := True;
                         end;
                     end;
                     {$endif}
                end;
           end;
     end;
     Result := lAutorizado;
end;

function TBasicoCliente.GetEmpresas: OleVariant;
begin
     Result := Servidor.GetCompanys( 0, Ord( tc3Datos ) );
end;

end.
