unit dIMSS;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
{$ifdef DOS_CAPAS}
     DServerIMSS,
{$else}
     IMSS_TLB,
{$endif}
     ZetaCommonClasses,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaClientDataSet;

type
  TdmIMSS = class(TDataModule)
    cdsIMSSDatosBim: TZetaClientDataSet;
    cdsIMSSDatosMen: TZetaClientDataSet;
    cdsMovBim: TZetaClientDataSet;
    cdsMovMen: TZetaClientDataSet;
    cdsIMSSDatosTot: TZetaClientDataSet;
    cdsIMSSHisMen: TZetaClientDataSet;
    cdsIMSSHisBim: TZetaClientDataSet;
    cdsMovHisBim: TZetaClientDataSet;
    cdsMovHisMen: TZetaClientDataSet;
    cdsHisConciliaInfo: TZetaClientDataSet;
    cdsHisTotalBim: TZetaClientDataSet;
    cdsMovimientosIDSE: TZetaClientDataSet;
    cdsMovimientosIDSENSS: TStringField;
    cdsMovimientosIDSETIPO: TSmallintField;
    cdsMovimientosIDSEFECHA: TDateField;
    cdsMovimientosIDSEPAGINA: TIntegerField;
    cdsMovimientosIDSEVALIDO: TBooleanField;
    cdsMovimientosIDSETEXTO: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    {$ifdef VER130}
    procedure ImssReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure ImssReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsIMSSDatosBimAlAdquirirDatos(Sender: TObject);
    procedure cdsIMSSDatosMenAlAdquirirDatos(Sender: TObject);
    procedure cdsIMSSDatosTotAlAdquirirDatos(Sender: TObject);
    procedure cdsIMSSHisMenAlAdquirirDatos(Sender: TObject);
    procedure cdsIMSSHisBimAlAdquirirDatos(Sender: TObject);
    procedure cdsMovBimAlCrearCampos(Sender: TObject);
    procedure cdsMovMenAlCrearCampos(Sender: TObject);
    procedure cdsMovHisBimAlCrearCampos(Sender: TObject);
    procedure cdsMovHisBimAlAdquirirDatos(Sender: TObject);
    procedure cdsMovHisMenAlCrearCampos(Sender: TObject);
    procedure cdsMovHisMenAlAdquirirDatos(Sender: TObject);
    procedure cdsMovMenAlAdquirirDatos(Sender: TObject);
    procedure cdsMovBimAlAdquirirDatos(Sender: TObject);
    procedure cdsIMSSDatosTotAlAgregar(Sender: TObject);
    procedure cdsIMSSDatosTotAlBorrar(Sender: TObject);
    procedure cdsIMSSDatosTotNewRecord(DataSet: TDataSet);
    procedure cdsIMSSDatosTotAlEnviarDatos(Sender: TObject);
    procedure cdsHisTotalBimAlAdquirirDatos(Sender: TObject);
    procedure cdsHisTotalBimAlCrearCampos(Sender: TObject);
    procedure cdsHisConciliaInfoAlAdquirirDatos(Sender: TObject);
    procedure cdsHisConciliaInfoCalcFields(DataSet: TDataSet);
    procedure cdsHisConciliaInfoAlCrearCampos(Sender: TObject);
    procedure cdsIMSSDatosAlModificar(Sender: TObject);
  private
    { Private declarations }
    FPrimaYear: Integer;
    FPrima: TTasa;
    FPatron: String;
{$ifdef DOS_CAPAS}
    function GetServerIMSS: TdmServerImss;
    property ServerIMSS: TdmServerImss read GetServerIMSS;
{$else}
    FServidor: IdmServerIMSSDisp;
    function GetServerIMSS: IdmServerImssDisp;
    property ServerIMSS: IdmServerImssDisp read GetServerIMSS;
{$endif}
    function GetDataSetMovimientosIDSE : TZetaClientDataSet;
    procedure IniciarCdsMovimientosIDSE;
  public
    { Public declarations }
    function GetPrimaRiesgo(const iYear: Integer; const sPatron: String): TTasa;

    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
    {$endif}
    procedure AplicaDecreto20( var lDescuento20,lParcialidades: Boolean );

    property DataSetMovimientosIDSE: TZetaClientDataSet read GetDataSetMovimientosIDSE;


  end;

var
  dmIMSS: TdmIMSS;

implementation

{$R *.DFM}

uses DCliente,
     ZBaseEdicion_DevEx,
     ZReconcile,
     ZetaDialogo,
     FTressShell,
     dCatalogos,
     FIMSSPagoNuevo_DevEx,
     FArchivoIDSE,
     FEditIMSSDatosTot_DevEx;

procedure TdmIMSS.DataModuleCreate(Sender: TObject);
begin
     FPrimaYear := 0;
     FPatron := '';
     FPrima := 0;
     //Data Module ArchivoIDSE  (Utilitario)
     ArchivoIDSE := TArchivoIDSE.Create( Self );
end;

{$ifdef DOS_CAPAS}
function TdmIMSS.GetServerIMSS: TdmServerImss;
begin
     Result := DCliente.dmCliente.ServerIMSS;
end;
{$else}
function TdmIMSS.GetServerIMSS: IdmServerImssDisp;
begin
     Result := IdmServerImssDisp( dmCliente.CreaServidor( CLASS_dmServerImss, FServidor ) );
end;
{$endif}

{$ifdef VER130}
procedure TdmIMSS.ImssReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmIMSS.ImssReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmIMSS.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmIMSS.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enLiq_IMSS , Entidades ) or ( Estado = stIMSS ) then
     {$else}
     if ( enLiq_IMSS in Entidades ) or ( Estado = stIMSS ) then
     {$endif}
     begin
          cdsIMSSDatosTot.SetDataChange;
          cdsIMSSDatosBim.SetDataChange;
          cdsMovBim.SetDataChange;
          cdsIMSSDatosMen.SetDataChange;
          cdsMovMen.SetDataChange;
          cdsIMSSHisBim.SetDataChange;
          cdsMovHisBim.SetDataChange;
          cdsIMSSHisMen.SetDataChange;
          cdsMovHisMen.SetDataChange;
          cdsHisConciliaInfo.SetDataChange;
          cdsHisTotalBim.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enLiq_Emp , Entidades ) or ( Estado = stEmpleado ) then
     {$else}
     if ( enLiq_Emp in Entidades ) or ( Estado = stEmpleado ) then
     {$endif}
     begin
          cdsIMSSDatosBim.SetDataChange;
          cdsMovBim.SetDataChange;
          cdsIMSSDatosMen.SetDataChange;
          cdsMovMen.SetDataChange;
          cdsIMSSHisBim.SetDataChange;
          cdsMovHisBim.SetDataChange;
          cdsIMSSHisMen.SetDataChange;
          cdsMovHisMen.SetDataChange;
          cdsHisConciliaInfo.SetDataChange;
          cdsHisTotalBim.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enLiq_Mov , Entidades ) then
     {$else}
     if ( enLiq_Mov in Entidades ) then
     {$endif}
     begin
          cdsMovBim.SetDataChange;
          cdsMovMen.SetDataChange;
          cdsMovHisBim.SetDataChange;
          cdsMovHisMen.SetDataChange;
          cdsHisConciliaInfo.SetDataChange;
          cdsHisTotalBim.SetDataChange;
     end;
end;

function TdmIMSS.GetPrimaRiesgo(const iYear: Integer; const sPatron: String): TTasa;
begin
     if ( iYear <> FPrimaYear ) or ( sPatron <> FPatron ) then
     begin
          FPrima := ServerIMSS.GetPrimaRiesgo( dmCliente.Empresa, EncodeDate( iYear, 12, 31 ), sPatron );
          FPrimaYear := iYear;
          FPatron := sPatron;
     end;
     Result := FPrima;
end;

{ cdsIMSSDatosBim }

procedure TdmIMSS.cdsIMSSDatosBimAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsIMSSDatosBim.Data := ServerIMSS.GetIMSSDatosBim( Empresa, IMSSYear, IMSSMes,
                                                              Ord( IMSSTipo ), Empleado,
                                                              IMSSPatron );
     end;
end;



{ cdsMovBim }

procedure TdmIMSS.cdsMovBimAlAdquirirDatos(Sender: TObject);
begin
     with cdsIMSSDatosBim do
     begin
          cdsMovBim.Data := ServerIMSS.GetMovIMSSDatosBim( dmCliente.Empresa,
                                                           FieldByName( 'LS_YEAR' ).AsInteger,
                                                           FieldByName( 'LS_MONTH' ).AsInteger,
                                                           FieldByName( 'LS_TIPO' ).AsInteger,
                                                           FieldByName( 'CB_CODIGO' ).AsInteger,
                                                           FieldByName( 'LS_PATRON' ).AsString );
     end;
end;

procedure TdmIMSS.cdsMovBimAlCrearCampos(Sender: TObject);
begin
     with cdsMovBim do
     begin
          MaskFecha( 'LM_FECHA' );
          MaskPesos( 'LM_BASE' );
          MaskPesos( 'LM_RETIRO' );
          MaskPesos( 'LM_CES_VEJ' );
          MaskPesos( 'LM_INF_PAT' );
          MaskPesos( 'LM_INF_AMO' );
     end;
end;

{ cdsIMSSDatosMen }

procedure TdmIMSS.cdsIMSSDatosMenAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsIMSSDatosMen.Data := ServerIMSS.GetIMSSDatosMen( Empresa, IMSSYear, IMSSMes,
                                                              Ord( IMSSTipo ), Empleado,
                                                              IMSSPatron );
     end;
end;

{ cdsMovMen }

procedure TdmIMSS.cdsMovMenAlAdquirirDatos(Sender: TObject);
begin
     with cdsIMSSDatosMen do
     begin
          cdsMovMen.Data := ServerIMSS.GetMovIMSSDatosMen( dmCliente.Empresa,
                                                           FieldByName( 'LS_YEAR' ).AsInteger,
                                                           FieldByName( 'LS_MONTH' ).AsInteger,
                                                           FieldByName( 'LS_TIPO' ).AsInteger,
                                                           FieldByName( 'CB_CODIGO' ).AsInteger,
                                                           FieldByName( 'LS_PATRON' ).AsString );
     end;
end;

procedure TdmIMSS.cdsMovMenAlCrearCampos(Sender: TObject);
begin
     with cdsMovMen do
     begin
          MaskFecha( 'LM_FECHA' );
          MaskPesos( 'LM_EYM_FIJ' );
          MaskPesos( 'LM_EYM_EXC' );
          MaskPesos( 'LM_EYM_DIN' );
          MaskPesos( 'LM_EYM_ESP' );
          MaskPesos( 'LM_RIESGOS' );
          MaskPesos( 'LM_INV_VID' );
          MaskPesos( 'LM_GUARDER' );
     end;
end;

{ cdsIMSSHisMen }

procedure TdmIMSS.cdsIMSSHisMenAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsIMSSHisMen.Data := ServerIMSS.GetIMSSHisMen( Empresa, IMSSYear, IMSSMes,
                                                          Ord( IMSSTipo ), IMSSPatron );
     end;
end;

{ cdsMovHisMen }

procedure TdmIMSS.cdsMovHisMenAlAdquirirDatos(Sender: TObject);
begin
     with cdsIMSSHisMen do
     begin
          cdsMovHisMen.Data := ServerIMSS.GetMovIMSSHisMen( dmCliente.Empresa,
                                                            FieldByName( 'LS_YEAR' ).AsInteger,
                                                            FieldByName( 'LS_MONTH' ).AsInteger,
                                                            FieldByName( 'LS_TIPO' ).AsInteger,
                                                            FieldByName( 'LS_PATRON' ).AsString );
     end;
end;

procedure TdmIMSS.cdsMovHisMenAlCrearCampos(Sender: TObject);
begin
     cdsMovHisMen.MaskPesos( 'LE_TOT_IMS' );
end;

{ cdsIMSSHisBim }

procedure TdmIMSS.cdsIMSSHisBimAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsIMSSHisBim.Data := ServerIMSS.GetIMSSHisBim( Empresa, IMSSYear, IMSSMes,
                                                          Ord( IMSSTipo ), IMSSPatron );
     end;
end;

{ cdsMovHisBim }

procedure TdmIMSS.cdsMovHisBimAlAdquirirDatos(Sender: TObject);
begin
     with cdsIMSSHisBim do
     begin
          cdsMovHisBim.Data := ServerIMSS.GetMovIMSSHisBim( dmCliente.Empresa,
                                                            FieldByName( 'LS_YEAR' ).AsInteger,
                                                            FieldByName( 'LS_MONTH' ).AsInteger,
                                                            FieldByName( 'LS_TIPO' ).AsInteger,
                                                            FieldByName( 'LS_PATRON' ).AsString );
     end;
end;

procedure TdmIMSS.cdsMovHisBimAlCrearCampos(Sender: TObject);
begin
     with cdsMovHisBim do
     begin
          MaskPesos( 'LE_TOT_RET' );
          MaskPesos( 'LE_TOT_INF' );
     end;
end;

{ cdsIMSSDatosTot }

procedure TdmIMSS.cdsIMSSDatosTotAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsIMSSDatosTot.Data := ServerIMSS.GetIMSSDatosTot( Empresa, IMSSYear, IMSSMes,
                                                              Ord( IMSSTipo ), IMSSPatron );
     end;
end;

procedure TdmIMSS.cdsIMSSDatosTotNewRecord(DataSet: TDataSet);
begin
     with cdsIMSSDatosTot do
     begin
          with dmCliente do
          begin
               FieldByName( 'LS_YEAR' ).AsInteger := IMSSYear;
               FieldByName( 'LS_MONTH' ).AsInteger := IMSSMes;
               FieldByName( 'LS_TIPO' ).AsInteger := Ord( IMSSTipo );
               FieldByName( 'US_CODIGO' ).Asinteger := Usuario;
               FieldByName( 'LS_PATRON' ).AsString := IMSSPatron;
          end;
          FieldByName( 'LS_STATUS' ).AsInteger := Ord( slSinCalcular );
          FieldByName( 'LS_FEC_REC' ).AsDateTime := NullDateTime;
     end;
end;

procedure TdmIMSS.cdsIMSSDatosTotAlAgregar(Sender: TObject);
begin
     cdsIMSSDatosTot.Append;
     ZBaseEdicion_DevEx.ShowFormaEdicion( IMSSPagoNuevo_DevEx, TIMSSPagoNuevo_DevEx );
end;

procedure TdmIMSS.cdsIMSSDatosTotAlBorrar(Sender: TObject);
begin
     with cdsIMSSDatosTot do
     begin
          if ( RecordCount > 0 ) then       // Tiene Registro de Liquidación de IMSS
          begin
               if ZConfirm( '¡ Atención !',
                            'El Registro de IMSS < ' + dmCliente.GetIMSSDescripcion + ' >' +
                            CR_LF +
                            'Contiene Aportaciones Calculadas de ' +
                            FieldByName( 'LS_NUM_TRA' ).AsString +
                            ' Empleados.' +
                            CR_LF +
                            '¿ Desea BORRARLO Completamente del Sistema ?', H40432_Borrar_pago_IMSS_activo, mbNo ) then
               begin
                    Delete;
                    Enviar;
               end;
          end
          else
              zInformation( '¡ Error !', 'No Hay Datos Para Borrar', 0 );
     end;
end;

procedure TdmIMSS.cdsIMSSDatosTotAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsIMSSDatosTot do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerImss.GrabaIMSSDatosTot( dmCliente.Empresa, Delta, ErrorCount ) );
               with TressShell do
               begin
                    SetDataChange( [ enLiq_IMSS ] );
               end;
          end;
     end;
end;


{ cdsHisTotalBim }

procedure TdmIMSS.cdsHisTotalBimAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsHisTotalBim.Data := ServerIMSS.GetIMSSDatosBim( Empresa, IMSSYear, IMSSMes,
                                                              Ord( IMSSTipo ), Empleado,
                                                              VACIO );
     end;
end;

procedure TdmIMSS.cdsHisTotalBimAlCrearCampos(Sender: TObject);
begin
     with cdsHisTotalBim do
     begin
          MaskPesos('LE_INF_AMO');
          MaskPesos('LE_TOT_INF');
          dmCatalogos.cdsRPatron.Conectar;
          CreateSimpleLookup ( dmCatalogos.cdsRPatron, 'TB_ELEMENT', 'LS_PATRON' );
     end;
end;


{ cdsConciliaInfo}


procedure TdmIMSS.cdsHisConciliaInfoAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsHisConciliaInfo.Data:= ServerIMSS.GetDatosConciliaInfo( Empresa, IMSSYear, IMSSMes, Empleado );
     end;
end;

procedure TdmIMSS.cdsHisConciliaInfoCalcFields(DataSet: TDataSet);
begin
     with Dataset do
     begin
          FieldByName( 'LE_BIM_ACTUAL' ).AsFloat :=  FieldByName( 'LE_ACUMULA' ).AsFloat + FieldByName( 'LE_BIM_ANT' ).AsFloat;
          FieldByName( 'LE_BIM_CALC' ).AsFloat := FieldByName( 'LE_BIM_ACTUAL' ).AsFloat + FieldByName( 'LE_BIM_SIG' ).AsFloat;
          FieldByName( 'LE_DIF_PROV' ).AsFloat := Abs( FieldByName( 'LE_INF_AMO' ).AsFloat - FieldByName( 'LE_BIM_CALC' ).AsFloat );
          FieldByName( 'LE_DIF_ACUM' ).AsFloat := Abs( FieldByName( 'LE_INF_AMO' ).AsFloat - FieldByName( 'LE_ACUMULA' ).AsFloat ) ;//AV 16-Mar-11
     end;
end;

procedure TdmIMSS.cdsHisConciliaInfoAlCrearCampos(Sender: TObject);
begin
     with cdsHisConciliaInfo do
     begin
          CreateCalculated( 'LE_BIM_ACTUAL', ftFloat, 0 );
          CreateCalculated( 'LE_BIM_CALC', ftFloat, 0 );
          CreateCalculated( 'LE_DIF_PROV', ftFloat, 0 );
          CreateCalculated( 'LE_DIF_ACUM', ftFloat,0  );//AV 16-Mar-11
          MaskPesos('LE_DIF_ACUM');
     end;
end;

procedure TdmIMSS.AplicaDecreto20( var lDescuento20,lParcialidades: Boolean );
begin
     {Decreto emitido el 7 de Mayo del 2009: Ofrece un descuento del 20% sobre cuotas patronales. Esto aplica únicamente a los meses de Mayo y Junio del 2009.
      Decreto emitido el 14 de Mayo del 2009. Ofrece a los patrones la facilidad de  presentar los pagos correspondientes a las cuotas patronales en parcialidades
      Aplica hasta noviembre}
     with dmCliente do
     begin
          lParcialidades:= ( IMSSYear = 2009 ) and ( IMSSMes > 5  ) and ( IMSSMes <= 11 );
          lDescuento20:= ( IMSSYear = 2009 ) and ( IMSSMes in [5, 6]  );
     end;
end;

procedure TdmIMSS.cdsIMSSDatosAlModificar(Sender: TObject);
begin
     ZBaseEdicion_DevEx.ShowFormaEdicion( EditIMSSDatosTot_DevEx, TEditIMSSDatosTot_DevEx );
end;

procedure TdmIMSS.IniciarCdsMovimientosIDSE;
begin
    with cdsMovimientosIDSE do
    begin
         if not Active then            // Para que solo se realize CreateDataSet una vez
         begin
              CreateDataSet;
              Open;
         end;
    end;
end;

function TdmIMSS.GetDataSetMovimientosIDSE : TZetaClientDataSet;
begin
     IniciarCdsMovimientosIDSE;
     Result := cdsMovimientosIDSE;
end;

end.
