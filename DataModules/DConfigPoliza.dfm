object dmConfigPoliza: TdmConfigPoliza
  OldCreateOrder = False
  Left = 192
  Top = 114
  Height = 480
  Width = 696
  object cdsTemporal: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 128
    Top = 189
  end
  object cdsCtaConcepto: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 128
    Top = 64
  end
  object cdsGrupo1: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 64
  end
  object cdsGrupo2: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 128
  end
  object cdsGrupo3: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 184
  end
  object cdsGrupo4: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 248
  end
  object cdsGrupo5: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 312
  end
  object cdsPoliza: TServerDataSet
    Aggregates = <>
    Params = <>
    Left = 128
    Top = 128
  end
end
