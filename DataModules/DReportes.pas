unit DReportes;

interface
{$INCLUDE DEFINES.INC}

{$ifdef TRESS_DELPHIXE5_UP}
{$DEFINE SUSCRIPCION}
{$endif}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient,

  {$ifdef DOS_CAPAS}
     DServerReporting,
  {$else}
      //Reporteador_TLB,
  {$endif}
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaTipoEntidad,
  ZReportTools,
  DBaseReportes,
       Variants;


  {$ifdef DOS_CAPAS}
  type
    TdmServerCalcNomina = TdmServerReporting;
  {$else}
    //IdmServerCalcNominaDisp = IdmServerReportingDisp;
  {$endif}

type
  TdmReportes = class(TdmBaseReportes)
    cdsSuscripReportesLookup: TZetaLookupDataSet;
    cdsLookupReportesEmpresa: TZetaLookupDataSet;
    procedure cdsCampoRepAlCrearCampos(Sender: TObject);
    procedure cdsReportesAlModificar(Sender: TObject);
    procedure cdsSuscripReportesLookupLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: string; var sKey, sDescription: string);
    procedure cdsSuscripReportesAlCrearCampos(Sender: TObject);
    procedure cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripReportesLookupAlCrearCampos(Sender: TObject);
    procedure cdsSuscripReportesLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsLookupReportesEmpresaAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FClasifActivo: eClasifiReporte;
    FClasificaciones: TStrings;
    FEmpresaReportes: Variant;
 {$ifdef DOS_CAPAS}
     function GetServerReportesBDE: TdmServerCalcNomina;
     property ServerReportesBDE: TdmServerCalcNomina read GetServerReportesBDE;
 {$else}
     {FServidorBDE: IdmServerCalcNominaDisp;
     function GetServerReportesBDE: IdmServerCalcNominaDisp;
     property ServerReportesBDE: IdmServerCalcNominaDisp read GetServerReportesBDE;}
 {$endif}
    procedure GrabaFotos;
    procedure GrabaDocumentos; // (JB) Anexar al expediente del trabajador documentos CR1880 T1107
    procedure ObtieneEntidadGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    function GetClasificaciones(oClasificaciones: TStrings): string;
  protected
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList: OleVariant;
                        var sError: WideString): OleVariant;override;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;override;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;override;
    procedure GetFormaReporte( const eTipo: eTipoReporte );override;
    procedure AfterGeneraSQL;override;
  public
    { Public declarations }
    //function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
    procedure CancelaCuentasPoliza;
    procedure LlenaConceptos(eTiposConcepto: TiposConcepto);
    procedure BuscaConcepto;override;
    function DirectorioPlantillas: string;override;
    property ClasifActivo: eClasifiReporte read FClasifActivo write FClasifActivo;
    property Clasificaciones: TStrings read FClasificaciones write FClasificaciones;
    property EmpresaReportes: Variant read FEmpresaReportes write FEmpresaReportes;
  end;

var
  dmReportes: TdmReportes;

implementation
uses DCliente,
     DCatalogos,
     DGlobal,
     ZReportModulo,
     ZGlobalTress,
     ZAccesosTress,
     ZetaCommonTools,
     ZetaDialogo
     {$ifdef SUSCRIPCION}
     ,ZReportConst,
     FLookupReporte_DevEx,
     dSistema
     {$endif} ,
{$IFDEF TRESS_DELPHIXE5_UP}
     FPolizaConcepto_DevEx,
     FPoliza_DevEx, DDiccionario;//Forma poliza DevEx
{$ELSE}
     FPolizaConcepto,
     FPoliza;//Forma poliza
{$ENDIF}

{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmReportes.GetServerReportesBDE: TdmServerCalcNomina;
begin
     Result := DCliente.dmCliente.ServerReporteador;
end;
{$else}
{$ifdef FALSE}
function TdmReportes.GetServerReportesBDE: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( {$ifdef REPORTING}CLASS_dmServerReporting{$ELSE}CLASS_dmServerCalcNomina{$ENDIF}, FServidorBDE ) );
end;
{$ENDIF}
{$endif}

function TdmReportes.GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ) : OleVariant;
begin
     Result := {$ifdef RDD}ServerReportes{$else}ServerReportesBDE{$endif}.GeneraSQL( dmCliente.Empresa, oSQLAgente, ParamList , sError  );
end;

procedure TdmReportes.AfterGeneraSQL;
begin
     GrabaFotos;
     GrabaDocumentos;
end;

procedure TdmReportes.GrabaFotos;
var
    FBlob: TBlobField;
    iColumna, iEmpleado: integer;
    sTipo, sDirectorio,sDirectorio2: string;
    lExportarImagen: Boolean;
begin
     if ParamList.IndexOf('GRABA_IMAGEN_DISCO') <> -1 then
     begin
          try
             with ParamList do
             begin
                  iColumna := ParamByName('COL_IMAGEN_DISCO').AsInteger;
                  sTipo := ParamByName('TIPO_IMAGEN_DISCO').AsString;
                  sDirectorio := ParamByName('GRABA_IMAGEN_DISCO').AsString;
                  sDirectorio2 := StrTransAll( sDirectorio, '\', '\\');
             end;

             with cdsResultados do
             begin
                  DisableControls;
                  try
                     First;
                     while NOT EOF do
                     begin
                          lExportarImagen := FALSE;
                          iEmpleado := Fields[iColumna].AsInteger;
                          if ( iEmpleado > 0 ) then
                          begin
                               {$ifdef RDD}
                               cdsBlobs.Data := ServerReportes.GetFoto( dmCliente.Empresa, iEmpleado, sTipo );
                               {$else}
                               cdsBlobs.Data := ServerReportesBDE.GetFoto( dmCliente.Empresa, iEmpleado, sTipo );
                               {$endif}

                               FBlob := TBlobField( cdsBlobs.FieldByName( 'CampoBlob' ) );
                               lExportarImagen := ( not FBlob.IsNull );
                               if lExportarImagen then
                                  FBlob.SaveToFile( sDirectorio + IntToStr( iEmpleado ) + '.jpg');
                          end;
                          Edit;
                          if lExportarImagen then
                             Fields[iColumna].AsString := sDirectorio2 + IntToStr( iEmpleado ) + '.jpg'
                          else
                              Fields[iColumna].AsString := VACIO;
                          Post;

                          Next;
                     end;
                  finally
                         MergeChangeLog;
                         First;
                         EnableControls;
                  end;
             end;
          finally
          end;
     end;
end;

procedure TdmReportes.GrabaDocumentos;
var
    FBlob: TBlobField;
    iColumna, iEmpleado: integer;
    sTipo, sDirectorio,sDirectorio2 : string;
    sExtension : WideString;
    lExportarArchivo: Boolean;
begin

     sExtension := '';

     if ParamList.IndexOf('GRABA_DOCUMENTO_DISCO') <> -1 then
     begin
          try
             with ParamList do
             begin
                  iColumna := ParamByName('COL_DOCUMENTO_DISCO').AsInteger;
                  sTipo := ParamByName('TIPO_DOCUMENTO_DISCO').AsString;
                  sDirectorio := ParamByName('GRABA_DOCUMENTO_DISCO').AsString;
                  sDirectorio2 := StrTransAll( sDirectorio, '\', '\\');
             end;

             with cdsResultados do
             begin
                  DisableControls;
                  try
                     First;
                     while NOT EOF do
                     begin
                          lExportarArchivo := FALSE;
                          iEmpleado := Fields[iColumna].AsInteger;
                          if ( iEmpleado > 0 ) then
                          begin
                               {$ifdef RDD}
                               cdsBlobs.Data := ServerReportes.GetDocumento( dmCliente.Empresa, iEmpleado, sTipo, sExtension);
                               {$else}
                               cdsBlobs.Data := ServerReportesBDE.GetDocumento( dmCliente.Empresa, iEmpleado, sTipo , sExtension);
                               {$endif}

                               FBlob := TBlobField( cdsBlobs.FieldByName( 'DO_BLOB' ) );
                               lExportarArchivo := ( not FBlob.IsNull );
                               if lExportarArchivo then
                                  FBlob.SaveToFile( sDirectorio + IntToStr( iEmpleado ) + '_' + sTipo + '.' + sExtension);
                          end;
                          Edit;
                          if lExportarArchivo then
                              Fields[iColumna].AsString := sDirectorio2 + IntToStr( iEmpleado ) + '_' + sTipo + '.' + sExtension
                          else
                              Fields[iColumna].AsString := VACIO;
                          Post;

                          Next;
                     end;
                  finally
                         MergeChangeLog;
                         First;
                         EnableControls;
                  end;
             end;
          finally
          end;
     end;
end;


procedure TdmReportes.GetFormaReporte( const eTipo : eTipoReporte );
begin
     inherited GetFormaReporte( eTipo );
{$IFDEF TRESS_DELPHIXE5_UP}
     case eTipo of
          trPoliza : ShowFormaReporte( Poliza_DevEx, TPoliza_DevEx, SoloLectura, SoloImpresion );
          trPolizaConcepto : ShowFormaReporte( PolizaConcepto_DevEx, TPolizaConcepto_DevEx, SoloLectura, SoloImpresion );
     end;
{$ELSE}
     case eTipo of
          trPoliza : ShowFormaReporte( Poliza, TPoliza, SoloLectura, SoloImpresion );
          trPolizaConcepto : ShowFormaReporte( PolizaConcepto, TPolizaConcepto, SoloLectura, SoloImpresion );
     end;
{$ENDIF}
end;

{$ifdef COMENTARIOS}

function TdmReportes.GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;
begin
     Result := ZReportModulo.GetDerechosClasifi(eClasifi);
     case eClasifi of
          crEmpleados  : Result:= D_REPORTES_EMPLEADOS;
          crCursos     : Result:= D_REPORTES_CURSOS;
          crAsistencia : Result:= D_REPORTES_ASISTENCIA;
          crNominas    : Result:= D_REPORTES_NOMINA;
          crPagosIMSS  : Result:= D_REPORTES_IMSS;
          crConsultas  : Result:= D_REPORTES_CONSULTAS;
          crCatalogos  : Result:= D_REPORTES_CATALOGOS;
          crTablas     : Result:= D_REPORTES_TABLAS;
          crSupervisor : Result:= D_REPORTES_SUPERVISORES;
          crCafeteria  : Result:= D_REPORTES_CAFETERIA;
          crLabor      : Result:= D_REPORTES_LABOR;
          crMedico     : Result:= D_REPORTES_MEDICOS;
          {$ifdef CARRERA}
          crCarrera    : Result:= D_REPORTES_CARRERA;
          {$endif}
          crMigracion  : Result:= D_REPORTES_MIGRACION

          else Result:= 0;
     end;
end;
{$ENDIF}


function TdmReportes.ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;
begin
     Result := {$ifdef RDD}ServerReportes{$else}ServerReportesBDE{$endif}.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParamList, sError );
end;

function TdmReportes.ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;
begin
     //CV: 17-junio-2005
     //La implementacion de esta parte se realizara cuando cambie la llamada de DDiccionario.PruebaFormula
     Result := FALSE;
end;



procedure TdmReportes.CancelaCuentasPoliza;
begin
     if ( cdsCampoRep.ChangeCount > 0 ) then
        cdsCampoRep.CancelUpdates;
end;

procedure TdmReportes.cdsCampoRepAlCrearCampos(Sender: TObject);
begin
     inherited;
     if cdsEditReporte.Active and (eTipoReporte( cdsEditReporte.FieldByName('RE_TIPO').AsInteger ) in [trPoliza, trPolizaConcepto] ) then
     begin
          dmCatalogos.cdsConceptosLookup.Conectar;
          if dmCatalogos.cdsConceptosLookup.Active then
             cdsCampoRep.CreateSimpleLookup(dmCatalogos.cdsConceptosLookup, 'CO_DESCRIP','CR_CALC');
     end;
end;


procedure TdmReportes.cdsLookupReportesEmpresaAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     {$ifdef SUSCRIPCION}
     cdsLookupReportesEmpresa.Data := ServerReportes.GetLookUpReportes(EmpresaReportes, dmDiccionario.VerConfidencial);
     {$endif}
end;

procedure TdmReportes.LlenaConceptos( eTiposConcepto : TiposConcepto );
const
     K_CARGO = 0;
     K_ABONO = 1;
     aAsiento: array[ false..true ] of integer = ( K_CARGO, K_ABONO );
var
   eTipo: eTipoConcepto;
begin
     with dmCatalogos.cdsConceptosLookup do
     begin
          try
             Conectar;
             if Active then
             begin
                  DisableControls;
                  First;
                  cdsCampoRep.DisableControls;
                  while not Eof do
                  begin
                       eTipo := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
                       if ( eTipo in eTiposConcepto ) then
                       begin
                            if NOT cdsCampoRep.Locate('CR_CALC', FieldByName('CO_NUMERO').AsInteger, []) then
                            begin
                                 cdsCampoRep.Append;
                                 cdsCampoRep.FieldByName( 'CR_CALC' ).AsInteger := FieldByName( 'CO_NUMERO' ).AsInteger;
                                 cdsCampoRep.FieldByName( 'CR_OPER' ).AsInteger := aAsiento[ ( eTipo in [coDeduccion,coObligacion] ) ];
                                 cdsCampoRep.Post;
                            end;
                       end;
                       Next;
                  end;
             end;
          finally
            cdsCampoRep.EnableControls;
            EnableControls;
          end;
     end;
end;

procedure TdmReportes.BuscaConcepto;
var
   sKey, sDescription: String;
begin
     if dmCatalogos.cdsConceptosLookUp.Search_DevEx( VACIO, sKey, sDescription ) then
     begin
          with cdsCampoRep do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CR_CALC' ).AsString := sKey;
          end;
     end;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     {$ifdef RDD}
     with dmCliente do
          Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
     {$else}
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
     {$endif}

end;

procedure TdmReportes.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
{ var
    Pendientecaro: integer;}
begin
     {$ifdef SUSCRIPCION}
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              {$ifdef RDD}
              Text := VACIO;
              {$else}
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
              {$endif}

     end
     else
         Text:= Sender.AsString;
     {$endif}
end;

procedure TdmReportes.cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
begin
     
{$ifdef SUSCRIPCION}
     if Not VarIsNull(EmpresaReportes) then
     begin
          with cdsSuscripReportes do
          begin
               IndexFieldNames:='';
               IndexName:='';
               Data := ServerReportes.GetReportes( EmpresaReportes,
                                                      Ord( FClasifActivo ),
                                                      Ord( crFavoritos ),
                                                      Ord( crSuscripciones ),
                                                      GetClasificaciones(FClasificaciones),
                                                      dmDiccionario.VerConfidencial);
               IndexFieldNames:='RE_CODIGO';
          end;
     end;
     {$endif}
end;

procedure TdmReportes.cdsSuscripReportesAlCrearCampos(Sender: TObject);
var
   oCampo: TField;
begin
     inherited;
     
{$ifdef SUSCRIPCION}
     with cdsSuscripReportes do
     begin
          oCampo := FindField( 'RE_ENTIDAD' );
          if ( oCampo <> nil )  then
          begin
               with oCampo do
               begin
                    OnGetText := ObtieneEntidadGetText;
                    Alignment := taLeftJustify;
               end;
          end;
          ListaFija('RE_TIPO', lfTipoReporte);
     end;
     {$endif}
end;

function TdmReportes.GetClasificaciones(oClasificaciones: TStrings): string;
var
   i: integer;
begin
{$ifdef SUSCRIPCION}
     Result := VACIO;
     if not dmCliente.ModoTress then
     begin
          with oClasificaciones do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Result := ConcatString( Result,
                                            IntToStr( Ord( eClasifiReporte( oclasificaciones.objects[ i ] ) ) ), ',' );
               end;
          end
     end;
     {$endif}
end;

procedure TdmReportes.cdsSuscripReportesLookupAlAdquirirDatos(Sender: TObject);
begin

{$ifdef SUSCRIPCION}
     inherited;
     with cdsSuscripReportesLookup do
     begin
          IndexFieldNames:='';
          IndexName:='';
          Data := ServerReportes.GetReportes( EmpresaReportes,
                                            Ord( FClasifActivo ),
                                            Ord( crFavoritos ),
                                            Ord( crSuscripciones ),
                                            GetClasificaciones(FClasificaciones),
                                            dmDiccionario.VerConfidencial );
          IndexFieldNames:='RE_CODIGO';
     end;
     {$endif}
end;

procedure TdmReportes.cdsSuscripReportesLookupAlCrearCampos(Sender: TObject);
var
   oCampo: TField;
begin

{$ifdef SUSCRIPCION}
     inherited;
     with cdsSuscripReportesLookup do
     begin
          oCampo := FindField( 'RE_ENTIDAD' );
          if ( oCampo <> nil )  then
          begin
               with oCampo do
               begin
                    OnGetText := ObtieneEntidadGetText;
                    Alignment := taLeftJustify;
               end;
          end;
          ListaFija('RE_TIPO', lfTipoReporte);
     end;
     {$endif}
end;

procedure TdmReportes.cdsSuscripReportesLookupLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: string; var sKey, sDescription: string);
begin

{$ifdef SUSCRIPCION}
     inherited;
     with Sender do
     begin
          if FormaLookupReporte_DevEx = NIL then
             FormaLookupReporte_DevEx := TFormaLookupReporte_DevEx.Create( self );

          with FormaLookupReporte_devEx do
          begin
               FormaLookupReporte_DevEx.RE_ENTIDAD.Visible := False;
               lOK := ShowModal = mrOk;
               if lOk then
               begin
                    sKey := IntToStr( NumReporte );
                    sDescription := NombreReporte;
               end;
          end;
     end;
     {$endif}
end;

procedure TdmReportes.cdsReportesAlModificar(Sender: TObject);
begin
     // (JB) Escenario de Presupuestos
     with cdsReportes do
     begin
{$ifdef PRESUPUESTOS}
        {if ( eTipoReporte( FieldByName( 'RE_TIPO' ).AsInteger ) = trPoliza ) then
            ZError( 'Reportes', 'No es posible abrir reportes Poliza desde Presupuestos', 0 )
        else
        }       //Se elimina la validacion: Presupuestos puede modificar reportes tipo poliza
            inherited;
{$else}
       inherited
{$endif}
     end;
end;

end.
