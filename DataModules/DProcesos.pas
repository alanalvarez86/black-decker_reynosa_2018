{$HINTS OFF}
unit DProcesos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     {$ifndef VER130}Variants,{$endif}
{$ifdef DOS_CAPAS}
     DServerIMSS,
     DServerAsistencia,
     DServerNomina,
     DServerCalcNomina,
     DServerAnualNomina,
     DServerSistema,
     DServerRecursos,
     DServerLogin,
{$ifdef TIMBRADO}
     DServerNominaTimbrado,
{$endif}
     DServerCafeteria,
{$else}
     IMSS_TLB,
     Asistencia_TLB,
     DAnualNomina_TLB,
     DCalcNomina_TLB,
     Nomina_TLB,
{$ifdef TIMBRADO}
     TimbradoNomina_TLB,
{$endif}
     Recursos_TLB,
     Sistema_TLB,
     Cafeteria_TLB,
     Login_TLB,
{$endif}
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaAsciiFile,
     ZetaClientDataSet,
     ZBaseDlgModal_DevEx,
     ZBaseThreads;

type
  TdmProcesos = class(TDataModule)
    cdsDataSet: TZetaClientDataSet;
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    cdsDeclaraGlobales: TZetaClientDataSet;
    cdsDataSetTimbrados: TZetaClientDataSet;
    cdsEmpleadosCedula: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
{$ifndef DOS_CAPAS}
    FServidorIMSS: IdmServerIMSSDisp;
    FServidorAsistencia: IdmServerAsistenciaDisp;
    {$ifdef TIMBRADO}
    FServidorNomina: IdmServerNominaTimbradoDisp;
    {$else}
     FServidorNomina: IdmServerNominaDisp;
    {$endif}
    FServidorCalcNomina: IdmServerCalcNominaDisp;
    FServidorAnualNomina: IdmServerAnualNominaDisp;
    FServidorRecursos: IdmServerRecursosDisp;
    FServidorSistema: IdmServerSistemaDisp;
    FServidorCafeteria: IdmServerCafeteriaDisp;
    FServidorLogin: IdmServerLoginDisp;
{$endif}
    FErrorCount: Integer;
    FCPArchivo : String;
    FFechaLimiteMovSUA : TDate;
    FRutaArchivoGenerado: String;
    FProcesoThread: Procesos;
    function AgregaMensaje(const sMensaje, sValor: String): String;
    function Check(const Resultado: OleVariant): Boolean;
    function CheckSilencioso(const Resultado: OleVariant; const lNomina: Boolean ): Boolean;
    function CorreThreadAnual(const eProceso: Procesos; const Lista: OleVariant; const lCargarPeriodo: Boolean; Parametros: TZetaParams): Boolean;
    function CorreThreadAsistencia( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
    function CorreThreadCalcNomina(const eProceso: Procesos; const Lista: OleVariant; Parametros : TZetaParams): Boolean;
    function CorreThreadCalcNominaBDE(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadCalcNominaSilent(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadIMSS( const eProceso: Procesos; Parametros: TZetaParams ): Boolean; overload;
    function CorreThreadIMSS( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean; overload;
    function CorreThreadNomina(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadNominaSilent(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
    function CorreThreadRecursos(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadCafeteria( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
    function CorreThreadSistema(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams): Boolean;
    function CorreThreadSistemaSilent(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams; Ventana : HWND;SetResultado:TSetResultadoEspecial): Boolean;
    function ExportarMovEnd(const Parametros, Datos: OleVariant): String;
    function ExportarSUAEnd(const Parametros, Empleados, Movimientos,Infonavit,Incapacidades: OleVariant): String;
    function CalcularPrimaRiesgoEnd(const Parametros, Datos: OleVariant): String;
    function RevisarDatosSTPSEnd(const Parametros, Datos: OleVariant): String;
    function GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
    function ExtraerChecadasBitTerminalesEnd(const Parametros, Datos: OleVariant): String;
    function RecuperaChecadasBitTerminalesEnd( const Parametros, Datos: OleVariant ): String;
    function GenerarCedulaEnd(const Parametros, Datos: OleVariant): String;



{$ifdef TIMBRADO}
   {$ifdef DOS_CAPAS}
       function GetServerNomina: TdmServerNominaTimbrado;
       property ServerNomina: TdmServerNominaTimbrado read GetServerNomina;
   {$else}
       function GetServerNomina: IdmServerNominaTimbradoDisp;
       property ServerNomina: IdmServerNominaTimbradoDisp read GetServerNomina;
   {$endif}

{$else}
   {$ifdef DOS_CAPAS}
       function GetServerNomina: TdmServerNomina;
       property ServerNomina: TdmServerNomina read GetServerNomina;
   {$else}
       function GetServerNomina: IdmServerNominaDisp;
       property ServerNomina: IdmServerNominaDisp read GetServerNomina;
   {$endif}
{$endif}

{$ifdef DOS_CAPAS}
    function GetServerIMSS: TdmServerIMSS;
    function GetServerAsistencia: TdmServerAsistencia;
   //TIMBRADO  function GetServerNomina: TdmServerNomina;
    function GetServerCalcNomina: TdmServerCalcNomina;
    function GetServerAnualNomina: TdmServerAnualNomina;
    function GetServerRecursos: TdmServerRecursos;
    function GetServerSistema: TdmServerSistema;
    function GetServerCafeteria: TdmServerCafeteria;
    function GetServerLogin: IdmServerLogin;
{$else}
    function GetServerIMSS: IdmServerIMSSDisp;
    function GetServerAsistencia: IdmServerAsistenciaDisp;
  //TIMBRADO  function GetServerNomina: IdmServerNominaDisp;
    function GetServerCalcNomina: IdmServerCalcNominaDisp;
    function GetServerAnualNomina: IdmServerAnualNominaDisp;
    function GetServerRecursos: IdmServerRecursosDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    function GetServerCafeteria: IdmServerCafeteriaDisp;
    function GetServerLogin: IdmServerLoginDisp;
{$endif}
    function ProcesarResultado( Info: TProcessInfo ): String;
    function RastrearEnd(const Parametros, Rastreo: OleVariant): String;
    function NumeroTarjetaEnd( const Bitacora: OleVariant ): String;
    //procedure ClearErroresASCII( oDataSet: TDataSet );
    procedure ExportarMovEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    procedure ExportarSUAEmpleados( Sender: TAsciiExport; DataSet: TDataset );
    procedure ExportarSUAMovimientos( Sender: TAsciiExport; DataSet: TDataset );
    //procedure ImportarAltasAfterOpen(DataSet: TDataSet);
    procedure ValidacionesAfterOpen(DataSet: TDataSet);
    procedure CB_CODIGOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ExportarSUADatosAfiliatorios(Sender: TAsciiExport;DataSet: TDataset);
    procedure ExportarSUAIncapacidades(Sender: TAsciiExport;DataSet: TDataset);
    procedure ExportarSUAInfonavit(Sender: TAsciiExport;DataSet: TDataset);
    function CrearAsciiDatosAfiliatorios(cdsDataSet: TZetaClientDataSet;const sArchivoDatosAfiliatorios: string): String;
    function CrearAsciiIncapacidades(cdsDataSet: TZetaClientDataSet;const sArchivoIncapacidades: string): String;
    function CrearAsciiMovimientos(cdsDataSet: TZetaClientDataSet;const sArchivoMovimientos: string): String;
    function CrearAsciiEmpleados (cdsDataSet: TZetaClientDataSet;const sArchivoEmpleados: string):String;
    function CrearAsciiInfonavit (cdsDataSet: TZetaClientDataSet;const sArchivoInfonavit: string):String;
    procedure LlenaParametrosCalculo( oParametros: TZetaParams; const lNomina: Boolean );
    procedure ExtraerChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    procedure RecuperaChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    function ImportaTerminalesEnd( const Bitacora: OleVariant ): String;
    procedure GenerarCedulaEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
    procedure GenerarCedulaTrabajadorEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );

    //procedure FillcdsASCII( const sArchivo: String );
  protected
    { Protected declarations }
  public
    { Public declarations }
    FListaProcesos: TProcessList;
    property RutaArchivoGenerado: String read FRutaArchivoGenerado write FRutaArchivoGenerado;
    property ProcesoThread : Procesos read FProcesoThread write FProcesoThread;
    property ErrorCount: Integer read FErrorCount;
{$ifdef DOS_CAPAS}
    property ServerIMSS: TdmServerIMSS read GetServerIMSS;
    property ServerAsistencia: TdmServerAsistencia  read GetServerAsistencia;

    property ServerCalcNomina: TdmServerCalcNomina  read GetServerCalcNomina;
    property ServerAnualNomina: TdmServerAnualNomina  read GetServerAnualNomina;
    property ServerRecursos: TdmServerRecursos  read GetServerRecursos;
    property ServerSistema: TdmServerSistema  read GetServerSistema;
    property ServerCafeteria: TdmServerCafeteria read GetServerCafeteria;
    property ServerLogin: TdmServerLogin read GetServerLogin;
{$else}
    property ServerIMSS: IdmServerIMSSDisp read GetServerIMSS;
    property ServerAsistencia: IdmServerAsistenciaDisp read GetServerAsistencia;
 
    property ServerCalcNomina: IdmServerCalcNominaDisp read GetServerCalcNomina;
    property ServerAnualNomina: IdmServerAnualNominaDisp read GetServerAnualNomina;
    property ServerRecursos: IdmServerRecursosDisp read GetServerRecursos;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    property ServerCafeteria: IdmServerCafeteriaDisp read GetServerCafeteria;
    property ServerLogin: IdmServerLoginDisp read GetServerLogin;
{$endif}
    function AplicarTabulador( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    function BorrarBajas(Parametros: TZetaParams): Boolean;
    function BorrarTarjetas(Parametros: TZetaParams): Boolean;
    function BorrarNominas(Parametros: TZetaParams): Boolean;
    function BorrarTimbradoNominas(Parametros: TZetaParams): Boolean;
    function BorrarPoll(Parametros: TZetaParams): Boolean;
    function BorrarBitacora(Parametros: TZetaParams): Boolean;
    function BorrarHerramientas(Parametros: TZetaParams): Boolean;
    function CalcularPagosIMSS(Parametros: TZetaParams): Boolean;
    function CalcularRecargosIMSS(Parametros: TZetaParams): Boolean;
    function CalcularPrenomina( Parametros: TZetaParams ): Boolean;
    function CalcularTasaINFONAVIT(Parametros: TZetaParams): Boolean;
    function CalcularPrimaRiesgo(Parametros: TZetaParams): Boolean;
    function CerrarAhorros(Parametros: TZetaParams): Boolean;
    function CerrarPrestamos(Parametros: TZetaParams): Boolean;
    function CorregirFechas( Parametros: TZetaParams ): Boolean;
    function ProcesarTarjetasPendientes( Parametros: TZetaParams ): Boolean;
    function PromediarVariables(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function RecalcularTarjetas( Parametros: TZetaParams ): Boolean;
    function RegistrosAutomaticos(Parametros: TZetaParams): Boolean;
    function RevisarSUA(Parametros: TZetaParams): Boolean;
    function AfectarNomina(Parametros: TZetaParams): Boolean;
    function CalcularDiferencias(Parametros: TZetaParams): Boolean;
    function CalcularNomina(Parametros: TZetaParams): Boolean;
    function DesafectarNomina(Parametros: TZetaParams): Boolean;
    function FoliarRecibos(Parametros: TZetaParams): Boolean;
    function LimpiarAcum(Parametros: TZetaParams): Boolean;
    function Rastrear(Parametros: TZetaParams): Boolean;
    function RecalculoAcumulado(Parametros: TZetaParams): Boolean;
    {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
    function RecalculoAhorros(Parametros: TZetaParams): Boolean;
    {*** FIN ***}
    {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
    function RecalculoPrestamos(Parametros: TZetaParams): Boolean;
    {*** FIN ***}
    function ReFoliarRecibos(Parametros: TZetaParams): Boolean;
    function SalNetoBruto(Parametros: TZetaParams): Boolean;
    function DefinirPeriodos(Parametros: TZetaParams): Boolean;
    function PagarAguinaldo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function CalcularRepartoAhorro(Parametros: TZetaParams): Boolean;
    function PTUCalcular(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function PTUPago(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function CopiarNomina(Parametros: TZetaParams): Boolean;
    function DeclaraCredito( Parametros: TZetaParams ): Boolean;
    function ISPTAnual( Parametros: TZetaParams ): Boolean;
    function PagarRepartoAhorro( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    function ISPTAnualPago( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    function ExportarMov(Parametros: TZetaParams): Boolean;
    function ExportarSUA( Parametros: TZetaParams ): Boolean;
    function ValidarMovimientosIDSE( Parametros: TZetaParams ): Boolean;
    function NumeroTarjeta: Boolean;
    procedure AplicarTabuladorGetList( Parametros: TZetaParams );
    procedure PromediarVariablesGetLista(Parametros: TZetaParams);
    procedure PagarAguinaldoGetLista(Parametros: TZetaParams);
    procedure PagarRepartoAhorroGetLista(Parametros: TZetaParams);
    procedure PTUPagoGetLista(Parametros: TZetaParams);
    procedure ISPTAnualPagoGetLista(Parametros: TZetaParams);
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure HandleProcessFileEnd(const iIndice, iValor: Integer);
    procedure HandleProcessSilent(const iIndice, iValor: Integer);
    //WizAsistPoll
    function Poll(Parametros: TZetaParams): Boolean;
     // WizNomCreditoAplicado
    function CreditoAplicado( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure CreditoAplicadoGetLista(Parametros: TZetaParams); 
    // WizSalarioIntegrado
    function SalarioIntegrado( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure SalarioIntegradoGetLista( Parametros: TZetaParams );
    // WizCambioSalario
    function CambioSalario(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CambioSalarioGetLista(Parametros: TZetaParams);
    // WizVacaciones
    function Vacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure VacacionesGetLista(Parametros: TZetaParams);
    // WizEventos
    function Eventos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure EventosGetLista(Parametros: TZetaParams);
    // WizImportarKardex
    function ImportarKardex(Parametros: TZetaParams): Boolean;
    procedure ImportarKardexGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarKardexGetLista(Parametros: TZetaParams);
    // FWizEmpCambioTurno
    function CambioMasivoTurnos(Parametros: TZetaParams; const DataSet: TZetaClientDataSet; const lVerificacion: Boolean): Boolean;

    // WizImportarAltas
    function ImportarAltas(Parametros: TZetaParams): Boolean;
    procedure ImportarAltasGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //procedure ImportarAltasGetLista(Parametros: TZetaParams);
    procedure ImportarAltasGetASCIICatalogos(Parametros: TZetaParams);
    //procedure ENTIDADOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    // WizCancelarKardex
    function CancelarKardex(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarKardexGetLista(Parametros: TZetaParams);
    // WizCancelarVacaciones
    function CancelarVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarVacacionesGetLista(Parametros: TZetaParams);
    // WizCierreGlobalVacaciones
    function CierreGlobalVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CierreGlobalVacacionesGetLista(Parametros: TZetaParams);
    // WizTransferencia
    function Transferencia(Parametros: TZetaParams; const EmpresaDestino: Variant): Boolean;
    // WizAutorizarHoras
    function AutorizarHoras( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AutorizarHorasGetLista( Parametros: TZetaParams );

    //WizAsistIntercambioFestivo
    function IntercambioFestivo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure IntercambioFestivoGetLista( Parametros: TZetaParams );

    //WizAsistCancelarExcepcionesFestivos
    function CancelarExcepcionesFestivos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarExcepcionesFestivosGetLista( Parametros: TZetaParams );

    // WizNomImportarMovimientos
    function ImportarMov(Parametros: TZetaParams): Boolean;
    procedure ImportarMovGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );

    {*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
    //WizNomImportarAcumuladosRazonSocial
    function ImportarAcumuladosRS(Parametros: TZetaParams): Boolean;
    {*** FIN ***}
    // WizNomImportarPagoRecibos
    function ImportarPagoRecibos(Parametros: TZetaParams): Boolean;
    procedure ImportarPagoRecibosGetListaASCII(Parametros: TZetaParams);
    procedure ImportarPagoRecibosGetLista(Parametros: TZetaParams);
    //CV
    procedure RecalculoDias( const iTipo: Integer );
    // WizNOMPagarPorFuera
    function PagarPorFuera(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure PagarPorFueraGetLista(Parametros: TZetaParams);
    // WizNOM Poliza
    function Poliza(Parametros: TZetaParams; aFiltros : OleVariant ): Boolean;
    // WizEmpLiquidacionGlobal
    function LiquidacionGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure LiquidacionGlobalLista( Parametros: TZetaParams );
    // WizNOMCalcularAguinaldo
    function CalcularAguinaldo(Parametros: TZetaParams): Boolean;
    procedure CalcularNominaEmpleado( const lNomina : Boolean = TRUE );
    procedure CalcularSimulacionEmpleado( const TipoPeriodo: eTipoPeriodo; const iPeriodo: Integer );
    function CalcularSimulacionGlobal(const iPeriodo: Integer): Boolean;
    function LiquidacionGlobalSimulacion( Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    // WizEmpEntHerr
    function EntregarHerramienta(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure EntregarHerramientaGetLista(Parametros: TZetaParams);
    // WizEmpRegresaHerr
    function RegresarHerramienta(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure RegresarHerramientaGetLista(Parametros: TZetaParams);
    // WizEmpCursoTomado
    function CursoTomado(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CursoTomadoGetLista(Parametros: TZetaParams);
    // WizEmpRenumera
    function RenumeraEmpleados( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure RenumeraEmpleadosGetLista(Parametros: TZetaParams);
    procedure RenumeraVerificaListaASCII( Parametros: TZetaParams );
    // WizEmpPermisoGlobal
    function PermisoGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure PermisoGlobalGetLista(Parametros: TZetaParams);
    //WizCalculoRetroactivos
    function CalculaRetroactivos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CalculaRetroactivoGetLista(Parametros: TZetaParams);
    // WizEmpBorrarCursoTomado
    function BorrarCursoTomado(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure BorrarCursoTomadoGetLista(Parametros: TZetaParams);

    //WizNomDeclaracionAnual
    function DeclaracionAnual(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure DeclaracionAnualGetLista(Parametros: TZetaParams);
    procedure GetDeclaracionAnualGlobales;
    procedure GrabaDeclaracionAnualGlobales;
    //WizNomDeclaracionAnualCierre
    function DeclaracionAnualCierre(Parametros: TZetaParams): Boolean;

    //WizComidasGrupales
    function ComidasGrupales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    function ComidasGrupalesListaASCII( Parametros: TZetaParams ): Boolean;
    procedure ComidasGrupalesGetLista( Parametros: TZetaParams );
    //WizEmpCorregirFechasCafe
    function CorregirFechasGlobalesCafe( Parametros: TZetaParams ): Boolean;
    // WizImportarAsistenciaSesiones
    function ImportarAsistenciaSesiones(Parametros: TZetaParams): Boolean;
    procedure ImportarAsistenciaSesionesGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarAsistenciaSesionesGetLista(Parametros: TZetaParams);
    //WizEmpCancelarCierreVacaciones
    function CancelarCierreVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarCierreVacacionesGetLista( Parametros: TZetaParams );
    //WizEmpCancelarPermisosGlobales
    function CancelarPermisosGlobales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarPermisosGlobalesGetLista( Parametros: TZetaParams );
    procedure PermisosAfterOpen(DataSet: TDataSet);
    procedure PM_CLASIFIOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    //WizEmpImportarAhorrosPrestamos
    function ImportarAhorrosPrestamos(Parametros: TZetaParams): Boolean;
    procedure ImportarAhorrosPrestamosGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //WizEmpRecalculaSaldosVaca
    function RecalculaSaldosVaca(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure RecalculaSaldosVacaGetLista(Parametros: TZetaParams);
    //WizNomAjusteRetFonacot
    function AjusteRetFonacot( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AjusteRetFonacotGetLista(Parametros: TZetaParams);
    //WizNomAjusteRetFonacotCancelacion
    function AjusteRetFonacotCancelacion( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AjusteRetFonacotCancelacionGetLista(Parametros: TZetaParams);
    //WizNomCalcularPagoFonacot
    function CalcularPagoFonacot( Parametros: TZetaParams): Boolean;
    //WizNomFonacotImportarCedula
    function ImportarCedulasFonacot( Parametros: TZetaParams): Boolean;
    procedure ImportarCedulasFonacotGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //WizGeneraCedPago
    function GenerarCedulaFonacot( Parametros: TZetaParams): Boolean;
    //WizSisEnrolarUsuarios
    function EnrolarUsuarios( Parametros: TZetaParams): Boolean;
    procedure EnrolarUsuariosGetLista( Parametros: TZetaParams);
    function ImportarEnrolamientoMasivo ( Parametros: TZetaParams): Boolean;
    procedure ImportarEnrolamientoMasivoGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //WizIMSSAjusteRetInfonavit
    function AjusteRetInfonavit( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AjusteRetInfonavitGetLista(Parametros: TZetaParams);
    // WizEmpProgCursoGlobal
    function CursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CursoProgGlobalGetLista(Parametros: TZetaParams);
    //WizEmpCancelarProgCursoGlobal
    function CancelarCursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure CancelarCursoProgGlobalGetLista(Parametros: TZetaParams);
    //WizFiniquitos
    function AplicarFiniquitosGlobales( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AplicarFiniquitosGlobalGetLista(Parametros: TZetaParams);
    //WizAsistAjustIncapaCal
    function AjusteIncapaCal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure AjusteIncapaCalGetLista(Parametros: TZetaParams);
    //Wizard Foliar Capacitaciones
    function FoliarCapacitaciones( Parametros: TZetaParams ): Boolean;
    procedure FoliarCapacitacionesSTPSGetLista( Parametros: TZetaParams );
    function FoliarCapacitacionesSTPS(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    //Wizard Autorizacion PreNomina
    function AutorizacionPreNomina( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure AutorizacionPreNominaGetLista(Parametros: TZetaParams);

    //WizAsistCancelarExcepcionesFestivos
    function RegistrarExcepcionesFestivos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure RegistrarExcepcionesFestivosGetLista( Parametros: TZetaParams );

    //WizNOTransferenciasCosteo
    function TransferenciasCosteo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
    procedure TransferenciasCosteoGetLista(Parametros: TZetaParams);
    
    //WizNoCalculaCosteo
    function CalculaCosteo(Parametros: TZetaParams): Boolean;

    //WizImportarClasificacionesTemporales
    procedure ImportarClasificacionesTemporalesGetListaASCII( oParametros: TZetaParams; const lFillASCII: Boolean = TRUE);
    function ImportarClasificacionesTemporales(Parametros: TZetaParams): Boolean;
    procedure ImportarClasificacionesTemporalesGetLista(Parametros: TZetaParams);
    //WizNomCancelarTransferenciasCosteo
    function CancelaTransferenciaCosteo(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
    procedure CancelaTransferenciasCosteoGetLista(Parametros: TZetaParams);

    //FWizNomPrevioISR
    function PrevioISR(Parametros: TZetaParams): Boolean;

    {$ifndef DOS_CAPAS}
    //WizDepuraBitBiometrico
    function DepuraBitacoraBio( Parametros:TZetaParams ): Boolean;
    //WizAsignaNumBiometricos
    function AsignaNumBiometricos( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    //WizAsignaGrupoTerminales
    function AsignaGrupoTerminales( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
    procedure EmpleadosConBiometricosGetLista( Parametros: TZetaParams );
    procedure EmpleadosSinBiometricosGetLista( Parametros: TZetaParams );
    function ImportaTerminales( Parametros: TZetaParams ): Boolean;
    {$endif}
    //WizAgregarBaseDatosSeleccion
    function AgregarBaseDatosSeleccion(Parametros:TZetaParams): Boolean;
    //WizAgregarBaseDatosVisitantes
    function AgregarBaseDatosVisitantes(Parametros:TZetaParams): Boolean;
    //WizAgregarBaseDatosPruebas
    function AgregarBaseDatosPruebas(Parametros:TZetaParams): Boolean;
    //WizAgregarBaseDatosEmpleados
    function AgregarBaseDatosEmpleados(Parametros:TZetaParams): Boolean;

    //WizAgregarBaseDatosPresupuestos
    function AgregarBaseDatosPresupuestos( Parametros: TZetaParams ): Boolean; 
    //WizSistImportarTablas
    function ImportarTablas( Parametros: TZetaParams ): Boolean;
    //WizSistImportarTablasCSV
    function ImportarTablaCSV( Parametros: TZetaParams ): Boolean;
    procedure ImportarTablaCSVGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    //WizSistRecuperarChecadas
    function RecuperaChecadas(Parametros:TZetaParams): Boolean; //@DC
    // WizSistExtraerChecadasReloj.
    function ExtraerChecadasBitTerminales( Parametros: TZetaParams ): Boolean;

    function ImportarCafeteria( Parametros: TZetaParams ): Boolean;
 // WizRHImportarSGM
    function ImportarSGM(Parametros: TZetaParams): Boolean;
    procedure ImportarSGMGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
    procedure ImportarSGMGetLista(Parametros: TZetaParams);

    procedure RenovacionSGMGetLista(Parametros: TZetaParams);
    function RenovacionSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;

    procedure BorrarSGMGetLista(Parametros: TZetaParams);
    function BorrarSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
  //WizImportaOrganigrama
   procedure ImportarOrganigramaGetListaASCII( oParametros: TZetaParams; const lFillASCII: Boolean = TRUE);
    function ImportarOrganigrama(Parametros: TZetaParams): Boolean;
    procedure ImportarOrganigramaGetLista(Parametros: TZetaParams);

    // WizEmpValidarDC4
    function RevisarDatosSTPS (Parametros: TZetaParams): Boolean;
 
   // Wizard Actualizar Bases de Datos
    function EjecutaMotorPatch(Parametros: TZetaParams;Ventana:HWND;SetResultado:TSetResultadoEspecial): Boolean;


  //WizEmpImportarImagenes
    function ImportarImagenes(Parametros: TZetaParams): Boolean;

    {$IFDEF TIMBRADO}
    //WizTimbrarNomina
    function TimbrarNomina( Parametros: TZetaParams ): Boolean;
    procedure TimbrarNominaGetLista(Parametros: TZetaParams);
    {$ENDIF}

    //WizAgregarBaseDatosEmpleados

	//WizImportarCedula
    procedure VerificarEmpleadosCedulaFonacot;
    procedure AplicarFiltroRFC(lEnabled : Boolean);
    function GetConteoCedula(Anio, Mes, RazonSocial : String) : Integer;

    //WizEliminarCedula
    function EliminarCedulaFonacot( Parametros: TZetaParams): Boolean;
    function GetConteoCedulaAfectada(Anio, Mes, RazonSocial : String) : Integer;
    function GetNominaAfectada(Anio, Mes, RazonSocial : String) : Integer;

    function GetStatusTimbrado(Parametros: TZetaParams ): Olevariant;
    procedure ImportarFonacot(Sender: TObject);
  end;

const
     K_VERSION_SUA_2000 = 0;
     K_VERSION_SUA_2006 = 1;
     aVersionSUA: array[0..1] of String = ( 'SUA 2000 Windows', 'SUA 2006 Windows' );

var
  dmProcesos: TdmProcesos;

procedure ShowWizard( const eProceso: Procesos );
procedure SetDataChange( const Proceso: Procesos );

implementation

uses
     ZcxWizardBasico,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaClientTools,
     ZAsciiTools,
     ZetaHayProblemas,
     ZetaDialogo,
     ZetaRegistryCliente,
     ZetaMessages,
     ZetaWizardFeedBack_DevEx,
     ZetaWizardFeedBackAbrir_DevEx,
     ZetaSystemWorking,
     FTressShell,
     DThreads,
     DGlobal,
     DCliente,
     DRecursos,
     DCatalogos,
     DConsultas,
{$IFDEF TRESS_DELPHIXE5_UP}
     DBorradorSUA,
{$ENDIF}
     FWizEmpAplicarTabulador_DevEx,
     FWizAsistPollRelojes_DevEx,
     FWizAsistCalculoPreNomina_DevEx,
     FWizAsistCorregirFechas_DevEx,
     FWizAsistRecalcular_DevEx,
     FWizAsistRegistrosAut_DevEx,
     FWizAsistProcesarTarjetas_DevEx,
     FWizIMSSCalcular_DevEx,
     FWizIMSSRevisarSua_DevEx,
 //**    FWizIMSSTasaINFONAVIT,
     FWizIMSSRecargos_DevEx,
     FWizIMSSExportarSua_DevEx,
     FWizIMSSPrimaRiesgo_DevEx,
     FWizSistBorrarBajas_DevEx,
     FWizSistBorrarTarjetas_DevEx,
     FWizSistBorrarPOLL_DevEx,
     FWizSistBorrarNominas_DevEx,
     FWizSistDestimbrarNominas_DevEx,
     FWizSistCerrarAhorros_DevEx,
     FWizSistCerrarPrestamos_DevEx,
     FWizNomCalcular_DevEx,
     FWizNomAfectar_DevEx,
     FWizNomDesafectar_DevEx,
     FWizNomFoliarRecibos_DevEx,
     FWizNomReFoliarRecibos_DevEx,
     FWizNomPagarPorFuera_DevEx,
     FWizNomLimpiarAcum_DevEx,
     FWizNomRastrear_DevEx,
     FWizNomPoliza_DevEx,
     FWizNomCreditoAplicado_DevEx,
     FWizNomPagoRecibos_DevEx,
     FWizNomRecalculoAcumulado_DevEx,
     FWizNomSalNetoBruto_DevEx,
     FWizNomLiquidacionGlobal_DevEx,
     FWizNomDefinirPeriodos_DevEx,
     FWizNomCalcularDiferencias_DevEx,
     FWizNomCalcularAguinaldo_DevEx,
     FWizNomPagarAguinaldo_DevEx,
     FWizNomCalcularRepartoAhorro_DevEx,
     FWizNomPTUCalcular_DevEx,
     FWizNomPTUPago_DevEx,
     FWizNomCopiar_DevEx,
     FWizNomCancelar_DevEx,
     FWizNomDeclaracionCreditoSalario_DevEx,
     FWizNomISPTAnual_DevEx,
     FWizNomPagarRepartoAhorro_DevEx,
     FWizNomISPTAnualPago_DevEx,
     FWizNomImportarMov_DevEx,
     FWizNomExportarMov_DevEx,
     FWizNomRetroactivos_DevEx,
     FWizNomDeclaracionAnual_DevEx,
     FWizNomDeclaracionAnualCierre_DevEx,
     FWizNomTransferenciasCosteo_DevEx,
     FWizEmpSalarioIntegrado_DevEx,
     FWizEmpPromVar_DevEx,
     FWizEmpCambioSalario_DevEx,
     FWizEmpVacaciones_DevEx,
     FWizEmpEventos_DevEx,
     FWizEmpImportarKardex_DevEx,
     FWizEmpCancelarKardex_DevEx,
     FWizEmpCursoTomado_DevEx,
     FWizEmpCancelarVacaciones_DevEx,
     FWizEmpTransferencia_DevEx,
     FWizAsistAutorizarHoras_DevEx,
     FWizSistBorrarBitacora_DevEx,
     FWizEmpCerrarVacaciones_DevEx,
     FWizEmpEntHerr_DevEx,
     FWizEmpRegresaHerr_DevEx,
     FWizSistBorrarHerramientas_DevEx,
     FWizEmpImportarAltas_DevEx,
     FWizEmpRenumera_DevEx,
     FWizEmpPermisoGlobal_DevEx,
     FWizEmpBorrarCursoTomado_DevEx,
     FWizEmpComidasGrupales_DevEx,
     FWizEmpCorregirFechasCafe_DevEx,
     FWizSistNumeroTarjeta_DevEx,
     FWizEmpImportarAsistenciaSesiones_DevEx,
     FWizEmpCancelarCierreVacaciones_DevEx,
     FWizAsistIntercambioFestivo_DevEx,
     FWizAsistCancelarExcepcionesFestivo_DevEx,
     FWizEmpCancelarPermisosGlobales_DevEx,
     FWizEmpImportarAhorrosPrestamos_DevEx,
     FWizEmpRecalculaSaldosVaca_DevEx,
     FWizNomAjusteRetFonacot_DevEx,
     FWizNomAjusteRetFonacotCancelacion_DevEx,
     FWizNomCalcularPagoFonacot_DevEx,
     FWizSistImportarEnrolamientoMasivo_DevEx,
     FWizSistEnrolamientoMasivo_DevEx,
     FWizIMSSAjusteRetInfonavit_DevEx,
     FWizEmpProgCursoGlobal_DevEx,
     FWizEmpCancelarProgCursoGlobal_DevEx,
 //**    FWizEmpFoliarCapacitaciones,
     FWizEmpFoliarCapacitacionesSTPS_DevEx,
     FWizAsistAjustIncapaCal_DevEx,
     FWizAsistAutorizacionPrenomina_DevEx,
     FWizAsistRegistrarExcepcionesFestivo_DevEx,
     FWizIMSSValidacionIDSE_DevEx,   
     FWizNomPrevioISR_DevEx,
	 dIMSS,
     FWizImportacionClasificaciones_DevEx,
     FWizNomCalculaCosteo_DevEx,
     FWizNomCancelarTransferenciasCosteo_DevEx,
     {$ifndef DOS_CAPAS}
     FWizDepuraBitBiometrico_DevEx, // SYNERGY
     FWizAsignaNumBiometricos_DevEx, // SYNERGY
     FWizAsignaGrupoTerminales_DevEx, // SYNERGY
     FWizSistImportaTerminales_DevEx,
     {$endif}
     FWizEmpImportarSGM_DevEx,
     FWizEmpRenovacionSGM_DevEx,
     FWizEmpBorrarSGM_DevEx,
     FWizEmpImportarOrganigrama_DevEx,
     FWizEmpValidarDC4_DevEx,
     FWizEmpReiniciarCapacitacionesSTPS_DevEx,
     FWizAgregarBaseDatosEmpleados_DevEx,
     FWizAgregarBaseDatosPruebas_DevEx,
     FWizAgregarBaseDatosSeleccion_DevEx,
     FWizAgregarBaseDatosVisitantes_DevEx,
     FWizAgregarBaseDatosPresupuestos_DevEx,
     FWizSistImportarTablas_DevEx,
     FWizSistImportarTablasCSV_DevEx,
     FWizEmpImportarImagenes_DevEx,
     FWizSistExtraerChecadasReloj,
     FWizSistRecuperaChecadas_DevEx,
     FWizEmpCambioTurno
     {$ifdef TIMBRADO}
     ,DInterfase
     {$endif}
     {*** US 13889: Crear proceso Importar acumulados ***}
     ,FWizNomImportarMovAcumuladosRS_DevEx
     {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
     ,FWizNomRecalculoAhorros_DevEx
     {$ifdef TRESS_DELPHIXE5_UP}
     ,FWizSistCopiarConfiguracionCafeteria_DevEx
     {$endif}
     {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
     ,FWizNomRecalculoPrestamos_DevEx

{$ifdef TRESS_DELPHIXE5_UP}
     ,FWizSistCalendarioReportes_DevEx,
     FSistCalendarioReportes_DevEx
     {$endif}
     {$ifndef VALIDADORDIMM }
     {$ifndef IMPORTAVACACIONES }
     {$ifndef MULTIPLE_RP }
     {$ifndef RECLASIFICACION_PLAZAS }
     {$ifndef INTERFAZ_SY }
     {$ifndef INTERFAZ_ZK }
     ,FWizNomFonacotImportarCedula_DevEx

     ,FWizCalculoConfPago
     ,FWizNomFonacotEliminarCedula_DevEx,
     FDetalleImportacionCedula_DevEx{$endif}{$endif}{$endif}{$endif}{$endif}{$endif}
     ;

const
     K_FORMATO = '#,###,###,###,##0';

{$R *.DFM}

{ ********** Funciones Globales ************* }

procedure SetDataChange( const Proceso: Procesos );
begin

     with TressShell do
     begin
          case Proceso of
               prRHSalarioIntegrado: SetDataChange( [ enEmpleado, enSal_Min ] );
               prRHCambioSalario: SetDataChange( [ enEmpleado, enSal_Min ] );
               prRHPromediarVariables: SetDataChange( [ enEmpleado, enSal_Min ] );
               prRHVacacionesGlobales: SetDataChange( [ enEmpleado, enVacacion ] );
               prRHAplicarTabulador: SetDataChange( [ enEmpleado, enSal_Min ] );
               prRHAplicarEventos: SetDataChange( [ enEmpleado, enKardex, enAhorro, enUsuarios] );
               prRHImportarKardex: SetDataChange( [ enEmpleado, enKardex, enAhorro, enUsuarios] );
               prRHCancelarKardex: SetDataChange( [ enEmpleado, enKardex, enUsuarios ] );
               prRHCursoTomado : SetDataChange( [ enKarCurso, enCurProg, enSesion ] );
               prRHRenumeraEmpleados:
               begin
                    with dmCliente do
                    begin
                         if GetEmpleadoPrimero then
                                CambiaEmpleadoActivos;
                    end;
               end;
               prRHBorrarCursoTomado : SetDataChange( [ enKarCurso, enCurProg ] );
               prRHImportarAsistenciaSesiones : SetDataChange( [ enKarCurso, enSesion ] );

               prASISPollRelojes: SetDataChange( [ enAusencia, enChecadas, enPoll ] );
               prASISProcesarTarjetas: SetDataChange( [ enAusencia, enChecadas, enPoll ] );
               prASISCalculoPreNomina:
               begin
                    SetDataChange( [ enPeriodo, enNomina, enAusencia ] );
                    if Global.GetGlobalBooleano( K_GLOBAL_PLAN_VACA_PRENOMINA ) then
                       SetDataChange( [ enEmpleado, enVacacion, enPlanVacacion ] );
               end;
               prASISExtrasPer : SetDataChange( [ enAusencia, enChecadas ] );
               prASISRegistrosAut: SetDataChange( [ enAusencia, enChecadas ] );
               prASISCorregirFechas: SetDataChange( [ enAusencia, enChecadas ] );
               prASISRecalculoTarjetas : SetDataChange( [ enAusencia, enChecadas ] );
               prASISIntercambioFestivo : SetDataChange( [ enAusencia ] );
               prASISCancelarExcepcionesFestivos : SetDataChange( [ enAusencia ] );
               prASISRegistrarExcepcionesFestivos : SetDataChange( [ enAusencia ] );
               prNOCalcular:
               begin
                    SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
                    if Global.GetGlobalBooleano( K_GLOBAL_PLAN_VACA_PRENOMINA ) then
                       SetDataChange( [ enEmpleado, enVacacion, enPlanVacacion ] );
               end;
               prNOAfectar: SetDataChange( [ enPeriodo, enNomina, enAcumula, enAhorro, enPrestamo ] );
               prNOImprimirListado:  SetDataChange( [] );
               prNOImprimirRecibos:  SetDataChange( [] );
               prNOPolizaContable: SetDataChange( [ enPolHead ] );
               prNODesafectar: SetDataChange( [ enPeriodo, enNomina, enAcumula, enAhorro, enPrestamo ] );
               prNOLimpiarAcum: SetDataChange( [ enAcumula ] );
               prNOImportarMov: SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas, enAcumula ] );
               {*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
               prNOImportarAcumuladosRS: SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas, enAcumula ] );
               prNOExportarMov: SetDataChange( [] );
               prNOPagosPorFuera: SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
               prNOCancelarPasadas: SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas] );
               prNOLiquidacion: SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas ] );
               prNOCalculoNetoBruto: SetDataChange( [] );
               prNORastrearCalculo: SetDataChange( [] );
               prNOFoliarRecibos: SetDataChange( [ enNomina, enFolio ] );
               prNOCreditoAplicado: SetDataChange( [ enAcumula ] );
               prNODefinirPeriodos:
               begin
                    ReinitPeriodo;
                    SetDataChange( [ enPeriodo ] );
               end;
               prNOCalculoAguinaldo: SetDataChange( [ enAguinaldo ] );
               prNORepartoAhorro: SetDataChange( [ enRepAhorro ] );
               prNOCalculoPTU: SetDataChange( [ enRepartoPTU ] );
               prNOCreditoSalario: SetDataChange( [ enDeclara ] );
               prNOISPTAnual: SetDataChange( [ enCompara ] );
               prNORecalcAcum: SetDataChange( [ enAcumula, enAhorro, enPrestamo ] );
               {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
               prNORecalcAhorros : SetDataChange( [ enAcumula, enAhorro, enPrestamo ] );
               {*** FIN ***}
               {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
               prNORecalcPrestamos : SetDataChange( [ enAcumula, enAhorro, enPrestamo ] );
               {*** FIN ***}
               prNOCalculaRetroactivos: SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas ] );
               prNODeclaracionAnual: SetDataChange( [ enDeclaraAnual ] );
               prNODeclaracionAnualCierre: SetDataChange( [ enDeclaraAnualCierre ] );

               prIMSSCalculoPagos: SetDataChange( [ enLiq_IMSS, enLiq_Emp, enLiq_Mov ] );
               prIMSSCalculoRecargos: SetDataChange( [ enLiq_IMSS, enLiq_Emp, enLiq_Mov ] );
               prIMSSRevisarSUA: SetDataChange( [] );
               prIMSSExportarSUA: SetDataChange( [] );
               prIMSSCalculoPrima:  SetDataChange( [ enEmpleado, enKardex, enRPatron ] );
               prIMSSTasaINFONAVIT: SetDataChange( [ enEmpleado ] );

               prASISAjusteColectivo:  SetDataChange( [] );
               prSISTBorrarBajas: SetDataChange( [ enEmpleado ] );
               prSISTBorrarTarjetas: SetDataChange( [ enAusencia, enChecadas ] );
               prSISTDepurar:  SetDataChange( [] );
               prRHTranferencia: SetDataChange( [ enEmpleado ] );
               prNOPagoAguinaldo: SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
               prNOPagoRepartoAhorro: SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
               prNOPagoPTU: SetDataChange( [ enPeriodo, enNomina, enMovimien ] );

               prSISTCierreAhorros: SetDataChange( [ enAhorro ] );
               prSISTCierrePrestamos: SetDataChange( [ enPrestamo ] );
               prSISTBorrarNominas: SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
               prSISTBorrarTimbradoNominas : SetDataChange( [ enPeriodo, enNomina, enMovimien ] );

               prNOISPTAnualPago: SetDataChange( [ enPeriodo, enNomina, enMovimien, enPrestamo ] );
               prNOPagoRecibos: SetDataChange( [ enNomina ] );
               prNOReFoliarRecibos: SetDataChange( [ enNomina, enFolio ] );
               prNOCopiarNominas: SetDataChange( [ enPeriodo, enNomina, enMovimien, enFaltas ] );
               prNOCalcularDiferencias: SetDataChange( [ enPeriodo, enNomina, enMovimien ] );
               prRHCancVacGlobales: SetDataChange( [ enEmpleado, enVacacion ] );
               prSISTBorrarPOLL: SetDataChange( [ enPoll ] );
               prRHCierreVacacionGlobal: SetDataChange( [ enEmpleado, enVacacion ] );
               prRHEntregarHerramienta: SetDataChange( [ enTools, enPrestamo ] );
               prRHRegresarHerramienta: SetDataChange( [ enTools, enPrestamo ] );
               prSISTBorrarHerramienta: SetDataChange( [ enTools ] );
               prRHImportarAltas: SetDataChange( [ enEmpleado ] );
               prRHPermisoGlobal: SetDataChange( [ enPermiso, enKardex ] );
               prCAFEComidasGrupales: SetDataChange( [ enComida, enInvitacion ] );
               prCAFECorregirFechas: SetDataChange( [ enComida, enInvitacion ] );
               prSISTNumeroTarjeta: SetDataChange( [] );
               prRHCancelarCierreVacaciones: SetDataChange( [ enEmpleado, enVacacion ] );
               prRHCancelarPermisosGlobales: SetDataChange( [ enPermiso, enKardex] );
               prRHImportarAhorrosPrestamos: SetDataChange( [ enPrestamo, enAhorro, enPCarAbo, enACarAbo] );
               prRHRecalculaSaldosVacaciones: SetDataChange( [ enEmpleado, enVacacion ] );
               prNOAjusteRetFonacot: SetDataChange( [ enPrestamo ] );
               prNOAjusteRetFonacotCancelacion: SetDataChange( [ enPrestamo ] );
               prNOCalcularPagoFonacot: SetDataChange( [ enFonCre, enFonEmp, enFonTot ] );
               prSistEnrolarUsuario : SetDataChange( [enUsuarios,enEmpleado] );
               prSistImportarEnrolamientoMasivo  : SetDataChange( [enUsuarios,enEmpleado] );
               prIMSSAjusteRetInfonavit: SetDataChange( [ enPrestamo ] );
               prRHCursoProgGlobal : SetDataChange( [ enEmpProg ] );
               prRHCancelarCursoProgGlobal : SetDataChange( [ enEmpProg ] );
               prASIAjustIncapaCal : SetDataChange( [ enAusencia ] );
               prASISAutorizacionPreNomina : SetDataChange( [ enNomina, enAusencia] );
               prIMSSValidacionMovimientosIDSE : SetDataChange( [ enKardex ] );
               prNoTransferenciasCosteo: SetDataChange( [ enTransferencias ] );
               prNOPrevioISR: SetDataChange( [ enPrevioISR ] );
               prRHImportarSGM: SetDataChange( [ enHisSegGasMed, enCatSegGasMed ] );
               prRHRenovacionSGM: SetDataChange( [ enHisSegGasMed, enCatSegGasMed ] );
               prRHBorrarSGM: SetDataChange( [ enHisSegGasMed, enCatSegGasMed ] );
               prASISImportacionClasificaciones: SetDataChange( [ enAusencia ] ); {JB: 20111031 Importacion de Clasificaciones}
               prNoCancelaTransferencias: SetDataChange( [ enTransferencias ] );
               {$ifndef DOS_CAPAS}
               prSISTDepuraBitBiometrico: SetDataChange( [] ); // SYNERGY
               prSISTAsignaNumBiometricos: SetDataChange( [] ); // SYNERGY
               prSISTAsignaGrupoTerminales: SetDataChange( [] ); // SYNERGY
               prSISTImportaTerminales: SetDataChange( [ enHuella ] ); // SYNERGY
               {$endif}
               prSISTPrepararPresupuestos: SetDataChange( [] );
               prSISTImportarTablas: SetDataChange( [] );
               prSISTImportarTablasCSV: SetDataChange( [] );
               prRHImportarImagenes: SetDataChange( [] );

               prSISTAgregarBaseDatosSeleccion: SetDataChange( [] );
               prSISTAgregarBaseDatosVisitantes: SetDataChange( [] );
               prSISTAgregarBaseDatosPruebas: SetDataChange( [] );
               prSISTAgregarBaseDatosEmpleados: SetDataChange( [] );
               prSISTExtraerChecadas: SetDataChange( [] );
               prSISTRecuperarChecadas: SetDataChange( [] );   //@DC
               prEmpCambioTurnoMasivo: SetDataChange( [] );
               prSISTImportarCafeteria: SetDataChange( [enDispositivos] );
               {$ifdef TRESS_DELPHIXE5_UP}
               prSISTAgregarTareaCalendarioReportes: SetDataChange( [enTareaCalendario] );
               {$endif}
               prNOFonacotImportarCedulas : SetDataChange( [ enPrestamo ]  );
               prNOFonacotEliminarCedula : SetDataChange( [ enPrestamo ]  );
          end;
     end;
end;

function GetCxWizardClass( const eProceso: Procesos ): TCXWizardBasicoClass;
begin
     case eProceso of
          prRHSalarioIntegrado: Result := TWizEmpSalarioIntegrado_DevEx;
          prRHCambioSalario: Result := TWizEmpCambioSalario_DevEx;
          prRHPromediarVariables: Result := TWizEmpPromVar_DevEx;
          prRHVacacionesGlobales: Result := TWizEmpVacaciones_DevEx; //DevEx(by am)
          prRHAplicarTabulador: Result := TWizEmpAplicarTabulador_DevEx;
          prRHAplicarEventos: Result := TWizEmpEventos_DevEx;
          prRHImportarKardex: Result := TWizEmpImportarKardex_DevEx;
          prRHCancelarKardex: Result := TWizEmpCancelarKardex_DevEx;
          prRHCursoTomado: Result := TWizEmpCursoTomado_DevEx; //DevEx(by am)

          prRHRenumeraEmpleados: Result := TWizEmpRenumeraEmpleados_DevEx;
          prRHBorrarCursoTomado: Result := TWizEmpBorrarCursoTomado_DevEx; //DevEx(by am)
          prRHImportarAsistenciaSesiones: Result := TWizEmpImportarAsistenciaSesiones_DevEx;  //DevEx(by am)

          prRHImportarAhorrosPrestamos: Result := TWizEmpImportarAhorrosPrestamos_DevEx;
          prASISProcesarTarjetas: Result := TWizAsistProcesarTarjetas_DevEx;
          prASISPollRelojes: Result := TWizAsistPollRelojes_DevEx;
          prASISCalculoPreNomina: Result := TWizAsistCalculoPrenomina_DevEx;
          prASISExtrasPer: Result := TWizAsistAutorizarHoras_DevEx;
          prASISRegistrosAut: Result := TWizAsistRegistrosAut_DevEx;
          prASISCorregirFechas: Result := TWizAsistCorregirFechas_DevEx;
          prASISRecalculoTarjetas: Result := TWizAsistRecalcular_DevEx;
          prASISIntercambioFestivo: Result := TWizAsistIntercambioFestivo_DevEx;
          prASISCancelarExcepcionesFestivos: Result := TWizAsistCancelarExcepcionesFestivo_DevEx;
          prASISRegistrarExcepcionesFestivos: Result := TWizAsistRegistrarExcepcionesFestivo_DevEx;

          prNOCalcular: Result := TWizNomCalcular_DevEx; //DevEx(by am)
          prNOAfectar: Result := TWizNomAfectar_DevEx; //DevEx(by am)
          {prNOImprimirListado: Result := nil;
          prNOImprimirRecibos: Result := nil;
          }
          prNOPolizaContable: Result := TWizNomPoliza_DevEx;
          prNODesafectar: Result := TWizNomDesafectar_DevEx; //DevEx(by am)
          prNOLimpiarAcum: Result := TWizNomLimpiarAcum_DevEx; //DevEx(by am)
          {prNOImportarMov: Result := TWizNomImportarMov;
          prNOExportarMov: Result := TWizNomExportarMov;
          prNOPagosPorFuera: Result := TWizNomPagarPorFuera;}
          prNOCancelarPasadas: Result := TWizNomCancelar_DevEx; //DevEx(by am) 
          //prNOLiquidacion: Result := TWizNomLiquidacionGlobal;
          prNOCalculoNetoBruto: Result := TWizNomSalNetoBruto_DevEx; //DevEx(by am)
          prNORastrearCalculo: Result := TWizNomRastrear_DevEx; //DevEx(by am)
          prNOFoliarRecibos: Result := TWizNomFoliarRecibos_DevEx; //DevEx(by am)
          prNOCreditoAplicado: Result := TWizNomCreditoAplicado_DevEx;
         
          prNODefinirPeriodos: Result := TWizNomDefinirPeriodos_DevEx;

          prNOCalculoAguinaldo: Result := TWizNomCalcularAguinaldo_DevEx;
          prNORepartoAhorro: Result := TWizNomCalcularRepartoAhorro_devEx;
          prNOCalculoPTU: Result := TWizNomPTUCalcular_DevEx; //DevEx(by am)
          prNOCreditoSalario: Result := TWizNomDeclaracionCreditoSalario_DevEx; //DevEx(by am)
          prNOISPTAnual: Result := TWizNomISPTAnual_DevEx;//DevEx(by am)
          prNORecalcAcum: Result := TWizNomRecalculoAcumulado_DevEx; //DevEx(by am)
          {*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
          prNORecalcAhorros: Result := TWizNomRecalculoAhorros_DevEx; //DevEx(by am)
          {prNOCalculaRetroactivos: Result := TWizNomRetroactivos;}
          {*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
          prNORecalcPrestamos: Result := TWizNomRecalculoPrestamos_DevEx; //DevEx(by am)
          {*** FIN ***}
          prNODeclaracionAnual: Result := TWizNomDeclaracionAnual_DevEx; //DevEx(by am)
          prNoDeclaracionAnualCierre: Result := TWizNomDeclaracionAnualCierre_DevEx; //DevEx(by am)
          prIMSSCalculoPagos: Result := TWizIMSSCalcular_DevEx;
          prIMSSCalculoRecargos: Result := TWizIMSSRecargos_DevEx;
          prIMSSRevisarSUA: Result := TWizIMSSRevisarSUA_DevEx;
          prIMSSExportarSUA: Result := TWizIMSSExportarSUA_DevEx;
          prIMSSCalculoPrima: Result := TWizIMSSPrimaRiesgo_DevEx;
          {prIMSSTasaINFONAVIT: Result := TWizIMSSTasaINFONAVIT;
          prASISAjusteColectivo: Result := nil;
          prSISTBorrarBajas: Result := TWizSISTBorrarBajas;
          prSISTBorrarTarjetas: Result := TWizSistBorrarTarjetas;
          prSISTDepurar: Result := TWizSistBorrarBitacora;}
          prRHTranferencia: Result := TWizEmpTransferencia_DevEx;
          prNOPagoAguinaldo: Result := TWizNomPagarAguinaldo_DevEx;
          prNOPagoRepartoAhorro: Result := TWizNomPagarRepartoAhorro_DevEx;
          prNOPagoPTU: Result := TWizNomPTUPago_DevEx; //DevEx(by am)
          prSISTCierreAhorros: Result := TWizSISTCerrarAhorros_DevEx;
          prSISTCierrePrestamos: Result := TWizSISTCerrarPrestamos_DevEx;
          {prSISTBorrarNominas: Result := TWizSistBorrarNominas; }

          prNOISPTAnualPago: Result := TWizNomISPTAnualPago_DevEx; //DevEx(by am)
          prNOPagoRecibos: Result := TWizNomPagoRecibos_DevEx; //DevEx(by am)
          prNOReFoliarRecibos: Result := TWizNomReFoliarRecibos_DevEx; //DevEx(by am)
          {prNOCopiarNominas: Result := TWizNomCopiar;
          prNOCalcularDiferencias: Result := TWizNomCalcularDiferencias;}
          prRHCancVacGlobales: Result := TWizEmpCancelarVacaciones_DevEx; //DevEx(by am)

          //prSISTBorrarPOLL: Result := TWizSistBorrarPoll;
          prRHCierreVacacionGlobal: Result := TWizEmpCerrarVacaciones_DevEx; //DevEx(by am)
          prRHEntregarHerramienta: Result := TWizEmpEntHerr_DevEx;
          prRHRegresarHerramienta: Result := TWizEmpRegresaHerr_DevEx;
          {prSISTBorrarHerramienta: Result := TWizSistBorrarHerramientas;
                 }
          prRHImportarAltas: Result := TWizEmpImportarAltas_DevEx;
          
          prSISTNumeroTarjeta: Result := TWizSistNumeroTarjeta_DevEx;
          prRHCancelarCierreVacaciones: Result := TWizEmpCancelarCierreVacaciones_DevEx; //DevEx(by am)
          prRHPermisoGlobal: Result := TWizEmpPermisoGlobal_DevEx;
          prCAFEComidasGrupales: Result := TWizEmpComidasGrupales_DevEx;
          prCAFECorregirFechas: Result := TWizEmpCorregirFechasCafe_DevEx;
          prRHCancelarPermisosGlobales: Result := TWizEmpCancelarPermisosGlobales_DevEx;
          prRHRecalculaSaldosVacaciones: Result := TWizEmpRecalculaSaldosVaca_DevEx; //DevEx(by am)
          prNOAjusteRetFonacot : Result := TWizNomAjusteRetFonacot_DevEx; //DevEx(by am)
          prNOAjusteRetFonacotCancelacion : Result := TWizNomAjusteRetFonacotCancelacion_DevEx;
          prNOCalcularPagoFonacot : Result:= TWizNomCalcularPagoFonacot_DevEx; //DevEx(by am)
          prSistEnrolarUsuario : Result:= TWizSistEnrolamientoMasivo_DevEx;

          prSistImportarEnrolamientoMasivo : Result:= TWizSistImportarEnrolamientoMasivo_DevEx;
          {prIMSSAjusteRetInfonavit : Result:= TWizIMSSAjusteRetInfonavit; }
          prRHCursoProgGlobal: Result := TWizEmpProgCursoGlobal_DevEx; //DevEx(by am)
          prRHCancelarCursoProgGlobal: Result := TWizEmpCancelarProgCursoGlobal_DevEx; //DevEx(by am)
          {prRHFoliarCapacitaciones : Result := TWizEmpFoliarCapacitaciones;}
          prRHFoliarCapacitaciones : Result := TWizEmpFoliarCapacitacionesSTPS_DevEx; //DevEx(by am)
          prASIAjustIncapaCal: Result := TWizAsistAjustIncapaCal_DevEx;
          prASISAutorizacionPreNomina : Result := TAutorizacionPrenomina_DevEx;
          prIMSSValidacionMovimientosIDSE : Result := TWizIMSSValidacionIDSE_DevEx;
          prNoTransferenciasCosteo : Result := TWizNomTransferenciasCosteo_DevEx;
          prASISImportacionClasificaciones: Result := TImportacionClasificaciones_DevEx; //JB: 20111031 Importacion de Clasificaciones

          prSISTDepuraBitBiometrico : Result := TWizDepuraBitBiometrico_DevEx; // SYNERGY
          prSISTAsignaNumBiometricos: Result := TWizAsignaNumBiometricos_DevEx; // SYNERGy
          prSISTAsignaGrupoTerminales: Result := TWizAsignaGrupoTerminales_DevEx; // SYNERGY
          prSISTImportaTerminales: Result := TWizSistImportaTerminales_DevEx; // SYNERGY  //DevEx(by am)
          prNoCalculaCosteo : Result := TWizNomCalculaCosteo_DevEx;
          prNoCancelaTransferencias : Result := TWizNomCancelarTransferenciasCosteo_DevEx;
          prRHImportarSGM: Result := TWizEmpImportarSGM_DevEx;
          prRHRenovacionSGM: Result := TWizEmpRenovacionSGM_DevEx;
          prRHBorrarSGM: Result := TWizEmpBorrarSGM_DevEx;
          prNOPrevioISR : Result := TWizNomPrevioISR_DevEx;
          prRHRevisarDatosSTPS : Result := TWizEmpValidarDC4_DevEx;  //DevEx(by am)
          prRHReiniciarCapacitacionesSTPS : Result := TWizEmpReiniciarCapacitacionesSTPS_DevEx; //DevEx(by am)
          prRHImportarOrganigrama : Result := TWizImportarOrganigrama_DevEx;

          prIMSSAjusteRetInfonavit : Result:= TWizIMSSAjusteRetInfonavit_DevEx;
          prSISTBorrarBajas: Result := TWizSISTBorrarBajas_DevEx;
          prSISTBorrarNominas: Result := TWizSISTBorrarNominas_DevEx;
          prSISTBorrarTimbradoNominas: Result := TWizSISTDestimbrarNominas_DevEx;
          prSISTBorrarTarjetas: Result := TWizSistBorrarTarjetas_DevEx;
          prSISTBorrarPOLL: Result := TWizSistBorrarPoll_DevEx;
          prSISTDepurar: Result := TWizSistBorrarBitacora_DevEx;
          prSISTBorrarHerramienta: Result := TWizSistBorrarHerramientas_DevEx;
          prNOLiquidacion: Result := TWizNomLiquidacionGlobal_DevEx;
          prNOImportarMov: Result := TWizNomImportarMov_DevEx;
          prNOExportarMov: Result := TWizNomExportarMov_DevEx;
          prNOPagosPorFuera: Result := TWizNomPagarPorFuera_DevEx;
          prNOCalculaRetroactivos: Result := TWizNomRetroactivos_DevEx;
          prNOCopiarNominas: Result := TWizNomCopiar_DevEx;
          prNOCalcularDiferencias: Result := TWizNomCalcularDiferencias_DevEx;
          prSISTImportarTablas : Result := TWizSistImportarTablas_DevEx;
          prSISTImportarTablasCSV : Result := TWizSistImportarTablasCSV_DevEx;
          prRHImportarImagenes: Result  := TWizEmpImportarImagenes_DevEx;
          prSISTAgregarBaseDatosSeleccion: Result := TWizAgregarBaseDatosSeleccion_DevEx;
          prSISTAgregarBaseDatosVisitantes: Result := TWizAgregarBaseDatosVisitantes_DevEx;
          prSISTAgregarBaseDatosPruebas: Result := TWizAgregarBaseDatosPruebas_DevEx; //DevEx(by am)
          prSISTAgregarBaseDatosEmpleados: Result := TWizAgregarBaseDatosEmpleados_DevEx; //DevEx(by am)
          prSISTPrepararPresupuestos: Result := TWizAgregarBaseDatosPresupuestos_DevEx;  //DevEx(by am)
          prSISTExtraerChecadas: Result := TWizSistExtraerChecadasReloj;
          prSISTRecuperarChecadas: Result := TWizSistRecuperaChecadas_DevEx; //DevEx(By @DC)
          prEmpCambioTurnoMasivo: Result := TWizEmpCambioTurno;
          {*** US 13889: Crear proceso Importar acumulados ***}
          prNOImportarAcumuladosRS: Result := TWizNomImportarMovAcumuladosRS_DevEx;
          {$ifdef TRESS_DELPHIXE5_UP}
          prSISTImportarCafeteria: Result := TWizSistCopiarConfiguracionCafeteria_DevEx;
          prSISTAgregarTareaCalendarioReportes: Result := TWizSistCalendarioReportes_DevEx;
          {$endif}
          {$ifndef VALIDADORDIMM }
          {$ifndef IMPORTAVACACIONES }
          {$ifndef MULTIPLE_RP }
          {$ifndef RECLASIFICACION_PLAZAS }
          {$ifndef INTERFAZ_SY }
          {$ifndef INTERFAZ_ZK }
          prNOFonacotImportarCedulas : Result:= TWizNomFonacotImportarCedula_DevEx;
          prNOFonacotGenerarCedula : Result:= TWizCalculoConfPago;
          prNOFonacotEliminarCedula : Result := TWizNomFonacotEliminarCedula_DevEx;
          {$endif}
          {$endif}
          {$endif}
          {$endif}
          {$endif}
          {$endif}
     else
     Result := Nil;
     end;
end;

procedure ShowWizard( const eProceso: Procesos );
var
   WizardCXClass:TcxWizardBasicoClass;
begin
     dmProcesos.cdsDataSet.Init;
     WizardCXClass := nil;
     WizardCXClass :=  GetCxWizardClass ( eProceso );
     ZCXWizardBasico.ShowWizard( WizardCXClass);
     
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function FechaSUA( const dValue: TDate ): String;
begin
     Result := FormatDateTime( 'ddmmyyyy', dValue );
end;

{ *********** TdmProcesos ************ }

procedure TdmProcesos.DataModuleCreate(Sender: TObject);
begin
     FListaProcesos := TProcessList.Create;
end;

procedure TdmProcesos.DataModuleDestroy(Sender: TObject);
begin
     FListaProcesos.Free;
end;

procedure TdmProcesos.HandleProcessSilent(const iIndice, iValor: Integer);
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             VerBitacora := FALSE;
             OcultarForma := dmCliente.FormsMoved;
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );
             //dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             ShowProcessInfo( True );
             //if ProcessOK then
             SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

procedure TdmProcesos.HandleProcessEnd( const iIndice, iValor: Integer );
var lState : Boolean;
begin
     lState := False;
     with TWizardFeedBack_DevEx.Create( Application ) do
     begin
          try
             OcultarForma := dmCliente.FormsMoved;
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );

             {$ifdef TRESS}
             with ProcessData do
             begin
                  if (Proceso = prSistImportarTablas) or (Proceso = prSistImportarTablasCSV)  then
                     dmConsultas.LeerUnProceso( ShowProcessInfo( True ), TRUE )
                  else
                  begin
                       if (Proceso = prNOFonacotGenerarCedula ) then
                       begin
                            Close;
                            HandleProcessFileEnd( iIndice, iValor );
                       end
                       else
					   begin							
                            lState := True;
                            dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
			           end;
                  end;
             end;
             {$else}
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             {$endif}

             if lState then
                if ProcessOK then
                   SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;

     if lState then
     begin
          TressShell.ReconectaMenu;
          Application.ProcessMessages;
     end;
end;

procedure TdmProcesos.HandleProcessFileEnd( const iIndice, iValor: Integer );
begin
     with TWizardFeedBackAbrir_DevEx.Create( Application ) do
     begin
          try
             OcultarForma := dmCliente.FormsMoved;
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );

             {$ifdef TRESS}
             with ProcessData do
             begin
                  if (Proceso = prSistImportarTablas) or (Proceso = prSistImportarTablasCSV)  then
                     dmConsultas.LeerUnProceso( ShowProcessInfo( True ), TRUE )
                  else
                       dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             end;
             {$else}
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             {$endif}

             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function TdmProcesos.Check( const Resultado: OleVariant): Boolean;
begin
     with TWizardFeedback_DevEx.Create( Application ) do
     begin
          try
             with ProcessData do
             begin
                  SetResultado( Resultado );
                  Result := ( Status in [ epsEjecutando, epsOK ] );
             end;
             if ProcessOK then
                SetMensajes( Self.ProcesarResultado( ProcessData ) );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
                 Free;
          end;
     end;
end;

function TdmProcesos.CheckSilencioso(const Resultado: OleVariant; const lNomina: Boolean ): Boolean;
begin
    if eProcessStatus( Resultado[ K_PROCESO_STATUS ] ) = epsOK then
    begin
         Result := TRUE;
         with TressShell do
         begin
              if lNomina then
                 SetDataChange( [ enPeriodo, enNomina, enMovimien ] )
              else
                 SetDataChange( [ enPeriodo, enNomina, enAusencia ] );
              if Global.GetGlobalBooleano( K_GLOBAL_PLAN_VACA_PRENOMINA ) then
                 SetDataChange( [ enEmpleado, enVacacion, enPlanVacacion ] );
         end;
    end
    else
        Result := Check( Resultado );
end;

function TdmProcesos.ProcesarResultado( Info: TProcessInfo ): String;
begin
     with Info do
     begin
          case Proceso of
               prIMSSExportarSua:
               begin
                    Result := ExportarSUAEnd( Cargo[ 0 ], Cargo[ 1 ], Cargo[ 2 ],Cargo[ 3 ],Cargo[ 4 ] );
               end;
               prIMSSCalculoPrima:
               begin
                    Result := CalcularPrimaRiesgoEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end; 
               prRHRevisarDatosSTPS:
               begin
                    Result := RevisarDatosSTPSEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prNOExportarMov:
               begin
                    Result := ExportarMovEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prNORastrearCalculo:
               begin
                    Result := RastrearEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prNODefinirPeriodos:
               begin
                    { Posicionar sobre el Per�odo 1 del Tipo Generado }
                    dmCliente.PeriodoTipo := eTipoPeriodo( Cargo );
                    {
                    TressShell.RefrescaPeriodo;
                    }
                    Result := '';
               end;
               prSISTNumeroTarjeta:
               begin
                    Result := NumeroTarjetaEnd( Cargo );
               end;
{
               prRHPromediarVariables:
               begin
                    Result := PromediarVariablesEnd;
               end;
}
               prSISTExtraerChecadas:
               begin
                    Result := ExtraerChecadasBitTerminalesEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
               prSISTRecuperarChecadas:
               begin
                    Result := RecuperaChecadasBitTerminalesEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;

               prSISTImportaTerminales:
               begin
                    Result := ImportaTerminalesEnd( Cargo );
               end;

               prNOFonacotGenerarCedula:
               begin
                    Result := GenerarCedulaEnd( Cargo[ 0 ], Cargo[ 1 ] );
               end;
          else
              Result := '';
          end;
     end;
end;

{$ifdef DOS_CAPAS}
function TdmProcesos.GetServerIMSS: TdmServerIMSS;
begin
     Result := DCliente.dmCliente.ServerIMSS;
end;

function TdmProcesos.GetServerAsistencia: TdmServerAsistencia;
begin
     Result := DCliente.dmCliente.ServerAsistencia;
end;


{$ifdef TIMBRADO}

function TdmProcesos.GetServerNomina: TdmServerNominaTimbrado;
begin
     Result := dmInterfase.ServerNominaTimbrado;
end;
{$else}

function TdmProcesos.GetServidorNomina: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;
{$endif}

function TdmProcesos.GetServerCalcNomina: TdmServerCalcNomina;
begin
     Result := DCliente.dmCliente.ServerCalcNomina;
end;

function TdmProcesos.GetServerAnualNomina: TdmServerAnualNomina;
begin
     Result := DCliente.dmCliente.ServerAnualNomina;
end;

function TdmProcesos.GetServerRecursos: TdmServerRecursos;
begin
     Result := DCliente.dmCliente.ServerRecursos;
end;

function TdmProcesos.GetServerSistema: TdmServerSistema;
begin
     Result := DCliente.dmCliente.ServerSistema;
end;

function TdmProcesos.GetServerCafeteria: TdmServerCafeteria;
begin
     Result := DCliente.dmCliente.ServerCafeteria;
end;

{$else}
function TdmProcesos.GetServerIMSS: IdmServerIMSSDisp;
begin
     Result := IdmServerIMSSDisp( dmCliente.CreaServidor( CLASS_dmServerIMSS, FServidorIMSS ) );
end;

function TdmProcesos.GetServerAsistencia: IdmServerAsistenciaDisp;
begin
     Result := IdmServerAsistenciaDisp( dmCliente.CreaServidor( CLASS_dmServerAsistencia, FServidorAsistencia ) );
end;

{$ifdef TIMBRADO}
function TdmProcesos.GetServerNomina: IdmServerNominaTimbradoDisp;
begin
     Result := IdmServerNominaTimbradoDisp( dmCliente.CreaServidor( CLASS_dmServerNominaTimbrado, FServidorNomina  ) );
end;
{$else}
function TdmProcesos.GetServerNomina: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( dmCliente.CreaServidor( CLASS_dmServerNomina, FServidorNomina ) );
end;
{$endif}

function TdmProcesos.GetServerCalcNomina: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( CLASS_dmServerCalcNomina, FServidorCalcNomina ) );
end;

function TdmProcesos.GetServerAnualNomina: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( dmCliente.CreaServidor( CLASS_dmServerAnualNomina, FServidorAnualNomina ) );
end;

function TdmProcesos.GetServerRecursos: IdmServerRecursosDisp;
begin
     Result := IdmServerRecursosDisp( dmCliente.CreaServidor( CLASS_dmServerRecursos, FServidorRecursos ) );
end;

function TdmProcesos.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServidorSistema ) );
end;

function TdmProcesos.GetServerCafeteria: IdmServerCafeteriaDisp;
begin
     Result := IdmServerCafeteriaDisp( dmCliente.CreaServidor( CLASS_dmServerCafeteria, FServidorCafeteria ) );
end;

function TdmProcesos.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( dmCliente.CreaServidor( CLASS_dmServerLogin, FServidorLogin ) );
end;

{$endif}

function TdmProcesos.GetLista( ZetaDataset: TZetaClientDataset ): OLEVariant;
begin
     with ZetaDataset do
     begin
          MergeChangeLog;  // Elimina Registros Borrados //
          Result := Data;
     end;
end;

function TdmProcesos.AgregaMensaje( const sMensaje, sValor: String ): String;
begin
     Result := sMensaje + CR_LF + sValor;
end;

{
procedure TdmProcesos.FiltraASCII( const lFiltrar: Boolean );
begin
     with cdsDataSet do
     begin
          if lFiltrar then
          begin
               Filter := 'NUM_ERROR > 0';
               Filtered := True;
          end
          else
          begin
               Filtered := False;
               Filter := '';
          end;
     end;
end;

procedure TdmProcesos.FillcdsASCII(const sArchivo: String);
var
   FAsciiServer: TAsciiServer;
begin
     with cdsASCII do
     begin
          if Active then
             Close;
          CreateDataSet;
          Open;
     end;
     FAsciiServer := TAsciiServer.Create;
     with FAsciiServer do
     begin
          try
             if Open( sArchivo ) then
             begin
                  repeat
                        cdsASCII.AppendRecord( [ Data ] );
                  until not Read;
                  Close;
                  //Borrar;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ClearErroresASCII( oDataSet: TDataSet );
begin
     with oDataSet do
     begin
          First;
          while not Eof do
          begin
               if ( FieldByName( 'NUM_ERROR' ).AsInteger > 0 ) then
                  Delete
               else
                   Next;
          end;
          First;
     end;
end;
}
{ ********** Procesos de Asistencia ********** }

function TdmProcesos.CorreThreadAsistencia(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TAsistenciaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorregirFechas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prASISCorregirFechas, NULL, Parametros );
end;

function TdmProcesos.BorrarTarjetas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prSISTBorrarTarjetas, NULL, Parametros );
end;

function TdmProcesos.BorrarPoll(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prSISTBorrarPOLL, NULL, Parametros );
end;

function TdmProcesos.AutorizarHoras(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASISExtrasPer, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASISExtrasPer, NULL, Parametros );
end;

procedure TdmProcesos.AutorizarHorasGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.AutorizarHorasGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;


function TdmProcesos.RegistrarExcepcionesFestivos(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASISRegistrarExcepcionesFestivos, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASISRegistrarExcepcionesFestivos, NULL, Parametros );
end;

procedure TdmProcesos.RegistrarExcepcionesFestivosGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.RegistrarExcepcionesFestivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CancelarExcepcionesFestivos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASISCancelarExcepcionesFestivos, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASISCancelarExcepcionesFestivos, NULL, Parametros );
end;

procedure TdmProcesos.CancelarExcepcionesFestivosGetLista( Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.CancelarExcepcionesFestivosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.IntercambioFestivo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASISIntercambioFestivo, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASISIntercambioFestivo, NULL, Parametros );
end;

procedure TdmProcesos.IntercambioFestivoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.IntercambioFestivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.AjusteIncapaCal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASIAjustIncapaCal, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASIAjustIncapaCal, NULL, Parametros );
end;

procedure TdmProcesos.AjusteIncapaCalGetLista(Parametros: TZetaParams);
begin
     cdsDataset.Data := ServerAsistencia.AjustarIncapaCalGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AutorizacionPreNomina( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prASISAutorizacionPreNomina, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prASISAutorizacionPreNomina, NULL, Parametros );
end;

procedure TdmProcesos.AutorizacionPreNominaGetLista(Parametros: TZetaParams);
begin
    cdsDataset.Data := ServerAsistencia.AutorizacionPreNominaGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

{JB: 20111031 Importacion de Clasificaciones}

function TdmProcesos.ImportarClasificacionesTemporales(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prASISImportacionClasificaciones, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.ImportarClasificacionesTemporalesGetListaASCII( oParametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, oParametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerAsistencia.GetClasificacionTempLista( dmCliente.Empresa, oParametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;     
end;

procedure TdmProcesos.ImportarClasificacionesTemporalesGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then
          ClearErroresASCII( cdsDataset );
     with cdsDataset do
     begin
          Data := ServerAsistencia.GetClasificacionTempGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataset ) );
     end;
end;

{ *************** Procesos de CalcNomina ************* }

function TdmProcesos.CorreThreadCalcNomina(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TCalcNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.Rastrear(Parametros: TZetaParams ): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadCalcNomina( prNORastrearCalculo, NULL, Parametros );
end;

function TdmProcesos.RastrearEnd( const Parametros, Rastreo: OleVariant ): String;
var
   sArchivo: String;
begin
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sArchivo := ParamByName( 'Archivo' ).AsString;
          finally
                 Free;
          end;
     end;
     with TStringList.Create do
     begin
          try
             Text := Rastreo;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;

{ ************ Fin de Procesos de CalcNomina ********** }

{ *************** Procesos de CalcNominaBDE ************* }

function TdmProcesos.CorreThreadCalcNominaBDE(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TCalcNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadCalcNominaSilent(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TCalcNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          SilentExecute := TRUE;
          Resume;
     end;
     Result := True;
end;
function TdmProcesos.CalcularNomina(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadCalcNominaBDE( prNOCalcular, NULL, Parametros );
end;

function TdmProcesos.CalcularPrenomina(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadCalcNominaBDE( prASISCalculoPrenomina, NULL, Parametros );
end;

function TdmProcesos.Poll(Parametros: TZetaParams): Boolean;
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     cdsASCII.Active := True;
     Result := CorreThreadCalcNominaBDE( prASISPollRelojes, GetLista( cdsASCII ), Parametros );
     cdsASCII.Active := False;
end;

function TdmProcesos.ProcesarTarjetasPendientes(Parametros:TZetaParams): Boolean;
begin
    Result := CorreThreadCalcNominaBDE( prASISProcesarTarjetas, NULL, Parametros );
end;

function TdmProcesos.RecalcularTarjetas(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadCalcNominaBDE( prASISRecalculoTarjetas, NULL, Parametros );
end;

function TdmProcesos.RegistrosAutomaticos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadCalcNominaBDE( prASISRegistrosAut, NULL, Parametros );
end;

{ ************ Fin de Procesos de CalcNominaBDE ********** }

{ *********** Procesos de IMSS ************* }

function TdmProcesos.CorreThreadIMSS(const eProceso: Procesos; Parametros: TZetaParams ): Boolean;
begin
     dmCliente.CargaActivosIMSS( Parametros );
     with TIMSSThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadIMSS(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     dmCliente.CargaActivosIMSS( Parametros );
     with TIMSSThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CalcularPagosIMSS(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadIMSS( prIMSSCalculoPagos, Parametros );
end;

function TdmProcesos.CalcularRecargosIMSS( Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSCalculoRecargos, Parametros );
end;

function TdmProcesos.CalcularTasaINFONAVIT(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSTasaINFONAVIT, Parametros );
end;

function TdmProcesos.RevisarSUA(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSRevisarSUA, Parametros );
end;

function TdmProcesos.ExportarSUA(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSExportarSUA, Parametros );
end;

function TdmProcesos.ValidarMovimientosIDSE(Parametros: TZetaParams): Boolean;
begin
       Result := CorreThreadIMSS( prIMSSValidacionMovimientosIDSE, GetLista( dmIMSS.cdsMovimientosIDSE ), Parametros );      
end;

procedure TdmProcesos.ExportarSUAEmpleados( Sender: TAsciiExport; DataSet: TDataset );
var
   rPrestamo: TPesos;
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'SUA_E_PAT' ).Datos := FieldByName( 'SUA_E_PAT' ).AsString;    { RegistroPatronal }
               ColumnByName( 'SUA_E_SS' ).Datos := FieldByName( 'SUA_E_SS' ).AsString;      { NumeroSeguro }
               ColumnByName( 'SUA_E_RFC' ).Datos := FieldByName( 'SUA_E_RFC' ).AsString;    { Registro Federal de Causantes }
               ColumnByName( 'SUA_E_CUR' ).Datos := FieldByName( 'SUA_E_CUR' ).AsString;    { # CURP }
               ColumnByName( 'SUA_E_NOMB' ).Datos := FieldByName( 'SUA_E_NOMB' ).AsString;  { Nombre del Asegurado }
               ColumnByName( 'SUA_E_MODP' ).Datos := IntToStr( FieldByName( 'SUA_E_MODP' ).AsInteger );   { M�dulo Patr�n }
               ColumnByName( 'SUA_E_JORN' ).Datos := IntToStr( FieldByName( 'SUA_E_JORN' ).AsInteger );   { Tipo Jornada }
               ColumnByName( 'SUA_E_INGR' ).Datos := FechaSUA( FieldByName( 'SUA_E_INGR' ).AsDateTime );  { Fecha Ingreso }
               ColumnByName( 'SUA_E_WAGE' ).Datos := ZetaCommonTools.StrToZero( 100 * FieldByName( 'SUA_E_WAGE' ).AsFloat, 7, 0 ); { Salario SUA }
               ColumnByName( 'SUA_E_ID' ).Datos := FieldByName( 'SUA_E_ID' ).AsString;      { Indentificaci�n }
               rPrestamo := FieldByName( 'SUA_E_LOAN' ).AsFloat;
               if ( rPrestamo > 0 ) then { Monto Pr�stamo INFONAVIT }
               begin
                    ColumnByName( 'SUA_E_CRED' ).Datos := FieldByName( 'SUA_E_CRED' ).AsString;               { # Cr�dito INFONAVIT }
                    ColumnByName( 'SUA_E_FE_P' ).Datos := FechaSUA( FieldByName( 'SUA_E_FE_P' ).AsDateTime ); { Fecha Pr�stamo INFONAVIT }
                    ColumnByName( 'SUA_E_TIPO' ).Datos := IntToStr( FieldByName( 'SUA_E_TIPO' ).AsInteger );  { Tipo Pr�stamo INFONAVIT }

                    if eTipoInfonavit( FieldByName( 'SUA_E_TIPO' ).AsInteger ) = tiCuotaFija then
                       ColumnByName( 'SUA_E_LOAN' ).Datos := ZetaCommonTools.StrToZero( Round( 1000 * rPrestamo ), 8, 0 ) { Monto Pr�stamo INFONAVIT de Cuota Fija en la Forma DDDDD.DD0 }
                    else
                        ColumnByName( 'SUA_E_LOAN' ).Datos := ZetaCommonTools.StrToZero( Round( 10000 * rPrestamo ), 8, 0 ); { Monto Pr�stamo INFONAVIT }
               end
               else
               begin
                    ColumnByName( 'SUA_E_CRED' ).Datos := Space( 10 );
                    ColumnByName( 'SUA_E_FE_P' ).Datos := Space( 8 );
                    ColumnByName( 'SUA_E_TIPO' ).Datos := Space( 1 );
                    ColumnByName( 'SUA_E_LOAN' ).Datos := ZetaCommonTools.StrToZero( 0, 8, 0 );
               end;
          end;
     end;
end;

function TdmProcesos.CrearAsciiEmpleados (cdsDataSet: TZetaClientDataSet;const sArchivoEmpleados:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoEmpleados, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  AgregaColumna( 'SUA_E_PAT', tgTexto, 11 );     { RegistroPatronal }
                  AgregaColumna( 'SUA_E_SS', tgTexto, 11 );      { NumeroSeguro }
                  AgregaColumna( 'SUA_E_RFC', tgTexto, 13 );     { Registro Federal de Causantes }
                  AgregaColumna( 'SUA_E_CUR', tgTexto, 18 );     { # CURP }
                  AgregaColumna( 'SUA_E_NOMB', tgTexto, 50 );    { Nombre del Asegurado }
                  AgregaColumna( 'SUA_E_MODP', tgNumero, 1 );    { M�dulo Patr�n }
                  AgregaColumna( 'SUA_E_JORN', tgNumero, 1 );    { Tipo Jornada }
                  AgregaColumna( 'SUA_E_INGR', tgTexto, 8 );     { Fecha Ingreso }
                  AgregaColumna( 'SUA_E_WAGE', tgFloat, 7 );     { Salario SUA }
                  AgregaColumna( 'SUA_E_ID', tgTexto, 17 );      { Indentificaci�n }
                  AgregaColumna( 'SUA_E_CRED', tgTexto, 10 );    { # Cr�dito INFONAVIT }
                  AgregaColumna( 'SUA_E_FE_P', tgTexto, 8 );     { Fecha Pr�stamo INFONAVIT }
                  AgregaColumna( 'SUA_E_TIPO', tgNumero, 1 );    { Tipo Pr�stamo INFONAVIT }
                  AgregaColumna( 'SUA_E_LOAN', tgTexto, 8 );     { Monto Pr�stamo INFONAVIT }
                  AlExportarColumnas := ExportarSUAEmpleados;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Empleados Exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUADatosAfiliatorios( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'SUA_E_PAT' ).Datos := FieldByName( 'SUA_E_PAT' ).AsString;                  { RegistroPatronal }
               ColumnByName( 'SUA_E_SS' ).Datos := FieldByName( 'SUA_E_SS' ).AsString;                    { NumeroSeguro }
               ColumnByName( 'SUA_E_CODP' ).Datos := FieldByName( 'SUA_E_CODP' ).AsString;                { C�digo Postal }
               ColumnByName( 'SUA_E_FE_N' ).Datos := FechaSUA( FieldByName( 'SUA_E_FE_N' ).AsDateTime);   { Fecha de Nacimiento  }
               ColumnByName( 'SUA_E_LNAC' ).Datos := FieldByName( 'SUA_E_LNAC' ).AsString;                { Lugar de Nacimiento }
               ColumnByName( 'SUA_E_CNAC' ).Datos := FieldByName( 'SUA_E_CNAC' ).AsString;                { Clave de Lugar de Nacimiento }
               ColumnByName( 'SUA_E_CLIN' ).Datos := FieldByName( 'SUA_E_CLIN' ).AsString;                { Cl�nica [Unidad de medicina Familiar UMF] }
               ColumnByName( 'SUA_E_OCUP' ).Datos := FieldByName( 'SUA_E_OCUP' ).AsString;                { Ocupaci�n 'Empleado' }
               ColumnByName( 'SUA_E_SEXO' ).Datos := FieldByName( 'SUA_E_SEXO' ).AsString;                { Sexo [M � F]}
               ColumnByName( 'SUA_E_T_SA' ).Datos := IntToStr( FieldByName( 'SUA_E_T_SA' ).AsInteger );     { Tipo de salario }
               ColumnByName( 'SUA_E_HORA' ).Datos := FieldByName( 'SUA_E_HORA' ).AsString;                { Hora }
          end;
     end;
end;

function TdmProcesos.CrearAsciiDatosAfiliatorios (cdsDataSet: TZetaClientDataSet;const sArchivoDatosAfiliatorios:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoDatosAfiliatorios, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;

                  AgregaColumna( 'SUA_E_PAT', tgTexto, 11 );   { RegistroPatronal }
                  AgregaColumna( 'SUA_E_SS', tgTexto, 11 );    { NumeroSeguro }
                  AgregaColumna( 'SUA_E_CODP',tgTexto,5 );    { C�digo Postal }
                  AgregaColumna( 'SUA_E_FE_N',tgTexto,8 );      { Fecha de Nacimiento }
                  AgregaColumna( 'SUA_E_LNAC',tgTexto,25 );    { Lugar de Nacimiento }
                  AgregaColumna( 'SUA_E_CNAC',tgTexto,2 );    { Clave de Lugar de Nacimiento }
                  AgregaColumna( 'SUA_E_CLIN',tgTexto,3 );    { Cl�nica [Unidad de medicina Familiar UMF] }
                  AgregaColumna( 'SUA_E_OCUP',tgTexto,12 );    { Ocupaci�n 'Empleado'}
                  AgregaColumna( 'SUA_E_SEXO',tgTexto,1 );     { Sexo [M � F] }
                  AgregaColumna( 'SUA_E_T_SA',tgNumero,1 );    { Tipo de salario }
                  AgregaColumna( 'SUA_E_HORA',tgTexto,1 );     { Hora  }

                  AlExportarColumnas := ExportarSUADatosAfiliatorios;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Datos afiliatorios exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUAInfonavit( Sender: TAsciiExport; DataSet: TDataset );
var sValorDescuento :string;
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'SUA_F_PAT' ).Datos := FieldByName( 'SUA_F_PAT' ).AsString;                  { RegistroPatronal }
               ColumnByName( 'SUA_F_SS' ).Datos := FieldByName( 'SUA_F_SS' ).AsString;                    { NumeroSeguro }

               ColumnByName( 'SUA_F_CRED' ).Datos := FieldByName( 'SUA_F_CRED' ).AsString;               { # Credito Infonavit }
               ColumnByName( 'SUA_F_TMOV' ).Datos := FieldByName( 'SUA_F_TMOV' ).AsString;      { Tipo de Movimiento  }
               ColumnByName( 'SUA_F_FMOV' ).Datos := FechaSUA( FieldByName( 'SUA_F_FMOV' ).AsDateTime );    { Fecha de Movimiento }
               ColumnByName( 'SUA_F_TDES' ).Datos := IntToStr( FieldByName( 'SUA_F_TDES' ).AsInteger );   { Tipo de Descuento }


                 if eTipoInfonavit( FieldByName( 'SUA_F_TDES' ).AsInteger ) = tiCuotaFija then
                 begin
                       //otra idea : ColumnByName( 'SUA_F_VDES' ).Datos := ZetaCommonTools.StrToZero( Round( 1000 * FieldByName( 'SUA_F_VDES' ).AsFloat ), 8, 0 )
                       { Monto Pr�stamo INFONAVIT de Cuota Fija en la Forma DDDDD.DD0 }
                       sValorDescuento := ZetaCommonTools.StrToZero( FieldByName( 'SUA_F_VDES' ).AsFloat, 9, 3 );  //9 = digitos + punto
                       ColumnByName( 'SUA_F_VDES' ).Datos := StrLeft(sValorDescuento,5)+StrRight(sValorDescuento,3); { Valor de Descuento }
                 end
                 else
                 begin
                      sValorDescuento := ZetaCommonTools.StrToZero( FieldByName( 'SUA_F_VDES' ).AsFloat, 9, 4 );
                      ColumnByName( 'SUA_F_VDES' ).Datos := StrLeft(sValorDescuento,4)+StrRight(sValorDescuento,4); { Valor de Descuento }
                 end;

               ColumnByName( 'SUA_F_DISM' ).Datos := FieldByName( 'SUA_F_DISM' ).AsString;                { Aplica Dism %  }
          end;
     end;
end;

function TdmProcesos.CrearAsciiInfonavit (cdsDataSet: TZetaClientDataSet;const sArchivoInfonavit:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoInfonavit, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;

                  AgregaColumna( 'SUA_F_PAT',tgTexto,11 );    { RegistroPatronal }
                  AgregaColumna( 'SUA_F_SS',tgTexto,11 );     { NumeroSeguro }
                  AgregaColumna( 'SUA_F_CRED',tgTexto,10 );  { # Credito Infonavit }
                  AgregaColumna( 'SUA_F_TMOV',tgTexto,2 );   { Tipo de Movimiento }
                  AgregaColumna( 'SUA_F_FMOV',tgTexto,8 );    { Fecha de Movimiento }
                  AgregaColumna( 'SUA_F_TDES',tgNumero,1 );   { Tipo de Descuento }
                  AgregaColumna( 'SUA_F_VDES',tgTexto,8 );   { Valor de Descuento }
                  AgregaColumna( 'SUA_F_DISM',tgTexto, 1 );   { Aplica Dism % }

                  AlExportarColumnas := ExportarSUAInfonavit;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Datos de Infonavit exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUAMovimientos( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               if ( FieldByName( 'SUA_M_DATE' ).AsDateTime >= FFechaLimiteMovSUA ) then
               begin
                    ColumnByName( 'SUA_M_PAT' ).Datos := FieldByName( 'SUA_M_PAT' ).AsString;                    { RegistroPatronal }
                    ColumnByName( 'SUA_M_SS' ).Datos := FieldByName( 'SUA_M_SS' ).AsString;                      { NumeroSeguro }
                    ColumnByName( 'SUA_M_TIPO' ).Datos := FieldByName( 'SUA_M_TIPO' ).AsString;                  { Tipo }
                    ColumnByName( 'SUA_M_DATE' ).Datos := FechaSUA( FieldByName( 'SUA_M_DATE' ).AsDateTime );    { Fecha };
                    ColumnByName( 'SUA_M_INCA' ).Datos := FieldByName( 'SUA_M_INCA' ).AsString;                  { Incapacidad }
                    ColumnByName( 'SUA_M_DIAS' ).Datos := ZetaCommonTools.StrToZero( FieldByName( 'SUA_M_DIAS' ).AsFloat, 2, 0 );       { Dias };
                    ColumnByName( 'SUA_M_MONT' ).Datos := ZetaCommonTools.StrToZero( 100 * FieldByName( 'SUA_M_MONT' ).AsFloat, 7, 0 ); { Monto };
               end
               else
                   Abort;   // Con una excepci�n no incluye el registro en el ASCII
          end;
     end;
end;

function TdmProcesos.CrearAsciiMovimientos (cdsDataSet: TZetaClientDataSet;const sArchivoMovimientos:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoMovimientos, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  AgregaColumna( 'SUA_M_PAT', tgTexto, 11 );     { RegistroPatronal }
                  AgregaColumna( 'SUA_M_SS', tgTexto, 11 );      { NumeroSeguro }
                  AgregaColumna( 'SUA_M_TIPO', tgTexto, 2 );     { Tipo }
                  AgregaColumna( 'SUA_M_DATE', tgTexto, 8 );     { Fecha };
                  AgregaColumna( 'SUA_M_INCA', tgTexto, 8 );     { Incapacidad }
                  AgregaColumna( 'SUA_M_DIAS', tgTexto, 2 );     { Dias }
                  AgregaColumna( 'SUA_M_MONT', tgTexto, 7 );     { Monto }
                  AlExportarColumnas := ExportarSUAMovimientos;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Movimientos exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

procedure TdmProcesos.ExportarSUAIncapacidades( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               if ( FieldByName( 'SUA_I_FINI' ).AsDateTime >= FFechaLimiteMovSUA ) then
               begin
                    ColumnByName( 'SUA_I_PAT' ).Datos := FieldByName( 'SUA_I_PAT' ).AsString;                    { RegistroPatronal }
                    ColumnByName( 'SUA_I_SS' ).Datos := FieldByName( 'SUA_I_SS' ).AsString;                      { NumeroSeguro }
                    ColumnByName( 'SUA_I_TIPO' ).Datos := IntToStr( FieldByName( 'SUA_I_TIPO' ).AsInteger );      { Tipo }
                    ColumnByName( 'SUA_I_FINI' ).Datos := FechaSUA( FieldByName( 'SUA_I_FINI' ).AsDateTime );    { Fecha }
                    ColumnByName( 'SUA_I_FOLIO' ).Datos := FieldByName( 'SUA_I_FOLIO' ).AsString;                { Folio }
                    ColumnByName( 'SUA_I_DIAS' ).Datos := StrZero( IntToStr( FieldByName( 'SUA_I_DIAS' ).AsInteger ),3 );    { Dias }
                    ColumnByName( 'SUA_I_INCA' ).Datos := StrToZero( MiRound( FieldByName( 'SUA_I_INCA' ).AsFloat,0 ),3,0 );      { % Incapacidad si no tiene 0 }
                    ColumnByName( 'SUA_I_RAMA' ).Datos := IntToStr( FieldByName( 'SUA_I_RAMA' ).AsInteger );     { Rama de incapacidad }
                    ColumnByName( 'SUA_I_RSGO' ).Datos := IntToStr( FieldByName( 'SUA_I_RSGO' ).AsInteger );     { Tipo de Riesgo }
                    ColumnByName( 'SUA_I_SCLA' ).Datos := IntToStr( FieldByName( 'SUA_I_SCLA' ).AsInteger );     { Secuela o consecuencia }
                    ColumnByName( 'SUA_I_CTIN' ).Datos := IntToStr( FieldByName( 'SUA_I_CTIN' ).AsInteger );     { Control de incapacidad [Motivo] }
                    ColumnByName( 'SUA_I_FFIN' ).Datos := FechaSUA( FieldByName( 'SUA_I_FFIN' ).AsDateTime );    { Fecha Final }
               end
               else
                   Abort;   // Con una excepci�n no incluye el registro en el ASCII
          end;
     end;
end;

function TdmProcesos.CrearAsciiIncapacidades (cdsDataSet: TZetaClientDataSet;const sArchivoIncapacidades:string):String;
begin
     with TAsciiExport.Create do
     begin
          try
             if Abre( sArchivoIncapacidades, True ) then
             begin
                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  AgregaColumna( 'SUA_I_PAT', tgTexto,11 );    { RegistroPatronal }
                  AgregaColumna( 'SUA_I_SS', tgTexto,11 );     { NumeroSeguro }
                  AgregaColumna( 'SUA_I_TIPO',tgNumero,1 );    { Tipo }
                  AgregaColumna( 'SUA_I_FINI',tgTexto,8 );     { Fecha de Inicio }
                  AgregaColumna( 'SUA_I_FOLIO',tgTexto,8 );    { Folio }
                  AgregaColumna( 'SUA_I_DIAS',tgNumero,3 );    { Dias }
                  AgregaColumna( 'SUA_I_INCA',tgTexto,3 );     { % Incapacidad si no tiene 0 }
                  AgregaColumna( 'SUA_I_RAMA',tgNumero,1 );    { Rama de incapacidad }
                  AgregaColumna( 'SUA_I_RSGO',tgNumero,1 );    { Tipo de Riesgo }
                  AgregaColumna( 'SUA_I_SCLA',tgNumero,1 );    { Secuela o consecuencia }
                  AgregaColumna( 'SUA_I_CTIN',tgNumero,1 );    { Control de incapacidad [Motivo] }
                  AgregaColumna( 'SUA_I_FFIN',tgTexto,8 );     { Fecha Final }

                  AlExportarColumnas := ExportarSUAIncapacidades;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Datos de Incapacidades exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
             Cierra;
          finally
                 Free;
          end;
     end;
end;

function TdmProcesos.ExportarSUAEnd( const Parametros, Empleados, Movimientos,Infonavit,Incapacidades: OleVariant ): String;
var
   oCursor: TCursor;
   lEmpleados, lMovimientos, lBorrarEmpleados : Boolean;
   sPatron, sRegistro, sPath, sArchivoEmpleados,sArchivoDatosAfiliatorios,sArchivoInfonavit,sArchivoIncapacidades, sArchivoMovimientos : String;
   eBorrarMovimientos: eBorrarMovimSUA;
   iYear, iMes, iVersionSUA: Integer;

   //(@am): CAMBIO TEMPORAL
   procedure BorraSUA2000;
   begin
        {with TBorradorSUA.Create( Self ) do
        begin
             try
                try
                   Empleados := lBorrarEmpleados;
                   Movimientos := ( eBorrarMovimientos <> bsNinguno );  // En SUA anterior siempre se borra todo, no se puede indicar solo lo del mes
                   Patron := sRegistro;
                   Path := sPath;
                   Borrar;
                except
                      on Error: Exception do
                      begin
                           Result := AgregaMensaje( Result, 'Error Al Borrar Archivos de SUA En' + CR_LF + sPath + CR_LF + Error.Message );
                      end;
                end;
             finally
                    Free;
             end;
         end;}
   end;

   procedure BorraSUA2006;
   begin
        {$IFDEF TRESS_DELPHIXE5_UP}
        dmBorradorSUA := TdmBorradorSUA.Create( Self );
        try
           with dmBorradorSUA do
           begin
                try
                   Empleados := lBorrarEmpleados;
                   BorrarMovimientos := eBorrarMovimientos;
                   Patron := sRegistro;
                   Path := sPath;
                   Year := iYear;
                   Mes  := iMes;
                   Borrar;
                except
                      on Error: Exception do
                      begin
                           Result := AgregaMensaje( Result, 'Error Al Borrar Archivos de SUA En' + CR_LF + sPath + CR_LF + Error.Message );
                      end;
                end;
           end;
        finally
               dmBorradorSUA.Free;
        end;
        {$ENDIF}
   end;

begin {ExportarSUAEnd}
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        InitAnimation( 'Exportando SUA' );
        with TZetaParams.Create do
        begin
             try
                VarValues := Parametros;
                sPatron := ParamByName( 'Patron' ).AsString;
                sRegistro := ParamByName( 'NumeroRegistro' ).AsString;
                sPath := Global.GetGlobalString( K_GLOBAL_DIR_DATOS_SUA );
                lEmpleados := ParamByName( 'EnviarEmpleados' ).AsBoolean;
                sArchivoEmpleados := ParamByName( 'ArchivoEmpleados' ).AsString;
                sArchivoDatosAfiliatorios :=  ParamByName('ArchivoDatosAfiliatorios').AsString;
                sArchivoInfonavit :=  ParamByName('ArchivoInfonavit').AsString;
                lBorrarEmpleados := ParamByName( 'BorrarEmpleados' ).AsBoolean;
                lMovimientos := ParamByName( 'EnviarMovimientos' ).AsBoolean;
                sArchivoIncapacidades :=  ParamByName('ArchivoIncapacidad').AsString;
                sArchivoMovimientos := ParamByName( 'ArchivoMovimientos' ).AsString;
                eBorrarMovimientos := eBorrarMovimSUA( ParamByName( 'BorrarMovimientos' ).AsInteger );
                iYear := ParamByName( 'Year' ).AsInteger;
                iMes := ParamByName( 'Mes' ).AsInteger;
                iVersionSUA := ParamByName( 'VersionSUA' ).AsInteger;
             finally
                    Free;
             end;
        end;
        Result := '';
        if lEmpleados then
        begin
             cdsDataSet.Data := Empleados;
             Result := AgregaMensaje( Result, CrearAsciiEmpleados(cdsDataSet,sArchivoEmpleados) );
             {$ifdef VER130}
             cdsDataSet.Data := Empleados;
             {$endif}
             Result := AgregaMensaje( Result, CrearAsciiDatosAfiliatorios(cdsDataSet,sArchivoDatosAfiliatorios) );
        end;
        if lMovimientos then
        begin
             with cdsDataSet do
             begin
                  Data := Movimientos;
                  if ( eBorrarMovimientos = bsNinguno ) then    // Si no se borra nada en SUA deben filtrarse las incapacidades que comenzaron el mes pasado
                     FFechaLimiteMovSUA := CodificaFecha( iYear, iMes, 1 )
                  else
                      FFechaLimiteMovSUA := NullDateTime;
             end;
             Result := CrearAsciiMovimientos (cdsDataSet,sArchivoMovimientos) ;
              cdsDataset.Data := Incapacidades;
             Result := AgregaMensaje( Result, CrearASCIIIncapacidades( cdsDataSet,sArchivoIncapacidades ) );

             cdsDataset.Data := Infonavit;
             Result := AgregaMensaje( Result, CrearAsciiInfonavit(cdsDataSet,sArchivoInfonavit) );
        end;
        if lBorrarEmpleados or ( eBorrarMovimientos <> bsNinguno ) then
        begin
             case iVersionSUA of
                  K_VERSION_SUA_2000 : BorraSUA2000;
                  K_VERSION_SUA_2006 : BorraSUA2006;
             else
                 Result := AgregaMensaje( Result, 'No se tiene implementado mecanismo de borrado para esta versi�n de SUA' );
             end;
        end;
     finally
            EndAnimation;
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

function TdmProcesos.CalcularPrimaRiesgo(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadIMSS( prIMSSCalculoPrima, Parametros );
end;

function TdmProcesos.CalcularPrimaRiesgoEnd(const Parametros, Datos: OleVariant): String;
var
   oCursor: TCursor;
   sArchivo: String;
begin
     Result := '';
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TZetaParams.Create do
        begin
             try
                VarValues := Parametros;
                sArchivo := ParamByName( 'Archivo' ).AsString;
             finally
                    Free;
             end;
        end;
        with TStringList.Create do
        begin
             try
                Text := Datos;
                SaveToFile( sArchivo );
             finally
                    Free;
             end;
        end;
        ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
     finally
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmProcesos.AjusteRetInfonavitGetLista(Parametros: TZetaParams);
begin
     cdsDataSet.Data:= ServerImss.AjusteRetInfonavitGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AjusteRetInfonavit(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadIMSS( prIMSSAjusteRetInfonavit, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadIMSS( prIMSSAjusteRetInfonavit, NULL, Parametros );
end;

{ ********** Fin de Procesos IMSS ************* }

{ ************ Procesos Anuales de N�mina *************** }
function TdmProcesos.CorreThreadAnual(const eProceso: Procesos; const Lista: OleVariant; const lCargarPeriodo: Boolean; Parametros: TZetaParams): Boolean;
begin
     if lCargarPeriodo then
     begin
          with dmCliente do
          begin
               CargaActivosPeriodo( Parametros );
          end;
     end;
     with TAnualThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CerrarAhorros(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prSISTCierreAhorros, NULL, False, Parametros );
end;

function TdmProcesos.CerrarPrestamos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prSISTCierrePrestamos, NULL, False, Parametros );
end;

function TdmProcesos.CalcularAguinaldo(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOCalculoAguinaldo, NULL, False, Parametros );

     {PENDIENTE: como se muestra esta informacion, primero se
     tiene que calcular en el Servidor, y despues mostrarlo.

     if inherited EjecutarWizard then
     begin
          with AgenteWizard.Parametros do
          begin
               zInformation( Caption,
                             'Empleados: ' + FormatFloat( '#,###,###,###,##0', ParamByName( 'iCuantos' ).AsInteger ) +
                             CR_LF +
                             'Aguinaldos a Pagar: $' + FormatFloat( '#,###,###,###,##0.00', ParamByName( 'rTotal' ).AsFloat ),
                             0 );
          end;
          Result := True;
     end
     else
         Result := False;
     }
end;

function TdmProcesos.PagarAguinaldo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOPagoAguinaldo, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOPagoAguinaldo, NULL, True, Parametros );
end;

procedure TdmProcesos.PagarAguinaldoGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     with cdsDataset do
     begin
          Data := ServerAnualNomina.PagarAguinaldoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.PTUCalcular(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     Result := CorreThreadAnual( prNOCalculoPTU, NULL, False, Parametros );
end;

function TdmProcesos.PTUPago(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOPagoPTU, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOPagoPTU, NULL, True, Parametros );
end;

procedure TdmProcesos.PTUPagoGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.PTUPagoGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

function TdmProcesos.CalcularRepartoAhorro(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNORepartoAhorro, NULL, False, Parametros );
     {      PENDIENTE
     FALTA PODER MOSTRAR LOS DATOS CALCULADOS
     DESPUES DE HABER SIDO CALCULADOS.

     if inherited EjecutarWizard then
     begin
          with AgenteWizard.Parametros do
          begin
               zInformation( Caption,
                             'Empleados: ' + FormatFloat( '#,###,###,###,##0', ParamByName( 'iCuantos' ).AsInteger ),
                             0 );
          end;
          Result := True;
     end
     else
         Result := False;
     }
end;

function TdmProcesos.PagarRepartoAhorro(Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOPagoRepartoAhorro, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOPagoRepartoAhorro, NULL, True, Parametros );
end;

procedure TdmProcesos.PagarRepartoAhorroGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.PagarAhorroGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

function TdmProcesos.ISPTAnual(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOISPTAnual, NULL, False, Parametros );
end;

function TdmProcesos.ISPTAnualPago(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOISPTAnualPago, GetLista( cdsDataset ), True, Parametros )
     else
         Result := CorreThreadAnual( prNOISPTAnualPago, NULL, True, Parametros );
end;

procedure TdmProcesos.ISPTAnualPagoGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.ISPTPagoGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

function TdmProcesos.DeclaraCredito(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOCreditoSalario, NULL, False, Parametros );
end;

function TdmProcesos.CalculaRetroactivos(Parametros: TZetaParams; const lVerificacion: Boolean):Boolean;
begin
     if lVerificacion then
        Result := CorreThreadCalcNominaBDE( prNOCalculaRetroactivos, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadCalcNominaBDE( prNOCalculaRetroactivos, NULL, Parametros );
end;

procedure TdmProcesos.CalculaRetroactivoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerCalcNomina.CalculaRetroactivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{ ******** Fin de Procesos Anuales de N�mina ************ }

{ ********** Procesos de Recursos Humanos ********* }

function TdmProcesos.CorreThreadRecursos( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TRecursosThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.BorrarBajas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prSISTBorrarBajas, NULL, Parametros );
end;

function TdmProcesos.BorrarHerramientas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prSISTBorrarHerramienta, NULL, Parametros );
end;

function TdmProcesos.SalNetoBruto(Parametros: TZetaParams): Boolean;
var
    NetoBruto: OleVariant;
begin
     NetoBruto := Parametros.VarValues;
     try
        ServerCalcNomina.CalculaNetoBruto( dmCliente.Empresa, NetoBruto );
        Parametros.VarValues := NetoBruto;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

function TdmProcesos.AplicarTabulador( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHAplicarTabulador, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHAplicarTabulador, NULL, Parametros );
end;

procedure TdmProcesos.AplicarTabuladorGetList( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.AplicarTabuladorGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.PromediarVariables(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHPromediarVariables, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHPromediarVariables, NULL, Parametros );
end;

procedure TdmProcesos.PromediarVariablesGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.PromediarVariablesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{
function TdmProcesos.PromediarVariablesEnd: String;
begin
     Result := 'NO OLVIDE CORRER EL PROCESO DE REDUCCION DE TASAS DE INFONAVIT';
end;
}

function TdmProcesos.SalarioIntegrado(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHSalarioIntegrado, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHSalarioIntegrado, NULL, Parametros );
end;

procedure TdmProcesos.SalarioIntegradoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.SalarioIntegradoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CambioSalario(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCambioSalario, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCambioSalario, NULL, Parametros );
end;

procedure TdmProcesos.CambioSalarioGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CambioSalarioGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.Vacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHVacacionesGlobales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHVacacionesGlobales, NULL, Parametros );
end;

procedure TdmProcesos.VacacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.VacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.Eventos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHAplicarEventos, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHAplicarEventos, NULL, Parametros );
end;

procedure TdmProcesos.EventosGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          IndexFieldNames := 'CB_CODIGO';
          Data := ServerRecursos.EventosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarKardex(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prRHImportarKardex, GetLista( cdsDataset ), Parametros )
end;

function TdmProcesos.CambioMasivoTurnos(Parametros: TZetaParams; const DataSet: TZetaClientDataSet; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prEmpCambioTurnoMasivo, GetLista( DataSet ), Parametros )
     else
        Result := CorreThreadRecursos( prEmpCambioTurnoMasivo, NULL, Parametros );
end;


procedure TdmProcesos.ImportarKardexGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.ImportarKardexGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

procedure TdmProcesos.ImportarKardexGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarKardexGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames := 'CB_CODIGO';
     end;
end;

function TdmProcesos.CancelarKardex(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarKardex, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancelarKardex, NULL, Parametros );
end;

procedure TdmProcesos.CancelarKardexGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarKardexGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CancelarVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancVacGlobales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancVacGlobales, NULL, Parametros );
end;

procedure TdmProcesos.CancelarVacacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarVacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CierreGlobalVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCierreVacacionGlobal, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCierreVacacionGlobal, NULL, Parametros );
end;

procedure TdmProcesos.CierreGlobalVacacionesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CierreGlobalVacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.EntregarHerramienta(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHEntregarHerramienta, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHEntregarHerramienta, NULL, Parametros );
end;

procedure TdmProcesos.EntregarHerramientaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.EntregarHerramientaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.RegresarHerramienta(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHRegresarHerramienta, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHRegresarHerramienta, NULL, Parametros );
end;

procedure TdmProcesos.RegresarHerramientaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.RegresarHerramientaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CursoTomado(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCursoTomado, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHCursoTomado, NULL, Parametros );
end;

procedure TdmProcesos.CursoTomadoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CursoTomadoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.RenumeraEmpleados( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHRenumeraEmpleados, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHRenumeraEmpleados, NULL, Parametros );
end;

procedure TdmProcesos.RenumeraEmpleadosGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.RenumeraGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.RenumeraVerificaListaASCII( Parametros: TZetaParams );
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.RenumeraValidaASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.PermisoGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHPermisoGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHPermisoGlobal, NULL, Parametros );
end;

procedure TdmProcesos.PermisoGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.PermisoGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.BorrarCursoTomado(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHBorrarCursoTomado, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHBorrarCursoTomado, NULL, Parametros );
end;

procedure TdmProcesos.BorrarCursoTomadoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.BorrarCursoTomadoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{ACL. 24.Feb.09. Cursos Programados Global}
function TdmProcesos.CursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCursoProgGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHCursoProgGlobal, NULL, Parametros );
end;

procedure TdmProcesos.CursoProgGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CursoProgGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

{ACL. 27.Feb.09. Cancelar Cursos Programados Global}
function TdmProcesos.CancelarCursoProgGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarCursoProgGlobal, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadRecursos( prRHCancelarCursoProgGlobal, NULL, Parametros );
end;

procedure TdmProcesos.CancelarCursoProgGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarCursoProgGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarAsistenciaSesiones(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prRHImportarAsistenciaSesiones, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarAsistenciaSesionesGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarAsistenciaSesionesGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames := 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarAsistenciaSesionesGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.ImportarAsistenciaSesionesGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.CancelarCierreVacaciones(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarCierreVacaciones, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancelarCierreVacaciones, NULL, Parametros );
end;

procedure TdmProcesos.CancelarCierreVacacionesGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.CancelarCierreVacacionesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

procedure TdmProcesos.CancelarPermisosGlobalesGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          try
             AfterOpen := PermisosAfterOpen;
             Data := ServerRecursos.CancelarPermisosGlobalesGetLista( dmCliente.Empresa, Parametros.VarValues );
          finally
                 AfterOpen := Nil;
          end;
     end;
end;

function TdmProcesos.CancelarPermisosGlobales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHCancelarPermisosGlobales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHCancelarPermisosGlobales, NULL, Parametros );

end;

procedure TdmProcesos.PermisosAfterOpen(DataSet: TDataSet);
begin
     with cdsDataSet do
     begin
          with FieldByName('PM_CLASIFI') do
          begin
               Alignment := taLeftJustify;
               OnGetText := PM_CLASIFIOnGetText;
          end;
     end;
end;

procedure TdmProcesos.PM_CLASIFIOnGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
     Text := ObtieneElemento( lfTipoPermiso,cdsDataSet.FieldByName('PM_CLASIFI').AsInteger );
end;

function TdmProcesos.ImportarAhorrosPrestamos(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadRecursos( prRHImportarAhorrosPrestamos, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarAhorrosPrestamosGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          with dmCliente do
          begin
               cdsDataset.Data := ServerRecursos.ImportarAhorrosPrestamosGetASCII( Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          end;
          Active := False;
     end;
end;

function TdmProcesos.RecalculaSaldosVaca(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadRecursos( prRHRecalculaSaldosVacaciones, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadRecursos( prRHRecalculaSaldosVacaciones, NULL, Parametros );

end;

procedure TdmProcesos.RecalculaSaldosVacaGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.RecalculaSaldosVacaGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarSGM(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadRecursos( prRHImportarSGM, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarSGMGetLista(Parametros: TZetaParams);
begin
      if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
          FErrorCount := 0;
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarSGMGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames := 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarSGMGetListaASCII(Parametros: TZetaParams;
  const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          cdsDataset.Data := ServerRecursos.ImportarSGMGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.RenovacionSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if not lVerificacion then
          cdsDataset.Data := ServerRecursos.RenovarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
     Result := CorreThreadRecursos( prRHRenovacionSGM, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.RenovacionSGMGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
          Data := ServerRecursos.RenovarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.BorrarSGM(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if not lVerificacion then
          cdsDataset.Data := ServerRecursos.BorrarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
     Result := CorreThreadRecursos( prRHBorrarSGM, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.BorrarSGMGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
          Data := ServerRecursos.BorrarSGMGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.ImportarOrganigrama(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadRecursos( prRHImportarOrganigrama, GetLista( cdsDataset ), Parametros );

end;

procedure TdmProcesos.ImportarOrganigramaGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerRecursos.ImportarOrganigramaGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames:= 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarOrganigramaGetListaASCII(oParametros: TZetaParams; const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, oParametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          with dmCliente do
          begin
               cdsDataset.Data := ServerRecursos.ImportarOrganigramaGetASCII(Empresa, oParametros.VarValues, GetLista( cdsASCII ), FErrorCount);
          end;
          Active := False;
     end;
end;

{ ********** Fin de Procesos de Recursos Humanos ********* }

{ *********** Procesos de Sistemas ********* }

function TdmProcesos.Transferencia(Parametros: TZetaParams; const EmpresaDestino: Variant): Boolean;
begin
     Result := CorreThreadCalcNomina( prRHTranferencia, EmpresaDestino, Parametros );
     {
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := prRHTranferencia;
          Employees := EmpresaDestino;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
     }
     if Result then
        dmCliente.PosicionaSiguienteEmpleado;
end;

function TdmProcesos.CorreThreadSistema(const eProceso: Procesos;  const Lista: OleVariant; Parametros: TZetaParams): Boolean;
begin
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadSistemaSilent(const eProceso: Procesos;const Lista: OleVariant; Parametros: TZetaParams ; Ventana : HWND;SetResultado:TSetResultadoEspecial): Boolean;
begin
      with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          ProcessInfo.FeedBack := False;
          ProcessInfo.SetResultadoEsp := SetResultado;
          CargaParametros( Parametros );
          SilentExecute := True;
          Resume;
     end;
     Result := True;
end;


function TdmProcesos.BorrarBitacora(Parametros: TZetaParams): Boolean;
begin
    Result := CorreThreadSistema( prSISTDepurar, NULL, Parametros );
     {
     with TSistemaThread.Create( FListaProcesos ) do
     begin
          Proceso := prSISTDepurar;
          Employees := NULL;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
     }
end;

function TdmProcesos.NumeroTarjeta: Boolean;
begin
    Result := CorreThreadSistema( prSISTNumeroTarjeta, NULL, NIL );
end;

function TdmProcesos.NumeroTarjetaEnd( const Bitacora: OleVariant ): String;
var
   sArchivo: String;
begin
     sArchivo := Format( '%sActualizarTarjeta.log', [ ExtractFilePath( Application.ExeName ) ] );
     with TStringList.Create do
     begin
          try
             Text := Bitacora;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;


function TdmProcesos.EnrolarUsuarios(Parametros: TZetaParams): Boolean;
begin
      Result := CorreThreadSistema( prSistEnrolarUsuario, NULL, parametros );
end;

procedure TdmProcesos.EnrolarUsuariosGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerSistema.EnrolamientoMasivoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarEnrolamientoMasivo(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     Result := CorreThreadSistema( prSistImportarEnrolamientoMasivo, GetLista( cdsDataset ), Parametros );
end;


procedure TdmProcesos.ImportarEnrolamientoMasivoGetListaASCII(Parametros: TZetaParams;const lFillASCII: Boolean);
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          with dmCliente do
          begin
               cdsDataset.Data := ServerSistema.ImportarEnrolamientoMasivoGetASCII(Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount,GetDatosUsuarioActivo.LoginAD );
          end;
          Active := False;
     end;
end;

{$ifndef DOS_CAPAS}
// SYNERGY
function TdmProcesos.DepuraBitacoraBio(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTDepuraBitBiometrico, NULL, Parametros );
end;

//SYNERGY
function TdmProcesos.AsignaNumBiometricos(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadSistema( prSISTAsignaNumBiometricos, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadSistema( prSISTAsignaNumBiometricos, NULL, Parametros );
end;

// SYNERGY
function TdmProcesos.AsignaGrupoTerminales( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadSistema( prSISTAsignaGrupoTerminales, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadSistema( prSISTAsignaGrupoTerminales, NULL, Parametros );
end;

// SYNERGY
procedure TdmProcesos.EmpleadosConBiometricosGetLista(Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerSistema.EmpleadosBiometricosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

// SYNERGY
procedure TdmProcesos.EmpleadosSinBiometricosGetLista(Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerSistema.EmpleadosSinBiometricosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

// SYNERGY
function TdmProcesos.ImportaTerminales( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadSistema( prSISTImportaTerminales, NULL, Parametros );
end;

function TdmProcesos.ImportaTerminalesEnd( const Bitacora: OleVariant ): String;
var
   sArchivo: String;
begin
     sArchivo := Format( '%sImportaTerminales.log', [ ExtractFilePath( Application.ExeName ) ] );
     with TStringList.Create do
     begin
          try
             Text := Bitacora;
             SaveToFile( sArchivo );
          finally
                 Free;
          end;
     end;
     ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
end;
{$endif}

function TdmProcesos.AgregarBaseDatosPresupuestos(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTPrepararPresupuestos, NULL, Parametros );
end;

function TdmProcesos.AgregarBaseDatosSeleccion(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTAgregarBaseDatosSeleccion, NULL, Parametros );
end;


function TdmProcesos.AgregarBaseDatosVisitantes(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTAgregarBaseDatosVisitantes, NULL, Parametros );
end;

function TdmProcesos.AgregarBaseDatosPruebas(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTAgregarBaseDatosPruebas, NULL, Parametros );
end;

function TdmProcesos.AgregarBaseDatosEmpleados(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTAgregarBaseDatosEmpleados, NULL, Parametros );
end;

function TdmProcesos.ImportarCafeteria(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadSistema( prSISTImportarCafeteria, NULL, Parametros );
end;

function TdmProcesos.ImportarImagenes(Parametros:TZetaParams): Boolean;
begin
     Result := CorreThreadRecursos( prRHImportarImagenes, GetLista( cdsDataset ), Parametros )
end;

function TdmProcesos.ImportarTablas( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadSistema( prSISTImportarTablas, NULL, Parametros );
end;

function TdmProcesos.ImportarTablaCSV( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadSistema( prSISTImportarTablasCSV, GetLista( cdsDataset ), Parametros )
end;

procedure TdmProcesos.ImportarTablaCSVGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;         
          cdsDataset.Data := ServerSistema.ImportarTablaCSVGetASCII( dmConsultas.BaseDatosImportacionXMLCSV, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Active := False;
     end;
end;

function TdmProcesos.ExtraerChecadasBitTerminales(Parametros:TZetaParams): Boolean;
begin
    Result := CorreThreadSistema( prSISTExtraerChecadas, NULL, Parametros );
end;

function TdmProcesos.ExtraerChecadasBitTerminalesEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   sFileName: String;
begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sFileName := ParamByName( 'Archivo' ).AsString;
          finally
                 Free;
          end;
     end;         

     Archivo := TAsciiExport.Create;
     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin
                  AgregaColumna( 'WS_MIENT', tgTexto, 31 );
                  AlExportarColumnas := ExtraerChecadasBitTermEscribeASCII;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Checadas exportadas' );

                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;

// ----- -----

function TdmProcesos.RecuperaChecadas(Parametros:TZetaParams): Boolean;    //@DC
begin
     Result := CorreThreadSistema( prSISTRecuperarChecadas, NULL, Parametros );
end;

// @DC
function TdmProcesos.RecuperaChecadasBitTerminalesEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   // eFormato: eFormatoASCII;
   sFileName: String;
begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sFileName := ParamByName( 'RutaSalidaArchivo' ).AsString;
          finally
                 Free;
          end;
     end;
     Archivo := TAsciiExport.Create;
     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin
                  AgregaColumna( 'CHECADA_RELOJ', tgTexto, 31 );
                  AlExportarColumnas := RecuperaChecadasBitTermEscribeASCII;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIFijo, cdsDataset ) ) + ' Checadas exportadas' );

                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;


{ ********** Fin de Procesos de Sistemas ********* }

{ ********** Proceso de Importaci�n de Altas ****** }
function TdmProcesos.ImportarAltas(Parametros: TZetaParams): Boolean;
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     Result := CorreThreadRecursos( prRHImportarAltas, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarAltasGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             cdsDataSet.AfterOpen := ValidacionesAfterOpen;
             cdsDataset.Data := ServerRecursos.ImportarAltasGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          finally
                 cdsDataSet.AfterOpen := Nil;
          end;
          Active := False;
     end;
end;

procedure TdmProcesos.ValidacionesAfterOpen(DataSet: TDataSet);
begin
      with cdsDataSet do
      begin
           with FieldByName('CB_CODIGO') do
           begin
                Alignment := taLeftJustify;
                OnGetText := CB_CODIGOOnGetText;
           end;
      end;
end;

procedure TdmProcesos.CB_CODIGOOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if (Sender.AsInteger = 0) then
                  Text := 'Por Asignar'
               else
                    Text := Sender.AsString;
          end;
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmProcesos.ImportarAltasGetASCIICatalogos(Parametros: TZetaParams);
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          try
             //cdsDataSet.AfterOpen := ImportarAltasAfterOpen;
             cdsDataset.Data := ServerRecursos.ImportarAltasValidaCatalogos( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          finally
                 cdsDataSet.AfterOpen := Nil;
          end;
          Active := False;
     end;
end;

{procedure TdmProcesos.ImportarAltasAfterOpen(DataSet: TDataSet);
begin
      with cdsDataSet do
      begin
           with FieldByName('ENTIDAD') do
           begin
                Alignment := taLeftJustify;
                OnGetText := ENTIDADOnGetText;
           end;
      end;
end;}

{procedure TdmProcesos.ENTIDADOnGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          with Sender.DataSet do
          begin
               if IsEmpty then
                  Text := VACIO
               else
                    Text := ObtieneEntidad( TipoEntidad(Sender.AsInteger) );
          end;
     end
     else
         Text:= Sender.AsString;
end;}

{procedure TdmProcesos.ImportarAltasGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     //16/01/03: MA: No se ocupa volver a hacer otra llamada al servidor ya que el Dataset ya viene listo
     //con el formato adecuado para ser mostrado
     GetLista( cdsDataSet );
end;}

{procedure TdmProcesos.GrabaVerificaCatalogos;
var
   eEntidad, eEntidadAnt: TipoEntidad;
   oLookupDataSet: TZetaLookupDataSet;
begin
     eEntidad := enNinguno;
     eEntidadAnt := enNinguno;
     oLookupDataSet := Nil;
     with cdsDataSet do
     begin
          First;
          while not Eof do
          begin
               eEntidad := TipoEntidad( FieldByName('ENTIDAD').AsInteger );
               if ( (eEntidad <> eEntidadAnt)and(eEntidadAnt <> enNinguno)and(oLookupDataSet <> nil) ) then
               begin
                    with oLookupDataSet do
                    begin
                         Edit;
                         Enviar;
                    end;
               end;
               case(eEntidad) of
                     enContrato : oLookupDataSet := dmCatalogos.cdsContratos;
                     enPuesto :  oLookupDataSet := dmCatalogos.cdsPuestos;
                     enClasifi : oLookupDataSet := dmCatalogos.cdsClasifi;
                     enTurno :   oLookupDataSet := dmCatalogos.cdsTurnos;
                     enRPatron : oLookupDataSet := dmCatalogos.cdsRPatron;
                     enNivel1 :  oLookupDataSet := dmTablas.cdsNivel1;
                     enNivel2 :  oLookupDataSet := dmTablas.cdsNivel2;
                     enNivel3 :  oLookupDataSet := dmTablas.cdsNivel3;
                     enNivel4 :  oLookupDataSet := dmTablas.cdsNivel4;
                     enNivel5 :  oLookupDataSet := dmTablas.cdsNivel5;
                     enNivel6 :  oLookupDataSet := dmTablas.cdsNivel6;
                     enNivel7 :  oLookupDataSet := dmTablas.cdsNivel7;
                     enNivel8 :  oLookupDataSet := dmTablas.cdsNivel8;
                     enNivel9 :  oLookupDataSet := dmTablas.cdsNivel9;
                     enSSocial : oLookupDataSet := dmCatalogos.cdsSSocial;
                     enEdoCivil :oLookupDataSet := dmTablas.cdsEstadoCivil;
                     enTransporte: oLookupDataSet := dmTablas.cdsTransporte;
                     enEntidad : oLookupDataSet := dmTablas.cdsEstado;
                     enViveCon : oLookupDataSet := dmTablas.cdsViveCon;
                     enViveEn :  oLookupDataSet := dmTablas.cdsHabitacion;
                     enEstudios: oLookupDataSet := dmTablas.cdsEstudios;
                     enExtra1 : oLookupDataSet := dmTablas.cdsExtra1;
                     enExtra2 : oLookupDataSet := dmTablas.cdsExtra2;
                     enExtra3 : oLookupDataSet := dmTablas.cdsExtra3;
                     enExtra4 : oLookupDataSet := dmTablas.cdsExtra4;
                     enNivel0 : oLookupDataSet := dmSistema.cdsNivel0;
                     enKarFija: oLookupDataSet := dmCatalogos.cdsOtrasPer;
               end;
               GrabaCatalogos( oLookupDataSet, eEntidad );
               eEntidadAnt := eEntidad;
               Next;
          end;
          if( oLookupDataSet <> Nil)and(eEntidad<>enNinguno)then
          begin
               oLookupDataSet.Edit;
               oLookupDataSet.Enviar;
          end;
     end;
end;

procedure TdmProcesos.GrabaCatalogos( oDataSet: TZetaLookupDataSet; eEntidad: TipoEntidad );
var
   sCodigo, sDescripcion: String;
begin
     with oDataSet do
     begin
          sCodigo := cdsDataSet.FieldByName('CODIGO').AsString;
          sDescripcion := cdsDataSet.FieldByName('DESCRIPCION').AsString;
          Conectar;
          Append;
          case (eEntidad) of
               enPuesto: begin
                              FieldByName('PU_CODIGO').AsString := sCodigo;
                              FieldByName('PU_DESCRIP').AsString := sDescripcion;
                         end;
               enTurno: begin
                             FieldByName('TU_CODIGO').AsString := sCodigo;
                             FieldByName('TU_DESCRIP').AsString := sDescripcion;
                        end;
          else
              begin
                   FieldByName('TB_CODIGO').AsString := sCodigo;
                   FieldByName('TB_ELEMENT').AsString := sDescripcion;
              end;
          end;
          Post;
     end;
end;}

{ ********** Fin del Proceso de Importar Altas *********** }
{ *********** Procesos de N�minas *********** }

function TdmProcesos.CorreThreadNomina(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.CorreThreadNominaSilent(const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          SilentExecute := TRUE;
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.BorrarNominas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prSISTBorrarNominas, NULL, Parametros );
end;

function TdmProcesos.BorrarTimbradoNominas(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prSISTBorrarTimbradoNominas, NULL, Parametros );
end;


procedure TdmProcesos.ImportarMovGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin
     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          with dmCliente do
          begin
               // (JB) Fix de Bug 631-2702
               CargaActivosPeriodo( Parametros );
               CargaActivosIMSS( Parametros );
               CargaActivosSistema( Parametros );
               cdsDataset.Data := ServerNomina.ImportarMovimientosGetASCII( Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          end;
          Active := False;
     end;
end;

function TdmProcesos.ImportarMov(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          // (JB) Fix de Bug 631-2702
          CargaActivosPeriodo( Parametros );
          CargaActivosIMSS( Parametros );
          CargaActivosSistema( Parametros );
     end;
     ClearErroresASCII( cdsDataset );
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadNomina( prNOImportarMov, GetLista( cdsDataset ), Parametros );
end;
{*** US 13895: Modificar proceso Importar movimientos de n�mina para delimitar su alcance a excepciones de n�mina ***}
function TdmProcesos.ImportarAcumuladosRS(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          // (JB) Fix de Bug 631-2702
          CargaActivosPeriodo( Parametros );
          CargaActivosIMSS( Parametros );
          CargaActivosSistema( Parametros );
     end;
     ClearErroresASCII( cdsDataset );
     cdsDataset.IndexFieldNames:= 'CB_CODIGO';  //Para que se vaya ordenado ?
     Result := CorreThreadNomina( prNOImportarAcumuladosRS, GetLista( cdsDataset ), Parametros );
end;
{*** FIN ***}

procedure TdmProcesos.ExportarMovEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'CB_CODIGO' ).Datos := ZetaCommonTools.FormateaNumero( FieldByName( 'CB_CODIGO' ).AsInteger, 9, ' ' );
               ColumnByName( 'CO_NUMERO' ).Datos := ZetaCommonTools.FormateaNumero( FieldByName( 'CO_NUMERO' ).AsInteger, 5, ' ' );
               ColumnByName( 'MO_MONTO' ).Datos := FormatFloat( '000000000000000.00', FieldByName( 'MO_MONTO' ).AsFloat );
               ColumnByName( 'MO_REFEREN' ).Datos := FieldByName( 'MO_REFEREN' ).AsString;
          end;
     end;
end;

function TdmProcesos.ExportarMovEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   eFormato: eFormatoASCII;
   sFileName: String;
begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             sFileName := ParamByName( 'Archivo' ).AsString;
             eFormato := eFormatoASCII( ParamByName( 'Formato' ).AsInteger );
          finally
                 Free;
          end;
     end;
     Archivo := TAsciiExport.Create;
     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin
                  AgregaColumna( 'CB_CODIGO', tgNumero, 9 );
                  AgregaColumna( 'CO_NUMERO', tgNumero, 5 );
                  AgregaColumna( 'MO_MONTO', tgFloat, 18 ); {30-Abr-2003: Se cambio de 17 a 18, para que coincida con la Importacion}
                  AgregaColumna( 'MO_REFEREN', tgTexto, 10 );
                  AlExportarColumnas := ExportarMovEscribeASCII;
                  Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( eFormato, cdsDataset ) ) + ' Movimientos Exportados' );
                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;

function TdmProcesos.ExportarMov(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOExportarMov, NULL, Parametros );
end;

function TdmProcesos.AfectarNomina(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOAfectar, NULL, Parametros );
end;

function TdmProcesos.DesafectarNomina(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNODesafectar, NULL, Parametros );
end;

function TdmProcesos.DefinirPeriodos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNODefinirPeriodos, NULL, Parametros );
end;

function TdmProcesos.LimpiarAcum(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOLimpiarAcum, NULL, Parametros );
end;

function TdmProcesos.RecalculoAcumulado(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNORecalcAcum, NULL, Parametros );
end;

{*** US 13909: Modificar proceso Recalcular Acumulados para delimitar el alcance solo a acumulados de n�mina***}
function TdmProcesos.RecalculoAhorros(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNORecalcAhorros, NULL, Parametros );
end;
{*** FIN ***}

{*** US 15765: Es necesario separar los procesos (wizards) de recalculo de ahorros y pr�stamos para administrar de mejor manera los accesos al proceso y prevenir rec�lculos innecesarios***}
function TdmProcesos.RecalculoPrestamos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNORecalcPrestamos, NULL, Parametros );
end;
{*** FIN ***}

procedure TdmProcesos.RecalculoDias( const iTipo: Integer );
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
             { El Tipo proviene del Combo Box y no necesariamente
             concide con el Valor Activo }
             Parametros.ParamByName( 'Tipo' ).AsInteger := iTipo;
        end;
        ServerNomina.RecalculoDias( dmCliente.Empresa, Parametros.VarValues );
     finally
            Parametros.Free;
     end;
end;

function TdmProcesos.CalcularDiferencias(Parametros: TZetaParams): Boolean;
{
const
     K_MESSAGE = 'Se Encontraron %s Conceptos Diferentes en %s N�minas';
var
   iNominas, iMovimientos: Integer;
}
begin
     Result := CorreThreadNomina( prNOCalcularDiferencias, NULL, Parametros );
     {
     if Result then
     begin
          lbMensaje.Caption := 'Las Diferencias Entre Las N�minas Fueron Calculadas';
          with AgenteWizard.Parametros do
          begin
               iNominas := ParamByName( 'iNumeroDiferencias' ).AsInteger;
               iMovimientos := ParamByName( 'iStatusDiferencias' ).AsInteger;
          end;
          if ( iNominas = 0 ) then
             zWarning( Caption, 'No Hay Diferencias Entre Las N�minas', 0, mbOk )
          else
              zInformation( Caption, Format( K_MESSAGE, [ IntToStr( iMovimientos ), IntToStr( iNominas ) ] ), 0 );
     end;
     }
end;

function TdmProcesos.CopiarNomina(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOCopiarNominas, NULL, Parametros );
end;

function TdmProcesos.FoliarRecibos(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOFoliarRecibos, NULL, Parametros );
end;

function TdmProcesos.CreditoAplicado( Parametros: TZetaParams; const lVerificacion: Boolean ): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNOCreditoAplicado, GetLista( cdsDataset ), FALSE, Parametros )
     else
         Result := CorreThreadAnual( prNOCreditoAplicado, NULL, FALSE, Parametros );
end;

procedure TdmProcesos.CreditoAplicadoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAnualNomina.CreditoAplicadoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ReFoliarRecibos(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOReFoliarRecibos, NULL, Parametros );
end;

function TdmProcesos.PagarPorFuera(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     if lVerificacion then
        Result := CorreThreadNomina( prNOPagosPorFuera, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadNomina( prNOPagosPorFuera, NULL, Parametros );
end;

procedure TdmProcesos.PagarPorFueraGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     with cdsDataset do
     begin
          Data := ServerNomina.PagarPorFueraGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.Poliza(Parametros: TZetaParams; aFiltros: OleVariant ): Boolean;
{
var
   rTotCar, rTotAbo: TPesos;

function FormatValor( const rValor: TPesos ): String;
begin
     Result := FormatFloat( '#,###,###,###,##0.00', rValor )
end;
}
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOPolizaContable, aFiltros, Parametros );
     {
     PENDIENTE:
     como se van a obtener los valores de regreso para mostrarlos?

     with AgenteWizard.Parametros do
     begin
          rTotCar := ParamByName( 'rTotCargos' ).AsFloat;
          rTotAbo := ParamByName( 'rTotAbonos' ).AsFloat;
     end;
     if PesosIguales( rTotCar, rTotAbo ) then
     begin
          if PesosIguales( rTotCar, 0 ) then
             zWarning( '� P�liza En Ceros !', 'La Suma De Los Cargos y Abonos Es Cero', 0, mbOk )
          else
              zInformation( '� P�liza Ha Cuadrado !', 'La P�liza Generada Cuadr� En ' + FormatValor( rTotCar ), 0 )
     end
     else
         zWarning( '� P�liza No Ha Cuadrado !', 'Suma De Cargos ( $' + FormatValor( rToTCar ) + ' ) Diferente de Suma De Abonos ( $' + FormatValor( rTotAbo ) + ' )', 0, mbOk );
     Result := True;
     }
end;

function TdmProcesos.LiquidacionGlobal(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     if lVerificacion then
        Result := CorreThreadNomina( prNOLiquidacion, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadNomina( prNOLiquidacion, NULL, Parametros );
end;

procedure TdmProcesos.LiquidacionGlobalLista( Parametros: TZetaParams );
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     with cdsDataset do
     begin
          Data := ServerNomina.LiquidacionGlobalGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ImportarPagoRecibos(Parametros: TZetaParams): Boolean;
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
     end;
     Result := CorreThreadNomina( prNOPagoRecibos, GetLista( cdsDataset ), Parametros );
end;

procedure TdmProcesos.ImportarPagoRecibosGetLista(Parametros: TZetaParams);
begin
     if ( FErrorCount > 0 ) then         // Si hubo errores quitarlos del DataSet
     begin
          ClearErroresASCII( cdsDataset );
     end;
     with cdsDataset do
     begin
          Data := ServerNomina.ImportarPagoRecibosGetLista( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsDataSet ) );
          IndexFieldNames:= 'CB_CODIGO';
     end;
end;

procedure TdmProcesos.ImportarPagoRecibosGetListaASCII(Parametros: TZetaParams);
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Open;
          cdsDataset.Data := ServerNomina.ImportarPagoRecibosGetASCII( dmCliente.Empresa, Parametros.VarValues, GetLista( cdsASCII ), FErrorCount );
          Close;
     end;
end;

procedure TdmProcesos.LlenaParametrosCalculo( oParametros: TZetaParams; const lNomina: Boolean );
begin
     with dmCliente do
     begin
          CargaActivosIMSS( oParametros );
          CargaActivosPeriodo( oParametros );
          CargaActivosSistema( oParametros );
          with oParametros do
          begin
               AddString( 'RangoLista', '' );
               AddString( 'Condicion', '' );
               { El UNICO empleado que se calcula es el Empleado ACTIVO }
               AddString( 'Filtro', Format( 'CB_CODIGO = %d', [ Empleado ] ) );
               AddInteger( 'TipoCalculo', Ord( tcNueva ) );
               if lNomina then
               begin
                    AddBoolean( 'MuestraTiempos', False );
                    AddBoolean( 'MuestraScript', False );
               end
               else
               begin
                    AddBoolean( 'PuedeCambiarTarjeta', PuedeModificarTarjetasAnteriores );
               end;
          end;
     end;
end;

procedure TdmProcesos.ExtraerChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'WS_MIENT' ).Datos := FieldByName( 'WS_MIENT' ).AsString;
          end;
     end;
end;

procedure TdmProcesos.RecuperaChecadasBitTermEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );//@DC
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'CHECADA_RELOJ' ).Datos := FieldByName( 'CHECADA_RELOJ' ).AsString;
          end;
     end;
end;

procedure TdmProcesos.CalcularNominaEmpleado( const lNomina : Boolean );
var
   Parametros: TZetaParams;
   Resultado : OleVariant;
begin
     Parametros := TZetaParams.Create;
     try
        LlenaParametrosCalculo( Parametros, lNomina );
        if lNomina then
            Resultado := ServerCalcNomina.CalculaNomina( dmCliente.Empresa, Parametros.VarValues )
        else
            Resultado := ServerCalcNomina.CalculaPreNomina( dmCliente.Empresa, Parametros.VarValues );
        CheckSilencioso( Resultado, lNomina );
     finally
            Parametros.Free;
     end;
end;

procedure TdmProcesos.CalcularSimulacionEmpleado( const TipoPeriodo: eTipoPeriodo; const iPeriodo: Integer );
var
   Parametros: TZetaParams;
   Resultado : OleVariant;
begin
     Parametros := TZetaParams.Create;
     try
        LlenaParametrosCalculo( Parametros, TRUE );
        with Parametros do
        begin
             AddInteger( 'Tipo', Ord( TipoPeriodo ) );
             AddInteger( 'Numero', iPeriodo );
        end;
        Resultado := ServerCalcNomina.CalculaNomina( dmCliente.Empresa, Parametros.VarValues );
        CheckSilencioso( Resultado, TRUE );
     finally
            Parametros.Free;
     end;
end;

function TdmProcesos.CalcularSimulacionGlobal( const iPeriodo: Integer ): Boolean;
var
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        LlenaParametrosCalculo( Parametros, TRUE );
        with Parametros do
        begin
             AddInteger( 'Numero', iPeriodo );
             AddString( 'SimulacionCiclo', K_GLOBAL_SI)
        end;
        Result := CorreThreadCalcNominaSilent( prNOCalcular, NULL, Parametros );
     finally
            Parametros.Free;
     end;
end;

function TdmProcesos.LiquidacionGlobalSimulacion( Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNominaSilent( prNOLiquidacion, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadNominaSilent( prNOLiquidacion, NULL, Parametros );
end;

//WizNomDeclaracionAnual
function TdmProcesos.DeclaracionAnual(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAnual( prNODeclaracionAnual, GetLista( cdsDataset ), FALSE, Parametros )
     else
         Result := CorreThreadAnual( prNODeclaracionAnual, NULL, FALSE, Parametros );
end;

procedure TdmProcesos.DeclaracionAnualGetLista(Parametros: TZetaParams);
begin
     with dmCliente do
     begin
          CargaActivosPeriodo( Parametros );
          with cdsDataset do
          begin
               Data := ServerAnualNomina.DeclaracionAnualGetLista( Empresa, Parametros.VarValues );
          end;
     end;
end;

procedure TdmProcesos.GetDeclaracionAnualGlobales;
begin
     cdsDeclaraGlobales.Data := ServerAnualNomina.GetDeclaracionGlobales( dmCliente.Empresa );
end;

procedure TdmProcesos.GrabaDeclaracionAnualGlobales;
 var
    ErrorCount: integer;
begin
     ServerAnualNomina.GrabaDeclaracionGlobales( dmCliente.Empresa,
                                                 cdsDeclaraGlobales.Delta,
                                                 ErrorCount );
end;


//WizNomDeclaracionAnualCierre
function TdmProcesos.DeclaracionAnualCierre(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNODeclaracionAnualCierre, NULL, FALSE, Parametros );
end;

//WizNomAjusteRetFonacot

procedure TdmProcesos.AjusteRetFonacotGetLista(Parametros: TZetaParams);
begin
     cdsDataSet.Data:= ServerNomina.AjusteRetFonacotGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AjusteRetFonacot(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNomina( prNOAjusteRetFonacot, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadNomina( prNOAjusteRetFonacot, NULL, Parametros );
end;


//WizNomAjusteRetFonacotCancelacion
procedure TdmProcesos.AjusteRetFonacotCancelacionGetLista(Parametros: TZetaParams);
begin
     cdsDataSet.Data:= ServerNomina.AjusteRetFonacotCancelacionGetLista( dmCliente.Empresa, Parametros.VarValues );
end;

function TdmProcesos.AjusteRetFonacotCancelacion(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNomina( prNOAjusteRetFonacotCancelacion, GetLista( cdsDataset ), Parametros )
     else
        Result := CorreThreadNomina( prNOAjusteRetFonacotCancelacion, NULL, Parametros );
end;


//WizNomCalcularPagoFonacot


function TdmProcesos.CalcularPagoFonacot(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina( prNOCalcularPagoFonacot, NULL, Parametros );
end;


//WizNomFonacotImportarCedula
function TdmProcesos.ImportarCedulasFonacot(Parametros: TZetaParams): Boolean;
begin
     //Result := CorreThreadNomina (prNOFonacotImportarCedulas, GetLista(cdsDataset), Parametros);
     with TNominaThread.Create( FListaProcesos ) do
     begin
          Proceso := prNOFonacotImportarCedulas;
          Employees := GetLista(cdsDataset);
          CargaParametros( Parametros );
          OnTerminate := ImportarFonacot;
          Resume;
     end;
     Result := True;
end;

procedure TdmProcesos.ImportarFonacot(Sender: TObject);
begin
     {$ifndef VALIDADORDIMM }
     {$ifndef IMPORTAVACACIONES }
     {$ifndef MULTIPLE_RP }
     {$ifndef RECLASIFICACION_PLAZAS }
     {$ifndef INTERFAZ_SY }
     {$ifndef INTERFAZ_ZK }
     ZBaseDlgModal_DevEx.ShowDlgModal( DetalleImportacionCedula_DevEx, TDetalleImportacionCedula_DevEx );
     {$endif}
     {$endif}
     {$endif}
     {$endif}
     {$endif}
     {$endif}
end;

//WizCalculoConfPago
function TdmProcesos.GenerarCedulaFonacot(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina (prNOFonacotGenerarCedula, NULL, Parametros );
end;

function TdmProcesos.GenerarCedulaEnd( const Parametros, Datos: OleVariant ): String;
var
   Archivo: TAsciiExport;
   eFormato: eFormatoASCII;
   sFileName: String;
   iTipoCedula: Integer;

begin
     Result := '';
     with cdsDataset do
     begin
          Active := False;
          Data := Datos;
     end;
     with TZetaParams.Create do
     begin
          try
             VarValues := Parametros;
             // sFileName := ParamByName( 'Archivo' ).AsString;
             // eFormato := eFormatoASCII( ParamByName( 'Formato' ).AsInteger );
             sFileName := ParamByName( 'CedulaPago' ).AsString;
             iTipoCedula := ParamByName( 'TipoCedula' ).AsInteger;

             RutaArchivoGenerado := sFileName;
             eFormato := faASCIIDel;
          finally
                 Free;
          end;
     end;
     Archivo := TAsciiExport.Create;    

     with Archivo do
     begin
          try
             if Abre( sFileName, True ) then
             begin

                  Clear;
                  Separador := '';
                  OEMConvert := True;
                  Delimitador := VACIO;

                  if iTipoCedula = ord (etacPorCredito) then
                  begin
                       AgregaRenglon('No_FONACOT,RFC,NOMBRE,No_CREDITO,RETENCION_MENSUAL,CLAVE_EMPLEADO,PLAZO,MESES,RETENCION_REAL,INCIDENCIA,FECHA_INI_BAJA,FECHA_FIN,REUBICADO');

                       AgregaColumna( 'NO_FONACOT', tgTexto, 30 );
                       AgregaColumna( 'RFC', tgTexto, 30 );
                       AgregaColumna( 'NOMBRE', tgTexto, 250 );
                       AgregaColumna( 'NO_CREDITO', tgTexto, 8 );
                       AgregaColumna( 'RETENCION_MENSUAL', tgFloat, 20 );
                       AgregaColumna( 'CLAVE_EMPLEADO', tgNumero, 20 );
                       AgregaColumna( 'PLAZO', tgNumero, 20 );
                       AgregaColumna( 'MESES', tgNumero, 20 );
                       AgregaColumna( 'RETENCION_REAL', tgFloat, 20 );
                       AgregaColumna( 'INCIDENCIA', tgTexto, 1 );
                       AgregaColumna( 'FECHA_INI_BAJA', tgTexto, 10 );
                       AgregaColumna( 'FECHA_FIN', tgTexto, 10 );
                       AgregaColumna( 'REUBICADO', tgTexto, 1 );
                       AlExportarColumnas := GenerarCedulaEscribeASCII;

                  end
                  else
                  begin
                       AgregaRenglon('NO_CT,ANIO_EMISION,MES_EMISION,NO_FONACOT,RFC,NO_SS,NOMBRE,CLAVE_EMPLEADO,CTD_CREDITOS,RETENCION_MENSUAL,TIPO,RETENCION_REAL,FECHA_BAJA,FECHA_INI,FECHA_FIN');

                       AgregaColumna( 'NO_CT', tgNumero, 30 );
                       AgregaColumna( 'ANIO_EMISION', tgNumero, 4 );
                       AgregaColumna( 'MES_EMISION', tgNumero, 2 );
                       AgregaColumna( 'NO_FONACOT', tgTexto, 30 );
                       AgregaColumna( 'RFC', tgTexto, 30 );
                       AgregaColumna( 'NO_SS', tgTexto, 11 );
                       AgregaColumna( 'NOMBRE', tgTexto, 250 );
                       AgregaColumna( 'CLAVE_EMPLEADO', tgNumero, 20 );
                       AgregaColumna( 'CTD_CREDITOS', tgNumero, 4 );
                       AgregaColumna( 'RETENCION_MENSUAL', tgFloat, 20 );
                       AgregaColumna( 'TIPO', tgTexto, 1 );
                       AgregaColumna( 'RETENCION_REAL', tgFloat, 20 );
                       AgregaColumna( 'FECHA_BAJA', tgTexto, 10 );
                       AgregaColumna( 'FECHA_INI', tgTexto, 10 );
                       AgregaColumna( 'FECHA_FIN', tgTexto, 10 );
                       AlExportarColumnas := GenerarCedulaTrabajadorEscribeASCII;

                  end;

                  // Result := AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( eFormato, cdsDataset ) ) + ' Movimientos Exportados' );
                  Result := 'Se generaron: '+ AgregaMensaje( Result, FormatFloat( K_FORMATO, Exporta( faASCIIDel,  cdsDataset ) ) + ' registros en la c�dula de pago Fonacot' + CR_LF + 'Archivo: ' + sFileName );

                  if HayError then
                     Result := AgregaMensaje( Result, Status );
             end;
          finally
                 Free;
          end;
     end;
     with cdsDataset do
     begin
          Active := False;
     end;
end;

procedure TdmProcesos.GenerarCedulaEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'NO_FONACOT' ).Datos := FieldByName( 'NO_FONACOT' ).AsString;
               ColumnByName( 'RFC' ).Datos := FieldByName( 'RFC' ).AsString;
               ColumnByName( 'NOMBRE' ).Datos := FieldByName( 'NOMBRE' ).AsString;
               ColumnByName( 'NO_CREDITO' ).Datos := FieldByName( 'NO_CREDITO' ).AsString;
               ColumnByName( 'RETENCION_MENSUAL' ).Datos := FloatToStr (FieldByName( 'RETENCION_MENSUAL' ).AsFloat);
               ColumnByName( 'CLAVE_EMPLEADO' ).Datos := IntToStr (FieldByName( 'CLAVE_EMPLEADO' ).AsInteger);
               ColumnByName( 'PLAZO' ).Datos := IntToStr (FieldByName( 'PLAZO' ).AsInteger);
               ColumnByName( 'MESES' ).Datos := IntToStr (FieldByName( 'MESES' ).AsInteger);
               ColumnByName( 'RETENCION_REAL' ).Datos := FloatToStr (FieldByName( 'RETENCION_REAL' ).AsFloat);
               ColumnByName( 'INCIDENCIA' ).Datos := FieldByName( 'INCIDENCIA' ).AsString;
               ColumnByName( 'FECHA_INI_BAJA' ).Datos := FieldByName( 'FECHA_INI_BAJA' ).AsString;
               ColumnByName( 'FECHA_FIN' ).Datos := FieldByName( 'FECHA_FIN' ).AsString;
               ColumnByName( 'REUBICADO' ).Datos := FieldByName( 'REUBICADO' ).AsString;
          end;
     end;
end;

procedure TdmProcesos.GenerarCedulaTrabajadorEscribeASCII( Sender: TAsciiExport; DataSet: TDataset );
begin
     with Sender do
     begin
          with Dataset do
          begin
               ColumnByName( 'NO_CT' ).Datos := IntToStr (FieldByName( 'NO_CT' ).AsInteger);
               ColumnByName( 'ANIO_EMISION' ).Datos := IntToStr (FieldByName( 'ANIO_EMISION' ).AsInteger);
               ColumnByName( 'MES_EMISION' ).Datos := IntToStr (FieldByName( 'MES_EMISION' ).AsInteger);
               ColumnByName( 'NO_FONACOT' ).Datos := FieldByName( 'NO_FONACOT' ).AsString;
               ColumnByName( 'RFC' ).Datos := FieldByName( 'RFC' ).AsString;
               ColumnByName( 'NO_SS' ).Datos := FieldByName( 'NO_SS' ).AsString;
               ColumnByName( 'NOMBRE' ).Datos := FieldByName( 'NOMBRE' ).AsString;
               ColumnByName( 'CLAVE_EMPLEADO' ).Datos := IntToStr (FieldByName( 'CLAVE_EMPLEADO' ).AsInteger);
               ColumnByName( 'CTD_CREDITOS' ).Datos := IntToStr (FieldByName( 'CTD_CREDITOS' ).AsInteger);
               ColumnByName( 'RETENCION_MENSUAL' ).Datos := FloatToStr (FieldByName( 'RETENCION_MENSUAL' ).AsFloat);
               ColumnByName( 'TIPO' ).Datos := FieldByName( 'TIPO' ).AsString;
               ColumnByName( 'RETENCION_REAL' ).Datos := FloatToStr (FieldByName( 'RETENCION_REAL' ).AsFloat);
               ColumnByName( 'FECHA_BAJA' ).Datos := FieldByName( 'FECHA_BAJA' ).AsString;
               ColumnByName( 'FECHA_INI' ).Datos := FieldByName( 'FECHA_INI' ).AsString;
               ColumnByName( 'FECHA_FIN' ).Datos := FieldByName( 'FECHA_FIN' ).AsString;
          end;
     end;
end;

procedure TdmProcesos.ImportarCedulasFonacotGetListaASCII(Parametros: TZetaParams; const lFillASCII: Boolean = TRUE );
begin

     if lFillASCII then
        FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;

          First;
          if not Eof then
             Delete;

          with dmCliente do
          begin
               cdsDataset.Data := ServerNomina.ImportarCedulasFonacotGetASCII (Empresa, Parametros.VarValues, GetLista (cdsASCII), FErrorCount);
          end;
          Active := False;
     end;
end;


function TdmProcesos.AplicarFiniquitosGlobales(Parametros: TZetaParams;const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadNomina( prEmpAplicacionFiniGlobal , GetLista( cdsDataset ), Parametros )
     else
          Result := CorreThreadNomina( prEmpAplicacionFiniGlobal, NULL, Parametros );

end;

procedure TdmProcesos.AplicarFiniquitosGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerNomina.AplicacionGlobalFiniquitosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.TransferenciasCosteo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prNoTransferenciasCosteo, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prNoTransferenciasCosteo, NULL, Parametros );
end;

procedure TdmProcesos.TransferenciasCosteoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.TransferenciasCosteoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.CalculaCosteo(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAsistencia( prNoCalculaCosteo, NULL, Parametros );
end;

function TdmProcesos.CancelaTransferenciaCosteo(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadAsistencia( prNoCancelaTransferencias, GetLista( cdsDataset ), Parametros )
     else
         Result := CorreThreadAsistencia( prNoCancelaTransferencias, NULL, Parametros );
end;

procedure TdmProcesos.CancelaTransferenciasCosteoGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerAsistencia.CancelaTransferenciasCosteoGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.PrevioISR(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadAnual( prNOPrevioISR, NULL, TRUE, Parametros );
end;

{ ******** Fin de Procesos de N�minas ******* }


{ *************** Procesos de Cafeteria ************* }
function TdmProcesos.CorreThreadCafeteria( const eProceso: Procesos; const Lista: OleVariant; Parametros: TZetaParams ): Boolean;
begin
     with TCafeteriaThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := Lista;
          CargaParametros( Parametros );
          Resume;
     end;
     Result := True;
end;

function TdmProcesos.ComidasGrupales(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
begin
     if lVerificacion then
        Result := CorreThreadCafeteria( prCAFEComidasGrupales, GetLista( cdsDataset ), Parametros )
     else
          Result := CorreThreadCafeteria( prCAFEComidasGrupales, NULL, Parametros );
end;

procedure TdmProcesos.ComidasGrupalesGetLista( Parametros: TZetaParams );
begin
     with cdsDataset do
     begin
          Data := ServerCafeteria.ComidasGrupalesGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.ComidasGrupalesListaASCII( Parametros: TZetaParams ): Boolean;
begin
     FillcdsASCII( cdsASCII, Parametros.ParamByName( 'Archivo' ).AsString );
     with cdsASCII do
     begin
          Active := True;
          Result := CorreThreadCafeteria( prCAFEComidasGrupales, GetLista( cdsASCII ), Parametros );
          Active := False;
     end;
end;

function TdmProcesos.CorregirFechasGlobalesCafe(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadCafeteria( prCAFECorregirFechas, NULL, Parametros );
end;

function TdmProcesos.FoliarCapacitaciones( Parametros: TZetaParams ): Boolean;
begin
     Result := CorreThreadRecursos( prRHFoliarCapacitaciones, NULL, Parametros );
end;

procedure TdmProcesos.FoliarCapacitacionesSTPSGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerRecursos.FoliarCapacitacionesSTPSGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;

function TdmProcesos.FoliarCapacitacionesSTPS(Parametros: TZetaParams; const lVerificacion: Boolean): Boolean;
var
   proceso : ZetaCommonLists.Procesos;
begin
   if  Parametros.ParamByName ('ReinicioFolio').AsBoolean then
       proceso := prRHReiniciarCapacitacionesSTPS
   else
       proceso := prRHFoliarCapacitaciones;
   if lVerificacion then
      Result := CorreThreadRecursos( proceso, GetLista( cdsDataset ), Parametros )
   else
       Result := CorreThreadRecursos( proceso, NULL, Parametros );
end;

// WizEmpValidarDC4
function TdmProcesos.RevisarDatosSTPS(Parametros: TZetaParams): Boolean;
begin
   Result := CorreThreadRecursos( prRHRevisarDatosSTPS, NULL, Parametros );
end;

function TdmProcesos.RevisarDatosSTPSEnd (const Parametros, Datos: OleVariant): String;
var
   oCursor: TCursor;
   sArchivo: String;
begin
     Result := '';
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TZetaParams.Create do
        begin
             try
                VarValues := Parametros;
                sArchivo := ParamByName( 'Archivo' ).AsString;
             finally
                    Free;
             end;
        end;
        with TStringList.Create do
        begin
             try
                Text := Datos;
                SaveToFile( sArchivo );
             finally
                    Free;
             end;
        end;
        if ZConfirm( 'Revisi�n de Datos para STPS', Format ('El archivo ''%s''', [ExtractFileName (sArchivo)]) + ' fu� Creado con �xito.' +
           CR_LF + '�Desea verlo?', 0, mbYes ) then
           // ZetaClientTools.ExecuteFile( 'EXCEL.EXE',  sArchivo, ExtractFileDir (sArchivo), SW_SHOWNORMAL );
           ZetaClientTools.ExecuteFile( sArchivo, '', '', SW_SHOWDEFAULT );
     finally
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

{ ******** Fin de Procesos de Cafeteria ******* }

function TdmProcesos.EjecutaMotorPatch(Parametros: TZetaParams; Ventana: HWND; SetResultado: TSetResultadoEspecial): Boolean;
begin
  Result := CorreThreadSistemaSilent(prPatchCambioVersion, null, Parametros, Ventana, SetResultado);
end;

{$IFDEF TIMBRADO}

function TdmProcesos.TimbrarNomina(Parametros: TZetaParams): Boolean;
begin
 Result := CorreThreadNomina( prNoTimbrarNomina , GetLista( cdsDataset ), Parametros )
end;
procedure TdmProcesos.TimbrarNominaGetLista(Parametros: TZetaParams);
begin
      with cdsDataSetTimbrados do
     begin
          Data := ServerNomina.TimbrarNominasGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;
{$ENDIF}

procedure TdmProcesos.VerificarEmpleadosCedulaFonacot;
begin
     cdsEmpleadosCedula.Data := ServerNomina.VerificarEmpleadosCedulaFonacot( dmCliente.Empresa, GetLista( cdsDataSet ) );
end;

procedure TdmProcesos.AplicarFiltroRFC(lEnabled : Boolean);
begin
     with cdsEmpleadosCedula do
     begin
          DisableControls;
          Filtered := False;
          Filter := 'Mostrar = ' + BoolToStr(lEnabled);

          if lEnabled then
             Filtered := True;
          IndexFieldNames := 'NombreAntes';
          EnableControls;
     end;
end;

function TdmProcesos.GetConteoCedula(Anio, Mes, RazonSocial : String) : Integer;
begin
      Result := ServerNomina.GetConteoCedula( dmCliente.Empresa, Anio, Mes, RazonSocial );
end;


//WizNomFonacotImportarCedula
function TdmProcesos.EliminarCedulaFonacot(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadNomina (prNOFonacotEliminarCedula, Null, Parametros);
end;

function TdmProcesos.GetConteoCedulaAfectada(Anio, Mes, RazonSocial : String) : Integer;
begin
      Result := ServerNomina.GetConteoCedulaAfectada( dmCliente.Empresa, Anio, Mes, RazonSocial );
end;

function TdmProcesos.GetNominaAfectada(Anio, Mes, RazonSocial : String) : Integer;
begin
      Result := ServerNomina.GetEstatusNomina( dmCliente.Empresa, Anio, Mes, RazonSocial );
end;

function TdmProcesos.GetStatusTimbrado(Parametros: TZetaParams ): Olevariant;
begin
     Result := ServerLogin.GetStatusTimbrado( dmCliente.Empresa, Parametros.VarValues );
end;


{
     if lVerificacion then
        Result := CorreThreadNomina( prEmpAplicacionFiniGlobal , GetLista( cdsDataset ), Parametros )
     else
          Result := CorreThreadNomina( prEmpAplicacionFiniGlobal, NULL, Parametros );
     
end;

procedure TdmProcesos.AplicarFiniquitosGlobalGetLista(Parametros: TZetaParams);
begin
     with cdsDataset do
     begin
          Data := ServerNomina.AplicacionGlobalFiniquitosGetLista( dmCliente.Empresa, Parametros.VarValues );
     end;
end;}
end.
