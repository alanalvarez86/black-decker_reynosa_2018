object dmIMSS: TdmIMSS
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsIMSSDatosBim: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsIMSSDatosBimAlAdquirirDatos
    Left = 56
    Top = 16
  end
  object cdsIMSSDatosMen: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsIMSSDatosMenAlAdquirirDatos
    Left = 168
    Top = 16
  end
  object cdsMovBim: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'LM_FECHA;LM_CLAVE'
    Params = <>
    AlAdquirirDatos = cdsMovBimAlAdquirirDatos
    AlCrearCampos = cdsMovBimAlCrearCampos
    Left = 56
    Top = 72
  end
  object cdsMovMen: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'LM_FECHA;LM_CLAVE'
    Params = <>
    AlAdquirirDatos = cdsMovMenAlAdquirirDatos
    AlCrearCampos = cdsMovMenAlCrearCampos
    Left = 168
    Top = 72
  end
  object cdsIMSSDatosTot: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnNewRecord = cdsIMSSDatosTotNewRecord
    OnReconcileError = ImssReconcileError
    AlAdquirirDatos = cdsIMSSDatosTotAlAdquirirDatos
    AlEnviarDatos = cdsIMSSDatosTotAlEnviarDatos
    AlAgregar = cdsIMSSDatosTotAlAgregar
    AlBorrar = cdsIMSSDatosTotAlBorrar
    AlModificar = cdsIMSSDatosAlModificar
    Left = 54
    Top = 248
  end
  object cdsIMSSHisMen: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsIMSSHisMenAlAdquirirDatos
    Left = 166
    Top = 128
  end
  object cdsIMSSHisBim: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsIMSSHisBimAlAdquirirDatos
    Left = 56
    Top = 128
  end
  object cdsMovHisBim: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsMovHisBimAlAdquirirDatos
    AlCrearCampos = cdsMovHisBimAlCrearCampos
    Left = 56
    Top = 184
  end
  object cdsMovHisMen: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsMovHisMenAlAdquirirDatos
    AlCrearCampos = cdsMovHisMenAlCrearCampos
    Left = 168
    Top = 184
  end
  object cdsHisConciliaInfo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsHisConciliaInfoCalcFields
    AlAdquirirDatos = cdsHisConciliaInfoAlAdquirirDatos
    AlCrearCampos = cdsHisConciliaInfoAlCrearCampos
    Left = 176
    Top = 240
  end
  object cdsHisTotalBim: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsHisTotalBimAlAdquirirDatos
    AlCrearCampos = cdsHisTotalBimAlCrearCampos
    Left = 176
    Top = 296
  end
  object cdsMovimientosIDSE: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 280
    Top = 16
    object cdsMovimientosIDSETIPO: TSmallintField
      FieldName = 'TIPO'
    end
    object cdsMovimientosIDSENSS: TStringField
      FieldName = 'NSS'
      Size = 40
    end
    object cdsMovimientosIDSEFECHA: TDateField
      FieldName = 'FECHA'
    end
    object cdsMovimientosIDSEPAGINA: TIntegerField
      FieldName = 'PAGINA'
    end
    object cdsMovimientosIDSEVALIDO: TBooleanField
      FieldName = 'VALIDO'
    end
    object cdsMovimientosIDSETEXTO: TStringField
      FieldName = 'TEXTO'
      Size = 100
    end
  end
end
