unit FEditCatAnfitrion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaSmartLists, ZBaseEdicion, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar,
  cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons,
  ZetaKeyLookup_DevEx, variants;

type
  TEditCatAnfitrion = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    lblNombre: TLabel;
    AN_NOMBRES: TDBEdit;
    lblApePat: TLabel;
    AN_APE_PAT: TDBEdit;
    lblApeMat: TLabel;
    AN_APE_MAT: TDBEdit;
    bOk: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bOkClick(Sender: TObject);
  private
    { Private declarations }
    bTimerCitasEnabled:boolean;
    procedure CargaNombre;
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatAnfitrion: TEditCatAnfitrion;

implementation

uses dVisitantes,
     ZAccesosTress,
     ZetaCommonLists,
     ZetaCommonClasses,
     FRegistroEntradaVisi,
     FTressShell,
     ZetaDialogo;

{$R *.DFM}

procedure TEditCatAnfitrion.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  TressShell.TimerCitas.Enabled := bTimerCitasEnabled;
end;

procedure TEditCatAnfitrion.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_ANFITRION;
    // FirstControl := AN_NUMERO;
    // AN_DEPTO.LookupDataset := dmVisitantes.cdsDepto;
     //TipoValorActivo1 := stExpediente;
     HelpContext:= H_VISMGR_EDIT_ANFITRIONES;
end;

procedure TEditCatAnfitrion.FormShow(Sender: TObject);
begin
     inherited;
     AN_NOMBRES.SetFocus;
     dxBarButton_AgregarBtnClick(Self);
     self.Height := 180;
     CargaNombre;
     bTimerCitasEnabled := TressShell.TimerCitas.Enabled;
     TressShell.TimerCitas.Enabled := false;
end;

procedure TEditCatAnfitrion.OK_DevExClick(Sender: TObject);
var
  sAnNombres:string;
  sAnApePat:string;

  function getDatosAnfi:string;
  begin

       with dmVisitantes.cdsAnfitrion do
       begin
            First;
            while not Eof do
            begin
                 if FieldByName('AN_NOMBRES').AsString = AN_NOMBRES.Text then
                 if FieldByName('AN_APE_PAT').AsString = AN_APE_PAT.Text then
                 if FieldByName('AN_APE_MAT').AsString = AN_APE_MAT.Text then
                    Result :=  FieldByName('AN_NUMERO').AsString;


                 next;
            end;


       end;
  end;
begin
     inherited;

     sAnNombres:= AN_NOMBRES.Text;
     sAnApePat := AN_APE_PAT.Text;
     with dmVisitantes.cdsAnfitrion do
     begin
          Enviar;
          refrescar;
          begin
              with FRegistroEntradaVisit do
              begin
                   VISITAA.Llave := getDatosAnfi;
                   VISITAA.SetFocus;
                   VISITAAExit(Self);
                   LI_ANFITR.Text := AN_NOMBRES.Text +' '+AN_APE_PAT.Text +' '+AN_APE_MAT.Text;
                   bEsNuevoAnfitrion := true;
              end;
          end;
     end;
     close;
end;


procedure TEditCatAnfitrion.Split(Delimiter: Char; Str: string;
  ListOfStrings: TStrings);
begin
      ListOfStrings.Clear;
      ListOfStrings.Delimiter       := Delimiter;
      ListOfStrings.StrictDelimiter := True; // Requires D2006 or newer.
      ListOfStrings.DelimitedText   := Str;
end;

procedure TEditCatAnfitrion.bOkClick(Sender: TObject);
begin
     inherited;
     inherited;
     if (AN_NOMBRES.Text = VACIO) then
     begin
          ZError('Error en visitantes', '! Se econtro un error !'+#13+'El nombre no puede quedar vacio.', 0);
          AN_NOMBRES.SetFocus;
     end
     else if (AN_APE_PAT.Text = VACIO) then
     begin
          ZError('Error en visitantes','! Se econtro un error !'+#13+'El apellido paterno no puede quedar vacio.', 0);
          AN_APE_PAT.SetFocus;
     end
     else
         OK_DevExClick(Self);
end;

procedure TEditCatAnfitrion.Cancelar_DevExClick(Sender: TObject);
begin
     close;
     inherited;
end;

procedure TEditCatAnfitrion.CargaNombre;
var
   OutPutList: TStringList;
begin
     with DataSource.DataSet do
     begin
          OutPutList := TStringList.Create;
          try
             Split(' ', FRegistroEntradaVisit.LI_ANFITR.Text, OutPutList) ;
             if OutPutList.Count > 0 then
                FieldByName('AN_NOMBRES').AsString:= OutPutList[0];
             if OutPutList.Count > 1 then
                FieldByName('AN_APE_PAT').AsString := OutPutList[1];
             if OutPutList.Count > 2 then
                FieldByName('AN_APE_MAT').AsString := OutPutList[2];
          finally
                 OutPutList.Free;
          end;
     end;
end;

procedure TEditCatAnfitrion.Connect;
begin
     with dmVisitantes do
     begin
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsAnfitrion;
     end;
end;

end.
