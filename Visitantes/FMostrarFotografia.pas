unit FMostrarFotografia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, DB,
  //:todo  TDMULTIP,
  ExtCtrls,ZetaClientDataSet, imageenview, ieview, imageen, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, cxButtons;

type
  TMostrarFotografia = class(TForm)
    dsFotografia: TDataSource;
//:todo     VI_FOTO: TPDBMultiImage;
    Panel: TPanel;
    BtnSalir: TcxButton;
    Foto: TImageEn;

    procedure FormShow(Sender: TObject);
    procedure BtnSalirClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FotoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
  public
    { Public declarations }
    property Dataset: TZetaClientDataset read FDataset write FDataset;
  protected
    { Protected declarations }
  end;

var
  MostrarFotografia: TMostrarFotografia;

  procedure MostrarFoto( const iEmpleado: Integer; ZetaDataset: TZetaClientDataSet );

implementation
uses
    DVisitantes,
    FToolsImageEn;
{$R *.dfm}

procedure MostrarFoto( const iEmpleado: Integer; ZetaDataset: TZetaClientDataSet );
begin
     if MostrarFotografia = nil then
        MostrarFotografia := TMostrarFotografia.Create(Application);
     try
        with MostrarFotografia do
        begin
             Dataset := ZetaDataset;
             ShowModal;
        end;
     finally
            FreeAndNil( MostrarFotografia );
     end;
end;


procedure TMostrarFotografia.BtnSalirClick(Sender: TObject);
begin
   Close;
end;

procedure TMostrarFotografia.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
    close;
end;

procedure TMostrarFotografia.FormShow(Sender: TObject);
begin
   FToolsImageEn.AsignaBlobAImagen( FOTO, dmVisitantes.cdsSQL, 'VI_FOTO' );
   dsFotografia.DataSet := FDataset;
   Foto.SetFocus;
end;

procedure TMostrarFotografia.FotoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if Key = 27 then
    close;
end;

end.
