unit FRegistroEntradaVisi;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, TressMorado2013,
  Vcl.Menus, cxButtons, Data.DB, Vcl.ImgList, Vcl.StdCtrls, cxTextEdit, cxMemo,
  cxDBEdit, Vcl.DBCtrls, imageenview, ieview, imageen, ZetaFecha, ZetaKeyCombo,
  Vcl.Mask, dxGDIPlusClasses, Vcl.ExtCtrls, ZetaKeyLookup_DevEx, ZVisitantesTools,
  cxMaskEdit, cxDropDownEdit, cxGroupBox, ZetaClientDataSet;



type
  TFRegistroEntradaVisit = class(TForm)
    Panel2: TPanel;
    PanelBotones: TPanel;
    cxImageList24_PanelBotones: TcxImageList;
    dsBuscaVisita: TDataSource;
    dsBuscaVisitandoA: TDataSource;
    dsTipoVisitante: TDataSource;
    dsVehiculo: TDataSource;
    dsAsunto: TDataSource;
    DataSource: TDataSource;
    dsBuscaCompania: TDataSource;
    dsTipoID: TDataSource;
    dsVisita: TDataSource;
    Cancelar_DevEx: TcxButton;
    dsSQL: TDataSource;
    bOk: TcxButton;
    btnOtro: TcxButton;
    cxImageList32: TcxImageList;
    pnlObserva: TPanel;
    icoObserva: TImage;
    lbIcoObserva: TLabel;
    Image5: TImage;
    lbObserva1: TLabel;
    lbObserva2: TLabel;
    LI_OBSERVA: TcxMemo;
    lbTexto1: TLabel;
    lbTexto2: TLabel;
    lbTexto3: TLabel;
    LI_TEXTO1: TEdit;
    LI_TEXTO2: TEdit;
    LI_TEXTO3: TEdit;
    cxGroupBox1: TPanel;
    pnlIdentifica: TPanel;
    icoIdenti: TImage;
    lbIcoIdenti: TLabel;
    Image2: TImage;
    lbIdentificacion: TLabel;
    cbTipoId: TcxComboBox;
    lbId: TLabel;
    LI_ID: TEdit;
    liGafete: TLabel;
    LI_GAFETE: TEdit;
    pnlAuto: TPanel;
    icoAuto: TImage;
    lbIcoAuto: TLabel;
    Image4: TImage;
    liTipoVehiculo: TLabel;
    cbTipoAuto: TcxComboBox;
    lbCarPla: TLabel;
    LI_CAR_PLA: TEdit;
    lbCarDesc: TLabel;
    LI_CAR_DES: TEdit;
    lbCarEst: TLabel;
    LI_CAR_EST: TEdit;
    pnlVisitaA: TPanel;
    Image1: TImage;
    Label3: TLabel;
    Image3: TImage;
    Label7: TLabel;
    VISITAA: TZetaKeyLookup_DevEx;
    bAgregaVisitante: TcxButton;
    Label8: TLabel;
    cmbDEPTO: TcxComboBox;
    pnlTop: TPanel;
    Foto: TImageEn;
    Label2: TLabel;
    cmbDriverFoto: TComboBox;
    btnTomaFoto: TcxButton;
    Label24: TLabel;
    lblApePat: TLabel;
    lblApeMat: TLabel;
    lblSexo: TLabel;
    Label1: TLabel;
    lblNacion: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    VI_NOMBRES: TEdit;
    VI_APE_PAT: TEdit;
    VI_APE_MAT: TEdit;
    cbSexo: TcxComboBox;
    lblFecNac: TLabel;
    cbTipoVisitante: TcxComboBox;
    VI_NACIONALIDAD: TEdit;
    VI_MIGRA: TEdit;
    dblCompania: TZetaKeyLookup_DevEx;
    bAgregaEmpre: TcxButton;
    pnlAsunto: TPanel;
    cmbTipoAsunto: TcxComboBox;
    Label9: TLabel;
    LI_EMPRESA: TEdit;
    bBuscaEmpre: TcxButton;
    LI_ANFITR: TEdit;
    bBuscaAnfitrion: TcxButton;
    VI_FEC_NAC: TZetaDBFecha;
    procedure FormShow(Sender: TObject);
    procedure btnTomaFotoClick(Sender: TObject);
    procedure cmbDriverFotoChange(Sender: TObject);
    procedure FotoDShowNewFrame(Sender: TObject);
    procedure bAgregaEmpreClick(Sender: TObject);
    procedure bAgregaVisitanteClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure VISITAAExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bOkClick(Sender: TObject);
    procedure btnOtroClick(Sender: TObject);
    procedure WMKeyDown(var Message: TWMKeyDown) ; message CM_DIALOGKEY;
    procedure FormCreate(Sender: TObject);
    procedure bBuscaEmpreClick(Sender: TObject);
    procedure bBuscaAnfitrionClick(Sender: TObject);
    procedure LI_EMPRESAExit(Sender: TObject);
    procedure LI_ANFITRExit(Sender: TObject);
    procedure LI_ANFITREnter(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure VI_FEC_NACKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LI_TEXTO3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }

    FEsRegistroGrupo:boolean;
    sNombreVisiNuevo:string;
    sApepatVisiNuevo:string;
    FOnSearch: TSearchEvent;
    FDone:boolean;
    FValidoAnfitrion:boolean;
    FEsNuevoAnfitrion:boolean;
    FEstadoAlerta:boolean;


    iVisitNumero:integer;
    arTiposVisitantes: array of string;
    arTiposAsuntos: array of string;
    arDeptos: array of string;
    arTipoId: array of string;
    arTipoAuto: array of string;
    FVisitaAgregada: Boolean;
    FReejecutar: Boolean;
    FTomoFoto: Boolean;
    FEsRegistroCita:boolean;
    FPermiteManejorCamara:Boolean;
    function ExisteVisitante: Boolean;
    function GetCodigoAsunto( i: integer ): string;
    procedure AgregarVisitante;
    procedure connect;
    procedure AntesDeEnviar;
    procedure VerificaCamposNecesarios;
    procedure SetIVisiNumero(const Value: integer);
    procedure LlenaDatosVisitante;
    procedure LlenaComboTipoVisitante;
    procedure LlenaComboAsunto;
    procedure LlenaComboTipoId;
    procedure LlenaComboDepto;
    procedure LlenaComboTipoAuto;
    function GetTipoVisitante(sTipoVisi:string):integer;
    procedure SetControls(lHabilita: Boolean);
    procedure MuestraImagenFoto;
    procedure ShowVideoFormats;
    procedure ConectaCamar;
    procedure ConectaCamaraTomaFoto;
    procedure LimpiaControlesEntrada(EsRegistroGrupo:boolean);
    procedure LlenaCombos;
    procedure IniciaControlesVisitante;
    procedure IniciaControlesNuevoVisitante;
    procedure IniciaCaptura;
    function ValidaCamposEnt:boolean;
    procedure BorraPanels;
    procedure LlenaDatosGrupo;

    procedure _AlEjecutar(Sender: TObject; var lOk: Boolean);
    function EjecutarWizard: Boolean;
    procedure TerminarWizard;
    procedure ActivaCamara;
    procedure LlenaCamposGrupo;
    function Search( const sFilter: String; var sKey, sDescription: String; iOpc:integer ): Boolean;
    procedure BuscaEmpresa;
    procedure BuscaAnfitrion;
    function ObtenNombreAnfi(iAnfi:integer):string;
    procedure CMDialogKey( Var msg: TCMDialogKey );
    procedure LlenaComboAsuntoCodigo(sCodigo:string);
  public
    { Public declarations }
    property iVisiNumero: integer read iVisitNumero write SetIVisiNumero;
    property Reejecutar: Boolean read FReejecutar write FReejecutar;
    property PermiteManejorCamara: Boolean read FPermiteManejorCamara write FPermiteManejorCamara;
    property TomoFoto: Boolean read FTomoFoto  write FTomoFoto;
    property EsRegistroCita: Boolean read FEsRegistroCita  write FEsRegistroCita;
    property sPNombreVisiNuevo: string read sNombreVisiNuevo write sNombreVisiNuevo;
    property sPApePatVisiNuevo: string read sApePatVisiNuevo write sApePatVisiNuevo;
    property bValidoAnitrion: Boolean read FValidoAnfitrion write FValidoAnfitrion;
    property bEsNuevoAnfitrion: Boolean read FEsNuevoAnfitrion write FEsNuevoAnfitrion;
    property FPEstadoAlerta: Boolean read FEstadoAlerta  write FEstadoAlerta;
    procedure NextVisitaAFocus;


  end;
var
  FRegistroEntradaVisit: TFRegistroEntradaVisit;

implementation
uses
    DVisitantes,
    ZetaCommonClasses,
    ZetaDialogo,
    DGlobal,
    ZGlobalTress,
    FToolsImageEn,
    ZAccesosMGR,
    ZAccesosTress,
    hyiedefs,
    FEditCatCompanys,
    FEditCatAnfitrion,
    ZetaCommonLists,
    ZetaCommonTools,
    FBusquedaVisit,
    FTressShell,
    ZVisitBusqueda_DevEx,
    FDatosVisitGrupo;
{$R *.dfm}


procedure TFRegistroEntradaVisit.ActivaCamara;
var
  arCamaras:array of string;
  iCont:integer;
  iContFuera:integer;
begin
     // Fill video source combobox
     cmbDriverFoto.Items.Assign(FOTO.IO.DShowParams.VideoInputs);
     SetLength(arCamaras, cmbDriverFoto.Items.Count);
     for iContFuera := 0 to 1 do
     begin
          iCont:=0;
          if (iContFuera = 1) and (iCont = 0) then
          begin
               cmbDriverFoto.Clear;
               cmbDriverFoto.Items.Add('');
          end;
          while iCont < length(arCamaras) do
          begin
               if iContFuera = 0 then
                  arCamaras[iCont] := cmbDriverFoto.Items[iCont]
               else cmbDriverFoto.Items.Add(arCamaras[iCont]);
               Inc(iCont);
          end;
    end;
    // Select first item
    if (cmbDriverFoto.Items.Count > 0) and (iVisitNumero <= 0 ) then
    begin
         cmbDriverFoto.ItemIndex:=1;
         cmbDriverFotoChange(Self);
          btnTomaFoto.Visible := true;
    end;
    if (cmbDriverFoto.Items.Count <= 2) or (iVisitNumero > 0) then
    begin
         cmbDriverFoto.Visible := false;
         Label2.Visible := false;
    end
    else
    begin
         cmbDriverFoto.Visible := true;
         Label2.Visible := true;
    end;
end;

procedure TFRegistroEntradaVisit.AgregarVisitante;
const
     K_VACIO = '';
begin
     if NOT ExisteVisitante then
     begin
          with dmVisitantes do
     begin
          with cdsVisitante do
     begin
          FieldByName('VI_NOMBRES').AsString := VI_NOMBRES.Text;                                //Se llenan datos si es nuevo visitante
          FieldByName('VI_APE_PAT').AsString := VI_APE_PAT.Text;
          FieldByName('VI_APE_MAT').AsString := VI_APE_MAT.Text;

               {***(@am) BUG #15712: Se comenta la logica anterior, pues tiene defectos de implementacion, los cambios realizados fueron:
               -Se remueve la validacion del global del asunto, pues no tiene nada que ver con el tipo de visitante.
               -Se remueve la resta el de una posicion al ItemIndex, pues si no contaramos con ningun tipo de visitante marcaria un error.
               -Se evalue el caso de no tener ningun tipo de visitante, en la BD en este campo no existe una llave foranea hacia la tabla de tipos de visiante. La solucion mas facil (sin incluir patch) es capturar un valor vacio en este
                 campo al darse esta situacion.***}

          //CODIGO ANTERIOR
          {if (cbTipoVisitante.ItemIndex > 0) and (not Global.GetGlobalBooleano(K_GLOBAL_REQ_ASUNTO) ) then
              FieldByName('VI_TIPO').AsString := arTiposVisitantes[cbTipoVisitante.ItemIndex-1];
          else
              FieldByName('VI_TIPO').AsString := arTiposVisitantes[cbTipoVisitante.ItemIndex];}

          //NUEVA IMPLEMENTACION
          if (cbTipoVisitante.ItemIndex >= 0 ) then
             FieldByName('VI_TIPO').AsString := arTiposVisitantes[cbTipoVisitante.ItemIndex]
          else
              FieldByName('VI_TIPO').AsString := K_VACIO;


          if cmbTipoAsunto.ItemIndex >= 0 then
          begin
               if cmbTipoAsunto.ItemIndex > 0 then
                  FieldByName('VI_ASUNTO').AsString := GetCodigoAsunto( cmbTipoAsunto.ItemIndex-1 );
          end;

          if cbSexo.ItemIndex = 0 then
             FieldByName('VI_SEXO').AsString := 'M'
          else
              FieldByName('VI_SEXO').AsString := 'F';
          if (VISITAA.Llave <> VACIO) then
             FieldByName('AN_NUMERO').AsInteger := strToInt(VISITAA.Llave);
          FieldByName('VI_NACION').AsString := VI_NACIONALIDAD.Text;
          FieldByName('VI_MIGRA').AsString := VI_MIGRA.Text;
          FieldByName('EV_NUMERO').AsInteger := dblCompania.Valor;
          if iVisitNumero <=0 then

          if TomoFoto then
             FToolsImageEn.AsignaImagenABlob( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' )
          else Foto.clear;

          Enviar;
          DisableControls; //acl {Se cambio a despues de enviar para que guarde la foto. ACL}

          try
             Last;
             iVisitNumero := FieldByName('VI_NUMERO').AsInteger;
             FVisitaAgregada := TRUE;
             Refrescar;
          finally
                 EnableControls;
          end;
     end;
     end;
     end;
end;

procedure TFRegistroEntradaVisit.AntesDeEnviar;

 function GetDato( const sCodigo: string; const lNoExiste: Boolean ): string;
 begin
      if lNoExiste then
         Result := VACIO
      else
          Result := sCodigo;
 end;
 var
    DataSetAnfitrion: TDataset;
    sTipoId:string;
    sTipoAuto:string;
begin
  with dmVisitantes do
  begin
       if not Global.GetGlobalBooleano(K_GLOBAL_REQ_IDENTIFICA) then
       begin
            if (cbTipoId.ItemIndex >= 1) then
               sTipoId := arTipoId[cbTipoId.ItemIndex-1];
       end
       else sTipoId := arTipoId[cbTipoId.ItemIndex];

       if not Global.GetGlobalBooleano(K_GLOBAL_REQ_VEHICULO) then
       begin
            if (cbTipoAuto.ItemIndex >= 1) then
               sTipoAuto := arTipoAuto[cbTipoAuto.ItemIndex-1];
       end
       else sTipoAuto := arTipoAuto[cbTipoAuto.ItemIndex];

       if iVisitNumero <=0 then
          AgregarVisitante;

       with cdsLibros do
       begin
            if (cbTipoId.Text <> VACIO) then
               FieldByName('LI_TIPO_ID').AsString := sTipoId;

            if (sTipoAuto <> VACIO) then
               FieldByName('LI_CAR_TIP').AsString := sTipoAuto;

            if Global.GetGlobalBooleano(K_GLOBAL_REQ_ASUNTO) then
            begin
                 if cmbTipoAsunto.ItemIndex >= 0 then
                    FieldByName('LI_ASUNTO').AsString := GetCodigoAsunto( cmbTipoAsunto.ItemIndex );
            end
            else
            begin
                 if cmbTipoAsunto.ItemIndex > 0 then
                    FieldByName('LI_ASUNTO').AsString := GetCodigoAsunto( cmbTipoAsunto.ItemIndex-1);
            end;

            FieldByName('LI_NOMBRE').AsString := VI_NOMBRES.Text +' '+VI_APE_PAT.Text+' '+VI_APE_MAT.Text;
            FieldByName('EV_NUMERO').AsInteger := dblCompania.Valor;
            FieldByName('LI_EMPRESA').AsString := dblCompania.Descripcion;
            FieldByName('VI_NUMERO').AsInteger  := iVisitNumero;
            FieldByName('LI_CAR_PLA').AsString  := LI_CAR_PLA.Text;
            FieldByName('LI_CAR_DES').AsString  := LI_CAR_DES.Text;
            FieldByName('LI_CAR_EST').AsString  := LI_CAR_EST.Text;

            FieldByName('LI_TEXTO1').AsString  := LI_TEXTO1.Text;
            FieldByName('LI_TEXTO2').AsString  := LI_TEXTO2.Text;
            FieldByName('LI_TEXTO3').AsString  := LI_TEXTO3.Text;
            FieldByName('LI_OBSERVA').AsString  := LI_OBSERVA.Text;

            FieldByName('LI_ID').AsString  := LI_ID.Text;
            FieldByName('LI_GAFETE').AsString  := LI_GAFETE.Text;

            DataSetAnfitrion := cdsAnfitrion;
            if (DataSetAnfitrion <> NIL ) then
            begin
                 FieldByName('LI_ANFITR').AsString :=  ObtenNombreAnfi(DataSetAnfitrion.FieldByName('AN_NUMERO').AsInteger);
                 FieldByName('LI_CDEPTO').AsString :=  DataSetAnfitrion.FieldByName('AN_DEPTO').AsString;
                 if cmbDEPTO.Text <> VACIO then
                    FieldByName('LI_DEPTO').AsString := DataSetAnfitrion.FieldByName('TB_ELEMENT').AsString;
                 if (VISITAA.Llave <> VACIO)  then
                    FieldByName('AN_NUMERO').AsInteger := strToInt(VISITAA.Llave);
            end;
       end;
  end;
end;



procedure TFRegistroEntradaVisit.connect;
begin
  with dmVisitantes do
  begin
       cdsTipoAsunto.Refrescar;
       cdsTipoCarro.Refrescar;
       cdsTipoID.Refrescar;
       cdsTipoVisita.Refrescar;
       cdsEmpVisitante.Conectar;//Se refresca en la busqueda de compa�ia
       //OPCSQL := 1;
       //cdsSQL.Conectar;
       //cdsCompaniaLookup.Conectar;
       //cdsCaseta.Conectar;
       cdsDepto.Conectar;
       DataSource.DataSet:= cdsLibros;
       dsTipoVisitante.DataSet := cdsTipoVisita;
       dsBuscaVisitandoA.DataSet := cdsAnfitrionLookup;
       dsBuscaVisita.DataSet := cdsVisitanteLookup;
       dsBuscaCompania.DataSet := cdsCompaniaLookup;
       dsAsunto.Dataset := cdsTipoAsunto;
       dsTipoID.Dataset := cdsTipoID;
       dsVehiculo.DataSet := cdsTipoCarro;
       dsVisita.Dataset := cdsVisitante;
       cdsVisitante.Refrescar;
       dsSQL.DataSet := cdsSQL;
  end;
  //:todo ChecaGlobales;
  //:todo VI_SEXO.ItemIndex :=0;
end;

procedure TFRegistroEntradaVisit.bAgregaEmpreClick(Sender: TObject);
begin
     if EditCatCompanys = nil then
        EditCatCompanys := TEditCatCompanys.Create(self);
     EditCatCompanys.ShowModal;
     dmVisitantes.cdsCompaniaLookup.Refrescar;
end;

procedure TFRegistroEntradaVisit.bAgregaVisitanteClick(Sender: TObject);
begin
     if EditCatAnfitrion = nil then
        EditCatAnfitrion := TEditCatAnfitrion.Create(self);
     EditCatAnfitrion.ShowModal;
     dmVisitantes.cdsAnfitrion.Refrescar;
end;

procedure TFRegistroEntradaVisit.bOkClick(Sender: TObject);
var
   lOk:boolean;

   procedure LlenaDatosVisitanteGrupo;
   begin
        with FDatosVisitanteGrupo do begin
             if length(sPNumero) > 0 then
                sPNumero := inttostr(strtoint(sPNumero) + 1)
             else sPNumero := '1';
             sPVisitante := VI_NOMBRES.Text +' '+VI_APE_PAT.Text+' '+VI_APE_MAT.Text;
             sPEmpresa := LI_EMPRESA.Text;
             sPAnfitrion := LI_ANFITR.Text;
             FRegistroEntradaVisit.Close;
        end;
   end;
begin
     lOk:=true;
     if ValidaCamposEnt then
     begin
          self.Visible := false;
          _AlEjecutar(Self, lOk); //Graba al visiante
          ModalResult := mrOk;
          with TressShell do                     //:todo   registro en grupos
          begin
               if (TressShell.bEsRegistroGrupo) or (TressShell.FPPrimerRegGrupo) then
               begin
                    if FDatosVisitanteGrupo = nil then
                       FDatosVisitanteGrupo := TFDatosVisitanteGrupo.Create(Self);
                    LlenaDatosVisitanteGrupo; //Pasa informacion del ultimo visitante para el registro del siguiente.

                    FDatosVisitanteGrupo.ShowModal;
                    if FDatosVisitanteGrupo.ModalResult = mrOk then
                    begin
                        //_Registrar_EntradaExecute(Self); //Esto fue una mala practica, ahora el ciclo de formas se ejecuta desde el shell.
						//Si se presiona "Otro visitante", se repite el ciclo
                         bEsRegistroGrupo := True;
                         FBusquedaVisitante.bCerrarBusqueda := true;
                    end
                    else
                    begin
                         FPPrimerRegGrupo := False; 
						 bEsRegistroGrupo := False;
                         LimpiaDatosGrupo; //Limpia los datos del grupo, porque ya se capturo el ultimo registro
                    end;
               end;
          end;
     end
     else
     begin
           ModalResult := mrNone;
     end;
end;

procedure TFRegistroEntradaVisit.BorraPanels;
begin
     //pnlTop.Visible := false;
     //pnlVisitaA.Visible := false;
     pnlAsunto.Visible := false;
     cxGroupBox1.Visible := false;
     pnlObserva.Visible := false;
     PanelBotones.Visible := false;
end;

procedure TFRegistroEntradaVisit.btnOtroClick(Sender: TObject);
var
   lOk:boolean;
begin
     lOk:=true;
     if ValidaCamposEnt then
     begin
          _AlEjecutar(Self, lOk);
          LimpiaControlesEntrada(true);
          dmVisitantes.cdsLibros.Append;
          Self.Close;
     end;
end;

procedure TFRegistroEntradaVisit.btnTomaFotoClick(Sender: TObject);
begin
     PermiteManejorCamara:=true;
     ConectaCamaraTomaFoto;
     FTomoFoto:=true;
     if PermiteManejorCamara then
     begin
          if btnTomaFoto.OptionsImage.ImageIndex <> 0 then
             cmbDriverFotoChange(Self);


          if btnTomaFoto.OptionsImage.ImageIndex =0 then
          begin
               btnTomaFoto.OptionsImage.ImageIndex := 1;
               btnTomaFoto.Hint := 'Prender c�mara';
               cmbDriverFoto.Enabled := false;
          end
          else
          begin
               btnTomaFoto.OptionsImage.ImageIndex := 0;
               btnTomaFoto.Hint := 'Tomar foto';
               cmbDriverFoto.Enabled := true;
          end;
     end;
     PermiteManejorCamara:=false;
end;

procedure TFRegistroEntradaVisit.BuscaAnfitrion;
var
   sKey, sDescription: String;
   sTexto:string;
begin
     inherited;
     FEsNuevoAnfitrion := false;
     sKey := LI_ANFITR.Text;
     sTexto := sDescription;
     if Search( VACIO, sKey, sDescription, 2) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_ANFITR').AsString := ObtenNombreAnfi(strtoint(sKey));
               FieldByName('AN_NUMERO').AsInteger := StrtoInt( sKey );
               if FieldByName('AN_NUMERO').AsInteger > 0 then
               begin
                    VISITAA.Llave := sKey;
                    LI_ANFITR.Text := ObtenNombreAnfi(strtoint(sKey));
               end;
               bValidoAnitrion := true;
               VISITAA.SetFocus;
               NextVisitaAFocus;
               //SelectNext(Self as TWinControl, True, false);
          end;
     end
     else
         begin
              if not bEsNuevoAnfitrion then
                 LI_ANFITR.SetFocus
              else
                  NextVisitaAFocus;
         end;
end;

procedure TFRegistroEntradaVisit.BuscaEmpresa;
var
   sKey, sDescription: String;
begin
     inherited;
     sKey := LI_EMPRESA.Text;
     if Search( VACIO, sKey, sDescription, 1) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_EMPRESA').AsString := sDescription;
               FieldByName('EV_NUMERO').AsInteger := StrtoInt( sKey );
               dblCompania.Llave := sKey;
               LI_EMPRESA.Text := sDescription;
               LI_ANFITR.SetFocus;
          end;
     end;
     //self.BringToFront;
end;

procedure TFRegistroEntradaVisit.bBuscaAnfitrionClick(Sender: TObject);
begin
     BuscaAnfitrion;
end;

procedure TFRegistroEntradaVisit.bBuscaEmpreClick(Sender: TObject);
begin
     BuscaEmpresa;
end;

procedure TFRegistroEntradaVisit.Cancelar_DevExClick(Sender: TObject);
begin
     CloseQuery;
     TressShell.bEsRegistroGrupo:=false;
    // FBusquedaVisitante.Close;
end;

procedure TFRegistroEntradaVisit.cmbDriverFotoChange(Sender: TObject);
begin
     PermiteManejorCamara:=true;
     if PermiteManejorCamara = true then
     begin
          ConectaCamaraTomaFoto;
          Foto.Clear;
          if cmbDriverFoto.ItemIndex > 0 then
          begin
               ShowVideoFormats;
               MuestraImagenFoto;
               btnTomaFoto.Enabled:=FOTO.IO.DShowParams.Connected;
         end
         else
         begin
              ConectaCamaraTomaFoto;
              btnTomaFoto.Enabled:=false;
              Foto.Enabled:=false;
         end;
     end;
     PermiteManejorCamara:=false;
end;


procedure TFRegistroEntradaVisit.CMDialogKey(var msg: TCMDialogKey);
begin
     if msg.Charcode <> VK_TAB then
        inherited;
end;

procedure TFRegistroEntradaVisit.ConectaCamar;
begin
  if (not FOTO.IO.DShowParams.Connected) then
  begin
       // set video source as index of IO.DShowParams.VideoInputs
       FOTO.IO.DShowParams.SetVideoInput(cmbDriverFoto.ItemIndex-1,
       StrToIntDef('0',0), // set this parameter if you have more than one camera with same name
       StrToIntDef('640', 0), // capture width
       StrToIntDef('480', 0), // capture height
       AnsiString('YUY2' )    // format
       );
       // enable frame grabbing
       FOTO.IO.DShowParams.EnableSampleGrabber := true;
       // connect to the video input
       FOTO.IO.DShowParams.Connect;

       //imageenview1.io.dshowparams.SaveGraph('c:\1.grf');
end;
end;

procedure TFRegistroEntradaVisit.ConectaCamaraTomaFoto;
var
   sMsgError: String;
begin
     SetControls( True );
     with Foto.IO.DShowParams do
     begin
          GetSample( Foto.IEBitmap );
          Stop;
          Disconnect;
          Update;
     end;
     if ( not FToolsImageEn.ResizeImagen( FOTO, sMsgError ) ) then
        ZError( self.Caption, sMsgError, 0);
     FToolsImageEn.AsignaImagenABlob( FOTO, dmVisitantes.cdsVisitante, 'VI_FOTO' );
end;


function TFRegistroEntradaVisit.EjecutarWizard: Boolean;
var
   oCursor: TCursor;
   iRecords, iFolio : integer;
   sIndex: string;
begin
     with Screen do
     begin
          oCursor := Screen.Cursor;
          Cursor := crHourglass;
          iFolio := -1;
          try
             Result := TRUE;
             try

                with dmVisitantes.cdsLibros do
                begin
                     iRecords := RecordCount;
                     AntesDeEnviar;
                      try
                         DisableControls;
                         sIndex := IndexFieldNames;
                         IndexFieldNames := 'LI_FOLIO';

                         Enviar;
                         LlenaDatosGrupo;


                         Refrescar;

                         if ( iRecords <> RecordCount ) then Last;

                         iFolio := FieldByName('LI_FOLIO').AsInteger;

                         TressShell.ImprimeForma(TRUE);

                      finally
                             IndexFieldNames := sIndex;
                             if (iFolio >= 0 ) then
                                Locate('LI_FOLIO',VarArrayOf([iFolio]), []);
                             EnableControls;
                      end;
                end;

             except
                      Result := FALSE;
                      Raise;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;

end;



function TFRegistroEntradaVisit.ExisteVisitante: Boolean;
begin
     Result := false;
end;

procedure TFRegistroEntradaVisit.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     PermiteManejorCamara:=true;
     with dmVisitantes.cdsLibros do
     begin
          if State in [dsInsert, dsEdit] then
             Cancel;
          end;
          if PermiteManejorCamara then
          begin
               ConectaCamaraTomaFoto;
               btnTomaFoto.OptionsImage.ImageIndex:=0;
          end;
     PermiteManejorCamara:=false;
     EsRegistroCita:=false;
     TressShell.TimerAlertas.Enabled :=FEstadoAlerta;
    if (FBusquedaVisitante <> nil) and (not FBusquedaVisitante.bCerrarBusqueda) then
        FBusquedaVisitante.Close;
end;

procedure TFRegistroEntradaVisit.FormCreate(Sender: TObject);
begin
     FEstadoAlerta := TressShell.TimerAlertas.Enabled;
     connect;
     VISITAA.LookupDataset := dmVisitantes.cdsAnfitrion;
     dblCompania.LookupDataset := dmVisitantes.cdsCompaniaLookup;
     iVisitNumero:=-1;

     EsRegistroCita:=false;
     //ActivaCamara;
     if Busqueda_DevEx = nil then
        Busqueda_DevEx := ZVisitBusqueda_DevEx.TBusqueda_DevEx.Create(Self);
     if FBusquedaVisitante = nil then
        FBusquedaVisitante := TFBusquedaVisitante.Create(Self);
end;

procedure TFRegistroEntradaVisit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if Key = VK_TAB then
        ShowMessage('Tab Key Down!');
end;

procedure TFRegistroEntradaVisit.FormShow(Sender: TObject);
begin
     FEstadoAlerta := TressShell.TimerAlertas.Enabled;
     TressShell.TimerAlertas.Enabled := false;
     IniciaCaptura;
     LimpiaControlesEntrada(False);
     VerificaCamposNecesarios;
     PermiteManejorCamara := true;
     LlenaCombos;
     if (iVisitNumero >=  0)then
     begin
          IniciaControlesVisitante;
          LlenaDatosVisitante;
     end
     else IniciaControlesNuevoVisitante;
     if (TressShell.bEsRegistroGrupo) and (not TressShell.FPPrimerRegGrupo) then
        LlenaCamposGrupo;

     if (TressShell.FPPrimerRegGrupo) or (TressShell.bEsRegistroGrupo) then
        self.Caption := 'Registro entrada visitante (Grupo)';


     btnTomaFoto.Enabled:=FOTO.IO.DShowParams.Connected;
     ActivaCamara;
     PermiteManejorCamara:=false;

     if not VI_NOMBRES.Enabled then
     begin
          if LI_ANFITR.Text <> VACIO then
             NextVisitaAFocus
          else if LI_EMPRESA.Text = VACIO then
             LI_EMPRESA.SetFocus
          else
              LI_ANFITR.SetFocus;
     end
     else VI_NOMBRES.SetFocus;
   //  BusquedaVisitante.bCerrarBusqueda := false;  //  DACP
end;

procedure TFRegistroEntradaVisit.FotoDShowNewFrame(Sender: TObject);
begin
     // copy current sample to ImageEnView bitmap
     FOTO.IO.DShowParams.GetSample(FOTO.Layers[0].Bitmap);
     // refresh ImageEnView1
     FOTO.Update;
end;

function TFRegistroEntradaVisit.GetCodigoAsunto(i: integer): string;
begin
     if i >= length( arTiposAsuntos)   then
        Result := VACIO
     else
        Result :=   arTiposAsuntos[i];
end;

function TFRegistroEntradaVisit.GetTipoVisitante(sTipoVisi: string): integer;
var
   iContTVisi:integer;
begin
     iContTVisi:= 0;
     while iContTVisi <= length(arTiposVisitantes)-1 do
     begin
          if arTiposVisitantes[iContTVisi] = sTipoVisi then
             Result:= iContTVisi;
          inc(iContTVisi);
     end;
end;

procedure TFRegistroEntradaVisit.IniciaCaptura;
begin
    cbSexo.ItemIndex :=0;
    cbTipoVisitante.ItemIndex :=0;
    if (iVisitNumero <=0) and (Global.GetGlobalBooleano(K_GLOBAL_REQ_DEPARTAMENTO)) then
      cmbDEPTO.ItemIndex :=0;
    cmbTipoAsunto.ItemIndex :=0;
    //VI_FEC_NAC.Texto := DateToStrSQL(date);
    VI_FEC_NAC.Valor := date;
    Foto.Enabled := dmVisitantes.MostrarGridAvanzado;
end;

procedure TFRegistroEntradaVisit.IniciaControlesNuevoVisitante;
var
   bMuestraComboCams:boolean;
begin
     FTomoFoto:=false;
     VI_FEC_NAC.Valor := date;
     VI_FEC_NAC.SetFocus;
     Foto.Clear;
     ConectaCamaraTomaFoto;
     with FBusquedaVisitante do
     begin
          if sPNombreVisiNuevo <> VACIO then
             VI_NOMBRES.Text := sPNombreVisiNuevo;
          if sPApepatVisiNuevo <> VACIO then
             Self.VI_APE_PAT.Text := sPApepatVisiNuevo;
     end;
     cmbDriverFoto.Enabled:=true;
     btnTomaFoto.Enabled:=true;
     if cmbDriverFoto.Items.Count <= 1  then
        bMuestraComboCams := false
     else
         bMuestraComboCams := true;


     if not TressShell.bEsRegistroGrupo then
     begin
          cbTipoVisitante.ItemIndex := 0;
          VI_NACIONALIDAD.Clear;
          LI_EMPRESA.Clear;
     end;

     cmbDriverFoto.Visible := bMuestraComboCams;
     Label2.Visible := bMuestraComboCams;
end;

procedure TFRegistroEntradaVisit.IniciaControlesVisitante;
begin
     cmbDriverFoto.Enabled:=false;
     btnTomaFoto.Enabled:=false;
     cmbDriverFoto.Visible := false;
     Label2.Visible := false;
     btnTomaFoto.Visible := false;


end;

procedure TFRegistroEntradaVisit.LimpiaControlesEntrada(EsRegistroGrupo:boolean);
begin
     VI_NOMBRES.Clear;
     VI_APE_PAT.Clear;
     VI_APE_MAT.Clear;
     cbSexo.ItemIndex:=0;
     cbTipoVisitante.Clear;
     VI_NACIONALIDAD.Clear;
     VI_MIGRA.Clear;
     LI_OBSERVA.Clear;
     LI_TEXTO1.Clear;
     LI_TEXTO2.Clear;
     LI_TEXTO3.Clear;
     LI_GAFETE.Clear;
     LI_ID.Clear;
     Foto.Clear;
     btnTomaFoto.Enabled:= false;
     dblCompania.Llave:=VACIO;
     LI_EMPRESA.Clear;
     LI_ANFITR.Clear;
     VISITAA.Llave:=VACIO;
     cmbDEPTO.Clear;
     LI_CAR_PLA.Clear;
     LI_CAR_DES.Clear;
     LI_CAR_EST.Clear;
     cbTipoAuto.Clear;
     cmbTipoAsunto.Clear;
     VI_FEC_NAC.Clear;
end;

procedure TFRegistroEntradaVisit.LI_ANFITREnter(Sender: TObject);
begin
     bValidoAnitrion:= false;
end;

procedure TFRegistroEntradaVisit.LI_ANFITRExit(Sender: TObject);
begin
     if (not bValidoAnitrion) and (LI_ANFITR.Text <> VACIO) then
     begin
          bValidoAnitrion := true;
          if (LI_ANFITR.Text <> VACIO) then
             BuscaAnfitrion;
     end;
end;

procedure TFRegistroEntradaVisit.LI_EMPRESAExit(Sender: TObject);
var
   bValido:boolean;
begin
     bValido:=false;
     if not bValido and (LI_EMPRESA.Text <> VACIO) then
     begin
          bValido := true;
          BuscaEmpresa;
     end;
end;

procedure TFRegistroEntradaVisit.LI_TEXTO3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if key = 13 then
        bOk.SetFocus;
end;

procedure TFRegistroEntradaVisit.LlenaCamposGrupo;
begin
     with TressShell do
     begin
          if not TressShell.FPPrimerRegGrupo then
          begin
               dblCompania.Llave := sPCompania;
               if dblCompania.Descripcion = VACIO then
                  dblCompania.Valor := -1;
               self.LI_EMPRESA.Text := dblCompania.Descripcion;
               Self.VISITAA.Llave := sPVisitaA;
               if self.VISITAA.Descripcion = VACIO then
                  self.VISITAA.Llave := '';
               self.LI_ANFITR.Text := self.VISITAA.Descripcion;
               cmbDEPTO.ItemIndex  := iPDepto;
               cmbTipoAsunto.ItemIndex := iPAsunto;
               cbTipoAuto.ItemIndex := iPTAuto;
               Self.LI_CAR_PLA.Text := sPCarPla;
               self.VISITAA.Llave := sPVisitaA;


               LI_CAR_DES.Text := sPCarDes;
               LI_CAR_EST.Text := sPCarEst;
               LI_OBSERVA.Text := sPObserva;
               LI_TEXTO1.Text := sPTexto1;
               LI_TEXTO2.Text := sPTexto2;
               LI_TEXTO3.Text := sPTexto3;

               if (iVisitNumero < 0)then
               begin
                    dblCompania.Valor := iPAntCompania;
                    if dblCompania.Descripcion <> VACIO then
                       Self.LI_EMPRESA.Text := dblCompania.Descripcion;
                    cbTipoVisitante.ItemIndex := iPAntTipoVisitante;
                    VI_NACIONALIDAD.Text := sPAntNacionalidad;
                    self.LI_ANFITR.Text := self.VISITAA.Descripcion;
               end;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.LlenaComboAsunto;
var
   iContAsunto:integer;
begin
     iContAsunto:=0;
     cmbTipoAsunto.Properties.Items.Clear;
     with dmVisitantes.cdsTipoAsunto do
     begin
          SetLength(arTiposAsuntos, RecordCount);
          {if RecordCount <=0 then
             DatabaseError('No existen asuntos');}
          if not Global.GetGlobalBooleano(K_GLOBAL_REQ_ASUNTO) then
             cmbTipoAsunto.Properties.Items.Add(' ');
          close;
          open;
          while iContAsunto <= RecordCount-1 do
          begin
               cmbTipoAsunto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTiposAsuntos[iContAsunto]:= FieldByName('TB_CODIGO').AsString;
               inc(iContAsunto);
               next;
          end;
     end;
         if Global.GetGlobalBooleano(K_GLOBAL_REQ_ASUNTO) then
            cmbTipoAsunto.ItemIndex := 0;
end;

procedure TFRegistroEntradaVisit.LlenaComboAsuntoCodigo(sCodigo: string);
var
   iCont:integer;
begin
     iCont := 0;
     while iCont <= cmbTipoAsunto.Properties.Items.Count - 1 do
     begin
          if GetCodigoAsunto(iCont) = sCodigo then
          begin
               if not Global.GetGlobalBooleano(K_GLOBAL_REQ_ASUNTO) then
                  cmbTipoAsunto.ItemIndex := iCont + 1
               else cmbTipoAsunto.ItemIndex := iCont;

          end;
          inc(iCont);
     end;

end;

procedure TFRegistroEntradaVisit.LlenaComboDepto;
var
   iContDepto:integer;
begin
     iContDepto:=0;
     cmbDepto.Properties.Items.Clear;
     with dmVisitantes.cdsDepto do
     begin
          SetLength(arDeptos, RecordCount);
          close;
          open;
          while iContDepto < RecordCount do
          begin
               cmbDepto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arDeptos[iContDepto]:= FieldByName('TB_CODIGO').AsString;
               inc(iContDepto);
               next;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.LlenaCombos;
begin
     with dmVisitantes do
     begin
          {SetLength(arTiposVisitantes, cdsTipoVisita.RecordCount);
          LlenaComboDs(cbTipoVisitante, cdsTipoVisita, arTiposVisitantes);//llena combo tipos visitantes

          SetLength(arTiposAsuntos, cdsTipoAsunto.RecordCount);
          LlenaComboDs(cmbTipoAsunto, cdsTipoAsunto, arTiposAsuntos);//llena combo asunto

          SetLength(arDeptos, cdsDepto.RecordCount);
          LlenaComboDs(cmbDEPTO, cdsDepto, arDeptos);//llena combo deptos

          SetLength(arTipoId, cdsTipoID.RecordCount);
          LlenaComboDs(cbTipoId, cdsTipoID, arTipoId);//llena combo id

          SetLength(arTipoAuto, cdsTipoCarro.RecordCount);
          LlenaComboDs(cbTipoAuto, cdsTipoCarro, arTipoAuto);//llena combo autos
           }
          LlenaComboTipoVisitante;
          LlenaComboAsunto;
          LlenaComboDepto;
          LlenaComboTipoId;
          LlenaComboTipoAuto
     end;
end;

procedure TFRegistroEntradaVisit.LlenaComboTipoAuto;
var
   iContId:integer;
begin
     iContId:=0;
     cbTipoAuto.Properties.Items.Clear;
     with dmVisitantes.cdsTipoCarro do
     begin
          SetLength(arTipoAuto, RecordCount);
          {if RecordCount <=0 then
              DatabaseError('No existen tipos de auto');}
          if not Global.GetGlobalBooleano(K_GLOBAL_REQ_VEHICULO) then
             cbTipoAuto.Properties.Items.Add(' ');
          close;
          open;
          while iContId <= RecordCount-1 do
          begin
               cbTipoAuto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTipoAuto[iContId]:= FieldByName('TB_CODIGO').AsString;
               inc(iContId);
               next;
          end;
     end;
       if cbTipoAuto.Properties.Items.Count >=0 then
          cbTipoAuto.ItemIndex := 0;
end;

procedure TFRegistroEntradaVisit.LlenaComboTipoId;
var
   iContId:integer;
begin
     iContId:=0;
     cbTipoId.Properties.Items.Clear;
     with dmVisitantes.cdsTipoID do
     begin
          SetLength(arTipoId, RecordCount);
          {if RecordCount <=0 then
              DatabaseError('No existen asuntos');}
          if not Global.GetGlobalBooleano(K_GLOBAL_REQ_IDENTIFICA) then
            cbTipoId.Properties.Items.Add(' ');
          close;
          open;
          while iContId <= RecordCount-1 do
          begin
               cbTipoId.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTipoId[iContId]:= FieldByName('TB_CODIGO').AsString;
               inc(iContId);
               next;
          end;
     end;
     if cbTipoId.Properties.Items.Count >=0 then
        cbTipoId.ItemIndex := 0;
end;

procedure TFRegistroEntradaVisit.LlenaComboTipoVisitante;
var
   iContTipoVisitante:integer;
begin
     iContTipoVisitante:=0;
     cbTipoVisitante.Properties.Items.Clear;
     with dmVisitantes.cdsTipoVisita do
     begin
          SetLength(arTiposVisitantes, RecordCount);
          {if RecordCount <=0 then
              DatabaseError('No existen tipos de visitante');}
          close;
          open;
          //cbTipoVisitante.Properties.Items.Add(' ');
          while iContTipoVisitante <= RecordCount-1 do
          begin
               cbTipoVisitante.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTiposVisitantes[iContTipoVisitante]:= FieldByName('TB_CODIGO').AsString;
               inc(iContTipoVisitante);
               next;
          end;
     end;
     if cbTipoVisitante.Properties.Items.Count >=0 then
        cbTipoVisitante.ItemIndex := 0;
end;

procedure TFRegistroEntradaVisit.LlenaDatosGrupo;
begin
     if TressShell.bEsRegistroGrupo then
     begin
          with TressShell do
          begin
               sPCompania := dblCompania.Llave;
               sPVisitaA := self.VISITAA.Llave;
               iPDepto := cmbDEPTO.ItemIndex;
               iPAsunto := cmbTipoAsunto.ItemIndex;
               iPTAuto :=  cbTipoAuto.ItemIndex;
               sPCarPla := Self.LI_CAR_PLA.Text;
               sPCarDes := LI_CAR_DES.Text;
               sPCarEst := LI_CAR_EST.Text;
               sPObserva := LI_OBSERVA.Text;
               sPTexto1 := LI_TEXTO1.Text;
               sPTexto2 := LI_TEXTO2.Text;
               sPTexto3 := LI_TEXTO3.Text;

               iPAntCompania := dblCompania.Valor;
               iPAntTipoVisitante := cbTipoVisitante.ItemIndex;
               sPAntNacionalidad := VI_NACIONALIDAD.Text;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.LlenaDatosVisitante;
var
   ds:TDataSet;
begin
     ds := nil;
     with dmVisitantes do
     begin
          if EsRegistroCita then
             ds := cdsCitasSQL
          else
              ds := cdsSQL;
          with ds do
          begin
               if Locate('VI_NUMERO',  iVisitNumero, []) then
               begin
                    VI_NOMBRES.Text := FieldByName('VI_NOMBRES').AsString;
                    VI_APE_PAT.Text := FieldByName('VI_APE_PAT').AsString;
                    VI_APE_MAT.Text := FieldByName('VI_APE_MAT').AsString;
                    VI_FEC_NAC.Valor := FieldByName('VI_FEC_NAC').AsDateTime;
                    if (FieldByName('AN_NUMERO').AsString <> vacio) and (FieldByName('AN_NUMERO').AsInteger > 0) then
                    begin
                         VISITAA.Llave := FieldByName('AN_NUMERO').AsString;
                         LI_ANFITR.Text := ObtenNombreAnfi(strtoint(VISITAA.Llave));
                         VISITAA.SetFocus;
                    end;
                    if FieldByName('VI_ASUNTO').AsString <> vacio then
                       LlenaComboAsuntoCodigo(FieldByName('VI_ASUNTO').AsString);
                    if FieldByName('VI_SEXO').AsString <> vacio then
                    begin
                         if (FieldByName('VI_SEXO').AsString = 'M') or (FieldByName('VI_SEXO').AsString = '1') then
                            cbSexo.ItemIndex := 0
                         else cbSexo.ItemIndex := 1;

                    end;
                    VI_FEC_NAC.Enabled := true;
                    VI_FEC_NAC.SetFocus;
                    VI_FEC_NAC.Enabled := false;
                    VI_MIGRA.Text := FieldByName('VI_MIGRA').AsString;
                    VI_NACIONALIDAD.Text := FieldByName('VI_NACION').AsString;
                    VI_MIGRA.Text := FieldByName('VI_MIGRA').AsString;
                    dblCompania.Valor := FieldByName('EV_NUMERO').AsInteger;
                    if dblCompania.Descripcion <> VACIO then
                       LI_EMPRESA.Text := dblCompania.Descripcion;
                    cbTipoVisitante.ItemIndex := getTipoVisitante(FieldByName('VI_TIPO').AsString);
                    FToolsImageEn.AsignaBlobAImagen( FOTO, ds, 'VI_FOTO' );
                    if (TressShell.iAnfiNumero >= 0) and (not TressShell.bEsRegistroGrupo) then
                    begin
                         VISITAA.Llave := inttostr(TressShell.iAnfiNumero);
                         LI_ANFITR.Text := ObtenNombreAnfi(strtoint(VISITAA.Llave));
                         if TressShell.sPAsunto <> VACIO then
                            LlenaComboAsuntoCodigo(TressShell.sPAsunto)
                         else
                             cmbTipoAsunto.ItemIndex := 0;
                         //if sPVisiObserva then

                         LI_OBSERVA.Text := TressShell.sPVisiObserva;
                         bValidoAnitrion:= true;
                    end;
                    if (FieldByName('VI_SEXO').AsString = 'M') or (FieldByName('VI_SEXO').AsString = '1')  then
                       cbSexo.ItemIndex := 0
                    else
                        cbSexo.ItemIndex := 1;

               end;
          end;
     end;
end;

procedure TFRegistroEntradaVisit.MuestraImagenFoto;
var
   w, h: integer;
   f: AnsiString;
begin
     ConectaCamar;
     // show info
     FOTO.IO.DShowParams.GetCurrentVideoFormat(w, h, f);
     // start capture
     FOTO.IO.DShowParams.Run;
end;

procedure TFRegistroEntradaVisit.NextVisitaAFocus;
begin
     if pnlAsunto.Visible then
           cmbTipoAsunto.SetFocus
        else if (cbTipoId.Visible) and (cbTipoId.Enabled) then
             cbTipoId.SetFocus
        else if (cbTipoAuto.Visible) and (cbTipoAuto.Enabled)then
             cbTipoAuto.SetFocus
        else if (LI_OBSERVA.Visible) and (LI_OBSERVA.Enabled) then
             LI_OBSERVA.SetFocus
        else
            bOk.SetFocus;

        bValidoAnitrion:= false;

end;

function TFRegistroEntradaVisit.ObtenNombreAnfi(iAnfi: integer): string;
begin
     with dmVisitantes.cdsAnfitrion do
     begin
          if Locate('AN_NUMERO', iAnfi, []) then
          begin
               Result := FieldByName('AN_NOMBRES').AsString +' '+ FieldByName('AN_APE_PAT').AsString+
                         ' '+fieldByName('AN_APE_MAT').AsString;
          end;
     end;
end;

function TFRegistroEntradaVisit.Search(const sFilter: String; var sKey,
  sDescription: String; iOpc:integer ): Boolean;
begin

     Busqueda_DevEx.bMuestraBotonAgregar := true;
     if Assigned( FOnSearch ) then
        FOnSearch( dmVisitantes.cdsCompaniaLookup, Result, sFilter, sKey, sDescription )
     else
     begin
          if iOpc = 1 then
             Result := ZVisitBusqueda_DevEx.ShowSearchForm( dmVisitantes.cdsCompaniaLookup, sFilter, sKey, sDescription )
          else if iOpc = 2 then
               Result := ZVisitBusqueda_DevEx.ShowSearchForm( dmVisitantes.cdsAnfitrion, sFilter, sKey, sDescription );
     end;
end;

procedure TFRegistroEntradaVisit.SetControls(lHabilita: Boolean);
begin

end;

procedure TFRegistroEntradaVisit.SetIVisiNumero(const Value: integer);
begin
     iVisitNumero:=Value;
end;

procedure TFRegistroEntradaVisit.ShowVideoFormats;
begin

end;



procedure TFRegistroEntradaVisit.TerminarWizard;
begin
     //Close;
     Winapi.Windows.SetForegroundWindow( Handle ); { Evita que la aplicaci�n pierda el Focus }

end;

function TFRegistroEntradaVisit.ValidaCamposEnt: boolean;
var
   bOk:boolean;
begin
     bOk:=true;
     if (VI_NOMBRES.Text = VACIO) and (VI_APE_PAT.Text=VACIO) then
     begin
          bOk:=false;
          if VI_NOMBRES.Enabled then
            VI_NOMBRES.SetFocus;
          ZError('Registro de entrada', 'El campo visitante requerido para grabar el registro de visitas', 0);
     end
     else if (dblCompania.Descripcion = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_COMPANYS)) or (LI_EMPRESA.Text = VACIO) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo compa��a es requerido para grabar el registro de visitas', 0);
          if LI_EMPRESA.Enabled then
            LI_EMPRESA.SetFocus;
     end
     else if (cmbTipoAsunto.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_ASUNTO)) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo asunto es requerido para grabar el registro de visitas', 0);
          if cmbTipoAsunto.Enabled then
            cmbTipoAsunto.SetFocus;
     end
     else if (VISITAA.Descripcion = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_ANFITRION)) or (LI_ANFITR.Text = VACIO) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo visita a es requerido para grabar el registro de visitas', 0);
          if LI_ANFITR.Enabled then
            LI_ANFITR.SetFocus;
     end
{     else if (cmbDEPTO.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_DEPARTAMENTO)) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo departamento requerido para grabar el registro de visitas', 0);
          if cmbDEPTO.Enabled then
            cmbDEPTO.SetFocus;
     end}
     else if (cbTipoId.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_IDENTIFICA)) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo identificaci�n es requerido para grabar el registro de visitas', 0);
          if cbTipoId.Enabled then
            cbTipoId.SetFocus;
     end
     else if (LI_GAFETE.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_GAFETE)) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo gafete es requerido para grabar el registro de visitas', 0);
          if LI_GAFETE.Enabled then
            LI_GAFETE.SetFocus;
     end
     else if (cbTipoAuto.Text = VACIO) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_VEHICULO)) then
     begin
          bOk:=false;
          ZError('Registro de entrada', 'El campo veh�culo es requerido para grabar el registro de visitas', 0);
          if cbTipoAuto.Enabled then
            cbTipoAuto.SetFocus;
     end;
     Result:=bOk;
end;


procedure TFRegistroEntradaVisit.VerificaCamposNecesarios;
var
   bMuestraIdentifica:boolean;
   bMuestraAutomovil:boolean;
   bMuestraObserva:boolean;
   bDatosEmpleado:boolean;
   bMuestraPnlInfoAuto:boolean;
   bMuestraAsunto:boolean;
begin

     with dmVisitantes do
     begin
          if iVisitNumero < 0 then
          bDatosEmpleado := true
          else bDatosEmpleado := false;

          bMuestraIdentifica := AutoReg_Identificacion;
          bMuestraAutomovil := AutoReg_Vehiculo;
          bMuestraObserva := AutoReg_Observaciones;
          bMuestraAsunto := AutoReg_AsuntoATratar;
     end;


     VI_NOMBRES.Enabled := bDatosEmpleado;
     VI_APE_PAT.Enabled := bDatosEmpleado;
     VI_APE_MAT.Enabled := bDatosEmpleado;
     cbSexo.Enabled := bDatosEmpleado;
     VI_FEC_NAC.Enabled := bDatosEmpleado;
     cbTipoVisitante.Enabled := bDatosEmpleado;
     VI_NACIONALIDAD.Enabled := bDatosEmpleado;
     VI_MIGRA.Enabled := bDatosEmpleado;
     {cmbDriverFoto.Enabled := bDatosEmpleado;
     btnTomaFoto.Enabled := bDatosEmpleado;
      }
     cbTipoId.enabled := bMuestraIdentifica;
     LI_ID.enabled := bMuestraIdentifica;
     LI_GAFETE.enabled := bMuestraIdentifica;
     lbId.enabled := bMuestraIdentifica;
     lbIdentificacion.enabled := bMuestraIdentifica;
     liGafete.enabled := bMuestraIdentifica;

     cbTipoAuto.enabled := bMuestraAutomovil;
     LI_CAR_PLA.enabled := bMuestraAutomovil;
     LI_CAR_DES.enabled := bMuestraAutomovil;
     LI_CAR_EST.enabled := bMuestraAutomovil;
     lbCarPla.enabled := bMuestraAutomovil;
     lbCarDesc.enabled := bMuestraAutomovil;
     lbCarEst.enabled := bMuestraAutomovil;
     liTipoVehiculo.enabled := bMuestraAutomovil;

     LI_OBSERVA.enabled := bMuestraObserva;
     LI_TEXTO1.enabled := bMuestraObserva;
     LI_TEXTO2.enabled := bMuestraObserva;
     LI_TEXTO3.enabled := bMuestraObserva;
     lbObserva1.enabled := bMuestraObserva;
     lbObserva2.enabled := bMuestraObserva;
     lbTexto1.enabled := bMuestraObserva;
     lbTexto2.enabled := bMuestraObserva;
     lbTexto3.enabled := bMuestraObserva;

     self.Height := 680;

     //BorraPanels;
     if (not bMuestraIdentifica) and (not bMuestraAutomovil) then
     begin
          cxGroupBox1.Visible := false;
     end
     else
         cxGroupBox1.Visible:=true;

     if cxGroupBox1.Visible then
     begin
          pnlAuto.Visible := bMuestraAutomovil;
          pnlIdentifica.Visible := bMuestraIdentifica;
     end
     else
         self.Height := self.Height - 129;

     pnlObserva.Visible := bMuestraObserva;
     if not bMuestraObserva then
        self.Height := Self.Height - 105;

     pnlAsunto.Visible := bMuestraAsunto;
     if not bMuestraAsunto then
        self.Height := Self.Height - 25;
end;

procedure TFRegistroEntradaVisit.VISITAAExit(Sender: TObject);
var
   iCont:integer;
begin
     cmbDEPTO.Clear;
     iCont:=0;
     while iCont <= length(arDeptos)-1 do
     begin
          if dmVisitantes.cdsAnfitrion.FieldByName('AN_DEPTO').AsString <> VACIO then
          if arDeptos[iCont] = dmVisitantes.cdsAnfitrion.FieldByName('AN_DEPTO').AsString then
             cmbDEPTO.ItemIndex := iCont;
          inc(iCont);
     end;
end;

procedure TFRegistroEntradaVisit.VI_FEC_NACKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if Key = 13 then
        SelectNext(ActiveControl as TWinControl, True, True );
end;

procedure TFRegistroEntradaVisit.WMKeyDown(var Message: TWMKeyDown);
begin
     inherited;
   case Message.CharCode of
     VK_RETURN: // ENTER pressed
       if (Message.KeyData and $1000000 <> 0) then
         // Test bit 24 of lParam
         SelectNext(ActiveControl as TWinControl, True, True )
       else
         SelectNext(ActiveControl as TWinControl, True, True );
end;
end;


procedure TFRegistroEntradaVisit._AlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Application.ProcessMessages;
        try
           lOk := EjecutarWizard;
        except
              on Error: Exception do
              begin
                   zExcepcion( Caption, 'Error al registrar entrada', Error, 0 );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     if lOk then
        TerminarWizard;
end;

end.
