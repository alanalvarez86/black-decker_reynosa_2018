unit FRegistroEntradaVisitante;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.StdCtrls, ZetaKeyCombo,
  ZetaFecha, Vcl.Mask, Vcl.DBCtrls, Vcl.ExtCtrls, dxGDIPlusClasses, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, TressMorado2013, cxTextEdit, cxMemo;

type
  TRegistroEntradaVisitante = class(TForm)
    DataSource: TDataSource;
    dsVisita: TDataSource;
    dsBuscaCompania: TDataSource;
    dsTipoVisitante: TDataSource;
    dsVehiculo: TDataSource;
    dsAsunto: TDataSource;
    dsTipoID: TDataSource;
    dsBuscaVisitandoA: TDataSource;
    dsBuscaVisita: TDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    Label2: TLabel;
    Image1: TImage;
    lblNombre: TLabel;
    VI_NOMBRES: TDBEdit;
    lblApePat: TLabel;
    VI_APE_PAT: TDBEdit;
    lblApeMat: TLabel;
    VI_APE_MAT: TDBEdit;
    lblSexo: TLabel;
    VI_SEXO: TZetaKeyCombo;
    lblFecNac: TLabel;
    VI_FEC_NAC: TZetaDBFecha;
    Label1: TLabel;
    ZetaKeyCombo1: TZetaKeyCombo;
    lblNacion: TLabel;
    VI_NACION: TDBEdit;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    Image3: TImage;
    Label4: TLabel;
    Image4: TImage;
    Label10: TLabel;
    Image2: TImage;
    Label3: TLabel;
    lblLicId: TLabel;
    LI_ID: TDBEdit;
    Label11: TLabel;
    LI_GAFETE: TDBEdit;
    Label12: TLabel;
    ZetaKeyCombo2: TZetaKeyCombo;
    Label13: TLabel;
    DBEdit6: TDBEdit;
    Label14: TLabel;
    DBEdit7: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit8: TDBEdit;
    ZetaKeyCombo3: TZetaKeyCombo;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit9: TDBEdit;
    Label19: TLabel;
    Label20: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    cxMemo1: TcxMemo;
  private
    { Private declarations }
    procedure Connect;
  public
    { Public declarations }
  end;

var
  RegistroEntradaVisitante: TRegistroEntradaVisitante;

implementation
uses
  DVisitantes;

{$R *.dfm}



procedure TRegistroEntradaVisitante.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoAsunto.Refrescar;
          cdsTipoCarro.Refrescar;
          cdsTipoID.Refrescar;
          cdsTipoVisita.Refrescar;
          cdsEmpVisitante.Conectar;//Se refresca en la busqueda de compa�ia
          //cdsCompaniaLookup.Conectar;
          //cdsCaseta.Conectar;
          //cdsDepto.Conectar;
          DataSource.DataSet:= cdsLibros;
          dsTipoVisitante.DataSet := cdsTipoVisita;
          dsBuscaVisitandoA.DataSet := cdsAnfitrionLookup;
          dsBuscaVisita.DataSet := cdsVisitanteLookup;
          dsBuscaCompania.DataSet := cdsCompaniaLookup;
          dsAsunto.Dataset := cdsTipoAsunto;
          dsTipoID.Dataset := cdsTipoID;
          dsVehiculo.DataSet := cdsTipoCarro;
          dsVisita.Dataset := cdsVisitante;
     end;

    //:todo ChecaGlobales;
    //:todo VI_SEXO.ItemIndex :=0;
end;

end.
