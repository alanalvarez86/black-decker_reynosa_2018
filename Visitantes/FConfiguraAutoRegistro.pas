unit FConfiguraAutoRegistro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, Vcl.ImgList, cxButtons;

type
  TConfiguraAutoRegistro = class(TZetaDlgModal_DevEx)
    GroupBox1: TGroupBox;
    cbAsuntoATratar: TCheckBox;
    cbVehiculo: TCheckBox;
    cbIdentificacion: TCheckBox;
    cbObservaciones: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure cbAsuntoATratarMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ActualizaRegistryObligatorios;
  public
    { Public declarations }
  end;

var
  ConfiguraAutoRegistro: TConfiguraAutoRegistro;

implementation
uses Printers,
     DVisitantes,
     ZetaCommonLists,
     ZetaCommonClasses,
     DGlobal,
     ZGlobalTress,
     DB;

{$R *.DFM}


procedure TConfiguraAutoRegistro.ActualizaRegistryObligatorios;
begin
 { with dmVisitantes do begin
    if Global.GetGlobalBooleano( K_GLOBAL_REQ_ASUNTO) then
      cbAsuntoATratar.Checked := true;
    if Global.GetGlobalBooleano( K_GLOBAL_REQ_VEHICULO) then
      cbVehiculo.Checked := true;
    if Global.GetGlobalBooleano( K_GLOBAL_REQ_IDENTIFICA) then
      cbIdentificacion.Checked := true;
  end;}
end;

procedure TConfiguraAutoRegistro.cbAsuntoATratarMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
{  if (Sender = cbAsuntoATratar) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_ASUNTO)) then begin
    cbAsuntoATratar.Checked:=true;
    DatabaseError('El asunto es un campo requerido.');
  end
  else if (Sender = cbVehiculo) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_VEHICULO)) then begin
    cbVehiculo.Checked:=true;
    DatabaseError('El vehiculo es un campo requerido.');
  end
  else if (Sender = cbIdentificacion) and (Global.GetGlobalBooleano( K_GLOBAL_REQ_IDENTIFICA)) then begin
    cbIdentificacion.Checked:=true;
    DatabaseError('El identificación es un campo requerido.');
  end;
 }
end;

procedure TConfiguraAutoRegistro.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          cbAsuntoATratar.Checked := AutoReg_AsuntoATratar;
          cbVehiculo.Checked := AutoReg_Vehiculo;
          cbIdentificacion.Checked := AutoReg_Identificacion;
          cbObservaciones.Checked := AutoReg_Observaciones;
     end;
     HelpContext := H_VIS_CASETA_REG_ENTRADA;
end;

procedure TConfiguraAutoRegistro.FormShow(Sender: TObject);
begin
  inherited;
  ActualizaRegistryObligatorios;
end;

procedure TConfiguraAutoRegistro.OK_DevExClick(Sender: TObject);
begin
   inherited;
     with dmVisitantes do
     begin
          AutoReg_AsuntoATratar := cbAsuntoATratar.Checked;
          AutoReg_Vehiculo := cbVehiculo.Checked;
          AutoReg_Identificacion := cbIdentificacion.Checked;
          AutoReg_Observaciones := cbObservaciones.Checked;
     end;

end;

end.
