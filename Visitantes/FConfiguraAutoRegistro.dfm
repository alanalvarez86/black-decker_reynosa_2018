inherited ConfiguraAutoRegistro: TConfiguraAutoRegistro
  Left = 418
  Top = 383
  Caption = 'Registro de Entrada '
  ClientHeight = 142
  ClientWidth = 225
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 231
  ExplicitHeight = 176
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 106
    Width = 225
    ExplicitTop = 106
    ExplicitWidth = 225
    inherited OK_DevEx: TcxButton
      Left = 53
      Top = 5
      OnClick = OK_DevExClick
      ExplicitLeft = 53
      ExplicitTop = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 134
      Top = 5
      ExplicitLeft = 134
      ExplicitTop = 5
    end
  end
  object GroupBox1: TGroupBox [1]
    Left = 0
    Top = 0
    Width = 225
    Height = 106
    Align = alClient
    Caption = 'Secciones a mostrar '
    TabOrder = 1
    object cbAsuntoATratar: TCheckBox
      Left = 67
      Top = 19
      Width = 97
      Height = 17
      Caption = 'Asunto a Tratar'
      TabOrder = 0
      OnMouseDown = cbAsuntoATratarMouseDown
    end
    object cbVehiculo: TCheckBox
      Left = 67
      Top = 56
      Width = 97
      Height = 17
      Caption = 'Veh'#237'culo'
      TabOrder = 1
      OnMouseDown = cbAsuntoATratarMouseDown
    end
    object cbIdentificacion: TCheckBox
      Left = 67
      Top = 38
      Width = 97
      Height = 17
      Caption = 'Identificaci'#243'n'
      TabOrder = 2
      OnMouseDown = cbAsuntoATratarMouseDown
    end
    object cbObservaciones: TCheckBox
      Left = 67
      Top = 75
      Width = 97
      Height = 17
      Caption = 'Observaciones'
      TabOrder = 3
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
