unit DCatalogos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient, ZetaClientDataSet;

type
  TdmCatalogos = class(TDataModule)
  private
    { Private declarations }
    function GetCondiciones: TZetaLookupDataSet ;
  public
    { Public declarations }
    property cdsCondiciones : TZetaLookupDataSet read GetCondiciones;
  end;

var
  dmCatalogos: TdmCatalogos;

implementation
{$R *.DFM}

function TdmCatalogos.GetCondiciones: TZetaLookupDataSet;
begin
     //Result := dmSeleccion.cdsCondiciones;
     Result := NIL;
end;

end.
