unit ZetaTipoEntidad;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Classes;
type
    { Tipo Entidades}
    TipoEntidad = ( enNinguno,              {0}
                    enCuenta,               {1}
                    enContrato,             {2}
                    enContacto,             {3}
                    enTicket,               {4}
                    enEjecutivo,            {5}
                    enModulo,               {6}
                    enCotizacion,           {7}
                    enNota,                 {8}
                    enReloj,                {9}
                    enProyecto,             {10}
                    enDocumento,            {11}
                    enModulosPorCuenta,     {12}
                    enCiudad,               {13}
                    enEstado,               {14}
                    enPais,                 {15}
                    enTipoCuenta,           {16}
                    enOrigen,               {17}
                    enDistribuidor,         {18}
                    enGiro,                 {19}
                    enNacionalidad,         {20}
                    enSector,               {21}
                    enCorporativo,          {22}
                    enTipoContacto,         {23}
                    enRole,                 {24}
                    enDepartamento,         {25}
                    enFuente,               {26}
                    enTipoTicket,           {27}
                    enProblema,             {28}
                    enSolucion,             {29}
                    enTicketDocto,          {30}
                    enTipoReloj,            {31}
                    enEjecVentas,           {32}
                    enEjecServicio,         {33}
                    enEjecOwner,            {34}
                    enEjecLider,            {35}
                    enEjecAsignado,         {36}
                    enReporte,              {37}
                    enCampoRep,             {38}
                    enCondiciones,          {39}
                    enBitacora,             {40}
                    enDiccion,              {41}
                    enUsuarios,             {42}
                    enCompanys,             {43}
                    enFunciones,            {44}
                    enFormula,              {45}
                    enAusencia,             {46}
                    enWorks,                {47}
                    enNomina,               {48}
                    enKarCurso,             {49}
                    enKardex,               {50}
                    enEmpleado,             {51}
                    enVacacion              {52}
                                        );

    ListaEntidades = set of TipoEntidad;

const

    EntidadesDescontinuadas{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}= [];
    NomParamEntidades{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}= [];
    EntidadDefaultCondicion = enNinguno;
    EntidadConCondiciones{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}=[];
    ReporteEspecial{$ifndef MULTIPLES_ENTIDADES}: ListaEntidades {$endif}= [];
    ReportesSinOrdenDef = [];


var
   aTipoEntidad: array[ TipoEntidad ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = (
        'Ninguno',
        'Cuenta',
        'Contrato',
        'Contacto',
        'Ticket',
        'Ejecutivo',
        'Modulo',
        'Cotizacion',
        'Nota',
        'Reloj',
        'Proyecto',
        'Documento',
        'ModulosPorCuenta',
        'Ciudad',
        'Estado',
        'Pais',
        'TipoCuenta',
        'Origen',
        'Distribuidor',
        'Giro',
        'Nacionalidad',
        'Sector',
        'Corporativo',
        'TipoContacto',
        'Role',
        'Departamento',
        'Fuente',
        'TipoTicket',
        'Problema',
        'Solucion',
        'TicketDocto',
        'TipoReloj',
        'EjecVentas',
        'EjecServicio',
        'EjecOwner',
        'EjecLider',
        'EjecAsignado',
        'Reporte',
        'CampoRep',
        'Condiciones',
        'Bitacora',
        'Diccion',
        'Usuarios',
        'Companys',
        'Funciones',
        'Formula',
        'Ausencia',
        'Works',
        'N�mina',
        'Curso',
        'Kardex',
        'Empleado',
        'Vacaciones' );

function ObtieneEntidad( const Index: TipoEntidad ): String;
procedure LlenaTipoPoliza( Lista: TStrings );

implementation

function ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     Result :=  aTipoEntidad[ Index  ];
end;

procedure LlenaTipoPoliza( Lista: TStrings );
begin
end;


end.
