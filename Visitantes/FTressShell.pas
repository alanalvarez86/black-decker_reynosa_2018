unit FTressShell;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls,
  Buttons, ZetaSmartLists, ZetaTipoEntidad, ZetaDBTextBox, //:todo OVCL
   Grids,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}

  DBGrids, Mask,  Db, ZetaDBGrid, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters,  TressMorado2013,
  dxSkinsDefaultPainters, 
  dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxRibbonCustomizationForm, System.Actions, cxStyles, cxClasses, dxSkinsForm,
  dxRibbon, dxStatusBar, dxRibbonStatusBar, dxSkinsCore,
  dxSkinscxPCPainter, dxBarBuiltInMenu, dxSkinsdxNavBarPainter,
  dxSkinsdxBarPainter, cxLocalization, dxBar, dxNavBar, cxButtons, cxPC,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxGridCustomView, cxGrid, ZetaCXGrid, cxImage, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridCardView, cxGridCustomLayoutView, cxCheckGroup,
  cxEditRepositoryItems, cxButtonEdit, cxLabel, dxGDIPlusClasses, Vcl.ToolWin,
  ZetaCommonLists, ZBaseShell, cxContainer, cxCheckBox;

type
  TTressShell = class(TBaseShell)
    PanelCaseta: TPanel;
    PanelCorte: TPanel;
    lbCorteActivo: TLabel;
    lbCorteStatus: TLabel;
    dsCitas: TDataSource;
    dsLibro: TDataSource;
    TimerCitas: TTimer;
    PageControl: TPageControl;
    tsLibro: TTabSheet;
    TimerAlertas: TTimer;
    Button1: TButton;
    PnlGafete: TPanel;
    eGafeteSalida: TEdit;
    LblGafete: TLabel;
    BtnGafete: TButton;
    PeriodoUltimo_DevEx: TcxButton;
    PeriodoSiguiente_DevEx: TcxButton;
    PeriodoAnterior_DevEx: TcxButton;
    PeriodoPrimero_DevEx: TcxButton;
    cxStyleRepository2: TcxStyleRepository;
    cxStyle3: TcxStyle;
    cxStyle4: TcxStyle;
    cxStyle5: TcxStyle;
    btnCerrarCorte: TcxButton;
    Label1: TLabel;
    SmallImageList: TcxImageList;
    Image16: TcxImageList;
    ArbolImages: TImageList;
    RibbonSmallImages: TcxImageList;
    dxBarManager1: TdxBarManager;
    cxStyle1: TcxStyle;
    bMenuAdministrador: TcxButton;
    btnCambioCaseta: TcxButton;
    cxButton3: TcxButton;
    DevEx_cxLocalizer: TcxLocalizer;
    cxStyle2: TcxStyle;
    cxStyle6: TcxStyle;
    cxStyle7: TcxStyle;
    cxStyleRepository3: TcxStyleRepository;
    stBoldBlack11: TcxStyle;
    stBoldRed11: TcxStyle;
    cxButton5: TcxButton;
    ActionList: TActionList;
    _Corte_Primero: TAction;
    _Corte_Anterior: TAction;
    _Corte_Siguiente: TAction;
    _Corte_Ultimo: TAction;
    _Refrescar_Citas: TAction;
    _Refrescar_Libro: TAction;
    _Cambiar_Caseta: TAction;
    _Cerrar_Libro: TAction;
    _Abrir_Libro: TAction;
    _Registrar_Entrada: TAction;
    _Registrar_Salida: TAction;
    _Imprime_una_forma: TAction;
    _Menu_Administrador: TAction;
    _Ver_Informacion: TAction;
    _BuscarLibro: TAction;
    _Registrar_Llegada: TAction;
    PanelLibroMaster: TPanel;
    Splitter1: TSplitter;
    Panel3: TPanel;
    PanelLibro: TPanel;
    bEntradaLibro: TSpeedButton;
    bSalidaLibro: TSpeedButton;
    bVerInformacion: TSpeedButton;
    ImprimirFormaBtn: TSpeedButton;
    BuscarBtn: TSpeedButton;
    bAbrirLibro: TSpeedButton;
    bCerrarLibro: TSpeedButton;
    bRefrescarLibro: TZetaSpeedButton;
    btnEntradaVisitante: TcxButton;
    btnSalidaVisitante: TcxButton;
    cxGrid1: TZetaCXGrid;
    cxGrid1CardView1: TcxGridCardView;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    LI_FOLIO: TcxGridDBBandedColumn;
    VI_FOTO: TcxGridDBBandedColumn;
    LI_ANFITR: TcxGridDBBandedColumn;
    US_ENT_LIB: TcxGridDBBandedColumn;
    US_SAL_LIB: TcxGridDBBandedColumn;
    LI_ENT_HOR: TcxGridDBBandedColumn;
    LI_SAL_HOR: TcxGridDBBandedColumn;
    LI_NOMBRE: TcxGridDBBandedColumn;
    LI_EMPRESA  : TcxGridDBBandedColumn;
    VI_TIPO: TcxGridDBBandedColumn;
    LI_DEPTO: TcxGridDBBandedColumn;
    ASUNTO: TcxGridDBBandedColumn;
    LI_CAR_PLA: TcxGridDBBandedColumn;
    Identificacion: TcxGridDBBandedColumn;
    LI_GAFETE: TcxGridDBBandedColumn;
    Relleno: TcxGridDBBandedColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid2: TZetaCXGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    LI_ENT_HOR2: TcxGridDBColumn;
    LI_SAL_HOR2: TcxGridDBColumn;
    US_ENT_LIB2: TcxGridDBColumn;
    US_SAL_LIB2: TcxGridDBColumn;
    LI_NOMBRE2: TcxGridDBColumn;
    LI_EMPRESA2: TcxGridDBColumn;
    LI_ANFITR2: TcxGridDBColumn;
    LI_DEPTO2: TcxGridDBColumn;
    LI_ASUNTO: TcxGridDBColumn;
    LI_GAFETE2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    GridLibro: TZetaDBGrid;
    Panel1: TPanel;
    cxGridCitas: TcxGrid;
    cxGridCitasDBBandedTableView1: TcxGridDBBandedTableView;
    CI_HORA: TcxGridDBBandedColumn;
    VI_NUMERO: TcxGridDBBandedColumn;
    AN_NUMERO: TcxGridDBBandedColumn;
    CI_STATUS: TcxGridDBBandedColumn;
    Depto: TcxGridDBBandedColumn;
    cxGridCitasLevel1: TcxGridLevel;
    Panel2: TPanel;
    VisitaA: TcxGridDBBandedColumn;
    De: TcxGridDBBandedColumn;
    dsVisita: TDataSource;
    dsSQL: TDataSource;
    btnImpLibro: TcxButton;
    btnInfoLibro: TcxButton;
    _Ver_totales: TAction;
    _bCerrarCorte: TAction;
    _AbrirCorte: TAction;
    Panel4: TPanel;
    cxButton6: TcxButton;
    bEntradaCita: TcxButton;
    btnRefrescaCitas: TcxButton;
    CI_OBSERVA: TcxGridDBBandedColumn;
    CI_ASUNTO: TcxGridDBBandedColumn;
    Timer1: TTimer;
    btnRegistroGrupo: TcxButton;
    _Registrar_EntradaGrupo: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _Corte_PrimeroExecute(Sender: TObject);
    procedure _Corte_AnteriorExecute(Sender: TObject);
    procedure _Corte_SiguienteExecute(Sender: TObject);
    procedure _Corte_UltimoExecute(Sender: TObject);
    procedure _Corte_PrimeroUpdate(Sender: TObject);
    procedure _Corte_UltimoUpdate(Sender: TObject);
    procedure TimerCitasTimer(Sender: TObject);
    procedure _Refrescar_CitasExecute(Sender: TObject);
    procedure _Refrescar_LibroExecute(Sender: TObject);
    procedure _Cambiar_CasetaExecute(Sender: TObject);
    procedure _Abrir_LibroExecute(Sender: TObject);
    procedure _Cerrar_LibroExecute(Sender: TObject);
    procedure _Registrar_EntradaExecute(Sender: TObject);
    procedure _Registrar_SalidaExecute(Sender: TObject);
    procedure BuscarBtnClick(Sender: TObject);
    procedure _Imprime_una_formaExecute(Sender: TObject);
    procedure GridLibroDblClick(Sender: TObject);
    procedure _Menu_AdministradorExecute(Sender: TObject);
    procedure TimerAlertasTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure _Ver_InformacionExecute(Sender: TObject);
    procedure _Registrar_LlegadaExecute(Sender: TObject);
    procedure dsCitasDataChange(Sender: TObject; Field: TField);
    procedure BtnGafeteClick(Sender: TObject);
    procedure BtnGafeteEnter(Sender: TObject);
    procedure PanelCasetaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bCambiarCasetaClick(Sender: TObject);
    procedure US_ENT_LIBCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure NoBorderEnTopCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure NoBorderEnMedioCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure NoBorderEnBottomCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure tbCambioCasetaClick(Sender: TObject);
    procedure btnCambioCasetaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure cxGrid2DBTableView1DblClick(Sender: TObject);
    procedure cxGridCitasResize(Sender: TObject);
    procedure cxGrid1DBBandedTableView1DblClick(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxGrid1DBBandedTableView1ColumnHeaderClick(
      Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure cxGrid2DBTableView1ColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure cxGrid1DBBandedTableView1DataControllerSortingChanged(
      Sender: TObject);
    procedure btnCerrarCorteClick(Sender: TObject);
    procedure cxGrid3DBTableView1Column2CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure cxGrid2DBTableView1DataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure btnSalidaVisitanteClick(Sender: TObject);
    procedure cxGrid2DBTableView1CellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBBandedTableView1CellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure bEntradaCitaClick(Sender: TObject);
    procedure LI_NOMBRE2StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure cxGrid2DBTableView1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure cxGrid1DBBandedTableView1CustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure _Ver_totalesExecute(Sender: TObject);
    procedure cxGridCitasDBBandedTableView1DblClick(Sender: TObject);
    procedure btnRefrescaCitasClick(Sender: TObject);
    procedure _Registrar_EntradaGrupoExecute(Sender: TObject);
  private
    { Private declarations }
    FLeeRegistry : Boolean;
    FEstaCargandoShell:boolean;
    FEstaLogeadoVisitantes: Boolean;

    FEsRegistroGrupo : boolean;
    FRegistroActivo : boolean;
    sCompania:string;
    sVisitaA:string;
    iFAnfiNumero:integer;
    iDepto:integer;
    iAsunto:integer;
    iTAuto:integer;
    sCarPla:string;
    sCarDes:string;
    sCarEst:string;
    sVisiObserva:string;
    sAsunto:string;
    sObserva:string;
    sTexto1:string;
    sTexto2:string;
    sTexto3:string;
    FPrimerRegGrupo:boolean;
    iAntTipoVisitante:integer;
    sAntNacionalidad:string;
    iAntCompania:integer;

    procedure CargaCasetaActiva;
    procedure RefrescaCasetaActiva;
    procedure CargaCorteActivo;
    procedure CargaCitas;
    procedure CargaLibro;
    procedure HabilitaControlesLibro;
    procedure HabilitaControlesAlertas;
    procedure ShowConfiguracion;
    procedure ShowConfiguraRegistro;
    procedure HabilitaControlesCita;
    procedure AbrirCorte;
    procedure CerrarCorte;
    procedure DoLogout;
    procedure PaintIcons(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure RefrescaShellVisitas;
    procedure SetEstaLogeadoVisitantes(const Value: Boolean);
    procedure AplicaGridMode;
    procedure ApplyMinWidth;
    procedure ActualizaBtnCerrar;
{$ifdef TEST_COMPLETE}
    function GetInformacionGridLibroDetalle: String;
    function GetInformacionGridLibro: String;
    function GetInformacionGridCitas: String;
    function ObtenerNombreAnfi(iAnfi: integer): string;
{$endif}
	procedure RegistrarEntrada;
    procedure RegistrarEntradaGrupo;
    function HayCitas: Boolean;
    function getFieldIcon(bandedColumn: string):integer;
    function TiempoExcede(sLiEntHor:string; sLiSalHor:string):boolean;
    function TextoNecesitaMascara(bandedColumn: string):boolean;
    function EscogeCaseta( lSetCasetaDefault: Boolean ): Boolean;

  protected
    AColumn: TcxGridDBColumn;
    procedure CargaTraducciones;
    procedure DoOpenAll;override;
    procedure DoCloseAll;override;
    procedure KeyPress( var Key: Char ); override;{ TWinControl }
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
   // procedure CMChildKey(var Message: TCMChildKey); message CM_CHILDKEY;
  published
    { Published declarations }
{$ifdef TEST_COMPLETE}
    property InformacionGridLibro: String read GetInformacionGridLibro;
    property InformacionGridLibroDetalle: String read GetInformacionGridLibroDetalle;
    property InformacionGridCitas: String read GetInformacionGridCitas;
{$endif}

  public
    { Public declarations }
    procedure SetDataChange(const Entidades: ListaEntidades);
    procedure ImprimeForma(const lValidaImpresion: Boolean);
    procedure ActualizaConfiguracionVisitantes;
    property EstaLogeadoVisitantes: Boolean  read FEstaLogeadoVisitantes write SetEstaLogeadoVisitantes;
    property EstaCargandoShell: Boolean  read FEstaCargandoShell;
    procedure RefrescaShell;
    procedure LimpiaDatosGrupo;
    {$ifdef TEST_COMPLETE} function GetDatosGridTestComplete(const ds:TDataSet; const sCampos:string):string;{$endif}

    property bRegistroActivo: Boolean read FRegistroActivo write FRegistroActivo;
    property bEsRegistroGrupo: Boolean read FEsRegistroGrupo write FEsRegistroGrupo;
    property sPCompania: string read sCompania write sCompania;
    property sPVisitaA: string read sVisitaA write sVisitaA;
    property iPDepto: integer read iDepto write iDepto;
    property iPAsunto: integer read iAsunto write iAsunto;
    property iPTAuto: integer read iTAuto write iTAuto;
    property sPCarPla: string read sCarPla write sCarPla;
    property sPCarDes: string read sCarDes write sCarDes;
    property sPCarEst: string read sCarEst write sCarEst;
    property iAnfiNumero: integer read iFAnfiNumero write iFAnfiNumero;
    property sPVisiObserva: string read sVisiObserva write sVisiObserva;
    property sPAsunto: string read sAsunto write sAsunto;
    property sPObserva: string read sObserva write sObserva;
    property sPTexto1: string read sTexto1 write sTexto1;
    property sPTexto2: string read sTexto2 write sTexto2;
    property sPTexto3: string read sTexto3 write sTexto3;
    property FPPrimerRegGrupo: Boolean read FPrimerRegGrupo write FPrimerRegGrupo;

    property iPAntTipoVisitante: integer read iAntTipoVisitante write iAntTipoVisitante;
    property sPAntNacionalidad: string read sAntNacionalidad write sAntNacionalidad;
    property iPAntCompania: integer read iAntCompania write iAntCompania;

  end;

var
  TressShell: TTressShell;

implementation

uses DCliente,
     DVisitantes,
     DSistema,
     DReportes,
     DDiccionario,
     DGlobal,

     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}

     FEscogeCaseta,
     FEditLibros,
     FEditLibrosSalida,
     FBusquedaLibro,
     FMenuConfigura,
     FConfiguraAutoRegistro,
     FConfigura,
     ZImprimeForma,
     ZAccesosMgr,
     ZAccesosTress,
     ZBaseEdicion,
     ZReportTools,
     ZGlobalTress,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses,
     cxTextEdit,
     ZetaClientDataset,
     FRegistroEntradaVisi,
     FBusquedaVisit,
     FTableroTotales,
     ZetaRegistryCliente, FDatosVisitGrupo;



{$R *.DFM}

type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);

procedure TTressShell.FormCreate(Sender: TObject);
begin
     FLeeRegistry := TRUE;
     {$ifdef DOS_CAPAS}
             DZetaServerProvider.InitAll;
     {$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmVisitantes := TdmVisitantes.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     //dmCatalogos := TdmCatalogos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     inherited;
     dsCitas.Dataset := dmVisitantes.cdsCitas;
     dsLibro.Dataset := dmVisitantes.cdsLibros;
     HabilitaControlesCita;
     HelpContext := H_VIS_PRINCIPAL;
     self.WindowState := wsMaximized;
     FEstaLogeadoVisitantes:=false;

     cxGrid1DBBandedTableView1.DataController.DataModeController.GridMode:= True;
     cxGridCitasDBBandedTableView1.DataController.DataModeController.GridMode:= True;

end;

procedure TTressShell.HabilitaControlesAlertas;
begin
     with TimerAlertas do
     begin
          Enabled := dmVisitantes.AlertasEnabled;
          Interval := Global.GetGlobalInteger( K_GLOBAL_ALERTAS_INTERVALO ) * 60000;
     end;
end;

procedure TTressShell.HabilitaControlesCita;
 var
    lVisible : Boolean;
begin
     lVisible := dmVisitantes.MostrarGridCitas;
     //tsCitas.TabVisible := lVisible;
     Panel1.Visible := lVisible;
     tsLibro.TabVisible := lVisible;
     TimerCitas.Enabled := lVisible;

     PageControl.ActivePage := tsLibro;
     cxGrid1.Align:= alClient;
     cxGrid2.Align:= alClient;
     bEntradaCita.Caption := 'Registrar llegada';
end;

procedure TTressShell.KeyPress(var Key: Char);
begin
     if ( dmVisitantes.CorteActivo ) then
     begin
          if ( not eGafeteSalida.Focused ) then
          begin
               if ( ( Key <> chr(9) ) and ( key <> #0 ) and ( ZetaCommonTools.EsDigito( Key ) or ZetaCommonTools.EsLetra( Key ) ) ) then
               begin
                    eGafeteSalida.SetFocus;
                    SendMessage(eGafeteSalida.Handle, WM_Char, word(Key), 0);
                    Key := #0;
               end;
          end;
     end;
end;

procedure TTressShell.LimpiaDatosGrupo;
begin
     sPCompania:='';
     sPVisitaA:='';
     iPDepto:= 0;
     iPAsunto:=0;
     FEsRegistroGrupo:=false;
     iAnfiNumero:= -1;
     sPCarPla := '';
     sPCarDes := '';
     sPCarEst := '';
     sPObserva :='';
     sPTexto1 := '';
     sPTexto2 := '';
     sPTexto3 := '';
end;

procedure TTressShell.LI_NOMBRE2StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
     {inherited;
     if dmVisitantes.TiempoExcedido( cxGrid2DBTableView1.DataController.DataSource.Dataset )then
       AStyle:= stBoldRed11
     else AStyle := stBoldBlack11;}
end;

procedure TTressShell.PaintIcons(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
var
   APainter: TcxPainterAccess;
   ARect: TRect;
   sTexto:string;
begin
     //NoBorderEnBottomCustomDrawCell(Sender,ACanvas, AViewInfo,ADone);
     if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo)then
        Exit;

     APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
     with APainter do
     begin
          try
             with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
               Left := Left + SmallImageList.Width -25;

             DrawContent;

             with AViewInfo.ClientBounds do
             begin

                  if (AViewInfo.Value <> null) then begin
                  if TextoNecesitaMascara(AViewInfo.Item.Caption) and (AViewInfo.Value <> '') then
                      sTexto:=MaskHora(AViewInfo.Value)
                  else
                      sTexto:=AViewInfo.Value;

                  ACanvas.Canvas.FillRect( AViewInfo.Bounds );
                  aRect := TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect;
                  aRect.Left := aRect.Left + SmallImageList.Width + 2;
                  {if  (AViewInfo.GridRecord.Values[5] <> null) and (AViewInfo.GridRecord.Values[6] <> null) then
                  if  (TiempoExcede(AViewInfo.GridRecord.Values[5], AViewInfo.GridRecord.Values[6]))  and
                       (AViewInfo.Item.Caption =  'Salida') and (sTexto = VACIO) then begin
                       AViewInfo.Item.Caption:='Tiempo excedido';
                      sTexto :='Tiempo excedido';    //codigo para agregar warning en campo salida
                  end;}

                  if sTexto <> '' then
                  with AViewInfo.ClientBounds do
                    SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, getFieldIcon(AViewInfo.Item.Caption));    //Agrega el icono a la columna
                    ACanvas.DrawText(sTexto, aRect, cxAlignLeft or cxShowEndEllipsis);                           //Agrega texto a la columna
                  end;
             end;
          finally
            Free;
          end;
          ADone := True;
    end;
end;

procedure TTressShell.PanelCasetaClick(Sender: TObject);
begin
     inherited;

end;

// Control de Edici�n del Grid
procedure TTressShell.KeyDown(var Key: Word; Shift: TShiftState);
begin
     if ( ActiveControl = GridLibro ) then
     begin
          eGafeteSalida.SetFocus;
          SendMessage(eGafeteSalida.Handle, WM_Char, word(Key), 0);
          Key := 0;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmDiccionario.Free;
     dmReportes.Free;
     //dmCatalogos.Free;
     dmSistema.Free;
     dmVisitantes.Free;
     dmCliente.Free;
     Global.Free;
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     LimpiaDatosGrupo;
     AplicaGridMode;
     CargaTraducciones;
     ActualizaConfiguracionVisitantes;
     FEstaLogeadoVisitantes:=true;
     //Para que nunca muestre el filterbox inferior
     cxGrid2DBTableView1.FilterBox.Visible := fvNever;
     dmVisitantes.OPCSQL:=1;
     bMenuAdministrador.Enabled := ClientRegistry.CanWrite;
end;

function TTressShell.getFieldIcon(bandedColumn: string):integer;
begin
     if  bandedColumn = 'Folio'then
         Result := 17
     else if (bandedColumn = 'Entrada') then
          Result := 4
     else if (bandedColumn = 'Salida') then
          Result := 5
     else if (bandedColumn = 'Identificacion')then
          Result := 11
     else if (bandedColumn = 'Placas') then
          Result :=  12
     else if (bandedColumn = 'Registr� salida') then
          Result :=  16;
end;

procedure TTressShell.SetDataChange(const Entidades: ListaEntidades);
begin
     RefrescaShell;
end;

procedure TTressShell.SetEstaLogeadoVisitantes(const Value: Boolean);
begin
     FEstaLogeadoVisitantes := Value;
end;

function TTressShell.TextoNecesitaMascara(bandedColumn: string): boolean;
begin
     if (bandedColumn = 'Entrada') then
      Result := true
    else if (bandedColumn = 'Salida') then
      Result := true
    else
      Result := false;
end;

procedure TTressShell.DoOpenAll;
begin
     inherited DoOpenAll;
     if EscogeCaseta( FALSE ) then
     begin
          Global.Conectar;

          dmVisitantes.InitActivosCorte;

          HabilitaControlesAlertas;

          CargaCasetaActiva;
          CargaCorteActivo;
          CargaCitas;
          RefrescaShell;
     end;

     bMenuAdministrador.Visible := ZAccesosMgr.CheckDerecho( D_SIST_ADMIN_CASETA, K_DERECHO_CONSULTA ) OR
                                   ZAccesosMgr.CheckDerecho( D_SIST_ADMIN_CASETA, K_DERECHO_ALTA );

    if ( dmVisitantes.CorteActivo AND eGafeteSalida.CanFocus ) then
       eGafeteSalida.SetFocus;
end;

procedure TTressShell.DoCloseAll;
begin
     if dmVisitantes.CorteActivo then
     begin
          if ZetaDialogo.ZConfirm( Caption, '� Desea cerrar el corte ?', 0, mbYes ) then
             CerrarCorte;
     end;
     inherited DoCloseAll;
end;


procedure TTressShell.CargaCorteActivo;
 const
      K_DATE_FORMAT = 'dddd d/mmmm';
      K_TIME_FORMAT = '00:00;0';
 var
    eStatus: eCorteStatus;
    sCorte: string;
begin
     label1.Caption := 'Corte '+ inttostr(dmVisitantes.Corte) + ':';
     eStatus := ecCerrado;
     with dmVisitantes.cdsCortes do
     begin
          if NOT IsEmpty then
          begin
               if dmVisitantes.CorteActivo then
               begin
                    sCorte := Format( 'Del %s a las %s horas',
                                      [ FormatDateTime( K_DATE_FORMAT, FieldByName('CO_INI_FEC').AsDateTime ),
                                        FormatMaskText( K_TIME_FORMAT, FieldByName('CO_INI_HOR').AsString ) ] );

                    eStatus := ecAbierto;
                    lbCorteStatus.Font.Color := clGreen;

               end
               else
               begin
                    if ( FieldByName('CO_INI_FEC').AsDateTime = FieldByName('CO_FIN_FEC').AsDateTime ) then
                    begin
                         sCorte := Format( 'Del %s de %s a %s',
                                           [ FormatDateTime( K_DATE_FORMAT, FieldByName('CO_INI_FEC').AsDateTime ),
                                             FormatMaskText( K_TIME_FORMAT, FieldByName('CO_INI_HOR').AsString ),
                                             FormatMaskText( K_TIME_FORMAT, FieldByName('CO_FIN_HOR').AsString ) ] );
                    end
                    else
                    begin
                         sCorte := Format( 'Del %s %s Hasta el %s %s',
                                           [ FormatDateTime( 'd\mmm', FieldByName('CO_INI_FEC').AsDateTime ),
                                             FormatMaskText( K_TIME_FORMAT, FieldByName('CO_INI_HOR').AsString ),
                                             FormatDateTime( 'd\mmmm', FieldByName('CO_FIN_FEC').AsDateTime ),
                                             FormatMaskText( K_TIME_FORMAT, FieldByName('CO_FIN_HOR').AsString ) ] );
                    end;

                    eStatus := ecCerrado;
                    lbCorteStatus.Font.Color := clRed;
               end;
          end
          else
              sCorte := 'No existe ning�n corte';
     end;

     lbCorteStatus.Caption := ObtieneElemento( lfCorteStatus, Ord( eStatus ) );
     lbCorteActivo.Caption := sCorte;
     HabilitaControlesLibro;
end;

procedure TTressShell.HabilitaControlesLibro;
 var
    iVigilante: integer;
begin
     iVigilante := 0;
     with dmVisitantes do
     begin
          if cdsCortes.Active then
          begin
               iVigilante := cdsCortes.FieldByName('CO_INI_VIG').AsInteger;
          end;
          btnEntradaVisitante.Enabled := CorteActivo and (dmCliente.Usuario = iVigilante);
          bEntradaLibro.Enabled := CorteActivo and (dmCliente.Usuario = iVigilante);
          bSalidaLibro.Enabled := CorteActivo and (dmCliente.Usuario = iVigilante);
          btnRegistroGrupo.Enabled := btnEntradaVisitante.Enabled;
          //btnRefrescaCitas.Enabled := btnEntradaVisitante.Enabled;
          bEntradaCita.Enabled := btnEntradaVisitante.Enabled and HayCitas;
          cxGridCitas.Enabled := btnEntradaVisitante.Enabled and HayCitas;
          lblGafete.Enabled := CorteActivo and (dmCliente.Usuario = iVigilante);
          eGafeteSalida.Enabled := CorteActivo and (dmCliente.Usuario = iVigilante);
          if (cdsCortes.State in [dsEdit, dsInsert]) and (( cdsCortes.FieldByName('CO_FOLIO').AsInteger = UltimoCorteAbierto ) OR
             (UltimoCorteAbierto = 0 )) then
          begin
               bAbrirLibro.Enabled := NOT CorteActivo;
               bCerrarLibro.Enabled := CorteActivo;
          end
          else
          begin
               bAbrirLibro.Enabled := FALSE;
               bCerrarLibro.Enabled := FALSE;
          end;

          //ImprimirFormaBtn.Enabled := NOT cdsLibros.IsEmpty;
          BuscarBtn.Enabled := NOT cdsLibros.IsEmpty;
          bVerInformacion.Enabled := NOT cdsLibros.IsEmpty;

          eGafeteSalida.Text := VACIO;

          if lbCorteStatus.Caption = 'Abierto' then begin
           btnCerrarCorte.OptionsImage.ImageIndex := 10;
           btnCerrarCorte.Caption:='Cerrar corte';      // Espacios necesarios para colocar icono a la izq
           btnCerrarCorte.Hint := 'Cerrar corte(Alt+C)';
           btnSalidaVisitante.Enabled:=true;
         end
         else begin
           btnCerrarCorte.OptionsImage.ImageIndex := 9;
           btnCerrarCorte.Caption:='Abrir corte';
           btnCerrarCorte.Hint := 'Abrir corte(Alt+A)';
           btnSalidaVisitante.Enabled:=false;
         end;
     end;
end;

procedure TTressShell.CargaCasetaActiva;
begin
     RefrescaCasetaActiva;
end;

procedure TTressShell.RefrescaCasetaActiva;
begin
     with dmVisitantes.cdsCaseta do
     begin
          if dmVisitantes.cdsCaseta.State in [dsBrowse] then begin
            with dmCliente.cdsUsuario do begin
              if State = dsInactive then
                open;
              Locate('US_CODIGO', dmCliente.Usuario, []);
            end;
            PanelCaseta.Caption := Format( '%s en %s', [ dmCliente.GetDatosUsuarioActivo.Nombre,
                                                       FieldByName('CA_NOMBRE').AsString ] );
          end;
     end;
end;

function TTressShell.EscogeCaseta( lSetCasetaDefault: Boolean ): Boolean;
var
   oCursor: TCursor;
   iCasetas: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        iCasetas := dmVisitantes.InitCaseta( FLeeRegistry );
     finally
            Screen.Cursor := oCursor;
     end;


     case iCasetas of
          0:
          begin
               zError( '� No hay casetas !', 'No hay caseta registradas en el sistema', 0 );
               Result := False;
          end;
          1:
          begin
               if lSetCasetaDefault then
                  Result := FEscogeCaseta.EscogeUnaCaseta( lSetCasetaDefault )
               else
                   Result := True;
          end
          else
              Result := FEscogeCaseta.EscogeUnaCaseta( lSetCasetaDefault );
     end;
end;
procedure TTressShell.RefrescaShell;

     procedure HabilitaBotonesImpConsul;
     var
        bHayDatosLibro:boolean;
     begin
          bHayDatosLibro := (dsLibro.DataSet.Active) and (dsLibro.DataSet.RecordCount > 0);
          btnInfoLibro.Enabled := bHayDatosLibro;
          btnImpLibro.Enabled := bHayDatosLibro;
     end;

begin
     CargaLibro;
     ApplyMinWidth;
     cxGrid1DBBandedTableView1.ApplyBestFit();
     cxGrid2DBTableView1.ApplyBestFit();
     RefrescaShellVisitas;
     ActualizaBtnCerrar;
     RefrescaCasetaActiva;
     HabilitaBotonesImpConsul;
end;

procedure TTressShell.RefrescaShellVisitas;
begin
     LI_FOLIO.Width := LI_FOLIO.Width  + 40;
     VI_FOTO.Width := 80;
     LI_ENT_HOR.Width := LI_ENT_HOR.Width + 40;
     US_ENT_LIB.Width := US_ENT_LIB.Width + 10;
     LI_SAL_HOR.Width := LI_SAL_HOR.Width  + 40;
     US_SAL_LIB.Width := US_SAL_LIB.Width + 10;
     Identificacion.Width := Identificacion.Width + 40;
end;

procedure TTressShell.RegistrarEntrada;
begin
     cxGridCitas.Enabled := false;
     FRegistroActivo := true;

     //Crea y muestra forma de busqeuda
     FBusquedaVisit.CreaBusquedaVisitante;
     //Invoca busqueda
     if FBusquedaVisitante <> nil then
     begin
          if not (FBusquedaVisitante.CancelaBusqueda) then
          begin
               //Muestra forma de Registro para visitante nuevo
              if FRegistroEntradaVisit <> nil then
              begin
                   FRegistroEntradaVisit.ShowModal;
                   FRegistroEntradaVisit.sPNombreVisiNuevo := '';
                   FRegistroEntradaVisit.sPApePatVisiNuevo := '';
              end;
              //Se grabe el registo o no, se refrescara el shell
              RefrescaShell;
              if (not bEsRegistroGrupo) and (not FBusquedaVisitante.bCerrarBusqueda)then
                 LimpiaDatosGrupo;
              cxGridCitas.Enabled := true;
              FRegistroActivo := false;
              CargaCitas;
          end;
     end;
end;


procedure TTressShell.RegistrarEntradaGrupo;
begin
     {***Ciclo para agregar Grupo***}
     Repeat
           RegistrarEntrada;
     Until (bEsRegistroGrupo = false) or (FBusquedaVisitante.CancelaBusqueda);
end;

procedure TTressShell.CargaCitas;
 var
    oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        dmVisitantes.CargaCitas;
        bEntradaCita.Enabled := HayCitas; //and dmVisitantes.CorteActivo;      DACP
        cxGridCitas.Enabled := HayCitas;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell.CargaLibro;
begin
     dmVisitantes.CargaLibro;
     HabilitaControlesLibro;
end;

procedure TTressShell.CargaTraducciones;
begin
     inherited;
     DevEx_cxLocalizer.Active := True;
     DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)
end;

procedure TTressShell._Corte_PrimeroExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmVisitantes do
        begin
             if GetCortePrimero then
             begin
                  CargaCorteActivo;
                  RefrescaShell;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Corte_AnteriorExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmVisitantes do
        begin
             if GetCorteAnterior then
             begin
                  CargaCorteActivo;
                  RefrescaShell;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Corte_SiguienteExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try
        with dmVisitantes do
        begin
             if GetCorteSiguiente then
             begin
                  CargaCorteActivo;
                  RefrescaShell;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TTressShell._Corte_UltimoExecute(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Application.ProcessMessages;
     try

        with dmVisitantes do
        begin
             if GetCorteUltimo then
             begin
                  CargaCorteActivo;
                  RefrescaShell;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;
procedure TTressShell._Corte_PrimeroUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmVisitantes.CorteDownEnabled;
end;

procedure TTressShell._Corte_UltimoUpdate(Sender: TObject);
begin
     inherited;
     TAction( Sender ).Enabled := EmpresaAbierta and dmVisitantes.CorteUpEnabled;
end;

procedure TTressShell.TimerCitasTimer(Sender: TObject);
begin
     inherited;
     {dsCitas.DataSet.Close;
     dsCitas.DataSet.Open;}
     if (EmpresaAbierta) and (not FRegistroActivo) then
     begin
          CargaCitas;
          if (dsCitas.DataSet.RecordCount > 0) and (btnEntradaVisitante.Enabled)then
          begin
               bEntradaCita.Enabled:=true;
               cxGridCitas.Enabled := true;
          end;
     end;
end;

procedure TTressShell.US_ENT_LIBCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
begin
     inherited;
     NoBorderEnTopCustomDrawCell(Sender,ACanvas, AViewInfo,ADone);
end;

procedure TTressShell._Refrescar_CitasExecute(Sender: TObject);
begin
     inherited;
     CargaCitas;
end;

procedure TTressShell._Refrescar_LibroExecute(Sender: TObject);
begin
     inherited;
     CargaLibro;
end;

procedure TTressShell._Cambiar_CasetaExecute(Sender: TObject);
begin
     inherited;
     FLeeRegistry := FALSE;
     try
        DoOpenAll;
     finally
            FLeeRegistry := TRUE;
     end;
end;

procedure TTressShell._Abrir_LibroExecute(Sender: TObject);
begin
     inherited;
     AbrirCorte;
end;

procedure TTressShell.DoLogout;
var
  iUsuario:integer;
begin
     iUsuario:= dmCliente.Usuario;
     if Login( False ) then
     begin
          CierraTodo; { Cierra Empresa Activa }
          Logout;     { Logout al Usuario Activo }
          Application.ProcessMessages;
          dmCliente.Usuario:= iUsuario;
          AbreEmpresa( False );
          if EmpresaAbierta then begin
            dmVisitantes.AbrirLibro;
            CargaCorteActivo;
          end;
          RefrescaShell;
     end
     else
     begin
          //ZetaDialogo.ZInformation( '� Atenci�n !', 'Error al entrar al sistema', 0 );
          //Application.Terminate;
          with dmCliente do begin
            Usuario:= iUsuario;
          end;
       //  _Corte_UltimoExecute(Self);
     end;
end;


procedure TTressShell.AbrirCorte;
begin
     DoLogout;
end;

procedure TTressShell.CerrarCorte;
begin
     dmVisitantes.CerrarLibro;
     CargaCorteActivo;
     RefrescaShell;
end;

{procedure TTressShell.CMChildKey(var Message: TCMChildKey);
begin
     if Message.CharCode = VK_RETURN then begin
        SelectNext(Screen.ActiveControl, not Bool(GetKeyState(VK_SHIFT) and $80), True);
     Message.Result := 1;
     end else
         inherited;
end;}

procedure TTressShell.cxButton4Click(Sender: TObject);
begin
     inherited;
  if Panel1.Width > 100 then
  begin
       Panel1.Width := 30;
       cxButton6.Left:= 3;
       cxButton6.OptionsImage.ImageIndex:=14;
       Panel4.Caption:='';
       cxGridCitas.Visible:= false;
       Splitter1.Visible:= false;
       //cxButton4.Hint:='Mostrar citas';
       panel2.Visible := false;
  end
  else
  begin
       Panel1.Width := 207;
       cxButton6.OptionsImage.ImageIndex:=15;
       Panel4.Caption:=' Pr�ximas citas';
       cxGridCitas.Visible:= true;
       cxButton6.Left:= cxGridCitas.Width - 30;
       Splitter1.Visible:= true;
       cxButton6.Hint:='Ocultar citas';
       panel2.Visible := true;
  end;

end;

procedure TTressShell.cxButton5Click(Sender: TObject);
var
   iFolio: integer;
begin
     inherited;
     if ( PageControl.ActivePage = tsLibro ) then
     begin
          if BusquedaLibro = NIL then
             BusquedaLibro := TBusquedaLibro.Create( self );

          with dmVisitantes do
          begin
               BusquedaLibro.ShowModal;
               if BusquedaLibro.ModalResult = mrOk then
               begin
                    if cdsLibroBusqueda.Active then
                    begin
                         if cdsLibros.Locate( 'LI_FOLIO', iFolio, [] ) then begin
                           if cxGrid1.Visible then
                              cxgrid1.SetFocus
                            else cxGrid2.SetFocus;
                         end
                         else
                             ZetaDialogo.ZInformation( Caption, 'El visitante no se encuentra en el corte activo', 0 );
                    end;
               end;
          end;
     end;
end;

procedure TTressShell.cxGrid1DBBandedTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
     inherited;
    // TimerAlertas.Enabled :=false;
     dmVisitantes.cdsLibros.Modificar;
     //TimerAlertas.Enabled:=true;
end;

procedure TTressShell.cxGrid1DBBandedTableView1ColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if cxGrid1DBBandedTableView1.DataController.DataModeController.GridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;

procedure TTressShell.cxGrid1DBBandedTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if  (AViewInfo.GetNamePath = 'LI_NOMBRE') then
     begin
          with cxGrid1DBBandedTableView1.DataController do
          begin
               if (AViewInfo.GridRecord.Values[5] <> null) and (AViewInfo.GridRecord.Values[6] <> null) and (AViewInfo.Item = LI_NOMBRE) then
               if  TiempoExcede( AViewInfo.GridRecord.Values[5], AViewInfo.GridRecord.Values[6] ) then
                   ACanvas.Font.Color := RGB(194, 0, 0);
          end;
     end;

end;

procedure TTressShell.cxGrid1DBBandedTableView1DataControllerSortingChanged(
  Sender: TObject);
begin
  if cxGrid1DBBandedTableView1.DataController.DataModeController.GridMode then
  begin
    inherited;
  //  cxGrid1.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
  end;
end;

procedure TTressShell.cxGrid1DBBandedTableView1DblClick(Sender: TObject);
begin
  inherited;
//  dmVisitantes.cdsLibros.Modificar;
end;

procedure TTressShell.cxGrid2DBTableView1CellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
     inherited;
     dmVisitantes.cdsLibros.Modificar;
end;

procedure TTressShell.cxGrid2DBTableView1ColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if cxGrid2DBTableView1.DataController.DataModeController.GridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;

procedure TTressShell.cxGrid2DBTableView1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     if(dmVisitantes.AlertasEnabled) then
     begin
          with cxGrid2DBTableView1.DataController do
          begin
               if (AViewInfo.GridRecord.Values[0] <> null) and (AViewInfo.GridRecord.Values[1] <> null) then
               if TiempoExcede( AViewInfo.GridRecord.Values[0], AViewInfo.GridRecord.Values[1] ) then
                  ACanvas.Font.Color := RGB(194, 0, 0);
          end;
     end;
end;

procedure TTressShell.cxGrid2DBTableView1DataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
   AIndex: Integer;
begin
     inherited;
     AIndex := AValueList.FindItemByKind(fviCustom);
     if AIndex <> -1 then
        AValueList.Delete(AIndex);

end;

procedure TTressShell.cxGrid2DBTableView1DblClick(Sender: TObject);
begin
     inherited;
     //  dmVisitantes.cdsLibros.Modificar;
end;

procedure TTressShell.cxGrid3DBTableView1Column2CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   APainter: TcxPainterAccess;
begin
end;

procedure TTressShell.cxGridCitasDBBandedTableView1DblClick(Sender: TObject);
begin
  inherited;
  bEntradaCitaClick(Self);
end;

procedure TTressShell.cxGridCitasResize(Sender: TObject);
begin
  inherited;
  cxGridCitasDBBandedTableView1.Bands[0].Width := cxGridCitas.Width - 3;
end;

procedure TTressShell.btnCambioCasetaClick(Sender: TObject);
begin
     inherited;
     {EscogeCaseta(true);
     RefrescaCasetaActiva;}
     FLeeRegistry := FALSE;
     try
        DoOpenAll;
     finally
            FLeeRegistry := TRUE;
     end;
end;

procedure TTressShell.btnCerrarCorteClick(Sender: TObject);
begin
  inherited;
   if btnCerrarCorte.Caption = 'Cerrar corte' then
    CerrarCorte
  else AbrirCorte;
end;

procedure TTressShell._Cerrar_LibroExecute(Sender: TObject);
begin
     inherited;
     CerrarCorte;
end;

procedure TTressShell._Registrar_EntradaExecute(Sender: TObject);
begin
     FPrimerRegGrupo := false;
     RegistrarEntrada;
end;

procedure TTressShell._Registrar_SalidaExecute(Sender: TObject);
begin
     inherited;
     dmVisitantes.RegistraSalida;
     RefrescaShell;
end;

procedure TTressShell.BuscarBtnClick(Sender: TObject);
var
   iFolio: integer;
begin
     inherited;
     if ( PageControl.ActivePage = tsLibro ) then
     begin
     if BusquedaLibro = NIL then
        BusquedaLibro := TBusquedaLibro.Create( self );

     with dmVisitantes do
     begin
          BusquedaLibro.ShowModal;
          if BusquedaLibro.ModalResult = mrOk then
          begin
               if cdsLibroBusqueda.Active then
               begin
                    iFolio := cdsLibroBusqueda.FieldByName('LI_FOLIO').AsInteger;
                    if cdsLibros.Locate( 'LI_FOLIO', iFolio, [] ) then
                    begin
                         if cxGrid1.Visible then
                            cxgrid1.SetFocus
                         else cxGrid2.SetFocus;
                    end
               else
                   ZetaDialogo.ZInformation( Caption, 'El visitante no se encuentra en el corte activo', 0 );
          end;
      end;
    end;
  end;
end;


procedure TTressShell.ImprimeForma( const lValidaImpresion: Boolean );
 var
    iReporte: integer;
 procedure Imprime;
 begin
     with dmVisitantes do
     begin
          dmReportes.ImpresoraDefault := ImpresoraDefault;

          if iReporte = 0 then
             ZImprimeForma.ImprimeUnaForma( enLibro, cdsLibros )
          else
              ZImprimeForma.ImprimeUnaForma( enLibro, cdsLibros, iReporte, tgImpresora );
     end;
 end;

begin
     inherited;
     iReporte := dmVisitantes.cdsCaseta.FieldByName('RE_CODIGO').AsInteger;
     if lValidaImpresion then
     begin
          case dmVisitantes.ImpresionGafete of
               igNoImprimir:; //No hace Nada
               igImprimirPreguntando:
               begin
                    if ZetaDialogo.ZConfirm( 'Impresi�n de Gafete', '� Desea imprimir el gafete ?', 0, mbYes ) then
                       Imprime;
               end;
               igImprimirSinPreguntar: Imprime;
          end;
     end
     else
         Imprime;
end;

procedure TTressShell._Imprime_una_formaExecute(Sender: TObject);
begin
     if btnImpLibro.Enabled then
        ImprimeForma(False);
end;

procedure TTressShell.GridLibroDblClick(Sender: TObject);
begin
     inherited;
     {if (dmVisitantes.cdsLibros.FieldByName('LI_SAL_COR').AsInteger = 0 ) then
     begin
          dmVisitantes.RegistraSalida;
          RefrescaShell;
     end
     else}
     dmVisitantes.cdsLibros.Modificar;
end;

procedure TTressShell._Menu_AdministradorExecute(Sender: TObject);
begin
     inherited;
     with ClientRegistry do
     begin
          if MenuConfigura = NIL then
             MenuConfigura := TMenuConfigura.Create( self );
          with MenuConfigura do
          begin
               ShowModal;
               Case Operacion of
                    1: EscogeCaseta( TRUE );
                    2: ShowConfiguracion;
                    3: ShowConfiguraRegistro;
               end;
          end;
     end;
end;

procedure TTressShell.ShowConfiguracion;
begin
     {   Se cambi� porque no estaba refrescando el registry
     if Configura = NIL then
        Configura := TConfigura.Create( self );
     Configura.ShowModal; }
     Configura := TConfigura.Create( self );
     with Configura do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TTressShell.ShowConfiguraRegistro;
begin
     if ConfiguraAutoRegistro = NIL then
        ConfiguraAutoRegistro := TConfiguraAutoRegistro.Create( self );
     ConfiguraAutoRegistro.ShowModal;
end;




procedure TTressShell.Splitter1Moved(Sender: TObject);
begin
  inherited;
  if Panel1.Width <= 145 then
    Panel1.Width := 146;
end;

procedure TTressShell.tbCambioCasetaClick(Sender: TObject);
begin
     inherited;
     EscogeCaseta(true);
     RefrescaCasetaActiva;
end;

function TTressShell.TiempoExcede(sLiEntHor, sLiSalHor: string): boolean;
begin
     if (sLiEntHor = null) or (sLiSalHor = null) then
        exit;
     with dmVisitantes do
     begin
          Result := NOT AlertasEnabled;
          if NOT Result then
          begin
               Result := StrVacio( sLiSalHor ) AND
               (( aMinutos(HoraAsStr(Time)) - aMinutos(sLiEntHor )) > TiempoMinimo );
          end;
     end;
end;

procedure TTressShell.Timer1Timer(Sender: TObject);
var
   today : TDateTime;
begin
     today := Time;
     PanelLibro.Caption := TimeToStr(today);
     inherited;
end;

procedure TTressShell.TimerAlertasTimer(Sender: TObject);
begin
     inherited;
     with dmVisitantes.cdsAlertas do
     begin
          if dmVisitantes.RevisaAlertas then
          begin
               Application.Restore;
               FlashWindow(Application.Handle,TRUE);
          end;
     end;
end;

procedure TTressShell.Button1Click(Sender: TObject);
begin
     inherited;
     dmVisitantes.RevisaAlertas;
end;

procedure TTressShell._Ver_InformacionExecute(Sender: TObject);
begin
     inherited;
     if btnImpLibro.Enabled then
        dmVisitantes.cdsLibros.Modificar;
end;

procedure TTressShell._Ver_totalesExecute(Sender: TObject);
begin
  inherited;
   inherited;
  if FTableroTots = nil then
    FTableroTots :=  TFTableroTots.Create(Self);
  FTableroTots.ShowModal;
end;

procedure TTressShell._Registrar_LlegadaExecute(Sender: TObject);
begin
     inherited;
     dmVisitantes.RegistraLlegada;
end;

procedure TTressShell.dsCitasDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     bEntradaCita.Enabled:= btnEntradaVisitante.Enabled and HayCitas;
end;

function TTressShell.HayCitas: Boolean;
begin
     with dsCitas do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := not Dataset.IsEmpty;
     end;
end;

procedure TTressShell.ActualizaBtnCerrar;
begin
     with dmVisitantes do begin
     if cdsCortes.State in [dsBrowse] then
     if ( cdsCortes.FieldByName('CO_FOLIO').AsInteger = UltimoCorteAbierto ) OR (UltimoCorteAbierto = 0 ) then
        btnCerrarCorte.Enabled := true
     else
     if lbCorteStatus.Caption = 'Abierto' then
        btnCerrarCorte.Enabled:= true
     else btnCerrarCorte.Enabled:=  false;
  end;
end;

procedure TTressShell.ActualizaConfiguracionVisitantes;
begin
     cxGrid1.Visible := dmVisitantes.MostrarGridAvanzado;
     cxGrid2.Visible := not dmVisitantes.MostrarGridAvanzado;
     btnRegistroGrupo.Visible := dmVisitantes.MostrarRegistroGrupo;
     if dmVisitantes.MostrarGridAvanzado then
        cxGrid1.Align := alClient
     else cxGrid2.Align := alClient;
     HabilitaControlesCita;
end;

procedure TTressShell.AplicaGridMode;
begin
     {***(am):Trabaja CON GridMode***}
     if cxGrid1DBBandedTableView1.DataController.DataModeController.GridMode then
        cxGrid1DBBandedTableView1.OptionsCustomize.ColumnFiltering := False;

     //Desactiva la posibilidad de agrupar
     cxGrid1DBBandedTableView1.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     cxGrid1DBBandedTableView1.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     cxGrid1DBBandedTableView1.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     cxGrid1DBBandedTableView1.FilterBox.CustomizeDialog := False;
     //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     cxGrid1DBBandedTableView1.ApplyBestFit();

     //Desactiva la posibilidad de agrupar
     cxGrid2DBTableView1.OptionsCustomize.ColumnGrouping := true;
     //Esconde la caja de agrupamiento
     cxGrid2DBTableView1.OptionsView.GroupByBox := false;
end;

procedure TTressShell.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  cxGrid1DBBandedTableView1 do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;
end;

procedure TTressShell.bCambiarCasetaClick(Sender: TObject);
begin
     inherited;
     EscogeCaseta(true);
     RefrescaCasetaActiva;
end;

procedure TTressShell.bEntradaCitaClick(Sender: TObject);
const
     K_MENSAJE = 'El visitante ya tiene un registro de entrada,'+#13+'para poder continuar es necesario registrar primero la salida.';
     K_MENSAJE_CORTE = 'No se pueden agregar citas en un corte cerrado, abra un corte e intente de nuevo.';
var
   iVisit:integer;
   iAnfi:integer;
   sObserva:string;
   sAsunto:string;
begin
     if ( dmVisitantes.CorteActivo ) then
     begin
           iVisit := StrToInt(cxGridCitasDBBandedTableView1.Controller.FocusedRow.Values[cxGridCitasDBBandedTableView1.Columns[1].Index]);
           iAnfi := StrToInt(cxGridCitasDBBandedTableView1.Controller.FocusedRow.Values[cxGridCitasDBBandedTableView1.Columns[2].Index]);
           if cxGridCitasDBBandedTableView1.Controller.FocusedRow.Values[cxGridCitasDBBandedTableView1.Columns[7].Index] <> null then
              sObserva := cxGridCitasDBBandedTableView1.Controller.FocusedRow.Values[cxGridCitasDBBandedTableView1.Columns[7].Index];
           if cxGridCitasDBBandedTableView1.Controller.FocusedRow.Values[cxGridCitasDBBandedTableView1.Columns[8].Index] <> vacio then
              sAsunto := cxGridCitasDBBandedTableView1.Controller.FocusedRow.Values[cxGridCitasDBBandedTableView1.Columns[8].Index];
           if dmVisitantes.cdsLibros.Locate('VI_NUMERO;LI_SAL_HOR', VarArrayOf([iVisit,'']),[]) then
              ZetaDialogo.ZInformation( 'Registro de entrada', K_MENSAJE, 0 )
           else
           begin
                if dsCitas.DataSet.FieldByName('CI_STATUS').AsInteger = 0 then
                   ZInformation('Registrar llegada','No es posible registrar llegada. El visitante ya lleg� a su cita',0)
                else if dsCitas.DataSet.FieldByName('CI_STATUS').AsInteger = 2 then
                     ZInformation('Registrar llegada','No es posible registrar llegada. La cita ha sido cancelada',0)
                else
                begin


                     ActualizaConfiguracionVisitantes;

                     with dmVisitantes do
                     begin
                          OPCSQL := 2;
                          FiltroSQL := quotedstr(IntToStr(iVisit));
                          if not cdsCitasSQL.Active then
                          begin
                               cdsCitasSQL.Conectar;
                               cdsCitasSQL.Active := true;
                          end;
                          cdsCitasSQLAlAdquirirDatos(Self);
                     end;

                     if FRegistroEntradaVisit = nil then
                        FRegistroEntradaVisit := TFRegistroEntradaVisit.Create(self); //Antes estaba como nil, y marcaba Access Violation al cerrar la aplicacion.
                     FRegistroEntradaVisit.EsRegistroCita:=true;
                     cxGridCitas.SetFocus;
                     with dmVisitantes.cdsLibros do
                     begin
                          if not ( State in [dsInsert] ) then begin
                             Append;
                          end;
                     end;
                     if iVisit >= 0 then
                        FRegistroEntradaVisit.iVisiNumero:= iVisit;
                     if iAnfi >= 0 then
                        TressShell.iAnfiNumero:= iAnfi;
                     TressShell.sPVisiObserva := sObserva;
                     TressShell.sPAsunto := sAsunto;

                     dmVisitantes.RegistraLlegada;

                     FRegistroEntradaVisit.iVisiNumero:= -1;
                     TressShell.iAnfiNumero:= -1;
                     TressShell.sPVisiObserva := '';
                     TressShell.sPAsunto := '';
                end;
           end;
     end
     else
     begin
          ZInformation('Registrar llegada',K_MENSAJE_CORTE,0)
     end;
end;

procedure TTressShell.BtnGafeteClick(Sender: TObject);
begin
     inherited;
     if ( dmVisitantes.CorteActivo AND eGafeteSalida.CanFocus ) then
     begin
          dmVisitantes.RegistraSalidaGafete( eGafeteSalida.Text );
          RefrescaShell;
     end;
end;

procedure TTressShell.BtnGafeteEnter(Sender: TObject);
begin
     inherited;
     if btnEntradaVisitante.Enabled then
        eGafeteSalida.SetFocus;
end;


procedure TTressShell.btnRefrescaCitasClick(Sender: TObject);
begin
     inherited;
     CargaCitas;
end;

procedure TTressShell._Registrar_EntradaGrupoExecute(Sender: TObject);
begin
     inherited;
     LimpiaDatosGrupo;
     bEsRegistroGrupo := true;
     if FDatosVisitanteGrupo = nil then
        FDatosVisitanteGrupo := TFDatosVisitanteGrupo.Create(Self);
     FDatosVisitanteGrupo.sPNumero := '0';
     TressShell.FPPrimerRegGrupo := true;
     RegistrarEntradaGrupo;
end;

procedure TTressShell.btnSalidaVisitanteClick(Sender: TObject);
begin
     inherited;
     dmVisitantes.RegistraSalida;
     RefrescaShell;
end;

procedure TTressShell.NoBorderEnMedioCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   ARect: TRect;
begin
     with AViewInfo do
     begin
          AViewInfo.Borders := [];
          ARect := Bounds;
          ACanvas.FillRect(ARect);
          if (AViewInfo.Item <> LI_ENT_HOR) and (AViewInfo.Item <> Identificacion)
          and (AViewInfo.Item <> LI_CAR_PLA) and (AViewInfo.Item <> LI_SAL_HOR)
          and (AViewInfo.Item <> De)then
              ACanvas.DrawTexT(Text, TextAreaBounds, cxAlignVCenter);

          if (AViewInfo.Item = De)then
          begin
               ACanvas.DrawTexT('De:', TextAreaBounds, cxAlignVCenter);
          end
          else
              ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bRight], 1);
          ADone := True;
     end;
end;

procedure TTressShell.NoBorderEnTopCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   ARect: TRect;
begin
     with AViewInfo do
     begin
          AViewInfo.Borders := [];
          ARect := Bounds;
          ACanvas.FillRect(ARect);
          if (AViewInfo.Item <> LI_ENT_HOR) and (AViewInfo.Item <> Identificacion) and (AViewInfo.Item <> LI_CAR_PLA)
          and (AViewInfo.Item <> LI_FOLIO) and (AViewInfo.Item <> LI_SAL_HOR)
          and (AViewInfo.Item <> VisitaA) and (AViewInfo.Item <> De)then
          begin
               ACanvas.DrawTexT(Text, TextAreaBounds, cxAlignVCenter);
          end;
          ADone := True;
     end;

     if(AViewInfo.Item = LI_ENT_HOR)
     or (AViewInfo.Item = Identificacion) {or {(AViewInfo.Item = LI_FOLIO)}
     or (AViewInfo.Item = LI_SAL_HOR)then
        PaintIcons(Sender,ACanvas,AViewInfo, ADone);

     {   if (AViewInfo.Item = Relleno) then
     ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [ bRight, bTop], 1);}
     if (AViewInfo.Item = LI_GAFETE) then
        ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [ bRight, bBottom], 1);
     if (AViewInfo.Item <> CI_HORA)then
        ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bRight], 1);
end;

procedure TTressShell.NoBorderEnBottomCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   ARect: TRect;
   bTiempoExcede:boolean;
begin
     bTiempoExcede:=false;
     with AViewInfo do
     begin
          AViewInfo.Borders := [];
          ARect := Bounds;
          ACanvas.FillRect(ARect);

          if (AViewInfo.Item = US_SAL_LIB) and (AViewInfo.Text ='Tiempo excedido') then
             bTiempoExcede:= true;

          if (AViewInfo.Item <> LI_ENT_HOR)and (AViewInfo.Item <> VisitaA) and (AViewInfo.Item <> DEPTO)
          and (AViewInfo.Item <> Identificacion) and (AViewInfo.Item <> LI_CAR_PLA) and (AViewInfo.Item <> De)
          {and (AViewInfo.Item <> LI_FOLIO)} and (AViewInfo.Item <> LI_SAL_HOR)and (AViewInfo.Item <> LI_GAFETE)
          and (not  bTiempoExcede) then
              ACanvas.DrawTexT(Text, TextAreaBounds, cxAlignVCenter);

          ADone := True;

          if (AViewInfo.Item = LI_CAR_PLA){or (AViewInfo.Item = LI_FOLIO)}or(bTiempoExcede)then
             PaintIcons(Sender,ACanvas,AViewInfo, ADone);


          if (AViewInfo.Item = VisitaA)then
          begin
               ACanvas.DrawTexT('Visita a:', TextAreaBounds, cxAlignVCenter);
               ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bBottom], 1);
          end
          else
              ACanvas.DrawComplexFrame(ARect, AViewInfo.BorderColor[bRight], AViewInfo.BorderColor[bLeft], [bRight, bBottom], 1);
     end;
end;

{$ifdef TEST_COMPLETE}
function TTressShell.GetInformacionGridLibroDetalle: String;
const
     K_GRID_LIBRO_DETALLE = '|LI_ENT_HOR| |US_ENT_LIB| |LI_SAL_HOR| |US_SAL_LIB| |LI_NOMBRE| |LI_EMPRESA| |LI_ANFITR| |LI_DEPTO| |ASUNTO| |Identificacion| |LI_CAR_PLA| |LI_GAFETE|';
begin
     result := '';
     result := GetDatosGridTestComplete(cxGrid2DBTableView1.DataController.DataSet, K_GRID_LIBRO_DETALLE);
end;

function TTressShell.GetInformacionGridCitas: String;
var
  sStatus:string;
begin
     Result := '';
     with cxGridCitasDBBandedTableView1.DataController.DataSet do
     begin
          first;
          while not eof do
          begin
               if fieldByName('CI_STATUS').AsString = '0' then
                  sStatus := 'Si'
               else
                  sStatus := 'No';
               result := result + Trim( fieldByName('CI_HORA').AsString ) + K_PIPE +
                                    Trim( sStatus ) + K_PIPE +
                                    Trim( fieldByName('VI_NOMBRES').AsString )+' '+Trim( fieldByName('VI_APE_PAT').AsString ) +' '+
                                    Trim( fieldByName('VI_APE_MAT').AsString )+ K_PIPE +
                                    Trim( fieldByName('EV_NOMBRE').AsString ) + K_PIPE +
                                    Trim( ObtenerNombreAnfi(fieldByName('AN_NUMERO').AsInteger) ) + K_PIPE +CR_LF;
               Next;
          end;
          Result := result + CR_LF;
      end;
end;

function TTressShell.GetInformacionGridLibro: String;
const
  K_GRID_LIBRO = '|LI_ENT_HOR| |LI_SAL_HOR| |US_ENT_LIB| |US_SAL_LIB| |LI_NOMBRE| |LI_EMPRESA| |LI_ANFITR| |LI_DEPTO| |ASUNTO| |LI_GAFETE|';
begin
     result := '';
     result := GetDatosGridTestComplete(cxGrid2DBTableView1.DataController.DataSet, K_GRID_LIBRO);
end;

function TTressShell.GetDatosGridTestComplete(const ds: TDataSet;
  const sCampos: string): string;
var
  sInfoGrid:string;
  sListaCampos:TStringList;
  i:Integer;
begin
     sListaCampos := TStringList.Create;
     sListaCampos.Delimiter := ' ';
     sListaCampos.QuoteChar := '|';
     sListaCampos.DelimitedText := sCampos;
     Result := '';
     sInfoGrid := '';
      with ds do
      begin
           first;
           while not eof do
           begin
                for i := 0 to sListaCampos.Count-1 do
                   sInfoGrid := sInfoGrid + Trim( fieldByName(sListaCampos[i]).AsString ) + K_PIPE;

                sInfoGrid := sInfoGrid + CR_LF;
                Next;
          end;
          Result := sInfoGrid + CR_LF;
      end;
end;

function TTressShell.ObtenerNombreAnfi(iAnfi: integer): string;
begin
     with dmVisitantes.cdsAnfitrion do
     begin
          if Locate('AN_NUMERO', iAnfi, []) then
          begin
               Result := FieldByName('AN_NOMBRES').AsString +' '+ FieldByName('AN_APE_PAT').AsString+
                         ' '+fieldByName('AN_APE_MAT').AsString;
          end;
     end;
end;

{$endif}

end.
