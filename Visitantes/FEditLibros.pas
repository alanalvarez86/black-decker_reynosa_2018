unit FEditLibros;

interface

uses     
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ComCtrls, Mask, ZetaKeyCombo, ZetaKeyLookup, ZetaHora, ZetaFecha,
  ZetaSmartLists, ZBaseEdicion, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, TressMorado2013, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, Vcl.ImgList,
  cxNavigator, cxDBNavigator, cxButtons, ZetaKeyLookup_DevEx,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxPC, dxGDIPlusClasses, imageenview,
  ieview, imageen, cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit,ZetaClientDataSet;

type
  TEditLibros = class(TBaseEdicion_DevEx)
    PageControl1: TcxPageControl;
    tsGeneral: TcxTabSheet;
    tsIO: TcxTabSheet;
    GroupBox3: TGroupBox;
    lblFecEnt: TLabel;
    lblHorEnt: TLabel;
    lblEntCas: TLabel;
    lblRegEntrada: TLabel;
    lblCorteEntrada: TLabel;
    LI_ENT_COR: TZetaDBTextBox;
    tsVehiculo: TcxTabSheet;
    LI_ASUNTO: TZetaDBKeyLookup_DevEx;
    gbOtros: TGroupBox;
    gbObserva: TGroupBox;
    LI_ENT_FEC: TZetaDBTextBox;
    LI_ENT_HOR: TZetaDBTextBox;
    LI_ENT_CAS: TZetaDBTextBox;
    LI_ENT_VIG: TZetaDBTextBox;
    GroupBox4: TGroupBox;
    lblSalFec: TLabel;
    lblHorSal: TLabel;
    lblSalCas: TLabel;
    lblRegSalida: TLabel;
    lblCorteSal: TLabel;
    LI_SAL_COR: TZetaDBTextBox;
    LI_SAL_VIG: TZetaDBTextBox;
    LI_SAL_FEC2: TZetaDBTextBox;
    LI_SAL_HOR2: TZetaDBTextBox;
    LI_SAL_CAS2: TZetaDBTextBox;
    bSalida: TcxButton;
    LI_TIPO_ID: TZetaDBKeyLookup;
    LI_CAR_TIP: TZetaDBKeyLookup;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    pnlTop: TPanel;
    Foto: TImageEn;
    lblStatus: TLabel;
    LI_STATUS: TZetaDBTextBox;
    lblVisitante: TLabel;
    LI_NOMBRE: TDBEdit;
    lblEmpresa: TLabel;
    LI_EMPRESA: TDBEdit;
    lblCita: TLabel;
    CI_FOLIO: TZetaDBTextBox;
    btnVisitantes: TcxButton;
    btnEmpresa: TcxButton;
    lblAnfitrion: TLabel;
    LI_ANFITR: TDBEdit;
    lblDepto: TLabel;
    LI_DEPTO: TDBEdit;
    btnAnfitrion: TcxButton;
    btnDepto: TcxButton;
    lblFolio: TLabel;
    LI_FOLIO: TZetaDBTextBox;
    pnlMedio: TPanel;
    pnlIdentificacion: TPanel;
    cbTipoId: TcxComboBox;
    LI_GAFETE: TDBEdit;
    LI_ID: TDBEdit;
    lblTipoId: TLabel;
    lblGafete: TLabel;
    lblLicId: TLabel;
    Image2: TImage;
    lbIcoIdenti: TLabel;
    icoIdenti: TImage;
    pnlVehiculo: TPanel;
    icoAuto: TImage;
    lbIcoAuto: TLabel;
    Image4: TImage;
    lblCajon: TLabel;
    lblCarro: TLabel;
    lblDescrip: TLabel;
    lblPlacas: TLabel;
    LI_CAR_PLA: TDBEdit;
    cbTipoAuto: TcxComboBox;
    LI_CAR_EST: TDBEdit;
    LI_CAR_DES: TDBEdit;
    pnlObserva: TPanel;
    icoObserva: TImage;
    lbIcoObserva: TLabel;
    Image5: TImage;
    lbObserva1: TLabel;
    lbObserva2: TLabel;
    LI_OBSERVA: TDBMemo;
    lblTexto1: TLabel;
    LI_TEXTO1: TDBEdit;
    lblTexto2: TLabel;
    LI_TEXTO2: TDBEdit;
    lblTexto3: TLabel;
    LI_TEXTO3: TDBEdit;
    Panel1: TPanel;
    Image3: TImage;
    Image6: TImage;
    imgSal: TImage;
    lbEntrada: TLabel;
    lbVigilante: TLabel;
    lbCorte: TLabel;
    lbSalida: TLabel;
    lbCasetaSalida: TLabel;
    lbVigiSalida: TLabel;
    lbVigiCorteSalida: TLabel;
    Image1: TImage;
    lbCaseta: TLabel;
    pnlAsunto: TPanel;
    lblAsunto: TLabel;
    cbAsunto: TcxComboBox;
    procedure FormShow(Sender: TObject);
    procedure btnAnfitrionClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnDeptoClick(Sender: TObject);
    procedure btnVisitantesClick(Sender: TObject);
    procedure btnEmpresaClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure bSalidaClick(Sender: TObject);
    procedure cbTipoIdPropertiesChange(Sender: TObject);
    procedure cbTipoAutoPropertiesChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure cbAsuntoPropertiesChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    arTipoId:array of string;
    arTipoAuto:array of string;
    arTipoAsunto:array of string;
    FPrimerUsoAuto:boolean;
    FPrimerUsoId:boolean;
    FPrimerUsoAsunto:boolean;
    FOnSearch: TSearchEvent;
    iTipoId : integer;
    iTipoAuto : integer;
    iTipoAsunto : integer;

    procedure VerificaControles;
    procedure ChecaGlobales;
    procedure LlenaDatosVisitante;
    procedure LlenaComboTipoId;
    procedure LlenaComboAsunto;
    procedure LlenaComboTipoAuto;
    procedure InicializaValores;
    procedure LlenaDatosEntSal;
    function Search( const sFilter: String; var sKey, sDescription: String; iOpc:integer ): Boolean;
    procedure VerificaCamposNecesarios;
    procedure ActualizaItemsCombos;
  protected
     procedure Connect; override;
     procedure EscribirCambios; override;
     function PuedeAgregar(var sMensaje: String): Boolean; override;
     function PuedeBorrar(var sMensaje: String): Boolean; override;
     function PuedeModificar(var sMensaje: String): Boolean; override;
  public
    { Public declarations }
    property PrimerUsoAuto: Boolean  read FPrimerUsoAuto write FPrimerUsoAuto;
    property PrimerUsoId: Boolean  read FPrimerUsoId write FPrimerUsoId;
    property PrimerUsoAsunto: Boolean  read FPrimerUsoAsunto write FPrimerUsoAsunto;

    property iPTipoId: integer read iTipoId write iTipoId;
    property iPTipoAuto: integer read iTipoAuto write iTipoAuto;
    property iPTipoAsunto: integer read iTipoAsunto write iTipoAsunto;


  end;

var
  EditLibros: TEditLibros;

implementation
uses DVisitantes,
     DSistema,
     DCliente,
     DGlobal,
     FBuscaVisitantes,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZAccesosTress,
     FBuscaAnfitriones,
     ZetaCommonTools,
     ZGlobalTress,
     ZetaDialogo,
     FToolsImageEn,
     ZVisitBusqueda_DevEx;

{$R *.DFM}

{ TEditLibros }
procedure TEditLibros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     InicializaValores;
end;

procedure TEditLibros.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_REG_LIBRO;
     FirstControl :=  LI_NOMBRE;
     with dmVisitantes do
     begin
          LI_ASUNTO.LookupDataset := cdsTipoAsunto;
          LI_TIPO_ID.LookupDataset := cdsTipoID;
          LI_CAR_TIP.LookupDataset := cdsTipoCarro;
     end;
     HelpContext := H_VIS_INF_USUARIOS;
     PrimerUsoAuto:=false;
     PrimerUsoId:=false;
     PrimerUsoAsunto:=false;
     if Busqueda_DevEx = nil then
        Busqueda_DevEx := ZVisitBusqueda_DevEx.TBusqueda_DevEx.Create(Self);
end;

procedure TEditLibros.FormShow(Sender: TObject);
begin
     inherited;
     VerificaControles;
     if dmVisitantes.TieneSalida then
        bSalida.Caption := 'Bo&rrar salida'
     else
        bSalida.Caption := '&Registrar salida';
     LlenaDatosVisitante;

     {if( dmVisitantes.cdsLibros.FieldByName('LI_STATUS').AsInteger = Ord( eLibroStatus( lsDentro ) ) )then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_SAL_FEC').AsDateTime := Date;
               FieldByName('LI_SAL_HOR').AsString := HoraAsStr( Now );
          end;
     end;}
     PageControl1.ActivePageIndex := 0;
     ChecaGlobales;
     LI_EMPRESA.SetFocus;

     //VerificaCamposNecesarios;  //funcionalidad para mostrar datos configurados en caseta
     //DataSource.AutoEdit := FALSE;

     ActualizaItemsCombos;
end;

procedure TEditLibros.InicializaValores;
begin
     PrimerUsoAuto:=false;
     PrimerUsoId:=false;
     PrimerUsoAsunto:=false;
end;

procedure TEditLibros.LlenaComboAsunto;
var
   iContId:integer;
begin
     iContId:=0;
    // cbAsunto.Properties.Items.Clear;        //DACP
     with dmVisitantes.cdsTipoAsunto do
     begin
          first;
          SetLength(arTipoAsunto, RecordCount);
          {if RecordCount <=0 then
              DatabaseError('No existen tipos asunto');}
          while iContId <= RecordCount-1 do
          begin
               cbAsunto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTipoAsunto[iContId]:= FieldByName('TB_CODIGO').AsString;
               inc(iContId);
               next;
          end;
     end;
end;

procedure TEditLibros.LlenaComboTipoAuto;
var
   iContId:integer;
begin
     iContId:=0;
     cbTipoAuto.Properties.Items.Clear;
     with dmVisitantes.cdsTipoCarro do
     begin
          first;
          SetLength(arTipoAuto, RecordCount);
          {if RecordCount <=0 then
              DatabaseError('No existen tipos de auto');}
          while iContId <= RecordCount-1 do
          begin
               cbTipoAuto.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTipoAuto[iContId]:= FieldByName('TB_CODIGO').AsString;
               inc(iContId);
               next;
          end;
     end;
end;

procedure TEditLibros.LlenaComboTipoId;
var
   iContId:integer;
begin
     iContId:=0;
     cbTipoId.Properties.Items.Clear;
     with dmVisitantes.cdsTipoID do
     begin
          first;
          SetLength(arTipoId, RecordCount);
          {if RecordCount <=0 then
              DatabaseError('No existen asuntos');}
          while iContId <= RecordCount-1 do
          begin
               cbTipoId.Properties.Items.Add(FieldByName('TB_ELEMENT').AsString);
               arTipoId[iContId]:= FieldByName('TB_CODIGO').AsString;
               inc(iContId);
               next;
          end;
     end;
end;

procedure TEditLibros.LlenaDatosEntSal;
var
   bVisitanteSalio:boolean;
begin
     lbEntrada.Caption := LI_ENT_HOR.Caption +' '+LI_ENT_FEC.Caption+' en';
     lbVigilante.Caption := 'Registr�: '+ LI_ENT_VIG.Caption;
     lbCaseta.Caption := 'Caseta: '+LI_ENT_CAS.Caption + ', ';
     lbCaseta.Left := lbEntrada.Left + lbEntrada.Width + 5;
     lbCorte.Caption := 'en Corte: '+LI_ENT_COR.Caption;
     lbVigilante.Left := lbCaseta.Left + lbCaseta.Width + 5;
     lbCorte.Left := lbVigilante.Left + lbVigilante.Width + 5;

     if LI_SAL_FEC2.Caption <> VACIO then
     begin
          bVisitanteSalio:= true;
          lbSalida.Caption := LI_SAL_HOR2.Caption +' '+LI_SAL_FEC2.Caption+' en';
          lbVigiSalida.Caption := 'Registr�: '+LI_SAL_VIG.Caption;
          lbCasetaSalida.Caption := 'Caseta: '+LI_SAL_CAS2.Caption+ ', ';
          lbCasetaSalida.Left := lbSalida.Left + lbSalida.Width + 5;
          lbVigiCorteSalida.Caption := 'en Corte: '+LI_SAL_COR.Caption;
          lbVigiSalida.Left := lbCasetaSalida.Left + lbCasetaSalida.Width + 5;
          lbVigiCorteSalida.Left := lbVigiSalida.Left + lbVigiSalida.Width + 5;
     end
     else bVisitanteSalio := false;

     lbSalida.Visible := bVisitanteSalio;
     lbVigiSalida.Visible := bVisitanteSalio;
     lbCasetaSalida.Visible := bVisitanteSalio;
     lbVigiCorteSalida.Visible := bVisitanteSalio;
     imgSal.Visible := bVisitanteSalio;

end;

procedure TEditLibros.LlenaDatosVisitante;
begin
     FToolsImageEn.AsignaBlobAImagen( FOTO, DataSource.DataSet, 'VI_FOTO' );

     LlenaDatosEntSal;
     LlenaComboTipoId;
     LlenaComboTipoAuto;
     LlenaComboAsunto;

     cbTipoId.Text := LI_TIPO_ID.Descripcion;
     cbTipoAuto.Text := LI_CAR_TIP.Descripcion;
     cbAsunto.Text := LI_ASUNTO.Descripcion;
     dmVisitantes.cdsLibros.Cancel;
end;

procedure TEditLibros.OK_DevExClick(Sender: TObject);
begin
  inherited;
  ActualizaItemsCombos;
end;

procedure TEditLibros.ChecaGlobales;
begin
     LI_ANFITR.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_ANFI );
     LI_NOMBRE.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_VISI );
     LI_EMPRESA.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_COMPANY );
     LI_DEPTO.ReadOnly := not Global.GetGlobalBooleano( K_GLOBAL_ABIERTA_DEPTO );
end;

procedure TEditLibros.VerificaCamposNecesarios;
var
   bMuestraIdentifica:boolean;
   bMuestraAutomovil:boolean;
   bMuestraObserva:boolean;
   bMuestraAsunto:boolean;
begin

     with dmVisitantes do
     begin

          bMuestraIdentifica := AutoReg_Identificacion;
          bMuestraAutomovil := AutoReg_Vehiculo;
          bMuestraObserva := AutoReg_Observaciones;
          bMuestraAsunto := AutoReg_AsuntoATratar;
     end;


     pnlAsunto.Visible := bMuestraAsunto;
     if (not bMuestraIdentifica) and (not bMuestraAutomovil) then
        pnlMedio.Visible := false
     else
     begin
          pnlIdentificacion.Visible := bMuestraIdentifica;
          pnlVehiculo.Visible := bMuestraAutomovil;
          pnlMedio.Visible := TRUE;
     end;
     pnlObserva.Visible:= bMuestraObserva;

     self.Height := 599;

     if pnlObserva.Visible = False then
        self.Height := Self.Height - pnlObserva.Height;
     if pnlMedio.Visible = false then
        self.Height := Self.Height - pnlMedio.Height;
     if pnlAsunto.Visible = false then
        self.Height := Self.Height - pnlAsunto.Height;

     self.Height := Self.Height + 15;

     if self.Height <= 310 then
        self.Height := Self.Height + 25;



end;

procedure TEditLibros.VerificaControles;
begin
     //CambiaControles( dmVisitantes.cdsLibros.FieldByName('LI_STATUS').AsInteger = Ord( eLibroStatus( lsDentro ) ) );
end;

procedure TEditLibros.Connect;
begin
     with dmvisitantes do
     begin
          cdsTipoID.Conectar;
          cdsTipoAsunto.Conectar;
          cdsTipoCarro.Conectar;
          //cdsCaseta.Conectar;
          cdsEmpVisitante.Conectar;
          cdsDepto.Conectar;
          DataSource.DataSet:= cdsLibros;
     end;
end;

function TEditLibros.PuedeAgregar(var sMensaje: String): Boolean;
begin
     {Result := False;
     if not Result then
        sMensaje := 'No se pueden agregar registros de libro desde VisitantesMgr';}
end;

function TEditLibros.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     if not Result then
        sMensaje := 'No se pueden borrar registros de libro desde VisitantesMgr';
end;

function TEditLibros.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := dmVisitantes.PuedeModificarLibro;
     if not Result then
        sMensaje := 'No se pueden modificar registros de libro desde Visitantes';

end;


function TEditLibros.Search(const sFilter: String; var sKey,
  sDescription: String; iOpc: integer): Boolean;
begin
     Busqueda_DevEx.bMuestraBotonAgregar := false;
     if Assigned( FOnSearch ) then
        FOnSearch( dmVisitantes.cdsCompaniaLookup, Result, sFilter, sKey, sDescription )
     else
     begin
          if iOpc = 1 then
             Result := ZVisitBusqueda_DevEx.ShowSearchForm( dmVisitantes.cdsCompaniaLookup, sFilter, sKey, sDescription )
          else if iOpc = 2 then
               Result := ZVisitBusqueda_DevEx.ShowSearchForm( dmVisitantes.cdsAnfitrion, sFilter, sKey, sDescription )
          else if iOpc = 3 then
               Result := ZVisitBusqueda_DevEx.ShowSearchForm( dmVisitantes.cdsDepto, sFilter, sKey, sDescription );

     end;
end;

procedure TEditLibros.btnAnfitrionClick(Sender: TObject);
var
   sKey: string;
   sDescription: String;
begin
     inherited;
     if search( VACIO, sKey, sDescription, 2) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_ANFITR').AsString := sDescription;
               FieldByName('AN_NUMERO').AsInteger := strtoint(sKey);
          end;
     end;
end;

procedure TEditLibros.btnDeptoClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if Search( VACIO, sKey, sDescription, 3) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_DEPTO').AsString := sDescription;
               FieldByName('LI_CDEPTO').AsString := sKey;
          end;
     end;
end;

procedure TEditLibros.btnVisitantesClick(Sender: TObject);
var
   iNumero, iEmpresa, iAnfitrion: Integer;
   sDescription, sAsunto, sAnfitrion, sDepto: String;
begin
     inherited;
     if FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_NOMBRE').AsString := sDescription;
               FieldByName('VI_NUMERO').AsInteger := iNumero;
          end;
     end;
     with dmVisitantes do
     begin
          if( iNumero <> 0 )then
          begin
               cdsVisitante.Conectar;
               if cdsVisitante.Locate( 'VI_NUMERO', iNumero, [] ) then
               begin
                    iEmpresa := cdsVisitante.FieldByName('EV_NUMERO').AsInteger;
                    sAsunto := cdsVisitante.FieldByName('VI_ASUNTO').AsString;
                    iAnfitrion := cdsVisitante.FieldByName('AN_NUMERO').AsInteger;
                    if cdsEmpVisitante.Locate( 'EV_NUMERO', iEmpresa, [] ) then
                    begin
                         cdsLibros.FieldByName('EV_NUMERO').AsInteger := iEmpresa;
                         cdsLibros.FieldByName('LI_EMPRESA').AsString := cdsEmpVisitante.FieldByName('EV_NOMBRE').AsString;
                    end;
                    if cdsTipoAsunto.Locate( 'TB_CODIGO', sAsunto, [] ) then
                         cdsLibros.FieldByName('LI_ASUNTO').AsString := cdsTipoAsunto.FieldByName('TB_CODIGO').AsString;
                    cdsAnfitrion.Conectar;
                    if( cdsAnfitrion.Locate( 'AN_NUMERO', iAnfitrion, [] ) )then
                    begin
                         cdsLibros.FieldByName('AN_NUMERO').AsInteger := iAnfitrion;
                         sAnfitrion := Trim( cdsAnfitrion.FieldByName( 'AN_APE_PAT' ).AsString ) + ' ' +
                                         Trim( cdsAnfitrion.FieldByName( 'AN_APE_MAT' ).AsString ) + ', ' +
                                         Trim( cdsAnfitrion.FieldByName( 'AN_NOMBRES' ).AsString ) ;
                         cdsLibros.FieldByName('LI_ANFITR').AsString := sAnfitrion;
                         sDepto := cdsAnfitrion.FieldByName('AN_DEPTO').AsString;
                         if strLleno( sDepto )then
                         begin
                              if( cdsDepto.Locate( 'TB_CODIGO', sDepto, [] ) )then
                              begin
                                   cdsLibros.FieldByName('LI_CDEPTO').AsString := sDepto;
                                   cdsLibros.FieldByName('LI_DEPTO').AsString := cdsDepto.FieldByName('TB_ELEMENT').AsString;
                              end;
                         end;
                    end
                    else
                    begin
                         with cdsLibros do
                         begin
                              FieldByName('AN_NUMERO').AsInteger := 0;
                              FieldByName('LI_ANFITR').AsString := VACIO;
                              FieldByName('LI_CDEPTO').AsString := VACIO;
                              FieldByName('LI_DEPTO').AsString := VACIO;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TEditLibros.Cancelar_DevExClick(Sender: TObject);
begin
     if Cancelar_DevEx.Caption = '&Cancelar' then
     begin
          cbTipoId.ItemIndex := iPTipoId;
          cbTipoAuto.ItemIndex := iPTipoAuto;
          cbAsunto.ItemIndex := iPTipoAsunto;
     end;
     inherited;
     {if DataSource.DataSet.State in [dsEdit, dsInsert] then
        DataSource.DataSet.Cancel end;}
end;

procedure TEditLibros.cbAsuntoPropertiesChange(Sender: TObject);
begin
     inherited;
     try
        if cbAsunto.ItemIndex >= 0 then
           LI_ASUNTO.Llave := arTipoAsunto[cbAsunto.ItemIndex];
        if (not (DataSource.DataSet.State in [dsEdit, dsInsert])){ and (PrimerUsoAsunto)}then
        begin
             DataSource.DataSet.Edit;
             DataSource.DataSet.FieldByName('LI_ASUNTO').AsString := arTipoAsunto[cbAsunto.ItemIndex];
        end;
     except
           // PrimerUsoAsunto:=true;
     end;
    // PrimerUsoAsunto:=true;
end;

procedure TEditLibros.cbTipoAutoPropertiesChange(Sender: TObject);
begin
     inherited;
     try
        if cbTipoAuto.ItemIndex >= 0 then
           LI_CAR_TIP.Llave := arTipoAuto[cbTipoAuto.ItemIndex];
        if (not (DataSource.DataSet.State in [dsEdit, dsInsert])){ and (PrimerUsoAuto)} then
        begin
             DataSource.DataSet.Edit;
             DataSource.DataSet.FieldByName('LI_CAR_TIP').AsString := arTipoAuto[cbTipoAuto.ItemIndex];
        end;
     except
           //PrimerUsoAuto:=true;
     end;
     //PrimerUsoAuto:=true;
end;

procedure TEditLibros.cbTipoIdPropertiesChange(Sender: TObject);
begin
     inherited;

     try
        if cbTipoId.ItemIndex >= 0 then
           LI_TIPO_ID.Llave := arTipoId[cbTipoId.ItemIndex];
        if (not (DataSource.DataSet.State in [dsEdit, dsInsert])){ and (PrimerUsoId)} then
        begin
             DataSource.DataSet.Edit;
             DataSource.DataSet.FieldByName('LI_TIPO_ID').AsString := arTipoId[cbTipoId.ItemIndex];
        end;

        //PrimerUsoId:=true;
     except
         //PrimerUsoId:=true;
     end;
end;

procedure TEditLibros.btnEmpresaClick(Sender: TObject);
var
   sKey, sDescription: String;
begin
     inherited;
     if Search( VACIO, sKey, sDescription, 1 ) then
     begin
          with dmVisitantes.cdsLibros do
          begin
               Edit;
               FieldByName('LI_EMPRESA').AsString := sDescription;
               FieldByName('EV_NUMERO').AsInteger := StrtoInt( sKey );
          end;
     end;
end;

procedure TEditLibros.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     VerificaControles;
end;

procedure TEditLibros.EscribirCambios;
var
   lCierraForma: Boolean;
   iFolio : Integer; 
begin
     with dmVisitantes.cdsLibros do
     begin
          Edit;
          if StrLleno( FieldByName('LI_SAL_CAS').AsString )then
          begin
               FieldByName('LI_STATUS').AsInteger := Ord( lsFuera );
               FieldByName('LI_SAL_VIG').AsInteger := dmCliente.Usuario;
          end;
     end;
     lCierraForma := ( dmVisitantes.cdsLibros.State = dsInsert );
     inherited EscribirCambios;
     if lCierraForma then
         Close;

     with dmVisitantes.cdsLibros do
     begin
          DisableControls;
          try
             iFolio:= FieldByName('LI_FOLIO').AsInteger;
             Refrescar;
             Locate( 'LI_FOLIO', iFolio, [] );
          finally
                 EnableControls;
          end;          
     end;
end;


procedure TEditLibros.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with DataSource do
     begin
          if ( Dataset <> NIL ) then
             bSalida.Enabled := Dataset.State = dsBrowse;
     end;
end;

procedure TEditLibros.ActualizaItemsCombos;
begin
     iTipoId := cbTipoId.ItemIndex;
     iTipoAuto := cbTipoAuto.ItemIndex;
     iTipoAsunto := cbAsunto.ItemIndex;
end;

procedure TEditLibros.bSalidaClick(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          if TieneSalida then
          begin
               if ZConfirm( Caption, '� Desea borrar la salida del visitante ? ',  0, mbNo ) then
               begin
                    CancelaSalida;
                    Close;
               end;
          end
          else
          begin
               if DoRegistraSalida( cdsLibros.FieldByName('LI_FOLIO').AsInteger ) then
                  Close;
          end;
     end;

end;

end.
