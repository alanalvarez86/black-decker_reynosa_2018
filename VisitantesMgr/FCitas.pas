unit FCitas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls,
  ZetaKeyLookup, Mask, ZetaFecha, ZetaDBTextBox, Buttons;

type
  TCitas = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    pnlFiltros: TPanel;
    lblFechaIni: TLabel;
    lblFechaFin: TLabel;
    lblAsunto: TLabel;
    Label4: TLabel;
    lblVisitante: TLabel;
    lblRegistros: TLabel;
    ztbRegistros: TZetaTextBox;
    zfFecIni: TZetaFecha;
    zfFecFin: TZetaFecha;
    CI_ASUNTO: TZetaKeyLookup;
    btnBuscar: TBitBtn;
    VI_NUMERO: TZetaKeyLookup;
    AN_NUMERO: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure zfFecIniValidDate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
  private
    FFiltro: String;     
    procedure CuentaRegistros;
    procedure PreparaFiltro;
    procedure LimpiaFiltros;
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  Citas: TCitas;

implementation
uses DVisitantes, ZetaCommonClasses, ZetaCommonTools, FBuscaAnfitriones, FBuscaVisitantes;

{$R *.DFM}

procedure TCitas.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          SetFechasInicio;
          zfFecIni.Valor := CitaFechaIni;
          zfFecFin.Valor := CitaFechaFin;
          CI_ASUNTO.LookupDataset := cdsTipoAsunto;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_NUMERO.LookupDataset := cdsVisitaCitas;
     end;
     HelpContext := H_VISMGR_CONS_CITAS;
end;

procedure TCitas.FormShow(Sender: TObject);
begin
     inherited;
     LimpiaFiltros;
     CuentaRegistros;
end;

procedure TCitas.LimpiaFiltros;
begin
     AN_NUMERO.Llave := VACIO;
     VI_NUMERO.Llave := VACIO;
     CI_ASUNTO.Llave := VACIO;
end;

procedure TCitas.CuentaRegistros;
begin
     ztbRegistros.Caption :=  InttoStr( dmVisitantes.cdsCitas.RecordCount );
end;

procedure TCitas.Connect;
begin
     with dmVisitantes do
     begin
          cdsTipoAsunto.Conectar;
          cdsAnfitrion.Conectar;
          cdsVisitanteLookup.Conectar;
          cdsCitas.Conectar;
          DataSource.DataSet := cdsCitas;
     end;
     PreparaFiltro;
end;

procedure TCitas.Refresh;
begin
     dmVisitantes.cdsCitas.Refrescar;
end;

procedure TCitas.Agregar;
begin
     dmVisitantes.cdsCitas.Agregar;
end;

procedure TCitas.Borrar;
begin
     dmVisitantes.cdsCitas.Borrar;
end;

procedure TCitas.Modificar;
begin
     dmVisitantes.cdsCitas.Modificar;
end;

procedure TCitas.zfFecIniValidDate(Sender: TObject);
begin
     inherited;
     with Sender as TZetaFecha do
     begin
          case Tag of
               0: dmVisitantes.CitaFechaIni := Valor;
               1: dmVisitantes.CitaFechaFin := Valor;
          end;
     end;
end;

procedure TCitas.PreparaFiltro;
begin
     FFiltro := VACIO;
     if strLleno( AN_NUMERO.Llave )then
         FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( C.AN_NUMERO ) = %s', [ EntreComillas( UpperCase( AN_NUMERO.Llave ) ) ]  ) );
     if strLleno( VI_NUMERO.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( C.VI_NUMERO ) = %s', [ EntreComillas( UpperCase( VI_NUMERO.Llave ) ) ]  ) );
     if strLleno( CI_ASUNTO.Llave )then
        FFiltro := ConcatFiltros( FFiltro, Format( 'Upper( C.CI_ASUNTO ) = %s', [ EntreComillas( UpperCase( CI_ASUNTO.Llave ) ) ]  ) );
end;


procedure TCitas.btnBuscarClick(Sender: TObject);
begin
     inherited;
     PreparaFiltro;
     dmVisitantes.ObtieneFiltros( FFiltro, 'Cita' );
     CuentaRegistros;
end;

end.
