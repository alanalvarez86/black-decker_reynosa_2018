object dmVisitantes: TdmVisitantes
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 479
  Width = 741
  object cdsTipoID: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Tipo de identificaci'#243'n'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 28
    Top = 20
  end
  object cdsTipoCarro: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Tipo de veh'#237'culo'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 92
    Top = 20
  end
  object cdsTipoAsunto: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Tipo de asunto'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 164
    Top = 20
  end
  object cdsTipoVisita: TZetaLookupDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Tipo de visitante'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 28
    Top = 76
  end
  object cdsDepto: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Departamentos'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 100
    Top = 76
  end
  object cdsCondiciones: TZetaLookupDataSet
    Tag = 12
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlAgregar = cdsCondicionesAlAgregar
    AlBorrar = cdsCondicionesAlBorrar
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 172
    Top = 76
  end
  object cdsAnfitrion: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'AN_NUMERO'
    Params = <>
    BeforePost = cdsAnfitrionBeforePost
    AfterPost = cdsAnfitrionAfterPost
    AfterDelete = cdsCatalogoAfterDelete
    OnNewRecord = cdsAnfitrionNewRecord
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsAnfitrionAlEnviarDatos
    AlCrearCampos = cdsAnfitrionAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Visitando A'
    LookupKeyField = 'AN_NUMERO'
    OnLookupDescription = cdsAnfitrionLookupDescription
    OnLookupSearch = cdsAnfitrionLookupSearch
    OnGetRights = cdsCatalogoGetRights
    Left = 20
    Top = 132
  end
  object cdsVisitante: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'VI_NUMERO'
    Params = <>
    BeforePost = cdsVisitanteBeforePost
    AfterPost = cdsVisitanteAfterPost
    AfterDelete = cdsCatalogoAfterDelete
    OnNewRecord = cdsVisitanteNewRecord
    OnReconcileError = cdsCatalogosReconcileError
    AlEnviarDatos = cdsVisitanteAlEnviarDatos
    AlCrearCampos = cdsVisitanteAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Visitante'
    LookupDescriptionField = 'VI_TIPO'
    LookupKeyField = 'VI_NUMERO'
    OnLookupDescription = cdsVisitanteLookupDescription
    OnLookupSearch = cdsVisitanteLookupSearch
    OnGetRights = cdsCatalogoGetRights
    Left = 92
    Top = 132
  end
  object cdsCaseta: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'CA_CODIGO'
    Params = <>
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlCrearCampos = cdsCasetaAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Caseta'
    LookupDescriptionField = 'CA_NOMBRE'
    LookupKeyField = 'CA_CODIGO'
    OnGetRights = cdsCatalogoGetRights
    Left = 164
    Top = 132
  end
  object cdsEmpVisitante: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'EV_NUMERO'
    Params = <>
    BeforePost = cdsEmpVisitanteBeforePost
    AfterPost = cdsEmpVisitanteAfterPost
    AfterDelete = cdsCatalogoAfterDelete
    OnNewRecord = cdsEmpVisitanteNewRecord
    OnReconcileError = cdsCatalogosReconcileError
    AlAdquirirDatos = cdsCatalogoAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlCrearCampos = cdsEmpVisitanteAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    UsaCache = True
    LookupName = 'Empresas'
    LookupDescriptionField = 'EV_NOMBRE'
    LookupKeyField = 'EV_NUMERO'
    OnGetRights = cdsCatalogoGetRights
    Left = 32
    Top = 184
  end
  object cdsCitas: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CI_FOLIO'
    Params = <>
    BeforePost = cdsCitasBeforePost
    AfterDelete = cdsCatalogoAfterDelete
    OnNewRecord = cdsCitasNewRecord
    OnReconcileError = cdsTablasReconcileError
    AlAdquirirDatos = cdsCitasAlAdquirirDatos
    AlEnviarDatos = cdsCitasAlEnviarDatos
    AlCrearCampos = cdsCitasAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    Left = 296
    Top = 16
  end
  object cdsAnfitrionLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsAnfitrionLookupAlCrearCampos
    UsaCache = True
    LookupName = 'Visitando A'
    LookupDescriptionField = 'Pretty_Anfi'
    LookupKeyField = 'AN_NUMERO'
    Left = 104
    Top = 184
  end
  object cdsVisitanteLookup: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsVisitanteLookupAlAdquirirDatos
    AlCrearCampos = cdsVisitanteLookupAlCrearCampos
    UsaCache = True
    LookupName = 'Visitantes'
    LookupDescriptionField = 'Pretty_Visit'
    LookupKeyField = 'VI_NUMERO'
    Left = 224
    Top = 184
  end
  object cdsCortes: TZetaClientDataSet
    Tag = 9
    Aggregates = <>
    IndexFieldNames = 'CO_FOLIO'
    Params = <>
    BeforePost = cdsCortesBeforePost
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsTablasReconcileError
    AlAdquirirDatos = cdsCortesAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlCrearCampos = cdsCortesAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    Left = 360
    Top = 16
  end
  object cdsLibros: TZetaClientDataSet
    Tag = 11
    Aggregates = <>
    IndexFieldNames = 'LI_FOLIO'
    Params = <>
    BeforePost = cdsLibrosBeforePost
    AfterDelete = cdsCatalogoAfterDelete
    OnReconcileError = cdsTablasReconcileError
    AlAdquirirDatos = cdsLibrosAlAdquirirDatos
    AlEnviarDatos = cdsCatalogoAlEnviarDatos
    AlCrearCampos = cdsLibrosAlCrearCampos
    AlModificar = cdsCatalogoAlModificar
    Left = 424
    Top = 16
  end
  object cdsCorteDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsTablasReconcileError
    AlAdquirirDatos = cdsCorteDetailAlAdquirirDatos
    AlCrearCampos = cdsCorteDetailAlCrearCampos
    Left = 296
    Top = 72
  end
  object cdsVisitaCitas: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'VI_NUMERO'
    Params = <>
    UsaCache = True
    LookupName = 'Visitante'
    LookupDescriptionField = 'VI_TIPO'
    LookupKeyField = 'VI_NUMERO'
    OnLookupDescription = cdsVisitanteLookupDescription
    OnLookupSearch = cdsVisitanteLookupSearch
    Left = 228
    Top = 236
  end
end
