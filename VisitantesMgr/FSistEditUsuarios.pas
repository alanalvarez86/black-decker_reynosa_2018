unit FSistEditUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditUsuarios, Db, ZetaDBTextBox, StdCtrls, ZetaKeyCombo,
  ZetaKeyLookup, DBCtrls, Mask, ZetaNumero, ComCtrls, ExtCtrls, Buttons,
  ZetaSmartLists;

type
  TSistEditUsuarios = class(TSistBaseEditUsuarios)
    Label15: TLabel;
    US_DOMAIN: TDBEdit;
    btnDominioUsuario: TSpeedButton;
    US_ACTIVO: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btnDominioUsuarioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure HabilitaControles; override;
  end;

var
  SistEditUsuarios: TSistEditUsuarios;

implementation
uses
    ZetaCommonClasses, DSistema,ZetaDialogo,ZetaCommonTools;
{$R *.DFM}

procedure TSistEditUsuarios.FormCreate(Sender: TObject);
begin
     inherited;
     Operacion.TabVisible := TRUE;
     HelpContext := H_VISMGR_EDIT_USUARIOS;
end;

procedure TSistEditUsuarios.btnDominioUsuarioClick(Sender: TObject);
begin
  with dmSistema do
     begin
          if ( UpperCase(US_DOMAIN.Text) <> UpperCase( cdsUsuarios.FieldByName('US_DOMAIN').AsString ) )then
          begin
               if (cdsUsuariosLookup.Locate('US_DOMAIN',UpperCase(US_DOMAIN.Text),[]))then
               begin
                    ZetaDialogo.ZWarning( Caption, 'El Usuario del Dominio: '+UpperCase(US_DOMAIN.Text)+' est� repetido' ,0,MBOK);
               end
               else
               begin
                    ZetaDialogo.zInformation( Caption, 'No existe Usuario '+UpperCase(US_DOMAIN.Text), 0 );
               end;
          end
          else
              ZetaDialogo.zInformation( Caption, 'No existe Usuario '+UpperCase(US_DOMAIN.Text), 0 );
     end;
end;

procedure TSistEditUsuarios.HabilitaControles;
begin
     inherited HabilitaControles;
     btnDominioUsuario.Enabled := Editing ;
end;

end.
