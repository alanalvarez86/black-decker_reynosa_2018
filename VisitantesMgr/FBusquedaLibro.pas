unit FBusquedaLibro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  Db, ZetaKeyCombo, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, cxDBData, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridLevel, cxGridCustomTableView, cxGridCardView,
  cxClasses, cxGridCustomView, cxGridCustomLayoutView, cxGrid, Vcl.ImgList,
  ZBaseEdicion_DevEx, Vcl.Menus, cxButtons, cxContainer, cxCheckBox,StrUtils,
  dxBarExtItems, dxBar, cxDBNavigator, ZetaCXGrid, cxGridDBTableView;

type
  TBusquedaLibro = class(TZetaDlgModal_DevEx)
    Panel1: TPanel;
    Label1: TLabel;
    eBusca: TEdit;
    btnBuscar: TcxButton;
    SmallImageList: TcxImageList;
    cxStyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle2: TcxStyle;
    DataSource: TDataSource;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    Pretty_Visit: TcxGridDBColumn;
    VI_TIPO: TcxGridDBColumn;
    ZetaDBGridDBTableViewColumn1: TcxGridDBColumn;
    VI_ASUNTO: TcxGridDBColumn;
    VI_STATUS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure FBusquedaLibroDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure eBuscaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OK_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellDblClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  private
    FNombre: String;
    FVisitante: Integer;
    procedure PreparaFiltros;
   procedure ConfigAgrupamiento;
    { Private declarations }

  published
    { Published declarations }

  public
    { Public declarations }
  end;

var
  BusquedaLibro: TBusquedaLibro;

function BuscaVisitanteDialogo( var iVisitante:Integer; var sNombre: String ): Boolean;

implementation
uses
    DVisitantes,
    ZetaDialogo,
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaCommonLists,
    ZGridModeTools,
    cxTextEdit;

{$R *.DFM}

type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);

function BuscaVisitanteDialogo( var iVisitante:Integer; var sNombre: String ): Boolean;
begin
     Result := False;
     if ( BusquedaLibro = nil ) then
        BusquedaLibro := TBusquedaLibro.Create( Application );
     try
        if ( BusquedaLibro <> nil ) then
        begin
             with BusquedaLibro do
             begin
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       sNombre := FNombre;
                       iVisitante := FVisitante;
                       Result := True;
                  end;
             end;
        end;
     finally
            FreeAndNil( BusquedaLibro );
     end;
end;

procedure TBusquedaLibro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     ZGridModeTools.LimpiaCacheGrid( ZetaDBGridDBTableView );
end;

procedure TBusquedaLibro.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext := H_VIS_BUS_VISITANTE;

end;

procedure TBusquedaLibro.btnBuscarClick(Sender: TObject);
begin
     inherited;
     if (eBusca.Text <> VACIO) or (sender = btnBuscar)then
        PreparaFiltros;
      //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TBusquedaLibro.PreparaFiltros;

var
   FParamList: TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'NOMBRE', UPPERCASE( eBusca.Text ) );
        FParamList.AddString( 'TIPO', UPPERCASE( '' ) );
        FParamList.AddString( 'ANFITRION', UPPERCASE(''{ AN_NUMERO.Llave} ) );
        FParamList.AddString( 'EMPRESA', UPPERCASE('' {EV_NUMERO.Llave} ) );
        FParamList.AddInteger('STATUS',1 ); //Se agrega el n�mero 1 ya que anteriormente era un combobox y el 1 era buscar todos
        with dmVisitantes do
        begin
             HacerBusquedaVisitante( FParamList );
             DataSource.DataSet := cdsVisitanteLookup;
             if ( cdsVisitanteLookup.IsEmpty ) then
             begin
                  ZetaDialogo.ZInformation('Informaci�n','No existe informaci�n con los datos requeridos', 0);
             end
        end;
      finally
            FreeAndNil( FParamList );
      end;
end;


procedure TBusquedaLibro.ZetaDBGridDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
     inherited;
     with dmVisitantes.cdsVisitanteLookup do
     begin
          if Active then
          begin
               FNombre := FieldByName('Pretty_Visit').AsString;
               FVisitante := FieldByName('VI_NUMERO').AsInteger;
               ModalResult := mrOk;
          end
          else
              ModalResult := mrOk;
     end;
end;

procedure TBusquedaLibro.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );   //se implemento la logica del ZbaseGridLectura ya que esta forma no hereda de gridLectura_DevEx
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;



procedure TBusquedaLibro.eBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if eBusca.Text <> VACIO then
  if key = 13 then
    PreparaFiltros;
end;

procedure TBusquedaLibro.FBusquedaLibroDblClick(Sender: TObject);
begin
     inherited;
     Close;
     ModalResult := mrOk;
end;

procedure TBusquedaLibro.FormShow(Sender: TObject);
begin
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     ConfigAgrupamiento;
     with dmVisitantes do
     begin
          cdsTipoVisita.Conectar;
          cdsAnfitrion.Conectar;
          cdsEmpVisitante.Conectar;
          cdsVisitanteLookup.Conectar;
     end;
     eBusca.SetFocus;
     eBusca.Text := VACIO;
end;



procedure TBusquedaLibro.OK_DevExClick(Sender: TObject);
begin
  inherited;
  with dmVisitantes.cdsVisitanteLookup do
     begin
          if Active then
          begin
               FNombre := FieldByName('Pretty_Visit').AsString;
               FVisitante := FieldByName('VI_NUMERO').AsInteger;
               ModalResult := mrOk;
          end
          else
              ModalResult := mrOk;
  end;
end;

procedure TBusquedaLibro.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := FALSE; //activa la posibilidad de agrupar
     ZetaDBGridDBTableView.OptionsView.GroupByBox := FALSE; //Muestra la caja de agrupamiento
     //Determinar las columnas por las que se podra agrupar
     Pretty_Visit.Options.Grouping:= FALSE;
     Pretty_Visit.Options.Filtering:= FALSE;
end;

end.
