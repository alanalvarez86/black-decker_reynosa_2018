unit FEditCatCompanys_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx,ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaNumero,
  ZetaKeyCombo, ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, cxControls, dxBarExtItems, dxBar, cxClasses,
  Vcl.ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
  cxTextEdit, cxMemo, cxDBEdit;

type
  TEditCatCompanys_DevEx = class(TBaseEdicion_DevEx)
    lblCompany: TLabel;
    lblNombre: TLabel;
    lblRazSoc: TLabel;
    lblTexto1: TLabel;
    lblTexto2: TLabel;
    lblTexto3: TLabel;
    lblTexto4: TLabel;
    lblTexto5: TLabel;
    lblStatus: TLabel;
    EV_NUMERO: TZetaDBNumero;
    EV_NOMBRE: TDBEdit;
    EV_RAZ_SOC: TcxDBMemo;
    EV_TEXTO1: TDBEdit;
    EV_TEXTO2: TDBEdit;
    EV_TEXTO3: TDBEdit;
    EV_TEXTO4: TDBEdit;
    EV_TEXTO5: TDBEdit;
    EV_STATUS: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatCompanys_DevEx: TEditCatCompanys_DevEx;

implementation

uses dVisitantes,
     ZAccesosTress,
     ZetaCommonClasses;
     
{$R *.DFM}

procedure TEditCatCompanys_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_COMPANYS;
     HelpContext:= H_VISMGR_EDIT_COMPANIAS;     
     //FirstControl := VI_NUMERO;
     //TipoValorActivo1 := stExpediente;
end;

procedure TEditCatCompanys_DevEx.Connect;
begin
     with dmVisitantes do
     begin
          DataSource.DataSet:= cdsEmpVisitante;
     end;
end;

end.
