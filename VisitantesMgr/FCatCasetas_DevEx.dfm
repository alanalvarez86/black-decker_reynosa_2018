inherited CatCasetas_DevEx: TCatCasetas_DevEx
  Left = 338
  Top = 299
  Caption = 'Casetas'
  ClientHeight = 271
  ClientWidth = 1069
  ExplicitWidth = 1069
  ExplicitHeight = 271
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1069
    ExplicitWidth = 1069
    inherited ValorActivo2: TPanel
      Width = 810
      ExplicitWidth = 810
      inherited textoValorActivo2: TLabel
        Width = 804
        ExplicitLeft = 724
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 1069
    Height = 252
    ExplicitWidth = 1069
    ExplicitHeight = 252
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle10
      Styles.Header = cxStyle11
      Styles.Indicator = cxStyle11
      object CA_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CA_CODIGO'
        MinWidth = 70
        Styles.Content = cxStyle12
        Width = 70
      end
      object CA_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'CA_NOMBRE'
        MinWidth = 70
        Styles.Content = cxStyle13
        Width = 200
      end
      object CA_UBICA: TcxGridDBColumn
        Caption = 'Ubicaci'#243'n'
        DataBinding.FieldName = 'CA_UBICA'
        MinWidth = 70
        Styles.Content = cxStyle14
        Width = 200
      end
      object CA_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'CA_STATUS'
        MinWidth = 70
        Styles.Content = cxStyle15
        Width = 80
      end
      object CA_CORTE: TcxGridDBColumn
        Caption = 'Ultimo corte'
        DataBinding.FieldName = 'CA_CORTE'
        MinWidth = 80
        Styles.Content = cxStyle16
        Width = 80
      end
      object CA_ABIERTA: TcxGridDBColumn
        Caption = #191'Abierta?'
        DataBinding.FieldName = 'CA_ABIERTA'
        MinWidth = 70
        Styles.Content = cxStyle17
      end
      object RE_NOMBRE: TcxGridDBColumn
        Caption = 'Reportes'
        DataBinding.FieldName = 'RE_NOMBRE'
        MinWidth = 100
        Styles.Content = cxStyle18
        Width = 200
      end
    end
    object ZetaDBGridDBTableView1: TcxGridDBTableView [1]
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      Styles.Content = cxStyle1
      Styles.Header = cxStyle2
      Styles.Indicator = cxStyle2
      object ZetaDBGridDBTableView1CA_CODIGO: TcxGridDBColumn
        DataBinding.FieldName = 'CA_CODIGO'
        Styles.Content = cxStyle3
        Width = 70
      end
      object ZetaDBGridDBTableView1CA_NOMBRE: TcxGridDBColumn
        DataBinding.FieldName = 'CA_NOMBRE'
        Styles.Content = cxStyle4
        Width = 200
      end
      object ZetaDBGridDBTableView1CA_UBICA: TcxGridDBColumn
        DataBinding.FieldName = 'CA_UBICA'
        Styles.Content = cxStyle5
        Width = 200
      end
      object ZetaDBGridDBTableView1CA_STATUS: TcxGridDBColumn
        DataBinding.FieldName = 'CA_STATUS'
        Styles.Content = cxStyle6
        Width = 80
      end
      object ZetaDBGridDBTableView1CA_CORTE: TcxGridDBColumn
        DataBinding.FieldName = 'CA_CORTE'
        Styles.Content = cxStyle7
        Width = 70
      end
      object ZetaDBGridDBTableView1CA_ABIERTA: TcxGridDBColumn
        DataBinding.FieldName = 'CA_ABIERTA'
        Styles.Content = cxStyle8
      end
      object ZetaDBGridDBTableView1RE_NOMBRE: TcxGridDBColumn
        DataBinding.FieldName = 'RE_NOMBRE'
        Styles.Content = cxStyle9
        Width = 200
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object cxStyleRepository1: TcxStyleRepository
    Top = 144
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle6: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle7: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle8: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle9: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
  object cxStyleRepository2: TcxStyleRepository
    Top = 80
    PixelsPerInch = 96
    object cxStyle10: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle11: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle12: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle13: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle14: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle15: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle16: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle17: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle18: TcxStyle
      AssignedValues = [svColor, svFont, svTextColor]
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clWindowText
    end
  end
end
