unit FCatCasetas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid;

type
  TCatCasetas = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    //procedure DoLookup; override;
  end;

var
  CatCasetas: TCatCasetas;

implementation

uses DVisitantes,
     ZetaCommonClasses;

{$R *.DFM}

{ TCatVisitantes }
procedure TCatCasetas.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     HelpContext := H_VISMGR_CONS_CASETAS;
end;

procedure TCatCasetas.Connect;
begin
     with dmVisitantes do
     begin
          cdsCaseta.Conectar;
          DataSource.DataSet := cdsCaseta;
     end;
end;

procedure TCatCasetas.Refresh;
begin
     dmVisitantes.cdsCaseta.Refrescar;
end;

procedure TCatCasetas.Agregar;
begin
     dmVisitantes.cdsCaseta.Agregar;
end;

procedure TCatCasetas.Borrar;
begin
    dmVisitantes.cdsCaseta.Borrar;
end;

procedure TCatCasetas.Modificar;
begin
     dmVisitantes.cdsCaseta.Modificar;
end;


end.
