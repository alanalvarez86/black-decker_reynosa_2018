inherited EditCatCompanys: TEditCatCompanys
  Left = 281
  Top = 205
  Caption = 'Edici'#243'n de Compa'#241#237'as'
  ClientHeight = 321
  ClientWidth = 432
  PixelsPerInch = 96
  TextHeight = 13
  object lblCompany: TLabel [0]
    Left = 39
    Top = 47
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Compa'#241#237'a:'
  end
  object lblNombre: TLabel [1]
    Left = 51
    Top = 69
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object lblRazSoc: TLabel [2]
    Left = 25
    Top = 107
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Raz'#243'n Social:'
  end
  object lblTexto1: TLabel [3]
    Left = 52
    Top = 145
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto 1:'
  end
  object lblTexto2: TLabel [4]
    Left = 52
    Top = 168
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto 2:'
  end
  object lblTexto3: TLabel [5]
    Left = 52
    Top = 191
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto 3:'
  end
  object lblTexto4: TLabel [6]
    Left = 52
    Top = 214
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto 4:'
  end
  object lblTexto5: TLabel [7]
    Left = 52
    Top = 237
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto 5:'
  end
  object lblStatus: TLabel [8]
    Left = 58
    Top = 260
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  inherited PanelBotones: TPanel
    Top = 285
    Width = 432
    TabOrder = 11
    inherited OK: TBitBtn
      Left = 264
    end
    inherited Cancelar: TBitBtn
      Left = 349
    end
  end
  inherited PanelSuperior: TPanel
    Width = 432
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 432
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 106
    end
  end
  object EV_NUMERO: TZetaDBNumero [12]
    Left = 96
    Top = 43
    Width = 90
    Height = 21
    Mascara = mnEmpleado
    MaxLength = 4
    TabOrder = 2
    DataField = 'EV_NUMERO'
    DataSource = DataSource
  end
  object EV_NOMBRE: TDBEdit [13]
    Left = 96
    Top = 65
    Width = 300
    Height = 21
    DataField = 'EV_NOMBRE'
    DataSource = DataSource
    TabOrder = 3
  end
  object EV_RAZ_SOC: TDBMemo [14]
    Left = 96
    Top = 88
    Width = 300
    Height = 50
    DataField = 'EV_RAZ_SOC'
    DataSource = DataSource
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object EV_TEXTO1: TDBEdit [15]
    Left = 96
    Top = 141
    Width = 300
    Height = 21
    DataField = 'EV_TEXTO1'
    DataSource = DataSource
    TabOrder = 5
  end
  object EV_TEXTO2: TDBEdit [16]
    Left = 96
    Top = 164
    Width = 300
    Height = 21
    DataField = 'EX_TEXTO2'
    DataSource = DataSource
    TabOrder = 6
  end
  object EV_TEXTO3: TDBEdit [17]
    Left = 96
    Top = 187
    Width = 300
    Height = 21
    DataField = 'EV_TEXTO3'
    DataSource = DataSource
    TabOrder = 7
  end
  object EV_TEXTO4: TDBEdit [18]
    Left = 96
    Top = 210
    Width = 300
    Height = 21
    DataField = 'EV_TEXTO4'
    DataSource = DataSource
    TabOrder = 8
  end
  object EV_TEXTO5: TDBEdit [19]
    Left = 96
    Top = 233
    Width = 300
    Height = 21
    DataField = 'EV_TEXTO5'
    DataSource = DataSource
    TabOrder = 9
  end
  object EV_STATUS: TZetaDBKeyCombo [20]
    Left = 96
    Top = 256
    Width = 120
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 10
    ListaFija = lfCatCompanys
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'EV_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 9
  end
end
