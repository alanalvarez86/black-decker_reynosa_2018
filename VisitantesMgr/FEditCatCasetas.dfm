inherited EditCatCasetas: TEditCatCasetas
  Left = 264
  Top = 335
  Caption = 'Edici'#243'n de Casetas'
  ClientHeight = 230
  ClientWidth = 467
  PixelsPerInch = 96
  TextHeight = 13
  object lblCodigo: TLabel [0]
    Left = 55
    Top = 46
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object lblNombre: TLabel [1]
    Left = 51
    Top = 69
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nombre:'
  end
  object lblUbica: TLabel [2]
    Left = 40
    Top = 92
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ubicaci'#243'n:'
  end
  object lblStatus: TLabel [3]
    Left = 58
    Top = 115
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status:'
  end
  object lblCorte: TLabel [4]
    Left = 31
    Top = 180
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ultimo Corte:'
  end
  object CA_CORTE: TZetaDBTextBox [5]
    Left = 96
    Top = 178
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'CA_CORTE'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CA_CORTE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label1: TLabel [6]
    Left = 19
    Top = 161
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caseta Abierta:'
  end
  object CA_ABIERTA: TZetaDBTextBox [7]
    Left = 96
    Top = 159
    Width = 100
    Height = 17
    AutoSize = False
    Caption = 'CA_ABIERTA'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CA_ABIERTA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lblReportes: TLabel [8]
    Left = 56
    Top = 138
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Gafete:'
  end
  inherited PanelBotones: TPanel
    Top = 194
    Width = 467
    TabOrder = 7
    inherited OK: TBitBtn
      Left = 299
    end
    inherited Cancelar: TBitBtn
      Left = 384
    end
  end
  inherited PanelSuperior: TPanel
    Width = 467
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 467
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 141
    end
  end
  object CA_CODIGO: TDBEdit [12]
    Left = 96
    Top = 42
    Width = 100
    Height = 21
    CharCase = ecUpperCase
    DataField = 'CA_CODIGO'
    DataSource = DataSource
    TabOrder = 2
  end
  object CA_NOMBRE: TDBEdit [13]
    Left = 96
    Top = 65
    Width = 350
    Height = 21
    DataField = 'CA_NOMBRE'
    DataSource = DataSource
    TabOrder = 3
  end
  object CA_UBICA: TDBEdit [14]
    Left = 96
    Top = 88
    Width = 350
    Height = 21
    DataField = 'CA_UBICA'
    DataSource = DataSource
    TabOrder = 4
  end
  object CA_STATUS: TZetaDBKeyCombo [15]
    Left = 96
    Top = 111
    Width = 118
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    ListaFija = lfCasetaStatus
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'CA_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object RE_CODIGO: TZetaDBKeyLookup [16]
    Left = 96
    Top = 134
    Width = 353
    Height = 21
    Filtro = 'RE_TIPO=1'
    TabOrder = 6
    TabStop = True
    WidthLlave = 65
    DataField = 'RE_CODIGO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 1
  end
end
