inherited GlobalIdentificacion: TGlobalIdentificacion
  ActiveControl = RazonSocial
  Caption = 'Identificaci�n'
  ClientHeight = 275
  ClientWidth = 419
  PixelsPerInch = 96
  TextHeight = 13
  object Label12: TLabel [0]
    Left = 48
    Top = 6
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Raz�n Social:'
  end
  object Label13: TLabel [1]
    Left = 88
    Top = 29
    Width = 26
    Height = 13
    Alignment = taRightJustify
    Caption = 'Calle:'
  end
  object Label14: TLabel [2]
    Left = 76
    Top = 52
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Colonia:'
  end
  object Label16: TLabel [3]
    Left = 78
    Top = 75
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ciudad:'
  end
  object Label17: TLabel [4]
    Left = 46
    Top = 120
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'C�digo Postal:'
  end
  object Label15: TLabel [5]
    Left = 78
    Top = 98
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado:'
  end
  object Label18: TLabel [6]
    Left = 81
    Top = 166
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'R.F.C.:'
  end
  object Label19: TLabel [7]
    Left = 44
    Top = 235
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = '# INFONAVIT:'
    Visible = False
  end
  object Label21: TLabel [8]
    Left = 12
    Top = 188
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante Legal:'
  end
  object Label1: TLabel [9]
    Left = 69
    Top = 143
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tel�fono:'
  end
  object Label3: TLabel [10]
    Left = 21
    Top = 212
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Directorio Reportes:'
  end
  object Buscar: TSpeedButton [11]
    Left = 392
    Top = 206
    Width = 25
    Height = 25
    Hint = 'Buscar Directorio'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
      300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
      330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
      333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
      339977FF777777773377000BFB03333333337773FF733333333F333000333333
      3300333777333333337733333333333333003333333333333377333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = BuscarClick
  end
  inherited PanelBotones: TPanel
    Top = 239
    Width = 419
    TabOrder = 10
    inherited OK: TBitBtn
      Left = 248
    end
    inherited Cancelar: TBitBtn
      Left = 328
    end
  end
  object RazonSocial: TEdit
    Tag = 1
    Left = 117
    Top = 2
    Width = 274
    Height = 21
    TabOrder = 0
  end
  object Calle: TEdit
    Tag = 2
    Left = 117
    Top = 25
    Width = 274
    Height = 21
    TabOrder = 1
  end
  object Colonia: TEdit
    Tag = 3
    Left = 117
    Top = 48
    Width = 274
    Height = 21
    TabOrder = 2
  end
  object Ciudad: TEdit
    Tag = 4
    Left = 117
    Top = 71
    Width = 274
    Height = 21
    TabOrder = 3
  end
  object CodigoPostal: TEdit
    Tag = 6
    Left = 117
    Top = 116
    Width = 188
    Height = 21
    TabOrder = 5
  end
  object Telefono: TEdit
    Tag = 7
    Left = 117
    Top = 139
    Width = 188
    Height = 21
    TabOrder = 6
  end
  object RepresentanteLegal: TEdit
    Tag = 12
    Left = 117
    Top = 185
    Width = 271
    Height = 21
    TabOrder = 8
  end
  object RFC: TMaskEdit
    Tag = 8
    Left = 117
    Top = 162
    Width = 188
    Height = 21
    TabOrder = 7
    Text = 'RFC'
  end
  object Infonavit: TMaskEdit
    Tag = 9
    Left = 117
    Top = 231
    Width = 271
    Height = 21
    TabOrder = 11
    Text = 'Infonavit'
    Visible = False
  end
  object Entidad: TEdit
    Tag = 4
    Left = 117
    Top = 94
    Width = 274
    Height = 21
    TabOrder = 4
  end
  object Plantillas: TEdit
    Tag = 61
    Left = 117
    Top = 208
    Width = 271
    Height = 21
    TabOrder = 9
  end
end
