unit FEditCatCompanys;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaNumero,
  ZetaKeyCombo, ZetaSmartLists;

type
  TEditCatCompanys = class(TBaseEdicion)
    lblCompany: TLabel;
    lblNombre: TLabel;
    lblRazSoc: TLabel;
    lblTexto1: TLabel;
    lblTexto2: TLabel;
    lblTexto3: TLabel;
    lblTexto4: TLabel;
    lblTexto5: TLabel;
    lblStatus: TLabel;
    EV_NUMERO: TZetaDBNumero;
    EV_NOMBRE: TDBEdit;
    EV_RAZ_SOC: TDBMemo;
    EV_TEXTO1: TDBEdit;
    EV_TEXTO2: TDBEdit;
    EV_TEXTO3: TDBEdit;
    EV_TEXTO4: TDBEdit;
    EV_TEXTO5: TDBEdit;
    EV_STATUS: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCatCompanys: TEditCatCompanys;

implementation

uses dVisitantes,
     ZAccesosTress,
     ZetaCommonClasses;
     
{$R *.DFM}

procedure TEditCatCompanys.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_CAT_COMPANYS;
     HelpContext:= H_VISMGR_EDIT_COMPANIAS;     
     //FirstControl := VI_NUMERO;
     //TipoValorActivo1 := stExpediente;
end;

procedure TEditCatCompanys.Connect;
begin
     with dmVisitantes do
     begin
          DataSource.DataSet:= cdsEmpVisitante;
     end;
end;

end.
