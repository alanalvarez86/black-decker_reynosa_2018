inherited SistUsuarios: TSistUsuarios
  Left = 417
  ClientWidth = 807
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 807
    inherited ValorActivo2: TPanel
      Width = 447
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 807
    Columns = <
      item
        Expanded = False
        FieldName = 'us_codigo'
        Title.Caption = 'N'#250'mero'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DOMAIN'
        Title.Caption = 'Dominio'
        Width = 132
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'us_corto'
        Title.Caption = 'Usuario'
        Width = 72
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'us_nombre'
        Title.Caption = 'Nombre'
        Width = 241
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'GR_DESCRIP'
        Title.Caption = 'Grupo'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_ACTIVO'
        Title.Caption = 'Activo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DENTRO'
        Title.Caption = 'Dentro'
        Width = 38
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_LUGAR'
        Title.Caption = 'Tel'#233'fono'
        Width = 65
        Visible = True
      end>
  end
  inherited Panel1: TPanel
    Width = 807
  end
end
