unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcSistGlobales,
                     efcReportes,
                     efcSistProcesos,
                     efcSistEmpresas,
                     efcSistUsuarios,
                     efcSistGrupos,
                     efcImpresoras,
                     efcQueryGral,
                     efcCatCondiciones,
                     efcBitacora,
                     efcTipoID,
                     efcTipoCarro,
                     efcTipoAsunto,
                     efcTipoVisita,
                     efcDepto,
                     efcRegVisitas,
                     efcProcVisitas,
                     efcAnfitrion,
                     efcVisitante,
                     efcCaseta,
                     efcRegLibro,
                     efcRegCitas,
                     efcRegCorte,
                     efcCatCompanys,
                     efcDiccion );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FReportes_DevEx,
     FGlobales,
     FSistBitacora_DevEx,
     FSistUsuarios_DevEx,
     FSistGrupos_DevEx,
     FSistEmpresas_DevEx,
     FQueryGral_DevEx,
     FCatCondiciones_DevEx,
     FImpresoras_DevEx,
     FTablas_DevEx,
     FCatAnfitrion_DevEx,
     FCatVisitantes_DevEx, //DACP
     FCatCasetas_DevEx,
     FCatCompanys_DevEx,
     FCitas_DevEx,
     FDiccion_DevEx,
     FCortes_DevEx,
     FLibros_DevEx;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
           efcTipoID                  : Result := Consulta( D_TAB_TIPO_ID, TCatTipoID_DevEx );
           efcTipoCarro               : Result := Consulta( D_TAB_TIPO_CARRO, TCatTipoCarro_DevEx );
           efcTipoAsunto              : Result := Consulta( D_TAB_TIPO_ASUNTO, TCatTipoAsunto_DevEx );
           efcTipoVisita              : Result := Consulta( D_TAB_TIPO_VISITA, TCatTipoVisita_DevEx );
           efcDepto                   : Result := Consulta( D_TAB_DEPTO, TCatDepto_DevEx );
           efcSistGlobales            : Result := Consulta( D_CAT_CONFI_GLOBALES, TSistGlobales );
           efcDiccion                 : Result := Consulta( D_CAT_CONFI_DICCIONARIO, TDiccion_DevEx );
           efcSistUsuarios            : Result := Consulta( D_SIST_DATOS_USUARIOS, TSistUsuarios_DevEx );
           efcSistGrupos              : Result := Consulta( D_SIST_DATOS_GRUPOS, TSistGrupos_DevEx );
           efcSistEmpresas            : Result := Consulta( D_SIST_DATOS_EMPRESAS, TSistEmpresas_DevEx );
           efcQueryGral               : Result := Consulta( D_CONS_SQL, TQueryGral_DevEx );
           efcReportes                : Result := Consulta( D_CONS_REPORTES, TReportes_DevEx );
           efcBitacora                : Result := Consulta( D_CONS_BITACORA, TSistBitacora_DevEx );
           efcCatCondiciones          : Result := Consulta( D_CAT_GRALES_CONDICIONES, TCatCondiciones_DevEx );
           efcImpresoras              : Result := Consulta( D_SIST_DATOS_IMPRESORAS, TImpresoras_DevEx );
           efcAnfitrion               : Result := Consulta( D_CAT_ANFITRION, TCatAnfitriones_DevEx );
           efcVisitante               : Result := Consulta( D_CAT_VISITANTE, TCatVisitantes_DevEx ); //DACP
           efcCaseta                  : Result := Consulta( D_CAT_CASETA, TCatCasetas_DevEx );
           efcCatCompanys             : Result := Consulta( D_CAT_COMPANYS, TCatCompanys_DevEx );
           efcRegCitas                : Result := Consulta( D_REG_CITAS, TCitas_DevEx );
           efcRegCorte                : Result := Consulta( D_REG_CORTE, TCortes_DevEx );
           efcRegLibro                : Result := Consulta( D_REG_LIBRO, TLibros_DevEx );
           //efcRegVisita               : Result := Consulta( D_REGISTRO, TRegArchivo );
           //efcProcVisita              : Result := Consulta( D_PROCESO, TProcArchivo );
     else
         Result := Consulta( 0, nil );
     end;
end;

end.
