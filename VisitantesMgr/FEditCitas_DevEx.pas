unit FEditCitas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx,ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaDBTextBox,
  ZetaKeyLookup, ZetaHora, Mask, ZetaFecha, ZetaKeyCombo, ZetaSmartLists,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls,
  dxBarExtItems, dxBar, cxClasses, Vcl.ImgList, cxNavigator, cxDBNavigator,
  cxButtons, cxContainer, cxEdit, cxTextEdit, cxMemo, cxDBEdit,
  ZetaKeyLookup_DevEx, cxGroupBox, ZetaEdit;

type
  TEditCitas_DevEx = class(TBaseEdicion_DevEx)
    lblAsunto: TLabel;
    lblAnfitrion: TLabel;
    lblVisitante: TLabel;
    lblFecha: TLabel;
    lblHora: TLabel;
    lblStatus: TLabel;
    CI_FECHA: TZetaDBFecha;
    CI_HORA: TZetaDBHora;
    lblFolio: TLabel;
    CI_ASUNTO: TZetaDBKeyLookup_DevEx;
    gbObserva: TcxGroupBox;
    lblUsuario: TLabel;
    US_CODIGO: TZetaDBTextBox;
    CI_STATUS: TZetaDBKeyCombo;
    CI_OBSERVA: TcxDBMemo;
    CI_FOLIO: TZetaDBTextBox;
    VI_NUMERO: TZetaDBKeyLookup_DevEx;
    AN_NUMERO: TZetaDBKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
  private
  { Private declarations }
         FVisitante: String;
         FAnfitrion: String;
  protected
     procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCitas_DevEx: TEditCitas_DevEx;

implementation
uses DVisitantes, DSistema, ZAccesosTress, FBuscaAnfitriones, ZetaCommonTools,
     ZetaCommonLists, FBuscaVisitantes,FBusquedaLibro,FBuscaAnfitriones_DevEx, ZetaCommonClasses,ZetaDialogo;
{$R *.DFM}

procedure TEditCitas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := ZAccesosTress.D_REG_CITAS;
     FirstControl := CI_HORA;
     with dmVisitantes do
     begin
          CI_ASUNTO.LookupDataset := cdsTipoAsunto;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          VI_NUMERO.LookupDataset := cdsVisitaCitas;
     end;
     CI_STATUS.ItemIndex := 0;
     HelpContext := H_VISMGR_EDIT_CITAS;
end;

procedure TEditCitas_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmSistema.cdsUsuariosLookup.Conectar;
     with dmVisitantes do
     begin
          cdsAnfitrion.Conectar;
          cdsCitas.Conectar;
          cdsVisitanteLookup.Conectar;
          cdsAnfitrionLookup.Conectar;
          cdsTipoAsunto.Conectar;
          DataSource.DataSet:= cdsCitas;
     end;
end;

end.
