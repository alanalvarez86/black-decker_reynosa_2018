unit ZGlobalTress;

interface

uses ZetaCommonLists,
     ZetaCommonClasses;

{ TEMPORAL. Esto va en otra unidad }
const
     K_NUM_GLOBALES = 43;
     K_NUM_DEFAULTS = 0;
     K_TOT_GLOBALES = K_NUM_GLOBALES + K_NUM_DEFAULTS;
     K_BASE_DEFAULTS = 2000;
     K_TOT_DESCRIPCIONES_GV = 13;

     {Constantes para Globales}
     K_GLOBAL_RAZON_EMPRESA            =  1; {Raz�n Social de Empresa}
     K_GLOBAL_CALLE_EMPRESA            =  2; {Calle de Empresa}
     K_GLOBAL_COLONIA_EMPRESA          =  3; {Colonia de Empresa}
     K_GLOBAL_CIUDAD_EMPRESA           =  4; {Ciudad de Empresa}
     K_GLOBAL_ENTIDAD_EMPRESA          =  5; {Entidad de Empresa}
     K_GLOBAL_CP_EMPRESA               =  6; {C�digo Postal Empresa}
     K_GLOBAL_TEL_EMPRESA              =  7; {Tel�fono de Empresa}
     K_GLOBAL_RFC_EMPRESA              =  8; {R.F.C. de Empresa}
     //K_GLOBAL_INFONAVIT_EMPRESA        =  9; { # de INFONAVIT de Empresa}
     K_GLOBAL_REPRESENTANTE            = 10; {Representante Legal}

     K_GLOBAL_DIR_PLANT                = 11; {Directorio Plantillas}

     K_GLOBAL_PRIMER_ADICIONAL = 0;
     K_GLOBAL_LAST_ADICIONAL = 0;

     K_GLOBAL_VERSION_DATOS            = 12;

     K_GLOBAL_EMAIL_HOST                = 13;
     K_GLOBAL_EMAIL_PORT                = 41;
     K_GLOBAL_EMAIL_USERID              = 14;
     K_GLOBAL_EMAIL_PSWD                = 42;
     K_GLOBAL_EMAIL_FROMADRESS          = 15;
     K_GLOBAL_EMAIL_FROMNAME            = 16;
     K_GLOBAL_EMAIL_ANUNCIO             = 17;
     K_GLOBAL_EMAIL_MSGERROR            = 18;
     K_GLOBAL_EMAIL_PLANTILLA           = 40;
     K_GLOBAL_EMAIL_AUTH                = 43;

     K_GLOBAL_GRABA_BITACORA            = 19;

     K_GLOBAL_FECHA_BASE                = 20;
     K_GLOBAL_LOG_BASE                  = 21;
     K_GLOBAL_NUM_BASE                  = 22;
     K_GLOBAL_TAB_BASE                  = 23;
     K_GLOBAL_TEXTO_BASE                = 24;

     K_GLOBAL_REQ_ANFITRION             = 25;
     K_GLOBAL_REQ_DEPARTAMENTO          = 26;
     K_GLOBAL_REQ_VISITANTE             = 27;
     K_GLOBAL_REQ_COMPANYS              = 28;
     K_GLOBAL_REQ_ASUNTO                = 29;
     K_GLOBAL_REQ_GAFETE                = 30;
     K_GLOBAL_REQ_VEHICULO              = 31;
     K_GLOBAL_REQ_IDENTIFICA            = 32;
     K_GLOBAL_ABIERTA_VISI              = 33;
     K_GLOBAL_ABIERTA_ANFI              = 34;
     K_GLOBAL_ABIERTA_COMPANY           = 35;
     K_GLOBAL_ABIERTA_DEPTO             = 36;

     //Alertas

     K_GLOBAL_ALERTAS_ACTIVADA          = 37;
     K_GLOBAL_ALERTAS_TIEMPO_MINIMO     = 38;
     K_GLOBAL_ALERTAS_INTERVALO         = 39;




     {Esta constante se puso nada mas para que compile el proyecto.
     Se utiliza en DQUERIES, pero Seleccion de Personal, nunca va a llegar
     a ejecutar esa linea de codigo.
     El valor de esta constante es DUMMY.}
     K_GLOBAL_PRIMER_DIA = 999;
     K_GLOBAL_DIGITO_EMPRESA = 999;     // Se utiliza en ZFuncsGlobal

     { ��� OJO !!!   CUANDO SE AGREGUE UNA CONSTANTE NUEVA

     1) SE TIENE QUE AGREGAR EN EL METODO DGLOBAL.GETTIPOGLOBAL CUANDO EL GLOBAL NO SEA TEXTO
     2) Se debe aumentar en 1 la constante K_NUM_GLOBALES ( est� al principio de esta unidad )
     3) Se debe considerar la constante en la funci�n GetTipoGlobal ( est� al final de esta unidad )
     }

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
function GlobalToPos( const iCodigo : Integer ) : Integer;
function PosToGlobal( const iPos : Integer ) : Integer;
function GetDescripcionGlobal( const iCodigo : integer ): string;

implementation

{ Permite compactar el arreglo de Globales }
{ Los globales arriba de 2000, les resta un offset }
function GlobalToPos( const iCodigo : Integer ) : Integer;
begin
     if ( iCodigo <= K_NUM_GLOBALES ) then
        Result := iCodigo
     else
        Result := iCodigo - K_BASE_DEFAULTS + K_NUM_GLOBALES;
end;

function PosToGlobal( const iPos : Integer ) : Integer;
begin
     if ( iPos <= K_NUM_GLOBALES ) then
        Result := iPos
     else
        Result := iPos + K_BASE_DEFAULTS - K_NUM_GLOBALES;
end;

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
begin
     // Si se tienen Globales que no sean de Texto deber�n condicionarse para regresar otros
     // tipos Globales: tgBooleano, tgFloat, tgNumero, tgFecha, etc.
     {if ( iCodigo in [ K_GLOBAL_NUM_GLOBAL1..K_GLOBAL_NUM_GLOBAL3 ] ) then
        Result := tgFloat
     else}

     if ( iCodigo in [ K_GLOBAL_REQ_ANFITRION..K_GLOBAL_ABIERTA_DEPTO ] ) OR
        ( iCodigo in [ K_GLOBAL_ALERTAS_ACTIVADA ] ) then
        Result := tgBooleano
     else if ( iCodigo in [ K_GLOBAL_ALERTAS_TIEMPO_MINIMO, K_GLOBAL_ALERTAS_INTERVALO] )
              or ( iCodigo = K_GLOBAL_EMAIL_PORT ) or ( iCodigo = K_GLOBAL_EMAIL_AUTH )  then
          Result := tgNumero
     else
        Result := tgTexto;
end;

function GetDescripcionGlobal( const iCodigo : integer ): string;
begin
     case iCodigo of
          K_GLOBAL_RAZON_EMPRESA : Result := 'Raz�n Social de Empresa';
          K_GLOBAL_CALLE_EMPRESA : Result := 'Calle de Empresa';
          K_GLOBAL_COLONIA_EMPRESA : Result := 'Colonia de Empresa';
          K_GLOBAL_CIUDAD_EMPRESA : Result := 'Ciudad de Empresa';
          K_GLOBAL_ENTIDAD_EMPRESA : Result := 'Entidad de Empresa';
          K_GLOBAL_CP_EMPRESA : Result := 'C�digo Postal Empresa';
          K_GLOBAL_TEL_EMPRESA : Result := 'Tel�fono de Empresa';
          K_GLOBAL_RFC_EMPRESA : Result := 'R.F.C. de Empresa';
          //K_GLOBAL_INFONAVIT_EMPRESA : Result := '# de INFONAVIT de Empresa';
          K_GLOBAL_REPRESENTANTE : Result := 'Representante Legal';

          K_GLOBAL_DIR_PLANT : Result := 'Directorio Plantillas';

          K_GLOBAL_VERSION_DATOS : Result := 'Version Datos';
          
          K_GLOBAL_EMAIL_HOST       : Result := 'Servidor de Correos';
          K_GLOBAL_EMAIL_PORT       : Result := 'Puerto';
          K_GLOBAL_EMAIL_USERID     : Result := 'ID. Usuario';
          K_GLOBAL_EMAIL_PSWD       : Result := 'Clave de Correo';
          K_GLOBAL_EMAIL_FROMADRESS : Result := 'Direcci�n del Email';
          K_GLOBAL_EMAIL_FROMNAME   : Result := 'Descripci�n del Email';
          K_GLOBAL_EMAIL_MSGERROR   : Result := 'Direcci�n para Errores';
          K_GLOBAL_EMAIL_PLANTILLA : Result := 'Plantilla Default';
          K_GLOBAL_EMAIL_ANUNCIO : Result := 'Direcci�n para Anuncio';
          K_GLOBAL_EMAIL_AUTH       : Result := 'M�todo de Autenticaci�n';
          K_GLOBAL_GRABA_BITACORA : Result := 'Movimientos que se registran en Bit�cora';

          K_GLOBAL_REQ_VISITANTE:    Result := 'Campo Visitante Requerido';
          K_GLOBAL_REQ_ANFITRION:    Result := 'Campo Visitando A Requerido';
          K_GLOBAL_REQ_COMPANYS:     Result := 'Campo Compa��a Requerido';
          K_GLOBAL_REQ_DEPARTAMENTO: Result := 'Campo Departamento Requerido';
          K_GLOBAL_REQ_ASUNTO:       Result := 'Campo Tipo De Asunto Requerido';
          K_GLOBAL_REQ_VEHICULO:     Result := 'Campo Tipo De Veh�culo Requerido';
          K_GLOBAL_REQ_IDENTIFICA:   Result := 'Campo Tipo De Identificaci�n Requerido';
          K_GLOBAL_REQ_GAFETE:       Result := 'Campo Gafete Requerido';
          K_GLOBAL_ABIERTA_VISI:     Result := 'Permitir Captura Abierta De Visitantes';
          K_GLOBAL_ABIERTA_ANFI:     Result := 'Permitir Captura Abierta De Visitando A';
          K_GLOBAL_ABIERTA_COMPANY:  Result := 'Permitir Captura Abierta De Compa��as';
          K_GLOBAL_ABIERTA_DEPTO:    Result := 'Permitir Captura Abierta De Departamentos';
          K_GLOBAL_ALERTAS_ACTIVADA: Result := 'Activar Mecanismo de Alertas';
          K_GLOBAL_ALERTAS_TIEMPO_MINIMO: Result := 'Tiempo M�nimo para Mostrar Alerta';
          K_GLOBAL_ALERTAS_INTERVALO:Result := 'Mostrar Alerta Cada n Minutos';
     else
         Result := 'Descripci�n No Disponible';
     end;
end;


end.
