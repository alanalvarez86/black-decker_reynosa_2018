unit DReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient,
  {$ifndef VER130}
  Variants,
  {$endif}
  {$ifdef DOS_CAPAS}
     DServerVisReportes,
  {$else}
     VisReportes_TLB,
  {$endif}
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaTipoEntidad,
  DBaseReportes;

type
  TdmReportes = class(TdmBaseReportes)
    procedure cdsLookupReportesLookupSearch(Sender: TZetaLookupDataSet;
      var lOk: Boolean; const sFilter: String; var sKey,
      sDescription: String);
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerReportesBDE: TdmServerVisReportes;
    property ServerReportesBDE: TdmServerVisReportes read GetServerReportesBDE;
{$else}
    FServidorBDE: IdmServerVisReportesDisp;
    function GetServerReportesBDE: IdmServerVisReportesDisp;
    property ServerReportesBDE: IdmServerVisReportesDisp read GetServerReportesBDE;
{$endif}
  protected
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList: OleVariant;
                        var sError: WideString): OleVariant;override;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;override;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;override;

  public
    { Public declarations }
    //function GetDerechosClasifi(eClasifi : eClasifiReporte): Integer;override;
    function DirectorioPlantillas: string;override;
  end;

var
  dmReportes: TdmReportes;

implementation
uses DCliente,
     Dglobal,
     ZetaCommonTools,
     ZGlobalTress,
     ZAccesosTress,
     ZetaBusqueda_DevEx,
     ZetaDialogo;
{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmReportes.GetServerReportesBDE: TdmServerVisReportes;
begin
     Result := DCliente.dmCliente.ServerVisReportes;
end;
{$else}
function TdmReportes.GetServerReportesBDE: IdmServerVisReportesDisp;
begin
     Result := IdmServerVisReportesDisp( dmCliente.CreaServidor( CLASS_dmServerVisReportes, FServidorBDE ) );
end;
{$endif}

function TdmReportes.GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ) : OleVariant;
begin
     Result := ServerReportesBDE.GeneraSQL( dmCliente.Empresa,oSQLAgente, ParamList , sError  );
end;


function TdmReportes.ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;
begin
     Result := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParamList, sError );
end;

procedure TdmReportes.cdsLookupReportesLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     with Sender do
     begin
          if NOT Filtered then
          begin
               Filter := 'RE_TIPO =1';
               Filtered := TRUE;
          end;
          lOk := NOT IsEmpty;
          Filtered := FALSE;
     end;

     if lOk then
        lOk := ZetaBusqueda_DevEx.ShowSearchForm( Sender, sFilter, sKey, sDescription )
     else ZInformation('B�squeda de Formas','No se han Definido Formas en el Reporteador',0);

end;

function TdmReportes.ServidorPruebaFormula(Parametros: TZetaParams; var sTexto: widestring;
         const EntidadActiva: TipoEntidad; oParams: Olevariant): Boolean;
begin
     Result := FALSE;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
end;

end.
