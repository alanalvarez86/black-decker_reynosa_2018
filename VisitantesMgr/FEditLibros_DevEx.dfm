inherited EditLibros_DevEx: TEditLibros_DevEx
  Left = 454
  Top = 167
  Caption = 'Edici'#243'n de Visitas'
  ClientHeight = 400
  ClientWidth = 383
  ExplicitWidth = 389
  ExplicitHeight = 429
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 364
    Width = 383
    ExplicitTop = 364
    ExplicitWidth = 827
    inherited OK_DevEx: TcxButton
      Left = 218
      ExplicitLeft = 662
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 297
      ExplicitLeft = 741
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 383
    ExplicitWidth = 827
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 83
      end
    end
    inherited ValorActivo2: TPanel
      Width = 57
      ExplicitWidth = 501
      inherited textoValorActivo2: TLabel
        Left = -29
        Width = 83
        ExplicitLeft = -29
      end
    end
  end
  object PageControl1: TcxPageControl [3]
    Left = 0
    Top = 50
    Width = 383
    Height = 314
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = tsVehiculo
    Properties.CustomButtons.Buttons = <>
    ExplicitWidth = 827
    ClientRectBottom = 312
    ClientRectLeft = 2
    ClientRectRight = 381
    ClientRectTop = 27
    object tsVehiculo: TcxTabSheet
      Caption = 'Visitante'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 823
      ExplicitHeight = 0
      object lblVisitante: TLabel
        Left = 29
        Top = 49
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Visitante:'
      end
      object lblEmpresa: TLabel
        Left = 20
        Top = 74
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = 'Compa'#241#237'a:'
      end
      object lblAsunto: TLabel
        Left = 36
        Top = 99
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asunto:'
      end
      object lblTipoId: TLabel
        Left = 6
        Top = 124
        Width = 66
        Height = 13
        Alignment = taRightJustify
        Caption = 'Identificaci'#243'n:'
      end
      object lblLicId: TLabel
        Left = 29
        Top = 149
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Licencia:'
      end
      object lblGafete: TLabel
        Left = 37
        Top = 172
        Width = 35
        Height = 13
        Alignment = taRightJustify
        Caption = 'Gafete:'
      end
      object lblStatus: TLabel
        Left = 39
        Top = 26
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object LI_STATUS: TZetaDBTextBox
        Left = 74
        Top = 24
        Width = 150
        Height = 17
        AutoSize = False
        Caption = 'LI_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LI_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblFolio: TLabel
        Left = 47
        Top = 5
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Folio:'
      end
      object LI_FOLIO: TZetaDBTextBox
        Left = 74
        Top = 3
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'LI_FOLIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'LI_FOLIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object btnVisitantes: TcxButton
        Left = 351
        Top = 45
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
          FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
          FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
          BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
          D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
          FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
          8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
          C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
          F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
          FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = btnVisitantesClick
      end
      object btnEmpresa: TcxButton
        Left = 351
        Top = 70
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
          FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
          FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
          BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
          D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
          FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
          8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
          C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
          F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
          FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = btnEmpresaClick
      end
      object LI_ASUNTO: TZetaDBKeyLookup_DevEx
        Left = 74
        Top = 95
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'LI_ASUNTO'
        DataSource = DataSource
      end
      object LI_TIPO_ID: TZetaDBKeyLookup_DevEx
        Left = 74
        Top = 120
        Width = 300
        Height = 21
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'LI_TIPO_ID'
        DataSource = DataSource
      end
      object LI_ID: TDBEdit
        Left = 74
        Top = 145
        Width = 275
        Height = 21
        DataField = 'LI_ID'
        DataSource = DataSource
        TabOrder = 4
      end
      object LI_GAFETE: TDBEdit
        Left = 74
        Top = 170
        Width = 275
        Height = 21
        CharCase = ecUpperCase
        DataField = 'LI_GAFETE'
        DataSource = DataSource
        TabOrder = 5
      end
      object gbCarro: TGroupBox
        Left = 2
        Top = 191
        Width = 375
        Height = 110
        Align = alCustom
        Caption = ' Veh'#237'culo '
        TabOrder = 6
        object lblCajon: TLabel
          Left = 40
          Top = 89
          Width = 30
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caj'#243'n:'
        end
        object lblDescrip: TLabel
          Left = 11
          Top = 63
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descripci'#243'n:'
        end
        object lblPlacas: TLabel
          Left = 35
          Top = 39
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Placas:'
        end
        object lblCarro: TLabel
          Left = 24
          Top = 16
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Veh'#237'culo:'
        end
        object LI_CAR_EST: TDBEdit
          Left = 72
          Top = 85
          Width = 150
          Height = 21
          DataField = 'LI_CAR_EST'
          DataSource = DataSource
          TabOrder = 3
        end
        object LI_CAR_DES: TDBEdit
          Left = 72
          Top = 61
          Width = 275
          Height = 21
          DataField = 'LI_CAR_DES'
          DataSource = DataSource
          TabOrder = 2
        end
        object LI_CAR_PLA: TDBEdit
          Left = 72
          Top = 37
          Width = 150
          Height = 21
          DataField = 'LI_CAR_PLA'
          DataSource = DataSource
          TabOrder = 1
        end
        object LI_CAR_TIP: TZetaDBKeyLookup_DevEx
          Left = 72
          Top = 12
          Width = 300
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 0
          TabStop = True
          WidthLlave = 60
          DataField = 'LI_CAR_TIP'
          DataSource = DataSource
        end
      end
      object LI_NOMBRE: TDBEdit
        Left = 74
        Top = 45
        Width = 275
        Height = 21
        DataField = 'LI_NOMBRE'
        DataSource = DataSource
        TabOrder = 0
        OnChange = LI_NOMBREChange
      end
      object LI_EMPRESA: TDBEdit
        Left = 74
        Top = 70
        Width = 275
        Height = 21
        DataField = 'LI_EMPRESA'
        DataSource = DataSource
        TabOrder = 1
        OnChange = LI_EMPRESAChange
      end
    end
    object tsGeneral: TcxTabSheet
      Caption = 'Visitando A'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 823
      ExplicitHeight = 0
      object lblCita: TLabel
        Left = 53
        Top = 11
        Width = 21
        Height = 13
        Alignment = taRightJustify
        Caption = 'Cita:'
      end
      object lblAnfitrion: TLabel
        Left = 34
        Top = 36
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre:'
      end
      object CI_FOLIO: TZetaDBTextBox
        Left = 77
        Top = 10
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CI_FOLIO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CI_FOLIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object lblDepto: TLabel
        Left = 4
        Top = 63
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Departamento:'
      end
      object btnAnfitrion: TcxButton
        Left = 353
        Top = 33
        Width = 21
        Height = 21
        Hint = 'Buscar Anfitri'#243'n'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
          FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
          FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
          BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
          D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
          FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
          8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
          C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
          F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
          FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = btnAnfitrionClick
      end
      object btnDepto: TcxButton
        Left = 353
        Top = 60
        Width = 21
        Height = 21
        Hint = 'Buscar Visitante'
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A4050000000000000000000000000000000000008B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CED2FFD0CCD0FF908791FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFC2BCC2FFFFFF
          FFFFFFFFFFFFD9D5D9FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFB1ABB2FFFFFFFFFFFFFFFFFFF4F3F4FF988F99FF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFA49DA5FFFAF9FAFFFFFFFFFFFAF9FAFFA39BA3FF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFAFA9B0FFDEDBDFFFF4F3F4FFFBFBFBFFE2DFE2FFF4F3F4FFFFFFFFFFFDFD
          FDFFAFA9B0FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFC5C0C6FFFFFFFFFFFFFFFFFFF8F7F8FFF2F1F2FFFFFF
          FFFFFFFFFFFFFFFFFFFFC0BAC0FF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFA39BA3FFFFFFFFFFF8F7F8FFBEB8
          BFFF8F8590FF8B818CFFA39BA3FFEDEBEDFFFFFFFFFFC2BCC2FF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFD2CE
          D2FFFFFFFFFFBEB8BFFF8B818CFF8B818CFF8B818CFF8B818CFFA69FA7FFFFFF
          FFFFF0EFF1FF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFFE0DDE0FFFFFFFFFFA8A1A9FF8B818CFF8B818CFF8B81
          8CFF8B818CFF908791FFFFFFFFFFFFFFFFFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFFCCC8CCFFFFFFFFFFC5C0
          C6FF8B818CFF8B818CFF8B818CFF8B818CFFAFA9B0FFFFFFFFFFEDEBEDFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA39BA3FFFDFDFDFFFDFDFDFFBCB6BDFF948B95FF8D838EFFAFA9B0FFF4F3
          F4FFFFFFFFFFBCB6BDFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFFBAB4BBFFFFFFFFFFFFFFFFFFFDFD
          FDFFFAF9FAFFFFFFFFFFFFFFFFFFD0CCD0FF8D838EFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFFA199A2FFD7D4D7FFEDEBEDFFEDEBEDFFD9D5D9FFAEA7AEFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B81
          8CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF8B818CFF}
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = btnDeptoClick
      end
      object LI_DEPTO: TDBEdit
        Left = 77
        Top = 60
        Width = 275
        Height = 21
        DataField = 'LI_DEPTO'
        DataSource = DataSource
        TabOrder = 1
        OnChange = LI_DEPTOChange
      end
      object gbOtros: TGroupBox
        Left = 3
        Top = 84
        Width = 371
        Height = 102
        Align = alCustom
        Caption = ' Detalles '
        TabOrder = 2
        object lblTexto1: TLabel
          Left = 24
          Top = 18
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #1:'
        end
        object lblTexto3: TLabel
          Left = 24
          Top = 73
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #3:'
        end
        object lblTexto2: TLabel
          Left = 24
          Top = 45
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Texto #2:'
        end
        object LI_TEXTO1: TDBEdit
          Left = 74
          Top = 15
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO1'
          DataSource = DataSource
          TabOrder = 0
        end
        object LI_TEXTO2: TDBEdit
          Left = 74
          Top = 42
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO2'
          DataSource = DataSource
          TabOrder = 1
        end
        object LI_TEXTO3: TDBEdit
          Left = 74
          Top = 70
          Width = 276
          Height = 21
          DataField = 'LI_TEXTO3'
          DataSource = DataSource
          TabOrder = 2
        end
      end
      object gbObserva: TGroupBox
        Left = 3
        Top = 189
        Width = 371
        Height = 112
        Align = alCustom
        Caption = ' Observaciones '
        TabOrder = 3
        object LI_OBSERVA: TDBMemo
          Left = 6
          Top = 14
          Width = 358
          Height = 93
          Align = alCustom
          DataField = 'LI_OBSERVA'
          DataSource = DataSource
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object LI_ANFITR: TDBEdit
        Left = 77
        Top = 33
        Width = 275
        Height = 21
        DataField = 'LI_ANFITR'
        DataSource = DataSource
        TabOrder = 0
        OnChange = LI_ANFITRChange
      end
    end
    object tsIO: TcxTabSheet
      Caption = 'Entradas/Salidas'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 823
      ExplicitHeight = 0
      object GroupBox3: TGroupBox
        Left = 4
        Top = 13
        Width = 368
        Height = 118
        Align = alCustom
        Caption = ' Entradas '
        TabOrder = 0
        object lblFecEnt: TLabel
          Left = 25
          Top = 35
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblHorEnt: TLabel
          Left = 32
          Top = 54
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object lblEntCas: TLabel
          Left = 22
          Top = 74
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caseta:'
        end
        object lblRegEntrada: TLabel
          Left = 16
          Top = 93
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'Registr'#243':'
        end
        object lblCorteEntrada: TLabel
          Left = 30
          Top = 15
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Corte:'
        end
        object LI_ENT_COR: TZetaDBTextBox
          Left = 61
          Top = 13
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_COR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_COR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_FEC: TZetaDBTextBox
          Left = 61
          Top = 33
          Width = 110
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_FEC'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_FEC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_HOR: TZetaDBTextBox
          Left = 61
          Top = 52
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_HOR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_HOR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_CAS: TZetaDBTextBox
          Left = 61
          Top = 72
          Width = 268
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_CAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_ENT_CAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_ENT_VIG: TZetaDBTextBox
          Left = 61
          Top = 91
          Width = 268
          Height = 17
          AutoSize = False
          Caption = 'LI_ENT_VIG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_ENT_LIB'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox4: TGroupBox
        Left = 3
        Top = 137
        Width = 369
        Height = 159
        Align = alCustom
        Caption = ' Salidas '
        TabOrder = 1
        object lblSalFec: TLabel
          Left = 24
          Top = 41
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object lblHorSal: TLabel
          Left = 31
          Top = 62
          Width = 26
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora:'
        end
        object lblSalCas: TLabel
          Left = 21
          Top = 84
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Caseta:'
        end
        object lblRegSalida: TLabel
          Left = 15
          Top = 106
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'Registr'#243':'
        end
        object lblCorteSal: TLabel
          Left = 29
          Top = 18
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = 'Corte:'
        end
        object LI_SAL_COR: TZetaDBTextBox
          Left = 60
          Top = 16
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_COR'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_SAL_COR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_VIG: TZetaDBTextBox
          Left = 60
          Top = 104
          Width = 267
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_VIG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_SAL_LIB'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_FEC2: TZetaDBTextBox
          Left = 60
          Top = 39
          Width = 100
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_FEC2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_SAL_FEC'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_HOR2: TZetaDBTextBox
          Left = 60
          Top = 60
          Width = 40
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_HOR2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'LI_SAL_HOR'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_CAS2: TZetaDBTextBox
          Left = 60
          Top = 82
          Width = 267
          Height = 17
          AutoSize = False
          Caption = 'LI_SAL_CAS2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'CA_NOM_SAL'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object LI_SAL_FEC: TZetaDBFecha
          Left = 60
          Top = 36
          Width = 100
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '17-Mar-04'
          Valor = 38063.000000000000000000
          DataField = 'LI_SAL_FEC'
          DataSource = DataSource
        end
        object LI_SAL_HOR: TZetaDBHora
          Left = 60
          Top = 58
          Width = 40
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 1
          Text = '    '
          Tope = 24
          Valor = '    '
          DataField = 'LI_SAL_HOR'
          DataSource = DataSource
        end
        object LI_SAL_CAS: TZetaDBKeyLookup_DevEx
          Left = 60
          Top = 80
          Width = 293
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 66
          DataField = 'LI_SAL_CAS'
          DataSource = DataSource
        end
        object LI_DEVGAF: TDBCheckBox
          Left = 41
          Top = 128
          Width = 97
          Height = 17
          Caption = 'Entreg'#243' Gafete'
          DataField = 'LI_DEVGAF'
          DataSource = DataSource
          TabOrder = 3
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 412
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670360
    ImageInfo = <
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF98A1E2FFF6F7FDFFB7BEEBFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFB1B8
          E9FFF6F7FDFFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFA6AEE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6
          E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8
          EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5ACDFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFF
          FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4A5A
          CDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
          FFFFAFB6E9FF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
          EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4A5ACDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
          E9FF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
          FFFFC3C8EEFF4A5ACDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFFFFFFFFFF
          FFFFFFFFFFFFB7BEEBFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFFAFB6E9FFFFFF
          FFFFFFFFFFFFF6F7FDFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF8792DEFFDFE2F6FFA1A9E5FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF9BA4
          E3FFDFE2F6FF98A1E2FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8
          A1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF8CE2B8FFE9F9
          F1FF54D396FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFF4FCF8FFFFFF
          FFFFADEACCFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF52D395FFD6F4E6FFFFFFFFFFFFFF
          FFFFFCFEFDFF70DAA7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FFA5E8C8FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFD9F5E7FF52D395FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFFCFEFDFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFF9AE5C1FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF5AD59AFFE9F9F1FFFFFFFFFFFFFFFFFFC3EFDAFFBDEE
          D6FFFFFFFFFFFFFFFFFFF4FCF8FF62D79FFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FFC3EFDAFFFFFFFFFFF7FDFAFF8CE2B8FF4FD293FF54D3
          96FFE1F7ECFFFFFFFFFFFFFFFFFFC8F1DDFF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF8AE1B7FFE6F9F0FF6AD9A4FF4FD293FF4FD293FF4FD2
          93FF5FD69DFFE4F8EEFFFFFFFFFFFFFFFFFF8FE2BAFF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF52D395FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF65D8A1FFEFFBF5FFFFFFFFFFF7FDFAFF68D8A2FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF6AD9A4FFEFFBF5FFFFFFFFFFDEF7EBFF52D395FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF78DDACFFF7FDFAFFFFFFFFFFB8EDD3FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF70DAA7FFEFFBF5FFFFFFFFFFA0E6
          C4FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF65D8A1FFE1F7ECFFFFFF
          FFFF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF57D498FFBDEE
          D6FFFCFEFDFF8AE1B7FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF81DFB1FF8FE2BAFF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD2
          93FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF4FD293FF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end
      item
        Image.Data = {
          36090000424D3609000000000000360000002800000018000000180000000100
          20000000000000090000000000000000000000000000000000004757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF5261CFFF6C79D6FF8D97DFFFACB3E8FFC8CD
          F0FFE8EAF9FFBAC0ECFF6F7CD7FF7582D9FF6471D4FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF848FDDFF8D97DFFFACB3E8FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFF969FE2FF6471D4FF969FE2FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFCFC
          FEFFD4D8F3FFBAC0ECFF4757CCFF4757CCFF969FE2FFF9FAFDFFFFFFFFFFFFFF
          FFFFA4ACE6FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFE5E7F8FFBAC0ECFF4757CCFF4757CCFF6F7CD7FFD1D5F2FFE5E7F8FFBAC0
          ECFF818CDCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF6976D6FF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFF4757CCFF4757CCFFA4ACE6FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFFA4ACE6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFBAC0ECFFACB3E8FFBAC0ECFFB1B8E9FF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF5564D0FF7582D9FF969FE2FFB1B8
          E9FFD1D5F2FFACB3E8FF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757
          CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF4757CCFF}
        Mask.Data = {
          9E000000424D9E000000000000003E0000002800000018000000180000000100
          010000000000600000000000000000000000020000000000000000000000FFFF
          FF00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000}
      end>
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 304
    Top = 56
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 3670280
  end
end
