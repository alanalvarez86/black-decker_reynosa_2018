unit FGlobalIdentificacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZBaseGlobal,ZBaseGlobal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList, cxButtons,ZetaClientTools;

type
  TGlobalIdentificacion_DevEx = class(TBaseGlobal_DevEx)
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label1: TLabel;
    RazonSocial: TEdit;
    Calle: TEdit;
    Colonia: TEdit;
    Ciudad: TEdit;
    CodigoPostal: TEdit;
    Telefono: TEdit;
    RepresentanteLegal: TEdit;
    RFC: TMaskEdit;
    Infonavit: TMaskEdit;
    Entidad: TEdit;
    Label3: TLabel;
    Plantillas: TEdit;
    Buscar: TcxButton;
    OpenDialog: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure BuscarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalIdentificacion_DevEx: TGlobalIdentificacion_DevEx;

implementation

uses ZGlobalTress,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

procedure TGlobalIdentificacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos          := D_CAT_CONFI_GLOBALES;
     RazonSocial.Tag        := K_GLOBAL_RAZON_EMPRESA;
     Calle.Tag              := K_GLOBAL_CALLE_EMPRESA;
     Colonia.Tag            := K_GLOBAL_COLONIA_EMPRESA;
     Ciudad.Tag             := K_GLOBAL_CIUDAD_EMPRESA;
     Entidad.Tag            := K_GLOBAL_ENTIDAD_EMPRESA;
     CodigoPostal.Tag       := K_GLOBAL_CP_EMPRESA;
     Telefono.Tag           := K_GLOBAL_TEL_EMPRESA;
     RepresentanteLegal.Tag := K_GLOBAL_REPRESENTANTE;
     RFC.Tag                := K_GLOBAL_RFC_EMPRESA;
     //Infonavit.Tag          := K_GLOBAL_INFONAVIT_EMPRESA;
     Plantillas.Tag         := K_GLOBAL_DIR_PLANT;
     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_GE_IDENTIF;
     {$else}
     HelpContext := H00050_Globales_de_empresa;
     {$endif}     
end;

procedure TGlobalIdentificacion_DevEx.BuscarClick(Sender: TObject);
begin
     inherited;

    // ZetaClientTools.AbreDialogo( OpenDialog, Plantillas.Text, 'Dat' );
    {  with OpenDialog do
     begin
          if Execute then
          begin
               if UpperCase(ExtractFileDir(FileName)) = UpperCase(InitialDir) then
                  Plantillas.Text := ExtractFileName(FileName)
               else
                  Plantillas.Text := FileName;
          end;
     end; }

     Plantillas.Text := BuscarDirectorio( Plantillas.Text );
end;

end.
