unit FTressShell;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ImgList, ActnList, ExtCtrls, ComCtrls, StdCtrls,
  Buttons, ZetaSmartLists, ZBaseShell, ZetaTipoEntidad, ZetaClientDataSet,ZBasicoNavBarShell,
  ZetaCommonLists, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxRibbonSkins, dxRibbonCustomizationForm,
  System.Actions, cxStyles, cxClasses, dxSkinsForm, dxRibbon, dxStatusBar,
  dxRibbonStatusBar, dxBarBuiltInMenu, cxLocalization, dxBar, dxNavBar,
  cxButtons, cxPC,ZNavBarTools,ZArbolTress;

type
  TTressShell = class(TBasicoNavBarShell)
    TabArchivo_btnCatalogoUsuarios: TdxBarLargeButton;
    TabArchivo_btnConfigurarEmpresa: TdxBarLargeButton;
    _A_ConfigurarEmpresa: TAction;
    _A_CatalogoUsuarios: TAction;
    _A_ExploradorReportes: TAction;
    _A_Bitacora_DevEx: TAction;
    _A_SQL_DevEx: TAction;
    AConsultas: TdxBar;
    TabArchivo_btnExplorardorReportes: TdxBarLargeButton;
    TabArchivo_btnSQL: TdxBarLargeButton;
    TabArchivo_btnBitacora: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure _A_CatalogoUsuariosExecute(Sender: TObject);
    procedure ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
      Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabArchivo_btnConfigurarEmpresaClick(Sender: TObject);

   procedure _A_ConfigurarEmpresaExecute(Sender: TObject);
    procedure _A_ExploradorReportesExecute(Sender: TObject);
    procedure _A_ListaProcesosExecute(Sender: TObject);
    procedure _A_Bitacora_DevExExecute(Sender: TObject);
    procedure _A_SQL_DevExExecute(Sender: TObject);


  protected
    procedure DoOpenAll; override;
    procedure DoCloseAll; override;
    procedure CargaTraducciones; override;
    procedure HabilitaControles; override;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado); override;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado); override;
    {$endif}
    function ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad; const sFilter: String;
             var sKey, sDescription: String; var lEncontrado: Boolean ): Boolean; override;
  private
    { Private declarations }
    procedure RevisaDerechos;
    procedure AsignacionTabOrder_DevEx;
  public
    { Public declarations }
    procedure LLenaOpcionesGrupos;
    function GetLookUpDataSet(const eEntidad: TipoEntidad): TZetaLookUpDataSet;override;
    procedure CargaVista; override;

    {$ifdef MULTIPLES_ENTIDADES}
    procedure SetDataChange(const Entidades: array of TipoEntidad);override;
    {$else}
    procedure SetDataChange(const Entidades: ListaEntidades); override;
    {$endif}
  end;

var
  TressShell: TTressShell;

implementation

uses DCliente, DGlobal, DSistema,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     DCatalogos, DReportes, DDiccionario, DVisitantes, ZAccesosMgr,
     ZAccesosTress,ZetaDialogo, ZetaCommonClasses,
     ZetaClientTools,ZGlobalTress, ZetaDespConsulta, DConsultas, FBuscaAnfitriones,
     FBuscaVisitantes;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
{$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
{$endif}
     Global := TdmGlobal.Create;
     dmCliente := TdmCliente.Create( Self );
     dmSistema := TdmSistema.Create( Self );
     dmConsultas := TdmConsultas.Create( Self );
     dmCatalogos := TdmCatalogos.Create( Self );
     dmReportes := TdmReportes.Create( Self );
     dmDiccionario := TdmDiccionario.Create( Self );
     dmVisitantes := TdmVisitantes.Create( Self );
     HelpContext := H_VISMGR_PRINCIPAL;     
     inherited;
     {HelpContext := H00003_Explorador;
     FHelpGlosario := 'Visitantes.hlp>Main';}
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     dmVisitantes.Free;
     dmDiccionario.Free;
     dmReportes.Free;
     dmCatalogos.Free;
     dmConsultas.Free;
     dmSistema.Free;
     dmCliente.Free;
     Global.Free;
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.FormShow(Sender: TObject);
begin
     inherited;
     BuscarFormaBtn_DevEx.Caption:='';
     CargaVista;
     CargaTraducciones;
end;

procedure TTressShell.DoOpenAll;
const
     K_TAMANO_ARBOLITOS = 5;
begin
     try
        inherited DoOpenAll;
        Global.Conectar;
        SetArbolitosLength( K_TAMANO_ARBOLITOS );
        CreaNavBar;
        LLenaOpcionesGrupos;
        HabilitaControles;
        RevisaDerechos;
     except
        on Error : Exception do
        begin
             ZetaDialogo.zError( 'Error al Conectar', 'Empresa: ' + dmCliente.Compania + CR_LF + Error.Message, 0 );
             DoCloseAll;
        end;
     end;
end;

procedure TTressShell.DoCloseAll;
begin
     CierraFormaTodas;
     if EmpresaAbierta then
     begin
          dmCliente.CierraEmpresa;
          CierraDatasets( dmDiccionario );
          CierraDatasets( dmReportes );
          CierraDatasets( dmConsultas );
          CierraDatasets( dmVisitantes );
          CierraDatasets( dmSistema );
          CierraDatasets( dmCatalogos );
     end;
     inherited DoCloseAll;
{$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll;
{$endif}
end;

procedure TTressShell.RevisaDerechos;
begin
      _A_CatalogoUsuarios.Enabled:=Revisa(   D_SIST_DATOS_USUARIOS ) and EmpresaAbierta;
      if(not ( _A_CatalogoUsuarios.Enabled ) ) then
      begin
           TabArchivo_btnCatalogoUsuarios.Visible:=ivNever;
      end;
      _A_ExploradorReportes.Enabled := CheckDerecho(GetPropConsulta(efcReportes).IndexDerechos , K_DERECHO_CONSULTA);
      _A_SQL_DevEx.enabled := CheckDerecho(GetPropConsulta(efcQueryGral).IndexDerechos,K_DERECHO_CONSULTA);
      _A_Bitacora_DevEx.enabled := CheckDerecho(GetPropConsulta(efcBitacora).IndexDerechos,K_DERECHO_CONSULTA);
end;
{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.SetDataChange(const Entidades: array of TipoEntidad);
{$else}
procedure TTressShell.SetDataChange(const Entidades: ListaEntidades);
{$endif}
begin
     { Cambios a otros datasets }
     inherited SetDataChange( Entidades );
     //NotifyDataChange( Entidades, stNinguno );
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TTressShell.NotifyDataChange(const Entidades: array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TTressShell.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     dmVisitantes.NotifyDataChange( Entidades, Estado );
     {dmNomina.NotifyDataChange( Entidades, Estado );
     dmAsistencia.NotifyDataChange( Entidades, Estado );
     dmIMSS.NotifyDataChange( Entidades, Estado );
     dmCatalogos.NotifyDataChange( Entidades, Estado );
     dmSistema.NotifyDataChange( Entidades, Estado );
     dmCafeteria.NotifyDataChange( Entidades, Estado );}
     inherited NotifyDataChange( Entidades, Estado );
end;

procedure TTressShell._A_CatalogoUsuariosExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcSistUsuarios );
end;

procedure TTressShell._A_ConfigurarEmpresaExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcSistGlobales );
end;

procedure TTressShell._A_ListaProcesosExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcSistProcesos );
end;

procedure TTressShell._A_ExploradorReportesExecute(Sender: TObject);
begin
     AbreFormaConsulta( efcReportes );
end;

procedure TTressShell._A_Bitacora_DevExExecute(Sender: TObject);
begin
  inherited;
  AbreFormaConsulta( efcBitacora );
end;

procedure TTressShell._A_SQL_DevExExecute(Sender: TObject);
begin
  AbreFormaConsulta( efcQueryGral );
end;

procedure TTressShell.TabArchivo_btnConfigurarEmpresaClick(Sender: TObject);
begin
     AbreFormaConsulta( efcSistEmpresas );
end;


function TTressShell.ProcesaDialogo(oLookUp: TZetaLookupDataset; const eEntidad: TipoEntidad;
         const sFilter: String; var sKey, sDescription: String; var lEncontrado: Boolean): Boolean;
var
   iNumero:Integer;
begin
     Result := ( eEntidad in [enAnfitrio, enVisita] );
     if Result then
     begin
          iNumero := StrToIntDef( sKey, 0 );
          case eEntidad of
              enAnfitrio : lEncontrado := FBuscaAnfitriones.BuscaAnfitrionDialogo( iNumero, sDescription );
              enVisita : lEncontrado := FBuscaVisitantes.BuscaVisitanteDialogo( iNumero, sDescription );
          end;
          if lEncontrado then
             sKey := IntToStr( iNumero );
     end;
end;

function TTressShell.GetLookUpDataSet( const eEntidad: TipoEntidad ): TZetaLookUpDataSet;
begin
     with dmVisitantes do
     begin
          case eEntidad of
                //enLibro: Result := cdsLibros;
                //enCita: Result := cdscit
                enTAsunto: Result := cdsTipoAsunto;
                enTCarro: Result := cdsTipoCarro;
                enTipoID: Result := cdsTipoID;
                enAnfitrio: Result := cdsAnfitrion;
                enDepto: Result := cdsDepto;
                enVisita: Result := cdsVisitante;
                enTVisita: Result := cdsTipoVisita;
                enEmpVisita: Result := cdsEmpVisitante;
                enCaseta: Result := cdsCaseta;
                enReporte: Result := dmReportes.cdsLookupReportes;
                {enCorte
                enReporte,
                enCampoRep,
                enQuerys,
                enBitacora,
                enProceso,
                enDiccion,
                enFunciones,
                enFormula,
                enSuscrip,
                enGlobal,
                enMisReportes,}
                enUsuarios: Result := dmSistema.cdsUsuariosLookup;
                //enCompanys
          else
              Result := nil;
          end;
     end;
end;

procedure TTressShell.HabilitaControles;
begin
     DevEx_BarManager.GetItemByName('TabArchivo_btnConfigurarEmpresa').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnCatalogoUsuarios').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnExplorardorReportes').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnBitacora').Enabled := EmpresaAbierta;
     DevEx_BarManager.GetItemByName('TabArchivo_btnSQL').Enabled := EmpresaAbierta;
end;

//Carga los textos a traducir
procedure TTressShell.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)

end;

procedure TTressShell.AsignacionTabOrder_DevEx;
begin
     DevEx_ShellRibbon.TabOrder:= 1;
     PanelNavBar.TabOrder := 2;
    // DevEx_BandaValActivos.TabOrder :=3;
     PanelConsulta.TabOrder :=3;
end;

procedure TTressShell.CargaVista;
begin
     inherited;
     //Mostar Banda Valores activos
     DevEx_BandaValActivos.Visible := FALSE;
     //Asignacion de TabOrders para el shell
     AsignacionTabOrder_DevEx;
     //Para avisar a los controles que hubo movimientos
     Application.ProcessMessages;
end;

procedure TTressShell.ClientAreaPG_DevExTcxPageControlPropertiesTcxPCCustomButtonsButtons0Click(
  Sender: TObject);
begin
  inherited;
  _V_Cerrar.Execute;
end;

Procedure TTressShell.LLenaOpcionesGrupos;
var
I:integer;
begin
  if(dmCliente.EmpresaAbierta) then
  begin
        for I:=0 to DevEx_NavBar.Groups.Count-1 do
        begin
              if (DevEx_NavBar.Groups[I].Caption = 'Registros')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=0;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=0;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Consultas')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=1;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=1;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Catálogos')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=2;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=2;
              end;
              if (DevEx_NavBar.Groups[I].Caption = 'Tablas')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=3;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=3;
              end;
               if (DevEx_NavBar.Groups[I].Caption = 'Sistema')  then
              begin
                    DevEx_NavBar.Groups[I].LargeImageIndex:=4;
                    DevEx_NavBar.Groups[I].SmallImageIndex:=4;
              end;
              if (I = 0) then
              begin
                    if(DevEx_NavBar.Groups.Count = 1) then
                    begin
                          DevEx_NavBar.Groups[0].LargeImageIndex:=4;
                          DevEx_NavBar.Groups[0].SmallImageIndex:=4;
                    end
                    else
                    begin
                          if (DevEx_NavBar.Groups[1].Caption = 'Registros')  then
                          begin
                                DevEx_NavBar.Groups[0].LargeImageIndex:=0;
                                DevEx_NavBar.Groups[0].SmallImageIndex:=0;
                          end;
                    end;
              end;
              DevEx_NavBar.Groups[I].UseSmallImages:=false;
        end;
  end
  else
  begin
        if(DevEx_NavBar.Groups.Count >0) then
        begin
              DevEx_NavBar.Groups[0].UseSmallImages:=false;
              DevEx_NavBar.Groups[0].LargeImageIndex:=4;
              DevEx_NavBar.Groups[0].SmallImageIndex:=4;
        end;
  end;
end;

end.
