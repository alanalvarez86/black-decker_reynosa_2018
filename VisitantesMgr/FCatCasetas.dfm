inherited CatCasetas: TCatCasetas
  Left = 338
  Top = 299
  Caption = 'Casetas'
  ClientWidth = 587
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 587
    inherited ValorActivo2: TPanel
      Width = 328
    end
  end
  object ZetaDBGrid1: TZetaDBGrid [1]
    Left = 0
    Top = 19
    Width = 587
    Height = 254
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'CA_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_UBICA'
        Title.Caption = 'Ubicaci'#243'n'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_STATUS'
        Title.Caption = 'Status'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_CORTE'
        Title.Caption = 'Ultimo Corte'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CA_ABIERTA'
        Title.Caption = #191'Abierta?'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RE_NOMBRE'
        Title.Caption = 'Reportes'
        Width = 200
        Visible = True
      end>
  end
end
