unit FBuscaVisitantes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FBaseBusquedas, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons, ExtCtrls,
  ZetaKeyCombo, ZetaKeyLookup, ZetaCommonLists, Db, ImgList;

type
  TBuscaVisitantes = class(TBaseBusquedas)
    GroupBox1: TGroupBox;
    lblTipo: TLabel;
    lblStatus: TLabel;
    VI_TIPO: TZetaKeyLookup;
    VI_STATUS: TZetaKeyCombo;
    lblEmpresa: TLabel;
    EV_NUMERO: TZetaKeyLookup;
    lblAnfitrion: TLabel;
    AN_NUMERO: TZetaKeyLookup;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edNombreChange(Sender: TObject);
    procedure btnAvzClick(Sender: TObject);
    procedure btnBuscarClick(Sender: TObject);
    procedure zGridBuscaDblClick(Sender: TObject);
  private
    { Private declarations }
    FNombre: String;
    FVisitante: Integer;
    procedure LlenaStatusVisitante( oLista: TStrings; oLFijas: ListasFijas );
  public
    { Public declarations }
  end;

var
  BuscaVisitantes: TBuscaVisitantes;

const
     K_ALTURA_SGRID = 110;
     K_ALTURA_CGRID = 380;
     K_ALTURA_CAVZ  = 225;

function BuscaVisitanteDialogo( var iVisitante:Integer; var sNombre: String ): Boolean;

implementation
uses DVisitantes, ZetaCommonTools, ZetaCommonClasses, ZetaDialogo, ZVisitantesTools;

{$R *.DFM}
function BuscaVisitanteDialogo( var iVisitante:Integer; var sNombre: String ): Boolean;
begin
     Result := False;
     if ( BuscaVisitantes = nil ) then
        BuscaVisitantes := TBuscaVisitantes.Create( Application );
     try
        if ( BuscaVisitantes <> nil ) then
        begin
             with BuscaVisitantes do
             begin
                  ShowModal;
                  if ( ModalResult = mrOk ) then
                  begin
                       sNombre := FNombre;
                       iVisitante := FVisitante;
                       Result := True;
                  end;
             end;
        end;
     finally
            FreeAndNil( BuscaVisitantes );
     end;
end;

procedure TBuscaVisitantes.FormCreate(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          VI_TIPO.LookupDataset := cdsTipoVisita;
          AN_NUMERO.LookupDataset := cdsAnfitrion;
          EV_NUMERO.LookupDataset := cdsEmpVisitante;
     end;
     with VI_STATUS do
     begin
          LlenaStatusVisitante( Lista, lfVisitanteStatus );
          ItemIndex := 1;
     end;
//     HelpContext:= ;
end;

procedure TBuscaVisitantes.FormShow(Sender: TObject);
begin
     inherited;
     with dmVisitantes do
     begin
          cdsTipoVisita.Conectar;
          cdsAnfitrion.Conectar;
          cdsEmpVisitante.Conectar;
     end;
     Self.Height := K_ALTURA_SGRID;
     btnAceptar.Enabled := FALSE;
end;

procedure TBuscaVisitantes.edNombreChange(Sender: TObject);
begin
     inherited;
     btnAceptar.Enabled := strLleno( edNombre.Text );
end;

procedure TBuscaVisitantes.btnAvzClick(Sender: TObject);
begin
     inherited;
     pnlAvz.Visible := not pnlAvz.Visible;
     if( pnlAvz.Visible ) then
     begin
          if( zGridBusca.Visible )then
             Self.Height := K_ALTURA_CGRID
          else
              Self.Height := K_ALTURA_CAVZ;
     end
     else
     begin
          if( zGridBusca.Visible )then
             Self.Height := K_ALTURA_CGRID
          else
              Self.Height := K_ALTURA_SGRID;
     end;
     VI_TIPO.Llave := VACIO;
     AN_NUMERO.Llave := VACIO;
     EV_NUMERO.Llave := VACIO;
     VI_STATUS.ItemIndex := 1;
end;

procedure TBuscaVisitantes.LlenaStatusVisitante( oLista: TStrings; oLFijas: ListasFijas );
var
   eValueStatus: eVisitanteStatus;
   i: Integer;
   FTemp: TStringList;
begin
     FTemp := TStringList.Create;
     try
          with FTemp do
          begin
               BeginUpdate;
               try
                  for eValueStatus := Low( eVisitanteStatus ) to High( eVisitanteStatus ) do
                       AddObject( ObtieneElemento( oLFijas, Ord( eValueStatus ) ), TObject( Ord( eValueStatus ) ) );
               finally
                      EndUpdate;
               end;
          end;
          with oLista do
          begin
               BeginUpdate;
               try
                  Clear;
                  for i := 0 to ( FTemp.Count ) do
                  begin
                       if ( i = 0 ) then
                           Add( '0=Todos' )
                       else
                           Add( IntToStr( ( Integer( FTemp.Objects[ i - 1 ] ) ) + 1 ) + '=' + FTemp.Strings[ i - 1 ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
     finally
            FreeAndNil( FTemp );
     end;
end;


procedure TBuscaVisitantes.btnBuscarClick(Sender: TObject);
var
   FParamList: TZetaParams;
begin
     FParamList := TZetaParams.Create;
     try
        FParamList.AddString( 'NOMBRE', UPPERCASE( edNombre.Text ) );
        FParamList.AddString( 'TIPO', UPPERCASE( VI_TIPO.Llave ) );
        FParamList.AddString( 'ANFITRION', UPPERCASE( AN_NUMERO.Llave ) );
        FParamList.AddString( 'EMPRESA', UPPERCASE( EV_NUMERO.Llave ) );
        FParamList.AddInteger('STATUS', VI_STATUS.ItemIndex );
        with dmVisitantes do
        begin
             HacerBusquedaVisitante( FParamList );
             DataSource.DataSet := cdsVisitanteLookup;
             if ( cdsVisitanteLookup.IsEmpty ) then 
             begin
                  //Height := K_ALTURA_SGRID;
                  //ZGridBusca.Visible := False;
                  //pnlAvz.Visible := False;
                  //btnAvz.Down := False;
                  ZetaDialogo.ZInformation('Información','No Existe Información Con Los Datos Requeridos', 0);
             end
             else
             begin
                  Height := K_ALTURA_CGRID;
                  ZGridBusca.Visible := True;
             end;
             btnAceptar.Enabled := ZGridBusca.Visible;
        end;
     finally
            FreeAndNil( FParamList );
     end;
end;

procedure TBuscaVisitantes.zGridBuscaDblClick(Sender: TObject);
begin
     inherited;
     with dmVisitantes.cdsVisitanteLookup do
     begin
          if Active then
          begin
               FNombre := FieldByName('Pretty_Visit').AsString;
               FVisitante := FieldByName('VI_NUMERO').AsInteger;
               ModalResult := mrOk;
          end
          else
              ModalResult := mrOk;
     end;
end;

end.
