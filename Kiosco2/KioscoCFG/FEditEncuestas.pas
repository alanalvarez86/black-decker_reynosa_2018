unit FEditEncuestas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicionRenglon, DB, Grids, DBGrids, ZetaDBGrid, ComCtrls,
  ExtCtrls, DBCtrls, ZetaSmartLists, Buttons, StdCtrls, ZetaEdit,
  ZetaKeyLookup, Mask, ZetaNumero, ZetaKeyCombo,
  ZetaCommonLists,
  DKiosco, ZetaFecha, CheckLst, ZetaHora, ZetaDBTextBox;

type
  TEdicionEncuestas = class(TBaseEdicionRenglon)
    Panel3: TPanel;
    bArriba: TZetaSmartListsButton;
    bAbajo: TZetaSmartListsButton;
    ENC_NOMBRE: TDBEdit;
    ENC_STATUS: TZetaDBKeyCombo;
    Label1: TLabel;
    Label2: TLabel;
    TB_ELEMENTlbl: TLabel;
    Filtros: TTabSheet;
    Resultados: TTabSheet;
    GBKioscos: TGroupBox;
    GBEmpresas: TGroupBox;
    GBScripts: TGroupBox;
    chkEmpresas: TCheckListBox;
    Label7: TLabel;
    Label8: TLabel;
    KioscoAdd: TZetaEdit;
    KioscoExcluidos: TListBox;
    Label9: TLabel;
    btnExcluir: TZetaSmartListsButton;
    GroupBox7: TGroupBox;
    GridResultados: TZetaDBGrid;
    Label10: TLabel;
    Label13: TLabel;
    ENC_FILTRO: TDBMemo;
    ENC_PREGUN: TDBMemo;
    btnRemoveExcluido: TSpeedButton;
    GBVigencia: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    ENC_FECINI: TZetaDBFecha;
    ENC_FECFIN: TZetaDBFecha;
    ENC_HORINI: TZetaDBHora;
    ENC_HORFIN: TZetaDBHora;
    GBOpciones: TGroupBox;
    Label5: TLabel;
    ENC_MSJCON: TZetaDBEdit;
    ENC_VOTOSE: TDBCheckBox;
    ENC_OPCION: TDBCheckBox;
    ENC_MRESUL: TDBCheckBox;
    VOTOS: TZetaDBTextBox;
    OPCION: TZetaDBTextBox;
    Label6: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bArribaClick(Sender: TObject);
    procedure bAbajoClick(Sender: TObject);
    procedure btnExcluirClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure chkEmpresasClickCheck(Sender: TObject);
    procedure btnRemoveExcluidoClick(Sender: TObject);
  private
    { Private declarations }
    procedure SeleccionaPrimerColumna;
    procedure SeleccionaSiguienteRenglon;
    procedure SubirBajarOpcion(eAccion: eSubeBaja);
    procedure CargaListaEmpresas;
    procedure HabilitaCamposStatus( const Status: eStatusEncuesta );
    procedure GrabarEmpresas;
    procedure GrabarExcluidos;
    procedure SetListaEmpresasAutorizadas(const Value: string);
    procedure CargarExcluidos(const sExcluidos: string);
  protected
    { Protected declarations }
    function PosicionaSiguienteColumna: Integer;override;
    procedure Connect;override;
    procedure DoCancelChanges;override;
    procedure KeyPress(var Key: Char);override;
    procedure EscribirCambios; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  EdicionEncuestas: TEdicionEncuestas;

implementation

uses
    ZetaDialogo,
    DCliente,
    ZetaCommonClasses,
    ZetaCommonTools;

{$R *.dfm}

const
     PRIMER_COLUMNA = 0;

procedure TEdicionEncuestas.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := 0; //D_CAT_CAPA_SESIONES;
     HelpContext:= 0;//H60647_Sesiones;
     FirstControl := ENC_NOMBRE;
     Datos.Visible := false;
end;

procedure TEdicionEncuestas.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.Pages[0].TabVisible  := False;
     if ( eStatusEncuesta( DataSource.DataSet.FieldByName( 'ENC_STATUS' ).AsInteger ) = stenDiseno ) then
        PageControl.ActivePage := Tabla
     else
        PageControl.ActivePage := Resultados;
end;

procedure TEdicionEncuestas.Connect;
begin
     with dmKiosco do
     begin
          DataSource.DataSet:= cdsEncuestas;
          dsRenglon.DataSet := cdsOpcionesEncuesta;
          CargaListaEmpresas;
          SetListaEmpresasAutorizadas( cdsEncuestas.FieldByName('ENC_COMPAN').AsString );
          CargarExcluidos(cdsEncuestas.FieldByName('ENC_KIOSCS').AsString);
     end;
end;

procedure TEdicionEncuestas.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then
     begin
          HabilitaCamposStatus( eStatusEncuesta( DataSource.DataSet.FieldByName( 'ENC_STATUS' ).AsInteger ) );
     end;
end;

procedure TEdicionEncuestas.HabilitaCamposStatus( const Status: eStatusEncuesta );
begin
     ENC_NOMBRE.Enabled := ( Status = stenDiseno );
     ENC_PREGUN.Enabled := ( Status = stenDiseno );
     BBAgregar.Enabled := ( Status = stenDiseno );
     BBBorrar.Enabled := ( Status = stenDiseno );
     BBModificar.Enabled := ( Status = stenDiseno );
     bArriba.Enabled := ( Status = stenDiseno );
     bAbajo.Enabled := ( Status = stenDiseno );
     GridRenglones.ReadOnly := ( Status <> stenDiseno );
     ENC_FECINI.Enabled := ( Status <> stenCerrada );
     ENC_HORINI.Enabled := ( Status <> stenCerrada );
     ENC_FECFIN.Enabled := ( Status <> stenCerrada );
     ENC_HORFIN.Enabled := ( Status <> stenCerrada );
     //ENC_CONFID.Enabled := ( Status <> stenCerrada );
     ENC_OPCION.Enabled := ( Status <> stenCerrada );
     ENC_MRESUL.Enabled := ( Status <> stenCerrada );
     ENC_MSJCON.Enabled := ( Status <> stenCerrada );
     ENC_FILTRO.Enabled := ( Status <> stenCerrada );
     chkEmpresas.Enabled := ( Status <> stenCerrada );
     KioscoAdd.Enabled := ( Status <> stenCerrada );
     KioscoExcluidos.Enabled := ( Status <> stenCerrada );
     btnExcluir.Enabled := ( Status <> stenCerrada );
     btnRemoveExcluido.Enabled := ( Status <> stenCerrada );
end;

procedure TEdicionEncuestas.DoCancelChanges;
begin
     with dmKiosco do
     begin
          cdsOpcionesEncuesta.CancelUpdates;
          inherited DoCancelChanges;
          CargaListaEmpresas;
          SetListaEmpresasAutorizadas(cdsEncuestas.FieldByName('ENC_COMPAN').AsString);
          CargarExcluidos(cdsEncuestas.FieldByName('ENC_KIOSCS').AsString);
     end;
end;

procedure TEdicionEncuestas.SeleccionaSiguienteRenglon;
begin
     with dmKiosco.cdsOpcionesEncuesta do
     begin
          Next;
          if Eof then
          begin
               SeleccionaPrimerColumna;
               Agregar;
          end
          else
              GridRenglones.SelectedIndex := 0;
     end;
end;

procedure TEdicionEncuestas.KeyPress(var Key: Char);
begin
     if ( GridEnfocado ) then
     begin
          if ( Key = Chr( VK_RETURN ) ) then
          begin
               Key := #0;
               GridRenglones.SelectedIndex := PosicionaSiguienteColumna;
               if ( GridRenglones.SelectedIndex = 0 ) then
                  SeleccionaSiguienteRenglon;
          end
          else
              if ( Key = Chr( VK_ESCAPE ) ) then
              begin
                   SeleccionaPrimerColumna;
              end;
     end
     else if ActiveControl.TabStop then
          inherited KeyPress( Key );
end;

function TEdicionEncuestas.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
               Result := 0
          else
              Result := i;
     end;
end;

procedure TEdicionEncuestas.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ PRIMER_COLUMNA ].Field;   // Posicionar en la primer columna
end;

procedure TEdicionEncuestas.bArribaClick(Sender: TObject);
begin
     inherited;
     SubirBajarOpcion(eSubir);
end;

procedure TEdicionEncuestas.bAbajoClick(Sender: TObject);
begin
     inherited;
     SubirBajarOpcion(eBajar);
end;

procedure TEdicionEncuestas.SubirBajarOpcion(eAccion: eSubeBaja);
const
     K_PRIMERO = 1;
var
   iPosActual, iNuevaPos: Integer;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmKiosco do
        begin
             iPosActual := dmKiosco.cdsOpcionesEncuesta.RecNo;
             case( eAccion ) of
                   eSubir: begin
                                if( iPosActual = K_PRIMERO )then
                                    ZetaDialogo.ZError( self.Caption, '� Es el inicio de la lista de opciones !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual - 1;
                                     CambiaOrdenOpcionesEncuesta( iPosActual, iNuevaPos );
                                end;
                           end;
                   eBajar: begin
                                if( iPosActual = cdsOpcionesEncuesta.RecordCount  )then
                                    ZetaDialogo.ZError( self.Caption, '� Es el final de la lista de opciones !', 0 )
                                else
                                begin
                                     iNuevaPos := iPosActual + 1;
                                     CambiaOrdenOpcionesEncuesta( iPosActual, iNuevaPos );
                                end;
                           end;
             else
                 ZetaDialogo.ZError( self.Caption, '� Operaci�n no v�lida !', 0 )
             end;
        end;
     finally
             Screen.Cursor := oCursor;
     end;
     Application.ProcessMessages;
end;

procedure TEdicionEncuestas.EscribirCambios;
begin
     GrabarEmpresas;
     GrabarExcluidos;
     inherited;
end;

procedure TEdicionEncuestas.CargaListaEmpresas;
begin
     with dmCliente do
     begin
          CargaListaEmpresas( chkEmpresas );
     end;
end;

procedure TEdicionEncuestas.SetListaEmpresasAutorizadas( const Value: string);
var
   oLista: TStrings;
   i, iPos: integer;
begin
     with chkEmpresas do
     begin
          for i:= 0 to Items.Count -1 do
              Checked[i] :=  FALSE;
     end;
     oLista := TStringList.Create;
     try
        oLista.Commatext := Value;
        for i:=0 to oLista.Count -1 do
        begin
             iPos := chkEmpresas.Items.IndexOfName(oLista[i]);
             if iPos >= 0 then
                chkEmpresas.Checked[iPos] := True;
        end;
     finally
            FreeAndNil(oLista);
     end;
end;

procedure TEdicionEncuestas.GrabarEmpresas;

   function GetListaEmpresasAutorizadas: string;
   var
      i: integer;
      sCodigo: string;
   begin
        Result := VACIO;
        with chkEmpresas do
        begin
             for i:= 0 to Items.Count - 1 do
             begin
                  if Checked[i] then
                  begin
                       sCodigo := Items.Names[i];
                       Result := ConcatString( Result,  sCodigo, ',' );
                  end;
             end;
        end;
   end;

begin
     dmKiosco.cdsEncuestas.FieldByName('ENC_COMPAN').AsString := GetListaEmpresasAutorizadas;
end;

{
procedure TEdicionEncuestas.CargaListaEmpresas;
var
   sCodigo: String;
begin
     with chkEmpresas.Items do
     begin
          BeginUpdate;
          try
             Clear;
             with dmCliente.cdsCompany do
             begin
                  Data := dmCliente.Servidor.GetCompanys( 0, Ord( tc3Datos ) );
                  while not Eof do
                  begin
                       if StrLleno( FieldByName( 'CM_DIGITO' ).AsString ) then
                       begin
                            sDigito := FieldByName( 'CM_CODIGO' ).AsString;
                            AddObject( Format( '(%s) %s:%s', [ FieldByName( 'CM_DIGITO' ).AsString, FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ), TObject( sCodigo ) ) );
                       end;
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;
}

procedure TEdicionEncuestas.GrabarExcluidos;
var
   sLista:string;
   i:Integer;
begin
     sLista := VACIO;
     for i := 0 to KioscoExcluidos.Items.Count - 1 do
     begin
          sLista := ConcatString( sLista, KioscoExcluidos.Items.Strings[i], ',' );
     end;
     dmKiosco.cdsEncuestas.FieldByName('ENC_KIOSCS').AsString := sLista;
end;

procedure TEdicionEncuestas.btnExcluirClick(Sender: TObject);
begin
     inherited;
     KioscoExcluidos.Items.Add(KioscoAdd.Text);
     KioscoAdd.Text := VACIO;
     dmKiosco.cdsEncuestas.Edit;
end;

procedure TEdicionEncuestas.CargarExcluidos(const sExcluidos:string);
var
    oLista: TStrings;
begin
     KioscoExcluidos.Items.Clear;

     oLista := TStringList.Create;
     try
        oLista.Commatext := sExcluidos;
        KioscoExcluidos.Items.AddStrings(oLista);
     finally
            FreeAndNil(oLista);
     end;
end;

procedure TEdicionEncuestas.chkEmpresasClickCheck(Sender: TObject);
begin
     inherited;
     dmKiosco.cdsEncuestas.Edit;
end;

procedure TEdicionEncuestas.btnRemoveExcluidoClick(Sender: TObject);
begin
     inherited;
     KioscoExcluidos.Items.Delete(KioscoExcluidos.ItemIndex );
     dmKiosco.cdsEncuestas.Edit;
end;

procedure TEdicionEncuestas.Agregar;
begin
     if GridEnfocado then
        BBAgregar.Click;
end;

procedure TEdicionEncuestas.Borrar;
begin
     if GridEnfocado then
        BBBorrar.Click;
end;

procedure TEdicionEncuestas.Modificar;
begin
     if GridEnfocado then
        BBModificar.Click;
end;

end.
