program KioscoCFG;

uses
  MidasLib,
  Forms,
  ZetaSplash,
  ZetaClientTools,
  DBaseSistema in '..\..\DataModules\DBaseSistema.pas' {dmBaseSistema: TDataModule},
  ZBaseConsulta in '..\..\Tools\ZBaseConsulta.pas' {BaseConsulta},
  ZBaseConsultaBotones in '..\..\Tools\ZBaseConsultaBotones.pas' {BaseBotones},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  ZBaseEdicion in '..\..\Tools\ZBaseEdicion.pas' {BaseEdicion},
  ZBaseEdicionRenglon in '..\..\Tools\ZBaseEdicionRenglon.pas' {BaseEdicionRenglon},
  ZBaseGlobal in '..\..\Tools\ZBaseGlobal.pas' {BaseGlobal},
  FConfiguracion in 'FConfiguracion.pas' {Configuracion},
  FTressShell in 'FTressShell.pas' {TressShell},
  FLookupReporte in 'FLookupReporte.pas' {FormaLookupReporte},
  FReseteaPass in 'FReseteaPass.pas' {ReseteaPass},
  DSistema in 'DSistema.pas' {dmSistema: TDataModule};

{$R *.RES}
{$R WINDOWSXP.RES}

procedure CierraSplash;
begin
     with SplashScreen do
     begin
          Close;
          Free;
     end;
end;
  begin
  ZetaClientTools.InitDCOM;
  Application.Initialize;
  SplashScreen := TSplashScreen.Create( Application );
  with SplashScreen do
  begin
       Show;
       Update;
  end;
  Application.Title := 'KioscoCFG Tress';
  Application.CreateForm(TTressShell, TressShell);
  with TressShell do
  begin
       if Login( True ) then
       begin
            CierraSplash;
            Show;
            Update;
            BeforeRun;
            Application.Run;
       end
       else
       begin
            CierraSplash;
       end;
       Free;
  end;
end.
