inherited TOEncuestas: TTOEncuestas
  Left = 204
  Top = 245
  Caption = 'Encuestas'
  ClientHeight = 442
  ClientWidth = 900
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 900
    inherited ValorActivo2: TPanel
      Width = 641
    end
  end
  object ZetaDBGrid: TZetaDBGrid [1]
    Left = 0
    Top = 49
    Width = 900
    Height = 393
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ENC_CODIGO'
        Title.Caption = 'C'#243'digo'
        Width = 39
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENC_NOMBRE'
        Title.Caption = 'Nombre'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENC_PREGUN'
        Title.Caption = 'Pregunta'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENC_STATUS'
        Title.Caption = 'Status'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENC_FECINI'
        Title.Caption = 'Inicio'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ENC_FECFIN'
        Title.Caption = 'Termina'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OPCION'
        Title.Caption = 'Opci'#243'n Ganadora'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VOTOS'
        Title.Caption = 'Votaci'#243'n'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_DESCRIP'
        Title.Caption = 'Modific'#243
        Width = 90
        Visible = True
      end>
  end
  object PanelFiltros: TPanel [2]
    Left = 0
    Top = 19
    Width = 900
    Height = 30
    Align = alTop
    TabOrder = 2
    DesignSize = (
      900
      30)
    object Label3: TLabel
      Left = 734
      Top = 8
      Width = 33
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Status:'
      FocusControl = cbStatus
    end
    object EdadMinimaLBL: TLabel
      Left = 18
      Top = 8
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fechas:'
      FocusControl = FechaInicial
    end
    object EdadMaximaLBL: TLabel
      Left = 182
      Top = 8
      Width = 10
      Height = 13
      Caption = 'A:'
      FocusControl = FechaFinal
    end
    object cbStatus: TComboBox
      Left = 770
      Top = 4
      Width = 120
      Height = 21
      Style = csDropDownList
      Anchors = [akTop, akRight]
      ItemHeight = 13
      TabOrder = 0
      OnChange = cbStatusChange
    end
    object FechaInicial: TZetaFecha
      Left = 58
      Top = 4
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '23/Mar/06'
      UseEnterKey = True
      Valor = 38799.000000000000000000
    end
    object FechaFinal: TZetaFecha
      Left = 198
      Top = 4
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 2
      Text = '23/Mar/06'
      UseEnterKey = True
      Valor = 38799.000000000000000000
    end
    object FILTRAR: TBitBtn
      Left = 323
      Top = 3
      Width = 120
      Height = 25
      Hint = 'Filtrar fechas de Encuestas'
      Caption = '&Filtrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = cbStatusChange
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
    end
  end
end
