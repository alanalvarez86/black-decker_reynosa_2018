unit FCamposMgr;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DB, DBCtrls, ComCtrls,
     ZetaNumero,
     ZetaEdit,
     ZetaFecha,
     ZetaPortalClasses,
     ZetaCommonLists;

type
  TContenido = class( TObject )
  private
    { Private declarations }
    FClase: String;
    FMostrar: Integer;
    FOrden: Integer;
    FReporte: Integer;
    FTipo: eContenido;
    FDescripcion: String;
    function GetNombre: String;
    procedure SetTipo(const Value: eContenido);
    procedure SetOrden(const Value: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Clase: String read FClase write FClase;
    property Mostrar: Integer read FMostrar write FMostrar;
    property Orden: Integer read FOrden write SetOrden;
    property Reporte: Integer read FReporte write FReporte;
    property Tipo: eContenido read FTipo write SetTipo;
    property Nombre: String read GetNombre;
    property Descripcion:String read FDescripcion write FDescripcion;
    function Comparar(Value: TContenido): Integer;
    procedure Assign(Value: TContenido);
  end;
  TPagina = class( TObject )
  private
    { Private declarations }
    FAnterior: Integer;
    FColumnas: Integer;
    FOrden: Integer;
    FTitulo: String;
    function GetTitulo: String;
    procedure SetColumnas(const Value: Integer);
    procedure SetOrden(const Value: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Anterior: Integer read FAnterior;
    property Columnas: Integer read FColumnas write SetColumnas;
    property Orden: Integer read FOrden write SetOrden;
    property Titulo: String read GetTitulo write FTitulo;
    function Comparar(Value: TPagina): Integer;
    function EsNueva: Boolean;
    procedure Assign(Value: TPagina);
    procedure ResetAnterior;
  end;
  TListaPaginas = class( TObject )
  private
    { Private declarations }
    FItems: TList;
    function GetPagina(Index: Integer): TPagina;
    procedure Delete(const Index: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Pagina[ Index: Integer ]: TPagina read GetPagina;
    function AddPagina( const iOrden, iColumnas: Integer; const iAnterior: Integer = K_NULL_PAGINA; const sTitulo: String = '' ): TPagina;
    function AgregaPagina(oPagina: TPagina): Integer;
    function Count: Integer;
    function GetIndexOf(const iOrden: Integer): Integer;
    function GetIndexOfAnterior(const iAnterior: Integer): Integer;
    procedure Assign(Source: TListaPaginas);
    procedure Borrar(const iOrden: Integer);
    procedure Clear;
    procedure Cargar(Lista: TStrings);
    procedure Merge( Nueva: TListaPaginas );
    procedure Ordena;
  end;
  TListaContenidos = class( TObject )
  private
    { Private declarations }
    FColumnaActiva: Integer;
    FContenidos: TList;
    FPaginas: TListaPaginas;
    FPaginaActiva: Integer;
    FUsuario: Integer;
    function GetContenido(Index: Integer): TContenido;
    procedure Delete(const Index: Integer);
    procedure SetColumnaActiva(const Value: Integer);
    procedure SetPaginaActiva(const Value: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property ColumnaActiva: Integer read FColumnaActiva write SetColumnaActiva;
    property Contenido[ Index: Integer ]: TContenido read GetContenido;
    property PaginaActiva: Integer read FPaginaActiva write SetPaginaActiva;
    property Paginas: TListaPaginas read FPaginas;
    property Usuario: Integer read FUsuario write FUsuario;
    function AddContenido(const eTipo: eContenido; const iOrden, iMostrar: Integer; const iReporte: Integer; const sClase,sDescripcion: String ): TContenido;
    function AgregaContenido( oContenido: TContenido ): Integer;
    function Count: Integer;
    procedure Assign(Source: TListaContenidos);
    procedure Borrar( const iOrden: Integer );
    procedure Clear;
    procedure Cargar(Lista: TStrings);
    procedure Ordena;
  end;

implementation

uses ZetaCommonTools,
     ZetaCommonClasses;

const
     K_MENOR = -1;
     K_IGUAL = 0;
     K_MAYOR = 1;

function ComparaContenidos( Item1, Item2: Pointer ): Integer;
begin
     Result := TContenido( Item1 ).Comparar( TContenido( Item2 ) );
end;

function ComparaPaginas( Item1, Item2: Pointer ): Integer;
begin
     Result := TPagina( Item1 ).Comparar( TPagina( Item2 ) );
end;

{ ******** TContenido ********** }

constructor TContenido.Create;
begin
    FTipo := eContenido( 0 );
    FClase := '';
    FMostrar := 3;
    FOrden := 0;
    FReporte := 0;
end;

destructor TContenido.Destroy;
begin
     inherited Destroy;
end;

procedure TContenido.Assign( Value: TContenido );
begin
     Tipo := Value.Tipo;
     Clase := Value.Clase;
     Mostrar := Value.Mostrar;
     Orden := Value.Orden;
     Reporte := Value.Reporte;
     Descripcion := Value.Descripcion;
end;

procedure TContenido.SetOrden(const Value: Integer);
begin
     FOrden := ZetaCommonTools.iMax( 1, Value );
end;

procedure TContenido.SetTipo(const Value: eContenido);
begin
     FTipo := Value;
end;

function TContenido.Comparar(Value: TContenido): Integer;
begin
     if ( Self.Orden < Value.Orden ) then
        Result := K_MENOR
     else
         if ( Self.Orden > Value.Orden ) then
            Result := K_MAYOR
         else
             Result := K_IGUAL;
end;

function TContenido.GetNombre: String;

function FormatNombre( const sValue: String ): String;
begin
     if ZetaCommonTools.StrLleno( Descripcion ) then
        Result := sValue + ' - ' + Descripcion
     else
         Result := sValue;
end;

begin
     case FTipo of
          ecReporte: Result := FormatNombre( 'Reporte' );
          ecGrafica: Result := FormatNombre( 'Gr�fica' );
          ecTitular: Result := FormatNombre( 'Titular' );
          ecNoticia: Result := FormatNombre( 'Noticia' );
          ecEvento: Result := FormatNombre( 'Evento' );
     else
         Result := '';
     end;
end;

{ ****** TPagina **** }

constructor TPagina.Create;
begin
     FOrden := 0;
     FAnterior := K_NULL_PAGINA;
     FColumnas := K_MIN_COLUMNAS;
     FTitulo := '';
end;

destructor TPagina.Destroy;
begin
     inherited Destroy;
end;

function TPagina.EsNueva: Boolean;
begin
     Result := ( FAnterior = K_NULL_PAGINA );
end;

function TPagina.GetTitulo: String;
begin
     if ZetaCommonTools.StrLleno( FTitulo ) then
        Result := FTitulo
     else
         if ( FOrden >= K_MIN_PAGINAS ) then
            Result := Format( 'P�gina %d (Default)', [ FOrden ] )
         else
             Result := 'P�gina Nueva';
end;

procedure TPagina.SetOrden(const Value: Integer);
begin
     FOrden := ZetaCommonTools.iMax( K_MIN_PAGINAS, ZetaCommonTools.iMin( Value, K_MAX_PAGINAS ) );
end;

procedure TPagina.SetColumnas(const Value: Integer);
begin
     FColumnas := ZetaCommonTools.iMax( K_MIN_COLUMNAS, ZetaCommonTools.iMin( Value, K_MAX_COLUMNAS ) );
end;

function TPagina.Comparar(Value: TPagina): Integer;
begin
     if ( Self.Orden < Value.Orden ) then
        Result := K_MENOR
     else
         if ( Self.Orden > Value.Orden ) then
            Result := K_MAYOR
         else
             Result := K_IGUAL;
end;

procedure TPagina.Assign(Value: TPagina);
begin
     Orden := Value.Orden;
     Titulo := Value.Titulo;
     Columnas := Value.Columnas;
     FAnterior := Value.Anterior;
end;

procedure TPagina.ResetAnterior;
begin
     FAnterior := Orden;
end;

{ ********* TListaContenidos *********** }

constructor TListaContenidos.Create;
begin
    FContenidos := TList.Create;
    FPaginas := TListaPaginas.Create;
end;

destructor TListaContenidos.Destroy;
begin
     Clear;
     FreeAndNil( FPaginas );
     FreeAndNil( FContenidos );
     inherited Destroy;
end;

procedure TListaContenidos.Assign(Source: TListaContenidos);
var
   i: Integer;
begin
     Self.Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Contenido[ i ] do
               begin
                    Self.AddContenido( Tipo, Orden, Mostrar, Reporte, Clase, Descripcion );
               end;
          end;
     end;
     FPaginas.Assign( Source.Paginas );
end;

procedure TListaContenidos.Clear;
var
   i: Integer;
begin
     with FContenidos do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaContenidos.Count: Integer;
begin
     Result := FContenidos.Count;
end;

procedure TListaContenidos.Delete(const Index: Integer);
begin
     Contenido[ Index ].Free;
     FContenidos.Delete( Index );
end;

function TListaContenidos.GetContenido(Index: Integer): TContenido;
begin
     with FContenidos do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TContenido( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaContenidos.SetColumnaActiva(const Value: Integer);
begin
     FColumnaActiva := ZetaCommonTools.iMax( K_MIN_COLUMNAS, ZetaCommonTools.iMin( Value, FPaginas.Pagina[ FPaginaActiva - 1 ].Columnas ) );
end;

procedure TListaContenidos.SetPaginaActiva(const Value: Integer);
begin
     FPaginaActiva := ZetaCommonTools.iMax( K_MIN_PAGINAS, ZetaCommonTools.iMin( Value, FPaginas.Count ) );
end;

procedure TListaContenidos.Ordena;
begin
     FContenidos.Sort( ComparaContenidos );
end;

procedure TListaContenidos.Cargar(Lista: TStrings);
var
   i: Integer;
begin
     Ordena;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  Lista.AddObject( Contenido[ i ].Nombre , Contenido[ i ] )
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

function TListaContenidos.AgregaContenido( oContenido: TContenido ): Integer;
var
   oNew: TContenido;
begin
     oNew := TContenido.Create;
     try
        FContenidos.Add( oNew );
        with oNew do
        begin
             Assign( oContenido );
             Orden := Count;
             Result := Orden - 1;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( oNew );
                raise;
           end;
     end;
end;

function TListaContenidos.AddContenido(const eTipo: eContenido; const iOrden, iMostrar: Integer; const iReporte: Integer; const sClase,sDescripcion: String ): TContenido;
begin
     Result := TContenido.Create;
     try
        FContenidos.Add( Result );
        with Result do
        begin
             Tipo := eTipo;
             Orden := iOrden;
             Mostrar := iMostrar;
             Reporte := iReporte;
             Clase := sClase;
             Descripcion := sDescripcion;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TListaContenidos.Borrar( const iOrden: Integer );
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Contenido[ i ].Orden = iOrden ) then
          begin
               Delete( i );
               Break;
          end;
     end;
end;

{ ******** TListaPaginas ****** }

constructor TListaPaginas.Create;
begin
     FItems := TList.Create;
end;

destructor TListaPaginas.Destroy;
begin
     Clear;
     FreeAndNil( FItems );
     inherited;
end;

procedure TListaPaginas.Clear;
var
   i: Integer;
begin
     with FItems do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaPaginas.Count: Integer;
begin
     Result := FItems.Count;
end;

procedure TListaPaginas.Delete(const Index: Integer);
begin
     Pagina[ Index ].Free;
     FItems.Delete( Index );
end;

function TListaPaginas.GetPagina(Index: Integer): TPagina;
begin
     with FItems do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TPagina( Items[ Index ] )
          else
              Result := nil;
     end;
end;

function TListaPaginas.GetIndexOf(const iOrden: Integer): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Pagina[ i ].Orden = iOrden ) then
          begin
               Result := i;
               Break;
          end;
     end;
end;

function TListaPaginas.GetIndexOfAnterior(const iAnterior: Integer): Integer;
var
   i: Integer;
begin
     Result := -1;
     for i := 0 to ( Count - 1 ) do
     begin
          if ( Pagina[ i ].Anterior = iAnterior ) then
          begin
               Result := i;
               Break;
          end;
     end;
end;

function TListaPaginas.AddPagina( const iOrden, iColumnas: Integer; const iAnterior: Integer = K_NULL_PAGINA; const sTitulo: String = '' ): TPagina;
begin
     Result := TPagina.Create;
     try
        FItems.Add( Result );
        with Result do
        begin
             Orden := iOrden;
             FAnterior := iAnterior;
             Columnas := iColumnas;
             Titulo := sTitulo;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

function TListaPaginas.AgregaPagina(oPagina: TPagina): Integer;
var
   oNewPage: TPagina;
begin
     oNewPage := TPagina.Create;
     try
        FItems.Add( oNewPage );
        with oNewPage do
        begin
             Assign( oPagina );
             Orden := Count;
             Result := Orden - 1;
        end;
     except
           on Error: Exception do
           begin
                FreeAndNil( oNewPage );
                raise;
           end;
     end;
end;

procedure TListaPaginas.Assign(Source: TListaPaginas);
var
   i: Integer;
begin
     Clear;
     with Source do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Pagina[ i ] do
               begin
                    Self.AddPagina( Orden, Columnas, Anterior, Titulo );
               end;
          end;
     end;
end;

procedure TListaPaginas.Merge( Nueva: TListaPaginas);
var
   i, iPtr: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          with Pagina[ i ] do
          begin
               iPtr := Nueva.GetIndexOfAnterior( Orden );
               if ( iPtr < 0 ) then
                  FOrden := K_NULL_PAGINA { Marca como Borrada: Importante que la asignaci�n sea a FOrden }
               else
               begin
                    Assign( Nueva.Pagina[ iPtr ] ); { Copia la nueva p�gina }
                    Nueva.Delete( iPtr ); { Borra la p�gina copiada de la lista nueva }
               end;
          end;
     end;
     with Nueva do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with Pagina[ i ] do
               begin
                    Self.AddPagina( Orden, Columnas, Anterior, Titulo ); { Agrega las p�ginas nuevas }
               end;
          end;
     end;
end;

procedure TListaPaginas.Borrar(const iOrden: Integer);
var
   i: Integer;
begin
     i := GetIndexOf( iOrden );
     if ( i >= 0 ) then
     begin
          Delete( i );
     end;
end;

procedure TListaPaginas.Cargar(Lista: TStrings);
var
   i: Integer;
begin
     Ordena;
     with Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for i := 0 to ( Self.Count - 1 ) do
             begin
                  Lista.AddObject( Pagina[ i ].Titulo, Pagina[ i ] )
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TListaPaginas.Ordena;
begin
     FItems.Sort( ComparaPaginas );
end;

end.
