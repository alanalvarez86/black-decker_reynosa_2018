unit ZFuncsWorkFlow;

interface

uses Classes, Sysutils, DB, DBClient,
     ZetaQRExpr,
     ZEvaluador,
     DZetaServerProvider,
     ZetaCommonClasses;

procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );

implementation

uses
    //ZetaTipoEntidad,
    ZFuncsGlobal;

type
  TZetaUsuario = class( TZetaFunc )
  private
    FDataSet : TClientDataSet;
  public
    constructor Create(oEvaluador:TQrEvaluator); override;
    destructor Destroy; override;
    procedure GetRequeridos( var TipoRegresa : TQREvResultType ); override;
    function Calculate: TQREvResult; override;
  end;

constructor TZetaUsuario.Create(oEvaluador:TQrEvaluator);
begin
     inherited Create(oEvaluador);
     FDataSet := TClientDataSet.Create( oZetaProvider );
end;

destructor TZetaUsuario.Destroy;
begin
     FDataSet.Free;
     inherited Destroy;
end;

procedure TZetaUsuario.GetRequeridos( var TipoRegresa : TQREvResultType );
begin
     TipoRegresa := resString;
     oZetaProvider.GetDatosActivos;
end;

function TZetaUsuario.Calculate: TQREvResult;
const
     sSQL = 'select US_CODIGO, US_NOMBRE from USUARIO where US_CODIGO = %s';
var
   iUsuario: Integer;
begin
     with oZetaProvider, Result do
     begin
          iUsuario:= DefaultInteger( 0, UsuarioActivo );
          Kind := resString;
          if ( iUsuario = UsuarioActivo ) then
             strResult := NombreUsuario
          else
          begin
               with FDataSet do
               begin
                    if ( not Active ) or ( FieldByName( 'US_CODIGO' ).AsInteger <> iUsuario ) then
                       Data := OpenSQL( Comparte, Format( sSQL, [ IntToStr( iUsuario ) ] ), TRUE );
                    strResult:= FieldByName( 'US_NOMBRE' ).AsString;
               end;
          end;
     end;
end;


procedure RegistraFunciones( FunctionLibrary: TQRFunctionLibrary );
begin
     with FunctionLibrary do
     begin
          RegisterFunction( ZFuncsGlobal.TGetGlobal, 'GLOBAL' );
          RegisterFunction( ZFuncsGlobal.TZetaActivo, 'ACTIVO' );
          RegisterFunction( TZetaUSUARIO, 'USUARIO' );
     end;
end;

end.
