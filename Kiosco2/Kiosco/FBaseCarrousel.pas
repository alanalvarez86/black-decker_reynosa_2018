unit FBaseCarrousel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, SHDocVw, ExtCtrls, StdCtrls, fcImager, fcpanel, Db, DbClient, Mask,
  ActiveX, DSIntf, DBCommon, StdVcl, {SFEStandardLib_TLB,} SZCodeBaseX, ZetaCommonLists, SFE;

type
  TCarrouselBrowser = class;

  TShowInfo = class(TObject)
  public
    { Public declarations }
    URL : String;
    Browser : TCarrouselBrowser;
    Timeout : Integer;
    Refrescar: Boolean;
  end;

  TBaseCarrousel = class( TForm )
    Timer: TTimer;
    TimerTeclado: TTimer;
    TimerSensorBiometrico: TTimer;
    TimerSync: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure TimerTecladoTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerSensorBiometricoTimer(Sender: TObject);
    procedure TimerSyncTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FURLContenido : String;
    FTimeout : Integer;
    FListaBrowsers : TList;
    FPosBrowser : Integer;
    FGafete: string;
    FCodigoSalida: Boolean;
    FDrivers: boolean;
    procedure ManejaMensaje(var Msg: TMsg; var Handled: Boolean);

    procedure MuestraBrowser;
    procedure MuestraBrowserSiguiente;
    procedure ResetTimer( const nTimeout : Integer );
    procedure ResetTimerTeclado;

    //BIOMETRICO
    procedure InicializaBiometrico;
    procedure InicializaControles(lBiometrico: Boolean);
    procedure enableButtons(bEnable : Boolean);
    procedure switchWorking(bWorking : Boolean);
    procedure SincronizaHuellas;
    procedure InicializaSensorBiometrico;
    function LeeArchivo:Boolean;
    function CaptureFinger : Integer;
    procedure RechazaChecada( const sLetrero : String );
    //procedure PrintError(error : Integer);
    procedure LogPantalla(sMensaje : String);
        

  public
    { Public declarations }
    property  Timeout : Integer read FTimeout write FTimeout;
    procedure ShowContenido;
    //procedure ShowPrimerBoton;
    procedure SetURLContenido( sURL : String );
    function PreparaCarrusel( const sCarrusel: String ): Boolean;
    procedure ShowCarrusel;
    procedure DetieneLector;
  end;



 TCarrouselBrowser = class( TWebBrowser )
    procedure AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
    procedure TerminoCargaDocumento(Sender: TObject; const pDisp: IDispatch; const URL: OleVariant);
  private
    { Private declarations }
    FWindowsProc: TWndMethod;
    FBase: TBaseCarrousel;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure Navegar( const lRefrescar: Boolean );
  end;

var
  BaseCarrousel: TBaseCarrousel;

  gTemplate         : array[0..1403] of Integer; //351*4 = 1404
  gImageBytes       : array[0..65535] of Byte; // 256 * 256
  addrOfTemplate    : PByte; //Direccion de memoria del template
  gHintStopPos      : Integer;
  addrOfImageBytes  : PByte; //Direccion de memoria de la imagen a mostrar
  gWorking          : Boolean; //Bandera de lector en procesos
  iDedo             : Integer;

const
  FINGER_BITMAP_WIDTH   : Integer = 256; //Ancho de la imagen de la huella
  FINGER_BITMAP_HEIGHT  : Integer = 256; //Alto de la imagen de la huella
  ColorNColor           : Integer = 3;

implementation

{$R *.DFM}

uses DCliente,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     FPideNip,
     FNipHtml;

{********** TBaseCarrousel **********}
procedure TBaseCarrousel.FormCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     Application.OnMessage := ManejaMensaje;
     FGafete := '';
     FDrivers := LoadDLL;
     with dmCliente do
     begin
          FTimeout := TimeoutDatos;
          PrimeraVez := True; //BIOMETRICO
     end;
end;

procedure TBaseCarrousel.FormShow(Sender: TObject);
begin
     with dmCliente do
     begin
          if ( EsBiometrico ) then
          begin
               InicializaControles( EsBiometrico );
               InicializaBiometrico;
          end;

     end;
end;

procedure TBaseCarrousel.ShowContenido;
begin
    ResetTimer( self.Timeout );
    ShowModal;
end;

procedure TBaseCarrousel.SetURLContenido(sURL: String);
begin
    FURLContenido := sURL;
end;

procedure TBaseCarrousel.ResetTimer( const nTimeout : Integer );
begin
     with Timer do
     begin
          Enabled := FALSE;
          Interval := nTimeout;
          Enabled := TRUE;
     end;
end;

procedure TBaseCarrousel.FormDestroy(Sender: TObject);
var
    i: Integer;
begin
     inherited;
     if ( FListaBrowsers <> NIL ) then
     begin
          with FListaBrowsers do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    TShowInfo( Items[ i ] ).Free;
               end;
               Free;
          end;
     end;
end;

{ TFormaCarrusel }
procedure TBaseCarrousel.MuestraBrowser;
var
    oShowInfo : TShowInfo;
begin
    oShowInfo := FListaBrowsers.Items[ FPosBrowser ];

    ResetTimer( oShowInfo.Timeout );
    oShowInfo.Browser.Navegar( self.Visible and oShowInfo.Refrescar ) ;

end;

procedure TBaseCarrousel.MuestraBrowserSiguiente;
var
    oShowInfoAnterior : TShowInfo;
begin
     if ( FListaBrowsers.Count <> 1 ) then
     begin
          oShowInfoAnterior := FListaBrowsers.Items[ FPosBrowser ];

          FPosBrowser := FPosBrowser + 1;
          // Si ya lleg� a la �ltima pantalla del carrusel, da la vuela a la primera
          if ( FPosBrowser >= FListaBrowsers.Count ) then
                  FPosBrowser := 0;
          MuestraBrowser;

          // Primero muestro el nuevo y luego escondo el actual
          // para que se vea mejor la transici�n entre browsers,
          // evitando que se vea el fondo gris de la forma
          oShowInfoAnterior.Browser.Hide;
     end
     else
     begin
          MuestraBrowser;
     end;
end;

function TBaseCarrousel.PreparaCarrusel( const sCarrusel: String ): Boolean;
var
    oShowInfo : TShowInfo;
    oBrowser  : TCarrouselBrowser;
begin
    dmCliente.GetCarrusel( sCarrusel );
    FListaBrowsers := TList.Create;

    with dmCliente.cdsCarrusel do
    begin
        Result := not Eof;
        while not Eof do
        begin
            oBrowser := TCarrouselBrowser.Create( self );
            TWinControl( oBrowser ).Parent := self;
            oShowInfo := TShowInfo.Create;

            with oShowInfo do
            begin
                URL     := dmCliente.GetURLKiosco( FieldByName( 'KI_URL' ).AsString );
                Timeout := FieldByName( 'KI_TIMEOUT' ).AsInteger * K_MIL;
                Refrescar := ZStrToBool( FieldByName( 'KI_REFRESH').AsString );
                Browser := oBrowser;
            end;

            with oBrowser do
            begin
                 Align := alClient;
                 Hide;
                 Navigate( oShowInfo.URL );
            end;
            FListaBrowsers.Add( oShowInfo );
            Next;
        end;
    end;
end;

procedure TBaseCarrousel.ShowCarrusel;
begin
     FPosBrowser := 0;
     MuestraBrowser;
     ShowModal;
end;

procedure TBaseCarrousel.TimerTimer(Sender: TObject);
begin
     MuestraBrowserSiguiente;
end;

procedure TBaseCarrousel.ManejaMensaje(var Msg: TMsg; var Handled: Boolean);
 const
      VK_JOTA = 'm';
      VK_SALIDA = Chr( VK_CONTROL ) + VK_JOTA;
 var
    cLetra: Char;
    iLetra : integer;
begin
     if ( Screen.ActiveForm <> NIL ) then
     begin
          if ( Screen.ActiveForm.Name = Self.Name ) then
          begin
               Handled := True;
               case Msg.Message of
                    WM_KEYDOWN:
                    begin
                         if ( Msg.wParam = VK_RETURN ) or ( FCodigoSalida and (Msg.wParam = 77) ) then
                         begin
                              try
                                 with dmCliente do
                                 begin
                                      if ( EsBiometrico and ( FGafete = GafeteAdmin ) ) or
                                         ( not EsBiometrico ) then
                                           MuestraPideGafete( fGafete )
                                      else
                                           
                                 end;

                              finally
                                     FGafete := VACIO;
                                     FCodigoSalida := FALSE;
                              end;
                         end
                         else if ( Msg.wParam = VK_CONTROL ) then
                         begin
                              FCodigoSalida := TRUE;
                         end
                         else
                         begin
                              iLetra := Msg.wParam;

                              if ( iLetra in [96..105] ) then
                                 iLetra := iLetra - 48;

                              if ( iLetra in [65..90] ) or
                                 ( iLetra in [48..57] ) then
                              begin
                                   cLetra := chr( iLetra );
                                   ResetTimerTeclado;
                                   FGafete := FGafete + cLetra;
                              end;
                         end;
                    end;
                    {$IFDEF PIDEGAFETE}
                    WM_LBUTTONDOWN:
                    begin
                         with dmCliente do
                         begin
                              MuestraPideGafete;
                         end;
                    end;
                    {$ENDIF}
               else
                   Handled := False;
               end;
          end
          else
          begin
               { Cambio para que en el click y dobleclick del carrusel no se esconda la forma de 'pedir nip'}
               if ( Screen.ActiveForm.Name = 'NipHtml' ) then
               begin
                    Handled := False;
                    if ( NipHtml.WebBrowser = nil ) or ( ( Msg.message <> WM_LBUTTONDOWN ) and  ( Msg.message <> WM_LBUTTONDBLCLK ) ) then
                       Exit;

                    Handled := IsDialogMessage( NipHtml.WebBrowser.Handle, Msg );
                    if (Handled) then
                    begin
                         case (Msg.message) of
                              WM_LBUTTONDOWN,WM_LBUTTONDBLCLK:
                                PideNipBringToFront;
                         end;
                    end;
               end;
               
          end;
     end;
end;

{ ***** TCarrouselBrowser ***** }

constructor TCarrouselBrowser.Create(AOwner: TComponent);
begin
     FBase := TBaseCarrousel( AOwner );
     inherited Create( AOwner );
     Self.OnBeforeNavigate2 := AntesDeNavegar;
     Self.OnDocumentComplete := TerminoCargaDocumento;
     AddressBar := False;
     MenuBar := False;
     Silent := True;
     StatusBar := False;
     Offline := False;
     FWindowsProc := Self.WindowProc;
end;

procedure TCarrouselBrowser.TerminoCargaDocumento(Sender: TObject; const pDisp: IDispatch; const URL: OleVariant);
begin
     if( Assigned( Self.Document ) )then
     begin
          Self.OleObject.Document.Body.Style.OverflowX := 'hidden';
          Self.OleObject.Document.Body.Style.OverflowY := 'hidden';
     end;
end;

procedure TCarrouselBrowser.AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
begin
     if( ReadyState = READYSTATE_COMPLETE )then
     begin
          Cancel := True;
     end;
end;

procedure TCarrouselBrowser.Navegar( const lRefrescar: Boolean );

var
   Level : Olevariant;
begin
     if lRefrescar then
     begin
          Level := REFRESH_COMPLETELY;
          Refresh2( Level );
     end;
     Show;
end;

procedure TBaseCarrousel.TimerTecladoTimer(Sender: TObject);
begin
     FGafete := VACIO;
     TimerTeclado.Enabled := FALSE;
end;

procedure TBaseCarrousel.ResetTimerTeclado;
begin
     TimerTeclado.Enabled := FALSE;
     TimerTeclado.Enabled := TRUE;
end;

//--------------------------------------------------------------------------------------BIOMETRICO

procedure TBaseCarrousel.TimerSensorBiometricoTimer(Sender: TObject);
var
   lPv : Boolean;
begin
     TimerSensorBiometrico.Enabled := False;

     //IniciaLector solo si es biometrico y si no hay alguna ventana abierta de configuracion
     with dmCliente do
     begin
          if ( EsBiometrico ) then
          begin
               lPv := PrimeraVez;
               PrimeraVez := False;

               DetieneLector;

               if ( LeeArchivo ) then
               begin
                    if ( lPv ) then
                         self.SincronizaHuellas;

                    InicializaSensorBiometrico;
               end
               else
                    dmCliente.MuestraErrorcito('Es necesario tener conectado el sensor biometrico. ' + CR_LF + ' Reinicie el Kiosco.');
          end;
     end;
end;

procedure TBaseCarrousel.InicializaBiometrico;
begin
     enableButtons(True);
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
     dmCliente.SetSiguienteHora;
     TimerSync.Enabled := True;
end;

procedure TBaseCarrousel.InicializaControles(lBiometrico: Boolean);
begin
     TimerSensorBiometrico.Enabled := lBiometrico;
end;

procedure TBaseCarrousel.DetieneLector;
begin
     if ( FDrivers ) then
        SFE.Close();

     enableButtons(False);
     switchWorking(False);

     TimerSensorBiometrico.Enabled := False;
     
     //Fix de perdida de focus.
     if not Assigned(Application.OnMessage) then
          Application.OnMessage := ManejaMensaje;
end;

function TBaseCarrousel.LeeArchivo: Boolean;
var
   iArchivoId, nRet, iRetry : Integer;
const
     K_RETRIES : Integer = 3;
begin
     Result := False;
     try
        if not FileExists( dmCliente.FileTemplates ) then //Si no existe lo crea vac�o.
        begin
             iArchivoId := FileCreate( dmCliente.FileTemplates );
             FileClose( iArchivoId );
        end;

        iRetry := 0;
        while ( iRetry < K_RETRIES ) do
        begin
             {$ifdef ANTES}
             nRet := SFE.Open( dmCliente.FileTemplates, 4, 0 );
             {$else}
             if ( FDrivers ) then
                nRet := SFE.Open( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( dmCliente.FileTemplates ), K_TIPO_BIOMETRICO, 0 );
             {$endif}
             if nRet = 0 then
             begin
                  enableButtons(True);
                  if ( FDrivers ) then
                     LogPantalla('Huellas: ' + IntToStr(SFE.GetEnrollCount()));
                  Result := True;
                  iRetry := K_RETRIES;
             end
             else
             begin
                  dmCliente.MuestraErrorcito('No hay dispositivo biometrico');
                  switchWorking(False);
             end;
             
             inc( iRetry );
        end;
     except
           on Error: Exception do
           begin
                switchWorking(False);
                Result := False;
                dmCliente.MuestraFormaError( Error );
           end;
     end;
end;

procedure TBaseCarrousel.enableButtons(bEnable: Boolean);
begin
     switchWorking(Not bEnable);
end;

procedure TBaseCarrousel.switchWorking(bWorking: Boolean);
begin
     gWorking := bWorking;
end;

procedure TBaseCarrousel.SincronizaHuellas;
var
   sArchivo, sArchivoTemplate : String;
   nRet, iId, iDedo, iHuellas : Integer;
   ss           :TStringStream;
   ms           :TMemoryStream;
   sLista : String;
   oCursor: TCursor;

const
     K_EXT_TEMPLATE = '.tem';

     Procedure AnexaHuella( iHuella : Integer );
     begin
          if ( StrLleno( sLista ) ) then
               sLista := sLista + ',' + intToStr(iHuella)
          else
               sLista := intToStr(iHuella);
     end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     //editNumero.Enabled := False;
     try

        LogPantalla( 'Sincronizando espere un momento' );
        sLista := VACIO;

        //Proceso de Sincronizacion de Huellas
        sArchivo := dmCliente.FileTemplates;

        try
           dmCliente.cdsHuellas.Refrescar;

           ms := TMemoryStream.Create;

           if ( not LeeArchivo ) then
              Exit;

           with dmCliente.cdsHuellas do
           begin
                iHuellas := Recordcount;
                LogPantalla( 'Sincronizando ' + intToStr(iHuellas) + ' huellas, espere un momento' );

                First;
                while not EOF do
                begin
                     try
                        iId := FieldByName( 'ID_NUMERO' ).AsInteger;
                        iDedo := FieldByName( 'HU_INDICE' ).AsInteger;
                        ss := TStringStream.Create( FieldByName( 'HU_HUELLA' ).AsString );

                        ms.Position := 0;
                        SZDecodeBase64( ss, ms );

                        sArchivoTemplate := ExtractFilePath( Application.ExeName ) + IntToStr( iId ) + '_' + IntToStr( iDedo ) +  K_EXT_TEMPLATE;
                        ms.SaveToFile( sArchivoTemplate );
                        if ( FDrivers ) then
                           nRet := SFE.LoadTemplateFromFile( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( sArchivoTemplate ), addrOfTemplate );

                        if nRet < 0 then
                        begin
                             raise Exception.Create( dmCliente.GetBioMessage(nRet) );
                        end
                        else
                        begin
                             if ( FDrivers ) then
                                nRet := SFE.TemplateEnroll( iId , iDedo, 0, addrOfTemplate );

                             if ( ( nRet < 0 ) and ( nRet <> -102 ) ) then
                             AnexaHuella( FieldByName( 'HU_ID' ).AsInteger );
                        end;

                     except
                        on Error: Exception do
                        begin
                             dmCliente.MuestraFormaError( Error );
                        end;
                     end;
                     DeleteFile( sArchivoTemplate );
                     Next;
                end;
           end;

           ms.Clear;
           Application.ProcessMessages();

           with dmCliente do
           begin
                PrimeraVez := False;
                Sync := False;
                ReportaHuella( sLista );
           end;
           Application.ProcessMessages();
        except
              on Error: Exception do
              begin
                   switchWorking(False);
                   dmCliente.MuestraErrorcito( 'Lector inactivo. Error: ' + Error.Message );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBaseCarrousel.InicializaSensorBiometrico;
var
   nRet, userId, fingerNum: Integer;

   function EsFormaActiva( oForma: TCustomForm ): Boolean;
   begin
        Result := ( oForma <> nil ) and ( oForma.Visible ) and ( oForma.Active );
   end;

   procedure ObtieneLectura;
   label
        stopWorking;
   begin
        if ( LeeArchivo ) then
        begin
             try
                switchWorking(True);
                while gWorking do
                begin
                     Application.ProcessMessages();
                     userId := 0;

                     nRet := CaptureFinger; //Devuelve algun emnsaje de lectura o error.

                     if dmCliente.Sync then
                     begin
                          DetieneLector;
                          TimerSensorBiometrico.Enabled := True;
                          dmCliente.PrimeraVez := True;
                     end;

                     if nRet <> 1 then
                        goto stopWorking;
                     if ( FDrivers ) then
                        nRet := SFE.Identify(userId, fingerNum);

                     if nRet < 0 then
                          RechazaChecada( 'Huella no identificada.' )
                     else
                          goto stopWorking;
                end;
stopWorking:
                switchWorking(False);
             except
                   on Error: Exception do
                   begin
                        switchWorking(False);
                        dmCliente.MuestraFormaError( Error );
                   end;
             end;
         end;
     end;

begin
     switchWorking(True);
     while gWorking do
     begin
          ObtieneLectura;

          if ( userId > 0 ) then
          begin
               TimerSensorBiometrico.Enabled := False; //Si no es biometrico, se apaga el timer
               dmCliente.MuestraPideGafete( IntToStr(userId) ); //Lo levanta como Thread
          end;
     end;
     TimerSensorBiometrico.Enabled := True; //Si no es biometrico, se apaga el timer
end;

function TBaseCarrousel.CaptureFinger: Integer;
var
  nRet, nCapArea, nMaxCapArea, nResult : Integer;
begin
     nResult := 1;
     nMaxCapArea := 0;

     while True do
     begin
          try
             Application.ProcessMessages();
             if Not gWorking then
             begin
                  nResult := 0;
                  break;
             end;
             if ( FDrivers ) then
                nRet := SFE.Capture();

             if dmCliente.Sync then
             begin
                  nResult := 0;
                  break;
             end;

             if nRet < 0 then
             begin
                  nResult := 0;
                  break;
             end;
             Application.ProcessMessages();
             if ( FDrivers ) then
                nCapArea := SFE.IsFinger();

             if nCapArea >= 0 then
             begin
                  if nCapArea < nMaxCapArea + 2 then break;
                  if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                  if nCapArea > 45 then break;
             end;
             Application.ProcessMessages();
          except
                Result := 0;
                Exit;
          end;
     end;
     Application.ProcessMessages();

     if nResult = 1 then
     begin
          if ( FDrivers ) then
             nRet := SFE.GetImage(addrOfImageBytes);

          if nRet < 0 then
          begin
               nResult := 0;
          end
     end;
     Application.ProcessMessages();
     CaptureFinger := nResult;
end;

procedure TBaseCarrousel.RechazaChecada(const sLetrero: String);
begin
     dmCliente.MuestraErrorcito( sLetrero );
end;

{procedure TBaseCarrousel.PrintError(error: Integer);
begin
     dmCliente.MuestraErrorcito( dmCliente.GetBioMessage(error) );
end;}

procedure TBaseCarrousel.LogPantalla(sMensaje: String);
begin
     //ZetaDialogo.ZInformation( Caption, sMensaje, 0 );
end;

procedure TBaseCarrousel.TimerSyncTimer(Sender: TObject);
begin
     dmCliente.ValidacionSincronizacion;
end;

procedure TBaseCarrousel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (dmCliente.EsBiometrico) then
          DetieneLector;
end;

end.

