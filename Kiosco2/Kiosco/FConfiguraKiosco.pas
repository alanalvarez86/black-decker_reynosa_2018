unit FConfiguraKiosco;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Mask, ExtCtrls,
     fcCombo, fcColorCombo, fcImage, fcImageForm, fcPanel,
     fcButton, fcImgBtn, fcShapeBtn,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaKeyCombo,
     FKioscoRegistry, CheckLst, Buttons;

type
  TConfiguraKiosco = class(TForm)
    fcImageForm: TfcImageForm;
    KioscoLBL: TLabel;
    btnEscribir: TfcShapeBtn;
    btnCancelar: TfcShapeBtn;
    cbKiosco: TZetaKeyCombo;
    ErrorText: TLabel;
    gbAdministrador: TfcGroupBox;
    ClaveLBL: TLabel;
    edClave: TEdit;
    edConfirmacion: TEdit;
    ConfirmacionLBL: TLabel;
    GafeteAdminLBL: TLabel;
    edGafeteAdmin: TEdit;
    fcAutoLogon: TfcGroupBox;
    DomainDefaultLBL: TLabel;
    DomainDefault: TEdit;
    UsuarioDefault: TEdit;
    UsuarioDefaultLBL: TLabel;
    PasswordDefaultLBL: TLabel;
    PasswordDefault: TEdit;
    PasswordConfirm: TEdit;
    PasswordConfirmLBL: TLabel;
    LogonAutoAdminLBL: TLabel;
    LogonAutoAdmin: TCheckBox;
    fcEmpresasautorizadas: TfcGroupBox;
    lbEmpresaAutorizadas: TCheckListBox;
    fcMensajeEmail: TfcGroupBox;
    edMensajeEmail: TMemo;
    fcDefaultsGafete: TfcGroupBox;
    DefaultLetraCredencialLBL: TLabel;
    DefaultDigitoEmpresaLBL: TLabel;
    DefaultLetraCredencial: TEdit;
    DefaultDigitoEmpresa: TEdit;
    Label1: TLabel;
    cbTipoGafete: TZetaKeyCombo;
    IntentosLBL: TLabel;
    edIntentos: TZetaNumero;
    txtRutaHuellas: TEdit;
    lblRutaHuellas: TLabel;
    btnHuellasFind: TSpeedButton;
    dlgHuellas: TOpenDialog;
    btnReiniciaHuellas: TSpeedButton;
    fcGroupBox1: TfcGroupBox;
    lblLimiteImpresionReportes: TLabel;
    cbLimitarImpresionReportes: TCheckBox;
    LblBotonImprimirReportes: TLabel;
    cbBotonImprimirReportes: TCheckBox;
    lblEncuestas: TLabel;
    cbEncuestas: TCheckBox;
    lblValidaImprimir: TLabel;
    cbNominaHuella: TCheckBox;
    lblImprimeHuella: TLabel;
    cbImprimeHuella: TCheckBox;
    lblIDKiosco: TLabel;
    edtIdKiosco: TEdit;
    txtHorasSincronizacion: TEdit;
    lblSincroniza: TLabel;
    lblHoras: TLabel;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnEscribirClick(Sender: TObject);
    procedure CambioEnDatos(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure LogonAutoAdminClick(Sender: TObject);
    procedure btnHuellasFindClick(Sender: TObject); //BIOMETRICO
    procedure cbTipoGafeteChange(Sender: TObject); //BIOMETRICO
    procedure btnReiniciaHuellasClick(Sender: TObject); //BIOMETRICO
    procedure edClaveKeyPress(Sender: TObject; var Key: Char);
    procedure edClaveExit(Sender: TObject);
    procedure edConfirmacionExit(Sender: TObject); //BIOMETRICO
  private
    function GetListaEmpresasAutorizadas: string;
    procedure SetListaEmpresasAutorizadas(const Value: string);

  private
    { Private declarations }
    FRegistry: TLogonRegistry;
    procedure CargaListaKioscos;
    procedure CargaListaEmpresas(var Lista: TCheckListBox);
    procedure LeeValoresRegistry;
    procedure EscribeRegistry;
    procedure EscribeError(const sError: String);
    procedure ResetError;
    procedure SetControls;
    property ListaEmpresasAutorizadas : string read GetListaEmpresasAutorizadas write SetListaEmpresasAutorizadas;
    procedure ToggleBiometrico( const bBioSeleccionado : Boolean ); //BIOMETRICO
  public
    { Public declarations }
    function Init: Boolean;
  end;

function ConfiguraKiosco: Boolean;

implementation

uses DCliente,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo;

const
     K_SINC_HUELLAS = 'Sincronizaci�n de Huellas'; //BIOMETRICO

function ConfiguraKiosco: Boolean;
var
   oConfiguraKiosco: TConfiguraKiosco;
begin
     Result := False;
     oConfiguraKiosco := TConfiguraKiosco.Create( Application );
     try
        with oConfiguraKiosco do
        begin
             if Init then
             begin
                  ShowModal;
                  Result := ( ModalResult = mrOk );
             end;
        end;
     finally
            FreeAndNil( oConfiguraKiosco );
     end;
end;

{$R *.DFM}

{ ***** TConfiguraKiosco ***** }

procedure TConfiguraKiosco.FormCreate(Sender: TObject);
begin
     FRegistry := TLogonRegistry.Create;
end;

procedure TConfiguraKiosco.FormShow(Sender: TObject);
begin
     SetControls;
     {$ifdef MISDATOS}
     KioscoLBL.Visible := FALSE;
     cbKiosco.Visible := FALSE;
     fcMensajeEmail.Visible := FALSE;
     lblEncuestas.Visible := FALSE;
     cbEncuestas.Visible := FALSE;
{
     lblLimiteImpresionReportes.Visible := FALSE;
     cbLimitarImpresionReportes.Visible := FALSE;
     LblBotonImprimirReportes.Visible := FALSE;
     cbBotonImprimirReportes.Visible := FALSE;
}
     gbAdministrador.Caption := ' Administrador Local de MisDatos ';
     {lblLimiteImpresionReportes.Top := ( lblLimiteImpresionReportes.Top - fcMensajeEmail.Height );
     cbLimitarImpresionReportes.Top := ( cbLimitarImpresionReportes.Top - fcMensajeEmail.Height );
     LblBotonImprimirReportes.Top := ( LblBotonImprimirReportes.Top - fcMensajeEmail.Height );
     cbBotonImprimirReportes.Top := ( cbBotonImprimirReportes.Top - fcMensajeEmail.Height ); }
     fcEmpresasautorizadas.Top := ( fcEmpresasautorizadas.Top - fcMensajeEmail.Height );
     fcEmpresasautorizadas.Height := ( fcEmpresasautorizadas.Height + fcMensajeEmail.Height );
     //fcEmpresasautorizadas.Top := ( fcEmpresasautorizadas.Top - lblEncuestas.Height );
     //fcEmpresasautorizadas.Height := ( fcEmpresasautorizadas.Height + lblEncuestas.Height );
     lblIDKiosco.Caption := 'ID Mis Datos:';
     {$endif}
end;

procedure TConfiguraKiosco.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
end;

function TConfiguraKiosco.Init: Boolean;
var
   oCursor: TCursor;
begin
//     Result := True;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           CargaListaKioscos;
           CargaListaEmpresas(lbEmpresaAutorizadas);
           LeeValoresRegistry;
           Result := True;
        except
              Result := FALSE;
              raise;
{
              on Error: Exception do
              begin
                   Application.HandleException( Error );
              end;
}
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfiguraKiosco.CargaListaKioscos;
begin
     with dmCliente.cdsKioscos do
     begin
          Conectar;
          while not EOF do
          begin
               cbKiosco.Lista.Add( FieldByName('KW_CODIGO').AsString +'='+ FieldByName('KW_NOMBRE').AsString );
               Next;
          end;
     end;
     {$ifdef MISDATOS}
     with cbKiosco do
     begin
          ItemIndex := Lista.IndexOfName( 'MDATOS' ); //K_MIS_DATOS );
          if ( ItemIndex < 0 ) then
             raise Exception.Create( 'No se puede configurar MisDatos' + CR_LF + CR_LF + 'Revise la configuraci�n global' );
     end;
     {$endif}
end;

procedure TConfiguraKiosco.CargaListaEmpresas(var Lista: TCheckListBox);
 var
    sDigito:Char;
    DigitoActual:string;
    AddConfidencial:Boolean;
    AddSinConfidencial:Boolean;

    function ExisteDigitoEmpresa (sDigito: string ; const EsSinConfidencial:Boolean):Boolean;
    var
       i:Integer;
    begin
         with Lista do
         begin
              Result := False;
              i := 0;
              While( ( i < Items.Count ) )  do
              begin
                   if ( Chr( Integer( Items.Objects[i] ) ) = sDigito ) then
                   begin
                        //Quitar la base de datos del mismo Digito y agregar
                        If EsSinConfidencial and not AddSinConfidencial then
                        begin
                             with Items do
                             begin
                                  BeginUpdate;
                                  try
                                     Delete(i);
                                  finally
                                         EndUpdate;
                                  end;
                             end;
                             Result := False;
                        end
                        else
                            Result := ( ( not EsSinConfidencial and AddConfidencial ) or AddSinConfidencial);
                   end;
                   i:= i+1;
              end;
         end;
    end;
begin
     with Lista.Items do
     begin
          BeginUpdate;
          try
             Clear;
             DigitoActual:= VACIO;
             with dmCliente.cdsCompany do
             begin
                  AddConfidencial:= False;
                  AddSinConfidencial := False;
                  Data := dmCliente.Servidor.GetCompanys( 0, Ord( tc3Datos ) );
                  while not Eof do
                  begin
                       if StrLleno( FieldByName( 'CM_DIGITO' ).AsString ) and ( not ExisteDigitoEmpresa(FieldByName( 'CM_DIGITO' ).AsString,StrVacio( FieldByName( 'CM_NIVEL0' ).AsString ) ) ) then
                       begin
                            if DigitoActual <> FieldByName( 'CM_DIGITO' ).AsString then
                            begin
                                 DigitoActual := FieldByName( 'CM_DIGITO' ).AsString;
                                 AddSinConfidencial := False;
                                 AddConfidencial := False;
                            end;
                            if not AddSinConfidencial then
                            begin
                                 sDigito := FieldByName( 'CM_DIGITO' ).AsString[1];
                                 AddObject( Format( '(%s) %s : %s', [ FieldByName( 'CM_DIGITO' ).AsString,FieldByName( 'CM_CODIGO' ).AsString, FieldByName( 'CM_NOMBRE' ).AsString ] ), TObject( ORD( sDigito ) ) );
                                 if StrVacio( FieldByName( 'CM_NIVEL0' ).AsString )then
                                    AddSinConfidencial := True
                                 else
                                     AddConfidencial := True;
                            end;
                       end;
                       Next;
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;     
end;

procedure TConfiguraKiosco.LeeValoresRegistry;
begin
     with KioskoRegistry do
     begin
          edtIdKiosco.Text := IdKiosco; //BIOMETRICO
          cbKiosco.Llave := CodigoKiosco;
          edGafeteAdmin.Text := GafeteAdministrador;
          edClave.Text := PasswordSalida;
          edIntentos.Valor := Intentos;
          DefaultLetraCredencial.Text := LetraGafete;
          DefaultDigitoEmpresa.Text := DigitoEmpresa;
          ListaEmpresasAutorizadas := EmpresasAutorizadas;
          edMensajeEmail.Text :=  MensajeEmail;
          cbLimitarImpresionReportes.Checked := LimitarImpresion;
          cbBotonImprimirReportes.Checked := SoloBotonImprimir;
          cbEncuestas.Checked := MostrarEncuestas;
          cbTipoGafete.Valor := TipoGafete;
          txtRutaHuellas.Text := RutaHuellas; //BIOMETRICO
          txtHorasSincronizacion.Text := HorasSincronizacion; //BIOMETRICO
          cbNominaHuella.Checked := ValidaImprimir; //BIOMETRICO
          cbImprimeHuella.Checked := ImprimeHuella; //BIOMETRICO
          ToggleBiometrico( ( cbTipoGafete.Valor = 2 ) ); //BIOMETRICO
     end;
     with FRegistry do
     begin
          DomainDefault.Text := DefaultDomainName;
          UsuarioDefault.Text := DefaultUserName;
          PasswordDefault.Text := DefaultPassword;
          PasswordConfirm.Text := DefaultPassword;
          LogonAutoAdmin.Checked := AutoAdminLogon;
     end;
end;

procedure TConfiguraKiosco.EscribeRegistry;
begin
     with KioskoRegistry do
     begin
          IdKiosco := edtIdKiosco.Text; //BIOMETRICO
          CodigoKiosco := cbKiosco.Llave;
          GafeteAdministrador := edGafeteAdmin.Text;
          PasswordSalida := edClave.Text;
          Intentos := edIntentos.ValorEntero;
          LetraGafete := DefaultLetraCredencial.Text;
          DigitoEmpresa := DefaultDigitoEmpresa.Text;
          EmpresasAutorizadas := ListaEmpresasAutorizadas;
          MensajeEmail:= edMensajeEmail.Text;
          LimitarImpresion := cbLimitarImpresionReportes.Checked;            //JB Se agrega la Limitante de Impresion de Reportes
          SoloBotonImprimir := cbBotonImprimirReportes.Checked;
          MostrarEncuestas := cbEncuestas.Checked;  
          TipoGafete := cbTipoGafete.Valor;
          RutaHuellas := txtRutaHuellas.Text; //BIOMETRICO
          HorasSincronizacion := txtHorasSincronizacion.Text; //BIOMETRICO
          ValidaImprimir := cbNominaHuella.Checked; //BIOMETRICO
          ImprimeHuella := cbImprimeHuella.Checked; //BIOMETRICO
     end;
     if LogonAutoAdmin.Checked then
     begin
          with FRegistry do
          begin
               DefaultDomainName := DomainDefault.Text;
               DefaultUserName := UsuarioDefault.Text;
               DefaultPassword := PasswordDefault.Text;
               AutoAdminLogon := LogonAutoAdmin.Checked;
          end;
     end
     else
     begin
          with FRegistry do
          begin
               DefaultDomainName := VACIO;
               DefaultUserName := VACIO;
               DefaultPassword := VACIO;
               AutoAdminLogon := FALSE;
          end;
     end;

end;

procedure TConfiguraKiosco.ResetError;
begin
     with ErrorText do
     begin
          Visible := False;
     end;
end;

procedure TConfiguraKiosco.EscribeError( const sError: String );
begin
     with ErrorText do
     begin
          Caption := sError;
          Visible := True;
     end;
end;

procedure TConfiguraKiosco.CambioEnDatos(Sender: TObject);
begin
     ResetError;
end;

procedure TConfiguraKiosco.SetControls;
begin
     DomainDefault.Enabled := LogonAutoAdmin.Checked;
     DomainDefaultLBL.Enabled := LogonAutoAdmin.Checked;
     UsuarioDefault.Enabled := LogonAutoAdmin.Checked;
     UsuarioDefaultLBL.Enabled := LogonAutoAdmin.Checked;
     PasswordDefault.Enabled := LogonAutoAdmin.Checked;
     PasswordDefaultLBL.Enabled := LogonAutoAdmin.Checked;
     PasswordConfirm.Enabled := LogonAutoAdmin.Checked;
     PasswordConfirmLBL.Enabled := LogonAutoAdmin.Checked;
end;

procedure TConfiguraKiosco.LogonAutoAdminClick(Sender: TObject);
begin
     SetControls;
     CambioEnDatos( Sender );
end;

procedure TConfiguraKiosco.btnEscribirClick(Sender: TObject);
var
   lOk: Boolean;
begin
     lOk := False;
     ModalResult := mrNone;
     
     if StrVacio( edtIdKiosco.Text ) then
     begin
          EscribeError( '� Identificador de Kiosco inv�lido !' );
          ActiveControl := edtIdKiosco;
     end
     else
     if StrVacio( cbKiosco.Llave ) then
     begin
          EscribeError( '� C�digo de Kiosco inv�lido !' );
          ActiveControl := cbKiosco;
     end
     else
         if StrVacio( edGafeteAdmin.Text ) then
         begin
              EscribeError( '� Gafete del Administrador no fu� capturado !' );
              ActiveControl := edGafeteAdmin;
         end
         else
             if StrVacio( edClave.Text ) then
             begin
                  EscribeError( '� Clave del Administrador no fu� capturada !' );
                  ActiveControl := edClave;
             end
             else
                 if ( edClave.Text <> edConfirmacion.Text ) then
                 begin
                      EscribeError( '� Clave del Administrador no fu� confirmada correctamente !' );
                      ActiveControl := edClave;
                 end
                 else if (ListaEmpresasAutorizadas = VACIO) then
                 begin
                      EscribeError( '� Debe de autorizarse por lo menos una empresa !' );
                      ActiveControl := lbEmpresaAutorizadas;
                 end
                 else if ( cbTipoGafete.Valor = 2 ) then //BIOMETRICO
                 begin
                      //Validaciones de Biometrico.
                      if (txtRutaHuellas.Text = VACIO) then
                      begin
                           EscribeError( '� Debe indicar una ruta para almacenar localmente las huellas digitales !' );
                           ActiveControl := txtRutaHuellas;
                      end
                      else if ( ( dmCliente.GetHoras( txtHorasSincronizacion.Text ) = -1 ) or
                                ( dmCliente.GetMinutos( txtHorasSincronizacion.Text ) = -1) ) then
                      begin
                           EscribeError( '� Horas de sincronizacion incorrectas !' );
                           ActiveControl := txtHorasSincronizacion;
                      end
                      else
                      begin
                           lOk := True;
                      end;
                 end
                 else
                     lOk := True;
     if lOk then
     begin
          EscribeRegistry;
          ModalResult := mrOk;
     end;
end;

procedure TConfiguraKiosco.btnCancelarClick(Sender: TObject);
begin
     ModalResult := mrCancel;
end;


function TConfiguraKiosco.GetListaEmpresasAutorizadas: string;
 var
    i: integer;
    sDigito: string;
begin
     Result := VACIO;
     with lbEmpresaAutorizadas do
          for i:=0 to Items.Count - 1 do
          begin
               if Checked[i] then
               begin
                    sDigito := Chr( Integer( Items.Objects[i] ) );
                    Result := ConcatString( Result,  sDigito, ',' );
               end;
          end;
end;

procedure TConfiguraKiosco.SetListaEmpresasAutorizadas( const Value: string);
 var
    oLista: TStrings;
    i, iPos, iDigito: integer;
begin
     with lbEmpresaAutorizadas do
     begin
          for i:= 0 to Items.Count -1 do
              Checked[i] :=  FALSE;
     end;

     oLista := TStringList.Create;
     try
        oLista.Commatext := Value;
        for i:=0 to oLista.Count -1 do
        begin
             iDigito := Ord( oLista[i][1] );
             iPos := lbEmpresaAutorizadas.Items.IndexOfObject(TObject(iDigito));
             if iPos >= 0 then
                lbEmpresaAutorizadas.Checked[iPos] := TRUE;
        end;
     finally
            FreeAndNil(oLista);
     end;
end;

//BIOMETRICO
procedure TConfiguraKiosco.btnHuellasFindClick(Sender: TObject);
begin
     dlgHuellas.FileName := txtRutaHuellas.Text;
     if ( dlgHuellas.Execute ) then
          txtRutaHuellas.Text := dlgHuellas.FileName;
end;

//BIOMETRICO
procedure TConfiguraKiosco.cbTipoGafeteChange(Sender: TObject);
begin
     ToggleBiometrico( ( cbTipoGafete.Valor = 2 ) );
end;

//BIOMETRICO
procedure TConfiguraKiosco.ToggleBiometrico( const bBioSeleccionado : Boolean );
begin
     txtRutaHuellas.Enabled := bBioSeleccionado;
     btnHuellasFind.Enabled := bBioSeleccionado;
     btnReiniciaHuellas.Enabled := bBioSeleccionado;
     txtHorasSincronizacion.Enabled := bBioSeleccionado;
     lblRutaHuellas.Enabled := bBioSeleccionado;
     lblHoras.Enabled := bBioSeleccionado;
     lblSincroniza.Enabled := bBioSeleccionado;
     lblValidaImprimir.Enabled := bBioSeleccionado;
     lblImprimeHuella.Enabled := bBioSeleccionado;
     cbNominaHuella.Enabled := bBioSeleccionado;
     cbImprimeHuella.Enabled := bBioSeleccionado;
     cbNominaHuella.Enabled := bBioSeleccionado;
     cbImprimeHuella.Enabled := bBioSeleccionado;
end;

//BIOMETRICO
procedure TConfiguraKiosco.btnReiniciaHuellasClick(Sender: TObject);
var
   sMensaje : String;
begin
     sMensaje := '�Est� seguro de que desea reiniciar la sincronizaci�n de huellas?';

     if ( ( ZConfirm(K_SINC_HUELLAS,sMensaje,0,mbNo) ) and FileExists( txtRutaHuellas.Text ) ) then
     begin
          try
             ZetaMessage(K_SINC_HUELLAS,'Se sincronizar�n las huellas al iniciar el Kiosco.',mtInformation,mbOKCancel,0,mbOK);
             DeleteFile( txtRutaHuellas.Text );
             dmCliente.ReiniciaHuellas;
          except

          end;
     end;
end;

procedure TConfiguraKiosco.edClaveKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key <> Chr( VK_BACK ) ) then
     begin
          if not ( Key  in ['0'..'9'] ) then
             Key := Chr( 0 );
     end;
end;


procedure TConfiguraKiosco.edClaveExit(Sender: TObject);
begin
     if ( Length(edClave.Text) < 4 ) then
     begin
          EscribeError( '� Clave del Administrador debe tener m�nimo 4 d�gitos !' );
          ActiveControl := edClave;
     end
end;

procedure TConfiguraKiosco.edConfirmacionExit(Sender: TObject);
begin
     if ( Length(edConfirmacion.Text) < 4 ) then
     begin
          EscribeError( '� Clave del Administrador debe tener m�nimo 4 d�gitos !' );
          ActiveControl := edConfirmacion;
     end
end;

end.
