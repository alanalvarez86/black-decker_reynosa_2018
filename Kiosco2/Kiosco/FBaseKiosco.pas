unit FBaseKiosco;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     OleCtrls, SHDocVw, ExtCtrls, StdCtrls, StrUtils, ZetaCommonClasses,
     fcImager, fcpanel, fcButton, fcImgBtn, fcText,
     fcShapeBtn, fcClearPanel, {fcButtonGroup,}fcBitmap,
     ZetaCommonLists, QRPrntr, Buttons, Variants,ZetaMessages;

{.$DEFINE USE_SHAPE_BUfTTONS}
{$DEFINE FIX_EMP_ANTERIOR}
type
  eTipoPanel = (tpBrowser, tpReportes, tpNip);
  TEstiloBoton = class(TObject)
  private
    { Private declarations }
    FFontItalic: Boolean;
    FFontBold: Boolean;
    FFontUnder: Boolean;
    FFontSize: Integer;
    FFontName: String;
    FCodigo: String;
    FColorBoton: TColor;
    FFontColor: TColor;
    FNombre: String;
    FBitmap: string;
    {$ifndef USE_SHAPE_BUTTONS}
    FButtonExt: TfcImageBtn;
    {$endif}
  public
    { Public declarations }
    property Codigo: String read FCodigo write FCodigo;
    property ColorBoton: TColor read FColorBoton write FColorBoton;
    property FontName: String read FFontName write FFontName;
    property FontSize: Integer read FFontSize write FFontSize;
    property FontColor: TColor read FFontColor write FFontColor;
    property FontBold: Boolean read FFontBold write FFontBold;
    property FontItalic: Boolean read FFontItalic write FFontItalic;
    property FontUnder: Boolean read FFontUnder write FFontUnder;
    property Nombre: String read FNombre write FNombre;
    property Bitmap: string read FBitmap write FBitmap;
    {$ifndef USE_SHAPE_BUTTONS}
    property ButtonExt: TfcImageBtn read FButtonExt write FButtonExt;
    {$endif}
  end;
  TBaseKiosco = class;
  TListaBotones = class;
  TBotonKiosco = class( TObject )
  private
    { Private declarations }
    FLista: TListaBotones;
    FLetrero: String;
    FURL: String;
    FPantalla: String;
    FOrden: Integer;
    FAccion: eAccionBoton;
    FAltura: Integer;
    FLugar: eLugarBoton;
    FSeparacion: Integer;
    FEstilo: TEstiloBoton;
    FPosicion: integer;
    FShapeButton: {$ifdef USE_SHAPE_BUTTONS}TfcShapeBtn{$else}TfcImageBtn{$endif};
    //FShapeButton : TfcCustomBitBtn;
    //FButtonClass: TfcCustomBitBtnClass;
    FFormaPantalla: TBaseKiosco;
    FReporte: integer;
    FImpresionDirecta: Boolean;
    //FUsaEmpresaReportes: Boolean;
    FUsaEmpEmpleado: Boolean;
    FPosIcono: ePosicionIcono;
    FIcono: string;
    FMarco: string;
    FRestringir: Boolean;
    procedure SetAltura( const Value: Integer );
    procedure SetEstilo( const Value: TEstiloBoton );
  public
    { Public declarations }
    constructor Create( Lista: TListaBotones );
    destructor Destroy; override;
    property Accion: eAccionBoton read FAccion write FAccion;
    property Altura: Integer read FAltura write SetAltura;
    property Letrero: String read FLetrero write FLetrero;
    property Lugar: eLugarBoton read FLugar write FLugar;
    property Separacion: Integer read FSeparacion write FSeparacion;
    property Orden: Integer read FOrden write FOrden;
    property Pantalla: String read FPantalla write FPantalla;
    property URL: String read FURL write FURL;
    property Estilo: TEstiloBoton read FEstilo write SetEstilo;
    property Posicion: integer read FPosicion write FPosicion;
    property Reporte: integer read FReporte write FReporte;
    property ImpresionDirecta: Boolean read FImpresionDirecta write FImpresionDirecta; 
    property Icono: string read FIcono write FIcono;
    property PosIcono: ePosicionIcono read FPosIcono write FPosIcono;
    property UsaEmpEmpleado: Boolean read FUsaEmpEmpleado write FUsaEmpEmpleado;
    property ShapeButton: {$ifdef USE_SHAPE_BUTTONS}TfcShapeBtn{$else}TfcImageBtn{$endif} read FShapeButton write FShapeButton;
    property Marco: String read FMarco write FMarco;
    property Restringir: Boolean read FRestringir write FRestringir;
    //property ShapeButton: TfcCustomBitBtn read FShapeButton write FShapeButton;

    procedure DoClick(Sender: TObject);
    procedure MuestraPantalla;
    procedure Prepara;
  end;
  TListaBotones = class( TObject )
  private
    { Private declarations }
    FForma: TBaseKiosco;
    FBotones: TList;
    //FButtonGroup: TfcButtonGroup;
    FButtonGroup: TfcPanel;
    FEstiloNormal: TEstiloBoton;
    FEstiloSelect: TEstiloBoton;
    FAltura: Integer;
    FSeparacion: Integer;
    FAncho: integer;
    FAlineacion: TAlign;
    FColore: TColor;
    function GetBoton(Index: Integer): TBotonKiosco;
    procedure Delete(const Index: Integer);
    procedure SeleccionaBoton(const Index: Integer);
  public
    { Public declarations }
    constructor Create( Forma: TBaseKiosco );
    destructor Destroy; override;
    property Altura: Integer read FAltura write FAltura;
    property Boton[ Index: Integer ]: TBotonKiosco read GetBoton;
    //property ButtonGroup: TfcButtonGroup read FButtonGroup write FButtonGroup;
    property ButtonGroup: TfcPanel read FButtonGroup write FButtonGroup;
    property EstiloNormal: TEstiloBoton read FEstiloNormal;
    property EstiloSelect: TEstiloBoton read FEstiloSelect;
    property Separacion: Integer read FSeparacion write FSeparacion;
    property Colore : TColor read FColore write FColore;
    property Alineacion : TAlign read FAlineacion write FAlineacion;
    property Ancho : integer read FAncho write FAncho;
    function Add: TBotonKiosco;
    function Count: Integer;
    procedure Clear;
    procedure DoClick( Boton: TBotonKiosco );
    procedure Prepara;
    procedure Show;


  end;
  TBaseKiosco = class(TForm)
    PanelFijo: TPanel;
    WebBrowser: TWebBrowser;
    Timer: TTimer;
    fcPanelBotones: TfcPanel;
    fcImagenFondo: TfcImager;
    PanelReportes: TPanel;
    Toolbar: TPanel;
    Print: TSpeedButton;
    ZoomToFit: TSpeedButton;
    ZoomTo100: TSpeedButton;
    ZoomToWidth: TSpeedButton;
    FirstPage: TSpeedButton;
    PrevPage: TSpeedButton;
    NextPage: TSpeedButton;
    LastPage: TSpeedButton;
    QRPreview: TQRPreview;
    StatusPanel: TPanel;
    Panel1: TPanel;
    Status: TLabel;
    PanelGeneral: TPanel;
    ImageBlanca: TImage;
    Imagen: TImage;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
    procedure AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
    procedure PrintClick(Sender: TObject);
    procedure ZoomToFitClick(Sender: TObject);
    procedure ZoomToWidthClick(Sender: TObject);
    procedure ZoomTo100Click(Sender: TObject);
    procedure FirstPageClick(Sender: TObject);
    procedure PrevPageClick(Sender: TObject);
    procedure NextPageClick(Sender: TObject);
    procedure LastPageClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
    procedure QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QRPreviewProgressUpdate(Sender: TObject; Progress: Integer);
    procedure WebBrowserNavigateComplete2(Sender: TObject; const pDisp: IDispatch; const URL: OleVariant);
  private
    { Private declarations }
    FListaBotones: TListaBotones;
    FBotonNormal: TEstiloBoton;
    FBotonSelect: TEstiloBoton;
    //FBotonActivo: Integer;
    FURLContenido: String;
    FURLPanel: String;
    FTimeout: Integer;
    FBrowserPanel : TWebBrowser;
    FUltimoBoton: Integer;
    FMostrarEncuesta:Boolean;
    FImprimiendo: Boolean;
    //procedure ManejaMensaje(var Msg: TMsg; var Handled: Boolean);
    function PrendeBoton( iBoton: Integer ): Boolean;
    function GetLastBoton: TBotonKiosco;
    function StrToken( var sValue: String; const cSeparator: Char ): String;
    function ImprimeReporte( const iReporte : Integer; const lUsaEmpresaEmpleado: Boolean; const lImprimeDirecto: Boolean = FALSE ): Boolean;
    procedure DoClick( Boton: TBotonKiosco );
    procedure GetEstilo( const sEstilo: String; oEstilo: TEstiloBoton );
    procedure GetContenido( const sURL: String );
    procedure Regresar;
    procedure ShowPanelFijo;
    procedure CreaControles;
    procedure CreaControlesPreview;
    procedure ResetTimer( const iTimeout: Integer );
    procedure ShowPanel( const eTipo : eTipoPanel );
    procedure CierraPreviewReportes;
    procedure UpdateInfo;
    procedure LimpiaPanelFijo;
    procedure DisableControls;
    procedure EnableControlsPreview( const lEnable: Boolean );
    procedure ObtieneDatosURL(URL: String; var iNumero, iAnio: Integer; var eTipo: eTipoPeriodo; var iReporte: Integer; var lUsaEmpresaEmpleado, lImprimeDirecto: Boolean);
    procedure ParentNotify(var Msg: TMessage); message WM_PARENTNOTIFY;
    procedure WMWizardEnd(var Message: TMessage); message WM_WIZARD_END;
    procedure WMWizardError(var Message: TMessage); message WM_WIZARD_ERROR;
    //procedure PintaBotonSeleccionado( const oBotonSelec: TfcCustomBitBtn );
  public
    { Public declarations }
    property  Timeout: Integer read FTimeout write FTimeout;
    procedure ShowContenido;
    //procedure SetVisibleBoton( const iBoton: Integer );
    procedure ConfiguraPantalla;
    procedure ReacomodaBotones;
    procedure ShowPrimerBoton;
    procedure SetURLContenido( const sURL: String );
    procedure CleanPantallaEmpAnterior;
    procedure ShowEncuesta;
  end;

implementation

uses DCliente,
     DReportes,
     FPreview,
     //FCAmbiaNip,
     FDlgInformativo,
     ZetaCommonTools,
     DBasicoCliente,
     FKioscoRegistry,
     FToolsKiosco,
     ZetaDialogo,
     DGlobalComparte;

{$R *.DFM}

const
     K_BLANK_URL = 'about:blank';
     K_ESPERE_UN_MOMENTO = 'http://kiosco/Kiosco2/Reporte.aspx?Pagina=EspereUnMomento.htm';
     K_TAG_LISTADO_NOMINA = '@ListadoNominas';
     K_TAG_RECIBO_NOMINA = '@ReciboNomina';
     K_SEPARADOR_URL = ';';
     K_SEPARADOR_DIGITOS = '=';

{******* TBaseKiosco *********}

procedure TBaseKiosco.FormCreate(Sender: TObject);
begin
     fcImagenFondo.Align := alClient;
     //fcButtonGroup.Align := alClient;

     FListaBotones := TListaBotones.Create( Self );
     //FListaBotones.ButtonGroup := fcButtonGroup;

     FListaBotones.ButtonGroup := fcPanelBotones;

     FBotonNormal := TEstiloBoton.Create;
     FBotonSelect := TEstiloBoton.Create;
     //FBotonActivo := 0;
     FTimeout     := dmCliente.TimeoutDatos;
     CreaControles;
     CreaControlesPreview;

     FImprimiendo := FALSE;

end;

procedure TBaseKiosco.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FBotonNormal );
     FreeAndNil( FBotonSelect );
     FreeAndNil( FListaBotones );
     FreeAndNil( FBrowserPanel );
end;

procedure TBaseKiosco.CreaControles;
var
   oBoton: TBotonKiosco;
   sImagen: string;
begin
     with dmCliente.cdsPantalla do
     begin
          with PanelFijo do
          begin
               {$ifdef FALSE}
               Color := FieldByName( 'KS_PF_COLR' ).AsInteger;
               Alignment := TAlignment( FieldByName( 'KS_PF_ALIN' ).AsInteger );
               {$endif}
               Height := FieldByName( 'KS_PF_SIZE' ).AsInteger;
               { WebBrowser del panel fijo }
               FURLPanel := dmCliente.GetURLKiosco( FieldByName( 'KS_PF_URL' ).AsString );
               if ( FURLPanel <> '' ) then
               begin
                    FreeAndNil( FBrowserPanel );
                    FBrowserPanel := TWebBrowser.Create( Self );
                    TWinControl( FBrowserPanel ).Parent := PanelFijo;
                    with FBrowserPanel do
                    begin
                         AddressBar := False;
                         MenuBar := False;
                         Silent := True;
                         StatusBar := False;
                         Offline := False;

                         Align := alClient;
                         FullScreen := True;
                         OnBeforeNavigate2 := AntesDeNavegar;
                    end;
               end;
          end;
          {********************************************************************}
          {*********************** AQUI se fijan las **************************}
          {*********************** propiedades de la **************************}
          {*********************** lista de botones ***************************}
          {********************************************************************}

          with fcPanelBotones do
          begin
               Color := FieldByName( 'KS_PB_COLR' ).AsInteger;
               if ( eKJustificacion( FieldByName( 'KS_PB_ALIN' ).AsInteger ) =  ekIzquierda ) then
                  Align := alLeft
               else
                   Align := alRight;

               Width := FieldByName( 'KS_PB_SIZE' ).AsInteger;
               sImagen := FieldByName('KS_BITMAP').AsString;



               fcImagenFondo.Visible := StrLleno( sImagen ) and FileExists( sImagen );

               if fcImagenFondo.Visible then
                  fcImagenFondo.Picture.LoadFromFile( sImagen );

               {$ifdef USE_SHAPE_BUTTONS}
               //pnSeparaTop.Color := Color;
               //pnSeparaBottom.Color := Color;
               {$endif}
          end;

          with FListaBotones do
          begin
               GetEstilo( FieldByName( 'KS_EST_NOR' ).AsString, EstiloNormal );
               GetEstilo( FieldByName( 'KS_EST_SEL' ).AsString, EstiloSelect );
               Separacion := FieldByName( 'KS_BTN_SEP' ).AsInteger;
               Altura := FieldByName( 'KS_BTN_ALT' ).AsInteger;
               Ancho := FieldByName( 'KS_PB_SIZE' ).AsInteger;

          end;
          dmCliente.GetBotones( FieldByName( 'KS_CODIGO' ).AsString );

          {********************************************************************}

     end;
     FListaBotones.Clear;
     with dmCliente.cdsBotones do
     begin
          while not Eof do
          begin
               oBoton := FListaBotones.Add;
               with oBoton do
               begin
                    Altura := FieldByName( 'KB_ALTURA' ).AsInteger;
                    Lugar := eLugarBoton( FieldByName( 'KB_LUGAR' ).AsInteger );
                    Separacion := FieldByName( 'KB_SEPARA' ).AsInteger;
                    Accion := eAccionBoton( FieldByName( 'KB_ACCION' ).AsInteger );
                    URL := dmCliente.GetURLKiosco( FieldByName( 'KB_URL' ).AsString, Accion );
                    Pantalla := FieldByName( 'KB_SCREEN' ).AsString;
                    Orden := FieldByName( 'KB_ORDEN' ).AsInteger;
                    Letrero := FieldByName( 'KB_TEXTO' ).AsString;
                    Reporte := FieldByName( 'KB_REPORTE' ).AsInteger;
                    ImpresionDirecta:= zStrToBool(FieldByName('KB_IMP_DIR').AsString );
                    Icono := FieldByName( 'KB_BITMAP' ).AsString;
                    PosIcono := ePosicionIcono( FieldByName( 'KB_POS_BIT' ).AsInteger );
                    Marco:= FieldByName('KS_CODIGO').AsString;
                    Restringir:= zStrToBool( FieldByName('KB_RESTRIC').AsString );
                    { Se cambi� para ya no estuviera fijo, el dato viene del bot�n
                    UsaEmpresaReportes := TRUE;//zStrToBool( FieldByName( 'KB_EMPRESA' ).AsString );  }
                    UsaEmpEmpleado := zStrToBool( FieldByName( 'KB_USA_EMPL' ).AsString );
                    Posicion := FListaBotones.Count;
               end;
               Next;
          end;
     end;
     {
     Tiene que ser en una segunda pasada para soportar la "recursividad"
     de un bot�n que llama a una forma con otros botones.

     }
     with FListaBotones do
     begin
          Prepara;
          Show;
     end;
end;

// Nota: Se puede optimizar teniendo un TStringList con los estilos
// porque se van a estar reutilizando.
// Un solo query donde te traes todos los estilos. Entonces se hace un ciclo para
// llenar la lista con todos los estilos. Luego se buscan con el CODIGO
// y en el objeto de la lista se tiene el TEstiloBoton
procedure TBaseKiosco.GetEstilo( const sEstilo: String; oEstilo: TEstiloBoton);
begin
     with dmCliente do
     begin
          GetEstilo( sEstilo );
          with oEstilo do
          begin
               Codigo := sEstilo;
               with cdsEstilo do
               begin
                    Nombre      := FieldByName( 'KE_NOMBRE' ).AsString;
                    ColorBoton  := FieldByName( 'KE_COLOR' ).AsInteger;
                    FontName    := FieldByName( 'KE_F_NAME' ).AsString;
                    FontSize    := FieldByName( 'KE_F_SIZE' ).AsInteger;
                    FontColor   := FieldByName( 'KE_F_COLR' ).AsInteger;
                    FontBold    := ZetaCommonTools.zStrToBool( FieldByName( 'KE_F_BOLD' ).AsString );
                    FontItalic  := ZetaCommonTools.zStrToBool( FieldByName( 'KE_F_ITAL' ).AsString );
                    FontUnder   := ZetaCommonTools.zStrToBool( FieldByName( 'KE_F_SUBR' ).AsString );
                    Bitmap      := FieldByName( 'KE_BITMAP' ).AsString;
                    {$ifndef USE_SHAPE_BUTTONS}
                    ButtonExt  := TfcImageBtn.Create(self);
                    if StrLleno( Bitmap ) and FileExists( Bitmap ) then
                    begin
                         Imagen.Picture.LoadFromFile( Bitmap );
                         ButtonExt.Image.LoadFromBitmap( Imagen.Picture.Bitmap );
                         ButtonExt.ImageDown.LoadFromBitmap( Imagen.Picture.Bitmap );
                         Imagen.Picture := NIL;
                    end
                    else
                    begin
                         ButtonExt.Image.LoadFromBitmap( ImageBlanca.Picture.Bitmap );
                         ButtonExt.ImageDown.LoadFromBitmap( ImageBlanca.Picture.Bitmap );
                         ButtonExt.TransparentColor := clGray;
                         ButtonExt.Color := ColorBoton;
                    end;
                    {$endif}
              end;
          end;
     end;
end;

procedure TBaseKiosco.EnableControlsPreview( const lEnable: Boolean );
begin
     Print.Enabled := lEnable;
     ZoomToFit.Enabled := lEnable;
     ZoomToWidth.Enabled := lEnable;
     ZoomToWidth.Down := TRUE;
     Zoomto100.Enabled := lEnable;
     FirstPage.Enabled := lEnable;
     PrevPage.Enabled := lEnable;
     NextPage.Enabled := lEnable;
     LastPage.Enabled := lEnable;
end;

procedure TBaseKiosco.DisableControls;
begin
     PanelGeneral.Visible := FALSE;
     WebBrowser.Navigate( K_BLANK_URL );
     {$ifdef FIX_EMP_ANTERIOR}
     LimpiaPanelFijo;
     {$endif}
     CierraPreviewReportes;
end;

procedure TBaseKiosco.DoClick( Boton: TBotonKiosco );
var
   eTipoBit: eTipoBitacora;
begin
     if FImprimiendo and ( Boton.Accion = abReporte ) then  // No se puede invocar otro reporte hasta que termine el anterior
        Exit;
     FUltimoBoton:= ( Boton.Posicion - 1 );
     Timer.Enabled := FALSE;
     //DisableControls;
     ShowPanel( tpBrowser );

     with Boton do
     begin
          //FBotonActivo := Orden;
          eTipoBit:= tbNormal;
          case Accion of
             abContenido:
             begin
                  dmCliente.EscribeBitacora( clbContenido, eTipoBit, Boton );
                  GetContenido( URL );
                  ResetTimer( Self.Timeout );
             end;
             abPantalla:
             begin
                  MuestraPantalla;
                  { De regreso de la otra pantalla }
                  PrendeBoton( 1 );
                  ResetTimer( self.Timeout );
             end;
             abMisDatos:
             begin
                  dmCliente.EscribeBitacora( clbMisDatos, eTipoBit, Boton );
                  GetContenido( URL );
                  ResetTimer( Self.Timeout );
             end;
             abRegresar:
                  Regresar;
             abReporte:
             begin
                  dmCliente.ParametrosURL.Clear;
                  EnableControlsPreview( FALSE );
                  Print.Tag := Reporte;
                  if not ( ImprimeReporte( Print.Tag, UsaEmpEmpleado, FImpresionDirecta ) ) then
                     eTipoBit:= tbError;
                  dmCliente.EscribeBitacora( clbReporte, eTipoBit, Boton );
             end;
             abCambiaNip:
             begin
                  with dmCliente do
                  begin
                       ShowPanel( tpNip );
                       EscribeBitacora( clbCambioNIP );
                       CambiaNIPEmpleado( PanelGeneral );
                       ResetTimer( self.Timeout );
                  end;
             end;
         end;
     end;
end;

function TBaseKiosco.ImprimeReporte( const iReporte : Integer; const lUsaEmpresaEmpleado: Boolean; const lImprimeDirecto: Boolean = FALSE): Boolean;
begin
     FImprimiendo := TRUE;
     try
        if ( not lImprimeDirecto ) then
           ShowPanel( tpReportes );
        Self.QrPreview.Visible := FALSE;
        dmCliente.QRPreview := Self.QrPreview;
        dmCliente.UsaEmpresaEmpleado := lUsaEmpresaEmpleado;
        if lImprimeDirecto then
        begin
             Result:= dmCliente.ImprimeReporte( iReporte );
        end
        else
        begin
             Result:= dmCliente.GeneraReporte( iReporte );
             if ( Result ) then
                Self.QrPreview.Visible := TRUE;
             ResetTimer( self.Timeout );
        end;
     finally
            FImprimiendo := FALSE;
     end;
end;

procedure TBaseKiosco.CierraPreviewReportes;
 var
    i: integer;
    oControl : TControl;
begin
     if ( PanelReportes <> NIL ) then
     begin
          PanelReportes.Visible := FALSE;

          if ( PanelReportes.ControlCount <> 0 ) then
          begin
               for i:= 0 to PanelReportes.ControlCount - 1 do
               begin
                    oControl := PanelReportes.Controls[i];
                    if ( oControl is TPreview ) then
                    begin
                         TPreview( oControl ).Parent := NIL;
                         TPreview( oControl ).CierraPreview;
                         Break;
                    end;
               end;
          end;
     end;
end;

procedure TBaseKiosco.ShowPanel( const eTipo : eTipoPanel );


   procedure Alinea( oControl: TWinControl; aAlinea: TAlign );
   begin
        oControl.Align := aAlinea;
        oControl.Visible := aAlinea = alClient;
   end;

begin
     Alinea( WebBrowser, alNone );
     Alinea( PanelReportes, alNone );
     Alinea( PanelGeneral, alNone );

     case eTipo of
          tpBrowser:  Alinea( WebBrowser, alClient );
          tpReportes: Alinea( PanelReportes, alClient );
          tpNip:      Alinea( PanelGeneral, alClient );
     end;
end;

procedure TBaseKiosco.GetContenido( const sURL: String );
begin
     with WebBrowser do
     begin
          {$ifdef FIX_EMP_ANTERIOR}
          Navigate( K_BLANK_URL );
          {$endif}
          Navigate( sURL );
     end;
end;

procedure TBaseKiosco.Regresar;
begin
     ModalResult := mrOK;
end;

procedure TBaseKiosco.ShowContenido;
begin
     ResetTimer( self.Timeout );
     ShowModal;
end;

function TBaseKiosco.PrendeBoton( iBoton: Integer ): Boolean;
var
   lEsBotonRegresar: Boolean;
begin
     with FListaBotones do
     begin
          Result:= TRUE;
          if Count > iBoton then
          begin
               while( ( not Boton[iBoton - 1].ShapeButton.Visible ) or ( ( Boton[iBoton - 1].Accion in [ abPantalla ] ) ) ) do
               begin
                    Inc(iBoton);
               end;
               lEsBotonRegresar:= ( Boton[iBoton - 1].Accion in [ abRegresar ] );
               { Esta condicion es para saber cuando prender el boton de regresar:
                Si el boton anterior esta visible y es de tipo pantalla, no hacer click pero mostrar un mensaje de informacion }
               if ( (  lEsBotonRegresar )  and
                    ( Boton[iBoton - 2].ShapeButton <> NIL ) and
                    ( Boton[iBoton - 2].ShapeButton.Visible  ) and
                    ( Boton[iBoton - 2 ].Accion in [ abPantalla ] ) ) then
               begin
                    FDlgInformativo.MuestraDialogoInf( 'No existen datos que mostrar' );
               end
               else
               begin
                    with Boton[iBoton - 1].ShapeButton do
                    begin
                         Click;
                         Down := True;
                         Result:= not ( lEsBotonRegresar );
                    end;
               end;

          end
          else
              FDlgInformativo.MuestraDialogoInf( 'No existe la lista de botones' );
     end;
end;

procedure TBaseKiosco.ShowPrimerBoton;
begin
     ShowPanelFijo;
     ResetTimer( Self.Timeout );
     ConfiguraPantalla;
     if FMostrarEncuesta then
        fcPanelBotones.Show; 
     if ( PrendeBoton( 1 ) ) and not FMostrarEncuesta then
        ShowModal;
end;

procedure TBaseKiosco.ShowEncuesta;
begin
     ResetTimer( Self.Timeout );
     ShowPanelFijo;
     fcPanelBotones.Hide;
     FMostrarEncuesta := KioskoRegistry.MostrarEncuestas;  
     with WebBrowser do
     begin
          Align := alClient;
          Navigate(Format(ZetaCommonTools.VerificaDirURL(GlobalComparte.GetGlobalString( K_KIOSCO_DIRECTORIO_RAIZ ))+'Encuestas.aspx?Kiosco=%s',[dmCliente.GetCodigoKioscoID]) );
     end;
     ShowModal;
end;

procedure TBaseKiosco.SetURLContenido( const sURL: String);
begin
     FURLContenido := sURL;
     GetContenido( sURL );
end;

procedure TBaseKiosco.ShowPanelFijo;
begin
     if ( FBrowserPanel <> NIL ) then
     begin
          with FBrowserPanel do
          begin
               Navigate( K_BLANK_URL );
               Navigate( FURLPanel );
          end;
     end;
end;

procedure TBaseKiosco.LimpiaPanelFijo;
begin
     if ( FBrowserPanel <> NIL ) then
     begin
          with FBrowserPanel do
          begin
               Navigate( K_BLANK_URL );
          end;
     end;
end;

procedure TBaseKiosco.CleanPantallaEmpAnterior;
begin
     {Para que cuando regrese de nuevo X empleado no vea los datos del empleado
                  anterior mientras se carga su infomaci�n }
     LimpiaPanelFijo;
     WebBrowser.Navigate( K_BLANK_URL );
end;

procedure TBaseKiosco.TimerTimer(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

procedure TBaseKiosco.ResetTimer( const iTimeout: Integer );
begin
     Timer.Enabled  := FALSE;
     Timer.Interval := iTimeout;
     Timer.Enabled  := TRUE;
end;

function TBaseKiosco.GetLastBoton: TBotonKiosco;
begin
     if ( FListaBotones <> Nil ) and ( FUltimoBoton <= FListaBotones.Count ) then //Solamente es por prevenir
        Result:= FListaBotones.Boton[FUltimoBoton]
     else
         Result:= Nil;
end;

procedure TBaseKiosco.AntesDeNavegar(Sender: TObject; const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData, Headers: OleVariant; var Cancel: WordBool);
var
   iNumero, iAnio, {iTipo,} iReporte: Integer;
//   ParametrosPagina: TZetaParams;
   lUsaEmpresaEmpleado : Boolean;
   lImprimeDirecto: Boolean;
   eTipoPer : eTipoPeriodo;
   oBoton: TBotonKiosco;
   sMarco: String;
begin
     if FImprimiendo then          // No se puede invocar otro reporte hasta que termine el anterior
        Exit;
        
     lUsaEmpresaEmpleado := TRUE;
     if ( Pos( UpperCase(K_TAG_LISTADO_NOMINA),UpperCase(URL) ) > 0 ) or
        ( Pos( UpperCase(K_TAG_RECIBO_NOMINA),UpperCase(URL) ) > 0 ) then
     begin
          ObtieneDatosURL( URL, iNumero, iAnio, eTipoPer, iReporte, lUsaEmpresaEmpleado, lImprimeDirecto );
          with dmCliente.ParametrosURL do
          begin
               Clear;
               AddInteger( 'Year', iAnio);
               AddInteger( 'Tipo', Ord(eTipoPer) );
               AddInteger( 'Numero', iNumero );
          end;
          EnableControlsPreview( FALSE );
          Print.Tag := iReporte;
          oBoton := GetLastBoton;
          if Assigned( oBoton ) then
             sMarco := oBoton.Marco;
          dmCliente.BitacoraImprimirReporte( iReporte, sMarco, ImprimeReporte( iReporte, lUsaEmpresaEmpleado, lImprimeDirecto ) );
          Cancel := True;
     end
     else
         ResetTimer( Self.Timeout );
end;

{ ********* TBotonKiosco ****** }

constructor TBotonKiosco.Create( Lista: TListaBotones );
begin
     FLista := Lista;
end;

destructor TBotonKiosco.Destroy;
begin
     inherited Destroy;
end;

procedure TBotonKiosco.SetAltura( const Value: Integer );
begin
     if ( Value <= 0 ) then
        FAltura := FLista.Altura
     else
         FAltura := Value;
end;

procedure TBotonKiosco.SetEstilo( const Value: TEstiloBoton );
begin
     if FEstilo <> Value then
     begin
          FEstilo := Value;

           with FEstilo do
           begin
                with FShapeButton do
                begin
                     {$ifndef USE_SHAPE_BUTTONS}
                     TransparentColor := FButtonExt.TransparentColor;
                     Color := FButtonExt.Color;
                     ExtImage := FButtonExt;
                     ExtImageDown := FButtonExt;
                     {$endif}
                     with Font do
                     begin
                          Name := FontName;
                          Color := FontColor;
                          Size := FontSize;
                          Style := [];
                          if FontBold then
                             Style := Style + [ fsBold ];
                          if FontItalic then
                             Style := Style + [ fsItalic ];
                          if FontUnder then
                             Style := Style + [ fsUnderline ];
                     end;
                end;
           end;
     end;
end;

procedure TBotonKiosco.Prepara;
begin
     if ( Accion = abPantalla ) then
        FFormaPantalla := dmCliente.GetPantalla( Pantalla );
end;

procedure TBotonKiosco.MuestraPantalla;
begin
     if Assigned( FFormaPantalla ) then
     begin
          FFormaPantalla.ShowPrimerBoton;
     end;
end;

procedure TBotonKiosco.DoClick(Sender: TObject);
begin
     FLista.DoClick( Self );
end;

{ ******* TListaBotones ******* }

constructor TListaBotones.Create( Forma: TBaseKiosco );
begin
     FForma := Forma;
     FBotones := TList.Create;
     FEstiloNormal := TEstiloBoton.Create;
     FEstiloSelect := TEstiloBoton.Create;
end;

destructor TListaBotones.Destroy;
begin
     Clear;
     FreeAndNil( FEstiloSelect );
     FreeAndNil( FEstiloNormal );
     FreeAndNil( FBotones );
     inherited Destroy;
end;

function TListaBotones.Add: TBotonKiosco;
begin
     Result := TBotonKiosco.Create( Self );
     try
        FBotones.Add( Result );
     except
           on Error: Exception do
           begin
                FreeAndNil( Result );
                raise;
           end;
     end;
end;

procedure TListaBotones.Clear;
var
   i: Integer;
begin
     with FBotones do
     begin
          for i := ( Count - 1 ) downto 0 do
          begin
               Self.Delete( i );
          end;
          Clear;
     end;
end;

function TListaBotones.Count: Integer;
begin
     Result := FBotones.Count;
end;

procedure TListaBotones.Delete(const Index: Integer);
begin
     Boton[ Index ].Free;
     FBotones.Delete( Index );
end;

function TListaBotones.GetBoton(Index: Integer): TBotonKiosco;
begin
     with FBotones do
     begin
          if ( Index >= 0 ) and ( Index < Count ) then
             Result := TBotonKiosco( Items[ Index ] )
          else
              Result := nil;
     end;
end;

procedure TListaBotones.SeleccionaBoton(const Index: Integer);
 var
    i: integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          Boton[ i ].Estilo := EstiloNormal;
     end;

     i := Index- 1;
     if (Count > i) then
        Boton[ Index- 1 ].Estilo := EstiloSelect;
end;


procedure TListaBotones.Prepara;
var
   i: Integer;
begin
     for i := 0 to ( Count - 1 ) do
     begin
          Boton[ i ].Prepara;
     end;
end;
{$ifdef BOTONES_ANTERIOR}
procedure TListaBotones.Show;
var
   i: Integer;
   {$ifdef USE_SHAPE_BUTTONS}
   Button: TfcShapeBtn;
   {$else}
   Button: TfcImageBtn;
   {$endif}
begin
     with FButtonGroup do
     begin
          ClickStyle := bcsRadioGroup;
          //AutoBold := True;
          {$ifdef USE_SHAPE_BUTTONS}
          ButtonClassName := 'TfcShapeBtn';
          {$else}
          ButtonClassName := 'TfcImageBtn';
          {$endif}
          ControlSpacing := Separacion;
          ShowDownAsUp := False;
          MaxControlSize := Altura;

          with ButtonItems do
          begin
               Clear;
               BeginUpdate;
               try
                  for i := 0 to ( Self.Count - 1 ) do
                  begin
                       {$ifdef USE_SHAPE_BUTTONS}
                       Button := TfcShapeBtn( Add.Button );
                       {$else}
                       Button := TfcImageBtn( Add.Button );
                       {$endif}
                       Boton[ i ].ShapeButton := Button;
                       with Button do
                       begin
                            Height := Altura;
                            with Self.Boton[ i ] do
                            begin
                                 Caption := Letrero;
                                 OnClick := DoClick;
                            end;

                            if StrLleno( Boton[ i ].Icono ) and FileExists( Boton[ i ].Icono ) then
                            begin
                                 Glyph.LoadFromFile( Boton[ i ].Icono );
                                 Layout := Boton[ i ].PosIcono;
                            end;
                            Boton[i].Estilo := EstiloNormal;

                            {$ifdef USE_SHAPE_BUTTONS}
                            Shape := bsRoundRect;
                            {$endif}
                            with TextOptions do
                            begin
                                 WordWrap := True;
                                 Options := [ toShowEllipsis ];
                            end;
                            Tag := i;
                       end;
                  end;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;
{$ELSE}
procedure TListaBotones.Show;
var
   i, nTop, nSepara, nAltura: Integer;

   {$ifdef USE_SHAPE_BUTTONS}
   Button: TfcShapeBtn;
   {$else}
   Button: TfcImageBtn;
   {$endif}
begin
     //fcPanelBotones
     nTop := 0;

     for i := 0 to ( Self.Count - 1 ) do
     begin
          {$ifdef USE_SHAPE_BUTTONS}
          Button := TfcShapeBtn( Add.Button );
          {$else}
          Button := TfcImageBtn.Create(FButtonGroup);
          {$endif}
          Button.Parent := FButtonGroup;
          Boton[ i ].ShapeButton := Button;

          case Boton[ i ].Lugar of
               lbDefault   :  nSepara := Separacion;
               lbEspaciado :  nSepara := Boton[ i ].Separacion;
               else
                   nSepara := 0;
          end;

          nAltura := Boton[ i ].Altura;
          if ( nAltura < 0 ) then nAltura := Altura;

          if ( Boton[ i ].Lugar = lbAbajo ) then
          begin
               Button.Top   := FButtonGroup.Height - nAltura - 5;
               Button.Anchors := [akLeft,akBottom];    // Para soportar el 'Maximize' de la forma
          end
          else
          begin
               nTop := nTop + nSepara;
               Button.Top := nTop;
               nTop := nTop + nAltura;
          end;


          with Button do
          begin
               Width := Ancho;
               Height := nAltura;
               with Self.Boton[ i ] do
               begin
                    Caption := Letrero;
                    OnClick := DoClick;
               end;

               if StrLleno( Boton[ i ].Icono ) and FileExists( Boton[ i ].Icono ) then
               begin
                    Glyph.LoadFromFile( Boton[ i ].Icono );
                    Layout := Boton[ i ].PosIcono;
               end;
               Boton[i].Estilo := EstiloNormal;

               {$ifdef USE_SHAPE_BUTTONS}
               Shape := bsRoundRect;
               {$endif}
               with TextOptions do
               begin
                    WordWrap := True;
                    Options := [ toShowEllipsis ];
               end;
               BringToFront;
               Tag := i;
          end;
     end;
end;
{$ENDIF}



procedure TListaBotones.DoClick( Boton: TBotonKiosco );
begin
     SeleccionaBoton( Boton.Posicion );
     FForma.DoClick( Boton );
end;

{ ************ Metodos y eventos para el Preview ***************** }
procedure TBaseKiosco.PrintClick(Sender: TObject);
var
   sMensaje: String;
begin
     try
        Timer.Enabled := FALSE;
        Print.Enabled := False;
        try
           if (QrPreview.QrPrinter <> NIL ) and (QrPreview.QrPrinter.Status = mpFinished) then
           begin
                if ( dmCliente.UltimoRepFormato <> tfImpresora ) then
                   dmCliente.ImprimeReporte( Print.Tag )      // Se envia por Email y se requiere volver a correr la generaci�n del reporte
                else
                begin   // Se envia a impresora optimizado para no volver a invocar la generaci�n del reporte
                     if dmCliente.PuedeImprimirReporte( Print.Tag, dmCliente.GetParamsReporte, sMensaje ) then
                     begin
                          if FToolsKiosco.ImpresoraDisponibe( QrPrinter.PrinterIndex, sMensaje ) then
                          begin
                               QrPreview.QrPrinter.ReportLoaded := TRUE; // No usar esta linea en ningun otro lado del codigo
                               QrPreview.QrPrinter.Print;
                               dmCliente.AgregaSesionImpReportes( Print.Tag );
                               dmCliente.AgregaImpresionNomina( Print.Tag );
                               FDlgInformativo.MuestraDialogoInf( 'El reporte ha sido impreso con �xito' );
                          end
                          else
                              FDlgInformativo.MuestraDialogoInf( sMensaje );
                     end
                     else
                         FDlgInformativo.MuestraDialogoInf( sMensaje );   // No tiene permiso ya de imprimir el reporte que mand�
                end;
           end;
        finally
               Print.Enabled := True;
        end;
        ResetTimer( self.Timeout );
     except
           raise;
     end;
end;

procedure TBaseKiosco.ZoomToFitClick(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.ZoomToFit;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.ZoomTo100Click(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.Zoom := 100;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.ZoomToWidthClick(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.ZoomToWidth;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.FirstPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := 1;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.PrevPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPreview.PageNumber - 1;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.NextPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPreview.PageNumber + 1;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.LastPageClick(Sender: TObject);
begin
     if QRPreview.QRPrinter <> NIL then
        QRPreview.PageNumber := QRPreview.QRPrinter.PageCount;
     UpdateInfo;
     ResetTimer( self.Timeout );
end;



procedure TBaseKiosco.CreaControlesPreview;
begin
     FirstPage.Caption := 'Primer' + CR_LF + 'P�gina';
     LastPage.Caption := 'Ultima' + CR_LF + 'P�gina';
     PrevPage.Caption := 'Anterior';
     NextPage.Caption := 'Siguiente';

     ZoomToFit.Caption := 'P�gina Completa';
     ZoomToWidth.Caption := 'Ancho P�gina';
     ZoomTo100.Caption := 'P�gina al 100%';

     if KioskoRegistry.SoloBotonImprimir then
     begin
          ZoomToFit.Visible := FALSE;
          ZoomToWidth.Visible := FALSE;
          ZoomTo100.Visible := FALSE;
          FirstPage.Visible := FALSE;
          PrevPage.Visible := FALSE;
          NextPage.Visible := FALSE;
          LastPage.Visible := FALSE;
     end;

end;

procedure TBaseKiosco.UpdateInfo;
begin
     if QrPreview.QrPrinter <> NIL then
        Status.Caption := 'P�gina ' + IntToStr(QRPreview.PageNumber) +
                          ' de ' + IntToStr(QRPreview.QRPrinter.PageCount);
end;


procedure TBaseKiosco.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if PanelReportes.Visible then
     begin
          case Key of
               VK_Next : if Shift=[ssCtrl] then
                            LastPageClick(Self)
                         else
                             NextPageClick(Self);
               VK_Prior : if Shift=[ssCtrl] then
                             FirstPageClick(Self)
                          else
                              PrevPageClick(Self);
               VK_Home : FirstPageClick(Self);
               VK_End : LastPageClick(Self);
          end;
     end
     else if PanelGeneral.Visible then
     begin
          ResetTimer( Self.Timeout );
     end;
end;

procedure TBaseKiosco.QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
begin
     UpdateInfo;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.QRPreviewProgressUpdate(Sender: TObject;   Progress: Integer);
begin
     EnableControlsPreview( Progress >= 100 );
end;

procedure TBaseKiosco.QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     with QrPreview do
     begin
          case Button of
               mbLeft: if ( Zoom < 300 ) then Zoom := Zoom + 10;
               mbRight: if ( Zoom > 0 ) then Zoom := Zoom - 10;
          end;
     end;
     ResetTimer( self.Timeout );
end;

procedure TBaseKiosco.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     DisableControls;
end;

procedure TBaseKiosco.ObtieneDatosURL(URL: String; var iNumero, iAnio: Integer; var eTipo: eTipoPeriodo; var iReporte: Integer; var lUsaEmpresaEmpleado, lImprimeDirecto: Boolean);
var
    UrlValores, Numerolbl, AnioLbl, TipoLbl, ReporteLbl, UsaEmpLbl, ImpDirectoLbl: String;
begin
     UrlValores :=  StrToken( URL, K_SEPARADOR_URL ) ;
     Numerolbl :=  StrToken( URL, K_SEPARADOR_URL );
     iNumero := StrToInt( Copy( Numerolbl, Pos( K_SEPARADOR_DIGITOS,Numerolbl)+1, Length(Numerolbl) ));
     TipoLbl := StrToken( URL, K_SEPARADOR_URL );
     eTipo   := eTipoPeriodo( StrToInt( Copy( TipoLbl, Pos( K_SEPARADOR_DIGITOS,TipoLbl)+1, Length(TipoLbl) )));
     AnioLbl := StrToken( URL, K_SEPARADOR_URL );
     iAnio :=   StrToInt( Copy( AnioLbl, Pos( K_SEPARADOR_DIGITOS,AnioLbl)+1, Length(AnioLbl) ));
     ReporteLbl := StrToken( URL, K_SEPARADOR_URL );
     iReporte := StrToInt( Copy( ReporteLbl, Pos( K_SEPARADOR_DIGITOS,ReporteLbl)+1, Length(ReporteLbl) ));
     UsaEmpLbl := StrToken( URL, K_SEPARADOR_URL );
     lUsaEmpresaEmpleado := zStrToBool( Copy( UsaEmpLbl, Pos( K_SEPARADOR_DIGITOS,UsaEmpLbl)+1, Length(UsaEmpLbl) ));
     ImpDirectoLbl := StrToken( URL, K_SEPARADOR_URL );
     lImprimeDirecto := zStrToBool( Copy( ImpDirectoLbl, Pos( K_SEPARADOR_DIGITOS,ImpDirectoLbl)+1, Length(ImpDirectoLbl) ));
end;

function TBaseKiosco.StrToken( var sValue: String; const cSeparator: Char ): String;
var
   i: Word;
begin
     i := Pos( cSeparator, sValue );
     if ( i <> 0 ) then
     begin
          Result := System.Copy( sValue, 1, ( i-1 ) );
          System.Delete( sValue, 1, i );
     end
     else
     begin
          Result := sValue;
          sValue := VACIO;
     end;
end;

procedure TBaseKiosco.ReacomodaBotones;
var
   i, nTop, nSepara, nAltura: Integer;
begin
     with FListaBotones do
     begin
          nTop:= 0;
          for i:= 0  to ( Count - 1 ) do
          begin
               if ( Boton[i].ShapeButton.Visible ) then
               begin
                    case Boton[ i ].Lugar of
                         lbDefault   :  nSepara := Separacion;
                         lbEspaciado :  nSepara := Boton[ i ].Separacion;
                         else
                             nSepara := 0;
                    end;
                    nAltura := Boton[ i ].Altura;
                    if ( nAltura < 0 ) then nAltura := Altura;

                    if ( Boton[ i ].Lugar = lbAbajo ) then
                    begin
                         Boton[i].ShapeButton.Top := FButtonGroup.Height - nAltura - 5;
                    end
                    else
                    begin
                         nTop := nTop + nSepara;
                         Boton[i].ShapeButton.Top := nTop;
                         nTop := nTop + nAltura;
                    end;
               end;
               Next;
          end;
     end;
end;

procedure TBaseKiosco.ConfiguraPantalla;
var
   i: Integer;
   lHayBotones, lEsBotonRegresar: Boolean;
begin
     with dmCliente.cdsAccesoEmp do
     begin
          lHayBotones:= FALSE;
          with FListaBotones do
          begin
               for i:= 0 to ( Count - 1 ) do
               begin
                    with Boton[i] do
                    begin
                         lEsBotonRegresar:= ( Accion = abRegresar );
                         ShapeButton.Visible := ( ( not Restringir ) ) or
                                                ( ( lEsBotonRegresar ) ) or
                                                ( FieldByName('GR_CODIGO').AsInteger = K_GRUPO_SISTEMA ) or
                                                ( Locate( 'KS_CODIGO;KB_ORDEN', VarArrayOf( [ Marco, Orden ] ), [] ) );

                         if ( ShapeButton.Visible ) then
                         begin
                              if ( not lEsBotonRegresar ) then
                                 lHayBotones:= TRUE;
                         end;
                    end;
               end;

               if ( lHayBotones ) then
                  ReacomodaBotones
               else if ( dmCliente.cdsShow.FieldByName('KW_SCR_DAT').AsString = Boton[0].Marco ) then
                    FDlgInformativo.MuestraDialogoInf( 'El empleado no cuenta con privilegios para acceder al sistema' );

          end;
     end;
end;

{$warnings off}
procedure TBaseKiosco.ParentNotify(var Msg: TMessage);
var
   oBoton: TBotonKiosco;
begin
     {Error de window handle: Por aqui pasa cuando el explorer intenta cerrar el browser, como el browser ya
     cerrado no se puede recuperar, se destruye y se vuelve a crear, no se encontr� la propiedad que lo recuperara}
     if (msg.WParamLo = WM_DESTROY) and (WebBrowser <> Nil ) and ( msg.LParam = WebBrowser.Handle) then
     begin
          FreeAndNil(WebBrowser);
          WebBrowser := TWebBrowser.Create( Self );
          TWinControl( WebBrowser ).Parent := Self;
          with WebBrowser do
          begin
               AddressBar:= False;
               MenuBar := False;
               Silent := True;
               StatusBar := False;
               Offline := False;
               Align := alClient;
               FullScreen := False;
               OnBeforeNavigate2 := AntesDeNavegar;
          end;
          if ( FListaBotones <> Nil ) and ( FUltimoBoton <= FListaBotones.Count ) then //Solamente es por prevenir
             oBoton:= FListaBotones.Boton[FUltimoBoton]
          else
              oBoton:= Nil;

          dmCliente.EscribeBitacora( clbErrorBrowser, tbError, oBoton  );
     end;
end;
{$warnings on}

procedure TBaseKiosco.WMWizardEnd(var Message: TMessage);
begin
     dmCliente.DespreparaEnvioCorreo(TRUE);
end;

procedure TBaseKiosco.WMWizardError(var Message: TMessage);
begin
     dmCliente.DespreparaEnvioCorreo(FALSE);
end;

procedure TBaseKiosco.WebBrowserNavigateComplete2(Sender: TObject; const pDisp: IDispatch; const URL: OleVariant);
begin
     if ( Pos( UpperCase('Salir'),UpperCase(URL) ) > 0 )then
     begin
          //sleep(10000);
          ShowPrimerBoton;
     end
end;

end.

