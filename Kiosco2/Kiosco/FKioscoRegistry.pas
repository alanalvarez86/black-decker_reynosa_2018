unit FKioscoRegistry;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Registry,
     ZetaRegistryCliente;

type
  TLogonRegistry = class( TObject )
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FCanWrite: Boolean;
    function GetRegistryKey: String;
    function GetRegistryRoot: HKEY;
    function GetAutoAdminLogon: Boolean;
    function GetDefaultPassword: String;
    function GetDefaultUserName: String;
    function GetDefaultDomainName: String;
    function GetForceAutoLogon: Boolean;
    procedure SetAutoAdminLogon(const Value: Boolean);
    procedure SetDefaultPassword(const Value: String);
    procedure SetDefaultUserName(const Value: String);
    procedure SetForceAutoLogon(const Value: Boolean);
    procedure SetDefaultDomainName(const Value: String);
  protected
    { Protected declarations }
  public
    { Public declarations }
    property CanWrite: Boolean read FCanWrite;
    property DefaultDomainName: String read GetDefaultDomainName write SetDefaultDomainName;
    property DefaultUserName: String read GetDefaultUserName write SetDefaultUserName;
    property DefaultPassword: String read GetDefaultPassword write SetDefaultPassword;
    property AutoAdminLogon: Boolean read GetAutoAdminLogon write SetAutoAdminLogon;
    property ForceAutoLogon: Boolean read GetForceAutoLogon write SetForceAutoLogon;
    property RegistryKey: String read GetRegistryKey;
    property RegistryRoot: HKEY read GetRegistryRoot;
    constructor Create;
    destructor Destroy; override;
  end;
  TZetaKioskoRegistry = class(TZetaClientRegistry)
  private
    { Private declarations }
    function GetEmpresa: String;
    function GetPasswordSalida: String;
    function GetNombreCompania: String;
    function GetBarraNombreBkColor: TColor;
    function GetPanelSuperiorBkColor: TColor;
    function GetPanelIntermedioBkColor: TColor;
    function GetPanelInferiorBkColor: TColor;
    function GetPanelSuperiorFontColor: TColor;
    function GetPanelIntermedioFontColor: TColor;
    function GetPanelInferiorFontColor: TColor;
    function GetVerPersonales: Boolean;
    function GetVerVacaciones: Boolean;
    function GetVerCursos: Boolean;
    function GetVerPrenomina: Boolean;
    function GetVerAsistencia: Boolean;
    function GetVerCalendario: Boolean;
    function GetVerNomina: Boolean;
    function GetVerAhorros: Boolean;
    function GetVerPrestamos: Boolean;
    function GetVerComidas: Boolean;
    function GetVerReportes: Boolean;
    function GetDefaultDatos: Integer;
    function GetInicioSecuencia: Integer;
    function GetPantallasTimeout: Integer;
    function GetRutaHuellas: String; //BIOMETRICO
    function GetHorasSincronizacion: String; //BIOMETRICO
    function GetValidaImprimir: Boolean; //BIOMETRICO
    function GetImprimeHuella: Boolean; //BIOMETRICO
    {$ifdef KIOSCO2}
    function GetCodigoKiosco: String;
    function GetIntentos: Integer;
    function GetGafeteAdministrador: String;
    function GetTipoGafete: Integer;
    function GetIdKiosco: String;
    {$endif}
    procedure SetEmpresa( Valor: String );
    procedure SetPasswordSalida( Valor: String );
    procedure SetNombreCompania( Valor: String );
    procedure SetBarraNombreBkColor( Valor: TColor );
    procedure SetPanelSuperiorBkColor( Valor: TColor );
    procedure SetPanelIntermedioBkColor( Valor: TColor );
    procedure SetPanelInferiorBkColor( Valor: TColor );
    procedure SetPanelSuperiorFontColor( Valor: TColor );
    procedure SetPanelIntermedioFontColor( Valor: TColor );
    procedure SetPanelInferiorFontColor( Valor: TColor );
    procedure SetVerPersonales( Valor: Boolean );
    procedure SetVerVacaciones( Valor: Boolean );
    procedure SetVerCursos( Valor: Boolean );
    procedure SetVerPrenomina( Valor: Boolean );
    procedure SetVerAsistencia( Valor: Boolean );
    procedure SetVerCalendario( Valor: Boolean );
    procedure SetVerNomina( Valor: Boolean );
    procedure SetVerAhorros( Valor: Boolean );
    procedure SetVerPrestamos( Valor: Boolean );
    procedure SetVerComidas( Valor: Boolean );
    procedure SetVerReportes( Valor: Boolean );
    procedure SetDefaultDatos( Valor: Integer );
    procedure SetInicioSecuencia( Valor: Integer );
    procedure SetPantallasTimeout( Valor: Integer );
    procedure SetRutaHuellas( Valor: String ); //BIOMETRICO
    procedure SetHorasSincronizacion( Valor: String ); //BIOMETRICO
    procedure SetValidaImprimir( Valor: Boolean ); //BIOMETRICO
    procedure SetImprimeHuella( Valor: Boolean ); //BIOMETRICO
    {$ifdef KIOSCO2}
    procedure SetCodigoKiosco( Valor: String );
    procedure SetIdKiosco( Valor: String );
    procedure SetIntentos( Valor: Integer );
    procedure SetGafeteAdministrador(const Value: String);
    function GetMaxDigitos: Byte;
    function GetMinDigitos: Byte;
    function GetDigitoEmpresa: String;
    function GetLetraGafete: string;
    procedure SetMaxDigitos(const Value: Byte);
    procedure SetMinDigitos(const Value: Byte);
    procedure SetDigitoEmpresa(const Value: String);
    procedure SetLetraGafete(const Value: string);
    function GetEmpresasAutorizadas: string;
    procedure SetEmpresasAutorizadas(const Value: string);
    function GetMensajeEmail: String;
    procedure SetMensajeEmail( const Value: String );
    function GetLimitarImpresionReportes: Boolean;
    procedure SetLimitarImpresionReportes( const Value: Boolean );
    function GetBotonImprimirReportes: Boolean;
    procedure SetBotonImprimirReportes( const Value: Boolean );
    function GetMostrarEncuestas: Boolean;
    procedure SetMostrarEncuestas( const Value: Boolean );
    procedure SetTipoGafete( Valor: Integer );
    {$endif}
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Empresa: String read GetEmpresa write SetEmpresa;
    property PasswordSalida: String read GetPasswordSalida write SetPasswordSalida;
    property NombreCompania: String read GetNombreCompania write SetNombreCompania;
    property BarraNombreBkColor: TColor read GetBarraNombreBkColor write SetBarraNombreBkColor;
    property PanelSuperiorBkColor: TColor read GetPanelSuperiorBkColor write SetPanelSuperiorBkColor;
    property PanelIntermedioBkColor: TColor read GetPanelIntermedioBkColor write SetPanelIntermedioBkColor;
    property PanelInferiorBkColor: TColor read GetPanelInferiorBkColor write SetPanelInferiorBkColor;
    property PanelSuperiorFontColor: TColor read GetPanelSuperiorFontColor write SetPanelSuperiorFontColor;
    property PanelIntermedioFontColor: TColor read GetPanelIntermedioFontColor write SetPanelIntermedioFontColor;
    property PanelInferiorFontColor: TColor read GetPanelInferiorFontColor write SetPanelInferiorFontColor;
    property VerPersonales: Boolean read GetVerPersonales write SetVerPersonales;
    property VerVacaciones: Boolean read GetVerVacaciones write SetVerVacaciones;
    property VerCursos: Boolean read GetVerCursos write SetVerCursos;
    property VerPrenomina: Boolean read GetVerPrenomina write SetVerPrenomina;
    property VerAsistencia: Boolean read GetVerAsistencia write SetVerAsistencia;
    property VerCalendario: Boolean read GetVerCalendario write SetVerCalendario;
    property VerNomina: Boolean read GetVerNomina write SetVerNomina;
    property VerAhorros: Boolean read GetVerAhorros write SetVerAhorros;
    property VerPrestamos: Boolean read GetVerPrestamos write SetVerPrestamos;
    property VerComidas: Boolean read GetVerComidas write SetVerComidas;
    property VerReportes: Boolean read GetVerReportes write SetVerReportes;
    property DefaultDatos: Integer read GetDefaultDatos write SetDefaultDatos;
    property InicioSecuencia: Integer read GetInicioSecuencia write SetInicioSecuencia;
    property PantallasTimeout: Integer read GetPantallasTimeout write SetPantallasTimeout;
    property RutaHuellas: String read GetRutaHuellas write SetRutaHuellas; //BIOMETRICO
    property HorasSincronizacion: String read GetHorasSincronizacion write SetHorasSincronizacion; //BIOMETRICO
    property ValidaImprimir: Boolean read GetValidaImprimir write SetValidaImprimir;//BIOMETRICO
    property ImprimeHuella: Boolean read GetImprimeHuella write SetImprimeHuella;//BIOMETRICO
    {$ifdef KIOSCO2}
    property CodigoKiosco: String read GetCodigoKiosco write SetCodigoKiosco;
    property Intentos: Integer read GetIntentos write SetIntentos;
    property GafeteAdministrador: String read GetGafeteAdministrador write SetGafeteAdministrador;
    property MinDigitos: Byte read GetMinDigitos write SetMinDigitos;
    property MaxDigitos: Byte read GetMaxDigitos write SetMaxDigitos;
    property DigitoEmpresa: String read GetDigitoEmpresa write SetDigitoEmpresa;
    property LetraGafete: string read GetLetraGafete write SetLetraGafete;
    function KioskoConfigurado: Boolean;
    property EmpresasAutorizadas: string read GetEmpresasAutorizadas write SetEmpresasAutorizadas;
    property MensajeEmail: String read GetMensajeEmail write SetMensajeEmail;
    property LimitarImpresion: Boolean read GetLimitarImpresionReportes write SetLimitarImpresionReportes;
    property SoloBotonImprimir: Boolean read GetBotonImprimirReportes write SetBotonImprimirReportes;
    property MostrarEncuestas: Boolean read GetMostrarEncuestas write SetMostrarEncuestas;
    property TipoGafete: Integer read GetTipoGafete write SetTipoGafete;
    property IdKiosco: string read GetIdKiosco write SetIdKiosco;
    {$endif}
    function GetLetraCredencial(const sEmpresa: String): String;
    function ClienteConfigurado: Boolean;
    function GetVirtualStore: Boolean;
    procedure ResetLetraCredencial;
    procedure SetLetraCredencial(const sEmpresa, sLetra: String);
    procedure NombreCompaniaFontLoad( oFont: TFont );
    procedure NombreCompaniaFontUnload( oFont: TFont );
  end;

var
   KioskoRegistry: TZetaKioskoRegistry;

{$ifndef KIOSCO2}
function EditKioskoRegistry: Boolean;
{$ENDIF}
procedure InitKioskoRegistry;
procedure ClearKioskoRegistry;

implementation

uses
    {$ifndef KIOSCO2}
     FKioskoRegistryEditor,
    {$endif} 
     ZetaCommonClasses,
     ZetaCommonTools;


const
     {$ifdef MISDATOS}
     K_KIOSKO = 'MisDatos';
     K_EMPRESAS_KIOSKO = 'MisDatos\Empresas';
     {$else}
     K_KIOSKO = 'Kiosko';
     K_EMPRESAS_KIOSKO = 'Kiosko\Empresas';
     {$endif}
     K_EMPRESA = 'Empresa';
     PASSWORD_SALIDA = 'PassWordSalida';
     NOMBRE_COMPANIA = 'Compania';
     NOMBRE_COMPANIA_FONT_COLOR = 'NombreCompaniaFontColor';
     NOMBRE_COMPANIA_FONT_HEIGHT = 'NombreCompaniaFontHeight';
     NOMBRE_COMPANIA_FONT_NAME = 'NombreCompaniaFontName';
     NOMBRE_COMPANIA_FONT_STYLE = 'NombreCompaniaFontStyle';
     BARRA_NOMBRE_BKCOLOR = 'BarraNombreBkColor';
     PANEL_SUPERIOR_BKCOLOR = 'PanelSuperiorBkColor';
     PANEL_INTERMEDIO_BKCOLOR = 'PanelIntermedioBkColor';
     PANEL_INFERIOR_BKCOLOR = 'PanelInferiorBkColor';
     PANEL_SUPERIOR_FONTCOLOR = 'PanelSuperiorFontColor';
     PANEL_INTERMEDIO_FONTCOLOR = 'PanelIntermedioFontColor';
     PANEL_INFERIOR_FONTCOLOR = 'PanelInferiorFontColor';
     VER_PERSONALES = 'VerPersonales';
     VER_VACACIONES = 'VerVacaciones';
     VER_CURSOS = 'VerCursos';
     VER_PRENOMINA = 'VerPrenomina';
     VER_ASISTENCIA = 'VerAsistencia';
     VER_CALENDARIO = 'VerCalendario';
     VER_NOMINA = 'VerNomina';
     VER_AHORROS = 'VerAhorros';
     VER_PRESTAMOS = 'VerPrestamos';
     VER_COMIDAS = 'VerComidas';
     VER_REPORTES = 'VerReportes';
     DEFAULT_DATOS = 'DefaultDatos';
     INICIO_SECUENCIA = 'InicioSecuencia';
     PANTALLAS_TIMEOUT = 'PantallasTimeOut';
     {$ifdef MISDATOS}
     K_CODIGO_KIOSCO = 'CodigoMisDatos';
     K_ID_KIOSCO = 'IdMisDatos';
     {$else}
     K_CODIGO_KIOSCO = 'CodigoKiosco';
     K_ID_KIOSCO = 'IdKiosco';
     {$endif}
     K_NUMERO_INTENTOS = 'Intentos';
     K_GAFETE_ADMIN = 'GafeteAdministrador';
     K_LETRA_GAFETE = 'LetraGafete';
     K_DIGITO_EMPRESA = 'DigitoEmpresa';
     K_EMPRESAS_AUTORIZADAS = 'DigitosEmpresasAutorizadas';
     K_MSJ_EMAIL = 'MsjReportesEmail';
     K_LIM_IMPRESIONES = 'LimitarImpresionReportes';
     K_BOTON_IMPRIMIR_REPORTES = 'SoloBotonImprimirReportes';
     K_MOSTRAR_ENCUESTAS ='MostrarEncuestas';
     { LogonRegistry }
     K_DEFAULT_DOMAIN = 'DefaultDomainName';
     K_DEFAULT_USERNAME = 'DefaultUserName';
     K_DEFAULT_PASSWORD = 'DefaultPassword';
     K_AUTO_ADMIN_LOGON = 'AutoAdminLogon';
     K_FORCE_AUTO_LOGON = 'ForceAutoLogon';
     K_TIPO_GAFETE = 'TipoGafete';
     K_HUELLAS_RUTA = 'BiometricoRuta'; //BIOMETRICO
     K_HUELLAS_HORAS = 'BiometricoHoras'; //BIOMETRICO
     K_HUELLAS_VALIDA = 'BiometricoValidaHuella'; //BIOMETRICO
     K_HUELLAS_Imprime = 'BiometricoImprimeHuella'; //BIOMETRICO

{ ******* Inicializaci�n / Destrucci�n ********* }

procedure InitKioskoRegistry;
begin
     KioskoRegistry := TZetaKioskoRegistry.Create;
     ZetaRegistryCliente.ClientRegistry := KioskoRegistry;
end;

procedure ClearKioskoRegistry;
begin
     ZetaRegistryCliente.ClientRegistry := nil;
     FreeAndNil( KioskoRegistry );
end;

{$Ifndef KIOSCO2}
function EditKioskoRegistry: Boolean;
begin
     KioskoRegistryEditor := TKioskoRegistryEditor.Create( Application );
     try
        with KioskoRegistryEditor do
        begin
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
     finally
            FreeAndNil( KioskoRegistryEditor );
     end;
end;
{$endif}

{ ******** TLogonRegistry ************ }

constructor TLogonRegistry.Create;
const
     LOGON_VALUES_KEY = 'SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon';
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          LazyWrite := False;
          FCanWrite := OpenKey( LOGON_VALUES_KEY, True );
          if not FCanWrite then
             OpenKeyReadOnly( LOGON_VALUES_KEY );
     end;
end;

destructor TLogonRegistry.Destroy;
begin
     FRegistry.Free;
     inherited Destroy;
end;

function TLogonRegistry.GetRegistryKey: String;
begin
     Result := FRegistry.CurrentPath;
end;

function TLogonRegistry.GetRegistryRoot: HKEY;
begin
     Result := FRegistry.RootKey;
end;

function TLogonRegistry.GetAutoAdminLogon: Boolean;
begin
     Result := FRegistry.ReadBool( '', K_AUTO_ADMIN_LOGON, False );
end;

function TLogonRegistry.GetDefaultDomainName: String;
begin
     Result := FRegistry.ReadString( '', K_DEFAULT_DOMAIN, '' );
end;

function TLogonRegistry.GetDefaultPassword: String;
begin
     Result := FRegistry.ReadString( '', K_DEFAULT_PASSWORD, '' );
end;

function TLogonRegistry.GetDefaultUserName: String;
begin
     Result := FRegistry.ReadString( '', K_DEFAULT_USERNAME, '' );
end;

function TLogonRegistry.GetForceAutoLogon: Boolean;
begin
     Result := FRegistry.ReadBool( '', K_FORCE_AUTO_LOGON, False );
end;

procedure TLogonRegistry.SetAutoAdminLogon(const Value: Boolean);
begin
     FRegistry.WriteBool( '', K_AUTO_ADMIN_LOGON, Value );
end;

procedure TLogonRegistry.SetDefaultDomainName(const Value: String);
begin
     FRegistry.WriteString( '', K_DEFAULT_DOMAIN, Value );
end;

procedure TLogonRegistry.SetDefaultPassword(const Value: String);
begin
     FRegistry.WriteString( '', K_DEFAULT_PASSWORD, Value );
end;

procedure TLogonRegistry.SetDefaultUserName(const Value: String);
begin
     FRegistry.WriteString( '', K_DEFAULT_USERNAME, Value );
end;

procedure TLogonRegistry.SetForceAutoLogon(const Value: Boolean);
begin
     FRegistry.WriteBool( '', K_FORCE_AUTO_LOGON, Value );
end;

{ ************ TZetaKioskoRegistry *********** }

function TZetaKioskoRegistry.GetEmpresa: String;
begin
     Result := Registry.ReadString( K_KIOSKO, K_EMPRESA, '' );
end;

function TZetaKioskoRegistry.GetPasswordSalida: String;
begin
     Result := Registry.ReadString( K_KIOSKO, PASSWORD_SALIDA, '' );
end;

function TZetaKioskoRegistry.GetNombreCompania: String;
begin
     Result := Registry.ReadString( K_KIOSKO, NOMBRE_COMPANIA, 'Nombre de Compa�ia' );
end;

function TZetaKioskoRegistry.GetBarraNombreBkColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, BARRA_NOMBRE_BKCOLOR, clWhite );
end;

function TZetaKioskoRegistry.GetPanelSuperiorBkColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANEL_SUPERIOR_BKCOLOR, clPurple );
end;

function TZetaKioskoRegistry.GetPanelIntermedioBkColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANEL_INTERMEDIO_BKCOLOR, clPurple );
end;

function TZetaKioskoRegistry.GetPanelInferiorBkColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANEL_INFERIOR_BKCOLOR, clPurple );
end;

function TZetaKioskoRegistry.GetPanelSuperiorFontColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANEL_SUPERIOR_FONTCOLOR, clWindowText );
end;

function TZetaKioskoRegistry.GetPanelIntermedioFontColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANEL_INTERMEDIO_FONTCOLOR, clWindowText );
end;

function TZetaKioskoRegistry.GetPanelInferiorFontColor: TColor;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANEL_INFERIOR_FONTCOLOR, clWindowText );
end;

function TZetaKioskoRegistry.GetVerPersonales: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_PERSONALES, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerVacaciones: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_VACACIONES, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerCursos: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_CURSOS, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerPrenomina: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_PRENOMINA, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerAsistencia: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_ASISTENCIA, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerCalendario: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_CALENDARIO, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerNomina: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_NOMINA, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerAhorros: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_AHORROS, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerPrestamos: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_PRESTAMOS, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerComidas: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_COMIDAS, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetVerReportes: Boolean;
begin
     Result := ZetaCommonTools.zStrToBool( Registry.ReadString( K_KIOSKO, VER_REPORTES, K_GLOBAL_SI ) );
end;

function TZetaKioskoRegistry.GetDefaultDatos: Integer;
begin
     Result := Registry.ReadInteger( K_KIOSKO, DEFAULT_DATOS, 0 );
end;

function TZetaKioskoRegistry.GetInicioSecuencia: Integer;
begin
     Result := Registry.ReadInteger( K_KIOSKO, INICIO_SECUENCIA, 1 );
end;

function TZetaKioskoRegistry.GetPantallasTimeout: Integer ;
begin
     Result := Registry.ReadInteger( K_KIOSKO, PANTALLAS_TIMEOUT, 30 ); { 30 segundos }
end;

{$ifdef KIOSCO2}
function TZetaKioskoRegistry.GetCodigoKiosco: String;
begin
     {$ifdef MISDATOS}
     Result := K_MIS_DATOS;    // Siempre se pide el show: K_MIS_DATOS
     {$else}
     Result := Registry.ReadString( K_KIOSKO, K_CODIGO_KIOSCO, '' );
     {$endif}
end;

function TZetaKioskoRegistry.GetIdKiosco: String;
begin
     Result := Registry.ReadString( K_KIOSKO, K_ID_KIOSCO, '' );
end;

function TZetaKioskoRegistry.GetIntentos: Integer;
begin
     Result := Registry.ReadInteger( K_KIOSKO, K_NUMERO_INTENTOS, 3 );
end;

function TZetaKioskoRegistry.GetGafeteAdministrador: String;
begin
     Result := Registry.ReadString( K_KIOSKO, K_GAFETE_ADMIN, 'ADMIN' );
end;

function TZetaKioskoRegistry.GetTipoGafete: Integer;
begin
     Result := Registry.ReadInteger( K_KIOSKO, K_TIPO_GAFETE, 0 );
end;
{$endif}

procedure TZetaKioskoRegistry.SetEmpresa(Valor: String);
begin
     Registry.WriteString( K_KIOSKO, K_EMPRESA, Valor );
end;

procedure TZetaKioskoRegistry.SetPasswordSalida( Valor: String );
begin
     Registry.WriteString( K_KIOSKO, PASSWORD_SALIDA, Valor );
end;

procedure TZetaKioskoRegistry.SetNombreCompania( Valor: String );
begin
     Registry.WriteString( K_KIOSKO, NOMBRE_COMPANIA, Valor );
end;

procedure TZetaKioskoRegistry.SetBarraNombreBkColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, BARRA_NOMBRE_BKCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetPanelSuperiorBkColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, PANEL_SUPERIOR_BKCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetPanelIntermedioBkColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, PANEL_INTERMEDIO_BKCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetPanelInferiorBkColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, PANEL_INFERIOR_BKCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetPanelSuperiorFontColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, PANEL_SUPERIOR_FONTCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetPanelIntermedioFontColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, PANEL_INTERMEDIO_FONTCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetPanelInferiorFontColor( Valor: TColor );
begin
     Registry.WriteInteger( K_KIOSKO, PANEL_INFERIOR_FONTCOLOR, Valor );
end;

procedure TZetaKioskoRegistry.SetVerPersonales( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_PERSONALES, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerVacaciones( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_VACACIONES, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerCursos( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_CURSOS, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerPrenomina( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_PRENOMINA, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerAsistencia( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_ASISTENCIA, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerCalendario( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_CALENDARIO, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerNomina( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_NOMINA, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerAhorros( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_AHORROS, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerPrestamos( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_PRESTAMOS, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerComidas( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_COMIDAS, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetVerReportes( Valor: Boolean );
begin
     Registry.WriteString( K_KIOSKO, VER_REPORTES, ZetaCommonTools.zBoolToStr( Valor ) );
end;

procedure TZetaKioskoRegistry.SetDefaultDatos( Valor: Integer );
begin
     Registry.WriteInteger( K_KIOSKO, DEFAULT_DATOS, Valor );
end;

procedure TZetaKioskoRegistry.SetInicioSecuencia( Valor: Integer );
begin
     Registry.WriteInteger( K_KIOSKO, INICIO_SECUENCIA, Valor );
end;

procedure TZetaKioskoRegistry.SetPantallasTimeout( Valor: Integer );
begin
     Registry.WriteInteger( K_KIOSKO, PANTALLAS_TIMEOUT, Valor );
end;

{$ifdef KIOSCO2}
procedure TZetaKioskoRegistry.SetCodigoKiosco( Valor: String );
begin
     Registry.WriteString( K_KIOSKO, K_CODIGO_KIOSCO, Valor );
end;

procedure TZetaKioskoRegistry.SetIdKiosco( Valor: String );
begin
     Registry.WriteString( K_KIOSKO, K_ID_KIOSCO, Valor );
end;

procedure TZetaKioskoRegistry.SetIntentos( Valor: Integer );
begin
     Registry.WriteInteger( K_KIOSKO, K_NUMERO_INTENTOS, Valor );
end;

procedure TZetaKioskoRegistry.SetGafeteAdministrador(const Value: String);
begin
     Registry.WriteString( K_KIOSKO, K_GAFETE_ADMIN, Value );
end;

function TZetaKioskoRegistry.KioskoConfigurado: Boolean;
begin
     Result := ClienteConfigurado and ZetaCommonTools.StrLleno( CodigoKiosco ) {$ifdef MISDATOS}and ZetaCommonTools.strLleno( IdKiosco ){$endif};
end;

procedure TZetaKioskoRegistry.SetTipoGafete( Valor: Integer );
begin
     Registry.WriteInteger( K_KIOSKO, K_TIPO_GAFETE, Valor );
end;
{$endif}

procedure TZetaKioskoRegistry.NombreCompaniaFontLoad(oFont: TFont);

function GetStyle( const sStyle: String ): TFontStyles;
var
   eStyle: TFontStyle;
begin
     Result := [];
     for eStyle := Low( TFontStyle ) to High( TFontStyle ) do
     begin
          if ( Pos( IntToStr( Ord( eStyle ) ), sStyle ) > 0 ) then
          begin
               Result := Result + [ eStyle ];
          end;
     end;
end;

begin
     with oFont do
     begin
          Name := Registry.ReadString( K_KIOSKO, NOMBRE_COMPANIA_FONT_NAME, 'Arial' );
          Color := Registry.ReadInteger( K_KIOSKO, NOMBRE_COMPANIA_FONT_COLOR, clRed );
          Height := Registry.ReadInteger( K_KIOSKO, NOMBRE_COMPANIA_FONT_HEIGHT, 14 );
          Style := GetStyle( Registry.ReadString( K_KIOSKO, NOMBRE_COMPANIA_FONT_STYLE, '' ) );
     end;
end;

procedure TZetaKioskoRegistry.NombreCompaniaFontUnload(oFont: TFont);

function SetStyle( const aStyle: TFontStyles ): String;
var
   eStyle: TFontStyle;
begin
     Result := '';
     for eStyle := Low( TFontStyle ) to High( TFontStyle ) do
     begin
          if ( eStyle in aStyle ) then
             Result := Result + IntToStr( Ord( eStyle ) );
     end;
end;

begin
     with oFont do
     begin
          Registry.WriteString( K_KIOSKO, NOMBRE_COMPANIA_FONT_NAME, Name );
          Registry.WriteInteger( K_KIOSKO, NOMBRE_COMPANIA_FONT_COLOR, Color );
          Registry.WriteInteger( K_KIOSKO, NOMBRE_COMPANIA_FONT_HEIGHT, Height );
          Registry.WriteString( K_KIOSKO, NOMBRE_COMPANIA_FONT_STYLE, SetStyle( Style ) );
     end;
end;

function TZetaKioskoRegistry.GetLetraCredencial( const sEmpresa: String ): String;
begin
     Result := Registry.ReadString( K_EMPRESAS_KIOSKO, sEmpresa, '' );
end;

procedure TZetaKioskoRegistry.ResetLetraCredencial;
begin
     Registry.EraseSection( K_EMPRESAS_KIOSKO );
end;

procedure TZetaKioskoRegistry.SetLetraCredencial( const sEmpresa, sLetra: String );
begin
     if ZetaCommonTools.StrLleno( sEmpresa ) and ZetaCommonTools.StrLleno( sLetra ) then
     begin
          Registry.WriteString( K_EMPRESAS_KIOSKO, sEmpresa, sLetra );
     end;
end;

function TZetaKioskoRegistry.ClienteConfigurado: Boolean;
begin
     Result := ZetaCommonTools.StrLleno( ComputerName );
end;

function TZetaKioskoRegistry.GetMaxDigitos: Byte;
begin
     //Se tiene que leer del Registry
     Result := 4;
end;

function TZetaKioskoRegistry.GetMinDigitos: Byte;
begin
     //Se tiene que leer del Registry
     Result := 4;
end;

procedure TZetaKioskoRegistry.SetMaxDigitos(const Value: Byte);
begin
     //Se tiene que escribir en el Registry
end;

procedure TZetaKioskoRegistry.SetMinDigitos(const Value: Byte);
begin
     //Se tiene que escribir en el Registry
end;

procedure TZetaKioskoRegistry.SetDigitoEmpresa(const Value: String);
begin
     Registry.WriteString( K_KIOSKO, K_DIGITO_EMPRESA, Value );
end;

procedure TZetaKioskoRegistry.SetLetraGafete(const Value: string);
begin
     Registry.WriteString( K_KIOSKO, K_LETRA_GAFETE, Value );
end;

procedure TZetaKioskoRegistry.SetEmpresasAutorizadas(const Value: string);
begin
     Registry.WriteString( K_KIOSKO, K_EMPRESAS_AUTORIZADAS, Value );
end;

function TZetaKioskoRegistry.GetDigitoEmpresa: String;
begin
     Result := Registry.ReadString( K_KIOSKO, K_DIGITO_EMPRESA, '' );
end;

function TZetaKioskoRegistry.GetLetraGafete: string;
begin
     Result := Registry.ReadString( K_KIOSKO, K_LETRA_GAFETE, '' );
end;

function TZetaKioskoRegistry.GetEmpresasAutorizadas: string;
begin
     Result := Registry.ReadString( K_KIOSKO, K_EMPRESAS_AUTORIZADAS, '' );
end;


function TZetaKioskoRegistry.GetMensajeEmail: String;
begin
     Result := Registry.ReadString( K_KIOSKO, K_MSJ_EMAIL, 'Favor de pasar al departamento correspondiente para su impresi�n' );
end;

procedure TZetaKioskoRegistry.SetMensajeEmail(const Value: String);
begin
     Registry.WriteString( K_KIOSKO, K_MSJ_EMAIL, Value );
end;

function TZetaKioskoRegistry.GetLimitarImpresionReportes: Boolean;
begin
     Result := Registry.ReadBool( K_KIOSKO, K_LIM_IMPRESIONES, False );
end;

procedure TZetaKioskoRegistry.SetLimitarImpresionReportes(const Value: Boolean);
begin
     Registry.WriteBool( K_KIOSKO, K_LIM_IMPRESIONES, Value );
end;

function TZetaKioskoRegistry.GetBotonImprimirReportes: Boolean;
begin
     Result := Registry.ReadBool( K_KIOSKO, K_BOTON_IMPRIMIR_REPORTES, False );
end;

procedure TZetaKioskoRegistry.SetBotonImprimirReportes(const Value: Boolean);
begin
     Registry.WriteBool( K_KIOSKO, K_BOTON_IMPRIMIR_REPORTES, Value );
end;

function TZetaKioskoRegistry.GetMostrarEncuestas: Boolean;
begin
     Result := Registry.ReadBool( K_KIOSKO, K_MOSTRAR_ENCUESTAS, False );
end;

procedure TZetaKioskoRegistry.SetMostrarEncuestas(const Value: Boolean);
begin
      Registry.WriteBool( K_KIOSKO, K_MOSTRAR_ENCUESTAS, Value );
end;

//Biometrico
function TZetaKioskoRegistry.GetRutaHuellas; //BIOMETRICO
begin
      Result := Registry.ReadString( K_KIOSKO, K_HUELLAS_RUTA, '' );
end;

//Biometrico
function TZetaKioskoRegistry.GetHorasSincronizacion; //BIOMETRICO
begin
      Result := Registry.ReadString( K_KIOSKO, K_HUELLAS_HORAS, '00:00' );
end;

//Biometrico
procedure TZetaKioskoRegistry.SetRutaHuellas( Valor: String ); //BIOMETRICO
begin
      Registry.WriteString( K_KIOSKO, K_HUELLAS_RUTA, Valor );
end;

//Biometrico
procedure TZetaKioskoRegistry.SetHorasSincronizacion( Valor: String ); //BIOMETRICO
begin
      Registry.WriteString( K_KIOSKO, K_HUELLAS_HORAS, Valor );
end;

//Biometrico
procedure TZetaKioskoRegistry.SetValidaImprimir( Valor: Boolean );
begin
     Registry.WriteBool( K_KIOSKO, K_HUELLAS_VALIDA, Valor );
end;

//Biometrico
procedure TZetaKioskoRegistry.SetImprimeHuella( Valor: Boolean );
begin
     Registry.WriteBool( K_KIOSKO, K_HUELLAS_IMPRIME, Valor );
end;

//Biometrico
function TZetaKioskoRegistry.GetValidaImprimir; //BIOMETRICO
begin
      Result := Registry.ReadBool( K_KIOSKO, K_HUELLAS_VALIDA, False );
end;

//Biometrico
function TZetaKioskoRegistry.GetImprimeHuella; //BIOMETRICO
begin
      Result := Registry.ReadBool( K_KIOSKO, K_HUELLAS_IMPRIME, False );
end;

function TZetaKioskoRegistry.GetVirtualStore: Boolean;
const
     CLIENT_REGISTRY_PATH_VS_32 = 'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Grupo Tress\TressWin\Client';
     CLIENT_REGISTRY_PATH_VS_64 = 'Software\Classes\VirtualStore\MACHINE\SOFTWARE\Wow6432Node\Grupo Tress\TressWin\Client';
     SERVER_COMPUTER_NAME = 'Server Computer Name';
var
   RegistryVS: TRegIniFile;
   sComputerName: String;
begin
     Result := FALSE;
     RegistryVS := TRegIniFile.Create;
     try
        with RegistryVS do
        begin
             RootKey := HKEY_CURRENT_USER;
             if ( TOSVersion.Architecture = arIntelX86 ) then
                Result := OpenKeyReadOnly( CLIENT_REGISTRY_PATH_VS_32 )
             else
                 Result := OpenKeyReadOnly( CLIENT_REGISTRY_PATH_VS_64 );
             if Result then
             begin
                  sComputerName := ReadString( '', SERVER_COMPUTER_NAME, '' );
                  Result := strLleno( sComputerName );
                  if Result then
                  begin
                       ComputerName := sComputerName;
                       self.CodigoKiosco := ReadString( K_KIOSKO, K_CODIGO_KIOSCO, '' );
                       self.IdKiosco := ReadString( K_KIOSKO, K_ID_KIOSCO, '' );
                       self.Intentos := ReadInteger( K_KIOSKO, K_NUMERO_INTENTOS, 3 );
                       self.GafeteAdministrador := ReadString( K_KIOSKO, K_GAFETE_ADMIN, 'ADMIN' );
                       self.DigitoEmpresa := ReadString( K_KIOSKO, K_DIGITO_EMPRESA, '' );
                       self.LetraGafete := ReadString( K_KIOSKO, K_LETRA_GAFETE, '' );
                       self.EmpresasAutorizadas := ReadString( K_KIOSKO, K_EMPRESAS_AUTORIZADAS, '' );
                       self.MensajeEmail := ReadString( K_KIOSKO, K_MSJ_EMAIL, 'Favor de pasar al departamento correspondiente para su impresi�n' );
                       self.LimitarImpresion := ReadBool( K_KIOSKO, K_LIM_IMPRESIONES, False );
                       self.SoloBotonImprimir := ReadBool( K_KIOSKO, K_BOTON_IMPRIMIR_REPORTES, False );
                       self.MostrarEncuestas := ReadBool( K_KIOSKO, K_MOSTRAR_ENCUESTAS, False );
                       self.TipoGafete := ReadInteger( K_KIOSKO, K_TIPO_GAFETE, 0 );
                       self.PasswordSalida := ReadString( K_KIOSKO, PASSWORD_SALIDA, '' );
                       self.LetraGafete := ReadString( K_KIOSKO, K_LETRA_GAFETE, '' );
                       self.RutaHuellas := ReadString( K_KIOSKO, K_HUELLAS_RUTA, '' );
                       self.HorasSincronizacion := ReadString( K_KIOSKO, K_HUELLAS_HORAS, '00:00' );
                       self.ValidaImprimir := ReadBool( K_KIOSKO, K_HUELLAS_VALIDA, False );
                       self.ImprimeHuella := ReadBool( K_KIOSKO, K_HUELLAS_IMPRIME, False );
                  end;
             end;
        end;
     finally
            RegistryVS.Free;
     end;
end;

end.
