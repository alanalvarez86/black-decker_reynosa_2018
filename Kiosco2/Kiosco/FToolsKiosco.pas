unit FToolsKiosco;

interface

uses Sysutils, printers, Winspool;

function ImpresoraDisponibe( const iImpresora: Integer; var sMensaje: String ): Boolean;

implementation

uses ZetaCommonClasses;

// (JB) Funcion que revisa los jobs de la impresora
//      Funcion viene de vista de Cotemar.
function ImpresoraDisponibe( const iImpresora: Integer; var sMensaje: String ): Boolean;
type
     TJobs  = array [0..1000] of JOB_INFO_1;
     PJobs = ^TJobs;

const
     K_ERR_INF_IMPRESORA = 'Error al obtener informaci�n de la Impresora';
     K_ERR_IMP_OCUPADA   = 'No es posible Imprimir el Documento: <Impresora en uso> '+CR_LF+'Favor de intentarlo m�s tarde';

var
   hPrinter: THandle;
   bytesNeeded, numJobs: Cardinal;
   pJ: PJobs;
   iPrinter : integer;

   function GetCurrentPrinterHandle: THandle;
   var
        Device, Driver, Port: array[0..255] of Char;
        hDeviceMode: THandle;
   begin
        Printer.GetPrinter(Device, Driver, Port, hDeviceMode);
        if not OpenPrinter(@Device, Result, nil) then
           sMensaje := K_ERR_INF_IMPRESORA; //No se pudo abrir la info de la impresora
   end;

begin
     Result:= FALSE;

     iPrinter := Printer.PrinterIndex;
     try
        if ( iImpresora >= 0 ) then   // No es la default
           Printer.PrinterIndex := iImpresora;

        hPrinter := GetCurrentPrinterHandle;
        try
           EnumJobs(hPrinter, 0, 1000, 1, nil, 0, bytesNeeded, numJobs);
           pJ := AllocMem(bytesNeeded);
           if EnumJobs(hPrinter, 0, 1000, 1, pJ, bytesNeeded,bytesNeeded, numJobs) then
           begin
                Result:= ( numJobs = 0 );
                if not Result then
                   sMensaje := K_ERR_IMP_OCUPADA;                             // (JB) Hay documentos pendientes por imprimir
           end
           else
               sMensaje := K_ERR_INF_IMPRESORA
        finally
             ClosePrinter(hPrinter);
        end;
        if Result then
           sMensaje := VACIO;
     finally
            Printer.PrinterIndex := iPrinter;
     end;
end;

end.
