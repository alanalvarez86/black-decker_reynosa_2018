inherited ConnectionProblem: TConnectionProblem
  Left = 410
  Top = 339
  Caption = 'Error En Conexi�n Al Servidor Remoto'
  ClientHeight = 142
  ClientWidth = 335
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Imagen: TImage [0]
    Left = 0
    Top = 0
    Width = 81
    Height = 106
    Align = alLeft
    Center = True
  end
  inherited PanelBotones: TPanel
    Top = 106
    Width = 335
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 103
      Top = 7
      Width = 88
      Caption = '&Reintentar'
      Kind = bkRetry
    end
    inherited Cancelar: TBitBtn
      Left = 244
      Top = 7
      Width = 88
      Hint = 'Salir del Programa'
    end
    object Configurar: TBitBtn
      Left = 10
      Top = 7
      Width = 88
      Height = 25
      Hint = 'Configurar Conexi�n Al Servidor Remoto'
      Anchors = [akTop, akRight]
      Caption = '&Configurar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = ConfigurarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555550FF0559
        1950555FF75F7557F7F757000FF055591903557775F75557F77570FFFF055559
        1933575FF57F5557F7FF0F00FF05555919337F775F7F5557F7F700550F055559
        193577557F7F55F7577F07550F0555999995755575755F7FFF7F5570F0755011
        11155557F755F777777555000755033305555577755F75F77F55555555503335
        0555555FF5F75F757F5555005503335505555577FF75F7557F55505050333555
        05555757F75F75557F5505000333555505557F777FF755557F55000000355557
        07557777777F55557F5555000005555707555577777FF5557F55553000075557
        0755557F7777FFF5755555335000005555555577577777555555}
      NumGlyphs = 2
    end
  end
  object Info: TMemo
    Left = 81
    Top = 0
    Width = 254
    Height = 106
    Align = alClient
    Alignment = taCenter
    BorderStyle = bsNone
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 1
  end
end
