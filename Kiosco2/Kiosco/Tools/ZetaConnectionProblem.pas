unit ZetaConnectionProblem;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Buttons,
     ZBaseDlgModal;

type
  TConnectionProblem = class(TZetaDlgModal)
    Imagen: TImage;
    Info: TMemo;
    Configurar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetError( Value: Exception );
  end;

var
  ConnectionProblem: TConnectionProblem;

function ConnectionProblemDialog( Error: Exception ): TModalResult;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
{$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor,
{$else}
     ZetaRegistryCliente,
{$endif}
     ZetaCommonClasses;

{$R *.DFM}

function ConnectionProblemDialog( Error: Exception ): TModalResult;
begin
     with TConnectionProblem.Create( Application ) do
     begin
          try
             SetError( Error );
             Chicharra;
             {$ifndef DOS_CAPAS}
             Configurar.Enabled := ZetaRegistryCliente.ClientRegistry.CanWrite;
             {$endif}
             ShowModal;
             Result := ModalResult;
          finally
                 Free;
          end;
     end;
end;

{ *********** TConnectionProblem ********* }

procedure TConnectionProblem.FormCreate(Sender: TObject);
begin
     inherited;
     with Imagen do
     begin
          Picture.Icon.Handle := LoadIcon( 0, IDI_HAND );
     end;
end;

procedure TConnectionProblem.FormShow(Sender: TObject);
begin
     inherited;
     Cancelar.Hint := Format( 'Salir de %s', [ Application.Title ] );
end;

procedure TConnectionProblem.SetError(Value: Exception);
begin
     Info.Lines.Text := Value.Message;
end;

procedure TConnectionProblem.ConfigurarClick(Sender: TObject);
begin
     inherited;
{$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor.EspecificaComparte;
{$else}
     ClientRegistry.BuscaServidor;
{$endif}
end;

end.
