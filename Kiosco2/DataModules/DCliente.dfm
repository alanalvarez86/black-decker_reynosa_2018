inherited dmCliente: TdmCliente
  OldCreateOrder = True
  Left = 285
  Top = 161
  object cdsEmpleadoLookUp: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    LookupName = 'Empleados'
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    OnLookupKey = cdsEmpleadoLookUpLookupKey
    OnLookupSearch = cdsEmpleadoLookUpLookupSearch
    Left = 144
    Top = 8
  end
  object cdsEmpresasKiosco: TZetaLookupDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpresasKioscoAlAdquirirDatos
    LookupDescriptionField = 'CM_NOMBRE'
    LookupKeyField = 'CM_CODIGO'
    Left = 136
    Top = 72
  end
end
