unit DKiosco;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBClient, Db, ComObj,fcCommon, ExtDlgs,
     {$ifndef VER130}Variants,{$endif}
     {$ifdef DOS_CAPAS}
     DServerKiosco,
     DServerReportes,
    {$else}
     Kiosco_TLB,
     ReporteadorDD_TLB,
    {$endif}
     FCamposMgr,
     ZetaPortalClasses,
     ZetaClientDataSet,
     ZetaCommonClasses,
     ZetaCommonLists;

const
     K_PRIMERA_PAGINA = 1;
     K_PRIMERA_COLUMNA = 1;
     K_TODOS_KEY = '000000';
     K_TODOS_TXT = 'Todos';
type
  eTipoReportal = ( eTipoReporte, eTipoLiga );
  //eSubeBaja = ( eSubir, eBajar );

  TdmKiosco = class(TDataModule)
    cdsContenidos: TZetaClientDataSet;
    cdsCarruseles: TZetaLookupDataSet;
    cdsMarcosInformacion: TZetaLookupDataSet;
    cdsEstilosBotones: TZetaLookupDataSet;
    cdsCarruselesInfo: TZetaClientDataSet;
    cdsBotones: TZetaClientDataSet;
    cdsMarcosLookup: TZetaLookupDataSet;
    cdsSuscripReportes: TZetaLookupDataSet;
    cdsPantallas: TZetaClientDataSet;
    cdsSecuencias: TZetaClientDataSet;
    cdsBitacora: TZetaClientDataSet;
    cdsAccesoBoton: TZetaClientDataSet;
    cdsConfiUsuarios: TZetaClientDataSet;
    cdsEncuestas: TZetaLookupDataSet;
    cdsOpcionesEncuesta: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsTablasAlAdquirirDatos(Sender: TObject);
    procedure cdsTablasAlModificar(Sender: TObject);
    procedure cdsTablasBeforePost(DataSet: TDataSet);
    procedure cdsTablasAlEnviarDatos(Sender: TObject);
    procedure cdsTablasAfterDelete(DataSet: TDataSet);
    procedure cdsCarruselesAlCrearCampos(Sender: TObject);
    procedure cdsEstilosBotonesNewRecord(DataSet: TDataSet);
    procedure cdsCarruselesNewRecord(DataSet: TDataSet);
    procedure cdsCarruselesInfoAfterDelete(DataSet: TDataSet);
    procedure cdsCarruselesInfoAfterEdit(DataSet: TDataSet);
    procedure cdsCarruselesInfoAlAdquirirDatos(Sender: TObject);
    procedure cdsCarruselesAlEnviarDatos(Sender: TObject);
    procedure cdsCarruselesBeforePost(DataSet: TDataSet);
    procedure cdsCarruselesInfoNewRecord(DataSet: TDataSet);
    procedure cdsCarruselesInfoBeforePost(DataSet: TDataSet);
    procedure cdsMarcosInformacionAlCrearCampos(Sender: TObject);
    procedure cdsMarcosInformacionAlEnviarDatos(Sender: TObject);
    procedure cdsMarcosInformacionBeforePost(DataSet: TDataSet);
    procedure cdsMarcosInformacionNewRecord(DataSet: TDataSet);
    procedure cdsBotonesAfterDelete(DataSet: TDataSet);
    procedure cdsBotonesAfterEdit(DataSet: TDataSet);
    procedure cdsBotonesAlAdquirirDatos(Sender: TObject);
    procedure cdsBotonesNewRecord(DataSet: TDataSet);
    procedure cdsBotonesAlCrearCampos(Sender: TObject);
    procedure cdsBotonesBeforePost(DataSet: TDataSet);
    procedure cdsEstilosBotonesAlCrearCampos(Sender: TObject);
    procedure cdsEstilosBotonesAlModificar(Sender: TObject);
    procedure cdsMarcosLookupGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripReportesAlCrearCampos(Sender: TObject);
    procedure cdsSuscripReportesLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsBitacoraAfterOpen(DataSet: TDataSet);
    procedure cdsBitacoraAlModificar(Sender: TObject);
   // procedure cdsAccesoBotonAlEnviarDatos(Sender: TObject);
   // procedure cdsAccesoBotonAlAdquirirDatos(Sender: TObject);
    procedure cdsMarcosInformacionAfterCancel(DataSet: TDataSet);
    procedure cdsConfiUsuariosAlAdquirirDatos(Sender: TObject);
    procedure cdsConfiUsuariosAlEnviarDatos(Sender: TObject);
    procedure cdsConfiUsuariosAlAgregar(Sender: TObject);
    procedure cdsEncuestasNewRecord(DataSet: TDataSet);
    procedure cdsEncuestasAlCrearCampos(Sender: TObject);
    procedure cdsOpcionesEncuestaCambios(DataSet: TDataSet);
    procedure cdsOpcionesEncuestaAlAdquirirDatos(Sender: TObject);
    procedure cdsOpcionesEncuestaAlCrearCampos(Sender: TObject);
    procedure cdsOpcionesEncuestaBeforePost(DataSet: TDataSet);
    procedure cdsOpcionesEncuestaNewRecord(DataSet: TDataSet);
    procedure cdsEncuestasBeforePost(DataSet: TDataSet);
    procedure cdsEncuestasAlAdquirirDatos(Sender: TObject);
    procedure cdsEncuestasAlAgregar(Sender: TObject);
    procedure cdsEncuestasAlModificar(Sender: TObject);
    procedure cdsOpcionesEncuestaCalcFields(DataSet: TDataSet);
    procedure cdsEncuestasAlEnviarDatos(Sender: TObject);
    procedure cdsEncuestasAlBorrar(Sender: TObject);
  private
    { Private declarations }
    FListaContenidos: TListaContenidos;
    FRutaImagenGlobal: String;
    FCompanyLookup: Olevariant;
    //FUltimoMarco: String;
    FClasifiKiosco: Integer;
    {$ifdef DOS_CAPAS}
    FServerReportes: TdmServerReportes;
    function GetServerKiosco: TdmServerKiosco;
    {$else}
    FServerReportes : IdmServerReporteadorDD;
    function GetServerKiosco: IdmServerKioscoDisp;
    function GetServerReportes: IdmServerReporteadorDDDisp;
    {$endif}
    function GetFechaDefault: TDate;
    function GetUsuarioDefault: Integer;
    function GetMaxEncuesta: Integer;
    procedure LeeTabla(Sender: TClientDataset);
    procedure SetFechaDefault(const Value: TDate);
    procedure ShowEdicion( const eTabla: eTipoTabla );
    procedure ConectacdsCarruselesInfo( const sCodigo: string );
    procedure ConectacdsBotones( const sCodigo: string );
    procedure KW_CODIGOChange(Sender: TField);
    procedure KW_CODIGOValidate(Sender: TField);
    procedure KS_CODIGOChange(Sender: TField);
    procedure KB_ACCIONChange(Sender: TField);
    procedure KS_CODIGOValidate(Sender: TField);
    procedure KE_COLORGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SetColorOnGetText(const lHayEvento: Boolean);
    procedure CambiaOrdenDataset( DataSet:TClientDataset; DataSetHijo: TClientDataSet;
                                  const sField: string;
                                  const iPosActual, iNuevaPos: integer );
    procedure ObtieneEntidadGetText(Sender: TField; var Text: String;DisplayText: Boolean);
    procedure GetAccesoBotones( const sMarco: String );
    procedure ENC_STATUSValidate(Sender: TField);
    procedure AjustaTotalesEncuesta;
  protected
    { Protected declarations }
    FFechaDefault: TDate;
    {$ifdef DOS_CAPAS}
    property ServerKiosco: TdmServerKiosco read GetServerKiosco;
    property ServerReportes: TdmServerReportes read FServerReportes;
    {$else}
    property ServerKiosco: IdmServerKioscoDisp read GetServerKiosco;
    property ServerReportes: IdmServerReporteadorDDDisp read GetServerReportes;
    {$endif}
  public
    { Public declarations }
    property ListaContenidos: TListaContenidos read FListaContenidos;
    property FechaDefault: TDate read GetFechaDefault write SetFechaDefault;
    property RutaImagenGlobal: String read FRutaImagenGlobal write FRutaImagenGlobal;
    property UsuarioDefault: Integer read GetUsuarioDefault;
    function GetAccesos: OleVariant;
    function GetGrupoActivo: Integer;
    function GetTipoReportal( const eValor: eTipoReportal ): String;
   // function GetClasifiKiosco: Integer;
    procedure EditarGlobales;
    procedure EditarGlobalesKiosco;
    procedure CambiaOrdenBotones( const iPosActual, iNuevaPos: integer );
    procedure CambiaOrdenPaginas( const iPosActual, iNuevaPos: integer );
    procedure ReseteaPassword( const sEmpresa: string; const iEmpleado: integer );
    procedure RefrescaReportes( const sEmpresa: string );
    procedure RefrescarBitacora(Parametros: TZetaParams);
    procedure CargaListaGrupos( Lista: TStrings );
    procedure DescargaListaGrupos( Lista: TStrings );
    procedure CambiaOrdenOpcionesEncuesta( const iPosActual, iNuevaPos: integer );
    procedure RefrescarEncuestas(Parametros : TZetaParams);
  end;

  function OpenDialogImagen( const sFilename, sExtension, sExtDef: string ) : string;

var
  dmKiosco: TdmKiosco;


implementation

{$R *.DFM}

uses DSistema,
     DGlobal,
     DCliente,
     DDiccionario,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaConnectionProblem,
     ZetaRegistryCliente,
     ZetaDialogo,
     ZetaMsgDlg,
     ZAccesosTress,
     ZBaseEdicion,
     ZReconcile,
     ZGlobalTress,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     FGlobalKioscoEdit,
     FEditEstilosBotones,
     FEditMarcos,
     FEditCarruseles,
     FEditBotones,
     FLookupReporte,
     FSistEditBitacoraKiosco,
     FEditEncuestas;

const
     K_RP_T_DESC = 10;
     K_SIN_RESTRICCION = -1;


function OpenDialogImagen( const sFilename, sExtension, sExtDef: string ) : string;
var
   OpenPictureDialog: TOpenPictureDialog;
begin
     Result := sFilename;
     OpenPictureDialog := TOpenPictureDialog.Create( Application.MainForm );
     OpenPictureDialog.HelpContext := 0;

     with OpenPictureDialog do
     begin
          //DefaultExt := sExtDef;
          Filter := sExTension;
          //All (*.tee;*.gif;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf)|*.tee;*.gif;*.jpg;*.jpeg;*.bmp;*.ico;*.emf;*.wmf|TeeChart (*.tee)|*.tee|GIF Image (*.gif)|*.gif|JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Enhanced Metafiles (*.emf)|*.emf|Metafiles (*.wmf)|*.wmf
          Filename := sFilename;
          if Execute then
             Result := FileName;
          Free;
     end;
end;
{ TdmKiosco }

procedure TdmKiosco.DataModuleCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     FServerPortalTress := TdmPortalTress.Create( Self );
     FServerReportes := TdmServerReportes.Create( self );
    {$endif}


     FFechaDefault := Date;
     FClasifiKiosco:= K_SIN_RESTRICCION;
     FListaContenidos := TListaContenidos.Create;
     cdsCarruseles.Tag := Ord( ekCarruseles );
     cdsMarcosInformacion.Tag := Ord( ekMarcosInformacion );
     cdsBotones.Tag := ord( ekBotones );
     cdsEstilosBotones.Tag := Ord( ekEstilosBotones );
     cdsMarcosLookup.Tag := Ord( ekMarcosInformacion );
     cdsEncuestas.Tag := Ord( eEncuestas );
     cdsOpcionesEncuesta.Tag := Ord( eOpcEncuesta );


     cdsSecuencias.OnReconcileError := cdsReconcileError;
     cdsContenidos.OnReconcileError := cdsReconcileError;
     cdsCarruseles.OnReconcileError := cdsReconcileError;
     cdsCarruselesInfo.OnReconcileError := cdsReconcileError;
     cdsMarcosInformacion.OnReconcileError := cdsReconcileError;
     cdsBotones.OnReconcileError := cdsReconcileError;
     cdsEstilosBotones.OnReconcileError := cdsReconcileError;
     cdsMarcosLookup.OnReconcileError := cdsReconcileError;
     cdsPantallas.OnReconcileError := cdsReconcileError;
     cdsBitacora.OnReconcileError := cdsReconcileError;
     cdsAccesoBoton.OnReconcileError:=  cdsReconcileError;
     cdsConfiUsuarios.OnReconcileError:= cdsReconcileError;

     inherited;

     {$ifdef DOS_CAPAS}
     FServerKiosco := TdmServerKiosco.Create( Self );
     {$endif}
end;

procedure TdmKiosco.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaContenidos );
     {$ifdef DOS_CAPAS}
     FreeAndNil( FServerReportes );
     FreeAndNil( FServerKiosco );
     {$endif}
end;

{$ifdef DOS_CAPAS}
function TdmKiosco.GetServerKiosco: TdmServerKiosco;
begin
     Result := dmCliente.ServerKiosco;
end;


{$else}
function TdmKiosco.GetServerKiosco: IdmServerKioscoDisp;
begin
     Result := dmCliente.ServerKiosco;
end;


function TdmKiosco.GetServerReportes: IdmServerReporteadorDDDisp;
begin
     Result:= IdmServerReporteadorDDDisp( dmCliente.CreaServidor( CLASS_dmServerReporteadorDD, FServerReportes ) );
end;
{$endif}

function TdmKiosco.GetFechaDefault: TDate;
begin
     Result := Trunc( FFechaDefault );
end;

procedure TdmKiosco.SetFechaDefault(const Value: TDate);
begin
     if ( FFechaDefault <> Value ) then
     begin
          FFechaDefault := Value;
     end;
end;

procedure TdmKiosco.LeeTabla( Sender: TClientDataset );
begin
     Sender.Data := ServerKiosco.GetTabla( Sender.Tag );
end;

function TdmKiosco.GetTipoReportal( const eValor: eTipoReportal ): String;
begin
     case eValor of
          eTipoReporte: Result := 'Reporte';
          eTipoLiga: Result := 'Liga';
     else
         Result := '?';
     end;
end;

function TdmKiosco.GetUsuarioDefault: Integer;
begin
     Result := Global.GetGlobalInteger( K_GLOBAL_USER_DEFAULT );
end;

{ Compatibilidad con formas de Tress }

function TdmKiosco.GetAccesos: OleVariant;
begin
     ZetaCommonTools.SetOLEVariantToNull( Result );
end;

function TdmKiosco.GetGrupoActivo : Integer;
begin
     Result := D_GRUPO_SIN_RESTRICCION;
end;

{ **************** Manejo de Client DataSets ************************ }

procedure TdmKiosco.ShowEdicion( const eTabla: eTipoTabla );
begin
     case eTabla of
          ekEstilosBotones: ZBaseEdicion.ShowFormaEdicion( EditTipoNoticias, TTOEditEstilosBotones );
          ekCarruseles: ZBaseEdicion.ShowFormaEdicion( EdicionCarruseles, TEdicionCarruseles );
          ekMarcosInformacion: ZBaseEdicion.ShowFormaEdicion( EdicionMarcos, TEdicionMarcos );
          ekBotones: ZBaseEdicion.ShowFormaEdicion( EdicionBotones, TTOEditBotones );
          eEncuestas: ZBaseEdicion.ShowFormaEdicion( EdicionEncuestas , TEdicionEncuestas );
     end;
end;

{$ifdef VER130}
procedure TdmKiosco.cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmKiosco.cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
begin
     Action := ZReconcile.HandleReconcileError( Dataset, UpdateKind, E );
end;

{ ********** cdsTablas ********* }

procedure TdmKiosco.cdsTablasAlAdquirirDatos(Sender: TObject);
begin
      LeeTabla( TZetaClientDataSet( Sender ) );
end;

procedure TdmKiosco.cdsTablasAlModificar(Sender: TObject);
begin
     ShowEdicion( eTipoTabla( TClientDataSet( Sender ).Tag ) );
end;

procedure TdmKiosco.cdsTablasBeforePost(DataSet: TDataSet);
begin
     if DataSet.Fields[ 0 ].IsNull then
        DB.DatabaseError( 'Folio o C�digo No Puede Quedar Vac�o' );
end;

procedure TdmKiosco.cdsTablasAfterDelete(DataSet: TDataSet);
begin
     with TZetaClientDataSet( DataSet ) do
     begin
          Enviar;
     end;
end;

procedure TdmKiosco.cdsTablasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          PostData;
          if ( Changecount > 0 ) then
             Reconcile( ServerKiosco.GrabaTabla( Tag, Delta, ErrorCount ) );
     end;
end;




{ ********** Globales ********* }

procedure TdmKiosco.EditarGlobales;
//var
   //Forma: TGlobalEdit;
begin
     {Global.Conectar;
     Forma := TGlobalEdit.Create( Self );
     try
        with Forma do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( Forma );
     end; }
end;

procedure TdmKiosco.EditarGlobalesKiosco;
var
   Forma: TGlobalKioscoEdit;
begin
     Global.Conectar;
     Forma := TGlobalKioscoEdit.Create( Self );
     try
        with Forma do
        begin
             ShowModal;
        end;
     finally
            FreeAndNil( Forma );
     end;
end;


procedure TdmKiosco.cdsEstilosBotonesNewRecord(DataSet: TDataSet);
begin
     with cdsEstilosBotones do
     begin
          FieldByName('KE_COLOR').AsInteger := clBlack;
          FieldByName('KE_F_NAME').AsString := 'Arial';
          FieldByName('KE_F_SIZE').AsInteger := 10;
          FieldByName('KE_F_COLR').AsInteger := clBlack;
          FieldByName('KE_F_BOLD').AsString := K_GLOBAL_NO;
          FieldByName('KE_F_ITAL').AsString := K_GLOBAL_NO;
          FieldByName('KE_F_SUBR').AsString := K_GLOBAL_NO;

     end;

end;

procedure TdmKiosco.cdsCarruselesNewRecord(DataSet: TDataSet);
begin
     with cdsCarruseles do
     begin
          //FieldByName('KW_CODIGO' ) :=
          //FieldByName('KW_NOMBRE' ) :=
          //FieldByName('KW_SCR_INF' ) :=
          //FieldByName('KW_SCR_DAT' ) :=
          FieldByName( 'KW_TIM_DAT' ).AsInteger := 5;
          FieldByName( 'KW_GET_NIP' ).AsString :=  K_GLOBAL_NO;
     end;
end;


procedure TdmKiosco.KW_CODIGOValidate(Sender: TField);
begin
     //ValidaCodigo( cdsCarruseles, cdsCarruselesInfo );
end;

procedure TdmKiosco.KW_CODIGOChange(Sender: TField);
begin
     //CambiaCodigoHijo( cdsCarruseles, cdsCarruselesInfo, 'KW_CODIGO' );
end;

procedure TdmKiosco.cdsCarruselesAlCrearCampos(Sender: TObject);
begin
     cdsMarcosInformacion.Conectar;
     with cdsCarruseles do
     begin
          CreateSimpleLookup( cdsMarcosInformacion, 'KW_SCR_NOM', 'KW_SCR_DAT' );
          FieldByName( 'KW_CODIGO' ).OnChange := KW_CODIGOChange;
          FieldByName( 'KW_CODIGO' ).OnValidate := KW_CODIGOValidate;
     end;
end;

procedure TdmKiosco.cdsCarruselesAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iErrorCount: Integer;
   oCarrusel, oCarruselInfo: OleVariant;
begin
     with cdsCarruselesInfo do
     begin
          PostData;
     end;

     with cdsCarruseles do
     begin
          PostData;

          if ( ( ChangeCount > 0 ) or ( cdsCarruselesInfo.ChangeCount > 0 ) ) then
          begin
               DisableControls;
               try
                  oCarrusel := ServerKiosco.GrabaCarruseles( DeltaNull, cdsCarruselesInfo.Data, FieldByname('KW_CODIGO').AsString,
                                                               oCarruselInfo, ErrorCount, iErrorCount );
                  if NOT Reconcile( oCarrusel ) then
                     Edit;
               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure TdmKiosco.cdsCarruselesBeforePost(DataSet: TDataSet);
begin
     with cdsCarruseles do
     begin
          if Fields[ 0 ].IsNull then
             DB.DatabaseError( 'C�digo no puede quedar vac�o' );
          if StrVacio( FieldByName('KW_SCR_DAT').AsString ) then
             DB.DatabaseError( 'El c�digo del Marco Principal no puede quedar vac�o' );
     end;

     with cdsCarruselesInfo do
     begin
          DisableControls;
          try
             if RecordCount = 0 then
             begin
                  DB.DatabaseError( 'El Carrusel debe de contener por lo menos una p�gina web' );
             end;
          finally
                 EnableControls;
          end;
     end;
end;


procedure TdmKiosco.cdsCarruselesInfoAfterDelete(DataSet: TDataSet);
begin
     with cdsCarruseles do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmKiosco.cdsCarruselesInfoAfterEdit(DataSet: TDataSet);
begin
     with cdsCarruseles do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;


procedure TdmKiosco.ConectacdsCarruselesInfo( const sCodigo: string );
begin
     with cdsCarruselesInfo do
     begin
          Data := ServerKiosco.GetCarruselesInfo( sCodigo );
     end;
end;

procedure TdmKiosco.cdsCarruselesInfoAlAdquirirDatos(Sender: TObject);
begin
     ConectacdsCarruselesInfo( cdsCarruseles.FieldByname('KW_CODIGO').AsString );
end;

procedure TdmKiosco.cdsCarruselesInfoNewRecord(DataSet: TDataSet);
begin
     with cdsCarruselesInfo do
     begin
          FieldByName('KW_CODIGO').AsString := cdsCarruseles.FieldByname('KW_CODIGO').AsString;
          FieldByName('KI_ORDEN').AsInteger := RecordCount + 1;
          FieldByName('KI_TIMEOUT').AsInteger := 3;
          FieldByName('KI_REFRESH').AsString := K_GLOBAL_NO;

     end;
end;

procedure TdmKiosco.cdsCarruselesInfoBeforePost(DataSet: TDataSet);
begin
     with cdsCarruselesInfo do
     begin
          if ( FieldByName('KI_REFRESH').AsString <> K_GLOBAL_NO ) and
             ( FieldByName('KI_REFRESH').AsString <> K_GLOBAL_SI ) then
          begin
               DB.DatabaseError( 'El valor de Refrescar solo puede ser ''S'' o ''N'' may�sculas' );
          end;
     end;
end;


procedure TdmKiosco.KS_CODIGOChange(Sender: TField);
begin
     //CambiaCodigoHijo( cdsMarcosInformacion, cdsBotones, 'KS_CODIGO' );
end;

procedure TdmKiosco.KB_ACCIONChange(Sender: TField);
begin
{
     with cdsBotones do
     begin
          if ( eAccionBoton ( FieldByName('KB_ACCION').AsInteger )  =  abRegresar ) and
               ( FieldByName('KB_RESTRIC').AsString = K_GLOBAL_SI ) then
          begin
               if not ( State in [dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName('KB_RESTRIC').AsString:= K_GLOBAL_NO;
          end;
     end; }

end;

procedure TdmKiosco.KS_CODIGOValidate(Sender: TField);
begin
     //ValidaCodigo( cdsMarcosInformacion, cdsBotones );
end;


procedure TdmKiosco.cdsMarcosInformacionAlCrearCampos(Sender: TObject);
begin
     cdsEstilosBotones.Conectar;
     with TZetaLookupDataSet( Sender ) do
     begin
          CreateSimpleLookup( cdsEstilosBotones, 'KS_NOR_NOM', 'KS_EST_NOR' );
          CreateSimpleLookup( cdsEstilosBotones, 'KS_SEL_NOM', 'KS_EST_SEL' );

          FieldByName( 'KS_CODIGO' ).OnChange := KS_CODIGOChange;
          FieldByName( 'KS_CODIGO' ).OnValidate := KS_CODIGOValidate;
     end;

end;

procedure TdmKiosco.cdsMarcosInformacionAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iErrorCount: Integer;
   oMarcos, oAccesoBotones: OleVariant;
begin
     with cdsBotones do
     begin
          IndexFieldNames := VACIO;
          try
             PostData;
          finally
                 IndexFieldNames := 'KS_CODIGO;KB_ORDEN';
          end;
     end;
     with cdsAccesoBoton do
     begin
          PostData;
          Filtered:= FALSE;
     end;

     with cdsMarcosInformacion do
     begin
          PostData;

          if ( ( ChangeCount > 0 ) or ( cdsBotones.ChangeCount > 0 ) or ( cdsAccesoBoton.ChangeCount > 0 ) ) then
          begin
               DisableControls;
               try
                  oMarcos := ServerKiosco.GrabaMarcosInfo( DeltaNull, cdsBotones.Data, cdsAccesoBoton.Data, FieldByname('KS_CODIGO').AsString,
                                                             oAccesoBotones, ErrorCount, iErrorCount );
                  if not Reconcile( oMarcos )  then
                  begin
                       Edit;
                  end
                  else if ( iErrorCount = 0 ) then
                  begin
                       {Si el reconcile de oMarcos Resulta exitoso el dataset de botones y accesos deben quedar sin cambios.
                       En el servidor no cambia el cliendataset de botones pero el de accesos si puede llegar a cambiar, es por eso que se
                       reasigna}
                       cdsBotones.MergeChangeLog;
                       cdsAccesoBoton.Data:= oAccesoBotones;
                  end
                  else
                      Edit;

               finally
                      EnableControls;
               end;
          end;
     end;
end;

procedure Tdmkiosco.cdsMarcosInformacionBeforePost(DataSet: TDataSet);
begin
     with cdsMarcosInformacion do
     begin
          if FieldByName('KS_CODIGO').IsNull then
             DB.DatabaseError( 'C�digo no puede quedar vac�o' );
          if StrVacio( FieldByName('KS_EST_NOR').AsString ) then
             DB.DatabaseError( 'El c�digo de Estilo Normal no puede quedar vac�o' );
          if StrVacio( FieldByName('KS_EST_SEL').AsString ) then
             DB.DatabaseError( 'El c�digo de Estilo Seleccionado no puede quedar vac�o' );
     end;

     with cdsBotones do
     begin
          DisableControls;
          try                            
             if RecordCount = 0 then
             begin
                  DB.DatabaseError( 'El Marco de Informaci�n debe de contener por lo menos un bot�n' );
             end;

             First;

             if ( eAccionBoton( FieldByName('KB_ACCION').AsInteger ) = abPantalla ) then
                DB.DatabaseError( 'En el Marco de Informaci�n el primer bot�n no puede ser tipo "Pantalla"' );

             if ( eAccionBoton( FieldByName('KB_ACCION').AsInteger ) = abRegresar ) then
                DB.DatabaseError( 'En el Marco de Informaci�n el primer bot�n no puede ser tipo "Regresar"' );

             if NOT Locate('KB_ACCION', Ord(abRegresar), [] ) then
                DB.DatabaseError( 'El Marco de Informaci�n debe de contener el bot�n tipo "Regresar"' );

             if NOT Locate('KB_ACCION', Ord(abContenido), [] ) AND
                NOT Locate('KB_ACCION', Ord(abPantalla), [] ) AND
                NOT Locate('KB_ACCION', Ord(abMisDatos), [] ) AND
                NOT Locate('KB_ACCION', Ord(abReporte), [] ) then
                DB.DatabaseError( 'El Marco de Informaci�n debe de contener por lo menos un bot�n tipo "Contenido", "Pantalla", "Mis Datos" � "Reporte"' );

          finally
                 EnableControls;
          end;
     end;

end;

procedure Tdmkiosco.cdsBotonesBeforePost(DataSet: TDataSet);
begin
     with cdsBotones do
     begin
          if StrVacio( FieldByName('KB_TEXTO').AsString ) and
             StrVacio( FieldByName('KB_BITMAP').AsString ) then
             DB.DatabaseError( 'Falta especificar el texto o �cono del bot�n' );

          case eAccionBoton( FieldByName('KB_ACCION').AsInteger ) of
               abContenido:
               begin
                    if StrVacio( FieldByName('KB_URL').AsString ) then
                       DB.DatabaseError( 'La P�gina Web no puede quedar vac�a' );
               end;
               abPantalla:
               begin
                    if StrVacio( FieldByName('KB_SCREEN').AsString ) then
                       DB.DatabaseError( 'El Marco no puede quedar vac�o' );
               end;
               abMisDatos:
               begin
                    if StrVacio( FieldByName('KB_URL').AsString ) then
                       DB.DatabaseError( 'La Pantalla no puede quedar vac�a' );
               end;
               abReporte:
               begin
                    if ( FieldByName('KB_REPORTE').AsInteger = 0 ) then
                       DB.DatabaseError( 'El Reporte no puede quedar vac�o o ser CERO ( 0 )' );

               end;
               abRegresar:
               begin
                    with cdsBotones do
                    begin
                         if ( FieldByName('KB_RESTRIC').AsString = K_GLOBAL_SI ) then
                              FieldByName('KB_RESTRIC').AsString:= K_GLOBAL_NO
                    end;
               end;
          end;
     end;

end;


procedure Tdmkiosco.cdsMarcosInformacionNewRecord(DataSet: TDataSet);
begin
     with cdsMarcosInformacion do
     begin
          FieldByName('KS_PF_SIZE').AsInteger := 172;
          FieldByName('KS_PB_SIZE').AsInteger := 200;
          FieldByName('KS_BTN_ALT').AsInteger := 60;
          FieldByName('KS_BTN_SEP').AsInteger := 10;
          FieldByName('KS_PB_ALIN').AsInteger := Ord( ekIzquierda );

     end;
end;

procedure Tdmkiosco.cdsBotonesAfterDelete(DataSet: TDataSet);
begin
     with cdsMarcosInformacion do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure Tdmkiosco.cdsBotonesAfterEdit(DataSet: TDataSet);
begin
     with cdsMarcosInformacion do
     begin
          if not ( State in [ dsEdit, dsInsert ] ) then
             Edit;
     end;
end;

procedure TdmKiosco.ConectacdsBotones( const sCodigo: string );
begin
     with cdsBotones do
     begin
          Data := ServerKiosco.GetBotones( sCodigo );
     end;
end;


procedure TdmKiosco.cdsBotonesAlAdquirirDatos(Sender: TObject);
begin
     ConectacdsBotones( cdsMarcosInformacion.FieldByname('KS_CODIGO').AsString );
     GetAccesoBotones( cdsMarcosInformacion.FieldByname('KS_CODIGO').AsString  );
end;

procedure TdmKiosco.cdsBotonesNewRecord(DataSet: TDataSet);
begin
     with cdsBotones do
     begin
          FieldByName('KS_CODIGO').AsString := cdsMarcosInformacion.FieldByname('KS_CODIGO').AsString;
          FieldByName('KB_ORDEN').AsInteger := RecordCount + 1;
          FieldByName('KB_ACCION').AsInteger := ord( 0 );
          FieldByName('KB_LUGAR').AsInteger := ord( 0 );
          FieldByName('KB_ALTURA').AsInteger := 0;// El default es Cero, para que la altura la tome de su Marco de Informaci�n
          FieldByName('KB_RESTRIC').AsString := K_GLOBAL_NO;
          FieldByName('KB_USA_EMPL').AsString:= K_GLOBAL_NO;
          FieldByName('KB_IMP_DIR').AsString:= K_GLOBAL_NO;

     end;
end;

procedure TdmKiosco.cdsBotonesAlCrearCampos(Sender: TObject);
begin
     with cdsBotones do
     begin
          ListaFija( 'KB_ACCION', lfAccionBoton );
          FieldByName('KB_ACCION').OnChange:= KB_ACCIONChange;
     end;

end;

procedure TdmKiosco.CambiaOrdenBotones( const iPosActual, iNuevaPos: integer );
begin
     CambiaOrdenDataset( cdsBotones, cdsAccesoBoton, 'KB_ORDEN', iPosActual, iNuevaPos );
end;

procedure TdmKiosco.CambiaOrdenPaginas( const iPosActual, iNuevaPos: integer );
begin
     cdsCarruselesInfo.PostData;
     CambiaOrdenDataset( cdsCarruselesInfo, NIL,  'KI_ORDEN', iPosActual, iNuevaPos );
end;

procedure TdmKiosco.CambiaOrdenOpcionesEncuesta( const iPosActual, iNuevaPos: integer );
begin
     cdsOpcionesEncuesta.PostData;
     CambiaOrdenDataset( cdsOpcionesEncuesta , NIL,  'OP_ORDEN', iPosActual, iNuevaPos );
end;


procedure TdmKiosco.CambiaOrdenDataset( DataSet:TClientDataset; DataSetHijo: TClientDataSet;
                                        const sField: string;
                                        const iPosActual, iNuevaPos: integer );
 procedure SwapBotones( const i,j: integer);
 begin
     with Dataset do
     begin
          try
             DisableControls;
             if Locate(sField,i,[]) then
             begin
                  Edit;
                  FieldByName(sField).AsInteger := j ;
                  Post;
             end;
          finally
                 EnableControls;
          end;
     end;

     if ( DataSetHijo <> NIL ) then
     begin
          with DataSetHijo do
          begin
               try
                  DisableControls;
                  Filter:= Format( '( %s = %d )', [ sField, i ] );
                  Filtered:= TRUE;
                  while not EOF do
                  begin
                       Edit;
                       FieldByName(sField).AsInteger := j ;
                       Post;
                  end;
               finally
                      Filtered:= FALSE;
                      EnableControls;
               end;
          end;
     end;
 end;

begin
     SwapBotones( iPosActual, -1 );
     SwapBotones( iNuevaPos, iPosActual );
     SwapBotones( -1, iNuevaPos);
end;

procedure TdmKiosco.SetColorOnGetText(const lHayEvento: Boolean);
begin
     if lHayEvento then
     begin
          with cdsEstilosBotones do
          begin
               FieldByName('KE_COLOR').OnGetText :=  KE_COLORGetText;
               FieldByName('KE_F_COLR').OnGetText :=  KE_COLORGetText;
          end;
     end
     else
     begin
          with cdsEstilosBotones do
          begin
               FieldByName('KE_COLOR').OnGetText := NIL;
               FieldByName('KE_F_COLR').OnGetText := NIL;
          end;
     end;
end;

procedure TdmKiosco.cdsEstilosBotonesAlCrearCampos(Sender: TObject);
begin
     SetColorOnGetText(True);
end;

procedure TdmKiosco.KE_COLORGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          //Text := ColorToString( Sender.AsInteger );
          if NOT ColorToIdent( Sender.AsInteger, Text ) then
             Text := fcCommon.fcColorToRGBString( Sender.AsInteger );
     end;
end;

procedure TdmKiosco.ReseteaPassword( const sEmpresa: string; const iEmpleado: integer );
begin
     with dmCliente do
     begin
          FindCompany(sEmpresa);
          ServerKiosco.ReseteaPassword( BuildEmpresa( cdsCompany ), iEmpleado );
     end;
end;


procedure TdmKiosco.cdsEstilosBotonesAlModificar(Sender: TObject);
begin
     SetColorOnGetText( FALSE );
     try
        ShowEdicion( eTipoTabla( TClientDataSet( Sender ).Tag ) );
     finally
            SetColorOnGetText( TRUE );
     end;
end;

procedure TdmKiosco.cdsMarcosLookupGetRights(Sender: TZetaClientDataSet;  const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := FALSE;
end;




procedure TdmKiosco.cdsSuscripReportesAlAdquirirDatos(Sender: TObject);
var
   oCursor: TCursor;
begin
     dmSistema.cdsUsuariosLookup.Conectar;
     with cdsSuscripReportes do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourGlass;
          DisableControls;
          try
             IndexFieldNames:='';
             IndexName:='';
              { Se cambi� esta llamada, Kiosco nada mas puede ver la clasificaci�n configurada en Empresa }
             try
                if ( FClasifiKiosco = K_SIN_RESTRICCION ) then
                   Data := ServerReportes.GetLookUpReportes(FCompanyLookup,dmDiccionario.VerConfidencial)
                else

                    Data:= ServerReportes.GetReportes( FCompanyLookup,
                                                       FClasifiKiosco,
                                                       Ord( crFavoritos ), Ord( crSuscripciones ), VACIO, dmDiccionario.VerConfidencial );

                First;
                while NOT EOF do
                begin
                     if NOT( ZetaCommonLists.eTipoReporte( FieldByName('RE_TIPO').AsInteger ) in [trListado,trForma,trEtiqueta] )  then
                        Delete
                     else
                         Next;
                end;
                First;
                MergeChangeLog;

             except
                   on Error: Exception do
                   begin
                        ZetaDialogo.ZError( 'Error en Empresa', '� Error al cargar reportes de Empresa !', 0 );
                        if not ( State in [ dsInactive ] ) then
                           EmptyDataSet;
                   end;
             end;
             IndexFieldNames:='RE_NOMBRE';
          finally
                 EnableControls;
                 Screen.Cursor := oCursor;
          end;
     end;
end;


procedure TdmKiosco.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin

     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
     end

     else
         Text:= Sender.AsString;
end;

procedure TdmKiosco.cdsSuscripReportesAlCrearCampos(Sender: TObject);
var
   oCampo: TField;
begin
     inherited;
     with cdsSuscripReportes do
     begin
          oCampo := FindField( 'RE_ENTIDAD' );
          if ( oCampo <> nil )  then
          begin
               with oCampo do
               begin
                    OnGetText := ObtieneEntidadGetText;
                    Alignment := taLeftJustify;
               end;
          end;
          ListaFija('RE_TIPO', lfTipoReporte);
     end;
end;
procedure TdmKiosco.cdsSuscripReportesLookupSearch( Sender: TZetaLookupDataSet;
                                                    var lOk: Boolean;
                                                    const sFilter: String;
                                                    var sKey, sDescription: String);
begin
     with Sender do
     begin
          if FormaLookupReporte = NIL then
             FormaLookupReporte := TFormaLookupReporte.Create( self );

          with FormaLookupReporte do
          begin
               lOK := ShowModal = mrOk;
               if lOk then
               begin
                    sKey := IntToStr( NumReporte );
                    sDescription := NombreReporte;
               end;
          end;
     end;
end;

procedure TdmKiosco.RefrescaReportes( const sEmpresa: string );
 var
    sCompany: string;
begin
      with dmCliente do
      begin
           sCompany := cdsCompany.FieldByName('CM_CODIGO').AsString;
           try
              cdsCompany.First;
              if FindCompany( sEmpresa ) then
              begin
                   FCompanyLookup := SetCompany;
                   FClasifiKiosco:= cdsCompany.FieldByName('CM_KCLASIFI').AsInteger;
              end;
           finally
                  FindCompany( sCompany );
                  SetCompany;
           end;
      end;

     cdsSuscripReportes.Refrescar;
end;



procedure TdmKiosco.cdsBitacoraAlModificar(Sender: TObject);
begin
     FSistEditBitacoraKiosco.ShowLogDetail( cdsBitacora );
end;

procedure TdmKiosco.RefrescarBitacora(Parametros: TZetaParams);
begin
     cdsBitacora.Data := ServerKiosco.GetBitacora( Parametros.VarValues );
end;


procedure TdmKiosco.cdsBitacoraAfterOpen(DataSet: TDataSet);
begin
     with cdsBitacora do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_ACCION', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
     end
end;

procedure TdmKiosco.CargaListaGrupos( Lista: TStrings );
var
   iCodigo, iPos: Integer;
   sOldIndex: String;
begin
     with dmSistema.cdsGrupos do
     begin
          Conectar;
          Filter:= Format( '( GR_CODIGO <> %d )', [D_GRUPO_SIN_RESTRICCION] );
          Filtered:= TRUE;
          First;
          sOldIndex:= IndexFieldNames;
          IndexFieldNames:= 'GR_CODIGO';
          with Lista do
          begin
               try
                  BeginUpdate;
                  Clear;
                  while not Eof do
                  begin
                       AddObject( FieldByName( 'GR_CODIGO' ).AsString + '=' + FieldByName( 'GR_DESCRIP' ).AsString, TObject( 0 ) );
                       Next;
                  end;
               finally
                     IndexFieldNames:= sOldIndex;
                     EndUpdate;
               end;
          end;

     end;

     //GetAccesoBotones( cdsBotones.FieldByName('KS_CODIGO').AsString );

     with cdsAccesoBoton do
     begin
          Filter:= Format( '( KB_ORDEN = %d )', [ cdsBotones.FieldByName('KB_ORDEN').AsInteger ] );
          Filtered:= TRUE;
          try
             First;
             with Lista do
             begin
                  BeginUpdate;
                  while not Eof do
                  begin
                       iCodigo := FieldByName( 'GR_CODIGO' ).AsInteger;
                       iPos := IndexOfName( IntToStr( iCodigo ) );
                       if ( iPos < 0 ) then
                          AddObject( IntToStr( iCodigo ) + '=' + ' ???', TObject( 1 ) )
                       else
                           Objects[ iPos ] := TObject( 1 );
                       Next;
                  end;
                  EndUpdate;
             end;
          finally
                 Filtered:= FALSE;
          end;
     end;
end;

procedure TdmKiosco.GetAccesoBotones( const sMarco: String );
begin
     //if ( FUltimoMarco <> sMarco ) then
   //  begin
          cdsAccesoBoton.Data:= ServerKiosco.GetAccesoBoton( sMarco );
        //  FUltimoMarco:= sMarco;
   //  end;
end;

procedure TdmKiosco.DescargaListaGrupos( Lista: TStrings );
var
   sPantalla, sCodigoGrupo: String;
   iOrdenBoton, i: Integer;

   procedure LimpiaDataSet;
   begin
        with cdsAccesoBoton do
        begin
             First;
             while not EOF do
             begin
                  if not ( State in [dsEdit, dsInsert ] ) then
                     Edit;
                  Delete;
             end;
        end;
   end;
begin
     iOrdenBoton := cdsBotones.FieldByName( 'KB_ORDEN' ).AsInteger;
     sPantalla:= cdsBotones.FieldByName( 'KS_CODIGO' ).AsString;
     with cdsAccesoBoton do
     begin
          Filter:= Format( '( KB_ORDEN = %d )', [ cdsBotones.FieldByName('KB_ORDEN').AsInteger ] );
          Filtered:= TRUE;
          try
             {Limpia el DataSet Filtrado por Bot�n}
             LimpiaDataSet;
             with Lista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       if ( Integer( Lista.Objects[ i ] ) > 0 ) then
                       begin
                            sCodigoGrupo := Names[i];
                            cdsAccesoBoton.Append;
                            FieldByName( 'KB_ORDEN').AsInteger:= iOrdenBoton;
                            FieldByName( 'KS_CODIGO').AsString := sPantalla;
                            FieldByName( 'GR_CODIGO').AsString := sCodigoGrupo;
                            PostData;
                       end;
                  end;
             end;

             with cdsBotones do
             begin
                  if not ( State in [dsEdit, dsInsert] ) then
                     Edit;
             end;
          finally
                 Filtered:= FALSE;
          end;
         // Enviar;
     end;
end;


{ Esto Resuelve el problema de cancelar los cambios en el bot�n y sus accesos }
procedure TdmKiosco.cdsMarcosInformacionAfterCancel(DataSet: TDataSet);
begin
     with cdsBotones do
          if not ( State in [ dsInactive ] ) then
             CancelUpdates;

      with cdsAccesoBoton do
          if not ( State in [ dsInactive ] ) then
             CancelUpdates;
end;

procedure TdmKiosco.cdsConfiUsuariosAlAdquirirDatos(Sender: TObject);
begin
     cdsConfiUsuarios.Data := ServerKiosco.GetUsuariosConfidencialidad(dmSistema.cdsEmpresasKiosco.FieldByName('CM_CODIGO').AsString);
end;


procedure TdmKiosco.cdsConfiUsuariosAlEnviarDatos(Sender: TObject);
var
   ErrorCount:Integer;
   oConfidencialidad: OleVariant;
begin
     with cdsConfiUsuarios do
     begin
          PostData;
          if ( Changecount > 0 ) then
          begin
               oConfidencialidad:= ServerKiosco.GrabaUsuariosConfidencialidad( dmSistema.cdsEmpresasKiosco.FieldByName('CM_CODIGO').AsString, Data, ErrorCount );
               if ( ErrorCount = 0 ) then
                    Data:= oConfidencialidad
               else
                   Edit;
          end;
     end;

end;


procedure TdmKiosco.cdsConfiUsuariosAlAgregar(Sender: TObject);
begin
     { No hace nada}
end;

procedure TdmKiosco.cdsEncuestasAlAdquirirDatos(Sender: TObject);
begin
     // no se requiere
end;

function TdmKiosco.GetMaxEncuesta: Integer;
begin
     Result := ServerKiosco.GetMaxEncuesta;
end;

procedure TdmKiosco.RefrescarEncuestas(Parametros : TZetaParams);
begin
     with cdsEncuestas do
     begin
          Data := ServerKiosco.GetEncuestas( Parametros.VarValues );
          ResetDataChange;
     end;
end;

procedure TdmKiosco.cdsEncuestasAlCrearCampos(Sender: TObject);
begin
     with cdsEncuestas do
     begin
          MaskHoras('ENC_HORINI');
          MaskHoras('ENC_HORFIN');
          MaskFecha('ENC_FECINI');
          MaskFecha('ENC_FECFIN');
          ListaFija('ENC_STATUS', lfStatusEncuesta);
          dmSistema.cdsUsuarios.Conectar;
          CreateSimpleLookup(dmSistema.cdsUsuarios,'US_DESCRIP','US_CODIGO' );
          FieldByName( 'ENC_STATUS' ).OnValidate := ENC_STATUSValidate;
     end;
end;

procedure TdmKiosco.ENC_STATUSValidate(Sender: TField);
var
   Status: eStatusEncuesta;

   function SinrangoFechas: Boolean;
   begin
        with Sender.DataSet do
        begin
             Result := ( FieldByName('ENC_FECFIN').AsDateTime < FieldByName('ENC_FECINI').AsDateTime ) or
                       ( ( FieldByName('ENC_FECFIN').AsDateTime = FieldByName('ENC_FECINI').AsDateTime ) and
                         ( aMinutos( FieldByName('ENC_HORFIN').AsString ) <= aMinutos( FieldByName('ENC_HORINI').AsString ) )
                       );
        end;
   end;

begin
     with Sender do
     begin
          Status := eStatusEncuesta( Sender.AsInteger );
          if ( Status = stenDiseno ) and ( DataSet.FieldByName( 'VOTOS' ).AsInteger > 0 ) then
               db.DataBaseError( 'No se puede poner en dise�o una encuesta con votos' );
          if ( Status = stenAbierta ) and ( cdsOpcionesEncuesta.RecordCount < 2 ) then
               db.DataBaseError( 'Se requieren m�nimo dos opciones para abrir la encuesta' );
          if ( Status = stenAbierta ) and ( SinrangoFechas ) then
               db.DataBaseError( 'Se requiere especificar un rango de fechas para la encuesta' );
     end;
end;

procedure TdmKiosco.cdsEncuestasAlAgregar(Sender: TObject);
begin
     cdsEncuestas.Append;
     with cdsOpcionesEncuesta do
     begin
          if Active then
             EmptyDataset
          else
              Refrescar;
     end;
     cdsEncuestas.Modificar;
end;

procedure TdmKiosco.cdsEncuestasAlModificar(Sender: TObject);
begin
     if ( not ( cdsEncuestas.State = dsInsert ) ) then
     begin
          cdsOpcionesEncuesta.Refrescar;
          AjustaTotalesEncuesta;
     end;
     ShowEdicion( eTipoTabla( TClientDataSet( Sender ).Tag ) );
end;

procedure TdmKiosco.AjustaTotalesEncuesta;
var
   Pos : TBookMark;
   sMaxOpcion: String;
   iVotosOpcion, iMaxVotosOp, iCountVotos: Integer;
begin
     if ( cdsEncuestas.State = dsBrowse ) then
     begin
          iMaxVotosOp := 0;
          iCountVotos := 0;
          sMaxOpcion := VACIO;
          with cdsOpcionesEncuesta do
          begin
               if ( not IsEmpty ) then
               begin
                    DisableControls;
                    Pos := GetBookMark;
                    try
                       First;
                       while ( not EOF ) do       // Est� ordenado ascendente y se ocupa descendente en el combo
                       begin
                            iVotosOpcion := FieldByName( 'VOTOS' ).AsInteger;
                            iCountVotos := ( iCountVotos + iVotosOpcion );
                            if ( iVotosOpcion > iMaxVotosOp ) then
                            begin
                                 sMaxOpcion := FieldByName( 'OP_TITULO' ).AsString;
                                 iMaxVotosOp := iVotosOpcion;
                            end;
                            Next;
                       end;
                    finally
                           if ( Pos <> nil ) then
                           begin
                                GotoBookMark( Pos );
                                FreeBookMark( Pos );
                           end;
                           EnableControls;
                    end;
               end;
          end;
          with cdsEncuestas do
          begin
               if ( FieldByName( 'OPCION' ).AsString <> sMaxOpcion ) or
                  ( FieldByName( 'VOTOS' ).AsInteger <> iCountVotos ) then
               begin
                    Edit;
                    FieldByName( 'OPCION' ).AsString := sMaxOpcion;
                    FieldByName( 'VOTOS' ).AsInteger := iCountVotos;
                    Post;
                    MergeChangeLog;
               end;
          end;
     end;
end;

procedure TdmKiosco.cdsEncuestasAlBorrar(Sender: TObject);
var
   lHayVotos, lBorrar: Boolean;
begin
     lHayVotos := ( cdsEncuestas.FieldByName( 'VOTOS' ).AsInteger > 0 );
     if lHayVotos then
     begin
          lBorrar := ZetaDialogo.ZWarningConfirm( 'Encuestas', 'Esta encuesta ya tiene votos registrados' + CR_LF + CR_LF +
                                                  '� Seguro de borrar la encuesta ?', 0, mbNo );
     end
     else
     begin
          lBorrar := ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar esta encuesta ?' );
     end;
     if lBorrar then
        cdsEncuestas.Delete;
end;

procedure TdmKiosco.cdsEncuestasNewRecord(DataSet: TDataSet);
begin
     with cdsEncuestas do
     begin
          FieldByName('ENC_CODIGO').AsInteger := GetMaxEncuesta + 1;
          FieldByName('ENC_CONFID').AsString := K_GLOBAL_NO;
          FieldByName('ENC_VOTOSE').AsString := K_GLOBAL_NO;
          FieldByName('ENC_STATUS').AsInteger := Ord( stenDiseno );
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('ENC_FECINI').AsDateTime := Date;
          FieldByName('ENC_FECFIN').AsDateTime := Date;
          FieldByName('ENC_HORINI').AsString := '0700';
          FieldByName('ENC_HORFIN').AsString := '1700';
          FieldByName('ENC_MRESUL').AsString := K_GLOBAL_NO;
          FieldByName('ENC_OPCION').AsString := K_GLOBAL_NO;
          FieldByName('ENC_MSJCON').AsString := 'Gracias por tu voto.';
     end;
end;

procedure TdmKiosco.cdsEncuestasBeforePost(DataSet: TDataSet);
begin
     with cdsEncuestas do
     begin
          If StrVacio(FieldByName('ENC_NOMBRE').AsString) then
             ZError('Encuestas','Nombre de la encuesta no puede quedar vac�o',0);

          If StrVacio(FieldByName('ENC_PREGUN').AsString) then
             ZError('Encuestas','Pregunta de la encuesta no puede quedar vac�o',0);

          If StrVacio(FieldByName('ENC_MSJCON').AsString) then
             ZError('Encuestas','Mensaje de Confirmaci�n de la encuesta no puede quedar vac�o',0);
     end;
end;

procedure TdmKiosco.cdsEncuestasAlEnviarDatos(Sender: TObject);
var
   iEncuesta, iOpciones, iErrorEncuesta: Integer;
   oEncuesta, oOpciones, oResults: OleVariant;
begin
     with cdsOpcionesEncuesta do
     begin
          PostData;
          iOpciones := ChangeCount;
          if ( iOpciones = 0 ) then
             oOpciones := Null
          else
              oOpciones := Delta;
     end;
     with cdsEncuestas do
     begin
          PostData;
          iEncuesta := ChangeCount;
          if ( iEncuesta = 0 ) then
             oEncuesta := Null
          else
              oEncuesta := Delta;
     end;
     if ( iEncuesta > 0 ) or ( iOpciones > 0 ) then
     begin
          iErrorEncuesta := iEncuesta;
          oEncuesta := ServerKiosco.GrabaEncuestas( oEncuesta,cdsOpcionesEncuesta.Data , iErrorEncuesta,oResults,cdsEncuestas.FieldByName('ENC_CODIGO').AsString );
          if ( ( iEncuesta = 0 ) or cdsEncuestas.Reconcile( oEncuesta ) ) then
          begin
               // Se graba OK
          end
          else
          begin
               if ( cdsEncuestas.ChangeCount > 0 ) or ( cdsOpcionesEncuesta.ChangeCount > 0 ) then
               begin
                    cdsEncuestas.Edit;
               end;
          end;
     end;
end;

procedure TdmKiosco.cdsOpcionesEncuestaAlAdquirirDatos(Sender: TObject);
begin
     with cdsOpcionesEncuesta do
     begin
          Data := ServerKiosco.GetOpcionesEncuesta( cdsEncuestas.FieldByName('ENC_CODIGO').AsInteger );
     end;
end;

procedure TdmKiosco.cdsOpcionesEncuestaAlCrearCampos(Sender: TObject);
begin
     with cdsOpcionesEncuesta do
     begin
          CreateCalculated( 'OP_AVANCE', ftString, 10 );
     end;
end;

procedure TdmKiosco.cdsOpcionesEncuestaCalcFields(DataSet: TDataSet);
var
   TotVotos: Integer;
   rAvance: TPesos;
begin
     TotVotos := cdsEncuestas.FieldByName( 'VOTOS' ).AsInteger;
     if ( TotVotos > 0 ) then
     begin
          with DataSet do
          begin
               rAvance := ( FieldByName( 'VOTOS' ).AsInteger / TotVotos );
               FieldByName( 'OP_AVANCE' ).AsString := FormatFloat( '#0.# %', 100 * rAvance );
          end;
     end
     else
         DataSet.FieldByName( 'OP_AVANCE' ).AsString := VACIO;
end;

procedure TdmKiosco.cdsOpcionesEncuestaCambios(DataSet: TDataSet);
begin
     with cdsEncuestas do
     begin
          if ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TdmKiosco.cdsOpcionesEncuestaBeforePost(DataSet: TDataSet);
begin
     with cdsOpcionesEncuesta do
     begin
          if ( RecordCount = 10 ) and ( State = dsInsert ) then
          begin
               DatabaseError('El L�mite de Opciones por Encuesta es 10 ');
               Cancel;
          end;

          if StrVacio( FieldByName('OP_TITULO').AsString ) then
          begin
               FieldByName('OP_TITULO').FocusControl;
               DatabaseError('El T�tulo de la opci�n no puede quedar Vac�o');
          end;

     end;
end;

procedure TdmKiosco.cdsOpcionesEncuestaNewRecord(DataSet: TDataSet);
begin
     with cdsOpcionesEncuesta do
     begin
          FieldByName('ENC_CODIGO').AsInteger := cdsEncuestas.FieldByName('ENC_CODIGO').AsInteger;
          FieldByName('OP_ORDEN').AsInteger := RecordCount + 1;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          FieldByName('OP_TITULO').AsString := VACIO;
          FieldByName('OP_DESCRIP').AsString := VACIO;
     end;
end;

end.


