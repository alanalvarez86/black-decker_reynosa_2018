object dmCatalogos: TdmCatalogos
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 616
  Top = 166
  Height = 479
  Width = 741
  object cdsCondiciones: TZetaLookupDataSet
    Tag = 6
    Aggregates = <>
    IndexFieldNames = 'QU_CODIGO'
    Params = <>
    BeforeEdit = cdsCondicionesBeforeEdit
    BeforePost = cdsCondicionesBeforePost
    AfterPost = cdsAfterPost
    BeforeDelete = cdsCondicionesBeforeEdit
    AfterDelete = cdsAfterPost
    OnNewRecord = cdsCondicionesNewRecord
    AlAdquirirDatos = cdsCondicionesAlAdquirirDatos
    AlModificar = cdsCondicionesAlModificar
    UsaCache = True
    LookupName = 'Cat'#225'logo de Condiciones'
    LookupDescriptionField = 'QU_DESCRIP'
    LookupKeyField = 'QU_CODIGO'
    OnGetRights = cdsCondicionesGetRights
    Left = 32
    Top = 32
  end
end
