inherited SistEditBitacoraKiosco: TSistEditBitacoraKiosco
  Left = 226
  Top = 192
  Caption = 'Registro de bit'#225'cora'
  ClientHeight = 275
  ClientWidth = 444
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 232
    Width = 444
    Height = 43
    inherited Cancelar: TBitBtn [0]
      Left = 345
      Visible = False
    end
    inherited OK: TBitBtn [1]
      Left = 361
      Top = 4
      Height = 28
      Anchors = [akTop, akRight, akBottom]
    end
    object DBNavigator: TDBNavigator
      Left = 12
      Top = 4
      Width = 104
      Height = 29
      DataSource = DataSource
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      Flat = True
      Hints.Strings = (
        'Primero'
        'Anterior'
        'Siguiente'
        'UItimo')
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 444
    Height = 233
    Align = alTop
    BevelOuter = bvNone
    Constraints.MinWidth = 438
    TabOrder = 1
    object lbFecha: TLabel
      Left = 43
      Top = 6
      Width = 33
      Height = 15
      Alignment = taRightJustify
      Caption = 'Fecha:'
    end
    object BI_FECHA: TZetaDBTextBox
      Left = 79
      Top = 4
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'BI_FECHA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_FECHA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbHora: TLabel
      Left = 50
      Top = 23
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Hora:'
    end
    object BI_HORA: TZetaDBTextBox
      Left = 79
      Top = 22
      Width = 65
      Height = 17
      AutoSize = False
      Caption = 'BI_HORA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_HORA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbTipo: TLabel
      Left = 52
      Top = 41
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object BI_TIPO: TZetaDBTextBox
      Left = 79
      Top = 40
      Width = 217
      Height = 17
      AutoSize = False
      Caption = 'BI_TIPO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_TIPO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbKiosco: TLabel
      Left = 41
      Top = 112
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Kiosco:'
    end
    object BI_KIOSCO: TZetaDBTextBox
      Left = 79
      Top = 112
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'BI_KIOSCO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_KIOSCO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object MensajeLBL: TLabel
      Left = 17
      Top = 148
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object lbEmpleado: TLabel
      Left = 26
      Top = 76
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empleado:'
    end
    object CB_CODIGO: TZetaDBTextBox
      Left = 79
      Top = 76
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CB_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CB_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object lbAccion: TLabel
      Left = 40
      Top = 59
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'Acci'#243'n:'
    end
    object BI_ACCION: TZetaDBTextBox
      Left = 79
      Top = 58
      Width = 217
      Height = 17
      AutoSize = False
      Caption = 'BI_ACCION'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_ACCION'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Imagen: TImage
      Left = 392
      Top = 11
      Width = 34
      Height = 34
      Picture.Data = {
        055449636F6E0000010002002020100000000000E80200002600000020200200
        00000000300100000E0300002800000020000000400000000100040000000000
        0002000000000000000000000000000000000000000000000000800000800000
        00808000800000008000800080800000C0C0C000808080000000FF0000FF0000
        00FFFF00FF000000FF00FF00FFFF0000FFFFFF00000008888888888888888888
        8888880000008888888888888888888888888880003000000000000000000000
        0008888803BBBBBBBBBBBBBBBBBBBBBBBB7088883BBBBBBBBBBBBBBBBBBBBBBB
        BBB708883BBBBBBBBBBBBBBBBBBBBBBBBBBB08883BBBBBBBBBBBB7007BBBBBBB
        BBBB08803BBBBBBBBBBBB0000BBBBBBBBBB7088003BBBBBBBBBBB0000BBBBBBB
        BBB0880003BBBBBBBBBBB7007BBBBBBBBB708800003BBBBBBBBBBBBBBBBBBBBB
        BB088000003BBBBBBBBBBB0BBBBBBBBBB70880000003BBBBBBBBB707BBBBBBBB
        B08800000003BBBBBBBBB303BBBBBBBB7088000000003BBBBBBBB000BBBBBBBB
        0880000000003BBBBBBB70007BBBBBB708800000000003BBBBBB30003BBBBBB0
        88000000000003BBBBBB00000BBBBB70880000000000003BBBBB00000BBBBB08
        800000000000003BBBBB00000BBBB7088000000000000003BBBB00000BBBB088
        0000000000000003BBBB00000BBB708800000000000000003BBB70007BBB0880
        00000000000000003BBBBBBBBBB70880000000000000000003BBBBBBBBB08800
        000000000000000003BBBBBBBB7088000000000000000000003BBBBBBB088000
        0000000000000000003BBBBBB708800000000000000000000003BBBBB0880000
        00000000000000000003BBBB70800000000000000000000000003BB700000000
        0000000000000000000003330000000000000000F8000003F0000001C0000000
        80000000000000000000000000000001000000018000000380000003C0000007
        C0000007E000000FE000000FF000001FF000001FF800003FF800003FFC00007F
        FC00007FFE0000FFFE0000FFFF0001FFFF0001FFFF8003FFFF8003FFFFC007FF
        FFC007FFFFE00FFFFFE01FFFFFF07FFFFFF8FFFF280000002000000040000000
        0100010000000000800000000000000000000000000000000000000000000000
        FFFFFF000000000000000000000000003FFFFFC07FFFFFE07FFFFFF07FFCFFF0
        7FF87FE03FF87FE03FFCFFC01FFFFFC01FFDFF800FFDFF800FFDFF0007F8FF00
        07F8FE0003F8FE0003F07C0001F07C0001F0780000F0780000F070000078F000
        007FE000003FE000003FC000001FC000001F8000000F8000000F000000060000
        00000000FFFFFFFFFFFFFFFFC000001F8000000F000000070000000700000007
        000000078000000F8000000FC000001FC000001FE000003FE000003FF000007F
        F000007FF80000FFF80000FFFC0001FFFC0001FFFE0003FFFE0003FFFF0007FF
        FF0007FFFF800FFFFF800FFFFFC01FFFFFC01FFFFFE03FFFFFE03FFFFFF07FFF
        FFF8FFFF}
    end
    object lbMarco: TLabel
      Left = 43
      Top = 130
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Marco:'
    end
    object BI_UBICA: TZetaDBTextBox
      Left = 79
      Top = 130
      Width = 109
      Height = 17
      AutoSize = False
      Caption = 'BI_UBICA'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'BI_UBICA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label1: TLabel
      Left = 32
      Top = 95
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa:'
    end
    object CM_CODIGO: TZetaDBTextBox
      Left = 79
      Top = 94
      Width = 80
      Height = 17
      AutoSize = False
      Caption = 'CM_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object BI_TEXTO: TDBMemo
      Left = 79
      Top = 148
      Width = 306
      Height = 69
      Color = clBtnFace
      DataField = 'BI_TEXTO'
      DataSource = DataSource
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
  object DataSource: TDataSource
    Left = 344
    Top = 56
  end
end
