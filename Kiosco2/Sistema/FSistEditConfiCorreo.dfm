inherited SistEditConfiCorreo: TSistEditConfiCorreo
  Left = 290
  Top = 210
  Caption = 'Configuraci'#243'n de Correo Electr'#243'nico'
  ClientHeight = 252
  ClientWidth = 471
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 70
    Top = 51
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
  end
  object Label2: TLabel [1]
    Left = 66
    Top = 73
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre:'
  end
  object ZetaDBTextBox1: TZetaDBTextBox [2]
    Left = 112
    Top = 49
    Width = 81
    Height = 17
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CM_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object CM_NOMBRE: TZetaDBTextBox [3]
    Left = 112
    Top = 71
    Width = 257
    Height = 17
    AutoSize = False
    Caption = 'CM_NOMBRE'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CM_NOMBRE'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 216
    Width = 471
    TabOrder = 3
    inherited Cancelar: TBitBtn
      Left = 388
    end
  end
  inherited PanelSuperior: TPanel
    Width = 471
  end
  inherited PanelIdentifica: TPanel
    Width = 471
    inherited ValorActivo2: TPanel
      Width = 145
    end
  end
  object GroupBox1: TGroupBox [7]
    Left = 0
    Top = 97
    Width = 471
    Height = 119
    Align = alBottom
    Caption = 'Correo Electr'#243'nico:'
    TabOrder = 0
    object lbListaUsuarios: TLabel
      Left = 22
      Top = 26
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Lista de Usuarios:'
    end
    object bUsuarios: TSpeedButton
      Left = 296
      Top = 22
      Width = 23
      Height = 22
      Hint = 'Buscar Usuarios'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00D808DDDDDDDD
        DDDD83308DDDDDDDDDDD873308DDDDDDDDDDD373308DDDDDDDDDDD3733088888
        8DDDDDD37330000088DDDDDD37338777808DDDDDD038F7F77808DDDDD08FCCCC
        C788DDDCD0F7C7F7C770DDDCC07FCCCCCF70DDDCD0F7CFFCF770DDDDC88FCCCC
        7F88DDDDDD08F7F7F80DDDDDDDD08F7F80DDDDDDDDDD80008DDD}
      ParentShowHint = False
      ShowHint = True
      OnClick = bUsuariosClick
    end
    object CM_KUSERS: TDBEdit
      Left = 110
      Top = 23
      Width = 185
      Height = 21
      DataField = 'CM_KUSERS'
      DataSource = DataSource
      TabOrder = 0
    end
    object CM_KCONFI: TDBCheckBox
      Left = 5
      Top = 48
      Width = 118
      Height = 17
      Alignment = taLeftJustify
      Caption = 'Por Confidencialidad:'
      DataField = 'CM_KCONFI'
      DataSource = DataSource
      TabOrder = 1
      ValueChecked = 'S'
      ValueUnchecked = 'N'
      OnClick = CM_KCONFIClick
    end
    object btnConfigurarConfidencialidad: TBitBtn
      Left = 110
      Top = 70
      Width = 163
      Height = 27
      Hint = 'Configurar Correo por Confidencialidad'
      Caption = 'Configurar Confidencialidad'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = btnConfigurarConfidencialidadClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00550B30555000
        0000557F57F557777777550B305508888880557F57F575555557550B30508888
        8805557F57F7FFFFFF75550B300000000055557F5777777777F5550300000000
        0055557F7777777777F55500780F0F0F70555F777577F7F7F7F50078880F0F0F
        705577755577F7F7F7F50888880F0F0F70557F555F77F7F7F7F50888080F0F0F
        70557F557F77F7F7F7F50888980F0F0F70557F557F77F7F7F7F5088898007070
        70557F557F77F757575508889880077705557F5F7F5775FF7555089998888000
        55557F777F555777555508999888075555557577755F77555555508888075555
        5555575FFF775555555555000755555555555577775555555555}
      NumGlyphs = 2
    end
  end
  inherited DataSource: TDataSource
    Left = 372
    Top = 9
  end
end
