unit FGridConfidecialidadCorreo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls, Buttons,
  ExtCtrls, StdCtrls, ZetaSmartLists;

type
  TGridConfidencialidadCorreo = class(TBaseGridEdicion)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
  protected
    procedure Connect; override;
    procedure Buscar; override;
  public
    { Public declarations }
  end;

var
  GridConfidencialidadCorreo: TGridConfidencialidadCorreo;

implementation

uses DCliente,
     DSistema,
     ZetaCommonClasses,
     ZetaBuscaEmpleado;

{$R *.DFM}

{ TBancaElectronica }

procedure TGridConfidencialidadCorreo.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext := H11619_Banca_Electronica;
end;

procedure TGridConfidencialidadCorreo.Connect;
begin
     //dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmSistema do
     begin
          cdsNivel0.Refrescar;
          DataSource.DataSet:= cdsNivel0;
     end;
end;

procedure TGridConfidencialidadCorreo.Buscar;
begin
     {with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'TB_CODIGO' ) then
             BuscaEmpleado;
     end;}
end;

procedure TGridConfidencialidadCorreo.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     {if ZetaBuscaEmpleado.BuscaEmpleadoDialogo( VACIO, sKey, sDescription ) then
     begin
          with dmRecursos.cdsBancaElec do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;      
               FieldByName('CB_CODIGO').AsString:= sKey;
          end;
     end;}
end;

end.
