object dmZetaServerProvider: TdmZetaServerProvider
  OldCreateOrder = False
  OnCreate = dmZetaProviderCreate
  OnDestroy = dmZetaProviderDestroy
  Height = 479
  Width = 741
  object prvUnico: TDataSetProvider
    DataSet = qryMaster
    Exported = False
    Options = [poCascadeUpdates]
    UpdateMode = upWhereKeyOnly
    OnGetData = prvUnicoGetData
    OnUpdateData = prvUnicoUpdateData
    OnUpdateError = prvUnicoUpdateError
    AfterUpdateRecord = prvUnicoAfterUpdateRecord
    BeforeUpdateRecord = prvUnicoBeforeUpdateRecord
    Left = 32
    Top = 88
  end
  object dsMaster: TDataSource
    Left = 104
    Top = 88
  end
  object adoEmpresa: TADOConnection
    CommandTimeout = 1800
    IsolationLevel = ilReadCommitted
    LoginPrompt = False
    Provider = 'SQLNCLI'
    OnExecuteComplete = adoEmpresaExecuteComplete
    Left = 28
    Top = 24
  end
  object adoComparte: TADOConnection
    CommandTimeout = 1800
    IsolationLevel = ilReadCommitted
    LoginPrompt = False
    Provider = 'SQLNCLI'
    OnExecuteComplete = adoComparteExecuteComplete
    Left = 100
    Top = 24
  end
  object qryMaster: TADODataSet
    CursorType = ctOpenForwardOnly
    AfterOpen = qryMasterAfterOpen
    CommandTimeout = 1800
    EnableBCD = False
    Parameters = <>
    Left = 168
    Top = 88
  end
  object qryDetail: TADOTable
    AfterOpen = qryMasterAfterOpen
    CommandTimeout = 1800
    EnableBCD = False
    MasterSource = dsMaster
    Left = 240
    Top = 88
  end
  object qryCambios: TADOCommand
    CommandTimeout = 1800
    Parameters = <>
    Left = 304
    Top = 88
  end
end
