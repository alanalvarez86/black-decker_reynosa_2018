unit DPrimasIMSS;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
     ZetaCommonClasses,
     ZetaCommonTools,
     DZetaServerProvider;

{.$define DESCUENTO_SUA}

type
  TPrimasIMSS = record
       SS_L_A1061           : TPesos;
       SS_L_A1062           : TPesos;
       SS_L_A107            : TPesos;
       SS_L_A25             : TPesos;
       SS_L_CV              : TPesos;
       SS_L_GUARD           : TPesos;
       SS_L_INFO            : TPesos;
       SS_L_IV              : TPesos;
       SS_L_RT              : TPesos;
       SS_L_SAR             : TPesos;
       SS_MAXIMO            : TPesos;
       SS_O_A1062           : TTasa;
       SS_O_A107            : TTasa;
       SS_O_A25             : TTasa;
       SS_O_CV              : TTasa;
       SS_O_IV              : TTasa;
       SS_P_A1061           : TTasa;
       SS_P_A1062           : TTasa;
       SS_P_A107            : TTasa;
       SS_P_A25             : TTasa;
       SS_P_CV              : TTasa;
       SS_P_GUARD           : TTasa;
       SS_P_INFO            : TTasa;
       SS_P_IV              : TTasa;
       SS_P_SAR             : TTasa;
  end;
  TdmPrimasIMSS = class(TDataModule)
    procedure dmPrimasIMSSCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FQueryPrimas: TZetaCursor;
    FQueryRiesgo: TZetaCursor;
    dPrimasIMSS: TDate;
    oPrimasIMSS: TPrimasIMSS;
    dRiesgo: TDate;
    FPatron: String;
    FPrimaRiesgo: TTasa;
{$ifdef DESCUENTO_SUA}
    FQueryDescto: TZetaCursor;
    FPatronPrimas: String;
    FAplicaDescuento: Boolean;
{$endif}
    function oZetaProvider: TdmZetaServerProvider;
    procedure PreparaQueryPrimas;
    procedure CachePrimas(const dReferencia: TDate );
    procedure PreparaQueryRiesgo;
    procedure CacheRiesgo(const dReferencia: TDate; const sPatron: String );
{$ifdef DESCUENTO_SUA}
    procedure PreparaQueryDescto;
{$endif}
  public
    { Public declarations }
    function GetPrimasIMSS(const dReferencia: TDate; const sPatron: String = VACIO ): TPrimasImss;
    function GetPrimaRiesgo(const dReferencia: TDate; const sPatron: String ): TTasa;
{$ifdef DESCUENTO_SUA}
    function GetAplicaDescuento( const sPatron: String; var rFactor: TTasa ): Boolean;
{$endif}
  end;

implementation

uses DQueries;

{$R *.DFM}

const
     Q_QUERY_PRIMAS = 0;
     Q_QUERY_RIESGO = 1;
{$ifdef DESCUENTO_SUA}
     Q_QUERY_DESCTO = 2;
     K_FACTOR_DESCTO = 0.95;
{$endif}

function GetSQLScript( const iScript: Integer ): String;
begin
     case iScript of
          Q_QUERY_PRIMAS: Result := 'select SS_L_A1061, SS_L_A1062, SS_L_A107, '+
                                    'SS_L_A25, SS_L_CV, SS_L_GUARD, SS_L_INFO, '+
                                    'SS_L_IV, SS_L_RT, SS_L_SAR, SS_MAXIMO, SS_O_A1062, '+
                                    'SS_O_A107, SS_O_A25, SS_O_CV, SS_O_IV, SS_P_A1061, '+
                                    'SS_P_A1062, SS_P_A107, SS_P_A25, SS_P_CV, SS_P_GUARD, '+
                                    'SS_P_INFO, SS_P_IV,SS_P_SAR '+
                                    'from LEY_IMSS where '+
                                    '( SS_INICIAL = ( select MAX( SS_INICIAL ) '+
                                    'from LEY_IMSS where ( SS_INICIAL <= :Fecha ) ) ) ';
          Q_QUERY_RIESGO: Result := 'select RT_PRIMA from PRIESGO ' +
                                    'where ( TB_CODIGO = :Patron ) and ( RT_FECHA <= :Fecha ) ' +
                                    'order by RT_FECHA desc';
{$ifdef DESCUENTO_SUA}
          Q_QUERY_DESCTO: Result := 'select TB_TEXTO from RPATRON ' +
                                    'where ( TB_CODIGO = :Patron ) ';
{$endif}
     else
         Result := '';
     end;
end;

{ ********* TdmPrimasIMSS ********** }

procedure TdmPrimasIMSS.dmPrimasIMSSCreate(Sender: TObject);
begin
     { Asegura que la primera vez se ejecute el Cache }
     dPrimasIMSS := 0;
     dRiesgo := 0;
     FPatron := VACIO;
{$ifdef DESCUENTO_SUA}
     FPatronPrimas := VACIO;
     FAplicaDescuento := FALSE;
{$endif}
end;

procedure TdmPrimasIMSS.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FQueryPrimas );
     FreeAndNil( FQueryRiesgo );
{$ifdef DESCUENTO_SUA}
     FreeAndNil( FQueryDescto );
{$endif}
end;

function TdmPrimasIMSS.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

function TdmPrimasIMSS.GetPrimasIMSS( const dReferencia : TDate; const sPatron: String = VACIO ) : TPrimasImss;
{$ifdef DESCUENTO_SUA}
var
   rFactor: TTasa;
{$endif}
begin
     { Si es distinta la fecha a la ULTIMA llamada, es cuando tiene que ejecutar el query }
     if ( dReferencia <> dPrimasIMSS ) then
        CachePrimas( dReferencia );
     Result := oPrimasIMSS;

{$ifdef DESCUENTO_SUA}
     if GetAplicaDescuento( sPatron, rFactor ) then
     begin
          with Result do
          begin
               SS_P_A1061 := ( SS_P_A1061 * rFactor );
               SS_P_A1062 := ( SS_P_A1062 * rFactor );
               SS_P_A107  := ( SS_P_A107 * rFactor );
               SS_P_A25   := ( SS_P_A25 * rFactor );
               SS_P_IV    := ( SS_P_IV * rFactor );
               SS_P_GUARD := ( SS_P_GUARD * rFactor );
          end;
     end;
{$endif}

end;

procedure TdmPrimasIMSS.PreparaQueryPrimas;
begin
     if ( FQueryPrimas = nil ) then
     begin
          FQueryPrimas := oZetaProvider.CreateQuery( GetSQLScript( Q_QUERY_PRIMAS ) );
     end;
end;

procedure TdmPrimasIMSS.CachePrimas(const dReferencia: TDate);
begin
     PreparaQueryPrimas;
     with FQueryPrimas do
     begin
          dPrimasIMSS := dReferencia;
          Active := False;
          oZetaProvider.ParamAsDate( FQueryPrimas, 'Fecha', dPrimasIMSS );
          Active := True;
          with oPrimasIMSS do
          begin
               SS_L_A1061 := FieldByName( 'SS_L_A1061' ).AsFloat;
               SS_L_A1062 := FieldByName( 'SS_L_A1062' ).AsFloat;
               SS_L_A107 := FieldByName( 'SS_L_A107' ).AsFloat;
               SS_L_A25 := FieldByName( 'SS_L_A25' ).AsFloat;
               SS_L_CV := FieldByName( 'SS_L_CV' ).AsFloat;
               SS_L_GUARD := FieldByName( 'SS_L_GUARD' ).AsFloat;
               SS_L_INFO := FieldByName( 'SS_L_INFO' ).AsFloat;
               SS_L_IV := FieldByName( 'SS_L_IV' ).AsFloat;
               SS_L_RT := FieldByName( 'SS_L_RT' ).AsFloat;
               SS_L_SAR := FieldByName( 'SS_L_SAR' ).AsFloat;
               SS_MAXIMO := FieldByName( 'SS_MAXIMO' ).AsFloat;
               SS_O_A1062 := FieldByName( 'SS_O_A1062' ).AsFloat;
               SS_O_A107 := FieldByName( 'SS_O_A107' ).AsFloat;
               SS_O_A25 := FieldByName( 'SS_O_A25' ).AsFloat;
               SS_O_CV := FieldByName( 'SS_O_CV' ).AsFloat;
               SS_O_IV := FieldByName( 'SS_O_IV' ).AsFloat;
               SS_P_A1061 := FieldByName( 'SS_P_A1061' ).AsFloat;
               SS_P_A1062 := FieldByName( 'SS_P_A1062' ).AsFloat;
               SS_P_A107 := FieldByName( 'SS_P_A107' ).AsFloat;
               SS_P_A25 := FieldByName( 'SS_P_A25' ).AsFloat;
               SS_P_CV := FieldByName( 'SS_P_CV' ).AsFloat;
               SS_P_GUARD := FieldByName( 'SS_P_GUARD' ).AsFloat;
               SS_P_INFO := FieldByName( 'SS_P_INFO' ).AsFloat;
               SS_P_IV := FieldByName( 'SS_P_IV' ).AsFloat;
               SS_P_SAR := FieldByName( 'SS_P_SAR' ).AsFloat;
          end;
          Active := False;
     end;
end;

{$ifdef DESCUENTO_SUA}

procedure TdmPrimasIMSS.PreparaQueryDescto;
begin
     if ( FQueryDescto = nil ) then
     begin
          FQueryDescto := oZetaProvider.CreateQuery( GetSQLScript( Q_QUERY_DESCTO ) );
     end;
end;

function TdmPrimasIMSS.GetAplicaDescuento( const sPatron: String; var rFactor: TTasa ): Boolean;
begin
     Result := strLleno( sPatron );
     if Result then
     begin
          if ( sPatron <> FPatronPrimas ) then
          begin
               PreparaQueryDescto;
               with FQueryDescto do
               begin
                    Active := False;
                    oZetaProvider.ParamAsChar( FQueryDescto, 'Patron', sPatron, K_ANCHO_REGPATRONAL );
                    Active := True;
                    FAplicaDescuento := zStrToBool( UpperCase( Copy( Trim( FieldByName( 'TB_TEXTO' ).AsString ), 1, 1 ) ) );
               end;
               FPatronPrimas := sPatron;
          end;
          Result := FAplicaDescuento;
     end;
     if Result then
        rFactor := K_FACTOR_DESCTO
     else
         rFactor := 1;
end;

{$endif}

{************** Prima de Riesgo de Trabajo, por Registro Patronal *************}

function TdmPrimasIMSS.GetPrimaRiesgo(const dReferencia: TDate; const sPatron: String): TTasa;
{$ifdef DESCUENTO_SUA}
var
   rFactor: TTasa;
{$endif}
begin
     { Si es distinta la fecha a la ULTIMA llamada, es cuando tiene que ejecutar el Query }
     if ( dReferencia <> dRiesgo ) or ( sPatron <> FPatron )  then
        CacheRiesgo( dReferencia, sPatron );
     Result := FPrimaRiesgo;

{$ifdef DESCUENTO_SUA}
     if GetAplicaDescuento( sPatron, rFactor ) then
        Result := ( Result * rFactor );
{$endif}
end;

procedure TdmPrimasIMSS.PreparaQueryRiesgo;
begin
     if ( FQueryRiesgo = nil ) then
     begin
          FQueryRiesgo := oZetaProvider.CreateQuery( GetSQLScript( Q_QUERY_RIESGO ) );
     end;
end;

procedure TdmPrimasIMSS.CacheRiesgo(const dReferencia: TDate; const sPatron : String);
begin
     PreparaQueryRiesgo;
     with FQueryRiesgo do
     begin
          dRiesgo := dReferencia;
          FPatron := sPatron;
          Active := False;
          with oZetaProvider do
          begin
               ParamAsDate( FQueryRiesgo, 'Fecha', dRiesgo );
               //ParamAsChar( FQueryRiesgo, 'Patron', FPatron, K_ANCHO_CODIGO1 );
               ParamAsChar( FQueryRiesgo, 'Patron', FPatron, K_ANCHO_PATRON );
          end;
          Active := True;
          FPrimaRiesgo := Fields[ 0 ].AsFloat;
          Active := False;
     end;
end;

end.
