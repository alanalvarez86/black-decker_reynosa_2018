object ServerBase: TServerBase
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Left = 612
  Top = 282
  Height = 330
  Width = 423
  object dsUsuarios: TDataSource
    DataSet = UserDataSet
    Left = 156
    Top = 144
  end
  object UserDataSet: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 108
    Top = 64
    object UserDataSetAddress: TStringField
      DisplayLabel = 'Direcci'#243'n'
      FieldName = 'Address'
      Size = 15
    end
    object UserDataSetActivity: TStringField
      DisplayLabel = 'Actividad'
      FieldName = 'Activity'
      Size = 25
    end
    object UserDataSetDate: TDateTimeField
      DisplayLabel = 'Fecha'
      DisplayWidth = 50
      FieldName = 'Date'
      DisplayFormat = 'dd/mmm/yyyy tt'
    end
  end
end
