unit DTablasISPT;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
     ZetaCommonClasses,
     DZetaServerProvider;

type
  TRenglonNumerica = record
    Inferior: TPesos;
    Cuota: TPesos;
    Tasa: TTasa;
  end;
  PRenglonLista = ^TRenglonLista;
  TRenglonLista = record
    Next: PRenglonLista;
    Datos: TRenglonNumerica;
  end;
  TTablaNumerica = class(TObject)
  private
    { Private declarations }
    FProvider: TdmZetaServerProvider;
    FTabla: Integer;
    FRenglones: Integer;
    ListaRenglones: PRenglonLista;
    FInicial : TDate;
    FFinal   : TDate;
    FQTablaISPT: TZetaCursor;
    FQFechaISPT: TZetaCursor;
    property oZetaProvider: TdmZetaServerProvider read FProvider;
    procedure BorraListaRenglones;
    procedure CacheTablaISPT( const dFecha: TDate );
    procedure LeeValores( const dFecha: TDate );
    procedure AgregaRenglon( const nInferior, nCuota, nTasa : Double );
  public
    { Public declarations }
    constructor Create( const iTabla: Integer; oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    property QTablaISPT : TZetaCursor write FQTablaISPT;
    property QFechaISPT : TZetaCursor write FQFechaISPT;
    function GetRenglon( const nValor: TPesos ): TRenglonNumerica;
    procedure SetTablaFecha( const dFecha: TDate );
  end;
  TdmTablasISPT = class(TDataModule)
    procedure dmTablasISPTCreate(Sender: TObject);
    procedure dmTablasISPTDestroy(Sender: TObject);
  private
    { Private declarations }
    FListaTablas: TStringList;
    FQTablaISPT: TZetaCursor;
    FQFechaISPT: TZetaCursor;
    FFactorSubsidio: TTasa;
    function GetTablaNumerica( const iTabla: Integer; const dFecha: TDate ): TTablaNumerica;
    function AgregaTablaNumerica( const iTabla: Integer ): Integer;
    procedure CierraTablas;
    function oZetaProvider: TdmZetaServerProvider;
  public
    { Public declarations }
    function ConsultaTablaNumerica( const iTabla: Integer; const nValor: TPesos ): TRenglonNumerica; overload;
    function ConsultaTablaNumerica( const iTabla: Integer; const nValor: TPesos; const dFecha: TDate ): TRenglonNumerica; overload;
    function GetFactorSubsidio: TTasa;
  end;

implementation

uses ZGlobalTress;

{$R *.DFM}

const
     K_FECHA_TOPE = 2100 * 365;

{*************************** TdmTablasISPT **************************}

procedure TdmTablasISPT.dmTablasISPTCreate(Sender: TObject);
const
     K_QUERY_TABLA = 'select A80_LI, A80_CUOTA, A80_TASA ' +
                     'from ART_80 ' +
                     'where NU_CODIGO = :Tabla and TI_INICIO = :Inicio ' +
                     'order by A80_LI';
     K_QUERY_FECHA = 'select TI_INICIO '+
                     'from T_ART_80 ' +
                     'where NU_CODIGO = :Tabla ' +
                     'order by TI_INICIO desc';
begin
     FListaTablas := TStringList.Create;
     with oZetaProvider do
     begin
          FQTablaISPT  := CreateQuery( K_QUERY_TABLA );
          FQFechaISPT  := CreateQuery( K_QUERY_FECHA );
     end;
     FFactorSubsidio := -1; // Obliga que lo lea la primera vez
end;

procedure TdmTablasISPT.dmTablasISPTDestroy(Sender: TObject);
begin
     FreeAndNil( FQFechaISPT );
     FreeAndNil( FQTablaISPT );
     CierraTablas;
     FListaTablas.Free;
end;

function TdmTablasISPT.oZetaProvider: TdmZetaServerProvider;
begin
     Result := TdmZetaServerProvider( Owner );
end;

function TdmTablasISPT.ConsultaTablaNumerica( const iTabla: Integer; const nValor: TPesos;
         const dFecha: TDate ): TRenglonNumerica;
begin
     with GetTablaNumerica( iTabla, dFecha ) do
          Result := GetRenglon( nValor );
end;

function TdmTablasISPT.ConsultaTablaNumerica( const iTabla: Integer;
         const nValor : TPesos ) : TRenglonNumerica;
begin
     with GetTablaNumerica( iTabla, Date ) do      // Se utiliza la fecha de computadora
          Result := GetRenglon( nValor );
end;

function TdmTablasISPT.AgregaTablaNumerica( const iTabla: Integer ) : Integer;
var
   oTabla: TTablaNumerica;
begin
     oTabla := TTablaNumerica.Create( iTabla, oZetaProvider );
     with oTabla do
     begin
          QTablaISPT := self.FQTablaISPT;    // Asigna Apuntadores a Querys del DataModule
          QFechaISPT := self.FQFechaISPT;
     end;
     //oTabla.LeeValores( FQTablaISPT );
     Result := FListaTablas.AddObject( IntToStr( iTabla ), oTabla );
end;

function TdmTablasISPT.GetTablaNumerica( const iTabla: Integer; const dFecha: TDate ) : TTablaNumerica;
var
   iPos: Integer;
begin
     with FListaTablas do
     begin
          iPos := Indexof( IntToStr( iTabla ) );
          if ( iPos < 0 ) then
             iPos := AgregaTablaNumerica( iTabla );
          Result := TTablaNumerica( Objects[ iPos ] );
          with Result do
          begin
               SetTablaFecha( dFecha );
          end;
     end;
end;

procedure TdmTablasISPT.CierraTablas;
var
   i: Integer;
begin
     if Assigned( FListaTablas ) then
     begin
          with FListaTablas do
          begin
               for i := 0 to Count-1 do
                   TTablaNumerica( Objects[ i ] ).Free;
               Clear;
          end;
     end;
end;

function TdmTablasISPT.GetFactorSubsidio: TTasa;
begin
     // CACHE: Si no lo ha consultado lo obtiene
     if ( FFactorSubsidio < 0 ) then
     begin
          with oZetaProvider do
          begin
               InitGlobales;
               FFactorSubsidio := GetGlobalReal( K_GLOBAL_PORCEN_SUBSIDIO );
          end;
     end;
     Result := FFactorSubsidio;
end;

{*********************** TTablaNumerica ***********************}

constructor TTablaNumerica.Create( const iTabla:Integer; oProvider: TdmZetaServerProvider );
begin
     FProvider := oProvider;
     FTabla := iTabla;
     FRenglones := 0;
     ListaRenglones := nil;
     FInicial := NullDateTime;
     FFinal := NullDateTime;
     FQTablaISPT := nil;
     FQFechaISPT := nil;
end;

destructor TTablaNumerica.Destroy;
begin
     BorraListaRenglones;
end;

procedure TTablaNumerica.BorraListaRenglones;
var
   Siguiente: PRenglonLista;
begin
     while ( ListaRenglones <> nil ) do
     begin
          Siguiente := ListaRenglones^.Next;
          Dispose( ListaRenglones );
          ListaRenglones := Siguiente;
     end;
     FRenglones := 0;
end;

procedure TTablaNumerica.SetTablaFecha( const dFecha: TDate );
begin
     if ( dFecha < FInicial ) or ( dFecha > FFinal ) then
        CacheTablaISPT( dFecha );
end;

procedure TTablaNumerica.CacheTablaISPT( const dFecha: TDate );
var
   lEncontro: Boolean;
begin
     lEncontro := FALSE;
     with FQFechaISPT do
     begin
          Active := FALSE;
          oZetaProvider.ParamAsInteger( FQFechaISPT, 'Tabla', FTabla );
          Active := TRUE;
          FFinal := K_FECHA_TOPE;
          { Recordar que el query viene en orden descendente;
            El primer registro del query es el de la �ltima fecha
            Generalmente, en el primer registro se encuentra }
          while ( not EOF ) do
          begin
               FInicial := FieldByName( 'TI_INICIO' ).AsDateTime;
               if ( FInicial <= dFecha ) then
               begin
                    lEncontro := True;
                    Break;
               end;
               FFinal := FInicial - 1;
               Next;
          end;
          Active := FALSE;
     end;
     // Agregar Renglones de La Tabla
     BorraListaRenglones;              // Borra Tabla Anterior
     if lEncontro then
        LeeValores( FInicial )
     else
         AgregaRenglon( 0, 0, 0 );

end;

procedure TTablaNumerica.LeeValores( const dFecha: TDate );
begin
     with FQTablaISPT do
     begin
          Active := FALSE;
          with oZetaProvider do
          begin
               ParamAsInteger( FQTablaISPT, 'Tabla', FTabla );
               ParamAsDate( FQTablaISPT, 'Inicio', dFecha );
          end;
          Active := TRUE;
          if EOF then
             AgregaRenglon( 0, 0, 0 )
          else
          begin
               while ( not EOF ) do
               begin
                    AgregaRenglon( FieldByName( 'A80_LI' ).AsFloat,
                                   FieldByName( 'A80_CUOTA' ).AsFloat,
                                   FieldByName( 'A80_TASA' ).AsFloat );
{
                    AgregaRenglon( Fields[ 0 ].AsFloat,
                                   Fields[ 1 ].AsFloat,
                                   Fields[ 2 ].AsFloat );
}
                    Next;
               end;
          end;
          Active := FALSE;
     end;
end;

procedure TTablaNumerica.AgregaRenglon( const nInferior, nCuota, nTasa : Double );
var
   P : PRenglonLista;
begin
     New( P );
     P^.Next := ListaRenglones;
     with P^.Datos do
     begin
          Inferior := nInferior;
          Cuota := nCuota;
          Tasa := nTasa;
     end;
     ListaRenglones := P;
     FRenglones := FRenglones + 1;
end;

function TTablaNumerica.GetRenglon( const nValor : TPesos ) : TRenglonNumerica;
var
   P : PRenglonLista;
begin
     P := ListaRenglones;
     while ( P <> NIL ) do
     begin
          with P^.Datos do
          begin
               // Valida que no se salga de la lista
               if ( nValor >= Inferior ) or ( P^.Next = NIL ) then
                  Break;
               P := P^.Next;
          end;
     end;
     Result := P^.Datos;
end;

end.
