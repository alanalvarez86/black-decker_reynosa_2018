unit ZAgenteSQL;

interface

uses Classes, SysUtils, ZetaEntidad, DEntidades,
     ZetaTipoEntidad,
     Variants,
     ZetaCommonLists, ZAgenteSQLClient, DBClient;

type
  TSQLAgente = class(TSQLAgenteClient)
  private
    FEntidades : TdmEntidades;
  public
    property Entidades : TdmEntidades read FEntidades write FEntidades;
    function AgregaColumna( sFormula : String; const lDiccion : Boolean;
                         const eTipoEntidad : TipoEntidad;
                         const eTipoFormula : eTipoGlobal;
                         const iAncho : Integer;
                         const sAlias : String = '';
                         const eTotal : eTipoOperacionCampo = ocAutomatico;
                         const lGroupExp : Boolean = FALSE;
                         const iBanda : Integer = -1 ) : Integer; override;

  end;


implementation

uses ZetaCommonTools;


{ TSQLAgente }


function TSQLAgente.AgregaColumna(sFormula: String;
  const lDiccion: Boolean; const eTipoEntidad : TipoEntidad;
  const eTipoFormula: eTipoGlobal; const iAncho : Integer; const sAlias : String;
  const eTotal : eTipoOperacionCampo;
  const lGroupExp : Boolean; const iBanda : Integer ): Integer;
begin
     // En un solo punto, convertir los campos "CAMPO" a "TABLA.CAMPO"
     // para evitar errores marcados por ODBCEXPRESS
     if ( lDiccion ) and Assigned( FEntidades ) and ( sFormula = sAlias ) and ( Pos( '.', sFormula ) = 0 ) then
        sFormula := FEntidades.NombreEntidad( eTipoEntidad ) + '.' + sFormula;
     Result := FListaColumnas.AgregaColumna( sFormula, lDiccion, eTipoEntidad, eTipoFormula, iAncho, sAlias, eTotal, lGroupExp, iBanda );
end;



end.
