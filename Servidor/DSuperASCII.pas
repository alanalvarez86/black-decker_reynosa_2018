unit DSuperASCII;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, Math,
     ZetaServerDataSet,
     ZetaCommonLists;

type
  TColumnaASCII = class
  private
    { Private declarations }
    FObligatorio: Boolean;
    FTipo: eTipoGlobal;
    FDecimal: Integer;
    FLongitud: Integer;
    FPosicion: Integer;
    FDefault: String;
  public
    { Public declarations }
    property Decimal: Integer read FDecimal write FDecimal;
    property Default: String read FDefault write FDefault;
    property Longitud: Integer read FLongitud write FLongitud;
    property Obligatorio: Boolean read FObligatorio write FObligatorio;
    property Posicion: Integer read FPosicion write FPosicion;
    property Tipo: eTipoGlobal read FTipo write FTipo;
  end;

type
  TExaminaASCIIEvent = procedure( var sDatos: String ) of object;
  TValidaASCIIEvent = procedure( DataSet: TDataset; var nProblemas : Integer; var sErrorMsg : String ) of object;
  TdmSuperASCII = class(TDataModule)
    cdsResultado: TServerDataSet;
    cdsASCII: TServerDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FOnValidar: TValidaASCIIEvent;
    FOnExaminar: TExaminaASCIIEvent;
    FFormato: eFormatoASCII;
    FFormatoImpFecha: eFormatoImpFecha;
    FErrores: Integer;
    FPosicion: Integer;
    FUsaCommaText: Boolean;
    FSeparador: Char;
    FListaColumnas: TStringList;
    {
    function GetAsciiDelimitedValue(const sData: String; const iPos: Word): String;
    function GetToken(const Value: String; const Token: Char; const Index: Word): String;
    }
    procedure Init;
    procedure BuildDataSetResultado;
    procedure ValidaData(const sCampo, sDatos: String; const Info: TColumnaASCII);
  public
    { Public declarations }
    property OnValidar: TValidaASCIIEvent read FOnValidar write FOnValidar;
    property OnExaminar: TExaminaASCIIEvent read FOnExaminar write FOnExaminar;
    property Formato: eFormatoASCII read FFormato write FFormato;
    property FormatoImpFecha: eFormatoImpFecha read FFormatoImpFecha write FFormatoImpFecha;
    property Errores: Integer read FErrores write FErrores;
    property UsaCommaText: Boolean read FUsaCommaText write FUsaCommaText;
    property Separador: Char read FSeparador write FSeparador;
    procedure AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer;
                             const lObligatorio: Boolean = False; const iDecimal: Integer = 0;
                             const lPunto: Boolean = True; const sDefault: String = '' );
    function Procesa( const Datos: OleVariant ): OleVariant;
  end;

var
  oSuperASCII: TdmSuperASCII;

implementation

uses ZetaCommonTools, ZetaServerTools, ZetaCommonClasses;

{$R *.DFM}

{ TdmSuperASCII }

procedure TdmSuperASCII.DataModuleCreate(Sender: TObject);
begin
     Init;
end;

procedure TdmSuperASCII.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaColumnas );
end;

procedure TdmSuperASCII.Init;
begin
     cdsResultado.InitTempDataset;
     Formato := faASCIIFijo;
     FFormatoImpFecha := ifDDMMYYYYs;
     FUsaCommaText := TRUE;
     FSeparador := ',';
     FListaColumnas := TStringList.Create;
     FPosicion:= 1;   // Primer Columna a Leer del ASCII ( Aplica para SDF ), quiz�s a futuro se pueda publicar para iniciar la lectura en otra posicion
     Errores:= 0;
end;

procedure TdmSuperASCII.AgregaColumna( const sCampo: String; const eTipo: eTipoGlobal; const iLongitud: Integer;
                                       const lObligatorio: Boolean = False; const iDecimal: Integer = 0;
                                       const lPunto: Boolean = True; const sDefault: String = '' );
var
   oObjeto : TColumnaASCII;
begin
     // Agrega Campo al DataSet
     with cdsResultado do
     begin
          case eTipo of
               tgBooleano : AddBooleanField( sCampo );
               tgFloat : AddFloatField( sCampo );
               tgNumero : AddIntegerField( sCampo );
               tgFecha : AddDateField( sCampo );
               tgBlob: AddBlobField(sCampo, iLongitud );
               tgMemo: AddMemoField(sCampo, iLongitud);
          else
               AddStringField( sCampo, iLongitud );
          end;
     end;
     // Agrega Datos al StringList
     oObjeto:= TColumnaASCII.Create;
     with oObjeto do
     begin
          Tipo := eTipo;
          Posicion := Self.FPosicion;
          Longitud := iLongitud;
          Obligatorio := lObligatorio;
          if lPunto then
             Decimal  := 0            // Si est� explicito, se leera la cantidad tal y como aparece
          else
             Decimal  := iDecimal;
          Default := sDefault;
     end;
     FListaColumnas.AddObject( sCampo, oObjeto );
     Self.FPosicion := Self.FPosicion + iLongitud;
end;

function TdmSuperASCII.Procesa( const Datos: OleVariant ): OleVariant;
var
   sData, sDatos, sErrormsg: String;
   i, iRenglon, iProblemas: Integer;
   Columna: TColumnaASCII;
   FRenglon: TStringList;

   procedure SetRenglonDelimitado( const sData: String );
   var
      iPos : Integer;
      sRenglon, sColumna : String;
   begin
        sRenglon := sData;
        FRenglon.Clear;
        Repeat
              iPos := Pos( FSeparador, sRenglon );
              if ( iPos > 0 ) then
                 sColumna := Copy( sRenglon, 1, iPos - 1 )
              else
                 sColumna := sRenglon;
              sRenglon := Copy( sRenglon, iPos + 1, Length( sRenglon ) );
              FRenglon.Add( sColumna );
        Until ( iPos = 0 );
   end;

begin
     cdsASCII.Data := Datos;
     BuildDataSetResultado;
     iRenglon := 0;
     FRenglon := TStringList.Create;
     try
        with cdsResultado do
        begin
             while not cdsASCII.Eof do
             begin
                  sDatos:= cdsASCII.FieldByName( 'RENGLON' ).AsString;
                  if ZetaCommonTools.StrLleno( sDatos ) then
                  begin
                       if Assigned( FOnExaminar ) then
                          FOnExaminar( sDatos );
                       iProblemas := 0;
                       sErrormsg := '';
                       Inc( iRenglon );
                       Append;
                       FieldByName( 'RENGLON' ).AsInteger := iRenglon;
                       if ( Formato = faASCIIDel ) then
                       begin
                            if FUsaCommaText then
                               FRenglon.CommaText := sDatos
                            else
                            begin
                                 FRenglon := ZetaCommonTools.ParseCSVLine(sDatos, FRenglon);
                                 //SetRenglonDelimitado( sDatos );
                            end;
                       end;
                       try
                          for i := 0 to ( FListaColumnas.Count - 1 ) do
                          begin
                               with FListaColumnas do
                               begin
                                    Columna := TColumnaASCII( Objects[ i ] );
                                    // Validaciones Sencillas de Columna y Tipo de Dato
                                    case FFormato of
                                         faASCIIFijo:
                                         begin
                                              with Columna do
                                              begin
                                                   sData := Copy( sDatos, Posicion, Longitud );
                                              end;
                                         end;
                                         faASCIIDel:
                                         begin
                                              with FRenglon do
                                              begin
                                                   if ( i < Count ) then
                                                      sData := Strings[ i ]
                                                   else
                                                       sData := '';
                                              end;
                                         end;
                                    else
                                        sData := '';
                                    end;
                                    ValidaData( Strings[ i ], sData, Columna );
                               end;

                          end;
                          if Assigned( FOnValidar ) then
                          begin
                               //Validaciones de L�gica de Negocios
                               FOnValidar( cdsResultado, iProblemas, sErrorMsg );
                          end;
                       except
                             on Error: Exception do
                             begin
                                  sErrorMsg := Error.Message;
                                  iProblemas := 1;
                             end;
                       end;
                       if ( iProblemas > 0 ) then
                          Inc( FErrores );
                       FieldByName( 'NUM_ERROR' ).AsInteger := iProblemas;
                       FieldByName( 'DESC_ERROR' ).AsString := sErrorMsg;
                       Post;
                  end;
                  cdsASCII.Next;
             end;
        end;
        Result := cdsResultado.Data;
     finally
            FreeAndNil( FRenglon );
     end;
end;

{ *************** METODOS Y FUNCIONES PRIVADOS ******************** }

procedure TdmSuperASCII.ValidaData( const sCampo, sDatos: String; const Info: TColumnaASCII );
var
   sData: String;
   dFecha: TDate;

procedure ValidaVacio(const sData, sCampo: String );
begin
     if ZetaCommonTools.StrVacio( sData ) then
        raise Exception.Create( 'Columna ' + sCampo + ' No puede Quedar Vacia' );
end;

function UsaDefault( const sData, sDefault: String ): String;
begin
     if ZetaCommonTools.StrLleno( Trim( sData ) ) then
        Result := sData
     else
         Result := sDefault;
end;

begin
     sData := sDatos;
     with cdsResultado, Info do
     begin
          case Tipo of
               tgBooleano:
               begin
                    sData := Trim( sData );
                    if Obligatorio then
                       ValidaVacio( sData, sCampo )
                    else
                        sData := UsaDefault( sData, Default );
                    FieldByName( sCampo ).AsBoolean := ZetaCommonTools.zStrToBool( UpperCase( sData ) );
               end;
               tgFloat:
               begin
                    sData := Trim( sData );
                    if Obligatorio then
                       ValidaVacio( sData, sCampo )
                    else
                        sData := UsaDefault( sData, Default );
                    if not StrVacio( sData ) then
                    begin
                         try
                            if ( FFormato = faASCIIFijo ) then
                               FieldByName( sCampo ).AsFloat := StrToFloat( sData ) / Power( 10, Decimal )
                            else
                                FieldByName( sCampo ).AsFloat := StrToFloat( sData );
                         except
                               raise Exception.Create( 'Columna ' + sCampo + ' No es un N�mero Flotante: ' + sData );
                         end;
                    end;
               end;
               tgFecha:
               begin
                    if Obligatorio then
                       ValidaVacio( sData, sCampo )
                    else
                        sData := UsaDefault( sData, Default );
                    if not StrVacio( sData ) then
                    begin
                         try
                            dFecha := ZetaCommonTools.GetFechaImportada( sData, FFormatoImpFecha );
{
                            if ( FFormato = faASCIIFijo ) then
                               dFecha := CTOD( sData )
                            else
                                dFecha := GetImportedDate( sData );
}
                            if ( dFecha = NullDateTime ) then
                               raise Exception.Create( 'Columna ' + sCampo + ' No es una Fecha V�lida: ' + sData );
                            FieldByName( sCampo ).AsDateTime := dFecha;
                         except
                            raise Exception.Create( 'Columna ' + sCampo + ' No es una Fecha: ' + sData );
                         end;
                    end;
               end;
               tgNumero:
               begin
                    sData := Trim( sData );
                    if Obligatorio then
                       ValidaVacio( sData, sCampo )
                    else
                        sData := UsaDefault( sData, Default );
                    if not StrVacio( sData ) then
                    begin
                         try
                            FieldByName( sCampo ).AsInteger := StrToInt( sData );
                         except
                               raise Exception.Create( 'Columna ' + sCampo + ' No es un N�mero Entero: ' + sData );
                         end;
                    end;
               end;
          else
              begin
                   sData := Trim( sData );
                   if Obligatorio then
                      ValidaVacio( sData, sCampo )
                   else
                       sData := UsaDefault( sData, Default );
                   if not StrVacio( sData ) then
                   begin
                        try
                           FieldByName( sCampo ).AsString := sData;
                        except
                              raise Exception.Create( 'Columna ' + sCampo + ' No es un Texto V�lido: ' + sData );
                        end;
                   end;
              end;
          end;
     end;
end;

procedure TdmSuperASCII.BuildDataSetResultado;
begin
     with cdsResultado do
     begin
          // Agrega Columnas Fijas
          AddIntegerField( 'RENGLON' );
          AddIntegerField( 'NUM_ERROR' );
          AddStringField( 'DESC_ERROR', 150 );
          CreateTempDataSet;
     end;
end;

{
function TdmSuperASCII.GetToken( const Value: String; const Token: Char; const Index: Word ): String;
var
   i, iPos: Word;
   lFound: Boolean;
begin
     i := 0;
     Result := Value;
     repeat
           iPos := Pos( Token, Result );
           i := i + 1;
           lFound := ( iPos <= 0 ) or ( i = Index );
           if lFound then
           begin
                if ( iPos > 0 ) then
                   Result := Copy( Result, 1, ( iPos - 1 ) )
                else if ( i < Index ) then
                   Result := VACIO;
           end
           else
               Result := Copy( Result, ( iPos + 1 ), Length( Result ) - iPos );
     until lFound;
end;

function TdmSuperASCII.GetAsciiDelimitedValue( const sData: String; const iPos: Word ): String;
const
     K_STRING_DEL = '"';
var
   i: Integer;
begin
     Result := GetToken( sData, ',', iPos );
     i := Length( Result );
     if ( i > 0 ) and ( Result[ 1 ] = K_STRING_DEL ) then
     begin
          Result := Copy( Result, 2, ( i - 1 ) );
          i := Length( Result );
          if ( i > 0 ) and ( Result[ i ] = K_STRING_DEL ) then
             Result := Copy( Result, 1, ( i - 1 ) );
     end;
end;
}

end.
