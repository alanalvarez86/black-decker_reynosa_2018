unit ZetaSQLBroker;

interface

{$DEFINE VALIDAEMPLEADOSGLOBAL}
{$DEFINE DEBUGSENTINEL}
{$IFDEF MSSQL}
  {$IFDEF SERVERCAFE}
    {$DEFINE SENTINELVIRTUAL}
  {$ENDIF}
  // Se comenta por la mejora a la Admon de BD {$define SENTINELVIRTUAL}
  // Ahora solo aplica para CAFETERA
{$ENDIF}

uses
  Windows, SysUtils, Classes, ZetaTipoEntidad, ZetaCommonLists, ZetaCommonClasses, ZAgenteSQL,
  ZSuperSQL, ZCreator, DSuperReporte,
  {$IFDEF VALIDAEMPLEADOSGLOBAL}
  ZetaLicenseMgr,
  {$ENDIF}
  DZetaServerProvider,
  {$IFDEF PROFILE}ZetaProfiler, {$ENDIF}
  FAutoServer, FSentinel;

type

  {$IFDEF VALIDAEMPLEADOSGLOBAL}
   { ESTO VA PARA OTRA UNIDAD }

  TCalculoEmpleadosThread = class(TThread)
  private
    { Private declarations }
    FZetaProvider: TdmZetaServerProvider;
    oLicenseMgr: TLicenseMgr;
    oAutoServer: TAutoServer;
  public
    termino: Boolean;
    property Provider: TdmZetaServerProvider read FZetaProvider write FZetaProvider;

  protected
    procedure InitValues;
    procedure FreeValues;
    procedure Execute; override;
  end;

  TCalculoEmpleadosThreadHandler = class(TObject)
  private
    lDebeCalcular: Boolean;
  public
    procedure OnSentinelTrigger(Sender: TObject);
    procedure EjecutarCalculo;
  end;

  {$ENDIF}

  TSQLBroker = class(TObject)
  private
    { Private declarations }
    FCreator: TZetaCreator;
    FAgente: TSQLAgente;
    FSuperSQL: TSuperSQL;
    FSuperReporte: TdmSuperReporte;
    function GetProvider: TdmZetaServerProvider;
    function GetHayFiltroCondicion: Boolean;
  public
    { Public declarations }
    constructor Create(oCreator: TZetaCreator);
    destructor Destroy; override;
    property oZetaProvider: TdmZetaServerProvider read GetProvider;
    property Agente: TSQLAgente read FAgente;
    property SuperSQL: TSuperSQL read FSuperSQL;
    property SuperReporte: TdmSuperReporte read FSuperReporte;
    property HayFiltroCondicion: Boolean read GetHayFiltroCondicion;
    function BuildDataset(const Empresa: OleVariant): Boolean;
    function GetTablaRangoLista(const sValue: String; const eEntidad: TipoEntidad): String;
    procedure AgregaRangoCondicion(Parametros: TZetaParams);
    procedure Init(const eEntidad: TipoEntidad);
    procedure FiltroConfidencial(const Empresa: OleVariant);
    {$ifdef Timbrado}
    procedure FiltroConfidencialTransferidos(const Empresa: OleVariant);
    {$ENDIF}
  end;

  {$IFDEF SENTINELVIRTUAL}

function AutorizaSentinelVirtual: Boolean;
{$ENDIF}
function GetAutoServer: TAutoServer;
procedure ReadAutoServer(oAuto: TAutoServer; DoGetData, DoSetData: TAutoData);
procedure FreeAutoServer(oAuto: TAutoServer);

function GetSQLBroker(oCreator: TZetaCreator): TSQLBroker;
function GetZetaCreator(oProvider: TdmZetaServerProvider): TZetaCreator;
procedure FreeSQLBroker(oBroker: TSQLBroker);
procedure FreeZetaCreator(oCreator: TZetaCreator);
{$IFDEF DOS_CAPAS}
procedure FreeAll(const lShutdown: Boolean = FALSE);

var
  oZetaCreator: TZetaCreator;
  oZetaSQLBroker: TSQLBroker;
  oAutoServer: TAutoServer;
  {$ENDIF}

implementation

uses
  FAutoClasses,
  {$IFDEF SENTINELVIRTUAL}
  FSentinelRegistry,
  {$ENDIF}
  ZetaServerTools, ZetaCommonTools;

function GetSQLBroker(oCreator: TZetaCreator): TSQLBroker;
begin
  {$IFDEF DOS_CAPAS}
  if not Assigned(oZetaSQLBroker) then
    oZetaSQLBroker := TSQLBroker.Create(oCreator);
  Result := oZetaSQLBroker;
  {$ELSE}
  Result := TSQLBroker.Create(oCreator);
  {$ENDIF}
end;

function GetZetaCreator(oProvider: TdmZetaServerProvider): TZetaCreator;
begin
  {$IFDEF DOS_CAPAS}
  if not Assigned(oZetaCreator) then
    oZetaCreator := TZetaCreator.Create(oProvider);
  Result := oZetaCreator;
  {$ELSE}
  Result := TZetaCreator.Create(oProvider);
  {$ENDIF}
end;

procedure FreeSQLBroker(oBroker: TSQLBroker);
begin
  {$IFNDEF DOS_CAPAS}
  if Assigned(oBroker) then
    oBroker.Free;
  {$ENDIF}
end;

procedure FreeZetaCreator(oCreator: TZetaCreator);
begin
  {$IFNDEF DOS_CAPAS}
  if Assigned(oCreator) then
    oCreator.Free;
  {$ENDIF}
end;

{$IFDEF DOS_CAPAS}

procedure FreeAll(const lShutdown: Boolean);
begin
  if lShutdown then
    FreeAndNil(oAutoServer);
  FreeAndNil(oZetaSQLBroker);
  FreeAndNil(oZetaCreator);
end;
{$ENDIF}

function GetAutoServer: TAutoServer;
begin
  {$IFDEF DOS_CAPAS}
  if not Assigned(oAutoServer) then
    oAutoServer := TAutoServer.Create;
  Result := oAutoServer;
  {$ELSE}
  Result := TAutoServer.Create;
  Result.AppType := atCorporativa;
  {$ENDIF}
  {$IFDEF INTERBASE}
  Result.SQLType := engInterbase;
  {$ENDIF}
  {$IFDEF MSSQL}
  Result.SQLType := engMSSQL;
  {$ENDIF}
end;

{$IFDEF SENTINELVIRTUAL}

{ CR Manejo de Sentinel Virtual }
function AutorizaSentinelVirtual: Boolean;
var
  oRegistrySentinel: TSentinelRegistryServer;
  oSentinelServer: TSentinelServer;
  sClave1, sClave2: String;
begin
  Result := FALSE;
  oRegistrySentinel := TSentinelRegistryServer.Create(FALSE);
  try
    if StrLleno(oRegistrySentinel.ArchivoLicencia) and FileExists(oRegistrySentinel.ArchivoLicencia) and FSentinelRegistry.CheckComputerInfo then begin  //@(am): Cambio validacion licencia
      try
        oSentinelServer := TSentinelServer.Create;

              with oSentinelServer do
              begin
                   SentinelLoad( oRegistrySentinel.Empleados, oRegistrySentinel.Usuarios, oRegistrySentinel.Modulos,oRegistrySentinel.Plataforma  );
                   sClave1 := oRegistrySentinel.Clave1;
                   sClave2 := oRegistrySentinel.Clave2;

          if SentinelTest(sClave1, sClave2) then begin
            Result := SentinelWrite;
                        {
                        if Result then
                        begin
                             //EventLog( Format( 'Autorizaci�n Renovada %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                             Result := True;
                        end
                        else
                        begin
                            // ErrorLog( Format( 'Autorizaci�n NO FUE Renovada %s', [ FormatDateTime( 'dd/MMM/yyyy hh:nn AM/PM', Now ) ] ) );
                            Result := False;
                        end; }
          end else begin
                        // ErrorLog( 'Error En Autorizaci�n: ' + StatusMsg );
            Result := FALSE;
          end;
        end;

      finally
        FreeAndNil(oSentinelServer);
      end;
    end;
  finally
    FreeAndNil(oRegistrySentinel);
  end;
end;
{$ENDIF}

procedure ReadAutoServer(oAuto: TAutoServer; DoGetData, DoSetData: TAutoData);
{$IFDEF VALIDAEMPLEADOSGLOBAL}
var
  oCalculoEmpleados: TCalculoEmpleadosThreadHandler;
  {$IFDEF PROFILE}
  oProfiler: TZetaProfiler;
  {$ENDIF}
  {$ENDIF}
begin

  {$IFDEF PROFILE} oProfiler := TZetaProfiler.Create;
  oProfiler.Start;
  oProfiler.Check('ReadAutoServer');
  Randomize;
  {$ENDIF}
  {$IFDEF VALIDAEMPLEADOSGLOBAL}
  oCalculoEmpleados := TCalculoEmpleadosThreadHandler.Create;
  {$ENDIF}
  with oAuto do begin
    {$IFDEF DOS_CAPAS}
    if not Loaded and Assigned(DoGetData) and Assigned(DoSetData) then begin
      OnGetData := DoGetData;
      OnSetData := DoSetData;
      {$IFDEF VALIDAEMPLEADOSGLOBAL}
      OnGetDataAdditional := oCalculoEmpleados.OnSentinelTrigger;
      {$ENDIF}
      Autorizar;
      {$IFDEF VALIDAEMPLEADOSGLOBAL}
      OnGetDataAdditional := nil;
      {$ENDIF}
    end;
    {$ELSE}
    {$IFDEF DEBUGSENTINEL}
        // Se comenta para que re-autorice Sentinel
    if { not Loaded and } Assigned(DoGetData) and Assigned(DoSetData) then begin
      OnGetData := DoGetData;
      OnSetData := DoSetData;
      {$IFDEF VALIDAEMPLEADOSGLOBAL}
      OnGetDataAdditional := oCalculoEmpleados.OnSentinelTrigger;
      {$ENDIF}
      Autorizar;
      OnGetData := nil;
      OnSetData := nil;
      OnGetDataAdditional := nil;
    end
    else
      {$ENDIF}
      Cargar;

    {$IFDEF SENTINELVIRTUAL}
{ Condiciones para revisar el Sentinel Virtual
0. Solo aplica para Version Corporativa 2012 MSSQL o version reciente
1. No exista sentinel f�sico o est� en modo Demo
2. Exista metodo para Tomar Informaci�n de la BD Comparte - Claves  ( DoGetData )
//3 Exista metodo para Modificar Informaci�n de la BD Comparte - Claves ( DoSetData )
4. Exista el archivo de licencia
5. La licencia sea valida para el Servidor
6. El usuario con el que cual est� registrado el componente pueda leer del Registry

Nota: No arrojar� errores de bit�cora solo seguir� en demo


}
    if ((not SentinelOK) or (oAuto.EsDemo)) and Assigned(DoGetData) { and Assigned( DoSetData ) }
    then begin
               // Se crea una instancia aparte para manipular los datos de Sentinel
      AutorizaSentinelVirtual;
      OnGetData := DoGetData;
      OnSetData := DoSetData;
      Autorizar;
      OnGetData := nil;
      OnSetData := nil;
    end;
    {$ENDIF}
    {$ENDIF}
  end;

  {$IFDEF VALIDAEMPLEADOSGLOBAL}
  oCalculoEmpleados.EjecutarCalculo;
  {$ENDIF}
  {$IFDEF PROFILE}
  oProfiler.Check('ReadAutoServer');
  oProfiler.Report(Format('C:\TEMP\ReadAutoServer_%d_%d.LOG', [Random(1000), Random(100000)]));
  FreeAndNil(oProfiler);
  {$ENDIF}
end;

procedure FreeAutoServer(oAuto: TAutoServer);
begin
  {$IFNDEF DOS_CAPAS}
  if Assigned(oAuto) then
    oAuto.Free;
  {$ENDIF}
end;

{ ******* TSQLBroker ******** }

constructor TSQLBroker.Create(oCreator: TZetaCreator);
var
  I: Integer;
begin
  FCreator := oCreator;
  with FCreator do begin
    PreparaEntidades;
  end;

  FAgente := TSQLAgente.Create;
  FAgente.Entidades := FCreator.dmEntidadesTress;
  FSuperSQL := TSuperSQL.Create(FCreator);
  {$IFDEF SERVERCAFE}
  FSuperReporte := TdmSuperReporte.Create(nil);
  {$ELSE}
  FSuperReporte := TdmSuperReporte.Create(oCreator);
  {$ENDIF}
end;

destructor TSQLBroker.Destroy;
begin
  FreeAndNil(FSuperReporte);
  FreeAndNil(FSuperSQL);
  FreeAndNil(FAgente);
end;

function TSQLBroker.GetProvider: TdmZetaServerProvider;
begin
  Result := FCreator.oZetaProvider;
end;

function TSQLBroker.GetTablaRangoLista(const sValue: String; const eEntidad: TipoEntidad): String;
begin
  Result := sValue;
  if (sValue <> '') then
    Result := StrTransAll(sValue, ARROBA_TABLA, FCreator.dmEntidadesTress.NombreEntidad(eEntidad))
end;

procedure TSQLBroker.AgregaRangoCondicion(Parametros: TZetaParams);

  procedure AddFilter(const sValue: String; const lDiccionario: Boolean;
    const eEntidad: TipoEntidad);
  begin
    if (sValue <> '') then begin
      FAgente.AgregaFiltro(sValue, lDiccionario, eEntidad);
    end;
  end;

begin
  with Parametros do begin
    AddFilter(GetTablaRangoLista(ParamByName('RangoLista').AsString, FAgente.Entidad), True,
      FAgente.Entidad);
    AddFilter(ParamByName('Condicion').AsString, FALSE, enNinguno);
    AddFilter(ParamByName('Filtro').AsString, FALSE, enNinguno);
  end;
end;

function TSQLBroker.BuildDataset(const Empresa: OleVariant): Boolean;
var
  sErrores: String;
  i: Integer;
begin
     { if FSuperSQL.Construye( FAgente ) then
        Result := FSuperReporte.Construye( FAgente )
     else
     begin
          sErrores := '';
          with FAgente do
          begin
               for i := 0 to ( NumErrores - 1 ) do
               begin
                    sErrores := sErrores + ListaErrores[ i ] + CR_LF;
               end;
          end;
          raise Exception.Create( sErrores );
     end; }
  if FSuperSQL.Construye(FAgente) then begin
    Result := FSuperReporte.Construye(FAgente);
    if (not Result) then begin
      sErrores := '';
      with FAgente do begin
        for i := 0 to (NumErrores - 1) do begin
          if (ListaErrores[i] <> K_NoHayRegistros) then
          // Si no existen registros no hay que reportar error
            sErrores := sErrores + ListaErrores[i] + CR_LF;
        end;
        if StrLleno(sErrores) then begin
          if (oZetaProvider.Log <> nil) then
          // Si est� dentro del proceso, se reporta el error en bitacora
            oZetaProvider.Log.Error(0, 'Error al evaluar f�rmulas', sErrores + CR_LF + SQL.Text)
          else
            raise Exception.Create(sErrores);
        end;
      end;
    end;
  end else begin
    sErrores := '';
    with FAgente do begin
      for i := 0 to (NumErrores - 1) do begin
        sErrores := sErrores + ListaErrores[i] + CR_LF;
      end;
    end;
    raise Exception.Create(sErrores);
  end;
{
     if not Result then
     begin
          sErrores := '';
          with FAgente do
          begin
               for i := 0 to ( NumErrores - 1 ) do
               begin
                    if ( ListaErrores[ i ] <> K_NoHayRegistros ) then      // Si no existen registros no hay que reportar error
                       sErrores := sErrores + ListaErrores[ i ] + CR_LF;
               end;
               if ( SQL.Count > 0 ) then
               begin
                    oZetaProvider.EscribeBitacora( tbError, clbEvalFormulas, 0, Now, 'Error al evaluar f�rmulas', sErrores + CR_LF + SQL.Text );
               end;
          end;
          if strLleno( sErrores ) then
             raise Exception.Create( sErrores );
     end;
}
end;

procedure TSQLBroker.Init(const eEntidad: TipoEntidad);
begin
  with FAgente do begin
    Clear;
    Entidad := eEntidad;
  end;
end;

{$IFDEF Timbrado}
procedure TSQLBroker.FiltroConfidencialTransferidos(const Empresa: OleVariant);
{$IFDEF TRESS}
var
  sConfi: String;
  {$ENDIF}
begin
  {$IFDEF TRESS}
  sConfi := GetFiltroNivel0(Empresa, 'V_EMP_TIMB');
  if (sConfi <> '') then
    FAgente.AgregaFiltro(sConfi, True, enEmpTimb);
  {$ENDIF}
end;
{$ENDIF}




procedure TSQLBroker.FiltroConfidencial(const Empresa: OleVariant);
{$IFDEF TRESS}
var
  sConfi: String;
  {$ENDIF}
begin
  {$IFDEF TRESS}
  sConfi := GetFiltroNivel0(Empresa, 'COLABORA');
  if (sConfi <> '') then
    FAgente.AgregaFiltro(sConfi, True, enEmpleado);
  {$ENDIF}
end;

function TSQLBroker.GetHayFiltroCondicion: Boolean;
begin
  with oZetaProvider.ParamList do begin
    Result := StrLleno(ParamByName('RangoLista').AsString) OR
      StrLleno(ParamByName('Condicion').AsString) OR StrLleno(ParamByName('Filtro').AsString);
  end;
end;

{$IFDEF VALIDAEMPLEADOSGLOBAL}
{ TCalculoEmpleadosThread }

procedure TCalculoEmpleadosThread.Execute;
begin
  inherited;
  InitValues;

  try
    oLicenseMgr.EmpleadosGetGlobal;
    with oAutoServer do begin
      OnGetData := oLicenseMgr.AutoGetData;
      OnSetData := oLicenseMgr.AutoSetData;
      OnGetDataAdditional := oLicenseMgr.AutoOnGetDataAdditional;
      EscribirAutorizacion;
      OnGetData := nil;
      OnSetData := nil;
      OnGetDataAdditional := nil;
    end;
  finally
  end;
  FreeValues;
end;

procedure TCalculoEmpleadosThread.InitValues;
begin
  FZetaProvider := TdmZetaServerProvider.Create(nil);
  FZetaProvider.EmpresaActiva := FZetaProvider.Comparte;
  oLicenseMgr := TLicenseMgr.Create(FZetaProvider);
  oAutoServer := ZetaSQLBroker.GetAutoServer;
end;

procedure TCalculoEmpleadosThread.FreeValues;
begin
  FreeAndNil(FZetaProvider);
  FreeAndNil(oLicenseMgr);
  {$IFNDEF DOS_CAPAS}
  FreeAndNil(oAutoServer);
  {$ENDIF}
end;
{$ENDIF}
{$IFDEF VALIDAEMPLEADOSGLOBAL}

{ TCalculoEmpleadosThreadHandler }
procedure TCalculoEmpleadosThreadHandler.EjecutarCalculo;
var
  oCalculoEmpleados: TCalculoEmpleadosThread;
begin
  if (lDebeCalcular) then begin
    oCalculoEmpleados := TCalculoEmpleadosThread.Create(True);
    oCalculoEmpleados.Resume;
  end;
end;

procedure TCalculoEmpleadosThreadHandler.OnSentinelTrigger(Sender: TObject);
begin
  lDebeCalcular := True;
end;
{$ENDIF}

end.
