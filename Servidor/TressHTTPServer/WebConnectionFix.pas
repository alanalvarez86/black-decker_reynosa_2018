unit WebConnectionFix;

interface

 TWebConnection = class(TStreamedConnection, ITransport)
  ...
  protected
     procedure SetConnected(Value: Boolean); override;  // Add this

procedure TWebConnection.SetConnected(Value: Boolean);
var
  i : integer;
begin
  if Value = Connected then Exit;

  if not Value then
  begin
    with TDataBlockInterpreter(FInterpreter) do
      for i := FDispatchList.Count - 1 downto 0 do
      begin
        FInterpreter.CallFreeObject(TDataDispatch(FDispatchList[i]).DispatchIndex);
        FInterpreter.RemoveDispatch(TDataDispatch(FDispatchList[i]));
      end;
  end;
  inherited;
end;



implementation

end.
 