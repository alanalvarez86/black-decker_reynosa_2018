{!$define TEST_ADVERTENCIA_FECHA}
unit ZetaLicenseClasses;

interface

uses Windows, Messages, SysUtils, Classes,Graphics, Controls, Forms, Dialogs,DB,
     Variants,
     ZetaCommonClasses, ZetaCommonLists,  FAutoServer, FAutoClasses;

const
   K_MAX_CONFIG_ARRAY = 9;
   K_INTERVALO_BD_LIMITE = 10;
   K_VALORES_DB = 6;
   K_VALORES_REG = 7;
   K_UMBRAL_LICENCIA  = 0.95 ; //A partir del 95% empieza advertir
   K_UMBRAL_LICENCIA_BD  = 0.80 ; //A partir del 80% empieza advertir

   K_LIC_MSG_ADVIERTE_UMBRAL = 'Se ha detectado un n�mero de empleados activos cercano al l�mite de la cantidad de empleados autorizados para procesar en Sistema TRESS.' + CR_LF +
                  'Est� al %d%% de la cantidad  de empleados activos autorizados.'+ CR_LF +
                  'A partir del %s al sobrepasar la cantidad autorizada la captura de empleados y el c�lculo de n�mina se suspender� en Sistema TRESS.';

   K_LIC_MSG_REBASA_ADVIERTE =
   'Se ha detectado un mayor numero de empleados activos a los autorizados para procesar en Sistema TRESS.' + CR_LF +
   'Favor de contactar a Grupo Tress Internacional' + CR_LF +
   'http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx  ' + CR_LF +
   'o a su distribuidor autorizado. ' + CR_LF +
   'El %s la captura de empleados y el c�lculo de n�mina se suspender� en Sistema TRESS.';

   K_LIC_MSG_REBASA_ADVIERTE_DEMO =
   'Se ha detectado un mayor numero de empleados activos a los autorizados para procesar en Sistema TRESS en Modo DEMO.' + CR_LF +
   'Favor de contactar a Grupo Tress Internacional' + CR_LF +
   'http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx  ' + CR_LF +
   'o a su distribuidor autorizado. ' + CR_LF +
   'El c�lculo de n�mina podr� operarse sin restricci�n sin embargo la captura de empleados est� suspendida.';


   K_LIC_MSG_REBASA_RESTRINGE =
   'Se ha detectado un mayor numero de empleados activos a los autorizados para procesar en Sistema TRESS.' + CR_LF +
   'La captura de empleados y el c�lculo de n�mina est� suspendida en Sistema TRESS.' + CR_LF +
   'Favor de contactar a Grupo Tress Internacional' + CR_LF +
   'http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx ' + CR_LF +
   'o a su distribuidor autorizado.';

   K_LIC_MSG_REBASA_RESTRINGE_DEMO =
   'El Sistema TRESS est� en Modo DEMO.' + CR_LF +
   'La finalizaci�n de la captura de empleados est� suspendida en Sistema TRESS.' + CR_LF +
   'Favor de contactar a Grupo Tress Internacional' + CR_LF +
   'http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx ' + CR_LF +
   'o a su distribuidor autorizado.';

   K_LIC_MSG_PRUEBAS_RESTRINGE = 'En las Empresas de Tipo Pruebas Tress No Est� Permitido Registrar Altas de Empleados o Reingresos';

   // K_LIC_MSG_REBASA_BD_ADVIERTE = 'Se ha detectado que est�n por agotarse el n�mero de altas permitidas de Bases de Datos de Producci�n (%d de %d)';
   K_LIC_MSG_REBASA_BD_ADVIERTE = 'Se ha detectado que est�n por agotarse el n�mero de altas permitidas de bases de datos de tipo Tress (%d de %d)';

type




  TGlobalLicenseValues = class (TObject)
  private
    FTotalGlobal : integer;
    FTipoCompany  : eTipoCompany;
    FFechaActual : TDate;
    FAutoServer: TAutoServer;
    // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
    FMostrarAdvertencia: Boolean;
    {$ifdef UNITTESTING}
    FUTEsDemo: Boolean;
    {$endif}
  public
    constructor Create( oAutoServer :TAutoServer );
    destructor Destroy; override;
    function DebeAdvertir : boolean;
    function DebeValidar : boolean;
    function GetFechaInicioAdvertencia : TDate;
    function GetGroupID : integer;
    function GetFechaZero : TDate;
    function GetFechaInicioValidacion : TDate;
    function GetMesesRelativos : integer; virtual;
    function GetMaxConfigBD : integer; virtual;
    function GetNoSentinel : integer; virtual;
    function GetMaxEmpleados : integer; virtual;
    function GetEsDemo : boolean; virtual;
    //  eEvaluaOperacion = ( evOpAlta, evOpNomina , evOpLogin );

    function EvaluarEmpleados( operacion : eEvaluaOperacion )  : eEvaluaTotalEmpleados; virtual;
    // US #11012 Opcion para obtener status de licencia de uso.
    function EvaluarEmpleadosPantallaBD: eEvaluaTotalEmpleados; virtual;

    function EvaluarEmpleadosMensaje (  operacion : eEvaluaOperacion ; var sMensajeAdvertencia, sMensajeError : string) : eEvaluaTotalEmpleados;  virtual;
    // US #11012 Opcion para obtener status de licencia de uso.
    function EvaluarEmpleadosMensajePantallaBD ( var sMensajeAdvertencia, sMensajeError : string) : eEvaluaTotalEmpleados;  virtual;

    function VarValues: OleVariant;
    property FechaActual: TDate read FFechaActual write FFechaActual;
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
    property TotalGlobal: integer read FTotalGlobal write FTotalGlobal;
    property TipoCompany: eTipoCompany read FTipoCompany write FTipoCompany;
    // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
    property MostrarAdvertencia: Boolean read FMostrarAdvertencia write FMostrarAdvertencia;

    {$ifdef UNITTESTING}
    property UTEsDemo: Boolean read FUTEsDemo write FUTEsDemo;
    {$endif}

  end;

  TClientGlobalLicenseValues = class (TGlobalLicenseValues)
  private
         FTotalGlobal : integer;
         FTipoCompany  : eTipoCompany;
         FMesesRelativos : integer;
         FMaxConfigBD : integer;
         FNoSentinel : integer;
         FMaxEmpleados : integer;
  public
    constructor Create( VarValues: OleVariant); overload;
    constructor Create( oGlobalLicenseValues : TGlobalLicenseValues;  iTotalGlobal : integer; tipoCompany : eTipoCompany  ); overload;
    destructor Destroy; override;
    function GetMesesRelativos : integer; override;
    function GetMaxConfigBD : integer; override;
    function GetNoSentinel : integer; override;
    function GetMaxEmpleados : integer; override;
    function GetEsDemo : boolean; override;
    function EvaluarEmpleadosCliente ( operacion : eEvaluaOperacion; var sMensajeAdvertencia, sMensajeError : string) : eEvaluaTotalEmpleados;
  end;


  TDbConfigRec = record
    ModN : integer;
    Fecha : TDate;
    Sentinel : Integer;
    Cantidad : Integer;
    Primera : TDate;
    Ultima : TDate;
    R1 : string;   //<-- Comparte Checksum = TDbConfigManager.SimpleChecksum( StoreHandler.ReadRegistryComparteBD )
    CambioComparte : integer;
    V2 : integer;   //<-- Valor dummy
    encryptedValue : string;
    encryptedRegValue : string;
  end;

  IDBConfigStoreHandler = Interface (IInterface)
     function EsDbConfigInicial : boolean;
     function ReadDBConfigFromDatabase( iKey : integer) : string;
     function WriteDBConfigFromDatabase( iKey : integer; encryptedValue : string ) : boolean;
     function ReadRegistryComparteBD : string;
     function ReadRegistryChecksum : string;
     function WriteRegistryChecksum( encryptedValue : string ) : boolean;
  end;


  TDbConfigManager = class(TObject)
  private
     { Private declarations }
     FGlobalLicenseValues : TGlobalLicenseValues;
     FAutoServer: TAutoServer;
     FComparteInicial : string;
     FComparteInicialChecksum : string;
     FComparteID : string;
     FStoreHandler : IDBConfigStoreHandler;
     FDbConfigArray : array [0..K_MAX_CONFIG_ARRAY] of TDbconfigRec;
     FLoadOK : boolean;
     procedure SetEncryptedFromValues( iKey : integer);
     procedure SetValuesFromEncrypted( iKey : integer; lFromReg : boolean);
     procedure InitDbConfig;
     procedure MaximizeDbConfig;
     procedure ProcesarCambioComparte;
     function GetEsInicial : boolean;
     function GetTramaBD(iKey: integer): string;
     function TramaToTDbConfigRec( sTrama : string; lFromReg : boolean ) : TDbConfigRec;
     function GetSimpleCheckSum ( sTexto : string; lZeroBased : boolean ) :  Int64;
     function GetComparteChecksum : string;
     function GenerateComparteID : string;
     function GetComparteID : string;

  protected
     { Protected declarations }
     function LoadDBConfigArray: boolean; virtual;
     function LoadRegistryConfig(var  regDbConfigRec : TDbConfigRec ): boolean;  virtual;
     function CopyRegistryConfigArray: boolean;  virtual;
     function GetSentinelKeyDB : integer;

  public
     { Public declarations }
     constructor Create(globalLicenseValues : TGlobalLicenseValues; oStoreHandler :IDBConfigStoreHandler);
     destructor Destroy; override;
     procedure LoadDbConfig;
     function GetDbConfigRec : TDbConfigRec;
     function PuedeAgregarDB : boolean;
     function AdvertirAgregarDB(var sMensajeAdv : string)  : boolean;
     function IncrementarDB : boolean;
     function ReiniciarDB : boolean;
     function SaveDbConfig : boolean;

     property AutoServer: TAutoServer read FAutoServer write FAutoServer;
     property EsInicial : boolean read GetEsInicial;
     property StoreHandler : IDBConfigStoreHandler read FStoreHandler write FStoreHandler;
end;


function GetIntervaloLimiteBDConfig( valor: integer) : integer;

implementation

uses ZetaCommonTools,
     ZetaServerTools;



function GetIntervaloLimiteBDConfig( valor: integer) : integer;
begin
    Result := (valor+1)*K_INTERVALO_BD_LIMITE;
end;


{ TGlobalLicenseValues }
constructor TGlobalLicenseValues.Create( oAutoServer :TAutoServer );
begin
     Self.FAutoServer := oAutoServer;

     if ( Self.FAutoServer <> nil ) then
     begin
         if  Self.FAutoServer.Loaded then
         begin
             FFechaActual := Date;
{             if ( Date >   Self.FAutoServer.Caducidad  - 1  ) then
                 FFechaActual := Date
              else
                  FFechaACtual :=    Self.FAutoServer.Caducidad - 1;}
         end
         else
         begin
              FFechaActual := Date;
         end;
     end;
end;

function TGlobalLicenseValues.DebeAdvertir: boolean;
begin
     Result := ( FFechaActual >= GetFechaInicioAdvertencia );
end;

function TGlobalLicenseValues.DebeValidar: boolean;
begin
     Result := ( FFechaActual >= GetFechaInicioValidacion );
end;

destructor TGlobalLicenseValues.Destroy;
begin

  inherited;
end;

function TGlobalLicenseValues.GetEsDemo: boolean;
begin
     Result :=  {$ifndef UNITTESTING} ( AutoServer.EsDemo ){$else} ( Self.UTEsDEMO )  {$endif}  ;
end;

function TGlobalLicenseValues.GetFechaInicioAdvertencia: TDate;
begin
     Result := IncMonth( GetFechaInicioValidacion, -1);
end;

//DATO DE FECHA INICIAL. DE AQUI DEPENDE EL +++  de los meses
function TGlobalLicenseValues.GetFechaZero: TDate;
begin
    Result :=  EncodeDate( 2013, 07, 01);
end;

function TGlobalLicenseValues.GetFechaInicioValidacion: TDate;
begin
{$ifdef TEST_RESTRINGE_FECHA}
     Result :=  EncodeDate( 2012, 07, 01);
{$else}
   {$ifdef TEST_ADVERTENCIA_FECHA}
        Result :=  EncodeDate( 2012, 06, 01);
   {$else}
        Result :=  IncMonth( GetFechaZero , GetMesesRelativos ) ;
   {$endif}
{$endif}
end;

function TGlobalLicenseValues.GetGroupID: integer;
begin
     Result := GetNoSentinel mod 10;
end;

function TGlobalLicenseValues.GetMaxConfigBD: integer;
begin
     Result := GetIntervaloLimiteBDConfig(  AutoServer.LimiteConfigBD );
end;

function TGlobalLicenseValues.GetMesesRelativos: integer;
begin
   Result := AutoServer.IniciaValidacion;
   {$ifdef UNITTESTING}
   Result := 0;
   {$endif}

end;

function TGlobalLicenseValues.GetNoSentinel: integer;
begin
    Result := AutoServer.NumeroSerie;
   {$ifdef UNITTESTING}
   Result := 99999;
   {$endif}
end;


function TGlobalLicenseValues.GetMaxEmpleados: integer;
begin
{$ifdef TEST_ADVERTENCIA_FECHA}
    Result := 31;
{$else}
    Result := AutoServer.Empleados;
{$endif}
end;

///( evIgnorar, evDebajoUmbral, evEnUmbral, evRebasaAdvierte, evRebasaRestringe )
function TGlobalLicenseValues.EvaluarEmpleados(operacion : eEvaluaOperacion) : eEvaluaTotalEmpleados;
var
  iEmpleadosUmbral  : integer;
  iMaxEmpleados : integer;
begin
     iMaxEmpleados :=  GetMaxEmpleados;
     iEmpleadosUmbral :=  Trunc(  K_UMBRAL_LICENCIA *  iMaxEmpleados  ) ;

     Result := evIgnorar;

     if ( Self.DebeAdvertir ) or ( Self.DebeValidar ) then
     begin
          if ( TotalGlobal > GetMaxEmpleados ) and ( Self.DebeAdvertir ) and ( not Self.DebeValidar ) then
             Result := evRebasaAdvierte
          else
          if ( TotalGlobal > GetMaxEmpleados ) and ( Self.DebeValidar ) then
             Result := evRebasaRestringe
          else
          if ( TotalGlobal >= iEmpleadosUmbral ) and ( TotalGlobal <= GetMaxEmpleados ) then
             Result := evEnUmbral
          else
          if ( TotalGlobal < GetMaxEmpleados ) then
             Result := evDebajoUmbral;

          //eTipoCompany = ( tc3Datos, tcRecluta, tcVisitas, tcOtro, tc3Prueba, tcPresupuesto );

          if (Result = evRebasaRestringe ) and ( TipoCompany = tcPresupuesto) then
             Result := evRebasaAdvierte;

          if (Result = evRebasaRestringe ) and ( operacion = evOpNomina ) and GetEsDemo then
	     Result := evRebasaAdvierteDemo;

          if (Result = evRebasaRestringe ) and ( operacion = evOpAlta ) and  GetEsDemo  then
	     Result := evRebasaRestringeDemo;

     end;


     if ( Self.DebeValidar ) and ( TipoCompany = tc3Prueba ) and ( operacion = evOpAlta ) then
     begin
          Result := evPruebasRestringe;
     end;

end;

// US #11012 Opcion para obtener status de licencia de uso.
function TGlobalLicenseValues.EvaluarEmpleadosPantallaBD : eEvaluaTotalEmpleados;
var
  iEmpleadosUmbral  : integer;
  iMaxEmpleados : integer;
begin
     iMaxEmpleados :=  GetMaxEmpleados;
     iEmpleadosUmbral :=  Trunc(  K_UMBRAL_LICENCIA *  iMaxEmpleados  ) ;

     Result := evIgnorar;

     if ( Self.DebeAdvertir ) or ( Self.DebeValidar ) then
     begin
          if ( TotalGlobal > GetMaxEmpleados ) and ( Self.DebeAdvertir ) and ( not Self.DebeValidar ) then
             Result := evRebasaAdvierte
          else
          if ( TotalGlobal > GetMaxEmpleados ) and ( Self.DebeValidar ) then
             Result := evRebasaRestringe
          else
          if ( TotalGlobal >= iEmpleadosUmbral ) and ( TotalGlobal <= GetMaxEmpleados ) then
             Result := evEnUmbral
          else
          if ( TotalGlobal < GetMaxEmpleados ) then
             Result := evDebajoUmbral;
     end;
end;



function TGlobalLicenseValues.EvaluarEmpleadosMensaje(operacion : eEvaluaOperacion; var  sMensajeAdvertencia,  sMensajeError: string): eEvaluaTotalEmpleados;
var
 iMaximo, iPorciento : integer;
 sFechaInicioValidacion : string;
begin
     sMensajeAdvertencia := VACIO;
     sMensajeError := VACIO;

     Result  := EvaluarEmpleados(operacion);

     iMaximo := GetMaxEmpleados;
     if ( iMaximo  >  0 ) then
         iPorciento :=   Trunc( ( FTotalGlobal / iMaximo ) * 100.00 )
     else
         iPorciento := 0;

     sFechaInicioValidacion := FechaLarga( GetFechaInicioValidacion );

     case Result of
         // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
         evEnUmbral :  if MostrarAdvertencia then sMensajeAdvertencia := Format( K_LIC_MSG_ADVIERTE_UMBRAL, [ iPorciento, sFechaInicioValidacion ] );
         evRebasaAdvierte : if MostrarAdvertencia then  sMensajeAdvertencia := Format( K_LIC_MSG_REBASA_ADVIERTE, [sFechaInicioValidacion] );
         evRebasaAdvierteDemo :if MostrarAdvertencia then  sMensajeAdvertencia := K_LIC_MSG_REBASA_ADVIERTE_DEMO;
         evRebasaRestringe : sMensajeError := K_LIC_MSG_REBASA_RESTRINGE ;
         evPruebasRestringe : sMensajeError := K_LIC_MSG_PRUEBAS_RESTRINGE;
         evRebasaRestringeDemo : sMensajeError := K_LIC_MSG_REBASA_RESTRINGE_DEMO ;
     end;

end;

// US #11012 Opcion para obtener status de licencia de uso.
function TGlobalLicenseValues.EvaluarEmpleadosMensajePantallaBD (var  sMensajeAdvertencia,  sMensajeError: string): eEvaluaTotalEmpleados;
const
     K_LIC_MSG_ADVIERTE_UMBRAL_PANTALLA_BD = 'Se ha detectado que el total de empleados activos est� cercano al l�mite de empleados autorizados en su licencia de uso de Sistema TRESS.' + CR_LF +
     'Est� al %d%% del l�mite de empleados activos autorizados.' + CR_LF +
     'Al sobrepasar la cantidad autorizada la captura de empleados y el c�lculo de n�mina se suspender�n en Sistema TRESS.';

     K_LIC_MSG_REBASA_ADVIERTE_PANTALLA_BD =
    'Se ha detectado que la cantidad de empleados activos excede del l�mite autorizado en su licencia de uso de Sistema TRESS.' + CR_LF +
    'Favor de contactar a Grupo Tress Internacional' + CR_LF +
    'http://www.tress.com.mx/esp/SOMOS/PRESENCIA/SUCURSALESDEGTI/tabid/102/Default.aspx  ' + CR_LF +
    'o a su distribuidor autorizado. ' + CR_LF +
    'El %s la captura de empleados y el c�lculo de n�mina se suspender� en Sistema TRESS.';

     K_LIC_MSG_REBASA_RESTRINGE_PANTALLA_BD =
    'Se ha detectado que la cantidad de empleados activos excede del l�mite autorizado en su licencia de uso de Sistema TRESS.' + CR_LF +
    'La captura de empleados y el c�lculo de n�mina est� suspendida en Sistema TRESS.' + CR_LF +
    'Favor de contactar a Grupo Tress Internacional' + CR_LF +
    'http://www.tress.com.mx/en-us/somos/cont%C3%A1ctanos.aspx' + CR_LF +
    'o a su distribuidor autorizado.';

var
 iMaximo, iPorciento : integer;
 sFechaInicioValidacion : string;
begin
     sMensajeAdvertencia := VACIO;
     sMensajeError := VACIO;

     Result  := EvaluarEmpleadosPantallaBD;

     iMaximo := GetMaxEmpleados;
     if ( iMaximo  >  0 ) then
         iPorciento :=   Trunc( ( FTotalGlobal / iMaximo ) * 100.00 )
     else
         iPorciento := 0;

     sFechaInicioValidacion := FechaLarga( GetFechaInicioValidacion );

     case Result of
         // US #9666 Desactivar/Activar advertencia de Validaci�n licencia empleados por medio de un global de sistema.
         evEnUmbral :  sMensajeAdvertencia := Format( K_LIC_MSG_ADVIERTE_UMBRAL_PANTALLA_BD, [ iPorciento ] );
         evRebasaAdvierte : sMensajeAdvertencia := Format( K_LIC_MSG_REBASA_ADVIERTE_PANTALLA_BD, [sFechaInicioValidacion] );
         evRebasaRestringe : sMensajeError := K_LIC_MSG_REBASA_RESTRINGE_PANTALLA_BD;
     end;

end;


function TGlobalLicenseValues.VarValues: OleVariant;
var
  oParams : TZetaParams;
begin
     oParams := TZetaParams.Create;
     oParams.AddInteger('TotalGlobal', TotalGlobal );
     oParams.AddInteger('TipoCompany', Ord( TipoCompany )  );
     oParams.AddDate('FechaActual', FechaActual );
     oParams.AddInteger('MesesRelativos', GetMesesRelativos);
     oParams.AddInteger('MaxConfigBD', GetMaxConfigBD );
     oParams.AddInteger('MaxEmpleados', GetMaxEmpleados );
     oParams.AddInteger('NoSentinel', GetNoSentinel  );
     Result := oParams.VarValues;
     FreeAndNil( oParams );
end;

{ TDbConfigManager }

constructor TDbConfigManager.Create(globalLicenseValues : TGlobalLicenseValues; oStoreHandler : IDBConfigStoreHandler);
begin
     FLoadOK := False;
     FComparteID := VACIO;
     FGlobalLicenseValues :=  globalLicenseValues;
     Self.StoreHandler := oStoreHandler;
     FComparteInicialChecksum := VACIO;
end;

destructor TDbConfigManager.Destroy;
begin
  inherited;
end;

function   TDbConfigManager.GetTramaBD(iKey : integer) : string;
const
// MODX|FECHA|SENTINEL|CANTIDAD|PRIMERA|ULTIMA|CAMBIOSCOMPARTE|(Comparte simple checksum)
     K_TRAMA_KEY_DB = '%d|%d|%d|%d|%d|%d|%d|%s' ;
begin
     with FDbConfigArray[iKey] do
     begin
          Result := Format( K_TRAMA_KEY_DB, [ ModN , Trunc( Double( Fecha )), Sentinel, Cantidad, Trunc( Double( Primera )),  Trunc( Double( Ultima )),  CambioComparte,  R1 ] );
     end;
end;

procedure TDbConfigManager.SetEncryptedFromValues( iKey : integer);

var
   sTrama : string;
begin
     sTrama := GetTramaBD(iKey);
     FDbConfigArray[iKey].encryptedValue := EncryptDB( sTrama );
     Randomize;
     sTrama := Format('%s|%d', [sTrama, Random( 256)]);
     FDbConfigArray[iKey].encryptedRegValue := EncryptDB( sTrama );
end;


procedure TDbConfigManager.SetValuesFromEncrypted( iKey : integer; lFromReg : boolean);
var
   sTrama : string;
begin
    if ( lFromReg ) then
       sTrama  := DecryptDB( FDbConfigArray[iKey].encryptedRegValue )
    else
       sTrama  := DecryptDB( FDbConfigArray[iKey].encryptedValue );

    FDbConfigArray[iKey] := TramaToTDbConfigRec( sTrama, lFromReg );

end;

procedure TDbConfigManager.InitDbConfig;
var
   dFecha : TDate;
   iKey : integer;
begin
    dFecha := Date;

    FComparteID := GenerateComparteID;

    for iKey:=0 to K_MAX_CONFIG_ARRAY do
    begin
          with FDbConfigArray[iKey] do
          begin
               ModN := iKey;
               Fecha := dFecha;
               Sentinel := FGlobalLicenseValues.GetNoSentinel;
               Cantidad := 0;
               Primera := dFecha;
               Ultima :=dFecha;
               CambioComparte := 0;
               R1 := FComparteID;
          end;
    end;


end;

procedure TDBConfigManager.MaximizeDbConfig;
var
   iKey : integer;
   dFecha : TDate;
begin
    dFecha := Date;
    for iKey:=0 to K_MAX_CONFIG_ARRAY do
    begin
          with FDbConfigArray[iKey] do
          begin
               ModN := iKey;
               Fecha := dFecha;
               Sentinel := FGlobalLicenseValues.GetNoSentinel;
               Cantidad := FGlobalLicenseValues.GetMaxConfigBD;   // AutoServer.Intentos
               Primera := dFecha;
               Ultima :=dFecha;
               CambioComparte := 0;
               R1 := Self.GenerateComparteID;
          end;

          //FDbConfigArray[ GetSentinelKeyDB ].Cantidad :=  50; // AutoServer.Intentos;
    end;
end;

procedure TDbConfigManager.LoadDbConfig;
var
 iKey : integer;
 lOK : boolean;
 countMax : integer;
 countMaxCambioK : integer;
 regDbConfigRec : TDbConfigRec;
begin
   lOk := True;
   countMax := 0;
   countMaxCambioK := 0;
   if ( StoreHandler <> nil ) then
      FComparteInicial := StoreHandler.ReadRegistryComparteBD;
   if not LoadDBConfigArray then
   begin
        lOK := CopyRegistryConfigArray;
   end;

   if( lOK ) then
   begin
       if LoadRegistryConfig( regDbConfigRec ) then
       begin
          countMax := regDbConfigRec.Cantidad;
          countMaxCambioK :=  regDbConfigRec.CambioComparte;
       end;

       for iKey:=0 to K_MAX_CONFIG_ARRAY do
       begin
            if FDbConfigArray[iKey].Cantidad > countMax then
               countMax := FDbConfigArray[iKey].Cantidad;

            if FDbConfigArray[iKey].CambioComparte  > countMaxCambioK then
               countMaxCambioK := FDbConfigArray[iKey].CambioComparte;

       end;

       FDbConfigArray[GetSentinelKeyDB].Cantidad := countMax;
       FDbConfigArray[GetSentinelKeyDB].CambioComparte := countMaxCambioK;

   end;

   if not lOK then
   begin
        if EsInicial then
        begin
            InitDbConfig;
            lOK := True;

        end
        else
        begin
             MaximizeDbConfig;
        end;
   end;


   FComparteInicialChecksum :=  GetComparteChecksum;

   FLoadOK := lOK;
end;

function TDbConfigManager.LoadDBConfigArray : Boolean;
var
   iKey : integer;
   sTrama : string;
begin
 if ( StoreHandler <> nil ) then
 begin
     Result := True;
     for iKey:=0 to K_MAX_CONFIG_ARRAY do
     begin
         sTrama := StoreHandler.ReadDBConfigFromDatabase( iKey );
         FDbConfigArray[iKey].encryptedValue := sTrama;
         SetValuesFromEncrypted( iKey, False);
         // Si no fue legible el valor fue alterado
         if ( FDbConfigArray[iKey].ModN <> iKey ) then
            Result := False;
     end;
 end
 else
     Result := False;
end;


function TDbConfigManager.LoadRegistryConfig( var  regDbConfigRec : TDbConfigRec ): boolean;
begin
   Result := False;

   if ( StoreHandler <> nil ) then
   begin
       regDbConfigRec := TramaToTDbConfigRec( DecryptDB( StoreHandler.ReadRegistryChecksum ), True);
       Result := regDbConfigRec.ModN >= 0;
   end;

end;


function TDbConfigManager.CopyRegistryConfigArray: boolean;
var
   regDbConfigRec : TDbConfigRec;
   iKey : integer;
   sTempComparteID : string;

begin
   Result := False;

   //Se tiene que generar un ID nuevo de comparte, ya que puede ser un cambio de comparte
   sTempComparteID := GenerateComparteID;

   if ( StoreHandler <> nil ) then
   begin
       regDbConfigRec := TramaToTDbConfigRec( DecryptDB( StoreHandler.ReadRegistryChecksum ) , True);
       Result := regDbConfigRec.ModN >= 0;

       if ( Result ) then
       begin
            for iKey:=0 to K_MAX_CONFIG_ARRAY do
            begin
                FDbConfigArray[iKey] := regDbConfigRec;
                FDbConfigArray[iKey].ModN := iKey;
                FDbConfigArray[iKey].R1 := sTempComparteID;
                //SetEncryptedFromValues( iKey );
           end;
       end;
   end;

end;


procedure TDbConfigManager.ProcesarCambioComparte;
var
  iKey : integer;

    function CambioComparte : boolean;
    var
       dbComparteID : string;
    begin
         dbComparteID := GetDbConfigRec.R1;

         Result := GetComparteID <> dbComparteID;
    end;

begin
   if ( CambioComparte ) and ( FGlobalLicenseValues.DebeValidar ) then
   begin
        iKey := GetSentinelKeyDB;

        Self.FComparteID := GenerateComparteID;
        FDbConfigArray[ iKey ].R1 := Self.FComparteID;
        FDbConfigArray[ iKey ].CambioComparte :=  FDbConfigArray[ iKey ].CambioComparte + 1;

        if ( Self.PuedeAgregarDB ) then
           FDbConfigArray[iKey].Cantidad :=  FDbConfigArray[ iKey ].Cantidad + 1 ;
   end;
end;

function TDbConfigManager.SaveDbConfig : boolean;
var
 iKey : integer;
 licDbConfigRec : TDbConfigRec;
{$ifdef DEBUG_KEYS}
 slKeys : TStringList;
{$endif}


begin
   Result := False;

   {$ifdef DEBUG_KEYS}
   slKeys := TStringList.Create;
   if (FileExists('C:\TEMP\KEYS.TXT') ) then
      slKeys.LoadFromFile('C:\TEMP\KEYS.TXT');
   slKeys.Add( DateTimeToStr( Self.FGlobalLicenseValues.FechaActual  ) );
   {$endif}

   ProcesarCambioComparte;
   licDbConfigRec := FDbConfigArray[GetSentinelKeyDB];

   for iKey:=0 to K_MAX_CONFIG_ARRAY do
   begin
        FDbConfigArray[iKey].R1 := licDbConfigRec.R1;
        FDbConfigArray[iKey].Cantidad := licDbConfigRec.Cantidad;
        FDbConfigArray[iKey].CambioComparte := licDbConfigRec.CambioComparte;
        FDbConfigArray[iKey].Ultima :=  licDbConfigRec.Ultima;
        SetEncryptedFromValues( iKey );
        if ( StoreHandler <> nil ) then
        begin
             Result :=  StoreHandler.WriteDBConfigFromDatabase(iKey, FDbConfigArray[iKey].encryptedValue );
             {$ifdef DEBUG_KEYS}
             slKeys.Add(Format('%d=%s',[ iKey, DecryptDB(FDbConfigArray[iKey].encryptedValue) ]))
             {$endif}
        end;
   end;

   if ( StoreHandler <> nil ) then
   begin
       Result := Result and  StoreHandler.WriteRegistryChecksum( FDbConfigArray[GetSentinelKeyDB].encryptedRegValue );
       {$ifdef DEBUG_KEYS}
       slKeys.Add(Format('Registro %d=%s',[ GetSentinelKeyDB, DecryptDB(FDbConfigArray[GetSentinelKeyDB].encryptedRegValue) ]))
       {$endif}

   end;
   {$ifdef DEBUG_KEYS}
    slKeys.SaveToFile('C:\TEMP\KEYS.TXT');
    FreeAndNil( slKeys );
   {$endif}
end;


function TDbConfigManager.GetSentinelKeyDB: integer;
begin
     Result := FGlobalLicenseValues.GetGroupID;
end;

function TDbConfigManager.TramaToTDbConfigRec( sTrama : string; lFromReg : boolean ) : TDbConfigRec;
var
   slItems : TStringList;
   dblFecha : Double;
   iCuantos : integer;
begin
    slItems := TStringList.Create;
    slItems.Delimiter := '|';
    slItems.DelimitedText := sTrama;


    with Result do
    begin
          ModN :=  -1;
    end;

    if ( lFromReg ) then
       iCuantos := 9
    else
        iCuantos := 8;


    if ( slItems.Count =  iCuantos ) then
    begin
          with Result do
          begin
               ModN :=  StrToIntDef( slItems[0], 0 );
               dblFecha := StrToFloatDef( slItems[1], 0);
               Fecha := TDateTime( dblFecha );
               Sentinel := StrToIntDef( slItems[2], 0 );
               Cantidad := StrToIntDef( slItems[3], 9999 );
               dblFecha := StrToFloatDef( slItems[4], 0);
               Primera := TDateTime( dblFecha );
               dblFecha := StrToFloatDef( slItems[5], 0);
               Ultima := TDateTime( dblFecha );
               CambioComparte := StrToIntDef( slItems[6], 9999);
               R1 :=  slItems[7];
          end;
    end;

    FreeAndNil ( slItems );

end;


function TDbConfigManager.GetDbConfigRec: TDbConfigRec;
begin
  Result := FDbConfigArray[ GetSentinelKeyDB ];
end;

function TDbConfigManager.IncrementarDB: boolean;
begin
    Result := True;
    if ( FGlobalLicenseValues.DebeValidar ) and (PuedeAgregarDB) then
    begin
       Inc ( FDbConfigArray[ GetSentinelKeyDB ].Cantidad );
       if ( Date >  FDbConfigArray[ GetSentinelKeyDB ].Ultima ) then
          FDbConfigArray[ GetSentinelKeyDB ].Ultima := Date;
    end;
end;

function TDbConfigManager.PuedeAgregarDB: boolean;
begin
     if ( FGlobalLicenseValues.DebeValidar ) then
     begin
     {$ifndef UNITTESTING}
          if FGlobalLicenseValues.AutoServer.EsDemo then
             Result := False
          else
     {$endif}
              Result := ( FDbConfigArray[ GetSentinelKeyDB ].Cantidad < FGlobalLicenseValues.GetMaxConfigBD {AutoServer.Intentos} )
     end
     else
         Result := True;
end;



function TDbConfigManager.ReiniciarDB: boolean;
begin
     Result := True;
     FDbConfigArray[ GetSentinelKeyDB ].Cantidad := 0;
end;



function TDbConfigManager.GetEsInicial: boolean;
begin
   if ( StoreHandler <> nil ) then
   begin
        Result := StoreHandler.EsDbConfigInicial;
   end
   else
       Result := False;

end;


function TDbConfigManager.GetSimpleCheckSum(sTexto : string; lZeroBased : boolean): Int64;
var
  i: integer;
  L: integer;
begin
  Result := 0;
  l := Length(sTexto);
  if l > 0 then
  begin
    for i := 1 to l do
      if lZeroBased then
        Inc(Result, Ord(sTexto[i]) - 65)
      else
         Inc(Result, Ord(sTexto[i]));
  end;
end;

function TDbConfigManager.GetComparteChecksum: string;
begin
   if ( StoreHandler <> nil ) then
   begin
        Result := IntToStr( GetSimpleCheckSum( StoreHandler.ReadRegistryComparteBD, True )  );
   end
   else
       Result := '0';

end;

function TDbConfigManager.GetComparteID: string;
var
   regDbConfigRec : TDbConfigRec;
begin
   Result := FComparteID;

   if LoadRegistryConfig( regDbConfigRec ) then
         Result := regDbConfigRec.R1;

end;

function TDbConfigManager.GenerateComparteID: string;
begin
     Randomize;
     Result := Format('%s%d', [GetComparteChecksum , Trunc( Double( Now )*1000000.00)]);
end;

function TDbConfigManager.AdvertirAgregarDB(
  var sMensajeAdv: string): boolean;
var
   iBDUmbral : integer;
begin
     sMensajeAdv := VACIO;
     if ( FGlobalLicenseValues.DebeValidar ) or (FGlobalLicenseValues.DebeAdvertir ) then
     begin
          if FGlobalLicenseValues.AutoServer.EsDemo then
             Result := False
          else
          begin
              iBDUmbral :=  Trunc(  K_UMBRAL_LICENCIA_BD *   FGlobalLicenseValues.GetMaxConfigBD  ) ;
              Result :=  (FDbConfigArray[ GetSentinelKeyDB ].Cantidad >= iBDUmbral )  and  ( FDbConfigArray[ GetSentinelKeyDB ].Cantidad  < FGlobalLicenseValues.GetMaxConfigBD );
              if ( Result ) then
                 sMensajeAdv :=  Format( K_LIC_MSG_REBASA_BD_ADVIERTE, [FDbConfigArray[ GetSentinelKeyDB ].Cantidad ,  FGlobalLicenseValues.GetMaxConfigBD] );
          end;
     end
     else
         Result := False;
end;

{ TClientGlobalLicenseValues }

constructor TClientGlobalLicenseValues.Create( VarValues: OleVariant );
var
   oZetaParams : TZetaParams;
begin
     oZetaParams := TZetaParams.Create;
     oZetaParams.VarValues := VarValues;

     Self.TotalGlobal := oZetaParams.ParamByName('TotalGlobal').AsInteger;
     Self.FTotalGlobal := Self.TotalGlobal;
     Self.TipoCompany := eTipoCompany( oZetaParams.ParamByName('TipoCompany').AsInteger );
     Self.FTipoCompany := Self.TipoCompany;
     Self.FechaActual := oZetaParams.ParamByName('FechaActual').AsDate;
     Self.FFechaActual := Self.FechaActual;
     Self.FMesesRelativos := oZetaParams.ParamByName('MesesRelativos').AsInteger;
     Self.FMaxConfigBD := oZetaParams.ParamByName('MaxConfigBD').AsInteger;
     Self.FMaxEmpleados := oZetaParams.ParamByName('MaxEmpleados').AsInteger;
     Self.FNoSentinel := oZetaParams.ParamByName('NoSentinel').AsInteger;

     FreeAndNil( oZetaParams );
end;


constructor TClientGlobalLicenseValues.Create(
  oGlobalLicenseValues: TGlobalLicenseValues;  iTotalGlobal : integer; tipoCompany : eTipoCompany );
begin
     Self.FTotalGlobal := iTotalGlobal;
     Self.TotalGlobal := iTotalGlobal;
     Self.FTipoCompany := tipoCompany;
     Self.TipoCompany := tipoCompany;
     Self.FechaActual := oGlobalLicenseValues.FechaActual;
     Self.FFechaActual := oGlobalLicenseValues.FechaActual;


     Self.FMesesRelativos := oGlobalLicenseValues.GetMesesRelativos;
     Self.FMaxConfigBD := oGlobalLicenseValues.GetMaxConfigBD;
     Self.FMaxEmpleados := oGlobalLicenseValues.GetMaxEmpleados;
     Self.FNoSentinel := oGlobalLicenseValues.GetNoSentinel;
end;

destructor TClientGlobalLicenseValues.Destroy;
begin
  inherited;
end;



function TClientGlobalLicenseValues.EvaluarEmpleadosCliente(  operacion : eEvaluaOperacion;
  var sMensajeAdvertencia, sMensajeError: string): eEvaluaTotalEmpleados;
begin
   Result := Self.EvaluarEmpleadosMensaje( operacion, sMensajeAdvertencia, sMensajeError );
end;

function TClientGlobalLicenseValues.GetEsDemo: boolean;
begin
     Result := Autorizacion.EsDemo;
end;

function TClientGlobalLicenseValues.GetMaxConfigBD: integer;
begin
     Result := FMaxConfigBD;
end;

function TClientGlobalLicenseValues.GetMaxEmpleados: integer;
begin
{$ifdef TEST_ADVERTENCIA_FECHA}
     Result := 31;
{$else}
     Result := Autorizacion.Empleados;
{$endif}
end;

function TClientGlobalLicenseValues.GetMesesRelativos: integer;
begin
     Result := FMesesRelativos;
end;

function TClientGlobalLicenseValues.GetNoSentinel: integer;
begin
     Result := Autorizacion.NumeroSerie;
end;

end.
