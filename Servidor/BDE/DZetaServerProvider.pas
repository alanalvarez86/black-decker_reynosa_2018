unit DZetaServerProvider;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Provider, DBClient,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaRegistryServer, DBTables;

type
  TQREvResultType = (resInt, resDouble, resString, resBool, resError);

  TQREvResult = record
    resFecha : Boolean;
    case Kind : TQREvResultType of
      resInt    : (intResult : longint);
      resDouble : (dblResult : double);
      resString : (strResult : string[255]);
      resBool   : (booResult : boolean);
  end;
type
    TZetaCursor = TDataset;
    TZetaField  = TField;
    TZetaCursorParam  = TParam;
    TZetaComando = TQuery;
type
  TZPQuery = TDataset;
  TZPUpdate = TQuery;
  TZPParam  = TParam;
  TZPConnection = TDatabase;
  eTipoQuery = (etqSelect, etqInsert, etqUpdate, etqDelete );
  EAdvertencia = class( Exception ); // Excepciones que se deben manejar como advertencias //
  TBitacoraHora = String[ 8 ];
  TBitacoraTexto = String[ 50 ];
  TLogStatus = String[ 1 ];
  TdmZetaServerProvider = class;
  TZetaLog = class( TObject )
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    FMaxSteps: Integer;
    FIncrement: Integer;
    FSteps: Integer;
    FCounter: Integer;
    FCancelado: Boolean;
    FErrorCount: Word;
    FEventCount: Word;
    FWarningCount: Word;
    FFolio: Integer;
    FInicio: TDate;
    FEmpleado: TNumEmp;
    FProceso: Procesos;
    FDataset: TZPQuery;
    FUpdate: TZPQuery;
    function Abierto: Boolean;
    procedure Init;
    procedure Conteo( const eTipo: eTipoBitacora );
    procedure Escribir( const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
    function GetStatus: TLogStatus;
    function GetEnumStatus: eProcessStatus;
  public
    { Public declarations }
    constructor Create( oProvider: TdmZetaServerProvider );
    function CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean;
    function CloseProcess: OleVariant;
    function OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer ): Boolean;
    procedure Cambio( const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; const sCampoEmpleado, sCampoFecha: String; Dataset: TDataSet );
    procedure CancelProcess( const iFolio: Integer );
    procedure Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto ); overload;
    procedure Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String ); overload;
    procedure Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto ); overload;
    procedure Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto : String ); overload;
    procedure Error( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto : String );
    procedure ErrorGrave( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto );
    procedure Excepcion( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception );
    function HayErrores: Boolean;
  end;
  TTablaInfo = class(TObject)
  private
    FTabla : String;
    FListaCampos : TStringList;
    FListaKey : TStringList;
    FListaOrder : TStringList;
    FListaForeignKey : TStringList;
    FSQL : TStrings;
    FParams : TParams;
    FFiltro : String;
    FOnUpdateData : TProviderDataEvent;
    FBeforeUpdateRecord : TBeforeUpdateRecordEvent;
    FAfterUpdateRecord : TAfterUpdateRecordEvent;
    FOnUpdateError : TResolverErrorEvent;
    function  ConstruyeQuery( const eTipo : eTipoQuery; Delta : TDataset; qryCambios : TZPUpdate ) : String;
    function  ConstruyeSelectSQL : String;
    function  ConstruyeUpdateSQL( const UpdateKind : TUpdateKind; Delta : TDataset; qryCambios : TZPUpdate ) : String;
  public
    constructor Create;
    destructor  Destroy; override;
    procedure SetCampos( const sCampos : String );
    procedure SetKey( const sCampos : String );
    procedure SetOrder( const sCampos : String );
    procedure SetForeignKey( const sCampos : String );
    procedure SetInfo( const sTabla, sCampos, sKey : String );
    procedure SetInfoDetail( const sTabla, sCampos, sKey, sForeignKey : String );
    property  Filtro : String read FFiltro write FFiltro;
    property  OnUpdateData : TProviderDataEvent read FOnUpdateData write FOnUpdateData;
    property  BeforeUpdateRecord : TBeforeUpdateRecordEvent read FBeforeUpdateRecord write FBeforeUpdateRecord;
    property  AfterUpdateRecord : TAfterUpdateRecordEvent read FAfterUpdateRecord write FAfterUpdateRecord;
    property  OnUpdateError : TResolverErrorEvent read FOnUpdateError write FOnUpdateError;
    property  SQL : TStrings read FSQL;
  end;
  TdmZetaServerProvider = class(TDataModule)
    prvUnico: TDataSetProvider;
    bdeEmpresa: TDatabase;
    bdeComparte: TDatabase;
    Session1: TSession;
    qryMaster: TQuery;
    qryCambios: TQuery;
    procedure dmZetaProviderCreate(Sender: TObject);
    procedure dmZetaProviderDestroy(Sender: TObject);
    procedure prvUnicoUpdateData(Sender: TObject; DataSet: TClientDataSet);
    procedure prvUnicoBeforeUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure prvUnicoGetData(Sender: TObject; DataSet: TClientDataSet);
    procedure prvUnicoAfterUpdateRecord(Sender: TObject;
      SourceDS: TDataSet; DeltaDS: TClientDataSet;
      UpdateKind: TUpdateKind);
    procedure prvUnicoUpdateError(Sender: TObject; DataSet: TClientDataSet;
      E: EUpdateError; UpdateKind: TUpdateKind;
      var Response: TResolverResponse);
  private
    { Private declarations }
    FTablaInfo : TTablaInfo;
    FDetailInfo : TTablaInfo;
    FRegistry: TZetaRegistryServer;
    FRecsOut : Integer;
    FLog: TZetaLog;
    FQueryGlobal : TZPQuery;
    FEmpresa : Variant;
    FConnection : TZPConnection;
    FParamList : TZetaParams;
    { Valores Activos }
    FDatosPeriodo: TDatosPeriodo;
    FPeriodoObtenido: Boolean;
    FDatosImss: TDatosImss;
    FYearDefault: Integer;
    FMesDefault: Integer;
    FEmpleadoActivo: Integer;
    FNombreUsuario, FCodigoEmpresa: String;
    FFechaDefault: TDate;
    FFechaAsistencia: TDate;
    FVerConfidencial: Boolean;
    FChecadaComida: TDatosComida;
    FDatosCalendario: TCalendario;
    function ConectaBaseDeDatos( const Empresa: Variant ): TZPConnection;
    function GetParametroPos(Dataset: TZPQuery;const iPos : Integer): TZPParam;
    procedure CreateLog;
    procedure ReleaseLog;
    procedure PreparaTabla;
    { Grabar Tablas con Joins }
    procedure UpdateTabla(Sender: TObject; SourceDS: TDataSet;
      DeltaDS: TClientDataSet; UpdateKind: TUpdateKind;
      var Applied: Boolean);
    procedure ConectaEmpresa( const Empresa: Variant );
    { Globales }
    function LeeGlobal( const iCodigo : Integer ) : String;
    { Empresas }
    function GetComparte: Variant;
    procedure SetEmpresa( Empresa : Variant );
    function GetUsuario: Integer;
    function GetDatasetInfo(Dataset: TDataset; const lBorrar : Boolean ): String;
  public
    { Public declarations }
    function GetMasterDetail( Empresa : Variant ) : OleVariant;
    function GetTabla( Empresa : Variant ) : OLEVariant;
    function GrabaMasterDetail( Empresa : Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    function GrabaTabla( Empresa : Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    function GrabaTablaGrid( Empresa : Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
    procedure ExecSQL( Empresa : Variant; const sSQL : String );
    function  OpenSQL( Empresa : Variant; const sSQL : String; const lDatos : Boolean ) : OleVariant;
    { Master y Detail }
    property TablaInfo : TTablaInfo read FTablaInfo;
    property DetailInfo : TTablaInfo read FDetailInfo;
    property RecsOut : Integer read FRecsOut;
    { Bitacora }
    property Log: TZetaLog read FLog;
    function OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer ): Boolean;
    function CanContinue: Boolean; overload;
    function CanContinue( const iEmpleado: TNumEmp ): Boolean; overload;
    function CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean; overload;
    function CloseProcess: OleVariant;
    procedure AbreBitacora;
    procedure CancelProcess( const iFolio: Integer );
    { F�brica de Querys }
    function  CreateQuery : TZPQuery; overload;
    function  CreateQueryLocate : TZPQuery;
    function  CreateQuery( const sScript: String ) : TZPQuery; overload;
    procedure PreparaQuery( Query: TZPQuery; const sScript: String );
    function  CreateStoredProcedure : TDataSet;
    procedure PreparaStoredProcedure( StoredProc : TDataSet; const sProcedureName: String );
    function  Ejecuta(Dataset: TZPQuery) : Integer;
    function  AbreQueryScript( Dataset : TZPQuery; const sScript: String ) : Boolean;
    procedure ParamAsInteger( Dataset: TZPQuery; const sName: String; const iValue: Integer );
    procedure ParamAsFloat( Dataset: TZPQuery; const sName: String; const rValue: Extended );
    procedure ParamAsDate( Dataset: TZPQuery; const sName: String; const dValue: TDate );
    procedure ParamAsString( Dataset: TZPQuery; const sName, sValue: String );
    procedure ParamAsVarChar( Dataset: TZPQuery; const sName, sValue: String; const iAncho: Integer );
    procedure ParamAsChar(Dataset: TZPQuery; const sName, sValue: String; const iAncho: Integer );
    procedure ParamAsBlob( Dataset: TZPQuery; const sName: String; const sValue: String);
    procedure ParamAsBoolean( Dataset: TZPQuery; const sName: String; const lValue: Boolean );
    procedure ParamVariant( Dataset: TZPQuery; const iPos: Integer; const Valor: Variant );
    // function  Params( DataSet : TZPQuery ) : TParams;
    { GetGlobales }
    procedure InitGlobales;
    function  GetGlobalBooleano( const iCodigo: Integer ): Boolean;
    function  GetGlobalString( const iCodigo: Integer ): String;
    function  GetGlobalInteger( const iCodigo: Integer ): Integer;
    function  GetGlobalReal( const iCodigo: Integer ): Real;
    function  GetGlobalDate( const iCodigo: Integer ): TDateTime;
    { Empresas }
    property Comparte: Variant read GetComparte;
    property EmpresaActiva : Variant read FEmpresa write SetEmpresa;
    property UsuarioActivo : Integer read GetUsuario;
    property ParamList : TZetaParams read FParamList;
    function AliasComparte : String;
    procedure AsignaDataSetParams( oDataSet : TZPQuery );
    procedure AsignaDataSetOtros( oDataSet : TZPQuery; oListaPares : Variant );
    procedure AsignaParamsDataSet( oDataSetParams : TZPQuery; oDataSetValores : TDataset );
    { Valores Activos }
    property DatosPeriodo : TDatosPeriodo read FDatosPeriodo;
    property DatosImss : TDatosImss read FDatosImss;
    property DatosComida : TDatosComida read FChecadaComida write FChecadaComida;
    property DatosCalendario : TCalendario read FDatosCalendario; 
    property FechaAsistencia : TDate read FFechaAsistencia;
    property FechaDefault : TDate read FFechaDefault;
    property YearDefault : Integer read FYearDefault write FYearDefault;
    property MesDefault : Integer read FMesDefault write FMesDefault;
    property EmpleadoActivo : Integer read FEmpleadoActivo;
    property NombreUsuario : String read FNombreUsuario;
    property CodigoEmpresa : String read FCodigoEmpresa;

    property VerConfidencial : Boolean read FVerConfidencial write FVerConfidencial;
    procedure GetDatosPeriodo(const lForzar : Boolean=FALSE);
    procedure GetDatosImss;
    procedure GetDatosActivos;
    procedure GetEncabPeriodo;

    procedure AsignaParamList( oParams : OleVariant );
    function  DescripcionParams : String;

    procedure EmpiezaTransaccion;
    procedure TerminaTransaccion( lCommit : Boolean );
    procedure RollBackTransaccion;

    procedure ActualizaTabla(OrigenDS: TDataSet; UpdateKind: TUpdateKind);
    procedure CambioCatalogo( const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; Dataset: TDataSet );
    procedure BorraCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; Dataset: TDataSet);
    procedure EscribeBitacora(const eTipo: eTipoBitacora;
                const eClase: eClaseBitacora; const iEmpleado: TNumEmp;
                dMovimiento: TDate; const sMensaje: TBitacoraTexto;
                const sTexto: String);
  end;

function PK_Violation( E : Exception ) : Boolean;
function GetEmptyProcessResult( eProceso: Procesos ): OleVariant;
procedure SetProcessOK( var Resultado: OleVariant );
procedure SetProcessError( var Resultado: OleVariant );
function  CursorDelCampo( oCampo : TZetaField ) : TZetaCursor;
function TipoCampo( oCampo : TField ) : eTipoGlobal;
function  AsignaResultado( oCampo: TField): TQREvResult;

implementation

uses ZetaCommonTools,
     ZetaServerTools;

const
     K_ANCHO_LOG_HORA = 8;
     K_ANCHO_LOG_TEXTO = 50;

{$R *.DFM}

function TipoCampo( oCampo : TField ) : eTipoGlobal;
begin
    case oCampo.DataType of
         ftString: Result := tgTexto;
         ftSmallint, ftInteger, ftWord: Result := tgNumero;
         ftBoolean: Result := tgBooleano;
         ftFloat, ftCurrency, ftBCD, ftTime : Result := tgFloat;
         ftDate,ftDateTime: Result := tgFecha;
         else Result := tgAutomatico;
    end;
end;

function AsignaResultado( oCampo : TField ) : TQREvResult;
begin
     with oCampo, Result do
     begin
          case TipoCampo( oCampo ) of
               tgTexto:
               begin
                    Kind := resString;
                    strResult := AsString;
               end;
               tgNumero:
               begin
                    Kind := resInt;
                    intResult := AsInteger;
               end;
               tgBooleano:
               begin
                    Kind := resBool;
                    booResult := AsBoolean;
               end;

               tgFloat :
               begin
                    Kind := resDouble;
                    resFecha  := FALSE;
                    dblResult := AsFloat;
               end;
               tgFecha:
               begin
                    Kind := resDouble;
                    resFecha := TRUE;
                    dblResult := AsFloat;
               end
          else
              Kind := resError;
          end;
     end;
end;

function  CursorDelCampo( oCampo : TZetaField ) : TZetaCursor;
begin
    Result := oCampo.DataSet;
end;

function TimeToStrSQL( const dValue: TDateTime ): String;
begin
     Result := FormatDateTime( 'hh:nn:ss', dValue );
end;


function PK_Violation( E : Exception ) : Boolean;
begin
    Result := (  Pos( 'VIOLATION OF PRIMARY', UpperCase( E.Message ) > 0 );
end;

procedure AsignaParamCampo(oParam: TZPParam; oCampo: TField);
begin
     with oParam do
     begin
       Assign( oCampo );
       if DataType = ftBlob then
       begin
            DataType := ftString;
            Value    := oCampo.AsString;
       end
       else if DataType = ftString then
            DataType := ftFixedChar;
     end;
end;

function GetExceptionInfo( Error: Exception ): String;
begin
     Result := Error.Message;
end;

function GetExceptionLogType( Problema: Exception ): eTipoBitacora;
begin
     if ( Problema is EAdvertencia ) then
        Result := tbAdvertencia
     else
         Result := tbErrorGrave;
end;

function GetEmptyProcessResult( eProceso: Procesos ): OleVariant;
begin
     Result := VarArrayOf( [ 0, Ord( epsEjecutando ), Ord( eProceso ), 0, 0, 0, Now, Now, 0, 0, 0 ] );
end;

procedure SetProcessOK( var Resultado: OleVariant );
begin
     Resultado[ K_PROCESO_STATUS ] := Ord( epsOK );
     Resultado[ K_PROCESO_FIN ] := Now;
end;

procedure SetProcessError( var Resultado: OleVariant );
begin
     Resultado[ K_PROCESO_STATUS ] := Ord( epsError );
     Resultado[ K_PROCESO_FIN ] := Now;
end;

{ ****************** TZetaLog ******************* }

constructor TZetaLog.Create( oProvider: TdmZetaServerProvider );
begin
     oZetaProvider := oProvider;
end;

procedure TZetaLog.Init;
begin
     FMaxSteps := 0;
     FIncrement := 0;
     FSteps := 0;
     FCounter := 0;
     FCancelado := False;
     FFolio := 0;
     FInicio := NullDateTime;
     FEmpleado := 0;
     FErrorCount := 0;
     FEventCount := 0;
     FWarningCount := 0;
end;

procedure TZetaLog.Conteo( const eTipo: eTipoBitacora );
begin
     case eTipo of
          tbNormal: Inc( FEventCount );
          tbAdvertencia: Inc( FWarningCount );
          tbError: Inc( FErrorCount );
          tbErrorGrave: Inc( FErrorCount );
     end;
end;

function TZetaLog.Abierto: Boolean;
begin
     Result := ( FFolio <> 0 );
end;

function TZetaLog.HayErrores: Boolean;
begin
     Result := ( FErrorCount > 0 );
end;

function TZetaLog.GetStatus: TLogStatus;
begin
     if FCancelado then
        Result := B_PROCESO_CANCELADO
     else
         Result := ZetaCommonTools.zBoolToStr( HayErrores );
end;

function TZetaLog.GetEnumStatus: eProcessStatus;
var
   cStatus: TLogStatus;
begin
     cStatus := GetStatus;
     if ( cStatus = B_PROCESO_CANCELADO ) then
        Result := epsCancelado
     else
         if ( cStatus = B_PROCESO_OK ) then
            Result := epsOK
         else
             if ( cStatus = B_PROCESO_ERROR ) then
                Result := epsError
             else
                 if ( cStatus = B_PROCESO_ABIERTO ) then
                    Result := epsError
                 else
                     Result := epsCatastrofico;
end;

function TZetaLog.OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer ): Boolean;
const
     K_INIT_SQL = 'select FOLIO from INIT_PROCESS_LOG( %d, %d, "%s", "%s", %d )';
     K_WRITE_LOG = 'insert into BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_PROC_ID, BI_TIPO, BI_CLASE, BI_NUMERO, BI_TEXTO, CB_CODIGO, BI_FEC_MOV, BI_DATA ) values '+
                   '( :US_CODIGO, :BI_FECHA, :BI_HORA, :BI_PROC_ID, :BI_TIPO, :BI_CLASE, :BI_NUMERO, :BI_TEXTO, :CB_CODIGO, :BI_FEC_MOV, :BI_DATA )';
     K_UPDATE_PROCESS = 'select STATUS from UPDATE_PROCESS_LOG( %d, :Empleado, :Paso, :Fecha, :Hora )';
var
   iUsuario: Integer;
   oQuery: TZPQuery;
begin
     Init;
     FInicio := Now;
     FProceso := eProceso;
     with oZetaProvider do
     begin
          iUsuario := EmpresaActiva[ P_USUARIO ];
          oQuery := CreateQuery;
          with oQuery do
          begin
               EmpiezaTransaccion;
               try
                  Result := AbreQueryScript( oQuery,
                                             Format( K_INIT_SQL,
                                                     [ Ord( FProceso ),
                                                       iUsuario,
                                                       DateToStrSQL( FInicio ),
                                                       TimeToStrSQL( FInicio ),
                                                       iMaxSteps ] ) );
                  TerminaTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminaTransaccion( False );
                          Result := False;
                     end;
               end;
               if Result then
               begin
                    FFolio := FieldByName( 'FOLIO' ).AsInteger;
                    FMaxSteps := iMaxSteps;
                    FIncrement := ZetaCommonTools.iMin( 25, ZetaCommonTools.iMax( 1, Trunc( 0.1 * FMaxSteps ) ) ); { PENDIENTE: 25 y 10% deben ser configurables }
                    FDataset := CreateQuery( K_WRITE_LOG );  { Prepara Query Para Escribir Eventos }
                    ParamAsInteger( FDataset, 'US_CODIGO', iUsuario );
                    ParamAsInteger( FDataset, 'BI_PROC_ID', Ord( FProceso ) );
                    ParamAsInteger( FDataset, 'BI_NUMERO', FFolio );
                    FUpdate := CreateQuery( Format( K_UPDATE_PROCESS, [ FFolio ] ) ); { Prepara Query Para Actualizar Status del Proceso }
               end
               else
                   DataBaseError( 'Error al Crear Bit�cora del Proceso' );
               Active := False;
          end;
     end;
end;

function TZetaLog.CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean;
const
     K_ANCHO_HORA = 8;
var
   dValue: TDateTime;
begin
     FCounter := FCounter + 1;
     FSteps := FSteps + 1;
     FEmpleado := iEmpleado;
     if FCancelado then
        Result := False
     else
         if ( FSteps > FIncrement ) then
         begin
              dValue := Now;
              with oZetaProvider do
              begin
                   { � Es correcto abrir/cerrar transacci�n }
                   { si el c�digo que llama a este m�todo }
                   { puede estar ya dentro de una transacci�n ? }
                   if lTx then
                      EmpiezaTransaccion;
                   try
                      ParamAsInteger( FUpdate, 'Empleado', iEmpleado );
                      ParamAsInteger( FUpdate, 'Paso', FCounter );
                      ParamAsDate( FUpdate, 'Fecha', Trunc( dValue ) );
                      ParamAsVarChar( FUpdate, 'Hora', TimeToStrSQL( dValue ), K_ANCHO_HORA );
                      with FUpdate do
                      begin
                           Active := True;
                           Result := ( Fields[ 0 ].AsInteger = 0 );
                           Active := False;
                      end;
                      if lTx then
                         TerminaTransaccion( True );
                   except
                         on Error: Exception do
                         begin
                              if lTx then
                                 TerminaTransaccion( False );
                              Result := not FCancelado; { � Es correcto suponer esto ? }
                         end;
                   end;
              end;
              FSteps := 1;
              FCancelado := not Result;
         end
         else
             Result := not FCancelado;
end;

function TZetaLog.CloseProcess: OleVariant;
const
     K_CLOSE_SQL = 'update PROCESO set '+
                   'PC_FEC_FIN = "%s", '+
                   'PC_HOR_FIN = "%s", '+
                   'PC_PASO = %d, '+
                   'PC_ERROR = "%s" '+
                   'where ( PC_NUMERO = %d )';
var
   dValue: TDateTime;
begin
     dValue := Now;
     with oZetaProvider do
     begin
          if Abierto then
          begin
               ExecSQL( EmpresaActiva, Format( K_CLOSE_SQL, [ DateToStrSQL( dValue ),
                                                              TimeToStrSQL( dValue ),
                                                              FCounter,
                                                              GetStatus,
                                                              FFolio ] ) );
          end;
     end;
     Result := GetEmptyProcessResult( FProceso );
     Result[ K_PROCESO_FOLIO ] := FFolio;
     Result[ K_PROCESO_STATUS ] := Ord( GetEnumStatus );
     Result[ K_PROCESO_MAXIMO ] := FMaxSteps;
     Result[ K_PROCESO_PROCESADOS ] := FCounter;
     Result[ K_PROCESO_ULTIMO_EMPLEADO ] := FEmpleado;
     Result[ K_PROCESO_INICIO ] := FInicio;
     Result[ K_PROCESO_FIN ] := dValue;
     Result[ K_PROCESO_ERRORES ] := FErrorCount;
     Result[ K_PROCESO_ADVERTENCIAS ] := FWarningCount;
     Result[ K_PROCESO_EVENTOS ] := FEventCount;
end;

procedure TZetaLog.CancelProcess( const iFolio: Integer );
const
     K_CANCEL_SQL = 'update PROCESO set '+
                   'PC_FEC_FIN = "%s", '+
                   'PC_HOR_FIN = "%s", '+
                   'PC_ERROR = "' + B_PROCESO_CANCELADO + '", '+
                   'US_CANCELA = %d '+
                   'where ( PC_NUMERO = %d )';
var
   dValue: TDate;
   iUsuario: Integer;
begin
     with oZetaProvider do
     begin
          dValue := Now;
          iUsuario := EmpresaActiva[ P_USUARIO ];
          EmpiezaTransaccion;
          try
             ExecSQL( EmpresaActiva, Format( K_CANCEL_SQL, [ DateToStrSQL( dValue ),
                                                             TimeToStrSQL( dValue ),
                                                             iUsuario,
                                                             iFolio ] ) );
             TerminaTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminaTransaccion( False );
                     raise;
                end;
          end;
     end;
end;

procedure TZetaLog.Escribir( const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
var
   dValor: TDateTime;
   dGravaMov: TDate;
begin
     dValor := Now;
     if ( dMovimiento = NullDateTime ) then
        dGravaMov := Trunc( dValor )   { Se usa Trunc() para que la base de datos no guarde la parte fraccionaria ( que representa la hora ) y los filtros de reportes puedan funcionar bien }
     else
         dGravaMov := dMovimiento;
     with oZetaProvider do
     begin
          try
             ParamAsDate( FDataset, 'BI_FECHA', Trunc( dValor ) );  { Se usa Trunc() para que la base de datos no guarde la parte fraccionaria ( que representa la hora ) y los filtros de reportes puedan funcionar bien }
             ParamAsVarChar( FDataset, 'BI_HORA', TimeToStrSQL( dValor ), K_ANCHO_LOG_HORA );
             ParamAsInteger( FDataset, 'BI_TIPO', Ord( eTipo ) );
             ParamAsInteger( FDataset, 'BI_CLASE', Ord( eClase ) );
             ParamAsVarChar( FDataset, 'BI_TEXTO', Copy( sMensaje, 1, 50 ), K_ANCHO_LOG_TEXTO );
             ParamAsInteger( FDataset, 'CB_CODIGO', iEmpleado );
             ParamAsDate( FDataset, 'BI_FEC_MOV', dGravaMov );
             ParamAsBlob( FDataset, 'BI_DATA', sTexto );
             Ejecuta( FDataset );
             Conteo( eTipo );
          except
                on Error: Exception do
                begin
                    Conteo( tbErrorGrave );
                     { � Que se debe hacer aqu� ? }
                end;
          end;
     end;
end;

procedure TZetaLog.Cambio( const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora; const sCampoEmpleado, sCampoFecha: String; Dataset: TDataSet );
begin
     Escribir( tbNormal, eClase, 0, Now, sMensaje, sMensaje );
end;

procedure TZetaLog.Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto );
begin
     Escribir( tbNormal, eClase, iEmpleado, dMovimiento, sMensaje, sMensaje );
end;

procedure TZetaLog.Evento( const eClase: eClaseBitacora; const iEmpleado: TNumEmp; const dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     Escribir( tbNormal, eClase, iEmpleado, dMovimiento, sMensaje, sTexto );
end;

procedure TZetaLog.Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto );
begin
     Escribir( tbAdvertencia, clbNinguno, iEmpleado, NullDateTime, sMensaje, sMensaje );
end;

procedure TZetaLog.Advertencia( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto : String );
begin
     Escribir( tbAdvertencia, clbNinguno, iEmpleado, NullDateTime, sMensaje, sTexto );
end;


procedure TZetaLog.Error( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; const sTexto: String );
begin
     Escribir( tbError, clbNinguno, iEmpleado, NullDateTime, sMensaje, sTexto );
end;

procedure TZetaLog.ErrorGrave( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto );
begin
     Escribir( tbErrorGrave, clbNinguno, iEmpleado, NullDateTime, sMensaje, sMensaje );
end;

procedure TZetaLog.Excepcion( const iEmpleado: TNumEmp; const sMensaje: TBitacoraTexto; Problema: Exception );
begin
     if PK_Violation( Problema ) then
        Escribir( tbError, clbNinguno, iEmpleado, NullDateTime, sMensaje, 'Ya existe ese Registro' )
     else
        Escribir( GetExceptionLogType( Problema ), clbNinguno, iEmpleado, NullDateTime, sMensaje, GetExceptionInfo( Problema ) );
end;


{ TTablaInfo }

constructor TTablaInfo.Create;
begin
     FListaCampos := TStringList.Create;
     FListaKey := TStringList.Create;
     FListaOrder := TStringList.Create;
     FListaForeignKey := TStringList.Create;
     FParams := TParams.Create;
     FSQL := TStringList.Create;
     FFiltro := '';
     FOnUpdateData := NIL;
     FBeforeUpdateRecord := NIL;
     FAfterUpdateRecord := NIL;
     FOnUpdateError := NIL;
end;

destructor TTablaInfo.Destroy;
begin
     FListaCampos.Free;
     FListaKey.Free;
     FListaOrder.Free;
     FListaForeignKey.Free;
     FParams.Free;
     FSQL.Free;
     inherited;
end;

procedure TTablaInfo.SetCampos(const sCampos: String);
begin
     FListaCampos.CommaText := sCampos;
end;

procedure TTablaInfo.SetKey(const sCampos: String);
begin
     FListaKey.CommaText := sCampos;
     FListaOrder.Assign( FListaKey );
end;

procedure TTablaInfo.SetOrder(const sCampos: String);
begin
     FListaOrder.CommaText := sCampos;
end;

procedure TTablaInfo.SetForeignKey(const sCampos: String);
begin
     FListaForeignKey.CommaText := sCampos;
end;


procedure TTablaInfo.SetInfo( const sTabla, sCampos, sKey : String );
begin
   FTabla := sTabla;
   SetCampos( sCampos );
   SetKey( sKey );
   Filtro := '';
   SetForeignKey( '' );
end;

procedure TTablaInfo.SetInfoDetail( const sTabla, sCampos, sKey, sForeignKey : String );
begin
   SetInfo( sTabla, sCampos, sKey );
   SetForeignKey( sForeignKey );
end;




function TTablaInfo.ConstruyeQuery( const eTipo : eTipoQuery; Delta : TDataset; qryCambios : TZPUpdate ) : String;
var
    lEsDetail : Boolean;
    oMasterDataSet : TDataSet;

  procedure Agrega( const sLinea : String );
  begin
       SQL.Add( sLinea );
  end;

  procedure AgregaLista( oLista : TStringList  );
  begin
       Agrega( oLista.CommaText );
  end;

  procedure ConstruyeSelect;
  begin
       Agrega( 'SELECT' );
       AgregaLista( FListaCampos );
  end;

  procedure ConstruyeDelete;
  begin
       Agrega( 'DELETE' );
  end;

  procedure ConstruyeFrom;
  begin
       Agrega( 'FROM ' + FTabla );
  end;

  function CampoConstante( const Valor : Variant; const Tipo : TFieldType ) : String;
  begin
       case Tipo of
            ftSmallint, ftInteger, ftWord :
              Result := IntToStr( Valor );
            ftString :
              Result := '''' + Valor + '''';
            ftFloat, ftCurrency, ftBCD :
              Result := FloatToStr( Valor );
            ftDate, ftTime, ftDateTime :
              Result := '''' + FormatDateTime( 'mm/dd/yyyy', Valor ) + '''';
       end;
  end;


  procedure RevisaCambios;
  var
     i, iPos : Integer;
  begin
       with Delta do
         for i := 0 to FieldCount-1 do
             with Fields[ i ] do
               if not IsNull and ( NewValue <> OldValue ) then
               begin
                 iPos := FListaCampos.IndexOf( FieldName );
                 // Si el campo existe en esta tabla
                 if ( iPos >= 0 ) then
                    FParams.CreateParam( DataType, FieldName, ptInput );
               end;
  end;

{
  procedure AgregaListaCambios( oLista : TStringList  );
  var
     i, iPos : Integer;
     sCampo, sValor : String;
     dCampo : TDate;
  begin
       with Delta do
         for i := 0 to FieldCount-1 do
           with Fields[ i ] do
                // Si el campo cambi� de valor
                if not IsNull and ( NewValue <> OldValue ) then
                begin
                     iPos := oLista.IndexOf( FieldName );
                     // Si el campo existe en esta tabla
                     if ( iPos >= 0 ) then
                     begin
                        // Si llamo CampoConstante, marca invalida variant type conversion
                        if DataType in [ ftDate, ftTime, ftDateTime ] then
                        begin
                            dCampo := NewValue;
                            sValor := '''' + FormatDateTime( 'mm/dd/yyyy', dCampo ) + '''';
                        end
                        else
                            sValor := CampoConstante( NewValue, DataType );

                        sCampo := FieldName + ' = ' + sValor;
                        sCampo := sCampo + ',';
                        Agrega( sCampo );
                     end;
                end;
       // Quita �ltima ','
       sCampo := SQL[SQL.Count-1];
       SQL[SQL.Count-1] := Copy(sCampo, 1, Length(sCampo) - 1);
  end;
}

  procedure AgregaCamposUpdate;
  var
     i : Integer;
     sCampo : String;
  begin
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
           sCampo := sCampo + ' = :' + sCampo;
           if ( i < Count-1 ) then
               sCampo := sCampo + ',';
           Agrega( sCampo );
         end;
  end;


  {
  insert into edocivil
  (TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO)
values
  (:TB_CODIGO, :TB_ELEMENT, :TB_INGLES, :TB_NUMERO, :TB_TEXTO)
}
  function EsForeignKey( const sCampo : String ) : Boolean;
  begin
    Result := FListaForeignKey.IndexOf( sCampo ) >= 0;
  end;

  procedure RevisaValores;
  var
     i, iPos : Integer;
  begin
       with Delta do
         for i := 0 to FieldCount-1 do
             with Fields[ i ] do
               if ( Not IsNull ) or ( lEsDetail and EsForeignKey( FieldName )) then
               begin
                 iPos := FListaCampos.IndexOf( FieldName );
                 // Si el campo existe en esta tabla
                 if ( iPos >= 0 ) then
                    FParams.CreateParam( DataType, FieldName, ptInput );
               end;
  end;


  procedure AgregaCamposInsert;
  var
     i : Integer;
     sCampo : String;
  begin
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
            if ( i < Count-1 ) then
               sCampo := sCampo + ',';
            Agrega( sCampo );
         end;

  end;

  procedure AgregaValoresInsert;
  var
     i : Integer;
     sCampo : String;
//     oField : TField;
  begin
{
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
           oField := Delta.FindField( sCampo );
           if ( lEsDetail and EsForeignKey( oField.FieldName )) then
             with oMasterDataSet.FieldByName( sCampo ) do
               sCampo := CampoConstante( Value, DataType )
           else
             sCampo := CampoConstante( oField.Value, oField.DataType );
           if ( i < Count-1 ) then
              sCampo := sCampo + ',';
           Agrega( sCampo );
         end;
}
      // Temporal: La unica diferencia con agrega campos Insert es ':'
      with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := ':' + Items[ i ].Name;
            if ( i < Count-1 ) then
               sCampo := sCampo + ',';
            Agrega( sCampo );
         end;

  end;

  procedure AsignaParams;
  var
     i : Integer;
     sCampo : String;
     oField : TField;
  begin
       with FParams do
         for i := 0 to Count-1 do
         begin
           sCampo := Items[ i ].Name;
           oField := Delta.FindField( sCampo );

           if ( eTipo = etqInsert ) and ( lEsDetail and EsForeignKey( oField.FieldName )) then
                oField := oMasterDataSet.FieldByName( sCampo );

           AsignaParamCampo( qryCambios.Params[ i ], oField );
         end;
  end;

  procedure ConstruyeInsert;
  begin
       Agrega( 'INSERT INTO ' + FTabla );
       Agrega( '(' );
       RevisaValores;
       AgregaCamposInsert;
       Agrega( ') VALUES (' );
       AgregaValoresInsert;
       Agrega( ')' );
  end;

  procedure ConstruyeUpdate;
  begin
       Agrega( 'UPDATE ' + FTabla );
       Agrega( 'SET' );
       RevisaCambios;
       AgregaCamposUpdate;
  end;


  procedure ConstruyeWhereDetail;
  var
     i : Integer;
     sCampoMaster, sCampoDetail : String;
     sCampo : String;
  begin
       with FListaForeignKey do
       begin
         Agrega( 'WHERE' );
         for i := 0 to Count-1 do
         begin
              sCampoMaster := Strings[ i ];
              // Si se tiene un Pare Name=Value
              if Pos( '=', sCampoMaster ) > 0 then
              begin
                   sCampoMaster := Names[i];
                   sCampoDetail := Values[ sCampoMaster ];
              end
              else
                  sCampoDetail := FListaKey.Strings[ i ];
              sCampo := sCampoDetail + ' = :' + sCampoMaster;
              if ( i < Count-1 ) then
                   sCampo := sCampo + ' and';
              Agrega( sCampo );
         end;
       end;
  end;

  procedure ConstruyeWherePadre;
  var
     i : Integer;
     sCampoMaster, sCampoDetail : String;
     sCampo : String;
  begin
       with FListaForeignKey do
       begin
         Agrega( 'WHERE' );
         for i := 0 to Count-1 do
         begin
              sCampoDetail := FListaKey.Strings[ i ];
              sCampoMaster := Strings[ i ];
              with Delta.FieldByName( sCampoMaster ) do
                   sCampoMaster := CampoConstante( NewValue, DataType );
              sCampo := sCampoDetail + ' = ' + sCampoMaster;
              if ( i < Count-1 ) then
                   sCampo := sCampo + ' and';
              Agrega( sCampo );
         end;
       end;
  end;


  procedure ConstruyeWhereFiltro;
  begin
       Agrega( 'WHERE' );
       Agrega( FFiltro );
  end;

  procedure AgregaListaWhere( oLista : TStringList  );
  var
     i : Integer;
     sCampo, sValor : String;
     oField : TField;
     dCampo : TDate;
  begin
       with oLista do
         for i := 0 to Count-1 do
         begin
              sCampo := Strings[ i ];
              oField := Delta.FindField( sCampo );
              if Assigned( oField ) then
              begin
                if oField.DataType in [ ftDate, ftTime, ftDateTime ] then
                begin
                    dCampo := oField.OldValue;
                    sValor := '''' + FormatDateTime( 'mm/dd/yyyy', dCampo ) + '''';
                end
                else
                    sValor := CampoConstante( oField.OldValue, oField.DataType );
                sCampo := sCampo + ' = ' + sValor;
                if ( i < Count-1 ) then
                   sCampo := sCampo + ' and';
                Agrega( sCampo );
              end;
         end;
  end;

  procedure ConstruyeWhere;
  begin
       Agrega( 'WHERE' );
       AgregaListaWhere( FListaKey );
  end;

  procedure ConstruyeOrder;
  begin
       Agrega( 'ORDER BY' );
       AgregaLista( FListaOrder );
  end;



begin // ConstruyeQuery
     FParams.Clear;
     SQL.Clear;
     lEsDetail := FListaForeignKey.Count > 0;
     if ( lEsDetail ) then
        oMasterDataSet := TClientDataSet( Delta ).DataSetField.DataSet;

     case eTipo of
          etqSelect :
            begin
                 ConstruyeSelect;
                 ConstruyeFrom;
                 if ( lEsDetail ) then
                   ConstruyeWhereDetail
                 else if ( Length( FFiltro ) > 0 ) then
                     ConstruyeWhereFiltro;
                 ConstruyeOrder;
            end;
          etqDelete :
            begin
                 ConstruyeDelete;
                 ConstruyeFrom;
                 ConstruyeWhere;
                 qryCambios.SQL.Text := SQL.Text;
            end;
          etqUpdate :
            begin
                 ConstruyeUpdate;
                 ConstruyeWhere;
                 qryCambios.SQL.Text := SQL.Text;
                 AsignaParams;
            end;
          etqInsert :
            begin
                 ConstruyeInsert;
                 qryCambios.SQL.Text := SQL.Text;
                 AsignaParams;
            end;
     end;
     Result := SQL.Text;

{
     if eTipo <> etqSelect then
      ShowMessage( Result );
}      

end;


function TTablaInfo.ConstruyeUpdateSQL(const UpdateKind: TUpdateKind;
  Delta: TDataset; qryCambios : TZPUpdate ) : String;
var
   TipoQuery : eTipoQuery;
begin
     case UpdateKind of
          ukModify : TipoQuery := etqUpdate;
          ukDelete : TipoQuery := etqDelete;
          else
                   TipoQuery := etqInsert;
     end;
     Result := ConstruyeQuery( TipoQuery, Delta, qryCambios );
end;

function TTablaInfo.ConstruyeSelectSQL : String;
begin
     Result := ConstruyeQuery( etqSelect, NIL, NIL );
end;

{ TdmZetaProvider }

procedure TdmZetaServerProvider.dmZetaProviderCreate(Sender: TObject);
begin
     Environment;
     FTablaInfo := TTablaInfo.Create;
     FDetailInfo := TTablaInfo.Create;
     FRegistry := TZetaRegistryServer.Create;
     FEmpresa := NULL;
     FPeriodoObtenido:= FALSE;
     FVerConfidencial := TRUE;
end;

procedure TdmZetaServerProvider.dmZetaProviderDestroy(Sender: TObject);
begin
     ReleaseLog;
     FRegistry.Free;
     FDetailInfo.Free;
     FTablaInfo.Free;
     FQueryGlobal.Free;
end;

function TdmZetaServerProvider.GetMasterDetail( Empresa : Variant ) : OleVariant;
begin
end;

procedure TdmZetaServerProvider.PreparaTabla;
begin
    qryMaster.SQL.Text := FTablaInfo.ConstruyeSelectSQL;
end;

function TdmZetaServerProvider.GetTabla( Empresa : Variant ) : OLEVariant;
begin
    ConectaEmpresa( Empresa );
    PreparaTabla;
    FRecsOut := 0;
    Result := prvUnico.GetRecords( -1, FRecsOut, Ord(grMetaData)+Ord(grReset));
end;

function TdmZetaServerProvider.GrabaTabla( Empresa : Variant; oDelta: OleVariant; var ErrorCount : Integer): OleVariant;
var
    OwnerData : OleVariant;
begin
    ConectaEmpresa( Empresa );
    EmpiezaTransaccion;
    Result := prvUnico.ApplyUpdates( oDelta, -1, ErrorCount, OwnerData );
    if ErrorCount = 0 then
       TerminaTransaccion( TRUE )
    else
       // TerminaTransaccion( FALSE );
       RollBackTransaccion;
end;

function TdmZetaServerProvider.GrabaTablaGrid(Empresa: Variant; oDelta: OleVariant; var ErrorCount: Integer): OleVariant;
var
    OwnerData : OleVariant;
begin
    ConectaEmpresa( Empresa );
    EmpiezaTransaccion;
    Result := prvUnico.ApplyUpdates( oDelta, -1, ErrorCount, OwnerData );
    TerminaTransaccion( TRUE )
end;


function TdmZetaServerProvider.GrabaMasterDetail( Empresa : Variant;
                                            oDelta: OleVariant;
                                            var ErrorCount: Integer): OleVariant;
begin
end;


procedure TdmZetaServerProvider.UpdateTabla(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind;
  var Applied: Boolean);
var
   TablaInfo : TTablaInfo;
begin
     if Assigned( DeltaDS.DataSetField ) then
        TablaInfo := FDetailInfo
     else
         TablaInfo := FTablaInfo;
//     qryCambios.CommandText := TablaInfo.ConstruyeUpdateSQL( UpdateKind, DeltaDS );
     TablaInfo.ConstruyeUpdateSQL( UpdateKind, DeltaDS, qryCambios );
//     try
        qryCambios.ExecSQL;
{
     // PENDIENTE
        if qryCambios.RowsAffected > 1 then
           DatabaseError('Demasiados registros cambiaron');
        if qryCambios.RowsAffected < 1 then
           DatabaseError('Registro modificado por otro usuario');
}
        Applied:= TRUE;
//     except
//      Applied := FALSE;
//     end;

end;

procedure TdmZetaServerProvider.prvUnicoUpdateData(Sender: TObject;
  DataSet: TClientDataSet);
begin
     with FTablaInfo do
          if Assigned( OnUpdateData ) then
             OnUpdateData( Sender, DataSet );

{ No funciona porque se altera el ORDEN del ClientDataSet
     oCampo := DataSet.FindField( 'qryDetail' ) as TDataSetField;
     if ( oCampo <> NIL ) then
       with oCampo.NestedDataSet do
         while not Eof do
         begin
            Edit;
            FieldByName( 'TB_CODIGO' ).AsString := DataSet.FieldByName( 'TB_CODIGO' ).AsString;
            Post;
            Next;
         end;
}
end;

procedure TdmZetaServerProvider.ExecSQL( Empresa : Variant; const sSQL : String);
begin
     ConectaEmpresa( Empresa );
     with qryCambios do
     begin
          ParamCheck  := FALSE; // Para cuando se modifican StoredProcedures (ej. Supervisores)
          SQL.Text := sSQL;
          ExecSQL;
          ParamCheck  := TRUE;
     end;
end;


procedure TdmZetaServerProvider.prvUnicoBeforeUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind;
  var Applied: Boolean);
begin
     // Llama al BeforeUpdateRecord del programador
     with FTablaInfo do
     begin
          if Assigned( BeforeUpdateRecord ) then
             BeforeUpdateRecord( Sender, SourceDS, DeltaDS, UpdateKind, Applied );

          if ( not Applied ) then
             UpdateTabla( Sender, SourceDS, DeltaDS, UpdateKind, Applied );
     end;
end;

function TdmZetaServerProvider.OpenSQL( Empresa : Variant; const sSQL : String; const lDatos: Boolean): OleVariant;
var
    nRecords : Integer;
begin
     with qryMaster do
     begin
          Active   := FALSE;
          ConectaEmpresa( Empresa );
          SQL.Text := sSQL;
          FRecsOut := 0;
          if ( lDatos ) then
            nRecords := -1
          else
            nRecords := 0;
          Result := prvUnico.GetRecords( nRecords, FRecsOut, Ord(grMetaData)+Ord(grReset) )
     end;
end;

function TdmZetaServerProvider.ConectaBaseDeDatos( const Empresa: Variant ): TZPConnection;
var
   sAlias, sUser, sPassword: String;
begin
     sAlias := Empresa[ 0 ];
     with FRegistry do
     begin
          if ( sAlias = AliasComparte ) then
          begin
               Result := bdeComparte;
               sPassword := Empresa[ 2 ]; { Ya viene decifrado }
          end
          else
          begin
               Result := bdeEmpresa;
               sPassword := ZetaServerTools.Decrypt( Empresa[ 2 ] ); { Viene cifrado }
          end;
     end;
     { Nota: No est� dise�ado para estar cambiando de Empresa.
      Una vez que se conecta, ya queda conectada con ese Alias }
     try
        with Result do
        begin
             if not Connected then
             begin
                  AliasName := sAlias;
                  sUser := Empresa[ 1 ];
                  Params.Add( Format( 'USER NAME=%s', [sUser] ));
                  Params.Add( Format( 'PASSWORD=%s', [sPassword] ));
                  Connected := True;
             end;
        end;
     except
           on Error: Exception do
           begin
                Error.Message := 'Error al Conectar a la Base de Datos: ' + sAlias + CR_LF + Error.Message +
                            CR_LF + sUser + CR_LF + sPassword;
                raise;
           end;
     end;
end;

procedure TdmZetaServerProvider.ConectaEmpresa( const Empresa: Variant );
begin
     FConnection := ConectaBaseDeDatos( Empresa );
     qryMaster.DataBaseName  := FConnection.DatabaseName;
     qryCambios.DataBaseName := FConnection.DatabaseName;
end;

{ ************ L�gica de Manejo de Queries / Stored Procedures ************** }

function TdmZetaServerProvider.CreateQueryLocate : TZPQuery;
begin
     Result := CreateQuery;
end;

function TdmZetaServerProvider.CreateQuery : TZPQuery;
begin
     Result := TQuery.Create( self );
     with TQuery( Result ) do
     begin
        Unidirectional := TRUE;
     end;
end;

function TdmZetaServerProvider.CreateQuery(const sScript: String): TZPQuery;
begin
     Result := CreateQuery;
     PreparaQuery( Result, sScript );
end;

procedure TdmZetaServerProvider.PreparaQuery( Query : TZPQuery; const sScript: String );
begin
     with Query as TQuery do
     begin
          Active := FALSE;
          DataBaseName := ConectaBaseDeDatos( EmpresaActiva ).DatabaseName;
          SessionName := Session1.SessionName;
          SQL.Text := sScript;
          Prepared := TRUE;
     end;
end;

function TdmZetaServerProvider.CreateStoredProcedure : TDataset;
begin
    Result := NIL;
    DataBaseError( 'No hay Stored Procedures' );
end;

procedure TdmZetaServerProvider.PreparaStoredProcedure( StoredProc : TDataset; const sProcedureName: String );
begin
end;

function TdmZetaServerProvider.Ejecuta( Dataset: TZPQuery ) : Integer;
begin
    with DataSet as TQuery do
    begin
         ExecSQL;
         Result := RowsAffected;
    end;
end;

function TdmZetaServerProvider.GetParametroPos(Dataset: TZPQuery;
  const iPos: Integer): TZPParam;
begin
     Result := TQuery( Dataset ).Params[ iPos ];
end;

procedure TdmZetaServerProvider.ParamVariant(Dataset: TZPQuery;
  const iPos: Integer; const Valor: Variant);
begin
    TQuery( DataSet ).Params[ iPos ].Value := Valor;
end;

procedure TdmZetaServerProvider.ParamAsInteger( Dataset: TZPQuery; const sName: String; const iValue: Integer );
begin
    TQuery( Dataset ).ParamByName( sName ).AsInteger := iValue;
end;

procedure TdmZetaServerProvider.ParamAsFloat( Dataset: TZPQuery; const sName: String; const rValue: Extended );
begin
    TQuery( Dataset ).ParamByName( sName ).AsFloat := rValue;
end;

procedure TdmZetaServerProvider.ParamAsDate( Dataset: TZPQuery; const sName: String; const dValue: TDate );
begin
    TQuery( Dataset ).ParamByName( sName ).AsDateTime := dValue;
end;

procedure TdmZetaServerProvider.ParamAsString( Dataset: TZPQuery; const sName, sValue: String );
begin
    TQuery( Dataset ).ParamByName( sName ).AsString := sValue;
end;

procedure TdmZetaServerProvider.ParamAsVarChar(Dataset: TZPQuery; const sName, sValue: String; const iAncho: Integer );
begin
    TQuery( Dataset ).ParamByName( sName ).AsString := sValue;
end;

procedure TdmZetaServerProvider.ParamAsChar(Dataset: TZPQuery; const sName, sValue: String; const iAncho: Integer );
begin
    TQuery( Dataset ).ParamByName( sName ).AsString := sValue;
end;


procedure TdmZetaServerProvider.ParamAsBoolean( Dataset: TZPQuery; const sName: String; const lValue: Boolean );
begin
     {
     GetParametro( Dataset, sName ).Value := ZetaCommonTools.zBoolToStr( lValue );
     }
     ParamAsChar( Dataset, sName, ZetaCommonTools.zBoolToStr( lValue ), 1 );
end;

procedure TdmZetaServerProvider.ParamAsBlob( Dataset: TZPQuery; const sName: String; const sValue: String );
begin
    TQuery( Dataset ).ParamByName( sName ).AsBlob := sValue;
end;

procedure TdmZetaServerProvider.CreateLog;
begin
     if not Assigned( FLog ) then
     begin
          FLog := TZetaLog.Create( Self );
     end;
end;

procedure TdmZetaServerProvider.ReleaseLog;
begin
     if Assigned( FLog ) then
     begin
          FreeAndNil( FLog );
     end;
end;

function TdmZetaServerProvider.CanContinue: Boolean;
begin
     Result := Log.CanContinue( 0, True );
end;

function TdmZetaServerProvider.CanContinue( const iEmpleado: TNumEmp ): Boolean;
begin
     Result := Log.CanContinue( iEmpleado, True );
end;

function TdmZetaServerProvider.CanContinue( const iEmpleado: TNumEmp; const lTx: Boolean ): Boolean;
begin
     Result := Log.CanContinue( iEmpleado, lTx );
end;

function TdmZetaServerProvider.OpenProcess( const eProceso: Procesos; const iMaxSteps: Integer ): Boolean;
begin
     CreateLog;
     Result := Log.OpenProcess( eProceso, iMaxSteps );
end;

function TdmZetaServerProvider.CloseProcess: OleVariant;
begin
     Result := Log.CloseProcess;
     ReleaseLog;
end;

procedure TdmZetaServerProvider.CancelProcess( const iFolio: Integer );
begin
     { Hay que crear y destruir la instancia de Log ya que }
     { Esto no se va a llamar como parte de un wizard }
     { sino que desde la pantalla de consulta del status de procesos }
     try
        CreateLog;
        Log.CancelProcess( iFolio );
     finally
            ReleaseLog;
     end;
end;

procedure TdmZetaServerProvider.AbreBitacora;
begin
     CreateLog;
end;


function TdmZetaServerProvider.AbreQueryScript(Dataset: TZPQuery; const sScript: String): Boolean;
begin
     try
       with Dataset as TQuery do
       begin
            Active       := FALSE;
            DatabaseName := ConectaBaseDeDatos( EmpresaActiva ).DatabaseName;
            SessionName  := Session1.SessionName;
            SQL.Text     := sScript;
            Active       := TRUE;
            Result       := TRUE;
       end;
     except
       ON E : Exception do
       begin
         // PENDIENTE
         ShowMessage( E.Message );
         Result := FALSE;
       end;
     end;
end;

function TdmZetaServerProvider.GetGlobalBooleano( const iCodigo: Integer): Boolean;
begin
     Result := zStrToBool( LeeGlobal( iCodigo ));
end;

function TdmZetaServerProvider.GetGlobalDate( const iCodigo: Integer): TDateTime;
begin
     Result := StrToFecha( LeeGlobal( iCodigo ));
end;

function TdmZetaServerProvider.GetGlobalInteger( const iCodigo: Integer): Integer;
begin
     Result := StrToIntDef( LeeGlobal( iCodigo ), 0 );
end;

function TdmZetaServerProvider.GetGlobalReal(const iCodigo: Integer): Real;
begin
     Result := StrToReal( LeeGlobal( iCodigo ) );
end;

function TdmZetaServerProvider.GetGlobalString( const iCodigo: Integer): String;
begin
     Result := LeeGlobal( iCodigo );
end;

procedure TdmZetaServerProvider.InitGlobales;
const
     K_SELECT_GLOBAL = 'select GL_FORMULA from GLOBAL where GL_CODIGO = :Codigo';
begin
     if ( FQueryGlobal = NIL ) then
          FQueryGlobal := CreateQuery;
     PreparaQuery( FQueryGlobal, K_SELECT_GLOBAL );
end;

// Como es dentro de esta unidad, puedo saber el tipo Exacto
// del DataSet y no tengo por qu� usar los m�todos abstractos
// Esto nos da mayor velocidad.
// Al quitar los TQuerys, cambia ligeramente la implementaci�n
function TdmZetaServerProvider.LeeGlobal(const iCodigo: Integer): String;
begin
     if ( FQueryGlobal = NIL ) then
        raise Exception.Create( 'Falta Llamar a InitGlobales antes de usar GetGlobal<x>' );
     with FQueryGlobal as TQuery do
     begin
          Active := FALSE;
          Params[ 0 ].AsInteger := iCodigo;
          Active := TRUE;
          Result := Fields[ 0 ].AsString;
     end;
end;


{
function TdmZetaServerProvider.Params(DataSet: TDataset): TParams;
begin
     if ( Dataset is TADOQuery ) then
        Result := TADOQuery( Dataset ).Params
     else
         if ( Dataset is TADOStoredProc ) then
            Result := TADOStoredProc( Dataset ).Params
         else
             Result := nil;

end;
}

function TdmZetaServerProvider.GetComparte: Variant;
begin
     with FRegistry do
     begin
          Result := VarArrayOf( [ AliasComparte, UserName, Password, 0 ] );
     end;
end;

function TdmZetaServerProvider.AliasComparte: String;
var nPos : Integer;
begin
     with FRegistry do
     begin
          // El DataLinkName para ADO es 'Comparte.UDL'
          // Es necesario quitarle la extensi�n para usarlo como Alias en BDE
          Result := DataLinkName;
          nPos := Pos( '.', Result );
          if ( nPos > 0 ) then
            Result := Copy( Result, 1, nPos-1 );
     end;
end;

procedure TdmZetaServerProvider.SetEmpresa(Empresa: Variant);
begin
     FEmpresa := Empresa;
     ConectaEmpresa( FEmpresa );
end;

function TdmZetaServerProvider.GetUsuario: Integer;
begin
     Result := FEmpresa[ 3 ];
end;



procedure TdmZetaServerProvider.AsignaDataSetParams(oDataSet: TZPQuery );
var
    oDatasetParams : TParams;
    oDSParam : TParam;
    oParam : TParam;
    i : Integer;
begin
    oDataSetParams := TQuery( oDataset ).Params;
    // Barre la lista de par�metros del Dataset buscando
    // par�metros con el mismo nombre en FListaParams
    // Si existen, entonces asigna el Valor el param del Dataset
    with oDataSetParams do
        for i := 0 to Count-1 do
        begin
            oDSParam := Items[ i ];
            oParam := FParamList.FindParam( oDSParam.Name );
            if ( oParam <> NIL ) then
                oDSParam.Value := oParam.Value;
        end;
end;

procedure TdmZetaServerProvider.AsignaDataSetOtros(oDataSet: TZPQuery; oListaPares : Variant );
var
    oDatasetParams : TParams;
    i, iElementos : Integer;
begin
    oDataSetParams := TQuery( oDataset ).Params;

    if ( VarIsArray( oListaPares )) then
    begin
        i := VarArrayLowBound( oListaPares, 1 );
        iElementos := VarArrayHighBound( oListaPares, 1 );
        while ( i < iElementos ) do
        begin
            oDataSetParams.ParamByName( oListaPares[ i ] ).Value :=
                oListaPares[ i + 1 ];
            // Pasa al siguiente 'par'
            i := i + 2;
        end;
    end;

end;

{ NOTA: Esto se tiene que hacer porque el ADO regresa los campos
  tipo CHAR con espacios a la derecha, a diferencia de los VARCHAR.
  Con el BDE esto no sucede.
  Se tiene que barrer todo el DataSet para hacerle un TRIM a los
  campos tipo 'String' identificados como 'FixedChar' }
procedure TdmZetaServerProvider.prvUnicoGetData(Sender: TObject;
  DataSet: TClientDataSet);
var
    i : Integer;
    oField : TField;
    oListaFixed : TList;
begin
    oListaFixed := TList.Create;
    with DataSet do
    begin
        for i := FieldCount-1 downto 0 do
        begin
            oField := Fields[ i ];
            if ( oField.DataType = ftString ) and
                TStringField( oField ).FixedChar then
                oListaFixed.Add( oField );
        end;

        if ( oListaFixed.Count > 0 ) then
          while not Eof do
          begin
              Edit;
              for i := oListaFixed.Count-1 downto 0 do
                with TField( oListaFixed.Items[ i ] ) do
                    AsString := Trim( AsString );
              Post;
              Next;
          end;
    end;
    oListaFixed.Free;

end;



procedure TdmZetaServerProvider.GetDatosPeriodo(const lForzar : Boolean);
 const Q_DATOS_PERIODO = 'SELECT PE_YEAR, PE_TIPO, PE_NUMERO, '+
                         'PE_FEC_INI, PE_FEC_FIN, PE_FEC_PAG, '+
                         'PE_DIAS, PE_USO, PE_STATUS, PE_MES, '+
                         'PE_INC_BAJ, PE_SOLO_EX, PE_AHORRO, PE_PRESTAM, '+
                         'PE_PER_MES, PE_POS_MES, PE_DIAS_AC ' +
                         'FROM PERIODO '+
                         'WHERE PE_YEAR = %d AND PE_TIPO = %d '+
                         'AND PE_NUMERO = %d ';
 var FDataSetPeriodo : TZPQuery;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if lForzar OR ( not FPeriodoObtenido ) then
     begin
          FDataSetPeriodo := CreateQuery;
          if AbreQueryScript( FDataSetPeriodo,
                              Format( Q_DATOS_PERIODO,
                                   [ fParamList.ParamByName('Year').AsInteger,
                                     fParamList.ParamByName('Tipo').AsInteger,
                                     fParamList.ParamByName('Numero').AsInteger ] )) then
          begin
               with FDatosPeriodo, FDataSetPeriodo do
               begin
                    Year:= FieldByName('PE_YEAR').AsInteger;
                    Tipo:= eTipoPeriodo(FieldByName('PE_TIPO').AsInteger);
                    Numero:= FieldByName('PE_NUMERO').AsInteger;
                    Inicio:= FieldByName('PE_FEC_INI').AsDateTime;
                    Fin:= FieldByName('PE_FEC_FIN').AsDateTime;
                    Pago:= FieldByName('PE_FEC_PAG').AsDateTime;
                    Dias := FieldByName( 'PE_DIAS' ).AsInteger;
                    Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
                    Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
                    Mes := FieldByName( 'PE_MES' ).AsInteger;
                    SoloExcepciones := zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
                    IncluyeBajas := zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
                    DescuentaAhorros := zStrToBool( FieldByName( 'PE_AHORRO' ).AsString );
                    DescuentaPrestamos := zStrToBool( FieldByName( 'PE_PRESTAM' ).AsString );
                    PosMes := FieldByName( 'PE_POS_MES' ).AsInteger;
                    PerMes := FieldByName( 'PE_PER_MES' ).AsInteger;
                    DiasAcumula := FieldByName( 'PE_DIAS_AC' ).AsInteger;
               end;
               FMesDefault := FDatosPeriodo.Mes;
               FPeriodoObtenido:= TRUE;
          end
          else raise Exception.Create('No existe periodo activo')
     end;
end;

procedure TdmZetaServerProvider.GetDatosImss;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else
     begin
          with FDatosImss, fParamList do
          begin
               Patron := ParamByName('RegistroPatronal').AsString;
               Year := ParamByName('ImssYear').AsInteger;
               Mes := ParamByName('ImssMes').AsInteger;
               Tipo := eTipoLiqIMSS(ParamByName('ImssTipo').AsInteger);
               {CV: Mientras no los requiera nadie, no se van a inicializar
               Status := eStatusLiqIMSS(Fields[0].AsInteger);
               Modifico := Fields[1].AsInteger;
               }
          end
     end;
end;

procedure TdmZetaServerProvider.GetDatosActivos;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if FYearDefault = 0 then
          with FParamList do
          begin
               FFechaAsistencia := ParamByName('FechaAsistencia').AsDate;
               FFechaDefault := ParamByName('FechaDefault').AsDate;
               FYearDefault := ParamByName('YearDefault').AsInteger;
               FEmpleadoActivo := ParamByName('EmpleadoActivo').AsInteger;
               FNombreUsuario := ParamByName('NombreUsuario').AsString;
               FCodigoEmpresa := ParamByName('CodigoEmpresa').AsString;
          end;
end;

procedure TdmZetaServerProvider.GetEncabPeriodo;
begin
     if FParamList = NIL then
        raise Exception.Create('No esta creada la lista de Par�metros')
     else if FDatosPeriodo.Year = 0 then
          with FDatosPeriodo, FParamList do
          begin
               Year := ParamByName('Year').AsInteger;
               Tipo := eTipoPeriodo( ParamByName('Tipo').AsInteger );
               Numero := ParamByName('Numero').AsInteger;
          end;
end;

procedure TdmZetaServerProvider.AsignaParamList(oParams: OleVariant);
begin
     if FParamList = NIL then
        FParamList := TZetaParams.Create;

     FParamList.VarValues := oParams;
end;


function TdmZetaServerProvider.DescripcionParams: String;
var
   i : Integer;
begin
     Result := '';
     with FParamList do
          for i := 0 to Count-1 do
              with Items[ i ] do
                   Result := Result + Name + '=' + AsString + CR_LF;
end;


procedure TdmZetaServerProvider.prvUnicoAfterUpdateRecord(Sender: TObject;
  SourceDS: TDataSet; DeltaDS: TClientDataSet; UpdateKind: TUpdateKind);
begin
     with FTablaInfo do
     begin
          if Assigned( AfterUpdateRecord ) then
             AfterUpdateRecord( Sender, SourceDS, DeltaDS, UpdateKind )
     end;
end;

procedure TdmZetaServerProvider.EmpiezaTransaccion;
begin
    FConnection.StartTransaction;
end;

procedure TdmZetaServerProvider.TerminaTransaccion(lCommit: Boolean);
begin
{ EZM: Como tuvimos problemas con RollBack y ODBC, se hace siempre Commit
    if ( lCommit ) then
        FConnection.Commit
    else
        FConnection.RollBack;
}
    FConnection.Commit;
end;

procedure TdmZetaServerProvider.RollBackTransaccion;
begin
    FConnection.RollBack;
end;


procedure TdmZetaServerProvider.AsignaParamsDataSet(oDataSetParams : TZPQuery;
  oDataSetValores: TDataset);
var
   i : Integer;
begin
     { Nota: Supone que LOS FIELDS corresponden exactamente, uno a uno }
     with oDataSetValores do
        for i := FieldCount-1 downto 0 do
                 AsignaParamCampo( GetParametroPos( oDataSetParams, i ),
                                   Fields[ i ] );
end;

procedure TdmZetaServerProvider.ActualizaTabla( OrigenDS: TDataSet; UpdateKind: TUpdateKind );
begin
     FTablaInfo.ConstruyeUpdateSQL( UpdateKind, OrigenDS, qryCambios );
     qryCambios.ExecSQL;
end;

procedure TdmZetaServerProvider.prvUnicoUpdateError(Sender: TObject;
  DataSet: TClientDataSet; E: EUpdateError; UpdateKind: TUpdateKind;
  var Response: TResolverResponse);
begin
     with FTablaInfo do
          if Assigned( OnUpdateError ) then
             OnUpdateError( Sender, DataSet, E, UpdateKind, Response );
end;


procedure TdmZetaServerProvider.BorraCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora;  Dataset: TDataSet);
begin
    EscribeBitacora( tbNormal, eClase, 0, NullDateTime, 'Borrado de ' + sMensaje, GetDatasetInfo( Dataset, TRUE ));
end;

procedure TdmZetaServerProvider.CambioCatalogo(const sMensaje: TBitacoraTexto; const eClase: eClaseBitacora;  Dataset: TDataSet);
begin
    EscribeBitacora( tbNormal, eClase, 0, NullDateTime, 'Modificaci�n de ' + sMensaje, GetDatasetInfo( Dataset, FALSE ));
end;

procedure TdmZetaServerProvider.EscribeBitacora( const eTipo: eTipoBitacora; const eClase: eClaseBitacora; const iEmpleado: TNumEmp; dMovimiento: TDate; const sMensaje: TBitacoraTexto; const sTexto: String );
const
     K_WRITE_SIMPLE_LOG = 'insert into BITACORA (US_CODIGO, BI_FECHA, BI_HORA, BI_TIPO, BI_CLASE, BI_TEXTO, CB_CODIGO, BI_FEC_MOV, BI_DATA ) values '+
                          '( :US_CODIGO, :BI_FECHA, :BI_HORA, :BI_TIPO, :BI_CLASE, :BI_TEXTO, :CB_CODIGO, :BI_FEC_MOV, :BI_DATA )';
var
    oQryInsert : TZPQuery;
    dBitacora : TDateTime;
begin
    try
       dBitacora := Now;
       if ( dMovimiento = 0 ) then
          dMovimiento := dBitacora;
       oQryInsert := CreateQuery( K_WRITE_SIMPLE_LOG );
       ParamAsInteger( oQryInsert, 'US_CODIGO', UsuarioActivo );
       ParamAsDate(    oQryInsert, 'BI_FECHA', Trunc( dBitacora ));
       ParamAsVarChar( oQryInsert, 'BI_HORA', TimeToStrSQL( dBitacora ), K_ANCHO_LOG_HORA );
       ParamAsInteger( oQryInsert, 'BI_TIPO', Ord( eTipo ) );
       ParamAsInteger( oQryInsert, 'BI_CLASE', Ord( eClase ) );
       ParamAsVarChar( oQryInsert, 'BI_TEXTO', Copy( sMensaje, 1, K_ANCHO_LOG_TEXTO ), K_ANCHO_LOG_TEXTO );
       ParamAsInteger( oQryInsert, 'CB_CODIGO', iEmpleado );
       ParamAsDate(    oQryInsert, 'BI_FEC_MOV', dMovimiento );
       ParamAsBlob(    oQryInsert, 'BI_DATA', sTexto );
       Ejecuta( oQryInsert );
    except
          raise;
    end;
end;

function TdmZetaServerProvider.GetDatasetInfo(Dataset: TDataset; const lBorrar : Boolean ): String;
var
    i : Integer;
    sCampo, sOld, sNew : String;
begin
    Result := '';
    with DataSet do
      for i := 0 to FieldCount-1 do
          with Fields[ i ] do
            if ( lBorrar ) or ( not IsNull and ( NewValue <> OldValue )) then
            begin
                if ( lBorrar ) then
                    sCampo := FieldName + ' = ' + AsString
                else
                begin
                    sOld   := OldValue;
                    sNew   := NewValue;
                    sCampo := FieldName + CR_LF +
                              ' De: ' + sOld + CR_LF +
                              ' A : ' + sNew;
                end;
                Result := Result + sCampo + CR_LF;
            end;
end;

end.

