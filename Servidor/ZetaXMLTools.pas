unit ZetaXMLTools;

interface

uses Classes, ActiveX, ComObj, Controls, SysUtils, DB,
     CHILKATXMLLib_TLB,
     DZetaServerProvider,
     ZetaCommonLists,
     ZetaCommonClasses;

{ Documentaci�n de ChilKat en http://www.chilkatsoft.com }
const
     K_LETRERO: WideString = 'letrero';
     K_DESCRIPCION: WideString = 'descripcion';
     aJustificacion: array[ TAlignment ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'LEFT', 'RIGHT', 'CENTER' );
type
  eTipoBoton = ( eBtnSubmit, eBtnReset, eBtnNormal );
  TZetaXMLNode = IChilkatXML;
  TZetaXML = class( TObject )
  private
    { Private declarations }
    FXMLFactory: IXMLFactory;
    FXMLParamList: TZetaXMLNode;
    FXMLDocument: TZetaXMLNode;
    FXMLDocErrores: TZetaXMLNode;
    FHuboErrores: Boolean;
    oZetaProvider: TdmZetaServerProvider;
    FErrorMsg: String;
    aClaseFuentePar: array[FALSE..TRUE] of String;
    FRecNo: integer;
    function GetHayError: Boolean;
    function GetParametros: TZetaXMLNode;
    function GetFieldInfo( oCampo: TField ): String;
    function GetFieldInfoNET( oCampo: TField ): String;
    function RecEnRango( const iRecCuantos, iRecno, iRecInicial, iRectope: integer ): Boolean;
    procedure ClearError;
    procedure SetError( const sErrorMsg: String );
    procedure BuildEmpresaXML(Datos: TZetaXMLNode; Dataset: TZetaCursor);
    procedure SetHeaderColumna( Datos: TZetaXMLNode; const sContenido: String );
    procedure SetTablaAtributos( Datos: TZetaXMLNode; const sTitulo: String; const iRegistros: Integer );
    procedure SetColumnaInfo( Datos: TZetaXMLNode; const sContenido: WideString; const justifica: TAlignment = taLeftJustify; const sClaseFuente: String = VACIO );
  public
    { Public declarations }
    constructor Create( oProvider: TdmZetaServerProvider );
    destructor Destroy; override;
    property ErrorMsg: String read FErrorMsg;
    property HayError: Boolean read GetHayError;
    property ParamList: TZetaXMLNode read FXMLParamList;
    property Documento: TZetaXMLNode read FXMLDocument;
    property DocErrores: TZetaXMLNode read FXMLDocErrores;
    property HuboErrores: Boolean read FHuboErrores;
    property RecNo: integer read FRecNo;
    function GetNode( Datos: TZetaXMLNode; const sTagName: WideString ): TZetaXMLNode;
    function LoadParametros( const sParametros: WideString ): Boolean;
    function NewXML: TZetaXMLNode;
    function GetDocXML: WideString;
    function SetEmpresaActiva( const sParametros: WideString ): Boolean;
    function TagAsString( Datos: TZetaXMLNode; const sTagName: WideString; const sDefault: String; const lDecrypt: Boolean ): String; overload;
    function TagAsString( Datos: TZetaXMLNode; const sTagName: WideString; const sDefault: String ): String; overload;
    function TagAsInteger( Datos: TZetaXMLNode; const sTagName: WideString; const iDefault: Integer ): Integer;
    function TagAsBoolean( Datos: TZetaXMLNode; const sTagName: WideString; const lDefault: Boolean ): Boolean;
    function TagAsDate( Datos: TZetaXMLNode; const sTagName: WideString; const dDefault: TDate ): TDate;
    function TagAsFloat( Datos: TZetaXMLNode; const sTagName: WideString; const rDefault: Extended ): Extended;
    function ParamAsString( const sParam: WideString ): String;
    function ParamAsInteger( const sParam: WideString ):Integer;
    function ParamBoolean( const sParam: WideString ): Boolean;
    function ParamAsDate( const sParam: WideString ): TDate;
    function ParamAsFloat( const sParam: WideString ): Extended;
    function ParamAsNode( const sParam: WideString ): TZetaXMLNode;
    function GetTagLiga( const sHref, sInfo: String; const sHint: string = VACIO ): WideString;
    function GetTagLigaImagen( const sHref, sFuente: String; const sHint: string = VACIO ): WideString;
    function GetTagCheck(const sCheck: String): WideString;
    function GetTagImagen( const sImagen: String; const sHint: string = VACIO ): WideString;
    function SetComboDataSet( Datos:TZetaXMLNode; oCursor: TZetaCursor; const sNombre, sAlCambiar: String; const lMenu: Boolean; var sDefault: String): TZetaXMLNode;
    function SetComboListaFija( Datos:TZetaXMLNode; ListaFija: ListasFijas; const sNombre, sAlCambiar: String; const lMenu: Boolean; var sDefault: String): TZetaXMLNode;
    function SetComboOtraOpcionDataSet( Datos:TZetaXMLNode; oCursor: TZetaCursor; const sNombre, sAlCambiar: String; const lMenu: Boolean; var sDefault: String): TZetaXMLNode;
    function GetTagLbl(const sLabel: String): WideString;
    function GetTagEdit(const sEdit, sNombre: String; sTamano: String = '5'; sLongitud: String = '0' ): WideString;
    function GetTagRadio( const sRadio, sNombre, sValor: String; lChecked: boolean; lValido: boolean = TRUE; sAlCambiar: string = VACIO ): WideString;
    function GetTagTexto( const sTexto, sNombre: String; lValido: boolean = True; sAlCambiar: string = VACIO ): WideString;
    function GetTagEditPswd(const sEdit, sNombre: String; sTamano: String = '5'; sLongitud: String = '0' ): WideString;
    function GetTagBoton(const sBoton, sNombre: String; const TipoBtn : eTipoBoton = eBtnSubmit; const sOnClick: String = VACIO ): WideString;
    function SetForma( Datos: TZetaXMLNode; const sFormaName, sAccion: String; const lPost: Boolean = TRUE; const sDeshacer: String = VACIO; const sValidar: String = VACIO; const sCargar: String = VACIO; const sDestino: String = VACIO ): TZetaXMLNode;
    function SetRenglon(Datos: TZetaXMLNode): TZetaXMLNode;
    function SetSeccion(Datos: TZetaXMLNode): TZetaXMLNode;
    function SetTablaMaster( Datos: TZetaXMLNode; const justifica: TAlignment = taCenter ): TZetaXMLNode;
    function SetMaster( Datos: TZetaXMLNode ): TZetaXMLNode;
    function SetContenido( Datos: TZetaXMLNode; const sContenido: WideString; const justifica: TAlignment = taLeftJustify; const sClaseFuente: String = VACIO ): TZetaXMLNode;
//    function SetContenidoBis( Datos: TZetaXMLNode; const sContenido: WideString; const justifica: TAlignment = taLeftJustify; const sClaseFuente: String = VACIO ): TZetaXMLNode;    
    function SetNewCombo( Datos:TZetaXMLNode; const sNombre, sAlCambiar: String; const lMenu: Boolean ): TZetaXMLNode;
    function AgregaFooter( Datos: TZetaXMLNode; lLinea: boolean = TRUE ): TZetaXMLNode;
    function AgregaDescripcionXML( Datos: TZetaXMLNode; const sModulo, sPantalla: String ): TZetaXMLNode;
    function FechaCortaXML( const dReferencia: TDateTime ): String;
    function ValidaHRef( const sHref: String; const lChecar: boolean = FALSE  ): String;
    function ValidaTexto( const sTexto: String ): String;        
    procedure AddDataset( Datos: TZetaXMLNode; const sDatasetTag, sRecordTag: WideString; oCursor: TZetaCursor );
    procedure AddLookup( Datos: TZetaXMLNode; const sSeccionTag, sRecordTag: WideString; const sQuery: String ); overload;
    procedure AddLookup( Datos: TZetaXMLNode; const sSeccionTag, sRecordTag: WideString; const sQuery, sCampo, sDescripcion: String ); overload;
    procedure AddDataset2( Datos: TZetaXMLNode; const sDatasetTag: WideString; oCursor: TZetaCursor; const iRecCuantos: Integer = 0; const iRecInicial: Integer = 1 );
    procedure AddRegistro( Datos: TZetaXMLNode; const sDatasetTag: WideString; oCursor: TZetaCursor );
    procedure AddCodDefault( Datos: TZetaXMLNode; const sDataSetTag: WideString; const sCodigo: String );
    procedure AddListaFija( Datos: TZetaXMLNode; const sDataSetTag: WideString; const sComboLabel: String; ListaFija: ListasFijas );
    procedure BuildErrorXML; overload;
    procedure BuildErrorXML( const sError: String ); overload;
    procedure BuildWarningXML( const sWarning: String );    
    procedure ListarEmpresa( Datos: TZetaXMLNode; const sScript: String );
    procedure ListarEmpresas( Datos: TZetaXMLNode; const sScript: String );
    procedure AsignaFieldValue( const sValue: WideString; oCampo: TField );
    procedure TraspasaRegistro( oDataSet: TDataSet; Datos: TZetaXMLNode );
    procedure AgregaRegistro( oDataSet: TDataSet; const sTagParametro: String );
    procedure AgregaTabla( oDataSet: TDataSet; const sTagParametro: String );
    procedure AgregaCursor( Datos: TZetaXMLNode; oDataSet: TZetaCursor; sTitulo: String = VACIO; sLigaPagina: string = VACIO; iCuantos: integer = 0; iPagina: integer = 1  );
    procedure AgregaDataSet( Datos: TZetaXMLNode; oDataSet: TDataSet; sTitulo: String = VACIO; sLigaPagina: string = VACIO; iCuantos: integer = 0; iPagina: integer = 1 );
    procedure AgregaEspacio( Datos:TZetaXMLNode );
    procedure AgregaLinea( Datos:TZetaXMLNode );
    procedure AgregaTexto( Datos:TZetaXMLNode; sTexto: string );
    procedure AgregaHiddenEdit(Datos: TZetaXMLNode; const sHiddenName, sValue: String);
    procedure AgregaMeta( Datos: TZetaXMLNode; const sEquiv, sContent: String );
    procedure SetComboOpcion( Datos:TZetaXMLNode; const sValue, sDescrip: String; const lSelected: Boolean = FALSE );
    procedure SetNodoContenido( Datos: TZetaXMLNode; const sContenido: WideString );
    procedure SetClaseFuentePar( const sFuentePar, sFuenteImpar: String );
    procedure SetBoton( Datos: TZetaXMLNode; const sBoton, sNombre: String; const TipoBtn : eTipoBoton = eBtnSubmit; const sOnClick: String = VACIO  );
    procedure SetTagLiga( Datos: TZetaXMLNode; const sHref, sInfo: String; const sHint: String = VACIO; const sDestino: String = VACIO );
    procedure SetTagLigaImagen( Datos: TZetaXMLNode; const sHref, sFuente: String; const sHint: string = VACIO; const sDestino: String = VACIO );
    procedure AgregaXMLDataSet( oNodo: TZetaXMLNode; sTitulo, sScript: String; oCursor: TZetaCursor; sLigaPagina: string = VACIO; iCuantos: integer = 0; iPagina: integer = 1  );
    procedure AgregaOnLoad( Datos: TZetaXMLNode; const sContent: String );
    procedure AgregaXMLQueryNET( oNodo: TZetaXMLNode; const sTitulo, sScript: String; const lValidarEmpty: Boolean = FALSE );
    procedure AgregaXMLCursorNET( oNodo: TZetaXMLNode; oCursor: TZetaCursor; const sTitulo: String; const lValidarEmpty: Boolean = FALSE );
    procedure AgregaXMLCursorDataNET( oNodo: TZetaXMLNode; oCursor: TZetaCursor; const sTitulo: String );
    procedure AgregaXMLDataSetDataNET( oNodo: TZetaXMLNode; oDataSet: TDataSet; const sTitulo: String );
  end;

implementation

uses ZetaCommonTools,
     Variants,
     ZetaServerTools;

const
     K_ROOT: WideString = 'RAIZ';
     K_EMPRESA: WideString = 'EMPRESA';
     K_PARAMETROS: WideString = 'PARAMETROS';
     K_EMPRESAS: WideString = 'EMPRESAS';
     K_CODIGO: WideString = 'CODIGO';
     K_NOMBRE: WideString = 'NOMBRE';
     K_USUARIO: WideString = 'USERNAME';
     K_CLAVE: WideString = 'PASSWORD';
     K_USCODIGO: WideString = 'USUARIO';
     K_NIVEL0: WideString = 'NIVEL0';
     K_DATOS: WideString = 'DATOS';
     K_ERRORES: WideString = 'ERRORES';
     K_WARNINGS: WideString = 'WARNINGS';     
     K_ERROR: WideString = 'ERROR';
     K_WARNING: WideString = 'WARNING';     
     K_INT_TRUE = 1;
     K_INT_FALSE = 0;
     K_ROWS_TAG = 'ROWS';
     K_ROWS_CUANTOS_ATTR = 'cuantos';
     K_ROW_TAG = 'ROW';
     K_PAR_TAG = 'PAR';
     K_LABELS_TAG = 'LABELS';

     // Nuevo Modelo con PlanCarrera.xsl
     K_TABLA_TAG = 'TABLA';
     K_TITULO_ATTR = 'titulo';
     K_REGISTRO_ATTR = 'registros';
     K_ENCABEZADO_TAG = 'ENCABEZADO';
     K_HEADER_TAG = 'HEADER';
     K_RENGLON_TAG = 'RENGLON';
     K_COLUMNA_TAG = 'COLUMNA';
     K_LIGA_TAG = 'LIGA';
     K_LIGA_IMG_TAG = 'LIGAIMG';
     K_XML_HEADER = '<?xml version="1.0" encoding="ISO-8859-1"?>';
     K_IMAGEN_TAG = 'IMAGEN';
     K_DESCRIPCIONES_TAG = 'DESCRIPCIONES';
     K_MODULO_TAG = 'MODULO';
     K_PANTALLA_TAG = 'PANTALLA';
     K_MASTER_TAG = 'MASTER';
     K_TABLAMASTER_TAG = 'TABLAMASTER';
     K_FORMA_TAG = 'FORMA';
     K_NOMBRE_ATTR = 'nombre';              // Se justifica declarar este atributo por que se usa en muchos tags
     K_HIDDEN_TAG = 'HIDDEN';
     K_CONTENIDO_TAG = 'CONTENIDO';
     K_LABEL_TAG = 'LABEL';
     K_COMBO_TAG = 'COMBO';
     K_MENU_TAG = 'MENU';
     K_EDIT_TAG = 'EDIT';
     K_EDIT_PSWD_TAG = 'EDITPSWD';     
     K_BTNSUBMIT_TAG = 'BTNSUBMIT';
     K_BTNRESET_TAG = 'BTNRESET';
     K_BTNNORMAL_TAG = 'BOTON';
     K_COMBO_OPCION_TAG = 'OPCION';
     K_COMBO_OPCION_SELECTED_TAG = 'OPCIONSELECTED';
     K_FOOTER_TAG = 'FOOTER';
     K_LINEA_TAG = 'LINEA';
     K_ESPACIO_TAG = 'ESPACIO';
     K_CHECK_TAG = 'CHECK';
     K_NOTCHECK_TAG = 'NOTCHECK';
     K_ALINEACION = 'alineacion';
     K_CLASE_FUENTE = 'clasefuente';
     K_META_TAG = 'META';
     K_TEXTO_TAG = 'TEXTO';
     K_ALCARGAR = 'CARGAR';
     K_OPTION_TAG = 'RADIO';
     K_OPTION_CHECKED_TAG = 'RADIOCHECK';
     K_OPTION_ABLE_TAG = 'RADIOABLE';
     K_OPTION_CHECKED_ABLE_TAG = 'RADIOCHECKABLE';
     K_TEXTAREA_ABLE_TAG = 'TEXTAREAABLE';
     K_TEXTAREA_TAG = 'TEXTAREA';
     K_SECCION_TAG = 'SECCION';
     { ********** Constantes de Paginaci�n *********** }
     K_PAGINACION = 'P�gina %d de %d';
     K_RESULTADOS_SIN_TITULO = 'Resultados %d - %d de un total de %d';
     K_RESULTADOS = '%d - %d de un total de %d';     
     K_IMG_ANTERIOR = 'Anterior.gif';
     K_IMG_SIGUIENTE = 'Siguiente.gif';
     K_ANTERIOR = ' P�gina Anterior';
     K_SIGUIENTE = 'P�gina Siguiente';
     K_RANGO = 10;

     aTagBoton: array[ eTipoBoton ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_BTNSUBMIT_TAG, K_BTNRESET_TAG, K_BTNNORMAL_TAG );

{ ********** TZetaXML *********** }

constructor TZetaXML.Create( oProvider: TdmZetaServerProvider );
begin
     oZetaProvider := oProvider;
     FXMLFactory := CreateComObject( CLASS_XmlFactory ) as IXMLFactory;
     FXMLDocument := NewXML;
     FXMLDocErrores := NewXML;
     FHuboErrores := FALSE;
     SetClaseFuentePar( VACIO, VACIO );
end;

destructor TZetaXML.Destroy;
begin
     FXMLFactory := nil;
     inherited Destroy;
end;

function TZetaXML.FechaCortaXML( const dReferencia: TDateTime ): String;
begin
     if ( dReferencia <= 0 ) then
        Result := VACIO
     else
         Result := FormatDateTime( 'dd/mmm/yyyy', dReferencia );
end;

function TZetaXML.NewXML: TZetaXMLNode;
begin
     Result := FXMLFactory.NewXML;
     Result.Tag := K_ROOT;
     Result.Encoding := 'ISO-8859-1';
end;

function TZetaXML.GetDocXML: WideString;
begin
     if FHuboErrores then
        Result := DocErrores.GetXML
     else
        Result := Documento.GetXML;
end;

procedure TZetaXML.ClearError;
begin
     FErrorMsg := VACIO;
end;

procedure TZetaXML.SetError( const sErrorMsg: String );
begin
     FErrorMsg := sErrorMsg;
end;

function TZetaXML.GetHayError: Boolean;
begin
     Result := ZetaCommonTools.StrLleno( FErrorMsg );
end;

procedure TZetaXML.SetClaseFuentePar( const sFuentePar, sFuenteImpar: String );
begin
     aClaseFuentePar[ FALSE ] := sFuenteImpar;
     aClaseFuentePar[ TRUE ] := sFuentePar;
end;

function TZetaXML.LoadParametros( const sParametros: WideString ): Boolean;
begin
     ClearError;
     if Assigned( FXMLParamList ) then
        FXMLParamList.Clear
     else
         FXMLParamList := NewXML;
     with FXMLParamList do
     begin
          Clear;
          Result := ( LoadXML( sParametros ) = K_INT_TRUE );
          if not Result then
             SetError( 'Par�metros Inv�lidos' );
     end;
end;

function TZetaXML.SetEmpresaActiva( const sParametros: WideString ): Boolean;
var
   Empresa: TZetaXMLNode;
   oEmpresa: OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
}
begin
     Result := LoadParametros( sParametros );
     if Result then
     begin
          with FXMLParamList do
          begin
               Empresa := GetNode( GetRoot, K_EMPRESA );
               if Assigned( Empresa ) then
               begin
                    try
                       oEmpresa := VarArrayOf( [ TagAsString( Empresa, K_DATOS, VACIO, True ),
                                                 TagAsString( Empresa, K_USUARIO, VACIO ),
                                                 TagAsString( Empresa, K_CLAVE, VACIO ),
                                                 TagAsInteger( Empresa, K_USCODIGO, 0 ),
                                                 TagAsString( Empresa, K_NIVEL0, VACIO ),
                                                 TagAsString( Empresa, K_CODIGO, VACIO ) ] );
                       oZetaProvider.EmpresaActiva := oEmpresa;
                    except
                          on Error: Exception do
                          begin
                               Result := False;
                               SetError( Error.Message );
                          end;
                    end;
               end
               else
               begin
                    Result := False;
                    SetError( 'Empresa No Especificada' );
               end;
          end;
     end;
end;

{
Severity:
=========

1 = Error ( Rojo )
2 = Advertencia ( Azul )
3 = Mensaje ( Negro )

<raiz>
   <datos>
   </datos>
</raiz>
<errores>
  <error severity="1">texto del error</error>
  <error severity="2">texto de la advertencia</error>
  <error severity="3">texto del mensaje</error>
</errores>
}

procedure TZetaXML.BuildErrorXML( const sError: String );
var
   Errores: TZetaXMLNode;
begin
     Errores := DocErrores.GetRoot.FindOrAddNewChild( K_ERRORES );
     if Assigned( Errores ) then
     begin
          Errores.NewChild( K_ERROR, sError );
     end;
     FHuboErrores := TRUE;
end;

procedure TZetaXML.BuildErrorXML;
begin
     BuildErrorXML( ErrorMsg );
end;

procedure TZetaXML.BuildWarningXML( const sWarning: String );
var
   Warning: TZetaXMLNode;

begin
     Warning := Documento.GetRoot.FindOrAddNewChild( K_WARNINGS );
     if Assigned( Warning ) then
     begin
          Warning.NewChild( K_WARNING, sWarning );
     end;
end;

procedure TZetaXML.BuildEmpresaXML(Datos: TZetaXMLNode; Dataset: TZetaCursor);
var
   Empresa: TZetaXMLNode;
begin
     Empresa := Datos.NewChild( K_EMPRESA, VACIO );
     with Empresa do
     begin
          with Dataset do
          begin
               if not IsEmpty then
               begin
                    NewChild( K_CODIGO, FieldByName( 'CM_CODIGO' ).AsString );
                    NewChild( K_NOMBRE, FieldByName( 'CM_NOMBRE' ).AsString );
                    NewChild( K_USUARIO, FieldByName( 'CM_USRNAME' ).AsString );
                    NewChild( K_CLAVE, FieldByName( 'CM_PASSWRD' ).AsString );
                    NewChild( K_NIVEL0, FieldByName( 'CM_NIVEL0' ).AsString );
                    NewChild( K_DATOS, ZetaServerTools.Encrypt( FieldByName( 'CM_DATOS' ).AsString ) );
               end;
          end;
     end;
end;

procedure TZetaXML.ListarEmpresa(Datos: TZetaXMLNode; const sScript: String);
var
   FQuery: TZetaCursor;
begin
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FQuery := CreateQuery( sScript );
          try
             with FQuery do
             begin
                  Active := True;
                  BuildEmpresaXML( Datos, FQuery );
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

procedure TZetaXML.ListarEmpresas(Datos: TZetaXMLNode; const sScript: String);
var
   FQuery: TZetaCursor;
   Empresas: TZetaXMLNode;
begin
     Empresas := Datos.GetRoot.NewChild( K_EMPRESAS, VACIO );
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          FQuery := CreateQuery( sScript );
          try
             with FQuery do
             begin
                  Active := True;
                  while not Eof do
                  begin
                       BuildEmpresaXML( Empresas, FQuery );
                       Next;
                  end;
                  Active := False;
             end;
          finally
                 FreeAndNil( FQuery );
          end;
     end;
end;

procedure TZetaXML.AddDataset( Datos: TZetaXMLNode; const sDatasetTag, sRecordTag: WideString; oCursor: TZetaCursor );
var
   Dataset, Registro, Campo: TZetaXMLNode;
   i: Integer;
   sValue: String;
begin
     with Datos do
     begin
          Dataset := Datos.GetRoot.FindOrAddNewChild( sDatasetTag );
          with oCursor do
          begin
               while not Eof do
               begin
                    Registro := Dataset.NewChild( sRecordTag, VACIO );
                    with Registro do
                    begin
                         for i := 0 to ( FieldCount - 1 ) do
                         begin
                              with Fields[ i ] do
                              begin
                                   case DataType of
                                        ftSmallint, ftInteger, ftWord:
                                        begin
                                             if ( Tag > 0 ) then
                                                sValue := ZetaCommonLists.ObtieneElemento( ListasFijas( Tag ), AsInteger )
                                             else
                                                sValue := IntToStr( AsInteger );
                                        end;
                                        ftString: sValue := AsString;
                                        ftBoolean: sValue := ZetaCommonTools.zBoolToStr( AsBoolean );
                                        ftFloat: sValue := AsString; //FormatFloat( '#.##', AsFloat );
                                        ftCurrency: sValue := FormatFloat( '#.00', AsFloat );
                                        ftDate: sValue := FormatDateTime( 'yyyymmdd', AsDateTime );
                                        ftTime: sValue := FormatDateTime( 'hhnn', AsDateTime );
                                        ftDateTime: sValue := FormatDateTime( 'yyyymmdd', AsDateTime );
                                        ftMemo: sValue := ZetaCommonTools.BorraCReturn( AsString );
                                   else
                                       sValue := ZetaCommonTools.BorraCReturn( AsString );
                                   end;
                                   Campo := Registro.NewChild( FieldName, sValue );
                                   if ZetaCommonTools.StrLleno( DisplayLabel ) then
                                      Campo.AddAttribute( K_LETRERO, DisplayLabel );
                              end;
                         end;
                    end;
                    Next;
               end;
          end;
     end;
end;

procedure TZetaXML.AddDataset2( Datos: TZetaXMLNode; const sDatasetTag: WideString; oCursor: TZetaCursor; const iRecCuantos: Integer = 0; const iRecInicial: Integer = 1 );
var
   Dataset, Registro, Campo: TZetaXMLNode;
   i, iRecTope, iCuantos: Integer;

begin
     with Datos do
     begin
          Dataset := Datos.FindOrAddNewChild( sDatasetTag );
          with oCursor do
          begin
               // Agrega Labels
               Registro := Dataset.NewChild( K_LABELS_TAG, VACIO );
               for i := 0 to ( FieldCount - 1 ) do
               begin
                    with Fields[ i ] do
                         Registro.AddAttribute( FieldName, DisplayLabel );
               end;
               // Agrega Registros
               iRecTope := iRecInicial + iRecCuantos - 1;
               FRecNo := 0;
               iCuantos := 0;
               Registro := Dataset.NewChild( K_ROWS_TAG, VACIO );
               while not Eof do
               begin
                    Inc( FRecNo );
                    if RecEnRango( iRecCuantos, FRecNo, iRecInicial, iRecTope ) then
                    begin
                         with Registro do
                         begin
                              // Agrega Registros
                              Campo := Registro.NewChild( K_ROW_TAG, VACIO );
                              Campo.AddAttribute( K_PAR_TAG, IntToStr( ( ( FRecNo - iRecInicial ) + 1 ) Mod 2 ) );
                              for i := 0 to ( FieldCount - 1 ) do
                              begin
                                   Campo.AddAttribute( Fields[ i ].FieldName, GetFieldInfo( Fields[i] ) );
                              end;
                         end;
                         Inc( iCuantos );
                    end;
                    Next;
               end;
               Registro.AddAttribute( K_ROWS_CUANTOS_ATTR, IntToStr( iCuantos ) );
          end;
     end;
end;

function TZetaXML.GetFieldInfo( oCampo: TField ): String;
var
   sTexto: string;
begin
     with oCampo do
     begin
          sTexto := DisplayText;
          if ( not ( DataType in [ ftBlob, ftMemo ] ) ) and ( sTexto <> AsString ) then           // Si se tiene un formateo por OnGetText, se deja este
             Result := sTexto
          else
          begin
               case DataType of
                    ftSmallint, ftInteger, ftWord: Result := IntToStr( AsInteger );
                    ftString: Result := AsString;
                    ftBoolean: Result := ZetaCommonTools.zBoolToStr( AsBoolean );
                    ftFloat: Result := FormatFloat( '#.##', AsFloat );
                    ftCurrency: Result := FormatFloat( '#.00', AsFloat );
                    ftDate, ftDateTime: Result := FechaCortaXML( AsDateTime );
                    ftTime: Result := FormatDateTime( 'hhnn', AsDateTime );
                    //ftMemo: Result := ZetaCommonTools.BorraCReturn( AsString );
               else
                   Result := AsString; //ZetaCommonTools.BorraCReturn( AsString );
               end;
          end;
     end;
end;

function TZetaXML.RecEnRango( const iRecCuantos, iRecno, iRecInicial, iRectope: integer ): Boolean;
begin
     Result := ( iRecCuantos = 0 ) or ( ( iRecno >= iRecInicial ) and ( iRecno <= iRecTope ) );
end;

procedure TZetaXML.AddCodDefault( Datos: TZetaXMLNode; const sDataSetTag: WideString; const sCodigo: String );
var
   Dataset: TZetaXMLNode;
begin
     with Datos do
     begin
          Dataset := Datos.FindOrAddNewChild( sDatasetTag );
          Dataset.NewChild( 'COD_DEFAULT', Trim( sCodigo ) );
     end;
end;

procedure TZetaXML.AddListaFija( Datos: TZetaXMLNode; const sDataSetTag: WideString; const sComboLabel: String; ListaFija: ListasFijas );
const
     K_TAG_COD_LISTAFIJA = 'codigo';                  // En minusculas por que no son campos de la Base de Datos
     K_TAG_DES_LISTAFIJA = 'descripcion';
var
   i: Integer;
   oLista: TStrings;
   Dataset, Registro, Campo: TZetaXMLNode;
begin
     oLista := TStringList.Create;
     try
        ZetaCommonLists.LlenaLista( ListaFija, oLista );
        with oLista do
        begin
             if ( Count > 0 ) then
             begin
                  DataSet := Datos.FindOrAddNewChild( sDatasetTag );
                  // Agrega Labels
                  Registro := Dataset.NewChild( K_LABELS_TAG, VACIO );
                  Registro.AddAttribute( K_TAG_COD_LISTAFIJA, sComboLabel );
                  Registro.AddAttribute( K_TAG_DES_LISTAFIJA, 'Descripci�n' );
                  // Agrega Registros
                  FRecNo := 0;
                  Registro := Dataset.NewChild( K_ROWS_TAG, VACIO );
                  Registro.AddAttribute( K_ROWS_CUANTOS_ATTR, IntToStr( Count ) );
                  for i := 0 to Count - 1 do
                  begin
                       Campo := Registro.NewChild( K_ROW_TAG, VACIO );
                       Campo.AddAttribute( K_PAR_TAG, IntToStr( ( FRecNo Mod 2 ) ) );
                       Campo.AddAttribute( K_TAG_COD_LISTAFIJA, IntToStr( i ) );
                       Campo.AddAttribute( K_TAG_DES_LISTAFIJA, ObtieneElemento( ListaFija, i ) );
                  end
             end;
        end;
     finally
        FreeAndNil( oLista );
     end;
end;

procedure TZetaXML.AddRegistro( Datos: TZetaXMLNode; const sDatasetTag: WideString; oCursor: TZetaCursor );
var
   Registro: TZetaXMLNode;
   i: Integer;
begin
     Registro := Datos.FindOrAddNewChild( sDatasetTag );
     with oCursor do
     begin
          for i := 0 to ( FieldCount - 1 ) do
          begin
               Registro.AddAttribute( Fields[ i ].FieldName, GetFieldInfo( Fields[i] ) );
          end;
     end;
end;

procedure TZetaXML.AddLookup( Datos: TZetaXMLNode; const sSeccionTag, sRecordTag: WideString; const sQuery, sCampo, sDescripcion: String );
var
   FCursor: TZetaCursor;
   Seccion, Registro: TZetaXMLNode;
begin
     FCursor := oZetaProvider.CreateQuery( sQuery );
     try
        Seccion := Datos.NewChild( sSeccionTag, VACIO );
        with FCursor do
        begin
             Active := True;
             while not Eof do
             begin
                  Registro := Seccion.NewChild( sRecordTag, FieldByName( sCampo ).AsString );
                  Registro.AddAttribute( K_DESCRIPCION, FieldByName( sDescripcion ).AsString );
                  Next;
             end;
             Active := False;
        end;
     finally
            FreeAndNil( FCursor );
     end;
end;

procedure TZetaXML.AddLookup( Datos: TZetaXMLNode; const sSeccionTag, sRecordTag: WideString; const sQuery: String );
begin
     AddLookup( Datos, sSeccionTag, sRecordTag, sQuery, 'TB_CODIGO', 'TB_ELEMENT' );
end;

function TZetaXML.GetNode( Datos: TZetaXMLNode; const sTagName: WideString ): TZetaXMLNode;
begin
     with Datos do
     begin
          if ( NumChildrenHavingTag( sTagName ) > 0 ) then
             Result := SearchForTag( Datos, sTagName )
          else
              Result := nil;
     end;
end;

function TZetaXML.TagAsBoolean( Datos: TZetaXMLNode;
                                const sTagName: WideString;
                                const lDefault: Boolean): Boolean;
var
   Nodo: TZetaXMLNode;
begin
     Nodo := GetNode( Datos, sTagName );
     if Assigned( Nodo ) then
     begin
          Result := ZetaCommonTools.zStrToBool( Nodo.Content )
     end
     else
         Result := lDefault;
end;

function TZetaXML.TagAsDate( Datos: TZetaXMLNode;
                             const sTagName: WideString;
                             const dDefault: TDate): TDate;
var
   Nodo: TZetaXMLNode;
begin
     Nodo := GetNode( Datos, sTagName );
     if Assigned( Nodo ) then
        Result := ZetaCommonTools.StrToFecha( Nodo.Content )
     else
         Result := dDefault;
end;

function TZetaXML.TagAsFloat( Datos: TZetaXMLNode;
                              const sTagName: WideString;
                              const rDefault: Extended): Extended;
var
   Nodo: TZetaXMLNode;
begin
     Nodo := GetNode( Datos, sTagName );
     if Assigned( Nodo ) then
        Result := StrToFloat( Nodo.Content )
     else
         Result := rDefault;
end;

function TZetaXML.TagAsInteger( Datos: TZetaXMLNode;
                                const sTagName: WideString;
                                const iDefault: Integer): Integer;
var
   Nodo: TZetaXMLNode;
begin
     Nodo := GetNode( Datos, sTagName );
     if Assigned( Nodo ) then
        Result := StrToIntDef( Nodo.Content, 0 )
     else
         Result := iDefault;
end;

function TZetaXML.TagAsString( Datos: TZetaXMLNode; const sTagName: WideString; const sDefault: String; const lDecrypt: Boolean ): String;
var
   Nodo: TZetaXMLNode;
begin
     Nodo := GetNode( Datos, sTagName );
     if Assigned( Nodo ) then
     begin
          Result := Nodo.Content;
          if lDecrypt then
             Result := ZetaServerTools.Decrypt( Result );
     end
     else
         Result := sDefault;
end;

function TZetaXML.TagAsString( Datos: TZetaXMLNode; const sTagName: WideString; const sDefault: String): String;
begin
     Result := TagAsString( Datos, sTagName, sDefault, False );
end;

function TZetaXML.GetParametros: TZetaXMLNode;
begin
     with FXMLParamList do
     begin
          Result := GetNode( GetRoot, K_PARAMETROS );
          if not Assigned( Result ) then
          begin
               SetError( 'Error! No se Existe Secci�n de Par�metros' );
          end;
     end;
end;

function TZetaXML.ParamAsString(const sParam: WideString): String;
begin
     Result := TagAsString( GetParametros, sParam, VACIO );
end;

function TZetaXML.ParamAsDate(const sParam: WideString): TDate;
begin
     Result := TagAsDate( GetParametros, sParam, NullDateTime );
end;

function TZetaXML.ParamAsFloat(const sParam: WideString): Extended;
begin
     Result := TagAsFloat( GetParametros, sParam, 0 );
end;

function TZetaXML.ParamAsInteger(const sParam: WideString): Integer;
begin
     Result := TagAsInteger( GetParametros, sParam, 0 );
end;

function TZetaXML.ParamBoolean(const sParam: WideString): Boolean;
begin
     Result := TagAsBoolean( GetParametros, sParam, False );
end;

function TZetaXML.ParamAsNode( const sParam: WideString ): TZetaXMLNode;
begin
     Result := GetNode( GetParametros, sParam );
end;

procedure TZetaXML.AsignaFieldValue( const sValue: WideString; oCampo: TField );
var
   sFecha: String;
begin
     if strLleno( sValue ) then
        with oCampo do
        begin
             case DataType of
                  ftSmallint, ftInteger, ftWord: AsInteger := StrToIntDef( sValue, 0 );
                  ftString: AsString := sValue;
                  ftBoolean: AsBoolean := ZetaCommonTools.zStrToBool( sValue );
                  ftFloat, ftCurrency: AsFloat := StrToFloat( sValue );
                  ftDate, ftTime, ftDateTime:
                  begin
                       sFecha := sValue;
                       AsDateTime := strAsFecha( sFecha );    // dd/mm/yyyy
                  end;
                  ftMemo: AsString := ZetaCommonTools.BorraCReturn( sValue );
             else
                  AsString := ZetaCommonTools.BorraCReturn( sValue );
             end;
        end;
end;

procedure TZetaXML.TraspasaRegistro( oDataSet: TDataSet; Datos: TZetaXMLNode );
var
   i : Integer;
begin
     with oDataSet do
          for i := 0 to FieldCount - 1 do
              AsignaFieldValue( Datos.GetAttrValue( Fields[i].FieldName ), Fields[i] )
end;

procedure TZetaXML.AgregaRegistro(oDataSet: TDataSet; const sTagParametro: String);
begin
     oDataSet.Append;
     TraspasaRegistro( oDataSet, GetNode( GetParametros, sTagParametro ) );
     oDataSet.Post;
end;

procedure TZetaXML.AgregaEspacio( Datos:TZetaXMLNode );
begin
     Datos.NewChild( K_ESPACIO_TAG, VACIO );
end;

procedure TZetaXML.AgregaLinea( Datos:TZetaXMLNode );
begin
     Datos.NewChild( K_LINEA_TAG, VACIO );
end;

procedure TZetaXML.AgregaTexto( Datos:TZetaXMLNode; sTexto: string );
begin
     Datos.NewChild( K_TEXTO_TAG, sTexto );
end;

procedure TZetaXML.AgregaTabla(oDataSet: TDataSet; const sTagParametro: String);
var
   NodoTabla, NodoRow: TZetaXMLNode;
begin
     NodoTabla := GetNode( GetParametros, sTagParametro );
     NodoTabla := GetNode( NodoTabla, 'ROWS' );
     if ( NodoTabla <> nil ) then
     begin
          NodoRow := NodoTabla.FirstChild;
          while ( NodoRow <> nil ) do
          begin
               oDataSet.Append;
               TraspasaRegistro( oDataSet, NodoRow );
               oDataSet.Post;
               NodoRow := NodoRow.NextSibling;
          end;
     end;
end;

procedure TZetaXML.AgregaCursor( Datos: TZetaXMLNode; oDataSet: TZetaCursor; sTitulo: String = VACIO; sLigaPagina: string = VACIO; iCuantos: integer = 0; iPagina: integer = 1 );
var
   NodoTabla, NodoRenglon, NodoColumna: TZetaXMLNode;
   i, iRecInicial, iRecTope, iPaginas, iRango, iInicio, iFinal: Integer;
   iFinPag: integer;

   procedure GetPaginacion;
   var
      x, iInicioPag: integer;
   begin
        if ( iCuantos <> 0 ) and ( FRecno > iCuantos ) then
        begin
             AgregaEspacio( Datos );
             iPaginas := iMin( 1, ( FRecNo mod iCuantos ) ) + ( FRecNo div iCuantos );
             NodoTabla := Datos.NewChild( K_TABLA_TAG, VACIO );
             if ( NodoTabla <> nil ) then
             begin
                  NodoRenglon := NodoTabla.NewChild( K_RENGLON_TAG, VACIO );
                  if ( NodoRenglon <> nil ) then
                  begin

                       { ************ Imagen de P�gina Anterior *********** }
                       if ( iPagina <> 1 ) then
                          SetContenido( NodoRenglon, GetTagLigaImagen( sLigaPagina + '&PAGINA=' + IntToStr( iPagina - 1 ), K_IMG_ANTERIOR, K_ANTERIOR ), taLeftJustify, aClaseFuentePar[ TRUE ] )
                       else
                           SetContenido( NodoRenglon, GetTagImagen( K_IMG_ANTERIOR, K_ANTERIOR ), taLeftJustify, aClaseFuentePar[ TRUE ] );

                       { *********** Ligas de las p�ginas que tuvieron resultados *********** }
                       NodoColumna := SetContenido( NodoRenglon, VACIO, taCenter, aClaseFuentePar[ FALSE ] );
                       iRango := ( iPagina div K_RANGO );

                       { ******* P�ginas Multiplos de 10 ********** }
                       if ( ( iPagina mod K_RANGO ) = 0 )then
                          iRango := iRango - 1;

                       { ******* Liga de las 10 p�ginas anteriores ******* }
                       if ( iRango >= 1 ) then
                       begin
                            iInicio := ( ( iRango - 1 ) * K_RANGO ) + 1;
                            setTagLiga( NodoColumna, sLigaPagina + '&PAGINA=' + IntToStr( iInicio ), '�� Anteriores 10 P�ginas ', 'P�gina ' + IntToStr( iInicio ) );
                            AgregaTexto( Nodocolumna, ' - ' );
                       end;
                       iInicioPag := ( ( iRango ) * K_RANGO ) + 1;
                       iFinPag := iInicioPag + ( K_RANGO - 1 ); //9
                       if ( iFinPag > iPaginas ) then
                          iFinPag := iPaginas;
                       for x := iInicioPag to iFinPag do
                       begin
                            if ( x = iPagina ) then
                            begin
                                 AgregaTexto( NodoColumna, '[ ' );
                                 AgregaTexto( NodoColumna, IntToStr( x ) );
                                 AgregaTexto( NodoColumna, ' ]' );
                            end
                            else
                                setTagLiga( NodoColumna, sLigaPagina + '&PAGINA=' + IntToStr( x ), IntToStr( x ), 'P�gina ' + IntToStr( x ) );
                            if ( x < iFinPag ) then
                               AgregaTexto( NodoColumna, ' - ' );
                       end;

                       { ****** Liga de las 10 p�ginas siguientes ****** }
                       iFinal := ( ( iRango + 1 ) * K_RANGO ) + 1;
                       if ( iFinal <= iPaginas ) then
                       begin
                            AgregaTexto( NodoColumna, ' - ' );
                            setTagLiga( NodoColumna, sLigaPagina + '&PAGINA=' + IntToStr( iFinal ), ' Siguientes 10 P�ginas ��', 'P�gina ' + IntToStr( iFinal ) );
                       end;

                       { ******* Imagen de siguiente en la navegaci�n de las p�ginas ****** }
                       if ( iPagina <> iPaginas ) then
                          SetContenido( NodoRenglon, GetTagLigaImagen( sLigaPagina + '&PAGINA=' + IntToStr( iPagina + 1 ), K_IMG_SIGUIENTE, K_SIGUIENTE ), taRightJustify, aClaseFuentePar[ TRUE ] )
                       else
                           SetContenido( NodoRenglon, GetTagImagen( K_IMG_SIGUIENTE, K_SIGUIENTE ), taRightJustify, aClaseFuentePar[ TRUE ] );
                  end;
             end;
             SetTablaAtributos( NodoTabla, Format( K_PAGINACION, [ iPagina, iPaginas ] ), iPaginas );
        end;
   end;

begin
     FRecno := 0;
     iRecInicial := ( ( iPagina - 1 ) * icuantos ) + 1;
     iRecTope := iRecInicial + iCuantos - 1;
     NodoTabla := Datos.NewChild( K_TABLA_TAG, VACIO );
     if ( NodoTabla <> nil ) then
     begin
          with oDataSet do
          begin
               if ( not IsEmpty ) then
               begin
                    // Headers
                    NodoRenglon := NodoTabla.NewChild( K_ENCABEZADO_TAG, VACIO );
                    if ( NodoRenglon <> nil ) then
                    begin
                         for i := 0 to ( FieldCount - 1 ) do
                         begin
                              if Fields[ i ].Visible then
                                 SetHeaderColumna( NodoRenglon, Fields[ i ].DisplayLabel );
                         end;
                    end;
                    // Renglones
                    while not EOF do
                    begin
                         Inc( FRecNo );
                         if RecEnRango( iCuantos, FRecNo, iRecInicial, iRecTope ) then
                         begin
                              NodoRenglon := NodoTabla.NewChild( K_RENGLON_TAG, VACIO );
                              if ( NodoRenglon <> nil ) then
                              begin
                                   for i := 0 to ( FieldCount - 1 ) do
                                   begin
                                        if Fields[ i ].Visible then
                                        begin
                                             NodoColumna := NodoRenglon.NewChild( K_COLUMNA_TAG, VACIO );
                                             SetColumnaInfo( NodoColumna, GetFieldInfo( Fields[i] ), Fields[i].Alignment, aClaseFuentePar[ ( FRecNo Mod 2 ) = 0 ] );
                                        end;
                                   end;
                              end;
                         end;
                         Next;
                    end;
               end;
          end;
          if ( iCuantos <> 0 ) then
          begin
               if ( iRecTope > FRecNo ) then
                  iRecTope := FRecNo;
               if ( FRecNo > 0 ) then
               begin
                    if ( StrLleno( sTitulo ) ) then
                       sTitulo := sTitulo + ' ' + Format( K_RESULTADOS, [ iRecInicial, iRectope, FRecNo ] )
                    else
                        sTitulo := sTitulo + Format( K_RESULTADOS_SIN_TITULO, [ iRecInicial, iRectope, FRecNo ] );
               end
          end;
          SetTablaAtributos( NodoTabla, sTitulo, FRecno );
     end;
     { ** Paginaci�n ** }
     GetPaginacion;
end;

procedure TZetaXML.AgregaDataSet( Datos: TZetaXMLNode; oDataSet: TDataSet; sTitulo: String = VACIO; sLigaPagina: string = VACIO; iCuantos: integer = 0; iPagina: integer = 1 );
var
   NodoTabla, NodoRenglon, NodoColumna: TZetaXMLNode;
   i, iRango, iInicio, iFinPag, iFinal, iRecInicial, iRecTope, iPaginas: Integer;

   procedure GetPaginacion;
   var
      x, iInicioPag: integer;
   begin
        if ( iCuantos <> 0 ) and ( FRecno > iCuantos ) then
        begin
             AgregaEspacio( Datos );
             iPaginas := iMin( 1, ( FRecNo mod iCuantos ) ) + ( FRecNo div iCuantos );
             NodoTabla := Datos.NewChild( K_TABLA_TAG, VACIO );
             if ( NodoTabla <> nil ) then
             begin
                  NodoRenglon := NodoTabla.NewChild( K_RENGLON_TAG, VACIO );
                  if ( NodoRenglon <> nil ) then
                  begin

                       { ************ Imagen de P�gina Anterior *********** }
                       if ( iPagina <> 1 ) then
                          SetContenido( NodoRenglon, GetTagLigaImagen( sLigaPagina + '&PAGINA=' + IntToStr( iPagina - 1 ), K_IMG_ANTERIOR, K_ANTERIOR ), taLeftJustify, aClaseFuentePar[ TRUE ] )
                       else
                           SetContenido( NodoRenglon, GetTagImagen( K_IMG_ANTERIOR, K_ANTERIOR ), taLeftJustify, aClaseFuentePar[ TRUE ] );

                       { *********** Ligas de las p�ginas que tuvieron resultados *********** }
                       NodoColumna := SetContenido( NodoRenglon, VACIO, taCenter, aClaseFuentePar[ FALSE ] );
                       iRango := ( iPagina div K_RANGO );

                       { ******* P�ginas Multiplos de 10 ********** }
                       if ( ( iPagina mod K_RANGO ) = 0 )then
                          iRango := iRango - 1;

                       { ******* Liga de las 10 p�ginas anteriores ******* }
                       if ( iRango >= 1 ) then
                       begin
                            iInicio := ( ( iRango - 1 ) * K_RANGO ) + 1;
                            setTagLiga( NodoColumna, sLigaPagina + '&PAGINA=' + IntToStr( iInicio ), '�� Anteriores 10 P�ginas ', 'P�gina ' + IntToStr( iInicio ) );
                            AgregaTexto( Nodocolumna, ' - ' );
                       end;
                       iInicioPag := ( ( iRango ) * K_RANGO ) + 1;
                       iFinPag := iInicioPag + ( K_RANGO - 1 ); //9
                       if ( iFinPag > iPaginas ) then
                          iFinPag := iPaginas;
                       for x := iInicioPag to iFinPag do
                       begin
                            if ( x = iPagina ) then
                            begin
                                 AgregaTexto( NodoColumna, '[ ' );
                                 AgregaTexto( NodoColumna, IntToStr( x ) );
                                 AgregaTexto( NodoColumna, ' ]' );
                            end
                            else
                                setTagLiga( NodoColumna, sLigaPagina + '&PAGINA=' + IntToStr( x ), IntToStr( x ), 'P�gina ' + IntToStr( x ) );
                            if ( x < iFinPag ) then
                               AgregaTexto( NodoColumna, ' - ' );
                       end;

                       { ****** Liga de las 10 p�ginas siguientes ****** }
                       iFinal := ( ( iRango + 1 ) * K_RANGO ) + 1;
                       if ( iFinal <= iPaginas ) then
                       begin
                            AgregaTexto( NodoColumna, ' - ' );
                            setTagLiga( NodoColumna, sLigaPagina + '&PAGINA=' + IntToStr( iFinal ), ' Siguientes 10 P�ginas ��', 'P�gina ' + IntToStr( iFinal ) );
                       end;

                       { ******* Imagen de siguiente en la navegaci�n de las p�ginas ****** }
                       if ( iPagina <> iPaginas ) then
                          SetContenido( NodoRenglon, GetTagLigaImagen( sLigaPagina + '&PAGINA=' + IntToStr( iPagina + 1 ), K_IMG_SIGUIENTE, K_SIGUIENTE ), taRightJustify, aClaseFuentePar[ TRUE ] )
                       else
                           SetContenido( NodoRenglon, GetTagImagen( K_IMG_SIGUIENTE, K_SIGUIENTE ), taRightJustify, aClaseFuentePar[ TRUE ] );
                  end;
             end;
             SetTablaAtributos( NodoTabla, Format( K_PAGINACION, [ iPagina, iPaginas ] ), iPaginas );
        end;
   end;

begin
     FRecno := 0;
     iRecInicial := ( ( iPagina - 1 ) * icuantos ) + 1;
     iRecTope := iRecInicial + iCuantos - 1;
     NodoTabla := Datos.NewChild( K_TABLA_TAG, VACIO );
     if ( NodoTabla <> nil ) then
     begin
          with oDataSet do
          begin
               if ( not IsEmpty ) then
               begin
                    // Headers
                    NodoRenglon := NodoTabla.NewChild( K_ENCABEZADO_TAG, VACIO );
                    if ( NodoRenglon <> nil ) then
                    begin
                         for i := 0 to ( FieldCount - 1 ) do
                         begin
                              if Fields[ i ].Visible then
                                 SetHeaderColumna( NodoRenglon, Fields[ i ].DisplayLabel );
                         end;
                    end;
                    // Renglones
                    First;
                    while not EOF do
                    begin
                         Inc( FRecNo );
                         NodoRenglon := NodoTabla.NewChild( K_RENGLON_TAG, VACIO );
                         if ( NodoRenglon <> nil ) then
                         begin
                              for i := 0 to ( FieldCount - 1 ) do
                              begin
                                   if Fields[ i ].Visible then
                                   begin
                                        NodoColumna := NodoRenglon.NewChild( K_COLUMNA_TAG, VACIO );
                                        SetColumnaInfo( NodoColumna, GetFieldInfo( Fields[i] ), Fields[i].Alignment, aClaseFuentePar[ ( FRecNo Mod 2 ) = 0 ] );
                                   end;
                              end;
                         end;
                         Next;
                    end;
               end;
          end;
          if ( iCuantos <> 0 ) then
          begin
               if ( iRecTope > FRecNo ) then
                  iRecTope := FRecNo;
               if ( sTitulo <> VACIO ) then
                  sTitulo := sTitulo + ' ';
               sTitulo := sTitulo + Format( K_RESULTADOS, [ iRecInicial, iRectope, FRecNo ] );
          end;
          SetTablaAtributos( NodoTabla, sTitulo, FRecno );
     end;
     { ** Paginaci�n ** }
     GetPaginacion;
end;

procedure TZetaXML.AgregaXMLDataSet( oNodo: TZetaXMLNode; sTitulo, sScript: String; oCursor: TZetaCursor; sLigaPagina: string = VACIO; iCuantos: integer = 0; iPagina: integer = 1 );
begin
     try
        oZetaProvider.PreparaQuery( oCursor, sScript );
        with oCursor do
        begin
             Active := True;
             if Assigned( AfterOpen ) then
                AfterOpen( oCursor );
             AgregaCursor( oNodo, oCursor, sTitulo, sLigaPagina, iCuantos, iPagina );
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                BuildErrorXML( Format( 'Error Al Leer Tabla %s: ', [ sTitulo ] ) + Error.Message );
           end;
     end
end;

procedure TZetaXML.SetHeaderColumna( Datos: TZetaXMLNode; const sContenido: String );
var
   NodoColumna: TZetaXMLNode;
begin
     NodoColumna := Datos.NewChild( K_HEADER_TAG, VACIO );
     SetNodoContenido( NodoColumna, sContenido );
end;

procedure TZetaXML.SetTablaAtributos( Datos: TZetaXMLNode; const sTitulo: String; const iRegistros: Integer );
begin
     with Datos do
     begin
          AddAttribute( K_TITULO_ATTR, sTitulo );
          AddAttribute( K_REGISTRO_ATTR, IntToStr( iRegistros ) );
     end;
end;

function TZetaXML.ValidaHRef( const sHref: String; const lChecar: boolean = FALSE ): String;
begin
     Result := sHref;

     Result := StrTransAll( Result, ' ', '%20' );
     Result := StrTransAll( Result, '�', '%E1' );
     Result := StrTransAll( Result, '�', '%E9' );
     Result := StrTransAll( Result, '�', '%ED' );
     Result := StrTransAll( Result, '�', '%F3' );
     Result := StrTransAll( Result, '�', '%FA' );
     Result := StrTransAll( Result, '�', '%F1', FALSE );
     Result := StrTransAll( Result, '�', '%D1', FALSE );
     Result := StrTransAll( Result, chr( 34 ), '%22' );  //"
     Result := StrTransAll( Result, chr( 39 ), '%27' );  //'
     Result := StrTransAll( Result, '(', '%28' );
     Result := StrTransAll( Result, ')', '%29' );
     Result := StrTransAll( Result, ',', '%2C' );
     Result := StrTransAll( Result, '#', '%23' );
     Result := StrTransAll( Result, '!', '%21' );
     Result := StrTransAll( Result, '�', '%A1' );

     if ( lChecar ) then
        Result := StrTransAll( Result, '&', '%26' );
end;

function TZetaXML.ValidaTexto( const sTexto: String ): String;
var
   i, iLongitud: integer;
   sTemp, sText: string;

begin
     iLongitud := length( sTexto );
     sTemp := sTexto;
     sText := '<LINE>';
     for i := 1 to ( iLongitud ) do
     begin
          if ( sTemp[ i ] = Chr( 13 ) ) then
          begin
               if ( sTemp[ i + 1 ] = Chr( 10 ) ) then
               begin
                    sText := sText + '</LINE><LINE>';
               end
               else
               begin
                    sText := sText + stemp[ i ];
               end;
          end
          else
          begin
               sText := sText + stemp[ i ];
          end;
          {
          Result := sTexto;
          Result := StrTransAll( Result, CR_LF, '<ESPACIO/>' );
          }
     end;
     sText := sText + '</LINE>';
     Result := sText;
end;

function TZetaXML.GetTagLiga( const sHref, sInfo: String; const sHint: string = VACIO ): WideString;
const
     K_SINTAXIS_LIGA = '<%0:s href="%1:s" hint="%3:s">%2:s</%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_LIGA, [ K_LIGA_TAG, ValidaHRef( sHref ), sInfo, sHint ] );
end;

function TZetaXML.GetTagLigaImagen( const sHref, sFuente: String; const sHint: string=VACIO ): WideString;
const
     K_SINTAXIS_LIGA_IMG = '<%0:s href="%1:s" fuente="%2:s" hint="%3:s" ></%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_LIGA_IMG, [ K_LIGA_IMG_TAG, ValidaHRef( sHref ), sFuente, sHint ] );
end;

function TZetaXML.GetTagCheck(const sCheck: String): WideString;
const
     K_SINTAXIS_CHECK = '<%s/>';

begin
     if ( sCheck = K_GLOBAL_SI ) then
        Result := K_XML_HEADER + Format( K_SINTAXIS_CHECK, [ K_CHECK_TAG ] )
     else
         Result := K_XML_HEADER + Format( K_SINTAXIS_CHECK, [ K_NOTCHECK_TAG ] );
end;

function TZetaXML.GetTagLbl(const sLabel: String): WideString;
const
     K_SINTAXIS_LABEL = '<%0:s>%1:s</%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_LABEL, [ K_LABEL_TAG, sLabel ] );
end;

function TZetaXML.GetTagImagen( const sImagen: String; const sHint: string = VACIO ): WideString;
const
     K_SINTAXIS_IMAGEN = '<%0:s fuente="%1:s" hint="%2:s"></%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_IMAGEN, [ K_IMAGEN_TAG, sImagen, sHint ] );
end;

function TZetaXML.GetTagEdit(const sEdit, sNombre: String; sTamano: String = '5'; sLongitud: String = '0' ): WideString;
const
     K_SINTAXIS_EDIT = '<%0:s nombre="%2:s" tamano="%3:s" longitud="%4:s">%1:s</%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_EDIT, [ K_EDIT_TAG, sEdit, sNombre, sTamano, sLongitud ] );
end;

function TZetaXML.GetTagEditPswd(const sEdit, sNombre: String; sTamano: String = '5'; sLongitud: String = '0' ): WideString;
const
     K_SINTAXIS_EDIT = '<%0:s nombre="%2:s" tamano="%3:s" longitud="%4:s">%1:s</%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_EDIT, [ K_EDIT_PSWD_TAG, sEdit, sNombre, sTamano, sLongitud ] );
end;

function TZetaXML.GetTagRadio(const sRadio, sNombre, sValor: String; lChecked: boolean; lValido: boolean = TRUE; sAlCambiar: string = VACIO ): WideString;
const
     K_SINTAXIS_EDIT = '<%0:s nombre="%2:s" valor="%3:s" alcambiar="%4:s">%1:s</%0:s>';
     K_SINTAXIS_CHECKED_EDIT = '<%0:s nombre="%2:s" valor="%3:s" alcambiar="%4:s">%1:s</%0:s>';

begin
     Result := K_XML_HEADER;
     if lChecked then
     begin
          if lValido then
             Result := result + Format( K_SINTAXIS_CHECKED_EDIT, [ K_OPTION_CHECKED_ABLE_TAG, sRadio, sNombre, sValor, sAlCambiar ] )
          else
              Result := result + Format( K_SINTAXIS_CHECKED_EDIT, [ K_OPTION_CHECKED_TAG, sRadio, sNombre, sValor, sAlCambiar ] );
     end
     else
     begin
          if lValido then
             Result := result + Format( K_SINTAXIS_EDIT, [ K_OPTION_ABLE_TAG, sRadio, sNombre, sValor, sAlCambiar ] )
          else
              Result := result + Format( K_SINTAXIS_EDIT, [ K_OPTION_TAG, sRadio, sNombre, sValor, sAlCambiar ] );
     end;
end;

function TZetaXML.GetTagTexto( const sTexto, sNombre: String; lValido: boolean = True; sAlCambiar: string = VACIO ): WideString;
const
     K_SINTAXIS_EDIT = '<%0:s nombre="%2:s" alcambiar="%3:s">%1:s</%0:s>';
begin
     if lValido then
        Result := K_XML_HEADER + Format( K_SINTAXIS_EDIT, [ K_TEXTAREA_ABLE_TAG, sTexto, sNombre, sAlCambiar ] )
     else
         Result := K_XML_HEADER + Format( K_SINTAXIS_EDIT, [ K_TEXTAREA_TAG, sTexto, sNombre, sAlCambiar ] );
end;

function TZetaXML.GetTagBoton(const sBoton, sNombre: String; const TipoBtn : eTipoBoton = eBtnSubmit; const sOnClick: String = VACIO ): WideString;
const
    K_SINTAXIS_BOTON = '<%0:s nombre="%2:s" onclick="%3:s">%1:s</%0:s>';
begin
     Result := K_XML_HEADER + Format( K_SINTAXIS_BOTON, [ aTagBoton[ TipoBtn ], sBoton, sNombre, sOnClick ] );
end;

procedure TZetaXML.SetBoton( Datos: TZetaXMLNode; const sBoton, sNombre: String; const TipoBtn : eTipoBoton = eBtnSubmit; const sOnClick: String = VACIO  );
var
   oNodo: TZetaXMLNode;
begin
     oNodo := Datos.NewChild( aTagBoton[ TipoBtn ], sBoton );
     if ( oNodo <> nil ) then
     begin
          with oNodo do
          begin
               AddAttribute( 'nombre', sNombre );
               AddAttribute( 'onclick', sOnClick );
          end;
     end;
end;

procedure TZetaXML.SetTagLiga( Datos: TZetaXMLNode; const sHref, sInfo: String; const sHint: string = VACIO; const sDestino: String = VACIO );
var
   oNodo: TZetaXMLNode;
begin
     oNodo := Datos.NewChild( K_LIGA_TAG, sInfo );
     with oNodo do
     begin
          AddAttribute( 'href', ValidaHRef( sHref ) );
          if strLleno( sHint ) then
             AddAttribute( 'hint', ValidaHRef( sHint ) );
          if strLleno( sDestino ) then
             AddAttribute( 'destino', ValidaHRef( sDestino ) );
     end;
end;

procedure TZetaXML.SetTagLigaImagen( Datos: TZetaXMLNode; const sHref, sFuente: String; const sHint: string = VACIO; const sDestino: String = VACIO );
var
   oNodo: TZetaXMLNode;
begin
     oNodo := Datos.NewChild( K_LIGA_IMG_TAG, VACIO );
     with oNodo do
     begin
          AddAttribute( 'href', ValidaHRef( sHref ) );
          AddAttribute( 'fuente', sFuente );
          if strLleno( sHint ) then
             AddAttribute( 'hint', ValidaHRef( sHint ) );
          if strLleno( sDestino ) then
             AddAttribute( 'destino', ValidaHRef( sDestino ) );
     end;
end;

procedure TZetaXML.SetComboOpcion( Datos:TZetaXMLNode; const sValue, sDescrip: String; const lSelected: Boolean = FALSE );
const
     aTipoOpcion: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_COMBO_OPCION_TAG, K_COMBO_OPCION_SELECTED_TAG );
var
   NodoOpcion: TZetaXMLNode;
begin
     NodoOpcion := Datos.NewChild( aTipoOpcion[ lSelected ], VACIO );
     if ( NodoOpcion <> nil ) then
        with NodoOpcion do
        begin
             AddAttribute( 'value', sValue );
             AddAttribute( 'descripcion', sDescrip );
        end;
end;

function TZetaXML.SetNewCombo( Datos:TZetaXMLNode; const sNombre, sAlCambiar: String; const lMenu: Boolean ): TZetaXMLNode;
const
     aTipoCombo: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( K_COMBO_TAG, K_MENU_TAG );
begin
     Result := Datos.NewChild( aTipoCombo[ lMenu ], VACIO );
     if ( Result <> nil ) then
     begin
          with Result do
          begin
               AddAttribute( K_NOMBRE_ATTR, sNombre );
               if strLleno( sAlCambiar ) then
                  AddAttribute( 'alcambiar', sAlCambiar );
          end;
     end;
end;

function TZetaXML.AgregaFooter( Datos: TZetaXMLNode; lLinea: boolean = TRUE ): TZetaXMLNode;
begin
     if ( lLinea ) then
        Datos.NewChild( K_LINEA_TAG, VACIO );
     Result := Datos.NewChild( K_FOOTER_TAG, VACIO );
end;

function TZetaXML.SetComboDataSet( Datos:TZetaXMLNode; oCursor: TZetaCursor; const sNombre, sAlCambiar: String; const lMenu: Boolean; var sDefault: String): TZetaXMLNode;
{
const
     K_SINTAXIS_COMBO = '<%0:s nombre="%1:s" alcambiar="%2:s">%3:s</%0:s>';
     K_SINTAXIS_OPCION = '<%0:s value="%1:s" descripcion="%2:s"/>';
}
var
   sPrimerCod, sCodigo: String;
   lExisteDefault: Boolean;
begin
     Result := SetNewCombo( Datos, sNombre, sAlCambiar, lMenu );
     if ( Result <> nil ) then
     begin
          with oCursor do
          begin
               if not IsEmpty then
               begin
                    sPrimerCod := Fields[0].AsString;
                    lExisteDefault := FALSE;
                    while not EOF do
                    begin
                         sCodigo := Fields[0].AsString;
                         lExisteDefault := lExisteDefault or ( strLleno( sDefault ) and ( sCodigo = sDefault ) );
                         SetComboOpcion( Result, sCodigo, Fields[1].AsString, ( sCodigo = sDefault ) );
                         Next;
                    end;
                    if not lExisteDefault then                       // Si no se tiene Default se regresa el primer registro
                       sDefault := sPrimerCod;
               end;
{
               sOpciones := VACIO;
               sPrimerCod := Fields[0].AsString;
               lExisteDefault := FALSE;
               while not EOF do
               begin
                    sCodigo := Fields[0].AsString;
                    lExisteDefault := lExisteDefault or ( strLleno( sDefault ) and ( sCodigo = sDefault ) );
                    sOpciones := sOpciones + Format( K_SINTAXIS_OPCION, [ aTipoOpcion[ sCodigo = sDefault ],
                                                                          sCodigo, Fields[1].AsString ] );
                    Next;
               end;
               if not lExisteDefault then                       // Si no se tiene Default se regresa el primer registro
                  sDefault := sPrimerCod;
               Result := K_XML_HEADER + Format( K_SINTAXIS_COMBO, [ aTipoCombo[ lMenu ], sNombre, sAlCambiar, sOpciones ] );
}
          end;
     end;
end;

function TZetaXML.SetComboOtraOpcionDataSet( Datos:TZetaXMLNode; oCursor: TZetaCursor; const sNombre, sAlCambiar: String; const lMenu: Boolean; var sDefault: String): TZetaXMLNode;
var
   sPrimerCod, sCodigo: String;
   lExisteDefault: Boolean;
begin
     Result := SetNewCombo( Datos, sNombre, sAlCambiar, lMenu );
     if ( Result <> nil ) then
     begin
          sPrimerCod := '0';     
          SetComboOpcion( Result, '0', 'Todos', True );
          with oCursor do
          begin
               if not IsEmpty then
               begin
                    //sPrimerCod := Fields[0].AsString;
                    lExisteDefault := FALSE;
                    while not EOF do
                    begin
                         sCodigo := Fields[0].AsString;
                         lExisteDefault := lExisteDefault or ( strLleno( sDefault ) and ( sCodigo = sDefault ) );
                         SetComboOpcion( Result, sCodigo, Fields[1].AsString, ( sCodigo = sDefault ) );
                         Next;
                    end;
                    if not lExisteDefault then                       // Si no se tiene Default se regresa el primer registro
                       sDefault := sPrimerCod;
               end;
          end;
     end;
end;

function TZetaXML.SetComboListaFija( Datos:TZetaXMLNode; ListaFija: ListasFijas; const sNombre, sAlCambiar: String; const lMenu: Boolean; var sDefault: String): TZetaXMLNode;
var
   sPrimerCod, sCodigo: String;
   lExisteDefault: Boolean;
   oLista: TStrings;
   i : Integer;
begin
     Result := SetNewCombo( Datos, sNombre, sAlCambiar, lMenu );
     if ( Result <> nil ) then
     begin
          oLista := TStringList.Create;
          try
             ZetaCommonLists.LlenaLista( ListaFija, oLista );
             with oLista do
             begin
                  if ( Count > 0 ) then
                  begin
                       sPrimerCod := IntToStr( 0 );
                       lExisteDefault := FALSE;
                       for i := 0 to Count - 1 do
                       begin
                            sCodigo := IntToStr( i );
                            lExisteDefault := lExisteDefault or ( strLleno( sDefault ) and ( sCodigo = sDefault ) );
                            SetComboOpcion( Result, sCodigo, ObtieneElemento( ListaFija, i ), ( sCodigo = sDefault ) );
                       end;
                       if not lExisteDefault then                       // Si no se tiene Default se regresa el primer registro
                          sDefault := sPrimerCod;
                  end;
             end;
          finally
             FreeAndNil( oLista );
          end;
     end;
end;

procedure TZetaXML.SetNodoContenido( Datos: TZetaXMLNode; const sContenido: WideString );
const
     K_ANY_TAG = 'ANY';
var
   Nodo, Nodo2: TZetaXMLNode;
begin
     if ( Pos( K_XML_HEADER, sContenido ) > 0 ) then     // Verifica si es Texto XML
     begin
          Nodo := NewXML;
          Nodo.LoadXML( sContenido );
          Nodo2 := Datos.NewChild( K_ANY_TAG, VACIO );
          Nodo2.SwapTree( Nodo );
     end
     else
          Datos.AppendToContent( sContenido );
end;

function TZetaXML.AgregaDescripcionXML(Datos: TZetaXMLNode; const sModulo, sPantalla: String): TZetaXMLNode;
begin
     Result := Datos.NewChild( K_DESCRIPCIONES_TAG, VACIO );
     if ( Result <> nil ) then
     begin
          Result.NewChild( K_MODULO_TAG, sModulo );
          Result.NewChild( K_PANTALLA_TAG, sPantalla );
     end;
end;

function TZetaXML.SetTablaMaster( Datos: TZetaXMLNode; const justifica: TAlignment = taCenter ): TZetaXMLNode;
var
   Nodo: TZetaXMLNode;
begin
     //Nodo:= Datos.GetRoot.FindOrAddNewChild( K_MASTER_TAG );
     Nodo := SetMaster( Datos.GetRoot );
     if ( Nodo <> nil ) then
     begin
          Result := Nodo.NewChild( K_TABLAMASTER_TAG, VACIO );
          Result.AddAttribute( K_ALINEACION, aJustificacion[ justifica ] );
     end
     else
        Result := nil;
end;

function TZetaXML.SetMaster( Datos: TZetaXMLNode ): TZetaXMLNode;
begin
     Result := Datos.FindOrAddNewChild( K_MASTER_TAG );
end;

function TZetaXML.SetForma( Datos: TZetaXMLNode; const sFormaName, sAccion: String; const lPost: Boolean = TRUE; const sDeshacer: String = VACIO; const sValidar: String = VACIO; const sCargar: String = VACIO; const sDestino: String = VACIO ): TZetaXMLNode;
const
     aMetodo: array[ FALSE..TRUE ] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'GET', 'POST' );
begin
     Result := Datos.NewChild( K_FORMA_TAG, VACIO );
     if ( Result <> nil ) then
        with Result do
        begin
             AddAttribute( K_NOMBRE_ATTR, sFormaName );
             AddAttribute( 'metodo', aMetodo[ lPost ] );
             AddAttribute( 'accion', sAccion );
             if strLleno( sDeshacer ) then
                AddAttribute( 'deshacer', sDeshacer );
             if strLleno( sValidar ) then
                AddAttribute( 'validar', sValidar );
             if strLleno( sCargar ) then
                AddAttribute( 'cargar', sCargar );
             if strLleno( sDestino ) then
                AddAttribute( 'destino', sDestino );
        end;
end;

procedure TZetaXML.AgregaHiddenEdit( Datos: TZetaXMLNode; const sHiddenName, sValue: String );
var
   Nodo: TZetaXMLNode;
begin
     Nodo := Datos.NewChild( K_HIDDEN_TAG, sValue );
     if ( Nodo <> nil ) then
        Nodo.AddAttribute( K_NOMBRE_ATTR, sHiddenName );
end;

procedure TZetaXML.AgregaMeta( Datos: TZetaXMLNode; const sEquiv, sContent: String );
var
   Nodo: TZetaXMLNode;
begin
     Nodo := Datos.NewChild( K_META_TAG, sContent );
     if ( Nodo <> nil ) then
        Nodo.AddAttribute( 'equiv', sEquiv );
end;

procedure TZetaXML.AgregaOnLoad( Datos: TZetaXMLNode; const sContent: String );
var
   Nodo: TZetaXMLNode;
begin
     Nodo := Datos.NewChild( K_ALCARGAR, sContent );
end;

function TZetaXML.SetRenglon( Datos: TZetaXMLNode ): TZetaXMLNode;
begin
     Result := Datos.NewChild( K_RENGLON_TAG, VACIO );
end;

function TZetaXML.SetSeccion( Datos: TZetaXMLNode ): TZetaXMLNode;
begin
     Result := Datos.NewChild( K_SECCION_TAG, VACIO );
end;

procedure TZetaXML.SetColumnaInfo( Datos: TZetaXMLNode; const sContenido: WideString; const justifica: TAlignment = taLeftJustify; const sClaseFuente: String = VACIO );
const
     K_LINE = '<LINE>';
     K_END_LINE = '</LINE>';
var
   iLongitud: integer;
   sTemp, sText: string;

   procedure EnviaLineaXLinea;
   var
      i: integer;
   begin
        sText := K_LINE;
        for i := 1 to ( iLongitud ) do
        begin
             if ( sTemp[ i ] = Chr( 13 ) ) then
             begin
                  if ( sTemp[ i + 1 ] = Chr( 10 ) ) then
                  begin
                       sText := K_XML_HEADER + sText + K_END_LINE;
                       SetNodoContenido( Datos, sText );
                       sText := K_LINE;
                  end
                  else
                  begin
                       sText := sText + stemp[ i ];
                  end;
             end
             else
             begin
                  sText := sText + stemp[ i ];
             end;
        end;
   end;

begin
     if ( Pos( CR_LF, sContenido ) > 0 ) then
     begin
          iLongitud := length( sContenido );
          sTemp := sContenido;
          EnviaLineaXLinea;
     end
     else
         SetNodoContenido( Datos, sContenido );
     if ( justifica <> taLeftJustify ) then
        Datos.AddAttribute( K_ALINEACION, aJustificacion[ justifica ] );
     if strLleno( sClaseFuente ) then
        Datos.AddAttribute( K_CLASE_FUENTE, sClaseFuente );
end;


function TZetaXML.SetContenido( Datos: TZetaXMLNode; const sContenido: WideString; const justifica: TAlignment = taLeftJustify; const sClaseFuente: String = VACIO ): TZetaXMLNode;
begin
     Result := Datos.NewChild( K_CONTENIDO_TAG, VACIO );
     if ( Result <> nil ) then
        SetColumnaInfo( Result, sContenido, justifica, sClaseFuente );
end;

{
function TZetaXML.SetContenido( Datos: TZetaXMLNode; const sContenido: WideString; const justifica: TAlignment = taLeftJustify; const sClaseFuente: String = VACIO ): TZetaXMLNode;
const
     K_LINE = '<LINE>';
     K_END_LINE = '</LINE>';

var
   iLongitud: integer;
   sTemp, sText: string;

   procedure EnviaLineaXLinea;
   var
      i: integer;
   begin
        sText := K_LINE;
        for i := 1 to ( iLongitud ) do
        begin
             if ( sTemp[ i ] = Chr( 13 ) ) then
             begin
                  if ( sTemp[ i + 1 ] = Chr( 10 ) ) then
                  begin
                       sText := K_XML_HEADER + sText + K_END_LINE;
                       if ( Result <> nil ) then
                          SetColumnaInfo( Result, sText, justifica, sClaseFuente );
                       sText := K_LINE;
                  end
                  else
                  begin
                       sText := sText + stemp[ i ];
                  end;
             end
             else
             begin
                  sText := sText + stemp[ i ];
             end;
        end;
   end;

begin
     Result := Datos.NewChild( K_CONTENIDO_TAG, VACIO );
     if ( Pos( CR_LF, sContenido ) > 0 ) then
     begin
          iLongitud := length( sContenido );
          sTemp := sContenido;
          EnviaLineaXLinea;
     end
     else
         if ( Result <> nil ) then
            SetColumnaInfo( Result, sContenido, justifica, sClaseFuente );
end;
}

{ M�todos para .NET }

procedure TZetaXML.AgregaXMLQueryNET( oNodo: TZetaXMLNode; const sTitulo, sScript: String;
          const lValidarEmpty: Boolean = FALSE );
var
   oCursor: TZetaCursor;
begin
     oCursor := oZetaProvider.CreateQuery( sScript );
     try
        AgregaXMLCursorNET( oNodo, oCursor, sTitulo, lValidarEmpty );
     finally
        FreeAndNil( oCursor );
     end;
end;

procedure TZetaXML.AgregaXMLCursorNET( oNodo: TZetaXMLNode; oCursor: TZetaCursor;
          const sTitulo: String; const lValidarEmpty: Boolean = FALSE );
begin
     try
        with oCursor do
        begin
             Active := True;
             if Assigned( AfterOpen ) then
                AfterOpen( oCursor );
             if ( not IsEmpty ) then
                AgregaXMLCursorDataNET( oNodo, oCursor, sTitulo )
             else if lValidarEmpty then
                BuildErrorXML( '* No Hay Datos *' );
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                BuildErrorXML( Format( 'Error Al Leer Tabla %s: ', [ sTitulo ] ) + Error.Message );
           end;
     end
end;

procedure TZetaXML.AgregaXMLCursorDataNET( oNodo: TZetaXMLNode; oCursor: TZetaCursor;
          const sTitulo: String );
var
   NodoRenglon: TZetaXMLNode;
   i: Integer;
begin
     { Funciona bien con Provider de ODBC, con IBObjects dar� problemas la llamada a
       GetFieldInfo() ya que recibe un TField y en IBObject es un TIB_Row }
     with oCursor do
     begin
          while not EOF do
          begin
               NodoRenglon := oNodo.NewChild( sTitulo, VACIO );
               if ( NodoRenglon <> nil ) then
               begin
                    for i := 0 to ( FieldCount - 1 ) do
                    begin
                         if ( Fields[i].Visible ) then
                            NodoRenglon.NewChild( Fields[i].FieldName, GetFieldInfoNET( Fields[i] ) );
                    end;
               end;
               Next;
          end;
     end;
end;

procedure TZetaXML.AgregaXMLDataSetDataNET( oNodo: TZetaXMLNode; oDataSet: TDataSet;
          const sTitulo: String );
var
   NodoRenglon: TZetaXMLNode;
   i: Integer;
begin
     with oDataSet do
     begin
          First;           // Siempre posicionar al principio, el apuntador puede estar en cualquier lado
          while not EOF do
          begin
               NodoRenglon := oNodo.NewChild( sTitulo, VACIO );
               if ( NodoRenglon <> nil ) then
               begin
                    for i := 0 to ( FieldCount - 1 ) do
                    begin
                         if ( Fields[i].Visible ) then
                            NodoRenglon.NewChild( Fields[i].FieldName, GetFieldInfoNET( Fields[i] ) );
                    end;
               end;
               Next;
          end;
     end;
end;

function TZetaXML.GetFieldInfoNET(oCampo: TField): String;
var
   sTexto: string;
begin
     with oCampo do
     begin
          sTexto := DisplayText;
          if ( not ( DataType in [ ftBlob, ftMemo ] ) ) and ( sTexto <> AsString ) then           // Si se tiene un formateo por OnGetText, se deja este
             Result := sTexto
          else
          begin
               case DataType of
                    ftSmallint, ftInteger, ftWord: Result := IntToStr( AsInteger );
                    ftString: Result := AsString;
                    ftBoolean: Result := ZetaCommonTools.zBoolToStr( AsBoolean );
                    ftFloat: Result := FormatFloat( '#.##', AsFloat );
                    ftCurrency: Result := FormatFloat( '#.00', AsFloat );
                    ftDate, ftDateTime: Result := ZetaCommonTools.FechaAsStr( AsDateTime );
                    ftTime: Result := FormatDateTime( 'hhnn', AsDateTime );
                    //ftMemo: Result := ZetaCommonTools.BorraCReturn( AsString );
               else
                   Result := AsString; //ZetaCommonTools.BorraCReturn( AsString );
               end;
          end;
     end;
end;

end.


