library Nomina;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

{%ToDo 'Nomina.todo'}
uses
  MidasLib,
  ComServ,
  Nomina_TLB in '..\MTS\Nomina_TLB.pas',
  DServerNomina in '..\MTS\DServerNomina.pas' {dmServerNomina: TMtsDataModule} {dmServerNomina: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Nomina.TLB}

{$R *.RES}

begin
end.
