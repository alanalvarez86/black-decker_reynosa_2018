@echo off
echo 32 bits de %~dp0Win32\
xcopy "%~dp0DLL.Win32\*.dll" "C:\Proyectos\Version\Proyectos Integrados\Version 2014\64bits\Entregables\Dlls.Win32\" /I /Y
xcopy "%~dp0..\WorkFlow\MTSMSSQL\DLL.Win32\*.dll" "C:\Proyectos\Version\Proyectos Integrados\Version 2014\64bits\Entregables\Dlls.Win32\" /I /Y
echo.
echo 64 bits de %~dp0Win64\
xcopy "%~dp0DLL.Win64\*.dll" "C:\Proyectos\Version\Proyectos Integrados\Version 2014\64bits\Entregables\Dlls.Win64\" /I /Y
xcopy "%~dp0..\WorkFlow\MTSMSSQL\DLL.Win64\*.dll" "C:\Proyectos\Version\Proyectos Integrados\Version 2014\64bits\Entregables\Dlls.Win64\" /I /Y
echo.
pause