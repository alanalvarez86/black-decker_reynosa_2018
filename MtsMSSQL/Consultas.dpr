library Consultas;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  Consultas_TLB in '..\MTS\Consultas_TLB.pas',
  DServerConsultas in '..\MTS\DServerConsultas.pas' {dmServerConsultas: TMtsDataModule} {dmServerConsultas: CoClass},
  DConteoPersonal in '..\MTS\DConteoPersonal.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Consultas.TLB}

{$R *.RES}

begin
end.
