library Visitantes;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  DServerVisitantes in '..\MTS\DServerVisitantes.pas' {dmServerVisitantes: TMtsDataModule} {dmSeverVisitantes: CoClass},
  Visitantes_TLB in '..\MTS\Visitantes_TLB.pas',
  ZCreator in '..\VisitantesMGR\ZCreator.pas',
  EditorDResumen in '..\Medicos\EditorDResumen.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Visitantes.TLB}

{$R *.RES}

begin
end.
