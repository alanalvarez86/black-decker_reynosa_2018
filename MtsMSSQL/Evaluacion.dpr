library Evaluacion;
{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$ENDIF}

uses
  MidasLib,
  ComServ,       
  Evaluacion_TLB in '..\MTS\Evaluacion_TLB.pas',
  DServerEvaluacion in '..\MTS\DServerEvaluacion.pas' {dmServerEvaluacion: TMtsDataModule} {dmServerEvaluacion: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Evaluacion.TLB}

{$R *.RES}

begin
end.
