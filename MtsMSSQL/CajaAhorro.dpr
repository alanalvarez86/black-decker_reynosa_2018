library CajaAhorro;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  CajaAhorro_TLB in 'CajaAhorro_TLB.pas',
  dServerCajaAhorro in 'dServerCajaAhorro.pas' {dmServerCajaAhorro: TMtsDataModule} {dmServerCajaAhorro: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
