library ServicioMedico;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,  
  ComServ,
  DServerMedico in '..\MTS\DServerMedico.pas' {dmServerMedico: TMtsDataModule},
  ServerMedico_TLB in '..\MTS\ServerMedico_TLB.pas';

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\ServicioMedico.TLB}

{$R *.RES}

begin
end.
