{* M�todos usados por MotorPatch y que los ultilizar�n otras unidades, sin tener que acceder al c�digo del Motor.
  @author Ricardo Carrillo 21/Mar/2014 }
unit MotorPatchUtils;

interface

uses
  DB, DBClient, ADODB, Classes, ZetaCommonClasses, ZetaCommonLists, ZetaServerDataSet, DZetaServerProvider;

const
  K_ESTRUCTURA_MASK     = 'SQL Server %d.sql';
  K_ESTRUCTURA_WILDCARD = 'SQL Server *.sql';
  K_ESPECIALES_WILDCARD = 'Especial*.sql';
  K_REPORTES_WILDCARD   = '*.RP1';
  K_ARCHIVOS_DEFINICION_EMPLEADOS = '\Empleados\NuevaBD';
  K_ARCHIVOS_DEFINICION_SELECCION = '\Seleccion\NuevaBD';
  K_ARCHIVOS_DEFINICION_VISITANTES = '\Visitantes\NuevaBD';
  K_INICIALES = '\Iniciales\';
  K_SUGERIDOS = '\Sugeridos\';
  K_DICCIONARIO = '\Diccionario\';
  EXT_XML = '.xml';

  K_GATITO_COMPARTE = '#COMPARTE';
  K_GATITO_DIGITO   = '#DIGITO';

  K_CONSULTA_BD    = 'select DB_TIPO, DB_DATOS, DB_USRNAME, DB_PASSWRD, DB_ESPC from DB_INFO where DB_DATOS = %s';
  K_SELECT_VERSION = 'select %s from %s where %s = %d';
  K_UPDATE_VERSION = 'update %s set %s = %s where %s = %d';

  MSG_CAMBIO_VERSION = '[%s] Cambio de versi�n %d a %d';
  MSG_CAMBIO_VERSION_D = '[%s] Cambio de versi�n %d a %d '+CR_LF+
                       'Importar Diccionario: %s '+CR_LF+
                       'Importar Reportes:    %s '+CR_LF+
                       'Aplicar especiales:   %s ';

  K_CONSULTA_EMPRESAS    = 'select DISTINCT CM_CODIGO from COMPANY where CM_DATOS = %s';

type
  TDetallesBD = class
  private
    fEsComparte       : Boolean;
    fDBTipo           : eTipoCompany;
    fDBVersion        : Integer; // * Versi�n actual de la Base de Datos
    fGlobalTabla      : string;
    fGlobalCampoTexto : string;
    fGlobalCampoNumero: string;
    fGlobalVersion    : Integer;
    fZProvider        : TdmZetaServerProvider;
    fPathPatch        : string;
    fPathEspeciales   : string;
    fPathDiccionario  : string;
    fPathReportes     : string;
    fComparteNom      : string;
    fPathConfig       : string;
    fDiccionarioMask  : string;
    fDBMaxVersion     : Integer;
    fDBVersiones      : TStringList;
    fAlias            : string;
    fDBEspecial       : Boolean;
    fFreeProvider     : Boolean;
    fEmpresas         : String;
    procedure SetDBVersion(const Value: Integer);
    procedure SetZetaProvider(const Value: TdmZetaServerProvider);
  public
    constructor Create(const AZetaProvider: TdmZetaServerProvider; Alias: string);
    destructor Destroy; override;
    property EsComparte       : Boolean      read fEsComparte;
    property ComparteNom      : string       read fComparteNom;
    property DiccionarioMask  : string       read fDiccionarioMask;
    property DBTipo           : eTipoCompany read fDBTipo;
    property DBVersion        : Integer      read fDBVersion write SetDBVersion;
    property DBEspecial       : Boolean      read fDBEspecial;
    property DBMaxVersion     : Integer      read fDBMaxVersion;
    property DBVersiones      : TStringList  read fDBVersiones;
    property GlobalVersion    : Integer      read fGlobalVersion;
    property PathPatch        : string       read fPathPatch;
    property PathDiccionario  : string       read fPathDiccionario;
    property PathReportes     : string       read fPathReportes;
    property PathEspeciales   : string       read fPathEspeciales;
    property Empresas         : String read fEmpresas;
    property ZetaProvider     : TdmZetaServerProvider read fZProvider write SetZetaProvider;
  end;

  function GetVersionDePatch(ArchivoSQL: string): Integer;
  function GrabaDataSet( cdsOrigen: TClientDataSet; const sTabla, sCamposLlave: String; oProvider: TdmZetaServerProvider;
                         var iAdvertencias: Integer; var iErrores: Integer; const lAplicaUpdate: Boolean = FALSE;
                         const lGrabarBitacora: Boolean = FALSE; const sCamposExcluidos: String = VACIO ): Boolean;
  function EsAdvertencia(Mensaje: string): Boolean;
  function EsDuplicado(Mensaje: string): Boolean;

implementation

uses
  SysUtils, Windows, Variants, ZetaRegistryServer, ZetaWinAPITools, ZGlobalTress, ZetaServerTools, ZetaCommonTools;

{* Obtener el n�mero de versi�n seg�n el nombre del archivo .SQL en disco en formato "SQL Server ###.sql"
   @autor Ricardo Carrillo Morales 22-Mar-2014}
function GetVersionDePatch(ArchivoSQL: string): Integer;
var
  Dummy: string;
begin
  Dummy := ChangeFileExt(ExtractFileName(ArchivoSQL), ''); // Extraer nombre de archivo y Borrar ".sql"
  Delete(Dummy, 1, 11);                                    // Borrar "SQL Server ", y queda s�lo el n�mero de versi�n
  Result := StrToIntDef(Dummy, 0);
end;

function GrabaDataSet( cdsOrigen: TClientDataSet; const sTabla, sCamposLlave: String; oProvider: TdmZetaServerProvider;
                       var iAdvertencias: Integer; var iErrores: Integer; const lAplicaUpdate: Boolean = FALSE;
                       const lGrabarBitacora: Boolean = FALSE; const sCamposExcluidos: String = VACIO ): Boolean;
var
  oCampos, oCamposLlave: TStringList;
  sCamposExcluye: String;
  FQryDestino: TZetaCursor;
  i : Integer;
  lUpdateTabla: Boolean;
  cdsUpdate: TClientDataset;

  function EsCampoValido( oCampo: TField ): Boolean;
  begin
       Result := NoEsCampoLLavePortal( oCampo ) and ( oCampo.DataType <> ftAutoInc ) and
                 ( strVacio( sCamposExcluidos ) or ( Pos( oCampo.FieldName, sCamposExcluidos ) = 0 ) );
  end;

  function CampoConstante(const Valor: Variant; const Tipo: TFieldType): string;
  begin
       Result := VACIO;
       case Tipo of
            ftSmallint, ftInteger, ftWord, ftAutoInc:  Result := IntToStr(Valor);
            ftString: Result := '''' + Valor + '''';
            ftFloat, ftCurrency, ftBCD, ftFMTBcd: Result := FloatToStr(Valor);
            ftDate, ftTime, ftDateTime: Result := '''' + FormatDateTime('mm/dd/yyyy', Valor) + '''';
       end;
  end;

  function GetFiltroRegistro: String;
  var
     j: Integer;
     sCampo, sValor: String;
     oField: TField;
  begin
       Result := VACIO;
       with oCamposLlave do
       begin
            Clear;
            CommaText := sCamposLlave;
            for j := 0 to Count - 1 do
            begin
                 sCampo := Strings[j];
                 oField := cdsOrigen.FindField( sCampo );
                 if Assigned(oField) then
                 begin
                      sValor := CampoConstante(oField.Value, oField.DataType);
                      sCampo := sCampo + ' = ' + sValor;
                      Result := ZetaCommonTools.ConcatFiltros( Result, Parentesis( sCampo ) );
                 end;
            end;
       end;
  end;

  procedure TraspasaInfoOrigen;
  var
     j: Integer;
     sCampo: String;
  begin
       with cdsUpdate do
       begin
            if not ( State in [ dsEdit, dsInsert ] ) then
               Edit;
            for j := 0 to ( FieldCount - 1 ) do
            begin
                 sCampo := Fields[ j ].FieldName;
                 if ( Pos( sCampo, sCamposLlave ) = 0 ) then   // No se modifican los campos llave
                    cdsUpdate.FieldByName( sCampo ).Assign( cdsOrigen.Fields[ j ] );
            end;
            if ( State in [ dsEdit, dsInsert ] ) then
               Post;
       end;
  end;

begin
     oCampos := TStringList.Create;
     oCamposLlave := TStringList.Create;
     sCamposExcluye := ConcatString( sCamposExcluidos, K_CAMPO_LLAVE_PORTAL + ',' + K_CAMPO_SINC_CREATED + ',' + K_CAMPO_SINC_UPDATED, ',' );
     cdsUpdate := TClientDataSet.Create( oProvider );
     try
        with cdsOrigen do
        begin
             Result := ( not IsEmpty );
             if Result then
             begin
                  if lAplicaUpdate then
                  begin
                       for i := 0 to Fields.Count - 1 do
                       begin
                            if EsCampoValido( Fields[i] ) then
                               oCampos.Add( Fields[i].FieldName );
                       end;
                       with oProvider.TablaInfo do
                       begin
                            SetInfo( sTabla, oCampos.CommaText, sCamposLlave );
                            Filtro := VACIO;
                       end;
                  end;
                  with oProvider do
                  begin
                       FQryDestino := CreateQuery( GetInsertScript( sTabla, sCamposExcluye, cdsOrigen ) );
                  end;
                  First;
                  while ( not EOF ) do
                  begin
                       with oProvider do
                       begin
                            EmpiezaTransaccion;
                            try
                               AsignaParamsDataSet( cdsOrigen, FQryDestino, sCamposExcluye );
                               Ejecuta( FQryDestino );
                               TerminaTransaccion( True );
                               lUpdateTabla := FALSE;
                            except
                               on Error: Exception do
                               begin
                                    RollBackTransaccion;
                                    if (lGrabarBitacora) and Assigned( Log ) then
                                    begin
                                        if Pos ('PRIMARY KEY', Error.Message) > 0 then
                                           Log.Advertencia(0, 'Registro repetido', Error.Message)
                                        else
                                           Log.Advertencia(0, 'Error al grabar registro', Error.Message);
                                    end;
                                    if EsDuplicado( Error.Message ) then
                                       lUpdateTabla := lAplicaUpdate
                                    else if EsAdvertencia( Error.Message ) then
                                       Inc( iAdvertencias )
                                    else
                                        Inc( iErrores );
                               end;
                            end;
                            if lUpdateTabla then
                            begin
                                 TablaInfo.Filtro := GetFiltroRegistro;
                                 cdsUpdate.Data := GetTabla( EmpresaActiva );
                                 TraspasaInfoOrigen;
                                 if ( cdsUpdate.ChangeCount > 0 ) then
                                 begin
                                      EmpiezaTransaccion;
                                      try
                                         ActualizaTabla( cdsUpdate, ukModify );
                                         TerminaTransaccion( True );
                                      except
                                            on Error: Exception do
                                            begin
                                                 RollBackTransaccion;
                                                 if (lGrabarBitacora) and Assigned( Log ) then
                                                 begin
                                                      Log.Advertencia(0, 'Error al actualizar registro', Error.Message);
                                                 end;
                                                 if EsAdvertencia( Error.Message ) then
                                                    Inc( iAdvertencias )
                                                 else
                                                     Inc( iErrores );
                                            end;
                                      end;
                                 end;
                            end;
                            Next;
                       end;
                  end;
             end;
        end;
     finally
            FreeAndNil( cdsUpdate );
            FreeAndNil( oCamposLlave );
            FreeAndNil( oCampos );
     end;
end;

function EsAdvertencia(Mensaje: string): Boolean;
begin
  Mensaje := UpperCase(Mensaje);
  Result := (Pos('ALREADY EXISTS', Mensaje) <> 0) or (Pos('MUST BE UNIQUE', Mensaje) <> 0) or
            (Pos('THERE IS ALREADY', Mensaje) <> 0) or (Pos('ALREADY HAS', Mensaje) <> 0) or
            (Pos('PRIMARY KEY', Mensaje) <> 0);
end;

function EsDuplicado(Mensaje: string): Boolean;
begin
  Mensaje := UpperCase(Mensaje);
  Result := (Pos('PRIMARY KEY', Mensaje) <> 0);
end;

{ TDetallesBD }

constructor TDetallesBD.Create(const AZetaProvider: TdmZetaServerProvider; Alias: string);
begin
  inherited Create;
  fDBVersiones := TStringList.Create;
  fAlias := Alias;
  ZetaProvider := AZetaProvider;
end;

destructor TDetallesBD.Destroy;
begin
  FreeAndNil(fDBVersiones);
  inherited;
end;

procedure TDetallesBD.SetDBVersion(const Value: Integer);
var
  oQuery: TZADOQuery;
begin
  oQuery := ZetaProvider.CreateQuery(Format(K_UPDATE_VERSION, [fGlobalTabla, fGlobalCampoTexto, IntToStr(Value), fGlobalCampoNumero, fGlobalVersion])) as TZADOQuery;
  try
    ZetaProvider.Ejecuta(oQuery);
    fDBVersion := Value;
  finally
    FreeAndNil(oQuery);
  end;
end;

procedure TDetallesBD.SetZetaProvider(const Value: TdmZetaServerProvider);
var
  I, iVersion: Integer;
  ZReg       : TZetaRegistryServer;
  oQuery     : TZADOQuery;
  oQueryEmpresas     : TZADOQuery;
  oArchivos  : TStringList;
  sServer    : string;
  sDatabase  : string;
  Empresa    : Variant;
begin
  fFreeProvider := not Assigned(Value);
  if not fFreeProvider then
    fZProvider := Value
  else begin
    fZProvider := TdmZetaServerProvider.Create(nil);
    fZProvider.EmpresaActiva := fZProvider.Comparte;
  end;
  oQuery := nil;

  fComparteNom     := '';
  fPathConfig      := '';
  fDiccionarioMask := 'Diccion.xml';
  fDBVersion       := 0;
  fDBEspecial      := False;
  fDBMaxVersion    := 0;
  fDBVersiones.Clear;

  // Predeterminado para un COMPARTE
  fGlobalTabla       := 'CLAVES';
  fGlobalCampoNumero := 'CL_NUMERO';
  fGlobalCampoTexto  := 'CL_TEXTO';
  fGlobalVersion     := 100;

  fPathPatch       := '';
  fPathDiccionario := '';
  fPathReportes    := '';
  fPathEspeciales  := '';
  fEmpresas        := '';

  try
    ZReg          := TZetaRegistryServer.Create(False);
    fComparteNom  := ZReg.DataBaseADO;
    fPathConfig   := ZReg.PathConfig;
  except
    on e: Exception do
      ;
  end;
  FreeAndNil(ZReg);

  if fPathConfig = '' then
    Exit;

  fPathConfig := IncludeTrailingPathDelimiter(fPathConfig);
  Empresa := fZProvider.EmpresaActiva;
  ZetaServerTools.GetServerDatabase(fAlias, sServer, sDatabase);
  fEsComparte := (sDatabase = fComparteNom);
  if fEsComparte then begin
    fZProvider.EmpresaActiva := fZProvider.Comparte;
    fPathPatch       := fPathConfig + 'Comparte\Patch\';
    fPathDiccionario := fPathConfig + 'Comparte\NuevaBD\Diccionario\';
    fPathEspeciales  := fPathConfig + 'Comparte\Patch\Especiales\';
    fDBTipo := tcOtro;        // Se pone otro por que no hay uno para Comparte
    fDBEspecial := TRUE;      // No se configura por DB_INFO - siempre busca scripts especiales.
    // No tiene reportes, por el momento...
  end else begin
    fGlobalTabla       := 'GLOBAL';
    fGlobalCampoNumero := 'GL_CODIGO';
    fGlobalCampoTexto  := 'GL_FORMULA';
    fGlobalVersion     := K_GLOBAL_VERSION_DATOS;

    try

      // Cambios para inluir commaText de CM_CODIGO asociados a la base de datos (DB_DATOS -> CM_DATOD)
      fZProvider.EmpresaActiva := fZProvider.Comparte;
      oQueryEmpresas := fZProvider.CreateQuery(Format(K_CONSULTA_EMPRESAS, [QuotedStr(fAlias)])) as TZADOQuery;
      with oQueryEmpresas do
      begin
        Open;
        First;
        while not Eof do
        begin
          if not StrLleno(fEmpresas) then
            fEmpresas := FieldByName ('CM_CODIGO').AsString
          else
            fEmpresas := fEmpresas + ',' + FieldByName ('CM_CODIGO').AsString;
          Next;
        end; //  while not Eof
      end; // with oQueryEmpresas
      // ==============================================================================================

      fZProvider.EmpresaActiva := fZProvider.Comparte;
      oQuery := fZProvider.CreateQuery(Format(K_CONSULTA_BD, [QuotedStr(fAlias)])) as TZADOQuery;
      with oQuery do begin
        Open;
        if not IsEmpty then begin
          fDBEspecial := FieldByName('DB_ESPC').AsBoolean;
          // Recuperar Globales
          fDBTipo := eTipoCompany(FieldByName('DB_TIPO').AsInteger);
          case fDBTipo of
            tc3Datos, tc3Prueba, tcPresupuesto:
              begin
                fDiccionarioMask := 'Diccion %d.xml';
                fPathPatch       := fPathConfig + 'Empleados\Patch\Estructura\';
                fPathDiccionario := fPathConfig + 'Empleados\Patch\Diccionario\';
                fPathReportes    := fPathConfig + 'Empleados\Patch\Reportes\';
                fPathEspeciales  := fPathConfig + 'Empleados\Patch\Especiales\';
              end;
            tcRecluta:
              begin
                fGlobalVersion   := 78;
                fPathPatch       := fPathConfig + 'Seleccion\Patch\';
                fPathDiccionario := fPathConfig + 'Seleccion\NuevaBD\Diccionario\';
                fPathReportes    := fPathConfig + 'Seleccion\NuevaBD\Iniciales\';
                fPathEspeciales  := fPathConfig + 'Seleccion\Patch\Especiales\';
              end;
            tcVisitas:
              begin
                fGlobalVersion   := 12;
                fPathPatch       := fPathConfig + 'Visitantes\Patch\';
                fPathDiccionario := fPathConfig + 'Visitantes\NuevaBD\Diccionario\';
                fPathReportes    := fPathConfig + 'Visitantes\NuevaBD\Iniciales\';
                fPathEspeciales  := fPathConfig + 'Visitantes\Patch\Especiales\';
              end;
            tcOtro:
              begin
                // Valores indefinidos
              end;
          end; // case
          //if not FieldByName('DB_ESPC').AsBoolean then
           // fPathEspeciales := '';
          fZProvider.adoEmpresa.Connected := False;
          fZProvider.EmpresaActiva := VarArrayOf([FieldByName('DB_DATOS').AsString, FieldByName('DB_USRNAME').AsString,
             FieldByName('DB_PASSWRD').AsString, 0]);
        end; // if not IsEmpty
      end; // with oQuery
    finally
      FreeAndNil(oQuery);
      FreeAndNil(oQueryEmpresas);
    end;
  end;

  oQuery := fZProvider.CreateQuery(Format(K_SELECT_VERSION, [fGlobalCampoTexto, fGlobalTabla, fGlobalCampoNumero, fGlobalVersion])) as TZADOQuery;
  try
    oQuery.Open;
    fDBVersion := StrToIntDef( Trim(oQuery.FieldByName(fGlobalCampoTexto).AsString),0);
  finally
    FreeAndNil(oQuery);
  end;

  if fPathPatch <> '' then begin
    oArchivos := TStringList.Create;
    SearchFile(fPathPatch, K_ESTRUCTURA_WILDCARD, oArchivos);
    for I := 0 to oArchivos.Count - 1 do begin
      sDatabase := ChangeFileExt(ExtractFileName(oArchivos[I]), ''); // Extraer nombre de archivo y Borrar ".sql"
      Delete(sDatabase, 1, 11);                                      // Borrar "SQL Server ", y queda s�lo el n�mero de versi�n
      iVersion := StrToIntDef(sDatabase, 0);
      // Detectar la versi�n m�s actual de los Patchs en disco
      if iVersion > fDBMaxVersion then
        fDBMaxVersion := iVersion;
      // Crear lista con Patchs a aplicar mayores a la versi�n actual
      if iVersion > fDBVersion then
        fDBVersiones.Add(sDatabase);
    end;
    FreeAndNil(oArchivos);

  end;

  if fFreeProvider then
    FreeAndNil(fZProvider)
  else
  begin
      if not (VarIsNull(Empresa)) then
        if(fZProvider.EmpresaActiva[P_ALIAS] <> Empresa[P_ALIAS]) then
          fZProvider.EmpresaActiva := Empresa;
  end;
end;

end.
