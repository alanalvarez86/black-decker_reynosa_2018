library Imss;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  Imss_TLB in '..\MTS\Imss_TLB.pas',
  DServerImss in '..\MTS\DServerImss.pas' {dmServerIMSS: TMtsDataModule} {dmServerImss: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Imss.TLB}

{$R *.RES}

begin
end.
