library Tablas;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  ComServ,
  Tablas_TLB in '..\MTS\Tablas_TLB.pas',
  DServerTablas in '..\MTS\DServerTablas.pas' {dmServerTablas: TMtsDataModule} {dmServerTablas: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\Tablas.TLB}

{$R *.RES}

begin
end.
