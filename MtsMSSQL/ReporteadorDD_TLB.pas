unit ReporteadorDD_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 52393 $
// File generated on 4/2/2014 11:41:15 AM from Type Library described below.

// ************************************************************************  //
// Type Lib: F:\3win_30_Vsn_2014\MtsMSSQL\ReporteadorDD (1)
// LIBID: {7C0BB0D9-98B4-47F3-9E37-326F426CD469}
// LCID: 0
// Helpfile:
// HelpString: ReporteadorDD Library
// DepndLst:
//   (1) v2.0 stdole, (C:\WINDOWS\system32\stdole2.tlb)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Windows, Classes, Variants, StdVCL, Graphics, OleServer, ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  ReporteadorDDMajorVersion = 1;
  ReporteadorDDMinorVersion = 0;

  LIBID_ReporteadorDD: TGUID = '{7C0BB0D9-98B4-47F3-9E37-326F426CD469}';

  IID_IdmServerReporteadorDD: TGUID = '{0A1DEDEC-5938-49F1-928F-A3B50355E8CD}';
  CLASS_dmServerReporteadorDD: TGUID = '{CB7C6272-A65B-48B5-BE0D-B9863C1BA11A}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  IdmServerReporteadorDD = interface;
  IdmServerReporteadorDDDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  dmServerReporteadorDD = IdmServerReporteadorDD;


// *********************************************************************//
// Interface: IdmServerReporteadorDD
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0A1DEDEC-5938-49F1-928F-A3B50355E8CD}
// *********************************************************************//
  IdmServerReporteadorDD = interface(IDispatch)
    ['{0A1DEDEC-5938-49F1-928F-A3B50355E8CD}']
    function GrabaDiccion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; safecall;
    function GetListaFunciones(Empresa: OleVariant): OleVariant; safecall;
    function GetListaGlobal(Empresa: OleVariant): OleVariant; safecall;
    function PruebaFormula(Empresa: OleVariant; Parametros: OleVariant; var Formula: WideString;
                           Entidad: Integer; ParamsRep: OleVariant): WordBool; safecall;
    function GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool;
                            const Filtro: WideString): OleVariant; safecall;
    function GetListaClasifi(Empresa: OleVariant): OleVariant; safecall;
    function GetTablasPorClasifi(Empresa: OleVariant; Clasifi: Integer): OleVariant; safecall;
    function GetRelaciones(Empresa: OleVariant; Entidad: Integer): OleVariant; safecall;
    function CamposPorTabla(Empresa: OleVariant; Entidad: Integer): OleVariant; safecall;
    function GetTablaGenerica(Empresa: OleVariant; Entidad: Integer; var sDatosLookup: WideString): OleVariant; safecall;
    function GetDerechosClasifi(Empresa: OleVariant; Grupo: Integer): OleVariant; safecall;
    function GetDatosDefault(Empresa: OleVariant; Entidad: Integer): OleVariant; safecall;
    function GetDerechosEntidades(Empresa: OleVariant; Grupo: Integer): OleVariant; safecall;
    function GrabaAccesosEntidad(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant;
                                 out ErrorCount: Integer): OleVariant; safecall;
    function GrabaAccesosClasifi(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant;
                                 out ErrorCount: Integer): OleVariant; safecall;
    function GetListasFijas(Empresa: OleVariant): OleVariant; safecall;
    function OrdenCambiar(Empresa: OleVariant; Dataset: Integer; Codigo: Integer;
                          OrdenViejo: Integer; OrdenNuevo: Integer): OleVariant; safecall;
    function EntidadesPorModulo(Empresa: OleVariant): OleVariant; safecall;
    function GetModulos(Empresa: OleVariant): OleVariant; safecall;
    function GetListasFijasValores(Empresa: OleVariant): OleVariant; safecall;
    function GrabaListasFijas(Empresa: OleVariant; Delta: OleVariant; DeltaValores: OleVariant;
                              var ErrorCount: Integer; iVersion: Integer; iUsuario: Integer): OleVariant; safecall;
    function GrabaDataset(Empresa: OleVariant; Delta: OleVariant; TablaInfo: Integer;
                          var ErrorCount: Integer; var Posicion: Integer): OleVariant; safecall;
    function GetEntidades(Empresa: OleVariant): OleVariant; safecall;
    procedure GetEditEntidades(Empresa: OleVariant; Entidad: Integer; var Campos: OleVariant;
                               var Relaciones: OleVariant; var Modulos: OleVariant;
                               var Clasifi: OleVariant; var CamposDef: OleVariant;
                               var OrdenDef: OleVariant; var FiltrosDef: OleVariant); safecall;
    function AgregarCamposFaltantes(Empresa: OleVariant; Campos: OleVariant; Entidad: OleVariant): OleVariant; safecall;
    function GetReportes(Empresa: OleVariant; iClasifActivo: Integer; iFavoritos: Integer;
                         iSuscripciones: Integer; const sClasificaciones: WideString;
                         lverConfidencial: WordBool): OleVariant; safecall;
    function GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant; safecall;
    function GetLookUpReportes(Empresa: OleVariant; lverConfidencial: WordBool): OleVariant; safecall;
    function GrabaReporte(Empresa: OleVariant; oDelta: OleVariant; oCampoRep: OleVariant;
                          out ErrorCount: Integer; var iReporte: Integer): OleVariant; safecall;
    function GetEscogeReporte(Empresa: OleVariant; Entidad: Integer; Tipo: Integer;
                              lverConfidencial: WordBool): OleVariant; safecall;
    function CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant; safecall;
    function BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant; safecall;
    function GrabaPoliza(Empresa: OleVariant; Delta: OleVariant; CampoRep: OleVariant;
                         out ErrorCount: Integer; var iReporte: Integer): OleVariant; safecall;
    function GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer; const sUsuarios: WideString;
                            const sReportes: WideString; var Usuarios: OleVariant): OleVariant; safecall;
    function BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant; safecall;
    function AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer; safecall;
    function GetEscogeReportes(Empresa: OleVariant; const Entidades: WideString;
                               const Tipos: WideString; lverConfidencial: WordBool): OleVariant; safecall;
    function GetPlantilla(const Nombre: WideString): OleVariant; safecall;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; var Parametros: OleVariant;
                       out Error: WideString): OleVariant; safecall;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
                         out Error: WideString): WordBool; safecall;
    function GetFoto(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString): OleVariant; safecall;
    function DirectorioPlantillas(Empresa: OleVariant): WideString; safecall;
    function GetPrimaryKey(Empresa: OleVariant; const TableName: WideString): WideString; safecall;
    function ExportarDiccionario(Empresa: OleVariant; Parametros: OleVariant;
                                 var XMLFile: WideString): OleVariant; safecall;
    function DiccionarioListaTablasGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ImportarDiccionario(Empresa: OleVariant; Parametros: OleVariant): OleVariant; safecall;
    function ExportarDiccionarioLista(Empresa: OleVariant; Lista: OleVariant;
                                      Parametros: OleVariant; var XMLFile: WideString): OleVariant; safecall;
    function SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer;
                       var MsgError: WideString): WordBool; safecall;
    function GetDocumento(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString;
                          var Extension: WideString): OleVariant; safecall;
    function GetReportesTodos(Empresa: OleVariant; const sClasificaciones: WideString;
                              lVerConfidencial: WordBool): OleVariant; safecall;
  end;

// *********************************************************************//
// DispIntf:  IdmServerReporteadorDDDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0A1DEDEC-5938-49F1-928F-A3B50355E8CD}
// *********************************************************************//
  IdmServerReporteadorDDDisp = dispinterface
    ['{0A1DEDEC-5938-49F1-928F-A3B50355E8CD}']
    function GrabaDiccion(Empresa: OleVariant; oDelta: OleVariant; out ErrorCount: Integer): OleVariant; dispid 8;
    function GetListaFunciones(Empresa: OleVariant): OleVariant; dispid 9;
    function GetListaGlobal(Empresa: OleVariant): OleVariant; dispid 10;
    function PruebaFormula(Empresa: OleVariant; Parametros: OleVariant; var Formula: WideString;
                           Entidad: Integer; ParamsRep: OleVariant): WordBool; dispid 11;
    function GetDiccionario(Empresa: OleVariant; lverConfidencial: WordBool;
                            const Filtro: WideString): OleVariant; dispid 12;
    function GetListaClasifi(Empresa: OleVariant): OleVariant; dispid 301;
    function GetTablasPorClasifi(Empresa: OleVariant; Clasifi: Integer): OleVariant; dispid 302;
    function GetRelaciones(Empresa: OleVariant; Entidad: Integer): OleVariant; dispid 303;
    function CamposPorTabla(Empresa: OleVariant; Entidad: Integer): OleVariant; dispid 304;
    function GetTablaGenerica(Empresa: OleVariant; Entidad: Integer; var sDatosLookup: WideString): OleVariant; dispid 305;
    function GetDerechosClasifi(Empresa: OleVariant; Grupo: Integer): OleVariant; dispid 306;
    function GetDatosDefault(Empresa: OleVariant; Entidad: Integer): OleVariant; dispid 307;
    function GetDerechosEntidades(Empresa: OleVariant; Grupo: Integer): OleVariant; dispid 309;
    function GrabaAccesosEntidad(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant;
                                 out ErrorCount: Integer): OleVariant; dispid 310;
    function GrabaAccesosClasifi(Empresa: OleVariant; Grupo: Integer; Delta: OleVariant;
                                 out ErrorCount: Integer): OleVariant; dispid 311;
    function GetListasFijas(Empresa: OleVariant): OleVariant; dispid 308;
    function OrdenCambiar(Empresa: OleVariant; Dataset: Integer; Codigo: Integer;
                          OrdenViejo: Integer; OrdenNuevo: Integer): OleVariant; dispid 313;
    function EntidadesPorModulo(Empresa: OleVariant): OleVariant; dispid 314;
    function GetModulos(Empresa: OleVariant): OleVariant; dispid 315;
    function GetListasFijasValores(Empresa: OleVariant): OleVariant; dispid 316;
    function GrabaListasFijas(Empresa: OleVariant; Delta: OleVariant; DeltaValores: OleVariant;
                              var ErrorCount: Integer; iVersion: Integer; iUsuario: Integer): OleVariant; dispid 317;
    function GrabaDataset(Empresa: OleVariant; Delta: OleVariant; TablaInfo: Integer;
                          var ErrorCount: Integer; var Posicion: Integer): OleVariant; dispid 318;
    function GetEntidades(Empresa: OleVariant): OleVariant; dispid 320;
    procedure GetEditEntidades(Empresa: OleVariant; Entidad: Integer; var Campos: OleVariant;
                               var Relaciones: OleVariant; var Modulos: OleVariant;
                               var Clasifi: OleVariant; var CamposDef: OleVariant;
                               var OrdenDef: OleVariant; var FiltrosDef: OleVariant); dispid 322;
    function AgregarCamposFaltantes(Empresa: OleVariant; Campos: OleVariant; Entidad: OleVariant): OleVariant; dispid 328;
    function GetReportes(Empresa: OleVariant; iClasifActivo: Integer; iFavoritos: Integer;
                         iSuscripciones: Integer; const sClasificaciones: WideString;
                         lverConfidencial: WordBool): OleVariant; dispid 101;
    function GetEditReportes(Empresa: OleVariant; iReporte: Integer; var CampoRep: OleVariant): OleVariant; dispid 102;
    function GetLookUpReportes(Empresa: OleVariant; lverConfidencial: WordBool): OleVariant; dispid 103;
    function GrabaReporte(Empresa: OleVariant; oDelta: OleVariant; oCampoRep: OleVariant;
                          out ErrorCount: Integer; var iReporte: Integer): OleVariant; dispid 104;
    function GetEscogeReporte(Empresa: OleVariant; Entidad: Integer; Tipo: Integer;
                              lverConfidencial: WordBool): OleVariant; dispid 105;
    function CampoRepFiltros(Empresa: OleVariant; Reporte: Integer): OleVariant; dispid 106;
    function BorraReporte(Empresa: OleVariant; iReporte: Integer): OleVariant; dispid 107;
    function GrabaPoliza(Empresa: OleVariant; Delta: OleVariant; CampoRep: OleVariant;
                         out ErrorCount: Integer; var iReporte: Integer): OleVariant; dispid 108;
    function GetSuscripcion(Empresa: OleVariant; iFrecuencia: Integer; const sUsuarios: WideString;
                            const sReportes: WideString; var Usuarios: OleVariant): OleVariant; dispid 109;
    function BorraFavoritos(Empresa: OleVariant; iReporte: Integer): OleVariant; dispid 110;
    function AgregaFavoritos(Empresa: OleVariant; iReporte: Integer): Integer; dispid 111;
    function GetEscogeReportes(Empresa: OleVariant; const Entidades: WideString;
                               const Tipos: WideString; lverConfidencial: WordBool): OleVariant; dispid 112;
    function GetPlantilla(const Nombre: WideString): OleVariant; dispid 113;
    function GeneraSQL(Empresa: OleVariant; var AgenteSQL: OleVariant; var Parametros: OleVariant;
                       out Error: WideString): OleVariant; dispid 201;
    function EvaluaParam(Empresa: OleVariant; var AgenteSQL: OleVariant; Parametros: OleVariant;
                         out Error: WideString): WordBool; dispid 202;
    function GetFoto(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString): OleVariant; dispid 203;
    function DirectorioPlantillas(Empresa: OleVariant): WideString; dispid 204;
    function GetPrimaryKey(Empresa: OleVariant; const TableName: WideString): WideString; dispid 205;
    function ExportarDiccionario(Empresa: OleVariant; Parametros: OleVariant;
                                 var XMLFile: WideString): OleVariant; dispid 206;
    function DiccionarioListaTablasGetLista(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 207;
    function ImportarDiccionario(Empresa: OleVariant; Parametros: OleVariant): OleVariant; dispid 208;
    function ExportarDiccionarioLista(Empresa: OleVariant; Lista: OleVariant;
                                      Parametros: OleVariant; var XMLFile: WideString): OleVariant; dispid 209;
    function SQLValido(Empresa: OleVariant; const Formula: WideString; Entidad: Integer;
                       var MsgError: WideString): WordBool; dispid 210;
    function GetDocumento(Empresa: OleVariant; Empleado: Integer; const Tipo: WideString;
                          var Extension: WideString): OleVariant; dispid 211;
    function GetReportesTodos(Empresa: OleVariant; const sClasificaciones: WideString;
                              lVerConfidencial: WordBool): OleVariant; dispid 212;
  end;

// *********************************************************************//
// The Class CodmServerReporteadorDD provides a Create and CreateRemote method to
// create instances of the default interface IdmServerReporteadorDD exposed by
// the CoClass dmServerReporteadorDD. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CodmServerReporteadorDD = class
    class function Create: IdmServerReporteadorDD;
    class function CreateRemote(const MachineName: string): IdmServerReporteadorDD;
  end;

implementation

uses ComObj;

class function CodmServerReporteadorDD.Create: IdmServerReporteadorDD;
begin
  Result := CreateComObject(CLASS_dmServerReporteadorDD) as IdmServerReporteadorDD;
end;

class function CodmServerReporteadorDD.CreateRemote(const MachineName: string): IdmServerReporteadorDD;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_dmServerReporteadorDD) as IdmServerReporteadorDD;
end;

end.

