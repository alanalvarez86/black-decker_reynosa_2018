library TimbradoSistema;
{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}
uses
  MidasLib,
  ComServ,
  TimbradoSistema_TLB in '..\MTS\TimbradoSistema_TLB.pas',
  DServerSistemaTimbrado in '..\MTS\DServerSistemaTimbrado.pas' {dmServerSistema: TMtsDataModule} {dmServerSistema: CoClass};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\MTS\TimbradoSistema.TLB}

{$R *.RES}

begin
end.
