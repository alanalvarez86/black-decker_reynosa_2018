program L5Poll;

uses
  Forms,
  SysUtils,
  FLinxBase in '..\Linx.5\LinxBase\FLinxBase.pas' {LinxBase},
  FAcercaDe in '..\Linx.5\LinxBase\FAcercaDe.pas' {AcercaDe},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FLinx5Poll in '..\Linx.5\L5Poll\FLinx5Poll.pas' {Linx5Poll};

{$R *.RES}
{$R WindowsXP.res}

begin
     Application.Initialize;
     Application.Title := 'L5Poll';
     Application.HelpFile := 'L5Poll.chm';
  Application.CreateForm(TLinx5Poll, Linx5Poll);
     case ParamCount of
          1:
          begin
               Linx5Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ) );
               FreeAndNil( Linx5Poll );
          end;
          2:
          begin
               Linx5Poll.ProcesaParametro( UpperCase( Trim( ParamStr( 1 ) ) ), ParamStr( 2 ) );
               FreeAndNil( Linx5Poll );
          end;
     end;
     Application.Run;
end.
