library TressExcel;

uses
  ComServ,
  TressExcel_TLB in '..\Reportes\Excel3\TressExcel_TLB.pas',
  FTress2Excel in '..\Reportes\Excel3\FTress2Excel.pas' {Tress2Excel: CoClass},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DReportesGenerador in '..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DBaseDiccionario in '..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in '..\Reportes\Excel3\DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FEscogerReporte in '..\Reportes\Excel3\FEscogerReporte.pas' {EscogerReporte};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R ..\Reportes\Excel3\TressExcel.TLB}

{$R *.RES}

begin
end.
