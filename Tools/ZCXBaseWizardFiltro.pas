unit ZCXBaseWizardFiltro;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ComCtrls, ExtCtrls, DbGrids,
     ZetaCommonLists,
     ZcxBaseWizard,
     ZetaEdit, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore,
     TressMorado2013, cxControls, cxContainer, cxEdit, ImgList, cxImage,
     cxButtons, ZetaCXWizard, ZetaKeyLookup_DevEx, cxTextEdit, cxMemo,
     cxRadioGroup, dxCustomWizardControl, dxWizardControl, cxGroupBox,
  cxLabel, dxGDIPlusClasses, cxScrollBar, dxSkinsDefaultPainters;

type
  TBaseCXWizardFiltro = class(TcxBaseWizard)

    FiltrosCondiciones: TdxWizardControlPage;
    Seleccionar: TcxButton;
    ECondicion: TZetaKeyLookup_DevEx;
    sCondicionLBl: TLabel;
    sFiltroLBL: TLabel;
    sFiltro: TcxMemo;
    GBRango: TGroupBox;
    lbInicial: TLabel;
    lbFinal: TLabel;
    RBTodos: TcxRadioButton;
    RBRango: TcxRadioButton;
    RBLista: TcxRadioButton;
    EInicial: TZetaEdit;
    EFinal: TZetaEdit;
    ELista: TZetaEdit;
    BInicial: TcxButton;
    BFinal: TcxButton;
    BLista: TcxButton;
    bAjusteISR: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RBTodosClick(Sender: TObject);
    procedure RBRangoClick(Sender: TObject);
    procedure RBListaClick(Sender: TObject);
    procedure BInicialClick(Sender: TObject);
    procedure BFinalClick(Sender: TObject);
    procedure BListaClick(Sender: TObject);
    procedure BAgregaCampoClick(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
  private
    { Private declarations }
    FTipoRango: eTipoRangoActivo;
    FEmpleadoCodigo: String;
    FEmpleadoFiltro: String;
    FVerificacion: Boolean;
    function BuscaEmpleado(const lConcatena: Boolean; const sLlave: String): String;
    procedure SetVerificacion( const Value: Boolean );
  protected
    { Protected declarations }
    property EmpleadoCodigo: String read FEmpleadoCodigo write FEmpleadoCodigo;
    property EmpleadoFiltro: String read FEmpleadoFiltro write FEmpleadoFiltro;
    property Verificacion: Boolean read FVerificacion write FVerificacion;
    function Verificar: Boolean; virtual; abstract;
    function GetFiltro: String;
    function GetRango: String;
    function GetCondicion: String;
    procedure CargaListaVerificacion; virtual; abstract;
    procedure CargaParametros; override;
    procedure EnabledBotones( const eTipo: eTipoRangoActivo );
  public
    { Public declarations }
    property TipoRango: eTipoRangoActivo read FTipoRango;
  end;

var
  BaseCXWizardFiltro: TBaseCXWizardFiltro;                              

implementation

uses ZetaClientTools,
     ZetaTipoEntidad,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx,
     ZConstruyeFormula,
     ZConfirmaVerificacion_DevEx,
     DCliente,
     DCatalogos, ZcxWizardBasico;

{$R *.DFM}

{ ************ TBaseWizardFiltro ************ }

procedure TBaseCXWizardFiltro.FormCreate(Sender: TObject);
begin
     inherited;
     FEmpleadoCodigo := ARROBA_TABLA + '.CB_CODIGO';
     FEmpleadoFiltro := '';
     ECondicion.LookupDataset := dmCatalogos.cdsCondiciones;
     Ejecucion.PageIndex := WizardControl.PageCount - 1;  
end;

procedure TBaseCXWizardFiltro.FormShow(Sender: TObject);
begin
     inherited;
     BajusteISR.Left := Sfiltro.Left + 315;
     BajusteISR.Top := Sfiltro.Top +1 ;
     FVerificacion := False;
     ECondicion.Llave := VACIO;
     sFiltro.Text := VACIO;
     ELista.Text := IntToStr( dmCliente.Empleado );
     RBTodos.Checked := True;
     dmCatalogos.cdsCondiciones.Conectar;
end;

procedure TBaseCXWizardFiltro.SetVerificacion( const Value: Boolean );
begin
     FVerificacion := FVerificacion or Value;
end;

procedure TBaseCXWizardFiltro.EnabledBotones( const eTipo: eTipoRangoActivo );
var
   lEnabled: Boolean;
begin
     FTipoRango := eTipo;
     lEnabled := ( eTipo = raRango );
     lbInicial.Enabled := lEnabled;
     lbFinal.Enabled := lEnabled;
     EInicial.Enabled := lEnabled;
     EFinal.Enabled := lEnabled;
     bInicial.Enabled := lEnabled;
     bFinal.Enabled := lEnabled;
     lEnabled := ( eTipo = raLista );
     ELista.Enabled := lEnabled;
     bLista.Enabled := lEnabled;
end;

function TBaseCXWizardFiltro.GetRango: String;
begin
     case TipoRango of
          raRango: Result := GetFiltroRango( FEmpleadoCodigo, Trim( EInicial.Text ), Trim( EFinal.Text ) );
          raLista: Result := GetFiltroLista( FEmpleadoCodigo, Trim( ELista.Text ) );
     else
         Result := '';
     end;
end;

function TBaseCXWizardFiltro.GetCondicion: String;
begin
     if StrLleno( ECondicion.Llave ) then
     begin
          Result := Trim( ECondicion.LookUpDataSet.FieldByName( 'QU_FILTRO' ).AsString );
          { Causa problemas porque le quita la '@'; los paréntesis no son necesarios
          if EsFormula( Result ) then
             Result := Parentesis( Result );
          }
     end
     else
         Result := '';
end;

function TBaseCXWizardFiltro.GetFiltro: String;
begin
     Result := Trim( sFiltro.Text );
     { Los Paréntesis no son necesarios
     if StrLleno( Result ) then
        Result := Parentesis( Result );
     }
end;

procedure TBaseCXWizardFiltro.CargaParametros;
begin
     Descripciones.Clear;
     with ParameterList do
     begin
          AddString( 'RangoLista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

     with Descripciones do
     begin
          AddString( 'Lista', GetRango );
          AddString( 'Condicion', GetCondicion );
          AddString( 'Filtro', GetFiltro );
     end;

     with dmCliente do
     begin
          CargaActivosIMSS( ParameterList );
          CargaActivosPeriodo( ParameterList );
          CargaActivosSistema( ParameterList );
     end;
     ParamInicial := 3;
end;

function TBaseCXWizardFiltro.BuscaEmpleado( const lConcatena: Boolean; const sLlave: String ): String;
var
   sKey, sDescripcion: String;
begin
     Result := sLlave;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( FEmpleadoFiltro, sKey, sDescripcion ) then
     begin
          if lConcatena and ZetaCommonTools.StrLleno( Text ) then
             Result := sLlave + ',' + sKey
          else
              Result := sKey;
     end;
end;

{ ********** Eventos ******** }

procedure TBaseCXWizardFiltro.RBTodosClick(Sender: TObject);
begin
     EnabledBotones( raTodos );
end;

procedure TBaseCXWizardFiltro.RBRangoClick(Sender: TObject);
begin
     EnabledBotones( raRango );
end;

procedure TBaseCXWizardFiltro.RBListaClick(Sender: TObject);
begin
     EnabledBotones( raLista );
end;

procedure TBaseCXWizardFiltro.BInicialClick(Sender: TObject);
begin
     with EInicial do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TBaseCXWizardFiltro.BFinalClick(Sender: TObject);
begin
     with EFinal do
     begin
          Text := BuscaEmpleado( False, Text );
     end;
end;

procedure TBaseCXWizardFiltro.BListaClick(Sender: TObject);
begin
     with ELista do
     begin
          Text := BuscaEmpleado( True, Text );
     end;
end;

procedure TBaseCXWizardFiltro.BAgregaCampoClick(Sender: TObject);
begin
     with sFiltro do
     begin
          Text:= ZConstruyeFormula.GetFormulaConst( enEmpleado, Text, SelStart, evBase );
     end;
end;

procedure TBaseCXWizardFiltro.SeleccionarClick(Sender: TObject);
var
   lCarga: Boolean;
   lOk: Boolean;
   oCursor: TCursor;
begin
     inherited;
     if Verificacion then
     begin
          case ZConfirmaVerificacion_DevEx.ConfirmVerification of
               mrYes:
               begin
                    lCarga := True;
                    lOk := True;
               end;
               mrNo:
               begin
                    lCarga := False;
                    lOk := True;
               end;
          else
              begin
                   lCarga := False;
                   lOk := False;
              end;
          end;
     end
     else
     begin
          lCarga := True;
          lOk := True;
     end;
     if lOk then
     begin
          if lCarga then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  CargaParametros;
                  CargaListaVerificacion;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
          SetVerificacion( Verificar );
     end;
end;

{
 TODO: Remover?
procedure TBaseCXWizardFiltro.EjecutarClick(Sender: TObject);
begin
     //Ejecutar.Visible:=false;
     inherited;
end;}

end.



