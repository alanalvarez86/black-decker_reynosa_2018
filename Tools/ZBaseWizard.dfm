inherited BaseWizard: TBaseWizard
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'BaseWizard'
  ClientWidth = 411
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Width = 411
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    AfterMove = WizardAfterMove
    DesignSize = (
      411
      36)
    inherited Salir: TZetaWizardButton
      Left = 320
      Kind = bkCancel
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 237
    end
  end
  inherited PageControl: TPageControl
    Width = 411
    ActivePage = Parametros
    TabHeight = 1
    TabWidth = 1
    object Parametros: TTabSheet
      Caption = 'Parametros'
      ImageIndex = 1
      TabVisible = False
    end
    object Ejecucion: TTabSheet
      Caption = 'Ejecucion'
      ImageIndex = 2
      TabVisible = False
      object Advertencia: TMemo
        Left = 0
        Top = 0
        Width = 403
        Height = 145
        Align = alTop
        Alignment = taCenter
        Color = clInfoBk
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Lines.Strings = (
          'Advertencia')
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object ProgressPanel: TPanel
        Left = 0
        Top = 165
        Width = 403
        Height = 67
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object ProgressBar: TProgressBar
          Left = 24
          Top = 3
          Width = 358
          Height = 17
          Step = 1
          TabOrder = 0
          Visible = False
        end
      end
    end
  end
end
