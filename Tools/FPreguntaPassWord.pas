unit FPreguntaPassWord;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, Buttons,
  ExtCtrls, ZetaEdit;

type
  TPreguntaPassWord = class(TZetaDlgModal)
    LblCred: TLabel;
    ePassWord: TZetaEdit;
    procedure FormShow(Sender: TObject);
  private
    function GetPassWord: string;
    { Private declarations }
  public
    property Clave : string read GetPassWord;
  end;

var
  PreguntaPassWord: TPreguntaPassWord;

  function ValidaPasswordOperador( const sClave: String ): Boolean;

implementation

uses ZetaCommonClasses, ZetaDialogo;

{$R *.DFM}

function ValidaPasswordOperador( const sClave: String ): Boolean;
begin
     Result := TRUE;
     if ( sClave <> VACIO ) then
     begin
          if ( PreguntaPassWord = nil ) then
             PreguntaPassWord := TPreguntaPassWord.Create(Application);
          with PreguntaPassWord do
          begin
               ShowModal;
               Result := ( ModalResult = mrOk );
               if Result then                    // Si presiona Cancelar no se permite acceso pero no se reporta error
               begin
                    Result := ( Clave = sClave );
                    if ( not Result ) then
                       ZetaDialogo.ZError( Caption, 'Clave Incorrecta' , 0 );
               end;
          end;
     end;
end;

procedure TPreguntaPassWord.FormShow(Sender: TObject);
begin
     inherited;
     ePassWord.SetFocus;
     ePassWord.Text := '';
end;

function TPreguntaPassWord.GetPassWord: string;
begin
     Result := ePassWord.Text;
end;

end.
