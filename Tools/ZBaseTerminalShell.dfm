object BaseTerminalShell: TBaseTerminalShell
  Left = 599
  Top = 238
  Caption = 'BaseTerminalShell'
  ClientHeight = 729
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelGeneral: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 729
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial Black'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    object PanelInfoAdicional: TPanel
      Left = 2
      Top = 265
      Width = 1004
      Height = 391
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 1
      BorderStyle = bsSingle
      TabOrder = 0
    end
    object PanelBanner: TPanel
      Left = 2
      Top = 656
      Width = 1004
      Height = 71
      Align = alBottom
      BevelOuter = bvNone
      BorderWidth = 1
      BorderStyle = bsSingle
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      object Banner: TZetaBanner
        Left = 101
        Top = 1
        Width = 757
        Height = 67
        Active = False
        Align = alClient
        Caption = 'Banner'
        Color = clInactiveCaption
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        StepsPerSecond = 20
        StepWidth = 1
        ExplicitLeft = 4
        ExplicitTop = 4
        ExplicitWidth = 632
        ExplicitHeight = 63
      end
      object panelDemo: TPanel
        Left = 1
        Top = 1
        Width = 100
        Height = 67
        Align = alLeft
        BevelOuter = bvNone
        BorderWidth = 1
        BorderStyle = bsSingle
        TabOrder = 0
        object lblDEMO: TLabel
          Left = 5
          Top = 18
          Width = 97
          Height = 30
          Alignment = taCenter
          AutoSize = False
          Caption = 'DEMO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Arial Black'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
      end
      object panelEnLinea: TPanel
        Left = 858
        Top = 1
        Width = 143
        Height = 67
        Align = alRight
        BevelOuter = bvNone
        BorderWidth = 1
        BorderStyle = bsSingle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial Black'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object LblConectado: TLabel
          Left = 5
          Top = 21
          Width = 132
          Height = 23
          Alignment = taCenter
          AutoSize = False
          Caption = 'Fuera de l'#237'nea'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
      end
    end
    object panelSuperior: TPanel
      Left = 2
      Top = 2
      Width = 1004
      Height = 263
      Align = alTop
      TabOrder = 2
      object PanelFoto: TPanel
        Left = 1
        Top = 1
        Width = 196
        Height = 261
        Align = alLeft
        BevelOuter = bvLowered
        TabOrder = 0
        object Foto: TImage
          Left = 1
          Top = 1
          Width = 194
          Height = 259
          Align = alClient
          Center = True
          Picture.Data = {
            0B546478504E47496D61676589504E470D0A1A0A0000000D49484452000000D8
            000001190806000000F5E3EB480000000467414D410000B18F0BFC6105000000
            097048597300000EC300000EC301C76FA86400000B4D49444154785EEDDDED56
            DB481685E1B9FF3B9CE9806D704208600804FC45E2611BB19AC031D8566D59D2
            79F75ACF1FA6BB2756B4B154AA2AFDE7CB7FBFAC00785030C088820146140C30
            A260801105038C28186044C100230A06185130C088820146140C30A260801105
            038C28186044C100230A06185130C088820146140C30A260801105038C281860
            44C100230A06185130C088820146140C30A260801105038C28186044C100230A
            06185130C088820146140C30A260801105038C28186044C100230A06185130C0
            88820146140C30A260801105038C28186044C100230A06185130C08882014614
            0C30A260801105038C28186044C100230AD65283A3C16A7C325E7D3DFDBA3A3F
            3B0FE97FD33FA37F36FA6FE0F028D8811DFDEF685D92CB1F97ABBBDBBBD56C36
            5BFDFEFD7BB56BF4EFE8DFD57FE3EAE26A5DBEE37F8EC3FF4F3487821DC0E9F0
            7435B99AAC66D3D9EACF9F3F55453C51E96E2637EB12477F167851B0868C8E47
            EB522D168BEAD46F3ECBE5725DB693E149F867447914CCEC7474BABE6C737F53
            ED9AFB5FF7AB6FE36FE19F19E55030135D924D1FA6D5E9DCDECC67F3D5F76FDF
            C3CF80FA285861C3E3E1FA1BAB6BD12F037DDB469F09FBA36085683470723969
            DDA5E0AEF979F393D1C782285801FAD6D288605FB25C2C57E311A38E2550B09A
            74FFB2CF73ABB647DFC4971797EB6FE6E873633B14AC060D79F73DBFEE7E71C9
            580305DB837EABDFFEBCAD4EC1FE470320832F4CC7DA0705DB917E9BEB1952B6
            2CE60BE63CEE8182ED40DF5C0FF70FD529972F946C77146C075D7CBE553A1A2D
            E59E6C7B146C4B190634B68DBEC5195DDC0E05DBC2C5F945756A9197E8817474
            ACF0370AF609CD3CEFFAEC0C5798C3F8390AF6015D0669322C89A307ECC3A361
            78ECF08C827D409741E4E3E81919F7639B51B00DB4EA986C17ED0F121D4350B0
            8D323FEFDA358FCB47BEC536A06001ADF425BB454B75A263991D057B43BF8935
            6381EC160D7830CBE33D0AF686869EC97EB9BEBA0E8F696614EC8DE9B4FDFB68
            B435FA16E35EEC6F14EC15ADE225F5A2592FD1B1CD8A82BDC264DEFAD1BE8FD1
            B1CD8A8255B4A090295165A26DBBA3639C1105ABFCF8FEA33A3D48DD3011F85F
            14ACC283E572797CE4C1F30B0AF6440B08B93C2C1B2E139F51B0279A4B47CA86
            CBC46714EC49A61DA29A8A362F8D8E753614EC0953A33C61EA14055BDF7F114F
            CEBE9E85C73C93F40563E6BC2FCC4DA460EB6516C4133DFA888E7926E90BA6BD
            D789277A1E161DF34CD2174C2F0927BE64DFA4347DC1FAF8EAA13625FB0BD753
            178C11447FB2BF683D75C1D839CA9FECEBC352174CCF698837D793DC43F5A90B
            C61C447F340D2D3AF6595030620D050B7E98C5C50FDE9AE28E9E3346C73E8BD4
            056316873FD967735030620D050B7E980505F38782053FCC8282F943C1821F66
            71F9E3B23A0D882BF7BFEEC3639F45EA82314CEF0FC3F4C10FB3A060FE64DFFC
            2675C1B4B518F146F7B9D1B1CF8282116B2858F0C32C46C7A3EA3420AE684BF2
            E8D86791BA607AE103F1462F348C8E7D16A90B26DA3782F8C28AE6E08799E841
            28F144FBFD677F0944FA8269189978329FCDC3639E49FA82B164C597EC4B5524
            7DC1D8D9D71776F6A560ABE1D1B03A1D48E9641F4194F40513F646F444BB7645
            C73B130AF68491C4F2D12F2D5E234BC1D65817563ED997A9BCA0604FC627E3EA
            B420A5A2B576D1B1CE86823DD1A50CF76165C3FDD7330A56D1250D2993C725AF
            2D7A41C12A97176C1F502A77B777E131CE888255C623EEC34A25FB1295D72858
            85FBB072D1C3FBE8186744C15E61E26FFD4C1FA6E1B1CD8A82BDC21602F5C3E5
            E1DF28D81BCBE5B23A55C8AED1FA2FAD128F8E6B5614EC0DCD0027FB85E529EF
            51B03778ADECFE61F6FC7B142C309BCDAA53866C1B26F7C62858E0EAE2AA3A6D
            C8B6C9BE83EF26142C30381AAC6FD8C9F661EE618C826D7033B9A94E1DF25958
            9AB21905DB40BBFEF22DB65DF4FC303A86A0601FD2AB77C8C7D1805074ECF08C
            827D8021FBCFC3D0FCC728D827F4F094C4592C160CCD7F82827D82F9899BA34D
            5BA363867F51B02DB0DAF97DF8F6DA0E05DBC268C088E2DB6847E4E858E16F14
            6C4B37D73C177B89F6918C8E11DEA3605B3AFEE79815CF4FD137B9BED1A36384
            F728D80E7813CB6AFD4D1E1D1BC428D80E7453AF9BFBACD137B8BEC9A3638318
            05DB51E6D71D9D9F9D87C7049B51B03D641CF06042EF7E28D81ED6978AF33C97
            8A7A513C7B6DEC8782ED49F314B33C1BE399D7FE28580D7A8348DFC34AE57A28
            580DBA54ECF3CBFB968B25A3863551B09A86C7C3F5DB44FA160DC99F8ED806A0
            2E0A56805E1CD1A7591EBAB73CFB7A167E56EC868215A285877D096FA72C8782
            15D487EDDE18D4288B8215D6E57D3CF43099355E6551B0C2748276716760DD77
            3162581E0533E8E2F331D6787950308393E14975DA762793CB49F859500F0533
            E9DAB331360FF5A060265D1AECD0FD17831B1E14CC44AF52ED4AA653DEABEC42
            C14CB438B12BB9BBBD0B3F03EAA360265D9AD9A1CBD9E833A03E0A66D2A51D81
            1941F4A160265DBA44A4603E14CCA44B2FF0D39F35FA0CA88F82994C1FA6D5E9
            DBFEE80D32D167407D14CC401BC47469BF8EF96C1E7E0ED447C10C744FD3B50C
            8F86E167413D14AC30CD48EFE216020C747850B0C2F4D0B68B592ED9E0C68182
            15D4F56DDC78E05C1E052BA48BF75D51782D6C5914AC260D0EF4ED15B3DA7B9F
            CBC53228D89EB467A03688E9EBF6D91AA8995C4D56A3635EB6570705DB817EAB
            6B19CA6CDABD3D37EA440FCDF5B9F956DB1D05DB82B600D06553F657C8EADB5A
            0321AC7EDE1E05DB402B7C3561578B11C9FB68589F4BC8CF51B037064783F589
            C30BCFB78F76A462ABED1805ABE837B12E03FB3A68D144F4FEEA8BF30BF6F778
            257DC1F4223DCDBEA058E5A26FFFEBABEBF5D54074CC33495B30DDA8F7EDF955
            DBF23228A25F62D1DF4106E90AA67B05062E9A8FEED3328E3EA629D8F864DCA9
            45907D8D8AA6BF8BE8EFA88F7A5F300D5E68C52E6957F47792E1D2B1B705D30D
            769FA732F525BA47EBF3B3B4DE154CD37934B39DE758DDC9CB60481F471D7B53
            303D7BD133983EBE903C4B54340DEFF769CE632F0A361A8C1819EC51968BE5EA
            DBF85BF877DD359D2E98BEB5F45E64EEB3FA194D00E8FA6563670BA611A82EBE
            AA95EC16DD4B6BA94C740E7441E70AA66F2D5DA7F3AD952B7A86A95B81E89C68
            B34E156C3C1AAF16F34575C849B6E897AA4688BB3499B81305E35B8BBC8E5694
            0F8FBBB1516AEB0BA64D6598E244DE46F7665D5883D6EA8269A8F6F191E75A64
            73AE27D7ADBE646C6DC1BABE8927692E5A76D4D687D3AD2B987E1B69653121BB
            448F6CDAF8CCAC5505D36F2166BE937DA369726D9BA1DF9A82A95C0C6690BAD1
            E0479BD69BB5A260948B944C9B4A76F082512EE2485B4A76D082694083721157
            5432EDCA1C9D7B4D3968C1BAFAB23AD29D68E9CB2147170F5630ED9E4B4813D1
            55D2A11E461FA4609AA1414893D18C8FE85C746BBC60832F83F58B030869329A
            28AED518D139E9D478C1B8EF22878A963A357DA9D868C1B83424878ED69345E7
            A64BA30563893F39743474DFE4C4E0C60AA6B53B84B4214D7E8B355630ED494E
            481BA23586D139EAD048C13472C8727FD2A634358DAA918269DB2D42DA14EDF1
            129DABA53552305E7447DA96F96C1E9EABA5355230B6B5266D4B53F7618D144C
            132E09695BA273B5B4460A46481BD3C4DE8A148CA44D6F0AA6077B40DB3431A3
            A3918201595130C088820146140C30A260801105038C28186044C100230A0618
            5130C088820146140C30A260801105038C28186044C100230A06185130C08882
            0146140C30A260801105038C28186044C100230A06185130C088820146140C30
            A260801105038C28186044C100230A06185130C088820146140C30A260801105
            038C28186044C100230A06185130C088820146140C30A260801105038C281860
            44C100230A06185130C088820146140C30A260801105038C28186044C1009B2F
            ABFF030A3C97A34D40BFA70000000049454E44AE426082}
          Proportional = True
          Stretch = True
          ExplicitLeft = -4
          ExplicitHeight = 208
        end
      end
      object panelTodosLosDatos: TPanel
        Left = 197
        Top = 1
        Width = 806
        Height = 261
        Align = alClient
        TabOrder = 1
        object panelRespuesta: TPanel
          Left = 1
          Top = 1
          Width = 804
          Height = 65
          Align = alTop
          Alignment = taLeftJustify
          BevelOuter = bvNone
          BorderWidth = 1
          BorderStyle = bsSingle
          Caption = ' Espere ...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -27
          Font.Name = 'Arial Black'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object panelTarjeta: TPanel
          Left = 1
          Top = 66
          Width = 804
          Height = 54
          Align = alTop
          BevelOuter = bvNone
          BorderWidth = 1
          BorderStyle = bsSingle
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial Black'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          object LblCredencial: TLabel
            Left = 253
            Top = 7
            Width = 119
            Height = 27
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Credencial:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Arial Black'
            Font.Style = []
            ParentFont = False
          end
          object edCredencial: TEdit
            Left = 378
            Top = 5
            Width = 200
            Height = 33
            CharCase = ecUpperCase
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Arial Black'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object pFechaHora: TPanel
            Left = 584
            Top = 1
            Width = 217
            Height = 50
            Align = alRight
            TabOrder = 1
            object lblHora: TLabel
              Left = 16
              Top = 11
              Width = 96
              Height = 27
              Alignment = taCenter
              Caption = '12:53 PM'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -19
              Font.Name = 'Arial Black'
              Font.Style = []
              ParentFont = False
            end
            object lblFecha: TLabel
              Left = 126
              Top = 13
              Width = 83
              Height = 23
              Alignment = taCenter
              Caption = '05/Oct/00'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -16
              Font.Name = 'Arial Black'
              Font.Style = []
              ParentFont = False
            end
          end
        end
        object panelEmpleado: TPanel
          Left = 1
          Top = 120
          Width = 804
          Height = 140
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 1
          BorderStyle = bsSingle
          Ctl3D = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Arial Black'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          object panelDatosEmpleado: TPanel
            Left = 1
            Top = 1
            Width = 800
            Height = 136
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Arial Black'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            DesignSize = (
              800
              136)
            object ImageLogo: TImage
              Left = 518
              Top = 21
              Width = 97
              Height = 101
              Anchors = [akLeft, akBottom]
              ExplicitTop = 138
            end
            object LblNombre: TLabel
              Left = 15
              Top = 18
              Width = 569
              Height = 27
              Anchors = [akLeft, akTop, akRight]
              AutoSize = False
              Caption = 'Nombre Completo'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clNavy
              Font.Height = -19
              Font.Name = 'Arial Black'
              Font.Style = []
              ParentFont = False
              WordWrap = True
              ExplicitWidth = 354
            end
            object Checada: TButton
              Left = 412
              Top = 136
              Width = 100
              Height = 25
              Caption = 'Checada'
              Default = True
              TabOrder = 0
              OnClick = ChecadaClick
              OnEnter = ChecadaEnter
            end
          end
        end
      end
    end
  end
  object VaCommA: TVaComm
    FlowControl.OutCtsFlow = False
    FlowControl.OutDsrFlow = False
    FlowControl.ControlDtr = dtrDisabled
    FlowControl.ControlRts = rtsDisabled
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    FlowControl.DsrSensitivity = False
    FlowControl.TxContinueOnXoff = False
    Options = [coNullStrip, coParityCheck]
    DeviceName = 'COM%d'
    MonitorEvents = [ceBreak, ceError, ceRxFlag]
    OnBreak = VaCommABreak
    OnError = VaCommAError
    OnTxEmpty = VaCommATxEmpty
    OnRx80Full = VaCommARx80Full
    Version = '1.8.0.0'
    Left = 368
    Top = 416
  end
  object VaCommB: TVaComm
    FlowControl.OutCtsFlow = False
    FlowControl.OutDsrFlow = False
    FlowControl.ControlDtr = dtrDisabled
    FlowControl.ControlRts = rtsDisabled
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    FlowControl.DsrSensitivity = False
    FlowControl.TxContinueOnXoff = False
    Options = [coNullStrip, coParityCheck]
    DeviceName = 'COM%d'
    MonitorEvents = [ceBreak, ceError, ceRxFlag]
    OnBreak = VaCommABreak
    OnError = VaCommAError
    OnTxEmpty = VaCommATxEmpty
    OnRx80Full = VaCommARx80Full
    Version = '1.8.0.0'
    Left = 424
    Top = 416
  end
  object TimerBanner: TTimer
    Interval = 1
    OnTimer = TimerBannerTimer
    Left = 488
    Top = 416
  end
  object DataSource: TDataSource
    Left = 720
    Top = 416
  end
  object Desconectado: TTimer
    Enabled = False
    Interval = 5000
    OnTimer = DesconectadoTimer
    Left = 557
    Top = 416
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 48
    Top = 543
    PixelsPerInch = 96
    object cxStyleStatusBarLabel: TcxStyle
      AssignedValues = [svBitmap, svColor, svFont, svTextColor]
      Color = clBlack
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowFrame
      Font.Height = -12
      Font.Name = 'Segoe UI'
      Font.Style = []
      TextColor = clWindowText
    end
    object cxStyle1: TcxStyle
      AssignedValues = [svBitmap, svColor]
      Color = clBlack
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svBitmap, svColor, svFont, svTextColor]
      Color = clActiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      TextColor = clDefault
    end
    object cxsColumnas: TcxStyle
      AssignedValues = [svColor, svFont]
      Color = clSilver
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -21
      Font.Name = 'Calibri'
      Font.Style = [fsBold]
    end
  end
  object DevEx_SkinController: TdxSkinController
    NativeStyle = False
    SkinName = 'TressMorado2013'
    Left = 48
    Top = 599
  end
end
