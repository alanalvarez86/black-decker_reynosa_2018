inherited BuscaEntero_DevEx: TBuscaEntero_DevEx
  ActiveControl = Codigo
  Caption = 'Buscar '
  ClientHeight = 100
  ClientWidth = 256
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object CodigoLBL: TLabel [0]
    Left = 45
    Top = 21
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'CodigoLBL:'
  end
  object NoExisteLBL: TLabel [1]
    Left = 168
    Top = 21
    Width = 71
    Height = 13
    Caption = #161' No Existe !'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 64
    Width = 256
    BevelOuter = bvNone
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 88
      Hint = 'Buscar Registro'
      Default = True
      ModalResult = 0
      OnClick = OKClick_DevEx
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 173
      Hint = 'Abandonar B'#250'squeda y Salir'
      Cancel = True
    end
  end
  object Codigo: TMaskEdit [3]
    Left = 104
    Top = 17
    Width = 57
    Height = 21
    EditMask = '99999;0; '
    MaxLength = 5
    TabOrder = 0
    OnChange = CodigoChange
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
