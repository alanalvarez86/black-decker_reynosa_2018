unit ZetaRegistryTerminal;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Registry,
     ZetaCommonLists;

type
  TZetaTerminalRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    FCanWrite: Boolean;
    function GetIdentificacion: String;
    procedure SetIdentificacion( const Value: String );
    function GetMostrarFoto: Boolean;
    procedure SetMostrarFoto( const Value: Boolean );
    function GetUsaTeclado: Boolean;
    procedure SetUsaTeclado( const Value: Boolean );
    function GetBanner: String;
    procedure SetBanner( const Value: String );
    function GetVelocidad: Integer;
    procedure SetVelocidad( const Value: Integer );
    function GetServidor: String;
    procedure SetServidor( const Value: String );
    function GetPuerto: Integer;
    procedure SetPuerto( const Value: Integer );
    function GetArchivo: String;
    procedure SetArchivo( const Value: String );
    function GetMensajeOK: String;
    procedure SetMensajeOK( const Value: String );
    function GetImpresora: String;
    procedure SetImpresora( const Value: String );
    function GetCodigoInicial: String;
    procedure SetCodigoInicial( const Value: String );
    function GetCodigoFinal: String;
    procedure SetCodigoFinal( const Value: String );
    function GetEject: String;
    procedure SetEject( const Value: String );
    function GetSonidoAcepta: String;
    function GetSonidoRechazo: String;
    procedure SetSonidoAcepta(const Value: String);
    procedure SetSonidoRechazo(const Value: String);
    function GetGafeteAdmon: String;
    procedure SetGafeteAdmon(const Value: String);
    function GetClaveAdmon: String;
    procedure SetClaveAdmon(const Value: String);
    function GetSerialA: Integer;
    procedure SetSerialA( const Value: Integer );
    function GetSerialB: Integer;
    procedure SetSerialB( const Value: Integer );
    function GetCodigoAcepta: String;
    procedure SetCodigoAcepta( const Value: String );
    function GetCodigoRechazo: String;
    procedure SetCodigoRechazo( const Value: String );
    function GetApagadoAcepta: String;
    procedure SetApagadoAcepta(const Value: String);
    function GetApagadoRechazo: String;
    procedure SetApagadoRechazo(const Value: String);
    function GetEsperaAcepta: integer;
    procedure SetEsperaAcepta(const Value: integer);
    function GetEsperaRechazo: integer;
    procedure SetEsperaRechazo(const Value: integer);
    function GetComEntrada: String;
    procedure SetComEntrada( const Value: String );
    function GetComSalida: String;
    procedure SetComSalida( const Value: String );
    function GetTipoGafete: eTipoGafete;
    procedure SetTipoGafete( const Value: eTipoGafete );
    function GetLectorProximidad: eTipoProximidad;
    procedure SetLectorProximidad(const Value: eTipoProximidad);
    function GetDefaultCredencial: String;
    function GetDefaultEmpresa: String;
    procedure SetDefaultCredencial(const Value: String);
    procedure SetDefaultEmpresa(const Value: String);
    function GetIntervalo: Integer;
    procedure SetIntervalo(const Value: Integer);
    function GetLimpiarPantalla: Integer;
    procedure SetLimpiarPantalla(const Value: Integer);
    //biométrico    @DACP
    function GetFileTemplates: String;
    procedure SetFileTemplates( const Value: String );
  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
  public
    { Public declarations }
    constructor Create( const sRegistryPath: String );
    destructor Destroy; override;
    property CanWrite: Boolean read FCanWrite;
    property Identificacion: String read GetIdentificacion write SetIdentificacion;
    property MostrarFoto: Boolean read GetMostrarFoto write SetMostrarFoto;
    property UsaTeclado: Boolean read GetUsaTeclado write SetUsaTeclado;
    property Banner: String read GetBanner write SetBanner;
    property Velocidad: Integer read GetVelocidad write SetVelocidad;
    property Servidor: String read GetServidor write SetServidor;
    property Puerto: Integer read GetPuerto write SetPuerto;
    property Intervalo: Integer read GetIntervalo write SetIntervalo;
    property LimpiarPantalla: Integer read GetLimpiarPantalla write SetLimpiarPantalla;
    property Archivo: String read GetArchivo write SetArchivo;
    property MensajeOk: String read GetMensajeOk write SetMensajeOk;
    property Impresora: String read GetImpresora write SetImpresora;
    property CodigoInicial: String read GetCodigoInicial write SetCodigoInicial;
    property CodigoFinal: String read GetCodigoFinal write SetCodigoFinal;
    property Eject: String read GetEject write SetEject;
    property SonidoAcepta: String read GetSonidoAcepta write SetSonidoAcepta;
    property SonidoRechazo: String read GetSonidoRechazo write SetSonidoRechazo;
    property GafeteAdmon: String read GetGafeteAdmon write SetGafeteAdmon;
    property ClaveAdmon: String read GetClaveAdmon write SetClaveAdmon;
    property SerialA: Integer read GetSerialA write SetSerialA;
    property SerialB: Integer read GetSerialB write SetSerialB;
    property CodigoAcepta: String read GetCodigoAcepta write SetCodigoAcepta;
    property CodigoRechazo: String read GetCodigoRechazo write SetCodigoRechazo;
    property ApagadoAcepta: String read GetApagadoAcepta write SetApagadoAcepta;
    property ApagadoRechazo: String read GetApagadoRechazo write SetApagadoRechazo;
    property EsperaAcepta: integer read GetEsperaAcepta write SetEsperaAcepta;
    property EsperaRechazo: integer read GetEsperaRechazo write SetEsperaRechazo;
    property ComEntrada: String read GetComEntrada write SetComEntrada;
    property ComSalida: String read GetComSalida write SetComSalida;
    property TipoGafete: eTipoGafete read GetTipoGafete write SetTipoGafete;
    property LectorProximidad: eTipoProximidad read GetLectorProximidad write SetLectorProximidad;
    property DefaultEmpresa: String read GetDefaultEmpresa write SetDefaultEmpresa;
    property DefaultCredencial: String read GetDefaultCredencial write SetDefaultCredencial;
    //Biometrico @DACP
    property FileTemplates: String read GetFileTemplates write SetFileTemplates;
  end;

implementation

uses ZetaCommonTools,
     ZetaServerTools,
     ZetaCommonClasses,
     ZetaMessages;

const
     TERMINAL_REGISTRY_SECTION = '';
     DEFAULT_CONFIG_SERIAL = '6,4,0,0,0,1,0,10';

{ ************ TZetaRegistry *********** }

constructor TZetaTerminalRegistry.Create( const sRegistryPath: String );
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          FCanWrite := OpenKey( sRegistryPath, TRUE );
          if not FCanWrite then
             OpenKeyReadOnly( sRegistryPath );
          LazyWrite := False;
     end;
end;

destructor TZetaTerminalRegistry.Destroy;
begin
     FRegistry.Free;
     inherited Destroy;
end;

function TZetaTerminalRegistry.GetIdentificacion: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'Identificacion', 'MAQ1' );
     end;
end;

procedure TZetaTerminalRegistry.SetIdentificacion(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'Identificacion' , Value );
     end;
end;

function TZetaTerminalRegistry.GetMostrarFoto: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( TERMINAL_REGISTRY_SECTION, 'MostrarFoto', TRUE );
     end;
end;

procedure TZetaTerminalRegistry.SetMostrarFoto(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( TERMINAL_REGISTRY_SECTION, 'MostrarFoto' , Value );
     end;
end;

function TZetaTerminalRegistry.GetUsaTeclado: Boolean;
begin
     with FRegistry do
     begin
          Result := ReadBool( TERMINAL_REGISTRY_SECTION, 'UsaTeclado', FALSE );
     end;
end;

procedure TZetaTerminalRegistry.SetUsaTeclado(const Value: Boolean);
begin
     with FRegistry do
     begin
          WriteBool( TERMINAL_REGISTRY_SECTION, 'UsaTeclado' , Value );
     end;
end;

function TZetaTerminalRegistry.GetBanner: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'Banner', 'Grupo Tress Internacional, S.A. de C.V.' );
     end;
end;

procedure TZetaTerminalRegistry.SetBanner(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'Banner' , Value );
     end;
end;

function TZetaTerminalRegistry.GetVelocidad: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'Velocidad', 5 );
     end;
end;

procedure TZetaTerminalRegistry.SetVelocidad(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'Velocidad' , Value );
     end;
end;

function TZetaTerminalRegistry.GetServidor: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'Servidor', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetServidor(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'Servidor' , Value );
     end;
end;

function TZetaTerminalRegistry.GetPuerto: Integer;
begin
     with FRegistry do
     begin

{$ifdef PANASONIC}
{$ifdef TRESSACCESOS}
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'Puerto', 33200 );

{$endif}
{$else}
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'Puerto', 33100 );
{$endif}
     end;
end;

procedure TZetaTerminalRegistry.SetPuerto(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'Puerto' , Value );
     end;
end;

function TZetaTerminalRegistry.GetArchivo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'Archivo', '' );
          if ( Result = '' ) then
             Result := ZetaMessages.SetFileNameDefaultPath( 'LECTURAS.DAT' );
     end;
end;

procedure TZetaTerminalRegistry.SetArchivo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'Archivo' , Value );
     end;
end;

function TZetaTerminalRegistry.GetMensajeOK: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'MensajeOk', 'ˇ Mensaje !' );
     end;
end;

procedure TZetaTerminalRegistry.SetMensajeOK(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'MensajeOk' , Value );
     end;
end;

function TZetaTerminalRegistry.GetImpresora: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'Impresora', 'LPT1' );
     end;
end;

procedure TZetaTerminalRegistry.SetImpresora(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'Impresora' , Value );
     end;
end;

function TZetaTerminalRegistry.GetIntervalo: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'Intervalo', 30 );
     end;
end;

procedure TZetaTerminalRegistry.SetIntervalo(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'Intervalo' , Value );
     end;
end;

function TZetaTerminalRegistry.GetLimpiarPantalla: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'LimpiarPantalla', 30 );
     end;
end;

procedure TZetaTerminalRegistry.SetLimpiarPantalla(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'LimpiarPantalla' , Value );
     end;
end;

function TZetaTerminalRegistry.GetCodigoInicial: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'CodigoInicial', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetCodigoInicial(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'CodigoInicial' , Value );
     end;
end;

function TZetaTerminalRegistry.GetCodigoFinal: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'CodigoFinal', '27,69' );
     end;
end;

procedure TZetaTerminalRegistry.SetCodigoFinal(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'CodigoFinal' , Value );
     end;
end;

function TZetaTerminalRegistry.GetEject: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'Eject', '12' );
     end;
end;

procedure TZetaTerminalRegistry.SetEject(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'Eject' , Value );
     end;
end;

function TZetaTerminalRegistry.GetSonidoAcepta: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'SonidoAcepta', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetSonidoAcepta(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'SonidoAcepta' , Value );
     end;
end;

function TZetaTerminalRegistry.GetSonidoRechazo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'SonidoRechazo', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetSonidoRechazo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'SonidoRechazo' , Value );
     end;
end;

function TZetaTerminalRegistry.GetGafeteAdmon: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'GafeteAdmon', 'ADMON' );
     end;
end;

procedure TZetaTerminalRegistry.SetGafeteAdmon(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'GafeteAdmon' , Value );
     end;
end;

function TZetaTerminalRegistry.GetClaveAdmon: String;
begin
     with FRegistry do
     begin
          Result := ZetaServerTools.Decrypt( ReadString( TERMINAL_REGISTRY_SECTION, 'ClaveAdmon', '' ) );
     end;
end;

procedure TZetaTerminalRegistry.SetClaveAdmon(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'ClaveAdmon' , ZetaServerTools.Encrypt( Value ) );
     end;
end;

function TZetaTerminalRegistry.GetSerialA: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'SerialA', 0 );
     end;
end;

procedure TZetaTerminalRegistry.SetSerialA(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'SerialA' , Value );
     end;
end;

function TZetaTerminalRegistry.GetSerialB: Integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'SerialB', 0 );
     end;
end;

procedure TZetaTerminalRegistry.SetSerialB(const Value: Integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'SerialB' , Value );
     end;
end;

function TZetaTerminalRegistry.GetCodigoAcepta: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'CodigoAcepta', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetCodigoAcepta(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'CodigoAcepta' , Value );
     end;
end;

function TZetaTerminalRegistry.GetCodigoRechazo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'CodigoRechazo', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetCodigoRechazo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'CodigoRechazo' , Value );
     end;
end;

function TZetaTerminalRegistry.GetComEntrada: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'ComEntrada', DEFAULT_CONFIG_SERIAL );
     end;
end;

procedure TZetaTerminalRegistry.SetComEntrada(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'ComEntrada' , Value );
     end;
end;

function TZetaTerminalRegistry.GetComSalida: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'ComSalida', DEFAULT_CONFIG_SERIAL );
     end;
end;

procedure TZetaTerminalRegistry.SetComSalida(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'ComSalida' , Value );
     end;
end;

function TZetaTerminalRegistry.GetTipoGafete: eTipoGafete;
begin
     with FRegistry do
     begin
          Result := eTipoGafete( ReadInteger( TERMINAL_REGISTRY_SECTION, 'TipoGafete', 0 ) );
     end;
end;

procedure TZetaTerminalRegistry.SetTipoGafete(const Value: eTipoGafete);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'TipoGafete', Ord( Value ) );
     end;
end;

function TZetaTerminalRegistry.GetLectorProximidad: eTipoProximidad;
begin
     with FRegistry do
     begin
          Result := eTipoProximidad( ReadInteger( TERMINAL_REGISTRY_SECTION, 'LectorProximidad', 0 ) );
     end;
end;

procedure TZetaTerminalRegistry.SetLectorProximidad( const Value: eTipoProximidad);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'LectorProximidad', Ord( Value ) );
     end;
end;

function TZetaTerminalRegistry.GetDefaultCredencial: String;
begin
     with FRegistry do
     begin
          Result := Copy( ReadString( TERMINAL_REGISTRY_SECTION, 'DefaultCredencial', '' ), 1, 1 );
     end;
end;

function TZetaTerminalRegistry.GetDefaultEmpresa: String;
begin
     with FRegistry do
     begin
          Result := Copy( ReadString( TERMINAL_REGISTRY_SECTION, 'DefaultEmpresa', '' ), 1, 1 );
     end;
end;

procedure TZetaTerminalRegistry.SetDefaultCredencial(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'DefaultCredencial', Value );
     end;
end;

procedure TZetaTerminalRegistry.SetDefaultEmpresa(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'DefaultEmpresa', Value );
     end;
end;

function TZetaTerminalRegistry.GetApagadoAcepta: String;
begin
     with FRegistry do
     begin
     Result := ReadString( TERMINAL_REGISTRY_SECTION, 'ApagadoAcepta', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetApagadoAcepta(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'ApagadoAcepta' , Value );
     end;
end;

function TZetaTerminalRegistry.GetApagadoRechazo: String;
begin
     with FRegistry do
     begin
          Result := ReadString( TERMINAL_REGISTRY_SECTION, 'ApagadoRechazo', '' );
     end;
end;

procedure TZetaTerminalRegistry.SetApagadoRechazo(const Value: String);
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'ApagadoRechazo' , Value );
     end;
end;

function TZetaTerminalRegistry.GetEsperaAcepta: integer;
begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'EsperaAcepta', 0 );
     end;
end;

procedure TZetaTerminalRegistry.SetEsperaAcepta(const Value: integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'EsperaAcepta' , Value );
     end;
end;

function TZetaTerminalRegistry.GetEsperaRechazo: integer;

begin
     with FRegistry do
     begin
          Result := ReadInteger( TERMINAL_REGISTRY_SECTION, 'EsperaRechazo', 0 );
     end;
end;

procedure TZetaTerminalRegistry.SetEsperaRechazo(const Value: integer);
begin
     with FRegistry do
     begin
          WriteInteger( TERMINAL_REGISTRY_SECTION, 'EsperaRechazo' , Value );
     end;
end;
 //BIOMETRICO @DACP
function TZetaTerminalRegistry.GetFileTemplates: String;
var
   sRes : String;
begin
     with FRegistry do
     begin
          sRes := ReadString( TERMINAL_REGISTRY_SECTION, 'FileTemplates', VACIO );
     end;
     Result := sRes;
end;

procedure TZetaTerminalRegistry.SetFileTemplates( const Value: String );
begin
     with FRegistry do
     begin
          WriteString( TERMINAL_REGISTRY_SECTION, 'FileTemplates', Value );
     end;
end;

end.
