unit FEscogeGrupoEmpresa;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, ImgList,
     ZBaseDlgModal,
     DBaseSistema;

type
  TEscogerGrupoEmpresa = class(TZetaDlgModal)
    TreeView: TTreeView;
    ImageList: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TreeViewDblClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FGroup: Integer;
    FGroupName: String;
    FCompany: String;
    FCompanyName: String;
    FLista: TGruposEmpresas;
  public
    { Public declarations }
    property Group: Integer read FGroup;
    property GroupName: String read FGroupName;
    property Company: String read FCompany;
    property CompanyName: String read FCompanyName;
    function Cargar: Boolean;
  end;

var
  EscogerGrupoEmpresa: TEscogerGrupoEmpresa;

function SeleccionaGrupoEmpresa( var iGrupo: Integer; var sEmpresa: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     DSistema;

const
{$ifdef DOS_CAPAS}
     K_TEXT_IMPORTA_DERECHOS = '�Desea importar los Derechos de Acceso de la Empresa' + CR_LF + '%s' + CR_LF + 'del Grupo %s?';
{$else}
     K_TEXT_IMPORTA_DERECHOS = '�Desea importar los Derechos de Acceso de la Empresa' + CR_LF + '%s' + CR_LF + 'del Grupo %s?' + CR_LF + 'S�lo se copiar�n los Derechos de la Pesta�a Seleccionada.';
{$endif}

{$R *.DFM}

function SeleccionaGrupoEmpresa( var iGrupo: Integer; var sEmpresa: String ): Boolean;
begin
     with TEscogerGrupoEmpresa.Create( Application ) do
     begin
          try
             if Cargar then
             begin
                  ShowModal;
                  Result := ( ModalResult = mrOk );
                  if Result then
                  begin
                       if ZetaDialogo.zConfirm( Caption, Format( K_TEXT_IMPORTA_DERECHOS, [ CompanyName, GroupName ] ), 0, mbNo ) then
                       begin
                            iGrupo := Group;
                            sEmpresa := Company;
                       end
                       else
                           Result := False;
                  end;
             end
             else
                 Result := False;
          finally
                 Free;
          end;
     end;
end;

{ ************* TEscogerGrupoEmpresa ********* }

procedure TEscogerGrupoEmpresa.FormCreate(Sender: TObject);
begin
     inherited;
     FLista := TGruposEmpresas.Create;
end;

procedure TEscogerGrupoEmpresa.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FLista );
     inherited;
end;

function TEscogerGrupoEmpresa.Cargar: Boolean;
var
   i, iGrupo, iDefault: Integer;
   Padre, MiDefault: TTreeNode;
   oCursor: TCursor;
begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FGroup := -1;
        FCompany := '';
        if dmSistema.CargarGruposAccesos( FLista, iDefault ) then
        begin
             Result := True;
             iGrupo := -1;
             Padre := nil;
             MiDefault := nil;
             TreeView.Items.Clear;
             with FLista do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       if ( iGrupo <> Items[ i ].Grupo ) then
                       begin
                            Padre := TreeView.Items.Add( Padre, Items[ i ].GetGroupDescription ); { Add a root node }
                            iGrupo := Items[ i ].Grupo;
                            if ( iGrupo = iDefault ) then
                               MiDefault := Padre;
                       end;
                       with TreeView.Items.AddChildObject( Padre, Items[ i ].GetEmpresaDescription, Items[ i ] ) do { Add a Child node }
                       begin
                            ImageIndex := 1;
                            SelectedIndex := 1;
                       end;
                  end;
             end;
             with TreeView do
             begin
                  if not Assigned( MiDefault ) then
                     MiDefault := Items.Item[ 0 ];
                  FullCollapse;
                  MiDefault.Expand( False );
                  Selected := MiDefault;
                  TopItem := MiDefault;
             end;
        end
        else
            ZetaDialogo.zError( Caption, 'No Hay Otro Grupo Con Derechos De Acceso', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEscogerGrupoEmpresa.TreeViewDblClick(Sender: TObject);
begin
     inherited;
     OK.Click;
end;

procedure TEscogerGrupoEmpresa.OKClick(Sender: TObject);
begin
     inherited;
     with TreeView do
     begin
          if Assigned( Selected ) then
          begin
               if Assigned( Selected.Data ) then
               begin
                    with TGrupoEmpresa( Selected.Data ) do
                    begin
                         Self.FGroup := Grupo;
                         Self.FGroupName := GrupoName;
                         Self.FCompany := Empresa;
                         Self.FCompanyName := EmpresaName;
                    end;
                    ModalResult := mrOk;
               end
               else
               begin
                    Beep;
                    ActiveControl := TreeView;
               end;
          end
          else
          begin
               Beep;
               ActiveControl := TreeView;
          end;
     end;
end;

end.
