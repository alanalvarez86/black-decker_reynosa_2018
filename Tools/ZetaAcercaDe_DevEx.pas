unit ZetaAcercaDe_DevEx;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
     Buttons, ExtCtrls, ComCtrls, Checklst, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxPC, cxContainer, cxEdit, cxCheckListBox,
  dxGDIPlusClasses, cxImage, dxBarBuiltInMenu;

type
  TZAcercaDe_DevEx = class(TForm)
    PageControl: TcxPageControl;
    PageVersion: TcxTabSheet;
    PageAutorizacion: TcxTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    ModulosGB: TGroupBox;
    Modulos: TCheckListBox;
    Caducidad: TGroupBox;
    Prestamos: TCheckListBox;
    SerialNumberLBL: TLabel;
    SerialNumber: TLabel;
    EmpresaLBL: TLabel;
    Empresa: TLabel;
    EmpleadosLBL: TLabel;
    Empleados: TLabel;
    VersionLBL: TLabel;
    Version: TLabel;
    VencimientoLBL: TLabel;
    Vencimiento: TLabel;
    UsuariosLBL: TLabel;
    Usuarios: TLabel;
    PanelVersion: TPanel;
    VersionBuild: TLabel;
    Label1: TLabel;
    Copyright: TLabel;
    lbServicePackDatos: TLabel;
    SQLEngineLBL: TLabel;
    SQLEngine: TLabel;
    LblFileVersion: TLabel;
    OK_DevEx: TcxButton;
    Prestamos_DevEx: TcxCheckListBox;
    Modulos_DevEx: TcxCheckListBox;
    ProgramIcon: TcxImage;
    lblPlataforma: TLabel;
    Label6: TLabel;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure LlenaPageAutorizacion;
    procedure llenaChecks_DevEx;
  public
    { Public declarations }
  end;

var
  ZAcercaDe_DevEx: TZAcercaDe_DevEx;

implementation

{$R *.DFM}

uses ZetaCommonTools,
     ZetaCommonClasses,
     ZetaClientTools,
     ZGlobalTress,
     FAutoClasses,
     DGlobal;

function BytesAMegas( const Bytes: Integer ): Real;
begin
     Result := Bytes / ( 1024 * 1024 );
end;

procedure TZAcercaDe_DevEx.FormCreate(Sender: TObject);
const
     {$ifdef DOS_CAPAS}
     //K_MENSAJE ='Acerca De %s Profesional';
     {$else}
     //K_MENSAJE ='Acerca De %s Corporativa';
     {$endif}
     //DevEx(by am): Se requirio que solo apareciera el nombre de la aplicacion.
     K_MENSAJE = 'Acerca de %s';
var
   sVersion: String;
   {$ifndef VALIDADORDIMM}
   sDatos: String;
   {$endif}
begin
     Caption := Format( K_MENSAJE, [ Application.Title ] );
     sVersion :=  'Versi�n ' + GetApplicationProductVersionSimple; //GetApplicationProductVersion;
{$ifdef HAY_AUTORIZACION}
     if Autorizacion.EsDemo then
        sVersion := sVersion + ' ( DEMO )';
{$endif}
     VersionBuild.Caption := sVersion;
     LblFileVersion.Caption := GetApplicationVersionBuild;
     LlenaPageAutorizacion;
     HelpContext := H00022_Acerca_de_Tress;
     PageControl.ActivePage := PageVersion;
     //{$ifdef RDDAPP}
     //{$else}
     {$ifdef VALIDADORDIMM}
     {$else}
     sDatos := Global.GetGlobalString(K_GLOBAL_VERSION_DATOS);
     {$endif}
     //{$endif}

     {$ifdef VALIDADORDIMM}
     lbServicePackDatos.Caption := ZetaCommonClasses.VACIO;
     {$else}
     if Length(sDatos) = 0 then
        sDatos := '<Indefinida>';
     lbServicePackDatos.Caption := 'Versi�n Datos: ' + sDatos;
     {$endif}

     {$ifdef WORKFLOWCFG}
     lbServicePackDatos.Visible := False;
     PanelVersion.Height := 79;
     Label1.Top := 40;
     Copyright.Top := 56
     {$endif}

     {@(am): La descripcion en "Derechos reservados" era estatica. Esto se cambia
     para que al igual que la etiqueta de "Version <a�o>" sea dinamica, y no sea necesario cambiarla manualmente
     cada a�o.}
     Copyright.Caption :=  'Derechos Reservados 1996-'+GetApplicationProductVersionSimple;
end;

procedure TZAcercaDe_DevEx.OKClick(Sender: TObject);
begin
     Close;
end;

procedure TZAcercaDe_DevEx.LlenaPageAutorizacion;
begin
     with Autorizacion do
     begin
          if EsDemo then
          begin
               Self.SerialNumber.Caption := 'DEMO';
               Self.Empresa.Caption := 'DEMO';
          end
          else
          begin
               Self.SerialNumber.Caption := IntToStr( NumeroSerie );
               Self.Empresa.Caption := IntToStr( Empresa );
          end;
          Self.Version.Caption := {$ifdef TRESS_DELPHIXE5_UP}String( Version ){$else}Version{$endif};
          Self.Empleados.Caption := GetEmpleadosStr;
          Self.Vencimiento.Caption := GetVencimientoStr;
          Self.Caducidad.Caption := ' Pr�stamos: ' + GetCaducidadStr + ' ';
          Self.Usuarios.Caption := IntToStr( Usuarios );
          Self.SQLEngine.Caption := GetSQLEngineStr;
          Self.lblPlataforma.Caption := GetPlataformaStr;
          GetModulos( Self.Modulos );
          GetPrestamos( Self.Prestamos );
          llenaChecks_DevEx;
     end;
end;
procedure TZAcercaDe_DevEx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
     case key of
          VK_ESCAPE: ModalResult := mrCancel;
     end;
end;

procedure TZAcercaDe_DevEx.llenaChecks_DevEx;
var
Indice : integer;
begin
  For indice  :=0 to Modulos.Items.Count -1 do
  begin
          with Modulos_DevEx.Items.Add do
          begin
               Text := Modulos.Items[indice];
               Checked := Modulos.Checked[indice];
               Enabled := false;
          end;
     end;

     for indice := 0 to Prestamos.Count - 1  do
     begin
          with Prestamos_DevEx.Items.Add do
          begin
               Text := Prestamos.Items[indice];
               Checked := Prestamos.Checked[indice];
               Enabled := false;
          end;
     end;


end;
procedure TZAcercaDe_DevEx.OK_DevExClick(Sender: TObject);
begin
     Close;
end;

end.

