unit ZetaBuscaEmpleado_DevExTimbradoAvanzada;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, Grids, DBGrids, StrUtils,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013,
  dxSkinsDefaultPainters,  cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxButtons, ExtCtrls, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZetaMessages,
     ZetaDBGrid,
     ZetaClientDataset, cxLabel, cxContainer, cxCheckBox;

type
  TBuscaEmpleado_DevExTimbradoAvanzada = class(TForm)
    PanelControles: TPanel;
    BuscarLBL: TcxLabel;
    BuscarEdit: TEdit;
    DataSource: TDataSource;
    MostrarBajas: TcxCheckBox;
    BuscarBtn_DevEx: TcxButton;
    GridEmpleados_DevEx: TZetaCXGrid;
    GridEmpleados_DevExDBTableView: TcxGridDBTableView;
    GridEmpleados_DevExLevel: TcxGridLevel;
    CB_CODIGO: TcxGridDBColumn;
    CB_APE_PAT: TcxGridDBColumn;
    CB_NOMBRES: TcxGridDBColumn;
    CB_RFC: TcxGridDBColumn;
    CB_SEGSOC: TcxGridDBColumn;
    CB_APE_MAT: TcxGridDBColumn;
    CB_BAN_ELE: TcxGridDBColumn;
    CB_ACTIVO: TcxGridDBColumn;
    STATUS: TcxGridDBColumn;
    BtnBusquedaAvanzada: TcxButton;
    EjemploLbl: TcxLabel;
    TABLA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BuscarEditChange(Sender: TObject);
    procedure MostrarBajasClick(Sender: TObject);
    procedure FiltraTextoEditChange(Sender: TObject);
    procedure BuscarBtn_DevExClick(Sender: TObject);
    procedure GridEmpleados_DevExDBTableViewDblClick(Sender: TObject);
    procedure GridEmpleados_DevExDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure GridEmpleados_DevExDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure CB_CODIGOCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_APE_PATCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_APE_MATCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_NOMBRESCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_RFCCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CB_SEGSOCCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure BtnBusquedaAvanzadaClick(Sender: TObject);
    procedure GridEmpleados_DevExDBTableViewKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    {procedure CB_BAN_ELECustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);    }
  private
    { Private declarations }
    FFiltro: String;
    FNumeroEmpleado: Integer;
    FNombreEmpleado: String;
    FCapturando: Boolean;
    FLookupDataSet: TZetaClientDataset;
    FEmpresaActual: OleVariant;
    sApellidoPaterno: String;
    sApellidoMaterno: String;
    sNombre: String;
    procedure Buscar(EsBusquedaAvanzada: Boolean);
    procedure SetNumero;
    procedure SetBotones( const Vacio : Boolean );
    procedure ShowGrid( const lVisible: Boolean );
    procedure Connect;
    procedure Disconnect;
    procedure RemoveFilter;
    procedure SetFilter;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    function AsignaValoresBusquedaAvanzada: Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property NumeroEmpleado: Integer read FNumeroEmpleado;
    property NombreEmpleado: String read FNombreEmpleado;
    property Filtro: String read FFiltro write FFiltro;
    property LookupDataSet: TZetaClientDataset read FLookupDataSet write FLookupDataSet;
    property EmpresaActual: OleVariant read FEmpresaActual write FEmpresaActual;
  end;

procedure SetOnlyActivos( const lActivos: Boolean );

var
  BuscaEmpleado_DevExTimbradoAvanzada: TBuscaEmpleado_DevExTimbradoAvanzada;

function BuscaEmpleadoDialogoDataSet( const sFilter: String; var sKey, sDescription: String; const oDataSet: TZetaClientDataset; oEmpresa: OleVariant ): Boolean;
function BuscaEmpleadoDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientTools,
     DGlobal,
     ZGlobalTress,
     DCliente,
     ZFiltroSQLTools,
     ZetaBuscaEmpleadoNombre_DevEx, DInterfase;

const
     CaracterSeparador = '+';
var
   FShowOnlyActivos: Boolean;

{$R *.DFM}

procedure SetOnlyActivos( const lActivos: Boolean );
begin
     FShowOnlyActivos := lActivos;
end;



function BuscaEmpleadoDialogoDataSet( const sFilter: String; var sKey, sDescription: String; const oDataSet: TZetaClientDataset; oEmpresa: OleVariant ): Boolean;
begin
     Result := False;
     if ( BuscaEmpleado_DevExTimbradoAvanzada = nil ) then
        BuscaEmpleado_DevExTimbradoAvanzada := TBuscaEmpleado_DevExTimbradoAvanzada.Create( Application );
     if ( BuscaEmpleado_DevExTimbradoAvanzada <> nil ) then
     begin
          with BuscaEmpleado_DevExTimbradoAvanzada do
          begin
               LookupDataset := oDataSet;
               EmpresaActual := oEmpresa;
               Filtro := sFilter;
               ShowModal;
               if ( ModalResult = mrOk ) and ( NumeroEmpleado <> 0 ) then
               begin
                    sKey := IntToStr( NumeroEmpleado );
                    sDescription := NombreEmpleado;
                    Result := True;
               end;
          end;
     end;
end;

function BuscaEmpleadoDialogo( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     Result := BuscaEmpleadoDialogoDataSet( sFilter, sKey, sDescription, Dminterfase.cdsEmpleadoLookupTimbrado, dmCliente.Empresa );
end;

{************** TBuscaEmpleado ************** }

procedure TBuscaEmpleado_DevExTimbradoAvanzada.FormCreate(Sender: TObject);
begin
     HelpContext := H00012_Busqueda_empleados;
     sApellidoPaterno := VACIO;
     sApellidoMaterno := VACIO;
     sNombre := VACIO;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
       inherited;
       case key of
            VK_ESCAPE: ModalResult := mrCancel;
       end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.FormShow(Sender: TObject);
begin
     WindowState := wsNormal;
     ActiveControl := BuscarEdit;
     Connect;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
     Action := caHide;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.SetFilter;
var
   sStatusFiltro :string;
begin
     sStatusFiltro := VACIO;

     if LookupDataset.FindField('STATUS') <> NIL then
     begin
          if MostrarBajas.Checked then
          begin
               sStatusFiltro := Format( 'or ( STATUS <> %d )',[ Ord( steBaja ) ] );
          end
          else
          begin
              sStatusFiltro := Format( 'or ( STATUS <> %d and TABLA = %s)',[ Ord( steBaja ),EntreComillas('COLABORA') ] );
          end;
     end;
     with LookupDataset do
     begin
          DisableControls;
          try
             Filtered := False;
             if MostrarBajas.Checked then
             begin
                  Filter := Filtro;
             end
             else
                 Filter := ZetaCommonTools.ConcatFiltros( Filtro, Format('( CB_ACTIVO = %s and TABLA = %s) %s',[EntreComillas(K_GLOBAL_SI),EntreComillas('COLABORA'),sStatusFiltro ] ) );
             Filtered := True;
          finally
                 EnableControls;
          end;
     end;
end;



procedure TBuscaEmpleado_DevExTimbradoAvanzada.RemoveFilter;
begin
     with LookupDataset do
     begin
          DisableControls;
          try
             if Filtered then
             begin
                  Filtered := False;
                  Filter := VACIO;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.Connect;
begin
     SetFilter;
     ShowGrid( False );
     SetBotones( True );
     ActiveControl := BuscarEdit;
     with MostrarBajas do
     begin
          if FShowOnlyActivos then
          begin
               Checked := False;
               Enabled := False;
               Visible := False;
          end
          else
          begin
               Enabled := True;
               Visible := True;
          end;
     end;
     BuscarEdit.Clear;
     //Para borrar los filtros si es que se hizo alguno
     GridEmpleados_DevExDBTableView.DataController.Filter.Root.Clear;
     Datasource.Dataset := LookupDataset;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.Disconnect;
begin
     Datasource.Dataset := nil;
     with LookupDataset do
     begin
          if Filtered then
          begin
               RemoveFilter;
          end;
     end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.BtnBusquedaAvanzadaClick(Sender: TObject);
begin
     if ( BuscaEmpleadoNombre = nil ) then
        BuscaEmpleadoNombre := TBuscaEmpleadoNombre.Create( Application );
     if ( BuscaEmpleadoNombre <> nil ) then
     begin
          with BuscaEmpleadoNombre do
          begin
               if ( BuscarEdit.Text <> VACIO ) then
               begin
                    AsignaValoresBusquedaAvanzada;
                    BuscaEmpleadoNombre.NombresEdit.Text := sNombre;
                    BuscaEmpleadoNombre.ApellidoPaternoEdit.Text := sApellidoPaterno;
                    BuscaEmpleadoNombre.ApellidoMaternoEdit.Text := sApellidoMaterno;
               end
               else
               begin
                    BuscaEmpleadoNombre.NombresEdit.Text := VACIO;
                    BuscaEmpleadoNombre.ApellidoPaternoEdit.Text := VACIO;
                    BuscaEmpleadoNombre.ApellidoMaternoEdit.Text := VACIO;
               end;
               ShowModal;
               if ( ModalResult = mrOk ) then
               begin
                    BuscarEdit.Text := BuscaEmpleadoNombre.NombresEdit.Text + CaracterSeparador +
                                       BuscaEmpleadoNombre.ApellidoPaternoEdit.Text + CaracterSeparador +
                                       BuscaEmpleadoNombre.ApellidoMaternoEdit.Text;
                    Buscar( AsignaValoresBusquedaAvanzada );
               end;
          end;
     end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.Buscar(EsBusquedaAvanzada: Boolean);
var
   oCursor: TCursor;
begin
     Application.ProcessMessages;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourglass;
        with LookupDataset do
        begin
             DisableControls;
             try
                Active := False;
                if ( not esBusquedaAvanzada ) then
                   dminterfase.GetEmpleadosBuscados_DevExTimbrado(BuscarEdit.Text,EmpresaActual,LookUpDataSet)
                else
                    //dminterfase.GetEmpleadosBuscados_DevExTimbrado( sApellidoPaterno, sApellidoMaterno, sNombre, VACIO, VACIO, VACIO ,EmpresaActual,LookUpDataSet);
                    dminterfase.GetEmpleadosBuscados_DevExTimbradoAvanzada( sApellidoPaterno, sApellidoMaterno, sNombre, VACIO, VACIO, VACIO ,EmpresaActual,LookUpDataSet);
                    //dminterfase.GetEmpleadosBuscados_DevExTimbrado(BuscarEdit.Text,EmpresaActual,LookUpDataSet);
                Active := True;
             finally
                    EnableControls;
             end;
             if IsEmpty then
             begin
                  zWarning( 'B�squeda de Empleados', '� No hay empleados con estos datos !', 0, mbOK );
                  SetBotones( True );
                  ActiveControl := BuscarEdit;
             end
             else
             begin
                  SetFilter;
                  ShowGrid( True );
                  SetBotones( False );
                  ActiveControl := GridEmpleados_DevEx;
             end;
             GridEmpleados_DevExDBTableView.ApplyBestFit();
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.SetNumero;
begin
     with LookupDataset do
     begin
          FNumeroEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          FNombreEmpleado := FieldByName( 'PrettyName' ).AsString;
     end;
     ModalResult := mrOK;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.SetBotones( const Vacio: Boolean );
begin
     FCapturando := Vacio;
     BuscarBtn_DevEx.Default := Vacio;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.ShowGrid( const lVisible: Boolean );
const
     GRID_HEIGHT = 200;
begin
      with GridEmpleados_DevEx do
     begin
          if lVisible then
          begin
               Visible:= True;
               Height := ZetaClientTools.GetScaledHeight( GRID_HEIGHT );
               Self.ClientHeight := ZetaClientTools.GetScaledHeight( PanelControles.Height + GRID_HEIGHT );
          end
          else
          begin
               Visible:= False;
               Height := 0;
               Self.ClientHeight := ZetaClientTools.GetScaledHeight( PanelControles.Height );
          end;
     end;
     //Repaint;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.WMExaminar(var Message: TMessage);
begin
     //AceptarBtn.Click;
      if not LookupDataset.IsEmpty then
        SetNumero;
end;

{procedure TBuscaEmpleado_DevEx.BuscarBtnClick(Sender: TObject);
begin
     Buscar;
end;}

procedure TBuscaEmpleado_DevExTimbradoAvanzada.BuscarEditChange(Sender: TObject);
begin
     if not FCapturando then
        SetBotones( True );
        FiltraTextoEditChange( Sender );
end;

{procedure TBuscaEmpleado_DevEx.GridEmpleadosDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     with GridEmpleados do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    if(  zStrToBool( LookupDataset.FieldByName( 'CB_ACTIVO' ).AsString ) or  GetStatusAct )then
                       Font.Color := GridEmpleados.Font.Color
                    else
                        Font.Color := clRed;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end; }

procedure TBuscaEmpleado_DevExTimbradoAvanzada.MostrarBajasClick(Sender: TObject);
begin
     SetFilter;
     GridEmpleados_DevExDBTableView.ApplyBestFit();
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.FiltraTextoEditChange(Sender: TObject);
var
   OldChange: TNotifyEvent;
   OldStart : Integer;
   CaracterAValidar: String;
   TextoTmp: String;
   TextoOriginal: String;
begin
    with (Sender as TEdit ) do
    begin
         OldChange := OnChange;
         OnChange  := nil;
         OldStart  := SelStart;
         //Text := FiltrarTextoSQL( Text );
         //filtrar solo el ultimo caracter agregado a la cadena
         if ( Length( Text )  > 0 ) then
         begin
               TextoOriginal := Text;
         CaracterAValidar := Copy( Text, Length(TextoOriginal), 1 );
         TextoTmp := Copy( Text, 1 , (Length(TextoOriginal)-1) );
         if  not ( CaracterAValidar = CaracterSeparador ) then
            Text := TextoTmp + FiltrarTextoSQL( CaracterAValidar );
         end;
         OnChange := OldChange;
         SelStart := OldStart;
    end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.BuscarBtn_DevExClick(Sender: TObject);
begin
      Buscar(AsignaValoresBusquedaAvanzada);
end;


function TBuscaEmpleado_DevExTimbradoAvanzada.AsignaValoresBusquedaAvanzada: Boolean;
var
   TextoPista :String;
   iPosicion: Integer;
begin
     Result := False;
     TextoPista := BuscarEdit.Text;
     sNombre := VACIO;
     sApellidoPaterno := VACIO;
     sApellidoMaterno := VACIO;
     //buscar el caracterseparador en la pista
     //primer caracter separador
     if ( AnsiContainsStr( TextoPista, CaracterSeparador) ) then
     begin
          Result := True;
          iPosicion := AnsiPos( CaracterSeparador, TextoPista );
          sNombre := Copy( TextoPista, 1, iPosicion-1 );
          TextoPista := Copy( TextoPista,  iPosicion + 1 , Length( TextoPista ));
          //segundo caracter separador
          if ( AnsiContainsStr( TextoPista, CaracterSeparador) ) then
          begin
               iPosicion := AnsiPos( CaracterSeparador, TextoPista );
               sApellidoPaterno := Copy( TextoPista, 1, iPosicion-1 );
               sApellidoMaterno := Copy( TextoPista,  iPosicion+1, Length( TextoPista ));
          end
          else
              sApellidoPaterno := TextoPista;
     end;
     sNombre := StringReplace(sNombre, CaracterSeparador, VACIO, [rfReplaceAll, rfIgnoreCase]);
     sApellidoPaterno := StringReplace(sApellidoPaterno, CaracterSeparador, VACIO, [rfReplaceAll, rfIgnoreCase]);
     sApellidoMaterno := StringReplace(sApellidoMaterno, CaracterSeparador, VACIO, [rfReplaceAll, rfIgnoreCase]);
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.GridEmpleados_DevExDBTableViewDblClick( Sender: TObject);
begin
     if not LookupDataset.IsEmpty then
        SetNumero;
end;


procedure TBuscaEmpleado_DevExTimbradoAvanzada.GridEmpleados_DevExDBTableViewKeyPress(
  Sender: TObject; var Key: Char);
begin
     if ( Key = Chr( VK_RETURN ) ) then
     begin
          if not LookupDataset.IsEmpty then
             SetNumero;
     end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.GridEmpleados_DevExDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

//DevEx: Se pone en rojo el color de los registros que son Baja
procedure TBuscaEmpleado_DevExTimbradoAvanzada.GridEmpleados_DevExDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
const
     K_COL_STATUS_DB=7;
     K_COL_STATUS=8;
var
     sStatusDB:String;
     lStatus: boolean;
     Origen: string;
begin

   sStatusDB := AViewInfo.GridRecord.DisplayTexts [CB_ACTIVO.Index];
   Origen := AviewInfo.GridRecord.DisplayTexts[TABLA.Index] ;
   lStatus := FALSE;
   if AViewInfo.GridRecord.DisplayTexts[STATUS.Index] <> VACIO then
      lStatus := StrToInt(AViewInfo.GridRecord.DisplayTexts [STATUS.Index]) <> Ord(steBaja);

   with ACanvas do
   begin
       // if (  zStrToBool( LookupDataset.FieldByName( 'CB_ACTIVO' ).AsString ) or  GetStatusAct )then
       if (  zStrToBool( sStatusDB )  or  lStatus )then
             if (Origen = 'COLABORA' ) then
             begin
                  Font.Color := GridEmpleados_DevEx.Font.Color;
             end
             else
             begin
                  Font.Color := clGray;
             end
        else
            Font.Color := clRed;
   end;
end;

//DevEx: Pone el bold el font de las celdas donde se encontro la palabra
procedure TBuscaEmpleado_DevExTimbradoAvanzada.CB_CODIGOCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.CB_APE_PATCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) or AnsiContainsStr ( sTexto, sApellidoPaterno ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.CB_APE_MATCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) or AnsiContainsStr ( sTexto, sApellidoMaterno ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.CB_NOMBRESCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) or AnsiContainsStr ( sTexto, sNombre )  then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.CB_RFCCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

procedure TBuscaEmpleado_DevExTimbradoAvanzada.CB_SEGSOCCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;

{***(@am): Se comenta este evento para dar reveersa a que se muestra la Banca Electronica en la busqueda de empleados***}
{procedure TBuscaEmpleado_DevEx.CB_BAN_ELECustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTexto: String;
begin
     sTexto:= AnsiUpperCase ( AViewInfo.Text );
     if AnsiContainsStr ( sTexto, BuscarEdit.Text ) then
          with ACanvas do
          begin
               Font.Style:= [fsUnderline];
               Brush.Color := RGB(245,208,169);
          end;
end;   }
end.
