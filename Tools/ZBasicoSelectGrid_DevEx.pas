unit ZBasicoSelectGrid_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, Db,     
     ZetaMessages,
     ZetaClientDataset,
     ZetaCommonClasses,
     ZetaDBGrid, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, ImgList,
  cxButtons, dxSkinsDefaultPainters;

type
  TBasicoGridSelect_DevEx = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    Excluir: TcxButton;
    Revertir: TcxButton;
    Imprimir: TcxButton;
    Exportar: TcxButton;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    procedure FormShow(Sender: TObject);
    procedure ExcluirClick(Sender: TObject);
    procedure ZetaDBGridorigCellClick(Column: TColumn);
    procedure ImprimirClick(Sender: TObject);
    procedure RevertirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ExportarClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCellDblClick(
      Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
  procedure ApplyMinWidth; Dynamic;
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
    FFiltro : String;
    FHayFiltro : Boolean;
    function GetDataset: TZetaClientDataset;
    procedure Exclude;
    procedure SetDataset( Value: TZetaClientDataset );
    //procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;

  protected
    { Protected declarations }
    AColumn: TcxGridDBColumn;
    procedure SetControls;
  public
    { Public declarations }
    property Dataset: TZetaClientDataset read GetDataset write SetDataset;
    property Filtro: String read FFiltro write FFiltro;
    property HayFiltro: Boolean read FHayFiltro write FHayFiltro;
  end;
  TBasicoGridSelectClass = class of TBasicoGridSelect_DevEx;

var
  BasicoGridSelect_DevEx: TBasicoGridSelect_DevEx;
  ParametrosGrid: TzetaParams;

function GridSelectBasico( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                           ValidarCambios: Boolean = TRUE ): Boolean;

implementation

uses ZetaDialogo,
     ZetaCommonLists,
     //(@am): Se migro la funcionalidad de impresion y exportacion de grids a la unidad ZImprimeGrid para la compilacion de XE5.
     {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes_DevEx{$ENDIF};

{$R *.DFM}

function GridSelectBasico( ZetaDataset: TZetaClientDataset; GridSelectClass: TBasicoGridSelectClass;
                           ValidarCambios: Boolean = TRUE ): Boolean;
begin
     if ZetaDataset.IsEmpty then
     begin
          ZetaDialogo.zInformation( '� Atenci�n !', 'La Lista A Verificar Est� Vac�a', 0 );
          Result := False;
     end
     else
     begin
          with GridSelectClass.Create( Application ) as TBasicoGridSelect_DevEx do
          begin
               try
                  Dataset := ZetaDataset;
                  try
                     with Dataset do
                     begin
                          HayFiltro := Filtered;
                          Filtro := Filter;
                     end;
                     ShowModal;
                  finally
                         with Dataset do
                         begin
                              if ( Filter <> Filtro ) or ( HayFiltro <> Filtered ) then
                              begin
                                   Filter := Filtro;
                                   Filtered := HayFiltro;
                              end;
                         end
                  end;
{
                  ShowModal;
}
                  Result := ( ModalResult = mrOk ) and ( ( not ValidarCambios ) or ( ZetaDataset.ChangeCount > 0 ) );
                  Dataset := nil;
               finally
                      Free;
               end;
          end;
     end;
end;

{ *********** TBaseGridSelect *********** }

procedure TBasicoGridSelect_DevEx.FormShow(Sender: TObject);
var I:integer;
begin
     inherited;
     SetControls;
     {***(am):Trabaja CON GridMode***}
     if ZetaDBGridDBTableView.DataController.DataModeController.GridMode then
        ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
     {***}
     ZetaDbGridDBTableView.OptionsCustomize.ColumnHorzSizing := True;
     for I:=0 to ZetadbGridDbtableview.ColumnCount -1 do
        begin
        if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='CB_CODIGO')
        then
        begin
          zetadbgriddbtableview.Columns[i].Options.HorzSizing := false;
          zetadbgriddbtableview.Columns[i].Caption:='C�digo' ;

        end
        else
        if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='PrettyName') or (zetadbgriddbtableview.Columns[i].DataBinding.FieldName='PRETTYNAME') then
        begin
            zetadbgriddbtableview.Columns[i].Caption:='Nombre';
            zetadbgriddbtableview.Columns[i].Options.HorzSizing := true;
            zetadbgriddbtableview.Columns[i].MinWidth := 150;
        end
        else
        begin
           zetadbgriddbtableview.Columns[i].Options.HorzSizing := false;
        end;
     end;
     ApplyMinWidth ;
end;

function TBasicoGridSelect_DevEx.GetDataset: TZetaClientDataset;
begin
     Result := FDataset;
end;

procedure TBasicoGridSelect_DevEx.SetDataset( Value: TZetaClientDataset );
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
          if ( Value <> nil ) then
             Datasource.Dataset := Value;
     end;
end;

procedure TBasicoGridSelect_DevEx.SetControls;
begin
     //Excluir.Enabled := ( ZetaDBGrid.SelectedRows.Count > 0 );
     Revertir.Enabled := ( FDataset.ChangeCount > 0 );
end;

procedure TBasicoGridSelect_DevEx.Exclude;
begin
     with zetadbgriddbtableview do
     begin
          if ( Controller.SelectedRowCount> 0 ) then
          begin
               with FDataset do
               begin
                    DisableControls;
                    Controller.deleteselection;
                    EnableControls;
               end;
          end
          else if ( not FDataSet.IsEmpty ) then
              FDataSet.Delete;
     end;
     SetControls;
end;

{procedure TBasicoGridSelect_DevEx.WMExaminar(var Message: TMessage);
begin
     Exclude;
end;}

procedure TBasicoGridSelect_DevEx.ExcluirClick(Sender: TObject);
begin
     inherited;
     Exclude;
     SetControls;
end;

procedure TBasicoGridSelect_DevEx.RevertirClick(Sender: TObject);
begin
     inherited;
     with FDataset do
     begin
          DisableControls;
          UndoLastChange( True );
          EnableControls;
     end;
     SetControls;
end;

procedure TBasicoGridSelect_DevEx.ZetaDBGridorigCellClick(Column: TColumn);
begin
     inherited;
     SetControls;
end;

procedure TBasicoGridSelect_DevEx.ImprimirClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir La Lista Mostrada ?', 0, mbYes ) then
         {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes_DevEx{$ENDIF}.ImprimirGridParams( ZetaDBGridDBTableView, ZetaDBGridDBTableView.DataController.DataSource.DataSet, Caption, 'IM', ParametrosGrid);
end;

{***(@am):Por defecto las formas que hereden de esta base trabajaran en GridMode, si se desea trabajar sin gridmode, sobreescribir este eventos
       y cambiar la propiedad a False.***}
procedure TBasicoGridSelect_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H95002_Lista_de_empleados;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= True;
end;

procedure TBasicoGridSelect_DevEx.ExportarClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm( 'Exportar...', '� Desea exportar a Excel la lista mostrada ?', 0, mbYes ) then
         {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes_DevEx{$ENDIF}.ExportarGridParams( ZetaDBGridDBTableView, ZetaDBGridDBTableView.DataController.DataSource.DataSet, ParametrosGrid, Caption, 'IM');
end;

procedure TBasicoGridSelect_DevEx.ZetaDBGridDBTableViewCellDblClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;
  Exclude;
  SetControls;
end;

{***(am):Trabaja CON GridMode***}
procedure TBasicoGridSelect_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
if ZetaDBGridDBTableView.DataController.DataModeController.GridMode then
  begin
      inherited;
      self.AColumn := TcxGridDBColumn(AColumn);
  end;
end;

{***(am):Trabaja CON GridMode***}
procedure TBasicoGridSelect_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  if ZetaDBGridDBTableView.DataController.DataModeController.GridMode then
  begin
    inherited;
    ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
  end;
end;

procedure TBasicoGridSelect_DevEx.ApplyMinWidth;
var
   i,widthantes,totalwidth: Integer;
const
     widthColumnOptions =25;
begin
     totalwidth := 0;
     with  ZetaDBGridDBTableView do
     begin

          for i:=0 to  ColumnCount-1 do
          begin
           if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName<>'PrettyName') or (zetadbgriddbtableview.Columns[i].DataBinding.FieldName<>'PRETTYNAME') then
              begin
              widthantes :=Canvas.TextWidth( Columns[i].Caption) + widthColumnOptions;
              Columns[i].applybestfit;
              if( widthantes >=   Columns[i].Width) then
              begin
                Columns[i].Width :=  widthantes;
                Columns[i].MinWidth :=  widthantes;
              end;
              if( columns[i].visible)then
              totalWidth :=  totalWidth + Columns[i].Width;
              end;
          end;
     end;
     if(totalwidth < 550 ) then
     width := 550
     else
     if(totalwidth < 1261) then
     width := totalwidth + 19
     else
     width := 1280;
     resize;
   
end;
procedure TBasicoGridSelect_DevEx.ZetaDBGridDBTableViewKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       Exclude;
       SetControls;
  end;
end;

end.
