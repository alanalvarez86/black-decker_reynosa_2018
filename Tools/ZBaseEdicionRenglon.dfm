inherited BaseEdicionRenglon: TBaseEdicionRenglon
  Left = 529
  Top = 199
  Caption = 'BaseEdicionRenglon'
  ClientHeight = 323
  ClientWidth = 431
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 287
    Width = 431
    TabOrder = 4
    DesignSize = (
      431
      36)
    inherited OK: TBitBtn
      Left = 273
    end
    inherited Cancelar: TBitBtn
      Left = 352
    end
  end
  inherited PanelSuperior: TPanel
    Width = 431
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 431
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 105
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 51
    Width = 431
    Height = 50
    Align = alTop
    TabOrder = 2
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 101
    Width = 431
    Height = 186
    ActivePage = Datos
    Align = alClient
    TabOrder = 3
    object Datos: TTabSheet
      Caption = 'Generales'
    end
    object Tabla: TTabSheet
      Caption = 'Tabla'
      object GridRenglones: TZetaDBGrid
        Left = 0
        Top = 29
        Width = 423
        Height = 129
        Align = alClient
        DataSource = dsRenglon
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = GridRenglonesTitleClick
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 423
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BBAgregar: TBitBtn
          Left = 8
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Agregar Rengl'#243'n'
          TabOrder = 0
          OnClick = BBAgregarClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
        end
        object BBBorrar: TBitBtn
          Left = 148
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Borrar Rengl'#243'n'
          TabOrder = 1
          OnClick = BBBorrarClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
        end
        object BBModificar: TBitBtn
          Left = 288
          Top = 1
          Width = 129
          Height = 25
          Caption = 'Modificar Rengl'#243'n'
          TabOrder = 2
          OnClick = BBModificarClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 380
    Top = 1
  end
  object dsRenglon: TDataSource
    Left = 399
    Top = 1
  end
end
