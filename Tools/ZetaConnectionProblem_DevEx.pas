unit ZetaConnectionProblem_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Buttons,
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxGDIPlusClasses, ImgList,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TConnectionProblem_DevEx = class(TZetaDlgModal_DevEx)
    Imagen: TImage;
    Configurar: TcxButton;
    Info: TcxMemo;
    procedure FormCreate(Sender: TObject);
    procedure ConfigurarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetError( Value: Exception );
    //US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
    procedure ModoHTTP( lModoHTTPLogin: Boolean = False );
  end;

var
  ConnectionProblem_DevEx: TConnectionProblem_DevEx;

function ConnectionProblemDialog( Error: Exception; lModoHTTPLogin: Boolean = False ): TModalResult;

implementation

uses ZetaDialogo,
     ZetaCommonTools,
{$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor,
{$else}
     ZetaRegistryCliente,
{$endif}
     ZetaCommonClasses;

{$R *.DFM}

function ConnectionProblemDialog( Error: Exception; lModoHTTPLogin: Boolean = False ): TModalResult;
begin
     with TConnectionProblem_DevEx.Create( Application ) do
     begin
          try
             SetError( Error );
             ModoHTTP( lModoHTTPLogin );
             Chicharra;
             {$ifndef DOS_CAPAS}
             Configurar.Enabled := ZetaRegistryCliente.ClientRegistry.CanWrite;
             {$endif}
             ShowModal;
             Result := ModalResult;
          finally
                 Free;
          end;
     end;
end;

{ *********** TConnectionProblem ********* }

procedure TConnectionProblem_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {with Imagen do
     begin
          Picture.Icon.Handle := LoadIcon( 0, IDI_HAND );
     end; }
end;

procedure TConnectionProblem_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Cancelar_DevEx.Hint := Format( 'Salir de %s', [ Application.Title ] );
     ModoHTTP;
end;

procedure TConnectionProblem_DevEx.SetError(Value: Exception);
begin
     Info.Lines.Text := Value.Message;
end;

procedure TConnectionProblem_DevEx.ConfigurarClick(Sender: TObject);
begin
     inherited;
{$ifdef DOS_CAPAS}
     ZetaRegistryServerEditor.EspecificaComparte;
{$else}
     ClientRegistry.BuscaServidor;
{$endif}
end;

//US# 11757: Proteccion funcionalidad HTTP por licencia sentinel
procedure TConnectionProblem_DevEx.ModoHTTP( lModoHTTPLogin: Boolean = False );
begin
     if lModoHTTPLogin then
     begin
          case ClientRegistry.TipoConexion of
               conxHTTP:
               begin
                    Cancelar_DevEx.Caption := '&Salir';
                    Cancelar_DevEx.Hint := Format( 'Salir de %s', [ Application.Title ] );
               end;
          end;
     end;
end;

end.
