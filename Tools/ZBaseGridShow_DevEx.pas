unit ZBaseGridShow_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, ZetaClientDataset, ExtCtrls, ZBaseDlgModal_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, ImgList,
  cxButtons, cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TBaseGridShow_DevEx = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    ZetaDBGrid: TZetaCXGrid;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FDataset: TZetaClientDataset;
    function GetDataset: TZetaClientDataset;
    procedure SetDataset( Value: TZetaClientDataset );
  protected
    procedure ApplyMinWidth;     //DevEx
  public
    { Public declarations }
    property Dataset: TZetaClientDataset read GetDataset write SetDataset;
  end;
  TBaseGridShowClass = class of TBaseGridShow_DevEx;

var
  BaseGridShow_DevEx: TBaseGridShow_DevEx;

function GridShow( ZetaDataset: TZetaClientDataset; GridShowClass: TBaseGridShowClass ): Boolean;

implementation

uses ZetaDialogo;

{$R *.DFM}

function GridShow( ZetaDataset: TZetaClientDataset; GridShowClass: TBaseGridShowClass ): Boolean;
begin
     if ZetaDataset.IsEmpty then
     begin
          ZetaDialogo.zInformation( '� Atenci�n !', 'La Lista a verificar est� vac�a', 0 );
          Result := False;
     end
     else
     begin
          with GridShowClass.Create( Application ) as TBaseGridShow_DevEx do
          begin
               try
                  Dataset := ZetaDataset;
                  ShowModal;
                  Result := ( ModalResult = mrOk );
                  Dataset := nil;
               finally
                  Free;
               end;
          end;
     end;
end;

{ TBaseGridShow }

function TBaseGridShow_DevEx.GetDataset: TZetaClientDataset;
begin
     Result := FDataset;
end;

procedure TBaseGridShow_DevEx.SetDataset(Value: TZetaClientDataset);
begin
     if ( FDataset <> Value ) then
     begin
          FDataset := Value;
          if ( Value <> nil ) then
             Datasource.Dataset := Value;
     end;
end;

procedure TBaseGridShow_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  //Desactiva la posibilidad de agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
  //No se permite filtrar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := False;
  //Para que nunca muestre el filterbox inferior
  ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  ApplyMinWidth;
  ZetaDBGridDBTableView.ApplyBestFit();
  ZetaDBGridDBTableView.OptionsCustomize.ColumnHorzSizing := False;
end;

procedure TBaseGridShow_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =30;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
