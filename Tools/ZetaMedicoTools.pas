unit ZetaMedicoTools;

interface

uses SysUtils, Classes, Math{$ifndef DOTNET},Windows,  Messages, Controls, Forms{$endif};

     function GestacionSemanas( const dDefault, dInicioEmb: TDateTime ): String;
     function GestacionSemanasInt( const dDefault, dInicioEmb: TDateTime ): Integer;

const
     K_MEDICO_SI = 'Si';
     K_MEDICO_NO = 'No';
     K_DIAS_SEMANA = 7;

implementation

function GestacionSemanas( const dDefault, dInicioEmb: TDateTime ): String;
begin
     Result := InttoStr( GestacionSemanasInt( dDefault, dInicioEmb ) ) + ' Semanas';
end;

function GestacionSemanasInt( const dDefault, dInicioEmb: TDateTime ): Integer;
begin
     Result := Trunc(( dDefault - dInicioEmb) / K_DIAS_SEMANA) + 1 ;
end;

end.

