inherited EditTablas: TEditTablas
  Left = 269
  Top = 150
  Caption = 'Edici�n de Tablas'
  ClientHeight = 204
  ClientWidth = 408
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 168
    Width = 408
    TabOrder = 2
    inherited OK: TBitBtn
      Left = 249
    end
    inherited Cancelar: TBitBtn
      Left = 326
    end
  end
  inherited PanelSuperior: TPanel
    Width = 408
    TabOrder = 0
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 408
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 82
    end
  end
  object PanelDatos: TPanel [3]
    Left = 0
    Top = 51
    Width = 408
    Height = 117
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object DBInglesLBL: TLabel
      Left = 75
      Top = 51
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Ingl�s:'
    end
    object DBDescripcionLBL: TLabel
      Left = 47
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci�n:'
    end
    object DBCodigoLBL: TLabel
      Left = 70
      Top = 9
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C�digo:'
    end
    object Label1: TLabel
      Left = 76
      Top = 93
      Width = 30
      Height = 13
      Alignment = taRightJustify
      Caption = 'Texto:'
    end
    object Label2: TLabel
      Left = 66
      Top = 72
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'N�mero:'
    end
    object TB_CODIGO: TZetaDBEdit
      Left = 109
      Top = 5
      Width = 75
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'TB_CODIGO'
      DataSource = DataSource
    end
    object TB_ELEMENT: TDBEdit
      Left = 109
      Top = 26
      Width = 280
      Height = 21
      DataField = 'TB_ELEMENT'
      DataSource = DataSource
      TabOrder = 1
    end
    object TB_INGLES: TDBEdit
      Left = 109
      Top = 47
      Width = 280
      Height = 21
      DataField = 'TB_INGLES'
      DataSource = DataSource
      TabOrder = 2
    end
    object TB_TEXTO: TDBEdit
      Left = 109
      Top = 89
      Width = 280
      Height = 21
      DataField = 'TB_TEXTO'
      DataSource = DataSource
      TabOrder = 4
    end
    object TB_NUMERO: TZetaDBNumero
      Left = 109
      Top = 68
      Width = 73
      Height = 21
      Mascara = mnNumeroGlobal
      TabOrder = 3
      Text = '0.00'
      DataField = 'TB_NUMERO'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 4
    Top = 57
  end
end
