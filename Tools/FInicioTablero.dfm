inherited InicioTablero: TInicioTablero
  Caption = 'Tablero de Inicio'
  ClientHeight = 1001
  ClientWidth = 1473
  Color = 15987699
  OnDestroy = FormDestroy
  OnShow = FormShow
  ExplicitLeft = -205
  ExplicitWidth = 1473
  ExplicitHeight = 1001
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 1473
    ExplicitWidth = 1473
    inherited ValorActivo2: TPanel
      Width = 1214
      ExplicitWidth = 1214
      inherited textoValorActivo2: TLabel
        Width = 1208
        ExplicitLeft = 1128
      end
    end
  end
  object PanelFechaGraficaAsistencia: TPanel [1]
    Left = 0
    Top = 19
    Width = 1473
    Height = 33
    Align = alTop
    Color = 15395562
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object pnlParentLinks: TPanel
      Left = 1
      Top = 1
      Width = 1471
      Height = 31
      Align = alClient
      BevelEdges = []
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Padding.Left = 10
      Padding.Top = 10
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        1471
        31)
      object PanelFecha: TPanel
        Left = 1243
        Top = 1
        Width = 225
        Height = 27
        Anchors = [akTop, akRight]
        BevelOuter = bvNone
        Color = 15987699
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        DesignSize = (
          225
          27)
        object FechaActiva: TLabel
          Left = 11
          Top = 8
          Width = 84
          Height = 13
          Anchors = []
          Caption = 'Fecha Asistencia:'
        end
        object AsistenciaFechaGrafica: TZetaFecha
          Left = 105
          Top = 3
          Width = 115
          Height = 22
          Cursor = crArrow
          Hint = 'Fecha Asistencia'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Text = '10/Dec/97'
          UseEnterKey = True
          Valor = 35774.000000000000000000
          OnValidDate = AsistenciaFechaGraficaValidDate
        end
      end
    end
  end
  object panelGraficas: TPanel [2]
    Left = 0
    Top = 52
    Width = 1473
    Height = 933
    Align = alClient
    BevelEdges = [beLeft, beTop, beRight]
    BevelOuter = bvNone
    Color = 15395562
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    ExplicitHeight = 908
    object GraficaBasePie: TChart
      Left = 653
      Top = 14
      Width = 400
      Height = 250
      AllowPanning = pmNone
      BackWall.Pen.Visible = False
      BackWall.Visible = False
      BottomWall.Visible = False
      LeftWall.Visible = False
      Legend.Symbol.Pen.Visible = False
      MarginTop = 15
      ScrollMouseButton = mbLeft
      Title.ClipText = False
      Title.CustomPosition = True
      Title.Font.Color = clBlack
      Title.Font.Height = -16
      Title.Font.Name = 'Segoe UI'
      Title.Font.Style = [fsBold]
      Title.Font.Quality = fqClearType
      Title.Font.Shadow.Visible = False
      Title.Frame.Visible = False
      Title.Left = 5
      Title.Margins.Left = 0
      Title.Shadow.Visible = False
      Title.Text.Strings = (
        'Titulo de Gr'#225'fica')
      Title.TextAlignment = taLeftJustify
      Title.Top = 5
      Title.AdjustFrame = False
      Frame.Visible = False
      LeftAxis.LabelsFormat.Frame.SmallDots = True
      Panning.MouseWheel = pmwNone
      Shadow.Visible = False
      View3D = False
      View3DOptions.Elevation = 315
      View3DOptions.Orthogonal = False
      View3DOptions.Perspective = 0
      View3DOptions.Rotation = 360
      Zoom.Allow = False
      Zoom.MouseButton = mbRight
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 0
      Visible = False
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        15
        21
        15
        21)
      ColorPaletteIndex = -2
      ColorPalette = (
        9142668
        4930381
        14199423
        13751808
        13490024
        45801
        4741324
        5296787
        3381759
        5664755
        8479477
        6114916
        7572782
        14461696
        4137411
        10785986)
      object BarSeries3: TPieSeries
        Legend.Visible = False
        Selected.Hover.Visible = False
        Marks.ChildLayout = slLeftRight
        Marks.Font.Color = 4539717
        Marks.Font.Name = 'MS Sans Serif'
        Marks.Font.Quality = fqBest
        Marks.Frame.Visible = False
        Marks.Transparent = True
        Marks.Style = smsValue
        Marks.BackColor = clWhite
        Marks.Callout.Length = 20
        Marks.Color = clWhite
        ShowInLegend = False
        XValues.Order = loAscending
        YValues.Name = 'Pie'
        YValues.Order = loNone
        Frame.InnerBrush.BackColor = clRed
        Frame.InnerBrush.Gradient.EndColor = clGray
        Frame.InnerBrush.Gradient.MidColor = clWhite
        Frame.InnerBrush.Gradient.StartColor = 4210752
        Frame.InnerBrush.Gradient.Visible = True
        Frame.MiddleBrush.BackColor = clYellow
        Frame.MiddleBrush.Gradient.EndColor = 8553090
        Frame.MiddleBrush.Gradient.MidColor = clWhite
        Frame.MiddleBrush.Gradient.StartColor = clGray
        Frame.MiddleBrush.Gradient.Visible = True
        Frame.OuterBrush.BackColor = clGreen
        Frame.OuterBrush.Gradient.EndColor = 4210752
        Frame.OuterBrush.Gradient.MidColor = clWhite
        Frame.OuterBrush.Gradient.StartColor = clSilver
        Frame.OuterBrush.Gradient.Visible = True
        Frame.Width = 4
        OtherSlice.Legend.Visible = False
        PiePen.Color = 10708548
        PiePen.Visible = False
        object AverageTeeFunction4: TAverageTeeFunction
          CalcByValue = False
        end
      end
    end
    object BaseGridListado: TZetaCXGrid
      Left = 653
      Top = 270
      Width = 400
      Height = 250
      TabOrder = 1
      Visible = False
      LockedStateImageOptions.AssignedValues = [lsiavFont]
      LockedStateImageOptions.Font.Charset = DEFAULT_CHARSET
      LockedStateImageOptions.Font.Color = clWindowText
      LockedStateImageOptions.Font.Height = -11
      LockedStateImageOptions.Font.Name = 'MS Sans Serif'
      LockedStateImageOptions.Font.Style = []
      object BaseGridListadoDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
      end
      object BaseGridListadoLevel1: TcxGridLevel
        GridView = BaseGridListadoDBTableView1
      end
    end
    object GraficaBaseHorizBarras: TChart
      Left = 1063
      Top = 270
      Width = 400
      Height = 250
      AllowPanning = pmNone
      BackWall.Pen.Visible = False
      BackWall.Visible = False
      BottomWall.Visible = False
      LeftWall.Visible = False
      Legend.Symbol.Pen.Visible = False
      MarginTop = 15
      PrintProportional = False
      Title.ClipText = False
      Title.CustomPosition = True
      Title.Font.Color = clBlack
      Title.Font.Height = -16
      Title.Font.Name = 'Segoe UI'
      Title.Font.Style = [fsBold]
      Title.Font.Quality = fqClearType
      Title.Font.Shadow.Visible = False
      Title.Frame.Visible = False
      Title.Left = 5
      Title.Margins.Left = 0
      Title.Shadow.Visible = False
      Title.Text.Strings = (
        'Titulo de Gr'#225'fica')
      Title.TextAlignment = taLeftJustify
      Title.Top = 5
      Title.AdjustFrame = False
      Frame.Visible = False
      LeftAxis.LabelsFormat.Frame.SmallDots = True
      Panning.MouseWheel = pmwNone
      Shadow.Visible = False
      View3D = False
      Zoom.Allow = False
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 2
      Visible = False
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        9
        14
        15
        15)
      ColorPaletteIndex = -2
      ColorPalette = (
        9142668
        4930381
        14199423
        13751808
        13490024
        45801
        4741324
        5296787
        3381759
        5664755
        8479477
        6114916
        7572782
        14461696
        4137411
        10785986)
      object HorizBarSeries2: THorizBarSeries
        Legend.Visible = False
        Selected.Hover.Visible = False
        BarBrush.Gradient.Direction = gdLeftRight
        BarPen.Color = 10708548
        BarPen.Visible = False
        Marks.ChildLayout = slLeftRight
        Marks.Font.Color = 4539717
        Marks.Font.Name = 'MS Sans Serif'
        Marks.Font.Quality = fqBest
        Marks.Frame.Visible = False
        Marks.Transparent = True
        Marks.Style = smsValue
        Marks.BackColor = clWhite
        Marks.Color = clWhite
        SeriesColor = 12615680
        ShowInLegend = False
        Gradient.Direction = gdLeftRight
        TickLines.Style = psDot
        TickLines.SmallDots = True
        XValues.Name = 'Bar'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loAscending
      end
    end
    object GraficaBaseColumnasBarras: TChart
      Left = 1063
      Top = 14
      Width = 400
      Height = 250
      AllowPanning = pmNone
      BackWall.Pen.Visible = False
      BackWall.Visible = False
      BottomWall.Visible = False
      LeftWall.Visible = False
      Legend.Symbol.Pen.Visible = False
      MarginTop = 15
      PrintProportional = False
      Title.ClipText = False
      Title.CustomPosition = True
      Title.Font.Color = clBlack
      Title.Font.Height = -16
      Title.Font.Name = 'Segoe UI'
      Title.Font.Style = [fsBold]
      Title.Font.Quality = fqClearType
      Title.Font.Shadow.Visible = False
      Title.Frame.Visible = False
      Title.Left = 5
      Title.Margins.Left = 0
      Title.Shadow.Visible = False
      Title.Text.Strings = (
        'Titulo de Gr'#225'fica')
      Title.TextAlignment = taLeftJustify
      Title.Top = 5
      Title.AdjustFrame = False
      Frame.Visible = False
      LeftAxis.LabelsFormat.Frame.SmallDots = True
      Panning.MouseWheel = pmwNone
      Shadow.Visible = False
      View3D = False
      Zoom.Allow = False
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 3
      Visible = False
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        9
        14
        15
        15)
      ColorPaletteIndex = -2
      ColorPalette = (
        9142668
        4930381
        14199423
        13751808
        13490024
        45801
        4741324
        5296787
        3381759
        5664755
        8479477
        6114916
        7572782
        14461696
        4137411
        10785986)
      object HorizBarSeries3: TBarSeries
        Legend.Visible = False
        Selected.Hover.Visible = False
        BarBrush.Gradient.Direction = gdLeftRight
        BarPen.Color = 10708548
        BarPen.Visible = False
        Marks.ChildLayout = slLeftRight
        Marks.Font.Color = 4539717
        Marks.Font.Name = 'MS Sans Serif'
        Marks.Font.Quality = fqBest
        Marks.Frame.Visible = False
        Marks.Transparent = True
        Marks.Style = smsValue
        Marks.BackColor = clWhite
        Marks.Color = clWhite
        SeriesColor = 12615680
        ShowInLegend = False
        Gradient.Direction = gdLeftRight
        TickLines.Style = psDot
        TickLines.SmallDots = True
        XValues.Name = 'X'
        XValues.Order = loAscending
        YValues.Name = 'Bar'
        YValues.Order = loNone
      end
    end
    object BaseHorizStack: TChart
      Left = 653
      Top = 526
      Width = 400
      Height = 250
      AllowPanning = pmNone
      BackWall.Pen.Visible = False
      BackWall.Visible = False
      BottomWall.Visible = False
      LeftWall.Visible = False
      Legend.Symbol.Pen.Visible = False
      MarginTop = 15
      PrintProportional = False
      Title.ClipText = False
      Title.CustomPosition = True
      Title.Font.Color = clBlack
      Title.Font.Height = -16
      Title.Font.Name = 'Segoe UI'
      Title.Font.Style = [fsBold]
      Title.Font.Quality = fqClearType
      Title.Font.Shadow.Visible = False
      Title.Frame.Visible = False
      Title.Left = 5
      Title.Margins.Left = 0
      Title.Shadow.Visible = False
      Title.Text.Strings = (
        'Titulo de Gr'#225'fica')
      Title.TextAlignment = taLeftJustify
      Title.Top = 5
      Title.AdjustFrame = False
      BottomAxis.Increment = 1.000000000000000000
      Frame.Visible = False
      LeftAxis.LabelsFormat.Frame.SmallDots = True
      LeftAxis.LabelsSeparation = 0
      Panning.MouseWheel = pmwNone
      Shadow.Visible = False
      View3D = False
      Zoom.Allow = False
      BevelOuter = bvNone
      Color = clWhite
      TabOrder = 4
      Visible = False
      DefaultCanvas = 'TGDIPlusCanvas'
      PrintMargins = (
        9
        14
        15
        15)
      ColorPaletteIndex = -2
      ColorPalette = (
        9142668
        4930381
        14199423
        13751808
        13490024
        45801
        4741324
        5296787
        3381759
        5664755
        8479477
        6114916
        7572782
        14461696
        4137411
        10785986)
      object HorizBarSeries1: THorizBarSeries
        Legend.Visible = False
        Selected.Hover.Visible = False
        BarBrush.Gradient.Direction = gdLeftRight
        BarPen.Color = 10708548
        BarPen.Visible = False
        Marks.ChildLayout = slLeftRight
        Marks.Font.Color = 4539717
        Marks.Font.Name = 'MS Sans Serif'
        Marks.Font.Quality = fqBest
        Marks.Frame.Visible = False
        Marks.Transparent = True
        Marks.Style = smsValue
        Marks.BackColor = clWhite
        Marks.Color = clWhite
        SeriesColor = 12615680
        ShowInLegend = False
        Gradient.Direction = gdLeftRight
        TickLines.Style = psDot
        TickLines.SmallDots = True
        XValues.Name = 'Bar'
        XValues.Order = loNone
        YValues.Name = 'Y'
        YValues.Order = loAscending
      end
    end
  end
  object PanelControl: TPanel [3]
    Left = 0
    Top = 985
    Width = 1473
    Height = 16
    Align = alBottom
    Anchors = []
    BevelEdges = []
    BevelOuter = bvNone
    Color = 15395562
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 3
    object ckMostrarPantalla: TCheckBox
      Left = 1237
      Top = 0
      Width = 236
      Height = 16
      Align = alRight
      Caption = 'Mostrar esta pantalla al iniciar Sistema Tress'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = ckMostrarPantallaClick
    end
  end
end
