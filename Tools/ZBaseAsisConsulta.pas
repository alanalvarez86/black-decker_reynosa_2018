unit ZBaseAsisConsulta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Db, ExtCtrls, StdCtrls;

type
  TBaseAsisConsulta = class(TBaseConsulta)
  private
    { Private declarations }
  protected
    function GetFechaTarjeta: TDate; virtual;       
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  BaseAsisConsulta: TBaseAsisConsulta;

implementation
uses
    DCliente,
    ZetaCommonClasses;
{$R *.DFM}

{ TBaseAsisConsulta }

function TBaseAsisConsulta.GetFechaTarjeta: TDate;
begin
     if ( DataSource.DataSet <> NIL ) then
        Result := DataSource.DataSet.FieldByName('AU_FECHA').AsDateTime
     else
         Result := NullDateTime;
end;

function TBaseAsisConsulta.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
end;

function TBaseAsisConsulta.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.Dataset, sMensaje );
     end;
end;

function TBaseAsisConsulta.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
end;

end.




