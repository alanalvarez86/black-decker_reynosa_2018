unit ZBaseEdicion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, DBCtrls, Db, DBGrids,
     {$ifndef ver130}
     variants,
     {$endif}
     ZetaClientDataset,
     ZetaCommonLists,
     ZBaseDlgModal_DevEx,
     ZetaSmartLists, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, TressMorado2013, dxSkinsDefaultPainters,
     cxButtons, dxSkinsdxBarPainter, ImgList, dxBar,
     cxClasses, dxBarExtItems, cxControls, cxNavigator, cxDBNavigator,
     cxGridCustomView, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, cxGrid;

type
  TBaseEdicion_DevEx = class(TZetaDlgModal_DevEx)
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    ValorActivo2: TPanel;
    DataSource: TDataSource;
    DevEx_BarManagerEdicion: TdxBarManager;
    BarSuperior: TdxBar;
    dxBarButton_AgregarBtn: TdxBarButton;
    cxImageList16Edicion: TcxImageList;
    dxBarButton_BorrarBtn: TdxBarButton;
    dxBarButton_ModificarBtn: TdxBarButton;
    dxBarButton_BuscarBtn: TdxBarButton;
    dxBarButton_ImprimirBtn: TdxBarButton;
    dxBarButton_ImprimirFormaBtn: TdxBarButton;
    dxBarButton_ExportarBtn: TdxBarButton;
    dxBarButton_CortarBtn: TdxBarButton;
    dxBarButton_CopiarBtn: TdxBarButton;
    dxBarButton_PegarBtn: TdxBarButton;
    dxBarButton_UndoBtn: TdxBarButton;
    dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem;
    DevEx_cxDBNavigatorEdicion: TcxDBNavigator;
    textoValorActivo1: TLabel;
    textoValorActivo2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure DataSourceStateChange(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure dxBarButton_AgregarBtnClick(Sender: TObject);
    procedure dxBarButton_BorrarBtnClick(Sender: TObject);
    procedure dxBarButton_ModificarBtnClick(Sender: TObject);
    procedure dxBarButton_BuscarBtnClick(Sender: TObject);
    procedure dxBarButton_ImprimirBtnClick(Sender: TObject);
    procedure dxBarButton_ImprimirFormaBtnClick(Sender: TObject);
    procedure dxBarButton_ExportarBtnClick(Sender: TObject);
    procedure dxBarButton_CortarBtnClick(Sender: TObject);
    procedure dxBarButton_CopiarBtnClick(Sender: TObject);
    procedure dxBarButton_PegarBtnClick(Sender: TObject);
    procedure dxBarButton_UndoBtnClick(Sender: TObject);
    procedure DevEx_BarManagerEdicionShowToolbarsPopup(
      Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
  private
    { Private declarations }
    FFirstControl: TWinControl;
    FIndexDerechos: Integer;
    FModo: TDatasetState;
    FTipoValorActivo1: TipoEstado;
    FTipoValorActivo2: TipoEstado;
    FInsertInicial: Boolean;
    function DoConnect: Boolean;
    function NoHayDatos: Boolean;
    procedure DoDisconnect;
    procedure InitValoresActivos;
    procedure CancelarCambios;
    procedure SetModo( const Value: TDatasetState );
    procedure DoExportar;
    procedure Exportar;
    procedure InitExcelBtn;
    {$IFNDEF TRESS_DELPHIXE5_UP}
    function ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
    {$ENDIF}
  protected
    { Protected declarations }
    property IndexDerechos: Integer read FIndexDerechos write FIndexDerechos;
    property InsertInicial: Boolean read FInsertInicial;
    property Modo: TDatasetState read FModo write SetModo;
    property FirstControl: TWinControl read FFirstControl write FFirstControl;
    property TipoValorActivo1: TipoEstado read FTipoValorActivo1 write FTipoValorActivo1;
    property TipoValorActivo2: TipoEstado read FTipoValorActivo2 write FTipoValorActivo2;
    function CheckDerechos(const iDerecho: Integer): Boolean;dynamic;
    function Editing: Boolean;
    function Inserting: Boolean;
    function BotonesActivos: Boolean; dynamic;
    function ClientDataset: TZetaClientDataset;
    function NoData( Fuente: TDatasource ): Boolean;
    function PuedeAgregar(var sMensaje: String): Boolean; dynamic;
    function PuedeBorrar(var sMensaje: String): Boolean; dynamic;
    function PuedeModificar(var sMensaje: String): Boolean; dynamic;
    function PuedeImprimir(var sMensaje: String): Boolean; dynamic;
    function ValoresGrid: Variant;virtual;
    function CanLookup: Boolean; virtual;
    procedure AsignaValoresActivos;
    procedure Connect; virtual; abstract;
    procedure Disconnect; virtual;
    procedure Agregar; virtual;
    procedure Borrar; virtual;
    procedure Modificar; virtual;
    procedure Imprimir; dynamic;
    procedure ImprimirForma;virtual;
    procedure DoCancelChanges; dynamic;
    procedure DoDelete;
    procedure DoEdit;
    procedure DoInsert;
    procedure DoLookup; virtual;
    procedure DoPrint;
    procedure DoPrintForma;
    procedure FocusFirstControl; dynamic;
    procedure HabilitaControles; dynamic;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure EscribirCambios; dynamic;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
  end;
  TBaseEdicionClass_DevEx = class of TBaseEdicion_DevEx;

var
  BaseEdicion_DevEx: TBaseEdicion_DevEx;

procedure ShowFormaEdicion( var Forma; EdicionClass: TBaseEdicionClass_DevEx );
procedure ShowFormaEdicionAndFree( var Forma; EdicionClass: TBaseEdicionClass_DevEx );

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     ZAccesosMgr,
     ZCerrarEdicion_DevEx,
     //(@am): Se migro la funcionalidad de impresion y exportacion de grids a la unidad ZImprimeGrid para la compilacion de XE5.
     {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF},
     DCliente,
     DBClient;

{$R *.DFM}

procedure ShowFormaEdicion( var Forma; EdicionClass: TBaseEdicionClass_DevEx );
begin
     ZBaseDlgModal_DevEx.ShowDlgModal( Forma, EdicionClass );
end;

procedure ShowFormaEdicionAndFree( var Forma; EdicionClass: TBaseEdicionClass_DevEx );
begin
     ZBaseDlgModal_DevEx.ShowDlgModal( Forma, EdicionClass );
     if ( TBaseEdicion_DevEx(Forma) <> nil ) then
        FreeAndNil( Forma );
end;

{ ************ TZetaBaseEdicion *********** }

constructor TBaseEdicion_DevEx.Create( AOwner: TComponent );
begin
     FTipoValorActivo1 := stNinguno;
     FTipoValorActivo2 := stNinguno;
     inherited Create( AOwner );
     InitValoresActivos;

     InitExcelBtn;
end;

{La razon de este metodo, es para no tener que revisar TODAS las herencias
de ZbaseEdicion.
El boton de excel entra a la mitad del panel, lo que se requiere que las descencias
se revisen y que no se encime el boton.
Con este metodo, si la forma NO tiene grid, el boton NO aparece}

procedure TBaseEdicion_DevEx.InitExcelBtn;
 var
    lVisible: boolean;
    i: integer;
begin
     lVisible := FALSE;
     {for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TDBGrid ) then
          begin
               lVisible := TRUE;
               Break;
          end;
     end;}
     //DevEx: Descomentar cuando se cambien los grids de las formas de edicion y se cambie el metodo Exportar de esta forma
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( ( Components[ i ] is TcxGridDBTableView ) or ( Components[ i ] is TDBGrid ) ) then
          begin
               lVisible := TRUE;
               Break;
          end;
     end;
     if lVisible then
        dxBarButton_ExportarBtn.Visible := ivAlways
     else
         dxBarButton_ExportarBtn.Visible := ivNever;
end;

procedure TBaseEdicion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     { Para permitir a descendientes invocar este evento }

end;

procedure TBaseEdicion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     if DoConnect then
     begin
          { Impide que usuarios sin derechos de Modificaci�n }
          { puedan llegar a modo de Edici�n simplemente cambiando }
          { el contenido de un control }
          Datasource.AutoEdit := CheckDerechos( K_DERECHO_CAMBIO );
          AsignaValoresActivos;
          FocusFirstControl;
          FInsertInicial := Inserting;
     end;
end;

procedure TBaseEdicion_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     inherited;
     if Editing then
     begin
          case ZCerrarEdicion_DevEx.CierraEdicion of
               mrOk:
               begin
                    EscribirCambios;
                    CanClose := not Editing;
               end;
               mrIgnore:
               begin
                    CancelarCambios;
                    CanClose := not Editing;
               end;
          else
              CanClose := False;
          end;
     end
     else
         CanClose := True;
end;

procedure TBaseEdicion_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     DoDisconnect;
     Action := caHide;
end;

function TBaseEdicion_DevEx.ClientDataset: TZetaClientDataset;
begin
     Result := TZetaClientDataset( Datasource.Dataset );
end;

function TBaseEdicion_DevEx.CanLookup: Boolean;
begin
     with DataSource do
     begin
          Result := ( Dataset <> nil ) and ( Dataset is TZetaLookupDataset );
     end;
end;

function TBaseEdicion_DevEx.NoData( Fuente: TDatasource ): Boolean;
begin
     with Fuente do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := Dataset.IsEmpty;
     end;
end;

function TBaseEdicion_DevEx.NoHayDatos: Boolean;
begin
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
              Result := Dataset.IsEmpty;
     end;
end;

function TBaseEdicion_DevEx.DoConnect: Boolean;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             try
                Connect;
                Result := True;
             except
                   on Error: Exception do
                   begin
                        zExcepcion( '� Error al conectar forma !', '� ventana no pudo ser conectada a sus datos !', Error, 0 );
                        Result := False;
                   end;
             end;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseEdicion_DevEx.DoDisconnect;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Disconnect;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TBaseEdicion_DevEx.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TBaseEdicion_DevEx.DoLookup;
{
var
   sKey, sDescripcion: String;
}
begin
     {
     if CanLookup then
        TZetaLookupDataset( ClientDataset ).Search( '', sKey, sDescripcion );
     }
end;

procedure TBaseEdicion_DevEx.DoCancelChanges;
begin
     with ClientDataset do
     begin
          CancelUpdates;
     end;
end;

procedure TBaseEdicion_DevEx.InitValoresActivos;
begin
     if ( TipoValorActivo1 = stNinguno ) and ( TipoValorActivo2 = stNinguno ) then
     begin
          PanelIdentifica.Visible := False;
          ValorActivo1.Visible := False;
          ValorActivo2.Visible := False;
          Splitter.Visible := False;
     end
     else
     begin
          if ( TipoValorActivo1 = stNinguno ) then
          begin
               ValorActivo1.Align := alNone;
               ValorActivo1.Visible := False;
               ValorActivo2.Align := alClient;
               Splitter.Visible := False;
          end;
          if ( TipoValorActivo2 = stNinguno ) then
          begin
               ValorActivo2.Align := alNone;
               ValorActivo2.Visible := False;
               ValorActivo1.Align := alClient;
               Splitter.Visible := False;
          end;
     end;
end;

{***DevEx (by am): Se dejo de usar el caption de los paneles para mostrar los valores activos.
Ahora se utilizara una label dentro del panel. ***}
procedure TBaseEdicion_DevEx.AsignaValoresActivos;
begin
     with dmCliente do
     begin
          textoValorActivo1.Caption := GetValorActivoStr( TipoValorActivo1 );
          textoValorActivo2.Caption := GetValorActivoStr( TipoValorActivo2 )
     end;

end;

procedure TBaseEdicion_DevEx.HabilitaControles;
begin
     //DevEx:
     DevEx_cxDBNavigatorEdicion.Enabled := not Editing;
     dxBarButton_AgregarBtn.Enabled := not Editing;
     dxBarButton_BorrarBtn.Enabled := not Editing and not NoHayDatos;
     dxBarButton_ModificarBtn.Enabled := not Editing and not NoHayDatos;
     dxBarButton_BuscarBtn.Enabled := CanLookup and not Editing;
     dxBarButton_ImprimirBtn.Enabled := not Editing;
     dxBarButton_ImprimirFormaBtn.Enabled := not Editing;
     dxBarButton_CortarBtn.Enabled := True;
     dxBarButton_CopiarBtn.Enabled := True;
     dxBarButton_PegarBtn.Enabled := True;
     dxBarButton_UndoBtn.Enabled := Editing;
     
     OK_DevEx.Enabled := Editing;
     if Editing then
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkCancel;
               OptionsImage.ImageIndex := 0;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar cambios';
          end;
          if Inserting then
             FocusFirstControl;
     end
     else
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkClose;
               OptionsImage.ImageIndex := 2;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar pantalla y salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                 SetFocus;
          end;
     end;
     Application.ProcessMessages;
end;

procedure TBaseEdicion_DevEx.FocusFirstControl;
begin
     if Visible and ( FirstControl <> nil ) then
     begin
          with FirstControl do
          begin
               if Visible and Enabled and CanFocus then
                  SetFocus;
          end;
     end;
end;

function TBaseEdicion_DevEx.Editing: Boolean;
begin
     Result := ( Modo in [ dsInsert, dsEdit ] );
end;

function TBaseEdicion_DevEx.Inserting: Boolean;
begin
     Result := ( Modo in [ dsInsert ] );
end;

function TBaseEdicion_DevEx.BotonesActivos: Boolean;
begin
     Result := ( not Editing );
end;

procedure TBaseEdicion_DevEx.SetModo(const Value: TDatasetState);
begin
     if ( FModo <> Value ) then
     begin
          FModo := Value;
          HabilitaControles;
     end;
end;

function TBaseEdicion_DevEx.CheckDerechos( const iDerecho: Integer ): Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( IndexDerechos, iDerecho );
end;

procedure TBaseEdicion_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     inherited KeyDown( Key, Shift );
     if ( Key <> 0 ) and BotonesActivos then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                        80: { Letra P = Imprimir }
                        begin
                             Key := 0;
                             DoPrint;
                        end;
                        79: { Letra 0 = Imprimir Forma }
                        begin
                             Key := 0;
                             {
                             if ImprimirFormaBtn.Enabled then
                                ImprimirForma;
                             }
                        end;
                        66:  { Letra F = Buscar }
                        begin
                             Key := 0;
                             if CanLookup then
                                DoLookup;
                        end;
                   end;
              end;
     end;
end;

{ ************* Eventos de Controles ************* }

procedure TBaseEdicion_DevEx.DataSourceStateChange(Sender: TObject);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Modo := dsInactive
          else
              Modo := Dataset.State;
     end;
end;

procedure TBaseEdicion_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if NoHayDatos then
        Close;
end;

procedure TBaseEdicion_DevEx.OK_DevExClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TBaseEdicion_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     if Editing then
        CancelarCambios
     else
         Close;
end;

{ ******** Manejo de Altas, Bajas, Cambios e Impresi�n ********** }

function TBaseEdicion_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_ALTA );
     if not Result then
        sMensaje := Format( 'No tiene permiso para %s registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukInsert) )] );
end;

procedure TBaseEdicion_DevEx.Agregar;
begin
     ClientDataset.Append;
end;

procedure TBaseEdicion_DevEx.DoInsert;
var
   sMensaje: String;
begin
     if PuedeAgregar( sMensaje ) then
        Agregar
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

function TBaseEdicion_DevEx.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_BAJA );
     if not Result then
        sMensaje := Format( 'No tiene permiso para %s registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) )] ) ;
end;

procedure TBaseEdicion_DevEx.Borrar;
begin
     ClientDataset.Borrar;
end;

procedure TBaseEdicion_DevEx.DoDelete;
var
   sMensaje: String;
begin
     if NoHayDatos then
        ZetaDialogo.zInformation( Caption, Format('No hay datos para %s', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukDelete) )] ), 0 )
     else
         if PuedeBorrar( sMensaje ) then
            Borrar
         else
             ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

function TBaseEdicion_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_CAMBIO );
     if not Result then
        sMensaje := Format('No tiene permiso para %s registros', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) )] );
end;

procedure TBaseEdicion_DevEx.Modificar;
begin
     ClientDataset.Edit;
end;

procedure TBaseEdicion_DevEx.DoEdit;
var
   sMensaje: String;
begin
     if NoHayDatos then
        ZetaDialogo.zInformation( Caption, Format('No hay datos para %s', [ZetaCommonLists.ObtieneElemento( lfUpdateKind, Ord(ukModify) )] ), 0 )
     else
         if PuedeModificar( sMensaje ) then
            Modificar
         else
             ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

function TBaseEdicion_DevEx.PuedeImprimir( var sMensaje: String ): Boolean;
begin
     Result := CheckDerechos( K_DERECHO_IMPRESION );
     if not Result then
        sMensaje := 'No tiene permiso para imprimir registros';
end;

function TBaseEdicion_DevEx.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ textoValorActivo1.Caption,
                             textoValorActivo2.Caption,
                             TipoValorActivo1,
                             TipoValorActivo2 ] ); 
end;

//DevEx(by am): Modificado para que pueda imprimir los grids de DevExpress
procedure TBaseEdicion_DevEx.Imprimir;
var
   lImprime: Boolean;
   i: integer;
   Valor : Variant;
   {$IFNDEF TRESS_DELPHIXE5_UP}
   //DevEx
   oGrid: TDBGrid;
   {$ENDIF}
begin
     lImprime := False;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( (Components[ i ] is TDBGrid) or ( Components[ i ] is TcxGridDBTableView ) ) then
          begin
               lImprime := True;
               if ZetaDialogo.zConfirm( 'Imprimir...',
                                        '� Desea imprimir el grid de la pantalla ' + Caption + ' ?',
                                        0,
                                        mbYes ) then
               begin
                    Valor := ValoresGrid;
                    if (Components[ i ] is TDBGrid) then
                       {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF}.ImprimirGrid( TDBGrid( Components[ i ] ), TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                                                                                                         Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin
                         {$IFDEF TRESS_DELPHIXE5_UP}
                         ZImprimeGrid.ImprimirGrid( ( Components[ i ] as TcxGridDBTableView), (Components[ i ] as TcxGridDBTableView).DataController.DataSource.DataSet , Caption, 'IM',
                                                    Valor[0],Valor[1],Valor[2],Valor[3]);
                         {$ELSE}
                         oGrid := ConvierteATDBGrid ( ( Components[ i ] as TcxGridDBTableView ) );  //(@am): En ZImprimeGrid ya se implemento el metodo para TcxGridDBtableview
                         try
                            FBaseReportes.ImprimirGrid( oGrid, oGrid.DataSource.DataSet , Caption, 'IM',
                                                        Valor[0],Valor[1],Valor[2],Valor[3]);
                         finally
                                FreeAndNil( oGrid );
                         end;
                         {$ENDIF}
                    end;

               end;
               Break;
          end;
     end;
     if not lImprime and ZetaDialogo.zConfirm( 'Imprimir...', '� Desea imprimir la pantalla ' + Caption + ' ?', 0, mbYes ) then
        Print;
end;

{$IFNDEF TRESS_DELPHIXE5_UP}
function TBaseEdicion_DevEx.ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
var
i: Integer;
oGrid: TDBGrid;
begin
     //Creamos un TDBGrid Virtual
     oGrid := TDBGrid.Create(Self);
     for i := 0 to DevEx_TableView.ColumnCount -1 do
     begin

          with oGrid.Columns.Add do
          begin
               Visible       := DevEx_TableView.Columns[ i ].Visible;
               FieldName     := DevEx_TableView.Columns[ i ].DataBinding.FieldName;
               Title.Caption := DevEx_TableView.Columns[ i ].Caption;
          end;
     end;
     oGrid.DataSource :=  DevEx_TableView.DataController.DataSource;
     result := oGrid;
end;
{$ENDIF}

procedure TBaseEdicion_DevEx.DoPrint;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        Imprimir
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TBaseEdicion_DevEx.CancelarCambios;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             DoCancelChanges;
          finally
                 Cursor := oCursor;
          end;
     end;
     if FInsertInicial then
        Close;
end;

procedure TBaseEdicion_DevEx.EscribirCambios;
var
   oCursor: TCursor;
begin
     with ClientDataset do
     begin
          with Screen do
          begin
               oCursor := Cursor;
               Cursor := crHourglass;
               try
                  Enviar;
               finally
                      Cursor := oCursor;
               end;
          end;
          if ( ChangeCount = 0 ) then
            FInsertInicial := FALSE
          else
              Edit;
     end;
end;

procedure TBaseEdicion_DevEx.ImprimirForma;
begin
     ZetaDialogo.ZInformation( Caption, 'Esta pantalla no cuenta con impresi�n de formas', 0 );
end;

procedure TBaseEdicion_DevEx.DoPrintForma;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        ImprimirForma
     else
         ZetaDialogo.zInformation( Caption, sMensaje, 0 );
end;

procedure TBaseEdicion_DevEx.DoExportar;
var
   sMensaje: String;
begin
     if PuedeImprimir( sMensaje ) then
        Exportar
     else
         ZetaDialogo.zInformation( Caption, 'No tiene permiso para exportar registros', 0 );
end;

{procedure TBaseEdicion_DevEx.Exportar;
var
   lExporta: Boolean;
   i: integer;
   Valor : Variant;
begin
     lExporta:= False;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( Components[ i ] is TDBGrid ) then
          begin
               lExporta:= True;
               if ZetaDialogo.zConfirm( 'Exportar...',
                                        '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?',
                                        0,
                                        mbYes ) then
               begin
                    Valor := ValoresGrid;
                    FBaseReportes_DevEx.ExportarGrid( TDBGrid( Components[ i ] ),
                                                TDBGrid( Components[ i ] ).DataSource.DataSet,
                                                Caption,
                                                'IM',
                                                Valor[0],
                                                Valor[1],
                                                Valor[2],
                                                Valor[3] );

               end;
               Break;
          end;
     end;
     if not lExporta then
        zInformation( 'Exportar...',Format( 'La Pantalla %s no Contiene Ning�n Grid para Exportar',[Caption] ), 0 );
end;}

 //DevEx(by am): Se modifica el metodo de exportar para que peuda utilizar el nuevo Grid.
 procedure TBaseEdicion_DevEx.Exportar;
 var
    lExporta: Boolean;
    i: integer;
    Valor : Variant;
   {$IFNDEF TRESS_DELPHIXE5_UP}
    //DevEx
    oGrid: TDBGrid;
   {$ENDIF}
begin
     lExporta:= FALSE;
     for i := 0 to ( ComponentCount - 1 ) do
     begin
          if ( (Components[ i ] is TDBGrid) or ( Components[ i ] is TcxGridDBTableView ) ) then
          begin
               lExporta:= TRUE;
               if zConfirm( 'Exportar...', '� Desea exportar a excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
               begin
                    Valor := ValoresGrid;
                    if (Components[ i ] is TDBGrid) then
                       {$IFDEF TRESS_DELPHIXE5_UP}ZImprimeGrid{$ELSE}FBaseReportes{$ENDIF}.ExportarGrid( TDBGrid( Components[ i ] ),
                                                                                                         TDBGrid( Components[ i ] ).DataSource.DataSet, Caption, 'IM',
                                                                                                         Valor[0],Valor[1],Valor[2],Valor[3])
                    else
                    begin
                         {$IFDEF TRESS_DELPHIXE5_UP}
                         ZImprimeGrid.ExportarGrid(  ( Components[ i ] as TcxGridDBTableView ),
                                                     (Components[ i ] as TcxGridDBTableView).DataController.DataSource.DataSet, Caption, 'IM',
                                                     Valor[0],Valor[1],Valor[2],Valor[3]);
                         {$ELSE}
                         oGrid := ConvierteATDBGrid ( ( Components[ i ] as TcxGridDBTableView ) );
                         try
                            FBaseReportes.ExportarGrid( oGrid, oGrid.DataSource.DataSet, Caption, 'IM',
                                                              Valor[0],Valor[1],Valor[2],Valor[3]);
                         finally
                                oGrid.Free;
                         end;
                         {$ENDIF}
                    end;
               end;
               Break;
          end;
     end;
     if not lExporta then
        zInformation( 'Exportar...',Format( 'La pantalla %s no contiene ning�n grid para exportar', [Caption] ), 0);
end;

//DevEx: Botones toolbar
procedure TBaseEdicion_DevEx.dxBarButton_AgregarBtnClick(Sender: TObject);
begin
  inherited;
  DoInsert;
end;

procedure TBaseEdicion_DevEx.dxBarButton_BorrarBtnClick(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TBaseEdicion_DevEx.dxBarButton_ModificarBtnClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
  FocusFirstControl;
end;

procedure TBaseEdicion_DevEx.dxBarButton_BuscarBtnClick(Sender: TObject);
begin
  inherited;
  DoLookup;
end;

procedure TBaseEdicion_DevEx.dxBarButton_ImprimirBtnClick(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TBaseEdicion_DevEx.dxBarButton_ImprimirFormaBtnClick(
  Sender: TObject);
begin
  inherited;
  DoPrintForma;
end;

procedure TBaseEdicion_DevEx.dxBarButton_ExportarBtnClick(Sender: TObject);
begin
  inherited;
  DoExportar;
end;

procedure TBaseEdicion_DevEx.dxBarButton_CortarBtnClick(Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_CUT, 0, 0 );
end;

procedure TBaseEdicion_DevEx.dxBarButton_CopiarBtnClick(Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_COPY, 0, 0 );
end;

procedure TBaseEdicion_DevEx.dxBarButton_PegarBtnClick(Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_PASTE, 0, 0 );
end;

procedure TBaseEdicion_DevEx.dxBarButton_UndoBtnClick(Sender: TObject);
begin
  inherited;
  SendMessage( ActiveControl.Handle, WM_UNDO, 0, 0 );
end;

//DevEx (by am): Metodo agregado para que no no salga ningun pop up  al dar clic derecho sobre los toolbar de las formas de edicion.
procedure TBaseEdicion_DevEx.DevEx_BarManagerEdicionShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
  inherited;
  Abort;
end;

end.
