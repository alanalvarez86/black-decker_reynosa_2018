unit FActivaCache;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl, ComCtrls,
     ZBaseDlgModal;

type
  TActivarCache = class(TZetaDlgModal)
    GB_Actualiza: TGroupBox;
    SB_Refrescar: TSpeedButton;
    lblArchivo: TStaticText;
    BarraProgreso: TProgressBar;
    LblModule: TStaticText;
    LblRefrescar: TStaticText;
    CB_Sistema: TCheckBox;
    GB_Sistema: TGroupBox;
    LblSistema: TLabel;
    FolderSistema: TEdit;
    BuscarFolderSistema: TSpeedButton;
    procedure SB_RefrescarClick(Sender: TObject);
    procedure BuscarFolderSistemaClick(Sender: TObject);
    procedure CB_SistemaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  published
    { Published declarations }
    CB_Activar: TCheckBox;
    LblFolder: TLabel;
    Folder: TEdit;
    BuscarFolder: TSpeedButton;
    procedure BuscarFolderClick(Sender: TObject);
    procedure CB_ActivarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FolderChange(Sender: TObject);
  private
    { Private declarations }
    FHuboCambios: Boolean;
    procedure SetHuboCambios( const Value : Boolean );
    property HuboCambios: Boolean read FHuboCambios write SetHuboCambios;
    procedure ChecaHuboCambios;
    procedure LeerRegistry;
    procedure SetControles;
    procedure SetControlesActivar(lEnabled: Boolean);
    procedure RefrescaLookUpDataSets;
    function CreaDirectorios(var sError: String): Boolean;
    procedure RefrescarDataSets;
    procedure SetGB_Sistema(const lEnabled: Boolean);
  public
    { Public declarations }
  end;

var
  ActivarCache: TActivarCache;

implementation

uses DCliente,
     DSistema,
     ZetaClientDataSet,
     ZetaRegistryCliente,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

function BuscarDirectorio( const Value: String ): String;
var
   sDirectory: String;
begin
     sDirectory := Value;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        Result := sDirectory
     else
        Result := Value;
end;

{$R *.DFM}

procedure TActivarCache.FormShow(Sender: TObject);
begin
     inherited;
     LeerRegistry;
end;

procedure TActivarCache.CB_ActivarClick(Sender: TObject);
begin
     SetControlesActivar( CB_Activar.Checked );
     ChecaHuboCambios;
end;

procedure TActivarCache.BuscarFolderClick(Sender: TObject);
begin
     Folder.Text := BuscarDirectorio( Folder.Text );
end;

procedure TActivarCache.BuscarFolderSistemaClick(Sender: TObject);
begin
     inherited;
     FolderSistema.Text := BuscarDirectorio( FolderSistema.Text );
end;

procedure TActivarCache.FolderChange(Sender: TObject);
begin
     ChecaHuboCambios;
end;

procedure TActivarCache.CB_SistemaClick(Sender: TObject);
begin
     SetGB_Sistema( CB_Sistema.Checked );
     ChecaHuboCambios;
end;

procedure TActivarCache.SB_RefrescarClick(Sender: TObject);
begin
     RefrescarDataSets;
end;

procedure TActivarCache.OKClick(Sender: TObject);
var
   lOk: Boolean;
   sError : String;
begin
     with ClientRegistry do
     begin
          lOk := FALSE;
          if ( not CB_Activar.Checked ) then
          begin
               CacheActive := FALSE;
               lOk := TRUE;
          end
          else
          begin
               if CB_Activar.Checked and CreaDirectorios( sError ) then
               begin
                    CacheActive := CB_Activar.Checked;
                    CacheFolder := Folder.Text;
                    CacheFecha := '';
                    CacheSistemaActive := CB_Sistema.Checked;
                    CacheSistemaFolder := FolderSistema.Text;
                    lOk := TRUE;
               end
               else
                   ZetaDialogo.zError( Self.Caption, sError, 0 );
          end;
          if lOk then
          begin
               dmCliente.SetCacheLookup;
               if CacheActive then
               begin
                    OK.Enabled := FALSE;
                    RefrescarDataSets;
                    OK.Enabled := TRUE;
               end;
               LeerRegistry;
               Cancelar.SetFocus;
          end;
     end;
end;

procedure TActivarCache.CancelarClick(Sender: TObject);
begin
     if HuboCambios then
        LeerRegistry
     else
        Close;
end;

function TActivarCache.CreaDirectorios( var sError: String ): Boolean;

   function VerificaDirectorio( FControl: TEdit ): Boolean;
   var
      sFolder: String;
   begin
        sFolder:= FControl.Text;
        case Windows.GetDriveType( PChar( ExtractFileDrive( sFolder ) ) ) of
             DRIVE_REMOVABLE: { The drive can be removed from the drive }
             begin
                  Result := False;
                  sError := 'No Se Permiten Discos Removibles';
             end;
             DRIVE_FIXED: Result := True; {	The disk cannot be removed from the drive }
             DRIVE_REMOTE: {	The drive is a remote (network) drive }
             begin
                  Result := False;
                  sError := 'No Se Permiten Directorios De Red';
             end;
             DRIVE_CDROM: {	The drive is a CD-ROM drive }
             begin
                  Result := False;
                  sError := 'No Se Permiten CD-ROM o Discos S�lo De Lectura';
             end;
             DRIVE_RAMDISK: {	The drive is a RAM disk }
             begin
                  Result := False;
                  sError := 'No Se Permiten Discos de Memoria RAM';
             end;
        else
            Result := True;
        end;
        if Result then
        begin
             Result := DirectoryExists( sFolder );
             if not Result then
             begin
                  Result := CreateDir( sFolder );
                  if not Result then
                  begin
                       sError := 'No Se Pudo Crear el Directorio : ' + sFolder;
                       FControl.SetFocus;
                  end;
             end;
        end;
   end;

begin
     Result := VerificaDirectorio( Folder );
     if Result then
        Result := ( not CB_Sistema.Checked ) or ( VerificaDirectorio( FolderSistema ) );
end;

procedure TActivarCache.RefrescarDataSets;
var
   oCursor: TCursor;

   procedure VerAvance( const lEnabled: Boolean );
   begin
        LblModule.Visible := lEnabled;
        LblArchivo.Visible := lEnabled;
        BarraProgreso.Visible := lEnabled;
        CB_Activar.Enabled := not lEnabled;
        Folder.Enabled := not lEnabled;
        BuscarFolder.Enabled := not lEnabled;
        CB_Sistema.Enabled := not lEnabled;
        FolderSistema.Enabled := not lEnabled;
        BuscarFolderSistema.Enabled := not lEnabled;
        Cancelar.Enabled := not lEnabled;
        SB_Refrescar.Enabled := not lEnabled;
   end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        VerAvance( TRUE );
        with ClientRegistry do
        begin
             RefrescaLookUpDataSets;
             CacheFecha := FechaLarga( Date ) + ' - ' + TimeToStr( Time );
             LblRefrescar.Caption := CacheFecha;
        end;
     finally
            VerAvance( FALSE );
            Screen.Cursor := oCursor;
     end;
end;

procedure TActivarCache.RefrescaLookUpDataSets;
var
   i : Integer;

   procedure RefrescaDataModule( DataModule: TDataModule; const sModulo: String );
   var
      i: Integer;
   begin
        with DataModule do
        begin
             LblModule.Caption := 'Refrescando ' + sModulo + ':';
             for i := 0 to ( ComponentCount - 1 ) do
             begin
                  if Components[ i ] is TZetaLookupDataSet then
                  begin
                       with TZetaLookupDataSet( Components[ i ] ) do
                       begin
                            LblArchivo.Caption := LookupName;
                            if UsaCache then
                               Refrescar;
                       end;
                  end
                  else
                      if Components[ i ] is TZetaClientDataSet then
                      begin
                           with TZetaClientDataSet( Components[ i ] ) do
                           begin
                                LblArchivo.Caption := Name;
                                if UsaCache then
                                   Refrescar;
                           end;
                      end;
                  BarraProgreso.StepIt;
                  Application.ProcessMessages;
             end;
        end;
   end;

begin
     with BarraProgreso do
     begin
          Position := 0;
          Max := dmCliente.GetCacheCount;
{         Max := dmTablas.ComponentCount +
                 dmCatalogos.ComponentCount +
                 dmDiccionario.ComponentCount; }
          if CB_Sistema.Checked then
             Max := Max + dmSistema.ComponentCount;
          for i := 0 to dmCliente.DataCache.Count - 1 do
              RefrescaDataModule( TDataModule( dmCliente.DataCache.Objects[i] ),
                                  dmCliente.DataCache[i] );
{          RefrescaDatamodule( dmTablas, 'Tabla' );
          RefrescaDatamodule( dmCatalogos, 'Cat�logo' );
          RefrescaDatamodule( dmDiccionario, 'Diccionario' ); }
          if CB_Sistema.Checked then
             RefrescaDatamodule( dmSistema, 'Tabla de Sistema' );
          Position := Max;
     end;
end;

procedure TActivarCache.ChecaHuboCambios;
begin
     with ClientRegistry do
     begin
          HuboCambios := ( ( CB_Activar.Checked <> CacheActive ) or
                           ( Folder.Text <> CacheFolder ) or
                           ( CB_Sistema.Checked <> CacheSistemaActive ) or
                           ( FolderSistema.Text <> CacheSistemaFolder ) );
     end;
end;

procedure TActivarCache.LeerRegistry;
begin
     with ClientRegistry do
     begin
          CB_Activar.Checked := CacheActive;
          Folder.Text := CacheFolder;
          LblRefrescar.Caption := CacheFecha;
          CB_Sistema.Checked := CacheSistemaActive;
          FolderSistema.Text := CacheSistemaFolder;
     end;
     SetControlesActivar( CB_Activar.Checked );
     SetControles;
     ChecaHuboCambios;
end;

procedure TActivarCache.SetHuboCambios(const Value: Boolean);
begin
     if ( FHuboCambios <> Value ) then
     begin
          FHuboCambios:= Value;
          SetControles;
     end;
end;

procedure TActivarCache.SetControles;
begin
     SB_Refrescar.Enabled := not HuboCambios and CB_Activar.Checked;
     OK.Enabled := HuboCambios;
     if HuboCambios then
     begin
          with Cancelar do
          begin
               Kind := bkCancel;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar do
          begin
               Kind := bkClose;
               ModalResult := mrNone;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
          end;
     end;
     Application.ProcessMessages;
end;

procedure TActivarCache.SetControlesActivar(lEnabled: Boolean);
begin
     if lEnabled and StrVacio( Folder.Text ) then
        Folder.Text := ExtractFileDir( Application.ExeName ) + '\' + dmCliente.Compania;
     LblFolder.Enabled := lEnabled;
     Folder.Enabled := lEnabled;
     BuscarFolder.Enabled := lEnabled;
     CB_Sistema.Enabled := lEnabled;
     SetGB_Sistema( lEnabled and CB_Sistema.Checked );
     Application.ProcessMessages;
end;

procedure TActivarCache.SetGB_Sistema( const lEnabled: Boolean );
begin
     LblSistema.Enabled := lEnabled;
     FolderSistema.Enabled := lEnabled;
     BuscarFolderSistema.Enabled := lEnabled;
     if lEnabled and StrVacio( FolderSistema.Text ) then
        FolderSistema.Text := ExtractFileDir( Application.ExeName ) + '\Sistema';
end;

procedure TActivarCache.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H80817_Archivos_temporales;
     dmCliente.InitDataCache;
end;

end.
