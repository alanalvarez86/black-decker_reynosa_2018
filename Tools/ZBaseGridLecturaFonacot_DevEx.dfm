inherited BaseGridLecturaFonacot_DevEx: TBaseGridLecturaFonacot_DevEx
  Left = 691
  Top = 436
  ClientHeight = 230
  ClientWidth = 640
  ExplicitWidth = 640
  ExplicitHeight = 230
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 640
    ExplicitWidth = 640
    inherited ValorActivo1: TPanel
      inherited textoValorActivo1: TLabel
        Width = 234
      end
    end
    inherited ValorActivo2: TPanel
      Width = 381
      ExplicitWidth = 381
      inherited textoValorActivo2: TLabel
        Left = 3
        Width = 375
        ExplicitLeft = 295
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 49
    Width = 640
    Height = 181
    ExplicitTop = 49
    ExplicitWidth = 640
    ExplicitHeight = 181
  end
  object IMSSPanel: TPanel [2]
    Left = 0
    Top = 19
    Width = 640
    Height = 30
    Align = alTop
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 2
    object IMSSMesLBL: TLabel
      Left = 275
      Top = 8
      Width = 23
      Height = 13
      Caption = 'Mes:'
    end
    object IMSSYearLBL: TLabel
      Left = 388
      Top = 8
      Width = 22
      Height = 13
      Caption = 'A'#241'o:'
    end
    object IMSSPatronLBL: TLabel
      Left = 5
      Top = 8
      Width = 66
      Height = 13
      Caption = 'Raz'#243'n Social:'
    end
    object IMSSTipoLBL: TLabel
      Left = 494
      Top = 8
      Width = 24
      Height = 13
      Caption = 'Tipo:'
    end
    object IMSSMesCB: TStateComboBox
      Tag = 121
      Left = 300
      Top = 4
      Width = 82
      Height = 21
      Hint = 'Mes Activo Para el IMSS'
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      DropDownCount = 12
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Items.Strings = (
        'Enero'
        'Febrero'
        'Marzo'
        'Abril'
        'Mayo'
        'Junio'
        'Julio'
        'Agosto'
        'Septiembre'
        'Octubre'
        'Noviembre'
        'Diciembre')
      ListaFija = lfMeses
      ListaVariable = lvPuesto
      EsconderVacios = False
      LlaveNumerica = True
      MaxItems = 12
      Offset = 1
      OnLookUp = IMSSMesCBLookUp
    end
    object IMSSAnioCB: TStateComboBox
      Tag = 121
      Left = 413
      Top = 4
      Width = 58
      Height = 21
      Hint = 'A'#241'o Activo Para El IMSS'
      AutoComplete = False
      BevelKind = bkFlat
      Ctl3D = False
      DropDownCount = 0
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      EsconderVacios = False
      LlaveNumerica = True
      MaxItems = 10
      Offset = 1994
      OnLookUp = IMSSAnioCBLookUp
    end
    object IMSSPatronCB: TStateComboBox
      Tag = 121
      Left = 78
      Top = 4
      Width = 195
      Height = 21
      Hint = 'Raz'#243'n Social'
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      DropDownCount = 0
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      EsconderVacios = False
      LlaveNumerica = False
      MaxItems = 10
      Offset = 0
      OnLookUp = IMSSPatronCBLookUp
    end
    object IMSSTipoCB: TStateComboBox
      Tag = 121
      Left = 521
      Top = 4
      Width = 96
      Height = 21
      Hint = 'Tipo de Liquidaci'#243'n Activa para el IMSS'
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      DropDownCount = 0
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      ListaFija = lfTipoLiqIMSS
      ListaVariable = lvPuesto
      EsconderVacios = False
      LlaveNumerica = True
      MaxItems = 10
      Offset = 0
      OnLookUp = IMSSTipoCBLookUp
    end
    object IMSSYearUpDown: TUpDown
      Left = 472
      Top = 2
      Width = 16
      Height = 24
      Hint = 'Cambiar a'#241'o'
      Max = 10000
      ParentShowHint = False
      Position = 5000
      ShowHint = True
      TabOrder = 4
      OnClick = IMSSYearUpDownClick
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
