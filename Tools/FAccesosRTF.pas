unit FAccesosRTF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ToolWin, ExtCtrls, Buttons;

type
  TAccesosRTF = class(TForm)
    Panel1: TPanel;
    RTF: TRichEdit;
  private
    { Private declarations }
    function ObtenerEstado(oNodo: TTreeNode): String;
  public
    { Public declarations }
    procedure ObtenerArbol(oArbol: TTreeView; sGrupo, sCompany: String);
  end;

const
  K_SIN_DERECHO = 'N';
  K_CON_DERECHO = 'S';

var
  AccesosRTF: TAccesosRTF;

implementation
uses Printers, ZetaDialogo, ZetaCommonTools;

{$R *.DFM}


procedure TAccesosRTF.ObtenerArbol(oArbol: TTreeView; sGrupo, sCompany: String);
          function ObtenerEspacios( sTitulo: String; iMenos: Integer ): string;
          var
             iLongitud, i: Integer;
             sEspacios: String;
          begin
               sEspacios := '';
               iLongitud := Length( sTitulo );
               iLongitud := iLongitud - iMenos;
               for i:=1 to (iLongitud - 1) do
               begin
                    sEspacios := sEspacios + ' ';
               end;
               Result := sEspacios;
          end;
const
     K_IDENT = 5;
var
   oNodo, oNodoAnt, oNodoSig, oNodoAux, oNodoAbuelo, oNodoAntSib: TTreeNode;
   oCursor: TCursor;
   sValor, sValorAnt, sBlcPadre, sBlcHijo, sBlcNieto: String;
   sTextoPadre, sTextoHijo, sTextoNieto, sTextoBisNieto, sSinDerecho: String;
begin
     with RTF do
     begin
          Paragraph.Alignment := taCenter;
          Lines.Insert( 0, Format( 'Derechos De Acceso Para %s', [ sGrupo ] ) );
          Lines.Insert( 1, 'Empresa: ' + sCompany );
     end;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             oNodo := Items[0];
             while (oNodo <> nil) do
             begin
                  sValor := ObtenerEstado ( oNodo );
                       with RTF do
                       begin
                            if ( sValor = K_CON_DERECHO ) then
                            begin
                                 sSinDerecho := '';
                                 SelAttributes.Style := [];
                                 SelAttributes.Color := clBlack;
                            end
                            else
                            begin
                                 sSinDerecho := 'XX ';
                                 //SelAttributes.Style := [fsStrikeout];
                                 SelAttributes.Color := clRed;
                            end;
                            Case oNodo.Level of
                                 0:begin
                                        if ( sValor = K_SIN_DERECHO ) then
                                             SelAttributes.Color := clRed;
                                        Lines.Add(' ');
                                        Paragraph.FirstIndent := K_IDENT;
                                        if ( sValor = K_SIN_DERECHO ) then
                                             SelAttributes.Color := clRed;
                                        sTextoPadre := '�->'+sSinDerecho+oNodo.Text+'|';
                                        Lines.Add(sTextoPadre);
                                   end;
                                 1:begin
                                        sBlcPadre := ObtenerEspacios( sTextoPadre,0 );
                                        if oNodo.HasChildren then
                                             sTextoHijo := '|-'+sSinDerecho+oNodo.Text+'|'
                                        else
                                             sTextoHijo := '|-'+sSinDerecho+oNodo.Text;
                                        Lines.Add(sBlcPadre + sTextoHijo);
                                   end;
                                 2:begin
                                        sBlcHijo := ObtenerEspacios( sTextoHijo,1 );
                                        oNodoAux := oNodo;
                                        oNodoAnt := oNodoAux.Parent;
                                        oNodoSig := oNodoAnt.getNextSibling;
                                        if ( oNodoSig <> nil ) then
                                        begin
                                             oNodo := oNodoAux;
                                             if oNodo.HasChildren then
                                                 sTextoNieto := sBlcPadre+'|'+sBlcHijo+'|-'+sSinDerecho+oNodo.Text+'|'
                                             else
                                                 sTextoNieto := sBlcPadre+'|'+sBlcHijo+'|-'+sSinDerecho+oNodo.Text;
                                        end
                                        else
                                        begin
                                             sBlcHijo := sBlcHijo + ' ';
                                             oNodo := oNodoAux;
                                             sTextoNieto := sBlcPadre+sBlcHijo+'|-'+sSinDerecho+oNodo.Text;
                                        end;
                                        Lines.Add(sTextoNieto);
                                   end;
                                 3:begin
                                        oNodoAux := oNodo;
                                        oNodoAnt := oNodoAux.Parent;
                                        sValorAnt := ObtenerEstado(oNodoAnt);
                                        if sValorAnt = K_CON_DERECHO then
                                            sBlcNieto := ObtenerEspacios( oNodoAnt.Text,0 )
                                        else
                                            sBlcNieto := ObtenerEspacios( oNodoAnt.Text+sSinDerecho,0 );
                                        sBlcnieto := sBlcNieto +'  ';
                                        oNodoAbuelo := oNodoAnt.Parent;
                                        oNodoSig := oNodoAbuelo.getNextSibling;
                                        if ( oNodoSig <> nil ) then
                                        begin
                                             oNodoAntSib := oNodoAnt.getNextSibling;
                                             if ( oNodoAntSib = nil ) then
                                             begin
                                                  sBlcNieto := sBlcNieto + ' ';
                                                  oNodo := oNodoAux;
                                                  sTextoBisNieto := sBlcPadre+'|'+sBlcHijo+sBlcNieto+'|-'+sSinDerecho+oNodo.Text;
                                             end
                                             else
                                             begin
                                                  oNodo := oNodoAux;
                                                  sTextoBisNieto := sBlcPadre+'|'+sBlcHijo+'|'+sBlcNieto+'|-'+sSinDerecho+oNodo.Text
                                             end;
                                        end
                                        else
                                        begin
                                             oNodoAntSib := oNodoAnt.getNextSibling;
                                             if ( oNodoAntSib <> nil ) then
                                             begin
                                                  oNodo := oNodoAux;
                                                  sTextoBisNieto := sBlcPadre+sBlcHijo+'|'+sBlcNieto+'|-'+sSinDerecho+oNodo.Text;
                                             end
                                             else
                                             begin
                                                  oNodo := oNodoAux;
                                                  sTextoBisNieto := sBlcPadre+sBlcHijo+sBlcNieto+'|-'+sSinDerecho+oNodo.Text;
                                             end;
                                        end;
                                        Lines.Add(sTextoBisNieto);
                                   end;
                             else
                                 ZetaDialogo.ZInformation('Warnning','Existe Un Nodo Que Es De Un Nivel Mayor',0);
                            end;
                       end;
             oNodo := oNodo.GetNext;
             end;
             RTF.SelStart :=0;
             RTF.SelLength := 0;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TAccesosRTF.ObtenerEstado(oNodo: TTreeNode): String;
begin
     with oNodo do
     begin
          if ( StateIndex = 1 ) then
              Result := K_SIN_DERECHO;
          if ( StateIndex = 2 ) then
              Result := K_CON_DERECHO;
     end;
end;

end.
