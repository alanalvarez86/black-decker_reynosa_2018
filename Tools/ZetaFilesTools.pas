unit ZetaFilesTools;

interface
uses
    Controls, SysUtils, DB, Classes,
    Windows, ShellAPI, ShlObj,
    ZetaClientDataSet;


{Constante para nombrar archivos temporales}
const    K_PREFIJO_TEMP = '~~T';
         OFASI_EDIT = $0001;
        OFASI_OPENDESKTOP = $0002;

procedure AbreDocumento( DataSet : TZetaClientDataSet; const sBlobField, sExtField : string );
procedure BorraArchivosTemporales;
function CargaDocumento( DataSet : TDataSet; const sPath, sBlobField, sExtField : string ): Boolean;
{$ifdef ICUMEDICAL_CURSOS}
function CargaDocumentoICU( DataSet: TDataSet; const sPath, sExtField : string ): Boolean;
procedure AbreDocumentoICU( DataSet: TZetaClientDataSet; const sRuta : string );
{$endif}

function GuardaDocumento( DataSet : TDataSet; const sPath, sBlobField, sExtField : string ): Boolean;
function GetTipoDocumento( const sExtension : string ): string;
function AbreArchivoPrograma( sFileName : string ) : boolean;
function  AbreArchivoEnExplorer( sFileName : string ) : boolean;

{$IFDEF UNICODE}
function ILCreateFromPath(pszPath: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}): PItemIDList stdcall; external shell32
  name 'ILCreateFromPathW';
{$ELSE}
function ILCreateFromPath(pszPath: PChar): PItemIDList stdcall; external shell32
  name 'ILCreateFromPathA';
{$ENDIF}
procedure ILFree(pidl: PItemIDList) stdcall; external shell32;
function SHOpenFolderAndSelectItems(pidlFolder: PItemIDList; cidl: Cardinal;
  apidl: pointer; dwFlags: DWORD): HRESULT; stdcall; external shell32;

implementation
uses
    ZetaCommonTools,
    ZetaClientTools,
    ZetaDialogo,
    Dialogs;


procedure BorraArchivosTemporales;
 var aDir : array[0..255] of char;
     FArchivo: TSearchRec;
begin
     try
        GetTempPath( 255, aDir );
        if FindFirst( aDir + K_PREFIJO_TEMP + '*.*', faArchive, FArchivo ) = 0 then
        begin
             DeleteFile( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( aDir+ FArchivo.Name ) );
             while FindNext( FArchivo) = 0 do
                   DeleteFile( {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( aDir + FArchivo.Name ) )
        end;
     finally
            SysUtils.FindClose( FArchivo );
     end;
end;

procedure AbreDocumento( DataSet : TZetaClientDataSet; const sBlobField, sExtField : string );
var
   sArchivo : string;
begin
     with Dataset do
     begin
          with TBlobField( FieldByName( sBlobField ) ) do
          begin
               if NOT IsNull then
               begin
                    sArchivo := GetTempFile( K_PREFIJO_TEMP, FieldByName( sExtField ).AsString );
                    if StrLleno( sArchivo ) then
                    begin
                         //CV: Nota: el archivo temporal que se esta generando en este momento,
                         //no se puede borrar en este metodo, porque, si se borra, las aplicaciones
                         //que no abren de forma exclusiva los archivos, se cuatrapean.
                         SaveTofile( sArchivo );
                         try
                            ExecuteFile( sArchivo, '', '', SW_SHOWDEFAULT );
                         except
                               On E:Exception do
                                  ZetaDialogo.ZError('Problemas al Abrir Documento', 'El Documento no Contiene Informaci�n', 0 );
                         end;
                    end;
               end
               else
                  ZetaDialogo.ZInformation( 'Problemas al Abrir Documento', 'El Documento no Contiene Informaci�n', 0 );
          end;
     end;
end;

procedure AbreDocumentoICU( DataSet: TZetaClientDataSet; const sRuta : string );
begin
     ExecuteFile( sRuta, '', '', SW_SHOWDEFAULT );
end;

function CargaDocumento( DataSet : TDataSet; const sPath, sBlobField, sExtField : string ): Boolean;
var
   oFieldBlob : TBlobField;
   oArchivo : TMemoryStream;
begin
     {AL llegar a este punto el dataset debe de estar en modo de insercion o edicion,
     de lo contrario va a marcar error }
     Result := FALSE;
     with DataSet do
     begin
          if StrLleno( sPath ) AND
             FieldByName( sBlobField ).IsBlob then
          begin
               if FileExists( sPath ) then
               begin
                    FieldByName( sExtField ).AsString := UpperCase( Copy( ExtractFileExt( sPath ), 2 , 255 ) );
                    oFieldBlob := TBlobField( FieldByName( sBlobField ) );

                    oArchivo := TMemoryStream.Create;
                    try
                       oArchivo.LoadFromFile( sPath );

                       oFieldBlob.LoadFromStream( oArchivo ) ;
                       Result := TRUE;
                    finally
                           FreeAndNil( oArchivo );
                    end;
               end
               else
                   ZetaDialogo.ZError('Agregando Documento', Format('El Archivo %s no Existe ', [sPath]), 0 );
          end;
     end;
end;

{$ifdef ICUMEDICAL_CURSOS}
function CargaDocumentoICU( DataSet: TDataSet; const sPath, sExtField : string ): Boolean;
begin
     Result := False;
     with DataSet do
     begin
          if StrLleno( sPath ) then
          begin
               if FileExists( sPath ) then
               begin
                    FieldByName( sExtField ).AsString := UpperCase( Copy( ExtractFileExt( sPath ), 2, 255 ) );
                    FieldByName( 'SE_D_RUTA' ).AsString := sPath;
                    Result := True;
               end
               else
                   ZetaDialogo.ZError( 'Agregando Documento', Format( 'El archivo %s no existe', [sPath] ), 0 );
          end;
     end;
end;
{$endif}

function GuardaDocumento( DataSet : TDataSet; const sPath, sBlobField, sExtField : string ): Boolean;
var
   sArchivo : string;
   lExistia : boolean;
begin
     Result := False;
     with Dataset do
     begin
          with TBlobField( FieldByName( sBlobField ) ) do
          begin
               if NOT IsNull then
               begin
                    sArchivo := sPath;
                    if StrLleno( sArchivo ) then
                    begin
                         try
                              lExistia := FileExists( sArchivo );
                              if ( lExistia ) then
                              begin
                                   if ( ZetaDialogo.ZConfirm('Documento existente.','�Desea que se reemplace el documento?', 0, mbNo) ) then
                                   begin
                                        SaveTofile( sArchivo );
                                   end;
                              end
                              else
                                   SaveTofile( sArchivo );

                              Result := FileExists( sArchivo );
                         except
                               On E:Exception do
                                  ZetaDialogo.ZError('Problemas al Guardar Documento', E.Message, 0 );
                         end;
                    end;
               end
               else
                  ZetaDialogo.ZInformation( 'Problemas al Guardar Documento', 'El Documento no Contiene Informaci�n', 0 );
          end;
     end;
end;

function GetTipoDocumento( const sExtension : string ): string;
 var
    aInfo: TSHFileInfo;
    oFile : TFileStream;
    sArchivo : {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
begin
     Result := 'Documento Vac�o';
     sArchivo := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( GetTempFile( K_PREFIJO_TEMP, sExtension ) );
     oFile := TFileStream.Create( sArchivo,fmCreate );
     try
        if SHGetFileInfo( sArchivo, 0, aInfo, Sizeof( aInfo ), SHGFI_TYPENAME ) <> 0 then
        begin
             Result := StrPas( aInfo.szTypeName );
             if StrVacio( Result ) then
                Result := 'Archivo de ' + sExtension;
        end
        else
            Result := 'Archivo Indefinido'
     finally
            oFile.Free;
            DeleteFile(sArchivo);
     end;
end;


function AbreArchivoPrograma(  sFileName: string): boolean;
begin
 try
     ExecuteFile( sFileName, '', '', SW_SHOWDEFAULT );
     Result := True;
 except
       On E:Exception do
       begin
          ZetaDialogo.ZError('Problemas al Abrir Archivo', Format( 'El Archivo %s no Contiene Informaci�n', [sFileName] ) , 0 );
          Result := False;
       end
 end;
end;


function  AbreArchivoEnExplorer( sFileName : string ) : boolean;
var
  IIDL: PItemIDList;
begin
  result := false;
  IIDL := ILCreateFromPath({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFileName));
  if IIDL <> nil then
    try
      result := SHOpenFolderAndSelectItems(IIDL, 0, nil, 0) = S_OK;
    finally
      ILFree(IIDL);
    end;
end;

end.

