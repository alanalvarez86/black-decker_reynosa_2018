unit SFE;

interface

uses Windows, SysUtils;

var Open : function(strDBFileName : PWideChar; nSensorType : LongInt; nSensorBrAdjust : LongInt) : LongInt cdecl;
var Close : function() : LongInt cdecl;
var GetEnrollCount : function() : LongInt cdecl;
var Delete : function(nID : LongInt; nFingerNum : LongInt) : LongInt cdecl;
var DeleteID : function(nID : LongInt) : LongInt cdecl;
var DeleteAll : function() : LongInt cdecl;
var SearchID : function(var pnID : LongInt) : LongInt cdecl;
var SearchFingerNum : function(nID : LongInt; var pnFingerNum : LongInt) : LongInt cdecl;
var CheckID : function(nID : LongInt) : LongInt cdecl;
var CheckFingerNum : function(nID : LongInt; nFingerNum : LongInt) : LongInt cdecl;
var CheckManager : function(nID : LongInt; nFingerNum : LongInt) : LongInt cdecl;
var CheckManagerID : function(nID : LongInt; var pnFingerNum : LongInt) : LongInt cdecl;
var AdjustSensor : function() : LongInt cdecl;
var Capture : function() : LongInt cdecl;
var IsFinger : function() : LongInt cdecl;
var GetImage : function(pImage : PByte) : LongInt cdecl;
var SetImage : function(pImage : PByte) : LongInt cdecl;
var EnrollStart : function() : LongInt cdecl;
var EnrollNth : function(nNth : LongInt) : LongInt cdecl;
var EnrollEnd : function(nID : LongInt; nFingerNum : LongInt; Manager : LongInt) : LongInt cdecl;
var Identify : function(var pnID : LongInt; var pnFingerNum : LongInt) : LongInt cdecl;
var Verify : function(nID : LongInt; nFingerNum : LongInt) : LongInt cdecl;
var VerifyID : function(nID : LongInt; var pnFingerNum : LongInt) : LongInt cdecl;
var TemplateGetFromImage : function(pTemplate : PByte) : LongInt cdecl;
var TemplateGetFromDB : function(nID : LongInt; nFingerNum : LongInt; pTemplate : PByte) : LongInt cdecl;
var TemplateEnroll : function(nID : LongInt; nFingerNum : LongInt; Manager : LongInt; pTemplate : PByte) : LongInt cdecl;
var TemplateIdentify : function(var pnID : LongInt; var pnFingerNum : LongInt; pTemplate : PByte) : LongInt cdecl;
var TemplateVerify : function(nID : LongInt; nFingerNum : LongInt; pTemplate : PByte) : LongInt cdecl;
var TemplateVerifyID : function(nID : LongInt; var pnFingerNum : LongInt; pTemplate : PByte) : LongInt cdecl;
var LoadImageFromFile : function(strImageFileName : PWideChar; pImage : PByte) : LongInt cdecl;
var SaveImageToFile : function(strImageFileName : PWideChar; pImage : PByte) : LongInt cdecl;
var LoadTemplateFromFile : function(strTemplFileName : PWideChar; pTemplate : PByte) : LongInt cdecl;
var SaveTemplateToFile : function(strTemplFileName : PWideChar; pTemplate : PByte) : LongInt cdecl;
var LedControl : function(nType : LongInt; nCount : LongInt; nInterval : LongInt) : LongInt cdecl;
var GetFpQuality : function(pImage : PByte) : LongInt cdecl;
var ConvertImage : function(pDstImage : PByte; pSrcImage : PByte; srcWidth : LongInt; srcHeight : LongInt; srcDPI : LongInt) : LongInt cdecl;

function GetWinSysDir: String;
function LoadDLL: boolean;

implementation
var
  DLLHandle: THandle;

function GetWinSysDir: String;
var
   Path: array[ 0..260 ] of Char;
begin
     GetSystemDirectory( Path, SizeOf( Path ) );
     Result := Path;
     if ( Result[ Length( Result ) ] = '\' ) then
        SetLength( Result, ( Length( Result ) - 1 ) );
end;

function LoadDLL: boolean;
var
   sDrivers: string;
begin
     sDrivers := {$IFDEF WIN32} GetWinSysDir + '\' + 'SFEMediator.dll'; {$ENDIF} {$IFDEF WIN64} GetWinSysDir + '\' + 'SFEMediator64.dll'; {$ENDIF}
     Result := ( ( sDrivers <> '' ) and FileExists( sDrivers ) );
     if Result then
     begin
          DLLHandle := LoadLibrary( PChar( sDrivers ));
          @Open                 := GetProcAddress(DLLHandle, 'sfem_Open');
          @Close							  := GetProcAddress(DLLHandle, 'sfem_Close');
          @GetEnrollCount				:= GetProcAddress(DLLHandle, 'sfem_GetEnrollCount');
          @Delete							  := GetProcAddress(DLLHandle, 'sfem_Delete');
          @DeleteID						  := GetProcAddress(DLLHandle, 'sfem_DeleteID');
          @DeleteAll						:= GetProcAddress(DLLHandle, 'sfem_DeleteAll');
          @SearchID						  := GetProcAddress(DLLHandle, 'sfem_SearchID');
          @SearchFingerNum			:= GetProcAddress(DLLHandle, 'sfem_SearchFingerNum');
          @CheckID						  := GetProcAddress(DLLHandle, 'sfem_CheckID');
          @CheckFingerNum				:= GetProcAddress(DLLHandle, 'sfem_CheckFingerNum');
          @CheckManager					:= GetProcAddress(DLLHandle, 'sfem_CheckManager');
          @CheckManagerID				:= GetProcAddress(DLLHandle, 'sfem_CheckManagerID');
          @AdjustSensor					:= GetProcAddress(DLLHandle, 'sfem_AdjustSensor');
          @Capture						  := GetProcAddress(DLLHandle, 'sfem_Capture');
          @IsFinger						  := GetProcAddress(DLLHandle, 'sfem_IsFinger');
          @GetImage						  := GetProcAddress(DLLHandle, 'sfem_GetImage');
          @SetImage						  := GetProcAddress(DLLHandle, 'sfem_SetImage');
          @EnrollStart					:= GetProcAddress(DLLHandle, 'sfem_EnrollStart');
          @EnrollNth						:= GetProcAddress(DLLHandle, 'sfem_EnrollNth');
          @EnrollEnd						:= GetProcAddress(DLLHandle, 'sfem_EnrollEnd');
          @Identify						  := GetProcAddress(DLLHandle, 'sfem_Identify');
          @Verify							  := GetProcAddress(DLLHandle, 'sfem_Verify');
          @VerifyID						  := GetProcAddress(DLLHandle, 'sfem_VerifyID');
          @TemplateGetFromImage	:= GetProcAddress(DLLHandle, 'sfem_TemplateGetFromImage');
          @TemplateGetFromDB		:= GetProcAddress(DLLHandle, 'sfem_TemplateGetFromDB');
          @TemplateEnroll				:= GetProcAddress(DLLHandle, 'sfem_TemplateEnroll');
          @TemplateIdentify			:= GetProcAddress(DLLHandle, 'sfem_TemplateIdentify');
          @TemplateVerify				:= GetProcAddress(DLLHandle, 'sfem_TemplateVerify');
          @TemplateVerifyID			:= GetProcAddress(DLLHandle, 'sfem_TemplateVerifyID');
          @LoadImageFromFile		:= GetProcAddress(DLLHandle, 'sfem_LoadImageFromFile');
          @SaveImageToFile			:= GetProcAddress(DLLHandle, 'sfem_SaveImageToFile');
          @LoadTemplateFromFile	:= GetProcAddress(DLLHandle, 'sfem_LoadTemplateFromFile');
          @SaveTemplateToFile		:= GetProcAddress(DLLHandle, 'sfem_SaveTemplateToFile');
          @LedControl						:= GetProcAddress(DLLHandle, 'sfem_LedControl');
          @GetFpQuality					:= GetProcAddress(DLLHandle, 'sfem_GetFpQuality');
          @ConvertImage					:= GetProcAddress(DLLHandle, 'sfem_ConvertImage');

     end;
end;

end.
