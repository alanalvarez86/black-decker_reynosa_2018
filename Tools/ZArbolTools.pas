unit ZArbolTools;

interface

uses ComCtrls, ZetaDespConsulta, dxSkinsdxNavBarPainter, dxNavBar, dxNavBarCollns, ZNavBarTools,
cxTreeview;//edit By Mp: Se agrego el CxTreeview;
 {ZBaseShell, ZetaCommonList }
const K_ARBOL_NODO_RAIZ = -1;

type
  TNodoInfo = record
    Caption : String;
    case EsForma : Boolean of
      True  : ( TipoForma : eFormaConsulta );
      False : ( IndexDerechos : Integer );
  end;

  {***(@:am): La forma de hacer propiedades a un tipo record, cambio en DelphiXE5. Si se cuenta con un contructor en la unidad,
              es preferible cambiar el tipo recor por una clase. Si se desea conservar el record, se debe crear una propiedad para
              acceder a cada campo.***}
  {$IFDEF TRESS_DELPHIXE5_UP}
  TDatosArbolUsuario = class(TObject)
  {$ELSE}
  TDatosArbolUsuario = record
  {$ENDIF}
    IncluirSistema: Boolean;
    TieneArbol : Boolean;
    NombreNodoInicial: String;
    NodoDefault: Integer;
  end;

type
  TArbolMgr = class(TObject)
  private
   FArbol: TTreeView;
   Farbol_Devex : TcxTreeView;////Edit by MP:Se agrega Farbol_DevEx
   FNumDefault : Integer;
   FUltimoNodo   : TTreeNode;
   FConfigura : Boolean;
   FDatosUsuario : TDatosArbolUsuario;
   function AgregaFolder( const Padre : TTreeNode; const sCaption: String; const iDerechos, iNodo : Integer ): TTreeNode;
   function AgregaForma(  const Padre : TTreeNode; const sCaption: String; const TipoForma : eFormaConsulta; const iNodo : Integer ): TTreeNode;
  public
   property Arbol: TTreeView read FArbol;
   property Arbol_DevEx: TcxTreeview read FArbol_DevEx;//Edit by MP: se agregan las propiedades de arbol_DevEx
   property NumDefault : Integer read FNumDefault write FNumDefault;
   property Configuracion : Boolean read FConfigura write FConfigura;
   property  DatosUsuario : TDatosArbolUsuario read FDatosUsuario {$IFDEF TRESS_DELPHIXE5_UP} write FDatosUsuario {$ENDIF};
   constructor Create( TreeView: TTreeView ); overload;
   constructor Create( TreeView: TcxTreeView ); overload;
   {$IFDEF TRESS_DELPHIXE5_UP}
   destructor Destroy; override;// se sobrecarga el constructor (constructor para tcxtreeview) - [dcc32 Warning] ZArbolTools.pas(52): W1010 Method 'Destroy' hides virtual method of base type 'TObject'
   {$ENDIF}
   function NodoNivel( const iNivel: Integer; const oNodo: TNodoInfo; const iNodo: Integer ): Boolean;
  end;



function EsForma( const Nodo : TTreeNode; var TipoForma : eFormaConsulta ) : Boolean;
function EsFolder( const Nodo : TTreeNode ) : Boolean;
function  BuscaNodoTexto( oArbol : TTreeView; sCaption, sTexto : String ) : Boolean; overload;
function  BuscaNodoTexto( oArbol : TcxTreeView; sCaption, sTexto : String ) : Boolean; overload
procedure BuscaNodoReset;

//DevEx
function  BuscaNodoTextoArbolito( oNavBar: TdxNavBar; ANavBarGroup: TdxNavBarGroup; oArbol : TTreeView; sTexto : String ) : Boolean;overload ;
function  BuscaNodoTextoArbolito( oNavBar: TdxNavBar; ANavBarGroup: TdxNavBarGroup; oArbol : TcxTreeView; sTexto : String ) : Boolean;overload ; // se sobrecarga el metodo de busqueda
var FUltimoTexto : String;
    FNodosTexto  : Integer;

implementation

uses Forms, ZAccesosMgr, SysUtils, ZetaDialogo, ZetaCommonClasses, ZetaCommonTools;

const
     { K_IMAGE_FOLDER    = 17;
      K_SELECTED_FOLDER = 18;
      K_IMAGE_FORMA     = 19;
      K_SELECTED_FORMA  = 20;
      }
      K_IMAGE_FOLDER    = 0;
      K_SELECTED_FOLDER = 1;
      K_IMAGE_FORMA     = 2;
      K_SELECTED_FORMA  = 3;

      //Imagenes especiales
      {K_IMAGE_FOLDER_ESP_DevEx = 0;
      K_IMAGE_SELECTED_FOLDER_ESP_DevEx = 1;

      //Constantes con imagen especial
      K_CAJA_REGISTRO                    = 543;
      K_CAJA_PROCESOS                    = 547;
      K_CONS_PROCESOS                    = 163;}

function EsFolder( const Nodo : TTreeNode ) : Boolean;
begin
     Result := ( Nodo.ImageIndex = K_IMAGE_FOLDER );
     {Result := FALSE;
     if ( (Nodo.ImageIndex = K_IMAGE_FOLDER) or (Nodo.ImageIndex = K_IMAGE_FOLDER_ESP_DevEx)) then
        Result := TRUE;}
end;

function EsForma( const Nodo : TTreeNode; var TipoForma : eFormaConsulta ) : Boolean;
begin
     Result := not EsFolder( Nodo );
     if ( Result ) then
        TipoForma := eFormaConsulta( Nodo.StateIndex );
end;

{************ TArbolMgr ***************}

constructor TArbolMgr.Create( TreeView: TTreeView );
begin
     FArbol:= TreeView;
     FUltimoNodo   := NIL;
     FNumDefault   := 0;
     FConfigura := FALSE;

     {$IFDEF TRESS_DELPHIXE5_UP}
     FDatosUsuario := TDatosArbolUsuario.Create;
     {$ENDIF}
end;
constructor TArbolMgr.Create( TreeView: TcxTreeView ); //Edit by MP:sobrecarga del Create
begin
   FArbol_DevEx:= TreeView;
     FUltimoNodo   := NIL;
     FNumDefault   := 0;
     FConfigura := FALSE;

     {$IFDEF TRESS_DELPHIXE5_UP}
     FDatosUsuario := TDatosArbolUsuario.Create;
     {$ENDIF}
     Farbol_Devex.Indent := 23;
end;

{$IFDEF TRESS_DELPHIXE5_UP}
destructor TArbolMgr.Destroy;
begin
  FreeAndNil(FDatosUsuario);
end;
{$ENDIF}

function TArbolMgr.AgregaFolder( const Padre : TTreeNode; const sCaption: String; const iDerechos, iNodo : Integer): TTreeNode;
begin
     // Si iNodo = RAIZ, entonces no se revisan derechos de Acceso
     if FConfigura or ( iNodo = K_ARBOL_NODO_RAIZ ) or ZAccesosMgr.CheckDerecho( iDerechos, K_DERECHO_CONSULTA ) then
     begin
        if(FArbol = nil) then////Edit by MP: Si no se creo el Farbol retornar el DevEx
        Result:= FArbol_DevEx.Items.AddChild( Padre, sCaption)
        else
         Result:= FArbol.Items.AddChild( Padre, sCaption);
		//FinEdit
         with Result do
         begin
              ImageIndex    := K_IMAGE_FOLDER;
              SelectedIndex := K_SELECTED_FOLDER;
              StateIndex := iDerechos;
              Data := Pointer( iNodo );
              FUltimoNodo := Result;
         end;
     end
     else
     begin
         Result := NIL;
         FUltimoNodo := Padre;
     end;
end;

function TArbolMgr.AgregaForma( const Padre : TTreeNode; const sCaption: String;
  const TipoForma : eFormaConsulta; const iNodo : Integer ): TTreeNode;
begin
     // El Padre puede ser Nil cuando no se tienen derechos en la Rama/Folder Padre
     //FConfigura debe de llegar en TRUE cuando el programa no es de TRESS y se asigna en CreaArbolUsuario

     if ( Padre <> NIL ) and
         ( FConfigura ) or ( ZAccesosMgr.CheckDerecho( ZetaDespConsulta.GetPropConsulta( TipoForma ).IndexDerechos,
                                 K_DERECHO_CONSULTA )) then
     begin
        if(FArbol = nil)then//Edit by MP: si no se creo el farbol, retornar el devex
          Result:= FArbol_DevEx.Items.AddChild( Padre, sCaption )
        else
          Result:= FArbol.Items.AddChild( Padre, sCaption );
        with Result do
        begin
		//fin edit
            {if( ( iNodo = K_CAJA_REGISTRO ) or ( iNodo = K_CAJA_PROCESOS ) or ( iNodo = K_CONS_PROCESOS )) then
            begin
                 ImageIndex    := K_IMAGE_FOLDER_ESP_DevEx;
                 SelectedIndex := K_IMAGE_SELECTED_FOLDER_ESP_DevEx;
            end
            else
            begin
                 ImageIndex    := K_IMAGE_FORMA;
                 SelectedIndex := K_SELECTED_FORMA;
            end;}
            ImageIndex    := K_IMAGE_FORMA;
            SelectedIndex := K_SELECTED_FORMA;
            StateIndex := Ord( TipoForma );
            Data := Pointer( iNodo );
            FUltimoNodo := Result;
       end;
     end
     else
     begin
         Result := NIL;
         FUltimoNodo := Padre;
     end;
end;

function TArbolMgr.NodoNivel( const iNivel: Integer; const oNodo: TNodoInfo; const iNodo: Integer ): Boolean;
var
   oPadre, oNewNodo : TTreeNode;
begin
     with oNodo do
     begin
          // Busca el padre del Nivel dado
          if ( iNivel <= 0 ) or ( FUltimoNodo = NIL ) then
             oPadre := NIL
          else
          begin
               oPadre := FUltimoNodo;
               while ( oPadre <> NIL ) and ( oPadre.Level >= iNivel ) do
               begin
                    oPadre := oPadre.Parent;
               end;
          end;

          // Agrega al Arbol, ya sea un Folder o una Forma
          if EsForma then
             oNewNodo := AgregaForma( oPadre, Caption, TipoForma, iNodo )
          else
              oNewNodo := AgregaFolder( oPadre, Caption, IndexDerechos, iNodo );

          // Marca como seleccionado el NodoDefault, Siempre que tenga Derechos
          if ( iNodo = NumDefault ) and ( oNewNodo <> NIL ) then
          begin
               oNewNodo.Selected := TRUE;
               NumDefault := -1;    // Para que quede marcado el Primero que encuentre
                                  // si tiene el mismo nodo dos veces
          end;
          Result := Assigned( oNewNodo );
     end;
end;

procedure BuscaNodoReset;
begin
  FUltimoTexto := '';
  FNodosTexto  := 0;
end;

function BuscaNodoTexto( oArbol : TTreeView; sCaption, sTexto : String ) : Boolean;
var
   oNodo: TTreeNode;
   sDescrip : String;

begin  { BuscaNodoTexto }
    Result := FALSE;
    with oArbol do
    begin
         if ( Items.Count <= 0 ) then Exit;

         sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
         if ( Selected = NIL ) or ( sTexto <> FUltimoTexto ) then
         begin
            oNodo := Items[ 0 ];
            BuscaNodoReset;
            FUltimoTexto := sTexto;
         end
         else
            oNodo := Selected.GetNext;

         HideSelection := False;
         while ( oNodo <> NIL ) do
         begin
            if ( Pos( sTexto, UpperCase( QuitaAcentos( oNodo.Text ))) > 0 ) then
            begin
                 Result := TRUE;
                 Inc( FNodosTexto ); // Para saber cuantos encontr�
                 Selected := oNodo;
                 SetFocus;
                 Exit;
            end;
            oNodo := oNodo.GetNext;
         end;
         HideSelection := True;
         if not Result then
         begin
            if ( FNodosTexto = 0 ) then
                sDescrip := 'ninguna'
            else
                sDescrip := 'otra';
            ZInformation( sCaption, Format( 'No hay %s Forma que contenga la palabra ' + CR_LF + '< %s >', [ sDescrip, sTexto ] ) , 0 );
            BuscaNodoReset;
         end;
    end;
end;
function BuscaNodoTexto( oArbol : TcxTreeView; sCaption, sTexto : String ) : Boolean;
var
   oNodo: TTreeNode;
   sDescrip : String;

begin  { BuscaNodoTexto }
    Result := FALSE;
    with oArbol do
    begin
         if ( Items.Count <= 0 ) then Exit;

         sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
         if ( Selected = NIL ) or ( sTexto <> FUltimoTexto ) then
         begin
            oNodo := Items[ 0 ];
            BuscaNodoReset;
            FUltimoTexto := sTexto;
         end
         else
            oNodo := Selected.GetNext;

         HideSelection := False;
         while ( oNodo <> NIL ) do
         begin
            if ( Pos( sTexto, UpperCase( QuitaAcentos( oNodo.Text ))) > 0 ) then
            begin
                 Result := TRUE;
                 Inc( FNodosTexto ); // Para saber cuantos encontr�
                 Selected := oNodo;
                 SetFocus;
                 Exit;
            end;
            oNodo := oNodo.GetNext;
         end;
         HideSelection := True;
         if not Result then
         begin
            if ( FNodosTexto = 0 ) then
                sDescrip := 'ninguna'
            else
                sDescrip := 'otra';
            ZInformation( sCaption, Format( 'No hay %s Forma que contenga la palabra ' + CR_LF + '< %s >', [ sDescrip, sTexto ] ) , 0 );
            BuscaNodoReset;
         end;
    end;
end;


//DevEx (by am)
function  BuscaNodoTextoArbolito( oNavBar: TdxNavBar; ANavBarGroup: TdxNavBarGroup; oArbol : TTreeView; sTexto : String ) : Boolean;
var
   oNodo: TTreeNode;
begin
     Result := FALSE;
     with oArbol do
     begin
         if ( Items.Count <= 0 ) then Exit;
     sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
     if ( Selected = NIL ) or ( sTexto <> FUltimoTexto ) then
     begin
          oNodo := Items[ 0 ];
          BuscaNodoReset;
          FUltimoTexto := sTexto;
     end
     else
         oNodo := Selected.GetNext;

     HideSelection := False;
     while ( oNodo <> NIL ) do
     begin
          if ( Pos( sTexto, UpperCase( QuitaAcentos( oNodo.Text ))) > 0 ) then
          begin
              if(ANavBarGroup.Visible)then   //edit by mp: NO buscara en los elmenetos Invisibles 
              begin
                Result := TRUE;
                //Como sabemos que encontro el nodo, ponemos activo el grupo en el que se encuentra
                oNavBar.ActiveGroup := ANavBarGroup;
                if ( ANavBarGroup.Expanded)then
                begin
                    Inc( FNodosTexto ); // Para saber cuantos encontr�
                    Selected := oNodo;
                    SetFocus;
                    Exit;
                end;
              end;
          end;
              oNodo := oNodo.GetNext;
     end;
     HideSelection := True;
     end;
end;

//Sobrecarga del busca Nodo TextoArbolito
function  BuscaNodoTextoArbolito( oNavBar: TdxNavBar; ANavBarGroup: TdxNavBarGroup; oArbol : TcxTreeView; sTexto : String ) : Boolean;
var
   oNodo: TTreeNode;
begin
     Result := FALSE;
     with oArbol do
     begin
         if ( Items.Count <= 0 ) then Exit;
     sTexto := UpperCase( ZetaCommonTools.QuitaAcentos( sTexto ));
     if ( Selected = NIL ) or ( sTexto <> FUltimoTexto ) then
     begin
          oNodo := Items[ 0 ];
          BuscaNodoReset;
          FUltimoTexto := sTexto;
     end
     else
         oNodo := Selected.GetNext;

     HideSelection := False;
     while ( oNodo <> NIL ) do
     begin
          if ( Pos( sTexto, UpperCase( QuitaAcentos( oNodo.Text ))) > 0 ) then
          begin
              if(ANavBarGroup.Visible)then   //edit by mp: NO buscara en los elmenetos Invisibles 
              begin
                Result := TRUE;
                //Como sabemos que encontro el nodo, ponemos activo el grupo en el que se encuentra
                oNavBar.ActiveGroup := ANavBarGroup;
                if ( ANavBarGroup.Expanded)then
                begin
                    Inc( FNodosTexto ); // Para saber cuantos encontr�
                    Selected := oNodo;
                    SetFocus;
                    Exit;
                end;
              end;
          end;
              oNodo := oNodo.GetNext;
     end;
     HideSelection := True;
     end;
end;

end.
