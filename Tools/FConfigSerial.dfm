inherited ConfigSerial: TConfigSerial
  Left = 612
  Top = 102
  Caption = 'Configuraci'#243'n de puerto serial'
  ClientHeight = 242
  ClientWidth = 292
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 298
  ExplicitHeight = 271
  PixelsPerInch = 96
  TextHeight = 13
  object LabelBaudrate: TLabel [0]
    Left = 13
    Top = 12
    Width = 133
    Height = 13
    Alignment = taRightJustify
    Caption = '&Velocidad de Transferencia:'
    FocusControl = ComboBaudrate
  end
  object LabelDataBits: TLabel [1]
    Left = 80
    Top = 36
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = '&Bits de Datos:'
    FocusControl = ComboDatabits
  end
  object LabelStopbits: TLabel [2]
    Left = 74
    Top = 60
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Bits de &Parada:'
    FocusControl = ComboStopbits
  end
  object LabelParity: TLabel [3]
    Left = 107
    Top = 84
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'P&aridad:'
    FocusControl = ComboParity
  end
  object LabelDTRControl: TLabel [4]
    Left = 84
    Top = 108
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Control D&TR:'
    FocusControl = ComboDTRControl
  end
  object LabelFlowControl: TLabel [5]
    Left = 70
    Top = 132
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Control de &Flujo:'
    FocusControl = ComboFlowControl
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object LabelRTSControl: TLabel [6]
    Left = 85
    Top = 156
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Control &RTS:'
    FocusControl = ComboRTSControl
  end
  object LabelRxChar: TLabel [7]
    Left = 89
    Top = 178
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terminador:'
    FocusControl = ComboRTSControl
  end
  inherited PanelBotones: TPanel
    Top = 206
    Width = 292
    TabOrder = 8
    ExplicitTop = 206
    ExplicitWidth = 292
    inherited OK_DevEx: TcxButton
      Left = 125
      OnClick = OKClick
      ExplicitLeft = 125
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 206
      ExplicitLeft = 206
    end
  end
  object ComboBaudrate: TcxComboBox [9]
    Left = 147
    Top = 7
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      '    110'
      '    300'
      '    600'
      '  1,200'
      '  2,400'
      '  4,800'
      '  9,600'
      ' 14,400'
      ' 19,200'
      ' 38,400'
      ' 56,000'
      ' 57,600'
      '115,200'
      '128,000'
      '256,000')
    TabOrder = 0
    Width = 140
  end
  object ComboDatabits: TcxComboBox [10]
    Left = 147
    Top = 31
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      '4'
      '5'
      '6'
      '7'
      '8')
    TabOrder = 1
    Width = 140
  end
  object ComboStopbits: TcxComboBox [11]
    Left = 147
    Top = 55
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      '1'
      '1.5'
      '2')
    TabOrder = 2
    Width = 140
  end
  object ComboParity: TcxComboBox [12]
    Left = 147
    Top = 79
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      'None'
      'Odd'
      'Even'
      'Mark'
      'Space')
    TabOrder = 3
    Width = 140
  end
  object ComboDTRControl: TcxComboBox [13]
    Left = 147
    Top = 103
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      'Disable'
      'Enable'
      'Handshake')
    TabOrder = 4
    Width = 140
  end
  object ComboFlowControl: TcxComboBox [14]
    Left = 147
    Top = 127
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      'None'
      'XonXoff'
      'RtsCts'
      'DtrDsr')
    TabOrder = 5
    Width = 140
  end
  object ComboRTSControl: TcxComboBox [15]
    Left = 147
    Top = 151
    ParentFont = False
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      'Disable'
      'Enable'
      'Handshake'
      'Toggle')
    TabOrder = 6
    Width = 140
  end
  object RxChar: TZetaNumero [16]
    Left = 147
    Top = 174
    Width = 28
    Height = 21
    Mascara = mnDias
    TabOrder = 7
    Text = '0'
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 12058688
  end
end
