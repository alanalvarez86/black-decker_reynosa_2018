unit FConfigFiltroCursos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseDlgModal_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore,
  TressMorado2013, Vcl.ImgList, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls;

type
  TConfigFiltroCursos = class(TZetaDlgModal_DevEx)
    Advertencia: TMemo;
    PanelOpciones: TPanel;
    CBPuestos: TCheckBox;
    CBTurnos: TCheckBox;
    CBClaseCursos: TCheckBox;
    CBTipoCursos: TCheckBox;
    CBCursos: TCheckBox;
    CBNivel1: TCheckBox;
    CBNivel2: TCheckBox;
    CBNivel3: TCheckBox;
    CBNivel4: TCheckBox;
    CBNivel5: TCheckBox;
    CBNivel6: TCheckBox;
    CBNivel7: TCheckBox;
    CBNivel8: TCheckBox;
    CBNivel9: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure CBOpcionesClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    oListaFiltros: TStrings;
    function SetOpcionesFiltros: Boolean;
    procedure SetCheckBoxNiveles(oControl: TCheckBox; const iMaxNiveles: Integer; const sNombre: String);
    procedure GetOpcionesFiltros;
    procedure InitBotones;
  public
    { Public declarations }
  end;

const
     K_MAX_FILTROS = 4;
     NIVELES_OFFSET = 5;

var
  ConfigFiltroCursos: TConfigFiltroCursos;

implementation

uses dTablas, dCatalogos, dGlobal,
     ZetaRegistryCliente,
     ZetaDialogo;

{$R *.dfm}

procedure TConfigFiltroCursos.FormCreate(Sender: TObject);
var
   iNiveles: Integer;
begin
     inherited;
     with dmCatalogos do
     begin
          CBPuestos.Caption := cdsPuestos.LookupName;
          CBTurnos.Caption := cdsTurnos.LookupName;
          CBCursos.Caption := cdsCursos.LookupName;
     end;
     with dmTablas do
     begin
          CBTipoCursos.Caption := cdsTipoCursos.LookupName;
          CBClaseCursos.Caption := cdsClasifiCurso.LookupName;
          iNiveles := Global.NumNiveles;
          SetCheckBoxNiveles( CBNivel1, iNiveles, cdsNivel1.LookupName );
          SetCheckBoxNiveles( CBNivel2, iNiveles, cdsNivel2.LookupName );
          SetCheckBoxNiveles( CBNivel3, iNiveles, cdsNivel3.LookupName );
          SetCheckBoxNiveles( CBNivel4, iNiveles, cdsNivel4.LookupName );
          SetCheckBoxNiveles( CBNivel5, iNiveles, cdsNivel5.LookupName );
          SetCheckBoxNiveles( CBNivel6, iNiveles, cdsNivel6.LookupName );
          SetCheckBoxNiveles( CBNivel7, iNiveles, cdsNivel7.LookupName );
          SetCheckBoxNiveles( CBNivel8, iNiveles, cdsNivel8.LookupName );
          SetCheckBoxNiveles( CBNivel9, iNiveles, cdsNivel9.LookupName );
     end;
     oListaFiltros := TStringList.Create;
end;

procedure TConfigFiltroCursos.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( oListaFiltros );
end;

procedure TConfigFiltroCursos.FormShow(Sender: TObject);
begin
     inherited;
     GetOpcionesFiltros;
     InitBotones;
end;

procedure TConfigFiltroCursos.OK_DevExClick(Sender: TObject);
begin
     inherited;
     if SetOpcionesFiltros then
        self.ModalResult := mrOk;
end;

procedure TConfigFiltroCursos.CBOpcionesClick(Sender: TObject);
begin
     inherited;
     if ( not OK_DevEx.Enabled ) then
     begin
          OK_DevEx.Enabled := TRUE;
          with Cancelar_DevEx do
          begin
               OptionsImage.ImageIndex := 0;
               ModalResult := mrNone;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar cambios';
          end;
          Application.ProcessMessages;
     end;
end;

procedure TConfigFiltroCursos.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     if Cancelar_DevEx.Cancel then
        Close
     else
     begin
          GetOpcionesFiltros;
          InitBotones;
     end;
end;

procedure TConfigFiltroCursos.GetOpcionesFiltros;
var
   i: Integer;
   oControl: TCheckBox;
begin
     oListaFiltros.CommaText := ClientRegistry.FiltroCursosProgramados;
     with PanelOpciones do
     begin
          for i := 0 to ( ControlCount - 1 ) do
          begin
               if Controls[i] is TCheckBox then
               begin
                    oControl := TCheckBox( Controls[i] );
                    oControl.Checked := ( oListaFiltros.IndexOf( IntToStr( oControl.Tag ) ) >= 0 );
               end;
          end;
     end;
end;

function TConfigFiltroCursos.SetOpcionesFiltros: Boolean;
var
   i: Integer;
   oControl: TCheckBox;
begin
     Result := FALSE;
     oListaFiltros.Clear;
     with PanelOpciones do
     begin
          for i := 0 to ( ControlCount - 1 ) do
          begin
               if Controls[i] is TCheckBox then
               begin
                    oControl := TCheckBox( Controls[i] );
                    if oControl.Checked then
                       oListaFiltros.Add( IntToStr( oControl.Tag ) );
               end;
          end;
     end;
     if ( oListaFiltros.Count <= K_MAX_FILTROS ) then
     begin
          try
             ClientRegistry.FiltroCursosProgramados := oListaFiltros.CommaText;
             Result := TRUE;
          except
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end
     else
         ZetaDialogo.ZError( self.Caption, Format( 'Se pueden seleccionar m�ximo %d tablas y/o cat�logos', [ K_MAX_FILTROS ] ), 0 );
end;

procedure TConfigFiltroCursos.SetCheckBoxNiveles( oControl: TCheckBox; const iMaxNiveles: Integer; const sNombre: String );
begin
     with oControl do
     begin
          Visible := ( ( Tag - NIVELES_OFFSET ) <= iMaxNiveles );
          if Visible then
             Caption := sNombre;
     end;
end;

procedure TConfigFiltroCursos.InitBotones;
begin
     OK_DevEx.Enabled := FALSE;
     with Cancelar_DevEx do
     begin
          OptionsImage.ImageIndex := 2;
          ModalResult := mrNone;
          Cancel := True;
          Caption := '&Salir';
          Hint := 'Cerrar pantalla y salir';
          if Self.Active then  // EZM: S�lo si la forma es visible
            SetFocus;
     end;
     Application.ProcessMessages;
end;

end.
