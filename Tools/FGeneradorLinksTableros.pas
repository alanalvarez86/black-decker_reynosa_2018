unit FGeneradorLinksTableros;

interface

uses
   Winapi.Windows,
   Controls,
   Classes,
   ExtCtrls,
   Graphics,
   SysUtils,
   StdCtrls,
   Forms,
   FGridPanel,
   ZetaDialogo,
   ZetaClientDataSet,
   ZetaCommonTools,
   VCLTee.Chart,
   VCLTee.TeEngine,
   VCLTee.TeCanvas,
   DDashlets,
   ZetaCXGrid,
   ZBaseConsulta,
   FBaseGridLayaout,
   FThreadIndicadoresGraficasTRESS;


type

//Guarda los datos para saber si refrescar el Tablero
TValoresRefrescar = record
  CambioValoresActivosGraficas: Boolean;
  ActualizarGraficasInicio: Boolean;
  Refrescar: Boolean;
  procedure Clear;
end;

TGeneradorLinksTableros = class
private
  { private declarations here }
  FTableroID: Integer;
  FTableroNombre: String;
  FTableroDescrip: String;
  FDataSet: TZetaClientDataSet;
  FAOwner: TComponent;
  FAParent: TWinControl;
  FTableroSesionID: Integer;
  FBaseChartBarrasHorizontal: TChart;
  FBaseChartColumnasVertical: TChart;
  FBaseChartPie: TChart;
  FBaseGridListado: TZetaCXGrid;
  FBaseHorizStack: TChart;
  FDatosConsultaDashlets: TZetaClientDataSet;
  FDashleetsConsulta: TdmDashleets;
  FAParentPanelBase: TWinControl;
  FAOwnerPanelBase: TComponent;
  FAParentMain: TWinControl;
  FAOwnerMain: TComponent;
  FTableroIDHome: Integer;
  FACallBackGraficas: TInfoIndicadoresGraficasCall;
  FACallBackGraficasAnimation: TInfoIndicadoresGraficasLoadAnimationCall;
  FArrayLabelLink: TArray<TLabel>;//array of TLabel;
  FArrayBaseGridLayout: TArray<TBaseGridLayaout>;//array of TBaseGridLayaout;
  FArrayThreadGraficas: TArray<TThreadIndicadoresGraficasTRESS>;// array of TThreadIndicadoresGraficasTRESS
  FArrayValoresActivos: TArray<TValoresRefrescar>; // array of TValoresRefrescar
  FTableroIDActivo: Integer;
  FTableroPosActivo: Integer;
  FCambioValoresActivosGraficas: Boolean;
  FActualizarGraficasInicio: Boolean;
  FRefrescar: Boolean;
  function GetTableroDescrip: String;
  function GetTableroID: Integer;
  function GetTableroNombre: String;
  function GetTableroSesionID: Integer;
  procedure SetTableroSesionID(const Value: Integer);
  function GetArrayLabelLink(Index: Integer): TLabel;
  procedure SetArrayLabelLink(Index: Integer; const Value: TLabel);
  function GetPosicionLink( const iTableroID: Integer ): Integer;
  function ObtenerFormaBaseDashlets( const iPosicionLink: Integer ): TBaseGridLayaout;
  function GetArrayBaseGridLayout(Index: Integer): TBaseGridLayaout;
  procedure SetArrayBaseGridLayout(Index: Integer; const Value: TBaseGridLayaout);
  procedure SetNoVisibleFormBase;
  function GetArrayThreadGraficas( Index: Integer ): TThreadIndicadoresGraficasTRESS;
  procedure SetArrayThreadGraficas(Index: Integer; const Value: TThreadIndicadoresGraficasTRESS);
  function ObtenerThreadDashlets( const iPosicionLink: Integer ): TThreadIndicadoresGraficasTRESS;
  procedure DestroyLinks;
  procedure DestroyFormaConsulta;
  procedure DestroyThreadLinks;
  procedure TerminarThread( var hThreadGrafica: TThreadIndicadoresGraficasTRESS );
  function GetExistenLinks: Boolean;
  function GetArrayValoresActivos( Index: Integer ): TValoresRefrescar;
  procedure SetArrayValoresActivos( Index: Integer; const Value: TValoresRefrescar);
  procedure DestroyValoresActivos;
  procedure ResetValoresActivosTablero( const iPosicionLink: Integer );
  function GetValorActivo: TValoresRefrescar;
  function ActualizarTablero( const iPosicionLink: Integer ): Boolean;
protected
  { protected declarations here }
public
  { public declarations here }
  property ArrayLabelLink[ Index: Integer]: TLabel read GetArrayLabelLink write SetArrayLabelLink;
  property ArrayBaseGridLayout[ Index: Integer ]: TBaseGridLayaout read GetArrayBaseGridLayout write SetArrayBaseGridLayout;
  property ArrayThreadGraficas[ Index: Integer ]: TThreadIndicadoresGraficasTRESS read GetArrayThreadGraficas write SetArrayThreadGraficas;
  property ArrayValoresActivos[ Index: Integer ]: TValoresRefrescar read GetArrayValoresActivos write SetArrayValoresActivos;
  constructor Create(const Dataset: TZetaClientDataSet; const AOwner: TComponent; const AParent: TWinControl;  const iSesionID: Integer ); overload;
  destructor  Destroy; override;
  function GetStringWidth(textoOrigen:string; anchocaja:integer; fuente:TFont ) : string;
  procedure GenerarLinks;
  procedure GenerarTableros( var GridPanelBase: TGridPanel; const iTableroID: Integer );//const AOwner: TComponent; const AParent: TWinControl; const iTableroID: Integer ): TGeneradorLayout;
  function GetNombreTablero( const iTableroID: Integer ): String;
  function ConsultaDashlets( const iTableroID: Integer): Boolean;
  procedure CargarDashlets(Sender: TObject);
  function GetDashletsInformacion( const iTableroID: Integer ): TZetaClientDataSet;
  procedure SetValoresActivosTablero;
  procedure MostrarMensajeEsperaTablero( lRefrescar: Boolean = False );
  procedure LanzarThreadGrafica( const iTableroID: Integer; const iPosicionLink: Integer );
//published
  { published declarations here }
  property TableroID: Integer read GetTableroID;
  property TableroNombre: String read GetTableroNombre;
  property TableroDescrip: String read GetTableroDescrip;
  property TableroSesionID: Integer read GetTableroSesionID write SetTableroSesionID;
  property BaseChartBarrasHorizontal: TChart read FBaseChartBarrasHorizontal write FBaseChartBarrasHorizontal;
  property BaseChartColumnasVertical: TChart read FBaseChartColumnasVertical write FBaseChartColumnasVertical;
  property BaseChartPie: TChart read FBaseChartPie write FBaseChartPie;
  property BaseHorizStack: TChart read FBaseHorizStack write FBaseHorizStack;
  property BaseGridListado: TZetaCXGrid read FBaseGridListado write FBaseGridListado;
  property AParentPanelBase: TWinControl read FAParentPanelBase write FAParentPanelBase;
  property AOwnerPanelBase: TComponent read FAOwnerPanelBase write FAOwnerPanelBase;
  property TableroIDHome: Integer read FTableroIDHome write FTableroIDHome;
  property AParentMain: TWinControl read FAParentMain write FAParentMain;
  property AOwnerMain: TComponent read FAOwnerMain write FAOwnerMain;
  property ACallBackGraficas: TInfoIndicadoresGraficasCall read FACallBackGraficas write FACallBackGraficas;
  property ACallBackGraficasAnimation: TInfoIndicadoresGraficasLoadAnimationCall read FACallBackGraficasAnimation write FACallBackGraficasAnimation;
  property TableroIDActivo: Integer read FTableroIDActivo write FTableroIDActivo;
  property TableroPosActivo: Integer read FTableroPosActivo write FTableroPosActivo;
  property CambioValoresActivosGraficas: Boolean read FCambioValoresActivosGraficas write FCambioValoresActivosGraficas;
  property ActualizarGraficasInicio: Boolean read FActualizarGraficasInicio write FActualizarGraficasInicio;
  property Refrescar: Boolean read FRefrescar write FRefrescar;
  property ExistenLinks: Boolean read GetExistenLinks;
end;

const
 K_NOMBRE_TABLERO = 'LINK_TABLERO_%d';
 K_NOMBRE_TABLERO_ID = 'TABLERO_COMPANY_%d';
 K_MENSAJE_ESPERE = 'Espere ...';
 K_MENSAJE_REFRESCANDO = 'REFRESCANDO ...';
 K_NAME_LBL_THREAD = 'lblRefrescando';
var
 GeneradorLinksTableros: TGeneradorLinksTableros;

implementation

{ TGeneradorLinksTableros }
uses
    FIndicadores, FIndicadorListado, FGraficasDashlets;

constructor TGeneradorLinksTableros.Create(const Dataset: TZetaClientDataSet; const AOwner: TComponent; const AParent: TWinControl; const iSesionID: Integer );
begin
     FDataSet := Dataset;
     FAOwner := AOwner;
     FAParent := AParent;
     FTableroSesionID := iSesionID;
     FDatosConsultaDashlets := TZetaClientDataSet.Create( AOwner );
     FTableroIDActivo := -1;
     FTableroPosActivo := -1;
     FDashleetsConsulta := TdmDashleets.Create( AOwner );
     SetLength(FArrayLabelLink, 0);
     SetLength(FArrayBaseGridLayout, 0);
     SetLength(FArrayThreadGraficas, 0);
     SetLength(FArrayValoresActivos, 0);
end;

destructor TGeneradorLinksTableros.Destroy;
begin
     inherited;
     DestroyLinks;
     DestroyFormaConsulta;
     DestroyThreadLinks;
     DestroyValoresActivos;
end;

//Destroy Objects

procedure TGeneradorLinksTableros.DestroyLinks;
var
  i : Integer;
begin
     for i:= Low( FArrayLabelLink ) to High( FArrayLabelLink ) do
     begin
           if Assigned( FArrayBaseGridLayout[ i ] ) then
             freeAndNil( FArrayBaseGridLayout[ i ] );
     end;
end;

procedure TGeneradorLinksTableros.DestroyFormaConsulta;
var
  i : Integer;
begin
     for i:= Low( FArrayBaseGridLayout ) to High( FArrayBaseGridLayout ) do
     begin
           if Assigned( FArrayBaseGridLayout[ i ] ) then
             freeAndNil( FArrayBaseGridLayout[ i ] );
     end;
end;

procedure TGeneradorLinksTableros.DestroyThreadLinks;
var
  i : Integer;
begin
     for i:= Low( FArrayThreadGraficas ) to High( FArrayThreadGraficas ) do
     begin
           if Assigned( FArrayThreadGraficas[ i ] ) then
             TerminarThread( FArrayThreadGraficas[ i ] );
     end;
end;

procedure TGeneradorLinksTableros.DestroyValoresActivos;
var
  i : Integer;
begin
     for i:= Low( FArrayValoresActivos ) to High( FArrayValoresActivos ) do
     begin
          FArrayValoresActivos[ i ].Clear;
     end;
end;

procedure TGeneradorLinksTableros.TerminarThread( var hThreadGrafica: TThreadIndicadoresGraficasTRESS );
begin
     if Assigned( hThreadGrafica ) and ( hThreadGrafica.StatusTerminate ) then
     begin
          hThreadGrafica.WaitFor;
          FreeAndNil( hThreadGrafica );
     end
     else if Assigned( hThreadGrafica ) and ( not hThreadGrafica.StatusTerminate ) then
     begin
          hThreadGrafica.Terminate;
          TerminateThread( hThreadGrafica.Handle, THREAD_PRIORITY_NORMAL );
          hThreadGrafica.WaitFor;
          FreeAndNil( hThreadGrafica );
     end;
end;
//end

procedure TGeneradorLinksTableros.ResetValoresActivosTablero( const iPosicionLink: Integer );
begin
     if Length( FArrayValoresActivos ) > 0 then
     begin
          with FArrayValoresActivos[ iPosicionLink ] do
          begin
               CambioValoresActivosGraficas := False;
               ActualizarGraficasInicio := False;
               Refrescar := False;
          end;
     end;
end;

function TGeneradorLinksTableros.GetValorActivo: TValoresRefrescar;
begin
     with Result do
     begin
          CambioValoresActivosGraficas := FCambioValoresActivosGraficas;
          ActualizarGraficasInicio := FActualizarGraficasInicio;
          Refrescar := FRefrescar;
     end;
end;

function TGeneradorLinksTableros.GetStringWidth(textoOrigen:string; anchocaja:integer; fuente:TFont ) : string;
var
   BM:Tbitmap;
   texto:string;
   ancho:integer;
   anchoborde:integer;
begin
     anchoborde:=0;
     texto:='';
     BM := TBitmap.Create;
     BM.Canvas.Font := Fuente;
     ancho := 25+trunc(BM.Canvas.TextWidth(textoOrigen))+8;
     while  anchoborde+ancho < anchocaja do
     begin
          texto := ' '+texto;
          anchoborde :=(BM.Canvas.TextWidth(texto+' ')*2);
     end;
     BM.Free;
     Result := texto+textoOrigen;
end;

procedure TGeneradorLinksTableros.GenerarLinks;
var
   lHomeIDTablero: Boolean;
   oLabel: TLabel;
   i, leftAux: Integer;
   function GetLabelWidth( etiqueta : TLabel  ) : integer;
   var
      bmWidth: TBitmap;
      begin
        bmWidth := TBitmap.Create;
        try
          bmWidth.Canvas.Font.Assign(etiqueta.Font);
          Result := bmWidth.Canvas.TextWidth(etiqueta.Caption);
        finally
          bmWidth.Free;
      end;
   end;
begin
     lHomeIDTablero := False;
     if ( FDataSet <> nil ) and not ( FDataSet.EOF ) then
     begin
          i := 0;
          leftAux := 0;
          with FDataSet do
          begin
              First;
              while ( not EOF ) do
              begin
                   oLabel := TLabel.Create( FAOwner );
                   with oLabel do
                   begin
                        Name := Format( K_NOMBRE_TABLERO, [ FieldByName( 'TableroID' ).AsInteger ] );
                        Parent := FAParent;
                        Caption := FieldByName( 'TableroNombre' ).AsString;
                        Alignment := taLeftJustify;
                        Cursor := crHandPoint;
                        Hint := FieldByName( 'TableroDescrip' ).AsString;
                        ShowHint := True;
                        Font.Name := 'Segoe UI';
                        Font.Size := 10;
                        OnClick := Self.CargarDashlets;
                        Tag := FieldByName( 'TableroID' ).AsInteger;
                        Width := GetLabelWidth(oLabel);
                        Top := 10;
                        if not lHomeIDTablero then
                        begin
                             FTableroIDHome := FieldByName( 'TableroID' ).AsInteger;
                             lHomeIDTablero := True;
                        end;
                        if i > 0 then
                           Left := leftAux + 20
                        else
                            Left := 20;
                   end;
                   SetLength( FArrayLabelLink, Length( FArrayLabelLink ) + 1 );
                   FArrayLabelLink[ High( FArrayLabelLink ) ] := oLabel;
                   SetLength( FArrayBaseGridLayout, Length( FArrayBaseGridLayout ) + 1 );
                   FArrayBaseGridLayout[ High( FArrayBaseGridLayout ) ] := nil;
                   SetLength( FArrayThreadGraficas, Length( FArrayThreadGraficas ) + 1 );
                   FArrayThreadGraficas[ High( FArrayThreadGraficas ) ] := nil;
                   SetLength( FArrayValoresActivos, Length( FArrayValoresActivos ) + 1 );
                   FArrayValoresActivos[ High( FArrayValoresActivos ) ] := GetValorActivo;
                   leftAux := leftAux + oLabel.Width + 20;
                   inc(i);
                   Next;
              end;
          end;
     end;
end;

procedure TGeneradorLinksTableros.LanzarThreadGrafica( const iTableroID: Integer; const iPosicionLink: Integer );
var
   oThreadGrafica: TThreadIndicadoresGraficasTRESS;
   procedure CrearThreadGrafica;
   begin
        oThreadGrafica := TThreadIndicadoresGraficasTRESS.Create( True, FACallBackGraficas, FACallBackGraficasAnimation );
        oThreadGrafica.PosTablero := iPosicionLink;
        oThreadGrafica.SesionIDDashlet := FTableroSesionID;
        oThreadGrafica.TableroID := iTableroID;
        oThreadGrafica.DatosConsultaDashlets := GetDashletsInformacion( iTableroID );
        with oThreadGrafica do
        begin
            FreeOnTerminate := False;
            Start;
        end;
        FArrayThreadGraficas[ iPosicionLink ] := oThreadGrafica;
   end;
begin
     oThreadGrafica := ObtenerThreadDashlets( iPosicionLink );
     if ( iPosicionLink <> -1 ) and ( not Assigned( oThreadGrafica ) ) then
     begin
          CrearThreadGrafica;
     end
     else if Assigned( oThreadGrafica ) then
     begin
          if oThreadGrafica.StatusTerminate then
          begin
               if ActualizarTablero( iPosicionLink ) then
               begin
                    FArrayThreadGraficas[ iPosicionLink ] := nil;
                    ResetValoresActivosTablero( iPosicionLink );
                    CrearThreadGrafica;
               end;
          end
          else
          begin
               if ActualizarTablero( iPosicionLink ) then
               begin
                    FArrayThreadGraficas[ iPosicionLink ].Terminate();
                    FArrayThreadGraficas[ iPosicionLink ] := nil;
                    CrearThreadGrafica;
               end;
          end;
     end;
end;

function TGeneradorLinksTableros.ActualizarTablero( const iPosicionLink: Integer ): Boolean;
begin
     Result := False;
     if Length( FArrayValoresActivos ) > 0 then
     begin
          with FArrayValoresActivos[ iPosicionLink ] do
          begin
               Result := FArrayValoresActivos[ iPosicionLink ].CambioValoresActivosGraficas or FArrayValoresActivos[ iPosicionLink ].ActualizarGraficasInicio or FArrayValoresActivos[ iPosicionLink ].Refrescar;
          end;
     end;
end;

function TGeneradorLinksTableros.GetPosicionLink( const iTableroID: Integer ): Integer;
var
  i : Integer;
begin
     Result := -1;
     for i:= Low( FArrayLabelLink ) to High( FArrayLabelLink ) do
     begin
          if FArrayLabelLink[ i ].Tag = iTableroID then
          begin
               Result := i;
               Break;
          end;
     end;
end;

procedure TGeneradorLinksTableros.SetNoVisibleFormBase;
var
  i : Integer;
begin
     for i:= Low( FArrayBaseGridLayout ) to High( FArrayBaseGridLayout ) do
     begin
          if Assigned( FArrayBaseGridLayout[ i ] ) then
             FArrayBaseGridLayout[ i ].Visible := False;
     end;
end;

function TGeneradorLinksTableros.ObtenerFormaBaseDashlets( const iPosicionLink: Integer ): TBaseGridLayaout;
begin
     Result := nil;
     if Assigned( FArrayBaseGridLayout[ iPosicionLink ] ) then
        Result := FArrayBaseGridLayout[ iPosicionLink ];
end;

function TGeneradorLinksTableros.ObtenerThreadDashlets( const iPosicionLink: Integer ): TThreadIndicadoresGraficasTRESS;
begin
     Result := nil;
     if Assigned( FArrayThreadGraficas[ iPosicionLink ] ) then
        Result := FArrayThreadGraficas[ iPosicionLink ];
end;

function TGeneradorLinksTableros.GetArrayBaseGridLayout( Index: Integer ): TBaseGridLayaout;
begin
     Result := FArrayBaseGridLayout[ Index ];
end;

function TGeneradorLinksTableros.GetArrayLabelLink(Index: Integer): TLabel;
begin
     Result := FArrayLabelLink[ Index ];
end;

function TGeneradorLinksTableros.GetArrayThreadGraficas( Index: Integer ): TThreadIndicadoresGraficasTRESS;
begin
     Result := FArrayThreadGraficas[ Index ];
end;

function TGeneradorLinksTableros.GetArrayValoresActivos( Index: Integer ): TValoresRefrescar;
begin
     Result := FArrayValoresActivos[ Index ];
end;

function TGeneradorLinksTableros.GetNombreTablero( const iTableroID: Integer ): String;
begin
     Result := Format( K_NOMBRE_TABLERO_ID, [ iTableroID ] );
end;

procedure TGeneradorLinksTableros.SetValoresActivosTablero;
var
   i: Integer;
begin
     if Length( FArrayValoresActivos ) > 0 then
     begin
          for i:= Low( FArrayValoresActivos ) to High( FArrayValoresActivos ) do
          begin
               FArrayValoresActivos[ i ] := GetValorActivo;
          end;
     end;
end;

procedure TGeneradorLinksTableros.CargarDashlets( Sender: TObject );
var
   iPosicionLink: Integer;
   iTableroID: Integer;
   oGridBaseTablero: TBaseGridLayaout;
   procedure SetNegritasLink;
   var
      i: Integer;
   begin
        for i:= Low( FArrayLabelLink ) to High( FArrayLabelLink ) do
        begin
             if Assigned( FArrayLabelLink[ i ] ) then
             begin
                  if FArrayLabelLink[ i ].Tag = iTableroID then
                  begin
                       FArrayLabelLink[ i ].Font.Style := [fsBold,fsUnderline];
                  end
                  else
                      FArrayLabelLink[ i ].Font.Style := [];
             end;
        end;
   end;

begin
     inherited;
     iTableroID := TLabel( Sender ).Tag;
     SetNegritasLink;
     iPosicionLink := GetPosicionLink( iTableroID );
     FTableroIDActivo := iTableroID;
     FTableroPosActivo := iPosicionLink;
     oGridBaseTablero := ObtenerFormaBaseDashlets( iPosicionLink );
     SetNoVisibleFormBase;
     if ( iPosicionLink <> -1 ) and ( not Assigned( oGridBaseTablero ) ) then
     begin
          oGridBaseTablero := TBaseGridLayaout.Create(FAOwnerMain);
          oGridBaseTablero.Parent := FAParentMain;
          FArrayBaseGridLayout[ iPosicionLink ] := oGridBaseTablero;
          if ( not oGridBaseTablero.Visible ) then
          begin
               oGridBaseTablero.Visible := True;
               oGridBaseTablero.GridPanelBase.ParentBackground := False;
          end
          else
          begin
               oGridBaseTablero.Visible := FALSE;
          end;
          GenerarTableros( oGridBaseTablero.GridPanelBase, TLabel( Sender).Tag );
          LanzarThreadGrafica( iTableroID, iPosicionLink );
     end
     else if Assigned( oGridBaseTablero ) then
     begin
          if ( not oGridBaseTablero.Visible ) then
          begin
               oGridBaseTablero.Visible := True;
               oGridBaseTablero.GridPanelBase.ParentBackground := False;
          end
          else
          begin
               oGridBaseTablero.Visible := True;
               oGridBaseTablero.GridPanelBase.ParentBackground := False;
          end;
          LanzarThreadGrafica( iTableroID, iPosicionLink );
     end;
end;

function TGeneradorLinksTableros.ConsultaDashlets( const iTableroID: Integer ): Boolean;
var
   sError: String;
begin
     Result := False;
     FDatosConsultaDashlets := TZetaClientDataSet.Create( FAOwner );
     FDatosConsultaDashlets.Data := FDashleetsConsulta.GetConsultaDashletsTablero( sError, FTableroSesionID, iTableroID );
     if StrVacio( sError ) and not ( FDatosConsultaDashlets.EOF ) then
        Result := True;
end;

function TGeneradorLinksTableros.GetDashletsInformacion( const iTableroID: Integer ): TZetaClientDataSet;
var
   sError: String;
   oDataSet: TZetaClientDataSet;
begin
     Result := nil;
     oDataSet := TZetaClientDataSet.Create( FAOwner );
     oDataSet.Data := FDashleetsConsulta.GetConsultaDashletsTablero( sError, FTableroSesionID, iTableroID );
     if StrVacio( sError ) and not ( oDataSet.EOF ) then
        Result := oDataSet;
end;


function TGeneradorLinksTableros.GetExistenLinks: Boolean;
begin
     Result := False;
     if Length( FArrayLabelLink ) > 0 then
        Result := True;
end;

procedure TGeneradorLinksTableros.GenerarTableros( var GridPanelBase: TGridPanel; const iTableroID: Integer );//const AOwner: TComponent; const AParent: TWinControl; const iTableroID: Integer ): TGeneradorLayout;
const
     K_HEIGHT_PNL_ASIS = 41;
     K_HEIGHT_PNL_BOTTOM = 41;
     K_HEIGHT_TOP = 10;
     K_HEIGHT_BOTTOM = 10;
var
   oGridPanel : TGridPanelLayout;
begin
     if ConsultaDashlets( iTableroID ) then
     begin
          oGridPanel := TGridPanelLayout.Create( GridPanelBase );
          oGridPanel.BaseChartBarrasHorizontal := FBaseChartBarrasHorizontal;
          oGridPanel.BaseChartColumnasVertical := FBaseChartColumnasVertical;
          oGridPanel.BaseChartPie := FBaseChartPie;
          oGridPanel.BaseHorizStack := FBaseHorizStack;
          oGridPanel.BaseGridListado := FBaseGridListado;
          oGridPanel.GenerarTablero( FDatosConsultaDashlets );
     end;
end;

function TGeneradorLinksTableros.GetTableroDescrip: String;
begin
     Result := FTableroDescrip;
end;

function TGeneradorLinksTableros.GetTableroID: Integer;
begin
     Result := FTableroID;
end;

function TGeneradorLinksTableros.GetTableroNombre: String;
begin
     Result := FTableroNombre;
end;

function TGeneradorLinksTableros.GetTableroSesionID: Integer;
begin
     Result := FTableroSesionID;
end;

procedure TGeneradorLinksTableros.SetArrayBaseGridLayout(Index: Integer; const Value: TBaseGridLayaout);
begin
     Self.FArrayBaseGridLayout[ Index ] := Value;
end;

procedure TGeneradorLinksTableros.SetArrayLabelLink( Index: Integer; const Value: TLabel);
begin
     Self.FArrayLabelLink[ Index ] := Value;
end;

procedure TGeneradorLinksTableros.SetArrayThreadGraficas( Index: Integer; const Value: TThreadIndicadoresGraficasTRESS );
begin
     Self.FArrayThreadGraficas[ Index ] := Value;
end;

procedure TGeneradorLinksTableros.SetArrayValoresActivos( Index: Integer; const Value: TValoresRefrescar );
begin
     Self.FArrayValoresActivos[ Index ] := Value;
end;

procedure TGeneradorLinksTableros.SetTableroSesionID(const Value: Integer);
begin
     Self.FTableroSesionID := Value;
end;

procedure TGeneradorLinksTableros.MostrarMensajeEsperaTablero( lRefrescar: Boolean = False );
var
   GeneradorLayout : TGridPanel;
   I: Integer;
begin
     if ( Length( FArrayBaseGridLayout ) > 0 ) and Assigned( FArrayBaseGridLayout[ FTableroPosActivo ] ) then
     begin
          GeneradorLayout := TGridPanel( FArrayBaseGridLayout[ FTableroPosActivo ].FindComponent( 'GridPanelBase' ));
          if Assigned( GeneradorLayout ) then
          begin
               for I := 0 to GeneradorLayout.ControlCollection.Count - 1 do
               begin
                    if FArrayThreadGraficas[ FTableroPosActivo ].StatusTerminate then
                       Break;
                    with GeneradorLayout.Controls[I] do
                    begin
                         if ( ClassType = TIndicadoresListado ) then //Indicadores Listado TcxGrid
                         begin
                              if ( lRefrescar ) then
                                 TIndicadoresListado( GeneradorLayout.Controls[I] ).MensajeEspera;
                         end;
                         if ( ClassType = TIndicadores ) then //Indicadores
                         begin
                              if ( lRefrescar ) then
                                   TIndicadores( GeneradorLayout.Controls[I] ).MensajeEspera;
                         end;
                         if ( ClassType = TGraficasDashletsColumnas ) then //Grafica de Columnas
                         begin
                              if ( lRefrescar ) then
                                 TGraficasDashletsColumnas( GeneradorLayout.Controls[I] ).MensajeEspera;
                         end;
                         if ( ClassType = TGraficasDashletsBarras ) then //Grafica de Barras
                         begin
                              if ( lRefrescar ) then
                                 TGraficasDashletsBarras( GeneradorLayout.Controls[I] ).MensajeEspera;
                         end;
                         if ( ClassType = TGraficasDashletsPie ) then //Grafica de Pie
                         begin
                              if ( lRefrescar ) then
                                 TGraficasDashletsPie( GeneradorLayout.Controls[I] ).MensajeEspera;
                         end;
                         if ( ClassType = TGraficasDashletsHorizStack ) then //Grafica de Horz Stack
                         begin
                              if ( lRefrescar ) then
                                 TGraficasDashletsHorizStack( GeneradorLayout.Controls[I] ).MensajeEspera;
                         end;
                    end;
               end;
          end;//validacion componente Grid
     end;//validacion componente Forma
end;

{ TValoresRefrescar }

procedure TValoresRefrescar.Clear;
const
     Default: TValoresRefrescar=();
begin
     Self := Default;
end;

end.
