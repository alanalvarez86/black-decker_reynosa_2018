inherited Buscador: TBuscador
  ActiveControl = Codigo
  Caption = 'Buscar '
  ClientHeight = 100
  ClientWidth = 256
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object CodigoLBL: TLabel [0]
    Left = 45
    Top = 21
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'CodigoLBL:'
    FocusControl = Codigo
  end
  object NoExisteLBL: TLabel [1]
    Left = 168
    Top = 21
    Width = 71
    Height = 13
    Caption = '� No Existe !'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Visible = False
  end
  inherited PanelBotones: TPanel
    Top = 64
    Width = 256
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 88
      Hint = 'Buscar Registro'
      Default = True
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 173
      Hint = 'Abandonar B�squeda y Salir'
    end
  end
  object Codigo: TEdit
    Left = 103
    Top = 17
    Width = 58
    Height = 21
    TabOrder = 1
    OnChange = CodigoChange
    OnKeyPress = CodigoKeyPress
  end
end
