unit ZConstruyeFormula;

interface

uses Classes,
     Forms,
     Controls,
     ZetaTipoEntidad,
     ZetaCommonLists;

function GetFormulaConst( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
function GetFormulaConstRelaciones( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const lRelaciones: Boolean ): String;
function GetFormulaUnlimited( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
function GetFormulaConstruye( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): Boolean;
function GetFormulaConstruyeParams( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const oParams : TStrings ): Boolean;

implementation
uses FConstruyeFormula_DevEx, DCliente;

{Metodos para El ZConstruyeFormula}
function GetFormulaConstruye( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo : eTipoEvaluador ): Boolean;
begin
     //if ( ConstruyeFormula_DevEx = nil ) then
     try
         ConstruyeFormula_DevEx:= TConstruyeFormula_DevEx.Create( Application );
         with ConstruyeFormula_DevEx do
         begin
              Formula := sFormula;
              PosCursor := oPosCursor;
              EntidadActiva := oEntidad;
              TipoEvaluador := eTipo;
              ShowModal;
              Result := ( ModalResult = mrOk );
              if Result then
                 sFormula := Formula;
         end;
     finally
        ConstruyeFormula_DevEx.Free;
     end;

end;

function GetFormulaConstruyeParams( const oEntidad: TipoEntidad;
                                    var sFormula: String;
                                    oPosCursor: Integer;
                                    const eTipo: eTipoEvaluador;
                                    const oParams : TStrings ): Boolean;
begin
     ConstruyeFormula_DevEx:= TConstruyeFormula_DevEx.Create( Application );
     try
        with ConstruyeFormula_DevEx do
        begin
             Formula := sFormula;
             PosCursor := oPosCursor;
             EntidadActiva := oEntidad;
             TipoEvaluador := eTipo;
             Params := oParams;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
                sFormula := Formula;
        end;
     finally
            ConstruyeFormula_DevEx.Free;
     end;
end;

function GetFormula( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const lTruncar: Boolean ): String;
var
   oConstruyeFormula_DevEx: TConstruyeFormula_DevEx;
begin
     oConstruyeFormula_DevEx := TConstruyeFormula_DevEx.Create( Application.MainForm );
     with oConstruyeFormula_DevEx do
     begin
          try
             Truncar := lTruncar;
             Formula := sFormula;
             EntidadActiva := oEntidad;
             PosCursor := oPosCursor;
             TipoEvaluador := eTipo;
             ShowModal;
             if ( ModalResult = mrOK ) then
                Result := Formula
             else
                 Result := sFormula;
          finally
                 Free;
          end;
     end;
end;

function GetFormulaConst( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
begin
     Result := GetFormula( oEntidad, sFormula, oPosCursor, eTipo, True );
end;

function GetFormulaUnlimited( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
begin
     Result := GetFormula( oEntidad, sFormula, oPosCursor, eTipo, False );
end;

function GetFormulaConstRelaciones( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const lRelaciones: Boolean ): String;
var
   oConstruyeFormula_DevEx: TConstruyeFormula_DevEx;
begin
     oConstruyeFormula_DevEx := TConstruyeFormula_DevEx.Create( Application.MainForm );
     with oConstruyeFormula_DevEx do
     begin
          try
             Formula := sFormula;
             EntidadActiva := oEntidad;
             PosCursor := oPosCursor;
             TipoEvaluador := eTipo;
             Relaciones := lRelaciones;
             ShowModal;
             if ( ModalResult = mrOK ) then
                Result := Formula
             else
                 Result := sFormula;
          finally
                 Free;
          end;
     end;
end;

end.
