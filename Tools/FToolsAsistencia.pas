unit FToolsAsistencia;

interface

uses Controls, Dialogs, ZetaCommonLists, ZetaCommonClasses, ZetaKeyLookup, db, SysUtils, ZetaClientDataSet ,ZetaKeyLookup_DevEx;

     function FaltaMotivoAutorizacion( const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; const sMotivo: String; var sMensaje: String ): Boolean;
     //DevEx (by am): Se sobrecarga este metodo para que funcione con el nuevo lookup
     function ValidaAutoWizard( const sCaption: String; const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; oLookupControl: TZetaKeyLookup ): Boolean; overload;
     function ValidaAutoWizard( const sCaption: String; const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; oLookupControl: TZetaKeyLookup_DevEx ): Boolean; overload;
     //
     procedure ValidaAutoBeforePost( const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; oCampo: TField );
     function GetChecadaPuntual (DataSet: TZetaClientDataSet; const sHorario: string; iTipoChecada: Integer; var sChecada: string): boolean;
     procedure RegistrarTarjetaPuntual(DataSet: TZetaClientDataSet; const sChecada: string);
     function PermitirCapturaMotivoCH: boolean;{ACL150409}
     function CapturaObligatoriaMotCH: boolean;{ACL150409}
     function EsTipoChecadaAutorizacion( const TipoChecada: eTipoChecadas ): boolean;{ACL160409}
     procedure ValidaMotChecadaManual( const sMotivo: String; const iUsuario: Integer);

implementation

uses DGlobal, ZetaCommonTools, ZetaDialogo, ZGlobalTress;

function FaltaMotivoAutorizacion( const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; const sMotivo: String; var sMensaje: String ): Boolean;
begin
     Result := ( Global.GetGlobalBooleano( K_GLOBAL_VALIDA_PERM_CGOCE + Ord( TipoAuto ) ) and ( rHoras <> 0 ) and strVacio( sMotivo ) );
     if Result then
        sMensaje :=  'Falta Especificar El Motivo de Autorización de ' + ZetaCommonLists.ObtieneElemento( lfAutorizaChecadas, Ord( TipoAuto ) );
end;

function ValidaAutoWizard( const sCaption: String; const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; oLookupControl: TZetaKeyLookup ): Boolean;
var
   sMensaje: String;
begin
     Result := not FaltaMotivoAutorizacion( TipoAuto, rHoras, oLookupControl.Llave, sMensaje );
     if not Result then
     begin
          oLookupControl.SetFocus;
          ZetaDialogo.ZError( sCaption, sMensaje, 0 );
     end;
end;

//DevEx (by am): Se sobrecarga este metodo para que funcione con el nuevo lookup
function ValidaAutoWizard( const sCaption: String; const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; oLookupControl: TZetaKeyLookup_DevEx ): Boolean;
var
   sMensaje: String;
begin
     Result := not FaltaMotivoAutorizacion( TipoAuto, rHoras, oLookupControl.Llave, sMensaje );
     if not Result then
     begin
          oLookupControl.SetFocus;
          ZetaDialogo.ZError( sCaption, sMensaje, 0 );
     end;
end;

procedure ValidaAutoBeforePost( const TipoAuto: eAutorizaChecadas; const rHoras: TDiasHoras; oCampo: TField );
var
   sMensaje: String;
begin
     if FaltaMotivoAutorizacion( TipoAuto, rHoras, oCampo.AsString, sMensaje ) then
     begin
          oCampo.FocusControl;
          DatabaseError( sMensaje );
     end;
end;

function GetChecadaPuntual (DataSet: TZetaClientDataSet; const sHorario: string; iTipoChecada: Integer; var sChecada: string): boolean;
const
     K_ENTRADA = 0;
     K_SALIDA = 1;
     aTipoChecada : array[K_ENTRADA..K_SALIDA] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ( 'entrada', 'salida' );
begin
     if strLLeno( sHorario ) then
     begin
          with DataSet do
          begin
               Result := Locate('HO_CODIGO', sHorario, []);
               if Result then
               begin
                    case iTipoChecada of
                         K_ENTRADA : sChecada := ZetaCommonTools.GetEntrada24( eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ),
                                                                               FieldByName( 'HO_INTIME' ).AsString );
                         K_SALIDA  : sChecada := ZetaCommonTools.GetSalida24( eTipoHorario( FieldByName( 'HO_TIPO' ).AsInteger ),
                                                                              FieldByName( 'HO_INTIME' ).AsString,
                                                                              FieldByName( 'HO_OUTTIME' ).AsString );
                    else
                        sChecada := 'Tipo de checada inválido';
                    end;
               end
               else
                   sChecada := 'No existe el horario seleccionado';
          end;
     end
     else
     begin
          sChecada := format( 'Debe seleccionar horario para capturar %s puntual.', [aTipoChecada[ iTipoChecada ]] );
          Result := false;
     end;
end;

procedure RegistrarTarjetaPuntual(DataSet: TZetaClientDataSet; const sChecada: string) ;
begin
     with DataSet do
     begin
          if (Locate('CH_H_REAL', sChecada, [])) then
            DataBaseError( Format('Ya existe checada a las %s', [MaskHora( ConvierteHora(sChecada) ) ] ) )
          else
          begin
               Append;
               FieldByName('CH_H_REAL').AsString := ConvierteHora(sChecada);
               if NOT CapturaObligatoriaMotCH then
                  Post;
          end;
     end;
end;

{ACL160409}
function PermitirCapturaMotivoCH: boolean;
begin
     Result := Global.GetGlobalBooleano( K_GLOBAL_PERMITIR_CAPTURA );
end;

{ACL160409}
function CapturaObligatoriaMotCH: boolean;
begin
     Result := Global.GetGlobalBooleano( K_GLOBAL_CAPTURA_OBLIGATORIA )AND Global.GetGlobalBooleano( K_GLOBAL_PERMITIR_CAPTURA ) ;
end;

{ACL160409}
function EsTipoChecadaAutorizacion( const TipoChecada: eTipoChecadas ): boolean;
begin
     Result := eTipoChecadas( tipoChecada )in [ chConGoce, chSinGoce, chExtras, chDescanso, chConGoceEntrada, chSinGoceEntrada ];
end;

procedure ValidaMotChecadaManual( const sMotivo: String; const iUsuario: Integer);
begin
      if PermitirCapturaMotivoCH and StrLleno(sMotivo) and (iUsuario = 0) then
          DataBaseError( 'No se puede especificar Motivo: No es una checada Manual' );
end;

end.
