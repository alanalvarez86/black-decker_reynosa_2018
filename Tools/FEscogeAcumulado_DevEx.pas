unit FEscogeAcumulado_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
     ZBaseEscogeGrid_DevEx, ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEscogeAcumulado_DevEx = class(TBaseEscogeGrid_DevEx)
    TipoAcumula: TRadioGroup;
    co_numero: TcxGridDBColumn;
    co_descrip: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function GetFormula: String; override;
    procedure Connect; override;
  public
    { Public declarations }
 end;

var
  EscogeAcumulado_DevEx: TEscogeAcumulado_DevEx;

function PickAcumulado: String;

implementation

uses DCatalogos,
     ZetaCommonClasses;

{$R *.DFM}

function PickAcumulado: String;
begin
     if ( EscogeAcumulado_DevEx = nil ) then
        EscogeAcumulado_DevEx:= TEscogeAcumulado_DevEx.Create( Application );
     with EscogeAcumulado_DevEx do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
             Result := Formula;
     end;
end;

{ ******** TEscogeAcumulado ********* }

procedure TEscogeAcumulado_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66215_Escoge_acumulado;
     {
     SqlNumber:= Q_ESCOGE_ACUMULADO;
     }
end;

procedure TEscogeAcumulado_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsConceptosLookup.Conectar;
          DataSource.DataSet:= cdsConceptosLookup;
     end;
end;

function TEscogeAcumulado_DevEx.GetFormula: String ;
begin
     with dmCatalogos.cdsConceptosLookup do
     begin
          if not IsEmpty then
             Result := 'A( ' + FieldByName( 'CO_NUMERO' ).AsString + ',' + IntToStr( TipoAcumula.ItemIndex + 1 ) +' )'
          else
              Result := '';
     end;
     {
     if DatasetLleno( FQuery ) then
        Result:= 'A( ' + FQuery.FieldByName( 'CO_NUMERO' ).AsString + ',' + IntToStr( TipoAcumula.ItemIndex + 1 ) + ' )';
     }
end;

end.
