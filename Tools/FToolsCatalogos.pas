unit FToolsCatalogos;

interface

uses Controls, Dialogs, SysUtils, ZetaCommonLists, ZetaCommonClasses, db, ZetaClientDataSet;

procedure MuestraCalendarioRitmo( const sTurno: TCodigo; oClientDataSet: TZetaClientDataSet );

implementation

uses DGlobal,
     ZGlobalTress,
     DCliente,
     FCalendarioRitmo_DevEx,
     ZetaCommonTools,
     ZetaDialogo;

function GetPatronNormal( oClientDataSet: TZetaClientDataSet ): String;
var
   i, iDias: Integer;
   sHorario, sHorarioAct, sPatron : String;
   StatusDia, StatusDiaAct, LastStatusDia : eStatusAusencia;

   function GetFillerStatus: String;
   const
        K_FILLER = '0';
   var
      iBrinco : Integer;
      j : Integer;
   begin
        iBrinco := 0;
        Result := ZetaCommonClasses.VACIO;
        if ( LastStatusDia = StatusDiaAct ) then   // Debe dar la vuelta completa
        begin
             iBrinco := 2;
        end
        else
        begin
             if ( ( LastStatusDia = auHabil ) and ( StatusDiaAct = auDescanso ) ) or
                ( ( LastStatusDia = auSabado ) and ( StatusDiaAct = auHabil ) ) or
                ( ( LastStatusDia = auDescanso ) and ( StatusDiaAct = auSabado ) ) then
                iBrinco := 1;
        end;
        for j := 1 to iBrinco do
            Result := ZetaCommonTools.ConcatString( Result, K_FILLER, ',' );
   end;

begin
     sPatron := ZetaCommonClasses.VACIO;
     LastStatusDia := auDescanso;  // Para que el primer dia lo ponga en el ritmo como H�bil
     sHorarioAct  := oClientDataSet.FieldByName( 'TU_HOR_1' ).AsString;
     StatusDiaAct := eStatusAusencia( oClientDataSet.FieldByName( 'TU_TIP_1' ).AsInteger );
     iDias := 1;
     for i := 2 to ZetaCommonClasses.K_DIAS_SEMANA do
     begin
          sHorario  := oClientDataSet.FieldByName( Format( 'TU_HOR_%d', [ i ] ) ).AsString;
          StatusDia := eStatusAusencia( oClientDataSet.FieldByName( Format( 'TU_TIP_%d', [ i ] ) ).AsInteger );
          if ( sHorario = sHorarioAct ) and ( StatusDia = StatusDiaAct ) then
             Inc( iDias )
          else
          begin
               // Asigna Filler de Cambio de Status
               sPatron := ZetaCommonTools.ConcatString( sPatron, GetFillerStatus, ',' );
               // Asigna Dias con su horario
               sPatron := ZetaCommonTools.ConcatString( sPatron, Format( '%d:%s', [ iDias, sHorarioAct ] ), ',' );
               LastStatusDia := StatusDiaAct;
               sHorarioAct  := sHorario;
               StatusDiaAct := StatusDia;
               iDias := 1;
          end;
     end;
     if ( iDias > 0 ) then
     begin
          // Asigna Filler de Cambio de Status
          sPatron := ZetaCommonTools.ConcatString( sPatron, GetFillerStatus, ',' );
          // Asigna Dias con su horario
          sPatron := ZetaCommonTools.ConcatString( sPatron, Format( '%d:%s', [ iDias, sHorarioAct ] ), ',' );
     end;
     Result := ZetaCommonClasses.TOKEN_RITMO_HSD + sPatron;   // Se considera formato H-S-D
end;

procedure MuestraCalendarioRitmo( const sTurno: TCodigo; oClientDataSet: TZetaClientDataSet );
var
   dInicio: TDate;
   sPatron: String;
   oDatosRitmo: TDatosRitmo;
begin
     with oClientDataSet do
     begin
          if ( FieldByName( 'TU_CODIGO' ).AsString = sTurno ) or Locate( 'TU_CODIGO', sTurno, [] ) then
          begin
               if ZetaCommonTools.StrLleno( FieldByName( 'TU_RIT_PAT' ).AsString ) then // Turno Ritmico
               begin
                    dInicio := FieldByName( 'TU_RIT_INI' ).AsDateTime;
                    sPatron := FieldByName( 'TU_RIT_PAT' ).AsString;
               end
               else  // Turno Normal
               begin
                    dInicio := EncodeDate( 1990, 1, 1 );
                    dInicio := dInicio + ( Global.GetGlobalInteger( K_GLOBAL_PRIMER_DIA ) - DayOfWeek( dInicio ) );
                    sPatron := GetPatronNormal( oClientDataSet );
               end;
               with oDatosRitmo do
               begin
                    Inicio   := dInicio;
                    Patron   := sPatron;
                    Horario1 := FieldByName( 'TU_HOR_1' ).AsString;
                    Horario2 := FieldByName( 'TU_HOR_2' ).AsString;
                    Horario3 := FieldByName( 'TU_HOR_3' ).AsString;
               end;
               with dmCliente do
               begin
                    FCalendarioRitmo_DevEx.ShowCalendarioRitmo( sTurno, FieldByName( 'TU_DESCRIP' ).AsString, oDatosRitmo );
               end;
          end
          else
          begin
               ZetaDialogo.ZError( 'Error', 'No se Tiene Informaci�n del Turno: ' + sTurno, 0 );
          end;
     end;
end;


end.
