unit ThreadGraficasTRESS;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    ComObj,ActiveX, Db, DBClient,
    ZetaCommonClasses, ZetaClientDataSet, ZetaClientTools, DDashlets;
type
    TThreadGraficasTRESS = class(TThread)
    private
      FDatosGraficaRotacion: TZetaClientDataSet;
      FDatosGraficaAsistencia: TZetaClientDataSet;
      FDatosGraficaIndicadores: TZetaClientDataSet;
      FDashleetsConsulta: TdmDashleets;
      FQueryGraficaRotacion: String;
      FQueryGraficaAsistencia: String;
      FQueryIndicadores: String;
      FErroresAsistencia: String;
      FErroresRotacion: String;
      FErroresIndicadores: String;
      FFechaAsistenciaControl: TDate;
      procedure GeneraDatosGrafica;
      function GetStatusTerminate: boolean;
    protected
      procedure Execute; override;
    public
      constructor Create(lCreateSuspended: boolean; lOnTerminate: boolean);
      destructor Destroy; override;
      procedure TerminatedThread;
      property QueryGraficaRotacion: String read FQueryGraficaRotacion write FQueryGraficaRotacion;
      property QueryGraficaAsistencia: String read FQueryGraficaAsistencia write FQueryGraficaAsistencia;
      property QueryIndicadores: String read FQueryIndicadores write FQueryIndicadores;
      property DatosGraficaRotacion: TZetaClientDataSet read FDatosGraficaRotacion write FDatosGraficaRotacion;
      property DatosGraficaAsistencia: TZetaClientDataSet read FDatosGraficaAsistencia write FDatosGraficaAsistencia;
      property DatosGraficaIndicadores: TZetaClientDataSet read FDatosGraficaIndicadores write FDatosGraficaIndicadores;
      property ErroresAsistencia: String read FErroresAsistencia write FErroresAsistencia;
      property ErroresRotacion: String read FErroresRotacion write FErroresRotacion;
      property ErroresIndicadores: String read FErroresIndicadores write FErroresIndicadores;
      property StatusTerminate: boolean read GetStatusTerminate;
      property FechaAsistenciaControl : TDate read FFechaAsistenciaControl write FFechaAsistenciaControl;
    end;

implementation

{ TThreadGraficasTRESS }

constructor TThreadGraficasTRESS.Create(lCreateSuspended: boolean; lOnTerminate: boolean);
begin
     inherited Create( lCreateSuspended );
     FreeOnTerminate := lOnTerminate;
     FDatosGraficaRotacion   := TZetaClientDataSet.Create( Application );
     FDatosGraficaAsistencia := TZetaClientDataSet.Create( Application );
     FDatosGraficaIndicadores:= TZetaClientDataSet.Create( Application );
     FDashleetsConsulta := TdmDashleets.Create( Application );
     FFechaAsistenciaControl := NullDateTime;
end;

destructor TThreadGraficasTRESS.Destroy;
begin
     FreeAndNil( FDatosGraficaRotacion );
     FreeAndNil( FDatosGraficaAsistencia );
     FreeAndNil( FDatosGraficaIndicadores );
     FreeAndNil( FDashleetsConsulta );
     inherited;
end;

procedure TThreadGraficasTRESS.Execute;
begin
     try
        ZetaClientTools.InitDCOM;
        try
           while not Self.Terminated do
           begin
                GeneraDatosGrafica;
                TerminatedThread;
           end;
        except
              on Error: Exception do
              begin
                   TerminatedThread;
              end;
        end;
     finally
            ZetaClientTools.UnInitDCOM;
     end;
end;

procedure TThreadGraficasTRESS.GeneraDatosGrafica;
begin
     FDashleetsConsulta.GetDatosProcedure(FQueryGraficaRotacion, FErroresRotacion, FFechaAsistenciaControl, FDatosGraficaRotacion );
     FDashleetsConsulta.GetDatosProcedure(FQueryGraficaAsistencia, FErroresAsistencia, FFechaAsistenciaControl, FDatosGraficaAsistencia );
     FDashleetsConsulta.GetDatosProcedure(FQueryIndicadores, FErroresIndicadores, FFechaAsistenciaControl, FDatosGraficaIndicadores );
end;

function TThreadGraficasTRESS.GetStatusTerminate: boolean;
begin
     Result := Self.Terminated;
end;

procedure TThreadGraficasTRESS.TerminatedThread;
begin
     Self.Terminate;
end;

end.
