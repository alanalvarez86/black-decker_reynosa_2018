unit FThreadIndicadoresGraficasTRESS;

interface

uses
    Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
    ComObj,ActiveX, Db, DBClient,
    ZetaCommonClasses, ZetaClientDataSet, ZetaClientTools, DDashlets,
    FIndicadores,
    FGridPanel,
    ZetaDialogo;
type
    TInfoIndicadoresGraficasLoadAnimationCall = procedure( const iPosTablero: Integer; const iTableroID: Integer; const lInicioAnimacion: Boolean ) of object;
    TInfoIndicadoresGraficasCall = procedure( const sNombreComponente: String; const oDataSet: TZetaClientDataSet; const sError: String; const eClassEstilo: eEstiloDashlet; const iPosTablero: Integer; const iTableroID: Integer ) of object;
    TThreadIndicadoresGraficasTRESS = class( TThread )
    private
      FNombreComponente: String;
      FDatosConsultaDashlets: TZetaClientDataSet;
      FInfoProc: TInfoIndicadoresGraficasCall;
      FInfoProcAnimation: TInfoIndicadoresGraficasLoadAnimationCall;
      FDataSet: TZetaClientDataSet;
      FDashleetsConsulta: TdmDashleets;
      FError : String;
      FClassEstilo: eEstiloDashlet;
      FSesionIDDashlet: Integer;
      FPosTablero: Integer;
      FTableroID: Integer;
      FIniciarAnimation: Boolean;
      csCriticalSection: TRTLCriticalSection;
      Procedure CallCallBack;
      Procedure CallCallBackAnimation;
      function GetStatusTerminate: boolean;
      procedure GenerarDataSetInfo( const K_NAME: String );
      function GetSesionIDDashlet: Integer;
      procedure SetSesionIDDashlet(const Value: Integer);
      function GetPosTablero: Integer;
      procedure SetPosTablero(const Value: Integer);
    function GetTableroID: Integer;
    procedure SetTableroID(const Value: Integer);
    public
      constructor Create( Susp:Boolean; CallBack: TInfoIndicadoresGraficasCall; CallBackAnimation: TInfoIndicadoresGraficasLoadAnimationCall ); overload;
      destructor Destroy; override;
      property DatosConsultaDashlets: TZetaClientDataSet read FDatosConsultaDashlets write FDatosConsultaDashlets;
      property StatusTerminate: boolean read GetStatusTerminate;
      property SesionIDDashlet: Integer read GetSesionIDDashlet write SetSesionIDDashlet;
      property PosTablero: Integer read GetPosTablero write SetPosTablero;
      property TableroID: Integer read GetTableroID write SetTableroID;
    protected
      Procedure Execute;override;
    end;

implementation

{ TThreadGraficasTRESS }

procedure TThreadIndicadoresGraficasTRESS.CallCallBack;
begin
     if Assigned( FInfoProc ) then FInfoProc( FNombreComponente,FDataSet, FError, FClassEstilo, FPosTablero, FTableroID );
end;

procedure TThreadIndicadoresGraficasTRESS.CallCallBackAnimation;
begin
     if Assigned( FInfoProcAnimation ) then FInfoProcAnimation( FPosTablero, FTableroID, FIniciarAnimation );
end;

constructor TThreadIndicadoresGraficasTRESS.Create( Susp: Boolean; CallBack: TInfoIndicadoresGraficasCall; CallBackAnimation: TInfoIndicadoresGraficasLoadAnimationCall );
begin
     inherited Create( Susp );
     FInfoProc := CallBack;
     FInfoProcAnimation := CallBackAnimation;
     FDataSet := TZetaClientDataSet.Create( Application );
     FDashleetsConsulta := TdmDashleets.Create( Application );
     FError := VACIO;
     FIniciarAnimation := True;
end;

destructor TThreadIndicadoresGraficasTRESS.Destroy;
begin
     FreeAndNil( FDataSet );
     DeleteCriticalSection(csCriticalSection);
     inherited;
end;

procedure TThreadIndicadoresGraficasTRESS.Execute;
begin
     inherited;
     try
        InitializeCriticalSection(csCriticalSection);
        FIniciarAnimation := True;
        Synchronize(CallCallBackAnimation);
        if FDatosConsultaDashlets <> nil then
        begin
             ZetaClientTools.InitDCOM;
             with FDatosConsultaDashlets do
             begin
                  First;
                  while ( not EOF ) do
                  begin
                       if Terminated then
                          Break;
                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpListado )  then //Listado
                       begin
                            FClassEstilo := tpNingunoEstilo;
                            GenerarDataSetInfo( K_NAME_LISTADO );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpIndicador ) then //Indicadores
                       begin
                            FClassEstilo := tpNingunoEstilo;
                            GenerarDataSetInfo( K_NAME_INDICADOR );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpColumnas ) then //Grafica Columnas
                       begin
                            FClassEstilo := tpColumnas;
                            GenerarDataSetInfo( K_NAME_GRAFICA );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpBarras ) then //Grafica Barras
                       begin
                            FClassEstilo := tpBarras;
                            GenerarDataSetInfo( K_NAME_GRAFICA );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica  ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpPastel ) then //Grafica Pie
                       begin
                            FClassEstilo := tpPastel;
                            GenerarDataSetInfo( K_NAME_GRAFICA );
                       end;

                       if ( eTipoDashlet( FieldByName( 'DashletTipo' ).AsInteger ) = tpGrafica  ) and ( eEstiloDashlet( FieldByName( 'DashletEstilo' ).AsInteger ) = tpHorizStack ) then //Grafica Horz Stack
                       begin
                            FClassEstilo := tpHorizStack;
                            GenerarDataSetInfo( K_NAME_GRAFICA );
                       end;
                       Next;
                  end;
             end;
             ZetaClientTools.UnInitDCOM;
        end;
     except
           on Error: Exception do
           begin
                ZetaDialogo.ZError( 'Error', Error.Message, 0 );
           end;
     end;
     FIniciarAnimation := False;
     Synchronize(CallCallBackAnimation);
     LeaveCriticalSection(csCriticalSection);
     if not Terminated then
        Self.Terminate;
end;

procedure TThreadIndicadoresGraficasTRESS.GenerarDataSetInfo( const K_NAME: String );
begin
     try
        with FDatosConsultaDashlets do
        begin
             FNombreComponente := Format( K_NAME, [ FieldByName( 'DashletID' ).AsInteger ] );
             FDataSet := TZetaClientDataSet.Create( Application );
             FDataSet.Data := FDashleetsConsulta.EjecutarDashlet( FError, FSesionIDDashlet, FieldByName( 'DashletID' ).AsInteger);
             Synchronize(CallCallBack);
        end;
     except
           on Error: Exception do
           begin
                FError := Error.Message;
           end;
     end;
end;

function TThreadIndicadoresGraficasTRESS.GetPosTablero: Integer;
begin
     Result := FPosTablero;
end;

function TThreadIndicadoresGraficasTRESS.GetSesionIDDashlet: Integer;
begin
     Result := FSesionIDDashlet;
end;

function TThreadIndicadoresGraficasTRESS.GetStatusTerminate: boolean;
begin
     Result := Self.Terminated;
end;

function TThreadIndicadoresGraficasTRESS.GetTableroID: Integer;
begin
     Result := FTableroID;
end;

procedure TThreadIndicadoresGraficasTRESS.SetPosTablero(const Value: Integer);
begin
     Self.FPosTablero := Value;
end;

procedure TThreadIndicadoresGraficasTRESS.SetSesionIDDashlet( const Value: Integer );
begin
     Self.FSesionIDDashlet := Value;
end;

procedure TThreadIndicadoresGraficasTRESS.SetTableroID(const Value: Integer);
begin
     Self.FTableroID := Value;
end;

end.
