inherited AccesosRapidos: TAccesosRapidos
  Caption = 'Accesos R'#225'pidos'
  ClientHeight = 559
  ClientWidth = 1157
  OnShow = FormShow
  ExplicitWidth = 1157
  ExplicitHeight = 559
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 560
    Top = 128
    Width = 83
    Height = 13
    Caption = 'Accesos Rapidos'
  end
  inherited PanelIdentifica: TPanel
    Width = 1157
    ExplicitWidth = 1157
    inherited ValorActivo2: TPanel
      Width = 898
      ExplicitWidth = 898
      inherited textoValorActivo2: TLabel
        Left = 812
        ExplicitLeft = 812
      end
    end
  end
  object dxAccesos: TdxTileControl [2]
    Left = 0
    Top = 19
    Width = 1157
    Height = 540
    ActionBars.Font.Charset = DEFAULT_CHARSET
    ActionBars.Font.Color = clDefault
    ActionBars.Font.Height = -11
    ActionBars.Font.Name = 'Tahoma'
    ActionBars.Font.Style = []
    OptionsView.CenterContentHorz = True
    OptionsView.CenterContentVert = True
    OptionsView.FixedIndentHorz = True
    OptionsView.FixedIndentVert = True
    OptionsView.GroupLayout = glVertical
    OptionsView.GroupMaxRowCount = 3
    TabOrder = 1
    object dxGroup1: TdxTileControlGroup
      Indent = 200
      Index = 0
    end
    object dxAControl1: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 0
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl2: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 4
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl3: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 5
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl4: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 6
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl5: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 7
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl6: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 8
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl7: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 9
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl8: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 10
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl9: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 11
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl10: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 1
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl11: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 2
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
    object dxAControl12: TdxTileControlItem
      GroupIndex = 0
      IndexInGroup = 3
      Text1.AssignedValues = []
      Text2.AssignedValues = []
      Text3.AssignedValues = []
      Text4.AssignedValues = []
      Visible = False
    end
  end
  inherited DataSource: TDataSource
    Left = 544
    Top = 24
  end
end
