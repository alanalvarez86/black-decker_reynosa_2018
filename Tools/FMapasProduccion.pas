unit FMapasProduccion;

interface

uses Classes, Graphics, ZetaSmartLists,ZetaClientDataSet,DB,ADODB,SysUtils,ZetaCommonClasses,
     ZetaCommonLists,Dialogs,ZetaWinAPITools;


type
    TEstados = (Libre,Maquina,Silla,Certificado,Ausente,NoCertificado);


type
    TElemento = class (TObject)
private
       FCodigo:string;
       FEstado:TEstados;
       FImagen:TPicture;
public
      Constructor Crear;
      destructor Destroy; override;
      property Codigo : string read FCodigo write FCodigo;
      property Estado : TEstados read FEstado write FEstado;
      property Imagen : TPicture read FImagen write FImagen;
protected

end;

type
    TMaquina = class (TElemento)
private
       FNombre:string;
       FCertificaciones:TList;
       FMultiple:Boolean;
public
      Constructor Crear;
      Destructor Destroy; override;
      property Nombre : String read FNombre write FNombre;
      property Multiple : Boolean read FMultiple write FMultiple;
      property Certificaciones : TList read FCertificaciones write FCertificaciones;
protected
end;

type
    TCertificacion = class(TObject)
private
       FCodigo:string;
       FNombre:string;
        {$ifdef SUPERVISORES}
       Constructor Create;
       {$endif}
public
      Destructor Destroy;override;
      property Codigo : String read FCodigo write FCodigo;
      property Nombre : String read FNombre write FNombre;
protected

end;


type
    TSilla = class (TElemento)
private
       FMaquinas:TList;
       FEmpleado:Integer;
       FHora:string;
       //FStatus:
       function GetPosX:Integer;
       function GetPosY:Integer;
public
      destructor Destroy;override;
      Constructor Crear;
      property Maquinas:TList read FMaquinas write FMaquinas;
      property Empleado : Integer read FEmpleado write FEmpleado;
      property Hora : string read FHora write FHora;
      property PosX : Integer read GetPosX;
      property PosY : Integer read GetPosY;
      procedure QuitarMaquina(Maquina:string);
      function MaquinaAsignada:Boolean;
      function TieneAsignadaMaquina(Maquina:string):Boolean;
protected

end;



type
    TLayout = class (TObject)
private
       FCodigo:String;
       FArea:String;
       FLayNombre:string;
       FAreaDescrip:string;
public
      property Codigo:string read FCodigo write FCodigo;
      property Area : string read FArea write FArea;
      property LayNombre:string read FLayNombre write FLayNombre;
      property AreaDescrip:string read FAreaDescrip write FAreaDescrip;
protected

end;

var
    GridEstados: Array [0..100, 0..50] of TElemento;


   procedure CargarMatriz(DataSet,Maquinas:TZetaClientDataSet;imgEmpleado,imgAusente,imgCertificado,imgNoCertificado:TPicture);
   function BuscarMaquinaAsignadas(i,j:Integer;oSilla:TSilla):Boolean;
   procedure CargarSillas(dsLaySillas,dsSillasMaquinas:TZetaClientDataSet;imgSilla:TPicture);
   procedure CargarMaquinas(dsLayMaquinas,dsMaquinas:TZetaClientDataSet);
   function ExisteMaquina(Codigo:string):Boolean;
   procedure InicializarLayout;
   procedure LimpiarLayout;
   procedure RemoverAsignacionMaquina(maquina:string);
   function BuscarSillaAsignadas(var i,j:integer):Boolean;
   function BuscarSillasAsignadas(i,j:Integer;oMaquina:TMaquina):Boolean;
   {$ifdef SUPERVISORES}
   procedure CargarCertificacionesMaquina(oMaquina:TMaquina);
   {$endif}
   function EmpleadoYaOcupado(Empleado:Integer):Boolean;
   procedure GeneraImagen(var MS: TMemoryStream;var Maquina:TMaquina);
   function BuscarMaqMultiple(i,j:Integer;oMaquina:TMaquina):Boolean;
   function RemoverSillasAsignadas(i,j:Integer;oMaquina:TMaquina):Boolean;
   
CONST
     K_MAX_ROW = 50;
     K_MAX_COL = 100;
     JPEGstarts = 'FFD8';
     BMPstarts = '424D';  



implementation

uses DBClient{$ifdef SUPERVISORES},DLabor{$endif},ZetaCommonTools,jpeg;

{ TElemento }

constructor TElemento.Crear;
begin
     Codigo := VACIO;
     Estado := Libre;
//     FImagen := TPicture.Create;
end;

destructor TElemento.Destroy;
begin
    // FreeAndNil(FImagen);
     inherited destroy;
end;

{ TMaquina }
Constructor TMaquina.Crear;
begin
     inherited Crear;
     Nombre := VACIO;
 	 Multiple := False;
     FCertificaciones := TList.Create;
     FImagen := TPicture.Create;
end;

destructor TMaquina.Destroy;
var
   i:Integer;
begin
     if FCertificaciones <> nil then
     begin
          for i:= 0 to FCertificaciones.Count -1 do
          begin
               if TCertificacion( FCertificaciones[i]) <> nil then
                  TCertificacion(FCertificaciones[i]).Free;
          end;
          FreeAndNil(FCertificaciones);
          if FImagen <> nil then
             FreeAndNil(FImagen);
     end;
     inherited destroy;
end;


procedure CargarMaquinas(dsLayMaquinas,dsMaquinas:TZetaClientDataSet);
var
   i,j:Integer;
   MS: TMemoryStream;
   oMaquina:TMaquina;

begin
     try
        MS := TMemoryStream.Create;
        with dsLayMaquinas do
        begin
             First;
             while not eof do
             begin
                   MS.Clear;
                   oMaquina:= TMaquina.Crear;
                   i:= FieldByName('LM_POS_X').AsInteger;
                   j:= FieldByName('LM_POS_Y').AsInteger;
                   oMaquina.Estado := Maquina;
                   oMaquina.Codigo := FieldByName('MQ_CODIGO').AsString;
                   dsMaquinas.Locate('MQ_CODIGO',FieldByName('MQ_CODIGO').AsString,[]);
				           oMaquina.Multiple := zStrToBool(dsMaquinas.FieldByName('MQ_MULTIP').AsString);
                   TBlobField(dsMaquinas.FieldByName('MQ_IMAGEN')).SaveToStream(MS);
                   GeneraImagen(MS,oMaquina);
                   //FreeAndNil(GridEstados[i][j]);
                   GridEstados[i][j] := oMaquina;
                   Next;
             end;
        end;
     finally
            FreeAndNil(MS);
     end;
end;

{$ifdef SUPERVISORES}
procedure CargarCertificacionesMaquina(oMaquina:TMaquina);
var
   oCertificacion:TCertificacion;
begin
     with dmLabor do
     begin
          with cdsCertificMaq do
          begin
               Filtered:= False;
               Filter := Format('MQ_CODIGO = ''%s''',[oMaquina.Codigo]);
               Filtered:= True;
               First;
               while not eof do
               begin
                    oCertificacion := TCertificacion.Create;
                    oCertificacion.Codigo := FieldByName('CI_CODIGO').AsString;
                    //oCertificacion.Nombre := FieldByName('CI_NOMBRE').AsString;
                    oMaquina.Certificaciones.Add(oCertificacion);
                    Next;
               end;
          end;
     end;
end;



{ TCertificacion }

constructor TCertificacion.Create;
begin
     FCodigo := VACIO;
     FNombre := VACIO;
end;
{$ENDIF}

procedure CargarSillas(dsLaySillas,dsSillasMaquinas:TZetaClientDataSet;imgSilla:TPicture);
var
   i,j:Integer;
   oSilla: TSilla;
   oMaquina: TMaquina;

procedure CargarMaqAsignadas;
begin
     with dsSillasMaquinas do
     begin
          Filter := 'SL_CODIGO = '''+IntToStr(i)+','+IntToStr(j)+'''';
          Filtered:= True;
          First;
          while not eof do
          begin
               oMaquina := TMaquina.Crear;
               oMaquina.Codigo := FieldByName('MQ_CODIGO').AsString;
               {$ifdef SUPERVISORES}
               CargarCertificacionesMaquina(oMaquina);
               {$endif}
               oSilla.Maquinas.Add(oMaquina);
               Next;
          end;
          Filtered:= False;
     end;
end;

begin
     with dsLaySillas do
     begin
          First;
          while not eof do
          begin
               oSilla:= TSilla.Crear;
               oSilla.Maquinas.Clear;
                i:= FieldByName('SL_POS_X').AsInteger;
                j:= FieldByName('SL_POS_Y').AsInteger;

                oSilla.Estado := Silla;
                oSilla.Codigo := IntToStr(i)+','+IntToStr(j);
                CargarMaqAsignadas;

                FreeAndNil(GridEstados[i][j]);
                GridEstados[i][j] := oSilla;
                GridEstados[i][j].Imagen := imgSilla;
                Next;
          end;
     end;
end;



procedure CargarMatriz(DataSet,Maquinas:TZetaClientDataSet;imgEmpleado,imgAusente,imgCertificado,imgNoCertificado:TPicture);
var i,j:Integer;
    MS: TMemoryStream;
begin
      MS := TMemoryStream.Create;
     with DataSet do
     begin
          try
              First;
              while not Eof do
              begin
                   MS.Clear;
                   if FieldByName('LM_TIPO').AsInteger = Ord(Maquina) then
                   begin
                        i:= FieldByName('LM_POS_X').AsInteger;
                        j:= FieldByName('LM_POS_Y').AsInteger;
                        GridEstados[i][j].Codigo := FieldByName('MQ_CODIGO').AsString;
                        Maquinas.Locate('MQ_CODIGO',FieldByName('MQ_CODIGO').AsString,[]);
                        TBlobField(Maquinas.FieldByName('MQ_IMAGEN')).SaveToStream(MS);
                        GeneraImagen(MS,TMaquina(GridEstados[i][j]));
                        GridEstados[i][j].Estado := Maquina;
                   end
                   else
                   begin
                        i:= FieldByName('LM_POS_X').AsInteger;
                        j:= FieldByName('LM_POS_Y').AsInteger;
                        GridEstados[i][j].Codigo := FieldByName('SL_CODIGO').AsString;
                   end;
                   Next;
              end
          finally
                 MS.Free;
          end;
     end;
end;

procedure GeneraImagen(var MS: TMemoryStream;var Maquina:TMaquina);
var
    i: SmallInt;
    sFileName:string;
begin
     Randomize;
     i := Random(High(SmallInt));
     sFileName := Format( ZetaWinAPITools.GetTempDir + 'IMG_%d.jpg', [i] );
     MS.SaveToFile(sFileName);
     try
        Maquina.Imagen.LoadFromFile(sFileName);
     finally
            DeleteFile(sFileName);
     end;
end;

function BuscarMaquinaAsignadas(i,j:Integer;oSilla:TSilla):Boolean;
var
   x,y:Integer;
   oElemento:TElemento;
procedure AsignarMaquina;
begin
     if (x >= 0 ) and (y >= 0) then
     begin
          if GridEstados[x][y] = nil then
             GridEstados[x][y] := TElemento.Crear;
              
          if GridEstados[x][y].Estado = Maquina then
          begin
               oElemento := TElemento.Crear;
               oElemento.Codigo := GridEstados[x][y].Codigo;
               oSilla.Maquinas.Add(oElemento);
               Result := True;
          end;
     end;
end;

begin
     Result:= False;
      oSilla.Maquinas := TList.Create;
      y:= j;

      x:= i-1;
      AsignarMaquina;

      y := y-1;
      AsignarMaquina;

      x := x+1;
      AsignarMaquina;

      x := x+1;
      AsignarMaquina;

      y := y+1;
      AsignarMaquina;

      y := y+1;
      AsignarMaquina;

      x := x-1;
      AsignarMaquina;

      x := x-1;
      AsignarMaquina;

end;

function BuscarSillasAsignadas(i,j:Integer;oMaquina:TMaquina):Boolean;
var
   x,y:Integer;
procedure AsignarSilla;
begin
     if (x >= 0 ) and (y >= 0) then
     begin
          if GridEstados[x][y] = nil then
             GridEstados[x][y] := TElemento.Crear;

          if  GridEstados[x][y].Estado = Silla then
          begin
               TSilla(GridEstados[x][y]).Maquinas.Add(oMaquina);
               Result := True;
          end;
     end;
end;

begin
     Result:= False;
      y := j;

      x := i-1;
      AsignarSilla;

      y := y-1;
      AsignarSilla;

      x := x+1;
      AsignarSilla;

      x := x+1;
      AsignarSilla;

      y := y+1;
      AsignarSilla;

      y := y+1;
      AsignarSilla;

      x := x-1;
      AsignarSilla;

      x := x-1;
      AsignarSilla;
end;

function RemoverSillasAsignadas(i,j:Integer;oMaquina:TMaquina):Boolean;
var
   x,y:Integer;
procedure RemoverSilla(oSilla:TSilla);
begin
     if GridEstados[x][y] = nil then
        GridEstados[x][y] := TElemento.Crear;
     if( ( x>=0 )and( y>=0 ) ) and (GridEstados[x][y].Estado = Silla)then
     begin
          oSilla.QuitarMaquina(oMaquina.Codigo);
          Result := True;
     end;
end;

begin
     Result:= False;
      y:= j;
      x:= i-1;

      RemoverSilla(TSilla(GridEstados[x][y]));

      y := y-1;
      RemoverSilla(TSilla(GridEstados[x][y]));

      x := x+1;
      RemoverSilla(TSilla(GridEstados[x][y]));

      x := x+1;
      RemoverSilla(TSilla(GridEstados[x][y]));

      y := y+1;
      RemoverSilla(TSilla(GridEstados[x][y]));

      y := y+1;
      RemoverSilla(TSilla(GridEstados[x][y]));

      x := x-1;
      RemoverSilla(TSilla(GridEstados[x][y]));

      x := x-1;
      RemoverSilla(TSilla(GridEstados[x][y]));
end;

function ExisteMaquina(Codigo:string):Boolean;
var i,j:Integer;
begin
     Result := False;
     for i := 0 to K_MAX_COL -1 do
     begin
          for j := 0 to K_MAX_ROW -1 do
          begin
               if ( ( GridEstados[i][j] <> nil ) and ( GridEstados[i][j].Estado = Maquina ) )then
               begin
                    if ( TMaquina(GridEstados[i][j]).Codigo = Codigo ) then
                    begin
                         Result:= True;
                         Break;
                    end;
               end;
          end;
          if Result then
             Break;
     end;
end;

procedure InicializarLayout;
var
i,j:integer;
begin
//     LimpiarLayout;
     for i := 0 to K_MAX_COL  do
     begin
          for j := 0 to K_MAX_ROW do
          begin
               //GridEstados[i][j] := Nil;
               //GridEstados[i][j] := TElemento.Crear;
          end;
     end;
end;

function BuscarMaqMultiple(i,j:Integer;oMaquina:TMaquina):Boolean;
var
   x,y:Integer;

   procedure RevisaMaquina;
   begin
        if( x >= 0 )and (y >= 0 )then
        begin
             if ( (GridEstados[x][y].Estado = Maquina ) and ( oMaquina.Codigo = TMaquina(GridEstados[x][y]).Codigo ) and ( TMaquina(GridEstados[x][y]).Multiple ) )then
             begin
                  Result := True;
             end;
        end;
   end;

begin
     Result:= False;
      y:= j;
      x:= i-1;
      RevisaMaquina;

      y := y-1;
      RevisaMaquina;

      x := x+1;
      RevisaMaquina;

      x := x+1;
      RevisaMaquina;

      y := y+1;
      RevisaMaquina;

      y := y+1;
      RevisaMaquina;

      x := x-1;
      RevisaMaquina;

      x := x-1;
      RevisaMaquina;
end;

procedure LimpiarLayout;
var
i,j:integer;
begin
     for i := 0 to K_MAX_COL  do
     begin
          for j := 0 to K_MAX_ROW do
          begin
               if( GridEstados[i][j] <> nil )then
               try
                  FreeAndNil(GridEstados[i][j]);
               except

               end;
               //GridEstados[i][j] := nil;
          end;
     end;
end;



procedure RemoverAsignacionMaquina(maquina:string);
var
   i,j:integer;
begin
     for i := 0 to K_MAX_COL  do
     begin
          for j := 0 to K_MAX_ROW do
          begin
               if GridEstados[i][j].Estado = Silla then
                  TSilla(GridEstados[i][j]).QuitarMaquina(maquina);
          end;
     end;
end;

function EmpleadoYaOcupado(Empleado:Integer):Boolean;
var
   i,j:integer;
begin
     Result := False;
     for i := 0 to K_MAX_COL  do
     begin
          for j := 0 to K_MAX_ROW do
          begin
               if GridEstados[i][j] <> nil then
               begin
                     if GridEstados[i][j].Estado in [ Ausente, Certificado, NoCertificado] then
                        Result := TSilla(GridEstados[i][j]).Empleado = Empleado;
                     if Result then
                        break
               end;
          end;
          if Result then
             break
     end;
end;


function BuscarSillaAsignadas(var i,j:integer):Boolean;
var
   indi,indj:integer;
begin
     Result := True;
     for indi := 0 to K_MAX_COL  do
     begin
          for indj := 0 to K_MAX_ROW do
          begin
               if( ( GridEstados[indi][indj] <> nil ) and ( GridEstados[indi][indj].Estado = Silla ) )then
                  Result := TSilla(GridEstados[indi][indj]).MaquinaAsignada;
               if not Result then
               begin
                    i := indi;
                    j := indj;
                    break
               end
          end;
          if not Result then
                  break
     end;
end;

destructor TCertificacion.Destroy;
begin

  inherited destroy;
end;

{ TSilla }

constructor TSilla.Crear;
begin
     inherited Crear;
     FMaquinas := TList.Create;
     Empleado:= -1;
     FHora := VACIO;
end;

destructor TSilla.Destroy;
var
   i:Integer;
begin
     if FMaquinas <> nil then
     begin
          for i:= 0 to FMaquinas.Count- 1 do
          begin
               if TMaquina( FMaquinas[i] ) <> nil then
                  TMaquina( FMaquinas[i] ).Free;
          end;
          FreeAndNil( FMaquinas );
     end;
     inherited destroy;
end;

function TSilla.GetPosX: Integer;
VAR
   Lista:TStrings;
begin
     Lista := TStringList.Create;
     try
        Lista.CommaText := Codigo;
        Result := StrToInt(Lista[0]);
     finally
            Lista.Free;
     end;
end;

function TSilla.GetPosY: Integer;
VAR
   Lista:TStrings;
begin
     Lista := TStringList.Create;
     try
        Lista.CommaText := Codigo;
        Result := StrToInt(Lista[1]);
     finally
            Lista.Free;
     end;
end;

function TSilla.MaquinaAsignada: Boolean;
var
   i:Integer;
begin
     Result := False;
     for i := 0 to FMaquinas.Count - 1 do
     begin
          Result := FMaquinas[i] <> nil;
          if Result then
             Break;
     end;

end;

procedure TSilla.QuitarMaquina(Maquina:string);
var
   i:Integer;
begin
     for i := 0 to FMaquinas.Count - 1 do
     begin
          if TElemento(FMaquinas[i]) <> nil then
          begin
               if TElemento(FMaquinas[i]).Codigo = Maquina then
               begin
                    FMaquinas[i] := nil;
                    break;
               end
          end;
     end;
end;

function TSilla.TieneAsignadaMaquina(Maquina:string):Boolean;
var
   i:Integer;
begin
     Result := False;
     for i := 0 to FMaquinas.Count - 1 do
     begin
          Result := TElemento(FMaquinas[i]).Codigo = Maquina;
          if Result then
             break;
     end;
end;

end.
