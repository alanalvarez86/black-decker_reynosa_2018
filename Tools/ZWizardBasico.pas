unit ZWizardBasico;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls,
     ZetaWizard;

type
  TWizardBasico = class(TForm)
    PageControl: TPageControl;
    Wizard: TZetaWizard;
    Anterior: TZetaWizardButton;
    Siguiente: TZetaWizardButton;
    Ejecutar: TZetaWizardButton;
    Salir: TZetaWizardButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;
  TWizardBasicoClass = class of TWizardBasico;

var
  WizardBasico: TWizardBasico;

function ShowWizard( WizardClass: TWizardBasicoClass ): Boolean;

implementation

uses ZetaSmartLists;

{$R *.DFM}

function ShowWizard( WizardClass: TWizardBasicoClass ): Boolean;
begin
     with WizardClass.Create( Application ) as TWizardBasico do
     begin
          try
             ShowModal;
             Result := Wizard.Ejecutado;
          finally
                 Free;
          end;
     end;
end;

{ ********* TWizardBasico ********** }

procedure TWizardBasico.FormShow(Sender: TObject);
begin
     with Wizard do
     begin
          Reset;
          Primero;
     end;
end;

procedure TWizardBasico.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     case Key of
          Chr( VK_RETURN ):
          begin
               if ( not ( ActiveControl is TButton ) ) and
                  ( not ( ActiveControl is TZetaSmartListBox ) ) and
                  ( not ( ActiveControl is TCustomMemo ) ) then
               begin
                    Key := #0;
                    Perform( WM_NEXTDLGCTL, ( Hi( GetKeyState( VK_SHIFT ) ) and 1 ), 0 );
               end;
          end;
     end;
end;

end.
