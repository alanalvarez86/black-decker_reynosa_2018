unit ZetaEscogeAjustar_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal_DevEx, StdCtrls, Buttons, ExtCtrls, 
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore, dxSkinsDefaultPainters, ImgList,
  cxButtons, dxGDIPlusClasses;

type
  TEscogeAjustar_DevEx = class(TZetaDlgModal_DevEx)
    lMensajeAjustar: TLabel;
    lAdvertencia: TLabel;
    rbAjustar: TRadioButton;
    rbAjustarNueva: TRadioButton;
    Image1: TImage;
  private
    { Private declarations } 
    // FAjustarNueva : Boolean;
  public
    { Public declarations }
    // function EscogeTipoAjuste : Boolean;
  published
     // property AjustarNueva: Boolean read FAjustarNueva write FAjustarNueva;
  end;
            
function InitEscogeAjustar ( const sCaption, sMsg: String; const iHelpCtx: Longint; dInicio, dAjusteInicio : TDate;
         oDefaultBoton: TMsgDlgBtn ): Boolean;

var
  EscogeAjustar_DevEx: TEscogeAjustar_DevEx;

implementation

uses ZetaCommonTools;

{$R *.dfm}

function InitEscogeAjustar ( const sCaption, sMsg: String; const iHelpCtx: Longint; dInicio, dAjusteInicio : TDate;
         oDefaultBoton: TMsgDlgBtn ): Boolean;
begin
        EscogeAjustar_DevEx := TEscogeAjustar_DevEx.Create( Application );

        with EscogeAjustar_DevEx do
        begin
             Caption := sCaption;
             lMensajeAjustar.Caption := sMsg;
             rbAjustar.Caption := 'Ajustar Incapacidad del : ' + FormatDateTime ('DD/MMM/YYYY', dInicio);
             rbAjustarNueva.Caption := 'Ajustar Incapacidad Nueva del : ' + FormatDateTime ('DD/MMM/YYYY', dAjusteInicio);
             HelpContext := iHelpCtx;
             ShowModal;
             Result := ( ModalResult = mrOk );
        end;
end;

end.
