unit FEntradaDemoKit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls,ZetaCommonLists, dxSkinsCore,
  dxSkinsDefaultPainters, 
  cxLookAndFeels, dxSkinsForm, dxGDIPlusClasses,
  TressMorado2013, cxGraphics, cxLookAndFeelPainters, Menus, cxButtons;

type
  TzEntradaDemoKit = class(TForm)
    MensajeDemo: TMemo;
    Panel1: TPanel;
    DemoBtn: TBitBtn;
    Panel2: TPanel;
    Image1: TImage;
    MensajeDistrib: TMemo;
    Image2: TImage;
    DemoBtn_DevEx: TcxButton;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    TipoEntrada : Byte;
  end;

const
     ES_DEMO = 0;
     ES_KIT = 1;
var
   zEntradaDemoKit: TzEntradaDemoKit;

procedure EntradaVersionDemoKit( const iTipo: Byte );

implementation

{$R *.DFM}

uses DCliente;

procedure EntradaVersionDemoKit( const iTipo: Byte );
begin
     zEntradaDemoKit:= TzEntradaDemoKit.Create( Application );
     try
        with zEntradaDemoKit do
        begin
             TipoEntrada := iTipo;
             ShowModal;
        end;
     finally
            FreeAndNil( ZEntradaDemoKit );
     end;
end;

procedure TzEntradaDemoKit.FormShow(Sender: TObject);
begin
     MensajeDemo.Color := clWindow;
     MensajeDistrib.Color := clWindow;
     //Image1.Visible := False;
     Image2.Visible := True;
     DemoBtn_DevEx.Enabled := True;
     DemoBtn_DevEx.Visible := True;
     MensajeDemo.Visible:= ( TipoEntrada = ES_DEMO );
     MensajeDistrib.Visible:= ( TipoEntrada = ES_KIT );
end;

end.
