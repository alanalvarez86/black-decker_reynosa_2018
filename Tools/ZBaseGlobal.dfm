inherited BaseGlobal: TBaseGlobal
  Caption = 'BaseGlobal'
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    inherited OK: TBitBtn
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      OnClick = CancelarClick
    end
  end
end
