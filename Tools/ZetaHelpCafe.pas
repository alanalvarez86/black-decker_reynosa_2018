unit ZetaHelpCafe;

interface

const
     H00001_Configuracion_General = 22;
     H00002_Configuracion_Calendario = 23;
     H00024_Configuracion_TipoComida = 24;
     H00003_Configuracion_Servidor = 25;
     H00004_Configuracion_Seguridad = 26;
     H00005_Configuracion_Impresora = 27;
     H00006_Configuracion_Sonidos = 28;
     H00007_Registrando_un_consumo_de_Cafeteria	= 7;
     H00008_Activando_el_menu_Operador = 8;
     H00009_Ejecutando_manualmente_el_programa_Servidor	= 9;
     H00010_Parametros_de_impresion = 10;
     H00011_Servidor_fuera_de_linea = 11;
     H00012_Configuracion_Interfase_Serial = 29;
     H00030_Configuracion_Biometrico = 30;
     H00032_Configuracion_Cliente_Wizard = 33620;

implementation

end.
