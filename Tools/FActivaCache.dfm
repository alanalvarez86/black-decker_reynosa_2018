inherited ActivarCache: TActivarCache
  Top = 96
  Caption = 'Activación de Archivos Temporales'
  ClientHeight = 318
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LblFolder: TLabel [0]
    Left = 8
    Top = 51
    Width = 48
    Height = 13
    Caption = '&Directorio:'
    FocusControl = Folder
  end
  object BuscarFolder: TSpeedButton [1]
    Left = 342
    Top = 47
    Width = 25
    Height = 25
    Hint = 'Buscar Directorio'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
      300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
      330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
      333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
      339977FF777777773377000BFB03333333337773FF733333333F333000333333
      3300333777333333337733333333333333003333333333333377333333333333
      333333333333333333FF33333333333330003333333333333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = BuscarFolderClick
  end
  object GB_Sistema: TGroupBox [2]
    Left = 8
    Top = 96
    Width = 361
    Height = 65
    TabOrder = 3
    object LblSistema: TLabel
      Left = 8
      Top = 27
      Width = 48
      Height = 13
      Caption = '&Directorio:'
      FocusControl = FolderSistema
    end
    object BuscarFolderSistema: TSpeedButton
      Left = 328
      Top = 24
      Width = 25
      Height = 25
      Hint = 'Buscar Directorio'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BuscarFolderSistemaClick
    end
    object FolderSistema: TEdit
      Left = 64
      Top = 24
      Width = 257
      Height = 21
      TabOrder = 0
      OnChange = FolderChange
    end
  end
  inherited PanelBotones: TPanel
    Top = 282
    inherited OK: TBitBtn
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      OnClick = CancelarClick
    end
  end
  object CB_Activar: TCheckBox
    Left = 16
    Top = 16
    Width = 265
    Height = 17
    Caption = '&Activar Tablas Temporales para Empresa Activa'
    TabOrder = 1
    OnClick = CB_ActivarClick
  end
  object Folder: TEdit
    Left = 64
    Top = 48
    Width = 271
    Height = 21
    TabOrder = 2
    OnChange = FolderChange
  end
  object GB_Actualiza: TGroupBox
    Left = 7
    Top = 176
    Width = 361
    Height = 49
    Caption = ' Ultima Actualización '
    TabOrder = 5
    object SB_Refrescar: TSpeedButton
      Left = 264
      Top = 15
      Width = 89
      Height = 25
      Caption = '&Refrescar'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003333330B7FFF
        FFB0333333777F3333773333330B7FFFFFB0333333777F3333773333330B7FFF
        FFB0333333777F3333773333330B7FFFFFB03FFFFF777FFFFF77000000000077
        007077777777777777770FFFFFFFF00077B07F33333337FFFF770FFFFFFFF000
        7BB07F3FF3FFF77FF7770F00F000F00090077F77377737777F770FFFFFFFF039
        99337F3FFFF3F7F777FF0F0000F0F09999937F7777373777777F0FFFFFFFF999
        99997F3FF3FFF77777770F00F000003999337F773777773777F30FFFF0FF0339
        99337F3FF7F3733777F30F08F0F0337999337F7737F73F7777330FFFF0039999
        93337FFFF7737777733300000033333333337777773333333333}
      NumGlyphs = 2
      OnClick = SB_RefrescarClick
    end
    object LblRefrescar: TStaticText
      Left = 8
      Top = 21
      Width = 233
      Height = 17
      Alignment = taCenter
      AutoSize = False
      TabOrder = 0
    end
  end
  object lblArchivo: TStaticText
    Left = 184
    Top = 233
    Width = 166
    Height = 17
    AutoSize = False
    TabOrder = 7
    Visible = False
  end
  object BarraProgreso: TProgressBar
    Left = 25
    Top = 256
    Width = 325
    Height = 16
    Min = 0
    Max = 100
    Step = 1
    TabOrder = 8
    Visible = False
  end
  object LblModule: TStaticText
    Left = 24
    Top = 233
    Width = 153
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    TabOrder = 6
    Visible = False
  end
  object CB_Sistema: TCheckBox
    Left = 16
    Top = 96
    Width = 169
    Height = 17
    Caption = ' Tablas Temporales de &Sistema '
    TabOrder = 4
    OnClick = CB_SistemaClick
  end
end
