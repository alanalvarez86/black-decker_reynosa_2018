unit ZetaAsciiFile;

interface

uses
  SysUtils, Controls, Windows, Classes, DB, DBClient, Dialogs,
  {$ifdef TRESS_DELPHIXE5_UP}System.Types,{$endif}
  ZetaCommonLists, ZetaCommonClasses {$ifdef TRESS_DELPHIXE5_UP}, StrUtils, IOUtils{$endif};

type
{$IFDEF TRESS_DELPHIXE5_UP}
  TString = String;
{$ELSE}
  TString = String[ 1 ];
{$ENDIF}
  TAsciiExport = class;
  TASCIIExportEvent = procedure( Sender: TAsciiExport; DataSet: TDataset ) of object;

  TAsciiServer = class( TObject )
  private
    { Private declarations }
    FData: String;
    FFileSize: Integer;
    FRecordSize: Integer;
    function GetEof: Boolean;
    function GetRecordCount: Integer;
  protected
    { Protected declarations }
    FBuffer: TextFile;
    FFileName: String;
    FUsed: Boolean;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Bytes: Integer read FFileSize;
    property Data: String read FData;
    property EndOfFile: Boolean read GetEof;
    property FileName: String read FFileName;
    property RecordSize: Integer read FRecordSize;
    property RecordCount: Integer read GetRecordCount;
    property FileSize: Integer read FFileSize;
    property Used: Boolean read FUsed;
    function Open( const sFileName: String ): Boolean;
    function Read: Boolean;
    procedure Close;
    procedure Borrar;
  end;

  TAsciiLog = class( TAsciiServer )
  private
    { Private declarations }
    FErrors: Integer;
    FEvents: Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Errors: Integer read FErrors;
    property Events: Integer read FEvents;
    procedure Init( const sFileName: String );
    procedure WriteEvent(const iEmpleado: TNumEmp; const sMensaje: String);
    procedure WriteException(const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception);
    procedure WriteTexto(const sTexto: String); dynamic;
    procedure WriteTimeStamp( const sText: String );
    function GetTimeStamp: String;
  end;

  TAsciiOut = class( TObject )
  private
    { Private declarations }
    FRenglones: Integer;
    FOEMConvert: Boolean;
    FName: String;
    FFile: TextFile;
    FRenglon: String;
    FSeparador: TString;
    FDelimitador: TString;
    FUsed: Boolean;
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Delimitador: TString read FDelimitador write FDelimitador;
    property Name: String read FName;
    property OEMConvert: Boolean read FOEMConvert write FOEMConvert;
    property Renglones: Integer read FRenglones;
    property Separador: TString read FSeparador write FSeparador;
    function Abre(const sFileName: String; const lCanOverwrite: Boolean): Boolean;
    procedure AgregaRenglon( const sRenglon: String );
    procedure Cierra;
    procedure RenglonBegin;
    procedure RenglonEnd;
    procedure AgregaCampoString( const sTexto: String );
    procedure AgregaCampoInteger( const iNumero: Integer );
    procedure AgregaCampoFloat( const rNumero: TPesos );
    procedure AgregaCampoFecha( const dFecha: TDate );
    procedure AgregaFijoString( const sTexto: String; const iAncho: Integer );
    procedure AgregaFijoInteger( const iNumero, iAncho: Integer );
    procedure AgregaFijoFloat( const rNumero: TPesos; const iAncho: Integer );
    procedure AgregaFijoFecha( const dFecha: TDate; const iAncho: Integer );
    procedure AddCampo( const sTexto: String );
  end;

  TAsciiFile = class( TAsciiOut )
  private
    { Private declarations }
  public
    { Public declarations }
    constructor Create( const sFileName: String );
  end;
  TAsciiExportItem = class( TObject )
  private
    { Private declarations }
    FAncho: Word;
    FDatos: String;
    FNombre: String;
    FOwner: TList;
    FTipo: eTipoGlobal;
  public
    { Public declarations }
    constructor Create(const Lista: TList);
    destructor Destroy; override;
    property Ancho: Word read FAncho write FAncho;
    property Datos: String read FDatos write FDatos;
    property Nombre: String read FNombre write FNombre;
    property Tipo: eTipoGlobal read FTipo write FTipo;
  end;

  TAsciiExport = class( TAsciiOut )
  private
    { Private declarations }
    FAlExportarColumnas: TASCIIExportEvent;
    FLista: TList;
    FStatus: String;
    {$ifdef OSRAM_INTERFAZ}
    FFooter: string;
    procedure AgregaFooter( const eFormato: eFormatoASCII );
    {$endif}
    function GetColumn( Index: Integer ): TAsciiExportItem;
    function ColumnCount: Integer;
    procedure SetError( const sMensaje: String; Error: Exception );
    procedure SetStatus( const sValue: String );
  public
    { Public declarations }
    constructor Create; reintroduce; overload;
    destructor Destroy; override;
    property AlExportarColumnas: TASCIIExportEvent read FAlExportarColumnas write FAlExportarColumnas;
    property Column[ Index: Integer ]: TAsciiExportItem read GetColumn;
    property Status: String read FStatus;
    {$ifdef OSRAM_INTERFAZ}
    property Footer: string read FFooter write FFooter;
    {$endif}
    function ColumnByName( const sNombre: String ): TAsciiExportItem;
    function Exporta(const eFormato: eFormatoASCII; Dataset: TDataset): Integer;
    function HayError: Boolean;
    procedure AgregaColumna( const sNombre: String; const eTipo: eTipoGlobal; const iAncho: Integer );
    procedure Clear;
    procedure EscribeRenglon( const eFormato: eFormatoASCII );
  end;

implementation

uses ZetaCommonTools,
     ZetaDialogo;

{ ****************** TAsciiServer ************ }

{$I+}         { Input/Output-Checking Directive }

constructor TAsciiServer.Create;
begin
     inherited;
     FUsed := False;
     FFileName := '';
end;

destructor TAsciiServer.Destroy;
begin
     if Used then
        Close;
     inherited;
end;

function TAsciiServer.Open( const sFileName: String ): Boolean;
var
   SearchRec: TSearchRec;
   sArchivoSinRuta : string;
   sExtensionArchivo : string;
begin
     sArchivoSinRuta := VACIO;
     sExtensionArchivo := VACIO;
     if FileExists( sFileName ) and ( FindFirst( sFileName, faAnyFile, SearchRec ) = 0 ) then
     begin
          try
             AssignFile( FBuffer, sFileName );
             Reset( FBuffer );
             FFileSize := SearchRec.Size;
             FUsed := True;
             FFileName := sFileName;
             Result := Read;
          except
                on Error: Exception do
                begin
                      FUsed := False;
                      {$ifdef TRESS_DELPHIXE5_UP}
                      if AnsiContainsText(Error.Message, 'I/O error') then
                      begin
                           sExtensionArchivo := TPath.GetExtension (sFileName);
                           sArchivoSinRuta := TPath.GetFileNameWithoutExtension (sFileName);
                           sArchivoSinRuta := sArchivoSinRuta + sExtensionArchivo;
                           raise Exception.Create( Format( 'El archivo "%s" est� abierto en otra aplicaci�n, cierre e intente de nuevo', [sArchivoSinRuta]) );
                      end
                      else
                      begin
                              CloseFile( FBuffer );
                              raise;
                      end;
                      {$else}
                      CloseFile( FBuffer );
                      raise;
                      {$endif}
                end;
          end;
     end
     else
         raise Exception.Create( 'Archivo No Existe' );
end;

procedure TAsciiServer.Close;
begin
     if Used then
     try
          CloseFile( FBuffer );
          FUsed := False;
     except

     end;
end;

procedure TAsciiServer.Borrar;
begin
     Close;
     Erase( FBuffer );
end;

function TAsciiServer.GetEof: Boolean;
begin
     Result := Eof( FBuffer );
end;

function TAsciiServer.GetRecordCount: Integer;
begin
     if ( FRecordSize > 0 ) then
        Result := Trunc( FFileSize / FRecordSize )
     else
         Result := 0;
end;

function TAsciiServer.Read: Boolean;
begin
     if GetEof then
     begin
          FData := '';
          Result := False;
     end
     else
     begin
          Readln( FBuffer, FData );
          FRecordSize := Length( FData );
          Result := True;
     end;
end;

{ *********** TAsciiLog ********* }

function TAsciiLog.GetTimeStamp: String;
begin
     Result := FormatDateTime( 'ddd dd/mmm/yyyy hh:nn:ss AM/PM', Now );
end;

procedure TAsciiLog.Init( const sFileName: String );
begin

 // El C�digo fue comentado ya que pon�a el acceso al archivo en modo exclusivo,
 // CafeteraServicio y CafeteraServicioUI pueden leer y escribir al estar corriendo las
 // dos aplicaciones al mismo tiempo,
 // Se traslad� la l�gica de apertura/escritura al m�todo WriteTexto de esta misma clase

//     try
//        AssignFile( FBuffer, sFileName );
//        if FileExists( sFileName ) then
//           Append( FBuffer )
//        else
//            Rewrite( FBuffer );
//        FUsed := True;
//     except
//           on Error: Exception do
//           begin
//                FUsed := False;
//                CloseFile( FBuffer );
//                //raise;
//           end;
//     end;
     FFileName := sFileName;
     FErrors := 0;
     FEvents := 0;
end;

procedure TAsciiLog.WriteTexto( const sTexto: String );
begin
     if FFileName = '' then
       Exit;
     try
        try
           AssignFile( FBuffer, FFileName );
           if FileExists( FFileName ) then
              Append( FBuffer )
           else
              Rewrite( FBuffer );
           Writeln( FBuffer, sTexto );
        finally
               CloseFile( FBuffer );
        end;
     except
       // ToDO: Excepcion silenciosa
     end;
end;

procedure TAsciiLog.WriteEvent( const iEmpleado: TNumEmp; const sMensaje: String );
begin
     try
        if ( iEmpleado > 0 ) then
           WriteTexto( 'Empleado # ' + IntToStr( iEmpleado ) );
        WriteTexto( sMensaje );
        FEvents := FEvents + 1;
     except
           raise Exception.Create( 'Error al Escribir a Bit�cora' + CR_LF + 'Verifique Derechos De Acceso Sobre El Directorio' );
     end;
end;

procedure TAsciiLog.WriteException( const iEmpleado: TNumEmp; const sMensaje: String; Error: Exception );
begin
     try
        WriteTexto( sMensaje );
        if Assigned( Error ) then
        begin
             if Error is EReconcileError then
             begin
                  with EReconcileError( Error ) do
                  begin
                       WriteTexto( Message );
                       WriteTexto( 'Error # ' + IntToStr( ErrorCode ) + ': ' + Context )
                  end;
             end
             else
             begin
                  if ( iEmpleado > 0 ) then
                     WriteTexto( 'Empleado # ' + IntToStr( iEmpleado ) );
                  WriteTexto( GetExceptionInfo( Error ) );
             end;
        end;
        FErrors := FErrors + 1;
     except
           raise Exception.Create( 'Error al Escribir a Bit�cora' + CR_LF + 'Verifique Derechos De Acceso Sobre El Directorio' );
     end;
end;

procedure TAsciiLog.WriteTimeStamp( const sText: String );
begin
     WriteTexto( Format( sText, [ GetTimeStamp ] ) );
end;

{ ********** TAsciiOut *********** }

constructor TAsciiOut.Create;
begin
     FSeparador := ',';
     FDelimitador := '"';
     FOEMConvert := False;
     FRenglones := 0;
     FUsed := False;
end;

destructor TAsciiOut.Destroy;
begin
     Cierra;
     inherited;
end;

function TAsciiOut.Abre( const sFileName: String; const lCanOverwrite: Boolean ): Boolean;
begin
     FName := sFileName;
     try
        if FileExists( sFileName ) then
        begin
             if lCanOverwrite or ZetaDialogo.zConfirm( '� Archivo Ya Existe !', 'El Archivo ' + sFileName + ' Ya Existe' + CR_LF + '� Desea Sobreescribirlo ?', 0, mbYes ) then
             begin
                  Result := SysUtils.DeleteFile( sFileName );
             end
             else
                 Result := False;
        end
        else
            Result := True;
        if Result then
        begin
             AssignFile( FFile, FName );
             Rewrite( FFile );
        end;
        FUsed := Result;
     except
           on Error: Exception do
           begin
                ZetaDialogo.zExcepcion( '� Error Al Crear Archivo ASCII !', 'Error Al Abrir Archivo ' + sFileName, Error, 0 );
                Result := False;
           end;
     end;
end;

procedure TAsciiOut.Cierra;
begin
     if FUsed then
     begin
          CloseFile( FFile );
          FUsed := False;
     end;
end;

{*** (@am): Debido al cambio de tipos de dato de XE5 se modifica la funcion AgregaRenglon***}
{$IFDEF TRESS_DELPHIXE5_UP}
procedure TAsciiOut.AgregaRenglon(const sRenglon: string);
var
  pRenglon2:AnsiString;
begin
  if OEMConvert then begin
    SetLength(pRenglon2, Length(sRenglon));
    try
      if CharToOEM({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sRenglon), PAnsiChar(pRenglon2)) then
        Writeln(FFile, pRenglon2)
      else
         Writeln(FFile, sRenglon);
    finally
    end;
  end else
    Writeln(FFile, sRenglon);

  FRenglones := FRenglones + 1;
end;
{$ELSE}
procedure TAsciiOut.AgregaRenglon(const sRenglon: string);
var
  pRenglon, pRenglon2: PChar;
begin
  if OEMConvert then begin
    pRenglon := StrAlloc(Length(sRenglon) + 1);
    pRenglon2 := StrAlloc(Length(sRenglon) + 1);
    try
      pRenglon := StrPCopy(pRenglon, sRenglon);
      CharToOEM(pRenglon, PAnsiChar(pRenglon2));
      Writeln(FFile, pRenglon2);
    finally
      StrDispose(pRenglon);
      StrDispose(pRenglon2);
    end;
  end else
    Writeln(FFile, sRenglon);

  FRenglones := FRenglones + 1;
end;
{$ENDIF}
procedure TAsciiOut.RenglonBegin;
begin
     FRenglon := '';
end;

procedure TAsciiOut.RenglonEnd;
begin
     AgregaRenglon( FRenglon );
end;

procedure TAsciiOut.AgregaCampoString( const sTexto: String );
begin
     AddCampo( Delimitador + sTexto + Delimitador );
end;

procedure TAsciiOut.AgregaCampoInteger( const iNumero: Integer );
begin
     AddCampo( IntToStr( iNumero ) );
end;

procedure TAsciiOut.AgregaCampoFloat( const rNumero: TPesos );
begin
     AddCampo( FloatToStr( rNumero ) );
end;

procedure TAsciiOut.AgregaCampoFecha( const dFecha: TDate );
begin
     AddCampo( Delimitador + DateToStr( dFecha ) + Delimitador );
end;

procedure TAsciiOut.AgregaFijoString( const sTexto: String; const iAncho: Integer );
begin
     AddCampo( PadR( sTexto, iAncho ) );
end;

procedure TAsciiOut.AgregaFijoInteger( const iNumero, iAncho: Integer );
begin
     AddCampo( PadL( IntToStr( iNumero ), iAncho ) );
end;

procedure TAsciiOut.AgregaFijoFloat( const rNumero: TPesos; const iAncho: Integer );
begin
     AddCampo( PadL( FloatToStr( rNumero ), iAncho ) );
end;

procedure TAsciiOut.AgregaFijoFecha( const dFecha: TDate; const iAncho: Integer );
begin
     AddCampo( PadR( DateToStr( dFecha ), iAncho ) );
end;

procedure TAsciiOut.AddCampo( const sTexto: String );
begin
     if ( Length( FRenglon ) = 0 ) then
        FRenglon := sTexto
     else
         FRenglon := FRenglon + FSeparador + sTexto;
end;

{ ********** TAsciiFile ********** }

constructor TAsciiFile.Create( const sFileName: String );
begin
     inherited Create;
     Abre( sFileName, True );
end;

{ ********* TAsciiExportItem ********* }

constructor TAsciiExportItem.Create(const Lista: TList);
begin
     FOwner := Lista;
     FOwner.Add( Self );
end;

destructor TAsciiExportItem.Destroy;
begin
     FOwner.Remove( Self );
     inherited Destroy;
end;

{ ********* TAsciiExport ********* }

constructor TAsciiExport.Create;
begin
     inherited Create;
     FLista := TList.Create;
     FOEMConvert := True;
end;

destructor TAsciiExport.Destroy;
begin
     Clear;
     FreeAndNil( FLista );
     inherited Destroy;
end;

procedure TAsciiExport.Clear;
var
   i: Integer;
begin
     for i := ( ColumnCount - 1 ) to 0 do
     begin
          Column[ i ].Free;
     end;
end;

{$ifdef OSRAM_INTERFAZ}
procedure TAsciiExport.AgregaFooter(const eFormato: eFormatoASCII);
begin
     if (eFormato = faASCIIPuntoComa) then
     begin
           AgregaRenglon( FFooter );
     end;
end;
{$endif}

function TAsciiExport.ColumnByName( const sNombre: String ): TAsciiExportItem;
var
   i: Integer;
begin
     Result := nil;
     for i := 0 to ( ColumnCount - 1 ) do
     begin
          if ( Column[ i ].Nombre = sNombre ) then
          begin
               Result := Column[ i ];
               Break;
          end;
     end;
end;

function TAsciiExport.HayError: Boolean;
begin
     Result := ZetaCommonTools.StrLleno( FStatus );
end;

procedure TAsciiExport.SetStatus( const sValue: String );
begin
     FStatus := sValue;
end;

procedure TAsciiExport.SetError( const sMensaje: String; Error: Exception );
begin
     SetStatus( sMensaje + CR_LF + Error.Message );
end;

function TAsciiExport.GetColumn(Index: Integer): TAsciiExportItem;
begin
     if ( Index >= 0 ) and ( Index < ColumnCount ) then
        Result := TAsciiExportItem( FLista.Items[ Index ] )
     else
         Result := nil;
end;

function TAsciiExport.ColumnCount: Integer;
begin
     Result := FLista.Count;
end;

procedure TAsciiExport.AgregaColumna(const sNombre: String; const eTipo: eTipoGlobal; const iAncho: Integer);
begin
     with TAsciiExportItem.Create( FLista ) do
     begin
          Nombre := sNombre;
          Tipo := eTipo;
          Ancho := iAncho;
     end;
end;

function TAsciiExport.Exporta( const eFormato: eFormatoASCII; Dataset: TDataset ): Integer;
var
   lOk, lCanContinue: Boolean;
//   i: Integer;
begin
     SetStatus( '' );
     with Dataset do
     begin
          Active := True;
          First;
          Result := RecordCount;
          if ( Result <= 0 ) then
          begin
               SetStatus( 'No Hay Datos A Vaciar En El Archivo ASCII' );
          end
          else
          begin
               try
                  lCanContinue := True;
                  case eFormato of
                       faASCIIFijo:
                       begin
                            Separador := '';
                       end;
                       faASCIIDel:
                       begin
                            Separador := ',';
                       end;
                       {$ifdef OSRAM_INTERFAZ}
                       faASCIIPuntoComa:
                       begin
                            Separador := ';';
                       end;
                       {$endif}
                  end;
                  while not Eof and lCanContinue do
                  begin
                       lOk := False;
                       try
                          if Assigned( FAlExportarColumnas ) then
                             FAlExportarColumnas( Self, Dataset );
                          lOk := True;
                       except
                             on Error: Exception do
                             begin
                             end;
                       end;
                       if lOk then
                       begin
                            try
                               EscribeRenglon( eFormato );
{                               RenglonBegin;
                               for i := 0 to ( ColumnCount - 1 ) do
                               begin
                                    with Column[ i ] do
                                    begin
                                         case eFormato of
                                              faASCIIFijo:
                                              begin
                                                   case Tipo of
                                                        tgFloat: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                                                        tgNumero: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                                                        tgFecha: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                                                        tgTexto: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                                                   end;
                                              end;
                                              faASCIIDel:
                                              begin
                                                   case Tipo of
                                                        tgFloat: AddCampo( Datos );
                                                        tgNumero: AddCampo( Datos );
                                                        tgFecha: AddCampo( Delimitador + Datos + Delimitador );
                                                        tgTexto: AddCampo( Delimitador + Datos + Delimitador );
                                                   end;
                                              end;
                                         end;
                                    end;
                               end;
                               RenglonEnd; }
                            except
                                  on Error: Exception do
                                  begin
                                  end;
                            end;
                       end;
                       Next;
                  end;
                  {$ifdef OSRAM_INTERFAZ}
                  AgregaFooter( eFormato );
                  {$endif}
               except
                     on Error: Exception do
                     begin
                          SetError( 'Error Al Exportar Archivo ' + Name, Error );
                     end;
               end;
          end;
          Active := False;
     end;
end;

procedure TAsciiExport.EscribeRenglon(const eFormato: eFormatoASCII);
var
   i : Integer;
begin
     RenglonBegin;
     for i := 0 to ( ColumnCount - 1 ) do
     begin
          with Column[ i ] do
          begin
               case eFormato of
                    faASCIIFijo:
                    begin
                         case Tipo of
                              tgFloat: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                              tgNumero: AddCampo( ZetaCommonTools.PadL( Datos, Ancho ) );
                              tgFecha: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                              tgTexto: AddCampo( ZetaCommonTools.PadR( Datos, Ancho ) );
                         end;
                    end;
                    faASCIIDel:
                    begin
                         case Tipo of
                              tgFloat: AddCampo( Datos );
                              tgNumero: AddCampo( Datos );
                              tgFecha: AddCampo( Delimitador + Datos + Delimitador );
                              tgTexto: AddCampo( Delimitador + Datos + Delimitador );
                         end;
                    end;
                    {$ifdef OSRAM_INTERFAZ}
                    faASCIIPuntoComa:
                    begin
                         case Tipo of
                              tgFloat: AddCampo( Datos );
                              tgNumero: AddCampo( Datos );
                              tgFecha: AddCampo( Delimitador + Datos + Delimitador );
                              tgTexto: AddCampo( Delimitador + Datos + Delimitador );
                         end;
                    end;
                    {$endif}
               end;
          end;
     end;
     RenglonEnd;
end;

{$I-}         { Input/Output-Checking Directive }

end.
