unit ZBaseEdicionRenglon;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Grids, DBGrids, ComCtrls, Db,
     ExtCtrls, Buttons, DBCtrls, StdCtrls,
     ZetaClientDataset,
     ZBaseEdicion,
     ZetaDBGrid,
     ZetaMessages, ZetaSmartLists;

type
  TBaseEdicionRenglon = class(TBaseEdicion)
    Panel1: TPanel;
    PageControl: TPageControl;
    Datos: TTabSheet;
    Tabla: TTabSheet;
    GridRenglones: TZetaDBGrid;
    dsRenglon: TDataSource;
    Panel2: TPanel;
    BBAgregar: TBitBtn;
    BBBorrar: TBitBtn;
    BBModificar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBAgregarClick(Sender: TObject);
    procedure BBBorrarClick(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure GridRenglonesTitleClick(Column: TColumn);
  private
    { Private declarations }
    FPrimerColumna: Integer;
  protected
    { Protected declarations }
    property PrimerColumna: Integer read FPrimerColumna write FPrimerColumna;
    function ClientDatasetHijo: TZetaClientDataset;
    function GridEnfocado: Boolean;
    function CheckDerechosPadre: Boolean;
    function PosicionaSiguienteColumna: Integer; Virtual;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure HabilitaControles; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure SeleccionaPrimerColumna;
    procedure Setok;

  public
    { Public declarations }
  end;

var
  BaseEdicionRenglon: TBaseEdicionRenglon;

implementation

uses ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

procedure TBaseEdicionRenglon.FormCreate(Sender: TObject);
begin
     inherited;
     GridRenglones.Options := [ dgEditing,
                                dgTitles,
                                dgIndicator,
                                dgColumnResize,
                                dgColLines,
                                dgRowLines,
                                dgTabs,
                                dgCancelOnExit ];
     FPrimerColumna := 0;
end;

procedure TBaseEdicionRenglon.FormShow(Sender: TObject);
begin
     inherited;
     PageControl.ActivePage := Datos;
end;

procedure TBaseEdicionRenglon.HabilitaControles;
begin
     inherited HabilitaControles;
     { Impide que usuarios sin derechos de Modificación }
     { puedan llegar a modo de Edición simplemente cambiando }
     { el contenido de un control }
     dsRenglon.AutoEdit := Editing or CheckDerechos( K_DERECHO_CAMBIO );
end;

procedure TBaseEdicionRenglon.KeyDown( var Key: Word; Shift: TShiftState );
begin
     if GridEnfocado and not ( ClientDatasetHijo.State in [ dsInsert, dsEdit ] ) then
     begin
          if ( ssShift in Shift ) then { SHIFT }
          begin
               case Key of
                    VK_INSERT:    { INS = Agregar }
                    begin
                         Key := 0;
                         DoInsert;
                    end;
                    VK_DELETE:    { DEL = Borrar }
                    begin
                         Key := 0;
                         DoDelete;
                    end;
               end;
          end
          else
              if ( ssCtrl in Shift ) then { CTRL }
              begin
                   case Key of
                        VK_INSERT:   { INS = Modificar }
                        begin
                             Key := 0;
                             DoEdit;
                        end;
                   end;
              end;
     end;
     inherited KeyDown( Key, Shift );
end;

function TBaseEdicionRenglon.ClientDatasetHijo: TZetaClientDataset;
begin
     Result := TZetaClientDataset( dsRenglon.Dataset );
end;

function TBaseEdicionRenglon.CheckDerechosPadre: Boolean;
var
   sMensaje: String;
begin
     if Editing then
        Result := True
     else
         if PuedeModificar( sMensaje ) then
            Result := True
         else
         begin
              ZetaDialogo.zInformation( Caption, sMensaje, 0 );
              Result := False;
         end;
end;

function TBaseEdicionRenglon.GridEnfocado: Boolean;
begin
     Result := ( ActiveControl = GridRenglones );
end;

function TBaseEdicionRenglon.PosicionaSiguienteColumna: Integer;
var
   i: Integer;
begin
     with GridRenglones do
     begin
          i := SelectedIndex + 1;
          while ( i < Columns.Count ) and ( Columns[ i ].ReadOnly or not Columns[ i ].Visible ) do
          begin
               i := i + 1;
          end;
          if ( i = Columns.Count ) then
             Result := FPrimerColumna
          else
              Result := i;
     end;
end;

procedure TBaseEdicionRenglon.SeleccionaPrimerColumna;
begin
     Self.ActiveControl := GridRenglones;
     GridRenglones.SelectedField := GridRenglones.Columns[ FPrimerColumna ].Field;
end;

procedure TBaseEdicionRenglon.Setok;
begin
     if OK.Enabled then
        OK.SetFocus
     else
         Cancelar.SetFocus;
end;

procedure TBaseEdicionRenglon.Agregar;
begin
     if GridEnfocado then
        BBAgregar.Click
     else
         inherited Agregar;
end;

procedure TBaseEdicionRenglon.BBAgregarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               Append;
          end;
          GridRenglones.SetFocus;
     end;
end;

procedure TBaseEdicionRenglon.Borrar;
begin
     if GridEnfocado then
        BBBorrar.Click
     else
         inherited Borrar;
end;

procedure TBaseEdicionRenglon.BBBorrarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               if not IsEmpty then
                  Delete;
          end;
     end;
end;

procedure TBaseEdicionRenglon.Modificar;
begin
     if GridEnfocado then
        BBModificar.Click
     else
         inherited Modificar;
end;

procedure TBaseEdicionRenglon.BBModificarClick(Sender: TObject);
begin
     inherited;
     if CheckDerechosPadre then
     begin
          with ClientDatasetHijo do
          begin
               if not IsEmpty then
                  Edit;
          end;
          GridRenglones.SetFocus;
     end;
end;

procedure TBaseEdicionRenglon.GridRenglonesTitleClick(Column: TColumn);
begin
     inherited;
     { QUEDA VACIA HASTA QUE ENCONTREMOS UNA SOLUCION }
     { PARA QUE NO APAREZCAN TODO EL HIJO }
end;

end.
