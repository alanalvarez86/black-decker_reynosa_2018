unit FEscogeParametro_DevEx;

interface
               
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, StdCtrls, Buttons, ExtCtrls,
     ZBaseEscogeGrid_DevEx,
     ZetaDBGrid, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TEscogeParametro_DevEx = class(TBaseEscogeGrid_DevEx)
    np_nombre: TcxGridDBColumn;
    np_descrip: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function GetFormula: String; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

function PickParametro: String;

var
  EscogeParametro_DevEx: TEscogeParametro_DevEx;

implementation

uses DCatalogos,
     ZetaCommonClasses;

{$R *.DFM}

function PickParametro: String;
begin
     if ( EscogeParametro_DevEx = nil ) then
        EscogeParametro_DevEx:= TEscogeParametro_DevEx.Create( Application.MainForm );
     with EscogeParametro_DevEx do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
             Result := Formula;
     end;
end;

{ ******* TEscogeParametro ************** }

procedure TEscogeParametro_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H66213_Escoge_parametro;
     {
     SqlNumber:= Q_ESCOGE_PARAMETRO;
     }
end;

procedure TEscogeParametro_DevEx.Connect;
begin
     with dmCatalogos do
     begin
          cdsNomParamLookUp.Conectar;
          DataSource.DataSet := cdsNomParamLookUp;
     end;
end;

function TEscogeParametro_DevEx.GetFormula: String;
begin
     with dmCatalogos.cdsNomParamLookUp do
     begin
          if not IsEmpty then
             Result := FieldByName( 'NP_NOMBRE' ).AsString
          else
              Result := '';
     end;
     {
     if DatasetLleno( FQuery ) then
        Result:= FQuery.FieldByName( 'NP_NOMBRE' ).AsString;
     }
end;

end.
