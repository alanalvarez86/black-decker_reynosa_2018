inherited BaseSistBitacora: TBaseSistBitacora
  Left = 257
  Top = 361
  Caption = 'Bit'#225'cora'
  ClientHeight = 414
  ClientWidth = 644
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 644
    inherited Slider: TSplitter
      Left = 339
    end
    inherited ValorActivo1: TPanel
      Width = 323
    end
    inherited ValorActivo2: TPanel
      Left = 342
      Width = 302
    end
  end
  object PanelFiltros: TPanel [1]
    Left = 0
    Top = 19
    Width = 644
    Height = 122
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object lbUsuario: TLabel
      Left = 14
      Top = 78
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = Usuario
    end
    object Label2: TLabel
      Left = 11
      Top = 54
      Width = 24
      Height = 13
      Hint = 'Filtrar por Tipo de Evento'
      Alignment = taRightJustify
      Caption = '&Tipo:'
      FocusControl = TipoChk
      ParentShowHint = False
      ShowHint = True
    end
    object Label5: TLabel
      Left = 183
      Top = 54
      Width = 29
      Height = 13
      Hint = 'Filtrar por Tipo de Evento'
      Alignment = taRightJustify
      Caption = '&Clase:'
      FocusControl = TipoChk
      ParentShowHint = False
      ShowHint = True
    end
    object Tipo: TZetaKeyCombo
      Left = 56
      Top = 50
      Width = 121
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      DropDownCount = 4
      Enabled = False
      ItemHeight = 13
      TabOrder = 2
      ListaFija = lfTipoBitacora
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object TipoChk: TCheckBox
      Left = 39
      Top = 52
      Width = 15
      Height = 17
      Caption = 'TipoChk'
      TabOrder = 1
      OnClick = TipoChkClick
    end
    object Refrescar: TBitBtn
      Left = 367
      Top = 74
      Width = 58
      Height = 45
      Hint = 'Obtener Nuevamente La Bit'#225'cora Deseada'
      Caption = '&Refrescar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = RefrescarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      Layout = blGlyphTop
      NumGlyphs = 2
    end
    object Usuario: TZetaKeyLookup
      Left = 56
      Top = 74
      Width = 308
      Height = 21
      LookupDataset = dmSistema.cdsUsuarios
      TabOrder = 3
      TabStop = True
      WidthLlave = 50
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 425
      Height = 45
      Caption = ' Fecha de '
      TabOrder = 0
      object Label4: TLabel
        Left = 115
        Top = 19
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = '&Inicial:'
        FocusControl = FechaInicial
      end
      object Label3: TLabel
        Left = 280
        Top = 19
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = '&Final:'
        FocusControl = FechaInicial
      end
      object FechaInicial: TZetaFecha
        Left = 148
        Top = 14
        Width = 110
        Height = 22
        Cursor = crArrow
        TabOrder = 2
        Text = '02/Jan/98'
        Valor = 35797.000000000000000000
      end
      object FechaFinal: TZetaFecha
        Left = 308
        Top = 14
        Width = 110
        Height = 22
        Cursor = crArrow
        TabOrder = 3
        Text = '02/Jan/98'
        Valor = 35797.000000000000000000
      end
      object RBDeMovimiento: TRadioButton
        Left = 8
        Top = 26
        Width = 80
        Height = 17
        Caption = 'Movimiento'
        TabOrder = 1
        OnClick = RBDeMovimientoClick
      end
      object RBDeCaptura: TRadioButton
        Left = 8
        Top = 11
        Width = 80
        Height = 17
        Caption = 'Captura'
        Checked = True
        TabOrder = 0
        TabStop = True
        OnClick = RBDeCapturaClick
      end
    end
    object ClaseBit: TComboBox
      Left = 216
      Top = 50
      Width = 209
      Height = 21
      Style = csDropDownList
      DropDownCount = 15
      ItemHeight = 13
      Sorted = True
      TabOrder = 5
    end
  end
  object BitacoraPG: TPageControl [2]
    Left = 0
    Top = 141
    Width = 644
    Height = 273
    Align = alClient
    TabOrder = 2
  end
  object ZetaDBGrid: TZetaDBGrid [3]
    Left = 0
    Top = 141
    Width = 644
    Height = 273
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'BI_FECHA'
        Title.Caption = 'Fecha'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_HORA'
        Title.Caption = 'Hora'
        Width = 47
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_TIPO'
        Title.Caption = 'Tipo'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'Usuario'
        Width = 44
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_TEXTO'
        Title.Caption = 'Descripci'#243'n'
        Width = 272
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_FEC_MOV'
        Title.Caption = 'Fecha Mov.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'BI_NUMERO'
        Title.Caption = 'Proceso'
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 552
    Top = 32
  end
end
