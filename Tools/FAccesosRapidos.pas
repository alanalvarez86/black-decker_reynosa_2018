{$HINTS OFF}
unit FAccesosRapidos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZBaseConsulta, Data.DB, Vcl.StdCtrls,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, TressMorado2013, dxCustomTileControl, cxClasses, dxTileControl,
  ZetaRegistryCliente;

type
  TAccesosRapidos = class(TBaseConsulta)
    dxAccesos: TdxTileControl;
    dxGroup1: TdxTileControlGroup;
    dxAControl1: TdxTileControlItem;
    dxAControl2: TdxTileControlItem;
    dxAControl3: TdxTileControlItem;
    dxAControl4: TdxTileControlItem;
    dxAControl5: TdxTileControlItem;
    dxAControl6: TdxTileControlItem;
    dxAControl7: TdxTileControlItem;
    dxAControl8: TdxTileControlItem;
    dxAControl9: TdxTileControlItem;
    dxAControl10: TdxTileControlItem;
    dxAControl11: TdxTileControlItem;
    dxAControl12: TdxTileControlItem;
    Label1: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure OnItemClick(Sender: TdxTileControlItem);
    procedure ObtenerAccesos;
   protected
    procedure Connect; override;
    procedure Refresh; override;
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    //function PuedeRefrescar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }

  end;


const
  dxItems = 12;

var
  AccesosRapidos: TAccesosRapidos;

implementation

uses FTressShell, ZetaDialogo;

{$R *.dfm}

procedure TAccesosRapidos.Connect;
begin
end;

procedure TAccesosRapidos.Refresh;
begin
    //ObtenerAccesos;
end;

procedure TAccesosRapidos.FormShow(Sender: TObject);
begin
  inherited;
  ObtenerAccesos;

  dxAccesos.OptionsBehavior.ItemMoving := False; //Remueve el DragNDrop
end;

procedure TAccesosRapidos.OnItemClick(Sender: TdxTileControlItem);
begin
     //TressShell.ActionList[Sender.Tag].OnExecute(Self);
     // TressShell.DevEx_BarManager.Items[Sender.Tag].OnClick(Self);
     if (TressShell.DevEx_BarManager.Items[Sender.Tag].Enabled) then
          TressShell.DevEx_BarManager.Items[Sender.Tag].OnClick(Self)
     else
          ZetaDialogo.ZError( self.Caption, 'No tiene derecho sobre esa pantalla.', 0 );
end;


procedure TAccesosRapidos.ObtenerAccesos;
    procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
    begin
       ListOfStrings.Clear;
       ListOfStrings.Delimiter     := Delimiter;
       ListOfStrings.DelimitedText := Str;
    end;

var
  registro       : string;
  AccesoSeccion  : TStringList;
  i, max      : integer;
  etiqueta       : string;
 // imagen         : TMemoryStream;
begin
    max := AccesoFuturo.Count;
    dxAccesos.Images := TressShell.RibbonsLargeImages; //Estable el origen de las imagenes

    for i := 0 to max-1 do
    begin
          AccesoSeccion := TStringList.Create;
          Split(':', AccesoFuturo[i], AccesoSeccion);
          if (AccesoSeccion.Count > 2) then
          begin
              dxAccesos.Items[i].Visible := True;
              dxAccesos.Items[i].Glyph.ImageIndex := StrToInt(AccesoSeccion[2]);

                    //if StrToInt(AccesoSeccion[2]) = -1 then
                      //imagen := TMemoryStream.Create;

                  //TressShell.DevEx_BarManager.Items[StrToInt(AccesoSeccion[3])].Glyph.
                     // dxAccesos.Items[i].Glyph.Image.load;//  LoadFromStream(imagen);
                      //imagen.Free;
                  //  begin

                  //  end;
              dxAccesos.Items[i].Style.Gradient := gmVertical;
              dxAccesos.Items[i].Style.GradientBeginColor := clWhite;
              dxAccesos.Items[i].Style.GradientEndColor := clWhite;
              etiqueta := StringReplace (TressShell.DevEx_BarManager.Items[StrToInt(AccesoSeccion[0])].Caption, '&', '',  [rfReplaceAll, rfIgnoreCase]);
              dxAccesos.Items[i].Text1.Value := etiqueta;
              dxAccesos.Items[i].Text1.Align := oaBottomCenter;
              dxAccesos.Items[i].Style.Font.Color := clBackground;
              dxAccesos.Items[i].Tag := StrToInt(AccesoSeccion[0]);
              dxAccesos.Items[i].OnClick := OnItemClick;
          end;
    end;
end;

function TAccesosRapidos.PuedeAgregar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden agregar accesos r�pidos desde esta forma';
     Result := False;
end;

function TAccesosRapidos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden borrar accesos r�pidos desde esta forma';
     Result := False;
end;

function TAccesosRapidos.PuedeModificar(var sMensaje: String): Boolean;
begin
     sMensaje := 'No se pueden modificar accesos r�pidos desde esta forma';
     Result := False;
end;

end.
