unit ZFiltroSQLTools;


interface

uses RegExpr, SysUtils;

function FiltrarTextoSQL ( sTexto : string ) : string ;

implementation


function FiltrarTextoSQL ( sTexto : string ) : string ;
const
     K_CARACTERES_LATINOS = '���������������';
     K_EXPRESION_FILTRO = '[\w\W\s]*';
var
   cleanRegexp : TRegExpr;
   {$ifndef TRESS_DELPHIXE5_UP}
   sRegExprWordCharsTmp : string;
   {$endif}

begin
      {***(@am): Anteriormente se modificaba la constante RegExprWordChars directamente
                para incluir los caracteres latinos. En XE5 ya no se permite.
                Por lo tanto despues de crear el objeto "cleanregexp" (durante la creacion
                de este objeto se le asigna a su propiedad WordChars el valor d ela constante)
                modificamos la propiedad WordChars para realizar la operacion. Y despues le
                volvemos a asignar la constante. ***}
     {$IFDEF TRESS_DELPHIXE5_UP}
     cleanregexp := TRegExpr.Create;
     //(@am): Modificamos el valor para utilizar los caracteres latinos.
     cleanregexp.WordChars := cleanregexp.WordChars + K_CARACTERES_LATINOS;
     cleanregexp.Expression := K_EXPRESION_FILTRO ;

     Result := '';

     try
         if ( cleanregexp.Exec( sTexto) ) then
         begin
              repeat
                    Result := Result + cleanregexp.Match[0];
              until  not cleanregexp.ExecNext;
         end;
         //(@am): Devolvef el valor original ya no es necesario pues destruiremos el objeto y la constante en si no fue modificada.
         //cleanregexp.WordChars := RegExprWordChars ;
     finally
         FreeAndNil(cleanRegexp ) ;
     end;
     {$ELSE}
     sRegExprWordCharsTmp := RegExprWordChars;
     RegExprWordChars := RegExprWordChars + K_CARACTERES_LATINOS;
     cleanregexp := TRegExpr.Create;
     cleanregexp.Expression := K_EXPRESION_FILTRO ;

     Result := '';

     try
         if ( cleanregexp.Exec( sTexto) ) then
         begin
              repeat
                    Result := Result + cleanregexp.Match[0];
              until  not cleanregexp.ExecNext;
         end;
         RegExprWordChars :=  sRegExprWordCharsTmp;
     finally
         FreeAndNil(cleanRegexp ) ;
     end;
     {$ENDIF}
end;


end.
 