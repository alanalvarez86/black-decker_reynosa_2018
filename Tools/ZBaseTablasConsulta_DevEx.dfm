inherited TablaOpcion_DevEx: TTablaOpcion_DevEx
  Left = 172
  Top = 331
  Caption = 'TablaOpcion_DevEx'
  ClientHeight = 286
  ClientWidth = 554
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 554
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 228
      inherited textoValorActivo2: TLabel
        Width = 222
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 554
    Height = 267
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
        MinWidth = 70
        Width = 70
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 90
        Width = 90
      end
      object TB_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TB_INGLES'
        MinWidth = 65
        Width = 65
      end
      object TB_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'TB_NUMERO'
        MinWidth = 70
      end
      object TB_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'TB_TEXTO'
        MinWidth = 65
        Width = 65
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 32
    Top = 96
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
