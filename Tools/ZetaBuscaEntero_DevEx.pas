unit ZetaBuscaEntero_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, DB, Mask,
     {$ifndef VER130}MaskUtils,{$endif}
     ZetaClientDataset,     
     ZetaNumero, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons;

type
  TBuscaEntero_DevEx = class(TZetaDlgModal_DevEx)
    CodigoLBL: TLabel;
    NoExisteLBL: TLabel;
    Codigo: TMaskEdit;
    procedure CodigoChange(Sender: TObject);
    procedure OKClick_DevEx(Sender: TObject);
  private
    { Private declarations }
    FCampo: String;
    FDataset: TZetaClientDataset;
    function Buscar: Boolean;
    procedure SetCampo( const Value: String );
    procedure SetNombreLlave( const Value: String );
    procedure SetNombreTabla(const Value: String);
  public
    { Public declarations }
    property Campo: String read FCampo write SetCampo;
    property Dataset: TZetaClientDataset read FDataset write FDataset;
    property NombreLlave: String write SetNombreLlave;
    property NombreTabla: String write SetNombreTabla;
  end;

procedure BuscarCodigo( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset );
//function BuscarCodigoValor( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset; var iValor: integer ): Boolean;


var
  BuscaEntero_DevEx: TBuscaEntero_DevEx;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZetaBuscador_DevEx;

{$R *.DFM}

procedure BuscarCodigo( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset );
begin
     if Tabla.IsEmpty then
        ZetaDialogo.zInformation( 'B�squeda de ' + sLlave, 'No Hay Datos Para Buscar', 0 )
     else
     begin
          if ( Tabla is TZetaLookupDataSet) then
          begin
               ZetaBuscador_DevEx.BuscarCodigo( sLlave, sTabla, sCampo, Tabla );
          end
          else
          begin
               with TBuscaEntero_DevEx.Create( Application ) do
               begin
                    try
                       Dataset := Tabla;
                       NombreLlave := sLlave;
                       NombreTabla := sTabla;
                       Campo := sCampo;
                       ShowModal;
                    finally
                           Free;
                    end;
               end;
          end;
     end;
end;
{
function BuscarCodigoValor( const sLlave, sTabla, sCampo: String; Tabla: TZetaClientDataset; var iValor: integer ): boolean;
begin
     Result := False;
     if Tabla.IsEmpty then
        ZetaDialogo.zInformation( 'B�squeda de ' + sLlave, 'No Hay Datos Para Buscar', 0 )
     else
     begin
          with TBuscaEntero.Create( Application ) do
          begin
               try
                  Campo := sCampo;
                  Dataset := Tabla;
                  NombreLlave := sLlave;
                  NombreTabla := sTabla;
                  ShowModal;
                  iValor := DataSet.FieldByName( FCampo ).AsInteger;
                  Result := iValor <> 0;
               finally
                      Free;
               end;
          end;
     end;
end;
}
{ ******** TBuscador ******* }

procedure TBuscaEntero_DevEx.SetCampo(const Value: String);
begin
     FCampo := Value;
end;

procedure TBuscaEntero_DevEx.SetNombreLlave(const Value: String);
begin
     CodigoLBL.Caption := Value + ':';
end;

procedure TBuscaEntero_DevEx.SetNombreTabla(const Value: String);
begin
     Self.Caption := 'Buscar ' + Value;
end;

procedure TBuscaEntero_DevEx.CodigoChange(Sender: TObject);
begin
     inherited;
     NoExisteLBL.Visible := False;
end;

function TBuscaEntero_DevEx.Buscar: Boolean;
begin
     Result := Dataset.Locate( FCampo, StrToIntDef( Codigo.Text, 0 ), [ loPartialKey ] );
end;

procedure TBuscaEntero_DevEx.OKClick_DevEx(Sender: TObject);
begin
     inherited;
     Codigo.Perform( CM_EXIT, 0, 0 ); { Forza el Exit del ZetaNumero }
     if Buscar then
        Close
     else
     begin
          ZetaCommonTools.Chicharra;
          NoExisteLBL.Visible := True;
          ActiveControl := Codigo;
     end;
end;

end.
