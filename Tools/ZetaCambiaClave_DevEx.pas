unit ZetaCambiaClave_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Mask, Buttons, ExtCtrls,
     {$ifndef VER130}MaskUtils,{$endif}
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
  cxButtons;

type
  TZCambiaClave_DevEx = class(TZetaDlgModal_DevEx)
    NPassWordLBL: TLabel;
    NPassWord2LBL: TLabel;
    NPassWord: TMaskEdit;
    NPassWord2: TMaskEdit;
    Label1: TLabel;
    OPassWord: TMaskEdit;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function GetOldPassword: String;
    function GetNewPassword: String;
    function GetConfirmation: String;
    procedure ResetControles( const sMensaje: String );
  public
    { Public declarations }
  end;

var
  ZCambiaClave_DevEx: TZCambiaClave_DevEx;

implementation

uses dCliente, ZetaDialogo, ZetaCommonClasses;

{$R *.DFM}

procedure TZCambiaClave_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     if ( ModalResult = mrOK ) then
     begin
          CanClose := False; // Default //
          if ( Length( GetOldPassword ) = 0 ) then
          begin
               OPassWord.SetFocus;
               Beep;
          end
          else if ( Length( GetNewPassword ) = 0 ) then
          begin
               NPassWord.SetFocus;
               Beep;
          end
          else if ( Length( GetConfirmation ) = 0 ) then
          begin
               NPassWord2.SetFocus;
               Beep;
          end
          else
          begin
               CanClose := ( GetNewPassword = GetConfirmation );
               if not CanClose then
                  ResetControles( 'La Nueva Clave y su Confirmación Son Diferentes' +
                                  CR_LF +  'Por Favor Teclee Clave de Nuevo' )
               else
                   with Screen do
                   begin
                        oCursor := Cursor;
                        Cursor := crHourglass;
                        try
                           try
                              dmCliente.ActualizaClaveUsuario( GetOldPassword, GetNewPassword );
                           except
                              on Error: Exception do
                              begin
                                   CanClose := FALSE;
                                   ResetControles( Error.message );
                              end;
                           end;
                        finally
                           Cursor := oCursor;
                        end;
                   end;
          end;
     end
     else
         CanClose := True;
end;

function TZCambiaClave_DevEx.GetOldPassWord: String;
begin
     Result := OPassWord.Text;
end;

function TZCambiaClave_DevEx.GetNewPassWord: String;
begin
     Result := NPassWord.Text;
end;

function TZCambiaClave_DevEx.GetConfirmation: String;
begin
     Result := NPassWord2.Text;
end;

procedure TZCambiaClave_DevEx.ResetControles( const sMensaje: String );
begin
     OPassWord.Text  := VACIO;
     NPassWord.Text  :=  VACIO;
     NPassWord2.Text := VACIO;
     ZError( VACIO, sMensaje, 0 );
     OPassWord.SetFocus;
end;

procedure TZCambiaClave_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H80838_Cambiando_su_clave_de_usuario;
end;

end.
