unit ZetaWizardFeedBack;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Buttons,
     ZBaseThreads,
     ZBaseDlgModal,
     ZetaDBTextBox;

type
  TWizardFeedback = class(TZetaDlgModal)
    Imagen: TImage;
    InicioLBL: TLabel;
    Inicio: TZetaTextBox;
    FinLBL: TLabel;
    Fin: TZetaTextBox;
    Duracion: TZetaTextBox;
    DuracionLBL: TLabel;
    MaximoLBL: TLabel;
    Maximo: TZetaTextBox;
    Avance: TZetaTextBox;
    AvanceLBL: TLabel;
    EmpleadoLBL: TLabel;
    Empleado: TZetaTextBox;
    FolioLBL: TLabel;
    Folio: TZetaTextBox;
    ProcesoLBL: TLabel;
    Proceso: TZetaTextBox;
    Status: TZetaTextBox;
    StatusLBL: TLabel;
    MensajesLBL: TLabel;
    Mensajes: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FProcessData: TProcessInfo;
    FOcultarForma: Boolean;
    FVerBitacora: Boolean;
    procedure SetBotonDefault(const lOK: Boolean);
  public
    { Public declarations }
    property ProcessData: TProcessInfo read FProcessData;
    property OcultarForma: Boolean read FOcultarForma write FOcultarForma;
    property VerBitacora: Boolean read FVerBitacora write FVerBitacora default TRUE;
    function ProcessOK: Boolean;
    function ShowProcessInfo(const lMostrar: Boolean): Integer;
    procedure SetMensajes( const sValue: String );
  end;

function GetProcessSpeed( const iMaximo: Integer; const dStart, dEnd: TDateTime ): String;

implementation

uses ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

const
     K_ALTURA_FORMA = 315; //+15 Para corregir el problema en los Large Fonts

{$R *.DFM}

function GetProcessSpeed( const iMaximo: Integer; const dStart, dEnd: TDateTime ): String;
var
   iHour, iMin, iSec, iMSec: Word;
   dValue: TDateTime;
begin
     dValue := ZetaClientTools.GetElapsed( dEnd, dStart );
     if ( dValue > 0 ) and ( iMaximo > 0 ) then
     begin
          DecodeTime( dValue, iHour, iMin, iSec, iMSec );
          iSec := 86400 * Trunc( dValue ) + ( 3600 * iHour ) + ( 60 * iMin ) + iSec;
          if ( iSec > 0 ) then
             Result := Result + Format( '%s / min', [ Trim( Format( '%8.0n', [ 60 * ( iMaximo / iSec )  ] ) ) ] )
          else
              Result := '';
     end
     else
         Result := '';
end;

{ *********** TWizardResults ********* }

procedure TWizardFeedback.FormCreate(Sender: TObject);
begin
     FProcessData := TProcessInfo.Create( nil );
     inherited;
     with Imagen do
     begin
          Imagen.Picture.Icon.Handle := LoadIcon( 0, IDI_HAND );
     end;
     Height := GetScaledHeight( K_ALTURA_FORMA );
     FOcultarForma := FALSE;
     FVerBitacora := TRUE;
end;

procedure TWizardFeedback.FormDestroy(Sender: TObject);
begin
     inherited;
     FProcessData.Free;
end;

procedure TWizardFeedback.FormShow(Sender: TObject);
const
     aIconIDs: array[ eProcessStatus ] of PChar = ( IDI_QUESTION, IDI_ASTERISK, IDI_EXCLAMATION, IDI_HAND, IDI_HAND );
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
var
   dDuracion: TDateTime;

procedure SetImagen( const pIcon: PChar );
begin
     Imagen.Picture.Icon.Handle := LoadIcon( 0, pIcon );
end;

procedure SetStatus( const sMensaje: String; const eColor: TColor );
begin
     with Self.Status do
     begin
          Font.Color := eColor;
          Caption := sMensaje;
     end;
end;

function SetPlural( const sPlural: String; const iValor: Integer ): String;
begin
     if ( iValor = 1 ) then
        Result := ''
     else
         Result := sPlural;
end;

function GetBenchMark( const iMaximo: Integer; const dStart, dEnd: TDateTime ): String;
var
   sSpeed: String;
begin
     Result := Trim( Format( '%8.0n', [ iMaximo / 1 ] ) );
     sSpeed := GetProcessSpeed( iMaximo, dStart, dEnd );
     if ( sSpeed > '' ) then
        Result := Result + Format( ' ( %s )', [ sSpeed ] );
end;

begin
     inherited;

     Cancelar.Visible := FVerBitacora;
     with FProcessData do
     begin
          case Status of
               epsEjecutando:
               begin
                    if FVerBitacora then
                    begin
                         Caption := '� Proceso Incompleto !';
                         SetStatus( 'No Ha Terminado', clRed );
                    end
                    else
                    begin
                         Caption := '� Proceso Terminado !';
                         SetStatus( 'Sin Empleados', clRed );
                    end;
                    SetBotonDefault( True );
                    SetImagen( AIconIDs[ epsEjecutando ] );
               end;
               epsOK:
               begin
                    Caption := '� Proceso Terminado !';
                    SetBotonDefault( True );
                    if ( TotalErrores > 0 ) then
                    begin
                         SetImagen( AIconIDs[ epsError ] );
                         SetStatus( Format( 'Termin� Con %d Error%s', [ TotalErrores, SetPlural( 'es', TotalErrores ) ] ), clRed );
                    end
                    else
                        if ( TotalAdvertencias > 0 ) then
                        begin
                             SetImagen( AIconIDs[ epsCancelado ] );
                             SetStatus( Format( 'Termin� Con %d Advertencia%s', [ TotalAdvertencias, SetPlural( 's', TotalAdvertencias ) ] ), clGreen );
                        end
                        else
                        begin
                             SetImagen( AIconIDs[ epsOK ] );
                             if ( TotalEventos > 0 ) then
                                SetStatus( Format( 'Termin� Con %d Mensaje%s', [ TotalEventos, SetPlural( 's', TotalEventos ) ] ), clBlue )
                             else
                                 SetStatus( 'Termin� Con Exito', clBlack );
                        end;
               end;
               epsCancelado:
               begin
                    Caption := '� Proceso Cancelado !';
                    SetStatus( 'Fu� Cancelado', clRed );
                    SetBotonDefault( False );
                    SetImagen( AIconIDs[ epsCancelado ] );
               end;
               epsError:
               begin
                    Caption := '� Proceso Con Errores !';
                    SetStatus( 'Termin� Con Errores', clRed );
                    SetBotonDefault( False );
                    SetImagen( AIconIDs[ epsError ] );
               end;
          end;
          dDuracion := ZetaClientTools.GetElapsed( Fin, Inicio );
          Self.Folio.Caption := IntToStr( Folio );
          Self.Proceso.Caption := ZetaClientTools.GetProcessName( Proceso );
          Self.Inicio.Caption := FormatDateTime( K_DIA_HORA, Inicio );
          Self.Fin.Caption := FormatDateTime( K_DIA_HORA, Fin );
          Self.Duracion.Caption := ZetaClientTools.GetDuration( dDuracion );
          Self.Maximo.Caption := GetBenchMark( Maximo, Inicio, Fin );
          Self.MaximoLBL.Caption := ZetaClientTools.GetProcessElement( Proceso ) + ':';
          Self.Empleado.Caption := IntToStr( Empleado );
          if ( Maximo > 0 ) then
          begin
               Self.Avance.Caption := FormatFloat( '#0.# %', 100 * Procesados  / ZetaCommonTools.iMax( 1, Maximo ) );
               Self.Avance.Visible := True;
               Self.AvanceLBL.Visible := True;
          end
          else
          begin
               Self.Avance.Caption := '';
               Self.Avance.Visible := False;
               Self.AvanceLBL.Visible := False;
          end;
     end;
     with Mensajes do
     begin
          if ( Lines.Count = 0 ) then
          begin
               MensajesLBL.Visible := False;
               Mensajes.Visible := False;
               Self.Height := GetScaledHeight( K_ALTURA_FORMA ) - GetScaledHeight( Height );
          end
          else
          begin
               MensajesLBL.Visible := True;
               Mensajes.Visible := True;
               Self.Height := GetScaledHeight( K_ALTURA_FORMA );
          end;
     end;
     Beep;
     if not Application.Active then
        FlashWindow(Application.Handle,TRUE);
end;

procedure TWizardFeedback.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with Mensajes.Lines do
     begin
          BeginUpdate;
          Clear;
          EndUpdate;
     end;
     Action := caHide;
end;

function TWizardFeedback.ProcessOK: Boolean;
begin
     Result := ( Self.ProcessData.Status in [ epsOk ] );
end;

procedure TWizardFeedback.SetBotonDefault( const lOK: Boolean );
begin
     if lOk then
     begin
          OK.Default := True;
          Cancelar.Default := False;
          ActiveControl := OK;
     end
     else
     begin
          if FVerBitacora then
          begin
               OK.Default := False;
               Cancelar.Default := True;
               ActiveControl := Cancelar;
          end
          else
          begin
               OK.Default := True;
               ActiveControl := OK;
          end;
     end;
end;

procedure TWizardFeedback.SetMensajes( const sValue: String );
begin
     with Mensajes.Lines do
     begin
          BeginUpdate;
          Clear;
          Text := sValue;
          EndUpdate;
     end;
end;

function TWizardFeedback.ShowProcessInfo( const lMostrar: Boolean ): Integer;
begin
     Result := 0;
     if not FOcultarForma then
        with ProcessData do
        begin
             if ( Status in [ epsCatastrofico ] ) then
             begin
                  ZetaDialogo.zError( '� Error En Proceso !',
                                      'Se Encontr� Un Error En El Proceso' +
                                      CR_LF +
                                      ZetaClientTools.GetProcessName( Proceso ) +
                                      CR_LF +
                                      Error, 0 );
             end
             else
             begin
                  if lMostrar or not ProcessOK then
                  begin
                       Application.Restore;
                       ShowModal;
                       if ( ModalResult = mrYes ) then
                          Result := Folio;
                  end;
             end;
        end;
end;

end.
