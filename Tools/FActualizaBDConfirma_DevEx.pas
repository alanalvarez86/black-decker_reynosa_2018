unit FActualizaBDConfirma_DevEx;

interface

uses
  SysUtils, ZetaCommonLists, ZetaCommonClasses, Forms, StdCtrls, Buttons, Controls, ExtCtrls, Graphics, Classes,
  dxGDIPlusClasses, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxGroupBox, cxRadioGroup, Menus, cxButtons;

type

  TActualizaBDConfirma_DevEx = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Mensaje: TMemo;
    btnContinuar: TcxButton;
    PanelLeft: TPanel;
    Image1: TImage;
    PanelRight: TPanel;
    PanelTop: TPanel;
    PanelBottom: TPanel;
    Opcion0: TcxRadioButton;
    Opcion1: TcxRadioButton;
    Opcion2: TcxRadioButton;
    Opcion3: TcxRadioButton;
    PanelBotones: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    function ShowDlgActualizaBDConfirma( sEmpresa: string; iVersionActual,iVersionUltima:Integer ): eOperacionConflicto;

var
  ActualizaBDConfirma_DevEx: TActualizaBDConfirma_DevEx;

implementation

uses
    ZetaCommonTools,ZAccesosMgr,ZAccesosTress, DCliente;

{$R *.DFM}

procedure TActualizaBDConfirma_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= 0;//TODO: HelpContext nueva pantalla ;
end;

function ShowDlgActualizaBDConfirma( sEmpresa: string; iVersionActual,iVersionUltima:Integer ): eOperacionConflicto;
var
   opcion:Integer;
begin
     LoadDerechos;
     try
        if ( ActualizaBDConfirma_DevEx = nil ) then
        begin
             ActualizaBDConfirma_DevEx := TActualizaBDConfirma_DevEx.Create( Application ); // Se destruye al Salir del Programa //
        end;

        with ActualizaBDConfirma_DevEx do
        begin
             Mensaje.Lines.Clear;
             Mensaje.Lines.Add(Format('La Base de Datos asociada a la empresa que desea ingresar no cuenta con la estructura requerida para la versi�n de Sistema TRESS instalada.'+CR_LF+CR_LF+
                                       'Se requiere actualizar la estructura de la Base de Datos de %d a %d.'+CR_LF+CR_LF+
                                       'Seleccione una opci�n para continuar:' ,[ iVersionActual,iVersionUltima]));
             if ShowModal = mrOk then
             begin
                   {***DevEx(@am): Se cambio el radioGroupBox por botones independientes para poder deshabilitar las opciones, segun el escenario en el que se musetra esta pantalla***}
                  //Guardar la opcion seleccionada
                  if Opcion0.Checked then
                     opcion := Opcion0.Tag
                  else if Opcion1.Checked then
                       opcion := Opcion1.Tag
                  else if Opcion2.Checked then
                       opcion := Opcion2.Tag
                  else
                      opcion := Opcion3.Tag;

                  case opcion of
                       0:  Result:= ocSustituir; //Actualiza Empresa
                       1:  Result:= ocSumar;     //Continuar Sin Actualizar Empresa
                       2:  Result:= ocReportar;  //Seleccionar Otra Empresa
                       else Result:= ocIgnorar;       //Configurar Sistema
                  end;
             end
             else
                  Result := ocReportar; //Si da en close regresa a la pantalla de seleccionar otra empresa
        end;
     finally
           FreeAndNil(ActualizaBDConfirma_DevEx);
     end;
end;

procedure TActualizaBDConfirma_DevEx.FormShow(Sender: TObject);
begin
     {***DevEx(@am): Se cambio el radioGroupBox por botones independientes para poder deshabilitar las opciones, segun el escenario en el que se musetra esta pantalla***}
     //Definir los botones que se mostraran segun la configuracion del usuario y la BD seleccionada
     Opcion0.Enabled :=  Revisa(D_SIST_ACTUALIZAR_BDS );
     Opcion3.Enabled := not(dmCliente.EmpresaAbierta) and ( dmcliente.GetDatosUsuarioActivo.Grupo = D_GRUPO_SIN_RESTRICCION );
     //Definir la opcion seleccionada por defecto, basados en las opciones habilitadas.
     if Opcion0.Enabled then
        Opcion0.Checked := True
     else
         Opcion1.Checked := True;

     //Enfocar el boton de continuar
     btnContinuar.SetFocus;
end;

end.
