unit ZBaseEscogeGrid_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, Grids, DBGrids,
     StdCtrls, Buttons, ExtCtrls,
     ZetaDBGrid,
     ZetaMessages, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore,  
  TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, Menus, cxButtons,zGridModeTools;

type
  TBaseEscogeGrid_DevEx = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DataSource: TDataSource;
    Grid_DevEx: TZetaCXGrid;
    Grid_DevExDBTableView: TcxGridDBTableView;
    Grid_DevExLevel: TcxGridLevel;
    OK_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure Grid_DevExDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure Grid_DevExDBTableViewDblClick(Sender: TObject);
    procedure Grid_DevExDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FFormula: String;
    //procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    { Protected declarations }
    function GetFormula: String; virtual;
    procedure Connect; virtual;
  public
    { Public declarations }
    property Formula: String read GetFormula write FFormula;
  end;

var
  BaseEscogeGrid_DevEx: TBaseEscogeGrid_DevEx;

implementation

{$R *.DFM}

procedure TBaseEscogeGrid_DevEx.FormCreate(Sender: TObject);
begin
     Grid_DevExDBTableView.DataController.DataModeController.GridMode:= True;
     Grid_DevExDBTableView.DataController.Filter.AutoDataSetFilter := True;
       //Desactiva la posibilidad de agrupar
     Grid_DevExDBTableView.OptionsCustomize.ColumnGrouping := False;
     //Esconde la caja de agrupamiento
     Grid_DevExDBTableView.OptionsView.GroupByBox := False;
     //Para que nunca muestre el filterbox inferior
     Grid_DevExDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     Grid_DevExDBTableView.FilterBox.CustomizeDialog := False;
end;

procedure TBaseEscogeGrid_DevEx.FormShow(Sender: TObject);
begin
     FFormula:= '';
     Connect;
     //DevEx: Despues de conectar los datos se acomodan los anchos de las columnas
     Grid_DevExDBTableView.ApplyBestFit();
end;

procedure TBaseEscogeGrid_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Grid_DevExDBTableView.DataController.Filter.Root.Clear;
end;

procedure TBaseEscogeGrid_DevEx.Connect;
begin
end;

function TBaseEscogeGrid_DevEx.GetFormula: String;
begin
end;

{procedure TBaseEscogeGrid_DevEx.WMExaminar(var Message: TMessage);
begin
     Close;
     ModalResult := mrOk;
end;}

procedure TBaseEscogeGrid_DevEx.Grid_DevExDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer;
          AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if Grid_DevExDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( Grid_DevExDBTableView, AItemIndex, AValueList );
end;

procedure TBaseEscogeGrid_DevEx.Grid_DevExDBTableViewDblClick(Sender: TObject);
begin
     Close;
     ModalResult := mrOk;
end;

procedure TBaseEscogeGrid_DevEx.Grid_DevExDBTableViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     if Key = VK_RETURN then
     begin
          Close;
          ModalResult := mrOk;
     end;
end;

end.
