unit ZBaseAsisEdicion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinsDefaultPainters;

type
  TBaseAsisEdicion_DevEx = class(TBaseEdicion_DevEx)
  private
    { Private declarations }
    //function GetFechaTarjeta: TDate; virtual;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  BaseAsisEdicion_DevEx: TBaseAsisEdicion_DevEx;

implementation
uses
    DCliente,
    ZetaCommonClasses;

{$R *.DFM}

{ TBaseAsisEdicion }

{function TBaseAsisEdicion.GetFechaTarjeta: TDate;
begin
     if ( DataSource.DataSet <> NIL ) then
        Result := DataSource.DataSet.FieldByName('AU_FECHA').AsDateTime
     else
         Result := NullDateTime;

end;}

function TBaseAsisEdicion_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
end;

function TBaseAsisEdicion_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.Dataset, sMensaje );
     end;
end;

function TBaseAsisEdicion_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmCliente.PuedeCambiarTarjeta( DataSource.Dataset, sMensaje );
     end;
end;

end.
