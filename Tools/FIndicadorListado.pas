unit FIndicadorListado;

interface

//{$R *.RES}

uses
   Controls,
   Classes,
   ExtCtrls,
   Graphics,
   Windows,
   StdCtrls,
   SysUtils,
   StrUtils,
   ZetaCXGrid,
   Data.DB,
   cxControls,
   cxGridLevel,
   cxGridDBTableView,
   cxGridCustomTableView,
   cxFilter,
   cxCustomData,
   cxGridTableView,
   cxGraphics,
   ZetaClientDataSet,
   ZGridModeTools;

type
  TIndicadoresListado = class(TPanel)//(TCustomPanel)
  private
    { private declarations here }
    FLabelTitulo: TLabel;//Titulo Indicador
    FPanelGridContenedor: TPanel;//Titulo Indicador
    FGrid : TZetaCXGrid;
    FView: TcxGridDBTableView;
    FLevel: TcxGridLevel;
    FDataSource: TDataSource;
    //Textos
    FTitulo: String;//Titulo Indicador
    ZetaDBGrid: TZetaCXGrid;
    AColumn: TcxGridDBColumn;

    procedure SetLabelTitulo(const Value: TLabel);
    procedure SetTitulo(Value: String);
    function GetLevel: TcxGridLevel;
    function GetView: TcxGridDBTableView;
    procedure SetLevel(const Value: TcxGridLevel);
    procedure SetView(const Value: TcxGridDBTableView);
    procedure AplicarMascaras( oDataSet: TZetaClientDataSet );
    function GetGrid: TZetaCXGrid;
    procedure SetGrid(const Value: TZetaCXGrid);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer; AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView; AColumn: TcxGridColumn);
    procedure OnDrawColumnHeaderAcentos( Sender: TcxGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean );
  protected
    { protected declarations here }

  public
    { public declarations here }
    constructor Create( AOwner : TComponent); overload; override;
    constructor Create(const ControlName: string; AOwner : TComponent; Parent: TWinControl ); reintroduce; overload;
    procedure MostrarDatos;
    procedure ApplyMinWidth;
    procedure SetIniPropiedadesGrid;
    procedure SetBorde;
    procedure LimpiarIndicador;
    procedure MensajeEspera;
    procedure ValoresDefaulPanel;
    property LabelTitulo: TLabel read FLabelTitulo write SetLabelTitulo;//Titulo Indicador
    property PanelGridContenedor: TPanel read FPanelGridContenedor write FPanelGridContenedor;//Contenedor Grid
    //Textos
    property Titulo: String read FTitulo write SetTitulo;//Titulo Indicador
    property View: TcxGridDBTableView read GetView write SetView;
    property Level: TcxGridLevel read GetLevel write SetLevel;
    property DataSource: TDataSource read FDataSource write FDataSource;
    property Grid: TZetaCXGrid read GetGrid write SetGrid;

  published
    { published declarations here }

end;

const
     K_MARGIN = 5;
     K_MENSJAE_THREAD = 'REFRESCANDO...';

var
   IndicadoresListado: TIndicadoresListado;

implementation

uses
    ZetaDBGrid, ZetaCommonClasses;

constructor TIndicadoresListado.Create(const ControlName: string; AOwner: TComponent; Parent: TWinControl);
begin
     inherited Create( AOwner );

     Self.Parent := Parent;
     Self.Name := ControlName;
     Self.Width := 150;
     Self.Height := 87;

     //Background
     Self.ParentBackground:= False;
     Self.ParentColor := False;
     Self.Color := clWhite;

     //Bordes
     Self.BevelEdges := [];
     Self.BevelInner := bvNone;
     Self.BevelKind := bkNone;
     Self.BevelOuter := bvNone;
     //Fin

     //Padding
     Padding.Left := K_MARGIN;
     Padding.Right := K_MARGIN;
     Padding.Top := K_MARGIN;
     Padding.Bottom := K_MARGIN;

     //Valor Centrado
     Caption := '';


     //Titulo
     FLabelTitulo := TLabel.Create(Self);
     with FLabelTitulo do
     begin
          Parent := Self;
          Layout := tlTop;
          Align := alTop;
          Width := 96;
          Height := 15;
          Constraints.MinHeight := 15;
          Caption := '';
          Font.Charset := DEFAULT_CHARSET;
          Font.Size := 10;
          Font.Name := 'Segoe UI';
          Font.Style := [fsBold];
          Font.Color := $003E3E3E;
          ParentFont := False;
     end;

     //Panel Contenedor Grid
     FPanelGridContenedor := TPanel.Create(Self);
     with FPanelGridContenedor do
     begin
          ParentBackground:= False;
          ParentColor := False;
          Color := clWhite;
          Parent := Self;
          Align := alClient;
          Padding.Left := K_MARGIN;
          Padding.Right := K_MARGIN;
          Padding.Top := K_MARGIN;
          Padding.Bottom := K_MARGIN;
          BevelEdges := [];
          BevelInner := bvNone;
          BevelKind := bkNone;
          BevelOuter := bvNone;
     end;


     FDataSource := TDataSource.Create(Self);
     FDataSource.Name := Format(ControlName + '_%s', ['DATASURCE'] );
     FGrid := TZetaCxGrid.Create(Self);
     FGrid.Name := Format(ControlName + '_%s', ['GRID'] );
     FGrid.BorderStyle := cxcbsNone;
     FGrid.BorderWidth := 0;
     FGrid.Height := FGrid.Height;
     FGrid.Visible := true;
     FGrid.Parent := FPanelGridContenedor;
     FGrid.Align := alClient;
     FLevel := FGrid.Levels.Add;
     FLevel.Name := Format(ControlName + '_%s', ['LEVEL'] );
     FView := FGrid.CreateView(TcxGridDBTableView) as TcxGridDBTableView;
     FLevel.GridView := View;
     SetIniPropiedadesGrid;
     FGrid.ParentFont := False;
     //Base Propiedades
     FLevel.GridView.DataController.OnSortingChanged := ZetaDBGridDBTableViewDataControllerSortingChanged;
     FLevel.GridView.DataController.Filter.OnGetValueList := ZetaDBGridDBTableViewDataControllerFilterGetValueList;
     FLevel.GridView.DataController.Summary.OnAfterSummary := ZetaDBGridDBTableViewDataControllerSummaryAfterSummary;
     FView.OnColumnHeaderClick := ZetaDBGridDBTableViewColumnHeaderClick;
     //Acentos Columnas
     FView.OnCustomDrawColumnHeader := OnDrawColumnHeaderAcentos;
end;

function TIndicadoresListado.GetGrid: TZetaCXGrid;
begin
     Result := FGrid;
end;

function TIndicadoresListado.GetLevel: TcxGridLevel;
begin
     Result := FLevel;
end;

function TIndicadoresListado.GetView: TcxGridDBTableView;
begin
      Result := FView;
end;

constructor TIndicadoresListado.Create(AOwner: TComponent);
begin
     inherited Create( AOwner );
end;

procedure TIndicadoresListado.SetTitulo(Value: String);
begin
     FTitulo := Value;
     FLabelTitulo.Caption := Value;
end;

procedure TIndicadoresListado.AplicarMascaras( oDataSet: TZetaClientDataSet );
var
   i: Integer;
begin
     if oDataSet <> nil then
     begin
          with oDataSet do
          begin
               for i:=0 to Fields.Count - 1 do
               begin
                    if ( FindField( Fields[ i ].FieldName ) is TFloatField ) then
                       MaskPesos( Fields[ i ].FieldName );
                    if ( ( FindField( Fields[ i ].FieldName ) is TDateTimeField ) or ( FindField( Fields[ i ].FieldName ) is TDateField ) ) then
                       MaskFecha( Fields[ i ].FieldName );
                    if ( FindField( Fields[ i ].FieldName ) is TTimeField ) then
                       MaskTime( Fields[ i ].FieldName );
                    if ( FindField( Fields[ i ].FieldName ) is TBooleanField ) then
                       MaskBool( Fields[ i ].FieldName );
               end;
          end;
     end;
end;

procedure TIndicadoresListado.MostrarDatos;
begin
     AplicarMascaras( TZetaClientDataSet( Self.FDataSource.DataSet ) );
     Self.FView.BeginUpdate;
     Self.View.ClearItems;
     Self.FView.DataController.DataSource := Self.FDataSource;
     Self.View.DataController.CreateAllItems;
     Self.View.EndUpdate;
     ValoresDefaulPanel;
end;

procedure TIndicadoresListado.SetBorde;
begin
     //Bordes
     Self.BevelEdges := [beLeft,beTop,beRight,beBottom];
     Self.BevelInner := bvNone;
     Self.BevelKind := bkTile;
     Self.BevelOuter := bvNone;
end;

procedure TIndicadoresListado.SetGrid(const Value: TZetaCXGrid);
begin
     Self.FGrid := Value;
end;

procedure TIndicadoresListado.SetView(const Value: TcxGridDBTableView);
begin
     Self.View := Value;
end;

procedure TIndicadoresListado.SetLabelTitulo(const Value: TLabel);
begin
     Self.FLabelTitulo := Value;
end;

procedure TIndicadoresListado.SetLevel(const Value: TcxGridLevel);
begin
     Self.FLevel := Value;
end;

procedure TIndicadoresListado.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions = 40;
begin
     with  Self.FView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
          SetIniPropiedadesGrid;
     end;
end;

procedure TIndicadoresListado.LimpiarIndicador;
begin
     Self.FDataSource.DataSet := nil;
     Self.MostrarDatos;
end;

procedure TIndicadoresListado.SetIniPropiedadesGrid;
begin
     with  Self.FView do
     begin
          OptionsCustomize.ColumnGrouping := False;
          OptionsView.GroupByBox := False;
          FilterBox.Visible := fvNever;
          FilterBox.CustomizeDialog := False;
          OptionsData.Appending := FALSE;
          OptionsData.Editing := FALSE;
          OptionsData.CancelOnExit := FALSE;
          OptionsData.Deleting := FALSE;
          OptionsData.Inserting := FALSE;
          OptionsData.DeletingConfirmation := FALSE;

          DataController.DataModeController.GridMode:= True;
          DataController.Filter.AutoDataSetFilter := TRUE;

          ApplyBestFit();
     end;
end;

{***(am):Trabaja CON GridMode***}
procedure TIndicadoresListado.ZetaDBGridDBTableViewDataControllerSortingChanged(Sender: TObject);
begin
     if Self.FView.DataController.IsGridMode then
     begin
          inherited;
          ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
     end;
end;

{***(am):Trabaja SIN GridMode***}
procedure TIndicadoresListado.ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria;
          AItemIndex: Integer; AValueList: TcxDataFilterValueList);
var
   oField: TField;
   sFieldName: String;
   cdsClone: TZetaClientDataSet;
   oListaLookup: TStringList;

   procedure StoreLookupValues;
   var
      ValorAnterior, ValorActual: String;
   begin
        sFieldName := oField.KeyFields;
        oListaLookup := TStringList.Create;
        cdsClone := TZetaClientDataSet.Create(self);
        try
           with cdsClone do
           begin
                CloneCursor( TZetaClientDataSet( Self.FView.DataController.DataSet ), TRUE, TRUE );
                IndexFieldNames := sFieldName;
                ValorAnterior := ZetaCommonClasses.VACIO;
                First;
                while ( not EOF ) do
                begin
                     ValorActual := FieldByName( sFieldName ).AsString;
                     if ( ValorActual <> ValorAnterior ) then
                     begin
                          oListaLookup.Add( Format( '%s=%s', [ TZetaLookupDataSet( oField.LookupDataSet ).GetDescripcion( ValorActual ), ValorActual ] ) );
                          ValorAnterior := ValorActual;
                     end;
                     Next;
                end;
                Close;
                oListaLookup.Sort;
           end;
        finally
               Self.FView.Columns[ AItemIndex ].Tag := Integer( oListaLookup );
               cdsClone.Free;
        end;
   end;

   procedure LlenaLookupValues;
   var
      i: Integer;
   begin
        if ( Self.FView.Columns[ AItemIndex ].Tag = 0 ) then
           StoreLookupValues;
        if ( Self.FView.Columns[ AItemIndex ].Tag > 0 ) then
        begin
             try
                oListaLookup := TStringList( Self.FView.Columns[ AItemIndex ].Tag );
                for i := 0 to oListaLookup.Count - 1 do
                begin
                     AValueList.Add( fviValue, oListaLookup.ValueFromIndex[i], oListaLookup.Names[i], TRUE );
                end;
             except
                   //Abort;   // excepci�n silenciosa - No continua con el llenado
             end;
        end;
   end;

begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if Self.FView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( Self.FView, AItemIndex, AValueList );
end;

procedure TIndicadoresListado.ZetaDBGridDBTableViewDataControllerSummaryAfterSummary(ASender: TcxDataSummary);
begin
     inherited;
     if Self.View.DataController.IsGridMode and Self.View.OptionsView.Footer then      // Hay columnas totalizadas
        ZGridModeTools.AsignaValorColumnaSummary( ASender );
end;

{***(am):Trabaja CON GridMode***}
procedure TIndicadoresListado.ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
     if Self.View.DataController.IsGridMode then
     begin
          inherited;
          self.AColumn := TcxGridDBColumn(AColumn);
     end;
end;

procedure TIndicadoresListado.OnDrawColumnHeaderAcentos(Sender: TcxGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridColumnHeaderViewInfo; var ADone: Boolean);
const
     K_GUION_BAJO_SIMPLE = '_';
     K_GUION_BAJO_DOBLE = '__';
     K_a_ACENTO = '�';
     K_e_ACENTO = '�';
     K_i_ACENTO = '�';
     K_o_ACENTO = '�';
     K_u_ACENTO = '�';
     K_A_MAY_ACENTO = '�';
     K_E_MAY_ACENTO = '�';
     K_I_MAY_ACENTO = '�';
     K_O_MAY_ACENTO = '�';
     K_U_MAY_ACENTO = '�';
     K_a_DIERESIS = '�';
     K_e_DIERESIS = '�';
     K_i_DIERESIS = '�';
     K_o_DIERESIS = '�';
     K_u_DIERESIS = '�';
     K_A_MAY_DIERESIS = '�';
     K_E_MAY_DIERESIS = '�';
     K_I_MAY_DIERESIS = '�';
     K_O_MAY_DIERESIS = '�';
     K_U_MAY_DIERESIS = '�';
var
   sTexto: String;

   function Acentos( const sTexto: String ): String;
   begin
        Result := sTexto;
        if sTexto = 'a' then
           Result := K_a_ACENTO
        else if sTexto = 'e' then
           Result := K_e_ACENTO
        else if sTexto = 'i' then
           Result := K_i_ACENTO
        else if sTexto = 'o' then
           Result := K_o_ACENTO
        else if sTexto = 'u' then
           Result := K_u_ACENTO
        else if sTexto = 'A' then
           Result := K_A_MAY_ACENTO
        else if sTexto = 'E' then
           Result := K_E_MAY_ACENTO
        else if sTexto = 'I' then
           Result := K_I_MAY_ACENTO
        else if sTexto = 'O' then
           Result := K_O_MAY_ACENTO
        else if sTexto = 'U' then
           Result := K_U_MAY_ACENTO;
   end;

   function Dieresis( const sTexto: String ): String;
   begin
        Result := sTexto;
        if sTexto = 'a' then
           Result := K_a_DIERESIS
        else if sTexto = 'e' then
           Result := K_e_DIERESIS
        else if sTexto = 'i' then
           Result := K_i_DIERESIS
        else if sTexto = 'o' then
           Result := K_o_DIERESIS
        else if sTexto = 'u' then
           Result := K_u_DIERESIS
        else if sTexto = 'A' then
           Result := K_A_MAY_DIERESIS
        else if sTexto = 'E' then
           Result := K_E_MAY_DIERESIS
        else if sTexto = 'I' then
           Result := K_I_MAY_DIERESIS
        else if sTexto = 'O' then
           Result := K_O_MAY_DIERESIS
        else if sTexto = 'U' then
           Result := K_U_MAY_DIERESIS;
   end;

   function ConvertirCaraterAcentos( const sTexto: String ): String;
   var
      iPos: Integer;
      sTextoAux: String;
      sCaracter: String;
   begin
        Result := sTexto;
        iPos := AnsiPos( K_GUION_BAJO_SIMPLE, sTexto );
        if iPos > 0 then
        begin
             sTextoAux := Copy( sTexto, iPos + 1, 1 );
             sCaracter := Acentos( sTextoAux );
             if sTextoAux <> sCaracter then
                Result := StringReplace( Result, K_GUION_BAJO_SIMPLE + sTextoAux, sCaracter, [rfReplaceAll] );
        end;
   end;

   function ConvertirCaraterDieresis( const sTexto: String ): String;
      var
      iPos: Integer;
      sTextoAux: String;
      sCaracter: String;
   begin
        Result := sTexto;
        iPos := AnsiPos( K_GUION_BAJO_DOBLE, sTexto );
        if iPos > 0 then
        begin
             sTextoAux := Copy( sTexto, iPos + 2, 1 );
             sCaracter := Dieresis( sTextoAux );
             if sTextoAux <> sCaracter then
                Result := StringReplace( Result, K_GUION_BAJO_DOBLE + sTextoAux, sCaracter, [rfReplaceAll] );
        end;
   end;

   function EsAcento( sTexto: String ): Boolean;
   var
      iPos: Integer;
      sTextoAux: String;
   begin
        Result := False;
        iPos := AnsiPos( K_GUION_BAJO_SIMPLE, sTexto );
        if iPos > 0 then
        begin
             sTextoAux := Copy( sTexto, iPos + 1 , 1 );
             if sTextoAux <> K_GUION_BAJO_SIMPLE then
                Result := True;
        end;
   end;

   function EsDieresis( sTexto: String ): Boolean;
   var
      iPos: Integer;
      sTextoAux: String;
   begin
        Result := False;
        iPos := AnsiPos( K_GUION_BAJO_SIMPLE, sTexto );
        if iPos > 0 then
        begin
             sTextoAux := Copy( sTexto, iPos + 1 , 1 );
             if sTextoAux = K_GUION_BAJO_SIMPLE then
                Result := True;
        end;
   end;

   function ConvertirTexto( const sTexto:String ): String;
   begin
        Result := sTexto;
        if EsAcento( sTexto ) then
           Result := ConvertirCaraterAcentos( sTexto )
        else if EsDieresis( sTexto ) then
           Result := ConvertirCaraterDieresis( sTexto );
   end;
begin
     sTexto := AViewInfo.Text;
     if sTexto <> VACIO then
        AViewInfo.Text := ConvertirTexto(sTexto);
end;

procedure TIndicadoresListado.MensajeEspera;
begin
     Self.FGrid.Hide;
     with Self.FPanelGridContenedor do
     begin
          Color := $00F4F4F4;
          Font.Name := 'Segoe UI';
          Font.Size := 10;
          Caption := K_MENSJAE_THREAD;
          Font.Color := $00A6A6A6;
          Font.Style := [fsBold];
     end;
end;

procedure TIndicadoresListado.ValoresDefaulPanel;
begin
     Self.FPanelGridContenedor.Color := clWhite;
     Self.FPanelGridContenedor.Caption := '';
     Self.FGrid.Visible := True;
end;


end.
