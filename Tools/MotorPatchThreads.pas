{* Unidad para definir los hilos que se utilzan por MotorPatch. @author Ricardo Carrillo Morales 2014-04-09}
unit MotorPatchThreads;

interface

uses
  Windows, SysUtils, Classes, DBClient, ZetaCommonClasses{$IFDEF TRESSCFG}, DZetaServerProvider{$ENDIF};

type
  {* Hilo base para las operaciones de MotorPatch. @author Ricardo Carrillo 2014-04-09}
  TThreadPatchBase = class(TThread)
  private
    { Private declarations }
    fCDS   : TClientDataSet;
    fParams: TZetaParams;
  protected
  public
    constructor Create(Params: TZetaParams; AlTerminar: TNotifyEvent; Lista: TList);
    destructor Destroy; override;
    property CDS   : TClientDataSet read fCDS;
    property Params: TZetaParams    read fParams;
  end;

  {* Hilo para actualizar avance en las operaciones de MotorPatch. @author Ricardo Carrillo 2014-04-09}
  TThreadPatchProgreso = class(TThreadPatchBase)
  private
    { Private declarations }
    fMutex     : THandle;
    fDS        : TClientDataSet;
    fSincroniza: TNotifyEvent;
    procedure Actualiza;
  protected
    procedure Execute; override;
  public
    constructor Create(Mutex: THandle; DS: TClientDataSet; Params: TZetaParams; AlActualizar, AlTerminar: TNotifyEvent; Lista: TList);
    property DataSet: TClientDataSet read fDS;
  end;

  {$IFDEF TRESSCFG}
  {* Hilo para correr un proceso de las operaciones de MotorPatch. @author Ricardo Carrillo 2014-04-09}
  TThreadPatchProceso = class(TThreadPatchBase)
  private
    { Private declarations }
    fZetaProvider: TdmZetaServerProvider;
  protected
    procedure Execute; override;
  public
    constructor Create(Params: TZetaParams; ZetaProvider: TdmZetaServerProvider; AlTerminar: TNotifyEvent; Lista: TList);
    property ZetaProvider: TdmZetaServerProvider read fZetaProvider;
  end;
  {$ENDIF}

implementation

uses
  ActiveX{$IFDEF TRESSCFG}, DMotorPatch{$ENDIF};

{ TThreadPatchBase }

constructor TThreadPatchBase.Create(Params: TZetaParams; AlTerminar: TNotifyEvent; Lista: TList);
begin
  fParams         := Params;
  FreeOnTerminate := True;
  OnTerminate     := AlTerminar;
  fCDS            := TClientDataSet.Create(nil);
  Lista.Add(Self);
  inherited Create(False);
end;

destructor TThreadPatchBase.Destroy;
begin
  FreeAndNil(fCDS);
  inherited;
end;

{ TThreadPatchProgreso }

constructor TThreadPatchProgreso.Create(Mutex: THandle; DS: TClientDataSet; Params: TZetaParams; AlActualizar, AlTerminar: TNotifyEvent;
  Lista: TList);
begin
  fMutex      := Mutex;
  fDS         := DS;
  fSincroniza := AlActualizar;
  inherited Create(Params, AlTerminar, Lista);
end;

procedure TThreadPatchProgreso.Actualiza;
begin
  fSincroniza(Self);
end;

procedure TThreadPatchProgreso.Execute;
begin
  CoInitialize(nil);
  while (not Terminated) and Assigned(fSincroniza) do begin
    if fMutex <> 0 then
      WaitForSingleObject(fMutex, INFINITE);
    try
      Synchronize(Actualiza);
      if (not CDS.IsEmpty) and (CDS.FieldByName('PC_ERROR').AsString <> B_PROCESO_ABIERTO) then
        Break; // Salir del m�todo con escala en el finally
    finally
      if fMutex <> 0 then
        ReleaseMutex(fMutex);
    end;
    Sleep(500);  // Actualizar cada X milisegundos
  end;
end;

{$IFDEF TRESSCFG}
{ TThreadPatchProceso }

constructor TThreadPatchProceso.Create(Params: TZetaParams; ZetaProvider: TdmZetaServerProvider; AlTerminar: TNotifyEvent;
  Lista: TList);
begin
  fZetaProvider := ZetaProvider;
  inherited Create(Params, AlTerminar, Lista);
end;

procedure TThreadPatchProceso.Execute;
begin
  CoInitialize(nil);
  try
    MotorPatch(fParams, ZetaProvider);
  finally

  end;
end;
{$ENDIF}

end.
