unit ZVisitantesTools;

interface

uses
    SysUtils, ComCtrls, Classes, Graphics {$ifndef TRESSEMAIL},cxRichEdit, EditorDResumen{$endif};

type
    eTipoCatalogo = ( eTipoId,      {0}
                      eTipoCarro,   {1}
                      eTipoAsunto,  {2}
                      eTipoVisita,  {3}
                      eDepto,       {4}
                      eCaseta,      {5}
                      eAnfitrion,   {6}
                      eCita,        {7}
                      eCompVisita,  {8}
                      eCorte,       {9}
                      eVisita,      {10}
                      eLibro,       {11}
                      eCondiciones, {12}
                      eVisitaMgr   {13}
                      );

    eTabSheetRegEnt = ( eVisitante,
                      eCompany,
                      eVisitaA,
                      eAsunto );

{$ifndef TRESSEMAIL}
type
    TEditorCliente = class
  private

    FEditor:TEditorDResumen;
    FTexto : string;
  public
    constructor Create;
    Destructor Destroy; override;
    procedure RenglonVacio;
    procedure Normal(const sCampo: String);
    procedure Titulo(const sTitulo: String);
    procedure Titulo1(const sTitulo: String);
    procedure Pos_Inicio ( oRichText : TRichedit );
    procedure Inicializa( oRichText : TcxRichEdit);
end;
{$endif}

const
     K_TIPO_ID     = Ord(eTipoId);
     K_TIPO_CARRO  = Ord(eTipoCarro);
     K_TIPO_ASUNTO = Ord(eTipoAsunto);
     K_TIPO_VISITA = Ord(eTipoVisita);
     K_DEPTO       = Ord(eDepto);
     K_CASETA      = Ord(eCaseta);
     K_ANFITRION   = Ord(eAnfitrion);
     K_EMP_VISI    = Ord(eCompVisita);
     K_CORTE       = Ord(eCorte);
     K_VISITANTE   = Ord(eVisita);
     K_LIBRO       = Ord(eLibro);
     K_CONDICIONES = Ord(eCondiciones);

 {$ifdef INTERBASE}
    PRETTY_ANFITRIO = 'AN_NOMBRES||'' ''||AN_APE_PAT||'' ''||AN_APE_MAT';
    PRETTY_VISIT = 'VI_NOMBRES||'' ''||VI_APE_PAT||'' ''||VI_APE_MAT';
 {$endif}
 {$ifdef MSSQL}
     PRETTY_ANFITRIO = 'AN_NOMBRES+'' ''+AN_APE_PAT+'' ''+AN_APE_MAT';
     PRETTY_VISIT = 'VI_NOMBRES+'' ''+VI_APE_PAT+'' ''+VI_APE_MAT';
 {$endif}

 P_ANFITRION_NAME = 'Pretty_Anfi';
 P_VISITA_NAME = 'PRETTY_VISIT'; //acl
 
implementation
uses
    ZetaCommonClasses;


{TEditorCliente}
const
     K_SIZE_LETRA_TITULO = 16;

{$ifndef TRESSEMAIL}
constructor TEditorCliente.Create;
begin
     FEditor := TEditorDResumen.Create;
end;

destructor TEditorCliente.Destroy;
begin
     FreeAndNil(FEditor);
end;

{procedure TEditorCliente.Inicializa(oRichText : TRichEdit);
begin
     with FEditor do
     begin
          ERichText := oRichText;
          Reset;
     end;
end;
 }
procedure TEditorCliente.Inicializa(oRichText : TcxRichEdit);
begin
     with FEditor do
     begin
          ERichText := oRichText;
          Reset;
     end;
end;

procedure TEditorCliente.Titulo(Const sTitulo :String );
begin
     With FEditor do
     begin
          AlineaLetra := taCenter;
          ColorLetra:=clBlack;
          EstiloLetra := [fsBold];
          TipoLetra := 'Book Antiqua';
          Tamano := K_SIZE_LETRA_TITULO;
          AgregarLinea(sTitulo);
     end;
end;

procedure TEditorCliente.Titulo1(Const sTitulo :String );
begin
     With FEditor do
     begin
          AlineaLetra := taCenter;
          ColorLetra:=clBlack;
          EstiloLetra := [];
          TipoLetra := 'Book Antiqua';
          Tamano := K_SIZE_LETRA_TITULO - 1;
          AgregarLinea(sTitulo);
     end;
end;

procedure TEditorCliente.RenglonVacio;
begin
     Normal( VACIO );
end;

procedure TEditorCliente.Normal(Const sCampo: String);
begin
     With FEditor do
     begin
          Ident := K_IDENT_CAMPOS_EXP;
          EstiloLetra := [];
          ColorLetra:=clBlack;
          Tamano := K_SIZE_LETRA;
          AgregarLinea(FTexto + ' ' + sCampo);
          FTexto := VACIO;
    end;
end;


procedure TEditorCliente.Pos_Inicio(oRichText: TRichedit);
begin
     { Se utiliza un insert y un delete para posicionar al RTF
       al principio despues de escribir los campos             }
     With oRichText.Lines do
     begin
          Insert( 0, VACIO );
          Delete( 0 );
     end;
end;
{$endif}



end.
