unit FEditTAhorroPE_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, StdCtrls, DBCtrls, ZetaKeyLookup, Mask, Db,
  ExtCtrls, Buttons, ZetaKeyCombo, ZetaEdit,
  {$ifdef CAJAAHORRO}
    dCajaAhorro,
  {$else}
    DTablas,
  {$endif}
  ZetaNumero, ZetaSmartLists, ZetaFecha,ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, cxControls, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxPCdxBarPopupMenu, ZetaKeyLookup_DevEx, cxPC, cxContainer, cxEdit,
  cxGroupBox, cxRadioGroup, dxSkinsCore,
  dxSkinsdxBarPainter, dxSkinscxPCPainter, dxBarBuiltInMenu;


type
  TEditTAhorroPE_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    CxPageControl: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    tsConfidencialidad: TcxTabSheet;
    TabGenerales: TcxTabSheet;
    Label16: TLabel;
    Label17: TLabel;
    TB_PAGO: TZetaDBKeyLookup_DevEx;
    lblPago: TLabel;
    Label7: TLabel;
    lblinteres: TLabel;
    TB_INTERES: TZetaDBKeyLookup_DevEx;
    TB_PRESTA: TZetaDBKeyLookup_DevEx;
    cxTabSheet1: TcxTabSheet;
    gbReportes: TcxGroupBox;
    FormatoLBL: TLabel;
    lblPrestamo: TLabel;
    lblRetiro: TLabel;
    TB_REP_INS: TZetaDBKeyLookup_DevEx;
    TB_REP_PRE: TZetaDBKeyLookup_DevEx;
    TB_REP_LIQ: TZetaDBKeyLookup_DevEx;
    gbValidarRango: TcxGroupBox;
    lblInicio: TLabel;
    lblFin: TLabel;
    cbDiaIni: TComboBox;
    cbMesIni: TComboBox;
    cbDiaFin: TComboBox;
    cbMesFin: TComboBox;
    GBTasas: TcxGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    TB_TASA1: TZetaDBNumero;
    TB_TASA2: TZetaDBNumero;
    TB_TASA3: TZetaDBNumero;
    Label6: TLabel;
    Label5: TLabel;
    TB_LIQUIDA: TZetaDBKeyCombo;
    TB_ALTA: TZetaDBKeyCombo;
    gbConfidencialidad: TcxGroupBox;
    btSeleccionarConfiden: TcxButton;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TcxRadioButton;
    rbConfidenAlgunas: TcxRadioButton;
    TB_RELATIV: TZetaDBKeyLookup_DevEx;
    TB_CONCEPT: TZetaDBKeyLookup_DevEx;
    TB_VAL_RAN: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TB_VAL_RANClick(Sender: TObject);
    procedure AlCambiarCombo(Sender: TObject);
    // procedure DataSourceDataChange(Sender: TObject; Field: TField); 
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private                       
    lValoresConfidencialidad : TStringList;
    procedure SetControlesFecha;   
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;

  protected
    procedure Connect;override;
    procedure DoLookup; override;
    procedure EscribirCambios;override;
    {$ifdef CAJAAHORRO}
    function dmTablas : TdmCajaAhorro;
    {$endif}
  public
  end;

var
  EditTAhorroPE_DevEx: TEditTAhorroPE_DevEx;

implementation

uses
    dCatalogos, dReportes, ZetaCommonClasses,ZetaCommonTools, ZAccesosTress,ZetaDialogo, ZetaBuscador,
     FSeleccionarConfidencialidad, dSistema, DCliente;
{$R *.DFM}

procedure TEditTAhorroPE_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef CAJAAHORRO}
     HelpContext:= H_REG_TIPO_AHORRO;
     {$else}
      HelpContext:= H70751_Tipo_ahorro;
     {$endif}

     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_NOM_TIPO_AHORRO;
     TB_CONCEPT.LookupDataset := dmCatalogos.cdsConceptos;
     TB_RELATIV.LookupDataset := dmCatalogos.cdsConceptos;
     TB_PRESTA.LookupDataset := dmTablas.cdsTPresta;  
     lValoresConfidencialidad := TStringList.Create;
     {$ifdef CAJAAHORRO}
     TB_INTERES.LookupDataset := dmTablas.cdsTInteres;
     TB_PAGO.LookupDataset := dmCatalogos.cdsConceptos;
     with dmReportes do
     begin
          TB_REP_INS.LookupDataset := cdsLookupReportes;
          TB_REP_PRE.LookupDataset := cdsLookupReportes;
          TB_REP_LIQ.LookupDataset := cdsLookupReportes;
     end;
     {$endif}
end;

procedure TEditTAhorroPE_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     dmReportes.cdsLookupReportes.Conectar;
     dmCatalogos.cdsConceptos.Conectar;
     dmTablas.cdsTPresta.Conectar;
     {$ifdef CAJAAHORRO}
     dmTablas.cdsTInteres.Conectar;
     {$endif}
     DataSource.DataSet := dmTablas.cdsTAhorro;
end;

procedure TEditTAhorroPE_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTAhorro );
end;

{$ifdef CAJAAHORRO}
function TEditTAhorroPE_DevEx.dmTablas: TdmCajaAhorro;
begin
     Result := dmCajaAhorro;
end;
{$endif}


procedure TEditTAhorroPE_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     gbValidarRango.Enabled := TB_VAL_RAN.Checked;
     TB_VAL_RANClick(Nil);
    SetControlesFecha;
    FillListaConfidencialidad;
end;

procedure TEditTAhorroPE_DevEx.TB_VAL_RANClick(Sender: TObject);
begin
     inherited;
     gbValidarRango.Enabled := TB_VAL_RAN.Checked;
     cbDiaIni.Enabled := gbValidarRango.Enabled;
     cbDiaFin.Enabled := gbValidarRango.Enabled;
     cbMesIni.Enabled := gbValidarRango.Enabled;
     cbMesFin.Enabled := gbValidarRango.Enabled;
     lblInicio.Enabled := gbValidarRango.Enabled;
     lblFin.Enabled := gbValidarRango.Enabled;
end;


procedure TEditTAhorroPE_DevEx.EscribirCambios;
begin
     try
        with dmTablas.cdsTAhorro do
        begin
             if zStrToBool( FieldByName('TB_VAL_RAN').AsString ) then
             begin
                  FieldByName('TB_FEC_INI').AsDateTime := EncodeDate( theYear(Date),cbMesIni.ItemIndex + 1,cbDiaIni.ItemIndex + 1);
                  FieldByName('TB_FEC_FIN').AsDateTime := EncodeDate( theYear(Date),cbMesFin.ItemIndex + 1,cbDiaFin.ItemIndex + 1);
             end
             else
             begin
                  FieldByName('TB_FEC_INI').AsDateTime := EncodeDate( theYear(Date), 1, 1);
                  FieldByName('TB_FEC_FIN').AsDateTime := EncodeDate( theYear(Date), 1, 1);
             end;
        end;
     except
           On Error : eConvertError do
           begin
                ZError('Tipos de Ahorros','  Fecha Inv�lida  ',0);
                Abort; //No debe de hacer le inherited
           end;
           On Error : Exception do
           begin
                Raise;
           end;
     end;
     inherited EscribirCambios;
end;

procedure TEditTAhorroPE_DevEx.AlCambiarCombo(Sender:TObject);
begin
     inherited;
     with dmTablas.cdsTAhorro do
     begin
           if not ( Editing or Inserting ) then
              Edit;
     end;
end;

procedure TEditTAhorroPE_DevEx.SetControlesFecha;
begin
     with dmTablas.cdsTAhorro do
     begin
          cbDiaIni.ItemIndex := TheDay( FieldByName('TB_FEC_INI').AsDateTime )-1;
          cbDiaFin.ItemIndex := TheDay( FieldByName('TB_FEC_FIN').AsDateTime )-1;
          cbMesIni.ItemIndex := TheMonth( FieldByName('TB_FEC_INI').AsDateTime )-1;
          cbMesFin.ItemIndex := TheMonth( FieldByName('TB_FEC_FIN').AsDateTime )-1;
     end;
end;

procedure TEditTAhorroPE_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditTAhorroPE_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditTAhorroPE_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditTAhorroPE_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;  
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditTAhorroPE_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditTAhorroPE_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditTAhorroPE_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          SetControlesFecha;
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

end.


