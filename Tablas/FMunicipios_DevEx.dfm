inherited Municipios_DevEx: TMunicipios_DevEx
  Left = 692
  Top = 317
  Caption = 'Municipios'
  ClientHeight = 281
  ClientWidth = 902
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 902
    inherited Slider: TSplitter
      Left = 323
    end
    inherited ValorActivo1: TPanel
      Width = 307
      inherited textoValorActivo1: TLabel
        Width = 301
      end
    end
    inherited ValorActivo2: TPanel
      Left = 326
      Width = 576
      inherited textoValorActivo2: TLabel
        Width = 570
      end
    end
  end
  object Panel1: TPanel [1]
    Left = 0
    Top = 19
    Width = 902
    Height = 41
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 21
      Top = 12
      Width = 36
      Height = 13
      Caption = 'Estado:'
    end
    object ESTADO_look: TZetaKeyLookup_DevEx
      Left = 64
      Top = 8
      Width = 300
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      OnValidKey = ESTADO_lookValidKey
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 60
    Width = 902
    Height = 221
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object TB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'TB_CODIGO'
        Options.Grouping = False
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        Options.Grouping = False
      end
      object ENTIDAD_TXT: TcxGridDBColumn
        Caption = 'Estado'
        DataBinding.FieldName = 'ENTIDAD_TXT'
      end
      object TB_INGLES: TcxGridDBColumn
        Caption = 'Ingl'#233's'
        DataBinding.FieldName = 'TB_INGLES'
        Options.Grouping = False
      end
      object TB_NUMERO: TcxGridDBColumn
        Caption = 'N'#250'mero'
        DataBinding.FieldName = 'TB_NUMERO'
        Options.Grouping = False
      end
      object TB_TEXTO: TcxGridDBColumn
        Caption = 'Texto'
        DataBinding.FieldName = 'TB_TEXTO'
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
