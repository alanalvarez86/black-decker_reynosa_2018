unit FEditPrioAhorrPresta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, ZetaSmartLists, Db, StdCtrls, ExtCtrls,
  ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, ZetaSmartLists_DevEx, ImgList, cxButtons,
  cxControls, cxContainer, cxEdit, cxListBox;

type
  TEditPrioAhorrPresta_DevEx = class(TZetaDlgModal_DevEx)
    Panel1: TPanel;
    Lista: TcxListBox;
    Subir: TZetaSmartListsButton_DevEx;
    Bajar: TZetaSmartListsButton_DevEx;
    procedure FormShow(Sender: TObject);
    procedure SubirClick(Sender: TObject);
    procedure BajarClick(Sender: TObject);
    procedure ListaClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CancelarCambios;
    procedure Cancelar_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FTieneDerechos: boolean;
    procedure Inicializa;
    procedure ActivaControles;
    procedure SetOkCancelar( const lEnabled: boolean );
    procedure EscribirCambios;
  public
    { Public declarations }
  end;

var
  EditPrioAhorrPresta_DevEx: TEditPrioAhorrPresta_DevEx;

implementation

uses
    ZAccesosMgr,
    ZAccesosTress,
    ZetaCommonClasses,
    dTablas;

{$R *.DFM}

procedure TEditPrioAhorrPresta_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Inicializa;
     FTieneDerechos := ZAccesosMgr.CheckDerecho( D_TAB_NOM_TIPO_AHORRO, K_DERECHO_CAMBIO ) and
                       ZAccesosMgr.CheckDerecho( D_TAB_NOM_TIPO_PRESTA, K_DERECHO_CAMBIO );
     ActivaControles;
     SetOkCancelar( False );
end;

procedure TEditPrioAhorrPresta_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TEditPrioAhorrPresta_DevEx.ListaClick(Sender: TObject);
begin
     inherited;
     ActivaControles;
end;

procedure TEditPrioAhorrPresta_DevEx.Inicializa;
begin
     with dmTablas.cdsPrioridAhorroPresta do
     begin
          Refrescar;
          First;
          Lista.Clear;
          while not Eof do
          begin
               with Lista.Items do
               begin
                    AddObject( Format( '(%s) %s = %s', [ FieldByName( 'TB_CLASE' ).AsString,
                                                         FieldByName( 'TB_CODIGO' ).AsString,
                                                         FieldByName( 'TB_ELEMENT' ).AsString ] ),
                               TObject( Integer( Ord( FieldByName( 'TB_CODIGO' ).AsString[ 1 ] ) ) ) );
               end;
               Next;
          end;
          lista.ItemIndex := 0;
     end;
end;

procedure TEditPrioAhorrPresta_DevEx.SubirClick(Sender: TObject);
var
   i: Integer;
begin
     SetOkCancelar( True );
     with Lista do
     begin
          i := ItemIndex;
          if ( i > 0 ) then
          begin
               Items.Exchange( i, ( i - 1 ) );
          end;
     end;
     ActivaControles;
end;

procedure TEditPrioAhorrPresta_DevEx.BajarClick(Sender: TObject);
var
   i: Integer;
begin
     SetOkCancelar( True );
     with Lista do
     begin
          i := ItemIndex;
          with Items do
          begin
               if ( i < ( Count - 1 ) ) then
               begin
                    Exchange( i, ( i + 1 ) );
               end;
          end;
     end;
     ActivaControles;
end;

procedure TEditPrioAhorrPresta_DevEx.ActivaControles;
var
   lHayMas: Boolean;
begin
     if FTieneDerechos then
     begin
          with Lista do
          begin
               lHayMas := ( Items.Count > 0 );
               Subir.Enabled := lHayMas and ( ItemIndex > 0 );
               Bajar.Enabled := lHayMas and ( ItemIndex < ( Items.Count - 1 ) );
          end;
     end
     else
     begin
          Subir.Enabled := FTienederechos;
          Bajar.Enabled := Subir.Enabled;
     end;
end;

procedure TEditPrioAhorrPresta_DevEx.SetOkCancelar( const lEnabled: boolean );
begin
     OK_DevEx.Enabled := lEnabled;
     if ( OK_DevEx.Enabled ) then
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkCancel;
               OptionsImage.ImageIndex := 0;
               ModalResult := mrCancel;
               Cancel := False;
               Caption := '&Cancelar';
               Hint := 'Cancelar Cambios';
          end;
     end
     else
     begin
          with Cancelar_DevEx do
          begin
               //Kind := bkClose;
               OptionsImage.ImageIndex := 2;
               ModalResult := mrCancel;
               Cancel := True;
               Caption := '&Salir';
               Hint := 'Cerrar Pantalla y Salir';
               if Self.Active then  // EZM: S�lo si la forma es visible
                 SetFocus;
          end;
     end;
end;

procedure TEditPrioAhorrPresta_DevEx.EscribirCambios;
begin
     if dmTablas.GrabaPrioridad( Lista.Items ) then
        Close;
end;

procedure TEditPrioAhorrPresta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H70754_Prioridad_de_ahorros_y_prestamos;
end;

procedure TEditPrioAhorrPresta_DevEx.CancelarCambios;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             dmTablas.cdsPrioridAhorroPresta.CancelUpdates;
          finally
                 Cursor := oCursor;
          end;
     end;
end;

procedure TEditPrioAhorrPresta_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
  inherited;
  if(Ok_DevEx.Enabled) then CancelarCambios;
end;

end.
