unit FCatNacCompetencias_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBGrids, Db, ExtCtrls,
     StdCtrls, ZBaseGridLectura_DevEx,
     cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
     dxSkinsCore,  TressMorado2013,
     dxSkinsDefaultPainters, dxSkinscxPCPainter, cxCustomData, cxFilter,
     cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
     cxGridCustomTableView, cxGridTableView, cxGridDBTableView, Menus,
     ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
     ZetaCXGrid;

type
  TCatNacCompetencias_DevEx = class(TBaseGridLectura_DevEx)
    TN_CODIGO: TcxGridDBColumn;
    TN_TITULO: TcxGridDBColumn;
    TN_COMITE: TcxGridDBColumn;
    TN_FEC_APR: TcxGridDBColumn;
    TN_NIVEL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  CatNacCompetencias_DevEx: TCatNacCompetencias_DevEx;

implementation

uses DCatalogos,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,ZetaCommonTools, dTablas,DGlobal,ZGlobalTress;

{$R *.DFM}

{ TMunicipios }

procedure TCatNacCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_CAT_NAC_COMPS;

end;

procedure TCatNacCompetencias_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsTNacComp.Conectar;
          DataSource.DataSet:= cdsTNacComp;
          
     end;
end;

procedure TCatNacCompetencias_DevEx.Refresh;
begin
     dmTablas.cdsTNacComp.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatNacCompetencias_DevEx.Agregar;
begin
     dmTablas.cdsTNacComp.Agregar;
end;

procedure TCatNacCompetencias_DevEx.Borrar;
begin
     dmTablas.cdsTNacComp.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCatNacCompetencias_DevEx.Modificar;
begin
     dmTablas.cdsTNacComp.Modificar;
end;

procedure TCatNacCompetencias_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'Competencia', 'Competencia', 'TN_CODIGO', dmTablas.cdsTNacComp );
end;


procedure TCatNacCompetencias_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('TN_CODIGO'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();


end;

end.
