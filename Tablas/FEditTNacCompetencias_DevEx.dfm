inherited EditTNacCompetencias_DevEx: TEditTNacCompetencias_DevEx
  Left = 325
  Top = 191
  Caption = 'Competencia Nacional'
  ClientHeight = 557
  ClientWidth = 579
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 521
    Width = 579
    TabOrder = 4
    inherited OK_DevEx: TcxButton
      Left = 415
      BiDiMode = bdLeftToRight
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 494
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 579
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 253
      inherited textoValorActivo2: TLabel
        Width = 247
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 579
    Height = 206
    Align = alTop
    TabOrder = 1
    object DBDescripcionLBL: TLabel
      Left = 76
      Top = 27
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'T'#237'tulo:'
    end
    object DBCodigoLBL: TLabel
      Left = 71
      Top = 4
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object Label1: TLabel
      Left = 72
      Top = 50
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Comit'#233':'
    end
    object Label2: TLabel
      Left = 60
      Top = 68
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Prop'#243'sito:'
    end
    object Label3: TLabel
      Left = 17
      Top = 157
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha Aprobaci'#243'n:'
    end
    object Label4: TLabel
      Left = 80
      Top = 179
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nivel:'
    end
    object TN_TITULO: TDBEdit
      Left = 112
      Top = 24
      Width = 422
      Height = 21
      DataField = 'TN_TITULO'
      DataSource = DataSource
      TabOrder = 1
    end
    object TN_CODIGO: TZetaDBEdit
      Left = 112
      Top = 1
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'TN_CODIGO'
      DataSource = DataSource
    end
    object TN_COMITE: TDBEdit
      Left = 112
      Top = 46
      Width = 422
      Height = 21
      DataField = 'TN_COMITE'
      DataSource = DataSource
      TabOrder = 2
    end
    object TN_PROPOSI: TcxDBMemo
      Left = 112
      Top = 68
      DataBinding.DataField = 'TN_PROPOSI'
      DataBinding.DataSource = DataSource
      ParentFont = False
      Properties.ScrollBars = ssVertical
      TabOrder = 3
      Height = 83
      Width = 422
    end
    object TN_FEC_APR: TZetaDBFecha
      Left = 112
      Top = 152
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 4
      Text = '01/may/13'
      Valor = 41395.000000000000000000
      DataField = 'TN_FEC_APR'
      DataSource = DataSource
    end
    object TN_NIVEL: TZetaDBEdit
      Left = 112
      Top = 175
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 5
      DataField = 'TN_NIVEL'
      DataSource = DataSource
    end
  end
  object PageControl1: TcxPageControl [4]
    Left = 0
    Top = 256
    Width = 579
    Height = 265
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = TabSheet1
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 263
    ClientRectLeft = 2
    ClientRectRight = 577
    ClientRectTop = 28
    object TabSheet1: TcxTabSheet
      Caption = 'Actividades Nivel'
      object TN_ACT_NIV: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'TN_ACT_NIV'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 235
        Width = 575
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Perfil'
      ImageIndex = 1
      object TN_PERFIL: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'TN_PERFIL'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 235
        Width = 575
      end
    end
    object TabSheet3: TcxTabSheet
      Caption = 'Criterios Evaluaci'#243'n'
      ImageIndex = 2
      object TN_CR_EVA: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'TN_CR_EVA'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 235
        Width = 575
      end
    end
    object TabSheet4: TcxTabSheet
      Caption = 'Actividades Competentes'
      ImageIndex = 3
      object TN_AC_COMP: TcxDBMemo
        Left = 0
        Top = 0
        Align = alClient
        DataBinding.DataField = 'TN_AC_COMP'
        DataBinding.DataSource = DataSource
        ParentFont = False
        Properties.ScrollBars = ssVertical
        TabOrder = 0
        Height = 235
        Width = 575
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 548
    Top = 73
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
