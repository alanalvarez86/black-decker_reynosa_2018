inherited EditTPrestamoPE_DevEx: TEditTPrestamoPE_DevEx
  Left = 896
  Top = 226
  Caption = 'Tipo de Prestamo'
  ClientHeight = 484
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 86
    Top = 83
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 58
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 81
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 87
    Top = 127
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 77
    Top = 105
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  inherited PanelBotones: TPanel
    Top = 448
    Width = 455
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 288
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 367
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 455
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 129
      inherited textoValorActivo2: TLabel
        Width = 123
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object TB_CODIGO: TZetaDBEdit [8]
    Left = 121
    Top = 37
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [9]
    Left = 121
    Top = 59
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_INGLES: TDBEdit [10]
    Left = 121
    Top = 81
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 4
  end
  object TB_TEXTO: TDBEdit [11]
    Left = 121
    Top = 125
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 6
  end
  object TB_NUMERO: TZetaDBNumero [12]
    Left = 121
    Top = 103
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 5
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_ACTIVO: TDBCheckBox [13]
    Left = 350
    Top = 37
    Width = 51
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'TB_ACTIVO'
    DataSource = DataSource
    TabOrder = 2
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object TabConfidencialidad: TcxPageControl [14]
    Left = 0
    Top = 157
    Width = 455
    Height = 291
    Align = alBottom
    TabOrder = 13
    Properties.ActivePage = TabGenerales
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 287
    ClientRectLeft = 4
    ClientRectRight = 451
    ClientRectTop = 24
    object TabGenerales: TcxTabSheet
      Caption = 'Generales'
      ImageIndex = 0
      object Label3: TLabel
        Left = 1
        Top = 16
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concepto Deducci'#243'n:'
        Transparent = True
      end
      object Label4: TLabel
        Left = 14
        Top = 41
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concepto Relativo:'
        Transparent = True
      end
      object Label7: TLabel
        Left = 13
        Top = 67
        Width = 92
        Height = 13
        Alignment = taRightJustify
        Caption = 'Concepto de Pago:'
        Transparent = True
      end
      object Label6: TLabel
        Left = 68
        Top = 91
        Width = 37
        Height = 13
        Caption = 'En Alta:'
        Transparent = True
      end
      object Label5: TLabel
        Left = 32
        Top = 113
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'En Liquidaci'#243'n:'
        Transparent = True
      end
      object TB_CONCEPT: TZetaDBKeyLookup_DevEx
        Left = 109
        Top = 14
        Width = 283
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 30
        DataField = 'TB_CONCEPT'
        DataSource = DataSource
      end
      object TB_RELATIV: TZetaDBKeyLookup_DevEx
        Left = 109
        Top = 39
        Width = 283
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 30
        DataField = 'TB_RELATIV'
        DataSource = DataSource
      end
      object GBTasas: TcxGroupBox
        Left = 24
        Top = 143
        Caption = ' Tasas de Inter'#233's '
        TabOrder = 2
        Height = 97
        Width = 377
        object Label8: TLabel
          Left = 58
          Top = 22
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa #1:'
          Transparent = True
        end
        object Label9: TLabel
          Left = 58
          Top = 44
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa #2:'
          Transparent = True
        end
        object Label10: TLabel
          Left = 58
          Top = 66
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tasa #3:'
          Transparent = True
        end
        object TB_TASA1: TZetaDBNumero
          Left = 105
          Top = 17
          Width = 100
          Height = 21
          Mascara = mnTasa
          TabOrder = 0
          Text = '0.0 %'
          DataField = 'TB_TASA1'
          DataSource = DataSource
        end
        object TB_TASA2: TZetaDBNumero
          Left = 105
          Top = 42
          Width = 100
          Height = 21
          Mascara = mnTasa
          TabOrder = 1
          Text = '0.0 %'
          DataField = 'TB_TASA2'
          DataSource = DataSource
        end
        object TB_TASA3: TZetaDBNumero
          Left = 105
          Top = 64
          Width = 100
          Height = 21
          Mascara = mnTasa
          TabOrder = 2
          Text = '0.0 %'
          DataField = 'TB_TASA3'
          DataSource = DataSource
        end
      end
      object TB_PAGO: TZetaDBKeyLookup_DevEx
        Left = 109
        Top = 64
        Width = 283
        Height = 21
        Filtro = 'CO_NUMERO < 1000'
        LookupDataset = dmCatalogos.cdsConceptos
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 30
        DataField = 'TB_PAGO'
        DataSource = DataSource
      end
      object TB_ALTA: TZetaDBKeyCombo
        Left = 109
        Top = 89
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 4
        ListaFija = lfAltaAhorroPrestamo
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TB_ALTA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object TB_LIQUIDA: TZetaDBKeyCombo
        Left = 109
        Top = 111
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 5
        ListaFija = lfTipoAhorro
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'TB_LIQUIDA'
        DataSource = DataSource
        LlaveNumerica = True
      end
    end
    object tsConfidencialidad: TcxTabSheet
      Caption = 'Confidencialidad'
      ImageIndex = 2
      object gbConfidencialidad: TcxGroupBox
        Left = 4
        Top = 37
        TabOrder = 0
        Height = 198
        Width = 441
        object btSeleccionarConfiden_DevEx: TcxButton
          Left = 348
          Top = 48
          Width = 23
          Height = 23
          Hint = 'Seleccionar Confidencialidad'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = btSeleccionarConfidenClick
          OptionsImage.Glyph.Data = {
            36090000424D3609000000000000360000002800000018000000180000000100
            200000000000000900000000000000000000000000000000000000B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
            FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
            FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
            EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
            E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
            F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
            FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
            F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
            EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
            F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
            F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
            E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
          OptionsImage.ImageIndex = 0
          OptionsImage.Margin = 0
          PaintStyle = bpsGlyph
        end
        object listaConfidencialidad: TListBox
          Left = 94
          Top = 48
          Width = 251
          Height = 137
          TabStop = False
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 0
        end
        object rbConfidenTodas: TcxRadioButton
          Left = 94
          Top = 10
          Width = 113
          Height = 17
          Caption = 'Todas'
          TabOrder = 1
          OnClick = rbConfidenTodasClick
          Transparent = True
        end
        object rbConfidenAlgunas: TcxRadioButton
          Left = 94
          Top = 28
          Width = 156
          Height = 17
          Caption = 'Aplica algunas'
          TabOrder = 2
          OnClick = rbConfidenAlgunasClick
          Transparent = True
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 284
    Top = 114
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
