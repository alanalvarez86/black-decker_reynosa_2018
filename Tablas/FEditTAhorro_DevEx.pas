unit FEditTAhorro_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, ZetaKeyLookup_DevEx, Mask, Db,
  ExtCtrls, Buttons, ZetaKeyCombo, ZetaEdit,
  {$ifdef CAJAAHORRO}
    dCajaAhorro,
  {$else}
    DTablas,
  {$endif}
  ZetaNumero, ZetaSmartLists, ZetaFecha, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
    TressMorado2013,
  dxSkinsDefaultPainters,  cxControls,
  dxSkinsdxBarPainter, ImgList, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC;


type
  TEditTAhorro_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    CxPageControl: TcxPageControl;
    TabGenerales: TcxTabSheet;
    Label3: TLabel;
    TB_CONCEPT: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    TB_RELATIV: TZetaDBKeyLookup_DevEx;
    Label6: TLabel;
    TB_ALTA: TZetaDBKeyCombo;
    Label5: TLabel;
    TB_LIQUIDA: TZetaDBKeyCombo;
    Label7: TLabel;
    TB_PRESTA: TZetaDBKeyLookup_DevEx;
    cxTabSheet2: TcxTabSheet;
    TB_VAL_RAN: TDBCheckBox;
    gbValidarRango: TGroupBox;
    lblInicio: TLabel;
    lblFin: TLabel;
    cbDiaIni: TComboBox;
    cbMesIni: TComboBox;
    cbDiaFin: TComboBox;
    cbMesFin: TComboBox;
    GBTasas: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    TB_TASA1: TZetaDBNumero;
    TB_TASA2: TZetaDBNumero;
    TB_TASA3: TZetaDBNumero;
    tsConfidencialidad: TcxTabSheet;
    gbConfidencialidad: TGroupBox;
    listaConfidencialidad: TListBox;
    rbConfidenAlgunas: TRadioButton;
    rbConfidenTodas: TRadioButton;
    btSeleccionarConfiden_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TB_VAL_RANClick(Sender: TObject);
    procedure AlCambiarCombo(Sender: TObject);
    // procedure DataSourceDataChange(Sender: TObject; Field: TField); 
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;
  private                       
    lValoresConfidencialidad : TStringList;
    procedure SetControlesFecha;   
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;

  protected
    procedure Connect;override;
    procedure DoLookup; override;
    procedure EscribirCambios;override;
    {$ifdef CAJAAHORRO}
    function dmTablas : TdmCajaAhorro;
    {$endif}
  public
  end;

var
  EditTAhorro_DevEx: TEditTAhorro_DevEx;

implementation

uses
    dCatalogos, ZetaCommonClasses,ZetaCommonTools, ZAccesosTress,ZetaDialogo, ZetaBuscador_DevEx,
    FSeleccionarConfidencialidad_DevEx, dSistema, DCliente;

{$R *.DFM}

procedure TEditTAhorro_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef CAJAAHORRO}
     HelpContext:= H_REG_TIPO_AHORRO;
     {$else}
      HelpContext:= H70751_Tipo_ahorro;
     {$endif}

     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_NOM_TIPO_AHORRO;
     TB_CONCEPT.LookupDataset := dmCatalogos.cdsConceptos;
     TB_RELATIV.LookupDataset := dmCatalogos.cdsConceptos;
     TB_PRESTA.LookupDataset := dmTablas.cdsTPresta;  
     lValoresConfidencialidad := TStringList.Create;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TEditTAhorro_DevEx.Connect;
begin
     dmSistema.cdsNivel0.Conectar;
     dmCatalogos.cdsConceptos.Conectar;
     dmTablas.cdsTPresta.Conectar;
     DataSource.DataSet := dmTablas.cdsTAhorro;
end;

procedure TEditTAhorro_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTAhorro );
end;

{$ifdef CAJAAHORRO}
function TEditTAhorro_DevEx.dmTablas: TdmCajaAhorro;
begin
     Result := dmCajaAhorro;
end;
{$endif}


procedure TEditTAhorro_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     gbValidarRango.Enabled := TB_VAL_RAN.Checked;
     TB_VAL_RANClick(Nil);
    SetControlesFecha;
    FillListaConfidencialidad;
end;

procedure TEditTAhorro_DevEx.TB_VAL_RANClick(Sender: TObject);
begin
     inherited;
     gbValidarRango.Enabled := TB_VAL_RAN.Checked;
     cbDiaIni.Enabled := gbValidarRango.Enabled;
     cbDiaFin.Enabled := gbValidarRango.Enabled;
     cbMesIni.Enabled := gbValidarRango.Enabled;
     cbMesFin.Enabled := gbValidarRango.Enabled;
     lblInicio.Enabled := gbValidarRango.Enabled;
     lblFin.Enabled := gbValidarRango.Enabled;
end;


procedure TEditTAhorro_DevEx.EscribirCambios;
begin
     try
        with dmTablas.cdsTAhorro do
        begin
             if zStrToBool( FieldByName('TB_VAL_RAN').AsString ) then
             begin
                  FieldByName('TB_FEC_INI').AsDateTime := EncodeDate( theYear(Date),cbMesIni.ItemIndex + 1,cbDiaIni.ItemIndex + 1);
                  FieldByName('TB_FEC_FIN').AsDateTime := EncodeDate( theYear(Date),cbMesFin.ItemIndex + 1,cbDiaFin.ItemIndex + 1);
             end
             else
             begin
                  FieldByName('TB_FEC_INI').AsDateTime := EncodeDate( theYear(Date), 1, 1);
                  FieldByName('TB_FEC_FIN').AsDateTime := EncodeDate( theYear(Date), 1, 1);
             end;
        end;
     except
           On Error : eConvertError do
           begin
                ZError('Tipos de Ahorros','  Fecha Inv�lida  ',0);
                Abort; //No debe de hacer le inherited
           end;
           On Error : Exception do
           begin
                Raise;
           end;
     end;
     inherited EscribirCambios;
end;

procedure TEditTAhorro_DevEx.AlCambiarCombo(Sender:TObject);
begin
     inherited;
     with dmTablas.cdsTAhorro do
     begin
           if not ( Editing or Inserting ) then
              Edit;
     end;
end;

procedure TEditTAhorro_DevEx.SetControlesFecha;
begin
     with dmTablas.cdsTAhorro do
     begin
          cbDiaIni.ItemIndex := TheDay( FieldByName('TB_FEC_INI').AsDateTime )-1;
          cbDiaFin.ItemIndex := TheDay( FieldByName('TB_FEC_FIN').AsDateTime )-1;
          cbMesIni.ItemIndex := TheMonth( FieldByName('TB_FEC_INI').AsDateTime )-1;
          cbMesFin.ItemIndex := TheMonth( FieldByName('TB_FEC_FIN').AsDateTime )-1;
     end;
end;

procedure TEditTAhorro_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditTAhorro_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditTAhorro_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditTAhorro_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;  
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditTAhorro_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditTAhorro_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditTAhorro_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          SetControlesFecha;
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

procedure TEditTAhorro_DevEx.SetEditarSoloActivos;
begin
     TB_CONCEPT.EditarSoloActivos := TRUE;
     TB_RELATIV.EditarSoloActivos := TRUE;
     TB_PRESTA.EditarSoloActivos := TRUE;
end;

end.


