unit FReglasPrestamos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, Buttons, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TReglasPrestamos_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    ZetaDBGridLevel1: TcxGridLevel;
    SubeOrden: TcxButton;
    BajaOrden: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure SubeOrdenClick(Sender: TObject);
    procedure BajaOrdenClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    { Public declarations }
    procedure DoLookup; override;
  end;

var
  ReglasPrestamos_DevEx: TReglasPrestamos_DevEx;

implementation

uses DTablas,
     DCatalogos,
     ZetaBuscaEntero_DevEx,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TReglasPrestamos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext := H_FReglasPrestamo;
end;

procedure TReglasPrestamos_DevEx.Connect;
begin
     dmCatalogos.cdsReglaPrestamo.Conectar;
     DataSource.DataSet := dmCatalogos.cdsReglaPrestamo;
end;

procedure TReglasPrestamos_DevEx.Refresh;
begin
     dmCatalogos.cdsReglaPrestamo.Refrescar;
end;

procedure TReglasPrestamos_DevEx.Agregar;
begin
     dmCatalogos.cdsReglaPrestamo.Agregar

end;

procedure TReglasPrestamos_DevEx.Borrar;
begin
     dmCatalogos.cdsReglaPrestamo.Borrar;
end;

procedure TReglasPrestamos_DevEx.Modificar;
begin
     dmCatalogos.cdsReglaPrestamo.Modificar;
end;

procedure TReglasPrestamos_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscaEntero_DevEx.BuscarCodigo( 'C�digo', 'Reglas de Validaci�n de Pr�stamos', 'RP_CODIGO', dmCatalogos.cdsReglaPrestamo );
end;

procedure TReglasPrestamos_DevEx.SubeOrdenClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          SubeBajaOrden(eSubir,cdsReglaPrestamo,cdsReglaPrestamo.FieldByName('RP_CODIGO').AsString,VACIO);
     end;
end;

procedure TReglasPrestamos_DevEx.BajaOrdenClick(Sender: TObject);
begin
     inherited;
     with dmCatalogos do
     begin
          SubeBajaOrden(eBajar,cdsReglaPrestamo,cdsReglaPrestamo.FieldByName('RP_CODIGO').AsString,VACIO);
     end;
end;

procedure TReglasPrestamos_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
     inherited;
     SubeOrden.Enabled := not DataSource.DataSet.IsEmpty;
     BajaOrden.Enabled := not DataSource.DataSet.IsEmpty;
end;

procedure TReglasPrestamos_DevEx.FormShow(Sender: TObject);
begin
  CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;

end;

end.
