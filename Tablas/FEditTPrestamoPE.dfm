inherited EditTPrestamoPE: TEditTPrestamoPE
  Left = 656
  Top = 242
  Caption = 'Tipo de Prestamo'
  ClientHeight = 600
  ClientWidth = 406
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 86
    Top = 83
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 58
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 81
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 87
    Top = 127
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 77
    Top = 105
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label3: TLabel [5]
    Left = 13
    Top = 149
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concepto Deducci'#243'n:'
  end
  object Label4: TLabel [6]
    Left = 26
    Top = 171
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concepto Relativo:'
  end
  object Label5: TLabel [7]
    Left = 44
    Top = 238
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'En Liquidaci'#243'n:'
  end
  object Label6: TLabel [8]
    Left = 80
    Top = 216
    Width = 37
    Height = 13
    Caption = 'En Alta:'
  end
  object Label7: TLabel [9]
    Left = 25
    Top = 194
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concepto de Pago:'
  end
  inherited PanelBotones: TPanel
    Top = 564
    Width = 406
    TabOrder = 15
    inherited OK: TBitBtn
      Left = 240
    end
    inherited Cancelar: TBitBtn
      Left = 325
    end
  end
  inherited PanelSuperior: TPanel
    Width = 406
    TabOrder = 0
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 406
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 80
      inherited textoValorActivo2: TLabel
        Width = 74
      end
    end
  end
  object TB_CONCEPT: TZetaDBKeyLookup [13]
    Left = 121
    Top = 147
    Width = 283
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    LookupDataset = dmCatalogos.cdsConceptos
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 8
    TabStop = True
    WidthLlave = 30
    DataField = 'TB_CONCEPT'
    DataSource = DataSource
  end
  object TB_RELATIV: TZetaDBKeyLookup [14]
    Left = 121
    Top = 169
    Width = 283
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    LookupDataset = dmCatalogos.cdsConceptos
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 9
    TabStop = True
    WidthLlave = 30
    DataField = 'TB_RELATIV'
    DataSource = DataSource
  end
  object TB_LIQUIDA: TZetaDBKeyCombo [15]
    Left = 121
    Top = 236
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 12
    ListaFija = lfTipoAhorro
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_LIQUIDA'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_CODIGO: TZetaDBEdit [16]
    Left = 121
    Top = 37
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [17]
    Left = 121
    Top = 59
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 4
  end
  object TB_INGLES: TDBEdit [18]
    Left = 121
    Top = 81
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_TEXTO: TDBEdit [19]
    Left = 121
    Top = 125
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  object TB_ALTA: TZetaDBKeyCombo [20]
    Left = 121
    Top = 214
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 11
    ListaFija = lfAltaAhorroPrestamo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_ALTA'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_NUMERO: TZetaDBNumero [21]
    Left = 121
    Top = 103
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object GBTasas: TGroupBox [22]
    Left = 16
    Top = 263
    Width = 377
    Height = 97
    Caption = ' Tasas de Inter'#233's '
    TabOrder = 13
    object Label8: TLabel
      Left = 58
      Top = 22
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa #1:'
    end
    object Label9: TLabel
      Left = 58
      Top = 44
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa #2:'
    end
    object Label10: TLabel
      Left = 58
      Top = 66
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa #3:'
    end
    object TB_TASA1: TZetaDBNumero
      Left = 105
      Top = 20
      Width = 100
      Height = 21
      Mascara = mnTasa
      TabOrder = 0
      Text = '0.0 %'
      DataField = 'TB_TASA1'
      DataSource = DataSource
    end
    object TB_TASA2: TZetaDBNumero
      Left = 105
      Top = 42
      Width = 100
      Height = 21
      Mascara = mnTasa
      TabOrder = 1
      Text = '0.0 %'
      DataField = 'TB_TASA2'
      DataSource = DataSource
    end
    object TB_TASA3: TZetaDBNumero
      Left = 105
      Top = 64
      Width = 100
      Height = 21
      Mascara = mnTasa
      TabOrder = 2
      Text = '0.0 %'
      DataField = 'TB_TASA3'
      DataSource = DataSource
    end
  end
  object TB_PAGO: TZetaDBKeyLookup [23]
    Left = 121
    Top = 192
    Width = 283
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    LookupDataset = dmCatalogos.cdsConceptos
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 10
    TabStop = True
    WidthLlave = 30
    DataField = 'TB_PAGO'
    DataSource = DataSource
  end
  object TB_ACTIVO: TDBCheckBox [24]
    Left = 350
    Top = 37
    Width = 51
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'TB_ACTIVO'
    DataSource = DataSource
    TabOrder = 3
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object gbConfidencialidad: TGroupBox [25]
    Left = 16
    Top = 360
    Width = 377
    Height = 198
    Caption = ' Conf&idencialidad: '
    TabOrder = 14
    object btSeleccionarConfiden: TSpeedButton
      Left = 348
      Top = 48
      Width = 23
      Height = 23
      Hint = 'Seleccionar Confidencialidad'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
      OnClick = btSeleccionarConfidenClick
    end
    object listaConfidencialidad: TListBox
      Left = 94
      Top = 48
      Width = 251
      Height = 137
      TabStop = False
      ExtendedSelect = False
      ItemHeight = 13
      TabOrder = 0
    end
    object rbConfidenTodas: TRadioButton
      Left = 94
      Top = 10
      Width = 113
      Height = 17
      Caption = 'Todas'
      TabOrder = 1
      OnClick = rbConfidenTodasClick
    end
    object rbConfidenAlgunas: TRadioButton
      Left = 94
      Top = 28
      Width = 156
      Height = 17
      Caption = 'Aplica algunas'
      TabOrder = 2
      OnClick = rbConfidenAlgunasClick
    end
  end
  inherited DataSource: TDataSource
    Left = 284
    Top = 114
  end
end
