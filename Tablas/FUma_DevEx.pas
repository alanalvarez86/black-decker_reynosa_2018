unit FUma_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  Grids, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TUma_DevEx = class(TBaseGridLectura_DevEx)
    UM_FEC_INI: TcxGridDBColumn;
    UM_VALOR: TcxGridDBColumn;
    UM_VALOR_MENSUAL: TcxGridDBColumn;
    UM_VALOR_ANUAL: TcxGridDBColumn;
    UM_FDESC: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
  public
    { Public declarations }
  end;

var
  Uma_DevEx: TUma_DevEx;

implementation

uses dTablas, ZetaCommonClasses;

{$R *.DFM}

procedure TUma_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70075_UnidadMedida_Actualizacion;
end;

procedure TUma_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsUma.Conectar;
          DataSource.DataSet:= cdsUma;
     end;
end;

procedure TUma_DevEx.Refresh;
begin
     dmTablas.cdsUma.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TUma_DevEx.Agregar;
begin
     dmTablas.cdsUma.Agregar;
end;

procedure TUma_DevEx.Borrar;
begin
     dmTablas.cdsUma.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TUma_DevEx.Modificar;
begin
     dmTablas.cdsUma.Modificar;
end;

procedure TUma_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('UM_FEC_INI'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
