inherited EditTAhorroPE: TEditTAhorroPE
  Left = 889
  Top = 164
  Caption = 'Tipo de Ahorro'
  ClientHeight = 837
  ClientWidth = 417
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 86
    Top = 83
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 58
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 81
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 87
    Top = 127
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 77
    Top = 105
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label3: TLabel [5]
    Left = 13
    Top = 149
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concepto Deducci'#243'n:'
  end
  object Label4: TLabel [6]
    Left = 26
    Top = 171
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concepto Relativo:'
  end
  object Label5: TLabel [7]
    Left = 44
    Top = 239
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'En Liquidaci'#243'n:'
  end
  object Label6: TLabel [8]
    Left = 80
    Top = 217
    Width = 37
    Height = 13
    Caption = 'En Alta:'
  end
  object Label7: TLabel [9]
    Left = 35
    Top = 261
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pr'#233'stamo Capital:'
  end
  object lblPago: TLabel [10]
    Left = 25
    Top = 194
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Concepto de Pago:'
  end
  object lblinteres: TLabel [11]
    Left = 35
    Top = 287
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pr'#233'stamo Inter'#233's:'
  end
  inherited PanelBotones: TPanel
    Top = 801
    Width = 417
    TabOrder = 20
    inherited OK: TBitBtn
      Left = 252
    end
    inherited Cancelar: TBitBtn
      Left = 337
    end
  end
  inherited PanelSuperior: TPanel
    Width = 417
    TabOrder = 0
    inherited BuscarBtn: TSpeedButton
      Visible = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 417
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 91
      inherited textoValorActivo2: TLabel
        Width = 85
      end
    end
  end
  object TB_CONCEPT: TZetaDBKeyLookup [15]
    Left = 121
    Top = 147
    Width = 283
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 8
    TabStop = True
    WidthLlave = 30
    DataField = 'TB_CONCEPT'
    DataSource = DataSource
  end
  object TB_RELATIV: TZetaDBKeyLookup [16]
    Left = 121
    Top = 169
    Width = 283
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 9
    TabStop = True
    WidthLlave = 30
    DataField = 'TB_RELATIV'
    DataSource = DataSource
  end
  object TB_LIQUIDA: TZetaDBKeyCombo [17]
    Left = 121
    Top = 237
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 12
    ListaFija = lfTipoAhorro
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_LIQUIDA'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_CODIGO: TZetaDBEdit [18]
    Left = 121
    Top = 37
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [19]
    Left = 121
    Top = 59
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 4
  end
  object TB_INGLES: TDBEdit [20]
    Left = 121
    Top = 81
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_TEXTO: TDBEdit [21]
    Left = 121
    Top = 125
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 7
  end
  object TB_ALTA: TZetaDBKeyCombo [22]
    Left = 121
    Top = 215
    Width = 145
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 11
    ListaFija = lfAltaAhorroPrestamo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_ALTA'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_NUMERO: TZetaDBNumero [23]
    Left = 121
    Top = 103
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 6
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  object TB_PRESTA: TZetaDBKeyLookup [24]
    Left = 121
    Top = 259
    Width = 283
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 13
    TabStop = True
    WidthLlave = 80
    DataField = 'TB_PRESTA'
    DataSource = DataSource
  end
  object GBTasas: TGroupBox [25]
    Left = 16
    Top = 480
    Width = 385
    Height = 97
    Caption = ' Tasas de Inter'#233's'
    TabOrder = 18
    object Label8: TLabel
      Left = 58
      Top = 22
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa #1:'
    end
    object Label9: TLabel
      Left = 58
      Top = 44
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa #2:'
    end
    object Label10: TLabel
      Left = 58
      Top = 66
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tasa #3:'
    end
    object TB_TASA1: TZetaDBNumero
      Left = 105
      Top = 20
      Width = 100
      Height = 21
      Mascara = mnTasa
      TabOrder = 0
      Text = '0.0 %'
      DataField = 'TB_TASA1'
      DataSource = DataSource
    end
    object TB_TASA2: TZetaDBNumero
      Left = 105
      Top = 42
      Width = 100
      Height = 21
      Mascara = mnTasa
      TabOrder = 1
      Text = '0.0 %'
      DataField = 'TB_TASA2'
      DataSource = DataSource
    end
    object TB_TASA3: TZetaDBNumero
      Left = 105
      Top = 64
      Width = 100
      Height = 21
      Mascara = mnTasa
      TabOrder = 2
      Text = '0.0 %'
      DataField = 'TB_TASA3'
      DataSource = DataSource
    end
  end
  object TB_ACTIVO: TDBCheckBox [26]
    Left = 350
    Top = 37
    Width = 51
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Activo:'
    DataField = 'TB_ACTIVO'
    DataSource = DataSource
    TabOrder = 3
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object gbValidarRango: TGroupBox [27]
    Left = 16
    Top = 397
    Width = 385
    Height = 83
    Caption = '      Validar Rango en que se Permite Agregar un Pr'#233'stamo  '
    TabOrder = 17
    object lblInicio: TLabel
      Left = 73
      Top = 29
      Width = 28
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inicia:'
    end
    object lblFin: TLabel
      Left = 60
      Top = 53
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Termina:'
    end
    object cbDiaIni: TComboBox
      Left = 104
      Top = 25
      Width = 49
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 0
      Text = '1'
      OnChange = AlCambiarCombo
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31')
    end
    object cbMesIni: TComboBox
      Left = 160
      Top = 25
      Width = 89
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 1
      Text = 'Enero'
      OnChange = AlCambiarCombo
      Items.Strings = (
        'Enero'
        'Febrero'
        'Marzo'
        'Abril'
        'Mayo'
        'Junio'
        'Julio'
        'Agosto'
        'Septiembre'
        'Octubre'
        'Noviembre'
        'Diciembre')
    end
    object cbDiaFin: TComboBox
      Left = 104
      Top = 49
      Width = 49
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 2
      Text = '1'
      OnChange = AlCambiarCombo
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20'
        '21'
        '22'
        '23'
        '24'
        '25'
        '26'
        '27'
        '28'
        '29'
        '30'
        '31')
    end
    object cbMesFin: TComboBox
      Left = 160
      Top = 49
      Width = 89
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      ItemIndex = 0
      TabOrder = 3
      Text = 'Enero'
      OnChange = AlCambiarCombo
      Items.Strings = (
        'Enero'
        'Febrero'
        'Marzo'
        'Abril'
        'Mayo'
        'Junio'
        'Julio'
        'Agosto'
        'Septiembre'
        'Octubre'
        'Noviembre'
        'Diciembre')
    end
  end
  object TB_VAL_RAN: TDBCheckBox [28]
    Left = 26
    Top = 400
    Width = 15
    Height = 17
    DataField = 'TB_VAL_RAN'
    DataSource = DataSource
    TabOrder = 16
    ValueChecked = 'S'
    ValueUnchecked = 'N'
    OnClick = TB_VAL_RANClick
  end
  object TB_PAGO: TZetaDBKeyLookup [29]
    Left = 121
    Top = 192
    Width = 283
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    LookupDataset = dmCatalogos.cdsConceptos
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 10
    TabStop = True
    WidthLlave = 30
    DataField = 'TB_PAGO'
    DataSource = DataSource
  end
  object gbReportes: TGroupBox [30]
    Left = 16
    Top = 307
    Width = 385
    Height = 86
    Caption = ' Formatos de impresi'#243'n '
    TabOrder = 15
    object FormatoLBL: TLabel
      Left = 47
      Top = 20
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Inscripci'#243'n:'
    end
    object lblPrestamo: TLabel
      Left = 55
      Top = 41
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'Pr'#233'stamo:'
    end
    object lblRetiro: TLabel
      Left = 71
      Top = 62
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = 'Retiro:'
    end
    object TB_REP_INS: TZetaDBKeyLookup
      Left = 106
      Top = 16
      Width = 274
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 0
      TabStop = True
      WidthLlave = 60
      DataField = 'TB_REP_INS'
      DataSource = DataSource
    end
    object TB_REP_PRE: TZetaDBKeyLookup
      Left = 106
      Top = 37
      Width = 273
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 1
      TabStop = True
      WidthLlave = 60
      DataField = 'TB_REP_PRE'
      DataSource = DataSource
    end
    object TB_REP_LIQ: TZetaDBKeyLookup
      Left = 106
      Top = 58
      Width = 273
      Height = 21
      EditarSoloActivos = False
      IgnorarConfidencialidad = False
      TabOrder = 2
      TabStop = True
      WidthLlave = 60
      DataField = 'TB_REP_LIQ'
      DataSource = DataSource
    end
  end
  object TB_INTERES: TZetaDBKeyLookup [31]
    Left = 121
    Top = 282
    Width = 283
    Height = 21
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 14
    TabStop = True
    WidthLlave = 80
    DataField = 'TB_INTERES'
    DataSource = DataSource
  end
  object gbConfidencialidad: TGroupBox [32]
    Left = 16
    Top = 582
    Width = 385
    Height = 201
    Caption = ' Conf&idencialidad: '
    TabOrder = 19
    object btSeleccionarConfiden: TSpeedButton
      Left = 356
      Top = 48
      Width = 23
      Height = 23
      Hint = 'Seleccionar Confidencialidad'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
      OnClick = btSeleccionarConfidenClick
    end
    object listaConfidencialidad: TListBox
      Left = 94
      Top = 48
      Width = 259
      Height = 137
      TabStop = False
      ExtendedSelect = False
      ItemHeight = 13
      TabOrder = 0
    end
    object rbConfidenTodas: TRadioButton
      Left = 94
      Top = 10
      Width = 113
      Height = 17
      Caption = 'Todas'
      TabOrder = 1
      OnClick = rbConfidenTodasClick
    end
    object rbConfidenAlgunas: TRadioButton
      Left = 94
      Top = 28
      Width = 156
      Height = 17
      Caption = 'Aplica algunas'
      TabOrder = 2
      OnClick = rbConfidenAlgunasClick
    end
  end
  inherited DataSource: TDataSource
    Left = 260
    Top = 18
  end
end
