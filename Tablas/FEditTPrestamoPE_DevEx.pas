unit FEditTPrestamoPE_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, StdCtrls, DBCtrls, ZetaKeyLookup, Mask, Db,
  ExtCtrls, Buttons, ZetaKeyCombo, ZetaEdit, ZetaNumero,
  {$ifdef CAJAAHORRO}
    dCajaAhorro,
  {$else}
    DTablas,
  {$endif}
  ZetaSmartLists, ZBaseEdicion, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxControls, dxBarExtItems, dxBar,
  cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons,
  cxPCdxBarPopupMenu, cxPC, cxContainer, cxEdit, cxGroupBox,
  ZetaKeyLookup_DevEx, cxRadioGroup, dxSkinsCore, TressMorado2013,
  dxSkinsdxBarPainter, dxSkinscxPCPainter;

type
  TEditTPrestamoPE_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_NUMERO: TZetaDBNumero;
    TB_TEXTO: TDBEdit;
    TB_ACTIVO: TDBCheckBox;
    TabConfidencialidad: TcxPageControl;
    TabGenerales: TcxTabSheet;
    tsConfidencialidad: TcxTabSheet;
    Label3: TLabel;
    TB_CONCEPT: TZetaDBKeyLookup_DevEx;
    Label4: TLabel;
    TB_RELATIV: TZetaDBKeyLookup_DevEx;
    GBTasas: TcxGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    TB_TASA1: TZetaDBNumero;
    TB_TASA2: TZetaDBNumero;
    TB_TASA3: TZetaDBNumero;
    gbConfidencialidad: TcxGroupBox;
    btSeleccionarConfiden_DevEx: TcxButton;
    listaConfidencialidad: TListBox;
    rbConfidenTodas: TcxRadioButton;
    rbConfidenAlgunas: TcxRadioButton;
    Label7: TLabel;
    TB_PAGO: TZetaDBKeyLookup_DevEx;
    TB_ALTA: TZetaDBKeyCombo;
    Label6: TLabel;
    Label5: TLabel;
    TB_LIQUIDA: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject); 
    procedure btSeleccionarConfidenClick(Sender: TObject);
    procedure rbConfidenTodasClick(Sender: TObject);
    procedure rbConfidenAlgunasClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private   
    lValoresConfidencialidad : TStringList;
    procedure FillListaConfidencialidad;
    procedure SetListConfidencialidad( sValores : string );
    procedure GetConfidencialidad;
  protected
    procedure Connect;override;
    procedure DoLookup; override;
    {$ifdef CAJAAHORRO}
    function dmTablas : TdmCajaAhorro;
    {$endif}
  public
  end;

var
  EditTPrestamoPE_DevEx: TEditTPrestamoPE_DevEx;

implementation

uses dCatalogos, ZetaCommonClasses, ZAccesosTress, ZetaBuscador_DevEx,
     FSeleccionarConfidencialidad,FSeleccionarConfidencialidad_DevEx, dSistema, DCliente, ZetaCommonTools;
{$R *.DFM}

procedure TEditTPrestamoPE_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef CAJAAHORRO}
     HelpContext:= H_REG_TIPO_PRESTAMO;
     {$else}
      HelpContext:= H70752_Tipo_prestamo;
     {$endif}

     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_NOM_TIPO_PRESTA;
     TB_CONCEPT.LookupDataset := dmCatalogos.cdsConceptos;
     TB_RELATIV.LookupDataset := dmCatalogos.cdsConceptos; 
     lValoresConfidencialidad := TStringList.Create;
     {$ifdef CAJAAHORRO}
     TB_PAGO.LookupDataset := dmCatalogos.cdsConceptos;
     {$endif}
end;

procedure TEditTPrestamoPE_DevEx.Connect;
begin     
     dmSistema.cdsNivel0.Conectar;
     dmCatalogos.cdsConceptos.Conectar;
     {$ifdef CAJAAHORRO}
     dmCajaAhorro.cdsTPresta.Conectar;
     DataSource.DataSet := dmCajaAhorro.cdsTPresta;
     {$else}
     dmTablas.cdsTPresta.Conectar;
     DataSource.DataSet := dmTablas.cdsTPresta;
     {$endif}
end;

procedure TEditTPrestamoPE_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTPresta );
end;

{$ifdef CAJAAHORRO}
function TEditTPrestamoPE_DevEx.dmTablas: TdmCajaAhorro;
begin
     Result := dmCajaAhorro;
end;
{$endif}


procedure TEditTPrestamoPE_DevEx.FormShow(Sender: TObject);
begin
  inherited;
  FillListaConfidencialidad;
end;


procedure TEditTPrestamoPE_DevEx.btSeleccionarConfidenClick(Sender: TObject);
var
   selConfi : TSeleccionarConfidencialidad_DevEx;
   sCM_NIVEL0Nuevo : string;
begin
  inherited;

  selConfi := TSeleccionarConfidencialidad_DevEx.Create( Self );
  sCM_NIVEL0Nuevo := selConfi.GetConfidencialidad( DataSource.DataSet.FieldByName('TB_NIVEL0').AsString, lValoresConfidencialidad, True, dmCliente.Confidencialidad );

  if (  sCM_NIVEL0Nuevo <>  DataSource.DataSet.FieldByName('TB_NIVEL0').AsString ) then
  begin
     with DataSource.DataSet do
     begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

           FieldByName('TB_NIVEL0').AsString  := sCM_NIVEL0Nuevo;
           GetConfidencialidad;
     end;
  end;

  FreeAndNil ( selConfi ) ;
end;

procedure TEditTPrestamoPE_DevEx.FillListaConfidencialidad;
begin
     with dmSistema.cdsNivel0 do
     begin
          First;
          lValoresConfidencialidad.Clear;
          lValoresConfidencialidad.NameValueSeparator := '=' ;
          While( not Eof )do
          begin
               lValoresConfidencialidad.Add(FieldByName('TB_CODIGO').AsString +'='+ FieldByName('TB_ELEMENT').AsString);
               Next;
          end;
     end;
end;

procedure TEditTPrestamoPE_DevEx.GetConfidencialidad;
begin
   if (  DataSource.DataSet  <> nil ) then
   with DataSource.DataSet do
   begin
        SetListConfidencialidad(  FieldByName('TB_NIVEL0').AsString );
   end;
end;

procedure TEditTPrestamoPE_DevEx.SetListConfidencialidad(sValores: string);
var
   Lista : TStringList;
   i, j : integer;
begin
    Lista := TStringList.Create;

    sValores := Trim( sValores );
    if StrLleno( sValores ) then
        Lista.CommaText  := sValores;

    listaConfidencialidad.Items.Clear;

    rbConfidenTodas.OnClick := nil;
    rbConfidenAlgunas.OnClick := nil;
    rbConfidenTodas.Checked := False;
    rbConfidenAlgunas.Checked := False;
                                                 
    if lValoresConfidencialidad.Count = 0 then
       FillListaConfidencialidad;

    for i:= 0 to lValoresConfidencialidad.Count - 1 do
    begin
        if ( Lista.Count > 0 ) then
        begin
                j := Lista.IndexOf( Trim( lValoresConfidencialidad.Names[i] )  );
                if ( j >= 0 ) then
                   listaConfidencialidad.Items.Add( lValoresConfidencialidad[i] )
        end
    end;

    rbConfidenTodas.Checked :=   listaConfidencialidad.Items.Count = 0;
    rbConfidenAlgunas.Checked := not rbConfidenTodas.Checked;

    if ( gbConfidencialidad.Enabled ) then
    begin
        listaConfidencialidad.Enabled := rbConfidenAlgunas.Checked;
        btSeleccionarConfiden_DevEx.Enabled := rbConfidenAlgunas.Checked;
    end;

    rbConfidenTodas.OnClick := rbConfidenTodasClick;
    rbConfidenAlgunas.OnClick := rbConfidenAlgunasClick;

    FreeAndNil( Lista );

end;

procedure TEditTPrestamoPE_DevEx.rbConfidenTodasClick(Sender: TObject);
begin
  inherited;

   with DataSource.DataSet do
   begin
          if not( State in [dsEdit,dsInsert] ) then
             Edit;

        FieldByName('TB_NIVEL0').AsString  :=  '';
   end;
   GetConfidencialidad;
end;

procedure TEditTPrestamoPE_DevEx.rbConfidenAlgunasClick(Sender: TObject);
begin
  inherited;
     if ( rbConfidenAlgunas.Checked ) then
     begin
        btSeleccionarConfidenClick( Sender );
        GetConfidencialidad;
     end;

end;

procedure TEditTPrestamoPE_DevEx.DataSourceDataChange(Sender: TObject;
  Field: TField);
begin
  inherited;
     if ( Field = nil ) then
     begin
          with DataSource.DataSet do
          begin
               if ( State in [dsBrowse] ) then
                  GetConfidencialidad
               else if (State in [dsInsert]) then
                    rbConfidenTodas.Checked := TRUE;
          end;

     end;
end;

end.



