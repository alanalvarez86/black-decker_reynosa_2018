inherited EditMotTools_DevEx: TEditMotTools_DevEx
  Left = 665
  Top = 324
  Caption = 'Motivo de Devoluci'#243'n de Herramienta'
  ClientHeight = 226
  ClientWidth = 402
  PixelsPerInch = 96
  TextHeight = 13
  object DBCodigoLBL: TLabel [0]
    Left = 41
    Top = 40
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 18
    Top = 61
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBInglesLBL: TLabel [2]
    Left = 46
    Top = 82
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object Label2: TLabel [3]
    Left = 37
    Top = 102
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label1: TLabel [4]
    Left = 47
    Top = 123
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label3: TLabel [5]
    Left = 22
    Top = 144
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descuento:'
  end
  object LblPresta: TLabel [6]
    Left = 6
    Top = 167
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pr'#233'stamo:'
  end
  inherited PanelBotones: TPanel
    Top = 190
    Width = 402
    TabOrder = 9
    inherited OK_DevEx: TcxButton
      Left = 237
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 316
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 402
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 76
      inherited textoValorActivo2: TLabel
        Width = 70
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 13
  end
  object TB_CODIGO: TZetaDBEdit [10]
    Left = 81
    Top = 36
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [11]
    Left = 81
    Top = 57
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [12]
    Left = 81
    Top = 78
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [13]
    Left = 81
    Top = 120
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_TIPO: TZetaDBKeyCombo [14]
    Left = 81
    Top = 141
    Width = 149
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 6
    OnChange = TB_TIPOChange
    ListaFija = lfMotTools
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TB_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TB_PRESTA: TZetaDBKeyLookup_DevEx [15]
    Left = 81
    Top = 163
    Width = 282
    Height = 21
    LookupDataset = dmTablas.cdsTPresta
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 7
    TabStop = True
    WidthLlave = 60
    DataField = 'TB_PRESTA'
    DataSource = DataSource
  end
  object TB_NUMERO: TZetaDBNumero [16]
    Left = 81
    Top = 99
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_BuscarBtn: TdxBarButton
      Visible = ivAlways
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
