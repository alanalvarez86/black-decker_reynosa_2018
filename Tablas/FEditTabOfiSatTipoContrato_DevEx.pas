unit FEditTabOfiSatTipoContrato_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx,  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaKeyCombo,ZetaCommonLists, ZetaNumero, ZetaEdit,
  ZetaSmartLists, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinsDefaultPainters;

type
  TEditTabOfiSatTipoContrato_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    Label5: TLabel;
    TB_ACTIVO: TDBCheckBox;
    Label2: TLabel;
    TB_NUMERO: TZetaDBNumero;
    TB_COD_SAT: TDBEdit;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditTabOfiSatTipoContrato_DevEx: TEditTabOfiSatTipoContrato_DevEx;

implementation

uses DTablas, ZetaCommonClasses, ZAccesosTress, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TEditTabOfiSatTipoContrato_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70077_ContratoSat;
     FirstControl := TB_CODIGO;
     IndexDerechos := ZAccesosTress.D_TAB_OFI_SAT_TIPO_CONTRATO;
end;

procedure TEditTabOfiSatTipoContrato_DevEx.Connect;
begin
     Datasource.Dataset := dmTablas.cdsTipoContratoSat;
end;

procedure TEditTabOfiSatTipoContrato_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO',  dmTablas.cdsTipoContratoSat);
end;

end.
