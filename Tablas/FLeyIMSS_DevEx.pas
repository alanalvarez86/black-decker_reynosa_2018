unit FLeyIMSS_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TSistLeyIMSS_DevEX = class(TBaseGridLectura_DevEx)
    SS_INICIAL: TcxGridDBColumn;
    SS_L_A1061: TcxGridDBColumn;
    SS_L_IV: TcxGridDBColumn;
    SS_P_A1061: TcxGridDBColumn;
    SS_P_A1062: TcxGridDBColumn;
    SS_P_A107: TcxGridDBColumn;
    SS_P_A25: TcxGridDBColumn;
    SS_P_SAR: TcxGridDBColumn;
    SS_P_GUARD: TcxGridDBColumn;
    SS_P_IV: TcxGridDBColumn;
    SS_P_CV: TcxGridDBColumn;
    SS_P_INFO: TcxGridDBColumn;
    SS_O_A1062: TcxGridDBColumn;
    SS_O_A107: TcxGridDBColumn;
    SS_O_A25: TcxGridDBColumn;
    SS_O_IV: TcxGridDBColumn;
    SS_O_CV: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    { Public declarations }
  end;

var
  SistLeyIMSS_DevEX: TSistLeyIMSS_DevEX;

implementation

uses dTablas, ZetaCommonClasses;

{$R *.DFM}

procedure TSistLeyIMSS_DevEX.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70762_Cuotas_IMSS;
end;

procedure TSistLeyIMSS_DevEX.Connect;
begin
     with dmTablas do
     begin
          cdsLeyIMSS.Conectar;
          DataSource.DataSet:= cdsLeyIMSS;
     end;
end;

procedure TSistLeyIMSS_DevEX.Refresh;
begin
     dmTablas.cdsLeyIMSS.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistLeyIMSS_DevEX.Agregar;
begin
     dmTablas.cdsLeyImss.Agregar;
end;

procedure TSistLeyIMSS_DevEX.Borrar;
begin
     dmTablas.cdsLeyImss.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSistLeyIMSS_DevEX.Modificar;
begin
     dmTablas.cdsLeyImss.Modificar;
end;

procedure TSistLeyIMSS_DevEX.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('SS_INICIAL'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;

end.
