unit FMonedas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db,
  ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TMonedas_DevEx = class(TBaseGridLectura_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    procedure DoLookup; override;
  end;

var
  Monedas_DevEx: TMonedas_DevEx;

implementation

uses dTablas, ZetaCommonClasses, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TMonedas_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70753_Monedas;
     CanLookup := True;
end;

procedure TMonedas_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsMonedas.Conectar;
          DataSource.DataSet:= cdsMonedas;
     end;
end;

procedure TMonedas_DevEx.Refresh;
begin
     dmTablas.cdsMonedas.Refrescar;
end;

procedure TMonedas_DevEx.Agregar;
begin
     dmTablas.cdsMonedas.Agregar;
end;

procedure TMonedas_DevEx.Borrar;
begin
     dmTablas.cdsMonedas.Borrar;
end;

procedure TMonedas_DevEx.Modificar;
begin
     dmTablas.cdsMonedas.Modificar;
end;

procedure TMonedas_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMonedas );
end;

procedure TMonedas_DevEx.FormShow(Sender: TObject);
begin
CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;

end;

end.
