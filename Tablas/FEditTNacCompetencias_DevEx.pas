unit FEditTNacCompetencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  ExtCtrls, ZetaEdit, ZetaKeyCombo, ZetaNumero,
  ComCtrls, ZetaFecha, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxPC, cxContainer, cxEdit, cxTextEdit, cxMemo,
  cxDBEdit;

type
  TEditTNacCompetencias_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    TN_TITULO: TDBEdit;
    DBDescripcionLBL: TLabel;
    TN_CODIGO: TZetaDBEdit;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    TN_COMITE: TDBEdit;
    Label2: TLabel;
    TN_PROPOSI: TcxDBMemo;
    TN_FEC_APR: TZetaDBFecha;
    Label3: TLabel;
    TN_NIVEL: TZetaDBEdit;
    Label4: TLabel;
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    TabSheet3: TcxTabSheet;
    TabSheet4: TcxTabSheet;
    TN_ACT_NIV: TcxDBMemo;
    TN_PERFIL: TcxDBMemo;
    TN_CR_EVA: TcxDBMemo;
    TN_AC_COMP: TcxDBMemo;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditTNacCompetencias_DevEx: TEditTNacCompetencias_DevEx;

implementation

uses dTablas,
     DGlobal,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaCommonLists,
     ZAccesosTress;

{$R *.DFM}

procedure TEditTNacCompetencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TN_CODIGO;
     HelpContext:= H_CAT_NAC_COMPS;
     IndexDerechos := D_CAT_TAB_NAC_COMP;
end;

procedure TEditTNacCompetencias_DevEx.Connect;
begin
     with dmTablas do
     begin
          DataSource.DataSet := cdsTNacComp;
     end;
     PageControl1.ActivePageIndex := 0; 
end;

procedure TEditTNacCompetencias_DevEx.DoLookup;
begin
     inherited;
     if dxBarButton_BuscarBtn.Enabled then
        ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsTNacComp );
end;

end.
