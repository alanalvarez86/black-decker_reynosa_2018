unit FTabOfiSatRiesgoPuesto_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, ZetaDBGrid, Db,
  ExtCtrls, StdCtrls, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, ZetaCXGrid, System.Actions;

type
  TTabOfiSatRiesgoPuesto_DevEx = class(TBaseGridLectura_DevEx)
    TB_SAT_BAN: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    procedure DoLookup; override;
  end;

var
  TabOfiSatRiesgoPuesto_DevEx: TTabOfiSatRiesgoPuesto_DevEx;

implementation

uses dTablas, ZetaCommonClasses, ZetaBuscador_DevEx;

{$R *.DFM}

procedure TTabOfiSatRiesgoPuesto_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70078_RiesgoPuesto;
     CanLookup := True;
end;

procedure TTabOfiSatRiesgoPuesto_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsRiesgosPuestoSat.Conectar;
          DataSource.DataSet:= cdsRiesgosPuestoSat;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();

end;

procedure TTabOfiSatRiesgoPuesto_DevEx.Refresh;
begin
     dmTablas.cdsRiesgosPuestoSat.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTabOfiSatRiesgoPuesto_DevEx.Agregar;
begin
     dmTablas.cdsRiesgosPuestoSat.Agregar;
end;

procedure TTabOfiSatRiesgoPuesto_DevEx.Borrar;
begin
     dmTablas.cdsRiesgosPuestoSat.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TTabOfiSatRiesgoPuesto_DevEx.Modificar;
begin
     dmTablas.cdsRiesgosPuestoSat.Modificar;
end;

procedure TTabOfiSatRiesgoPuesto_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsRiesgosPuestoSat );
end;

procedure TTabOfiSatRiesgoPuesto_DevEx.FormShow(Sender: TObject);
begin
   CreacolumaSumatoria(ZetaDbGridDbTableView.Columns[0],0,'',skCount);
  inherited;


end;

end.
