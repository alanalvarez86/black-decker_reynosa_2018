unit FEditMotivoBaja_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Db,
  Buttons, ExtCtrls, ZetaEdit, ZetaKeyCombo, ZetaNumero, ZetaSmartLists,
  ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter, dxBarExtItems,
  dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TTOEditMotivoBaja_DevEx = class(TBaseEdicion_DevEx)
    DBInglesLBL: TLabel;
    DBDescripcionLBL: TLabel;
    DBCodigoLBL: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TB_CODIGO: TZetaDBEdit;
    TB_ELEMENT: TDBEdit;
    TB_INGLES: TDBEdit;
    TB_TEXTO: TDBEdit;
    TB_IMSS: TZetaDBKeyCombo;
    DBCampo1LBL: TLabel;
    TB_NUMERO: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  protected
    procedure Connect;override;
    procedure DoLookup; override;
  public
  end;

var
  EditMotivoBaja: TTOEditMotivoBaja_DevEx;

implementation

uses dTablas,
     DGlobal,
     ZetaBuscador_DevEx,
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress;

{$R *.DFM}

procedure TTOEditMotivoBaja_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TB_CODIGO;
     HelpContext:= H70742_Motivo_baja;
     IndexDerechos := ZAccesosTress.D_TAB_HIST_MOT_BAJA;
     Height := 275;//250;
end;

procedure TTOEditMotivoBaja_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsMotivoBaja;
     self.Caption := dmTablas.cdsMotivoBaja.LookupName;
end;

procedure TTOEditMotivoBaja_DevEx.DoLookup;
begin
     inherited;
     ZetaBuscador_DevEx.BuscarCodigo( 'C�digo', Caption, 'TB_CODIGO', dmTablas.cdsMotivoBaja );
end;

end.
