inherited EditTArt80_DevEx: TEditTArt80_DevEx
  Left = 263
  Caption = 'Tabla ISPT y N'#250'mericas'
  ClientHeight = 226
  ClientWidth = 382
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 25
    Top = 116
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vigencia Desde:'
  end
  object Label2: TLabel [1]
    Left = 44
    Top = 141
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  inherited PanelBotones: TPanel
    Top = 190
    Width = 382
    inherited OK_DevEx: TcxButton
      Left = 218
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 297
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 382
    inherited ValorActivo2: TPanel
      Width = 56
      inherited textoValorActivo2: TLabel
        Width = 50
      end
    end
  end
  object Panel1: TPanel [5]
    Left = 0
    Top = 50
    Width = 382
    Height = 50
    Align = alTop
    TabOrder = 3
    object TB_CODIGOlbl: TLabel
      Left = 67
      Top = 9
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object TB_ELEMENTlbl: TLabel
      Left = 44
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object NU_CODIGO: TZetaDBTextBox
      Left = 107
      Top = 8
      Width = 41
      Height = 17
      AutoSize = False
      Caption = 'NU_CODIGO'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'NU_CODIGO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object NU_DESCRIP: TZetaTextBox
      Left = 107
      Top = 29
      Width = 230
      Height = 17
      AutoSize = False
      Caption = 'NU_DESCRIP'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object TI_INICIO: TZetaDBFecha [6]
    Left = 107
    Top = 112
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '19/ene/04'
    Valor = 38005.000000000000000000
    ConfirmEdit = True
    DataField = 'TI_INICIO'
    DataSource = DataSource
  end
  object TI_DESCRIP: TDBEdit [7]
    Left = 107
    Top = 139
    Width = 230
    Height = 21
    DataField = 'TI_DESCRIP'
    DataSource = DataSource
    TabOrder = 5
  end
  inherited DataSource: TDataSource
    Left = 332
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
