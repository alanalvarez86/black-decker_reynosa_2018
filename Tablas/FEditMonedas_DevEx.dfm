inherited EditMonedas_DevEx: TEditMonedas_DevEx
  Left = 231
  Top = 184
  Caption = 'Monedas'
  ClientHeight = 207
  ClientWidth = 402
  PixelsPerInch = 96
  TextHeight = 13
  object DBInglesLBL: TLabel [0]
    Left = 45
    Top = 83
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ingl'#233's:'
  end
  object DBDescripcionLBL: TLabel [1]
    Left = 17
    Top = 62
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descripci'#243'n:'
  end
  object DBCodigoLBL: TLabel [2]
    Left = 40
    Top = 41
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo:'
  end
  object Label1: TLabel [3]
    Left = 46
    Top = 126
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Texto:'
  end
  object Label2: TLabel [4]
    Left = 36
    Top = 105
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero:'
  end
  object Label3: TLabel [5]
    Left = 49
    Top = 147
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor:'
  end
  inherited PanelBotones: TPanel
    Top = 171
    Width = 402
    TabOrder = 8
    inherited OK_DevEx: TcxButton
      Left = 238
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 316
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 402
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 76
      inherited textoValorActivo2: TLabel
        Width = 70
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object TB_CODIGO: TZetaDBEdit [9]
    Left = 80
    Top = 37
    Width = 65
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
    ConfirmEdit = True
    DataField = 'TB_CODIGO'
    DataSource = DataSource
  end
  object TB_ELEMENT: TDBEdit [10]
    Left = 80
    Top = 58
    Width = 280
    Height = 21
    DataField = 'TB_ELEMENT'
    DataSource = DataSource
    TabOrder = 2
  end
  object TB_INGLES: TDBEdit [11]
    Left = 80
    Top = 79
    Width = 280
    Height = 21
    DataField = 'TB_INGLES'
    DataSource = DataSource
    TabOrder = 3
  end
  object TB_TEXTO: TDBEdit [12]
    Left = 80
    Top = 122
    Width = 280
    Height = 21
    DataField = 'TB_TEXTO'
    DataSource = DataSource
    TabOrder = 5
  end
  object TB_VALOR: TZetaDBNumero [13]
    Left = 80
    Top = 143
    Width = 57
    Height = 21
    Mascara = mnPesosDiario
    TabOrder = 6
    Text = '0.00'
    DataField = 'TB_VALOR'
    DataSource = DataSource
  end
  object TB_NUMERO: TZetaDBNumero [14]
    Left = 80
    Top = 101
    Width = 100
    Height = 21
    Mascara = mnNumeroGlobal
    TabOrder = 4
    Text = '0.00'
    DataField = 'TB_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 284
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
