unit FEditUma_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  DBCtrls, StdCtrls, Mask, ZetaFecha,
  ZetaNumero, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditUma_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    Label2: TLabel;
    UM_FEC_INI: TZetaDBFecha;
    UM_VALOR: TZetaDBNumero;
    UM_VALOR_MENSUAL: TZetaDBNumero;
    Label3: TLabel;
    UM_VALOR_ANUAL: TZetaDBNumero;
    Label4: TLabel;
    Label5: TLabel;
    UM_FDESC: TZetaDBNumero;
    procedure FormCreate(Sender: TObject);
  private
  protected
    procedure Connect;override;

  public
    { Public declarations }
  end;

var
  EditUma_DevEx: TEditUma_DevEx;

implementation

uses ZetaCommonClasses, ZAccesosTress, dTablas;

{$R *.DFM}

procedure TEditUma_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70075_UnidadMedida_Actualizacion;
     FirstControl := UM_FEC_INI;
     IndexDerechos := ZAccesosTress.D_TAB_UMA;
end;

procedure TEditUma_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsUma;
end;

end.
