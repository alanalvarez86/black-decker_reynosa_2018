unit FGradoRiesgo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls,
  StdCtrls, ZBaseGridLectura_DevEx, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TRiesgo_DevEx = class(TBaseGridLectura_DevEx)
    RI_CLASE: TcxGridDBColumn;
    RI_GRADO: TcxGridDBColumn;
    RI_INDICE: TcxGridDBColumn;
    RI_PRIMA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
  public
    { Public declarations }
  end;

var
  Riesgo_DevEx: TRiesgo_DevEx;

implementation

{$R *.DFM}


uses dTablas, ZetaCommonClasses;

{ TSistRiesgo }

procedure TRiesgo_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H70764_Grados_riesgo;
end;

procedure TRiesgo_DevEx.Connect;
begin
     with dmTablas do
     begin
          cdsGradosRiesgo.Conectar;
          DataSource.DataSet:= cdsGradosRiesgo;
     end;
end;

procedure TRiesgo_DevEx.Refresh;
begin
     dmTablas.cdsGradosRiesgo.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TRiesgo_DevEx.Agregar;
begin
     dmTablas.cdsGradosRiesgo.Agregar;
end;

procedure TRiesgo_DevEx.Borrar;
begin
     dmTablas.cdsGradosRiesgo.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TRiesgo_DevEx.Modificar;
begin
     dmTablas.cdsGradosRiesgo.Modificar;
end;

procedure TRiesgo_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     //ver la caja de agrupamiento
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('RI_CLASE'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
