unit FEditTArt80_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, DBCtrls, StdCtrls, Mask, ZetaFecha,
  ZetaDBTextBox, ZBaseEdicion_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons;

type
  TEditTArt80_DevEx = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    TB_CODIGOlbl: TLabel;
    TB_ELEMENTlbl: TLabel;
    NU_CODIGO: TZetaDBTextBox;
    NU_DESCRIP: TZetaTextBox;
    TI_INICIO: TZetaDBFecha;
    Label1: TLabel;
    Label2: TLabel;
    TI_DESCRIP: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    function CheckDerechos(const iDerecho: Integer): Boolean;override;
    procedure Connect; override;
    procedure DoCancelChanges; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  EditTArt80_DevEx: TEditTArt80_DevEx;

implementation

uses dTablas,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

{ TEditTArt80 }

procedure TEditTArt80_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FirstControl := TI_INICIO;
     { No se ocupa derecho ya que para llegar a la forma se evalua en
       forma de Edici�n de N�merica el derecho del padre
       IndexDerechos := ZAccesosTress.D_TAB_OFI_ISPT_NUMERICAS;
     }
     //HelpContext := ZetaCommonClasses.H70763_ISPT_numericas; PENDIENTE
end;

procedure TEditTArt80_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     NU_DESCRIP.Caption := dmTablas.cdsEditNumericas.FieldByName( 'NU_DESCRIP' ).AsString;
end;

procedure TEditTArt80_DevEx.Connect;
begin
     DataSource.DataSet := dmTablas.cdsTArt80;
end;

function TEditTArt80_DevEx.CheckDerechos(const iDerecho: Integer): Boolean;
begin
     Result := TRUE;   // Si llega a la forma ya tiene todos los derechos - Llega siempre por medio de FEditNumerica
end;

procedure TEditTArt80_DevEx.DoCancelChanges;
begin
     with ClientDataset do
     begin
          Cancel;    // Solo cancela el registro actual - PENDIENTE Checar que pasa con los hijos
     end;
end;

procedure TEditTArt80_DevEx.EscribirCambios;
begin
     with ClientDataset do
     begin
          Enviar;    // El Enviar hace un post, se graba con el enviar de cdsEditNumericas
     end;
end;

end.
