object dmLabor: TdmLabor
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Left = 193
  Top = 228
  Height = 402
  Width = 741
  object cdsWorks: TZetaClientDataSet
    Tag = 10
    Aggregates = <>
    Filter = '( WK_TIPO < 2 or WK_TIPO > 6 ) and WK_MANUAL = '#39'S'#39
    Params = <>
    BeforePost = cdsWorksBeforePost
    OnNewRecord = cdsWorksNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsWorksAlAdquirirDatos
    AlEnviarDatos = cdsWorksAlEnviarDatos
    AlCrearCampos = cdsWorksAlCrearCampos
    AlAgregar = cdsWorksAlAgregar
    AlBorrar = cdsWorksAlBorrar
    AlModificar = cdsWorksAlModificar
    Left = 32
    Top = 16
  end
  object cdsOpera: TZetaLookupDataSet
    Tag = 8
    Aggregates = <>
    IndexFieldNames = 'OP_NUMBER'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsOperaAlAdquirirDatos
    LookupName = 'Cat'#225'logo de %s'
    LookupDescriptionField = 'OP_NOMBRE'
    LookupKeyField = 'OP_NUMBER'
    OnGetRights = cdsSuperLookupGetRights
    Left = 104
    Top = 16
  end
  object cdsModula1: TZetaLookupDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de %s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 32
    Top = 80
  end
  object cdsModula2: TZetaLookupDataSet
    Tag = 2
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de %s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 104
    Top = 80
  end
  object cdsModula3: TZetaLookupDataSet
    Tag = 3
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de %s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 176
    Top = 80
  end
  object cdsTiempoMuerto: TZetaLookupDataSet
    Tag = 4
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de %s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 248
    Top = 80
  end
  object cdsWOrderLookup: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'WO_NUMBER'
    Params = <>
    ReadOnly = True
    AlCrearCampos = cdsWOrderLookupAlCrearCampos
    LookupName = 'Catalogo de %s'
    LookupDescriptionField = 'WO_DESCRIP'
    LookupKeyField = 'WO_NUMBER'
    OnLookupKey = cdsWOrderLookupLookupKey
    OnLookupSearch = cdsWOrderLookupLookupSearch
    OnGetRights = cdsSuperLookupGetRights
    Left = 176
    Top = 16
  end
  object cdsGridLabor: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = ReconcileError
    AlEnviarDatos = cdsGridLaborAlEnviarDatos
    Left = 248
    Top = 16
    object cdsGridLaborCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
      OnChange = CB_CODIGOChange
      OnValidate = CB_CODIGOValidate
    end
    object cdsGridLaborPRETTYNAME: TStringField
      FieldName = 'PRETTYNAME'
      Size = 50
    end
    object cdsGridLaborCB_PUESTO: TStringField
      FieldName = 'CB_PUESTO'
      Size = 6
    end
  end
  object cdsArea: TZetaLookupDataSet
    Tag = 15
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    ReadOnly = True
    AfterOpen = cdsAreaAfterOpen
    AlAdquirirDatos = cdsAreaAlAdquirirDatos
    LookupName = 'Cat'#225'logo de %s'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 32
    Top = 144
  end
  object cdsPartes: TZetaLookupDataSet
    Tag = 7
    Aggregates = <>
    IndexFieldNames = 'AR_CODIGO'
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = CatalogosAlAdquirirDatos
    LookupName = 'Cat'#225'logo de  %s'
    LookupDescriptionField = 'AR_NOMBRE'
    LookupKeyField = 'AR_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 104
    Top = 144
  end
  object cdsMultiLote: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlEnviarDatos = cdsMultiLoteAlEnviarDatos
    Left = 176
    Top = 144
    object cdsMultiLoteWO_NUMBER: TStringField
      FieldName = 'WO_NUMBER'
      OnChange = cdsMultiLoteWO_NUMBERChange
      OnValidate = cdsWorksLookupsValidate
    end
    object cdsMultiLoteWO_DESCRIP: TStringField
      FieldName = 'WO_DESCRIP'
      Size = 50
    end
    object cdsMultiLoteAR_CODIGO: TStringField
      FieldName = 'AR_CODIGO'
      Size = 15
    end
    object cdsMultiLoteCW_PIEZAS: TFloatField
      FieldName = 'CW_PIEZAS'
    end
  end
  object cdsAusencia: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsAusenciaCalcFields
    AlCrearCampos = cdsAusenciaAlCrearCampos
    Left = 248
    Top = 144
  end
  object cdsCedulas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsCedulasBeforePost
    BeforeCancel = cdsCedulasBeforeCancel
    AfterCancel = cdsCedulasAfterCancel
    AfterDelete = cdsCedulasAfterDelete
    OnNewRecord = cdsCedulasNewRecord
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsCedulasAlAdquirirDatos
    AlEnviarDatos = cdsCedulasAlEnviarDatos
    AlCrearCampos = cdsCedulasAlCrearCampos
    AlAgregar = cdsCedulasAlAgregar
    AlModificar = cdsCedulasAlModificar
    Left = 32
    Top = 200
  end
  object cdsCedEmp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsCedEmpAfterOpen
    AfterInsert = cdsCedulasHuboCambios
    AfterEdit = cdsCedulasHuboCambios
    BeforeDelete = cdsCedEmpBeforeDelete
    AfterDelete = cdsCedulasHuboCambios
    AlAdquirirDatos = cdsCedEmpAlAdquirirDatos
    AlCrearCampos = cdsCedEmpAlCrearCampos
    Left = 104
    Top = 200
    object cdsCedEmpCE_FOLIO: TIntegerField
      FieldName = 'CE_FOLIO'
    end
    object cdsCedEmpCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsCedEmpCE_POSICIO: TSmallintField
      FieldName = 'CE_POSICIO'
    end
    object cdsCedEmpCB_PUESTO: TStringField
      FieldName = 'CB_PUESTO'
      FixedChar = True
      Size = 6
    end
    object cdsCedEmpPRETTYNAME: TStringField
      FieldName = 'PRETTYNAME'
      Origin = 'TRESS DATOS.COLABORA.CB_APE_PAT'
      Size = 93
    end
    object cdsCedEmpCB_TIPO: TSmallintField
      FieldKind = fkLookup
      FieldName = 'CB_TIPO'
      LookupDataSet = dmSuper.cdsEmpleados
      LookupKeyFields = 'CB_CODIGO'
      LookupResultField = 'CB_TIPO'
      KeyFields = 'CB_CODIGO'
      Lookup = True
    end
    object cdsCedEmpCHECADAS: TIntegerField
      FieldKind = fkLookup
      FieldName = 'CHECADAS'
      LookupDataSet = dmSuper.cdsEmpleados
      LookupKeyFields = 'CB_CODIGO'
      LookupResultField = 'CHECADAS'
      KeyFields = 'CB_CODIGO'
      OnGetText = cdsCedEmpCHECADASGetText
      Lookup = True
    end
    object cdsCedEmpRETARDOS: TIntegerField
      FieldKind = fkLookup
      FieldName = 'RETARDOS'
      LookupDataSet = dmSuper.cdsEmpleados
      LookupKeyFields = 'CB_CODIGO'
      LookupResultField = 'RETARDOS'
      KeyFields = 'CB_CODIGO'
      Lookup = True
    end
    object cdsCedEmpSTATUS: TIntegerField
      FieldKind = fkLookup
      FieldName = 'STATUS'
      LookupDataSet = dmSuper.cdsEmpleados
      LookupKeyFields = 'CB_CODIGO'
      LookupResultField = 'STATUS'
      KeyFields = 'CB_CODIGO'
      Lookup = True
    end
  end
  object cdsCedMulti: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsCedMultiAfterOpen
    AfterInsert = cdsCedulasHuboCambios
    AfterEdit = cdsCedulasHuboCambios
    AfterDelete = cdsCedulasHuboCambios
    AlAdquirirDatos = cdsCedMultiAlAdquirirDatos
    AlEnviarDatos = cdsCedMultiAlEnviarDatos
    Left = 176
    Top = 200
    object cdsCedMultiCE_FOLIO: TIntegerField
      FieldName = 'CE_FOLIO'
      Origin = 'TRESS DATOS.CED_WORD.CE_FOLIO'
    end
    object cdsCedMultiWO_NUMBER: TStringField
      FieldName = 'WO_NUMBER'
      Origin = 'TRESS DATOS.CED_WORD.WO_NUMBER'
    end
    object cdsCedMultiCW_POSICIO: TSmallintField
      FieldName = 'CW_POSICIO'
      Origin = 'TRESS DATOS.CED_WORD.CW_POSICIO'
    end
    object cdsCedMultiCW_PIEZAS: TFloatField
      FieldName = 'CW_PIEZAS'
    end
    object cdsCedMultiWO_DESCRIP: TStringField
      FieldName = 'WO_DESCRIP'
      Size = 50
    end
    object cdsCedMultiAR_CODIGO: TStringField
      FieldName = 'AR_CODIGO'
      Size = 15
    end
  end
  object cdsAsignaAreas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 248
    Top = 200
    object cdsAsignaAreasCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsAsignaAreasCB_AREA: TStringField
      FieldName = 'CB_AREA'
      Size = 6
    end
    object cdsAsignaAreasANTERIOR: TStringField
      FieldName = 'ANTERIOR'
      Size = 6
    end
    object cdsAsignaAreasPRETTYNAME: TStringField
      FieldKind = fkLookup
      FieldName = 'PRETTYNAME'
      LookupDataSet = cdsEmpAreas
      LookupKeyFields = 'CB_CODIGO'
      LookupResultField = 'PRETTYNAME'
      KeyFields = 'CB_CODIGO'
      ReadOnly = True
      Size = 50
      Lookup = True
    end
    object cdsAsignaAreasCB_AREA_DESCRIP: TStringField
      Tag = 1
      FieldKind = fkLookup
      FieldName = 'CB_AREA_DESCRIP'
      LookupDataSet = cdsArea
      LookupKeyFields = 'TB_CODIGO'
      LookupResultField = 'TB_ELEMENT'
      KeyFields = 'CB_AREA'
      ReadOnly = True
      OnGetText = cdsAsignaAreasCB_AREA_DESCRIPGetText
      Size = 30
      Lookup = True
    end
    object cdsAsignaAreasANTERIOR_DESCRIP: TStringField
      Tag = 2
      FieldKind = fkLookup
      FieldName = 'ANTERIOR_DESCRIP'
      LookupDataSet = cdsArea
      LookupKeyFields = 'TB_CODIGO'
      LookupResultField = 'TB_ELEMENT'
      KeyFields = 'ANTERIOR'
      ReadOnly = True
      OnGetText = cdsAsignaAreasCB_AREA_DESCRIPGetText
      Size = 30
      Lookup = True
    end
  end
  object cdsEmpAreas: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsEmpAreasAlAdquirirDatos
    LookupDescriptionField = 'PRETTYNAME'
    LookupKeyField = 'CB_CODIGO'
    Left = 32
    Top = 256
  end
  object cdsKarArea: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Descendente'
        Fields = 'KA_FECHA;KA_HORA'
        Options = [ixDescending]
      end>
    IndexName = 'Descendente'
    Params = <>
    StoreDefs = True
    OnReconcileError = ReconcileError
    AlAdquirirDatos = cdsKarAreaAlAdquirirDatos
    AlCrearCampos = cdsKarAreaAlCrearCampos
    Left = 104
    Top = 256
  end
  object cdsEmpBreaks: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsEmpBreaksReconcileError
    AlAdquirirDatos = cdsEmpBreaksAlAdquirirDatos
    AlEnviarDatos = cdsEmpBreaksAlEnviarDatos
    AlCrearCampos = cdsEmpBreaksAlCrearCampos
    Left = 176
    Top = 256
  end
  object cdsDataSet: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 248
    Top = 256
  end
  object cdsKardexDiario: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'KA_HORA'
    Params = <>
    BeforePost = cdsKardexDiarioBeforePost
    AfterDelete = cdsKardexDiarioAfterDelete
    OnNewRecord = cdsKardexDiarioNewRecord
    OnReconcileError = cdsKardexDiarioReconcileError
    AlAdquirirDatos = cdsKardexDiarioAlAdquirirDatos
    AlEnviarDatos = cdsKardexDiarioAlEnviarDatos
    AlCrearCampos = cdsKardexDiarioAlCrearCampos
    AlModificar = cdsKardexDiarioAlModificar
    Left = 32
    Top = 312
  end
  object cdsMaquinas: TZetaLookupDataSet
    Tag = 33
    Aggregates = <>
    Params = <>
    OnNewRecord = cdsMaquinasNewRecord
    AlAdquirirDatos = cdsMaquinasAlAdquirirDatos
    LookupName = 'Cat'#225'logo de M'#225'quinas'
    LookupDescriptionField = 'MQ_NOMBRE'
    LookupKeyField = 'MQ_CODIGO'
    Left = 112
    Top = 312
  end
  object cdsLayMaquinas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLayMaquinasAlAdquiriratos
    Left = 200
    Top = 312
  end
  object cdsKarEmpMaq: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsKarEmpMaqAlAdquirirDatos
    Left = 296
    Top = 312
  end
  object cdsSillasMaquina: TZetaClientDataSet
    Tag = 41
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLayMaquinasAlAdquiriratos
    Left = 376
    Top = 196
  end
  object cdsSillasLayout: TZetaClientDataSet
    Tag = 40
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsLayMaquinasAlAdquiriratos
    Left = 376
    Top = 252
  end
  object cdsCertificEmpleados: TZetaClientDataSet
    Tag = 41
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsCertificEmpleadosAlAdquirirDatos
    Left = 376
    Top = 76
  end
  object cdsCertificMaq: TZetaClientDataSet
    Tag = 41
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsCertificMaqAlCrearCampos
    Left = 368
    Top = 132
  end
  object cdsKarMaquinas: TZetaClientDataSet
    Tag = 37
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsKarMaquinasAlAdquirirDatos
    AlCrearCampos = cdsKarMaquinasAlCrearCampos
    Left = 488
    Top = 132
  end
  object cdsLayoutsAsignados: TZetaLookupDataSet
    Tag = -1
    Aggregates = <>
    Params = <>
    ReadOnly = True
    AlAdquirirDatos = cdsLayoutsAsignadosAlAdquirirDatos
    AlCrearCampos = cdsLayoutsAsignadosAlCrearCampos
    LookupName = 'Cat'#225'logo de Layouts asignados'
    LookupDescriptionField = 'LY_NOMBRE'
    LookupKeyField = 'LY_CODIGO'
    OnGetRights = cdsLayoutsAsignadosGetRights
    Left = 512
    Top = 216
  end
end
