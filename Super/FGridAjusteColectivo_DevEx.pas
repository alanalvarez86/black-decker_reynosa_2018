unit FGridAjusteColectivo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls,
  StdCtrls, ZetaDBGrid, ZetaKeyCombo, ZetaDBTextBox, ComCtrls,
  ZetaSmartLists, ImgList, Mask, ZetaFecha, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, cxNavigator, cxDBNavigator, cxButtons,
  cxLabel, cxBarEditItem;

type
  TGridAjusteColectivo_DevEx = class(TBaseGridEdicion_DevEx)
    zCombo: TZetaDBKeyCombo;
    zComboComio: TZetaDBKeyCombo;
    StatusBar: TStatusBar;
    PanelFiltros: TPanel;
    LabDel: TLabel;
    Fecha: TZetaFecha;
    Label1: TLabel;
    CB_FiltroFijo: TZetaKeyCombo;
    cxBarEditItem_LabDel: TcxBarEditItem;
    cxBarEditItem_Filtro: TcxBarEditItem;
    dxBarButton_btnEntrar: TdxBarButton;
    dxBarButton_btnSalir: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
              DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridColExit(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure TarjetaPuntualClick(Sender: TObject);
    procedure CB_FiltroFijoChange(Sender: TObject);
    procedure FechaChange(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure OK_DevExClick(Sender: TObject);

  private
    { Private declarations }
    FModalidad : TModalResult;
    FAprobarAut : Boolean;
    ValidaEdicion : Boolean;
    BeforeChanges : Integer;
    FFiltroFijo: Integer;
    function EsCampoMotivo(sCampo: String; const lAutorizacion: Boolean = TRUE ): Boolean;
    procedure BuscaHorario;
    procedure BuscaMotivo(const sCampo: String; const lEsAutorizacion: Boolean = TRUE );
    procedure SetColumnasAutorizaciones( const iDerechos: Integer );
    procedure SetColumnasAjusteAsistencia( const iDerechos: Integer );
    procedure SetVisibleColumnasAuto( const oColumna: TColumn; const lActivar: Boolean );
  protected
    { Protected declarations }
    function Editing: Boolean; override;
    procedure Connect; override;
    procedure Agregar; override;
    procedure Buscar; override;
    procedure EscribirCambios; override;
    procedure CancelarCambios; override;
    procedure HabilitaControles; override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    aDerechos : Array [ 0..3 ] of Boolean;
    property Modalidad : TModalResult read FModalidad;
  end;
  TDBGridHack = class(TDBGrid); //declared


var
  GridAjusteColectivo_DevEx: TGridAjusteColectivo_DevEx;

implementation

uses dSuper, dCatalogos, dTablas, dCliente, dGlobal,
     ZetaDialogo, ZGlobalTress,
     ZAccesosMgr, ZAccesosTress, ZetaCommonLists, ZetaCommonClasses, ZetaCommonTools, FToolsAsistencia;

{$R *.DFM}

const
     US_COD_OK = 'OK_';
     HRS_APROB = 'AP_';
     K_MOTIVO = 'M_';
     K_USUARIO_OK = 'US_';
     K_TODOS = 0;
     K_AU_FECHA = 1;
     K_IN_ELEMENT= 2;
     K_CHECADA1 = 3;
     K_MC_CHECADA1 = 4;
     K_CHECADA2 = 5;
     K_MC_CHECADA2 = 6;
     K_CHECADA3 = 7;
     K_MC_CHECADA3 = 8;
     K_CHECADA4 = 9;
     K_MC_CHECADA4 = 10;
     K_HO_CODIGO = 11;
     K_HO_DESCRIP = 12;
     K_AU_STATUS = 13;
     K_AU_HORASCK = 14;
     K_AU_NUM_EXT = 15;
     K_HRS_EXTRAS = 16;
     K_M_HRS_EXTRAS = 17;
     K_PRE_FUERA_JOR = 18;
     K_M_PRE_FUERA_JOR = 19;
     K_PRE_DENTRO_JOR = 20;
     K_M_PRE_DENTRO_JOR = 21;
     K_DESCANSO = 22;
     K_M_DESCANSO = 23;
     K_PER_CG = 24;
     K_M_PER_CG = 25;
     K_PER_CG_ENT = 26;
     K_M_PER_CG_ENT = 27;
     K_PER_SG = 28;
     K_M_PER_SG = 29;
     K_PER_SG_ENT = 30;
     K_M_PER_SG_ENT = 31;
     K_AU_OUT2EAT = 32;

procedure TGridAjusteColectivo_DevEx.FormCreate(Sender: TObject);
begin
     SetColumnasAutorizaciones( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL );
     SetColumnasAjusteAsistencia( D_SUPER_AJUSTAR_ASISTENCIA );
     inherited;
     PrimerColumna := K_CHECADA1;
     dxBarButton_btnEntrar.Tag  := 0;
     dxBarButton_btnSalir.Tag  := 1;
     TDBGridHack( ZetaDBGrid ).OnMouseWheel:= MyMouseWheel;
     HelpContext := H_GRID_AJUSTE_COLECTIVO;
end;

procedure TGridAjusteColectivo_DevEx.FormShow(Sender: TObject);
begin
     ValidaEdicion := TRUE;
     inherited;
     FModalidad := mrNone;
     FAprobarAut := Global.GetGlobalBooleano(K_GLOBAL_APROBAR_AUTORIZACIONES) ;
     StatusBar.Visible := FAprobarAut;
     {
     with DataSource.DataSet do
          if not IsEmpty then
             FechaLbl.Caption := FechaCorta( FieldByName( 'AU_FECHA' ).AsDateTime );  }

     with DataSource.DataSet do
          if not IsEmpty then
             Fecha.Valor := FieldByName( 'AU_FECHA' ).AsDateTime;

end;

procedure TGridAjusteColectivo_DevEx.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     with CB_FiltroFijo do
     begin
          ItemIndex:= K_TODOS; //Se le asigna el itemindex para que en el onchange llegue con el valor nuevo
          Valor:= K_TODOS;
     end;
     CanClose:= TRUE;
end;

procedure TGridAjusteColectivo_DevEx.Connect;
begin
     dmCatalogos.cdsHorarios.Conectar;
     with dmTablas do
     begin
          cdsMotAuto.Conectar;
          cdsIncidencias.Conectar;
          cdsMotCheca.Conectar;
     end;
     with dmSuper do
     begin
          DataSource.DataSet:= cdsGridAsistencia;
          BeforeChanges := cdsGridAsistencia.SavePoint;
     end;
end;

procedure TGridAjusteColectivo_DevEx.Agregar;
begin
     { No hace nada }
end;

procedure TGridAjusteColectivo_DevEx.Buscar;
var
   sCampo : String;
begin
     with ZetaDBGrid do
          if ( not Columns[ SelectedIndex ].ReadOnly ) then
          begin
               sCampo := ZetaDBGrid.SelectedField.FieldName;
               if ( sCampo = 'HO_CODIGO' ) then
                  BuscaHorario
               else if ( EsCampoMotivo( sCampo ) ) then
                  BuscaMotivo( sCampo )
               else if ( EsCampoMotivo( sCampo, FALSE ) ) then
                    BuscaMotivo( sCampo, FALSE )
          end;
end;

procedure TGridAjusteColectivo_DevEx.BuscaHorario;
var
   sHorario, sHorarioDesc: String;
begin
     with dmSuper.cdsGridAsistencia do
     begin
          sHorario := FieldByName( 'HO_CODIGO' ).AsString;
          if ( dmCatalogos.cdsHorarios.Search_DevEx( '', sHorario, sHorarioDesc ) ) then
          begin
               if ( sHorario <> FieldByName( 'HO_CODIGO' ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName('HO_CODIGO').AsString := sHorario;
               end;
          end;
     end;
end;


procedure TGridAjusteColectivo_DevEx.BuscaMotivo( const sCampo: String; const lEsAutorizacion: Boolean);
var
   sMotivo, sMotivoDesc: String;
begin
     with dmSuper.cdsGridAsistencia do
     begin
          sMotivo := FieldByName( sCampo ).AsString;
          if ( ( lEsAutorizacion ) and ( dmTablas.cdsMotAuto.Search_DevEx( Format( 'TB_TIPO = %d OR TB_TIPO = %d',[ K_MOTIVO_AUTORIZ_OFFSET , dmSuper.GetTipoMotivo( sCampo ) ] ), sMotivo, sMotivoDesc ) ) ) or
             ( not lEsAutorizacion and dmTablas.cdsMotCheca.Search_DevEx( VACIO, sMotivo, sMotivoDesc )  ) then
          begin
               if ( sMotivo <> FieldByName( sCampo ).AsString ) then
               begin
                    if not ( State in [ dsEdit, dsInsert ] ) then
                       Edit;
                    FieldByName( sCampo ).AsString := sMotivo;
               end;
          end;
     end;
end;

function TGridAjusteColectivo_DevEx.EsCampoMotivo(sCampo: String; const lAutorizacion: Boolean ): Boolean;
begin
     if lAutorizacion then
     begin
          Result:=  ( sCampo = 'M_HRS_EXTRAS' ) or
                    ( sCampo = 'M_DESCANSO' ) or
                    ( sCampo = 'M_PER_CG' ) or
                    ( sCampo = 'M_PER_CG_ENT' ) or
                    ( sCampo = 'M_PER_SG' ) or
                    ( sCampo = 'M_PER_SG_ENT' ) or
                    ( sCampo = 'M_PRE_FUERA_JOR') or
                    ( sCampo = 'M_PRE_DENTRO_JOR');
     end
     else
         Result:= ( sCampo = 'MC_CHECADA1' ) or
                    ( sCampo = 'MC_CHECADA2' ) or
                    ( sCampo = 'MC_CHECADA3' ) or
                    ( sCampo = 'MC_CHECADA4' );


end;



procedure TGridAjusteColectivo_DevEx.SetVisibleColumnasAuto( const oColumna: TColumn; const lActivar: Boolean );
begin
     with ZetaDBGrid do
     begin
          if ZAccesosMgr.CheckDerecho( D_SUPER_AUTORIZACION_COLECTIVA, K_DERECHO_ALTA ) then   //"ver columnas que no puede modificar"
             oColumna.ReadOnly:= ( not lActivar )
          else
              oColumna.Visible:= lActivar;
     end;
end;

procedure TGridAjusteColectivo_DevEx.SetColumnasAutorizaciones(const iDerechos: Integer);
var
   lActivar: Boolean;
begin

     with ZetaDBGrid do
     begin
          // Aqu� se validar�an las columnas de Motivo si no se quieren ver
          lActivar := ZAccesosMgr.CheckDerecho( iDerechos, HORAS_EXTRAS );

          SetVisibleColumnasAuto( Columns[K_HRS_EXTRAS], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_HRS_EXTRAS], lActivar );

          //Horas Prepagadas fuera y dentro de jornada
          lActivar := ZAccesosMgr.CheckDerecho( iDerechos, PER_PREPAG_FUERADENTROJOR );

          SetVisibleColumnasAuto( Columns[K_PRE_FUERA_JOR], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_PRE_FUERA_JOR], lActivar );
          SetVisibleColumnasAuto( Columns[K_PRE_DENTRO_JOR], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_PRE_DENTRO_JOR], lActivar );


          lActivar := ZAccesosMgr.CheckDerecho( iDerechos, DESCANSO_TRABAJADO );

          SetVisibleColumnasAuto( Columns[K_DESCANSO], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_DESCANSO], lActivar );

          lActivar := ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_CON_GOCE );

          SetVisibleColumnasAuto( Columns[K_PER_CG], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_PER_CG], lActivar );
          SetVisibleColumnasAuto( Columns[K_PER_CG_ENT], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_PER_CG_ENT], lActivar );

          lActivar := ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_SIN_GOCE );

          SetVisibleColumnasAuto( Columns[K_PER_SG], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_PER_SG], lActivar );
          SetVisibleColumnasAuto( Columns[K_PER_SG_ENT], lActivar );
          SetVisibleColumnasAuto( Columns[K_M_PER_SG_ENT], lActivar );

          // Horario
          Columns[K_HO_CODIGO].ReadOnly := not ( ZAccesosMgr.CheckDerecho( iDerechos, DERECHO_HORARIO ) );

          // Tipo de D�a
          Columns[K_AU_STATUS].ReadOnly := not ( ZAccesosMgr.CheckDerecho( iDerechos, DERECHO_TIPO_DIA ) );
          zCombo.Enabled := ( not Columns[K_AU_STATUS].ReadOnly ) ;

          //Comi�
          // Se evalua primero el global y luego el derecho de
          //"ver columnas que no puede modificar" porque �ste tambien evalua el su derecho de modificar
          Columns[K_AU_OUT2EAT].Visible := Global.GetGlobalBooleano( K_GLOBAL_DESCONTAR_COMIDAS );

          if Columns[K_AU_OUT2EAT].Visible then
          begin
               lActivar := ZAccesosMgr.CheckDerecho( iDerechos, DESCUENTO_COMIDAS );
               SetVisibleColumnasAuto( Columns[K_AU_OUT2EAT], lActivar );
               //Columns[28].ReadOnly := not lActivar;
               zComboComio.Enabled := ( NOT Columns[K_AU_OUT2EAT].ReadOnly ) and ( Columns[K_AU_OUT2EAT].Visible );
          end;

          lActivar:= FToolsAsistencia.PermitirCapturaMotivoCH;

          Columns[K_MC_CHECADA1].Visible:= lActivar;
          Columns[K_MC_CHECADA2].Visible:= lActivar;
          Columns[K_MC_CHECADA3].Visible:= lActivar;
          Columns[K_MC_CHECADA4].Visible:= lActivar;
     end;
end;

procedure TGridAjusteColectivo_DevEx.CB_FiltroFijoChange(Sender: TObject);
begin
     inherited;
     with dmSuper.cdsGridAsistencia, CB_FiltroFijo do
     begin
          if FFiltroFijo <> Valor then
          begin
               FFiltroFijo:= Valor;
               DisableControls;
               try
                  Filtered:= FALSE;
                  Filter:= dmSuper.GetFiltroEmpleado( FFiltroFijo );
                  Filtered:= StrLleno(Filter);
               finally
                      EnableControls;
               end;

          end;
     end;
end;


procedure TGridAjusteColectivo_DevEx.SetColumnasAjusteAsistencia(const iDerechos: Integer);
var
   lActivar: Boolean;
begin
     with ZetaDBGrid do
     begin
          lActivar:= CheckDerecho( iDerechos, K_DERECHO_CAMBIO );
          Columns[K_CHECADA1].ReadOnly := not lActivar;
          Columns[K_CHECADA2].ReadOnly := not lActivar;
          Columns[K_CHECADA3].ReadOnly := not lActivar;
          Columns[K_CHECADA4].ReadOnly := not lActivar;
          dxBarButton_btnEntrar.Enabled:= lActivar;
          dxBarButton_btnSalir.Enabled:= lActivar;

          Columns[K_MC_CHECADA1].ReadOnly := not lActivar;
          Columns[K_MC_CHECADA2].ReadOnly := not lActivar;
          Columns[K_MC_CHECADA3].ReadOnly := not lActivar;
          Columns[K_MC_CHECADA4].ReadOnly := not lActivar;

     end;
end;


procedure TGridAjusteColectivo_DevEx.HabilitaControles;
begin
     inherited;
     Self.ActiveControl := ZetaDBGrid;
     Application.ProcessMessages;
end;

procedure TGridAjusteColectivo_DevEx.KeyPress(var Key: Char);

 procedure CampoAprobado( const sField : string );
 begin
      if (ZetaDBGrid.SelectedField.FieldName = sField) OR
         (ZetaDBGrid.SelectedField.FieldName = K_MOTIVO + sField) then
      begin
           with dmSuper do
                if NOT AprobarAut_Derecho( cdsGridAsistencia, US_COD_OK + sField ) then
                   Key := #0;
      end;
 end;
begin
     inherited;
     if GridEnfocado then
     begin
          if ( ( Key <> Chr( 9 ) ) and ( Key <> #0 ) ) then
          begin
               if ( ZetaDBGrid.SelectedField.FieldName = 'AU_STATUS' ) then
               begin
                    if ( zcombo.Enabled ) then
                    begin
                         zCombo.SetFocus;
                         SendMessage( zCombo.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end;
               if ( ZetaDBGrid.SelectedField.FieldName = 'AU_OUT2EAT' ) then
               begin
                    if ( zComboComio.Enabled ) then
                    begin
                         zComboComio.SetFocus;
                         SendMessage( zComboComio.Handle, WM_Char, Word( Key ), 0 );
                    end;
                    Key := #0;
               end;

               CampoAprobado( 'HRS_EXTRAS' );
               CampoAprobado( 'DESCANSO' );
               CampoAprobado( 'PER_CG' );
               CampoAprobado( 'PER_CG_ENT' );
               CampoAprobado( 'PER_SG' );
               CampoAprobado( 'PER_SG_ENT' );
               CampoAprobado('PRE_FUERA_JOR');
               CampoAprobado('PRE_DENTRO_JOR');

          end;
     end
     else
         if ( ActiveControl = zCombo ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   SelectedField := dmSuper.cdsGridAsistencia.FieldByName( 'HRS_EXTRAS' );
              end;
         end
     else
         if ( ActiveControl = zComboComio ) and ( ( Key = Chr( VK_RETURN ) ) or ( Key = Chr( 9 ) ) ) then
         begin
              Key := #0;
              with ZetaDBGrid do
              begin
                   SetFocus;
                   SelectedField := dmSuper.cdsGridAsistencia.FieldByName( 'CB_CODIGO' );
              end;
         end;

end;

procedure TGridAjusteColectivo_DevEx.ZetaDBGridColExit(Sender: TObject);
begin
     inherited;
     with ZetaDBGrid do
     begin
          {if ( SelectedField <> nil ) and
             ( SelectedField.FieldName = 'AU_STATUS' ) and
             ( zCombo.Visible ) then
             zCombo.Visible:= False;}
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'AU_STATUS' ) and
                  ( zCombo.Visible ) then
                  zCombo.Visible:= False;

               if ( SelectedField.FieldName = 'AU_OUT2EAT' ) and
                  ( zComboComio.Visible ) then
                  zComboComio.Visible:= False;
          end;
     end;
end;


procedure TGridAjusteColectivo_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
          DataCol: Integer; Column: TColumn; State: TGridDrawState);

 procedure SetCombo( oCombo: TComboBox );
 begin
      with oCombo do
      begin
           Left := Rect.Left + ZetaDBGrid.Left;
           Top := Rect.Top + ZetaDBGrid.Top;
           Width := Column.Width + 2;
           Visible := True;
      end;
 end;

 function GetStatusAprobado( const sField: string ): string;
 begin
      Result := VACIO;
      with dmSuper.cdsGridAsistencia do
      begin
           if (Column.FieldName = sField ) then
           begin
                if FieldByName( US_COD_OK + sField).AsInteger <> 0 then
                   Result := Format( 'Aprobado Por: %s, Horas Aprobadas: %s',
                                      [ FieldByName( US_COD_OK + sField).AsString + '=' + FieldByName(K_USUARIO_OK + sField).AsString,
                                        FieldByName( HRS_APROB + sField).AsString ] )
                else
                    Result := VACIO;
           end;
      end;
 end;

 procedure CambiaColorAprobado(const sField: string);
 begin
      if (Column.FieldName = sField ) then
      begin
           with ZetaDBGrid do
           begin
                if NOT dmSuper.Aprobar_Autorizaciones(dmSuper.cdsGridAsistencia, 'OK_' + sField ) then
                begin
                     Canvas.Font.Color := clRed;
                end;
                DefaultDrawDataCell( Rect, Column.Field, State );
           end;
      end;
 end;


 var
    sTexto : string;
    //iCurrentRow, iRecNo: Integer;
begin
     //inherited;

     if ( gdFocused in State ) then
     begin
          if ( Column.FieldName = 'AU_STATUS' ) then
          begin
               SetCombo( zCombo );
          end
          else if ( Column.FieldName = 'AU_OUT2EAT' ) then
          begin
               SetCombo( zComboComio );
          end
     end;

     if FAprobarAut then
     begin
          if ( gdSelected in State ) then
          begin
               {La funcion GetStatusAprobado regresa vacio si no esta aprobada la autorizacion.}
               sTexto := GetStatusAprobado( 'HRS_EXTRAS' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'DESCANSO' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'PER_CG' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'PER_CG_ENT' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'PER_SG' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'PER_SG_ENT' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'PRE_FUERA_JOR' );
               if StrVacio(sTexto) then sTexto := GetStatusAprobado( 'PRE_DENTRO_JOR' );
               StatusBar.SimpleText := sTexto;
          end;
     end;


     CambiaColorAprobado( 'HRS_EXTRAS' );
     CambiaColorAprobado( 'DESCANSO' );
     CambiaColorAprobado( 'PER_CG' );
     CambiaColorAprobado( 'PER_CG_ENT' );
     CambiaColorAprobado( 'PER_SG' );
     CambiaColorAprobado( 'PER_SG_ENT' );
     CambiaColorAprobado( 'PRE_FUERA_JOR' );
     CambiaColorAprobado( 'PRE_DENTRO_JOR' );

    {with ZetaDBGrid, Canvas, Brush do
    begin
         if ( State = [] ) then
         begin
              iCurrentRow := Rect.Top div (1+Rect.Bottom-Rect.Top);
              iRecNo:= TDBGridHack( ZetaDBGrid ).Row;
              if iCurrentRow = iRecNo then
              begin
                   Color:= clBtnShadow;
                   Font.Color := clBtnText;
              end;
         end
         else if gdSelected in State then
         begin
              Color:= clHighlight;
              Font.Color := clHighlightText;
         end;
         DefaultDrawColumnCell(Rect, DataCol, Column, State);
    end; }
    inherited;
end;

procedure TGridAjusteColectivo_DevEx.MyMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
begin
     Handled := True;
     with ZetaDBGrid.DataSource.DataSet do
     begin
          if WheelDelta > 0 then
             Prior
          else
              Next;
     end;
end;

procedure TGridAjusteColectivo_DevEx.OKClick(Sender: TObject);
begin
     inherited;
     FModalidad := mrOk;
end;

function TGridAjusteColectivo_DevEx.Editing: Boolean;
begin
     Result := ValidaEdicion;
{
     if not ValidaEdicion then
        Result := FALSE
     else
        Result := inherited Editing;
     }
end;

procedure TGridAjusteColectivo_DevEx.CancelarCambios;
begin
     with dmSuper.cdsGridAsistencia do
          SavePoint := BeforeChanges;
     ValidaEdicion := FALSE;
     Close;
end;

procedure TGridAjusteColectivo_DevEx.EscribirCambios;
begin
     inherited;
     ValidaEdicion := FALSE;
end;

procedure TGridAjusteColectivo_DevEx.TarjetaPuntualClick(Sender: TObject);
const
     aTipoChecada : array [0..1] of {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif} = ('entrada', 'salida');
var
   sChecada: string;
   sCampoChecada: string;
begin
     inherited;
     with dmSuper.cdsGridAsistencia do
     begin
          if (TdxBarButton(Sender).Tag = 0) then
          begin
               if ( not strLleno( FieldByName( 'CHECADA1' ).AsString) )  then
               begin
                    sCampoChecada := 'CHECADA1';
               end;
          end
          else
          begin
               if ( strLleno( FieldByName( 'CHECADA2' ).AsString) )  then
               begin
                  if ( not strLleno( FieldByName( 'CHECADA4' ).AsString) )  then
                      sCampoChecada := 'CHECADA4';
               end
               else
                   sCampoChecada := 'CHECADA2';
          end;
          if (StrLLeno (sCampoChecada))  then
          begin
               if (dmSuper.GetChecadaPuntual(FieldByName( 'HO_CODIGO' ).AsString, TdxBarButton(Sender).Tag, sChecada) ) then
               begin
                    dmSuper.RegistrarTarjetaPuntualColectiva(dmSuper.cdsGridAsistencia, sChecada, sCampoChecada, 'Autorizaciones Colectivas de Asistencia');
               end
               else
               begin
                    Zerror(self.Caption, sChecada, 0);
               end;
               ZetaDBGrid.SelectedField:= FieldByName(sCampoChecada);
          end
          else
              ZError(Self.Caption, Format( 'No se puede agregar checada de %s', [aTipoChecada[TdxBarButton(Sender).Tag]] ), 0 );
     end;
end;

procedure TGridAjusteColectivo_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     with Field do
          if ( Field <> NIL ) and ( not ReadOnly ) and ( FieldName <> 'AU_FECHA' ) then
             dmSuper.CambioGrid:= TRUE;
     ZetaDBGrid.Invalidate;
end;

procedure TGridAjusteColectivo_DevEx.FechaChange(Sender: TObject);
var
   lTieneFiltro: Boolean;
   oCursor: TCursor;
begin
     inherited;
     lTieneFiltro:= FALSE;
     with dmSuper.cdsGridAsistencia, Fecha do
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          DisableControls;
          try
             lTieneFiltro:= Filtered;
             Filtered:= FALSE;
             if ( Valor <> FieldByName('AU_FECHA').AsDateTime ) and ( not dmSuper.RefrescaLista( Valor ) ) then
                Valor:= FieldByName('AU_FECHA').AsDateTime;
             BeforeChanges := SavePoint;
          finally
                 Screen.Cursor := oCursor;
                 Filtered:= lTieneFiltro;
                 EnableControls;
          end;
     end;
end;




procedure TGridAjusteColectivo_DevEx.OK_DevExClick(Sender: TObject);
begin
  inherited;
  FModalidad := mrOk;
end;

end.
