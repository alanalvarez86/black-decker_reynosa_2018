inherited GridOperaciones: TGridOperaciones
  Left = 312
  Top = 311
  Caption = 'Operaciones Registradas'
  ClientHeight = 304
  ClientWidth = 756
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 268
    Width = 756
    inherited OK_DevEx: TcxButton
      Left = 590
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 670
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 756
    inherited ValorActivo2: TPanel
      Width = 430
      inherited textoValorActivo2: TLabel
        Width = 424
      end
    end
  end
  inherited ZetaDBGrid: TZetaDBGrid
    Width = 756
    Height = 218
    OnColExit = ZetaDBGridColExit
    Columns = <
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'WK_HORA_R'
        Title.Caption = 'Hora'
        Width = 45
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'WK_TIPO'
        ReadOnly = True
        Title.Caption = 'Tipo'
        Width = 65
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'WK_PIEZAS'
        Title.Caption = 'Cantidad'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WO_NUMBER'
        Title.Caption = 'Orden'
        Width = 75
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AR_CODIGO'
        Title.Caption = 'Parte'
        Width = 145
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_AREA'
        Title.Caption = #193'rea'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'OP_NUMBER'
        Title.Caption = 'Operaci'#243'n'
        Width = 75
        Visible = True
      end
      item
        ButtonStyle = cbsNone
        Expanded = False
        FieldName = 'WK_STATUS'
        Title.Caption = 'Status'
        Width = 95
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_MOD_1'
        Title.Caption = 'Mod. 1'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_MOD_2'
        Title.Caption = 'Mod. 2'
        Width = 40
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'WK_MOD_3'
        Title.Caption = 'Mod. 3'
        Width = 40
        Visible = True
      end>
  end
  object zCombo: TZetaDBKeyCombo [4]
    Left = 216
    Top = 128
    Width = 105
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    DropDownCount = 3
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 4
    TabStop = False
    Visible = False
    Items.Strings = (
      'Descanso')
    ListaFija = lfStatusLectura
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'WK_STATUS'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DataSource: TDataSource
    Left = 356
    Top = 105
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
end
