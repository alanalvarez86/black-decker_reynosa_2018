inherited EvaluacionMultiple: TEvaluacionMultiple
  Left = 385
  Top = 252
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Evaluaci'#243'n m'#250'ltiple'
  ClientHeight = 273
  ClientWidth = 779
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 237
    Width = 779
    inherited OK: TBitBtn
      Left = 611
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 696
      OnClick = CancelarClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 779
    Height = 33
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 8
      Width = 268
      Height = 13
      Caption = 'Seleccionar el nivel de evaluaci'#243'n de cada competencia'
    end
    object btnNivel: TBitBtn
      Left = 664
      Top = 4
      Width = 107
      Height = 25
      Caption = 'Cambiar Nivel'
      TabOrder = 0
      OnClick = btnNivelClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555775777777
        57705557757777775FF7555555555555000755555555555F777F555555555550
        87075555555555F7577F5555555555088805555555555F755F75555555555033
        805555555555F755F75555555555033B05555555555F755F75555555555033B0
        5555555555F755F75555555555033B05555555555F755F75555555555033B055
        55555555F755F75555555555033B05555555555F755F75555555555033B05555
        555555F75FF75555555555030B05555555555F7F7F75555555555000B0555555
        5555F777F7555555555501900555555555557777755555555555099055555555
        5555777755555555555550055555555555555775555555555555}
      NumGlyphs = 2
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 33
    Width = 779
    Height = 204
    Align = alClient
    TabOrder = 2
    object GridRenglones: TZetaDBGrid
      Left = 1
      Top = 1
      Width = 777
      Height = 202
      Align = alClient
      DataSource = dsEvaluacionMult
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnColExit = GridRenglonesColExit
      OnDrawColumnCell = GridRenglonesDrawColumnCell
      Columns = <
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'PRETTYNAME'
          ReadOnly = True
          Title.Caption = 'Empleado'
          Width = 207
          Visible = True
        end
        item
          Color = clInfoBk
          Expanded = False
          FieldName = 'CC_ELEMENT'
          ReadOnly = True
          Title.Caption = 'Competencia'
          Width = 195
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NC_DESCRIP'
          ReadOnly = True
          Title.Caption = 'Nivel'
          Width = 154
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EC_COMENT'
          Title.Caption = 'Observaciones'
          Width = 194
          Visible = True
        end>
      object btnBuscarPerfil: TSpeedButton
        Left = 544
        Top = 18
        Width = 25
        Height = 17
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33033333333333333F7F3333333333333000333333333333F777333333333333
          000333333333333F777333333333333000333333333333F77733333333333300
          033333333FFF3F777333333700073B703333333F7773F77733333307777700B3
          33333377333777733333307F8F8F7033333337F333F337F3333377F8F9F8F773
          3333373337F3373F3333078F898F870333337F33F7FFF37F333307F99999F703
          33337F377777337F3333078F898F8703333373F337F33373333377F8F9F8F773
          333337F3373337F33333307F8F8F70333333373FF333F7333333330777770333
          333333773FF77333333333370007333333333333777333333333}
        NumGlyphs = 2
        Visible = False
        OnClick = btnBuscarPerfilClick
      end
    end
  end
  object dsEvaluacionMult: TDataSource
    Left = 312
    Top = 8
  end
end
