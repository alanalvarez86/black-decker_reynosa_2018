unit FWizAsistAjusteColectivo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {ZetaKeyLookup,} ExtCtrls, Mask, ZetaNumero, ZetaEdit,
  StdCtrls, Buttons, ComCtrls,  ZetaFecha, ZetaKeyCombo,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013, cxControls,
  cxContainer, cxEdit, ImgList, ZetaKeyLookup_DevEx, cxTextEdit, cxMemo,
  cxImage, cxButtons,ZCXBaseWizardFiltro, ZetaCXWizard,
  cxRadioGroup, dxGDIPlusClasses, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl;

type
  TWizAsistAjusteColectivo_DevEx = class(TBaseCXWizardFiltro)
    DefaultsGB: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    D_EXTRAS: TZetaNumero;
    D_DES_TRA: TZetaNumero;
    D_PER_CG: TZetaNumero;
    D_CG_ENT: TZetaNumero;
    D_PER_SG: TZetaNumero;
    D_SG_ENT: TZetaNumero;
    D_ANTERIOR: TRadioGroup;
    M_D_EXTRAS: TZetaKeyLookup_DevEx;
    M_D_DES_TRA: TZetaKeyLookup_DevEx;
    M_D_PER_CG: TZetaKeyLookup_DevEx;
    M_D_CG_ENT: TZetaKeyLookup_DevEx;
    M_D_PER_SG: TZetaKeyLookup_DevEx;
    M_D_SG_ENT: TZetaKeyLookup_DevEx;
    Label1: TLabel;
    FECHA_AJUSTE: TZetaFecha;
    lblTomoComida: TLabel;
    AU_OUT2EAT: TZetaKeyCombo;
    M_D_PRE_FUERA_JOR: TZetaKeyLookup_DevEx;
    D_PRE_FUERA_JOR: TZetaNumero;
    Label2: TLabel;
    Label9: TLabel;
    D_PRE_DENTRO_JOR: TZetaNumero;
    M_D_PRE_DENTRO_JOR: TZetaKeyLookup_DevEx;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormShow(Sender: TObject);
    procedure SeleccionarClick(Sender: TObject);
  private
    { Private declarations }
    GridEditado : Boolean;
    procedure SetControlesAutorizaciones( const iDerechos: Integer );
    procedure ControlesActivos( oControl : TZetaNumero; oDBLookup: TZetaKeyLookup_DevEx; lEnabled : Boolean );
    procedure LlenaOut2Eat;
    //procedure SetParametroActivo;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
    procedure TerminarWizard; override;
  public
    { Public declarations }
    procedure SetAnimacion(lEnabled: Boolean);
  end;

var
  WizAsistAjusteColectivo_DevEx: TWizAsistAjusteColectivo_DevEx;

implementation

uses DSuper,
     DTablas,
     DCatalogos,
     DCliente,
     DGlobal,
     ZAccesosMgr,
     ZetaTipoEntidad,
     ZAccesosTress,
     ZetaDialogo,
     ZGlobalTress,
     FTressShell,
     FGridAjusteColectivo_DevEx,
     FToolsAsistencia,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TWizAsistAjusteColectivo_DevEx.FormCreate(Sender: TObject);
const
     K_FLT_TIPO_MOTIVO = 'TB_TIPO = %d OR TB_TIPO = %d';

function SetFilter ( eMotivo:eAutorizaChecadas ):string;
begin
     Result := Format ( K_FLT_TIPO_MOTIVO ,[ K_MOTIVO_AUTORIZ_OFFSET, Ord( eMotivo ) ] );
end;

begin
     inherited;
     EmpleadoCodigo := 'COLABORA.CB_CODIGO';
     GridEditado:= FALSE;
     ParametrosControl := FECHA_AJUSTE;
     SetControlesAutorizaciones( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL );
     //SetParametroActivo;
     HelpContext := 00503;

     with dmTablas do
     begin
          M_D_EXTRAS.LookupDataset := cdsMotAuto;
          M_D_DES_TRA.LookupDataset := cdsMotAuto;
          M_D_PER_CG.LookupDataset := cdsMotAuto;
          M_D_CG_ENT.LookupDataset := cdsMotAuto;
          M_D_PER_SG.LookupDataset := cdsMotAuto;
          M_D_SG_ENT.LookupDataset := cdsMotAuto;
          M_D_PRE_FUERA_JOR.LookupDataSet := cdsMotAuto;
          M_D_PRE_DENTRO_JOR.LookupDataSet := cdsMotAuto;

          M_D_EXTRAS.Filtro := SetFilter(acExtras);
          M_D_DES_TRA.Filtro := SetFilter(acDescanso);
          M_D_PER_CG.Filtro := SetFilter(acConGoce);
          M_D_CG_ENT.Filtro := SetFilter(acConGoceEntrada);
          M_D_PER_SG.Filtro := SetFilter(acSinGoce);
          M_D_SG_ENT.Filtro := SetFilter(acSinGoceEntrada);
          M_D_PRE_FUERA_JOR.Filtro := SetFilter(acPrepagFueraJornada );
          M_D_PRE_DENTRO_JOR.Filtro := SetFilter(acPrepagDentroJornada );

     end;

     LlenaOut2Eat;
     AU_OUT2EAT.Valor := Ord( e2Sistema );
end;

procedure TWizAsistAjusteColectivo_DevEx.LlenaOut2Eat;
 var
    eValue : eOut2Eat;
begin
     with AU_OUT2EAT.Lista do
     begin
          BeginUpdate;
          try
             Add( '0=Sin Valor Default' );
             for eValue := ( Low( eOut2Eat ) ) to High( eOut2Eat ) do
             begin
                  Add( IntToStr( Ord( eValue ) + 1 ) + '=' + ObtieneElemento( lfOut2Eat, Ord( eValue ) )  );
             end
          finally
                 EndUpdate;
          end;
     end;
end;


procedure TWizAsistAjusteColectivo_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     dmCatalogos.cdsHorarios.Conectar;
     with dmTablas do
     begin
          cdsIncidencias.Conectar;
          cdsMotAuto.Conectar;
     end;
     FECHA_AJUSTE.Valor := dmCliente.FechaSupervisor;
     DGlobal.SetDescuentoComida( lblTomoComida, AU_OUT2EAT );
end;

procedure TWizAsistAjusteColectivo_DevEx.CargaParametros;
begin
     inherited;
     with ParameterList do
     begin
          AddDate( 'FECHA_AJUSTE', FECHA_AJUSTE.Valor );
          AddFloat( 'D_EXTRAS', D_EXTRAS.Valor );
          AddString( 'M_D_EXTRAS', M_D_EXTRAS.Llave );
          AddFloat( 'D_DES_TRA', D_DES_TRA.Valor );
          AddString( 'M_D_DES_TRA', M_D_DES_TRA.Llave );
          AddFloat( 'D_PER_CG', D_PER_CG.Valor );
          AddString( 'M_D_PER_CG', M_D_PER_CG.Llave );
          AddFloat( 'D_CG_ENT', D_CG_ENT.Valor );
          AddString( 'M_D_CG_ENT', M_D_CG_ENT.Llave );
          AddFloat( 'D_PER_SG', D_PER_SG.Valor );
          AddString( 'M_D_PER_SG', M_D_PER_SG.Llave );
          AddFloat( 'D_SG_ENT', D_SG_ENT.Valor );
          AddString( 'M_D_SG_ENT', M_D_SG_ENT.Llave );
          AddFloat('D_PRE_FUERA_JOR', D_PRE_FUERA_JOR.Valor );
          AddString('M_D_PRE_FUERA_JOR', M_D_PRE_FUERA_JOR.Llave );
          AddFloat('D_PRE_DENTRO_JOR', D_PRE_DENTRO_JOR.Valor );
          AddString('M_D_PRE_DENTRO_JOR', M_D_PRE_DENTRO_JOR.Llave );

          AddInteger( 'D_ANTERIOR', D_ANTERIOR.ItemIndex );
          AddInteger('OUT2EAT', iMax( AU_OUT2EAT.ItemIndex - 1, 0 ) );
          AddBoolean( 'CAMBIACOMIDA', AU_OUT2EAT.ItemIndex <> 0 );
          AddBoolean( 'APROBARAUTORIZACIONES', ZAccesosMgr.CheckDerecho( D_SUPER_AJUSTAR_ASISTENCIA_ESPECIAL, APROBAR_AUTORIZACION ) );
          AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
          AddBoolean('PuedeAgregarTarjeta', ZAccesosMgr.CheckDerecho( D_SUPER_AJUSTAR_ASISTENCIA, K_DERECHO_ALTA ));
     end;
end;

procedure TWizAsistAjusteColectivo_DevEx.CargaListaVerificacion;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             Verificacion := FALSE;
             dmSuper.AjusteColectivoGetLista( ParameterList );
          finally
             Cursor := oCursor;
          end;
     end;
end;

function TWizAsistAjusteColectivo_DevEx.Verificar: Boolean;
begin
     if dmSuper.cdsGridAsistencia.IsEmpty then
     begin
          ZetaDialogo.zInformation( '� Atenci�n !', 'La Lista A Verificar Est� Vac�a', 0 );
          Result := False;
     end
     else
     begin
          GridAjusteColectivo_DevEx := TGridAjusteColectivo_DevEx.Create( Self );
          try
             Self.Hide;
             with GridAjusteColectivo_DevEx do
             begin
                  ShowModal;
                  General := TRUE;
                  Result := ( Modalidad = mrOk );
                  if Result then
                  begin
                       GridEditado := TRUE;
                       EjecutarWizard;
                  end;
                  TerminarWizard;
             end;
          finally
                 FreeAndNil( GridAjusteColectivo_DevEx );
          end;
     end;
end;


function TWizAsistAjusteColectivo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmSuper.AjusteColectivoGrabaLista( Self, ParameterList );
     if Result then
        TressShell.SetDataChange( [ enAusencia ] );
end;

procedure TWizAsistAjusteColectivo_DevEx.TerminarWizard;
begin
     Close;
end;

procedure TWizAsistAjusteColectivo_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);

begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = FiltrosCondiciones ) then
          begin
               SeleccionarClick( Sender );
               CanMove := Verificacion and GridEditado;
               GridEditado:= FALSE;
          end
          else if ( WizardControl.ActivePage = Parametros ) then
          begin
               CanMove := ValidaAutoWizard( Self.Caption, acExtras, D_EXTRAS.Valor, M_D_EXTRAS ) and
                          ValidaAutoWizard( Self.Caption, acDescanso, D_DES_TRA.Valor, M_D_DES_TRA ) and
                          ValidaAutoWizard( Self.Caption, acConGoce, D_PER_CG.Valor, M_D_PER_CG ) and
                          ValidaAutoWizard( Self.Caption, acConGoceEntrada, D_CG_ENT.Valor, M_D_CG_ENT ) and
                          ValidaAutoWizard( Self.Caption, acSinGoce, D_PER_SG.Valor, M_D_PER_SG ) and
                          ValidaAutoWizard( Self.Caption, acSinGoceEntrada, D_SG_ENT.Valor, M_D_SG_ENT ) and
                          ValidaAutoWizard( Self.Caption, acPrepagFueraJornada,  D_PRE_FUERA_JOR.Valor, M_D_PRE_FUERA_JOR ) and
                          ValidaAutoWizard( Self.Caption, acPrepagDentroJornada,  D_PRE_DENTRO_JOR.Valor, M_D_PRE_DENTRO_JOR );
               if CanMove then
               begin
                    CanMove := dmCliente.PuedeCambiarTarjetaDlg( Fecha_Ajuste.Valor, 0 );
               end;
          end;
     end;
end;

procedure TWizAsistAjusteColectivo_DevEx.SetControlesAutorizaciones( const iDerechos: Integer );
{ cv: 27/NOV/2003
  Se quitaron de aqui, porque estan repetidos con las constantes que est� en DSuper.pas
const
     HORAS_EXTRAS = 1;
     DESCANSO_TRABAJADO = 2;
     PERMISO_CON_GOCE = 4;
     PERMISO_SIN_GOCE = 8;
}
 var
    lEnabledComida : Boolean;
begin
     ControlesActivos( D_EXTRAS, M_D_EXTRAS, ZAccesosMgr.CheckDerecho( iDerechos, HORAS_EXTRAS ) );
     ControlesActivos( D_DES_TRA, M_D_DES_TRA, ZAccesosMgr.CheckDerecho( iDerechos, DESCANSO_TRABAJADO ) );
     ControlesActivos( D_PER_CG, M_D_PER_CG, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_CON_GOCE ) );
     ControlesActivos( D_CG_ENT, M_D_CG_ENT, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_CON_GOCE ) );
     ControlesActivos( D_PER_SG, M_D_PER_SG, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_SIN_GOCE ) );
     ControlesActivos( D_SG_ENT, M_D_SG_ENT, ZAccesosMgr.CheckDerecho( iDerechos, PERMISO_SIN_GOCE ) );
     ControlesActivos( D_PRE_FUERA_JOR, M_D_PRE_FUERA_JOR, ZAccesosMgr.CheckDerecho( iDerechos, PER_PREPAG_FUERADENTROJOR ) );
     ControlesActivos( D_PRE_DENTRO_JOR, M_D_PRE_DENTRO_JOR, ZAccesosMgr.CheckDerecho( iDerechos, PER_PREPAG_FUERADENTROJOR ) );

     lEnabledComida := ZAccesosMgr.CheckDerecho( iDerechos, DESCUENTO_COMIDAS );
     lblTomoComida.Enabled := lEnabledComida;
     AU_OUT2EAT.Enabled := lEnabledComida;
end;

procedure TWizAsistAjusteColectivo_DevEx.ControlesActivos( oControl : TZetaNumero; oDBLookup: TZetaKeyLookup_DevEx; lEnabled : Boolean );
begin
     oDBLookup.Enabled:= lEnabled;
     oControl.Enabled := lEnabled;
     if lEnabled then
        oControl.Color := clWindow
     else
        oControl.Color := clBtnFace;
end;

{
procedure TWizAsistAjusteColectivo.SetParametroActivo;
begin
     if D_EXTRAS.Enabled then
        ParametrosControl := D_EXTRAS
     else if D_DES_TRA.Enabled then
        ParametrosControl := D_DES_TRA
     else if D_PER_CG.Enabled then
        ParametrosControl := D_PER_CG
     else if D_CG_ENT.Enabled then
        ParametrosControl := D_CG_ENT
     else if D_PER_SG.Enabled then
        ParametrosControl := D_PER_SG
     else if D_SG_ENT.Enabled then
        ParametrosControl := D_SG_ENT
     else
        ParametrosControl := D_ANTERIOR;
end;
}

procedure TWizAsistAjusteColectivo_DevEx.SeleccionarClick(Sender: TObject);
begin
     GridEditado := FALSE;
     inherited;
end;

procedure TWizAsistAjusteColectivo_DevEx.SetAnimacion( lEnabled: Boolean );
begin
     Application.ProcessMessages;
end;

end.
