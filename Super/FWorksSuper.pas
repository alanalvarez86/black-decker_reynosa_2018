unit FWorksSuper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FWorks_DevEx, StdCtrls, Buttons, Db, Grids, DBGrids, ZetaDBGrid, ExtCtrls,
  ZetaDBTextBox, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TWorksSuper = class(TWorks_DevEx)
    BtnModificarTodas_DevEx: TcxButton;
    procedure BtnModificarTodasClick(Sender: TObject);
    procedure BtnModificarTodas_DevExClick(Sender: TObject);
  private
    { Private declarations }
  protected
  public
    { Public declarations }
  end;

var
  WorksSuper: TWorksSuper;

implementation

uses dLabor, DCliente, ZetaCommonLists, ZetaDialogo;

{$R *.DFM}

procedure TWorksSuper.BtnModificarTodasClick(Sender: TObject);
var
   sMensaje : String;
begin
     inherited;
     if PuedeModificar( sMensaje ) then
        dmLabor.EditarGridOperaciones
     else
        zInformation( Caption, sMensaje, 0 );
end;

procedure TWorksSuper.BtnModificarTodas_DevExClick(Sender: TObject);
var
   sMensaje : String;
begin
     inherited;
     if PuedeModificar( sMensaje ) then
        dmLabor.EditarGridOperaciones
     else
        zInformation( Caption, sMensaje, 0 );
end;

end.
