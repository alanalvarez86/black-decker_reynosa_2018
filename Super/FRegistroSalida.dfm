inherited RegistroSalida: TRegistroSalida
  Left = 242
  Top = 138
  Caption = 'Registro de %s ( Salida )'
  ClientHeight = 306
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 270
  end
  inherited Operaciones: TPageControl
    Height = 165
    inherited Generales: TTabSheet
      object Panel2: TPanel
        Left = 0
        Top = 115
        Width = 504
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object LblNewArea: TLabel
          Left = 83
          Top = 4
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Destino:'
        end
        object NEWAREA: TZetaKeyLookup
          Left = 125
          Top = 0
          Width = 327
          Height = 21
          LookupDataset = dmLabor.cdsArea
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
      end
    end
  end
end
