unit FRegistroSalida;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FRegistroWorks, Db, StdCtrls, ZetaKeyCombo, ZetaHora, ComCtrls, Mask,
  ZetaFecha, ExtCtrls, DBCtrls, Buttons, ZetaNumero;

type
  TRegistroSalida = class(TRegistroWorks)
    Panel2: TPanel;
    LblNewArea: TLabel;
    NEWAREA: TZetaKeyLookup;
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure SetControlesVisibles; override;
    procedure SetOtrosDatos( const lMultiLote: Boolean ); override;
  public
    { Public declarations }
  end;

var
  RegistroSalida: TRegistroSalida;

implementation

uses dLabor, ZetaCommonClasses;

{$R *.DFM}

procedure TRegistroSalida.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 9505;
     Control := NEWAREA;
     NewArea.LookupDataset := dmLabor.cdsArea;
     lFalso := False;
end;

procedure TRegistroSalida.FormShow(Sender: TObject);
begin
     inherited;
     NEWAREA.Llave := VACIO;
end;

procedure TRegistroSalida.SetControlesVisibles;
begin
     inherited;
     LblCantidad.Visible := FALSE;
     WK_PIEZAS.Visible := FALSE;
end;

procedure TRegistroSalida.OKClick(Sender: TObject);
begin
     dmLabor.OtrosDatos := VarArrayOf( [ CB_CODIGO.Valor, NEWAREA.Llave ] );
     inherited;
     if dmLabor.cdsWorks.ChangeCount = 0 then    // Se Aplicaron los cambios
        NEWAREA.Llave := VACIO;
end;

procedure TRegistroSalida.SetOtrosDatos(const lMultiLote: Boolean);
begin
     dmLabor.cdsWorks.FieldByName('WK_PIEZAS').AsFloat := 0;
end;

end.
