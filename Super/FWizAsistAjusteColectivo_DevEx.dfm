inherited WizAsistAjusteColectivo_DevEx: TWizAsistAjusteColectivo_DevEx
  Left = 218
  Top = 153
  Caption = 'Autorizaciones Colectivas de Asistencia'
  ClientHeight = 484
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 455
    Height = 484
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EC100000EC101B891
      6BED00000301494441545847C556CF6B1341148E2855B4872A227AF2A2A04541
      FC011E8A54F00FF0A0E0C183A082DE3C7A909D6C12B5D9D9344252512214AAF9
      7D28E849147A88A820B4D6D26C3C15CC41C1430F1E7AF010DFDBBCDD6693B73F
      2620FDE00BECE6BDF9BE79336F76629D4E27A60221C40E53A46F6574396BEAC6
      8AA9CB2F6642660D615CA29048405D5B5BC54046648ED882BAEC70445379911F
      A5F040281B80199E92BAF18713EEE3125689D27CA16C00CABDC088F114C67D4A
      F385920158F3CBAC900FB15250B18394CE42CD802E5FF68B84521837289D85AA
      81A50181304267503A8BC806ECB6D3E5DF018170BEA3215844368080355D6704
      022975394FE92C140DC84F9C4820136692D25928193013468E1509A014F20AA5
      B3503200834D72227EC425CB8AEC18A5B3503280D8D28308A150853674CE2E4A
      F385B2018414E93B2010D4923F4D611EA3F0400C6500819B0B843813EDA8E288
      A10D20ECCF329C74D09E6F606FD4E1DCBF17A5ECBDF018D84A767F86001ED1F6
      AD489813F44A09910CA472F59DF1D2B79320B68D5EB980D29771EDF1D39B16E9
      B3F4DA837879E5843EF779841E07E06B402F2E8E6995D5475AB5B5216AAD8E28
      AD9EA1BF5C389B110C34B86B9828374FDBB940ADD62A884AEB10FDE58235102F
      2E8FBBC2CE00D5D62FC39CF388E085030CAC71874EBCB4BC5BAB59EBBD63D8AC
      5A93146263C080A8342F0E24116116EEF77D3A91DF0B33B7A8FD3C970F58AEED
      103BCF8D8184C9DCA450AF0151B7F6F4CFBC9FA9C2FBF3762C6C4030F0DB3100
      D5B86E0F0280A5BBC6E57A48CBE131A055AD1936B88710F3E3F1C3DC018C8733
      A04006369C3D907AF6F62897D74FA85043CCAE6D1A88BF5ADCCF057204130FBA
      069E5C4003CEC5636AEA39AEFB772E8765D51A770DC0C33936C887C9170BC751
      140CB49DF283B12417EB4758EEABAE01ADD6BCCB05F911673A9D7C3A2A13191D
      CBDFDB725189ADB969A062D5B9A0206A95E66DD88C23A999D7FBD8960B21EE83
      9E0AB43E7041618C17BF1E0E6AB9207A0D54AD8F5C5018C3DA3688FD15687041
      FF935D039DD83F6DF4C116D2DC57030000000049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 'Proceso que registra autorizaciones para un grupo de empleados.'
      Header.Title = 'Autorizaciones colectivas de asistencia'
      object Label1: TLabel
        Left = 14
        Top = 9
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha:'
        Color = clWhite
        FocusControl = FECHA_AJUSTE
        ParentColor = False
      end
      object DefaultsGB: TGroupBox
        Left = 3
        Top = 32
        Width = 441
        Height = 313
        Caption = ' Valores Default '
        Color = clWhite
        ParentColor = False
        TabOrder = 1
        object Label3: TLabel
          Left = 78
          Top = 22
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Horas &Extras:'
          Color = clWhite
          FocusControl = D_EXTRAS
          ParentColor = False
        end
        object Label4: TLabel
          Left = 39
          Top = 47
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = '&Descanso Trabajado:'
          Color = clWhite
          FocusControl = D_DES_TRA
          ParentColor = False
        end
        object Label5: TLabel
          Left = 78
          Top = 72
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso C/&G:'
          Color = clWhite
          FocusControl = D_PER_CG
          ParentColor = False
        end
        object Label6: TLabel
          Left = 38
          Top = 97
          Width = 103
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso C/G E&ntrada:'
          Color = clWhite
          FocusControl = D_CG_ENT
          ParentColor = False
        end
        object Label7: TLabel
          Left = 78
          Top = 122
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = '&Permiso S/G:'
          Color = clWhite
          FocusControl = D_PER_SG
          ParentColor = False
        end
        object Label8: TLabel
          Left = 38
          Top = 147
          Width = 103
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per&miso S/G Entrada:'
          Color = clWhite
          FocusControl = D_SG_ENT
          ParentColor = False
        end
        object lblTomoComida: TLabel
          Left = 73
          Top = 224
          Width = 68
          Height = 13
          Caption = 'Tom'#243' Comida:'
          Color = clWhite
          ParentColor = False
        end
        object Label2: TLabel
          Left = 10
          Top = 173
          Width = 131
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prepagadas Fuera &Jornada:'
          Color = clWhite
          ParentColor = False
        end
        object Label9: TLabel
          Left = 5
          Top = 195
          Width = 136
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prepagadas &Dentro Jornada:'
          Color = clWhite
          ParentColor = False
        end
        object D_EXTRAS: TZetaNumero
          Left = 147
          Top = 19
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 0
          Text = '0.00'
        end
        object D_DES_TRA: TZetaNumero
          Left = 147
          Top = 44
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 2
          Text = '0.00'
        end
        object D_PER_CG: TZetaNumero
          Left = 147
          Top = 69
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 4
          Text = '0.00'
        end
        object D_CG_ENT: TZetaNumero
          Left = 147
          Top = 94
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 6
          Text = '0.00'
        end
        object D_PER_SG: TZetaNumero
          Left = 147
          Top = 119
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 8
          Text = '0.00'
        end
        object D_SG_ENT: TZetaNumero
          Left = 147
          Top = 144
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 10
          Text = '0.00'
        end
        object D_ANTERIOR: TRadioGroup
          Left = 64
          Top = 250
          Width = 320
          Height = 49
          Caption = ' En caso de Existir Autorizaci'#243'n '
          Color = clWhite
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Dejar Anterior'
            '     Sumar'
            '    Sustituir')
          ParentColor = False
          TabOrder = 17
        end
        object M_D_EXTRAS: TZetaKeyLookup_DevEx
          Left = 204
          Top = 19
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 40
        end
        object M_D_DES_TRA: TZetaKeyLookup_DevEx
          Left = 204
          Top = 44
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          WidthLlave = 40
        end
        object M_D_PER_CG: TZetaKeyLookup_DevEx
          Left = 204
          Top = 69
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 5
          TabStop = True
          WidthLlave = 40
        end
        object M_D_CG_ENT: TZetaKeyLookup_DevEx
          Left = 204
          Top = 94
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 7
          TabStop = True
          WidthLlave = 40
        end
        object M_D_PER_SG: TZetaKeyLookup_DevEx
          Left = 204
          Top = 119
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 9
          TabStop = True
          WidthLlave = 40
        end
        object M_D_SG_ENT: TZetaKeyLookup_DevEx
          Left = 204
          Top = 144
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 11
          TabStop = True
          WidthLlave = 40
        end
        object AU_OUT2EAT: TZetaKeyCombo
          Left = 147
          Top = 220
          Width = 160
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          Enabled = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 16
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object M_D_PRE_FUERA_JOR: TZetaKeyLookup_DevEx
          Left = 204
          Top = 169
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 13
          TabStop = True
          WidthLlave = 40
        end
        object D_PRE_FUERA_JOR: TZetaNumero
          Left = 147
          Top = 169
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 12
          Text = '0.00'
        end
        object D_PRE_DENTRO_JOR: TZetaNumero
          Left = 147
          Top = 191
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 14
          Text = '0.00'
        end
        object M_D_PRE_DENTRO_JOR: TZetaKeyLookup_DevEx
          Left = 204
          Top = 192
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 15
          TabStop = True
          WidthLlave = 40
        end
      end
      object FECHA_AJUSTE: TZetaFecha
        Left = 51
        Top = 5
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '01/Nov/00'
        Valor = 36831.000000000000000000
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 249
        Width = 433
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 433
        inherited Advertencia: TcxLabel
          Caption = 
            'Al presionar el bot'#243'n Aplicar se registrar'#225'n las autorizaciones ' +
            'a los empleados indicados con par'#225'metros marcados.'
          Style.IsFontAssigned = True
          Width = 365
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 24
        Top = 169
      end
      inherited sFiltroLBL: TLabel
        Left = 49
        Top = 198
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Top = 286
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 76
        Top = 166
      end
      inherited sFiltro: TcxMemo
        Left = 76
        Top = 197
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 76
        Top = 36
        Color = clWhite
        ParentColor = False
      end
      inherited bAjusteISR: TcxButton
        Left = 390
        Top = 198
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 360
  end
end
