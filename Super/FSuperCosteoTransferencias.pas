unit FSuperCosteoTransferencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseAsisConsulta_DevEx, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons, Db,
  ExtCtrls, ZBaseConsulta, Mask, ZetaFecha, ZetaKeyCombo,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, TressMorado2013, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, Menus, cxButtons, ZetaKeyLookup_DevEx, System.Actions,
  Vcl.ActnList, Vcl.ImgList;

type
  TSuperCosteoTransferencias = class(TBaseAsisConsulta_DevEx)
    pnFiltros: TPanel;
    Label1: TLabel;
    lblMaestro: TLabel;
    CCOrigen: TZetaKeyLookup_DevEx;
    CCDestino: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    TR_MOTIVO: TZetaKeyLookup_DevEx;
    rgTipo: TRadioGroup;
    Label3: TLabel;
    cbStatusTransferencia: TZetaKeyCombo;
    Label5: TLabel;
    //ZetaDBGrid: TZetaCXGrid;
    //ZetaDBGridDBTableView: TcxGridDBTableView;
    //ZetaDBGridLevel: TcxGridLevel;
    LLAVE: TcxGridDBColumn;
    AU_FECHA: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CC_ORIGEN: TcxGridDBColumn;
    CC_CODIGO: TcxGridDBColumn;
    TR_HORAS: TcxGridDBColumn;
    ASIS_HORAS: TcxGridDBColumn;
    TR_TIPO: TcxGridDBColumn;
    TR_MOTIVO_GRID: TcxGridDBColumn;
    TR_STATUS: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    US_APRUEBA: TcxGridDBColumn;
    TR_TEXTO: TcxGridDBColumn;
    btnFiltrar: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure ApplyMinWidth;
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SuperCosteoTransferencias: TSuperCosteoTransferencias;

implementation

uses dSuper, dSistema, dGlobal, ZetaCommonLists, ZetaCommonClasses, dTablas, dCliente,
     ZAccesosMgr, ZAccesosTress, ZGlobalTress,ZetaClientDataSet;

{$R *.DFM}

procedure TSuperCosteoTransferencias.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Transferencias_Supervisores;
     TipoValorActivo1 := stFecha;
     rgTipo.ItemIndex := 0;
     cbStatusTransferencia.ItemIndex := 0;
end;


procedure TSuperCosteoTransferencias.Connect;
 var
    oTransferencia: TZetaLookupDataset;
begin
     dmSistema.cdsUsuarios.Conectar;
     oTransferencia := dmTablas.GetDataSetTransferencia;
     oTransferencia.Conectar;
     dmTablas.cdsMotivoTransfer.Conectar;

     CCOrigen.LookupDataset := dmTablas.GetDataSetTransferencia;
     CCDestino.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;

     with dmSuper do
     begin
          cdsCosteoTransferencias.Conectar;
          DataSource.DataSet:= cdsCosteoTransferencias;
          //if RefrescaTransferencias then
             DoRefresh;
     end;
end;

procedure TSuperCosteoTransferencias.Refresh;
 var
    eStatus: integer;
begin
     if dmSuper.ParametrosTransferencias = NIL then
        dmSuper.ParametrosTransferencias := TZetaParams.Create(NIL);
     with dmSuper do
     begin
          with ParametrosTransferencias do
          begin
               AddDate( 'FechaIni', dmCliente.FechaSupervisor );
               AddDate( 'FechaFin', dmCliente.FechaSupervisor );
               AddString( 'CCOrigen', CCOrigen.Llave );
               AddString( 'CCDestino', CCDestino.Llave );
               AddString( 'Motivo', TR_MOTIVO.Llave );
               AddInteger( 'Tipo', rgTipo.ItemIndex - 1 );
               AddBoolean( 'SoloNegativos', FALSE );
               AddInteger( 'Globales', - 1 ); //Indica que son todas
               AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );

               AddBoolean( 'ModuloSupervisores', TRUE );
               with cbStatusTransferencia do
               begin
                    case eStatusTransCosteoConsulta(ItemIndex) of
                         stcPorAprobarMio, stcPorAprobarOtro: eStatus :=  Ord( stcPorAprobar );
                         stcAprobadaMio, stcAprobadaOtro: eStatus :=  Ord( stcAprobada );
                         stcCanceladaMio, stcCanceladaOtro: eStatus :=  Ord( stcCancelada )
                         else
                             eStatus := -1; //Mostrar todas las transferencias sin importar el status.
                    end;

                    AddBoolean( 'MisPendientes', eStatusTransCosteoConsulta(ItemIndex) in [ stcPorAprobarMio, stcAprobadaMio, stcCanceladaMio ] ); //Mostrar pendientes del Supervisor activo?
               end;

               AddInteger( 'Status', eStatus );

          end;
         ObtieneTransferencias( ParametrosTransferencias );
     end;
     DoBestFit;
end;

procedure TSuperCosteoTransferencias.Agregar;
begin
     dmSuper.cdsCosteoTransferencias.Agregar;
     DoBestFit;
end;

procedure TSuperCosteoTransferencias.Borrar;
begin
     dmSuper.cdsCosteoTransferencias.Borrar;
     DoBestFit;
end;

procedure TSuperCosteoTransferencias.Modificar;
begin
     dmSuper.cdsCosteoTransferencias.Modificar;
     DoBestFit;
end;

procedure TSuperCosteoTransferencias.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TSuperCosteoTransferencias.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
    if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TSuperCosteoTransferencias.FormShow(Sender: TObject);
begin
  inherited;
  //Para que ponga el MinWidth ideal dependiendo del texto presente en las columnas.
  ApplyMinWidth;
  //Desactiva la posibilidad de agrupar
  ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
  //Esconde la caja de agrupamiento
  ZetaDBGridDBTableView.OptionsView.GroupByBox := False;
   //Para que nunca muestre el filterbox inferior
  ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
  //Para que no aparezca el Custom Dialog
  ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
  //Para que ponga el Width ideal dependiendo del texto presente en las columnas.
  ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TSuperCosteoTransferencias.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TSuperCosteoTransferencias.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

end.
