unit FEvaluarSeleccion;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids,
  ZetaDBGrid, DB;

type
  TEvaluacionMultiple = class(TZetaDlgModal)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    dsEvaluacionMult: TDataSource;
    GridRenglones: TZetaDBGrid;
    btnBuscarPerfil: TSpeedButton;
    btnNivel: TBitBtn;
    procedure OKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridRenglonesColExit(Sender: TObject);
    procedure GridRenglonesDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure btnBuscarPerfilClick(Sender: TObject);
    procedure btnNivelClick(Sender: TObject);
  private
    procedure GrabarEvaluaciones;
    { Private declarations }
  public
    procedure CargarDatos;
    { Public declarations }
  end;

var
  EvaluacionMultiple: TEvaluacionMultiple;

implementation

uses
    DSuper, ZetaClientDataSet,DCliente,DCatalogos,FTressShell,ZetaBusqueda,ZetaDialogo,ZetaCommonClasses,ZetaCommonTools;

{$R *.dfm}

procedure TEvaluacionMultiple.CargarDatos;
begin
     with dmSuper.cdsEvaluacionSel do
     begin
          Init;
          Fields.Clear;  
          AddIntegerField('CB_CODIGO');
          AddStringField('CC_CODIGO',6);
          AddStringField('PRETTYNAME',150);
          AddStringField('CC_ELEMENT',100);
          AddStringField('NC_DESCRIP',50);
          AddIntegerField('NC_NIVEL');
          AddStringField('EC_COMENT',100);

          //CreateLookup(dmCatalogos.cdsCompNiveles,'NC_DESCRIP','CC_CODIGO;NC_NIVEL','CC_CODIGO;NC_NIVEL','NC_DESCRIP' );
          CreateTempDataset;
     end;
end;


procedure TEvaluacionMultiple.OKClick(Sender: TObject);
begin
     inherited;
     GrabarEvaluaciones;
     Close;
end;

procedure TEvaluacionMultiple.GrabarEvaluaciones;
var
   Empleado,Nivel:Integer;
   Competencia,Observa:string;
   oCursor: TCursor;
procedure GrabarEvaluacion;
begin
     //Asignar empleado Activo

     with dmSuper.cdsHisCompEvalua do
     begin
          Append;
          FieldByName('CB_CODIGO').AsInteger := Empleado;
          FieldByName('CC_CODIGO').AsString := Competencia;
          FieldByName('NC_NIVEL').AsInteger := Nivel;
          FieldByName('EC_COMENT').AsString := Observa;
          Post;
     end;
end;

begin
     with dmSuper.cdsEvaluacionSel do
     begin
          DisableControls;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             First;
             dmSuper.EvaluarMatriz := True;
             dmSuper.cdsHisCompEvalua.Conectar;
             While Not Eof do
             begin
                  Empleado:=     FieldByName('CB_CODIGO').AsInteger;
                  Competencia := FieldByName('CC_CODIGO').AsString;
                  Nivel:=        FieldByName('NC_NIVEL').AsInteger;
                  Observa :=     FieldByName('EC_COMENT').AsString;
                  GrabarEvaluacion;
                  Next;
             end;
             dmSuper.EvaluarMatriz := False;
             dmSuper.cdsHisCompEvalua.Enviar;
          finally
                 EnableControls;
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TEvaluacionMultiple.FormCreate(Sender: TObject);
begin
     inherited;
     CargarDatos;
end;

procedure TEvaluacionMultiple.CancelarClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TEvaluacionMultiple.FormShow(Sender: TObject);
begin
     inherited;
     with dmSuper do
     begin
          dsEvaluacionMult.DataSet := cdsEvaluacionSel;
     end;
end;


procedure TEvaluacionMultiple.GridRenglonesColExit(Sender: TObject);
begin
     inherited;
     with GridRenglones do
     begin
          if ( SelectedField <> nil ) then
          begin
               if ( SelectedField.FieldName = 'NC_DESCRIP' ) and
                  ( btnBuscarPerfil.Visible ) then
                  btnBuscarPerfil.Visible:= False;
          end;
     end;
end;

procedure TEvaluacionMultiple.GridRenglonesDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     inherited;
     if (( gdFocused in State ) and ( dsEvaluacionMult.State = dsEdit ) )then
     begin
          if ( Column.FieldName = 'NC_DESCRIP' ) then
          begin
               with btnBuscarPerfil do
               begin
                    Left := Rect.Left + GridRenglones.Left + Column.Width - Width;
                    Top := Rect.Top;
                    Visible := True;
               end;
          end;
     end
     else if  (dsEvaluacionMult.DataSet.State in [dsBrowse ] )then
     begin
          btnBuscarPerfil.Visible := False;
     end;

end;

procedure TEvaluacionMultiple.btnBuscarPerfilClick(Sender: TObject);
var
   sKey,sDescrip :string;
begin
     inherited;
     with dmCatalogos do
     begin
          with cdsCompNiveles do
          begin
               Filtered := False;
               Filter := Format('CC_CODIGO = ''%s''',[dmSuper.cdsEvaluacionSel.FieldByName('CC_CODIGO').AsString]);
               Filtered := True;
          end;
          ZetaBusqueda.ShowSearchForm(cdsCompNiveles,cdsCompNiveles.Filter,sKey,sDescrip,False,True);

          if StrLleno(sKey )then
          begin
               with dmSuper.cdsEvaluacionSel do
               begin
                    Edit;
                    FieldByName('NC_NIVEL').AsInteger := StrToInt(sKey);// cdsCompNiveles.FieldByName('NC_NIVEL').AsInteger;
                    FieldByName('NC_DESCRIP').AsString := sDescrip;
                    Post;
               end;
          end;
     end;
end;

procedure TEvaluacionMultiple.btnNivelClick(Sender: TObject);
begin
     inherited;
     dmSuper.cdsEvaluacionSel.Edit;
     GridRenglones.SetFocus;   
end;

end.
