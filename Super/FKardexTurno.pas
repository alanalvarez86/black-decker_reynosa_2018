unit FKardexTurno;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls,
  Buttons, FKardexBase_DevEx, ZetaSmartLists, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
   TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons,
  ZetaKeyLookup_DevEx, dxBarBuiltInMenu;

type
  TKardexTurno = class(TKardexBase_DevEx)
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    HorarioLbl: TLabel;
    bbMostrarCalendario_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    //procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendario_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
   procedure Connect;override;
  public
    { Public declarations }
  end;

var
  KardexTurno: TKardexTurno;

implementation

uses ZAccesosTress, ZetaCommonClasses, ZetaCommonLists, ZetaCommonTools,
     dCatalogos, dSistema, dSuper;

{$R *.DFM}

procedure TKardexTurno.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10155_Cambio_turno;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_TURNO.EditarSoloActivos := TRUE; //@DACP Se cambio el valor por codigo, ya que no respetaba lo que se le asigne en el DFM, Bug#15743
end;

procedure TKardexTurno.FormShow(Sender: TObject);
begin
  inherited;
  Datasource.AutoEdit := TRUE;
end;

procedure TKardexTurno.Connect;
begin
     dmCatalogos.cdsTurnos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmSuper do
     begin
          cdsHisKardex.Conectar;
          DataSource.DataSet := cdsHisKardex;
     end;
end;

{procedure TKardexTurno_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end; }

procedure TKardexTurno.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmSuper.cdsHisKardex do
               bbMostrarCalendario_DevEx.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;

procedure TKardexTurno.bbMostrarCalendario_DevExClick(
  Sender: TObject);
begin
  inherited;
  dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

end.






