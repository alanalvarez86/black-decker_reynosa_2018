unit FKardexAltaDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, ZBaseDlgModal_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, ImgList, cxButtons;

type
  TKardexAltaDlg = class(TZetaDlgModal_DevEx)
    TipoAlta: TRadioGroup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  KardexAltaDlg: TKardexAltaDlg;

implementation

{$R *.DFM}

procedure TKardexAltaDlg.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 00503;
end;

end.
