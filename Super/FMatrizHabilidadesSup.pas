unit FMatrizHabilidadesSup;



interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseConsulta, Grids, DBGrids, Db,
  ZetaDBGrid, ExtCtrls, StdCtrls, Buttons, ZetaKeyCombo, ZetaKeyLookup,
  ComCtrls, ZetaDBTextBox,ZetaCommonClasses,Variants, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxClasses,
  cxCustomData, cxStyles, cxEdit, cxCustomPivotGrid, cxDBPivotGrid,
  dxmdaset, cxPivotGridCustomDataSet, cxPivotGridSummaryDataSet,cxExportPivotGridLink,
  cxLocalization, Menus,Math, dxSkinsCore, 
  TressMorado2013, ImgList, cxButtons, ZetaKeyLookup_DevEx;

type
  TMatrizHabilidades = class(TBaseConsulta)
    Panel1: TPanel;
    PageControl1: TPageControl;
    cxStyleRepRojo: TcxStyleRepository;
    dbPivoteMatrizHabilidades: TcxDBPivotGrid;
    CB_CODIGO: TcxDBPivotGridField;
    PRETTYNAME: TcxDBPivotGridField;
    CC_ELEMENT: TcxDBPivotGridField;
    CP_ELEMENT: TcxDBPivotGridField;
    PC_PESO: TcxDBPivotGridField;
    PU_DESCRIP: TcxDBPivotGridField;
    Nivel_1: TcxDBPivotGridField;
    Nivel_2: TcxDBPivotGridField;
    Nivel_3: TcxDBPivotGridField;
    Nivel_4: TcxDBPivotGridField;
    Nivel_5: TcxDBPivotGridField;
    Nivel_6: TcxDBPivotGridField;
    Nivel_7: TcxDBPivotGridField;
    Nivel_8: TcxDBPivotGridField;
    Nivel_9: TcxDBPivotGridField;
    NC_DESCRIP: TcxDBPivotGridField;
    lookupGrupoCompentencias: TZetaKeyLookup_DevEx;
    Label2: TLabel;
    SaveDialog1: TSaveDialog;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxLocalizer1: TcxLocalizer;
    popRegistroCap: TPopupMenu;
    EvaluarCompetencias1: TMenuItem;
    btnEvaluarSeleccion_DevEx: TcxButton;
    cxImageList_MenuContextual: TcxImageList;
    RefrescarHabilidades: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure PC_PESOCalculateCustomSummary(Sender: TcxPivotGridField; ASummary: TcxPivotGridCrossCellSummary);
    procedure PC_PESO_COMPCalculateCustomSummary(Sender: TcxPivotGridField; ASummary: TcxPivotGridCrossCellSummary);
    procedure dbPivoteMatrizHabilidadesStylesGetContentStyle(Sender: TcxCustomPivotGrid; ACell: TcxPivotGridDataCellViewInfo; var AStyle: TcxStyle);
    procedure PuestosValidKey(Sender: TObject);
    procedure EvaluarCompetencias1Click(Sender: TObject);
    procedure popRegistroCapPopup(Sender: TObject);
    procedure btnEvaluarSeleccion_DevExClick(Sender: TObject);
    procedure RefrescarHabilidadesClick(Sender: TObject);
  private
    { Private declarations }
    FStyleRojo: TcxStyle;
    FStyleVerde: TcxStyle;
    function FiltrosLookup: Boolean;
    procedure SetFiltroNivel;
    procedure RegistrarCompetencias(Planear: Boolean);
    procedure RegistrarSeleccion(Empleado:Integer;Competencia,NombreEmpleado,DescripcionComp:string);
    procedure HabilitarEvaluacion;
    function PuedeEvaluar: Boolean;
  protected
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Exportar;override;
    procedure Imprimir;override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
  public
    { Public declarations }
  end;

var
  MatrizHabilidades: TMatrizHabilidades;

implementation

{$R *.DFM}

uses dSuper, DCatalogos,dSistema, ZetaCommonLists,ZetaCommonTools,ZAccesosTress,ZAccesosMgr,
     DGlobal,DCliente,ZetaDialogo,FEvaluarSeleccion_DevEx;

procedure TMatrizHabilidades.Connect;
begin
     with dmSuper do
     begin
          FiltroMatHabilidades.AddDate('FechaSup',dmCliente.FechaSupervisor );
          if FiltrosLookup then
             cdsMatrizHabilidades.Conectar;
          DataSource.DataSet:= cdsMatrizHabilidades;
     end;
     with dmCatalogos do
     begin
          cdsCompetencias.Conectar;
          cdsCatPerfiles.Conectar;
     end;

     FStyleRojo := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
     FStyleRojo.Color := TColor($8C92F7);

     FStyleVerde := cxStyleRepRojo.CreateItem(TcxStyle) as TcxStyle;
     FStyleVerde.Color := TColor($9FFFB8);
     HabilitarEvaluacion;
end;

procedure TMatrizHabilidades.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H_Sup_MATRIZ_HABLDS;
     IndexDerechos := D_SUP_MAT_HBLDS;
     with dmCatalogos do
     begin
          lookupGrupoCompentencias.LookupDataset := cdsCatPerfiles;
     end;
     SetFiltroNivel;
     cxLocalizer1.Active := true;
     cxLocalizer1.Locale := 3082;
end;


procedure TMatrizHabilidades.SetFiltroNivel();
var
   lPermitido:Boolean;
   i:Integer;
begin
     for i := 1 to 9 do
     begin
          lPermitido := ( i <= Global.NumNiveles );
          if lPermitido then
          begin
               case i of
                 1 : begin
                          Nivel_1.Visible := True;
                          Nivel_1.Hidden  := False;
                          Nivel_1.Caption := Global.NombreNivel(i);
                     end;
                 2 : begin
                          Nivel_2.Visible := True;
                          Nivel_2.Hidden  := False;
                          Nivel_2.Caption := Global.NombreNivel(i);
                     end;
                 3 : begin
                          Nivel_3.Visible := True;
                          Nivel_3.Hidden  := False;
                          Nivel_3.Caption := Global.NombreNivel(i);
                     end;
                 4 : begin
                          Nivel_4.Visible := True;
                          Nivel_4.Hidden  := False;
                          Nivel_4.Caption := Global.NombreNivel(i);
                     end;
                 5 : begin
                          Nivel_5.Visible := True;
                          Nivel_5.Hidden  := False;
                          Nivel_5.Caption := Global.NombreNivel(i);
                     end;
                 6 : begin
                          Nivel_6.Visible := True;
                          Nivel_6.Hidden  := False;
                          Nivel_6.Caption := Global.NombreNivel(i);
                     end;
                 7 : begin
                          Nivel_7.Visible := True;
                          Nivel_7.Hidden  := False;
                          Nivel_7.Caption := Global.NombreNivel(i);
                     end;
                 8 : begin
                          Nivel_8.Visible := True;
                          Nivel_8.Hidden  := False;
                          Nivel_8.Caption := Global.NombreNivel(i);
                     end;
                 9 : begin
                          Nivel_9.Visible := True;
                          Nivel_9.Hidden  := False;
                          Nivel_9.Caption := Global.NombreNivel(i);
                     end;
               else ;
               end;
          end;
     end;
end;

procedure TMatrizHabilidades.Agregar;
begin
     //dmRecursos.cdsHisCompeten.Agregar;
end;

procedure TMatrizHabilidades.Borrar;
begin
     //dmRecursos.cdsHisCompeten.Borrar;
end;

procedure TMatrizHabilidades.Modificar;
begin
     //dmRecursos.cdsHisCompeten.Modificar;
end;

procedure TMatrizHabilidades.Refresh;
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     if FiltrosLookup then
     begin
          try
             Screen.Cursor := crHourGlass;
             dmSuper.cdsMatrizHabilidades.Refrescar;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

function TMatrizHabilidades.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          sMensaje := 'No se Puede Borrar en la Matriz';
          Result := False;

     end;
end;

function TMatrizHabilidades.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          sMensaje := 'No Se puede Modificar';
          Result := False;
     end;
end;

function TMatrizHabilidades.PuedeAgregar(var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          sMensaje := 'No Se puede Agregar';
          Result := False;
     end;
end;

procedure TMatrizHabilidades.PC_PESOCalculateCustomSummary(Sender: TcxPivotGridField; ASummary: TcxPivotGridCrossCellSummary);

   function VarToDouble(const AValue: Variant): Double;
   begin
        Result := 0;
        if not VarIsNull(AValue) then
           Result := AValue;
   end;

begin
     ASummary.Custom := (ASummary.Sum / ASummary.Count)*100;
end;

procedure TMatrizHabilidades.PC_PESO_COMPCalculateCustomSummary(Sender: TcxPivotGridField; ASummary: TcxPivotGridCrossCellSummary);

   function VarToDouble(const AValue: Variant): Double;
   begin
        Result := 0;
        if not VarIsNull(AValue) then
           Result := AValue;
   end;

var
   ARow, AColumn: TcxPivotGridGroupItem;
   AGrandTotalValue, ASum: Double;

begin
     if PRETTYNAME.Area = faRow then
     begin
          ARow := ASummary.Owner.Row;
          AColumn := ASummary.Owner.Column;
     end
     else // rotated to 90 degrees
     begin
          ARow := ASummary.Owner.Column;
          AColumn := ASummary.Owner.Row;
     end;
     if AColumn.Parent = nil then
        ASummary.Custom := 100
     else
     begin
          AGrandTotalValue := VarToDouble(ARow.GetCellByCrossItem(
          AColumn.Parent).GetSummaryByField(Sender, stSum));
          ASum := VarToDouble(ASummary.Sum);
          if AGrandTotalValue <> 0 then
             ASummary.Custom := ASum / AGrandTotalValue  * 100
          else
              ASummary.Custom := 0;
     end;
end;

procedure TMatrizHabilidades.dbPivoteMatrizHabilidadesStylesGetContentStyle(Sender: TcxCustomPivotGrid;
          ACell: TcxPivotGridDataCellViewInfo; var AStyle: TcxStyle);
begin
     inherited;
     if not ACell.IsTotal OR not ACell.IsGrandTotal then
     begin
          if ACell.Value = 0.00 then
             AStyle := FStyleRojo
          else if ACell.Value >= 1 then
              AStyle := FStyleVerde;
     end
end;

procedure TMatrizHabilidades.PuestosValidKey(Sender: TObject);
begin
     inherited;
     with dmSuper.FiltroMatHabilidades do
     begin
          AddString('Grupo',lookupGrupoCompentencias.Llave);
     end;
     //Refresh;   Ya no se hace Refresh, el supervisor debe presionar el bot�n de Refrescar
     //HabilitarEvaluacion;
end;

procedure TMatrizHabilidades.Exportar;
begin
     //inherited;
     if SaveDialog1.Execute then
     begin
          cxExportPivotGridToExcel(SaveDialog1.FileName, dbPivoteMatrizHabilidades);
     end;
end;

procedure TMatrizHabilidades.RegistrarCompetencias(Planear:Boolean);
var
   I,X,Y,RightX,BottomY,Empleado,iPos: Integer;
   ASelection: TRect;
   Competencia,sVar:string;

   function GetEmpleado (Renglon:string ):Integer;
   begin
        iPos :=  Pos(':',Renglon);
        sVar :=    Copy( Renglon, 0, ( iPos - 2 ));
        Result := StrToIntDef(sVar,0 );
   end;

   function GetCompetencia(Nombre:string):string;
   begin
        Result := '???';
        with dmCatalogos.cdsCompetencias do
        begin
             if Locate('CC_ELEMENT',Nombre,[]) then
                Result := FieldByName('CC_CODIGO').AsString;
        end;
   end;

begin
     with dbPivoteMatrizHabilidades.ViewData do
     begin
          for I := 0 to Selection.Count - 1 do
          begin
               ASelection := Selection.Regions[I];
               RightX := Min(ASelection.Right, ColumnCount - 1);
               BottomY := Min(ASelection.Bottom, RowCount - 1);
               for X := ASelection.Left to RightX do
               begin
                    for Y := ASelection.Top to BottomY do
                    begin
                         if not (Rows[Y].IsTotalItem or Columns[X].IsTotalItem) then
                         begin
                              if Cells[Y,X].Sum = 0 then
                              begin
                                   Empleado := GetEmpleado(Rows[Y].Value);
                                   Competencia := GetCompetencia(Columns[X].Value);
                                   if ( Empleado > 0 ) and (Competencia <> '???') then
                                   begin
                                        RegistrarSeleccion(Empleado,Competencia,Rows[Y].Value,Columns[X].Value);
                                   end;
                              end;

                         end;
                    end;
               end;
          end;
     end;
end;

procedure TMatrizHabilidades.RegistrarSeleccion(Empleado:Integer;Competencia,NombreEmpleado,DescripcionComp:string);
var
   Descripcion:string;
begin
     dmSuper.AgregandoMatriz := True;
     with dmSuper.cdsEvaluacionSel do
     begin
          Open;
          Append;
          FieldByName('CB_CODIGO').AsInteger := Empleado;
          FieldByName('CC_CODIGO').AsString := Competencia;
          FieldByName('PRETTYNAME').AsString := NombreEmpleado;
          FieldByName('CC_ELEMENT').AsString := DescripcionComp;
          FieldByName('NC_NIVEL').AsInteger := dmCatalogos.GetNivelRequeridoCompetencia(Competencia,Descripcion);
          FieldByName('NC_DESCRIP').AsString := Descripcion;
          Post;
     end;
     dmSuper.AgregandoMatriz := False;
end;

procedure TMatrizHabilidades.EvaluarCompetencias1Click(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.zConfirm('Matriz de Habilidades','� Desea evaluar a los empleados seleccionados ?',0,mbOK)then
     begin
          if EvaluacionMultiple_DevEx = nil then
            EvaluacionMultiple_DevEx:= TEvaluacionMultiple_DevEx.Create(Self)
          else
          begin
               with dmSuper.cdsEvaluacionSel do
               begin
                    if not Active then
                    begin
                         Open;
                    end;
                    EmptyDataSet;
               end;
          end;
          RegistrarCompetencias(True);

          if not dmSuper.cdsEvaluacionSel.IsEmpty then
          begin
               EvaluacionMultiple_DevEx.ShowModal;
               if ( EvaluacionMultiple_DevEx.ModalResult = mrOk )then
                  Refresh;
          end
          else
              ZetaDialogo.ZInformation('Matriz de Habilidades','La Evaluaci�n es sobre los registros de empleados y competencias. No sobre agrupaciones',0);
     end;
end;

procedure TMatrizHabilidades.popRegistroCapPopup(Sender: TObject);
begin
     inherited;
     EvaluarCompetencias1.Enabled := StrLleno(lookupGrupoCompentencias.Llave) and PuedeEvaluar;
end;

procedure TMatrizHabilidades.HabilitarEvaluacion;
begin
     btnEvaluarSeleccion_DevEx.Enabled := StrLleno(lookupGrupoCompentencias.Llave) and PuedeEvaluar;
end;

function TMatrizHabilidades.PuedeEvaluar:Boolean;
begin
     Result := ZAccesosMgr.CheckDerecho( D_SUP_EVAL_COMPETEN , K_DERECHO_ALTA );
end;

procedure TMatrizHabilidades.Imprimir;
begin
     inherited;
     Exportar;
end;

procedure TMatrizHabilidades.btnEvaluarSeleccion_DevExClick(Sender: TObject);
begin
     inherited;
     if dbPivoteMatrizHabilidades.ViewData.Selection.Count > 0 then
     begin
          EvaluarCompetencias1Click(Self);
     end
     else
         ZetaDialogo.ZInformation(Self.Caption,'Seleccionar al menos una celda de la Matriz de Habilidades',0);
end;

function TMatrizHabilidades.FiltrosLookup: Boolean;
begin
     Result := strLleno( lookupGrupoCompentencias.Llave );
end;

procedure TMatrizHabilidades.RefrescarHabilidadesClick(Sender: TObject);
begin
     inherited;
     if FiltrosLookup then
     begin
          Refresh;
          HabilitarEvaluacion;
     end
     else
         ZetaDialogo.ZInformation( Self.Caption, 'Debe indicar grupo de competencias de la matriz de habilidades', 0 );
end;

end.
