unit FCedulas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ExtCtrls, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}
  {ZBaseConsulta,}
  ZBaseGridLectura_DevEx,
  ZetaCommonLists, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons;

type
  TCedulas = class(TBaseGridLectura_DevEx)
    PanelBtnCedulas: TPanel;
    CE_FOLIO: TcxGridDBColumn;
    CE_HORA: TcxGridDBColumn;
    CE_TIPO: TcxGridDBColumn;
    WO_NUMBER: TcxGridDBColumn;
    AR_CODIGO: TcxGridDBColumn;
    CE_PIEZAS: TcxGridDBColumn;
    OP_NUMBER: TcxGridDBColumn;
    CE_AREA: TcxGridDBColumn;
    CE_COMENTA: TcxGridDBColumn;
    BtnOperaciones_DevEx: TcxButton;
    BtnTMuerto_DevEx: TcxButton;
    BtnEspeciales_DevEx: TcxButton;
    procedure FormCreate(Sender: TObject);
    //procedure BtnOperacionesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnOperaciones_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FTipoCedula : eTipoCedula;
    function GetTipoCedulaActivo: eTipoCedula;
    procedure AsignaColumnas;
  protected
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect;override;
    procedure Agregar;override;
    procedure Borrar;override;
    procedure Modificar;override;
    procedure Refresh;override;
    function ValoresGrid: Variant;override;
  public
    { Public declarations }
  end;

var
  Cedulas: TCedulas;

implementation

uses DLabor,
     DGlobal,
     DCliente,
     ZetaCommonClasses,
     ZGlobalTress,
     ZetaLaborTools,
     ZetaDialogo;

{$R *.DFM}

procedure TCedulas.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 9504;
     TipoValorActivo1 := stFecha;
     BtnOperaciones_DevEx.Tag := Ord( tcOperacion );
     BtnTMuerto_DevEx.Tag := Ord( tcTMuerto );
     BtnEspeciales_DevEx.Tag := Ord( tcEspeciales );
end;

procedure TCedulas.FormShow(Sender: TObject);
begin
     AsignaColumnas;
     ApplyMinWidth;
     inherited;
     FTipoCedula := tcOperacion;
end;

procedure TCedulas.Connect;
begin
     with dmLabor do
     begin
          cdsOpera.Conectar;
          cdsTiempoMuerto.Conectar;
          cdsCedulas.Conectar;
          DataSource.DataSet := cdsCedulas;
     end;
end;

procedure TCedulas.Refresh;
begin
     dmLabor.cdsCedulas.Refrescar;
     DoBestFit;
end;

function TCedulas.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
        Result := ZetaLaborTools.ChecaTipoCedula( FTipoCedula, ukInsert, IndexDerechos, sMensaje );
end;

procedure TCedulas.Agregar;
begin
     dmLabor.cdsCedulas.Agregar;
     DoBestFit;
end;

function TCedulas.PuedeModificar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
        Result := ChecaTipoCedula( GetTipoCedulaActivo, ukModify, IndexDerechos, sMensaje );
end;

procedure TCedulas.Modificar;
begin
     dmLabor.cdsCedulas.Modificar;
     DoBestFit;
end;

function TCedulas.PuedeBorrar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
        Result := ChecaTipoCedula( GetTipoCedulaActivo, ukDelete, IndexDerechos, sMensaje );
end;

procedure TCedulas.Borrar;
begin
     dmLabor.cdsCedulas.Borrar;
     DoBestFit;
end;

function TCedulas.GetTipoCedulaActivo: eTipoCedula;
begin
     if NoHayDatos then
        Result := tcOperacion       // Valor Default - No debe llegar a cumplirse esta condici�n por que no puede llegar aqu� si NoHayDatos
     else
        Result := eTipoCedula( DataSource.DataSet.FieldByName( 'CE_TIPO' ).AsInteger );
end;

procedure TCedulas.AsignaColumnas;
begin
     {with Grid do
     begin
          AsignaGlobalColumn( K_GLOBAL_LABOR_ORDEN, Columns[ GetFieldColumnIndex( 'WO_NUMBER' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_PARTE, Columns[ GetFieldColumnIndex( 'AR_CODIGO' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_PIEZAS, Columns[ GetFieldColumnIndex( 'CE_PIEZAS' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_OPERACION, Columns[ GetFieldColumnIndex( 'OP_NUMBER' ) ] );
          AsignaGlobalColumn( K_GLOBAL_LABOR_AREA, Columns[ GetFieldColumnIndex( 'CE_AREA' ) ] );
     end;}
      with ZetaDBGridDBTableView do
     begin
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_ORDEN, GetColumnByFieldName('WO_NUMBER'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PARTE, GetColumnByFieldName('AR_CODIGO'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_PIEZAS, GetColumnByFieldName('CE_PIEZAS'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_OPERACION, GetColumnByFieldName('OP_NUMBER'));
           AsignaGlobalColumn_DevEx( K_GLOBAL_LABOR_AREA, GetColumnByFieldName('CE_AREA'));
     end;
end;

{procedure TCedulas.BtnOperacionesClick(Sender: TObject);
var
   sMensaje : String;
begin
     inherited;
     FTipoCedula := eTipoCedula( TBitBtn( Sender ).Tag );
     try
        if PuedeAgregar( sMensaje ) then
           dmLabor.ShowGridLabor( FTipoCedula )
        else
           zInformation( Caption, sMensaje, 0 );
     finally
        FTipoCedula := tcOperacion;
     end;
end; }

function TCedulas.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stFecha ), VACIO, stFecha, stNinguno ] );
end;

procedure TCedulas.BtnOperaciones_DevExClick(Sender: TObject);
var
   sMensaje : String;
begin
   inherited;
     FTipoCedula := eTipoCedula( TcxButton( Sender ).Tag );
     try
        if PuedeAgregar( sMensaje ) then
           dmLabor.ShowGridLabor( FTipoCedula )
        else
           zInformation( Caption, sMensaje, 0 );
     finally
        FTipoCedula := tcOperacion;
     end;

end;

end.
