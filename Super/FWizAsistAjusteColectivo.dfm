inherited WizAsistAjusteColectivo: TWizAsistAjusteColectivo
  Left = 218
  Top = 153
  Caption = 'Autorizaciones Colectivas de Asistencia'
  ClientHeight = 396
  ClientWidth = 455
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 360
    Width = 455
    BeforeMove = WizardBeforeMove
    inherited Salir: TZetaWizardButton
      Left = 362
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 279
    end
  end
  inherited PageControl: TPageControl
    Width = 455
    Height = 360
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object Label1: TLabel
        Left = 11
        Top = 11
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = '&Fecha:'
        FocusControl = FECHA_AJUSTE
      end
      object DefaultsGB: TGroupBox
        Left = 0
        Top = 32
        Width = 441
        Height = 313
        Caption = ' Valores Default '
        TabOrder = 1
        object Label3: TLabel
          Left = 78
          Top = 22
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Horas &Extras:'
          FocusControl = D_EXTRAS
        end
        object Label4: TLabel
          Left = 39
          Top = 47
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = '&Descanso Trabajado:'
          FocusControl = D_DES_TRA
        end
        object Label5: TLabel
          Left = 78
          Top = 72
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso C/&G:'
          FocusControl = D_PER_CG
        end
        object Label6: TLabel
          Left = 38
          Top = 97
          Width = 103
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso C/G E&ntrada:'
          FocusControl = D_CG_ENT
        end
        object Label7: TLabel
          Left = 78
          Top = 122
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = '&Permiso S/G:'
          FocusControl = D_PER_SG
        end
        object Label8: TLabel
          Left = 38
          Top = 147
          Width = 103
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per&miso S/G Entrada:'
          FocusControl = D_SG_ENT
        end
        object lblTomoComida: TLabel
          Left = 73
          Top = 224
          Width = 68
          Height = 13
          Caption = 'Tom'#243' Comida:'
        end
        object Label2: TLabel
          Left = 10
          Top = 173
          Width = 131
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prepagadas Fuera &Jornada:'
        end
        object Label9: TLabel
          Left = 5
          Top = 195
          Width = 136
          Height = 13
          Alignment = taRightJustify
          Caption = 'Prepagadas &Dentro Jornada:'
        end
        object D_EXTRAS: TZetaNumero
          Left = 147
          Top = 19
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 0
          Text = '0.00'
        end
        object D_DES_TRA: TZetaNumero
          Left = 147
          Top = 44
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 2
          Text = '0.00'
        end
        object D_PER_CG: TZetaNumero
          Left = 147
          Top = 69
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 4
          Text = '0.00'
        end
        object D_CG_ENT: TZetaNumero
          Left = 147
          Top = 94
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 6
          Text = '0.00'
        end
        object D_PER_SG: TZetaNumero
          Left = 147
          Top = 119
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 8
          Text = '0.00'
        end
        object D_SG_ENT: TZetaNumero
          Left = 147
          Top = 144
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 10
          Text = '0.00'
        end
        object D_ANTERIOR: TRadioGroup
          Left = 64
          Top = 250
          Width = 320
          Height = 49
          Caption = ' En caso de Existir Autorizaci'#243'n '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Dejar Anterior'
            '     Sumar'
            '    Sustituir')
          TabOrder = 17
        end
        object M_D_EXTRAS: TZetaKeyLookup
          Left = 204
          Top = 19
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 1
          TabStop = True
          WidthLlave = 40
        end
        object M_D_DES_TRA: TZetaKeyLookup
          Left = 204
          Top = 44
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 3
          TabStop = True
          WidthLlave = 40
        end
        object M_D_PER_CG: TZetaKeyLookup
          Left = 204
          Top = 69
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 5
          TabStop = True
          WidthLlave = 40
        end
        object M_D_CG_ENT: TZetaKeyLookup
          Left = 204
          Top = 94
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 7
          TabStop = True
          WidthLlave = 40
        end
        object M_D_PER_SG: TZetaKeyLookup
          Left = 204
          Top = 119
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 9
          TabStop = True
          WidthLlave = 40
        end
        object M_D_SG_ENT: TZetaKeyLookup
          Left = 204
          Top = 144
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 11
          TabStop = True
          WidthLlave = 40
        end
        object AU_OUT2EAT: TZetaKeyCombo
          Left = 147
          Top = 220
          Width = 160
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          Enabled = False
          ItemHeight = 13
          TabOrder = 16
          ListaFija = lfNinguna
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
        end
        object M_D_PRE_FUERA_JOR: TZetaKeyLookup
          Left = 204
          Top = 169
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 13
          TabStop = True
          WidthLlave = 40
        end
        object D_PRE_FUERA_JOR: TZetaNumero
          Left = 147
          Top = 169
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 12
          Text = '0.00'
        end
        object D_PRE_DENTRO_JOR: TZetaNumero
          Left = 147
          Top = 191
          Width = 50
          Height = 21
          Mascara = mnHoras
          TabOrder = 14
          Text = '0.00'
        end
        object M_D_PRE_DENTRO_JOR: TZetaKeyLookup
          Left = 204
          Top = 192
          Width = 230
          Height = 21
          LookupDataset = dmTablas.cdsMotAuto
          TabOrder = 15
          TabStop = True
          WidthLlave = 40
        end
      end
      object FECHA_AJUSTE: TZetaFecha
        Left = 51
        Top = 7
        Width = 115
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '01/Nov/00'
        Valor = 36831.000000000000000000
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 447
        Lines.Strings = (
          'Las autorizaciones y ajustes de asistencia capturadas ser'#225'n '
          'registradas.'
          ''
          'Presione el bot'#243'n '#39'Ejecutar'#39' para iniciar el proceso.')
      end
      inherited ProgressPanel: TPanel
        Top = 257
        Width = 447
      end
      object Animate: TAnimate
        Left = 0
        Top = 169
        Width = 447
        Height = 88
        Align = alClient
        Active = True
        AutoSize = False
        CommonAVI = aviCopyFiles
        StopFrame = 31
        Visible = False
      end
      object PanelMensaje: TPanel
        Left = 0
        Top = 324
        Width = 447
        Height = 26
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Grabando Ajustes de Asistencia ...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        Visible = False
      end
    end
  end
end
