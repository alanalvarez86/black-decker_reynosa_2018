inherited Permisos: TPermisos
  Left = 312
  Top = 196
  Caption = 'Permisos'
  ClientHeight = 288
  ClientWidth = 484
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 484
    inherited ValorActivo2: TPanel
      Width = 225
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 484
    Height = 269
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopupMenu = nil
      DataController.DataSource = DataSource
      object PM_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha Inicio'
        DataBinding.FieldName = 'PM_FEC_INI'
        MinWidth = 95
      end
      object PM_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'PM_DIAS'
        MinWidth = 70
      end
      object PM_FEC_FIN: TcxGridDBColumn
        Caption = 'Regreso'
        DataBinding.FieldName = 'PM_FEC_FIN'
        MinWidth = 90
      end
      object PM_CLASIFI: TcxGridDBColumn
        Caption = 'Clasificaci'#243'n'
        DataBinding.FieldName = 'PM_CLASIFI'
        MinWidth = 95
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 70
      end
      object PM_NUMERO: TcxGridDBColumn
        Caption = 'Referencia'
        DataBinding.FieldName = 'PM_NUMERO'
        MinWidth = 85
      end
      object PM_COMENTA: TcxGridDBColumn
        Caption = 'Comentario'
        DataBinding.FieldName = 'PM_COMENTA'
        MinWidth = 90
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 424
    Top = 0
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
