inherited KardexDiario: TKardexDiario
  Left = 403
  Top = 579
  Caption = 'Kardex Diario'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = DataSource
      object KA_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'KA_FECHA'
        MinWidth = 75
      end
      object CB_AREA: TcxGridDBColumn
        Caption = '%s'
        DataBinding.FieldName = 'CB_AREA'
        MinWidth = 100
      end
      object TB_ELEMENT: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'TB_ELEMENT'
        MinWidth = 100
      end
      object KA_HORA: TcxGridDBColumn
        Caption = 'Hora Inicio'
        DataBinding.FieldName = 'KA_HORA'
        MinWidth = 110
      end
      object KA_HOR_FIN: TcxGridDBColumn
        Caption = 'Hora Fin'
        DataBinding.FieldName = 'KA_HOR_FIN'
        MinWidth = 110
      end
      object KA_TIEMPO: TcxGridDBColumn
        Caption = 'Tiempo'
        DataBinding.FieldName = 'KA_TIEMPO'
        MinWidth = 90
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end
