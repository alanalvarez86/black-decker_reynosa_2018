unit FEditVacacion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FEditHisVacacion, Db, ZetaFecha, ZetaSmartLists, StdCtrls, DBCtrls,
  ExtCtrls, ZetaKeyLookup, ZetaKeyCombo, ZetaNumero, Mask, ComCtrls,
  Buttons, ZetaDBTextBox;

type
  TEditVacacion = class(TEditHisVacacion)
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaCampos;
  protected
    function PuedeModificar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditVacacion: TEditVacacion;

implementation

uses dSuper,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZAccesosTress;

{$R *.DFM}

{ TEditVacacion }

procedure TEditVacacion.FormCreate(Sender: TObject);
begin
     inherited;
     //HelpContext:= H11510_vacaciones; - Se usa la misma que en tress
     IndexDerechos := D_SUPER_VACACIONES;
     TabGenerales.TabVisible := FALSE;
end;

procedure TEditVacacion.Connect;
begin
     inherited;
     // Si est� deshabilitado el periodo que no presente ??? en descripci�n cuando no tenga capturado el periodo
     with VA_NOMNUME do
     begin
          if ( not Enabled ) and ( Valor = 0 ) then
             SetLlaveDescripcion( VACIO, VACIO );
     end;
end;

function TEditVacacion.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje ) and
               dmSuper.ValidateInicioVacacion( DataSource.DataSet.FieldByName( 'VA_FEC_INI' ).AsDateTime,
                                               ukModify, sMensaje );
end;

function TEditVacacion.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje ) and
               dmSuper.ValidateInicioVacacion( DataSource.DataSet.FieldByName( 'VA_FEC_INI' ).AsDateTime,
                                               ukDelete, sMensaje );
end;

procedure TEditVacacion.HabilitaCampos;
var
   lEnabled, lDerechoPago : Boolean;
   i : Integer;
begin
     lDerechoPago := self.CheckDerechos( K_DERECHO_SIST_KARDEX );     // Bit #5 de IndexDerechos 'Registrar Vacaciones Pagadas' ( D_SUPER_VACACIONES )
     lEnabled := dmSuper.ValidaInicioVacacion( DataSource.DataSet.FieldByName( 'VA_FEC_INI' ).AsDateTime );

     LVA_VAC_DEL.Enabled := lEnabled;
     VA_FEC_INI.Enabled := lEnabled;
     LVA_VAC_AL.Enabled := lEnabled;
     VA_FEC_FIN.Enabled := lEnabled;
     BtnRegreso.Enabled := lEnabled;
     LblGozados.Enabled := lEnabled;
     VA_GOZO.Enabled := lEnabled;
     BtnRecalculo.Enabled := lEnabled;
     LblPeriodo.Enabled := lEnabled;
     VA_PERIODO.Enabled := lEnabled;
     LblObservaciones.Enabled := lEnabled;
     VA_COMENTA.Enabled := lEnabled;
     LblPago.Enabled := lEnabled and lDerechoPago;
     VA_PAGO.Enabled := lEnabled and lDerechoPago;
     LblOtros.Enabled := lEnabled and lDerechoPago;
     VA_OTROS.Enabled := lEnabled and lDerechoPago;
     L_VA_P_PRIMA.Enabled := lEnabled and lDerechoPago;
     VA_P_PRIMA.Enabled := lEnabled and lDerechoPago;
     with GBNominaPago do
     begin
          Enabled := lEnabled and lDerechoPago;
          for i := 0 to ( ControlCount - 1 ) do
          begin
               Controls[ i ].Enabled := lEnabled and lDerechoPago;
          end;
     end;
     if lEnabled then
        SetControls;
end;

procedure TEditVacacion.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field = nil ) then       // Solo cuando cambie de Registro o conecte el primero
        HabilitaCampos;
end;

end.
