object MaquinaDetalle: TMaquinaDetalle
  Left = 329
  Top = 203
  BorderStyle = bsDialog
  Caption = 'M'#225'quina'
  ClientHeight = 270
  ClientWidth = 487
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 72
    Width = 487
    Height = 198
    Align = alBottom
    Caption = ' Certificaciones '
    TabOrder = 0
    object ZetaDBGrid: TZetaCXGrid
      Left = 2
      Top = 15
      Width = 483
      Height = 181
      Hint = ''
      Align = alClient
      TabOrder = 0
      object ZetaDBGridDBTableView: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        FilterBox.CustomizeDialog = False
        DataController.DataSource = dsCertificaciones
        DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        DataController.OnSortingChanged = ZetaDBGridDBTableViewDataControllerSortingChanged
        Filtering.ColumnFilteredItemsList = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OnColumnHeaderClick = ZetaDBGridDBTableViewColumnHeaderClick
        object CI_CODIGO: TcxGridDBColumn
          Caption = 'C'#243'digo'
          DataBinding.FieldName = 'CI_CODIGO'
          MinWidth = 97
        end
        object CI_DESCRIP: TcxGridDBColumn
          Caption = 'Descripci'#243'n'
          DataBinding.FieldName = 'CI_DESCRIP'
          MinWidth = 312
          Styles.Content = cxStyle2
        end
      end
      object ZetaDBGridLevel: TcxGridLevel
        GridView = ZetaDBGridDBTableView
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 487
    Height = 72
    Align = alClient
    TabOrder = 1
    object DBCodigoLBL: TLabel
      Left = 111
      Top = 14
      Width = 36
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo:'
    end
    object DBDescripcionLBL: TLabel
      Left = 89
      Top = 36
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descripci'#243'n:'
    end
    object MQ_NOMBRE: TDBEdit
      Left = 150
      Top = 33
      Width = 246
      Height = 21
      DataField = 'MQ_NOMBRE'
      DataSource = dsMaquina
      ReadOnly = True
      TabOrder = 1
    end
    object MQ_CODIGO: TZetaDBEdit
      Left = 150
      Top = 10
      Width = 65
      Height = 21
      CharCase = ecUpperCase
      ReadOnly = True
      TabOrder = 0
      ConfirmEdit = True
      DataField = 'MQ_CODIGO'
      DataSource = dsMaquina
    end
    object IMAGEN: TImageEnView
      Left = 0
      Top = 10
      Width = 83
      Height = 56
      Cursor = crDefault
      ParentCustomHint = False
      Background = clBtnFace
      Ctl3D = False
      ParentCtl3D = False
      LegacyBitmap = False
      AutoFit = True
      AutoStretch = True
      EnableInteractionHints = True
      PlayLoop = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 2
      TabStop = True
    end
  end
  object dsCertificaciones: TDataSource
    Left = 415
    Top = 137
  end
  object dsMaquina: TDataSource
    Left = 271
    Top = 65529
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = 16377581
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = clWhite
    end
  end
end
