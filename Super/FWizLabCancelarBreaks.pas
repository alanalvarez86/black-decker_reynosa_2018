unit FWizLabCancelarBreaks;

interface

uses                                
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
   ZetaHora, Mask, ZetaFecha, ComCtrls,
  ZetaEdit, StdCtrls, Buttons, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, cxContainer, cxEdit, ImgList,
  ZetaKeyLookup_DevEx, cxTextEdit, cxMemo, cxImage, cxButtons,
  ZCXBaseWizardFiltro, ZetaCXWizard, cxRadioGroup,
  dxGDIPlusClasses, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl;

type
  TWizLabCancelarBreaks = class(TBaseCXWizardFiltro)
    Fechalbl: TLabel;
    ZFecha: TZetaFecha;
    Horalbl: TLabel;
    ZHora: TZetaHora;
    AreaKL: TZetaKeyLookup_DevEx;
    Arealbl: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
    procedure SetAnimacion( const lEnabled: Boolean );
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaListaVerificacion; override;
    procedure CargaParametros; override;
    procedure TerminarWizard; override;
  public
    { Public declarations }
  end;

var
  WizLabCancelarBreaks: TWizLabCancelarBreaks;

implementation

uses DCliente, DLabor, DGlobal, FTressShell, ZBaseSelectGrid_DevEx, FLabCancelarBreaksGridSelect, ZetaTipoEntidad,
     ZGlobalTress, ZetaLaborTools, ZetaCommonTools, ZetaDialogo, ZetaCommonClasses;

{$R *.DFM}

{ TWizLabCancelarBreaks }

procedure TWizLabCancelarBreaks.FormCreate(Sender: TObject);
begin
     inherited;
     AreaKL.Filtro := MIS_AREAS;
     ParametrosControl := ZFecha;
     ZFecha.Valor := dmCliente.FechaDefault;
     ZHora.Valor := dmLabor.GetHoraDefault; //FormatDateTime( 'hhmm', Now );
     HelpContext :=  H9509_Cancelacion_de_Breaks_Manuales;
     AreaKL.Lookupdataset := dmLabor.cdsArea;
end;

procedure TWizLabCancelarBreaks.FormShow(Sender: TObject);
begin
     inherited;
     dmLabor.cdsArea.Conectar;
     Arealbl.Caption := Global.GetGlobalString( K_GLOBAL_LABOR_AREAS ) + ':';
end;

procedure TWizLabCancelarBreaks.CargaParametros;
begin

     inherited CargaParametros;

     with ParameterList do
     begin
          AddDate( 'Fecha', zFecha.Valor );
          AddString( 'Hora', zHora.Valor );
          AddString( 'Area', AreaKL.Llave );
     end;
      with Descripciones do
     begin
          AddDate( 'Fecha', zFecha.Valor );
          AddString( 'Hora Final', zHora.Valor );
          AddString( 'Area', GetDescripLlave( AreaKL ) );
     end;

end;

procedure TWizLabCancelarBreaks.CargaListaVerificacion;
begin
     dmLabor.CancelarBreaksGetLista( ParameterList );
end;

function TWizLabCancelarBreaks.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmLabor.cdsDataset, TLabCancelarBreaksGridSelect );
end;

function TWizLabCancelarBreaks.EjecutarWizard: Boolean;
begin
     Result := dmLabor.CancelarBreaks( ParameterList, Verificacion, SetAnimacion );
     if Result then
        TressShell.SetDataChange( [ enWorks ] );
end;

procedure TWizLabCancelarBreaks.TerminarWizard;
begin
     Close;
end;

procedure TWizLabCancelarBreaks.SetAnimacion( const lEnabled: Boolean );
begin

     Application.ProcessMessages;
end;

procedure TWizLabCancelarBreaks.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   dFechaCorte: Tdate;
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               with Global do
               begin
                    if ZetaCommonTools.strVacio( zHora.Valor ) then
                       CanMove := Error( 'ˇ Debe Especificarse La Hora De Los Breaks !', zHora )
                    else if GetGlobalBooleano( K_GLOBAL_LABOR_VALIDA_FECHACORTE ) then
                         begin
                              dFechaCorte := GetGlobalDate( K_GLOBAL_LABOR_FECHACORTE );
                              if not ( zFecha.Valor > dFechaCorte ) then
                                 CanMove := Error( Format( 'ˇ No se Permite Borrar Información de Labor !' + CR_LF +
                                                           'La Fecha de Corte es : %s', [ FechaCorta( dFechaCorte ) ] ),
                                                           zFecha )
                         end;
               end;
          end;
     end;
end;

end.



