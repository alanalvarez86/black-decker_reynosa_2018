inherited KardexVarios: TKardexVarios
  Left = 815
  Top = 153
  Caption = 'Cambios M'#250'ltiples'
  ClientHeight = 414
  ClientWidth = 417
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 378
    Width = 417
    inherited OK_DevEx: TcxButton
      Left = 252
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 331
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 417
    inherited ValorActivo2: TPanel
      Width = 91
      inherited textoValorActivo2: TLabel
        Width = 85
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 50
    Width = 417
    Height = 29
    Align = alTop
    TabOrder = 3
    object Label7: TLabel
      Left = 26
      Top = 6
      Width = 33
      Height = 13
      Caption = 'Fecha:'
    end
    object ZFecha: TZetaFecha
      Left = 63
      Top = 3
      Width = 116
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '01/May/98'
      UseEnterKey = True
      Valor = 35916.000000000000000000
    end
  end
  object PageControl: TcxPageControl [4]
    Left = 0
    Top = 79
    Width = 417
    Height = 299
    Align = alClient
    TabOrder = 4
    Properties.ActivePage = TabCambios
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 297
    ClientRectLeft = 2
    ClientRectRight = 415
    ClientRectTop = 28
    object TabCambios: TcxTabSheet
      Caption = '&Cambios'
      object LPuesto: TLabel
        Left = 59
        Top = 10
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object LClasificacion: TLabel
        Left = 33
        Top = 32
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object LTurno: TLabel
        Left = 64
        Top = 54
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 6
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        OnExit = CB_PUESTOExit
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 28
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 50
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
    end
    object TabArea: TcxTabSheet
      Caption = '&Area'
      object LNivel7: TLabel
        Left = 5
        Top = 137
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 7'
      end
      object LNivel8: TLabel
        Left = 5
        Top = 160
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 8'
      end
      object LNivel9: TLabel
        Left = 5
        Top = 183
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 9'
      end
      object LNivel1: TLabel
        Left = 5
        Top = 4
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 1'
      end
      object Lnivel2: TLabel
        Left = 5
        Top = 26
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 2'
      end
      object Lnivel3: TLabel
        Left = 5
        Top = 48
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 3'
      end
      object LNivel4: TLabel
        Left = 5
        Top = 70
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel4'
      end
      object LNivel5: TLabel
        Left = 5
        Top = 92
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel5'
      end
      object LNivel6: TLabel
        Left = 5
        Top = 114
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 6'
      end
      object LNivel10: TLabel
        Left = 5
        Top = 206
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 10'
        Visible = False
      end
      object LNivel11: TLabel
        Left = 5
        Top = 229
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 11'
        Visible = False
      end
      object LNivel12: TLabel
        Left = 5
        Top = 252
        Width = 90
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'Nivel 12'
        Visible = False
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 133
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 156
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 179
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 0
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 22
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 44
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 66
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 88
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 110
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 202
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 225
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 101
        Top = 248
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
    object TabGeneral: TcxTabSheet
      Caption = '&General'
      object lbDescrip: TLabel
        Left = 15
        Top = 16
        Width = 59
        Height = 13
        Caption = 'Descripci'#243'n:'
      end
      object lbObserva: TLabel
        Left = 0
        Top = 44
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
      object ZMemo: TcxMemo
        Left = 76
        Top = 38
        Properties.ScrollBars = ssVertical
        TabOrder = 1
        Height = 209
        Width = 330
      end
      object ZDescrip: TEdit
        Left = 76
        Top = 12
        Width = 330
        Height = 21
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 300
    Top = 49
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ModificarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
