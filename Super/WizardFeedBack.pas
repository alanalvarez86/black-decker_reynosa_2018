unit WizardFeedBack;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, Buttons,
     ZetaCommonLists, ZetaDBTextBox, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxButtons, ZBaseDlgModal_DevEx,
  ImgList, dxGDIPlusClasses;

type
  TInfoProceso = class( TObject )
  public
    Empleado: Integer;
    Error: String;
    Folio: Integer;
    Fin: TDateTime;
    Inicio: TDateTime;
    Maximo: Integer;
    Proceso: Procesos;
    Procesados: Integer;
    Status: eProcessStatus;
    procedure SetResultado( Valor: OleVariant );
  end;

  TWizardSuperFeedback = class(TZetaDlgModal_DevEx)
    Imagen: TImage;
    InicioLBL: TLabel;
    Inicio: TZetaTextBox;
    FinLBL: TLabel;
    Fin: TZetaTextBox;
    Duracion: TZetaTextBox;
    Label1: TLabel;
    MaximoLBL: TLabel;
    Maximo: TZetaTextBox;
    Avance: TZetaTextBox;
    PasoLBL: TLabel;
    EmpleadoLBL: TLabel;
    Empleado: TZetaTextBox;
    FolioLBL: TLabel;
    Folio: TZetaTextBox;
    ProcesoLBL: TLabel;
    Proceso: TZetaTextBox;
    Status: TZetaTextBox;
    StatusLBL: TLabel;
    MensajesLBL: TLabel;
    Mensajes: TMemo;
    cxImageList_ImagenMensaje: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FProcessData: TInfoProceso;
    procedure SetBotonDefault(const lOK: Boolean);
  public
    { Public declarations }
    property ProcessData: TInfoProceso read FProcessData;
    function ProcessOK: Boolean;
    function ShowProcessInfo(const lMostrar: Boolean): Integer;
    procedure SetMensajes( const sValue: String );
  end;

implementation

uses ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo;

const
     K_ALTURA_FORMA = 310;
     K_STATUS_NO_3 = 3;

{$R *.DFM}

{ TInfoProceso }

procedure TInfoProceso.SetResultado(Valor: OleVariant);
begin
     Folio := Valor[ K_PROCESO_FOLIO ];
     Status := eProcessStatus( Valor[ K_PROCESO_STATUS ] );
     Proceso := Procesos( Valor[ K_PROCESO_ID ] );
     Maximo := Valor[ K_PROCESO_MAXIMO ];
     Procesados := Valor[ K_PROCESO_PROCESADOS ];
     Empleado := Valor[ K_PROCESO_ULTIMO_EMPLEADO ];
     Inicio := Valor[ K_PROCESO_INICIO ];
     Fin := Valor[ K_PROCESO_FIN ];
     Error := '';
end;

{ *********** TWizardResults ********* }

procedure TWizardSuperFeedback.FormCreate(Sender: TObject);
begin
     FProcessData := TInfoProceso.Create;
     inherited;
     Height := K_ALTURA_FORMA;
end;

procedure TWizardSuperFeedback.FormDestroy(Sender: TObject);
begin
     FProcessData.Free;
     inherited;
end;

procedure TWizardSuperFeedback.FormShow(Sender: TObject);
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     inherited;
     with FProcessData do
     begin
          //DevEx (by am): Existe la posibilidad de 5 status para los procesos, contando del 0-4 los status 3 y 4 utilizan la misma imagen.
          if (Ord(Status)> K_STATUS_NO_3 ) then
             cxImageList_ImagenMensaje.GetBitmap(K_STATUS_NO_3,Imagen.Picture.Bitmap)
          else
             cxImageList_ImagenMensaje.GetBitmap(Ord(Status),Imagen.Picture.Bitmap);
          case Status of
               epsEjecutando:
               begin
                    Caption := '� Proceso Incompleto !';
                    Self.Status.Caption := 'No Ha Terminado';
                    SetBotonDefault( True );
               end;
               epsOK:
               begin
                    Caption := '� Proceso Terminado !';
                    Self.Status.Caption := 'Termin� Con Exito';
                    SetBotonDefault( True );
               end;
               epsCancelado:
               begin
                    Caption := '� Proceso Cancelado !';
                    Self.Status.Caption := 'Fu� Cancelado';
                    SetBotonDefault( False );
               end;
               epsError:
               begin
                    Caption := '� Proceso Con Errores !';
                    Self.Status.Caption := 'Termin� Con Errores';
                    SetBotonDefault( False );
               end;
          end;
          Self.Folio.Caption := IntToStr( Folio );
          Self.Proceso.Caption := ZetaClientTools.GetProcessName( Proceso );
          Self.Inicio.Caption := FormatDateTime( K_DIA_HORA, Inicio );
          Self.Fin.Caption := FormatDateTime( K_DIA_HORA, Fin );
          Self.Duracion.Caption := ZetaClientTools.GetDuration( ZetaClientTools.GetElapsed( Fin, Inicio ) );
          Self.Maximo.Caption := IntToStr( Maximo );
          Self.Avance.Caption := FormatFloat( '#0.# %', 100 * Procesados  / ZetaCommonTools.iMax( 1, Maximo ) );
          Self.Empleado.Caption := IntToStr( Empleado );
     end;
     with Mensajes do
     begin
          if ( Lines.Count = 0 ) then
          begin
               MensajesLBL.Visible := False;
               Mensajes.Visible := False;
               Self.Height := K_ALTURA_FORMA - Height;
          end
          else
          begin
               MensajesLBL.Visible := True;
               Mensajes.Visible := True;
               Self.Height := K_ALTURA_FORMA;
          end;
     end;
     Beep;
     if not Application.Active then
        FlashWindow(Application.Handle,TRUE);
end;

procedure TWizardSuperFeedback.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with Mensajes.Lines do
     begin
          BeginUpdate;
          Clear;
          EndUpdate;
     end;
end;

function TWizardSuperFeedback.ProcessOK: Boolean;
begin
     Result := ( ProcessData.Status in [ epsOk ] );
end;

procedure TWizardSuperFeedback.SetBotonDefault( const lOK: Boolean );
begin
     if lOk then
     begin
          OK_DevEx.Default := True;
          Cancelar_DevEx.Default := False;
          ActiveControl := OK_DevEx;
     end
     else
     begin
          OK_DevEx.Default := False;
          Cancelar_DevEx.Default := True;
          ActiveControl := Cancelar_DevEx;
     end;
end;

procedure TWizardSuperFeedback.SetMensajes( const sValue: String );
begin
     with Mensajes.Lines do
     begin
          BeginUpdate;
          Clear;
          Text := sValue;
          EndUpdate;
     end;
end;

function TWizardSuperFeedback.ShowProcessInfo( const lMostrar: Boolean ): Integer;
begin
     with ProcessData do
     begin
          Result := 0;
          if ( Status in [ epsCatastrofico ] ) then
          begin
               ZetaDialogo.zError( '� Error En Proceso !',
                                   'Se Encontr� Un Error En El Proceso' +
                                   CR_LF +
                                   ZetaClientTools.GetProcessName( Proceso ) +
                                   CR_LF +
                                   Error, 0 );
          end
          else
          begin
               if lMostrar or not ProcessOK then
               begin
                    Application.Restore;
                    ShowModal;
                    if ( ModalResult = mrYes ) then
                       Result := Folio;
               end;
          end;
     end;
end;

end.
