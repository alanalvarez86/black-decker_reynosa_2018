object dmSuper: TdmSuper
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 479
  Width = 741
  object cdsTarjeta: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsTarjetaBeforePost
    AfterCancel = cdsTarjetaAfterCancel
    OnCalcFields = cdsTarjetaCalcFields
    OnNewRecord = cdsTarjetaNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsTarjetaAlAdquirirDatos
    AlEnviarDatos = cdsTarjetaAlEnviarDatos
    AlCrearCampos = cdsTarjetaAlCrearCampos
    AlAgregar = cdsTarjetaAlAgregar
    AlBorrar = cdsTarjetaAlBorrar
    AlModificar = cdsTarjetaAlModificar
    Left = 32
    Top = 32
  end
  object cdsChecadas: TZetaClientDataSet
    Aggregates = <>
    Filter = '( CH_TIPO < 5 ) AND ( CH_SISTEMA <> '#39'S'#39' )'
    Params = <>
    AfterOpen = cdsChecadasAfterOpen
    AfterInsert = cdsChecadasAfterCambios
    AfterEdit = cdsChecadasAfterCambios
    BeforePost = cdsChecadasBeforePost
    AfterPost = cdsChecadasAfterPost
    AfterCancel = cdsChecadasAfterCancel
    AfterDelete = cdsChecadasAfterCambios
    AlCrearCampos = cdsChecadasAlCrearCampos
    Left = 112
    Top = 32
  end
  object cdsGridAsistencia: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO'
    Params = <>
    BeforePost = cdsGridAsistenciaBeforePostGenerales
    AlAdquirirDatos = cdsGridAsistenciaAlAdquirirDatos
    AlEnviarDatos = cdsGridAsistenciaAlEnviarDatos
    AlCrearCampos = cdsGridAsistenciaAlCrearCampos
    Left = 192
    Top = 32
  end
  object cdsEmpleados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsEmpleadosCalcFields
    AlAdquirirDatos = cdsEmpleadosAlAdquirirDatos
    AlCrearCampos = cdsEmpleadosAlCrearCampos
    Left = 34
    Top = 96
  end
  object cdsHisPermisos: TZetaClientDataSet
    Tag = 15
    Aggregates = <>
    IndexFieldNames = 'PM_FEC_INI'
    Params = <>
    BeforePost = cdsHisPermisosBeforePost
    AfterDelete = cdsHisPermisosAfterDelete
    OnNewRecord = cdsHisPermisosNewRecord
    OnReconcileError = cdsHisPermisosRenconcileError
    AlAdquirirDatos = cdsHisPermisosAlAdquirirDatos
    AlEnviarDatos = cdsHisPermisosAlEnviarDatos
    AlCrearCampos = cdsHisPermisosAlCrearCampos
    AlModificar = cdsHisPermisosAlModificar
    Left = 192
    Top = 96
  end
  object cdsHisKardex: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'Descendente'
        Fields = 'CB_FECHA;CB_NIVEL'
        Options = [ixDescending]
      end>
    IndexName = 'Descendente'
    Params = <>
    StoreDefs = True
    AfterDelete = cdsHisKardexAfterDelete
    OnNewRecord = cdsHisKardexNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsHisKardexAlAdquirirDatos
    AlEnviarDatos = cdsHisKardexAlEnviarDatos
    AlCrearCampos = cdsHisKardexAlCrearCampos
    AlAgregar = cdsHisKardexAlAgregar
    AlModificar = cdsHisKardexAlModificar
    Left = 32
    Top = 160
  end
  object cdsMisAsignados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsMisAsignadosAlAdquirirDatos
    Left = 112
    Top = 224
  end
  object cdsSupervisores: TZetaLookupDataSet
    Aggregates = <>
    IndexFieldNames = 'TB_CODIGO'
    Params = <>
    AlAdquirirDatos = cdsSupervisoresAlAdquirirDatos
    LookupName = 'Lista de Mis Supervisores'
    LookupDescriptionField = 'TB_ELEMENT'
    LookupKeyField = 'TB_CODIGO'
    OnGetRights = cdsSuperLookupGetRights
    Left = 194
    Top = 224
  end
  object cdsAsignaciones: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    AlEnviarDatos = cdsAsignacionesAlEnviarDatos
    Left = 32
    Top = 224
    object cdsAsignacionesCB_CODIGO: TIntegerField
      FieldName = 'CB_CODIGO'
    end
    object cdsAsignacionesCB_NIVEL: TStringField
      FieldName = 'CB_NIVEL'
      Size = 6
    end
    object cdsAsignacionesANTERIOR: TStringField
      FieldName = 'ANTERIOR'
      Size = 6
    end
    object cdsAsignacionesFECHA: TDateField
      FieldName = 'FECHA'
    end
  end
  object cdsDatosEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsDatosEmpleadoAlAdquirirDatos
    AlEnviarDatos = cdsDatosEmpleadoAlEnviarDatos
    AlCrearCampos = cdsDatosEmpleadoAlCrearCampos
    AlModificar = cdsDatosEmpleadoAlModificar
    Left = 112
    Top = 96
  end
  object cdsOtrosHistoriales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 160
  end
  object cdsGridAutoXAprobar: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'CB_CODIGO'
    Params = <>
    BeforePost = cdsGridAutoXAprobarBeforePost
    AlAdquirirDatos = cdsGridAutoXAprobarAlAdquirirDatos
    AlEnviarDatos = cdsGridAutoXAprobarAlEnviarDatos
    AlCrearCampos = cdsGridAutoXAprobarAlCrearCampos
    Left = 32
    Top = 280
  end
  object cdsEmpVacacion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsEmpVacacionAlCrearCampos
    Left = 112
    Top = 280
  end
  object cdsHisVacacion: TZetaClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    IndexFieldNames = 'VA_FEC_INI'
    Params = <>
    StoreDefs = True
    BeforeInsert = cdsHisVacacionBeforeInsert
    BeforeEdit = cdsHisVacacionBeforeEdit
    BeforePost = cdsHisVacacionBeforePost
    AfterCancel = cdsHisVacacionAfterCancel
    AfterDelete = cdsHisVacacionAfterDelete
    OnNewRecord = cdsHisVacacionNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsHisVacacionAlAdquirirDatos
    AlEnviarDatos = cdsHisVacacionAlEnviarDatos
    AlCrearCampos = cdsHisVacacionAlCrearCampos
    AlBorrar = cdsHisVacacionAlBorrar
    AlModificar = cdsHisVacacionAlModificar
    Left = 192
    Top = 160
  end
  object cdsDataSet: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 192
    Top = 280
  end
  object cdsClasifiTemp: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsClasifiTempBeforePost
    AfterDelete = cdsClasifiTempAfterDelete
    OnNewRecord = cdsClasifiTempNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsClasifiTempAlAdquirirDatos
    AlEnviarDatos = cdsClasifiTempAlEnviarDatos
    AlCrearCampos = cdsClasifiTempAlCrearCampos
    AlModificar = cdsClasifiTempAlModificar
    Left = 32
    Top = 336
  end
  object cdsCalendarioEmpleado: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 336
  end
  object cdsAutorizarPrenomina: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsAutorizarPrenominaBeforePost
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsAutorizarPrenominaAlAdquirirDatos
    AlEnviarDatos = cdsAutorizarPrenominaAlEnviarDatos
    AlCrearCampos = cdsAutorizarPrenominaAlCrearCampos
    Left = 192
    Top = 336
  end
  object cdsPlanVacacion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsPlanVacacionBeforePost
    AfterDelete = cdsPlanVacacionAfterDelete
    OnNewRecord = cdsPlanVacacionNewRecord
    OnReconcileError = cdsPlanVacacionReconcileError
    AlEnviarDatos = cdsPlanVacacionAlEnviarDatos
    AlCrearCampos = cdsPlanVacacionAlCrearCampos
    AlAgregar = cdsPlanVacacionAlAgregar
    AlModificar = cdsPlanVacacionAlModificar
    Left = 288
    Top = 32
  end
  object cdsPlazas: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    Left = 288
    Top = 96
  end
  object cdsSesion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsSesionAfterInsert
    BeforePost = cdsSesionBeforePost
    AfterCancel = cdsSesionAfterCancel
    AfterDelete = cdsSesionAfterDelete
    OnNewRecord = cdsSesionNewRecord
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsSesionAlAdquirirDatos
    AlEnviarDatos = cdsSesionAlEnviarDatos
    AlCrearCampos = cdsSesionAlCrearCampos
    AlModificar = cdsSesionAlModificar
    Left = 288
    Top = 160
  end
  object cdsHisCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsHisCursosBeforePost
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsHisCursosAlAdquirirDatos
    AlEnviarDatos = cdsHisCursosAlEnviarDatos
    AlCrearCampos = cdsHisCursosAlCrearCampos
    AlAgregar = cdsCursosAlAgregar
    AlBorrar = cdsHisCursosAlBorrar
    AlModificar = cdsCursosAlModificar
    Left = 304
    Top = 216
  end
  object cdsHisCursosProg: TZetaClientDataSet
    Tag = 1
    Aggregates = <>
    IndexFieldNames = 'KC_FEC_TOM'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsHisCursosProgAlAdquirirDatos
    AlCrearCampos = cdsHisCursosProgAlCrearCampos
    Left = 304
    Top = 264
  end
  object cdsAprobados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = cdsAprobadosAfterEditInsert
    AfterEdit = cdsAprobadosAfterEditInsert
    AfterDelete = cdsAprobadosAfterEditInsert
    OnReconcileError = cdsSesionReconcileError
    AlAdquirirDatos = cdsAprobadosAlAdquirirDatos
    AlCrearCampos = cdsAprobadosAlCrearCampos
    Left = 296
    Top = 336
  end
  object cdsGrupos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsGruposCalcFields
    OnReconcileError = cdsReconcileError
    AlCrearCampos = cdsGruposAlCrearCampos
    AlAgregar = cdsCursosAlAgregar
    AlBorrar = cdsGruposAlBorrar
    AlModificar = cdsCursosAlModificar
    Left = 384
    Top = 104
  end
  object cdsGridTarjetasPeriodo: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforeOpen = cdsGridTarjetasPeriodoBeforeOpen
    BeforePost = cdsGridTarjetasPeriodoBeforePost
    OnReconcileError = cdsGridTarjetasPeriodoReconcileError
    AlAdquirirDatos = cdsGridTarjetasPeriodoAlAdquirirDatos
    AlEnviarDatos = cdsGridTarjetasPeriodoAlEnviarDatos
    AlCrearCampos = cdsGridTarjetasPeriodoAlCrearCampos
    Left = 392
    Top = 40
  end
  object cdsListaEmpleados: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsEmpleadosAlAdquirirDatos
    AlCrearCampos = cdsEmpleadosAlCrearCampos
    Left = 392
    Top = 160
  end
  object cdsCertEmp: TZetaClientDataSet
    Tag = 37
    Aggregates = <>
    Params = <>
    OnCalcFields = cdsCertEmpCalcFields
    AlAdquirirDatos = cdsCertEmpAlAdquirirDatos
    AlCrearCampos = cdsCertEmpAlCrearCampos
    Left = 392
    Top = 220
  end
  object cdsCosteoTransferencias: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'AU_FECHA;CB_CODIGO'
    Params = <>
    BeforeInsert = cdsCosteoTransferenciasBeforeInsert
    BeforePost = cdsCosteoTransferenciasBeforePost
    AfterPost = cdsCosteoTransferenciasAfterPost
    OnNewRecord = cdsCosteoTransferenciasNewRecord
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsCosteoTransferenciasAlEnviarDatos
    AlCrearCampos = cdsCosteoTransferenciasAlCrearCampos
    AlAgregar = cdsCosteoTransferenciasAlAgregar
    AlBorrar = cdsCosteoTransferenciasAlBorrar
    AlModificar = cdsCosteoTransferenciasAlModificar
    Left = 584
    Top = 40
  end
  object cdsSuperCosteo: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'US_CODIGO;CB_NIVEL'
    Params = <>
    AlAdquirirDatos = cdsSuperCosteoAlAdquirirDatos
    Left = 584
    Top = 104
  end
  object cdsGridCosteoTransferencia: TZetaClientDataSet
    Aggregates = <>
    IndexFieldNames = 'AU_FECHA;CB_CODIGO'
    Params = <>
    OnReconcileError = cdsReconcileError
    AlEnviarDatos = cdsGridCosteoTransferenciaAlEnviarDatos
    AlCrearCampos = cdsCosteoTransferenciasAlCrearCampos
    Left = 576
    Top = 168
  end
  object cdsSaldoVacacionesTotales: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlCrearCampos = cdsSaldoVacacionesTotalesAlCrearCampos
    Left = 512
    Top = 80
  end
  object cdsSaldoVacaciones: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsSaldoVacacionesAlAdquirirDatos
    AlCrearCampos = cdsSaldoVacacionesAlCrearCampos
    Left = 512
    Top = 144
  end
  object cdsPlanCapacitacion: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsPlanCapacitacionAlAdquirirDatos
    AlCrearCampos = cdsPlanCapacitacionAlCrearCampos
    Left = 472
    Top = 296
  end
  object cdsHisCompeten: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsHisCompetenBeforePost
    AfterDelete = cdsHisCompetenAfterDelete
    OnNewRecord = cdsHisCompetenNewRecord
    OnReconcileError = cdsHisCompEvaluaReconcileError
    AlAdquirirDatos = cdsHisCompetenAlAdquirirDatos
    AlEnviarDatos = cdsHisCompetenAlEnviarDatos
    AlCrearCampos = cdsHisCompetenAlCrearCampos
    AlModificar = cdsHisCompetenAlModificar
    Left = 568
    Top = 296
  end
  object cdsHisCompEvalua: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsHisCompEvaluaBeforePost
    AfterDelete = cdsHisCompEvaluaAfterDelete
    OnNewRecord = cdsHisCompEvaluaNewRecord
    OnReconcileError = cdsHisCompEvaluaReconcileError
    AlAdquirirDatos = cdsHisCompEvaluaAlAdquirirDatos
    AlEnviarDatos = cdsHisCompEvaluaAlEnviarDatos
    AlCrearCampos = cdsHisCompEvaluaAlCrearCampos
    AlModificar = cdsHisCompEvaluaAlModificar
    Left = 480
    Top = 376
  end
  object cdsMatrizHabilidades: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsMatrizHabilidadesAlAdquirirDatos
    AlCrearCampos = cdsMatrizHabilidadesAlCrearCampos
    Left = 608
    Top = 360
  end
  object cdsEvaluacionSel: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnNewRecord = cdsEvaluacionSelNewRecord
    Left = 368
    Top = 376
  end
  object cdsEvaluacionDiaria: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsEvaluacionDiariaBeforePost
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsEvaluacionDiariaAlAdquirirDatos
    AlEnviarDatos = cdsEvaluacionDiariaAlEnviarDatos
    AlCrearCampos = cdsEvaluacionDiariaAlCrearCampos
    Left = 488
    Top = 224
  end
  object cdsMatrizCursos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    OnReconcileError = cdsReconcileError
    AlAdquirirDatos = cdsMatrizCursosAlAdquirirDatos
    AlCrearCampos = cdsMatrizCursosAlCrearCampos
    Left = 560
    Top = 240
  end
end
