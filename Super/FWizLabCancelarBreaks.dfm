inherited WizLabCancelarBreaks: TWizLabCancelarBreaks
  Left = 336
  Top = 171
  Caption = 'Cancelaci'#243'n de Breaks'
  ClientHeight = 434
  ClientWidth = 435
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 435
    Height = 434
    Header.AssignedValues = [wchvGlyph]
    Header.Glyph.Data = {
      89504E470D0A1A0A0000000D4948445200000020000000200806000000737A7A
      F4000000206348524D00007A25000080830000F9FF000080E9000075300000EA
      6000003A980000176F925FC546000000097048597300000EBC00000EBC0195BC
      724900000301494441545847C556CF6B1341148E2855B4872A227AF2A2A04541
      FC011E8A54F00FF0A0E0C183A082DE3C7A909D6C12B5D9D9344252512214AAF9
      7D28E849147A88A820B4D6D26C3C15CC41C1430F1E7AF010DFDBBCDD6693B73F
      2620FDE00BECE6BDF9BE79336F76629D4E27A60221C40E53A46F6574396BEAC6
      8AA9CB2F6642660D615CA29048405D5B5BC54046648ED882BAEC70445379911F
      A5F040281B80199E92BAF18713EEE3125689D27CA16C00CABDC088F114C67D4A
      F385920158F3CBAC900FB15250B18394CE42CD802E5FF68B84521837289D85AA
      81A50181304267503A8BC806ECB6D3E5DF018170BEA3215844368080355D6704
      022975394FE92C140DC84F9C4820136692D25928193013468E1509A014F20AA5
      B3503200834D72227EC425CB8AEC18A5B3503280D8D28308A150853674CE2E4A
      F385B2018414E93B2010D4923F4D611EA3F0400C6500819B0B843813EDA8E288
      A10D20ECCF329C74D09E6F606FD4E1DCBF17A5ECBDF018D84A767F86001ED1F6
      AD489813F44A09910CA472F59DF1D2B79320B68D5EB980D29771EDF1D39B16E9
      B3F4DA837879E5843EF779841E07E06B402F2E8E6995D5475AB5B5216AAD8E28
      AD9EA1BF5C389B110C34B86B9828374FDBB940ADD62A884AEB10FDE58235102F
      2E8FBBC2CE00D5D62FC39CF388E085030CAC71874EBCB4BC5BAB59EBBD63D8AC
      5A93146263C080A8342F0E24116116EEF77D3A91DF0B33B7A8FD3C970F58AEED
      103BCF8D8184C9DCA450AF0151B7F6F4CFBC9FA9C2FBF3762C6C4030F0DB3100
      D5B86E0F0280A5BBC6E57A48CBE131A055AD1936B88710F3E3F1C3DC018C8733
      A04006369C3D907AF6F62897D74FA85043CCAE6D1A88BF5ADCCF057204130FBA
      069E5C4003CEC5636AEA39AEFB772E8765D51A770DC0C33936C887C9170BC751
      140CB49DF283B12417EB4758EEABAE01ADD6BCCB05F911673A9D7C3A2A13191D
      CBDFDB725189ADB969A062D5B9A0206A95E66DD88C23A999D7FBD8960B21EE83
      9E0AB43E7041618C17BF1E0E6AB9207A0D54AD8F5C5018C3DA3688FD15687041
      FF935D039DD83F6DF4C116D2DC57030000000049454E44AE426082}
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'Este proceso borra los breaks que se hayan registrado manualment' +
        'e por medio del proceso Registro Breaks.'
      Header.Title = 'Cancelaci'#243'n de breaks'
      object Fechalbl: TLabel
        Left = 42
        Top = 50
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Fecha:'
        Color = clWhite
        ParentColor = False
      end
      object Horalbl: TLabel
        Left = 24
        Top = 80
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Hora Final:'
        Color = clWhite
        ParentColor = False
      end
      object Arealbl: TLabel
        Left = 50
        Top = 110
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Area:'
        Color = clWhite
        ParentColor = False
      end
      object ZFecha: TZetaFecha
        Left = 78
        Top = 45
        Width = 121
        Height = 22
        Cursor = crArrow
        TabOrder = 0
        Text = '06/Jun/02'
        Valor = 37413.000000000000000000
      end
      object ZHora: TZetaHora
        Left = 78
        Top = 76
        Width = 51
        Height = 21
        EditMask = '99:99;0'
        TabOrder = 1
        Tope = 48
      end
      object AreaKL: TZetaKeyLookup_DevEx
        Left = 78
        Top = 106
        Width = 315
        Height = 21
        LookupDataset = dmLabor.cdsArea
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 199
        Width = 413
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 413
        inherited Advertencia: TcxLabel
          Caption = 'Proceso para cancelar breaks para una fecha y hora indicada.'
          Style.IsFontAssigned = True
          Width = 345
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 10
        Top = 139
      end
      inherited sFiltroLBL: TLabel
        Left = 35
        Top = 168
      end
      inherited Seleccionar: TcxButton
        Left = 138
        Top = 256
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 64
        Top = 136
      end
      inherited sFiltro: TcxMemo
        Left = 64
        Top = 167
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 64
        Top = 6
        Width = 314
        Color = clWhite
        ParentColor = False
      end
      inherited bAjusteISR: TcxButton
        Left = 379
        Top = 168
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
  end
end
