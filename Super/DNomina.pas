unit DNomina;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBClient,
{$ifdef DOS_CAPAS}
  DServerNomina,
{$else}
  Nomina_TLB,
{$endif}
  ZetaClientDataSet, ZetaTipoEntidad;

{$define QUINCENALES}
{.$undefine QUINCENALES}
  
type
  TdmNomina = class(TDataModule)
    cdsDatosAsist: TZetaClientDataSet;
    cdsMovDatosAsist: TZetaClientDataSet;
    procedure cdsDatosAsistAlAdquirirDatos(Sender: TObject);
    procedure cdsDatosAsistAfterOpen(DataSet: TDataSet);
    procedure cdsDatosAsistAlCrearCampos(Sender: TObject);
    procedure cdsMovDatosAsistAlCrearCampos(Sender: TObject);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AU_FECHAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure HO_CODIGOChange(Sender: TField);
  private
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerNomina;
    property Servidor: TdmServerNomina read GetServidor;
{$else}
    FServidor: IdmServerNominaDisp;
    function GetServidor: IdmServerNominaDisp;
    property Servidor: IdmServerNominaDisp read GetServidor;
{$endif}
  public
    function ValidaAfectada(var sMensaje: String; const lActiva: Boolean; const sOperacion: String): Boolean;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades);
    {$endif}
  end;

var
  dmNomina: TdmNomina;

implementation

uses DCliente, DCatalogos, DSistema, DSuper,
     ZetaCommonClasses, ZetaCommonLists;

{$R *.DFM}

{ TdmNomina }

{$ifdef DOS_CAPAS}
function TdmNomina.GetServidor: TdmServerNomina;
begin
     Result := DCliente.dmCliente.ServerNomina;
end;
{$else}
function TdmNomina.GetServidor: IdmServerNominaDisp;
begin
     Result := IdmServerNominaDisp( dmCliente.CreaServidor( CLASS_dmServerNomina, FServidor ) );
end;
{$endif}

procedure TdmNomina.cdsDatosAsistAlAdquirirDatos(Sender: TObject);
var
   Tarjetas: OleVariant;
   Parametros: TZetaParams;
begin
     Parametros := TZetaParams.Create;
     try
        with dmCliente do
        begin
             CargaActivosPeriodo( Parametros );
             with Parametros do
             begin
                  AddInteger( 'Empleado', dmSuper.EmpleadoNumero );
                  with cdsPeriodo do
                  begin
                       {$ifdef QUINCENALES}
                       AddDate( 'FechaIni', FieldByName( 'PE_ASI_INI' ).AsDateTime );
                       AddDate( 'FechaFin', FieldByName( 'PE_ASI_FIN' ).AsDateTime );
                       {$else}
                       AddDate( 'FechaIni', FieldByName( 'PE_FEC_INI' ).AsDateTime );
                       AddDate( 'FechaFin', FieldByName( 'PE_FEC_FIN' ).AsDateTime );
                       {$endif}
                  end;
             end;
        end;
        cdsDatosAsist.Data := Servidor.GetNomDatosAsist( dmCliente.Empresa, Parametros.VarValues, Tarjetas );
        with cdsMovDatosAsist do
        begin
             Data := Tarjetas
{$ifdef FALSE}
             if not cdsDatosAsist.IsEmpty then

             else
                 if Active then
                    EmptyDataSet;
{$endif}
        end;
     finally
            Parametros.Free;
     end;
end;

procedure TdmNomina.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

procedure TdmNomina.cdsDatosAsistAfterOpen(DataSet: TDataSet);
begin
     with cdsDatosAsist do
     begin
          FieldByName( 'US_CODIGO' ).OnGetText := US_CODIGOGetText;
          FieldByName( 'NO_USER_RJ' ).OnGetText := US_CODIGOGetText;
     end;
end;

procedure TdmNomina.cdsDatosAsistAlCrearCampos(Sender: TObject);
begin
     with cdsDatosAsist do
     begin
          ListaFija( 'NO_STATUS', lfStatusPeriodo );
          MaskTime( 'NO_HOR_OK' );
          MaskFecha( 'NO_FEC_OK' );
          CreateSimpleLookup( dmCatalogos.cdsTurnos, 'TURNO', 'CB_TURNO' );
          CreateSimpleLookup( dmSistema.cdsUsuariosLookUp, 'NO_DES_OK', 'NO_SUP_OK' );
     end;
end;

procedure TdmNomina.cdsMovDatosAsistAlCrearCampos(Sender: TObject);
begin
     with cdsMovDatosAsist do
     begin
          FieldByName( 'AU_FECHA' ).OnGetText := AU_FECHAGetText;
          FieldByName( 'HO_CODIGO' ).OnChange := HO_CODIGOChange;
          ListaFija( 'AU_STATUS', lfStatusAusencia );
          ListaFija( 'AU_TIPODIA', lfTipoDiaAusencia );
          MaskHoras( 'AU_HORAS' );
          MaskHoras( 'AU_EXTRAS' );
          MaskHoras( 'AU_NUM_EXT' );          
          MaskHoras( 'AU_TARDES' );
          MaskHoras( 'AU_PER_CG' );
          MaskHoras( 'AU_PER_SG' );
          MaskHoras( 'AU_DES_TRA' );
          MaskTime( 'CHECADA1' );
          MaskTime( 'CHECADA2' );
          MaskTime( 'CHECADA3' );
          MaskTime( 'CHECADA4' );
     end;
end;

procedure TdmNomina.HO_CODIGOChange(Sender: TField);
begin
     with Sender.Dataset do
     begin
          FieldByName( 'AU_HOR_MAN' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmNomina.AU_FECHAGetText( Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.DataSet.IsEmpty then
        Text := ''
     else
        Text := Sender.AsString + ' -  ' + ObtieneElemento( lfDiasSemana, DayOfWeek( Sender.AsDateTime ) - 1 );
end;

function TdmNomina.ValidaAfectada(var sMensaje: String; const lActiva: Boolean;
         const sOperacion: String): Boolean;
begin
     Result := FALSE;
     sMensaje := 'No Se Puede Modificar En Esta Pantalla';
end;

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmNomina.NotifyDataChange(const Entidades: Array of TipoEntidad);
{$else}
procedure TdmNomina.NotifyDataChange(const Entidades: ListaEntidades);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
      if Dentro( enEmpleado , Entidades ) or Dentro( enNomina , Entidades ) then
     {$else}
     if ( enEmpleado in Entidades ) or ( enNomina in Entidades ) then
     {$endif}
        cdsDatosAsist.SetDataChange;
end;



end.
