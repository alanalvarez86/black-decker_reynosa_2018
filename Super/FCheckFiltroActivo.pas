unit FCheckFiltroActivo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, ExtCtrls, Buttons;

type
  eAccionFiltro = ( afQuitar, afContinuar, afCancelar );
  TCheckFiltroActivo = class(TZetaDlgModal)
    TipoAccion: TRadioGroup;
    Mensaje: TLabel;
    Image1: TImage;
    Memo1: TMemo;
    CBNoPreguntar: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure TipoAccionClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function PreguntaFiltroActivo( var lNoPreguntar: Boolean ): eAccionFiltro;

var
  CheckFiltroActivo: TCheckFiltroActivo;

implementation

{$R *.DFM}

function PreguntaFiltroActivo( var lNoPreguntar: Boolean ): eAccionFiltro;
begin
     Result := afCancelar;

     if ( CheckFiltroActivo = nil ) then
        CheckFiltroActivo := TCheckFiltroActivo.Create( Application.MainForm ); // Se destruye al Salir del Programa //

     with CheckFiltroActivo do
     begin
          CBNoPreguntar.Checked := lNoPreguntar;
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               Result := eAccionFiltro( TipoAccion.ItemIndex );
               lNoPreguntar := CBNoPreguntar.Checked;
          end;
     end;
end;

procedure TCheckFiltroActivo.FormCreate(Sender: TObject);
begin
     inherited;
     TipoAccion.ItemIndex := 0;
     //HelpContext := 00503;  PENDIENTE
end;

procedure TCheckFiltroActivo.FormShow(Sender: TObject);
begin
     inherited;
     TipoAccionClick( self );
     OK.SetFocus;
end;

procedure TCheckFiltroActivo.TipoAccionClick(Sender: TObject);
begin
     inherited;
     CBNoPreguntar.Enabled := ( TipoAccion.ItemIndex = 1 );
end;

end.
