inherited AsignandoEmpleados: TAsignandoEmpleados
  Left = 406
  Top = 533
  Caption = 'Empleados Certificados'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    inherited OK_DevEx: TcxButton
      OnClick = OK_DevExClick
    end
  end
  object ZetaDBGrid: TZetaCXGrid [1]
    Left = 0
    Top = 0
    Width = 375
    Height = 164
    Align = alClient
    TabOrder = 1
    object ZetaDBGridDBTableView: TcxGridDBTableView
      OnDblClick = ZetaDBGridDBTableViewDblClick
      Navigator.Buttons.CustomButtons = <>
      FilterBox.CustomizeDialog = False
      DataController.DataSource = DataSource1
      DataController.Options = [dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnSortingChanged = ZetaDBGridDBTableViewDataControllerSortingChanged
      Filtering.ColumnFilteredItemsList = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OnColumnHeaderClick = ZetaDBGridDBTableViewColumnHeaderClick
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        OnCustomDrawCell = CB_CODIGOCustomDrawCell
        MinWidth = 64
        Width = 64
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
        MinWidth = 262
        Styles.Content = cxStyle2
      end
    end
    object ZetaDBGridLevel: TcxGridLevel
      GridView = ZetaDBGridDBTableView
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object DataSource1: TDataSource
    Left = 160
    Top = 8
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = 16377581
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = clWhite
    end
  end
end
