unit FPermisos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$ifndef VER130}
  Variants, MaskUtils,
  {$endif}
  {ZBaseConsulta,} Grids, DBGrids, ZetaDBGrid, Db, ExtCtrls, ZBaseGridLectura_DevEx,
  ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TPermisos = class(TBaseGridLectura_DevEx)
    PM_FEC_INI: TcxGridDBColumn;
    PM_DIAS: TcxGridDBColumn;
    PM_FEC_FIN: TcxGridDBColumn;
    PM_CLASIFI: TcxGridDBColumn;
    TB_ELEMENT: TcxGridDBColumn;
    PM_NUMERO: TcxGridDBColumn;
    PM_COMENTA: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    //function IncidenciaValida( const sAccion: String; var sMensaje: String): Boolean;
    { Private declarations }
  protected
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    function ValoresGrid: Variant;override;
  public
    procedure Connect;override;
    procedure Refresh;override;
  end;

var
  Permisos: TPermisos;

implementation

uses DSuper, DCliente, ZetaCommonLists;

{$R *.DFM}

procedure TPermisos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 11513;
end;

procedure TPermisos.Connect;
begin
     with dmSuper do
     begin
          DataSource.DataSet := cdsHisPermisos;
          cdsHisPermisos.Conectar;
     end;
end;

procedure TPermisos.Refresh;
begin
     dmSuper.cdsHisPermisos.Refrescar;
     DoBestFit;
end;

procedure TPermisos.Agregar;
begin
     inherited;
     dmSuper.cdsHisPermisos.Agregar;
end;

procedure TPermisos.Borrar;
begin
     inherited;
     dmSuper.cdsHisPermisos.Borrar;
     DoBestFit;
end;

procedure TPermisos.Modificar;
begin
     inherited;
     dmSuper.cdsHisPermisos.Modificar;
end;

function TPermisos.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje ) and dmSuper.IncidenciaValida( 'Borrar', sMensaje );
end;

function TPermisos.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje ) and dmSuper.IncidenciaValida( 'Modificar', sMensaje );
end;

{
function TPermisos.IncidenciaValida( const sAccion: String; var sMensaje: String ): Boolean;
begin
     Result := ( Datasource.Dataset.FieldByName( 'PM_CLASIFI' ).AsInteger = Ord( tpSinGoce ) );
     if not Result then
        sMensaje := 'Solo Se Pueden Modificar Permisos SIN Goce De Sueldo';
end;
}

function TPermisos.ValoresGrid : Variant;
begin
     Result := VarArrayOf( [ dmCliente.GetValorActivoStr( stEmpleado ),
                             dmCliente.GetValorActivoStr( stFecha ),
                             stEmpleado,stFecha] );
end;

procedure TPermisos.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
  inherited;
end;

end.
