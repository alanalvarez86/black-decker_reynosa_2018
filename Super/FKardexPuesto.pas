unit FKardexPuesto;

interface                                                       

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ZetaFecha, ExtCtrls, StdCtrls, DBCtrls, ZetaDBTextBox, Mask, ComCtrls,
  Buttons, Grids, DBGrids,
  {$ifndef VER130}
  Variants,
  {$endif}
  FKardexBase_DevEx,
  ZetaNumero,
  ZetaSmartLists, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxPCdxBarPopupMenu, cxControls, cxContainer, cxEdit, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxTextEdit, cxMemo, cxDBEdit, cxPC, cxButtons, ZetaKeyLookup_DevEx,
  dxBarBuiltInMenu;

type
  TKardexPuesto = class(TKardexBase_DevEx)
    CB_RANGO_S: TZetaDBNumero;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_RANGO_SLbl: TLabel;
    ClasificacionLbl: TLabel;
    PuestoLbl: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CB_PUESTOExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SetEditarSoloActivos;
  protected
    procedure Connect;override;
    procedure EscribirCambios; override;
  private
    { Private declarations }
    sPuesto: String;
    FOldPuesto: String;
    procedure SetMatsushita;
  public
    { Public declarations }
  end;

var
  KardexPuesto: TKardexPuesto;

implementation

uses ZAccesosTress,ZetaCommonClasses,ZetaCommonLists, ZetaCommonTools, ZetaDialogo,
     dCatalogos, dSistema, DSuper, DCliente;

{$R *.DFM}

procedure TKardexPuesto.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     HelpContext:= H10156_Cambio_puesto;
     IndexDerechos := ZAccesosTress.D_EMP_EXP_KARDEX;
     with dmCatalogos do
     begin
          CB_PUESTO.LookupDataSet  := cdsPuestos;
          CB_CLASIFI.LookupDataSet := cdsClasifi;
     end;
     //@DACP Se cambio el valor de la propiedad "EditarSoloActivos" por codigo, ya que no respetaba, lo que se le asigna en el DFM, Bug#15743
     SetEditarSoloActivos;
end;

procedure TKardexPuesto.FormShow(Sender: TObject);
begin
     inherited;
     Datasource.AutoEdit := TRUE;
     SetMatsushita;
     CB_PUESTO.Enabled := ( not dmCliente.UsaPlazas );
     if CB_PUESTO.Enabled then
        self.Caption := 'Cambio de Puesto'
     else
         self.Caption := 'Cambio de Clasificación';
end;

procedure TKardexPuesto.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmCatalogos do
     begin
          cdsPuestos.Conectar;
          cdsClasifi.Conectar;
     end;
     with dmSuper do
     begin
          cdsHisKardex.Conectar;
          DataSource.DataSet := cdsHisKardex;
          sPuesto := cdsHisKardex.FieldByName( 'CB_PUESTO' ).AsString;
     end;
end;

procedure TKardexPuesto.EscribirCambios;
begin
     with dmSuper do
     begin
          if ( FOldPuesto = cdsHisKardex.FieldByName( 'CB_PUESTO' ).AsString ) or
             ( ( dmCliente.EsPuestoValido( cdsHisKardex.FieldByName( 'CB_PUESTO' ).AsString ) ) ) then
          begin
               if zStrToBool( cdsDatosEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString ) then
               begin
                   DataBaseError( 'No Se Permite Cambiar Puesto/Clasificación A Empleados Con Tabulador' );
               end
               else
                   inherited;
          end;
     end;
end;

procedure TKardexPuesto.CB_PUESTOExit(Sender: TObject);
var
   sClasifi : String;
begin
     if StrLleno( CB_PUESTO.Llave ) and ( sPuesto <> CB_PUESTO.Llave ) then
     begin
          sPuesto := CB_PUESTO.LLave;
          sClasifi := dmCatalogos.cdsPuestos.FieldByName('PU_CLASIFI').AsString ;
          if StrLleno( sClasifi ) then
             CB_CLASIFI.Llave := sClasifi;
     end;
end;

procedure TKardexPuesto.SetMatsushita;
var
   lEnabled: Boolean;
begin
     lEnabled:=  ( zStrToBool( dmSuper.cdsDatosEmpleado.FieldByName( 'CB_AUTOSAL' ).AsString ) ) and
                Inserting;
     with dmCliente do
     begin
          with CB_RANGO_SLbl do
          begin
               Visible:= lEnabled;
               Enabled:= FALSE;
          end;
          with CB_RANGO_S do
          begin
               Visible:= lEnabled;
               Enabled:= FALSE;
          end;
     end;
end;

procedure TKardexPuesto.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( Field <> nil ) and ( ( Field.FieldName = 'CB_PUESTO' ) or ( Field.FieldName = 'CB_CLASIFI' ) ) then
     begin
          CB_RANGO_S.Enabled := CB_RANGO_S.Visible;
          CB_RANGO_SLbl.Enabled := CB_RANGO_S.Enabled;
          if ( Field.FieldName = 'CB_CLASIFI' ) and ( CB_RANGO_S.Visible ) then
             ActiveControl := CB_RANGO_S;
     end;
     if( Field = nil )then
     begin
          FOldPuesto := DataSource.DataSet.FieldByName( 'CB_PUESTO' ).AsString;
     end;
end;

procedure TKardexPuesto.SetEditarSoloActivos;
begin
     CB_PUESTO.EditarSoloActivos := TRUE;
     CB_CLASIFI.EditarSoloActivos := TRUE;
end;


end.
