unit FKardexVarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion_DevEx, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls,
  {$ifndef ver130} variants, maskutils,{$endif}
  ComCtrls, Mask, ZetaFecha, ZetaSmartLists, ZGlobalTress,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, cxControls, dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator, cxDBNavigator,
  cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, ZetaKeyLookup_DevEx,
  cxPC, cxContainer, cxEdit, cxTextEdit, cxMemo;

type
  TKardexVarios = class(TBaseEdicion_DevEx)
    Panel1: TPanel;
    Label7: TLabel;
    ZFecha: TZetaFecha;
    PageControl: TcxPageControl;
    TabCambios: TcxTabSheet;
    LPuesto: TLabel;
    LClasificacion: TLabel;
    LTurno: TLabel;
    TabArea: TcxTabSheet;
    LNivel7: TLabel;
    LNivel8: TLabel;
    LNivel9: TLabel;
    TabGeneral: TcxTabSheet;
    lbDescrip: TLabel;
    lbObserva: TLabel;
    ZMemo: TcxMemo;
    ZDescrip: TEdit;
    CB_PUESTO: TZetaDBKeyLookup_DevEx;
    CB_CLASIFI: TZetaDBKeyLookup_DevEx;
    CB_TURNO: TZetaDBKeyLookup_DevEx;
    CB_NIVEL7: TZetaDBKeyLookup_DevEx;
    CB_NIVEL8: TZetaDBKeyLookup_DevEx;
    CB_NIVEL9: TZetaDBKeyLookup_DevEx;
    LNivel1: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup_DevEx;
    Lnivel2: TLabel;
    CB_NIVEL2: TZetaDBKeyLookup_DevEx;
    Lnivel3: TLabel;
    CB_NIVEL3: TZetaDBKeyLookup_DevEx;
    LNivel4: TLabel;
    CB_NIVEL4: TZetaDBKeyLookup_DevEx;
    LNivel5: TLabel;
    CB_NIVEL5: TZetaDBKeyLookup_DevEx;
    LNivel6: TLabel;
    CB_NIVEL6: TZetaDBKeyLookup_DevEx;
    CB_NIVEL10: TZetaDBKeyLookup_DevEx;
    LNivel10: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup_DevEx;
    LNivel11: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup_DevEx;
    LNivel12: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CB_PUESTOExit(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    sPuesto, OldPuesto, OldClasifi, OldTurno : String;
    OldAreas : Array [1..K_GLOBAL_NIVEL_MAX] of String;
    procedure SetCamposNivel;
    procedure SetDBControles(const lbPuesto, lbTurno, lbArea: Boolean);
    procedure ControlDBKeyLookupActivo(oControl: TZetaDBKeyLookup_DevEx; oLabel: TLabel; lEnabled: Boolean);
    procedure ValidaCambiosKardex;
  protected
    procedure Connect;override;
  public
  end;

var
  KardexVarios: TKardexVarios;

implementation

uses dSuper, dTablas, dCatalogos, dCliente, dGlobal, ZAccesosMgr, ZAccesosTress,
     ZetaCommonLists, ZetaCommonTools, ZetaClientTools, ZetaCommonClasses;

{$R *.DFM}

{ TKardexVarios }

procedure TKardexVarios.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     LNIVEL10.Visible := True;
     LNIVEL11.Visible := True;
     LNIVEL12.Visible := True;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     FirstControl := ZFecha;
     HelpContext:= 31314;
     SetCamposNivel;
     SetDBControles( ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_KARDEX, TPUESTO ),
                     ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_KARDEX, TTURNO ),
                     ZAccesosMgr.CheckDerecho( D_SUPER_TIPO_KARDEX, TAREA ) );
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
{$ifdef ACS}
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
{$endif}
end;

procedure TKardexVarios.FormShow(Sender: TObject);
const
     aMemoTop: array[FALSE..TRUE] of Integer = ( 38, 12 );
     aLabelTop: array[FALSE..TRUE] of Integer = ( 44, 16 );
begin
     ZMemo.Text:= VACIO;
     ZDescrip.Text:= VACIO;

     with dmCliente do
     begin
          ZFecha.Valor:= FechaSupervisor;
          ZDescrip.Visible:= not UsaPlazas;
          lbDescrip.Visible:= not UsaPlazas;
          ZMemo.Top:= aMemoTop[ UsaPlazas ];
          lbObserva.Top:= aLabelTop [ UsaPlazas ];
     end;
     PageControl.ActivePage:= TabCambios;
     inherited;
     Datasource.AutoEdit := TRUE;   // No se evaluan derechos una vez abierta la forma
end;

procedure TKardexVarios.Connect;
var
   i : Integer;
begin
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
     end;
     with dmTablas do
     begin
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmSuper do
     begin
          DataSource.DataSet:= cdsDatosEmpleado;
          with cdsDatosEmpleado do
          begin
               sPuesto  :=  FieldByName( 'CB_PUESTO' ).AsString;
               OldPuesto := sPuesto;
               OldClasifi := FieldByName( 'CB_CLASIFI' ).AsString;
               OldTurno := FieldByName( 'CB_TURNO' ).AsString;
               for i := 1 to 9 do
                   OldAreas[i] := FieldByName( 'CB_NIVEL' + IntToStr(i) ).AsString;
          end;
     end;
end;

procedure TKardexVarios.CB_PUESTOExit(Sender: TObject);
var
   sClasifi : String;
begin
     if StrLleno( CB_PUESTO.Llave ) and ( sPuesto <> CB_PUESTO.Llave ) then
     begin
          sPuesto := CB_PUESTO.LLave;
          sClasifi := dmCatalogos.cdsPuestos.FieldByName('PU_CLASIFI').AsString ;
          if StrLleno( sClasifi ) then
             CB_CLASIFI.Llave := sClasifi;
     end;
end;

procedure TKardexVarios.OKClick(Sender: TObject);
begin
     with dmSuper do
     begin
          ValidaCambiosKardex;
          with cdsDatosEmpleado do
               if ( zStrToBool( FieldByName( 'CB_AUTOSAL' ).AsString ) ) and
                  ( ( FieldByName( 'CB_PUESTO' ).AsString <> OldPuesto ) or
                    ( FieldByName( 'CB_CLASIFI' ).AsString <> OldClasifi ) ) then
                  DataBaseError( 'No Se Permite Cambiar Puesto/Clasificación A Empleados Con Tabulador' );
          OtrosDatos := VarArrayOf( [ZFecha.Valor, ZDescrip.Text, ZMemo.Text] );
     end;
     inherited;
end;

procedure TKardexVarios.ValidaCambiosKardex;
const
     sMensaje = 'Ya Existe un Cambio de %s para el %s';
var
   dFecha : TDate;
   lCambioArea : Boolean;
   i : Integer;
begin
     dFecha := ZFecha.Valor;
     with dmSuper do
          with cdsDatosEmpleado do
          begin
               // Puesto
               if ( OldPuesto <> FieldByName( 'CB_PUESTO' ).AsString ) or
                  ( OldClasifi <> FieldByName( 'CB_CLASIFI' ).AsString ) then
                  if ExisteKardex( dFecha, K_T_PUESTO ) then
                     DataBaseError( Format( sMensaje, [ K_T_PUESTO, FechaCorta( dFecha ) ] ) );
               // Turno
               if ( OldTurno <> FieldByName( 'CB_TURNO' ).AsString ) then
                  if ExisteKardex( dFecha, K_T_TURNO ) then
                     DataBaseError( Format( sMensaje, [ K_T_TURNO, FechaCorta( dFecha ) ] ) );
               // Area
               lCambioArea := FALSE;
               for i := 1 to 9 do
                   if ( OldAreas[i] <> FieldByName( 'CB_NIVEL' + IntToStr(i) ).AsString ) then
                   begin
                        lCambioArea := TRUE;
                        Break;
                   end;
               if lCambioArea then
                  if ExisteKardex( dFecha, K_T_AREA ) then
                     DataBaseError( Format( sMensaje, [ K_T_AREA, FechaCorta( dFecha ) ] ) );
     end;
end;

procedure TKardexVarios.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, LNivel1, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, LNivel2, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, LNivel3, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, LNivel4, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, LNivel5, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, LNivel6, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, LNivel7, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, LNivel8, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, LNivel9, CB_NIVEL9 );
     {$ifdef ACS}
     SetCampoNivel( 10, iNiveles, LNivel10, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, LNivel11, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, LNivel12, CB_NIVEL12 );
     {$endif}
end;

procedure TKardexVarios.SetDBControles( const lbPuesto, lbTurno, lbArea : Boolean );
begin
     { Puesto }
     ControlDBKeyLookupActivo( CB_PUESTO, LPuesto, lbPuesto );
     ControlDBKeyLookupActivo( CB_CLASIFI, LClasificacion, lbPuesto );
     { Turno }
     ControlDBKeyLookupActivo( CB_TURNO, LTurno, lbTurno );
     { Area }
     ControlDBKeyLookupActivo( CB_NIVEL1, LNivel1, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL2, LNivel2, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL3, LNivel3, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL4, LNivel4, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL5, LNivel5, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL6, LNivel6, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL7, LNivel7, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL8, LNivel8, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL9, LNivel9, lbArea );
     {$ifdef ACS}
     ControlDBKeyLookupActivo( CB_NIVEL10, LNivel10, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL11, LNivel11, lbArea );
     ControlDBKeyLookupActivo( CB_NIVEL12, LNivel12, lbArea );
     {$endif}
end;

procedure TKardexVarios.ControlDBKeyLookupActivo( oControl : TZetaDBKeyLookup_DevEx; oLabel : TLabel; lEnabled : Boolean );
begin
     oControl.Enabled := lEnabled;
end;

procedure TKardexVarios.OK_DevExClick(Sender: TObject);
begin
     with dmSuper do
     begin
          ValidaCambiosKardex;
          with cdsDatosEmpleado do
               if ( zStrToBool( FieldByName( 'CB_AUTOSAL' ).AsString ) ) and
                  ( ( FieldByName( 'CB_PUESTO' ).AsString <> OldPuesto ) or
                    ( FieldByName( 'CB_CLASIFI' ).AsString <> OldClasifi ) ) then
                  DataBaseError( 'No Se Permite Cambiar Puesto/Clasificación A Empleados Con Tabulador' );
          OtrosDatos := VarArrayOf( [ZFecha.Valor, ZDescrip.Text, ZMemo.Text] );
     end;
     inherited;
end;

end.



