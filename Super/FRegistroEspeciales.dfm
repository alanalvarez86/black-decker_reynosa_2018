inherited RegistroEspeciales: TRegistroEspeciales
  Left = 370
  Caption = 'Registro de Horas Especiales'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    TabOrder = 2
  end
  inherited Operaciones: TcxPageControl
    inherited Generales: TcxTabSheet
      inherited PanelHora: TPanel
        inherited LblHora: TLabel
          Left = 177
          Width = 51
          Caption = 'Hora Final:'
        end
        inherited LblCantidad: TLabel
          Left = 380
        end
        object Label3: TLabel [2]
          Left = 67
          Top = 4
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Hora Inicial:'
        end
        object LblDuracion: TLabel [3]
          Left = 283
          Top = 4
          Width = 46
          Height = 13
          Alignment = taRightJustify
          Caption = 'Duraci'#243'n:'
        end
        object Label4: TLabel [4]
          Left = 388
          Top = 4
          Width = 37
          Height = 13
          Alignment = taRightJustify
          Caption = 'Minutos'
        end
        inherited WK_HORA_R: TZetaDBHora
          Left = 231
          TabOrder = 1
          OnExit = WK_HORA_IExit
        end
        inherited WK_PIEZAS: TZetaDBNumero
          Left = 424
          Width = 25
          TabOrder = 3
        end
        object WK_HORA_I: TZetaHora
          Left = 125
          Top = 0
          Width = 42
          Height = 21
          EditMask = '99:99;0'
          TabOrder = 0
          Tope = 48
          OnExit = WK_HORA_IExit
        end
        object WK_PRE_CAL: TZetaDBNumero
          Left = 333
          Top = 0
          Width = 49
          Height = 21
          TabStop = False
          Enabled = False
          Mascara = mnDias
          TabOrder = 2
          Text = '0'
          DataField = 'WK_PRE_CAL'
          DataSource = DataSource
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
