unit FLabCancelarBreaksGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid_DevEx, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013, cxControls,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, ImgList, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ZetaCXGrid, cxButtons;

type
  TLabCancelarBreaksGridSelect = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    WK_PRE_CAL: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LabCancelarBreaksGridSelect: TLabCancelarBreaksGridSelect;

implementation

{$R *.DFM}

procedure TLabCancelarBreaksGridSelect.FormShow(Sender: TObject);
var I:integer;
begin
     SetControls;
     ZetaDbGridDbTableView.ApplyBestFit();
     for I:=0 to ZetadbGridDbtableview.ColumnCount -1 do
     begin
     Zetadbgriddbtableview.Columns[i].Width:= Zetadbgriddbtableview.Columns[i].Width + 25;
  if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='CB_CODIGO') then  zetadbgriddbtableview.Columns[i].Caption:='Codigo' ;
  if(zetadbgriddbtableview.Columns[i].DataBinding.FieldName='PRETTYNAME') then zetadbgriddbtableview.Columns[i].Caption:='Nombre';
     end;

end;

end.
