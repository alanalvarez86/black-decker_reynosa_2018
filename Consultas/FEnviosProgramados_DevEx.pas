unit FEnviosProgramados_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridLectura_DevEx, DB, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  StdCtrls, ZBaseConsulta, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Menus, ActnList, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, System.Actions, cxButtons, ZetaKeyCombo, Vcl.Mask,
  ZetaFecha, ZetaCommonLists, ZetaDialogo, ZFiltroSQLTools;

type
  TEnviosProgramados_DevEx = class(TBaseGridLectura_DevEx)
    PanelParam: TPanel;
    InscribirUsuarios: TcxButton;
    InscribirRoles: TcxButton;
    CA_NOMBRE: TcxGridDBColumn;
    CM_CODIGO: TcxGridDBColumn;
    CA_REPORT: TcxGridDBColumn;
    CA_ACTIVO: TcxGridDBColumn;
    CA_FREC: TcxGridDBColumn;
    CA_NX_FEC: TcxGridDBColumn;
    CA_US_CHG: TcxGridDBColumn;
    CA_ULT_FEC: TcxGridDBColumn;
    CA_FECHA: TcxGridDBColumn;
    CA_CAPTURA: TcxGridDBColumn;
    chkActivos: TCheckBox;
    BuscaBtn: TcxButton;
    EditBusca: TEdit;
    Label1: TLabel;
    RE_NOMBRE: TcxGridDBColumn;
    EjecutarEnvio: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure InscribirUsuariosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure InscribirRolesClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewFocusedRecordChanged( Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure BuscaBtnClick(Sender: TObject);
    procedure EditBuscaChange(Sender: TObject);
    procedure chkActivosClick(Sender: TObject);
    procedure EjecutarEnvioClick(Sender: TObject);
  private
    { Private declarations }
    FBuscaFiltro: string;
    procedure ConfigAgrupamiento;
    procedure HabilitarControles;
    procedure FiltroGridEnviosProgramados;
  protected
    { Protected declarations}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure ReconnectGrid; override;
  public
    { Public declarations }
     property sBuscaFiltro: string read FBuscaFiltro write FBuscaFiltro;
  end;

var
  EnviosProgramados_DevEx: TEnviosProgramados_DevEx;

implementation
uses
    DSistema,
    ZetaCommonClasses,
    ZAccesosMgr,
    ZAccesosTress,
    ZetaCommonTools,
    FWizSistCalendarioReportes_DevEx,
    FSistSuscripcionUsuarios_DevEx,
    FSistSuscripcionRoles_DevEx,
    ZcxWizardBasico,
    ZBaseDlgModal_DevEx;

{$R *.dfm}

{ TLstDispositivos }

procedure TEnviosProgramados_DevEx.Connect;
begin
     with dmSistema do
     begin
          cdsUsuarios.Conectar;
          cdsEmpresasLookup.Conectar;
          cdsUsuariosLookUp.Conectar;
          cdsEnviosProgramadosEmpresa.Conectar;
          DataSource.DataSet := cdsEnviosProgramadosEmpresa;
     end;
     HabilitarControles;
end;

procedure TEnviosProgramados_DevEx.EditBuscaChange(Sender: TObject);
begin
  inherited;
        FiltroGridEnviosProgramados;
end;

procedure TEnviosProgramados_DevEx.ReconnectGrid;
begin
     inherited;
     FiltroGridEnviosProgramados;
     ZetaDBGridDBTableView.DataController.Filter.Refresh;
end;

procedure TEnviosProgramados_DevEx.EjecutarEnvioClick(Sender: TObject);
var sError, sNombre : String;
begin
     inherited;
     sNombre := dmSistema.cdsEnviosProgramadosEmpresa.FieldByName ('CA_NOMBRE').AsString;
     if ZConfirm(Self.Caption,'Se proceder� a ejecutar el env�o ' + sNombre + '.' + CR_LF + '�Seguro de ejecutar el env�o?',0,mbYes ) then
     begin
          sError := dmSistema.InsertarSolicitudEnvio(dmSistema.cdsEnviosProgramadosEmpresa);
          if StrLleno(sError) then
             ZError(Self.Caption, sError, 0)
          else
              ZInformation(Self.Caption, 'Se ha ejecutado el env�o: ' + sNombre + '.' + CR_LF + 'Puede consultar el estatus en la pantalla de ' + CR_LF + 'solicitudes de env�os programados.', 0);
     end;
end;

procedure TEnviosProgramados_DevEx.ZetaDBGridDBTableViewFocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord, AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
const K_CERO = 0;
begin
  inherited;
     InscribirUsuarios.Enabled := (ZetaDBGridDBTableView.DataController.DataRowCount <> K_CERO)
                               and (dmSistema.cdsEnviosProgramadosEmpresa.FieldByName ('CA_REPEMP').AsInteger = K_CERO);
     InscribirRoles.Enabled := InscribirUsuarios.Enabled;
     EjecutarEnvio.Enabled := ZetaDBGridDBTableView.DataController.DataRowCount <> K_CERO;
end;

procedure TEnviosProgramados_DevEx.Refresh;
begin
     dmSistema.cdsEnviosProgramadosEmpresa.Refrescar;
     ZetaDBGridDBTableView.ApplyBestFit();
     HabilitarControles;
     FiltroGridEnviosProgramados;
end;

procedure TEnviosProgramados_DevEx.Agregar;
begin
     dmSistema.cdsEnviosProgramadosEmpresa.Agregar;
     HabilitarControles;
end;

procedure TEnviosProgramados_DevEx.Modificar;
begin
     dmSistema.cdsEnviosProgramadosEmpresa.Modificar;
     HabilitarControles;
end;

procedure TEnviosProgramados_DevEx.Borrar;
begin
     dmSistema.cdsEnviosProgramadosEmpresa.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
     HabilitarControles;
end;

procedure TEnviosProgramados_DevEx.BuscaBtnClick(Sender: TObject);
begin
  inherited;
            FiltroGridEnviosProgramados;
end;

procedure TEnviosProgramados_DevEx.chkActivosClick(Sender: TObject);
begin
  inherited;
            FiltroGridEnviosProgramados;
end;

procedure TEnviosProgramados_DevEx.ConfigAgrupamiento;
begin
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := false;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := false;
end;

procedure TEnviosProgramados_DevEx.FiltroGridEnviosProgramados;
Const
      SIMBOLO = '%';
VAR
      sTextoBuscar : string;
begin
     ZetaDBGridDBTableView.DataController.Filter.Clear;
     FBuscaFiltro := VACIO;
     sTextoBuscar := VACIO;
     sTextoBuscar := FiltrarTextoSQL(EditBusca.Text);
     try
         if chkActivos.Checked then
         begin
               if strLleno( sTextoBuscar ) then
               begin
                    with DataSource.DataSet do
                    begin
                          Filtered := False;
                          FBuscaFiltro := '( CA_NOMBRE like '''+SIMBOLO+sTextoBuscar+SIMBOLO+''' OR CA_REPORT like '''+SIMBOLO+sTextoBuscar+SIMBOLO+''') AND CA_ACTIVO = ''S'' ';
                          Filter := FBuscaFiltro;
                          Filtered := True;
                    end;
               end
               else
               begin
                     if chkActivos.Checked then
                     begin
                          with DataSource.DataSet do
                          begin
                                Filtered := False;
                                FBuscaFiltro := ' CA_ACTIVO = ''S'' ';
                                Filter := FBuscaFiltro;
                                Filtered := True;
                          end;
                     end
                     else
                     begin
                          with DataSource.DataSet do
                          begin
                                Filtered := False;
                          end;
                     end;
               end;
         end
         else
         begin
               if strLleno( sTextoBuscar ) then
               begin
                    with DataSource.DataSet do
                    begin
                          Filtered := False;
                          FBuscaFiltro := '( CA_NOMBRE like '''+SIMBOLO+sTextoBuscar+SIMBOLO+''' OR CA_REPORT like '''+SIMBOLO+sTextoBuscar+SIMBOLO+''') ';
                          Filter := FBuscaFiltro;
                          Filtered := True;
                    end;
               end
               else
               begin
                    with DataSource.DataSet do
                    begin
                          Filtered := False;
                    end;
               end;
         end;
      Except
             on Error: Exception do
             begin
                  Raise Exception.Create( Error.Message );
             end;
      end;
end;

procedure TEnviosProgramados_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     IndexDerechos := ZAccesosTress.D_CONS_ENVIOS_PROGRAMADOS;
     HelpContext := H5005405_Envios_Programados;
end;

procedure TEnviosProgramados_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     CreaColumaSumatoria(zetadbGriddbtableview.Columns[0],0,'',SkCount);
     chkActivos.Checked := True;
     inherited;
     ConfigAgrupamiento;
     FiltroGridEnviosProgramados;
end;

procedure TEnviosProgramados_DevEx.InscribirRolesClick(Sender: TObject);
begin
     inherited;
     dmSistema.EnvioProgramadoXEmpresa := true;
     if CheckDerecho( D_CONS_ENVIOS_PROGRAMADOS, K_DERECHO_SIST_KARDEX ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( SistSuscripcionRoles_DevEx, TSistSuscripcionRoles_DevEx )
     else
         ZetaDialogo.zInformation( Caption, 'No tiene derechos para modificar suscripciones a roles.', 0 );
end;

procedure TEnviosProgramados_DevEx.InscribirUsuariosClick(Sender: TObject);
begin
     inherited;
     dmSistema.EnvioProgramadoXEmpresa := true;
     if CheckDerecho( D_CONS_ENVIOS_PROGRAMADOS, K_DERECHO_BANCA ) then
        ZBaseDlgModal_DevEx.ShowDlgModal( SistSuscripcionUsuarios_DevEx, TSistSuscripcionUsuarios_DevEx )
     else
         ZetaDialogo.zInformation( Caption, 'No tiene derechos para modificar suscripciones a usuarios.', 0 );
end;

procedure TEnviosProgramados_DevEx.HabilitarControles;
const K_CERO = 0;
var
   lHabilitar : Boolean;
begin
     if ZetaDBGridDBTableView.DataController.DataRowCount = 0 then
          lHabilitar := False
     else
          lHabilitar := True;

     EjecutarEnvio.Enabled := lHabilitar;
     lHabilitar := lHabilitar and (dmSistema.cdsEnviosProgramadosEmpresa.FieldByName ('CA_REPEMP').AsInteger = K_CERO);
     InscribirUsuarios.Enabled := lHabilitar;
     InscribirRoles.Enabled := lHabilitar;
end;

end.


