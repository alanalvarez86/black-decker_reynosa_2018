unit ZArbolCalendarioReportes;

interface

uses Forms,
     SysUtils,
     ComCtrls,
     ImgList,
     DBClient,
     Controls,
     cxTreeView; //DevEx(by am): Agregada para poer utilizar la clase del arbold e DevExpress
     procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean ); overload;
     procedure CrearArbol( oArbol: TcxTreeView; const iPlazaDefault: Integer ); overload;
     procedure AgregaNodos( oArbol: TcxTreeView; oPadre: TTreeNode; const iReportaA: Integer ); overload;

     function CrearNodoHijo( oArbol: TcxTreeView; oNodo: TTreeNode; sMensaje: String; iImageIndex, iOrdenDataSet : integer): TTreeNode;
     function CrearNodosDeCalendario(var oNodoUsuarios, oNodoRoles: TTreeNode; oArbol: TcxTreeView; oNodo: TTreeNode; sMensaje: String; iImageIndex: integer): TTreeNode;
     function CrearNodo( oArbol: TcxTreeView; oNodo: TTreeNode; sMensaje: String; iImageIndex, iOrdenDataSet : integer): TTreeNode;

implementation

uses
    DSistema,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonLists,
    ZetaCommonClasses;

const
     K_IGUAL = ' = ';
     K_IMAGEN_USUARIO = 0;
     K_IMAGEN_ROL = 1;
     K_IMAGEN_CALENDARIO = 2;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure CrearArbol( oArbol: TcxTreeView; const iPlazaDefault: Integer );
var
   oPadre: TTreeNode;

   procedure InitArbol;
   begin
        oArbol.Items.Clear;
   end;

begin
     oArbol.Items.BeginUpdate;
     try
        InitArbol;
        AgregaNodos( oArbol, oPadre, 0 );
     finally
            oArbol.Items.EndUpdate;
     end;
end;

function CrearNodoHijo( oArbol: TcxTreeView; oNodo: TTreeNode; sMensaje: String; iImageIndex, iOrdenDataSet : integer): TTreeNode;
begin
     oNodo := oArbol.Items.AddChild(oNodo, sMensaje);
     if Assigned( oNodo ) then
     begin
          with oNodo do
          begin
               ImageIndex := iImageIndex;
               Data := TObject(iOrdenDataSet);
          end;
     end;
     Result := oNodo;
end;

function CrearNodo( oArbol: TcxTreeView; oNodo: TTreeNode; sMensaje: String; iImageIndex, iOrdenDataSet : integer): TTreeNode;//CREA NODOS INICIALES Y HERMANOS
begin
     oNodo := oArbol.Items.Add(oArbol.Selected, sMensaje);
     if Assigned( oNodo ) then
     begin
          with oNodo do
          begin
               ImageIndex := iImageIndex;
               Data := TObject(iOrdenDataSet);
          end;
     end;
     Result := oNodo;
end;

function CrearNodosDeCalendario(var oNodoUsuarios, oNodoRoles: TTreeNode; oArbol: TcxTreeView; oNodo: TTreeNode; sMensaje: String; iImageIndex : integer): TTreeNode;
begin
       oNodoUsuarios := CrearNodoHijo( oArbol, oNodo,  'Usuarios', K_IMAGEN_USUARIO , 0);
       oNodoRoles := CrearNodoHijo( oArbol, oNodo,  'Roles', K_IMAGEN_ROL , 0);
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure AgregaNodos( oArbol: TcxTreeView; oPadre: TTreeNode; const iReportaA: Integer );
const
    COUNT_CALENDARIO_REGISTROS = 3;
var
   oNodo, oNodoRol: TTreeNode;
   iFolioNext, iFolioAnterior : integer;
   iCantidadRegistros : integer;
   sRolCodigo, sRolCodigoAnterior : string;
   iUSCodigo : integer;
   iPrimeroRama : integer;
   oNodoUsuarios, oNodoRoles : TTreeNode;
   sTarea, sTareaDescrip, sFrecuencia, sHora, sEmail : string;
   iOrdenDataSet : integer;
   iContadorRegistros : integer;
   sDatosUsuario : string;

begin
     iFolioNext := 0;
     iFolioAnterior := 0;
     iPrimeroRama := 0;
     iOrdenDataSet := 0;
     sRolCodigoAnterior := VACIO;
     iCantidadRegistros :=  dmSistema.cdsSuscripCalendarioReportes.RecordCount;
     iContadorRegistros := 0;

     if iCantidadRegistros > 0 then
     begin
           while(iCantidadRegistros <> 0) do
           begin
                 with dmSistema.cdsSuscripCalendarioReportes do
                 begin
                      iFolioNext := FieldByName( 'CA_FOLIO' ).AsInteger;
                      sRolCodigo := FieldByName( 'RO_CODIGO' ).AsString;
                      iUSCodigo := FieldByName( 'US_CODIGO' ).AsInteger;

                      //dmSistema.cdsSuscripCalendarioReportes.ListaFija();

                      sTarea := FieldByName( 'CA_NOMBRE' ).AsString;
                      sTareaDescrip := FieldByName( 'CA_DESCRIP' ).AsString;
                      sFrecuencia := ZetaCommonLists.ObtieneElemento(lfEmailFrecuencia, FieldByName( 'CA_FREC' ).AsInteger);
                      sHora := FieldByName( 'CA_HORA' ).AsString;
                      sEmail := FieldByName( 'US_EMAIL' ).AsString;
                      iOrdenDataSet := FieldByName( 'ORDEN' ).AsInteger;

                      if sEmail = VACIO then
                      begin
                           sDatosUsuario := FieldByName( 'US_CODIGO' ).AsString + K_IGUAL + FieldByName( 'US_NOMBRE' ).AsString;
                      end
                      else
                      begin
                           sDatosUsuario := FieldByName( 'US_CODIGO' ).AsString + K_IGUAL + FieldByName( 'US_NOMBRE' ).AsString + ' - Email: '+sEmail;
                      end;

                      if (iFolioNext > 0) and (iFolioNext <> iFolioAnterior) then
                      begin
                           //REGISTRO CALENDARIO - NODO PRINCIPAL
                           oNodo := CrearNodo( oArbol, oNodo,sTarea+' - '+sFrecuencia , K_IMAGEN_CALENDARIO , iOrdenDataSet);

                           //if PrimerRegistroCalendario and tienes suscriptos then CREAR RAMAS DE USUARIO Y ROLES
                           dmSistema.EliminarUsuariosRolesCalendario(COUNT_CALENDARIO_REGISTROS, iFolioNext, iUsCodigo, VACIO, iContadorRegistros);
                           if iContadorRegistros > 0 then
                           begin
                                CrearNodosDeCalendario( oNodoUsuarios, oNodoRoles, oArbol, oNodo,  FieldByName( 'CA_FOLIO' ).AsString + K_IGUAL + FieldByName( 'CA_NOMBRE' ).AsString, K_IMAGEN_USUARIO );
                           end;

                           if (sRolCodigo <> VACIO) then //RAMA ROLES
                           begin
                                if sRolCodigo <>  sRolCodigoAnterior then
                                begin
                                     oNodoRol := oNodo;
                                     oNodoRol := CrearNodoHijo( oArbol, oNodoRoles,  FieldByName( 'RO_NOMBRE' ).AsString, K_IMAGEN_ROL , iOrdenDataSet);
                                     if (iUSCodigo <> 0) then
                                     begin
                                          CrearNodoHijo( oArbol, oNodoRol,  sDatosUsuario, K_IMAGEN_ROL , iOrdenDataSet);
                                     end;
                                end
                                else
                                begin
                                     if (iUSCodigo <> 0) then
                                     begin
                                          CrearNodoHijo( oArbol, oNodoRol,  sDatosUsuario, K_IMAGEN_ROL , iOrdenDataSet);
                                     end;
                                end;

                           end //RAMA USUARIOS
                           else if (iUSCodigo <> 0) then
                           begin
                                oNodo := CrearNodoHijo( oArbol, oNodoUsuarios,  sDatosUsuario, K_IMAGEN_USUARIO , iOrdenDataSet);
                           end;
                      end
                      else
                      begin
                           if (sRolCodigo <> VACIO) then //RAMA ROLES
                           begin
                                if sRolCodigo <>  sRolCodigoAnterior then
                                begin
                                     oNodoRol := oNodo;
                                     oNodoRol := CrearNodoHijo( oArbol, oNodoRoles,  FieldByName( 'RO_NOMBRE' ).AsString, K_IMAGEN_ROL , iOrdenDataSet);
                                     if (iUSCodigo <> 0) then
                                     begin
                                          CrearNodoHijo( oArbol, oNodoRol,  sDatosUsuario, K_IMAGEN_USUARIO , iOrdenDataSet);
                                     end;
                                end
                                else
                                begin
                                     if (iUSCodigo <> 0) then
                                     begin
                                          CrearNodoHijo( oArbol, oNodoRol,  sDatosUsuario, K_IMAGEN_USUARIO , iOrdenDataSet);
                                     end;
                                end;

                           end
                           else if (iUSCodigo <> 0) then //RAMA USUARIOS
                           begin
                                oNodo := CrearNodoHijo( oArbol, oNodoUsuarios,  sDatosUsuario, K_IMAGEN_USUARIO , iOrdenDataSet);
                           end;
                      end;

                      iFolioAnterior := dmSistema.cdsSuscripCalendarioReportes.FieldByName( 'CA_FOLIO' ).AsInteger;
                      sRolCodigoAnterior := dmSistema.cdsSuscripCalendarioReportes.FieldByName( 'RO_CODIGO' ).AsString;
                      Next;
                      iCantidadRegistros := iCantidadRegistros - 1;
                 end;
           end;
     end;

end;

procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean ); overload;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             if lAccion then
                FullCollapse
             else
                 FullExpand;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
