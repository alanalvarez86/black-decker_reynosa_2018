inherited WizConteo: TWizConteo
  Left = 382
  Top = 163
  Caption = 'Agregar Presupuestos'
  ClientHeight = 268
  ClientWidth = 422
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 232
    Width = 422
    BeforeMove = WizardBeforeMove
    inherited Anterior: TZetaWizardButton
      Left = 14
    end
    inherited Siguiente: TZetaWizardButton
      Left = 93
      Enabled = True
    end
    inherited Salir: TZetaWizardButton
      Left = 328
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 245
      Enabled = False
    end
  end
  inherited PageControl: TPageControl
    Width = 422
    Height = 232
    ActivePage = Detalle
    inherited Parametros: TTabSheet
      Caption = ''
      object PanelEspacio: TPanel
        Left = 0
        Top = 0
        Width = 414
        Height = 17
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
      end
      object Panel1: TPanel
        Tag = 1
        Left = 0
        Top = 17
        Width = 414
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object LblNivel1: TLabel
          Left = 22
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Nivel1:'
        end
        object NIVEL1: TZetaKeyLookup
          Left = 20
          Top = 12
          Width = 285
          Height = 21
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
      end
      object Panel4: TPanel
        Tag = 4
        Left = 0
        Top = 140
        Width = 414
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object LblNivel4: TLabel
          Left = 112
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Nivel4:'
        end
        object NIVEL4: TZetaKeyLookup
          Left = 110
          Top = 12
          Width = 285
          Height = 21
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
        object Panel4b: TPanel
          Left = 0
          Top = 0
          Width = 100
          Height = 41
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Bevel1: TBevel
            Left = 85
            Top = -30
            Width = 50
            Height = 50
          end
        end
      end
      object Panel2: TPanel
        Tag = 2
        Left = 0
        Top = 58
        Width = 414
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object LblNivel2: TLabel
          Left = 52
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Nivel2:'
        end
        object NIVEL2: TZetaKeyLookup
          Left = 50
          Top = 12
          Width = 285
          Height = 21
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
        object Panel2b: TPanel
          Left = 0
          Top = 0
          Width = 40
          Height = 41
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Bevel2: TBevel
            Left = 25
            Top = -30
            Width = 50
            Height = 50
          end
        end
      end
      object Panel3: TPanel
        Tag = 3
        Left = 0
        Top = 99
        Width = 414
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object LblNivel3: TLabel
          Left = 82
          Top = 0
          Width = 33
          Height = 13
          Caption = 'Nivel3:'
        end
        object NIVEL3: TZetaKeyLookup
          Left = 80
          Top = 12
          Width = 285
          Height = 21
          TabOrder = 0
          TabStop = True
          WidthLlave = 75
        end
        object Panel3b: TPanel
          Left = 0
          Top = 0
          Width = 70
          Height = 41
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Bevel3: TBevel
            Left = 55
            Top = -30
            Width = 50
            Height = 50
          end
        end
      end
    end
    object Detalle: TTabSheet [1]
      Caption = 'Detalle'
      ImageIndex = 2
      TabVisible = False
      object Prender: TBitBtn
        Tag = 1
        Left = 96
        Top = 196
        Width = 107
        Height = 25
        Caption = '&Prender Todos'
        TabOrder = 0
        OnClick = PrenderClick
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          7777700000007777777777777777700000007777777774F77777700000007777
          7777444F77777000000077777774444F777770000000700000444F44F7777000
          000070FFF444F0744F777000000070F8884FF0774F777000000070FFFFFFF077
          74F77000000070F88888F077774F7000000070FFFFFFF0777774F000000070F8
          8777F07777774000000070FFFF00007777777000000070F88707077777777000
          000070FFFF007777777770000000700000077777777770000000777777777777
          777770000000}
      end
      object Apagar: TBitBtn
        Left = 209
        Top = 196
        Width = 107
        Height = 25
        Caption = '&Apagar Todos'
        TabOrder = 1
        OnClick = PrenderClick
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
          77777777777777777777777700000000007777770FFFFFFFF07777770FFFFFFF
          F077771F0F888888F077711F0F85BFB8F0777711F11BFBF8F077777151788888
          F077777511FFFFFFF07775111F1FFF00007771570FF1FF0F077777770FFFFF00
          7777777700000007777777777777777777777777777777777777}
      end
      object GroupPresupuesto: TGroupBox
        Left = 0
        Top = 0
        Width = 414
        Height = 192
        Align = alTop
        Caption = ' GroupPresupuesto '
        TabOrder = 2
        object Presupuesto: TCheckListBox
          Left = 2
          Top = 15
          Width = 410
          Height = 175
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 414
        Lines.Strings = (
          ''
          ''
          ''
          'Se Agregarán los Presupuestos Indicados')
      end
      inherited ProgressPanel: TPanel
        Top = 155
        Width = 414
        inherited ProgressBar: TProgressBar
          Left = 28
          Visible = True
        end
      end
    end
  end
end
