unit ZArbolOrganigramaTools;

interface

uses Forms,
     SysUtils,
     ComCtrls,
     ImgList,
     DBClient,
     Controls,
     cxTreeView; //DevEx(by am): Agregada para poer utilizar la clase del arbold e DevExpress

//function GetNodo( oArbol: TTreeView; const sPlaza: String ): TTreeNode;
//function ObtenNodoSeleccionado( oArbol: TTreeview ): string;
//function NodoTieneEmpleado( DataSet: TClientDataset; oNodo, oHermano: TTreeNode ): Boolean;
function GetNodoPlaza( oArbol: TTreeView; const iPlaza: Integer ): TTreeNode; overload;
function GetNodoPlaza( oArbol: TcxTreeView; const iPlaza: Integer ): TTreeNode; overload;
function GetTextoNodo( const oNodo: TTreeNode ): String;
function GetPlazaNodo( const oNodo: TTreeNode ): Integer;
function ObtenPlazaSeleccionada( oArbol: TTreeview ): Integer; overload;
function ObtenPlazaSeleccionada( oArbol: TcxTreeview ): Integer; overload;
function UbicaPlaza(  oArbol: TTreeView; const iPlaza: Integer ): Boolean; overload;
function UbicaPlaza(  oArbol: TcxTreeView; const iPlaza: Integer ): Boolean; overload;
function NodoTieneTitular( oNodo: TTreeNode ): Boolean; 
//function CustomSortNodo( Node1: TTreeNode; Node2: TTreeNode; Data: Longint ): Integer;
//function CustomSortNodo( Node1, Node2: TTreeNode; Data: Integer ): Integer;
//procedure SetImage( const oNodo: TTreeNode; const ImageList: TImageList; const iImageIndex: integer; iImageSelected: integer = 0 );
//procedure CreaArbol( oArbol: TTreeView; const ImageList: TImageList; const iPlazaDefault: Integer );
//procedure BorraRama( DataSet: TClientDataset; oNodo, oHermano: TTreeNode );
procedure CrearArbol( oArbol: TTreeView; const iPlazaDefault: Integer ); overload;
procedure CrearArbol( oArbol: TcxTreeView; const iPlazaDefault: Integer ); overload;
procedure AgregaNodos( oArbol: TTreeView; oPadre: TTreeNode; const iReportaA: Integer ); overload;
procedure AgregaNodos( oArbol: TcxTreeView; oPadre: TTreeNode; const iReportaA: Integer ); overload;
procedure AgregaNuevosNodosSubordinados( oArbol: TTreeView; oPadre: TTreeNode ); overload;
procedure AgregaNuevosNodosSubordinados( oArbol: TcxTreeView; oPadre: TTreeNode ); overload;
procedure CambiaArbol( oArbol: TTreeView; const lAccion: boolean ); overload;
procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean ); overload;
procedure BorraNodo( oArbol: TTreeView; oNodo: TTreeNode ); overload;
procedure BorraNodo( oArbol: TcxTreeView; oNodo: TTreeNode ); overload;
procedure GetNodosPadres( var Resultado: string; var oNodo: TTreeNode; const oHermano: TTreeNode; var lMaximo: Boolean );
procedure GetNodosHijos( var Resultado: string; oNodo,oHermano: TTreeNode);
procedure ObtenTotalesNodo( oNodoActual: TTreeNode; var iTotPlazas, iTotOcupadas, iTotVacantes: Integer );
procedure MoverNodoArbol( oArbol: TTreeView; oNodoOrigen, oNodoDestino: TTreeNode ); overload;
procedure MoverNodoArbol( oArbol: TcxTreeView; oNodoOrigen, oNodoDestino: TTreeNode ); overload;
function UbicaEmpleado( oArbol: TTreeView; const iEmpleado: Integer ): Boolean; overload;
function UbicaEmpleado( oArbol: TcxTreeView; const iEmpleado: Integer ): Boolean; overload;

implementation

uses
    DCliente,
    DCatalogos,
    dRecursos,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses;

const
     K_IGUAL = ' = ';
     K_IMAGEN_EMPRESA = 0;
     K_IMAGEN_PUESTO = 1;
     K_IMAGEN_PUESTO_SELECCIONADO = 2;
     K_FILTRO_REPORTA = '( PL_REPORTA = %d )';

procedure CrearArbol( oArbol: TTreeView; const iPlazaDefault: Integer );
var
   oPadre: TTreeNode;

   procedure InitArbol;
   begin
        oArbol.Items.Clear;
        oPadre := oArbol.Items.AddFirst( NIL, dmCliente.GetDatosEmpresaActiva.Nombre );
        with oPadre do
        begin
             ImageIndex := K_IMAGEN_EMPRESA;
             SelectedIndex := K_IMAGEN_EMPRESA;
        end;
   end;

begin
     oArbol.Items.BeginUpdate;
     try
        InitArbol;
        AgregaNodos( oArbol, oPadre, 0 );
     finally
            with dmRecursos.cdsArbolPlazas do
            begin
                 Filtered := FALSE;
                 Filter := VACIO;
            end;
            if ( iPlazaDefault = 0 ) or ( not UbicaPlaza( oArbol, iPlazaDefault ) ) then
               oArbol.Selected := oArbol.Items[0];
            oArbol.Items.EndUpdate;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure CrearArbol( oArbol: TcxTreeView; const iPlazaDefault: Integer );
var
   oPadre: TTreeNode;

   procedure InitArbol;
   begin
        oArbol.Items.Clear;
        oPadre := oArbol.Items.AddFirst( NIL, dmCliente.GetDatosEmpresaActiva.Nombre );
        with oPadre do
        begin
             ImageIndex := K_IMAGEN_EMPRESA;
             SelectedIndex := K_IMAGEN_EMPRESA;
        end;
   end;

begin
     oArbol.Items.BeginUpdate;
     try
        InitArbol;
        AgregaNodos( oArbol, oPadre, 0 );
     finally
            with dmRecursos.cdsArbolPlazas do
            begin
                 Filtered := FALSE;
                 Filter := VACIO;
            end;
            if ( iPlazaDefault = 0 ) or ( not UbicaPlaza( oArbol, iPlazaDefault ) ) then
               oArbol.Selected := oArbol.Items[0];
            oArbol.Items.EndUpdate;
     end;
end;

procedure AgregaNodos( oArbol: TTreeView; oPadre: TTreeNode; const iReportaA: Integer );

var
   oNodo: TTreeNode;
begin
     with dmRecursos.cdsArbolPlazas do
     begin
          Filter := Format( K_FILTRO_REPORTA, [ iReportaA ] );
          Filtered := TRUE;
          First;
          while ( not EOF ) do
          begin
               oNodo := oArbol.Items.AddChild( oPadre, FieldByName( 'PL_FOLIO' ).AsString + K_IGUAL + FieldByName( 'PL_NOMBRE' ).AsString );
               if Assigned( oNodo ) then
               begin
                    with oNodo do
                    begin
                         ImageIndex := K_IMAGEN_PUESTO;
                         SelectedIndex := K_IMAGEN_PUESTO_SELECCIONADO;
                         Data := TObject( FieldByName( 'CB_CODIGO' ).AsInteger );
                    end;
               end;
               Delete;   // Ya lo agreg� y se quita del dataset para ir bajando el tama�o
          end;
          if ( oPadre.HasChildren ) then
          begin
               oNodo := oPadre.GetFirstChild;
               while ( oNodo <> nil ) do
               begin
                    AgregaNodos( oArbol, oNodo, GetPlazaNodo( oNodo ) );
                    oNodo := oNodo.GetNextSibling;
               end;
          end;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure AgregaNodos( oArbol: TcxTreeView; oPadre: TTreeNode; const iReportaA: Integer );

var
   oNodo: TTreeNode;
begin
     with dmRecursos.cdsArbolPlazas do
     begin
          Filter := Format( K_FILTRO_REPORTA, [ iReportaA ] );
          Filtered := TRUE;
          First;
          while ( not EOF ) do
          begin
               oNodo := oArbol.Items.AddChild( oPadre, FieldByName( 'PL_FOLIO' ).AsString + K_IGUAL + FieldByName( 'PL_NOMBRE' ).AsString );
               if Assigned( oNodo ) then
               begin
                    with oNodo do
                    begin
                         ImageIndex := K_IMAGEN_PUESTO;
                         SelectedIndex := K_IMAGEN_PUESTO_SELECCIONADO;
                         Data := TObject( FieldByName( 'CB_CODIGO' ).AsInteger );
                    end;
               end;
               Delete;   // Ya lo agreg� y se quita del dataset para ir bajando el tama�o
          end;
          if ( oPadre.HasChildren ) then
          begin
               oNodo := oPadre.GetFirstChild;
               while ( oNodo <> nil ) do
               begin
                    AgregaNodos( oArbol, oNodo, GetPlazaNodo( oNodo ) );
                    oNodo := oNodo.GetNextSibling;
               end;
          end;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure AgregaNuevosNodosSubordinados( oArbol: TcxTreeView; oPadre: TTreeNode );
var
   oNodo: TTreeNode;
   iPlaza: Integer;
begin
     oArbol.Items.BeginUpdate;
     try
        with dmRecursos.cdsArbolPlazas do
        begin
             First;
             while ( not EOF ) do
             begin
                  iPlaza := FieldByName( 'PL_FOLIO' ).AsInteger;
                  oNodo := GetNodoPlaza( oArbol, iPlaza );
                  if ( not Assigned( oNodo ) ) then    // Solo Agrega nuevos nodos
                  begin
                       oNodo := oArbol.Items.AddChild( oPadre, FieldByName( 'PL_FOLIO' ).AsString + K_IGUAL + FieldByName( 'PL_NOMBRE' ).AsString );
                       if Assigned( oNodo ) then
                       begin
                            with oNodo do
                            begin
                                 ImageIndex := K_IMAGEN_PUESTO;
                                 SelectedIndex := K_IMAGEN_PUESTO_SELECCIONADO;
                                 Data := TObject( FieldByName( 'CB_CODIGO' ).AsInteger );
                            end;
                       end;
                  end;
                  Next;
             end;
        end;
     finally
            with oPadre do
            begin
                 CustomSort( nil, 0 );
                 Expand( TRUE );
            end;
            oArbol.Items.EndUpdate;
     end;
end;

procedure AgregaNuevosNodosSubordinados( oArbol: TTreeView; oPadre: TTreeNode ); overload;
var
   oNodo: TTreeNode;
   iPlaza: Integer;
begin
     oArbol.Items.BeginUpdate;
     try
        with dmRecursos.cdsArbolPlazas do
        begin
             First;
             while ( not EOF ) do
             begin
                  iPlaza := FieldByName( 'PL_FOLIO' ).AsInteger;
                  oNodo := GetNodoPlaza( oArbol, iPlaza );
                  if ( not Assigned( oNodo ) ) then    // Solo Agrega nuevos nodos
                  begin
                       oNodo := oArbol.Items.AddChild( oPadre, FieldByName( 'PL_FOLIO' ).AsString + K_IGUAL + FieldByName( 'PL_NOMBRE' ).AsString );
                       if Assigned( oNodo ) then
                       begin
                            with oNodo do
                            begin
                                 ImageIndex := K_IMAGEN_PUESTO;
                                 SelectedIndex := K_IMAGEN_PUESTO_SELECCIONADO;
                                 Data := TObject( FieldByName( 'CB_CODIGO' ).AsInteger );
                            end;
                       end;
                  end;
                  Next;
             end;
        end;
     finally
            with oPadre do
            begin
                 CustomSort( nil, 0 );
                 Expand( TRUE );
            end;
            oArbol.Items.EndUpdate;
     end;
end;

procedure BorraNodo( oArbol: TTreeView; oNodo: TTreeNode );
var
   oPadre, oHermano: TTreeNode;
begin
     oArbol.Items.BeginUpdate;
     oPadre := oNodo.Parent;
     oHermano := oNodo.GetNextSibling;
     if ( oHermano = nil ) then
        oHermano := oNodo.GetPrevSibling;
     if ( oHermano = nil ) then
        oHermano := oPadre;               // Si no hay hermanos se colocar� sobre el padre
     try
        oNodo.Delete;
     finally
            oPadre.CustomSort( nil, 0 );
            with oArbol do
            begin
                 Selected := oHermano;
                 Items.EndUpdate;
            end;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure BorraNodo( oArbol: TcxTreeView; oNodo: TTreeNode ); overload;
var
   oPadre, oHermano: TTreeNode;
begin
     oArbol.Items.BeginUpdate;
     oPadre := oNodo.Parent;
     oHermano := oNodo.GetNextSibling;
     if ( oHermano = nil ) then
        oHermano := oNodo.GetPrevSibling;
     if ( oHermano = nil ) then
        oHermano := oPadre;               // Si no hay hermanos se colocar� sobre el padre
     try
        oNodo.Delete;
     finally
            oPadre.CustomSort( nil, 0 );
            with oArbol do
            begin
                 Selected := oHermano;
                 Items.EndUpdate;
            end;
     end;
end;

function ObtenPlazaSeleccionada( oArbol: TTreeview ): Integer;
var
   oNodo: TTreeNode;
begin
     oNodo := oArbol.Selected;
     if ( oNodo <> nil ) then
        Result := StrToIntDef( GetTextoNodo( oNodo ), 0 )
     else
         Result := 0;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
function ObtenPlazaSeleccionada( oArbol: TcxTreeview ): Integer;
var
   oNodo: TTreeNode;
begin
     oNodo := oArbol.Selected;
     if ( oNodo <> nil ) then
        Result := StrToIntDef( GetTextoNodo( oNodo ), 0 )
     else
         Result := 0;
end;

function GetTextoNodo( const oNodo: TTreeNode ): String;
var
   sTemporal: string;
begin
     if Assigned( oNodo ) then
     begin
          sTemporal := oNodo.Text;
          Result := copy( sTemporal, 1, pos( K_IGUAL, sTemporal ) - 1 );
     end
     else Result := VACIO;
end;

function GetPlazaNodo( const oNodo: TTreeNode ): Integer;
begin
     Result := StrToIntDef( GetTextoNodo( oNodo ), 0 );
end;

function GetNodoPlaza( oArbol: TTreeView; const iPlaza: Integer ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;
begin
     Result := nil;
     oNodo  := oArbol.Items[ 0 ];
     lSalir := FALSE;
     repeat
           if ( iPlaza = GetPlazaNodo( oNodo ) ) then
           begin
                Result := oNodo;
                lSalir := TRUE;
           end;
           oNodo := oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;
end;
//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
function GetNodoPlaza( oArbol: TcxTreeView; const iPlaza: Integer ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;
begin
     Result := nil;
     oNodo  := oArbol.Items[ 0 ];
     lSalir := FALSE;
     repeat
           if ( iPlaza = GetPlazaNodo( oNodo ) ) then
           begin
                Result := oNodo;
                lSalir := TRUE;
           end;
           oNodo := oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;
end;

function UbicaPlaza( oArbol: TTreeView; const iPlaza: Integer): boolean;
var
   oNodo: TTreeNode;
begin
     Result := ( oArbol.Items.count > 0 );
     if Result then
     begin
          oNodo := oArbol.Selected;
          Result := Assigned( oNodo ) and ( Integer( oNodo.Data ) = iPlaza );  // Se optimiza si ya est� posicionado el arbol
          if ( not Result ) then
          begin
               with oArbol.Items do
               begin
                    BeginUpdate;
               end;
               try
                  oNodo := GetNodoPlaza( oArbol, iPlaza );
                  Result := ( oNodo <> nil );
                  if ( Result ) then
                  begin
                       oArbol.Selected := oNodo;
                  end;
               finally
                      oArbol.Items.EndUpdate;
               end;
          end;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
function UbicaPlaza(  oArbol: TcxTreeView; const iPlaza: Integer ): Boolean;
var
   oNodo: TTreeNode;
begin
     Result := ( oArbol.Items.count > 0 );
     if Result then
     begin
          oNodo := oArbol.Selected;
          Result := Assigned( oNodo ) and ( Integer( oNodo.Data ) = iPlaza );  // Se optimiza si ya est� posicionado el arbol
          if ( not Result ) then
          begin
               with oArbol.Items do
               begin
                    BeginUpdate;
               end;
               try
                  oNodo := GetNodoPlaza( oArbol, iPlaza );
                  Result := ( oNodo <> nil );
                  if ( Result ) then
                  begin
                       oArbol.Selected := oNodo;
                  end;
               finally
                      oArbol.Items.EndUpdate;
               end;
          end;
     end;
end;

function UbicaEmpleado( oArbol: TTreeView; const iEmpleado: Integer ): Boolean;
var
   oNodo: TTreeNode;
begin
     Result := False;
     oNodo := oArbol.Items.GetFirstNode;
     while ( Assigned ( oNodo ) ) and ( Not Result) do
     begin
          Result := Integer(oNodo.Data ) = iEmpleado;
          if not Result then
             oNodo := oNodo.GetNext;
     end;
     if Result then
     begin
          with oArbol.Items do
          begin
               BeginUpdate;
               try
                  if oNodo <> nil then
                     oArbol.Selected := oNodo;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
function UbicaEmpleado( oArbol: TcxTreeView; const iEmpleado: Integer ): Boolean; overload;
var
   oNodo: TTreeNode;
begin
     Result := False;
     oNodo := oArbol.Items.GetFirstNode;
     while ( Assigned ( oNodo ) ) and ( Not Result) do
     begin
          Result := Integer(oNodo.Data ) = iEmpleado;
          if not Result then
             oNodo := oNodo.GetNext;
     end;
     if Result then
     begin
          with oArbol.Items do
          begin
               BeginUpdate;
               try
                  if oNodo <> nil then
                     oArbol.Selected := oNodo;
               finally
                      EndUpdate;
               end;
          end;
     end;
end;


procedure ObtenTotalesNodo( oNodoActual: TTreeNode; var iTotPlazas, iTotOcupadas, iTotVacantes: Integer );
var
   oNodo: TTreeNode;
begin
     iTotPlazas := 0;
     iTotOcupadas := 0;
     iTotVacantes := 0;
     if oNodoActual.HasChildren then
     begin
          oNodo := oNodoActual.GetFirstChild;
          while ( Assigned ( oNodo ) ) do
          begin
               if ( oNodo.HasAsParent( oNodoActual ) ) then
               begin
                    Inc( iTotPlazas );
                    if ( Integer( oNodo.Data ) > 0 ) then  // Es una plaza y tiene titular
                       Inc( iTotOcupadas )
                    else
                        Inc( iTotVacantes );
               end;
               oNodo := oNodo.GetNext;
          end;
     end;
end;

procedure MoverNodoArbol( oArbol: TTreeView; oNodoOrigen, oNodoDestino: TTreeNode );
begin
     oArbol.Items.BeginUpdate;
     try
        oNodoOrigen.MoveTo( oNodoDestino, naAddChild );
        with oNodoDestino do
        begin
             CustomSort( nil, 0 );
             Expand( TRUE );
        end;
     finally
            oArbol.Items.EndUpdate;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure MoverNodoArbol( oArbol: TcxTreeView; oNodoOrigen, oNodoDestino: TTreeNode ); overload;
begin
     oArbol.Items.BeginUpdate;
     try
        oNodoOrigen.MoveTo( oNodoDestino, naAddChild );
        with oNodoDestino do
        begin
             CustomSort( nil, 0 );
             Expand( TRUE );
        end;
     finally
            oArbol.Items.EndUpdate;
     end;
end;

procedure CambiaArbol( oArbol: TTreeView; const lAccion: boolean );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             if lAccion then
                FullCollapse
             else
                 FullExpand;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

//DevEx(by am): Se Sobrecarga este metodo para que funcione con el nuevo arbol
procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean ); overload;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             if lAccion then
                FullCollapse
             else
                 FullExpand;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function NodoTieneTitular( oNodo: TTreeNode ): Boolean;
begin
     Result := FALSE;
     if Assigned( oNodo ) then
     begin
          Result := ( Integer( oNodo.Data ) > 0 );
          if ( not Result ) then
          begin
               if oNodo.HasChildren then
               begin
                    oNodo := oNodo.GetFirstChild;
                    repeat
                          Result := NodoTieneTitular( oNodo );
                          oNodo := oNodo.GetNextSibling;
                    until ( oNodo = nil ) or Result;
               end;
          end;
     end;
end;

procedure GetNodosHijos( var Resultado: string; oNodo, oHermano: TTreeNode );
begin
     if ( oNodo <> nil ) then
     begin
          repeat
                if oNodo.HasChildren then
                begin
                     GetNodosHijos( Resultado, oNodo.GetFirstChild, nil );
                end;
                Resultado := ConcatString( Resultado, GetTextoNodo(oNodo), ',' );
                oNodo := oNodo.GetNextSibling;
          until ( oNodo = nil ) or (oNodo = oHermano);
     end;
{
     else
     begin
          Resultado := ConcatString( Resultado, '0', ',' );
     end;
}
end;

procedure GetNodosPadres( var Resultado: string; var oNodo: TTreeNode; const oHermano: TTreeNode; var lMaximo: Boolean );
const
     K_LIMITE_OBJETOS_FB = 1000;
var
   i: Integer;
   oPrimerHijo: TTreeNode;
begin
     if ( oNodo <> nil ) then
     begin
          i:= 0;
          repeat
                if oNodo.HasChildren then
                begin
                     Resultado := ConcatString( Resultado, GetTextoNodo(oNodo), ',' );
                     oPrimerHijo:= oNodo.GetFirstChild;
                     GetNodosPadres( Resultado, oPrimerHijo, nil, lMaximo );
                     if ( lMaximo ) then
                     begin
                          oNodo:= oPrimerHijo;
                     end;
                     Inc(i);
                end;
                oNodo := oNodo.GetNextSibling;
                lMaximo:= ( i = K_LIMITE_OBJETOS_FB );
          until ( oNodo = nil ) or (oNodo = oHermano) or (lMaximo);
     end;
end;

{
function CustomSortNodo( Node1: TTreeNode; Node2: TTreeNode; Data: Longint ): Integer;
var
   iPlaza1, iPlaza2: Integer;
begin
     iPlaza1 := GetPlazaNodo( TTreeNode( Node1 ) );
     iPlaza2 := GetPlazaNodo( TTreeNode( Node2 ) );
     if Data = 0 then
       Result := ( iPlaza1 - iPlaza2 )
     else
       Result := -( iPlaza1 - iPlaza2 );
end;
}

{
procedure CreaArbol( oArbol: TTreeView; const ImageList: TImageList; const iPlazaDefault: Integer );
const
     K_MAX_NIVEL = 100;
var
   oPadre, oHijo, oNodoDefault: TTreeNode;
   i, iContador, iCount: integer;
   lOk: boolean;
   sTempIndice : string;
   aVerifica: Array of integer;

   procedure InitArbol;
   begin
        oPadre := oArbol.Items.AddFirst( NIL, dmCliente.GetDatosEmpresaActiva.Nombre );
        SetImage( oPadre, ImageList,  K_IMAGEN_EMPRESA );
        iContador := 0;
        oNodoDefault := oPadre;
   end;

begin
     iCount := 0;
     with oArbol.Items do
     begin
          BeginUpdate;
          Clear;
     end;
     try
        InitArbol;
        with dmRecursos.cdsPlazas do
        begin
             DisableControls;
             sTempIndice := IndexFieldNames;
             try
                IndexFieldNames :=  'PL_FOLIO;PL_NOMBRE';
                First;
                SetLength( aVerifica, RecordCount );
                // Se llena el Arreglo de Veficaci�n
                for i := 0 to ( RecordCount - 1 ) do
                    aVerifica[ i ] := 0;
                Repeat
                      lOk := True;
                      iCount := 0;
                      while not EOF do
                      begin
                           if ( aVerifica[ iCount ] <> 1 ) then
                           begin
                                oPadre := GetNodo( oArbol, FormatFloat( '0;0;#', FieldByName( 'PL_REPORTA' ).AsInteger ) );
                                if ( oPadre = nil ) then
                                   lOk := False
                                else
                                begin
                                     oHijo := oArbol.Items.AddChild( oPadre, IntToStr( FieldByName( 'PL_FOLIO' ).AsInteger ) + K_IGUAL + FieldByName( 'PL_NOMBRE' ).AsString );
                                     if ( iPlazaDefault = FieldByName( 'PL_FOLIO' ).AsInteger ) then
                                        oNodoDefault := oHijo;
                                     SetImage( oHijo, ImageList, K_IMAGEN_PUESTO, K_IMAGEN_PUESTO_SELECCIONADO );
                                     oHijo.Data := TObject( FieldByName( 'CB_CODIGO' ).AsInteger );
                                     aVerifica[ iCount ] := 1;
                                end;
                           end;
                           Inc( iCount );
                           Next;
                      end; //While
                      Inc( iContador );
                      if ( iContador >= K_MAX_NIVEL ) then     //100 Niveles Maximo
                      begin
                           lOk := True;
                           ZetaDialogo.ZError( 'Aviso', '� Existe una Relaci�n Circular En La Definici�n De Las Plazas !', 0 );
                      end;
                      if not lOk then
                         First;
                until ( lOk = True );
             finally
                    EnableControls;
                    IndexFieldNames := sTempIndice;
             end;
        end;
     finally
            with oArbol do
            begin
                 if ( oNodoDefault <> nil ) then
                    Selected := oNodoDefault
                 else if ( iCount > 0 ) then
                    Selected := Items[iCount - ( iCount-1 )];
                 Items.EndUpdate;
            end;
     end;
end;

procedure SetImage( const oNodo: TTreeNode; const ImageList: TImageList; const iImageIndex: integer; iImageSelected: integer = 0 );
begin
     with oNodo do
     begin
          ImageIndex := iImageIndex;
          SelectedIndex := iImageSelected;
     end;
end;

function GetNodo( oArbol: TTreeView; const sPlaza: String ): TTreeNode;
var
   oNodo: TTreeNode;
   lSalir: Boolean;

begin
     Result := nil;

     oNodo  := oArbol.Items[ 0 ];
     lSalir := False;
     repeat
           if ( sPlaza = GetTextoNodo( oNodo ) ) then
           begin
                Result := oNodo;
                lSalir := True;
           end;
           oNodo := oNodo.GetNext;
     until ( oNodo = nil ) or lSalir;
end;

function ObtenNodoSeleccionado( oArbol: TTreeview ): string;
var
   oNodo: TTreeNode;
begin
     oNodo := oArbol.Selected;
     if ( oNodo <> nil ) then
        Result := GetTextoNodo( oNodo )
     else
         Result := VACIO;
end;

//Se busca que NINGUN hijo tenga Titular asignado, con uno que lo tenga, no se puede borrar la rama
function NodoTieneEmpleado(  DataSet: TClientDataset; oNodo, oHermano: TTreeNode ): Boolean;
begin
     Result := FALSE;
     if ( oNodo <> nil ) then
     begin
          repeat
                if DataSet.Locate( 'PL_FOLIO', GetTextoNodo(oNodo), [] ) then
                begin
                     Result := (DataSet.FieldByName('CB_CODIGO').AsInteger <> 0);
                     if Result then
                        Break;
                end;

                if oNodo.HasChildren then
                begin
                     oNodo := oNodo.GetFirstChild;
                     Result := NodoTieneEmpleado( DataSet, oNodo, NIL );
                end;

                oNodo := oNodo.GetNextSibling;
          until ( oNodo = nil ) or (Result = TRUE) or (oNodo = oHermano);
     end;
end;

procedure BorraRama( DataSet: TClientDataset; oNodo, oHermano: TTreeNode );
begin
     if ( oNodo <> nil ) then
     begin
          repeat
                if DataSet.Locate( 'PL_FOLIO', GetTextoNodo(oNodo), [] ) then
                   DataSet.Delete;

                if oNodo.HasChildren then
                begin
                     BorraRama( DataSet, oNodo.GetFirstChild, NIL );
                end;

                oNodo := oNodo.GetNextSibling;
          until ( oNodo = nil ) or (oNodo = oHermano);
     end;
end;
}

end.
