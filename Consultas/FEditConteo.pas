unit FEditConteo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, Buttons, ExtCtrls, ComCtrls, Series, TeEngine,
  TeeProcs, Chart, DBChart, ZetaDBTextBox, Grids, DBGrids, ZBaseEdicion,
  {$ifndef VER130}Variants,{$endif}
  ZetaSmartLists, Db, DBCtrls, ImgList, DbClient, ZetaDBGrid, ZetaMessages;

type
  TEditConteo = class(TBaseEdicion)
    PanelArbol: TPanel;
    Arbol: TTreeView;
    Splitter1: TSplitter;
    RefrescarBtn: TZetaSpeedButton;
    BtnConfigurar: TBitBtn;
    ImageList: TImageList;
    dsDetalle: TDataSource;
    dsTotales: TDataSource;
    Panel1: TPanel;
    PageDetalle: TPageControl;
    Detalle: TTabSheet;
    GridDetalle: TZetaDBGrid;
    Compara: TTabSheet;
    DBChart1: TDBChart;
    Series3: TBarSeries;
    Series4: TBarSeries;
    Real: TTabSheet;
    DBChart3: TDBChart;
    Series2: TPieSeries;
    PanelInferior: TPanel;
    lbTotales: TLabel;
    Label2: TLabel;
    CT_CUANTOS: TZetaDBTextBox;
    Label4: TLabel;
    CT_REAL: TZetaDBTextBox;
    Label3: TLabel;
    CT_EXCEDENTE: TZetaDBTextBox;
    Label6: TLabel;
    CT_VACANTES: TZetaDBTextBox;
    UpBtn: TZetaSpeedButton;
    lbNiveles: TLabel;
    LblNoGraficar: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure RefrescarBtnClick(Sender: TObject);
    procedure BtnConfigurarClick(Sender: TObject);
    procedure ArbolChange(Sender: TObject; Node: TTreeNode);
    procedure DataSourceStateChange(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure PageDetalleChange(Sender: TObject);
    procedure GridDetalleDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ModificarBtnClick(Sender: TObject);
    procedure UpBtnClick(Sender: TObject);
    procedure GridDetalleTitleClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    FRefrescarGraficas : Array[1..2] of Boolean;
    FTitulo : string;
    procedure Refrescar;
    procedure ConstruyeArbol( const lRefresca: Boolean = TRUE );
    function GetFiltroGrupos(Nodo: TTreeNode): String;
    procedure SetBotonesEdicion;
    procedure PosicionaArbol( Llaves: array of string; const lParcialKey: Boolean = FALSE );
    procedure SetRefrescarGraficas(const lEnabled: Boolean);
    procedure RefrescaGrafica(iPos: Integer);
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
    procedure InvocarModificacion;
    procedure SeleccionaSiguienteRenglon;
    procedure PosicionaSiguienteColumna;
  protected
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function ValoresGrid: Variant;override;
    function BotonesActivos: Boolean; override;
    procedure EscribirCambios;override;
    procedure DoCancelChanges; override;
    procedure Imprimir;override;
    procedure Connect; override;
    procedure Disconnect;override;
    procedure Agregar; override;
    procedure Modificar; override;
    procedure Borrar; override;
    procedure HabilitaControles; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
  end;

const
    OFF_SET = 1;

var
  EditConteo: TEditConteo;

implementation

uses dConteo, FEditPresup, FGlobalConteo, ZetaCommonClasses, ZetaCommonTools, ZGlobalTress,
     ZetaDialogo, ZAccesosMgr, FWizConteo, ZAccesosTress, FAutoClasses, ZetaCommonLists;

{$R *.DFM}

procedure TEditConteo.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_CONS_CONTEO_PERSONAL;
     BtnConfigurar.Left := DBNavigator.Left;
     RefrescarBtn.Left := BuscarBtn.Left;
     UpBtn.Left := CortarBtn.Left;
     SetRefrescarGraficas( FALSE );

     DBChart1.Title.Text.Text := '';
     DBChart3.Title.Text.Text := '';

     HelpContext := H50551_Detalle_del_presupuesto;
end;

procedure TEditConteo.FormShow(Sender: TObject);
begin
     inherited;
     PageDetalle.ActivePage := Detalle;
     GridDetalle.ReadOnly :=  not CheckDerecho( D_CONS_CONTEO_PERSONAL, K_DERECHO_CAMBIO );
end;

procedure TEditConteo.Connect;
begin
     with dmConteo do
     begin
          cdsConteo.Conectar;
          DataSource.DataSet:= cdsConteo;
          ConstruyeArbol;
     end;
end;

procedure TEditConteo.Disconnect;
begin
     inherited;
     dmConteo.QuitaFiltros;
end;

procedure TEditConteo.Refrescar;
var
   oCursor: TCursor;
begin
     with Screen do
     begin
          oCursor := Cursor;
          Cursor := crHourglass;
          try
             with dmConteo do
             begin
                  cdsConteo.Refrescar;
                  DataSource.DataSet:= cdsConteo;
             end;
             ConstruyeArbol;
          finally
             Cursor := oCursor;
          end;
     end;
end;

procedure TEditConteo.Agregar;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     with dmConteo.cdsConteo do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
     end;

     with TWizConteo.Create( Application ) do
     begin
          try
             NivelActual := Arbol.Selected.Level + 1;
             if ( NivelActual > 0 ) then
                ShowModal;
             Application.ProcessMessages;
             if Wizard.Ejecutado then
             begin
                  dmConteo.RecalculaTotales;
                  ConstruyeArbol( FALSE );
                  PosicionaArbol( [ Llave1, Llave2, Llave3, Llave4 ] );
                  DataSourceStateChange( self );
             end;
          finally
                 Free;
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TEditConteo.Modificar;
begin
     with dmConteo do
     begin
          with cdsConteo do
               if State in [ dsEdit, dsInsert ] then
                  Post;
{
          RefreshTotales := FALSE;
}
     end;

     ZBaseEdicion.ShowFormaEdicion( EditPresup, TEditPresup );

{     with dmConteo do
          if RefreshTotales then
          begin
               RecalculaTotales;
               ArbolChange( Arbol, Arbol.Selected );   // Filtrar el Detalle
               DataSourceStateChange( self );
          end;
}
end;

procedure TEditConteo.Borrar;
var
   aPosicion : Array[0..3] of string;
   i: integer;
   sFiltro, sNivel : string;
   oCursor: TCursor;
begin
     if dsDetalle.DataSet.FieldByName('CT_REAL').AsInteger > 0 then
        ZInformation( Caption, Format('El Criterio de %s no se Puede Borrar porque Tiene Empleados Activos',[Detalle.Caption]), 0 )
     else if ZConfirm( self.Caption, '� Desea Borrar Este Registro ?', 0, mbOk ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with dmConteo do
             begin
                  cdsConteo.DisableControls;
                  for i:=0 to NumNiveles do
                  begin
                      sNivel := DataSetTotales(Arbol.Selected.Level+1).Fields[i].AsString;
                      if (sNivel <> '') and (i<K_MAX_CONTEO_NIVELES) then
                         sFiltro := sFiltro + ' CT_NIVEL_'+ IntToStr(i+1) + '=' +
                                    EntreComillas(sNivel) + ' AND ' ;
                      if i < Arbol.Selected.Level then
                         aPosicion[i] := sNivel;
                  end;

                  sFiltro := Copy( sFiltro, 1, Length(sFiltro)-4 );

                  with cdsConteo do
                  begin
                       Filter := sFiltro;
                       Filtered := TRUE;

                       while NOT EOF do
                             Delete;

                       Filtered := FALSE;
                  end;

                  if cdsConteo.ChangeCount > 0 then
                  begin
                       DataSourceStateChange( self );
                       RecalculaTotales;
                       ConstruyeArbol( FALSE );
                       PosicionaArbol( aPosicion, TRUE );
                  end;
                  cdsConteo.EnableControls;
             end;
          finally
                 Screen.Cursor:= oCursor;
          end;
     end;
end;

function TEditConteo.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := ( dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name );
          if not Result then
             sMensaje:= Format( 'No se puede Modificar %s', [ Detalle.Caption ] );
     end;
end;

procedure TEditConteo.BtnConfigurarClick(Sender: TObject);
begin
     if dmConteo.GetGlobalConteo then
     begin
          FreeAndNil( EditPresup );
          Refrescar;
     end;
end;

procedure TEditConteo.RefrescarBtnClick(Sender: TObject);
begin
     Refrescar;
end;

{ Metodos del Arbol }

procedure TEditConteo.ConstruyeArbol( const lRefresca: Boolean = TRUE );
var
   oPadre, oHijo : TTreeNode;
   sActual, sOldIndex : String;
   aGrupos : Array of String;
   nCorte, nNivel : Integer;
   cdsGrupos : TClientDataSet;

   procedure InicializaArbol;
   var
       i : Integer;
   begin
        if dmConteo.NumNiveles > 1 then
        begin
             aGrupos := VarArrayCreate( [ 0, dmConteo.NumNiveles-2 ], varOleStr );
             for i := 0 to dmConteo.NumNiveles-2 do
                 aGrupos[ i ] := '';
        end;
             Arbol.Items.BeginUpdate;
             Arbol.Items.Clear;
             oPadre := Arbol.Items.AddChild( NIL, 'Empresa' );

             with oPadre do
             begin
                 ImageIndex := 19;
                 SelectedIndex := 19;
                 Data := Pointer( 0 );
             end;
   end;

   function GrupoCorte : Integer;
   var
      i : Integer;
   begin
       Result := -1;
       for i := 0 to dmConteo.NumNiveles-2 do
       begin
           if ( cdsGrupos.Fields[ i ].AsString <> aGrupos[ i ] ) then
           begin
               Result := i;
               Exit;
           end;
       end;
   end;

begin   // ConstruyeArbol
     InicializaArbol;
     if dmConteo.NumNiveles > 1 then
     begin
          cdsGrupos := dmConteo.cdsConteo;
          with cdsGrupos do
          begin
               DisableControls;
               sOldIndex := IndexFieldNames;
               IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';
               try
                  First;
                  while not Eof do
                  begin
                       nCorte := GrupoCorte;
                       if ( nCorte >= 0 ) then
                       begin
                            while oPadre.Level > nCorte do
                                oPadre := oPadre.Parent;

                            for nNivel := nCorte to dmConteo.NumNiveles-2 do
                            begin
                                 sActual := Fields[ nNivel*Off_SET ].AsString;
                                 aGrupos[ nNivel ] := sActual ;

                                 oHijo := Arbol.Items.AddChild( oPadre, sActual + ' = '+
                                                                FieldByName( 'TB_ELEMENT'+IntToStr(nNivel*Off_SET+1) ).AsString );
                                 with oHijo do
                                 begin
                                      if Level = 1 then
                                      begin
                                           ImageIndex := 14;
                                           SelectedIndex := 15;
                                      end
                                      else
                                      begin
                                           {Las imagenes de colores empiezan en el Index 21}
                                           ImageIndex := 21 + ((Level - 2) *2);
                                           SelectedIndex := 22 + ((Level - 2) *2);
                                      end;
                                      Data := Pointer( RecNo );
                                 end;
                                 oPadre := oHijo;
                            end;
                       end;
                       Next;
                  end;
               finally
                  IndexFieldNames := sOldIndex;
                  EnableControls;
               end;
          end;
     end;
     with Arbol do
     begin
          if lRefresca then
          begin
               Items[ 0 ].Expand( FALSE );
               Selected := Items[ 0 ];
          end;
          Items.EndUpdate;
          // ArbolChange( self, Selected );  No se necesita. Selected lo dispara.
     end;
end;

procedure TEditConteo.ArbolChange(Sender: TObject; Node: TTreeNode);
var
    oTotales, oDataSet : TDataset;
    sCodigo  : String;

    procedure SetTitulos;
    var
       oNode: TTreeNode;
       sNivel, sField : string;
       i: integer;
    begin
         oNode := Node;
         lbNiveles.Caption := '';
         lbTotales.Caption := '';
         sNivel := '';
         FTitulo :='';

         if oNode.Level = 0 then
         begin
              FTitulo := 'Totales de Empresa';
              lbNiveles.Caption := FTitulo;
              lbTotales.Caption := '';
         end
         else if oNode.Level = 1 then
         begin
              FTitulo := 'Totales de ' + dmConteo.GetDescripConteoNiveles( oNode.Level-1 ) + ': '+
                         dmConteo.cdsConteo.FieldByName('CT_NIVEL_1').AsString;
              lbNiveles.Caption := FTitulo;
              lbTotales.Caption := '';
         end
         else
         begin
              with dmConteo, cdsConteo do
              begin
                   for i:= 1 to oNode.Level - 1 do
                   begin
                        sNivel := GetDescripConteoNiveles( i-1) + ': ';
                        sField := FieldByName('CT_NIVEL_'+ IntToStr(i)).AsString;
                        lbNiveles.Caption := lbNiveles.Caption + ' -- ' + sNivel + sField;
                        FTitulo := FTitulo + sNivel + sField  +'='+ FieldByName('TB_ELEMENT'+ IntToStr(i)).AsString + CR_LF;
                   end;
                   sNivel := dmConteo.GetDescripConteoNiveles( oNode.Level -1 ) + ': ';
                   lbNiveles.Caption := lbNiveles.Caption + ' -- ';
                   sField := FieldByName('CT_NIVEL_'+ IntToStr(oNode.Level) ).AsString;
                   lbTotales.Caption := 'Totales de ' + sNivel + sField;
                   FTitulo := FTitulo + sNivel + sField + '='+ FieldByName('TB_ELEMENT'+ IntToStr(oNode.Level)).AsString;
              end;
         end;
    end;

var
   i : integer;
   oCursor : TCursor;
   oParentNodo : TTreeNode;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if Node <> NIL then
        begin
            // Filtra los padres hacia arriba del nodo para recalculo de Totales
            oParentNodo := Node.Parent;
            while ( oParentNodo <> nil ) and ( oParentNodo.Level > 0 ) do
            begin
                 with dmConteo.DataSetTotales( oParentNodo.Level ) do
                 begin
                      Filter := GetFiltroGrupos( oParentNodo );
                      Filtered := TRUE;
                 end;
                 oParentNodo := oParentNodo.Parent;
            end;
            // Filtra Dataset Detalle
            oDataSet := dmConteo.DataSetTotales( Node.Level+1 );
            oDataSet.Filtered := FALSE;
            oDataSet.Filter := GetFiltroGrupos( Node );
            oDataSet.Filtered := TRUE;
            dsDetalle.DataSet := oDataSet;
            with dmConteo do
            begin
                 Detalle.Caption := GetDescripConteoNiveles( Node.Level );
            end;
            // DataSet de Totales
            oTotales := dmConteo.DataSetTotales( Node.Level );
            oTotales.Filter := oDataSet.Filter;
            oTotales.Filtered := TRUE;
            dsTotales.DataSet := oTotales;

            // Columna de Grid
            sCodigo := oDataSet.Fields[ Node.Level ].FieldName;
            with GridDetalle do
            begin
                 Columns[ 0 ].FieldName := sCodigo;
                 Columns[ 1 ].Title.Caption := Detalle.Caption;
                 Columns[ 5 ].FieldName := 'CT_VACANTES';
                 Columns[ 6 ].FieldName := 'CT_EXCEDENTE';
            end;

            for i:=0 to GridDetalle.Columns.Count-1 do
                GridDetalle.Columns[i].ReadOnly := TRUE;

            with dmConteo do
            begin
                 if dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name then
                 begin
                      with GridDetalle do
                      begin
                           Options := GridDetalle.Options - [dgRowSelect] + [dgEditing];
                           Columns[ 2 ].ReadOnly := FALSE;
                           Columns[ 5 ].FieldName := 'VACANTES';
                           Columns[ 6 ].FieldName := 'EXCEDENTE';
                      end;
                 end
                 else
                 begin
                      GridDetalle.Options := GridDetalle.Options + [dgRowSelect] - [dgEditing];
                 end;
            end;
            SetBotonesEdicion;

            // Gr�fica Real
            Series2.DataSource := oDataSet;
            //Series2.XLabelsSource := sCodigo;
            Series2.XLabelsSource := 'TB_ELEMENT';
            Series2.YValues.ValueSource   := 'CT_REAL';
            with Series2.OtherSlice do
            begin
                 Style := poBelowPercent;
                 Text := 'Otros';
                 Value := 5;
            end;

            // Gr�fica Presupuesto vs Real
            Series3.DataSource := oDataSet;
            Series3.XLabelsSource := sCodigo;
            Series3.YValues.ValueSource := 'CT_CUANTOS';

            Series4.DataSource := oDataSet;
            Series4.XLabelsSource := sCodigo;
            Series4.YValues.ValueSource := 'CT_REAL';

            SetTitulos;

            SetRefrescarGraficas( TRUE );
            PageDetalleChange( PageDetalle );

            UpBtn.Enabled := Node.Level > 0 ;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TEditConteo.GetFiltroGrupos(Nodo: TTreeNode): String;
var
   nRecord : Integer;
   i : Integer;
   cdsGrupos : TDataset;
   sIndexField : string;

   function FiltroUnCampo( const nPos : Integer ) : String;
   begin //PENDIENTE SI SE AGRUPA POR TIPO <> STRING
        // Supone que todos los Grupos son tipo String
        with cdsGrupos.Fields[ nPos*Off_SET] do
             if DataType in [ftDate,ftTime,ftDateTime] then
                Result := FieldName  + ' = ' + EntreComillas(FormatDateTime('dd/mm/yyyy',AsDateTime))
             else Result := FieldName  + ' = ' + EntreComillas(AsString);
   end;

begin
     nRecord := Integer( Nodo.Data );
     if ( nRecord = 0 ) then
         Result := ''
     else
     begin
          sIndexField := dmConteo.cdsConteo.IndexFieldNames;
          dmConteo.cdsConteo.IndexFieldNames := 'CT_NIVEL_1;CT_NIVEL_2;CT_NIVEL_3;CT_NIVEL_4;CT_NIVEL_5';

          cdsGrupos := dmConteo.cdsConteo;
          cdsGrupos.Filtered := FALSE;
          cdsGrupos.Recno := nRecord;
          Result := FiltroUnCampo( 0 );
          for i := 1 to Nodo.Level-1 do
              Result := ConcatFiltros(Result, FiltroUnCampo( i ));

          dmConteo.cdsConteo.IndexFieldNames := sIndexField;
     end;
end;

procedure TEditConteo.PosicionaArbol( Llaves: array of string; const lParcialKey: Boolean = FALSE );
var
   oNodo : TTreeNode;
   i: integer;
   oCursor : TCursor;

   function SetParcialKey: Boolean;
   var
      j : Integer;
   begin
        Result := lParcialKey;
        if Result then
        begin
             for j := K_MAX_CONTEO_NIVELES-2 downto 0 do
                 if StrLleno( Llaves[j] ) then
                 begin
                      Llaves[j] := VACIO;
                      Break;
                 end;
             Result := StrLleno( Llaves[0] );
        end;
   end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with Arbol do
        begin
             Items.BeginUpdate;
             FullCollapse;

             Repeat
                   oNodo := Items.GetFirstNode;
                   for i:=0 to K_MAX_CONTEO_NIVELES-2 do
                   begin
                        if Llaves[i] = '' then Break;
                        if oNodo <> NIL then
                           oNodo := oNodo.GetNext;
                        while ( oNodo <> NIL ) and
                              (Copy(oNodo.Text,1,Pos(' = ',oNodo.Text)-1) <> Llaves[i]) do
                              oNodo := oNodo.GetNextSibling;
                   end;
             Until ( oNodo <> nil ) or ( not SetParcialKey );

             if oNodo <> NIL then
                Selected := oNodo
             else
                Selected := Items.GetFirstNode;

             Selected.Expand( FALSE );

             Items.EndUpdate;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditConteo.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     //inherited;    ... No debe hacer nada
end;

procedure TEditConteo.DataSourceStateChange(Sender: TObject);
begin
     //inherited;
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Modo := dsInactive
          else if ( TClientDataSet( DataSet ).ChangeCount > 0 ) then
             Modo := dsEdit
          else
             Modo := Dataset.State;

     end;
     RefrescarBTN.Enabled := Modo = dsBrowse;
end;

procedure TEditConteo.DoCancelChanges;
begin
     inherited;
     dmConteo.RecalculaTotales;
     ConstruyeArbol;
     DataSourceStateChange( self );
end;

procedure TEditConteo.OKClick(Sender: TObject);
var
   aPosicion : Array[0..3] of string;
   i: integer;
   sNivel : string;
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try

        with dmConteo do
          for i:=0 to NumNiveles do
          begin
               sNivel := DataSetTotales(Arbol.Selected.Level+1).Fields[i].AsString;
               if i < Arbol.Selected.Level then
                  aPosicion[i] := sNivel;
          end;

        inherited;

        if ( ClientDataset.ChangeCount = 0 ) then
        begin
             DataSourceStateChange( self );
             ConstruyeArbol;
             PosicionaArbol( aPosicion, TRUE );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditConteo.HabilitaControles;
begin
     inherited;
     AgregarBtn.Enabled := TRUE;
     ImprimirBtn.Enabled := AgregarBtn.Enabled;
     SetBotonesEdicion;
end;

procedure TEditConteo.SetBotonesEdicion;
begin
     BorrarBtn.Enabled := ( dsDetalle.Dataset <> nil ) and ( dsDetalle.Dataset.Active ) and ( dsDetalle.DataSet.RecordCount > 0 );
     ModificarBtn.Enabled := BorrarBtn.Enabled;
end;

procedure TEditConteo.SetRefrescarGraficas( const lEnabled: Boolean );
var
   i : Integer;
begin
     for i := 1 to 3 do
         FRefrescarGraficas[i] := Enabled;
end;

procedure TEditConteo.PageDetalleChange(Sender: TObject);
var
   iPag : Integer;
begin
     inherited;
     iPag := PageDetalle.ActivePageIndex;
     if ( iPag > 0 ) and FRefrescarGraficas[iPag] then
        RefrescaGrafica( iPag );
end;

procedure TEditConteo.RefrescaGrafica( iPos: Integer );
begin
     case iPos of
          1 : DBChart1.RefreshData;
          2 : begin
                   LblNoGraficar.Visible := ( dsTotales.Dataset.FieldByName( 'CT_REAL' ).AsFloat = 0 );
                   DBChart3.Visible := not LblNoGraficar.Visible;
                   if DBChart3.Visible then
                      DBChart3.RefreshData;
              end;
     end;
     FRefrescarGraficas[iPos] := FALSE;
end;

procedure TEditConteo.GridDetalleDrawColumnCell(Sender: TObject; const Rect: TRect;
          DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
     inherited;
     with GridDetalle do
     begin
          if ( gdSelected in State )  then
          begin
               with Canvas do
               begin
                    Font.Color := clWhite;
                    Brush.Color := clNavy;
               end;
          end
          else
          begin
               with Canvas do
               begin
                    Brush.Color := clWhite;
                    with Font, DataSource.DataSet do
                    begin
                         if FindField('CT_DIFERENCIA') <> NIL then
                         begin
                              if FieldByName('CT_DIFERENCIA').AsInteger < 0 then
                                 Color := clRed
                              else if FieldByName('CT_DIFERENCIA').AsInteger > 0 then
                                   Color := clGreen
                              else Color := clBlack;
                         end;
                    end;
               end;
          end;
          DefaultDrawDataCell( Rect, Column.Field, State );
     end;
end;

procedure TEditConteo.Imprimir;
var
   iAncho : integer;
begin
     if PageDetalle.ActivePage = Detalle then
     begin
          with GridDetalle.DataSource.DataSet.FieldByName('TB_ELEMENT') do
          begin
               iAncho := DisplayWidth;
               DisplayWidth := 30;

               Inherited Imprimir;

               DisplayWidth := iAncho;
          end;
     end
     else if zConfirm( 'Imprimir...', '� Desea Imprimir La Pantalla ' + Caption + ' ?', 0, mbYes ) then
          Print;
end;

procedure TEditConteo.InvocarModificacion;
var
   i : Integer;
   aPosicion : Array[0..3] of string;
begin
     if ( dsDetalle.DataSet.Name = dmConteo.cdsConteo.Name ) then
        DoEdit
     else
        with dmConteo, Arbol.Selected do
        begin
             for i:=0 to iMin(Level+1,3) do
                 aPosicion[i] := DataSetTotales(Level+1).Fields[i].AsString;
             PosicionaArbol( aPosicion );
        end;
end;

procedure TEditConteo.WMExaminar(var Message: TMessage);
begin
     InvocarModificacion;
end;

procedure TEditConteo.ModificarBtnClick(Sender: TObject);
begin
     InvocarModificacion;
end;

procedure TEditConteo.KeyPress( var Key: Char );
begin
     if (ActiveControl = GridDetalle) and
        (Key = chr(VK_RETURN)) and
        (GridDetalle.SelectedField.FieldName = 'CT_CUANTOS') then
     begin
          Key := #0;
          GridDetalle.SetFocus;
     end;

     inherited KeyPress( Key );
end;

procedure TEditConteo.KeyDown( var Key: Word; Shift: TShiftState );
begin
{     if ( ActiveControl = GridDetalle ) and (Key = VK_RETURN) and
        (GridDetalle.SelectedField.FieldName = 'CT_CUANTOS') then
     begin
          Key := 0;
          SeleccionaSiguienteRenglon;
     end; }

     inherited KeyDown( Key, Shift );

     if ( Key <> 0 ) then
     begin
          if ( Key = VK_F5 ) and ( not Editing ) then
          begin
               Key := 0;
               Refrescar;
          end;
          if ( ActiveControl = GridDetalle ) then
          begin
               if ( Key = VK_TAB ) then
               begin
                    Key := 0;
                    PosicionaSiguienteColumna;
               end;
               if ( Key = VK_DOWN ) or ( (Key = VK_RETURN) and
                  (GridDetalle.SelectedField.FieldName = 'CT_CUANTOS') ) then
               begin
                    Key := 0;
                    SeleccionaSiguienteRenglon;
               end;
          end;
     end;
end;


procedure TEditConteo.SeleccionaSiguienteRenglon;
begin
     with GridDetalle.DataSource.DataSet do
     begin
          Next;
     end;
end;

procedure TEditConteo.PosicionaSiguienteColumna;
var
   i : Integer;
begin
     with GridDetalle do
     begin
          i := SelectedIndex;
          if ( i = Columns.Count - 1 ) then
          begin
               SeleccionaSiguienteRenglon;
               SelectedIndex := 0;
          end
          else
             SelectedIndex := i + 1;
     end;
end;

function TEditConteo.ValoresGrid: Variant;
begin
     Result := VarArrayOf( [ FTitulo, '', stTodos, stNinguno ] );
end;

procedure TEditConteo.UpBtnClick(Sender: TObject);
begin
     inherited;
     with Arbol do
          if ( Selected <> NIL ) and ( Selected.Parent <> NIL ) then
             Selected := Selected.Parent;
end;

procedure TEditConteo.GridDetalleTitleClick(Column: TColumn);
begin
     inherited;
     GridDetalle.OrdenarPor( Column );
     SetRefrescarGraficas( TRUE );
end;

procedure TEditConteo.EscribirCambios;
begin
     {cv: CUMPLEVERSION ES TEMPORAL SE DEBE ELIMINAR CUANDO SE GENERE LA 2.1}
     if Autorizacion.CumpleVersion( '2.1' ) and not Autorizacion.EsDemo then
     begin
          inherited EscribirCambios;
     end
     else
     begin
          ZetaDialogo.zInformation( Caption, 'Operaci�n no Permitida en Modo Demo', 0);
     end;
end;

procedure TEditConteo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if ( dmConteo.cdsConteo.ChangeCount > 0 ) then
        DoCancelChanges;
     inherited;
end;

function TEditConteo.BotonesActivos: Boolean;
begin
     Result := TRUE;
end;

end.
