unit FSistProcesos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, ExtCtrls, Grids,
     DBGrids, StdCtrls, Buttons, DBCtrls, Mask, ComCtrls,
     ZetaMessages,
     ZBaseConsulta,
     ZetaDBGrid,
     ZetaKeyCombo,
     ZetaKeyLookup_DevEx,
     ZetaFecha, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, 
  dxSkinsDefaultPainters, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ZetaCXGrid, Menus, ActnList, ImgList,
  TressMorado2013, cxButtons, System.Actions;

type
  TSistProcesos_DevEx = class(TBaseConsulta)
    PanelProcesos: TPanel;
    ProcessLBL: TLabel;
    ProcessStartLBL: TLabel;
    ProcessEndLBL: TLabel;
    ProcessUserLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    Usuario: TZetaKeyLookup_DevEx;
    Process: TZetaKeyCombo;
    StatusLBL: TLabel;
    Status: TComboBox;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    ZetaDBGridLevel: TcxGridLevel;
    PC_PROC_ID: TcxGridDBColumn;
    PC_AVANCE: TcxGridDBColumn;
    PC_FALTA: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    PC_STATUS: TcxGridDBColumn;
    PC_FEC_INI: TcxGridDBColumn;
    PC_SPEED: TcxGridDBColumn;
    PC_ERROR: TcxGridDBColumn;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    cxImage16: TcxImageList;
    PopupMenu1: TPopupMenu;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    RefrescarProcesos_DevEx: TcxButton;
    Cancelar_DevEx: TcxButton;
    PC_HOR_INI: TcxGridDBColumn;
    PC_NUMERO: TcxGridDBColumn;
    PC_TIEMPO: TcxGridDBColumn;
    LblCambioFechas: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure RefrescarProcesosClick(Sender: TObject);
    //procedure ProcessGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure StatusChange(Sender: TObject);
    procedure ProcessChange(Sender: TObject);
    procedure UsuarioValidLookup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZetaDBGridDBTableViewDataControllerSortingChanged(
      Sender: TObject);
    procedure ZetaDBGridDBTableViewColumnHeaderClick(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure FechasValidDate(Sender: TObject);
  private
    { Private declarations }
    FPuedeCancelar: Boolean;
    AColumn: TcxGridDBColumn;
    procedure WMFocus(var Message: TMessage); message WM_FOCUS;
    procedure WMDefocus(var Message: TMessage); message WM_DEFOCUS;
    //DevEx (by ame): Proceso Agregado para asignar el MinWidth Ideal
    procedure ApplyMinWidth;
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    function PuedeModificar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure Refresh; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  SistProcesos_DevEx: TSistProcesos_DevEx;

implementation

uses DConsultas,
     DSistema,
     {$ifdef ADUANAS}
     ZHelpContext,
     {$ENDIF}
     ZetaDialogo,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZGridModeTools,
     ZetaClientDataSet;

{$R *.DFM}

procedure TSistProcesos_DevEx.FormCreate(Sender: TObject);
begin
     FechaInicial.Valor := Date();
     FechaFinal.Valor := Date();
     {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
     ZetaDBGridDBTableView.DataController.DataModeController.GridMode:= TRUE;
     ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter := TRUE;
     {***}
     inherited;
     {$ifdef ADUANAS}
     HelpContext := H_PROCESOS_DEL_SISTEMA;
     {$ELSE}
     HelpContext := H60655_Procesos_Abiertos;
     {$ENDIF}
     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_PROCESOS;
     {$endif}
     Status.ItemIndex := 0;
     with Process do
     begin
          dmConsultas.LlenaListaProcesos( Lista );
          ItemIndex := 0;
     end;
     Usuario.LookupDataset := dmSistema.cdsUsuarios;
end;

procedure TSistProcesos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     //DevEx: Inactiva la capacidad de filtrar a las columnas
     ZetaDBGridDBTableView.OptionsCustomize.ColumnFiltering := True;
     //Para que nunca muestre el filterbox inferior
     ZetaDBGridDBTableView.FilterBox.Visible := fvNever;
     //Para que no aparezca el Custom Dialog
     ZetaDBGridDBTableView.FilterBox.CustomizeDialog := False;
     //No Permitir agrupacion y desaparecer la caja de agrupacion
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := False;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := False;

     //DevEx: Aplicar MinWidth ideal despues de que esten definidas todas las columnas y antes del BestFit
     ApplyMinWidth;
     //Aplicar width ideal para las columnas
     ZetaDBGridDBTableView.ApplyBestFit();

     // Desarrollo de US #12445: Cambios sobre la pantalla de Consulta de procesos.
     DoRefresh;
end;

procedure TSistProcesos_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmConsultas do
     begin
          DataSource.DataSet := cdsProcesos;
     end;

     // Desarrollo de US#12445: Cambios sobre la pantalla de Consulta de procesos.
     // Comentar llamada a DoRefresh excepto en el m�todo TSistProcesos_DevEx.RefrescarProcesosClick.
     // DoRefresh;

     FPuedeCancelar := CheckDerechos( K_DERECHO_BAJA );




end;

procedure TSistProcesos_DevEx.Disconnect;
begin
     inherited Disconnect;
end;

procedure TSistProcesos_DevEx.Refresh;
begin
     with dmConsultas do
     begin
          RefrescarListaProcesos( Status.ItemIndex, Process.LlaveEntero, Usuario.Valor, FechaInicial.Valor, FechaFinal.Valor );
     end;
     LblCambioFechas.Visible := FALSE;
end;

function TSistProcesos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar Un Proceso';
end;

function TSistProcesos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Permite Borrar Procesos';
end;

function TSistProcesos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := True; { Siempre se permite ya que la interfase de usuario no permite cambios }
end;

procedure TSistProcesos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     Cancelar_DevEx.Enabled := FPuedeCancelar and dmConsultas.ProcesoEstaAbierto;
end;

procedure TSistProcesos_DevEx.CancelarClick(Sender: TObject);
begin
     inherited;
     dmConsultas.CancelarProceso;
     //DoRefresh;
     DoBestFit;
end;

procedure TSistProcesos_DevEx.Modificar;
begin
     inherited;
     dmConsultas.cdsProcesos.Modificar;
     DoBestFit;
end;

procedure TSistProcesos_DevEx.RefrescarProcesosClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     //ProcessGrid.SetFocus;
     ZetaDBGrid.SetFocus;
     DoBestFit;
end;

procedure TSistProcesos_DevEx.StatusChange(Sender: TObject);
begin
     inherited;
     //DoRefresh;
     ZetaDBGridDBTableView.DataController.DataSource := nil;
     ZetaDBGridDBTableView.DataController.DataSource := DataSource;
     DoBestFit;
end;

procedure TSistProcesos_DevEx.ProcessChange(Sender: TObject);
begin
     inherited;
     //DoRefresh;
     DoBestFit;
end;

procedure TSistProcesos_DevEx.FechasValidDate(Sender: TObject);
begin
     inherited;
     LblCambioFechas.Visible := TRUE;
end;

procedure TSistProcesos_DevEx.UsuarioValidLookup(Sender: TObject);
begin
     inherited;
     //DoRefresh;
     DoBestFit;
end;

procedure TSistProcesos_DevEx.WMDefocus(var Message: TMessage);
begin
     RefrescarProcesos_DevEx.Default := False;
end;

procedure TSistProcesos_DevEx.WMFocus(var Message: TMessage);
begin
     RefrescarProcesos_DevEx.Default := True;
end;

procedure TSistProcesos_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(Sender: TcxFilterCriteria; AItemIndex: Integer;
          AValueList: TcxDataFilterValueList);
begin
     inherited;
     ZGridModeTools.BorrarItemGenericoAll( AValueList );
     if ZetaDBGridDBTableView.DataController.IsGridMode then
        ZGridModeTools.FiltroSetValueLista( ZetaDBGridDBTableView, AItemIndex, AValueList );
end;

procedure TSistProcesos_DevEx.ZetaDBGridDBTableViewDataControllerSortingChanged(
  Sender: TObject);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  ZetaDBGrid.OrdenarPor( AColumn , TZetaClientDataset(DataSource.DataSet) );
end;

procedure TSistProcesos_DevEx.ZetaDBGridDBTableViewDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TSistProcesos_DevEx.ZetaDBGridDBTableViewColumnHeaderClick(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  {***(@am): Solo aplicara cuando el usuario tenga configurado GridMode***}
  self.AColumn := TcxGridDBColumn(AColumn);
end;

procedure TSistProcesos_DevEx.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
 status: String;
begin
  inherited;

  //Sacamos el status de la columna invisible
  // status := AViewInfo.GridRecord.DisplayTexts [8];
  status := AViewInfo.GridRecord.DisplayTexts [9];
  With ACanvas do
  begin
      with Font do
      begin
           case ZetaClientTools.GetProcessStatus( status ) of
                epsEjecutando: Color := clGreen;
                epsCancelado: Color := clFuchsia;
                epsError: Color := clRed;
           else
               Color := ZetaDBGrid.Font.Color;
           end;
      end;
  end;
end;
procedure TSistProcesos_DevEx._A_ImprimirExecute(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TSistProcesos_DevEx._E_AgregarExecute(Sender: TObject);
begin
  inherited;
  DoInsert;
end;

procedure TSistProcesos_DevEx._E_BorrarExecute(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TSistProcesos_DevEx._E_ModificarExecute(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TSistProcesos_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =40;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;

procedure TSistProcesos_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       DoEdit;
  end;
end;

end.
