unit FProcessLogShow;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Variants,
     Grids, DBGrids, DBCtrls, Db, ComCtrls,
     ZetaCommonLists,
     ZetaDBTextBox,
     ZetaDBGrid;

type
  TLogShow = class(TForm)
    PanelInferior: TPanel;
    Salir: TBitBtn;
    Datasource: TDataSource;
    DBNavigator: TDBNavigator;
    ExportarBtn: TBitBtn;
    PanelSuperior: TPanel;
    InicioLBL: TLabel;
    FinLBL: TLabel;
    DuracionLBL: TLabel;
    Label2: TLabel;
    CanceladoPorLBL: TLabel;
    UsuarioLBL: TLabel;
    StatusLBL: TLabel;
    EmpleadosLBL: TLabel;
    PC_TIEMPO: TZetaDBTextBox;
    dsLog: TDataSource;
    PC_STATUS: TZetaDBTextBox;
    PC_NUMERO: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    US_CANCELA: TZetaDBTextBox;
    PC_MAXIMO: TZetaDBTextBox;
    UltimoEmpleadoLBL: TLabel;
    CB_CODIGO: TZetaDBTextBox;
    DBGrid: TZetaDBGrid;
    AvanceLBL: TLabel;
    PC_AVANCE: TZetaDBTextBox;
    PC_INICIO: TZetaDBTextBox;
    PC_FIN: TZetaDBTextBox;
    SaveDialog: TSaveDialog;
    Imprimir: TBitBtn;
    lblParametros: TLabel;
    PC_PARAM: TDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SalirClick(Sender: TObject);
    procedure ExportarBtnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure DBGridDblClick(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
  private
    { Private declarations }
    FProceso: Procesos;
    function GetBitacoraFile( var sArchivo: String ): Boolean;
    procedure Connect;
    procedure Disconnect;
    procedure ExportarBitacora;
    procedure SetProceso(const Value: Procesos);
  public
    { Public declarations }
    property Proceso: Procesos read FProceso write SetProceso;
  end;

var
  LogShow: TLogShow;

procedure GetBitacora( const eProceso: Procesos; const iFolio: Integer );
procedure ShowLog( const eProceso: Procesos );

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaMessages,
     ZetaDialogo,
     ZetaClientDataSet,
     {$ifdef CONCILIA}
     ZImprimeGrid,
     {$ifdef FONACOT}
     DConcilia;
     {$else}
     DCliente;
     {$endif}
     {$else}
     FBaseReportes,
     DConsultas;
     {$endif}

{$R *.DFM}

procedure ShowLog( const eProceso: Procesos );
begin
     with TLogShow.Create( Application ) do
     begin
          try
             Proceso := eProceso;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure GetBitacora( const eProceso: Procesos; const iFolio: Integer );
begin
     {$ifdef CONCILIA}
     {$ifdef FONACOT}
     with dmConcilia do
     {$else}
     with dmCliente do
     {$endif}
     {$else}
     with dmConsultas do
          {$ifdef TRESS}
          if (eProceso in [ prSistImportarTablas, prSistImportarTablasCSV, prPatchCambioVersion ] ) then
          begin
               {***DevEx(@am): La siguiente validacion es necesaria pues la propiedad dmConsultas.BaseDatosImportacionXMLCSV
                               solo se inicializaba desde la ejecutacion de los procesos de: Importacion de Tablas, Importacion de Tablas CSV
                               y actualizacion de BD. De esta forma, al acceder la bitacora al final de un proceso o desde la opcion F9 despues de
                               ejecutar un proceso, esta aparecia correctamente. Sin embargo, al entrar a TRESS e ir directamente a la opcion F9
                               para ver una bitacora correspondiente a alguno de estos tres casos, SE desplegaba el error reportado en el bug 5493 en TP.***}
                if VarIsEmpty(BaseDatosImportacionXMLCSV) then
                    GetProcessLog(iFolio, FALSE)
                else
                    GetProcessLog(iFolio, TRUE);
          end
          else
          {$endif}
     {$endif}
          GetProcessLog(iFolio);
     ShowLog( eProceso );
end;

{ ************ TShowLog ************ }

procedure TLogShow.FormCreate(Sender: TObject);
begin
     HelpContext := H00005_Bitacora_procesos;
end;

procedure TLogShow.FormShow(Sender: TObject);
begin
     Connect;
end;

procedure TLogShow.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Disconnect;
end;

procedure TLogShow.SetProceso( const Value: Procesos );
begin
     FProceso := Value;
     Caption := ZetaClientTools.GetProcessName( Proceso );
     EmpleadosLBL.Caption := ZetaClientTools.GetProcessElement( Proceso ) + ':';
end;

procedure TLogShow.Connect;
begin
    {$ifdef CONCILIA}
     {$ifdef FONACOT}
     with dmConcilia do
     {$else}
     with dmCliente do
     {$endif}
     {$else}
     with dmConsultas do
     {$endif}
     begin
          Datasource.Dataset := cdsProcesos;
          dsLog.Dataset := cdsLogDetail;
     end;
     with Imprimir do
     begin
          Enabled := ZAccesosMgr.CheckDerecho( D_CONS_BITACORA, K_DERECHO_IMPRESION );
          ExportarBtn.Enabled := Enabled;
     end;
end;

procedure TLogShow.Disconnect;
begin
     Datasource.Dataset := nil;
end;

procedure TLogShow.DBGridDrawColumnCell(Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
   oColor: TColor;
begin
     with DBGrid do
     begin
          oColor := Canvas.Font.Color;
          try
             if ( eTipoBitacora( Columns.Items[ 2 ].Field.AsInteger ) in [ tbError, tbErrorGrave ] ) then
                Canvas.Font.Color := clRed;
             DefaultDrawColumnCell( Rect, DataCol, Column, State );
          finally
                 Canvas.Font.Color := oColor;
          end;
     end;
end;

procedure TLogShow.SalirClick(Sender: TObject);
begin
     Close;
end;

procedure TLogShow.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = Chr( VK_ESCAPE ) then
     begin
          Key := #0;
          Close;
     end;
end;

procedure TLogShow.ExportarBtnClick(Sender: TObject);
begin
     ExportarBitacora;
end;

procedure TLogShow.ImprimirClick(Sender: TObject);
begin
    if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir El Grid De ' + Caption + '?', 0, mbYes ) then
     begin
    {$ifdef CONCILIA}
        ZImprimeGrid.ImprimirGrid( DBGrid,
                                      DBGrid.DataSource.DataSet,
                                      Caption,
                                      'IM',
                                      '',
                                      '',
                                      stNinguno,
                                      stNinguno );
    {$else}
          FBaseReportes.ImprimirGrid( DBGrid,
                                      DBGrid.DataSource.DataSet,
                                      Caption,
                                      'IM',
                                      '',
                                      '',
                                      stNinguno,
                                      stNinguno );

     {$endif}
      end;
end;

function TLogShow.GetBitacoraFile( var sArchivo: String ): Boolean;
begin
     with SaveDialog do
     begin
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );
          if Execute then
          begin
               Result := True;
               sArchivo := FileName;
          end
          else
              Result := False;
     end;
end;

procedure TLogShow.ExportarBitacora;
var
   Posicion: TBookMarkStr;
   sArchivo: String;
   oCursor: TCursor;
begin
     sArchivo := SetFileNameDefaultPath( Caption + '.txt' );
     if GetBitacoraFile( sArchivo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          with TStringList.Create do
          begin
               try
                  Add( 'Proceso:  ' + Self.Caption );
                  Add( 'Folio:    ' + PC_NUMERO.Caption );
                  Add( 'Inicio:   ' + PC_INICIO.Caption );
                  Add( 'Fin:      ' + PC_FIN.Caption );
                  Add( 'Duraci�n: ' + PC_TIEMPO.Caption );
                  Add( 'Status:   ' + PC_STATUS.Caption );
                  Add( 'Usuario:  ' + US_CODIGO.Caption );
                  Add( 'Cancelado Por: ' + US_CANCELA.Caption );
                  Add( ZetaClientTools.GetProcessElement( Proceso ) + ': ' + PC_MAXIMO.Caption );
                  Add( 'Ultimo Empleado: ' + CB_CODIGO.Caption );
                  Add( 'Parametros' );
                  Add( PC_PARAM.Text );                                    
                  Add( StringOfChar( '-', 40 ) );
                  with dsLog.Dataset do
                  begin
                       Posicion := BookMark;
                       DisableControls;
                       try
                          First;
                          while not Eof do
                          begin
                               Add( 'Tipo: ' + FieldByName( 'BI_TIPO' ).AsString );
                               Add( 'Empleado: ' + FieldByName( 'CB_CODIGO' ).AsString );
                               Add( 'Movimiento: ' + FieldByName( 'BI_FEC_MOV' ).AsString );
                               Add( 'Error: ' + FieldByName( 'BI_TEXTO' ).AsString );
                               Add( '' );
                               Add( FieldByName( 'BI_DATA' ).AsString );
                               Add( '===============================================' );
                               Next;
                          end;
                       finally
                              BookMark := Posicion;
                              EnableControls;
                       end;
                  end;
                  SaveToFile( sArchivo );
                  if ZetaDialogo.zConfirm( Caption, 'El Archivo ' + sArchivo + ' Fu� Creado' + CR_LF + '� Desea Verlo ?', 0, mbYes ) then
                  begin
                       ZetaClientTools.ExecuteFile( 'NOTEPAD.EXE', sArchivo, ExtractFileDir( sArchivo ), SW_SHOWDEFAULT );
                  end;
               finally
                      Free;
               end;
          end;
          Screen.Cursor := oCursor;
     end;
end;

procedure TLogShow.US_CODIGOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     {$ifdef CONCILIA}
     {$else}
     dmConsultas.UsuarioGetText( Sender, Text, DisplayText );
     {$endif}
end;

procedure TLogShow.DBGridDblClick(Sender: TObject);
begin
     TZetaClientDataSet(dsLog.Dataset).Modificar;
end;

end.
