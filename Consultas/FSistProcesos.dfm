inherited SistProcesos: TSistProcesos
  Left = 223
  Top = 174
  Caption = 'Procesos'
  ClientHeight = 344
  ClientWidth = 644
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 644
    inherited ValorActivo2: TPanel
      Width = 385
    end
  end
  object PanelProcesos: TPanel [1]
    Left = 0
    Top = 19
    Width = 644
    Height = 112
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object ProcessLBL: TLabel
      Left = 23
      Top = 26
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = '&Proceso:'
      FocusControl = Process
    end
    object ProcessStartLBL: TLabel
      Left = 2
      Top = 49
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha &Inicial:'
      FocusControl = FechaInicial
    end
    object ProcessEndLBL: TLabel
      Left = 7
      Top = 70
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha &Final:'
      FocusControl = FechaFinal
    end
    object ProcessUserLBL: TLabel
      Left = 26
      Top = 92
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = Usuario
    end
    object StatusLBL: TLabel
      Left = 27
      Top = 5
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = '&Mostrar:'
      FocusControl = Status
    end
    object FechaInicial: TZetaFecha
      Left = 68
      Top = 44
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 2
      Text = '12/Nov/99'
      Valor = 36476
      OnValidDate = StatusChange
    end
    object FechaFinal: TZetaFecha
      Left = 68
      Top = 66
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 3
      Text = '12/Nov/99'
      Valor = 36476
      OnValidDate = StatusChange
    end
    object Usuario: TZetaKeyLookup
      Left = 68
      Top = 88
      Width = 219
      Height = 21
      LookupDataset = dmSistema.cdsUsuarios
      TabOrder = 4
      TabStop = True
      WidthLlave = 45
      OnValidLookup = UsuarioValidLookup
    end
    object RefrescarProcesos: TBitBtn
      Left = 289
      Top = 57
      Width = 133
      Height = 25
      Hint = 'Obtener Nuevamente La Bitácora Deseada'
      Caption = '&Refrescar Procesos'
      Default = True
      ModalResult = 4
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = RefrescarProcesosClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Process: TZetaKeyCombo
      Left = 68
      Top = 23
      Width = 218
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = ProcessChange
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object Status: TComboBox
      Left = 68
      Top = 2
      Width = 218
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      OnChange = StatusChange
      Items.Strings = (
        'Abiertos'
        'Terminados'
        'Terminados Con Exito'
        'Terminados Con Error'
        'Cancelados'
        'Todos')
    end
    object Cancelar: TBitBtn
      Left = 289
      Top = 85
      Width = 133
      Height = 25
      Hint = 'Cancelar Ejecución del Proceso Selecionado'
      Cancel = True
      Caption = '&Cancelar Proceso'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = CancelarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object ProcessGrid: TZetaDBGrid [2]
    Left = 0
    Top = 131
    Width = 644
    Height = 213
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDrawColumnCell = ProcessGridDrawColumnCell
    Columns = <
      item
        Expanded = False
        FieldName = 'PC_PROC_ID'
        Title.Caption = 'Proceso'
        Width = 146
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PC_AVANCE'
        Title.Caption = 'Avance'
        Width = 43
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PC_FALTA'
        Title.Caption = 'Faltan'
        Width = 99
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'Empleado'
        Width = 65
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'Usuario'
        Width = 51
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PC_STATUS'
        Title.Caption = 'Status'
        Width = 58
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PC_FEC_INI'
        Title.Caption = 'Fecha'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PC_SPEED'
        Title.Caption = 'Velocidad'
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 184
    Top = 240
  end
end
