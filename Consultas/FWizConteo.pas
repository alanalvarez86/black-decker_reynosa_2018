unit FWizConteo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseWizard, ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaKeyLookup, CheckLst;

type
  TWizConteo = class(TBaseWizard)
    PanelEspacio: TPanel;
    Panel1: TPanel;
    Panel4: TPanel;
    LblNivel1: TLabel;
    NIVEL1: TZetaKeyLookup;
    NIVEL4: TZetaKeyLookup;
    LblNivel4: TLabel;
    Panel4b: TPanel;
    Bevel1: TBevel;
    Panel2: TPanel;
    LblNivel2: TLabel;
    NIVEL2: TZetaKeyLookup;
    Panel2b: TPanel;
    Bevel2: TBevel;
    Panel3: TPanel;
    LblNivel3: TLabel;
    NIVEL3: TZetaKeyLookup;
    Panel3b: TPanel;
    Bevel3: TBevel;
    Detalle: TTabSheet;
    Prender: TBitBtn;
    Apagar: TBitBtn;
    GroupPresupuesto: TGroupBox;
    Presupuesto: TCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure PrenderClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
  private
    { Private declarations }
    FNivelActual : Integer;
    procedure SetNivelActual(const Value: Integer);
    procedure SetLookupDataSets;
    procedure PrendeApaga( const lState: Boolean );
    function SetCheckBox( const lFiltrar: Boolean ): Boolean;
    function CheckPresupuesto: Boolean;
    function GetLlave1: String;
    function GetLlave2: String;
    function GetLlave3: String;
    function GetLlave4: String;
  protected
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
    property NivelActual : Integer read FNivelActual write SetNivelActual;
    property Llave1: String read GetLlave1;
    property Llave2: String read GetLlave2;
    property Llave3: String read GetLlave3;
    property Llave4: String read GetLlave4;
  end;

var
  WizConteo: TWizConteo;

implementation

uses dConteo, ZetaClientDataSet, ZGlobalTress, ZetaCommonTools, ZetaDialogo;

{$R *.DFM}

procedure TWizConteo.FormCreate(Sender: TObject);
begin
     inherited;
     SetLookupDataSets;
     //HelpContext:= H10168_Cancelar_kardex;
end;

procedure TWizConteo.FormShow(Sender: TObject);
begin
     inherited;
     ProgressBar.Visible := FALSE;
     if ( ParametrosControl = nil ) then
     begin
          PageControl.ActivePage := Detalle;
          ActiveControl := Presupuesto;
     end;
end;

function TWizConteo.EjecutarWizard: Boolean;
var
   i : Integer;
begin
     with Presupuesto, dmConteo do
     begin
          ProgressBar.Visible := TRUE;
          ProgressBar.Max := Items.Count;
          cdsConteo.DisableControls;
          try
             for i := 0 to ( Items.Count - 1 ) do
             begin
                  if Checked[ i ] then
                     with cdsConteo do
                     begin
                          Append;
                          Fields[0].AsString := NIVEL1.Llave;
                          Fields[1].AsString := NIVEL2.Llave;
                          Fields[2].AsString := NIVEL3.Llave;
                          Fields[3].AsString := NIVEL4.Llave;
                          Fields[NumNiveles-1].AsString := Items.Names[i];
                          Post;
                     end;
                  ProgressBar.StepIt;
                  Application.ProcessMessages;
             end;
          finally
             cdsConteo.EnableControls;
          end;
     end;
     Result := TRUE;
end;

procedure TWizConteo.SetLookupDataSets;

   procedure SetPanelLookup( oPanel: TPanel; oLabel: TLabel; oLookup : TZetaKeyLookup );
   begin
        with dmConteo do
        begin
             oPanel.Visible := ( oPanel.Tag < NumNiveles );
             if ( oPanel.Visible ) then
                with GetRecordConteo( GetConteoNiveles( oPanel.Tag - 1 ) ) do
                begin
                     oLabel.Caption := Descripcion;
                     oLookup.LookupDataset := Lookup;
                end;
        end;
   end;

begin
     SetPanelLookup( Panel1, LblNivel1, NIVEL1 );
     SetPanelLookup( Panel2, LblNivel2, NIVEL2 );
     SetPanelLookup( Panel3, LblNivel3, NIVEL3 );
     SetPanelLookup( Panel4, LblNivel4, NIVEL4 );
     with dmConteo do
          GroupPresupuesto.Caption := GetRecordConteo( GetConteoNiveles( NumNiveles - 1 ) ).Descripcion;
end;

procedure TWizConteo.SetNivelActual(const Value: Integer);

   function SetPanelActivo( oPanel: TPanel; oLabel: TLabel; oLookup : TZetaKeyLookup ): Boolean;
   var
      iCodigos : Integer;
      sCodigos : String;
   begin
        Result := TRUE;
        if oPanel.Visible then
        begin
             oPanel.Enabled := ( oPanel.Tag >= Value );
             oLabel.Enabled := oPanel.Enabled;
             oLookup.Enabled := oPanel.Enabled;
             if ( oPanel.Enabled ) and ( ParametrosControl = nil ) then
             begin
                  ParametrosControl := oLookup;
                  with oLookup do
                  begin
                       sCodigos := dmConteo.GetListaCodigos( oPanel.Tag, iCodigos );
                       if ( iCodigos > 0 ) then
                          Filtro := Format( 'NOT %s in (%s)', [ LookupDataSet.LookupKeyField,
                                                                sCodigos ] );
                       Result := ( iCodigos < LookupDataSet.RecordCount );
                       if not Result then
                          ZetaDialogo.ZError( self.Caption, Format( 'No se Puede Agregar Otro Valor de %s', [ oLabel.Caption ] ), 0 );
                  end;
             end;
             if not ( oPanel.Enabled ) then
                oLookup.Llave := dmConteo.DataSetTotales( FNivelActual ).Fields[oPanel.Tag-1].AsString;
        end;
   end;

begin
     FNivelActual := Value;
     ParametrosControl := nil;
     if ( not SetPanelActivo( Panel1, LblNivel1, NIVEL1 ) ) or
        ( not SetPanelActivo( Panel2, LblNivel2, NIVEL2 ) ) or
        ( not SetPanelActivo( Panel3, LblNivel3, NIVEL3 ) ) or
        ( not SetPanelActivo( Panel4, LblNivel4, NIVEL4 ) ) or
        ( not SetCheckBox( Value = dmConteo.NumNiveles ) ) then
        FNivelActual := 0;
end;

function TWizConteo.SetCheckBox( const lFiltrar: Boolean ): Boolean;
var
   sCodigo: String;
begin
     with dmConteo, Presupuesto do
     begin
          Clear;
          Items.BeginUpdate;
          try
             if lFiltrar then
                cdsConteo.DisableControls;
             try
                with GetRecordConteo( GetConteoNiveles( NumNiveles - 1 ) ).Lookup do
                begin
                     First;
                     while not EOF do
                     begin
                          sCodigo := FieldByName( LookupKeyField ).AsString;
                          if ( not lFiltrar ) or
                             ( not cdsConteo.Locate( cdsConteo.Fields[ FNivelActual - 1 ].FieldName,
                                   sCodigo, [ ] ) ) then
                             Items.Add( sCodigo + '=' + FieldByName( LookupDescriptionField ).AsString );
                          Next;
                     end;
                end;
             finally
             if lFiltrar then
                cdsConteo.EnableControls;
             end;
          finally
             Items.EndUpdate;
          end;
          Result := ( Items.Count > 0 );
          if not Result then
             ZetaDialogo.ZError( self.Caption, Format( 'No se Puede Agregar Otro Valor de %s', [ GroupPresupuesto.Caption ] ), 0 );
     end;
end;

procedure TWizConteo.PrendeApaga(const lState: Boolean);
var
   i: Integer;
begin
     with Presupuesto do
     begin
          with Items do
          begin
               try
                  BeginUpdate;
                  for i := 0 to ( Count - 1 ) do
                      Checked[ i ] := lState;
               finally
                  EndUpdate;
               end;
          end;
     end;
end;

procedure TWizConteo.PrenderClick(Sender: TObject);
begin
     inherited;
     PrendeApaga( TBitBtn( Sender ).Tag = 1 );
end;

procedure TWizConteo.WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
          var CanMove: Boolean);

   function ValidaControlLookup( oLookup: TZetaKeyLookup; oLabel: TLabel ): Boolean;
   begin
        with oLookup do
        begin
             Result := ( not Parent.Visible ) or strLleno( Llave );
             if not Result then
                Error( 'El Criterio de ' + oLabel.Caption + ' No Puede Quedar Vac�o', oLookup );
        end;
   end;

begin
     inherited;
     if CanMove and Wizard.Adelante then
        if Wizard.EsPaginaActual( Parametros ) then
           CanMove := ValidaControlLookup( NIVEL1, LblNivel1 ) and
                      ValidaControlLookup( NIVEL2, LblNivel2 ) and
                      ValidaControlLookup( NIVEL3, LblNivel3 ) and
                      ValidaControlLookup( NIVEL4, LblNivel4 )
        else if Wizard.EsPaginaActual( Detalle ) then
        begin
             CanMove := CheckPresupuesto;
             if not CanMove then
                Error( 'Debe Especificar Criterios del Presupuesto', Presupuesto );
        end;
end;

function TWizConteo.CheckPresupuesto: Boolean;
var
   i: Integer;
begin
     Result := FALSE;
     with Presupuesto do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               Result := Checked[ i ];
               if Result then
                  Break;
          end;
     end;
end;

function TWizConteo.GetLlave1: String;
begin
     Result := NIVEL1.Llave;
end;

function TWizConteo.GetLlave2: String;
begin
     Result := NIVEL2.Llave;
end;

function TWizConteo.GetLlave3: String;
begin
     Result := NIVEL3.Llave;
end;

function TWizConteo.GetLlave4: String;
begin
     Result := NIVEL4.Llave;
end;

end.
