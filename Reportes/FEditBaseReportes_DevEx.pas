{$HINTS OFF}
unit FEditBaseReportes_DevEx;

interface

{$INCLUDE DEFINES.INC}
{$ifdef TRESS}
 {$DEFINE MULTIPLES_ENTIDADES}
{$endif}
{$define CONFIDENCIALIDAD_MULTIPLE}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dbclient,  Buttons, StdCtrls, ExtCtrls,
  ZetaSmartLists, ZetaKeyLookup, ZetaEdit, ZetaFecha, CheckLst, Db,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Grids, DBGrids,
  ZetaDBGrid,
  FileCtrl,
  ZReportTools,ZReportToolsConsts,
  ZetaTipoEntidad,
  ZetaTipoEntidadTools,
  ZetaCommonLists,
  ZetaCommonClasses,
  {$ifdef RDD}
  {$else}
  DEntidadesTress,
  {$endif}
  ZAgenteSQLClient, FBaseReportes_DevEx, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, dxSkinsdxBarPainter,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, cxButtons,
  dxBar, cxClasses, ImgList, ZetaKeyLookup_DevEx,
  cxStyles, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  ZetaSmartLists_DevEx, cxListBox, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxPC, cxCheckListBox, dxBarBuiltInMenu, cxDBEdit, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer, cxTLData,
  cxDBTL, cxTreeView ;

type
  TEditBaseReportes_DevEx = class(TBaseReportes_DevEx)
    tsParametro: TcxTabSheet;
    GroupBox13: TGroupBox;
    LBParametros: TZetaSmartListBox_DevEx;
    GBPropiedadesParametro: TGroupBox;
    LTituloParametro: TLabel;
    lFormulaParametro: TLabel;
    ETituloParametro: TEdit;
    EFormulaParametro: TcxMemo;
    SmartListParametros: TZetaSmartLists_DevEx;
    tsImpresora: TcxTabSheet;
    lbTablaPrincipal: TLabel;
    RE_ENTIDAD: TZetaDBTextBox;
    dsResultados: TDataSource;
    RGOrden: TRadioGroup;
    GroupBox3: TGroupBox;
    cbBitacora: TCheckBox;
    tsSuscripciones: TcxTabSheet;
    lbSubordinados: TLabel;
    RE_IFECHA: TDBCheckBox;
    GBPropiedadesSus: TGroupBox;
    LbFormatoRep: TLabel;
    LbDireccion: TLabel;
    GBPersonasSus: TGroupBox;
    LBPersonasSus: TZetaSmartListBox_DevEx;
    US_EMAIL: TEdit;
    LbNoDatos: TLabel;
    lbFrecuencia: TLabel;
    SU_FRECUEN: TZetaKeyCombo;
    SU_VACIO: TZetaKeyCombo;
    SmartListSuscritos: TZetaSmartLists_DevEx;
    US_FORMATO: TEdit;
    bbSuscripcion: TdxBarButton;
    BPreeliminar: TdxBarButton;
    BAgregaFormulaOrden: TcxButton;
    BTablaPrincipal: TcxButton;
    bDisenador: TcxButton;
    BBorraParametro: TcxButton;
    BAgregaParametro: TcxButton;
    BArribaParametro: TZetaSmartListsButton_DevEx;
    BAbajoParametro: TZetaSmartListsButton_DevEx;
    bFormulaParametro: TcxButton;
    BtnAgregaUsuario: TcxButton;
    BtnBorraUsuario: TcxButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    ImageList: TcxImageList;
    Arbol: TcxTreeView;
    GBEnviosCalendarizados: TGroupBox;
    ImageButtons: TcxImageList;
    btnEditarEnvioProgramado: TcxButton;
    btnAsignarRoles: TcxButton;
    btnAsignarUsuarios: TcxButton;
    btnAutoSuscribirse: TcxButton;
    btnAgregarEnviosProgram: TcxButton;
    btnEliminarCalendarizaciones: TcxButton;
    Expander: TcxButton;
    Compactar: TcxButton;
    procedure FormKeyUp(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure RGOrdenClick(Sender: TObject);
    procedure SmartListParametrosAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure ParametrosClick(Sender: TObject);
    procedure BBorraParametroClick(Sender: TObject);
    procedure BAgregaOrdenClick(Sender: TObject);
    procedure bFormulaParametroClick(Sender: TObject);
    procedure BAgregaParametroClick(Sender: TObject);
    procedure BTablaPrincipalClick(Sender: TObject);
    procedure BPreeliminarClick(Sender: TObject);
    procedure BImprimirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure eFormulaOrdenExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bbSuscripcionClick(Sender: TObject);
    procedure BtnAgregaUsuarioClick(Sender: TObject);
    procedure BtnBorraUsuarioClick(Sender: TObject);
    procedure SmartListSuscritosAlSeleccionar(Sender: TObject; var Objeto: TObject; Texto: String);
    procedure SuscripcionClick(Sender: TObject);
    procedure ExpanderClick(Sender: TObject);
    procedure CompactarClick(Sender: TObject);
    procedure btnEliminarCalendarizacionesClick(Sender: TObject);
    procedure btnEditarEnvioProgramadoClick(Sender: TObject);
    procedure btnAsignarRolesClick(Sender: TObject);
    procedure btnAsignarUsuariosClick(Sender: TObject);
    procedure btnAgregarEnviosProgramClick(Sender: TObject);
    procedure ArbolGetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FExportaUser : String; //NOmbre del archivo a exportar, pero modificado por el usuario
    FCambioArchivo : Boolean; //Bandera para cambio en nombre archivos.
    {$ifdef RDD}
    {$else}
    dmEntidadesShell : TdmEntidadesTress;
    {$endif}
    FRelaciones : TStrings;
    FBitacora : TStrings;
    FClasificacionActiva : eClasifiReporte;
    FPuedeVerSuscrip: Boolean;

    procedure AgregaParametro;
    procedure CambiaTablaPrincipal;
    function EntidadValida(NuevaEntidad: TipoEntidad; oCampo: TCampoMaster;
      lCambiaTabla: Boolean): Boolean;
    function GetDisenaName: string;
    function ValidaPlantilla( var sPlantilla: string ): Boolean;
    procedure GetDatosSuscripcion ( oCampo : TSuscripCampoMaster );
    procedure AgregaSuscripcion(oUsuario : TSuscripCampoMaster);
    procedure AgregaSQLFiltrosEspeciales( const sFiltro: string;const lDeDiccion : Boolean = FALSE );
    function PuedeAgregarSuscripcion( iUsuario:Integer; const lAgregar: Boolean ): Boolean;
    function PuedeSuscribir( const lEsAutoSuscripcion, lEsOtrosUsuarios: Boolean ): Boolean;
    procedure EnabledSuscritos(const lEnable: Boolean);
    procedure EnabledSuscripControls( const lEnable: Boolean );
    procedure IniciarArbol;
    procedure ValidarDerechosCalendarioSuscripciones;
    procedure ValidarHabilitadoBotonesRolesUsuarios;
  protected
    FInicial: boolean;
    fControl : TWinControl;
    SQLAgente : TSQLAgenteClient;
    FDatosImpresion : TDatosImpresion;
    FTipoPantalla : TipoPantalla;
    FFiltroFormula : string;
    FFiltroDescrip : string;
    FEntidadActual : TipoEntidad;
    FHayImagenes : Boolean;
    Procedure AbreDisenador( const sFileName: string );
    procedure AgregaListaParametro;override;
    procedure LimpiaListas;override;
    procedure LlenaListas;override;
    procedure PosicionaListas;override;
    procedure IniciaListas;override;
    procedure GrabaListas;override;
    procedure GrabaOrden;override;
    procedure GrabaParametros;
    procedure GrabaSuscripciones;
    procedure HabilitaControles;override;

    //LISTA ORDEN
    procedure ActualizaOrden(oOrden: TOrdenOpciones);override;
    procedure ActualizaOrdenObjeto;
    procedure AsignaProcOrden(oProc: TNotifyEvent);
    procedure AgregaFormulaOrden;
    procedure EnabledOrden(const bEnable, bFormula : Boolean );override;
    procedure EnabledGrupos(const bEnable, bFormula: Boolean);virtual;

    //LISTA PARAMETRO
    procedure ActualizaParametro(oParametro: TCampoMaster);
    procedure ActualizaParametroObjeto;
    procedure AsignaProcParametro(oProc: TNotifyEvent);
    procedure EnabledParametro(bEnable: Boolean);

    //LISTA SUSCRITOS
    procedure ActualizaSuscrito(oUsuario: TSuscripCampoMaster);
    procedure ActualizaSuscritoObjeto;
    procedure AsignaProcSuscrito(oProc: TNotifyEvent);

    //CAMBIO DE TABLA PRINCIPAL
    procedure AlBorrarDatos( const oEntidad : TipoEntidad );virtual;
    procedure AlRevisarDatos( const oEntidad : TipoEntidad );virtual;
    procedure DespuesDeBorrarDatos;virtual;
    procedure BorraDatos( oLista: TStrings;
                          const oEntidad: TipoEntidad;
                          const Inicio: integer);
    procedure Revisa( oLista: TStrings;
                      const oEntidad: TipoEntidad;
                      const sTipoCampo: string;
                      const Inicio: integer);
    function GetGruposStrings: TStrings;virtual;

    {GENERACION DE SQL}
    procedure AgregaSQLCampoRep;virtual;abstract;
    procedure AgregaSQLColumnas(oCampo: TCampoOpciones; const Banda : integer = -1);
    procedure AgregaSQLColumnasParam(oParam : TCampoMaster);
    procedure AgregaSQLFiltros(oFiltro: TFiltroOpciones);
    procedure AgregaSQLGrupos(oGrupo: TGrupoOpciones);
    procedure AgregaSQLOrdenes(oOrden: TOrdenOpciones);

    procedure AntesDeConstruyeSQL;virtual;abstract;
    procedure DespuesDeConstruyeSQL;virtual;
    procedure DespuesDeGeneraReporte;virtual;
    function GeneraReporte: Boolean;virtual;abstract;
    {Metodos para la impresion de Reportes}
    procedure ImpresionReporte( const Tipo : TipoPantalla );override;

    function GetFirstPestana: TcxTabSheet;override;

    procedure CreaEntidadesShell;
    procedure ExisteDirectorio(const sArchivo: string;oControl: TWinControl);
    procedure ParametrosMovimienLista(FParamList : TZetaParams);virtual;
    procedure ParametrosListadoNomina(FParamList : TZetaParams);virtual;
    procedure Connect;override;
    function ConstruyeFormula(var oMemo: TCustomMemo): Boolean;overload;override;
    function ConstruyeFormula(var oMemo: TcxCustomMemo): Boolean;overload;override;
    property ExportaUser : string read FExportaUser write FExportaUser;
    property CambioArchivo : boolean read FCambioArchivo write FCambioArchivo;
    function GeneraNombreArchivo(const sNombreArchivo:string): String;
  public
    procedure Preview;override;
    procedure Imprime;override;
    {GENERACION DE SQL}
    function GeneraSQL: Boolean;
    procedure RolesUsuarios (lEnabled : Boolean);
  end;

var
  EditBaseReportes_DevEx: TEditBaseReportes_DevEx;

implementation

uses
     FTablaPrincipal_DevEx,
     FDialogoPrinc_DevEx,
     ZetaCommonTools,
     ZReportConst,
     ZetaDialogo,
     ZConstruyeFormula,
     ZFuncsCliente,
     DReportes,
     DCliente,
     DSistema,
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     ZArbolCalendarioReportes,
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     ZetaSystemWorking,
     ZAccesosMgr,
     ZAccesosTress,
     DDiccionario,
     ZetaBusqueda_DevEx,
     ZBaseDlgModal_DevEx,
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     FSistSuscripcionUsuarios_DevEx,
     FSistSuscripcionRoles_DevEx,
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     FTressShell,
     ZcxWizardBasico,
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     FWizSuscripcionReportes_DevEx,
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ifdef CAROLINA}
     DBaseReportes,
     {$endif}
     {$ifdef CAlSONIC}
     DGlobal, ZGlobalTress,
     {$endif}
     ZetaClientTools;

{$R *.DFM}

const K_PROPIEDAD_PARAM = 'Propiedades del Par�metro';

procedure TEditBaseReportes_DevEx.AgregaListaParametro;
var  oParametro : TCampoMaster;
begin
     oParametro := TCampoMaster.Create;
     oParametro.TipoCampo := eTipoGlobal( CampoRep['CR_TFIELD'] );
     GetDatosGenerales( oParametro );
     LBParametros.Items.AddObject( oParametro.Titulo, oParametro );
end;

procedure TEditBaseReportes_DevEx.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     inherited;
     if ( (Shift = [ssCtrl]) OR (Shift  = [ssCtrl,ssShift]) ) then
     begin
          if ( Chr(Key) = 'P' ) OR ( Chr(Key) = 'I' ) then
          begin
               cbBitacora.Checked := cbBitacora.Checked OR (Shift = [ssCtrl,ssShift]);
               if ( Chr(Key) = 'P' ) then ImpresionReporte( tgPreview )
               else ImpresionReporte( tgImpresora ) ;
               Key := 0;
          end;
     end
end;


procedure TEditBaseReportes_DevEx.PosicionaListas;
begin
     inherited;
     SmartListParametros.SelectEscogido( 0 );
     SmartListSuscritos.SelectEscogido( 0 );
end;

procedure TEditBaseReportes_DevEx.LimpiaListas;
begin
     Inherited;
     LBParametros.Items.Clear;
     LBPersonasSus.Items.Clear;

     EnabledParametro( FALSE );
     EnabledSuscritos( FALSE );
end;

//MANEJO DE LISTA ORDEN
procedure TEditBaseReportes_DevEx.ActualizaOrden( oOrden : TOrdenOpciones );
begin
     AsignaProcOrden( NIL );
     ETituloOrden.Text := oOrden.Titulo;
     EFormulaOrden.Text := oOrden.Formula;
     RGOrden.ItemIndex := oOrden.DireccionOrden;
     AsignaProcOrden( RGOrdenClick );
     EnabledOrden( TRUE, oOrden.Calculado<>0 );
end;

procedure TEditBaseReportes_DevEx.AsignaProcOrden( oProc : TNotifyEvent );
begin
     RgOrden.OnClick := oProc;
end;

procedure TEditBaseReportes_DevEx.EnabledOrden( const bEnable, bFormula : Boolean  );
begin
     inherited;
     //BBorraOrden.Enabled := bEnable;
     RGOrden.Enabled := bEnable AND NOT bFormula;
     //if NOT bEnable then EFormulaOrden.Text := '';
end;

procedure TEditBaseReportes_DevEx.RGOrdenClick(Sender: TObject);
begin
     inherited;
     ActualizaOrdenObjeto;
end;

procedure TEditBaseReportes_DevEx.ActualizaOrdenObjeto;
 var  Objeto : TOrdenOpciones;
begin
     with LBOrden, Items do
     begin
          Objeto := TOrdenOpciones( Objects[ ItemIndex ] );
          if Objeto <> NIL then
          begin
               Objeto.Titulo := eTituloOrden.Text;
               Items[ItemIndex] := Objeto.Titulo;
               Objeto.Formula := eFormulaOrden.Text;
               Objeto.DireccionOrden := RGOrden.ItemIndex;
               Modo := dsEdit;
          end;
     end;
end;

procedure TEditBaseReportes_DevEx.GrabaOrden;
 var i: integer;
begin
     with LBOrden.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoGeneral( TCampoMaster(Objects[i]),
                                  tcOrden, i, -1 );
               with TOrdenOpciones(Objects[i]) do
               begin
                    CampoRep.FieldByName('CR_CALC').AsInteger := DireccionOrden;
                    CampoRep.FieldByName('CR_COLOR').AsInteger := Calculado;
               end;
               CampoRep.Post;
          end;
end;

procedure TEditBaseReportes_DevEx.GrabaParametros;
 var i: integer;
begin
     with LBParametros.Items do
          for i:= 0 to Count -1 do
          begin
               CampoRep.Append;
               GrabaCampoGeneral( TCampoMaster(Objects[i]),
                                  tcParametro, i, -1 );
               CampoRep.Post;
          end;
end;


procedure TEditBaseReportes_DevEx.GrabaListas;
begin
     inherited;
     GrabaParametros;
     GrabaSuscripciones;
end;



//Lista de PARAMETROS
procedure TEditBaseReportes_DevEx.SmartListParametrosAlSeleccionar(
  Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaParametro( TCampoMaster( Objeto ) );
          Texto := TCampoMaster( Objeto ).Titulo;
          BAgregaParametro.Enabled := LBParametros.Items.Count < 10;
     end
     else EnabledParametro( FALSE );
end;

procedure TEditBaseReportes_DevEx.ActualizaParametro( oParametro : TCampoMaster );
begin
     AsignaProcParametro( NIL );
     GBPropiedadesParametro.Caption := K_PROPIEDAD_PARAM + ' #' + IntToStr( LBParametros.ItemIndex  + 1 );
     ETituloParametro.Text := oParametro.Titulo;
     EFormulaParametro.Text := oParametro.Formula;
     AsignaProcParametro( ParametrosClick );
     EnabledParametro( TRUE );
end;

procedure TEditBaseReportes_DevEx.AsignaProcParametro( oProc : TNotifyEvent );
begin
     ETituloParametro.OnExit := oProc;
     EFormulaParametro.OnExit := oProc;
end;

procedure TEditBaseReportes_DevEx.EnabledParametro( bEnable : Boolean );
begin
     ETituloParametro.Enabled := bEnable;
     lTituloParametro.Enabled := bEnable;
     EFormulaParametro.Enabled := bEnable;
     lFormulaParametro.Enabled := bEnable;
     bFormulaParametro.Enabled := bEnable;
     BBorraParametro.Enabled := bEnable;
     if NOT bEnable then
     begin
          GBPropiedadesParametro.Caption := K_PROPIEDAD_PARAM;
          ETituloParametro.Text := '';
          EFormulaParametro.Text := '';
     end;
end;


procedure TEditBaseReportes_DevEx.ActualizaParametroObjeto;
 var Objeto : TCampoMaster;
     i : integer;
begin
     i := LBParametros.ItemIndex;
     Objeto := TCampoMaster( LBParametros.Items.Objects[ i ] );
     if Objeto <> NIL then
     begin
          Objeto.Titulo := ETituloParametro.Text;
          Objeto.Formula := EFormulaParametro.Text;
          Objeto.TipoCampo := tgAutomatico;
          LBParametros.Items[ i ] := Objeto.Titulo;
          Modo := dsEdit;
     end;
end;

procedure TEditBaseReportes_DevEx.ParametrosClick(Sender: TObject);
begin
     inherited;
     ActualizaParametroObjeto
end;

procedure TEditBaseReportes_DevEx.BBorraParametroClick(Sender: TObject);
begin
     inherited;
     BorraDato( SmartListParametros, bAgregaParametro, bBorraParametro, LBParametros );
     EnabledParametro(LBParametros.Items.Count > 0);
end;

procedure TEditBaseReportes_DevEx.BAgregaOrdenClick(Sender: TObject);
begin
     with TBitBtn(Sender) do
     begin
          SetFocus;
          if Tag = 0 then AgregaOrden
          else AgregaFormulaOrden;
     end;
end;


procedure TEditBaseReportes_DevEx.bFormulaParametroClick(Sender: TObject);
begin
     inherited;
     ConstruyeFormula( TcxCustomMemo(EFormulaParametro) );
     ActualizaParametroObjeto;
end;


procedure TEditBaseReportes_DevEx.BAgregaParametroClick(Sender: TObject);
begin
     inherited;
     AgregaParametro;
end;

procedure TEditBaseReportes_DevEx.AgregaParametro;
 var oParametro : TCampoMaster;
begin
     BAgregaParametro.SetFocus;
     oParametro := TCampoMaster.Create;
     oParametro.TipoCampo := tgAutomatico;
     oParametro.Titulo := K_PARAMETRO + IntToStr( LBParametros.Items.Count + 1 );
     DespuesDeAgregarDato( oParametro,
                           SmartListParametros,
                           BBorraParametro,
                           ETituloParametro );
     Modo := dsEdit;
end;

procedure TEditBaseReportes_DevEx.BTablaPrincipalClick(Sender: TObject);
begin
     inherited;
     CambiaTablaPrincipal;
     SetLookupCondicion;
end;

procedure TEditBaseReportes_DevEx.btnAgregarEnviosProgramClick(Sender: TObject);
begin
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     inherited;
     try
        dmSistema.CodigoReporte := VACIO;
        dmSistema.NombreReporte := VACIO;
        dmSistema.TituloReporte := VACIO;
        dmSistema.FolioCalendario := 0;
        dmSistema.EditarCalendario := FALSE;

        dmSistema.CodigoReporte := Reporte.FieldByName('RE_CODIGO').AsString;
        dmSistema.NombreReporte := Reporte.FieldByName('RE_NOMBRE').AsString;
        dmSistema.TituloReporte := dmSistema.NombreReporte;

        with dmSistema.cdsSistTareaCalendario do
        begin
             Conectar;
             Append;
        end;

        ZcxWizardBasico.ShowWizard(TWizSuscripcionReportes_DevEx);
        IniciarArbol;
        ValidarHabilitadoBotonesRolesUsuarios;
        dmReportes.cdsReportes.Refrescar;
        dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, []);
        RolesUsuarios(False);
     except
     end;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

procedure TEditBaseReportes_DevEx.BorraDatos( oLista : TStrings; const oEntidad : TipoEntidad; const Inicio : integer );
var i : integer;
begin
     i := Inicio;
     while i < oLista.Count do
     begin
          with oLista do
               if NOT EntidadValida( oEntidad, TCampoMaster( Objects[ i ] ), TRUE ) then
               begin
                    oLista.Delete( i );
                    i := Inicio;
               end
               else i := i + 1;
     end;
end;

procedure TEditBaseReportes_DevEx.Revisa( oLista : TStrings; const oEntidad : TipoEntidad; const sTipoCampo : string; const Inicio : integer );
var i : integer;
begin
     for i := Inicio to oLista.Count -1 do
     begin
          with oLista do
               if NOT EntidadValida( oEntidad, TCampoMaster( Objects[ i ]), FALSE ) then
                  fBitacora.Add( PadR( sTipoCampo + ' # ' + IntToStr(i) +
                                 ' : ' + TCampoMaster( Objects[ i ]).Titulo, 40 ) );
     end;
end;

procedure TEditBaseReportes_DevEx.AlBorrarDatos(const oEntidad : TipoEntidad);
begin
     BorraDatos( LBOrden.Items, oEntidad, 0 );
     BorraDatos( LBFiltros.Items, oEntidad, 0 );
end;

procedure TEditBaseReportes_DevEx.AlRevisarDatos(const oEntidad : TipoEntidad);
begin
     Revisa( LBOrden.Items, oEntidad, 'Orden', 0 );
     Revisa( LBFiltros.Items, oEntidad, 'Filtro', 0 );
end;

procedure TEditBaseReportes_DevEx.ArbolGetSelectedIndex(Sender: TObject; Node: TTreeNode);
const
     K_IGUAL = ' = ';
     K_IMAGEN_USUARIO = 0;
     K_IMAGEN_ROL = 1;
     K_IMAGEN_CALENDARIO = 2;
     K_IMAGEN_USUARIO_BOLD = 3;
     K_IMAGEN_ROL_BOLD = 4;
     K_IMAGEN_CALENDARIO_BOLD = 5;
  var
     iImageIndex : integer;
     lEnabled : Boolean;
begin
     inherited;
     if Assigned( Arbol.Selected ) then
     begin
          lEnabled := True;
          iImageIndex := Arbol.Selected.ImageIndex;

          case iImageIndex of
               K_IMAGEN_USUARIO: Arbol.Selected.SelectedIndex := K_IMAGEN_USUARIO_BOLD;
               K_IMAGEN_ROL: Arbol.Selected.SelectedIndex := K_IMAGEN_ROL_BOLD;
               K_IMAGEN_CALENDARIO: Arbol.Selected.SelectedIndex := K_IMAGEN_CALENDARIO_BOLD;
          end;
     end
     else
         lEnabled := False;

     RolesUsuarios(lEnabled);
end;

procedure TEditBaseReportes_DevEx.CambiaTablaPrincipal;
 var iActual, iNueva : TipoEntidad;


 procedure CambiaTabla;
 begin
      {$ifdef RDD}
      if dmDiccionario.ExisteEntidadEnTablasPorClasificacion( iNueva ) then
      begin
      {$endif}
           AlBorrarDatos(iNueva);
           ZInformation( Caption,
                         'La Tabla Principal fue Cambiada de' + CR_LF +
                         dmReportes.ObtieneEntidad( iActual ) + ' a ' +
                         dmReportes.ObtieneEntidad( iNueva ),
                         0 );
           if (Modo = dsBrowse) or (Reporte.State = dsBrowse) then
           begin
                Modo := dsEdit;
                Reporte.Edit;
           end;

           {$ifdef RDD}
           Reporte.FieldByName('EN_NIVEL0').AsString := dmDiccionario.GetEsTablaNivel0;
           Reporte.FieldByName('EN_TABLA').AsString := dmDiccionario.GetNombreTabla( iNueva );
           {$endif}

           Reporte['RE_ENTIDAD'] := iNueva;
           {$ifdef RDD}
           Reporte['RE_TABLA'] := dmReportes.ObtieneEntidad( iNueva );
           {$endif}
           DespuesDeBorrarDatos;
      {$ifdef RDD}
      end
      else
          ZError( Caption, Format( 'Error al buscar la Tabla #%d.' + CR_LF + 'La Tabla Principal no pudo ser cambiada.' +CR_LF + 'El Reporte permanece sin cambios.' , [ Ord( iNueva ) ] ),0 );
      {$endif}
 end;
begin
     iNueva := Entidad;
     iActual := iNueva;
     if FRelaciones <> NIL then FRelaciones.Clear;
     ShowTablaPrincipal_DevEx( iNueva, eClasifiReporte(Reporte['RE_CLASIFI']) );
     {$ifndef SELECCION}
     if dmCliente.ModoTress then
     {$endif}
     begin
        if (iNueva <> iActual) then
        begin
             if fBitacora = NIL then
                fBitacora := TStringList.Create
             else fBitacora.Clear;

             AlRevisarDatos(iNueva);

             if fBitacora.Count > 0 then
             begin
                  if ShowDialogoPrinc( fBitacora ) then
                     CambiaTabla;
             end
             else CambiaTabla;
        end;
     end;
end;


procedure TEditBaseReportes_DevEx.CreaEntidadesShell;
begin
     {$ifdef RDD}
     {$else}
     if dmEntidadesShell = NIL then
        dmEntidadesShell := TdmEntidadesTress.Create( self );
     {$endif}
end;

procedure TEditBaseReportes_DevEx.btnEliminarCalendarizacionesClick(Sender: TObject);
const
     DELETE_USUARIO = 1;
     DELETE_ROL = 2;
     COUNT_CALENDARIO_REGISTROS = 3;
     TAREA_CALENDARIO = 'TAREA_CALENDARIO';
     FILTRO_COUNT = '((RO_CODIGO <> '') or (US_CODIGO <> 0) AND ( CA_FOLIO = %d ))';
var
   iIndiceRegistro, iContadorRegistros : integer;
   sRoCodigo, sUsuarioNombre, sRolNombre, sCalendarioNombre : string;
   iCaFolio, iUsCodigo : integer;
   sQuery : string;
begin
     {$IFNDEF SUPERVISORES}
     inherited;
     try
        iIndiceRegistro := 0;
        iContadorRegistros := 0;
        if (Arbol.Selected <> Nil) then
        begin
             iIndiceRegistro := Integer(Arbol.Selected.Data);
        end;
        sRoCodigo := VACIO;
        iCaFolio := 0;
        iUsCodigo := 0;
        sUsuarioNombre := VACIO;
        sRolNombre := VACIO;
        sCalendarioNombre := VACIO;

        if (iIndiceRegistro <> 0) then
        begin
             dmSistema.cdsSuscripCalendarioReportes.Locate('ORDEN', iIndiceRegistro, []);
             sRoCodigo := dmSistema.cdsSuscripCalendarioReportes.FieldByName('RO_CODIGO').AsString;
             iCaFolio  := dmSistema.cdsSuscripCalendarioReportes.FieldByName('CA_FOLIO').AsInteger;
             iUsCodigo  := dmSistema.cdsSuscripCalendarioReportes.FieldByName('US_CODIGO').AsInteger;
             sUsuarioNombre := dmSistema.cdsSuscripCalendarioReportes.FieldByName('US_NOMBRE').AsString;
             sRolNombre := dmSistema.cdsSuscripCalendarioReportes.FieldByName('RO_NOMBRE').AsString;
             sCalendarioNombre := dmSistema.cdsSuscripCalendarioReportes.FieldByName('CA_NOMBRE').AsString;
             if Arbol.Selected.Level = 0 then //ES UNA TAREA DE CALENDARIO O NODO PRINCIPAL?
             begin
                  dmSistema.EliminarUsuariosRolesCalendario(COUNT_CALENDARIO_REGISTROS, iCaFolio, iUsCodigo, sRoCodigo, iContadorRegistros);
                  if iContadorRegistros = 0 then
                  begin
                       dmSistema.cdsSistTareaCalendario.Locate('CA_FOLIO', iCaFolio, []);
                       dmSistema.cdsSistTareaCalendario.Borrar;
                       IniciarArbol;
                       dmReportes.cdsReportes.Refrescar;
                       dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, [])
                  end
                  else
                  begin
                       zetadialogo.ZWarning(Caption,'No es posible eliminar este env�o programado, a�n tiene asignados usuarios/roles.',0, mbok);
                  end;
                  ValidarHabilitadoBotonesRolesUsuarios;
             end
             else
             begin
                  if (iUsCodigo > 0) and (sRoCodigo = VACIO) then
                  begin
                       if ZetaDialogo.zConfirm( 'Eliminaci�n de Usuarios en Env�o Programado', '� Desea eliminar la asignaci�n del usuario '+sUsuarioNombre+' en el env�o programado '+sCalendarioNombre+' ?', 0, mbYes ) then
                       begin
                            dmSistema.EliminarUsuariosRolesCalendario(DELETE_USUARIO, iCaFolio, iUsCodigo, sRoCodigo, iContadorRegistros);
                            Arbol.Selected.Delete;
                            dmReportes.cdsReportes.Refrescar;
                            dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, [])
                            //IniciarArbol;
                       end;
                  end
                  else if(sRoCodigo <> VACIO) then
                  begin
                       if (iUsCodigo > 0) then
                       begin
                            if ZetaDialogo.zConfirm( 'Eliminaci�n de Roles en Env�o Programado', '� Desea eliminar la asignaci�n del rol '+sRolNombre+' en el env�o programado '+sCalendarioNombre+' ?', 0, mbYes ) then
                            begin
                                 dmSistema.EliminarUsuariosRolesCalendario(DELETE_ROL, iCaFolio, iUsCodigo, sRoCodigo, iContadorRegistros);
                                 Arbol.Selected.Parent.Delete; //SI ESTA SELECCIONANDO UN USUARIO DENTRO DE UN ROL ELIMINA EL ROL PADRE DEL USUARIO
                                 dmReportes.cdsReportes.Refrescar;
                                 dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, [])
                                 //IniciarArbol;
                            end;
                       end
                       else
                       begin
                            if ZetaDialogo.zConfirm( 'Eliminaci�n de Roles en Env�o Programado', '� Desea eliminar la asignaci�n del rol '+sRolNombre+' en el env�o programado '+sCalendarioNombre+' ?', 0, mbYes ) then
                            begin
                                 dmSistema.EliminarUsuariosRolesCalendario(DELETE_ROL, iCaFolio, iUsCodigo, sRoCodigo, iContadorRegistros);
                                 Arbol.Selected.Delete; //SI ESTA SELECCIONANDO EL ROL ELIMINA LO ELIMINA
                                 dmReportes.cdsReportes.Refrescar;
                                 dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, [])
                                 //IniciarArbol;
                            end;
                       end;
                  end;
             end;
        end;
     except
     end;
     {$ENDIF}
end;

procedure TEditBaseReportes_DevEx.btnEditarEnvioProgramadoClick(Sender: TObject);
var
   iIndiceRegistro : integer;
   iCaFolio : integer;
begin
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     {$IFNDEF RDDAPP}
     inherited;
     try
        iIndiceRegistro := 0;
        if (Arbol.Selected <> Nil) then
        begin
             iIndiceRegistro := Integer(Arbol.Selected.Data);
        end;
        iCaFolio := 0;

        if (iIndiceRegistro <> 0) then
        begin
             dmSistema.cdsSuscripCalendarioReportes.Locate('ORDEN', iIndiceRegistro, []);
             iCaFolio  := dmSistema.cdsSuscripCalendarioReportes.FieldByName('CA_FOLIO').AsInteger;

             dmSistema.CodigoReporte := VACIO;
             dmSistema.NombreReporte := VACIO;
             dmSistema.TituloReporte := VACIO;
             dmSistema.FolioCalendario := 0;

             dmSistema.EditarCalendario := TRUE;
             dmSistema.CodigoReporte := Reporte.FieldByName('RE_CODIGO').AsString;
             dmSistema.NombreReporte := Reporte.FieldByName('RE_NOMBRE').AsString;
             dmSistema.TituloReporte := dmSistema.NombreReporte;
             dmSistema.FolioCalendario := iCaFolio;

             ZcxWizardBasico.ShowWizard(TWizSuscripcionReportes_DevEx);
             IniciarArbol;
        end;
     except
     end;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

procedure TEditBaseReportes_DevEx.btnAsignarRolesClick(Sender: TObject);
var
   iIndiceRegistro, iSelectedIndex : integer;
   sRoCodigo : string;
   iCaFolio, iUsCodigo : integer;
begin
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     inherited;
     try
        iIndiceRegistro := 0;
        if (Arbol.Selected <> Nil) then
        begin
             iIndiceRegistro := Integer(Arbol.Selected.Data);
             iSelectedIndex := Arbol.Selected.AbsoluteIndex;
             if (iIndiceRegistro = 0) and (Arbol.Selected.Level = 1) then
             begin
                  iIndiceRegistro := Integer(Arbol.Selected.Parent.Data);
             end;
        end;
        sRoCodigo := VACIO;
        iCaFolio := 0;
        iUsCodigo := 0;

        if (iIndiceRegistro <> 0) then
        begin
             dmSistema.cdsSuscripCalendarioReportes.Locate('ORDEN', iIndiceRegistro, []);
             sRoCodigo := dmSistema.cdsSuscripCalendarioReportes.FieldByName('RO_CODIGO').AsString;
             iCaFolio  := dmSistema.cdsSuscripCalendarioReportes.FieldByName('CA_FOLIO').AsInteger;
             iUsCodigo := dmSistema.cdsSuscripCalendarioReportes.FieldByName('US_CODIGO').AsInteger;
             dmSistema.cdsSistTareaCalendario.Locate('CA_FOLIO', iCaFolio, []);
             dmSistema.EnvioProgramadoXEmpresa := False;
             ZBaseDlgModal_DevEx.ShowDlgModal( SistSuscripcionRoles_DevEx, TSistSuscripcionRoles_DevEx );
             dmSistema.EnvioProgramadoXEmpresa := True;
             IniciarArbol;
             dmReportes.cdsReportes.Refrescar;
             dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, []);

             Arbol.Selected:= Arbol.Items[iSelectedIndex];
        end;
        except
     end;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

procedure TEditBaseReportes_DevEx.btnAsignarUsuariosClick(Sender: TObject);
var
   iIndiceRegistro, iSelectedIndex: integer;
   sRoCodigo : string;
   iCaFolio, iUsCodigo : integer;
   tNode : TTreeNode;
begin
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     inherited;
     try
        iIndiceRegistro := 0;
        if (Arbol.Selected <> Nil) then
        begin
             iIndiceRegistro := Integer(Arbol.Selected.Data);
             iSelectedIndex := Arbol.Selected.AbsoluteIndex;
             if (iIndiceRegistro = 0) and (Arbol.Selected.Level = 1) then
             begin
                  iIndiceRegistro := Integer(Arbol.Selected.Parent.Data);
             end;
        end;
        sRoCodigo := VACIO;
        iCaFolio := 0;
        iUsCodigo := 0;

        if (iIndiceRegistro <> 0) then
        begin
             dmSistema.cdsSuscripCalendarioReportes.Locate('ORDEN', iIndiceRegistro, []);
             sRoCodigo := dmSistema.cdsSuscripCalendarioReportes.FieldByName('RO_CODIGO').AsString;
             iCaFolio  := dmSistema.cdsSuscripCalendarioReportes.FieldByName('CA_FOLIO').AsInteger;
             iUsCodigo  := dmSistema.cdsSuscripCalendarioReportes.FieldByName('US_CODIGO').AsInteger;
             dmSistema.cdsSistTareaCalendario.Locate('CA_FOLIO', iCaFolio, []);
             dmSistema.EnvioProgramadoXEmpresa := false;
             ZBaseDlgModal_DevEx.ShowDlgModal( SistSuscripcionUsuarios_DevEx, TSistSuscripcionUsuarios_DevEx );
             dmSistema.EnvioProgramadoXEmpresa := true;
             IniciarArbol;
             dmReportes.cdsReportes.Refrescar;
             dmReportes.cdsReportes.Locate('RE_CODIGO', dmSistema.CodigoReporte, []);
             Arbol.Selected:= Arbol.Items[iSelectedIndex];
        end
        except
     end;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

function TEditBaseReportes_DevEx.EntidadValida( NuevaEntidad : TipoEntidad; oCampo : TCampoMaster; lCambiaTabla : Boolean ) : Boolean;
 procedure GetRelaciones;
 begin
      {$ifdef RDD}
      {$else}
      if FRelaciones = NIL then
         FRelaciones := TStringList.Create;

      if FRelaciones.Count = 0 then
      begin
           with TStringList(fRelaciones) do
           begin
                Sorted := TRUE;
                Duplicates := dupIgnore;
           end;

           CreaEntidadesShell;

           with dmEntidadesShell do
                RelacionesEntidad( NuevaEntidad, TStringList(fRelaciones), TRUE );

      end;
      {$endif}
 end;

begin
     Result := (oCampo.Calculado < 0) OR (oCampo.Entidad = NuevaEntidad);

     if ( NOT Result ) AND (oCampo.Calculado = 0) then
     begin
          {$ifdef RDD}
          Result := dmDiccionario.EntidadValida(NuevaEntidad, oCampo.Entidad ) ;
          {$else}
          GetRelaciones;
          Result := FRelaciones.IndexOf( IntToStr(Ord(oCampo.Entidad)) )>=0;
          {$endif}
     end;
end;

procedure TEditBaseReportes_DevEx.DespuesDeBorrarDatos;
begin
      EnabledOrden( NOT LbOrden.Items.Count = 0, FALSE );
      SmartListOrden.SelectEscogido( 0 );
      EnabledFiltro( NOT LbFiltros.Items.Count = 0 );
      SmartListFiltros.SelectEscogido( 0 );
end;

procedure TEditBaseReportes_DevEx.IniciaListas;
begin
     inherited;
     LBParametros.OnDblClick := LbListaDblClick;
     LBPersonasSus.OnDblClick := LbListaDblClick;
end;

procedure TEditBaseReportes_DevEx.BPreeliminarClick(Sender: TObject);
{$IFDEF PREVIEW_100}
 //var i: integer;
{$ENDIF}
begin
     inherited;
     {$IFDEF PREVIEW_100}
     while TRUE do
     {$ENDIF}
           ImpresionReporte( tgPreview );
end;

procedure TEditBaseReportes_DevEx.BImprimirClick(Sender: TObject);
begin
     inherited;
     ImpresionReporte( tgImpresora );
end;


{*************************************************}
{*************** GENERACION DE SQL ***************}
{*************************************************}
{function TEditBaseReportes.EvaluaParametros( var sError : wideString;
                                             var oParams : OleVariant;
                                             const lMuestraDialogo : Boolean ) : Boolean;
 var i, iCount: integer;
     oParam : OleVariant;
     oColumna : TSQLColumna;
begin
     Result := TRUE;
     iCount := LBParametros.Items.Count;
     oParams := VarArrayCreate([1, K_MAX_PARAM ], varVariant);

     if iCount > 0 then
     begin
          for i:=0 to iCount-1 do
              AgregaSQLColumnasParam(TCampoMaster(LBParametros.Items.Objects[i]));

          Result := dmReportes.EvaluaParams( SQLAgente, sError );

          if Result then
          begin
               for i := 0 to LBParametros.Items.Count -1do
               begin
                   with TCampoMaster(LBParametros.Items.Objects[i]) do
                        if (PosAgente >= 0) then
                        begin
                             oColumna := SQLAgente.GetColumna( PosAgente );
                             oParam := VarArrayCreate([1, 3], varVariant);
                             oParam[1] := Titulo;
                             oParam[2] := oColumna.Formula;
                             oParam[3] := oColumna.TipoFormula;
                        end
                        else
                        begin
                             oParam := VarArrayCreate([1, 3], varVariant);
                             oParam[1] := Titulo;
                             oParam[2] := Formula;
                             oParam[3] := TipoCampo;
                        end;
                   oParams[i+1] := oParam;
               end;

               if lMuestraDialogo then
                  Result := FParametros.MuestraParametros( oParams, lBParametros.Items.Count );
          end;
     end;
end;
}
function TEditBaseReportes_DevEx.GeneraSQL : Boolean;
 var sError : WideString;
     oParams : OleVariant;
     FParamList : TZetaParams;

 function GetCampoCB_Codigo( const iEntidad: TipoEntidad ) : string;
  var
     sTabla : string;
 begin
      //Los proyectos de Seleccion y Visitantes no deben de llegar a este punto.
      {$ifndef RDD}
      sTabla := dmDiccionario.GetNombreTabla( iEntidad );
      {$else}
      sTabla := Reporte.FieldByName('EN_TABLA').AsString ;
      {$endif}
      {$ifdef TRESS}
      case iEntidad of
           enEmbarazo,enMedEntregada,enConsulta,enAccidente : Result := 'or EXPEDIEN.CB_CODIGO = 0';
           enPoll: Result := ' or '+ sTabla + '.PO_NUMERO = 0';
           enInvitacion: Result := 'or INVITA.CB_CODIGO = 0';
           enRotacion,enDemografica  : Result := VACIO;
           else
               Result := ' or '+ sTabla + '.CB_CODIGO = 0';
      end;
      {$endif}
 end;
     {$ifdef TRESS}
var
     i: integer;
     {$endif}
begin
     SQLAgente.Clear;
     FFiltroFormula :=  '';
     FFiltroDescrip := '';
     FHayImagenes := FALSE;

     FParamList := TZetaParams.Create;
     try
        Result := dmReportes.EvaluaParametros( SqlAgente, LbParametros.Items, sError, oParams, TRUE );
        if Result then
        begin
             dsResultados.DataSet.Active := FALSE;
             SQLAgente.Clear;
             SQLAgente.Entidad := TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger);
             SQLAgente.Parametros := oParams;

             if (FFiltroEspecial = '') then
             begin
                  if Trim(QU_CODIGO.LLave) > '' then
                  begin
                       with QU_CODIGO.LookUpDataset do
                       begin
                            if QU_CODIGO.LLave = FieldByName('QU_CODIGO').AsString then
                               AgregaSQLFiltrosEspeciales(FieldByName('QU_FILTRO').AsString);
                       end;
                  end;
                  AgregaSQLFiltrosEspeciales(Reporte.FieldByName('RE_FILTRO').AsString);

                  {$ifdef ANTES}
                  if Strlleno(dmCliente.Confidencialidad) and
                  begin
                       {$ifdef MULTIPLES_ENTIDADES}
                       Dentro( Entidad , EntidadConCondiciones ) then
                       {$else}
                       (Entidad in EntidadConCondiciones) then
                       {$endif}
                       AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_NIVEL0=''%s''',[dmCliente.Confidencialidad]),
                                                   FALSE );
                  end;
                  {$else}
                  if Strlleno(dmCliente.Confidencialidad) then
                  begin
                       {$ifdef MULTIPLES_ENTIDADES}
                               if Dentro( Entidad , EntidadConCondiciones )
                         {$IFDEF RDD}
                               or zStrToBool( Reporte.FieldByName('EN_NIVEL0').AsString )
                         {$ENDIF}
                               then
                       {$else}
                              if (Entidad in EntidadConCondiciones) then
                       {$endif}
                       {$ifdef CONFIDENCIALIDAD_MULTIPLE}
                        begin
                        AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_CODIGO > 0 %s ',[GetCampoCB_Codigo(Entidad) ]), FALSE );
                        AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_NIVEL0 in %s %s ',[dmCliente.ConfidencialidadListaIN, GetCampoCB_Codigo(Entidad) ]), TRUE );
                        end;
                       {$else}
                       AgregaSQLFiltrosEspeciales( Format('COLABORA.CB_NIVEL0=''%s'' %s ',[dmCliente.Confidencialidad, GetCampoCB_Codigo(Entidad) ]),
                                                   FALSE );
                       {$endif}
                  end;
                  {$endif}
                  AgregaSQLCampoRep;
                  {$ifdef TRESS}
                  if dmCliente.ModoSuper
                     {a futuro PENDIENTECARO:
                     OR ( Reporte.FieldByName('RE_IFECHA').AsInteger = 1 )} then
                       {$ifdef MULTIPLES_ENTIDADES}
                       if Dentro( SQLAgente.Entidad , EntidadConCondiciones )
                         {$IFDEF RDD}
                               or zStrToBool( Reporte.FieldByName('EN_NIVEL0').AsString )
                         {$ENDIF}
                               then
                       {$else}
                       if SQLAgente.Entidad in EntidadConCondiciones then
                       {$endif}
                       begin
                            SQLAgente.AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 30, 'EMPLEADO_SUPERVISOR' );
                            AgregaSQLFiltrosEspeciales( dmCliente.GetListaEmpleados('COLABORA.CB_CODIGO'),
                                                        TRUE );

                       end;

                  //Para los recibos mensuales se requiere saber la lista de meses y de a�os que se esta solicitando.
                  if (SQLAgente.Entidad = enMovimienBalanzaMensual) then
                  begin
                       //Si no existe el MES, ListaMesesTodos debe de quedar como falso
                       FParamList.AddBoolean( 'ListaMesesTodos', FALSE );

                       for i:=0 to LBFiltros.Items.Count -1 do
                       begin
                            if ( Pos( 'PE_MES',  TFiltroOpciones(LBFiltros.Items.Objects[i]).Formula ) <> 0 ) then
                            begin
                                 FParamList.AddString( 'ListaMeses', TFiltroOpciones(LBFiltros.Items.Objects[i]).Lista );
                                 FParamList.AddBoolean( 'ListaMesesTodos', TFiltroOpciones(LBFiltros.Items.Objects[i]).TipoRangoActivo = raTodos );
                            end;
                            if ( Pos( 'PE_YEAR',  TFiltroOpciones(LBFiltros.Items.Objects[i]).Formula ) <> 0 ) then
                               FParamList.AddString( 'ListaYears', TFiltroOpciones(LBFiltros.Items.Objects[i]).Lista );
                       end;
                  end;

                  {$endif}
             end
             else
             begin
                  {$ifdef ADUANAS}
                  AgregaSQLCampoRep;
                  {$endif}
                  {$ifdef CAJAAHORRO}
                  AgregaSQLCampoRep;
                  {$endif}
                  {$ifdef CAPTURACURSOS}
                  AgregaSQLCampoRep;
                  {$endif}
                  AgregaSQLFiltrosEspeciales( FFiltroEspecial, TRUE );
             end;

             {$IFDEF CALSONIC}
             FParamList.AddBoolean('GeneraBitacora', (cbBitacora.Checked) or (Global.GetGlobalBooleano(K_GLOBAL_ESPECIAL_LOG_GLOBAL_1)));
             {$ELSE}
             FParamList.AddBoolean('GeneraBitacora', cbBitacora.Checked);
             {$ENDIF}

             AntesDeConstruyeSQL;
             FParamList.AddBoolean('ContieneImagenes', fHayImagenes);

             {Los Reportes Especiales, podran agregar
             parametros especiales en este metodo}

             {$ifdef TRESS}
             ZReportTools.ParametrosEspeciales( TipoEntidad(Reporte.FieldByName('RE_ENTIDAD').AsInteger),
                                                fParamList,
                                                LBFiltros.Items,
                                                GetGruposStrings );
             {$endif}


             {$IFDEF CALSONIC}
             Result := dmReportes.ConstruyeSQL( SQLAgente, sError, FFiltroFormula, FFiltroDescrip, FParamList, FTipoPantalla,
                                                (cbBitacora.Checked) or (Global.GetGlobalBooleano(K_GLOBAL_ESPECIAL_LOG_GLOBAL_1)),
                                                FHayImagenes);
             {$ELSE}
             Result := dmReportes.ConstruyeSQL( SQLAgente, sError, FFiltroFormula, FFiltroDescrip, FParamList, FTipoPantalla,
                                                cbBitacora.Checked, FHayImagenes);
             {$ENDIF}

             if Result then
             begin
                  ZFuncsCliente.RegistraFuncionesCliente;
                  //Asignacion de Parametros del Reporte,
                  //para poder evaluar la funcion PARAM() en el Cliente;
                  ZFuncsCliente.ParametrosReporte := SQLAgente.Parametros;
                  DespuesDeConstruyeSQL;

                  {$ifdef CAROLINA}
                  dmReportes.cdsResultados.SaveTofile('d:\temp\Resultados.cds');
                  {$endif}
             end
             else
             begin
                  if (StrVacio(sError) AND dsResultados.DataSet.IsEmpty) then
                     sError := K_NoHayRegistros;

                  ZetaDialogo.ZError(Caption,sError,0);
                  if Pos({$IFDEF TRESS_DELPHIXE5_UP}WideString({$ENDIF}K_NoHayRegistros{$IFDEF TRESS_DELPHIXE5_UP}){$ENDIF}, sError ) > 0 then
                     PageControl.ActivePage := tsFiltros;
             end;
        end
        else
        begin
             if sError > '' then
                ZetaDialogo.ZError(Caption,sError,0)
        end;

     finally
            FParamList.Free;
     end;
end;

procedure TEditBaseReportes_DevEx.ImpresionReporte( const Tipo : TipoPantalla );
 var oCursor : TCursor;
begin
     fControl := ActiveControl;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FEntidadActual := Entidad;
        FTipoPantalla := Tipo;
        if Visible then Cancelar_DevEx.SetFocus;
        if GeneraSQL then
        begin
               {***(@am): Anteriormente deshabilitar y habilitar el dataset desde este metodo no afectaba pues el control MULTIIMAGE
                          no necesitaba a notificacion para mostrar la imagen o el metodo disableControls en realidad no lo deshabilitaba.
                          Con el nuevo componente, necesitamos habilitar los controles antes de mostrar el PREVIEW, por lo que se movieron
                          estas dos lieneas al metodo GeneraForma de la unidad ZQrReporte.***}
             //dsResultados.DataSet.DisableControls;
             GeneraReporte;
             //dsResultados.DataSet.EnableControls;
        end;
     finally
            DespuesDeGeneraReporte;
            TressShell.ReconectaMenu;
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditBaseReportes_DevEx.AgregaSQLColumnasParam(oParam : TCampoMaster);
begin
     ZReportTools.AgregaSQLColumnasParam( SQLAgente, oParam );
end;

procedure TEditBaseReportes_DevEx.AgregaSQLColumnas( oCampo : TCampoOpciones; const Banda : integer );
begin
     ZReportTools.AgregaSQLColumnas( SQLAgente, oCampo, FDatosImpresion, Banda, LbParametros.Items.Count );
end;

procedure TEditBaseReportes_DevEx.AgregaSQLFiltrosEspeciales(const sFiltro : string; const lDeDiccion : Boolean = FALSE);
begin
     ZReportTools.AgregaSQLFiltrosEspeciales( SQLAgente, sFiltro, lDeDiccion, LbParametros.Items.Count );
end;

procedure TEditBaseReportes_DevEx.AgregaSQLFiltros( oFiltro : TFiltroOpciones );
begin
     ZReportTools.AgregaSQLFiltros( SQLAgente, oFiltro, FFiltroFormula, FFiltroDescrip );
end;


procedure TEditBaseReportes_DevEx.AgregaSQLGrupos(oGrupo: TGrupoOpciones);
begin
     ZReportTools.AgregaSQLGrupos( SQLAgente, oGrupo, LbParametros.Items.Count );
end;

procedure TEditBaseReportes_DevEx.AgregaSQLOrdenes(oOrden: TOrdenOpciones);
begin
     ZReportTools.AgregaSQLOrdenes( SQLAgente, oOrden, LbParametros.Items.Count );
end;

procedure TEditBaseReportes_DevEx.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
   Arbol.Items.Clear;
end;

procedure TEditBaseReportes_DevEx.FormCreate(Sender: TObject);
begin
     FPuedeVerSuscrip:= FALSE;
     {$IFNDEF SUPERVISORES}
     tsSuscripciones.TabVisible := FPuedeVerSuscrip;
     {$ELSE}
     tsSuscripciones.TabVisible := FALSE;
     {$ENDIF}
     bbSuscripcion.Enabled:= FPuedeVerSuscrip;
     inherited;
     dsResultados.DataSet := dmReportes.cdsResultados;
     SQLAgente := TSQLAgenteClient.Create;

end;

procedure TEditBaseReportes_DevEx.FormDestroy(Sender: TObject);
begin
     with LBParametros do
          while Items.Count > 0 do
          begin
                TCampoMaster(Items.Objects[0]).Free;
                Items.Delete(0);
          end;

      with  LBPersonasSus do
           while Items.Count > 0 do
           begin
                TSuscripCampoMaster(Items.Objects[0]).Free;
                Items.Delete(0);
           end;

     SQLAgente.Free;
     fBitacora.Free;
     FRelaciones.Free;
     FRelaciones := NIL;

     {$ifdef RDD}
     {$else}
     dmEntidadesShell.Free;
     dmEntidadesShell := NIL;
     {$endif}

     inherited;
end;

procedure TEditBaseReportes_DevEx.CompactarClick(Sender: TObject);
begin
     inherited;
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     ZArbolCalendarioReportes.CambiaArbol( Arbol, TRUE );
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     Arbol.Selected := Arbol.Items[0];
end;

procedure TEditBaseReportes_DevEx.Connect;
begin
     inherited;
     //dmReportes.cdsReportes.Conectar;
     dsResultados.DataSet := dmReportes.cdsResultados;
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     with dmSistema do
     begin
          dmSistema.CodigoReporte := VACIO;
          dmSistema.NombreReporte := VACIO;
          dmSistema.TituloReporte := VACIO;

          dmSistema.CodigoReporte := Reporte.FieldByName('RE_CODIGO').AsString;
          dmSistema.NombreReporte := Reporte.FieldByName('RE_NOMBRE').AsString;
          dmSistema.TituloReporte := dmSistema.NombreReporte;
          cdsSuscripCalendarioReportes.Conectar;
          cdsSistTareaCalendario.Conectar;
     end;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     IniciarArbol;
end;

procedure TEditBaseReportes_DevEx.IniciarArbol;
begin
     try
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
        dmSistema.cdsSuscripCalendarioReportes.Refrescar;
        if (  dmReportes.cdsEditReporte.State <> dsInsert ) then
        begin
          ZArbolCalendarioReportes.CrearArbol( Arbol, 0 );
        end;
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     finally
            TressShell.ReconectaMenu;
     end;
end;

procedure CambiaArbol( oArbol: TcxTreeView; const lAccion: boolean ); overload;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with oArbol do
        begin
             if lAccion then
                FullCollapse
             else
                 FullExpand;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditBaseReportes_DevEx.DespuesDeConstruyeSQL;
begin
end;

procedure TEditBaseReportes_DevEx.Preview;
begin
     DataSource.DataSet := dmReportes.cdsEditReporte;
     LlenaListas;
     ImpresionReporte(tgPreview);
end;

procedure TEditBaseReportes_DevEx.Imprime;
begin
     DataSource.DataSet := dmReportes.cdsEditReporte;
     LlenaListas;
     ImpresionReporte(tgImpresora);
end;


function TEditBaseReportes_DevEx.GetFirstPestana: TcxTabSheet;
 var i: integer;
begin
     Result := NIL;
     BGuardarComo.Enabled := NOT SoloImpresion;
     BDisenador.Visible := NOT SoloImpresion;
     with PageControl do
          if SoloImpresion then
          begin
               for i:= 0 to PageControl.PageCount -1 do
                   if (Pages[i] <> tsOrden) and
                      (Pages[i].Name <> 'tsGrupos') and
                      (Pages[i] <> tsFiltros) and
                      (Pages[i] <> tsImpresora) then
                   begin
                        Pages[i].TabVisible := FALSE;
                   end;
               Result := tsFiltros;

          end
          else
          begin
               for i:= 0 to PageControl.PageCount -1 do
                   Pages[i].TabVisible := TRUE;
          end;
end;

procedure TEditBaseReportes_DevEx.ExisteDirectorio( const sArchivo : string; oControl : TWinControl );
begin
     if NOT DirectoryExists(ExtractFileDir(sArchivo)) then
        Error( tsImpresora, oControl, 'El Directorio : '+ ExtractFileDir(sArchivo) +' No Existe' );
end;

procedure TEditBaseReportes_DevEx.ExpanderClick(Sender: TObject);
begin
     inherited;
     {$IFNDEF SUPERVISORES}
     {$IFNDEF VISITANTES}
     {$IFNDEF CARRERA}
     {$IFNDEF EVALUACION}
     ZArbolCalendarioReportes.CambiaArbol( Arbol, FALSE );
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
     {$ENDIF}
end;

procedure TEditBaseReportes_DevEx.ParametrosListadoNomina(FParamList : TZetaParams);
begin
end;

procedure TEditBaseReportes_DevEx.ParametrosMovimienLista(FParamList : TZetaParams);
begin
end;

procedure TEditBaseReportes_DevEx.DespuesDeGeneraReporte;
begin
end;

procedure TEditBaseReportes_DevEx.eFormulaOrdenExit(Sender: TObject);
begin
     inherited;
     ActualizaOrdenObjeto;
end;

procedure TEditBaseReportes_DevEx.EnabledGrupos(const bEnable, bFormula: Boolean);
begin
end;

procedure TEditBaseReportes_DevEx.AgregaFormulaOrden;
 var oFormula : TOrdenOpciones;
begin
     BAgregaFormulaOrden.SetFocus;
     oFormula := TOrdenOpciones.Create;
     with oFormula do
     begin
         Entidad := enFormula;
         Formula := '';
         Titulo := K_TITULO;
         Calculado := -1;
     end;
     with LBOrden do
     begin
          Items.AddObject( oFormula.Titulo, oFormula );
          SmartListOrden.SelectEscogido( Items.Count - 1 );
          Modo := dsEdit;
     end;
     ETituloOrden.SetFocus;
     ETituloOrden.SelectAll;
end;


function TEditBaseReportes_DevEx.ConstruyeFormula( var oMemo : TCustomMemo ) : Boolean;
 var sFormula : string;
begin
     sFormula := oMemo.Text;
     Result := ZConstruyeFormula.GetFormulaConstruyeParams( Entidad, sFormula, oMemo.SelStart, evReporte, LbParametros.Items );
     if Result then
     begin
          Reporte.Edit;
          Modo := dsEdit;
          oMemo.Text := sFormula;
     end;
end;

function TEditBaseReportes_DevEx.ConstruyeFormula( var oMemo : TcxCustomMemo ) : Boolean;
 var sFormula : string;
begin
     sFormula := oMemo.Text;
     Result := ZConstruyeFormula.GetFormulaConstruyeParams( Entidad, sFormula, oMemo.SelStart, evReporte, LbParametros.Items );
     if Result then
     begin
          Reporte.Edit;
          Modo := dsEdit;
          oMemo.Text := sFormula;
     end;
end;

function TEditBaseReportes_DevEx.GetGruposStrings: TStrings;
begin
     Result := NIL;
end;

procedure TEditBaseReportes_DevEx.ValidarDerechosCalendarioSuscripciones;
begin
//      if (CheckDerecho( D_SUSCRIPCION_REPORTE, K_DERECHO_CONSULTA ) or CheckDerecho( D_SUSCRIPCION_USUARIOS, K_DERECHO_CONSULTA )) then
//      begin
//           btnEliminarCalendarizaciones.Enabled := true;
//      end
//      else
//      begin
//           btnEliminarCalendarizaciones.Enabled := false;
//      end;
//
//      //btnEditarEnvioProgramado
//      //btnAgregarEnviosProgram
//
//      if (CheckDerecho( D_SUSCRIPCION_REPORTE, K_DERECHO_CONSULTA ) or CheckDerecho( D_SUSCRIPCION_USUARIOS, K_DERECHO_CONSULTA )) then
//      begin
//          btnEditarEnvioProgramado.Enabled := true;
//          btnAgregarEnviosProgram.Enabled := true;
//      end
//      else
//      begin
//          btnEditarEnvioProgramado.Enabled := false;
//          btnAgregarEnviosProgram.Enabled := false;
//      end;
//

      tsSuscripciones.TabVisible:= FALSE;
      {$IFNDEF SUPERVISORES}
      {$IFNDEF VISITANTES}
      if CheckDerecho( D_SUSCRIPCION_USUARIOS, K_DERECHO_CONSULTA) then
      begin
//           btnAsignarRoles.Enabled := true;
//           btnAsignarUsuarios.Enabled := true;
             tsSuscripciones.TabVisible:= TRUE;
      end
      else
      begin
//           btnAsignarRoles.Enabled := false;
//           btnAsignarUsuarios.Enabled := false;
             tsSuscripciones.TabVisible:= FALSE;
      end;
      {$ENDIF}
      {$ENDIF}

end;


procedure TEditBaseReportes_DevEx.ValidarHabilitadoBotonesRolesUsuarios;
var
  iCantidadRegistros : integer;
begin
     {$IFNDEF SUPERVISORES}
     iCantidadRegistros := 0;
     if (  dmReportes.cdsEditReporte.State <> dsInsert ) and ( dmSistema.cdsSuscripCalendarioReportes.Active ) then
     begin
         iCantidadRegistros := dmSistema.cdsSuscripCalendarioReportes.RecordCount;
     end;
     if iCantidadRegistros < 1 then
     begin
          RolesUsuarios(False);
     end
     else
     begin
          RolesUsuarios(True);
     end;
     {$ENDIF}
end;

procedure TEditBaseReportes_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     FPuedeVerSuscrip:= ( not dmCliente.ModoSuper ) and ( not ReadOnly );
     FExportaUser := VACIO;
     FCambioArchivo := False;

     FClasificacionActiva := eClasifiReporte( dmReportes.cdsEditReporte.FieldByName( 'RE_CLASIFI' ).AsInteger );
     bbSuscripcion.Enabled:= FPuedeVerSuscrip and PuedeSuscribir( TRUE, FALSE );
//     tsSuscripciones.TabVisible:= FPuedeVerSuscrip and PuedeSuscribir( FALSE, TRUE );
     HabilitaControles;

     ValidarDerechosCalendarioSuscripciones;
     IniciarArbol;
     ValidarHabilitadoBotonesRolesUsuarios;
end;

function TEditBaseReportes_DevEx.GetDisenaName: string;
begin
{$ifdef ADUANAS}
        Result := 'TressIMXDisena.exe';
{$else}
       {$ifdef SELECCION}
               Result := 'SeleccionDisena.exe';
       {$else}
              {$ifdef VISITANTES}
                      Result := 'VisitantesDisena.exe';
              {$else}
                     Result := 'TressDisena.exe';
              {$endif}
       {$endif}
{$endif}
end;

function TEditBaseReportes_DevEx.ValidaPlantilla( var sPlantilla: string ): Boolean;
begin
    if StrVacio(sPlantilla) then
       sPlantilla := ZReportTools.DirPlantilla + 'DEFAULT.QR2'
    else
    begin
         if ExtractFileExt( sPlantilla ) = '' then
            sPlantilla := sPlantilla + '.QR2';
         if ExtractFilePath( sPlantilla ) = '' then
            sPlantilla := ZReportTools.DirPlantilla + sPlantilla;
    end;
    Result := FileExists(sPlantilla);
    if NOT Result then
    begin
         Result := ZetaDialogo.ZConfirm( Caption, Format( 'La plantilla %s no existe.' + CR_LF +
                  ' � Desea abrir el dise�ador de formas para crearla ?' , [sPlantilla] ), 0, mbYes );
         sPlantilla := VACIO;
    end;
end;

procedure TEditBaseReportes_DevEx.AbreDisenador( const sFileName: string );
const
     K_PATH_DISENA = 'EMPRESA=%s USUARIO=%d PASS=%s PLANTILLA="%s"';
var
   sPlantilla, sAppPath, sDisenaName, sParametros: string;
begin
     sPlantilla:= sFileName;
     if ValidaPlantilla(sPlantilla) then
     begin
          sAppPath := VerificaDir( ExtractFilePath(Application.ExeName) );
          sDisenaName := sAppPath + GetDisenaName;
          if FileExists( sDisenaName ) then
          begin
               sParametros := Format( K_PATH_DISENA, [dmCliente.Compania, dmCliente.Usuario, dmCliente.PasswordUsuario, sPlantilla ] );
               //sParametros := Format( K_PATH_DISENA, [dmCliente.Compania, dmCliente.Usuario, dmCliente.PasswordUsuario, 'C:\Progra~1\default.qr2' ] );
               ExecuteFile( sDisenaName, sParametros, '', SW_SHOWDEFAULT );
          end
          else
          begin
               ZetaDialogo.ZError(Caption, Format( 'No se encontr� el Dise�ador de Formas en "%s"', [sDisenaName] ), 0 )
          end
     end;
end;



function TEditBaseReportes_DevEx.GeneraNombreArchivo(const sNombreArchivo:string ): String;
var  sError : WideString;
     oParams : OleVariant;
begin
     SqlAgente.Clear;
     dmReportes.EvaluaParametros( SqlAgente, LbParametros.Items, sError, oParams, TRUE );
     if ( StrVacio( sError ) )then
     begin
          Result:= TransParamNomConfig( sNombreArchivo, LbParametros.Count, oParams, TRUE, FALSE );
     end
     else
     begin
          ZetaDialogo.zError('� Error !', sError, 0 );
          Result:= VACIO;
     end;
end;

procedure TEditBaseReportes_DevEx.bbSuscripcionClick(Sender: TObject);
var
   oUsuario: TSuscripCampoMaster;
begin
     inherited;
      dmSistema.cdsSuscrip.DisableControls;
      try
         if ( dmReportes.AgregaSuscripciones( Reporte ) ) then
         begin
              ZetaDialogo.ZInformation( 'Explorador de Reportes',
                              Format( 'Reporte "%s" Agregado a Mis Suscripciones', [ Reporte.FieldByName( 'RE_NOMBRE' ).AsString ] ),
                              0 );
              if PuedeAgregarSuscripcion(dmCliente.Usuario,TRUE) then
              begin
                   oUsuario:= TSuscripCampoMaster.Create;
                   AgregaSuscripcion(oUsuario);
                   RefrescarStatusBar;
              end;
        end;
     finally
            dmSistema.cdsSuscrip.EnableControls;
     end;
end;

//LISTA SUSCRITOS

procedure TEditBaseReportes_DevEx.SmartListSuscritosAlSeleccionar( Sender: TObject; var Objeto: TObject; Texto: String);
begin
     inherited;
     if Objeto <> NIL then
     begin
          ActualizaSuscrito( TSuscripCampoMaster( Objeto ) );
          Texto := TSuscripCampoMaster( Objeto ).Titulo;
     end
     else
         EnabledSuscritos(FALSE);
end;

procedure TEditBaseReportes_DevEx.EnabledSuscritos(const lEnable: Boolean);
const
   K_VACIO_COMBO = -1;
begin
     if tsSuscripciones.TabVisible then
     begin
          SU_FRECUEN.Enabled:= lEnable;
          SU_VACIO.Enabled:= lEnable;
          lbFrecuencia.Enabled:= lEnable;
          lbNoDatos.Enabled:= lEnable;
          BtnBorraUsuario.Enabled:= lEnable;

          if not lEnable then
          begin
               SU_FRECUEN.ItemIndex:= K_VACIO_COMBO;
               SU_VACIO.ItemIndex:= K_VACIO_COMBO;
               US_EMAIL.Text:= VACIO;
               US_FORMATO.Text:= VACIO;
          end;
     end;
end;

procedure TEditBaseReportes_DevEx.EnabledSuscripControls( const lEnable: Boolean );
var
   i: Integer;
begin
     if tsSuscripciones.TabVisible then
     begin
          for i:= 0 to tsSuscripciones.ControlCount - 1 do
          begin
               tsSuscripciones.Controls[i].Enabled:= lEnable;
          end;
     end;
     if bbSuscripcion.Enabled then
        bbSuscripcion.Enabled:= ( lEnable and PuedeSuscribir( TRUE, FALSE ) );
     EnabledSuscritos( ( lEnable ) and ( LBPersonasSus.Items.Count > 0 ) );
end;

procedure TEditBaseReportes_DevEx.BtnAgregaUsuarioClick(Sender: TObject);
var
   oUsuario: TSuscripCampoMaster;
   sKey,sDescripcion: String;
begin
     inherited;
     with dmSistema.cdsUsuarios do
     begin
          BtnAgregaUsuario.SetFocus;
          sKey:= LookupKeyField;
          sDescripcion:=  LookupDescriptionField;
          if( ZetaBusqueda_DevEx.ShowSearchForm(dmSistema.cdsUsuarios, VACIO, sKey, sDescripcion ) ) then
          begin
               if( PuedeAgregarSuscripcion(FieldByName('US_CODIGO').AsInteger,TRUE) ) then
               begin
                    oUsuario := TSuscripCampoMaster.Create;
                    oUsuario.Usuario:= FieldByName('US_CODIGO').AsInteger;
                    oUsuario.Titulo := FieldByName('US_NOMBRE').AsString;
                    oUsuario.Frecuencia:= eEmailFrecuencia(dmSistema.UltimaFrecuencia);
                    oUsuario.HayDatos:= enEnviar;
                    oUsuario.Direccion:= FieldByName('US_EMAIL').AsString;
                    oUsuario.FormatoRep:= eEmailFormato(FieldByName('US_FORMATO').AsInteger);
                    DespuesDeAgregarDato( TCampoMaster( oUsuario ),
                           SmartListSuscritos,
                           BtnBorraUsuario,
                           SU_FRECUEN);
                    Modo := dsEdit;
               end;
          end;
     end;
end;

procedure TEditBaseReportes_DevEx.LlenaListas;
var  oUsuario : TSuscripCampoMaster;
begin
     inherited;
     with dmSistema.cdsSuscrip do
     begin
          if Active then
          begin
               First;
               dmSistema.cdsUsuarios.Conectar;
               with dmSistema do
               begin
                    while NOT EOF do
                    begin
                         oUsuario := TSuscripCampoMaster.Create;
                         AgregaSuscripcion(oUsuario);
                         Next;
                    end;
               end;
          end;
     end;
     SmartListSuscritos.SelectEscogido( 0 );
end;

procedure TEditBaseReportes_DevEx.BtnBorraUsuarioClick(Sender: TObject);
var
   oCampo: TSuscripCampoMaster;
begin
     inherited;
     oCampo:= TSuscripCampoMaster(LBPersonasSus.Items.Objects[LBPersonasSus.ItemIndex]);
     with dmSistema do
     begin
          if ( PuedeAgregarSuscripcion ( oCampo.Usuario,FALSE ) )then
          begin
               if ( ZetaDialogo.ZConfirm(Caption, '� Desea Borrar la Suscripci�n del Usuario ?', 0, mbNo ) ) then
               begin
                    if ( cdsSuscrip.Locate('US_CODIGO', oCampo.Usuario, []) ) then
                       cdsSuscrip.Delete;
                    BorraDato( SmartListSuscritos, BtnAgregaUsuario, BtnBorraUsuario, LBPersonasSus );
                    EnabledSuscritos(LBPersonasSus.Items.Count > 0)
               end;
          end;
     end;

end;

procedure TEditBaseReportes_DevEx.ActualizaSuscrito( oUsuario : TSuscripCampoMaster );
begin
     AsignaProcSuscrito( NIL );
     SU_FRECUEN.ItemIndex := Ord( oUsuario.Frecuencia );
     SU_VACIO.ItemIndex := Ord( oUsuario.HayDatos )  ;
     US_EMAIL.Text := oUsuario.Direccion;
     US_FORMATO.Text:= ObtieneElemento(lfEmailFormato,Ord(oUsuario.FormatoRep));
     AsignaProcSuscrito( SuscripcionClick );
     EnabledSuscritos(TRUE);
end;

procedure TEditBaseReportes_DevEx.ActualizaSuscritoObjeto;
var  Objeto : TSuscripCampoMaster;
     i:Integer;
begin
     i := LBPersonasSus.ItemIndex;
     with LBPersonasSus, Items do
     begin
          Objeto := TSuscripCampoMaster( Objects[ i ] );
          if Objeto <> NIL then
          begin
               Objeto.Frecuencia := eEmailFrecuencia( SU_FRECUEN.ItemIndex );
               Items[i] := Objeto.Titulo;
               Objeto.HayDatos := eEmailNotificacion( SU_VACIO.ItemIndex ) ;
               Modo := dsEdit;
          end;
     end;
end;


procedure TEditBaseReportes_DevEx.SuscripcionClick(Sender: TObject);
begin
     inherited;
     ActualizaSuscritoObjeto;
end;

procedure TEditBaseReportes_DevEx.AsignaProcSuscrito(oProc: TNotifyEvent);
begin
     SU_FRECUEN.OnClick := oProc;
     SU_VACIO.OnClick := oProc;
     US_EMAIL.OnExit := oProc;
     US_FORMATO.OnExit := oProc;
end;

procedure TEditBaseReportes_DevEx.GetDatosSuscripcion( oCampo: TSuscripCampoMaster );
begin
     with  dmSistema do
     begin
          if cdsUsuariosLookup.Locate( 'US_CODIGO', cdsSuscrip.FieldByName('US_CODIGO').AsInteger, [] ) then
          begin
               oCampo.Titulo:= cdsUsuariosLookup.FieldByName('US_NOMBRE').AsString;
               oCampo.Direccion := cdsUsuariosLookup.FieldByName('US_EMAIL').AsString;
               oCampo.FormatoRep := eEmailFormato( cdsUsuariosLookup.FieldByName('US_FORMATO').AsInteger );
          end
          else
          begin
               oCampo.Titulo:= '<Usuario desconocido>';
               oCampo.Direccion := VACIO;
               oCampo.FormatoRep := efHTML;
          end;

          oCampo.Usuario:= cdsSuscrip.FieldByName('US_CODIGO').AsInteger;
          oCampo.Frecuencia :=  eEmailFrecuencia( cdsSuscrip.FieldByName('SU_FRECUEN').AsInteger );
          oCampo.HayDatos := eEmailNotificacion( cdsSuscrip.FieldByName('SU_VACIO').AsInteger );
     end;
end;

procedure TEditBaseReportes_DevEx.GrabaSuscripciones;
var
   i: Integer;
begin
     with LBPersonasSus.Items do
     begin
          for i:= 0 to Count - 1 do
          begin
               with TSuscripCampoMaster(Objects[i]) do
               begin
                    with dmSistema.cdsSuscrip do
                    begin
                         if ( Locate('US_CODIGO', Usuario,[]) ) then
                         begin
                              Edit;
                              FieldByName('SU_FRECUEN').AsInteger:= Ord(Frecuencia);
                              FieldByName('SU_VACIO').AsInteger:= Ord(HayDatos);
                              Post;
                         end
                         else
                         begin
                              if ( dmSistema.AgregaSuscripciones( Usuario, Reporte.FieldByName('RE_CODIGO').AsInteger, Ord(Frecuencia), Ord(HayDatos) ) ) then
                              begin
                                   dmReportes.ModificaCampoReporte('US_SUSCRITO', Usuario);
                                   if( dmReportes.EstaEnClasifi( dmReportes.cdsReportes.FieldByName('RE_CODIGO').AsInteger, 'US_SUSCRITO' )  ) then
                                       RefrescarStatusBar;
                              end;
                         end;
                    end;
               end;
          end;

     end;
end;

procedure TEditBaseReportes_DevEx.AgregaSuscripcion(oUsuario: TSuscripCampoMaster );
begin
     GetDatosSuscripcion( oUsuario );
     LBPersonasSus.Items.AddObject( oUsuario.Titulo, oUsuario );
end;

function TEditBaseReportes_DevEx.PuedeAgregarSuscripcion(iUsuario: Integer; const lAgregar: Boolean): Boolean;
var
   i: Integer;
   oCampo: TSuscripCampoMaster;
begin
     Result:= TRUE;
     with LBPersonasSus.Items do
     begin
          if ( ( dmReportes.PuedeSuscribirse ) or
                  not ( dmCliente.Usuario = iUsuario ) )  then
          begin
               for i:= 0 to Count - 1 do
               begin
                    if ( lAgregar ) then
                    begin
                         oCampo:= TSuscripCampoMaster(Objects[i]);
                         Result:= not ( oCampo.Usuario = iUsuario );
                         if not Result then
                            Exit;
                    end
                    else
                        Result:= TRUE;
               end;
          end
          else
          begin
               ZError('Error','El Usuario no tiene los suficientes derechos para suscribirse o borrar su sucripci�n',0);
               Result:= FALSE;
          end;
     end;
end;

function TEditBaseReportes_DevEx.PuedeSuscribir(const lEsAutoSuscripcion, lEsOtrosUsuarios: Boolean): Boolean;

 function CheckaUnDerecho( const NumeroDerecho, iDerecho : Integer ): Boolean;
 begin
      {$ifdef RDD}
      Result:= ZAccesosMgr.CheckUnDerecho( NumeroDerecho, iDerecho );
      {$else}
      Result:= ZAccesosMgr.CheckDerecho( NumeroDerecho, iDerecho );
      {$endif}
 end;
begin
      Result :=  ( dmCliente.GetGrupoActivo = D_GRUPO_SIN_RESTRICCION );

     if NOT Result then
     begin
          with dmReportes, dmDiccionario do
          begin
               if lEsAutoSuscripcion and lEsOtrosUsuarios then
               begin
                    Result:= PuedeAbrirCandado and
                             PuedeSuscribirse and
                             PuedeSuscribirUsuarios and
                             CheckaUnDerecho( GetDerechosClasifi(FClasificacionActiva), K_DERECHO_CAMBIO );
               end
               else if lEsAutoSuscripcion then
                    Result:= PuedeAbrirCandado and
                             PuedeSuscribirse and
                             CheckaUnDerecho( GetDerechosClasifi(FClasificacionActiva),K_DERECHO_CAMBIO)
               else
                   Result:= PuedeAbrirCandado and
                            PuedeSuscribirUsuarios and
                            CheckaUnDerecho(GetDerechosClasifi(FClasificacionActiva),K_DERECHO_CAMBIO);
          end;
     end;
end;

procedure TEditBaseReportes_DevEx.HabilitaControles;
begin
     inherited;
     EnabledSuscripControls( dmReportes.cdsEditReporte.State <> dsInsert );
end;

procedure TEditBaseReportes_DevEx.RolesUsuarios(lEnabled : Boolean);
begin
     btnAsignarUsuarios.Enabled := lEnabled;
     btnAsignarRoles.Enabled := lEnabled;
end;

end.
