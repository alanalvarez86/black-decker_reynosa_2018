unit FDuplicaPlantilla;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, FileCtrl, Buttons, StdCtrls,
  QRDesign, ExtCtrls, QuickRpt,
  ComCtrls, QRDLDR, thsdRuntimeLoader, thsdRuntimeEditor, qrdBaseCtrls,
  qrdQuickrep, QRPrntr, StrUtils, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxCheckBox, Vcl.Menus, cxButtons, cxClasses, dxSkinsForm,
  cxGroupBox;

type
  TTDuplicaPlantilla = class(TForm)
    QRepDesigner: TQRepDesigner;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DesignQuickReport1: TDesignQuickReport;
    OpenDialog: TOpenDialog;
    DevEx_SkinController: TdxSkinController;
    cxGroupBox1: TcxGroupBox;
    gpbSeleccionPlantilla: TGroupBox;
    Label1: TLabel;
    lbPlantillas: TLabel;
    NuevaPlantillaDir: TEdit;
    PlantillaDir: TEdit;
    Buscar: TcxButton;
    gpbConfig: TGroupBox;
    cbPrintBothSides: TcxCheckBox;
    btnGeneraPlantilla: TcxButton;
    procedure BuscarClick(Sender: TObject);
    procedure btnGeneraPlantillaClick(Sender: TObject);
  private
    //function BuscarDirectorio(const Value: String): String;  //remover
    //procedure Convertir(const sDirectorio: string);  //remover
    procedure GrabaNuevaPlantilla(const sPlantilla, sNuevaPlantilla: string);
    //procedure Log(const sMensaje: string);  //remover
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TDuplicaPlantilla: TTDuplicaPlantilla;

implementation
uses
    ZetaCommonClasses,
    ZetaCommonTools,
    ZetaClientTools,
    ZetaDialogo;

{$R *.dfm}

procedure TTDuplicaPlantilla.BuscarClick(Sender: TObject);
var sPlantilla: String;
begin
     sPlantilla:= ZetaClientTools.AbreDialogo( OpenDialog,  PlantillaDir.Text, 'QR2' );
     if StrLleno(sPlantilla) then
     begin
        PlantillaDir.Text := sPlantilla;
        sPlantilla := StrTransform(sPlantilla,'.QR2','_VS2015.QR2');
        NuevaPlantillaDir.Text := sPlantilla;
     end;
end;

procedure TTDuplicaPlantilla.btnGeneraPlantillaClick(Sender: TObject);
begin
      if (strLleno(PlantillaDir.Text) and strLleno(NuevaPlantillaDir.Text)) then
        GrabaNuevaPlantilla( PlantillaDir.Text, NuevaPlantillaDir.Text )
      else
        ZError(Caption, 'Seleccione la plantilla original primero.' , 0);
end;

procedure TTDuplicaPlantilla.GrabaNuevaPlantilla(const sPlantilla, sNuevaPlantilla: string);
var oCursor: TCursor;
const
  DUPLEX=1;
  NO_DUPLEX=0;
function StrFixTitle( sOrigen: String ): String;
var
    sFind:String;
    i:Integer;
begin
    sFind := LeftStr(sOrigen,1);
    i := AnsiPos( AnsiUpperCase( sFind ),AnsiUpperCase( sOrigen ) );
        if ( i > 0 ) then
            Delete( sOrigen, i, Length( sFind ) );
    Result := sOrigen;
end;

begin
     if FileExists( sPlantilla ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if not QRepDesigner.LoadReport(sPlantilla)then
             begin
                //Log(Format( 'Error al Cargar Plantilla %s', [sPlantilla])); //OLD
                ZError(Caption, 'Error al Cargar Plantilla ' + sPlantilla , 0);
             end
             else
             begin
                 try
                      //(@am): Corregir Titulo del reporte, el cual se carga el caracter #2 en plantillas generadas con otra version de QR.
                      QRepDesigner.QReport.ReportTitle := StrFixTitle(QRepDesigner.QReport.ReportTitle);
                      //(@am): Agrega configuracion bothsides si el usuario lo especifico
                      if cbPrintBothSides.checked then
                          QRepDesigner.QReport.Tag := DUPLEX
                      else
                          QRepDesigner.QReport.Tag := NO_DUPLEX;
                      //Duplica el archivo sin reemplazar el anterior
                      if QRepDesigner.SaveReport(sNuevaPlantilla) then
                        ZInformation(Caption, Format('Plantilla generada correctamente en:' + CR_LF+'%s',[sNuevaPlantilla]),0)
                      else
                        ZError(Caption, 'Error al grabar la plantilla, verifique que cuenta con derechos' + CR_LF+' de escritura en la carpeta donde desea generar el duplicado.',0);
                 except
                         On Error:Exception do
                            ZError(Caption,Format( 'Error al grabar la plantilla %s' +CR_LF + Error.Message, [sPlantilla]) , 0);
                 end;
             end;
          finally
            Screen.Cursor := oCursor;
          end;

     end
     else
         ZError(Caption, 'La plantilla %s no existe' + sPlantilla , 0);
end;

end.
