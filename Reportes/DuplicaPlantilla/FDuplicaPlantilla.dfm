object TDuplicaPlantilla: TTDuplicaPlantilla
  Left = 378
  Top = 199
  Caption = 'Duplica Plantilla'
  ClientHeight = 242
  ClientWidth = 619
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 619
    Height = 242
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      TabVisible = False
      object cxGroupBox1: TcxGroupBox
        Left = 0
        Top = 0
        Align = alClient
        Style.Edges = []
        Style.TransparentBorder = True
        TabOrder = 0
        Height = 232
        Width = 611
        object gpbSeleccionPlantilla: TGroupBox
          Left = 17
          Top = 19
          Width = 576
          Height = 102
          Align = alCustom
          Caption = '1. Selecci'#243'n plantilla'
          TabOrder = 0
          object Label1: TLabel
            Left = 15
            Top = 67
            Width = 73
            Height = 13
            Alignment = taRightJustify
            Caption = '&Nueva plantilla:'
            FocusControl = PlantillaDir
          end
          object lbPlantillas: TLabel
            Left = 13
            Top = 23
            Width = 75
            Height = 13
            Alignment = taRightJustify
            Caption = '&Plantilla original:'
            FocusControl = PlantillaDir
          end
          object NuevaPlantillaDir: TEdit
            Tag = 61
            Left = 94
            Top = 64
            Width = 435
            Height = 21
            Color = clInfoBk
            ReadOnly = True
            TabOrder = 2
          end
          object PlantillaDir: TEdit
            Tag = 61
            Left = 94
            Top = 20
            Width = 435
            Height = 21
            Color = clInfoBk
            ReadOnly = True
            TabOrder = 0
          end
          object Buscar: TcxButton
            Left = 535
            Top = 20
            Width = 21
            Height = 21
            Hint = 'Buscar plantilla original'
            OptionsImage.Glyph.Data = {
              DA050000424DDA05000000000000360000002800000013000000130000000100
              200000000000A40500000000000000000000000000000000000000B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFDFF5
              FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5FCFFDFF5
              FCFFDFF5FCFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF10B7EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF40C5EFFF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF40C5EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6CD3
              F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6CD3F2FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFF93DEF6FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF97E0F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFCBEFFBFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFEBF9FDFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF54CB
              F0FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF60CF
              F1FF60CFF1FF60CFF1FF60CFF1FF60CFF1FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDFF5FCFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FFDFF5FCFFFFFFFFFFFFFFFFFFFFFFFFFFBFECF9FF80D9F4FF80D9
              F4FF80D9F4FF80D9F4FF80D9F4FF80D9F4FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC3EDFAFFDFF5FCFFDFF5FCFFDFF5
              FCFF70D4F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
              E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BuscarClick
          end
        end
        object gpbConfig: TGroupBox
          Left = 17
          Top = 127
          Width = 576
          Height = 58
          Align = alCustom
          Caption = '2. Configuraci'#243'n'
          TabOrder = 1
          object cbPrintBothSides: TcxCheckBox
            Left = 15
            Top = 24
            Hint = 'Configure para impresiones por ambos lados'
            Caption = 'Impresi'#243'n por ambos lados'
            ParentShowHint = False
            Properties.Alignment = taRightJustify
            ShowHint = True
            TabOrder = 0
            Transparent = True
            Width = 156
          end
        end
        object btnGeneraPlantilla: TcxButton
          Left = 488
          Top = 194
          Width = 105
          Height = 25
          Hint = 'Generar plantilla'
          Caption = '&Generar plantilla'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = btnGeneraPlantillaClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      TabVisible = False
      object DesignQuickReport1: TDesignQuickReport
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Functions.Strings = (
          'PAGENUMBER'
          'COLUMNNUMBER'
          'REPORTTITLE')
        Functions.DATA = (
          '0'
          '0'
          #39#39)
        Options = [FirstPageHeader, LastPageFooter]
        Page.Columns = 1
        Page.Orientation = poPortrait
        Page.PaperSize = Letter
        Page.Continuous = False
        Page.Values = (
          127.000000000000000000
          2794.000000000000000000
          127.000000000000000000
          2159.000000000000000000
          127.000000000000000000
          127.000000000000000000
          0.000000000000000000)
        PrinterSettings.Copies = 1
        PrinterSettings.OutputBin = Auto
        PrinterSettings.Duplex = False
        PrinterSettings.FirstPage = 0
        PrinterSettings.LastPage = 0
        PrinterSettings.UseStandardprinter = False
        PrinterSettings.UseCustomBinCode = False
        PrinterSettings.CustomBinCode = 0
        PrinterSettings.ExtendedDuplex = 0
        PrinterSettings.UseCustomPaperCode = False
        PrinterSettings.CustomPaperCode = 0
        PrinterSettings.PrintMetaFile = False
        PrinterSettings.MemoryLimit = 1000000
        PrinterSettings.PrintQuality = 0
        PrinterSettings.Collate = 0
        PrinterSettings.ColorOption = 0
        PrintIfEmpty = True
        SnapToGrid = True
        Units = Inches
        Zoom = 100
        PrevFormStyle = fsNormal
        PreviewInitialState = wsNormal
        PrevInitialZoom = qrZoomToFit
        PreviewDefaultSaveType = stQRP
        PreviewLeft = 0
        PreviewTop = 0
        LabelSettings.FirstLabel = 0
        LabelSettings.LabelCount = 0
        PrepareAutomatically = False
        DesignBackgroundDX = 0
        DesignBackgroundDY = 0
      end
    end
  end
  object QRepDesigner: TQRepDesigner
    ParentControl = DesignQuickReport1
    ScriptsEnabled = True
    RuntimeMessages.Strings = (
      
        'Calcfield needs database=No tables on current report! You need a' +
        ' table for a calculated field.'
      'GetQueryParamCaption=Parameter of %s'
      'GetQueryParamPrompt=Please type in the parameter "%s'#39'":'
      'Invalid value='#161'Valor Inv'#225'lido!'
      
        'Delete band='#191'Borrar la Banda Seleccionada y los Elementos en Ell' +
        'a?'
      'Delete selected bands='#191'Borrar Todas las Bandas Seleccionadas?'
      'Image not found='#161'Archivo de Imagen %s no se encontr'#243'!'
      'Error opening database=Error al Abrir Tabla %s'
      'Error opening database-query=Error al Abrir Query %s'
      'Reportfile not found='#161'Plantilla %s no se Encontr'#243'!'
      'Error in reportfile='#161'Error en la Plantilla!'
      'Write Error='#161'Error al Escribir %s!'
      'BandTitle=T'#237'tulo'
      'BandPageHeader=Encabezado de P'#225'gina'
      'BandDetail=Detalle'
      'BandPageFooter=Pie de P'#225'gina'
      'BandSummary=Sumario'
      'BandGroupheader=Encabezado de Grupo'
      'BandGroupfooter=Pie de Grupo'
      'BandSubdetail=Sub-Detalle'
      'BandColumnHeader=Encabezado de Columna'
      'BandOverlay=Overlay'
      'CalcSum=Suma'
      'CalcCount=Cuantos'
      'CalcMaximum=Valor M'#225'ximo'
      'CalcMinimum=Valor M'#237'nimo'
      'CalcAverage=Promedio'
      'SysdataTime=Hora'
      'SysdataDate=Fecha'
      'SysdataDateTime=Fecha/Hora'
      'SysdataPagenumber=N'#250'mero de P'#225'gina'
      'SysdataReportTitle=T'#237'tulo de la Plantilla'
      'SysdataDetailcounter=Cuenta del Detalle'
      'SysdataDetailnumber=N'#250'mero de Detalle'
      'ShapeRectangle=Rect'#225'ngulo'
      'ShapeCircle=C'#237'rculo'
      'ShapeVerticalLine=L'#237'nea Vertical'
      'ShapeHorizontalLine=L'#237'nea Horizontal'
      'ShapeTopBottomLines=L'#237'neas Arriba/Abajo'
      'ShapeLeftRightLines=L'#237'neas Izquierda/Derecha'
      'BrushEmpty=Vac'#237'o'
      'BrushSolid=S'#243'lido'
      'BrushCross=Cruzado'
      'BrushDiagCross=Diagonal Cruzado'
      'BrushDiagLinesBT=L'#237'neas Diagonales de Abajo hacia Arriba'
      'BrushDiagLinesTB=L'#237'neas Diagonales de Arriba hacia Abajo'
      'BrushHorLines=L'#237'nea Horizontal'
      'BrushVertLines=L'#237'nea Vertical'
      'PenMode1=Default'
      'PenMode2=Siempre Negro'
      'PenMode3=Siempre Blanco'
      'PenMode4=Sin Cambio'
      'PenMode5=Inverso al Color de la Pantalla'
      'PenMode6=Inverso al Color de la Pluma'
      
        'PenMode7=Combinaci'#243'n de Color de Pluma e Inverso de Color Pantal' +
        'la'
      'PenMode8=Colores Comunes de Plumna e Invetso de Color Pantalla'
      
        'PenMode9=Combinaci'#243'n de Color de Pantalla e Inverso de Color Plu' +
        'ma'
      'PenMode10=common colors of screen and inverse pen color'
      'PenMode11=combination of pen and screen color'
      'PenMode12=inverse combination of pen and screen color'
      'PenMode13=common colors of pen and screen color'
      'PenMode14=inverse common colors of pen and screen color'
      'PenMode15=colors from either pen or screen color'
      'PenMode16=inverse colors from either pen or screen color'
      'PenStyleSolid=L'#237'nea S'#243'lida'
      'PenStyleDashed=L'#237'nea Rayada'
      'PenStyleDot=L'#237'nea Punteada'
      'PenStyleDashDot=L'#237'nea Rayada-Punteada'
      'PenStyleDashDotDot=L'#237'nea Rayada-Punteada-Punteada'
      'PenStyleHiddenMark=Esconder L'#237'nea de Marco'
      'PenStyleInsideFrame=Marco Interior'
      'CalcFieldEditFormCaption=Campo Calculado de %s'
      'PropertyNotSet=[No Inicializado]'
      'GraphicFrom=Gr'#225'fica de %s'
      'RulerNone=Nada'
      'RulerCmHorizontal=CM Horizontal'
      'RulerCmVertical=CM Vertical'
      'RulerCMHorVert=CM Horizontal/Vertical'
      'RulerInchHorizontal=Pulgada Horizontal'
      'RulerInchVertical=Pulgada Vertical'
      'RulerInchHorVert=Pulgada Horizontal/Vertical'
      'No databases available='#161'No hay Tablas Disponibles!'
      'Invalid componentname='#161'Nombre de Elemento Inv'#225'lido!'
      'Save report=Guardar Plantilla %s?'
      
        'Report not saved=La plantilla Tiene Cambios sin Guardar! Continu' +
        'ar?'
      'Database not found=(Tabla no Encontrada)'
      
        'DeleteDetailLinkQuestion='#191'Borrar la Definici'#243'n de la Liga del De' +
        'talle?'
      'Database not set=(Tabla no Incializada)'
      'DeleteGroupQuestion='#191'Borrar la Definici'#243'n del Grupo?'
      'DefaultTableName=Tabla'
      'Databasename=Nombre de Tabla'
      'InputName=Teclee un T'#237'tulo:'
      'Error opening database='#161'Error al Abrir Tabla!'
      
        'ErrorDeleteMainDatabase=La Tabla Principal no se Puede Borrar. S' +
        'eleccione otra Tabla.'
      
        'DeleteDatabaseConfirmation=If you delete this table, all reporte' +
        'lements using it will become invalid. Continue?'
      'Error activating query=Error activating query!'
      'PRIMARY INDEX=PRIMARY INDEX'
      'None=<Ninguno>'
      'RenameError='#161'Error: No se Puede Renombrar a %s!'
      'No SQL parameters=The SQL Statement has no parameters!'
      'Invalid value for datatype='#161'Valor Inv'#225'lido para el Tipo de Dato!'
      'Calcfield not found='#161'Campo Calculado no Encontrado!'
      
        'Error creating calcfield='#161'El Campo Calculado %s no Pudo ser Crea' +
        'do!'
      'NoAlias=<Ninguno>'
      'Calculated field=Campo Calculado'
      'Datafield=Campo de la Tabla'
      'ImgAlign None=<Ninguno>'
      'ImgAlign Top=Arriba'
      'ImgAlign Bottom=Abajo'
      'ImgAlign Left=Izquierda'
      'ImgAlign Right=Derecha'
      'Band=Banda'
      'Childband=Banda Hija'
      'Subdetailband=Banda Sub-Detalle'
      'Groupband=Banda de Grupo'
      'Label=Texto Fijo'
      'Memo=Memo'
      'Image=Imagen'
      'Shape=Figura'
      'Sysdata=Campo de Sistema'
      'Datafield=Campo de la Tabla'
      'Image datafield=Imagen de la Tabla'
      'Expression=F'#243'rmula'
      'Richtext=Texto Enriquecido'
      'Richtext datafield=Texto Enriquecido de la Tabla'
      'Chart=Gr'#225'fica'
      'PopupMenuProperties=Edici'#243'n'
      'PopupMenuOptions=Opciones'
      'PopupMenuBringToFront=Traer al Frente'
      'PopupMenuSendToBack=Mandar Hacia Atr'#225's'
      'PopupMenuGroup=Agrupar Elementos'
      'PopupMenuUngroup=DesAgrupar Elementos'
      'PopupMenuUngroupAll=DesAgrupar Todos los Elementos')
    Version = '1.59.0'
    UseDatamodules = True
    Cursor = crDefault
    UndoEnabled = True
    UndoLevels = 3
    GridSizeX = 4
    GridSizeY = 4
    KeyboardMoveX = 1
    KeyboardMoveY = 1
    MinElementWidth = 5
    MinElementHeight = 5
    SQLSettings.DelimiterType = delNoQuotes
    SQLSettings.SQLStringDelimiterLeft = #39
    SQLSettings.SQLStringDelimiterRight = #39
    SQLSettings.SQLTableDelimiterLeft = #39
    SQLSettings.SQLTableDelimiterRight = #39
    SQLSettings.SQLDateDelimiterLeft = #39
    SQLSettings.SQLDateDelimiterRight = #39
    SQLSettings.SQLWildcard = '%'
    AddCursor = crDefault
    AllowBlockEdit = True
    AllowNewCalcFields = True
    AllowSQLEdit = True
    AutoEditAfterInsert = True
    BackupFileExtension = '.QRB'
    BandNameFont.Charset = DEFAULT_CHARSET
    BandNameFont.Color = clSilver
    BandNameFont.Height = 20
    BandNameFont.Name = 'MS Sans Serif'
    BandNameFont.Style = []
    ComponentFrameColor = clGray
    ComponentFrameStyle = psDot
    ReportFileExtension = '.QR2'
    SaveBackupFiles = True
    ScriptEditByUser = False
    ShowBandNames = True
    ShowComponentFrames = False
    ShowDatafieldListbox = True
    DatafieldsSorted = False
    QReport = DesignQuickReport1
    SaveLoadPrinterSetup = True
    Left = 32
    Top = 320
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'dat'
    Filter = 'Plantillas (*.qr2)|*.qr2'
    Options = [ofHideReadOnly]
    Title = 'Seleccione la plantilla a duplicar'
    Left = 214
    Top = 194
  end
  object DevEx_SkinController: TdxSkinController
    Kind = lfFlat
    NativeStyle = False
    SkinName = 'TressMorado2013'
    Left = 341
    Top = 201
  end
end
