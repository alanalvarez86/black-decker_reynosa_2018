unit FWizConviertePlantilla;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.StrUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ZcxWizardBasico, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
  cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses,
  cxImage, cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl,
  cxClasses, dxSkinsForm, Vcl.Menus, Vcl.StdCtrls, cxButtons, thsdRuntimeLoader,
  thsdRuntimeEditor, QRDesign, FileCtrl, ZetaSmartLists_DevEx, cxListBox,
  Vcl.ExtCtrls, QuickRpt, qrdBaseCtrls, qrdQuickrep, Vcl.ImgList, DB,
  DBClient, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, cxDBData, cxRadioGroup, cxCheckBox, ZetaClientDataSet, cxLocalization,
  cxProgressBar, dxSkinscxPCPainter, Vcl.Grids, Vcl.DBGrids, ZetaDBGrid;

type
  TWizConviertePlantilla = class(TCXWizardBasico)
    DevEx_SkinController: TdxSkinController;
    Subdirectorios: TdxWizardControlPage;
    Plantillas: TEdit;
    lbPlantillas: TLabel;
    btnBuscar: TcxButton;
    QRepDesigner: TQRepDesigner;
    zmlFuente: TZetaSmartListBox_DevEx;
    Label20: TLabel;
    Label11: TLabel;
    zmlDestino: TZetaSmartListBox_DevEx;
    ZetaSmartListsButton1: TZetaSmartListsButton_DevEx;
    ZetaSmartListsButton2: TZetaSmartListsButton_DevEx;
    ZetaSmartLists: TZetaSmartLists_DevEx;
    btnSeleccionarTodos: TcxButton;
    btnDeseleccionarTodos: TcxButton;
    lstDirectorios: TListBox;
    lblListaDirectorios: TLabel;
    btnGuardarBitacora: TcxButton;
    DesignQuickReport1: TDesignQuickReport;
    cxImageList: TcxImageList;
    DataSourceBitacora: TDataSource;
    gbBitacora: TcxGroupBox;
    DevEx_cxLocalizer: TcxLocalizer;
    pbAvance: TcxProgressBar;
    lblAvance: TLabel;
    lblDirectorio: TLabel;
    ckSoloErrores: TcxCheckBox;
    lblBitacora: TLabel;
    lblResultado: TLabel;
    procedure btnBuscarClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure btnSeleccionarTodosClick(Sender: TObject);
    procedure btnDeseleccionarTodosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnGuardarBitacoraClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FListaSubdirectorios: TStrings;
    FDirectoriosConversion: TStrings;
    FParametrosControl: TWinControl;
    FConteoErrores: Integer;
    cdsBitacora   : TZetaClientDataSet;
    function BuscarDirectorio(const Value: String): String;
    procedure BuscaSubdirectorios( DirectorioPadre: String; Lista:TStrings);
    procedure LimpiaSubdirectorios;
    function CargaSubdirectorios: Boolean;
    function ConteoArchivos (const sDirectorio: string) :Integer;
    procedure AddHorizontalScrollbar(ListBox: TZetaSmartListBox_DevEx);
    procedure CargaTraducciones;
    procedure IniciaProgressBar(iMax: Double);
    procedure GetInformacion(directorio:String; lSoloErrores:Boolean);
  public
    { Public declarations }
  protected
    property ParametrosControl: TWinControl read FParametrosControl write FParametrosControl;
  end;

var
  WizConviertePlantilla: TWizConviertePlantilla;

implementation

uses
  ZetaDialogo,
  ZetaCommonTools,
  ZetaCommonClasses,
  ZetaCommonLists;

{$R *.dfm}

procedure TWizConviertePlantilla.btnDeseleccionarTodosClick(Sender: TObject);
begin
  inherited;
  zmlFuente.Items.AddStrings(zmlDestino.Items);
  zmlDestino.Items.Clear;
  AddHorizontalScrollbar(zmlFuente);
end;

procedure TWizConviertePlantilla.GetInformacion(directorio:String; lSoloErrores:Boolean);
const
    K_COMMA = ',';
var
    Pos : TBookMark;
    i : Integer;
    cadena: String;
    MyText: TStringlist;
begin
      MyText:= TStringlist.create;
      try
              with cdsBitacora  do
              begin
                   if ( not IsEmpty ) then
                   begin
                        DisableControls;
                        try
                           Pos:= GetBookMark;
                           First;
                           for i := 0 to ( FieldCount - 1 ) do
                              if i = 2 then
                                  cadena := cadena + Fields[i].FieldName //Ultimo valor no necesita comma
                              else
                                  cadena := cadena + Fields[i].FieldName + K_COMMA;
                           MyText.Add(cadena);
                           cadena:= '';
                           while not Eof do
                           begin
                                if lSoloErrores then
                                begin
                                  if UpperCase(FieldByName('STATUS').AsString) = 'ERROR' then
                                  begin
                                    cadena := FieldByName('ARCHIVO').AsString + K_COMMA + FieldByName('MENSAJE').AsString +  K_COMMA + FieldByName('STATUS').AsString;
                                    MyText.Add(cadena);
                                  end;
                                end
                                else
                                begin
                                  cadena := FieldByName('ARCHIVO').AsString + K_COMMA + FieldByName('MENSAJE').AsString +  K_COMMA + FieldByName('STATUS').AsString;
                                  MyText.Add(cadena);
                                end;
                                Next;
                           end;
                           if ( Pos <> nil ) then
                           begin
                                GotoBookMark( Pos );
                                FreeBookMark( Pos );
                           end;
                        finally
                           EnableControls;
                        end;
                   end;
              end;
      finally
          MyText.SaveToFile(directorio,TEncoding.Unicode);
          MyText.Free
      end;
end;


procedure TWizConviertePlantilla.btnGuardarBitacoraClick(Sender: TObject);
var
sDirectorioBitacora:String;
begin
  inherited;
  sDirectorioBitacora := '';
  sDirectorioBitacora := BuscarDirectorio(sDirectorioBitacora);
  if DirectoryExists(sDirectorioBitacora) then
  begin
      try
          if copy(sDirectorioBitacora, length(sDirectorioBitacora), 1) = '\' then
            sDirectorioBitacora := sDirectorioBitacora+'Bitacora.csv'
          else
            sDirectorioBitacora := sDirectorioBitacora+'\Bitacora.csv';
          GetInformacion(sDirectorioBitacora, ckSoloErrores.Checked);
          ZetaDialogo.ZInformation('Bit�cora de la conversi�n', 'El archivo con el contenido de la bit�cora '+CR_LF+'fue generado correctamente en:' + CR_LF + sDirectorioBitacora,0);
      except
          ZetaDialogo.ZError('Bit�cora de la conversi�n', 'El archivo con el contenido de la bit�cora '+CR_LF+'no pudo ser generado.',0);
      end;
  end;
end;

procedure TWizConviertePlantilla.btnSeleccionarTodosClick(Sender: TObject);
begin
  inherited;
  zmlDestino.Items.AddStrings(zmlFuente.Items);
  zmlFuente.Items.Clear;
  AddHorizontalScrollbar(zmlDestino);
end;

function TWizConviertePlantilla.BuscarDirectorio( const Value: String ): String;
var
   sDirectory: String;
begin
     sDirectory := Value;
     if SelectDirectory( sDirectory, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
        Result := sDirectory
     else
         Result := Value;
end;

procedure TWizConviertePlantilla.BuscaSubdirectorios( DirectorioPadre: String; Lista:TStrings);
procedure AgregaSlash;
begin
    if DirectorioPadre[Length(DirectorioPadre)] <> '\' then
      DirectorioPadre := DirectorioPadre + '\';
end;
var
  Directorio: TSearchRec;
  iResultado: Integer;
begin
  // Si la ruta no termina en contrabarra se la ponemos
  if DirectorioPadre[Length(DirectorioPadre)] <> '\' then
    DirectorioPadre := DirectorioPadre + '\';

  iResultado := FindFirst( DirectorioPadre + '*.*', FaAnyfile, Directorio );
  while iResultado = 0 do
  begin
    // �Es un directorio y hay que entrar en �l?
    if ( Directorio.Attr = faDirectory ) then
    begin
      if ( Directorio.Name <> '.' ) and ( Directorio.Name <> '..' ) then
      begin
         Lista.Add( DirectorioPadre + Directorio.Name+ '\' );
         BuscaSubdirectorios( DirectorioPadre + Directorio.Name, Lista ); ///Abrimos el directorio para buscar mas directorios.
      end;
    end;
    iResultado := FindNext( Directorio );
  end;
  FindClose( Directorio );
end;

//Carga subdirectorios en el smartListBox fuente.
function TWizConviertePlantilla.CargaSubdirectorios: Boolean;
begin
    FListaSubdirectorios.Clear;
    BuscaSubdirectorios(Plantillas.Text, FListaSubdirectorios);
    //zmlFuente.Items.AddStrings(Lista);
    Result := (FListaSubdirectorios.Count > 0);
end;

procedure TWizConviertePlantilla.FormCreate(Sender: TObject);
begin
  cdsBitacora := TZetaClientDataSet.Create(nil);
  FListaSubdirectorios := TStringList.Create;
  FDirectoriosConversion := TStringList.Create;
  ZetaSmartListsButton1.Glyph := nil;
  ZetaSmartListsButton2.Glyph := nil;
  ZetaSmartListsButton1.ImageIndex := 0;
  ZetaSmartListsButton2.ImageIndex := 1;
  inherited;
end;


procedure TWizConviertePlantilla.FormDestroy(Sender: TObject);
begin
  FreeAndNil(cdsBitacora);
  inherited;
end;

procedure TWizConviertePlantilla.FormShow(Sender: TObject);
begin
  inherited;
  //Textos componentes
  CargaTraducciones;
end;

//Limpia ambos smartListBoxes
procedure TWizConviertePlantilla.LimpiaSubdirectorios;
begin
    zmlFuente.Items.Clear;
    zmlDestino.Items.Clear;
end;

function TWizConviertePlantilla.ConteoArchivos( const sDirectorio: string ): Integer;
var iConteoArchivos:Integer;
    oFile: TSearchRec;
    sArchivo: string;
begin
   iConteoArchivos := 0;
   sArchivo := '*.QR2';
   if DirectoryExists(sDirectorio) then
   begin
        if FindFirst( sDirectorio + sArchivo, faArchive, oFile) = 0 then
        begin
            repeat
                inc(iConteoArchivos);
            until (FindNext(oFile)<>0);
        end;
        FindClose(oFile);
   end;
   Result := iConteoArchivos;
end;

procedure TWizConviertePlantilla.IniciaProgressBar(iMax: Double);
begin
     pbAvance.Properties.Min := 0.00;
     pbAvance.Properties.Max := iMax;
     pbAvance.Position :=0.00;
     pbAvance.Update;
end;

procedure TWizConviertePlantilla.WizardAfterMove(Sender: TObject);
begin
  inherited;
  with Wizard do
  begin
      if Adelante and EsPaginaActual( Subdirectorios ) then
      begin
           LimpiaSubdirectorios;
           //CargaSubdirectorios;
           zmlFuente.Items.AddStrings(FListaSubdirectorios);
           if  zmlFuente.Items.Count > 0 then
              AddHorizontalScrollbar(zmlFuente);
      end
      else if Adelante and EsPaginaActual(Ejecucion) then
      begin
          lstDirectorios.Items.Clear;
          lstDirectorios.Items.Add(Plantillas.Text+'\');
          lstDirectorios.Items.AddStrings(FDirectoriosConversion);
          FDirectoriosConversion.Clear;
      end;
  end;
end;

procedure TWizConviertePlantilla.WizardAlEjecutar(Sender: TObject;
  var lOk: Boolean);
var
  i, iConteoArchivos:Integer;

procedure GeneraDatasetTemporal;
begin
  with cdsBitacora do
  begin
           AddStringField('ARCHIVO' , 1024 );
           AddStringField('MENSAJE', 1024 ) ;
           AddStringField('STATUS', 255 ) ;
           CreateTempDataset;
  end;
end;

procedure GrabaEnBitacora( sArchivo, sMsg, sStatus:String);
begin
    with cdsBitacora do
    begin
        try
          DisableControls;
          Append;
          FieldByName('ARCHIVO').AsString := sArchivo;
          FieldByName('MENSAJE').AsString := sMsg;
          FieldByName('STATUS').AsString := sStatus;
          Post;
        finally
          EnableControls;
        end;
    end;
end;

procedure Convertir( const sDirectorio: string );
var
    oFile: TSearchRec;
    sArchivo: string;
    lResultado:Boolean;
    function GrabaNuevaPlantilla(const sPlantilla: string):Boolean;
    var lResultado:Boolean;
        function StrFixTitle( sOrigen: String ): String;
        var
            sFind:String;
            i:Integer;
        begin
            sFind := LeftStr(sOrigen,1);
            i := AnsiPos( AnsiUpperCase( sFind ),AnsiUpperCase( sOrigen ) );
                if ( i > 0 ) then
                    Delete( sOrigen, i, Length( sFind ) );
            Result := sOrigen;
        end;

    begin
         lResultado := False;
         if FileExists( sPlantilla ) then
         begin
              try
                 if not QRepDesigner.LoadReport(sPlantilla)then
                 begin
                    FConteoErrores := FConteoErrores+1;
                 end
                 else
                 begin
                     try
                          //(@am): Corregir Titulo del reporte, el cual se carga el caracter #2 en plantillas generadas con otra version de QR.
                          QRepDesigner.QReport.ReportTitle := StrFixTitle(QRepDesigner.QReport.ReportTitle);
                          QRepDesigner.SaveReport(sPlantilla);
                          lResultado := True;
                     except
                             On Error:Exception do
                             begin
                                FConteoErrores := FConteoErrores+1;
                             end;
                     end;
                 end;
              finally
                  //Application.ProcessMessages;
              end;
         end;
         Result := lResultado;
    end;
begin
    sArchivo := '*.QR2';
    try
          if FindFirst( sDirectorio + sArchivo, faArchive, oFile) = 0 then
          begin
              repeat
                  //Convierte archivo
                  lResultado := GrabaNuevaPlantilla( sDirectorio + oFile.Name );
                  //Actualiza progress bar
                  pbAvance.Position := pbAvance.Position +1;
                  pbAvance.Update;
                  //Graba en bitacora
                  if lResultado then
                    GrabaEnBitacora( sDirectorio + oFile.Name , 'La plantilla ha sido actualizada al nuevo formato.', 'Convertida' )
                  else
                    GrabaEnBitacora( sDirectorio + oFile.Name , 'Error al cargar o grabar la plantilla.', 'Error' );
              until (FindNext(oFile)<>0);
          end;
    finally
          FindClose(oFile);
    end;
end;

begin
  FConteoErrores := 0;
  //Se desactivan para no poder repetir la operacion.
  WizardControl.Buttons.Back.Enabled := FALSE; //temp
  WizardControl.Buttons.Cancel.Enabled := FALSE; //temp
  //Crea y asigna el cds al datasource antes de cargar informacion.
  GeneraDatasetTemporal;
  DatasourceBitacora.DataSet := cdsBitacora;
  {**Proceso de conversion**}
  try
      for i:=0 to lstDirectorios.Items.Count - 1 do
      begin
          iConteoArchivos := ConteoArchivos(lstDirectorios.Items.Strings[i]);
          if iConteoArchivos>0 then
          begin
             lblDirectorio.Caption := lstDirectorios.Items.Strings[i];
             Application.ProcessMessages;
             IniciaProgressBar(iConteoArchivos);
             //Convierte archivos del directorio
             Convertir(lstDirectorios.Items.Strings[i]);
          end
          else
            GrabaEnBitacora( lstDirectorios.Items.Strings[i], 'No se encontraron plantillas en esta ruta.', 'Error');
      end;
  finally
    {**Notificacionn al terminar**}
    lblResultado.Visible := FALSE;
    if FConteoErrores > 0 then
    begin
        lblResultado.Font.Color := clRed;
        lblResultado.Caption := 'Se produjeron ' + IntToStr(FConteoErrores)+ ' errores.';
    end
    else
    begin
      lblResultado.Font.Color := clGreen;
      lblResultado.Caption := 'Sin errores';
    end;
    lblDirectorio.Caption := 'El proceso ha finalizado.';
    lblResultado.Visible := TRUE;
    btnGuardarBitacora.Enabled := TRUE;
  end;
  inherited;
end;

procedure TWizConviertePlantilla.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
  ParametrosControl := nil;
  with Wizard do
  begin
      if Adelante and EsPaginaActual( Parametros ) then
      begin
         if not DirectoryExists(Plantillas.Text)  then //Validar que no esta vacio el directorio principal.
         begin
            CanMove:= False;
            ZetaDialogo.ZError('Error', 'El directorio de plantillas no es valido',0);
         end;
         if CanMove then
            Subdirectorios.PageVisible := CargaSubdirectorios ;
      end
      else if Adelante and EsPaginaActual( Subdirectorios ) then
      begin
         FDirectoriosConversion.Clear;
         FDirectoriosConversion.AddStrings(zmlDestino.Items);
      end;
  end;
end;

procedure TWizConviertePlantilla.btnBuscarClick(Sender: TObject);
begin
  inherited;
  Plantillas.Text := BuscarDirectorio(Plantillas.Text);
end;

procedure TWizConviertePlantilla.AddHorizontalScrollbar(ListBox: TZetaSmartListBox_DevEx);
var i, intWidth, intMaxWidth: Integer;
begin
  intMaxWidth := 0;
  for i := 0 to ListBox.Items.Count-1 do
  begin
    intWidth := ListBox.Canvas.TextWidth(ListBox.Items.Strings[i] + 'x');
    if intMaxWidth < intWidth then
      intMaxWidth := intWidth;
  end;
  SendMessage(ListBox.Handle, LB_SETHORIZONTALEXTENT, intMaxWidth, 0);
end;

procedure TWizConviertePlantilla.CargaTraducciones;
begin
    DevEx_cxLocalizer.Active := True;
    DevEx_cxLocalizer.Locale := 2058;  // Carinal para Espanol (Mexico)
end;

end.
