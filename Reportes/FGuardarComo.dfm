object GuardarComo: TGuardarComo
  Left = 304
  Top = 233
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Guardar Como...'
  ClientHeight = 73
  ClientWidth = 303
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 41
    Width = 303
    Height = 32
    Align = alBottom
    TabOrder = 0
    object Cancel: TBitBtn
      Left = 219
      Top = 4
      Width = 75
      Height = 25
      Caption = '&Cancelar'
      TabOrder = 1
      Kind = bkCancel
    end
    object Ok: TBitBtn
      Left = 139
      Top = 4
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 303
    Height = 41
    Align = alClient
    TabOrder = 1
    object lbNombre: TLabel
      Left = 25
      Top = 16
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nombre:'
      FocusControl = sNombre
    end
    object sNombre: TEdit
      Tag = 99
      Left = 73
      Top = 12
      Width = 205
      Height = 21
      MaxLength = 30
      TabOrder = 0
    end
  end
end
