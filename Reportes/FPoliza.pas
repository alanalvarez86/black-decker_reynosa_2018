unit FPoliza;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FPolizaBase, ZetaSmartLists, DB, ZetaKeyLookup, ZetaEdit,
  ZetaFecha, CheckLst, StdCtrls, ExtCtrls, Grids, DBGrids, ZetaDBGrid,
  ZetaKeyCombo, DBCtrls, Mask, ZetaDBTextBox, ComCtrls, Buttons;

type
  TPoliza = class(TPolizaBase)
    btnCreaPoliza: TSpeedButton;
    Label7: TLabel;
    lbNumCuentas: TLabel;
    Panel3: TPanel;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    Label4: TLabel;
    CBCuentas: TCheckBox;
    CBBCuentas: TComboBox;
    ECuentas: TEdit;
    EFormula: TEdit;
    CBBFormula: TComboBox;
    CBFormula: TCheckBox;
    rgTipoCuenta: TRadioGroup;
    procedure btnCreaPolizaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CBCuentasClick(Sender: TObject);
    procedure CBFormulaClick(Sender: TObject);
    procedure CBBCuentasChange(Sender: TObject);
    procedure CBBFormulaChange(Sender: TObject);
    procedure EFormulaChange(Sender: TObject);
    procedure ECuentasChange(Sender: TObject);
    procedure rgTipoCuentaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BorrarBtnClick(Sender: TObject);
    procedure dsCuentasStateChange(Sender: TObject);
  private
    { Private declarations }
    FTipoCuenta : string;
    FFiltroCuenta : string;
    FFiltroFormula : string;

    procedure FiltraCampoRep;
    procedure CuentaCuentas;
    procedure FiltraPoliza;
    procedure FiltraTipoCuenta;
    function GetFiltro(const eFiltro: eFiltroPoliza; const sCampo: string; sFiltro: string): string;

  protected
    procedure EscribirCambios;override;

  public
    { Public declarations }
    procedure Connect;override;

  end;

var
  Poliza: TPoliza;

implementation

uses
    DReportes,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    FConfigurarPoliza, FBaseReportes, ZetaCommonLists;


{$R *.dfm}


procedure TPoliza.FormCreate(Sender: TObject);
begin
     inherited;
     btnCreaPoliza.Hint := 'Configurar Cuentas Automáticamente' + CR_LF +
                           '( Antes: Utilería CreaPóliza )  ';

end;

procedure TPoliza.btnCreaPolizaClick(Sender: TObject);
begin
     inherited;
     { 10-Feb-2003:
       CV: Este boton manda llamar lo que inicialmente era la utileria CreaPoliza }

     if ( LBOrden.Items.Count > 0 ) then
     begin
          if ConfigurarPoliza = NIL then
             ConfigurarPoliza := TConfigurarPoliza.Create( Self );

          with ConfigurarPoliza do
          begin
               Grupos := LbOrden.Items;
               ShowModal;

               if ModalResult = mrOk then
               begin
                    Modo := dsEdit;
                    EscribirCambios;
               end
               else
                   dmReportes.CancelaCuentasPoliza;
          end;
     end
     else
     begin
          PageControl.ActivePage := tsOrden;
          ZError(Caption, 'La Póliza Debe de Tener por lo Menos un Criterio de Agrupación', 0 );
     end;
end;


procedure TPoliza.Connect;
begin
     inherited;
     FTipoCuenta := '';
     FFiltroCuenta := '';
     FFiltroFormula := '';

     FiltraCampoRep;

     CBCuentas.Checked := FALSE;
     ECuentas.Text := '';
     CBBCuentas.ItemIndex := 0;
     CBFormula.Checked := FALSE;
     EFormula.Text := '';
     CBBFormula.ItemIndex := 0;

end;

procedure TPoliza.FiltraTipoCuenta;
begin
     case rgTipoCuenta.ItemIndex of
          0: FTipoCuenta := '';
          1: FTipoCuenta := 'CR_OPER=0';
          2: FTipoCuenta := 'CR_OPER=1';
     end;
     FiltraCampoRep;
end;

procedure TPoliza.FiltraCampoRep;
 var sFiltro : string;
begin
     sFiltro := Q_FILTRO;
     sFiltro := ConcatFiltros( sFiltro, FTipoCuenta );
     sFiltro := ConcatFiltros( sFiltro, FFiltroCuenta );
     sFiltro := ConcatFiltros( sFiltro, FFiltroFormula );

     with CampoRep do
     begin
          Filter := sFiltro;
          Filtered := TRUE;
     end;
     
     CuentaCuentas;
end;

procedure TPoliza.CuentaCuentas;
begin
     if CampoRep.Active then
        lbNumCuentas.Caption := FormatFloat('0,0', CampoRep.RecordCount);
end;

procedure TPoliza.FiltraPoliza;
begin
     if CBCuentas.Checked then
        FFiltroCuenta := GetFiltro(eFiltroPoliza(CBBCuentas.ItemIndex), 'CR_TITULO',ECuentas.Text)
     else FFiltroCuenta := '';

     if CBFormula.Checked then
        FFiltroFormula := GetFiltro(eFiltroPoliza(CBBFormula.ItemIndex), 'CR_FORMULA', EFormula.Text)
     else FFiltroFormula := '';
     
     FiltraCampoRep;
end;

procedure TPoliza.CBCuentasClick(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza.CBFormulaClick(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza.CBBCuentasChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza.CBBFormulaChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;

procedure TPoliza.EFormulaChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;

end;

procedure TPoliza.ECuentasChange(Sender: TObject);
begin
     inherited;
     FiltraPoliza;
end;


procedure TPoliza.rgTipoCuentaClick(Sender: TObject);
begin
     inherited;
     FiltraTipoCuenta;
end;


procedure TPoliza.EscribirCambios;
begin
     inherited;
     FiltraCampoRep;
end;

procedure TPoliza.FormShow(Sender: TObject);
begin
     inherited;
     rgTipoCuenta.ItemIndex := 0;

     {$ifdef PRESUPUESTOS}
     if ( Reporte.FieldByName('RE_TIPO').AsInteger = Ord( trPoliza ) ) then
          ReadOnly := True;
     {$ENDIF}
end;

function TPoliza.GetFiltro( const eFiltro : eFiltroPoliza;
                            const sCampo: string; sFiltro : string): string;
begin
     Result := sCampo;
     case eFiltro of
          fpIgual: Result := Result + '='+EntreComillas( sFiltro );
          fpDiferente: Result := Result + '<>'+EntreComillas( sFiltro );
          fpEmpiezeCon: Result := Result + ' LIKE '+EntreComillas( sFiltro+'%');
          fpNOEmpiezeCon: Result := ' NOT (' + Result + ' LIKE '+EntreComillas( sFiltro+'%')+')';
          fpTermineEn: Result := Result + ' LIKE '+EntreComillas( '%' + sFiltro);
          fpNoTermineEn: Result := ' NOT (' + Result + ' LIKE '+EntreComillas('%' + sFiltro) + ')';
          fpContenga: Result := Result + ' LIKE '+EntreComillas( '%' +sFiltro+'%');
          fpNOContenga: Result := ' NOT (' + Result + ' LIKE '+EntreComillas( '%'+sFiltro+'%')+')';
          fpMayor: Result := Result + '> '+EntreComillas( sFiltro);
          fpMayorIgual: Result := Result + '>= '+EntreComillas( sFiltro);
          fpMenor: Result := Result + '< '+EntreComillas( sFiltro);
          fpMenorIgual: Result := Result + '<= '+EntreComillas( sFiltro);
          fpVacio: Result := Result + '='+EntreComillas('');
          fpNoVacio: Result := Result + '<>'+EntreComillas('');
     end;
end;
procedure TPoliza.BorrarBtnClick(Sender: TObject);
begin
     inherited;
     CuentaCuentas;

end;

procedure TPoliza.dsCuentasStateChange(Sender: TObject);
begin
     inherited;
     CuentaCuentas;
end;

end.
