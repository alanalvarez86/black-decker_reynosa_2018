inherited EditReportes: TEditReportes
  Left = 126
  Top = 162
  Caption = 'EditReportes'
  PixelsPerInch = 96
  TextHeight = 13
  inherited PageControl: TPageControl
    ActivePage = tsGrupos
    OnChange = PageControlChange
    object tsCampos: TTabSheet [1]
      Caption = 'Lista de Campos'
      ImageIndex = 5
      object GroupBox4: TGroupBox
        Left = 9
        Top = 4
        Width = 199
        Height = 209
        Caption = 'Campos Seleccionados'
        TabOrder = 0
        object LBCampos: TZetaSmartListBox
          Left = 5
          Top = 15
          Width = 188
          Height = 186
          Style = lbOwnerDrawFixed
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object BAgregaCampo: TBitBtn
        Left = 9
        Top = 219
        Width = 100
        Height = 25
        Caption = '&Agrega Campo'
        TabOrder = 2
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BBorraCampo: TBitBtn
        Left = 109
        Top = 219
        Width = 100
        Height = 25
        Caption = '&Borra Campo'
        Enabled = False
        TabOrder = 3
        OnClick = BBorraCampoClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object BFormula: TBitBtn
        Tag = 1
        Left = 34
        Top = 248
        Width = 150
        Height = 25
        Caption = 'Agrega &F'#243'rmula'
        TabOrder = 4
        OnClick = BFormulaClick
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
          DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
          0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
          D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
          D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
          0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
          DDDDD0000000}
      end
      object GBPropiedadesCampo: TGroupBox
        Left = 247
        Top = 4
        Width = 305
        Height = 245
        Caption = 'Propiedades del Campo:'
        TabOrder = 1
        object LTituloCampo: TLabel
          Left = 14
          Top = 24
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'T'#237'tulo:'
          Enabled = False
        end
        object lFormulaCampo: TLabel
          Left = 5
          Top = 48
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'F'#243'rmula:'
          Enabled = False
        end
        object lMascaraCampo: TLabel
          Left = 59
          Top = 166
          Width = 44
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#225'scara:'
          Enabled = False
        end
        object lAnchoCampo: TLabel
          Left = 18
          Top = 191
          Width = 85
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ancho ( # Letras):'
          Enabled = False
        end
        object lTipoCampo: TLabel
          Left = 38
          Top = 140
          Width = 65
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de Dato:'
          Enabled = False
        end
        object CBMascaraCampo: TComboBox
          Left = 108
          Top = 162
          Width = 165
          Height = 21
          Enabled = False
          ItemHeight = 0
          MaxLength = 20
          TabOrder = 4
          OnExit = CampoOpcionesClick
        end
        object ETituloCampo: TEdit
          Left = 48
          Top = 20
          Width = 225
          Height = 21
          Enabled = False
          MaxLength = 30
          TabOrder = 0
          OnExit = CampoOpcionesClick
        end
        object bFormulaCampo: TBitBtn
          Left = 276
          Top = 104
          Width = 25
          Height = 25
          Hint = 'Editor de F'#243'rmulas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
            DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
            0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
            D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
            D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
            0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
            DDDDD0000000}
        end
        object EFormulaCampo: TMemo
          Left = 48
          Top = 48
          Width = 225
          Height = 81
          MaxLength = 255
          ScrollBars = ssVertical
          TabOrder = 1
          OnExit = CampoOpcionesClick
        end
        object CBTipoCampo: TZetaKeyCombo
          Left = 108
          Top = 136
          Width = 165
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 0
          TabOrder = 3
          OnClick = CampoOpcionesClick
          ListaFija = lfTipoGlobalAuto
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object EAnchoCampo: TEdit
          Left = 108
          Top = 187
          Width = 61
          Height = 21
          TabOrder = 5
        end
      end
    end
    object tsGrupos: TTabSheet [3]
      Caption = 'Grupos'
      ImageIndex = 7
      object BAbajoGrupo: TZetaSmartListsButton
        Left = 212
        Top = 33
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          3333333333303333333333333309033333333333330903333333333330999033
          3333333330999033333333330999990333333333099999033333333099999990
          3333333000090000333333333309033333333333330903333333333333090333
          3333333333090333333333333309033333333333330003333333}
        Tipo = bsBajar
        SmartLists = SmartListGrupos
      end
      object BArribaGrupo: TZetaSmartListsButton
        Left = 212
        Top = 8
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
          3333333333090333333333333309033333333333330903333333333333090333
          3333333333090333333333300009000033333330999999903333333309999903
          3333333309999903333333333099903333333333309990333333333333090333
          3333333333090333333333333330333333333333333033333333}
        Tipo = bsSubir
        SmartLists = SmartListGrupos
      end
      object GroupBox6: TGroupBox
        Left = 9
        Top = 4
        Width = 199
        Height = 209
        Caption = 'Agrupar Por'
        TabOrder = 0
        object LbGrupos: TZetaSmartListBox
          Left = 5
          Top = 15
          Width = 188
          Height = 188
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object BAgregaGrupo: TBitBtn
        Left = 9
        Top = 219
        Width = 100
        Height = 25
        Caption = '&Agrega Grupo'
        TabOrder = 1
        OnClick = BAgregaFormulaGrupoClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BBorraGrupo: TBitBtn
        Left = 109
        Top = 219
        Width = 100
        Height = 25
        Caption = '&Borra Grupo'
        Enabled = False
        TabOrder = 2
        OnClick = BBorraGrupoClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object GroupBoxGrupo: TGroupBox
        Left = 242
        Top = 4
        Width = 285
        Height = 93
        Caption = 'Propiedades de Grupo'
        TabOrder = 4
        object lTituloGrupo: TLabel
          Left = 15
          Top = 20
          Width = 31
          Height = 13
          Caption = 'T'#237'tulo:'
        end
        object lFormulaGrupo: TLabel
          Left = 6
          Top = 38
          Width = 40
          Height = 13
          Caption = 'F'#243'rmula:'
        end
        object eFormulaGrupo: TMemo
          Left = 49
          Top = 40
          Width = 200
          Height = 45
          Enabled = False
          MaxLength = 255
          TabOrder = 1
          OnExit = GrupoOpcionesClick
        end
        object bFormulaGrupo: TBitBtn
          Left = 254
          Top = 60
          Width = 25
          Height = 25
          Hint = 'Editor de F'#243'rmulas'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = bFormulaGrupoClick
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
            DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
            0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
            D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
            D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
            0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
            DDDDD0000000}
        end
        object ETituloGrupo: TEdit
          Left = 49
          Top = 16
          Width = 200
          Height = 21
          Enabled = False
          TabOrder = 0
          OnExit = GrupoOpcionesClick
        end
      end
      object BAgregaFormulaGrupo: TBitBtn
        Tag = 1
        Left = 34
        Top = 248
        Width = 150
        Height = 25
        Caption = 'Agrega &F'#243'rmula'
        TabOrder = 3
        OnClick = BAgregaFormulaGrupoClick
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
          DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
          0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
          D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
          D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
          0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
          DDDDD0000000}
      end
    end
    inherited tsParametro: TTabSheet
      inherited BArribaParametro: TZetaSmartListsButton
        SmartLists = SmartListCampos
      end
      inherited BAbajoParametro: TZetaSmartListsButton
        SmartLists = SmartListCampos
      end
    end
  end
  inherited SmartListOrden: TZetaSmartLists
    Left = 368
  end
  object SmartListCampos: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LBCampos
    ListaEscogidos = LBCampos
    AlBajar = Intercambia
    AlSeleccionar = SmartListCamposAlSeleccionar
    AlSubir = Intercambia
    Left = 336
  end
  object SmartListGrupos: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LbGrupos
    ListaEscogidos = LbGrupos
    AlBajar = SmartListGruposAlBajar
    AlSeleccionar = SmartListGruposAlSeleccionar
    AlSubir = SmartListGruposAlSubir
    Left = 304
  end
end
