unit FGuardarComo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Menus, ComCtrls, ExtCtrls;

type
  TGuardarComo = class(TForm)
    Panel1: TPanel;
    Cancel: TBitBtn;
    Ok: TBitBtn;
    Panel2: TPanel;
    lbNombre: TLabel;
    sNombre: TEdit;
    procedure FormShow(Sender: TObject);
  private
    function GetNombre: String;
    procedure SetNombre( const sNombreRep: String );
  public
    property Nombre: String read GetNombre write SetNombre;
  end;

var GuardarComo: TGuardarComo;

function DialogoGuardar( var  sNombre : string ) : Boolean;

implementation

{$R *.DFM}

function DialogoGuardar( var sNombre : string ) : Boolean;
begin
     if GuardarComo = NIL then GuardarComo := TGuardarComo.Create( Application.MainForm );
        GuardarComo.Nombre := sNombre;
     with GuardarComo do
     begin
          ShowModal;
          Result := ( ModalResult = mrOk ) AND ( length( Nombre ) > 0 );
     end;
     if Result then sNombre := GuardarComo.Nombre;

end;

function TGuardarComo.GetNombre : String;
begin
     Result:= sNombre.Text;
end;

procedure TGuardarComo.SetNombre( const sNombreRep : String );
begin
     sNombre.Text := sNombreRep;
end;

procedure TGuardarComo.FormShow(Sender: TObject);
begin
     sNombre.SetFocus;
end;

end.
