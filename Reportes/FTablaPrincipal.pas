unit FTablaPrincipal;

interface

{$INCLUDE DEFINES.INC}

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo,
  ZetaTipoEntidad,
  ZetaCommonLists;

type
  TTablaPrincipal = class(TForm)
    PanelInferior: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    Panel1: TPanel;
    gbTablaPrincipal: TGroupBox;
    LBTablas: TListBox;
    GroupBox1: TGroupBox;
    RGClasificacion: TListBox;
    procedure RGClasificacionClick(Sender: TObject);
    procedure LBTablasDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
         FClasifiReporte: eClasifiReporte;
         FEntidad: TipoEntidad;
         function GetEntidad : TipoEntidad;
         procedure LlenaListBox;
  public
        property Entidad : TipoEntidad read GetEntidad write FEntidad;
        property Clasificacion : eClasifiReporte read FClasifiReporte write FClasifiReporte;
  end;

var
  TablaPrincipal: TTablaPrincipal;

  function ShowTablaPrincipal( var iEntidad : TipoEntidad; const iClasifi : eClasifiReporte ) : Boolean;

implementation
uses ZReportTools,
     ZetaDialogo,
     DCliente,
     DDiccionario;
{$R *.DFM}

function ShowTablaPrincipal( var iEntidad : TipoEntidad; const iClasifi : eClasifiReporte ) : Boolean;
begin
     if TablaPrincipal = NIL then
        TablaPrincipal := TTablaPrincipal.Create( Application.MainForm );
     with TablaPrincipal do
     begin
          Entidad := iEntidad;
          Clasificacion := iClasifi;
          ShowModal;
          Result := ModalResult = mrOk;
          if Result then
             iEntidad := Entidad;
     end;
end;

function TTablaPrincipal.GetEntidad : TipoEntidad;
begin
     with LbTablas do
          Result := TipoEntidad(Items.Objects[ItemIndex]);
end;


procedure TTablaPrincipal.RGClasificacionClick(Sender: TObject);
begin
     LlenaListBox;
end;

procedure TTablaPrincipal.LlenaListBox;
 var i, iOffset: integer;

begin
     {$ifdef RDD}
     iOffset :=0;
     {$else}
     iOffset :=1;
     {$endif}
     with dmDiccionario, RgClasificacion do
          TablasPorClasificacion( Integer( Items.Objects[ItemIndex]) + iOffset,
                                  LBTablas.Items );

     with LbTablas do
     begin
          for i := 0 to Items.Count -1 do

          if TipoEntidad(Items.Objects[i]) = FEntidad then
          begin
               ItemIndex:= i;
               Break;
          end;
          if ItemIndex = -1 then
             ItemIndex := 0;
     end;
end;

procedure TTablaPrincipal.LBTablasDblClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;


procedure TTablaPrincipal.FormShow(Sender: TObject);
begin

{$ifdef RDD}
     RGClasificacion.ItemIndex := dmDiccionario.GetPosClasifi( RGClasificacion.Items, Clasificacion );
{$else}
     RGClasificacion.ItemIndex := 0 ;
{$endif}

     LlenaListBox;
     {$IFDEF COMPARTE_MGR}
     RGClasificacion.Enabled := TRUE;
     {$ELSE}
            {$ifdef ADUANAS}
            RGClasificacion.Enabled := TRUE;
            {$ELSE}
            RGClasificacion.Enabled := dmCliente.ModoTress;
            {$endif}
     {$ENDIF}
end;

procedure TTablaPrincipal.FormCreate(Sender: TObject);
begin
     dmDiccionario.GetListaClasifi(RGClasificacion.Items,FALSE);
     {$ifndef TRESS}
            {$IFDEF COMPARTE_MGR}
            RgClasificacion.Visible := TRUE;
            RGClasificacion.Height := 150;
            RGClasificacion.Top := 57;
            {$ELSE}
                   {$ifdef ADUANAS}
                   RgClasificacion.Visible := TRUE;
                   {$ELSE}
                   RgClasificacion.Visible := FALSE;
                   gbTablaPrincipal.Left := 87;
                   {$ENDIF}
            {$ENDIF}
     {$endif}
end;

procedure TTablaPrincipal.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
     CanClose := (lbTablas.Items.Count > 0) or (lbTablas.ItemIndex >= 0 );
     if not CanClose then
        ZInformation( Caption, 'La Clasificación no contiene Ninguna Tabla'  ,0 );
end;

end.
