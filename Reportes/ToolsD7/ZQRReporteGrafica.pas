unit ZQRReporteGrafica;

{$ifdef TRESS}
  {$define CR2020}
{$else}
       {$ifdef SELECCION}
               {$define CR2020}
       {$else}
             {$ifdef VISITANTES}
                     {$define CR2020}
             {$endif}
       {$endif}
{$endif}



interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZQRReporte, Db, QRDLDR, QRDesign, QuickRpt, Qrctrls, Qrdctrls, TDMULTIP,
  ExtCtrls, Chart, DBChart, TeeProcs, TeEngine, QrTee,
  ZAgenteSQLClient,
  ZetaCommonClasses,
  {$ifdef QR362}
  qrpBaseCtrls,
  {$endif}
  StdCtrls, ComCtrls,
{$ifndef VER130}
  thsdRuntimeLoader, thsdRuntimeEditor, qrdBaseCtrls,
  qrdQuickrep,
{$endif}  
  ZReportTools;

type
  TQrReporteGrafica = class(TQrReporte)
  private
    procedure AsignaChart(oChart: TDBChart);
    procedure CambiaSeries(oOrigen, oDestino: TCustomChart);
    procedure BeforePrintDetalle(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure BeforePrintGrupos(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FiltraDataSetGrupos;
    { Private declarations }
  protected
    QrChart : TQRDesignTeeChart;
    FDataSet : TDataSet;
    FTotalPaginas : integer;
    FPagina : integer;
    FDetailAlinea : TAlign;
    FDetailTop,FDetailLeft,
    FDetailWidth,FDetailHeight : integer;
  public
    { Public declarations }
    procedure Init( oAgente : TSQLAgenteClient; oDatosImpresion : TDatosImpresion; iCountParametros: Integer; var lHayImagenes : Boolean  ); override;
    procedure GeneraVariasGraficas( oChart : TDBChart; oDataSet, oGrupos : TDataSet; const Paginas : integer );
    procedure GeneraUnaGrafica( oChart : TDBChart; oDataSet : TDataSet; const Paginas : integer );
  end;

var
  QrReporteGrafica: TQrReporteGrafica;

  procedure PreparaGrafica;
  procedure DesPreparaGrafica;


implementation
uses ZetaCommonTools,
     ZFuncsCliente;

{$R *.DFM}

procedure PreparaGrafica;
begin
     if QrReporteGrafica = NIL then
        QrReporteGrafica := TQrReporteGrafica.Create( Application );
end;

procedure DesPreparaGrafica;
begin
     QrReporteGrafica.Free;
     QrReporteGrafica := NIL;
end;

procedure TQrReporteGrafica.CambiaSeries(oOrigen, oDestino : TCustomChart);
 var
    i: integer;
begin
     {$ifdef false}
     with oOrigen.SeriesList do
          while Count > 0 do
          {$ifdef ver130}
                Series[ 0 ].ParentChart := oDestino;
          {$else}
          begin
                Items[ 0 ].ParentChart := NIL;
                Items[ 0 ].ParentChart := oDestino;
          end;
          {$endif}
     {$endif}


     for i:=0 to oOrigen.SeriesCount -1 do
     begin
          oDestino.AddSeries(oOrigen.Series[i]);
     end;

end;

procedure TQrReporteGrafica.AsignaChart( oChart : TDBChart );
 const K_NO_INVALIDATE = 2020;
 var i : integer;

begin
     CargaReporte( fDirDefault+FDatosImpresion.Grafica );

     for i := 0 to ComponentCount -1 do
     begin
          if Components[i].ClassNameIs('TQRDesignTeeChart') then
          begin
               QRChart := TQRDesignTeeChart(Components[i]);
               Break;
          end;
     end;

     if QRChart = NIL then
        Raise Exception.Create('La Plantilla GRAFICA.QR2 no Contiene Ning�n Elemento Tipo Gr�fica');

     with QRChart do
     begin
          FDetailAlinea := Align;
          FDetailTop := Top;
          FDetailLeft := Left;
          FDetailWidth := Width;
          FDetailHeight := Height;
          {$ifdef CR2020}
          Chart.Tag := K_NO_INVALIDATE;
          {$endif}
     end;

     QRChart.Chart.Assign( oChart );


     CambiaSeries( oChart, QRChart.Chart );
end;

procedure TQrReporteGrafica.FiltraDataSetGrupos;
  const Off_SET = 3;
  var sTitulo : string;

  procedure AgregaTitulo( const Posicion : integer );
  begin
       case Posicion mod OFF_SET of
            0 :
            begin
                 if sTitulo <> '' then
                 begin
                      QrChart.Chart.Title.Text.Add( sTitulo );
                      sTitulo := '';
                 end;
            end;
            2: sTitulo := sTitulo + ':' + FQReport.DataSet.Fields[Posicion].AsString
            else sTitulo := sTitulo + FQReport.DataSet.Fields[Posicion].AsString;
       end;
  end;
  var sFiltro : string;
     i : integer;

begin
     sFiltro := '';
     QrChart.Chart.Title.Text.Text := '';

     with FQReport do
     begin
          for i := 0 to DataSet.FieldCount-1 do
          begin
               AgregaTitulo(i);
               sFiltro := ConcatFiltros(sFiltro, DataSet.Fields[i].FieldName + '='+
                                                 EntreComillas(DataSet.Fields[i].AsString));
          end;
          AgregaTitulo(DataSet.FieldCount);
     end;
     FDataSet.Filter := sFiltro;
     FDataSet.Filtered := TRUE;

     QrChart.Chart.MaxPointsPerPage := iMin(5,FDataset.RecordCount);
     QrChart.Chart.RefreshData;
end;

procedure TQrReporteGrafica.BeforePrintDetalle(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
     with QrChart.Chart do
     begin
          Inc(FPagina);
          PrintBand := FPagina <= FTotalPaginas;
          if PrintBand then
             Page := FPagina;
     end;
end;

procedure TQrReporteGrafica.BeforePrintGrupos(Sender: TQRCustomBand; var PrintBand: Boolean);
begin
     PrintBand :=True;
     FiltraDatasetGrupos;
end;

procedure TQrReporteGrafica.GeneraVariasGraficas( oChart : TDBChart; oDataSet, oGrupos : TDataSet;
                                                  const Paginas : integer );
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crDefault;

     try
        AsignaChart( oChart );
        FDataSet := oDataSet;
        FPagina := 0;
        FTotalPaginas := Paginas;
        if oGrupos <> NIL then
        begin
             FQReport.DataSet := oGrupos;
             TQrCustomBand(QrChart.Parent).BeforePrint := BeforePrintGrupos;
        end
        else
        begin
             FTotalPaginas := Paginas;
             FQReport.DataSet := oDataset;
             TQrCustomBand(QrChart.Parent).BeforePrint := BeforePrintDetalle;
        end;

        with FQReport,Bands do
        begin
             if HasDetail then
             begin
                  DetailBand.Height := Trunc((FDetailHeight/2))-2;
                  Page.Columns := 2;
                  Page.ColumnSpace := 0.01;
             end;
        end;
        with QRChart do
        begin
             Height := FQreport.Bands.DetailBand.Height;
             Width := FQreport.Bands.DetailBand.Width;
             Align := alClient;
        end;

        {$ifdef CAROLINA}
        FQReport.Preview;
        FPagina := 0;
        {$ELSE}
        FQReport.Print;
        {$ENDIF}

     finally
            CambiaSeries( QrChart.Chart, oChart );
            Screen.Cursor := oCursor;
     end;
end;

procedure TQrReporteGrafica.GeneraUnaGrafica( oChart : TDBChart; oDataSet : TDataSet;
                                              const Paginas : integer );
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crDefault;

     try
        AsignaChart( oChart );
        FPagina := 0;
        FDataSet := oDataSet;
        FQReport.DataSet := FDataset;
        //FTotalPaginas := Trunc(FDataset.RecordCount/oChart.MaxPointsPerPage);
        FTotalPaginas := Paginas;
        TQrCustomBand(QrChart.Parent).BeforePrint := BeforePrintDetalle;
        with FQReport,Bands do
        begin
             if HasDetail then
                DetailBand.Height := FDetailHeight;
             Page.Columns := 1;
             Page.ColumnSpace := 0;
        end;
        with QRChart do
        begin
             Align := FDetailAlinea;
             Top   := FDetailTop;
             Left  := FDetailLeft;
             Width := FDetailWidth;
             Height := FDetailHeight;
        end;

        {$ifdef CAROLINA}
        FQReport.Preview;
        FPagina := 0;
        {$ELSE}
        FQReport.Print;
        {$ENDIF}

        finally
            if QrChart<> NIL then
               CambiaSeries( QrChart.Chart, oChart );
            Screen.Cursor := oCursor;
     end;
end;

{procedure TQrReporteGrafica.GeneraGrafica( oChart : TDBChart;
                                           oDataSet, oGrupos : TDataSet );
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crDefault;

     try
        AsignaChart( oChart );
        FDataSet := oDataSet;
        if ( oGrupos=NIL ) or oGrupos.IsEmpty then
        begin
             FPlantillaGrafica := 'GRAFICA.QR2';
             FQReport.DataSet := FDataset;
             FPagina := 0;
             FTotalPaginas := Trunc(FDataset.RecordCount/oChart.MaxPointsPerPage);
             TQrCustomBand(QrChart.Parent).BeforePrint := BeforePrintDetalle;
             FQReport.Preview;
             //FQReport.Print;
        end
        else
        begin
             FPlantillaGrafica := 'GRAFICA_GRUPO.QR2';
             FQReport.DataSet := oGrupos;
             TQrCustomBand(QrChart.Parent).BeforePrint := BeforePrintGrupos;
             FQReport.Preview;
        end;
     finally
            CambiaSeries( QrChart.Chart, oChart );
     end;

     Screen.Cursor := oCursor;
end;}

procedure TQrReporteGrafica.Init( oAgente : TSQLAgenteClient; oDatosImpresion : TDatosImpresion; iCountParametros: Integer; var lHayImagenes : Boolean  ); 
begin
     ZFuncsCliente.RegistraFunciones;

     FAgente := oAgente;
     FDatosImpresion := oDatosImpresion;
     fDirDefault := zReportTools.DirPlantilla;
     CargaReporte( fDirDefault+FDatosImpresion.Grafica );

     if FQReport.Bands.HasDetail then
        FDetailHeight := FQReport.Bands.DetailBand.Height;
     AgregaFormulas;

end;

end.
