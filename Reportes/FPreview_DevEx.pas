{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: QuickReport 2.0 Delphi 1.0/2.0/3.0                      ::
  ::                                                         ::
  :: QRPREV - QuickReport standard preview form              ::
  ::                                                         ::
  :: Copyright (c) 1997 QuSoft AS                            ::
  :: All Rights Reserved                                     ::
  ::                                                         ::
  :: web: http://www.qusoft.no   mail: support@qusoft.no     ::
  ::                             fax: +47 22 41 74 91        ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

unit FPreview_DevEx;

interface

{$define EZM}
{$INCLUDE DEFINES.INC}

uses
//{$ifdef win32}
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, QRPrntr,
  QuickRpt, dxSkinsCore,
  //(@am): COMENTADO TEMPORALMENTE - Este codigo no ha sido necesario
  //{$else}
  {Wintypes, WinProcs, {Sysutils, Messages, Classes, Controls, StdCtrls, ExtCtrls,
   Buttons, QRPrntr, Graphics, Forms, Dialogs, } QR2const, {QuickRpt, }
  //{$endif}
  TressMorado2013,
  dxSkinsDefaultPainters,  dxSkinsdxBarPainter,
  dxBarExtItems, dxBar, cxClasses, ImgList, cxGraphics;

type
  TPreview_DevEx = class(TForm)
    StatusPanel: TPanel;
    Panel1: TPanel;
    Status: TLabel;
    QRPreview: TQRPreview;
    Timer1: TTimer;
    cxImageList16Edicion: TcxImageList;
    DevEx_BarManagerEdicion: TdxBarManager;
    BarSuperior: TdxBar;
    dxBarButton_ZoomToFit: TdxBarButton;
    dxBarButton_ZoomTo100: TdxBarButton;
    dxBarButton_ZoomToWidth: TdxBarButton;
    dxBarButton_FirstPage: TdxBarButton;
    dxBarButton_PrevPage: TdxBarButton;
    dxBarButton_NextPage: TdxBarButton;
    dxBarButton_LastPage: TdxBarButton;
    dxBarButton_Print: TdxBarButton;
    dxBarButton_BGrafica: TdxBarButton;
    dxBarButton_PegarBtn: TdxBarButton;
    dxBarButton_UndoBtn: TdxBarButton;
    dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dxBarButton_ZoomToFitClick(Sender: TObject);
    procedure dxBarButton_ZoomTo100Click(Sender: TObject);
    procedure dxBarButton_ZoomToWidthClick(Sender: TObject);
    procedure dxBarButton_FirstPageClick(Sender: TObject);
    procedure dxBarButton_PrevPageClick(Sender: TObject);
    procedure dxBarButton_NextPageClick(Sender: TObject);
    procedure dxBarButton_LastPageClick(Sender: TObject);
    procedure dxBarButton_PrintClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QRPreviewPageAvailable(Sender: TObject; PageNum: Integer);
{$ifdef EZM}
    procedure QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    //procedure BGraficaClick(Sender: TObject);
    procedure dxBarButton_BGraficaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DevEx_BarManagerEdicionShowToolbarsPopup(
      Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
{$endif}
  private
    FQRPrinter : TQRPrinter;
    FReporte : TQuickRep;
    FGrafica: Boolean;
    FZoomPreview: Boolean;
    procedure SetGrafica(const Value: Boolean);
  public
    constructor CreatePreview(AOwner : TComponent; aQRPrinter : TQRPrinter; oReport : TQuickRep ); virtual;
    procedure UpdateInfo;
    property QRPrinter : TQRPrinter read FQRPrinter write FQRPrinter;
    property Reporte : TQuickRep read FReporte write FReporte;
    property ShowGrafica : Boolean read FGrafica write SetGrafica;
  end;

implementation

uses FRangoPaginas_DevEx,
     DCliente,
     FGrafica_DevEx;
{$R *.DFM}

constructor TPreview_DevEx.CreatePreview(AOwner : TComponent; aQRPrinter : TQRPrinter; oReport : TQuickRep);
begin
     inherited Create(AOwner);
     QRPrinter := aQRPrinter;
     FReporte := oReport;
     QRPreview.QRPrinter := aQRPrinter;
     if QRPrinter <> nil then Caption := QRPrinter.Title;
     dxBarButton_ZoomToFit.Down := True;
     WindowState := wsMaximized;
end;

procedure TPreview_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Timer1.Enabled := FALSE;
     Action := caFree;
end;

procedure TPreview_DevEx.UpdateInfo;
begin
{$ifdef EZM}
     if FQrPrinter <> NIL then
        Status.Caption := 'P�gina ' + IntToStr(QRPreview.PageNumber) +
                          ' de ' + IntToStr(QRPreview.QRPrinter.PageCount);
{$else}
     Status.Caption := LoadStr(SqrPage) + ' ' + IntToStr(QRPreview.PageNumber) + ' ' +
                       LoadStr(SqrOf) + ' ' + IntToStr(QRPreview.QRPrinter.PageCount);
{$endif}

    {***(@am): UpdateImage no respeta el Zoom que se le daba al QRPreview en el FormShow,
               debemos realizarlo aqui solo la primera vez.***}
    if ((dxBarButton_ZoomToWidth.Down) and (FZoomPreview)) then
    begin
      QRPreview.ZoomToWidth;
      FZoomPreview:=FALSE;
    end;

end;

procedure TPreview_DevEx.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_Next : if Shift=[ssCtrl] then
                dxBarButton_LastPageClick(Self)
              else
                dxBarButton_NextPageClick(Self);
    VK_Prior : if Shift=[ssCtrl] then
                 dxBarButton_FirstPageClick(Self)
               else
                 dxBarButton_PrevPageClick(Self);
    VK_Home : dxBarButton_FirstPageClick(Self);
    VK_End : dxBarButton_LastPageClick(Self);
    VK_ESCAPE: Close;
  end;
end;

procedure TPreview_DevEx.QRPreviewPageAvailable(Sender: TObject;
  PageNum: Integer);
begin
  UpdateInfo;
end;

{$ifdef EZM}
procedure TPreview_DevEx.QRPreviewMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     with QrPreview do
     begin
          case Button of
               mbLeft: if ( Zoom < 300 ) then Zoom := Zoom + 10;
               mbRight: if ( Zoom > 0 ) then Zoom := Zoom - 10;
          end;
     end;
end;
{$endif}

procedure TPreview_DevEx.FormResize(Sender: TObject);
begin
     WindowState := wsMaximized;
end;

procedure TPreview_DevEx.FormShow(Sender: TObject);
begin
     //dxBarButton_ZoomToFitClick(nil); (@am): Se cambio a UpdateInfo, porque al ejecutarse QRPreviewPageAvailable la primera vez ya no lo respeta.
     FZoomPreview:= TRUE; //(@am): Bandera para ejecutar el Zoom solo al mostrar la forma en UpdateInfo.
     {$IFDEF PREVIEW_100}
     Timer1.Enabled := TRUE;
     {$ENDIF}
end;

procedure TPreview_DevEx.SetGrafica(const Value: Boolean);
begin
     FGrafica := Value;
     if Value then
        dxBarButton_BGrafica.Visible := ivAlways
     else
        dxBarButton_BGrafica.Visible := ivNever;
end;

procedure TPreview_DevEx.Timer1Timer(Sender: TObject);
begin
     Close;
end;

procedure TPreview_DevEx.dxBarButton_ZoomToFitClick(Sender: TObject);
begin
     Application.ProcessMessages;
  QRPreview.ZoomToFit;
end;

procedure TPreview_DevEx.dxBarButton_ZoomTo100Click(Sender: TObject);
begin
   Application.ProcessMessages;
  QRPreview.Zoom := 100;
end;

procedure TPreview_DevEx.dxBarButton_ZoomToWidthClick(Sender: TObject);
begin
     Application.ProcessMessages;
     QRPreview.ZoomToWidth;
end;

procedure TPreview_DevEx.dxBarButton_FirstPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := 1;
  UpdateInfo;
end;

procedure TPreview_DevEx.dxBarButton_PrevPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPreview.PageNumber - 1;
  UpdateInfo;
end;

procedure TPreview_DevEx.dxBarButton_NextPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPreview.PageNumber + 1;
  UpdateInfo;
end;

procedure TPreview_DevEx.dxBarButton_LastPageClick(Sender: TObject);
begin
     QRPreview.PageNumber := QRPrinter.PageCount;
  UpdateInfo;
end;

procedure TPreview_DevEx.dxBarButton_PrintClick(Sender: TObject);
var iInicial, iFinal, iCopias : integer;

 function Min( const i, j : integer ) : integer;
 begin
      if i<j then Result := i
      else Result := j;
 end;

begin
     iInicial := Min( FQrPrinter.FirstPage, FQrPrinter.PageCount );
     iFinal := Min( FQrPrinter.LastPage, FQrPrinter.PageCount);
     iCopias := FQrPrinter.Copies;

     if DialogoPaginas( iInicial, iFinal, iCopias ) then
     begin
          {***(@am):Las propiedades que hay que tomar en cuenta para establecer el rango de impresiones
                    cambiaron para XE5.***}
          {$IFDEF TRESS_DELPHIXE5_UP}
              fReporte.PrinterSettings.FirstPage := iInicial;
              fReporte.PrinterSettings.LastPage := iFinal;
              fReporte.PrinterSettings.Copies := iCopias;
              QrPreview.QrPrinter.FirstPage := fReporte.PrinterSettings.FirstPage;
              QrPreview.QRPrinter.LastPage := fReporte.PrinterSettings.LastPage;
              QrPreview.QRPrinter.Copies  := fReporte.PrinterSettings.Copies;
          {$ELSE}
              fReporte.PrinterSettings.FirstPage := iInicial;
              fReporte.PrinterSettings.LastPage := iFinal;
              fReporte.PrinterSettings.Copies := iCopias;
          {$ENDIF}
          //QrPreview.QrPrinter.Print;  //OLD
          fReporte.Print; //(@am): Se optimiza para que utilice el mismo metodo de impresion que la forma de edicion de reportes.
     end;
end;

procedure TPreview_DevEx.dxBarButton_BGraficaClick(Sender: TObject);
begin
      Grafica_DevEx.GeneraGrafica; 
end;

procedure TPreview_DevEx.DevEx_BarManagerEdicionShowToolbarsPopup(
  Sender: TdxBarManager; PopupItemLinks: TdxBarItemLinks);
begin
     Abort;
end;

end.
