unit FDialogoPrinc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TDialogoPrinc = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Image1: TImage;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure BitBtn3Click(Sender: TObject);
  private
    FBitacora : TStrings;
    procedure Informacion;
  public
    property Bitacora : TStrings read FBitacora write FBitacora;
  end;

var
  DialogoPrinc: TDialogoPrinc;

function ShowDialogoPrinc( const oBitacora : TStrings ) : Boolean;

implementation

uses ZetaCommonClasses, ZetaDialogo;
{$R *.DFM}

function ShowDialogoPrinc( const oBitacora : TStrings ) : Boolean;
begin
     if DialogoPrinc = NIL then
        DialogoPrinc := TDialogoPrinc.Create(Application.Mainform);

     with DialogoPrinc do
     begin
          Bitacora := oBitacora;
          ShowModal;
          Result := ModalResult = mrOk;
     end;
end;

procedure TDialogoPrinc.Informacion;
 const MSG1 = 'Los siguientes datos NO son COMPATIBLES con la nueva Tabla Principal:';
begin
     ZInformation( Caption, MSG1 + CR_LF + FBitacora.Text, 0 );
end;

procedure TDialogoPrinc.BitBtn3Click(Sender: TObject);
begin
     Informacion;
end;


end.
