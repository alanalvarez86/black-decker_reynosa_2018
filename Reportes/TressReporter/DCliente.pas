unit DCliente;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient,
     Variants,
     Consultas_TLB,
     Reportes_TLB,
     DBasicoCliente,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet;

type
  TdmCliente = class(TBasicoCliente)
    cdsReportes: TZetaClientDataSet;
    cdsQuery: TZetaClientDataSet;
    cdsPeriodo: TZetaClientDataSet;
    cdsLookupReportes: TZetaLookupDataSet;
    cdsEmpleado: TZetaClientDataSet;
    cdsPatron: TZetaClientDataSet;
    cdsResultado: TZetaClientDataSet;
    procedure cdsReportesAlAdquirirDatos(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsReportesAfterOpen(DataSet: TDataSet);
    procedure cdsLookupReportesAfterOpen(DataSet: TDataSet);
    procedure cdsQueryAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FColPtr: Integer;
    FRowPtr: Integer;
    FFormatoFecha: String;
    FGrupo: Integer;
    FParametros: TZetaParams;
    FDatosPeriodo: TDatosPeriodo;
    FDatosIMSS: TDatosIMSS;
    FEmpleado: TNumEmp;
    FRefrescaActivos: Boolean;
    FPeriodoInicializado: Boolean;
    FServerConsultas: IdmServerConsultasDisp;
    FServerReportes: IdmServerReportesDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
    function GetServerReportes: IdmServerReportesDisp;
    function GetIMSSMes: Integer;
    function GetIMSSPatron: String;
    function GetIMSSTipo: eTipoLiqIMSS;
    function GetIMSSYear: Integer;
    procedure CargaActivosIMSS(Parametros: TZetaParams);
    procedure CargaActivosPeriodo(Parametros: TZetaParams);
    procedure CargaActivosSistema( Parametros: TZetaParams );
    procedure ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure SetDatosPeriodo(const Value: TDatosPeriodo);
  protected
    { Protected declarations }
    procedure GetEmpleadoInicial;
    procedure GetIMSSInicial;
    procedure GetPeriodoInicial;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property FormatoFecha: String read FFormatoFecha write FFormatoFecha;
    property Parametros: TZetaParams read FParametros;
    property PeriodoActivo: TDatosPeriodo read FDatosPeriodo write SetDatosPeriodo;
    property IMSSPatron: String read GetIMSSPatron;
    property IMSSYear: Integer read GetIMSSYear;
    property IMSSTipo: eTipoLiqIMSS read GetIMSSTipo;
    property IMSSMes: Integer read GetIMSSMes;
    property Empleado: integer read FEmpleado write FEmpleado;
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    function GetDatosPeriodoActivo: TDatosPeriodo;
    function GetReporteNombre: String;
    function GetReporteNumero: Integer;
    function GetSQLData(const sSQL: String ): Integer;
    function GetValorActivoStr( const eTipo: TipoEstado ): String; override;
    function GetVarActivo(const sValor: String): OleVariant;
    function GetValorActivo(const sValor: String): String;override;
    function InitCompanies: Boolean;
    function SetEmpresaActiva( const sCodigo: String ): Boolean;
    function UsuarioLogin( const sNombre, sClave: String ): Boolean;
    function EoF: Boolean;
    function EoRow: Boolean;
    function FieldName: String;
    function Valor: OleVariant;
    procedure BuscarReportes(const eClasificacion: eClasifiReporte);
    procedure CargaActivosTodos( Parametros: TZetaParams ); override;
    procedure First;
    procedure InitResultDataset;
    procedure Next;
    procedure NextCol;
    procedure SetVarActivo(const sValor: String; const Valor: OleVariant);
  end;

threadvar
   dmCliente: TdmCliente;


implementation

uses FAutoClasses,
     DDiccionario,
     ZReportTools,
     ZAccesosMgr,
     ZetaCommonTools,
     ZetaRegistryCliente,
     ZetaBusqueda,
     ZetaTipoEntidadTools,
     ZetaWinAPITools,
     ZetaTipoEntidad;

{$R *.DFM}

const
     K_EMPLEADO = '@EMPLEADO';

     K_IMSS_PATRON = '@PATRON';
     K_IMSS_YEAR = '@IMSS_YEAR';
     K_IMSS_MES = '@IMSS_MES';
     K_IMSS_TIPO = '@IMSS_TIPO';

     K_YEAR = '@YEAR';
     K_TIPO = '@TIPO';
     K_NUMERO = '@NUMERO';

     K_FECHA = '@FECHA';


{ ********* TdmCliente ********** }

constructor TdmCliente.Create(AOwner: TComponent);
begin
     FFormatoFecha := 'dd/mm/yyyy';
     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;
     inherited Create( AOwner );
end;

procedure TdmCliente.DataModuleCreate(Sender: TObject);
begin
     FGrupo := 0;
     with FDatosIMSS do
     begin
          Patron := VACIO;
          Mes := ZetaCommonTools.TheMonth( Date );
          Tipo := tlOrdinaria;
          Year := ZetaCommonTools.TheYear( Date );
     end;
     FEmpleado := 0;
     with FDatosPeriodo do
     begin
          Year := ZetaCommonTools.TheYear( Date );
          Tipo := tpSemanal;
          Numero := Trunc( ( Now - ZetaCommonTools.FirstDayOfYear( Year ) ) / 7 ) + 1;
     end;
     FRefrescaActivos := TRUE;
     FPeriodoInicializado := FALSE;
     inherited;
end;

destructor TdmCliente.Destroy;
begin
     ZetaRegistryCliente.ClearClientRegistry;
     inherited Destroy;
end;

procedure TdmCliente.DataModuleDestroy(Sender: TObject);
begin
     inherited;
     {$ifdef DOS_CAPAS}
     {$endif}
end;

function TdmCliente.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( CreaServidor( CLASS_dmServerConsultas, FServerConsultas ) );
end;

function TdmCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp( CreaServidor( CLASS_dmServerReportes, FServerReportes ) );
end;

{ ********** Métodos para Autenticar y Empresas ************* }

function TdmCliente.UsuarioLogin( const sNombre, sClave: String ): Boolean;
begin
     case GetUsuario( sNombre, sClave, True ) of
          lrOK: Result := True;
     else
         Result := False;
     end;
end;

function TdmCliente.InitCompanies: Boolean;
begin
     with cdsCompany do
     begin
          if not Active or HayQueRefrescar then
          begin
               Data := Servidor.GetCompanys( Usuario, Ord( tc3Datos ) );
               ResetDataChange;
          end;
          Result := ( RecordCount > 0 );
     end;
end;

function TdmCliente.SetEmpresaActiva(const sCodigo: String): Boolean;
var
   iGrupo: Integer;
   lCompanyChange: Boolean;
{$ifdef FALSE}
{$endif}
begin
     Result := False;
     lCompanyChange := False;
     {$ifdef FALSE}
     {$endif}
     InitCompanies;
     with cdsCompany do
     begin
          if ( FieldByName( 'CM_CODIGO' ).AsString = sCodigo ) then
          begin
               Result := True;
               SetCompany;
          end
          else
          begin
               if Locate( 'CM_CODIGO', sCodigo, [] ) then
               begin
                    Result := True;
                    lCompanyChange := True;
                    {$ifdef FALSE}
                    {$endif}
                    SetCompany;
               end;
          end;
     end;
     if Result then
     begin
          iGrupo := GetGrupoActivo;
          if ( iGrupo <> FGrupo ) or lCompanyChange then
          begin
               FGrupo := iGrupo;
               ZAccesosMgr.LoadDerechos;
          end;
          {$ifdef FALSE}
          if lCompanyChange or FRefrescaActivos then
          begin
               GetEmpleadoInicial;
               GetPeriodoInicial;
               GetIMSSInicial;
               FRefrescaActivos := FALSE;   // No se refrescará hasta que cambie de empresa o se vuelva a crear el datamodule
          end;
          {$endif}
     end;
end;

{ ********** Navegación del ClientDataset ************* }

procedure TdmCliente.InitResultDataset;
begin
     with cdsQuery do
     begin
          if Active then
          begin
               EmptyDataset;
               Active := False;
          end;
          Init;
     end;
end;

function TdmCliente.EoF: Boolean;
begin
     with cdsQuery do
     begin
          if Active then
             Result := Eof or ( ( Autorizacion.EsDemo ) and ( FRowPtr > 3 ) )
          else
              Result := True;
     end;
end;

function TdmCliente.EoRow: Boolean;
begin
     with cdsQuery do
     begin
          if Active then
             Result := ( FColPtr >= FieldCount )
          else
              Result := True;
     end;
end;

procedure TdmCliente.First;
begin
     with cdsQuery do
     begin
          if Active then
             First;
     end;
     FColPtr := 0;
end;

procedure TdmCliente.Next;
begin
     with cdsQuery do
     begin
          if Active then
             Next;
     end;
     FColPtr := 0;
     Inc( FRowPtr );
end;

procedure TdmCliente.NextCol;
begin
     Inc( FColPtr );
end;

function TdmCliente.Valor: OleVariant;
begin
     with cdsQuery do
     begin
          if Active then
          begin
               with cdsQuery.Fields[ FColPtr ] do
               begin
                    {$ifdef FALSE}
                    case DataType of
                         ftString: Result := AsString;
                         ftSmallint: Result := IntToStr( AsInteger );
                         ftInteger: Result := IntToStr( AsInteger );
                         ftWord: Result := IntToStr( AsInteger );
                         ftBoolean: Result := ZetaCommonTools.BoolToSiNo( AsBoolean );
                         ftFloat: Result := Format( '%n', [ AsFloat ] );
                         ftCurrency: Result := Format( '%n', [ AsCurrency ] );
                         ftBCD: Result := Format( '%n', [ AsFloat ] );
                         ftDate: Result := FormatDateTime( FormatoFecha, AsDateTime );
                         ftTime: Result := FormatDateTime( 'hh:nn:ss', AsDateTime );
                         ftDateTime: Result := FormatDateTime( FormatoFecha, AsDateTime );
                         ftBlob: Result := '<BLOB>';
                         ftMemo: Result := AsString;
                         ftGraphic: Result := '<GRAPHIC>';
                         ftWideString: Result := AsString;
                         ftLargeint: Result := IntToStr( AsInteger );
                    else
                        Result := '<???>';
                    end;
                    {$endif}
                    Result := Value;
               end;
          end
          else
              Result := NULL;
     end;
end;

function TdmCliente.FieldName: String;
begin
     with cdsQuery do
     begin
          if Active then
             Result := Fields[ FColPtr ].DisplayLabel
          else
              Result := VACIO;
     end;
end;

{ ********** Valores Activos ************ }

procedure TdmCliente.GetEmpleadoInicial;
var
   Datos: OleVariant;
   lResult: Boolean;
begin
     lResult := Servidor.GetEmpleadoSiguiente( Empresa, 0, Datos );
     if lResult then
     begin
          with cdsEmpleado do
          begin
               Data := Datos;
               if not Eof then
                  FEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
          end;
     end;
end;

procedure TdmCliente.GetPeriodoInicial;
begin
     with cdsPeriodo do
     begin
          Data := Servidor.GetPeriodoInicial( Empresa, TheYear(Date), Ord( tpSemanal ) );
          with FDatosPeriodo do
          begin
               Year := FieldByName( 'PE_YEAR' ).AsInteger;
               Tipo := eTipoPeriodo(FieldByName( 'PE_TIPO' ).AsInteger);
               Numero := FieldByName( 'PE_NUMERO' ).AsInteger;
               Inicio := FieldByName( 'PE_FEC_INI' ).AsDateTime;
               Fin := FieldByName( 'PE_FEC_FIN' ).AsDateTime;
               Pago := FieldByName( 'PE_FEC_PAG' ).AsDateTime;
               Dias := FieldByName( 'PE_DIAS' ).AsInteger;
               Status := eStatusPeriodo( FieldByName( 'PE_STATUS' ).AsInteger );
               Uso := eUsoPeriodo( FieldByName( 'PE_USO' ).AsInteger );
               Mes := ZetaCommonTools.iMax( 1, FieldByName( 'PE_MES' ).AsInteger );
               SoloExcepciones := ZetaCommonTools.zStrToBool( FieldByName( 'PE_SOLO_EX' ).AsString );
               IncluyeBajas := ZetaCommonTools.zStrToBool( FieldByName( 'PE_INC_BAJ' ).AsString );
          end;
     end;
end;

procedure TdmCliente.GetIMSSInicial;
begin
     with FDatosIMSS do
     begin
          with cdsPatron do
          begin
               Data := Servidor.GetPatrones( Empresa );
               if IsEmpty then
                  Patron := ''
               else
                   Patron := FieldByName( 'TB_CODIGO' ).AsString;
          end;
          Year := ZetaCommonTools.TheYear( Now );
          Tipo := tlOrdinaria;
          Mes := ZetaCommonTools.TheMonth( Date ) - 1;
          if ( Mes = 0 ) then
          begin
               Mes := 12;
               Year := Year - 1;
          end;
     end;
end;


procedure TdmCliente.CargaActivosIMSS(Parametros: TZetaParams);
begin
     with Parametros do
     begin
          AddString( 'RegistroPatronal', ImssPatron );
          AddInteger( 'IMSSYear', ImssYear );
          AddInteger( 'IMSSMes', ImssMes );
          AddInteger( 'IMSSTipo', Ord( ImssTipo ) );
     end;
end;

procedure TdmCliente.CargaActivosPeriodo(Parametros: TZetaParams);
begin
     if FPeriodoInicializado then
     begin
           with Parametros do
           begin
                AddInteger( 'Year', FDatosPeriodo.Year );
                AddInteger( 'Tipo', Ord( FDatosPeriodo.Tipo ) );
                AddInteger( 'Numero', FDatosPeriodo.Numero );
           end;
     end
     else
     begin
          GetPeriodoInicial;
          with cdsPeriodo do
          begin
               with Parametros do
               begin
                    AddInteger( 'Year', FieldByName( 'PE_YEAR' ).AsInteger );
                    AddInteger( 'Tipo', FieldByName( 'PE_TIPO' ).AsInteger );
                    AddInteger( 'Numero', FieldByName( 'PE_NUMERO' ).AsInteger );
               end;
          end;
     end;
end;

function TdmCliente.GetDatosPeriodoActivo: TDatosPeriodo;
begin
     Result := FDatosPeriodo;
end;

procedure TdmCliente.CargaActivosSistema( Parametros: TZetaParams );
begin
     with Parametros do
     begin
          AddDate( 'FechaAsistencia', FechaDefault );
          AddDate( 'FechaDefault', FechaDefault );
          AddInteger( 'YearDefault', TheYear( FechaDefault ) );
          AddInteger( 'EmpleadoActivo', FEmpleado );
          AddString( 'NombreUsuario', 'REP_WEB' );
          AddString( 'CodigoEmpresa', GetDatosEmpresaActiva.Codigo );
     end;
end;

procedure TdmCliente.CargaActivosTodos(Parametros: TZetaParams);
begin
     CargaActivosIMSS( Parametros );
     CargaActivosPeriodo( Parametros );
     CargaActivosSistema( Parametros );
end;

function TdmCliente.GetIMSSMes: Integer;
begin
     Result := FDatosIMSS.Mes;
end;

function TdmCliente.GetIMSSPatron: String;
begin
     Result := FDatosIMSS.Patron;
end;

function TdmCliente.GetIMSSTipo: eTipoLiqIMSS;
begin
     Result := FDatosIMSS.Tipo;
end;

function TdmCliente.GetIMSSYear: Integer;
begin
     Result := FDatosIMSS.Year;
end;

function TdmCliente.GetValorActivo( const sValor : String ): String;
begin
     Result := dmDiccionario.GetValorActivo( sValor );
end;

function TdmCliente.GetValorActivoStr(const eTipo: TipoEstado): String;
begin
     Result := '';
end;

function TdmCliente.GetVarActivo(const sValor: String): OleVariant;
begin
     if ( sValor = K_EMPLEADO ) then
        Result := FEmpleado
     else
         if ( sValor = K_IMSS_PATRON ) then
            Result := IMSSPatron
         else
             if ( sValor = K_IMSS_YEAR ) then
                Result := IMSSYear
             else
                 if ( sValor = K_IMSS_MES ) then
                    Result := IMSSMes
                 else
                     if ( sValor = K_IMSS_TIPO ) then
                        Result := Ord( IMSSTipo )
                     else
                         if ( sValor = K_YEAR ) then
                            Result := PeriodoActivo.Year
                         else
                             if ( sValor = K_TIPO ) then
                                Result := Ord( PeriodoActivo.Tipo )
                             else
                                 if ( sValor = K_NUMERO ) then
                                    Result := PeriodoActivo.Numero
                                 else
                                     if ( sValor = K_FECHA ) then
                                        Result := FechaDefault
                                     else
                                         Result := sValor;
end;

procedure TdmCliente.SetVarActivo(const sValor: String; const Valor: OleVariant );
begin
     if ( sValor = K_EMPLEADO ) then
        FEmpleado := Valor
     else
         if ( sValor = K_IMSS_PATRON ) then
            FDatosIMSS.Patron := Valor
         else
             if ( sValor = K_IMSS_YEAR ) then
                FDatosIMSS.Year := Valor
             else
                 if ( sValor = K_IMSS_MES ) then
                    FDatosIMSS.Mes := Valor
                 else
                     if ( sValor = K_IMSS_TIPO ) then
                       FDatosIMSS.Tipo := eTipoLiqIMSS( Valor )
                     else
                         if ( sValor = K_YEAR ) then
                            FDatosPeriodo.Year := Valor
                         else
                             if ( sValor = K_TIPO ) then
                                FDatosPeriodo.Tipo := eTipoPeriodo( Valor )
                             else
                                 if ( sValor = K_NUMERO ) then
                                    FDatosPeriodo.Numero := Valor
                                 else
                                     if ( sValor = K_FECHA ) then
                                        FechaDefault := Valor;
end;

function TdmCliente.GetSQLData(const sSQL: String ): Integer;
begin
     with cdsQuery do
     begin
          Data := ServerConsultas.GetQueryGral( Empresa, sSQL );
          Result := RecordCount;
     end;
end;

function TdmCliente.GetReporteNumero: Integer;
begin
     Result := cdsLookupReportes.FieldByName( 'RE_CODIGO' ).AsInteger;
end;

function TdmCliente.GetReporteNombre: String;
begin
     Result := cdsLookupReportes.FieldByName( 'RE_NOMBRE' ).AsString;
end;

procedure TdmCliente.BuscarReportes( const eClasificacion: eClasifiReporte );
begin
     with cdsLookUpReportes do
     begin
          Data := ServerReportes.GetReportes( Empresa, Ord( eClasificacion ), Ord(crFavoritos), Ord( crSuscripciones ), VACIO, True );
     end;
end;


{ ********** Client Datasets ************ }

procedure TdmCliente.cdsReportesAlAdquirirDatos(Sender: TObject);
begin
     inherited;
     cdsReportes.Data := ServerReportes.GetEscogeReporte( Empresa, Ord( enNomina ), Ord( trPoliza ), True );
end;

procedure TdmCliente.cdsReportesAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsReportes do
     begin
          MaskFecha( 'RE_FECHA' );
          ListaFija( 'RE_TIPO', lfTipoReporte );
     end;
end;

procedure TdmCliente.cdsQueryAfterOpen(DataSet: TDataSet);
begin
     inherited;
     FRowPtr := 1;
end;

procedure TdmCliente.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := VACIO
          else
              Text := ZetaTipoEntidadTools.ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
     end
     else
         Text:= Sender.AsString;
end;

procedure TdmCliente.cdsLookupReportesAfterOpen(DataSet: TDataSet);
begin
     inherited;
     with cdsLookUpReportes do
     begin
          FieldByName( 'RE_CODIGO' ).Alignment := taLeftJustify;
          MaskFecha( 'RE_FECHA' );
          ListaFija( 'RE_TIPO', lfTipoReporte );
          with FieldByName( 'RE_ENTIDAD' ) do
          begin
               OnGetText := ObtieneEntidadGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TdmCliente.SetDatosPeriodo(const Value: TDatosPeriodo);
begin
     FPeriodoInicializado := TRUE;
     FDatosPeriodo := Value;
end;

end.
