{ Invokable implementation File for TTressReporter which implements ITressReporter }

unit DTressReporterImpl;

interface

uses Types, SysUtils, Classes,StrUtils,
     {$ifndef TEST}
     InvokeRegistry, XSBuiltIns,
     DTressReporterIntf,
     {$endif}
     gtCstPDFEng,
     gtExPDFEng,
     gtPDFEng,
     DCliente,
     DXMLTools,
     DReportesXML,
     DReportesPDF,
     DValoresActivos;

type
  { TTressReporter }
  TTressReporter = class( {$ifdef TEST}TObject{$else}TInvokableClass, ITressReporter{$endif})
  private
    { Private declarations }
    FXMLTools: DXMLTools.TdmXMLTools;
    FReportesXML: TdmReportesXML;
    FReportesPDF : TdmReportesPDF;
    ///FCliente: TdmCliente;
    function CargarValoresActivos( const Valores: WideString ): Boolean;
    function ErrorAsXML(const Error: String; Excepcion: Exception): WideString;overload;
    function ErrorAsXML( const Error: String ): WideString;overload;
  protected
    { Protected declarations }
    property XMLTools: TdmXMLTools read FXMLTools;
    property ReportesXML: TdmReportesXML read FReportesXML;
    procedure ReportesXMLInit;
    procedure XMLToolsInit;
    procedure ReportesPDFInit(FStream: TStream);
  public
    { Public declarations }
    constructor Create; {$ifndef TEST}override;{$endif}
    destructor Destroy; override;
    ///property dmCliente: TdmCliente read FCliente;
    function Clasificaciones( const Empresa: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
    function Reportes( const Companys, UserName, Password: WideString; const Clasificacion: Integer ): WideString; {$ifndef TEST}stdcall;{$endif}
    function ReportAsXML( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
    function ReportAsPDF( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): TByteDynArray; {$ifndef TEST}stdcall;{$endif}
    function ReportAsPDFStream( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): TByteDynArray; {$ifndef TEST}stdcall;{$endif}
  end;

implementation

uses ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

{ Constructores / Destructores }

constructor TTressReporter.Create;
begin
     inherited Create;
     //FCliente := TdmCliente.Create( nil );
end;

destructor TTressReporter.Destroy;
begin
     //FreeAndNil( FCliente );
     if Assigned( FXMLTools ) then
     begin
          FreeAndNil( FXMLTools );
     end;
     if Assigned( FReportesXML ) then
     begin
          FreeAndNil( FReportesXML );
     end;
     inherited Destroy;
end;

{ Metodos privados }

procedure TTressReporter.XMLToolsInit;
begin
     if not Assigned( FXMLTools ) then
     begin
          FXMLTools := TdmXMLTools.Create( nil );
     end;
end;

procedure TTressReporter.ReportesXMLInit;
begin
     if not Assigned( FReportesXML ) then
     begin
          FReportesXML := TdmReportesXML.Create( nil );
     end;
end;

function TTressReporter.ErrorAsXML( const Error: String ): WideString;
begin
     Result := Format( '<ERROR>%s</ERROR>', [ Error ] );
end;

function TTressReporter.ErrorAsXML( const Error: String; Excepcion: Exception ): WideString;
begin
     Result := ErrorAsXML(  Format( '%s: %s', [ Error, Excepcion.Message ] ) );
end;

procedure TTressReporter.ReportesPDFInit( FStream: TStream );
begin
     if not Assigned( FReportesPDF ) then
     begin
          FReportesPDF := TdmReportesPDF.Create( nil );
          FReportesPDF.DefaultStream := FStream;
     end;
end;

function TTressReporter.CargarValoresActivos( const Valores: WideString ): Boolean;
var
   ValoresActivos: TValoresActivosXML;
begin
     if StrLleno( Valores ) then
     begin
          ValoresActivos := TValoresActivosXML.Create;
          try
             ValoresActivos.LoadFromXML( Valores );
             with dmCliente do
             begin
                  PeriodoActivo := ValoresActivos.Periodo;
                  Empleado := ValoresActivos.Empleado;
                  FechaDefault := ValoresActivos.Fecha;
             end;
             Result := True;
          finally
                 FreeAndNil( ValoresActivos );
          end;
     end
     else
         raise Exception.Create( 'La lista de par�metros se encuentra vac�a' );
end;

{ Interfases }

function TTressReporter.Clasificaciones( const Empresa: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
var
   eClasificacion: eClasifiReporte;
   Nodo: TZetaXMLNode;
begin
     try
        XMLToolsInit;
        with XMLTools do
        begin
             XMLInit( 'CLASIFICACIONES' );
             for eClasificacion := Low( eClasifiReporte ) to High( eClasifiReporte ) do
             begin
                  Nodo := WriteStartElement( 'CLASIFICACION' );
                  WriteAttributeInteger( 'Ord', Ord( eClasificacion ), Nodo );
                  WriteString( ZetaCommonLists.ObtieneElemento( ZetaCommonLists.lfClasifiReporte, Ord( eClasificacion ) ), Nodo );
                  WriteEndElement;
             end;
             XMLEnd;
             Result := XMLAsText;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

function TTressReporter.Reportes( const Companys, UserName, Password: WideString; const Clasificacion: Integer ): WideString; {$ifndef TEST}stdcall;{$endif}
var
   eClasificacion: eClasifiReporte;
   Nodo: TZetaXMLNode;
begin
     try
        XMLToolsInit;
        with XMLTools do
        begin
             XMLInit( 'REPORTES' );
             try
                eClasificacion := eClasifiReporte( Clasificacion );
                if Not Assigned( dmCliente ) then
                   dmCliente := TdmCliente.Create(NIL);
                try
                   with dmCliente do
                   begin
                        if UsuarioLogin( UserName, Password ) then
                        begin
                             if SetEmpresaActiva( Companys ) then
                             begin
                                  BuscarReportes( eClasificacion );
                                  with cdsLookUpReportes do
                                  begin
                                       if IsEmpty then
                                       begin
                                            XMLError( Format( 'Empresa [%s] No Tiene Reportes Para Clasificacion %s', [ Empresa, ZetaCommonLists.ObtieneElemento( lfClasifiReporte, Ord( eClasificacion ) ) ] ) );
                                       end
                                       else
                                       begin
                                            First;
                                            while not Eof do
                                            begin
                                                 Nodo := WriteStartElement( 'REPORTE' );
                                                 WriteAttributeInteger( 'Numero', FieldByName( 'RE_CODIGO' ).AsInteger, Nodo );
                                                 WriteString( FieldByName( 'RE_NOMBRE' ).AsString, Nodo );
                                                 WriteEndElement;
                                                 Next;
                                            end;
                                       end;
                                  end;
                             end
                             else
                             begin
                                  XMLError( Format( 'Empresa [%s] No Existe', [ Companys ] ) );
                             end;
                        end;
                   end;
                finally
                       FreeAndNil( dmCliente );
                end;
             except
                   on Error: Exception do
                   begin
                        XMLError( Error.Message );
                   end;
             end;
             XMLEnd;
             Result := XMLAsText;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

function TTressReporter.ReportAsXML( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): WideString; {$ifndef TEST}stdcall;{$endif}
var
   sNombre: String;
begin
     try
        XMLToolsInit;
        ReportesXMLInit;
        if CargarValoresActivos( Valores ) then
        begin
             with XMLTools do
             begin
                  XMLInit( 'DATOS' );
                  try
                     with dmCliente do
                     begin
                          if UsuarioLogin( UserName, Password ) then
                          begin
                               if SetEmpresaActiva( Companys ) then
                               begin
                                    with ReportesXML do
                                    begin
                                         XMLTools := Self.XMLTools;
                                         Resultado := dmCliente.cdsResultado;
                                         if ( Procesar( Companys, Reporte, sNombre ) > 0 ) then
                                         begin
                                              XMLBuildDataSet( Resultado, 'REPORTE' );
                                         end
                                         else
                                         begin
                                              XMLError( Format( 'Reporte %d - %s: No Hay Datos', [ Reporte, sNombre ] ) );
                                         end;
                                    end;
                               end
                               else
                               begin
                                    XMLError( Format( 'Reporte %d: Empresa "%s" no existe', [ Reporte, Companys ] ) );
                               end;
                          end
                          else
                          begin
                               XMLError( Format( 'Reporte %d: Credenciales incorrectas', [ Reporte ] ) );
                          end;
                     end;
                  except
                        on Error: Exception do
                        begin
                             XMLError( Error.Message );
                        end;
                  end;
                  XMLEnd;
                  Result := XMLAsText;
             end;
        end;
     except
           on Error: Exception do
           begin
                Result := ErrorAsXML( 'Excepci�n Encontrada', Error );
           end;
     end;
end;

function TTressReporter.ReportAsPDF( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): TByteDynArray;
const
     K_LENGTH_NULOS =5;
var
   sNombre: String;
   oStream: TMemoryStream;
   Data: Byte;
   i, iSize: Integer;

   {$ifdef FALSE}
procedure CreateNoDataPDF;
var
   gtPDFEngine: TgtPDFEngine;
begin
     oStream.Clear;
     gtPDFEngine := TgtPDFEngine.Create( nil );
     try
        with gtPDFEngine do
        begin
             with Preferences do
             begin
                  OutputToUserStream := True;
                  ShowSetupDialog := FALSE;
                  OpenAfterCreate := FALSE;
             end;
             UserStream := oStream;
             BeginDoc;
             TextOut( 2, 2, Format( 'El Reporte %d No Tiene Datos', [ Reporte ] ) );
             TextOut( 2, 3, Format( 'Reporte Generado a las %1:s de %0:s', [ FormatDateTime('dd/mmm/yyyy', Now ), FormatDateTime('hh:nn:ss', Now ) ] ) );
             EndDoc;
        end;
     finally
            FreeAndNil( gtPDFEngine );
     end;
end;
{$ELSE}
procedure CreateNoDataMsg(const sMsg: string);
 const K_ES_ERROR: array[0..4] of Byte = (1,1,2,3,5);
       K_ESPACIO = '  ';
var
   i: integer;
begin
     oStream.Clear;
     for i:=0 to LENGTH(K_ES_ERROR)-1 do
         oStream.WriteBuffer( K_ES_ERROR[i], 1 );
     for i:=1 to LENGTH(sMsg) do
         oStream.WriteBuffer( sMsg[i] , 1 );
     oStream.WriteBuffer( K_ESPACIO , 2 );
end;
{$ENDIF}

begin
     try
        XMLToolsInit;
        oStream := TMemoryStream.Create;
        try
           ReportesPDFInit( oStream );
           if CargarValoresActivos( Valores ) then
           begin
                with dmCliente do
                begin
                     if UsuarioLogin( UserName, Password ) then
                     begin
                          if SetEmpresaActiva( Companys ) then
                          begin
                               with FReportesPDF do
                               begin
                                    if ( Procesar( Companys, Reporte, sNombre ) > 0 ) then
                                    begin
                                         if ( oStream.Size <= 0 ) then
                                         begin
                                              CreateNoDataMsg( Format( 'El reporte %s no contiene informaci�n', [sNombre] ) );
                                         end;
                                    end
                                    else
                                    begin
                                         if StrLleno(sNombre) then
                                            CreateNoDataMsg( 'No hay registros que cumplan con los filtros especificados' )
                                         else
                                             CreateNoDataMsg( Format( 'Reporte %d no existe', [ Reporte ] ) );
                                    end;

                               end;
                          end
                          else
                              CreateNoDataMsg( Format( 'Reporte %d: Empresa "%s" no existe', [ Reporte, Companys ] ) );
                     end
                     else
                         CreateNoDataMsg( Format( 'Reporte %d: Usuario o Password inv�lido', [ Reporte ] ) );
                     iSize := oStream.Size;
                     if ( iSize > 0 ) then
                     begin
                          SetLength( Result, iSize );
                          oStream.Position := 0;
                          for i := 0 to ( iSize ) do
                          begin
                               oStream.ReadBuffer( Data, 1 );
                               Result[ oStream.Position ] := Data;
                               oStream.Position := i;
                          end;
                     end;

                end;
           end;
        finally
               FreeAndNil( oStream );
        end;
     except
           on Error: Exception do
           begin
                raise Exception.Create( 'Excepci�n Encontrada: ' + Error.Message );
           end;
     end;
end;


function TTressReporter.ReportAsPDFStream( const Companys, UserName, Password: WideString; const Reporte: Integer; const Valores: WideString ): TByteDynArray; {$ifndef TEST}stdcall;{$endif}
var
   mStream: TMemoryStream;
   gtPDFEngine: TgtPDFEngine;
   aData : TByteDynArray;
   Data: Byte;
   i, iSize : integer;
begin
     gtPDFEngine:= TgtPDFEngine.Create(NIL);

     with gtPDFEngine do
     begin
          mStream:= TMemoryStream.Create;
          with Preferences do
          begin
               OutputToUserStream := True;
               ShowSetupDialog := FALSE;
               OpenAfterCreate := FALSE;
          end;
          UserStream := mStream;
          BeginDoc;
          TextOut(2, 2,'HOOOOOOLA! DE PARTE DE CAROLINA <8-) ' );
          TextOut(2, 3,Format( ' Hoy es %s, y son las %s', [FormatDateTime('dd/mmm/yyyy', Date), FormatDateTime('hh:nn:ss', Now) ]) );
          EndDoc;
          {****** Hasta aqui la creacion del PDF}

          SetLength( aData, mStream.Size );
          iSize := mStream.Size;
          mStream.Position :=0;
          for i := 0 to ( iSize - 1 ) do
          begin
               mStream.ReadBuffer( Data, 1 );
               aData[mStream.Position] := Data;
               mStream.Position :=i;
          end;
          mStream.Free;

          Result := aData;
     end;
end;

{$ifndef TEST}
initialization
  { Invokable classes must be registered }
  InvRegistry.RegisterInvokableClass(TTressReporter);
{$endif}
end.





