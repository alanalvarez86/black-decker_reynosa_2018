unit FConfigurarPoliza_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBCtrls, StdCtrls, Grids, DBGrids, ComCtrls, Buttons, ExtCtrls, Registry,
     ZBaseDlgModal_DevEx, ZetaDBTextBox, DConfigPoliza, ZBaseDlgModal,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinscxPCPainter, dxBarBuiltInMenu, cxControls, cxPC,
  ImgList, cxButtons, cxContainer, cxEdit, cxProgressBar;

type
  TConfigurarPoliza_DevEx = class(TZetaDlgModal_DevEx)
    OpenCDS: TOpenDialog;
    SaveCDS: TSaveDialog;
    dsGrupo: TDataSource;
    dsCtaConceptos: TDataSource;
    dsPoliza: TDataSource;
    dsCampoRep: TDataSource;
    pbAgregar: TProgressBar;
    PageControl_DevEx: TcxPageControl;
    TabAsientos_DevEx: TcxTabSheet;
    TabConceptos_DevEx: TcxTabSheet;
    Formulas_DevEx: TcxTabSheet;
    TabGrupos_DevEx: TcxTabSheet;
    gridConceptos: TDBGrid;
    Panel2: TPanel;
    gridGrupo: TDBGrid;
    Panel4: TPanel;
    cbGrupoLBL: TLabel;
    cbGrupo: TComboBox;
    dbnGrupo: TDBNavigator;
    Panel3: TPanel;
    editCuentaLBL: TLabel;
    editCuenta: TEdit;
    gridPoliza: TDBGrid;
    gridCampoRep: TDBGrid;
    Panel5: TPanel;
    PolizaLBL: TLabel;
    Poliza: TZetaTextBox;
    btnBorrarTodos: TcxButton;
    btnAgregar: TcxButton;
    dbnPoliza: TDBNavigator;
    btnGenerar: TcxButton;
    btnImportaGrupo: TcxButton;
    btnAbreGrupo: TcxButton;
    btnExportaGrupo: TcxButton;
    btnBorraTodos: TcxButton;
    btnLlenaConceptos: TcxButton;
    btnAbreConceptos: TcxButton;
    btnExportaConceptos: TcxButton;
    dbnConceptos: TDBNavigator;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnLlenaConceptosClick(Sender: TObject);
    procedure btnAbreConceptosClick(Sender: TObject);
    procedure btnExportaConceptosClick(Sender: TObject);
    procedure btnImportaGrupoClick(Sender: TObject);
    procedure btnAbreGrupoClick(Sender: TObject);
    procedure btnExportaGrupoClick(Sender: TObject);
    procedure btnBorraTodosClick(Sender: TObject);
    procedure btnGenerarClick(Sender: TObject);
    procedure btnConsultarClick(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnBorrarTodosClick(Sender: TObject);
    procedure cbGrupoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FRegistry: TRegistry;
    FListGrupos: TStrings;
    function GetExportFileName(var sFile: String): Boolean;
    function GetGroupType: eGroupType;
    function GetImportFileName(var sFile: String): Boolean;
    procedure ConectarGrupo;
    procedure SetControls;
    procedure MoveProgressBar(Sender: TObject);
    procedure DisConnect;
  public
    { Public declarations }
    procedure Connect;
    property Grupos: TStrings read FListGrupos write FListGrupos;
  end;

var
  ConfigurarPoliza_DevEx: TConfigurarPoliza_DevEx;

implementation

uses ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo,
     FListaConceptos_DevEx,
     DReportes;

const
     K_KEY_ESTRUCTURA = 'Estructura';

{$R *.DFM}

{ ********** TFormaCreaPoliza ************ }

procedure TConfigurarPoliza_DevEx.FormCreate(Sender: TObject);
var
   sEstructura: String;
begin
     inherited;
     HelpContext := H50519_Generador_Cuentas_contables;
     FRegistry := TRegistry.Create;
     with FRegistry do
     begin
          {$ifndef TRESS_DELPHIXE5_UP}
          RootKey := HKEY_LOCAL_MACHINE;    // Al migrar las aplicaciones a Delphi XE5 se cambia a usar el registry del usuario: HKEY_CURRENT_USER que es el default
          {$endif}
          if OpenKey( 'Software\Grupo Tress\CreaPoliza', TRUE ) then
          begin
               sEstructura := ReadString( K_KEY_ESTRUCTURA );
               if ZetaCommonTools.StrLleno( sEstructura ) then
                  editCuenta.Text := sEstructura;
          end;
     end;
end;


procedure TConfigurarPoliza_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Connect;
     PageControl_DevEx.ActivePage := TabConceptos_DevEx;
end;

procedure TConfigurarPoliza_DevEx.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     inherited;
     Disconnect;
end;

procedure TConfigurarPoliza_DevEx.FormDestroy(Sender: TObject);
begin
     inherited;
     if ( FRegistry <> NIL ) then
     begin
          with FRegistry do
          begin
               WriteString( K_KEY_ESTRUCTURA, editCuenta.Text );
               CloseKey;
          end;
          FreeAndNil( FRegistry );
     end;

     FreeAndNil( ListaConceptos_DevEx );
end;

procedure TConfigurarPoliza_DevEx.DisConnect;
begin
     dmConfigPoliza.DesconectaConfiguraPoliza;
end;

procedure TConfigurarPoliza_DevEx.Connect;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if dmConfigPoliza = NIL then
           dmConfigPoliza := TdmConfigPoliza.Create( self );


        with dmConfigPoliza do
        begin
             if ConectaConfiguraPoliza then
             begin
                  Caption := Format( 'Configurar P�liza Contable # %d %s', [ PolizaActiva, PolizaNombre ] );
                  Poliza.Caption := Format( '# %d %s', [ PolizaActiva, PolizaNombre ] );

                  with cbGrupo do
                  begin
                       LlenaPolizaGrupos( Items, FListGrupos );
                       if ( Items.Count > 0 ) then
                          ItemIndex := 0;
                  end;
                  ConectarGrupo;
                  dsCtaConceptos.Dataset := cdsCtaConcepto;
                  dsPoliza.Dataset := cdsPoliza;
                  dsCampoRep.Dataset := dmReportes.cdsCampoRep;
                  SetControls;
             end
             else
                 ZetaDialogo.zError( '� Atenci�n !', 'No Hay Reportes Tipo POLIZA En Esta Base De Datos', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TConfigurarPoliza_DevEx.GetGroupType: eGroupType;
begin
     Result := eGroupType( cbGrupo.ItemIndex );
end;

procedure TConfigurarPoliza_DevEx.ConectarGrupo;
begin
     dsGrupo.Dataset := dmConfigPoliza.GetDatasetGrupo( GetGroupType );
end;

function TConfigurarPoliza_DevEx.GetImportFileName( var sFile: String ): Boolean;
begin
     Result := False;
     with OpenCDS do
     begin
          FileName := sFile;
          InitialDir := ExtractFilePath( sFile );
          if Execute then
          begin
               Result := True;
               sFile := FileName;
          end;
     end;
end;

function TConfigurarPoliza_DevEx.GetExportFileName( var sFile: String ): Boolean;
begin
     Result := False;
     with SaveCDS do
     begin
          FileName := sFile;
          //InitialDir := ExtractFilePath( sFile );
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               Result := True;
               sFile := FileName;
          end;
     end;
end;

procedure TConfigurarPoliza_DevEx.SetControls;
begin
     with dsPoliza do
     begin
          if Assigned( Dataset ) then
          begin
               btnAgregar.Enabled := True;
          end
          else
              btnAgregar.Enabled := False;
     end;
     with dsCampoRep do
     begin
          if Assigned( Dataset ) then
          begin
               btnBorrarTodos.Enabled := not Dataset.IsEmpty;
          end
          else
              btnBorrarTodos.Enabled := False;
     end;
end;

{ ******* Eventos de Botones ********** }

procedure TConfigurarPoliza_DevEx.cbGrupoClick(Sender: TObject);
begin
     inherited;
     ConectarGrupo;
end;

procedure TConfigurarPoliza_DevEx.btnLlenaConceptosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if ListaConceptos_DevEx = NIL then
           ListaConceptos_DevEx := TListaConceptos_DevEx.Create( self );
           
        with ListaConceptos_DevEx do
        begin
             ShowModal;
             if ModalResult = mrOK then
                dmConfigPoliza.LlenaConceptos( Conceptos );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza_DevEx.btnAbreConceptosClick(Sender: TObject);
var
   sFile: String;
begin
     inherited;
     sFile := 'CtaConcepto.cds';
     if GetImportFileName( sFile ) then
        dmConfigPoliza.ImportaCtaConcepto( sFile );
end;

procedure TConfigurarPoliza_DevEx.btnExportaConceptosClick(Sender: TObject);
var
   sFile: String;
begin
     inherited;
     sFile := 'CtaConcepto.cds';
     if GetExportFileName( sFile ) then
        dmConfigPoliza.ExportaCtaConcepto( sFile );
end;

procedure TConfigurarPoliza_DevEx.btnImportaGrupoClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmConfigPoliza.ImportaGrupoTress( GetGroupType );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza_DevEx.btnAbreGrupoClick(Sender: TObject);
var
   sFile: String;
   eGrupo: eGroupType;
begin
     inherited;
     eGrupo := GetGroupType;
     sFile := Format( 'CtaGrupo%d', [ Ord( eGrupo ) ] );
     if GetImportFileName( sFile ) then
        dmConfigPoliza.ImportaGrupo( eGrupo, sFile );
end;

procedure TConfigurarPoliza_DevEx.btnExportaGrupoClick(Sender: TObject);
var
   sFile: String;
   eGrupo: eGroupType;
begin
     inherited;
     eGrupo := GetGroupType;
     sFile := Format( 'CtaGrupo%d', [ Ord( eGrupo ) ] );
     if GetExportFileName( sFile ) then
        dmConfigPoliza.ExportaGrupo( eGrupo, sFile );
end;

procedure TConfigurarPoliza_DevEx.btnBorraTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm('� Atenci�n !', '� Desea Borrar Todos Estos Grupos ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmConfigPoliza.BorraGruposTodos( GetGroupType );
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TConfigurarPoliza_DevEx.btnGenerarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmConfigPoliza do
        begin
             GenerarPoliza( editCuenta.Text );
        end;
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza_DevEx.btnConsultarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        SetControls;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TConfigurarPoliza_DevEx.MoveProgressBar(Sender: TObject);
begin
     pbAgregar.StepIt;

end;

procedure TConfigurarPoliza_DevEx.btnAgregarClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     with dmConfigPoliza do
     begin
          if ZetaDialogo.zConfirm( '� Atenci�n !', Format( '� Desea Agregar Los %d Nuevos Asientos A La P�liza ?', [ CuantosAsientos ] ), 0, mbNo ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  with pbAgregar do
                  begin
                       Max := CuantosAsientos;
                       Visible := True;
                       try
                          AgregaAsientos( MoveProgressBar );
                          SetControls;
                       finally
                              Visible := False;
                       end;
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

procedure TConfigurarPoliza_DevEx.btnBorrarTodosClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm('� Atenci�n !', '� Desea Borrar Todas Las F�rmulas De Esta P�liza ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             dmConfigPoliza.BorraFormulasPoliza;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TConfigurarPoliza_DevEx.OK_DevExClick(Sender: TObject);
 var
    oCursor: TCursor;
begin
     inherited;
     if ZetaDialogo.zConfirm('� Atenci�n !', '� Desea Grabar Esta P�liza ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             ModalResult := mrOk
          finally
                 Screen.Cursor := oCursor;
          end;
     end
     else
         ModalResult := mrNone;
end;

end.

