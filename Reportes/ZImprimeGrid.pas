unit ZImprimeGrid;

interface

uses
    Windows, Messages, SysUtils, Classes,
    DBGrids, DB, Printers,
    ZetaCommonLists,
    ZetaCommonClasses,cxGridDBTableView ;

procedure ImprimirGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado ); overload;
procedure ImprimirGridParams( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo: string; oParametros: TZetaParams ); overload;
procedure ExportarGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado; const eSalida: eTipoFormato = tfXLS ); overload;
procedure ExportarGridParams( oGrid : TDBGrid; oDataSet : TDataSet; oParametros: TZetaParams; const sTitulo, sCodigo: string; const eSalida: eTipoFormato = tfXLS ); overload;

procedure ImprimirGrid( oGrid : TcxGridDBTableview; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado ); overload;
procedure ImprimirGridParams( oGrid : TcxGridDBTableview; oDataSet : TDataSet; const sTitulo, sCodigo: string; oParametros: TZetaParams ); overload;
procedure ExportarGrid( oGrid : TcxGridDBTableview; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado; const eSalida: eTipoFormato = tfXLS );  overload;
procedure ExportarGridParams( oGrid : TcxGridDBTableview; oDataSet : TDataSet; oParametros: TZetaParams; const sTitulo, sCodigo: string; const eSalida: eTipoFormato = tfXLS ); overload;

implementation

uses ZetaCommonTools,
     ZQRReporteListado,
     ZReportTools,
     ZReportToolsConsts,
     FTressShell;



function ParametroValido( oParam :TParam ): Boolean;
 //Son todos los valores activos del Sistema
const Arreglo: array[1..13] of string = ( 'RegistroPatronal', 'IMSSYear',
                                    'IMSSMes', 'IMSSTipo', 'Year',
                                    'Tipo', 'Numero', 'FechaAsistencia',
                                    'FechaDefault', 'YearDefault', 'EmpleadoActivo',
                                    'NombreUsuario', 'CodigoEmpresa' );
 var
    i: integer;
begin
     Result := TRUE;
     for i:= Low(Arreglo) to High(Arreglo) do
     begin
          if Arreglo[i] = oParam.Name then
          begin
               Result := FALSE;
               Break;
          end;
     end;
end;

function GetDescripParametros( oParametros: TZetaParams ): string;
 var
    i: integer;
    oParam: TParam;
    sValor: string;
begin
     Result := VACIO;
     for i:= 0 to ( oParametros.Count - 1 ) do
     begin
          oParam := oParametros[i];
          with oParametros do
          begin
               case ParamByName( oParam.Name ).DataType of
                    ftInteger ,ftFloat:
                    begin
                         if (ParamByName( oParam.Name ).AsFloat <> 0) then
                            sValor := ParamByName( oParam.Name ).AsString
                         else
                             sValor := VACIO;
                    end;
                    ftBoolean: sValor := BoolAsSiNo( ParamByName( oParam.Name ).AsBoolean );
                    ftDate,ftDateTime: sValor := FechaCorta( ParamByName( oParam.Name ).AsDateTime )
                    else sValor := ParamByName( oParam.Name ).AsString;
               end;
          end;
          if StrLleno( sValor ) then
             Result := ConcatString( Result, oParam.Name + '=' + sValor, CR_LF );
     end;
end;

{******Metodos que reciben TDBGrid*****}

procedure ImprimirGridGeneral( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado; eSalida: eTipoFormato; lMostrarPreview: Boolean ); overload;
 var i : integer;
     oCampos : TStringList;
     oCampo : TCampoListado;
     oDatosImpresion : TDatosImpresion;
     sCaption : String;

     function GetMascara( const sDefault  : string) : string;
     begin
          Result := oCampo.Mascara;
          if StrVacio( Result ) then Result := sDefault;
     end;

begin
     if oDataSet.IsEmpty then
        raise Exception.Create('El Grid ' + sTitulo + ' est� Vac�o' );

     oCampos := TStringList.Create;
     try
        for i := 0 to oGrid.Columns.Count - 1 do
        begin
             if (oGrid.Fields[ i ] <> NIL) AND
                (oGrid.Columns[i].Visible) then
             begin
                  oCampo := TCampoListado.Create;
                  with oCampo do
                  begin
                       PosAgente := -1;
                       Formula     := oGrid.Columns[ i ].FieldName;
                       Titulo      := oGrid.Columns[ i ].Title.Caption;
                       Ancho       := oGrid.Fields[ i ].DisplayWidth;
                       Mascara     := oGrid.Fields[ i ].EditMask;
                       Operacion  := ocNinguno;

                       if (oGrid.Fields[ i ] is TNumericField) then
                       begin
                            if oGrid.Fields[ i ].DataType in [ ftSmallint,
                                                               ftInteger,
                                                               ftWord,
                                                               ftAutoInc ] then
                            begin
                                 Ancho := iMin( Ancho, 10 );
                                 TipoCampo := tgNumero;
                                 Mascara := GetMascara( K_MASCARA_INT );
                            end
                            else
                            begin
                                 Ancho := iMin( Ancho, 15 );
                                 TipoCampo := tgFloat;
                                 Operacion := ocSuma;
                                 Mascara := GetMascara( K_MASCARA_FLOAT );
                            end;
                       end
                       else if oGrid.Fields[ i ] is TDateTimeField then
                       begin
                            TipoCampo := tgFecha;
                            { Se usa ShortDateFormat dado que esto es lo que se
                            usa en TZetaClientDataset.MaskFecha }
                            Ancho := iMin( Ancho, 11 );
                            Mascara := GetMascara( {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat );

                       end
                       else if oGrid.Fields[ i ] is TStringField then
                       begin
                            Ancho := iMin( Ancho, 50 );
                            TipoCampo := tgTexto;
                       end;
                       OPImp := Operacion;
                       TipoImp := TipoCampo;
                  end;

                  oCampos.AddObject( '', oCampo );
             end;
        end;
        try
           with oDatosImpresion do
           begin
                Tipo := eSalida;
                with Printer do
                begin
                     if Printers.Count = 0 then
                        Exception.Create('No Hay Impresoras Instaladas')
                     else
                     begin
                          PrinterIndex := -1;
                          Impresora := -1;
                     end;
                end;
                Copias := 1;
                PagInicial := 1;
                PagFinal := 999;
                //Ya no se usa la plantilla default, se
                // va a utilizar la plantilla interna
                //ver defecto 1107
                //Archivo := zReportTools.DirPlantilla+'DEFAULT.QR2';
                Separador := '';
           end;
           sCaption := 'Pantalla: ' + sTitulo;
           ZQRReporteListado.PreparaReporte;

           if (oDatosImpresion.Tipo = tfImpresora) then
           begin
                QRReporteListado.InitGrid( oDatosImpresion,
                                           sValor1, sValor2, sCaption, eValor1, eValor2 );
           end
           else
           begin
                QRReporteListado.InitExportacion( oDatosImpresion,
                                                  sValor1, sValor2, sCaption, eValor1, eValor2 );
           end;

           QRReporteListado.GeneraListado( sCaption,
                                           lMostrarPreview,
                                           FALSE,
                                           FALSE,
                                           FALSE,
                                           oDataSet,
                                           oCampos,
                                           NIL );

        finally
               ZQRReporteListado.DesPreparaReporte;
        end;
     finally
            if oCampos <> NIL then
            begin
                 for i := 0 to oCampos.Count -1 do
                     oCampos.Objects[i].Free;
                 oCampos.Free;
            end;
            TressShell.ReconectaMenu;
     end;
end;

procedure ImprimirGridParams( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo: string; oParametros: TZetaParams );
 var
    sValor: string;
begin
     sValor := GetDescripParametros(oParametros);
     ImprimirGrid( oGrid, oDataSet, sTitulo, sCodigo, sValor, VACIO, stNinguno, stNinguno );
end;

procedure ImprimirGrid( oGrid : TDBGrid; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado );
begin
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor1, sValor2 , eValor1, eValor2 ,
                          tfImpresora, TRUE );
end;

procedure ExportarGrid( oGrid : TDBGrid; oDataSet : TDataSet;
                        const sTitulo, sCodigo,sValor1, sValor2 : string;
                        const eValor1, eValor2 : TipoEstado;
                        const eSalida: eTipoFormato = tfXLS );
begin
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor1, sValor2 , eValor1, eValor2 ,eSalida, FALSE );
end;

procedure ExportarGridParams( oGrid : TDBGrid; oDataSet : TDataSet; oParametros: TZetaParams; const sTitulo, sCodigo: string; const eSalida: eTipoFormato = tfXLS );
 var
    sValor : string;
begin
     sValor := GetDescripParametros(oParametros);
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor, VACIO , stNinguno, stNinguno, eSalida, FALSE );
end;

{*****Recib grid DevEx****}

procedure ImprimirGridGeneral( oGrid : TcxGriddbtableview; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado; eSalida: eTipoFormato; lMostrarPreview: Boolean ); overload;
 var i : integer;
     oCampos : TStringList;
     oCampo : TCampoListado;
     oDatosImpresion : TDatosImpresion;
     sCaption : String;

     function GetMascara( const sDefault  : string) : string;
     begin
          Result := oCampo.Mascara;
          if StrVacio( Result ) then Result := sDefault;
     end;

begin
     if oDataSet.IsEmpty then
        raise Exception.Create('El Grid ' + sTitulo + ' est� Vac�o' );

     oCampos := TStringList.Create;
     try
        for i := 0 to oGrid.ColumnCount  - 1 do
        begin
             if (oGrid.datacontroller.DataSource.DataSet.FindField(oGrid.Columns[i].Databinding.FieldName) <> NIL) AND
                (oGrid.Columns[i].Visible) then
             begin
                  oCampo := TCampoListado.Create;
                  with oCampo do
                  begin
                       PosAgente := -1;
                       Formula     := oGrid.Columns[ i ].DataBinding.FieldName;
                       Titulo      := oGrid.Columns[ i ].Caption;
                       Ancho       := oGrid.datacontroller.DataSource.DataSet.FieldByName(oGrid.Columns[i].Databinding.FieldName).DisplayWidth;
                       Mascara     := oGrid.datacontroller.DataSource.DataSet.FieldByName(oGrid.Columns[i].Databinding.FieldName).EditMask;
                       Operacion  := ocNinguno;

                       if (oGrid.datacontroller.DataSource.DataSet.FieldByName(oGrid.Columns[i].Databinding.FieldName) is TNumericField) then
                       begin
                            if oGrid.datacontroller.DataSource.DataSet.FieldByName(oGrid.Columns[i].Databinding.FieldName).DataType in [ ftSmallint,
                                                               ftInteger,
                                                               ftWord,
                                                               ftAutoInc ] then
                            begin
                                 Ancho := iMin( Ancho, 10 );
                                 TipoCampo := tgNumero;
                                 Mascara := GetMascara( K_MASCARA_INT );
                            end
                            else
                            begin
                                 Ancho := iMin( Ancho, 15 );
                                 TipoCampo := tgFloat;
                                 Operacion := ocSuma;
                                 Mascara := GetMascara( K_MASCARA_FLOAT );
                            end;
                       end
                       else if oGrid.datacontroller.DataSource.DataSet.FieldByName(oGrid.Columns[i].Databinding.FieldName) is TDateTimeField then
                       begin
                            TipoCampo := tgFecha;
                            { Se usa ShortDateFormat dado que esto es lo que se
                            usa en TZetaClientDataset.MaskFecha }
                            Ancho := iMin( Ancho, 11 );
                            Mascara := GetMascara(  {$IFDEF TRESS_DELPHIXE5_UP}FormatSettings.{$ENDIF}ShortDateFormat );
                       end
                       else if oGrid.datacontroller.DataSource.DataSet.FieldByName(oGrid.Columns[i].Databinding.FieldName) is TStringField then
                       begin
                            Ancho := iMin( Ancho, 50 );
                            TipoCampo := tgTexto;
                       end;
                       OPImp := Operacion;
                       TipoImp := TipoCampo;
                  end;

                  oCampos.AddObject( '', oCampo );
             end;
        end;
        try
           with oDatosImpresion do
           begin
                Tipo := eSalida;
                with Printer do
                begin
                     if Printers.Count = 0 then
                        Exception.Create('No Hay Impresoras Instaladas')
                     else
                     begin
                          PrinterIndex := -1;
                          Impresora := -1;
                     end;
                end;
                Copias := 1;
                PagInicial := 1;
                PagFinal := 999;
                //Ya no se usa la plantilla default, se
                // va a utilizar la plantilla interna
                //ver defecto 1107
                //Archivo := zReportTools.DirPlantilla+'DEFAULT.QR2';
                Separador := '';
           end;
           sCaption := 'Pantalla: ' + sTitulo;
           ZQRReporteListado.PreparaReporte;

           if (oDatosImpresion.Tipo = tfImpresora) then
           begin
                QRReporteListado.InitGrid( oDatosImpresion,
                                           sValor1, sValor2, sCaption, eValor1, eValor2 );
           end
           else
           begin
                QRReporteListado.InitExportacion( oDatosImpresion,
                                                  sValor1, sValor2, sCaption, eValor1, eValor2 );
           end;

           QRReporteListado.GeneraListado( sCaption,
                                           lMostrarPreview,
                                           FALSE,
                                           FALSE,
                                           FALSE,
                                           oDataSet,
                                           oCampos,
                                           NIL );

        finally
               ZQRReporteListado.DesPreparaReporte;
        end;
     finally
            if oCampos <> NIL then
            begin
                 for i := 0 to oCampos.Count -1 do
                     oCampos.Objects[i].Free;
                 oCampos.Free;
            end;
            TressShell.ReconectaMenu;
     end;
end;

procedure ImprimirGridParams( oGrid : TcxGriddbtableview; oDataSet : TDataSet; const sTitulo, sCodigo: string; oParametros: TZetaParams );
 var
    sValor: string;
begin
     sValor := GetDescripParametros(oParametros);
     ImprimirGrid( oGrid, oDataSet, sTitulo, sCodigo, sValor, VACIO, stNinguno, stNinguno );
end;

procedure ImprimirGrid( oGrid : TcxGriddbtableview; oDataSet : TDataSet; const sTitulo, sCodigo,sValor1, sValor2 : string; const eValor1, eValor2 : TipoEstado );
begin
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor1, sValor2 , eValor1, eValor2 ,
                          tfImpresora, TRUE );
end;

procedure ExportarGrid( oGrid : TcxGriddbtableview; oDataSet : TDataSet;
                        const sTitulo, sCodigo,sValor1, sValor2 : string;
                        const eValor1, eValor2 : TipoEstado;
                        const eSalida: eTipoFormato = tfXLS );
begin
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor1, sValor2 , eValor1, eValor2 ,eSalida, FALSE );
end;

procedure ExportarGridParams( oGrid : TcxGriddbtableview; oDataSet : TDataSet; oParametros: TZetaParams; const sTitulo, sCodigo: string; const eSalida: eTipoFormato = tfXLS );
 var
    sValor : string;
begin
     sValor := GetDescripParametros(oParametros);
     ImprimirGridGeneral( oGrid, oDataSet , sTitulo, sCodigo,sValor, VACIO , stNinguno, stNinguno, eSalida, FALSE );
end;

end.
