unit FEscogeReporte_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ExtCtrls, StdCtrls, ComCtrls,
     Db, ImgList,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZReportTools,
     ZetaTipoEntidad,
     ZetaCommonLists,
     ZetaKeyCombo, ToolWin, ZBaseDlgModal_DevEx, cxGraphics,
     cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters, cxButtons, cxControls,
     cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
     cxDataStorage, cxEdit, cxNavigator, cxDBData, cxTextEdit,
     cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
     cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid;

type
  TEscogeReporte_DevEx = class(TZetaDlgModal_DevEx)
    DataSource: TDataSource;
    ListaSmallImages: TImageList;
    PanelPrincipal: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    ZetaKeyCombo1: TZetaKeyCombo;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    DBGridLevel: TcxGridLevel;
    DBGrid: TZetaCXGrid;
    Imagen_GRID: TcxGridDBColumn;
    ToolBar: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    RE_NOMBRE: TcxGridDBColumn;
    RE_FECHA: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    SmallImageList: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    //procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
    //  DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ToolButton2Click(Sender: TObject);
    procedure Imagen_GRIDCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FEntidad: TipoEntidad;
    FTipoReporte: eTipoReporte;
    {$ifdef ADUANAS}
    FListaEntidades: ArregloEntidades;
    FListaReportes: ArregloReportes;
    {$ENDIF}
    FReporteDefault: Integer;
    FImprimeForma : Boolean;
    FCampos : Variant;
    FValores : Variant;
    procedure SeleccionaReporte;
    function GetFiltroEspecial: string;
  protected
    procedure ApplyMinWidth;    //DevEx
  public
    { Public declarations }
    property Entidad: TipoEntidad read FEntidad write FEntidad;
    property TipoReporte: eTipoReporte read FTipoReporte write FTipoReporte;
    {$ifdef ADUANAS}
    property ListaEntidad: ArregloEntidades read FListaEntidades write FListaEntidades;
    property ListaReporte: ArregloReportes read FListaReportes write FListaReportes;
    {$ENDIF}
    property ReporteDefault: Integer read FReporteDefault write FReporteDefault;
    property ImprimeForma : Boolean read FImprimeForma write FImprimeForma;
    property Campos : Variant read FCampos write FCampos;
    property Valores : Variant read FValores write FValores;
  end;


  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);


var
  EscogeReporte_DevEx: TEscogeReporte_DevEx;

procedure EscogeUnReporte( const sCaption: String; const Entity: TipoEntidad; const ReportType: eTipoReporte; const iDefault: Integer );
procedure ImprimeUnaForma( const Entity: TipoEntidad;
                           const ReportType: eTipoReporte;
                           const vCampos, vValores : Variant );overload;
procedure ImprimeUnaForma( const iReporte: integer;
                           const vCampos, vValores : Variant;
                           const eSalida : TipoPantalla = tgPreview );overload;

{$ifdef ADUANAS}
procedure ImprimeUnaForma( const Entity: ArregloEntidades;
                           const ReportType: ArregloReportes;
                           const vCampos, vValores : Variant );overload;
{$ENDIF}

implementation


uses DReportes,
     DSistema,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;


{$R *.DFM}

function GetFiltroEspecialVariant(Campos, Valores: Variant) : string;
 var i : integer;
begin
     Result := '';
     for i := VarArrayLowBound(Campos,1) to VarArrayHighBound(Campos,1) do
     begin
          if Result > '' then Result := Result + ' AND ';
          Result := Result + Campos[i]+'='+Valores[i]
     end;
end;

procedure EscogeUnReporte( const sCaption: String; const Entity: TipoEntidad; const ReportType: eTipoReporte; const iDefault: Integer );
begin
     with TEscogeReporte_DevEx.Create( Application ) do
     begin
          try
             Caption := sCaption;
             Entidad := Entity;
             TipoReporte := ReportType;
             ReporteDefault := iDefault;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure ImprimeUnaForma( const Entity: TipoEntidad;
                           const ReportType: eTipoReporte;
                           const vCampos, vValores : Variant );
begin
     with TEscogeReporte_DevEx.Create( Application ) do
     begin
          try
             Caption := 'Impresión de Formas';
             Entidad := Entity;
             TipoReporte := ReportType;
             ImprimeForma := TRUE;
             Campos := vCampos;
             Valores := vValores;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;


procedure ImprimeUnaForma( const iReporte: integer;
                           const vCampos, vValores : Variant;
                           const eSalida : TipoPantalla );
begin
     dmReportes.ImprimeUnaForma( GetFiltroEspecialVariant( vCampos,vValores ), iReporte, eSalida );
end;

{$ifdef ADUANAS}
procedure ImprimeUnaForma( const Entity: ArregloEntidades;
                           const ReportType: ArregloReportes;
                           const vCampos, vValores : Variant );
begin
 with TEscogeReporte_DevEx.Create( Application ) do
     begin
          try
             Caption := 'Impresión de Formas';
             ListaEntidad := Entity;
             ListaReporte := ReportType;
             ImprimeForma := TRUE;
             Campos := vCampos;
             Valores := vValores;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;
{$ENDIF}


{ ************** TEscogeReporte ************ }

procedure TEscogeReporte_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext := H00018_Lista_reportes;
     {$ifdef ADUANAS}
     FListaEntidades := [];
     FListaReportes := [];
     {$ENDIF}
     PanelPrincipal.Visible := FALSE;
     ToolBar.Visible := FALSE;
     {$ifdef ADUANAS}
     ToolBar.Visible := TRUE;
     {$ENDIF}
end;

//(@am):Para el keypress en DBTableViews funcione
procedure TEscogeReporte_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if ( ActiveControl.parent.classname = 'TZetaCXGrid' )then
     begin
        if Key = Chr( VK_RETURN )then
        begin
             if OK_DevEx.Visible and OK_DevEx.Enabled then
             begin
                  Key := #0; //Limpiamos el Key para que no se ejecute el ENTER de la clase padre
                  OK_DevEx.Click;
             end;
        end;
     end;
     inherited;  //Se ejecuta KeyPress de la clase padre
end;

procedure TEscogeReporte_DevEx.FormShow(Sender: TObject);
 var lVacio : Boolean; 
begin
     dmReportes.TipoReporte := FTipoReporte;
     with dmReportes do
     begin
          EntidadReporte := FEntidad;
          {$ifdef ADUANAS}
          ListaEntidad := FListaEntidades;
          ListaReporte := FListaReportes;
          {$ENDIF}
          dmSistema.cdsUsuarios.Conectar;

          cdsEscogeReporte.Refrescar;
          //cdsEscogeReporte.Conectar;
          DataSource.DataSet := cdsEscogeReporte;

          lVacio := cdsEscogeReporte.IsEmpty;

          OK_DevEx.Enabled := NOT lVacio;
          DBGrid.Enabled := NOT lVacio;

          if lVacio then
             ZetaDialogo.ZError(Caption, 'No hay Reportes disponibles',0);
     end;
     ApplyMinWidth;
     ZetaDBGridDBTableView.ApplyBestFit();
     ZetaDBGridDBTableView.OptionsCustomize.ColumnHorzSizing := False;

end;

function TEscogeReporte_DevEx.GetFiltroEspecial : string;
begin
     Result := GetFiltroEspecialVariant( Campos, Valores );
end;

procedure TEscogeReporte_DevEx.SeleccionaReporte;
begin
     if FImprimeForma then
     begin
          dmReportes.ImprimeUnaForma(GetFiltroEspecial);
     end
     else dmReportes.cdsEscogeReporte.Modificar;
end;

procedure TEscogeReporte_DevEx.OK_DevExClick(Sender: TObject);
begin
     SeleccionaReporte;
end;
 {
procedure TEscogeReporte.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
     if ( DataCol = 0 ) then
        ListaSmallImages.Draw( DBGrid.Canvas, Rect.Left, Rect.Top + 1,
                               DataSource.DataSet.FieldByName('RE_TIPO').AsInteger )
end;
}

procedure TEscogeReporte_DevEx.ToolButton2Click(Sender: TObject);
begin
     with dmReportes.cdsEscogeReporte do
     begin
          with TToolButton(Sender) do
          begin
               Filtered := ( Tag >= 0 );
               Filter := Format( 'RE_TIPO = %d', [Tag] )
          end;
     end;
end;

//DevEx
procedure TEscogeReporte_DevEx.Imagen_GRIDCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;
           DrawContent;
           DrawBorders;
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, DataSource.DataSet.FieldByName('RE_TIPO').AsInteger )
        finally
               Free;
        end;
  end;
  ADone := True;
end;


procedure TEscogeReporte_DevEx.ApplyMinWidth;
var
   i: Integer;
const
     widthColumnOptions =25;
begin
     with  ZetaDBGridDBTableView do
     begin
          for i:=0 to  ColumnCount-1 do
          begin
               if ( Columns[i].Name = ('Imagen_GRID')) then
               begin
                  Columns[i].MinWidth := 20;
               end
               else
                   Columns[i].MinWidth :=  Canvas.TextWidth( Columns[i].Caption)+ widthColumnOptions;
          end;
     end;

end;


end.
