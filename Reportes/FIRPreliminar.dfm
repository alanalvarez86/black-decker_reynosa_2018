object FormaIRPreliminar: TFormaIRPreliminar
  Left = 247
  Top = 197
  Width = 525
  Height = 459
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Preliminar de Impresi�n R�pida'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 517
    Height = 41
    Align = alTop
    TabOrder = 0
    object btnSiguiente: TButton
      Left = 96
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Siguiente'
      TabOrder = 0
      OnClick = btnSiguienteClick
    end
    object btnPrimero: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Primero'
      TabOrder = 1
      OnClick = btnPrimeroClick
    end
    object btnImprimir: TButton
      Left = 256
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Imprimir'
      ModalResult = 1
      TabOrder = 2
    end
    object btnSalir: TButton
      Left = 336
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Salir'
      ModalResult = 2
      TabOrder = 3
    end
    object Panel2: TPanel
      Left = 444
      Top = 1
      Width = 72
      Height = 39
      Align = alRight
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = 'P�gina:'
      TabOrder = 4
      object lblPagina: TLabel
        Tag = 1
        Left = 41
        Top = 13
        Width = 8
        Height = 13
        Caption = '1'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object btnUltimo: TButton
      Left = 176
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Ultimo'
      TabOrder = 5
      OnClick = btnUltimoClick
    end
  end
  object ScrollBox1: TScrollBox
    Left = 0
    Top = 41
    Width = 517
    Height = 387
    Align = alClient
    TabOrder = 1
    object MemoPreliminar: TMemo
      Left = 0
      Top = 0
      Width = 63
      Height = 285
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Lines.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12'
        '13'
        '14'
        '15'
        '16'
        '17'
        '18'
        '19'
        '20')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      WordWrap = False
    end
  end
end
