library TressExcel;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}


uses
  dxGDIPlusClasses,
  MidasLib,
  ComServ,
  TressExcel_TLB in 'TressExcel_TLB.pas',
  FTress2Excel in 'FTress2Excel.pas' {Tress2Excel: CoClass},
  DBasicoCliente in '..\..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule},
  DReportesGenerador in '..\..\DataModules\DReportesGenerador.pas' {dmReportGenerator: TDataModule},
  DBaseDiccionario in '..\..\DataModules\DBaseDiccionario.pas' {dmBaseDiccionario: TDataModule},
  DDiccionario in 'DDiccionario.pas' {dmDiccionario: TDataModule},
  ZBaseDlgModal in '..\..\tools\ZBaseDlgModal.pas' {ZetaDlgModal};

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}

begin
end.
