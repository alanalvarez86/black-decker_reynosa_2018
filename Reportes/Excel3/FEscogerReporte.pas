unit FEscogerReporte;
{$INCLUDE DEFINES.INC}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ZetaKeyCombo, Db, Grids, DBGrids,
     ZetaMessages,
     ZetaCommonLists,
     ZBaseDlgModal,
     ZetaDBGrid;

type
  TEscogerReporte = class(TZetaDlgModal)
    ZetaDBGrid: TZetaDBGrid;
    DataSource: TDataSource;
    PanelSuperior: TPanel;
    ClasificacionLBL: TLabel;
    Clasificacion: TZetaKeyCombo;
    lbBusca: TLabel;
    EBuscaNombre: TEdit;
    BBusca: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ClasificacionClick(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
  private
    { Private declarations }
    procedure Conectar;
    procedure FiltraReporte;
    procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
    function OKPressed: Boolean;
    function GetReporteNumero: Integer;
    function GetReporteNombre: String;
  end;

var
  EscogerReporte: TEscogerReporte;

implementation

uses DCliente,
     DDiccionario;

{$R *.DFM}

procedure TEscogerReporte.FormCreate(Sender: TObject);
begin
     inherited;

     Caption := Format( 'Escoja El %s Deseado', [  ZetaCommonLists.ObtieneElemento( lfTipoReporte, Ord( trListado ) ) ] );
end;

procedure TEscogerReporte.FormShow(Sender: TObject);
begin
     inherited;
     {$ifdef RDD}
     dmDiccionario.GetListaClasifi( Clasificacion.Items );
     {$endif}
     Clasificacion.ItemIndex := 0;
     Conectar;
end;

procedure TEscogerReporte.Conectar;
var
   oCursor: TCursor;
   iClasificacion: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmCliente do
        begin
             with cdsLookUpReportes do
             begin
                  DisableControls;
                  try
                     {$ifdef RDD}
                     iClasificacion:=  Integer( Clasificacion.Items.Objects[ Clasificacion.Valor ] );
                     {$else}
                     iClasificacion:= Clasificacion.Valor;
                     {$endif}
                     BuscarReportes( eClasifiReporte( iClasificacion ) );
                     FiltraReporte;
                  finally
                         EnableControls;
                  end;
             end;
             Self.Datasource.Dataset := cdsLookUpReportes;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEscogerReporte.FiltraReporte;
const
     K_SOLO_LISTADOS = '(RE_TIPO=%d)';
begin
     with dmCliente.cdsLookUpReportes do
     begin
          DisableControls;
          try
             if BBusca.Down then
             begin
                  Filtered := False;
                  Filter := Format( '%s and ( UPPER(RE_NOMBRE) like ''%s'' )', [ Format( K_SOLO_LISTADOS, [ Ord( trListado ) ] ), Format( '%0:s%1:s%0:s', [ '%', UpperCase( EBuscaNombre.Text ) ] ) ] );
                  Filtered := True;
             end
             else
             begin
                  Filtered := False;
                  Filter := Format( K_SOLO_LISTADOS, [ Ord( trListado ) ] );
                  Filtered := True;
             end;
          finally
                 EnableControls;
          end;
     end;
     ZetaDBGrid.SelectedRows.Clear;
end;

function TEscogerReporte.OKPressed: Boolean;
begin
     Result := ( ModalResult = mrOk );
end;

function TEscogerReporte.GetReporteNombre: String;
begin
     Result := dmCliente.GetReporteNombre;
end;

function TEscogerReporte.GetReporteNumero: Integer;
begin
     Result := dmCliente.GetReporteNumero;
end;

procedure TEscogerReporte.ClasificacionClick(Sender: TObject);
begin
     inherited;
     Conectar;
end;

procedure TEscogerReporte.BBuscaClick(Sender: TObject);
begin
     inherited;
     FiltraReporte;
end;

procedure TEscogerReporte.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     FiltraReporte;
end;

procedure TEscogerReporte.WMExaminar(var Message: TMessage);
begin
     OK.Click;
end;

end.
