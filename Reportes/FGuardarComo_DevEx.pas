unit FGuardarComo_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Menus, ComCtrls, ExtCtrls, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, 
   TressMorado2013, dxSkinsDefaultPainters,
   cxButtons;

type
  TGuardarComo_DevEx = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    lbNombre: TLabel;
    sNombre: TEdit;
    Ok: TcxButton;
    Cancel: TcxButton;
    procedure FormShow(Sender: TObject);
  private
    function GetNombre: String;
    procedure SetNombre( const sNombreRep: String );
  public
    property Nombre: String read GetNombre write SetNombre;
  end;

var GuardarComo_DevEx: TGuardarComo_DevEx;

function DialogoGuardar( var  sNombre : string ) : Boolean;

implementation

{$R *.DFM}

function DialogoGuardar( var sNombre : string ) : Boolean;
begin
     if GuardarComo_DevEx = NIL then GuardarComo_DevEx := TGuardarComo_DevEx.Create( Application.MainForm );
        GuardarComo_DevEx.Nombre := sNombre;
     with GuardarComo_DevEx do
     begin
          ShowModal;
          Result := ( ModalResult = mrOk ) AND ( length( Nombre ) > 0 );
     end;
     if Result then sNombre := GuardarComo_DevEx.Nombre;

end;

function TGuardarComo_DevEx.GetNombre : String;
begin
     Result:= sNombre.Text;
end;

procedure TGuardarComo_DevEx.SetNombre( const sNombreRep : String );
begin
     sNombre.Text := sNombreRep;
end;

procedure TGuardarComo_DevEx.FormShow(Sender: TObject);
begin
     sNombre.SetFocus;
end;

end.
