inherited TressShell: TTressShell
  Left = 293
  Top = 236
  Height = 303
  Caption = 'Generador De P�lizas Contables'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 250
  end
  object PolizasGB: TGroupBox [1]
    Left = 0
    Top = 0
    Width = 449
    Height = 214
    Align = alClient
    Caption = ' P�lizas Contables Existentes '
    TabOrder = 1
    object ZetaDBGrid: TZetaDBGrid
      Left = 2
      Top = 15
      Width = 445
      Height = 197
      Align = alClient
      DataSource = dsPolizas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = ZetaDBGridDblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'RE_CODIGO'
          Title.Caption = '#'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RE_NOMBRE'
          Title.Caption = 'Nombre'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RE_TIPO'
          Title.Caption = 'Tipo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RE_FECHA'
          Title.Caption = 'Fecha'
          Visible = True
        end>
    end
  end
  object PanelBotones: TPanel [2]
    Left = 0
    Top = 214
    Width = 449
    Height = 36
    Align = alBottom
    TabOrder = 2
    object Salir: TBitBtn
      Left = 371
      Top = 5
      Width = 75
      Height = 25
      Hint = 'Salir Del Programa'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = SalirClick
      Kind = bkClose
    end
    object Editar: TBitBtn
      Left = 5
      Top = 4
      Width = 102
      Height = 25
      Hint = 'Editar Esta P�liza Contable'
      Caption = 'Editar P�liza'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = EditarClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555500005577777777777777777500005000000000000000007500005088
        80FFFFFF0FFFF0750000508180F4444F0F44F0750000508880FFFFFF0FFFF075
        0000508180F4444F0F44F0750000508880FFFFFF0FFFF0750000508180F4444F
        0F44F0750000508880FF0078088880750000508180F400007844807500005088
        80FF7008007880750000508180F4408FF80080750000508880FFF70FFF800075
        0000500000000008FF803007000050EEEEEEEE70880B43000000500000000000
        00FBB43000005555555555550BFFBB43000055555555555550BFFBB400005555
        55555555550BFFBB0000}
    end
  end
  object dsPolizas: TDataSource
    Left = 120
    Top = 88
  end
end
