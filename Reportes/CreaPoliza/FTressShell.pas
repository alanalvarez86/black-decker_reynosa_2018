unit FTressShell;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Buttons,
     Forms, Dialogs, ExtCtrls, ComCtrls, Db, Grids, DBGrids, StdCtrls,
     ZBaseShell,
     ZetaDBGrid;

type
  TTressShell = class(TBaseShell)
    PolizasGB: TGroupBox;
    PanelBotones: TPanel;
    ZetaDBGrid: TZetaDBGrid;
    dsPolizas: TDataSource;
    Salir: TBitBtn;
    Editar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure EditarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
  private
    { Private declarations }
    procedure DoEditPoliza;
    procedure SetControls(const lEnabled: Boolean);
  protected
    { Protected declarations }
    procedure DoOpenAll; override;
  public
    { Public declarations }
  end;

var
  TressShell: TTressShell;

implementation

uses ZetaDialogo,
     {$ifdef DOS_CAPAS}
     DZetaServerProvider,
     ZetaSQLBroker,
     {$endif}
     DCliente,
     FConfigurarPoliza;

{$R *.DFM}

procedure TTressShell.FormCreate(Sender: TObject);
begin
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( Self );
     inherited;
end;

procedure TTressShell.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( dmCliente );
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
end;

procedure TTressShell.DoOpenAll;
begin
     inherited DoOpenAll;
     with dmCliente do
     begin
          if GetPolizas then
          begin
               dsPolizas.Dataset := cdsReportes;
               SetControls( True );
          end
          else
          begin
               SetControls( False );
               ZetaDialogo.zError( '� Atenci�n !', 'No Hay P�lizas En Esta Empresa', 0 );
          end;
     end;
end;

procedure TTressShell.SetControls(const lEnabled: Boolean);
begin
     ZetaDBGrid.Enabled := lEnabled;
     Editar.Enabled := lEnabled;
end;

procedure TTressShell.DoEditPoliza;
begin
     if dmCliente.EsPoliza then
     begin
          ConfigurarPoliza := TConfigurarPoliza.Create( Self );
          try
             with ConfigurarPoliza do
             begin
                  Connect;
                  ShowModal;
             end;
          finally
                 FreeAndNil( ConfigurarPoliza );
          end;
     end
     else
         ZetaDialogo.zError( '� Atenci�n !', 'Escoja Un Reporte Tipo P�liza', 0 );
end;

procedure TTressShell.ZetaDBGridDblClick(Sender: TObject);
begin
     inherited;
     DoEditPoliza;
end;

procedure TTressShell.EditarClick(Sender: TObject);
begin
     inherited;
     DoEditPoliza;
end;

procedure TTressShell.SalirClick(Sender: TObject);
begin
     inherited;
     Close;
end;

end.
