unit FListaConceptos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, CheckLst, Buttons, ExtCtrls,
  ZReportTools, ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
   TressMorado2013, dxSkinsDefaultPainters,
   ImgList, cxButtons, cxControls, cxContainer, cxEdit,
  cxCheckListBox;

type
  TListaConceptos_DevEx = class(TZetaDlgModal_DevEx)
    cbListaConceptos: TcxCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    function GetTiposConceptos: TiposConcepto;
    { Private declarations }
  public
    { Public declarations }
    property Conceptos : TiposConcepto read GetTiposConceptos;
  end;

var
  ListaConceptos_DevEx: TListaConceptos_DevEx;

implementation

uses
ZetaCommonLists;

{$R *.DFM}

procedure TListaConceptos_DevEx.FormCreate(Sender: TObject);
var
Cadena:Tstrings;
i:integer;
begin
     inherited;
     Cadena :=TstringList.Create;
     Cadena.Clear;
     cbListaConceptos.Items.Clear;
     ZetaCommonLists.LlenaLista( lfTipoConcepto,  cbListaConceptos.Items.CheckListBox.Items );
     for i:=0 to cbListaConceptos.Items.CheckListBox.items.Count-1 do
     begin
     Cadena.Add(CbListaConceptos.Items.CheckListBox.Items[i]);
     end;
     cbListaConceptos.Items.CheckListBox.items.Clear;
     for i:=0 to Cadena.Count-1 do
     begin
     cbListaConceptos.Items.Add.Text:=Cadena[i];
     end;
end;

procedure TListaConceptos_DevEx.FormShow(Sender: TObject);
 var
    i : integer;
begin
     inherited;
     for i:= 0 to Ord( High(eTipoConcepto) ) do
     begin
          cbListaConceptos.items[i].Checked := ( i in [Ord(coPercepcion), Ord(coDeduccion)] );
     end;
end;

function TListaConceptos_DevEx.GetTiposConceptos: TiposConcepto;
 var
    i : integer;
begin
     Result := [];
     for i:= 0 to Ord( High(eTipoConcepto) ) do
     begin
          if cbListaConceptos.Items[i].Checked then
             Result := Result + [eTipoConcepto( i )];
     end;
end;

end.
