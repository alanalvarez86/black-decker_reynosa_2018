unit FPdfDlg_devEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, Buttons, Mask, ZetaNumero,
  ZetaKeyCombo, ZDocumentDlg_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters,
   ImgList, cxButtons, dxSkinscxPCPainter, dxBarBuiltInMenu,
  cxControls, cxPC;

type
  TPdfDlg_DevEx = class(TDocumentDlg_DevEx)
    GroupBox1: TGroupBox;
    cbActiveHyperLinks: TCheckBox;
    rgTrueType: TRadioGroup;
    lblFontEncoding: TLabel;
    cbEncoding: TZetaKeyCombo;
    tsAvanzado: TcxTabSheet;
    gbUseCompression: TGroupBox;
    lblCompressionLevel: TLabel;
    cbCompressionLevel: TZetaKeyCombo;
    chkCompressDocument: TCheckBox;
    gbEncryption: TGroupBox;
    lblOwnerPassword: TLabel;
    lblConfirmOwnerPassword: TLabel;
    lblUserPassword: TLabel;
    lblConfirmUserPassword: TLabel;
    lblEncryptionLevel: TLabel;
    edOwnerPassword: TEdit;
    edConfirmOwnerPassword: TEdit;
    gbUserPermissions: TGroupBox;
    chkPrint: TCheckBox;
    chkModify: TCheckBox;
    chkCopy: TCheckBox;
    chkAnnotation: TCheckBox;
    chkFormFill: TCheckBox;
    chkAccessibility: TCheckBox;
    chkDocumentAssembly: TCheckBox;
    chkHighResolutionPrint: TCheckBox;
    edUserPassword: TEdit;
    edConfirmUserPassword: TEdit;
    cbEncryptionLevel: TZetaKeyCombo;
    cbEnableEncryption: TCheckBox;
    tsUsuario: TcxTabSheet;
    gbPresentationMode: TGroupBox;
    lblPageTransitionEffect: TLabel;
    lblPageTransitionDuration: TLabel;
    lblSecs: TLabel;
    edPageTransitionDuration: TZetaNumero;
    cbPageTransitionEffect: TZetaKeyCombo;
    gbHideUIElements: TGroupBox;
    chkMenuBar: TCheckBox;
    chkToolBar: TCheckBox;
    chkNavigationControls: TCheckBox;
    GroupBox2: TGroupBox;
    lblPageLayout: TLabel;
    lblPageMode: TLabel;
    cbPageLayout: TZetaKeyCombo;
    cbPageMode: TZetaKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure cbEnableEncryptionClick(Sender: TObject);
    procedure edConfirmOwnerPasswordExit(Sender: TObject);
    procedure edConfirmUserPasswordExit(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;
  public
    { Public declarations }
  end;

var
  PdfDlg_DevEx: TPdfDlg_DevEx;

implementation
uses
    DExportEngine,
    ZetaDialogo,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists;

{$R *.DFM}

{ TDocumentDlg1 }


procedure TPdfDlg_DevEx.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          cbActiveHyperLinks.Checked := LeeLogico( K_ActiveLinks, TRUE );
          rgTrueType.ItemIndex := LeeEntero( K_TrueTypeFont, 0 );
          chkCompressDocument.Checked := LeeLogico( K_CompressionEnabled, TRUE );
          cbCompressionLevel.ItemIndex := LeeEntero( K_CompressionLevel, Ord(clMaxCompress) );

          cbEnableEncryption.Checked := LeeLogico( K_EncryptionEnabled, FALSE );
          cbEncryptionLevel.ItemIndex := LeeEntero( K_EncryptionLevel, Ord(el40bit) );
          edOwnerPassword.Text := {$ifdef TRESS_DELPHIXE5_UP}String({$endif} Decrypt( LeeTexto( K_EncryptionOwner, VACIO ) ) {$ifdef TRESS_DELPHIXE5_UP}){$endif};
          edConfirmOwnerPassword.Text := edOwnerPassword.Text;
          edUserPassword.Text := {$ifdef TRESS_DELPHIXE5_UP}String({$endif} Decrypt( LeeTexto( K_EncryptionUser, VACIO ) ) {$ifdef TRESS_DELPHIXE5_UP}){$endif};
          edConfirmUserPassword.Text := edUserPassword.Text;

          chkPrint.Checked := LeeLogico( K_UserPermissionPrint, TRUE );
          chkModify.Checked := LeeLogico( K_UserPermissionModify, TRUE );
          chkCopy.Checked := LeeLogico( K_UserPermissionCopy, TRUE  );
          chkAnnotation.Checked := LeeLogico( K_UserPermissionAnnotation  );
          chkFormFill.Checked := LeeLogico( K_UserPermissionFormFill  );
          chkAccessibility.Checked := LeeLogico( K_UserPermissionAccessibility  );
          chkDocumentAssembly.Checked := LeeLogico( K_UserPermissionDocumentAssembly  );
          chkHighResolutionPrint.Checked := LeeLogico( K_UserPermissionHighResolutionPrint  );

          cbPageLayout.ItemIndex := LeeEntero( K_PageLayout );
          cbPageMode.ItemIndex := LeeEntero( K_PageMode );
          chkMenuBar.Checked := LeeLogico( K_MenuBar );
          chkToolBar.Checked := LeeLogico( K_ToolBar  );
          chkNavigationControls.Checked := LeeLogico( K_NavigationControls );
          edPageTransitionDuration.Valor := LeeEntero( K_PageTransitionDuration );
          cbPageTransitionEffect.ItemIndex := LeeEntero( K_PageTransitionEffect );
          cbEncoding.ItemIndex := LeeEntero(  K_EncodingFont );
     end;
     MostrarControles( cbEnableEncryption.Checked, gbEncryption );

end;

procedure TPdfDlg_DevEx.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeLogico( K_ActiveLinks, cbActiveHyperLinks.Checked );
          EscribeEntero( K_TrueTypeFont, rgTrueType.ItemIndex );
          EscribeLogico( K_CompressionEnabled, chkCompressDocument.Checked );
          EscribeEntero( K_CompressionLevel, cbCompressionLevel.ItemIndex );

          EscribeLogico( K_EncryptionEnabled, cbEnableEncryption.Checked );
          EscribeEntero( K_EncryptionLevel, cbEncryptionLevel.ItemIndex );
          EscribeTexto( K_EncryptionOwner, Encrypt( edOwnerPassword.Text ) );
          EscribeTexto( K_EncryptionUser, Encrypt( edUserPassword.Text ) );

          EscribeLogico( K_UserPermissionPrint, chkPrint.Checked );
          EscribeLogico( K_UserPermissionModify, chkModify.Checked );
          EscribeLogico( K_UserPermissionCopy, chkCopy.Checked  );
          EscribeLogico( K_UserPermissionAnnotation, chkAnnotation.Checked  );
          EscribeLogico( K_UserPermissionFormFill, chkFormFill.Checked  );
          EscribeLogico( K_UserPermissionAccessibility, chkAccessibility.Checked  );
          EscribeLogico( K_UserPermissionDocumentAssembly, chkDocumentAssembly.Checked );
          EscribeLogico( K_UserPermissionHighResolutionPrint, chkHighResolutionPrint.Checked );

          EscribeEntero( K_PageLayout, cbPageLayout.ItemIndex );
          EscribeEntero( K_PageMode, cbPageMode.ItemIndex );
          EscribeLogico( K_MenuBar, chkMenuBar.Checked );
          EscribeLogico( K_ToolBar, chkToolBar.Checked   );
          EscribeLogico( K_NavigationControls, chkNavigationControls.Checked );
          EscribeEntero( K_PageTransitionDuration, edPageTransitionDuration.ValorEntero );
          EscribeEntero( K_PageTransitionEffect, cbPageTransitionEffect.ItemIndex  );
          EscribeEntero(  K_EncodingFont, cbEncoding.ItemIndex );
     end;
     inherited;
end;

procedure TPdfDlg_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     tsPreferencias.TabVisible := TRUE;
end;

procedure TPdfDlg_DevEx.cbEnableEncryptionClick(Sender: TObject);
begin
     inherited;
     MostrarControles( cbEnableEncryption.Checked, gbEncryption );
end;

procedure TPdfDlg_DevEx.edConfirmOwnerPasswordExit(Sender: TObject);
begin
     inherited;
     if ( edOwnerPassword.Text <>  edConfirmOwnerPassword.Text ) then
     begin
          edConfirmOwnerPassword.SetFocus;
          ZError( Caption, 'La Clave del Due�o y su Confirmaci�n no Son Iguales', 0);
     end;

end;

procedure TPdfDlg_DevEx.edConfirmUserPasswordExit(Sender: TObject);
begin
     inherited;
     if ( edUserPassword.Text <>  edConfirmUserPassword.Text ) then
     begin
          edConfirmUserPassword.SetFocus;
          ZError( Caption, 'La Clave del Usuario y su Confirmaci�n no Son Iguales', 0);
     end;
end;

end.
