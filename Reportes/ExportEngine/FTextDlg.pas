unit FTextDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZDocumentDlg, StdCtrls, ComCtrls, Buttons, ExtCtrls;

type
  TTextDlg = class(TDocumentDlg)
    GroupBox1: TGroupBox;
    cbSeparador: TCheckBox;
    cbBreak: TCheckBox;
    cbUnDocumento: TCheckBox;
  private
    { Private declarations }
  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;
  public
    { Public declarations }
  end;

var
  TextDlg: TTextDlg;

implementation

{$R *.DFM}

{ TTextDlg }

procedure TTextDlg.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          cbSeparador.Checked    := LeeLogico( K_PageEndLines, TRUE );
          cbBreak.Checked        := LeeLogico( K_InsertPageBreaks, TRUE );
          cbUnDocumento.Checked  := LeeLogico( K_SingleFile, TRUE );
     end;
end;

procedure TTextDlg.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeLogico( K_PageEndLines, cbSeparador.Checked );
          EscribeLogico( K_InsertPageBreaks, cbBreak.Checked );
          EscribeLogico( K_SingleFile, cbUnDocumento.Checked );
     end;
     inherited;
end;

end.
