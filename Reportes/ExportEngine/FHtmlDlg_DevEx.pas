unit FHtmlDlg_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  {$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
  ZDocumentDlg, StdCtrls, ComCtrls, Buttons, ExtCtrls, ExtDlgs,
  ZetaCommonLists, ZetaKeyCombo, ZDocumentDlg_DevEx, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,  ImgList, cxButtons,
  dxSkinscxPCPainter, dxBarBuiltInMenu, cxControls, cxPC;

type
  THtmlDlg_DevEx = class(TDocumentDlg_DevEx)
    tsNavegador: TTabSheet;
    cbUnaPagina: TCheckBox;
    ColorDialog: TColorDialog;
    FontDialog: TFontDialog;
    GroupBox2: TGroupBox;
    cbLigasActivas: TCheckBox;
    cbSeparador: TCheckBox;
    cbOptimizar: TCheckBox;
    OpenPictureDialog: TOpenPictureDialog;
    gbMostrarNavegador: TGroupBox;
    GroupBox3: TGroupBox;
    shBackgroundColor: TShape;
    shpHoverBackColor: TShape;
    shpHoverForeColor: TShape;
    lblNavigatorBackgroundColor: TLabel;
    lblHoverBackColor: TLabel;
    lblHoverForeColor: TLabel;
    GroupBox4: TGroupBox;
    lblNavigatorType: TLabel;
    lblNavigatorOrientation: TLabel;
    lblNavigatorPosition: TLabel;
    cbNavigatorType: TZetaKeyCombo;
    cbNavigatorOrientation: TZetaKeyCombo;
    cbNavigatorPosition: TZetaKeyCombo;
    gbUseLinks: TGroupBox;
    pcShowNavigator: TPageControl;
    tsUseTextLinks: TTabSheet;
    lblFirst: TLabel;
    lblNext: TLabel;
    lblPrevious: TLabel;
    lblLast: TLabel;
    lblLinkCaptions: TLabel;
    ePrimerTexto: TEdit;
    eAnteriorTexto: TEdit;
    eSiguienteTexto: TEdit;
    eUltimoTexto: TEdit;
    tsUseGraphicLinks: TTabSheet;
    lblUseGraphicLinksFirst: TLabel;
    lblUseGraphicLinksNext: TLabel;
    lblUseGraphicLinksPrevious: TLabel;
    lblUseGraphicLinksLast: TLabel;
    lblImageSource: TLabel;
    ePrimeroImagen: TEdit;
    eAnteriorImagen: TEdit;
    eUltimoImagen: TEdit;
    eSiguienteImagen: TEdit;
    rbtnUseTextLinks: TRadioButton;
    rbtnUseGraphicLinks: TRadioButton;
    cbMostrarNavegador: TCheckBox;
    btnSetFont: TcxButton;
    bPrimero_DevEx: TcxButton;
    bSiguiente_DevEx: TcxButton;
    bAnterior_DevEx: TcxButton;
    bUltimo_DevEx: TcxButton;
    procedure shBackgroundColorMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnSetFontClick(Sender: TObject);
    procedure bPrimeroClick(Sender: TObject);
    procedure bSiguienteClick(Sender: TObject);
    procedure bAnteriorClick(Sender: TObject);
    procedure bUltimoClick(Sender: TObject);
    procedure cbUnaPaginaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cbMostrarNavegadorClick(Sender: TObject);
    procedure rbtnUseTextLinksClick(Sender: TObject);
    procedure rbtnUseGraphicLinksClick(Sender: TObject);
  private
    { Private declarations }
    FFuente : TFont;
    procedure DialogoImagen(oEdit: TEdit);

  protected
    procedure CargaArchivo; override;
    procedure GrabaArchivo; override;
  public
    { Public declarations }
  end;

var
  HtmlDlg_DevEx: THtmlDlg_DevEx;

implementation
uses
    ZetaCommonClasses;

{$R *.DFM}
procedure THtmlDlg_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     FFuente := TFont.Create;
     tsPreferencias.TabVisible := TRUE;
     cbUnaPagina.Checked := TRUE;
     cbMostrarNavegador.Checked := TRUE;
     cbMostrarNavegador.Enabled := FALSE;
     cbUnaPagina.OnClick( cbUnaPagina );
     pcShowNavigator.ActivePage := tsUseTextLinks;
end;

procedure THtmlDlg_DevEx.shBackgroundColorMouseDown( Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     inherited;
     with ColorDialog do
     begin
          Color := TShape( Sender ).Brush.Color;
          if Execute then
             TShape( Sender ).Brush.Color := Color;
     end;
end;

procedure THtmlDlg_DevEx.btnSetFontClick(Sender: TObject);
begin
     inherited;
     with FontDialog do
     begin
          Font := FFuente;
          if Execute then
          begin
               FFuente := Font;
               ePrimerTexto.Font := FFuente;
               eSiguienteTexto.Font := FFuente;
               eAnteriorTexto.Font := FFuente;
               eUltimoTexto.Font := FFuente;
          end;
     end;
end;

procedure THtmlDlg_DevEx.CargaArchivo;
begin
     inherited;
     with FIniFile do
     begin
          cbLigasActivas.Checked     := LeeLogico( K_ActiveLinks  , TRUE );
          cbSeparador.Checked        := LeeLogico( K_PageEndLines , TRUE );
          cbOptimizar.Checked        := LeeLogico( K_OptimizeforIE, TRUE );
          cbUnaPagina.Checked        := LeeLogico( K_SingleFile   , TRUE );
          cbMostrarNavegador.Checked := LeeLogico( K_ShowNavigator, TRUE );
          cbUnaPagina.OnClick( cbUnaPagina );
          
          shBackgroundColor.Brush.Color := LeeEntero( K_ColorBackGround, clWhite );
          shpHoverBackColor.Brush.Color := LeeEntero( K_ColorBackGroundHover, clBlue );
          shpHoverForeColor.Brush.Color := LeeEntero( K_ColorForeGroundHover, clWhite );
          cbNavigatorType.ItemIndex        := LeeEntero( K_NavigatorType, 0 );
          cbNavigatorOrientation.ItemIndex := LeeEntero( K_NavigatorOrientation, 0 );
          cbNavigatorPosition.ItemIndex    := LeeEntero( K_NavigatorPosition, 0 );
          rbtnUseTextLinks.Checked    := LeeLogico( K_NavLinksUseText, TRUE );
          rbtnUseGraphicLinks.Checked := LeeLogico( K_NavLinksUseGraphic, FALSE );

          ePrimerTexto.Text     := LeeTexto( K_NavLinksFirstText, K_Primero );
          eAnteriorTexto.Text   := LeeTexto( K_NavLinksPriorText, K_Anterior  );
          eSiguienteTexto.Text  := LeeTexto( K_NavLinksNextText,  K_Siguiente );
          eUltimoTexto.Text     := LeeTexto( K_NavLinksLastText,  K_Ultimo   );

          ePrimeroImagen.Text   := LeeTexto( K_NavLinksFirstGraphic, VACIO );
          eAnteriorImagen.Text  := LeeTexto( K_NavLinksPriorGraphic, VACIO );
          eUltimoImagen.Text    := LeeTexto( K_NavLinksLastGraphic, VACIO );
          eSiguienteImagen.Text := LeeTexto( K_NavLinksNextGraphic, VACIO );
          
          with FFuente do
          begin
               Name := LeeTexto( K_FontName, K_FontNameDefault );
               Color := LeeEntero( K_FontColor, clNavy );
               Size := LeeEntero( K_FontSize, K_FontSizeNumber );
               Style := [];
               if LeeLogico( K_FontBold, FALSE ) then
                  Style := Style + [fsBold];
               if LeeLogico( K_FontItalic, FALSE ) then
                  Style := Style + [fsItalic];
               if LeeLogico( K_FontUnderline, FALSE ) then
                  Style := Style + [fsUnderline];
               if LeeLogico( K_FontStrikeOut, FALSE ) then
                  Style := Style + [fsStrikeOut];
          end;
     end;
     ePrimerTexto.Font := FFuente;
     eAnteriorTexto.Font := FFuente;
     eSiguienteTexto.Font := FFuente;
     eUltimoTexto.Font := FFuente;

end;

procedure THtmlDlg_DevEx.GrabaArchivo;
begin
     with FIniFile do
     begin
          EscribeLogico( K_ActiveLinks, cbLigasActivas.Checked );
          EscribeLogico( K_PageEndLines, cbSeparador.Checked );
          EscribeLogico( K_OptimizeforIE, cbOptimizar.Checked );
          EscribeLogico( K_SingleFile, cbUnaPagina.Checked );
          EscribeLogico( K_ShowNavigator, cbMostrarNavegador.Enabled  );
          EscribeEntero( K_ColorBackGround, shBackgroundColor.Brush.Color );
          EscribeEntero( K_ColorBackGroundHover, shpHoverBackColor.Brush.Color );
          EscribeEntero( K_ColorForeGroundHover, shpHoverForeColor.Brush.Color );
          EscribeEntero( K_NavigatorType, cbNavigatorType.ItemIndex );
          EscribeEntero( K_NavigatorOrientation, cbNavigatorOrientation.ItemIndex );
          EscribeEntero( K_NavigatorPosition, cbNavigatorPosition.ItemIndex );
          EscribeLogico( K_NavLinksUseText, rbtnUseTextLinks.Checked  );
          EscribeLogico( K_NavLinksUseGraphic, rbtnUseGraphicLinks.Checked );

          EscribeTexto( K_NavLinksFirstText, ePrimerTexto.Text );
          EscribeTexto( K_NavLinksPriorText, eAnteriorTexto.Text );
          EscribeTexto( K_NavLinksNextText,  eSiguienteTexto.Text );
          EscribeTexto( K_NavLinksLastText,  eUltimoTexto.Text );

          EscribeTexto( K_NavLinksFirstGraphic, ePrimeroImagen.Text );
          EscribeTexto( K_NavLinksPriorGraphic, eAnteriorImagen.Text );
          EscribeTexto( K_NavLinksLastGraphic,  eUltimoImagen.Text );
          EscribeTexto( K_NavLinksNextGraphic,  eSiguienteImagen.Text );

          with FFuente do
          begin
               EscribeTexto( K_FontName, Name );
               EscribeEntero( K_FontColor, Color );
               EscribeEntero( K_FontSize, Size );
               EscribeLogico( K_FontBold, fsBold in Style );
               EscribeLogico( K_FontItalic, fsItalic in Style );
               EscribeLogico( K_FontUnderline, fsUnderline in Style );
               EscribeLogico( K_FontStrikeOut, fsStrikeOut in Style )
          end;
     end;

     inherited;

end;

procedure THtmlDlg_DevEx.DialogoImagen( oEdit: TEdit );
begin
     with OpenPictureDialog do
     begin
          FileName := oEdit.Text;
          if Execute then
             oEdit.Text := FileName;
     end;
end;

procedure THtmlDlg_DevEx.bPrimeroClick(Sender: TObject);
begin
     inherited;
     DialogoImagen( ePrimeroImagen );
end;

procedure THtmlDlg_DevEx.bSiguienteClick(Sender: TObject);
begin
     inherited;
     DialogoImagen( eSiguienteImagen );
end;

procedure THtmlDlg_DevEx.bAnteriorClick(Sender: TObject);
begin
     inherited;
     DialogoImagen( eAnteriorImagen );
end;

procedure THtmlDlg_DevEx.bUltimoClick(Sender: TObject);
begin
     inherited;
     DialogoImagen( eUltimoImagen );
end;

procedure THtmlDlg_DevEx.cbUnaPaginaClick(Sender: TObject);
begin
     inherited;
     cbMostrarNavegador.Enabled := NOT cbUnaPagina.Checked;
     MostrarControles( NOT cbUnaPagina.Checked AND cbMostrarNavegador.Checked,
                       gbMostrarNavegador );
end;




procedure THtmlDlg_DevEx.cbMostrarNavegadorClick(Sender: TObject);
begin
     inherited;
     MostrarControles( NOT cbUnaPagina.Checked AND cbMostrarNavegador.Checked,
                       gbMostrarNavegador );
end;

procedure THtmlDlg_DevEx.rbtnUseTextLinksClick(Sender: TObject);
begin
     inherited;
     rbtnUseGraphicLinks.Checked := NOT rbtnUseTextLinks.Checked;
     pcShowNavigator.ActivePage := tsUseTextLinks;
end;

procedure THtmlDlg_DevEx.rbtnUseGraphicLinksClick(Sender: TObject);
begin
     inherited;
     rbtnUseTextLinks.Checked := NOT rbtnUseGraphicLinks.Checked;
     pcShowNavigator.ActivePage := tsUseGraphicLinks; 
end;

end.
