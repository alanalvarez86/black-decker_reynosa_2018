inherited RtfDlg: TRtfDlg
  Caption = 'RtfDlg'
  ClientHeight = 326
  ClientWidth = 327
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 290
    Width = 327
    inherited OK: TBitBtn
      Left = 159
    end
    inherited Cancelar: TBitBtn
      Left = 244
    end
  end
  inherited PageControl: TPageControl
    Width = 327
    Height = 290
    inherited tsPreferencias: TTabSheet
      inherited gbPageRange: TGroupBox
        Width = 319
      end
      inherited gbItemsToRender: TGroupBox
        Width = 319
        Align = alTop
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 147
        Width = 319
        Height = 115
        Align = alClient
        Caption = ' Opciones '
        TabOrder = 2
        object cbActiveHiperLinks: TCheckBox
          Left = 16
          Top = 16
          Width = 193
          Height = 17
          Caption = 'Ligas Activas'
          TabOrder = 0
        end
        object cbGraphicDataBinary: TCheckBox
          Left = 16
          Top = 36
          Width = 153
          Height = 17
          Caption = 'Gr�ficos en Datos Binarios'
          TabOrder = 1
        end
        object cbEncondingType: TRadioGroup
          Left = 15
          Top = 57
          Width = 289
          Height = 49
          Caption = ' Tipo de Codificaci�n '
          Columns = 2
          Items.Strings = (
            'Posici�n Exacta'
            'F�cil de Editar')
          TabOrder = 2
        end
      end
    end
  end
end
