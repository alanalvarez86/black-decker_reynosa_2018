inherited DocumentDlg_DevEx: TDocumentDlg_DevEx
  Left = 983
  Top = 200
  Caption = 'Configuraci'#243'n'
  ClientHeight = 211
  ClientWidth = 317
  OldCreateOrder = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 175
    Width = 317
    ExplicitTop = 175
    ExplicitWidth = 317
    DesignSize = (
      317
      36)
    inherited OK_DevEx: TcxButton
      Left = 134
      ExplicitLeft = 134
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 224
      Width = 85
      ExplicitLeft = 224
      ExplicitWidth = 85
    end
  end
  object PageControl: TcxPageControl [1]
    Left = 0
    Top = 0
    Width = 317
    Height = 175
    Align = alClient
    TabOrder = 1
    Properties.ActivePage = tsPreferencias
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 173
    ClientRectLeft = 2
    ClientRectRight = 315
    ClientRectTop = 27
    object tsPreferencias: TcxTabSheet
      Caption = 'Preferencias'
      object gbPageRange: TGroupBox
        Left = 0
        Top = 0
        Width = 313
        Height = 69
        Align = alTop
        Caption = ' Rango de P'#225'ginas '
        TabOrder = 0
        object rbTodas: TRadioButton
          Left = 8
          Top = 17
          Width = 113
          Height = 17
          Caption = 'Todas'
          Checked = True
          TabOrder = 0
          TabStop = True
          OnClick = rbTodasClick
        end
        object rbRango: TRadioButton
          Left = 8
          Top = 38
          Width = 65
          Height = 17
          Hint = 
            'Escriba el N'#250'mero y/o Rango de P'#225'ginas Separado por Comas. Por E' +
            'jemplo: 1,3,5-12'
          Caption = 'Algunas:'
          TabOrder = 1
          OnClick = rbTodasClick
          OnEnter = rbRangoEnter
        end
        object ePaginas: TEdit
          Left = 73
          Top = 36
          Width = 224
          Height = 21
          Hint = 
            'Escriba el N'#250'mero y/o Rango de P'#225'ginas Separado por Comas. Por E' +
            'jemplo: 1,3,5-12'
          TabOrder = 2
          OnEnter = ePaginasEnter
        end
      end
      object gbItemsToRender: TGroupBox
        Left = 0
        Top = 69
        Width = 313
        Height = 77
        Align = alClient
        Caption = ' Elementos a Exportar '
        TabOrder = 1
        object cbTextos: TCheckBox
          Left = 16
          Top = 16
          Width = 57
          Height = 17
          Caption = 'Textos'
          TabOrder = 0
        end
        object cbFiguras: TCheckBox
          Left = 16
          Top = 36
          Width = 57
          Height = 17
          Caption = 'Figuras'
          TabOrder = 1
        end
        object cbImagen: TCheckBox
          Left = 16
          Top = 56
          Width = 73
          Height = 17
          Caption = 'Im'#225'genes'
          TabOrder = 2
        end
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
