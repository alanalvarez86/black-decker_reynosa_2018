{$HINTS OFF}
unit FReportes_DevEx;

interface

{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, ComCtrls, ExtCtrls,FileCtrl,
     StdCtrls, Buttons, Db, Menus, ImgList, Grids, DBGrids,
     ZBaseConsulta,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaKeyCombo,
     ZetaDBGrid,
     ZReportConst,
     DReportes, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxTextEdit,
  ZetaCXGrid, cxPCdxBarPopupMenu, cxPC, ActnList, TressMorado2013, cxButtons,
  dxBarBuiltInMenu, System.Actions, cxContainer, cxMaskEdit, cxDropDownEdit,
  ZetaCXStateComboBox;

type
  TcxViewInfoAcess = class(TcxGridTableDataCellViewInfo);
  TcxPainterAccess = class(TcxGridTableDataCellPainter);

type
  TReportes_DevEx = class(TBaseConsulta)
    PanelSuperior: TPanel;
    ListaSmallImages: TImageList;
    Panel1: TPanel;
    SaveDialog: TSaveDialog;
    OpenDialog: TOpenDialog;
    EBuscaNombre: TEdit;
    lbBusca: TLabel;
    PopupMenu1: TPopupMenu;
    mSeleccion: TMenuItem;
    Reportes_PC: TcxPageControl;
    TabReportes: TcxTabSheet;
    TabFavoritos: TcxTabSheet;
    ZetaDBGrid: TZetaCXGrid;
    ZetaDBGridDBTableView: TcxGridDBTableView;
    RE_TIPO_DESCRIP: TcxGridDBColumn;
    RE_CODIGO: TcxGridDBColumn;
    RE_NOMBRE: TcxGridDBColumn;
    RE_TIPO: TcxGridDBColumn;
    RE_TABLA: TcxGridDBColumn;
    RE_FECHA: TcxGridDBColumn;
    US_DESCRIP: TcxGridDBColumn;
    ZetaDBGridLevel: TcxGridLevel;
    TabSuscripciones: TcxTabSheet;
    RE_CLASIFI: TcxGridDBColumn;
    US_FAVORITO: TcxGridDBColumn;
    US_SUSCRITO: TcxGridDBColumn;
    RE_CLASIFI_DESCRIP: TcxGridDBColumn;
    SmallImageList: TcxImageList;
    US_FAVORITOS_DESCRIP: TcxGridDBColumn;
    US_SUSCRITO_DESCRIP: TcxGridDBColumn;
    ZetaDBGrid_Favoritos: TZetaCXGrid;
    ZetaDBGridDBTableView_Favoritos: TcxGridDBTableView;
    RE_TIPO_DESCRIP_FAVORITOS: TcxGridDBColumn;
    RE_CODIGO_FAVORITOS: TcxGridDBColumn;
    RE_NOMBRE_FAVORITOS: TcxGridDBColumn;
    RE_TIPO_FAVORITOS: TcxGridDBColumn;
    RE_TABLA_FAVORITOS: TcxGridDBColumn;
    RE_FECHA_FAVORITOS: TcxGridDBColumn;
    US_FAVORITO_FAVORITOS: TcxGridDBColumn;
    RE_CLASIFI_FAVORITOS: TcxGridDBColumn;
    ZetaDBGridLevel_Favoritos: TcxGridLevel;
    ZetaDBGrid_Suscripciones: TZetaCXGrid;
    ZetaDBGridDBTableView_Suscripciones: TcxGridDBTableView;
    RE_TIPO_DESCRIP_SUSCRIP: TcxGridDBColumn;
    RE_CODIGO_SUSCRIP: TcxGridDBColumn;
    RE_NOMBRE_SUSCRIP: TcxGridDBColumn;
    RE_TIPO_SUSCRIP: TcxGridDBColumn;
    RE_TABLA_SUSCRIP: TcxGridDBColumn;
    RE_FECHA_SUSCRIP: TcxGridDBColumn;
    US_DESCRIP_SUSCRIP: TcxGridDBColumn;
    RE_CLASIFI_DESC_SUSCRIP: TcxGridDBColumn;
    US_SUSCRITO_SUSCRIP: TcxGridDBColumn;
    RE_CLASIFI_SUSCRIP: TcxGridDBColumn;
    ZetaDBGridLevel_Suscripciones: TcxGridLevel;
    US_DESCRIP_FAVORITOS: TcxGridDBColumn;
    RE_CLASIFI_DESCRIP_FAVORITOS: TcxGridDBColumn;
    RE_TIPO_CONDICION: TcxGridDBColumn;
    RE_TIPO_IMAGEN_FAVORITOS: TcxGridDBColumn;
    RE_TIPO_IMAGEN_SUSCRIP: TcxGridDBColumn;
    cxImage16: TcxImageList;
    ActionList: TActionList;
    _A_Imprimir: TAction;
    _E_Agregar: TAction;
    _E_Borrar: TAction;
    _E_Modificar: TAction;
    hh1: TMenuItem;
    mAgregar: TMenuItem;
    mBorrar: TMenuItem;
    mModificar: TMenuItem;
    mImprimir: TMenuItem;
    BImportar: TcxButton;
    BExportar: TcxButton;
    BFavoritos: TcxButton;
    BBusca_DevEx: TcxButton;
    RE_CODIGO_TEXT: TcxGridDBColumn;
    RE_CODIGO_TEXT_FAVORITOS: TcxGridDBColumn;
    RE_CODIGO_TEXT_SUSCRIP: TcxGridDBColumn;
    cbTipoBusqueda: TcxStateComboBox;
    procedure FormCreate(Sender: TObject);
    //procedure CBTablasChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    {procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState); }
    {procedure DBGridTitleClick(Column: TColumn);}
    procedure BExportarOrClick(Sender: TObject);
    procedure BImportarOrClick(Sender: TObject);
    procedure BBuscaClick(Sender: TObject);
    procedure EBuscaNombreChange(Sender: TObject);
    Procedure CreaColumaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind; GridView:TcxGridDbTableView );
    procedure mSeleccionClick(Sender: TObject);
    procedure BFavoritosOrClick(Sender: TObject);
    {procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);}
    procedure US_SUSCRITO_DESCRIPCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure US_FAVORITOS_DESCRIPCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure RE_TIPO_DESCRIPCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TabFavoritosShow(Sender: TObject);
    procedure TabSuscripcionesShow(Sender: TObject);
    procedure ZetaDBGridDBTableViewDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableView_FavoritosDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableView_SuscripcionesDblClick(Sender: TObject);
    procedure ZetaDBGridDBTableView_SuscripcionesDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableView_FavoritosDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure ZetaDBGridDBTableViewDataControllerFilterGetValueList(
      Sender: TcxFilterCriteria; AItemIndex: Integer;
      AValueList: TcxDataFilterValueList);
    procedure TabReportesShow(Sender: TObject);
    procedure RE_TIPO_DESCRIP_FAVORITOSCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure RE_TIPO_DESCRIP_SUSCRIPCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
    procedure RE_TIPO_DESCRIP_SUSCRIPHeaderClick(Sender: TObject);
    procedure RE_TIPO_DESCRIPHeaderClick(Sender: TObject);
    procedure RE_TIPO_DESCRIP_FAVORITOSHeaderClick(Sender: TObject);
    procedure US_FAVORITOS_DESCRIPHeaderClick(Sender: TObject);
    procedure US_SUSCRITO_DESCRIPHeaderClick(Sender: TObject);
    procedure ZetaDBGridDBTableView_FavoritosCanSelectRecord(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      var AAllow: Boolean);
    procedure _A_ImprimirExecute(Sender: TObject);
    procedure _E_AgregarExecute(Sender: TObject);
    procedure _E_BorrarExecute(Sender: TObject);
    procedure _E_ModificarExecute(Sender: TObject);
    procedure ZetaDBGridDBTableViewCanSelectRecord(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      var AAllow: Boolean);
    procedure ZetaDBGridDBTableView_SuscripcionesCanSelectRecord(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      var AAllow: Boolean);
    procedure ZetaDBGridDBTableViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ZetaDBGridDBTableView_FavoritosKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure ZetaDBGridDBTableView_SuscripcionesKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure cbTipoBusquedaPropertiesChange(Sender: TObject);

  private
    //lBorrando : Boolean;
    FActiveTab: TcxTabSheet;
    FClasifiReporte : eClasifiReporte;
    FCampo:TcxGridDBColumn;
    //FClasifiTemp : integer;
    procedure ExportaArchivo;
    procedure ImportaArchivo;
    //DevEx: Se cambio el funcionamiento de GetDerechosClasifi
    function GetDerechosClasifi: Integer;
    //function PuedeConsultar: Boolean;
    procedure EliminaClasificaciones;
    function ChecaReportes: Boolean;
    function ChecaFavoritos: Boolean;
    function ChecaSuscripciones: Boolean;
    procedure FiltraReporte;
    procedure FiltroTab( ZetaDBGridDBTableView_Temp:TcxGridDBTableView; ColTab: TcxGridDBColumn);
    //DevEx: Selecciona el grid al que se le aplicara el BestFit
    procedure AplicaBestFit;
    //procedure GridRecordCount;
    procedure AgregaFavoritos;
    //procedure ActualizaForma( const eClasificacion: eClasifiReporte );
    procedure DefineColAgrupacion;
    procedure DesactivaMultiSelect;
    function NoHayDatosGridFavoritos: Boolean;
    function NoHayDatosGridSuscripciones: Boolean;
    function NoHayDatosGrid: Boolean;
    function ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
    procedure ActivaBotones;
    procedure DesactivaBotones;
    procedure EnfocaPrimeroEnGrupo (DevEx_TableView: TcxGridDBTableView);
    procedure EnfocaDefault (DevEx_TableView: TcxGridDBTableView);
    function EsGrupo ( ARow: TcxCustomGridRow ): Boolean;
    procedure ConexionDatos;
    function RealField(const FieldName: String):Boolean;
    procedure ReasignaCampoUsuario;
    procedure PreparaFiltros;
  protected
    {********* NUEVA IMPLEMENTACION ********}
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
    procedure Imprimir;override;
    procedure Exportar; override;

    function PuedeImprimir(var sMensaje: String): Boolean;override;
    function PuedeModificar(var sMensaje: String): Boolean;override;
    function PuedeAgregar(var sMensaje: String): Boolean;override;
    function PuedeBorrar(var sMensaje: String): Boolean;override;

    function CheckUnDerecho( const NumeroDerecho, iDerecho : Integer ): Boolean;
    procedure Disconnect; override;
  public
    FListaClasificaciones : TStrings;
    property  ClasifiReporte : eClasifiReporte read FClasifiReporte write FClasifiReporte default crFavoritos;
    procedure DoLookup;override;
  end;

const
     //K_COL_IMAGE = 0;
     //K_COL_TIPO  = 3;
     K_COL_TABLA = 4;
     //K_COL_FECHA = 5;
     //K_COL_ESPACIO = 6; //porque se quido col vacia
     //K_COL_USUARIO = 7;
     //K_COL_USUARIO = 6;
     //Columnas Invisibles
     K_COL_FAVORITO = 10;
     K_COL_SUSCRITO = 11;
     //Globales para ImageIndex
     K_IMAGE_FAVORITO = 0;
     K_IMAGE_SUSCRITO = 1;
     K_IMAGE_LISTADO = 2;
     K_IMAGE_FORMA = 3;
     K_IMAGE_ETIQUETA = 4;
     K_IMAGE_POLIZA = 5;
     K_IMAGE_P_CONCEPTO = 6;
     K_IMAGE_I_RAPIDA = 7;
     K_IMAGE_FUTURO = 8;
     //GLOBALES PARA TIPOS DE REPORTES
     K_ES_LISTA = 'Listado';
     K_ES_FORMA = 'Forma';
     K_ES_ETIQUETA = 'Etiqueta';
     K_ES_POLIZA = 'P�liza';
     K_ES_P_CONCEPTO = 'P-Concepto';
     K_ES_I_RAPIDA = 'I-R�pida';
     K_ES_FUTURO = '<FUTURO>';
          {***Formastos para las sumatorias***}
     K_SIN_TIPO = 0;
     K_TIPO_MONEDA = 1;

     K_ESPACIO = ' ';
     K_FORMATO_MONEDA = ',0.00;-,0.00';
     K_FORMATO_DEFAULT ='0' ;
var
  Reportes_DevEx: TReportes_DevEx;

implementation

uses ZetaDialogo,
     ZReportTools,
     ZetaCommonTools,
     ZetaClientTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaWinAPITools,
     {$IFDEF ADUANAS}
     ZHelpContext,
     {$endif}
     FTressShell,
     DCliente,
     DBaseReportes,
     DDiccionario,
     FBaseReportes; //Se incluye pues se tiene que hacer override a Exportar porque ahora tenemos 3 Grids en  pantalla.

{$R *.DFM}


{************** Forma de Reportes **************}

{*******Metodos para reutilizar funciones basicas******}

//Definicion de columnas por las cuales se puede agrupar
procedure TReportes_DevEx.DefineColAgrupacion;
begin
      //Permitir agrupacion y aparecer la caja de agrupacion
      ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
      ZetaDBGridDBTableView.OptionsView.GroupByBox := True;

      ZetaDBGridDBTableView_Favoritos.OptionsCustomize.ColumnGrouping := True;
      ZetaDBGridDBTableView_Favoritos.OptionsView.GroupByBox := True;

      ZetaDBGridDBTableView_Suscripciones.OptionsCustomize.ColumnGrouping := True;
      ZetaDBGridDBTableView_Suscripciones.OptionsView.GroupByBox := True;

      {***Bloquear agrupacion para las siguientes columnas en tiempo de ejecucion***}
       //Reportes_PC.ActivePage := TabReportes;
      //Grid Reportes
      ZetaDBGridDBTableView.GetColumnByFieldName('RE_CODIGO').Options.Grouping := False;
      ZetaDBGridDBTableView.GetColumnByFieldName('RE_NOMBRE').Options.Grouping := False;
      //Grid Favoritos
      ZetaDBGridDBTableView_Favoritos.GetColumnByFieldName('RE_CODIGO').Options.Grouping := False;
      ZetaDBGridDBTableView_Favoritos.GetColumnByFieldName('RE_NOMBRE').Options.Grouping := False;
      //Grid Suscripciones
      ZetaDBGridDBTableView_Suscripciones.GetColumnByFieldName('RE_CODIGO').Options.Grouping := False;
      ZetaDBGridDBTableView_Suscripciones.GetColumnByFieldName('RE_NOMBRE').Options.Grouping := False;

      {***Nota: Para las columnas asociadas a un OnDrawColumn event se bloquea el agrupamiento
          desde el diseno pues marca un AccesViolation porque no tienen asociado un FieldName
          y es la forma que utilizamos para encontrar la columna y bloquear el agrupacmiento.
          Estas columnas son las siguientes: RE_TIPO_DESCRIP, US_FAVORITOS_DESCRIP,
          US_SUSCRITO_DESCRIP, RE_TIPO_DESCRIP_FAVORITOS, RE_TIPO_DESCRIP_SUSCRIP***}
end;

//Metodos para el control de Clasificaciones
procedure TReportes_DevEx.EliminaClasificaciones; //DevEx: Quitar clasificaciones que no tengan derecho de consulta
var
 I, iDerecho:Integer;
 eClasificacion: eClasifiReporte;
begin
     with dmReportes do
     begin
          for I := ClasificacionesPermitidas.Count - 1 downto 0 do
          begin
               eClasificacion := eClasifiReporte( Integer( ClasificacionesPermitidas.Objects[ I ] ) );
               iDerecho := dmDiccionario.GetDerechosClasifi( eClasificacion );
               if not CheckUnDerecho( iDerecho, K_DERECHO_CONSULTA ) then
                  ClasificacionesPermitidas.Delete(I);
          end;
     end;
end;

function TReportes_DevEx.GetDerechosClasifi: Integer;
{$IFNDEF VISITANTES}
var
   i: Integer;
   eClasificacion: eClasifiReporte;
{$ENDIF}
begin
     {$IFDEF VISITANTES}
             Result := dmDiccionario.GetDerechosClasifi( crVisitantes ) ;
     {$ELSE}
         //Como no tenemos selectedIndex, recorremos la lista para buscar el ItemIndex que debemos mandar
         eClasificacion := -1;
         with dmReportes do
         begin
              for i := 0 to ClasificacionesPermitidas.Count - 1 do
              begin
                   if ( cdsEditReporte.State = dsBrowse ) then
                      cdsEditReporte.Active := FALSE;
                   if Integer( ClasificacionesPermitidas.Objects[i] ) = cdsReportes.FieldByName('RE_CLASIFI').AsInteger then
                   begin
                         eClasificacion := eClasifiReporte( Integer( ClasificacionesPermitidas.Objects[ i ] ) );
                         Break;
                   end;

              end;
         end;
         if eClasificacion >= 0 then
            Result := dmDiccionario.GetDerechosClasifi( eClasificacion )
         else
             Result := eClasificacion;
         //Este metodo hace lo mismo pero lo busca por el String
         {with dmReportes do
         begin
              for i := 0 to ClasificacionesPermitidas.Count - 1 do
              begin
                   if ( cdsEditReporte.State = dsBrowse ) then
                      cdsEditReporte.Active := FALSE;
                   if ClasificacionesPermitidas.Strings[i] = cdsReportes.FieldByName('RE_CLASIFI_DESC').AsString then
                   begin
                        eClasificacion := eClasifiReporte( Integer( ClasificacionesPermitidas.Objects[ i ] ) );
                        Result := dmDiccionario.GetDerechosClasifi( eClasificacion );
                   end;
              end;
         end;}
     {$ENDIF}
end;

//Revision de derechos
function TReportes_DevEx.CheckUnDerecho( const NumeroDerecho, iDerecho : Integer ): Boolean;
begin
     {$ifdef RDD}
     Result:= ZAccesosMgr.CheckUnDerecho( NumeroDerecho, iDerecho );
     {$else}
     Result:= ZAccesosMgr.CheckDerecho( NumeroDerecho, iDerecho );
     {$endif}
end;

//BestFit
procedure TReportes_DevEx.AplicaBestFit;
begin
      {
      Aplicar BestFit al grid del tab activo, donde:
       0=TabReportes
       1=TabFavoritos
       2=TabSuscripciones
      }
      //Guardar Selected Record
     case Reportes_PC.ActivePageIndex of
          0 : ZetaDBGridDBTableView.ApplyBestFit();
          1 : ZetaDBGridDBTableView_Favoritos.ApplyBestFit();
          2 : ZetaDBGridDBTableView_Suscripciones.ApplyBestFit();
     end;
     //Devolver Selected Record
end;

{
procedure TReportes_DevEx.GridRecordCount;
var
   GridRowCount:Integer;
begin
    case Reportes_PC.ActivePageIndex of
          0 :
          begin
               GridRowCount := ZetaDBGridDBTableView.DataController.RowCount;
               if GridRowCount > 0 then
               begin
                    ZetaDBGridDBTableView.ViewData.Rows[0].Focused := True;
               end;
          end;
          1 :
          begin
               GridRowCount := ZetaDBGridDBTableView_Favoritos.DataController.RowCount;
               if GridRowCount > 0 then
               begin
                    ZetaDBGridDBTableView_Favoritos.ViewData.Rows[0].Focused := True;
               end;
          end;
          2 :
          begin
               GridRowCount := ZetaDBGridDBTableView_Suscripciones.DataController.RowCount;
               if GridRowCount > 0 then
               begin
                    ZetaDBGridDBTableView_Suscripciones.ViewData.Rows[0].Focused := True;
               end;
          end;
     end;
      //Bloquea Borrar ,Modificar ,Agregar
end;   }

//Procedimientos utiles para enfocar registros
procedure TReportes_DevEx.EnfocaDefault (DevEx_TableView: TcxGridDBTableView); //Enfoca el primer registro de un TableView
Const
     PRIMER_REGISTRO = 0;
begin
     with DevEx_TableView do
     begin
          BeginUpdate;
          try
             ViewData.Records [PRIMER_REGISTRO].Focused := True;
             ViewData.Records [PRIMER_REGISTRO].Selected := True;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TReportes_DevEx.EnfocaPrimeroEnGrupo (DevEx_TableView: TcxGridDBTableView); //Enfoca el primer registro de un grupo. (Se utiliza para resolver un bug que te cierra el grupo despues de refrescar)
var
 index:Integer;
begin
     with DevEx_TableView do
     begin
          if EsGrupo (Controller.FocusedRow) then
          begin
               //Abrir el grupo
               Controller.FocusedRow.AsGroupRow.Expanded := True;
               //Enfocar el primer registro
               index := Controller.FocusedRow.Index;
               Controller.ClearSelection;
               if  not (DataController.RowCount<=0) then
               begin
                    BeginUpdate;
                    try
                       ViewData.Records [index+1].Focused := True;
                       ViewData.Records [index+1].Selected := True;
                    finally
                           EndUpdate;
                    end;
               end;

          end;
     end;
end;

//Definicon del Grid Activo
function TReportes_DevEx.ChecaReportes: Boolean;
begin
     with Reportes_PC do
         Result := ( ActivePage = TabReportes );
end;

function TReportes_DevEx.ChecaFavoritos: Boolean;
begin
     with Reportes_PC do
         Result := ( ActivePage = TabFavoritos );
end;

function TReportes_DevEx.ChecaSuscripciones: boolean;
begin
     with Reportes_PC do
         Result := ( ActivePage = TabSuscripciones );
end;

//NoHayDatos para los Grids
function TReportes_DevEx.NoHayDatosGridFavoritos: Boolean;
begin
     Result := False;
     if ZetaDBGridDBTableView_Favoritos.DataController.RowCount<=0 then
        Result := True;

end;
function TReportes_DevEx.NoHayDatosGridSuscripciones: Boolean;
begin
     Result := False;
     if ZetaDBGridDBTableView_Suscripciones.DataController.RowCount<=0 then
       Result := True;
end;
function TReportes_DevEx.NoHayDatosGrid: Boolean;
begin
     Result := False;
     if ZetaDBGridDBTableView.DataController.RowCount<=0 then
       Result := True;
end;
//Funcion Que define si se selecciono un grupo o no
function TReportes_DevEx.EsGrupo ( ARow: TcxCustomGridRow ): Boolean;
var
   AAllow: Boolean;
begin
   if ARow is TcxGridGroupRow then
    AAllow := True
  else
    AAllow := False;
  Result:= AAllow;
end;

//Activacion y Desactivacion de los Botones
procedure TReportes_DevEx.ActivaBotones;
begin
     BFavoritos.Enabled := True;
     BExportar.Enabled := True;
     BImportar.Enabled := True;
//Edit Devex By MP: Modificacion para habilitar y deshabilitar los botones de fevoritos, Importar y exportar
     BExportar.Enabled := not ( dmCliente.ModoSuper or
                                        dmReportes.ChecaFavoritos or
                                        dmReportes.ChecaSuscripciones );

             BImportar.Enabled := BExportar.Enabled;
             BFavoritos.Enabled := not ( dmReportes.ChecaFavoritos or
                                        dmReportes.ChecaSuscripciones );
//Fin edit
end;
procedure TReportes_DevEx.DesactivaBotones;
begin
     BFavoritos.Enabled := False;
     BExportar.Enabled := False;
     BImportar.Enabled := False;
end;

//Multiselect
procedure TReportes_DevEx.DesactivaMultiSelect;
begin
     //DesactivaMultiselect para todos los Grids
     mSeleccion.Checked:=False;
     ZetaDBGridDBTableView.OptionsSelection.MultiSelect := False;
     ZetaDBGridDBTableView_Favoritos.OptionsSelection.MultiSelect := False;
     ZetaDBGridDBTableView_Suscripciones.OptionsSelection.MultiSelect := False;
end;

{*********** Metodos Base ********************}
procedure TReportes_DevEx.Connect;
begin
//
end;

procedure TReportes_DevEx.Refresh;
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        //Carga Reportes
        dmReportes.cdsReportes.Refrescar;
        //Ordena del DataSet por el cambo RE_NOMBRE
        //ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'RE_NOMBRE'); old
        FiltraReporte; //Hace la busqueda si hay texto en la cajita
        AplicaBestFit;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TReportes_DevEx.Agregar;
begin
     dmReportes.cdsReportes.Agregar;
     AplicaBestFit;
end;

procedure TReportes_DevEx.Borrar;
var
   i, index: integer;
   lEsUltimo, lHuboCambios: Boolean;
   codigos: TStringList;
{procedure EnfocaDefault;
begin
     if not NoHayDatos then
     begin
          ZetaDBGridDBTableView.ViewData.Records [0].Focused := True;
          ZetaDBGridDBTableView.ViewData.Records [0].Selected := True;
     end;
end;}
procedure EnfocaRegistro ( ATableView:TcxGridDBTableView; lEsUltimo:Boolean; index:Integer );
begin
     with ATableView do
     begin
          Controller.ClearSelection;
          if  not (DataController.RowCount<=0) then
          begin
               //Verifica si se borro el ultimo registro del Grid, en caso de que si enfoca y selecciona el anterior.
               BeginUpdate;
               try
                  if lEsUltimo then
                  begin
                    ViewData.Records [index-1].Focused := True;
                    ViewData.Records [index-1].Selected := True;
                  end
                  else
                  begin
                    ViewData.Records [index].Focused := True;  //CUANDO NO SE BORRA NADA TMB. DEBE ENTRAR AQUI.
                    ViewData.Records [index].Selected := True;
                  end;
               finally
                      EndUpdate;
               end;
               //
               //
               {
                              Except
                     EnfocaDefault;
               }
          end;
          {else
               EnfocaDefault;}
     end;
end;
begin
     //Inicializacion
     lHuboCambios := False;
     lEsUltimo := False;
     index:=0;
     codigos := TStringList.Create;
     codigos.Clear;

     //La eliminacion del registro solo se hace desde el TabReportes
     if ChecaFavoritos then
     begin
          //Elimina de Favoritos
          with ZetaDBGridDBTableView_Favoritos do
          begin
               //Si solo es un reporte
               if Controller.SelectedRecordCount <=1 then
               begin
                    BeginUpdate;
                    try
                       //Se guarda el index del registro antes de borrarlo y se inicializa la bandera para saber si es el ultimo registro
                       index := Controller.FocusedRow.Index;
                       if index = DataController.RowCount-1 then
                          lEsUltimo:= True;
                       if ZetaDialogo.ZConfirm( Caption, Format( '�Desea Quitar el Reporte: ' + CR_LF + ' %s de Favoritos?' , [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0, mbNo) then
                       begin
                            dmReportes.BorraReporteDeFavoritos;
                            lHuboCambios:= True;
                       end;
                    finally
                           EndUpdate;
                    end;
               end
               else
               begin
                    //Si estan seleccionados varios reportes
                    if ZetaDialogo.ZConfirm( Caption, Format( '�Desea Quitar los %d Reportes Seleccionados de Favoritos?', [ Controller.SelectedRecordCount ] ), 0, mbNo) then
                    begin
                         BeginUpdate;
                         try
                            //Es necesario recorrer la seleccion  de abajo hacia arriba para guardar el primer indice.
                             for i := Controller.SelectedRecordCount -1 downto 0 do
                             begin
                                  //Se enfoca el registro que se va a borrar, antes de hacer el bookmark
                                  Controller.SelectedRecords[i].Focused := True;
                                   //Verifica si se borro el ultimo registro del Grid
                                  index := Controller.FocusedRow.Index;
                                  if index = DataController.RowCount-1 then
                                     lEsUltimo:= True;
                                  codigos.Add(DataController.DataSource.DataSet.FieldByName('RE_CODIGO').AsString);
                                  {if dmReportes.PuedeAbrirCandado then
                                       codigos.Add(DataController.DataSource.DataSet.FieldByName('RE_CODIGO').AsString)
                                  else
                                      ZetaDialogo.zInformation( '� Atenci�n !', Format( 'El Reporte %s' + CR_LF + 'S�lo Puede Ser Borrado Por Su Autor', [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0 );}
                             end;
                             {***BORRAR CODIGOS***}
                             dmReportes.BorraReportesDeFavoritos(codigos);
                             lHuboCambios:= True;
                         finally
                                EndUpdate;
                         end;
                    end;
               end;
          end;
     end
     else if ChecaSuscripciones then
     begin
          //Elimina de Suscripciones
          with ZetaDBGridDBTableView_Suscripciones do
          begin
               if Controller.SelectedRecordCount <=1 then
               begin
                    BeginUpdate;
                    try
                       //Se guarda el index del registro antes de borrarlo y se inicializa la bandera para saber si es el ultimo registro
                       index := Controller.FocusedRow.Index;
                       if index = DataController.RowCount-1 then
                          lEsUltimo:= True;
                       //Si el usuario desea borrar el reporte
                        if  dmReportes.PuedeAbrirCandado then
                        begin
                            ZetaDialogo.zInformation( '� Atenci�n !', '� No se Pueden Borrar Reportes desde Suscripciones !', 0 );
                        end
                        else
                            ZetaDialogo.zInformation( '� Atenci�n !', '� No se Pueden Borrar Reportes desde Suscripciones !', 0 );
                    finally
                      EndUpdate;
                    end;
               end
               else
               begin
                    if ZetaDialogo.ZConfirm( Caption, Format( '�Desea Quitar los %d Reportes Seleccionados de Suscripciones?', [ Controller.SelectedRecordCount ] ), 0, mbNo) then
                    begin
                         BeginUpdate;
                         try
                            for i := Controller.SelectedRecordCount -1 downto 0 do
                             begin
                                  //Se enfoca el registro que se va a borrar, antes de hacer el bookmark
                                  Controller.SelectedRecords[i].Focused := True;
                                   //Verifica si se borro el ultimo registro del Grid
                                  index := Controller.FocusedRow.Index;
                                  if index = DataController.RowCount-1 then
                                     lEsUltimo:= True;
                                  if dmReportes.PuedeAbrirCandado then
                                       codigos.Add(DataController.DataSource.DataSet.FieldByName('RE_CODIGO').AsString)
                                  else
                                      ZetaDialogo.zInformation( '� Atenci�n !', Format( 'El Reporte %s' + CR_LF + 'S�lo Puede Ser Borrado Por Su Autor', [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0 );
                             end;
                             {***BORRAR CODIGOS***}
                             dmReportes.BorraReportesDeSuscripciones(codigos);
                             lHuboCambios:= True;
                         finally
                                EndUpdate;
                         end; //End Try
                    end;//End If ZetaDialogo
               end;//End If SelectedRecordCount
          end; //End with
     end //EndIf ChecaSuscripciones
     else
     begin
          //Elimina el reporte
          with ZetaDBGridDBTableView do
          begin
               //Si solo es un reporte
               if Controller.SelectedRecordCount <=1 then
               begin
                    BeginUpdate;
                    try
                       //Se guarda el index del registro antes de borrarlo y se inicializa la bandera para saber si es el ultimo registro
                       index := Controller.FocusedRow.Index;
                       if index = DataController.RowCount-1 then
                          lEsUltimo:= True;
                       if  dmReportes.PuedeAbrirCandado then
                       begin
                         if ZetaDialogo.ZConfirm( Caption, Format( '�Desea Borrar el Reporte: ' + CR_LF + ' %s?', [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0, mbNo) then
                         begin
                              dmReportes.BorraReporte;
                              lHuboCambios:= True;
                         end;
                       end
                       else
                        ZetaDialogo.zInformation( '� Atenci�n !', 'Este Reporte S�lo Puede Ser Borrado Por Su Autor', 0 );
                    finally
                           EndUpdate;
                    end;
               end
               else
               begin
                    //Si estan seleccionados varios reportes
                    if ZetaDialogo.ZConfirm( Caption, Format( '�Desea Borrar los %d Reportes Seleccionados?', [ Controller.SelectedRecordCount ] ), 0, mbNo) then
                    begin
                         BeginUpdate;
                         try
                         //Se recorren los registros seleccionados del ultimo al primero
                             {for i := Controller.SelectedRecordCount -1 downto 0 do
                             begin
                                  //Se enfoca el registro que se va a borrar, antes de hacer el bookmark
                                  Controller.SelectedRecords[i].Focused := True;
                                  //Verifica si se borro el ultimo registro del Grid
                                  index := Controller.FocusedRow.Index;
                                  if index = DataController.RowCount-1 then
                                  begin
                                       lEsUltimo:= True;
                                  end;
                                  //DataSource.DataSet.GotoBookmark(DataController.DataSet.GetBookmark); //OLD
                                  if dmReportes.PuedeAbrirCandado then
                                       dmReportes.BorraReporte
                                  else
                                      ZetaDialogo.zInformation( '� Atenci�n !', Format( 'El Reporte %s' + CR_LF + 'S�lo Puede Ser Borrado Por Su Autor', [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0 );
                             end;}
                             //Es necesario recorrer la seleccion  de abajo hacia arriba para guardar el primer indice.
                             for i := Controller.SelectedRecordCount -1 downto 0 do
                             begin
                                  //Se enfoca el registro que se va a borrar, antes de hacer el bookmark
                                  Controller.SelectedRecords[i].Focused := True;
                                   //Verifica si se borro el ultimo registro del Grid
                                  index := Controller.FocusedRow.Index;
                                  if index = DataController.RowCount-1 then
                                     lEsUltimo:= True;
                                  if dmReportes.PuedeAbrirCandado then
                                       codigos.Add(DataController.DataSource.DataSet.FieldByName('RE_CODIGO').AsString)
                                  else
                                      ZetaDialogo.zInformation( '� Atenci�n !', Format( 'El Reporte %s' + CR_LF + 'S�lo Puede Ser Borrado Por Su Autor', [ DataSource.DataSet.FieldByName('RE_NOMBRE').AsString ] ), 0 );
                             end;
                             {***BORRAR CODIGOS***}
                             dmReportes.BorraReporte_DevEx(codigos);
                             lHuboCambios:= True;
                         finally
                                EndUpdate;
                         end;

                    end;
               end;
          end;
     end;
     //Si hubo cambios Refresca y Enfocar
     if lHuboCambios then
     begin
          {***REFRESCAR INFORMACION***}
          Refresh;
          {***ENFOCAR REGISTRO***}
          if ChecaFavoritos then
             EnfocaRegistro ( ZetaDBGridDBTableView_Favoritos, lEsUltimo, index )
          else if ChecaSuscripciones then
               EnfocaRegistro ( ZetaDBGridDBTableView_Suscripciones, lEsUltimo, index )
          else
              EnfocaRegistro ( ZetaDBGridDBTableView, lEsUltimo, index );
     end;
     AplicaBestFit;
end;

procedure TReportes_DevEx.cbTipoBusquedaPropertiesChange(Sender: TObject);
begin
  inherited;
  PreparaFiltros;
  FiltraReporte; //Al cambiar de col, busca el texto en la nueva col.
end;

procedure TReportes_DevEx.PreparaFiltros;
begin
    {***(@am):Selecciona la columna a la que se le aplicara el filtro***}
    if ChecaFavoritos then
    begin
        case eTipoColumnaReporte( cbTipoBusqueda.ItemIndex ) of
          tcNombre: FCampo := RE_NOMBRE_FAVORITOS ;
          tcCodigo: FCampo := RE_CODIGO_FAVORITOS ;
          tcClasificacion: FCampo := RE_CLASIFI_DESCRIP_FAVORITOS  ;
          tcTablaPrincipal: FCampo := RE_TABLA_FAVORITOS  ;
        end;
    end
    else if ChecaSuscripciones then
    begin
        case eTipoColumnaReporte( cbTipoBusqueda.ItemIndex ) of
          tcNombre: FCampo := RE_NOMBRE_SUSCRIP ;
          tcCodigo: FCampo := RE_CODIGO_SUSCRIP ;
          tcClasificacion: FCampo := RE_CLASIFI_DESC_SUSCRIP ;
          tcTablaPrincipal: FCampo := RE_TABLA_SUSCRIP ;
        end;
    end
    else
    begin
         case eTipoColumnaReporte( cbTipoBusqueda.ItemIndex ) of
          tcNombre: FCampo := RE_NOMBRE ;
          tcCodigo: FCampo := RE_CODIGO ;
          tcClasificacion: FCampo := RE_CLASIFI_DESCRIP ;
          tcTablaPrincipal: FCampo := RE_TABLA ;
         end;
    end;
end;

procedure TReportes_DevEx.Modificar;
var
   oCursor : TCursor;
   cxTabeView:TcxGridDBTableView;
begin
//Definir en que tableView estamos
if ChecaFavoritos then
   cxTabeView:= ZetaDBGridDBTableView_Favoritos
else if ChecaSuscripciones then
     cxTabeView:= ZetaDBGridDBTableView_Suscripciones
else
     cxTabeView:= ZetaDBGridDBTableView;
//Valida que sea un registro y no un grupo
if not EsGrupo(cxTabeView.Controller.FocusedRow) then  //PENDIENTE: Validiar que siempre exista una row enfocada y que se haga para cada TableView
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        with dmReportes do
        begin
             if dmCliente.ModoSuper then
             begin
                  SoloLectura := TRUE;
                  SoloImpresion := TRUE;
             end
             else
             begin
                  if not SoloLectura then
                     SoloLectura := not dmReportes.PuedeAbrirCandado;
                  SoloImpresion := SoloLectura;
             end;
             cdsReportes.Modificar;
        end;
     finally
            Screen.Cursor := oCursor;
            with dmReportes do
            begin
                 SoloImpresion := FALSE;
                 SoloLectura := FALSE;
            end;
     end;
     //Enfoca primer registro de un grupo
     EnfocaPrimeroEnGrupo( cxTabeView );
     //AplicaBestFit; //Se habia agregado por si se borraba al agrupar, pero no sirve.
end;
end;

//No se ocupa porque el cxGrid ya ordena
{procedure TReportes_DevEx.DBGridTitleClick(Column: TColumn);
begin
     inherited; }
     {if Column.Index = 0 then
        DBGrid.OrdenarPor( DBGrid.Columns[2] )
     else if Column.Index = 5 then
          DBGrid.OrdenarPor( DBGrid.Columns[4] )
     else DBGrid.OrdenarPor( Column );}
     {with dmReportes do
          case Column.Index of
               K_COL_IMAGE:ZetaClientTools.OrdenarPor(cdsReportes,DBGrid.Columns[K_COL_TIPO].FieldName);
               K_COL_ESPACIO:ZetaClientTools.OrdenarPor(cdsReportes,DBGrid.Columns[K_COL_FECHA].FieldName);
               K_COL_USUARIO:ZetaClientTools.OrdenarPor(cdsReportes,'US_CODIGO')
               else ZetaClientTools.OrdenarPor(cdsReportes,Column.FieldName )
          end;
end;
}

procedure TReportes_DevEx.ExportaArchivo;
 var sError, sNombre, sDirPlantilla : string;
     i : integer;
     FLista : TStrings;
  function RevisaArchivo : string;
  begin
       Result := sNombre;
       Result := StrTransAll( Result, '/', ' ' );
       Result := StrTransAll( Result, '\', ' ' );
       Result := StrTransAll( Result, ':', ' ' );
       Result := StrTransAll( Result, '*', ' ' );
       Result := StrTransAll( Result, '?', ' ' );
       Result := StrTransAll( Result, '"', ' ' );
       Result := StrTransAll( Result, '<', ' ' );
       Result := StrTransAll( Result, '>', ' ' );
       Result := StrTransAll( Result, '|', ' ' );
       Result := StrTransAll( Result, '-', ' ' );
  end;
  function EjecutaSaveDialog : Boolean;
  begin
       Result := TRUE;
       with SaveDialog do
       begin
            if ( i >0 ) then
                 FileName := InitialDir +'\'+RevisaArchivo + '.RP1'
            else
            begin
                if InitialDir = '' then
                   InitialDir := sDirPlantilla;
                FileName := RevisaArchivo;
                Result := Execute;
                if Result then
                   InitialDir := ExtractFileDir(FileName);
            end;
       end;
  end;
  function ExportaReporte : Boolean;
  begin
       sNombre := dmReportes.cdsReportes.FieldByName('RE_NOMBRE').AsString;

       Result := EjecutaSaveDialog;
       if Result then
       begin
            if dmReportes.Exporta( SaveDialog.FileName, sError ) then
               FLista.Add( 'El Reporte:  ' + Comillas(sNombre) + '  fue exportado con �xito')
            else
                if sError > '' then
                   FLista.Add( 'El Reporte:  ' + Comillas(sNombre) + '  no se export� debido al siguiente error: '+ CR_LF+sError );
            //cv: 25-JULIO-2002
            //Hay m�dulos en los que el combo de CBTablas (Clasificaci�n de Reportes),
            //no est� visible y hay m�dulos en los que el combo esta deshabilitado.
            {if  CBTablas.Visible AND CBTablas.Enabled then
            begin
                 CBTablas.SetFocus;
            end;}
       end
  end;
begin
if ChecaReportes then
begin
     sDirPlantilla := zReportTools.DirPlantilla;

     if NOT DirectoryExists(sDirPlantilla) then
     begin
          sDirPlantilla := ExtractFileDir( ZetaWinAPITools.GetTempDir );
     end;

     FLista := TStringList.Create;
     try
        //dmReportes.cdsReportes.DisableControls;
             with ZetaDBGridDBTableView do
             begin
                  if Controller.SelectedRecordCount <=1 then
                  begin
                       if not EsGrupo( Controller.FocusedRow ) then
                       begin
                            ExportaReporte;
                       end
                       else
                           ZetaDialogo.ZInformation(Caption, 'Seleccione el reporte dentro del grupo',0);
                  end
                  else
                  begin
                       for i :=0 to Controller.SelectedRecordCount -1 do
                             begin
                                  Controller.SelectedRecords[i].Focused := True;
                                  DataSource.DataSet.GotoBookmark(DataController.DataSet.GetBookmark);
                                  sNombre := dmReportes.cdsReportes.FieldByName('RE_NOMBRE').AsString;
                                  if not EsGrupo( Controller.FocusedRow ) then
                                     if NOT ExportaReporte then
                                        Break;
                             end;
                  end;
             end;
        if FLista.Count >0 then
           ZetaDialogo.ZInformation( Caption, FLista.Text, 0);
     finally
            FreeAndNil(FLista);
            //dmReportes.cdsReportes.EnableControls;
     end;
end
else
    ZetaDialogo.ZInformation( Caption,
                                    'Colocarse en Pesta�a Reportes para Exportar',
                                    0 );
end;

procedure TReportes_DevEx.ImportaArchivo;
 var
    sDirPlantilla : string;
    oCursor : TCursor;
 function EjecutaOpenDialog : Boolean;
 begin
      with OpenDialog do
      begin
            {(@am): ***En Delphi7 la asignacion de InitialDir no funcionaba. Por lo tanto OpenDialog
                    mostraba el directorio default ( ultima ruta a la cual se accedio). Al migrar a XE5 ese error del
                    compilador se corrigio.
                    Sin embargo, los usuarios ya estan acostumbrados a la operacion anterior. La cual resulta
                    util al exportar a un directorio distinto al de plantillas e importar ese mismo reporte en otra
                    empresa, pues al realizar el paso de importacion OpenDialog se coloca en la ultima ruta a la cual se accedio.
                    Por lo anto se decide omitir esta asignacion para que el directorio de inicio sea el mismo que en en versiones
                    anteriores a 2015.***  }
           {if InitialDir = '' then
           begin
                InitialDir := sDirPlantilla;
           end;}
           FileName := '';
           Result := Execute;
           if Result then
           begin
                InitialDir := ExtractFileDir(FileName);
           end;
      end;
 end;

 var sError : string;
     lExito :Boolean;
     i : integer;
     FLista : TStrings;
begin
if ChecaReportes then
begin
     if dmCliente.EsModoDemo then Exit;

     sDirPlantilla := zReportTools.DirPlantilla;
     if NOT DirectoryExists(sDirPlantilla) then
     begin
          sDirPlantilla := ExtractFileDir( ZetaWinAPITools.GetTempDir );
     end;
     lExito := FALSE;
     FLista := TStringList.Create;
     try
        if EjecutaOpenDialog then
        begin
             for i:= 0 to OpenDialog.Files.Count -1 do
                 if dmReportes.Importa_DevEx( OpenDialog.Files[i], sError ) then
                 begin
                      FLista.Add('El Reporte:  ' + Comillas( sError ) + '  fue importado con �xito');
                      lExito := TRUE;
                 end
                 else
                 begin
                      if sError > '' then
                         FLista.Add('El Archivo:  ' + Comillas( ExtractFileName( OpenDialog.Files[i] ) ) + '  no se import� debido al siguiente error: '+ CR_LF+sError);
                 end;
             if lExito then
             begin
                  oCursor := Screen.Cursor;
                  Screen.Cursor := crHourGlass;
                  try
                     //Carga Reportes
                     with dmReportes do
                     begin
                          with cdsReportes do
                          begin
                               Refrescar;
                               //Ordena del DataSet por el cambo RE_NOMBRE
                               //ZetaClientTools.OrdenarPor(cdsReportes,'RE_NOMBRE'); //(@am)Se elimno porque es muy lento
                               //FiltraReporte; //Ya se hace en Refrescar
                               Locate( 'RE_CODIGO', cdsEditReporte.FieldByname( 'RE_CODIGO' ).AsInteger, [] );
                          end;
                     end;
                  finally
                         Screen.Cursor := oCursor;
                  end;
             end;
             if FLista.Text > '' then
                ZetaDialogo.ZInformation( Caption, FLista.Text, 0);
        end;
     finally
            FLista.Free;
     end;
end
else
    ZetaDialogo.ZInformation( Caption,
                                    'Colocarse en Pesta�a Reportes para Importar',
                                    0 );
end;

procedure TReportes_DevEx.AgregaFavoritos;
var
   i, iReportes: integer;
begin
     if ChecaReportes then
     begin
          if NoHayDatos then
          begin
               ZetaDialogo.ZInformation( Caption,
                                    'No Hay Ning�n Reporte para Agregar Favoritos',
                                    0 )
          end
          else
          begin
              with ZetaDBGridDBTableView do
              begin
                   if Controller.SelectedRecordCount <=1 then
                   begin
                        if not EsGrupo( Controller.FocusedRow ) then
                        begin
                             if ( dmReportes.AgregaFavoritos( DataSource.DataSet ) ) then
                                ZetaDialogo.ZInformation( Caption,
                                            Format( 'Reporte "%s" Agregado a Favoritos', [ DataSource.DataSet.FieldByName( 'RE_NOMBRE' ).AsString ] ),
                                            0 );
                        end
                        else
                            ZetaDialogo.ZInformation(Caption, 'Seleccione el reporte dentro del grupo',0)
                   end
                   else
                   begin
                        //Multiple
                        iReportes:= 0;
                        for i := Controller.SelectedRecordCount -1 downto 0 do
                        begin
                              Controller.SelectedRecords[i].Focused := True;
                              DataSource.DataSet.GotoBookmark(DataController.DataSet.GetBookmark);
                              if not EsGrupo( Controller.FocusedRow ) then
                                 if ( dmReportes.AgregaFavoritos( DataSource.DataSet ) ) then
                                    Inc( iReportes );
                        end;
                        if ( iReportes > 0 ) then
                           ZetaDialogo.ZInformation( Caption,
                                            Format('%d Reportes Agregados a Favoritos', [ iReportes ] ),
                                            0 );
                   end;
              end;
          end;
     end
     else
        ZetaDialogo.ZInformation( Caption,
                                    'Colocarse en Pesta�a Reportes para Agregar a Favoritos',
                                    0 );
end;

procedure TReportes_DevEx.Imprimir;
begin
     dmReportes.SoloLectura := TRUE;
     dmReportes.SoloImpresion := TRUE;
     Modificar;
     dmReportes.SoloLectura := FALSE;
     dmReportes.SoloImpresion := FALSE;
end;

function TReportes_DevEx.ConvierteATDBGrid (DevEx_TableView: TcxGridDBTableView): TDBGrid;
var
i: Integer;
oGrid: TDBGrid;
begin
//Creamos un TDBGrid Virtual
          oGrid := TDBGrid.Create(Self);
          for i := 0 to DevEx_TableView.ColumnCount -1 do
          begin

               with oGrid.Columns.Add do
               begin
                    Visible       := DevEx_TableView.Columns[ i ].Visible;
                    FieldName     := DevEx_TableView.Columns[ i ].DataBinding.FieldName;
                    Title.Caption := DevEx_TableView.Columns[ i ].Caption;
               end;
          end;
          //Para que tome el dataset Filtrado
          DevEx_TableView.DataController.Filter.AutoDataSetFilter:=True;
          oGrid.DataSource :=  DevEx_TableView.DataController.DataSource;
          result := oGrid;
end;

procedure TReportes_DevEx.Exportar;
var
    lExporta: Boolean;
    //DevEx
    oGrid: TDBGrid;
begin
     //lExporta:= FALSE;
     oGrid := nil;
     if ChecaFavoritos then
     begin
          lExporta:= TRUE;
          if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
          begin
               try
                  oGrid := ConvierteATDBGrid ( ZetaDBGridDBTableView_Favoritos );
                  if oGrid <> nil then
                     ExportarEspecial(oGrid);
               finally
                    oGrid.Free;
                    ZetaDBGridDBTableView_Favoritos.DataController.Filter.AutoDataSetFilter:=False;
                    Refresh;
               end;
          end;
     end
     else if ChecaSuscripciones then
     begin
          lExporta:= TRUE;
          if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
          begin
               try
                  oGrid := ConvierteATDBGrid ( ZetaDBGridDBTableView_Suscripciones );
                  if oGrid <> nil then
                     ExportarEspecial(oGrid);
               finally
                    oGrid.Free;
                    ZetaDBGridDBTableView_Suscripciones.DataController.Filter.AutoDataSetFilter:=False;
                    Refresh;
               end;
          end;
     end
     else
     begin
          lExporta:= TRUE;
          if zConfirm( 'Exportar...', '� Desea exportar a Excel la informaci�n de ' + Caption + ' ?', 0, mbYes ) then
          begin
               try
                  oGrid := ConvierteATDBGrid ( ZetaDBGridDBTableView );
               if oGrid <> nil then
                  ExportarEspecial(oGrid);
               finally
                    oGrid.Free;
                    ZetaDBGridDBTableView.DataController.Filter.AutoDataSetFilter:=False;
                    Refresh;
               end;
          end;
     end;
     if not lExporta then
        zInformation( 'Exportar...',Format( 'La Pantalla %s no Contiene Ning�n Grid para Exportar', [Caption] ), 0);

end;


function TReportes_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
      Result := not ChecaFavoritos;
      if Result then
      begin
           Result := not ChecaSuscripciones;
           if Result then
           begin
                Result := NOT dmCliente.ModoSuper;
                if Result then
                begin
                     Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_ALTA );
                     if not Result then
                        sMensaje := 'Usuario no Posee Derecho para Agregar en esta Clasificaci�n';
                end
                else sMensaje := 'No se Pueden Agregar Reportes en M�dulo Supervisores';
           end
           else
             sMensaje := '� No se Pueden Agregar Reportes desde Suscripciones !';
      end
      else
          sMensaje := '� No se Pueden Agregar Reportes desde Favoritos !';
end;

function TReportes_DevEx.PuedeBorrar( var sMensaje: String ): Boolean;
var
bHayDatos:Boolean;
begin
     //Inicializacion
     bHayDatos:= True;
     Result := False;
     //Checar que en el Grid hay Datos
     if ChecaFavoritos then
     begin
          if NoHayDatosGridFavoritos then
          begin
               Result := FALSE;
               bHayDatos:= False;
               sMensaje := 'No Hay Ning�n Reporte para Quitar de Favoritos';
          end;
     end
     else if ChecaSuscripciones then
     begin
          if NoHayDatosGridSuscripciones then
          begin
               Result := FALSE;
               bHayDatos:= False;
               sMensaje := 'No Hay Ning�n Reporte para Quitar de Suscripciones';
          end;
     end
     else
         bHayDatos:= True;

     //Si hay datos para borrar
     if bHayDatos then
     begin
          Result := ChecaFavoritos;
          if not Result then
          begin
               Result := NOT dmCliente.ModoSuper;
               if Result then
               begin
                  Result := ChecaSuscripciones;
                  if not Result then
                  begin
                    Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_BAJA );
                    if not Result then
                       sMensaje := 'Usuario no Posee Derecho para Borrar en esta Clasificaci�n';
                  end;
               end
               else
                   sMensaje := 'No se Pueden Borrar Reportes en M�dulo Supervisores';
          end;
     end;
end;

function TReportes_DevEx.PuedeModificar( var sMensaje: String ): Boolean;
var
   //iDerecho:Integer;
   bHayDatos:Boolean;
begin
     //Inicializacion
     bHayDatos:= True;
     Result := False;
     //Checar que en el Grid hay Datos
     if ChecaFavoritos then
     begin
          if NoHayDatosGridFavoritos then
          begin
               Result := FALSE;
               bHayDatos:= False;
               sMensaje := 'No Hay Ning�n Reporte para Modificar en Favoritos';
          end;
     end
     else if ChecaSuscripciones then
     begin
          if NoHayDatosGridSuscripciones then
          begin
               Result := FALSE;
               bHayDatos:= False;
               sMensaje := 'No Hay Ning�n Reporte para Modificar en Suscripciones';
          end;
     end
     else
         bHayDatos:= True;

     //Si hay Datos
     if bHayDatos then
     begin
          Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_CAMBIO );
          //Si el ususario no tiene derechos de modificar la clasificacion
          if not Result then
          begin
               //Result := CheckUnDerecho( iDerecho, K_DERECHO_CONSULTA );
               Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_CONSULTA );
               {Si tiene derechos de consulta abre el wizard como solo lectura, para que el usuario pueda
               imprimir o visualizar el reporte.}
               if Result then
               begin
                    dmReportes.SoloImpresion := TRUE;
                    dmReportes.SoloLectura := TRUE;
               end
               else
                   sMensaje := 'Usuario no Posee Derecho para Modificar en esta Clasificaci�n';
          end;
     end;
end;


function TReportes_DevEx.PuedeImprimir( var sMensaje: String ): Boolean;
var
   bHayDatos:Boolean;
begin
      //Inicializacion
     bHayDatos:= True;
     Result := False;
     //Checar que en el Grid hay Datos
     if ChecaFavoritos then
     begin
          if NoHayDatosGridFavoritos then
          begin
               Result := FALSE;
               bHayDatos:= False;
               sMensaje := 'No Hay Ning�n Reporte para Imprimir en Favoritos';
          end;
     end
     else if ChecaSuscripciones then
     begin
          if NoHayDatosGridSuscripciones then
          begin
               Result := FALSE;
               bHayDatos:= False;
               sMensaje := 'No Hay Ning�n Reporte para Imprimir en Suscripciones';
          end;
     end
     else
         bHayDatos:= True;

     //Si hay Datos
     if bHayDatos then
     begin
          Result := CheckUnDerecho( GetDerechosClasifi, K_DERECHO_IMPRESION );
          if Result then
          begin
               if NoHayDatos then
               begin
                    Result := FALSE;
                    sMensaje := 'No Hay Ning�n Reporte para Imprimir';
               end;
          end
          else
              sMensaje := 'Usuario no Posee Derecho para Imprimir en esta Clasificaci�n';
     end;
end;

procedure TReportes_DevEx.FiltroTab( ZetaDBGridDBTableView_Temp:TcxGridDBTableView; ColTab: TcxGridDBColumn);
var FilterString: String;
begin
          FilterString := IntToStr( dmCliente.GetDatosUsuarioActivo.Codigo );
          with ZetaDBGridDBTableView_Temp.DataController do
          begin
               BeginUpdate;
               Filter.Active := False;
               with Filter.Root do
               begin
                    Clear;
                    AddItem (ColTab, foEqual,FilterString, FilterString);
               end;
               Filter.Active := True;
               EndUpdate;
          end;
end;

procedure TReportes_DevEx.FiltraReporte;
var sTexto:String;
procedure FiltroUsusario ( ZetaDBGridDBTableView_Temp:TcxGridDBTableView);
begin
     with ZetaDBGridDBTableView_Temp.DataController do
     begin
          with  Filter do
          begin
               BeginUpdate;
               Active := False;
               sTexto := EBuscaNombre.Text;
               if eTipoColumnaReporte( cbTipoBusqueda.ItemIndex ) = tcCodigo then
                  sTexto := EBuscaNombre.Text + '%'
               else
                  sTexto := '%'+ EBuscaNombre.Text + '%';
               Root.AddItem (FCampo, foLike,sTexto, sTexto);
               Active := True;
               EndUpdate;
          end;
     end;
end;

begin
     {***Aplica Filtro***}
     if ChecaFavoritos then
     begin
          with ZetaDBGridDBTableView_Favoritos.DataController do
          begin
               {***Agregar filtro de favoritos Simpre ***}
               FiltroTab(ZetaDBGridDBTableView_Favoritos, US_FAVORITO_FAVORITOS); //Aqui limpia los filtros
               {***Agregar filtro del usuario si hay texto en la caja***}
               if (Length(EBuscaNombre.Text)>0) and BBusca_DevEx.Down and (FCampo <> nil) then
                  FiltroUsusario(ZetaDBGridDBTableView_Favoritos);
          end;
     end
     else if ChecaSuscripciones then
     begin
          with ZetaDBGridDBTableView_Suscripciones.DataController do
          begin
               {***Agregar filtro de favoritos Simpre ***}
               FiltroTab(ZetaDBGridDBTableView_Suscripciones, US_SUSCRITO_SUSCRIP); //Aqui limpia los filtros
               {***Agregar filtro del usuario si hay texto en la caja***}
               if (Length(EBuscaNombre.Text)>0) and BBusca_DevEx.Down and (FCampo <> nil) then
                  FiltroUsusario(ZetaDBGridDBTableView_Suscripciones);
          end;
     end
     else
     begin
         with ZetaDBGridDBTableView.DataController do
         begin
              with Filter do
              begin
                   BeginUpdate;
                   Active := False;
                   Root.Clear;
                   EndUpdate;
                   if (Length(EBuscaNombre.Text)>0) and BBusca_DevEx.Down and (FCampo <> nil) then
                      FiltroUsusario(ZetaDBGridDBTableView);
              end;
         end;
     end
end;

procedure TReportes_DevEx.DoLookup;
begin
     if EBuscaNombre.Focused then
     begin
          BBusca_DevEx.Down := NOT BBusca_DevEx.Down;
          FiltraReporte;
     end
     else EBuscaNombre.SetFocus;
end;

{******Eventos*******}
procedure TReportes_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := False;
     {$ifdef ADUANAS}
     HelpContext := H_EXPLORADOR_DE_REPORTES;
     {$ELSE}
     if dmCliente.ModoSuper then
        HelpContext := 50001
     else if dmCliente.ModoLabor then
        HelpContext := 5005101
     else
         HelpContext := H5005101_Explorador_reportes_DevEx;
         //HelpContext := H50051_Explorador_reportes; //old
     {$ENDIF}
     ListaSmallImages.ResourceLoad(rtBitmap, 'EXPLORERSMALL', clOlive);

     //DevEx: Se crea la Lista de clasificaciones permitidas para el modulo
     dmReportes.ClasificacionesPermitidas := TStringList.Create;

     {$ifdef RDD}
     ZetaDBGridDBTableView.Columns [RE_TABLA.Index].DataBinding.FieldName := 'RE_TABLA'; //K_COL_TABLA
     ZetaDBGridDBTableView_Favoritos.Columns [RE_TABLA_FAVORITOS.Index].DataBinding.FieldName := 'RE_TABLA';
     ZetaDBGridDBTableView_Suscripciones.Columns [RE_TABLA_SUSCRIP.Index].DataBinding.FieldName := 'RE_TABLA';
     {$else}
     ZetaDBGridDBTableView.Columns [RE_TABLA.Index].DataBinding.FieldName := 'RE_ENTIDAD';
     ZetaDBGridDBTableView_Favoritos.Columns [RE_TABLA_FAVORITOS.Index].DataBinding.FieldName := 'RE_ENTIDAD';
     ZetaDBGridDBTableView_Suscripciones.Columns [RE_TABLA_SUSCRIP.Index].DataBinding.FieldName := 'RE_ENTIDAD';
     {$endif}

     {$ifdef SELECCION}
     {
     lbClasificacion.Visible := FALSE;
     CBTablas.Visible := FALSE;
     lbBusca.Left := 5;
     EBuscaNombre.Left := 40;
     BBusca.Left := 165;
     }
     {$endif}

     {$ifdef VISITANTES}
     HelpContext := H_VISMGR_REPORTES;
     {$endif}

     {$ifdef WORKFLOWCFG}
     HelpContext := H_WORKFLOWCFG_CONSULTAS_REPORTES;
     {$endif}
     //(@am): Inicializa Tipo de busqueda en campo NOMBRE.
     cbTipoBusqueda.Indice := 0;
end;

function TReportes_DevEx.RealField(const FieldName: String): Boolean;
var
  findField: TField;
begin
  Result := false; {Posit failure}
  findField := nil;
  findField := dmReportes.cdsReportes.FindField(FieldName);
  Result := (Assigned(findField));
end;

//@(am): Si el query no trae el campo US_CORTO. Asigna US_CODIGO a las columnas de "Modifico"
procedure TReportes_DevEx.ReasignaCampoUsuario;
begin
     ZetaDBGridDBTableView.Columns [US_DESCRIP.Index].DataBinding.FieldName := 'US_CODIGO';
     ZetaDBGridDBTableView_Favoritos.Columns [US_DESCRIP_FAVORITOS.Index].DataBinding.FieldName := 'US_CODIGO';
     ZetaDBGridDBTableView_Suscripciones.Columns [US_DESCRIP_SUSCRIP.Index].DataBinding.FieldName := 'US_CODIGO';
end;

procedure TReportes_DevEx.ConexionDatos;
var
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;

     //Genera la lista de clasificaciones Permitidas
     dmReportes.ClasificacionesPermitidas.Clear;
     dmDiccionario.GetListaClasifi( dmReportes.ClasificacionesPermitidas );

     //Eliminar las clasificaciones que no tienen derecho de consulta
     EliminaClasificaciones;
     with dmReportes do
     begin
          cdsReportes.Conectar;
          try
             cdsReportes.DisableControls;
             //(@am): Reasigna el campo a la columna "Modifico"
             if not dmReportes.RealField('US_DESCRIP_SERVIDOR') then   //if not RealField('US_DESCRIP_SERVIDOR')then
                ReasignaCampoUsuario;
             DataSource.DataSet := cdsReportes; //Se cambio la asignacion al final para minimizar el tiempo.
          finally
             cdsReportes.EnableControls;
             Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TReportes_DevEx.FormShow(Sender: TObject);
begin

      {***(@am): Se cambia la conexion de los datos a este evento para que solo se ejecute una vez ya que
                cuesta mucho hacer la reconexion porque esta pantalla maneja mucha informacion.
                Seria mejor activar el GridMode, sin embargo el filtrado de las pantallas de favoritos
                y Suscripciones se realiza con la funcionalidad de filtrado del grid, la cual no funciona
                al activar GridMode.***}
     ConexionDatos;
     inherited;
     DefineColAgrupacion;
     {***El Filtering se bloquea en tiempo diseno para las siguientes columnas:
         =Grid Reportes=
         RE_TIPO_DESCRIP
         US_FAVORITOS_DESCRIP
         US_SUSCRITO_DESCRIP
         =Grid Favoritos=
         RE_TIPO_DESCRIP_FAVORITOS
         =Grid Suscripciones=
         RE_TIPO_DESCRIP_SUSCRIP***}

     //Por defecto se selecciona el TAB de FAVORITOS
     Reportes_PC.ActivePage := TabFavoritos;
     //Se valida si FAVORITOS tiene datos.
     if NoHayDatosGridFavoritos then
     begin
           Reportes_PC.ActivePage := TabReportes;
           FActiveTab := TabReportes;
           //Selecciona el primer record del Grid Reportes
           if not NoHayDatos then
           begin
                ZetaDBGridDBTableView.ViewData.Records [0].Focused := True;
                ZetaDBGridDBTableView.ViewData.Records [0].Selected := True;
           end;
     end;
     AplicaBestFit;

     CreaColumaSumatoria(RE_NOMBRE,0,'',SkCount,ZetaDBGridDBTableView);
     CreaColumaSumatoria(RE_NOMBRE_FAVORITOS,0,'',SkCount,ZetaDBGridDBTableView_Favoritos);
     CreaColumaSumatoria(RE_NOMBRE_SUSCRIP,0,'',SkCount,ZetaDBGridDBTableView_Suscripciones);
end;

// PopUp Seleccion multiple
procedure TReportes_DevEx.mSeleccionClick(Sender: TObject);
begin
     inherited;
     mSeleccion.Checked := NOT mSeleccion.Checked;
     if mSeleccion.Checked then
     begin
          if ChecaFavoritos then
          begin
               ZetaDBGridDBTableView.OptionsSelection.MultiSelect := False;
               ZetaDBGridDBTableView_Favoritos.OptionsSelection.MultiSelect := True;
               ZetaDBGridDBTableView_Suscripciones.OptionsSelection.MultiSelect := False;
          end
          else if ChecaSuscripciones then
          begin
               ZetaDBGridDBTableView.OptionsSelection.MultiSelect := False;
               ZetaDBGridDBTableView_Favoritos.OptionsSelection.MultiSelect := False;
               ZetaDBGridDBTableView_Suscripciones.OptionsSelection.MultiSelect := True;
          end
          else
          begin
               ZetaDBGridDBTableView.OptionsSelection.MultiSelect := True;
               ZetaDBGridDBTableView_Favoritos.OptionsSelection.MultiSelect := False;
               ZetaDBGridDBTableView_Suscripciones.OptionsSelection.MultiSelect := False;
          end;
     end
     else
     begin
          ZetaDBGridDBTableView.OptionsSelection.MultiSelect := False;
          ZetaDBGridDBTableView_Favoritos.OptionsSelection.MultiSelect := False;
          ZetaDBGridDBTableView_Suscripciones.OptionsSelection.MultiSelect := False;
     end;
end;

//Eventos para SpeedButtons de la barra de herramientas
procedure TReportes_DevEx.BExportarOrClick(Sender: TObject);
begin
     inherited;
     ExportaArchivo;
end;

procedure TReportes_DevEx.BFavoritosOrClick(Sender: TObject);
begin
     inherited;
     AgregaFavoritos;
end;

procedure TReportes_DevEx.BImportarOrClick(Sender: TObject);
begin
     inherited;
     ImportaArchivo;

end;
procedure TReportes_DevEx.BBuscaClick(Sender: TObject);
begin
     inherited;

     {***Enfoca Registro***}
     if ChecaFavoritos then
     begin
        if ZetaDBGridDBTableView_Favoritos.DataController.Filter.Root.Count > 1 then
        begin
            FiltroTab(ZetaDBGridDBTableView_Favoritos, US_FAVORITO_FAVORITOS);
            BBusca_DevEx.Down := False;
        end
        else
            FiltraReporte;
        if  not Assigned (ZetaDBGridDBTableView_Favoritos.Controller.FocusedRow) then
            if not NoHayDatosGridFavoritos then
               EnfocaDefault( ZetaDBGridDBTableView_Favoritos );
     end
     else if ChecaSuscripciones then
     begin
          if ZetaDBGridDBTableView_Suscripciones.DataController.Filter.Root.Count > 1 then
          begin
            FiltroTab(ZetaDBGridDBTableView_Suscripciones, US_SUSCRITO_SUSCRIP);
            BBusca_DevEx.Down := False;
          end
          else
            FiltraReporte;
          if  not Assigned (ZetaDBGridDBTableView_Suscripciones.Controller.FocusedRow) then
              if not NoHayDatosGridSuscripciones then
                 EnfocaDefault( ZetaDBGridDBTableView_Suscripciones );
     end
     else
     begin
          with ZetaDBGridDBTableView.DataController.Filter do
          begin
               if Root.Count <=0 then
                  FiltraReporte
               else
               begin
                    //Limpiar los filtros
                    BeginUpdate;
                    Active := False;
                    Root.Clear;
                    EndUpdate;
                    BBusca_DevEx.Down := False;
               end;
          end;
          if  not Assigned (ZetaDBGridDBTableView.Controller.FocusedRow) then
              if not NoHayDatosGridSuscripciones then
                 EnfocaDefault( ZetaDBGridDBTableView);
     end;
     {***Aplica BestFit***}
     AplicaBestFit;
end;

procedure TReportes_DevEx.EBuscaNombreChange(Sender: TObject);
begin
     inherited;
     BBusca_DevEx.Down := Length(EBuscaNombre.Text)>0;
     FiltraReporte;
     {***Aplica BestFit***}
     AplicaBestFit;
     {***Enfoca Registro***}
     if ChecaFavoritos then
        if  not Assigned (ZetaDBGridDBTableView_Favoritos.Controller.FocusedRow) then
            if not NoHayDatosGridFavoritos then
               EnfocaDefault( ZetaDBGridDBTableView_Favoritos )
     else if ChecaSuscripciones then
          if  not Assigned (ZetaDBGridDBTableView_Suscripciones.Controller.FocusedRow) then
              if not NoHayDatosGridSuscripciones then
                 EnfocaDefault( ZetaDBGridDBTableView_Suscripciones )
     else
          if  not Assigned (ZetaDBGridDBTableView.Controller.FocusedRow) then
              if not NoHayDatosGrid then
                 EnfocaDefault( ZetaDBGridDBTableView);
end;
//DevEx: CustomDrawCell para las columnas que muestran imagenes
procedure TReportes_DevEx.US_SUSCRITO_DESCRIPCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sEsSuscrito: String;
   APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  //Determinamos si es Suscrito
  sEsSuscrito := AViewInfo.GridRecord.DisplayTexts [US_SUSCRITO.Index]; // K_COL_SUSCRITO

  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;
           DrawContent;
           DrawBorders;
           if sEsSuscrito = IntToStr( dmCliente.GetDatosUsuarioActivo.Codigo ) then
           begin
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_IMAGE_SUSCRITO);
           end;
        finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TReportes_DevEx.US_FAVORITOS_DESCRIPCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sEsFavorito: String;
   APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  //Determinamos si es Favorito
  sEsFavorito := AViewInfo.GridRecord.DisplayTexts [US_FAVORITO.Index];    //K_COL_FAVORITO

  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;

           DrawContent;
           DrawBorders;
           if sEsFavorito = IntToStr( dmCliente.GetDatosUsuarioActivo.Codigo ) then
           begin
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1 , Top + 1, K_IMAGE_FAVORITO);
           end;
        finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TReportes_DevEx.RE_TIPO_DESCRIPCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTipo: String;
   APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  //Determinamos si es Favorito
  sTipo := AViewInfo.GridRecord.DisplayTexts [RE_TIPO_CONDICION.Index];
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;
           DrawContent;
           DrawBorders;

           if sTipo = K_ES_LISTA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_LISTADO)
           else if sTipo = K_ES_FORMA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_FORMA)
           else if sTipo = K_ES_ETIQUETA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_ETIQUETA)
           else if sTipo = K_ES_POLIZA then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_POLIZA)
           else if sTipo = K_ES_P_CONCEPTO then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_P_CONCEPTO)
           else if sTipo = K_ES_I_RAPIDA then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_I_RAPIDA)
           else if sTipo = K_ES_FUTURO then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_FUTURO);
        finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TReportes_DevEx.RE_TIPO_DESCRIP_FAVORITOSCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTipo: String;
   APainter: TcxPainterAccess;
begin
  inherited;
  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;
  //Determinamos si es Favorito
  sTipo := AViewInfo.GridRecord.DisplayTexts [RE_TIPO_IMAGEN_FAVORITOS.Index];
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;
           DrawContent;
           DrawBorders;
           if sTipo = K_ES_LISTA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_LISTADO)
           else if sTipo = K_ES_FORMA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_FORMA)
           else if sTipo = K_ES_ETIQUETA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_ETIQUETA)
           else if sTipo = K_ES_POLIZA then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_POLIZA)
           else if sTipo = K_ES_P_CONCEPTO then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_P_CONCEPTO)
           else if sTipo = K_ES_I_RAPIDA then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_I_RAPIDA)
           else if sTipo = K_ES_FUTURO then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_FUTURO);
        finally
               Free;
        end;
  end;
  ADone := True;
end;

procedure TReportes_DevEx.RE_TIPO_DESCRIP_SUSCRIPCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   sTipo: String;
   APainter: TcxPainterAccess;
begin
  inherited;

  if not (AViewInfo.EditViewInfo is TcxCustomTextEditViewInfo) then
     Exit;

  //Determinamos si es Favorito
  sTipo := AViewInfo.GridRecord.DisplayTexts [RE_TIPO_IMAGEN_SUSCRIP.Index];
  APainter := TcxPainterAccess(TcxViewInfoAcess(AViewInfo).GetPainterClass.Create(ACanvas, AViewInfo));
  with APainter do
        begin
        try
           with TcxCustomTextEditViewInfo(AViewInfo.EditViewInfo).TextRect do
                Left := Left + SmallImageList.Width + 1;
           DrawContent;
           DrawBorders;
           if sTipo = K_ES_LISTA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_LISTADO)
           else if sTipo = K_ES_FORMA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_FORMA)
           else if sTipo = K_ES_ETIQUETA then
              with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_ETIQUETA)
           else if sTipo = K_ES_POLIZA then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_POLIZA)
           else if sTipo = K_ES_P_CONCEPTO then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_P_CONCEPTO)
           else if sTipo = K_ES_I_RAPIDA then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_I_RAPIDA)
           else if sTipo = K_ES_FUTURO then
                with AViewInfo.ClientBounds do
                   SmallImageList.Draw(ACanvas.Canvas, Left + 1, Top + 1, K_IMAGE_FUTURO);
        finally
               Free;
        end;
  end;
  ADone := True;
end;

//Show para los tabs
procedure TReportes_DevEx.TabReportesShow(Sender: TObject);
begin
  inherited;
  FActiveTab := TabReportes;
  PreparaFiltros;
  FiltraReporte;
  {***Aplica Best Fit***}
  AplicaBestFit;
  {***Desactiva Multiselect para todos los grids. Esto nos evita problemas con el SelectedRow que queremos para el grid.***}
  DesactivaMultiselect;
  {***Activar Botones***}
  ActivaBotones;
end;
//Filtro para tab Favoritos
procedure TReportes_DevEx.TabFavoritosShow(Sender: TObject);
begin
  inherited;
  FActiveTab := TabFavoritos;
  PreparaFiltros;
  FiltraReporte;
  {***Selecciona Row si es necesario y si es posible ***}
  if  not Assigned (ZetaDBGridDBTableView_Favoritos.Controller.FocusedRow) then
      if not NoHayDatosGridFavoritos then
         EnfocaDefault( ZetaDBGridDBTableView_Favoritos );
  {***Aplica Best Fit***}
  AplicaBestFit;
  {***Desactiva Multiselect para todos los grids. Esto nos evita problemas con el SelectedRow que queremos para el grid.***}
  DesactivaMultiselect;
  {***Activar Botones***}
  DesactivaBotones;
end;
//Filtro para tab Suscripciones
procedure TReportes_DevEx.TabSuscripcionesShow(Sender: TObject);
begin
  inherited;
  FActiveTab := TabSuscripciones;
  PreparaFiltros;
  FiltraReporte;
  {***Selecciona Row si es necesario y si es posible***}
  if  not Assigned (ZetaDBGridDBTableView_Suscripciones.Controller.FocusedRow) then
      if not NoHayDatosGridSuscripciones then
         EnfocaDefault( ZetaDBGridDBTableView_Suscripciones );
  {***Aplica Best Fit***}
  AplicaBestFit;
  {***Desactiva Multiselect para todos los grids. Esto nos evita problemas con el SelectedRow que queremos para el grid. ***}
  DesactivaMultiselect;
  {***ValidaRecords***}
  DesactivaBotones;
end;

//Eventos DblClick para los grids
procedure TReportes_DevEx.ZetaDBGridDBTableViewDblClick(Sender: TObject);
begin
  inherited;
     DoEdit;
end;
procedure TReportes_DevEx.ZetaDBGridDBTableViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       DoEdit;
  end;
end;

procedure TReportes_DevEx.ZetaDBGridDBTableView_FavoritosDblClick(
  Sender: TObject);
   //var AAllow: Boolean;
begin
  inherited;
  DoEdit;
end;

procedure TReportes_DevEx.ZetaDBGridDBTableView_FavoritosKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       DoEdit;
  end;
end;

procedure TReportes_DevEx.ZetaDBGridDBTableView_SuscripcionesDblClick(
  Sender: TObject);
begin
  inherited;
  DoEdit;
end;

procedure TReportes_DevEx.ZetaDBGridDBTableView_SuscripcionesKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
       DoEdit;
  end;
end;

//(@am): Eventos OnGetValueList para los grids, se eliminan opciones de filtro (Custom, Blancks, Non Blancks)
procedure TReportes_DevEx.ZetaDBGridDBTableView_SuscripcionesDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
  AIndex := AValueList.FindItemByKind(fviBlanks);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
  AIndex := AValueList.FindItemByKind(fviNonBlanks);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TReportes_DevEx.ZetaDBGridDBTableView_FavoritosDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
  AIndex := AValueList.FindItemByKind(fviBlanks);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
  AIndex := AValueList.FindItemByKind(fviNonBlanks);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

procedure TReportes_DevEx.ZetaDBGridDBTableViewDataControllerFilterGetValueList(
  Sender: TcxFilterCriteria; AItemIndex: Integer;
  AValueList: TcxDataFilterValueList);
var
  AIndex: Integer;
begin
  inherited;
  AIndex := AValueList.FindItemByKind(fviCustom);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
  AIndex := AValueList.FindItemByKind(fviBlanks);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
  AIndex := AValueList.FindItemByKind(fviNonBlanks);
  if AIndex <> -1 then
      AValueList.Delete(AIndex);
end;

//Sort para las columnas con la imagen del Tipo
procedure TReportes_DevEx.RE_TIPO_DESCRIP_SUSCRIPHeaderClick(
  Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'RE_TIPO');
end;

procedure TReportes_DevEx.RE_TIPO_DESCRIPHeaderClick(Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'RE_TIPO');
end;

procedure TReportes_DevEx.RE_TIPO_DESCRIP_FAVORITOSHeaderClick(
  Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'RE_TIPO');
end;

procedure TReportes_DevEx.US_FAVORITOS_DESCRIPHeaderClick(Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'US_FAVORITO');
end;


procedure TReportes_DevEx.US_SUSCRITO_DESCRIPHeaderClick(Sender: TObject);
begin
  inherited;
  ZetaClientTools.OrdenarPor(dmReportes.cdsReportes,'US_SUSCRITO');
end;

//Acciones locales que se asocian al PopUp
procedure TReportes_DevEx._A_ImprimirExecute(Sender: TObject);
begin
  inherited;
  DoPrint;
end;

procedure TReportes_DevEx._E_AgregarExecute(Sender: TObject);
begin
  inherited;
  DoInsert;
end;

procedure TReportes_DevEx._E_BorrarExecute(Sender: TObject);
begin
  inherited;
  DoDelete;
end;

procedure TReportes_DevEx._E_ModificarExecute(Sender: TObject);
begin
  inherited;
  DoEdit;
end;

//Eventos para prevenir la seleccion de grupos cuando se activa la seleccion multiple
procedure TReportes_DevEx.ZetaDBGridDBTableView_FavoritosCanSelectRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  inherited;
  if ARecord is TcxGridGroupRow then
    AAllow := False
  else
    AAllow := True;
end;

procedure TReportes_DevEx.ZetaDBGridDBTableViewCanSelectRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  inherited;
  if ARecord is TcxGridGroupRow then
    AAllow := False
  else
    AAllow := True;
end;

procedure TReportes_DevEx.ZetaDBGridDBTableView_SuscripcionesCanSelectRecord(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  var AAllow: Boolean);
begin
  inherited;
  if ARecord is TcxGridGroupRow then
    AAllow := False
  else
    AAllow := True;
end;
Procedure TReportes_DevEx.CreaColumaSumatoria(Columna:TcxGridDBColumn; TipoFormato: Integer ; TextoSumatoria: String ; FooterKind : tcxSummaryKind ;gridview:TcxGridDBtableView);
procedure SeleccionTexto;
const
     K_VACIO = '';
begin
     if TextoSumatoria = K_VACIO then
     begin
          case FooterKind of
               skCount: TextoSumatoria:= 'Registros: ';
               skSum: TextoSumatoria:= 'Total: ';
               skAverage: TextoSumatoria:= 'Promedio: ';
               skMax: TextoSumatoria:= ' M�ximo: ';
               skMin:  TextoSumatoria:= ' M�nimo: ';
          end;
     end;
end;
begin
if Columna.Visible then
begin
     //Si la banda no esta visible se muestra.
     if gridview.OptionsView.Footer = false then
        gridview.OptionsView.Footer := true;
      with gridview.DataController.Summary do
      begin
           BeginUpdate;
      try
         with FooterSummaryItems.Add as TcxGridDBTableSummaryItem do
         begin
              Column := Columna;
              Kind := FooterKind;
               {***Se aplica un texto Default si el parametro de texto se mando vacio.**}
              SeleccionTexto;
              {***Tipo de formato que se desee aplicar**}
              //Al agregar el tipo de formato declarando la constante en esta unidad;
                 case TipoFormato of
                      K_TIPO_MONEDA : Format := TextoSumatoria + K_ESPACIO + K_FORMATO_MONEDA;
                      K_SIN_TIPO : Format := TextoSumatoria + K_ESPACIO + K_FORMATO_DEFAULT;
                 end;
         end;
      finally
             EndUpdate;
      end;
      end;
end;
end;

procedure TReportes_DevEx.Disconnect;
begin
     {***(@am): Se sobreescribe este metodo debido para no desconectar el Grid de los datos,
                porque cuesta mucho hacer la reconexion ya que esta pantalla maneja mucha informacion.
                Seria mejor activar el GridMode, sin embargo el filtrado de las pantallas de favoritos
                y Suscripciones se realiza con la funcionalidad de filtrado del grid, la cual no funciona
                al activar el GridMode. Quedara pendiente un redise�o de la pantalla para que pueda
                trabajar en GridMode.***}
end;

end.


