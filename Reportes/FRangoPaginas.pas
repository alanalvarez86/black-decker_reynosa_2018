unit FRangoPaginas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls;

type
  TRangoPaginas = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    EPAGINAS: TLabel;
    EPaginaInicial: TEdit;
    EPAginasAl: TLabel;
    EPaginaFinal: TEdit;
    lbCopias: TLabel;
    ECopias: TEdit;
    UDCopias: TUpDown;
    OK: TBitBtn;
    Cancelar: TBitBtn;
  private
         function GetInicial : integer;
         procedure SetInicial( iInicial : integer );
         function GetFinal : integer;
         procedure SetFinal( iFinal : integer );
         function GetCopias : integer;
         procedure SetCopias( iCopias : integer );
  public
    property Inicial : Integer read GetInicial write SetInicial;
    property Final : Integer read GetFinal write SetFinal;
    property Copias : Integer read GetCopias write SetCopias;
  end;

var
  RangoPaginas: TRangoPaginas;

  function DialogoPaginas( var iInicial, iFinal, iCopias : integer ) : Boolean;

implementation

{$R *.DFM}

function DialogoPaginas( var iInicial, iFinal, iCopias : integer ) : Boolean;
begin
     if RangoPaginas = NIL then RangoPaginas := TRangoPaginas.Create( Application.MainForm );
     with RangoPaginas do
     begin
          Inicial := iInicial;
          Final := iFinal;
          Copias := iCopias;
          ShowModal;
          Result := ModalResult = mrOk;
          if Result  then
          begin
               iInicial := Inicial;
               iFinal := Final;
               iCopias := Copias;
          end;
     end;
end;

function TRangoPaginas.GetInicial : integer;
begin
     Result := StrToInt( EPaginaInicial.Text );
end;

procedure TRangoPaginas.SetInicial( iInicial : integer );
begin
     EPaginaInicial.Text := IntToStr( iInicial );
end;

function TRangoPaginas.GetFinal : integer;
begin
     Result := StrToInt( EPaginaFinal.Text );
end;

procedure TRangoPaginas.SetFinal( iFinal : integer );
begin
     EPaginaFinal.Text := IntToStr( iFinal );
end;

function TRangoPaginas.GetCopias : integer;
begin
     Result := StrToInt( ECopias.Text );
end;

procedure TRangoPaginas.SetCopias( iCopias : integer );
begin
     ECopias.Text := IntToStr( iCopias );
end;


end.
