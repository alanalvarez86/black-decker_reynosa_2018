object TipoFuentes: TTipoFuentes
  Left = 242
  Top = 122
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'C�digos de Impresi�n'
  ClientHeight = 295
  ClientWidth = 210
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TPanel
    Left = 0
    Top = 263
    Width = 210
    Height = 32
    Align = alBottom
    TabOrder = 3
    object BitBtn1: TBitBtn
      Left = 27
      Top = 3
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 107
      Top = 4
      Width = 75
      Height = 25
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object RGCaracteres: TRadioGroup
    Left = 16
    Top = 131
    Width = 177
    Height = 65
    Caption = 'Caracteres por Pulgada'
    Items.Strings = (
      '10 caracteres (Ancho normal)'
      '12 caracteres'
      '17 caracteres (Comprimido)')
    TabOrder = 1
  end
  object RGLineas: TRadioGroup
    Left = 16
    Top = 203
    Width = 177
    Height = 57
    Caption = 'L�neas por Pulgada'
    Items.Strings = (
      '6 l�neas (Default)'
      '8 l�neas')
    TabOrder = 2
  end
  object GBGenerales: TGroupBox
    Left = 16
    Top = 3
    Width = 177
    Height = 121
    Caption = 'C�digos Generales'
    TabOrder = 0
    object CBNegrita: TCheckBox
      Left = 8
      Top = 18
      Width = 81
      Height = 17
      Caption = 'Negrita'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object CBSubrayado: TCheckBox
      Left = 8
      Top = 50
      Width = 81
      Height = 17
      Caption = 'Subrayado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      TabOrder = 2
    end
    object CBItalica: TCheckBox
      Left = 8
      Top = 34
      Width = 81
      Height = 17
      Caption = 'It�lica'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 1
    end
    object CBLandscape: TCheckBox
      Left = 8
      Top = 66
      Width = 85
      Height = 17
      Caption = 'Landscape'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 5
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
    end
    object CBSaltoPag: TCheckBox
      Left = 8
      Top = 82
      Width = 125
      Height = 17
      Caption = 'Salto de P�gina'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 5
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object CBReset: TCheckBox
      Left = 8
      Top = 98
      Width = 125
      Height = 17
      Caption = 'Reset de Impresora'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = 5
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
    end
  end
end
