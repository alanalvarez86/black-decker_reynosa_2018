unit FIRPreliminar_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FImpresionRapida_DevEx, StdCtrls, ExtCtrls, dxSkinsCore,
    TressMorado2013,
  dxSkinsDefaultPainters,  dxSkinsdxBarPainter, ImgList,
  cxGraphics, dxBar, cxClasses, dxBarExtItems, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, cxButtons;

type
  TFormaIRPreliminar_DevEx = class(TForm)
    ScrollBox1: TScrollBox;
    MemoPreliminar: TMemo;
    Panel2: TPanel;
    lblPagina: TLabel;
    DevEx_BarManagerEdicion: TdxBarManager;
    BarSuperior: TdxBar;
    dxBarButton_btnPrimero: TdxBarButton;
    dxBarButton_btnSiguiente: TdxBarButton;
    dxBarButton_btnImprimir: TdxBarButton;
    cxImageList16Edicion: TcxImageList;
    dxBarButton_btnUltimo: TdxBarButton;
    dxBarControlContainerItem_Panel2: TdxBarControlContainerItem;
    procedure FormShow(Sender: TObject);
    //procedure btnSiguienteClick(Sender: TObject);
   // procedure btnPrimeroClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    //procedure btnUltimoClick(Sender: TObject);
    procedure dxBarButton_btnPrimeroClick(Sender: TObject);
    procedure dxBarButton_btnSiguienteClick(Sender: TObject);
    procedure dxBarButton_btnUltimoClick(Sender: TObject);
    procedure dxBarButton_btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
    FFastForm : TFastForm;
    procedure CentraMemo;
    procedure Primero;
    procedure Siguiente;
    procedure Ultimo;
    procedure CursorAlInicio;
    procedure MuestraPagina;
  public
    { Public declarations }
    property FastForm : TFastForm read FFastForm write FFastForm;
  end;

var
  FormaIRPreliminar_DevEx: TFormaIRPreliminar_DevEx;

implementation

{$R *.DFM}

procedure TFormaIRPreliminar_DevEx.CentraMemo;
begin
    with FastForm do
    begin
        ScrollBox1.VertScrollBar.Position := 0;
        ScrollBox1.HorzScrollBar.Position := 0;
        MemoPreliminar.Left   := 0;
        MemoPreliminar.Top    := 0;
        MemoPreliminar.Width  := ( ColumnasForma + 1 ) * 7;
        MemoPreliminar.Height := LineasForma * 14 + 25;
        if ( MemoPreliminar.Width < ScrollBox1.ClientWidth ) then
            MemoPreliminar.Left   := ( ScrollBox1.ClientWidth - MemoPreliminar.Width ) div 2 - 1;
        if ( MemoPreliminar.Height < ScrollBox1.ClientHeight ) then
            MemoPreliminar.Top := ( ScrollBox1.ClientHeight - MemoPreliminar.Height ) div 2 - 1;
    end;
end;

procedure TFormaIRPreliminar_DevEx.FormShow(Sender: TObject);
begin
    Primero;
end;

{procedure TFormaIRPreliminar_DevEx.btnUltimoClick(Sender: TObject);
begin
     Ultimo;
end;

procedure TFormaIRPreliminar_DevEx.btnSiguienteClick(Sender: TObject);
begin
    Siguiente;
end;}

procedure TFormaIRPreliminar_DevEx.Primero;
begin
  FastForm.IniciaPreliminar;
  CentraMemo;
  CursorAlInicio;

  {btnSiguiente.Enabled := TRUE;
  btnPrimero.Enabled   := FALSE;
  btnUltimo.Enabled := TRUE;}

  dxBarButton_btnSiguiente.Enabled := TRUE;
  dxBarButton_btnPrimero.Enabled := FALSE;
  dxBarButton_btnUltimo.Enabled := TRUE;

  lblPagina.Tag := 1;
  MuestraPagina;
end;

procedure TFormaIRPreliminar_DevEx.Siguiente;
begin
    {btnSiguiente.Enabled := FastForm.SiguientePreliminar;
    btnUltimo.Enabled := btnSiguiente.Enabled;
    btnPrimero.Enabled := TRUE;}

    dxBarButton_btnSiguiente.Enabled := FastForm.SiguientePreliminar;
    dxBarButton_btnUltimo.Enabled := dxBarButton_btnSiguiente.Enabled;
    dxBarButton_btnPrimero.Enabled := TRUE;

    //if btnSiguiente.Enabled then
    if dxBarButton_btnSiguiente.Enabled then
    begin
         CursorAlInicio;
         lblPagina.Tag := lblPagina.Tag + 1;
         MuestraPagina;
    end;
end;

procedure TFormaIRPreliminar_DevEx.Ultimo;
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        {btnSiguiente.Enabled := FALSE;
        btnUltimo.Enabled := FALSE;
        btnPrimero.Enabled := TRUE;}

        dxBarButton_btnSiguiente.Enabled := FALSE;
        dxBarButton_btnUltimo.Enabled := FALSE;
        dxBarButton_btnPrimero.Enabled := TRUE;

        CursorAlInicio;
        lblPagina.Tag := FastForm.UltimoPreliminar;
        MuestraPagina;
     finally
            Screen.Cursor := oCursor;
     end;
end;

{procedure TFormaIRPreliminar_DevEx.btnPrimeroClick(Sender: TObject);
begin
     Primero;
end; }

procedure TFormaIRPreliminar_DevEx.FormResize(Sender: TObject);
begin
     CentraMemo;
end;

procedure TFormaIRPreliminar_DevEx.CursorAlInicio;
begin
    with MemoPreliminar do
    begin
        SelStart := 1;
        SelLength := 1;
    end;
end;

procedure TFormaIRPreliminar_DevEx.MuestraPagina;
begin
    lblPagina.Caption := IntToStr( lblPagina.Tag );
end;


procedure TFormaIRPreliminar_DevEx.dxBarButton_btnPrimeroClick(
  Sender: TObject);
begin
   Primero;
end;

procedure TFormaIRPreliminar_DevEx.dxBarButton_btnSiguienteClick(
  Sender: TObject);
begin
     Siguiente;
end;

procedure TFormaIRPreliminar_DevEx.dxBarButton_btnUltimoClick(
  Sender: TObject);
begin
     Ultimo;
end;

procedure TFormaIRPreliminar_DevEx.dxBarButton_btnImprimirClick(
  Sender: TObject);
begin
     ModalResult := mrOK;
end;

end.
