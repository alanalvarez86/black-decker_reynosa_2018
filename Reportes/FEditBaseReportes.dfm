inherited EditBaseReportes: TEditBaseReportes
  Left = 450
  Top = 129
  Caption = 'EditBaseReportes'
  ClientHeight = 377
  ClientWidth = 568
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    Width = 568
    inherited CortarBtn: TSpeedButton
      Left = 159
    end
    inherited CopiarBtn: TSpeedButton
      Left = 183
    end
    inherited PegarBtn: TSpeedButton
      Left = 207
    end
    inherited UndoBtn: TSpeedButton
      Left = 231
    end
    inherited BGuardarComo: TSpeedButton
      Left = 64
    end
    inherited BImprimir: TSpeedButton
      OnClick = BImprimirClick
    end
    inherited bbFavoritos: TSpeedButton
      Left = 93
    end
    object BPreeliminar: TSpeedButton
      Left = 30
      Top = 2
      Width = 25
      Height = 25
      Hint = 'Preliminar (Ctrl + P)'
      Flat = True
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000D000000000000DD00D000000D0FF
        FFFFFFFF0D000D000000D0FFFFFFF0000800DD000000D0FFFFFF0877808DDD00
        0000D0FFFFF0877E880DDD000000D0FFFFF07777870DDD000000D0FFFFF07E77
        870DDD000000D0FFFFF08EE7880DDD000000D0FFFFFF087780DDDD000000D0FF
        FFFFF0000DDDDD000000D0FFFFFFFFFF0DDDDD000000D0FFFFFFF0000DDDDD00
        0000D0FFFFFFF070DDDDDD000000D0FFFFFFF00DDDDDDD000000DD00000000DD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
      ParentShowHint = False
      ShowHint = True
      OnClick = BPreeliminarClick
    end
    object bbSuscripcion: TSpeedButton
      Left = 122
      Top = 2
      Width = 25
      Height = 25
      Hint = 'Suscribirse a un Reporte'
      Flat = True
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
        0000377777777777777707FFFFFFFFFFFF70773FF33333333F770F77FFFFFFFF
        77F07F773FFFFFFF77F70FFF7700000000007F337777777777770FFFFF0FFFFF
        FFF07F333F7F3FFFF3370FFF700F0000FFF07F3F777F777733370F707F0FFFFF
        FFF07F77337F3FFFFFF7007EEE0F000000F077FFFF7F777777370777770FFFFF
        FFF07777777F3FFFFFF7307EEE0F000000F03773FF7F7777773733707F0FFFFF
        FFF03337737F3FFF33373333700F000FFFF03333377F77733FF73333330FFFFF
        00003333337F3FF377773333330F00FF0F033333337F77337F733333330FFFFF
        00333333337FFFFF773333333300000003333333337777777333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = bbSuscripcionClick
    end
  end
  inherited PageControl: TPageControl
    Width = 568
    Height = 294
    inherited tsGenerales: TTabSheet
      inherited GBParametrosEncabezado: TGroupBox
        Top = 45
        Height = 190
        inherited lbNombreReporte: TLabel
          Top = 47
        end
        inherited Label17: TLabel
          Top = 77
        end
        inherited Label3: TLabel
          Top = 107
        end
        inherited lbCodigoReporte: TLabel
          Top = 22
        end
        inherited RE_CODIGO: TZetaDBTextBox
          Top = 18
        end
        object lbTablaPrincipal: TLabel [5]
          Left = 61
          Top = 154
          Width = 73
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tabla Principal:'
        end
        object BTablaPrincipal: TSpeedButton [6]
          Left = 444
          Top = 148
          Width = 25
          Height = 25
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000010000000000000000000
            80000080000000808000800000008000800080800000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333300000033330000000FFFF033330FFFF00F00F033330F00F00FFFF033330F
            FFF00F00F039330F00F00FFFF033930FFFF00F00F099990F00F00FFFF033930F
            FFF00F00F039330F00F00FFFF033330FFFF00CCCC033330CCCC00CCCC033330C
            CCC0000000333300000033333333333333333333333333333333}
          OnClick = BTablaPrincipalClick
        end
        object RE_ENTIDAD: TZetaDBTextBox [7]
          Left = 140
          Top = 152
          Width = 301
          Height = 17
          AutoSize = False
          Caption = 'RE_ENTIDAD'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RE_ENTIDAD'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        inherited RE_CANDADOlbl: TLabel
          Top = 131
        end
        object lbSubordinados: TLabel [9]
          Left = 174
          Top = 131
          Width = 144
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mostrar solo mis subordinados:'
          Visible = False
        end
        inherited RE_NOMBRE: TDBEdit
          Top = 43
        end
        inherited RE_TITULO: TDBMemo
          Top = 67
        end
        inherited RE_CLASIFI: TComboBox
          Top = 103
        end
        inherited RE_CANDADO: TDBCheckBox
          Top = 129
        end
        object RE_IFECHA: TDBCheckBox
          Left = 324
          Top = 129
          Width = 18
          Height = 17
          DataField = 'RE_IFECHA'
          DataSource = DataSource
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
          Visible = False
        end
      end
    end
    inherited tsOrden: TTabSheet
      inherited GroupBox2: TGroupBox
        Height = 209
        inherited LbOrden: TZetaSmartListBox
          Height = 186
        end
      end
      inherited BAgregaOrden: TBitBtn
        Top = 219
      end
      inherited BBorraOrden: TBitBtn
        Top = 219
      end
      inherited GroupBoxOrden: TGroupBox
        Height = 173
        TabOrder = 4
        inherited eFormulaOrden: TMemo
          OnExit = eFormulaOrdenExit
        end
        inherited ETituloOrden: TEdit
          OnExit = eFormulaOrdenExit
        end
        object RGOrden: TRadioGroup
          Left = 49
          Top = 84
          Width = 170
          Height = 73
          Caption = 'Orden'
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Ascendente'
            'Descendente')
          TabOrder = 3
          OnClick = RGOrdenClick
        end
      end
      object BAgregaFormulaOrden: TBitBtn
        Tag = 1
        Left = 34
        Top = 248
        Width = 150
        Height = 25
        Caption = 'Agrega &F'#243'rmula'
        TabOrder = 3
        OnClick = BAgregaOrdenClick
        Glyph.Data = {
          42010000424D4201000000000000760000002800000011000000110000000100
          040000000000CC00000000000000000000001000000010000000000000000000
          BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
          DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
          DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
          0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
          D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
          D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
          0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
          DDDDD0000000}
      end
    end
    inherited tsFiltros: TTabSheet
      inherited PageControlFiltros: TPageControl
        Left = 36
        inherited tsFiltroFechas: TTabSheet
          inherited GBCampoFecha: TGroupBox [2]
          end
          inherited GBRangoLista: TGroupBox [3]
          end
          inherited GBRangoEntidad: TGroupBox [4]
          end
          inherited GBAbierto: TGroupBox [5]
          end
          inherited RGBooleano: TGroupBox [6]
          end
          inherited GroupBox1: TGroupBox [7]
          end
          inherited BAgregaFiltro: TBitBtn [8]
          end
          inherited BBorraFiltro: TBitBtn [9]
          end
          inherited EFormulaFiltro: TEdit [10]
          end
        end
      end
    end
    object tsImpresora: TTabSheet
      Caption = 'Impresora'
      ImageIndex = 4
      object GroupBox3: TGroupBox
        Left = 56
        Top = 216
        Width = 449
        Height = 41
        TabOrder = 0
        object bDisenador: TSpeedButton
          Left = 384
          Top = 16
          Width = 25
          Height = 25
          Hint = 'Abrir la plantilla con el Dise'#241'ador de Formas'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
            7777777777770000007777747770000000077774477033333000444444700000
            003077744770F7F7F7007774777000000780777777770FFFF007770000070F88
            F077770FFF0770FFFF0700000F07770000070FFF0F07777777770F8F00077777
            77770F8F0077777777770FF00777777777770000777777777777}
          ParentShowHint = False
          ShowHint = True
        end
        object cbBitacora: TCheckBox
          Left = 8
          Top = 16
          Width = 209
          Height = 17
          Caption = 'Incluir Sentencia de SQL en Bit'#225'cora'
          TabOrder = 0
        end
      end
    end
    object tsParametro: TTabSheet
      Caption = 'Par'#225'metros'
      ImageIndex = 3
      object BArribaParametro: TZetaSmartListsButton
        Left = 211
        Top = 23
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
          3333333333090333333333333309033333333333330903333333333333090333
          3333333333090333333333300009000033333330999999903333333309999903
          3333333309999903333333333099903333333333309990333333333333090333
          3333333333090333333333333330333333333333333033333333}
        Tipo = bsSubir
        SmartLists = SmartListParametros
      end
      object BAbajoParametro: TZetaSmartListsButton
        Left = 211
        Top = 48
        Width = 25
        Height = 25
        Enabled = False
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
          3333333333303333333333333309033333333333330903333333333330999033
          3333333330999033333333330999990333333333099999033333333099999990
          3333333000090000333333333309033333333333330903333333333333090333
          3333333333090333333333333309033333333333330003333333}
        Tipo = bsBajar
        SmartLists = SmartListParametros
      end
      object GroupBox13: TGroupBox
        Left = 5
        Top = 19
        Width = 199
        Height = 209
        Caption = 'Par'#225'metros'
        TabOrder = 0
        object LBParametros: TZetaSmartListBox
          Left = 5
          Top = 15
          Width = 188
          Height = 186
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object BAgregaParametro: TBitBtn
        Left = 5
        Top = 234
        Width = 115
        Height = 25
        Caption = '&Agrega Par'#225'metro'
        TabOrder = 2
        OnClick = BAgregaParametroClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BBorraParametro: TBitBtn
        Left = 120
        Top = 234
        Width = 115
        Height = 25
        Caption = '&Borra Par'#225'metro'
        Enabled = False
        TabOrder = 3
        OnClick = BBorraParametroClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object GBPropiedadesParametro: TGroupBox
        Left = 244
        Top = 19
        Width = 305
        Height = 142
        Caption = 'Propiedades del Par'#225'metro:'
        TabOrder = 1
        object LTituloParametro: TLabel
          Left = 40
          Top = 24
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'T'#237'tulo:'
          Enabled = False
        end
        object lFormulaParametro: TLabel
          Left = 44
          Top = 48
          Width = 27
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor:'
          Enabled = False
        end
        object ETituloParametro: TEdit
          Left = 76
          Top = 20
          Width = 197
          Height = 21
          Enabled = False
          MaxLength = 30
          TabOrder = 0
          OnExit = ParametrosClick
        end
        object EFormulaParametro: TMemo
          Left = 76
          Top = 48
          Width = 197
          Height = 81
          Enabled = False
          MaxLength = 255
          ScrollBars = ssVertical
          TabOrder = 1
          OnExit = ParametrosClick
        end
        object bFormulaParametro: TBitBtn
          Left = 276
          Top = 104
          Width = 25
          Height = 25
          Hint = 'Editor de F'#243'rmulas'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = bFormulaParametroClick
          Glyph.Data = {
            42010000424D4201000000000000760000002800000011000000110000000100
            040000000000CC00000000000000000000001000000010000000000000000000
            BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDD1
            DDDDDDDDDDDDD0000000DD818DDDD444DD44D0000000DD181DDD44DD4488D000
            0000D81D1DD44DDDD4DDD0000000118D1DD44DDDD4DDD0000000D18D18D44DDD
            D48DD0000000DDDD81DD44DD4448D0000000DDDDD1DDD4448D84D0000000DDDD
            D1DDDDDDDDDDD0000000DDDDD18888888888D0000000DDDDD11111111111D000
            0000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDDDDDDD0000000DDDDDDDDDDDD
            DDDDD0000000}
        end
      end
    end
    object tsSuscripciones: TTabSheet
      Caption = 'Suscripciones'
      ImageIndex = 5
      object BtnAgregaUsuario: TBitBtn
        Left = 5
        Top = 234
        Width = 109
        Height = 25
        Caption = '&Agrega Usuario'
        TabOrder = 1
        OnClick = BtnAgregaUsuarioClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
          0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
          33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
      object BtnBorraUsuario: TBitBtn
        Left = 114
        Top = 234
        Width = 100
        Height = 25
        Caption = '&Borra Usuario'
        Enabled = False
        TabOrder = 2
        OnClick = BtnBorraUsuarioClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
          305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
          005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
          B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
          B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
          B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
          B0557777FF577777F7F500000E055550805577777F7555575755500000555555
          05555777775555557F5555000555555505555577755555557555}
        NumGlyphs = 2
      end
      object GBPropiedadesSus: TGroupBox
        Left = 239
        Top = 19
        Width = 314
        Height = 142
        Caption = 'Propiedades de la Suscripci'#243'n:'
        TabOrder = 0
        object LbFormatoRep: TLabel
          Left = 6
          Top = 108
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = 'Formato de Reportes:'
          Enabled = False
        end
        object LbDireccion: TLabel
          Left = 60
          Top = 83
          Width = 48
          Height = 13
          Alignment = taRightJustify
          Caption = 'Direcci'#243'n:'
          Enabled = False
        end
        object LbNoDatos: TLabel
          Left = 30
          Top = 60
          Width = 78
          Height = 13
          Alignment = taRightJustify
          Caption = 'Si no hay Datos:'
          Color = clBtnFace
          ParentColor = False
        end
        object lbFrecuencia: TLabel
          Left = 52
          Top = 36
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'Frecuencia:'
          Color = clBtnFace
          ParentColor = False
        end
        object US_EMAIL: TEdit
          Left = 112
          Top = 80
          Width = 180
          Height = 21
          Enabled = False
          MaxLength = 30
          TabOrder = 2
          OnExit = SuscripcionClick
        end
        object SU_FRECUEN: TZetaKeyCombo
          Left = 112
          Top = 32
          Width = 140
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnClick = SuscripcionClick
          ListaFija = lfEmailFrecuencia
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object SU_VACIO: TZetaKeyCombo
          Left = 112
          Top = 56
          Width = 140
          Height = 21
          AutoComplete = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
          OnClick = SuscripcionClick
          ListaFija = lfEmailNotificacion
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = False
        end
        object US_FORMATO: TEdit
          Left = 112
          Top = 104
          Width = 180
          Height = 21
          Enabled = False
          MaxLength = 30
          TabOrder = 3
          OnExit = SuscripcionClick
        end
      end
      object GBPersonasSus: TGroupBox
        Left = 5
        Top = 19
        Width = 208
        Height = 209
        Caption = 'Personas Suscritas'
        TabOrder = 3
        object LBPersonasSus: TZetaSmartListBox
          Left = 5
          Top = 15
          Width = 195
          Height = 186
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
  end
  inherited PanelBotones: TPanel
    Top = 324
    Width = 568
    Anchors = [akLeft, akTop, akBottom]
    DesignSize = (
      568
      34)
    inherited OK: TBitBtn
      Left = 407
    end
    inherited Cancelar: TBitBtn
      Left = 485
    end
  end
  inherited StatusBarRep: TStatusBar
    Top = 358
    Width = 568
  end
  inherited DataSource: TDataSource
    Left = 408
  end
  inherited SmartListFiltros: TZetaSmartLists
    Left = 344
  end
  inherited SmartListOrden: TZetaSmartLists
    Left = 312
  end
  object SmartListParametros: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LBParametros
    ListaEscogidos = LBParametros
    AlBajar = Intercambia
    AlSeleccionar = SmartListParametrosAlSeleccionar
    AlSubir = Intercambia
    Left = 376
  end
  object dsResultados: TDataSource
    DataSet = dmReportes.cdsResultados
    Left = 440
  end
  object SmartListSuscritos: TZetaSmartLists
    BorrarAlCopiar = False
    CopiarObjetos = False
    ListaDisponibles = LBPersonasSus
    ListaEscogidos = LBPersonasSus
    AlSeleccionar = SmartListSuscritosAlSeleccionar
    Left = 472
  end
end
