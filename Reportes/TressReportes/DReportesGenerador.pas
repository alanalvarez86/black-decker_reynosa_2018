unit DReportesGenerador;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      DReportesMail.pas                          ::
  :: Descripci�n: Programa principal de TressEMail.exe       ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$INCLUDE DEFINES.INC}
{$DEFINE MULTIPLES_ENTIDADES}
{$define CONFIDENCIALIDAD_MULTIPLE}

uses Windows, Messages, SysUtils, Classes, Graphics, ComObj,
     Controls, Forms, Dialogs, Db, DBClient,
     Variants,
     FAutoClasses,
     ZAgenteSQLClient,
     ZetaWinAPITools,
     ZetaCommonClasses,
     ZetaClientDataSet,
     ZReportTools,
     ReporteadorDD_TLB,
     Super_TLB,
     Login_TLB,
     Catalogos_TLB,
     dCliente,
     ZetaTipoEntidad
     , Sistema_TLB
     , DGlobal
     , IOUtils
     , ZetaAsciiFile
     , DZetaServerProvider
     , ZetaRegistryServer
     , ZetaCommonLists;

  type
    IdmServerCalcNominaDisp = IdmServerReporteadorDDDisp;
    {$ifdef RDD}IdmServerReportesDisp =IdmServerReporteadorDDDisp;{$endif}

type
  TLogCallBack = procedure( const sText: String ) of object;
  TdmReportGenerator = class(TDataModule)
    cdsSuscripcion: TZetaClientDataSet;
    cdsReporte: TZetaClientDataSet;
    cdsCampoRep: TZetaClientDataSet;
    cdsUsuarios: TZetaClientDataSet;
    cdsResultados: TZetaClientDataSet;
    cdsEmpleados: TZetaClientDataSet;
    cdsPlantilla: TZetaClientDataSet;
    cdsCondiciones: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FZetaProvider: TdmZetaServerProvider;
    FParams: TZetaParams;
    FParametros: TStrings;
    FCampos: TStrings;
    FOrden: TStrings;
    FFiltros: TStrings;
    FGrupos: TStrings;
    FSoloTotales: Boolean;
    FContieneImagenes: Boolean;
    FBitacora: TAsciiLog;
    FFrecuencia: Integer;
    FParamCount: Integer;
    FEmpresa: String;
    FNombreReporte: String;
    FLogCallBack: TLogCallBack;
    FCodigoReporte: Integer;
    FFiltroFormula, FFiltroDescrip : string;
    FDatosImpresion : TDatosImpresion;
    FParamValues: array[ 1..K_MAX_PARAM ] of String;
    FServerReportes:IdmServerReportesDisp;
    FServerLogin: IdmServerLoginDisp;
    FServerCatalogos: IdmServerCatalogosDisp;
    FServerReportesBDE:IdmServerReporteadorDDDisp;
    FServerSuper: IdmServerSuperDisp;
    FServerSistema: IdmServerSistemaDisp;
    FModuloAutorizado: Boolean;
    function GetServerSistema: IdmServerSistemaDisp;
    function GetServerReportes: IdmServerReportesDisp;
    function GetServerLogin: IdmServerLoginDisp;
    function GetServerCatalogos: IdmServerCatalogosDisp ;
    function GetServerReportesBDE: IdmServerReporteadorDDDisp;
    function GetServerSuper: IdmServerSuperDisp;
    function AgregaFiltroSupervisor(oSQLAgente: TSQLAgenteClient): Boolean;
    procedure AsignaLista(oLista: TStrings);
    procedure CreaBitacora;
    procedure ResetParamValues;
  protected
    { Protected declarations }
    FSQLAgente: TSQLAgenteClient;
    FAppParams: TStrings;
    FNombresImagenes : TStrings;
    FPageCount : integer;
    FUnSoloHTML : Boolean;
    FMostrarError : Boolean;
    FUsaMismoResultado: Boolean;
    FdmClienteCreado: Boolean;
    FdmGlobalCreado: Boolean;
    FGeneraListaEmpleados :Boolean;
    FCopia: String;
    FFolioCalensoli: Integer;
    FFolioCalensoliCancelado: Boolean;
    FFolioCalendario: Integer;
    FNombreCalendario: String;
    FNombre: String;
    FReporteEmpleados: Integer;
    FReporteSupervisor: Boolean;
    FSubtype: Integer;
    property Empresa: String read FEmpresa write FEmpresa;
    property Frecuencia: Integer read FFrecuencia write FFrecuencia;
    property Params: TZetaParams read FParams;
    property NombreReporte: string read FNombreReporte write FNombreReporte;
    property CodigoReporte: Integer read FCodigoReporte write FCodigoReporte;
    property SoloTotales: Boolean read FSoloTotales write FSoloTotales;
    property ContieneImagenes: Boolean read FContieneImagenes write FContieneImagenes;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
    property ServerLogin: IdmServerLoginDisp read GetServerLogin;
    property ServerCatalogo: IdmServerCatalogosDisp read GetServerCatalogos;
    property ServerReportesBDE: IdmServerCalcNominaDisp read GetServerReportesBDE;
    property ServerSuper: IdmServerSuperDisp read GetServerSuper;
    property ModuloAutorizado: Boolean read FModuloAutorizado write FModuloAutorizado;
    function EvaluaParametros( var oParams: OleVariant ): Boolean;overload;
    function GeneraSQL( var Error : widestring ): Boolean;
    function GetReportes( const iReporte: Integer ): Boolean;virtual;
    function InitAutorizacion( const eModulo: TModulos ): Boolean;
    function PreparaAgente( oParams: OleVariant ): Boolean;
    function PreparaParamsReporte: Boolean;
    procedure AsignaListas;
    procedure Log(const sTexto: String);
    procedure LogError( const sTexto: String; eTipo: eTipoBitacora );virtual;
    procedure DoAfterGetResultado( const lResultado: Boolean; const Error : string );dynamic;
    procedure DoBeforeGetResultado;dynamic;
    procedure DoOnGetResultado( const lResultado: Boolean; const Error : string );dynamic;
    procedure DoOnGetDatosImpresion;dynamic;
    procedure DoOnGetReportes( const lResultado: Boolean ); dynamic;
    function GetExtensionDef : string;dynamic;
    function PreparaPlantilla( var sError : WideString ) : Boolean;dynamic;
    procedure DesPreparaPlantilla;dynamic;
    function GetResultado(const iReporte: integer; const lUsaPlantilla: Boolean ): Boolean;
    function GetDatosImpresion: TDatosImpresion;dynamic;
    function GetEmpresa: Olevariant;virtual;
    procedure DoAfterCargaActivos;virtual;
    function RevisaUsuariosInactivos( var sMensaje: string ): Boolean;
    procedure FiltraUsuariosInactivos;
    procedure DoBeforePreparaAgente( var oParams:OleVariant );dynamic;
    function GrabaBitacoraServicio (sTexto, sMemo: String; tbTipo: etipoBitacora) : Boolean;
    procedure actualizaCalenSoli (iCAL_FOLIO: Integer; tcSECAL_ESTATUS: eSolicitudEnvios; sCAL_MENSAJE: String; bInicio: Boolean);

  public
    { Public declarations }
    property oZetaProvider: TdmZetaServerProvider read FZetaProvider;
    property AppParams: TStrings read FAppParams;
    property OnLogCallBack: TLogCallBack read FLogCallBack write FLogCallBack;

    property Campos: TStrings read FCampos;
    property Orden: TStrings read FOrden;
    property Filtros: TStrings read FFiltros;
    property Grupos: TStrings read FGrupos;
    property Parametros: TStrings read FParametros;
    property DatosImpresion: TDatosImpresion read FDatosImpresion write FDatosImpresion;
    property PageCount: integer read FPageCount write FPageCount;
    property UnSoloHTML: Boolean read FUnSoloHTML write FUnSoloHTML;
    property MostrarError: Boolean read FMostrarError write FMostrarError default FALSE;
    property GeneraListaEmpleados: Boolean read FGeneraListaEmpleados  write FGeneraListaEmpleados default false;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    function EvaluaParametros( oSQLAgente : TSQLAgenteClient;Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;overload;
    procedure SetParamValue( const iParametro: Integer; const sValor: String );
    function GetPlantilla( const Plantilla: string ): TBlobField;
    function GetLogo( const Logo: string ): TBlobField;
    function DirectorioPlantillas: string;virtual;
    procedure CancelarSolicitudesCalensoli (iCAL_FOLIO: Integer = 0; sCAL_MENSAJE: String = VACIO);

 end;

 {$IFDEF REPORTES_THREAD}
  TGeneraSQLThread = class(TThread)
  private
  {private}
     FoSQLAgente: OleVariant;
     FoParams: OleVariant;
     FError: WideString;
     FDatos: OleVariant;
     FEmpresa: OleVariant;
     FServerThread: IdmServerCalcNominaDisp;
  protected
  {protected}
  public
     constructor Create (Empresa: OleVariant; ServerThread: IdmServerCalcNominaDisp; var oSQLAgente: OleVariant; var oParams: OleVariant; var Error: WideString);
     destructor Destroy; override;
     procedure Execute; override;

     // Propiedades
     property oSQLAgente: OleVariant read FoSQLAgente write FoSQLAgente;
     property oParams: OleVariant read FoParams write FoParams;
     property Error: WideString read FError write FError;
     property Datos: OleVariant read FDatos write FDatos;
     property Empresa: OleVariant read FEmpresa write FEmpresa;
     property ServerThread: IdmServerCalcNominaDisp read FServerThread write FServerThread;
  end;
  {$ENDIF}

var
  dmReportGenerator: TdmReportGenerator;

 const
      k_file = 'D:\3win_13\Datos\Fuentes\PlantillasDatos\texto.txt';
      K_DEFAULT_NOMBRE_REPORTE = 'Reporte';
      K_ACTUALIZA_CALENSOLI_INICIO = 'UPDATE CALENSOLI SET CAL_INICIO = %s, CAL_ESTATUS = %d, CAL_MENSAJ = %s WHERE CAL_FOLIO = %d';
      K_ACTUALIZA_CALENSOLI_FIN = 'UPDATE CALENSOLI SET CAL_FIN = %s, CAL_ESTATUS = %d, CAL_MENSAJ = %s WHERE CAL_FOLIO = %d';
      K_LEE_CALENSOLI_CANCELAR = 'SELECT CAL_FOLIO FROM CALENSOLI WHERE CAL_ESTATUS = %d';

{$ifdef RW}
procedure Save( const sMsg: string );overload;
procedure Save( const iMsg: integer );overload;
{$ENDIF}


implementation

uses ZetaRegistryCliente,
     ZetaCommonTools,
     ZetaClientTools,
     ZReportConst,
     ZGlobalTress,
     ZFiltroSQLTools;

{$R *.DFM}

{$IFDEF RW}
procedure Save( const sMsg: string );
 var
    oLista: TStrings;

begin
     oLista:= TStringList.Create;
     if FileExists( k_file ) then
        oLista.LoadFromFile(  k_file );
     oLista.Add( sMsg );
     oLista.SaveToFile(  k_file );
     oLista.Free;
end;

procedure Save( const iMsg: integer );
begin
     Save('dmReportes- - ' + IntToStr(iMsg));
end;
{$ENDIF}

{ ****** TdmReportGenerator ******* }

procedure TdmReportGenerator.DataModuleCreate(Sender: TObject);
begin
     FdmClienteCreado:= FALSE;
     FdmGlobalCreado := FALSE;
     if dmCliente = NIL then
     begin
          dmCliente := TdmCliente.Create( Self );
          FdmClienteCreado:= TRUE;
     end;

     if Global = NIL then
     begin
          Global := TdmGlobal.Create;
          FdmGlobalCreado := TRUE;
     end;
        
     FSQLAgente := TSQLAgenteClient.Create;
     FParams := TZetaParams.Create;
     FAppParams := TStringList.Create;
     FCampos := TStringList.Create;
     FGrupos := TStringList.Create;
     FOrden := TStringList.Create;
     FFiltros := TStringList.Create;
     FParametros := TStringList.Create;
     FNombresImagenes := TStringList.Create;
     FUsaMismoResultado := FALSE;
     ResetParamValues;
     FZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
     FZetaProvider.EmpresaActiva := FZetaProvider.Comparte;

     // Correcci�n de Bug #16207:
     // Electrocomponentes. Error al ejecutar reporte con par�metro de tipo fecha. El proceso arroja error: 29122014 is not a valid date..
     ZetaCommonTools.Environment;

     ModuloAutorizado := TRUE;
end;

procedure TdmReportGenerator.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FNombresImagenes );
 {$ifndef TIMBRADO}
     FreeAndNil( FBitacora );
 {$endif}
     FreeAndNil( FCampos );
     FreeAndNil( FOrden );
     FreeAndNil( FGrupos );
     FreeAndNil( FFiltros );
     FreeAndNil( FParametros );
     FreeAndNil( FAppParams );
     FreeAndNil( FParams );
     FreeAndNil( FSQLAgente );

     if FdmGlobalCreado then
        FreeAndNil( Global );
     if FdmClienteCreado then
        FreeAndNil( dmCliente );

     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

function TdmReportGenerator.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;

function TdmReportGenerator.GetServerReportesBDE: IdmServerCalcNominaDisp;
begin
     Result := IdmServerCalcNominaDisp( dmCliente.CreaServidor( {$ifdef RDD}CLASS_dmServerReporteadorDD,{$ELSE}CLASS_dmServerReportes,{$ENDIF}
               FServerReportesBDE ) );
end;

function TdmReportGenerator.GetServerSuper: IdmServerSuperDisp;
begin
     Result:= IdmServerSuperDisp( dmCliente.CreaServidor( CLASS_dmServerSuper, FServerSuper ) );
end;

function TdmReportGenerator.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp( dmCliente.CreaServidor( {$ifdef RDD}
                                                                CLASS_dmServerReporteadorDD
                                                              {$ELSE}
                                                                CLASS_dmServerReportes
                                                              {$ENDIF},
                                           FServerReportes ) );
end;

function TdmReportGenerator.GetServerLogin: IdmServerLoginDisp;
begin
     Result := IdmServerLoginDisp( dmCliente.CreaServidor( CLASS_dmServerLogin, FServerLogin ) );
end;

function TdmReportGenerator.GetServerCatalogos: IdmServerCatalogosDisp;
begin
     Result := IdmServerCatalogosDisp( dmCliente.CreaServidor( CLASS_dmServerCatalogos, FServerCatalogos ) );
end;

procedure TdmReportGenerator.CreaBitacora;
begin
     if (FBitacora = NIL)
     then
     begin
          FBitacora := TAsciiLog.Create;
     end;
end;

function TdmReportGenerator.InitAutorizacion( const eModulo: TModulos ): Boolean;
begin
     try
        Autorizacion.Cargar( ServerLogin.GetAuto );
        begin
             with Autorizacion do
             begin
                  Result := OkModulo( eModulo );
             end;
        end;
     except
           on Error: Exception do
           begin
                if ModuloAutorizado then
                begin
                     ModuloAutorizado := FALSE;
                     LogError('No es posible iniciar proceso de reportes email.', tbError);
                end;
           end;
     end;
end;
{CV:21/Septiembre
Se puso el CreaBitacora en este m�todo, porque si el objeto TdmReportGenerator
o una descendencia se crea al principio de la aplicacion y se corre un segundo ejecutable,
el segundo exe marca error de "I/O File", debido a que la primera instancia abre la bitacora.}

procedure TdmReportGenerator.Log(const sTexto: String );
begin
     CreaBitacora;
     if (FBitacora <> NIL) then
     begin
          FBitacora.WriteTexto( sTexto );
          if Assigned( FLogCallBack ) then
             FLogCallBack( sTexto );
     end;
end;

procedure TdmReportGenerator.LogError(const sTexto: String; eTipo: eTipoBitacora);
begin
     GrabaBitacoraServicio ('Error al generar reporte', FiltrarTextoSQL (sTexto), eTipo);
end;

procedure TdmReportGenerator.AsignaLista( oLista: TStrings );
var
   i: Integer;
begin
     with oLista do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TCampoOpciones( Objects[ i ] ) do
               begin
                    if ( PosAgente >= 0 ) then
                    begin
                         SQLColumna := FSQLAgente.GetColumna( PosAgente );
                         TipoImp := SQLColumna.TipoFormula;
                    end;
               end;
          end;
     end;
end;

procedure TdmReportGenerator.AsignaListas;
var
   i: Integer;
begin
     AsignaLista( FCampos );
     with FGrupos do
     begin
          for i := 0 to ( Count - 1 ) do
          begin
               with TGrupoOpciones( Objects[ i ] ) do
               begin
                    AsignaLista( ListaEncabezado );
                    AsignaLista( ListaPie );
               end;
          end;
     end;
end;

procedure TdmReportGenerator.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerCatalogo.GetCondiciones( dmCliente.Empresa );
end;

function TdmReportGenerator.GetEmpresa: Olevariant;
begin
     Result := dmCliente.Empresa;
end;

function TdmReportGenerator.GetReportes( const iReporte: Integer ): Boolean;
var
   oCampoRep: OleVariant;
begin
     FCodigoReporte := iReporte;
     cdsReporte.Data := ServerReportes.GetEditReportes( GetEmpresa, iReporte, oCampoRep );
     Result := not cdsReporte.IsEmpty;
     if Result then
     begin
          cdsCampoRep.Data := oCampoRep;
          with cdsReporte do
          begin
               FNombreReporte := FieldByName( 'RE_NOMBRE' ).AsString;
               FSoloTotales := ZetaCommonTools.zStrToBool( FieldByName( 'RE_SOLOT' ).AsString );
          end;
     end;
end;

function TdmReportGenerator.EvaluaParametros( var oParams: OleVariant ): Boolean;
const
     P_TITULO = 1;
     P_FORMULA = 2;
     P_TIPO = 3;
var
   oParam, oSQLAgente: OleVariant;
   oColumna: TSQLColumna;
   i, iCount: Integer;
   sError: WideString;
   eTipo: eTipoGlobal;
begin
     // Correcci�n de Bug #16207:
     // Electrocomponentes. Error al ejecutar reporte con par�metro de tipo fecha. El proceso arroja error: 29122014 is not a valid date..
     ZetaCommonTools.Environment;

     Result := TRUE;
     FParametros.Clear;
     with cdsCampoRep do
     begin
          First;
          Filter := Format( 'CR_TIPO = %d', [ Ord( tcParametro ) ] );
          Filtered := TRUE;
          FParamCount := cdsCampoRep.RecordCount;
          while not Eof do
          begin
               AgregaListaObjeto( cdsCampoRep, FParametros );
               Next;
          end;
          Filtered := FALSE;
          Filter := Format( 'CR_TIPO <> %d', [ Ord( tcParametro ) ] );
          Filtered := TRUE;
     end;
     iCount := FParametros.Count;
     oParams := VarArrayCreate( [ 1, K_MAX_PARAM ], varVariant );
     FSQLAgente.Clear;

     if ( iCount > 0 ) then
     begin
          for i := 0 to ( iCount - 1 ) do
          begin
               AgregaSQLColumnasParam( FSQLAgente, TCampoMaster( FParametros.Objects[ i ] ) );
          end;
          oSQLAgente := FSQLAgente.AgenteToVariant;
          Result := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, Params.VarValues, sError );
          if Result then
          begin
               FSQLAgente.VariantToAgente( oSQLAgente );
               for i := 0 to ( FParametros.Count - 1 ) do
               begin
                    with TCampoMaster( FParametros.Objects[ i ] ) do
                    begin
                         oParam := VarArrayCreate( [ P_TITULO, P_TIPO ], varVariant );
                         oParam[ P_TITULO ] := Titulo;
                         if ( PosAgente >= 0 ) then
                         begin
                              oColumna := FSQLAgente.GetColumna( PosAgente );
                              eTipo := oColumna.TipoFormula;
                              case eTipo of
                                   // Correcci�n de Bug #16207:
                                   // Electrocomponentes. Error al ejecutar reporte con par�metro de tipo fecha. El proceso arroja error: 29122014 is not a valid date..
                                   tgFecha: oParam[ P_FORMULA ] := ZetaCommonTools.StrAsFecha( oColumna.Formula );

                                   tgBooleano: oParam[ P_FORMULA ] := ZetaCommonTools.zStrToBool( oColumna.Formula );
                              else
                                  oParam[ P_FORMULA ] := oColumna.Formula;
                              end;
                         end
                         else
                         begin
                              eTipo := TipoCampo;
                              oParam[ P_FORMULA ] := Formula;
                         end;
                         oParam[ P_TIPO ] := eTipo;
                         { Cargar Valores Capturados (Si hay ) }
                         iCount := i + 1;
                         if ( iCount >= Low( FParamValues ) ) and ( iCount <= High( FParamValues ) ) then
                         begin
                              if ZetaCommonTools.StrLleno( FParamValues[ iCount ] ) then
                              begin
                                   try
                                      case eTipo of
                                           tgBooleano: oParam[ P_FORMULA ] := ZetaCommonTools.zStrToBool( FParamValues[ iCount ] );
                                           tgFloat: oParam[ P_FORMULA ] := StrToFloat( FParamValues[ iCount ] );
                                           tgNumero: oParam[ P_FORMULA ] := StrToFloat( FParamValues[ iCount ] );
                                           tgFecha: oParam[ P_FORMULA ] := ZetaCommonTools.StrAsFecha( FParamValues[ iCount ] );
                                           tgTexto: oParam[ P_FORMULA ] := FParamValues[ iCount ];
                                      end;
                                   except
                                         on Error: Exception do
                                         begin
                                              Error.Message := Format( 'Error En Valor Del Par�metro %s # %d: %s', [ ZetaCommonLists.ObtieneElemento( lfTipoGlobal, Ord( eTipo ) ), iCount, Error.Message ] );
                                              raise;
                                         end;
                                   end;
                              end;
                         end;
                    end;
                    oParams[ i + 1 ] := oParam;
               end;
          end;
     end;
end;

function TdmReportGenerator.AgregaFiltroSupervisor( oSQLAgente: TSQLAgenteClient ) : Boolean;

function GetParametrosCdsEmpleados: OleVariant;
var
   FParams: TZetaParams;
begin
     FParams := TZetaParams.Create( Self );
     try
        with FParams do
        begin
             AddDate( 'Fecha', dmCliente.FechaDefault );
             AddString( 'RangoLista', '' );
             AddString( 'Condicion', '' );
             AddString( 'Filtro', '' );
             AddInteger( 'FiltroFijo', 0 );
             AddBoolean( 'LaborActivado', FALSE );
             Result := VarValues;
        end;
     finally
            FreeAndNil( FParams );
     end;
end;

begin
     Result := TRUE;

     if eClasifiReporte( cdsReporte.FieldByName( 'RE_CLASIFI' ).AsInteger ) = crSupervisor then
     begin
          {Solamente se agrega el filtro supervisor si la tabla principal del reporte en
          cuestion, tiene relacion a COLABORA. Si el reporte no tiene relacion hacia
          COLABORA, }
          {$ifdef MULTIPLES_ENTIDADES}
          if Dentro( FSQLAgente.Entidad , EntidadConCondiciones ) then
          {$else}
          if FSQLAgente.Entidad in EntidadConCondiciones then
          {$endif}
          begin
               dmCliente.Usuario := cdsSuscripcion.FieldByName( 'US_CODIGO' ).AsInteger ;
               cdsEmpleados.Data := ServerSuper.GetEmpleados( dmCliente.Empresa, GetParametroscdsEmpleados );

               oSQLAgente.AgregaColumna( 'COLABORA.CB_CODIGO', TRUE, enEmpleado, tgNumero, 30, 'CB_CODIGO' );
               try
                  AgregaSQLFiltrosEspeciales( oSQLAgente, dmCliente.ConstruyeListaEmpleados( cdsEmpleados, 'COLABORA.CB_CODIGO' ), TRUE );
               except
                     On E:Exception do
                     begin
                          Result := FALSE;
                          LogError (E.Message, tbError);
                     end;
               end;
               dmCliente.Usuario := 0;
          end;
     end
end;

function TdmReportGenerator.PreparaAgente( oParams: OleVariant ):Boolean;
var
   eTipo: eTipoCampo;
   oLista: TStrings;
   oCampo: TCampoMaster;
   QU_CODIGO: string;

 function GetCampoCB_Codigo( const iEntidad: TipoEntidad ) : string;
  var
     sTabla : string;
 begin
      //Los proyectos de Seleccion y Visitantes no deben de llegar a este punto.
      {$ifndef RDD}
      sTabla := dmDiccionario.GetNombreTabla( iEntidad );
      {$else}
      sTabla := cdsReporte.FieldByName('EN_TABLA').AsString ;
      {$endif}
      case iEntidad of
           enEmbarazo,enMedEntregada,enConsulta,enAccidente : Result := 'EXPEDIEN.CB_CODIGO';
           enPoll: Result := sTabla + '.PO_NUMERO';
           enInvitacion: Result :='INVITA.CB_CODIGO'
           else
               Result := sTabla + '.CB_CODIGO';
      end;
 end;

begin
     FSQLAgente.Clear;
     FSQLAgente.Parametros := oParams;

     DatosImpresion := GetDatosImpresion;
     FFiltroFormula := VACIO;
     FFiltroDescrip := VACIO;

     with cdsReporte do
     begin
          FSQLAgente.Entidad := TipoEntidad( FieldByName( 'RE_ENTIDAD' ).AsInteger );
          AgregaSQLFiltrosEspeciales( FSQLAgente, FieldByName( 'RE_FILTRO' ).AsString, FALSE, FParamCount );

          {Defecto #1432: Kiosco no debe de validar confidencialidad}
          {$ifdef ANTES}
          {$else}
          if Strlleno(dmCliente.Confidencialidad) then
          begin
               {$ifdef MULTIPLES_ENTIDADES}
                         if Dentro( FSQLAgente.Entidad , EntidadConCondiciones )
                      {$IFDEF RDD}
                         or zStrToBool( cdsReporte.FieldByName('EN_NIVEL0').AsString )
                      {$ENDIF}
                         then
               {$else}
                   if (FSQLAgente.Entidad in EntidadConCondiciones) then
               {$endif}
               {$ifdef CONFIDENCIALIDAD_MULTIPLE}
               begin
                    AgregaSQLFiltrosEspeciales( FSQLAgente, Format('COLABORA.CB_CODIGO > 0 or %s=0 ',[GetCampoCB_Codigo(FSQLAgente.Entidad) ]), FALSE, FParamCount );
                    AgregaSQLFiltrosEspeciales( FSQLAgente, Format('COLABORA.CB_NIVEL0 in %s or %s=0 ',[dmCliente.ConfidencialidadListaIN, GetCampoCB_Codigo(FSQLAgente.Entidad)]), TRUE, FParamCount );
               end;
               {$else}
               AgregaSQLFiltrosEspeciales( FSQLAgente, Format('COLABORA.CB_NIVEL0=''%s'' or %s=0 ',[dmCliente.Confidencialidad, GetCampoCB_Codigo(FSQLAgente.Entidad)]), FALSE, FParamCount );
               {$endif}
          end;
          {$endif}
          QU_CODIGO := FieldByName( 'QU_CODIGO' ).AsString;
          if ( Trim( QU_CODIGO ) > '' ) then
          begin
               cdsCondiciones.Refrescar;
               if cdsCondiciones.Locate( 'QU_CODIGO', VarArrayOf( [ QU_CODIGO ] ), [] ) then
               begin
                    AgregaSQLFiltrosEspeciales( FSQLAgente, cdsCondiciones.FieldByName( 'QU_FILTRO' ).AsString, FALSE, FParamCount );
               end;
          end;
     end;

     oLista := NIL;
     FCampos.Clear;
     FGrupos.Clear;
     FOrden.Clear;
     FFiltros.Clear;

     with cdsCampoRep do
     begin
          IndexFieldNames := 'RE_CODIGO;CR_TIPO;CR_POSICIO;CR_SUBPOS';
          First;
          while not Eof do
          begin
               eTipo := eTipoCampo( FieldByName( 'CR_TIPO' ).AsInteger );
               case eTipo of
                    tcCampos: oLista := FCampos;
                    tcGrupos: oLista := FGrupos;
                    tcOrden: oLista := FOrden;
                    tcEncabezado: oLista := TGrupoOpciones( FGrupos.Objects[ FieldByName( 'CR_POSICIO' ).AsInteger ] ).ListaEncabezado;
                    tcPieGrupo: oLista := TGrupoOpciones( FGrupos.Objects[ FieldByName( 'CR_POSICIO' ).AsInteger ] ).ListaPie;
                    tcFiltro: oLista := FFiltros;
                    tcParametro: oLista := FParametros;
               end;
               AgregaListaObjeto( cdsCampoRep, oLista );

               oCampo := TCampoMaster( oLista.Objects[ oLista.Count - 1 ] );
               case eTipo of

                    tcCampos:
                             begin
                                  if FGeneraListaEmpleados then
                                     AgregaSQLColumnasAlias( FSQLAgente, TCampoOpciones( oCampo ), FDatosImpresion, -1, FParamCount )
                                  else
                                      AgregaSQLColumnas( FSQLAgente, TCampoOpciones( oCampo ), FDatosImpresion, -1, FParamCount )
                             end;
                    tcEncabezado, tcPieGrupo: AgregaSQLColumnas( FSQLAgente, TCampoOpciones( oCampo ), FDatosImpresion, FieldByName('CR_POSICIO').AsInteger, FParamCount );
                    tcGrupos: AgregaSQLGrupos( FSQLAgente, TGrupoOpciones( oCampo ) );
                    tcOrden: AgregaSQLOrdenes( FSQLAgente, TOrdenOpciones( oCampo ) );
                    tcFiltro: AgregaSQLFiltros( FSQLAgente, TFiltroOpciones( oCampo ), FFiltroFormula, FFiltroDescrip );
               end;
               Next;
          end;
     end;
     Result := AgregaFiltroSupervisor( FSQLAgente );
end;

function TdmReportGenerator.PreparaParamsReporte:Boolean;
begin
     Result := TRUE;

     ZReportTools.ParametrosReportes( cdsReporte, cdsCondiciones, FParams,
                                      FFiltroFormula, FFiltroDescrip,
                                      FSoloTotales, {$ifdef TRESSEXCEL}
                                                    dmDiccionario.VerConfidencial
                                                    {$else}
                                                    TRUE
                                                    {$endif}, FALSE, FContieneImagenes );

          {28-Octubre: CV: Esta llamada se requiere por que los reportes especiales requieren
          agregar ciertos parametros especificos, al FParams. En el Servidor,
          se espera que lleguen estos parametros.}
          ZReportTools.ParametrosEspeciales( TipoEntidad( cdsReporte.FieldByName('RE_ENTIDAD').AsInteger ),
                                             FParams,
                                             FFiltros,
                                             FGrupos );


end;

function TdmReportGenerator.GeneraSQL( var Error : widestring ): Boolean;
var
   oSQLAgente, oParams: OleVariant;
   {$IFDEF REPORTES_THREAD}
   generaSQLThread: TGeneraSQLThread;
   {$ENDIF}
   sMensajeListaEmpleados: String;
begin
     oSQLAgente := FSQLAgente.AgenteToVariant;
     oParams := Params.VarValues;

     sMensajeListaEmpleados := VACIO;
     try
        with cdsResultados do
        begin
             Init;

             {$IFDEF REPORTES_THREAD}
             generaSQLThread := TGeneraSQLThread.Create (dmCliente.Empresa, ServerReportesBDE, oSQLAgente, oParams, Error);
             generaSQLThread.WaitFor;
             Data := generaSQLThread.Datos;
             {$ELSE}
             Data := ServerReportesBDE.GeneraSQL (dmCliente.Empresa, oSQLAgente, oParams, Error);
             {$ENDIF}

             {$ifdef CAROLINA}
             SaveToFile('C:\Reportes\ResEmail_Thread.XML', dfXML);
             {$endif}

             Result := not IsEmpty;
        end;
        if Result then
        begin
             {$IFDEF REPORTES_THREAD}
             FSQLAgente.VariantToAgente (generaSQLThread.oSQLAgente);
             {$ELSE}
             FSQLAgente.VariantToAgente( oSQLAgente );
             {$ENDIF}

             if not FSoloTotales then
             begin
                  FSQLAgente.OrdenaDataset( cdsResultados );
             end;
        end
        else
        begin
             if StrLleno (Error)
             and (Pos (UpperCase (K_NoHayRegistros), UpperCase (Error)) = 0)
             then
             begin
                  LogError(Error, tbError)
             end
             else
             begin
                  if (FReporteEmpleados <> 0) and (FCodigoReporte = FReporteEmpleados) then
                  begin
                       sMensajeListaEmpleados := Format (' para el reporte de lista de empleados %d', [FReporteEmpleados]);
                  end;

                  LogError ('No hay registros que cumplan con los filtros especificados' + sMensajeListaEmpleados, tbAdvertencia);
                  Error := 'No hay registros que cumplan con los filtros especificados' + sMensajeListaEmpleados;
             end;
        end;
     finally
            {$IFDEF REPORTES_THREAD}
            FreeAndNil (generaSQLThread);
            {$ENDIF}
     end;
end;

procedure TdmReportGenerator.DoAfterGetResultado( const lResultado: Boolean; const Error : string );
begin

end;

procedure TdmReportGenerator.DoBeforeGetResultado;
begin

end;

procedure TdmReportGenerator.DoOnGetResultado(const lResultado: Boolean; const Error : string );
begin

end;

procedure TdmReportGenerator.DoOnGetDatosImpresion;
begin

end;

procedure TdmReportGenerator.DoOnGetReportes( const lResultado: Boolean );
begin
end;

procedure TdmReportGenerator.DoBeforePreparaAgente( var oParams:OleVariant );
begin
		 
end;

function TdmReportGenerator.GetExtensionDef : string;
begin
     Result := '';
end;


function TdmReportGenerator.GetResultado( const iReporte : integer; const lUsaPlantilla : Boolean ): Boolean;
 var
    oParams : OleVariant;
    Error : wideString;
begin
     Result := GetReportes( iReporte );
     DoOnGetReportes(Result);
     if Result then
     begin
          dmCliente.CargaActivosTodos( Params );
          DoAfterCargaActivos;
          FParams.AddBoolean( 'RDD_APP', FALSE );
          Result := EvaluaParametros( oParams );
          if Result then
          begin
               DoBeforePreparaAgente(oParams);
               Result := PreparaAgente( oParams );
               if Result then
               begin
                    DoBeforeGetResultado;
                    if lUsaPlantilla AND ( eTipoFormato( cdsReporte.FieldByName('RE_PFILE').AsInteger ) <> tfMailMerge ) then
                    begin
                         Result := PreparaPlantilla( Error );
                    end;

                    { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
                    try
                       if Result then
                       begin
                            Result := PreparaParamsReporte;

                            if Result then
                            begin
                                 try
                                    if NOT FUsaMismoResultado then
                                       Result := GeneraSQL( Error )
                                    else
                                        Result := TRUE;

                                    // Revisar solicitudes y cancelar si as� se ha solicitado.
                                    // Son las que tienen estatus = 4.
                                    CancelarSolicitudesCalensoli;

                                    if not FFolioCalensoliCancelado then
                                       DoOnGetResultado( Result, Error )
                                    else
                                        Result := FALSE;

                                    finally
                                           DoAfterGetResultado( Result, Error );
                                    end;
                            end
                            else
                            begin
                                LogError( Format( 'Problemas al cargar plantilla del reporte #%d' + CR_LF + Error, [ iReporte ] ), tbError)
                            end;
                       end;
                    finally
                           if lUsaPlantilla AND ( eTipoFormato( cdsReporte.FieldByName('RE_PFILE').AsInteger ) <> tfMailMerge )  then
                              DespreparaPlantilla;
                    end;
               end
          end
          else
              LogError( Format( 'Problemas al evaluar los par�metros del reporte #%d', [ iReporte ] ), tbError);
     end
     else
         LogError( Format( 'El reporte #%d no existe', [ iReporte ] ), tbError);

     ResetParamValues;
end;

function TdmReportGenerator.PreparaPlantilla( var sError : WideString ) : Boolean;
begin
     Result := TRUE;
end;
                               
procedure TdmReportGenerator.DesPreparaPlantilla;
begin

end;

function TdmReportGenerator.GetDatosImpresion : TDatosImpresion;
 const
      K_CM_CODIGO = 5;
 var
    oDirDefault : string;
    sError : string;
begin
     Result := FDatosImpresion;
     with Result do
     begin
          with cdsReporte do
          begin
               if eTipoReporte(FieldByName('RE_TIPO').AsInteger) = trForma then
               begin
                    Tipo := ZReportTools.GetTipoFormatoForma(eFormatoFormas(FieldByName('RE_PFILE').AsInteger) );
               end
               else
               begin
                    Tipo := eTipoFormato(FieldByName('RE_PFILE').AsInteger);
               end;

               Archivo := FieldByName('RE_REPORTE').AsString;
               Exportacion := TransParamNomConfig( FieldByName('RE_ARCHIVO').AsString, Parametros.Count, FSQLAgente.Parametros, TRUE, FALSE );

               Grafica := 'GRAFICA.QR2';
               oDirDefault := zReportTools.DirPlantilla;

               Archivo := GetNombreArchivo( Result, sError );

               // Nombre del reporte.
               // Se usar� en caso de no tener capturado un nombre de archivo de salida.
               // Esto puede ser oom�n en los casos de reportes con salida a impresora.
               FNombre := FieldByName('RE_NOMBRE').AsString;
               if FNombre = VACIO then
                  FNombre := K_DEFAULT_NOMBRE_REPORTE;
               Exportacion := GetNombreExportacion (Result, GetExtensionDef, sError, FNombre);

               dmCliente.GetServicioReportesCorreos;
               with dmCliente.DatosServicioReportesCorreos do
               begin
                    if FCopia <> VACIO then
                    begin
                         Copia := FCopia + '\' + ExtractFileName(Exportacion);
                    end;

                    if FReporteSupervisor then
                    begin
                         Exportacion := DirectorioReportes + '\' + TPath.GetFileNameWithoutExtension(Exportacion)
                                     // Agregar c�digo de empresa para identificar los reportes.
                                     // Agregar n�mero de usuario para identificar los reportes por supervisor
                                     + '_' + dmCliente.Empresa[K_CM_CODIGO] + IntToStr (FFolioCalendario) + IntToStr (dmCliente.Usuario) + ExtractFileExt(Exportacion);
                    end
                    else if FReporteEmpleados <= 0 then
                    begin
                         Exportacion := DirectorioReportes + '\' + TPath.GetFileNameWithoutExtension(Exportacion)
                                     // Agregar c�digo de empresa para identificar los reportes.
                                     + '_' + dmCliente.Empresa[K_CM_CODIGO] + IntToStr (FFolioCalendario) + ExtractFileExt(Exportacion)
                    end
                    else
                    begin
                         Exportacion := DirectorioReportes + '\' + TPath.GetFileNameWithoutExtension(Exportacion)
                                     // Agregar c�digo de empresa para identificar los reportes.
                                     // Agregar n�mero de empleado para identificar los reportes.
                                     + '_' + dmCliente.Empresa[K_CM_CODIGO] + IntToStr (FFolioCalendario) + IntToStr (dmCliente.Empleado) + ExtractFileExt(Exportacion);
                    end;

                    FSubtype := 0;
                    if Tipo in [tfImpresora,tfHTML,tfHTM] then
                       FSubtype := 1;
                    FolioCalendario := FFolioCalendario;
                    NombreCalendario := FNombreCalendario;
               end;

               // Nombre del reporte.
               // Se usar� en caso de no tener capturado un nombre de archivo de salida.
               // Esto puede ser oom�n en los casos de reportes con salida a impresora.
               FNombre := FieldByName('RE_NOMBRE').AsString;

               Separador := Copy(FieldByName('RE_CFECHA').AsString,1,1);
               //Si la impresora es -1, siempre toma la impresora default de la maquina.
               Impresora := -1;
          end;
     end;
end;

function TdmReportGenerator.EvaluaParametros( oSQLAgente : TSQLAgenteClient;Parametros : TStrings;var sError : wideString;var oParams : OleVariant;const lMuestraDialogo : Boolean ) : Boolean;
begin
     Result := TRUE;
end;

procedure TdmReportGenerator.ResetParamValues;
var
   i: Integer;
begin
     for i := Low( FParamValues ) to High( FParamValues ) do
     begin
          FParamValues[ i ] := VACIO;
     end;
end;

procedure TdmReportGenerator.SetParamValue( const iParametro: Integer; const sValor: String );
begin
     if ( iParametro > 0 ) and ( iParametro <= K_MAX_PARAM ) then
     begin
          FParamValues[ iParametro ] := Trim( sValor );
     end;
end;


function TdmReportGenerator.GetPlantilla( const Plantilla: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Plantilla );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

function TdmReportGenerator.GetLogo( const Logo: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Logo );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

function TdmReportGenerator.DirectorioPlantillas: string;
begin
     Result := VerificaDir (Global.GetGlobalString (K_GLOBAL_DIR_PLANT));
end;

procedure TdmReportGenerator.DoAfterCargaActivos;
begin
     //No borrar
end;

procedure TdmReportGenerator.FiltraUsuariosInactivos;
begin
     with cdsUsuarios do
     begin
         Filter := Format( 'US_ACTIVO = %s', [ Comillas(K_GLOBAL_SI)] ) ;
         Filtered := TRUE;
         First;
     end;
end;

function TdmReportGenerator.RevisaUsuariosInactivos( var sMensaje: string ): Boolean;
 var
    sUsuariosInactivos: string;
begin
     sUsuariosInactivos := VACIO;
     with cdsUsuarios do
     begin

          First;
          Filter := Format( 'US_ACTIVO = %s', [ Comillas(K_GLOBAL_NO)] ) ;
          Filtered := TRUE;
          try
             while NOT EOF do
             begin
                   if  NOT zStrToBool( FieldByName('US_ACTIVO').AsString ) then
                       sUsuariosInactivos := ConcatString( sUsuariosInactivos, Format( '   Usuario: %d, Nombre(%s): %s', [FieldByName('US_CODIGO').AsInteger, FieldByName('US_CORTO').AsString, FieldByName('US_NOMBRE').AsString] ), CR_LF );
                   Next;
             end;

             Result := StrVacio(sUsuariosInactivos) ;
             if Not Result then
                sMensaje := sUsuariosInactivos;

          finally
                 Filter := Format( 'US_ACTIVO = %s', [ Comillas(K_GLOBAL_SI)] ) ;
                 Filtered := TRUE;
                 First;
          end;
     end;
end;

function TdmReportGenerator.GrabaBitacoraServicio (sTexto, sMemo: String; tbTipo: etipoBitacora): Boolean;
const K_GRABA_BITACORA = 'INSERT INTO BITSERVREP ' +
	' (CA_FOLIO, BI_FECHA, BI_HORA, BI_TIPO, BI_TEXTO, BI_DATA, CAL_FOLIO ) ' +
	' VALUES ' +
	' ( %d, %s, %s, %d, %s, %s, %d ) ';
var
   sScriptBitacora: String;
begin
     sScriptBitacora := Format (K_GRABA_BITACORA, [FFolioCalendario, EntreComillas (DateToStrSQL (Now)),
                     EntreComillas (FormatDateTime('hh:nn:ss', Now)), ord (tbTipo), EntreComillas( Copy (sTexto, 0, 254)), EntreComillas (sMemo), FFolioCalenSoli]);

     try
        oZetaProvider.EjecutaAndFree (sScriptBitacora);
     except
           on Error: Exception do
           begin
                // Si se llega a esta situaci�n, ni siquiera poder escribir bit�cora de servicio, entonces cancelar ejecuci�n actual.
                // Informar en solicitud de env�o programado.
                if FFolioCalensoli <> 0 then
                begin
                     FFolioCalensoliCancelado := TRUE;
                     CancelarSolicitudesCalensoli (FFolioCalensoli, 'Env�o cancelado. Error grave en el proceso:' + CR_LF + FiltrarTextoSQL (Error.Message) );
                end;
           end;
     end;
end;

procedure TdmReportGenerator.actualizaCalenSoli (iCAL_FOLIO: Integer; tcSECAL_ESTATUS: eSolicitudEnvios; sCAL_MENSAJE: String; bInicio: Boolean);
begin
     try
        if bInicio then
        begin
             oZetaProvider.EjecutaAndFree (Format (K_ACTUALIZA_CALENSOLI_INICIO,
                       [ EntreComillas (FormatDateTime ('mm/dd/yyyy hh:nn:ss.zzz', Now)), ord (tcSECAL_ESTATUS), EntreComillas (FiltrarTextoSQL (sCAL_MENSAJE)), iCAL_FOLIO]));
        end
        else
        begin
             oZetaProvider.EjecutaAndFree (Format (K_ACTUALIZA_CALENSOLI_FIN,
                       [ EntreComillas (FormatDateTime ('mm/dd/yyyy hh:nn:ss.zzz', Now)), ord (tcSECAL_ESTATUS), EntreComillas (FiltrarTextoSQL (sCAL_MENSAJE)), iCAL_FOLIO]));

        end;
     except
           on Error: Exception do
           begin
                // Escribir en bit�cora de servicio.
                // No pudo ser actualizada la tabla Calendario.
                LogError ('No es posible actualizar solicitud de env�o programado de reportes.' + CR_LF + Error.Message, tbErrorGrave);
           end;
     end;
end;

procedure TdmReportGenerator.CancelarSolicitudesCalensoli (iCAL_FOLIO: Integer = 0; sCAL_MENSAJE: String = VACIO);
const
     K_CAL_MENSAJE = 'Env�o cancelado por el usuario.';
var
   FCalenSoliCancelar: TZetaCursor;
begin
     if sCAL_MENSAJE = VACIO then
        sCAL_MENSAJE := K_CAL_MENSAJE;

     try
        if iCAL_FOLIO =  0 then
        begin
             FCalenSoliCancelar := oZetaProvider.CreateQuery(Format (K_LEE_CALENSOLI_CANCELAR, [ord(tcSECancelar)]));

             try
                FCalenSoliCancelar.Active := TRUE;
                while not FCalenSoliCancelar.Eof do
                begin
                     actualizaCalenSoli(FCalenSoliCancelar.FieldByName('CAL_FOLIO').AsInteger, tcSECancelado, sCAL_MENSAJE, FALSE);

                     // Si se cancel� el Folio de CalenSoli actual, activar bandera indic�ndolo.
                     if FCalenSoliCancelar.FieldByName('CAL_FOLIO').AsInteger = FFolioCalensoli then
                        FFolioCalensoliCancelado := TRUE;

                     FCalenSoliCancelar.Next;
                end;

                FCalenSoliCancelar.Active := FALSE;
             finally
                    FreeAndNil (FCalenSoliCancelar);
             end;
        end
        else
        begin
              actualizaCalenSoli(iCAL_FOLIO, tcSECancelado, sCAL_MENSAJE, FALSE);              
        end;
     except
           on Error: Exception do
           begin
                LogError('No es posible cancelar solicitud de env�o programado.' + CR_LF + Error.Message, tbError);
                if Assigned (FCalenSoliCancelar) then
                   FreeAndNil (FCalenSoliCancelar);
           end;
     end;
end;

{$IFDEF REPORTES_THREAD}

{TGeneraSQLThread: Inicio}
constructor TGeneraSQLThread.Create (Empresa: OleVariant; ServerThread: IdmServerCalcNominaDisp; var oSQLAgente: OleVariant; var oParams: OleVariant; var Error: WideString);
begin
     FreeOnTerminate := FALSE;
     Self.Empresa := Empresa;
     Self.ServerThread := ServerThread;
     Self.oSQLAgente := oSQLAgente;
     Self.oParams := oParams;
     Self.Error := Error;
     inherited Create (FALSE);
end;

procedure TGeneraSQLThread.Execute;
begin
     inherited;
     try
        FDatos := dmReportGenerator.ServerReportesBDE.GeneraSQL (FEmpresa, FoSQLAgente, FoParams, FError);
     except
           on Error: Exception do
           begin
                // Error.Message := Error.Message;
           end;
     end;
end;

destructor TGeneraSQLThread.Destroy;
begin
     inherited;
end;
{TGeneraSQLThread: Fin}

{$ENDIF}

end.

