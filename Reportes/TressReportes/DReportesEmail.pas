unit DReportesEmail;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs, ActiveX,
  ZetaWinAPITools, Winapi.WinSvc;

type
  TTressReportesServicio = class(TService)
    procedure ServiceExecute(Sender: TService);
    procedure ServiceStart(Sender: TService; var Started: Boolean);
    procedure ServiceStop(Sender: TService; var Stopped: Boolean);
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceDestroy(Sender: TObject);
  private
    { Private declarations }
    function FormatMessage(const sMessage: String): String;
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
    procedure EscribeBitacora(const sMensaje: String);
  end;

  TProcesoReporte = class(TThread)
  private
    FReporte: Integer;
  public
    constructor Create( iReporte: Integer );
    destructor Destroy; override;
  end;

var
  TressReportesServicio: TTressReportesServicio;

implementation

uses
    dCliente,
    dReportes,
    ZetaRegistryCliente,
    ZetaClientTools,
    ZetaCommonTools,
    ZetaCommonClasses;

{$R *.DFM}


function GetServiceStatus(const AMachine, AService: PChar; out AStatus: SERVICE_STATUS_PROCESS): Boolean;
var
   scman_handle, svc_handle: SC_HANDLE;
   bytesNeeded             : DWORD;
begin
     result := FALSE;
     scman_handle := OpenSCManager(AMachine, nil, SC_MANAGER_CONNECT);
     if scman_handle <> 0 then
     try
        svc_handle := OpenService(scman_handle, AService, SERVICE_QUERY_STATUS);
        if svc_handle <> 0 then
        try
           result := QueryServiceStatusEx(svc_handle, SC_STATUS_PROCESS_INFO, @AStatus, SizeOf(SERVICE_STATUS_PROCESS), bytesNeeded);
        finally
               CloseServiceHandle(svc_handle);
        end;
     finally
            CloseServiceHandle(scman_handle);
     end;
end;

procedure serviceInfo;
var
  ss: SERVICE_STATUS_PROCESS;
begin
     if GetServiceStatus(nil, 'Nombre_Servicio', ss) then
     begin
          WriteLn(Format('Service type              : %d', [ss.dwServiceType]));
          WriteLn(Format('Current state             : %d', [ss.dwCurrentState]));
          WriteLn(Format('Controls accepted         : %d', [ss.dwControlsAccepted]));
          WriteLn(Format('Win32 exit code           : %d', [ss.dwWin32ExitCode]));
          WriteLn(Format('Service specific exit code: %d', [ss.dwServiceSpecificExitCode]));
          WriteLn(Format('Checkpoint                : %d', [ss.dwCheckPoint]));
          WriteLn(Format('Wait hint                 : %d', [ss.dwWaitHint]));
          WriteLn(Format('Process Id                : %d', [ss.dwProcessId]));
          WriteLn(Format('Service flags             : %d', [ss.dwServiceFlags]));
     end
     else
         WriteLn(Format('Error retrieving service status [%d]', [GetLastError]));
     ReadLn;
end;

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
     TressReportesServicio.Controller(CtrlCode);
end;

function TTressReportesServicio.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

procedure TTressReportesServicio.ServiceStart(Sender: TService; var Started: Boolean);
begin
     InitDCOM;
end;

procedure TTressReportesServicio.ServiceStop(Sender: TService; var Stopped: Boolean);
begin
     UnInitDCOM;
end;

procedure TTressReportesServicio.ServiceCreate(Sender: TObject);
begin
     dmCliente := TdmCliente.Create( nil );
     dmReportes := TdmReportes.Create( nil );

     ZetaCommonTools.Environment;
     ZetaRegistryCliente.InitClientRegistry;

     { ** Ejecutar las siguientes lineas para pruebas del servicio. ** }
       // dmReportes.AgregarSolicitudesCalensoli;
       // dmReportes.CancelarSolicitudesCalensoli;
       // dmReportes.ProcesaReportes;
       // dmReportes.CalendarizaPendientes;
     { ** ** }
end;

procedure TTressReportesServicio.ServiceDestroy(Sender: TObject);
begin
     ZetaRegistryCliente.ClearClientRegistry;
     FreeAndNil( dmReportes );
     FreeAndNil( dmCliente );
end;

procedure TTressReportesServicio.ServiceExecute(Sender: TService);
const
     K_UN_SEGUNDO = 1000;
     K_5_MIN = 5;
     K_TRESS_CORREOS = 'TressCorreosServicio';
var
   sHora, sNewHora: String;
   { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
   iContador5Min: Integer;
   ss: SERVICE_STATUS_PROCESS;

begin
     sHora := '';
     dmCliente.FechaDefault := Now;

     { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
     iContador5Min := 0;

     while not Terminated do
     begin
          // Cancelar solicitudes
          dmReportes.CancelarSolicitudesCalensoli;
          // Procesar tareas pendientes.
          dmReportes.ProcesaReportes;

          { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
          if dmReportes.DetenerServicio = TRUE then
          begin
               Exit;
          end;

          sNewHora := FormatDateTime( 'hhnn', Time );

          if ( sHora <> sNewHora ) then
          begin
               { ** Bug #18814 Hosting - Error "Canvas does not allow drawing en reportes email" en ReportesEmail. **}
               // Si pasaron 5 min, revisar servicio de correos e inciar si est� detenido.
               if iContador5Min = K_5_MIN then
               begin
                    iContador5Min := 0;
                    if GetServiceStatus (nil, K_TRESS_CORREOS, ss) then
                    begin
                          if ss.dwCurrentState = SERVICE_STOPPED then
                          begin
                               // Iniciar servicio de Tress Correos.
                               ZetaWinAPITools.InitService (K_TRESS_CORREOS, '', FALSE)
                          end;
                    end;
               end
               else
               begin
                    iContador5Min := iContador5Min + 1;
               end;

               sHora := sNewHora;
               // Agregar solicitudes de env�o de reportes en la tabla CalenSoli.
               dmReportes.AgregarSolicitudesCalensoli;
               // Modificar fecha de siguiente ejecuci�n.
               // Calendarizar tareas.
               dmReportes.CalendarizaPendientes;
          end;

          if dmCliente.FechaDefault <> Now then
          begin
               dmCliente.FechaDefault := Now;
          end;

          Sleep( K_UN_SEGUNDO );
          ServiceThread.ProcessRequests( False );
     end;
end;

function TTressReportesServicio.FormatMessage(const sMessage: String): String;
begin
     Result := Format( '***   %s   ***', [ sMessage ] );
end;

procedure TTressReportesServicio.EscribeBitacora(const sMensaje: String);
begin
     try
        LogMessage( FormatMessage( sMensaje ), EVENTLOG_INFORMATION_TYPE, 0, 0 );
     except
           LogMessage( FormatMessage( 'Error al escribir a bit�cora' ), EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

constructor TProcesoReporte.Create( iReporte: Integer );
begin
     FReporte := iReporte;
     FreeOnTerminate := True;
     inherited Create( True );
end;

destructor TProcesoReporte.Destroy;
begin
     inherited;
end;

end.
