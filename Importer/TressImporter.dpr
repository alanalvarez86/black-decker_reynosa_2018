program TressImporter;
{$APPTYPE CONSOLE}
uses
  MidasLib,
  SysUtils,
  ZetaClientTools,
  DImporter in 'DImporter.pas' {dmImporter: TDataModule},
  DBasicoCliente in '..\DataModules\DBasicoCliente.pas' {BasicoCliente: TDataModule};

const
     K_OPERACION = 'OPERACION';
     K_COD_OPERA = 'T';
     K_COD_FECHA = 'F';    
var
   sComando, sOperacion: String;
begin
     ZetaClientTools.InitDCOM;
     try
        dmImporter := TdmImporter.Create( nil );
        with dmImporter do
        begin
             sOperacion := ListaParametros.Values[ K_OPERACION ];
             if ( ParamCount >= 4 ) or ( ( ParamCount = 2 ) and ( sOperacion = K_COD_OPERA ) ) or ( ( ParamCount = 3 ) and ( sOperacion = K_COD_FECHA ) ) {$ifdef COMMSCOPE} or ( sOperacion = 'E' ) {$endif} then
                Procesar
             else
             begin
                  sComando := Format( '%s %s=<X> %s=', [ ParamStr( 0 ), P_EMPRESA, P_OPERACION ] );
                  DespliegaAyuda( sComando );
             end;
        end;
     finally
            FreeAndNil( dmImporter );
     end;
end.
