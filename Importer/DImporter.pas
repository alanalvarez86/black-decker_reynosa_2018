unit DImporter;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerLabor,
     DServerAnualNomina,
     DServerGlobal,
     DServerSistema,
     {$else}
     Labor_TLB,
     DAnualNomina_TLB,
     Global_TLB,
     Sistema_TLB,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}
     FAutoClasses,     
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaASCIIFile,
     ZetaClientDataSet,
     ZetaRegistryCliente;

const
     P_EMPRESA        = 'EMPRESA';
     P_OPERACION      = 'OPERACION';
     P_TIPO           = 'TIPO';
     P_OP_CALCULO     = 'C';
     P_CALCULO_INICIO = 'INICIO';
     P_CALCULO_FIN    = 'FIN';
     P_OP_AFECTAR     = 'A';
     P_AFECTAR_FECHA  = 'FECHACORTE';
     P_AFECTAR_YEAR   = 'YEAR';
     P_AFECTAR_TIPO   = 'TIPO';
     P_AFECTAR_NUMERO = 'NUMERO';
     P_FORMATO        = 'FORMATO';
     P_OP_TARJETA     = 'T';
     P_OP_FECHA       = 'F';
     P_FECHA_LIMITE   = 'FECHALIMITE';
{$ifdef COMMSCOPE}
     P_OP_EVALDIARIA     = 'E';
     P_FECHA_EVALDIARIA  = 'FECHAEVALDIARIA';
{$endif}
type
  eImportOp = ( eiWorder, eiPartes, eiComponentes, eiCedulas );
  eOperacion = ( eoImportarCatalogos, eoCalcularTiempos, eoAfectarLabor, eoActualizarTarjeta, eoFechaLimite {$ifdef COMMSCOPE}, eoFechaEvaluacionDiaria {$endif} );
  TdmImporter = class(TDataModule)
    cdsASCII: TZetaClientDataSet;
    cdsASCIIRenglon: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    FBitacora: TAsciiLog;
    FEmpresa: String;
    FOperacion: eImportOp;
    FCalcular: Boolean;
    FInicio: TDate;
    FFinal: TDate;
    FAfectar: boolean;
{$ifdef COMMSCOPE}
    FEvalDiaria: boolean;
    FFechaEvalDiaria: TDate;
{$endif}
    FTarjeta: boolean;
    FFecha: boolean;
    FFechaLimite: TDate;
    FFechaCorte: TDate;
    FYear: integer;
    FTipoPeriodo: integer;
    FNumero: integer;
    FArchivo: String;
    FTipo: eFormatoASCII;
    FFormatoFecha: eFormatoImpFecha;
    FListaParametros: TStrings;
    FOpcion: eOperacion;
    {$ifdef DOS_CAPAS}
    FServerLabor: TdmServerLabor;
    FServerAnualNomina: TdmServerAnualNomina;
    FServerGlobal: TdmServerGlobal;
    FServerSistema: TdmServerSistema;
    {$else}
    FServerLabor: IdmServerLaborDisp;
    FServerAnualNomina: IdmServerAnualNominaDisp;
    FServerGlobal: IdmServerGlobalDisp;
    FServerSistema: IdmServerSistemaDisp;
    function GetServerLabor: IdmServerLaborDisp ;
    function GetServerAnualNomina: IdmServerAnualNominaDisp;
    function GetServerGlobal: IdmServerGlobalDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    {$endif}
    function InitAutorizacion: Boolean;
    //function GetTipoPeriodos: string; //Pendiente
    function GetModulo: TModulos;
    procedure CalcularTiempos(Parametros: OleVariant);
    procedure AfectarLabor(Parametros: OleVariant);
{$ifdef COMMSCOPE}
    procedure AfectaFechaEvalDiaria( Parametros: OleVariant );
{$endif}
    procedure Log( const sTexto: String );
    procedure LogError( const sTexto: String );
    procedure LogEnd;
    procedure LogInit;
    procedure LogProcessEnd(Resultado: OleVariant);
    procedure ImportaOrdenes( const iCount: Integer; Data, Parametros: OleVariant );
    procedure ImportaPartes( const iCount: Integer; Data, Parametros: OleVariant );
    procedure ImportaComponentes( const iCount: Integer; Data, Parametros: OleVariant );
    procedure ImportaCedulas( const iCount: Integer; Data, Parametros: OleVariant );
    procedure ActualizarNumeroTarjeta;
    procedure ModificarFechaLimite;    
    procedure ParametrosPrepara;
    procedure GetListaParametros;
  protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerLabor: TdmServerLabor read FServerLabor;
    property ServerAnualNomina: TdmServerAnualNomina read FServerAnualNomina;
    property ServerGlobal: TdmServerGlobal read FServerGlobal;
    property ServerSistema: TdmServerSistema read FServerSistema;
    {$else}
    property ServerLabor: IdmServerLaborDisp read GetServerLabor;
    property ServerAnualNomina: IdmServerAnualNominaDisp read GetServerAnualNomina;
    property ServerGlobal: IdmServerGlobalDisp read GetServerGlobal;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    {$endif}
  public
    { Public declarations }
    procedure Procesar;
    procedure DespliegaAyuda( sComando: string );
    property ListaParametros: TStrings read FListaParametros;
  end;

var
  dmImporter: TdmImporter;

implementation

uses
     FAutoServer,
     ZetaCommonTools,
     ZGlobalTress,
     ZASCIITools,
     ZBaseThreads,
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker,
     DZetaServerProvider,
     {$endif}
     DCliente;

{$R *.DFM}

const
     K_RELLENO = 10;

{ ********* TdmImporter ********** }

procedure TdmImporter.DataModuleCreate(Sender: TObject);
begin
     ZetaCommonTools.Environment;
     FBitacora := TAsciiLog.Create;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( Self );
    {$ifdef DOS_CAPAS}
     FServerLabor := TdmServerLabor.Create( Self );
     FServerAnualNomina := TdmServerAnualNomina.Create( Self );
     FServerGlobal := TdmServerGlobal.Create( Self );
     FServerSistema := TdmServerSistema.Create( Self );
    {$endif}
    FListaParametros := TStringList.Create;
    GetListaParametros;
end;

procedure TdmImporter.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( FListaPArametros );
    {$ifdef DOS_CAPAS}
     FreeAndNil( FServerLabor );
     FreeAndNil( FServerAnualNomina );
     FreeAndNil( FServerGlobal );
     FreeAndNil( FServerSistema );     
    {$endif}
     FreeAndNil( dmCliente );
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
     FreeAndNil( FBitacora );
end;

{$ifndef DOS_CAPAS}
function TdmImporter.GetServerLabor: IdmServerLaborDisp;
begin
     Result := IdmServerLaborDisp( dmCliente.CreaServidor( CLASS_dmServerLabor, FServerLabor ) );
end;

function TdmImporter.GetServerAnualNomina: IdmServerAnualNominaDisp;
begin
     Result := IdmServerAnualNominaDisp( dmCliente.CreaServidor( CLASS_dmServerAnualNomina, FServerAnualNomina ) );
end;

function TdmImporter.GetServerGlobal: IdmServerGlobalDisp;
begin
     Result := IdmServerGlobalDisp( dmCliente.CreaServidor( CLASS_dmServerGlobal, FServerGlobal ) );
end;

function TdmImporter.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;
{$endif}

procedure TdmImporter.LogInit;
begin
     with FBitacora do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + Title + '.log' );
               end;
          end;
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
     end;
end;

procedure TdmImporter.LogProcessEnd( Resultado: OleVariant );
const
     K_DIA_HORA = 'hh:nn:ss AM/PM dd/mmm/yy';
begin
     with TProcessInfo.Create( nil ) do
     begin
          try
             SetResultado( Resultado );
             case Status of
                  epsEjecutando: Log( '� Proceso Incompleto !' );
                  epsOK:
                  begin
                       if ( TotalErrores > 0 ) then
                          Log( Format( 'Termin� Con %d Errores', [ TotalErrores ] ) )
                       else
                           if ( TotalAdvertencias > 0 ) then
                              Log( Format( 'Termin� Con %d Advertencias', [ TotalAdvertencias ] ) )
                           else
                               if ( TotalEventos > 0 ) then
                                  Log( Format( 'Termin� Con %d Mensajes', [ TotalEventos ] ) )
                                else
                                    Log( 'Termin� Con Exito' );
                  end;
                  epsCancelado: Log( '� Proceso Cancelado !' );
                  epsError: Log( '� Proceso Con Errores !' );
             end;
             Log( Format( 'Folio:    %d', [ Folio ] ) );
             Log( Format( 'Inicio:   %s', [ FormatDateTime( K_DIA_HORA, Inicio ) ] ) );
             Log( Format( 'Fin:      %s', [ FormatDateTime( K_DIA_HORA, Fin ) ] ) );
          finally
                 Free;
          end;
     end;
end;

procedure TdmImporter.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          Close;
     end;
end;

{$ifdef ANTES}
function TdmImporter.InitAutorizacion: Boolean;
begin
     with TAutoServer.Create do
     begin
          try
             Cargar;
             { MV: (13/abr/2005) anteriormente se validaba siempre contra el m�dulo de Labor }
             {
             Result := OkModulo( okLabor );
             }
             if ( FOpcion = eoCalcularTiempos ) then
                Result := ( not EsDemo ) and OkModulo( GetModulo )   //Se valida que se tenga el modulo autorizado y que no es DEMO
             else
                 Result := OKModulo( GetModulo );      //El resto de los casos solo se debe validar que el m�dulo este autorizado
          finally
                 Free;
          end;
     end;
end;
{$else}
function TdmImporter.InitAutorizacion: Boolean;
begin
     Autorizacion.Cargar( dmCliente.GetAuto );
     begin
          with Autorizacion do
          begin
               if ( FOpcion = eoCalcularTiempos ) then
                  Result := ( not EsDemo ) and OkModulo( GetModulo )   //Se valida que se tenga el modulo autorizado y que no es DEMO
               else
                   Result := OKModulo( GetModulo );      //El resto de los casos solo se debe validar que el m�dulo este autorizado
          end;
     end;
end;
{$endif}


function TdmImporter.GetModulo: TModulos;
begin
     case FOpcion of
          eoCalcularTiempos, eoImportarCatalogos, eoAfectarLabor {$ifdef COMMSCOPE}, eoFechaEvaluacionDiaria {$endif}: Result := okLabor;
          eoActualizarTarjeta: Result := okL5Poll;
          eoFechaLimite: Result := okAsistencia;
     else
         Result := okLabor;
     end;
end;

procedure TdmImporter.Procesar;
var
   FParametros: TZetaParams;
   sMensajeError: String;
   lLoop, lSalirTress: Boolean;
   sMensajeErrorHTTP: String;

   function GetOperation: String;
   begin
        case FOperacion of
             eiWorder:      Result := 'Ordenes de Trabajo';
             eiPartes:      Result := 'Partes';
             eiComponentes: Result := 'Componentes';
             eiCedulas:     Result := 'C�dulas de Captura';
        else
            Result := '???';
        end;
   end;
   { MV: Nueva funci�n para conocer si se est� utilizando HTTP, en caso positivo se realiza la validaci�n del m�dulo. }
   procedure validarHTTPEmail( var sMensajeErrorHTTP: String );
   begin
        sMensajeErrorHTTP := VACIO;
        if ClientRegistry.TipoConexion = conxHTTP then
        begin
             dmCliente.ValidarConexionHTTP( lLoop, lSalirTress, sMensajeErrorHTTP, True);
        end;
   end;

begin
     LogInit;
     try
        { MV: (13/abr/2005) Se cambio el orden en las siguientes lineas para que se investigue que operaci�n fue seleccionada }
        {
        if InitAutorizacion then
        begin
             ParametrosPrepara;
        }
        ParametrosPrepara;

        InitAutorizacion;
        validarHTTPEmail( sMensajeErrorHTTP );
        if strVacio( sMensajeErrorHTTP ) then
        begin
             if InitAutorizacion then 
             begin
                  with FBitacora do
                  begin
                       WriteTexto( Format( 'Empresa:             %s', [ FEmpresa ] ) );
                       if FCalcular then
                       begin
                            WriteTexto( Format( 'Fecha Inicial: %s', [ ZetaCommonTools.FechaCorta( FInicio ) ] ) );
                            WriteTexto( Format( 'Fecha Final: %s', [ ZetaCommonTools.FechaCorta( FFinal ) ] ) );
                       end
                       else if FAfectar then
                            begin
                                 {WriteTexto( Format( 'Fecha de Corte:      %s', [ ZetaCommonTools.FechaCorta( FFechaCorte ) ] ) );
                                 WriteTexto( Format( 'Tipo de N�mina:      %s', [ ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( FTipoPeriodo ) ) ] ) );
                                 WriteTexto( Format( 'A�o:                 %s', [ IntToStr( FYear ) ] ) );
                                 WriteTexto( Format( 'N�mero del Peri�do:  %s', [ IntToStr( FNumero ) ] ) ); }
                            end
                            else if FTarjeta then
                                 begin
                                      WriteTexto( 'Operaci�n: Actualizaci�n # Tarjeta Todas las Empresas de Comparte' )
                                 end
               {$ifdef COMMSCOPE}
                                 else if FEvalDiaria then
                                      begin
                                           WriteTexto( Format( 'Fecha limite de evaluaci�n diaria: %s', [ ZetaCommonTools.FechaCorta( FFechaEvalDiaria ) ] ) );
                                      end
               {$endif}
                                 else if FFecha then
                                      begin
                                           WriteTexto( 'Operaci�n: Modificaci�n Manual A Fecha L�mite De Bloqueo' );
                                           WriteTexto( Format( 'Fecha de L�mite:      %s', [ ZetaCommonTools.FechaCorta( FFechaLimite ) ] ) );
                                      end
                                      else
                                      begin
                                           WriteTexto( Format( 'Operaci�n: Importar %s', [ GetOperation ] ) );
                                           WriteTexto( Format( 'Archivo:   %s', [ FArchivo ] ) );
                                           WriteTexto( Format( 'Tipo:      %s', [ ZetaCommonLists.ObtieneElemento( lfFormatoASCII, Ord( FTipo ) ) ] ) );
                                           if ( FOperacion = eiCedulas ) then
                                              WriteTexto( Format( 'Formato:   %s', [ ZetaCommonLists.ObtieneElemento( lfFormatoImpFecha, Ord( FFormatoFecha ) ) ] ) );
                                      end;
                       if NOT FAfectar then WriteTexto( StringOfChar( '-', 40 ) );
                  end;
                  if dmCliente.BuscaCompany( FEmpresa ) then
                  begin
                       FParametros := TZetaParams.Create;
                       try
                          with FParametros do
                          begin
                               if not FTarjeta then
                               begin
                                    if FCalcular then
                                    begin
                                         AddString( 'RangoLista', VACIO );
                                         AddString( 'Condicion', VACIO );
                                         AddString( 'Filtro', VACIO );
                                         AddDate( 'FechaInicial', FInicio );
                                         AddDate( 'FechaFinal', FFinal );
                                         AddBoolean( 'CalcularLecturas', False );
                                         AddBoolean( 'RegistrarBitacora', False );
                                    end
                                    else if FAfectar then
                                         begin
                                              AddDate( 'FechaCorte', FFechaCorte );
                                              AddInteger( 'Year', FYear );
                                              AddInteger( 'Tipo', FTipoPeriodo );
                                              AddInteger( 'Numero', FNumero );
                                         end
          {$ifdef COMMSCOPE}
                                         else if FEvalDiaria then
                                              begin
                                                   AddDate( 'FechaEvalDiaria', FFechaEvalDiaria );
                                              end
          {$endif}
                                         else if FFecha then
                                              begin
                                                   AddDate( 'FechaLimite', FFechaLimite );
                                              end
                                              else
                                              begin
                                                   AddString( 'Archivo', FArchivo );
                                                   AddInteger( 'Formato', Ord( FTipo ) );
                                                   AddInteger( 'FormatoImpFecha', Ord( FFormatoFecha ) );   // No afecta a los procesos que no lo requieren
                                              end;
                               end;
                          end;
                          if FCalcular then
                          begin
                               CalcularTiempos( FParametros.VarValues );
                          end
                          else if FAfectar then
                               begin
                                    AfectarLabor( FParametros.VarValues );
                               end
     {$ifdef COMMSCOPE}
                          else if FEvalDiaria then
                               begin
                                    AfectaFechaEvalDiaria( FParametros.VarValues );
                               end
     {$endif}
                               else if FTarjeta then
                                    begin
                                         ActualizarNumeroTarjeta;
                                    end
                                    else if FFecha then
                                         begin
                                              ModificarFechaLimite;
                                         end
                                         else
                                         begin
                                              if FileExists( FArchivo ) then
                                              begin
                                                   ZASCIITools.FillcdsASCII( cdsASCII, FArchivo );
                                                   with cdsASCII do
                                                   begin
                                                        Active := True;
                                                        try
                                                           MergeChangeLog;  { Elimina Registros Borrados }
                                                           case FOperacion of
                                                                eiWorder:      ImportaOrdenes( RecordCount, Data, FParametros.VarValues );
                                                                eiPartes:      ImportaPartes( RecordCount, Data, FParametros.VarValues );
                                                                eiComponentes: ImportaComponentes( RecordCount, Data, FParametros.VarValues );
                                                                eiCedulas:     ImportaCedulas( RecordCount, Data, FParametros.VarValues );
                                                           end;
                                                        finally
                                                               Active := False;
                                                        end;
                                                   end;
                                              end
                                              else
                                                  LogError( Format( 'El Archivo %s No Existe', [ FArchivo ] ) );
                                         end;
                       finally
                              FParametros.Free;
                       end;
                  end
                  else
                      LogError( 'La L�nea de Comandos Est� Mal Configurada. Empresa ' + FEmpresa + ' No Existe' );
             end
             else
             begin
                  if StrVacio( sMensajeError ) then
                     LogError( 'El M�dulo De ' + Autorizacion.GetModuloStr( GetModulo ) + ' No Est� Autorizado' + CR_LF + 'Consulte A Su Distribuidor' )
                  else
                      LogError( sMensajeError );

             end;
        end
        else
        begin
             LogError( sMensajeErrorHTTP );
        end;
     finally
            LogEnd;
     end;
end;

procedure TdmImporter.DespliegaAyuda( sComando: string );
begin
     WriteLn( '' );
     WriteLn( 'Formato Para Importar' );
     WriteLn( '=====================' );
     WriteLn( Format( '%s<Y> %s=<Z> <Archivo> [%s=<W>]', [ sComando, P_TIPO, P_FORMATO ] ) );
     WriteLn( '' );
     WriteLn( 'Par�metros:' );
     WriteLn( '' );
     WriteLn( '<X>:       C�digo de Empresa ( COMPANY.CM_CODIGO )' );
     WriteLn( '<Y>:       Tipo de Importaci�n a Efectuar: 0 - Ordenes, 1 - Partes, 2 - Componentes, 3 - C�dulas' );
     WriteLn( '<Z>:       Tipo de ASCII: 0 = Longitud Fija, 1 = Delimitado' );
     WriteLn( '<Archivo>: Archivo ASCII a Importar ( Usar "" Para Nombres Largos )' );
     WriteLn( '<W>:       Formato de Fecha: 0 = dd/mm/yy, 1 = dd/mm/yyyy, 2 = ddmmyy, 3 = ddmmyyyy, 4 = mm/dd/yy, 5 = mm/dd/yyyy, 6 = mmddyy, 7 = mmddyyyy, 8 = yy/mm/dd, 9 = yyyy/mm/dd, 10 = yymmdd, 11 = yyyymmdd' );
     WriteLn( '           * Solo Disponible En C�dulas - Valor Default = 1' );
     WriteLn( '' );
     WriteLn( 'Formato Para Calcular Tiempos' );
     WriteLn( '=============================' );
     WriteLn( Format( '%s%s %s=<Y> %s=<Z>', [ sComando, P_OP_CALCULO, P_CALCULO_INICIO, P_CALCULO_FIN ] ) );
     WriteLn( '' );
     WriteLn( 'Par�metros:' );
     WriteLn( '' );
     WriteLn( '<X>:       C�digo de Empresa ( COMPANY.CM_CODIGO )' );
     WriteLn( '<Y>:       Inicio del Per�odo de C�lculo: Hoy = 0, Ayer = -1, Anteayer = -2' );
     WriteLn( '<Z>:       Final del Per�odo de C�lculo: Hoy = 0, Ma�ana = 1, Pasado Ma�ana = 2' );
     WriteLn( '' );
     WriteLn( 'Formato Para Afectar Labor' );
     WriteLn( '==========================' );
     WriteLn( Format( '%s%s %s=<S> %s=<W> %s=<Y> %s=<Z>', [ sComando, P_OP_AFECTAR, P_AFECTAR_FECHA, P_AFECTAR_TIPO, P_AFECTAR_YEAR, P_AFECTAR_NUMERO ] ) );
     WriteLn( '' );
     WriteLn( 'Par�metros:' );
     WriteLn( '' );
     WriteLn( '<X>:       C�digo de Empresa ( COMPANY.CM_CODIGO )' );
     WriteLn( '<S>:       Fecha de Corte en Labor: Hoy = 0, Ayer = -1, Anteayer = -2, etc...' );
     WriteLn( '<W>:       Tipo de Periodo: (TPERIODO.TP_TIPO)' );
     WriteLn( '<Y>:       A�o: 0 = A�o de la Fecha de Corte Para Cambios en Labor' );
     WriteLn( '<Z>:       N�mero del Periodo: Indicar el N�mero del Periodo de N�mina' );
     WriteLn( '' );
     WriteLn( 'Formato Para Actualizar # de Tarjeta' );
     WriteLn( '=========================================' );
     WriteLn( Format( '%s%s', [ sComando, P_OP_TARJETA ] ) );
     WriteLn( '' );
     WriteLn( 'Par�metros:' );
     WriteLn( '' );
     WriteLn( '<X>:       C�digo de Empresa ( COMPANY.CM_CODIGO )' );
     WriteLn( '' );
     WriteLn( 'Formato Para Modificaci�n Manual A Fecha Limite De Bloqueo' );
     WriteLn( '==========================================================' );
     WriteLn( Format( '%s%s %s=<S>', [ sComando, P_OP_FECHA, P_FECHA_LIMITE ] ) );
     WriteLn( '' );
     WriteLn( 'Par�metros:' );
     WriteLn( '' );
     WriteLn( '<X>:       C�digo de Empresa ( COMPANY.CM_CODIGO )' );
     WriteLn( '<S>:       Fecha de L�mite: Hoy = 0, Ayer = -1, Anteayer = -2, etc...' );
     WriteLn( '' );
{$ifdef COMMSCOPE}
     WriteLn( 'Formato para modificar fecha l�mite de evaluaci�n diaria' );
     WriteLn( '===========================================================' );
     WriteLn( Format( '%s%s %s=<S>', [ sComando, P_OP_EVALDIARIA, P_FECHA_EVALDIARIA ] ) );
     WriteLn( '' );
     WriteLn( 'Par�metros:' );
     WriteLn( '' );
     WriteLn( '<X>:       C�digo de Empresa ( COMPANY.CM_CODIGO )' );
     WriteLn( '<S>:       Fecha l�mite de evaluaci�n diaria: Hoy = 0, Ayer = -1, Anteayer = -2, etc...' );
     WriteLn( '' );
{$endif}

end;

procedure TdmImporter.Log( const sTexto: String );
begin
     FBitacora.WriteTexto( sTexto );
     WriteLn( sTexto );
end;

procedure TdmImporter.LogError( const sTexto: String );
begin
     Log( 'ERROR: ' + sTexto );
     Log( '' );
end;

procedure TdmImporter.ParametrosPrepara;
var
   sOperacion: String;
begin
     with FListaParametros do
     begin
          FEmpresa := Values[ P_EMPRESA ];
          sOperacion := Values[ P_OPERACION ];
          if ( sOperacion = P_OP_CALCULO ) then
          begin
               FAfectar := False;
               FTarjeta := False;
               FFecha := False;
               FCalcular := True;
               FInicio := Date + StrToIntDef( Values[ P_CALCULO_INICIO ], 0 );
               FFinal := Date + StrToIntDef( Values[ P_CALCULO_FIN ], 0 );
               FOpcion := eoCalcularTiempos;
          end
          else if ( sOperacion = P_OP_AFECTAR ) then
               begin
                    FCalcular := False;
                    FAfectar := True;
                    FFecha := False;
                    FTarjeta := False;
                    FFechaCorte := Date + StrToIntDef( Values[ P_AFECTAR_FECHA ], 0 );
                    FYear := StrToIntDef( P_AFECTAR_YEAR, 0 );
                    if FYear = 0 then
                       FYear := TheYear( FFechaCorte );
                    FTipoPeriodo := StrToIntDef( Values[ P_AFECTAR_TIPO ], Ord( tpSemanal ) );
                    FNumero := StrToIntDef( Values[ P_AFECTAR_NUMERO ], 1 );
                    FOpcion := eoAfectarLabor;
               end
          {$ifdef COMMSCOPE}
          else if ( sOperacion = P_OP_EVALDIARIA ) then
               begin
                    FCalcular := False;
                    FAfectar := False;
                    FFecha := False;
                    FTarjeta := False;
                    FEvalDiaria := True;
                    FFechaEvalDiaria := Date + StrToIntDef( Values[ P_FECHA_EVALDIARIA ], 0 );
                    FOpcion := eoFechaEvaluacionDiaria;
               end
          {$endif}
               else if ( sOperacion = P_OP_TARJETA ) then
                    begin
                         FCalcular := False;
                         FAfectar := False;
                         FFecha := False;
                         FTarjeta := True;
                         FOpcion := eoActualizarTarjeta;
                    end
                    else if ( sOperacion = P_OP_FECHA ) then
                         begin
                              FCalcular := False;
                              FAfectar := False;
                              FTarjeta := False;
                              FFecha := True;
                              FFechaLimite := Date + StrToIntDef( Values[ P_FECHA_LIMITE ], 0 );
                              FOpcion := eoFechaLimite;
                         end
                         else
                         begin
                              FCalcular := False;
                              FAfectar := False;
                              FTarjeta := False;
                              FFecha := False;
                              FOperacion := eImportOp( StrToIntDef( sOperacion, 0 ) );
                              FTipo := eFormatoASCII( StrToIntDef( Values[ P_TIPO ], 0 ) );
                              FArchivo := ParamStr( 4 );
                              FFormatoFecha := eFormatoImpFecha( StrToIntDef( Values[ P_FORMATO ], Ord( ifDDMMYYYYs ) ) );
                              FOpcion := eoImportarCatalogos;
                         end;
     end;
end;

procedure TdmImporter.CalcularTiempos( Parametros: OleVariant );
begin
     Log( 'Calculando Tiempos' );
     try
        ServerAnualNomina.CalcularTiempos( dmCliente.Empresa, Parametros );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'C�lculo de Tiempos Terminado' );
end;

procedure TdmImporter.AfectarLabor( Parametros: OleVariant );
var
   aGlobalServer: Variant;
   ErrorCount, i: integer;
begin
     dmCliente.InitArrayTPeriodo;  //acl
     with FBitacora do
     begin
          WriteTexto( Format( 'Fecha de Corte:      %s', [ ZetaCommonTools.FechaCorta( FFechaCorte ) ] ) );
          WriteTexto( Format( 'Tipo de N�mina:      %s', [ ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( FTipoPeriodo ) ) ] ) );
          WriteTexto( Format( 'A�o:                 %s', [ IntToStr( FYear ) ] ) );
          WriteTexto( Format( 'N�mero del Peri�do:  %s', [ IntToStr( FNumero ) ] ) );
          WriteTexto( StringOfChar( '-', 40 ) );
     end;

     Log( 'Afectando Labor' );
     try
        aGlobalServer := VarArrayCreate( [ 0, K_TOT_GLOBALES ], varOleStr );
        for i := 0 to K_TOT_GLOBALES do
            aGlobalServer[ i ] := K_TOKEN_NIL;
        aGlobalServer[ K_GLOBAL_LABOR_FECHACORTE ] := FechatoStr( FFechaCorte );
        ServerGlobal.GrabaGlobales( dmCliente.Empresa, aGlobalServer, False, ErrorCount );
        if ( ErrorCount > 0 ) then
           LogError( 'Error al Grabar en Tabla Global' );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     try
        ServerAnualNomina.AfectarLabor( dmCliente.Empresa, Parametros );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Afectaci�n de Labor Terminado' );
end;

{$ifdef COMMSCOPE}
procedure TdmImporter.AfectaFechaEvalDiaria( Parametros: OleVariant );
var
   aGlobalServer: Variant;
   ErrorCount, i: integer;
begin
     Log( 'Fecha de evaluaci�n diaria' );
     try
        aGlobalServer := VarArrayCreate( [ 0, K_TOT_GLOBALES ], varOleStr );
        for i := 0 to K_TOT_GLOBALES do
            aGlobalServer[ i ] := K_TOKEN_NIL;
        aGlobalServer[ K_GLOBAL_FECHA_EVALUACION_DIARIA ] := FechatoStr( FFechaEvalDiaria );
        ServerGlobal.GrabaGlobales( dmCliente.Empresa, aGlobalServer, False, ErrorCount );
        if ( ErrorCount > 0 ) then
           LogError( 'Error al Grabar en Tabla Global' );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Fecha de evaluaci�n diaria Terminado' );
end;
{$endif}

procedure TdmImporter.ImportaOrdenes( const iCount: Integer; Data, Parametros: OleVariant );
begin
     Log( Format( 'Importando %d Ordenes', [ iCount ] ) );
     try
        LogProcessEnd( ServerLabor.ImportarOrdenes( dmCliente.Empresa, Parametros, Data ) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( Format( '%d Ordenes Fueron Importadas', [ iCount ] ) );
end;

procedure TdmImporter.ImportaPartes( const iCount: Integer; Data, Parametros: OleVariant );
begin
     Log( Format( 'Importando %d Partes', [ iCount ] ) );
     try
        LogProcessEnd( ServerLabor.ImportarPartes( dmCliente.Empresa, Parametros, Data ) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( Format( '%d Partes Fueron Importadas', [ iCount ] ) );
end;

procedure TdmImporter.ImportaComponentes( const iCount: Integer; Data, Parametros: OleVariant );
begin
     Log( Format( 'Importando %d Componentes', [ iCount ] ) );
     try
        LogProcessEnd( ServerLabor.ImportarComponentes( dmCliente.Empresa, Parametros, Data ) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( Format( '%d Componentes Fueron Importados', [ iCount ] ) );
end;

procedure TdmImporter.ImportaCedulas( const iCount: Integer; Data, Parametros: OleVariant );
begin
     Log( Format( 'Importando %d C�dulas de Captura', [ iCount ] ) );
     try
        LogProcessEnd( ServerLabor.ImportarCedulas( dmCliente.Empresa, Parametros, Data ) );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( Format( '%d C�dulas de Captura Fueron Importados', [ iCount ] ) );
end;

{function TdmImporter.GetTipoPeriodos: string;
var
   i: eTipoPeriodo;
begin
     Result := VACIO;
     for i:= 0 to FArregloPeriodo.Count - 1 do
     begin
          Result := ConcatString( Result,ObtieneElemento( lfTipoPeriodo, Ord( i ) ) + ' = ' + IntToStr( Ord( i ) ), ', ' );
     end;
end;}

procedure TdmImporter.ActualizarNumeroTarjeta;
var
   Bitacora: OleVariant;
   i: integer;
begin
     try
        ServerSistema.ActualizaNumeroTarjeta( Bitacora );
        with TStringList.Create do
        begin
             try
                Text := Bitacora;
                for i := 0 to ( Count - 1 ) do
                begin
                     Log( Strings[ i ] );
                end;
             finally
                    Free;
             end;
        end;
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
end;

procedure TdmImporter.ModificarFechaLimite;
begin
     Log( 'Modificando Fecha L�mite' );
     try
        ServerGlobal.SetFechaLimite( dmCliente.Empresa, FFechaLimite );
     except
           on Error: Exception do
           begin
                LogError( Error.Message );
           end;
     end;
     Log( 'Fecha L�mite Modificada' );
end;

procedure TdmImporter.GetListaParametros;
var
   i: integer;
begin
     with FListaParametros do
     begin
          Clear;
          for i := 1 to ParamCount do
          begin
               Add( UpperCase( ParamStr( i ) ) );
          end;
     end;
end;

end.

