unit ZetaClientTools;

interface

uses Windows, SysUtils, StdCtrls, Classes, Messages, Controls, ShellApi,
      FileCtrl,Mask, Dialogs, Checklst, Forms, DB, ExtCtrls, DBClient,
     Variants, StrUtils,
     {$ifndef DOS_CAPAS}
     ComObj, ActiveX,
     {$endif}
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaKeyCombo,cxCheckListBox;

type
    VersionInfo = record
      MajorVersion: Word;
      MinorVersion: Word;
      Release: Word;
      Build: Word;
    end;

function AbreDialogo( OpenDialog: TOpenDialog; const sArchivo, sExt: String ): String;
function AddDateTime( const dValue: TDate; const sHora: String ): TDateTime;
function AddValue( const sValue, sFormatStr: String; const iValue: Integer ): String;
function DecodeLista( oListBox: TCheckListBox; const iLista: Integer ): String;overload
function DecodeLista( oListBox: TcxCheckListBox; const iLista: Integer ): String;overload
function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
function GetApplicationVersionNumber: String;
function GetApplicationVersionBuild: String;
function GetApplicationVersionInfo( const sItem: String ): String;
function GetApplicationProductVersionSimple: String;  //**ProductVersion formato sin puntos
function GetApplicationProductVersion: String;
function GetDuration( const dValue: TDateTime ): String;
function GetElapsed( const dFinal, dInicial: TDateTime ): TDateTime;
function GetFiltroLista( const sCampo, sValores: String ): String;
function GetFiltroRango( const sCampo, sInicial, sFinal: String ): String;
function GetProcessName( const eProceso: Procesos ): String;
function GetProcessElement( const eProceso: Procesos ): String;
function GetProcessStatus( const sStatus: String ): eProcessStatus;
function ValorCampo( oCampo: TField ): Variant;
function ValorCampoOLE( oCampo: TField ): OLEVariant;
function GetScaledHeight( const iAltura: Integer ): Integer;
function PK_Violation( Error: EReconcileError ): Boolean;
function FK_Violation( Error: EReconcileError ): Boolean;
function UK_Violation( Error: EReconcileError ): Boolean;
{*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
function UKC_Violation( Error: EReconcileError ): Boolean;
function FechaResidencia( const rYear, rMonth: Real ): TDate;
function VerificaValorTasa( oValor: TTasa ): Boolean;
procedure LlenaTasaAnterior( oComboBox: TZetaKeyCombo );
procedure DecodeTexto( oListBox: TCheckListBox; sTexto: String; const iLista: Integer );overload
procedure DecodeTexto( oListBox: TcxCheckListBox; sTexto: String; const iLista: Integer );overload
procedure SetEnabled( Panel: TPanel; const lEnabled: Boolean );
procedure SetEnabledControl( oParentControl: TWinControl; const lEnabled: Boolean );
procedure CierraDatasets( DataModule: TDataModule );
procedure EnviaMensaje( oControl: TWinControl; const sTexto: String );
procedure InitDCOM( const lInitSecurity: Boolean = True );
procedure UnInitDCOM;
procedure SetControlEnabled( oControl: TWinControl; oLabel: TLabel; const lEnabled: Boolean );
procedure SetYearsMonths( const dFecha: TDate; var rYear, rMonth: Real );
//procedure LlenaListaBoolean( oListaNames: TStrings; oListaValues: TStrings );
procedure OrdenarPor( DataSet: TClientDataset; const FieldName: String );
function SplitList(Delimiter: Char; Str: string; ListOfStrings: TStrings):Boolean ;
function CambiaCampo( oCampo: TField ): Boolean;

var
   VersionArchivo: VersionInfo;


implementation

uses ZetaCommonTools;

procedure InitDCOM( const lInitSecurity: Boolean = True );
{$ifndef DOS_CAPAS}
const
     { Authentication service provider constants: the default should be used }
     RPC_C_AUTHN_NONE                  :Integer = 0;
     RPC_C_AUTHN_WINNT                 :Integer = 10;
     RPC_C_AUTHN_DEFAULT               :DWord = $FFFFFFFF;
     { Authentication level constants }
     RPC_C_AUTHN_LEVEL_DEFAULT         :Integer = 0;
     RPC_C_AUTHN_LEVEL_NONE            :Integer = 1;
     RPC_C_AUTHN_LEVEL_CONNECT         :Integer = 2;
     RPC_C_AUTHN_LEVEL_CALL            :Integer = 3;
     RPC_C_AUTHN_LEVEL_PKT             :Integer = 4;
     RPC_C_AUTHN_LEVEL_PKT_INTEGRITY   :Integer = 5;
     RPC_C_AUTHN_LEVEL_PKT_PRIVACY     :Integer = 6;
     { Impersonation level constants }
     RPC_C_IMP_LEVEL_ANONYMOUS         :Integer = 1;
     RPC_C_IMP_LEVEL_IDENTIFY          :Integer = 2;
     RPC_C_IMP_LEVEL_IMPERSONATE       :Integer = 3;
     RPC_C_IMP_LEVEL_DELEGATE          :Integer = 4;
     { Constants for the capabilities }
     API_NULL                          :Integer = 0;
     S_OK                              :Integer = 0;
     EOAC_NONE                         :Integer = $0;
     EOAC_MUTUAL_AUTH                  :Integer = $1;
     EOAC_CLOAKING                     :Integer = $10;
     EOAC_SECURE_REFS                  :Integer = $2;
     EOAC_ACCESS_CONTROL               :Integer = $4;
     EOAC_APPID                        :Integer = $8;
{$endif}
begin
     {$ifndef DOS_CAPAS}
     ComObj.CoInitFlags := COINIT_MULTITHREADED + COINIT_SPEED_OVER_MEMORY;
     ComObj.OleCheck( ActiveX.CoInitialize( nil ) );
     {$ifdef FALSE}
     if lInitSecurity then
     begin
          { Noviembre 2002: ER,CV, se requiere que este codigo no se ejecute,
          cuando se estan ejecutando los casos de prueba que realiza el TestComplete,
          falta probar que la  version nueva (posterior a la 2.0) del TestComplete, si
          no marca error con este codigo.
          El error que marca es que no se puede accesar el Dll del TestComplete }
          ComObj.OleCheck( ActiveX.CoInitializeSecurity( nil, { Points to security descriptor }
                                                         -1,  { Count of entries in asAuthSvc }
                                                         nil, { Array of names to register }
                                                         nil, { Reserved for future use }
                                                         RPC_C_AUTHN_LEVEL_NONE,    { The default authentication level for proxies }
                                                         RPC_C_IMP_LEVEL_IMPERSONATE, { The default impersonation level for proxies }
                                                         nil,        { Reserved; must be set to NULL }
                                                         EOAC_NONE,  { Additional client and/or server-side capabilities }
                                                         nil ) );    { Reserved for future use }
     end;
     {$endif}
     {$endif}
end;

procedure UnInitDCOM;
begin
     {$ifndef DOS_CAPAS}
     ActiveX.CoUninitialize;
     {$endif}
end;

function GetProcessName( const eProceso: Procesos ): String;
begin
     Result := aProcesos[ eProceso ];
end;

function GetProcessElement( const eProceso: Procesos ): String;
begin
     {$ifdef COMPARTE_MGR}
     Result := 'Registros';
     {$else}
     {$ifdef ADUANAS}
     Result := 'Registros';
     {$else}
     case eProceso of
          prASISCorregirFechas: Result := 'Tarjetas';
          prASISRecalculoTarjetas: Result := 'Tarjetas';
          prSISTBorrarTarjetas: Result := 'Tarjetas';
          prSISTBorrarPOLL: Result := 'Checadas';
          prLabImportarOrdenes: Result := 'Ordenes';
          prLabImportarPartes: Result := 'Partes';
          prSISTCierrePrestamos: Result := 'Pr�stamos';
          prSISTCierreAhorros: Result := 'Ahorros';
          prNODeclaracionAnual,prNoDeclaracionAnualCierre: Result := 'C�lculos';
          prNOFonacotImportarCedulas: Result := 'Cr�ditos';
          prNOFonacotGenerarCedula: Result := 'Cr�ditos';
          prNOFonacotEliminarCedula: Result := 'Cr�ditos';
     else
         Result := 'Empleados';
     end;
     {$endif}
     {$endif}
end;

function GetProcessStatus( const sStatus: String ): eProcessStatus;
begin
     if ( sStatus = B_PROCESO_ABIERTO ) then
        Result := epsEjecutando
     else
         if ( sStatus = B_PROCESO_CANCELADO ) then
            Result := epsCancelado
         else
             if ( sStatus = B_PROCESO_OK ) then
                Result := epsOK
             else
                 Result := epsError;
end;

function GetElapsed( const dFinal, dInicial: TDateTime ): TDateTime;
begin
     if ( dFinal > dInicial ) then
        Result := dFinal - dInicial
     else
         Result := NullDateTime;
end;

function AddDateTime( const dValue: TDate; const sHora: String ): TDateTime;
begin
     Result := dValue + EncodeTime( StrToIntDef( Copy( sHora, 1, 2 ), 0 ),
                                    StrToIntDef( Copy( sHora, 4, 2 ), 0 ),
                                    StrToIntDef( Copy( sHora, 7, 2 ), 0 ),
                                    0 );
end;

function AddValue( const sValue, sFormatStr: String; const iValue: Integer ): String;
begin
     if ( iValue < 1 ) then
        Result := sValue
     else
         if ( iValue = 1 ) then
            Result := sValue + Format( sFormatStr, [ iValue ] )
         else
             Result := sValue + Format( sFormatStr, [ iValue ] ) + 's';
end;

function GetDuration( const dValue: TDateTime ): String;
var
   iDias, iHour, iMin, iSec, iMSec: Word;
begin
     if ( dValue = NullDateTime ) then
        Result := ''
     else
     begin
          DecodeTime( dValue, iHour, iMin, iSec, iMSec );
          iDias := Trunc( dValue );
          Result := AddValue( '', ' %d D�a', iDias );
          Result := AddValue( Result, ' %d Hora', iHour );
          Result := AddValue( Result, ' %d Minuto', iMin );
          Result := AddValue( Result, ' %d Segundo', iSec );
     end;
end;

function AbreDialogo( OpenDialog: TOpenDialog; const sArchivo, sExt: String ): String;
begin
     Result := sArchivo;
     with OpenDialog do
     begin
          DefaultExt := sExt;
          if StrVacio( Filter ) then
             Filter := sExt + '|' + '*.' + sExt + '|' + 'Todos' + '|' + '*.*' ;
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );
          if Execute then
             Result := FileName;
     end;
end;
//se le agrego Overload A este metodo Para utilizarlo tanto con LLamadas de Componentes normales o de devex
function DecodeLista( oListBox: TCheckListBox; const iLista: Integer ): String;overload;
var
   i, iOffset: Integer;
begin
     iOffSet := GetOffSet( ListasFijas( iLista ) );
     with oListBox do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               if Checked[ i ] then
               begin
                    if ZetaCommonTools.StrLleno( Result ) then
                       Result := Result + ',';
                    Result := Result + IntToStr( i  + iOffSet );
               end;
          end;
     end;
end;
//Nuevos Metodo Para DecodeLista de  TcXchecklistbox
function DecodeLista( oListBox: TcxCheckListBox; const iLista: Integer ): String;overload;
var
   i, iOffset: Integer;
begin
     iOffSet := GetOffSet( ListasFijas( iLista ) );
     with oListBox do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               if Items[ i ].Checked then
               begin
                    if ZetaCommonTools.StrLleno( Result ) then
                       Result := Result + ',';
                    Result := Result + IntToStr( i  + iOffSet );
               end;
          end;
     end;
end;
//fin del nuevo metodo

procedure DecodeTexto( oListBox: TCheckListBox;  sTexto: String; const iLista: Integer );overload;
var
   i, iOffset: Integer;
begin
     iOffSet := GetOffSet( ListasFijas( iLista ) );
     sTexto := ','+sTexto + ',';
     with oListBox do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin
               Checked[ i ] := ( Pos( ','+ IntToStr( i + iOffSet ) + ',', sTexto ) > 0 );
          end;
     end;
end;
//Nuevo Metodo Para Decode Texto de TcxChecklistbox
procedure DecodeTexto( oListBox: TcxCheckListBox;  sTexto: String; const iLista: Integer );overload;
var
   i, iOffset: Integer;
begin
     iOffSet := GetOffSet( ListasFijas( iLista ) );
     sTexto := ','+sTexto + ',';
     with oListBox do
     begin
          for i := 0 to ( Items.Count - 1 ) do
          begin

               oListBox.Items[i].Checked:= ( Pos( ','+ IntToStr( i + iOffSet ) + ',', sTexto ) > 0 );
          end;
     end;
end;
// Fin del Metodo Nuevo
function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
begin
     zFileName := StrAlloc( Length( FileName ) + 1 );
     try
        zParams := StrAlloc( Length( Params ) + 1 );
        try
           zDir := StrAlloc( Length( DefaultDir ) + 1 );
           try
              zFileName := StrPCopy( zFileName, FileName );
              zParams := StrPCopy( zParams, Params );
              zDir := StrPCopy( zDir, DefaultDir );
              Result := ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      zFileName,
                                      zParams,
                                      zDir,
                                      ShowCmd );
           finally
                  StrDispose( zDir );
           end;
        finally
               StrDispose( zParams );
        end;
     finally
            StrDispose( zFileName );
     end;
end;

{function ExecuteFile( const FileName, Params, DefaultDir: String; ShowCmd: Integer ): THandle;
var
   zFileName, zParams, zDir: array[ 0..254 ] of Char;
begin
     Result := ShellExecute( Application.MainForm.Handle,
                             nil,
                             StrPCopy( zFileName, FileName ),
                             StrPCopy( zParams, Params ),
                             StrPCopy( zDir, DefaultDir ),
                             ShowCmd );
end;}

function GetFiltroRango( const sCampo, sInicial, sFinal: String ): String;
begin
     if ZetaCommonTools.StrLleno( sInicial ) and ZetaCommonTools.StrLleno( sFinal ) then
        Result := ZetaCommonTools.ConcatFiltros( ZetaCommonTools.Parentesis( sCampo +  '>= ' + sInicial ),
                                                 ZetaCommonTools.Parentesis( sCampo + '<= ' + sFinal ) )
     else
         if ZetaCommonTools.StrLleno( sInicial ) then
            Result := ZetaCommonTools.Parentesis( sCampo + '>= ' + sInicial )
         else
             if ZetaCommonTools.StrLleno( sFinal ) then
                Result := ZetaCommonTools.Parentesis( sCampo + '<= ' + sFinal )
             else
                 Result := '';
end;

function GetFiltroLista( const sCampo, sValores: String ): String;
begin
     if ZetaCommonTools.StrLleno( sCampo ) and ZetaCommonTools.StrLleno( sValores ) then
        Result := ZetaCommonTools.Parentesis( sCampo + ' in ( ' + sValores + ' )' )
     else
         Result := '';
end;

function GetVersionInfo: VersionInfo;
var
   Size, Value: DWord;
   pFileName: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
   sFileName: String;
   Data, Info: Pointer;
   iLen: UINT;

function HighWord( Value: DWord ): Word;
begin
     Result := ( ( Value and $FFFF0000 ) shr $10 );
end;

function LowWord( Value: DWord ): Word;
begin
     Result := ( Value and $0000FFFF );
end;

begin
     sFileName := Application.ExeName;
     pFileName := StrAlloc( Length( sFileName ) + 1 );
     try
        pFileName := StrPCopy( pFileName, sFileName );
        Size := Windows.GetFileVersionInfoSize( pFileName, Value );
        if ( Size > 0 ) then
        begin
             GetMem( Data, Size );
             try
                if Windows.GetFileVersionInfo( pFileName, Value, Size, Data ) then
                begin
                     if Windows.VerQueryValue( Data, '\', Info, iLen ) then
                     begin
                          with Result, PVSFixedFileInfo( Info )^ do
                          begin
	                       MajorVersion := HighWord( dwFileVersionMS );
                               MinorVersion := LowWord( dwFileVersionMS );
                               Release := HighWord( dwFileVersionLS );
                               Build := LowWord( dwFileVersionLS );
                          end;
                     end;
                end;
             finally
                    FreeMem( Data );
             end;
        end;
     finally
            StrDispose( pFileName );
     end;
end;

function GetApplicationVersionNumber: String;
begin
     VersionArchivo := GetVersionInfo;
     with VersionArchivo do
     begin
          Result := 'Versi�n ' +
                    IntToStr( MajorVersion ) +
                    '.' +
                    IntToStr( MinorVersion ) +
                    '.' +
                    IntToStr( Release );
     end;
end;

function GetApplicationVersionBuild: String;
begin
     VersionArchivo := GetVersionInfo;
     with VersionArchivo do
          Result := Format( 'Build %d.%d.%d.%d', [ MajorVersion, MinorVersion, Release, Build ] );
end;

function GetApplicationVersionInfo( const sItem: String ): String;
const
     K_LEE_FILE_INFO = '\\StringFileInfo\\040904E4\\%s';
var
   Size, Value: DWord;
   pFileName: {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif};
   sFileName: String;
   Data, Info: Pointer;
   iLen: UINT;
begin
     Result := VACIO;
     sFileName := Application.ExeName;
     pFileName := StrAlloc( Length( sFileName ) + 1 );
     try
        pFileName := StrPCopy( pFileName, sFileName );
        Size := Windows.GetFileVersionInfoSize( pFileName, Value );
        if ( Size > 0 ) then
        begin
             GetMem( Data, Size );
             try
                if Windows.GetFileVersionInfo( pFileName, Value, Size, Data ) then
                begin
                     if Windows.VerQueryValue( Data, {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( Format( K_LEE_FILE_INFO, [ sItem ] ) ), Info, iLen ) then
                        Result := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}( Info );
                end;
             finally
                    FreeMem( Data );
             end;
        end;
     finally
            StrDispose( pFileName );
     end;
end;

function GetApplicationProductVersion: String;
begin
     Result := 'Versi�n ' + GetApplicationVersionInfo( 'ProductVersion' );
end;

function GetApplicationProductVersionSimple: String;
var
   iPos: Integer;
   sVersion: String;
begin
     sVersion:= GetApplicationVersionInfo( 'ProductVersion' );
     iPos := AnsiPos('.', sVersion);
     if ( iPos > 0 ) then
        Result := AnsiLeftStr( sVersion, ( iPos - 1 )  )
     else
        Result := sVersion;

end;

function ValorCampo( oCampo: TField ): Variant;
begin
     with oCampo do
     begin
          if VarIsEmpty( NewValue ) then
          begin
               if not VarIsNull( OldValue ) then
               begin
                    if ( DataType = ftDateTime ) then
                       Result := VarAsType(OldValue, varDate)
                    else
                        Result := OldValue;
               end
               else
               begin
                    if DataType in [ ftString, ftBlob ] then
                       Result := ''
                    else
                       Result := 0;
               end;
          end
          else
               Result := NewValue;
     end;
end;

function ValorCampoOLE( oCampo: TField ): OLEVariant;
begin
     with oCampo do
     begin
          if VarIsEmpty( NewValue ) then
          begin
               if not VarIsNull( OldValue ) then
               begin
                    if ( DataType = ftDateTime ) then
                       Result := VarAsType(OldValue, varDate)
                    else
                        Result := OldValue;
               end
               else
               begin
                    if DataType in [ ftString, ftBlob ] then
                       Result := ''
                    else
                       Result := 0;
               end;
          end
          else
               Result := NewValue;
     end;
end;

procedure SetEnabled( Panel: TPanel; const lEnabled: Boolean );
var
   i: Integer;
begin
     with Panel do
     begin
          for i := 0 to ( ControlCount - 1 ) do
          begin
               Controls[ i ].Enabled := lEnabled;
               if not lEnabled and ( Tag > 0 ) then
                  Caption := '';
          end;
     end;
end;

procedure SetEnabledControl( oParentControl: TWinControl; const lEnabled: Boolean );
var
   i: Integer;
begin
     with oParentControl do
     begin
          for i := 0 to ( ControlCount - 1 ) do
          begin
               Controls[ i ].Enabled := lEnabled;
          end;
     end;
end;

procedure CierraDatasets( DataModule: TDataModule );
var
   i: Integer;
begin
     with DataModule do
     begin
          for i := 0 to ( ComponentCount - 1 ) do
          begin
               if Components[ i ] is TDataset then
               begin
                    TDataset( Components[ i ] ).Active := False;
               end;
          end;
     end;
end;

procedure EnviaMensaje( oControl: TWinControl; const sTexto: String );
var
   i : Integer;
begin
     if ( oControl <> nil ) and  strLleno( sTexto ) then
     begin
          for i := 1 to Length( sTexto ) do
          begin
               PostMessage( oControl.Handle, WM_CHAR, Ord( sTexto[i] ), 1 );
//               Application.ProcessMessages;
          end;
//          keybd_event( 10, 0, 0, 0 );
          keybd_event( 13, 0, 0, 0 );
     end;
end;

function GetScaledHeight( const iAltura: Integer ): Integer;
begin
     Result := Trunc( iAltura * ( Screen.PixelsPerInch / K_PIXELES_PULGADA ) );
end;

function PK_Violation( Error: EReconcileError ): Boolean;
begin
     with Error do
     begin
          Result := {( ErrorCode = 3604 ) or} 
                    (  Pos( 'VIOLATION OF PRIMARY', UpperCase( Message )) > 0 );
     end;
end;

function FK_Violation( Error: EReconcileError ): Boolean;
begin
     with Error do
     begin
          Result := (  Pos( 'CONFLICTED WITH THE REFERENCE CONSTRAINT', UpperCase( Message )) > 0 ) or
                    (  Pos( 'VIOLATION OF FOREIGN KEY', UpperCase( Message )) > 0 ) or
                    (  Pos( 'FOREIGN KEY', UpperCase( Message )) > 0 );//US 14933: El usuario requiere que los procesos de nomina sean funcionales con cualquier tipo de periodo (Parte1)
     end;
end;

function UK_Violation( Error: EReconcileError ): Boolean;
begin
     with Error do
     begin
          Result := (  Pos( 'UNIQUE INDEX', UpperCase( Message )) > 0 )
     end;
end;

{*** US 13905: El usuario necesita realizar la consulta y edici�n de Acumulados por Razon Social para as� revisar la informacion y hacer los ajustes necesarios para sus procesos de Declaracion Anual, Diferencias, Reportes , etc.***}
function UKC_Violation( Error: EReconcileError ): Boolean;
begin
     with Error do
     begin
          Result := (  Pos( 'UNIQUE KEY', UpperCase( Message )) > 0 )
     end;
end;
{*** FIN ***}

procedure SetControlEnabled( oControl: TWinControl; oLabel: TLabel; const lEnabled: Boolean );
begin
     if oControl.Visible then
     begin
          oControl.Enabled := lEnabled;
          oLabel.Enabled := lEnabled;
     end;
end;

function FechaResidencia( const rYear, rMonth: Real ): TDate;
begin
     Result := Date - Round( ( rYear  * K_DIAS_ANNO_PROMEDIO ) + ( rMonth * K_DIAS_MES_PROMEDIO ) );
end;

procedure SetYearsMonths( const dFecha: TDate; var rYear, rMonth: Real );
var
   rDays : Real;
begin
     rDays  := ( Date - dFecha + 1 );
     rYear  := Trunc( rDays / K_DIAS_ANNO_PROMEDIO );
     rMonth := MiRound( ( ModReal( rDays, K_DIAS_ANNO_PROMEDIO ) / K_DIAS_MES_PROMEDIO ), 1 );
end;

procedure LlenaTasaAnterior( oComboBox: TZetaKeyCombo );
var
   i: Integer;
begin
     with oComboBox.Items do
     begin
          Clear;
          for i := Low( ZetaCommonClasses.aArregloTasa ) to High( ZetaCommonClasses.aArregloTasa ) do
          begin
	       Add( ZetaCommonClasses.aArregloTasa[i] );
          end;
      end;
end;

function VerificaValorTasa( oValor: TTasa ): Boolean;
var
   i: Integer;
   rValor: TTasa;
begin
     if( oValor <> 0 )then
     begin
          for i := Low( ZetaCommonClasses.aArregloTasa ) to High( ZetaCommonClasses.aArregloTasa ) do
          begin
               try
                  rValor := StrToFloat( ZetaCommonClasses.aArregloTasa[i] )
               except
                     rValor := 0;
               end;
               if ZetaCommonTools.PesosIguales( rValor, oValor ) then
               begin
                    Result := True;
                    break;
               end
               else
                   Result := False;
          end;
     end
     else
         Result := True;
end;

procedure OrdenarPor( DataSet: TClientDataset; const FieldName: String );
var
   sField, sIndex: String;
   lDescendente: TIndexOptions;
begin
     sField := FieldName+'Index';
     with DataSet do
     begin
          sIndex := IndexName;
          if sIndex <> '' then
          begin
               GetIndexInfo(sIndex);
               IndexDefs.Update;
               with IndexDefs.Find(sIndex) do
               begin
                    if (sField = sIndex) AND (Options = []) then
                       lDescendente := [ixDescending]
                    else lDescendente := [];
               end;
               DeleteIndex(sIndex);
          end
          else
              lDescendente := [];

          AddIndex( sField, FieldName, lDescendente);
          IndexName := sField;
          First;
     end;
end;

function SplitList(Delimiter: Char; Str: string; ListOfStrings: TStrings):Boolean ;
var
i: Integer;
begin
     Result := TRUE;
     ListOfStrings.Clear;
     ListOfStrings.Delimiter     := Delimiter;
     ListOfStrings.DelimitedText := Str;
     for i:= 0 to ListOfStrings.Count - 1 do
     begin
          try
             StrToInt( ListOfStrings[i] );
          except
                Result := FALSE;
                Break;
          end;
     end;
end;
{
procedure LlenaListaBoolean( oListaNames, oListaValues: TStrings );
begin
     with oListaNames do
     begin
          BeginUpdate;
          try
              Clear;
              Add( K_PROPPERCASE_SI );
              Add( K_PROPPERCASE_NO );
              Add( 'Usar Global de Empresa' );
          finally
                 EndUpdate;
          end;
     end;
     with oListaValues do
     begin
          BeginUpdate;
          try
              Clear;
              Add( K_GLOBAL_SI );
              Add( K_GLOBAL_NO );
              Add( K_USAR_GLOBAL );
          finally
                 EndUpdate;
          end;
     end;
end;
}

function CambiaCampo( oCampo: TField ): Boolean;
begin
     with oCampo do
     begin
          if IsNull then
             Result := False
          else
              if ( DataType = ftDateTime ) then
                 Result := ( VarAsType( OldValue, varDate ) <> NewValue )
              else
                  Result := ( OldValue <> NewValue );
     end;
end;

initialization
begin
     with VersionArchivo do
     begin
          MajorVersion := 0;
          MinorVersion := 0;
          Release := 0;
          Build := 0;
     end;
end;

end.

