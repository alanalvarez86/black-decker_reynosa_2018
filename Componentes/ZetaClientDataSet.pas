unit ZetaClientDataSet;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DbClient, Mask,
     ActiveX, DSIntf, DBCommon, StdVcl,
     {$ifndef VER130}Variants,MaskUtils,{$endif}
     {$ifdef TRESS_DELPHIXE5_UP}System.UITypes,{$endif}
     ZetaCommonLists,
     ZetaCommonClasses;

{$INCLUDE JEDI.INC}
type
  TZetaClientDataSet = class;
  TZetaLookupDataSet = class;
  TDescriptionEvent = procedure( Sender: TZetaLookupDataset; var sDescription: String ) of object;
  TSearchEvent = procedure( Sender: TZetaLookupDataset; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String ) of object;
  TLookupKeyEvent = procedure( Sender: TZetaLookupDataset; var lOk: Boolean; const sFilter, sKey: String; var sDescription: String ) of object;
  TRightsEvent = procedure( Sender: TZetaClientDataset; const iRight: Integer; var lHasRights: Boolean ) of object;
  TZetaClientDataSet = class(TClientDataSet)
  private
    { Private declarations }
    FAlAdquirirDatos: TNotifyEvent;
    FAlEnviarDatos: TNotifyEvent;
    FAlCrearCampos: TNotifyEvent;
    FAlAgregar: TNotifyEvent;
    FAlBorrar: TNotifyEvent;
    FAlModificar: TNotifyEvent;
    FHuboErrores: Boolean;
    FLlaveError: Variant;
    FCamposLlaveError: String;
    FListaLlave: TList;
    FUsaCache: Boolean;
    FDataChange: Boolean;
    FEnviando: Boolean;
    function GetValor(Campo: TField): Variant;
    function GetCacheFolder: String;
    function GetDeltaNull: OleVariant;
    procedure BoolGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure FechaGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GrabaArchivo;
    procedure ObtieneElementoGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TimeGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure AddField(const sNombre: String; const Tipo: TFieldType; const iSize: Integer);
    //procedure ValidaOrdenPreRefrescar;
  protected
    { Protected declarations }
    procedure CreateFields; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property HuboErrores: Boolean read FHuboErrores;
    property DeltaNull: OleVariant read GetDeltaNull;
    function EsModificable: Boolean;
    function HayQueRefrescar: Boolean;
    function PosicionaError: Boolean;
    function Reconciliar( const ApplyResults: OleVariant ): Boolean;
    function ReconciliaError( DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError; CamposLlave, ErrorMsg: String ): TReconcileAction;
    procedure CreateCalculated(const sFieldName: String; const Tipo: TFieldType; const iAncho: Integer);
    procedure CreateLookup(Fuente: TZetaClientDataset; const sFieldName, sKeyFields, sLookupKeyFields, sResultField: String );
    procedure CreateSimpleLookup(Fuente: TZetaLookupDataset; const sFieldName, sKeyFields: String);
    procedure AddBlobField(const sNombre: String; const iSize: Integer);
    procedure AddBooleanField(const sNombre: String);
    procedure AddDateField(const sNombre: String);
    procedure AddDateTimeField(const sNombre: String);
    procedure AddFloatField(const sNombre: String);
    procedure AddIntegerField(const sNombre: String);
    procedure AddMemoField(const sNombre: String; const iSize: Integer);
    procedure AddStringField(const sNombre: String; const iSize: Integer);
    procedure Agregar;
    procedure Borrar;
    procedure Conectar;
    procedure CreateTempDataset;
    procedure Enviar;
    procedure Init;
    procedure Modificar;
    procedure Refrescar;
    procedure SetDataChange;
    procedure ResetDataChange;
    procedure PostData;
  published
    { Published declarations }
    property AlAdquirirDatos: TNotifyEvent read FAlAdquirirDatos write FAlAdquirirDatos;
    property AlEnviarDatos: TNotifyEvent read FAlEnviarDatos write FAlEnviarDatos;
    property AlCrearCampos: TNotifyEvent read FAlCrearCampos write FAlCrearCampos;
    property AlAgregar: TNotifyEvent read FAlAgregar write FAlAgregar;
    property AlBorrar: TNotifyEvent read FAlBorrar write FAlBorrar;
    property AlModificar: TNotifyEvent read FAlModificar write FAlModificar;
    property UsaCache: Boolean read FUsaCache write FUsaCache default False;
    procedure CampoNoCero(const Campo, Descripcion: String);
    procedure ListaFija(const Campo: String; const Lista: ListasFijas);
    procedure MaskBool(const Campo: String);
    procedure MaskFecha(const Campo: String);
    procedure MaskHoras(const Campo: String);
    procedure MaskNumerico(const Campo, Mascara: String);
    procedure MaskPesos(const Campo: String);
    procedure MaskTasa(const Campo: String);
    procedure MaskFloat(const Campo: String; const iDecimales:integer);
    procedure MaskTipoCambio(const Campo: String);
    procedure MaskTime(const Campo: String);
  end;
  TZetaLookupDataSet = class(TZetaClientDataSet)
  private
    { Private declarations }
    FLookupName: String;
    FDescriptionField: String;
    FKeyField: String;
    FActivoField: String;
    FConfidenField: String;
    FOnGetDescription: TDescriptionEvent;
    FOnLookupKey: TLookupKeyEvent;
    FOnSearch: TSearchEvent;
    FOnGetRights: TRightsEvent;
    function GetKeyFieldSize: Integer;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property LookupKeyFieldSize: Integer read GetKeyFieldSize;
    function GetDescription: String;
    function LookupKey( const sKey, sFilter: String; var sDescription: String; var lActive, lConfidential : boolean ): Boolean; overload;
    function LookupKey( const sKey, sFilter: String; var sDescription: String ): Boolean; overload;
    function PuedeConsultar: Boolean;
    function Search( const sFilter: String; var sKey, sDescription: String ): Boolean; overload;
    function Search_DevEx( const sFilter: String; var sKey, sDescription: String ): Boolean; overload;
    function Search( const sFilter: String; var sKey, sDescription: String; lSoloActivos, lUsarConfidencialidad : boolean  ): Boolean; overload;
    function Search_DevEx( const sFilter: String; var sKey, sDescription: String; lSoloActivos, lUsarConfidencialidad : boolean ): Boolean; overload;
    function GetDescripcion( const sCodigo: String ): String;
    function GetRights( const iRight: Integer ): Boolean;
    function GetIsActive: Boolean;
    function GetIsConfidential: Boolean;
  published
    { Published declarations }
    property LookupName: String read FLookupName write FLookupName;
    property LookupDescriptionField: String read FDescriptionField write FDescriptionField;
    property LookupKeyField: String read FKeyField write FKeyField;
    property LookupActivoField: String read FActivoField write FActivoField;
    property LookupConfidenField: String read FConfidenField write FConfidenField;
    property OnLookupDescription: TDescriptionEvent read FOnGetDescription write FOnGetDescription;
    property OnLookupKey: TLookupKeyEvent read FOnLookupKey write FOnLookupKey;
    property OnLookupSearch: TSearchEvent read FOnSearch write FOnSearch;
    property OnGetRights: TRightsEvent read FOnGetRights write FOnGetRights;
  end;

//function PK_Violation( Error: EReconcileError ): Boolean;
function CodigosRepetidos( Error: EDatabaseError ):Boolean;
function GetDBErrorDescription( Error: EDatabaseError ): String;
function GetErrorDescription( Error: EReconcileError ): String;

var
   CacheActive : Boolean;
   CacheFolder : String;
   CacheSistemaActive : Boolean;
   CacheSistemaFolder : String;

{ GlobalSoloKeyActivos tiene como valor default FALSE,
  se especifica el valor al conectar empresa en los ejecutables TRESS,SUPERVISORES,LABOR,RECLASIFICACION DE PLAZAS y CAJA DE AHORRO
  AV: 16-Nov-2010
  }
   GlobalSoloKeysActivos : Boolean = FALSE;

   GlobalListaConfidencialidad : string = '';

implementation

uses ZetaMsgDlg,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaBusqueda_DevEx;

const
     K_DM_SISTEMA = 'dmSistema';

{function PK_Violation( Error: EReconcileError ): Boolean;
begin
     with Error do
     begin
          Result := ( ErrorCode = 3604 ) or
                    ( Pos( 'PRIMARY', Message ) > 0 );
     end;
end;}

function CodigosRepetidos( Error: EDatabaseError ):Boolean;
begin
     with Error do
     begin
          Result := ( Pos( 'LINKFIELDS TO DETAIL MUST BE UNIQUE.', UpperCase(Message) ) > 0 ) or
                    ( Pos( 'KEY VIOLATION.', UpperCase(Message) ) > 0 );
     end;
end;

function GetDBErrorDescription( Error: EDatabaseError ): String;
begin
     if CodigosRepetidos( Error ) then
        Result := '� C�digo Ya Existe !'
     else
         Result := Error.message;
end;

function GetErrorDescription( Error: EReconcileError ): String;
begin
     if ( PK_Violation( Error ) ) then
        Result := '� C�digo Ya Existe !'
     else
     begin
          with Error do
          begin
               Result := Message;
               if ( Context <> '' ) then
                  Result := Result + '; ' + Context;
          end;
     end;
end;

function SetFileNamePath( const sPath, sFile: String ): String;
const
     EXTENSION = '.cds';
begin
     Result := sPath;
     if ( Result[ Length( Result ) ] <> '\' ) then
        Result := Result + '\';
     Result := Result + sFile + EXTENSION;
end;

{ ********* TZetaClientDataSet *************** }

constructor TZetaClientDataSet.Create( AOwner: TComponent );
begin
     FUsaCache := False;
     FDataChange := False;
     FEnviando := False;
     inherited Create( AOwner );
end;

destructor TZetaClientDataSet.Destroy;
begin
     FreeAndNil( FListaLlave );
     inherited;
end;

function TZetaClientDataSet.Reconciliar( const ApplyResults: OleVariant ): Boolean;
begin
     Result := Reconcile( ApplyResults );
     FHuboErrores := not Result;
end;

function TZetaClientDataSet.GetValor( Campo: TField ): Variant;
begin
     with Campo do
     begin
          if DataType in [ ftDate, ftDateTime ] then
             Result := FormatDateTime( 'dd/mm/yyyy', AsDateTime )
          else
              Result := Value;
     end;
end;

function TZetaClientDataSet.ReconciliaError(DataSet: TDataSet; UpdateKind: TUpdateKind; ReconcileError: EReconcileError; CamposLlave, ErrorMsg : String): TReconcileAction;
var
   i: Integer;
begin
     MessageDlg( ErrorMsg, mtError, [ mbOK ], 0 );
     if ( UpdateKind = ukDelete ) then
        Result := raCancel
     else
         Result := raAbort;
     FCamposLlaveError := CamposLlave;
     { No es com�n llegar a usar este m�todo, por eso no se crea en el 'Create' }
     if ( FListaLlave = nil ) then
        FListaLlave := TList.Create
     else
         FListaLlave.Clear;
     DataSet.GetFieldList( FListaLlave, CamposLlave );
     with FListaLlave do
     begin
          FLlaveError := VarArrayCreate( [ 0, Count - 1 ], varVariant );
          for i := 0 to ( Count - 1 ) do
          begin
               FLlaveError[ i ] := GetValor( TField( Items[ i ] ));
          end;
     end;
end;

function TZetaClientDataSet.PosicionaError: Boolean;

  function EsLlaveError: Boolean;
  var
     i: Integer;
  begin
       Result := True;
       with FListaLlave do
       begin
            for i := 0 to ( Count - 1 ) do
            begin
                 if ( FLlaveError[ i ] <> GetValor( TField( Items[ i ] ) ) ) then
                 begin
                      Result := False;
                      Exit;
                 end;
            end;
       end;
  end;

begin  // PosicionaError
     {
     Result := Locate( FCamposLlaveError, FLlaveError, [] );
     }
     FListaLlave.Clear;
     GetFieldList( FListaLlave, FCamposLlaveError );
     Last;
     Result := FALSE;
     while not Bof do
     begin
          if EsLlaveError then
          begin
               Result := TRUE;
               Exit;
          end;
          Prior;
     end;
end;

procedure TZetaClientDataSet.Init;
begin
     Active := False;
     Filtered := FALSE;
     Filter := '';
     IndexFieldNames := '';
     while FieldCount > 0 do
     begin
          Fields[ FieldCount - 1 ].DataSet := nil;
     end;
     FieldDefs.Clear;
end;

procedure TZetaClientDataset.AddField( const sNombre: String; const Tipo: TFieldType; const iSize: Integer );
begin
     with FieldDefs.AddFieldDef do
     begin
          Name := sNombre;
          DisplayName := sNombre;
          DataType := Tipo;
          Size := iSize;
     end;
end;

procedure TZetaClientDataset.CreateTempDataset;
var
   i: Integer;                          
begin
     for i := 0 to ( FieldDefs.Count - 1 ) do
     begin
          FieldDefs[ i ].CreateField( Self );
     end;
     CreateDataset;
end;

procedure TZetaClientDataset.AddStringField( const sNombre: String; const iSize: Integer );
begin
     AddField( sNombre, ftString, iSize );
end;

procedure TZetaClientDataset.AddFloatField( const sNombre: String );
begin
     AddField( sNombre, ftFloat, 0 );
end;

procedure TZetaClientDataset.AddBlobField(const sNombre: String; const iSize: Integer);
begin
     AddField( sNombre, ftBlob, iSize );
end;

procedure TZetaClientDataset.AddBooleanField(const sNombre: String);
begin
     AddField( sNombre, ftBoolean, 0 );
end;

procedure TZetaClientDataset.AddDateField(const sNombre: String);
begin
     AddField( sNombre, ftDate, 0 );
end;

procedure TZetaClientDataset.AddDateTimeField(const sNombre: String);
begin
     AddField( sNombre, ftDateTime, 0 );
end;

procedure TZetaClientDataset.AddIntegerField(const sNombre: String);
begin
     AddField( sNombre, ftInteger, 0 );
end;

procedure TZetaClientDataset.AddMemoField(const sNombre: String; const iSize: Integer);
begin
     AddField( sNombre, ftMemo, 0 );
end;

procedure TZetaClientDataSet.CreateFields;
begin
     inherited CreateFields;
     if Assigned( FAlCrearCampos ) and not ( csReading in ComponentState ) then
     begin
          FAlCrearCampos( Self );
     end;
end;

procedure TZetaClientDataset.CreateSimpleLookup( Fuente: TZetaLookupDataset; const sFieldName, sKeyFields: String );
begin
     CreateLookup( Fuente, sFieldName, sKeyFields, Fuente.LookupKeyField, Fuente.LookupDescriptionField );
end;

procedure TZetaClientDataset.CreateLookup( Fuente: TZetaClientDataset; const sFieldName, sKeyFields, sLookupKeyFields, sResultField: String );
var
   FieldClassType: TFieldClass;
   Campo: TField;
   CampoDef: TFieldDef;
begin
     with Fuente do
     begin
          if not Active then
            DatabaseError( 'Falta Conectar ' + Name );
          CampoDef := FieldDefs.Find( sResultField );
          FieldClassType := GetFieldClass( CampoDef.DataType );
          if ( FieldClassType = nil ) then
             DatabaseError( 'Tipo de Campo Lookup Desconocido', Self );
     end;
     Campo := FieldClassType.Create( Self );
     try
        with Campo do
        begin
             Size := CampoDef.Size;
             SetFieldType( CampoDef.DataType );
             FieldName := sFieldName;
             Name := sFieldName;
             Required := False;
             ReadOnly := True;
             FieldKind := fkLookup;
             Lookup := True;
             KeyFields := sKeyFields;
             LookupDataset := Fuente;
             LookupKeyFields := sLookupKeyFields;
             //DevEx: Se agrego Reset del resultfield cuando el keyfield esta vacio by:mp
             if(keyfields='')then
             begin
             LookupResultField := '';
             end
             else
             begin
             LookupResultField := sResultField;
             end;
             //
             Dataset := Self;
        end;
     except
           Campo.Free;
           raise;
     end;
end;

procedure TZetaClientDataset.CreateCalculated( const sFieldName: String; const Tipo: TFieldType; const iAncho: Integer );
var
   FieldClassType: TFieldClass;
   Campo: TField;
begin
     FieldClassType := GetFieldClass( Tipo );
     if ( FieldClassType = nil ) then
        DatabaseError( 'Tipo de Campo Calculado Desconocido', Self );
     Campo := FieldClassType.Create( Self );
     try
        with Campo do
        begin
             Size := iAncho;
             SetFieldType( Tipo );
             FieldName := sFieldName;
             Name := sFieldName;
             Required := False;
             ReadOnly := False;
             FieldKind := fkCalculated;
             Lookup := False;
             Dataset := Self;
        end;
     except
           Campo.Free;
           raise;
     end;
end;

procedure TZetaClientDataSet.Agregar;
begin
     if Assigned( FAlAgregar ) then
        FAlAgregar( Self )
     else
     begin
          Append;
          Modificar;
     end;
end;

procedure TZetaClientDataSet.Conectar;
var
   sArch : String;
begin
     if not Active or HayQueRefrescar then
     begin
          if CacheActive and UsaCache and ( ( Owner.Name <> K_DM_SISTEMA ) or ( ( Owner.Name = K_DM_SISTEMA ) and CacheSistemaActive ) ) then
          begin
               sArch := SetFileNamePath( GetCacheFolder, Name );
               if FileExists( sArch ) then
               begin
                    LoadFromFile( sArch );
                    ResetDataChange;
               end
               else
                    Refrescar;
          end
          else
              Refrescar;
     end;
end;

procedure TZetaClientDataSet.Borrar;
begin
     if Assigned( FAlBorrar ) then
        FAlBorrar( Self )
     else
     begin
          if ZetaMsgDlg.ConfirmaCambio( '� Desea Borrar Este Registro ?') then
          begin
               Delete;
          end;
     end;
end;

function TZetaClientDataSet.EsModificable: Boolean;
begin
     Result := Assigned( FAlModificar );
end;

procedure TZetaClientDataSet.Modificar;
begin
     if Assigned( FAlModificar ) then
        FAlModificar( Self );
end;

procedure TZetaClientDataSet.Refrescar;

begin
     if Assigned( FAlAdquirirDatos ) then
     begin
          DisableControls;
          try
             FAlAdquirirDatos( Self );
             ResetDataChange;
          finally
             EnableControls;
          end;
     end;
     GrabaArchivo;
end;

{procedure TZetaClientDataSet.ValidaOrdenPreRefrescar;
begin
      try
          if IndexName <> '' then
          begin
            if not IndexDefs.Updated then
                IndexDefs.Update;
            if IndexDefs.Find(IndexName) = nil then
                IndexName := '';
          end;
      except
          IndexName := '';
      end;
end;}

procedure TZetaClientDataSet.Enviar;
begin
     if Assigned( FAlEnviarDatos ) then
     begin
          { Con esta bandera el ZetaClientDataset evita que los mensajes }
          { de SetDataChange tengan efectos sobre si mismo }
          FEnviando := True;
          try
             FAlEnviarDatos( Self );
          finally
                 FEnviando := False;
          end;
     end
     else
     begin
          Post;
     end;
     GrabaArchivo;
end;

procedure TZetaClientDataSet.GrabaArchivo;
begin
     if Active and CacheActive and UsaCache and ( ( Owner.Name <> K_DM_SISTEMA ) or ( ( Owner.Name = K_DM_SISTEMA ) and CacheSistemaActive ) ) then
        SaveToFile( SetFileNamePath( GetCacheFolder, Name ) );
end;

function TZetaClientDataSet.GetCacheFolder: String;
begin
     if ( Owner.Name = K_DM_SISTEMA ) then
        Result := CacheSistemaFolder
     else
         Result := CacheFolder;
end;

procedure TZetaClientDataSet.ObtieneElementoGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if IsEmpty then
             Text := ''
          else
              Text := ObtieneElemento( ListasFijas( Sender.Tag ), Sender.AsInteger );
     end
     else
         Text:= Sender.AsString;

end;

procedure TZetaClientDataSet.ListaFija( const Campo: String; const Lista: ListasFijas );
var
   oCampo: TField;
begin
     oCampo := FindField( Campo );
     if ( oCampo <> nil )  then
     begin
          with oCampo do
          begin
               OnGetText := ObtieneElementoGetText;
               Alignment := taLeftJustify;
               Tag := Ord( Lista );
          end;
     end;
end;

procedure TZetaClientDataSet.FechaGetText( Sender: TField; var Text: String; DisplayText: Boolean );
var
   dValue: TDate;
begin
     if DisplayText then
     begin
          if IsEmpty then
             Text := ''
          else
          begin
               dValue := Sender.AsDateTime;
               if ( dValue <= 0 ) then
                  Text := ''
               else
                  Text := FormatDateTime( {$IFDEF DELPHIXE3_UP}FormatSettings.{$ENDIF}ShortDateFormat , dValue );
          end;
     end;
end;

procedure TZetaClientDataSet.MaskFecha( const Campo: String );
var
   oCampo: TField;
begin
     oCampo := FindField( Campo );
     if ( oCampo <> nil ) and ( oCampo is TDateTimeField ) then
     begin
          with oCampo do
          begin
               OnGetText := FechaGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

procedure TZetaClientDataSet.TimeGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if IsEmpty then
             Text := ''
          else
             Text := FormatMaskText( '99:99;0', Sender.AsString );
     end
     else
          Text := Sender.AsString;
end;

procedure TZetaClientDataSet.MaskTime(const Campo: String);
var
   oCampo: TField;
begin
     oCampo := FindField( Campo );
     if ( oCampo <> nil ) and ( oCampo is TStringField ) then
     begin
          with oCampo do
          begin
               OnGetText := TimeGetText;
               Alignment := taRightJustify;
               EditMask := '!90:00;0';
          end;
     end;
end;

procedure TZetaClientDataSet.BoolGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if IsEmpty then
             Text := ''
          else
              if ZetaCommonTools.zStrToBool( Sender.AsString ) then
                 Text := 'Si'
              else
                  Text := '';
     end
     else
          Text := Sender.AsString;
end;

procedure TZetaClientDataSet.MaskBool(const Campo: String);
var
   oCampo: TField;
begin
     oCampo := FindField( Campo );
     if ( oCampo <> nil ) and ( oCampo is TStringField ) then
     begin
          with oCampo do
          begin
               OnGetText := BoolGetText;
               Alignment := taLeftJustify;
               EditMask := 'A';
          end;
     end;
end;

procedure TZetaClientDataSet.MaskNumerico( const Campo, Mascara: String );
var
   oCampo: TField;
begin
     oCampo := FindField( Campo );
     if ( oCampo <> nil ) and ( oCampo is TNumericField ) then
     begin
          TNumericField( oCampo ).DisplayFormat := Mascara;
     end;
end;

procedure TZetaClientDataSet.MaskPesos( const Campo: String );
begin
     MaskNumerico( Campo, '#,0.00' );
end;

procedure TZetaClientDataSet.MaskTasa( const Campo: String );
begin
     MaskNumerico( Campo, '#,0.##### %' );
end;

procedure TZetaClientDataset.MaskTipoCambio(const Campo: String);
begin
     MaskNumerico( Campo, '#,0.000000' );
end;

procedure TZetaClientDataSet.MaskFloat(const Campo: String; const iDecimales:integer );
begin
     MaskNumerico( Campo, Format( '#,0.%s', [ Replicate( '0', iDecimales ) ] ) );
end;

procedure TZetaClientDataSet.MaskHoras( const Campo: String );
begin
     MaskNumerico( Campo, '#0.00;;#' );
end;

procedure TZetaClientDataSet.CampoNoCero( const Campo, Descripcion: String );
var
   oCampo: TField;
begin
     oCampo := FindField( Campo );
     if ( oCampo <> nil ) and ( oCampo.AsFloat = 0 ) then
     begin
          oCampo.FocusControl;
          raise Exception.Create( '� Campo ' + Descripcion + ' No Puede Ser Cero !' );
     end;
end;

function TZetaClientDataset.HayQueRefrescar: Boolean;
begin
     Result := FDataChange;
end;

procedure TZetaClientDataSet.SetDataChange;
begin
     if not FEnviando then
        FDataChange := True;
end;

procedure TZetaClientDataSet.ResetDataChange;
begin
     FDataChange := False;
end;

function TZetaClientDataSet.GetDeltaNull: OleVariant;
begin
     if ( ChangeCount > 0 ) then
        Result := Delta
     else
        TVarData( Result ).VType := varNull;
end;

procedure TZetaClientDataSet.PostData;
begin
     if ( State in [ dsEdit, dsInsert ] ) then
        Post;
end;

{ ********* TZetaLookupDataSet *********** }

function TZetaLookupDataSet.GetRights( const iRight: Integer ): Boolean;
begin
     if Assigned( FOnGetRights ) then
        FOnGetRights( Self, iRight, Result )
     else
         Result := True;
end;

function TZetaLookupDataSet.PuedeConsultar: Boolean;
begin
     Result := GetRights( K_DERECHO_CONSULTA );
end;

function TZetaLookupDataSet.GetDescripcion(const sCodigo: String): String;
begin
     Result := '';
     LookUpKey( sCodigo, '', Result );
end;

function TZetaLookupDataSet.GetDescription: String;
var
   sDescription: String;
begin
     if Assigned( FOnGetDescription ) then
     begin
          FOnGetDescription( Self, sDescription );
          Result := sDescription;
     end
     else
         if ( FDescriptionField = '' ) then
            Result := ''
         else
             Result := FieldByName( FDescriptionField ).AsString;
end;

function TZetaLookupDataSet.GetKeyFieldSize: Integer;
var
   KeyField: TField;
begin
     Result := 0;
     if Active then
     begin
          KeyField := FindField( FKeyField );
          if ( KeyField <> nil ) then
             Result := KeyField.Size;
     end;
end;

function TZetaLookupDataSet.LookupKey( const sKey, sFilter: String; var sDescription: String; var lActive, lConfidential: boolean): Boolean;
var
   KeyField: TField;
   Pos : TBookMark;
begin
     lActive := True;
     lConfidential := False;

     if Assigned( FOnLookupKey ) then
     begin
          FOnLookupKey( Self, Result, sFilter, sKey, sDescription );
     end
     else
     begin
          DisableControls;
          try
             KeyField := FindField( FKeyField );
             if ( KeyField <> nil ) then
             begin
                  if ( sFilter <> '' ) then
                  begin
                       Filtered := False;
                       Filter := sFilter;
                       Filtered := True;
                  end;
                  if ( KeyField.DataType = ftString ) then
                     Result := Locate( FKeyField, sKey, [] ) { Ten�a antes [ LoPartialKey ]. � Por qu� ? }
                  else
                      Result := Locate( FKeyField, StrToIntDef( sKey, 0 ), [] ); { Ten�a antes [ LoPartialKey ]. � Por qu� ? }
                  if Result then
                  begin
                     sDescription := GetDescription;
                     lActive := GetIsActive;
                     lConfidential := GetIsConfidential;
                  end;

                  if ( sFilter <> '' ) then
                  begin
                       Pos:= GetBookMark;

                       Filtered := False;
                       Filter := '';

                       if ( Pos <> nil ) then
                       begin
                            if BookMarkValid( Pos ) then
                               GotoBookMark( Pos );
                            FreeBookMark( Pos );
                       end;
                  end;
             end;
          finally
                 EnableControls;
          end;
     end;
end;

function TZetaLookupDataSet.Search( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     if Assigned( FOnSearch ) then
        FOnSearch( Self, Result, sFilter, sKey, sDescription )
     else
         Result := ZetaBusqueda_DevEx.ShowSearchForm( Self, sFilter, sKey, sDescription );
end;

//DevEx (by am): Se realizo un metodo especial para invocar la forma de busqueda de la nueva interfaz.
Function TZetaLookupDataSet.Search_DevEx( const sFilter: String; var sKey, sDescription: String ): Boolean;
begin
     if Assigned( FOnSearch ) then
        FOnSearch( Self, Result, sFilter, sKey, sDescription )
     else
     Result := ZetaBusqueda_DevEx.ShowSearchForm( Self, sFilter, sKey, sDescription );
end;

function TZetaLookupDataSet.Search( const sFilter: String; var sKey, sDescription: String; lSoloActivos, lUsarConfidencialidad : boolean  ): Boolean;
begin
     if Assigned( FOnSearch ) then
        FOnSearch( Self, Result, sFilter, sKey, sDescription )
     else
         Result := ZetaBusqueda_DevEx.ShowSearchForm( Self, sFilter, sKey, sDescription, TRUE, lSoloActivos, lUsarConfidencialidad );
end;
function TZetaLookupDataSet.Search_DevEx( const sFilter: String; var sKey, sDescription: String; lSoloActivos, lUsarConfidencialidad : boolean  ): Boolean;
begin
if Assigned( FOnSearch ) then
        FOnSearch( Self, Result, sFilter, sKey, sDescription )
     else
         Result := ZetaBusqueda_DevEx.ShowSearchForm( Self, sFilter, sKey, sDescription, TRUE, lSoloActivos, lUsarConfidencialidad );
end;
function TZetaLookupDataSet.LookupKey(const sKey, sFilter: String;
  var sDescription: String): Boolean;
var
  lActive, lConfidential : Boolean;
begin
Result := LookupKey( sKey, sFilter, sDescription, lActive, lConfidential);
end;

function TZetaLookupDataSet.GetIsActive: Boolean;
begin
    if ( FActivoField = '' ) or ( not GlobalSoloKeysActivos ) then
       Result := True
    else
        Result := ( FieldByName( FActivoField ).AsString = 'S'  ) ;
end;

function TZetaLookupDataSet.GetIsConfidential: Boolean;
begin
    if StrVacio( FConfidenField ) or StrVacio(GlobalListaConfidencialidad) then
       Result := False
    else
    begin
        Result :=  not ListaIntersectaConfidencialidad( FieldByName( FConfidenField ).AsString , GlobalListaConfidencialidad ) ;
    end;
end;


end.
