object Calendario: TCalendario
  Left = 200
  Top = 108
  ActiveControl = StringGrid
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Escoja la Fecha Deseada'
  ClientHeight = 191
  ClientWidth = 258
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid: TStringGrid
    Left = 0
    Top = 25
    Width = 258
    Height = 133
    Align = alClient
    BorderStyle = bsNone
    ColCount = 7
    DefaultColWidth = 36
    DefaultRowHeight = 18
    FixedCols = 0
    RowCount = 7
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected]
    ScrollBars = ssNone
    TabOrder = 0
    OnClick = StringGridClick
    OnDblClick = StringGridDblClick
    OnDrawCell = StringGridDrawCell
    OnSelectCell = StringGridSelectCell
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 158
    Width = 258
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object YearLBL: TLabel
      Left = 4
      Top = 10
      Width = 22
      Height = 13
      Alignment = taRightJustify
      Caption = 'A�o:'
    end
    object MonthLBL: TLabel
      Left = 82
      Top = 9
      Width = 23
      Height = 13
      Alignment = taRightJustify
      Caption = 'Mes:'
    end
    object OK: TBitBtn
      Left = 193
      Top = 4
      Width = 30
      Height = 25
      Hint = 'Aceptar Fecha Seleccionada'
      Default = True
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object Cancelar: TBitBtn
      Left = 225
      Top = 4
      Width = 30
      Height = 25
      Hint = 'Salir Sin Aceptar Fecha'
      Cancel = True
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object MonthValue: TZetaKeyCombo
      Left = 107
      Top = 6
      Width = 84
      Height = 21
      Style = csDropDownList
      DropDownCount = 12
      ItemHeight = 13
      TabOrder = 0
      OnChange = MonthValueChange
      ListaFija = lfMeses
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
    end
    object YearValue: TSpinEdit
      Left = 28
      Top = 5
      Width = 52
      Height = 22
      MaxLength = 4
      MaxValue = 2050
      MinValue = 1899
      TabOrder = 3
      Value = 1899
      OnChange = YearValueChange
    end
  end
  object PanelFecha: TPanel
    Left = 0
    Top = 0
    Width = 258
    Height = 25
    Align = alTop
    BevelOuter = bvNone
    Caption = 'PanelFecha'
    TabOrder = 2
  end
end
