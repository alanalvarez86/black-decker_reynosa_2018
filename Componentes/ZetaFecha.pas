unit ZetaFecha;

interface

{$R *.RES}

uses WinTypes, Classes, StdCtrls, Controls, Messages,
     SysUtils, Forms, Graphics, Menus, Buttons, Dialogs,
     Mask, MaskUtils,
     DB, DBCtrls, Comctrls, Clipbrd;

type
  TZetaFecha = class( TCustomMaskEdit )
  private
    { Private declarations }
    FOpcional: Boolean;
    FEpoca: Word;
    FCentury: Word;
    FYear: Word;
    FValor: TDate;
    FDlgBtn: TSpeedButton;
    FUpDownBtn: TSpeedButton;
    FUseEnterKey: Boolean;
    FOnValidDate: TNotifyEvent;
    function GetMinHeight: Integer;
    function GetTexto: String;
    function GetValor: TDate;
    function GetValorAsText: String;
    function CheckDate( const sDate: String; var dValue: TDate ): Boolean;
    procedure SetTexto( const Value: String );
    procedure SetValor( const Value: TDate );
    procedure SetEpoca( const Value: Word );
    procedure SetEditRect;
    procedure DisplayDate;
    procedure DisplayText( const Value: String );
    procedure CNKeyDown(var Message: TWMKeyDown); message CN_KEYDOWN; // De TWinControl.CNKeyDown //
    procedure CMExit( var Message: TCMExit ); message CM_EXIT;
    procedure WMSize( var Message: TWMSize ); message WM_SIZE;
    procedure WMCopy( var Message: TMessage ); message WM_COPY;
    procedure WMCut( var Message: TMessage ); message WM_CUT;
    procedure WMPaste( var Message: TMessage ); message WM_PASTE;
    procedure DoSearch;
    procedure DoValidDate;
    procedure MoverValor( const dValor: TDate );
    procedure SubirValor;
    procedure BajarValor;
  protected
    { Protected declarations }
    function FechaVacia: Boolean;
    function GetDateValue: Boolean;
    function ValidEdit: Boolean; virtual;
    procedure BtnClick( Sender: TObject );
    procedure BtnMouseDown( Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
    procedure ChangeValue( const Value: TDate ); virtual;
    procedure CreateParams( var Params: TCreateParams ); override;
    procedure CreateWnd; override;
    procedure WndProc( var Message: TMessage ); override;
    procedure DoEnter; override;
    procedure DoEdit;
    procedure DoUndo;
    procedure Enfocar;
    procedure HayError;
    procedure KeyDown( var Key: Word; Shift: TShiftState); override;
    procedure KeyPress( var Key: Char ); override;
    procedure SetMask( const lEdicion: Boolean );
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property DialogButton: TSpeedButton read FDlgBtn;
    property Texto: String read GetTexto write SetTexto;
    property ValorAsText: String read GetValorAsText;
    property UpDownButton: TSpeedButton read FUpDownBtn;
    procedure Loaded; override;
    procedure CopyToClipboard;
    procedure CutToClipboard;
    procedure PasteFromClipboard;
  published
    { Published declarations }
    property AutoSelect;
    property AutoSize;
    property BorderStyle;
    property CharCase;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Epoca: Word read FEpoca write SetEpoca default 1930;
    property Font;
    property ImeMode;
    property ImeName;
    property MaxLength;
    property Opcional: Boolean read FOpcional write FOpcional default False;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Text;
    property UseEnterKey: Boolean read FUseEnterKey write FUseEnterKey default False;
    property Valor: TDate read GetValor write SetValor;
    property Visible;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
    property OnValidDate: TNotifyEvent read FOnValidDate write FOnValidDate;
  end;
  TZetaDBFecha = class( TZetaFecha )
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    FConfirmEdit: Boolean;
    FConfirmMsg: String;
    function GetConfirmMsg: String;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    procedure SetDataField( const Value: string );
    procedure SetDataSource( Value: TDataSource );
    procedure SetReadOnly( const Value: Boolean );
    procedure CheckFieldType( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure EditingChange( Sender: TObject );
    procedure UpdateData( Sender: TObject );
    procedure ActiveChange( Sender: TObject );
  protected
    { Protected declarations }
    function ValidEdit: Boolean; override;
    procedure Change; override;
    procedure ChangeValue( const Value: TDate ); override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property ConfirmEdit: Boolean read FConfirmEdit write FConfirmEdit default False;
    property ConfirmMsg: String read FConfirmMsg write FConfirmMsg;
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
  end;

implementation

uses Winprocs,
     ZetaMsgDlg,
     ZetaCommonTools,
     ZetaCommonClasses,ZetaCommonLists,
     FCalendario_DevEx;

{ ********* TZetaFecha **************** }

constructor TZetaFecha.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     Cursor := crArrow;
     Epoca := 1930;
     FValor := Int( Now );
     FOpcional := False;
     FUseEnterKey := False;
     FDlgBtn := TSpeedButton.Create ( Self );
     with FDlgBtn do
     begin
          Parent := Self;
          Width := 20;
          Height := 17;
          Visible := True;
          ShowHint := True;
          Hint := 'Buscar Fecha ( Ctrl-F )';
          Layout := blGlyphLeft;
          Margin := 0;
          Glyph.LoadFromResourceName( HInstance, 'BTN_FECHA' );
          NumGlyphs := 1;
          GroupIndex := 0;
          OnClick := BtnClick;
     end;
     FUpDownBtn := TSpeedButton.Create ( Self );
     with FUpDownBtn do
     begin
          Parent := Self;
          Width := 16;
          Height := 17;
          Visible := True;
          Layout := blGlyphLeft;
          ShowHint := True;
          Hint := 'Aumentar/Disminuir Fecha';
          Margin := 0;
          Glyph.LoadFromResourceName( HInstance, 'BTN_UPDOWN' );
          NumGlyphs := 1;
          GroupIndex := 0;
          OnMouseUp := BtnMouseDown;
     end;
     Width := 115;
     ControlStyle := ControlStyle - [csSetCaption];
end;

destructor TZetaFecha.Destroy;
begin
     FUpDownBtn := nil;
     FDlgBtn := nil;
     inherited Destroy;
end;

procedure TZetaFecha.Loaded;
begin
     inherited Loaded;
     DisplayDate;
end;

procedure TZetaFecha.CreateParams( var Params: TCreateParams );
begin
     inherited CreateParams( Params );
     Params.Style := Params.Style or ES_MULTILINE or WS_CLIPCHILDREN;
end;

procedure TZetaFecha.CreateWnd;
begin
     inherited CreateWnd;
     SetEditRect;
end;

procedure TZetaFecha.WndProc( var Message: TMessage );
begin
     with Message do  { Manejo especial de VK_ESCAPE dado que la propiedad }
     begin            { ES_MULTILINE hace que el control no procese el ESC }
          if ( Msg = WM_KEYDOWN ) and ( wParam = VK_ESCAPE ) and ( Result = 0 ) then
          begin
               GetParentForm( Self ).Perform( CM_DIALOGKEY, wParam, lParam );
               Result := 1;
          end
          else
              inherited WndProc( Message );
     end;
end;

function TZetaFecha.GetTexto: String;
begin
     Result := EditText;
end;

function TZetaFecha.GetValor: TDate;
begin
     Result := Int( FValor );
end;

function TZetaFecha.GetValorAsText: String;
begin
     if FechaVacia then
        Result := '  /  /    '
     else
         Result := FormatDateTime( 'dd/mm/yy"  "', Valor );
end;

procedure TZetaFecha.SetTexto( const Value: String );
begin
     if ( EditText <> Value ) then
        EditText := Value;
end;

procedure TZetaFecha.ChangeValue( const Value: TDate );
begin
     FValor := Int( Value );
end;

procedure TZetaFecha.SetValor( const Value: TDate );
begin
     if ( Value <> FValor ) then
     begin
          ChangeValue( Value );
          Modified := True;
          DoValidDate;
     end;
     DisplayDate;
end;

procedure TZetaFecha.MoverValor( const dValor: TDate );
begin
     if not ReadOnly and ValidEdit then
     begin
          DoEdit;
          Valor := dValor;
     end;
end;

procedure TZetaFecha.SubirValor;
begin
     MoverValor( Valor + 1 );
end;

procedure TZetaFecha.BajarValor;
begin
     MoverValor( Valor - 1 );
end;

procedure TZetaFecha.SetEpoca( const Value: Word );
begin
     if ( Value <> FEpoca ) and ( Value >= 1900 ) and ( Value <= 2100 ) then
     begin
          FEpoca := Value;
          FCentury := 100 * Trunc( Value / 100 );
          FYear := Value - FCentury;
     end;
end;

procedure TZetaFecha.SetEditRect;
var
   Area: TRect;
begin
     SendMessage( Handle, EM_GETRECT, 0, LongInt( @Area ) );
     with Area do
     begin
          Top := 0;
          Left := 0;
          Bottom := ClientHeight;
          Right := ClientWidth - FDlgBtn.Width - FUpDownBtn.Width - 2;
     end;
     SendMessage( Handle, EM_SETRECTNP, 0, LongInt( @Area ) );
     SendMessage( Handle, EM_GETRECT, 0, LongInt( @Area ) );
end;

procedure TZetaFecha.CMExit( var Message: TCMExit );
begin
     if GetDateValue then
     begin
          Modified := False;
          SetMask( False );
          inherited;
     end
     else
         HayError;
end;

procedure TZetaFecha.WMSize( var Message: TWMSize );
var
   iCol, MinHeight: Integer;
begin
     inherited;
     MinHeight := GetMinHeight;
     if ( Height < MinHeight ) then
        Height := MinHeight
     else
     begin
          if ( FDlgBtn <> nil ) or ( FUpDownBtn <> nil ) then
          begin
               iCol := Width;
               if ( FDlgBtn <> nil ) then
               begin
                    iCol := iCol - FDlgBtn.Width;
                    FDlgBtn.SetBounds( iCol, 0, FDlgBtn.Width, ( Height - 3 ) );
               end;
               if ( FUpDownBtn <> nil ) then
               begin
                    iCol := iCol - FUpDownBtn.Width;
                    FUpDownBtn.SetBounds( iCol, 0, FUpDownBtn.Width, ( Height - 3 ) );
               end;
               SetEditRect;
          end;
     end;
end;

function TZetaFecha.GetMinHeight: Integer;
var
   DC: HDC;
   SaveFont: HFont;
   i: Integer;
   SysMetrics, Metrics: TTextMetric;
begin
     DC := GetDC( 0 );
     try
        GetTextMetrics( DC, SysMetrics );
        SaveFont := SelectObject( DC, Font.Handle );
        GetTextMetrics( DC, Metrics );
        SelectObject( DC, SaveFont );
     finally
            ReleaseDC( 0, DC );
     end;
     i := SysMetrics.tmHeight;
     if ( i > Metrics.tmHeight ) then
        i := Metrics.tmHeight;
     Result := Metrics.tmHeight + ( i div 4 ) + ( 4 * GetSystemMetrics( SM_CYBORDER ) ) + 2;
end;

procedure TZetaFecha.WMCopy( var Message: TMessage );
begin
     CopyToClipboard;
end;

procedure TZetaFecha.WMPaste( var Message: TMessage );
begin
     PasteFromClipboard;
end;

procedure TZetaFecha.WMCut( var Message: TMessage );
begin
     CutToClipboard;
end;

procedure TZetaFecha.BtnClick( Sender: TObject );
begin
     DoSearch;
end;

procedure TZetaFecha.BtnMouseDown( Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
     if ( Button = mbLeft ) then
     begin
          if ( Y > ( FUpDownBtn.Height / 2 ) ) then
             BajarValor
          else
              SubirValor;
     end;
end;

function TZetaFecha.FechaVacia: Boolean;
begin
     Result := ( Valor = NullDateTime );
end;

function TZetaFecha.CheckDate( const sDate: String; var dValue: TDate ): Boolean;
var
   sDia, sMes, sYear: String;
   dNueva: TDate;

function GetSiguiente: String;
var
   iPos: Word;
begin
     iPos := Pos( '/', sYear );
     Result := Trim( Copy( sYear, 0, ( iPos - 1 ) ) );
     sYear := Trim( Copy( sYear, ( iPos + 1 ), MaxInt ) );
end;

begin
     sYear := sDate;
     sDia := GetSiguiente;
     sMes := GetSiguiente;
     if Opcional and ( sDia = '' ) and ( sMes = '' ) and ( sYear = '' ) then
     begin
          dValue := NullDateTime;
          Result := True;
     end
     else
     begin
          dNueva := CodificaFecha( StrAsInteger( sYear ),
                                   StrAsInteger( sMes ),
                                   StrAsInteger( sDia ) );
          if ( dNueva = NullDateTime ) then
             Result := False
          else
          begin
               dValue := dNueva;
               Result := True;
          end;
     end;
end;

function TZetaFecha.GetDateValue: Boolean;
var
   dValue: TDate;
begin
     if Modified then
     begin
          if CheckDate( Texto, dValue ) then
          begin
               Valor := dValue;
               Result := True;
          end
          else
              Result := False;
     end
     else
         Result := True;
end;

procedure TZetaFecha.DisplayDate;
begin
     if IsMasked then
        DisplayText( ValorAsText )
     else
         if FechaVacia then
            DisplayText( '' )
         else
             DisplayText( FechaCorta( Valor ) );
     { GA: Sirve para seleccionar todo el texto al adquirir un nuevo VALOR }
     {
     if Focused then
        SelectAll;
     }
end;

procedure TZetaFecha.SetMask( const lEdicion: Boolean );
begin
     if ( lEdicion <> IsMasked ) then
     begin
          if lEdicion then
             EditMask := '!09/09/0999;1; '
          else
              EditMask := '';
          DisplayDate;
     end;
end;

procedure TZetaFecha.DisplayText( const Value: String );
begin
     Texto := Value;
end;

procedure TZetaFecha.DoValidDate;
begin
     if Assigned( FOnValidDate ) then
     begin
          FOnValidDate( Self );
     end;
end;

procedure TZetaFecha.DoEnter;
begin
     SetMask( True );
     inherited DoEnter;
end;

function TZetaFecha.ValidEdit: Boolean;
begin
     Result := True;
end;

procedure TZetaFecha.DoEdit;
begin
     if not Focused then
        Enfocar;
end;

procedure TZetaFecha.DoSearch;
var
   dValor: TDate;
begin
     if not ReadOnly then
     begin
          dValor := Valor;
          if GetDateValue then
             dValor := Valor;
          if FCalendario_DevEx.BuscaFecha( dValor ) then
             MoverValor( dValor );
     end;
end;

procedure TZetaFecha.DoUndo;
begin
     DisplayDate;
     Enfocar;
end;

procedure TZetaFecha.CNKeyDown(var Message: TWMKeyDown);
begin
     with Message do
     begin
          if ( Charcode = VK3_F ) and ( ssCtrl in KeyDataToShiftState( KeyData ) ) then
          begin
               Result := 1;
               DoSearch;
          end
          else
              inherited;
     end;
end;

procedure TZetaFecha.KeyDown( var Key: Word; Shift: TShiftState);
begin
     case Key of
          VK_DELETE:
          begin
               if ( Shift = [] ) then
               begin
                    if ValidEdit then
                       DoEdit
                    else
                        Key := 0;
               end;
          end;
          VK_UP: SubirValor;
          VK_DOWN: BajarValor;
          VK_RETURN:
          begin
               if UseEnterKey then
               begin
                    Key := 0;
                    if not GetDateValue then
                       HayError;
               end;
          end;
     end;
     inherited KeyDown( Key, Shift );
end;

procedure TZetaFecha.KeyPress( var Key: Char );

procedure PerformEdit;
begin
     if ValidEdit then
        DoEdit
     else
         Key := Chr( 0 );
end;

begin
     if ( Key <> Chr( VK_RETURN )) then { ENTER as TAB }
     begin
          case Key of
               #48..#57: PerformEdit;
               Chr( VK_BACK ): PerformEdit;
          else
              begin
                   MessageBeep( MB_ICONEXCLAMATION );
                   Key := Chr( 0 );
              end;
          end;
     end;
     inherited KeyPress( Key );
end;

procedure TZetaFecha.HayError;
begin
     MessageBeep( MB_ICONEXCLAMATION );
     Enfocar;
end;

procedure TZetaFecha.Enfocar;
begin
     if CanFocus then
     begin
          SelectAll;
          SetFocus;
     end;
end;

procedure TZetaFecha.CopyToClipboard;
begin
     if Focused then
        Clipboard.AsText := Text
     else
         Clipboard.AsText := ValorAsText;
end;

procedure TZetaFecha.CutToClipboard;
begin
     if not ReadOnly and ValidEdit then
     begin
          CopyToClipboard;
          Valor := NullDateTime;
     end;
end;

procedure TZetaFecha.PasteFromClipboard;
var
   sValue: String;
   dValue: TDate;
begin
     with Clipboard do
     begin
          if not ReadOnly and HasFormat( CF_TEXT ) then
          begin
               sValue := AsText;
               if CheckDate( sValue, dValue ) then
                  MoverValor( dValue );
          end;
     end;
end;

{ ************ TZetaDBFecha ************** }

constructor TZetaDBFecha.Create( AOwner : TComponent );
begin
     inherited Create( AOwner );
     FConfirmEdit := False;
     FConfirmMsg := '';
     FDataLink := TFieldDataLink.Create;
     with FDataLink do
     begin
          Control := Self;
          OnDataChange := DataChange;
          OnEditingChange := EditingChange;
          OnUpdateData := UpdateData;
          OnActiveChange := ActiveChange;
     end;
     if not ( csDesigning in ComponentState ) then
        CheckFieldType( DataField );
end;

destructor TZetaDBFecha.Destroy;
begin
     with FDataLink do
     begin
          OnActiveChange := nil;
          OnUpdateData := nil;
          OnEditingChange := nil;
          OnDataChange := nil;
          Control := nil;
          Free;
     end;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBFecha.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) then
        if ( FDataLink <> nil ) and ( AComponent = DataSource ) then
           DataSource := nil;
end;

function TZetaDBFecha.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBFecha.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBFecha.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBFecha.SetDataField( const Value: string );
begin
     CheckFieldType( Value );
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBFecha.SetDataSource( Value: TDataSource );
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaDBFecha.GetReadOnly: Boolean;
begin
     Result := FDataLink.ReadOnly;
end;

procedure TZetaDBFecha.SetReadOnly( const Value: Boolean );
begin
     FDataLink.ReadOnly := Value;
     inherited ReadOnly := Value;
end;

function TZetaDBFecha.GetConfirmMsg: String;
begin
     if ( FConfirmMsg = VACIO ) then
        Result := '� Desea Cambiar Esta Fecha ?'
     else
         Result := FConfirmMsg;
end;

procedure TZetaDBFecha.CheckFieldType( const Value: String );
begin
     if ( Value <> '' ) and
        ( FDataLink <> nil ) and
        ( FDataLink.Dataset <> nil ) and
        ( FDataLink.Dataset.Active ) then
     begin
          with FDataLink.Dataset.FieldByName( Value ) do
               if not ( DataType in [ ftDate, ftDateTime ] ) then
                  raise EInvalidFieldType.Create( 'TZetaDBFecha.DataField can only ' +
                                                  'be connected to columns of type ' +
                                                  'Date, DateTime' );
     end;
end;

procedure TZetaDBFecha.DataChange( Sender: TObject );
begin
     with FDataLink do
     begin
          if ( Field = nil ) then
             Valor := NullDateTime
          else
              if TDateTimeField( Field ).IsNull then
                 Valor := NullDateTime
              else
                  Valor := TDateTimeField( Field ).AsDateTime;
     end;
end;

procedure TZetaDBFecha.EditingChange(Sender: TObject);
begin
     {
     if Focused and FDataLink.Editing then
     }
     if FDataLink.Editing then
     begin
          SetMask( True );
          {
          Enfocar;
          }
     end;
end;

procedure TZetaDBFecha.UpdateData(Sender: TObject);
begin
     if ( FDataLink.Field <> nil ) then
     begin
          with TDateTimeField( FDataLink.Field ) do
          begin
               if ( AsDateTime <> Valor ) then
                  AsDateTime := Valor;
          end;
     end;
end;

procedure TZetaDBFecha.ActiveChange( Sender: TObject );
begin
     if ( FDataLink <> nil ) and FDataLink.Active then
        CheckFieldType( DataField );
end;

procedure TZetaDBFecha.Change;
begin
     if ( FDataLink <> nil ) then
        FDataLink.Modified;
     inherited Change;
end;

procedure TZetaDBFecha.ChangeValue( const Value: TDate );
begin
{$ifndef ANTES}
     Change;
{$endif}
     inherited ChangeValue( Value );
     if ( FDataLink <> nil ) and FDataLink.Editing then
     begin
          try
             with FDataLink do
             begin
                  UpdateRecord;
             end;
          except
                on Error: Exception do
                   Application.HandleException( Error );
          end;
     end;
end;

function TZetaDBFecha.ValidEdit;
begin
     if ( FDataLink <> nil ) then
     begin
          if FDataLink.Editing then
             Result := True
          else
          begin
               if not FConfirmEdit or ConfirmaCambio( GetConfirmMsg) then
                  Result := FDataLink.Edit
               else
                   Result := False;
          end;
     end
     else
         Result := True;
     if Result then
        Result := inherited ValidEdit;
end;

end.

