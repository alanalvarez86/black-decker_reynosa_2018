unit ZetaWizard;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, Buttons, ExtCtrls,
     ZetaCommonTools,
     ZetaMessages;

type
  TZetaWizard = class;
  WizardState = ( wzDataEntry, wzExecuting, wzExecuted, wzCanceled );
  BotonWizard = ( bwAnterior, bwSiguiente, bwEjecutar, bwCancelar );
  TWizardMove = procedure( Sender: TObject; var iNewPage: Integer; var CanMove: Boolean ) of object;
  TZetaWizardButton = class( TBitBtn )
  private
    { Private declarations }
    FTipo: BotonWizard;
    FWizard: TZetaWizard;
    FParentForm: TCustomForm;
    procedure SetWizard( Value: TZetaWizard );
    procedure SetTipo( const Value: BotonWizard );
    procedure SetState;
    procedure SetStateSalir;
    procedure WMWizardMove( var Message: TMessage ); message WM_WIZARD_MOVE;
    procedure WMWizardFocus( var Message: TMessage ); message WM_WIZARD_FOCUS;
    procedure WMPaint( var Message: TWMPaint ); message WM_PAINT;
    procedure Enfocar;
  protected
    { Protected declarations }
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    procedure Click; override;
  published
    { Published declarations }
    property Tipo: BotonWizard read FTipo write SetTipo;
    property Wizard: TZetaWizard read FWizard write SetWizard;
  end;
  TZetaWizard = class( TPanel )
  private
    { Private declarations }
    FAdelante: Boolean;
    FPageControl: TPageControl;
    FAlEjecutar: TConfirmEvent;
    FAlCancelar: TConfirmEvent;
    FAfterMove: TNotifyEvent;
    FBeforeMove: TWizardMove;
    FEstado: WizardState;
    FReejecutar: Boolean;
    function GetFirstPage: Integer;
    function GetLastPage: Integer;
    function GetPrimerPaso: Boolean;
    function GetUltimoPaso: Boolean;
    function GetCapturando: Boolean;
    function GetEjecutando: Boolean;
    function GetEjecutado: Boolean;
    function GetCancelado: Boolean;
    procedure SetEstado( Value: WizardState );
    procedure NotifyButtons;
  protected
    { Protected declarations }
    function HayPageControl: Boolean;
    procedure SetPageControl( Value: TPageControl );
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    property Adelante: Boolean read FAdelante;
    property Cancelado: Boolean read GetCancelado;
    property Capturando: Boolean read GetCapturando;
    property Ejecutando: Boolean read GetEjecutando;
    property Ejecutado: Boolean read GetEjecutado;
    property Estado: WizardState read FEstado;
    property Preparado: Boolean read HayPageControl;
    property PrimeraPagina: Integer read GetFirstPage;
    property PrimerPaso: Boolean read GetPrimerPaso;
    property UltimaPagina: Integer read GetLastPage;
    property UltimoPaso: Boolean read GetUltimoPaso;
    function EsPaginaActual( Value: TTabSheet ): Boolean;
    procedure Reset;
    procedure Primero;
    procedure Anterior;
    procedure Siguiente;
    procedure Ultimo;
    procedure Ejecutar;
    procedure Cancelar;
    procedure Abortar;
    procedure Saltar( iNewPageIndex: Integer );
  published
    { Published declarations }
    property AlEjecutar: TConfirmEvent read FAlEjecutar write FAlEjecutar;
    property AlCancelar: TConfirmEvent read FAlCancelar write FAlCancelar;
    property AfterMove: TNotifyEvent read FAfterMove write FAfterMove;
    property BeforeMove: TWizardMove read FBeforeMove write FBeforeMove;
    property PageControl: TPageControl read FPageControl write SetPageControl;
    property Reejecutar: Boolean read FReejecutar write FReejecutar;
  end;

implementation

{ *************** TZetaWizardBtn ***************** }

constructor TZetaWizardButton.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
end;

procedure TZetaWizardButton.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( AComponent = Wizard ) then
        Wizard := nil;
end;

procedure TZetaWizardButton.Loaded;
begin
     FParentForm := GetParentForm( Self );
end;

procedure TZetaWizardButton.SetWizard( Value: TZetaWizard );
begin
     if ( FWizard <> Value ) then
     begin
          FWizard := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

procedure TZetaWizardButton.SetTipo( const Value: BotonWizard );
begin
     if ( FTipo <> Value ) then
     begin
          FTipo := Value;
          case Tipo of
               bwAnterior: TabOrder := 3;
               bwSiguiente: TabOrder := 0;
               bwEjecutar: TabOrder := 1;
               bwCancelar: TabOrder := 2;
          end;
     end;
end;

procedure TZetaWizardButton.WMPaint( var Message: TWMPaint );
begin
     SetState;
     inherited;
end;

procedure TZetaWizardButton.WMWizardMove( var Message: TMessage );
begin
     Invalidate;
end;

procedure TZetaWizardButton.WMWizardFocus( var Message: TMessage );
begin
     case Tipo of
          bwSiguiente: Enfocar;
          bwEjecutar: Enfocar;
     end;
end;

procedure TZetaWizardButton.SetState;
var
   lEnabled: Boolean;
begin
     if Assigned( FWizard ) and FWizard.Preparado then
     begin
          case Tipo of
               bwAnterior:
               begin
                    with FWizard do
                    begin
                         lEnabled := Capturando and not PrimerPaso;
                         if not lEnabled and Reejecutar and Ejecutado then
                            lEnabled := True;
                    end;
                    Enabled := lEnabled;
               end;
               bwSiguiente: Enabled := FWizard.Capturando and not FWizard.UltimoPaso;
               bwEjecutar: Enabled := FWizard.Capturando and FWizard.UltimoPaso;
               bwCancelar: SetStateSalir;
          end;
     end;
end;

procedure TZetaWizardButton.Enfocar;
begin
     if Enabled and CanFocus then
        FParentForm.ActiveControl := Self;
end;

procedure TZetaWizardButton.SetStateSalir;
begin
     if FWizard.Ejecutado or FWizard.Cancelado then
     begin
          if ( Kind <> bkClose ) then
          begin
               Kind := bkClose;
               Caption := '&Salir';
          end
     end
     else
         if ( Kind <> bkCancel ) then
         begin
              Kind := bkCancel;
              Caption := '&Cancelar';
         end;
end;

procedure TZetaWizardButton.Click;
begin
     if Assigned( FWizard ) and FWizard.Preparado then
     begin
          case Tipo of
               bwAnterior:
               begin
                    with FWizard do
                    begin
                         if Reejecutar and Ejecutado then
                            Reset;
                         Anterior;
                    end;
               end;
               bwSiguiente: FWizard.Siguiente;
               bwEjecutar: FWizard.Ejecutar;
               bwCancelar: FWizard.Cancelar;
          end;
     end
     else
         inherited Click;
end;

{ *************** TZetaWizard ***************** }

constructor TZetaWizard.Create( AOwner: TComponent );
begin
     inherited Create( AOwner );
     ControlStyle := ControlStyle - [ csSetCaption ];
     Reset;
end;

procedure TZetaWizard.Reset;
begin
     FAdelante := False;
     SetEstado( wzDataEntry );
end;

procedure TZetaWizard.SetPageControl( Value: TPageControl );
begin
     if ( FPageControl <> Value ) then
     begin
          FPageControl := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaWizard.GetCapturando: Boolean;
begin
     Result := ( FEstado = wzDataEntry );
end;

function TZetaWizard.GetEjecutando: Boolean;
begin
     Result := ( FEstado = wzExecuting );
end;

function TZetaWizard.GetEjecutado: Boolean;
begin
     Result := ( FEstado = wzExecuted );
end;

function TZetaWizard.GetCancelado: Boolean;
begin
     Result := ( FEstado = wzCanceled );
end;

procedure TZetaWizard.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( AComponent = PageControl ) then
        PageControl := nil;
end;

function TZetaWizard.HayPageControl: Boolean;
begin
     Result := Assigned( FPageControl ) and ( FPageControl.PageCount > 0 );
end;

function TZetaWizard.EsPaginaActual( Value: TTabSheet ): Boolean;
begin
     Result := HayPageControl and ( FPageControl.ActivePage.PageIndex = Value.PageIndex );
end;

function TZetaWizard.GetFirstPage: Integer;
var
   i: Integer;
begin
     Result := -2;
     if HayPageControl then
     begin
          with FPageControl do
          begin
               for i := 0 to ( PageCount - 1 ) do
               begin
                    if Pages[ i ].Enabled then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TZetaWizard.GetPrimerPaso: Boolean;
begin
     if HayPageControl then
        Result := ( GetFirstPage = FPageControl.ActivePage.PageIndex )
     else
         Result := False;
end;

function TZetaWizard.GetLastPage: Integer;
var
   i: Integer;
begin
     Result := -2;
     if HayPageControl then
     begin
          with FPageControl do
          begin
               for i := ( PageCount - 1 ) downto 0 do
               begin
                    if Pages[ i ].Enabled then
                    begin
                         Result := i;
                         Break;
                    end;
               end;
          end;
     end;
end;

function TZetaWizard.GetUltimoPaso: Boolean;
begin
     if HayPageControl then
        Result := ( GetLastPage = FPageControl.ActivePage.PageIndex )
     else
         Result := False;
end;

procedure TZetaWizard.Primero;
begin
     if HayPageControl then
        Saltar( GetFirstPage );
end;

procedure TZetaWizard.Anterior;
begin
     if HayPageControl then
        Saltar( FPageControl.ActivePage.PageIndex - 1 );
end;

procedure TZetaWizard.Siguiente;
begin
     if HayPageControl then
        Saltar( FPageControl.ActivePage.PageIndex + 1 );
end;

procedure TZetaWizard.Ultimo;
begin
     if HayPageControl then
        Saltar( GetLastPage );
end;

procedure TZetaWizard.Ejecutar;
var
   lOk: Boolean;
   wzValue: WizardState;
begin
     wzValue := FEstado;
     SetEstado( wzExecuting );
     try
        lOk := True;
        if Assigned( FAlEjecutar ) then
           FAlEjecutar( Self, lOk );
     finally
            if lOk then
               SetEstado( wzExecuted )
            else
                SetEstado( wzValue );
     end;
end;

procedure TZetaWizard.Abortar;
begin
     SetEstado( wzCanceled );
end;

procedure TZetaWizard.Cancelar;
var
   lOk: Boolean;
begin
     try
        lOk := True;
        if Assigned( FAlCancelar ) then
           FAlCancelar( Self, lOk );
     finally
            if lOk then
               SetEstado( wzCanceled );
     end;
end;

procedure TZetaWizard.SetEstado( Value: WizardState );
begin
     FEstado := Value;
     NotifyButtons;
end;

procedure TZetaWizard.Saltar( iNewPageIndex: Integer );
var
   CanMove: Boolean;
begin
     CanMove := True;
     FAdelante := HayPageControl and ( FPageControl.ActivePage.PageIndex < iNewPageIndex );
     if Assigned( FBeforeMove ) then
        FBeforeMove( Self, iNewPageIndex, CanMove );
     if CanMove then
     begin
          if HayPageControl and
             ( iNewPageIndex >= 0 ) and
             ( iNewPageIndex < FPageControl.PageCount ) and
             ( iNewPageIndex <> FPageControl.ActivePage.PageIndex ) then
          begin
               with FPageControl do
               begin
                    ActivePage := Pages[ iNewPageIndex ];
                    while not ActivePage.Enabled do
                    begin
                         ActivePage := FindNextPage( ActivePage, FAdelante, False );
                    end;
                    iNewPageIndex := ActivePage.PageIndex;
               end;
               if ( iNewPageIndex = GetFirstPage ) then
                  Reset;
               NotifyButtons;
               if Assigned( FAfterMove ) then
                  FAfterMove( Self );
          end;
     end;
end;

procedure TZetaWizard.NotifyButtons;
begin
     if HayPageControl then
     begin
          NotifyControls( WM_WIZARD_MOVE );
          Application.ProcessMessages;
          NotifyControls( WM_WIZARD_FOCUS );
          Application.ProcessMessages;
     end;
end;

end.
