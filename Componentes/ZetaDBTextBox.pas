unit ZetaDBTextBox;

interface

uses SysUtils, WinTypes, WinProcs, Classes, Graphics,
     DB, DBCtrls, StdCtrls, Controls, Mask,
     MaskUtils,
     Messages;

type
  TZetaTextBox = class(TLabel)
  private
    { Private declarations }
    FFormatSpecifier: String;
    FBrush: TBrush;
    FBorder: Boolean;
    FGetSmallerClientRect: Boolean;
    procedure SetBorder(Value: Boolean);
  protected
    { Protected declarations }
    function GetLabelText: String; override;
    function GetClientRect: TRect; override;
    function FormatLabelText( const Value: String ): String;
    procedure Paint; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property Brush: TBrush read FBrush write FBrush;
    property Border: Boolean read FBorder write SetBorder;
    property FormatSpecifier: String read FFormatSpecifier write FFormatSpecifier;
  end;
  TZetaDBTextBox = class(TZetaTextBox)
  private
    { Private declarations }
    FDataLink: TFieldDataLink;
    FFormatFloat: String;
    FFormatCurrency: String;
    FOnGetText: TFieldGetTextEvent;
    function GetDataField: String;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetFieldText: String;
    procedure SetDataField( const Value: String );
    procedure SetDataSource( Value: TDataSource );
    procedure SetFormatFloat( const Value: String );
    procedure SetFormatCurrency( const Value: String );
    procedure DataChange( Sender: TObject );
    procedure CMGetDataLink( var Message: TMessage ); message CM_GETDATALINK;
  protected
    { Protected declarations }
    function GetLabelText: String; override;
    procedure Notification( AComponent: TComponent; Operation: TOperation ); override;
    procedure Loaded; override;
  public
    { Public declarations }
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property FormatFloat: String read FFormatFloat write SetFormatFloat;
    property FormatCurrency: String read FFormatCurrency write SetFormatCurrency;
    property OnGetText: TFieldGetTextEvent read FOnGetText write FOnGetText;
  end;

implementation

uses ZetaCommonTools;

{ ************* TZetaTextBox ************ }

constructor TZetaTextBox.Create(AOwner: TComponent);
begin
     FFormatSpecifier := '';
     FBrush := TBrush.Create;
     FBrush.Style := bsSolid;
     FBrush.Color := clBtnFace;
     FBorder := True;
     FGetSmallerClientRect := False;
     inherited Create( AOwner );
     ControlStyle := ControlStyle + [csReplicatable];
     ShowAccelChar := False;
     AutoSize := False;
end;

destructor TZetaTextBox.Destroy;
begin
     FBrush.Free;
     inherited Destroy;
end;

procedure TZetaTextBox.SetBorder( Value: Boolean );
begin
     FBorder := Value;
     Repaint;
end;

function TZetaTextBox.GetClientRect: TRect;
begin
     Result := inherited GetClientRect;
     if FGetSmallerClientRect then
        with Result do
        begin
             Left := Left + 3;
             Top := Top + 2;
             Right := Right - 3;
             Bottom := Bottom - 2;
        end;
end;

function TZetaTextBox.FormatLabelText( const Value: String ): String;
begin
     if ( FFormatSpecifier <> '' ) then
        Result := FormatMaskText( FFormatSpecifier, Value )
     else
         Result := Value;
end;

function TZetaTextBox.GetLabelText: String;
begin
     Result := FormatLabelText( Caption );
end;

procedure TZetaTextBox.Paint;
var
   Rect: TRect;
function HexToWord( HexStr: String ): LongWord;
begin
     Result := StrToInt('$'+HexStr);
end;
begin
     { Draw Frame Border }
     Rect := ClientRect;
     //DrawEdge( Canvas.Handle, Rect, EDGE_SUNKEN, BF_RECT ); //old
     //DevEx: Agregado para cambiar la apariencia de este componente a Flat
     //Canvas.Pen.Color := RGB(240,240,240);
     Canvas.Pen.Color := RGB(171,171,171);
     Canvas.Brush.Color := RGB(235,235,235);
     Canvas.Rectangle(Rect);
     //
     { Dimension smaller drawing area }
     try
        FGetSmallerClientRect := True;
        inherited Paint; { This uses ClientRect to draw Text }
     finally
            { Dimension original drawing area }
            FGetSmallerClientRect := False;
     end;
end;

{ ************* TZetaDBTextBox ************ }

constructor TZetaDBTextBox.Create(AOwner: TComponent);
begin
     FFormatFloat := '%14.2n';
     FFormatCurrency := '%m';
     inherited Create( AOwner );
     FDataLink := TFieldDataLink.Create;
     FDataLink.OnDataChange := DataChange;
end;

destructor TZetaDBTextBox.Destroy;
begin
     FDataLink.OnDataChange := nil;
     FDataLink.Free;
     FDataLink := nil;
     inherited Destroy;
end;

procedure TZetaDBTextBox.Notification( AComponent: TComponent; Operation: TOperation );
begin
     inherited Notification( AComponent, Operation );
     if ( Operation = opRemove ) and ( FDataLink <> nil ) and ( AComponent = DataSource ) then
        DataSource := nil;
end;

procedure TZetaDBTextBox.Loaded;
begin
     inherited Loaded;
     if ( csDesigning in ComponentState ) then DataChange( Self );
end;

function TZetaDBTextBox.GetDataField: String;
begin
     Result := FDataLink.FieldName;
end;

function TZetaDBTextBox.GetField: TField;
begin
     Result := FDataLink.Field;
end;

function TZetaDBTextBox.GetDataSource: TDataSource;
begin
     Result := FDataLink.DataSource;
end;

procedure TZetaDBTextBox.SetDataField( Const Value: string );
begin
     FDataLink.FieldName := Trim( Value );
end;

procedure TZetaDBTextBox.SetDataSource(Value: TDataSource);
begin
     if ( FDataLink.DataSource <> Value ) then
     begin
          FDataLink.DataSource := Value;
          if ( Value <> nil ) then
             Value.FreeNotification( Self );
     end;
end;

function TZetaDBTextBox.GetFieldText: String;
begin
  with FDataLink do begin
    if (Field = nil) then begin
      if (csDesigning in ComponentState) then
        Result := Self.Name
    end else begin
      with Field do begin
        if Assigned(OnGetText) then
          Result := DisplayText
        else
          case DataType of
            ftString:
              Result := AsString;
            ftSmallint, ftInteger, ftWord, ftAutoInc:
              Result := IntToStr(AsInteger);
            ftBoolean:
              Result := zBoolToStr(AsBoolean);
            ftFloat, ftBCD, ftFMTBcd:
              Result := Format(FFormatFloat, [AsFloat]);
            ftCurrency:
              Result := Format(FFormatCurrency, [AsCurrency]);
            ftDate, ftDateTime:
              Result := SetDateTimeText(AsDateTime, True);
            ftTime:
              Result := SetDateTimeText(AsDateTime, False);
            else
              Result := '�' + Self.Name + '? ft' + FieldTypeNames[DataType]
          end;
      end;
      if Assigned(FOnGetText) then
        FOnGetText(Field, Result, True);
    end;
  end;
end;

function TZetaDBTextBox.GetLabelText: String;
begin
     if ( csPaintCopy in ControlState ) then
        Result := FormatLabelText( GetFieldText )
     else
         Result := inherited GetLabelText;
end;

procedure TZetaDBTextBox.CMGetDataLink( var Message: TMessage );
begin
     Message.Result := Integer( FDataLink );
end;

procedure TZetaDBTextBox.SetFormatFloat( const Value: String );
begin
     if ( Value <> FFormatFloat ) then
     begin
          FFormatFloat := Value;
          if ( FDataLink <> nil ) then
             FDataLink.OnDataChange( Self );
     end;
end;

procedure TZetaDBTextBox.SetFormatCurrency( const Value: String );
begin
     if ( Value <> FFormatCurrency ) then
     begin
          FFormatCurrency := Value;
          if ( FDataLink <> nil ) then
             FDataLink.OnDataChange( Self );
     end;
end;

procedure TZetaDBTextBox.DataChange( Sender: TObject );
begin
     Caption := GetFieldText;
end;

end.
