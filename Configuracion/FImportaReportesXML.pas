unit FImportaReportesXML;

interface


uses SysUtils, Classes,
{$ifdef MSSQL}
     DReportesCliente,
{$else}
     DReportes,
{$endif}
     ZetaMigrar,
     DXMLTools,
     ZetaCommonClasses,
     ZetaCommonTools;
type
  TdmXMLTools_Rep = class( TdmXMLTools)
  private
      FActualNode : TZetaXMLNode;
      procedure SetActualNode( Node: TZetaXMLNode );
  public
      property ActualNode : TZetaXMLNode read FActualNode write SetActualNode;
end;

type
  TImportaReportesXML = class
  private
    { Private declarations }
    FArchivoXMLPath : string;
    FAbsolutePath : string;
    FBitacora    : TZetaMigrarLog;
    FCodigoEmpresa : string;
  private
    procedure BitacoraError( sError : string ) ;
    procedure BitacoraWarning(sReport, sWarning : string ) ;
    procedure BitacoraMensaje( sMensaje : string ) ;
    procedure SetDefaults( const sReportesPath , sCodigoEmpresa: String);


  protected
    { Protected declarations }
  public
    { Public declarations }
    function GetArchivoXML : String;
    function GetReportesFromPath ( const sDirectoryPath : String ) : TStringList;
    function ImportarReportesFromList (  const sDirectoryPath, sCodigoEmpresa : string;
             const listaReportes : TStringList ;
             const       lImportarPlantilla ,
              lSustituirPLantilla,
              lContinuarPlantilla : boolean
) : integer;



    property Bitacora: TZetaMigrarLog read FBitacora write FBitacora;
end;

implementation


procedure TdmXMLTools_Rep.SetActualNode( Node: TZetaXMLNode );
begin
end;



procedure TImportaReportesXML.BitacoraError( sError : string ) ;
begin
  Bitacora.AgregaError( sError );
end;

procedure TImportaReportesXML.BitacoraMensaje( sMensaje : string ) ;
begin
  Bitacora.AgregaError( sMensaje );
end;

    { Public declarations }
function TImportaReportesXML.GetArchivoXML : String;
begin
     Result := FArchivoXMLPath;
end;




procedure TImportaReportesXML.SetDefaults( const sReportesPath, sCodigoEmpresa: String );
begin
   FAbsolutePath := IncludeTrailingBackslash(  sReportesPath ) ;
   FCodigoEmpresa := sCodigoEmpresa;
end;


function TImportaReportesXML.GetReportesFromPath(
  const sDirectoryPath : String ): TStringList;

const K_FIND_REPORTES = '*.RP1';

var
  SR: TSearchRec;
begin

  SetDefaults(sDirectoryPath, VACIO);

  Result  := TStringList.Create;

  if FindFirst(FAbsolutePath + K_FIND_REPORTES, faAnyFile, SR) = 0 then
  begin
    repeat
      if (SR.Attr <> faDirectory) then
      begin
        Result.Add(SR.Name);
      end;
    until FindNext(SR) <> 0;
    FindClose(SR);
  end;
end;


function TImportaReportesXML.ImportarReportesFromList(
  const sDirectoryPath , sCodigoEmpresa: string;
  const listaReportes: TStringList;
  const       lImportarPlantilla ,
              lSustituirPLantilla,
              lContinuarPlantilla : boolean

  ): integer;

var
  iErrores, iExitos, iArchivo: integer;


  function ImportarReporte( sArchivoReporte : string )  : boolean;
  var
     sArchivoReporteCorto, sArchivoReportePath : string;
     sError, sErrores, sMensajes : string;
  begin
       if not StrVacio( sArchivoReporte ) then
       begin
            sArchivoReporteCorto := sArchivoReporte;
            sArchivoReportePath := FAbsolutePath + sArchivoReporte;
            BitacoraMensaje ( 'Reporte a Importar: '+ sArchivoReporte ) ;

            sError := VACIO;
            sErrores := VACIO;
            sMensajes := VACIO;
            {$ifdef MSSQL}
            try
               Result := dmReportesCliente.ImportaReporteBatch( sArchivoReportePath, VACIO, lImportarPlantilla, lSustituirPlantilla, lContinuarPlantilla, sError, sErrores, sMensajes );
            except
                 on Error: Exception do
                 begin
//                    BitacoraError( 'Error al Importar Reporte '+ sArchivoReporteCorto+' :' + Error.Message  );
                    BitacoraWarning( 'No Fue Posible Importar Reporte '+ sArchivoReporteCorto,  Error.Message  );
                    Result := False;
                 end;
            end;

             {$else}
                   BitacoraError( 'Error al Importar Reporte '+ sArchivoReporteCorto+' : FUNCIONALIDAD NO DISPONIBLE PARA PROFESIONAL'   );
                   Result := False;
            {$endif}


            if ( Result ) then
               BitacoraMensaje( sMensajes )
            else
            begin
                BitacoraMensaje( sMensajes );
                //BitacoraError( 'Error(es) al Importar Reporte '+ sArchivoReporteCorto +':' );
                BitacoraWarning( 'No Fue Posible Importar Reporte '+ sArchivoReporteCorto , sError + CR_LF + sErrores);
//                BitacoraError( sError );
//                BitacoraError( sErrores );
            end;

       end
       else
       begin
            BitacoraError ( 'No se especificó el Archivo en la Importación de Reportes ' );
            Result := False;
       end;
  end;

begin
  iErrores := 0;
  iExitos := 0;

  SetDefaults(sDirectoryPath, sCodigoEmpresa);

  for iArchivo := 0 to listaReportes.Count-1 do
  begin
       if ImportarReporte ( listaReportes[iArchivo] ) then
          Inc(iExitos)
       else
           Inc(iErrores);

  end;


   //Todos con error y Ninguno OK
   if ( iExitos = 0 ) and ( iErrores > 0 ) then
        Result := -10
   else
   //Algunos fallaron
   if ( iExitos > 0)  and ( iErrores > 0 ) then
      Result := 10
   else
      Result := 0;


end;

procedure TImportaReportesXML.BitacoraWarning(sReport, sWarning: string);
begin
    //Bitacora.AddWarning(sReport, sWarning);

    Bitacora.AgregaMensaje( sReport + ' En '+FCodigoEmpresa + ':' + sWarning);
    //Bitacora.( sReport + ' En '+FCodigoEmpresa , sWarning);
end;

end.
