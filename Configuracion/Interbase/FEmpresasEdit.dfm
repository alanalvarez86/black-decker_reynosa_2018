inherited EmpresaEdit: TEmpresaEdit
  Left = 470
  Top = 191
  Caption = 'EmpresaEdit'
  ClientHeight = 471
  ClientWidth = 435
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object CM_CODIGOlbl: TLabel [0]
    Left = 72
    Top = 10
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = '&C'#243'digo:'
    FocusControl = CM_CODIGO
  end
  object CM_NOMBRElbl: TLabel [1]
    Left = 68
    Top = 32
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = '&Nombre:'
    FocusControl = CM_NOMBRE
  end
  object CM_ACUMULAlbl: TLabel [2]
    Left = 54
    Top = 76
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Acu&mula a:'
    FocusControl = CM_ACUMULA
  end
  object CM_EMPATElbl: TLabel [3]
    Left = 17
    Top = 98
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = '&Sumar al Acumular:'
    FocusControl = CM_EMPATE
  end
  object CM_TIPO_Elbl: TLabel [4]
    Left = 83
    Top = 54
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = '&Tipo:'
    FocusControl = CM_TIPO_E
  end
  object DigitoGafetesLBL: TLabel [5]
    Left = 21
    Top = 153
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'D'#237'gito en &Gafetes:'
    FocusControl = CM_DIGITO
  end
  inherited PanelBotones: TPanel
    Top = 435
    Width = 435
    TabOrder = 9
    inherited OK: TBitBtn
      Left = 267
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 352
      ModalResult = 0
      OnClick = CancelarClick
      Kind = bkCustom
    end
    object DBNavigator: TDBNavigator
      Left = 5
      Top = 5
      Width = 108
      Height = 25
      DataSource = DataSource
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
      TabOrder = 2
    end
  end
  object DBGroupBox: TGroupBox
    Left = 0
    Top = 308
    Width = 435
    Height = 127
    Align = alBottom
    Caption = ' Base de Datos  '
    TabOrder = 8
    object CM_ALIASlbl: TLabel
      Left = 82
      Top = 19
      Width = 25
      Height = 13
      Alignment = taRightJustify
      Caption = '&Alias:'
    end
    object CM_USRNAMElbl: TLabel
      Left = 13
      Top = 61
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Nom&bre de Usuario:'
      FocusControl = CM_USRNAME
    end
    object CM_PASSWRDlbl: TLabel
      Left = 23
      Top = 84
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cla&ve de Acceso:'
      FocusControl = CM_PASSWRD
    end
    object PasswordCheckLBL: TLabel
      Left = 43
      Top = 107
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Con&firmaci'#243'n:'
      FocusControl = PasswordCheck
    end
    object CrearAlias: TSpeedButton
      Left = 379
      Top = 14
      Width = 23
      Height = 23
      Hint = 'Crear Alias de BDE'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = CrearAliasClick
    end
    object CM_DATOSlbl: TLabel
      Left = 76
      Top = 39
      Width = 31
      Height = 13
      Alignment = taRightJustify
      Caption = '&Datos:'
    end
    object CM_ALIAS: TZetaDBTextBox
      Left = 110
      Top = 16
      Width = 267
      Height = 18
      AutoSize = False
      Caption = 'CM_ALIAS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_ALIAS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_DATOS: TZetaDBTextBox
      Left = 110
      Top = 36
      Width = 267
      Height = 18
      AutoSize = False
      Caption = 'CM_DATOS'
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'CM_DATOS'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object CM_USRNAME: TDBEdit
      Left = 110
      Top = 57
      Width = 145
      Height = 21
      DataField = 'CM_USRNAME'
      DataSource = DataSource
      TabOrder = 0
    end
    object PasswordCheck: TEdit
      Left = 110
      Top = 103
      Width = 145
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
    end
    object CM_PASSWRD: TDBEdit
      Left = 110
      Top = 80
      Width = 145
      Height = 21
      DataField = 'CM_PASSWRD'
      DataSource = DataSource
      PasswordChar = '*'
      TabOrder = 1
      OnChange = CM_PASSWRDChange
    end
  end
  object CM_NOMBRE: TDBEdit
    Left = 111
    Top = 28
    Width = 288
    Height = 21
    DataField = 'CM_NOMBRE'
    DataSource = DataSource
    TabOrder = 1
  end
  object CM_EMPATE: TZetaDBNumero
    Left = 111
    Top = 94
    Width = 50
    Height = 21
    Mascara = mnDias
    TabOrder = 4
    Text = '0'
    DataField = 'CM_EMPATE'
    DataSource = DataSource
  end
  object CM_ACUMULA: TZetaDBKeyCombo
    Left = 111
    Top = 72
    Width = 288
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 3
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'CM_ACUMULA'
    DataSource = DataSource
    LlaveNumerica = False
  end
  object CM_USACAFE: TDBCheckBox
    Left = 37
    Top = 115
    Width = 87
    Height = 17
    Alignment = taLeftJustify
    Caption = '&Usa Cafeter'#237'a:'
    DataField = 'CM_USACAFE'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object CM_TIPO_E: TComboBox
    Left = 111
    Top = 50
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    OnChange = CM_TIPO_EChange
  end
  object CM_DIGITO: TDBEdit
    Left = 111
    Top = 149
    Width = 23
    Height = 21
    DataField = 'CM_DIGITO'
    DataSource = DataSource
    MaxLength = 1
    TabOrder = 7
    OnKeyPress = CM_DIGITOKeyPress
  end
  object CM_CODIGO: TZetaDBEdit
    Left = 111
    Top = 6
    Width = 121
    Height = 21
    TabOrder = 0
    Text = 'CM_CODIGO'
    ConfirmEdit = True
    DataField = 'CM_CODIGO'
    DataSource = DataSource
  end
  object gbConfidencialidad: TGroupBox
    Left = 16
    Top = 174
    Width = 412
    Height = 127
    Caption = ' Conf&idencialidad: '
    TabOrder = 10
    object btSeleccionarConfiden: TSpeedButton
      Left = 380
      Top = 48
      Width = 23
      Height = 23
      Hint = 'Seleccionar Confidencialidad'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C00001F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C007C007C007C007C007C007C00001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1042007C007C00001F7C007C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1042007C00001F7C1F7C1F7C1F7C007C007C00001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C007C007C00001F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1042007C
        00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        007C007C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ParentShowHint = False
      ShowHint = True
      OnClick = btSeleccionarConfidenClick
    end
    object listaConfidencialidad: TListBox
      Left = 94
      Top = 48
      Width = 285
      Height = 69
      TabStop = False
      ExtendedSelect = False
      ItemHeight = 13
      TabOrder = 0
    end
    object rbConfidenNinguna: TRadioButton
      Left = 94
      Top = 11
      Width = 131
      Height = 17
      Caption = 'Sin Confidencialidad '
      TabOrder = 1
      OnClick = rbConfidenNingunaClick
    end
    object rbConfidenAlgunas: TRadioButton
      Left = 94
      Top = 28
      Width = 156
      Height = 17
      Caption = 'Aplica algunas'
      TabOrder = 2
      OnClick = rbConfidenAlgunasClick
    end
  end
  object CM_USACASE: TDBCheckBox
    Left = 48
    Top = 131
    Width = 76
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Usa Ca&seta:'
    DataField = 'CM_USACASE'
    DataSource = DataSource
    TabOrder = 6
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object DataSource: TDataSource
    OnStateChange = DataSourceStateChange
    OnDataChange = DataSourceDataChange
    Left = 200
    Top = 97
  end
end
