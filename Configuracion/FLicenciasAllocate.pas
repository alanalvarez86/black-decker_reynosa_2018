unit FLicenciasAllocate;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls,
     ZBaseDlgModal, Mask, ZetaNumero, ZetaDBTextBox;

type
  TFAllocateLicences = class(TZetaDlgModal)
    CompanyLBL: TLabel;
    Company: TZetaTextBox;
    CompanyName: TZetaTextBox;
    CompanyNameLBL: TLabel;
    OldValueLBL: TLabel;
    OldValue: TZetaTextBox;
    LicensesLBL: TLabel;
    Licenses: TZetaNumero;
    EmpleadosLBL: TLabel;
    Empleados: TZetaTextBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FAnterior: Integer;
    FEmpresa: String;
    FRazonSocial: String;
    FEmployees: Integer;
    function GetLicencias: Integer;
  public
    { Public declarations }
    property Empresa: String read FEmpresa write FEmpresa;
    property RazonSocial: String read FRazonSocial write FRazonSocial;
    property Anterior: Integer read FAnterior write FAnterior;
    property Employees: Integer read FEmployees write FEmployees;
    property Licencias: Integer read GetLicencias;
  end;

var
  FAllocateLicences: TFAllocateLicences;

implementation

uses ZetaDialogo;

{$R *.DFM}

procedure TFAllocateLicences.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 0;
end;

procedure TFAllocateLicences.FormShow(Sender: TObject);
begin
     inherited;
     Company.Caption := Empresa;
     CompanyName.Caption := RazonSocial;
     Empleados.Caption := Trim( Format( '%8.0n', [ Employees / 1 ] ) );
     OldValue.Caption := Trim( Format( '%8.0n', [ Anterior / 1 ] ) );
     Licenses.Valor := 0;
     ActiveControl := Licenses;
end;

function TFAllocateLicences.GetLicencias: Integer;
begin
     Result := Licenses.ValorEntero;
end;

procedure TFAllocateLicences.OKClick(Sender: TObject);
begin
     inherited;
     if ( Licencias < Employees ) and ( Licencias > 0 ) then
     begin
          ZetaDialogo.zError( '� Licencias Insuficientes !', 'Las Licencias Asignadas No Son Suficientes', 0 );
          ActiveControl := Licenses;
     end
     else
     begin
          ModalResult := mrOk;
     end;
end;

end.
