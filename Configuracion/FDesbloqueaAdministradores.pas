unit FDesbloqueaAdministradores;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables, ExtCtrls,
     FileCtrl, ComCtrls, Mask, DBCtrls, Grids, DBGrids, Db, Checklst,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo;

type
  TDesbloqueaAdmon = class(TForm)
    Wizard: TZetaWizard;
    Desbloquear: TZetaWizardButton;
    Cancelar: TZetaWizardButton;
    PageControl: TPageControl;
    Comparacion: TTabSheet;
    Mensaje: TMemo;
    procedure CancelarClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
   DesbloqueaAdmon: TDesbloqueaAdmon;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaMessages,
     ZetaDialogo,
     FHelpContext,
     DDBConfig,
     DCompara;

{$R *.DFM}

procedure TDesbloqueaAdmon.FormCreate(Sender: TObject);
begin
     HelpContext := H50016_Desbloquear_administradores;
end;

procedure TDesbloqueaAdmon.FormShow(Sender: TObject);
begin
     with Mensaje.Lines do
     begin
          BeginUpdate;
          try
             Clear;
             Add( '' );
             Add( '' );
             Add( '' );
             Add( '' );
             Add( '' );             
             Add( 'Este Proceso Desbloquear� TODOS los ' );
             Add( 'Usuarios del Grupo ADMINISTRADORES' );
             Add( 'Del Sistema Tress' );
             Add( '' );
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TDesbloqueaAdmon.CancelarClick(Sender: TObject);
begin
     Close;
end;

procedure TDesbloqueaAdmon.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if dmdbConfig.DesbloqueaAdministradores then
           ZetaDialogo.ZInformation( 'TressCFG', '� Proceso Termin� con Exito !', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TDesbloqueaAdmon.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     Close;
end;

end.
