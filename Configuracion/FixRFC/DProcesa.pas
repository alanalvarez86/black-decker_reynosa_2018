unit DProcesa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms,
  DZetaServerProvider,
  Dialogs, Db, DBClient, ZetaClientDataSet;

type
    eQuerys = ( eInfoEmpleados, eUpdateRFC );
    eCorreccion = ( ecDiganostico, ecTodo, ecSobreEscribir );
type
  TdmProcesa = class(TDataModule)
    cdsRFCEmpleados: TZetaClientDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    oZetaProvider: TdmZetaServerProvider;
    function GetScript(const eTipo : eQuerys): String;
  public
    { Public declarations }
    procedure GetDatosEmpleados;
    function Procesa( const iTipo: integer; var oBitacora, oErrores: TStrings ): boolean;
  end;

var
  dmProcesa: TdmProcesa;

implementation

{$R *.DFM}

uses
    ZetaCommonClasses,
    ZetaCommonTools,
    DCliente;

function TdmProcesa.GetScript(const eTipo: eQuerys): String;
begin
     Result := '';
     case etipo of
          eInfoEmpleados : Result := 'select CB_CODIGO, CB_APE_PAT, CB_APE_MAT, CB_NOMBRES, CB_FEC_NAC, CB_RFC, '+ K_PRETTYNAME + ' as PRETTYNAME ' +
                                     'from COLABORA '+
                                     'order by CB_CODIGO';
          eUpdateRFC : Result := 'update COLABORA set CB_RFC = :CB_RFC where CB_CODIGO = :CB_CODIGO';
     end;
end;

procedure TdmProcesa.DataModuleCreate(Sender: TObject);
begin
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmProcesa.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
end;

procedure TdmProcesa.GetDatosEmpleados;
begin
     oZetaProvider.EmpresaActiva := dmCliente.Empresa;
     with oZetaProvider do
     begin
          cdsRFCEmpleados.Data := OpenSQL( dmCliente.Empresa, GetScript( eInfoEmpleados ), TRUE );
     end;
end;

function TdmProcesa.Procesa(const iTipo: integer; var oBitacora, oErrores: TStrings): boolean;
const
     K_ENCABEZADO = CR_LF +
                    'Empresa: %s' + CR_LF +
                    'Usuario: %s' + CR_LF +
                    'Opción:  %s' + CR_LF +
                    'Fecha:   %s' + CR_LF +
                    'Inicio:  %s' + CR_LF +
                    '-------------------------------------------------------' + CR_LF;
     K_FOOTER =     '-------------------------------------------------------' + CR_LF+
                    'Fin:                            %s'+ CR_LF +
                    '%s           %d'+ CR_LF +
                    'Total de Empleados Procesados:  %d';

    aEncabezado: array[ FALSE..TRUE ] of PChar = ( 'BITACORA DE ERRORES', 'BITACORA DE MENSAJES' );
    aMensaje: array[ FALSE..TRUE ] of PChar = ( 'Empleados Arreglados:', 'Empleados Con Error: ' );
    aOpcion: array[ eCorreccion ] of PChar = ( 'Diagnosticar Información', 'ArreglarToda La Información', 'Sobreescribir Toda La Información' );

var
   sPaterno, sMaterno, sNombres, sRFC, sGenerado, sEmpleado: string;
   iCodigo, iRegBitacora, iRegErrores, iTotal: integer;
   dFecNac: TDate;
   eTipo: eCorreccion;
   FCursor: TZetaCursor;

   procedure InitBitacora( var Bitacora: TStrings; lTipo: boolean );
   begin
        Bitacora.Clear;
        with dmCliente do
             Bitacora.Add( aEncabezado [ lTipo ] + Format( K_ENCABEZADO, [ GetDatosEmpresaActiva.Nombre,
                                                                           GetDatosUsuarioActivo.Nombre,
                                                                           aOpcion[ etipo ],
                                                                           FechaCorta( Date ),
                                                                           FormatDateTime( 'hh:nn:ss am/pm', Time ) ] ) );
        Application.ProcessMessages;                                                                            
   end;

   procedure EscribeFooter( var Bitacora: TStrings; lTipo: boolean );
   begin
        Bitacora.Add( Format( K_FOOTER, [ FormatDateTime( 'hh:nn:ss am/pm', Time ),
                                          aMensaje[ lTipo ],
                                          iRegErrores, iTotal ] ) );
   end;

   procedure LimpiaBitacora( var Bitacora: TStrings );
   begin
        Bitacora.Clear;
   end;

   procedure InitRegistros;
   begin
        iRegBitacora := 0;
        iRegErrores := 0;
   end;

   procedure IncrementaBitacora;
   begin
        Inc( iRegBitacora );
   end;

   procedure IncrementaErrores;
   begin
        Inc( iRegErrores );
   end;

   procedure LeeDatos;
   begin
        with cdsRFCEmpleados do
        begin
             sPaterno := FieldByName( 'CB_APE_PAT' ).AsString;
             sMaterno := FieldByName( 'CB_APE_MAT' ).AsString;
             sNombres := FieldByName( 'CB_NOMBRES' ).AsString;
             dFecNac  := FieldByName( 'CB_FEC_NAC' ).AsDateTime;
             sRFC     := FieldByName( 'CB_RFC' ).AsString;
             iCodigo  := FieldByName( 'CB_CODIGO' ).AsInteger;
             sEmpleado:= FieldByName( 'PRETTYNAME' ).AsString;
        end;
   end;

   function EscribeRFC: boolean;
   begin
        with oZetaProvider do
        begin
             EmpiezaTransaccion;
             try
                ParamAsString( FCursor, 'CB_RFC', sGenerado );
                ParamAsInteger( FCursor, 'CB_CODIGO', iCodigo );
                Result := ( Ejecuta( FCursor ) > 0 );
                TerminaTransaccion( TRUE );
             except
                   on Error: Exception do
                   begin
                        TerminaTransaccion( FALSE );
                        raise;
                   end;
             end;
        end;
   end;

   function FormateaRFC( const sRFC: string ): string;
   begin
        Result := Format( '%s-%s-%s', [ Copy( sRFC, 1, 4 ),
                                        Copy( sRFC, 5, 6 ),
                                        Copy( sRFC, 11, 3 ) ] );
   end;

   { Procedimientos Principales del Programa }
   { Diagnostico }
   function Diagnosticar: boolean;
   begin
        InitRegistros;
        with cdsRFCEmpleados do
        begin
             Result := not IsEmpty;
             if Result then
             begin
                  iTotal := RecordCount;
                  first;
                  while not EOF do
                  begin
                       LeeDatos;
                       sGenerado := ZetaCommonTools.CalcRFC( sPaterno, sMaterno, sNombres, dFecNac );
                       if ( sRFC <> sGenerado ) then
                       begin
                            oErrores.Add( Format( 'Empleado:  %0:d %1:s %2:s'+
                                                  'Fec. Nac.: %3:s %2:s'+
                                                  'Actual:    %4:s %2:s'+
                                                  'Generado:  %5:s %2:s', [ iCodigo, sEmpleado, CR_LF,
                                                                            FechaCorta( dFecNac ),
                                                                            FormateaRFC( sRFC ), FormateaRFC( sGenerado ) ] ) );
                            IncrementaErrores;
                       end;
                       Next;
                  end;
                  EscribeFooter( oBitacora, TRUE );
                  EscribeFooter( oErrores, TRUE );
             end;
        end;
   end;

   { Arreglar los Errores Encontrados }
   function Arreglar: boolean;
   begin
        InitRegistros;
        with cdsRFCEmpleados do
        begin
             Result := not IsEmpty;
             if Result then
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := dmCliente.Empresa;
                       FCursor := CreateQuery( GetScript( eUpdateRFC ) );
                  end;
                  try
                     iTotal := RecordCount;
                     first;
                     while not EOF do
                     begin
                          LeeDatos;
                          sGenerado := ZetaCommonTools.RFCVerifica( sRFC, sPaterno, sMaterno,
                                                                    sNombres, dFecNac );
                          if ( sRFC <> sGenerado ) then
                          begin
                               if EscribeRFC then
                               begin
                                    oBitacora.Add( Format( 'Empleado:  %0:d %1:s %2:s' +
                                                           'Fec. Nac.: %3:s %2:s'+
                                                           'Original:  %4:s %2:s' +
                                                           'Corregido: %5:s %2:s', [ iCodigo, sEmpleado, CR_LF,
                                                                                     FechaCorta( dFecNac ),
                                                                                     FormateaRFC( sRFC ), FormateaRFC( sGenerado ) ] ) );
                                    IncrementaErrores;
                               end;
                          end;
                          Next;
                     end;
                     EscribeFooter( oBitacora, FALSE );
                     LimpiaBitacora( oErrores );
                  finally
                         FreeAndNil( FCursor );
                  end;
             end;
        end;
   end;

   { Sobreescribir todos los Resgistros }
   function SobreEscribir: boolean;
   begin
        InitRegistros;
        with cdsRFCEmpleados do
        begin
             Result := not IsEmpty;
             if Result then
             begin
                  with oZetaProvider do
                  begin
                       EmpresaActiva := dmCliente.Empresa;
                       FCursor := CreateQuery( GetScript( eUpdateRFC ) );
                  end;
                  try
                     iTotal := RecordCount;
                     first;
                     while not EOF do
                     begin
                          LeeDatos;
                          sGenerado := ZetaCommonTools.CalcRFC( sPaterno, sMaterno, sNombres, dFecNac );
                          if ( sRFC <> sGenerado ) then
                          begin
                               if EscribeRFC then
                               begin
                                    oBitacora.Add( Format( 'Empleado:  %0:d %1:s %2:s' +
                                                           'Fec. Nac.: %3:s %2:s'+
                                                           'Original:  %4:s %2:s' +
                                                           'Corregido: %5:s %2:s', [ iCodigo, sEmpleado, CR_LF,
                                                                                     FechaCorta( dFecNac ),
                                                                                     FormateaRFC( sRFC ), FormateaRFC( sGenerado ) ] ) );
                                    IncrementaErrores;
                               end;
                          end;
                          Next;
                     end;
                     EscribeFooter( oBitacora, FALSE );
                     LimpiaBitacora( oErrores );
                  finally
                         FreeAndNil( FCursor );
                  end;
             end;
        end;
   end;

begin
     eTipo := eCorreccion( iTipo );
     InitBitacora( oBitacora, TRUE );
     InitBitacora( oErrores, FALSE );
     case eTipo of
          ecDiganostico:   Result := Diagnosticar;
          ecTodo:          Result := Arreglar;
          ecSobreEscribir: Result := SobreEscribir;
     else
         Result := FALSE;
     end;
end;

end.
