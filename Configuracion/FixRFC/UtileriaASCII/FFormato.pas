unit FFormato;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TFormato = class(TForm)
    Panel: TPanel;
    Salir: TBitBtn;
    Memo: TMemo;
    LogSave: TBitBtn;
    SaveDialog: TSaveDialog;
    procedure LogSaveClick(Sender: TObject);
  private
    function GetFileName(const sFile: String): String;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Formato: TFormato;

implementation

uses ZetaDialogo,
     ZetaCommonClasses,
     FFixRFC;

{$R *.DFM}

function TFormato.GetFileName( const sFile: String ): String;
begin
     Result := Format( '%s%s', [ ExtractFilePath( Application.ExeName ), sFile ] );
end;

procedure TFormato.LogSaveClick(Sender: TObject);
var
   sFileName: String;
begin
     sFileName := GetFileName( 'FormatoASCII.txt' );
     with SaveDialog do
     begin
          FilterIndex := 0;
          FileName := sFileName;
          InitialDir := ExtractFilePath( sFileName );
          if Execute then
          begin
               sFileName := FileName;
               Memo.Lines.SaveToFile( sFileName );
               if ZetaDialogo.zConfirm( '� Formato Exportado !', Format( 'El Formato Fu� Exportado Al Archivo %0:s %1:s %0:s � Desea Verlo ?', [ CR_LF, sFileName ] ), 0, mbOK ) then
               begin
                    CallNotePad( sFileName );
               end;
          end;
     end;
end;

end.
