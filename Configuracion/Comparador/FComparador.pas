unit FComparador;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, DBTables, ExtCtrls,
     FileCtrl, ComCtrls, Mask, DBCtrls, Grids, DBGrids, Db, IniFiles,
     ZetaWizard,
     ZetaNumero,
     ZetaKeyCombo;

type
  TComparator = class(TForm)
    Wizard: TZetaWizard;
    Anterior: TZetaWizardButton;
    Siguiente: TZetaWizardButton;
    Comparar: TZetaWizardButton;
    Cancelar: TZetaWizardButton;
    PageControl: TPageControl;
    Parametros: TTabSheet;
    Comparacion: TTabSheet;
    PanelResultados: TPanel;
    dsMaestro: TDataSource;
    StatusBar: TStatusBar;
    dsPrueba: TDataSource;
    QueryMaestroGB: TGroupBox;
    DBGridMaestro: TDBGrid;
    QueryPruebaGB: TGroupBox;
    DBGridPrueba: TDBGrid;
    Splitter: TSplitter;
    Diferencias: TBitBtn;
    SiguienteDiferencia: TBitBtn;
    AnteriorDiferencia: TBitBtn;
    WinDatosGB: TGroupBox;
    ScriptTestGB: TGroupBox;
    ScriptTest: TMemo;
    PanelPrueba: TPanel;
    AliasLBL: TLabel;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    Alias: TComboBox;
    Usuario: TEdit;
    Password: TEdit;
    DOSDatosGB: TGroupBox;
    ScriptMasterGB: TGroupBox;
    ScriptMaster: TMemo;
    PanelMaster: TPanel;
    AliasMaestroLBL: TLabel;
    UsuarioMaestroLBL: TLabel;
    PasswordMaestroLBL: TLabel;
    AliasMaestro: TComboBox;
    UsuarioMaestro: TEdit;
    PasswordMaestro: TEdit;
    Splitter1: TSplitter;
    SameScript: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure DiferenciasClick(Sender: TObject);
    procedure MaestroChange(Sender: TObject);
    procedure PruebaChange(Sender: TObject);
    procedure ScriptMasterChange(Sender: TObject);
    procedure SiguienteDiferenciaClick(Sender: TObject);
    procedure AnteriorDiferenciaClick(Sender: TObject);
    procedure SameScriptClick(Sender: TObject);
  private
    { Private declarations }
    FIniFile: TIniFile;
    FScriptLines: TStrings;
    FPruebaChange: Boolean;
    FMaestroChange: Boolean;
    FScriptChange: Boolean;
    procedure MuestraError( Sender: TObject; Error: Exception );
    procedure ShowStatus( const sMsg: String );
    procedure MostrarResultados;
    procedure SetControls( const lEnabled: Boolean );
    procedure LoadIniFile;
    procedure SaveIniFile;
  public
    { Public declarations }
  end;

var
  Comparator: TComparator;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     FDiferencias,
     DComparador;

{$R *.DFM}

const
     K_MAESTRO = 'MAESTRO';
     K_ALIAS = 'ALIAS';
     K_USER_NAME = 'USER NAME';
     K_PASSWORD = 'PASSWORD';
     K_PRUEBA = 'PRUEBA';
     K_SCRIPT = 'SCRIPT';
     K_QUERY = 'QUERY';

procedure TComparator.FormCreate(Sender: TObject);
var
   OldConfigMode: TConfigMode;
begin
     Environment;
     Application.OnException := MuestraError;
     with Session do
     begin
          OldConfigMode := ConfigMode;
          ConfigMode := cmAll; //[ cfmPersistent ];
          if not Active then
             Active := True;
          GetAliasNames( Alias.Items );
          GetAliasNames( AliasMaestro.Items );
          ConfigMode := OldConfigMode;
     end;
     Wizard.Primero;
     FScriptLines := TStringList.Create;
     FIniFile := TIniFile.Create( ExtractFilePath( Application.ExeName ) + 'Comparador.INI' );
     dmComparador := TdmComparador.Create( Self );
     LoadIniFile;
     FPruebaChange := True;
     FMaestroChange := True;
     FScriptChange := True;
end;

procedure TComparator.FormDestroy(Sender: TObject);
begin
     dmComparador.Free;
     FIniFile.Free;
     FScriptLines.Free;
end;

procedure TComparator.LoadIniFile;
var
   i: Integer;
begin
     with FIniFile do
     begin
          UsuarioMaestro.Text := ReadString( K_MAESTRO, K_USER_NAME, 'SYSDBA' );
          PasswordMaestro.Text := ReadString( K_MAESTRO, K_PASSWORD, 'm' );
          with Alias do
          begin
               ItemIndex := Items.IndexOf( ReadString( K_MAESTRO, K_ALIAS, 'Tress Datos' ) );
          end;
          Usuario.Text := ReadString( K_PRUEBA, K_USER_NAME, 'SYSDBA' );
          Password.Text := ReadString( K_PRUEBA, K_PASSWORD, 'm' );
          with AliasMaestro do
          begin
               ItemIndex := Items.IndexOf( ReadString( K_PRUEBA, K_ALIAS, 'Tress Local' ) );
          end;
          ReadSectionValues( K_SCRIPT, FScriptLines );
     end;
     with ScriptMaster.Lines do
     begin
          BeginUpdate;
          Clear;
          for i := 0 to ( FScriptLines.Count - 1 ) do
          begin
               Add( FScriptLines.Values[ FScriptLines.Names[ i ] ] );
          end;
          EndUpdate;
     end;
end;

procedure TComparator.SaveIniFile;
var
   i: Integer;
begin
     with FIniFile do
     begin
          WriteString( K_MAESTRO, K_USER_NAME, UsuarioMaestro.Text );
          WriteString( K_MAESTRO, K_PASSWORD, PasswordMaestro.Text );
          with Alias do
          begin
               WriteString( K_MAESTRO, K_ALIAS, Items.Strings[ ItemIndex ] );
          end;
          WriteString( K_PRUEBA, K_USER_NAME, Usuario.Text );
          WriteString( K_PRUEBA, K_PASSWORD, Password.Text );
          with AliasMaestro do
          begin
               WriteString( K_PRUEBA, K_ALIAS, Items.Strings[ ItemIndex ] );
          end;
          EraseSection( K_SCRIPT );
          with ScriptMaster.Lines do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    WriteString( K_SCRIPT, K_QUERY + IntToStr( i ), Strings[ i ] );
               end;
          end;
     end;
end;

procedure TComparator.SetControls( const lEnabled: Boolean );
begin
     Diferencias.Enabled := lEnabled;
     SiguienteDiferencia.Enabled := lEnabled;
     AnteriorDiferencia.Enabled := lEnabled;
end;

procedure TComparator.MuestraError( Sender: TObject; Error: Exception );
begin
     zExcepcion( '� Error En ' + Caption + ' !', '� Se Encontr� Un Error !', Error, 0 );
end;

procedure TComparator.ShowStatus( const sMsg: String );
begin
     StatusBar.SimpleText := sMsg;
end;

procedure TComparator.MostrarResultados;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with dmComparador do
        begin
             with dsMaestro do
             begin
                  Dataset := nil;
                  Dataset := tqMaestro;
             end;
             with dsPrueba do
             begin
                  Dataset := nil;
                  Dataset := tqPrueba;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TComparator.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
begin
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               oCursor := Screen.Cursor;
               Screen.Cursor := crHourglass;
               try
                  with dmComparador do
                  begin
                       with AliasMaestro do
                       begin
                            if FMaestroChange then
                            begin
                                 if ConectarMaestro( Items.Strings[ ItemIndex ], UsuarioMaestro.Text, PasswordMaestro.Text ) then
                                    FMaestroChange := False
                                 else
                                 begin
                                      ActiveControl := AliasMaestro;
                                      CanMove := False;
                                 end;
                            end;
                       end;
                       with Alias do
                       begin
                            if FPruebaChange then
                            begin
                                 if ConectarPrueba( Items.Strings[ ItemIndex ], Usuario.Text, Password.Text ) then
                                    FPruebaChange := False
                                 else
                                 begin
                                      ActiveControl := Alias;
                                      CanMove := False;
                                 end;
                            end;
                       end;
                  end;
                  if CanMove then
                  begin
                       SaveIniFile;
                  end;
               finally
                      Screen.Cursor := oCursor;
               end;
          end;
     end;
end;

procedure TComparator.WizardAfterMove(Sender: TObject);
begin
     with Wizard do
     begin
          QueryMaestroGB.Caption :=  ' Query Maestro: ' + AliasMaestro.Text + ' ';
          QueryPruebaGB.Caption :=  ' Query Prueba: ' + Alias.Text + ' ';

          if EsPaginaActual( Comparacion ) and FScriptChange then
          begin
               SetControls( False );
          end;
     end;
end;

procedure TComparator.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   lHayDiferencias: Boolean;
begin
     ShowStatus( 'Empezando Comparaci�n' );
     with dmComparador do
     begin
          if SameScript.Checked then
             lHayDiferencias := Comparar( ScriptMaster.Lines.Text, ScriptTest.Lines.Text )
          else
              lHayDiferencias := Comparar( ScriptMaster.Lines.Text );
          SetControls( lHayDiferencias );
          MostrarResultados;
          if not lHayDiferencias then
             zInformation( '� Buenas Noticias !', 'No Se Encontraron Diferencias', 0 );
          FScriptChange := False;
     end;
     ShowStatus( 'Comparaci�n Terminada' );
     lOk := True;
end;

procedure TComparator.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     lOk := True;
     Close;
end;

procedure TComparator.ScriptMasterChange(Sender: TObject);
begin
     FScriptChange := True;
end;

procedure TComparator.MaestroChange(Sender: TObject);
begin
     FMaestroChange := True;
end;

procedure TComparator.PruebaChange(Sender: TObject);
begin
     FPruebaChange := True;
end;

procedure TComparator.SameScriptClick(Sender: TObject);
begin
     with ScriptTest.Lines do
     begin
          Clear;
          if SameScript.Checked then
             Assign( ScriptMaster.Lines );
     end;
end;

procedure TComparator.DiferenciasClick(Sender: TObject);
begin
     FDiferencias.MostrarDiferencias;
end;

procedure TComparator.SiguienteDiferenciaClick(Sender: TObject);
var
   lHayDiferencias: Boolean;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        with dmComparador do
        begin
             lHayDiferencias := BuscaSiguienteDiferencia;
             if not lHayDiferencias then
                zInformation( '� Buenas Noticias !', 'No Hay M�s Diferencias', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TComparator.AnteriorDiferenciaClick(Sender: TObject);
var
   lHayDiferencias: Boolean;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        with dmComparador do
        begin
             lHayDiferencias := BuscaAnteriorDiferencia;
             if not lHayDiferencias then
                zInformation( '� Buenas Noticias !', 'No Hay M�s Diferencias', 0 );
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

end.
