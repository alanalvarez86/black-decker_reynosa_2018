unit FInitBD;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, FileCtrl,
     FMigrar,
     ZetaDBTextBox,
     ZetaWizard, jpeg;

type
  TInitBD = class(TMigrar)
    DBDataGB: TGroupBox;
    Alias: TZetaTextBox;
    AliasLBL: TLabel;
    UsuarioLBL: TLabel;
    Usuario: TZetaTextBox;
    DirectorioGB: TGroupBox;
    DirectorioLBL: TLabel;
    Directorio: TEdit;
    ArchivoBuscar: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ArchivoBuscarClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    FLista: TStrings;
    FEsComparte: Boolean;
    FBaseDatos: String;
    function GetDataFolder: String;
    function Init: Boolean;
    function InitFileList: Boolean;
    procedure CounterClear(Sender: TObject; var Continue: Boolean);
    procedure CounterMove(Sender: TObject; var lOk: Boolean);
    procedure SetDataFolder(const Value: String);
    procedure CounterStart(Sender: TObject; const sMsg: String);
    procedure FormatMensajeInicial;
    function GetCompanyName: String;
    function GetFolderTipoEmpresa: String;
  protected
    { Protected declarations }
    property DataFolder: String read GetDataFolder write SetDataFolder;
  public
    { Public declarations }
    property EsComparte: Boolean read FEsComparte write FEsComparte;
  end;

procedure InicializarDatos( const lEsComparte: Boolean );

var
  InitBD: TInitBD;

implementation

uses ZetaDialogo,
     ZetaTressCFGTools,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FHelpContext,
     DReportes,
     DDBConfig,
     ZetaCommonLists, 
     ZetaRegistryServer;

{$R *.DFM}

procedure InicializarDatos( const lEsComparte: Boolean );
begin
     with TInitBD.Create( Application ) do
     begin
          try
             //SetWizard( GetPointer );
             EsComparte:= lEsComparte;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;



procedure TInitBD.FormCreate(Sender: TObject);
begin
     inherited;
     EsComparte:= FALSE;
     dmMigracion := TdmReportes.Create( Self );
     FLista := TStringList.Create;
     HelpContext := H50015_Inicializando_los_datos_de_una_empresa;
end;

procedure TInitBD.FormShow(Sender: TObject);
const
     aCaption: array[FALSE..TRUE] of Pchar = (' Una Empresa', ' Comparte');
begin
     inherited;
     //Format( MensajeInicial.Lines, [ aCaption[FEsComparte], TargetAlias ] );
     EsComparte:= dmDBConfig.InicializaComparte;
     FBaseDatos:= aCaption[FEsComparte];
     FormatMensajeInicial;
     LogFileName := ZetaTressCFGTools.SetFileNameDefaultPath( 'InitBD.log' );
     {AP(26/06/2008): Se cambió para que tomara la inicialización de acuerdo al tipo }
     DataFolder := ZetaTressCFGTools.SetFileNameDefaultPath( GetFolderTipoEmpresa );
     Caption := 'Inicializar Datos De' + FBaseDatos;
     Alias.Caption := GetCompanyName;
     Usuario.Caption := TargetUserName;
     if EsComparte then
     begin
          with dmDbConfig.Registry do
          begin
               TargetAlias := AliasComparte;
               TargetUserName := UserName;
     		   TargetPassword := Password;
          end;
     end;
end;

procedure TInitBD.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FLista );
     FreeAndNil( dmMigracion );
     inherited;
end;

function TInitBD.GetDataFolder: String;
begin
     Result := Directorio.Text;
end;

procedure TInitBD.SetDataFolder(const Value: String);
begin
     Directorio.Text := Value;
end;

function TInitBD.GetFolderTipoEmpresa: String;
const
     K_COMPARTE = 'Comparte';
     K_VISITAS =  'Visitas';
     K_SELECCION = 'Seleccion';
     K_FUENTES = 'Fuentes';
var
   sTipoEmpresa: String;
begin
     sTipoEmpresa:= dmDBConfig.GetTipoEmpresa;

     if ( EsComparte ) then
     begin
          Result:= K_COMPARTE;
     end
     else if ( sTipoEmpresa = ObtieneElemento( lfTipoCompanyName, Ord( tcVisitas ) ) ) then
     begin
          Result:= K_VISITAS;
     end
     else if ( sTipoEmpresa = ObtieneElemento( lfTipoCompanyName, Ord( tcRecluta ) ) ) then
     begin
          Result:= K_SELECCION;
     end
     else
     begin
          Result:= K_FUENTES;
     end;
end;

function TInitBD.Init: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             AbreBDReceptora;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( 'ˇ Error Al Abrir Base De Datos Destino !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

function TInitBD.InitFileList: Boolean;
var
   oFileData: TSearchRec;
begin
     with FLista do
     begin
          BeginUpdate;
          Clear;
          try
             if ( SysUtils.FindFirst( Format( '%s*.DB', [ ZetaCommonTools.VerificaDir( DataFolder ) ] ), faAnyFile, oFileData ) = 0 ) then
             begin
                  try
                     repeat
                           Add( oFileData.Name );
                     until ( SysUtils.FindNext( oFileData ) <> 0 );
                  finally
                         SysUtils.FindClose( oFileData );
                  end;
             end;
          finally
                 EndUpdate;
          end;
          Result := ( Count > 0 );
     end;
end;

procedure TInitBD.CounterStart(Sender: TObject; const sMsg: String);
begin
     with AvanceProceso do
     begin
          Max := FLista.Count;
          Step := 1;
          Position := 0;
     end;
end;

procedure TInitBD.CounterMove(Sender: TObject; var lOk: Boolean);
begin
     StatusBar.Panels[ 0 ].Text := TdmReportes( dmMigracion ).ItemName;
     with AvanceProceso do
     begin
          StepIt;
     end;
     UpdateLogDisplay;
     Application.ProcessMessages;
     lOk := PuedeContinuar;
end;

procedure TInitBD.CounterClear(Sender: TObject; var Continue: Boolean);
begin
     UpdateLogDisplay;
     with AvanceProceso do
     begin
          Position := Max;
     end;
end;

procedure TInitBD.ArchivoBuscarClick(Sender: TObject);
var
   sDirectory: String;
begin
     sDirectory := DataFolder;
     if FileCtrl.SelectDirectory( sDirectory, [], 0 ) then
     begin
          DataFolder := sDirectory;
     end;
end;

procedure TInitBD.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Inicio ) then
               begin
                    CanMove := Self.Init;
               end
               else
                   if EsPaginaActual( DirectorioDOS ) then
                   begin
                        if not DirectoryExists( DataFolder ) then
                        begin
                             ZetaDialogo.ZError( Self.Caption, 'Directorio De Archivos Fuente No Existe', 0 );
                             CanMove := False;
                             ActiveControl := Directorio;
                        end
                        else
                            if not InitFileList then
                            begin
                                 CanMove := False;
                                 ZetaDialogo.ZError( Self.Caption, 'Directorio No Contiene Archivos *.DB', 0 );
                                 ActiveControl := Directorio;
                            end;
                   end;
          end;
     end;
end;

procedure TInitBD.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   oCursor: TCursor;
begin
     StartLog;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with TdmReportes( dmMigracion ) do
           begin
                SourceSharedPath := DataFolder;
                StartCallBack := Self.CounterStart;
                CallBack := Self.CounterMove;
                EndCallBack := Self.CounterClear;
                lOk := InicializaBaseDeDatos( FLista, FEsComparte );
           end;
        except
              on Error: Exception do
              begin
                   TdmReportes( dmMigracion ).ConfiguraBaseDeDatosError( 'ˇ Error Al Inicializar Base De Datos !', Error );
                   lOk := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     EndLog;
     EndProcess;
     inherited;
end;

function TInitBD.GetCompanyName: String;
begin
     if FEsComparte then
         Result:= dmDBConfig.Registry.AliasComparte
     else
         Result:= dmDBConfig.GetCompanyName;

end;


procedure TInitBD.FormatMensajeInicial;
begin
     with MensajeInicial.Lines do
     begin
          BeginUpdate;
          try
             Clear;
             Add( '' );
             Add( '' );
             Add( 'Este Proceso Inicializa los datos de' );
             Add( 'la base de datos de' );
             Add( FBaseDatos );
             Add( '' );
             Add( GetCompanyName );
             Add( 'Se Inicializarán Los Contenidos De' );
             Add( 'Las Tablas Representadas Por' );
             Add( 'Archivos *.DB En El Directorio' );
             Add( 'Que Se Pide.' );
             Add( '' );

          finally
                 EndUpdate;
          end;
     end;
end;



end.
