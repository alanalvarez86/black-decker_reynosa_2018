********* Constantes Globales ************
#define K_GLOBAL_RAZON_EMPRESA               1   //Raz�n Social de Empresa//
#define K_GLOBAL_CALLE_EMPRESA               2   //Calle de Empresa//
#define K_GLOBAL_COLONIA_EMPRESA             3   // Colonia de Empresa//
#define K_GLOBAL_CIUDAD_EMPRESA              4   //Ciudad de Empresa//
#define K_GLOBAL_ENTIDAD_EMPRESA             5   //Entidad de Empresa//
#define K_GLOBAL_CP_EMPRESA                  6   //C�digo Postal Empresa//
#define K_GLOBAL_TEL_EMPRESA                 7   //Tel�fono de Empresa//
#define K_GLOBAL_RFC_EMPRESA                 8   //R.F.C. de Empresa//
#define K_GLOBAL_INFONAVIT_EMPRESA           9   // # de INFONAVIT de Empresa//
#define K_GLOBAL_DIGITO_EMPRESA             10   //D�gito de Empresa//
#define K_GLOBAL_NUM_IMSS                   11   //N�mero de Gu�a IMSS//
#define K_GLOBAL_REPRESENTANTE              12   //Representante Legal//
#define K_GLOBAL_NUM_EMP_AUTOMATICO	    116	 //Generar Num Emp Autom�tico//

******** Niveles de Organigrama **********
#define K_GLOBAL_NIVEL1			    13   // Nombre de Nivel #1//
#define K_GLOBAL_NIVEL2			    14   //Nombre de Nivel #2//
#define K_GLOBAL_NIVEL3			    15   //Nombre de Nivel #3//
#define K_GLOBAL_NIVEL4			    16   //Nombre de Nivel #4//
#define K_GLOBAL_NIVEL5			    17   //Nombre de Nivel #5//
#define K_GLOBAL_NIVEL6			    18   //Nombre de Nivel #6//
#define K_GLOBAL_NIVEL7			    19   //Nombre de Nivel #7//
#define K_GLOBAL_NIVEL8			    20   //Nombre de Nivel #8//
#define K_GLOBAL_NIVEL9			    21   //Nombre de Nivel #9//

******** Variables Adicionales **********
#define K_GLOBAL_FECHA1			    22   //Nombre de Fecha #1//
#define K_GLOBAL_FECHA2			    23   //Nombre de Fecha #2//
#define K_GLOBAL_FECHA3			    24   //Nombre de Fecha #3//
#define K_GLOBAL_TEXTO1			    25   //Nombre de Texto #1//
#define K_GLOBAL_TEXTO2			    26   //Nombre de Texto #2//
#define K_GLOBAL_TEXTO3			    27   //Nombre de Texto #3//
#define K_GLOBAL_TEXTO4			    28   //Nombre de Texto #4//
#define K_GLOBAL_NUM1			    29   //Nombre de N�mero #1//
#define K_GLOBAL_NUM2			    30   //Nombre de N�mero #2//
#define K_GLOBAL_NUM3			    31   //Nombre de N�mero #3//
#define K_GLOBAL_LOG1			    32   //Nombre de L�gico #1//
#define K_GLOBAL_LOG2			    33   //Nombre de L�gico #2//
#define K_GLOBAL_LOG3			    34   //Nombre de L�gico #3//
#define K_GLOBAL_TAB1			    35   //Nombre de Tabla #1//
#define K_GLOBAL_TAB2			    36   //Nombre de Tabla #2//
#define K_GLOBAL_TAB3			    37   //Nombre de Tabla #3//
#define K_GLOBAL_TAB4			    38   //Nombre de Tabla #4//

**************** N�mina ******************
#define K_GLOBAL_REDONDEO_NETO              39   //Redondeo de Neto//
#define K_GLOBAL_PRIMER_DIA                 40   //Primer d�a de la semana//
#define K_GLOBAL_LLEVAR_SALDO_REDONDEO      41   //Llevar Saldo de Redondeo//
#define K_GLOBAL_PROMEDIAR_SALARIOS         42   //Promediar Salarios//
#define K_GLOBAL_PORCEN_SUBSIDIO            43   //% Subsidio Acreditable//
#define K_GLOBAL_ZONAS_GEOGRAFICAS          44   //Zonas Geogr�ficas//
#define K_GLOBAL_FORMATO_POLIZA             45   //Formato de P�liza Contable//
#define K_GLOBAL_FORMATO_BANCA              46   //Formato Banca Electr�nica//
#define K_GLOBAL_NUM_GLOBAL1                47   //N�mero Global #1//
#define K_GLOBAL_NUM_GLOBAL2                48   //N�mero Global #2//
#define K_GLOBAL_NUM_GLOBAL3                49   //N�mero Global #3//
#define K_GLOBAL_ISPT                       50   //Concepto para ISPT//
#define K_GLOBAL_CREDITO                    51   //Concepto Cr�dito al Salario//
#define K_GLOBAL_AJUSTE                     52   //Concepto Ajuste de Moneda//
#define K_GLOBAL_INCOBRABLES                53   //Concepto Deduc. Incobrables//
#define K_GLOBAL_SEPARAR_CRED_SAL           54   //Separar Cr�dito al Salario//
#define K_GLOBAL_PROM_SAL_MIN               55   //Promediar Salarios M�nimos//
#define K_GLOBAL_NUM_GLOBAL4                56   //N�mero Global #4//
#define K_GLOBAL_NUM_GLOBAL5                57   //N�mero Global #5//
#define K_GLOBAL_NUM_GLOBAL6                58   //N�mero Global #6//
#define K_GLOBAL_NUM_GLOBAL7                59   //N�mero Global #7//
#define K_GLOBAL_NUMERO_GLOBAL8             60   //N�mero Global #8//
#define K_GLOBAL_DIR_PLANT                  61   //Directorio Plantillas//

************* IMSS/INFONAVIT *************
#define K_GLOBAL_ADICIONAL_RETIRO           62   //Concepto Adicional Retiro//
#define K_GLOBAL_FORM_PERCEP                63   //F�rmula M�d. 18 Percepciones//
#define K_GLOBAL_FORM_DIAS                  64   //F�rmula M�d. 18 D�as//
#define K_GLOBAL_FILE_EMP_SUA               65   //SUA: Archivo Empleados//
#define K_GLOBAL_FILE_MOVS_SUA              66   //SUA: Archivo Movimientos//
#define K_GLOBAL_DIR_DATOS_SUA              67   //SUA: Directorio de Datos//
#define K_GLOBAL_FEC_1_EXP_SUA              68   //SUA: Fecha 1a. Exportaci�n//
#define K_GLOBAL_DELETE_FILE_SUA            69   //SUA: Bborrar Archivos//
#define K_GLOBAL_TOPE_INFONAVIT             70   //Tope Amortizaci�n INFONAVIT//
#define K_GLOBAL_TAB_REDON_ORD              71   //Tabla Redondeo Ordinarias//
#define K_GLOBAL_TAB_REDON_X                72   //Tabla Redondeo Extras//
#define K_GLOBAL_GRACIA_2CHECK              73   //Gracia Checadas Repetidas//
#define K_GLOBAL_AUT_X                      74   //Autorizar Horas Extras//
#define K_GLOBAL_AUT_FESTIVO_TRAB           75   //Autorizar Festivo Trabajado//
#define K_GLOBAL_TRATO_X_SAB                76   //Trato Extras en S�bado//
#define K_GLOBAL_TRATO_X_DESC               77   //Trato Extras en Descanso//
#define K_GLOBAL_TRATO_X_FESTIVO            78   //Trato Extras en Festivo//
#define K_GLOBAL_SUMAR_JORNADAS             79   //Sumar Jornadas de Horarios//

**************** Capacitaci�n *****************
#define K_GLOBAL_GIRO_EMPRESA               80   //Giro de la Empresa//
#define K_GLOBAL_REGISTRO_STPS              81   //# de Registro ante STPS//
#define K_GLOBAL_REPRESENTA_STPS_EMPRESA    82   //Representa Empresa STPS//
#define K_GLOBAL_REPRESENTA_STPS_EMPLEADO   83   //Representa Empleados STPS//

************* VARIABLES GLOBALES **************
#define K_GLOBAL_NUM_GLOBAL9                84   //N�mero Global #9//
#define K_GLOBAL_NUM_GLOBAL10               85   //N�mero Global #10//
#define K_GLOBAL_TEXT_GLOBAL1               86   //Texto Global #1 //
#define K_GLOBAL_TEXT_GLOBAL2               87   //Texto Global #2//
#define K_GLOBAL_TEXT_GLOBAL3               88   //Texto Global #3//

********* Defaults de Captura de Empleados ********
#define K_GLOBAL_DEF_CHECA_TARJETA          89   // Default: Checa Tarjeta //
#define K_GLOBAL_DEF_NACIONALIDAD           90   // Default: Nacionalidad //
#define K_GLOBAL_DEF_SEXO                   91   // Default: Sexo //
#define K_GLOBAL_DEF_SALARIO_TAB            92   // Default: Salario por Tabulador //
#define K_GLOBAL_DEF_SALARIO_DIARIO         93   // Default: Salario Diario //
#define K_GLOBAL_DEF_LETRA_GAFETE           94   // Default: Letra de Gafete //
#define K_GLOBAL_DEF_ESTADO_CIVIL           95   // Default: C�digo de Estado Civil //
#define K_GLOBAL_DEF_VIVE_EN                96   // Default: C�digo de Vive En //
#define K_GLOBAL_DEF_VIVE_CON               97   // Default: C�digo de Vive Con //
#define K_GLOBAL_DEF_MEDIO_TRANSPORTE       98   // Default: C�digo de Medio de Transporte //
#define K_GLOBAL_DEF_ESTUDIOS               99   // Default: C�digo de Grado de Estudios //
#define K_GLOBAL_DEF_CONTRATO               100  // Default: Tipo de Contrato //
#define K_GLOBAL_DEF_PUESTO                 101  // Default: C�digo de Puesto //
#define K_GLOBAL_DEF_CLASIFICACION          102  // Default: Clasificaci�n / Tabulador //
#define K_GLOBAL_DEF_TURNO                  103  // Default: C�digo de Turno //
#define K_GLOBAL_DEF_PRESTACIONES           104  // Default: C�digo Tabla Prestaciones //
#define K_GLOBAL_DEF_REGISTRO_PATRONAL      105  // Default: C�digo Registro Patronal //
#define K_GLOBAL_DEF_NIVEL_1                106  // Default: C�digo de Nivel #1 //
#define K_GLOBAL_DEF_NIVEL_2                107  // Default: C�digo de Nivel #2 //
#define K_GLOBAL_DEF_NIVEL_3                108  // Default: C�digo de Nivel #3 //
#define K_GLOBAL_DEF_NIVEL_4                109  // Default: C�digo de Nivel #4 //
#define K_GLOBAL_DEF_NIVEL_5                110  // Default: C�digo de Nivel #5 //
#define K_GLOBAL_DEF_NIVEL_6                111  // Default: C�digo de Nivel #6 //
#define K_GLOBAL_DEF_NIVEL_7                112  // Default: C�digo de Nivel #7 //
#define K_GLOBAL_DEF_NIVEL_8                113  // Default: C�digo de Nivel #8 //
#define K_GLOBAL_DEF_NIVEL_9                114  // Default: C�digo de Nivel #9 //
#define K_GLOBAL_DEF_MOTIVO_BAJA            115  // Default: C�digo de Motivo de Baja //
#define K_GLOBAL_LETRERO                    118  // Default: Mensaje de Exito //
#define K_GLOBAL_AGREGAR_CON_ERROR          119  // Default: Agregar con Error Al Importar //
#define K_EXTRAS_NO_CHECAN		    121  // Default: Extras para los que no Checan //
#define K_DEFAULT_RECIBOS_PAGADOS	    122  // Default: Default Recibos Pagados //
#define K_TITULO_MONTO_ADICIONAL_RECIBO     123  // Default: T�tulo Monto Adicional Recibo //
#define K_FORMULA_MONTO_ADICIONAL_RECIBO    124  // Default: F�rmula Monto Adicional Recibo //
#define K_NIVEL_PROGRAMACION_CURSOS         125  // Default: Nivel Programaci�n de Cursos //
#define K_NIVEL_CONTEO_1		    126  // Default: Nivel de Conteo #1 //
#define K_NIVEL_CONTEO_2	            127  // Default: Nivel de Conteo #2 //
#define K_NIVEL_CONTEO_3		    128  // Default: Nivel de Conteo #3 //
#define K_NIVEL_CONTEO_4		    129  // Default: Nivel de Conteo #4 //
#define K_NIVEL_CONTEO_5		    130  // Default: Nivel de Conteo #5 //
#define K_SUPER_SEGUNDOS                    131  // Default: Super: Segundos de Inactividad //
#define K_LIMITE_MODIFICAR_ASISTENCIA       132  // Default: L�mite para Modificar Asistencia //

********* Defaults de Wizards ********
#define K_GLOBAL_DEF_PROGRAMA_POLL     			2001 // Default: Programa Poll //
#define K_GLOBAL_DEF_PARAMETROS_POLL			2002 // Default: Par�metros Poll //
#define K_GLOBAL_DEF_ARCHIVO_POLL	    		2003 // Default: Archivo Poll //
#define K_GLOBAL_DEF_LISTADO_NOMINA         		2004 // Default: Listado de N�mina //
#define K_GLOBAL_DEF_RECIBOS_NOMINA         		2005 // Default: Recibos de N�mina //
#define K_GLOBAL_DEF_POLIZA                 		2006 // Default: Listado Poliza Contable //
#define K_GLOBAL_DEF_ASCII_POLIZA           		2007 // Default: Ascii Poliza Contable //
#define K_GLOBAL_DEF_IMP_MOVS               		2008 // Default: Archivo Importacion Mov de Nomina //
#define K_GLOBAL_DEF_PROM_CONCEPTO          		2009 // Default: Promediar Variables : Conceptos //
#define K_GLOBAL_DEF_PROM_DIAS              		2010 // Default: Promediar Variables : Dias Considerados //
#define K_GLOBAL_DEF_AGUINALDO_FALTAS              	2011 // Default: Faltas En Aguinaldo //
#define K_GLOBAL_DEF_AGUINALDO_INCAPACIDADES       	2012 // Default: Incapacidades Aguinaldo //
#define K_GLOBAL_DEF_AGUINALDO_ANTICIPOS           	2013 // Default: Anticipos En Aguinaldo //
#define K_GLOBAL_DEF_REPARTIR_AHORROS_TIPO         	2014 // Default: Tipo De Ahorro a Repartir //
#define K_GLOBAL_DEF_REPARTIR_AHORROS_MONTO        	2015 // Default: Monto Ahorrado a Repartir //
#define K_GLOBAL_DEF_REPARTIR_AHORROS_PROPORCIONAL 	2016 // Default: Proporcionar Ahorro //
#define K_GLOBAL_DEF_REPARTIR_AHORROS_CONCEPTO          2017 // Default: Concepto Reparto Ahorros //
#define K_GLOBAL_DEF_AGUINALDO_REPORTE			2018 // Default: Reporte de Aguinaldo //
#define K_GLOBAL_DEF_REPARTIR_AHORROS_REPORTE		2019 // Default: Reporte de Reparto de Ahorros //
#define K_GLOBAL_DEF_COMP_ANUAL_INGRESO_BRUTO		2020 // Default Comp Anual Ingreso Bruto //
#define K_GLOBAL_DEF_COMP_ANUAL_INGRESO_EXENTO		2021 // Default Comp Anual Ingreso Exento //
#define K_GLOBAL_DEF_COMP_ANUAL_IMPUESTO_RETENIDO	2022 // Default Comp Anual Impuesto Retenido //
#define K_GLOBAL_DEF_COMP_ANUAL_CREDITO_PAGADO		2023 // Default Comp Anual Credito Pagado //
#define K_GLOBAL_DEF_COMP_ANUAL_CALCULO_IMPUESTO	2024 // Default Comp Anual Calculo Impuesto //
#define K_GLOBAL_DEF_COMP_ANUAL_REPORTE                 2025 // Default: Reporte Comp Anual //
#define K_GLOBAL_DEF_CRED_SAL_REPORTE			2026 // Default: Reporte Decl Crd Sal //
#define K_GLOBAL_DEF_CRED_SAL_ING_BRUTO			2027 // Default: Decl Crd Sal Ing Bruto //
#define K_GLOBAL_DEF_CRED_SAL_ING_EXENTO		2028 // Default: Decl Crd Sal Ing Exento //
#define K_GLOBAL_DEF_CRED_SAL_PAGADO			2029 // Default: Decl Crd Sal Crd Pagado //
#define K_GLOBAL_DEF_PTU_REPORTE                	2030 // Default: Reporte de PTU  //
#define K_GLOBAL_DEF_PTU_UTILIDAD_REPARTIR         	2031 // Default: PTU Utilidad Repartir  //
#define K_GLOBAL_DEF_PTU_SUMA_DIAS                 	2032 // Default: PTU Suma de D�as  //
#define K_GLOBAL_DEF_PTU_PERCEPCIONES              	2033 // Default: PTU Percepciones  //
#define K_GLOBAL_DEF_PTU_FILTRO_DIRECTOR           	2034 // Default: PTU Filtro Director  //
#define K_GLOBAL_DEF_PTU_FILTRO_PLANTA             	2035 // Default: PTU Filtro Planta  //
#define K_GLOBAL_DEF_PTU_FILTRO_EVENTUAL           	2036 // Default: PTU Filtro Eventual  //
#define K_GLOBAL_DEF_PTU_SALARIO_TOPE              	2037 // Default: PTU Salario Tope  //

















