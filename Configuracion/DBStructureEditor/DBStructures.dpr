program DBStructures;

uses
  Forms,
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FSPConsulta in '..\FSPConsulta.pas' {SPConsulta},
  DReportes in 'DReportes.pas' {dmReportes: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TSPConsulta, SPConsulta);
  with SPConsulta do
  begin
       Init;
       Caption := 'Scripts para SQL Server 2000';
  end;
  Application.Run;
end.
