inherited SPEdit: TSPEdit
  BorderIcons = [biSystemMenu, biMinimize, biMaximize]
  Caption = 'Editar Script de MS SQL'
  ClientHeight = 326
  ClientWidth = 400
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 290
    Width = 400
    inherited OK: TBitBtn
      Left = 232
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 317
    end
  end
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 400
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object SE_NOMBRElbl: TLabel
      Left = 24
      Top = 9
      Width = 40
      Height = 13
      Alignment = taRightJustify
      BiDiMode = bdLeftToRight
      Caption = '&Nombre:'
      FocusControl = SE_NOMBRE
      ParentBiDiMode = False
    end
    object SE_TIPOlbl: TLabel
      Left = 40
      Top = 51
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = '&Tipo:'
    end
    object SE_DESCRIPlbl: TLabel
      Left = 5
      Top = 30
      Width = 59
      Height = 13
      Alignment = taRightJustify
      Caption = '&Descripci'#243'n:'
      FocusControl = SE_DESCRIP
    end
    object SE_SEQ_NUMlbl: TLabel
      Left = 10
      Top = 72
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = '&Secuencia:'
      FocusControl = SE_SEQ_NUM
    end
    object SE_ARCHIVOlbl: TLabel
      Left = 25
      Top = 93
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Archivo:'
      FocusControl = SE_ARCHIVO
    end
    object SE_ARCHIVOSeek: TSpeedButton
      Left = 373
      Top = 89
      Width = 23
      Height = 22
      Hint = 'Buscar Archivo de Scripts'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SE_ARCHIVOSeekClick
    end
    object SE_NOMBRE: TDBEdit
      Left = 66
      Top = 5
      Width = 148
      Height = 21
      DataField = 'SE_NOMBRE'
      DataSource = DataSource
      TabOrder = 0
    end
    object SE_TIPO: TZetaDBKeyCombo
      Left = 66
      Top = 47
      Width = 148
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      ListaFija = lfNinguna
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      DataField = 'SE_TIPO'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object SE_DESCRIP: TDBEdit
      Left = 66
      Top = 26
      Width = 303
      Height = 21
      DataField = 'SE_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
    object SE_SEQ_NUM: TZetaDBNumero
      Left = 66
      Top = 68
      Width = 43
      Height = 21
      Mascara = mnDias
      TabOrder = 3
      Text = '0'
      DataField = 'SE_SEQ_NUM'
      DataSource = DataSource
    end
    object SE_ARCHIVO: TDBEdit
      Left = 66
      Top = 89
      Width = 303
      Height = 21
      DataField = 'SE_ARCHIVO'
      DataSource = DataSource
      TabOrder = 4
    end
  end
  object CodigoGB: TGroupBox
    Left = 0
    Top = 113
    Width = 400
    Height = 177
    Align = alClient
    Caption = ' C'#243'd&igo '
    Color = clBtnFace
    ParentColor = False
    TabOrder = 2
    object PanelIB: TPanel
      Left = 363
      Top = 15
      Width = 35
      Height = 160
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object IBLoadFromFile: TSpeedButton
        Left = 5
        Top = 5
        Width = 25
        Height = 25
        Hint = 'Leer Archivo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333333FFFFFFFFF333333000000000033333377777777773333330FFFFF
          FFF03333337F333333373333330FFFFFFFF03333337F3FF3FFF73333330F00F0
          00F03333F37F773777373330330FFFFFFFF03337FF7F3F3FF3F73339030F0800
          F0F033377F7F737737373339900FFFFFFFF03FF7777F3FF3FFF70999990F00F0
          00007777777F7737777709999990FFF0FF0377777777FF37F3730999999908F0
          F033777777777337F73309999990FFF0033377777777FFF77333099999000000
          3333777777777777333333399033333333333337773333333333333903333333
          3333333773333333333333303333333333333337333333333333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = IBLoadFromFileClick
      end
    end
    object SE_DATA: TDBMemo
      Left = 2
      Top = 15
      Width = 361
      Height = 160
      Align = alClient
      DataField = 'SE_DATA'
      DataSource = DataSource
      ScrollBars = ssBoth
      TabOrder = 1
    end
  end
  object DataSource: TDataSource
    Left = 248
    Top = 48
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'sql'
    Filter = 
      'Archivos Texto ( *.txt )|*.txt|Scripts de SQL ( *.sql )|*.sql|To' +
      'dos los Archivos |*.*'
    FilterIndex = 2
    Title = 'Escoja El Archivo Deseado'
    Left = 224
    Top = 8
  end
end
