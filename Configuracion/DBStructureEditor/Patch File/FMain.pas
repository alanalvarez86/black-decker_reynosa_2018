unit FMain;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons;

type
  eAction = ( eClose, ePatch, eMSSQLStructure );
  TMain = class(TForm)
    Patch: TBitBtn;
    EstructuraMSSQL: TBitBtn;
    Salir: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure PatchClick(Sender: TObject);
    procedure EstructuraMSSQLClick(Sender: TObject);
  private
    { Private declarations }
    FAccion: eAction;
  public
    { Public declarations }
    property Accion: eAction read FAccion;
  end;

function GetAccion: eAction;

implementation

function GetAccion: eAction;
var
  Main: TMain;
begin
     Result := eClose;
     Main := TMain.Create( Application );
     try
        with Main do
        begin
             ShowModal;
             if ( ModalResult = mrOk ) then
                Result := Accion;
        end;
     finally
            FreeAndNil( Main );
     end;
end;

{$R *.DFM}

procedure TMain.FormCreate(Sender: TObject);
begin
     FAccion := eClose;
end;

procedure TMain.PatchClick(Sender: TObject);
begin
     FAccion := ePatch;
end;

procedure TMain.EstructuraMSSQLClick(Sender: TObject);
begin
     FAccion := eMSSQLStructure;
end;

end.
