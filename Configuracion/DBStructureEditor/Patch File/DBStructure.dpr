program DBStructure;

uses
  Forms,
  DStructure in 'DStructure.pas' {dmDBStruct: TDataModule},
  FMain in 'FMain.pas' {Main},
  FSPConsulta in 'FSPConsulta.pas',
  ZBaseDlgModal in '..\..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FStructure in 'FStructure.pas' {DBStruct};

{$R *.RES}

begin
  Application.Initialize;
  case FMain.GetAccion of
       ePatch: Application.CreateForm(TDBStruct, DBStruct);
       eMSSQLStructure: Application.CreateForm(TSPConsulta, SPConsulta);

  end;
  Application.Run;
end.
