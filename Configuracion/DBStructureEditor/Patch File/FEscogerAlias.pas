unit FEscogerAlias;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DBTables, ExtCtrls,
     DStructure,
     ZetaCommonLists,
     ZetaKeyCombo;

type
  TEscogerAlias = class(TForm)
    OpenDialog: TOpenDialog;
    DatabaseGB: TGroupBox;
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    DatabaseFileLBL: TLabel;
    DatabaseFile: TEdit;
    DatabaseFileSeek: TSpeedButton;
    Usuario: TEdit;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    Password: TEdit;
    DBType: TRadioGroup;
    PanelVersion: TPanel;
    Version: TComboBox;
    VersionLBL: TLabel;
    procedure FormShow(Sender: TObject);
    procedure DatabaseFileSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDatabase: String;
    function GetUsuario: String;
    function GetPassword: String;
    function GetTypeDB: eDbType;
    function GetVersion: String;
  end;

var
  EscogerAlias: TEscogerAlias;

function EscogeUnAlias( var sDatabase, sUsuario, sPassword, sVersion: String; var eType: eDbType ): Boolean;

implementation

{$R *.DFM}

function EscogeUnAlias( var sDatabase, sUsuario, sPassword, sVersion: String; var eType: eDbType ): Boolean;
begin
     if not Assigned( EscogerAlias ) then
        EscogerAlias := TEscogerAlias.Create( Application );
     with EscogerAlias do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               sDatabase := GetDatabase;
               sUsuario := GetUsuario;
               sPassword := GetPassword;
               eType := GetTypeDB;
               sVersion := GetVersion;
               Result := True;
          end
          else
              Result := False;
     end;
end;

{ ************ TEscogerAlias ************** }

procedure TEscogerAlias.FormCreate(Sender: TObject);
var
   eValor: eDbType;
begin
     with Version do
     begin
          DStructure.FillVersionList( Items );
          ItemIndex := 1;
     end;
     with DBType do
     begin
          with Items do
          begin
               for eValor := Low( eDbType ) to High( eDbType ) do
               begin
                    Add( DStructure.GetDbType( eValor ) );
               end;
          end;
          ItemIndex := 0;
     end;
end;

procedure TEscogerAlias.FormShow(Sender: TObject);
begin
     ActiveControl := DatabaseFile;
     DBType.ItemIndex := Ord( dmDBStruct.DBType );
end;

function TEscogerAlias.GetTypeDB: eDbType;
begin
     Result := eDbType( DBType.ItemIndex );
end;

function TEscogerAlias.GetDatabase: String;
begin
     Result := DatabaseFile.Text;
end;

function TEscogerAlias.GetUsuario: String;
begin
     Result := Usuario.Text;
end;

function TEscogerAlias.GetPassword: String;
begin
     Result := Password.Text;
end;

function TEscogerAlias.GetVersion: String;
begin
     with Version do
     begin
          Result := Text; // Items.Strings[ ItemIndex ];
     end;
end;

procedure TEscogerAlias.DatabaseFileSeekClick(Sender: TObject);
begin
     with DatabaseFile do
     begin
          with OpenDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

end.
