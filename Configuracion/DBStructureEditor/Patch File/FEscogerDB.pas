unit FEscogerDB;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DBTables, ExtCtrls,
     DStructure,
     ZetaCommonLists,
     ZetaKeyCombo;

type
  TEscogerDB = class(TForm)
    OpenDialog: TOpenDialog;
    DatabaseGB: TGroupBox;
    PanelBotones: TPanel;
    OK: TBitBtn;
    Cancelar: TBitBtn;
    DatabaseFileLBL: TLabel;
    DatabaseFile: TEdit;
    DatabaseFileSeek: TSpeedButton;
    Usuario: TEdit;
    UsuarioLBL: TLabel;
    PasswordLBL: TLabel;
    Password: TEdit;
    procedure FormShow(Sender: TObject);
    procedure DatabaseFileSeekClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function GetDatabase: String;
    function GetUsuario: String;
    function GetPassword: String;
  end;

function EscogerBaseDeDatos( var sDatabase, sUsuario, sPassword: String ): Boolean;

implementation

{$R *.DFM}

var
  EscogerDB: TEscogerDB;

function EscogerBaseDeDatos( var sDatabase, sUsuario, sPassword: String ): Boolean;
begin
     if not Assigned( EscogerDB ) then
        EscogerDB := TEscogerDB.Create( Application );
     with EscogerDB do
     begin
          ShowModal;
          if ( ModalResult = mrOk ) then
          begin
               sDatabase := GetDatabase;
               sUsuario := GetUsuario;
               sPassword := GetPassword;
               Result := True;
          end
          else
              Result := False;
     end;
end;

{ ************ TEscogerAlias ************** }

procedure TEscogerDB.FormShow(Sender: TObject);
begin
     ActiveControl := DatabaseFile;
end;

function TEscogerDB.GetDatabase: String;
begin
     Result := DatabaseFile.Text;
end;

function TEscogerDB.GetUsuario: String;
begin
     Result := Usuario.Text;
end;

function TEscogerDB.GetPassword: String;
begin
     Result := Password.Text;
end;

procedure TEscogerDB.DatabaseFileSeekClick(Sender: TObject);
begin
     with DatabaseFile do
     begin
          with OpenDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

end.
