�
 TDBSTRUCT 0'   TPF0	TDBStructDBStructLeft[Top3Width�HeightVCaption-Crear Archivos de Estructura de Base de DatosColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style MenuMainMenuOldCreateOrder	PositionpoScreenCenterOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight TPanelPanelControlesLeft Top!Width�Height� AlignalClient
BevelOuterbvNoneTabOrder 
OnDblClickEditarModificarClick 	TSplitter	Splitter1Left� Top Height�   TPageControlPageControlLeft� Top Width� Height� 
ActivePageMSSQLAlignalClientTabOrder  	TTabSheet	InterbaseCaption
&Interbase TDBMemoSP_INTRBASELeft Top Width� Height� AlignalClient	DataFieldSP_INTRBASE
DataSourcedsSPFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontReadOnly	TabOrder 
OnDblClickEditarModificarClick   	TTabSheetMSSQLCaption&MS SQL TDBMemo	SP_ORACLELeft Top Width� Height� AlignalClient	DataField	SP_ORACLE
DataSourcedsSPFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontReadOnly	TabOrder 
OnDblClickEditarModificarClick    TPanel	PanelGridLeft Top Width� Height� AlignalLeftTabOrder TDBGridDBGridLeftTopWidth� Height� AlignalClient
DataSourcedsSPOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgAlwaysShowSelectiondgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickEditarModificarClickColumnsExpanded	FieldName
SP_VERSIONTitle.CaptionVer #WidthVisible	 	AlignmenttaRightJustifyExpanded	FieldNameSP_TIPOTitle.CaptionTipoWidthVisible	 Expanded	FieldName
SP_SEQ_NUMTitle.CaptionSeq #Width!Visible	 Expanded	FieldNameSP_NAMETitle.CaptionNombreWidth� Visible	      TPanelPanelBotonesLeft Top Width�Height!AlignalTopTabOrder TSpeedButtonAgregarLeft� TopWidthHeightHintAgregar Registro
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337�333330����337�333330����3?��333��������ww�333w;������7w�3?�ww30��  337�3wws330���3337�37�330��3337�3w�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphsParentShowHintShowHint	OnClickEditarAgregarClick  TSpeedButtonBorrarLeft� TopWidthHeightHintBorrar Registro
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUU_���_U��wwwuuY�WwwwuuWw�UUUUUP0UUUUU�W��UUUP[�UUUUWu�w�UUU�  UUUUuWww�UUP���UUUWUuWW�UU���UUUUUuW�UU��� UUUUWUw�UP����UUWUUW��U�� �UUuu�Uw��P�࿰��UW�W_WW�� ���Uw�u����  ��  �Uww�Www��  UP�UwwuUWWUP  UUUUWwwUUUUU UUUUUwuUUUuU	NumGlyphsParentShowHintShowHint	OnClickEditarBorrarClick  TSpeedButton	ModificarLeft� TopWidthHeightHintModificar Registro
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsParentShowHintShowHint	OnClickEditarModificarClick  TSpeedButtonBuscarLeft� TopWidthHeightHintBuscar Registro
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333?3333330 333333�w333333 33333?ws333330 333333�w333333 333?�?ws337 ;p333?ws�w333ww �333w37ws330��p3337�3�7�33w����s33737�7?33����333���33����337ww333����33s�7�3s33w����s337�737�330��p3337?�3�3333ww3333w?�s33337 333333ws3333	NumGlyphsParentShowHintShowHint	OnClickEditarBuscarClick  TSpeedButtonExportarLeftTopWidthHeightHintExportar Tablas a Paradox
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333�333333�3333333w�333333�3?����w�   ����3wwwwwww��𙙙�37wwww�𙙙��?�wwwww 𙙙�w7wwwws�𙙙�3?�wwww3 ���3w337ws3����33?���w33 �  33w7wws33���333?��s333��3333w7�3333��3333��s3333   33333www333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickParadoxExportarClick  TDBNavigatorDBNavigatorLeftTopWidthtHeight
DataSourcedsSPVisibleButtonsnbFirstnbPriornbNextnbLast Hints.StringsPrimeroAnterior	SiguienteUltimoAgregarBorrar	Modificar ParentShowHintShowHint	TabOrder   TBitBtnAbrirLeft&TopWidthKHeightHintAbrir Archivos de EstructuraCaptionAbrirParentShowHintShowHint	TabOrderOnClickArchivoAbrirClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU_�����UU     UUUwwwww_UP����UWu�UUUu�P𸸸��UW�_UUUW_P�����W�u����P��    W�WwwwwuP�����UUW�UUUW�UP�����UUW�UU_�UUP��� UUW_UUwuUUU��UUUUUu��UUUUUp UUUUUWwuUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU	NumGlyphs   
TStatusBar	StatusBarLeft TopWidth�HeightPanelsWidth�  Width2    	TMainMenuMainMenuLeft@Top�  	TMenuItemArchivoCaption&Archivo 	TMenuItemArchivoAbrirCaption&AbrirHintAbrir Base de DatosOnClickArchivoAbrirClick  	TMenuItemArchivoExportarCaption	&ExportarHintExportar Hacia Paradox  	TMenuItemN2Caption-  	TMenuItemArchivoSalirCaption&SalirHintSalir del ProgramaShortCuts@OnClickArchivoSalirClick   	TMenuItemEditarCaption&Editar 	TMenuItemEditarAgregarCaption&AgregarHintAgregar RegistroShortCut-@OnClickEditarAgregarClick  	TMenuItemEditarBorrarCaption&BorrarHintBorrar el RegistroShortCut.@OnClickEditarBorrarClick  	TMenuItemEditarModificarCaption
&ModificarHintModificar RegistroOnClickEditarModificarClick  	TMenuItemN1Caption-  	TMenuItemEditarBuscarCaption&BuscarHintBuscar RegistroShortCutF@OnClickEditarBuscarClick   	TMenuItemInterbaseDBCaption
&Interbase 	TMenuItemInterbaseImportarTriggersCaptionImportar &TriggersHint!Importar Triggers de BD InterbaseOnClickInterbaseImportarTriggersClick  	TMenuItemInterbaseImportarProceduresCaptionImportar &Stored ProceduresHint*Importar Stored Procedures de BD InterbaseOnClick InterbaseImportarProceduresClick  	TMenuItemN6Caption-  	TMenuItemComparaTriggersIBCaption&Comparar TriggersOnClickComparaTriggersIBClick  	TMenuItemCompararSPIBCaptionCom&parar Stored ProceduresOnClickCompararSPIBClick   	TMenuItem	SQLServerCaption&MS SQL 	TMenuItemSQLServerImportarTriggersCaptionImportar &TriggersOnClickSQLServerImportarTriggersClick  	TMenuItemSQLServerImportarProceduresCaptionImportar &Stored ProceduresOnClick SQLServerImportarProceduresClick  	TMenuItemN5Caption-  	TMenuItemCompararMSSQLCaption&Comparar Triggers y SP'sOnClickCompararMSSQLClick   	TMenuItemParadoxCaptionArchivo &Patch 	TMenuItemParadoxInitCaptionIniciar Archivo NuevoHintAInicializa Un Nuevo Archivo A Partir De SPs y Triggers AnterioresOnClickParadoxInitClick  	TMenuItemN3Caption-  	TMenuItemParadoxExportarCaption&Crear Archivo PatchHint!Exportar Hacia Archivo De ParadoxOnClickParadoxExportarClick  	TMenuItemN4Caption-  	TMenuItemParadoxImportarCaption&Importar Archivo PatchHint!Importar Desde Archivo De ParadoxOnClickParadoxImportarClick    TDataSourcedsSPDataSetdmDBStruct.tqSPLeftTop6  TOpenDialog
OpenDialog
DefaultExtdbFilter'Paradox ( *.db )|*.db|Todos ( *.* )|*.*FilterIndex Left%Topq   