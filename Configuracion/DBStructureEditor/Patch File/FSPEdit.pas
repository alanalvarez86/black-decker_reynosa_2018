unit FSPEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     Db, DBCtrls, DBTables,
     ZBaseDlgModal,
     ZetaNumero,
     ZetaKeyCombo;

type
  TSPEdit = class(TZetaDlgModal)
    PanelControles: TPanel;
    SE_NOMBRElbl: TLabel;
    SE_TIPOlbl: TLabel;
    SE_NOMBRE: TDBEdit;
    SE_TIPO: TZetaDBKeyCombo;
    SE_DESCRIPlbl: TLabel;
    SE_DESCRIP: TDBEdit;
    CodigoGB: TGroupBox;
    PanelIB: TPanel;
    IBLoadFromFile: TSpeedButton;
    SE_DATA: TDBMemo;
    SE_SEQ_NUM: TZetaDBNumero;
    SE_SEQ_NUMlbl: TLabel;
    DataSource: TDataSource;
    OpenDialog: TOpenDialog;
    SE_ARCHIVOlbl: TLabel;
    SE_ARCHIVO: TDBEdit;
    SE_ARCHIVOSeek: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure IBLoadFromFileClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure SE_ARCHIVOSeekClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Editar(const lInsert: Boolean);
  end;

var
  SPEdit: TSPEdit;

implementation

uses FHelpContext,
     ZetaDialogo,
     ZetaMigrar,
     ZetaCommonLists,
     DStructure;

{$R *.DFM}

procedure TSPEdit.FormCreate(Sender: TObject);
var
   eCiclo: eTipoCompany;
begin
     inherited;
     with SE_TIPO.Lista do
     begin
          Add( '0=Comparte' );
          for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
          begin
               Add( Format( '%d=%s', [ Ord( eCiclo ) + 1, ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ] ) );
          end;
     end;
     HelpContext := 0;
end;

procedure TSPEdit.FormShow(Sender: TObject);
begin
     inherited;
     Datasource.Dataset := dmDBStruct.tqScript;
     ActiveControl := SE_NOMBRE;
end;

procedure TSPEdit.Editar( const lInsert: Boolean );
begin
     with dmDBStruct do
     begin
          MSSQLScriptEditar( lInsert );
     end;
end;

procedure TSPEdit.SE_ARCHIVOSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          if Execute then
             SE_ARCHIVO.Text := FileName;
     end;
end;

procedure TSPEdit.IBLoadFromFileClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := SE_ARCHIVO.Text;
          InitialDir := ExtractFilePath( SE_ARCHIVO.Text );
          if Execute then
          begin
               SE_ARCHIVO.Text := FileName;
               SE_DATA.Lines.LoadFromFile( FileName );
          end;
     end;
end;

procedure TSPEdit.OKClick(Sender: TObject);
begin
     inherited;
     with dmDBStruct do
     begin
          try
             if MSSQLScriptEscribirCambios then
                ModalResult := mrOk;
          except
                on Error: EDBEngineError do
                begin
                     { Si el registro ya existe, hay que seguir transfiriendo }
                     with Error do
                     begin
                          if ( ErrorCount > 0 ) and ( Errors[ 0 ].ErrorCode = DBIERR_KEYVIOL ) then
                          begin
                               ZetaDialogo.zError( '� Registro Ya Existe !', 'Escoja Otro # de Secuencia Para Este Stored Procedure', 0 );
                               SE_SEQ_NUM.SetFocus;
                          end
                          else
                              Application.HandleException( Error );
                     end;
                end;
                on Error: Exception do
                begin
                     Application.HandleException( Error );
                end;
          end;
     end;
end;

end.
