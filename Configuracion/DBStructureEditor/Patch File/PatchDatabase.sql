CREATE DATABASE "D:\3Win_13\Datos\Patch File\PatchDatabase.gdb" USER "SYSDBA" PASSWORD "m" PAGE_SIZE = 4096;

CREATE DOMAIN Nombre CHAR(31) DEFAULT "" NOT NULL;

CREATE DOMAIN Descripcion VARCHAR(50) DEFAULT "" NOT NULL;

CREATE DOMAIN Archivo VARCHAR(255) DEFAULT "" NOT NULL;

CREATE DOMAIN Memo BLOB;

CREATE DOMAIN Version CHAR(8) DEFAULT "" NOT NULL;

CREATE DOMAIN Tipo SmallInt DEFAULT 0 NOT NULL;

CREATE DOMAIN Secuencia SmallInt DEFAULT 0 NOT NULL;

CREATE TABLE COMPARTE_IB(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE DATOS_IB(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE SELECCION_IB(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE PORTAL_IB(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE COMPARTE_MSSQL(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE DATOS_MSSQL(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE SELECCION_MSSQL(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE PORTAL_MSSQL(
        SP_NAME     NOMBRE NOT NULL,
        SP_INTRBASE MEMO,
        SP_ORACLE   MEMO,
        SP_VERSION  VERSION NOT NULL,
        SP_SEQ_NUM  SECUENCIA NOT NULL,
        SP_TIPO     TIPO NOT NULL,
PRIMARY KEY (SP_VERSION, SP_TIPO, SP_NAME));

CREATE TABLE METADATA_MSSQL(
       SE_TIPO    TIPO,
       SE_SEQ_NUM SECUENCIA,
       SE_NOMBRE  NOMBRE,
       SE_DESCRIP DESCRIPCION,
       SE_ARCHIVO ARCHIVO,
       SE_DATA    MEMO,
PRIMARY KEY (SE_TIPO,SE_SEQ_NUM));

commit;

set term !!;

create procedure IMPORTA_COMPARTE_IB(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from COMPARTE_IB
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into COMPARTE_IB(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_INTRBASE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update COMPARTE_IB set SP_SEQ_NUM = :SP_SEQ_NUM,
                                 SP_INTRBASE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_DATOS_IB(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from DATOS_IB
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into DATOS_IB(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_INTRBASE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update DATOS_IB set SP_SEQ_NUM = :SP_SEQ_NUM,
                                 SP_INTRBASE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_SELECCION_IB(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from SELECCION_IB
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into SELECCION_IB(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_INTRBASE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update SELECCION_IB set SP_SEQ_NUM = :SP_SEQ_NUM,
                                  SP_INTRBASE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_PORTAL_IB(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from PORTAL_IB
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into PORTAL_IB(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_INTRBASE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update PORTAL_IB set SP_SEQ_NUM = :SP_SEQ_NUM,
                               SP_INTRBASE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_COMPARTE_MSSQL(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from COMPARTE_MSSQL
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into COMPARTE_MSSQL(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_ORACLE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update COMPARTE_MSSQL set SP_SEQ_NUM = :SP_SEQ_NUM,
                                 SP_ORACLE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_DATOS_MSSQL(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from DATOS_MSSQL
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into DATOS_MSSQL(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_ORACLE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update DATOS_MSSQL set SP_SEQ_NUM = :SP_SEQ_NUM,
                                 SP_ORACLE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_SELECCION_MSSQL(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from SELECCION_MSSQL
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into SELECCION_MSSQL(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_ORACLE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update SELECCION_MSSQL set SP_SEQ_NUM = :SP_SEQ_NUM,
                                  SP_ORACLE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

create procedure IMPORTA_PORTAL_MSSQL(
SP_VERSION CHAR(8),
SP_NAME CHAR(31),
SP_SEQ_NUM SMALLINT,
SP_BLOB BLOB,
SP_TIPO SMALLINT )
as
  declare variable iExiste Integer;
begin
     select COUNT(*) from PORTAL_MSSQL
     where ( SP_VERSION = :SP_VERSION ) and
           ( SP_NAME = :SP_NAME ) and
           ( SP_TIPO = :SP_TIPO )
     into iExiste;
     if ( iExiste = 0 ) then
     begin
          insert into PORTAL_MSSQL(
          SP_VERSION, SP_NAME, SP_SEQ_NUM, SP_TIPO, SP_ORACLE ) values (
          :SP_VERSION, :SP_NAME, :SP_SEQ_NUM, :SP_TIPO, :SP_BLOB );
     end
     else
     begin
          update PORTAL_MSSQL set SP_SEQ_NUM = :SP_SEQ_NUM,
                               SP_ORACLE = :SP_BLOB
          where ( SP_VERSION = :SP_VERSION ) and
                ( SP_NAME = :SP_NAME ) and
                ( SP_TIPO = :SP_TIPO );
     end
end !!

set term ; !!

commit;