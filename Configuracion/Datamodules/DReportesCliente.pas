unit DReportesCliente;

interface
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs,ComObj, ActiveX, Db, DBClient, IniFiles,
     {$ifndef VER130}
     Variants,
     {$endif}
{$ifdef DOS_CAPAS}
{$else}
     {$ifdef RDDTRESSCFG}
     ReporteadorDD_TLB,
     {$else}
     Reportes_TLB,
     {$endif}
{$endif}
     ZetaClientDataSet,
     ZetaTipoEntidad,
     ZetaTipoEntidadTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZReportConst,
     FDatosImportReporte;

const
     K_RENGLON_NUEVO = -2;
      K_TEMPLATE_EXT = '.QR2';
      K_TEMPLATE_LAND = 'DEFAULT LANDSCAPE';
      K_TEMPLATE = 'DEFAULT';
      K_TEMPLATE_HTM = 'DEFAULT.HTM';
      K_IMP_DEFAULT  = 'IMPRESORA DEFAULT:';
      K_DOC_DEFAULT = 'CONTRATO.DOC';
      K_BLOBFIELD = 'IM_BLOB';
            K_NO_REQUIERE = 'NO REQUIERE';
      K_GENERAL = '[GENERALES]';
      K_REPORTE = '[REPORTE]';


{$ifdef RDDTRESSCFG}
type
    {$ifdef DOS_CAPAS}
    {$else}
    IdmServerReportesDisp = IdmServerReporteadorDDDisp;
    {$endif}
{$endif}


type
  TdmReportesCliente = class(TDataModule)
    cdsCampoRepFiltros: TZetaClientDataSet;
    cdsEditReporte: TZetaClientDataSet;
    cdsReportes: TZetaClientDataSet;
    cdsLookupReportes: TZetaLookupDataSet;
    cdsEscogeReporte: TZetaClientDataSet;
    cdsResultados: TZetaClientDataSet;
    cdsCampoRep: TZetaClientDataSet;
    cdsImprimeForma: TZetaClientDataSet;
    cdsSuscripReportes: TZetaClientDataSet;
    cdsPlantilla: TZetaClientDataSet;
    cdsBlobs: TZetaClientDataSet;
    cdsEditReporteTemporal: TZetaClientDataSet;
    procedure cdsReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsReportesAlModificar(Sender: TObject);
    procedure cdsReportesAlCrearCampos(Sender: TObject);
    {$ifdef ver130}
    procedure cdsReportesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure cdsReportesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsEditReporteNewRecord(DataSet: TDataSet);
    procedure cdsEditReporteAlAdquirirDatos(Sender: TObject);
    procedure cdsEditReporteAlEnviarDatos(Sender: TObject);
    procedure cdsEditReporteAfterCancel(DataSet: TDataSet);
    procedure cdsEditReporteAlCrearCampos(Sender: TObject);
    procedure cdsCampoRepFiltrosNewRecord(DataSet: TDataSet);
    procedure cdsCampoRepFiltrosAfterDelete(DataSet: TDataSet);
    procedure cdsEditReporteBeforePost(DataSet: TDataSet);
    procedure cdsReportesAlAgregar(Sender: TObject);
    procedure cdsLookupReportesAlAdquirirDatos(Sender: TObject);
    procedure cdsLookupReportesAlModificar(Sender: TObject);
    procedure cdsEscogeReporteAlCrearCampos(Sender: TObject);
    procedure cdsEscogeReporteAlAdquirirDatos(Sender: TObject);
    procedure cdsEscogeReporteAlModificar(Sender: TObject);
    procedure cdsCampoRepFiltrosAlAdquirirDatos(Sender: TObject);
    procedure cdsCampoRepAfterPost(DataSet: TDataSet);
    procedure cdsImprimeFormaAlAdquirirDatos(Sender: TObject);
    procedure cdsImprimeFormaAlCrearCampos(Sender: TObject);
    procedure cdsImprimeFormaAlModificar(Sender: TObject);
    procedure cdsLookupReportesAlAgregar(Sender: TObject);
    procedure cdsLookupReportesLookupSearch(Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String; var sKey, sDescription: String);
    procedure cdsCampoRepAlCrearCampos(Sender: TObject);
    procedure cdsLookupReportesGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
  private
    { Private declarations }
    FClasifActivo: eClasifiReporte;
    FTipoReporte: eTipoReporte;
    FEntidadReporte: TipoEntidad;
    {$ifdef ADUANAS}
    FListaReportes: ArregloReportes;
    FListaEntidades: ArregloEntidades;
    {$endif}
    FReadOnly : Boolean;
    FCodigoReporte: integer;
    //FReporte : integer;
    lHuboCambios : Boolean;
    FPolizaCuenta: string;
    FPolizaFormula: string;
    FSoloImpresion: Boolean;
    FParamList: TZetaParams;
    FClasificaciones: TStrings;
    FImpresoraDefault: string;
    FPuedeSuscribirse: Boolean;
    FPuedeSuscribirUsuarios: Boolean;
    FImprimeSubsecuentes : Boolean;
    FForzarImpresion: Boolean;

    //FModoSuper: Boolean;
    function ReporteValido( const eEntidad: TipoEntidad ): Boolean;
    function EsPoliza: Boolean;
    function GetClasificaciones: string;
    procedure GrabaReporte;
    procedure PosicionaConsulta;
    procedure CR_OPERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
    procedure ListaFijaEntidad( DataSet : TZetaClientDataSet);
  protected
    { Protected declarations }
    FReporteActivo : integer;
{$ifdef DOS_CAPAS}
{$else}
    FServidor: IdmServerReportesDisp;
    function GetServerReportes: IdmServerReportesDisp ;
    property ServerReportes: IdmServerReportesDisp read GetServerReportes;
{$endif}
  public
    { Public declarations }
    function EstaEnClasifi( iReporte:Integer; sCampo: String ): boolean;

    property PuedeSuscribirse: Boolean read FPuedeSuscribirse;
    property PuedeSuscribirUsuarios: Boolean read FPuedeSuscribirUsuarios;
    property ClasifActivo: eClasifiReporte read FClasifActivo write FClasifActivo;
    property EntidadReporte: TipoEntidad read FEntidadReporte write FEntidadReporte;
    property TipoReporte: eTipoReporte read FTipoReporte write FTipoReporte;
    {$ifdef ADUANAS}
    property ListaReporte: ArregloReportes read FListaReportes write FListaReportes;
    property ListaEntidad: ArregloEntidades read FListaEntidades write FListaEntidades;
    {$endif}
    property CodigoReporte: integer read FCodigoReporte write FCodigoReporte;
    property SoloImpresion: Boolean read FSoloImpresion write FSoloImpresion default FALSE;
    property SoloLectura: Boolean read FReadOnly write FReadOnly default FALSE;
    property ParamList: TZetaParams read FParamList;
    property Clasificaciones: TStrings read FClasificaciones write FClasificaciones;
    property ImpresoraDefault: string read FImpresoraDefault write FImpresoraDefault;
    property ImprimeSubsecuentes: Boolean read FImprimeSubsecuentes write FImprimeSubsecuentes;
    property ForzarImpresion: Boolean read FForzarImpresion write FForzarImpresion;


    function ImportaReporteBatch( const sArchivo, sNombreReporte : string; lImportarPlantilla,  lSustituirPlantilla, lContinuarNoExistePlantilla: boolean; var sError, sErrores, sMensajes : string ) : Boolean;
    function GrabaPoliza : Boolean;
    procedure ModificaCampoReporte( const sCampo: String; const iUsuario: Integer );
    procedure BorraReporte;

    function PuedeAbrirCandado: Boolean;
    function AgregaFavoritos( oDataSet : TDataSet ): boolean;
    function AgregaSuscripciones( oDataSet : TDataSet ): boolean;
    function ChecaFavoritos: boolean;
    function ChecaSuscripciones: boolean;
    function GetPlantilla( const Plantilla: string ): TBlobField;
    function GetLogo( const Logo: string ): TBlobField;
    procedure BuscaConcepto;virtual;
    function DirectorioPlantillas: string;virtual;
    function ObtieneEntidad( const Index: TipoEntidad ): String;
    function ObtieneClasificacion( const Index: eClasifiReporte ): String;
    function ExisteClasificacion( const Index: eClasifiReporte ): Boolean;
    function ExisteEntidad( const Index: TipoEntidad ): Boolean;
  end;

var
  dmReportesCliente: TdmReportesCliente;


implementation

uses ZBaseDlgModal,
     ZReconcile,
     ZAccesosMgr,
     ZAccesosTress,
     ZWizardBasico,
     ZetaCommonTools,
     ZetaSystemWorking,
     ZetaDialogo,
     ZetaAsciiFile,
     DSistema,
     DCliente,
     DDiccionario,
     ZetaRegistryCliente;

{$R *.DFM}

{ ********** TdmReportes ********* }

{$ifdef DOS_CAPAS}
{$else}
function TdmReportesCliente.GetServerReportes: IdmServerReportesDisp;
begin
     Result := IdmServerReportesDisp(dmCliente.CreaServidor( {$ifdef RDDTRESSCFG}CLASS_dmServerReporteadorDD{$ELSE}CLASS_dmServerReportes{$ENDIF}, FServidor ));
end;
{$endif}

procedure TdmReportesCliente.cdsReportesAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet(Sender) do
     begin
          IndexFieldNames:='';
          IndexName:='';
          Data := ServerReportes.GetReportes( dmCliente.Empresa,
                                              Ord( FClasifActivo ),
                                              Ord( crFavoritos ),
                                              Ord( crSuscripciones ),
                                              GetClasificaciones,
                                              dmDiccionario.VerConfidencial );
          IndexFieldNames:='RE_CODIGO';
     end;
end;

procedure TdmReportesCliente.cdsReportesAlAgregar(Sender: TObject);
begin
     cdsEditReporte.Conectar;
     cdsEditReporte.Append;
     FReporteActivo := 0;

     if ( lHuboCambios ) then
        PosicionaConsulta;

end;


function TdmReportesCliente.ReporteValido( const eEntidad: TipoEntidad ): Boolean;
begin
     Result := True;
end;

procedure TdmReportesCliente.cdsReportesAlModificar(Sender: TObject);
begin
     with cdsReportes do
     begin
          if ReporteValido( TipoEntidad( FieldByName( 'RE_ENTIDAD' ).AsInteger ) ) then
          begin
               if ( cdsEditReporte.State = dsBrowse ) then
                  cdsEditReporte.Active := FALSE;
               FReporteActivo := FieldByName( 'RE_CODIGO' ).AsInteger;

               if ( lHuboCambios ) then
                  PosicionaConsulta;
          end;
     end;
end;

procedure TdmReportesCliente.ObtieneEntidadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin

     if DisplayText then
     begin
          if Sender.DataSet.IsEmpty then
             Text := ''
          else
              {$ifdef RDDTRESSCFG}
              with Sender.DataSet do
                   if ( FindField('RE_TABLA') <> NIL ) then
                   begin
                        if StrLleno(FieldByName('RE_TABLA').AsString)then
                           Text := FieldByName('RE_TABLA').AsString
                        else
                            Text := Format( '< Tabla #%d >', [FieldByName('RE_ENTIDAD').AsInteger] );
                   end
                   else Text := Format( '< Tabla #%d >', [FieldByName('RE_ENTIDAD').AsInteger] );
              {$else}
              Text := ObtieneEntidad( TipoEntidad( Sender.AsInteger ) );
              {$endif}

     end
     else
         Text:= Sender.AsString;
end;

procedure TdmReportesCliente.ListaFijaEntidad( DataSet : TZetaClientDataSet);
var
   oCampo: TField;
begin
     oCampo := DataSet.FindField( 'RE_ENTIDAD' );
     if ( oCampo <> nil )  then
     begin
          with oCampo do
          begin
               OnGetText := ObtieneEntidadGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;


procedure TdmReportesCliente.cdsReportesAlCrearCampos(Sender: TObject);
begin
     dmSistema.cdsUsuarios.Conectar;
     with TZetaClientDataSet(Sender) do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( TZetaClientDataSet(Sender) );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

{$ifdef ver130}
procedure TdmReportesCliente.cdsReportesReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$else}
procedure TdmReportesCliente.cdsReportesReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
{$endif}
begin
     if ( E.ErrorCode = 1 ) and (Pos('DUPLICATE',UpperCase(E.Message))>0) then
        E.Message := 'El Nombre del Reporte est� Repetido';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;

{ Edicion de Reportes }

procedure TdmReportesCliente.cdsEditReporteNewRecord(DataSet: TDataSet);
var
     aClasificacion: array [FALSE..TRUE] of Integer;
begin
     aClasificacion[FALSE]:= Ord( ClasifActivo );
     aClasificacion[TRUE]:= 0;
     with cdsEditReporte do
     begin
          Randomize;
          FieldByName( 'RE_TIPO' ).AsInteger := 0;
          // Cuando es una copia y esta en suscripciones o favoritos toma los valores de la copia.
          FieldByName( 'RE_CLASIFI' ).AsInteger := aClasificacion[ ChecaFavoritos or ChecaSuscripciones ];
           //  FieldByName( 'RE_CLASIFI' ).AsInteger := Ord( ClasifActivo );
          FieldByName( 'RE_NOMBRE' ).AsString := 'Reporte Nuevo ' + IntToStr( Random( 9999 ) );
          FieldByName( 'RE_TITULO' ).AsString := 'T�tulo ' + FieldByName( 'RE_NOMBRE' ).AsString;
          FieldByName( 'RE_VERTICA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'RE_SOLOT' ).AsString := K_GLOBAL_NO;
          FieldByName( 'RE_PFILE' ).AsInteger := Ord( tfImpresora );
          FieldByName( 'RE_REPORTE' ).AsString := K_TEMPLATE;
          FieldByName( 'RE_COPIAS' ).AsInteger := 1;
          FieldByName( 'RE_PRINTER' ).AsString := K_IMP_DEFAULT;
          FieldByName( 'RE_GENERAL' ).AsString := '';
          FieldByName( 'RE_COLNUM' ).AsInteger := 0;
          FieldByName( 'RE_MAR_SUP' ).AsInteger := 0;
          FieldByName( 'RE_ANCHO' ).AsInteger := 66;
          FieldByName( 'RE_RENESPA' ).AsInteger := 66;
          FieldByName( 'RE_COLESPA' ).AsInteger := 0;
          FieldByName( 'RE_ALTO' ).AsInteger := 0;
          FieldByName( 'RE_HOJA' ).AsInteger := 0;
          FieldByName( 'RE_CANDADO' ).AsInteger := Ord( cnAbierto );
          FieldByName('RE_FONTSIZ').AsInteger := 0;
     end;
end;

procedure TdmReportesCliente.PosicionaConsulta;
begin
     with cdsReportes do
     begin
          Refrescar;
          Locate( 'RE_CODIGO', cdsEditReporte.FieldByname( 'RE_CODIGO' ).AsInteger, [] );
     end;
end;

procedure TdmReportesCliente.cdsEditReporteAlAdquirirDatos(Sender: TObject);
var
   CampoRep: OleVariant;
begin
     FPolizaFormula := '';
     FPolizaCuenta := '';
     lHuboCambios := FALSE;
     cdsEditReporte.Data := ServerReportes.GetEditReportes( dmCliente.Empresa, FReporteActivo, CampoRep );
     cdsCampoRep.Data := CampoRep; { Se esta simulando el Master-Detail }
     {$IFNDEF SUPERVISORES}
     dmSistema.GetReporteSuscrip( FReporteActivo );
     {$ENDIF}
     {$ifdef TRESS}
     FPuedeSuscribirse:= ZAccesosMgr.CheckDerecho(D_SUSCRIPCION_REPORTE,K_DERECHO_CONSULTA);
     FPuedeSuscribirUsuarios:= ZAccesosMgr.CheckDerecho(D_SUSCRIPCION_USUARIOS,K_DERECHO_CONSULTA);
     {$else}
     FPuedeSuscribirse:= dmDiccionario.VerConfidencial;
     FPuedeSuscribirUsuarios:= dmDiccionario.VerConfidencial;
     {$endif}
end;

procedure TdmReportesCliente.BorraReporte;
begin
     with cdsReportes do
     begin
          if ( ChecaFavoritos ) then
             ServerReportes.BorraFavoritos( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger )
          else if ( ChecaSuscripciones) then
             {$IFNDEF SUPERVISORES}
             dmSistema.BorraSuscripciones( FieldByName('RE_CODIGO').AsInteger )
             {$ENDIF}
          else
              ServerReportes.BorraReporte( dmCliente.Empresa, FieldByName( 'RE_CODIGO' ).AsInteger );
          Delete;
     end;
     cdsLookUpReportes.Close;
end;

function TdmReportesCliente.AgregaFavoritos( oDataSet : TDataSet ): boolean;
begin
     Result := False;
end;

function TdmReportesCliente.AgregaSuscripciones( oDataSet : TDataSet ): boolean;
begin
     Result := False;
end;

procedure TdmReportesCliente.ModificaCampoReporte( const sCampo: String; const iUsuario: Integer );
begin
     with cdsReportes do
     begin
          Edit;
          FieldByName(sCampo).AsInteger := iUsuario;
          Post;
          MergeChangeLog;
     end;
end;

function TdmReportesCliente.ChecaSuscripciones: boolean;
begin
     Result := ( ClasifActivo = crSuscripciones );
end;

function TdmReportesCliente.ChecaFavoritos: boolean;
begin
     Result := ( ClasifActivo = crFavoritos );
end;

procedure TdmReportesCliente.cdsEditReporteAlEnviarDatos(Sender: TObject);
begin
     if EsPoliza then
        GrabaPoliza
     else
         GrabaReporte;
end;

procedure TdmReportesCliente.GrabaReporte;
var
   ErrorCount, iReporte: Integer;
begin
     ErrorCount := 0;
     with cdsEditReporte do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if (ChangeCount > 0) OR
             (cdsCampoRep.ChangeCount>0)then
          begin
               lHuboCambios := TRUE;
               iReporte := FieldByName('RE_CODIGO').AsInteger ;
               Reconcile( ServerReportes.GrabaReporte( dmCliente.Empresa,
                                                       Delta,
                                                       cdsCampoRep.Delta,
                                                       ErrorCount,
                                                       iReporte) );
               if ErrorCount = 1 then Edit;
               if ( ErrorCount = 0 ) then
               begin
                    if ( FieldByName( 'RE_CODIGO' ).AsInteger <> iReporte ) then
                    begin
                         Edit;
                         FieldByName( 'RE_CODIGO' ).AsInteger := iReporte;
                         Post;
                         MergeChangeLog;

                         while NOT dmSistema.cdsSuscrip.EOF do
                         begin
                              dmSistema.cdsSuscrip.Edit;
                              dmSistema.cdsSuscrip.FieldByName( 'RE_CODIGO' ).AsInteger := iReporte;
                              dmSistema.cdsSuscrip.Post;
                              dmSistema.cdsSuscrip.Next;
                         end;
                    end;
                    dmSistema.GrabaSuscrip;
               end;

               {$ifdef TRESSCFG}
               if ( ErrorCount = 1 ) then
               begin
                    if ( ChangeCount > 0 ) then
                         Cancelupdates;
               end;
               {$endif}



          end;
     end;
end;

function TdmReportesCliente.GrabaPoliza : Boolean;
var
   iReporte, ErrorCount : Integer;
   oCampoRep : OleVariant;
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        Result := TRUE;
        ErrorCount := 0;
        with cdsEditReporte do
        begin
             if State in [ dsEdit, dsInsert ] then
                Post;
             if (ChangeCount > 0) OR
                (cdsCampoRep.ChangeCount>0)then
             begin
                lHuboCambios := TRUE;
                if (cdsCampoRep.ChangeCount>0) then
                   oCampoRep := cdsCampoRep.Delta
                else oCampoRep := NULL;
                iReporte := FieldByName('RE_CODIGO').AsInteger;
                Reconcile ( ServerReportes.GrabaPoliza( dmCliente.Empresa,
                                                        Delta, oCampoRep,
                                                        ErrorCount, iReporte ) );
                if (ErrorCount = 0) then
                begin
                     if (FieldByName('RE_CODIGO').AsInteger <> iReporte) then
                     begin
                          Edit;
                          FieldByName('RE_CODIGO').AsInteger := iReporte;
                          Post;
                          MergeChangeLog;
                          cdsLookupReportes.Close;
                     end;
                     cdsCamporep.MergeChangeLog;
                end
                else Result := FALSE;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmReportesCliente.cdsEditReporteAfterCancel(DataSet: TDataSet);
begin
     cdsCampoRep.CancelUpdates;
end;

procedure TdmReportesCliente.cdsEditReporteAlCrearCampos(Sender: TObject);
begin

     with cdsEditReporte do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( cdsEditReporte );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

function TdmReportesCliente.EsPoliza : Boolean;
begin
     Result := eTipoReporte(cdsEditReporte.FieldByName('RE_TIPO').AsInteger) in [trPoliza,trPolizaConcepto];
end;

procedure TdmReportesCliente.cdsCampoRepFiltrosNewRecord(DataSet: TDataSet);
begin
     with cdsCampoRep do
     begin
          if EsPoliza then
          begin
               with FieldByName( 'CR_OPER' ) do
               begin
                    if NOT Assigned(OnGetText) then
                    begin
                         OnGetText := CR_OPERGetText;
                         Alignment := taLeftJustify;
                    end;
               end;
               FieldByName('CR_TITULO').AsString := FPolizaCuenta;
               FieldByName('CR_FORMULA').AsString := FPolizaFormula;
          end
          else FieldByName( 'CR_OPER' ).OnGetText := NIL;

          FieldByName('RE_CODIGO').AsInteger := cdsEditReporte.FieldByName('RE_CODIGO').AsInteger;
          FieldByName('CR_TIPO').AsInteger := 0;
          FieldByName('CR_POSICIO').AsInteger := -1;
          FieldByName('CR_SUBPOS').AsInteger := -1;
          FieldByName('CR_REQUIER').AsString := '';
          FieldByName('CR_CALC').AsInteger   := 0;
          FieldByName('CR_MASCARA').AsString := '';
          FieldByName('CR_ANCHO').AsInteger   := 0;
          FieldByName('CR_OPER').AsInteger    := 0;
          FieldByName('CR_TFIELD').AsInteger  := 0;
          FieldByName('CR_SHOW').AsInteger    := 0;
          FieldByName('CR_DESCRIP').AsString := '';
          FieldByName('CR_BOLD').AsString    := '';
          FieldByName('CR_ITALIC').AsString  := '';
          FieldByName('CR_SUBRAYA').AsString := '';
          FieldByName('CR_STRIKE').AsString  := '';
          FieldByName('CR_ALINEA').AsInteger  := 0;
          FieldByName('CR_COLOR').AsInteger   := 0;
     end;
end;

procedure TdmReportesCliente.cdsCampoRepFiltrosAfterDelete(DataSet: TDataSet);
begin
     with cdsEditReporte do
          if State = dsBrowse then Edit;
end;

procedure TdmReportesCliente.cdsEditReporteBeforePost(DataSet: TDataSet);
begin
     with cdsEditReporte do
     begin
          FieldByName('RE_FECHA').AsDateTime := Now;
          FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
          with FieldByName('RE_COPIAS') do
               AsInteger := iMax(AsInteger,1);
     end;
end;


procedure TdmReportesCliente.cdsLookupReportesAlAdquirirDatos(Sender: TObject);
begin
     cdsLookUpReportes.Data := ServerReportes.GetLookUpReportes(dmCliente.Empresa,dmDiccionario.VerConfidencial);
end;

procedure TdmReportesCliente.cdsLookupReportesAlModificar(Sender: TObject);
begin
     if ( cdsEditReporte.State = dsBrowse ) then
        cdsEditReporte.Active := FALSE;
     FReporteActivo := cdsLookupReportes.FieldByName('RE_CODIGO').AsInteger;
end;

procedure TdmReportesCliente.cdsEscogeReporteAlCrearCampos(Sender: TObject);
begin
     with cdsEscogeReporte do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( cdsEscogeReporte );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmReportesCliente.cdsLookupReportesGetRights( Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     {GA:  Este LookupDataset no debe tener ningun derecho de Consulta, Alta, Baja o Cambio };
     lHasRights := False;
end;

procedure TdmReportesCliente.cdsEscogeReporteAlAdquirirDatos(Sender: TObject);
 {$ifdef ADUANAS}
 var
    eReporte: eTipoReporte;
    eEntidad: TipoEntidad;
    sTipos, sEntidades: string;
 {$ENDIF}
begin
     {$ifdef ADUANAS}
     if ( FListaReportes = [] ) AND ( FListaEntidades = [] ) then
        cdsEscogeReporte.Data := ServerReportes.GetEscogeReporte(dmCliente.Empresa,Ord(FEntidadReporte),Ord(FTipoReporte),dmDiccionario.VerConfidencial)
     else
     begin
          for eReporte := Low(eTipoReporte) to High(eTipoReporte) do
              if ( eReporte in FListaReportes) then
                 sTipos := ConcatString( sTipos, IntToStr(Ord(eReporte)) , ',' );

          for eEntidad := Low(TipoEntidad) to High(TipoEntidad) do
              if ( eEntidad in FListaEntidades) then
                 sEntidades := ConcatString( sEntidades, IntToStr(Ord(eEntidad)) , ',' );

          cdsEscogeReporte.Data := ServerReportes.GetEscogeReportes(dmCliente.Empresa,sEntidades,sTipos,dmDiccionario.VerConfidencial);
     end;
     {$else}
     cdsEscogeReporte.Data := ServerReportes.GetEscogeReporte(dmCliente.Empresa,Ord(FEntidadReporte),Ord(FTipoReporte),dmDiccionario.VerConfidencial)
     {$endif}
end;


procedure TdmReportesCliente.cdsEscogeReporteAlModificar(Sender: TObject);
begin
     if ( cdsEditReporte.State = dsBrowse ) then
        cdsEditReporte.Active := FALSE;
     SoloLectura := TRUE;
     FReporteActivo := cdsEscogeReporte.FieldByName('RE_CODIGO').AsInteger;
     SoloLectura := FALSE;
end;


procedure TdmReportesCliente.cdsCampoRepFiltrosAlAdquirirDatos(Sender: TObject);
begin
     cdsCampoRepFiltros.Data := ServerReportes.CampoRepFiltros( dmCliente.Empresa, CodigoReporte );
end;

procedure TdmReportesCliente.CR_OPERGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if Sender.AsInteger = 1 then
        Text:= 'Abono'
     else
        Text:= 'Cargo'
end;

procedure TdmReportesCliente.cdsCampoRepAfterPost(DataSet: TDataSet);
begin
     if EsPoliza AND
        ( eTipoCampo( cdsCampoRep.FieldByName('CR_TIPO').AsInteger ) = tcCampos ) then
     begin
          with cdsCampoRep do
          begin
               FPolizaFormula := FieldByName( 'CR_FORMULA' ).AsString;
               FPolizaCuenta := FieldByName( 'CR_TITULO' ).AsString;
          end;
     end;
end;


function TdmReportesCliente.ImportaReporteBatch( const sArchivo, sNombreReporte : string; lImportarPlantilla,  lSustituirPlantilla, lContinuarNoExistePlantilla: boolean; var sError, sErrores, sMensajes : string ) : Boolean;
 const K_ERROR = 'ERROR';
       K_NO_VALIDO = 'El archivo de importaci�n no es v�lido';
       K_IMPORTA = 'Importaci�n de Reportes';

 var oEncabezado,oImpReporte : TStringList;
     oDatosImport : TDatosImportReporte;
     iCount : integer;

 function LeeSeccion( var i : integer;
                      DataSet : TDataSet ): Boolean;
  var sRenglon,sField,sFormat,sValue : string;
      IgualPos : integer;

  function EsCorchete : Boolean;
  begin
       Result := Copy(oImpReporte[i],1,1)='[' ;
  end;
  function QuitaComillas : string;
  begin
       Result := sValue;
       if Copy(Result,1,1) = UnaCOMILLA then
       begin
            Result := Copy(Result,2,Length(Result));
            if Copy(Result,Length(Result),1) = UnaCOMILLA then
               Result := Copy(Result,1,Length(Result)-1);
       end;
  end;
  var oField : TField;
 begin
      Result := TRUE;
      if EsCorchete then
         i := i+1;

      while (i<iCount) AND (NOT EsCorchete) do
      begin
           with DataSet do
           begin
                sRenglon := oImpReporte[i];
                if StrLleno(sRenglon) then
                begin
                     IgualPos := Pos('=',sRenglon);
                     sField := Copy(sRenglon,1,IgualPos-1);
                     sValue := Copy(sRenglon,IgualPos+1,Length(sRenglon));
                     oField := FindField(sField);
                     if oField <> NIL then
                     begin
                          with oField do
                          begin
                               if DataType in [ftDate, ftDateTime] then
                               try
                                  sFormat := ShortDateFormat;
                                  ShortDateFormat := 'mm/dd/yyyy';
                                  AsString := sValue;
                               finally
                                      ShortDateFormat:=sFormat;
                               end
                               else if oField is TNumericField then
                                    AsString := sValue
                               else AsString := QuitaComillas;
                          end;
                     end
                     else Result := FALSE;
                end;
                i := i+1;
           end;
      end;
 end;


function GetPlantilla(const Value: string) : string;
begin

     if ( Value = '' ) OR (Value=(K_TEMPLATE+'.QR2')) then
     begin
          lImportarPlantilla := False;
          Result := K_TEMPLATE+'.QR2';
     end
     else if Value = K_NO_REQUIERE then
     begin
          lImportarPlantilla := False;
          Result :=  Value;
     end
     else
     begin
          lImportarPlantilla := True;
          Result := Value;
     end;
end;


 function ImportaPlantilla : Boolean;
  var sFuente, sDestino : string;
 begin
      Result := TRUE;
      if lImportarPlantilla then
      begin
           sFuente := VerificaDir(ExtractFilePath(sArchivo));
//           sDestino := dmGlobal..DirPlantilla;

           if sFuente <> sDestino then
           begin
                sFuente := sFuente + oDatosImport.Plantilla;
                sDestino := sDestino + oDatosImport.Plantilla;
                if FileExists(sDestino) then
                begin
                   if lSustituirPlantilla then
                      CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFuente),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE)
                   else
                       Result := FALSE;
                end
                else
                    CopyFile({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sFuente),{$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(sDestino),FALSE);
           end;
      end;
 end;

  var i{ j {, iReporte} : integer;
      sFuente : string;
      lExistePlantilla : Boolean;
begin
     oImpReporte := TStringList.Create;
     oEncabezado := TStringList.Create;
     try
        oImpReporte.LoadFromFile(sArchivo);
        iCount := oImpReporte.Count;
        if Pos('TOTAL=',UpperCase(oImpReporte[iCount-1]))>0 then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        if Pos('[TOTAL_DETALLE]',UpperCase(oImpReporte[iCount-1]))>0  then
           oImpReporte.Delete(iCount-1);
        iCount := oImpReporte.Count;
        oDatosImport := TDatosImportReporte.Create;

        if oImpReporte[0] = K_GENERAL then i :=1
        else i :=0;

        while oImpReporte[i] <> K_REPORTE DO
        begin
             oEncabezado.Add(oImpReporte[i]);
             i := i+1;
        end;
        oEncabezado.Add('RE_CLASIFI='+oImpReporte.Values['RE_CLASIFI']);
        with oEncabezado do
        begin
             sError := Values['NOMBRE'];
             if sError = '' then
             begin
                  Result := FALSE;
                  sError := K_NO_VALIDO;
                  Exit;
             end;
             with oDatosImport do
             begin
                  Empresa:= Values['EMPRESA'];
                  Usuario := Values['USUARIO'];
                  Fecha := Values['FECHA'];
                  Nombre := sError;
                  Tipo := ObtieneElemento(lfTipoReporte,StrToInt(Values['TIPO']));
                  Tabla := ObtieneEntidad(TipoEntidad(StrToInt(Values['TABLA'])));
                  if StrVacio(sNombreReporte) then
                     NombreNuevo := sError
                  else
                     NombreNuevo := sNombreReporte;
                  Plantilla := Values['PLANTILLA'];
                  Clasificacion := eClasifiReporte(StrToInt(Values['RE_CLASIFI']));
                  Result := True
             end;
        end;

        oDatosImport.ImportarPlantilla := lImportarPlantilla;

        if ( oDatosImport.ImportarPlantilla ) AND
           ( oDatosImport.Plantilla <> K_NO_REQUIERE )  AND
           ( oDatosImport.Plantilla <> K_TEMPLATE+'.QR2' ) then
        begin
             sFuente := VerificaDir(ExtractFilePath(sArchivo));
             lExistePlantilla := FileExists( sFuente + oDatosImport.Plantilla);

             // Si no existe plantilla
             if NOT lExistePlantilla then
             begin

                  Result := lContinuarNoExistePlantilla; //zConfirm( K_IMPORTA, 'La Plantilla  ' + Comillas(sFuente + oDatosImport.Plantilla) + ' No Existe,'+CR_LF+  '�Desea Continuar con la Importaci�n? ',0,mbNo );
                  if (  Result ) then
                        sMensajes :=  sMensajes + CR_LF +  Format( 'La Plantilla %s no existe en el directorio fuente. El reporte se importar� sin plantilla',
                                                              [oDatosImport.Plantilla] );
             end;
        end
        else
            lExistePlantilla := FALSE;

        if Result then
        begin
             with cdsEditReporte do
             begin
                  Active := FALSE;
                  Conectar;
                  Append;
                  LeeSeccion(i,cdsEditReporte);
                  if Result then
                  begin
                       if NOT ExisteEntidad( TipoEntidad( FieldByName('RE_ENTIDAD').AsInteger ) ) then
                       begin
                            sErrores :=  ObtieneEntidad(TipoEntidad( FieldByName('RE_ENTIDAD').AsInteger )) + ' El reporte no se puede importar';
                            Abort;
                       end;
                       FieldByName('RE_CODIGO').AsInteger := 0;
                       FieldByName('RE_NOMBRE').AsString := oDatosImport.NombreNuevo;
                       FieldByName('RE_FECHA').AsDateTime := DATE;
                       FieldByName('US_CODIGO').AsInteger := dmCliente.Usuario;
                       if ExisteClasificacion(oDatosImport.Clasificacion) then
                          FieldByName('RE_CLASIFI').AsInteger := Ord(oDatosImport.Clasificacion)
                       else
                       begin
                            FieldByName('RE_CLASIFI').AsInteger := Ord( FClasifActivo );
                            sMensajes := sMensajes + CR_LF +  Format( 'La Clasificaci�n #%d no existe. ' + 'El reporte se importar� en la Clasificaci�n %s',
                                                              [Ord(oDatosImport.Clasificacion),ObtieneClasificacion(FClasifActivo) ] );
                       end;
                       Post;

                       cdsCampoRep.EmptyDataSet;
                       cdsCampoRep.Conectar;
                       while i < iCount do
                       begin
                            cdsCampoRep.Append;
                            if LeeSeccion(i,cdsCampoRep) then
                               cdsCampoRep.Post
                            else cdsCampoRep.Cancel;
                       end;
                       Enviar;
                       PosicionaConsulta;
                       Result := FieldByName('RE_CODIGO').AsInteger <> 0;

                       if Result then
                       begin
                            if lExistePlantilla then
                               ImportaPlantilla
                       end
                       else sError := 'El reporte ya existe!';
                  end;
             end
        end
        else sError := '';
     finally
            FreeAndNil( oEncabezado );
            FreeAndNil( oImpReporte );
     end;
end;


procedure TdmReportesCliente.cdsImprimeFormaAlAdquirirDatos(Sender: TObject);
begin
     cdsImprimeForma.Data := ServerReportes.GetEscogeReporte(dmCliente.Empresa,Ord(FEntidadReporte),Ord(FTipoReporte),dmDiccionario.VerConfidencial);
end;

procedure TdmReportesCliente.cdsImprimeFormaAlCrearCampos(Sender: TObject);
begin
     with cdsImprimeForma do
     begin
          MaskFecha('RE_FECHA');
          ListaFija('RE_TIPO', lfTipoReporte);
          ListaFijaEntidad( cdsImprimeForma );
          CreateSimpleLookUp( dmSistema.cdsUsuariosLookUp, 'US_DESCRIP', 'US_CODIGO' );
     end;
end;

procedure TdmReportesCliente.cdsImprimeFormaAlModificar(Sender: TObject);
begin
     if ( cdsImprimeForma.State = dsBrowse ) then
        cdsImprimeForma.Active := FALSE;
     FReporteActivo := cdsImprimeForma.FieldByName('RE_CODIGO').AsInteger;
end;

procedure TdmReportesCliente.cdsLookupReportesAlAgregar(Sender: TObject);
begin
     ShowMessage('no');
end;

procedure TdmReportesCliente.cdsLookupReportesLookupSearch(
  Sender: TZetaLookupDataSet; var lOk: Boolean; const sFilter: String;
  var sKey, sDescription: String);
begin
     with Sender do
     begin
          if NOT Filtered then
          begin
               Filter := 'RE_TIPO IN( 3,4 )';
               Filtered := TRUE;
          end;
          lOk := NOT IsEmpty;
          Filtered := FALSE;
     end;
end;


procedure TdmReportesCliente.cdsCampoRepAlCrearCampos(Sender: TObject);
begin
     if EsPoliza then
     begin
          //Aplica Solamente para Polizas Contables
          with cdsCampoRep.FieldByName( 'CR_OPER' ) do
          begin
               OnGetText := CR_OPERGetText;
               Alignment := taLeftJustify;
          end;
     end
     else cdsCampoRep.FieldByName( 'CR_OPER' ).OnGetText := NIL;
end;

 

function TdmReportesCliente.PuedeAbrirCandado: Boolean;
begin
     with dmCliente.GetDatosUsuarioActivo do
     begin
          Result := ( Grupo = K_GRUPO_SISTEMA );
          if not Result then
          begin
               with cdsReportes do
               begin
                    case eCandado( FieldByName( 'RE_CANDADO' ).AsInteger ) of
                         cnAbierto: Result := True;
                         cnSoloImprimir: Result := ( Codigo = FieldByName( 'US_CODIGO' ).AsInteger );
                    else
                        Result := ( Codigo = FieldByName( 'US_CODIGO' ).AsInteger );
                    end;
               end;
          end;
     end;
          //( UsuarioActivo, Grupo, UsuarioReporte, CandadoReporte )
end;

function TdmReportesCliente.GetClasificaciones: string;
var
   i: integer;
begin
     Result := VACIO;
     if not dmCliente.ModoTress then
     begin
          with FClasificaciones do
          begin
               for i := 0 to ( Count - 1 ) do
               begin
                    Result := ConcatString( Result,
                                            IntToStr( Ord( eClasifiReporte( Fclasificaciones.objects[ i ] ) ) ),
                                            ',' );
               end;
          end
     end;
end;


function TdmReportesCliente.GetPlantilla( const Plantilla: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Plantilla );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

function TdmReportesCliente.GetLogo( const Logo: string ): TBlobField;
begin
     cdsPlantilla.Data := ServerReportes.GetPlantilla( Logo );
     Result := TBlobField( cdsPlantilla.FieldByName( 'CampoBlob' ) );
end;

procedure TdmReportesCliente.BuscaConcepto;
begin
end;

function TdmReportesCliente.EstaEnClasifi( iReporte: Integer; sCampo: String ): boolean;
begin
     Result:= False;
     if ( cdsEditReporte.FieldByName('RE_CODIGO').AsInteger = iReporte ) then
     begin
          Result:= ( cdsReportes.FieldByName(sCampo).AsInteger = dmCliente.Usuario ) ;

     end;
end;

function TdmReportesCliente.ObtieneEntidad( const Index: TipoEntidad ): String;
begin
     {$ifdef RDDTRESSCFG}
     if ExisteEntidad( Index ) then
        Result := aTipoEntidad[Index]
     else
         Result := Format( 'Tabla #%d no Existe', [Ord(Index)] );
     {$else}
     Result := ZetaTipoEntidadTools.ObtieneEntidad( TipoEntidad( Index ) );
     {$endif}

end;

function TdmReportesCliente.ObtieneClasificacion( const Index: eClasifiReporte ): String;
begin
     {$ifdef RDDTRESSCFG}
     case Index of
          crFavoritos: Result := K_MIS_FAVORITOS_DES;
          crSuscripciones: Result := K_MIS_SUSCRIPCIONES_DES
          else
          begin
               with dmDiccionario.cdsClasificaciones do
               begin
                    if ExisteClasificacion(Index) then
                       Result := StrDef( FieldByName('RC_NOMBRE').AsString, Format( 'Clasificaci�n #%d' , [Ord( Index )] ) )
                    else
                        Result := Format( 'Clasificaci�n #%d no Existe', [Ord(Index)] );
               end;
          end;
     end;

     {$else}
     Result := ObtieneElemento(lfClasifiReporte,Ord(Index) );
     {$endif}
end;

function TdmReportesCliente.ExisteClasificacion( const Index: eClasifiReporte ): Boolean;
begin
     {$ifdef RDDTRESSCFG}
     with dmDiccionario.cdsClasificaciones do
     begin
          Refrescar;
          Result := Locate( 'RC_CODIGO', Ord(Index), [] );
     end;
     {$else}
     Result := TRUE;
     {$endif}
end;

function TdmReportesCliente.ExisteEntidad( const Index: TipoEntidad ): Boolean;
begin
     {$ifdef RDDTRESSCFG}
     dmDiccionario.LlenaArregloEntidades;
     with dmDiccionario.cdsEntidades do
     begin
          Conectar;
          Result := Locate( 'EN_CODIGO', Ord(Index), [] );
     end;
     {$else}
     Result := TRUE;
     {$endif}
end;



function TdmReportesCliente.DirectorioPlantillas: string;
begin
     {$ifdef RDD}
     with dmCliente do
          Result := VerificaDir( ServerReportes.DirectorioPlantillas(Empresa) );
     {$else}
     //Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
     {$endif}

end;





end.



