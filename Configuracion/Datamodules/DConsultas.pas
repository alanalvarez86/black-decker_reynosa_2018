unit DConsultas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,
     ZetaClientDataSet,
    // ZetaClientTools,
     ZetaCommonClasses,
     ZetaCommonLists,
{$ifdef DOS_CAPAS}
     DServerConsultas;
{$else}
     Consultas_TLB;
{$endif}

type
  TdmConsultas = class(TDataModule)
    cdsQueryGeneral: TZetaClientDataSet;
    cdsBitacora: TZetaClientDataSet;
    cdsLogDetail: TZetaClientDataSet;
    cdsProcesos: TZetaClientDataSet;
    procedure cdsQueryGeneralAlAdquirirDatos(Sender: TObject);
    procedure cdsLogDetailAfterOpen(DataSet: TDataSet);
    procedure cdsBitacoraAfterOpen(DataSet: TDataSet);
    procedure UsuarioGetText(Sender: TField; var Text: String; DisplayText: Boolean);
  private
    { Private declarations }
    FSQLText: String;
{$ifdef DOS_CAPAS}
    function GetServerConsultas: TdmServerConsultas;
{$else}
    FServidor: IdmServerConsultasDisp;
    function GetServerConsultas: IdmServerConsultasDisp;
{$endif}
    procedure GetProcessText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ConfigProcIdField(Campo: TField);
  public
    { Public declarations }
{$ifdef DOS_CAPAS}
    property ServerConsultas: TdmServerConsultas read GetServerConsultas;
{$else}
    property ServerConsultas: IdmServerConsultasDisp read GetServerConsultas;
{$endif}
    property SQLText: String read FSQLText write FSQLText;
    procedure GetProcessLog(const iProceso: Integer);
    procedure RefrescarBitacora( Parametros: TZetaParams );
  end;

var
  dmConsultas: TdmConsultas;

implementation

uses ZetaCommonTools,
     DSistema,
     DCliente;


{$R *.DFM}

{ ************** TdmConsultas **************** }

{$ifdef DOS_CAPAS}
function TdmConsultas.GetServerConsultas: TdmServerConsultas;
begin
     Result := DCliente.dmCliente.ServerConsultas;
end;
{$else}
function TdmConsultas.GetServerConsultas: IdmServerConsultasDisp;
begin
     Result := IdmServerConsultasDisp( dmCliente.CreaServidor( CLASS_dmServerConsultas, FServidor ) );
end;
{$endif}

procedure TdmConsultas.UsuarioGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if ( Sender.AsInteger = 0 ) then
        Text := ''
     else
         Text := Sender.AsString + '=' + dmSistema.cdsUsuariosLookUp.GetDescripcion( Sender.AsString );
end;

procedure TdmConsultas.GetProcessText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
  {   if DisplayText then
     begin
          if Sender.Dataset.IsEmpty then
             Text := ''
          else
              Text := ZetaClientTools.GetProcessName( Procesos( Sender.AsInteger ) );
     end
     else}
         Text := Sender.AsString;
end;

procedure TdmConsultas.ConfigProcIdField( Campo: TField );
begin
     with Campo do
     begin
          OnGetText := GetProcessText;
          Alignment := taLeftJustify;
     end;
end;



{ cdsQueryGeneral }

procedure TdmConsultas.cdsQueryGeneralAlAdquirirDatos(Sender: TObject);
begin
     cdsQueryGeneral.Data := ServerConsultas.GetQueryGral( dmCliente.Empresa, FSQLText , True );
end;

{ cdsLogDetail }

procedure TdmConsultas.GetProcessLog( const iProceso: Integer );
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := oCursor;
     try
        cdsLogDetail.Data := ServerConsultas.GetProcessLog( dmCliente.Empresa, iProceso );
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmConsultas.cdsLogDetailAfterOpen(DataSet: TDataSet);
begin
     with cdsLogDetail do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
     end;
end;

{ cdsBitacora }

procedure TdmConsultas.RefrescarBitacora(Parametros: TZetaParams);
begin
     cdsBitacora.Data := ServerConsultas.GetBitacora( dmCliente.Empresa, Parametros.VarValues );
end;

procedure TdmConsultas.cdsBitacoraAfterOpen(DataSet: TDataSet);
begin
     with cdsBitacora do
     begin
          ListaFija( 'BI_TIPO', lfTipoBitacora );
          ListaFija( 'BI_CLASE', lfClaseBitacora );
          MaskFecha( 'BI_FECHA' );
          MaskFecha( 'BI_FEC_MOV' );
          ConfigProcIdField( FieldByName( 'BI_PROC_ID' ) );
     end;
end;


end.
