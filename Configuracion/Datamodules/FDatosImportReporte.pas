unit FDatosImportReporte;

interface

uses
  Windows, Messages, SysUtils, Classes, ZetaCommonLists;

type
  TDatosImportReporte = class(TObject)
  private
    FPlantilla : string;
    FClasificacion : eClasifiReporte;

    FEmpresa : string ;
    FUsuario : string ;
    FFecha : string ;
    FNombre : string ;
    FTipo : string ;
    FTabla : string ;
    FImportarPlantilla, FImportar : Boolean;
    FNombreNuevo : string;



    function GetNombreNuevo: string;
    procedure SetEmpresa(const Value: string);
    procedure SetFecha(const Value: string);
    procedure SetNombre(const Value: string);
    procedure SetNombreNuevo(const Value: string);
    procedure SetTabla(const Value: string);
    procedure SetTipo(const Value: string);
    procedure SetUsuario(const Value: string);
    procedure SetPlantilla(const Value: string);
    procedure SetClasificacion(const Value: eClasifiReporte);
    procedure SetImportarPlantilla(const Value: boolean);
    function GetImportarPlantilla : Boolean;
  public
    property Empresa : string write SetEmpresa;
    property Usuario : string write SetUsuario;
    property Fecha : string write SetFecha;
    property Nombre : string write SetNombre;
    property Tipo : string write SetTipo;
    property Tabla : string write SetTabla;
    property Clasificacion : eClasifiReporte read FClasificacion write SetClasificacion;
    property Plantilla : string read FPlantilla write SetPlantilla;
    property ImportarPlantilla : Boolean read GetImportarPlantilla write SetImportarPlantilla;
    property NombreNuevo : string read GetNombreNuevo write SetNombreNuevo;
   end;
const
     K_RENGLON_NUEVO = -2;
      K_TEMPLATE_EXT = '.QR2';
      K_TEMPLATE_LAND = 'DEFAULT LANDSCAPE';
      K_TEMPLATE = 'DEFAULT';
      K_TEMPLATE_HTM = 'DEFAULT.HTM';
      K_IMP_DEFAULT  = 'IMPRESORA DEFAULT:';
      K_DOC_DEFAULT = 'CONTRATO.DOC';
      K_BLOBFIELD = 'IM_BLOB';
            K_NO_REQUIERE = 'NO REQUIERE';
      K_GENERAL = '[GENERALES]';
      K_REPORTE = '[REPORTE]';

implementation
uses ZetaCommonClasses;


function TDatosImportReporte.GetNombreNuevo: string;
begin
     Result := FNombreNuevo;
end;

procedure TDatosImportReporte.SetNombreNuevo(const Value: string);
begin
     FNombreNuevo := Copy(Value,1,30);
end;

procedure TDatosImportReporte.SetEmpresa(const Value: string);
begin
     FEmpresa := Value;
end;

procedure TDatosImportReporte.SetFecha(const Value: string);
begin
     FFecha := Value;
end;

procedure TDatosImportReporte.SetNombre(const Value: string);
begin
     FNombre := Value;
end;

procedure TDatosImportReporte.SetTabla(const Value: string);
begin
     FTabla := Value;
end;

procedure TDatosImportReporte.SetTipo(const Value: string);
begin
     FTipo := Value;
end;

procedure TDatosImportReporte.SetUsuario(const Value: string);
begin
     FUsuario := Value;
end;

procedure TDatosImportReporte.SetPlantilla(const Value: string);
begin
     if ( Value = '' ) OR (Value=(K_TEMPLATE+'.QR2')) then
     begin
          FImportar := FALSE;
          FPlantilla := K_TEMPLATE+'.QR2';
     end
     else if Value = K_NO_REQUIERE then
     begin
          FImportar := FALSE;
          FPlantilla := Value;
     end
     else
     begin
          FImportar := TRUE;
          FPlantilla := Value;
     end;
end;


function TDatosImportReporte.GetImportarPlantilla : Boolean;
begin
     Result := FImportar;
end;

procedure TDatosImportReporte.SetClasificacion(const Value: eClasifiReporte);
begin
     FClasificacion := Value;
end;


procedure TDatosImportReporte.SetImportarPlantilla(const Value: boolean);
begin
     FImportarPlantilla := Value;
end;

end.
