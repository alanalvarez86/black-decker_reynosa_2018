unit DBaseSistema;

interface

{$DEFINE MULTIPLES_ENTIDADES}
{$INCLUDE DEFINES.INC}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBClient, CheckLst,
     ZetaCommonLists,
     ZAccesosMgr,
     ZetaTipoEntidad,
     {$ifdef DOS_CAPAS}
     DServerSistema,
     {$else}
     Sistema_TLB,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaClientDataSet,
     ZetaServerDataSet,
     ComCtrls,
     ZetaCommonClasses,
     ZetaKeyCombo;

type
  eArbolDerechos = (adTress,adClasifi,adEntidades,adAdicionales);
  TGetTextoBitacora = function ( Nodo: TTreeNode; const iDerecho: TDerechosAcceso): String of Object;
  TGrupoEmpresa = class( TObject )
  private
    { Private declarations }
    FGrupo: Integer;
    FGrupoName: String;
    FEmpresa: String;
    FEmpresaName: String;
  public
    { Public declarations }
    property Grupo: Integer read FGrupo write FGrupo;
    property GrupoName: String read FGrupoName write FGrupoName;
    property Empresa: String read FEmpresa write FEmpresa;
    property EmpresaName: String read FEmpresaName write FEmpresaName;
    function GetGroupDescription: String;
    function GetEmpresaDescription: String;
  end;
  TGruposEmpresas = class( TObject )
  private
    { Private declarations }
    FLista: TList;
    function GetItem( Index: Integer ): TGrupoEmpresa;
    procedure Delete(const Index: Integer);
  public
    { Public declarations }
    constructor Create;
    destructor Destroy; override;
    property Items[ Index: Integer ]: TGrupoEmpresa read GetItem;
    function Add: TGrupoEmpresa;
    function Count: Integer;
    procedure Clear;
  end;
  TdmBaseSistema = class(TDataModule)
    cdsEmpresas: TZetaLookupDataSet;
    cdsGrupos: TZetaLookupDataSet;
    cdsUsuarios: TZetaLookupDataSet;
    cdsImpresoras: TZetaClientDataSet;
    cdsAccesosBase: TZetaClientDataSet;
    cdsEmpresasLookUp: TZetaLookupDataSet;
    cdsSuscrip: TZetaClientDataSet;
    cdsCopiarAccesos: TZetaClientDataSet;
    cdsGruposLookup: TZetaLookupDataSet;
    cdsEmpresasAccesos: TZetaLookupDataSet;
    cdsDerechos: TServerDataSet;
    cdsUsuariosLookup: TZetaLookupDataSet;
    cdsClasifiRepEmp: TZetaLookupDataSet;
    cdsRoles: TZetaLookupDataSet;
    cdsUserRoles: TZetaLookupDataSet;
    cdsGruposTodos: TZetaLookupDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsEmpresasAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosAlAdquirirDatos(Sender: TObject);
    procedure cdsImpresorasAlAdquirirDatos(Sender: TObject);
    procedure cdsUsuariosAlModificar(Sender: TObject);
    procedure cdsUsuariosAfterOpen(DataSet: TDataSet);
    procedure cdsUsuariosAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosAlCrearCampos(Sender: TObject);
    procedure cdsEmpresasAlEnviarDatos(Sender: TObject);
    procedure cdsEmpresasAfterDelete(DataSet: TDataSet);
    procedure cdsEmpresasBeforePost(DataSet: TDataSet);
    procedure cdsEmpresasNewRecord(DataSet: TDataSet);
    procedure cdsImpresorasAfterDelete(DataSet: TDataSet);
    procedure cdsImpresorasAlEnviarDatos(Sender: TObject);
    procedure cdsGruposAfterDelete(DataSet: TDataSet);
    procedure cdsGruposAlEnviarDatos(Sender: TObject);
    procedure cdsUsuariosAfterDelete(DataSet: TDataSet);
    procedure cdsUsuariosBeforePost(DataSet: TDataSet);
    procedure cdsUsuariosNewRecord(DataSet: TDataSet);
    procedure cdsAccesosBaseAlEnviarDatos(Sender: TObject);
    {$ifdef VER130}
    procedure GenericReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsUsuariosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$else}
    procedure GenericReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsUsuariosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    {$endif}
    procedure cdsEmpresasLookUpAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpresasBeforeInsert(DataSet: TDataSet);
    procedure cdsUsuariosGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsEmpresasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsGruposGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsSuscripAlAdquirirDatos(Sender: TObject);
    procedure cdsSuscripAlCrearCampos(Sender: TObject);
    procedure cdsSuscripAlEnviarDatos(Sender: TObject);
    procedure cdsSuscripNewRecord(DataSet: TDataSet);
    procedure cdsSuscripBeforePost(DataSet: TDataSet);
    procedure cdsGruposNewRecord(DataSet: TDataSet);
    procedure cdsGruposBeforePost(DataSet: TDataSet);
    procedure cdsGruposLookupAlAdquirirDatos(Sender: TObject);
    procedure cdsGruposBeforeInsert(DataSet: TDataSet);
    procedure cdsEmpresasAccesosAlAdquirirDatos(Sender: TObject);
    procedure cdsEmpresasAccesosAlCrearCampos(Sender: TObject);
    procedure cdsUsuariosLookupGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsGruposTodosAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    FTipoBitacora: Integer;
    FEmpleadoNumero: Integer;
    FFechaInicial: TDate;
    FUltimaFrecuencia: Integer;
    FFechaFinal: TDate;
    FUsuario: Integer;
    FSiguienteGrupo: integer;
    FMaxDigito: Char;
    FBloqueoSistema: Boolean;
    FArbolSeleccionado: eArbolDerechos;
    FCodigoCopiado: string ;
    FEditandoUsuarios:Boolean;
    function GetTitulo( Nodo: TTreeNode; var iPos: Integer): String;
    procedure CambiaUsuarioFechaSalida( Sender: TField );
    procedure ActualizaFechaPassWord(Sender: TField; const Text: String);
    procedure SU_FRECUENGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ACCESOSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ConectaUsuarioSuscrip( const iUsuario: integer );

  protected
    { Protected declarations }
    FTextoBitacora: String;
    FcdsAccesos: TZetaClientDataSet;

{$ifdef DOS_CAPAS}
    function GetServerSistema: TdmServerSistema;
    property ServerSistema: TdmServerSistema read GetServerSistema;
{$else}
    FServidor: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
{$endif}
    function GetcdsAccesos : TZetaClientDataSet; virtual;
    function BuscaAccesos( const iPosition: Integer ): Boolean; virtual;
  public
    { Public declarations }
    property UltimaFrecuencia: Integer read FUltimaFrecuencia;
    property FechaInicial: TDate read FFechaInicial write FFechaInicial;
    property FechaFinal: TDate read FFechaFinal write FFechaFinal;
    property TipoBitacora: Integer  read FTipoBitacora write FTipoBitacora;
    property EmpleadoNumero: Integer read FEmpleadoNumero write FEmpleadoNumero;
    property Usuario: Integer read FUsuario write FUsuario;
    property BloqueoSistema: Boolean read FBloqueoSistema;
    property ArbolSeleccionado : eArbolDerechos read FArbolSeleccionado write FArbolSeleccionado ;
    property CodigoCopiado: string read FCodigoCopiado write FCodigoCopiado;
    property EditandoUsuarios: Boolean read FEditandoUsuarios write FEditandoUsuarios;
    function BuscaDerecho( const iPosition: Integer ): TDerechosAcceso; virtual;
    function BuscaDerechoCopiado( const iPosition: Integer ): TDerechosAcceso;
    function CargarGruposAccesos( Lista: TGruposEmpresas; var iDefault: Integer ): Boolean;
    function UsuarioPosicionado: Integer;
    procedure ActivaUsuarios;
    procedure AgregaEmpresa;
    procedure ConectaAccesos( var sGroupName, sCompanyName: String ); virtual;
    procedure CopyAccesos(iGrupo: Integer; sCompany: String);virtual;
    procedure DatasetDerechosCrear;
    procedure GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);virtual;
    procedure PosicionaUsuarioActivo;
    procedure SuspendeUsuarios;
    procedure BorraSuscripciones(const iReporte: Integer );
    function AgregaSuscripciones( iUsuario, iReporte: Integer ): boolean;overload;
    function AgregaSuscripciones( iUsuario, iReporte, iFrecuencia: Integer; iHayDatos:Integer): boolean; overload;
    function PuedeModificarUsuarioPropio( DataSet : TDataSet ) : Boolean;

    procedure GetReporteSuscrip( const iReporte: Integer );
    procedure GrabaSuscrip;
    property cdsAccesos : TZetaClientDataSet read  GetcdsAccesos;
    function GetEmpresaAccesos: OleVariant;overload;
    function GetEmpresaAccesos( const sCompany: string ): OleVariant;overload;

    procedure GetListaClasifiEmpresa( oLista : TStrings ); virtual;
    procedure ObtieneDescripcionPadre( const iPadre: Integer; var sCodigo, sDescrip: String );
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);virtual;
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);virtual;
    {$endif}
    function BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso; virtual;
    function BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso; virtual;
    function BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;virtual;
    function BuscaDerechoCopiadoAdicionales(const iPosition: Integer ): TDerechosAcceso;virtual;
    function BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;virtual;
    function BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;virtual;

    procedure ModificaUsuarios(const lNavegacion: Boolean );
    procedure DescargaListaUserRoles( Lista: TStrings );virtual;
    procedure CargaListaRolesDefault( Lista: TStrings );virtual;
    procedure CargaListaRoles( Lista: TStrings );virtual;
    procedure BajaUsuario(const iEmpleado:Integer;const lQuitarUsuario:Boolean);virtual;
    procedure ConectaGrupoAdAdmin( oEmpresa: OleVariant );virtual;
    procedure ConectaAccesosAdicionales;virtual;
    procedure CopiaDerechosAdicionales;virtual;
    procedure LeeTodosGrupos( Combo: TZetaKeyCombo );
  end;

var
  dmBaseSistema: TdmBaseSistema;

const
     K_MODIFICAR_USUARIO_PROPIO = ZetaCommonClasses.K_DERECHO_SIST_KARDEX;

procedure CambiarAccesos;

implementation

uses ZetaCommonTools,
     ZetaDataSetTools,
     ZetaTipoEntidadTools,
     ZetaDialogo,
     ZAccesosTress,
     ZReconcile,
     DCliente,
     DSistema;

{$R *.DFM}

procedure CambiarAccesos;
begin
end;

{ ****** TGrupoEmpresa ****** }

function TGrupoEmpresa.GetGroupDescription: String;
begin
     Result := Format( 'Grupo: %s', [ FGrupoName ] );
end;

function TGrupoEmpresa.GetEmpresaDescription: String;
begin
     Result := Format( '%s', [ FEmpresaName ] );
end;

{ ****** TGruposEmpresas ****** }

constructor TGruposEmpresas.Create;
begin
     FLista := TList.Create;
end;

destructor TGruposEmpresas.Destroy;
begin
     FreeAndNil( FLista );
     inherited Destroy;
end;

procedure TGruposEmpresas.Clear;
var
   i: Integer;
begin
     for i := ( Count - 1 ) downto 0 do
         Delete( i );
end;

function TGruposEmpresas.Add: TGrupoEmpresa;
begin
     Result := TGrupoEmpresa.Create;
     FLista.Add( Result );
end;

procedure TGruposEmpresas.Delete(const Index: Integer);
begin
     Items[ Index ].Free;
     FLista.Delete( Index );
end;

function TGruposEmpresas.Count: Integer;
begin
     Result := FLista.Count;
end;

function TGruposEmpresas.GetItem(Index: Integer): TGrupoEmpresa;
begin
     Result := TGrupoEmpresa( FLista.Items[ Index ] );
end;

{ ********* TdmBaseSistema *********** }

procedure TdmBaseSistema.DataModuleCreate(Sender: TObject);
begin
     FUltimaFrecuencia := Ord( frDiario );
end;

function TdmBaseSistema.GetcdsAccesos : TZetaClientDataSet;
begin
     Result := cdsAccesosBase;
end;

{$ifdef DOS_CAPAS}
function TdmBaseSistema.GetServerSistema: TdmServerSistema;
begin
     Result := DCliente.dmCliente.ServerSistema;
end;
{$else}
function TdmBaseSistema.GetServerSistema: IdmServerSistemaDisp;
begin
     Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServidor ) );
end;
{$endif}

{$ifdef VER130}
procedure TdmBaseSistema.GenericReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmBaseSistema.GenericReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmBaseSistema.NotifyDataChange(const Entidades: Array of TipoEntidad; const Estado: TipoEstado);
{$else}
procedure TdmBaseSistema.NotifyDataChange(const Entidades: ListaEntidades; const Estado: TipoEstado);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enUsuarios , Entidades ) then
     {$else}
     if ( enUsuarios in Entidades ) then
     {$endif}
        cdsUsuarios.SetDataChange;
end;

{ *********** Empresas ************ }

procedure TdmBaseSistema.cdsEmpresasAlAdquirirDatos(Sender: TObject);
begin
     with dmCliente do
     begin
          cdsEmpresas.Data := ServerSistema.GetEmpresas( Ord( TipoCompany ), GetGrupoActivo );
          { Para que se refresque el Dataset con el que se escogen las empresas }
          cdsCompany.SetDataChange;
     end;
end;

procedure TdmBaseSistema.AgregaEmpresa;
begin
     with cdsEmpresas do
     begin
          Refrescar;
          Agregar;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsEmpresas do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerSistema.GrabaEmpresas( Delta, ErrorCount ) ) then
               begin
                    cdsEmpresasLookUp.Refrescar;
                    { Para que se refresque el Dataset con el que se escogen las empresas }
                    dmCliente.cdsCompany.SetDataChange;
               end;
          end;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasAfterDelete(DataSet: TDataSet);
begin
     cdsEmpresas.Enviar;
end;

procedure TdmBaseSistema.cdsEmpresasBeforePost(DataSet: TDataSet);
begin
     with cdsEmpresas do
     begin
          if StrVacio( FieldByName( 'CM_CODIGO' ).AsString ) then
             DatabaseError( 'El C�digo De La Empresa No Puede Quedar Vac�o' );
          if ( FieldByName( 'CM_ACUMULA' ).AsString = FieldByName( 'CM_CODIGO' ).AsString ) then
             DataBaseError( 'C�digo De Empresa Acumuladora Debe Ser Diferente Al C�digo De Empresa' );
     end;
end;

procedure TdmBaseSistema.cdsEmpresasBeforeInsert(DataSet: TDataSet);
begin
     cdsEmpresasLookUp.Data := cdsEmpresas.Data;  // si se hace despu�s, regresa el Dataset a dsBrowse
     FMaxDigito := ZetaDataSetTools.GetNextCaracter( DataSet, 'CM_DIGITO' );
     if EsMinuscula( FMaxDigito ) then
        FMaxDigito := #0;               // La 'a' minuscula es mayor que 'Z' y el m�ximo valor es 'Z'
end;

procedure TdmBaseSistema.cdsEmpresasNewRecord(DataSet: TDataSet);
const
     K_SIN_RESTRICCION = -1;
begin
     with cdsEmpresas do
     begin
          FieldByName( 'CM_EMPATE' ).AsInteger := 0;
          FieldByName( 'CM_USACAFE' ).AsString := K_GLOBAL_NO;
          FieldByName( 'CM_CONTROL' ).AsString := ObtieneElemento( lfTipoCompany, Ord( dmCliente.TipoCompany ) );
          FieldByName( 'CM_DIGITO' ).AsString := FMaxDigito;
          FieldByName( 'CM_KCLASIFI').AsInteger:= K_SIN_RESTRICCION;
     end;
end;

procedure TdmBaseSistema.cdsEmpresasGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_EMPRESAS, iRight );
end;

procedure TdmBaseSistema.ACCESOSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     Text := ZetaCommonTools.BoolToSiNo( Sender.AsInteger > 0 );
end;

{ cdsEmpresasLookup }

procedure TdmBaseSistema.cdsEmpresasLookUpAlAdquirirDatos(Sender: TObject);
begin
     with cdsEmpresas do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsEmpresasLookUp.Data := Data;
          end;
     end;
end;

{ cdsEmpresasAccesos }

procedure TdmBaseSistema.cdsEmpresasAccesosAlAdquirirDatos( Sender: TObject);
begin
     with dmCliente do
     begin
          cdsEmpresasAccesos.Data := ServerSistema.GetEmpresasAccesos( Ord( TipoCompany ), cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger );
     end;
end;

procedure TdmBaseSistema.cdsEmpresasAccesosAlCrearCampos(Sender: TObject);
begin
     with cdsEmpresasAccesos do
     begin
          with FieldByName( 'ACCESOS' ) do
          begin
               OnGetText := ACCESOSGetText;
               Alignment := taLeftJustify;
          end;
     end;
end;

{ ********* Grupos *********** }

procedure TdmBaseSistema.cdsGruposAlAdquirirDatos(Sender: TObject);
begin
     cdsGrupos.Data := ServerSistema.GetGrupos( dmCliente.GetGrupoActivo );
end;


procedure TdmBaseSistema.cdsGruposNewRecord(DataSet: TDataSet);
begin
     cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger := FSiguienteGrupo;
end;

procedure TdmBaseSistema.cdsGruposBeforeInsert(DataSet: TDataSet);
begin
     FSiguienteGrupo := ZetaDataSetTools.GetNextEntero( DataSet, 'GR_CODIGO', 1 );
     cdsGruposLookUp.Data := cdsGrupos.Data;  // si se hace despu�s, regresa el Dataset a dsBrowse
end;

procedure TdmBaseSistema.cdsGruposAfterDelete(DataSet: TDataSet);
begin
     cdsGrupos.Enviar
     // Cambiar a espa�ol el 'Cannot delete parent table because child table exists'
end;

procedure TdmBaseSistema.cdsGruposBeforePost(DataSet: TDataSet);
begin
     with cdsGrupos do
     begin
          if ( FieldByName( 'GR_CODIGO' ).AsInteger <= 0 ) then
          begin
               FieldByName( 'GR_CODIGO' ).FocusControl;
               DataBaseError( '� Falta Especificar el C�digo del Grupo !' );
          end  { Verificaci�n de que el C�digo del Grupo NO sea el Mismo que el Grupo al que esta subordinado }
          else if ( FieldByName( 'GR_CODIGO' ).AsInteger = FieldByName( 'GR_PADRE' ).AsInteger ) then
               begin
                    FieldByName( 'GR_PADRE' ).FocusControl;
                    DataBaseError( '� El C�digo Del Grupo No Puede Ser Igual Al C�digo del Grupo Subordinado !' );
               end { Verificaci�n de que el C�digo del Grupo Subordinado no esta Vacio }
               else if StrVacio ( FieldByName( 'GR_PADRE' ).AsString ) or ( FieldByName( 'GR_PADRE' ).AsInteger = 0 ) then
                    begin
                         FieldByName( 'GR_PADRE' ).FocusControl;
                         DataBaseError( '� El C�digo Del Grupo Subordinado No Puede Quedar Vacio !' );
                    end
     end;
end;

procedure TdmBaseSistema.cdsGruposAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsGrupos do
     begin
          if State in [dsEdit, dsInsert] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               if Reconcile( ServerSistema.GrabaGrupos( Delta, dmCliente.Usuario, ErrorCount) ) then
               begin
                    cdsGruposLookup.Refrescar;
               end;
          end;
     end;
end;

procedure TdmBaseSistema.cdsGruposGetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_GRUPOS, iRight );
end;

procedure TdmBaseSistema.cdsGruposLookupAlAdquirirDatos(Sender: TObject);
begin
     with cdsGrupos do
     begin
          if ( State <> dsInsert ) then  // se condiciona porque el traspaso de Data regresa el
          begin                          // dataset a dsBrowse
               Conectar;
               cdsGruposLookUp.Data := Data;
          end;
     end;
end;

{ ************ Usuarios ********* }

procedure TdmBaseSistema.cdsUsuariosAlCrearCampos(Sender: TObject);
begin
     cdsGrupos.Conectar;
     cdsUsuarios.CreateSimpleLookup( cdsGrupos, 'GR_DESCRIP', 'GR_CODIGO' );
     cdsUsuarios.ListaFija('US_FORMATO', lfTipoFormato );
end;

procedure TdmBaseSistema.cdsUsuariosAlAdquirirDatos(Sender: TObject);
var
   iLongitud: Integer;
   lBlockSistema: WordBool;
begin
     with dmCliente do
     begin
          cdsUsuarios.Data := ServerSistema.GetUsuarios( iLongitud, GetGrupoActivo, lBlockSistema );
          SetPasswordLongitud( iLongitud );
          FBloqueoSistema:= lBlockSistema;
     end;
     cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
end;

procedure TdmBaseSistema.SuspendeUsuarios;
begin
     with dmCliente do
          cdsUsuarios.Data := ServerSistema.SuspendeUsuarios( Usuario, GetGrupoActivo);
end;

procedure TdmBaseSistema.ActivaUsuarios;
begin
     with dmCliente do
          cdsUsuarios.Data := ServerSistema.ActivaUsuarios( Usuario, GetGrupoActivo );
end;

procedure TdmBaseSistema.cdsUsuariosAfterDelete(DataSet: TDataSet);
begin
     cdsUsuarios.Enviar;
end;

procedure TdmBaseSistema.cdsUsuariosBeforePost(DataSet: TDataSet);
var
   sClave, sMensaje: String;
begin
     with cdsUsuarios do
     begin
          if ( FieldByName( 'US_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'N�mero De Usuario Debe Ser Mayor A 0' );
          if ZetaCommonTools.StrVacio( FieldByName( 'US_CORTO' ).AsString ) then
             DataBaseError( 'El Nombre Corto De Usuario No Puede Quedar Vac�o' );
          if ZetaCommonTools.StrVacio(FieldByName( 'US_NOMBRE' ).AsString ) then
             DataBaseError( 'El Nombre Del Usuario No Puede Quedar Vac�o' );
          if ( FieldByName( 'GR_CODIGO' ).AsInteger <= 0 ) then
             DataBaseError( 'El Grupo Del Usuario Debe Ser Mayor A 0' );

          sClave := FieldByName( 'US_PASSWRD' ).AsString;

          if ZetaCommonTools.StrVacio( sClave ) then
             DataBaseError( 'La Clave De Acceso Del Usuario No Puede Quedar Vac�a' );
          with dmCliente.DatosSeguridad do
          begin
               if ( PasswordLongitud > 0 ) and ( Length( sClave ) < PasswordLongitud ) then
                  DataBaseError( Format( 'Clave De Acceso Debe Tener Por Lo Menos %d Caracteres', [ PasswordLongitud ] ) );
          end;
          if not dmCliente.ValidaCaracteresPassword( sClave, sMensaje ) then
             DataBaseError( sMensaje );
          if strVacio( FieldByName( 'CM_CODIGO' ).AsString ) then
             FieldByName('CB_CODIGO').AsInteger := 0;
             
          if dmCliente.getDatosUsuarioActivo.LoginAD then
          begin
               cdsUsuariosLookup.Locate('US_CODIGO',FieldByName( 'US_CODIGO' ).AsInteger,[] );

               if ( UpperCase( cdsUsuariosLookup.FieldByName('US_DOMAIN').AsString ) <> UpperCase( FieldByName( 'US_DOMAIN' ).AsString ) )then
               begin
                    if( cdsUsuariosLookup.Locate('US_DOMAIN',UpperCase( FieldByName( 'US_DOMAIN' ).AsString ),[] ) )then
                     DataBaseError( 'El Usuario del Dominio: '+UpperCase( FieldByName( 'US_DOMAIN' ).AsString )+' est� repetido' )
                    else
                        FieldByName( 'US_DOMAIN' ).AsString := UpperCase( FieldByName( 'US_DOMAIN' ).AsString );
               end;
          end
          else
              FieldByName( 'US_DOMAIN' ).AsString := UpperCase( FieldByName( 'US_DOMAIN' ).AsString );


          FieldByName( 'US_CORTO' ).AsString := UpperCase( FieldByName( 'US_CORTO' ).AsString );
     end;
end;

procedure TdmBaseSistema.cdsUsuariosNewRecord(DataSet: TDataSet);
begin
     with cdsUsuarios do
     begin
          FieldByName( 'US_CODIGO' ).AsInteger := ServerSistema.GetNuevoUsuario;
          FieldByName( 'US_CAMBIA' ).AsString := K_GLOBAL_SI;
          FieldByName( 'US_NIVEL' ).AsInteger := 1;
          FieldByName( 'US_BLOQUEA' ).AsString := K_GLOBAL_NO;
          FieldByName( 'US_DENTRO' ).AsString := K_GLOBAL_NO;
          FieldByName( 'US_ACTIVO' ).AsString := K_GLOBAL_SI;
     end;
end;

procedure TdmBaseSistema.cdsUsuariosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   iLongitud: Integer;
   lBlockSistema: WordBool;

   deltaBack : Olevariant;
begin
     ErrorCount := 0;

     with cdsUsuarios do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               deltaBack := Delta;
               if Reconcile( ServerSistema.GrabaUsuarios( Delta, dmCliente.Usuario, ErrorCount ) ) then
               begin
                  {$ifndef DOS_CAPAS}
                  {$ifdef TRESS}
                  if (dmCliente.EmpresaAbierta) and ( ErrorCount = 0 ) then
                  begin
                       ServerSistema.ActualizaUsuariosEmpresa( dmCliente.Empresa, deltaBack);
                  end;
                  {$endif}
                  {$endif}
                  cdsUsuariosLookUp.Data := ServerSistema.GetUsuarios( iLongitud, ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION, lBlockSistema );
               end;
          end;



     end;


     {
     Adem�s de cambiar los datos del usuario hay que
     cambiar el campo SUPER.US_CODIGO si USUARIO.US_CODIGO
     tuvo cambio � si el usuario fu� borrado
     }
end;

procedure TdmBaseSistema.CambiaUsuarioFechaSalida( Sender: TField );
begin
     with Sender do
     begin
          if ( Dataset.State in [ dsEdit, dsInsert ] ) and not ZetaCommonTools.zStrToBool( AsString ) then
             Dataset.FieldByName( 'US_FEC_OUT' ).AsDateTime := Now;
     end;
end;

procedure TdmBaseSistema.cdsUsuariosAfterOpen(DataSet: TDataSet);
begin
     with cdsUsuarios do
     begin
          FieldByName( 'US_DENTRO' ).OnChange := CambiaUsuarioFechaSalida;
          FieldByName( 'US_PASSWRD' ).OnSetText := ActualizaFechaPassWord;
     end;
end;

procedure TdmBaseSistema.ActualizaFechaPassWord(Sender: TField; const Text: String);
begin
     Sender.AsString := Text;
     cdsUsuarios.FieldByName( 'US_FEC_PWD' ).AsDateTime := Date;
end;

procedure TdmBaseSistema.ModificaUsuarios(const lNavegacion: Boolean );
begin
end;

procedure TdmBaseSistema.cdsUsuariosAlModificar(Sender: TObject);
begin
     ModificaUsuarios(TRUE);
end;

procedure TdmBaseSistema.PosicionaUsuarioActivo;
begin
     with cdsUsuarios do
     begin
          Conectar;
          // Posiciona 'cdsUsuarios' en Usuario Activo
          GetDescripcion( IntToStr( dmCliente.Usuario ) );
     end;
end;

function TdmBaseSistema.UsuarioPosicionado: Integer;
begin
     // No es lo mismo UsuarioActivo (el que di� Login) que
     // usuario posicionado (El registro activo de cdsUsuarios)
     Result := cdsUsuarios.FieldByName( 'US_CODIGO' ).AsInteger;
end;

procedure TdmBaseSistema.cdsUsuariosGetRights(Sender: TZetaClientDataSet;const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( D_SIST_DATOS_USUARIOS, iRight );
end;

{$ifdef VER130}
procedure TdmBaseSistema.cdsUsuariosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( pos( 'XAK1', E.Message ) > 0 ) then
        E.Message := '� Nombre De Usuario Ya Existe !';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmBaseSistema.cdsUsuariosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     if ( pos( 'XAK1', E.Message ) > 0 ) then
        E.Message := '� Nombre De Usuario Ya Existe !';
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{ ****** Impresoras *********** }

procedure TdmBaseSistema.cdsImpresorasAlAdquirirDatos(Sender: TObject);
begin
     cdsImpresoras.Data := ServerSistema.GetImpresoras;
end;

procedure TdmBaseSistema.cdsImpresorasAfterDelete(DataSet: TDataSet);
begin
     cdsImpresoras.Enviar;
end;

procedure TdmBaseSistema.cdsImpresorasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsImpresoras do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerSistema.GrabaImpresoras( Delta, ErrorCount ) );
    end;
end;

{ *********** Derechos de Acceso ************** }

procedure TdmBaseSistema.ConectaAccesos( var sGroupName, sCompanyName: String );
var
   iGrupo: Integer;
   sCompany: String;
begin
     FTextoBitacora:= VACIO;
     with cdsGrupos do
     begin
          iGrupo := FieldByName( 'GR_CODIGO' ).AsInteger;
          sGroupName := FieldByName( 'GR_DESCRIP' ).AsString;
     end;
     with cdsEmpresasAccesos do
     begin
          sCompany := FieldByName( 'CM_CODIGO' ).AsString;
          sCompanyName := FieldByName( 'CM_NOMBRE' ).AsString;
     end;
     cdsAccesos.Data := ServerSistema.GetAccesos( iGrupo, sCompany )
end;

function TdmBaseSistema.BuscaAccesos( const iPosition: Integer ): Boolean;
begin
     Result := cdsAccesos.Locate( 'GR_CODIGO;CM_CODIGO;AX_NUMERO', VarArrayOf( [ cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger, cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString, iPosition ] ), [] );
end;

function TdmBaseSistema.BuscaDerecho( const iPosition: Integer ): TDerechosAcceso;
begin
     if BuscaAccesos( iPosition ) then
        Result := cdsAccesos.FieldByName( 'AX_DERECHO' ).AsInteger
     else
         Result := K_SIN_DERECHOS;
end;

procedure TdmBaseSistema.CopyAccesos( iGrupo: Integer; sCompany: String );
begin
     with cdsCopiarAccesos do
     begin
          Data := ServerSistema.GetAccesos( iGrupo, sCompany );
          IndexFieldNames := 'AX_NUMERO';
     end;
end;

function TdmBaseSistema.BuscaDerechoCopiado( const iPosition: Integer ): TDerechosAcceso;
begin
     with cdsCopiarAccesos do
     begin
          if Locate( 'AX_NUMERO', VarArrayOf( [ iPosition ] ), [] ) then
             Result := FieldByName( 'AX_DERECHO' ).AsInteger
          else
              Result := K_SIN_DERECHOS;
     end;
end;

function TdmBaseSistema.GetTitulo( Nodo: TTreeNode; var iPos: Integer ): String;
var
   oPadre: TTreeNode;
begin
     Result := VACIO;
     oPadre := Nodo.Parent;
     while ( Assigned ( oPadre ) ) do
     begin
           Result := oPadre.Text + '-' + Result;
           oPadre := oPadre.Parent;
     end;
     iPos:= length(Result);
     Result:= Result + Nodo.Text;
end;

procedure TdmBaseSistema.GrabaDerecho( const iPosition: Integer; const iDerecho: TDerechosAcceso; oNodo: TTreeNode; oTextoBitacora: TGetTextoBitacora);
var
   sTextoBitacora: String;
   iPos: Integer;
begin
     with cdsAccesos do
     begin
          if BuscaAccesos( iPosition ) then
          begin
               if ( FieldByName( 'AX_DERECHO' ).AsInteger <> iDerecho ) then
               begin
                    sTextoBitacora:= GetTitulo(oNodo, iPos )+ CR_LF;
                    sTextoBitacora:= sTextoBitacora + Space(iPos) + 'Antes: '+ oTextoBitacora( oNodo, FieldByName('AX_DERECHO').AsInteger ) + CR_LF ;
                    Edit;
                    FieldByName( 'AX_DERECHO' ).AsInteger := iDerecho;
                    sTextoBitacora:= sTextoBitacora + Space(iPos)+ 'Nuevo: '+ oTextoBitacora( oNodo, iDerecho )+ CR_LF;
                    Post;
                    FTextoBitacora:= FTextoBitacora + sTextoBitacora;
               end;
          end
          else
          begin
               sTextoBitacora:= GetTitulo(oNodo, iPos)+ CR_LF;
               sTextoBitacora:= sTextoBitacora + Space(iPos) + 'Antes: Ninguno'+ CR_LF;
               Append;
               FieldByName( 'GR_CODIGO' ).AsInteger := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
               FieldByName( 'CM_CODIGO' ).AsString := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
               FieldByName( 'AX_NUMERO' ).AsInteger := iPosition;
               FieldByName( 'AX_DERECHO' ).AsInteger := iDerecho;
               sTextoBitacora:= sTextoBitacora + Space(iPos)+ 'Nuevo: '+ oTextoBitacora( oNodo, iDerecho)+ CR_LF;
               Post;
               FTextoBitacora:= FTextoBitacora + sTextoBitacora;
          end;
     end;
end;


procedure TdmBaseSistema.cdsAccesosBaseAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
   sCodigo, sDescripcion: string;
   oParametros: TZetaParams;
begin
     ErrorCount := 0;
     with cdsAccesos do
     begin
          if State in [ dsEdit, dsInsert ] then
             PostData;
          if ( ChangeCount > 0 ) then
          begin
               oParametros:= TZetaParams.Create;
               try
                  sDescripcion:= 'Cambi� Accesos: Grupo: ' + cdsGrupos.FieldByName('GR_CODIGO').AsString + ', Empresa: ' + cdsEmpresasAccesos.FieldByName('CM_CODIGO').AsString;
                  with oParametros do
                  begin
                       AddString('Descripcion', sDescripcion);
                       AddBlob('TextoBitacora', FTextoBitacora);
                       AddInteger('Usuario', dmCliente.Usuario);
                  end;

                  if Reconcile( ServerSistema.GrabaAccesos( Delta, oParametros.VarValues, ErrorCount ) ) then
                  begin
                       sCodigo := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
                       cdsEmpresasAccesos.Refrescar;
                       cdsEmpresasAccesos.Locate( 'CM_CODIGO', sCodigo, [ loCaseInsensitive ] );
                  end;
               finally
                      FreeAndNil(oParametros);
               end;
          end;
     end;
     FTextoBitacora:= VACIO;
end;

{ ********* cdsSuscrip ********* }

procedure TdmBaseSistema.ConectaUsuarioSuscrip( const iUsuario: integer );
begin
     cdsSuscrip.Data := ServerSistema.GetUsuarioSuscrip( dmCliente.Empresa, iUsuario );
end;

procedure TdmBaseSistema.cdsSuscripAlAdquirirDatos(Sender: TObject);
begin
     ConectaUsuarioSuscrip( UsuarioPosicionado );
end;

procedure TdmBaseSistema.cdsSuscripAlCrearCampos(Sender: TObject);
begin
    with cdsSuscrip do
    begin
         with FieldByName( 'SU_FRECUEN' ) do
         begin
              OnGetText := SU_FRECUENGetText;
              Alignment := taLeftJustify;
         end;
    end;
end;

procedure TdmBaseSistema.SU_FRECUENGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
    if Sender.IsNull then
        Text := ''
    else
        Text := ZetaCommonLists.ObtieneElemento( lfEmailFrecuencia, Sender.AsInteger );
end;

procedure TdmBaseSistema.cdsSuscripAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     ErrorCount := 0;
     with cdsSuscrip do
     begin
          if State in [ dsEdit, dsInsert ] then
             Post;
          if ( ChangeCount > 0 ) then
            Reconcile( ServerSistema.GrabaUsuarioSuscrip( dmCliente.Empresa, Delta, ErrorCount, UsuarioPosicionado ) );
    end;
end;

procedure TdmBaseSistema.cdsSuscripNewRecord(DataSet: TDataSet);
begin
    with cdsSuscrip do
        FieldByName( 'SU_FRECUEN' ).AsInteger := FUltimaFrecuencia;
end;

procedure TdmBaseSistema.cdsSuscripBeforePost(DataSet: TDataSet);
begin
    FUltimaFrecuencia := cdsSuscrip.FieldByName( 'SU_FRECUEN' ).AsInteger;
end;

function TdmBaseSistema.AgregaSuscripciones( iUsuario, iReporte, iFrecuencia, iHayDatos: Integer  ): boolean;
begin
     with cdsSuscrip do
     begin
          if not( Locate('US_CODIGO', iUsuario,[] ) ) then
          begin
               Append;
               FieldByName('RE_CODIGO').AsInteger:= iReporte;
               FieldByName('US_CODIGO').AsInteger:= iUsuario;
               FieldByName('SU_FRECUEN').AsInteger:= iFrecuencia;
               FieldByName('SU_VACIO').AsInteger:= iHayDatos;
               Post;
          end;
          Result:= ( ChangeCount > 0 );
     end
end;
function TdmBaseSistema.AgregaSuscripciones( iUsuario, iReporte: Integer ): boolean;
begin
     Result:= AgregaSuscripciones( iUsuario, iReporte, FUltimaFrecuencia, Ord(enEnviar) );
end;

procedure TdmBaseSistema.BorraSuscripciones( const iReporte: Integer );
begin
     with cdsSuscrip do
     begin
          ConectaUsuarioSuscrip( dmCliente.Usuario );
          if Locate('RE_CODIGO',iReporte, [] ) then;
          begin
             Delete;
             Enviar;
          end;
     end;
end;

procedure TdmBaseSistema.GetReporteSuscrip( const iReporte: Integer );
begin
     cdsSuscrip.Data := ServerSistema.GetReporteSuscrip( dmCliente.Empresa, iReporte );
end;

procedure TdmBaseSistema.GrabaSuscrip;
begin
     dmSistema.cdsSuscrip.Enviar;
end;

{ ********* Copiar Derechos de Acceso ********* }

function TdmBaseSistema.CargarGruposAccesos( Lista: TGruposEmpresas; var iDefault: Integer ): Boolean;
var
   iGrupo: Integer;
   sCompany: String;
begin
     iDefault := cdsGrupos.FieldByName( 'GR_CODIGO' ).AsInteger;
     iGrupo := iDefault;
     sCompany := cdsEmpresasAccesos.FieldByName( 'CM_CODIGO' ).AsString;
     Lista.Clear;
     with cdsCopiarAccesos do
     begin
          IndexName := '';
          Data := ServerSistema.GetGruposAccesos( iGrupo, sCompany, Ord( dmCliente.TipoCompany ) );
          while not Eof do
          begin
               with Lista do
               begin
                    with Add do
                    begin
                         Grupo := FieldByName( 'GR_CODIGO' ).AsInteger;
                         GrupoName := FieldByName( 'GR_DESCRIP' ).AsString;
                         Empresa := FieldByName( 'CM_CODIGO' ).AsString;
                         EmpresaName := FieldByName( 'CM_NOMBRE' ).AsString;
                    end;
               end;
               Next;
          end;
          Result := ( Lista.Count > 0 );
     end;
end;

{ ********* Imprimir  Derechos de Acceso ********* }

procedure TdmBaseSistema.DatasetDerechosCrear;
const
     K_MAX_BIT_RIGHT = $F;  { 0..15 }
var
   i: Integer;
begin
     with cdsDerechos do
     begin
          InitTempDataSet;
          AddIntegerField( 'RI_POS' );
          AddIntegerField( 'RI_NIVEL' );
          AddStringField( 'RI_NOMBRE', 255 );
          AddStringField( 'RI_COMENTA', 255 );
          for i := 0 to K_MAX_BIT_RIGHT do
          begin
               AddStringField( Format( 'RI_ITEM_%2.2d', [ i ] ), 1 );
          end;
          CreateTempDataset;
          LogChanges := False;
     end;
end;

function TdmBaseSistema.PuedeModificarUsuarioPropio( DataSet : TDataSet ) : Boolean;
begin
     Result := (dmCliente.Usuario <> Dataset.FieldByName( 'US_CODIGO' ).AsInteger) or
               ( (dmCliente.Usuario = Dataset.FieldByName( 'US_CODIGO' ).AsInteger) and
                 (ZAccesosMgr.CheckDerecho( D_SIST_DATOS_USUARIOS, K_MODIFICAR_USUARIO_PROPIO )) );
end;

function TdmBaseSistema.GetEmpresaAccesos: OleVariant;


begin
     with cdsEmpresasAccesos do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  dmSistema.Usuario,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString,
                                  cdsGrupos.FieldByName('GR_CODIGO').AsInteger ] );
     end;
end;

function TdmBaseSistema.GetEmpresaAccesos( const sCompany: string ): OleVariant;

 var sCompanyInicial: string;
begin

     with cdsEmpresasAccesos do
     begin
          sCompanyInicial := FieldByName('CM_CODIGO').AsString;
          try
             if Locate( 'CM_CODIGO', sCompany, [] ) then
                  Result := GetEmpresaAccesos
             else
             begin
                    raise Exception.Create( 'Error al buscar la Empresa ' + sCompany );
             end;
          finally
                 //Posicionarse otra vez en la empresa en la que estaba.
                 if NOT Locate( 'CM_CODIGO', sCompanyInicial, [] ) then
                    raise Exception.Create( 'Error al buscar la Empresa ' + sCompanyInicial );
          end
     end;
end;

procedure TdmBaseSistema.ObtieneDescripcionPadre( const iPadre: Integer; var sCodigo, sDescrip: String );
var
   oDataSetLookup: TZetaLookupDataSet;
begin
     sCodigo := VACIO;
     sDescrip := VACIO;
     if ( iPadre <> 0 ) then   // Si es administrador no tiene padre y regresa vacio
     begin
          oDataSetLookup := TZetaLookupDataSet.Create( self );
          try
             with oDataSetLookup do
             begin
                  Data := ServerSistema.GetGrupos( ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION );
                  if Locate( 'GR_CODIGO', iPadre, [] ) then
                  begin
                       sCodigo := IntToStr( iPadre );
                       sDescrip := FieldbyName( 'GR_DESCRIP' ).AsString;
                  end;
             end;
          finally
                 FreeAndNil( oDataSetLookup );
          end;
     end;
end;
{ Clasificaciones de reportes por empresa}

procedure TdmBaseSistema.GetListaClasifiEmpresa( oLista : TStrings );
begin
     {Aqui se conecta cdsClasifiRepEmp de acuerdo a la Empresa, se dejo en la base por compatibilidad }

end;

function TdmBaseSistema.BuscaDerechoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmBaseSistema.BuscaDerechoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmBaseSistema.BuscaDerechoAdicionales(const iPosition: Integer): TDerechosAcceso;
begin
     Result :=0;
end;

function TdmBaseSistema.BuscaDerechoCopiadoEntidades(const iPosition: Integer): TDerechosAcceso;
begin
     Result := 0;
end;

function TdmBaseSistema.BuscaDerechoCopiadoClasificaciones(const iPosition: Integer): TDerechosAcceso;
begin
     Result := 0;
end;

function TdmBaseSistema.BuscaDerechoCopiadoAdicionales(const iPosition: Integer ): TDerechosAcceso;
begin
     Result := 0;
end;



procedure TdmBaseSistema.cdsUsuariosLookupGetRights( Sender: TZetaClientDataSet; const iRight: Integer;
  var lHasRights: Boolean);
begin
     lHasRights := False;
end;

procedure TdmBaseSistema.DescargaListaUserRoles( Lista: TStrings );
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.CargaListaRolesDefault( Lista: TStrings );
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.CargaListaRoles( Lista: TStrings );
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.BajaUsuario(const iEmpleado:Integer;const lQuitarUsuario:Boolean);
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.ConectaGrupoAdAdmin;
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.ConectaAccesosAdicionales;
begin
     // No hace nada en la base
end;

procedure TdmBaseSistema.CopiaDerechosAdicionales;
begin
end;

procedure TdmBaseSistema.cdsGruposTodosAlAdquirirDatos(Sender: TObject);
begin
     cdsGruposTodos.Data := ServerSistema.GetGrupos( ZetaCommonClasses.D_GRUPO_SIN_RESTRICCION );
end;

procedure TdmBaseSistema.LeeTodosGrupos( Combo: TZetaKeyCombo);
begin
     with Combo do
     begin
          with cdsGruposTodos do
          begin
               Refrescar;
               First;
               while ( not EOF ) do
               begin
                    if strLleno( FieldByName( 'GR_DESCRIP' ).AsString ) then
                       Items.AddObject( FieldByName( 'GR_DESCRIP' ).AsString, TObject( FieldByName( 'GR_CODIGO' ).AsInteger ) );
                    Next;
               end;
          end;
          Sorted := True;
     end;
end;

end.

