object dmConsultas: TdmConsultas
  OldCreateOrder = False
  Left = 285
  Top = 161
  Height = 479
  Width = 741
  object cdsQueryGeneral: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AlAdquirirDatos = cdsQueryGeneralAlAdquirirDatos
    Left = 40
    Top = 16
  end
  object cdsBitacora: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsBitacoraAfterOpen
    Left = 40
    Top = 64
  end
  object cdsLogDetail: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = cdsLogDetailAfterOpen
    Left = 40
    Top = 161
  end
  object cdsProcesos: TZetaClientDataSet
    Aggregates = <>
    Params = <>
    Left = 40
    Top = 113
  end
end
