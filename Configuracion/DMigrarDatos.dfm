inherited dmMigrarDatos: TdmMigrarDatos
  Left = 285
  Height = 479
  Width = 741
  inherited dbSourceShared: TDatabase
    HandleShared = True
    Left = 268
    Top = 370
  end
  inherited dbSourceDatos: TDatabase
    HandleShared = True
    Left = 299
    Top = 370
  end
  inherited dbTarget: TDatabase
    HandleShared = True
    Left = 330
    Top = 370
  end
  inherited tqSource: TQuery
    Left = 454
    Top = 370
  end
  inherited tqTarget: TQuery
    SQL.Strings = ()
    Left = 485
    Top = 370
  end
  inherited tqDelete: TQuery
    Left = 516
    Top = 370
  end
  inherited ttTarget: TTable
    Left = 361
    Top = 370
  end
  inherited BatchMove: TBatchMove
    Left = 423
    Top = 370
  end
  inherited ttSource: TTable
    Left = 392
    Top = 370
  end
  object tqUno: TQuery
    Left = 16
    Top = 368
  end
  object tqDos: TQuery
    Left = 56
    Top = 368
  end
  object tqTres: TQuery
    Left = 96
    Top = 368
  end
  object tqCuatro: TQuery
    Left = 136
    Top = 368
  end
  object tqCinco: TQuery
    DatabaseName = 'dbTress'
    Left = 176
    Top = 368
  end
  object tqSeis: TQuery
    DatabaseName = 'dbTress'
    Left = 212
    Top = 368
  end
  object Global: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from GLOBAL')
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'update GLOBAL set GL_FORMULA = :GL_FORMULA'
      'where ( GL_CODIGO = :GL_CODIGO )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from GLOBAL.DBF')
    StartMessage = 'Configuraci'#243'n'
    TargetDatabase = dbTarget
    TargetTableName = 'GLOBAL'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = GlobalCloseDatasets
    OnFilterRecord = GlobalFilterRecord
    OnMoveExcludedFields = GlobalMoveExcludedFields
    OnOpenDatasets = GlobalOpenDatasets
    Left = 16
    Top = 6
  end
  object TCambio: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      
        'insert into TCAMBIO ( TC_FEC_INI, TC_MONTO, TC_NUMERO, TC_TEXTO ' +
        ')'
      'values ( :TC_FEC_INI, :TC_MONTO, :TC_NUMERO, :TC_TEXTO )'
      '')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'48'#39') and '
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Tipo Cambio'
    TargetDatabase = dbTarget
    TargetTableName = 'TCAMBIO'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnMoveExcludedFields = TCambioMoveExcludedFields
    Left = 64
    Top = 6
  end
  object Sal_Min: TZetaMigrar
    Tag = 1
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into SAL_MIN (SM_FEC_INI,'
      'SM_ZONA_A,'
      'SM_ZONA_B,'
      'SM_ZONA_C) values (:SM_FEC_INI,'
      ':SM_ZONA_A,'
      ':SM_ZONA_B,'
      ':SM_ZONA_C)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'45'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Salarios M'#237'nimos'
    TargetDatabase = dbTarget
    TargetTableName = 'SAL_MIN'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Sal_MinMoveExcludedFields
    Left = 112
    Top = 6
  end
  object Ley_Imss: TZetaMigrar
    Tag = 1
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into LEY_IMSS (SS_INICIAL,'
      'SS_L_A1061,'
      'SS_L_A1062,'
      'SS_L_A107,'
      'SS_L_A25,'
      'SS_L_CV,'
      'SS_L_GUARD,'
      'SS_L_INFO,'
      'SS_L_IV,'
      'SS_L_RT,'
      'SS_L_SAR,'
      'SS_MAXIMO,'
      'SS_O_A1061,'
      'SS_O_A1062,'
      'SS_O_A107,'
      'SS_O_A25,'
      'SS_O_CV,'
      'SS_O_GUARD,'
      'SS_O_INFO,'
      'SS_O_IV,'
      'SS_O_SAR,'
      'SS_P_A1061,'
      'SS_P_A1062,'
      'SS_P_A107,'
      'SS_P_A25,'
      'SS_P_CV,'
      'SS_P_GUARD,'
      'SS_P_INFO,'
      'SS_P_IV,'
      'SS_P_SAR) values (:SS_INICIAL,'
      ':SS_L_A1061,'
      ':SS_L_A1062,'
      ':SS_L_A107,'
      ':SS_L_A25,'
      ':SS_L_CV,'
      ':SS_L_GUARD,'
      ':SS_L_INFO,'
      ':SS_L_IV,'
      ':SS_L_RT,'
      ':SS_L_SAR,'
      ':SS_MAXIMO,'
      ':SS_O_A1061,'
      ':SS_O_A1062,'
      ':SS_O_A107,'
      ':SS_O_A25,'
      ':SS_O_CV,'
      ':SS_O_GUARD,'
      ':SS_O_INFO,'
      ':SS_O_IV,'
      ':SS_O_SAR,'
      ':SS_P_A1061,'
      ':SS_P_A1062,'
      ':SS_P_A107,'
      ':SS_P_A25,'
      ':SS_P_CV,'
      ':SS_P_GUARD,'
      ':SS_P_INFO,'
      ':SS_P_IV,'
      ':SS_P_SAR)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from LEY_IMSS.DBF')
    StartMessage = 'Tabla de Ley IMSS'
    TargetDatabase = dbTarget
    TargetTableName = 'LEY_IMSS'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Ley_ImssMoveExcludedFields
    Left = 161
    Top = 6
  end
  object Numerica: TZetaMigrar
    Tag = 1
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NUMERICA (NU_CODIGO,'
      'NU_DESCRIP,'
      'NU_INGLES,'
      'NU_NUMERO,'
      'NU_TEXTO) values (:NU_CODIGO,'
      ':NU_DESCRIP,'
      ':NU_INGLES,'
      ':NU_NUMERO,'
      ':NU_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA='#39'06'#39') and'
      '(TB_CODIGO<>'#39#39') and'
      '(TB_CODIGO<>'#39'07'#39') and'
      '(TB_CODIGO<>'#39'08'#39')')
    StartMessage = 'Num'#233'rica'
    TargetDatabase = dbTarget
    TargetTableName = 'NUMERICA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = NumericaCloseDatasets
    OnMoveExcludedFields = NumericaMoveExcludedFields
    Left = 214
    Top = 6
  end
  object Art_80: TZetaMigrar
    Tag = 1
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into ART_80 (NU_CODIGO,'
      'TI_INICIO,'
      'A80_LI,'
      'A80_CUOTA,'
      'A80_TASA) values (:NU_CODIGO,'
      ':TI_INICIO,'
      ':A80_LI,'
      ':A80_CUOTA,'
      ':A80_TASA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from ART_80.DBF')
    StartMessage = 'Art'#237'culo 80'
    TargetDatabase = dbTarget
    TargetTableName = 'ART_80'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = Art_80CloseDatasets
    OnFilterRecord = Art_80FilterRecord
    OnMoveExcludedFields = Art_80MoveExcludedFields
    Left = 264
    Top = 6
  end
  object Mot_Auto: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into MOT_AUTO(TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'47'#39') and '
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Motivo Auto'
    TargetDatabase = dbTarget
    TargetTableName = 'MOT_AUTO'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 312
    Top = 6
  end
  object Extra1: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into EXTRA1 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'31'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Extra 1'
    TargetDatabase = dbTarget
    TargetTableName = 'EXTRA1'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 360
    Top = 6
  end
  object Extra2: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into EXTRA2 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'32'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Extra 2'
    TargetDatabase = dbTarget
    TargetTableName = 'EXTRA2'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 408
    Top = 6
  end
  object Extra3: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into EXTRA3 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'33'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Extra 3'
    TargetDatabase = dbTarget
    TargetTableName = 'EXTRA3'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 454
    Top = 6
  end
  object Extra4: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into EXTRA4 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'34'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Extra 4'
    TargetDatabase = dbTarget
    TargetTableName = 'EXTRA4'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 502
    Top = 6
  end
  object EdoCivil: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into EDOCIVIL (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'01'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Estados Civiles'
    TargetDatabase = dbTarget
    TargetTableName = 'EDOCIVIL'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 16
    Top = 51
  end
  object Vive_En: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into VIVE_EN (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'02'#39') and '
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Vive En'
    TargetDatabase = dbTarget
    TargetTableName = 'VIVE_EN'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 64
    Top = 51
  end
  object Vive_Con: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into VIVE_CON (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'03'#39') and '
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla Vive Con'
    TargetDatabase = dbTarget
    TargetTableName = 'VIVE_CON'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 112
    Top = 51
  end
  object Entidad: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into ENTIDAD (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'44'#39') and '
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Entidades'
    TargetDatabase = dbTarget
    TargetTableName = 'ENTIDAD'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 161
    Top = 51
  end
  object Nivel1: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL1 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'21'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 1'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL1'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 214
    Top = 51
  end
  object Nivel2: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL2 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'22'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 2'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL2'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 264
    Top = 51
  end
  object Nivel3: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL3 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'23'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 3'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL3'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 312
    Top = 51
  end
  object Nivel4: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL4 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'24'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 4'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL4'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 360
    Top = 51
  end
  object Nivel5: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL5 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'25'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 5'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL5'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 408
    Top = 51
  end
  object Nivel6: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL6 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'26'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 6'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL6'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 454
    Top = 51
  end
  object Nivel7: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL7 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'27'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 7'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL7'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 502
    Top = 51
  end
  object Nivel8: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL8 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'28'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 8'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL8'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 16
    Top = 96
  end
  object Nivel9: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NIVEL9 (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'29'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Nivel de Organigrama 9'
    TargetDatabase = dbTarget
    TargetTableName = 'NIVEL9'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 64
    Top = 96
  end
  object OtrasPer: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into OTRASPER (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_TIPO,'
      'TB_MONTO,'
      'TB_TASA,'
      'TB_IMSS,'
      'TB_ISPT,'
      'TB_TOPE) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_TIPO,'
      ':TB_MONTO,'
      ':TB_TASA,'
      ':TB_IMSS,'
      ':TB_ISPT,'
      ':TB_TOPE)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'15'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Otras Percepciones'
    TargetDatabase = dbTarget
    TargetTableName = 'OTRASPER'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = OtrasPerMoveExcludedFields
    Left = 112
    Top = 96
  end
  object Clasifi: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CLASIFI (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_SALARIO,'
      'TB_OP1,'
      'TB_OP2,'
      'TB_OP3,'
      'TB_OP4,'
      'TB_OP5) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_SALARIO,'
      ':TB_OP1,'
      ':TB_OP2,'
      ':TB_OP3,'
      ':TB_OP4,'
      ':TB_OP5)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '( TB_TABLA = '#39'13'#39' ) and'
      '( TB_CODIGO <> '#39#39' )')
    StartMessage = 'Clasificaciones'
    TargetDatabase = dbTarget
    TargetTableName = 'CLASIFI'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = ClasifiMoveExcludedFields
    Left = 161
    Top = 96
  end
  object Puesto: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PUESTO ('
      'PU_CODIGO,'
      'PU_DESCRIP,'
      'PU_INGLES,'
      'PU_CLASIFI,'
      'PU_NUMERO,'
      'PU_TEXTO,'
      'PU_DETALLE,'
      'PU_ACTIVO,'
      'PU_CHECA,'
      'PU_AUTOSAL) values ('
      ':PU_CODIGO,'
      ':PU_DESCRIP,'
      ':PU_INGLES,'
      ':PU_CLASIFI,'
      ':PU_NUMERO,'
      ':PU_TEXTO,'
      ':PU_DETALLE,'
      ':PU_ACTIVO,'
      ':PU_CHECA,'
      ':PU_AUTOSAL)'
      ' '
      ' ')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'14'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Puestos'
    TargetDatabase = dbTarget
    TargetTableName = 'PUESTO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = PuestoMoveExcludedFields
    Left = 214
    Top = 96
  end
  object TCurso: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into TCURSO (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'K1'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Cursos'
    TargetDatabase = dbTarget
    TargetTableName = 'TCURSO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 264
    Top = 96
  end
  object Maestro: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into MAESTRO (MA_CODIGO,'
      'MA_NOMBRE,'
      'MA_CEDULA,'
      'MA_RFC) values (:MA_CODIGO,'
      ':MA_NOMBRE,'
      ':MA_CEDULA,'
      ':MA_RFC)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from MAESTROS.DBF')
    StartMessage = 'Maestros'
    TargetDatabase = dbTarget
    TargetTableName = 'MAESTRO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = MaestroMoveExcludedFields
    Left = 312
    Top = 96
  end
  object Curso: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CURSO (CU_CODIGO,'
      'CU_ACTIVO,'
      'CU_CLASIFI,'
      'CU_COSTO1,'
      'CU_COSTO2,'
      'CU_HORAS,'
      'CU_INGLES,'
      'CU_NOMBRE,'
      'CU_TEXTO1,'
      'CU_TEXTO2,'
      'MA_CODIGO) values (:CU_CODIGO,'
      ':CU_ACTIVO,'
      ':CU_CLASIFI,'
      ':CU_COSTO1,'
      ':CU_COSTO2,'
      ':CU_HORAS,'
      ':CU_INGLES,'
      ':CU_NOMBRE,'
      ':CU_TEXTO1,'
      ':CU_TEXTO2,'
      ':MA_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CURSOS.DBF')
    StartMessage = 'Cursos'
    TargetDatabase = dbTarget
    TargetTableName = 'CURSO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = CursoCloseDatasets
    OnMoveExcludedFields = CursoMoveExcludedFields
    OnOpenDatasets = CursoOpenDatasets
    Left = 360
    Top = 96
  end
  object CalCurso: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CALCURSO (CU_CODIGO,'
      'CC_FECHA) values (:CU_CODIGO,'
      ':CC_FECHA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CALENDAR')
    StartMessage = 'Calendario de Cursos'
    TargetDatabase = dbTarget
    TargetTableName = 'CALCURSO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = CalCursoMoveExcludedFields
    Left = 408
    Top = 96
  end
  object Entrena: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into ENTRENA (CU_CODIGO,'
      'PU_CODIGO,'
      'EN_DIAS,'
      'EN_OPCIONA,'
      'EN_LISTA) values (:CU_CODIGO,'
      ':PU_CODIGO,'
      ':EN_DIAS,'
      ':EN_OPCIONA,'
      ':EN_LISTA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from PUESTOS')
    StartMessage = 'Entrenamientos'
    TargetDatabase = dbTarget
    TargetTableName = 'ENTRENA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = EntrenaCloseDatasets
    OnMoveExcludedFields = EntrenaMoveExcludedFields
    OnMoveOtherTables = EntrenaMoveOtherTables
    OnOpenDatasets = EntrenaOpenDatasets
    Left = 454
    Top = 96
  end
  object Mot_Baja: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into MOT_BAJA (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_IMSS,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_IMSS,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS where (TB_TABLA= '#39'10'#39') AND (TB_CODIGO<>'#39#39')'
      '')
    StartMessage = 'Motivos de Baja'
    TargetDatabase = dbTarget
    TargetTableName = 'MOT_BAJA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Mot_BajaMoveExcludedFields
    Left = 502
    Top = 96
  end
  object Moneda: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into MONEDA (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_VALOR) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_VALOR)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'18'#39') and'
      '(TB_CODIGO<>'#39#39') and'
      '(TB_NUMERO<>0)')
    StartMessage = 'Tabla de Monedas'
    TargetDatabase = dbTarget
    TargetTableName = 'MONEDA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = MonedaMoveExcludedFields
    Left = 16
    Top = 142
  end
  object Querys: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from QUERYS')
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into QUERYS (QU_CODIGO,'
      'QU_DESCRIP,'
      'QU_FILTRO,'
      'QU_NIVEL,'
      'US_CODIGO) values (:QU_CODIGO,'
      ':QU_DESCRIP,'
      ':QU_FILTRO,'
      ':QU_NIVEL,'
      ':US_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from QUERYS.DBF where ( QU_CODIGO > '#39#39' )')
    StartMessage = 'Querys'
    TargetDatabase = dbTarget
    TargetTableName = 'QUERYS'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = QuerysMoveExcludedFields
    Left = 64
    Top = 142
  end
  object Evento: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into EVENTO (EV_ACTIVO,'
      'EV_ALTA,'
      'EV_ANT_FIN,'
      'EV_ANT_INI,'
      'EV_AUTOSAL,'
      'EV_TABULA,'
      'EV_BAJA,'
      'EV_CLASIFI,'
      'EV_CODIGO,'
      'EV_CONTRAT,'
      'EV_DESCRIP,'
      'EV_FEC_BSS,'
      'EV_FILTRO,'
      'EV_KARDEX,'
      'EV_MOT_BAJ,'
      'EV_OTRAS_1,'
      'EV_OTRAS_2,'
      'EV_OTRAS_3,'
      'EV_OTRAS_4,'
      'EV_OTRAS_5,'
      'EV_PATRON,'
      'EV_NOMNUME,'
      'EV_PRIORID,'
      'EV_PUESTO,'
      'EV_QUERY,'
      'EV_SALARIO,'
      'EV_TABLASS,'
      'EV_NIVEL1,'
      'EV_NIVEL2,'
      'EV_NIVEL3,'
      'EV_NIVEL4,'
      'EV_NIVEL5,'
      'EV_NIVEL6,'
      'EV_NIVEL7,'
      'EV_NIVEL8,'
      'EV_NIVEL9,'
      'EV_TURNO,'
      'EV_INCLUYE,'
      'EV_NOMTIPO,'
      'EV_NOMYEAR,'
      'EV_M_ANTIG,'
      'EV_CAMPO,'
      'EV_FORMULA,'
      'EV_FEC_CON ) values (:EV_ACTIVO,'
      ':EV_ALTA,'
      ':EV_ANT_FIN,'
      ':EV_ANT_INI,'
      ':EV_AUTOSAL,'
      ':EV_TABULA,'
      ':EV_BAJA,'
      ':EV_CLASIFI,'
      ':EV_CODIGO,'
      ':EV_CONTRAT,'
      ':EV_DESCRIP,'
      ':EV_FEC_BSS,'
      ':EV_FILTRO,'
      ':EV_KARDEX,'
      ':EV_MOT_BAJ,'
      ':EV_OTRAS_1,'
      ':EV_OTRAS_2,'
      ':EV_OTRAS_3,'
      ':EV_OTRAS_4,'
      ':EV_OTRAS_5,'
      ':EV_PATRON,'
      ':EV_NOMNUME,'
      ':EV_PRIORID,'
      ':EV_PUESTO,'
      ':EV_QUERY,'
      ':EV_SALARIO,'
      ':EV_TABLASS,'
      ':EV_NIVEL1,'
      ':EV_NIVEL2,'
      ':EV_NIVEL3,'
      ':EV_NIVEL4,'
      ':EV_NIVEL5,'
      ':EV_NIVEL6,'
      ':EV_NIVEL7,'
      ':EV_NIVEL8,'
      ':EV_NIVEL9,'
      ':EV_TURNO,'
      ':EV_INCLUYE,'
      ':EV_NOMTIPO,'
      ':EV_NOMYEAR,'
      ':EV_M_ANTIG,'
      ':EV_CAMPO,'
      ':EV_FORMULA,'
      ':EV_FEC_CON )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from EVENTO.DBF')
    StartMessage = 'Eventos'
    TargetDatabase = dbTarget
    TargetTableName = 'EVENTO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = EventoMoveExcludedFields
    Left = 112
    Top = 142
  end
  object Contrato: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CONTRATO (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_DIAS) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_DIAS)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      
        'select * from TABLAS.DBF where (TB_TABLA= '#39'09'#39') and (TB_CODIGO<>' +
        #39#39')')
    StartMessage = 'Contratos'
    TargetDatabase = dbTarget
    TargetTableName = 'CONTRATO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = ContratoMoveExcludedFields
    Left = 161
    Top = 142
  end
  object Horario: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into HORARIO (HO_CODIGO,'
      'HO_ADD_EAT,'
      'HO_CHK_EAT,'
      'HO_COMER,'
      'HO_DESCRIP,'
      'HO_DOBLES,'
      'HO_EXT_FIJ,'
      'HO_IGN_EAT,'
      'HO_IN_EAT,'
      'HO_IN_TARD,'
      'HO_IN_TEMP,'
      'HO_INTIME,'
      'HO_LASTOUT,'
      'HO_MIN_EAT,'
      'HO_JORNADA,'
      'HO_OU_TARD,'
      'HO_OU_TEMP,'
      'HO_OUT_EAT,'
      'HO_OUTTIME,'
      'HO_TIPO,'
      'HO_EXT_COM,'
      'HO_EXT_MIN,'
      'HO_OUT_BRK,'
      'HO_IN_BRK ) values (:HO_CODIGO,'
      ':HO_ADD_EAT,'
      ':HO_CHK_EAT,'
      ':HO_COMER,'
      ':HO_DESCRIP,'
      ':HO_DOBLES,'
      ':HO_EXT_FIJ,'
      ':HO_IGN_EAT,'
      ':HO_IN_EAT,'
      ':HO_IN_TARD,'
      ':HO_IN_TEMP,'
      ':HO_INTIME,'
      ':HO_LASTOUT,'
      ':HO_MIN_EAT,'
      ':HO_JORNADA,'
      ':HO_OU_TARD,'
      ':HO_OU_TEMP,'
      ':HO_OUT_EAT,'
      ':HO_OUTTIME,'
      ':HO_TIPO,'
      ':HO_EXT_COM,'
      ':HO_EXT_MIN,'
      ':HO_OUT_BRK,'
      ':HO_IN_BRK )'
      '')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from HORARIO.DBF')
    StartMessage = 'Horarios'
    TargetDatabase = dbTarget
    TargetTableName = 'HORARIO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = HorarioMoveExcludedFields
    Left = 214
    Top = 142
  end
  object Turno: TZetaMigrar
    AdditionalRows = 1
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into TURNO (TU_CODIGO,'
      'TU_DESCRIP,'
      'TU_DIAS,'
      'TU_DOBLES,'
      'TU_DOMINGO,'
      'TU_FESTIVO,'
      'TU_HOR_1,'
      'TU_HOR_2,'
      'TU_HOR_3,'
      'TU_HOR_4,'
      'TU_HOR_5,'
      'TU_HOR_6,'
      'TU_HOR_7,'
      'TU_HORARIO,'
      'TU_JORNADA,'
      'TU_NOMINA,'
      'TU_RIT_INI,'
      'TU_RIT_PAT,'
      'TU_TIP_1,'
      'TU_TIP_2,'
      'TU_TIP_3,'
      'TU_TIP_4,'
      'TU_TIP_5,'
      'TU_TIP_6,'
      'TU_TIP_7,'
      'TU_TIP_JOR,'
      'TU_VACA_DE,'
      'TU_VACA_SA,'
      'TU_VACA_HA ) values (:TU_CODIGO,'
      ':TU_DESCRIP,'
      ':TU_DIAS,'
      ':TU_DOBLES,'
      ':TU_DOMINGO,'
      ':TU_FESTIVO,'
      ':TU_HOR_1,'
      ':TU_HOR_2,'
      ':TU_HOR_3,'
      ':TU_HOR_4,'
      ':TU_HOR_5,'
      ':TU_HOR_6,'
      ':TU_HOR_7,'
      ':TU_HORARIO,'
      ':TU_JORNADA,'
      ':TU_NOMINA,'
      ':TU_RIT_INI,'
      ':TU_RIT_PAT,'
      ':TU_TIP_1,'
      ':TU_TIP_2,'
      ':TU_TIP_3,'
      ':TU_TIP_4,'
      ':TU_TIP_5,'
      ':TU_TIP_6,'
      ':TU_TIP_7,'
      ':TU_TIP_JOR,'
      ':TU_VACA_DE,'
      ':TU_VACA_SA,'
      ':TU_VACA_HA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TURNO.DBF')
    StartMessage = 'Turnos'
    TargetDatabase = dbTarget
    TargetTableName = 'TURNO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnAdditionalRows = TurnoAdditionalRows
    OnMoveExcludedFields = TurnoMoveExcludedFields
    Left = 264
    Top = 142
  end
  object Festivo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into FESTIVO (TU_CODIGO,'
      'FE_MES,'
      'FE_DIA,'
      'FE_DESCRIP,'
      'FE_TIPO,'
      'FE_CAMBIO) values (:TU_CODIGO,'
      ':FE_MES,'
      ':FE_DIA,'
      ':FE_DESCRIP,'
      ':FE_TIPO,'
      ':FE_CAMBIO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from FESTIVO.DBF')
    StartMessage = 'D'#237'as Festivos'
    TargetDatabase = dbTarget
    TargetTableName = 'FESTIVO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = FestivoMoveExcludedFields
    Left = 312
    Top = 142
  end
  object Concepto: TZetaMigrar
    AdditionalRows = 10
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from CONCEPTO')
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CONCEPTO (CO_A_PTU,'
      'CO_ACTIVO,'
      'CO_CALCULA,'
      'CO_DESCRIP,'
      'CO_FORMULA,'
      'CO_G_IMSS,'
      'CO_G_ISPT,'
      'CO_IMP_CAL,'
      'CO_IMPRIME,'
      'CO_LISTADO,'
      'CO_MENSUAL,'
      'CO_NUMERO,'
      'CO_QUERY,'
      'CO_RECIBO,'
      'CO_TIPO,'
      'CO_X_ISPT) values (:CO_A_PTU,'
      ':CO_ACTIVO,'
      ':CO_CALCULA,'
      ':CO_DESCRIP,'
      ':CO_FORMULA,'
      ':CO_G_IMSS,'
      ':CO_G_ISPT,'
      ':CO_IMP_CAL,'
      ':CO_IMPRIME,'
      ':CO_LISTADO,'
      ':CO_MENSUAL,'
      ':CO_NUMERO,'
      ':CO_QUERY,'
      ':CO_RECIBO,'
      ':CO_TIPO,'
      ':CO_X_ISPT)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CONCEPTO.DBF')
    StartMessage = 'Conceptos'
    TargetDatabase = dbTarget
    TargetTableName = 'CONCEPTO'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnAdditionalRows = ConceptoAdditionalRows
    OnFilterRecord = ConceptoFilterRecord
    OnMoveExcludedFields = ConceptoMoveExcludedFields
    Left = 360
    Top = 142
  end
  object TAhorro: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into TAHORRO (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_CONCEPT,'
      'TB_RELATIV,'
      'TB_LIQUIDA,'
      'TB_ALTA) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_CONCEPT,'
      ':TB_RELATIV,'
      ':TB_LIQUIDA,'
      ':TB_ALTA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '( TB_TABLA = '#39'40'#39' ) and'
      '( TB_CODIGO <>'#39#39' ) and'
      '( TB_NUMERO <>0 )')
    StartMessage = 'Tabla de Ahorros'
    TargetDatabase = dbTarget
    TargetTableName = 'TAHORRO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = TAhorroMoveExcludedFields
    Left = 408
    Top = 142
  end
  object TPresta: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into TPRESTA (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_CONCEPT,'
      'TB_RELATIV,'
      'TB_LIQUIDA,'
      'TB_ALTA) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_CONCEPT,'
      ':TB_RELATIV,'
      ':TB_LIQUIDA,'
      ':TB_ALTA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '( TB_TABLA = '#39'40'#39' ) and'
      '( TB_CODIGO <>'#39#39' ) and'
      '( TB_NUMERO = 0 )')
    StartMessage = 'Tabla de Pr'#233'stamos'
    TargetDatabase = dbTarget
    TargetTableName = 'TPRESTA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = TPrestaCloseDatasets
    OnMoveExcludedFields = TPrestaMoveExcludedFields
    Left = 454
    Top = 142
  end
  object TKardex: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from TKARDEX where TB_SISTEMA = '#39'S'#39)
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into TKARDEX (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_NIVEL,'
      'TB_SISTEMA) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_NIVEL,'
      ':TB_SISTEMA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'16'#39') and'
      '(TB_CODIGO<>'#39#39') and'
      '( TB_CODIGO <> '#39'INCAPA'#39' ) and'
      '( TB_CODIGO <> '#39'PER_CG'#39' ) and'
      '( TB_CODIGO <> '#39'PER_SG'#39' ) and'
      '( TB_CODIGO <> '#39'VAC   '#39' )')
    StartMessage = 'Tabla de Tipos de Kardex'
    TargetDatabase = dbTarget
    TargetTableName = 'TKARDEX'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = TKardexMoveExcludedFields
    Left = 502
    Top = 142
  end
  object Estudios: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into ESTUDIOS (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS where (TB_TABLA= '#39'05'#39') AND (TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Estudios'
    TargetDatabase = dbTarget
    TargetTableName = 'ESTUDIOS'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 16
    Top = 188
  end
  object Inciden: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from INCIDEN where TB_SISTEMA = '#39'S'#39)
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into INCIDEN (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_INCIDEN,'
      'TB_INCAPA) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_INCIDEN,'
      ':TB_INCAPA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'12'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Incidencias'
    TargetDatabase = dbTarget
    TargetTableName = 'INCIDEN'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = IncidenMoveExcludedFields
    Left = 64
    Top = 188
  end
  object RPatron: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into RPATRON (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_NUMREG,'
      'TB_MODULO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_NUMREG,'
      ':TB_MODULO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where'
      '(TB_TABLA= '#39'46'#39') and'
      '(TB_CODIGO<>'#39#39')')
    StartMessage = 'Registros Patronales'
    TargetDatabase = dbTarget
    TargetTableName = 'RPATRON'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = RPatronMoveExcludedFields
    Left = 112
    Top = 188
  end
  object Transpor: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into TRANSPOR (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS where (TB_TABLA= '#39'04'#39') AND (TB_CODIGO<>'#39#39')')
    StartMessage = 'Tabla de Medios de Transporte'
    TargetDatabase = dbTarget
    TargetTableName = 'TRANSPOR'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Extra1MoveExcludedFields
    Left = 161
    Top = 188
  end
  object PRiesgo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PRIESGO (TB_CODIGO,'
      'RT_FECHA,'
      'RT_PRIMA) values (:TB_CODIGO,'
      ':RT_FECHA,'
      ':RT_PRIMA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA= '#39'46'#39') and'
      '(TB_CODIGO<>'#39#39')'
      '')
    StartMessage = 'Primas de Riesgo'
    TargetDatabase = dbTarget
    TargetTableName = 'PRIESGO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = PRiesgoCloseDatasets
    OnMoveExcludedFields = PRiesgoMoveExcludedFields
    OnOpenDatasets = PRiesgoOpenDatasets
    Left = 214
    Top = 188
  end
  object SSocial: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into SSOCIAL (TB_CODIGO,'
      'TB_ELEMENT,'
      'TB_INGLES,'
      'TB_NUMERO,'
      'TB_TEXTO,'
      'TB_PRIMAVA,'
      'TB_DIAS_AG,'
      'TB_PAGO_7,'
      'TB_PRIMA_7,'
      'TB_PRIMADO,'
      'TB_DIAS_AD) values (:TB_CODIGO,'
      ':TB_ELEMENT,'
      ':TB_INGLES,'
      ':TB_NUMERO,'
      ':TB_TEXTO,'
      ':TB_PRIMAVA,'
      ':TB_DIAS_AG,'
      ':TB_PAGO_7,'
      ':TB_PRIMA_7,'
      ':TB_PRIMADO,'
      ':TB_DIAS_AD)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select T1.TB_CODIGO, T1.TB_ELEMENT, T1.TB_INGLES,'
      'T2.TB_LETRA as T2_LETRA, T2.TB_NUMERO as T2_NUMERO,'
      'T2.TB_ELEMENT as T2_ELEMENT from'
      'TABLAS.DBF T1, TABLAS.DBF T2 where'
      '(T1.TB_TABLA='#39'17'#39') and (T1.TB_CODIGO<>'#39#39') and'
      '(T2.TB_CODIGO='#39#39') and'
      '(T2.TB_TABLA='#39'9'#39'||T1.TB_CODIGO)')
    StartMessage = 'Seguro Social'
    TargetDatabase = dbTarget
    TargetTableName = 'SSOCIAL'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = SSocialMoveExcludedFields
    Left = 264
    Top = 188
  end
  object Prestaci: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PRESTACI (TB_CODIGO,'
      'PT_YEAR,'
      'PT_DIAS_VA,'
      'PT_PRIMAVA,'
      'PT_DIAS_AG,'
      'PT_DIAS_AD,'
      'PT_PRIMADO,'
      'PT_PAGO_7,'
      'PT_PRIMA_7,'
      'PT_FACTOR) values (:TB_CODIGO,'
      ':PT_YEAR,'
      ':PT_DIAS_VA,'
      ':PT_PRIMAVA,'
      ':PT_DIAS_AG,'
      ':PT_DIAS_AD,'
      ':PT_PRIMADO,'
      ':PT_PAGO_7,'
      ':PT_PRIMA_7,'
      ':PT_FACTOR)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from TABLAS.DBF where '
      '(TB_TABLA like '#39'9%'#39') and'
      '( TB_CODIGO<>'#39#39' )')
    StartMessage = 'Prestaciones'
    TargetDatabase = dbTarget
    TargetTableName = 'PRESTACI'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = PrestaciCloseDatasets
    OnMoveExcludedFields = PrestaciMoveExcludedFields
    OnOpenDatasets = PrestaciOpenDatasets
    Left = 312
    Top = 188
  end
  object Colabora: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into COLABORA (CB_CODIGO,'
      'CB_ACTIVO,'
      'CB_APE_MAT,'
      'CB_APE_PAT,'
      'CB_AUTOSAL,'
      'CB_BAN_ELE,'
      'CB_CARRERA,'
      'CB_CHECA,'
      'CB_CIUDAD,'
      'CB_CLASIFI,'
      'CB_CODPOST,'
      'CB_CONTRAT,'
      'CB_CREDENC,'
      'CB_CURP,'
      'CB_CALLE,'
      'CB_COLONIA,'
      'CB_EDO_CIV,'
      'CB_FEC_RES,'
      'CB_EST_HOR,'
      'CB_EST_HOY,'
      'CB_ESTADO,'
      'CB_ESTUDIO,'
      'CB_EVALUA,'
      'CB_EXPERIE,'
      'CB_FEC_ANT,'
      'CB_FEC_BAJ,'
      'CB_FEC_BSS,'
      'CB_FEC_CON,'
      'CB_FEC_ING,'
      'CB_FEC_INT,'
      'CB_FEC_NAC,'
      'CB_FEC_REV,'
      'CB_FEC_VAC,'
      'CB_G_FEC_1,'
      'CB_G_FEC_2,'
      'CB_G_FEC_3,'
      'CB_G_LOG_1,'
      'CB_G_LOG_2,'
      'CB_G_LOG_3,'
      'CB_G_NUM_1,'
      'CB_G_NUM_2,'
      'CB_G_NUM_3,'
      'CB_G_TAB_1,'
      'CB_G_TAB_2,'
      'CB_G_TAB_3,'
      'CB_G_TAB_4,'
      'CB_G_TEX_1,'
      'CB_G_TEX_2,'
      'CB_G_TEX_3,'
      'CB_G_TEX_4,'
      'CB_HABLA,'
      'CB_TURNO,'
      'CB_IDIOMA,'
      'CB_INFCRED,'
      'CB_INFMANT,'
      'CB_INFTASA,'
      'CB_INF_INI,'
      'CB_CLINICA,'
      'CB_LA_MAT,'
      'CB_LAST_EV,'
      'CB_LUG_NAC,'
      'CB_MAQUINA,'
      'CB_MED_TRA,'
      'CB_PASAPOR,'
      'CB_FEC_INC,'
      'CB_FEC_PER,'
      'CB_NOMYEAR,'
      'CB_NACION,'
      'CB_NOMTIPO,'
      'CB_NEXT_EV,'
      'CB_NOMNUME,'
      'CB_NOMBRES,'
      'CB_PATRON,'
      'CB_PUESTO,'
      'CB_RFC,'
      'CB_SAL_INT,'
      'CB_SALARIO,'
      'CB_SEGSOC,'
      'CB_SEXO,'
      'CB_TABLASS,'
      'CB_TEL,'
      'CB_VIVECON,'
      'CB_VIVEEN,'
      'CB_ZONA,'
      'CB_ZONA_GE,'
      'CB_NIVEL1,'
      'CB_NIVEL2,'
      'CB_NIVEL3,'
      'CB_NIVEL4,'
      'CB_NIVEL5,'
      'CB_NIVEL6,'
      'CB_NIVEL7,'
      'CB_INFTIPO,'
      'CB_NIVEL8,'
      'CB_NIVEL9,'
      'CB_DER_FEC,'
      'CB_DER_PAG,'
      'CB_V_PAGO,'
      'CB_DER_GOZ,'
      'CB_V_GOZO,'
      'CB_TIP_REV,'
      'CB_MOT_BAJ,'
      'CB_FEC_SAL,'
      'CB_INF_OLD,'
      'CB_OLD_SAL,'
      'CB_OLD_INT,'
      'CB_TOT_GRA,'
      'CB_SAL_TOT,'
      'CB_PER_VAR,'
      'CB_PRE_INT,'
      'CB_FAC_INT,'
      'CB_RANGO_S )'
      'values (:CB_CODIGO,'
      ':CB_ACTIVO,'
      ':CB_APE_MAT,'
      ':CB_APE_PAT,'
      ':CB_AUTOSAL,'
      ':CB_BAN_ELE,'
      ':CB_CARRERA,'
      ':CB_CHECA,'
      ':CB_CIUDAD,'
      ':CB_CLASIFI,'
      ':CB_CODPOST,'
      ':CB_CONTRAT,'
      ':CB_CREDENC,'
      ':CB_CURP,'
      ':CB_CALLE,'
      ':CB_COLONIA,'
      ':CB_EDO_CIV,'
      ':CB_FEC_RES,'
      ':CB_EST_HOR,'
      ':CB_EST_HOY,'
      ':CB_ESTADO,'
      ':CB_ESTUDIO,'
      ':CB_EVALUA,'
      ':CB_EXPERIE,'
      ':CB_FEC_ANT,'
      ':CB_FEC_BAJ,'
      ':CB_FEC_BSS,'
      ':CB_FEC_CON,'
      ':CB_FEC_ING,'
      ':CB_FEC_INT,'
      ':CB_FEC_NAC,'
      ':CB_FEC_REV,'
      ':CB_FEC_VAC,'
      ':CB_G_FEC_1,'
      ':CB_G_FEC_2,'
      ':CB_G_FEC_3,'
      ':CB_G_LOG_1,'
      ':CB_G_LOG_2,'
      ':CB_G_LOG_3,'
      ':CB_G_NUM_1,'
      ':CB_G_NUM_2,'
      ':CB_G_NUM_3,'
      ':CB_G_TAB_1,'
      ':CB_G_TAB_2,'
      ':CB_G_TAB_3,'
      ':CB_G_TAB_4,'
      ':CB_G_TEX_1,'
      ':CB_G_TEX_2,'
      ':CB_G_TEX_3,'
      ':CB_G_TEX_4,'
      ':CB_HABLA,'
      ':CB_TURNO,'
      ':CB_IDIOMA,'
      ':CB_INFCRED,'
      ':CB_INFMANT,'
      ':CB_INFTASA,'
      ':CB_INF_INI,'
      ':CB_CLINICA,'
      ':CB_LA_MAT,'
      ':CB_LAST_EV,'
      ':CB_LUG_NAC,'
      ':CB_MAQUINA,'
      ':CB_MED_TRA,'
      ':CB_PASAPOR,'
      ':CB_FEC_INC,'
      ':CB_FEC_PER,'
      ':CB_NOMYEAR,'
      ':CB_NACION,'
      ':CB_NOMTIPO,'
      ':CB_NEXT_EV,'
      ':CB_NOMNUME,'
      ':CB_NOMBRES,'
      ':CB_PATRON,'
      ':CB_PUESTO,'
      ':CB_RFC,'
      ':CB_SAL_INT,'
      ':CB_SALARIO,'
      ':CB_SEGSOC,'
      ':CB_SEXO,'
      ':CB_TABLASS,'
      ':CB_TEL,'
      ':CB_VIVECON,'
      ':CB_VIVEEN,'
      ':CB_ZONA,'
      ':CB_ZONA_GE,'
      ':CB_NIVEL1,'
      ':CB_NIVEL2,'
      ':CB_NIVEL3,'
      ':CB_NIVEL4,'
      ':CB_NIVEL5,'
      ':CB_NIVEL6,'
      ':CB_NIVEL7,'
      ':CB_INFTIPO,'
      ':CB_NIVEL8,'
      ':CB_NIVEL9,'
      ':CB_DER_FEC,'
      ':CB_DER_PAG,'
      ':CB_V_PAGO,'
      ':CB_DER_GOZ,'
      ':CB_V_GOZO,'
      ':CB_TIP_REV,'
      ':CB_MOT_BAJ,'
      ':CB_FEC_SAL,'
      ':CB_INF_OLD,'
      ':CB_OLD_SAL,'
      ':CB_OLD_INT,'
      ':CB_TOT_GRA,'
      ':CB_SAL_TOT,'
      ':CB_PER_VAR,'
      ':CB_PRE_INT,'
      ':CB_FAC_INT,'
      ':CB_RANGO_S)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from COLABORA%0:s.DBF')
    StartMessage = 'Empleados'
    TargetDatabase = dbTarget
    TargetTableName = 'COLABORA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = ColaboraCloseDatasets
    OnMoveExcludedFields = ColaboraMoveExcludedFields
    OnMoveOtherTables = ColaboraMoveOtherTables
    OnOpenDatasets = ColaboraOpenDatasets
    Left = 360
    Top = 188
  end
  object KarCurso: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into KARCURSO (CB_CODIGO,'
      'CU_CODIGO,'
      'CB_CLASIFI,'
      'CB_TURNO,'
      'KC_EVALUA,'
      'KC_FEC_TOM,'
      'KC_HORAS,'
      'CB_PUESTO,'
      'CB_NIVEL1,'
      'CB_NIVEL2,'
      'CB_NIVEL3,'
      'CB_NIVEL4,'
      'CB_NIVEL5,'
      'CB_NIVEL6,'
      'CB_NIVEL7,'
      'CB_NIVEL8,'
      'CB_NIVEL9,'
      'MA_CODIGO) values (:CB_CODIGO,'
      ':CU_CODIGO,'
      ':CB_CLASIFI,'
      ':CB_TURNO,'
      ':KC_EVALUA,'
      ':KC_FEC_TOM,'
      ':KC_HORAS,'
      ':CB_PUESTO,'
      ':CB_NIVEL1,'
      ':CB_NIVEL2,'
      ':CB_NIVEL3,'
      ':CB_NIVEL4,'
      ':CB_NIVEL5,'
      ':CB_NIVEL6,'
      ':CB_NIVEL7,'
      ':CB_NIVEL8,'
      ':CB_NIVEL9,'
      ':MA_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from KARCURSO%0:s.DBF')
    StartMessage = 'Cursos Programados'
    TargetDatabase = dbTarget
    TargetTableName = 'KARCURSO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnFilterRecord = KarCursoFilterRecord
    OnMoveExcludedFields = KarCursoMoveExcludedFields
    Left = 408
    Top = 188
  end
  object Kardex: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into KARDEX (CB_CODIGO,'
      'CB_FECHA,'
      'CB_TIPO,'
      'CB_AUTOSAL,'
      'CB_CLASIFI,'
      'CB_COMENTA,'
      'CB_CONTRAT,'
      'CB_FEC_ING,'
      'CB_FEC_ANT,'
      'CB_FAC_INT,'
      'CB_FEC_CAP,'
      'CB_FEC_CON,'
      'CB_FEC_INT,'
      'CB_FEC_REV,'
      'CB_FECHA_2,'
      'CB_GLOBAL,'
      'CB_TURNO,'
      'CB_MONTO,'
      'CB_MOT_BAJ,'
      'CB_NIVEL,'
      'CB_OLD_INT,'
      'CB_OLD_SAL,'
      'CB_OTRAS_P,'
      'CB_PATRON,'
      'CB_PER_VAR,'
      'CB_PRE_INT,'
      'CB_PUESTO,'
      'CB_RANGO_S,'
      'CB_SAL_INT,'
      'CB_SAL_TOT,'
      'CB_SALARIO,'
      'CB_STATUS,'
      'CB_TABLASS,'
      'CB_TOT_GRA,'
      'US_CODIGO,'
      'CB_ZONA_GE,'
      'CB_NIVEL1,'
      'CB_NIVEL2,'
      'CB_NIVEL3,'
      'CB_NIVEL4,'
      'CB_NIVEL5,'
      'CB_NIVEL6,'
      'CB_NIVEL7,'
      'CB_NIVEL8,'
      'CB_NIVEL9,'
      'CB_NOMTIPO,'
      'CB_NOMYEAR,'
      'CB_NOMNUME,'
      'CB_REINGRE,'
      'CB_NOTA) values (:CB_CODIGO,'
      ':CB_FECHA,'
      ':CB_TIPO,'
      ':CB_AUTOSAL,'
      ':CB_CLASIFI,'
      ':CB_COMENTA,'
      ':CB_CONTRAT,'
      ':CB_FEC_ING,'
      ':CB_FEC_ANT,'
      ':CB_FAC_INT,'
      ':CB_FEC_CAP,'
      ':CB_FEC_CON,'
      ':CB_FEC_INT,'
      ':CB_FEC_REV,'
      ':CB_FECHA_2,'
      ':CB_GLOBAL,'
      ':CB_TURNO,'
      ':CB_MONTO,'
      ':CB_MOT_BAJ,'
      ':CB_NIVEL,'
      ':CB_OLD_INT,'
      ':CB_OLD_SAL,'
      ':CB_OTRAS_P,'
      ':CB_PATRON,'
      ':CB_PER_VAR,'
      ':CB_PRE_INT,'
      ':CB_PUESTO,'
      ':CB_RANGO_S,'
      ':CB_SAL_INT,'
      ':CB_SAL_TOT,'
      ':CB_SALARIO,'
      ':CB_STATUS,'
      ':CB_TABLASS,'
      ':CB_TOT_GRA,'
      ':US_CODIGO,'
      ':CB_ZONA_GE,'
      ':CB_NIVEL1,'
      ':CB_NIVEL2,'
      ':CB_NIVEL3,'
      ':CB_NIVEL4,'
      ':CB_NIVEL5,'
      ':CB_NIVEL6,'
      ':CB_NIVEL7,'
      ':CB_NIVEL8,'
      ':CB_NIVEL9,'
      ':CB_NOMTIPO,'
      ':CB_NOMYEAR,'
      ':CB_NOMNUME,'
      ':CB_REINGRE,'
      ':CB_NOTA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from C_ALTAS%0:s.DBF')
    StartMessage = 'Kardex'
    TargetDatabase = dbTarget
    TargetTableName = 'KARDEX'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = KardexCloseDatasets
    OnFilterRecord = KardexFilterRecord
    OnMoveExcludedFields = KardexMoveExcludedFields
    OnOpenDatasets = KardexOpenDatasets
    Left = 454
    Top = 188
  end
  object Kar_Fija: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into KAR_FIJA ( '
      'CB_CODIGO,'
      'CB_FECHA,'
      'CB_TIPO,'
      'KF_FOLIO,'
      'KF_CODIGO,'
      'KF_MONTO,'
      'KF_GRAVADO,'
      'KF_IMSS) values ('
      ':CB_CODIGO,'
      ':CB_FECHA,'
      ':CB_TIPO,'
      ':KF_FOLIO,'
      ':KF_CODIGO,'
      ':KF_MONTO,'
      ':KF_GRAVADO,'
      ':KF_IMSS)')
    Multiples = 5
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from C_ALTAS%0:s.DBF')
    StartMessage = 'Kardex Fijos'
    TargetDatabase = dbTarget
    TargetTableName = 'KAR_FIJA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCloseDatasets = Kar_FijaCloseDatasets
    OnFilterRecord = Kar_FijaFilterRecord
    OnMoveExcludedFields = Kar_FijaMoveExcludedFields
    OnOpenDatasets = Kar_FijaOpenDatasets
    Left = 502
    Top = 188
  end
  object Periodo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PERIODO (PE_YEAR,'
      'PE_TIPO,'
      'PE_NUMERO,'
      'PE_DESCRIP,'
      'PE_USO,'
      'PE_STATUS,'
      'PE_INC_BAJ,'
      'PE_SOLO_EX,'
      'PE_FEC_INI,'
      'PE_FEC_FIN,'
      'PE_ASI_INI,'
      'PE_ASI_FIN,'
      'PE_FEC_PAG,'
      'PE_MES,'
      'PE_DIAS,'
      'PE_DIAS_AC,'
      'PE_DIA_MES,'
      'PE_POS_MES,'
      'PE_PER_MES,'
      'PE_PER_TOT,'
      'PE_FEC_MOD,'
      'PE_AHORRO,'
      'PE_PRESTAM,'
      'PE_NUM_EMP,'
      'PE_TOT_PER,'
      'PE_TOT_NET,'
      'PE_TOT_DED,'
      'US_CODIGO) values (:PE_YEAR,'
      ':PE_TIPO,'
      ':PE_NUMERO,'
      ':PE_DESCRIP,'
      ':PE_USO,'
      ':PE_STATUS,'
      ':PE_INC_BAJ,'
      ':PE_SOLO_EX,'
      ':PE_FEC_INI,'
      ':PE_FEC_FIN,'
      ':PE_ASI_INI,'
      ':PE_ASI_FIN,'
      ':PE_FEC_PAG,'
      ':PE_MES,'
      ':PE_DIAS,'
      ':PE_DIAS_AC,'
      ':PE_DIA_MES,'
      ':PE_POS_MES,'
      ':PE_PER_MES,'
      ':PE_PER_TOT,'
      ':PE_FEC_MOD,'
      ':PE_AHORRO,'
      ':PE_PRESTAM,'
      ':PE_NUM_EMP,'
      ':PE_TOT_PER,'
      ':PE_TOT_NET,'
      ':PE_TOT_DED,'
      ':US_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from NOMINA%0:s.DBF')
    StartMessage = 'Per'#237'odos'
    TargetDatabase = dbTarget
    TargetTableName = 'PERIODO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = PeriodoCloseDatasets
    OnFilterRecord = PeriodoFilterRecord
    OnMoveExcludedFields = PeriodoMoveExcludedFields
    Left = 16
    Top = 233
  end
  object Incapaci: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into INCAPACI (CB_CODIGO,'
      'IN_FEC_INI,'
      'IN_COMENTA,'
      'IN_DIAS,'
      'IN_FEC_FIN,'
      'IN_FIN,'
      'IN_NUMERO,'
      'IN_TASA_IP,'
      'IN_CAPTURA,'
      'IN_TIPO,'
      'US_CODIGO,'
      'IN_MOTIVO) values (:CB_CODIGO,'
      ':IN_FEC_INI,'
      ':IN_COMENTA,'
      ':IN_DIAS,'
      ':IN_FEC_FIN,'
      ':IN_FIN,'
      ':IN_NUMERO,'
      ':IN_TASA_IP,'
      ':IN_CAPTURA,'
      ':IN_TIPO,'
      ':US_CODIGO,'
      ':IN_MOTIVO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from INC_KAR%0:s.DBF')
    StartMessage = 'Incapacidades'
    TargetDatabase = dbTarget
    TargetTableName = 'INCAPACI'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnFilterRecord = IncapaciFilterRecord
    OnMoveExcludedFields = IncapaciMoveExcludedFields
    Left = 64
    Top = 233
  end
  object Permiso: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PERMISO (CB_CODIGO,'
      'PM_FEC_INI,'
      'PM_CLASIFI,'
      'PM_COMENTA,'
      'PM_DIAS,'
      'PM_FEC_FIN,'
      'PM_CAPTURA,'
      'US_CODIGO,'
      'PM_TIPO,'
      'PM_NUMERO) values (:CB_CODIGO,'
      ':PM_FEC_INI,'
      ':PM_CLASIFI,'
      ':PM_COMENTA,'
      ':PM_DIAS,'
      ':PM_FEC_FIN,'
      ':PM_CAPTURA,'
      ':US_CODIGO,'
      ':PM_TIPO,'
      ':PM_NUMERO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from INC_KAR%0:s.DBF')
    StartMessage = 'Permisos'
    TargetDatabase = dbTarget
    TargetTableName = 'PERMISO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnFilterRecord = PermisoFilterRecord
    OnMoveExcludedFields = PermisoMoveExcludedFields
    Left = 112
    Top = 233
  end
  object Vacacion: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into VACACION (CB_CODIGO,'
      'VA_FEC_INI,'
      'VA_TIPO,'
      'VA_FEC_FIN,'
      'VA_COMENTA,'
      'VA_CAPTURA,'
      'US_CODIGO,'
      'VA_D_PAGO,'
      'VA_PAGO,'
      'VA_S_PAGO,'
      'VA_D_GOZO,'
      'VA_GOZO,'
      'CB_SALARIO,'
      'CB_TABLASS,'
      'VA_NOMYEAR,'
      'VA_NOMTIPO,'
      'VA_NOMNUME,'
      'VA_YEAR,'
      'VA_MONTO,'
      'VA_SEVEN,'
      'VA_TASA_PR,'
      'VA_PRIMA,'
      'VA_OTROS,'
      'VA_TOTAL,'
      'VA_PERIODO,'
      'VA_GLOBAL) values (:CB_CODIGO,'
      ':VA_FEC_INI,'
      ':VA_TIPO,'
      ':VA_FEC_FIN,'
      ':VA_COMENTA,'
      ':VA_CAPTURA,'
      ':US_CODIGO,'
      ':VA_D_PAGO,'
      ':VA_PAGO,'
      ':VA_S_PAGO,'
      ':VA_D_GOZO,'
      ':VA_GOZO,'
      ':CB_SALARIO,'
      ':CB_TABLASS,'
      ':VA_NOMYEAR,'
      ':VA_NOMTIPO,'
      ':VA_NOMNUME,'
      ':VA_YEAR,'
      ':VA_MONTO,'
      ':VA_SEVEN,'
      ':VA_TASA_PR,'
      ':VA_PRIMA,'
      ':VA_OTROS,'
      ':VA_TOTAL,'
      ':VA_PERIODO,'
      ':VA_GLOBAL)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from INC_VACA%0:s.DBF')
    StartMessage = 'Vacaciones'
    TargetDatabase = dbTarget
    TargetTableName = 'VACACION'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = VacacionCloseDatasets
    OnMoveExcludedFields = VacacionMoveExcludedFields
    Left = 161
    Top = 233
  end
  object Liq_IMSS: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into LIQ_IMSS (LS_ACT_AMO,'
      'LS_ACT_APO,'
      'LS_ACT_IMS,'
      'LS_ACT_INF,'
      'LS_ACT_RET,'
      'LS_APO_VOL,'
      'LS_CES_VEJ,'
      'LS_DIAS_BM,'
      'LS_DIAS_CO,'
      'LS_EYM_DIN,'
      'LS_EYM_ESP,'
      'LS_EYM_EXC,'
      'LS_EYM_FIJ,'
      'LS_FAC_ACT,'
      'LS_FAC_REC,'
      'LS_FEC_REC,'
      'LS_GUARDER,'
      'LS_INF_ACR,'
      'LS_INF_AMO,'
      'LS_INF_NAC,'
      'LS_INF_NUM,'
      'LS_INV_VID,'
      'LS_NUM_BIM,'
      'LS_NUM_TRA,'
      'LS_PATRON,'
      'LS_REC_AMO,'
      'LS_REC_APO,'
      'LS_REC_IMS,'
      'LS_REC_INF,'
      'LS_REC_RET,'
      'LS_RETIRO,'
      'LS_RIESGOS,'
      'LS_STATUS,'
      'LS_YEAR,'
      'LS_MONTH,'
      'LS_TIPO,'
      'LS_SUB_IMS,'
      'LS_TOT_IMS,'
      'LS_TOT_MES,'
      'LS_SUB_RET,'
      'LS_TOT_RET,'
      'LS_SUB_INF,'
      'LS_TOT_INF,'
      'LS_IMSS_OB,'
      'LS_IMSS_PA,'
      'US_CODIGO) values (:LS_ACT_AMO,'
      ':LS_ACT_APO,'
      ':LS_ACT_IMS,'
      ':LS_ACT_INF,'
      ':LS_ACT_RET,'
      ':LS_APO_VOL,'
      ':LS_CES_VEJ,'
      ':LS_DIAS_BM,'
      ':LS_DIAS_CO,'
      ':LS_EYM_DIN,'
      ':LS_EYM_ESP,'
      ':LS_EYM_EXC,'
      ':LS_EYM_FIJ,'
      ':LS_FAC_ACT,'
      ':LS_FAC_REC,'
      ':LS_FEC_REC,'
      ':LS_GUARDER,'
      ':LS_INF_ACR,'
      ':LS_INF_AMO,'
      ':LS_INF_NAC,'
      ':LS_INF_NUM,'
      ':LS_INV_VID,'
      ':LS_NUM_BIM,'
      ':LS_NUM_TRA,'
      ':LS_PATRON,'
      ':LS_REC_AMO,'
      ':LS_REC_APO,'
      ':LS_REC_IMS,'
      ':LS_REC_INF,'
      ':LS_REC_RET,'
      ':LS_RETIRO,'
      ':LS_RIESGOS,'
      ':LS_STATUS,'
      ':LS_YEAR,'
      ':LS_MONTH,'
      ':LS_TIPO,'
      ':LS_SUB_IMS,'
      ':LS_TOT_IMS,'
      ':LS_TOT_MES,'
      ':LS_SUB_RET,'
      ':LS_TOT_RET,'
      ':LS_SUB_INF,'
      ':LS_TOT_INF,'
      ':LS_IMSS_OB,'
      ':LS_IMSS_PA,'
      ':US_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from LIQ_IMSS%0:s.DBF')
    StartMessage = 'Liquidaciones del IMSS'
    TargetDatabase = dbTarget
    TargetTableName = 'LIQ_IMSS'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Liq_IMSSMoveExcludedFields
    Left = 214
    Top = 233
  end
  object Liq_Emp: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into LIQ_EMP (LE_APO_VOL,'
      'LE_CES_VEJ,'
      'LE_DIAS_BM,'
      'CB_CODIGO,'
      'LE_DIAS_CO,'
      'LE_EYM_DIN,'
      'LE_EYM_ESP,'
      'LE_EYM_EXC,'
      'LE_EYM_FIJ,'
      'LE_GUARDER,'
      'LE_INF_AMO,'
      'LE_INF_PAT,'
      'LE_INV_VID,'
      'LE_RETIRO,'
      'LE_RIESGOS,'
      'LE_STATUS,'
      'LS_YEAR,'
      'LS_MONTH,'
      'LS_PATRON,'
      'LS_TIPO,'
      'LE_TOT_IMS,'
      'LE_TOT_RET,'
      'LE_TOT_INF,'
      'LE_IMSS_OB,'
      'LE_IMSS_PA) values (:LE_APO_VOL,'
      ':LE_CES_VEJ,'
      ':LE_DIAS_BM,'
      ':CB_CODIGO,'
      ':LE_DIAS_CO,'
      ':LE_EYM_DIN,'
      ':LE_EYM_ESP,'
      ':LE_EYM_EXC,'
      ':LE_EYM_FIJ,'
      ':LE_GUARDER,'
      ':LE_INF_AMO,'
      ':LE_INF_PAT,'
      ':LE_INV_VID,'
      ':LE_RETIRO,'
      ':LE_RIESGOS,'
      ':LE_STATUS,'
      ':LS_YEAR,'
      ':LS_MONTH,'
      ':LS_PATRON,'
      ':LS_TIPO,'
      ':LE_TOT_IMS,'
      ':LE_TOT_RET,'
      ':LE_TOT_INF,'
      ':LE_IMSS_OB,'
      ':LE_IMSS_PA)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from LIQ_EMP%0:s.DBF')
    StartMessage = 'Liquidaciones de Empleados'
    TargetDatabase = dbTarget
    TargetTableName = 'LIQ_EMP'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = Liq_EmpMoveExcludedFields
    Left = 264
    Top = 233
  end
  object Liq_Mov: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into LIQ_MOV (LM_AUSENCI,'
      'LM_BASE,'
      'CB_CODIGO,'
      'LM_CES_VEJ,'
      'LM_CLAVE,'
      'LM_DIAS,'
      'LM_EYM_DIN,'
      'LM_EYM_ESP,'
      'LM_EYM_EXC,'
      'LM_EYM_FIJ,'
      'LM_FECHA,'
      'LM_GUARDER,'
      'LM_INF_AMO,'
      'LM_INV_VID,'
      'LM_KAR_FEC,'
      'LM_KAR_TIP,'
      'LM_RETIRO,'
      'LM_RIESGOS,'
      'LM_TIPO,'
      'LS_PATRON,'
      'LS_YEAR,'
      'LS_MONTH,'
      'LM_INF_PAT,'
      'LS_TIPO,'
      'LM_INCAPAC) values (:LM_AUSENCI,'
      ':LM_BASE,'
      ':CB_CODIGO,'
      ':LM_CES_VEJ,'
      ':LM_CLAVE,'
      ':LM_DIAS,'
      ':LM_EYM_DIN,'
      ':LM_EYM_ESP,'
      ':LM_EYM_EXC,'
      ':LM_EYM_FIJ,'
      ':LM_FECHA,'
      ':LM_GUARDER,'
      ':LM_INF_AMO,'
      ':LM_INV_VID,'
      ':LM_KAR_FEC,'
      ':LM_KAR_TIP,'
      ':LM_RETIRO,'
      ':LM_RIESGOS,'
      ':LM_TIPO,'
      ':LS_PATRON,'
      ':LS_YEAR,'
      ':LS_MONTH,'
      ':LM_INF_PAT,'
      ':LS_TIPO,'
      ':LM_INCAPAC)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from LIQ_MOV%0:s.DBF')
    StartMessage = 'Movimientos de Liquidaciones'
    TargetDatabase = dbTarget
    TargetTableName = 'LIQ_MOV'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = Liq_MovCloseDatasets
    OnMoveExcludedFields = Liq_MovMoveExcludedFields
    OnOpenDatasets = Liq_MovOpenDatasets
    Left = 312
    Top = 233
  end
  object Nomina: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into NOMINA (PE_YEAR,'
      'PE_TIPO,'
      'PE_NUMERO,'
      'CB_CODIGO,'
      'CB_CLASIFI,'
      'CB_TURNO,'
      'CB_PATRON,'
      'CB_PUESTO,'
      'CB_ZONA_GE,'
      'NO_FOLIO_1,'
      'NO_FOLIO_2,'
      'NO_FOLIO_3,'
      'NO_FOLIO_4,'
      'NO_FOLIO_5,'
      'NO_OBSERVA,'
      'NO_STATUS,'
      'US_CODIGO,'
      'NO_USER_RJ,'
      'CB_NIVEL1,'
      'CB_NIVEL2,'
      'CB_NIVEL3,'
      'CB_NIVEL4,'
      'CB_NIVEL5,'
      'CB_NIVEL6,'
      'CB_NIVEL7,'
      'CB_NIVEL8,'
      'CB_NIVEL9,'
      'CB_SAL_INT,'
      'CB_SALARIO,'
      'NO_DIAS,'
      'NO_ADICION,'
      'NO_DEDUCCI,'
      'NO_NETO,'
      'NO_DES_TRA,'
      'NO_DIAS_AG,'
      'NO_DIAS_VA,'
      'NO_DOBLES,'
      'NO_EXTRAS,'
      'NO_FES_PAG,'
      'NO_FES_TRA,'
      'NO_HORA_CG,'
      'NO_HORA_SG,'
      'NO_HORA_PD,'
      'NO_HORAS,'
      'NO_IMP_CAL,'
      'NO_JORNADA,'
      'NO_PER_CAL,'
      'NO_PER_MEN,'
      'NO_PERCEPC,'
      'NO_TARDES,'
      'NO_TRIPLES,'
      'NO_TOT_PRE,'
      'NO_VAC_TRA,'
      'NO_X_CAL,'
      'NO_X_ISPT,'
      'NO_X_MENS,'
      'NO_D_TURNO,'
      'NO_DIAS_AJ,'
      'NO_DIAS_AS,'
      'NO_DIAS_CG,'
      'NO_DIAS_EM,'
      'NO_DIAS_FI,'
      'NO_DIAS_FJ,'
      'NO_DIAS_FV,'
      'NO_DIAS_IN,'
      'NO_DIAS_NT,'
      'NO_DIAS_OT,'
      'NO_DIAS_RE,'
      'NO_DIAS_SG,'
      'NO_DIAS_SS,'
      'NO_DIAS_SU,'
      'NO_LIQUIDA,'
      'NO_FUERA,'
      'NO_FEC_PAG,'
      'NO_USR_PAG) values (:PE_YEAR,'
      ':PE_TIPO,'
      ':PE_NUMERO,'
      ':CB_CODIGO,'
      ':CB_CLASIFI,'
      ':CB_TURNO,'
      ':CB_PATRON,'
      ':CB_PUESTO,'
      ':CB_ZONA_GE,'
      ':NO_FOLIO_1,'
      ':NO_FOLIO_2,'
      ':NO_FOLIO_3,'
      ':NO_FOLIO_4,'
      ':NO_FOLIO_5,'
      ':NO_OBSERVA,'
      ':NO_STATUS,'
      ':US_CODIGO,'
      ':NO_USER_RJ,'
      ':CB_NIVEL1,'
      ':CB_NIVEL2,'
      ':CB_NIVEL3,'
      ':CB_NIVEL4,'
      ':CB_NIVEL5,'
      ':CB_NIVEL6,'
      ':CB_NIVEL7,'
      ':CB_NIVEL8,'
      ':CB_NIVEL9,'
      ':CB_SAL_INT,'
      ':CB_SALARIO,'
      ':NO_DIAS,'
      ':NO_ADICION,'
      ':NO_DEDUCCI,'
      ':NO_NETO,'
      ':NO_DES_TRA,'
      ':NO_DIAS_AG,'
      ':NO_DIAS_VA,'
      ':NO_DOBLES,'
      ':NO_EXTRAS,'
      ':NO_FES_PAG,'
      ':NO_FES_TRA,'
      ':NO_HORA_CG,'
      ':NO_HORA_SG,'
      ':NO_HORA_PD,'
      ':NO_HORAS,'
      ':NO_IMP_CAL,'
      ':NO_JORNADA,'
      ':NO_PER_CAL,'
      ':NO_PER_MEN,'
      ':NO_PERCEPC,'
      ':NO_TARDES,'
      ':NO_TRIPLES,'
      ':NO_TOT_PRE,'
      ':NO_VAC_TRA,'
      ':NO_X_CAL,'
      ':NO_X_ISPT,'
      ':NO_X_MENS,'
      ':NO_D_TURNO,'
      ':NO_DIAS_AJ,'
      ':NO_DIAS_AS,'
      ':NO_DIAS_CG,'
      ':NO_DIAS_EM,'
      ':NO_DIAS_FI,'
      ':NO_DIAS_FJ,'
      ':NO_DIAS_FV,'
      ':NO_DIAS_IN,'
      ':NO_DIAS_NT,'
      ':NO_DIAS_OT,'
      ':NO_DIAS_RE,'
      ':NO_DIAS_SG,'
      ':NO_DIAS_SS,'
      ':NO_DIAS_SU,'
      ':NO_LIQUIDA,'
      ':NO_FUERA,'
      ':NO_FEC_PAG,'
      ':NO_USR_PAG)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from NOMINA%0:s.DBF')
    StartMessage = 'N'#243'mina'
    TargetDatabase = dbTarget
    TargetTableName = 'NOMINA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = NominaCloseDatasets
    OnFilterRecord = NominaFilterRecord
    OnMoveExcludedFields = NominaMoveExcludedFields
    Left = 360
    Top = 233
  end
  object Acumula: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into ACUMULA (AC_MES_01,'
      'AC_YEAR,'
      'AC_MES_02,'
      'CO_NUMERO,'
      'AC_MES_03,'
      'AC_MES_04,'
      'AC_MES_05,'
      'AC_MES_06,'
      'AC_MES_07,'
      'AC_MES_08,'
      'AC_MES_09,'
      'AC_MES_10,'
      'AC_MES_11,'
      'AC_MES_12,'
      'AC_MES_13,'
      'CB_CODIGO,'
      'AC_ANUAL) values (:AC_MES_01,'
      ':AC_YEAR,'
      ':AC_MES_02,'
      ':CO_NUMERO,'
      ':AC_MES_03,'
      ':AC_MES_04,'
      ':AC_MES_05,'
      ':AC_MES_06,'
      ':AC_MES_07,'
      ':AC_MES_08,'
      ':AC_MES_09,'
      ':AC_MES_10,'
      ':AC_MES_11,'
      ':AC_MES_12,'
      ':AC_MES_13,'
      ':CB_CODIGO,'
      ':AC_ANUAL)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from ACUMULA%0:s.DBF')
    StartMessage = 'Acumulados'
    TargetDatabase = dbTarget
    TargetTableName = 'ACUMULA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = AcumulaMoveExcludedFields
    Left = 408
    Top = 233
  end
  object Faltas: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into FALTAS (PE_YEAR,'
      'PE_TIPO,'
      'PE_NUMERO,'
      'CB_CODIGO,'
      'FA_FEC_INI,'
      'FA_DIA_HOR,'
      'FA_MOTIVO,'
      'FA_DIAS,'
      'FA_HORAS) values (:PE_YEAR,'
      ':PE_TIPO,'
      ':PE_NUMERO,'
      ':CB_CODIGO,'
      ':FA_FEC_INI,'
      ':FA_DIA_HOR,'
      ':FA_MOTIVO,'
      ':FA_DIAS,'
      ':FA_HORAS)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from FALTAS%0:s.DBF')
    StartMessage = 'Faltas'
    TargetDatabase = dbTarget
    TargetTableName = 'FALTAS'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = FaltasMoveExcludedFields
    Left = 454
    Top = 233
  end
  object Movimien: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into MOVIMIEN (PE_YEAR,'
      'PE_TIPO,'
      'PE_NUMERO,'
      'CB_CODIGO,'
      'MO_ACTIVO,'
      'CO_NUMERO,'
      'MO_IMP_CAL,'
      'US_CODIGO,'
      'MO_PER_CAL,'
      'MO_REFEREN,'
      'MO_X_ISPT,'
      'MO_PERCEPC,'
      'MO_DEDUCCI) values (:PE_YEAR,'
      ':PE_TIPO,'
      ':PE_NUMERO,'
      ':CB_CODIGO,'
      ':MO_ACTIVO,'
      ':CO_NUMERO,'
      ':MO_IMP_CAL,'
      ':US_CODIGO,'
      ':MO_PER_CAL,'
      ':MO_REFEREN,'
      ':MO_X_ISPT,'
      ':MO_PERCEPC,'
      ':MO_DEDUCCI)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from MOVIMIEN%0:s.DBF')
    StartMessage = 'Movimientos de N'#243'minas'
    TargetDatabase = dbTarget
    TargetTableName = 'MOVIMIEN'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = MovimienMoveExcludedFields
    Left = 502
    Top = 233
  end
  object Ausencia: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into AUSENCIA (CB_CODIGO,'
      'AU_DES_TRA,'
      'AU_DOBLES,'
      'AU_EXTRAS,'
      'PE_YEAR,'
      'AU_FECHA,'
      'PE_TIPO,'
      'AU_HORAS,'
      'AU_HORASCK,'
      'PE_NUMERO,'
      'AU_PER_CG,'
      'AU_PER_SG,'
      'AU_TARDES,'
      'AU_TIPO,'
      'AU_TIPODIA,'
      'CB_CLASIFI,'
      'CB_TURNO,'
      'CB_PUESTO,'
      'HO_CODIGO,'
      'CB_NIVEL1,'
      'CB_NIVEL2,'
      'CB_NIVEL3,'
      'CB_NIVEL4,'
      'CB_NIVEL5,'
      'CB_NIVEL6,'
      'CB_NIVEL7,'
      'CB_NIVEL8,'
      'CB_NIVEL9,'
      'AU_AUT_EXT,'
      'AU_AUT_TRA,'
      'AU_HOR_MAN,'
      'AU_STATUS,'
      'AU_NUM_EXT,'
      'AU_TRIPLES,'
      'US_CODIGO,'
      'AU_POSICIO) values (:CB_CODIGO,'
      ':AU_DES_TRA,'
      ':AU_DOBLES,'
      ':AU_EXTRAS,'
      ':PE_YEAR,'
      ':AU_FECHA,'
      ':PE_TIPO,'
      ':AU_HORAS,'
      ':AU_HORASCK,'
      ':PE_NUMERO,'
      ':AU_PER_CG,'
      ':AU_PER_SG,'
      ':AU_TARDES,'
      ':AU_TIPO,'
      ':AU_TIPODIA,'
      ':CB_CLASIFI,'
      ':CB_TURNO,'
      ':CB_PUESTO,'
      ':HO_CODIGO,'
      ':CB_NIVEL1,'
      ':CB_NIVEL2,'
      ':CB_NIVEL3,'
      ':CB_NIVEL4,'
      ':CB_NIVEL5,'
      ':CB_NIVEL6,'
      ':CB_NIVEL7,'
      ':CB_NIVEL8,'
      ':CB_NIVEL9,'
      ':AU_AUT_EXT,'
      ':AU_AUT_TRA,'
      ':AU_HOR_MAN,'
      ':AU_STATUS,'
      ':AU_NUM_EXT,'
      ':AU_TRIPLES,'
      ':US_CODIGO,'
      ':AU_POSICIO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from AUSCHECK%0:s.DBF')
    StartMessage = 'Ausencias'
    TargetDatabase = dbTarget
    TargetTableName = 'AUSENCIA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = AusenciaMoveExcludedFields
    Left = 16
    Top = 280
  end
  object Checadas: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CHECADAS (CB_CODIGO,'
      'AU_FECHA,'
      'CH_DESCRIP,'
      'CH_GLOBAL,'
      'CH_H_AJUS,'
      'CH_H_REAL,'
      'CH_HOR_DES,'
      'CH_HOR_EXT,'
      'CH_HOR_ORD,'
      'CH_IGNORAR,'
      'CH_RELOJ,'
      'CH_SISTEMA,'
      'CH_TIPO,'
      'US_CODIGO,'
      'CH_POSICIO) values (:CB_CODIGO,'
      ':AU_FECHA,'
      ':CH_DESCRIP,'
      ':CH_GLOBAL,'
      ':CH_H_AJUS,'
      ':CH_H_REAL,'
      ':CH_HOR_DES,'
      ':CH_HOR_EXT,'
      ':CH_HOR_ORD,'
      ':CH_IGNORAR,'
      ':CH_RELOJ,'
      ':CH_SISTEMA,'
      ':CH_TIPO,'
      ':US_CODIGO,'
      ':CH_POSICIO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CHECADAS%0:s.DBF')
    StartMessage = 'Checadas'
    TargetDatabase = dbTarget
    TargetTableName = 'CHECADAS'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnFilterRecord = ChecadasFilterRecord
    OnMoveExcludedFields = ChecadasMoveExcludedFields
    Left = 64
    Top = 280
  end
  object Ahorro: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into AHORRO (AH_ABONOS,'
      'AH_CARGOS,'
      'AH_FECHA,'
      'AH_FORMULA,'
      'AH_NUMERO,'
      'AH_SALDO_I,'
      'AH_STATUS,'
      'AH_SALDO,'
      'AH_TIPO,'
      'AH_TOTAL,'
      'US_CODIGO,'
      'CB_CODIGO) values (:AH_ABONOS,'
      ':AH_CARGOS,'
      ':AH_FECHA,'
      ':AH_FORMULA,'
      ':AH_NUMERO,'
      ':AH_SALDO_I,'
      ':AH_STATUS,'
      ':AH_SALDO,'
      ':AH_TIPO,'
      ':AH_TOTAL,'
      ':US_CODIGO,'
      ':CB_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from AHORRO.DBF')
    StartMessage = 'Ahorros'
    TargetDatabase = dbTarget
    TargetTableName = 'AHORRO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = AhorroCloseDatasets
    OnFilterRecord = AhorroFilterRecord
    OnMoveExcludedFields = AhorroMoveExcludedFields
    OnOpenDatasets = AhorroOpenDatasets
    Left = 112
    Top = 280
  end
  object ACar_Abo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into ACAR_ABO (AH_TIPO,'
      'CB_CODIGO,'
      'CR_CAPTURA,'
      'CR_ABONO,'
      'CR_FECHA,'
      'CR_CARGO,'
      'CR_OBSERVA,'
      'US_CODIGO) values (:AH_TIPO,'
      ':CB_CODIGO,'
      ':CR_CAPTURA,'
      ':CR_ABONO,'
      ':CR_FECHA,'
      ':CR_CARGO,'
      ':CR_OBSERVA,'
      ':US_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CAR_ABO.DBF')
    StartMessage = 'Cargos/Abonos de Ahorros'
    TargetDatabase = dbTarget
    TargetTableName = 'ACAR_ABO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = ACar_AboCloseDatasets
    OnFilterRecord = ACar_AboFilterRecord
    OnMoveExcludedFields = ACar_AboMoveExcludedFields
    Left = 161
    Top = 280
  end
  object Prestamo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 100
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PRESTAMO (PR_ABONOS,'
      'PR_CARGOS,'
      'PR_FECHA,'
      'PR_FORMULA,'
      'PR_MONTO,'
      'PR_NUMERO,'
      'PR_REFEREN,'
      'PR_SALDO_I,'
      'PR_STATUS,'
      'PR_TIPO,'
      'PR_TOTAL,'
      'PR_SALDO,'
      'US_CODIGO,'
      'CB_CODIGO) values (:PR_ABONOS,'
      ':PR_CARGOS,'
      ':PR_FECHA,'
      ':PR_FORMULA,'
      ':PR_MONTO,'
      ':PR_NUMERO,'
      ':PR_REFEREN,'
      ':PR_SALDO_I,'
      ':PR_STATUS,'
      ':PR_TIPO,'
      ':PR_TOTAL,'
      ':PR_SALDO,'
      ':US_CODIGO,'
      ':CB_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from AHORRO.DBF')
    StartMessage = 'Pr'#233'stamos'
    TargetDatabase = dbTarget
    TargetTableName = 'PRESTAMO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = PrestamoCloseDatasets
    OnFilterRecord = PrestamoFilterRecord
    OnMoveExcludedFields = PrestamoMoveExcludedFields
    OnOpenDatasets = PrestamoOpenDatasets
    Left = 214
    Top = 280
  end
  object PCar_Abo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into PCAR_ABO (CB_CODIGO,'
      'CR_CAPTURA,'
      'PR_TIPO,'
      'CR_FECHA,'
      'PR_REFEREN,'
      'CR_OBSERVA,'
      'CR_ABONO,'
      'CR_CARGO,'
      'US_CODIGO) values (:CB_CODIGO,'
      ':CR_CAPTURA,'
      ':PR_TIPO,'
      ':CR_FECHA,'
      ':PR_REFEREN,'
      ':CR_OBSERVA,'
      ':CR_ABONO,'
      ':CR_CARGO,'
      ':US_CODIGO)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CAR_ABO.DBF')
    StartMessage = 'Cargos y Abonos de Pr'#233'stamos'
    TargetDatabase = dbTarget
    TargetTableName = 'PCAR_ABO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnCloseDatasets = PCar_AboCloseDatasets
    OnFilterRecord = PCar_AboFilterRecord
    OnMoveExcludedFields = PCar_AboMoveExcludedFields
    Left = 264
    Top = 280
  end
  object Diccion: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from DICCION')
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    Enabled = False
    Poblar = True
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    StartMessage = 'Diccionario de Datos'
    TargetDatabase = dbTarget
    TargetTableName = 'DICCION'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = DiccionCloseDatasets
    Left = 312
    Top = 280
  end
  object Folio: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into FOLIO (FL_CODIGO,'
      'FL_DESCRIP,'
      'FL_REPORTE,'
      'FL_MONTO,'
      'FL_REPITE,'
      'FL_MONEDA,'
      'FL_CEROS,'
      'FL_INICIAL,'
      'FL_FINAL) values (:FL_CODIGO,'
      ':FL_DESCRIP,'
      ':FL_REPORTE,'
      ':FL_MONTO,'
      ':FL_REPITE,'
      ':FL_MONEDA,'
      ':FL_CEROS,'
      ':FL_INICIAL,'
      ':FL_FINAL)')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from FOLIO.DBF')
    StartMessage = 'Folios'
    TargetDatabase = dbTarget
    TargetTableName = 'FOLIO'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = FolioMoveExcludedFields
    Left = 360
    Top = 280
  end
  object TFijas: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DefaultSQL.Strings = (
      'select * from TFIJAS')
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    Enabled = False
    Poblar = True
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    StartMessage = 'Tablas Fijas'
    TargetDatabase = dbTarget
    TargetTableName = 'TFIJAS'
    TargetTable = ttTarget
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    Left = 408
    Top = 280
  end
  object RepRH: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from REPORTES.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO')
    StartMessage = 'Reportes Recursos Humanos'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = RepRHOpenDatasets
    Left = 454
    Top = 279
  end
  object RepTarj: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from REP_TARJ.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO'
      '')
    StartMessage = 'Reportes Checadas'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = RepTarjOpenDatasets
    Left = 502
    Top = 279
  end
  object RepAsist: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from REP_ASIS.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO'
      '')
    StartMessage = 'Reportes Asistencia'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = RepAsistOpenDatasets
    Left = 16
    Top = 326
  end
  object RepCursos: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from REP_CUR.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO')
    StartMessage = 'Reportes Cursos'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = RepCursosOpenDatasets
    Left = 64
    Top = 326
  end
  object RepSar: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from REP_SAR.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO'
      '')
    StartMessage = 'Reportes Pagos Imss'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = RepSarOpenDatasets
    Left = 112
    Top = 326
  end
  object Especial: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from ESPECIAL.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO'
      '')
    StartMessage = 'Reportes Nomina'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = EspecialOpenDatasets
    Left = 161
    Top = 326
  end
  object Forma2: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from FORMA2.DBF'
      'order by FO_CODIGO, FO_TIPO, FO_POSICIO, FO_RENGLON, FO_COLUMNA')
    StartMessage = 'Formas'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = Forma2CloseDatasets
    OnFilterRecord = Forma2FilterRecord
    OnOpenDatasets = Forma2OpenDatasets
    Left = 214
    Top = 326
  end
  object Polizas: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TITULO,'
      'CR_MASCARA,'
      'CR_OPER,'
      'CR_FORMULA,'
      'CR_CALC,'
      'CR_DESCRIP ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TITULO,'
      ':CR_MASCARA,'
      ':CR_OPER,'
      ':CR_FORMULA,'
      ':CR_CALC,'
      ':CR_DESCRIP )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from POLIZA.DBF order by '
      'ES_CODIGO, ES_TOKEN, PO_FOLIO')
    StartMessage = 'P'#243'lizas'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = PolizasCloseDatasets
    OnFilterRecord = PolizasFilterRecord
    OnOpenDatasets = PolizasOpenDatasets
    Left = 264
    Top = 326
  end
  object RepSuper: TZetaMigrar
    CallBackCount = 10
    EmptyTargetTable = False
    InsertSQL.Strings = (
      'insert into CAMPOREP ('
      'RE_CODIGO,'
      'CR_TIPO,'
      'CR_POSICIO,'
      'CR_SUBPOS,'
      'CR_TABLA,'
      'CR_TITULO,'
      'CR_FORMULA,'
      'CR_REQUIER,'
      'CR_CALC, '
      'CR_MASCARA,'
      'CR_ANCHO,'
      'CR_OPER,'
      'CR_TFIELD,'
      'CR_SHOW,'
      'CR_DESCRIP,'
      'CR_BOLD,'
      'CR_ITALIC,'
      'CR_SUBRAYA,'
      'CR_STRIKE,'
      'CR_ALINEA,'
      'CR_COLOR ) values ('
      ':RE_CODIGO,'
      ':CR_TIPO,'
      ':CR_POSICIO,'
      ':CR_SUBPOS,'
      ':CR_TABLA,'
      ':CR_TITULO,'
      ':CR_FORMULA,'
      ':CR_REQUIER,'
      ':CR_CALC,'
      ':CR_MASCARA,'
      ':CR_ANCHO,'
      ':CR_OPER,'
      ':CR_TFIELD,'
      ':CR_SHOW,'
      ':CR_DESCRIP,'
      ':CR_BOLD,'
      ':CR_ITALIC,'
      ':CR_SUBRAYA,'
      ':CR_STRIKE,'
      ':CR_ALINEA,'
      ':CR_COLOR )')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from REP_SUP.DBF order by '
      'ES_CODIGO, ES_TOKEN, ES_POSICIO'
      '')
    StartMessage = 'Reportes Supervisores'
    TargetDatabase = dbTarget
    TargetTableName = 'CAMPOREP'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnCloseDatasets = RepRHCloseDatasets
    OnFilterRecord = RepRHFilterRecord
    OnOpenDatasets = RepSuperOpenDatasets
    Left = 313
    Top = 326
  end
  object Conteo: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    Enabled = False
    InsertSQL.Strings = (
      'insert into CONTEO('
      'CT_FECHA,'
      'CT_NIVEL_1,'
      'CT_NIVEL_2,'
      'CT_NIVEL_3,'
      'CT_NIVEL_4,'
      'CT_NIVEL_5,'
      'CT_NUMERO1,'
      'CT_NUMERO2,'
      'CT_NUMERO3,'
      'CT_TEXTO1,'
      'CT_TEXTO2,'
      'CT_REAL,'
      'CT_CUANTOS ) values ('
      ':CT_FECHA,'
      ':CT_NIVEL_1,'
      ':CT_NIVEL_2,'
      ':CT_NIVEL_3,'
      ':CT_NIVEL_4,'
      ':CT_NIVEL_5,'
      ':CT_NUMERO1,'
      ':CT_NUMERO2,'
      ':CT_NUMERO3,'
      ':CT_TEXTO1,'
      ':CT_TEXTO2,'
      ':CT_REAL,'
      ':CT_CUANTOS )'
      ''
      '')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CONTEO.DBF')
    StartMessage = 'Tabla Conteo'
    TargetDatabase = dbTarget
    TargetTableName = 'CONTEO'
    TargetQuery = tqTarget
    TransactionCount = 10
    OnCallBack = DoCallBack
    OnMoveExcludedFields = ConteoMoveExcludedFields
    Left = 360
    Top = 326
  end
  object CafRegla: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CAFREGLA('
      'CL_CODIGO,'
      'CL_LETRERO,'
      'CL_ACTIVO,'
      'CL_QUERY,'
      'CL_FILTRO,'
      'CL_TIPOS,'
      'CL_TOTAL,'
      'CL_LIMITE,'
      'CL_EXTRAS ) values ('
      ':CL_CODIGO,'
      ':CL_LETRERO,'
      ':CL_ACTIVO,'
      ':CL_QUERY,'
      ':CL_FILTRO,'
      ':CL_TIPOS,'
      ':CL_TOTAL,'
      ':CL_LIMITE,'
      ':CL_EXTRAS )'
      '')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CAFREGLA.DBF')
    StartMessage = 'Reglas'
    TargetDatabase = dbTarget
    TargetTableName = 'CAFREGLA'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = CafReglaMoveExcludedFields
    Left = 408
    Top = 326
  end
  object CafCome: TZetaMigrar
    BatchMove = BatchMove
    CallBackCount = 10
    DeleteQuery = tqDelete
    EmptyTargetTable = True
    InsertSQL.Strings = (
      'insert into CAF_COME('
      'CB_CODIGO,'
      'CF_FECHA,'
      'CF_HORA,'
      'CF_TIPO,'
      'CF_COMIDAS,'
      'CF_EXTRAS,'
      'CF_RELOJ,'
      'US_CODIGO,'
      'CL_CODIGO,'
      'CF_REG_EXT ) values ('
      ':CB_CODIGO,'
      ':CF_FECHA,'
      ':CF_HORA,'
      ':CF_TIPO,'
      ':CF_COMIDAS,'
      ':CF_EXTRAS,'
      ':CF_RELOJ,'
      ':US_CODIGO,'
      ':CL_CODIGO,'
      ':CF_REG_EXT )'
      '')
    Proceso = prNinguno
    SourceDatabase = dbSourceDatos
    SourceQuery = tqSource
    SourceSQL.Strings = (
      'select * from CAF_COME.DBF')
    StartMessage = 'Comidas'
    TargetDatabase = dbTarget
    TargetTableName = 'CAF_COME'
    TargetQuery = tqTarget
    TransactionCount = 100
    OnCallBack = DoCallBack
    OnMoveExcludedFields = CafComeMoveExcludedFields
    Left = 456
    Top = 326
  end
  object spActualizaDiccion: TStoredProc
    DatabaseName = 'dbTarget'
    StoredProcName = 'SP_REFRESH_DICCION'
    Left = 503
    Top = 326
  end
  object Session2: TSession
    Left = 576
    Top = 56
  end
end
