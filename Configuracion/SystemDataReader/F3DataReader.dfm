object TressDataLoader: TTressDataLoader
  Left = 325
  Top = 117
  Width = 532
  Height = 640
  Caption = 'Cargar Datos de Instalaciones del Sistema TRESS'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PanelControles: TPanel
    Left = 0
    Top = 0
    Width = 524
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object DirectorioLBL: TLabel
      Left = 10
      Top = 11
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Directorio:'
    end
    object DirectorioBTN: TSpeedButton
      Left = 486
      Top = 8
      Width = 23
      Height = 22
      Hint = 
        'Buscar Directorio Donde Residen Archivos de Datos de Instalacion' +
        'es'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = DirectorioBTNClick
    end
    object Directorio: TEdit
      Left = 61
      Top = 8
      Width = 420
      Height = 21
      TabOrder = 0
    end
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 565
    Width = 524
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      524
      41)
    object Procesar: TBitBtn
      Left = 358
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Procesar los archivos encontrados'
      Anchors = [akTop, akRight]
      Caption = 'Procesar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = ProcesarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        55555555FFFFFFFFFF55555000000000055555577777777775F55500B8B8B8B8
        B05555775F555555575F550F0B8B8B8B8B05557F75F555555575550BF0B8B8B8
        B8B0557F575FFFFFFFF7550FBF0000000000557F557777777777500BFBFBFBFB
        0555577F555555557F550B0FBFBFBFBF05557F7F555555FF75550F0BFBFBF000
        55557F75F555577755550BF0BFBF0B0555557F575FFF757F55550FB700007F05
        55557F557777557F55550BFBFBFBFB0555557F555555557F55550FBFBFBFBF05
        55557FFFFFFFFF7555550000000000555555777777777755555550FBFB055555
        5555575FFF755555555557000075555555555577775555555555}
      NumGlyphs = 2
    end
    object Salir: TBitBtn
      Left = 441
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Salir de este programa'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = SalirClick
      Kind = bkClose
    end
    object Verificar: TBitBtn
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Verificar las licencias de usuario vs los datos le'#237'dos'
      Caption = 'Verificar '
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = VerificarClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      NumGlyphs = 2
    end
  end
  object BitacoraGB: TGroupBox
    Left = 0
    Top = 33
    Width = 524
    Height = 532
    Align = alClient
    Caption = ' Bit'#225'cora '
    TabOrder = 2
    object Bitacora: TMemo
      Left = 2
      Top = 15
      Width = 520
      Height = 515
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Color = clBtnFace
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssBoth
      TabOrder = 0
    end
  end
end
