object VerificarLicencias: TVerificarLicencias
  Left = 192
  Top = 114
  Width = 870
  Height = 640
  ActiveControl = Abrir
  Caption = 'Verificar Uso de Licencias'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter: TSplitter
    Left = 0
    Top = 130
    Width = 862
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object PanelInferior: TPanel
    Left = 0
    Top = 565
    Width = 862
    Height = 41
    Align = alBottom
    TabOrder = 0
    DesignSize = (
      862
      41)
    object Cerrar: TBitBtn
      Left = 781
      Top = 8
      Width = 75
      Height = 25
      Hint = 'Cerrar Esta Ventana'
      Anchors = [akTop, akRight]
      Cancel = True
      Caption = '&Cerrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = CerrarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00388888888877
        F7F787F8888888888333333F00004444400888FFF444448888888888F333FF8F
        000033334D5007FFF4333388888888883338888F0000333345D50FFFF4333333
        338F888F3338F33F000033334D5D0FFFF43333333388788F3338F33F00003333
        45D50FEFE4333333338F878F3338F33F000033334D5D0FFFF43333333388788F
        3338F33F0000333345D50FEFE4333333338F878F3338F33F000033334D5D0FFF
        F43333333388788F3338F33F0000333345D50FEFE4333333338F878F3338F33F
        000033334D5D0EFEF43333333388788F3338F33F0000333345D50FEFE4333333
        338F878F3338F33F000033334D5D0EFEF43333333388788F3338F33F00003333
        4444444444333333338F8F8FFFF8F33F00003333333333333333333333888888
        8888333F00003333330000003333333333333FFFFFF3333F00003333330AAAA0
        333333333333888888F3333F00003333330000003333333333338FFFF8F3333F
        0000}
      NumGlyphs = 2
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 130
    Align = alTop
    TabOrder = 1
    object PanelBotones: TPanel
      Left = 773
      Top = 1
      Width = 88
      Height = 128
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object Abrir: TBitBtn
        Left = 8
        Top = 6
        Width = 75
        Height = 25
        Hint = 'Abrir el Query especificado'
        Caption = 'Abrir'
        Default = True
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = AbrirClick
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
          07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
          0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
          33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
      end
    end
    object QueryGB: TGroupBox
      Left = 1
      Top = 1
      Width = 772
      Height = 128
      Align = alClient
      Caption = ' Query '
      Color = clBtnFace
      ParentColor = False
      TabOrder = 1
      object Query: TMemo
        Left = 2
        Top = 15
        Width = 768
        Height = 111
        Align = alClient
        BorderStyle = bsNone
        Lines.Strings = (
          
            'select T.SI_FOLIO Sistema, T.SN_NUMERO Sentinel, T.AD_FOLIO Foli' +
            'o,'
          
            'T.AD_SUM_EMP Empleados, A.AD_NUM_EMP Licencia, (T.AD_SUM_EMP - A' +
            '.AD_NUM_EMP ) Diferencia, '
          
            'S.SI_RAZ_SOC Empresa, Z.DI_CODIGO Cod_Dist, D.DI_NOMBRE Distribu' +
            'idor, A.AD_ARCHIVO Archivo'
          'from  CHECK_LIMIT_EMP( '#39'01/01/2006'#39', '#39'2.8'#39' ) T'
          'left outer join SISTEMA S on ( S.SI_FOLIO = T.SI_FOLIO )'
          
            'left outer join AUDIT A on ( A.SI_FOLIO = T.SI_FOLIO ) and ( A.S' +
            'N_NUMERO = T.SN_NUMERO ) and ( A.AD_FOLIO = T.AD_FOLIO )'
          
            'left outer join AUTORIZA Z on ( Z.SI_FOLIO = T.SI_FOLIO ) and ( ' +
            'Z.AU_FOLIO = S.AU_FOLIO )'
          'left outer join DISTRIBU D on ( D.DI_CODIGO = Z.DI_CODIGO )'
          'where ( Z.DI_CODIGO = '#39'GTI'#39' )')
        ScrollBars = ssBoth
        TabOrder = 0
      end
    end
  end
  object ZetaDBGrid: TZetaDBGrid
    Left = 0
    Top = 133
    Width = 862
    Height = 432
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = ZetaDBGridDblClick
  end
  object DataSource: TDataSource
    Left = 232
    Top = 280
  end
end
