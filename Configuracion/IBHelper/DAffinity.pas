unit DAffinity;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, SvcMgr, Dialogs;

type
  TSetIB2CPU2 = class(TService)
    procedure ServiceCreate(Sender: TObject);
    procedure ServiceExecute(Sender: TService);
  private
    { Private declarations }
    FStart: TDateTime;
    FPeriod: TDateTime;
    FProcessors: Integer;
    FPrivilegeSet: Boolean;
    function EnableSeDebugPrivilege: Boolean;
    procedure LogWrite(const sMessage: String);
    procedure LogAndTerminate(const sMessage: String);
    procedure SetInterbaseAffinity;
  public
    { Public declarations }
    function GetServiceController: TServiceController; override;
  end;

var
  SetIB2CPU2: TSetIB2CPU2;

implementation

uses ZetaCommonClasses,
     ZetaWinAPITools;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
     SetIB2CPU2.Controller(CtrlCode);
end;

function TSetIB2CPU2.GetServiceController: TServiceController;
begin
     Result := ServiceController;
end;

procedure TSetIB2CPU2.ServiceCreate(Sender: TObject);
begin
     FProcessors := ZetaWinAPITools.GetNumberOfProcessors;
     FPrivilegeSet := False;
     FPeriod := EncodeTime( 1, 0, 0, 0 ); { One Hour }
     FStart := 0;
end;

procedure TSetIB2CPU2.ServiceExecute(Sender: TService);
const
     K_ONE_SECOND = 1000;
begin
     while not Terminated do
     begin
          if ( FProcessors > 1 ) then
          begin
               if ( ( Now - FStart ) > FPeriod ) then
               begin
                    FStart := Now;
                    SetInterbaseAffinity;
               end;
          end
          else
              LogAndTerminate( 'There Is Only One CPU: No Need To Set Affinity' );
          Sleep( 5 * K_ONE_SECOND );
          ServiceThread.ProcessRequests( False );
          ReportStatus;
     end;
end;

procedure TSetIB2CPU2.SetInterbaseAffinity;
const
     K_INTERBASE = 'IBSERVER.EXE';
     K_AFFINITY_MASK = 2;
var
   iProcess: Integer;
   iProcessMask, iSysMask: DWORD;
   iHandle: THandle;
begin
     if EnableSeDebugPrivilege then
     begin
          try
             iProcess := ZetaWinAPITools.ProcessID( K_INTERBASE );
             if ( iProcess > 0 ) then
             begin
                  iProcessMask := 0;
                  iSysMask := 0;
                  iHandle := Windows.OpenProcess( PROCESS_QUERY_INFORMATION or PROCESS_SET_INFORMATION, False, iProcess );
                  if ( iHandle > 0 ) then
                  begin
                       try
                          if Windows.GetProcessAffinityMask( iHandle, iProcessMask, iSysMask ) then
                          begin
                               if ( iProcessMask <> K_AFFINITY_MASK ) then
                               begin
                                    if Windows.SetProcessAffinityMask( iHandle, K_AFFINITY_MASK ) then
                                       LogWrite( Format( 'Interbase Affinity Set To CPU %d', [ K_AFFINITY_MASK ] ) )
                                    else
                                        LogAndTerminate( Format( 'Unable To Set Interbase Affinity To CPU %d ( %s )', [ K_AFFINITY_MASK, ZetaWinAPITools.GetErrorMessage ] ) );
                               end;
                               {
                               else
                                   LogAndTerminate( Format( 'Interbase Affinity Mask Already Set To %d ( %s )', [ K_AFFINITY_MASK ] ) );
                               }
                          end
                          else
                              LogAndTerminate( 'Unable To Read Interbase Affinity Mask' );
                       finally
                              Windows.CloseHandle( iHandle );
                       end;
                  end
                  else
                      LogAndTerminate( Format( 'Unable To Get Process Handle ( %s )', [ ZetaWinAPITools.GetErrorMessage ] ) );
             end
             else
                 LogAndTerminate( Format( '%s Is Not Running', [ K_INTERBASE ] ) );
          except
                on Error: Exception do
                begin
                     LogAndTerminate( Format( 'IB2CPU2 Error: %s', [ ZetaWinAPITools.GetErrorMessage ] ) );
                end;
          end;
     end;
end;

function TSetIB2CPU2.EnableSeDebugPrivilege: Boolean;
const
     K_PRIVILEGE = 'SeDebugPrivilege';
var
   FOwnToken: THandle;
   FPrivilege: TTokenPrivileges;
   iResult: DWORD;
begin
     { Enable SeDebugPrivilege needed to be able to set affinity mask }
     { if IBServer runs as service using the system account }
     if FPrivilegeSet then
        Result := True
     else
     begin
          Result := False;
          try
             FOwnToken := 0;
             if Windows.OpenProcessToken( Windows.GetCurrentProcess, TOKEN_ADJUST_PRIVILEGES, FOwnToken ) then
             begin
                  if Windows.LookupPrivilegeValue( nil, pChar( K_PRIVILEGE ), FPrivilege.Privileges[ 0 ].Luid ) then
                  begin
                       with FPrivilege do
                       begin
                            PrivilegeCount := 1;
                            Privileges[ 0 ].Attributes := SE_PRIVILEGE_ENABLED
                       end;
                       if Windows.AdjustTokenPrivileges( FOwnToken, False, FPrivilege, SizeOf( FPrivilege ), nil, iResult ) then
                       begin
                            Result := True;
                            FPrivilegeSet := Result;
                       end
                       else
                           LogAndTerminate( Format( 'Error Setting Token Privileges: %s', [ ZetaWinAPITools.GetErrorMessage ] ) );
                  end
                  else
                      LogAndTerminate( Format( 'Error Getting Lookup Privilege Value: %s', [ ZetaWinAPITools.GetErrorMessage ] ) );
             end
             else
                 LogAndTerminate( Format( 'Error Getting Token: %s', [ ZetaWinAPITools.GetErrorMessage ] ) );
          except
                on Error: Exception do
                begin
                     LogAndTerminate( Format( 'Error Setting SeDebugPrivilege: %s', [ Error.Message ] ) );
                end;
          end;
     end;
end;

procedure TSetIB2CPU2.LogWrite(const sMessage: String);
const
     CR_LF = Chr( 13 ) + Chr( 10 );
begin
     try
        LogMessage( CR_LF + sMessage, EVENTLOG_INFORMATION_TYPE, 0, 0 );
     except
           LogMessage( CR_LF + 'Error Writing To System Log', EVENTLOG_ERROR_TYPE, 0, 0 );
     end;
end;

procedure TSetIB2CPU2.LogAndTerminate( const sMessage: String );
begin
     LogWrite( sMessage );
     ServiceThread.Terminate;
end;

end.
