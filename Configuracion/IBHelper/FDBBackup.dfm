object IBBackup: TIBBackup
  Left = 200
  Top = 386
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Respaldar Base de Datos'
  ClientHeight = 256
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Opciones: TGroupBox
    Left = 0
    Top = 37
    Width = 431
    Height = 167
    Align = alClient
    Caption = ' Opciones '
    TabOrder = 1
    object Defaults: TBitBtn
      Left = 346
      Top = 13
      Width = 80
      Height = 25
      Hint = 'Grabar Opciones Como Default De Respaldo'
      Anchors = [akTop, akRight]
      Caption = '&Defaults'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 6
      OnClick = DefaultsClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333300003333330000000000000300003333330788888888880300003333
        330F77777777780300003333330F99777777780300003333330FFFFFFFFFF703
        0000333333000000000000030000333333333333333333330000333333333333
        33003333000033333333333330000333000030000000033300000033000030FF
        FFFF033333003333000030F4444F033333003333000030FFFFFF033338003333
        000030F4444F030000083333000030FFFFFF030000833333000030F44F000333
        33333333000030FFFF0033333333333300003000000333333333333300003333
        33333333333333330000}
    end
    object Transportable: TCheckBox
      Left = 24
      Top = 16
      Width = 129
      Height = 17
      Caption = 'Forma&to Port�til'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object MetadataOnly: TCheckBox
      Left = 24
      Top = 42
      Width = 185
      Height = 17
      Caption = 'Respaldar Solo &Metadata'
      TabOrder = 1
    end
    object DisableGarbageCollection: TCheckBox
      Left = 24
      Top = 67
      Width = 209
      Height = 17
      Caption = 'Deshabilitar Reco&lecci�n de Basura'
      TabOrder = 2
    end
    object IgnoreLimboTx: TCheckBox
      Left = 24
      Top = 93
      Width = 201
      Height = 17
      Caption = '&Ignorar Transacciones En Limbo'
      TabOrder = 3
    end
    object IgnoreCheckSums: TCheckBox
      Left = 24
      Top = 118
      Width = 209
      Height = 17
      Caption = 'Ignorar &Verificadores ( CheckSums )'
      TabOrder = 4
    end
    object VerboseOutput: TCheckBox
      Left = 24
      Top = 144
      Width = 97
      Height = 17
      Caption = 'Mostrar Ava&nce'
      TabOrder = 5
    end
  end
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 431
    Height = 37
    Align = alTop
    TabOrder = 0
    object ArchivoLBL: TLabel
      Left = 12
      Top = 12
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = '&Archivo De Respaldo:'
      FocusControl = Archivo
    end
    object SeekArchivo: TSpeedButton
      Left = 397
      Top = 8
      Width = 23
      Height = 22
      Hint = 'Buscar Archivo De Respaldo'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SeekArchivoClick
    end
    object Archivo: TEdit
      Left = 120
      Top = 8
      Width = 273
      Height = 21
      TabOrder = 0
    end
  end
  object PanelBotones: TPanel
    Left = 0
    Top = 204
    Width = 431
    Height = 33
    Align = alBottom
    TabOrder = 2
    object Salir: TBitBtn
      Left = 350
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Salir De Esta Ventana'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = SalirClick
      Kind = bkClose
    end
    object Respaldar: TBitBtn
      Left = 261
      Top = 4
      Width = 81
      Height = 25
      Hint = 'Respaldar La Base De Datos'
      Anchors = [akTop, akRight]
      Caption = '&Respaldar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = RespaldarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888800008888888888880080088888888888008008880000000800
        008880C088F80800800880C088F80800800880CC00000800008880CCCCCCCC88
        888880CC00000CC0888880C0FFFFF0C0888880C0FFFFF0C0888880C0FFFFF0C0
        888880C0FFFFF0C0888880000000000088888888888888888888}
    end
    object GenerarBatch: TBitBtn
      Left = 5
      Top = 4
      Width = 110
      Height = 25
      Hint = 'Generar Archivo Batch Con El Comando De Respaldo'
      Caption = '&Generar Batch'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = GenerarBatchClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        888888880000880000788000780007880000801919070CDC003B307800008091
        9190CD0D03B0B30800008090009900C0D0080B080000809078490788C480B088
        0000809078090788050B08880000809078490788C0B088880000809000990070
        0B078078000080999990CD0D0B000B080000809999080CDC40BBB07800008800
        0788700077000888000088070888070870707088000088070808070807880088
        0000880700700707888870880000880707070708700008880000880770807708
        0788887800008807088807080777708800008800888880087000078800008888
        88888888888888880000}
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 237
    Width = 431
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'gbk'
    Filter = 'Respaldo Interbase ( *.gbk )|*.gbk|Todos ( *.* )|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Seleccione El Archivo de Respaldo'
    Left = 176
    Top = 8
  end
end
