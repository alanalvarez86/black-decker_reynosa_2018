unit FDBBackup;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, FileCtrl,
     ZetaASCIIFile, ComCtrls;

type
  TIBBackup = class(TForm)
    SaveDialog: TSaveDialog;
    Opciones: TGroupBox;
    PanelSuperior: TPanel;
    ArchivoLBL: TLabel;
    Archivo: TEdit;
    SeekArchivo: TSpeedButton;
    PanelBotones: TPanel;
    Salir: TBitBtn;
    Respaldar: TBitBtn;
    GenerarBatch: TBitBtn;
    Defaults: TBitBtn;
    Transportable: TCheckBox;
    MetadataOnly: TCheckBox;
    DisableGarbageCollection: TCheckBox;
    IgnoreLimboTx: TCheckBox;
    IgnoreCheckSums: TCheckBox;
    VerboseOutput: TCheckBox;
    StatusBar: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure SeekArchivoClick(Sender: TObject);
    procedure GenerarBatchClick(Sender: TObject);
    procedure RespaldarClick(Sender: TObject);
    procedure SalirClick(Sender: TObject);
    procedure DefaultsClick(Sender: TObject);
  private
    { Private declarations }
    FAsciiLog: TAsciiLog;
    FTodas: Boolean;
    FComputerName: String;
    FDatabaseFile: String;
    FPassword: String;
    FUserName: String;
    function CreateCommand( const sBkupFileName, sDatabase, sUserName, sPassword: String ): String;
    function GetLogFile( const sFileName: String ): String;
    function GetParameters( const sBkupFileName, sDatabase, sUserName, sPassword: String ): String;
    function GetProgram: String;
    procedure InitControls;
    function GetBatchFileName(var sFileName: String): Boolean;
    procedure ShowBatch(const sBatch: String);
    procedure ShowText(const sMensaje: String);
  public
    { Public declarations }
    property AsciiLog: TAsciiLog read FAsciiLog write FAsciiLog;
    property Todas: Boolean read FTodas write FTodas;
    property ComputerName: String read FComputerName write FComputerName;
    property DatabaseFile: String read FDatabaseFile write FDatabaseFile;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
    procedure BackupAll;
  end;

var
  IBBackup: TIBBackup;

implementation

uses ZetaDialogo,
     ZetaWinAPITools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaAliasManager,
     FDBRegistry,
     DInterbase;

{$R *.DFM}

procedure TIBBackup.FormShow(Sender: TObject);
begin
     if Todas then
     begin
          if dmInterbase.ConectarComparte then
          begin
               Caption := 'Respaldar Todas Las Bases de Datos De Tress';
               SeekArchivo.Hint := 'Buscar Directorio De Respaldo';
               ArchivoLBL.Caption := '&Directorio de Respaldo:';
               Archivo.Text := ZetaRegistry.BackupFolder;
               Respaldar.Hint := 'Respaldar Todas Las Bases De Datos De Tress';
               InitControls;
          end
          else
              Close;
     end
     else
     begin
          Archivo.Text := ZetaCommonTools.StrTransform( DatabaseFile, ExtractFileExt( DatabaseFile ), '.gbk' );
          InitControls;
     end;
end;

procedure TIBBackup.InitControls;
begin
     Transportable.Checked := ZetaRegistry.Transportable;
     MetadataOnly.Checked := ZetaRegistry.MetadataOnly;
     DisableGarbageCollection.Checked := ZetaRegistry.DisableGarbageCollection;
     IgnoreLimboTx.Checked := ZetaRegistry.IgnoreLimboTx;
     IgnoreCheckSums.Checked := ZetaRegistry.IgnoreCheckSumsBkup;
     VerboseOutput.Checked := ZetaRegistry.VerboseOutput;
end;

function TIBBackup.GetLogFile( const sFileName: String ): String;
begin
     Result := ZetaCommonTools.VerificaDir( ExtractFilePath( sFileName ) ) + 'Bkup' + ExtractFileName( sFileName );
     Result := ZetaCommonTools.StrTransform( Result, ExtractFileExt( Result ), '.log' );
end;

procedure TIBBackup.ShowText( const sMensaje: String );
begin
     StatusBar.SimpleText := sMensaje;
end;

procedure TIBBackup.ShowBatch( const sBatch: String );
begin
     ShowText( Format( 'Archivo Batch %s Creado', [ sBatch ] ) );
     if ZetaDialogo.zConfirm( Caption, 'El Archivo BATCH Ha Sido Creado' +
                                       CR_LF +
                                       CR_LF +
                                       sBatch +
                                       CR_LF +
                                       CR_LF +
                                       '� Desea Verlo ?', 0, mbYes ) then
     begin
          DInterbase.CallNotePad( sBatch );
     end;
end;

function TIBBackup.GetBatchFileName( var sFileName: String ): Boolean;
begin
     with SaveDialog do
     begin
          Filter := 'Archivo Batch( *.bat )|*.bat|Archivo Texto ( *.txt )|*.txt|Todos ( *.* )|*.*';
          FilterIndex := 0;
          Title := 'Seleccione El Archivo Batch A Crear';
          FileName := ExtractFileName( sFileName );
          InitialDir := ExtractFilePath( Application.ExeName );
          if Execute then
          begin
               sFileName := FileName;
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TIBBackup.BackupAll;
var
   i: Integer;
   sPrograma, sLog, sDatabase, sBkupFolder, sBkupFileName: String;
begin
     FTodas := True;
     try
        with FAsciiLog do
        begin
             WriteTimeStamp( '***** Inicio De Respaldo De Bases De Datos De Tress %s *****' );
        end;
        if dmInterbase.ConectarComparte then
        begin
             InitControls;
             sPrograma := GetProgram;
             sBkupFolder := ZetaRegistry.BackupFolder;
             with dmInterbase do
             begin
                  for i := 0 to ( EmpresasCount - 1 ) do
                  begin
                       with Empresas[ i ] do
                       begin
                            try
                               sDatabase := Datos;
                               sBkupFileName := ZetaAliasManager.GetDatabaseBkupFileName( sBkupFolder, sDatabase );
                               sLog := GetLogFile( sBkupFileName );
                               if FileExists( sLog ) then
                                  DeleteFile( sLog );
                               if DInterbase.Ejecutar( GetProgram, GetParameters( sBkupFileName, sDatabase, UserName, Password ), False ) then
                                  FAsciiLog.WriteTimeStamp( Nombre + ' %s' );
                            except
                                  on Error: Exception do
                                  begin
                                       FAsciiLog.WriteException( 0, Format( 'Error Al Respaldar Empresa %s', [ Nombre ] ), Error );
                                  end;
                            end;
                       end;
                  end;
             end;
        end;
        with FAsciiLog do
        begin
             WriteTimeStamp( '***** Fin De Respaldo De Bases De Datos De Tress %s *****' );
        end;
     except
           on Error: Exception do
           begin
                FAsciiLog.WriteException( 0, 'Error En Respaldo De Tress', Error );
           end;
     end;
end;

function TIBBackup.GetProgram: String;
const
     K_COMMAND = '%sgbak.exe';
begin
     Result := Format( K_COMMAND, [ DInterbase.GetServerDirectory ] );
end;

function TIBBackup.GetParameters( const sBkupFileName, sDatabase, sUserName, sPassword: String ): String;
const
     K_OPTIONS = '-B -z -user "%s" -password "%s" -y "%s"';
     K_PARAMETERS = '%s "%s" "%s"';
begin
     Result := Format( K_OPTIONS, [ sUserName, sPassword, GetLogFile( sBkupFileName ) ] );
     if Transportable.Checked then
        Result := Result + ' -t'
     else
         Result := Result + ' -nt';
     if MetadataOnly.Checked then
        Result := Result + ' -m';
     if DisableGarbageCollection.Checked then
        Result := Result + ' -g';
     if IgnoreLimboTx.Checked then
        Result := Result + ' -l';
     if IgnoreCheckSums.Checked then
        Result := Result + ' -ig';
     if VerboseOutput.Checked then
        Result := Result + ' -v';
     Result := Format( K_PARAMETERS, [ Result, sDatabase, sBkupFileName ] );
end;

function TIBBackup.CreateCommand( const sBkupFileName, sDatabase, sUserName, sPassword: String ): String;
const
     K_COMMAND = '"%s" %s';
begin
     Result := Format( K_COMMAND, [ GetProgram, GetParameters( sBkupFileName, sDatabase, sUserName, sPassword ) ] );
end;

procedure TIBBackup.SeekArchivoClick(Sender: TObject);
var
   sDirectory: String;
begin
     with Archivo do
     begin
          if Todas then
          begin
               sDirectory := Text;
               if SelectDirectory( sDirectory, [], 0 ) then
                  Text := sDirectory;
          end
          else
          begin
               with SaveDialog do
               begin
                    Filter := 'Respaldo Interbase( *.gbk )|*.gbk|Respaldo ( *.bak )|*.bak|Todos ( *.* )|*.*';
                    FilterIndex := 0;
                    Title := 'Seleccione El Archivo de Respaldo';
                    FileName := ExtractFileName( Text );
                    InitialDir := ExtractFilePath( Text );
                    if Execute then
                       Text := FileName;
               end;
          end;
     end;
end;

procedure TIBBackup.DefaultsClick(Sender: TObject);
const
     K_MENSAJE = 'Esta Operaci�n Declara Como Defaults' + CR_LF +
                 'Los Par�metros Capturados En Esta Pantalla' + CR_LF +
                 CR_LF +
                 'Los Defaults Son Utilizados En El Proceso' + CR_LF +
                 'De Respaldo Autom�tico';
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', K_MENSAJE, 0, mbNo ) then
     begin
          ZetaRegistry.Transportable := Transportable.Checked;
          ZetaRegistry.MetadataOnly := MetadataOnly.Checked;
          ZetaRegistry.DisableGarbageCollection := DisableGarbageCollection.Checked;
          ZetaRegistry.IgnoreLimboTx := IgnoreLimboTx.Checked;
          ZetaRegistry.IgnoreCheckSumsBkup := IgnoreCheckSums.Checked;
          ZetaRegistry.VerboseOutput := VerboseOutput.Checked;
          if Todas then
             ZetaRegistry.BackupFolder := Archivo.Text;
     end;
end;
{$IFDEF ANTES}
procedure TIBBackup.GenerarBatchClick(Sender: TObject);
var
   i: Integer;
   FDatos: TStrings;
   sBatch, sDatabase, sBkupFileName: String;
begin
     if GetBatchFileName( sBatch ) then
     begin
          FDatos := TStringList.Create;
          try
             with FDatos do
             begin
                  if Todas then
                  begin
                       Add( '@echo off' );
                       with dmInterbase do
                       begin
                            for i := 0 to ( EmpresasCount - 1 ) do
                            begin
                                 with Empresas[ i ] do
                                 begin
                                      sDatabase := Datos;
                                      sBkupFileName := ZetaAliasManager.GetDatabaseBkupFileName( Archivo.Text, sDatabase );
                                      Add( Format( 'REM Respaldar Empresa %s ( Alias %s )', [ Nombre, Alias ] ) );
                                      Add( Format( 'del "%s"', [ GetLogFile( sBkupFileName ) ] ) );
                                      Add( CreateCommand( sBkupFileName, sDatabase, UserName, Password ) );
                                      Add( '' );
                                 end;
                            end;
                       end;
                       Add( 'REM Escriba Aqu� Los Comandos Para Transferir Los Archivos' );
                       Add( 'REM De Respaldo ( *.gbk ) Hacia La Unidad De Respaldo' );
                       Add( 'REM Deseada ( Cinta, Disco, etc. )' );
                       Add( 'REM De Esta Manera Se Asegura Que Los Archivos De Respaldo ( *.gbk )' );
                       Add( 'REM Sean Creados Primero y Luego Sean Transferidos' );
                  end
                  else
                  begin
                       sBkupFileName := Archivo.Text;
                       Add( Format( 'del "%s"', [ GetLogFile( sBkupFileName ) ] ) );
                       Add( CreateCommand( sBkupFileName, ComputerName + ':' + DatabaseFile, UserName, Password ) );
                  end;
                  SaveToFile( sBatch );
             end;
          finally
                 FDatos.Free;
          end;
          ShowBatch( sBatch );
     end;
end;
{$ELSE }
procedure TIBBackup.GenerarBatchClick(Sender: TObject);
var
   i: Integer;
   FDatos, FDatabases:TStrings;
   sBatch, sDatabase, sBkupFileName: String;
begin
     if GetBatchFileName( sBatch ) then
     begin
          FDatos := TStringList.Create;
          try
             with FDatos do
             begin
                  if Todas then
                  begin
                       FDatabases := TStringList.Create;
                       try
                          Add( '@echo off' );
                          with dmInterbase do
                          begin
                               for i := 0 to ( EmpresasCount - 1 ) do
                               begin
                                    with Empresas[ i ] do
                                    begin
                                         sDatabase := Datos;
                                         if ( FDatabases.IndexOf( sDatabase ) < 0 ) then
                                         begin
                                              FDatabases.Add( sDatabase );
                                              sBkupFileName := ZetaAliasManager.GetDatabaseBkupFileName( Archivo.Text, sDatabase );
                                              Add( Format( 'REM Respaldar Empresa %s ( Alias %s )', [ Nombre, Alias ] ) );
                                              Add( Format( 'del "%s"', [ GetLogFile( sBkupFileName ) ] ) );
                                              Add( CreateCommand( sBkupFileName, sDatabase, UserName, Password ) );
                                              Add( '' );
                                         end;
                                    end;
                               end;
                          end;
                          Add( 'REM Escriba Aqu� Los Comandos Para Transferir Los Archivos' );
                          Add( 'REM De Respaldo ( *.gbk ) Hacia La Unidad De Respaldo' );
                          Add( 'REM Deseada ( Cinta, Disco, etc. )');
                          Add( 'REM De Esta Manera Se Asegura Que Los Archivos De Respaldo ( *.gbk )' );
                          Add( 'REM Sean Creados Primero y Luego Sean Transferidos' );
                       finally
                              FDatabases.Free;
                       end;
                  end
                  else
                  begin
                       sBkupFileName := Archivo.Text;
                       Add( Format( 'del "%s"', [ GetLogFile( sBkupFileName ) ] ) );
                       Add( CreateCommand( sBkupFileName, ComputerName + ':' + DatabaseFile, UserName, Password ) );
                  end;
                  SaveToFile( sBatch );
             end;
          finally
                 FDatos.Free;
          end;
          ShowBatch( sBatch );
     end;
end;
{$ENDIF}
procedure TIBBackup.RespaldarClick(Sender: TObject);
var
   i, iCtr: Integer;
   oCursor: TCursor;
   lOk, lViewLog: Boolean;
   sLog, sPrograma, sDatabase, sBkupFolder, sBkupFileName: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        if Todas then
        begin
             sPrograma := GetProgram;
             sBkupFolder := Archivo.Text;
             with dmInterbase do
             begin
                  iCtr := 0;
                  ShowText( Format( 'Iniciando Respaldo De %d Empresas', [ EmpresasCount ] ) );
                  for i := 0 to ( EmpresasCount - 1 ) do
                  begin
                       with Empresas[ i ] do
                       begin
                            try
                               sDatabase := Datos;
                               sBkupFileName := ZetaAliasManager.GetDatabaseBkupFileName( sBkupFolder, sDatabase );
                               sLog := GetLogFile( sBkupFileName );
                               if FileExists( sLog ) then
                               begin
                                    ShowText( Format( 'Archivo %s Borrado', [ sLog ] ) );
                                    DeleteFile( sLog );
                               end;
                               ShowText( Format( 'Respaldando %s ( %s )', [ Nombre, sDatabase ] ) );
                               if DInterbase.Ejecutar( GetProgram, GetParameters( sBkupFileName, sDatabase, UserName, Password ), False ) then
                                  Inc( iCtr )
                               else
                                   ZetaDialogo.zError( '� Error Al Respaldar !', Format( 'Respaldo De %s' + CR_LF + '%s' + CR_LF + 'No Tuvo Exito', [ Nombre, sDatabase ] ), 0 );
                            except
                                  on Error: Exception do
                                  begin
                                       Application.HandleException( Error );
                                  end;
                            end;
                       end;
                  end;
                  ShowText( Format( 'Respaldo De %d de %d Empresas Terminado', [ iCtr, EmpresasCount ] ) );
                  ZetaDialogo.zInformation( 'Respaldo Terminado', Format( '%d de %d Empresas Fueron Respaldadas', [ iCtr, EmpresasCount ] ), 0 );
             end;
        end
        else
        begin
             sBkupFileName := Archivo.Text;
             sLog := GetLogFile( sBkupFileName );
             if FileExists( sLog ) then
                DeleteFile( sLog );
             lOk := DInterbase.Ejecutar( GetProgram, GetParameters( sBkupFileName, ComputerName + ':' + DatabaseFile, UserName, Password ), False );
             if FileExists( sLog ) then
             begin
                  if lOk then
                     lViewLog := ZetaDialogo.zConfirm( Caption,
                                                       'El Respaldo Ha Terminado' +
                                                       CR_LF +
                                                       CR_LF +
                                                       'La Bit�cora Ha Sido Creada En El Archivo' +
                                                       CR_LF +
                                                       CR_LF +
                                                       sLog +
                                                       CR_LF +
                                                       CR_LF +
                                                       '� Desea Verla ?', 0, mbYes )
                  else
                      lViewLog := ZetaDialogo.zErrorConfirm( Caption,
                                                             'El Respaldo Termin� Con Error' +
                                                             CR_LF +
                                                             CR_LF +
                                                             'La Bit�cora Ha Sido Creada En El Archivo' +
                                                             CR_LF +
                                                             CR_LF +
                                                             sLog +
                                                             CR_LF +
                                                             CR_LF +
                                                             '� Desea Verla ?', 0, mbYes );
                  if lViewLog then
                     DInterbase.CallNotePad( sLog );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TIBBackup.SalirClick(Sender: TObject);
begin
     Close;
end;

end.
