program IB2CPU2;

uses
  SvcMgr,
  DAffinity in 'DAffinity.pas' {SetIB2CPU2: TService};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Interbase 2 CPU 2 Service';
  Application.CreateForm(TSetIB2CPU2, SetIB2CPU2);
  Application.Run;
end.
