object IBMaintenance: TIBMaintenance
  Left = 192
  Top = 107
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Mantenimiento A La Base De Datos'
  ClientHeight = 219
  ClientWidth = 337
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelBotones: TPanel
    Left = 0
    Top = 167
    Width = 337
    Height = 33
    Align = alBottom
    TabOrder = 3
    object Salir: TBitBtn
      Left = 256
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Salir De Esta Ventana'
      Anchors = [akTop, akRight]
      Caption = '&Salir'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = SalirClick
      Kind = bkClose
    end
    object Ejecutar: TBitBtn
      Left = 167
      Top = 4
      Width = 81
      Height = 25
      Hint = 'Ejecutar La Operación De Mantenimiento Escogida'
      Anchors = [akTop, akRight]
      Caption = '&Ejecutar'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = EjecutarClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
    object GenerarBatch: TBitBtn
      Left = 5
      Top = 4
      Width = 110
      Height = 25
      Hint = 'Generar Archivo Batch Con El Comando De Mantenimiento'
      Caption = '&Generar Batch'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = GenerarBatchClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        888888880000880000788000780007880000801919070CDC003B307800008091
        9190CD0D03B0B30800008090009900C0D0080B080000809078490788C480B088
        0000809078090788050B08880000809078490788C0B088880000809000990070
        0B078078000080999990CD0D0B000B080000809999080CDC40BBB07800008800
        0788700077000888000088070888070870707088000088070808070807880088
        0000880700700707888870880000880707070708700008880000880770807708
        0788887800008807088807080777708800008800888880087000078800008888
        88888888888888880000}
    end
  end
  object Operacion: TRadioGroup
    Left = 0
    Top = 0
    Width = 337
    Height = 134
    Align = alClient
    Caption = ' Escoja La Operación Deseada  '
    ItemIndex = 0
    Items.Strings = (
      '&Sweep'
      '&Validar'
      '&Eliminar Registros Corruptos')
    TabOrder = 0
    OnClick = OperacionClick
  end
  object PanelOpciones: TPanel
    Left = 0
    Top = 134
    Width = 337
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object IgnoreCheckSum: TCheckBox
      Left = 7
      Top = 7
      Width = 200
      Height = 17
      Caption = '&Ignorar Verificadores ( CheckSums )'
      TabOrder = 0
    end
    object Defaults: TBitBtn
      Left = 255
      Top = 4
      Width = 80
      Height = 25
      Hint = 'Grabar Opciones Como Default De Mantenimiento'
      Anchors = [akTop, akRight]
      Caption = '&Defaults'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = DefaultsClick
      Glyph.Data = {
        66010000424D6601000000000000760000002800000014000000140000000100
        040000000000F000000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333300003333330000000000000300003333330788888888880300003333
        330F77777777780300003333330F99777777780300003333330FFFFFFFFFF703
        0000333333000000000000030000333333333333333333330000333333333333
        33003333000033333333333330000333000030000000033300000033000030FF
        FFFF033333003333000030F4444F033333003333000030FFFFFF033338003333
        000030F4444F030000083333000030FFFFFF030000833333000030F44F000333
        33333333000030FFFF0033333333333300003000000333333333333300003333
        33333333333333330000}
    end
  end
  object ValidateModes: TRadioGroup
    Left = 62
    Top = 52
    Width = 137
    Height = 33
    Caption = ' Registros y Estructuras '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      '&Arreglar'
      '&Reportar')
    TabOrder = 1
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 200
    Width = 337
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'gbk'
    Filter = 'Respaldo Interbase ( *.gbk )|*.gbk|Todos ( *.* )|*.*'
    FilterIndex = 0
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'Seleccione El Archivo de Respaldo'
    Left = 176
    Top = 8
  end
end
