object DBCopier: TDBCopier
  Left = 459
  Top = 158
  ActiveControl = TargetComputerName
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Copiar Hacia Otra Base De Datos'
  ClientHeight = 330
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TargetGB: TGroupBox
    Left = 0
    Top = 111
    Width = 418
    Height = 111
    Align = alTop
    Caption = ' Base de Datos Destino '
    TabOrder = 1
    object TargetComputerNameLBL: TLabel
      Left = 29
      Top = 17
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Co&mputadora:'
      FocusControl = TargetComputerName
    end
    object TargetUserNameLBL: TLabel
      Left = 56
      Top = 41
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = TargetUserName
    end
    object TargetPasswordLBL: TLabel
      Left = 11
      Top = 64
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cla&ve de Acceso:'
      FocusControl = TargetPassword
    end
    object TargetLBL: TLabel
      Left = 30
      Top = 87
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = '&Archivo GDB:'
      FocusControl = TargetDB
    end
    object SeekTargetDB: TSpeedButton
      Left = 384
      Top = 83
      Width = 23
      Height = 22
      Hint = 'Buscar Archivo De La Base De Datos'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SeekTargetDBClick
    end
    object TargetComputerName: TEdit
      Left = 96
      Top = 14
      Width = 209
      Height = 21
      TabOrder = 0
      Text = 'SourceComputerName'
    end
    object TargetUserName: TEdit
      Left = 96
      Top = 37
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'SYSDBA'
    end
    object TargetPassword: TEdit
      Left = 96
      Top = 60
      Width = 121
      Height = 21
      PasswordChar = '*'
      TabOrder = 2
      Text = 'masterkey'
    end
    object TargetDB: TEdit
      Left = 96
      Top = 83
      Width = 284
      Height = 21
      TabOrder = 3
    end
    object TargetConnect: TBitBtn
      Left = 312
      Top = 13
      Width = 97
      Height = 25
      Hint = 'Conectar Base de Datos Destino'
      Caption = '&Conectar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = TargetConnectClick
      Glyph.Data = {
        4E010000424D4E01000000000000760000002800000012000000120000000100
        040000000000D800000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000DDDDDDDDDDDDDDDDDD000000DDD0
        0000000000000D000000DDD08888888888880D000000DDD0F770007777780D00
        0000DDD0F777077777780D000000DDD08B78087777780D000000DBD08B007000
        00780D000000D8B88B78B77777780D000000DD8B8B8BFFFFFFF80D000000DBB8
        BBB0000000000D000000DD88BBBDDDDDDDDDDD000000DD8B8B8BDDDDDDDDDD00
        0000D8BD8BDDDDDDDDDDDD000000DBDD8BDD8BDDDDDDDD000000DDDD8BDDDDDD
        DDDDDD000000DDDDDDDDDDDDDDDDDD000000}
    end
    object TargetDisconnect: TBitBtn
      Left = 312
      Top = 40
      Width = 97
      Height = 25
      Hint = 'Desconectar Base de Datos'
      Caption = '&Desconectar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      OnClick = TargetDisconnectClick
      Glyph.Data = {
        42010000424D4201000000000000760000002800000011000000110000000100
        040000000000CC00000000000000000000001000000010000000000000000000
        BF0000BF000000BFBF00BF000000BF00BF00BFBF0000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        7777700000007777777777777777700000007770707070707070700000007777
        7777777777777000000077707770707777707000000077777777077777777000
        000077707778787777707000000077777070707070777000000071F077777777
        77707000000011F778877777777770000000711F11F070707070700000007718
        1777777777777000000077811F777777777770000000811771F7777777777000
        0000187777177777777770000000777777777777777770000000777777777777
        777770000000}
    end
  end
  object Copiar: TBitBtn
    Left = 342
    Top = 225
    Width = 75
    Height = 25
    Hint = 'Empezar Copiado De Base De Datos'
    Anchors = [akRight, akBottom]
    Caption = 'C&opiar'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = CopiarClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888800008888888888888888888800008888777777778888888800008800
      00000000788888880000880BFFFBFFF0777777880000880F444444F000000078
      0000880FFBFFFBF0FBFFF0780000880F444444F04444F0780000880BFFFBFFF0
      FFFBF0780000880F444444F04444F0780000880FFBFFFBF0FBFFF0780000880F
      44F000004477F0780000880BFFF0FFF0FF0007780000880F44F0FB00F70A0778
      0000880FFBF0F0FF000A00080000880000000F470AAAAA080000888888880FFB
      000A00080000888888880000770A088800008888888888888800088800008888
      88888888888888880000}
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 311
    Width = 418
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object Salir: TBitBtn
    Left = 342
    Top = 285
    Width = 75
    Height = 25
    Hint = 'Salir De Esta Ventana'
    Anchors = [akRight, akBottom]
    Caption = '&Salir'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = SalirClick
    Kind = bkClose
  end
  object Interrumpir: TBitBtn
    Left = 342
    Top = 255
    Width = 75
    Height = 25
    Hint = 'Interrumpir Copiado'
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    Enabled = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnClick = InterrumpirClick
    Kind = bkCancel
  end
  object Opciones: TGroupBox
    Left = 0
    Top = 222
    Width = 185
    Height = 89
    Align = alLeft
    Caption = ' Opciones '
    TabOrder = 2
    object BorrarTablas: TCheckBox
      Left = 11
      Top = 16
      Width = 97
      Height = 17
      Hint = 
        'Borrar Tablas En Base De Dato Destino Antes De Copiar ( Tarda M�' +
        's )'
      Caption = '&Borrar Tablas'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
    end
    object Verificar: TCheckBox
      Left = 11
      Top = 33
      Width = 105
      Height = 17
      Hint = 'Verificar Copia De Cada Tabla ( Tarda M�s )'
      Caption = 'Veri&ficar Copiado'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object DontStopOnError: TCheckBox
      Left = 11
      Top = 50
      Width = 166
      Height = 17
      Hint = 'Si Se Detecta Un Error, Continuar y Reportar Al Final'
      Caption = '&Continuar Al Detectar Errores'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
    end
    object CheckNulls: TCheckBox
      Left = 11
      Top = 67
      Width = 133
      Height = 17
      Caption = 'Ajustar Valores Nulos'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
  end
  object SourceGB: TGroupBox
    Left = 0
    Top = 0
    Width = 418
    Height = 111
    Align = alTop
    Caption = ' Base de Datos Fuente '
    TabOrder = 0
    object SourceComputerNameLBL: TLabel
      Left = 29
      Top = 17
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Co&mputadora:'
      FocusControl = SourceComputerName
    end
    object SourceUserNameLBL: TLabel
      Left = 56
      Top = 41
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = '&Usuario:'
      FocusControl = SourceUserName
    end
    object SourcePasswordLBL: TLabel
      Left = 11
      Top = 64
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cla&ve de Acceso:'
      FocusControl = SourcePassword
    end
    object SourceLBL: TLabel
      Left = 30
      Top = 87
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = '&Archivo GDB:'
      FocusControl = SourceDB
    end
    object SourceComputerName: TEdit
      Left = 96
      Top = 14
      Width = 209
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 0
      Text = 'SourceComputerName'
    end
    object SourceUserName: TEdit
      Left = 96
      Top = 37
      Width = 121
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 1
      Text = 'SYSDBA'
    end
    object SourcePassword: TEdit
      Left = 96
      Top = 60
      Width = 121
      Height = 21
      Color = clBtnFace
      Enabled = False
      PasswordChar = '*'
      TabOrder = 2
      Text = 'masterkey'
    end
    object SourceDB: TEdit
      Left = 96
      Top = 83
      Width = 284
      Height = 21
      Color = clBtnFace
      Enabled = False
      TabOrder = 3
    end
  end
  object TableList: TBitBtn
    Left = 261
    Top = 226
    Width = 75
    Height = 25
    Hint = 'Generar Lista De Tablas A Copiar'
    Anchors = [akRight, akBottom]
    Caption = '&Tablas'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 7
    OnClick = TableListClick
    Glyph.Data = {
      66010000424D6601000000000000760000002800000014000000140000000100
      040000000000F000000000000000000000001000000010000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
      8888888800008888888888888888888800008888888888888888888800008888
      8884444488888888000088888884FFF488888888000088888804444488888888
      00008888808888888888888800004444488444448844444800004FFF4004BBB4
      004FFF4800004444488444448844444800008888808888888888888800008888
      8804444488444448000088888884FFF4004FFF48000088888884444488444448
      0000888888888888088888880000888888888888804444480000888888888888
      884FFF4800008888888888888844444800008888888888888888888800008888
      88888888888888880000}
  end
  object DatabaseFind: TOpenDialog
    DefaultExt = 'gdb'
    Filter = 'Interbase Database ( *.gdb )|*.gdb|All Files ( *.* )|*.*'
    FilterIndex = 0
    Title = 'Select An Interbase Database File'
    Left = 248
    Top = 48
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'txt'
    Filter = 'Texto ( *.txt )|*.txt|Todos ( *.* )|*.*'
    FilterIndex = 0
    Title = 'Escoja El Archivo Deseado'
    Left = 219
    Top = 48
  end
end
