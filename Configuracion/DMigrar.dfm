object dmMigrar: TdmMigrar
  OldCreateOrder = True
  OnCreate = dmMigrarCreate
  OnDestroy = dmMigrarDestroy
  Left = 541
  Top = 219
  Height = 250
  Width = 256
  object dbSourceShared: TDatabase
    DatabaseName = 'dbSourceShared'
    DriverName = 'STANDARD'
    LoginPrompt = False
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    Left = 33
    Top = 72
  end
  object dbSourceDatos: TDatabase
    DatabaseName = 'dbSourceDatos'
    DriverName = 'STANDARD'
    LoginPrompt = False
    Params.Strings = (
      '')
    SessionName = 'Default'
    TransIsolation = tiDirtyRead
    Left = 33
    Top = 9
  end
  object dbTarget: TDatabase
    DatabaseName = 'dbTarget'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=SYSDBA'
      'PASSWORD=m')
    SessionName = 'Default'
    Left = 33
    Top = 124
  end
  object tqSource: TQuery
    UniDirectional = True
    Left = 112
    Top = 9
  end
  object tqTarget: TQuery
    SQL.Strings = (
      'select * from LEY_IMSS')
    UniDirectional = True
    Left = 112
    Top = 124
  end
  object tqDelete: TQuery
    UniDirectional = True
    Left = 176
    Top = 126
  end
  object ttTarget: TTable
    Left = 176
    Top = 72
  end
  object BatchMove: TBatchMove
    AbortOnKeyViol = False
    AbortOnProblem = False
    Destination = ttTarget
    Mode = batCopy
    Left = 112
    Top = 72
  end
  object ttSource: TTable
    Left = 175
    Top = 9
  end
end
