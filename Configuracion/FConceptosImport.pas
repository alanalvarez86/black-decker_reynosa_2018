unit FConceptosImport;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls, Grids, DBGrids, Db,
     FMigrar,
     ZetaWizard, jpeg;

type
  TImportarConceptos = class(TMigrar)
    DirectorioGB: TGroupBox;
    PathLBL: TLabel;
    PathSeek: TSpeedButton;
    Path: TEdit;
    ListaConceptos: TTabSheet;
    TodosAlgunos: TRadioGroup;
    DBGrid: TDBGrid;
    DBNavigator: TDBNavigator;
    dsConceptos: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure TodosAlgunosClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    function Todos: Boolean;
  public
    { Public declarations }
  end;

var
  ImportarConceptos: TImportarConceptos;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaMigrar,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TImportarConceptos.FormCreate(Sender: TObject);
begin
     FParadoxPath := Path;
     dmMigracion := TdmReportes.Create( Self );
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( '' );
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Conceptos.log' );
     inherited;
     HelpContext := H00009_Importar_conceptos;
end;

procedure TImportarConceptos.FormDestroy(Sender: TObject);
begin
     inherited;
     TdmReportes( dmMigracion ).Free;
end;

function TImportarConceptos.Todos: Boolean;
begin
     Result := ( TodosAlgunos.ItemIndex = 0 );
end;

procedure TImportarConceptos.TodosAlgunosClick(Sender: TObject);
begin
     inherited;
     with DBGrid do
     begin
          if Todos then
             Options := Options - [ dgMultiSelect ]
          else
              Options := Options + [ dgMultiSelect ];
     end;
end;

procedure TImportarConceptos.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          with Wizard do
          begin
               if Adelante then
               begin
                    if EsPaginaActual( DirectorioDOS ) then
                    begin
                         with TdmReportes( dmMigracion ) do
                         begin
                              SourceSharedPath := TablasDefaultPath;
                              try
                                 CanMove := ValidaDirectorioParadox and AbreArchivoConceptos( dsConceptos );
                              except
                                    on Error: Exception do
                                    begin
                                         CanMove := False;
                                         ShowError( '� Error En Directorio De Archivo Externo CONCEPTO.DB !', Error );
                                    end;
                              end;
                              if not CanMove then
                                 ActiveControl := Path;
                         end;
                    end;
               end
               else
                   if EsPaginaActual( ListaConceptos ) then
                   begin
                        dsConceptos.Dataset := nil;
                   end;
          end;
     end;
end;

procedure TImportarConceptos.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( GetCuantosConceptos );
          StartProcess;
          lOk := ImportarConceptos( DBGrid.SelectedRows );
          EndProcess;
     end;
     EndLog;
     inherited;
end;

end.
