unit ZConvierteFormula;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, ZetaQRExpr;

function ConvierteFormula( const sValor: String; const EsFiltro: Boolean ): String;
function TransformaResult( sValor : string; ioffSet : integer ) : string;
function ConvierteCamposColabora( const Arreglo : array of string;
                                  const sTabla,sTexto: string) : string;

var
   oEvaluador : TQREvaluator;

implementation

uses ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonClasses;

function StrTransVar( sOrigen, sSearch, sReplace: String ) : String;
var
   iPos, iLast, iSearch : Integer;
   sCopia : String;
   lCambia : Boolean;
begin
     Result  := '';
     sSearch := AnsiUpperCase( sSearch );
     iSearch := Length( sSearch );
     sCopia  := AnsiUpperCase( sOrigen );
     iPos    := AnsiPos( sSearch, sCopia );
     while ( iPos > 0 ) do
     begin
          iLast := iPos + iSearch;
          if ( iLast <= Length( sCopia )) then
               lCambia := not ( sCopia[iLast] in ['A'..'Z','0'..'9','_'] )
          else
               lCambia := TRUE;

          if ( lCambia ) then
               Result := Result + Copy( sOrigen, 1, iPos-1 ) + sReplace
          else
          begin
              Result := Result + Copy( sOrigen, 1, iPos );
              iLast := iPos + 1;
          end;
          sCopia := Copy( sCopia, iLast, MAXINT );
          sOrigen := Copy( sOrigen, iLast, MAXINT );
          iPos := AnsiPos( sSearch, sCopia );
     end;
     Result := Result + sOrigen;
end;

function PosUpper(const sSearch, sTarget : string ): Integer;
begin
      Result := AnsiPos( AnsiUpperCase( sSearch ), AnsiUpperCase( sTarget ) );
end;


function ConvierteCamposColabora( const Arreglo : array of string;
                                  const sTabla,sTexto: string) : string;
 var i,iPos : integer;
begin
     Result := sTexto;
     for i:= Low(Arreglo) to High(Arreglo) do
     begin
          iPos := PosUpper(Arreglo[i],Result);
          if iPos > 0 then
          begin
               if ((iPos - 1)=0) OR
                  (Result[iPos-1]<>'.') then
               begin
                    Result := StrTransAll( Result, Arreglo[i], sTabla+'.'+Arreglo[i]);
               end;
          end;
     end;
end;

function TransformaGlobal( sValor : string ) : string;
const
aGlobalString : array[1..89] of string=
('CY_NAME','CY_DIRECC1','CY_DIRECC2','CY_CITY','CY_ENTIDAD','CY_CODPOST',
'CY_TELEFON','CY_RFC','CY_INFONAV','CY_EMPRESA','CY_GUIA','CY_REP_LEG','CY_NIV_1',
'CY_NIV_2','CY_NIV_3','CY_NIV_4','CY_NIV_5','CY_NIV_6','CY_NIV_7','CY_NIV_8',
'CY_NIV_9','CY_FEC_1','CY_FEC_2','CY_FEC_3','CY_TEX_1','CY_TEX_2','CY_TEX_3',
'CY_TEX_4','CY_NUM_1','CY_NUM_2','CY_NUM_3','CY_LOG_1','CY_LOG_2','CY_LOG_3',
'CY_TAB_1','CY_TAB_2','CY_TAB_3','CY_TAB_4','CY_REDONDO','CY_OFF_DIA','CY_DIF_MONO',
'CY_PROMEDI','CY_SUBSIDI','CY_ZONA_GE','CY_ANCHONI','NOM_FORMAT','NOM_BANCO',
'NOM_1','NOM_2','NOM_3','NOM_ISPT','NOM_RETRO','NOM_AJUSTE','NOM_DIFERE','NOM_SEP_CR',
'NOM_PRO_SM','RH_1','RH_2','RH_3','RH_4','RH_5','RH_DIR_FOT','RH_DIR_DOC',
'SAR_AD_IMSS','SAR_18_PER','SAR_18_DIA','SAR_ARCH_SU','SAR_MOVS_SU','SAR_EXE_SUA',
'SAR_FEC_SUA','SAR_ZAP_SUA','SAR_INF_AMO','REL_IGUALES','REL_AUT_EXT','REL_AUT_FES',
'REL_EXT_SAB','REL_EXT_DES','REL_EXT_FES','REL_SUM_HOR','ZCUR_GIRO','ZCUR_REGI',
'ZCUR_REMP','ZCUR_RTRA','ZCUR_NUM1','ZCUR_NUM2','ZCUR_TEX1','ZCUR_TEX2','PR_ADICIO',
'CY_PATRONAL');

aGlobalInteger : array[1..{$ifdef ACS}92{$else}89{$endif}] of integer=
( K_GLOBAL_RAZON_EMPRESA, K_GLOBAL_CALLE_EMPRESA, K_GLOBAL_COLONIA_EMPRESA,
 K_GLOBAL_CIUDAD_EMPRESA, K_GLOBAL_ENTIDAD_EMPRESA, K_GLOBAL_CP_EMPRESA,
 K_GLOBAL_TEL_EMPRESA, K_GLOBAL_RFC_EMPRESA, K_GLOBAL_INFONAVIT_EMPRESA,
 K_GLOBAL_DIGITO_EMPRESA, K_GLOBAL_NUM_IMSS, K_GLOBAL_REPRESENTANTE,
 K_GLOBAL_NIVEL1, K_GLOBAL_NIVEL2, K_GLOBAL_NIVEL3, K_GLOBAL_NIVEL4,
 K_GLOBAL_NIVEL5, K_GLOBAL_NIVEL6, K_GLOBAL_NIVEL7, K_GLOBAL_NIVEL8,
 K_GLOBAL_NIVEL9, {$ifdef ACS}K_GLOBAL_NIVEL10,K_GLOBAL_NIVEL11,K_GLOBAL_NIVEL12, {$endif}
 K_GLOBAL_FECHA1, K_GLOBAL_FECHA2, K_GLOBAL_FECHA3,
 K_GLOBAL_TEXTO1, K_GLOBAL_TEXTO2, K_GLOBAL_TEXTO3, K_GLOBAL_TEXTO4,
 K_GLOBAL_NUM1, K_GLOBAL_NUM2, K_GLOBAL_NUM3, K_GLOBAL_LOG1, K_GLOBAL_LOG2,
 K_GLOBAL_LOG3, K_GLOBAL_TAB1, K_GLOBAL_TAB2, K_GLOBAL_TAB3, K_GLOBAL_TAB4,
 K_GLOBAL_REDONDEO_NETO, K_GLOBAL_PRIMER_DIA, K_GLOBAL_LLEVAR_SALDO_REDONDEO,
 K_GLOBAL_PROMEDIAR_SALARIOS, K_GLOBAL_PORCEN_SUBSIDIO, K_GLOBAL_ZONAS_GEOGRAFICAS,
 1000, K_GLOBAL_FORMATO_POLIZA, K_GLOBAL_FORMATO_BANCA, K_GLOBAL_NUM_GLOBAL1,
 K_GLOBAL_NUM_GLOBAL2, K_GLOBAL_NUM_GLOBAL3, K_GLOBAL_ISPT, K_GLOBAL_CREDITO,
 K_GLOBAL_AJUSTE, K_GLOBAL_INCOBRABLES, K_GLOBAL_SEPARAR_CRED_SAL, K_GLOBAL_PROM_SAL_MIN,
 K_GLOBAL_NUM_GLOBAL4, K_GLOBAL_NUM_GLOBAL5, K_GLOBAL_NUM_GLOBAL6, K_GLOBAL_NUM_GLOBAL7,
 K_GLOBAL_NUMERO_GLOBAL8, 1001, 1002, K_GLOBAL_ADICIONAL_RETIRO, K_GLOBAL_FORM_PERCEP,
 K_GLOBAL_FORM_DIAS, K_GLOBAL_FILE_EMP_SUA, K_GLOBAL_FILE_MOVS_SUA, K_GLOBAL_DIR_DATOS_SUA,
 K_GLOBAL_FEC_1_EXP_SUA, K_GLOBAL_DELETE_FILE_SUA, K_GLOBAL_TOPE_INFONAVIT,
 K_GLOBAL_GRACIA_2CHECK, K_GLOBAL_AUT_X, K_GLOBAL_AUT_FESTIVO_TRAB, K_GLOBAL_TRATO_X_SAB,
 K_GLOBAL_TRATO_X_DESC, K_GLOBAL_TRATO_X_FESTIVO, K_GLOBAL_SUMAR_JORNADAS, K_GLOBAL_GIRO_EMPRESA,
 K_GLOBAL_REGISTRO_STPS, K_GLOBAL_REPRESENTA_STPS_EMPRESA, K_GLOBAL_REPRESENTA_STPS_EMPLEADO,
 K_GLOBAL_NUM_GLOBAL9, K_GLOBAL_NUM_GLOBAL10, K_GLOBAL_TEXT_GLOBAL1, K_GLOBAL_TEXT_GLOBAL2,
 1003,K_GLOBAL_NUM_IMSS );

 var i : integer;
begin
     Result := sValor;
     for i := Low(aGlobalString) to High(aGlobalString) do
     begin
          Result := StrTransAll( Result, aGlobalString[ i ], 'GLOBAL(' + IntToStr( aGlobalInteger[ i ] ) +')' );
     end;
end;

function TransformaGenerico( sValor : string ) : string;
const aGenerico : array[1..46] of string=
     ('T_EDOCIV','T_HABITA','T_VIVECON','T_MEDIO','T_ESTUDIO','T_TABLAS',
      '','T_TURNO','T_CONTRATO','T_MOTBAJA','','T_FALTAS','T_CLASIFI','T_PUESTO','T_OTRAS_P',
      'T_KARDEX','T_IMSS','T_MONEDAS','','','T_NIVEL[1]','T_NIVEL[2]','T_NIVEL[3]','T_NIVEL[4]',
      'T_NIVEL[5]','T_NIVEL[6]','T_NIVEL[7]','T_NIVEL[8]','T_NIVEL[9]','',
      'T_EXTRA[1]','T_EXTRA[2]','T_EXTRA[3]','T_EXTRA[4]','','',
      '','','T_CLAS','T_AHORRO','T_BANCO','T_LOCALIDAD','T_SUCURSAL','T_ENTIDAD','T_PATRONAL',
      'T_AUTORIZA');

const aGenerico2: array[1..16,1..2] of string=
(('TABLA_GENE("F", CB_EDO_CIV, T_EdoCiv  )','EDOCIVIL.TB_ELEMENT'),
('TABLA_GENE("F", CB_VIVEEN, T_Habita )','VIVE_EN.TB_ELEMENT'),
('TABLA_GENE("F", CB_VIVECON, T_ViveCon )','VIVE_CON.TB_ELEMENT'),
('TABLA_GENE("F", CB_MED_TRA, T_Medio )','TRANSPOR.TB_ELEMENT'),
('TABLA_GENE("F", CB_ESTUDIO, T_Estudio )','ESTUDIOS.TB_ELEMENT'),
('TABLA_GENE("F", CB_CONTRAT, T_Contrato )','CONTRATO.TB_ELEMENT'),
('TABLA_GENE("F", CB_PUESTO, T_Puesto )','PUESTO.PU_DESCRIP'),
('TABLA_GENE("F", CB_CLASIFI,T_Clasifi )','CLASIFI.TB_ELEMENT'),
('TABLA_GENE("F", CB_TURNO, T_Turno )','TURNO.TU_DESCRIP'),
('TABLA_GENE("F", CB_TABLASS, T_IMSS )','SSOCIAL.TB_ELEMENT'),
('TABLA_GENE( "F", CB_MOT_BAJ, T_MotBaja )','MOT_BAJA.TB_ELEMENT'),
('TABLA_GENE("F", CB_OTRAS_1, T_Otras_P )','OTRASPER.TB_ELEMENT'),
('TABLA_GENE("F", CB_OTRAS_2, T_Otras_P )','OTRASPER.TB_ELEMENT'),
('TABLA_GENE("F", CB_OTRAS_3, T_Otras_P )','OTRASPER.TB_ELEMENT'),
('TABLA_GENE("F", CB_OTRAS_4, T_Otras_P )','OTRASPER.TB_ELEMENT'),
('TABLA_GENE("F", CB_OTRAS_5, T_Otras_P )','OTRASPER.TB_ELEMENT'));

 var i : integer;
begin
     Result := sValor;
     for  i := Low(aGenerico2) to High(aGenerico2) do
          Result := StrTransAll( Result, aGenerico2[i,1], aGenerico2[i,2] );
     for  i := Low(aGenerico) to High(aGenerico) do
          if aGenerico[i] <> '' then
             Result := StrTransAll( Result, aGenerico[ i ], IntToStr( i ) );
end;
{cv}
function TransformaResult( sValor : string; ioffSet : integer ) : string;

 const K_RESULT250 = 'RESULT[';
       K_RESULTWIN = 'RESULT(';
       K_SUMA_RESULT250 = 'SUMA_RESULT[';
       K_SUMA_RESULTWIN = 'SUMA_RESULT(';

  var i,j, iInicial, iFinal, iLength : integer;

 function GetNumero( sFormula: string; sToken : char ) : integer;
  var sInicial : string;
 begin
      Result :=0;
      while ( j <= iLength ) AND ( sFormula[ j ] <> sToken ) do
      begin
           sInicial := sInicial + sFormula[ j ];
           Inc( j );
      end;
      try
         Result := StrToInt( Trim( sInicial ) );
      except
      end;
 end;

 function Resultado( const sSumaResult, sResult: string;
                     sFormula : string;
                     sToken : char) : string;
 begin
      i := PosUpper( sSumaResult, sFormula );
      while i > 0 do
      begin
           j:= i + 12;
           iLength:= Length( sFormula );
           iInicial := GetNumero( sFormula,',') ;
           inc(j);
           iFinal := GetNumero(sFormula,sToken) ;
           sFormula := StrTransform( sFormula,
                       Copy( sFormula, i, j - i + 1 ),
                       '@SRESULT@' +
                       IntToStr( iInicial  + iOffSet ) +','+
                       IntToStr( iFinal + iOffSet ) + ')' );

           i := PosUpper( sSumaResult , sFormula );
      end;

      i := PosUpper(sResult , sFormula );
      while i > 0 do
      begin
           j:= i + 7;
           iLength:= Length( sFormula );
           iFinal := GetNumero(sFormula,sToken) ;
           sFormula := StrTransform( sFormula,
                                     Copy( sFormula, i, j - i + 1 ),
                                     '@RESULT@' +
                                     IntToStr( iFinal + iOffSet ) + ')' );
           i := PosUpper( sResult, sFormula );
      end;
      Result := sFormula;
 end;
begin
     Result := sValor;
     Result := Resultado(K_SUMA_RESULT250, K_RESULT250, Result,']');
     Result := Resultado(K_SUMA_RESULTWIN, K_RESULTWIN, Result,')');

     Result := StrTransAll( Result, '@SRESULT@', 'SUMA_RESULT(');
     Result := StrTransAll( Result, '@RESULT@', 'RESULT(');
end;

function TransLogicos( const sFormula : string ) : string;
const aLogicos : array[1..44] of string[10] =
('CL_ACTIVO','CF_REG_EXT','CH_GLOBAL','CH_SISTEMA','CH_IGNORAR',
'AU_HOR_MAN','CB_G_LOG_1','CB_G_LOG_2','CB_G_LOG_3','CB_AUTOSAL','CB_CHECA',
'CB_INFMANT','CO_ACTIVO','CO_G_ISPT','CO_G_IMSS','CO_A_PTU','CO_CALCULA','CO_IMPRIME',
'CO_MENSUAL','CO_LISTADO','CB_GLOBAL','EV_ACTIVO','EV_ALTA','EV_BAJA',
'EV_M_ANTIG','HO_ADD_EAT','HO_CHK_EAT','HO_IGN_EAT','IV_ACTIVO',
'KA_OPCIONA','MO_PERCEP','MO_ACTIVO','MO_IMPRIME','MO_A_PTU','MO_MENSUAL','MO_PER_CAL',
'EN_OPCIONA','TU_MADRUGA','TU_FESTIVO','TU_MOD_18','TU_REDUCID','VA_IMPRESA','CO_ISN','@@@--');
const aLogicosTablas : array[1..44] of string[20] =
('CAFREGLA.CL_ACTIVO','CAF_INV.CF_REG_EXT',
'CHECADAS.CH_GLOBAL','CHECADAS.CH_SISTEMA','CHECADAS.CH_IGNORAR',
'AUSENCIA.AU_HOR_MAN','COLABORA.CB_G_LOG_1','COLABORA.CB_G_LOG_2',
'COLABORA.CB_G_LOG_3','COLABORA.CB_AUTOSAL','COLABORA.CB_CHECA',
'COLABORA.CB_INFMANT','CONCEPTO.CO_ACTIVO','CONCEPTO.CO_G_ISPT',
'CONCEPTO.CO_G_IMSS','CONCEPTO.CO_A_PTU','CONCEPTO.CO_CALCULA',
'CONCEPTO.CO_IMPRIME','CONCEPTO.CO_MENSUAL','CONCEPTO.CO_LISTADO',
'KARDEX.CB_GLOBAL','KARDEX.CB_AUTOSAL','EVENTO.EV_ACTIVO','EVENTO.EV_ALTA',
'EVENTO.EV_BAJA','EVENTO.EV_M_ANTIG','HORARIO.HO_ADD_EAT','HORARIO.HO_CHK_EAT',
'HORARIO.HO_IGN_EAT','INVITA.IV_ACTIVO',
'KA_OPCIONA','MOVIMIEN.MO_PERCEP','MOVIMIEN.MO_ACTIVO',
'MOVIMIEN.MO_IMPRIME','MOVIMIEN.MO_A_PTU','MOVIMIEN.MO_MENSUAL',
'MOVIMIEN.MO_PER_CAL',
'ENTRENA.EN_OPCIONA','TU_MADRUGA','TURNO.TU_FESTIVO','TU_MOD_18',
'TU_REDUCID','VA_IMPRESA','CONCEPTO.CO_ISN');

var i : integer;
    sExpr : string;
begin
     Result := sFormula;
     for i:= Low(aLogicosTablas) to High(aLogicosTablas) do
     begin
          sExpr := '@'+ StrZero( IntToStr(i), 2 );
          Result := StrTransAll( Result, aLogicosTablas[ i ], sExpr+'@' );
          Result := StrTransAll( Result, aLogicos[ i ], sExpr );
          Result := StrTransAll( Result, sExpr+'@', '(RTRIM('+aLogicosTablas[ i ] + ')="S")'  );
          Result := StrTransAll( Result, sExpr, '(RTRIM('+aLogicos[ i ] + ')="S")'  );
     end;
end;

function TransCASE( const sCodigo : string ) : string;
begin
     Result := sCodigo;
     if (PosUpper('CASE',Result)>0) AND
        (PosUpper('{',Result)>0) then
     begin
          Result := StrTransAll( Result, '{', '' );
          Result := StrTransAll( Result, '}', '' );
     end;
end;

function TransLlaveS( const sCodigo : string ) : string;
 var iPosInicial, iPosFinal : integer;
     sTexto, sLlave : string;
begin
     sLlave := sCodigo;
     if Length( sLlave ) > 0 then
     begin
          iPosInicial := PosUpper( '{', sLlave );
          iPosFinal := PosUpper( '}', sLlave );
          sTexto := '';
          while ( iPosInicial > 0 ) and ( iPosFinal > 0 ) do
          begin
               iPosInicial := iPosInicial + 1;
               while iPosInicial < iPosFinal do
               begin
                    sTexto := sTexto + sLlave[ iPosInicial ];
                    iPosInicial := iPosInicial + 1;
               end;
               Result := sTexto;
               Result := StrTransAll( Result, '[', '' );
               Result := StrTransAll( Result, ']', '' );
               Result := StrTransAll( Result, UnaComilla, '' );
               Result := StrTransAll( Result, Comilla, '' );
               Result := StrTransAll( Result, ' ', '' );

               sLlave := StrTransAll( sLlave, sTexto, Result );
               sLlave := StrTransform( sLlave, '{', '@@@' );
               sLlave := StrTransform( sLlave, '}', '@@@' );

               iPosInicial := PosUpper( '{', sLlave );
               iPosFinal := PosUpper( '}', sLlave );
               sTexto := '';
          end;
          sLlave := StrTransAll( sLlave, '@@@', UnaComilla );
          sLlave := StrTransAll( sLlave, '@@@', UnaComilla );
     end;
     Result := sLlave;
end;

function TransformaTablas( sValor : string ) : string;
const
     aTablas : array[1..17] of string=
               ( 'LIQ_IMSS', 'LIQ_EMP', 'AHORRO', 'CHECADAS', 'CONCEPTO', 'KARCURSO',
                 'HORARIO', 'INCAPACI', 'LEY_IMSS', 'LIQ_EMP', 'LIQ_IMSS', 'LIQ_MOV',
                 'MOVIMIEN', 'PERIODO', 'TURNO', 'VACACION','KARDEX' );
     FLECHA = '->';
     PUNTO = '.';
var
   i : integer;
begin
     Result := sValor;
     Result := StrTransAll( Result, 'COLABORA->(', '(' );
     Result := StrTransAll( Result, 'A->(', '(' );
     Result := StrTransAll( Result, 'COLABORA->', 'COLABORA.' );
     Result := StrTransAll( Result, 'NOMINA->', 'NOMINA.' );
     Result := StrTransAll( Result, 'ACUMULA->', 'ACUMULA.' );
     Result := StrTransAll( Result, 'AUSENCIA->', 'AUSENCIA.' );
     Result := StrTransAll( Result, 'C->', 'COLABORA.' );
     Result := StrTransAll( Result, 'A->', 'COLABORA.' );

     Result := StrTransAll( Result, 'CHECK', 'AUSENCIA' );
     Result := StrTransAll( Result, 'CK_HORAS', 'AU_HORASCK' );
     Result := StrTransAll( Result, 'CK_DIA', 'AUSENCIA.AU_POSICIO' );
     Result := StrTransAll( Result, 'CK_R_IN1', 'CHECADA(1,1)' );
     Result := StrTransAll( Result, 'CK_R_OUT1', 'CHECADA(1,2)' );
     Result := StrTransAll( Result, 'CK_R_IN2', 'CHECADA(2,1)' );
     Result := StrTransAll( Result, 'CK_R_OUT2', 'CHECADA(2,2)' );
     Result := StrTransAll( Result, 'CK_', 'AU_' );

     Result := StrTransAll( Result, 'C_ALTAS->', 'KARDEX.' );
     Result := StrTransAll( Result, 'C_ALTAS', 'KARDEX' );
     Result := StrTransAll( Result, 'CURSOS->', 'CURSO.' );
     Result := StrTransAll( Result, 'MAESTROS->', 'MAESTRO.' );

     for  i := Low( aTablas ) to High( aTablas ) do
          if aTablas[i] <> '' then
             Result := StrTransAll( Result, aTablas[ i ]+FLECHA, aTablas[ i ]+PUNTO );
end;

function TransformaCampoTabla( sValor, sCampo, sTabla, sFuncion : string ) : string;
const
     PUNTO = '.';
begin
     Result := sValor;
     Result := StrTransAll( Result, sTabla + PUNTO + sCampo, sFuncion );
     Result := StrTransAll( Result, sCampo, sFuncion );
end;

function TransformaFSalario( const sTexto : string ): string;
 procedure TransSalario(const i,j:integer);
 begin
      if (i>0) AND (j>0) AND (i<>j) then
         Result :=  StrTransAll( Result, Copy(Result,i,j-i),'CB_SALARIO');
 end;
begin
     Result := sTexto;
     TransSalario(PosUpper('SALARIO(CTOD',Result),LastDelimiter('"))',Result));
     TransSalario(PosUpper('SALARIO("',Result),LastDelimiter('"))',Result));
     TransSalario(PosUpper('SALARIO(''', Result),LastDelimiter('''))',Result));
end;

function TransformaNoPeriodo(const sTexto : string): string;
 const Arreglo : Array[1..4] of string =
 ('NOMINA.','PERIODO.','MOVIMIEN.','');
 const Operador : Array[1..7] of string =
 ('=','>','<','<>','$','>=','<=');
 const FOLIO ='NO_FOLIO_%d';
       AFOLIO ='NO@@@_FOLIO_%d';
  var i,j: integer;
      sSustituye,sFolio : string;
begin
     Result := sTexto;

     Result := StrTransAll( Result, 'NO_PERIODO', 'PE_NUMERO' );
     for  i := Low( Arreglo ) to High( Arreglo ) do
     begin
          for j:= 1 to 7 do
          begin
               Result := StrTransAll( Result, Arreglo[i]+'PE_NUMERO'+Operador[j], 'STR('+Arreglo[i]+'PE_@@@NUMERO,3)'+Operador[j] );
               Result := StrTransAll( Result, Arreglo[i]+'PE_NUMERO '+Operador[j], 'STR('+Arreglo[i]+'PE_@@@NUMERO,3)'+Operador[j] );
               Result := StrTransAll( Result, Arreglo[i]+'PE_TIPO'+Operador[j], 'STR('+Arreglo[i]+'PE_@@@TIPO,1)'+Operador[j] );
               Result := StrTransAll( Result, Arreglo[i]+'PE_TIPO '+Operador[j], 'STR('+Arreglo[i]+'PE_@@@TIPO,1)'+Operador[j] );
          end;
     end;
     Result := StrTransAll( Result, 'PE_@@@NUMERO', 'PE_NUMERO' );
     Result := StrTransAll( Result, 'PE_@@@TIPO', 'PE_TIPO' );

     for  i := 1 to 5 do
     begin
          for j:= 1 to 7 do
          begin
               sFolio := Format(FOLIO,[i]);
               sSustituye := 'STR('+Format('NOMINA.'+AFOLIO,[i])+',6)'+Operador[j];
               Result := StrTransAll( Result, 'NOMINA.'+sFolio+Operador[j], sSustituye);
               Result := StrTransAll( Result, 'NOMINA.'+sFolio+' '+Operador[j],sSustituye );
               Result := StrTransAll( Result, sFolio+Operador[j],sSustituye );
               Result := StrTransAll( Result, sFolio+' '+Operador[j],sSustituye);
          end;
     end;
     Result := StrTransAll( Result, 'NO@@@_FOLIO_','NO_FOLIO_');
end;

function TransformaEmpleado( sValor : string; const EsFiltro : Boolean ) : string;
 const Arreglo : Array[1..16] of string =
 ('COLABORA.','AUSENCIA.','CHECADAS.',
  'NOMINA.','KARDEX.','INCAPACI.','KARCURSO.',
  'VACACION.','MOVIMIEN.','ACUMULA.','AHORRO.',
  'CAR_ABO.','KAR_CURSO.','LIQ_EMP.','LIQ_MOV.','');
  var i: integer;
begin
     Result := sValor;

     for  i := Low( Arreglo ) to High( Arreglo ) do
     begin
          if EsFiltro then
          begin
               Result := StrTransAll( Result, Arreglo[i]+'CB_CODIGO=', 'STR('+Arreglo[i]+'CB_@@@CODIGO,5)=' );
               Result := StrTransAll( Result, Arreglo[i]+'CB_CODIGO =', 'STR('+Arreglo[i]+'CB_@@@CODIGO,5)=' );
               Result := StrTransAll( Result, Arreglo[i]+'CB_CODIGO$', 'STR('+Arreglo[i]+'CB_@@@CODIGO,5)$' );
               Result := StrTransAll( Result, Arreglo[i]+'CB_CODIGO $', 'STR('+Arreglo[i]+'CB_@@@CODIGO,5)$' );
          end
          else
          begin
               if (PosUpper('TRIM',Result)>0) OR (PosUpper('LEFT',Result)>0) OR
                  (PosUpper('RIGTH',Result)>0) OR (PosUpper('SUBSTR',Result)>0) OR
                  (PosUpper('VAL',Result)>0) OR (PosUpper('PADL',Result)>0) OR
                  (PosUpper('PADR',Result)>0) OR (PosUpper('PADC',Result)>0) then
                  Result := StrTransAll( Result, Arreglo[i]+'CB_CODIGO', 'STR('+Arreglo[i]+'CB_@@@CODIGO,5)' );
          end;
     end;
     Result := StrTransAll( Result, 'CB_@@@CODIGO', 'CB_CODIGO' );
end;

function TransformaUsuarios( sValor : String; const EsFiltro : Boolean ) : String;
var
   sUsuario : String;

   procedure ReemplazaUsuario( const sCampo : String; sTabla : String );
   const
        PUNTO = '.';
   begin
        sTabla := sTabla + PUNTO;
        Result := StrTransAll( Result, sTabla + sCampo, Format( sUsuario, [ sTabla ] ) );
        Result := StrTransAll( Result, sCampo, Format( sUsuario, [ VACIO ] ) );
   end;

begin
     Result := sValor;

     if EsFiltro then
        sUsuario := 'StrZero( %sUS_CODIGO, 3 )'
     else
        sUsuario := '%sUS_CODIGO';

     ReemplazaUsuario( 'AH_USER', 'AHORRO' );
     ReemplazaUsuario( 'AU_USER', 'AUSENCIA' );

     Result := StrTransAll( Result, 'CR_USER', Format( sUsuario, [VACIO] ) );       // No se Puede saber que tabla es : PCAR_ABO ? ACAR_ABO

     ReemplazaUsuario( 'CH_USER', 'CHECADAS' );
     ReemplazaUsuario( 'CB_USER', 'KARDEX' );                     // En este punto ya se convirtió C_ALTAS a KARDEX
     ReemplazaUsuario( 'LS_USER', 'LIQ_IMSS' );
     ReemplazaUsuario( 'LS_USER', 'LIQ_IMSS' );
     Result := StrTransAll( Result, 'PORFUERA.MO_USER', Format( sUsuario, ['MOVIMIEN.'] ) );
     ReemplazaUsuario( 'MO_USER', 'MOVIMIEN' );

     Result := StrTransAll( Result, 'NO_USER_RJ', 'NO_@@@USER_RJ' );       // Temporal para que el siguiente reemplazo no le afecte
     ReemplazaUsuario( 'NO_USER', 'NOMINA' );
     Result := StrTransAll( Result, 'NO_@@@USER_RJ', 'NO_USER_RJ' );     // Regresa al valor correcto
     Result := StrTransAll( Result, 'ES_USER', Format( sUsuario, [VACIO] ) );

end;

function TransformaTotalesImss( const sTexto : string  ): string;
 const Arreglo : Array[1..7] of string =
 ('TOTAL_MES','TOTAL_RET','TOTAL_INFO','TOTAL_IMSS',
  'SUBTOT_IMSS','SUBTOT_RET','SUBTOT_INFO');
 const Arreglo2 : Array[1..7] of string =
 ('@LS_TOT_IMS+@LS_TOT_RET+@LS_TOT_INF',
  '@LS_TOT_INF','@LS_TOT_RET','@LS_TOT_IMS',
  '@LS_SUB_IMS','@LS_SUB_RET','@LS_SUN_INF');
  var i: integer;
begin
     Result := sTexto;
     for i:= Low(Arreglo) to High(Arreglo) do
     begin
          Result := StrTransAll( Result, Arreglo[i]+'()', Arreglo2[i] );
          Result := StrTransAll( Result, Arreglo[i]+'( )', Arreglo2[i] );
          Result := StrTransAll( Result, Arreglo[i], Arreglo2[i] );
     end;
     Result := StrTransAll( Result, '@LS_', 'LIQ_IMSS.LS_' );

end;

function TransformaAhorroPresta( const sTexto, sTabla : String ) : String;
var
   sFormula, sNewFormula, sResultado, sCaracter: String;
   i, j, iComas, iCaracter, iPos : Byte;
begin
     Result := sTexto;
     if PosUpper( sTabla + '(', Result ) > 0 then
     begin
          sResultado := '';
          while PosUpper( sTabla + '(', Result ) > 0 do
          begin
               iComas:= 0;
               i:= PosUpper( sTabla + '(', Result );
               if ( i > 0 ) then  // Se usa en el Texto
               begin
                    j:= PosUpper( ')', Copy( Result, i, Length( sTexto ) ) );
                    if j > 0 then
                    begin
                         sFormula:= Copy( Result, i, j ); //Formula de AHORRO o PRESTAMO
                    end;
               end;
               for i:= 1 to Length( sFormula ) do
               begin
                    sCaracter := sFormula[i];
                    if sCaracter = ',' then
                       iComas := iComas + 1
                    else if ( iComas = 2 ) and ( sCaracter <> ')' ) and ( sCaracter <> ' ' ) then
                    begin
                         iCaracter := StrToIntDef( sCaracter, 0 );
                         if iCaracter > 0 then
                         begin
                              iCaracter := iCaracter - 1;
                              sCaracter := IntToStr( iCaracter );
                         end;
                    end;
                    sNewFormula := sNewFormula + sCaracter;
               end;
               iPos := PosUpper( sFormula, Result );
               sResultado := sResultado + Copy(Result,1,iPos-1);
               Result := Copy(Result,iPos+length(sFormula), MAXINT);
               sResultado := sResultado + strTransform( sNewFormula, sTABLA, 'P@@@' );
               sNewFormula := '';
          end;
          Result := strTransAll( sResultado, 'P@@@', sTabla )+Result;
     end;
end;

function TransHP( const sValor : string ) : String;
 const K_HPCampo = '@@Campo@@';
       K_HP = 'HPVacio';
begin
     Result := sValor;
     Result := StrTransAll( Result, 'HPMEMO', K_HPCampo );
     Result := StrTransAll( Result, 'HPREV', K_HPCampo );
     Result := StrTransAll( Result, 'HPPAT', K_HPCampo );
     Result := StrTransAll( Result, 'HPGRIS', K_HPCampo );
     Result := StrTransAll( Result, 'HPSOMBRA', K_HPCampo );
     Result := StrTransAll( Result, 'HPGIRA', K_HPCampo );
     Result := StrTransAll( Result, 'HPCONFIGURA', K_HPCampo );
     Result := StrTransAll( Result, 'HPFONT', K_HPCampo );
     Result := StrTransAll( Result, 'HPCIRCULO', K_HPCampo );
     Result := StrTransAll( Result, 'HPRAYA', K_HPCampo );
     Result := StrTransAll( Result, 'HPL', K_HPCampo );
     Result := StrTransAll( Result, 'HPEJECT', K_HPCampo );
     Result := StrTransAll( Result, 'HPFOTO', K_HPCampo );
     Result := StrTransAll( Result, 'HPCAJAGRIS', K_HPCampo );
     Result := StrTransAll( Result, 'HPCAJADIBUJO', K_HPCampo );
     Result := StrTransAll( Result, 'HPCAJA', K_HPCampo );
     Result := StrTransAll( Result, 'HP', K_HPCampo );
     Result := StrTransAll( Result, K_HPCampo, K_HP );
end;

function ConvierteFormula( const sValor: String; const EsFiltro: Boolean ): String;
var
   i : Byte;
begin
     //NO CAMBIAR EL ORDEN DEL CODIGO
     //POR QUE HAY DEPENDENCIAS ENTRE LAS SUSTITUCIONES
     Result := Trim( sValor );

     Result := TransformaTablas( Result );             // Transformacion de Tablas->

     Result := StrTransAll( Result, '.NOT.', ' not ' );
     Result := StrTransAll( Result, '.AND.', ' and ' );
     Result := StrTransAll( Result, '.OR.', ' or ' );
     //Result := StrTransAll( Result, '!', ' not ' );
     Result := StrTransAll( Result, 'ALLTRIM', 'ALLT@RIM' );
     Result := StrTransAll( Result, 'RTRIM', 'RT@RIM' );
     Result := StrTransAll( Result, 'LTRIM', 'LT@RIM' );
     Result := StrTransAll( Result, 'TRIM', 'ALLT@RIM' );
     Result := StrTransAll( Result, 'T@RIM', 'TRIM' );
     Result := StrTransAll( Result, 'IMSS_O_97(', 'IMSS_OBRER(' );
     Result := StrTransAll( Result, 'IMSS_97_EXTRAS(', 'IMSS_EXTRA(' );
     Result := StrTransAll( Result, 'IMSS_P_97(', 'IMSS_PATRO(' );
     Result := StrTransAll( Result, 'INFO_97(', 'INFO_PATRO(' );
     Result := StrTransAll( Result, 'RANGO_STATUS(', 'RANGO_STAT(' );
     Result := StrTransAll( Result, 'QHD(', 'HD(' );
     Result := StrTransAll( Result, 'BASE_INFO', 'SAL_INT' );
     Result := StrTransAll( Result, 'BASE_IMSS', 'SAL_INT' );
     Result := StrTransAll( Result, 'BASE_SAR', 'SAL_INT' );
     Result := StrTransAll( Result, 'C_SUMA_SEMANA', 'SUMA_SEMAN' );
     Result := StrTransAll( Result, 'C_SEMANA', 'SEMANA' );
     Result := StrTransAll( Result, 'COLABORA.CB_STATUS', 'IF(COLABORA.CB_ACTIVO="S"," ","9")' );
     Result := StrTransAll( Result, 'KARDEX.CB_STATUS', 'IF(KARDEX.CB_ACTIVO=9,"9"," ")' );   // TEMPORAL "CB_ACTIVO" - PARA QUE EL SIGUIENTE REEMPLAZO NO PIERDA QUE VIENE DE KARDEX
     Result := StrTransAll( Result, 'CB_STATUS', 'IF(COLABORA.CB_ACTIVO="S"," ","9")' );
     Result := StrTransAll( Result, 'KARDEX.CB_ACTIVO', 'KARDEX.CB_STATUS' );
     Result := StrTransVar( Result, 'LIQUIDA', 'NOMINA.NO_LIQUIDA=1' );
     Result := StrTransVar( Result, 'INDEMNIZA', 'NOMINA.NO_LIQUIDA=2' );
     Result := StrTransAll( Result, 'DOMINGOS', 'DIAS_DOMIN' );
     Result := StrTransAll( Result, 'CB_MICA', 'CB_PASAPOR' );
     Result := StrTransAll( Result, 'CB_HORARIO', 'CB_TURNO' );
     Result := StrTransAll( Result, 'CB_EN_TIJ', 'CB_FEC_RES' );
     Result := StrTransAll( Result, 'CB_DIR1', 'CB_CALLE' );
     Result := StrTransAll( Result, 'CB_DIR2', 'CB_COLONIA' );
     Result := StrTransAll( Result, 'CB_CONTRATO', 'CB_CONTRAT' );
     Result := StrTransAll( Result, 'CB_ESTUDIOS', 'CB_ESTUDIO' );
     Result := StrTransAll( Result, 'CB_FEC_BAJA', 'CB_FEC_BAJ' );
     Result := TransformaCampoTabla( Result, 'CB_PADRE', 'COLABORA', 'PARIENTE(1,1)' );
     Result := TransformaCampoTabla( Result, 'CB_MADRE', 'COLABORA', 'PARIENTE(1,2)' );
     Result := TransformaCampoTabla( Result, 'CB_N_HIJOS', 'COLABORA', 'PARIENTE(0,3)' );
     Result := StrTransAll( Result, 'MOV_APE_PAT', 'IF(EMPTY(COLABORA.CB_APE_MAT),"",COLABORA.CB_APE_PAT)' );
     Result := StrTransAll( Result, 'MOV_APE_MAT', 'IF(EMPTY(COLABORA.CB_APE_MAT),COLABORA.CB_APE_PAT,COLABORA.CB_APE_MAT)' );
     Result := StrTransAll( Result, 'MOV_NOMBRES', 'COLABORA.CB_NOMBRES' );
     Result := StrTransAll( Result, 'MOV_RFC', 'TRANSFORM(COLABORA.CB_RFC,"LLLL-999999-AAA;0")' );
     Result := StrTransAll( Result, 'MOV_IMSS', 'TRANSFORM(COLABORA.CB_SEGSOC,"99-99-99-9999-9;0")' );
     Result := StrTransAll( Result, 'FEC_INICIAL', 'PERIODO.PE_FEC_INI' );
     Result := StrTransAll( Result, 'FEC_FINAL', 'PERIODO.PE_FEC_FIN' );
     Result := StrTransAll( Result, 'FEC_PAGO', 'PERIODO.PE_FEC_PAG' );
     Result := StrTransAll( Result, 'INS(1)', 'MAESTRO.MA_NOMBRE' );
     Result := StrTransAll( Result, 'INS(2)', 'MAESTRO.MA_CEDULA' );
     Result := StrTransAll( Result, 'INS(3)', 'MAESTRO.MA_RFC' );
     Result := StrTransAll( Result, 'RELATED(CU_CURSO, "CURSOS", "CU_NOMBRE")', 'CURSO.CU_NOMBRE' );
     Result := StrTransAll( Result, 'RELATED(CU_CURSO, "CURSOS", "CU_NAME")', 'CURSO.CU_INGLES' );
     Result := StrTransAll( Result, 'RELATED(HO_CODIGO,'+EntreComillas('HORARIO')+','+EntreComillas('HO_DESCRIP')+')','HORARIO.HO_DESCRIP' );
     Result := StrTransAll( Result, 'CU_CURSO', 'CU_CODIGO' );
     Result := StrTransAll( Result, 'KA_FECHA', 'KC_FEC_TOM' );
     Result := StrTransAll( Result, 'KA_EVALUA', 'KC_EVALUA' );
     Result := StrTransAll( Result, 'KA_HORAS', 'KC_HORAS' );
     Result := StrTransAll( Result, 'PU_PUESTO', 'CB_PUESTO' );
     Result := StrTransAll( Result, 'PRETTY_NAME', 'PRETTY_NAM' );
     Result := StrTransAll( Result, 'NAME()', 'PRETTY_NAM' );
     Result := StrTransAll( Result, 'INIT_MONEDAS()', '.T.' );
     Result := StrTransAll( Result, 'COLABORA.PE_NUMERO','COLABORA.CB_NOMNUME' );
     Result := StrTransAll( Result, 'FIELD->', '' );
     Result := StrTransAll( Result, 'MEMVAR->', '' );
     Result := StrTransAll( Result, 'M->', '' );
     Result := StrTransAll( Result, 'COLABORA.CB_PUESTO1', 'ANTES_PTO(1)' );
     Result := StrTransAll( Result, 'COLABORA.CB_PUESTO2', 'ANTES_PTO(2)' );
     Result := StrTransAll( Result, 'COLABORA.CB_PUESTO3', 'ANTES_PTO(3)' );
     Result := StrTransAll( Result, 'COLABORA.CB_CURSO1', 'ANTES_CUR(1)' );
     Result := StrTransAll( Result, 'COLABORA.CB_CURSO2', 'ANTES_CUR(2)' );
     Result := StrTransAll( Result, 'COLABORA.CB_CURSO3', 'ANTES_CUR(3)' );
     Result := StrTransAll( Result, 'CB_PUESTO1', 'ANTES_PTO(1)' );
     Result := StrTransAll( Result, 'CB_PUESTO2', 'ANTES_PTO(2)' );
     Result := StrTransAll( Result, 'CB_PUESTO3', 'ANTES_PTO(3)' );
     Result := StrTransAll( Result, 'CB_CURSO1', 'ANTES_CUR(1)' );
     Result := StrTransAll( Result, 'CB_CURSO2', 'ANTES_CUR(2)' );
     Result := StrTransAll( Result, 'CB_CURSO3', 'ANTES_CUR(3)' );
     Result := StrTransAll( Result, 'CB_SAL_INF', 'CB_SAL_INT' );
     Result := StrTransAll( Result, 'SAL_INF', 'SAL_INT' );
     Result := StrTransAll( Result, 'LM_INCAPACI', 'LM_INCAPAC' );
     Result := StrTransAll( Result, 'CY_SAL_MIN', 'SAL_MIN' );
     Result := StrTransAll( Result, 'CB_CARGO', '"CB_CARGO"' );
     Result := StrTransAll( Result, 'PU_OPCIONA', 'EN_OPCIONA');
     Result := StrTransAll( Result, 'KARCURSO.KA_OPCIONA','.F.');
     Result := StrTransAll( Result, 'KA_OPCIONA','.F.');
     Result := StrTransAll( Result, 'TURNO.TU_MADRUGA','.F.');
     Result := StrTransAll( Result, 'TU_MADRUGA','.F.');
     Result := StrTransAll( Result, 'TURNO.TU_MOD_18','.F.');
     Result := StrTransAll( Result, 'TU_MOD_18','.F.');
     Result := StrTransAll( Result, 'TURNO.TU_REDUCID','.F.');
     Result := StrTransAll( Result, 'TU_REDUCID','.F.');
     Result := StrTransAll( Result, 'VACACION.VA_IMPRESA','.F.');
     Result := StrTransAll( Result, 'VA_IMPRESA','.F.');
     {Result := StrTransAll( Result, 'EMP_VACACION(DATE())','(EMP_VACACI(DATE)=DATE)');
     Result := StrTransAll( Result, 'EMP_PERMISO(DATE())','(EMP_PERMIS(DATE)=DATE)');
     Result := StrTransAll( Result, 'EMP_INCAPA(DATE())','(EMP_INCAPA(DATE)=DATE)');}
     Result := StrTransAll( Result, 'EMP_VACACION(DATE())','(EMP_VACACI(DATE)="S")');
     Result := StrTransAll( Result, 'EMP_PERMISO(DATE())','(EMP_PERMIS(DATE)="S")');
     Result := StrTransAll( Result, 'EMP_INCAPA(DATE())','(EMP_INCAPA(DATE)="S")');

     Result := TransformaCampoTabla( Result, 'CB_FEC_SOL', 'COLABORA', 'POS_KARDEX(1,"ALTA","CB_FEC_CAP")' );
     // Nominas
     Result := StrTransAll( Result, 'NUM_RECIBO', 'STR(NOMINA.NO_FOLIO_1,6)' );
     Result := StrTransAll( Result, 'NOMINA.NO_COMPROB', 'STR(NOMINA.NO_FOLIO_1,6)' );
     Result := StrTransAll( Result, 'NO_COMPROB', 'STR(NO_FOLIO_1,6)' );
     Result := StrTransAll( Result, 'HORAS_EXENTAS()', 'NOMINA.NO_EXENTAS' );
     Result := StrTransAll( Result, 'FOLIO(1)', 'NOMINA.NO_FOLIO_1' );
     Result := StrTransAll( Result, 'FOLIO(2)', 'NOMINA.NO_FOLIO_2' );
     Result := StrTransAll( Result, 'FOLIO(3)', 'NOMINA.NO_FOLIO_3' );
     Result := StrTransAll( Result, 'FOLIO(4)', 'NOMINA.NO_FOLIO_4' );
     Result := StrTransAll( Result, 'FOLIO(5)', 'NOMINA.NO_FOLIO_5' );
     Result := StrTransAll( Result, 'DEF_NOMINA()', 'SET_HEADER(2)' );
     Result := StrTransAll( Result, 'DEF_PERIODO()', 'SET_HEADER(3)' );
     Result := StrTransAll( Result, 'DEF_NOMBRE()', 'TF(14,NOMINA.PE_TIPO)' );
     Result := StrTransAll( Result, 'NOMSTATUS()', 'PERIODO.PE_STATUS' );
     // Vacaciones
     Result := StrTransAll( Result, 'VA_DIAS', 'VA_PAGO' );
     Result := StrTransAll( Result, 'VA_GOZADOS', 'VA_GOZO' );
     Result := StrTransAll( Result, 'VA_FECHA', 'VA_FEC_INI' );
     Result := StrTransAll( Result, 'VA_OTROS_M', 'VA_OTROS' );
     Result := TransformaCampoTabla( Result, 'CB_V_ID_0', 'COLABORA', 'DATOS_VACA(1)' );
     Result := StrTransAll( Result, 'CB_V_ID_1', 'CB_DER_PAG' );
     Result := TransformaCampoTabla( Result, 'CB_V_ID_2', 'COLABORA', '0' );
     Result := TransformaCampoTabla( Result, 'CB_V_PA_0', 'COLABORA', '0' );
     Result := StrTransAll( Result, 'CB_V_PA_1', 'CB_V_PAGO' );
     Result := TransformaCampoTabla( Result, 'CB_V_PA_2', 'COLABORA', '0' );
     Result := TransformaCampoTabla( Result, 'CB_V_GO_0', 'COLABORA', '0' );
     Result := StrTransAll( Result, 'CB_V_GO_1', 'CB_V_GOZO' );
     Result := TransformaCampoTabla( Result, 'CB_V_GO_2', 'COLABORA', '0' );
     Result := StrTransAll( Result, 'VA_NUM_PER', 'VA_NOMNUME' );
     Result := StrTransAll( Result, 'VA_YEARPER', 'VA_NOMYEAR' );
     Result := StrTransAll( Result, 'V_SALDO_PAGO()', 'DATOS_VACA(2)' );
     Result := StrTransAll( Result, 'V_SALDO_GOCE()', 'DATOS_VACA(3)' );
     Result := StrTransAll( Result, 'V_SUMA_IDEAL()', 'COLABORA.CB_DER_PAG' );
     Result := StrTransAll( Result, 'V_SUMA_PAGO()', 'COLABORA.CB_V_PAGO' );
     Result := StrTransAll( Result, 'V_SUMA_GOCE()', 'COLABORA.CB_V_GOZO' );
     // Ausencia
     Result := StrTransAll( Result, 'SHOW_TIPODIA()', 'UPPER( TF(40,AU_STATUS) )' );
     Result := StrTransAll( Result, 'DIA()', 'C_DIA()' );
     Result := StrTransAll( Result, 'AU_STATUS=1', 'AU_STATUS=0' );
     Result := StrTransAll( Result, 'AU_STATUS=2', 'AU_STATUS=1' );
     Result := StrTransAll( Result, 'AU_STATUS=3', 'AU_STATUS=2' );
     Result := StrTransAll( Result, 'RELATED(HO_CODIGO,''HORARIO'',''HO_DESCRIP'')', 'HORARIO.HO_DESCRIP' );
     Result := StrTransAll( Result, 'CHECADA(1,''E'')', 'CHECADA(1,1)' );
     Result := StrTransAll( Result, 'CHECADA(1,''S'')', 'CHECADA(1,2)' );
     Result := StrTransAll( Result, 'CHECADA(2,''E'')', 'CHECADA(2,1)' );
     Result := StrTransAll( Result, 'CHECADA(2,''S'')', 'CHECADA(2,2)' );
     Result := StrTransAll( Result, 'CHECADA(1,''E'',2)', 'CHECADA(1,1,2)' );
     Result := StrTransAll( Result, 'CHECADA(1,''S'',2)', 'CHECADA(1,2,2)' );
     Result := StrTransAll( Result, 'CHECADA(2,''E'',2)', 'CHECADA(2,1,2)' );
     Result := StrTransAll( Result, 'CHECADA(2,''S'',2)', 'CHECADA(2,2,2)' );
     Result := StrTransAll( Result, 'CHECADA(1,''E'',4)', 'CHECADA(1,1,4)' );
     Result := StrTransAll( Result, 'CHECADA(1,''S'',4)', 'CHECADA(1,2,4)' );
     Result := StrTransAll( Result, 'CHECADA(2,''E'',4)', 'CHECADA(2,1,4)' );
     Result := StrTransAll( Result, 'CHECADA(2,''S'',4)', 'CHECADA(2,2,4)' );
     Result := StrTransAll( Result, 'CHECADA(1,''E'',"CH_USER")<>"   "', 'CHECADA(1,1,"US_CODIGO")>0' );
     Result := StrTransAll( Result, 'CB_EMP_STA', 'STATUS_EMP()' );
     // Niveles
     for i := 1 to K_GLOBAL_NIVEL_MAX do
         Result := StrTransAll( Result, format( 'TNIVEL(%s)', [ IntToStr( i ) ] ),
                                        format( 'NIVEL%s.TB_ELEMENT', [ IntToStr( i ) ] ) );
     Result := TransformaCampoTabla( Result, 'KA_STATUS', 'KARCURSO', '1=1' );
     Result := TransformaCampoTabla( Result, 'CB_ESTADO', 'COLABORA', 'ENTIDAD.TB_ELEMENT' );
     Result := StrTransAll( Result, 'SHOW_ST_MOV()', 'UPPER(TF(47,KARDEX.CB_STATUS))' );
     Result := StrTransAll( Result, 'DIAS_AJUSTE', 'DIAS_AJUST' );
     Result := StrTransAll( Result, 'CB_PER_INF', 'CB_PER_VAR' );
     Result := StrTransAll( Result, 'NOMINA.CB_INFCRED', 'COLABORA.CB_INFCRED' );

     //CV
     Result := StrTransAll( Result, 'DISP_TIPOCO(CO_TIPO)', 'TF(25, CONCEPTO.CO_TIPO)' );
     Result := StrTransAll( Result, 'DISP_PROB(AU_IN_FIX)', '' );
     Result := StrTransAll( Result, 'DISP_PROB(AU_OUT_FIX)', '' );
     Result := StrTransAll( Result, 'MEMOLINE', 'LEFT' );
     Result := StrTransAll( Result, 'CLAS_PERM()', 'TF(50,PERMISO.PM_CLASIFI)' );
     Result := StrTransAll( Result, 'DIAS_PER', 'PERIODO.PE_DIAS' );
     Result := StrTransAll( Result, 'DIAS_ACU', 'PERIODO.PE_DIAS_AC' );
     Result := StrTransAll( Result, 'SIN_DIA', 'FECHA_CORTA' );
     Result := StrTransAll( Result, 'VA_TRA_DEL', 'VA_PERIODO' );
     Result := StrTransAll( Result, 'VACACION.VA_TRA_AL', '' );
     Result := StrTransAll( Result, 'ART79_I', 'ART80_SEPA' );

     Result := StrTransAll( Result, 'VA_TRA_AL', '' );
     Result := TransformaEmpleado( Result, EsFiltro );
     Result := TransformaNoPeriodo(Result);
     Result := TransformaFSalario(Result);
     Result := TransformaAhorroPresta( Result, 'AHORRO' );
     Result := TransformaAhorroPresta( Result, 'PRESTAMO' );
     Result := TransformaTotalesImss( Result );
     Result := TransformaUsuarios( Result, EsFiltro );
     Result := TransformaGlobal( Result );
     Result := TransformaGenerico( Result );
     Result := TransLogicos( Result );
     Result := TransCASE( Result );
     Result := TransLlaves( Result ); // Se refiere a convertir { por '"" (comillas)
     Result := oEvaluador.RevisaMigracion( Result )
end;

end.
