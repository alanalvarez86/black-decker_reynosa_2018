unit FPatchDatosMultiple;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, DbTables, Mask,
     FMigrar,
     FileCtrl,
     ZetaWizard,
     ZetaNumero, jpeg, ZetaKeyCombo, Grids, DBGrids, DBCtrls, ZetaDBGrid, DB,
     ImgList, CheckLst, DMigrar, DMigrarDatos, ZetaClientTools, ZetaCommonClasses
     {$ifdef MSSQL},FClienteTRESS,DReportesCliente{$endif};

type
  TPatchDatosMultiple = class(TMigrar)
    OpenDialog: TOpenDialog;
    Parametros: TTabSheet;
    ParametrosGB: TGroupBox;
    VersionLBL: TLabel;
    Version: TComboBox;
    PatchTriggers: TCheckBox;                                    
    PatchSP: TCheckBox;
    PatchDB: TCheckBox;
    RangoScriptsGB: TGroupBox;
    ScriptInicialLBL: TLabel;
    ScriptFinalLBL: TLabel;
    ScriptInicial: TZetaNumero;
    ScriptFinal: TZetaNumero;
    ScriptsTodos: TRadioButton;
    ScriptsRango: TRadioButton;
    Label1: TLabel;
    VerActual: TEdit;
    UpDown: TUpDown;
    RecalcularKardex: TCheckBox;
    varTipoBDanterior: TZetaKeyCombo;
    Label2: TLabel;
    SeleccionEmpresas: TTabSheet;
    GroupBox1: TGroupBox;
    checkTodas: TCheckBox;
    EmpresasGrid: TZetaDBGrid;
    DataSourceEmpresas: TDataSource;
    ImageGrid: TImageList;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    SpeedButton1: TSpeedButton;
    ConfigFile: TEdit;
    DiccionarioDatosCorpDatos: TTabSheet;
    gbDiccionarioDatos: TGroupBox;
    gbActualizarReportesCorp: TGroupBox;
    DiccionarioDatos: TTabSheet;
    TablasDefaultsGB: TGroupBox;
    ListaDefaults: TCheckListBox;
    ArchivoSeek: TSpeedButton;
    lblDiccionarioDatos: TLabel;
    Archivo: TEdit;
    rdgConflicto: TRadioGroup;
    lPrenderDerechos: TCheckBox;
    lblDirectorioReportesCorp: TLabel;
    DirectorioReportesCorp: TEdit;
    SpeedButton2: TSpeedButton;
    gbReportesProfesional: TGroupBox;
    lblDirectorioReportesProfesional: TLabel;
    SpeedButton3: TSpeedButton;
    DirectorioReportesProfesional: TEdit;
    dlgImportar: TOpenDialog;
    dlgImportarReportes: TOpenDialog;
    GroupBox6: TGroupBox;
    gridEmpresasActualizando: TZetaDBGrid;
    Label7: TLabel;
    cbVersionActual: TComboBox;
    cbImportarReportes: TCheckBox;
    cbImportarDiccionario: TCheckBox;
    varTipoBD: TComboBox;
    cbPararEnCasoError: TCheckBox;
    Panel1: TPanel;
    TablaDiccionarioGB: TGroupBox;
    Label4: TLabel;
    SpeedButton4: TSpeedButton;
    Path: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConfigFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure PatchDBClick(Sender: TObject);
    procedure VersionChange(Sender: TObject);
    procedure SetDefaults;
    procedure CargarEmpresas;
    procedure GetVersionActualTodas;
    procedure EmpresasGridDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure MarcarTodas ( lMarcar : boolean );
    procedure MarcarUna;
    function RevisaMarcarTodas : integer;
    procedure SetVersionActualizarEmpresas;
    procedure checkTodasClick(Sender: TObject);
    procedure EmpresasGridDblClick(Sender: TObject);
    procedure EmpresasGridKeyPress(Sender: TObject; var Key: Char);
    procedure PathSeekClick(Sender: TObject);
    function CrearPath: String;
    procedure WizardAfterMove(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
  {$ifdef MSSQL}
    function GetParametrosImportacionDiccionarioRDD : TZetaParams;
  {$endif}
      procedure SpeedButton2Click(Sender: TObject);
    procedure cbVersionActualChange(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EmpresasGridCellClick(Column: TColumn);
    procedure PathChange(Sender: TObject);

  private
    { Private declarations }
    FDirReportesEdit : TEdit;
    FLblDirectorioReportes : TLabel;
    FStreamBitacoraSummary : TFileStream ;
    FSaltarPagina : integer;
    FContinuarTodos : boolean;
  {$ifdef MSSQL}
    FVersionReportes : string;
  {$endif}
    FCambioTipo, FCambioTipoReportes, FCambioVersion, FCambioVersionReportes, FPrimeraVez  : boolean;


    FListaLogDetalle : TStringList;
  {$ifdef MSSQL}
    FClienteTRESSObject : TClienteTRESS;
    FPathDiccionXML  : string;
  {$endif}
    FPathReportesXML : string;
    function GetConfigFile: String;
    function GetVersion: String;
    procedure SetConfigFile( const sValue: String );
    procedure FiltrarSeleccionEmpresas( const lFiltrar : Boolean );
  protected
    { Protected declarations }
     dmMigracionPoblar: TdmMigrarDatos;
     dmMigracionTemp : TdmMigrar;
    procedure SetControls; override;
    procedure PoblarDatosBegin; override;
    procedure InitListaTablasPoblar;
    procedure EscribeBitacoraMensaje( const sMensaje :  string );
    procedure EscribeBitacoraError( const sError :  string );
    function  GetTimeStamp: String;

    procedure StartLogGeneral;
    procedure EndLogGeneral;

    procedure StartProcessPoblar;
    procedure EndProcessPoblar;
    function  PoblarDatos(lShowDialogs: Boolean; pDmMigracion : TdmMigrar): Boolean; override;

    function GetDetailLogFileName: String; override;
    function GetEmpresaActual : string;
    function PuedeContinuar: Boolean; override;

    procedure MakeLogFile;  override;
    procedure ViewLog; override;
    procedure StartLog; override;
    procedure EndLog; override;
    procedure UpdateLogDisplay; override;


  public
    { Public declarations }
    function Init: Boolean;


  end;

const
     K_ERROR_ACT = 'ERROR';
     K_ADVER_ACT = 'ADVERTENCIA';
     K_OK_ACT = 'OK';

var
  PatchDatosMultiple: TPatchDatosMultiple;

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaTressCFGTools,
     FHelpContext,
     DReportes,
     DDBConfig
{$ifdef MSSQL}
     ,DReporteadorCliente
     ,ZBaseThreads

{$endif}
     ,FImportaReportesXML
     ,DXMLTools
     ,DBClient;

{$R *.DFM}

procedure TPatchDatosMultiple.FormCreate(Sender: TObject);
begin
     Version.ItemIndex := 0;
     FSaltarPagina := 0;
     FParadoxPath := Path;
     FListaDefaults := ListaDefaults;
     dmMigracion := TdmReportes.Create( Self );
     dmMigracionPoblar := TdmMigrarDatos.Create( Self );
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( '' );
     ScriptInicial.Valor := 1;
     ScriptFinal.Valor := 999;
     inherited;
     HelpContext := H00007_Actualizar_base_de_datos;
     FListaLogDetalle := TStringList.Create;


end;

procedure TPatchDatosMultiple.SetDefaults;
const

     K_CAPTION_FORMA = 'Actualizar M�ltiples Bases de Datos ';
     {$ifdef INTERBASE}
     //K_PATCH_FILE = 'SP_%s';
     K_PATCH_FILE = '%sSP_%s';
     {$endif}
     {$ifdef MSSQL}
     //K_PATCH_FILE = 'SP_MSSQL_%s';
     K_PATCH_FILE = '%sSP_MSSQL_%s';
     {$endif}
var
   sTipo: String;
   sPath: String;
   lEnabled: Boolean;
begin
     checkTodas.Checked := True;

     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( CrearPath );

     case CompanyType of
          tcRecluta:
          begin
               lEnabled := False;
               sTipo := 'Seleccion';
               sPath := 'Seleccion\Patch\';
               Caption := K_CAPTION_FORMA + ' De Selecci�n';

               FParadoxPath := Self.Path;
               cbImportarReportes.Visible := False;
          end;
          tcVisitas:
          begin
               lEnabled := False;
               sTipo := 'Visitantes';
               sPath := 'Visitas\Patch\';
               FParadoxPath := Self.Path;
               cbImportarReportes.Visible := False;
               Caption := K_CAPTION_FORMA + ' De Visitantes';
          end;
     else
         begin
              lEnabled := True;
              sTipo := 'Datos';
              sPath := 'Fuentes\Patch\';
              FPathReportesXML := 'Fuentes\Reportes\';
              Caption := K_CAPTION_FORMA + ' De Empresas';
              FParadoxPath := Self.Path;
              {$ifdef MSSQL}
              FPathDiccionXML := 'Fuentes\Config\';
              FDirReportesEdit := Self.DirectorioReportesCorp;
              FLblDirectorioReportes := Self.lblDirectorioReportesCorp;
              {$endif}
              {$ifdef INTERBASE}
              FDirReportesEdit := Self.DirectorioReportesProfesional;
              FLblDirectorioReportes := Self.lblDirectorioReportesProfesional;
              {$endif}
              cbImportarReportes.Visible := True;

         end;
     end;
     with RecalcularKardex do
     begin
          Checked := False;
          Enabled := lEnabled;
          Visible := lEnabled;
     end;
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( Format( 'Patch%s.log', [ sTipo ] ) );
     ConfigFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( Format( '%s.db', [ Format( K_PATCH_FILE, [ sPath , sTipo ] ) ] ) );

     SetControls;
end;

procedure TPatchDatosMultiple.FormShow(Sender: TObject);

begin
     inherited;
     PageControl.ActivePage := Inicio;
     if  not ( CompanyType in  [tc3Datos, tcRecluta, tcVisitas] ) then
         CompanyType := tc3Datos;

     varTipoBD.ItemIndex := Ord( CompanyType);
     cbPararEnCasoError.Checked := False;
     FContinuarTodos := True;

     FPrimeraVez := True;

     SetDefaults;
end;

procedure TPatchDatosMultiple.FormDestroy(Sender: TObject);
begin
     TdmReportes( dmMigracion ).Free;
     dmMigracionPoblar.Bitacora := nil;
     dmMigracionPoblar.Free;
     FListaLogDetalle.Free;
     inherited;
end;

function TPatchDatosMultiple.Init: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             CompanyType := Self.CompanyType;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Base De Datos Destino (Alias: '+TargetAlias+ ') !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

function TPatchDatosMultiple.GetConfigFile: String;
begin
     Result := ConfigFile.Text;
end;

function TPatchDatosMultiple.GetVersion: String;
begin
     with Version do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

procedure TPatchDatosMultiple.SetConfigFile( const sValue: String );
begin
     ConfigFile.Text := sValue
end;

procedure TPatchDatosMultiple.ConfigFileSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := GetConfigFile;
          InitialDir := ExtractFileDir( FileName );
          if Execute then
             SetConfigFile( FileName );
     end;
end;

procedure TPatchDatosMultiple.SetControls;
var
   lEnabled: Boolean;
begin
     lEnabled := PatchDB.Checked;
     RangoScriptsGB.Enabled := lEnabled;
     ScriptsTodos.Enabled := lEnabled;
     ScriptsRango.Enabled := lEnabled;
     lEnabled := lEnabled and ScriptsRango.Checked;
     ScriptInicialLBL.Enabled := lEnabled;
     ScriptInicial.Enabled := lEnabled;
     ScriptFinalLBL.Enabled := lEnabled;
     ScriptFinal.Enabled := lEnabled;
end;

procedure TPatchDatosMultiple.VersionChange(Sender: TObject);
var
   lEnabled: Boolean;
begin
     inherited;
     lEnabled := ( GetVersion = '385' );
     with RecalcularKardex do
     begin
          Enabled := lEnabled;
          if not lEnabled then
             Checked := False;
     end;
end;

procedure TPatchDatosMultiple.CargarEmpresas;
begin
     dmDBConfig.EmpresasActualizarConectar(False, eTipoCompany ( varTipoBD.ItemIndex ) );
     GetVersionActualTodas;
     self.DataSourceEmpresas.DataSet := dmDBConfig.cdsEmpresasActualizar;

end;

procedure TPatchDatosMultiple.GetVersionActualTodas;
var
 sVerActual : string;

 procedure AgregaVersionCombo;
 begin
      if ( cbVersionActual.Items.IndexOf( sVerActual ) < 0 ) then
         cbVersionActual.Items.Add(sVerActual);
 end;

 procedure HabilitarColExcepcion( lHabilitar  : boolean );
 begin
     EmpresasGrid.Columns.Items[4].Visible:= lHabilitar;
     EmpresasGrid.Columns.Items[4].Width := 200;
 end;

begin
   cbVersionActual.Items.BeginUpdate;
   cbVersionActual.Items.Clear;
   HabilitarColExcepcion( False );

   with dmDBConfig.cdsEmpresasActualizar do
   begin
       First;
       while not Eof do
       begin
          Edit;
          with TdmReportes( dmMigracion ) do
          begin
               TargetAlias := FieldByName('CM_ALIAS').AsString;
               TargetUserName := FieldByName('CM_USRNAME').AsString;
               TargetPassword := FieldByName('CM_PASSWRD').AsString;
               CompanyType := Self.CompanyType;
               try
                   if AbreBDReceptora(False) then
                   begin
                      sVerActual := GetVerActual;
                      AgregaVersionCombo;
                      FieldByName('CM_VERSION').AsInteger := StrToIntDef( sVerActual, 300 );
                      FieldByName('CM_ACTUALIZAR').AsString := 'S';
                   end
               except
                     on Error: Exception do
                     begin
                          FieldByName('CM_VERSION').AsInteger := -1;
                          FieldByName('CM_ACTUALIZAR').AsString := 'N';
                          FieldByName('CM_EXCEPCION').AsString := Error.Message;
                          HabilitarColExcepcion( True );
                     end;
               end;
          end;

          Next;
       end;
       First;
   end;
   cbVersionActual.Sorted := True;
   cbVersionActual.Items.EndUpdate;

   if ( cbVersionActual.Items.Count > 0 ) then
   begin
        cbVersionActual.ItemIndex := 0;
   end;
end;

procedure TPatchDatosMultiple.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
   lImportarReportes, lImportarDiccionario : boolean;
   sVersionBak : string;

   procedure SetArchivosDefaultReportes;
   {$ifdef MSSQL}
   var
      sVersion : string;
   {$endif}
   begin
        if  (CompanyType = tc3Datos ) then

        begin
       {$ifdef MSSQL}
            sVersion := GetVersion;
            if ( FVersionReportes <> sVersion ) then
               FCambioVersionReportes := TRUE;

            if ( FCambioTipoReportes ) or (FCambioVersionReportes ) then
            begin
                 Archivo.Text := ZetaTressCFGTools.SetFileNameDefaultPath( FPathDiccionXML + 'DICCION ' + sVersion + '.XML' ) ;
                 FVersionReportes := sVersion;
            end;
           // ArchivoReportesXML.Text  := ZetaTressCFGTools.SetFileNameDefaultPath(  FPathReportesXML + 'REPORTES ' + GetVersion + '.XML' );
       {$endif}
            if ( FCambioTipoReportes ) then
            begin
                 FDirReportesEdit.Text :=   ZetaTressCFGTools.SetFileNameDefaultPath(VACIO);
            end;
            FCambioVersionReportes := FALSE;
            FCambioTipoReportes := FALSE;
        end
        else
        begin
            gbReportesProfesional.Visible := False;
        end;
   end;

   procedure ActivarImportacionReportes( lActivar  : boolean ) ;
   begin
        if ( FDirReportesEdit <> nil ) then
        begin
          FLblDirectorioReportes.Enabled := lActivar;
          FDirReportesEdit.Enabled := lActivar;
          gbActualizarReportesCorp.Visible := lActivar;
          gbReportesProfesional.Visible := lActivar;

          gridEmpresasActualizando.Columns.Items[6].Visible:= lActivar ;
          if ( not lActivar ) then
          begin
             FDirReportesEdit.Text := VACIO;
          end;
        end
        else
        begin
             gridEmpresasActualizando.Columns.Items[6].Visible:= False;
        end;

   end;

   procedure ActivarImportacionDiccionario( lActivar  : boolean ) ;
   begin
          lblDiccionarioDatos.Enabled := lActivar;
          Archivo.Enabled := lActivar;
          gbDiccionarioDatos.Visible := lActivar;
          TablasDefaultsGB.Visible := lActivar;
          TablaDiccionarioGB.Visible := lActivar;
          gridEmpresasActualizando.Columns.Items[5].Visible:= lActivar ;
          if  not lActivar then
          begin
               Archivo.Text := VACIO;
          end;
   end;

   function ActualizaraDiccionario : boolean;
   begin
        Result := cbImportarDiccionario.Checked ;
   end;

   function InstalaraReportes : boolean;
   begin
        if  (CompanyType = tc3Datos ) then
        begin
            Result := cbImportarReportes.Checked;
        end
        else
        begin
            Result := False;
        end;

   end;

   function ValidarDirectorioReportes : boolean;
   var
      listaReportes : TStringList;
      oImportaReportes : TImportaReportesXML;
   begin
      oImportaReportes := TImportaReportesXML.Create;
      if StrLLeno( FDirReportesEdit.Text) then
      begin
         if not DirectoryExists( FDirReportesEdit.Text ) then
         begin
              ZWarning( Self.Caption, 'El Directorio de Reportes ' + FDirReportesEdit.Text + ' No Existe', 0, mbOK);
              FDirReportesEdit.SetFocus;
              CanMove := False;
         end
         else
         begin
             listaReportes :=  oImportaReportes.GetReportesFromPath( FDirReportesEdit.Text );
             if ( listaReportes.Count <= 0 ) then
             begin
                  ZWarning( Self.Caption, 'El Directorio de Reportes ' + FDirReportesEdit.Text + ' No Contiene Archivos .RP1', 0, mbOK);
                  FDirReportesEdit.SetFocus;
                  CanMove := False;
             end;
             FreeAndNil ( listaReportes );
         end;
      end
      else
      begin
           ZWarning( Self.Caption, 'El Directorio de Reportes No Est� Especificado', 0, mbOK);
           FDirReportesEdit.SetFocus;
           CanMove := False;
      end;

      Result := CanMove;

      FreeAndNil ( oImportaReportes);
   end;


   procedure SetVersionProxima( sVersionBak : string );
   var
      iVersionDestino, iVersion, iV, iSel : integer;

   begin
        iVersionDestino := StrToIntDef( VerActual.Text , 0 );
        iSel := 0;
        for iV:= Version.Items.Count -1 downto 0 do
        begin
             iVersion := StrToIntDef ( Version.Items.Strings[iV], 0);

             if ( FCambioVersion ) then
             begin
                 if ( iVersion  > iVersionDestino ) then
                 begin
                    iSel := iV;
                    break;
                 end;
             end
             else
             begin
                  if ( iVersion  =  StrToIntDef ( sVersionBak, 0 ) ) then
                  begin
                       iSel := iV;
                       break;
                  end;
             end;
        end;


        Version.ItemIndex := iSel;
        FCambioVersion := FALSE;

   ///GetVersion
        //Version.Items
   end;


        function ShortDir(Dir: String): String;
     var
       SearchRec : TSearchRec;

       Ok        : Integer;
       Ind       : Integer;

       Name      : String;
       TmpDir    : String;
       DirDst    : String;

     begin
       if Copy(Dir, Length(Dir), 1) = '\' then
          Dir := Copy(Dir, 1, Length(Dir)-1);

       TmpDir := Dir;
       while Length(TmpDir) > 3 do begin
         Ok := FindFirst(TmpDir, faDirectory, SearchRec);
         FindClose(SearchRec);

         if Ok = 0 then begin
            if SearchRec.FindData.cAlternateFileName[0] = #0 then begin
               Name := SearchRec.Name;
            end else begin
               Ind  := 0;
               Name := '';
               while Ind < 14 do begin 
                 if (SearchRec.FindData.cAlternateFileName[Ind] = #0) then
                    Ind  := 14
                 else 
                    Name := Name +
                            SearchRec.FindData.cAlternateFileName[Ind];
                 Inc(Ind); 
               end;
            end;

            DirDst := Name + '\' + DirDst;
         end;

         TmpDir := ExtractFileDir(TmpDir);
       end;

       DirDst := TmpDir + DirDst;

       if Copy(DirDst, Length(DirDst), 1) = '\' then
          DirDst := Copy(DirDst, 1, Length(DirDst)-1);

       Result := DirDst;
     end;


     function ValidaDirectorioParadoxMultiple : boolean;
     begin
          Result :=  ValidaDirectorioParadox;
          if ( Result ) then
          begin
              if FlistaDefaults.Count = 0 then
              begin
                   zWarning( Caption, 'No se Encontraron Tablas de Diccionario de Datos en el Directorio:'+Path.Text, 0, mbOK );
                   Result := False;
              end;
          end;
     end;

begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try

             if Wizard.Adelante then
             begin
                  if Wizard.EsPaginaActual( DirectorioDOS ) then
                  begin
                       if not FileExists( GetConfigFile ) then
                       begin
                            zWarning( Caption, 'Archivo de Scripts No Existe', 0, mbOK );
                            ActiveControl := ConfigFile;
                            CanMove := False;
                       end
                       else
                       begin
                            with TdmReportes( dmMigracion ) do
                            begin
                                 SourceSharedPath := ShortDir( ExtractFileDir( GetConfigFile ) );
                                 try
                                    if AbreArchivoParadox( ExtractFileName( GetConfigFile ) ) then
                                    begin
                                         sVersionBak := Version.Text;
                                         if GetVersionList( Version.Items, ExtractFileName( GetConfigFile ) ) then
                                         begin
                                              Version.ItemIndex := 0;
                                              SetVersionProxima(sVersionBak);
                                              CanMove := True;
                                         end
                                         else
                                         begin
                                              zWarning( Caption, 'Archivo de Scripts Es Inv�lido', 0, mbOK );
                                              ActiveControl := ConfigFile;
                                              CanMove := False;
                                         end;
                                    end;
                                 except
                                       on Error: Exception do
                                       begin
                                            CanMove := False;
                                            ShowError( '� Error En Archivo de Scripts !', Error );
                                       end;
                                 end;
                            end;
                       end;
                  end;
                  if Wizard.EsPaginaActual( Parametros ) then
                  begin
                       if ( StrToIntDef( Self.VerActual.Text, 0 ) < 200 ) then
                       begin
                            zWarning( Caption, 'Error en N�mero de Versi�n, Valores Permitidos [ 200...999 ]', 0, mbOK );
                            ActiveControl := VerActual;
                            CanMove := False;
                       end;


                       SetArchivosDefaultReportes;
                       SetVersionActualizarEmpresas;

                       lImportarReportes := InstalaraReportes ;
                       lImportarDiccionario := ActualizaraDiccionario ;

                       if CanMove then
                       begin
                            ActivarImportacionReportes( lImportarReportes );
                            ActivarImportacionDiccionario( lImportarDiccionario );
                       end;

                       {$ifdef MSSQL}
                       if  (CompanyType = tc3Datos ) and ( CanMove ) and (iNewPage = DiccionarioDatos.PageIndex) then
                          iNewPage := DiccionarioDatosCorpDatos.PageIndex;
                       {$endif}

                       if  not lImportarReportes and not lImportarDiccionario then
                           iNewPage  := Migracion.PageIndex;


                  end;

                  if Wizard.EsPaginaActual( DiccionarioDatos ) then
                  begin
                       if ( CompanyType = tc3Datos ) then
                       begin
                            if ( CanMove ) and ( InstalaraReportes)  then
                            begin
                                 CanMove := ValidarDirectorioReportes;
                            end;
                       end;
                       if ( CanMove ) and (iNewPage = DiccionarioDatosCorpDatos.PageIndex) then
                          iNewPage := Migracion.PageIndex;

                       lImportarDiccionario := ActualizaraDiccionario ;

                       if ( lImportarDiccionario ) and ( CanMove ) then
                       begin
                          {$ifdef MSSQL}
                          if ( CompanyType in [tcRecluta, tcVisitas] ) then
                          begin
                             CanMove := ValidaDirectorioParadoxMultiple;
                          end
                          else
                              CanMove := True;
                          {$endif}
                          {$ifdef INTERBASE}
                          CanMove := ValidaDirectorioParadoxMultiple;
                          {$endif}
                       end;


                  end;



                  if Wizard.EsPaginaActual( Inicio ) then
                  begin
                       if  ( eTipoCompany ( varTipoBD.ItemIndex ) = tc3Datos )
                        or ( eTipoCompany ( varTipoBD.ItemIndex ) = tcRecluta )
                        or ( eTipoCompany ( varTipoBD.ItemIndex ) = tcVisitas ) then
                        begin
                           FCambioTipo := ( CompanyType <> eTipoCompany ( varTipoBD.ItemIndex ) ) ;
                           FCambioTipoReportes := FCambioTipo or FPrimeraVez;
                           CompanyType := eTipoCompany ( varTipoBD.ItemIndex ) ;
                           if ( FCambioTipo ) or ( FPrimeraVez ) then
                           begin
                               FCambioTipo := False;
                               CargarEmpresas;
                               SetDefaults;
                               MarcarTodas(True);
                               FiltrarSeleccionEmpresas( False );
                               FPrimeraVez := False;
                           end;
                        end
                        else
                        begin
                             zWarning( Caption, 'Favor de  elegir solo Bases de Datos de tipo Tress, Seleccion de Personal o Visitantes', 0, mbOK );
                             CanMove := False;
                        end;
                  end;
                  if Wizard.EsPaginaActual( SeleccionEmpresas ) then
                  begin
                       if ( RevisaMarcarTodas = 0 ) then
                       begin
                            zWarning( Caption, 'No ha seleccionado Base(s) de Dato(s) para su actualizaci�n', 0, mbOK );
                            CanMove := False;
                       end
                       else
                       begin
                            FCambioVersion := ( Self.VerActual.Text <> cbVersionActual.Text ) or FCambioTipo;
                            FCambioVersionReportes := FCambioVersion;
                            Self.VerActual.Text := cbVersionActual.Text;
                            FiltrarSeleccionEmpresas( True );
                       end;
                  end;

                 {$ifdef MSSQL}
                  if Wizard.EsPaginaActual( DiccionarioDatosCorpDatos ) then
                  begin
                       if ( CompanyType = tc3Datos ) then
                       begin
                            if ( ActualizaraDiccionario ) then
                            begin
                                 if StrLLeno( Archivo.Text) then
                                 begin
                                    if not FileExists( Archivo.Text ) then
                                    begin
                                         ZWarning( Self.Caption, 'El Archivo Diccionario ' + Archivo.Text + ' No Existe', 0, mbOK);
                                         Archivo.SetFocus;
                                         CanMove := False;
                                    end;
                                 end
                                 else
                                 begin
                                      ZWarning( Self.Caption, 'El Archivo Diccionario No Est� Especificado', 0, mbOK);
                                      Archivo.SetFocus;
                                      CanMove := False;
                                 end;
                            end;
                            if ( CanMove ) and ( InstalaraReportes)  then
                            begin
                                  CanMove := ValidarDirectorioReportes;
                            end;
                       end;
                  end;
                  {$endif}

             end
             else
             begin
                  if Wizard.EsPaginaActual( DiccionarioDatosCorpDatos ) then
                  begin
                  {$ifdef MSSQL}
                        if  (CompanyType = tc3Datos )  and ( CanMove ) then
                          iNewPage := Parametros.PageIndex;
                  {$endif}

                  end;

                  if Wizard.EsPaginaActual( Migracion ) then
                  begin
                       if InstalaraReportes or ActualizaraDiccionario then
                       begin
                           iNewPage := DiccionarioDatos.PageIndex;
                           {$ifdef MSSQL}
                             if  (CompanyType = tc3Datos )  and ( CanMove ) then
                               iNewPage := DiccionarioDatosCorpDatos.PageIndex;
                          {$endif}
                       end
                       else
                       begin
                            iNewPage := Parametros.PageIndex;
                       end;

                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TPatchDatosMultiple.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);

var
 sEmpresaActual : string;
 
function GetModoPatchEstructuras: TModoPatch;
begin
     with Result do
     begin
          Habilitado := PatchDB.Checked;
          if ScriptsTodos.Checked then
             Filtro := esTodos
          else
              if ScriptsRango.Checked then
                 Filtro := esRango
              else
                  Filtro := esLista;
          Inicial := ScriptInicial.ValorEntero;
          Final := ScriptFinal.ValorEntero;
          Lista := '';
     end;
end;

function PoblarTablasDefault : boolean;
begin
     TdmReportes( dmMigracion ).CierraBDReceptora;
     dmMigracionPoblar.CierraBDReceptora;
     dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importando Tablas Default' );
     UpdateLogDisplay;
     dmMigracionPoblar.Bitacora := dmMigracion.Bitacora;
     Result  := PoblarDatos(False, dmMigracionPoblar);
     if ( Result ) then
          dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importando Tablas Default Con �xito' )
     else
          dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importando Tablas Default Con Errores' );
     UpdateLogDisplay;
end;

{$ifdef MSSQL}
function IniciarClienteTRESS : boolean;
var
   oEmpresa : OleVariant;
begin
    oEmpresa := dmDBConfig.GetEmpresaActualizar;
    //if VariantNull??
    FClienteTRESSObject := TClienteTRESS.Create( oEmpresa, GetEmpresaActual );
    Result := ( FClienteTRESSObject <> nil );

    if ( Result ) then
    begin
        FClienteTRESSObject.Init;
    end;
end;

function TerminarClienteTRESS : boolean;
begin
    FreeAndNil ( FClienteTRESSObject );
    Result := ( FClienteTRESSObject = nil );
end;


function ImportarDiccionarioRDD : boolean;
var
   zParams : TZetaParams;
   oEmpresa : OleVariant;
   resultadoProc : TProcessInfo;
begin
     if StrLleno (Archivo.Text) then
     begin
         try
            zParams := GetParametrosImportacionDiccionarioRDD;
            dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importando Diccionario de Datos' );
            EscribeBitacoraMensaje(GetTimeStamp + ': Importando Diccionario de Datos : ' + zParams.ParamByName('Archivo').AsString );

            UpdateLogDisplay;
            Application.ProcessMessages;

            try
               oEmpresa := dmDBConfig.GetEmpresaActualizar;
               resultadoProc := dmReporteadorCliente.ImportarDiccionario( oEmpresa, zParams);
               // epsEjecutando, epsOK, epsCancelado, epsError, epsCatastrofico
               FClienteTRESSObject.ReportaProcessDetail( resultadoProc.Folio,  dmMigracion.Bitacora );
               Result := resultadoProc.Status = epsOK;

               if ( Result ) then
                       dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importaci�n de Diccionario de Datos Con �xito' )
               else
                   dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importaci�n de Diccionario de Datos Con Errores' );

            except
                 on Error: Exception do
                 begin
                      dmMigracion.Bitacora.HandleException( 'Error al importar Diccionario de Datos', Error );
                      dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importaci�n de Diccionario de Datos Con Errores' );
                      Result := False;
                 end;
            end;

            UpdateLogDisplay;
            Application.ProcessMessages;

         finally
                FreeAndnil( resultadoProc);
                FreeAndnil( zParams);
         end;
     end
     else
         Result := True;
end;
{$endif}

function ActualizarDiccionario : boolean;
var
 oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;

    {$ifdef MSSQL}
    if ( CompanyType in [tcRecluta, tcVisitas] ) then
    begin
       Result := PoblarTablasDefault;
    end
    else
        Result := ImportarDiccionarioRDD;
    {$endif}
    {$ifdef INTERBASE}
    Result := PoblarTablasDefault;
    {$endif}

    Screen.Cursor := oCursor;
end;


procedure SetStatusBD( sStatus, sVersion : string );
begin
      with dmDBConfig.cdsEmpresasActualizar do
      begin
           if not ( state in [ dsinsert,dsedit ] ) then
              Edit;

           FieldByName('CM_AVANCE_PATCH').AsString := sStatus;

           if StrLleno( sVersion ) then
              FieldByName('CM_VERSION').AsString := sVersion;

      end;

      Application.ProcessMessages;
end;

procedure SetStatusDiccionario( sStatus : string );
begin
      with dmDBConfig.cdsEmpresasActualizar do
      begin
                 if not ( state in [ dsinsert,dsedit ] ) then
              Edit;
           FieldByName('CM_AVANCE_DICCIONARIO').AsString := sStatus;
      end;

      Application.ProcessMessages;
end;

procedure SetStatusReportes( sStatus : string );
begin
      with dmDBConfig.cdsEmpresasActualizar do
      begin
                 if not ( state in [ dsinsert,dsedit ] ) then
              Edit;
           FieldByName('CM_AVANCE_REPORTES').AsString := sStatus;
      end;

      Application.ProcessMessages;
end;

procedure SetStatusTodos( sStatus, sVersion : string );
begin
     SetStatusBD( sStatus, sVersion );
     SetStatusDiccionario( sStatus );
     SetStatusReportes( sStatus );
end;

function ImportarReportesXML : boolean;
var
   oImportaReportes : TImportaReportesXML;
   iResultado : integer;
   listaReportes : TStringList;
begin
   if StrLleno ( FDirReportesEdit.Text ) then
   begin
      iResultado := 0;
      oImportaReportes := TImportaReportesXML.Create;
      oImportaReportes.Bitacora := dmMigracion.Bitacora;

      dmMigracion.Bitacora.AgregaResumen( GetTimeStamp + ': Importando Reportes' );
      UpdateLogDisplay;
      Application.ProcessMessages;

      try

         try
            listaReportes := oImportaReportes.GetReportesFromPath(FDirReportesEdit.Text);
         {$ifdef MSSQL}
            iResultado := oImportaReportes.ImportarReportesFromList(FDirReportesEdit.Text, dmDBConfig.GetCompany, listaReportes, True, True, True );
         {$else}
             iResultado := TdmReportes( dmMigracion ).ImportarReportesRP1(FDirReportesEdit.Text, listaReportes , True, True, True);
         {$endif}
            if ( iResultado = 0 ) then
            begin
               SetStatusReportes( K_OK_ACT );
               dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importando Reportes Con �xito' );
            end
            else
            begin
                SetStatusReportes( K_ADVER_ACT );
                dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importaci�n de Reportes termin� con Advertencias, revisar bit�cora para m�s detalle');
            end;
         except
              on Error: Exception do
              begin
                   SetStatusReportes( K_ERROR_ACT );
                   dmMigracion.Bitacora.HandleException( 'Error al importar Reportes', Error );
                   dmMigracion.Bitacora.AgregaResumen(GetTimeStamp + ': Importaci�n de Reportes Con Errores' );
              end;
         end;


      finally
         Result := iResultado >= 0;
         FreeAndNil ( oImportaReportes );
         FreeAndNil ( listaReportes ) ;
      end;
   end
   else
       Result := True;

   UpdateLogDisplay;
   Application.ProcessMessages;

end;


function ImportarReportesMultiples : boolean;
var
   oCursor: TCursor;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    if CompanyType = tc3Datos then
    begin
        Result := ImportarReportesXML
    end
    else
        Result := True;

     Screen.Cursor  := oCursor;
end;


function ActualizarEmpresa : boolean;
var
   lHuboErrores, lHuboAdvertencias : Boolean;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          try
             Result := AbreBDReceptora(False);
          except
              on Error: Exception do
              begin
                   EscribeBitacoraError( '� Error Al Abrir Base De Datos Destino !:' +  Error.Message );
                   SetStatusTodos ( K_ERROR_ACT , VACIO);
                   Result := False;
              end;
          end;

          {$ifdef MSSQL}
          //Abre Conexion OLE con TRESS
          if ( Result ) then
          begin
              try
                 Result := IniciarClienteTRESS;
              except
                  on Error: Exception do
                  begin
                       EscribeBitacoraError( '� Error Conectarse al Servidor de Procesos COM de TRESS !:' +  Error.Message );
                       SetStatusTodos ( K_ERROR_ACT , VACIO);
                       Result := False;
                  end;
              end;
          end;
          {$endif}

          if ( Result ) then
          begin
             InitCounter( GetPatchSteps );
             StartProcess;
             EnableStopOnError;
             lHuboAdvertencias := False;

             lOk := ActualizarDatos( ExtractFileName( GetConfigFile ),
                                     GetVerActual,
                                     GetVersion,
                                     PatchTriggers.Checked,
                                     PatchSP.Checked,
                                     RecalcularKardex.Checked,
                                     cbImportarDiccionario.Checked,
                                     cbImportarReportes.Checked,
                                     GetModoPatchEstructuras,
                                     lHuboErrores, lHuboAdvertencias , True);


             if ( lOK ) and ( not lHuboErrores )  then
             begin
                if ( lHuboAdvertencias ) then
                   SetStatusBD(K_ADVER_ACT, GetVerActual)
                else
                    SetStatusBD(K_OK_ACT, GetVerActual);

                if ( cbImportarDiccionario.Checked ) then
                begin

                     lOK := ActualizarDiccionario;

                     if lOK then
                        SetStatusDiccionario( K_OK_ACT )
                     else
                         SetStatusDiccionario( K_ERROR_ACT );
                end;

                if ( cbImportarReportes.Checked ) then
                    lOK := ImportarReportesMultiples;
             end
             else
             begin
                SetStatusBD( K_ERROR_ACT , VACIO);
             end;

              if not FContinuarTodos then
                 Bitacora.AgregaResumen('Se interrumpi� el proceso general por un Error de actualizaci�n de parche');
             EndProcess;
          end;
          {$ifdef MSSQL}
          TerminarClienteTRESS;
          {$endif}
          {$ifdef INTERBASE}
           TdmReportes( dmMigracion ).CierraBDReceptora;
          {$endif}

     end;


            
     EndLog;
     Screen.Cursor := oCursor;
end;

begin
      StartLogGeneral;
      FContinuarTodos := True;

      dmDBConfig.cdsEmpresas.DisableControls;
      sEmpresaActual := dmDBConfig.tqCompany.FieldByName('CM_CODIGO').AsString;

      with dmDBConfig.cdsEmpresasActualizar do
      begin
          First;
          while ( not Eof )  and (FContinuarTodos) do
          begin
               if dmDBConfig.tqCompany.Locate('CM_CODIGO', FieldByName('CM_CODIGO').AsString, []) then
               begin
                  Edit;
                  if ( FieldByName('CM_ACTUALIZAR').AsString = ZetaCommonClasses.K_GLOBAL_SI ) then
                  begin
                      Self.TargetAlias := FieldByName('CM_ALIAS').AsString;
                      Self.TargetUserName := FieldByName('CM_USRNAME').AsString;
                      Self.TargetPassword := FieldByName('CM_PASSWRD').AsString;

                      with TdmReportes( dmMigracion ) do
                      begin
                           TargetAlias := FieldByName('CM_ALIAS').AsString;
                           TargetUserName := FieldByName('CM_USRNAME').AsString;
                           TargetPassword := FieldByName('CM_PASSWRD').AsString;
                           Self.VerActual.Text := FieldByName('CM_VERSION').AsString;
                      end;
                      with TdmReportes( dmMigracionPoblar ) do
                      begin
                           TargetAlias := FieldByName('CM_ALIAS').AsString;
                           TargetUserName := FieldByName('CM_USRNAME').AsString;
                           TargetPassword := FieldByName('CM_PASSWRD').AsString;
                      end;

                      ActualizarEmpresa;

                  end;
               end;
               Next;
          end;
          First;
      end;

      dmDBConfig.tqCompany.Locate('CM_CODIGO', sEmpresaActual, []);
      dmDBConfig.tqCompany.EnableControls;



      EndLogGeneral;
      inherited;
end;

procedure TPatchDatosMultiple.PatchDBClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TPatchDatosMultiple.EmpresasGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);

var
   lPuedeActualizar, lHayExcepcion : Boolean;

   function GetPosicion: Integer;
   begin
        Result := Rect.Right - Rect.Left;
        if ( Result > ImageGrid.Width ) then
           Result := Rect.Left + Round( ( Result - ImageGrid.Width ) / 2 ) + 1
        else
           Result := Rect.Left;
   end;


begin

     if ( not DataSourceEmpresas.DataSet.IsEmpty ) and ( Column.FieldName = 'CM_ACTUALIZAR' ) then
        ImageGrid.Draw( EmpresasGrid.Canvas, GetPosicion, Rect.top + 1, BoolToInt( zStrToBool( Column.Field.AsString ) ) )
     else
     begin
          with EmpresasGrid  do
          begin
               with Canvas do
               begin
                    if  not (DataSource.DataSet.IsEmpty) then
                    begin
                         lPuedeActualizar := ( DataSource.DataSet.FieldByName('CM_VERSION').AsString = cbVersionActual.Text );
                         lHayExcepcion  := StrLLeno( DataSource.DataSet.FieldByName('CM_EXCEPCION').AsString );
                         if (not lPuedeActualizar ) then
                         begin
                              Font.Color := clLtGray;
                         end;
                         if ( lHayExcepcion ) then
                         begin
                              Font.Color := clRed;
                         end;
                    end;
               end;
          end;

          EmpresasGrid.DefaultDrawColumnCell( Rect, DataCol, Column, State );
     end;

end;

procedure TPatchDatosMultiple.MarcarTodas ( lMarcar : boolean );
var
     Pos : TBookMark;
     FDataSet : TDataSet;
begin
     FDataSet := dmDBConfig.cdsEmpresasActualizar;

     with FDataSet do
     begin
          if not isEmpty then
          begin
               Pos := GetBookmark;
               DisableControls;
               try
                  First;
                  while not Eof do
                  begin
                       Edit;
                       if ( FieldByName('CM_VERSION').AsString = cbVersionActual.Text  ) then
                          FieldByName('CM_ACTUALIZAR').AsString := zBoolToStr( lMarcar )
                       else
                           FieldByName('CM_ACTUALIZAR').AsString := zBoolToStr( false );

                       Next;
                  end;
                  Edit;
               finally
                      if BookMarkValid( Pos ) then
                         GotoBookMark( Pos );
                      FreeBookMark( Pos );
                      EnableControls;
               end;
          end;
     end;
end;

function TPatchDatosMultiple.RevisaMarcarTodas : integer;
var
     Pos : TBookMark;
     FDataSet : TDataSet;
     lTodosCheck, lActualizar : boolean;
begin
     FDataSet := dmDBConfig.cdsEmpresasActualizar;
     Result := 0;
     lTodosCheck := True;
     with FDataSet do
     begin
          if not isEmpty then
          begin
               Pos := GetBookmark;
               DisableControls;
               try
                  First;
                  while not Eof do
                  begin
                      if ( FieldByName('CM_VERSION').AsString = cbVersionActual.Text  ) then
                      begin
                       lActualizar :=  zStrToBool( FieldByName('CM_ACTUALIZAR').AsString );
                       if (lActualizar) then
                       begin
                          inc(Result);

                       end;
                       lTodosCheck :=  lActualizar and lTodosCheck;
                      end;
                       Next;
                  end;
               finally
                      if BookMarkValid( Pos ) then
                         GotoBookMark( Pos );
                      FreeBookMark( Pos );
                      EnableControls;
               end;
          end;
     end;

     checkTodas.OnClick := nil;
     checkTodas.Checked := lTodosCheck;
     checkTodas.OnClick := checkTodasClick;


end;
procedure TPatchDatosMultiple.MarcarUna;
var
     FDataSet : TDataSet;
begin
     FDataSet := dmDBConfig.cdsEmpresasActualizar;
     inherited;
     with FDataSet do
     begin
          if not IsEmpty then
          begin
               DisableControls;
               Edit;

               if ( FieldByName('CM_VERSION').AsString = cbVersionActual.Text  ) then
               begin
                    if FieldByName('CM_ACTUALIZAR').AsString = K_GLOBAL_NO then
                    begin
                         FieldByName('CM_ACTUALIZAR').AsString := K_GLOBAL_SI;
                    end
                    else
                        FieldByName('CM_ACTUALIZAR').AsString := K_GLOBAL_NO;
               end
               else
                   FieldByName('CM_ACTUALIZAR').AsString := K_GLOBAL_NO;

               Post;
               EnableControls;
               Edit;
          end;
     end;
     RevisaMarcarTodas;
end;

procedure TPatchDatosMultiple.checkTodasClick(Sender: TObject);
begin
  inherited;
  MarcarTodas( checkTodas.Checked );
end;

procedure TPatchDatosMultiple.EmpresasGridDblClick(Sender: TObject);
begin
  inherited;
//  MarcarUna;
end;

procedure TPatchDatosMultiple.EmpresasGridKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  MarcarUna;
end;

procedure TPatchDatosMultiple.PathSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     if Assigned( FParadoxPath ) then
     begin
          with FParadoxPath do
          begin
               sDirectory := Text;
               if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
                  Text := sDirectory;
          end;
     end;
end;

function TPatchDatosMultiple.CrearPath: String;
var
   sPath, sTipo: String;
begin
     sPath  := '%sConfig';
     case CompanyType of
          tcRecluta: sTipo := 'Seleccion\';
          tcVisitas: sTipo := 'Visitas\';
     else
         sTipo := 'Fuentes\';
     end;
     Result := Format( sPath, [ sTipo ] );
end;


procedure TPatchDatosMultiple.WizardAfterMove(Sender: TObject);
begin
  inherited;
  if Wizard.EsPaginaActual( SeleccionEmpresas  ) then
  begin
       FiltrarSeleccionEmpresas( False );
  end;

  if Wizard.EsPaginaActual( DiccionarioDatos ) then
  begin
      {$ifdef MSSQL}
      if ( CompanyType in [tcRecluta, tcVisitas] ) then
      begin
           InitListaTablasPoblar;
      end
      else
      begin
           if FSaltarPagina > 0 then
           begin
                Wizard.AfterMove := nil;
                Wizard.Saltar( FSaltarPagina );
                Wizard.AfterMove := WizardAfterMove;
                FSaltarPagina := 0;
           end;
      end;
      {$endif}
      {$ifdef INTERBASE}
      InitListaTablasPoblar
      {$endif}
  end;


 {
  if Wizard.EsPaginaActual( DiccionarioDatos ) then
  begin
       InitListaTablasPoblar;
  end;}
end;

procedure TPatchDatosMultiple.PoblarDatosBegin;
begin
     dmMigracionPoblar.SourceDatosPath := Self.TablasDefaultPath;
     //dmMigracion.SourceDatosPath := Self.TablasDefaultPath;
end;


procedure TPatchDatosMultiple.StartProcessPoblar;
begin
{     FHayError := False;
     FErrores := 0;
     FSiATodos := True;}

     with dmMigracionPoblar do
     begin
          TargetAlias := Self.TargetAlias;
          TargetUserName := Self.TargetUserName;
          TargetPassword := Self.TargetPassword;
          StartCallBack := Self.StartCounter;
          CallBack := Self.MoveCounter;
          EndCallBack := Self.ClearCounter;
     end;
end;

procedure TPatchDatosMultiple.EndProcessPoblar;
begin
     UpdateLogDisplay;
     {with dmMigracionPoblar do
     begin
          FHayError := ErrorFound;
          MakeLogFile;
     end;}
end;

function TPatchDatosMultiple.PoblarDatos(lShowDialogs: Boolean; pDmMigracion : TdmMigrar): Boolean;
var
   oCursor: TCursor;
   iPasos: Word;
begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     with pDmMigracion do
     begin
          iPasos := Pasos;
          if ( iPasos <= 0 ) then
             Result := True
          else
          begin
               PoblarDatosBegin;
               SourceSharedPath := Self.SourceSharedPath;
               StartProcessPoblar;
               try
                  InitCounter( iPasos );
                  Poblar;
                  StopCounter;
                  Result := True;
               except
                     on Error: Exception do
                     begin
                          if lShowDialogs then
                             ShowError( 'Error Al Poblar Base De Datos', Error )
                          else
                              Result := False;

                     end;
               end;
               EndProcessPoblar;
          end;
     end;
     Screen.Cursor := oCursor;
end;



procedure TPatchDatosMultiple.InitListaTablasPoblar;
var
   iCheck : integer;
begin
     if Assigned( FListaDefaults ) then
     begin
          PoblarDatosBegin;

          with dmMigracionPoblar do
          begin
               GetPasosList( FListaDefaults.Items );
          end;

          if FlistaDefaults.Count > 0 then
          for iCheck := 0 to FlistaDefaults.Count-1 do
                    FlistaDefaults.Checked[iCheck] := True;
          {
          with dmMigracion do
          begin
               GetPasosList( FListaDefaults.Items );
          end;}
     end;
end;


{Importacion de Diccionario de Datos RDD}
procedure TPatchDatosMultiple.ArchivoSeekClick(Sender: TObject);
begin
  inherited;
  Archivo.Text := ZetaClientTools.AbreDialogo( dlgImportar , Archivo.Text, 'xml' );
end;

{$ifdef MSSQL}
function TPatchDatosMultiple.GetParametrosImportacionDiccionarioRDD : TZetaParams;

   function GetConflicto: eConflictoImp;
   begin
        Result := eConflictoImp( rdgConflicto.ItemIndex );
   end;

var
    XML: TdmXMLTools;
begin
     Result := TZetaParams.Create;

     with Result do
     begin
          AddString( 'Archivo', Archivo.Text );
          AddInteger( 'Respetar', Ord( GetConflicto ) );
          AddBoolean( 'PrenderDerechos', lPrenderDerechos.Checked );

          if lPrenderDerechos.Checked then
          begin
               AddString( 'EmpresasConfidenciales', dmReporteadorCliente.GetListaEmpresasConfidenciales );
          end;

          XML := TdmXMLTools.Create(NIL);
          with XML do
          begin
               XMLDocument.LoadFromFile( ParamByName('Archivo').AsString );
               AddString( 'XMLFile', XMLDocument.XML.Text );
               Free;
          end;
     end;
end;

{$endif}

procedure TPatchDatosMultiple.SpeedButton2Click(Sender: TObject);

var
   sDirectory: String;

begin
  inherited;

     if FDirReportesEdit <> nil then
     begin
          with FDirReportesEdit do
          begin
               sDirectory := Text;
               if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
                  Text := sDirectory;
          end;
     end;
end;

procedure TPatchDatosMultiple.EscribeBitacoraError(const sError: string);
begin
     dmMigracion.Bitacora.AgregaError( sError );
     UpdateLogDisplay;
end;

procedure TPatchDatosMultiple.EscribeBitacoraMensaje(
  const sMensaje: string);
begin
     dmMigracion.Bitacora.AgregaMensaje( sMensaje );
     UpdateLogDisplay;
end;

procedure TPatchDatosMultiple.FiltrarSeleccionEmpresas(
  const lFiltrar: Boolean);
const
     K_FILTRO_EMPRESAS_ACTUALIZAR = 'CM_ACTUALIZAR=''S''';
begin
     if ( lFiltrar ) then
     begin
        dmDBConfig.cdsEmpresasActualizar.Filter :=K_FILTRO_EMPRESAS_ACTUALIZAR ;
        dmDBConfig.cdsEmpresasActualizar.Filtered := True;
     end
     else
     begin
        dmDBConfig.cdsEmpresasActualizar.Filter := VACIO;
        dmDBConfig.cdsEmpresasActualizar.Filtered := False;
     end;
end;

procedure TPatchDatosMultiple.cbVersionActualChange(Sender: TObject);
begin
  inherited;
  checkTodas.Checked := True;
  MarcarTodas( True );
end;

procedure TPatchDatosMultiple.StartLogGeneral;
begin
    FStreamBitacoraSummary := TFileStream.Create( LogFileName,  fmCreate );
    FListaLogDetalle.Clear;
    StatusBar.Panels[ 1 ].Text := 'Inicio: ' + TimeToStr( Now );
end;

procedure TPatchDatosMultiple.EndLogGeneral;
begin
    FStreamBitacoraSummary.Free;
    StatusBar.Panels[ 2 ].Text := 'Fin: ' + TimeToStr( Now );
end;

procedure TPatchDatosMultiple.StartLog;
begin
     inherited HayError :=  False;

     //dmMigracion.Bitacora.

     with (  FDetailLog )  do
     begin
          LogErrors := Self.EscribirBitacora;
          FileName := DetailLogFileName;
          FListaLogDetalle.Add( FileName );
          Open;
     end;

     dmMigracion.InitBitacora( FDetailLog, LogSummary );

end;

procedure TPatchDatosMultiple.EndLog;
begin
     dmMigracion.ClearBitacoraErrores;
     FDetailLog.Close;
end;

procedure TPatchDatosMultiple.MakeLogFile;
begin
     dmMigracion.CierraBitacora;
     LogSummary.SaveToStream( FStreamBitacoraSummary );
end;



procedure TPatchDatosMultiple.UpdateLogDisplay;
begin
   with LogDisplay do
     begin
          ItemIndex := ( Items.Count - 1 );
     end;
end;

procedure TPatchDatosMultiple.ViewLog;

procedure ShowLogs;
var
   iLog : integer;
begin
     for iLog:= 0 to  FListaLogDetalle.Count-1 do
     begin
          ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( FListaLogDetalle[iLog] ), ExtractFilePath( FListaLogDetalle[iLog] ), SW_SHOWDEFAULT );
     end;

     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;

begin
     if EscribirBitacora then
     begin
          if HayError then
          begin
               if ZetaDialogo.zErrorConfirm( Caption, 'El Proceso Ha Terminado Con Errores' + CR_LF + '� Desea Ver Las Bit�coras Del Proceso ?', 0, mbOk ) then
                  ShowLogs;
          end
          else
              if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver Las Bit�coras ?', 0, mbOk ) then
                 ShowLogs;
     end;
end;

function TPatchDatosMultiple.GetDetailLogFileName: String;
var
   sExtension: String;
begin
     sExtension := ExtractFileExt( LogFileName );
     if ( sExtension = '' ) then
        Result := Format( '%s ( Detalle )', [ LogFileName ] )
     else
         Result := Format( '%s (%s Detalle )%s', [ Copy( LogFileName, 1, ( Pos( sExtension, LogFileName ) - 1 ) ), GetEmpresaActual , sExtension ] );

end;

function TPatchDatosMultiple.GetEmpresaActual: string;
begin
     Result := VACIO;
     if  not dmDBConfig.cdsEmpresasActualizar.IsEmpty  then
     begin
          Result := dmDBConfig.cdsEmpresasActualizar.FieldByName('CM_CODIGO').AsString;
     end;
end;

procedure TPatchDatosMultiple.SpeedButton3Click(Sender: TObject);
begin
  inherited;
 SpeedButton2Click( Sender );
end;

function TPatchDatosMultiple.GetTimeStamp: String;
begin
     Result := FormatDateTime( 'hh:nn dd/mmm/yy', Now );
end;


function TPatchDatosMultiple.PuedeContinuar: Boolean;
begin
     Result := not Wizard.Cancelado;
     if Result then
     begin
          if not FSiATodos and HayErrores then
          begin
               FSiATodos := not cbPararEnCasoError.Checked;
               FContinuarTodos := FSiATodos;
          end;
     end;
end;

procedure TPatchDatosMultiple.SetVersionActualizarEmpresas;
begin
   with dmDBConfig.cdsEmpresasActualizar do
   begin
       First;
       while not Eof do
       begin
          Edit;
          FieldByName('CM_VERSION_NUEVA').AsString := GetVersion;
          Next;
       end;
       First;
   end;
end;

procedure TPatchDatosMultiple.EmpresasGridCellClick(Column: TColumn);
begin
  inherited;
  MarcarUna;
end;

procedure TPatchDatosMultiple.PathChange(Sender: TObject);
begin
  inherited;

  if ( Wizard.PageControl.ActivePage =  DiccionarioDatos ) then
  begin
      {$ifdef MSSQL}
      if ( CompanyType in [tcRecluta, tcVisitas] ) then
      begin
           InitListaTablasPoblar;
      end
      {$endif}
      {$ifdef INTERBASE}
      InitListaTablasPoblar;
      {$endif}
  end;
end;

end.
