unit FMigrar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ComCtrls, DB, DBTables, FileCtrl,
     Buttons, ExtCtrls, Checklst,
     DMigrar,
     ZetaCommonLists,
     ZetaMigrar,
     ZetaWizard;

const
     K_PREPARAR_LOG = 'MIGRAR.LOG';
type
  eModoMigrar = ( miSinLlaves, miConLlaves, miRevision );
  TMigrar = class(TForm)
    Wizard: TZetaWizard;
    Anterior: TZetaWizardButton;
    Siguiente: TZetaWizardButton;
    Migrar: TZetaWizardButton;
    Salir: TZetaWizardButton;
    PageControl: TPageControl;
    Inicio: TTabSheet;
    DirectorioDOS: TTabSheet;
    Migracion: TTabSheet;
    PanelLog: TPanel;
    ProcesoGB: TGroupBox;
    AvanceProceso: TProgressBar;
    MensajeInicial: TMemo;
    Imagen: TImage;
    LogFileGB: TGroupBox;
    LogFileWrite: TCheckBox;
    LogFileLBL: TLabel;
    LogFile: TEdit;
    LogFileSeek: TSpeedButton;
    LogFileDialog: TSaveDialog;
    LogDisplay: TListBox;
    StatusBar: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure PathSeekClick(Sender: TObject);
    procedure TressDOSPathSeekClick(Sender: TObject);
    procedure LogFileWriteClick(Sender: TObject);
    procedure LogFileSeekClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure ListaDefaultsClickCheck(Sender: TObject);
    procedure ListaDefaultsClick(Sender: TObject);
  private
    { Private declarations }
    FHayError: Boolean;
    FErrores: Integer;

    FTargetAlias: String;
    FTargetUserName: String;
    FTargetPassword: String;

    function GetSourceSharedPath: String;
    function GetLogFileName: String;
    function GetLogSummary: TStrings;
    function GetTablasDefaultPath: String;
    function LlamaExterno( const Modo: eModoMigrar; const dFecha, dMinima: TDate; const sPathSuper: String; const lEsVer260: Boolean ): Boolean;

    procedure SetSourceSharedPath( const Value: String );
    procedure SetLogFileName( const Value: String );
  protected
    { Protected declarations }
    FSiATodos: Boolean;
    FDetailLog: TAsciiLog;
    dmMigracion: TdmMigrar;
    FListaFuente: TListBox;
    FType: eTipoCompany;
    FNombre: TLabel;
    FDirectorio: TLabel;
    FParadoxPath: TEdit;
    FDosPath: TEdit;
    FListaDefaults: TCheckListBox;
    FParamMsg: TStrings;

    property LogSummary: TStrings read GetLogSummary;
    function GetDetailLogFileName: String; virtual;
    function GetSourceDatosPath: String; virtual;
    function HayErrores: Boolean;
    function ValidaDbaseDriver: Boolean;
    function ValidaDirectorioParadox: Boolean;
    function ValidaDirectorioDOS: Boolean;
    function ValidaEmpresa: Boolean;
    function LlenaListaCompanys: Boolean;
    function RevisarArchivos: Boolean;
    function EscribirBitacora: Boolean;
    function PrepararArchivos( const Modo: eModoMigrar; const dFecha, dMinima: TDate; const sPathSuper: String; const lEsVer260: Boolean ): Boolean;
    function PoblarDatos: Boolean; overload;
    function PoblarDatos(lShowDialogs : Boolean;  pDmMigracion : TdmMigrar): Boolean; overload; virtual;
    function MigrarDatos: Boolean;
    function CrearDefaults: Boolean;
    function PuedeContinuar: Boolean;  virtual;
    function BoolToYesNo( const lValue: Boolean ): String;
    procedure InitListaTablas;
    procedure ToggleTabla( const iItem: Integer );
    procedure PoblarDatosBegin;  virtual;
    procedure StartProcess;
    procedure EndProcess;
    procedure EnableStopOnError;
    procedure SetControls; virtual;
    procedure ShowError( const sMensaje: String; Error: Exception ); virtual;
    procedure MakeLogFile;  virtual;
    procedure ViewLog; virtual;
    procedure StartLog; virtual;
    procedure EndLog; virtual;
    procedure UpdateLogDisplay; virtual;
  public
    { Public declarations }
    property HayError: Boolean read FHayError write FHayError;
    property DetailLogFileName: String read GetDetailLogFileName;
    property LogFileName: String read GetLogFileName write SetLogFileName;
    property SourceDatosPath: String read GetSourceDatosPath;
    property SourceSharedPath: String read GetSourceSharedPath write SetSourceSharedPath;
    property TablasDefaultPath: String read GetTablasDefaultPath;
    property TargetAlias: String read FTargetAlias write FTargetAlias;
    property TargetUserName: String read FTargetUserName write FTargetUserName;
    property TargetPassword: String read FTargetPassword write FTargetPassword;
    property CompanyType: eTipoCompany read FType write FType;
    function GetPointer: TMigrar;
    procedure InitCounter( const iSteps: Integer );
    procedure StopCounter;
    procedure StartCounter( Sender: TObject; const sMsg: String );
    procedure MoveCounter( Sender: TObject; var lOk: Boolean );
    procedure ClearCounter( Sender: TObject; var Continue: Boolean );
    procedure SetCompania( const Value: String );
  end;

var
  Migrar: TMigrar;

implementation

uses ZetaCommonTools,
     ZetaDialogo,
     ZDlgSiNoSiTodos,
     ZetaTressCFGTools,
     ZetaCommonClasses;

{$R *.DFM}

{ ********** TMigrar ********* }

procedure TMigrar.FormCreate(Sender: TObject);
begin
     FDetailLog := TAsciiLog.Create;
     FParamMsg := TStringList.Create;
     FType := eTipoCompany( 0 );
     Wizard.Primero;
     SetControls;
end;

procedure TMigrar.FormDestroy(Sender: TObject);
var
   i: Integer;
begin
     if Assigned( FListaFuente ) then
     begin
          with FListaFuente.Items do
          begin
               for i := ( Count - 1 ) downto 0 do
                   TCompanyInfo( Objects[ i ] ).Free;
          end;
     end;
     FParamMsg.Free;
     FDetailLog.Free;
end;

function TMigrar.BoolToYesNo( const lValue: Boolean ): String;
const
     aYesNo: array[ False..True ] of pChar = ( 'NO', 'SI' );
begin
     Result := aYesNo[ lValue ];
end;

function TMigrar.GetPointer: TMigrar;
begin
     Result := Self;
end;

function TMigrar.GetSourceDatosPath: String;
var
   i: Integer;
begin
     if Assigned( FListaFuente ) then
     begin
          with FListaFuente do
          begin
               i := ItemIndex;
               if ( i < 0 ) then
                  Result := ''
               else
                   Result := VerificaDir( TCompanyInfo( Items.Objects[ i ] ).Path );
          end;
     end
     else
         Result := '';
end;

function TMigrar.GetTablasDefaultPath: String;
begin
     if Assigned( FParadoxPath ) then
        Result := FParadoxPath.Text
     else
         Result := '';
end;

function TMigrar.GetSourceSharedPath: String;
begin
     if Assigned( FDOSPath ) then
        Result := Trim( FDOSPath.Text )
     else
         Result := '';
end;

function TMigrar.GetLogFileName: String;
begin
     Result := LogFile.Text;
end;

function TMigrar.GetDetailLogFileName: String;
var
   sExtension: String;
begin
     sExtension := ExtractFileExt( LogFileName );
     if ( sExtension = '' ) then
        Result := Format( '%s ( Detalle )', [ LogFileName ] )
     else
         Result := Format( '%s ( Detalle )%s', [ Copy( LogFileName, 1, ( Pos( sExtension, LogFileName ) - 1 ) ), sExtension ] );
end;

function TMigrar.GetLogSummary: TStrings;
begin
     Result := LogDisplay.Items;
end;

procedure TMigrar.SetCompania( const Value: String );
begin
     MensajeInicial.Text := Format( MensajeInicial.Text, [ Value ] );
end;

procedure TMigrar.SetLogFileName( const Value: String );
begin
     LogFile.Text := Value;
end;

procedure TMigrar.SetSourceSharedPath( const Value: String );
begin
     if Assigned( FDOSPath ) then
        FDOSPath.Text := Value;
end;

procedure TMigrar.InitListaTablas;
begin
     if Assigned( FListaDefaults ) then
     begin
          PoblarDatosBegin;
          with dmMigracion do
          begin
               GetPasosList( FListaDefaults.Items );
          end;
     end;
end;

procedure TMigrar.ToggleTabla( const iItem: Integer );
begin
     if Assigned( FListaDefaults ) then
     begin
          with FListaDefaults do
          begin
               if ( iItem >= 0 ) then
                  TZetaMigrar( Items.Objects[ iItem ] ).Poblar := Checked[ iItem ];
          end;
     end;
end;

procedure TMigrar.ListaDefaultsClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

procedure TMigrar.ListaDefaultsClickCheck(Sender: TObject);
begin
     inherited;
     ToggleTabla( FListaDefaults.ItemIndex );
end;

procedure TMigrar.PathSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     if Assigned( FParadoxPath ) then
     begin
          with FParadoxPath do
          begin
               sDirectory := Text;
               if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
                  Text := sDirectory;
          end;
     end;
end;

procedure TMigrar.LogFileSeekClick(Sender: TObject);
begin
     with LogFileDialog do
     begin
          FileName := LogFileName;
          if Execute then
             LogFileName := FileName;
     end;
end;

procedure TMigrar.TressDOSPathSeekClick(Sender: TObject);
var
   sDirectory: String;
begin
     sDirectory := SourceSharedPath;
     if SelectDirectory( sDirectory, [ sdAllowCreate ], 0 ) then
        SourceSharedPath := sDirectory;
end;

procedure TMigrar.SetControls;
var
   lEnabled: Boolean;
   i: Integer;
begin
     lEnabled := LogFileWrite.Checked;
     LogFileLBL.Enabled := lEnabled;
     LogFile.Enabled := lEnabled;
     LogFileSeek.Enabled := lEnabled;
     if Assigned( FListaFuente ) then
     begin
          with FListaFuente do
          begin
               if ( Items.Count > 0 ) then
               begin
                    i := ItemIndex;
                    if ( i < 0 ) then
                    begin
                         FNombre.Caption := '';
                         FDirectorio.Caption := '';
                    end
                    else
                    begin
                         with TCompanyInfo( Items.Objects[ i ] ) do
                         begin
                              FNombre.Caption := Name;
                              FDirectorio.Caption := Path;
                         end;
                    end;
               end;
          end;
     end;
     if Assigned( FListaDefaults ) then
     begin
          with FListaDefaults do
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    with TZetaMigrar( Items.Objects[ i ] ) do
                         Checked[ i ] := Poblar;
               end;
          end;
     end;
end;

procedure TMigrar.LogFileWriteClick(Sender: TObject);
begin
     SetControls;
end;

function TMigrar.ValidaDbaseDriver: Boolean;
const
     DBASE_DRIVER_NUMBER = 'DB437ES1';
     DBASE_DRIVER_NAME = 'dBASE ESP cp437';
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Result := dmMigracion.DbaseLanguageDriverNameOK( DBASE_DRIVER_NUMBER );
        if not Result then
           zError( Caption, 'El Driver del BDE Para Archivos dBase Debe Ser' +
                            CR_LF +
                            DBASE_DRIVER_NAME + ' ( ' + DBASE_DRIVER_NUMBER + ' )' +
                            CR_LF +
                            'Reconfigure El BDE', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TMigrar.ValidaDirectorioParadox: Boolean;
var
   sValor: String;
begin
     Result := False;
     sValor := FParadoxPath.Text;
     if not DirectoryExists( sValor ) then
     begin
          ZetaDialogo.zWarning( Caption, 'Directorio De Tablas ' + EntreComillas( 'Default' ) + ' No Existe', 0, mbOK );
          ActiveControl := FParadoxPath;
     end
     else
         if not dmMigracion.DriveOK( sValor ) then
         begin
              ZetaDialogo.zWarning( Caption, sValor, 0, mbOK );
              ActiveControl := FParadoxPath;
         end
         else
             Result := True;
end;

function TMigrar.ValidaDirectorioDOS: Boolean;
begin
     Result := DirectoryExists( SourceSharedPath );
     if not Result then
     begin
          zWarning( Caption, 'Directorio Ra�z de Tress DOS No Existe', 0, mbOK );
          ActiveControl := FDOSPath;
     end;
end;

function TMigrar.LlenaListaCompanys: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with FListaFuente do
           begin
                dmMigracion.LlenaListaEmpresas( Items, SourceSharedPath );
                ItemIndex := 0;
           end;
           Result := True;
        except
              on Error: Exception do
              begin
                   ShowError( 'Error Al Llenar Lista De Empresas', Error );
                   ActiveControl := FDOSPath;
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TMigrar.ValidaEmpresa: Boolean;
begin
     Result := False;
     if ( SourceDatosPath = '' ) then
     begin
          zWarning( Caption, 'No Se Ha Especificado La Compa��a De Tress DOS', 0, mbOK );
          ActiveControl := FListaFuente;
     end
     else
         Result := True;
end;

procedure TMigrar.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     ViewLog;
end;

procedure TMigrar.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     with Wizard do
     begin
          if Ejecutando then
             lOk := zConfirm( Caption, '� Desea Interrumpir El Proceso ?', 0, mbNo )
          else
          begin
               lOk := True;
               Close;
          end;
     end;
end;

{ ***** L�gica de Manejo de Migraci�n ********* }

procedure TMigrar.ShowError( const sMensaje: String; Error: Exception );
begin
     zExcepcion( Caption, sMensaje, Error, 0 );
end;

procedure TMigrar.UpdateLogDisplay;
begin
     with LogDisplay do
     begin
          ItemIndex := ( Items.Count - 1 );
     end;
end;

function TMigrar.EscribirBitacora: Boolean;
begin
     Result := LogFileWrite.Checked and ( LogFileName <> '' );
end;

procedure TMigrar.StartLog;
begin
     FHayError := False;
     with FDetailLog do
     begin
          LogErrors := Self.EscribirBitacora;
          FileName := DetailLogFileName;
          Open;
     end;
     dmMigracion.InitBitacora( FDetailLog, LogSummary );
     StatusBar.Panels[ 1 ].Text := 'Inicio: ' + TimeToStr( Now );
end;

procedure TMigrar.EndLog;
begin
     dmMigracion.ClearBitacora;
     StatusBar.Panels[ 2 ].Text := 'Fin: ' + TimeToStr( Now );
     FDetailLog.Close;
end;

procedure TMigrar.MakeLogFile;
begin
     dmMigracion.CierraBitacora;
     LogSummary.SaveToFile( Format( '%s', [ LogFileName ] ) );
end;

procedure TMigrar.ViewLog;

procedure ShowLogs;
begin
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( DetailLogFileName ), ExtractFilePath( DetailLogFileName ), SW_SHOWDEFAULT );
     ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;

begin
     if EscribirBitacora then
     begin
          if HayError then
          begin
               if ZetaDialogo.zErrorConfirm( Caption, 'El Proceso Ha Terminado Con Errores' + CR_LF + '� Desea Ver Las Bit�coras Del Proceso ?', 0, mbOk ) then
                  ShowLogs;
          end
          else
              if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver Las Bit�coras ?', 0, mbOk ) then
                 ShowLogs;
     end;
end;

procedure TMigrar.StartCounter( Sender: TObject; const sMsg: String );
begin
     StatusBar.Panels[ 0 ].Text := sMsg;
     UpdateLogDisplay;
     Application.ProcessMessages;
end;

procedure TMigrar.MoveCounter( Sender: TObject; var lOk: Boolean );
begin
     if Sender is TZetaMigrar then
     begin
          with TZetaMigrar( Sender ) do
          begin
               if ( SourceRecordNumber > 0 ) then
               begin
                    StatusBar.Panels[ 0 ].Text := TargetTableName + ' ' + Trim( Format( '%8.0n', [ SourceRecordNumber / 1 ] ) );
               end;
          end;
     end
     else
         if Sender is TdmMigrar then
         begin
              with TdmMigrar( Sender ) do
              begin
                   StatusBar.Panels[ 0 ].Text := ItemName;
              end;
         end;
     Application.ProcessMessages;
     lOk := PuedeContinuar;
end;

procedure TMigrar.ClearCounter( Sender: TObject; var Continue: Boolean );
begin
     AvanceProceso.StepIt;
     UpdateLogDisplay;
     StatusBar.Panels[ 0 ].Text := '';
     Application.ProcessMessages;
     Continue := PuedeContinuar;
     Continue := not Wizard.Cancelado;
end;

function TMigrar.HayErrores: Boolean;
begin
     with dmMigracion do
     begin
          if ErrorFound and ( ErrorCount > FErrores ) then
          begin
               FErrores := ErrorCount; { Evita que se llame otra vez por el mismo error }
               Result := True;
          end
          else
              Result := False;
     end;
end;

procedure TMigrar.EnableStopOnError;
begin
     FSiATodos := False;
end;

function TMigrar.PuedeContinuar: Boolean;
begin
     Result := not Wizard.Cancelado;
     if Result then
     begin
          if not FSiATodos and HayErrores then
          begin
               case SiNoSiTodos of
                    mrCancel:
                    begin
                         Result := False;
                         FParamMsg.Add( '*** Proceso INTERRUMPIDO Por Usuario ***' );
                    end;
                    mrAll: FSiATodos := True;
               end;
          end;
     end;
end;

procedure TMigrar.InitCounter( const iSteps: Integer );
begin
     with AvanceProceso do
     begin
          Max := iSteps;
          Step := 1;
          Position := 0;
     end;
end;

procedure TMigrar.StopCounter;
begin
     with AvanceProceso do
     begin
          Position := Max;
     end;
end;

procedure TMigrar.StartProcess;
begin
     FHayError := False;
     FErrores := 0;
     FSiATodos := True;
     with dmMigracion do
     begin
          TargetAlias := Self.TargetAlias;
          TargetUserName := Self.TargetUserName;
          TargetPassword := Self.TargetPassword;
          StartCallBack := Self.StartCounter;
          CallBack := Self.MoveCounter;
          EndCallBack := Self.ClearCounter;
     end;
end;

procedure TMigrar.EndProcess;
begin
     UpdateLogDisplay;
     with dmMigracion do
     begin
          FHayError := ErrorFound;
          MakeLogFile;
     end;
end;

procedure TMigrar.PoblarDatosBegin;
begin
     dmMigracion.SourceDatosPath := Self.TablasDefaultPath;
end;



function TMigrar.PoblarDatos : Boolean;
begin
     Result := Self.PoblarDatos( True,  dmMigracion );
end;


function TMigrar.PoblarDatos(lShowDialogs: Boolean; pDmMigracion : TdmMigrar): Boolean;
var
   oCursor: TCursor;
   iPasos: Word;
begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     with pDmMigracion do
     begin
          iPasos := Pasos;
          if ( iPasos <= 0 ) then
             Result := True
          else
          begin
               PoblarDatosBegin;
               SourceSharedPath := Self.SourceSharedPath;
               StartProcess;
               try
                  InitCounter( iPasos );
                  Poblar;
                  StopCounter;
                  Result := True;
               except
                     on Error: Exception do
                     begin
                          if lShowDialogs then
                             ShowError( 'Error Al Poblar Base De Datos', Error )
                          else
                              Result := False;

                     end;
               end;
               EndProcess;
          end;
     end;
     Screen.Cursor := oCursor;
end;



function TMigrar.CrearDefaults: Boolean;
var
   oCursor: TCursor;
   iPasos: Word;
begin
     Result := False;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     with dmMigracion do
     begin
          iPasos := PasosDefault;
          if ( iPasos <= 0 ) then
             Result := True
          else
          begin
               SourceDatosPath := Self.TablasDefaultPath;
               StartProcess;
               try
                  InitCounter( iPasos );
                  PoblarDefault;
                  StopCounter;
                  Result := True;
               except
                     on Error: Exception do
                     begin
                          ShowError( 'Error Al Poblar Base De Datos', Error );
                     end;
               end;
               EndProcess;
          end;
     end;
     Screen.Cursor := oCursor;
end;

function TMigrar.MigrarDatos: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Result := False;
     with dmMigracion do
     begin
          if PoblarDatos and not Wizard.Cancelado then
          begin
               SourceDatosPath := Self.SourceDatosPath;
               SourceSharedPath := Self.SourceSharedPath;
               StartProcess;
               try
                  InitCounter( Steps );
                  Migrar( FParamMsg );
                  StopCounter;
                  Result := True;
               except
                     on Error: Exception do
                     begin
                          ShowError( 'Error En Migraci�n', Error );
                     end;
               end;
               EndProcess;
          end;
     end;
     Screen.Cursor := oCursor;
end;

{ ******* SHELL para llamar ejecutable de Clipper *********** }

function TMigrar.RevisarArchivos: Boolean;
begin
     InitCounter( 0 );
     Result := LlamaExterno( miRevision, Date, Date, '', False );
end;

function TMigrar.PrepararArchivos( const Modo: eModoMigrar; const dFecha, dMinima: TDate; const sPathSuper: String; const lEsVer260: Boolean ): Boolean;
begin
     Result := LlamaExterno( Modo, dFecha, dMinima, sPathSuper, lEsVer260 );
end;

function TMigrar.LlamaExterno( const Modo: eModoMigrar; const dFecha, dMinima: TDate; const sPathSuper: String; const lEsVer260: Boolean ): Boolean;
const
     K_PREPARAR_EXE = 'MIGRAR.EXE';
     K_CAPTION = 'Preparaci�n de Archivos de Tress DOS';
     REPLY_FAILURE = -2;
     REPLY_NOT_FOUND = -1;
     REPLY_OK = 0;
     REPLY_ERROR = 1;
     REPLY_CANCEL = 2;

function WinExecAndWait32( const Programa, Parametros: String; Visibility: Integer ): Integer;
var
   zAppName: array[ 0..512 ] of Char;
   zCurDir: array[ 0..255 ] of Char;
   WorkDir: String;
   StartupInfo: TStartupInfo;
   ProcessInfo: TProcessInformation;
   iExitCode: DWord;
begin
     if FileExists( Programa ) then
     begin
          if StrLleno( Parametros ) then
             StrPCopy( zAppName, Programa + ' ' + Parametros )
          else
              StrPCopy( zAppName, Programa );
          GetDir( 0,WorkDir );
          StrPCopy( zCurDir,WorkDir );
          FillChar( StartupInfo, Sizeof( StartupInfo ), #0 );
          with StartupInfo do
          begin
               cb := Sizeof( StartupInfo );
               dwFlags := STARTF_USESHOWWINDOW;
               wShowWindow := Visibility;
          end;
          if not CreateProcess( nil,
                                zAppName, { pointer to command line string }
                                nil,      { pointer to process security attributes }
                                nil,      { pointer to thread security attributes }
                                False,    { handle inheritance flag }
                                CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS,          { creation flags }
                                nil,               { pointer to new environment block }
                                nil,               { pointer to current directory name }
                                StartupInfo,       { pointer to STARTUPINFO }
                                ProcessInfo ) then
          begin
               Result := REPLY_FAILURE { FRACASO EN CREACION DEL PROCESO }
          end
          else
          begin
               with ProcessInfo do
               begin
                    WaitForSingleObject( hProcess, INFINITE );
                    GetExitCodeProcess( hProcess, iExitCode );
                    Result := iExitCode;
               end;
          end;
          Application.ProcessMessages;
     end
     else
         Result := REPLY_NOT_FOUND; { NO EXISTE EL ARCHIVO }
end;

procedure RevisaBitacora;
begin
     ExecuteFile( 'NOTEPAD.EXE', K_PREPARAR_LOG, Self.SourceDatosPath, SW_SHOWDEFAULT );
end;

var
   sFileName, sParametros: String;
{$ifdef USE_SHORT_PATH_NAME_8_3 }
   sShortPath: array[ 0..255 ] of Char;
{$endif}
begin
     Result := False;
{$ifdef USE_SHORT_PATH_NAME_8_3 }
     GetShortPathName( pChar( ExtractFileDir( Application.ExeName ) ), sShortPath, 255 );
     sFileName := SetFileNamePath( sShortPath, K_PREPARAR_EXE );
{$else}
     sFileName := SetFileNameDefaultPath( K_PREPARAR_EXE );
{$endif}
     sParametros := Self.SourceDatosPath +
                    ' ' +
                    FormatDateTime( 'dd/mm/yyyy', dFecha ) +
                    ' ' +
                    FormatDateTime( 'dd/mm/yyyy', dMinima ) +
                    ' ' +
                    IntToStr( Ord( Modo ) + 1 ) +
                    ' ' +
                    zBoolToStr( lEsVer260 ) +
                    ' ' +
                    sPathSuper;
     try
        case WinExecAndWait32( sFileName, sParametros, SW_SHOWNORMAL ) of
             REPLY_FAILURE: zError( K_CAPTION, 'El Programa ' + CR_LF + sFileName + ' ' + sParametros + CR_LF + ' No Pudo Ser Ejecutado', 0 );
             REPLY_NOT_FOUND: zError( K_CAPTION, 'El Programa ' + CR_LF + sFileName + CR_LF + ' No Fu� Encontrado', 0 );
             REPLY_OK:
             begin
                  Result := True;
                  if ( Modo = miRevision ) then
                  begin
                       LogSummary.LoadFromFile( Self.SourceDatosPath + K_PREPARAR_LOG );
                       FDetailLog.CopiaLista( LogSummary );
                  end;
             end;
             REPLY_ERROR:
             begin
                  if zErrorConfirm( K_CAPTION, '� Se Encontr� Un Error !' + CR_LF + '� Desea Ver La Bit�cora Del Proceso ?', 0, mbOk ) then
                     RevisaBitacora;
             end;
             REPLY_CANCEL:
             begin
                  Result := ZWarningConfirm( K_CAPTION, 'El Proceso Fu� Cancelado' + CR_LF + '� Desea Continuar ?', 0, mbOk );
                  if Result then
                     FParamMsg.Add( 'El Proceso De Preparaci�n Fu� CANCELADO' );
             end;
        else
            zError( K_CAPTION, 'El Proceso No Pudo Ser Efectuado', 0 );
        end;
     except
           on Error: Exception do
           begin
                ShowError( K_CAPTION, Error );
           end;
     end;
end;

end.
