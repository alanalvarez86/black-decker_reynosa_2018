﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FirebirdSql.Data.FirebirdClient;
using FirebirdSql.Data.Client;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml.Linq;
using System.Xml;
using System.Diagnostics;
using System.ServiceProcess;

namespace Wizard_Migracion_Tress
{
    public partial class FPrepara : Form
    {
        public FPrepara()
        {
            InitializeComponent();
            
        }
        public ComboBox combo;
        public string conexionstringfb;
        public string conexionstringsql;
        public double indice;
        public bool parcial;
        public string rutabitacora;
        public string rutadetalle;
        public bool AgregaTipoKardex;
        public bool AgregaTipoPrestamo;
        public DateTime bitacoraini;
        public DateTime bitacorafin;
        public DateTime asistenciaini;
        public DateTime asistenciafin;
        public bool bitacoraparcial;
        public bool asistenciaparcial;
        bool rowabajo = true;
        public string tempcolumnas;
        
        private void FPrepara_Load(object sender, EventArgs e)
        {
            loadposition();
            
        
        }
        public void showhelp()
        {
            System.Windows.Forms.Help.ShowHelp(this, @"Transferencia.chm", HelpNavigator.Topic, "6.htm" );
        
        }
        
        private void DataGridViewPreparacion_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
           
                GridPreparar.BeginEdit(true);
            
        }
        public Point punto;
        private void FPrepara_Shown(object sender, EventArgs e)
        {
            this.Location = punto;
           
        
         
        }
        bool guardado = false;
       
        bool cancelado = false;
        public string nombrearchivo;
        private void guarda_estructura_click(object sender, EventArgs e)
        {
            DialogResult resultado;
            if (temp == true)
            {
                resultado = DialogResult.OK;
                nombrearchivo = "Temp.XML";
                temp = false;
            }
            else
            {
                resultado = saveFileDialog1.ShowDialog();
            }
            if (resultado == DialogResult.OK)
            {
                guardado = true;
                nombrearchivo = saveFileDialog1.FileName;
                this.Cursor = Cursors.WaitCursor;
                guardarBtn_Click();
                Wizard_Migracion_Tress.Program.Migrar.DiccionarioMigracion.elemento.Clear();
                foreach (DataGridViewRow row in GridPreparar.Rows)
                {

                    //  Program.Migrar.QuerysEspeciales.Agregar(row.Cells["Nombre"].Value.ToString(), row.Cells["Campos"].Value.ToString(), row.Cells["Condicion"].Value.ToString()); 
                    if (row.Cells["Nombre"].Value != null)
                    {
                        if (row.Cells[7].Value == null)
                        { row.Cells[7].Value = ""; }
                            Program.Migrar.DiccionarioMigracion.Agregar(
                                    bool.Parse(row.Cells[0].Value.ToString()),
                                    row.Cells[1].Value.ToString(),
                                    row.Cells[2].Value.ToString(),
                                    row.Cells[3].Value.ToString(),
                                    row.Cells[4].Value.ToString(),
                                    row.Cells[5].Value.ToString(),
                                    row.Cells[6].Value.ToString(),
                                    row.Cells[7].Value.ToString());
                        
                        
                    }
                }
                extraQuery[] datos = Program.Migrar.DiccionarioMigracion.elemento.Values.ToArray<extraQuery>();
                //convierte el diccionario en xml
                XElement tablas = new XElement("Transferencia");
                for (int i = 0; i < datos.Length; i++)
                {
                    
                    tablas.Add(new XElement("Tabla",
                        new XElement("Omitir", datos[i].Migrar.ToString()),
                        new XElement("Posicion", double.Parse(datos[i].posicion)),
                        new XElement("Tipo", datos[i].tipo + ""),
                        new XElement("Nombre", datos[i].Nombre + ""),
                        new XElement("Campos", datos[i].Columnas + ""),
                        new XElement("Condicion", datos[i].Condiciones + ""),
                        new XElement("Query", datos[i].query + ""),
                        new XElement("fbrows", datos[i].fbrows + "")));
                    
                }
                //////guarda la tabla
                tablas.Save(nombrearchivo,SaveOptions.None);
                /////
                BtnCarga.PerformClick();
                this.Cursor = Cursors.Default;
            }
            else
            {
                cancelado = true;
            }
            
        }
        private void carga_estructura_click(object sender, EventArgs e)
        {
            DialogResult resultado;
            string filename;
            if (guardado == true)
            {
                 resultado = DialogResult.OK;
                 filename = nombrearchivo;
                 guardado = false;
            }
            else
            {
               resultado = openFileDialog1.ShowDialog();
               filename = openFileDialog1.FileName;
            }
            if (resultado == DialogResult.OK)
            {
                
                this.Cursor = Cursors.WaitCursor;

                XElement datos = XElement.Load(filename);
       
                GridPreparar.Rows.Clear();
                Program.Migrar.DiccionarioMigracion.elemento.Clear();

                string[] row = new string[8];
                int i = 0;
                int filas = 0;
                foreach (XElement datos2 in datos.Elements())
                {

                    foreach (XElement dato3 in datos2.Elements())
                    {


                        if (dato3.FirstNode == null)
                        {
                            row[i] = "";
                        }
                        else
                        {
                            row[i] = dato3.FirstNode.ToString();
                        }

                        i++;
                    }
                    i = 0;
                    GridPreparar.Columns[1].ValueType = typeof(double);

                    GridPreparar.Rows.Add();
                    for (int j = 0; j < row.Length; j++)
                    {
                        if (j == 1)
                        {
                            GridPreparar[1, filas].Value = double.Parse(row[j]);
                            string tipo = GridPreparar.Columns[1].CellType.ToString();
                        }
                        GridPreparar[j, filas].Value = row[j];


                    }


                    if (GridPreparar[2, filas].Value.ToString() == "Tabla")
                    {



                    }

                    filas++;
                    Program.Migrar.DiccionarioMigracion.Agregar(bool.Parse(row[0]), row[1], row[2], row[3], row[4], row[5], row[6],row[7]);
                }



                for (i = 0; i < filas; i++)
                {
                    GridPreparar[1, i].Value = double.Parse(GridPreparar[1, i].Value.ToString());
                }
                this.Cursor = Cursors.Default;
            }
        }
        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;
        int rightmouseclick;
        private void DGVPreparar_MouseMove(object sender, MouseEventArgs e)
        {
           
        }

        private void DGVPreparar_MouseDown(object sender, MouseEventArgs e)
        {
           
        }

        private void DGVPreparar_DragOver(object sender, DragEventArgs e)
        {
         
        }

        private void DGVPreparar_DragDrop(object sender, DragEventArgs e)
        {
         
    
        }

        private void guardarBtn_Click()
        {
            for (int i = 0; i < GridPreparar.Rows.Count; i++)
            {
                
                GridPreparar[1, i].Value = (double)i;

            }
        }

        private void Ordenarbtn_Click(object sender, EventArgs e)
        {
            
                GridPreparar.Sort(Posicion, ListSortDirection.Ascending);
                GridPreparar.Width = GridPreparar.Width -1;
        }

        private void DGVPreparar_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (rowabajo)
            {
                if (e.RowIndex > 1)
                    GridPreparar[1, e.RowIndex - 1].Value = (double)(e.RowIndex - 1);
            }
            rowabajo = false;
        }
        bool front = false;
        private void FPrepara_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (back == false && front == false)
            {
                if (MessageBox.Show(this, "¿Seguro que deseas Salir?", "Transferencia de Datos", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    Environment.Exit(Environment.ExitCode);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                if (back == true)
                {
                    Program.Principal.Location = this.Location;
                    Program.Principal.Show();
                    Program.Principal.MoverHoja(-1);
                }
            }
        }
        bool temp = false;
        private void Migrar_click(object sender, EventArgs e)
        {
            cancelado = false;
            DialogResult diag;
            diag = MessageBox.Show(this, "¿Deseas guardar estructura de Transferencia antes de iniciar el proceso?", "Transferencia de Datos", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            BtnOrdena.PerformClick();
            if (diag== DialogResult.Yes)
            {
  
            }
            else
            {
            
                temp=true;
            }
            if(diag==DialogResult.Cancel)
            {
                
            }
            else
            {
                
                    BtnGuarda.PerformClick();
                    if (cancelado == false)
                    {
                        Program.Form1 = new FMigrar();
                        Program.Form1.StartPosition = FormStartPosition.CenterScreen;
                      
                        Program.Form1.punto = this.Location;
                        front = true;
                        this.Close();
                        Program.Form1.Show();
                }
            }
            
           
        }

        private void DGVPreparar_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            
        }

        private void DGVPreparar_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
           
        }
        private void calcularRows()
        {
            FbConnection conexionfb = new FbConnection(conexionstringfb);
            FbCommand queryfb = new FbCommand();
            queryfb.Connection = conexionfb;
            SqlConnection conexionsql = new SqlConnection(conexionstringsql);
            SqlCommand querysql = new SqlCommand();
            querysql.Connection=conexionsql;
            querysql.Connection.Open();
            queryfb.Connection.Open();
            totalrows = 0;
            for (int i = 0; i < GridPreparar.RowCount; i++)
            {
                if (GridPreparar[2, i].Value != null)
                {
                    if (GridPreparar[2, i].Value.ToString() == "Tabla" && GridPreparar[0, i].Value.ToString() == true.ToString())
                    {
                        if (GridPreparar[3, i].Value.ToString() == "BITACORA" || GridPreparar[3, i].Value.ToString() == "AUSENCIA" || GridPreparar[3, i].Value.ToString() == "MOVIMIEN" || GridPreparar[3, i].Value.ToString() == "CHECADAS")
                        {

                            Program.reiniciaServidor(ref conexionfb);
                        }
                        queryfb.CommandText = "select count(*) from " + GridPreparar[3, i].Value.ToString() + " " + GridPreparar[5, i].Value.ToString();
                        querysql.CommandText = queryfb.CommandText;
                        try
                        {
                            int rows = int.Parse(queryfb.ExecuteScalar().ToString());
                            GridPreparar[7, i].Value = rows;
                            GridPreparar[8, i].Value = querysql.ExecuteScalar();
                            totalrows = totalrows + rows;
                        }
                        catch
                        {
                            GridPreparar[0, i].Value = false;

                        }
                    }
                    if (GridPreparar[2, i].Value.ToString() == "Proceso" && GridPreparar[0, i].Value.ToString() == true.ToString())
                    {
                        if (GridPreparar[3, i].Value.ToString() == "TipoKardex")
                        {
                            queryfb.CommandText = "select count(Distinct(CB_TIPO)) from Kardex where CB_TIPO not in (select tb_Codigo from TKARDEX)";
                            try
                            {
                                int rows = int.Parse(queryfb.ExecuteScalar().ToString());
                                GridPreparar[7, i].Value = rows;
                                GridPreparar[8, i].Value = "?";
                                totalrows = totalrows + rows;
                             
                            }
                            catch
                            {
                                GridPreparar[0, i].Value = false;
                            }
                        }
                        if (GridPreparar[3, i].Value.ToString() == "TipoPrestamo")
                        {
                            queryfb.CommandText = "select count(Distinct(PR_TIPO)) from PRESTAMO where PR_TIPO not in (select TB_CODIGO from TPRESTA)";
                            try
                            {
                                int rows = int.Parse(queryfb.ExecuteScalar().ToString());
                                GridPreparar[7, i].Value = rows;
                                GridPreparar[8, i].Value = "?";
                                totalrows = totalrows + rows;

                            }
                            catch
                            {
                                GridPreparar[0, i].Value = false;
                            }
                        }
                    }

                }
            }
            dtiempoestimado = (totalrows / 4500);
            stiempoestimado = ((int)(dtiempoestimado / 3600)).ToString() + ":" + ((int)(((int)dtiempoestimado % 3600) / 60)).ToString("00") + ":" + ((int)dtiempoestimado % 60).ToString("00");
            queryfb.Connection.Close();
            querysql.Connection.Close();
        
        }
        public string[] tablaarray() //arreglo de tablas a migrar
        {
            string[] array;
            if (Program.MigrarComparte == false)
            {
                array = new string[] {   "GLOBAL",
                                 "COLABORA",
                                "TAHORRO",
                                "AHORRO",
                                "ACAR_ABO",
                                "ACCESLOG",
                                "EXPEDIEN",
                                "ACCIDENT",
                                "CURSO",
                                "ACCION",
                                "ACCREGLA",
                                "CONCEPTO",
                                "ACUMULA",
                                "AGUINAL",
                                "ANTESCUR",
                                "ANTESPTO",
                                "AREA",
                                "NUMERICA",
                                "T_ART_80",
                                "ART_80",
                                "AR_TEM_CUR",
                                "ASIGNA",
                                "AULA",
                                "AUSENCIA",
                                "BITACORA",
                                "BREAKS",
                                "BRK_HORA",
                                "CAFREGLA",
                                "CAF_COME",
                                "INVITA",
                                "CAF_INV",
                                "CALCURSO",
                                "CALIFICA",
                                "REPORTE",
                                "CAMPOREP",
                                "CAMPO_AD",
                                "CAMPO_EX",
                                "CAUSACCI",
                                "CCURSO",
                                "CEDSCRAP",
                                "CEDULA",
                                "CED_EMP",
                                "CED_INSP",
                                "CED_WORD",
                                "CERTIFIC",
                                "PUESTO",
                                "PTO_CERT",
                                "CERNIVEL",
                                "CHECADAS",
                                "CLASIFI",
                                "CLASITMP",
                                "COLONIA",
                                "COMPARA",
                                "COMPETEN",
                                "COMPONEN",
                                "COMP_CAL",
                                "FAM_PTO",
                                "COMP_FAM",
                                "COMP_MAP",
                                "COMP_PTO",
                                "CONSULTA",
                                "CONTEO",
                                "CONTRATO",
                                "CTABANCO",
                                "CTA_MOVS",
                                "CURSOPRE",
                                "SESION",
                                "INSCRITO",
                                "RESERVA",
                                "CUR_ASIS",
                                "CUR_REV",
                                "DECLARA",
                                "TDEFECTO",
                                "DEFECTO",
                                "PARTES",
                                "DEFSTEPS",
                                "DESCTIPO",
                                "DESC_FLD",
                                "PERFIL",
                                "DESC_PTO",
                                "DIAGNOST",
                                "DICCION",
                                "DIMENSIO",
                                "DOCUMENTO",
                                "EDOCIVIL",
                                "EMBARAZO",
                                "EMP_COMP",
                                "EMP_PLAN",
                                "POLIZA_MED",
                                "EMP_POLIZA",
                                "EMP_PROG",
                                "ENTIDAD",
                                "ENTRENA",
                                "ENTNIVEL",
                                "ESTABLEC",
                                "ESTUDIOS",
                                "EVENTO",
                                "EXTRA1",
                                "EXTRA10",
                                "EXTRA11",
                                "EXTRA12",
                                "EXTRA13",
                                "EXTRA14",
                                "EXTRA2",
                                "EXTRA3",
                                "EXTRA4",
                                "EXTRA5",
                                "EXTRA6",
                                "EXTRA7",
                                "EXTRA8",
                                "EXTRA9",
                                "PERIODO",
                                "NOMINA",
                                "FALTAS",
                                "TURNO",
                                "FESTIVO",
                                "FOLIO",
                                "FON_TOT",
                                "FON_EMP",
                                "FON_CRE",
                               
                                "GRUPO_AD",
                                "GRUPO_EX",
                                "GR_AD_ACC",
                                "HORARIO",
                                "HUELLA",
                                "IMAGEN",
                                "INCAPACI",
                                "INCIDEN",
                                "ISR_AJUSTE",
                                "KARCURSO",
                                "TKARDEX",
                                "KARDEX",
                                "KARINF",
                                "KAR_AREA",
                                "KAR_CERT",
                                "KAR_EMPSIL",
                                "KAR_FIJA",
                                "TOOL",
                                "KAR_TOOL",
                                "LAY_PRO_CE",
                                "MAQUINA",
                                "LAY_MAQ",
                                "LECTURAS",
                                "LEY_IMSS",
                                "LIQ_IMSS",
                                "LIQ_EMP",
                                "LIQ_MOV",
                                "MAESTRO",
                                "MAQ_CERT",
                                "MEDICINA",
                                "MED_ENTR",
                                "MISREPOR",
                                "MODULA1",
                                "MODULA2",
                                "MODULA3",
                                "MONEDA",
                                "MOTACCI",
                                "MOTSCRAP",
                                "MOT_AUTO",
                                "MOT_BAJA",
                                "MOT_CHECA",
                                "MOT_TOOL",
                                "MOVIMIEN",
                                "MOV_GRAL",
                                "MUNICIPIO",
                                "NIVEL1",
                                "NIVEL2",
                                "NIVEL3",
                                "NIVEL4",
                                "NIVEL5",
                                "NIVEL6",
                                "NIVEL7",
                                "NIVEL8",
                                "NIVEL9",
                                "NIV_PTO",
                                "NIV_DIME",
                                "NOMPARAM",
                                "OCUPA_NAC",
                                "OPERA",
                                "ORDFOLIO",
                                "OTRASPER",
                                "PARIENTE",
                                "TPRESTA",
                                "PRESTAMO",
                                "PCAR_ABO",
                                "PENSION",
                                "PEN_PORCEN",
                                "PERMISO",
                                "PLAZA",
                                "POL_TIPO",
                                "POL_HEAD",
                                "POLIZA",
                                "POL_VIGENC",
                                "SSOCIAL",
                                "PRESTACI",
                                "REGLAPREST",
                                "PRESTAXREG",
                                "RSOCIAL",
                                "RPATRON",
                                "PRIESGO",
                                "PROCESO",
                                "PROV_CAP",
                                "PTOFIJAS",
                                "PTOTOOLS",
                                "PTO_DIME",
                                "QUERYS",
                                "REP_AHO",
                                "REP_EMPS",
                                "REP_PTU",
                                "RIESGO",
                                "SAL_MIN",
                                "SCRAP",
                                "SILLA_LAY",
                                "SILLA_MAQ",
                                "WORDER",
                                "STEPS",
                                "SUPER",
                                "SUP_AREA",
                                "SUSCRIP",
                                "SY_ALARMA",
                                "TACCIDEN",
                                "TALLA",
                                "TCAMBIO",
                                "TCOMPETE",
                                "TCONSLTA",
                                "TCTAMOVS",
                                "TCURSO",
                                "TESTUDIO",
                                "TMAQUINA",
                                "TMPBALA2",
                                "TMPBALAMES",
                                "TMPBALAN",
                                "TMPCALEN",
                                "TMPCALHR",
                                "TMPDEMO",
                                "TMPDIMM",
                                "TMPDIMMTOT",
                                "TMPESTAD",
                                "TMPEVENT",
                                "TMPFOLIO",
                                "TMPHORAS",
                                "TMPLABOR",
                                "TMPLISTA",
                                "TMPMOVS",
                                "TMPNOM",
                                "TMPPROMVAR",
                                "TMPROTA",
                                "TMPROTAI",
                                "TMPSALAR",
                                "TMPTOTAL",
                                "TMUERTO",
                                "TOPERA",
                                "TPARTE",
                                "TPENSION",
                                "TPERIODO",
                                "TRANSPOR",
                                "VACACION",
                                "VACAPLAN",
                                "VIVE_CON",
                                "VIVE_EN",
                                "WOFIJA",
                                "WORKS",
                                "TIPO_SAT",
                                };

            }
            else
            {
            array = new string[]
            {
            "COMPANY",
            "GRUPO",
            "ACCESO",
            "USUARIO",
            "ARBOL",
            "BITACORA",
            "BUZONSUG",
            "CLAVES",
            "CONTIENE",
            "CORREO",
            "DISPOSIT",
            "DISXCOM",
            "DOCUMENT",
            "EMP_ID",
            "EVENTO",
            "GLOBAL",
            "NIVEL0",
            "NOTICIA",
            "PAGINA",
            "PASSLOG",
            "POLL",
            "PRINTER",
            "REPORTAL",
            "REP_COLS",
            "REP_DATA",
            "SP_PATCH",
            "TARJETA",
            "TDOCUMEN",
            "TEVENTO",
            "TNOTICIA",
            "USER_BIT",
            "CUENTATIM",
            };
              
            }
            return array;
        }
        private void GridPreparar_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == 0)
                {
                    if (GridPreparar[0, e.RowIndex].Value.ToString() == false.ToString())
                    {
                        GridPreparar.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
                    }
                    else
                    {
                        GridPreparar.Rows[e.RowIndex].DefaultCellStyle.BackColor = default(Color);
                    }
                }
            }
           
        }
        int enter = 0;
        private void GridPreparar_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (enter == 0)
            {

                if (e.ColumnIndex == 4)
                {

                    FTableSelector TableSelector = new FTableSelector();
                    TableSelector.conexionstring = conexionstringfb;
                    if (GridPreparar[e.ColumnIndex, e.RowIndex].Value != null)
                    {
                        tempcolumnas = GridPreparar[e.ColumnIndex, e.RowIndex].Value.ToString();

                    }
                    else
                    {
                        tempcolumnas = "*";
                    }
                    TableSelector.tabla = "'" + GridPreparar["Nombre", e.RowIndex].Value.ToString() + "'";
                    TableSelector.columnas = tempcolumnas;
                    TableSelector.ShowDialog();
                    GridPreparar[e.ColumnIndex, e.RowIndex].Value = tempcolumnas;
                    e.Cancel = true;
                    enter++;
                }
            }
            else 
            {
                enter = 0;
                e.Cancel=true;
            }
           
        }

        private void Calcular_filas_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            calcularRows();
            LblTstimado.Text = "Tiempo Estimado = " + stiempoestimado;
            this.Cursor = Cursors.Default;
           //firebirdserver.Start();
           // while (LblTstimado.Text != "asd");
        }
        double totalrows;
        double dtiempoestimado;
        string stiempoestimado;
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
                foreach (DataGridViewRow row in GridPreparar.Rows)
                {
                    row.Cells[0].Value = checkBox1.Checked;
                   
                }
            
        }
        bool back = false;
        private void button2_Click(object sender, EventArgs e)
        {
            Program.Principal.punto = this.Location;
            back = true;
            this.Close();
            
            
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LblTstimado_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FPrepara_SizeChanged(object sender, EventArgs e)
        {
            loadposition();
        }
        public void loadposition()
        {
            foreach (DataGridViewColumn cols in GridPreparar.Columns)
            {
                cols.HeaderCell.Style.Alignment=  DataGridViewContentAlignment.MiddleCenter;
            }
            panel1.Width = this.Width;
            panel2.Width=this.Width;
          
            GridPreparar.Height = this.Height - 196;

            GridPreparar.Left = 0;
            GridPreparar.Width = this.ClientSize.Width - 7;
            panel1.Top = this.Height - 81;
            LblTstimado.Left = GridPreparar.Right - LblTstimado.Width;
            LblTstimado.Top = GridPreparar.Bottom + 10;
            checkBox1.Left = GridPreparar.Left+10;
           
            label1.Top = checkBox1.Top + 3; ;
            label1.Left = checkBox1.Right + 10;
            BtnOrdena.Left = 14;
            BtnGuarda.Left = 149;
            BtnCalcula.Left= 419;
            BtnCarga.Left = 284;
            BtnVolver.Left = this.Width - 289;
            BtnMigra.Left = this.Width - (155);
            textBox1.Left = GridPreparar.Right - (textBox1.Width + 5);
            label3.Left = textBox1.Left - 60;
        }
        public void loadgrid()
        {
            combo = new ComboBox();
            combo.Items.Add("Tabla");
            combo.Items.Add("Query");
            int i = 0;



            Program.Migrar = new Migrar(conexionstringfb, conexionstringsql, rutabitacora);
            if (parcial)
            {
                // i = indice;
            }
            else
            {
                i = 0;
            }
            if (Program.MigrarComparte == false)
            {
                if (AgregaTipoKardex)
                {


                    GridPreparar.Rows.Add();
                    GridPreparar["Omitir", i].Value = true;
                    GridPreparar["Posicion", i].Value = (double)i;
                    GridPreparar["Tipo", i].Value = "Proceso";
                    GridPreparar["Nombre", i].Value = "TipoKardex";
                    GridPreparar["Campos", i].Value = "Distinct(CB_TIPO)";
                    GridPreparar["Condicion", i].Value = "where CB_TIPO not in (select tb_Codigo from TKARDEX)";
                    GridPreparar["Query", i].Value = "";


                    // DataGridViewPreparacion[i, 7].Value = querytablasFb.ExecuteScalar().ToString();
                    i++;
                }
                if (AgregaTipoPrestamo)
                {


                    GridPreparar.Rows.Add();
                    GridPreparar["Omitir", i].Value = true;
                    GridPreparar["Posicion", i].Value = (double)i;
                    GridPreparar["Tipo", i].Value = "Proceso";
                    GridPreparar["Nombre", i].Value = "TipoPrestamo";
                    GridPreparar["Campos", i].Value = "Distinct(TB_CODIGO)";
                    GridPreparar["Condicion", i].Value = "where TB_TIPO not in (select PR_TIPO from PRESTAMO)";
                    GridPreparar["Query", i].Value = "";


                    // DataGridViewPreparacion[i, 7].Value = querytablasFb.ExecuteScalar().ToString();
                    i++;
                }
            }
                int inicio = 0;
            
            for (int indicerow = 0; indicerow < tablaarray().Length; indicerow++)
            {
                 inicio = indicerow + i;

                GridPreparar.Rows.Add();
                if (indicerow < indice)
                {
                    GridPreparar["Omitir", inicio].Value = false;
                }
                else
                {
                    GridPreparar["Omitir", inicio].Value = true;
                }
                GridPreparar["Query", inicio].Value = "";
                GridPreparar["Posicion", inicio].Value = (double)inicio + i;
                GridPreparar["Tipo", inicio].Value = "Tabla";
                GridPreparar["Nombre", inicio].Value = tablaarray()[indicerow];
                if ((tablaarray()[indicerow] == "BITACORA" && bitacoraparcial == true) || ((tablaarray()[indicerow] == "AUSENCIA" || tablaarray()[indicerow] == "CHECADAS") && (asistenciaparcial == true)) || (tablaarray()[indicerow] == "PEN_PORCEN"))
                {

                    if (tablaarray()[indicerow] == "BITACORA")
                    {
                        GridPreparar["Campos", inicio].Value = "*";
                        GridPreparar["Condicion", inicio].Value = "where BI_FECHA between '" + bitacoraini.ToShortDateString() + "' and '" + bitacorafin.ToShortDateString() + "'";
                    }
                    if (tablaarray()[indicerow] == "AUSENCIA")
                    {
                        GridPreparar["Condicion", inicio].Value = "where AU_fecha between '" + asistenciaini.ToShortDateString() + "' and ' " + asistenciafin.ToShortDateString() + "'";
                        GridPreparar["Campos", inicio].Value = "*";
                    }
                    if (tablaarray()[indicerow] == "CHECADAS")
                    {
                        GridPreparar["Condicion", inicio].Value = "where AU_fecha between '" + asistenciaini.ToShortDateString() + "' and ' " + asistenciafin.ToShortDateString() + "'";
                        GridPreparar["Campos", inicio].Value = "*";
                    }
                    if (tablaarray()[indicerow] == "PEN_PORCEN")
                    {
                        GridPreparar["Condicion", inicio].Value = "";
                        GridPreparar["Campos", inicio].Value = "TP_CODIGO,CB_CODIGO,ps_ORDEN,pp_PORCENT";

                    }
                    GridPreparar["Query", inicio].Value = "";

                }
                else
                {
                    GridPreparar["Posicion", inicio].Value = (double)inicio;
                    GridPreparar["Tipo", inicio].Value = "Tabla";
                    GridPreparar["Campos", inicio].Value = "*";
                    GridPreparar["Condicion", inicio].Value = "";
                }


            }
            if (Program.CDEmpresa != "")
            {
                i = inicio + 1;
                GridPreparar["Omitir", i].Value = true;
                GridPreparar["Posicion", i].Value = (double)i+1;
                GridPreparar["Tipo", i].Value = "Proceso";
                GridPreparar["Nombre", i].Value = "Clasificaciones de reportes";
                GridPreparar["Campos", i].Value = "";
                GridPreparar["Condicion", i].Value = "";
                GridPreparar["Query", i].Value = "";
            }
        }

        private void DGVPreparar_Menupopup(object sender, MouseEventArgs e)
        {
           
        }

        private void GridPreparar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void GridPreparar_Click(object sender, EventArgs e)
        {
            
        }
        int rowindex;
        private void GridPreparar_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {

                rightmouseclick = GridPreparar.HitTest(e.X, e.Y).ColumnIndex;
                if (rightmouseclick >= 0 && GridPreparar.HitTest(e.X, e.Y).RowIndex >= 0)
                {
                    rowindex = GridPreparar.HitTest(e.X, e.Y).RowIndex;
                    rowindex = int.Parse(GridPreparar[1, rowindex].Value.ToString());
                    MarcMenuStrip.Show(GridPreparar, new Point(e.X, e.Y));
                }
            }
        }

        private void marcarTodos_Click(object sender, EventArgs e)
        {
            BtnOrdena.PerformClick();
            for (int i = GridPreparar.Rows.Count; i >= rowindex; i--)
            {
                try
                {
                    GridPreparar[0, i].Value = true;
                }
                catch { }
            }
        }

        private void desmarcartodos_Click(object sender, EventArgs e)
        {
            BtnOrdena.PerformClick();
            for (int i = 0; i <= rowindex; i++)
            {
                try
                {
                    GridPreparar[0, i].Value = false;
                    GridPreparar.Rows[i].Selected=false;
                }
                catch { }
            }
            
        }

        private void marcar1_Click(object sender, EventArgs e)
        {
            GridPreparar[0, rowindex].Value = true;
        }

        private void demarcar_Click(object sender, EventArgs e)
        {
            GridPreparar[0, rowindex].Value = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            bool found = false;
            foreach (DataGridViewRow row in GridPreparar.Rows)
            {
                row.Cells[3].Selected = false;
                if (found == false)
                {
                    try
                    {

                        if (row.Cells[3].Value.ToString() == textBox1.Text.ToUpper())
                        {
                            found = true;
                            GridPreparar.FirstDisplayedScrollingRowIndex = row.Index;
                            row.Cells[3].Selected = true;

                        }

                    }
                    catch { }
                }
            }
            if (found == false)
            {
                foreach (DataGridViewRow row in GridPreparar.Rows)
                {
                    row.Cells[3].Selected = false;
                    if (found == false)
                    {
                        try
                        {
                            if (row.Cells[3].Value.ToString().Contains(textBox1.Text.ToUpper()))
                            {
                                found = true;
                                GridPreparar.FirstDisplayedScrollingRowIndex = row.Index;
                                row.Cells[3].Selected = true;
                            }
                        }
                        catch { }
                    }

                }
            }
        
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {


        }

        private void FPrepara_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                showhelp();
            }
            if (e.KeyData == Keys.F12)
            {

                Program.Acercadefrm = new Acercadefrm();
                Program.Acercadefrm.ShowDialog();
            }
        }
    }
}
