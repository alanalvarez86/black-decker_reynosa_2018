using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FirebirdSql.Data.Client;
using FirebirdSql.Data.FirebirdClient;
using System.Data.SqlClient;
using System.Data.Sql;
using System.Security.Permissions;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.ServiceProcess;
using System.Net;
using System.Management;
using System.Security.Principal;
using System.Reflection;

namespace Wizard_Migracion_Tress
{
   
    public partial class FPrincipal : Form
    {
        public Point punto ;
        string conexionfbstr="";
        string conexionSql="";
        string servidorfb="";
        string basedatosfb="";
        string servidorsql="";
        string basedatosql="";
        double t = 0,ultimafila=0;
        public string rutadetalle;
        public string rutabitacora;
        public string rutadetalleComparte;
        public string rutabitacoraComparte;
        bool play = true;
        int indiceinicio;
        Thread t1;
        int indicetabla = 0;
        string[] mensajes = new string[10];
        int K_MINIMO_PATCH_DESTINO = 440;
        
        public FPrincipal()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
       
        }

        private void button3_Click(object sender, EventArgs e)
        {
            MoverHoja(+1);
        }
        bool nextclick;
        public void MoverHoja(int posicion)
        {
            if (WizardPages1.SelectedIndex  < 6)
            {
                if (WizardPages1.SelectedIndex == 0)
                {
                    int aux = btAtrass.Left;
                    btAtrass.Left = BtnCancel.Left;
                    BtnCancel.Left = aux;
                }
                

                if (WizardPages1.SelectedIndex == 1 && posicion > 0)
                {
                    nextclick = true;
                    if (Program.validremoteuser == true && Program.server == TxtServidorFB.Text)
                    {
                        if (probarfb(true) == false)
                        {
                            nextclick = false;
                            return;
                        }
                    }
                    else
                    {
                        System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                        try
                        {
                            if (TxtServidorFB.Text != "" && !(ping.Send(TxtServidorFB.Text).Status == System.Net.NetworkInformation.IPStatus.Success))
                            {
                                MessageBox.Show(this, "No existe comunicacion con la maquina: " + TxtServidorFB.Text, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                nextclick = false;
                                return;
                            }
                            else
                            {
                                if (validatefirebird(true) == false)
                                {
                                    nextclick = false;
                                    return;

                                }
                            }
                        }
                        catch
                        {
                            MessageBox.Show(this, "No existe comunicacion con la maquina: " + TxtServidorFB.Text, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            nextclick = false;
                            return;
                        }
                    }
                    nextclick = false;
                }
                if (WizardPages1.SelectedIndex == 2 && posicion > 0)
                {
                    nextclick = true;
                    if (probarsql(true) == false)
                    {
                        nextclick = false;
                        return;

                    }
                    nextclick = false;
                }
                if (posicion + WizardPages1.SelectedIndex < WizardPages1.TabCount)
                {
                    WizardPages1.SelectTab(WizardPages1.SelectedIndex + posicion);
                    if (WizardPages1.SelectedIndex > 0)
                    {
                        btAtrass.Visible = true;
                    }
                    else
                    {
                       btAtrass.Visible = false;
                        int aux = BtnCancel.Left;
                        BtnCancel.Left = btAtrass.Left;
                        btAtrass.Left = aux;
                        
                    }
                }

                if (WizardPages1.SelectedIndex == 5)
                {


                    ejecutarPreparacion();
                }
                if (WizardPages1.SelectedIndex == 4)
                {
                    if (Program.CDEmpresa != "")
                    {
                        lblCodigo.Text = Program.CDEmpresa;
                    }
                    else
                    {
                        lblCodigo.Text = "N\\A";
                    }

                    if (TxtBaseDatosFB.Text == "")
                    {
                        lblOrigen.Text = "Pendiente";
                    }
                    else
                    {
                        string server;
                        if (TxtServidorFB.Text == "")
                        {
                            server = "Local";
                        }
                        else
                        {
                            server = TxtServidorFB.Text;
                        }
                        lblOrigen.Text = "Servidor : " + server + Environment.NewLine + "Base de Datos : " + TxtBaseDatosFB.Text;
                    }
                    if (TxtBaseDatosSQL.Text == "" || TxtServidorSQL.Text == "")
                    {
                        lblDestino.Text = "Pendiente";
                    }
                    else
                    {
                        lblDestino.Text = "Servidor : " + TxtServidorSQL.Text + Environment.NewLine + "Base de Datos : " + TxtBaseDatosSQL.Text;
                    }
                    if (radBtnBitacoraCompleta.Checked)
                    {
                        lblBitacora.Text = "Completa";
                    }
                    else
                    {

                        lblBitacora.Text = "Del : " + FechaIniBitacora.Value.ToShortDateString() + "  Al: " + FechaFinBitacora.Value.ToShortDateString();
                    }
                    if (RadBtnAsistenciaCompleta.Checked)
                    {
                        lblTarjetas.Text = "Completa";
                    }
                    else
                    {
                        lblTarjetas.Text = "Del : " + FechaIniAsistencia.Value.ToShortDateString() + "  Al: " + FechaFinBitacora.Value.ToShortDateString();
                    }
                    if (CheckKardexAgregar.Checked)
                    {
                        lblKardex.Text = "Pasar Tipos Faltantes al Kardex";
                    }
                    else
                    {
                        lblKardex.Text = "Ignorar Tipos Faltantes del Kardex";
                    }
                    if (TxtRuta.Text != "")
                    {
                        lblRuta.Text = TxtRuta.Text.ToString();
                    }


                    bdCorpText.Text = lblDestino.Text;
                    bdProtext.Text = lblOrigen.Text;

                }
                else
                {
                    btnSiguiente.Text = "Siguiente";
                }

                
                if (WizardPages1.SelectedIndex == 3)
                {

                    TxtRuta.Text = Path.GetDirectoryName(Application.ExecutablePath).ToString() + "\\" + ("Bitacoras_" + TxtBaseDatosSQL.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm")) + "\\Bitacora_" + TxtBaseDatosSQL.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + ".CSV";
                    Program.reporte = Path.GetDirectoryName(TxtRuta.Text).ToString() + "\\Reportes_Transferencia_extras_" + TxtBaseDatosSQL.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + ".CSV";
                    rutadetalle = Path.GetDirectoryName(TxtRuta.Text).ToString() + "\\Bitacora_" + TxtBaseDatosSQL.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + "_Detalle.CSV";
                    SaveFDRuta.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath).ToString();
                    SaveFDRuta.FileName = "Bitacora_" + TxtBaseDatosSQL.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + ".CSV";
                }
            }
            else
            {
                if (posicion > 0)
                {
                   
                    switch (WizardPages1.SelectedIndex)
                    {

                        case 6:
                            nextclick = true;
                            if (validatefirebirdComparte(false) == false)
                            {
                                nextclick = false;
                                return;
                            }
                            nextclick = false;
                            break;
                        case 7:
                     nextclick = true;
                     if (probarsqlComparte(true) == false)
                    {
                        nextclick = false;
                        return;

                    }
                    nextclick = false;
       

                    TxtRutaBitacoraComparte.Text = Path.GetDirectoryName(Application.ExecutablePath).ToString() + "\\" + ("Bitacoras_" + DropDownSqlBaseComparte.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm")) + "\\Bitacora_" + DropDownSqlBaseComparte.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + ".CSV";
                    Program.reporte = Path.GetDirectoryName(TxtRutaBitacoraComparte.Text).ToString() + "\\Reportes_Transferencia_extras_" + DropDownSqlBaseComparte.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + ".CSV";
                    rutadetalle = Path.GetDirectoryName(TxtRutaBitacoraComparte.Text).ToString() + "\\Bitacora_" + DropDownSqlBaseComparte.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + "_Detalle.CSV";
                    SaveFDRuta.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath).ToString();
                    SaveFDRuta.FileName = "Bitacora_" + DropDownSqlBaseComparte.Text + "_" + DateTime.Now.ToString("ddMMyyyy'_'HHmm") + ".CSV";
                            break;
                        case 8:
                            txtInfoBdFirebird.Text = "Servidor : Local" + Environment.NewLine + "Base de Datos : " + txtfbBaseComparte.Text;
                            txtInfoBdMSSQL.Text = "Servidor : " + txtSqlServerComparte.Text + Environment.NewLine + "Base de Datos : " + DropDownSqlBaseComparte.Text;
                            txtInfoBitacoraRuta.Text = TxtRutaBitacoraComparte.Text;
                            break;
                        case 9:
                            ejecutarPreparacionComparte();
                            break;
                    }
                    WizardPages1.SelectedIndex++;
                }
                if (posicion < 0)
                {
                    switch (WizardPages1.SelectedIndex)
                    {

                        case 6:
                            break;
                        case 7:
                            break;
                        case 8:
                            break;
                        case 9:
                            break;
                    }
                    if (WizardPages1.SelectedIndex - 1 < 6)
                    {
                        WizardPages1.SelectedIndex=1;
                        WizardPages1.SuspendLayout();
                        MoverHoja(-1);
                        WizardPages1.ResumeLayout();
                    }
                    else
                    {
                        WizardPages1.SelectedIndex--;
                    }
                }

                if (posicion + WizardPages1.SelectedIndex < WizardPages1.TabCount)
                {
              
              
                }
            }
            lblAccion.Text = mensajes[WizardPages1.SelectedIndex];
            if (WizardPages1.SelectedIndex > 0)
            {
                LabelTipoMigracion.Text = "Datos";
                btnSiguiente.Enabled = true;
            }
            if (WizardPages1.SelectedIndex > 5)
            {
                LabelTipoMigracion.Text = "Comparte";
            }
            if (WizardPages1.SelectedIndex == 0)
            {
                LabelTipoMigracion.Text = "";
                btnSiguiente.Enabled = false;
            }
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            MoverHoja(-1);
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void BtnProbarFirebird_Click(object sender, EventArgs e)
        {
          this.Cursor = Cursors.WaitCursor;
          System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
          try
          {
              if (TxtServidorFB.Text != "" && !(ping.Send(TxtServidorFB.Text).Status == System.Net.NetworkInformation.IPStatus.Success))
              {
                  MessageBox.Show(this, "No existe comunicacion con la maquina: " + TxtServidorFB.Text, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);

              }
              else
              {
                  validatefirebird(true);
              }
             
          }
          catch
          {
              MessageBox.Show(this, "No existe comunicacion con la maquina: " + TxtServidorFB.Text, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
          }
          this.Cursor = Cursors.Default;
          TxtUsuarioFB.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFDFirebird.Filter = "Base de Datos FireBird (*.GDX,*.gdb)|*.gdx;*.gdb|Todos(*.*)|*.*";
            OpenFDFirebird.FilterIndex = 1;
            OpenFDFirebird.ShowDialog();
            TxtBaseDatosFB.Text = OpenFDFirebird.FileName;
            TxtBaseDatosFB.Focus();

        }

        private void btnProbarSql_Click(object sender, EventArgs e)
        {
            Pruevaclick = true;
            probarsql(true);
            TxtServidorSQL.Focus();
        }
        public void alerta(string caption, string titulo, Image icono)
        {
            boxmensaje message = new boxmensaje();
         
            message.label1.Text = caption;
            message.pictureBox1.BackgroundImage = icono;
            message.StartPosition = FormStartPosition.CenterScreen;
            message.ShowDialog();
        
        }
        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (RadBtnBitacoraRango.Checked)
            {
                FechaIniBitacora.Enabled = true;
                FechaFinBitacora.Enabled = true;
            }
            else
            {
                FechaFinBitacora.Enabled = false;
                FechaIniBitacora.Enabled = false;
            
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (RadBtnAsistenciaRango.Checked)
            {
                FechaIniAsistencia.Enabled = true;
                FechaFinAsistencia.Enabled = true;
            }
            else
            {
                FechaFinAsistencia.Enabled = false;
                FechaIniAsistencia.Enabled = false;

            }
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void FPrincipal_Load(object sender, EventArgs e)
        {
            punto = this.Location;
            btAtrass.Image = global::Wizard_Migracion_Tress.Properties.Resources.Back;
            
            TxtServidorSQL.Text = System.Environment.MachineName;
            txtSqlServerComparte.Text = System.Environment.MachineName;
            FechaFinAsistencia.Value = DateTime.Now;
            FechaFinBitacora.Value = DateTime.Now;
            FechaIniAsistencia.Value = DateTime.Now.AddYears(-5);
            FechaIniBitacora.Value = DateTime.Now.AddYears(-5);


            mensajes[0] = "    Asistente de Transferencia de Datos";
            mensajes[1] = "1.   Base de Datos de FireBird ";
            mensajes[2] = "2.   Base de Datos de MSSQL";
            mensajes[3] = "3.   Parámetros de Transferencia";
            mensajes[4] = "4.   Resumen del Proceso";
            mensajes[5] = "";
            mensajes[6] = "1.   Base de Datos de FireBird";
            mensajes[7] = "2.   Base de Datos de MSSQL";
            mensajes[8] = "3.   Bitácora de Transferencia";
            mensajes[9] = "4.   Resumen del Proceso";

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            
            SaveFDRuta.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            SaveFDRuta.Filter = "Archivo de Texto(*.CSV)|*.CSV";
            SaveFDRuta.FileName = Path.GetFileName(TxtRuta.Text);
           DialogResult diag= SaveFDRuta.ShowDialog();
           if (diag != DialogResult.Cancel)
           {
               SaveFDRuta.InitialDirectory = Path.GetDirectoryName(SaveFDRuta.FileName);
               TxtRuta.Text = SaveFDRuta.FileName;
               rutadetalle = Path.GetDirectoryName(SaveFDRuta.FileName) + "\\" + Path.GetFileNameWithoutExtension(SaveFDRuta.FileName) + "_Detalle.CSV";
               Program.reporte = Path.GetDirectoryName(SaveFDRuta.FileName) + "\\Reportes_Transferencia_extras_" + Path.GetFileNameWithoutExtension(TxtRuta.Text) + ".CSV";
           }
           TxtRuta.Focus();
        }

        private void lblOrigen_Click(object sender, EventArgs e)
        {

        }

        private void TxtRuta_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show(this, "¿Seguro que deseas salir?", "Transferencia de Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        [SecurityPermissionAttribute(SecurityAction.Demand, ControlThread = true)]
        private void FinalizaEjecucion()
        {
            Environment.Exit(Environment.ExitCode);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        protected override bool IsInputKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                    return true;
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                    return true;
            }
            return base.IsInputKey(keyData);
        }
        private void FPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            
                if (Program.Migrar != null && (Program.Migrar.finalizoProceso == false && Program.Migrar.InicioProceso == true))
                {
                    DialogResult resultado = MessageBox.Show("Te encuentras ejecutando un proceso, ¿seguro que deseas Salir?", "Transferencia de Datos", MessageBoxButtons.YesNo);
                    if (resultado == DialogResult.Yes)
                    {
                        FinalizaEjecucion();
                    }
                    else
                    {
                        e.Cancel = true;

                    }
                }
            
        }
        public void ejecutar()
        {
            if (probarsql(false) && probarfb(false))
            {
                if (lblBitacora.Text == "Pendiente" || lblDestino.Text == "Pendiente" || lblKardex.Text == "Pendiente" || lblOrigen.Text == "Pendiente" || lblRuta.Text == "Pendiente" || lblTarjetas.Text == "Pendiente")
                {
                    MessageBox.Show("No puedes dejar pendientes antes de iniciar la transferencia");
                    MoverHoja(-1);
                }
                else
                {
                    if (MessageBox.Show(this, "¿Estas listo para iniciar la transferencia de datos?", "Transferencia de Datos", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {

                        
                    }
                    else
                    {
                        MoverHoja(-1);
                    }
                }
            }
            else
            {
                MoverHoja(-1);
            }
        }
        public void ejecutarPreparacion()
        {
           
            if (probarsql(false) && probarfb(false))
            
            {
                if (lblBitacora.Text == "Pendiente" || lblDestino.Text == "Pendiente" || lblKardex.Text == "Pendiente" || lblOrigen.Text == "Pendiente" || lblRuta.Text == "Pendiente" || lblTarjetas.Text == "Pendiente")
                {
                    MessageBox.Show("No puedes dejar pendientes antes de iniciar la transferencia");
                    MoverHoja(-1);
                }
                else
                {


                    Program.MigrarComparte = false;
                    Program.Prepara = new FPrepara();
                    Program.Prepara.AgregaTipoKardex = CheckKardexAgregar.Checked;
                    Program.Prepara.AgregaTipoPrestamo = CheckPrestamosAgregar.Checked;
                    Program.Prepara.conexionstringfb = conexionfbstr;
                    Program.Prepara.conexionstringsql = conexionSql;
                    Program.Prepara.rutabitacora = TxtRuta.Text;
                    Program.Prepara.rutadetalle = rutadetalle;
                    Program.Prepara.indice = indiceinicio;
                    Program.Prepara.parcial = false;
                    Program.Prepara.bitacoraini = FechaIniBitacora.Value;
                    Program.Prepara.bitacorafin = FechaFinBitacora.Value;
                    Program.Prepara.bitacoraparcial = RadBtnBitacoraRango.Checked;
                    Program.Prepara.asistenciaini = FechaIniAsistencia.Value;
                    Program.Prepara.asistenciafin = FechaFinAsistencia.Value;
                    Program.Prepara.asistenciaparcial = RadBtnAsistenciaRango.Checked;
                   
                    Program.Prepara.loadgrid();
                    Program.Prepara.punto = this.Location;
                    Program.Prepara.Show();
                    rutabitacora = TxtRuta.Text;
                 
                    this.Hide();

                }

            }
        }
        public void ejecutarPreparacionComparte()
        {
            if (probarsqlComparte(false) && probarfbComparte(false))
            {
               


                    Program.MigrarComparte = true;
                    Program.Prepara = new FPrepara();
                    Program.Prepara.AgregaTipoKardex = false;
                    Program.Prepara.AgregaTipoPrestamo = false;
                    Program.Prepara.conexionstringfb = conexionfbstr;
                    Program.Prepara.conexionstringsql = conexionSql;
                    Program.Prepara.rutabitacora = TxtRutaBitacoraComparte.Text;
                    Program.Prepara.rutadetalle = rutadetalle;
                    //Program.Prepara.indice = indiceinicio;
                    Program.Prepara.parcial = false;
                    Program.Prepara.bitacoraini = FechaIniBitacora.Value;
                    Program.Prepara.bitacorafin = FechaFinBitacora.Value;
                    Program.Prepara.bitacoraparcial = false;
                   // Program.Prepara.asistenciaini = FechaIniAsistencia.Value;
                    //Program.Prepara.asistenciafin = FechaFinAsistencia.Value;
                    Program.Prepara.asistenciaparcial = false;
                    Program.Prepara.loadgrid();
                    Program.Prepara.punto = this.Location;
                    Program.Prepara.Show();
                    rutabitacora = TxtRuta.Text;
                    this.Hide();

                

            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkWSA.Checked)
            {
                TxtUsuarioSQl.Enabled = false;
                TxtContrasenaSQL.Enabled = false;
            }
            else
            {
                TxtUsuarioSQl.Enabled = true;
                TxtContrasenaSQL.Enabled = true;
            }
        }
        bool Pruevaclick;
        private void timer1_Tick(object sender, EventArgs e)
        {

            /*   //TablaLbl.Text = Program.Migrar.NombreTabla;
              // TablaLbl.Left = ((progressBar1.Left + progressBar1.Right) / 2) - (TablaLbl.Width / 2);
            
             //  TransLbl.Left = ((progressBar1.Left + progressBar1.Right) / 2) - (TransLbl.Width / 2);
            //   t++;
            //   progressBar2.Maximum = Program.Migrar.tablaarray().Length;
            //   progressBar2.Minimum = Program.Migrar.tablapos;
           //    progressBar1.Maximum = Program.Migrar.FilasMigrar;
            //   progressBar1.Minimum = 0;
           //    progressBar2.Value = Program.Migrar.tablapos + Program.Migrar.tablasrecorridas;
          //     progressBar1.Value = (int)Program.Migrar.FilasTabla;
               double velocidad;
               //obtiene los valroes de la clase migrar 
               //richTextBox2.Text = migrar.error;

          //     StatusTextbox.Text = " Tabla actual: " + Program.Migrar.NombreTabla + Environment.NewLine + " Filas migradas de esta tabla: " + (Program.Migrar.FilasTabla - Program.Migrar.errores).ToString() + "(" + (Math.Round((((Program.Migrar.FilasTabla - Program.Migrar.errores) / Program.Migrar.FilasMigrar) * 100),0)).ToString() + "%) " + Environment.NewLine + " Filas Ignoradas: " + Program.Migrar.errores.ToString() + Environment.NewLine + Environment.NewLine + " Filas Migradas Totales: " + (Program.Migrar.FilasTotales + Program.Migrar.FilasTabla - (Program.Migrar.errorestotales+Program.Migrar.errores)).ToString() + Environment.NewLine + "Filas Ignoradas Totales : " + Program.Migrar.errorestotales;
               ultimafila = Program.Migrar.FilasTabla;
            

               velocidad = (int)(((Program.Migrar.FilasTotales + Program.Migrar.FilasTabla-(Program.Migrar.errores+Program.Migrar.errorestotales)) * 10) / t);
               if (t % 10 == 0)
               {

               }
         //      StatusTextbox.AppendText(Environment.NewLine+ velocidad.ToString()+" registros/segundo");
               if (Program.Migrar.finalizoProceso == true)
               {

                   timer1.Stop();
                   button1.Enabled = true;
          //         StatusTextbox.AppendText(Environment.NewLine + "la conexion se callo :" + Program.Migrar.conexioncaida.ToString() + " veces");
          //         StatusTextbox.AppendText(" Tiempo total = " + ((int)((t/10)/60)).ToString() + ":"+((int)((t/10)%60)).ToString()+" minutos");
                   timer1.Stop();
                   btnFin.Enabled = true;
         //          TransLbl.Text = "F I N A L I Z A D O";
               }*/
        }
        public string NombreEmpresa = "";
        MessageBoxIcon icon = new MessageBoxIcon();
        public string conexionComparte;
        public bool probarsql(bool mensajes)
        {

            NombreEmpresa = "";
            Program.CDEmpresa = "";
            Program.ConexionMaster = "";
            Program.NombreComparte = "";
            SqlConnection conexion = new SqlConnection();
            FbConnection conexionfb = new FbConnection();
            if (CmBEmpresa.SelectedIndex != -1)
            {
                Program.Compañia = CB_CODIGOS[CmBEmpresa.SelectedIndex];
            }


            if (checkWSA.Checked)
            {
                conexionSql = "Database=" + TxtBaseDatosSQL.Text + " ; server=" + TxtServidorSQL.Text + " ; Trusted_Connection=Yes;";
                conexionComparte = " ;Database=" + CmBComparte.Text + " ;Server=" + TxtServidorSQL.Text + "; Trusted_Connection=Yes; ";
                Program.ConexionMaster = "Database=Master; server=" + TxtServidorSQL.Text + "; Trusted_connection=Yes;";
            }
            else
            {
                conexionSql = "User id=" + TxtUsuarioSQl.Text + ";Password=" + TxtContrasenaSQL.Text + " ;Database=" + TxtBaseDatosSQL.Text + " ;Server=" + TxtServidorSQL.Text + " ; ";
                conexionComparte = "User id=" + TxtUsuarioSQl.Text + ";Password=" + TxtContrasenaSQL.Text + " ;Database=" + CmBComparte.Text + " ;Server=" + TxtServidorSQL.Text + " ; ";
                Program.ConexionMaster = "User id=" + TxtUsuarioSQl.Text + ";Password=" + TxtContrasenaSQL.Text + " ;Database= Master ;Server=" + TxtServidorSQL.Text + " ; ";
                Program.NombreComparte = CmBComparte.Text;
                Program.nombrebaseDatos = TxtBaseDatosSQL.Text;
              
            }
            try
            {

                


                conexion = new SqlConnection(conexionSql);
                conexion.Open();
                SqlConnection conexioncomp = new SqlConnection(conexionComparte);






                if (mensajes == true && nextclick == false)
                {
                    if (Pruevaclick == true)
                    {
                        if (Program.Compañia == "")
                            throw new System.Exception("Debes seleccionar un código de base de datos para extraer las clasificaciones de reportes");
                        try
                        {
                            if (CmBComparte.Text != "")
                            {
                                if (conexioncomp.State == ConnectionState.Open)
                                {
                                    conexioncomp.Close();
                                }
                                conexioncomp.Open();
                                SqlCommand querysqlcomparte = new SqlCommand("select CM_Codigo,CM_NOMBRE from company");
                                querysqlcomparte.Connection = conexioncomp;
                                SqlDataReader sqlreader = querysqlcomparte.ExecuteReader();
                                while (sqlreader.Read())
                                {



                                }
                            }
                        }
                        catch (Exception exc)
                        {
                            if (!exc.Message.Contains("Empresa Incorrecta"))
                            {
                                Pruevaclick = false;
                                throw new System.ArgumentException("Base de datos seleccionada de COMPARTE no es compatible" + Environment.NewLine + "Elige una base de datos que sea COMPARTE", "");
                            }
                            else
                            {
                                throw exc;
                            }
                        }
                        
                        Pruevaclick = false;
                    }










                    SqlCommand query = new SqlCommand("select TOP(1) COUNT(*) from COLABORA", conexion);
                    int intento = int.Parse(query.ExecuteScalar().ToString());
                    conexionfb = new FbConnection(conexionfbstr);
                    conexionfb.Open();
                    FbCommand queryfb = new FbCommand("select gl_formula from global where gl_codigo = 133", conexionfb);
                    SqlCommand querysql = new SqlCommand("select gl_formula from global where gl_codigo = 133", conexion);
                    try
                    {
                        string codigosql = querysql.ExecuteScalar().ToString();
                        int iCodigoSQL = int.Parse(codigosql);
                        string codigofb = queryfb.ExecuteScalar().ToString();
                        Program.VersionCompartesql = codigosql;
                        conexion.Close();
                        if (codigofb != codigosql && iCodigoSQL < K_MINIMO_PATCH_DESTINO)
                        {
                            MessageBox.Show(this, "Estructuras no Compatibles" + Environment.NewLine + "Estructura FB  = " + codigofb + Environment.NewLine + "Estructura SQL = " + codigosql + Environment.NewLine + "Por favor seleccionar otra base de datos Destino o cambiar de base de datos Fuente", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;

                        }
                        else
                        {
                            alerta("Conexión Válida", "Transferencia de Datos", global::Wizard_Migracion_Tress.Properties.Resources.Agt_Action_Success);
                        }
                    }
                    catch
                    {
                        throw new System.ArgumentException("Versión de base de datos no localizada"+Environment.NewLine+" Es necesario reconstruir la base de datos", "");
                    }
                   
                }
                else
                {
                    if (nextclick == true)
                    {
                       
                        try
                        {
                           
                            if (CmBComparte.Text != "")
                            {
                                if (conexioncomp.State == ConnectionState.Open)
                                {
                                    conexioncomp.Close();
                                }
                                conexioncomp.Open();
                                SqlCommand querysqlcomparte = new SqlCommand("select CM_Codigo,CM_NOMBRE from company");
                                querysqlcomparte.Connection = conexioncomp;
                                SqlDataReader sqlreader = querysqlcomparte.ExecuteReader();
                                if (Program.Compañia == "")
                                    throw new System.Exception("Debes seleccionar un código de base de datos para extraer las clasificaciones de reportes");
                                while (sqlreader.Read())
                                {
                                    
                                
                                   
                                }
                            }
                        }
                        catch(Exception exc)
                        {
                            if (exc.Message.Contains("clasificaciones"))
                            {
                                throw exc;
                            }
                            else
                            {
                                if (!exc.Message.Contains("Empresa Incorrecta"))
                                {
                                    Pruevaclick = false;
                                    throw new System.ArgumentException("Base de datos seleccionada de COMPARTE no es compatible" + Environment.NewLine + "Elige una base de datos que sea COMPARTE", "");
                                }
                                else
                                {
                                    throw exc;
                                }
                            }
                        }
                       


                        SqlCommand query = new SqlCommand("select top(1) count(*) from colabora", conexion);
                        int intento = int.Parse(query.ExecuteScalar().ToString());

                        conexionfb = new FbConnection(conexionfbstr);
                        conexionfb.Open();

                        FbCommand queryfb = new FbCommand("select gl_formula from global where gl_codigo = 133", conexionfb);
                        SqlCommand querysql = new SqlCommand("select gl_formula from global where gl_codigo = 133", conexion);

                        try
                        {
                            string codigosql = querysql.ExecuteScalar().ToString();
                            int iCodigoSQL = int.Parse(codigosql);
                            string codigofb = queryfb.ExecuteScalar().ToString();
                            Program.VersionCompartesql = codigosql;
                  
                            conexion.Close();
                            if (codigofb != codigosql && iCodigoSQL < K_MINIMO_PATCH_DESTINO)
                            {
                                MessageBox.Show(this, "Estructuras no Compatibles" + Environment.NewLine + "Estructura FB  = " + codigofb + Environment.NewLine + "Estructura SQL = " + codigosql + Environment.NewLine + "Por favor seleccionar otra base de datos Destino o cambiar de base de datos Fuente", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;

                            }
                        }
                        catch
                        {
                            throw new System.ArgumentException("Versión de base de datos no localizada"+Environment.NewLine+" Es necesario reconstruir la base de datos", "");
                        }
                    }
                   
                }
                
              
                 
                    basedatosql = TxtBaseDatosSQL.Text;
                    servidorsql = TxtServidorSQL.Text;
                    Program.CDEmpresa = CmBComparte.Text;
                    
                    return true;
                   
                
            }
            catch (Exception exc)
            {
                string msg=exc.Message;
                if (exc.ToString().Contains("Versión"))
                {
                    msg = exc.Message.ToString();
                }
                if (exc.ToString().Contains("Login"))
                {
                    msg = "Usuario o Contraseña Inválidos";
                }
                if (exc.ToString().Contains("The server was not found"))
                {
                    msg="No se encontro el servidor";
                }
                
                if(exc.ToString().Contains("Invalid object name"))
                {
                    msg="Base de datos Inválida";
                }
                    if (mensajes == true || WizardPages1.SelectedIndex > 4)
                    {
                        MessageBox.Show(this, "¡ Conexión  MSSQL Falló ! ;" + msg, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(this, msg, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     
                    }
                
                nextclick = false;
                conexionSql = "";
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                if (conexionfb.State == ConnectionState.Open)
                {
                    conexionfb.Close();
                }
                return false;
               
               
            }
        }

        public bool probarfb(bool mensajes)
        {
            string localhost = "";
            string uncPath = "";
            if (localcheck.Checked)
            {
                localhost = "Localhost:";
            }

            try
            {
                if (Program.usuario == null)
                {

                }
                using (Impersonator imp = new Impersonator(Program.usuario, Program.domain, Program.password))
                {


                    uncPath = TxtBaseDatosFB.Text;
                    if (uncPath.Contains("\\\\"))
                    {
                        try
                        {

                            uncPath = TxtBaseDatosFB.Text.Replace(@"\\", "");
                            string[] uncParts = uncPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                            // Get a connection to the server as found in the UNC path
                            ManagementScope scope = new ManagementScope(@"\\" + uncParts[0] + @"\root\cimv2");
                            // Query the server for the share name
                            SelectQuery query = new SelectQuery("Select * From Win32_Share Where Name = '" + uncParts[1] + "'");
                            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);

                            // Get the path
                            string path = string.Empty;
                            foreach (ManagementObject obj in searcher.Get())
                            {
                                path = obj["path"].ToString();
                            }

                            // Append any additional folders to the local path name
                            if (uncParts.Length > 2)
                            {
                                for (int i = 2; i < uncParts.Length; i++)
                                    path = path.EndsWith(@"\") ? path + uncParts[i] : path + @"\" + uncParts[i];
                            }

                            uncPath = path;
                        }

                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("denied"))
                            {
                                MessageBox.Show(this, "El Servidor: " + Program.server + " a rechazado la conexion, Se Requiere de Permisos de Acceso remoto", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                    }
                }
                conexionfbstr = "User ID=" + TxtUsuarioFB.Text + " ;Password=" + TxtContrasenaFB.Text + " ;Database=" + localhost + uncPath + " ;DataSource=" + Program.server + " ;Charset=NONE";
                FbConnection conexion = new FbConnection(conexionfbstr);

                conexion.Open();
                if (mensajes == true && nextclick == false)
                {
                    FbCommand query = new FbCommand("select first 1 COUNT(*) from COLABORA", conexion);
                    int intento = int.Parse(query.ExecuteScalar().ToString());
                    alerta("Conexión Válida", "Transferencia de Datos", global::Wizard_Migracion_Tress.Properties.Resources.Agt_Action_Success);
                }
                else
                {
                    if (nextclick == true)
                    {
                        FbCommand query = new FbCommand("select first 1 COUNT(*) from COLABORA", conexion);
                        int intento = int.Parse(query.ExecuteScalar().ToString());
                    }
                }

                conexion.Close();
                basedatosfb = TxtBaseDatosFB.Text;
                servidorfb = TxtServidorFB.Text;

                return true;
            }
            catch (Exception exc)
            {
                string msg = "Causa Desconocida";
                if (mensajes == true || WizardPages1.SelectedIndex > 4)
                {
                    if (exc.ToString().Contains("open file"))
                    {
                        msg = "No se pudo abrir el archivo seleccionado :" + uncPath;
                    }
                    if (exc.ToString().Contains("Table unknown"))
                    {
                        msg = "Base de datos inválida";
                    }
                    if (exc.ToString().Contains("password"))
                    {
                        msg = "Usuario o Contraseña Inválidos";
                    }
                    if (exc.ToString().Contains("host"))
                    {
                        msg = "Servidor Incorrecto";
                    }
                    if (exc.ToString().Contains("connection string argument"))
                    {
                        msg = "Debes de seleccionar una base de datos";
                    }
                    MessageBox.Show("¡ Conexión  FireBird Falló ! ; " + msg, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                conexionfbstr = "";
                return false;

            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            /*
            int click = 1;
            if (play == true)
            {
             //   button3.BackgroundImage = global::Wizard_Migracion_Tress.Properties.Resources.play;
                play = false;
                Program.Migrar.pausa = true;
                click = 0;
         //       TransLbl.Text = "P A U S A D O";
                
            }
            if (play == false && click==1)
            {
         //       button3.BackgroundImage = global::Wizard_Migracion_Tress.Properties.Resources.media_pause;
                play = true;
                Program.Migrar.pausa = false;
          //      TransLbl.Text = "M I G R A N D O";
            }
        }*/
        }
        private void TxtServidorSQL_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxtBaseDatosSQL_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            TxtBaseDatosSQL.Text = "";
            probarsql(false);
            SqlConnection conexion = new SqlConnection(conexionSql);
            try{
            TxtBaseDatosSQL.Items.Clear();
            SqlCommand obtenertablas = new SqlCommand("select name from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb');", conexion);
            obtenertablas.Connection.Open();
            SqlDataReader reader = obtenertablas.ExecuteReader();
                while(reader.Read())
                {
                    TxtBaseDatosSQL.Items.Add(reader["name"].ToString());
                }
            obtenertablas.Connection.Close();
            }
            catch
            {
            
            }
            this.Cursor = Cursors.Default;
        }

        private void btnFin_Click(object sender, EventArgs e)
        {
            /*
            if (Program.Resultados == null || Program.Resultados.IsDisposed)
            {
                Program.Resultados = new FResultados();
                Program.Resultados.conexionquery = conexionSql;
                Program.Resultados.bitacora = lblRuta.Text;
                Program.Resultados.detalle = rutadetalle;
                Program.Resultados.Show();
                Program.Resultados.BringToFront();

            }
            else
            {
                Program.Resultados.Show();
                Program.Resultados.BringToFront();
            
            
            }
             */
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = TxtRuta.Text ;
            p.StartInfo.Arguments = "ProcessStart.cs";
            p.Start();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Process p = new Process();
            p.StartInfo.FileName = rutadetalle;
            p.StartInfo.Arguments = "ProcessStart.cs";
            p.Start();
        }

        private void CheckKardexAgregar_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void OpenFDFirebird_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void TxtTablaInicial_MouseEnter(object sender, EventArgs e)
        {
        
        }

        private void radioButton2_CheckedChanged_1(object sender, EventArgs e)
        {
            if (RadBtnMigracionParcial.Checked == true)
            {
                TxtTablaInicial.Enabled = true;
                TxtTablaInicial.Focus();
            }
            else
            {
                TxtTablaInicial.Enabled = false;
            }
        }

        private void TxtTablaInicial_Leave(object sender, EventArgs e)
        {
            bool inicia = false;
            for (int i = 0; i < Program.Prepara.tablaarray().Length; i++)
            { 
                if(TxtTablaInicial.Text==Program.Prepara.tablaarray()[i].ToString())
                {
                    indiceinicio = i;
                    inicia = true;
                }
            }
            if (inicia == false)
            {
                MessageBox.Show(this, TxtTablaInicial.Text + " No es Una tabla Válida", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                TxtTablaInicial.Text = "";
                RadBtnMigracionCompleta.Checked = true;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void wizardPages1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabPage7_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox2_Enter(object sender, EventArgs e)
        {
            
        }

        private void tabPage6_Click(object sender, EventArgs e)
        {

        }

        private void TxtBaseDatosSQL_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public void nuevo()
        {
            FechaIniBitacora.Value = DateTime.Now;
            FechaFinBitacora.Value = DateTime.Now.AddYears(-5);
            FechaIniAsistencia.Value = DateTime.Now;
            FechaFinAsistencia.Value = DateTime.Now.AddYears(-5);
            radBtnBitacoraCompleta.Checked = true;
            RadBtnAsistenciaCompleta.Checked = true;
            RadBtnMigracionCompleta.Checked = true;
            CheckKardexAgregar.Checked = true;
           
        }
        
        private void FPrincipal_Shown(object sender, EventArgs e)
        {
            if (WizardPages1.SelectedIndex == 0)
            {
                btnSiguiente.Enabled = false;
            }
            else
                btnSiguiente.Enabled = true;
                for (int i = 0; i < WizardPages1.TabPages.Count; i++)
            {
                foreach (Control con in WizardPages1.TabPages[i].Controls)
                {
                    if (con.GetType() == typeof(TextBox))
                    {

                        con.KeyPress += new KeyPressEventHandler(nobeep);
                    }
                }

            }
            if (WizardPages1.SelectedIndex == 0)
            { 
             int aux= BtnCancel.Left;
             BtnCancel.Left = btAtrass.Left;
             btAtrass.Left=aux;
            }
            
           // moverbotones();
        }

        private void TxtBaseDatosSQL_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void TxtContrasenaFB_TextChanged(object sender, EventArgs e)
        {

        }
        private void validateuser()
        {
            if (Program.server == TxtServidorFB.Text && Program.validremoteuser == true)
            { }
            else
            {
                Program.server = TxtServidorFB.Text;
                FormImpersonateUser impersonate = new FormImpersonateUser();
                impersonate.comboBox1.Items.Add(Environment.UserDomainName.ToString());
                if (Environment.UserDomainName.ToString() != Environment.MachineName.ToString())
                {
                    impersonate.comboBox1.Items.Add(Environment.MachineName.ToString());
                }
                    impersonate.ShowDialog();
            }
        }
        private bool validatefirebird(bool condicion)
        {
           
            string ip = "";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress IPA in host.AddressList)
            {

                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    ip += IPA.ToString();
                }
            }
            if (System.Environment.MachineName.ToString() == TxtServidorFB.Text.ToLower() || ip.Contains(TxtServidorFB.Text))
            {
                Program.usuario = null;
                Program.password = null;
                ServiceController ctl = ServiceController.GetServices().Where(s => s.DisplayName.Contains("Firebird Guardian")).FirstOrDefault();
                Program.server = Environment.MachineName.ToString();
                if (ctl == null)
                {
                    MessageBox.Show(this, "Servidor de Firebird no Encontrado", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    if (ctl.Status != ServiceControllerStatus.Running)
                    {
                        if (MessageBox.Show(this, "Servicio de Firebird Sin Iniciar, ¿Deseas Iniciarlo?", "Transferencia de Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            ctl.Start();
                            ctl.WaitForStatus(ServiceControllerStatus.Running);
                            return probarfb(condicion);
                          
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {


                        return probarfb(condicion);

                    }
                }
            }
            else
            {
                validateuser();
                if (Program.validremoteuser == true)
                {

                  return  probarfb(condicion);
                }
                else
                {
                    
                    return false;
                }
               
            }
            
        
        }
        private bool validatefirebirdComparte(bool condicion)
        {
            string ip = "";
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress IPA in host.AddressList)
            {

                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    ip += IPA.ToString();
                }
            }
            if (System.Environment.MachineName.ToString() == TxtServidorFB.Text.ToLower() || ip.Contains(TxtServidorFB.Text))
            {
                Program.usuario = null;
                Program.password = null;
                ServiceController ctl = ServiceController.GetServices().Where(s => s.DisplayName.Contains("Firebird Guardian")).FirstOrDefault();
                Program.server = Environment.MachineName.ToString();
                if (ctl == null)
                {
                    MessageBox.Show(this, "Servidor de Firebird no Encontrado", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                {
                    if (ctl.Status != ServiceControllerStatus.Running)
                    {
                        if (MessageBox.Show(this, "Servicio de Firebird Sin Iniciar, ¿Deseas Iniciarlo?", "Transferencia de Datos", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            ctl.Start();
                            ctl.WaitForStatus(ServiceControllerStatus.Running);
                            return probarfb(condicion);
                            
                        }
                        else
                        {
                            return false;
                        }

                    }
                    else
                    {


                        return probarfbComparte(condicion);

                    }
                }
            }
            else
            {
                validateuser();
                if (Program.validremoteuser == true)
                {

                    return probarfb(condicion);
                }
                else
                {

                    return false;
                }
               
            }


        }
        private void button5_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Help.ShowHelp(this, @"Transferencia.chm", HelpNavigator.Topic, (WizardPages1.SelectedIndex + 1).ToString() + ".htm");
        }

        private void FPrincipal_KeyPress(object sender, KeyPressEventArgs e)
        {
           
        }
        public void nobeep(object sender,KeyPressEventArgs e)
        { 
        if(e.KeyChar == (char)Keys.Enter)
            {
            e.Handled=true;
            }
        }
        private void FPrincipal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F1)
            {
                button5.PerformClick();
            }
            if (e.KeyData == Keys.Enter)
            {
                e.Handled = true;
                btnSiguiente.PerformClick();
              
            }
            if (e.KeyData == Keys.F12)
            {
                Program.Acercadefrm = new Acercadefrm();
                Program.Acercadefrm.ShowDialog();
            }
            
        }

        private void label19_Click(object sender, EventArgs e)
        {

        }

        private void radBtnBitacoraCompleta_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void FechaIniBitacora_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void FechaFinBitacora_ValueChanged(object sender, EventArgs e)
        {

        }

        private void RadBtnAsistenciaCompleta_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void FechaIniAsistencia_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void FechaFinAsistencia_ValueChanged(object sender, EventArgs e)
        {

        }

        private void TxtRuta_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void CmBComparte_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            CmBEmpresa.Text = "";
            CmBComparte.Text = "";
            probarsql(false);
            SqlConnection conexion = new SqlConnection(conexionSql);
            try
            {
                CmBComparte.Items.Clear();
                CmBEmpresa.Items.Clear();
                CB_CODIGOS.Clear();
                SqlCommand obtenertablas = new SqlCommand("select name from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb');", conexion);
                obtenertablas.Connection.Open();
                SqlDataReader reader = obtenertablas.ExecuteReader();
                while (reader.Read())
                {
                    CmBComparte.Items.Add(reader["name"].ToString());
                }
                obtenertablas.Connection.Close();
            }
            catch
            {

            }
            this.Cursor = Cursors.Default;
        }
        List<string> CB_CODIGOS = new List<string>();
        private void CmBEmpresa_DropDown(object sender, EventArgs e)
        {
            CmBEmpresa.Items.Clear();
            CB_CODIGOS.Clear();
            try
            {
                if (checkWSA.Checked)
                {
                 
                    conexionComparte = " ;Database=" + CmBComparte.Text + " ;Server=" + TxtServidorSQL.Text + "; Trusted_Connection=Yes; ";
                }
                else
                {
                    
                    conexionComparte = "User id=" + TxtUsuarioSQl.Text + ";Password=" + TxtContrasenaSQL.Text + " ;Database=" + CmBComparte.Text + " ;Server=" + TxtServidorSQL.Text + " ; ";

                }
                SqlConnection conexion = new SqlConnection(conexionComparte);
                conexion.Open();
                SqlCommand query = new SqlCommand("Select DB_CODIGO,DB_DESCRIP from DB_INFO");
                query.Connection = conexion;
                SqlDataReader lectorCompany = query.ExecuteReader();
                while (lectorCompany.Read())
                {
                    CmBEmpresa.Items.Add(lectorCompany["DB_CODIGO"].ToString().Trim()+" : "+lectorCompany["DB_DESCRIP"].ToString().Trim());
                    CB_CODIGOS.Add(lectorCompany["DB_CODIGO"].ToString().Trim());
                }
            }
            catch
            {
                
                MessageBox.Show("No se pueden extraer los códigos de las bases de datos, Verifica tu conexión");
            }
        }

        private void checkBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (checkBox1.Checked == false)
            {
                CmBComparte.Items.Clear();
                CmBEmpresa.Items.Clear();
                CmBComparte.Text = "";
                CmBEmpresa.Text = "";
                CB_CODIGOS.Clear();
            }
       
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {
           
           
        }

        private void CmBEmpresa_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void BtnMigrarComparte_Click(object sender, EventArgs e)
        {
           
            WizardPages1.SuspendLayout();
            MoverHoja(+6);
            WizardPages1.ResumeLayout();

        }
        public void moverbotones()
        {
            if (WizardPages1.SelectedIndex == 0)
            {
                btAtrass.Visible = false;
                btnSiguiente.Visible = false;
                int temp = BtnCancel.Left;
                BtnCancel.Left = btnSiguiente.Left;
                btnSiguiente.Left = btAtrass.Left;
                btAtrass.Left = temp;
            }
            else
            {

                int temp = btAtrass.Left;
                btAtrass.Left = btnSiguiente.Left;
                btnSiguiente.Left = BtnCancel.Left;
                BtnCancel.Left = temp;
                btAtrass.Visible = true;
                btnSiguiente.Visible = true;
            }
        
        
        }

        private void BtnMigrarDatos_Click(object sender, EventArgs e)
        {
            MoverHoja(1);
        }

        private void TxtUsuarioFB_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void TxtServidorFB_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void TxtBaseDatosFB_TextChanged(object sender, EventArgs e)
        {

        }

        private void localcheck_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label27_Click(object sender, EventArgs e)
        {

        }

        private void label38_Click(object sender, EventArgs e)
        {

        }

        private void label37_Click(object sender, EventArgs e)
        {

        }

        private void label36_Click(object sender, EventArgs e)
        {

        }

        private void label35_Click(object sender, EventArgs e)
        {

        }

        private void ButtonBitacoraComparte_Click(object sender, EventArgs e)
        {
            SaveFDRuta.InitialDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            SaveFDRuta.Filter = "Archivo de Texto(*.CSV)|*.CSV";
            SaveFDRuta.FileName = Path.GetFileName(TxtRutaBitacoraComparte.Text);
           DialogResult diag= SaveFDRuta.ShowDialog();
           if (diag != DialogResult.Cancel)
           {
               SaveFDRuta.InitialDirectory = Path.GetDirectoryName(SaveFDRuta.FileName);
               TxtRutaBitacoraComparte.Text = SaveFDRuta.FileName;
               rutadetalle = Path.GetDirectoryName(SaveFDRuta.FileName) + "\\" + Path.GetFileNameWithoutExtension(SaveFDRuta.FileName) + "_Detalle.CSV";
               Program.reporte = Path.GetDirectoryName(SaveFDRuta.FileName) + "\\Reportes_Transferencia_extras_" + Path.GetFileNameWithoutExtension(TxtRuta.Text) + ".CSV";
           }
           TxtRuta.Focus();
        }

        private void button3_Click_2(object sender, EventArgs e)
        {
            OpenFDFirebird.Filter = "Base de Datos FireBird (*.GDX,*.gdb)|*.gdx;*.gdb|Todos(*.*)|*.*";
            OpenFDFirebird.FilterIndex = 1;
            OpenFDFirebird.ShowDialog();
            txtfbBaseComparte.Text = OpenFDFirebird.FileName;
            txtfbBaseComparte.Focus();
        }

        private void btnConeccionComparte_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
            try
            {
                if (TxtServidorFB.Text != "" && !(ping.Send(TxtServidorFB.Text).Status == System.Net.NetworkInformation.IPStatus.Success))
                {
                    MessageBox.Show(this, "No existe comunicacion con la maquina: " + txtfbServerComparte.Text, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {
                    validatefirebirdComparte(true);
                }

            }
            catch
            {
                MessageBox.Show(this, "No existe comunicacion con la maquina: " + txtfbServerComparte.Text, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            this.Cursor = Cursors.Default;
            TxtUsuarioFB.Focus();
        }
        public bool probarfbComparte(bool mensajes)
        {
            string localhost = "";
            string uncPath = "";
            if (localcheck.Checked)
            {
                localhost = "Localhost:";
            }

            try
            {
                if (Program.usuario == null)
                {

                }
                using (Impersonator imp = new Impersonator(Program.usuario, Program.domain, Program.password))
                {


                    uncPath = txtfbBaseComparte.Text;
                    if (uncPath.Contains("\\\\"))
                    {
                        try
                        {

                            uncPath = txtfbBaseComparte.Text.Replace(@"\\", "");
                            string[] uncParts = uncPath.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                            // Get a connection to the server as found in the UNC path
                            ManagementScope scope = new ManagementScope(@"\\" + uncParts[0] + @"\root\cimv2");
                            // Query the server for the share name
                            SelectQuery query = new SelectQuery("Select * From Win32_Share Where Name = '" + uncParts[1] + "'");
                            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);

                            // Get the path
                            string path = string.Empty;
                            foreach (ManagementObject obj in searcher.Get())
                            {
                                path = obj["path"].ToString();
                            }

                            // Append any additional folders to the local path name
                            if (uncParts.Length > 2)
                            {
                                for (int i = 2; i < uncParts.Length; i++)
                                    path = path.EndsWith(@"\") ? path + uncParts[i] : path + @"\" + uncParts[i];
                            }

                            uncPath = path;
                        }

                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("denied"))
                            {
                                MessageBox.Show(this, "El Servidor: " + Program.server + " a rechazado la conexion, Se Requiere de Permisos de Acceso remoto", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                    }
                }
                conexionfbstr = "User ID=" + txtfbUserComparte.Text + " ;Password=" + TxtFbPassComparte.Text + " ;Database=" + localhost + uncPath + " ;DataSource=" + Program.server + " ;Charset=NONE";
                FbConnection conexion = new FbConnection(conexionfbstr);

                conexion.Open();
                if (mensajes == true && nextclick == false)
                {
                    FbCommand query = new FbCommand("select first 1 COUNT(*) from Usuario", conexion);
                    int intento = int.Parse(query.ExecuteScalar().ToString());
                    alerta("Conexión Válida", "Transferencia de Datos", global::Wizard_Migracion_Tress.Properties.Resources.Agt_Action_Success);
                }
                else
                {
                    if (nextclick == true)
                    {
                        FbCommand query = new FbCommand("select first 1 COUNT(*) from Usuario", conexion);
                        int intento = int.Parse(query.ExecuteScalar().ToString());
                    }
                }

                conexion.Close();
                basedatosfb = txtfbBaseComparte.Text;
                servidorfb = txtfbServerComparte.Text;

                return true;
            }
            catch (Exception exc)
            {
                string msg = "Causa Desconocida";
                if (mensajes == true || WizardPages1.SelectedIndex > 4)
                {
                    if (exc.ToString().Contains("open file"))
                    {
                        msg = "No se pudo abrir el archivo seleccionado :" + uncPath;
                    }
                    if (exc.ToString().Contains("Table unknown"))
                    {
                        msg = "Base de datos inválida";
                    }
                    if (exc.ToString().Contains("password"))
                    {
                        msg = "Usuario o Contraseña Inválidos";
                    }
                    if (exc.ToString().Contains("host"))
                    {
                        msg = "Servidor Incorrecto";
                    }
                    if (exc.ToString().Contains("connection string argument"))
                    {
                        msg = "Debes de seleccionar una base de datos";
                    }
                    MessageBox.Show("¡ Conexión  FireBird Falló ! ; " + msg, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                conexionfbstr = "";
                return false;

            }
        }

        private void btnConeccionSqlComparte_Click(object sender, EventArgs e)
        {
            Pruevaclick = true;
            probarsqlComparte(true);
            txtSqlServerComparte.Focus();
        }
        public bool probarsqlComparte(bool mensajes)
        {

            NombreEmpresa = "";
            Program.CDEmpresa = "";
            Program.ConexionMaster = "";
            Program.NombreComparte = "";
            SqlConnection conexion = new SqlConnection();
            FbConnection conexionfb = new FbConnection();




            if (checkWSA.Checked)
            {
                conexionSql = "Database=" + DropDownSqlBaseComparte.Text + " ; server=" + txtSqlServerComparte.Text + " ; Trusted_Connection=Yes;";
                Program.ConexionMaster = "Database=Master; server=" + txtSqlServerComparte.Text + "; Trusted_connection=Yes;";
            }
            else
            {
                conexionSql = "User id=" + TxtsqlUsuarioComparte.Text + ";Password=" + TxtsqlPassComparte.Text + " ;Database=" + DropDownSqlBaseComparte.Text + " ;Server=" + txtSqlServerComparte.Text + " ; ";
                Program.ConexionMaster = "User id=" + TxtsqlUsuarioComparte.Text + ";Password=" + TxtsqlPassComparte.Text + " ;Database= Master ;Server=" + txtSqlServerComparte.Text + " ; ";
                Program.NombreComparte = CmBComparte.Text;
                Program.nombrebaseDatos = TxtBaseDatosSQL.Text;
              
            }
            try
            {




                conexion = new SqlConnection(conexionSql);
                conexion.Open();
                SqlConnection conexioncomp = new SqlConnection(conexionComparte);






                if (mensajes == true && nextclick == false)
                {
                   
                    SqlCommand query = new SqlCommand("select TOP(1) COUNT(*) from Company", conexion);
                    int intento = int.Parse(query.ExecuteScalar().ToString());
                    conexionfb = new FbConnection(conexionfbstr);
                    conexionfb.Open();
                    FbCommand queryfb = new FbCommand("select cl_texto from Claves where cl_numero = 100", conexionfb);
                    SqlCommand querysql = new SqlCommand("select cl_texto from Claves where cl_numero = 100", conexion);
                    try
                    {
                        string codigosql = querysql.ExecuteScalar().ToString();
                        int iCodigoSQL = int.Parse(codigosql);
                        string codigofb = queryfb.ExecuteScalar().ToString();
                        Program.VersionCompartesql = codigosql;
                        conexion.Close();
                        if (codigofb != codigosql && iCodigoSQL < K_MINIMO_PATCH_DESTINO)
                        {
                            MessageBox.Show(this, "Estructuras no Compatibles" + Environment.NewLine + "Estructura FB  = " + codigofb + Environment.NewLine + "Estructura SQL = " + codigosql + Environment.NewLine + "Por favor seleccionar otra base de datos Destino o cambiar de base de datos Fuente", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;

                        }
                        else
                        {
                            alerta("Conexión Válida", "Transferencia de Datos", global::Wizard_Migracion_Tress.Properties.Resources.Agt_Action_Success);
                        }
                    }
                        catch
                        {
                            throw new System.ArgumentException("Versión de base de datos no localizada"+Environment.NewLine+" Es necesario reconstruir la base de datos", "");
                        }
                   
                }
                else
                {

                    if (nextclick == true)
                    {
                       
                        SqlCommand query = new SqlCommand("select top(1) count(*) from company", conexion);
                        int intento = int.Parse(query.ExecuteScalar().ToString());
                        conexionfb = new FbConnection(conexionfbstr);
                        conexionfb.Open();

                        FbCommand queryfb = new FbCommand("select cl_texto from Claves where cl_numero = 100", conexionfb);
                        SqlCommand querysql = new SqlCommand("select cl_texto from Claves where cl_numero = 100", conexion);

                        try
                        {
                            string codigosql = querysql.ExecuteScalar().ToString();
                            int iCodigoSQL = int.Parse(codigosql);
                            string codigofb = queryfb.ExecuteScalar().ToString();
                            Program.VersionCompartesql = codigosql;
                            try
                            {
                                querysql = new SqlCommand(@"
                                                        if not exists( SELECT * FROM sysObjects WHERE NAME = 'CuentaTim')
                                                        CREATE TABLE CuentaTim(
                                                        CT_CODIGO Codigo,
                                                        CT_DESCRIP Descripcion, 
                                                        CT_ID FolioGrande,
                                                        CT_PASSWRD Formula,
                                                        CT_ACTIVO Booleano,
                                                        US_CODIGO Usuario,
                                                        US_FEC_MOD Fecha,
                                                        CONSTRAINT PK_CuentaTim  PRIMARY KEY 
                                                        (
                                                        CT_CODIGO
                                                        )
                                                        ) ", conexion);
                                querysql.ExecuteNonQuery();
                            }
                            catch { }
                            conexion.Close();
                            if (codigofb != codigosql && iCodigoSQL < K_MINIMO_PATCH_DESTINO)
                            {
                                MessageBox.Show(this, "Estructuras no Compatibles" + Environment.NewLine + "Estructura FB  = " + codigofb + Environment.NewLine + "Estructura SQL = " + codigosql + Environment.NewLine + "Por favor seleccionar otra base de datos Destino o cambiar de base de datos Fuente", "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;

                            }
                        }
                        catch
                        {
                            throw new System.ArgumentException("Versión de base de datos no localizada"+Environment.NewLine+" Es necesario reconstruir la base de datos", "");
                        }
                        
                    }
                   
                }
                basedatosql = DropDownSqlBaseComparte.Text;
                servidorsql = txtSqlServerComparte.Text;
                    return true;
                
            }
            catch (Exception exc)
            {
                string msg=exc.Message;
                if (exc.ToString().Contains("Versión"))
                {
                    msg = exc.Message.ToString();
                }
                if (exc.ToString().Contains("Login"))
                {
                    msg = "Usuario o Contraseña Inválidos";
                }
                if (exc.ToString().Contains("The server was not found"))
                {
                    msg="No se encontro el servidor";
                }
                
                if(exc.ToString().Contains("Invalid object name"))
                {
                    msg="Base de datos Inválida";
                }
                    if (mensajes == true || WizardPages1.SelectedIndex > 4)
                    {
                        MessageBox.Show(this, "¡ Conexión  MSSQL Falló ! ;" + msg, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show(this, msg, "Transferencia de Datos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                     
                    }
                
                nextclick = false;
                conexionSql = "";
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                if (conexionfb.State == ConnectionState.Open)
                {
                    conexionfb.Close();
                }
                return false;
               
               
            }
        }

        private void DropDownSqlBaseComparte_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void DropDownSqlBaseComparte_DropDown(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            DropDownSqlBaseComparte.Text = "";
            probarsqlComparte(false);
            SqlConnection conexion = new SqlConnection(conexionSql);
            try
            {
                DropDownSqlBaseComparte.Items.Clear();
                SqlCommand obtenertablas = new SqlCommand("select name from sys.databases WHERE name NOT IN ('master', 'tempdb', 'model', 'msdb');", conexion);
                obtenertablas.Connection.Open();
                SqlDataReader reader = obtenertablas.ExecuteReader();
                while (reader.Read())
                {
                    DropDownSqlBaseComparte.Items.Add(reader["name"].ToString());
                }
                obtenertablas.Connection.Close();
            }
            catch
            {

            }
            this.Cursor = Cursors.Default;
        }

        private void label31_Click(object sender, EventArgs e)
        {

        }

        private void label32_Click(object sender, EventArgs e)
        {

        }

        private void CmBEmpresa_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void CmBComparte_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }

        
        }
       
    }
