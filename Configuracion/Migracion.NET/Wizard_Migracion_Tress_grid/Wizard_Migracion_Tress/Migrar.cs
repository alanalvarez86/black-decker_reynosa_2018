﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security;
using FirebirdSql.Data.Client;
using System.Xml.Linq;
using System.Xml;
using System.Web;
using System.Text.RegularExpressions;
using FirebirdSql.Data.FirebirdClient;

namespace Wizard_Migracion_Tress
{
   class Migrar
    {
       
        bool registroscalculados = false;
        public bool tpresta;
        List<string> textos;
        List<int> numeros;
        public bool comprobando = false;
        int erroresMovimien = 1;
        public int advertencias;
        StreamWriter outfile2;
        int NoAdvertencia;
        public int recorridoTablas = 0;
        public int FilasMigrar = 0;
        public double filasTemporales = 0; // cantidad de filas analizadas 
        SqlConnection conexionsql = new SqlConnection();
        public bool finalizoProceso = false;
        public string NombreTabla;
        FbConnection conexionbulk = new FbConnection();
        FbConnection conexionerror = new FbConnection();
        int bulksize = 1000;
        int bulkerrorsize = 100;
        public int errores,errorestotales;
        public double FilasTabla = 0;
        public double FilasTotales = 0;
        public bool InicioProceso = false;
        public int conexioncaida;
        public bool bulkednormal = false;
        public bool bulkederror = false;
        public string rutabitacora;
        public string rutadetalle;
        public bool pausa = false;
        public int tablasrecorridas;
        public bool pausado = false;
        List<extraQuery> ListaQuerys = new List<extraQuery>();
        List<string[]> listaErroresMovimien = new List<string[]>();
        bool insercionunitaria;
        public diccionario DiccionarioMigracion = new diccionario();
        bool tkardexfk;
        public int tablapos;
        public string tiempoenborrar="";
        public string[,] tablaresulados;
        int tablaactual;
        public string borrandotabla="";
        string[] arreglodetablas ;
        string[] arregloborrar;
        string cambiosreporte = "";
        int reportesagregados;
        public bool finalizoPreparacion = false;
        string cadenadeconexionsql;
        private string[,] lista;
        public bool bitacorainicia = false;
        public bool detenerMigracion = false;
        public string conexionsqlbulk;
        public string Rutabase = "";
        public string RutaResumen = "";
        public int borradas = 0;
        int times = 1;
        bool TerminaTabla;
        string excmessage;
        public string reportnom;
        bool timeoutError = false;
        bool constraintError = false;
        bool falloBulk = true;
        string[,] derechos = new string[19, 2];
        string[] queryOpciones = new string[2];
        private void generalista()
        {
            string query = "select c.name as colname ,o.name as tabname from sys.computed_columns as c, sys.objects as o   where o.object_id = c.object_id ";
         SqlCommand command = new SqlCommand(query, conexionsql);
         SqlDataReader reader = command.ExecuteReader();
         int length = 0;
         while (reader.Read())
         {
             length++;
         
         
         }
         reader.Close();
         lista = new string[length, 2];
      
         reader= command.ExecuteReader();
         length =0;
         while(reader.Read())
         {
             lista[length, 0] = reader["colname"].ToString();
             lista[length, 1] = reader["tabname"].ToString();
             length++;
         }
         reader.Close();
        }
        public Migrar(string conexionStringFireBird, string ConexionStringSql,string ruta)
        {
            rutabitacora = ruta;
            cadenadeconexionsql = ConexionStringSql ;
            rutadetalle = Path.GetDirectoryName(rutabitacora) +"\\" +Path.GetFileNameWithoutExtension(rutabitacora) + "_Detalle.CSV";
            Rutabase = Path.GetDirectoryName(rutabitacora) + "\\" + Path.GetFileNameWithoutExtension(rutabitacora);
            RutaResumen = Rutabase + "_Resumen.CSV";
            conexionsql.ConnectionString = cadenadeconexionsql;
            conexionbulk.ConnectionString = conexionStringFireBird;            
            conexionerror.ConnectionString = conexionStringFireBird;
            conexionsqlbulk = cadenadeconexionsql;
            Directory.CreateDirectory(Path.GetDirectoryName(rutabitacora));
            
        }
        public void iterador()
        {
            try
            {
                using (StreamWriter outfile = new StreamWriter(rutadetalle, false))
                {
                    outfile.Write("No,No Tabla,Nombre Tabla,Tipo,Detalle,Mensaje," + Environment.NewLine);
                }
                using (StreamWriter outfile = new StreamWriter(rutabitacora, false))//inicia detalle de bitacora
                {
                    outfile.Write("Tabla,Porcentaje,Transferidas,Total" + Environment.NewLine);
                }
                bitacorainicia = true;
                extraQuery buscador = new extraQuery();
                arreglodetablas = TablasMigracion();
                arregloborrar = TablasMigracion();
                tablaresulados = new string[TablasMigracion().Length, 3];

                // QuerysEspeciales.Agregar("PEN_PORCEN", "TP_CODIGO,CB_CODIGO,ps_ORDEN,pp_PORCENT", "");
                conexionbulk.Open();//////////////////
                conexionerror.Open();
                conexionsql.Open();//////////////////
                InicioProceso = true;

                generalista();




                int inversoborrar = 0;
                for (int i = arregloborrar.Length - 1; i >= 0; i--)
                {
                    try
                    {
                        if (DiccionarioMigracion.elemento[arregloborrar[i]].Migrar == true && DiccionarioMigracion.elemento[arregloborrar[i]].tipo == "Tabla")
                        {
                            if (DiccionarioMigracion.elemento[arregloborrar[i]].fbrows.Trim() != "")
                            {
                                if (int.Parse(DiccionarioMigracion.elemento[arregloborrar[i]].fbrows) > 0)
                                {
                                    borrandotabla = arregloborrar[i];
                                    borrar(arregloborrar[i]);
                                }
                            }
                        }
                    }
                    catch
                    {

                    }
                    inversoborrar++;
                    borradas = (inversoborrar * 100) / arregloborrar.Length;

                }
                borrandotabla = "";
                finalizoPreparacion = true;

                //ejecutarparcial();
                // ejecutatkardexfk();

                for (recorridoTablas = 0; recorridoTablas < arreglodetablas.Length; recorridoTablas++) // se recorren todas las tablas 
                {
                   
                    tablaresulados[recorridoTablas, 0] = "0";
                    tablaresulados[recorridoTablas, 1] = "0";
                    tablasrecorridas++;
                    NombreTabla = arreglodetablas[recorridoTablas];// Encontramos la actual
                    //tabla especial genera salta el proceso de bulks y genera inserciones individuales
                    pausar();//chequeo de pausa
                    //ciclo para introducir tablas en orden 
                    FilasTotales = FilasTotales + FilasTabla + errores;
                    FilasTabla = 0;
                    filasTemporales = 0;
                    DiccionarioMigracion.elemento[NombreTabla].Condiciones = DiccionarioMigracion.elemento[NombreTabla].Condiciones.Replace("&lt;", "<");
                    DiccionarioMigracion.elemento[NombreTabla].Condiciones = DiccionarioMigracion.elemento[NombreTabla].Condiciones.Replace("&gt;", ">");
                    DiccionarioMigracion.elemento[NombreTabla].Condiciones = DiccionarioMigracion.elemento[NombreTabla].Condiciones.Replace("&amp;", "&");
                    DiccionarioMigracion.elemento[NombreTabla].Condiciones = DiccionarioMigracion.elemento[NombreTabla].Condiciones.Replace("&apos;", "'");
                    DiccionarioMigracion.elemento[NombreTabla].Condiciones = DiccionarioMigracion.elemento[NombreTabla].Condiciones.Replace("&quot;", "\"");
                    if (NombreTabla == "BITACORA" || NombreTabla == "AUSENCIA" || NombreTabla == "CHECADAS" || NombreTabla == "MOVIMIEN")
                    {
                        Program.reiniciaServidor(ref conexionerror, ref conexionbulk, ref conexionsql);

                    }
                    errores = 0;
                    if (DiccionarioMigracion.elemento[NombreTabla].tipo == "Tabla" && DiccionarioMigracion.elemento[NombreTabla].Migrar)
                    {
                        MigracionDeTablas(NombreTabla);
                    }
                    if (DiccionarioMigracion.elemento[NombreTabla].tipo == "Proceso" && DiccionarioMigracion.elemento[NombreTabla].Migrar)
                    {
                        ProcesosMigracion(NombreTabla);
                    }
                    if (DiccionarioMigracion.elemento[NombreTabla].tipo == "Query" && DiccionarioMigracion.elemento[NombreTabla].Migrar)
                    {
                        QueryMigracion(NombreTabla);
                    }
                    if (detenerMigracion == true)
                    {
                        recorridoTablas = arreglodetablas.Length;
                    }
                    if (NombreTabla == "BITACORA" || NombreTabla == "AUSENCIA" || NombreTabla == "CHECADAS" || NombreTabla == "MOVIMIEN")
                    {
                        Program.reiniciaServidor(ref conexionerror, ref conexionbulk, ref conexionsql);

                    }
                }
                if (detenerMigracion == false && !Program.MigrarComparte)
                {
                    Comprobacion();//inicia Comprobacion y generacion de resultados;      
                }
                if (Program.MigrarComparte)
                {
                    AjustaCompany();
                }
                FilasTabla = 0;
                filasTemporales = 0;
                errores = 0;
                NombreTabla = "";
                Program.horafin = DateTime.Now;

                using (StreamWriter outfile = new StreamWriter(rutabitacora, true))//inicia detalle de bitacora
                {

                    string TiemposTotales = " ,, ,Hora de Inicio =" + Program.horaini.ToString("hh:mm:ss tt") + Environment.NewLine
                                     + " ,, ,Hora de Finalizacion =" + Program.horafin.ToString("hh:mm:ss tt") + Environment.NewLine
                                     + " , ,,Tiempo Transcurrido= " + (Program.horafin - Program.horaini).ToString() + Environment.NewLine;
                    outfile.Write(TiemposTotales);
                }


                if (Program.MigrarComparte == false)
                {
                    generareportes();
                }
                    finalizoProceso = true;

                conexionbulk.Close();
                conexionerror.Close();
                conexionsql.Close();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }
        public void generareportes()
        {
            for (int i = 0; i < 3; i++)
            {

                escribereportes(i);

            }

        }
        public void escribereportes(int reporte)
        {
             reportnom = Rutabase + "_Extras.csv";
            string[] commandtext = new string[3];
            commandtext[0] = @"select R.RE_CODIGO, R.RE_NOMBRE from REPORTE R
where ( ( select count(*) from CAMPOREP C where C.RE_CODIGO = R.RE_CODIGO and UPPER(C.CR_FORMULA) like '%SELECT%' ) > 0 )
";
            commandtext[1] = @"select CO_NUMERO, CO_DESCRIP from CONCEPTO
where ( UPPER(CO_FORMULA) like '%SELECT%' )
";

            commandtext[2] = @"select NP_FOLIO, NP_NOMBRE from NOMPARAM
where ( UPPER( cast(NP_FORMULA as varchar(255) ) ) like '%SELECT%' )
";

            string[] text = new string[3];
            text[0] = "Reportes con fórmulas que consultan querys directos a la BD con @select";
            text[1] = "Conceptos con fórmulas que consultan querys directos a la BD con @select";
            text[2] = "Parametros con fórmulas que consultan querys directos a la BD con @select";
            SqlConnection conexionsql = new SqlConnection(Program.Prepara.conexionstringsql);
            SqlCommand query = new SqlCommand();
            query.Connection = conexionsql;
            query.CommandText = commandtext[reporte];

            conexionsql.Open();
            SqlDataReader reader = query.ExecuteReader();
            bool header = false;
            while (reader.Read())
            {
                if (header == false)
                {
                    using (StreamWriter outfile = new StreamWriter(reportnom, true, System.Text.Encoding.Default)) // se guardan en la bitacora los resultados 
                    {
                        string encabezado = "";
                        for (int columnas = 0; columnas < reader.FieldCount; columnas++)
                        {
                            encabezado += reader.GetName(columnas);
                            if (columnas < reader.FieldCount - 1)
                            {
                                encabezado += ",";
                            }
                        }
                        outfile.Write(Environment.NewLine + text[reporte] + Environment.NewLine + encabezado + Environment.NewLine);
                    }
                    header = true;
                }
                using (StreamWriter outfile = new StreamWriter(reportnom, true, System.Text.Encoding.Default)) // se guardan en la bitacora los resultados 
                {
                    string encabezado = "";
                    for (int columnas = 0; columnas < reader.FieldCount; columnas++)
                    {

                        encabezado += reader[columnas];
                        if (columnas < reader.FieldCount - 1)
                        {
                            encabezado += ",";
                        }

                    }
                    outfile.Write(encabezado + Environment.NewLine);
                }
            }
            reader.Close();
            conexionsql.Close();
        }
        private bool encuentratablagenerada(string nombretabla)
        {
            bool vuelve = false;
            for (int i = 0; i < lista.GetLength(0); i++)
            {
                if (nombretabla == lista[i, 1])
                {
                    vuelve = true;
                }
            }
            return vuelve;
        
        }
        public string[,] ResumenMigracion;
        private void Comprobacion()
        {
            ResumenMigracion = new string[5, 3];
            ResumenMigracion[0, 0] = "Total Empleados Activos";
            ResumenMigracion[1, 0] = "Total Saldos de Vacaciones de todos los empleados";
            ResumenMigracion[2, 0] = "Total Saldos de Ahorros de todos los empleados";
            ResumenMigracion[3, 0] = "Total Saldos de Préstamos de todos los empleados";
            ResumenMigracion[4, 0] = "Total Saldos de Nómina del ultimo año";

            FbDataReader CompruebaReaderPro;
            SqlDataReader CompruebaReaderCorp;
           
            bool ErrorComprueba;
            double compruebaTotalPro;
            double CompruebaTotalCorp;
            using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
            {
                outfile.Write("Concepto,Resultado" + Environment.NewLine);
            }
            bool errorEmpleados = false;
            comprobando = true;
            if (conexionerror.State == ConnectionState.Closed)
            {
                conexionerror.Open();
            }
            if (conexionsql.State == ConnectionState.Closed)
            {
                conexionsql.Open();
            }
            /////////Comprueba Empleados Altas 
            FbCommand ProCompruebaQuery = new FbCommand("select count(*) from colabora where cb_activo ='S'",conexionerror);
            SqlCommand CorpCompruebaQuery = new SqlCommand("select count(*) from colabora where cb_activo ='S'",conexionsql);
            int empleadospro = int.Parse(ProCompruebaQuery.ExecuteScalar().ToString());
            int empleadoscorp = int.Parse(CorpCompruebaQuery.ExecuteScalar().ToString());
            if (empleadoscorp != empleadospro)
            {
                errorEmpleados = true;
            }
            if (errorEmpleados == true)
            {
                using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                {
                    outfile.Write("Empleados Activos,Existen diferencias entre ambas plataformas" + Environment.NewLine);
                }
                ResumenMigracion[0, 1] = "Existen diferencias entre ambas plataformas";
              
            }
            else
            {
                using (StreamWriter outfile = new StreamWriter(RutaResumen, true,System.Text.Encoding.Default))
                {
                    outfile.Write("Total Empleados Activos,La información coincide en Firebird y MSSQL" +  Environment.NewLine);
                }
                ResumenMigracion[0, 1] = "La información coincide en Firebird y MSSQL";
             
            }
            ResumenMigracion[0, 2] = "No Mostrar";
            try{
            ProCompruebaQuery.CommandText = "select sum(va_d_pago) as VA_D_PAGO ,sum(va_pago) as VA_PAGO,sum(va_s_pago) as VA_S_PAGO,sum(va_d_gozo) as VA_D_GOZO ,sum(va_gozo) as VA_GOZO,sum(va_s_gozo) as VA_S_GOZO ,Sum(VA_PRIMA) as VA_PRIMA, sum(va_d_prima) as VA_D_PRIMA, Sum(Va_p_prima) as VA_P_PRIMA, sum(Va_s_Prima) VA_S_PRIMA from vacacion";
            CorpCompruebaQuery.CommandText = "select sum(va_d_pago) as VA_D_PAGO ,sum(va_pago) as VA_PAGO,sum(va_s_pago) as VA_S_PAGO,sum(va_d_gozo) as VA_D_GOZO ,sum(va_gozo) as VA_GOZO,sum(va_s_gozo) as VA_S_GOZO , Sum(VA_PRIMA) as VA_PRIMA, sum(va_d_prima) as VA_D_PRIMA, Sum(Va_p_prima) as VA_P_PRIMA, sum(Va_s_Prima) VA_S_PRIMA from vacacion";
             CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();
             CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
            CompruebaReaderCorp.Read();
            CompruebaReaderPro.Read();
             ErrorComprueba = false;
             string MensajeResultadoPro = "";
             string MensajeResultadoCorp="";
            for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
            {
                if (CompruebaReaderPro[i].ToString() != "" && CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString() != "")
                {
                    compruebaTotalPro = (int)Math.Round(double.Parse(CompruebaReaderPro[i].ToString()),5);
                    CompruebaTotalCorp = (int)double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                    if (compruebaTotalPro != CompruebaTotalCorp)
                    {
                        ErrorComprueba = true;
                        MensajeResultadoCorp += CompruebaReaderPro.GetName(i).ToString() + "=" + double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString()).ToString() + "|";
                        MensajeResultadoPro += CompruebaReaderPro.GetName(i).ToString() + "="+ Math.Round(double.Parse(CompruebaReaderPro[i].ToString()),5).ToString()+"|"; 
                    }
                    
                }
                else
                {
                    if (CompruebaReaderPro[i].ToString() != CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString())
                    {
                        ErrorComprueba = true;
                    }
                }
            }
            if (MensajeResultadoCorp.Length > 0)
            {
                MensajeResultadoCorp = MensajeResultadoCorp.Substring(0, MensajeResultadoCorp.Length - 1);
                MensajeResultadoPro = MensajeResultadoPro.Substring(0, MensajeResultadoPro.Length - 1);
            }
CompruebaReaderPro.Close();
            CompruebaReaderCorp.Close();
            if (ErrorComprueba == true)
            {
                using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                {
                    outfile.Write("Saldos de Vacaciones de todos los empleados,Existen diferencias entre ambas plataformas, ver reporte de Vacaciones" + Environment.NewLine);
                    ResumenMigracion[1, 1] = "Existen diferencias entre ambas plataformas, ver reporte de Vacaciones";
                   
                    ResumenMigracion[1, 2] = Rutabase + "_Reporte_Vacaciones.csv";
                }
                using (StreamWriter outfile = new StreamWriter(Rutabase+"_Reporte_Vacaciones.csv", true))
                {
                    outfile.Write("Datos en Profesional, Datos en Corporativa" + Environment.NewLine);
                }
                //Imprime Error de Vacaciones en bitacora
                ProCompruebaQuery.CommandText = "select VA_D_PAGO,	VA_PAGO,	VA_S_PAGO,	VA_D_GOZO,	VA_GOZO,	VA_S_GOZO,  VA_PRIMA,	VA_D_PRIMA,	VA_P_PRIMA,	VA_S_PRIMA,CB_CODIGO,VA_FEC_INI,VA_TIPO,US_CODIGO  from vacacion order by cb_codigo,VA_FEC_INI,VA_TIPO,US_CODIGO";

                CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();

                while (CompruebaReaderPro.Read())
                {
                    bool columnerror = false;

                    double ValPro;
                    double ValCorp;
                    string where = "";
                    for (int i = 9; i < 13; i++)
                    {
                        where += CompruebaReaderPro.GetName(i).ToString() + "='" + CompruebaReaderPro[i].ToString() + "'";
                        if (i != 12)
                        {
                            where += " and ";
                        }

                    }

                    CorpCompruebaQuery.CommandText = "select VA_D_PAGO,	VA_PAGO,	VA_S_PAGO,	VA_D_GOZO,	VA_GOZO,	VA_S_GOZO,  VA_PRIMA,	VA_D_PRIMA,	VA_P_PRIMA,	VA_S_PRIMA from vacacion where " + where + "  order by cb_codigo,VA_FEC_INI,VA_TIPO,US_CODIGO";
                    CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                    string notfounderror = "";
                    CompruebaReaderCorp.Read();
                    try
                    {
                        for (int i = 0; i < 10; i++)
                        {
                            ValPro = double.Parse(CompruebaReaderPro[i].ToString());
                            ValCorp = double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                            if (ValPro != ValCorp)
                            {
                                columnerror = true;
                            }
                        }
                    }
                    catch
                    {
                        notfounderror = "Registro No Encontrado  con los datos : " + where;
                        columnerror = true;
                    }
                    if (columnerror == true)
                    {
                        if (notfounderror != "")
                        {

                            string mensaje = "";
                            for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                            {
                                mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                            }
                            mensaje += ",No Encontrado";
                            imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Vacaciones.csv");
                            notfounderror = "";
                        }
                        else
                        {
                            string mensaje = "";
                            for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                            {
                                mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                            }
                            mensaje += ",";
                            for (int i = 0; i < CompruebaReaderCorp.FieldCount; i++)
                            {
                                mensaje += CompruebaReaderCorp.GetName(i) + "=" + CompruebaReaderCorp[i].ToString() + "|";
                            }
                            imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Vacaciones.csv");

                            //Reporta Error en los valores

                        }
                    }
                    CompruebaReaderCorp.Close();
                }
            }
            else
            {
                using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                {
                    outfile.Write("Total Saldos de Vacaciones de todos los empleados,La información coincide en Firebird y MSSQL" + Environment.NewLine);
                    ResumenMigracion[1, 1] = "La información coincide en Firebird y MSSQL";
                    
                    ResumenMigracion[1, 2] = "No Mostrar";
                }
            }
        } catch (Exception ex)
            {
                imprimirreporte("Error al leer Vacaciones," + ex.Message + Environment.NewLine, RutaResumen);
                ResumenMigracion[1, 1] = "Error al leer Vacaciones";
             
                ResumenMigracion[1, 2] = "No Mostrar";
            }
            //Inicia Comprobacion Ahorro 
            ////////
            /////////
            /////////
            //////////
            try
            {
                ProCompruebaQuery.CommandText = "select sum(AH_ABONOS)as AH_ABONOS, sum(AH_CARGOS) as AH_CARGOS, sum(AH_SALDO) as AH_SALDO, sum(AH_TOTAL) as AH_TOTAL FROM AHORRO";
                CorpCompruebaQuery.CommandText = "select sum(AH_ABONOS)as AH_ABONOS, sum(AH_CARGOS) as AH_CARGOS, sum(AH_SALDO) as AH_SALDO, sum(AH_TOTAL) as AH_TOTAL FROM AHORRO";
                CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();
                CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                CompruebaReaderCorp.Read();
                CompruebaReaderPro.Read();
                ErrorComprueba = false;
                string MensajeResultadoCorp = "";
                string MensajeResultadoPro = "";
                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                {

                    if (CompruebaReaderPro[i].ToString() != "" && CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString() != "")
                    {
                        compruebaTotalPro = (int)Math.Round(double.Parse(CompruebaReaderPro[i].ToString()),5);
                        CompruebaTotalCorp = (int)double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                        if (compruebaTotalPro != CompruebaTotalCorp)
                        {
                            ErrorComprueba = true;
                            MensajeResultadoCorp += CompruebaReaderPro.GetName(i).ToString() + "=" + double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString()).ToString() + "|";
                            MensajeResultadoPro += CompruebaReaderPro.GetName(i).ToString() + "=" + Math.Round(double.Parse(CompruebaReaderPro[i].ToString()), 5).ToString() + "|"; 
                        }
                    }
                    else
                    {
                        if (CompruebaReaderPro[i].ToString() != CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString())
                        {
                            ErrorComprueba = true;

                        }
                    }
                }
                if (MensajeResultadoCorp.Length > 0)
                {
                    MensajeResultadoCorp = MensajeResultadoCorp.Substring(0, MensajeResultadoCorp.Length - 1);
                    MensajeResultadoPro = MensajeResultadoPro.Substring(0, MensajeResultadoPro.Length - 1);
                }
                CompruebaReaderPro.Close();
                CompruebaReaderCorp.Close();
                if (ErrorComprueba == true)
                {
                    using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                    {
                        outfile.Write("Saldos de Ahorros de todos los empleados,Existen diferencias entre ambas plataformas, ver reporte de Ahorro" + Environment.NewLine);
                        ResumenMigracion[2, 1] = "Existen diferencias entre ambas plataformas, ver reporte de Ahorro";
                  
                        ResumenMigracion[2, 2] = Rutabase + "_Reporte_Ahorros.csv";
                    
                    }
                    using (StreamWriter outfile = new StreamWriter(Rutabase + "_Reporte_Ahorros.csv", true))
                    {
                        outfile.Write("Datos en Profesional, Datos en Corporativa" + Environment.NewLine);
                    }
                    ProCompruebaQuery.CommandText = "select AH_ABONOS,AH_CARGOS,AH_SALDO,AH_TOTAL,CB_CODIGO ,AH_TIPO  from ahorro";

                    CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();

                    while (CompruebaReaderPro.Read())
                    {
                        bool columnerror = false;

                        double vapro;
                        double vacorp;
                        string where = "";
                        for (int i = 4; i < 6; i++)
                        {
                            where += CompruebaReaderPro.GetName(i).ToString() + "='" + CompruebaReaderPro[i].ToString() + "'";
                            if (i != 5)
                            {
                                where += " and ";
                            }

                        }

                        CorpCompruebaQuery.CommandText = "select AH_ABONOS,AH_CARGOS,AH_SALDO,AH_TOTAL,CB_CODIGO ,AH_TIPO  from ahorro where " + where;
                        CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                        string notfounderror = "";
                        CompruebaReaderCorp.Read();
                        try
                        {
                            for (int i = 0; i < 6; i++)
                            {
                                vapro = double.Parse(CompruebaReaderPro[i].ToString());
                                vacorp = double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                                if (vapro != vacorp)
                                {
                                    columnerror = true;
                                }
                            }
                        }
                        catch
                        {
                            notfounderror = "Registro No Encontrado  con los datos : " + where;
                            columnerror = true;
                        }
                        if (columnerror == true)
                        {
                            if (notfounderror != "")
                            {

                                string mensaje = "";
                                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                                }
                                mensaje += ",No Encontrado";
                                imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Ahorros.csv");
                                notfounderror = "";
                            }
                            else
                            {
                                string mensaje = "";
                                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                                }
                                mensaje += ",";
                                for (int i = 0; i < CompruebaReaderCorp.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderCorp.GetName(i) + "=" + CompruebaReaderCorp[i].ToString() + "|";
                                }
                                imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Ahorros.csv");

                                //Reporta Error en los valores

                            }
                        }
                        CompruebaReaderCorp.Close();
                    }
                }
                else
                {
                    using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                    {
                        outfile.Write("Total Saldos de Ahorros de todos los empleados,La información coincide en Firebird y MSSQL" + Environment.NewLine);
                        ResumenMigracion[2, 1] = "La información coincide en Firebird y MSSQL";
                       
                        ResumenMigracion[2, 2] = "No Mostrar";
                    }
                }
            }
            catch (Exception ex)
            {
                imprimirreporte("error al leer Ahorros," + ex.Message + Environment.NewLine, RutaResumen);
                ResumenMigracion[2, 1] = "Error al leer Ahorros";
              
                ResumenMigracion[2, 2] = "No Mostrar";
            }
            //Inicia Comprobacion Prestamos
            ////////
            /////////
            /////////
            //////////
            try
            {

                ProCompruebaQuery.CommandText = "select sum(pr_abonos) as PR_ABONOS , sum (pr_cargos) as PR_CARGOS , sum (pr_monto) as PR_MONTO, sum (pr_saldo) as PR_SALDO, sum(pr_total) as PR_TOTAL from prestamo";
                CorpCompruebaQuery.CommandText = "select sum(pr_abonos) as PR_ABONOS , sum (pr_cargos) as PR_CARGOS , sum (pr_monto) as PR_MONTO, sum (pr_saldo) as PR_SALDO, sum(pr_total) as PR_TOTAL from prestamo";
                CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();
                CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                CompruebaReaderCorp.Read();
                CompruebaReaderPro.Read();
                ErrorComprueba = false;
                string MensajeResultadoPro = "";
                string MensajeResultadoCorp = "";
                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                {
                    if (CompruebaReaderPro[i].ToString() != "" && CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString() != "")
                    {
                        compruebaTotalPro = (int)Math.Round(double.Parse(CompruebaReaderPro[i].ToString()),5);
                        CompruebaTotalCorp = (int)double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                        if (compruebaTotalPro != CompruebaTotalCorp)
                        {
                            ErrorComprueba = true;
                            MensajeResultadoCorp += CompruebaReaderPro.GetName(i).ToString() + "=" + double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString()).ToString() + "|";
                            MensajeResultadoPro += CompruebaReaderPro.GetName(i).ToString() + "=" + Math.Round(double.Parse(CompruebaReaderPro[i].ToString()), 5).ToString() + "|"; 
                        }
                    }
                    else
                    {
                        if (CompruebaReaderPro[i].ToString() != CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString())
                        {
                            ErrorComprueba = true;

                        }
                    }
                }
                if (MensajeResultadoCorp.Length > 0)
                {
                    MensajeResultadoCorp = MensajeResultadoCorp.Substring(0, MensajeResultadoCorp.Length - 1);
                    MensajeResultadoPro = MensajeResultadoPro.Substring(0, MensajeResultadoPro.Length - 1);
                }
                CompruebaReaderPro.Close();
                CompruebaReaderCorp.Close();
                if (ErrorComprueba == true)
                {
                    using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                    {
                        outfile.Write("Saldos de Préstamos de todos los empleados,Existen diferencias entre ambas plataformas, ver reporte de Prestamos" + Environment.NewLine);
                        ResumenMigracion[3, 1] = "Existen diferencias entre ambas plataformas, ver reporte de Prestamos";
                        
                        ResumenMigracion[3, 2] = Rutabase + "_Reporte_Prestamos.csv";
                    }
                    using (StreamWriter outfile = new StreamWriter(Rutabase + "_Reporte_Prestamos.csv", true))
                    {
                        outfile.Write("Datos en Profesional, Datos en Corporativa" + Environment.NewLine);
                    }
                    ProCompruebaQuery.CommandText = "select pr_abonos,PR_Cargos,PR_MONTO, PR_SALDO, PR_TOTAl,pr_referen,pr_tipo,cb_codigo from prestamo";

                    CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();

                    while (CompruebaReaderPro.Read())
                    {
                        bool columnerror = false;

                        double vapro;
                        double vacorp;
                        string where = "";
                        for (int i = 5; i < 8; i++)
                        {
                            where += CompruebaReaderPro.GetName(i).ToString() + "='" + CompruebaReaderPro[i].ToString() + "'";
                            if (i != 7)
                            {
                                where += " and ";
                            }

                        }

                        CorpCompruebaQuery.CommandText = "select pr_abonos,PR_Cargos,PR_MONTO, PR_SALDO, PR_TOTAl from prestamo where " + where;
                        CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                        string notfounderror = "";
                        CompruebaReaderCorp.Read();
                        try
                        {
                            for (int i = 0; i < 5; i++)
                            {
                                vapro = double.Parse(CompruebaReaderPro[i].ToString());
                                vacorp = double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                                if (vapro != vacorp)
                                {
                                    columnerror = true;
                                }
                            }
                        }
                        catch
                        {

                            notfounderror = "Registro No Encontrado  con los datos : " + where;
                            columnerror = true;
                        }
                        if (columnerror == true)
                        {
                            if (notfounderror != "")
                            {

                                string mensaje = "";
                                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                                }
                                mensaje += ",No Encontrado";
                                imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Prestamos.csv");
                                notfounderror = "";
                            }
                            else
                            {
                                string mensaje = "";
                                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                                }
                                mensaje += ",";
                                for (int i = 0; i < CompruebaReaderCorp.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderCorp.GetName(i) + "=" + CompruebaReaderCorp[i].ToString() + "|";
                                }
                                imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Prestamos.csv");

                                //Reporta Error en los valores

                            }
                        }
                        CompruebaReaderCorp.Close();
                    }
                }
                else
                {
                    using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                    {
                        outfile.Write("Total Saldos de Préstamos de todos los empleados,La información coincide en Firebird y MSSQL" + Environment.NewLine);
                        ResumenMigracion[3, 1] = "La información coincide en Firebird y MSSQL";
                   
                        ResumenMigracion[3, 2] = "No Mostrar";
                    }
                }
            }
            catch (Exception ex)
            {
                imprimirreporte("error al leer Préstamos," + ex.Message + Environment.NewLine, RutaResumen);
                ResumenMigracion[3, 1] = "Error al leer Préstamos";
               
                ResumenMigracion[3, 2] = "No Mostrar";
            
            }
            //Inicia Comprobacion de NOMINA
            ////////
            /////////
            /////////
            //////////
            try
            {

                ProCompruebaQuery.CommandText = "select sum(NO_DEDUCCI) as NO_DEDUCCI,  sum (NO_PERCEPC) as NO_PERCEPC, sum(NO_NETO) as NO_NETO from nomina where pe_year = EXTRACT(YEAR FROM CURRENT_TIMESTAMP);";
                CorpCompruebaQuery.CommandText = "select sum(NO_DEDUCCI) as NO_DEDUCCI, sum (NO_PERCEPC)as NO_PERCEPC, sum(NO_NETO) as NO_NETO from nomina where pe_year = datepart(year,getdate())";
                CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();
                CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                CompruebaReaderCorp.Read();
                CompruebaReaderPro.Read();
                ErrorComprueba = false;
                string MensajeResultadoPro = "";
                string MensajeResultadoCorp = "";
                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                {
                    if (CompruebaReaderPro[i].ToString() != "" && CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString() != "")
                    {
                        compruebaTotalPro = (int)Math.Round(double.Parse(CompruebaReaderPro[i].ToString()),5);
                        CompruebaTotalCorp = (int)double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                        if (compruebaTotalPro != CompruebaTotalCorp)
                        {
                            ErrorComprueba = true;
                            MensajeResultadoCorp += CompruebaReaderPro.GetName(i).ToString() + "=" + double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString()).ToString() + "|";
                            MensajeResultadoPro += CompruebaReaderPro.GetName(i).ToString() + "=" + Math.Round(double.Parse(CompruebaReaderPro[i].ToString()), 5).ToString() + "|"; 
                        }
                    }
                    else
                    {
                        if (CompruebaReaderPro[i].ToString() != CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString())
                        {
                            ErrorComprueba = true;

                        }
                    }
                }
                if (MensajeResultadoCorp.Length > 0)
                {
                    MensajeResultadoCorp = MensajeResultadoCorp.Substring(0, MensajeResultadoCorp.Length - 1);
                    MensajeResultadoPro = MensajeResultadoPro.Substring(0, MensajeResultadoPro.Length - 1);
                }
                CompruebaReaderPro.Close();
                CompruebaReaderCorp.Close();
                if (ErrorComprueba == true)
                {
                    using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                    {
                        outfile.Write("Saldos de Nómina de todos los empleados,Existen diferencias entre ambas plataformas, ver reporte de Nomnina" + Environment.NewLine);
                        ResumenMigracion[4, 1] = "Existen diferencias entre ambas plataformas, ver reporte de Nomina";
                 
                        ResumenMigracion[4, 2] = Rutabase + "_Reporte_Nomina.csv";
                    
                    }
                    using (StreamWriter outfile = new StreamWriter(Rutabase + "_Reporte_Nomina.csv", true))
                    {
                        outfile.Write("Datos en Profesional, Datos en Corporativa" + Environment.NewLine);
                    }
                    ProCompruebaQuery.CommandText = "select NO_DEDUCCI, NO_PERCEPC,NO_NETO, pe_tipo, pe_numero , cb_codigo from nomina where pe_year = EXTRACT(YEAR FROM CURRENT_TIMESTAMP);";

                    CompruebaReaderPro = ProCompruebaQuery.ExecuteReader();

                    while (CompruebaReaderPro.Read())
                    {
                        bool columnerror = false;

                        double vapro;
                        double vacorp;
                        string where = "";
                        for (int i = 3; i < 6; i++)
                        {
                            where += CompruebaReaderPro.GetName(i).ToString() + "='" + CompruebaReaderPro[i].ToString() + "'";
                            if (i != 5)
                            {
                                where += " and ";
                            }

                        }

                        CorpCompruebaQuery.CommandText = "select NO_DEDUCCI, NO_PERCEPC,NO_NETO, pe_tipo, pe_numero , cb_codigo from nomina where pe_year = datepart(year,getdate()) and " + where;
                        CompruebaReaderCorp = CorpCompruebaQuery.ExecuteReader();
                        string notfounderror = "";
                        CompruebaReaderCorp.Read();
                        try
                        {
                            for (int i = 0; i < 3; i++)
                            {
                                vapro = double.Parse(CompruebaReaderPro[i].ToString());
                                vacorp = double.Parse(CompruebaReaderCorp[CompruebaReaderPro.GetName(i)].ToString());
                                if (vapro != vacorp)
                                {
                                    columnerror = true;
                                }
                            }
                        }
                        catch
                        {
                            notfounderror = "Registro No Encontrado  con los datos : " + where;
                            columnerror = true;

                        }
                        if (columnerror == true)
                        {
                            if (notfounderror != "")
                            {

                                string mensaje = "";
                                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                                }
                                mensaje += ",No Encontrado";
                                imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Nomina.csv");
                                notfounderror = "";
                            }
                            else
                            {
                                string mensaje = "";
                                for (int i = 0; i < CompruebaReaderPro.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderPro.GetName(i) + "=" + CompruebaReaderPro[i].ToString() + "|";
                                }
                                mensaje += ",";
                                for (int i = 0; i < CompruebaReaderCorp.FieldCount; i++)
                                {
                                    mensaje += CompruebaReaderCorp.GetName(i) + "=" + CompruebaReaderCorp[i].ToString() + "|";
                                }
                                imprimirreporte(mensaje + Environment.NewLine, Rutabase + "_Reporte_Nomina.csv");

                                //Reporta Error en los valores

                            }
                        }
                        CompruebaReaderCorp.Close();
                    }
                }
                else
                {
                    using (StreamWriter outfile = new StreamWriter(RutaResumen,true,System.Text.Encoding.Default))
                    {
                        outfile.Write("Total Saldos de Nómina de todos los empleados,La información coincide en Firebird y MSSQL" + Environment.NewLine);
                        ResumenMigracion[4, 1] = "La información coincide en Firebird y MSSQL";
                    
                        ResumenMigracion[4, 2] = "No Mostrar";
                    }
                }
            }
            catch(Exception ex)
            {
                imprimirreporte("error al leer Nómina," + ex.Message + Environment.NewLine, RutaResumen);
                ResumenMigracion[4, 1] = "Error al leer Nómina";
             
                ResumenMigracion[4, 2] = "No Mostrar";
            }
       }
        public void imprimirreporte(string mensaje, string NombreArchivo)
       {
           using (StreamWriter outfile = new StreamWriter(NombreArchivo, true))
           {
               outfile.Write(mensaje);
           }
       
       }
        private bool encuentraGenerado(string nombretabla, string nombrecampo)
        {
            bool vuelve = false;
        for(int i=0;i<lista.GetLength(0);i++)
        {
            if (nombretabla == lista[i, 1] && nombrecampo == lista[i, 0])
            {
                vuelve = true;
            }
        }
        return vuelve;
        }
        private void bulkinsert(string queryColumnas, string queryCondicion) //insercion en bulk (1000 en 1000)
           {
              
               pausar();
               string skip = " "; //variable para busqueda ( salta la cantida de filas recorridas)
               if (FilasTabla > 0)
               {
                   skip = " skip " + FilasTabla.ToString();
               }
               SqlBulkCopy bulk = new SqlBulkCopy(conexionsqlbulk);
               bulk.SqlRowsCopied += new SqlRowsCopiedEventHandler(FilasCopiadas);
               bulk.DestinationTableName = NombreTabla;
               FbDataReader lector;
     
            bool repite=false;
            do
            {
                string newquerycolumnas = "";
                FbCommand datosFUente = new FbCommand("select " + skip + " " + queryColumnas + " from " + NombreTabla + " " + queryCondicion, conexionbulk);
                lector = datosFUente.ExecuteReader();
                SqlCommand cardinal = new SqlCommand("select top(1) * from " + NombreTabla, conexionsql);
                SqlDataReader CardinalReader = cardinal.ExecuteReader();
               
             
                //batch, notify y la el aumento de filas tienen el mismo valor 
                
                for (int cardinalindex = 0; cardinalindex < CardinalReader.FieldCount; cardinalindex++)
                {
                    
                    for (int registros = 0; registros < lector.FieldCount; registros++)
                    {
                        
                        if (lector.GetName(registros) == CardinalReader.GetName(cardinalindex))
                        {
                            
                                if (newquerycolumnas.Length == 0 && repite == true)
                                {

                                }
                                else
                                {
                                    if (encuentratablagenerada(NombreTabla))
                                    {
                                        repite = true;

                                        if (!encuentraGenerado(NombreTabla, CardinalReader.GetName(cardinalindex)))
                                        {
                                            newquerycolumnas += lector.GetName(registros)+",";
                                        }

                                    }





                                    if (NombreTabla.Trim() == "NOMINA")
                                    {
                                        repite = true;

                                        if (lector.GetName(registros).Trim() == "NO_RASTREO")
                                        {
                                            newquerycolumnas += "SUBSTRING(NO_RASTREO FROM 1 FOR 32000) as NO_RASTREO,";
                                        }
                                        else
                                        {
                                            newquerycolumnas += lector.GetName(registros) + ",";
                                        }

                                    }
                                
                                    
                                    if (NombreTabla.Trim() == "BITACORA")
                                    {
                                        repite = true;

                                        if (lector.GetName(registros).Trim() == "BI_DATA")
                                        {
                                            newquerycolumnas += "SUBSTRING(BI_DATA FROM 1 FOR 32000) as BI_DATA,";
                                        }
                                        else
                                        {
                                            newquerycolumnas += lector.GetName(registros) + ",";
                                        }

                                    }
                                }
                            


                                if (repite == false || newquerycolumnas.Length==0)
                                {
                                    
                                    bulk.ColumnMappings.Add(new SqlBulkCopyColumnMapping(registros, cardinalindex));
                                }
                                registros = lector.FieldCount; // para salirse del ciclo
                           
                        }
                    }
                }

                if (newquerycolumnas.Length > 0)
                {
                    newquerycolumnas = newquerycolumnas.Substring(0, newquerycolumnas.Length - 1);
                    queryColumnas = newquerycolumnas;
                    queryOpciones[0] = queryColumnas;
                }
                else
                {
                    repite = false;
                }

                CardinalReader.Close();
                if (repite == true) lector.Close();
            } while (repite == true);
               bulk.BatchSize = bulksize;
               bulk.NotifyAfter = 1000;
           

               try
               {

                   //inicia transferencia 
                   while (conexionsql.State == ConnectionState.Closed)
                   {
                       conexionsql.Open();
                       conexioncaida++;
                   }
                   bulkednormal = false;
                   bulk.BulkCopyTimeout = 0;
                   falloBulk = false;
                   bulk.WriteToServer(lector);
                   lector.Close();
                   TerminaTabla = true;
               }
               catch (Exception exc)
               {
                   excmessage = exc.Message.ToString();
                   falloBulk = true;
                   if (exc.Message.ToLower().Contains("timeout"))
                   {
                       timeoutError = true;
                       constraintError = false;
                       times++;
                       //bulkinsert(queryColumnas, queryCondicion);
                       lector.Close();
                   }


                   else
                   {
                       // conexionbulk.Close(); //finaliza la conexion
                       if (exc is SqlException || exc.Message.Contains("constraint"))
                       {
                           lector.Close();
                           timeoutError = false;
                           constraintError = true;
                           //buscaerrorBulk(bulksize,queryColumnas,queryCondicion); //buscamos los errores renglon por renglon

                       }
                       else
                       {
                           lector.Close();
                           timeoutError = false;
                           constraintError = false;
                           //  buscaerrorBulk(0,queryColumnas,queryCondicion);
                       }
                   }
               }


               // si se llego a esta parte del codigo, se finaliza la ejecucion

           }
        private void buscaerrorBulk(int lastbulk, string queryColumnas, string queryCondicion)//falla bulk de 1000, inserta en bulks de 100
           {
               pausar();
               if (bulkednormal)
               {
                   filasTemporales = FilasTabla - lastbulk;
                   FilasTabla = filasTemporales;
                   tablaresulados[recorridoTablas, 0] = (FilasTabla - errores).ToString();  
               }
               string skip = " "; //variable para busqueda ( salta la cantida de filas recorridas)
               if (filasTemporales > 0)
               {
                   skip = " skip " + FilasTabla.ToString();
               }
               FbCommand datosFUente = new FbCommand("select " + skip + " "  +queryColumnas + " from " + NombreTabla + " " + queryCondicion, conexionbulk);
               FbDataReader lector = datosFUente.ExecuteReader();
               SqlBulkCopy bulkerror = new SqlBulkCopy(conexionsqlbulk);
             
            bulkerror.BatchSize = bulkerrorsize;
           

                bulkerror.DestinationTableName = NombreTabla;
                bulkerror.SqlRowsCopied += new SqlRowsCopiedEventHandler(FilasCopiadasError);
                bulkerror.NotifyAfter = 100;
                try
                {

                    //inicia transferencia 
                    while (conexionsql.State == ConnectionState.Closed)
                    {
                        conexionsql.Open();
                        conexioncaida++;
                    }
                    bulkederror = false;
                    bulkerror.BulkCopyTimeout = 0;
                    bulkerror.WriteToServer(lector);
                    lector.Close();

                }
                catch (Exception exc)
                {
                    lector.Close();
                    // conexionbulk.Close(); //finaliza la conexion
                    if (exc is SqlException || exc.Message.Contains("constraint"))
                    {
                        buscaerrorFilas(bulkerrorsize, queryColumnas, queryCondicion); //buscamos los errores renglon por renglon
                    }
                    else
                    {
                        buscaerrorFilas(0, queryColumnas, queryCondicion);
                    }
                }
            }
        bool BuscaErrorFallo;
        private void buscaerrorFilas(int lastbulk, string queryColumnas, string queryCondicion) //balla bulk de 100, recorre cada fila individualmente
           {
               string MO_REFEREN = "";
               string PE_YEAR = "";
               string PE_NUMERO = "";
               string CB_CODIGO = "";
               string CO_NUMERO = "";
               string PE_TIPO = "";
               int posicionMO_REFEREN = 0;
               string[] columnasfechas;
               bool errorfecha = false;
               if (excmessage.Contains("SqlDateTime overflow"))
               {
                   errorfecha = true;
                   SqlConnection conexionfechas = new SqlConnection(cadenadeconexionsql);
                   SqlCommand gettablasfechas = new SqlCommand("SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE DOMAIN_name LIKE 'fecha' AND table_name LIKE '" + NombreTabla + "'", conexionfechas);
                   conexionfechas.Open();
                   columnasfechas = new string[int.Parse(gettablasfechas.ExecuteScalar().ToString())];
                   gettablasfechas.CommandText = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE DOMAIN_name LIKE 'fecha' AND table_name LIKE '" + NombreTabla + "'";
                   SqlDataReader reader = gettablasfechas.ExecuteReader();
                   int recorrido = 0;
                   while (reader.Read())
                   {
                       columnasfechas[recorrido] = reader["column_name"].ToString();
                       recorrido++;
                   }
                   reader.Close();
                   conexionfechas.Close();
               }
               else
               {
                   columnasfechas = new string[0];
               }
               
               if (bulkederror == true)
               {
                   filasTemporales = FilasTabla - lastbulk; //contador de filas siempre va adalantado en 10 a partir de que el bulkinsert inicializa
                   FilasTabla = filasTemporales;
                   tablaresulados[recorridoTablas, 0] = (FilasTabla - errores).ToString();  
               }
               string skip = "";
               if (filasTemporales == 0)
               {

               }
               else
               {
                   skip = "skip " + filasTemporales.ToString();//variable de busqueda 
               }
               FbCommand datosFUente = new FbCommand("select " + skip + " " + queryColumnas + " from " + NombreTabla +" " + queryCondicion , conexionerror);
               FbDataReader lector = datosFUente.ExecuteReader();
               SqlCommand InsercionUnitaria = new SqlCommand();
               InsercionUnitaria.Connection = conexionsql;
               bool salir = false;




               //extrae el nombre de los campos, su respectivo valor y escribe en el query 
               string[,] campos;
               string QueryInsercion;
               int parametername = 0;
               while (lector.Read() && salir == false && detenerMigracion == false)
               {

                   parametername++;
                   pausar();
                 
                   QueryInsercion = "Insert Into " + NombreTabla + "(";
                   FilasTabla++;//por cada fila que el lector lee, se aumenta filas en 1 
                   tablaresulados[recorridoTablas, 0] = (FilasTabla - errores).ToString();
                   filasTemporales = FilasTabla;
                   tablaresulados[recorridoTablas, 0] = FilasTabla.ToString();
                   campos = new string[lector.FieldCount, 2];
                   for (int i = 0; i < lector.FieldCount; i++)
                   {
                       campos[i, 0] = lector.GetName(i);
                       if (NombreTabla == "MOVIMIEN")
                       {
                           if (lector.GetName(i) == "MO_REFEREN")
                           {
                               MO_REFEREN = lector["MO_REFEREN"].ToString();
                               posicionMO_REFEREN = i;
                           }
                           if (lector.GetName(i) == "PE_YEAR")
                           {
                               PE_YEAR = lector["PE_YEAR"].ToString();
                           
                           }
                           if (lector.GetName(i) == "PE_TIPO")
                           {
                               PE_TIPO = lector["PE_TIPO"].ToString();
                              
                           }
                           if (lector.GetName(i) == "PE_NUMERO")
                           {
                               PE_NUMERO = lector["PE_NUMERO"].ToString();
                             
                           }
                           if (lector.GetName(i) == "CB_CODIGO")
                           {
                               CB_CODIGO = lector["CB_CODIGO"].ToString();
                            
                           }
                           if (lector.GetName(i) == "CO_NUMERO")
                           {
                               CO_NUMERO = lector["CO_NUMERO"].ToString();
                            
                           }
                       }
                       bool modificofecha = false;
                       if (errorfecha == true)
                       {

                           if (columnasfechas.Contains(lector.GetName(i)))
                           {
                              
                               
                               try
                               {
                                   if (DateTime.Parse(lector[i].ToString()) < new DateTime(1899, 12, 30))
                                   {
                                       campos[i, 1] = new DateTime(1899, 12, 30).ToShortDateString();
                                       modificofecha = true; ;
                                       salir = true;
                                   }
                               }
                               catch { }
                           }

                       }




                       if (modificofecha == false)
                       {
                           campos[i, 1] = lector[i].ToString();
                       }
                       QueryInsercion += campos[i, 0];

                       if ((i + 1) < lector.FieldCount)
                       {
                           QueryInsercion += ",";
                       }
                   }
                   QueryInsercion = QueryInsercion + ") values(";

                   //escribe los valores en el query
                   for (int i = 0; i < lector.FieldCount; i++)
                   {

                       if (lector[i].GetType() == typeof(int) || lector[i].GetType() == typeof(double) || lector[i].GetType() == typeof(long) || lector[i].GetType() == typeof(short) || lector[i].GetType() == typeof(float))
                       {
                       }
                       else
                       {
                           string domain = lector.GetName(i).ToString().ToUpper();
                           if (lector[i].GetType() == typeof(byte[]) || domain.Contains("BLOB") || domain.Contains("FOTO") || domain.Contains("IMAGEN") || lector[i].GetType() == typeof(DateTime))
                           {
                               if (domain.Contains("BLOB") || domain.Contains("IMAGE") || domain.Contains("FOTO"))
                               {


                                   campos[i, 1] = "@imagen" + parametername.ToString();
                                   parametername++;

                                   System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bin = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                                   MemoryStream stream;
                                   if (lector[i].ToString() != "")
                                   {
                                       stream = new MemoryStream((byte[])lector[i]);


                                       if (stream.Length > 0)
                                       {



                                           InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.Binary).Value = (byte[])lector[i];
                                       }

                                       else
                                       {
                                           InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.Image).Value = DBNull.Value;
                                       }
                                   }
                                   else
                                   {
                                       InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.Image).Value = DBNull.Value;
                                   }



                               }
                               else
                               {
                                 if(  lector[i].GetType() == typeof(DateTime))
                                   {
                                       DateTime fechaInserta;
                                       campos[i, 1] = "@fecha" + parametername.ToString();
                                       parametername++;
                                       try
                                       {
                                           fechaInserta = (DateTime)lector[i];
                                       }
                                       catch
                                       {
                                           fechaInserta = new DateTime(1899, 12, 30);
                                       }
                                       InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.DateTime).Value = fechaInserta;
                                   }
                                   else
                                   {
                                       campos[i, 1] = System.Text.Encoding.Default.GetString((byte[])lector[i]);
                                       campos[i, 1] = "'" + campos[i, 1].Replace("'", "''") + "'";
                                   }
                               
                               }
                           }
                           else
                           {
                               campos[i, 1] = "'" + campos[i, 1].Replace("'", "''") + "'";
                           }

                       }

                       QueryInsercion = QueryInsercion + campos[i, 1];
                       if ((i + 1) < lector.FieldCount)
                       {
                           QueryInsercion += ",";
                       }
                   }
                   QueryInsercion += ") ";

                   try
                   {

                       InsercionUnitaria.CommandText = QueryInsercion;
                       while (conexionsql.State == ConnectionState.Closed)
                       {
                           conexionsql.Open();
                           conexioncaida++;
                       }
                       InsercionUnitaria.ExecuteNonQuery(); //insertamos cada query por separado
                       InsercionUnitaria.Parameters.Clear();
                   }
                   catch (Exception exc)
                   {
                      
                      
                       if (exc.Message.Contains("PRIMARY KEY") && NombreTabla == "MOVIMIEN")
                       {
                           NoAdvertencia++;
                           erroresMovimien = GetMovimienError(PE_YEAR, PE_TIPO, PE_NUMERO, CB_CODIGO, CO_NUMERO, MO_REFEREN);
                           int longitud = ((erroresMovimien.ToString().Length + MO_REFEREN.ToString().Length) - 8);
                           if (longitud > 0)
                           {
                               MO_REFEREN = MO_REFEREN.Remove((8 - longitud));
                           }
                           string CampoDesp = MO_REFEREN + erroresMovimien.ToString();
                           campos[posicionMO_REFEREN, 1] ="'"+ MO_REFEREN + erroresMovimien.ToString()+"'";
                           QueryInsercion = "insert into MOVIMIEN(";
                           for (int i = 0; i < campos.GetLength(0); i++)
                           {
                               QueryInsercion += campos[i, 0] + ",";
                           }
                           QueryInsercion = QueryInsercion.Remove(QueryInsercion.Length-1);
                           QueryInsercion += ") Values (";
                           for (int i = 0; i < campos.GetLength(0); i++)
                           {
                               QueryInsercion += campos[i, 1] + ",";
                           }
                           QueryInsercion = QueryInsercion.Remove(QueryInsercion.Length-1);
                           QueryInsercion += ")";
                           try
                           {
                              
                               InsercionUnitaria.CommandText = QueryInsercion;
                               while (conexionsql.State == ConnectionState.Closed)
                               {
                                   conexionsql.Open();
                                   conexioncaida++;
                               }
                               InsercionUnitaria.ExecuteNonQuery(); //insertamos cada query por separado
                               string detalle = "| PE_YEAR = '"+PE_YEAR+"' |PE_NUMERO = '"+PE_NUMERO+"' | CB_CODIGO = '"+CB_CODIGO+"' | CO_NUMERO = '"+CO_NUMERO+"' |PE_TIPO ='"+PE_TIPO+"' |";



                               string Modificamovimien = (errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), NombreTabla) + "," + NombreTabla + ",Modificado,Referencia Duplicada ; Detalle: " + detalle + " , MO_REFEREN Anterior: " + MO_REFEREN + " -- MO_REFEREN Nuevo :" + CampoDesp + Environment.NewLine);
                           
                             using (StreamWriter outfile = new StreamWriter(rutadetalle, true,System.Text.Encoding.Default))
                             {
                                 outfile.Write(Modificamovimien);
                                 advertencias++;
                                 tablaresulados[recorridoTablas, 2] = advertencias.ToString();
                             }
                            
    
                           }
                           catch 
                           {
                               salir = true; // si hubo error nos salimos del ciclo
                               string filaerror;
                               errores++;
                               errorestotales++;
                               tablaresulados[recorridoTablas, 0] = (FilasTabla - errores).ToString();
                               tablaresulados[recorridoTablas, 1] = errores.ToString();

                               int ciclo;
                               if (lector.FieldCount > 5)
                               {
                                   ciclo = 5;
                               }
                               else
                               {
                                   ciclo = lector.FieldCount;
                               }


                               filaerror = (errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), NombreTabla) + 1).ToString() + "," + NombreTabla + ",Error,";



                               for (int i = 0; i < ciclo; i++)
                               {

                                   filaerror += (campos[i, 0] + "=" + campos[i, 1] + "|").Replace(Environment.NewLine, " ");


                               }
                               filaerror += "," + exc.Message.ToString().Replace(",", "-");
                               bool er = false;
                               do
                               {

                                   er = false;
                                   try
                                   {
                                       outfile2 = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default);

                                       outfile2.Write(filaerror.Replace(Environment.NewLine, " ") + Environment.NewLine);
                                       outfile2.Flush();
                                       outfile2.Close();
                                   }
                                   catch
                                   {
                                       er = true;

                                   }
                               } while (er == true);
                           }
                           
                           salir = true;
                          
                       }
                       else
                       {
                      
                           salir = true; // si hubo error nos salimos del ciclo
                           string filaerror;
                           errores++;
                           errorestotales++;
                           try
                           {
                               tablaresulados[recorridoTablas, 0] = (FilasTabla - errores).ToString();
                           }
                           catch 
                           { }
                           tablaresulados[recorridoTablas, 1] = errores.ToString();

                           int ciclo;
                           if (lector.FieldCount > 5)
                           {
                               ciclo = 5;
                           }
                           else
                           {
                               ciclo = lector.FieldCount;
                           }


                           filaerror = (errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), NombreTabla) + 1).ToString() + "," + NombreTabla + ",Error,";



                           for (int i = 0; i < ciclo; i++)
                           {

                               filaerror += (campos[i, 0] + "=" + campos[i, 1] + "|").Replace(Environment.NewLine, " ");


                           }
                           filaerror += "," + exc.Message.ToString().Replace(",", "-");
                           bool er = false;
                           do
                           {

                               er = false;
                               try
                               {
                                   outfile2 = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default);

                                   outfile2.Write(filaerror.Replace(Environment.NewLine, " ") + Environment.NewLine);
                                   outfile2.Flush();
                                   outfile2.Close();
                               }
                               catch
                               {
                                   er = true;

                               }
                           } while (er == true);

                       }

                   }
               }

               lector.Close();

               if (salir == true)
               {
                   
                   if (errores >= 50)
                   {

                       insercionDirecta(queryColumnas, queryCondicion);
                   }
                   else
                   {
                       BuscaErrorFallo = true;
                       //bulkinsert(queryColumnas, queryCondicion); // una vez localizado el error, continuamos con el bulk insert
                   }
               }
               else
               {
                   BuscaErrorFallo = false;
               }
           }
        private int GetMovimienError(string PE_YEAR,string  PE_TIPO,string PE_NUMERO, string CB_CODIGO,string CO_NUMERO, string MO_REFEREN)
       {
           string[] error = new string[7];
           error[0] = PE_YEAR;
           error[1] = PE_TIPO;
           error[2] = PE_NUMERO;
           error[3] = CB_CODIGO;
           error[4] = CO_NUMERO;
           error[5] = MO_REFEREN;
           bool encontrado=false;
           foreach(string[] erroresGuardados in listaErroresMovimien)
           {
            if(error[0] == erroresGuardados[0])
            {
                if(error[1] == erroresGuardados[1])
                {
                    if(error[2] == erroresGuardados[2])
                    {
                        if(error[3] == erroresGuardados[3])
                        {
                            if(error[4] == erroresGuardados[4])
                            {
                                if(error[5].ToUpper() == erroresGuardados[5].ToUpper())
                                {
                                    erroresGuardados[6] = (int.Parse(erroresGuardados[6].ToString()) + 1).ToString();
                                    error[6] = erroresGuardados[6];
                                    encontrado=true;
                                }
                            }
                        }
                    }
                }
            }

           }
           if(encontrado==false)
           {
               error[6]="1";
               listaErroresMovimien.Add(error);
               
           }
           return int.Parse(error[6]);
        }
        private void FilasCopiadas(object sender, SqlRowsCopiedEventArgs e)
           {
               
               bulkednormal = true;
               // SqlDataReader reader = new SqlDataReader();
               FilasTabla = e.RowsCopied + filasTemporales;//aumenta la cantidad de filas cuando se cumple un batch  
               tablaresulados[recorridoTablas, 0] = FilasTabla.ToString();
               if (detenerMigracion)
               {
                   e.Abort = true;
               }




           }
        private void FilasCopiadasError(object sender, SqlRowsCopiedEventArgs e)
           {
               bulkederror = true;
               FilasTabla = e.RowsCopied + filasTemporales;//aumenta la cantidad de filas cuando se cumple un batch
               tablaresulados[recorridoTablas, 0] = FilasTabla.ToString();
               if (detenerMigracion)
               {
                   e.Abort = true;
               }



           }        
        private void insercionDirecta(string queryColumnas, string queryCondicion) //insercion separada, va directamente e inserta de una por una
        {
            try
            {
                advertencias = 0;
                textos = new List<string>();
                numeros = new List<int>();
                string campoAntes;
                string skip = "skip " + FilasTabla.ToString();//variable de busqueda 
                FbCommand datosFUente = new FbCommand("select " + skip + " " + queryColumnas + " from " + NombreTabla + " " + queryCondicion, conexionerror);
                FbDataReader lector = datosFUente.ExecuteReader();
                SqlCommand InsercionUnitaria = new SqlCommand();
                InsercionUnitaria.Connection = conexionsql;

                string[,] campos = new string[lector.FieldCount, 2];
                string QueryInsercion;
                int index = 0; ;
                int parametername;
                while (lector.Read() && detenerMigracion == false)
                {
                    parametername = 0;
                    InsercionUnitaria.Parameters.Clear();
                    pausar();

                    QueryInsercion = "Insert Into " + NombreTabla + "(";
                    FilasTabla++;
                    tablaresulados[recorridoTablas, 0] = FilasTabla.ToString();
                    filasTemporales = FilasTabla;
                    

                    for (int i = 0; i < lector.FieldCount; i++)
                    {
                        
                         
                            campos[i, 0] = lector.GetName(i);
                            campos[i, 1] = lector[i].ToString();
                            if (!(NombreTabla == "CTA_MOVS" && (campos[i, 0].Trim() == "CM_DEPOSIT" || campos[i, 0].Trim() == "CM_RETIRO")))
                            {
                                QueryInsercion += campos[i, 0];
                                if (NombreTabla == "CTA_MOVS")
                                {
                                    if ((i + 3) < lector.FieldCount)
                                    {
                                        QueryInsercion += ",";
                                    }
                                }
                                else
                                {
                                    if ((i + 1) < lector.FieldCount)
                                    {
                                        QueryInsercion += ",";
                                    }
                                }
                            }
                            
                        }
                    
                    QueryInsercion = QueryInsercion + ") values(";

                    //escribe los valores en el query
                    string keys = "";
                    for (int i = 0; i < lector.FieldCount; i++)
                    {
                        index = 0;
                        cambiosreporte = "";
                        if ((NombreTabla.Trim() == "REPORTE" && i == 1) || (NombreTabla.Trim() == "PRESTAMO" && i == 6))
                        {
                            if ((NombreTabla.Trim() == "REPORTE"))
                            {
                                index = indice(campos[i, 1]);
                            }
                            if ((NombreTabla.Trim() == "PRESTAMO"))
                            {

                                index = indice(lector["CB_CODIGO"].ToString() + lector["PR_REFEREN"].ToString() + lector["PR_TIPO"].ToString());
                                keys = "CB_codigo=" + lector["cb_codigo"].ToString() + " and pr_tipo like '%" + lector["PR_TIPO"].ToString() + "%'";
                            }
                            if (index > 0)
                            {
                                NoAdvertencia++;
                                reportesagregados++;
                                campoAntes = campos[i, 1];
                                campos[i, 1] = GenerarCampo(index, campos[i, 1], keys);
                                string errorstring = "";
                                if ((NombreTabla.Trim() == "PRESTAMO"))
                                {
                                    errorstring = "; Numero de empleado = " + lector["cb_codigo"].ToString() + "; Tipo de Préstamos = " + lector["PR_TIPO"].ToString();

                                }
                                cambiosreporte += (errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), NombreTabla) + "," + NombreTabla + ",Modificado,Registro con nombre duplicado , Nombre Anterior: " + campoAntes + " -- Nombre Nuevo :" + campos[i, 1] + errorstring + Environment.NewLine);
                                if (NombreTabla.Trim() == "PRESTAMO")
                                {
                                    actualizaMovimienen(lector["CB_CODIGO"].ToString(), campoAntes, campos[i, 1]);

                                }


                                using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))
                                {
                                    outfile.Write(cambiosreporte);
                                    advertencias++;
                                    tablaresulados[recorridoTablas, 2] = advertencias.ToString();
                                }
                                if (NombreTabla == "REPORTE")
                                {
                                    index = indice(campos[i, 1]);
                                }
                                else
                                {
                                    index = indice(lector["CB_CODIGO"].ToString() + lector["PR_REFEREN"].ToString() + lector["PR_TIPO"].ToString());
                                }
                            }
                        }
                        if (lector[i].GetType() == typeof(int) || lector[i].GetType() == typeof(double) || lector[i].GetType() == typeof(long) || lector[i].GetType() == typeof(short) || lector[i].GetType() == typeof(float))
                        {
                        }
                        else
                        {
                            string domain = lector.GetName(i).ToString().ToUpper();
                            if (lector[i].GetType() == typeof(byte[]) || domain.Contains("BLOB") || domain.Contains("FOTO") || domain.Contains("IMAGEN") || lector[i].GetType() == typeof(DateTime))
                            {
                                if (domain.Contains("BLOB") || domain.Contains("IMAGE") || domain.Contains("FOTO"))
                                {


                                    campos[i, 1] = "@imagen" + parametername.ToString();
                                    parametername++;

                                    System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bin = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                                    MemoryStream stream;
                                    if (lector[i].ToString() != "")
                                    {
                                        stream = new MemoryStream((byte[])lector[i]);
                                        if (stream.Length > 0)
                                        {
                                            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(stream);
                                            bmp.Save(stream = new MemoryStream(), System.Drawing.Imaging.ImageFormat.Bmp);
                                            InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.VarChar).Value = System.Text.Encoding.Default.GetString((byte[])lector[i]);
                                        }
                                        else
                                        {
                                            InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.Image).Value = DBNull.Value;
                                        }
                                    }
                                    else
                                    {
                                        InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.Image).Value = DBNull.Value;
                                    }





                                }
                                else
                                {
                                    DateTime fechaInserta;
                                   
                                   if (lector[i].GetType() == typeof(DateTime))
                                    {
                                        
                                        campos[i, 1] = "@fecha" + parametername.ToString();
                                        parametername++;
                                        try
                                        {
                                             fechaInserta = (DateTime)lector[i];
                                        }
                                        catch
                                        {
                                            fechaInserta = new DateTime(1899, 12, 30);
                                        }
                                        InsercionUnitaria.Parameters.Add(campos[i, 1], SqlDbType.DateTime).Value = fechaInserta;
                                    }
                                    else
                                    {
                                        campos[i, 1] = System.Text.Encoding.Default.GetString((byte[])lector[i]);
                                        campos[i, 1] = "'" + campos[i, 1].Replace("'", "''") + "'";
                                    }
                                }
                            }
                            else
                            {
                                campos[i, 1] = "'" + campos[i, 1].Replace("'", "''") + "'";
                            }



                        }
                        if (!(NombreTabla == "CTA_MOVS" && (campos[i, 0].Trim() == "CM_DEPOSIT" || campos[i, 0].Trim() == "CM_RETIRO")))
                        {
                            QueryInsercion = QueryInsercion + campos[i, 1];
                            if (NombreTabla == "CTA_MOVS")
                            {
                                if ((i + 3) < lector.FieldCount)
                                {
                                    QueryInsercion += ",";
                                }
                            }
                            else
                            {
                                if ((i + 1) < lector.FieldCount)
                                {
                                    QueryInsercion += ",";
                                }
                            }
                        }
                    }
                    QueryInsercion += ") ";

                    try
                    {

                        InsercionUnitaria.CommandText = QueryInsercion;
                        while (conexionsql.State == ConnectionState.Closed)
                        {
                            conexionsql.Open();
                            conexioncaida++;
                        }
                        InsercionUnitaria.ExecuteNonQuery(); //insertamos cada query por separado

                    }
                    catch (Exception exc)
                    {
                        string filaerror;
                        errores++;
                        errorestotales++;
                        tablaresulados[recorridoTablas, 0] = (FilasTabla - errores).ToString();
                        tablaresulados[recorridoTablas, 1] = errores.ToString();

                        int ciclo;
                        if (lector.FieldCount > 5)
                        {
                            ciclo = 5;
                        }
                        else
                        {
                            ciclo = lector.FieldCount;
                        }


                        filaerror = (errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), NombreTabla) + 1).ToString() + "," + NombreTabla + ",Error,";



                        for (int i = 0; i < ciclo; i++)
                        {
                            //extraer los datos del reader para introducirlos en el reporte

                            filaerror += (campos[i, 0] + "=" + campos[i, 1] + "|").Replace(Environment.NewLine, " ");


                        }
                        filaerror += "," + exc.Message.ToString().Replace(",", "-");
                        using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))
                        {

                            outfile.Write(filaerror.Replace(Environment.NewLine, " ") + Environment.NewLine);


                        }




                    }

                    if (NombreTabla == "REPORTE")
                    {
                        if (lector["RE_REPORTE"].ToString().Contains(":") || lector["RE_REPORTE"].ToString().Contains("\\\\"))
                        {

                            NoAdvertencia++;
                            string filaerror = (errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), NombreTabla) + 1).ToString() + "," + NombreTabla + ",Advertencia,";



                            for (int i = 0; i < 5; i++)
                            {
                                //extraer los datos del reader para introducirlos en el reporte

                                filaerror += (campos[i, 0] + "=" + campos[i, 1] + "|").Replace(Environment.NewLine, " ");


                            }
                            filaerror += ", Este Reporte Contiene una Ruta Especifica hacia " + lector["RE_REPORTE"].ToString();
                            using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))
                            {

                                outfile.Write(filaerror.Replace(Environment.NewLine, " ") + Environment.NewLine);
                                advertencias++;
                                tablaresulados[recorridoTablas, 2] = advertencias.ToString();
                            }


                        }
                    }


                }

                tablaresulados[recorridoTablas, 2] = advertencias.ToString();
                lector.Close();
            }
            catch(Exception e) 
            {
                MessageBox.Show(e.ToString());
            }
           }
        public string[] TablasMigracion()
        {
            string[] array = DiccionarioMigracion.elemento.Keys.ToArray<string>();

            return array;
        
        }
        public void pausar()//ciclo pausa
        {

            while (pausa == true)
            {
                pausado = true;
            
            }
            pausado = false;
        }
        private void borrar(string nombretabla)
        {

            SqlCommand borrarcommand = new SqlCommand(@"WHILE(SELECT COUNT(*)  FROM "+nombretabla+@"  )  >0
                                                        BEGIN
                                                        DELETE TOP(1000) FROM "+nombretabla+ @" 
                                                        END"  , conexionsql);
            borrarcommand.CommandTimeout = 0;
            borrarcommand.ExecuteNonQuery();

            
        }
        private int indice(string dato)
        {
            bool textorepetido = false;
            int index=0;
            for (int i = 0; i < textos.Count; i++)
            {
                if (dato.Trim().ToLower() == textos[i].ToLower())
                {
                    numeros[i]++;
                    textorepetido = true;
                    index=numeros[i];
                }
            
            }
            if (textorepetido == true)
            {
                return index;
            }
            else
            {
                textos.Add(dato);
                numeros.Add(0);
                return 0;
            }

            
        }
        private string GenerarCampo(int indice, string dato, string keys)
        {
            FbConnection conexionfb = new FbConnection();
            conexionfb.ConnectionString = conexionerror.ConnectionString;
            conexionfb.Open();
            int length = 0;
            int resta = 0;

            if (NombreTabla.Trim() == "REPORTE")
            {
                length = 30;
                resta = 3;
            }
            if (NombreTabla.Trim() == "PRESTAMO")
            {
                length = 8;
            }
            if ((dato.Length + indice.ToString().Length + resta) > length)
            { 
                dato = dato.Substring(0,(length -(resta+(dato.Length.ToString().Length))));
            }
            int existe = 0;
            do
            {
                if (NombreTabla == "PRESTAMO")
                {
                    FbCommand query = new FbCommand("select count (*) from prestamo where upper(pr_referen) like '" + dato.ToUpper() + "" + indice.ToString() + "' and " + keys, conexionfb);
                
                    existe = int.Parse(query.ExecuteScalar().ToString());
                    
                 
                }
                if (NombreTabla == "REPORTE")
                {
                    FbCommand query = new FbCommand("select count (*) from REPORTE where upper(RE_NOMBRE) like '" + dato.ToUpper() + "" + indice.ToString() + "'",conexionfb);
                    
                    existe = int.Parse(query.ExecuteScalar().ToString());
                }
                if(existe>0)
                {
                indice++;
                
                }
            }
            while (existe > 0);
            conexionfb.Close();
            
            return dato + ""+indice.ToString()+"";
        } 
        private void ProcesosMigracion(string NombreProceso)
        {
             advertencias = 0;  
            if (NombreProceso == "TipoKardex")
            {
                int tipocardexagergados=0;
                int tipocardexerrores = 0;
                string insert;
                SqlCommand tkardexinsert = new SqlCommand();
                tkardexinsert.Connection = conexionsql;
           
           
                FbCommand arreglaTkardex = new FbCommand("select distinct(cb_tipo) from kardex where CB_tipo not in (select tb_Codigo from tKARDEX)", conexionerror);
                while (arreglaTkardex.Connection.State == ConnectionState.Closed)
                {
                    arreglaTkardex.Connection.Open();
                }
                FbDataReader readerkardex = arreglaTkardex.ExecuteReader();
                while (readerkardex.Read())
                {
                    insert = " insert into tkardex values('" + readerkardex["CB_TIPO"].ToString() + "','Recuperado en Transferencia','',0,'',1,'N')";
                    tkardexinsert.CommandText = insert;
                    while (tkardexinsert.Connection.State == ConnectionState.Closed)
                    {
                        tkardexinsert.Connection.Open();
                        
                    }

                       
                    try
                    {
                        NoAdvertencia++;
                        tkardexinsert.ExecuteNonQuery();
                        advertencias++;
                        tablaresulados[recorridoTablas, 2] = advertencias.ToString();
                        using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))//inicia detalle de bitacora
                        {
                            outfile.Write((errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), "TKARDEX") + 1).ToString() + ",Tkardex, Preparacion ," + readerkardex["CB_TIPO"] + " Agregado , Agregando Tipos de Kardex Faltantes" + Environment.NewLine);
                        }
                       
                        tipocardexagergados++;
                        tablaresulados[recorridoTablas, 0] = tipocardexagergados.ToString();
                    }
                    catch (Exception exc)
                    {
                        using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))//inicia detalle de bitacora
                        {
                            outfile.Write((errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), "TKARDEX") + 1).ToString() + ",Tkardex, Error Preparacion ," + "No se Logro agregar a TKardex :"+ readerkardex["CB_TIPO"] +","+ exc.Message.Replace(Environment.NewLine," ").Replace(",","-")+Environment.NewLine);
                               
                        }
                      
                        tipocardexerrores++;
                        tablaresulados[recorridoTablas, 1] = tipocardexerrores.ToString();
                    }

                }
                
            }

            if (NombreProceso == "TipoPrestamo")
            {
                int tipocardexagergados = 0;
                int tipocardexerrores = 0;
                string insert;
                SqlCommand tPrestaInsert = new SqlCommand();
                tPrestaInsert.Connection = conexionsql;


                FbCommand arreglaPresta = new FbCommand("select distinct(PR_TIPO) from prestamo where PR_TIPO not in (select TB_CODIGO from tPRESTA)", conexionerror);
                while (arreglaPresta.Connection.State == ConnectionState.Closed)
                {
                    arreglaPresta.Connection.Open();
                }
                FbDataReader readerPresta = arreglaPresta.ExecuteReader();
                while (readerPresta.Read())
                {
                    insert = " insert into TPRESTA values('" + readerPresta["PR_TIPO"].ToString() + "','Recuperado en Transferencia','',0,'','','','','','',0,0,0,0,1)";
                    tPrestaInsert.CommandText = insert;
                    while (tPrestaInsert.Connection.State == ConnectionState.Closed)
                    {
                        tPrestaInsert.Connection.Open();

                    }


                    try
                    {
                        NoAdvertencia++;
                        tPrestaInsert.ExecuteNonQuery();
                        advertencias++;
                        tablaresulados[recorridoTablas, 2] = advertencias.ToString();
                        using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))//inicia detalle de bitacora
                        {
                            outfile.Write((errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), "TPRESTA") + 1).ToString() + ",TPRESTA, Preparacion ," + readerPresta["PR_TIPO"] + " Agregado , Agregando Tipos de Prestamo Faltantes" + Environment.NewLine);
                        
                        }
                        tpresta = true;
                        tipocardexagergados++;
                        tablaresulados[recorridoTablas, 0] = tipocardexagergados.ToString();
                    }
                    catch (Exception exc)
                    {
                        using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))//inicia detalle de bitacora
                        {
                         
                            outfile.Write((errorestotales + NoAdvertencia).ToString() + "," + (Array.IndexOf(TablasMigracion(), "TPRESTA") + 1).ToString() + ",TPRESTA, Error Preparacion ," + "No se Logro agregar a TPRESTA :" + readerPresta["PR_TIPO"]+","+ exc.Message.Replace(Environment.NewLine, " ").Replace(",", "-") + Environment.NewLine);
                            
                        }

                        tipocardexerrores++;
                        tablaresulados[recorridoTablas, 1] = tipocardexerrores.ToString();
                    }

                }

            }


            if (NombreProceso=="Clasificaciones de reportes")
            {
                bool huboerrores=false;
                LlenaDerechosDeAcceso();
                SqlCommand queryDerechos = new SqlCommand();

                string dbo = ".dbo";
                string querydbo = "select * from " + Program.NombreComparte + dbo + ".Company";
                SqlConnection masterconexion = new SqlConnection(Program.ConexionMaster);
                SqlCommand MasterQuery = new SqlCommand(querydbo, masterconexion);
                MasterQuery.Connection.Open();
                try
                {
                    string value = MasterQuery.ExecuteScalar().ToString();
                }
                catch
                {
                    dbo = "";
                }
                queryDerechos.Connection = masterconexion;
                if (queryDerechos.Connection.State == ConnectionState.Closed) 
                {
                    queryDerechos.Connection.Open();
                }
                string GetCo_codigos = "select CM_CODIGO from " + Program.NombreComparte + dbo + ".company where DB_CODIGO  like '%" + Program.Compañia  + "'";
                SqlConnection CodigosGrupoConection = new SqlConnection(Program.ConexionMaster);
                SqlConnection CodigosConection = new SqlConnection(Program.ConexionMaster);
                SqlConnection EntidadesConection = new SqlConnection(Program.ConexionMaster);
                SqlConnection InsercionAccEntConection = new SqlConnection(Program.ConexionMaster);
                InsercionAccEntConection.Open();
                SqlCommand InsercionAccEntCMD;
                CodigosConection.Open();
                SqlCommand CO_codigoQUery = new SqlCommand(GetCo_codigos, CodigosConection);
                SqlDataReader lectorCodigos = CO_codigoQUery.ExecuteReader();
               
                //durante el ciclo de los Codigos en company por cada base de dato se insertaran los derechos de acceso de clasificaciones y entidades de reportes
                while (lectorCodigos.Read())
                {
                    for (int i = 0; i < derechos.GetLength(0); i++)
                    {

                 
                        string[] derechosSQL = Derechossql(i);
                       
                        //ciclo para insertar derechos de clasificaciones
                        for (int j = 0; j < derechosSQL.Length; j++)
                        {
                            string query = "insert into " + Program.nombrebaseDatos + dbo + ".R_CLAS_ACC(CM_CODIGO,GR_CODIGO,RC_CODIGO,RA_DERECHO,RA_VERSION,US_CODIGO) select CM_CODIGO,GR_CODIGO," + derechosSQL[j] + ",AX_DERECHO,0,0 from " + Program.NombreComparte + dbo + ".ACCESO where AX_NUMERO = " + derechos[i, 0] + " and CM_CODIGO='" +lectorCodigos["CM_CODIGO"].ToString()+ "'";
                            queryDerechos.CommandText = query;
                            try
                            {

                                queryDerechos.ExecuteNonQuery();


                            }
                            catch
                            {
                                huboerrores = true;
                            }
                        }
                   

                    
                    }
                    string getGr_Codigos = "select GR_CODIGO from " + Program.NombreComparte + dbo + ".GRUPO ";
                    SqlCommand GR_codigoQUery = new SqlCommand(getGr_Codigos, CodigosGrupoConection);
                    CodigosGrupoConection.Open();
                    SqlDataReader lectorCodigosGrupo = GR_codigoQUery.ExecuteReader();
                    //ciclo para recorrer codigos de grupos y de entidades donde se insertaran las entidades para TODOS los grupos
                    while (lectorCodigosGrupo.Read())
                    {
                        string selectEntACC = "Select EN_CODIGO from "+Program.nombrebaseDatos + dbo +".R_ENTIDAD";
                        SqlCommand CMDReaderEntidad = new SqlCommand(selectEntACC, EntidadesConection);
                        EntidadesConection.Open();
                        SqlDataReader lectorCodigosEntidad = CMDReaderEntidad.ExecuteReader();
                        while (lectorCodigosEntidad.Read())
                        {
                            string insertENTACC = "insert into " + Program.nombrebaseDatos + dbo + ".R_ENT_ACC(CM_CODIGO,GR_CODIGO,EN_CODIGO,RE_DERECHO,RE_VERSION,US_CODIGO) values('" + lectorCodigos["CM_CODIGO"].ToString() + "','" + lectorCodigosGrupo["GR_CODIGO"].ToString() + "','" + lectorCodigosEntidad["EN_CODIGO"].ToString() + "',1,0,0)";
                            InsercionAccEntCMD = new SqlCommand(insertENTACC, InsercionAccEntConection);

                            try
                            {
                                InsercionAccEntCMD.ExecuteNonQuery();
                            }
                            catch 
                            {
                                huboerrores = true;
                            }
                        }
                       
                        EntidadesConection.Close();
                        
                    }
                    
                    CodigosGrupoConection.Close();
                }
                InsercionAccEntConection.Close();
                lectorCodigos.Close();
                CodigosConection.Close();
                InsercionAccEntConection.Close();
                if (queryDerechos.Connection.State == ConnectionState.Closed)
                {
                    queryDerechos.Connection.Close();
                }
                if (!huboerrores)
                {
                    using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))//inicia detalle de bitacora
                    {
                        outfile.Write((errorestotales + NoAdvertencia+1).ToString() + "," + (TablasMigracion().Length + 1).ToString() + ",Acceso a clasificaciones de reportes, Transferencia de accesos a clasificaciones de reportes, Accesos a clasificaciones de reportes Transferidos , Se Transfirieron exitosamente" + Environment.NewLine);
                    }

                }
                else
                {
                    using (StreamWriter outfile = new StreamWriter(rutadetalle, true, System.Text.Encoding.Default))//inicia detalle de bitacora
                    {
                        outfile.Write((errorestotales + NoAdvertencia + 1).ToString() + "," + (TablasMigracion().Length + 1).ToString() + ",Acceso a clasificaciones de reportes, Transferencia de accesos a clasificaciones de reportes, Accesos a clasificaciones de reportes Transferidos , Errores durante la Transferencia de derechos de acceso" + Environment.NewLine);
                    }
                }
            }





           
        }
        private void MigracionDeTablas(string NombreTabla)
        {
                
            bool tablaespecial = false;
            bool tablaexiste; 
            string queryCondicion = DiccionarioMigracion.elemento[NombreTabla].Condiciones;
            string columnas = DiccionarioMigracion.elemento[NombreTabla].Columnas;
            double porcentage = 0;
            if (NombreTabla.Trim() == "REPORTE")
            {
                tablaespecial = true; //REPORTE es una tabla especial
            }
          
            if (NombreTabla.Trim() == "PRESTAMO")
            {
                tablaespecial = true; //REPORTE es una tabla especial
            }
            
            if (NombreTabla.Trim() == "PLAZA")
            {
                tablaespecial = true; //Plaza se migrara individualmente, por proposito de Identity_Insert
            }
            if (NombreTabla.Trim() == "GR_AD_ACC")
            {
                tablaespecial = true; //Plaza se migrara individualmente, por proposito de Identity_Insert
            }
            if (NombreTabla.Trim() == "CTA_MOVS")
            {
                tablaespecial = true; //Plaza se migrara individualmente, por proposito de Identity_Insert
            }
            if(NombreTabla == "KARDEX")
            {
                tablaespecial = true;
            }
            if (NombreTabla == "TPERIODO")
            {
            //    tablaespecial = true;
            }
            if (NombreTabla == "TMPNOM")
            {
            //    tablaespecial = true;
            }
         /*   if (NombreTabla == "KARINF")
            {
             //   tablaespecial = true;
            }*/
            FbCommand filascommand = new FbCommand("Select count(*) from rdb$relations where upper(rdb$relation_name) like '%" + NombreTabla.ToUpper() + "%' " , conexionbulk);
            if(int.Parse(filascommand.ExecuteScalar().ToString())>0)
            {
                FilasMigrar = int.Parse(DiccionarioMigracion.elemento[NombreTabla].fbrows);
                tablaexiste = true;
            }
            else
            {
                tablaexiste = false; //puede que la tabla a buscar no existe en la version actual de TRESS en FB
            }
            if (tablaexiste == true) //Solo ejecutar si la tabla existe dentro de la base de datos de FB
            {

                    
                if (tablaespecial == true) // si es tabla especial pasamos a insersion directa
                {
                    if (NombreTabla == "PLAZA" || NombreTabla == "GR_AD_ACC" || NombreTabla == "CTA_MOVS") // tablas con tipos de datos identity para insertar
                    {
                        SqlCommand ActivarIdentityInsertCommand = new SqlCommand("Set Identity_Insert "+NombreTabla+" On", conexionsql);
                        if (conexionsql.State == ConnectionState.Closed)
                        {
                            conexionsql.Open();

                        }

                        ActivarIdentityInsertCommand.ExecuteNonQuery();
                        
                    }
                    insercionDirecta(columnas, queryCondicion);
                    if (NombreTabla == "PLAZA" || NombreTabla == "GR_AD_ACC" || NombreTabla == "CTA_MOVS") //desactiva insercion de las tablas identity
                    {
                        SqlCommand DesactivarIdentityInsertCommand = new SqlCommand("Set Identity_Insert " + NombreTabla + " Off", conexionsql);
                        if (conexionsql.State == ConnectionState.Closed)
                        {
                             conexionsql.Open();
                        }                            
                        DesactivarIdentityInsertCommand.ExecuteNonQuery();
                        
                    }
                      
                }
                else// si no, pasamos a insersion por bulk
                {
                    TerminaTabla = false;
                    queryOpciones[0] = columnas;
                    queryOpciones[1] = queryCondicion;
                    while (TerminaTabla == false && detenerMigracion == false )
                    {
                        bulkinsert(queryOpciones[0], queryOpciones[1]);
                        if (falloBulk)
                        {
                            if (timeoutError)
                            {
                                bulkinsert(queryOpciones[0], queryOpciones[1]);
                            }
                            else
                            {
                                if (constraintError)
                                {
                                    buscaerrorBulk(bulksize, queryOpciones[0], queryOpciones[1]);
                                }
                                else
                                {
                                    buscaerrorBulk(0, queryOpciones[0], queryOpciones[1]);
                                }
                            }
                         }
                        
                        //if(BuscaErrorFallo)
                           // bulkinsert(queryOpciones[0], queryOpciones[1]); ; // una vez localizado el error, continuamos con el bulk insert

                           // buscaerrorFilas();
                    }
                }
                using (StreamWriter outfile = new StreamWriter(rutabitacora, true)) // se guardan en la bitacora los resultados 
                {
                    if (FilasMigrar > 0)
                    {
                        porcentage = Math.Round((((double)FilasMigrar - errores) / FilasMigrar) * 100, 2);
                        outfile.Write(  NombreTabla + "," + porcentage.ToString() + "%, " + (FilasMigrar - errores).ToString() + " , " + FilasMigrar.ToString() + " " + Environment.NewLine);
                        tablaresulados[recorridoTablas, 0] = (FilasMigrar - errores).ToString();  
                    }
                    else
                    {
                        outfile.Write(NombreTabla + ",100%,0,0" + Environment.NewLine);
                    }
                }
            }
            else
            {
                using (StreamWriter outfile = new StreamWriter(rutabitacora, true)) // se guardan en la bitacora los resultados 
                {
                    outfile.Write(NombreTabla + ",100%,0,0" + Environment.NewLine);
                }
            }
            if (Program.MigrarComparte == true)
            {
                if(NombreTabla == "CLAVES")
                { AjustaClave(Program.VersionCompartesql); }
            }
            else
            {
                if (NombreTabla == "GLOBAL")
                { AjustaGlobal(Program.VersionCompartesql); }
            }
            tablaespecial = false; // se reinician campos 
            queryCondicion = "";
        }
       
        private void QueryMigracion(string NombreTabla)
        {
            try
            {
                SqlCommand query = new SqlCommand();
                query.Connection = conexionsql;
                query.CommandText = DiccionarioMigracion.elemento[NombreTabla].query;
                query.ExecuteNonQuery();
            }    
            catch
            {
                using (StreamWriter outfile = new StreamWriter(rutadetalle, false))
                {
                    outfile.Write(errorestotales.ToString()+advertencias.ToString()+","+"No Tabla"+",Nombre Tabla"+",Tipo"+",Detalle"+",Mensaje"+"," + Environment.NewLine);
                }
            }
            }
        public void actualizaMovimienen(string cb_usuario,string PR_REFEREN_old,string pr_referen_new)
        {

            SqlCommand query = new SqlCommand();
            query.Connection = conexionsql;
            query.CommandText = "update movimien set MO_REFEREN  ='" + pr_referen_new + "' where CB_CODIGO =" + cb_usuario + " and MO_REFEREN COLLATE Latin1_General_CS_AS = '" + PR_REFEREN_old + "'";
            query.CommandTimeout = 0;
            query.ExecuteNonQuery();

        }
        public void RequestStop()
        {
           
        }
        public string[] Derechossql(int indice)
        {
            string listaderechos = derechos[indice,1];

            return listaderechos.Split(',');
        }
        public void LlenaDerechosDeAcceso()
       {
           
           derechos[0, 0] = "217";
           derechos[1, 0] = "275";
           derechos[2, 0] = "218";
           derechos[3, 0] = "219";
           derechos[4, 0] = "220";
           derechos[5, 0] = "221";
           derechos[6, 0] = "222";
           derechos[7, 0] = "223";
           derechos[8, 0] = "247";
           derechos[9, 0] = "424";
           derechos[10, 0] = "423";
           derechos[11, 0] = "274";
           derechos[12, 0] = "248";
           derechos[13, 0] = "321";
           derechos[14, 0] = "391";
           derechos[15, 0] = "461";
           derechos[16, 0] = "276";
           derechos[17, 0] = "518";
           derechos[18, 0] = "635";
           derechos[0, 1] = "0,23,27,20";
           derechos[1, 1] = "10";
           derechos[2, 1] = "1";
           derechos[3, 1] = "2,13,19,24,25";
           derechos[4, 1] = "3";
           derechos[5, 1] = "4,21,22";
           derechos[6, 1] = "5,26";
           derechos[7, 1] = "6,999";
           derechos[8, 1] = "8";
           derechos[9, 1] = "15";
           derechos[10, 1] = "16";
           derechos[11, 1] = "9";
           derechos[12, 1] = "7";
           derechos[13, 1] = "12";
           derechos[14, 1] = "14";
           derechos[15, 1] = "17";
           derechos[16, 1] = "11";
           derechos[17, 1] = "18";
           derechos[18, 1] = "28";


     
      
       
       }
        private void AjustaGlobal(string VersionCompartesql)
        {
            SqlConnection ConexionCompany = new SqlConnection(cadenadeconexionsql);
            ConexionCompany.Open();
            string query = "Update Global set GL_FORMULA = " + VersionCompartesql + " where GL_CODIGO = 133";
            SqlCommand cmdAjustarDatos = new SqlCommand(query, ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
        }
        private void AjustaClave(string VersionCompartesql)
        {
            SqlConnection ConexionCompany = new SqlConnection(cadenadeconexionsql);
            ConexionCompany.Open();
            string query = "Update Claves set CL_TEXTO = '" + VersionCompartesql + "' where CL_NUMERO = '100'";

            SqlCommand cmdAjustarDatos = new SqlCommand(query, ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch (Exception ex)
            {

            }
        }
        public void AjustaCompany()//Metodo Final para ajustar Comparte
        {
            SqlConnection ConexionCompany = new SqlConnection(cadenadeconexionsql);
            ConexionCompany.Open();
            SqlCommand cmdAjustarDatos;
            //Patch para migrar de 2013->2014
            string QueryPatch = @"create procedure REGISTROS_DB_INFO
                                as
                                begin
	                            SET NOCOUNT ON;
	                            /* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
	                            /* 1. Borrar objeto FK_DB_INFO_DB_CODIGO */
	                            IF exists (SELECT * FROM dbo.sysobjects where id = object_id(N'[FK_DB_INFO_DB_CODIGO]') AND xtype in (N'F'))
		                            ALTER TABLE COMPANY DROP CONSTRAINT FK_DB_INFO_DB_CODIGO

	                            /* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
	                            /* 2. Eliminar registros en DB_INFO */
	
	                            if (SELECT COUNT(DB_CODIGO) FROM DB_INFO) = 0 
	                            BEGIN
	
		                            /* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
		                            /* 3. Programa para insertar registros en DB_INFO. Uno por BD (COMPANY.CM_DATOS) */	

		                            DECLARE @CM_DATOS ACCESOS;

		                            DECLARE DB_INFO_CURSOR CURSOR FOR
		                            SELECT DISTINCT CM_DATOS FROM COMPANY

		                            OPEN DB_INFO_CURSOR

		                            FETCH NEXT FROM DB_INFO_CURSOR INTO @CM_DATOS

		                            WHILE @@FETCH_STATUS = 0
		                            BEGIN
			                            INSERT INTO DB_INFO (DB_CODIGO, DB_DESCRIP, DB_CONTROL, DB_TIPO, DB_DATOS, DB_USRNAME, DB_PASSWRD, DB_CTRL_RL, DB_CHKSUM)
			                            SELECT TOP 1 COMPANY.CM_CODIGO, COMPANY.CM_NOMBRE, COMPANY.CM_CONTROL,
			                            CASE
				                            WHEN CM_CTRL_RL = '3DATOS' THEN 0
				                            WHEN CM_CTRL_RL = '3RECLUTA' THEN 1
				                            WHEN CM_CTRL_RL = '3VISITA' THEN 2
				                            WHEN CM_CTRL_RL = '3OTRO' THEN 3
				                            WHEN CM_CTRL_RL = '3PRUEBA' THEN 4
				                            WHEN CM_CTRL_RL = '3PRESUP' THEN 5
				                            ELSE 0
			                            END, COMPANY.CM_DATOS, COMPANY.CM_USRNAME, COMPANY.CM_PASSWRD,
			                            COMPANY.CM_CTRL_RL, COMPANY.CM_CHKSUM FROM COMPANY
			                            WHERE CM_DATOS = @CM_DATOS
			                            ORDER BY CM_NIVEL0, CM_CODIGO;

		                              FETCH NEXT FROM DB_INFO_CURSOR INTO @CM_DATOS
		                            END

		                            CLOSE DB_INFO_CURSOR;

		                            DEALLOCATE DB_INFO_CURSOR;

		                            /* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
		                            /* 4. Actualizar tabla COMPANY con los Códigos que le corresponden de la tabla DB_INFO */
		                            UPDATE COMPANY SET DB_CODIGO =
		                            (SELECT DB_CODIGO FROM DB_INFO WHERE DB_DATOS = COMPANY.CM_DATOS);
	
	                            END

	                            /* Patch 440: Agregar registros en DB_INFO a partir de COMPANY */
	                            /* 5. Crear llave foránea FK_DB_INFO_DB_CODIGO */
	                            IF EXISTS (select * from syscolumns where id=object_id('COMPANY') and name='DB_CODIGO')
		                            ALTER TABLE COMPANY ADD CONSTRAINT FK_DB_INFO_DB_CODIGO FOREIGN KEY(DB_CODIGO) REFERENCES DB_INFO(DB_CODIGO)
			                            ON UPDATE CASCADE
                               end";

            cmdAjustarDatos = new SqlCommand("alter table company disable trigger all ", ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch
            {}
            cmdAjustarDatos = new SqlCommand("alter table db_info disable trigger all  ", ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch
            {}
            cmdAjustarDatos = new SqlCommand("Update Company set  CM_PASSWRD='' , CM_USRNAME=''", ConexionCompany);
            try
            {
               cmdAjustarDatos.ExecuteNonQuery();
            }
            catch
            {}
            cmdAjustarDatos = new SqlCommand(QueryPatch, ConexionCompany);
            try
            {
                cmdAjustarDatos.ExecuteNonQuery();
            }
            catch(Exception ex)
            {}
            QueryPatch = "execute REGISTROS_DB_INFO";
            //se ejecuta el Procedure
            cmdAjustarDatos = new SqlCommand(QueryPatch, ConexionCompany);
            try
            {
                cmdAjustarDatos.ExecuteNonQuery();
            }
            catch(Exception ex)
            {}
            
            QueryPatch = "DROP PROCEDURE REGISTROS_DB_INFO";
            //se dropea el procedure 
            cmdAjustarDatos = new SqlCommand(QueryPatch, ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch(Exception ex)
            {}
            cmdAjustarDatos = new SqlCommand("Update Company set  CM_PASSWRD='' , CM_USRNAME=''", ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch
            {}
            cmdAjustarDatos = new SqlCommand("Update Claves set CL_TEXTO='' where CL_NUMERO = 1 ", ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch
            {}
            cmdAjustarDatos = new SqlCommand("alter table company enable trigger all", ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch
            {}
            cmdAjustarDatos = new SqlCommand("alter table company Enable trigger all ", ConexionCompany);
            try
            {

                cmdAjustarDatos.ExecuteNonQuery();

            }
            catch
            {}
            if (ConexionCompany.State == ConnectionState.Open)
            {
                    conexionbulk.Close();
                
            
            }
        }
    }
    

}

