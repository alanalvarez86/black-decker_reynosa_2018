﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;

namespace Wizard_Migracion_Tress
{
    public partial class Acercadefrm : Form
    {
        public Acercadefrm()
        {
            InitializeComponent();
        }

        private void Acercadefrm_Load(object sender, EventArgs e)
        {
            
            panel1.Left = ((this.Width) / 2) - ((panel1.Width / 2)+5);
            pictureBox1.Left = 10;
            pictureBox1.Width = this.Width - 36;
            label1.Text += Assembly.GetExecutingAssembly().GetName().Version.ToString();
            label1.Left = (panel1.Width / 2) - (label1.Width / 2);
            label2.Left = (panel1.Width / 2) - (label2.Width / 2);
            label3.Left = (panel1.Width / 2) - (label3.Width / 2);
            label4.Left = (panel1.Width / 2) - (label4.Width / 2);
            
        }
    }
}
