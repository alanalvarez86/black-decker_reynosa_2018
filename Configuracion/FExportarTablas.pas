unit FExportarTablas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, CheckLst,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, DbTables,
     FMigrar,
     ZetaWizard, jpeg;

type
  TExportarTablas = class(TMigrar)
    OpenDialog: TOpenDialog;
    ExportTablesGB: TGroupBox;
    ExportTables: TCheckListBox;
    DirectorioGB: TGroupBox;
    PathLBL: TLabel;
    PathSeek: TSpeedButton;
    Path: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    function Init: Boolean;
  public
    { Public declarations }
  end;

var
  ExportarTablas: TExportarTablas;

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaTressCFGTools,
     ZetaCommonTools,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TExportarTablas.FormCreate(Sender: TObject);
begin
     FParadoxPath := Path;
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'ExportarTablas.log' );
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( '' );
     dmMigracion := TdmReportes.Create( Self );
     inherited;
     HelpContext := H00021_Exportando_tablas;
end;

procedure TExportarTablas.FormShow(Sender: TObject);
begin
     inherited;
     Init;
end;

procedure TExportarTablas.FormDestroy(Sender: TObject);
begin
     TdmReportes( dmMigracion ).Free;
     inherited;
end;

function TExportarTablas.Init: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             GetListaTablas( ExportTables.Items );
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Base De Datos Fuente !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

procedure TExportarTablas.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          with Wizard do
          begin
               if Adelante and EsPaginaActual( DirectorioDOS ) then
                  CanMove := ValidaDirectorioParadox
          end;
     end;
end;

procedure TExportarTablas.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   i: Integer;
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          Lista1.Clear;
          with ExportTables do
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    if Checked[ i ] then
                       Lista1.Add( Items.Strings[ i ] );
               end;
          end;
          InitCounter( Lista1.Count );
          StartProcess;
          SourceSharedPath := Path.Text;
          lOk := ExportarTablas;
          EndProcess;
     end;
     EndLog;
     inherited;
end;

end.
