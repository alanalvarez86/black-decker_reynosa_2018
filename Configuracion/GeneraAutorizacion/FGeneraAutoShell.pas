unit FGeneraAutoShell;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, dxSkinsCore, Vcl.ExtCtrls, cxLookAndFeels, dxSkinsForm, cxClasses,
  cxLocalization, cxGraphics, cxControls, cxLookAndFeelPainters, cxContainer,
  cxEdit, Vcl.Menus, cxCheckBox, Vcl.StdCtrls, cxButtons, Vcl.ImgList,
  cxTextEdit, cxLabel, dxGDIPlusClasses, cxProgressBar, cxGroupBox, AbBase,
  AbBrowse, AbZBrows, AbZipper, FAutoServer, ShellAPI, cxShellBrowserDialog,
  cxImage, ZetaRegistryServer, cxCheckGroup, cxMemo, Vcl.ExtDlgs, FSentinelRegistry;

type
  TFGeneraAutorizacion = class(TForm)
    cxLocalizer: TcxLocalizer;
    dxSkinController: TdxSkinController;
    plnEncabezado: TPanel;
    imgTitleDatosSistema: TImage;
    lblTDatosSistema: TcxLabel;
    plnServerInfo: TPanel;
    plnLog: TPanel;
    cxUserLogged: TcxTextEdit;
    lblDatosUsuario: TcxLabel;
    cxServidorWin: TcxTextEdit;
    lblDatosServWindows: TcxLabel;
    ImageButtons: TcxImageList;
    cxGrupoAvance: TcxGroupBox;
    gpBitacoraOperacion: TcxGroupBox;
    btAbrirFolder: TcxButton;
    lblEnvio: TLabel;
    lblArchivo: TLabel;
    lblRecolectarGenerar: TLabel;
    lblInfoAvance: TLabel;
    pbAvance: TcxProgressBar;
    gpInicio: TcxGroupBox;
    btnArchivo3dt: TcxButton;
    txtArchivo3dt: TcxTextEdit;
    chbGuardarArchivo: TcxCheckBox;
    chbEnviarArchivo: TcxCheckBox;
    cxExportar: TcxButton;
    AbZipper: TAbZipper;
    cxOpenDialog: TcxShellBrowserDialog;
    imgCheck_EnviaArchivo: TcxImage;
    imgCheck_GuardaArchivo: TcxImage;
    imgCheck_RecolectaInfo: TcxImage;
    imgUnCheck_EnviaArchivo: TcxImage;
    imgUnCheck_GuardaArchivo: TcxImage;
    imgUnCheck_RecolectaInfo: TcxImage;
    cxCheckGroup1: TcxCheckGroup;
    rbtnConfiguracionGuardia: TRadioGroup;
    memConfiguracionInfo: TcxMemo;
    procedure FormShow(Sender: TObject);
    procedure cxExportarClick(Sender: TObject);
    procedure btAbrirFolderClick(Sender: TObject);
    procedure btnArchivo3dtClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure rbtnConfiguracionGuardiaClick(Sender: TObject);
  private
    { Private declarations }
    //FRegistry: TZetaRegistryServer;
    FRegistrySent: TSentinelRegistryServer;
    procedure CargarDatosSistema;
    procedure ExportarDatos(sFileName: String; lGuardarArchivo: Boolean; lEnviarArchivo:Boolean);
    procedure Avance (sTexto:String; dProgreso: Double);
    procedure LimpiaInterfaz;
    procedure ConfigSeccionGuardia;
  public
    { Public declarations }
  end;

var
  FGeneraAutorizacion: TFGeneraAutorizacion;

implementation

uses
  ZetaWinAPITools,
  ZetaCommonTools,
  ZetaCommonClasses,
  ZetaDialogo,
  FExportaDatosSistLic,
  FErrorGrabarArchivo3dt,
  ZetaServerTools;

{$R *.dfm}

const
     K_FECHA = 'yyyy-mm-dd';
     K_MSG_CONFIG_AUTO = 'Se usar� la informaci�n del guardia almacenada en el servidor.';
     K_MSG_CONFIG_REINICIO ='Se borrar� la informaci�n del guardia almacenada en el servidor.';

procedure TFGeneraAutorizacion.Avance (sTexto:String; dProgreso: Double);
begin
     lblInfoAvance.Caption := sTexto;
     pbAvance.Position := pbAvance.Position + dProgreso;
     Application.ProcessMessages;
end;

procedure TFGeneraAutorizacion.ExportarDatos(sFileName: String; lGuardarArchivo: Boolean; lEnviarArchivo:Boolean);
const
    K_NO_CONEX = '-1';
var
    oCursor: TCursor;
    sNombreZIP: String;
    oExportaDatosSistLic : TExportaDatosSistLic;
    i: Integer;
procedure ComprimirArchivo (sFileName: String);
var
  stDatos: TStringStream;
begin
    stDatos := TStringStream.Create;
    try
        stDatos.WriteString(oExportaDatosSistLic.Datos.Text);
        AbZipper.FileName := sFileName;
        AbZipper.AddFromStream(oExportaDatosSistLic.FileName, stDatos);
        AbZipper.CloseArchive;
    finally
        FreeAndNil (stDatos);
    end;
end;
begin
    oCursor := Screen.Cursor;
    Screen.Cursor := crHourglass;
    try
      //@(am): Extraccion de informacion y generacion del archivo 3dt.
      try
          oExportaDatosSistLic := TExportaDatosSistLic.Create();
          with oExportaDatosSistLic do
          begin
              //Carga autoserver
              AutoServer :=  TAutoServer.Create;
              AutoServer.Cargar;
              Avance ('Recolectando informaci�n para archivo 3DT', 0);

              Datos := TStringList.Create;
              Datos.Add('<?xml version="1.0" encoding="ISO-8859-1" ?>');
              Datos.Add( Format( '<TressDT Fecha="%s">', [ FormatDateTime( K_FECHA, Now )] ) );

              Avance ('Obtenci�n de datos del sistema operativo', 0);
              ZetaWinAPITools.WriteSystemInfo( Datos, True);

              Avance ('Recolectando informaci�n de Sentinel',25);

              if rbtnConfiguracionGuardia.ItemIndex = 1 then
              begin
                   FRegistrySent.ArchivoLicencia := VACIO;
                   FSentinelRegistry.BorrarSentinel;
              end;

              LoadSentinelData;

              //@(am): El 3dt generado por la utileria no inclye la info. de usuarios ni empresas, ya que tambien sera utilizada para servidores nuevos.
              {
              Avance ('Recolectando informaci�n del usuario', 5);
              LoadUserData;  //PENDIENTE
              Avance ('Recolectando informaci�n de las compa��as', 5);
              LoadCompanyData;
              }
              Datos.Add('</TressDT>');

              //(@am): Encriptacion del contenido del archivo 3dt
              {$ifdef DEBUG_XML_3DT}
                  SaveToFile( sFileName  + '.xml' );
              {$endif}
              with Datos do
              begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                         Strings[ i ] := ZetaServerTools.Encrypt( Strings[ i ] );
                  end;
              end;

              Avance ('Se gener� informaci�n para archivo 3DT', 25);
              gpBitacoraOperacion.Visible := TRUE;
              imgCheck_RecolectaInfo.Visible := TRUE;
          end;
      except
          imgUnCheck_RecolectaInfo.Visible := TRUE;
          imgUnCheck_GuardaArchivo.Visible := TRUE;
          imgUnCheck_EnviaArchivo.Visible := TRUE;
          ZetaDialogo.zInformation( 'Error',
          'No ha sido posible exportar con �xito los datos del sistema', 0 );
      end;
      //Validar si se genera el 3dt con exito.
      if imgCheck_RecolectaInfo.Visible then
      begin
          //@(am):  Enviar archivo 3DT por medio del WebService
          with oExportaDatosSistLic do
          begin
                FileName := sFileName;
              //@(am): Guardar archivo local
              if lGuardarArchivo then
              begin
                  if not IntentarGuardarArchivo then
                      FileName := FErrorGrabarArchivo3dt.EditarArchivo(FileName);

                  Avance ('Se guard� archivo 3DT: ' + FileName, 10);
                  imgCheck_GuardaArchivo.Visible := TRUE;
                  btAbrirFolder.Enabled := TRUE;
              end;
              if lEnviarArchivo then
              begin
                      //Crear nombre para el archivo ZIP
                      sNombreZIP := VerificaDir(ExtractFilePath(FileName)) + crearNombreArchivoZIP;
                      //Hacer el archivo ZIP
                      try
                          Avance('Comprimir archivo 3DT', 10);
                          ComprimirArchivo(sNombreZIP);
                          //Envio del archivo
                          Avance ('Enviando archivo 3DT', 10);
                          EnviarArchivo3DT(FileName, sNombreZIP);
                      finally
                          // Aunque lo pueda enviar o no, debe borrarse el comprimido y liberar el objeto
                          System.SysUtils.DeleteFile(sNombreZIP);
                          AbZipper.Free;
                      end;
                      if (Pos ('FINAL', UpperCase(Respuesta)) > 0) then
                      begin
                          //Notificar el envio al usuario
                          Avance ('Finalizo el envio del archivo exit�samente.', 20); //PENDENTE:PONER EL TEXTO DELA RESP. DEL WEB SERVICE
                          imgCheck_EnviaArchivo.Visible := TRUE;
                      end
                      else
                      begin
                          if Respuesta = K_NO_CONEX then
                              Avance ('No es posible establecer conexi�n con el servicio', 0);
                          imgUnCheck_EnviaArchivo.Visible := TRUE;
                          ZetaDialogo.zError( 'Error','No ha sido posible realizar el env�o del archivo con �xito.', 0 );
                      end;
              end;
          end;
          if (btAbrirFolder.Enabled) or  (imgCheck_EnviaArchivo.Visible) then  //Si se guardo el archivo o se envio
            ZetaDialogo.zInformation( 'Los datos han sido exportados','Los datos del sistema han sido exportados con �xito', 0 );
      end;
    finally
      Screen.Cursor := oCursor;
      oExportaDatosSistLic.AutoServer.Free;
      FreeAndNil(oExportaDatosSistLic );
    end;
end;

procedure TFGeneraAutorizacion.btAbrirFolderClick(Sender: TObject);
begin
  ShellExecute(Handle, 'open','c:\windows\explorer.exe', {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(ExtractFilePath(txtArchivo3dt.Text)), nil, SW_SHOWNORMAL);
end;

procedure TFGeneraAutorizacion.btnArchivo3dtClick(Sender: TObject);
var sFileName: String;
begin
      sFileName := StrDef (ExtractFileName(txtArchivo3dt.Text), 'DatosSistema.3dt');
      cxOpenDialog.Path := ExtractFilePath( Application.ExeName );
      if not strLleno (ExtractFileExt(sFileName)) then
          sFileName := sFileName + '.3dt';
      if cxOpenDialog.Execute then
        txtArchivo3dt.Text := VerificaDir (cxOpenDialog.Path)  + sFileName;
end;

procedure TFGeneraAutorizacion.CargarDatosSistema;
begin
  cxServidorWin.Text := GetWindowsVersionName( System.SysUtils.Win32MajorVersion, System.SysUtils.Win32MinorVersion );
  cxUserLogged.Text := ZetaWinApiTools.GetCurrentUserName;
end;

procedure TFGeneraAutorizacion.LimpiaInterfaz;
begin
    lblInfoAvance.Caption := '';
    pbAvance.Position := 0;
    Application.ProcessMessages;
    imgCheck_RecolectaInfo.Visible := FALSE;
    imgCheck_GuardaArchivo.Visible := FALSE;
    imgCheck_EnviaArchivo.Visible := FALSE;
    imgUnCheck_RecolectaInfo.Visible := FALSE;
    imgUnCheck_GuardaArchivo.Visible := FALSE;
    imgUnCheck_EnviaArchivo.Visible := FALSE;
    gpBitacoraOperacion.Visible := FALSE;
end;

procedure TFGeneraAutorizacion.ConfigSeccionGuardia;
begin
      memConfiguracionInfo.Clear;
      case rbtnConfiguracionGuardia.ItemIndex of
         0:
         begin
              memConfiguracionInfo.Lines.Add(K_MSG_CONFIG_AUTO);
         end;
         1:
         begin
              memConfiguracionInfo.Lines.Add(K_MSG_CONFIG_REINICIO);
         end;
      end;
end;
procedure TFGeneraAutorizacion.rbtnConfiguracionGuardiaClick(Sender: TObject);
begin
     ConfigSeccionGuardia;
end;

procedure TFGeneraAutorizacion.cxExportarClick(Sender: TObject);
var
    sFileName: String;
    lExtraerInfo:Boolean;
begin
    LimpiaInterfaz;
    sFileName := txtArchivo3dt.Text;
    lExtraerInfo := TRUE;
    if strLleno(sFileName) then
    begin
      if rbtnConfiguracionGuardia.ItemIndex = 1 then //1 --> Reinicio de guardia
           if  not ZConfirm( 'Reiniciar informacion del guardia', 'Este cambio va a reiniciar la informacion del guardia' + CR_LF + '� Desea continuar ? ', 0, mbYes ) then
              lExtraerInfo := FALSE;

      //(@am):Si lExtraerInfo es TRUE procede la exportacion de datos.
      if lExtraerInfo then
      begin
           if StrVacio( ExtractFileExt( sFileName ) ) then
               sFileName := sFileName + '.3dt';
           ExportarDatos(sFileName, chbGuardarArchivo.checked, chbEnviarArchivo.checked);
      end;
    end
    else
      ZetaDialogo.zInformation('Advertencia','Seleccione una ruta para guardar el archivo.',0);
end;

procedure TFGeneraAutorizacion.FormCreate(Sender: TObject);
begin
    FRegistrySent := TSentinelRegistryServer.Create( True );
    //FRegistry := TZetaRegistryServer.Create;
end;

procedure TFGeneraAutorizacion.FormDestroy(Sender: TObject);
begin
     FRegistrySent := TSentinelRegistryServer.Create( True );
     //FRegistry.Free;
end;

procedure TFGeneraAutorizacion.FormShow(Sender: TObject);
begin
  with FRegistrySent do
  begin
        if  CanWrite then
        begin
            LimpiaInterfaz;
            CargarDatosSistema;
        end
        else
        begin
            ZetaDialogo.ZError('Error', 'Ejecute esta aplicaci�n como Administrador.', 0);
            close();
        end

  end;

  //Configuracion Automatica es el default.
  rbtnConfiguracionGuardia.ItemIndex := 0;
  if rbtnConfiguracionGuardia.ItemIndex = 0 then
       memConfiguracionInfo.Lines.Add(K_MSG_CONFIG_AUTO);
end;



end.
