unit FConsolidar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBCtrls, Grids, DBGrids, ComCtrls, StdCtrls,
     Buttons, ExtCtrls, Db, Mask,
     DMigrar,
     FMigrar,
     ZetaWizard,
     ZetaFecha,
     ZetaNumero, jpeg;

type
  TConsolidar = class(TMigrar)
    EmpresasGB: TGroupBox;
    memoEmpresas: TMemo;
    AcumuladosGB: TGroupBox;
    YearLBL: TLabel;
    Year: TZetaNumero;
    AsistenciaGB: TGroupBox;
    FechaInicialLBL: TLabel;
    FechaFinalLBL: TLabel;
    FechaInicial: TZetaFecha;
    FechaFinal: TZetaFecha;
    EmpresaGB: TGroupBox;
    BorrarTablas: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    FEmpresas: TCompanyList;
    FCompanyName: String;
    FLimiteEmpleados: Integer;
    procedure SetCompanyName( const sValor: String );
  public
    { Public declarations }
    function InitListaEmpresas: Boolean;
    property CompanyName: String read FCompanyName write SetCompanyName;
    property LimiteEmpleados: Integer read FLimiteEmpleados write FLimiteEmpleados;
  end;

var
  Consolidar: TConsolidar;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaMigrar,
     ZetaDialogo,
     FHelpContext,
     DDBConfig,
     DReportes;

{$R *.DFM}

procedure TConsolidar.FormCreate(Sender: TObject);
var
   iYear: Integer;
begin
     dmMigracion := TdmReportes.Create( Self );
     FEmpresas := TCompanyList.Create;
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Consolidacion.log' );
     iYear := TheYear( Now );
     Year.Valor := iYear;
     FechaInicial.Valor := FirstDayOfYear( iYear );
     FechaFinal.Valor := LastDayOfYear( iYear );
     inherited;
     HelpContext := H00006_Consolidar_empresas;
end;

procedure TConsolidar.FormDestroy(Sender: TObject);
begin
     FEmpresas.Free;
     inherited;
     TdmReportes( dmMigracion ).Free;
end;

procedure TConsolidar.SetCompanyName( const sValor: String );
begin
     if ( FCompanyName <> sValor ) then
     begin
          FCompanyName := sValor;
          EmpresaGB.Caption := ' ' + FCompanyName + ' ';
     end;
end;

function TConsolidar.InitListaEmpresas: Boolean;
var
   oCursor: TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with dmDBConfig do
        begin
             LlenaListaConsolidadas( FEmpresas );
             CompanyName := GetCompanyName;
        end;
        if ( FEmpresas.Count < 1 ) then
        begin
             zWarning( Caption, 'Se Requiere Una o M�s Empresas Para Poder Consolidar', 0, mbOK );
             Result := False;
        end
        else
        begin
             with memoempresas.Lines do
             begin
                  BeginUpdate;
                  Clear;
                  with FEmpresas do
                  begin
                       for i := 0 to ( Count - 1 ) do
                       begin
                            Add( Format( '%3d', [ i + 1 ] ) + ' .- ' + Empresa[ i ].Nombre );
                       end;
                  end;
                  EndUpdate;
             end;
             Result := True;
        end;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Crear Lista de Empresas !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

procedure TConsolidar.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( GetConsolidationSteps( FEmpresas ) );
          StartProcess;
          EnableStopOnError;
          TargetAlias := Self.TargetAlias;
          TargetUserName := Self.TargetUserName;
          TargetPassword := Self.TargetPassword;
          Consolidar( CompanyName,
                      FEmpresas,
                      Year.ValorEntero,
                      FechaInicial.Valor,
                      FechaFinal.Valor,
                      BorrarTablas.Checked,
                      LimiteEmpleados );
          EndProcess;
     end;
     EndLog;
     inherited;
end;

end.
