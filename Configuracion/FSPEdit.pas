unit FSPEdit;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, Buttons, ExtCtrls, Mask,
     Db, DBCtrls, DBTables,
     ZBaseDlgModal,
     ZetaNumero,
     ZetaCommonTools,
     ZetaKeyCombo, ComCtrls;

type
  TSPEdit = class(TZetaDlgModal)
    PanelControles: TPanel;
    SE_NOMBRElbl: TLabel;
    SE_TIPOlbl: TLabel;
    SE_NOMBRE: TDBEdit;
    SE_TIPO: TZetaDBKeyCombo;
    SE_DESCRIPlbl: TLabel;
    SE_DESCRIP: TDBEdit;
    PanelIB: TPanel;
    IBLoadFromFile: TSpeedButton;
    SE_DATA: TDBMemo;
    SE_SEQ_NUM: TZetaDBNumero;
    SE_SEQ_NUMlbl: TLabel;
    DataSource: TDataSource;
    OpenDialog: TOpenDialog;
    lblAplicarEn: TLabel;
    SE_APLICA: TZetaDBKeyCombo;
    PageControlScripts: TPageControl;
    tabProcedure: TTabSheet;
    tabEspecial: TTabSheet;
    SE_ARCHIVO: TDBEdit;
    Label1: TLabel;
    SQLSelectFile: TSpeedButton;
    OpenDialogScriptPath: TOpenDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IBLoadFromFileClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure SQLSelectFileClick(Sender: TObject);
    procedure SE_TIPOChange(Sender: TObject);
  private
    { Private declarations }
    FAgrego, FEdito : boolean;
  public
    { Public declarations }
    property Agrego : boolean  read FAgrego;
    property Edito : boolean  read FEdito;
  end;

var
  SPEdit: TSPEdit;

implementation

uses FHelpContext,
     ZetaDialogo,
     ZetaMigrar,
     ZetaCommonLists,
     DReportes;

{$R *.DFM}

procedure TSPEdit.FormCreate(Sender: TObject);
var
   eCiclo: eTipoCompany;
begin
     inherited;
     {$ifndef MSSQL}
         tabEspecial.Visible := False;
         tabEspecial.TabVisible := False;
         tabEspecial.Enabled := False;
     {$endif}

     FAgrego := False;
     FEdito := False;

     with SE_TIPO.Lista do
     begin
          Add( '0=Comparte' );
          for eCiclo := Low( eTipoCompany ) to High( eTipoCompany ) do
          begin
               Add( Format( '%d=%s', [ Ord( eCiclo ) + 1, ZetaCommonLists.ObtieneElemento( lfTipoCompanyName, Ord( eCiclo ) ) ] ) );
          end;
     end;

     //dmReportes.LlenaListaCompaniasAplica( SE_APLICA.Lista );
     HelpContext := H00027_Stored_Procedures_Adicionales;
end;

procedure TSPEdit.FormShow(Sender: TObject);
begin
     inherited;
     ActiveControl := SE_NOMBRE;

     {$ifdef MSSQL}
     if StrLleno( Datasource.DataSet.FieldByName('SE_ARCHIVO').AsString ) then
        PageControlScripts.TabIndex := tabEspecial.TabIndex
     else
         PageControlScripts.TabIndex := tabProcedure.TabIndex;
     {$else}
        PageControlScripts.TabIndex := tabProcedure.TabIndex;
     {$endif}
end;

procedure TSPEdit.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     with Datasource.Dataset do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Cancel;
     end;
end;
procedure TSPEdit.IBLoadFromFileClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          if Execute then
          begin
             SE_DATA.Lines.LoadFromFile( FileName );
          end;
     end;
end;

procedure TSPEdit.OKClick(Sender: TObject);
begin
     inherited;

     try
        with Datasource.Dataset do
        begin
             if ( State in [ dsEdit ] ) then
             begin
                Post;
                dmReportes.GrabaSPPatch;
                FEdito := True;
             end
             else
             if ( State in [ dsInsert ] ) then
             begin
//                Post;
                   dmReportes.AgregaSPPatch;
                   {
                if ( dmReportes.AgregaSPPatch  ) then
                   Post;                             }

                FAgrego := True;
             end;

        end;
        ModalResult := mrOk;
     except
           on Error: Exception do
           begin
                ModalResult := mrNone;
                with Datasource.DataSet do
                begin
                   if  not ( State in [ dsEdit, dsInsert ] ) then
                   begin
                        if ( FEdito) then
                           Edit;
                   end
                end;
                Application.HandleException( Error );
           end;
     end;
end;

procedure TSPEdit.CancelarClick(Sender: TObject);
begin
     inherited;
     with Datasource.Dataset do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Cancel;
     end;
end;

procedure TSPEdit.DataSourceDataChange(Sender: TObject; Field: TField);
var
 tcTipo : eTipoCompany;
 iSE_TIPO : integer;
begin
  inherited;
  SE_APLICA.Enabled := not ( Datasource.Dataset.FieldByName('SE_TIPO').AsInteger = 0 );
  iSE_TIPO :=  Datasource.Dataset.FieldByName('SE_TIPO').AsInteger - 1;
  if ( iSE_TIPO < 0 ) then
     tcTipo := tc3Datos
  else
      tcTipo := eTipoCompany( iSE_TIPO);

  dmReportes.LlenaListaCompaniasAplica( SE_APLICA.Lista, tcTipo );
  SE_APLICA.Refresh;
end;

procedure TSPEdit.SQLSelectFileClick(Sender: TObject);
begin
    inherited;
    with OpenDialogScriptPath do
    begin
         if Execute then
         begin
            Datasource.Dataset.FieldByName('SE_ARCHIVO').AsString := FileName;
         end;
    end
end;

procedure TSPEdit.SE_TIPOChange(Sender: TObject);
var
 tcTipo : eTipoCompany;
 iSE_TIPO : integer;
begin
  inherited;
  SE_APLICA.Enabled := not ( SE_TIPO.ItemIndex  = 0 );

  iSE_TIPO :=  SE_TIPO.ItemIndex - 1;
  if ( iSE_TIPO < 0 ) then
     tcTipo := tc3Datos
  else
      tcTipo := eTipoCompany( iSE_TIPO);

  dmReportes.LlenaListaCompaniasAplica( SE_APLICA.Lista, tcTipo );
  SE_APLICA.ItemIndex := SE_APLICA.Items.Count - 1;
  SE_APLICA.Refresh;

end;

end.

