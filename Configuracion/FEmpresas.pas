unit FEmpresas;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FEmpresas.pas                              ::
  :: Descripci�n: Programa principal de TressCFG.EXE         ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

{$ifdef MSSQL}
{$define DEBUGSENTINEL}
{$define SENTINELVIRTUAL}
{$endif}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Menus,
     Forms, Dialogs, ExtCtrls, StdCtrls, Buttons, Db, Grids, DBGrids,
     ToolWin, ComCtrls, DBCtrls, Registry, ActnList, ImgList,
     DDBConfig, ShellAPI,
     FAutoServer,
     FAutoServerLoader,
     FMigrar,
     ZetaCommonLists,
     ZetaSmartLists,
     {$IFDEF TRESS_DELPHIXE5_UP} HtmlHelpViewer, {$ELSE} FHelpManager, {$ENDIF}
     FPatchDatosMultiple, System.Actions;

type
  TEmpresas = class(TForm)
    dsCompany: TDataSource;
    PanelEmpresas: TPanel;
    MainMenu: TMainMenu;
    Archivo: TMenuItem;
    Empresas: TMenuItem;
    EmpresasN1: TMenuItem;
    Procesos: TMenuItem;
    ProcesosPoblar: TMenuItem;
    ProcesosRevisar: TMenuItem;
    ProcesosMigrar: TMenuItem;
    ProcesosMigrarComparte: TMenuItem;
    N9: TMenuItem;
    ProcesosExportaReportes: TMenuItem;
    ProcesosImportaReportes: TMenuItem;
    ProcesosImportaConceptos: TMenuItem;
    ProcesosExportaTablas: TMenuItem;
    Ayuda: TMenuItem;
    AyudaContenido: TMenuItem;
    AyudaUsandoAyuda: TMenuItem;
    AyudaN1: TMenuItem;
    AyudaAcercaDe: TMenuItem;
    PanelToolBar: TPanel;
    StatusBar: TStatusBar;
    DBGrid: TDBGrid;
    ProcesosImportaTablas: TMenuItem;
    N10: TMenuItem;
    ProcesosActualizarEmpresa: TMenuItem;
    ProcesosActualizarComparte: TMenuItem;
    ProcesosConsolidarEmpresas: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    ActionList: TActionList;
    ImageList: TImageList;
    _ArchivoAbrir: TAction;
    _ArchivoCerrar: TAction;
    _ArchivoConfigurar: TAction;
    _ArchivoEstructuraComparte: TAction;
    _ArchivoBDE: TAction;
    _ArchivoODBC: TAction;
    _ArchivoSalir: TAction;
    ArchivoAbrir: TMenuItem;
    ArchivoCerrar: TMenuItem;
    N4: TMenuItem;
    ArchivoConfigurar: TMenuItem;
    ArchivoInvocarBDE: TMenuItem;
    ArchivoInvocarODBC: TMenuItem;
    N5: TMenuItem;
    ArchivoSalir: TMenuItem;
    _EmpresaAgregar: TAction;
    _EmpresaBorrar: TAction;
    _EmpresaModificar: TAction;
    _EmpresaPrimera: TAction;
    _EmpresaSiguiente: TAction;
    _EmpresaAnterior: TAction;
    _EmpresaUltima: TAction;
    BtnPrimero: TZetaSpeedButton;
    btnAnterior: TZetaSpeedButton;
    btnSiguiente: TZetaSpeedButton;
    btnUltima: TZetaSpeedButton;
    EmpresaAgregar: TMenuItem;
    EmpresaBorrar: TMenuItem;
    EmpresaModificar: TMenuItem;
    EmpresaPrimera: TMenuItem;
    EmpresaAnterior: TMenuItem;
    EmpresaSiguiente: TMenuItem;
    EmpresaUltima: TMenuItem;
    btnAgregar: TZetaSpeedButton;
    btnBorrar: TZetaSpeedButton;
    btnModificar: TZetaSpeedButton;
    _AyudaContenido: TAction;
    _AyudaUsandoAyuda: TAction;
    Sentinel: TMenuItem;
    _SentinelVer: TAction;
    _SentinelActualizar: TAction;
    _SentinelRenovar: TAction;
    SentinelVer: TMenuItem;
    SentinelActualizar: TMenuItem;
    SentinelRenovar: TMenuItem;
    _SistemaDatos: TAction;
    _SistemaProcessServer: TAction;
    Sistema: TMenuItem;
    DatosDelSistema1: TMenuItem;
    ServidordeProcesos1: TMenuItem;
    _EmpresaProbar: TAction;
    N6: TMenuItem;
    EmpresaProbarAcceso: TMenuItem;
    btnProbar: TZetaSpeedButton;
    _ProcesosPoblar: TAction;
    _ProcesosRevisar: TAction;
    _ProcesosMigrar: TAction;
    _ProcesosMigrarComparte: TAction;
    _ProcesosActualizar: TAction;
    _ProcesosActualizarComparte: TAction;
    _ProcesosConsolidar: TAction;
    _ExportarReportes: TAction;
    _ImportarReportes: TAction;
    _ImportarConceptos: TAction;
    _ExportarTablas: TAction;
    _ImportarTablas: TAction;
    btnPoblar: TZetaSpeedButton;
    btnActualizar: TZetaSpeedButton;
    btnExportarReportes: TZetaSpeedButton;
    btnImportarReportes: TZetaSpeedButton;
    ZetaSpeedButton1: TZetaSpeedButton;
    ZetaSpeedButton2: TZetaSpeedButton;
    ImportExport1: TMenuItem;
    Importar1: TMenuItem;
    _ImportarDatos: TAction;
    _ImportarComparte: TAction;
    ImportarComparte1: TMenuItem;
    _ExportarDatos: TAction;
    _ExportarComparte: TAction;
    DatosdeSQLServeraIB1: TMenuItem;
    CompartedeSQLServeraIB1: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    _ProcesosCompararCatalogos: TAction;
    _ProcesosTransferir: TAction;
    N1: TMenuItem;
    ProcesosCompararCatlogos: TMenuItem;
    ProcesosTransferirEmpleados: TMenuItem;
    _ArchivoStoredProcedures: TAction;
    N11: TMenuItem;
    ArchivoEstructuraComparte: TMenuItem;
    _EmpresaEstructura: TAction;
    EmpresaConfigurarEstructura: TMenuItem;
    ArchivoStoredProceduresAdicionales: TMenuItem;
    _EmpresaInit: TAction;
    InicializarDatos1: TMenuItem;
    _ImportarImagenes: TAction;
    ProcesosImportarImagenes: TMenuItem;
    _AyudaNuevo: TAction;
    NuevoenTress: TMenuItem;
    N12: TMenuItem;
    DesbloqueoAdministradores1: TMenuItem;
    _SistemaDesbloquearAdministradores: TAction;
    AyudaLicenciadeUso: TMenuItem;
    _AyudaLicencia: TAction;
    _SentinelInstalacion: TAction;
    SentinelInstalacion: TMenuItem;
    _EmpresaAsignarLicencias: TAction;
    N13: TMenuItem;
    EmpresasAsignarLicenciasDeEmpleados: TMenuItem;
    ProcesosInicializaComparte: TMenuItem;
    _ProcesosComparteInit: TAction;
     _PrepararPresupuestos: TAction;
    PrepararPresupuestos1: TMenuItem;
    _ProcesosActualizarMultiples: TAction;
    ActualizarMltiplesEmpresas1: TMenuItem;
    _SentinelLicencia: TAction;
    LicenciadeSentinelVirtual1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure dsCompanyDataChange(Sender: TObject; Field: TField);
    procedure dsCompanyStateChange(Sender: TObject);
    procedure _ArchivoAbrirExecute(Sender: TObject);
    procedure _ArchivoCerrarExecute(Sender: TObject);
    procedure _ArchivoConfigurarExecute(Sender: TObject);
    procedure _ArchivoEstructuraComparteExecute(Sender: TObject);
    procedure _ArchivoBDEExecute(Sender: TObject);
    procedure _ArchivoODBCExecute(Sender: TObject);
    procedure _ArchivoSalirExecute(Sender: TObject);
    procedure _EmpresaAgregarExecute(Sender: TObject);
    procedure _EmpresaBorrarExecute(Sender: TObject);
    procedure _EmpresaModificarExecute(Sender: TObject);
    procedure _EmpresaPrimeraExecute(Sender: TObject);
    procedure _EmpresaAnteriorExecute(Sender: TObject);
    procedure _EmpresaSiguienteExecute(Sender: TObject);
    procedure _EmpresaUltimaExecute(Sender: TObject);
    procedure _AyudaContenidoExecute(Sender: TObject);
    procedure _AyudaUsandoAyudaExecute(Sender: TObject);
    procedure _AyudaAcercaDeExecute(Sender: TObject);
    procedure _SentinelActualizarExecute(Sender: TObject);
    procedure _SentinelVerExecute(Sender: TObject);
    procedure _SentinelRenovarExecute(Sender: TObject);
    procedure _SistemaDatosExecute(Sender: TObject);
    procedure _SistemaProcessServerExecute(Sender: TObject);
    procedure _EmpresaProbarExecute(Sender: TObject);
    procedure _ProcesosPoblarExecute(Sender: TObject);
    procedure _ProcesosRevisarExecute(Sender: TObject);
    procedure _ProcesosMigrarExecute(Sender: TObject);
    procedure _ProcesosConsolidarExecute(Sender: TObject);
    procedure _ExportarReportesExecute(Sender: TObject);
    procedure _ImportarReportesExecute(Sender: TObject);
    procedure _ProcesosActualizarExecute(Sender: TObject);
    procedure _ProcesosMigrarComparteExecute(Sender: TObject);
    procedure _ProcesosActualizarComparteExecute(Sender: TObject);
    procedure _ImportarConceptosExecute(Sender: TObject);
    procedure _ExportarTablasExecute(Sender: TObject);
    procedure _ImportarTablasExecute(Sender: TObject);
    procedure _ImportarDatosExecute(Sender: TObject);
    procedure _ImportarComparteExecute(Sender: TObject);
    procedure _ProcesosCompararCatalogosExecute(Sender: TObject);
    procedure _ProcesosTransferirExecute(Sender: TObject);
    procedure _ArchivoStoredProceduresExecute(Sender: TObject);
    procedure _EmpresaEstructuraExecute(Sender: TObject);
    procedure _EmpresaInitExecute(Sender: TObject);
    procedure _ImportarImagenesExecute(Sender: TObject);
    procedure _AyudaNuevoExecute(Sender: TObject);
    procedure _SistemaDesbloquearAdministradoresExecute(Sender: TObject);
    procedure _AyudaLicenciaExecute(Sender: TObject);
    procedure _SentinelInstalacionExecute(Sender: TObject);
    procedure _EmpresaAsignarLicenciasExecute(Sender: TObject);
    procedure _ProcesosComparteInitExecute(Sender: TObject);
    procedure _PrepararPresupuestosExecute(Sender: TObject);
    procedure _ProcesosActualizarMultiplesExecute(Sender: TObject);
    procedure _SentinelLicenciaExecute(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    FStarting: Boolean;
    FOldHelpEvent: THelpEvent;
    FTErrorActualiza: Boolean;
    FExisteChecksum : Boolean;
    function GetMaxEmpleados: Integer;
    property MaxEmpleados: Integer read GetMaxEmpleados;
    function HayComparte: Boolean;
    function HayEmpresas: Boolean;
    function Connect: Boolean;
    function ConnectCompanys( var lLoop: Boolean ): Boolean;
    function Connected: Boolean;
    function EstructuraComparte: Boolean;
    function ActualizaComparte: Boolean;
    function Tipo: eTipoCompany;
    function TipoReal: eTipoCompany;
    function TipoTress: Boolean;
    function TipoTressProduccion : Boolean;
    function TipoPresupuesto: Boolean;   
    procedure Disconnect;
    procedure Editar( ModoEdicion: TDataSetState );
    procedure ManejaErrorSentinel(const sMensaje: String);
    procedure Refrescar;
    procedure SetControls;
    procedure SetWizard( Wizard: TMigrar );
    function HTMLHelpHook( Command: Word; Data: Longint; var CallHelp: Boolean ): Boolean;
  public
    { Public declarations }
    //function ExecuteFile(const FileName, Params,DefaultDir: String): THandle;
    procedure DisplayHint( Sender: TObject );
  end;

var
  Empresas: TEmpresas;

implementation

uses ZetaDialogo,      
     ZetaODBCTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaFilesTools,
     {$ifdef DEBUGSENTINEL}
     ZetaLicenseMgr,
     DZetaServerProvider,
     {$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
     {$endif}
     {$else}
     DSentinelWrite,
     {$endif}
     FHelpContext,
     FDBChecker,
     FAutoClasses,
     FTressCFGAcercaDe,
     FHayProblemas,
     FHayProblemasComparte,
     FEmpresasEdit,
     FMigrarDatos,
     FMigrarComparte,
     FPoblarDatos,
     FProcessServer,
     FRevisarDatos,
     FReporteExport,
     FReporteImport,
     FConsolidar,
     FComparaTablas,
     FTransferirEmpleados,
     FConceptosImport,
     FExportarTablas,
     FImportarTablas,
     FImportarImagenes,
     FPatchDatos,
     FPatchComparte,
     FSPConsulta,
     FDesbloqueaAdministradores,
     {$ifdef MSSQL}
     FConfigurarBD,
     FWizardImportar,
     FWizardPresupuesto,
     FSentinelLicenciaWrite,
     {$endif}
     FInitBD,
     FSystemData,
     FSentinelVer,
     FSentinelWrite,
     FSentinelInstalacion,
     FLicencias,
     FLicenciaUsuario;

{$R *.DFM}

{ ************* TEmpresas ************** }

procedure TEmpresas.FormCreate(Sender: TObject);
const
     K_CAPTION = 'Configurador de Tress - %s';
begin
     FStarting := True;
     FAutoServerLoader.FCalculandoEmpleadosLoader := FALSE;
     FAutoServer := TAutoServer.Create;
     {$ifdef INTERBASE}
     Caption := Format( K_CAPTION, [ 'Interbase' ] );
     with FAutoServer do
     begin
          SQLType := engInterbase;
          AppType := atAmbas;
     end;
     {$endif}
     {$ifdef MSSQL}
     Caption := Format( K_CAPTION, [ 'SQL Server' ] );
     with FAutoServer do
     begin
          SQLType := engMSSQL;
          AppType := atCorporativa;
     end;
     {$endif}


     Environment;
     dmDBConfig := TdmDBConfig.Create( Self );
     dmDBConfig.SetAutoServer(  Self.FAutoServer );
     HelpContext := H00001_Usando_el_programa_TressCFG;
     with Application do
     begin
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
          OnHint := DisplayHint;
          UpdateFormatSettings := FALSE;
     end;
end;

procedure TEmpresas.FormShow(Sender: TObject);
var
      lOk: Boolean;

      procedure SetUpMenuItem( Item: TMenuItem; const lHabilitado: Boolean );
      begin
           with Item do
           begin
                Enabled := lHabilitado;
                Visible := lHabilitado;
           end;
      end;

      procedure SetUpAction( Item: TAction; const lHabilitado: Boolean );
      begin
           with Item do
           begin
                Visible := lHabilitado;
           end;
      end;

begin
     lOk := dmDBConfig.CheckRegistry;
     if not lOk then
        lOk := FHayProblemas.EditarRegistry;
     if lOk then
     begin
          if Connect then
          begin
{$ifdef INTERBASE}
               SetUpMenuItem( N2, True );
               SetUpAction( _ProcesosRevisar, True );
               SetUpAction( _ProcesosMigrar, True );
               SetUpAction( _ProcesosMigrarComparte, True );
               SetUpAction( _PrepararPresupuestos, False );
{$endif}
{$ifdef MSSQL}
               SetUpMenuItem( N7, True );
               SetUpMenuItem( N9, True );
               SetUpAction( _ArchivoEstructuraComparte, True );
               SetUpAction( _EmpresaEstructura, True );
               SetUpAction( _ImportarDatos, True );
               SetUpAction( _ImportarComparte, True );
               SetUpAction( _ExportarDatos, False );  // No se ha implementado
               SetUpAction( _ExportarComparte, False );  // No se ha implementado
               SetUpAction( _PrepararPresupuestos, True );
{$endif}
          end
          else
              Application.Terminate;
     end
     else
         Application.Terminate;
     FStarting := False;
end;

procedure TEmpresas.FormDestroy(Sender: TObject);
begin
     dmDBConfig.Free;
     FreeAndNil( FAutoServer );
end;

function TEmpresas.HTMLHelpHook(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
     Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

procedure TEmpresas.DisplayHint(Sender: TObject);
begin
     StatusBar.SimpleText := GetLongHint( Application.Hint );
end;

function TEmpresas.Connect: Boolean;
var
     oCursor: TCursor;
     lLoop: Boolean;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     Result := False;
     lLoop := True;
     FExisteChecksum := False;
     try

     {$ifdef SENTINELVIRTUAL}
       FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
     {$endif}
       FAutoServer.Cargar;
       TAutoServerLoader.LoadSentinelInfo( FAutoServer);

        while lLoop and not Result do
        begin
             try
                dmDBConfig.ConectarComparte;
                Result := ConnectCompanys( lLoop );
                if ( Result ) then
                begin
                   FExisteChecksum := dmDBConfig.CrearEstructurasChecksum;
                   dmDBConfig.ActualizarCheckSumTodasCompanys;
                   dmDBConfig.RefrescarCompany;
                end;
             except
                   on Error: Exception do
                   begin
                        lLoop := FHayProblemas.ConnectionProblemDialog( Error );
                   end;
              end;
        end;
        if Result then
        begin
            FAutoServer.Cargar;
            TAutoServerLoader.LoadSentinelInfo( FAutoServer);
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     SetControls;
     {$ifdef SENTINELVIRTUAL}
     FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
     {$endif}

end;

function TEmpresas.ConnectCompanys( var lLoop: Boolean ): Boolean;
const
     {$ifdef MSSQL}
     K_ERROR_COLUMNA = 'Invalid column name';
     {$else}
      K_ERROR_COLUMNA = 'Column unknown';
     {$endif}
begin
     Result := False;                                       
     try
        with dmDBConfig do
        begin
             AbrirCompany;
             ConectarCompany( dsCompany );
             Result := True;
        end;
     except
           on Error: Exception do
           begin
                FTErrorActualiza:= ( Pos( K_ERROR_COLUMNA,Error.Message ) > 0 );
                {$ifdef INTERBASE}
                if  (FStarting or  FHayProblemasComparte.MetaDataProblemDialog( Error, FTErrorActualiza )) and ( FTErrorActualiza and ActualizaComparte ) then
                begin
                     lLoop := True;
                     //Result := True;
                end
                else
                begin
                     lLoop := False;
                     Result:= True;
                end;
                {$endif}
                {$ifdef MSSQL}
                if ( FStarting or FHayProblemasComparte.MetaDataProblemDialog( Error, FTErrorActualiza ) ) and
                   ( ( FTErrorActualiza and ActualizaComparte ) or
                     ( not FTErrorActualiza and EstructuraComparte ) ) then
                begin
                     lLoop := True;
                end
                else
                begin
                     lLoop := False;
                     Result := True;
                end;
                {$endif}
           end;
     end;
end;

procedure TEmpresas.Disconnect;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with dmDBConfig do
           begin
                CerrarCompany;
                DesconectaComparte;
                DesconectarCompany( dsCompany );
           end;
        except
              on Error: Exception do
              begin
                   zExcepcion( '� Hay Problemas !', 'Error Al Cerrar Base de Datos Compartida', Error, 0 );
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     SetControls;
end;

procedure TEmpresas.Refrescar;
begin
     Disconnect;
     Connect;
end;

function TEmpresas.Connected: Boolean;
begin
     Result := dmDBConfig.Connected;
end;

function TEmpresas.HayComparte: Boolean;
begin
     Result := Connected;
     if Result then
     begin
          with dsCompany do
          begin
               if Assigned( Dataset ) then
                  Result := Dataset.Active
               else
                   Result := False;
          end;
     end;
end;

function TEmpresas.HayEmpresas: Boolean;
begin
     Result := Connected;
     if Result then
     begin
          with dsCompany do
          begin
               if Assigned( Dataset ) then
               begin
                    with Dataset do
                    begin
                         Result := Active and not IsEmpty;
                    end;
               end
               else
                   Result := False;
          end;
     end;
end;

function TEmpresas.Tipo: eTipoCompany;
var
   oCampo: TField;
begin
     Result := tc3Datos;
     with dsCompany do
     begin
          if Assigned( Dataset ) then
          begin
               oCampo := Dataset.FieldByName( 'CM_TIPO' );
               if Assigned( oCampo ) then
               begin
                  Result := eTipoCompany( oCampo.AsInteger );
                  if ( Result = tc3Prueba ) then
                     Result := tc3Datos;
               end;
          end;
     end;
end;

function TEmpresas.TipoReal: eTipoCompany;
var
   oCampo: TField;
begin
     Result := tc3Datos;
     with dsCompany do
     begin
          if Assigned( Dataset ) then
          begin
               oCampo := Dataset.FieldByName( 'CM_TIPO' );
               if Assigned( oCampo ) then
               begin
                  Result := eTipoCompany( oCampo.AsInteger );
               end;
          end;
     end;
end;


function TEmpresas.TipoTressProduccion : Boolean;
begin
     Result := ( TipoReal = tc3Datos );
end;

function TEmpresas.TipoTress: Boolean;
begin
     Result := ( Tipo = tc3Datos );
end;

function TEmpresas.TipoPresupuesto: Boolean;
begin
     Result := ( Tipo = tcPresupuesto );
end;

procedure TEmpresas.SetControls;
var
   lHayEmpresas, lHayComparte: Boolean;
   lConnected: Boolean;
begin
     lHayComparte := HayComparte;
     DBGrid.Enabled :=   FExisteChecksum;

     lHayEmpresas := HayEmpresas and FExisteChecksum;
     lConnected := Connected;
     _ArchivoAbrir.Enabled := not lConnected;
     _ArchivoCerrar.Enabled := lConnected;
     _EmpresaProbar.Enabled := lHayEmpresas;
     _EmpresaAgregar.Enabled := lHayComparte  and FExisteChecksum;;
     _EmpresaBorrar.Enabled := lHayEmpresas;
     _EmpresaModificar.Enabled := lHayEmpresas;
     {$ifdef MSSQL}
     _EmpresaEstructura.Enabled := lHayEmpresas;
     _EmpresaInit.Enabled := lHayEmpresas;
     {$endif}
     _EmpresaAsignarLicencias.Enabled := lHayEmpresas;
     _EmpresaPrimera.Enabled := lHayEmpresas;
     _EmpresaAnterior.Enabled := lHayEmpresas;
     _EmpresaSiguiente.Enabled := lHayEmpresas;
     _EmpresaUltima.Enabled := lHayEmpresas;
     _ProcesosPoblar.Enabled := lHayEmpresas;
     {$ifdef INTERBASE}
     _ProcesosRevisar.Enabled := lHayEmpresas and TipoTress;
     _ProcesosMigrar.Enabled := lHayEmpresas and TipoTress;
     _ProcesosMigrarComparte.Enabled := lConnected;
     _SentinelLicencia.Visible := False;
     _SentinelLicencia.Enabled := False;
     {$endif}
     _ProcesosActualizar.Enabled := lHayEmpresas;
     _ProcesosActualizarMultiples.Enabled := lHayEmpresas;
     _ProcesosActualizarComparte.Enabled := lConnected;
     _ProcesosConsolidar.Enabled := lHayEmpresas and TipoTress;
     _ProcesosCompararCatalogos.Enabled := lHayEmpresas and TipoTress;
     _ProcesosTransferir.Enabled := lHayEmpresas and TipoTressProduccion ;
     _PrepararPresupuestos.Enabled := lHayEmpresas and TipoPresupuesto;
     _ExportarReportes.Enabled := lHayEmpresas;
     _ImportarReportes.Enabled := lHayEmpresas;
     _ImportarConceptos.Enabled := lHayEmpresas and TipoTress;
     _ImportarTablas.Enabled := lHayEmpresas;
     {$ifdef MSSQL}
     _ImportarDatos.Enabled := lHayEmpresas;
     _ImportarComparte.Enabled := lConnected;
     _SentinelLicencia.Visible := True;
     _SentinelLicencia.Enabled := True;
     {$endif}
     _ExportarTablas.Enabled := lHayEmpresas;
     _ImportarImagenes.Enabled := lHayEmpresas and TipoTress;
     _SistemaDesbloquearAdministradores.Enabled := lHayComparte;
     with _SentinelRenovar do
     begin
          case FAutoServer.PlataformaAuto of
               ptCorporativa: Enabled := {$ifdef DEBUGSENTINEL}True{$else}False{$endif};
               {$ifdef INTERBASE}
               ptProfesional: Enabled := True;
               {$endif}
               {$ifdef MSSQL}
               ptProfesional: Enabled := True;
               {$endif}
          end;
     end;

     with _SistemaProcessServer do
     begin
          case FAutoServer.PlataformaAuto of
               ptCorporativa: Enabled := True;
               ptProfesional: Enabled := False;
          end;
     end;
     with _EmpresaAsignarLicencias do
     begin
          {$ifdef VALIDA_EMPLEADOS}
          Caption := 'Asignar Licencias De Empleados';
          Hint := 'Asignar Licencias De Empleados A Empresas';
          {$else}
          Caption := 'Empleados Por Empresa';
          Hint := 'Mostrar Empleados Activos e Inactivos En Cada Empresa';
          {$endif}
     end;
     with _SentinelInstalacion do
     begin
          Visible := {$ifdef ANTES_VALIDA_EMPLEADOS}True{$else}False{$endif};
     end;
end;

procedure TEmpresas.dsCompanyDataChange(Sender: TObject; Field: TField);
begin
     SetControls;
end;

procedure TEmpresas.dsCompanyStateChange(Sender: TObject);
begin
     SetControls;
end;

procedure TEmpresas.Editar( ModoEdicion: TDataSetState );
begin
     with TEmpresaEdit.Create( Self ) do
     begin
          try
             DBConfig := dmDBConfig;
             Modo := ModoEdicion;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas.SetWizard( Wizard: TMigrar );
begin
     with Wizard do
     begin
          CompanyType := Self.Tipo;
          with dmDBConfig do
          begin
               TargetAlias := GetCompanyAlias;
               TargetUserName := GetCompanyUserName;
               TargetPassword := GetCompanyPassword;
               SetCompania( GetCompanyName );
          end;
     end;
end;

procedure TEmpresas.ManejaErrorSentinel( const sMensaje: String );
begin
     ZetaDialogo.ZError( '� Error Al Renovar Autorizaci�n !', sMensaje, 0 );
end;

function TEmpresas.EstructuraComparte: Boolean;
begin
     {$ifdef MSSQL}
     with FConfigurarBD.TConfigurarBD.Create( Self ) do
     begin
          try
             Tipo := -1;
             Company := 'COMPARTE';
             Alias :=  dmDBConfig.Registry.AliasComparte;
             Database := dmDBConfig.Registry.Database;
             UserName := dmDBConfig.Registry.UserName;
             Password := dmDBConfig.Registry.Password;
             ShowModal;
             Result := OK;
          finally
                 Free;
          end;
     end;
     {$else}
     Result := True;
     {$endif}
end;

function TEmpresas.GetMaxEmpleados: Integer;
begin
     TAutoServerLoader.LoadSentinelInfo( FAutoServer );
     Result := FAutoServer.Empleados;
end;

{ ***** Eventos del ActionList ******* }

procedure TEmpresas._ArchivoAbrirExecute(Sender: TObject);
begin
     Connect;
end;

procedure TEmpresas._ArchivoCerrarExecute(Sender: TObject);
begin
     Disconnect;
end;

procedure TEmpresas._ArchivoConfigurarExecute(Sender: TObject);
begin
     if FHayProblemas.EditarRegistry then
        Refrescar;
end;

procedure TEmpresas._ArchivoEstructuraComparteExecute(Sender: TObject);
begin
     EstructuraComparte;
end;

procedure TEmpresas._ArchivoStoredProceduresExecute(Sender: TObject);
begin
     with TSPConsulta.Create( Self ) do
     begin
          try
             if Init then
                ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ArchivoBDEExecute(Sender: TObject);
begin
     FHayProblemas.InvocarBDE;
end;

procedure TEmpresas._ArchivoODBCExecute(Sender: TObject);
begin
     ZetaODBCTools.InvokeODBCAdministrator( Handle );
end;

procedure TEmpresas._ArchivoSalirExecute(Sender: TObject);
begin
     Close;
end;

procedure TEmpresas._EmpresaProbarExecute(Sender: TObject);
begin
     with FDBChecker.TDBChecker.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
             if RefrescarShell then
                dmDBConfig.RefrescarCompany;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._EmpresaAgregarExecute(Sender: TObject);
begin
     Editar( dsInsert );
end;

procedure TEmpresas._EmpresaBorrarExecute(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( 'Borrar Compa��a', '� Desea Borrar Esta Compa��a ?', 0, mbNo ) then
     begin
          try
             dmDBConfig.BorraCompany;
          except
                on Error: Exception do
                begin
                     ZetaDialogo.zExcepcion( '� Error Al Borrar Compa��a !', 'Se Encontr� Un Error Al Borrar Compa��a', Error, 0 );
                end;
          end;
     end;
end;

procedure TEmpresas._EmpresaModificarExecute(Sender: TObject);
begin
     Editar( dsBrowse );
end;

procedure TEmpresas._EmpresaEstructuraExecute(Sender: TObject);
{$ifdef MSSQL}
var
     iTipoTemp : Integer;
{$endif}
begin
     {$ifdef MSSQL}
     iTipoTemp := 0;
     with FConfigurarBD.TConfigurarBD.Create( Self ) do
     begin
          try
               Tipo := Ord( Self.Tipo );
               iTipoTemp := Tipo;
               // (JB) Tipo Presupuestos correra los mismos scripts que Tipo Tress
               If ( Tipo = Ord( tcPresupuesto ) ) then
                    Tipo := Ord( tc3Datos );
               with dmDBConfig do
               begin
                    Company := GetCompanyName;
                    Alias := GetCompanyAlias;
                    Database := GetCompanyDatabase;
                    UserName := GetCompanyUserName;
                    Password := GetCompanyPassword;
               end;
               ShowModal;
          finally
               Tipo := iTipoTemp;
               Free;
          end;
     end;
     {$endif}
end;

procedure TEmpresas._EmpresaInitExecute(Sender: TObject);
begin
     {$ifdef MSSQL}
     {$endif}
     dmDBConfig.InicializaComparte:= FALSE;
     with FInitBD.TInitBD.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             {
             Tipo := Ord( Self.Tipo );
             with dmDBConfig do
             begin
                  Company := GetCompanyName;
                  Alias := GetCompanyAlias;
                  Database := GetCompanyDatabase;
                  UserName := GetCompanyUserName;
                  Password := GetCompanyPassword;
             end;
             }
            ShowModal;
            // InicializarDatos( FALSE );
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._EmpresaAsignarLicenciasExecute(Sender: TObject);
begin
     with TLicenseAllocation.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._EmpresaPrimeraExecute(Sender: TObject);
begin
     dsCompany.Dataset.First;
end;

procedure TEmpresas._EmpresaAnteriorExecute(Sender: TObject);
begin
     dsCompany.Dataset.Prior;
end;

procedure TEmpresas._EmpresaSiguienteExecute(Sender: TObject);
begin
     dsCompany.Dataset.Next;
end;

procedure TEmpresas._EmpresaUltimaExecute(Sender: TObject);
begin
     dsCompany.Dataset.Last;
end;

procedure TEmpresas._SistemaDatosExecute(Sender: TObject);
begin
     with TSystemData.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._SistemaProcessServerExecute(Sender: TObject);
begin
     FProcessServer.ShowProcessServerInfo;
end;

procedure TEmpresas._SistemaDesbloquearAdministradoresExecute(Sender: TObject);
begin
     DesbloqueaAdmon := TDesbloqueaAdmon.Create( Self );
     try
        DesbloqueaAdmon.ShowModal;
     finally
            FreeAndNil( DesbloqueaAdmon );
     end;
end;

procedure TEmpresas._SentinelVerExecute(Sender: TObject);
begin
     with TSentinelView.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._SentinelActualizarExecute(Sender: TObject);
begin
     with TSentinelWrite.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
             if ( ModalResult = mrOK ) then
             begin
                  Refrescar;
                  dmDBConfig.EscribeAUTOMOD( FAutoServer ) ;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._SentinelRenovarExecute(Sender: TObject);
var
   oCursor: TCursor;
   {$ifdef DEBUGSENTINEL}
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
   {$endif}
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        {$ifdef DEBUGSENTINEL}
        try
           with FAutoServer do
           begin
                if Cargar then
                begin
                     oZetaProvider := TdmZetaServerProvider.Create( Self );
                     try
                        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
                        try
                           oZetaProvider.EmpresaActiva:= oZetaProvider.Comparte;
                           oLicenseMgr.EmpleadosGetGlobal;
                           OnSetData := oLicenseMgr.AutoSetData;
                           OnGetDataAdditional := oLicenseMgr.AutoOnGetDataAdditional;
                           try
                              EscribirAutorizacion;
                              ZetaDialogo.zInformation( '� Autorizaci�n Renovada !', 'La Autorizaci�n Del Sistema Fu� Renovada', 0 );
                              Self.Refrescar;
                              dmDBConfig.EscribeAUTOMOD( FAutoServer );
                           finally
                                  OnSetData := nil;
                                  onGetDataAdditional := nil;
                           end;
                        finally
                               FreeAndNil( oLicenseMgr );
                        end;
                     finally
                            FreeAndNil( oZetaProvider );
                     end;
                end
     {$ifndef SENTINELVIRTUAL};{$else}
                else
                begin
                     FSentinelRegistry.FEmpleadosConteoVirtual := 0;
                     if FSentinelRegistry.ExisteSentinelVirtual then
                     begin
                         if TAutoServerLoader.CargarVirtualSentinelInfo( FAutoServer, True) then
                         begin
                              ZetaDialogo.zInformation( '� Autorizaci�n con Sentinel Virtual Renovada !', 'La Autorizaci�n con Sentinel Virtual Del Sistema Fu� Renovada', 0 );
                              Self.Refrescar;
                              dmDBConfig.EscribeAUTOMOD( FAutoServer );
                         end;
                     end;
                end;
     {$endif}
           end;
        except
              on Error: Exception do
              begin
                   ManejaErrorSentinel( Error.Message );
              end;
        end;
        {$else}
        FAutoServer.Cargar;
        if DSentinelWrite.RenovarAutorizacion( ManejaErrorSentinel ) then
        begin
             ZetaDialogo.zInformation( '� Autorizaci�n Renovada !', 'La Autorizaci�n Del Sistema Fu� Renovada', 0 );
             Refrescar;
        end;
        {$endif}
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEmpresas._SentinelInstalacionExecute(Sender: TObject);
begin
     with TSentinelInstall.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
             if ( ModalResult = mrOK ) then
                Refrescar;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ProcesosPoblarExecute(Sender: TObject);
begin
     with TPoblarDatos.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ProcesosRevisarExecute(Sender: TObject);
begin
     {$IFDEF INTERBASE}
     with TRevisarDatos.Create( Self ) do
     begin
          try
             CompanyType := Self.Tipo;
             ShowModal;
          finally
                 Free;
          end;
     end;
     {$ENDIF}
end;

procedure TEmpresas._ProcesosMigrarExecute(Sender: TObject);
begin
     {$IFDEF INTERBASE}
     with TMigrarDatos.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
     {$ENDIF}
end;

procedure TEmpresas._ProcesosMigrarComparteExecute(Sender: TObject);
begin
     {$IFDEF INTERBASE}
     with TMigrarComparte.Create( Self ) do
     begin
          try
             CompanyType := Self.Tipo;
             ShowModal;
             if Migrado then
                Self.Refrescar;
          finally
                 Free;
          end;
     end;
     {$ENDIF}
end;

procedure TEmpresas._ProcesosActualizarExecute(Sender: TObject);
begin
     with TPatchDatos.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             if Init then
                ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ProcesosActualizarComparteExecute(Sender: TObject);
begin
     try
        Disconnect;
        with TPatchComparte.Create( Self ) do
        begin
             try
                CompanyType := Self.Tipo;
                if Init then
                   ShowModal;
             finally
                    Free;
             end;
        end;
     finally
            Connect;
     end;
end;

procedure TEmpresas._ProcesosConsolidarExecute(Sender: TObject);
begin
     with TConsolidar.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             LimiteEmpleados := MaxEmpleados;
             if InitListaEmpresas then
                ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ProcesosCompararCatalogosExecute(Sender: TObject);
begin
     with TComparaTablas.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ProcesosTransferirExecute(Sender: TObject);
begin
     with TTransferirEmpleados.Create( Self ) do
     begin
          try
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ExportarReportesExecute(Sender: TObject);
begin
     with TExportarReportes.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ImportarReportesExecute(Sender: TObject);
begin
     with TImportarReportes.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ImportarConceptosExecute(Sender: TObject);
begin
     with TImportarConceptos.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ExportarTablasExecute(Sender: TObject);
begin
     with TExportarTablas.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ImportarTablasExecute(Sender: TObject);
begin
     with TImportarTablas.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._ImportarDatosExecute(Sender: TObject);
begin
     {$IFDEF MSSQL}
     with TWizardImportar.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
     {$ENDIF}
end;

procedure TEmpresas._ImportarComparteExecute(Sender: TObject);
begin
     {$IFDEF MSSQL}
     with TWizardImportar.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             MigrarComparte := TRUE;
             ShowModal;
             if Migrado then
                Self.Refrescar;
          finally
                 Free;
          end;
     end;
     {$ENDIF}
end;

procedure TEmpresas._ImportarImagenesExecute(Sender: TObject);
begin
     with TImportarImagenes.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._AyudaContenidoExecute(Sender: TObject);
begin
     Application.HelpCommand( HELP_FINDER, 0 );
end;

procedure TEmpresas._AyudaUsandoAyudaExecute(Sender: TObject);
begin
     Application.HelpContext( AyudaUsandoAyuda.HelpContext );
     {
     Application.HelpCommand( HELP_HELPONHELP, 0 );
     }
end;

procedure TEmpresas._AyudaNuevoExecute(Sender: TObject);
const
     K_HELP_CAMBIOS = 'Version.chm';
var
   sHelpFile: String;
begin
     with Application do
     begin
          sHelpFile := HelpFile;
          try
             HelpFile := K_HELP_CAMBIOS;
             HelpCommand( HELP_FINDER, 0 );
          finally
             HelpFile := sHelpFile;
          end;
     end;
end;

procedure TEmpresas._AyudaAcercaDeExecute(Sender: TObject);
begin
     FAutoClasses.InitAuto;
     try
        TAutoServerLoader.LoadSentinelInfo( FAutoServer );
        FAutoClasses.Autorizacion.Cargar( FAutoServer.Exportar( VERSION_ARCHIVO ) );

        with TTressCFGAcercaDe.Create( Self ) do
        begin
             try
                ShowModal;
             finally
                    Free;
             end;
        end;
     finally
            FAutoClasses.ClearAuto;
     end;
end;

procedure TEmpresas._AyudaLicenciaExecute(Sender: TObject);
var
   FLicencia: TLicenciaUsuario;
begin
     FLicencia := TLicenciaUsuario.Create( Self );
     try
        Flicencia.ShowModal;
     finally
            FreeAndNil( FLicencia );
     end;
end;

procedure TEmpresas._ProcesosComparteInitExecute(Sender: TObject);
begin
     {$ifdef MSSQL}
     {$endif}
     dmDBConfig.InicializaComparte:= TRUE;
     with FInitBD.TInitBD.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             ShowModal;
            // InicializarDatos( TRUE );
          finally
                 dmDBConfig.InicializaComparte:= FALSE;
                 Free;
          end;
     end;
end;

function TEmpresas.ActualizaComparte: Boolean;
begin
     Result:= FALSE;
     try
        Disconnect;
        with TPatchComparte.Create( Self ) do
        begin
             try
                CompanyType := Self.Tipo;
                if Init then
                   ShowModal;
                Result:= OK;
             finally
                    Free;
             end;
        end;
     finally
            if ( Result ) then
               Connect;
     end;
end;


procedure TEmpresas._PrepararPresupuestosExecute(Sender: TObject);
begin
     //(JB) Se agrega Proceso para preparar base de datos para presupuestos
{$IFDEF MSSQL}
     if ( Self.Tipo = tcPresupuesto ) then
          with TWizardPresupuesto.Create( Self ) do
          begin
               try
                    ShowModal;
               finally
                    Free;
               end;
          end
     else
          ZetaDialogo.zInformation( Self.Caption , 'El proceso de Preparar Base de Presupuestos es �nicamente para Empresas de tipo Presupuesto', 0 );
{$ENDIF}
end;

procedure TEmpresas._ProcesosActualizarMultiplesExecute(Sender: TObject);
begin
  with TPatchDatosMultiple.Create( Self ) do
     begin
          try
             SetWizard( GetPointer );
             if Init then
                ShowModal;
          finally
                 Free;
          end;
     end;
end;

procedure TEmpresas._SentinelLicenciaExecute(Sender: TObject);
begin
{$ifdef MSSQL}
        with TSentinelLicenciaWrite.Create( Self ) do
        begin
            try
               //AutoServer := FAutoServer;
               ShowModal;
            finally
                   Free;
            end;
        end;
{$endif}
end;

end.



