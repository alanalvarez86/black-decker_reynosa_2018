unit FReporteExport;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, DBCtrls, Grids, DBGrids, ComCtrls, StdCtrls,
     Buttons, ExtCtrls, Db,
     FMigrar,
     ZetaWizard, jpeg;

type
  TExportarReportes = class(TMigrar)
    DirectorioGB: TGroupBox;
    PathLBL: TLabel;
    PathSeek: TSpeedButton;
    Path: TEdit;
    ListaReportes: TTabSheet;
    ListaReportesGB: TGroupBox;
    DBGrid: TDBGrid;
    DBNavigator: TDBNavigator;
    dsReportes: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    function InitListaReportes: Boolean;
  public
    { Public declarations }
  end;

var
  ExportarReportes: TExportarReportes;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaMigrar,
     ZetaDialogo,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TExportarReportes.FormCreate(Sender: TObject);
begin
     FParadoxPath := Path;
     dmMigracion := TdmReportes.Create( Self );
     Path.Text := ZetaTressCFGTools.SetFileNameDefaultPath( '' );
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'ExportarReportes.log' );
     inherited;
     HelpContext := H00004_Importar_y_Exportar_reportes;
end;

procedure TExportarReportes.FormShow(Sender: TObject);
begin
     inherited;
     InitListaReportes;
end;

procedure TExportarReportes.FormDestroy(Sender: TObject);
begin
     inherited;
     TdmReportes( dmMigracion ).Free;
end;

function TExportarReportes.InitListaReportes: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             OpenReportTable( dsReportes );
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Tabla De Reportes !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

procedure TExportarReportes.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               if Wizard.EsPaginaActual( DirectorioDOS ) then
                  CanMove := ValidaDirectorioParadox
               else
                   if Wizard.EsPaginaActual( ListaReportes ) then
                   begin
                        if ( DBGrid.SelectedRows.Count = 0 ) then
                        begin
                             CanMove := False;
                             zWarning( Caption, '� No Se Escogi� Ning�n Reporte !', 0, mbOK );
                             ActiveControl := DBGrid;
                        end
                   end
                   else
                       CanMove := True;
          end;
     end;
end;

procedure TExportarReportes.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( GetExportSteps );
          StartProcess;
          SourceSharedPath := Path.Text;
          lOk := ExportarReportes( DBGrid.SelectedRows );
          EndProcess;
     end;
     EndLog;
     inherited;
end;

end.
