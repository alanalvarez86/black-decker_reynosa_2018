unit DMigrarDatos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, DBTables, Printers, IniFiles,
     TMultiP,
     DMigrar,
     ZetaTipoEntidad,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaMigrar,
     ZetaEntidad;

{$define QUINCENALES}
{.$undefine QUINCENALES}

type
  eBaseReporte = ( eREPORTES, eREPCUR, eREPASIS, eREPTARJ, eESPECIAL, eREPSAR, eFORMA, eSUPERVISOR );
  eFiltroEmpleados = ( eEMPLEADOS, eBAJAS, eTODOS );
  TInfo = record
        Titulo: String;
        Ancho: Integer;
        Mascara: String;
  end;
  TdmMigrarDatos = class(TdmMigrar)
    tqUno: TQuery;
    tqDos: TQuery;
    tqTres: TQuery;
    tqCuatro: TQuery;
    Global: TZetaMigrar;
    Folio: TZetaMigrar;
    Extra1: TZetaMigrar;
    Extra2: TZetaMigrar;
    Extra3: TZetaMigrar;
    Extra4: TZetaMigrar;
    EdoCivil: TZetaMigrar;
    Vive_En: TZetaMigrar;
    Vive_Con: TZetaMigrar;
    Entidad: TZetaMigrar;
    Nivel1: TZetaMigrar;
    Nivel2: TZetaMigrar;
    Nivel3: TZetaMigrar;
    Nivel4: TZetaMigrar;
    Nivel5: TZetaMigrar;
    Nivel6: TZetaMigrar;
    Nivel7: TZetaMigrar;
    Nivel8: TZetaMigrar;
    Nivel9: TZetaMigrar;
    {$ifdef ACS}
    Nivel10: TZetaMigrar;
    Nivel11: TZetaMigrar;
    Nivel12: TZetaMigrar;
    {$endif}
    Transpor: TZetaMigrar;
    Estudios: TZetaMigrar;
    TCurso: TZetaMigrar;
    Maestro: TZetaMigrar;
    CalCurso: TZetaMigrar;
    Moneda: TZetaMigrar;
    Mot_Baja: TZetaMigrar;
    Puesto: TZetaMigrar;
    Evento: TZetaMigrar;
    Entrena: TZetaMigrar;
    Clasifi: TZetaMigrar;
    Festivo: TZetaMigrar;
    Concepto: TZetaMigrar;
    Contrato: TZetaMigrar;
    Horario: TZetaMigrar;
    Turno: TZetaMigrar;
    TAhorro: TZetaMigrar;
    TPresta: TZetaMigrar;
    TKardex: TZetaMigrar;
    Querys: TZetaMigrar;
    Inciden: TZetaMigrar;
    RPatron: TZetaMigrar;
    OtrasPer: TZetaMigrar;
    PRiesgo: TZetaMigrar;
    SSocial: TZetaMigrar;
    Prestaci: TZetaMigrar;
    Colabora: TZetaMigrar;
    Periodo: TZetaMigrar;
    ACar_Abo: TZetaMigrar;
    PCar_Abo: TZetaMigrar;
    Ahorro: TZetaMigrar;
    Prestamo: TZetaMigrar;
    Kardex: TZetaMigrar;
    Faltas: TZetaMigrar;
    Incapaci: TZetaMigrar;
    Permiso: TZetaMigrar;
    Vacacion: TZetaMigrar;
    KarCurso: TZetaMigrar;
    Curso: TZetaMigrar;
    Liq_Emp: TZetaMigrar;
    Liq_IMSS: TZetaMigrar;
    Liq_Mov: TZetaMigrar;
    Nomina: TZetaMigrar;
    Acumula: TZetaMigrar;
    Movimien: TZetaMigrar;
    Ausencia: TZetaMigrar;
    Checadas: TZetaMigrar;
    Sal_Min: TZetaMigrar;
    Ley_Imss: TZetaMigrar;
    Numerica: TZetaMigrar;
    Art_80: TZetaMigrar;
    Diccion: TZetaMigrar;
    TFijas: TZetaMigrar;
    Kar_Fija: TZetaMigrar;
    tqCinco: TQuery;
    tqSeis: TQuery;
    RepRH: TZetaMigrar;
    RepTarj: TZetaMigrar;
    RepAsist: TZetaMigrar;
    RepCursos: TZetaMigrar;
    Especial: TZetaMigrar;
    Forma2: TZetaMigrar;
    RepSar: TZetaMigrar;
    Polizas: TZetaMigrar;
    RepSuper: TZetaMigrar;
    TCambio: TZetaMigrar;
    Mot_Auto: TZetaMigrar;
    Conteo: TZetaMigrar;
    spActualizaDiccion: TStoredProc;
    CafCome: TZetaMigrar;
    CafRegla: TZetaMigrar;
    Session2: TSession;
    procedure dmMigrarCreate(Sender: TObject);
    procedure dmMigrarDestroy(Sender: TObject);
    procedure GlobalOpenDatasets(Sender: TObject);
    procedure GlobalCloseDatasets(Sender: TObject);
    procedure GlobalFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure GlobalMoveExcludedFields(Sender: TObject);
    procedure FolioMoveExcludedFields(Sender: TObject);
    procedure Art_80FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure Art_80MoveExcludedFields(Sender: TObject);
    procedure Art_80CloseDatasets(Sender: TObject);
    procedure NumericaMoveExcludedFields(Sender: TObject);
    procedure NumericaCloseDatasets(Sender: TObject);
    procedure RiesgoMoveExcludedFields(Sender: TObject);
    procedure Sal_MinMoveExcludedFields(Sender: TObject);
    procedure Ley_ImssMoveExcludedFields(Sender: TObject);
    procedure Extra1MoveExcludedFields(Sender: TObject);
    procedure MaestroMoveExcludedFields(Sender: TObject);
    procedure ColaboraMoveExcludedFields(Sender: TObject);
    procedure ColaboraMoveOtherTables(Sender: TObject);
    procedure ColaboraCloseDatasets(Sender: TObject);
    procedure ColaboraOpenDatasets(Sender: TObject);
    procedure TAhorroMoveExcludedFields(Sender: TObject);
    procedure TPrestaMoveExcludedFields(Sender: TObject);
    procedure ACar_AboFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ACar_AboMoveExcludedFields(Sender: TObject);
    procedure PCar_AboFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure PCar_AboMoveExcludedFields(Sender: TObject);
    procedure AhorroMoveExcludedFields(Sender: TObject);
    procedure AhorroFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure AhorroOpenDatasets(Sender: TObject);
    procedure AhorroCloseDatasets(Sender: TObject);
    procedure PrestamoMoveExcludedFields(Sender: TObject);
    procedure PrestamoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure PrestamoOpenDatasets(Sender: TObject);
    procedure PrestamoCloseDatasets(Sender: TObject);
    procedure AcumulaMoveExcludedFields(Sender: TObject);
    procedure AusenciaMoveExcludedFields(Sender: TObject);
    procedure ChecadasFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ChecadasMoveExcludedFields(Sender: TObject);
    procedure FaltasMoveExcludedFields(Sender: TObject);
    procedure IncapaciFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure IncapaciMoveExcludedFields(Sender: TObject);
    procedure KardexFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure KardexMoveExcludedFields(Sender: TObject);
    procedure KardexOpenDatasets(Sender: TObject);
    procedure KardexCloseDatasets(Sender: TObject);
    procedure Liq_EmpMoveExcludedFields(Sender: TObject);
    procedure Liq_IMSSMoveExcludedFields(Sender: TObject);
    procedure Liq_MovMoveExcludedFields(Sender: TObject);
    procedure Liq_MovOpenDatasets(Sender: TObject);
    procedure Liq_MovCloseDatasets(Sender: TObject);
    procedure MovimienMoveExcludedFields(Sender: TObject);
    procedure ClasifiMoveExcludedFields(Sender: TObject);
    procedure ConceptoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure ConceptoMoveExcludedFields(Sender: TObject);
    procedure ConceptoAdditionalRows(Sender: TObject);
    procedure ContratoMoveExcludedFields(Sender: TObject);
    procedure CursoMoveExcludedFields(Sender: TObject);
    procedure CursoOpenDatasets(Sender: TObject);
    procedure CursoCloseDatasets(Sender: TObject);
    procedure Mot_BajaMoveExcludedFields(Sender: TObject);
    procedure EventoMoveExcludedFields(Sender: TObject);
    procedure FestivoMoveExcludedFields(Sender: TObject);
    procedure HorarioMoveExcludedFields(Sender: TObject);
    procedure IncidenMoveExcludedFields(Sender: TObject);
    procedure KarCursoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure KarCursoMoveExcludedFields(Sender: TObject);
    procedure MonedaMoveExcludedFields(Sender: TObject);
    procedure PRiesgoMoveExcludedFields(Sender: TObject);
    procedure PRiesgoOpenDatasets(Sender: TObject);
    procedure PRiesgoCloseDatasets(Sender: TObject);
    procedure PuestoMoveExcludedFields(Sender: TObject);
    procedure QuerysMoveExcludedFields(Sender: TObject);
    procedure RPatronMoveExcludedFields(Sender: TObject);
    procedure SSocialMoveExcludedFields(Sender: TObject);
    procedure TKardexMoveExcludedFields(Sender: TObject);
    procedure TurnoMoveExcludedFields(Sender: TObject);
    procedure TurnoAdditionalRows(Sender: TObject);
    procedure NominaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure NominaMoveExcludedFields(Sender: TObject);
    procedure OtrasPerMoveExcludedFields(Sender: TObject);
    procedure PeriodoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure PeriodoMoveExcludedFields(Sender: TObject);
    procedure PeriodoCloseDatasets(Sender: TObject);
    procedure PermisoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure PermisoMoveExcludedFields(Sender: TObject);
    procedure PrestaciMoveExcludedFields(Sender: TObject);
    procedure PrestaciOpenDatasets(Sender: TObject);
    procedure PrestaciCloseDatasets(Sender: TObject);
    procedure VacacionMoveExcludedFields(Sender: TObject);
    procedure VacacionCloseDatasets(Sender: TObject);
    procedure CalCursoMoveExcludedFields(Sender: TObject);
    procedure EntrenaOpenDatasets(Sender: TObject);
    procedure EntrenaCloseDatasets(Sender: TObject);
    procedure EntrenaMoveExcludedFields(Sender: TObject);
    procedure EntrenaMoveOtherTables(Sender: TObject);
    procedure Kar_FijaOpenDatasets(Sender: TObject);
    procedure Kar_FijaCloseDatasets(Sender: TObject);
    procedure Kar_FijaMoveExcludedFields(Sender: TObject);
    procedure Kar_FijaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure RepRHOpenDatasets(Sender: TObject);
    procedure RepRHCloseDatasets(Sender: TObject);
    procedure RepRHFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure RepTarjOpenDatasets(Sender: TObject);
    procedure RepAsistOpenDatasets(Sender: TObject);
    procedure RepCursosOpenDatasets(Sender: TObject);
    procedure RepSarOpenDatasets(Sender: TObject);
    procedure EspecialOpenDatasets(Sender: TObject);
    procedure Forma2OpenDatasets(Sender: TObject);
    procedure Forma2FilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure PolizasOpenDatasets(Sender: TObject);
    procedure PolizasCloseDatasets(Sender: TObject);
    procedure PolizasFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure DiccionCloseDatasets(Sender: TObject);
    procedure RepSuperOpenDatasets(Sender: TObject);
    procedure TCambioMoveExcludedFields(Sender: TObject);
    procedure ConteoMoveExcludedFields(Sender: TObject);
    procedure CafReglaMoveExcludedFields(Sender: TObject);
    procedure CafComeMoveExcludedFields(Sender: TObject);
    procedure ACar_AboCloseDatasets(Sender: TObject);
    procedure PCar_AboCloseDatasets(Sender: TObject);
    procedure NominaCloseDatasets(Sender: TObject);
    procedure Forma2CloseDatasets(Sender: TObject);
    procedure TPrestaCloseDatasets(Sender: TObject);
  private
    { Private declarations }
    PMultiImage: TPMultiImage;
    FAhorro: TStrings;
    FConcepto: TStrings;
    FEstado: String;
    FAnio: Integer;
    FAcumulados: Integer;
    FDirSuper: String;
    FDirFotos: String;
    FDirDocumentos: String;
    FInfOldField: String;
    FNiveles: Integer;
    FNivelIni1: Integer;
    FNivelLen1: Integer;
    FNivelIni2: Integer;
    FNivelLen2: Integer;
    FNivelIni3: Integer;
    FNivelLen3: Integer;
    FNivelIni4: Integer;
    FNivelLen4: Integer;
    FNivelIni5: Integer;
    FNivelLen5: Integer;
    FNivelIni6: Integer;
    FNivelLen6: Integer;
    FNivelIni7: Integer;
    FNivelLen7: Integer;
    FNivelIni8: Integer;
    FNivelLen8: Integer;
    FNivelIni9: Integer;
    FNivelLen9: Integer;
    {$ifdef ACS}
    FNivelIni10: Integer;
    FNivelLen10: Integer;
    FNivelIni11: Integer;
    FNivelLen11: Integer;
    FNivelIni12: Integer;
    FNivelLen12: Integer;
    {$endif}
    FReporteOK: Boolean;
    FReporte: String;
    FTipoForma : string;
    FCodigoReporte: Integer;
    FPosicion: Integer;
    FBaseReporte: eBaseReporte;
    FNombreTabla : string;
    FEntidad: TipoEntidad;
    FMaster : TipoEntidad;
    FTipoSalida : Integer;
    FEncabezado : Boolean;
    FMascaraNoCeros : Boolean;
    sMascaraNoCeros: string;
    FOffset : integer;
    FClasificacion: eClasifiReporte;
    FEsCarta : Boolean;
    FFiltroEmpleados: eFiltroEmpleados;
    FAgrupaEmpleado: Boolean;
    FRecalcularKardex: Boolean;
    FMinima: TDate;
    FPrestaxCreated: Boolean;
    function LeeCodigoEmpleado: TNumEmp;
    function GetDirFotos: String;
    function GetDirDocumentos: String;
    function GetInfo( sCampo: String; Entidad: TipoEntidad ): TInfo;
    function GetAncho( const iAncho: Integer; const sFormula: String; const Entidad: TipoEntidad ): Integer;
    function EsAhorro( const sTipo: String ): Boolean;
    function ConvierteConcepto( sValor: String ): Integer;
    function SiNo( const sValor: String ): String;
    function GetPosicion: integer;
    function AnalizaFormula( sFormula : string; const EmpleadoSTR : Boolean; var iAncho : integer  ) : string;
    function ChecaFormula( const sFormula, sMensaje: String; const lValor: Boolean ): String;
    function VerificaFormula( const sFormula, sMensaje: String; const lValor: Boolean ): String;
    function ChecaAnchoFormula( const sFormula, sMensaje: String ): String;
    function PuedeMigrarKardex( const Value: TDate; const iEmpleado: TNumEmp ): Boolean;
    procedure AgregaCampo( const iPosicion, iSubPos, iCalculado,
                                 iAncho, iTField, iShow, iAlinea, iColor, iOperacion: Integer;
                                 sFormula, sTitulo, sMascara, sRequiere,
                                 sDescripcion, sBold, sItalic, sSubraya, sStrike: String;
                           const TipoCampo: eTipoCampo;
                           const iEntidad: TipoEntidad;
                           const FDeDiccion : Boolean );
    procedure FiltroFormas( const TipoFiltro : eTipoRango;
                            const sCampo: string;
                            sFiltro, sDescrip : string;
                            const iEntidad : TipoEntidad;
                            const iOper : integer;
                            const iTipo : eTipoGlobal;
                            const Ancho : integer );
    procedure MigrarSiNo( const sTargetField, sSourceField: String );
    procedure MigrarUsuario( const sTargetField, sSourceField: String );
    procedure MigrarBooleanos( const sTargetField, sSourceField: String );
    procedure MigrarNiveles( const sTargetField, sSourceField: String );
    procedure MigrarNivelesNull( const sTargetField, sSourceField: String );
    procedure MigrarFecha( const sTargetField: String; const Value: TDate );
    procedure MigrarFechaNull( const sTargetField: String; const Value: TDate );
    procedure MigrarFechaKey( const sTargetField: String; const Value: TDate );
    procedure MigrarFechaKardex( const sTargetField: String; const Value: TDate );
    procedure MigrarStringKey( const sTargetField, sValue: String );
    procedure MigrarStringKeyNull( const sTargetField, sValue: String );
    procedure MigrarIntegerKeyNull( const sTargetField: String; iValue: Integer );
    procedure MigrarMemo( const sTargetField, sValue: String );
    procedure MigrarAsInteger( const sTargetField, sSourceField: String );
    procedure MigrarCodigoEmpleado;
    procedure MigrarEmpleado( const iEmpleado: TNumEmp );
    procedure ReportesOpenDatasets;
    procedure SetDirSuper( const Value: String );
    procedure SetDirFotos( const Value: String );
    procedure SetDirDocumentos( const Value: String );
    procedure SetNivelStructure( const sValor: String );
    procedure AgregarImagenes( const iCodigo: TNumEmp );
//    procedure ActualizaConteo;
  protected
    { Protected declarations }
    procedure MigrarAdicionales; override;
    procedure SetEsVer260( const lValue: Boolean ); override;
  public
    { Public declarations }
        function CierraBDReceptora: Boolean;
    property Anio: Integer read FAnio write FAnio;
    property Acumulados: Integer read FAcumulados write FAcumulados;
    property DirFotos: String read GetDirFotos write SetDirFotos;
    property DirDocumentos: String read GetDirDocumentos write SetDirDocumentos;
    property DirSupervisores: String read FDirSuper write SetDirSuper;
    property Estado: String read FEstado write FEstado;
    property FechaMinima: TDate read FMinima write FMinima;
    property RecalcularKardex: Boolean read FRecalcularKardex write FRecalcularKardex;
    procedure LlenaListaEstados( Lista: TStrings; const sDirectory: String );
  end;

var
  dmMigrarDatos: TdmMigrarDatos;

implementation

uses ZetaQRExpr,
     ZetaCommonTools,
     ZetaTressCFGTools,
     ZGlobalTress,
     ZConvierteFormula;

const
     K_MARCA = 'MIGRACION';
     K_EMPRESA = 'Grupo Nivel Empresa';
     MOTIVO_DIA =  'FJPCZ9VIGANQHMW';
     MOTIVO_HORA = 'OE23RDLSTUXY';
     K_ANCHO_MAXIMO = 255;
     K_CARGO = 'C';
     K_MASCULINO = 'M';
     K_FEMENINO = 'F';
     K_STRING_NULO = '';
     K_EMPLEADO_CERO = '    0';
     K_ES_PERMISO = '2';
     K_PRETTY='PRETTY_NAM';
     K_CARTA_PLANTILLA = 'Para Estos Reportes Usar CARTAPLANTILLA:';
     K_ASISTENCIA = 'Revisar Funcionamiento De Estos Reportes DE ASISTENCIA:';
     K_REPORTE_CON_FORMULAS_DESCONTINUADAS = 'Para Estos Reportes Crear PLANTILLAS:';      

     aTablas : Array[1..16] of string =
     ('COLABORA.','AUSENCIA.','CHECADAS.',
     'NOMINA.','KARDEX.','INCAPACI.','KARCURSO.',
     'VACACION.','MOVIMIEN.','ACUMULA.','AHORRO.',
     'CAR_ABO.','KAR_CURSO.','LIQ_EMP.','LIQ_MOV.','');

     aEmpleados : array[1..{$ifdef ACS}124{$else}121{$endif}] of string =
     ('CB_ACTIVO','CB_APE_MAT','CB_APE_PAT','CB_AUTOSAL','CB_BAN_ELE','CB_CALLE',
     'CB_CARRERA','CB_CHECA','CB_CIUDAD','CB_CLASIFI','CB_CODIGO','CB_CODPOST','CB_COLONIA',
     'CB_CONTRAT','CB_CREDENC','CB_CURP','CB_DER_FEC','CB_DER_GOZ','CB_DER_PAG',
     'CB_EDO_CIV','CB_ESTADO','CB_ESTUDIO','CB_EST_HOR','CB_EST_HOY','CB_EVALUA',
     'CB_EXPERIE','CB_FAC_INT','CB_FEC_ANT','CB_FEC_BAJ','CB_FEC_BSS','CB_FEC_CON',
     'CB_FEC_INC','CB_FEC_ING','CB_FEC_INT','CB_FEC_NAC','CB_FEC_PER','CB_FEC_RES',
     'CB_FEC_REV','CB_FEC_SAL','CB_FEC_VAC','CB_G_FEC_1','CB_G_FEC_2','CB_G_FEC_3',
     'CB_G_LOG_1','CB_G_LOG_2','CB_G_LOG_3','CB_G_NUM_1','CB_G_NUM_2','CB_G_NUM_3',
     'CB_G_TAB_1','CB_G_TAB_2','CB_G_TAB_3','CB_G_TAB_4','CB_G_TEX_1','CB_G_TEX_2',
     'CB_G_TEX_3','CB_G_TEX_4','CB_HABLA','CB_IDIOMA','CB_INFCRED','CB_INFMANT','CB_INFTASA',
     'CB_INFTIPO','CB_INF_INI','CB_INF_OLD','CB_LAST_EV','CB_LA_MAT','CB_LUG_NAC','CB_MAQUINA',
     'CB_MED_TRA','CB_MOT_BAJ','CB_NACION','CB_NEXT_EV','CB_NIVEL1','CB_NIVEL2','CB_NIVEL3',
     'CB_NIVEL4','CB_NIVEL5','CB_NIVEL6','CB_NIVEL7','CB_NIVEL8','CB_NIVEL9',
     {$ifdef ACS}'CB_NIVEL10','CB_NIVEL11','CB_NIVEL12',{$endif}
     'CB_NOMBRES','CB_NOMNUME',
     'CB_NOMTIPO','CB_NOMYEAR','CB_OLD_INT','CB_OLD_SAL','CB_PASAPOR','CB_PATRON','CB_PER_VAR','CB_PRE_INT',
     'CB_PUESTO','CB_RANGO_S','CB_RFC','CB_SALARIO','CB_SAL_INT','CB_SAL_TOT',
     'CB_SEGSOC','CB_SEXO','CB_TABLASS','CB_TEL','CB_TIP_REV','CB_TOT_GRA','CB_TURNO',
     'CB_VIVECON','CB_VIVEEN','CB_V_GOZO','CB_V_PAGO','CB_ZONA','CB_ZONA_GE','CB_MUNICIP','CB_TSANGRE','CB_ALERGIA',
     'CB_BRG_ACT','CB_BRG_TIP','CB_BRG_ROL','CB_BRG_JEF','CB_BRG_CON','CB_BRG_PRA','CB_BRG_NOP'

      );



     aKardex : array[1..{$ifdef ACS}37{$else}34{$endif}] of string = (
     'CB_CODIGO','CB_AUTOSAL','CB_CLASIFI',
     'CB_CONTRAT','CB_FAC_INT','CB_FEC_CON',
     'CB_FEC_INT','CB_FEC_REV','CB_TURNO',
     'CB_MOT_BAJ','CB_OLD_INT','CB_OLD_SAL',
     'CB_PATRON','CB_PER_VAR','CB_PRE_INT',
     'CB_PUESTO','CB_RANGO_S','CB_SAL_INT',
     'CB_SAL_TOT','CB_SALARIO','CB_TABLASS',
     'CB_TOT_GRA','CB_ZONA_GE','CB_NIVEL1',
     'CB_NIVEL2','CB_NIVEL3','CB_NIVEL4',
     'CB_NIVEL5','CB_NIVEL6','CB_NIVEL7',
     'CB_NIVEL8','CB_NIVEL9',
     {$ifdef ACS}'CB_NIVEL10','CB_NIVEL11','CB_NIVEL12',{$endif}
     'CB_FEC_ING',
     'CB_FEC_ANT' );

     aAusencia :  array[1..{$ifdef ACS}16{$else}13{$endif}] of string =('CB_CODIGO',
     'CB_CLASIFI','CB_TURNO','CB_PUESTO',
     'CB_NIVEL1','CB_NIVEL2','CB_NIVEL3',
     'CB_NIVEL4','CB_NIVEL5',
     'CB_NIVEL6','CB_NIVEL7',
     'CB_NIVEL8','CB_NIVEL9'{$ifdef ACS},'CB_NIVEL10','CB_NIVEL11','CB_NIVEL12'{$endif});

     aKarCurso :  array[1..{$ifdef ACS}16{$else}13{$endif}] of string =(
     'CB_CODIGO','CB_CLASIFI','CB_TURNO',
     'CB_PUESTO','CB_NIVEL1','CB_NIVEL2',
     'CB_NIVEL3','CB_NIVEL4','CB_NIVEL5',
     'CB_NIVEL6','CB_NIVEL7','CB_NIVEL8','CB_NIVEL9'{$ifdef ACS},'CB_NIVEL10','CB_NIVEL11','CB_NIVEL12'{$endif});

     aNominas : array[1..{$ifdef ACS}80{$else}77{$endif}] of string=
    ('CB_CLASIFI','CB_CODIGO','CB_NIVEL1','CB_NIVEL2','CB_NIVEL3','CB_NIVEL4',
     'CB_NIVEL5','CB_NIVEL6','CB_NIVEL7','CB_NIVEL8','CB_NIVEL9',
     {$ifdef ACS}'CB_NIVEL10','CB_NIVEL11','CB_NIVEL12',{$endif}
     'CB_PATRON','CB_PUESTO',
     'CB_SALARIO','CB_SAL_INT','CB_TURNO','CB_ZONA_GE','NO_ADICION','NO_DEDUCCI','NO_DES_TRA',
     'NO_DIAS_AG','NO_DIAS_AJ','NO_DIAS_AS','NO_DIAS_CG','NO_DIAS_EM','NO_DIAS_FI',
     'NO_DIAS_FJ','NO_DIAS_FV','NO_DIAS_IN','NO_DIAS_NT','NO_DIAS_OT','NO_DIAS_RE','NO_DIAS_SG',
     'NO_DIAS_SS','NO_DIAS_SU','NO_DIAS_VA','NO_DOBLES','NO_D_TURNO','NO_EXENTAS','NO_EXTRAS',
     'NO_FEC_PAG','NO_FES_PAG','NO_FES_TRA','NO_FOLIO_1','NO_FOLIO_2','NO_FOLIO_3','NO_FOLIO_4',
     'NO_FOLIO_5','NO_FUERA','NO_HORAS','NO_HORA_CG','NO_HORA_PD','NO_HORA_SG','NO_IMP_CAL',
     'NO_JORNADA','NO_LIQUIDA','NO_NETO','NO_OBSERVA','NO_PERCEPC','NO_PER_CAL','NO_PER_MEN','NO_PER_ISN',
     'NO_STATUS','NO_TARDES','NO_TOT_PRE','NO_TPONOM','NO_TRIPLES','NO_USER_RJ','NO_USR_PAG',
     'NO_VAC_TRA','NO_X_CAL','NO_X_ISPT','NO_X_MENS','PE_NUMERO','PE_TIPO','PE_YEAR',
     'US_CODIGO');
     aLiqImss : array[1..30] of string=
     ('LS_INICIAL','LS_L_A1061','LS_L_A1062','LS_L_A107','LS_L_A25','LS_L_CV',
      'LS_L_GUARD','LS_L_INFO','LS_L_IV','LS_L_RT','LS_L_SAR','LS_MAXIMO','LS_O_A1061',
      'LS_O_A1062','LS_O_A107','LS_O_A25','LS_O_CV','LS_O_GUARD','LS_O_INFO','LS_O_IV',
      'LS_O_SAR','LS_P_A1061','LS_P_A1062','LS_P_A107','LS_P_A25','LS_P_CV','LS_P_GUARD',
      'LS_P_INFO','LS_P_IV','LS_P_SAR');

{$R *.DFM}

{ ************** Funciones generales ************* }

function Limita( const sValor: String; iLen: Word ): String;
begin
     Result := Copy( Trim( sValor ), 1, iLen );
end;

function GetImpresora: String;
const
     K_IMP_DEFAULT   = 'IMPRESORA DEFAULT:';
begin
     Result := K_IMP_DEFAULT;
end;

function SetListIndex( const iValue: Integer ): Integer;
begin
     Result := iMax( ( iValue - 1 ), 0 );
end;

function Str2Integer( const sValue: String ): Integer;
var
   i: Integer;
   cValue: Char;
begin
     for i := 1 to Length( sValue ) do
     begin
          cValue := sValue[ i ];
          if ( ( cValue < '0' ) or ( cValue > '9' ) ) and ( cValue <> ' ' ) then
          begin
               Result := 0;
               Exit;
          end;
     end;
     Result := StrAsInteger( sValue );
end;

function CodigoNumerico( const sValue: String ): Integer;
begin
     Result := Str2Integer( sValue );
end;

function FechaArt80: TDate;
begin
     Result := EncodeDate( ZetaCommonTools.TheYear( Date ), 1, 1 );
end;

function ConvierteFechaSalMin( const sFecha: String; const lEsVer260: Boolean ): TDate;
begin
     if lEsVer260 then
     begin
          Result := CodificaFecha( Str2Integer( Copy( sFecha, 1, 4 ) ),
                                   Str2Integer( Copy( sFecha, 5, 2 ) ),
                                   Str2Integer( Copy( sFecha, 7, 2 ) ) );
     end
     else
     begin
          Result := CodificaFecha( Str2Integer( Copy( sFecha, 1, 2 ) ) + 1900,
                                   Str2Integer( Copy( sFecha, 3, 2 ) ),
                                   Str2Integer( Copy( sFecha, 5, 2 ) ) );
     end;
end;

{cv}
{CV}
function ConvierteDescripcion( const sValor: String ): eDescripcionChecadas;
begin
     if ( sValor = '0' ) then
        Result := dcOrdinaria
     else
     if ( sValor = 'R' ) then
        Result := dcRetardo
     else
     if ( sValor = 'P' ) then
        Result := dcPuntual
     else
     if ( sValor = 'E' ) then
        Result := dcExtras
     else
     if ( sValor = 'C' ) then
        Result := dcComida
     else
     if ( sValor = '1' ) then
        Result := dcDescanso1
     else
     if ( sValor = '2' ) then
        Result := dcDescanso2
     else
     if ( sValor = 'D' ) then
        Result := dcDescanso
     else
     if ( sValor = 'N' ) then
        Result := dcEntrada
     else
         Result := dcDesconocida;
end;

function ConvierteChTipo( const sValor: String; Descripcion: eDescripcionChecadas ): eTipoChecadas;
begin
     if ( sValor = 'E' ) then
        Result := chEntrada
     else
     if ( sValor = 'S' ) then
        Result := chSalida
     else
     if ( sValor = 'I' ) then
        Result := chInicio
     else
     if ( sValor = 'F' ) then
        Result := chFin
     else
     if ( sValor = 'P' ) then
     begin
          if ( Descripcion = dcEntrada ) then
             Result := chConGoceEntrada
          else
              Result := chConGoce;
     end
     else
     if ( sValor = 'N' ) then
     begin
          if ( Descripcion = dcEntrada ) then
             Result := chSinGoceEntrada
          else
              Result := chSinGoce;
     end
     else
     if ( sValor = 'A' ) then
        Result := chExtras
     else // � Falta chDescanso ! //
         Result := chDesconocido;
end;

function EsMotivoDia( const sValue: String ): Boolean;
begin
     Result := ( Pos( sValue, MOTIVO_DIA ) > 0 );
end;

function ConvierteMotivo( const sValue: String ): Integer;
begin
     if EsMotivoDia( sValue ) then
        Result := SetListIndex( Pos( sValue, MOTIVO_DIA ) )
     else
         Result := SetListIndex( Pos( sValue, MOTIVO_HORA ) );
end;

function ConvierteHorario( const iValue: Integer ): eTipoHorario;
begin
     case iValue of
          4,5,6: Result := thSinHorario;
          7: Result := thMediaNoche;
     else
         Result := eTipoHorario( iValue );
     end;
end;

function ConvierteTurno( const iValue: Integer ): eTipoTurno;
begin
     case iValue of
          5: Result := ttSinJornada;
          6: Result := ttConJornada;
     else
         Result := ttNormal;
     end;
end;

function ConvierteIncidencia( const sValor: String; iValor: Integer ): eIncidencias;
begin
     if ( iValor > 0 ) then
        Result := eiIncapacidad
     else
     if ( sValor = 'FI' ) or ( sValor = 'FSS' ) then
        Result := eiFaltas
     else
     if ( sValor = 'ING' ) or ( sValor = 'BAJ' ) then
        Result := eiKardex
     else
     if ( sValor = 'V' ) then
        Result := eiVacaciones
     else
     if ( sValor = 'RE' ) then
        Result := eiRetardo
     else  // � Falta eiOtros ! //
         Result := eiPermiso;
end;

function ConvierteSistema( sValue: String ): String;
begin
     sValue := Trim( sValue );
     Result := zBoolToStr( ( sValue = K_T_ALTA ) or
                          ( sValue = K_T_BAJA ) or
                          ( sValue = K_T_CAMBIO ) or
                          ( sValue = K_T_RENOVA ) or
                          ( sValue = K_T_AREA ) or
                          ( sValue = K_T_TURNO ) or
                          ( sValue = K_T_PRESTA ) or
                          ( sValue = K_T_PUESTO ) or
                          ( sValue = K_T_TAS_INF ) or
                          ( sValue = K_T_EVENTO ) or
                          ( sValue = K_T_CIERRA ) );
end;

function ConvierteNivelKardex( const sTipo: String ): Integer;
begin
     if ( sTipo = K_T_ALTA ) then
        Result := 0
     else
     if ( sTipo = K_T_TAS_INF ) then
        Result := 1
     else
     if ( sTipo = K_T_EVENTO ) then
        Result := 1
     else
     if ( sTipo = K_T_PUESTO ) then
        Result := 2
     else
     if ( sTipo = K_T_AREA ) then
        Result := 3
     else
     if ( sTipo = K_T_TURNO ) then
        Result := 4
     else
     if ( sTipo = K_T_RENOVA ) then
        Result := 5
     else
     if ( sTipo = K_T_PRESTA ) then
        Result := 6
     else
     if ( sTipo = K_T_CAMBIO ) then
        Result := 7
     else
     if ( sTipo = K_T_CIERRA ) then
        Result := 8
     else
     if ( sTipo = K_T_BAJA ) then
        Result := 9
     else
         Result := 1;
end;

function ConvierteOtras( const sValue: String ): eIMSSOtrasPer;
begin
     if ( sValue = 'S' ) then
        Result := ioGravado
     else
     if ( sValue = 'A' ) or ( sValue = 'B' ) then
        Result := ioAsistencia
     else
     if ( sValue = 'D' ) or ( sValue = 'E' ) then
        Result := ioDespensa
     else
         Result := ioExento;
end;

function ConvierteStatusAusencia( const iValor: Integer ): eStatusAusencia;
begin
     Result := eStatusAusencia( SetListIndex( iValor ) );
end;

function ConvierteTipoDia( const iValor: Integer ): eTipoDiaAusencia;
begin
     case iValor of
          2: Result := daVacaciones;
          3: Result := daIncapacidad;
          4: Result := daConGoce;
          5: Result := daSinGoce;
          8: Result := daFestivo;
          9: Result := daNoTrabajado;
          10: Result := daJustificada;
          11: Result := daSuspension;
          12: Result := daOtroPermiso;
     else
         Result := daNormal;
     end;
end;

function ConvierteStatus( const sValue, sTipo, sCargo: ShortString ): eStatusKardex;
begin
     { Los movimientos tipo AREA ponen CB_STATUS = skCapturado, skImpreso }
     { para reflejar si C_ALTAS.DBF->CB_CARGO = "A", <> "A" y poder distinguir }
     { los cambios de TURNO que usan movimientos C_ALTAS.DBF->CB_TIPO = "AREA" }
     if ( sTipo = K_T_AREA ) then
     begin
          if ( sCargo = 'A' ) then
             Result := skCapturado
          else
              Result := skImpreso;
     end
     else
         if ( Length( sValue ) > 0 ) then
         begin
              case sValue[ 1 ] of
                   '3': Result := skImpreso;
                   '4': Result := skExportado;
                   '5': Result := skEntregado;
                   '6': Result := skRecibido;
              else
                  Result := skCapturado;
              end;
         end
         else
             Result := skCapturado;
end;

function ConvierteLiquidacionNomina( sValue: String ): eLiqNomina;
const
     LIQ_LIQ = 'LIQUIDACION';
     LIQ_IND = 'INDEMNIZACION';
begin
     sValue := Trim( sValue );
     if ( sValue = LIQ_LIQ ) then
        Result := lnLiquidacion
     else
         if ( sValue = LIQ_IND ) then
            Result := lnIndemnizacion
         else
             Result := lnNormal;
end;

function ConvierteTipoLiquidacion( const Day: Word ): eTipoLiqIMSS;
begin
     case ( Day - 1 ) of
          1: Result := tlExtemporanea;
          2: Result := tlComplementaria;
     else
         Result := tlOrdinaria;
     end;
end;

function ConvierteInfonavitTipo( const sCredito, sTipo: String ): eTipoInfonavit;
begin
     if StrLleno( sCredito ) then
     begin
          case Str2Integer( sTipo ) of
               0: Result := tiNoTiene;
               1: Result := tiPorcentaje;
               2: Result := tiCuotaFija;
               3: Result := tiVeces;
          else
              Result := tiNoTiene;
          end;
     end
     else
         Result := tiNoTiene;
end;

function ConvierteClasePermiso( const sValor: String ): eTipoPermiso;
begin
     if StrLleno( sValor ) then
        Result := eTipoPermiso( Pos( sValor[ 1 ], 'SNJUO' ) - 1 )
     else
         Result := tpOtros;
end;

function ConvierteStatusAhorro( const iValue: Integer ): eStatusAhorro;
begin
     Result := eStatusAhorro( SetListIndex( iValue ) );
end;

function ConvierteStatusPrestamo( const iValue: Integer ): eStatusPrestamo;
begin
     Result := eStatusPrestamo( SetListIndex( iValue ) );
end;

function ConvierteAltaAhorroPrestamo( const sValor: String ): eAltaAhorroPrestamo;
begin
     if StrVacio( sValor ) or ( sValor = K_GLOBAL_NO ) then
        Result := apNoAgregar
     else
         if ZetaCommonTools.zStrToBool( sValor ) then
            Result := apAgregarAutomaticamente
         else
             Result := apAgregarPreguntando;
end;

function ConvierteFinIncapacidad( const iValue: Integer ): eFinIncapacidad;
begin
     Result := eFinIncapacidad( SetListIndex( iValue ) );
end;

function ConvierteMotivoIncapacidad( const iValue: Integer ): eMotivoIncapacidad;
begin
     Result := eMotivoIncapacidad( SetListIndex( iValue ) );
end;

function ConvierteUsoPeriodo( const iNumero: Integer ): eUsoPeriodo;
begin
     if ( iNumero < 200 ) then
        Result := upOrdinaria
     else
         Result := upEspecial;
end;

function QuitaTodos( const sQuery: String ): String;
begin
     Result := Trim( sQuery );
     if ( Result = '*TODOS*' ) then
        Result := K_STRING_NULO;
end;

procedure MigrarFechaQuery( Query: TQuery; const sTargetField: String; Valor: TDate );
begin
     Query.ParamByName( sTargetField ).AsDateTime := Valor;
end;

procedure MigrarStringKeyQuery( Query: TQuery; const sTargetField, sValor: String );
begin
     with Query.ParamByName( sTargetField ) do
     begin
          if StrLleno( sValor ) then
             AsString := sValor
          else
              AsString := K_STRING_NULO;
     end;
end;

{ ************** TdmMigrarDatos *************** }

procedure TdmMigrarDatos.dmMigrarCreate(Sender: TObject);
begin
     inherited;
     ZConvierteFormula.oEvaluador := TQREvaluator.Create;
     FAhorro := TStringList.Create;
     FConcepto := TStringList.Create;
     FReporteOK := False;
     FReporte := '';
     FNiveles := 0;
     FInfOldField := '';
     FClasificacion := crMigracion;
     ///CV
     FEncabezado := FALSE;
     FTipoSalida := 0;
     FMascaraNoCeros := FALSE;
     with FConcepto do
     begin
	  Add( 'T0=1000|Total de Percepciones' );
	  Add( 'T1=1001|Total de Deducciones' );
	  Add( 'TN=1002|Total Neto' );
	  Add( 'TX=1003|Total Exento ISPT' );
	  Add( 'TU=1004|Total Percepciones p/PTU' );
	  Add( 'TM=1005|Total Percepciones Mensuales' );
	  Add( 'TY=1006|Exento Percepciones Mensuales' );
	  Add( 'T2=1007|Percepciones c/ISPT Individual' );
	  Add( 'T3=1008|Total ISPT Individuales' );
	  Add( 'TZ=1009|Exento ISPT Individuales' );
          Add( '--=1010|Total de Prestaciones' );

	  Add( 'FI=1100|Faltas Injustificadas' );
	  Add( 'FJ=1101|Faltas Justificadas' );
	  Add( 'DG=1102|Permisos c/Goce' );
	  Add( 'DP=1103|Permisos s/Goce' );
	  Add( 'DU=1104|Suspensiones' );
	  Add( 'DO=1105|Permisos Otros' );
	  Add( 'DI=1106|Incapacidades' );
	  Add( 'D7=1107|Trabajados/A Pagar con 7o D�a' );
	  Add( 'DE=1108|Retardos' );
	  Add( 'DR=1109|Proporcionales a Turno' );
	  Add( 'DT=1110|Trabajados/A Pagar sin 7o D�a' );
	  Add( 'DS=1111|Asistencia' );
	  Add( 'NT=1112|No Trabajados (Por Per�odo)' );
	  Add( 'FV=1113|Faltas por Vacaciones' );
	  Add( 'DV=1114|Vacaciones Pagado' );
	  Add( 'DA=1115|Aguinaldo Pagado' );
	  Add( 'DH=1116|Ajuste' );
	  Add( 'DM=1117|Base IMSS / INFONAVIT' );
	  Add( 'DY=1118|Base Enfermedad y Maternidad' );

	  Add( 'HT=1200|Trabajadas Prop Turno/Per�odo' );
	  Add( 'HR=1201|Reales' );
	  Add( 'HE=1202|Extras (Dobles+Triples)' );
	  Add( 'H2=1203|Extras Dobles' );
	  Add( 'H3=1204|Extras Triples' );
	  Add( 'HA=1205|Adicionales' );
	  Add( 'HS=1206|Retardos' );
	  Add( 'HD=1207|Trabajadas en Domingo' );
	  Add( 'HX=1208|Permiso c/Goce' );
	  Add( 'HY=1209|Permiso s/Goce' );
	  Add( 'HF=1210|Festivo Trabajado' );
	  Add( 'HC=1211|Descanso Trabajado' );
	  Add( 'HU=1212|Vacaciones Trabajadas' );
          Add( '--=1213|Festivo Pagado' );
          Concepto.AdditionalRows := Count;
     end;
     FPrestaxCreated := False;
     FEstado := '02';
     FAnio := TheYear( Now );
     FAcumulados := TheYear( Now );
     FDirFotos := K_STRING_NULO;
     FDirDocumentos := K_STRING_NULO;
     FDirSuper := K_STRING_NULO;
     SetNivelStructure( '333333333' );
     { Es obligatorio que Owner sea TForm = Wizard FMigrarDatos }
     PMultiImage := TPMultiImage.Create( Owner );
     with PMultiImage do
     begin
          Parent := TForm( Owner );
          Name := 'PMultiImage';
          Visible := False;
     end;

     {$ifdef FALSE}
     Colabora.Enabled := False;
     Vacacion.Enabled := False;
     Incapaci.Enabled := False;
     Karcurso.Enabled := False;
     Kardex.Enabled := False;
     Kar_Fija.Enabled := False;
     Liq_Imss.Enabled := False;
     Liq_Emp.Enabled := False;
     Liq_Mov.Enabled := False;
     Nomina.Enabled := False;
     Acumula.Enabled := False;
     Faltas.Enabled := False;
     Movimien.Enabled := False;
     Ausencia.Enabled := False;
     Checadas.Enabled := False;
     Ahorro.Enabled := False;
     ACar_Abo.Enabled := False;
     Prestamo.Enabled := False;
     PCar_Abo.Enabled := False;
     Folio.Enabled := False;

     Global.Enabled := False;
     Concepto.Enabled := False;
     TFijas.Enabled := False;

     {Especial.Enabled := False;
     RepRH.Enabled := False;
     RepTarj.Enabled := False;
     RepAsist.Enabled := False;
     RepCursos.Enabled := False;
     RepSar.Enabled := False;
     Forma2.Enabled := False;
     RepSuper.Enabled := False;
     Polizas.Enabled := False;}
     {$endif}
end;

procedure TdmMigrarDatos.dmMigrarDestroy(Sender: TObject);
begin
     PMultiImage.Free;
     FConcepto.Free;
     FAhorro.Free;
     ZConvierteFormula.oEvaluador.Free;
     inherited;
end;

procedure TdmMigrarDatos.SetDirSuper( const Value: String );
begin
     FDirSuper := Value;
end;

procedure TdmMigrarDatos.SetEsVer260( const lValue: Boolean );
begin
     inherited SetEsVer260( lValue );
     TCambio.Enabled := EsVer260;
     Mot_Auto.Enabled := EsVer260;
     CafRegla.Enabled := EsVer260;
     CafCome.Enabled := EsVer260;
end;

procedure TdmMigrarDatos.LlenaListaEstados( Lista: TStrings; const sDirectory: String );
begin
     with Lista do
     begin
          BeginUpdate;
          Clear;
          try
             SourceDatosPath := sDirectory;
             OpenDBSourceDatos;
             with Entidad do
             begin
                  PrepareSourceQuery( SourceQuery, SourceSQL.Text );
                  with SourceQuery do
                  begin
                       try
                          Active := True;
                          First;
                          while not Eof do
                          begin
                               Add( FieldByName( 'TB_CODIGO' ).AsString + '=' + FieldByName( 'TB_ELEMENT' ).AsString );
                               Next;
                          end;
                       except
                       end;
                       Active := False;
                  end;
             end;
          except
          end;
          CloseDBSourceDatos;
          EndUpdate;
     end;
end;

function TdmMigrarDatos.EsAhorro( const sTipo: String ): Boolean;
begin
     Result := ( FAhorro.IndexOf( sTipo ) >= 0 );
end;

function TdmMigrarDatos.ChecaAnchoFormula( const sFormula, sMensaje: String ): String;
begin
     Result := sFormula;
     if ( Length( Result ) > K_ANCHO_MAXIMO ) then
     begin
          with Bitacora do
          begin
               AgregaResumen( '' );
               AgregaResumen( Format( '%s: Excede %d caracteres', [ sMensaje, K_ANCHO_MAXIMO ] ) + CR_LF + Result );
          end;
          Result := Copy( Result, 1, K_ANCHO_MAXIMO );
     end;
end;

function TdmMigrarDatos.VerificaFormula( const sFormula, sMensaje: String; const lValor: Boolean ): String;
  function TransCC( sOrigen, sSearch, sReplace: String ) : String;
  var
     iPos, iLast, iSearch : Integer;
     sCopia : String;
     lCambia : Boolean;
  begin
       Result  := '';
       sSearch := AnsiUpperCase( sSearch );
       iSearch := Length( sSearch );
       sCopia  := AnsiUpperCase( sOrigen );
       iPos    := AnsiPos( sSearch, sCopia );
       while ( iPos > 0 ) do
       begin
            iLast := iPos + iSearch;
            if ( iLast <= Length( sCopia )) then
                 lCambia := NOT( sCopia[iLast-iSearch-1] in ['A'..'Z','0'..'9','_'] )
            else
                 lCambia := TRUE;

            if ( lCambia ) then
                 Result := Result + Copy( sOrigen, 1, iPos-1 ) + sReplace
            else
            begin
                Result := Result + Copy( sOrigen, 1, iPos );
                iLast := iPos + 1;
            end;
            sCopia := Copy( sCopia, iLast, MAXINT );
            sOrigen := Copy( sOrigen, iLast, MAXINT );
            iPos := AnsiPos( sSearch, sCopia );
       end;
       Result := Result + sOrigen;
  end;
begin
     Result := ChecaFormula( sFormula, sMensaje, lValor  );
     Result := TransCC( Result, 'C(', 'CC(');
end;

function TdmMigrarDatos.ChecaFormula( const sFormula, sMensaje: String; const lValor: Boolean ): String;
begin
     Result := ConvierteFormula( sFormula, lValor );
     Result := ChecaAnchoFormula( Result, sMensaje );
end;

function TdmMigrarDatos.ConvierteConcepto( sValor: String ): Integer;
begin
     sValor := Trim( sValor );
     Result := Str2Integer( sValor );
     if ( Result <= 0 ) then
     begin
          with FConcepto do
          begin
               Result := IndexOfName( sValor );
               if ( Result < 0 ) then
               begin
                    Result := 5000 + ( Count - 1 );
                    Add( sValor + '=' + IntToStr( Result ) );
               end
               else
                   Result := Str2Integer( Copy( Values[ sValor ], 1, 4 ) );
          end;
     end;
end;

function TdmMigrarDatos.GetDirFotos: String;
begin
     if StrLleno( FDirFotos ) then
        Result := FDirFotos
     else
         Result := SourceDatosPath + '\FOTOS';
     Result := VerificaDir( Result );
end;

function TdmMigrarDatos.GetDirDocumentos: String;
begin
     if StrLleno( FDirDocumentos ) then
        Result := FDirDocumentos
     else
         Result := SourceDatosPath + '\DOCUMENT';
     Result := VerificaDir( Result );
end;

function TdmMigrarDatos.SiNo( const sValor: String ): String;
begin
     Result := ZetaCommonTools.zBoolToStr( sValor = K_GLOBAL_SI );
end;

procedure TdmMigrarDatos.SetDirFotos( const Value: String );
begin
     if ( Value <> FDirFotos ) and StrLleno( Value ) then
        FDirFotos := Value;
end;

procedure TdmMigrarDatos.SetDirDocumentos( const Value: String );
begin
     if ( Value <> FDirDocumentos ) and StrLleno( Value ) then
        FDirDocumentos := Value;
end;

procedure TdmMigrarDatos.SetNivelStructure( const sValor: String );
begin
     FNivelLen1 := Str2Integer( Copy( sValor, 1, 1 ) );
     FNivelLen2 := Str2Integer( Copy( sValor, 2, 1 ) );
     FNivelLen3 := Str2Integer( Copy( sValor, 3, 1 ) );
     FNivelLen4 := Str2Integer( Copy( sValor, 4, 1 ) );
     FNivelLen5 := Str2Integer( Copy( sValor, 5, 1 ) );
     FNivelLen6 := Str2Integer( Copy( sValor, 6, 1 ) );
     FNivelLen7 := Str2Integer( Copy( sValor, 7, 1 ) );
     FNivelLen8 := Str2Integer( Copy( sValor, 8, 1 ) );
     FNivelLen9 := Str2Integer( Copy( sValor, 9, 1 ) );
     {$ifdef ACS}
     FNivelLen10 := Str2Integer( Copy( sValor,10, 1 ) );
     FNivelLen11 := Str2Integer( Copy( sValor, 11, 1 ) );
     FNivelLen12 := Str2Integer( Copy( sValor, 12, 1 ) );
     {$endif}
     FNivelIni1 := 1;
     FNivelIni2 := FNivelIni1 + FNivelLen1;
     FNivelIni3 := FNivelIni2 + FNivelLen2;
     FNivelIni4 := FNivelIni3 + FNivelLen3;
     FNivelIni5 := FNivelIni4 + FNivelLen4;
     FNivelIni6 := FNivelIni5 + FNivelLen5;
     FNivelIni7 := FNivelIni6 + FNivelLen6;
     FNivelIni8 := FNivelIni7 + FNivelLen7;
     FNivelIni9 := FNivelIni8 + FNivelLen8;
     {$ifdef ACS}
     FNivelIni10 := FNivelIni9 + FNivelLen9;
     FNivelIni11 := FNivelIni10 + FNivelLen10;
     FNivelIni12 := FNivelIni11 + FNivelLen11;
     {$endif}
end;

function TdmMigrarDatos.LeeCodigoEmpleado: TNumEmp;
begin
     Result := CodigoNumerico( Source.FieldByName( 'CB_CODIGO' ).AsString );
end;

procedure TdmMigrarDatos.MigrarEmpleado( const iEmpleado: TNumEmp );
begin
     TargetQuery.ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
end;

procedure TdmMigrarDatos.MigrarCodigoEmpleado;
begin
     MigrarEmpleado( LeeCodigoEmpleado );
end;

procedure TdmMigrarDatos.MigrarUsuario( const sTargetField, sSourceField: String );
begin
     TargetQuery.ParamByName( sTargetField ).AsInteger := Str2Integer( Source.FieldByName( sSourceField ).AsString );
end;

procedure TdmMigrarDatos.MigrarBooleanos( const sTargetField, sSourceField: String );
begin
     TargetQuery.ParamByName( sTargetField ).AsString := zBoolToStr( Source.FieldByName( sSourceField ).AsBoolean );
end;

procedure TdmMigrarDatos.MigrarFecha( const sTargetField: String; const Value: TDate );
begin
     MigrarFechaQuery( TargetQuery, sTargetField, Value );
end;

procedure TdmMigrarDatos.MigrarFechaKey( const sTargetField: String; const Value: TDate );
begin
     MigrarFechaQuery( TargetQuery, sTargetField, Value );
end;

procedure TdmMigrarDatos.MigrarFechaNull( const sTargetField: String; const Value: TDate );
begin
     MigrarFechaQuery( TargetQuery, sTargetField, Value );
end;

function TdmMigrarDatos.PuedeMigrarKardex( const Value: TDate; const iEmpleado: TNumEmp ): Boolean;
begin
     Result := ( Value > NullDateTime );
     if not Result then
     begin
          Bitacora.AgregaError( Format( 'Empleado %d: Registro de Kardex Descartado Por Fecha Inv�lida ( %s )', [ iEmpleado, FormatDateTime( 'dd/mmm/yyyy', Value ) ] ) );
     end;
end;

procedure TdmMigrarDatos.MigrarFechaKardex( const sTargetField: String; const Value: TDate );
begin
     MigrarFechaQuery( TargetQuery, sTargetField, Value );
end;

procedure TdmMigrarDatos.MigrarStringKey( const sTargetField, sValue: String );
begin
     MigrarStringKeyQuery( TargetQuery, sTargetField, sValue );
end;

procedure TdmMigrarDatos.MigrarStringKeyNull( const sTargetField, sValue: String );
begin
     MigrarStringKeyQuery( TargetQuery, sTargetField, sValue );
end;

procedure TdmMigrarDatos.MigrarIntegerKeyNull( const sTargetField: String; iValue: Integer );
begin
     TargetQuery.ParamByName( sTargetField ).AsInteger := iValue;
end;

procedure TdmMigrarDatos.MigrarMemo( const sTargetField, sValue: String );
begin
     TargetQuery.ParamByName( sTargetField ).AsBlob := TBlobData( sValue );
end;

procedure TdmMigrarDatos.MigrarSiNo( const sTargetField, sSourceField: String );
begin
     TargetQuery.ParamByName( sTargetField ).AsString := SiNo( Source.FieldByName( sSourceField ).AsString );
end;

procedure TdmMigrarDatos.MigrarAsInteger( const sTargetField, sSourceField: String );
begin
     TargetQuery.ParamByName( sTargetField ).AsInteger := Str2Integer( Source.FieldByName( sSourceField ).AsString );
end;

procedure TdmMigrarDatos.MigrarNiveles( const sTargetField, sSourceField: String );
var
   sNiveles: String;
begin
     sNiveles := Source.FieldByName( sSourceField ).AsString;
     MigrarStringKey( sTargetField + '1', Copy( sNiveles, FNivelIni1, FNivelLen1 ) );
     MigrarStringKey( sTargetField + '2', Copy( sNiveles, FNivelIni2, FNivelLen2 ) );
     MigrarStringKey( sTargetField + '3', Copy( sNiveles, FNivelIni3, FNivelLen3 ) );
     MigrarStringKey( sTargetField + '4', Copy( sNiveles, FNivelIni4, FNivelLen4 ) );
     MigrarStringKey( sTargetField + '5', Copy( sNiveles, FNivelIni5, FNivelLen5 ) );
     MigrarStringKey( sTargetField + '6', Copy( sNiveles, FNivelIni6, FNivelLen6 ) );
     MigrarStringKey( sTargetField + '7', Copy( sNiveles, FNivelIni7, FNivelLen7 ) );
     MigrarStringKey( sTargetField + '8', Copy( sNiveles, FNivelIni8, FNivelLen8 ) );
     MigrarStringKey( sTargetField + '9', Copy( sNiveles, FNivelIni9, FNivelLen9 ) );
     {$ifdef ACS}
     MigrarStringKey( sTargetField + '10', Copy( sNiveles, FNivelIni10, FNivelLen10 ) );
     MigrarStringKey( sTargetField + '11', Copy( sNiveles, FNivelIni11, FNivelLen11 ) );
     MigrarStringKey( sTargetField + '12', Copy( sNiveles, FNivelIni12, FNivelLen12 ) );
     {$endif}
end;

procedure TdmMigrarDatos.MigrarNivelesNull( const sTargetField, sSourceField: String );
var
   sNiveles: String;
begin
     sNiveles := Source.FieldByName( sSourceField ).AsString;
     MigrarStringKeyNull( sTargetField + '1', Copy( sNiveles, FNivelIni1, FNivelLen1 ) );
     MigrarStringKeyNull( sTargetField + '2', Copy( sNiveles, FNivelIni2, FNivelLen2 ) );
     MigrarStringKeyNull( sTargetField + '3', Copy( sNiveles, FNivelIni3, FNivelLen3 ) );
     MigrarStringKeyNull( sTargetField + '4', Copy( sNiveles, FNivelIni4, FNivelLen4 ) );
     MigrarStringKeyNull( sTargetField + '5', Copy( sNiveles, FNivelIni5, FNivelLen5 ) );
     MigrarStringKeyNull( sTargetField + '6', Copy( sNiveles, FNivelIni6, FNivelLen6 ) );
     MigrarStringKeyNull( sTargetField + '7', Copy( sNiveles, FNivelIni7, FNivelLen7 ) );
     MigrarStringKeyNull( sTargetField + '8', Copy( sNiveles, FNivelIni8, FNivelLen8 ) );
     MigrarStringKeyNull( sTargetField + '9', Copy( sNiveles, FNivelIni9, FNivelLen9 ) );
     {$ifdef ACS}
     MigrarStringKeyNull( sTargetField + '10', Copy( sNiveles, FNivelIni10, FNivelLen10 ) );
     MigrarStringKeyNull( sTargetField + '11',Copy( sNiveles, FNivelIni11, FNivelLen11 ) );
     MigrarStringKeyNull( sTargetField + '12', Copy( sNiveles, FNivelIni12, FNivelLen12 ) );
     {$endif}
end;

procedure TdmMigrarDatos.MigrarAdicionales;
var
   iYear: Integer;
   sPath, sNewPath: String;
   FQuery: TQuery;
begin
     inherited MigrarAdicionales;
     CloseDBSourceDatos;
     SetTablaMigrada( Acumula );
     iYear := FAnio - 1;
     sPath := SourceDatosPath + '%d\';
     while ( iYear >= FAcumulados ) do
     begin
          sNewPath := Format( sPath, [ iYear ] );
          if FileExists( sNewPath + 'ACUMULA.DBF' ) then
          begin
               FAnio := iYear;
               OpenDBSourceDatosAlternate( sNewPath );
               with TablaMigrada do
               begin
                    FQuery := DeleteQuery;
                    DeleteQuery := nil;
                    try
                       StartMessage := Format( 'Acumulados de %d', [ iYear ] );
                       DoStartCallBack( Format( 'Migrando %s ( %s )', [ StartMessage, TargetTableName ] ) );
                       Migrar( Bitacora );
                       SetErrorFound( HayError );
                    finally
                           DeleteQuery := FQuery;
                    end;
                    if not DoEndCallBack then
                       Break;
               end;
          end;
          Dec( iYear );
     end;
end;

{ ********* Eventos de Base de Datos de TressDatos ********** }

procedure TdmMigrarDatos.GlobalOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select COUNT(*) from GLOBAL where ( GL_CODIGO = :GL_CODIGO )' );
     end;
     with FLista do
     begin
          Clear;
          if StrLleno( FDirSuper ) then
          begin
               with TIniFile.Create( FDirSuper ) do
               begin
                    try
                       Add( IntToStr( K_SUPER_SEGUNDOS ) + '=' + IntToStr( ReadInteger( 'ESPERAS', 'SCREEN SAVER', 120 ) ) );
                       Add( IntToStr( K_LIMITE_MODIFICAR_ASISTENCIA ) + '=' + IntToStr( ReadInteger( 'TRESS', 'STATUS MINIMO DE NOMINA', Ord( spCalculadaTotal ) ) ) );
                    finally
                           Free;
                    end;
               end;
          end;
     end;
end;

procedure TdmMigrarDatos.GlobalCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
     end;
     spActualizaDiccion.ExecProc;
end;

procedure TdmMigrarDatos.GlobalFilterRecord(DataSet: TDataSet; var Accept: Boolean);
const
     K_GLOBAL_NIVEL_INFO = 1000;
     K_GLOBAL_DIR_FOTOS_INFO = 1001;
     K_GLOBAL_DIR_DOCS_INFO = 1002;
     K_GLOBAL_CB_INF_OLD_VAR = 1003;
var
   sValue: String;
   iCodigo: Integer;
begin
     with DataSet do
     begin
          Accept := False;
          iCodigo := FieldByName( 'GL_CODIGO' ).AsInteger;
          case iCodigo of
               K_SUPER_SEGUNDOS: Accept := ( FLista.IndexOf( IntToStr( K_SUPER_SEGUNDOS ) ) >= 0 );
               K_LIMITE_MODIFICAR_ASISTENCIA: Accept := ( FLista.IndexOf( IntToStr( K_LIMITE_MODIFICAR_ASISTENCIA ) ) >= 0 );
               K_GLOBAL_NIVEL_INFO: SetNivelStructure( FieldByName( 'GL_FORMULA' ).AsString );
               K_GLOBAL_DIR_FOTOS_INFO: DirFotos := FieldByName( 'GL_FORMULA' ).AsString;
               K_GLOBAL_DIR_DOCS_INFO: DirDocumentos := FieldByname( 'GL_FORMULA' ).AsString;
               K_GLOBAL_CB_INF_OLD_VAR:
               begin
                    sValue := FieldByname( 'GL_FORMULA' ).AsString;
                    if ( sValue = 'T1' ) then
                       FInfOldField := 'CB_G_TEX_1'
                    else
                    if ( sValue = 'T2' ) then
                       FInfOldField := 'CB_G_TEX_2'
                    else
                    if ( sValue = 'T3' ) then
                       FInfOldField := 'CB_G_TEX_3'
                    else
                    if ( sValue = 'T4' ) then
                       FInfOldField := 'CB_G_TEX_4'
                    else
                    if ( sValue = 'N1' ) then
                       FInfOldField := 'CB_G_NUM_1'
                    else
                    if ( sValue = 'N2' ) then
                       FInfOldField := 'CB_G_NUM_2'
                    else
                    if ( sValue = 'N3' ) then
                       FInfOldField := 'CB_G_NUM_3';
               end;
          else
               begin
                    with tqUno do
                    begin
                         Active := False;
                         ParamByName( 'GL_CODIGO' ).AsInteger := iCodigo;
                         Active := True;
                         if IsEmpty or ( Fields[ 0 ].AsInteger = 0 ) then
                            TablaMigrada.AgregaError( 'Valor Global ' + IntToStr( iCodigo ) + ' No Existe' )
                         else
                             Accept := True;
                         Active := False;
                    end;
               end;
          end;
     end;
end;

procedure TdmMigrarDatos.GlobalMoveExcludedFields(Sender: TObject);
var
   iValor: Integer;
   sValor: String;

function InvestigaNivel( const sNivel: String; const iNivel: Word ): String;
begin
     Result := ZetaTressCFGTools.UpperLower( sValor );
     if StrLleno( sValor ) then
        FNiveles := iMax( FNiveles, iNivel );
end;

begin
     with Source do
     begin
          iValor := FieldByName( 'GL_CODIGO' ).AsInteger;
          sValor := FieldByName( 'GL_FORMULA' ).AsString;
     end;
     with TargetQuery do
     begin
          case iValor of
               K_GLOBAL_NIVEL1: sValor := InvestigaNivel( sValor, 1 );
               K_GLOBAL_NIVEL2: sValor := InvestigaNivel( sValor, 2 );
               K_GLOBAL_NIVEL3: sValor := InvestigaNivel( sValor, 3 );
               K_GLOBAL_NIVEL4: sValor := InvestigaNivel( sValor, 4 );
               K_GLOBAL_NIVEL5: sValor := InvestigaNivel( sValor, 5 );
               K_GLOBAL_NIVEL6: sValor := InvestigaNivel( sValor, 6 );
               K_GLOBAL_NIVEL7: sValor := InvestigaNivel( sValor, 7 );
               K_GLOBAL_NIVEL8: sValor := InvestigaNivel( sValor, 8 );
               K_GLOBAL_NIVEL9: sValor := InvestigaNivel( sValor, 9 );
               {$ifdef ACS}
               K_GLOBAL_NIVEL10: sValor := InvestigaNivel( sValor, 10 );
               K_GLOBAL_NIVEL11: sValor := InvestigaNivel( sValor, 11 );
               K_GLOBAL_NIVEL12: sValor := InvestigaNivel( sValor, 12 );
               {$endif}
               K_GLOBAL_FECHA1: sValor := UpperLower( sValor );
               K_GLOBAL_FECHA2: sValor := UpperLower( sValor );
               K_GLOBAL_FECHA3: sValor := UpperLower( sValor );
               K_GLOBAL_TEXTO1: sValor := UpperLower( sValor );
               K_GLOBAL_TEXTO2: sValor := UpperLower( sValor );
               K_GLOBAL_TEXTO3: sValor := UpperLower( sValor );
               K_GLOBAL_TEXTO4: sValor := UpperLower( sValor );
               K_GLOBAL_NUM1: sValor := UpperLower( sValor );
               K_GLOBAL_NUM2: sValor := UpperLower( sValor );
               K_GLOBAL_NUM3: sValor := UpperLower( sValor );
               K_GLOBAL_LOG1: sValor := UpperLower( sValor );
               K_GLOBAL_LOG2: sValor := UpperLower( sValor );
               K_GLOBAL_LOG3: sValor := UpperLower( sValor );
               K_GLOBAL_TAB1: sValor := UpperLower( sValor );
               K_GLOBAL_TAB2: sValor := UpperLower( sValor );
               K_GLOBAL_TAB3: sValor := UpperLower( sValor );
               K_GLOBAL_TAB4: sValor := UpperLower( sValor );
               K_SUPER_SEGUNDOS: sValor := FLista.Values[ IntToStr( K_SUPER_SEGUNDOS ) ];
               K_LIMITE_MODIFICAR_ASISTENCIA: FLista.Values[ IntToStr( K_LIMITE_MODIFICAR_ASISTENCIA ) ];
          end;
          ParamByName( 'GL_CODIGO' ).AsInteger := iValor;
          ParamByName( 'GL_FORMULA' ).AsString := sValor;
     end;
end;

procedure TdmMigrarDatos.FolioMoveExcludedFields(Sender: TObject);
var
   sMonto: String;
   iCodigo: Integer;
begin
     with Source do
     begin
          iCodigo := FieldByName( 'FL_CODIGO' ).AsInteger;
          sMonto := ChecaFormula( FieldByName( 'FL_MONTO' ).AsString, Format( 'Folio %d: F�rmula de Monto ( FOLIO.FL_MONTO )', [ iCodigo ] ), False );
          sMonto := StrTransAll( sMonto, 'TOT_NETO', 'NOMINA.NO_NETO' );
     end;
     with TargetQuery do
     begin
          ParamByName( 'FL_CODIGO' ).AsInteger := iCodigo;
          ParamByName( 'FL_DESCRIP' ).AsString := Source.FieldByName( 'FL_DESCRIP' ).AsString;
          ParamByName( 'FL_REPORTE' ).AsInteger := Source.FieldByName( 'FL_REPORTE' ).AsInteger;
          ParamByName( 'FL_MONTO' ).AsString := sMonto;
          ParamByName( 'FL_REPITE' ).AsString := Source.FieldByName( 'FL_REPITE' ).AsString;
          ParamByName( 'FL_MONEDA' ).AsFloat := Source.FieldByName( 'FL_MONEDA' ).AsFloat;
          ParamByName( 'FL_CEROS' ).AsString := Source.FieldByName( 'FL_CEROS' ).AsString;
          ParamByName( 'FL_INICIAL' ).AsInteger := Source.FieldByName( 'FL_INICIAL' ).AsInteger;
          ParamByName( 'FL_FINAL' ).AsInteger := Source.FieldByName( 'FL_FINAL' ).AsInteger;
     end;
end;

procedure TdmMigrarDatos.Art_80FilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     with Dataset.FieldByName( 'A80_TIPO' ) do
     begin
          Accept := ( AsString <> '07' ) and ( AsString <> '08' );
     end;
end;

procedure TdmMigrarDatos.Art_80MoveExcludedFields(Sender: TObject);
begin
     MigrarAsInteger( 'NU_CODIGO', 'A80_TIPO' );
     with TargetQuery do
     begin
          ParamByName( 'TI_INICIO' ).AsDate := FechaArt80;
          ParamByName( 'A80_LI' ).AsFloat := Source.FieldByName( 'A80_LS' ).AsFloat;
          ParamByName( 'A80_CUOTA' ).AsFloat := Source.FieldByName( 'A80_CUOTA' ).AsFloat;
          ParamByName( 'A80_TASA' ).AsFloat := Source.FieldByName( 'A80_TASA' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.Art_80CloseDatasets(Sender: TObject);
var
   iCodigo, iViejo: Integer;
   dInicio: TDate;
   rLimite: TPesos;
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select NU_CODIGO, TI_INICIO, A80_LI from ART_80 order by NU_CODIGO, TI_INICIO, A80_LI' );
          PrepareTargetQuery( tqDos, 'update ART_80 set A80_LI = :NuevoLimite where ( NU_CODIGO = :Codigo ) and ( TI_INICIO = :Fecha ) and ( A80_LI = :Limite )' );
     end;
     with tqUno do
     begin
          iViejo := 0;
          rLimite := 0;
          Active := True;
          First;
          while not Eof do
          begin
               try
                  iCodigo := FieldByName( 'NU_CODIGO' ).AsInteger;
                  dInicio := FieldByName( 'TI_INICIO' ).AsDateTime;
                  if ( iViejo <> iCodigo ) then // Corte //
                  begin
                       tqDos.ParamByName( 'NuevoLimite' ).AsFloat := 0;
                       iViejo := iCodigo;
                  end
                  else
                      tqDos.ParamByName( 'NuevoLimite' ).AsFloat := rLimite + 0.01;
                  rLimite := FieldByName( 'A80_LI' ).AsFloat;
                  with tqDos do
                  begin
                       ParamByName( 'Codigo' ).AsInteger := iCodigo;
                       ParamByName( 'Fecha' ).AsDate := dInicio;
                       ParamByName( 'Limite' ).AsFloat := rLimite;
                       ExecSQL;
                  end;
               except
               end;
               Next;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.NumericaMoveExcludedFields(Sender: TObject);
begin
     MigrarAsInteger( 'NU_CODIGO', 'TB_CODIGO' );
     with TargetQuery do
     begin
          ParamByName( 'NU_DESCRIP' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'NU_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'NU_NUMERO' ).AsInteger := 0;
          ParamByName( 'NU_TEXTO' ).AsString := K_STRING_NULO;
     end;
end;

procedure TdmMigrarDatos.NumericaCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, Format( 'insert into T_ART_80 select NU_CODIGO, ''%s'', ''%s'' from NUMERICA', [ ZetaCommonTools.DateToStrSQL( FechaArt80 ), K_MARCA ] ) );
     end;
     with tqUno do
     begin
          ExecSQL;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.RiesgoMoveExcludedFields(Sender: TObject);
begin
     MigrarAsInteger( 'RI_CLASE', 'RI_CLASE' );
     with TargetQuery do
     begin
          ParamByName( 'RI_GRADO' ).AsInteger := Source.FieldByName( 'RI_GRADO' ).AsInteger;
          ParamByName( 'RI_INDICE' ).AsInteger := Source.FieldByName( 'RI_INDICE' ).AsInteger;
          ParamByName( 'RI_PRIMA' ).AsFloat := Source.FieldByName( 'RI_PRIMA' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.Sal_MinMoveExcludedFields(Sender: TObject);
begin
     MigrarFecha( 'SM_FEC_INI', ConvierteFechaSalMin( Source.FieldByName( 'TB_CODIGO' ).AsString, EsVer260 ) );
     with TargetQuery do
     begin
          ParamByName( 'SM_ZONA_A' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
          ParamByName( 'SM_ZONA_B' ).AsFloat := StrToReal( Source.FieldByName( 'TB_ELEMENT' ).AsString );
          ParamByName( 'SM_ZONA_C' ).AsFloat := StrToReal( Source.FieldByName( 'TB_INGLES' ).AsString );
     end;
end;

procedure TdmMigrarDatos.Ley_ImssMoveExcludedFields(Sender: TObject);
begin
     with Source do
     begin
          MigrarFecha( 'SS_INICIAL', FieldByName( 'SS_INICIAL' ).AsDateTime );
     end;
     with TargetQuery do
     begin
          ParamByName( 'SS_L_A1061' ).AsFloat := Source.FieldByName( 'SS_L_A1061' ).AsFloat;
          ParamByName( 'SS_L_A1062' ).AsFloat := Source.FieldByName( 'SS_L_A1062' ).AsFloat;
          ParamByName( 'SS_L_A107' ).AsFloat := Source.FieldByName( 'SS_L_A107' ).AsFloat;
          ParamByName( 'SS_L_A25' ).AsFloat := Source.FieldByName( 'SS_L_A25' ).AsFloat;
          ParamByName( 'SS_L_CV' ).AsFloat := Source.FieldByName( 'SS_L_CV' ).AsFloat;
          ParamByName( 'SS_L_GUARD' ).AsFloat := Source.FieldByName( 'SS_L_GUARD' ).AsFloat;
          ParamByName( 'SS_L_INFO' ).AsFloat := Source.FieldByName( 'SS_L_INFO' ).AsFloat;
          ParamByName( 'SS_L_IV' ).AsFloat := Source.FieldByName( 'SS_L_IV' ).AsFloat;
          ParamByName( 'SS_L_RT' ).AsFloat := Source.FieldByName( 'SS_L_RT' ).AsFloat;
          ParamByName( 'SS_L_SAR' ).AsFloat := Source.FieldByName( 'SS_L_SAR' ).AsFloat;
          ParamByName( 'SS_MAXIMO' ).AsFloat := Source.FieldByName( 'SS_MAXIMO' ).AsFloat;
          ParamByName( 'SS_O_A1061' ).AsFloat := Source.FieldByName( 'SS_O_A1061' ).AsFloat;
          ParamByName( 'SS_O_A1062' ).AsFloat := Source.FieldByName( 'SS_O_A1062' ).AsFloat;
          ParamByName( 'SS_O_A107' ).AsFloat := Source.FieldByName( 'SS_O_A107' ).AsFloat;
          ParamByName( 'SS_O_A25' ).AsFloat := Source.FieldByName( 'SS_O_A25' ).AsFloat;
          ParamByName( 'SS_O_CV' ).AsFloat := Source.FieldByName( 'SS_O_CV' ).AsFloat;
          ParamByName( 'SS_O_GUARD' ).AsFloat := Source.FieldByName( 'SS_O_GUARD' ).AsFloat;
          ParamByName( 'SS_O_INFO' ).AsFloat := Source.FieldByName( 'SS_O_INFO' ).AsFloat;
          ParamByName( 'SS_O_IV' ).AsFloat := Source.FieldByName( 'SS_O_IV' ).AsFloat;
          ParamByName( 'SS_O_SAR' ).AsFloat := Source.FieldByName( 'SS_O_SAR' ).AsFloat;
          ParamByName( 'SS_P_A1061' ).AsFloat := Source.FieldByName( 'SS_P_A1061' ).AsFloat;
          ParamByName( 'SS_P_A1062' ).AsFloat := Source.FieldByName( 'SS_P_A1062' ).AsFloat;
          ParamByName( 'SS_P_A107' ).AsFloat := Source.FieldByName( 'SS_P_A107' ).AsFloat;
          ParamByName( 'SS_P_A25' ).AsFloat := Source.FieldByName( 'SS_P_A25' ).AsFloat;
          ParamByName( 'SS_P_CV' ).AsFloat := Source.FieldByName( 'SS_P_CV' ).AsFloat;
          ParamByName( 'SS_P_GUARD' ).AsFloat := Source.FieldByName( 'SS_P_GUARD' ).AsFloat;
          ParamByName( 'SS_P_INFO' ).AsFloat := Source.FieldByName( 'SS_P_INFO' ).AsFloat;
          ParamByName( 'SS_P_IV' ).AsFloat := Source.FieldByName( 'SS_P_IV' ).AsFloat;
          ParamByName( 'SS_P_SAR' ).AsFloat := Source.FieldByName( 'SS_P_SAR' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.TCambioMoveExcludedFields(Sender: TObject);
begin
     inherited;
     with TargetQuery do
     begin
          ParamByName( 'TC_FEC_INI' ).AsDate := STOD( Source.FieldByName( 'TB_CODIGO' ).AsString );
          ParamByName( 'TC_MONTO' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
          ParamByName( 'TC_NUMERO' ).AsFloat := 0;
          ParamByName( 'TC_TEXTO' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
     end;
end;

procedure TdmMigrarDatos.Extra1MoveExcludedFields(Sender: TObject);
begin
     with TargetQuery do
     begin
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'TB_NUMERO' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
          ParamByName( 'TB_TEXTO' ).AsString := Source.FieldByName( 'TB_LETRA' ).AsString;
     end;
end;

procedure TdmMigrarDatos.MaestroMoveExcludedFields(Sender: TObject);
begin
     with TargetQuery do
     begin
          ParamByName( 'MA_CODIGO' ).AsString := Source.FieldByName( 'MA_CODIGO' ).AsString;
          ParamByName( 'MA_NOMBRE' ).AsString := Limita( Source.FieldByName( 'MA_NOMBRE' ).AsString, 40 );
          ParamByName( 'MA_CEDULA' ).AsString := Source.FieldByName( 'MA_CEDULA' ).AsString;
          ParamByName( 'MA_RFC' ).AsString := Source.FieldByName( 'MA_RFC' ).AsString;
     end;
end;

procedure TdmMigrarDatos.CalCursoMoveExcludedFields(Sender: TObject);
begin
     MigrarFecha( 'CC_FECHA', Source.FieldByName( 'CA_FECHA' ).AsDateTime );
     with TargetQuery do
     begin
          ParamByName( 'CU_CODIGO' ).AsString := Trim( Source.FieldByName( 'CU_CURSO' ).AsString );
     end;
end;

procedure TdmMigrarDatos.EntrenaOpenDatasets(Sender: TObject);
begin
     inherited;
     FLista.Clear;
     if EsVer260 then
     begin
          with TablaMigrada do
          begin
               PrepareTargetQuery( tqUno, 'insert into ENTNIVEL ( PU_CODIGO, CU_CODIGO, ET_CODIGO ) values ( :PU_CODIGO, :CU_CODIGO, :ET_CODIGO )' );
          end;
     end;
end;

procedure TdmMigrarDatos.EntrenaCloseDatasets(Sender: TObject);
begin
     inherited;
     if EsVer260 then
     begin
          with TablaMigrada do
          begin
               CloseQuery( tqUno );
          end;
     end;
end;

procedure TdmMigrarDatos.EntrenaMoveExcludedFields(Sender: TObject);
var
   sLista: String;
begin
     inherited;
     MigrarBooleanos( 'EN_OPCIONA', 'PU_OPCIONA' );
     with Source do
     begin
          MigrarStringKey( 'PU_CODIGO', FieldByName( 'PU_PUESTO' ).AsString );
          MigrarStringKey( 'CU_CODIGO', Trim( FieldByName( 'CU_CURSO' ).AsString ) );
          if EsVer260 then
             sLista := ZetaCommonTools.zBoolToStr( StrLleno( FieldByName( 'PU_NIVEL' ).AsString ) )
          else
              sLista := K_GLOBAL_NO;
     end;
     with TargetQuery do
     begin
          ParamByName( 'EN_DIAS' ).AsInteger := Source.FieldByName( 'PU_DIAS' ).AsInteger;
          ParamByName( 'EN_LISTA' ).AsString := sLista;
     end;
end;

procedure TdmMigrarDatos.EntrenaMoveOtherTables(Sender: TObject);
var
   lLista: Boolean;
   sPuesto, sCurso: String;
   i: Integer;
begin
     inherited;
     if EsVer260 then
     begin
          with TargetQuery do
          begin
               lLista := ZetaCommonTools.zStrToBool( ParamByName( 'EN_LISTA' ).AsString );
               sPuesto := ParamByName( 'PU_CODIGO' ).AsString;
               sCurso := ParamByName( 'CU_CODIGO' ).AsString;
          end;
          if lLista then
          begin
               with FLista do
               begin
                    CommaText := Source.FieldByName( 'PU_NIVEL' ).AsString;
                    for i := 0 to ( Count - 1 ) do
                    begin
                         with tqUno do
                         begin
                              ParamByName( 'PU_CODIGO' ).AsString := sPuesto;
                              ParamByName( 'CU_CODIGO' ).AsString := sCurso;
                              ParamByName( 'ET_CODIGO' ).AsString := Strings[ i ];
                              ExecSQL;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TdmMigrarDatos.AgregarImagenes( const iCodigo: TNumEmp );
var
   i: Integer;

procedure FindAllFiles( const sFileSpec: String; FileList: TStrings );

procedure FindAllFilesOfType( const sFileSpec: String );
var
   iFound: Integer;
   FileInfo: TSearchRec;
   sPath: String;
begin
     sPath := ExtractFileDir( sFileSpec );
     with FileList do
     begin
          BeginUpdate;
          try
             iFound := FindFirst( sFileSpec, faAnyFile, FileInfo );
             try
                while ( iFound = 0 ) do
                begin
                     Add( sPath + '\' + FileInfo.Name );
                     iFound := FindNext( FileInfo );
                end;
             finally
                    FindClose( FileInfo );
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

begin
     FileList.Clear;
     FindAllFilesOfType( sFileSpec + '.BMP' );
     FindAllFilesOfType( sFileSpec + '.PCX' );
     FindAllFilesOfType( sFileSpec + '.JPG' );
end;

function AgregaUnaImagen( const sTipo, sFileName: String ): Boolean;
var
   sImageBuffer: String;
begin
     Result := FileExists( sFileName );
     if Result then
     begin
          if ( AnsiUpperCase( ExtractFileExt( sFileName ) ) <> '.JPG' ) then
          begin
               sImageBuffer := ExtractFileDir( Application.ExeName ) + '\IMAGEBUF.JPG';
               with PMultiImage do
               begin
                    ImageName := sFileName;
                    SaveAsJpg( sImageBuffer );
               end;
          end
          else
              sImageBuffer := sFileName;
          TablaMigrada.ClearParameters( tqUno );
          with tqUno do
          begin
               ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
               ParamByName( 'IM_TIPO' ).AsString := sTipo;
               ParamByName( 'IM_BLOB' ).LoadFromFile( sImageBuffer, ftBlob );
               ExecSQL;
          end;
     end;
end;

begin
     with FLista do
     begin
          { Primero las Fotos }
          FindAllFiles( DirFotos + IntToStr( iCodigo ), FLista );
          for i := 0 to ( Count - 1 ) do
          begin
               if ( i = 0 ) then
                  AgregaUnaImagen( 'FOTO', Strings[ i ] )
               else
                   AgregaUnaImagen( 'FOTO' + IntToStr( i ), Strings[ i ] );
          end;
          { Luego los Documentos }
          FindAllFiles( DirDocumentos + IntToStr( iCodigo ), FLista );
          for i := 0 to ( Count - 1 ) do
          begin
               AgregaUnaImagen( 'DOC' + IntToStr( i ), Strings[ i ] );
          end;
     end;
end;

procedure TdmMigrarDatos.ColaboraMoveOtherTables(Sender: TObject);
var
   iCodigo: TNumEmp;
   iFolio: Integer;
   dReferencia: TDate;

procedure AgregarAntesCurso( const sCurso: String );
begin
     if StrLleno( sCurso ) then
     begin
          TablaMigrada.ClearParameters( tqCuatro );
          MigrarFechaQuery( tqCuatro, 'AR_FECHA', NullDateTime );
          with tqCuatro do
          begin
               ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
               ParamByName( 'AR_FOLIO' ).AsInteger := iFolio;
               ParamByName( 'AR_CURSO' ).AsString := sCurso;
               ExecSQL;
          end;
          iFolio := iFolio + 1;
     end;
end;

procedure AgregarAntesPuesto( const sPuesto: String );
begin
     if StrLleno( sPuesto ) then
     begin
          TablaMigrada.ClearParameters( tqTres );
          MigrarFechaQuery( tqTres, 'AP_FEC_INI', NullDateTime );
          MigrarFechaQuery( tqTres, 'AP_FEC_FIN', NullDateTime );
          with tqTres do
          begin
               ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
               ParamByName( 'AP_FOLIO' ).AsInteger := iFolio;
               ParamByName( 'AP_PUESTO' ).AsString := sPuesto;
               ExecSQL;
          end;
          iFolio := iFolio + 1;
     end;
end;

procedure AgregarPariente( const sNombre, sSexo: String; const eRelacion: eTipoPariente; const dNacimiento: TDate );
begin
     TablaMigrada.ClearParameters( tqDos );
     MigrarFechaQuery( tqDos, 'PA_FEC_NAC', dNacimiento );
     with tqDos do
     begin
          ParamByName( 'CB_CODIGO' ).AsInteger := iCodigo;
          ParamByName( 'PA_RELACIO' ).AsInteger := Ord( eRelacion );
          ParamByName( 'PA_FOLIO' ).AsInteger := iFolio;
          ParamByName( 'PA_NOMBRE' ).AsString := sNombre;
          ParamByName( 'PA_SEXO' ).AsString := sSexo;
          ParamByName( 'PA_TRABAJA' ).AsInteger := 0;
          ParamByName( 'PA_NUMERO' ).AsInteger := 0;
          ExecSQL;
     end;
     iFolio := iFolio + 1;
end;

procedure AgregarPadre( const sNombre, sSexo: String; const eRelacion: eTipoPariente );
begin
     if StrLleno( sNombre ) then
        AgregarPariente( sNombre, sSexo, eRelacion, NullDateTime );
end;

procedure AgregarHijos( const sListaEdad, sSexo: String );
var
   i, iLen, iAnios, iYear: Integer;
begin
     iYear := TheYear( dReferencia );
     if ( iYear > 0 ) then
     begin
          i := 1;
          iLen := Length( sListaEdad );
          while ( i < iLen ) do
          begin
               iAnios := Str2Integer( Copy( sListaEdad, i, 2 ) );
               AgregarPariente( '', sSexo, paHijo, DateTheYear( dReferencia, ( iYear - iAnios ) ) );
               i := i + 2;
          end;
     end;
end;

begin
     inherited;
     with TargetQuery do
     begin
          iCodigo := ParamByName( 'CB_CODIGO' ).AsInteger;
     end;
     with Source do
     begin
          dReferencia := FieldByName( 'CB_FEC_SOL' ).AsDateTime;
          iFolio := 1;
          AgregarPadre( FieldByName( 'CB_PADRE' ).AsString, K_MASCULINO, paPadre );
          iFolio := 1;
          AgregarPadre( FieldByName( 'CB_MADRE' ).AsString, K_FEMENINO, paMadre );
          iFolio := 1;
          AgregarHijos( FieldByName( 'CB_EDAD_HS' ).AsString, K_MASCULINO );
          AgregarHijos( FieldByName( 'CB_EDAD_MS' ).AsString, K_FEMENINO );
          iFolio := 1;
          AgregarAntesPuesto( FieldByName( 'CB_PUESTO1' ).AsString );
          AgregarAntesPuesto( FieldByName( 'CB_PUESTO2' ).AsString );
          AgregarAntesPuesto( FieldByName( 'CB_PUESTO3' ).AsString );
          iFolio := 1;
          AgregarAntesCurso( FieldByName( 'CB_CURSO1' ).AsString );
          AgregarAntesCurso( FieldByName( 'CB_CURSO2' ).AsString );
          AgregarAntesCurso( FieldByName( 'CB_CURSO3' ).AsString );
     end;
     AgregarImagenes( iCodigo );
end;

procedure TdmMigrarDatos.ColaboraMoveExcludedFields(Sender: TObject);
const
     K_STATUS_INACTIVO = '9';
var
   FCampoAdicional: TField;
   sValor, sInfCred, sInfTipo, sZonaGeo, sSexo, sTipoRev, sClinica: String;
   iValor, iYear, iNumero, iOldTasa: Integer;
   dFecInt, dFecRev, dFecIng, dFecSal, dDerecho, dInfIni, dFecCon: TDate;
   rDerecho, rInfOld, rInfTasa: Extended;
   lInActivo: Boolean;
   eTipo: eTipoPeriodo;
   eInfonavit: eTipoInfonavit;
begin
     with Source do
     begin
          dFecInt := FieldByName( 'CB_FEC_INT' ).AsDateTime;
          dFecIng := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          dFecRev := FieldByName( 'CB_FEC_REV' ).AsDateTime;
          dFecCon := FieldByName( 'CB_FEC_CON' ).AsDateTime;
          if ( dFecRev > dFecInt ) then
             dFecSal := dFecRev
          else
              dFecSal := dFecInt;
          if ( dFecIng = dFecSal ) then
             sTipoRev := K_T_ALTA
          else
              sTipoRev := K_T_CAMBIO;
          if ( dFecCon = NullDateTime ) then
             dFecCon := dFecIng;
          sZonaGeo := FieldByName( 'CB_ZONA_GE' ).AsString;
          if not StrLleno( sZonaGeo ) then
             sZonaGeo := K_T_ZONA_A;
          sInfCred := FieldByName( 'CB_INFCRED' ).AsString;
          dInfIni := STOD( Copy( sInfCred, 12, 8 ) );
          sInfTipo := Copy( sInfCred, 11, 1 );
          iOldTasa := StrAsInteger( Copy( sInfCred, 20, 1 ) );
          sClinica := Copy( sInfCred, 21, 3 );
          sInfCred := Copy( sInfCred, 1, 10 );
          eInfonavit := ConvierteInfonavitTipo( sInfCred, sInfTipo );
          rInfTasa := FieldByName( 'CB_INFTASA' ).AsFloat;
          if ( rInfTasa > 0 ) and ( eInfonavit = tiNoTiene ) then
             eInfonavit := tiPorcentaje;
          if EsVer260 then
          begin
               { GA: Se elimin� para considerar Porcentajes }
               {
               if ( eInfonavit = tiCuotaFija ) then
               begin
               }
                    case iOldTasa of
                         2: rInfOld := 20;
                         3: rInfOld := 25;
                         4: rInfOld := 30;
                    else
                        rInfOld := 0;
                    end;
               {
               end
               else
                   rInfOld := 0;
               }
          end
          else
          begin
               if ( eInfonavit = tiCuotaFija ) and StrLleno( FInfOldField ) then
               begin
                    eInfonavit := tiPorcentaje;
                    FCampoAdicional := FieldByName( FInfOldField );
                    with FCampoAdicional do
                    begin
                         case DataType of
                              ftString: rInfOld := StrToReal( AsString );
                              ftSmallint: rInfOld := AsInteger;
                              ftInteger: rInfOld := AsInteger;
                              ftWord: rInfOld := AsInteger;
                              ftFloat: rInfOld := AsFloat;
                              ftCurrency: rInfOld := AsFloat;
                         else
                             rInfOld := 0;
                         end;
                    end;
               end
               else
                   rInfOld := 0;
          end;
          sValor := FieldByName( 'CB_COD_AUS' ).AsString;
          rDerecho := FieldByName( 'CB_V_ID_1' ).AsFloat + FieldByName( 'CB_V_ID_2' ).AsFloat;
          iValor := FieldByName( 'CB_CONGELA' ).AsInteger;
          if ( iValor = 0 ) then
             dDerecho := NullDateTime
          else
          begin
               dDerecho := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
               dDerecho := DateTheYear( dDerecho, TheYear( dDerecho ) + iValor );
          end;
          sSexo := FieldByName( 'CB_SEXO' ).AsString;
          if ( sSexo <> K_MASCULINO ) then
             sSexo := K_FEMENINO;
          lInActivo := ( FieldByName( 'CB_STATUS' ).AsString = K_STATUS_INACTIVO );
          if lInActivo then
          begin
               iYear := TheYear( FieldByName( 'CB_FEC_BAJ' ).AsDateTime );
               eTipo := tpSemanal; // � Debe ser tipo default de COMPANY.REC ? //;
               iNumero := Str2Integer( FieldByName( 'PE_NUMERO' ).AsString );
          end
          else
          begin
               iYear := 0;
               eTipo := tpDiario;
               iNumero := 0;
          end;
          MigrarFecha( 'CB_FEC_ANT', FieldByName( 'CB_FEC_ANT' ).AsDateTime );
          MigrarFecha( 'CB_FEC_BAJ', FieldByName( 'CB_FEC_BAJ' ).AsDateTime );
          MigrarFecha( 'CB_FEC_BSS', FieldByName( 'CB_FEC_BSS' ).AsDateTime );
          //MigrarFecha( 'CB_FEC_CON', FieldByName( 'CB_FEC_CON' ).AsDateTime );
          MigrarFecha( 'CB_FEC_NAC', FieldByName( 'CB_FEC_NAC' ).AsDateTime );
          MigrarFecha( 'CB_G_FEC_1', FieldByName( 'CB_G_FEC_1' ).AsDateTime );
          MigrarFecha( 'CB_G_FEC_2', FieldByName( 'CB_G_FEC_2' ).AsDateTime );
          MigrarFecha( 'CB_G_FEC_3', FieldByName( 'CB_G_FEC_3' ).AsDateTime );
          MigrarFecha( 'CB_LAST_EV', FieldByName( 'CB_LAST_EV' ).AsDateTime );
          MigrarFecha( 'CB_NEXT_EV', FieldByName( 'CB_NEXT_EV' ).AsDateTime );
          MigrarFecha( 'CB_FEC_RES', FieldByName( 'CB_EN_TIJ' ).AsDateTime );
          MigrarStringKeyNull( 'CB_MOT_BAJ', FieldByName( 'CB_MOT_BAJ' ).AsString );
          MigrarStringKey( 'CB_EDO_CIV', FieldByName( 'CB_EDO_CIV' ).AsString );
          MigrarStringKey( 'CB_ESTADO', Estado );
          MigrarStringKey( 'CB_ESTUDIO', FieldByName( 'CB_ESTUDIO' ).AsString );
          MigrarStringKey( 'CB_G_TAB_1', FieldByName( 'CB_G_TAB_1' ).AsString );
          MigrarStringKey( 'CB_G_TAB_2', FieldByName( 'CB_G_TAB_2' ).AsString );
          MigrarStringKey( 'CB_G_TAB_3', FieldByName( 'CB_G_TAB_3' ).AsString );
          MigrarStringKey( 'CB_G_TAB_4', FieldByName( 'CB_G_TAB_4' ).AsString );
          MigrarStringKey( 'CB_MED_TRA', FieldByName( 'CB_MED_TRA' ).AsString );
          MigrarStringKey( 'CB_VIVECON', FieldByName( 'CB_VIVECON' ).AsString );
          MigrarStringKey( 'CB_VIVEEN', FieldByName( 'CB_VIVEEN' ).AsString );
          MigrarStringKey( 'CB_CONTRAT', FieldByName( 'CB_CONTRAT' ).AsString );
          MigrarStringKey( 'CB_TABLASS', FieldByName( 'CB_TABLASS' ).AsString );
          MigrarStringKey( 'CB_TURNO', FieldByName( 'CB_HORARIO' ).AsString );
          MigrarStringKey( 'CB_CLASIFI', FieldByName( 'CB_CLASIFI' ).AsString );
          MigrarStringKey( 'CB_PUESTO', FieldByName( 'CB_PUESTO' ).AsString );
          MigrarStringKey( 'CB_TIP_REV', sTipoRev );
          MigrarStringKey( 'CB_PATRON', FieldByName( 'CB_PATRON' ).AsString );
     end;
     MigrarCodigoEmpleado;
     MigrarNiveles( 'CB_NIVEL', 'CB_NIVELES' );
     MigrarBooleanos( 'CB_AUTOSAL', 'CB_AUTOSAL' );
     MigrarBooleanos( 'CB_CHECA', 'CB_CHECA' );
     MigrarBooleanos( 'CB_G_LOG_1', 'CB_G_LOG_1' );
     MigrarBooleanos( 'CB_G_LOG_2', 'CB_G_LOG_2' );
     MigrarBooleanos( 'CB_G_LOG_3', 'CB_G_LOG_3' );
     MigrarBooleanos( 'CB_INFMANT', 'CB_INFMANT' );
     MigrarSiNo( 'CB_EST_HOY', 'CB_EST_HOY' );
     MigrarSiNo( 'CB_HABLA', 'CB_HABLA' );
     MigrarSiNo( 'CB_PASAPOR', 'CB_MICA' );
     MigrarFecha( 'CB_DER_FEC', dDerecho );
     MigrarFechaNull( 'CB_FEC_INC', CTOD( Copy( sValor, 1, 8 ) ) );
     MigrarFecha( 'CB_FEC_VAC', CTOD( Copy( sValor, 31, 8 ) ) );
     MigrarFechaNull( 'CB_FEC_PER', CTOD( Copy( sValor, 61, 8 ) ) );
     MigrarFecha( 'CB_FEC_INT', dFecInt );
     MigrarFecha( 'CB_FEC_ING', dFecIng );
     MigrarFecha( 'CB_FEC_REV', dFecRev );
     MigrarFecha( 'CB_FEC_SAL', dFecSal );
     MigrarFecha( 'CB_INF_INI', dInfIni );
     MigrarFecha( 'CB_FEC_CON', dFecCon );
     with TargetQuery do
     begin
          ParamByName( 'CB_ACTIVO' ).AsString := zBoolToStr( not lInActivo );
          ParamByName( 'CB_NOMYEAR' ).AsInteger := iYear;
          ParamByName( 'CB_NOMTIPO' ).AsInteger := Ord( eTipo );
          ParamByName( 'CB_NOMNUME' ).AsInteger := iNumero;
          ParamByName( 'CB_ZONA_GE' ).AsString := sZonaGeo;
          ParamByName( 'CB_SEXO' ).AsString := sSexo;
          ParamByName( 'CB_INFCRED' ).AsString := sInfCred;
          ParamByName( 'CB_INFTIPO' ).AsInteger := Ord( eInfonavit );
          ParamByName( 'CB_INFTASA' ).AsFloat := rInfTasa;
          ParamByName( 'CB_INF_OLD' ).AsFloat := rInfOld;
          ParamByName( 'CB_CLINICA' ).AsString := sClinica;
          ParamByName( 'CB_DER_PAG' ).AsFloat := rDerecho;
          ParamByName( 'CB_V_PAGO' ).AsFloat := Source.FieldByName( 'CB_V_PA_0' ).AsFloat + Source.FieldByName( 'CB_V_PA_1' ).AsFloat + Source.FieldByName( 'CB_V_PA_2' ).AsFloat; // + VL
          ParamByName( 'CB_DER_GOZ' ).AsFloat := rDerecho;
          {
          ParamByName( 'CB_V_GOZO' ).AsFloat := Source.FieldByName( 'CB_V_GO_1' ).AsFloat + Source.FieldByName( 'CB_V_GO_2' ).AsFloat; // + VL
          }
          ParamByName( 'CB_V_GOZO' ).AsFloat := Source.FieldByName( 'CB_V_GO_0' ).AsFloat + Source.FieldByName( 'CB_V_GO_1' ).AsFloat + Source.FieldByName( 'CB_V_GO_2' ).AsFloat;
          ParamByName( 'CB_CALLE' ).AsString := Source.FieldByName( 'CB_DIR1' ).AsString;
          ParamByName( 'CB_COLONIA' ).AsString := Source.FieldByName( 'CB_DIR2' ).AsString;
          ParamByName( 'CB_APE_MAT' ).AsString := Source.FieldByName( 'CB_APE_MAT' ).AsString;
          ParamByName( 'CB_APE_PAT' ).AsString := Source.FieldByName( 'CB_APE_PAT' ).AsString;
          ParamByName( 'CB_BAN_ELE' ).AsString := Source.FieldByName( 'CB_BAN_ELE' ).AsString;
          ParamByName( 'CB_CARRERA' ).AsString := Limita( Source.FieldByName( 'CB_CARRERA' ).AsString, 40 );
          ParamByName( 'CB_CIUDAD' ).AsString := Source.FieldByName( 'CB_CIUDAD' ).AsString;
          ParamByName( 'CB_CODPOST' ).AsString := Source.FieldByName( 'CB_CODPOST' ).AsString;
          ParamByName( 'CB_CREDENC' ).AsString := Source.FieldByName( 'CB_CREDENC' ).AsString;
          ParamByName( 'CB_CURP' ).AsString := Source.FieldByName( 'CB_CURP' ).AsString;
          ParamByName( 'CB_EST_HOR' ).AsString := Source.FieldByName( 'CB_EST_HOR' ).AsString;
          ParamByName( 'CB_EVALUA' ).AsFloat := Source.FieldByName( 'CB_EVALUA' ).AsFloat;
          ParamByName( 'CB_EXPERIE' ).AsString := Source.FieldByName( 'CB_EXPERIE' ).AsString;
          ParamByName( 'CB_G_NUM_1' ).AsFloat := Source.FieldByName( 'CB_G_NUM_1' ).AsFloat;
          ParamByName( 'CB_G_NUM_2' ).AsFloat := Source.FieldByName( 'CB_G_NUM_2' ).AsFloat;
          ParamByName( 'CB_G_NUM_3' ).AsFloat := Source.FieldByName( 'CB_G_NUM_3' ).AsFloat;
          ParamByName( 'CB_G_TEX_1' ).AsString := Source.FieldByName( 'CB_G_TEX_1' ).AsString;
          ParamByName( 'CB_G_TEX_2' ).AsString := Source.FieldByName( 'CB_G_TEX_2' ).AsString;
          ParamByName( 'CB_G_TEX_3' ).AsString := Source.FieldByName( 'CB_G_TEX_3' ).AsString;
          ParamByName( 'CB_G_TEX_4' ).AsString := Source.FieldByName( 'CB_G_TEX_4' ).AsString;
          ParamByName( 'CB_IDIOMA' ).AsString := Source.FieldByName( 'CB_IDIOMA' ).AsString;
          ParamByName( 'CB_LA_MAT' ).AsString := Source.FieldByName( 'CB_LA_MAT' ).AsString;
          ParamByName( 'CB_LUG_NAC' ).AsString := Source.FieldByName( 'CB_LUG_NAC' ).AsString;
          ParamByName( 'CB_MAQUINA' ).AsString := Source.FieldByName( 'CB_MAQUINA' ).AsString;
          ParamByName( 'CB_NACION' ).AsString := Source.FieldByName( 'CB_NACION' ).AsString;
          ParamByName( 'CB_NOMBRES' ).AsString := Source.FieldByName( 'CB_NOMBRES' ).AsString;
          ParamByName( 'CB_RFC' ).AsString := Source.FieldByName( 'CB_RFC' ).AsString;
          ParamByName( 'CB_SAL_INT' ).AsFloat := Source.FieldByName( 'CB_SAL_INT' ).AsFloat;
          ParamByName( 'CB_SALARIO' ).AsFloat := Source.FieldByName( 'CB_SALARIO' ).AsFloat;
          ParamByName( 'CB_SEGSOC' ).AsString := Source.FieldByName( 'CB_SEGSOC' ).AsString;
          ParamByName( 'CB_TEL' ).AsString := Source.FieldByName( 'CB_TEL' ).AsString;
          ParamByName( 'CB_ZONA' ).AsString := Source.FieldByName( 'CB_ZONA' ).AsString;
          ParamByName( 'CB_OLD_SAL' ).AsFloat := Source.FieldByName( 'CB_OLD_SAL' ).AsFloat;
          ParamByName( 'CB_OLD_INT' ).AsFloat := Source.FieldByName( 'CB_OLD_INT' ).AsFloat;
          ParamByName( 'CB_TOT_GRA' ).AsFloat := Source.FieldByName( 'CB_TOT_GRA' ).AsFloat;
          ParamByName( 'CB_SAL_TOT' ).AsFloat := Source.FieldByName( 'CB_SAL_TOT' ).AsFloat;
          ParamByName( 'CB_PER_VAR' ).AsFloat := Source.FieldByName( 'CB_PER_VAR' ).AsFloat;
          ParamByName( 'CB_PRE_INT' ).AsFloat := Source.FieldByName( 'CB_PRE_INT' ).AsFloat;
          ParamByName( 'CB_FAC_INT' ).AsFloat := Source.FieldByName( 'CB_FAC_INT' ).AsFloat;
          ParamByName( 'CB_RANGO_S' ).AsFloat := Source.FieldByName( 'CB_RANGO_S' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.ColaboraOpenDatasets(Sender: TObject);
begin
     with TablaMigrada do
     begin
          DeleteTable( 'ANTESCUR' );
          DeleteTable( 'ANTESPTO' );
          DeleteTable( 'PARIENTE' );
          DeleteTable( 'IMAGEN' );
          PrepareTargetQuery( tqCuatro, 'insert into ANTESCUR ' +
                                        '( CB_CODIGO, AR_FOLIO, AR_CURSO, AR_FECHA) values ' +
                                        '(:CB_CODIGO,:AR_FOLIO,:AR_CURSO,:AR_FECHA)' );
          PrepareTargetQuery( tqTres, 'insert into ANTESPTO ' +
                                      '( CB_CODIGO, AP_FOLIO, AP_PUESTO, AP_FEC_INI, AP_FEC_FIN) values ' +
                                      '(:CB_CODIGO,:AP_FOLIO,:AP_PUESTO,:AP_FEC_INI,:AP_FEC_FIN)' );
          PrepareTargetQuery( tqDos, 'insert into PARIENTE ' +
                                     '( CB_CODIGO, PA_RELACIO, PA_FOLIO, PA_NOMBRE, PA_SEXO, PA_TRABAJA, PA_NUMERO, PA_FEC_NAC) values ' +
                                     '(:CB_CODIGO,:PA_RELACIO,:PA_FOLIO,:PA_NOMBRE,:PA_SEXO,:PA_TRABAJA,:PA_NUMERO,:PA_FEC_NAC)' );
          PrepareTargetQuery( tqUno, 'insert into IMAGEN (CB_CODIGO,IM_BLOB,IM_TIPO) values (:CB_CODIGO,:IM_BLOB,:IM_TIPO)' );
     end;
end;

procedure TdmMigrarDatos.ColaboraCloseDatasets(Sender: TObject);
begin
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
          CloseQuery( tqDos );
          CloseQuery( tqTres );
          CloseQuery( tqCuatro );
     end;
end;

procedure TdmMigrarDatos.TAhorroMoveExcludedFields(Sender: TObject);
begin
     FAhorro.Add( Source.FieldByName( 'TB_CODIGO' ).AsString );
     TPrestaMoveExcludedFields( Sender );
end;

procedure TdmMigrarDatos.TPrestaMoveExcludedFields(Sender: TObject);
var
   sValor: String;
begin
     with Source do
     begin
          sValor := FieldByName( 'TB_LETRA' ).AsString;
          MigrarIntegerKeyNull( 'TB_CONCEPT', Str2Integer( Copy( sValor, 1, 3 ) ) );
          MigrarIntegerKeyNull( 'TB_RELATIV', Str2Integer( Copy( sValor, 4, 3 ) ) );
     end;
     with TargetQuery do
     begin
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_LIQUIDA' ).AsInteger := BoolToInt( Copy( sValor, 7, 1 ) = K_GLOBAL_SI );
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'TB_ALTA' ).AsInteger := Ord( ConvierteAltaAhorroPrestamo( Copy( sValor, 8, 1 ) ) );
     end;
end;

procedure TdmMigrarDatos.TPrestaCloseDatasets(Sender: TObject);
var
   iTipo, iPrioridad: Integer;
   sCodigo: String;
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select A.TB_CODIGO CODIGO, 0 as TIPO from TAHORRO A union select P.TB_CODIGO CODIGO, 1 as TIPO from TPRESTA P order by 1 desc' );
          PrepareTargetQuery( tqDos, 'update TAHORRO set TB_PRIORID = :Prioridad where ( TAHORRO.TB_CODIGO = :Codigo )' );
          PrepareTargetQuery( tqTres, 'update TPRESTA set TB_PRIORID = :Prioridad where ( TPRESTA.TB_CODIGO = :Codigo )' );
     end;
     iPrioridad := 1;
     with tqUno do
     begin
          Active := True;
          First;
          while not Eof do
          begin
               sCodigo := FieldByName( 'CODIGO' ).AsString;
               iTipo := FieldByName( 'TIPO' ).AsInteger;
               try
                  case iTipo of
                       0:
                       with tqDos do
                       begin
                            ParamByName( 'Codigo' ).AsString := sCodigo;
                            ParamByName( 'Prioridad' ).AsInteger := iPrioridad;
                            ExecSQL;
                       end;
                       1:
                       with tqTres do
                       begin
                            ParamByName( 'Codigo' ).AsString := sCodigo;
                            ParamByName( 'Prioridad' ).AsInteger := iPrioridad;
                            ExecSQL;
                       end;
                  end;
               except
               end;
               Inc( iPrioridad );
               Next;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqTres );
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.ACar_AboFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     Accept := EsAhorro( Dataset.FieldByName( 'AH_TIPO' ).AsString );
end;

procedure TdmMigrarDatos.ACar_AboMoveExcludedFields(Sender: TObject);
var
   rCargo, rAbono: Extended;
begin
     with Source do
     begin
          rCargo := FieldByName( 'CR_MONTO' ).AsFloat;
          rAbono := 0;
          if ( FieldByName( 'CR_CAR_ABO' ).AsString <> K_CARGO ) then
          begin
               rAbono := rCargo;
               rCargo := 0;
          end;
     end;
     MigrarCodigoEmpleado;
     MigrarUsuario( 'US_CODIGO', 'CR_USER' );
     MigrarFecha( 'CR_CAPTURA', Source.FieldByName( 'CR_CAPTURA' ).AsDateTime );
     MigrarFecha( 'CR_FECHA', Source.FieldByName( 'CR_FECHA' ).AsDateTime );
     with TargetQuery do
     begin
          ParamByName( 'CR_CARGO' ).AsFloat := rCargo;
          ParamByName( 'CR_ABONO' ).AsFloat := rAbono;
          ParamByName( 'AH_TIPO' ).AsString := Source.FieldByName( 'AH_TIPO' ).AsString;
          ParamByName( 'CR_OBSERVA' ).AsString := Source.FieldByName( 'CR_OBSERVA' ).AsString;
     end;
end;

procedure TdmMigrarDatos.ACar_AboCloseDatasets(Sender: TObject);
begin
     inherited;
     // Recalcular Ahorros
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select distinct(CB_CODIGO) from AHORRO order by CB_CODIGO' );
          PrepareTargetQuery( tqDos, 'execute procedure RECALCULA_AHORROS( :CB_CODIGO, 6 )' );
     end;
     with tqUno do
     begin
          Active := True;
          First;
          while not Eof do
          begin
               try
                  tqDos.ParamByName( 'CB_CODIGO' ).AsInteger := FieldByName( 'CB_CODIGO' ).AsInteger;
                  tqDos.ExecSQL;
               except
               end;
               Next;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.PCar_AboFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     Accept := not EsAhorro( Dataset.FieldByName( 'AH_TIPO' ).AsString );
end;

procedure TdmMigrarDatos.PCar_AboMoveExcludedFields(Sender: TObject);
var
   rCargo, rAbono: Extended;
begin
     with Source do
     begin
          MigrarFecha( 'CR_CAPTURA', FieldByName( 'CR_CAPTURA' ).AsDateTime );
          MigrarFecha( 'CR_FECHA', FieldByName( 'CR_FECHA' ).AsDateTime );
          rCargo := FieldByName( 'CR_MONTO' ).AsFloat;
          rAbono := 0;
          if ( FieldByName( 'CR_CAR_ABO' ).AsString <> K_CARGO ) then
          begin
               rAbono := rCargo;
               rCargo := 0;
          end;
     end;
     MigrarCodigoEmpleado;
     MigrarUsuario( 'US_CODIGO', 'CR_USER' );
     with TargetQuery do
     begin
          ParamByName( 'CR_CARGO' ).AsFloat := rCargo;
          ParamByName( 'CR_ABONO' ).AsFloat := rAbono;
          ParamByName( 'CR_OBSERVA' ).AsString := Source.FieldByName( 'CR_OBSERVA' ).AsString;
          ParamByName( 'PR_REFEREN' ).AsString := Trim( Source.FieldByName( 'AH_REFEREN' ).AsString );
          ParamByName( 'PR_TIPO' ).AsString := Source.FieldByName( 'AH_TIPO' ).AsString;
     end;
end;

procedure TdmMigrarDatos.PCar_AboCloseDatasets(Sender: TObject);
begin
     inherited;
     // Recalcular Prestamos
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select distinct(CB_CODIGO) from PRESTAMO order by CB_CODIGO' );
          PrepareTargetQuery( tqDos, 'execute procedure RECALCULA_PRESTAMOS( :CB_CODIGO, 6 )' );
     end;
     with tqUno do
     begin
          Active := True;
          First;
          while not Eof do
          begin
               try
                  tqDos.ParamByName( 'CB_CODIGO' ).AsFloat := FieldByName( 'CB_CODIGO' ).AsInteger;
                  tqDos.ExecSQL;
               except
               end;
               Next;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.AhorroMoveExcludedFields(Sender: TObject);
var
   rValor, rTotal, rSaldoIni: TPesos;
   sTipo: String;
   iEmpleado: TNumEmp;
   iNumero: Integer;
{$ifdef AJUSTA_AHORROS}
procedure AjustaSaldo( const sTipo: String; const iEmpleado: TNumEmp; var rSaldoIni, rTotal: TPesos; var iNumero: Integer );
var
   iConcepto: Integer;
   rTotalNew: TPesos;
begin
     ParamAsString( 'Tipo', sTipo, tqUno );
     with tqUno do
     begin
          Active := True;
          if IsEmpty then
             iConcepto := 0
          else
              iConcepto := Fields[ 0 ].AsInteger;
          Active := False;
     end;
     if ( iConcepto > 0 ) then
     begin
          ParamAsInteger( 'Empleado', iEmpleado, tqDos );
          ParamAsInteger( 'Concepto', iConcepto, tqDos );
          with tqDos do
          begin
               Active := True;
               if not IsEmpty then
               begin
                    iNumero := Fields[ 0 ].AsInteger;
                    rTotalNew := Fields[ 1 ].AsFloat;
                    rSaldoIni := rSaldoIni + rMax( ( rTotal - rTotalNew ), 0 );
                    rTotal := rTotalNew;
               end;
               Active := False;
          end;
     end;
end;
{$endif}
begin
     with Source do
     begin
          rValor := FieldByName( 'AH_ABONOS' ).AsFloat -
                    FieldByName( 'AH_CARGOS' ).AsFloat;
          iNumero := FieldByName( 'AH_NUMERO' ).AsInteger;
          rTotal := FieldByName( 'AH_TOTAL' ).AsFloat;
          rSaldoIni := FieldByName( 'AH_SALDO_I' ).AsFloat;
          sTipo := FieldByName( 'AH_TIPO' ).AsString;
          iEmpleado := LeeCodigoEmpleado;
     end;
     MigrarStringKey( 'AH_TIPO', sTipo );
     MigrarEmpleado( iEmpleado );
     MigrarUsuario( 'US_CODIGO', 'AH_USER' );
     MigrarFecha( 'AH_FECHA', Source.FieldByName( 'AH_FECHA' ).AsDateTime );
{$ifdef AJUSTA_AHORROS}
     AjustaSaldo( sTipo, iEmpleado, rSaldoIni, rTotal, iNumero );
{$endif}
     with TargetQuery do
     begin
          ParamByName( 'AH_STATUS' ).AsInteger := Ord( ConvierteStatusAhorro( Source.FieldByName( 'AH_STATUS' ).AsInteger ) );
          ParamByName( 'AH_FORMULA' ).AsString := VerificaFormula( Source.FieldByName( 'AH_FORMULA' ).AsString, Format( 'Emp %d Tipo %s: F�rmula ( AHORRO.AH_FORMULA )', [ iEmpleado, sTipo ] ), False );
          ParamByName( 'AH_NUMERO' ).AsInteger := iNumero;
          ParamByName( 'AH_SALDO_I' ).AsFloat := rSaldoIni;
          ParamByName( 'AH_ABONOS' ).AsFloat := Source.FieldByName( 'AH_ABONOS' ).AsFloat;
          ParamByName( 'AH_CARGOS' ).AsFloat := Source.FieldByName( 'AH_CARGOS' ).AsFloat;
          ParamByName( 'AH_TOTAL' ).AsFloat := rTotal;
          ParamByName( 'AH_SALDO' ).AsFloat := rSaldoIni + rTotal + rValor;
     end;
end;

procedure TdmMigrarDatos.AhorroFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     Accept := EsAhorro( Dataset.FieldByName( 'AH_TIPO' ).AsString );
end;

procedure TdmMigrarDatos.AhorroOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select TB_CONCEPT from TAHORRO where ( TB_CODIGO = :Tipo )' );
          PrepareTargetQuery( tqDos, 'select COUNT(*), SUM( M.MO_DEDUCCI ) from MOVIMIEN M join NOMINA N on ' +
                                     '( N.PE_YEAR = M.PE_YEAR ) and ' +
                                     '( N.PE_TIPO = M.PE_TIPO ) and ' +
                                     '( N.PE_NUMERO = M.PE_NUMERO ) and ' +
                                     '( N.CB_CODIGO = :Empleado ) ' +
                                     ' where ( M.CB_CODIGO = :Empleado ) and ' +
                                     '( M.CO_NUMERO = :Concepto ) and ' +
                                     '( M.MO_ACTIVO = ' + EntreComillas( 'S' ) + ' ) and ' +
                                     '( N.NO_STATUS = :Status ) ' );
     end;
     ParamAsInteger( 'Status', Ord( spAfectadaTotal ), tqDos );
end;

procedure TdmMigrarDatos.AhorroCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.PrestamoMoveExcludedFields(Sender: TObject);
var
   rValor, rTotal, rSaldo, rSaldoIni: TPesos;
   sTipo, sReferencia: String;
   iEmpleado: TNumEmp;
   iNumero: Integer;
   eStatus: eStatusPrestamo;
{$ifdef AJUSTA_PRESTAMOS}
procedure AjustaSaldo( const sTipo, sReferencia: String; const iEmpleado: TNumEmp; var rSaldoIni, rTotal: TPesos; var iNumero: Integer );
var
   iConcepto: Integer;
   rTotalNew: TPesos;
begin
     ParamAsString( 'Tipo', sTipo, tqUno );
     with tqUno do
     begin
          Active := True;
          if IsEmpty then
             iConcepto := 0
          else
              iConcepto := Fields[ 0 ].AsInteger;
          Active := False;
     end;
     if ( iConcepto > 0 ) then
     begin
          ParamAsInteger( 'Empleado', iEmpleado, tqDos );
          ParamAsInteger( 'Concepto', iConcepto, tqDos );
          ParamAsString( 'Referencia', sReferencia, tqDos );
          with tqDos do
          begin
               Active := True;
               if not IsEmpty then
               begin
                    iNumero := Fields[ 0 ].AsInteger;
                    rTotalNew := Fields[ 1 ].AsFloat;
                    rSaldoIni := rSaldoIni + rMax( ( rTotal - rTotalNew ), 0 );
                    rTotal := rTotalNew;
               end;
               Active := False;
          end;
     end;
end;
{$endif}
begin
     with Source do
     begin
          rValor := FieldByName( 'AH_MONTO' ).AsFloat -
                    FieldByName( 'AH_ABONOS' ).AsFloat +
                    FieldByName( 'AH_CARGOS' ).AsFloat;
          iNumero := FieldByName( 'AH_NUMERO' ).AsInteger;
          rSaldoIni := FieldByName( 'AH_SALDO_I' ).AsFloat;
          rTotal := FieldByName( 'AH_TOTAL' ).AsFloat;
          sTipo := FieldByName( 'AH_TIPO' ).AsString;
          sReferencia := Trim( FieldByName( 'AH_REFEREN' ).AsString );
          iEmpleado := LeeCodigoEmpleado;
          eStatus := ConvierteStatusPrestamo( FieldByName( 'AH_STATUS' ).AsInteger );
{$ifdef AJUSTA_PRESTAMOS}
          AjustaSaldo( sTipo, sReferencia, iEmpleado, rSaldoIni, rTotal, iNumero );
{$endif}
          rSaldo := rValor - rSaldoIni - rTotal;
     end;
     MigrarEmpleado( iEmpleado );
     MigrarStringKey( 'PR_TIPO', sTipo );
     MigrarFecha( 'PR_FECHA', Source.FieldByName( 'AH_FECHA' ).AsDateTime );
     MigrarUsuario( 'US_CODIGO', 'AH_USER' );
{$ifdef FALSE}
     if ( eStatus = spSaldado ) and ( rSaldo <> 0 ) then  { Se debe Cancelar }
{$else}
     if ( eStatus = spSaldado ) then  { Se debe Cancelar para evitar que se recalcule despues }
{$endif}
     begin
          eStatus := spCancelado;
          Bitacora.AgregaError( Format( 'Empleado %d Se Le Cancel� Prestamo Tipo %s : Ten�a Status Saldado', [ iEmpleado, sTipo ] ) );
     end;
     with TargetQuery do
     begin
          ParamByName( 'PR_REFEREN' ).AsString := sReferencia;
          ParamByName( 'PR_STATUS' ).AsInteger := Ord( eStatus );
          ParamByName( 'PR_FORMULA' ).AsString := VerificaFormula( Source.FieldByName( 'AH_FORMULA' ).AsString, Format( 'Emp %d Tipo %s: F�rmula ( PRESTAMO.PR_FORMULA )', [ iEmpleado, sTipo ] ), False );
          ParamByName( 'PR_NUMERO' ).AsInteger := iNumero;
          ParamByName( 'PR_MONTO' ).AsFloat := Source.FieldByName( 'AH_MONTO' ).AsFloat;
          ParamByName( 'PR_SALDO_I' ).AsFloat := rSaldoIni;
          ParamByName( 'PR_TOTAL' ).AsFloat := rTotal;
          ParamByName( 'PR_CARGOS' ).AsFloat := Source.FieldByName( 'AH_CARGOS' ).AsFloat;
          ParamByName( 'PR_ABONOS' ).AsFloat := Source.FieldByName( 'AH_ABONOS' ).AsFloat;
          ParamByName( 'PR_SALDO' ).AsFloat := rSaldo;
     end;
end;

procedure TdmMigrarDatos.PrestamoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     Accept := not EsAhorro( Dataset.FieldByName( 'AH_TIPO' ).AsString );
end;

procedure TdmMigrarDatos.PrestamoOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select TB_CONCEPT from TPRESTA where ( TB_CODIGO = :Tipo )' );
          PrepareTargetQuery( tqDos, 'select COUNT(*), SUM( M.MO_DEDUCCI ) from MOVIMIEN M join NOMINA N on ' +
                                     '( N.PE_YEAR = M.PE_YEAR ) and ' +
                                     '( N.PE_TIPO = M.PE_TIPO ) and ' +
                                     '( N.PE_NUMERO = M.PE_NUMERO ) and ' +
                                     '( N.CB_CODIGO = :Empleado ) ' +
                                     ' where ( M.CB_CODIGO = :Empleado ) and ' +
                                     '( M.CO_NUMERO = :Concepto ) and ' +
                                     '( M.MO_REFEREN = :Referencia ) and ' +
                                     '( M.MO_ACTIVO = ' + EntreComillas( 'S' ) + ' ) and ' +
                                     '( N.NO_STATUS = :Status ) ' );
     end;
     ParamAsInteger( 'Status', Ord( spAfectadaTotal ), tqDos );
end;

procedure TdmMigrarDatos.PrestamoCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.AcumulaMoveExcludedFields(Sender: TObject);
var
   rMes1, rMes2, rMes3, rMes4, rMes5, rMes6, rMes7, rMes8, rMes9, rMes10, rMes11, rMes12, rMes13: TPesos;
begin
     with Source do
     begin
          rMes1 := FieldByName( 'AC_MES_01' ).AsFloat;
          rMes2 := FieldByName( 'AC_MES_02' ).AsFloat;
          rMes3 := FieldByName( 'AC_MES_03' ).AsFloat;
          rMes4 := FieldByName( 'AC_MES_04' ).AsFloat;
          rMes5 := FieldByName( 'AC_MES_05' ).AsFloat;
          rMes6 := FieldByName( 'AC_MES_06' ).AsFloat;
          rMes7 := FieldByName( 'AC_MES_07' ).AsFloat;
          rMes8 := FieldByName( 'AC_MES_08' ).AsFloat;
          rMes9 := FieldByName( 'AC_MES_09' ).AsFloat;
          rMes10 := FieldByName( 'AC_MES_10' ).AsFloat;
          rMes11 := FieldByName( 'AC_MES_11' ).AsFloat;
          rMes12 := FieldByName( 'AC_MES_12' ).AsFloat;
          rMes13 := FieldByName( 'AC_MES_13' ).AsFloat;
     end;
     MigrarCodigoEmpleado;
     with TargetQuery do
     begin
          ParamByName( 'CO_NUMERO' ).AsInteger := ConvierteConcepto( Source.FieldByName( 'CO_NUMERO' ).AsString );
          ParamByName( 'AC_YEAR' ).AsInteger := Anio;
          ParamByName( 'AC_ANUAL' ).AsFloat := rMes1 + rMes2 + rMes3 + rMes4 + rMes5 + rMes6 + rMes7 + rMes8 + rMes9 + rMes10 + rMes11 + rMes12 + rMes13;
          ParamByName( 'AC_MES_01' ).AsFloat := rMes1;
          ParamByName( 'AC_MES_02' ).AsFloat := rMes2;
          ParamByName( 'AC_MES_03' ).AsFloat := rMes3;
          ParamByName( 'AC_MES_04' ).AsFloat := rMes4;
          ParamByName( 'AC_MES_05' ).AsFloat := rMes5;
          ParamByName( 'AC_MES_06' ).AsFloat := rMes6;
          ParamByName( 'AC_MES_07' ).AsFloat := rMes7;
          ParamByName( 'AC_MES_08' ).AsFloat := rMes8;
          ParamByName( 'AC_MES_09' ).AsFloat := rMes9;
          ParamByName( 'AC_MES_10' ).AsFloat := rMes10;
          ParamByName( 'AC_MES_11' ).AsFloat := rMes11;
          ParamByName( 'AC_MES_12' ).AsFloat := rMes12;
          ParamByName( 'AC_MES_13' ).AsFloat := rMes13;
     end;
end;

procedure TdmMigrarDatos.AusenciaMoveExcludedFields(Sender: TObject);
var
   rValor, rHoras: Extended;
   eStatus: eStatusAusencia;
   eTipo: eTipoPeriodo;
   iAnio, iNumero: Integer;
begin
     with Source do
     begin
          iAnio := Anio;
          eTipo := eTipoPeriodo( FieldByName( 'PE_TIPO' ).AsInteger );
          iNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
          if ( eTipo = tpDiario ) or ( iNumero = 0 ) then
          begin
               iAnio := 0;
               eTipo := tpDiario;
               iNumero := 0;
          end;
          eStatus := ConvierteStatusAusencia( FieldByName( 'CK_STATUS' ).AsInteger );
          rHoras := FieldByName( 'AU_HORAS' ).AsFloat;
          rValor := rMax( ( FieldByName( 'AU_EXTRAS' ).AsFloat - FieldByName( 'AU_DOBLES' ).AsFloat ), 0 );
          MigrarFechaKey( 'AU_FECHA', FieldByName( 'AU_FECHA' ).AsDateTime );
          MigrarStringKeyNull( 'AU_TIPO', FieldByName( 'AU_TIPO' ).AsString );
          MigrarStringKey( 'CB_TURNO', FieldByName( 'CB_HORARIO' ).AsString );
          MigrarStringKey( 'CB_PUESTO', FieldByName( 'CB_PUESTO' ).AsString );
          MigrarStringKey( 'CB_CLASIFI', FieldByName( 'CB_CLASIFI' ).AsString );
          MigrarStringKey( 'HO_CODIGO', FieldByName( 'HO_CODIGO' ).AsString );
     end;
     MigrarCodigoEmpleado;
     MigrarNiveles( 'CB_NIVEL', 'CB_NIVELES' );
     MigrarUsuario( 'US_CODIGO', 'AU_USER' );
     MigrarBooleanos( 'AU_HOR_MAN', 'CK_HOR_MAN' );
     MigrarIntegerKeyNull( 'PE_YEAR', iAnio );
     MigrarIntegerKeyNull( 'PE_TIPO', Ord( eTipo ) );
     MigrarIntegerKeyNull( 'PE_NUMERO', iNumero );
     with TargetQuery do
     begin
          ParamByName( 'AU_STATUS' ).AsInteger := Ord( eStatus );
          ParamByName( 'AU_TIPODIA' ).AsInteger := Ord( ConvierteTipoDia( Source.FieldByName( 'AU_TIPODIA' ).AsInteger ) );
          ParamByName( 'AU_TRIPLES' ).AsFloat := rValor;
          ParamByName( 'AU_NUM_EXT' ).AsFloat := Source.FieldByName( 'CK_NUM_EXT' ).AsFloat;
          ParamByName( 'AU_AUT_EXT' ).AsFloat := Source.FieldByName( 'CK_AUT_EXT' ).AsFloat;
          ParamByName( 'AU_AUT_TRA' ).AsFloat := Source.FieldByName( 'CK_AUT_TRA' ).AsFloat;
          ParamByName( 'AU_DES_TRA' ).AsFloat := Source.FieldByName( 'AU_DES_TRA' ).AsFloat;
          ParamByName( 'AU_DOBLES' ).AsFloat := Source.FieldByName( 'AU_DOBLES' ).AsFloat;
          ParamByName( 'AU_EXTRAS' ).AsFloat := Source.FieldByName( 'AU_EXTRAS' ).AsFloat;
          ParamByName( 'AU_HORAS' ).AsFloat := rHoras;
          ParamByName( 'AU_HORASCK' ).AsFloat := rHoras;
          ParamByName( 'AU_PER_CG' ).AsFloat := Source.FieldByName( 'AU_PER_CG' ).AsFloat;
          ParamByName( 'AU_PER_SG' ).AsFloat := Source.FieldByName( 'AU_PER_SG' ).AsFloat;
          ParamByName( 'AU_TARDES' ).AsFloat := Source.FieldByName( 'AU_TARDES' ).AsFloat;
          ParamByName( 'AU_POSICIO' ).AsInteger := Source.FieldByName( 'AU_POSICIO' ).AsInteger;
     end;
end;

procedure TdmMigrarDatos.ChecadasFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     with Dataset do
     begin
          Accept := ( FieldByName( 'CK_FECHA' ).AsDateTime >= FMinima );
     end;
end;

procedure TdmMigrarDatos.ChecadasMoveExcludedFields(Sender: TObject);
var
   Descripcion: eDescripcionChecadas;
begin
     with Source do
     begin
          MigrarFecha( 'AU_FECHA', FieldByName( 'CK_FECHA' ).AsDateTime );
          Descripcion := ConvierteDescripcion( FieldByName( 'CH_DESCRIP' ).AsString );
     end;
     MigrarCodigoEmpleado;
     MigrarBooleanos( 'CH_GLOBAL', 'CH_GLOBAL' );
     MigrarBooleanos( 'CH_IGNORAR', 'CH_IGNORAR' );
     MigrarBooleanos( 'CH_SISTEMA', 'CH_SISTEMA' );
     MigrarUsuario( 'US_CODIGO', 'CH_USER' );
     with TargetQuery do
     begin
          ParamByName( 'CH_TIPO' ).AsInteger := Ord( ConvierteChTipo( Source.FieldByName( 'CH_TIPO' ).AsString, Descripcion ) );
          ParamByName( 'CH_DESCRIP' ).AsInteger := Ord( Descripcion );
          ParamByName( 'CH_POSICIO' ).AsInteger := 0; {Pendiente}
          ParamByName( 'CH_H_AJUS' ).AsString := Source.FieldByName( 'CH_H_AJUS' ).AsString;
          ParamByName( 'CH_H_REAL' ).AsString := Source.FieldByName( 'CH_H_REAL' ).AsString;
          ParamByName( 'CH_HOR_DES' ).AsFloat := Source.FieldByName( 'CH_HOR_DES' ).AsFloat;
          ParamByName( 'CH_HOR_EXT' ).AsFloat := Source.FieldByName( 'CH_HOR_EXT' ).AsFloat;
          ParamByName( 'CH_HOR_ORD' ).AsFloat := Source.FieldByName( 'CH_HOR_ORD' ).AsFloat;
          ParamByName( 'CH_RELOJ' ).AsString := Source.FieldByName( 'CH_RELOJ' ).AsString;
     end;
end;

procedure TdmMigrarDatos.FaltasMoveExcludedFields(Sender: TObject);
var
   sValor, sMotivo, sDiaHora: String;
   rDias, rHoras: Extended;
begin
     with Source do
     begin
          sMotivo := FieldByName( 'FA_MOTIVO' ).AsString;
          sValor := FieldByName( 'NO_PERIODO' ).AsString;
          rDias := FieldByName( 'FA_DIAS' ).AsFloat;
          rHoras := 0;
          sDiaHora := K_TIPO_DIA;
          if not EsMotivoDia( sMotivo ) then
          begin
               sDiaHora := K_TIPO_HORA;
               rHoras := rDias;
               rDias := 0;
          end;
     end;
     MigrarCodigoEmpleado;
     MigrarFechaKey( 'FA_FEC_INI', Source.FieldByName( 'FA_FEC_INI' ).AsDateTime );
     with TargetQuery do
     begin
          ParamByName( 'PE_YEAR' ).AsInteger := Anio;
          ParamByName( 'PE_TIPO' ).AsInteger := Str2Integer( Copy( sValor, 1, 1 ) );
          ParamByName( 'PE_NUMERO' ).AsInteger := Str2Integer( Copy( sValor, 2, 3 ) );
          ParamByName( 'FA_DIA_HOR' ).AsString := sDiaHora;
          ParamByName( 'FA_DIAS' ).AsFloat := rDias;
          ParamByName( 'FA_HORAS' ).AsFloat := rHoras;
          ParamByName( 'FA_MOTIVO' ).AsInteger := ConvierteMotivo( sMotivo );
     end;
end;

procedure TdmMigrarDatos.IncapaciFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     Accept := not StrLleno( Dataset.FieldByName( 'IN_CLASE' ).AsString );
end;

procedure TdmMigrarDatos.IncapaciMoveExcludedFields(Sender: TObject);
begin
     MigrarCodigoEmpleado;
     with Source do
     begin
          MigrarFecha( 'IN_CAPTURA', FieldByName( 'CB_FEC_CAP' ).AsDateTime );
          MigrarFecha( 'IN_FEC_INI', FieldByName( 'IN_FEC_INI' ).AsDateTime );
          MigrarFecha( 'IN_FEC_FIN', FieldByName( 'IN_FEC_FIN' ).AsDateTime );
          MigrarStringKey( 'IN_TIPO', FieldByName( 'IN_TIPO' ).AsString );
     end;
     MigrarUsuario( 'US_CODIGO', 'CB_USER' );
     with TargetQuery do
     begin
          ParamByName( 'IN_FIN' ).AsInteger := Ord( ConvierteFinIncapacidad( Source.FieldByName( 'IN_FIN' ).AsInteger ) );
          ParamByName( 'IN_MOTIVO' ).AsInteger := Ord( ConvierteMotivoIncapacidad( Str2Integer( Source.FieldByName( 'IN_CARGO' ).AsString ) ) );
          ParamByName( 'IN_COMENTA' ).AsString := Limita( Source.FieldByName( 'IN_COMENTA' ).AsString, 30 );
          ParamByName( 'IN_DIAS' ).AsInteger := Source.FieldByName( 'IN_DIAS' ).AsInteger;
          ParamByName( 'IN_NUMERO' ).AsString := Source.FieldByName( 'IN_NUMERO' ).AsString;
          ParamByName( 'IN_TASA_IP' ).AsFloat := Source.FieldByName( 'IN_TASA_IP' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.KardexFilterRecord(DataSet: TDataSet; var Accept: Boolean);
const
     K_INCAPACIDAD = 'INCAPA';
     K_PERMISO_CON_GOCE = 'PER_CG';
     K_PERMISO_SIN_GOCE = 'PER_SG';
     K_VACACION = 'VAC';
var
   sTipo: String;
begin
     inherited;
     with Dataset do
     begin
          sTipo := Trim( FieldByName( 'CB_TIPO' ).AsString );
          Accept := ( sTipo <> K_INCAPACIDAD ) and
                    ( sTipo <> K_PERMISO_CON_GOCE ) and
                    ( sTipo <> K_PERMISO_SIN_GOCE ) and
                    ( sTipo <> K_VACACION ) and
                    PuedeMigrarKardex( FieldByName( 'CB_FECHA' ).AsDateTime, LeeCodigoEmpleado );
     end;
end;

procedure TdmMigrarDatos.KardexMoveExcludedFields(Sender: TObject);
const
     K_CARGO_NULO = '';
     K_CARGO_ALTA = 'A';
     K_CARGO_REINGRESO = 'R';
var
   sTipo, sCargo: String;
   dFecha, dFecha2, dFecAnt, dFecIng: TDate;
   iYear, iNomina: Integer;
   eTipo: eTipoPeriodo;
   iEmpleado: TNumEmp;

procedure BuscaFechas( const iEmpleado: TNumEmp; var dFecAnt, dFecIng: TDate );
begin
     with tqUno do
     begin
          if not Active or ( Params[ 0 ].AsInteger <> iEmpleado ) then
          begin
               Active := False;
               Params[ 0 ].AsInteger := iEmpleado;
               Active := True;
          end;
          if not Active or IsEmpty then
          begin
               dFecAnt := NullDateTime;
               dFecIng := NullDateTime;
          end
          else
          begin
               dFecAnt := Fields[ 0 ].AsDateTime;
               dFecIng := Fields[ 1 ].AsDateTime;
          end;
     end;
end;

function ConvierteTipoKardex( const sTipo: String ): String;
const
     K_PRESTAX = 'PRESTX';
     K_T_PRESTA_NOMBRE = 'Cambio Tabla de Prestacio';
     K_INSERT = 'insert into TKARDEX( TB_CODIGO, TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, TB_NIVEL, TB_SISTEMA ) '+
                'select "%s", TB_ELEMENT, TB_INGLES, TB_NUMERO, TB_TEXTO, %d, "%s" from TKARDEX where ( TB_CODIGO = "%s" )';
     K_UPDATE = 'update TKARDEX set TB_ELEMENT = "%s" where ( TB_CODIGO = "%s" )';
begin
     Result := Trim( sTipo );
     if ( Result = K_T_PRESTA ) then
     begin
          if not FPrestaxCreated then
          begin
               with TablaMigrada do
               begin
                    PrepareTargetQuery( tqDos,
                                        Format( K_INSERT,
                                                [ K_PRESTAX,
                                                  ConvierteNivelKardex( K_PRESTAX ),
                                                  ConvierteSistema( K_PRESTAX ),
                                                  K_T_PRESTA ] ) );
                    with tqDos do
                    begin
                         ExecSQL;
                    end;
                    CloseQuery( tqDos );
                    PrepareTargetQuery( tqDos, Format( K_UPDATE, [ K_T_PRESTA_NOMBRE, K_T_PRESTA ] ) );
                    with tqDos do
                    begin
                         ExecSQL;
                    end;
                    CloseQuery( tqDos );
               end;
               FPrestaxCreated := True;
          end;
          Result := K_PRESTAX;
     end;
end;

begin
     with Source do
     begin
          sTipo := ConvierteTipoKardex( FieldByName( 'CB_TIPO' ).AsString );
          sCargo := Trim( FieldByName( 'CB_CARGO' ).AsString );
          dFecha := FieldByName( 'CB_FECHA' ).AsDateTime;
          dFecha2 := FieldByName( 'CB_FECHA_2' ).AsDateTime;
          if ( dFecha2 = NullDateTime ) then
             dFecha2 := dFecha;
          if ( sTipo = K_T_BAJA ) then
          begin
               iYear := TheYear( dFecha );
               eTipo := tpSemanal; // ( 1 ) // � Debe Ser Tipo de N�mina Default de COMPANY.REC ! //
               iNomina := Str2Integer( sCargo );
          end
          else
          begin
               iYear := 0;
               eTipo := tpDiario;
               iNomina := 0;
          end;
          BuscaFechas( FieldByName( 'CB_CODIGO' ).AsInteger, dFecAnt, dFecIng );
          MigrarFecha( 'CB_FEC_CAP', FieldByName( 'CB_FEC_CAP' ).AsDateTime );
          MigrarFecha( 'CB_FEC_CON', FieldByName( 'CB_FEC_CON' ).AsDateTime );
          MigrarFecha( 'CB_FEC_INT', FieldByName( 'CB_FEC_INT' ).AsDateTime );
          MigrarFecha( 'CB_FEC_REV', FieldByName( 'CB_FEC_REV' ).AsDateTime );
          MigrarStringKeyNull( 'CB_MOT_BAJ', FieldByName( 'CB_MOT_BAJ' ).AsString );
          MigrarStringKey( 'CB_TIPO', sTipo );
          MigrarStringKey( 'CB_TURNO', FieldByName( 'CB_HORARIO' ).AsString );
          MigrarStringKey( 'CB_CLASIFI', FieldByName( 'CB_CLASIFI' ).AsString );
          MigrarStringKey( 'CB_PUESTO', FieldByName( 'CB_PUESTO' ).AsString );
          MigrarStringKey( 'CB_TABLASS', FieldByName( 'CB_TABLASS' ).AsString );
          MigrarStringKey( 'CB_PATRON', FieldByName( 'CB_PATRON' ).AsString );
          MigrarStringKey( 'CB_CONTRAT', FieldByName( 'CB_CONTRAT' ).AsString );
     end;
     iEmpleado := LeeCodigoEmpleado;
     MigrarEmpleado( iEmpleado );
     MigrarFechaKardex( 'CB_FECHA', dFecha );
     MigrarFecha( 'CB_FECHA_2', dFecha2 );
     MigrarFecha( 'CB_FEC_ANT', dFecAnt );
     MigrarFecha( 'CB_FEC_ING', dFecIng );
     MigrarNiveles( 'CB_NIVEL', 'CB_NIVELES' );
     MigrarBooleanos( 'CB_AUTOSAL', 'CB_AUTOSAL' );
     MigrarBooleanos( 'CB_GLOBAL', 'CB_GLOBAL' );
     MigrarUsuario( 'US_CODIGO', 'CB_USER' );
     MigrarMemo( 'CB_NOTA', K_STRING_NULO );
     with TargetQuery do
     begin
          ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( sTipo );
          ParamByName( 'CB_STATUS' ).AsInteger := Ord( ConvierteStatus( Source.FieldByName( 'CB_STATUS' ).AsString, sTipo, sCargo ) );
          ParamByName( 'CB_NOMYEAR' ).AsInteger := iYear;
          ParamByName( 'CB_NOMTIPO' ).AsInteger := Ord( eTipo );
          ParamByName( 'CB_NOMNUME' ).AsInteger := iNomina;
          ParamByName( 'CB_REINGRE' ).AsString := zBoolToStr( ( sTipo = K_T_ALTA ) and ( sCargo = K_CARGO_REINGRESO ) );
          ParamByName( 'CB_COMENTA' ).AsString := Source.FieldByName( 'CB_COMENTA' ).AsString;
          ParamByName( 'CB_FAC_INT' ).AsFloat := Source.FieldByName( 'CB_FAC_INT' ).AsFloat;
          ParamByName( 'CB_MONTO' ).AsFloat := Source.FieldByName( 'CB_MONTO' ).AsFloat;
          ParamByName( 'CB_OLD_INT' ).AsFloat := Source.FieldByName( 'CB_OLD_INT' ).AsFloat;
          ParamByName( 'CB_OLD_SAL' ).AsFloat := Source.FieldByName( 'CB_OLD_SAL' ).AsFloat;
          ParamByName( 'CB_OTRAS_P' ).AsFloat := Source.FieldByName( 'CB_OTRAS_P' ).AsFloat;
          ParamByName( 'CB_PER_VAR' ).AsFloat := Source.FieldByName( 'CB_PER_VAR' ).AsFloat;
          ParamByName( 'CB_PRE_INT' ).AsFloat := Source.FieldByName( 'CB_PRE_INT' ).AsFloat;
          ParamByName( 'CB_RANGO_S' ).AsFloat := Source.FieldByName( 'CB_RANGO_S' ).AsFloat;
          ParamByName( 'CB_SAL_INT' ).AsFloat := Source.FieldByName( 'CB_SAL_INT' ).AsFloat;
          ParamByName( 'CB_SAL_TOT' ).AsFloat := Source.FieldByName( 'CB_SAL_TOT' ).AsFloat;
          ParamByName( 'CB_SALARIO' ).AsFloat := Source.FieldByName( 'CB_SALARIO' ).AsFloat;
          ParamByName( 'CB_TOT_GRA' ).AsFloat := Source.FieldByName( 'CB_TOT_GRA' ).AsFloat;
          ParamByName( 'CB_ZONA_GE' ).AsString := Source.FieldByName( 'CB_ZONA_GE' ).AsString;
     end;
end;

procedure TdmMigrarDatos.KardexOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select CB_FEC_ANT, CB_FEC_ING from COLABORA where ( CB_CODIGO = :CB_CODIGO )' );
     end;
end;

procedure TdmMigrarDatos.KardexCloseDatasets(Sender: TObject);
const
     K_CAMPOS = 'CB_CODIGO, CB_ACTIVO, CB_AUTOSAL, CB_CLASIFI, CB_CONTRAT, '+
                'CB_FEC_ANT, CB_FEC_CON, CB_FEC_ING, CB_FEC_INT, '+
                'CB_FEC_REV, CB_FEC_BAJ, CB_FEC_BSS, CB_MOT_BAJ, '+
                'CB_NIVEL1, CB_NIVEL2, CB_NIVEL3, CB_NIVEL4, CB_NIVEL5, CB_NIVEL6, CB_NIVEL7, CB_NIVEL8, CB_NIVEL9, '+
                {$ifdef ACS}'CB_NIVEL10, CB_NIVEL11, CB_NIVEL12, '+{$endif}                
                'CB_NOMNUME, CB_NOMTIPO, CB_NOMYEAR, CB_PATRON, '+
                'CB_PER_VAR, CB_PRE_INT, CB_PUESTO, CB_SAL_INT, '+
                'CB_SAL_TOT, CB_SALARIO, CB_TABLASS, CB_TOT_GRA, CB_TURNO, '+
                'CB_ZONA_GE, CB_OLD_SAL, CB_OLD_INT, CB_FAC_INT';
var
   dAlta, dBaja: TDate;
   lActivo: Boolean;

function DeterminaAltaBaja: Boolean;
begin
     with tqUno do
     begin
          dAlta := FieldByName( 'CB_FEC_ING' ).AsDateTime;
          dBaja := FieldByName( 'CB_FEC_BAJ' ).AsDateTime;
          lActivo := ZetaCommonTools.zStrToBool( FieldByName( 'CB_ACTIVO' ).AsString );
     end;
     if not lActivo then
     begin
          if ( dBaja = NullDateTime ) then
             dBaja := dAlta;
     end;
     Result := ( dAlta <> NullDateTime );
end;

procedure AgregaBaja( const iEmpleado: TNumEmp );
begin
     with tqTres do
     begin
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          ParamByName( 'CB_TIPO' ).AsString := K_T_BAJA;
          ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( K_T_BAJA );
          ParamByName( 'CB_FECHA' ).AsDate := dBaja;
          ParamByName( 'CB_FECHA_2' ).AsDate := tqUno.FieldByName( 'CB_FEC_BSS' ).AsDateTime;
          ParamByName( 'CB_NOMYEAR' ).AsInteger := tqUno.FieldByName( 'CB_NOMYEAR' ).AsInteger;
          ParamByName( 'CB_NOMTIPO' ).AsInteger := tqUno.FieldByName( 'CB_NOMTIPO' ).AsInteger;
          ParamByName( 'CB_NOMNUME' ).AsInteger := tqUno.FieldByName( 'CB_NOMNUME' ).AsInteger;
          ParamByName( 'CB_MOT_BAJ' ).AsString := tqUno.FieldByName( 'CB_MOT_BAJ' ).AsString;
          ParamByName( 'CB_COMENTA' ).AsString := K_MARCA;
          ExecSQL;
     end;
     with Bitacora do
     begin
          AgregaError( 'Kardex: BAJA Agregada Al Empleado ' + IntToStr( iEmpleado ) );
     end;
end;

procedure AgregaAlta( const iEmpleado: Integer );
begin
     with tqDos do
     begin
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          ParamByName( 'CB_TIPO' ).AsString := K_T_ALTA;
          ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( K_T_ALTA );
          ParamByName( 'CB_FECHA' ).AsDate := dAlta;
          ParamByName( 'CB_AUTOSAL' ).AsString := tqUno.FieldByName( 'CB_AUTOSAL' ).AsString;
          ParamByName( 'CB_CLASIFI' ).AsString := tqUno.FieldByName( 'CB_CLASIFI' ).AsString;
          ParamByName( 'CB_CONTRAT' ).AsString := tqUno.FieldByName( 'CB_CONTRAT' ).AsString;
          ParamByName( 'CB_FEC_ANT' ).AsDate := tqUno.FieldByName( 'CB_FEC_ANT' ).AsDateTime;
          ParamByName( 'CB_FEC_CON' ).AsDate := tqUno.FieldByName( 'CB_FEC_CON' ).AsDateTime;
          ParamByName( 'CB_FEC_ING' ).AsDate := dAlta;
          ParamByName( 'CB_FEC_INT' ).AsDate := tqUno.FieldByName( 'CB_FEC_INT' ).AsDateTime;
          ParamByName( 'CB_FEC_REV' ).AsDate := tqUno.FieldByName( 'CB_FEC_REV' ).AsDateTime;
          ParamByName( 'CB_NIVEL1' ).AsString := tqUno.FieldByName( 'CB_NIVEL1' ).AsString;
          ParamByName( 'CB_NIVEL2' ).AsString := tqUno.FieldByName( 'CB_NIVEL2' ).AsString;
          ParamByName( 'CB_NIVEL3' ).AsString := tqUno.FieldByName( 'CB_NIVEL3' ).AsString;
          ParamByName( 'CB_NIVEL4' ).AsString := tqUno.FieldByName( 'CB_NIVEL4' ).AsString;
          ParamByName( 'CB_NIVEL5' ).AsString := tqUno.FieldByName( 'CB_NIVEL5' ).AsString;
          ParamByName( 'CB_NIVEL6' ).AsString := tqUno.FieldByName( 'CB_NIVEL6' ).AsString;
          ParamByName( 'CB_NIVEL7' ).AsString := tqUno.FieldByName( 'CB_NIVEL7' ).AsString;
          ParamByName( 'CB_NIVEL8' ).AsString := tqUno.FieldByName( 'CB_NIVEL8' ).AsString;
          ParamByName( 'CB_NIVEL9' ).AsString := tqUno.FieldByName( 'CB_NIVEL9' ).AsString;
          {$ifdef ACS}
          ParamByName( 'CB_NIVEL10' ).AsString := tqUno.FieldByName( 'CB_NIVEL10' ).AsString;
          ParamByName( 'CB_NIVEL11' ).AsString := tqUno.FieldByName( 'CB_NIVEL11' ).AsString;
          ParamByName( 'CB_NIVEL12' ).AsString := tqUno.FieldByName( 'CB_NIVEL12' ).AsString;
          {$endif}
          ParamByName( 'CB_PATRON' ).AsString := tqUno.FieldByName( 'CB_PATRON' ).AsString;
          ParamByName( 'CB_PER_VAR' ).AsFloat := tqUno.FieldByName( 'CB_PER_VAR' ).AsFloat;
          ParamByName( 'CB_PRE_INT' ).AsFloat := tqUno.FieldByName( 'CB_PRE_INT' ).AsFloat;
          ParamByName( 'CB_PUESTO' ).AsString := tqUno.FieldByName( 'CB_PUESTO' ).AsString;
          ParamByName( 'CB_SAL_INT' ).AsFloat := tqUno.FieldByName( 'CB_SAL_INT' ).AsFloat;
          ParamByName( 'CB_SAL_TOT' ).AsFloat := tqUno.FieldByName( 'CB_SAL_TOT' ).AsFloat;
          ParamByName( 'CB_SALARIO' ).AsFloat := tqUno.FieldByName( 'CB_SALARIO' ).AsFloat;
          ParamByName( 'CB_TABLASS' ).AsString := tqUno.FieldByName( 'CB_TABLASS' ).AsString;
          ParamByName( 'CB_TOT_GRA' ).AsFloat := tqUno.FieldByName( 'CB_TOT_GRA' ).AsFloat;
          ParamByName( 'CB_TURNO' ).AsString := tqUno.FieldByName( 'CB_TURNO' ).AsString;
          ParamByName( 'CB_ZONA_GE' ).AsString := tqUno.FieldByName( 'CB_ZONA_GE' ).AsString;
          ParamByName( 'CB_OLD_SAL' ).AsFloat := tqUno.FieldByName( 'CB_OLD_SAL' ).AsFloat;
          ParamByName( 'CB_OLD_INT' ).AsFloat := tqUno.FieldByName( 'CB_OLD_INT' ).AsFloat;
          ParamByName( 'CB_FAC_INT' ).AsFloat := tqUno.FieldByName( 'CB_FAC_INT' ).AsFloat;
          ParamByName( 'CB_COMENTA' ).AsString := K_MARCA;
          ExecSQL;
     end;
     with Bitacora do
     begin
          AgregaError( 'Kardex: ALTA Agregada Al Empleado ' + IntToStr( iEmpleado ) );
     end;
end;

procedure CreaAltaBaja( const iEmpleado: Integer );
begin
     if DeterminaAltaBaja then
     begin
          EmpezarTransaccion;
          try
             AgregaAlta( iEmpleado );
             if not lActivo then
                AgregaBaja( iEmpleado );
             TerminarTransaccion( True );
          except
                on Error: Exception do
                begin
                     TerminarTransaccion( False );
                     with TablaMigrada do
                     begin
                          HandleException( 'Error Al Crear Alta/Baja de Kardex Empleado ' + IntToStr( iEmpleado ), Error );
                     end;
                end;
          end;
     end
     else
     begin
          with TablaMigrada do
          begin
               AgregaError( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin Kardex: Fecha De Ingreso Vac�a' );
          end;
     end;
end;

procedure VerificaCambioTablaPrestaciones( const iEmpleado: Integer; const sTablaSS: String );
var
   dMaxima: TDate;
begin
     with tqCinco do
     begin
          Active := False;
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          Active := True;
          dMaxima := Fields[ 0 ].AsDateTime;
          Active := False;
     end;
     if ( dMaxima > NullDateTime ) then
     begin
          with tqSeis do
          begin
               ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
               ParamByName( 'CB_TIPO' ).AsString := K_T_PRESTA;
               ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( K_T_PRESTA );
               ParamByName( 'CB_FECHA' ).AsDate := dMaxima;
               ParamByName( 'CB_TABLASS' ).AsString := sTablaSS;
               ParamByName( 'CB_COMENTA' ).AsString := K_MARCA;
               ExecSQL;
          end;
     end;
end;

procedure VerificaCambioTurno( const iEmpleado: TNumEmp );
var
   lAlta, lCambio: Boolean;
   sOriginal, sTurno, sTipo: TCodigo;
   dMaxima: TDate;
begin
     with tqCinco do
     begin
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          Active := True;
          lCambio := False;
          lAlta := False;
          dMaxima := NullDateTime;
          while not Eof do
          begin
               sTipo := FieldByName( 'CB_TIPO' ).AsString;
               if ( sTipo = K_T_ALTA ) then
               begin
                    sOriginal := FieldByName( 'CB_TURNO' ).AsString;
                    lAlta := True;
                    lCambio := False;
               end
               else
                   if lAlta then
                   begin
                        if ( sTipo = K_T_AREA ) then
                        begin
                             if ( FieldByName( 'CB_TURNO' ).AsString <> sOriginal ) then
                             begin
                                  sTurno := FieldByName( 'CB_TURNO' ).AsString;
                                  dMaxima := FieldByName( 'CB_FECHA' ).AsDateTime;
                                  lCambio := True;
                             end;
                        end
                        else
                            if ( sTipo = K_T_TURNO ) then
                            begin
                                 lCambio := False;
                            end;
                   end;
               Next;
          end;
          Active := False;
     end;
     if lCambio then
     begin
          with tqSeis do
          begin
               ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
               ParamByName( 'CB_TIPO' ).AsString := K_T_TURNO;
               ParamByName( 'CB_NIVEL' ).AsInteger := ConvierteNivelKardex( K_T_TURNO );
               ParamByName( 'CB_FECHA' ).AsDate := dMaxima;
               ParamByName( 'CB_TURNO' ).AsString := sTurno;
               ParamByName( 'CB_COMENTA' ).AsString := K_MARCA;
               ExecSQL;
          end;
     end;
end;

procedure AgregaAltaBaja( const iEmpleado: Integer );
var
   dKardexAlta, dKardexBaja: TDate;
   lUltimoEsBaja: Boolean;
begin
     with tqUno do
     begin
          Active := False;
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          Active := True;
          if DeterminaAltaBaja then
          begin
               dKardexAlta := NullDateTime;
               dKardexBaja := NullDateTime;
               lUltimoEsBaja := False;
               with tqSeis do
               begin
                    Active := False;
                    ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                    Active := True;
                    if not IsEmpty then
                    begin
                         dKardexAlta := FieldByName( 'CB_FECHA' ).AsDateTime;
                         while not Eof do
                         begin
                              lUltimoEsBaja := ( FieldByName( 'CB_TIPO' ).AsString = K_T_BAJA );
                              dKardexBaja := FieldByName( 'CB_FECHA' ).AsDateTime;
                              Next;
                         end;
                    end;
                    Active := False;
               end;
               if ( dAlta > dKardexAlta ) then
               begin
                    with TablaMigrada do
                    begin
                         AgregaError( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin ALTA Inicial: Fecha De Ingreso Posterior Al Primer Movimiento en KARDEX' )
                    end;
               end
               else
                   if not lActivo and not lUltimoEsBaja and ( dBaja < dKardexBaja ) then
                   begin
                        with TablaMigrada do
                        begin
                             AgregaError( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin BAJA Final: Fecha De Baja Anterior Al Ultimo Movimiento en KARDEX' )
                        end;
                   end
                   else
                   begin
                        EmpezarTransaccion;
                        try
                           AgregaAlta( iEmpleado );
                           if not lActivo and not lUltimoEsBaja then
                              AgregaBaja( iEmpleado );
                           with tqCuatro do
                           begin
                                ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                ExecSQL;
                           end;
                           TerminarTransaccion( True );
                        except
                              on Error: Exception do
                              begin
                                   TerminarTransaccion( False );
                                   with TablaMigrada do
                                   begin
                                        HandleException( 'Error Al Crear Alta/Baja de Kardex Empleado ' + IntToStr( iEmpleado ), Error );
                                   end;
                              end;
                        end;
                   end;
          end
          else
          begin
               with TablaMigrada do
               begin
                    AgregaError( 'Empleado ' + IntToStr( iEmpleado ) + ' Sin ALTA Inicial: Fecha De Ingreso Vac�a' );
               end;
          end;
          Active := False;
     end;
end;

begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
          if FRecalcularKardex then
          begin
               PrepareTargetQuery( tqUno, 'select ' + K_CAMPOS + ' from COLABORA where ( ( select COUNT(*) from KARDEX K where ( K.CB_CODIGO = COLABORA.CB_CODIGO ) ) = 0 )' );
               PrepareTargetQuery( tqDos, 'insert into KARDEX( CB_CODIGO, '+
                                                              'CB_TIPO, '+
                                                              'CB_NIVEL, '+
                                                              'CB_FECHA, '+
                                                              'CB_AUTOSAL, '+
                                                              'CB_CLASIFI, '+
                                                              'CB_CONTRAT, '+
                                                              'CB_FEC_ANT, '+
                                                              'CB_FEC_CON, '+
                                                              'CB_FEC_ING, '+
                                                              'CB_FEC_INT, '+
                                                              'CB_FEC_REV, '+
                                                              'CB_NIVEL1, '+
                                                              'CB_NIVEL2, '+
                                                              'CB_NIVEL3, '+
                                                              'CB_NIVEL4, '+
                                                              'CB_NIVEL5, '+
                                                              'CB_NIVEL6, '+
                                                              'CB_NIVEL7, '+
                                                              'CB_NIVEL8, '+
                                                              'CB_NIVEL9, '+
                                                              {$ifdef ACS}
                                                              'CB_NIVEL10, '+
                                                              'CB_NIVEL11, '+
                                                              'CB_NIVEL12, '+
                                                              {$endif}
                                                              'CB_PATRON, '+
                                                              'CB_PER_VAR, '+
                                                              'CB_PRE_INT, '+
                                                              'CB_PUESTO, '+
                                                              'CB_SAL_INT, '+
                                                              'CB_SAL_TOT, '+
                                                              'CB_SALARIO, '+
                                                              'CB_TABLASS, '+
                                                              'CB_TOT_GRA, '+
                                                              'CB_TURNO, '+
                                                              'CB_ZONA_GE, '+
                                                              'CB_OLD_SAL, '+
                                                              'CB_OLD_INT, '+
                                                              'CB_FAC_INT, '+
                                                              'CB_COMENTA ) values ('+
                                                              ':CB_CODIGO, '+
                                                              ':CB_TIPO, '+
                                                              ':CB_NIVEL, '+
                                                              ':CB_FECHA, '+
                                                              ':CB_AUTOSAL, '+
                                                              ':CB_CLASIFI, '+
                                                              ':CB_CONTRAT, '+
                                                              ':CB_FEC_ANT, '+
                                                              ':CB_FEC_CON, '+
                                                              ':CB_FEC_ING, '+
                                                              ':CB_FEC_INT, '+
                                                              ':CB_FEC_REV, '+
                                                              ':CB_NIVEL1, '+
                                                              ':CB_NIVEL2, '+
                                                              ':CB_NIVEL3, '+
                                                              ':CB_NIVEL4, '+
                                                              ':CB_NIVEL5, '+
                                                              ':CB_NIVEL6, '+
                                                              ':CB_NIVEL7, '+
                                                              ':CB_NIVEL8, '+
                                                              ':CB_NIVEL9, '+
                                                              {$ifdef ACS}
                                                              ':CB_NIVEL10, '+
                                                              ':CB_NIVEL11, '+
                                                              ':CB_NIVEL12, '+
                                                              {$endif}
                                                              ':CB_PATRON, '+
                                                              ':CB_PER_VAR, '+
                                                              ':CB_PRE_INT, '+
                                                              ':CB_PUESTO, '+
                                                              ':CB_SAL_INT, '+
                                                              ':CB_SAL_TOT, '+
                                                              ':CB_SALARIO, '+
                                                              ':CB_TABLASS, '+
                                                              ':CB_TOT_GRA, '+
                                                              ':CB_TURNO, '+
                                                              ':CB_ZONA_GE, '+
                                                              ':CB_OLD_SAL, '+
                                                              ':CB_OLD_INT, '+
                                                              ':CB_FAC_INT, '+
                                                              ':CB_COMENTA )' );
               PrepareTargetQuery( tqTres, 'insert into KARDEX( CB_CODIGO, '+
                                                               'CB_TIPO, '+
                                                               'CB_NIVEL, '+
                                                               'CB_FECHA, '+
                                                               'CB_FECHA_2, '+
                                                               'CB_MOT_BAJ, '+
                                                               'CB_NOMYEAR, '+
                                                               'CB_NOMTIPO, '+
                                                               'CB_NOMNUME, '+
                                                               'CB_COMENTA ) values ( '+
                                                               ':CB_CODIGO, '+
                                                               ':CB_TIPO, '+
                                                               ':CB_NIVEL, '+
                                                               ':CB_FECHA, '+
                                                               ':CB_FECHA_2, '+
                                                               ':CB_MOT_BAJ, '+
                                                               ':CB_NOMYEAR, '+
                                                               ':CB_NOMTIPO, '+
                                                               ':CB_NOMNUME, '+
                                                               ':CB_COMENTA )' );
               PrepareTargetQuery( tqCuatro, 'execute procedure RECALCULA_KARDEX( :CB_CODIGO )' );
               { Agregar movimiento de Alta a aquellos empleados SIN registros en la tabla KARDEX }
               try
                  with tqUno do
                  begin
                       Active := True;
                       while not Eof do
                       begin
                            CreaAltaBaja( FieldByName( 'CB_CODIGO' ).AsInteger );
                            Next;
                       end;
                       Active := False;
                  end;
               except
                     on Error: Exception do
                     begin
                          with TablaMigrada do
                          begin
                               HandleException( 'Error Al Examinar Kardex', Error );
                          end;
                     end;
               end;
               CloseQuery( tqUno );
               { Hay que buscar empleados que tienen cambios en COLABORA.CB_TABLASS }
               { para insertar el movimiento de CB_TIPO = K_T_PRESTA ya que en DOS }
               { esto se hac�a con un CB_TIPO = K_T_CAMBIO }
               PrepareTargetQuery( tqUno, 'select C.CB_CODIGO, C.CB_TABLASS from COLABORA C where ( ( select COUNT(*) from KARDEX K where ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CB_TABLASS > "" ) and ( K.CB_TABLASS <> C.CB_TABLASS ) ) > 0 )' );
               PrepareTargetQuery( tqCinco, Format( 'select MAX( CB_FECHA ) from KARDEX where ( CB_CODIGO = :CB_CODIGO ) and ( CB_TIPO in ( "%s", "%s", "%s" ) )', [ K_T_ALTA, K_T_AREA, K_T_CIERRA ] ) );
               PrepareTargetQuery( tqSeis, 'insert into KARDEX( CB_CODIGO, '+
                                                                'CB_TIPO, '+
                                                                'CB_NIVEL, '+
                                                                'CB_FECHA, '+
                                                                'CB_TABLASS, '+
                                                                'CB_COMENTA ) values ( '+
                                                                ':CB_CODIGO, '+
                                                                ':CB_TIPO, '+
                                                                ':CB_NIVEL, '+
                                                                ':CB_FECHA, '+
                                                                ':CB_TABLASS, '+
                                                                ':CB_COMENTA )' );
               try
                  with tqUno do
                  begin
                       Active := True;
                       while not Eof do
                       begin
                            VerificaCambioTablaPrestaciones( FieldByName( 'CB_CODIGO' ).AsInteger, FieldByName( 'CB_TABLASS' ).AsString );
                            Next;
                       end;
                       Active := False;
                  end;
               except
                     on Error: Exception do
                     begin
                          with TablaMigrada do
                          begin
                               HandleException( 'Error Al Ajustar Tabla De Prestaciones', Error );
                          end;
                     end;
               end;
               CloseQuery( tqSeis );
               CloseQuery( tqCinco );
               CloseQuery( tqUno );
               { En versiones de DOS anteriores a 1998, no exist�a un movimiento de }
               { kardex tipo 'TURNO', sino que el cambio de turno iba junto }
               { con el tipo 'AREA'. }
               { Desde el momento en que aparece el tipo 'TURNO' se empez� a grabar una 'A' }
               { en el campo CB_CARGO de los movimientos tipo 'AREA' para identificar que }
               { esos movimientos SOLO AFECTAN AREA y no Turno. }
               { En la migraci�n, lo que hay que hacer es crear un movimiento tipo TURNO. }
               { Para no llenar el kardex de movimientos viejos, sugiero que se haga SI Y SOLO SI: }
               { 1) Es el ULTIMO de los movimientos de AREA con CB_CARGO <> "A" }
               {   ( Si hubo varios cambios de turno, s�lo se arregla el �ltimo ) }
               { 2) No hay un registro de TURNO despu�s de este. }
               {   ( No tiene caso agregar un cambio de turno si despu�s de este aparece otro ) }
               {   ( En estos casos no se presenta el problema en Windows ) }
               { 3) S�lo si hubo cambios de turno respecto al movimiento de ALTA }
               {   ( Si el empleado nunca cambi� de turno, no se agrega movimiento ) }
               PrepareTargetQuery( tqUno, 'select C.CB_CODIGO from COLABORA C where ( ( select COUNT(*) from KARDEX K where ( K.CB_CODIGO = C.CB_CODIGO ) and ( K.CB_TIPO = "' + K_T_AREA + '" ) and ( K.CB_STATUS <> 0 ) ) > 0 )' );
               PrepareTargetQuery( tqCinco, Format( 'select CB_FECHA, CB_TIPO, CB_TURNO, CB_COMENTA from KARDEX where ( CB_CODIGO = :CB_CODIGO ) and ( ( CB_TIPO in ( "%s", "%s" ) ) or ( ( CB_TIPO = "%s" ) and ( CB_STATUS <> 0 ) ) ) order by CB_FECHA, CB_NIVEL', [ K_T_ALTA, K_T_TURNO, K_T_AREA ] ) );
               PrepareTargetQuery( tqSeis, 'insert into KARDEX( CB_CODIGO, '+
                                                                'CB_TIPO, '+
                                                                'CB_NIVEL, '+
                                                                'CB_FECHA, '+
                                                                'CB_TURNO, '+
                                                                'CB_COMENTA ) values ( '+
                                                                ':CB_CODIGO, '+
                                                                ':CB_TIPO, '+
                                                                ':CB_NIVEL, '+
                                                                ':CB_FECHA, '+
                                                                ':CB_TURNO, '+
                                                                ':CB_COMENTA )' );
               try
                  with tqUno do
                  begin
                       Active := True;
                       while not Eof do
                       begin
                            VerificaCambioTurno( FieldByName( 'CB_CODIGO' ).AsInteger );
                            Next;
                       end;
                       Active := False;
                  end;
               except
                     on Error: Exception do
                     begin
                          with TablaMigrada do
                          begin
                               HandleException( 'Error Al Escribir Cambio de Turno', Error );
                          end;
                     end;
               end;
               CloseQuery( tqSeis );
               CloseQuery( tqCinco );
               CloseQuery( tqUno );
               { Al recalcular el Kardex, el StoredProcedure reporta los empleados }
               { quienes, teniendo registros en la tabla KARDEX, el primer movimiento }
               { NO es KARDEX.CB_TIPO = 'ALTA' }
               PrepareTargetQuery( tqUno, 'select ' + K_CAMPOS + ' from COLABORA where ( CB_CODIGO = :CB_CODIGO )' );
               PrepareTargetQuery( tqCinco, 'select CB_CODIGO, PROBLEMA from RECALCULA_TODOS' );
               PrepareTargetQuery( tqSeis, 'select CB_FECHA, CB_TIPO from KARDEX where ( CB_CODIGO = :CB_CODIGO ) order by CB_FECHA, CB_NIVEL' );
               try
                  with tqCinco do
                  begin
                       Active := True;
                       while not Eof do
                       begin
                            AgregaAltaBaja( FieldByName( 'CB_CODIGO' ).AsInteger );
                            Next;
                       end;
                       Active := False;
                  end;
               except
                     on Error: Exception do
                     begin
                          with TablaMigrada do
                          begin
                               HandleException( 'Error Al Recalcular Kardex', Error );
                          end;
                     end;
               end;
               CloseQuery( tqSeis );
               CloseQuery( tqCinco );
               CloseQuery( tqCuatro );
               CloseQuery( tqTres );
               CloseQuery( tqDos );
               CloseQuery( tqUno );
          end;
     end;
end;

procedure TdmMigrarDatos.Liq_EmpMoveExcludedFields(Sender: TObject);
var
   Year, Month, Day: Word;
   rTotIMS, rTotRet, rTotInf, rImssOB, rImssPA: Extended;
begin
     with Source do
     begin
          DecodeDate( FieldByName( 'LS_FECHA' ).AsDateTime, Year, Month, Day );
          rTotIMS := FieldByName( 'LE_EYM_FIJ' ).AsFloat +
                     FieldByName( 'LE_EYM_EXC' ).AsFloat +
                     FieldByName( 'LE_EYM_DIN' ).AsFloat +
                     FieldByName( 'LE_EYM_ESP' ).AsFloat +
                     FieldByName( 'LE_INV_VID' ).AsFloat +
                     FieldByName( 'LE_RIESGOS' ).AsFloat +
                     FieldByName( 'LE_GUARDER' ).AsFloat +
                     // SOP 4527
                     // Aportaci�n voluntaria se calcula mensualmente
                     FieldByName( 'LE_APO_VOL' ).AsFloat;
                     // ----- -----
          rTotRet := FieldByName( 'LE_CES_VEJ' ).AsFloat +
                     FieldByName( 'LE_RETIRO' ).AsFloat;
                     // SOP 54527
                     // Aportaci�n voluntaria se suma al TotIMS por tratarse de un valor mensual
                     // y no bimestral
                     // FieldByName( 'LE_APO_VOL' ).AsFloat;
                     // ----- -----
          rTotInf := FieldByName( 'LE_INF_PAT' ).AsFloat +
                     FieldByName( 'LE_INF_AMO' ).AsFloat;
          if EsVer260 then
          begin
               rImssOB := FieldByName( 'LE_IMSS_OB' ).AsFloat;
               rImssPA := FieldByName( 'LE_IMSS_PA' ).AsFloat;
          end
          else
          begin
               rImssOB := 0;
               rImssPA := 0;
          end;
     end;
     MigrarCodigoEmpleado;
     with TargetQuery do
     begin
          ParamByName( 'LS_YEAR' ).AsInteger := Year;
          ParamByName( 'LS_MONTH' ).AsInteger := Month;
          ParamByName( 'LS_TIPO' ).AsInteger := Ord( ConvierteTipoLiquidacion( Day ) );
          ParamByName( 'LE_STATUS' ).AsInteger := 0;
          ParamByName( 'LE_TOT_IMS' ).AsFloat := rTotIMS;
          ParamByName( 'LE_TOT_RET' ).AsFloat := rTotRet;
          ParamByName( 'LE_TOT_INF' ).AsFloat := rTotInf;
          ParamByName( 'LE_APO_VOL' ).AsFloat := Source.FieldByName( 'LE_APO_VOL' ).AsFloat;
          ParamByName( 'LE_CES_VEJ' ).AsFloat := Source.FieldByName( 'LE_CES_VEJ' ).AsFloat;
          ParamByName( 'LE_DIAS_BM' ).AsInteger := Source.FieldByName( 'LE_DIAS_BM' ).AsInteger;
          ParamByName( 'LE_DIAS_CO' ).AsInteger := Source.FieldByName( 'LE_DIAS_CO' ).AsInteger;
          ParamByName( 'LE_EYM_DIN' ).AsFloat := Source.FieldByName( 'LE_EYM_DIN' ).AsFloat;
          ParamByName( 'LE_EYM_ESP' ).AsFloat := Source.FieldByName( 'LE_EYM_ESP' ).AsFloat;
          ParamByName( 'LE_EYM_EXC' ).AsFloat := Source.FieldByName( 'LE_EYM_EXC' ).AsFloat;
          ParamByName( 'LE_EYM_FIJ' ).AsFloat := Source.FieldByName( 'LE_EYM_FIJ' ).AsFloat;
          ParamByName( 'LE_GUARDER' ).AsFloat := Source.FieldByName( 'LE_GUARDER' ).AsFloat;
          ParamByName( 'LE_INF_AMO' ).AsFloat := Source.FieldByName( 'LE_INF_AMO' ).AsFloat;
          ParamByName( 'LE_INF_PAT' ).AsFloat := Source.FieldByName( 'LE_INF_PAT' ).AsFloat;
          ParamByName( 'LE_INV_VID' ).AsFloat := Source.FieldByName( 'LE_INV_VID' ).AsFloat;
          ParamByName( 'LE_RETIRO' ).AsFloat := Source.FieldByName( 'LE_RETIRO' ).AsFloat;
          ParamByName( 'LE_RIESGOS' ).AsFloat := Source.FieldByName( 'LE_RIESGOS' ).AsFloat;
          ParamByName( 'LS_PATRON' ).AsString := Source.FieldByName( 'LS_PATRON' ).AsString;
          ParamByName( 'LE_IMSS_OB' ).AsFloat := rImssOB;
          ParamByName( 'LE_IMSS_PA' ).AsFloat := rImssPA;
     end;
end;

procedure TdmMigrarDatos.Liq_IMSSMoveExcludedFields(Sender: TObject);
var
   Year, Month, Day: Word;
   rSubIMS, rTotIMS, rSubRet, rTotRet, rSubInf, rTotInf, rImssOB, rImssPA: Extended;
begin
     with Source do
     begin
          DecodeDate( FieldByName( 'LS_FECHA' ).AsDateTime, Year, Month, Day );
          rSubIMS := FieldByName( 'LS_EYM_FIJ' ).AsFloat +
                     FieldByName( 'LS_EYM_EXC' ).AsFloat +
                     FieldByName( 'LS_EYM_DIN' ).AsFloat +
                     FieldByName( 'LS_EYM_ESP' ).AsFloat +
                     FieldByName( 'LS_INV_VID' ).AsFloat +
                     FieldByName( 'LS_RIESGOS' ).AsFloat +
                     FieldByName( 'LS_GUARDER' ).AsFloat;
          rTotIMS := rSubIMS +
                     FieldByName( 'LS_ACT_IMS' ).AsFloat +
                     FieldByName( 'LS_REC_IMS' ).AsFloat;
          rSubRet := FieldByName( 'LS_CES_VEJ' ).AsFloat +
                     FieldByName( 'LS_RETIRO' ).AsFloat;
          rTotRet := rSubRet +
                     FieldByName( 'LS_ACT_RET' ).AsFloat +
                     FieldByName( 'LS_REC_RET' ).AsFloat +
                     FieldByName( 'LS_APO_VOL' ).AsFloat;
          rSubInf := FieldByName( 'LS_INF_NAC' ).AsFloat +
                     FieldByName( 'LS_INF_ACR' ).AsFloat +
                     FieldByName( 'LS_INF_AMO' ).AsFloat;
          rTotInf := rSubInf +
                     FieldByName( 'LS_ACT_INF' ).AsFloat +
                     FieldByName( 'LS_REC_INF' ).AsFloat;
          MigrarFecha( 'LS_FEC_REC', FieldByName( 'LS_FEC_REC' ).AsDateTime );
          MigrarStringKey( 'LS_PATRON', FieldByName( 'LS_PATRON' ).AsString );
          if EsVer260 then
          begin
               rImssOB := FieldByName( 'LS_IMSS_OB' ).AsFloat;
               rImssPA := FieldByName( 'LS_IMSS_PA' ).AsFloat;
          end
          else
          begin
               rImssOB := 0;
               rImssPA := 0;
          end;
     end;
     with TargetQuery do
     begin
          ParamByName( 'LS_YEAR' ).AsInteger := Year;
          ParamByName( 'LS_MONTH' ).AsInteger := Month;
          ParamByName( 'LS_TIPO' ).AsInteger := Ord( ConvierteTipoLiquidacion( Day  ) );
          ParamByName( 'LS_STATUS' ).AsInteger := 0;
          ParamByName( 'LS_SUB_IMS' ).AsFloat := rSubIMS;
          ParamByName( 'LS_TOT_IMS' ).AsFloat := rTotIMS;
          ParamByName( 'LS_SUB_RET' ).AsFloat := rSubRet;
          ParamByName( 'LS_TOT_RET' ).AsFloat := rTotRet;
          ParamByName( 'LS_SUB_INF' ).AsFloat := rSubInf;
          ParamByName( 'LS_TOT_INF' ).AsFloat := rTotInf;
          ParamByName( 'LS_TOT_MES' ).AsFloat := rTotIMS + rTotRet + rTotInf;
          ParamByName( 'US_CODIGO' ).AsInteger := 0;
          ParamByName( 'LS_ACT_AMO' ).AsFloat := Source.FieldByName( 'LS_ACT_AMO' ).AsFloat;
          ParamByName( 'LS_ACT_APO' ).AsFloat := Source.FieldByName( 'LS_ACT_APO' ).AsFloat;
          ParamByName( 'LS_ACT_IMS' ).AsFloat := Source.FieldByName( 'LS_ACT_IMS' ).AsFloat;
          ParamByName( 'LS_ACT_INF' ).AsFloat := Source.FieldByName( 'LS_ACT_INF' ).AsFloat;
          ParamByName( 'LS_ACT_RET' ).AsFloat := Source.FieldByName( 'LS_ACT_RET' ).AsFloat;
          ParamByName( 'LS_APO_VOL' ).AsFloat := Source.FieldByName( 'LS_APO_VOL' ).AsFloat;
          ParamByName( 'LS_CES_VEJ' ).AsFloat := Source.FieldByName( 'LS_CES_VEJ' ).AsFloat;
          ParamByName( 'LS_DIAS_BM' ).AsInteger := Source.FieldByName( 'LS_DIAS_BM' ).AsInteger;
          ParamByName( 'LS_DIAS_CO' ).AsInteger := Source.FieldByName( 'LS_DIAS_CO' ).AsInteger;
          ParamByName( 'LS_EYM_DIN' ).AsFloat := Source.FieldByName( 'LS_EYM_DIN' ).AsFloat;
          ParamByName( 'LS_EYM_ESP' ).AsFloat := Source.FieldByName( 'LS_EYM_ESP' ).AsFloat;
          ParamByName( 'LS_EYM_EXC' ).AsFloat := Source.FieldByName( 'LS_EYM_EXC' ).AsFloat;
          ParamByName( 'LS_EYM_FIJ' ).AsFloat := Source.FieldByName( 'LS_EYM_FIJ' ).AsFloat;
          ParamByName( 'LS_FAC_ACT' ).AsFloat := Source.FieldByName( 'LS_FAC_ACT' ).AsFloat;
          ParamByName( 'LS_FAC_REC' ).AsFloat := Source.FieldByName( 'LS_FAC_REC' ).AsFloat;
          ParamByName( 'LS_GUARDER' ).AsFloat := Source.FieldByName( 'LS_GUARDER' ).AsFloat;
          ParamByName( 'LS_INF_ACR' ).AsFloat := Source.FieldByName( 'LS_INF_ACR' ).AsFloat;
          ParamByName( 'LS_INF_AMO' ).AsFloat := Source.FieldByName( 'LS_INF_AMO' ).AsFloat;
          ParamByName( 'LS_INF_NAC' ).AsFloat := Source.FieldByName( 'LS_INF_NAC' ).AsFloat;
          ParamByName( 'LS_INF_NUM' ).AsInteger := Source.FieldByName( 'LS_INF_NUM' ).AsInteger;
          ParamByName( 'LS_INV_VID' ).AsFloat := Source.FieldByName( 'LS_INV_VID' ).AsFloat;
          ParamByName( 'LS_NUM_BIM' ).AsInteger := Source.FieldByName( 'LS_NUM_BIM' ).AsInteger;
          ParamByName( 'LS_NUM_TRA' ).AsInteger := Source.FieldByName( 'LS_NUM_TRA' ).AsInteger;
          ParamByName( 'LS_REC_AMO' ).AsFloat := Source.FieldByName( 'LS_REC_AMO' ).AsFloat;
          ParamByName( 'LS_REC_APO' ).AsFloat := Source.FieldByName( 'LS_REC_APO' ).AsFloat;
          ParamByName( 'LS_REC_IMS' ).AsFloat := Source.FieldByName( 'LS_REC_IMS' ).AsFloat;
          ParamByName( 'LS_REC_INF' ).AsFloat := Source.FieldByName( 'LS_REC_INF' ).AsFloat;
          ParamByName( 'LS_REC_RET' ).AsFloat := Source.FieldByName( 'LS_REC_RET' ).AsFloat;
          ParamByName( 'LS_RETIRO' ).AsFloat := Source.FieldByName( 'LS_RETIRO' ).AsFloat;
          ParamByName( 'LS_RIESGOS' ).AsFloat := Source.FieldByName( 'LS_RIESGOS' ).AsFloat;
          ParamByName( 'LS_IMSS_OB' ).AsFloat := rImssOB;
          ParamByName( 'LS_IMSS_PA' ).AsFloat := rImssPA;
     end;
end;

procedure TdmMigrarDatos.Liq_MovMoveExcludedFields(Sender: TObject);

function EstaDuplicado( const sPatron, sTipo: String; iYear, iMonth: Integer; eTipo: eTipoLiqIMSS; iEmpleado: TNumEmp; dFecha: TDate ): Boolean;
begin
     with tqUno do
     begin
          Active := False;
          ParamByName( 'LS_PATRON' ).AsString := sPatron;
          ParamByName( 'LS_YEAR' ).AsInteger := iYear;
          ParamByName( 'LS_MONTH' ).AsInteger := iMonth;
          ParamByName( 'LS_TIPO' ).AsInteger := Ord( eTipo );
          ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
          ParamByName( 'LM_TIPO' ).AsString := sTipo;
          ParamByName( 'LM_FECHA' ).AsDateTime := dFecha;
          Active := True;
          if IsEmpty then
             Result := False
          else
              Result := ( Fields[ 0 ].AsInteger > 0 );
     end;
end;

var
   dFecha: TDate;
   sPatron, sTipo: String;
   iYear, iMonth, iDay: Word;
   eTipo: eTipoLiqIMSS;
   iEmpleado: TNumEmp;
begin
     with Source do
     begin
          sPatron := FieldByName( 'LS_PATRON' ).AsString;
          DecodeDate( FieldByName( 'LS_FECHA' ).AsDateTime, iYear, iMonth, iDay );
          eTipo := ConvierteTipoLiquidacion( iDay );
          iEmpleado := LeeCodigoEmpleado;
          sTipo := FieldByName( 'LM_TIPO' ).AsString;
          dFecha := FieldByName( 'LM_FECHA' ).AsDateTime;
          if EstaDuplicado( sPatron, sTipo, iYear, iMonth, eTipo, iEmpleado, dFecha ) then
             dFecha := dFecha + 1;
          MigrarFecha( 'LM_KAR_FEC', FieldByName( 'LM_KAR_FEC' ).AsDateTime );
          MigrarStringKey( 'LM_CLAVE', FieldByName( 'LM_CLAVE' ).AsString );
          MigrarStringKey( 'LM_KAR_TIP', FieldByName( 'LM_KAR_TIP' ).AsString );
     end;
     MigrarEmpleado( iEmpleado );
     MigrarFechaKey( 'LM_FECHA', dFecha );
     with TargetQuery do
     begin
          ParamByName( 'LS_MONTH' ).AsInteger := iMonth;
          ParamByName( 'LS_YEAR' ).AsInteger := iYear;
          ParamByName( 'LS_TIPO' ).AsInteger := Ord( eTipo );
          ParamByName( 'LM_TIPO' ).AsString := sTipo;
          ParamByName( 'LM_AUSENCI' ).AsInteger := Source.FieldByName( 'LM_AUSENCI' ).AsInteger;
          ParamByName( 'LM_BASE' ).AsFloat := Source.FieldByName( 'LM_BASE' ).AsFloat;
          ParamByName( 'LM_CES_VEJ' ).AsFloat := Source.FieldByName( 'LM_CES_VEJ' ).AsFloat;
          ParamByName( 'LM_DIAS' ).AsInteger := Source.FieldByName( 'LM_DIAS' ).AsInteger;
          ParamByName( 'LM_EYM_DIN' ).AsFloat := Source.FieldByName( 'LM_EYM_DIN' ).AsFloat;
          ParamByName( 'LM_EYM_ESP' ).AsFloat := Source.FieldByName( 'LM_EYM_ESP' ).AsFloat;
          ParamByName( 'LM_EYM_EXC' ).AsFloat := Source.FieldByName( 'LM_EYM_EXC' ).AsFloat;
          ParamByName( 'LM_EYM_FIJ' ).AsFloat := Source.FieldByName( 'LM_EYM_FIJ' ).AsFloat;
          ParamByName( 'LM_GUARDER' ).AsFloat := Source.FieldByName( 'LM_GUARDER' ).AsFloat;
          ParamByName( 'LM_INF_AMO' ).AsFloat := Source.FieldByName( 'LM_INF_AMO' ).AsFloat;
          ParamByName( 'LM_INV_VID' ).AsFloat := Source.FieldByName( 'LM_INV_VID' ).AsFloat;
          ParamByName( 'LM_RETIRO' ).AsFloat := Source.FieldByName( 'LM_RETIRO' ).AsFloat;
          ParamByName( 'LM_RIESGOS' ).AsFloat := Source.FieldByName( 'LM_RIESGOS' ).AsFloat;
          ParamByName( 'LS_PATRON' ).AsString := sPatron;
          ParamByName( 'LM_INF_PAT' ).AsFloat := Source.FieldByName( 'LM_INF_PAT' ).AsFloat;
          ParamByName( 'LM_INCAPAC' ).AsInteger := Source.FieldByName( 'LM_INCAPAC' ).AsInteger;
     end;
end;

procedure TdmMigrarDatos.Liq_MovOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select COUNT(*) from LIQ_MOV where ' +
                                     '( LS_PATRON = :LS_PATRON ) and ' +
                                     '( LS_YEAR = :LS_YEAR ) and ' +
                                     '( LS_MONTH = :LS_MONTH ) and ' +
                                     '( LS_TIPO = :LS_TIPO ) and ' +
                                     '( CB_CODIGO = :CB_CODIGO ) and ' +
                                     '( LM_TIPO = :LM_TIPO ) and ' +
                                     '( LM_FECHA = :LM_FECHA )' );
     end;
end;

procedure TdmMigrarDatos.Liq_MovCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.MovimienMoveExcludedFields(Sender: TObject);
var
   sValor: String;
   rPercepcion, rDeduccion: Extended;
begin
     with Source do
     begin
          sValor := FieldByName( 'NO_PERIODO' ).AsString;
          rPercepcion := FieldByName( 'MO_MONTO' ).AsFloat;
          rDeduccion := 0;
          if not FieldByName( 'MO_PERCEP' ).AsBoolean then
          begin
               rDeduccion := rPercepcion;
               rPercepcion := 0;
          end;
     end;
     MigrarCodigoEmpleado;
     MigrarBooleanos( 'MO_ACTIVO', 'MO_ACTIVO' );
     MigrarBooleanos( 'MO_PER_CAL', 'MO_PER_CAL' );
     MigrarUsuario( 'US_CODIGO', 'MO_USER' );
     with TargetQuery do
     begin
          ParamByName( 'PE_YEAR' ).AsInteger := Anio;
          ParamByName( 'PE_TIPO' ).AsInteger := Str2Integer( Copy( sValor, 1, 1 ) );
          ParamByName( 'PE_NUMERO' ).AsInteger := Str2Integer( Copy( sValor, 2, 3 ) );
          ParamByName( 'CO_NUMERO' ).AsInteger := ConvierteConcepto( Source.FieldByName( 'CO_NUMERO' ).AsString );
          ParamByName( 'MO_PERCEPC' ).AsFloat := rPercepcion;
          ParamByName( 'MO_DEDUCCI' ).AsFloat := rDeduccion;
          ParamByName( 'MO_REFEREN' ).AsString := Trim( Source.FieldByName( 'MO_REFEREN' ).AsString );
          ParamByName( 'MO_IMP_CAL' ).AsFloat := Source.FieldByName( 'MO_IMP_CAL' ).AsFloat;
          ParamByName( 'MO_X_ISPT' ).AsFloat := Source.FieldByName( 'MO_X_ISPT' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.ClasifiMoveExcludedFields(Sender: TObject);
var
   sValor: String;
begin
     sValor := Source.FieldByName( 'TB_ELEMENT' ).AsString;
     MigrarStringKeyNull( 'TB_OP1', Copy( sValor, 16, 2 ) );
     MigrarStringKeyNull( 'TB_OP2', Copy( sValor, 18, 2 ) );
     MigrarStringKeyNull( 'TB_OP3', Copy( sValor, 20, 2 ) );
     MigrarStringKeyNull( 'TB_OP4', Copy( sValor, 22, 2 ) );
     MigrarStringKeyNull( 'TB_OP5', Copy( sValor, 24, 2 ) );
     with TargetQuery do
     begin
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_ELEMENT' ).AsString := Copy( sValor, 1, 15 );
          ParamByName( 'TB_SALARIO' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
     end;
end;

procedure TdmMigrarDatos.ConceptoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     with DataSet do
     begin
          Tag := Str2Integer( FieldByName( 'CO_NUMERO' ).AsString );
          Accept := ( Tag > 0 ) and ( Tag < 1000 );
          Tag := 0;
     end;
end;

procedure TdmMigrarDatos.ConceptoMoveExcludedFields(Sender: TObject);
var
   iConcepto: Integer;

function RevisaConcepto( const sFormula, sMensaje: String; const lValor: Boolean ): String;
begin
     Result := VerificaFormula( sFormula, sMensaje, lValor );
     if ( ( Result = 'N' ) or ( Result = 'NO' ) ) then
        Result := '';
end;

function RevisaRecibo(const sFormula, sMensaje: String; const lValor: Boolean): string;
begin
     Result := ChecaFormula( sFormula, sMensaje, lValor );
     if ( AnsiPos( 'CALC_CREDITO', Result ) > 0 ) OR
        ( AnsiPos( 'ART86', Result ) > 0 ) OR
        ( AnsiPos( 'ART86I', Result ) > 0 ) OR
        ( AnsiPos( 'CALC_ISPT', Result ) > 0 ) OR
        ( AnsiPos( 'ART80_SEPARA', Result ) > 0 ) then
     begin
          Bitacora.AgregaResumen( sMensaje + CR_LF +
                                  'No se Pueden Utilizar las Funciones CALC_CREDITO(), CALC_ISPT(), ART86(), ART86_I() y/o ART80_SEPARA()' );
     end;
end;

begin
     iConcepto := Str2Integer( Source.FieldByName( 'CO_NUMERO' ).AsString );
     MigrarBooleanos( 'CO_ACTIVO', 'CO_ACTIVO' );
     MigrarBooleanos( 'CO_CALCULA', 'CO_CALCULA' );
     MigrarBooleanos( 'CO_A_PTU', 'CO_A_PTU' );
     MigrarBooleanos( 'CO_G_IMSS', 'CO_G_IMSS' );
     MigrarBooleanos( 'CO_G_ISPT', 'CO_G_ISPT' );
     MigrarBooleanos( 'CO_MENSUAL', 'CO_MENSUAL' );
     MigrarBooleanos( 'CO_IMPRIME', 'CO_IMPRIME' );
     MigrarBooleanos( 'CO_LISTADO', 'CO_LISTADO' );
     MigrarBooleanos( 'CO_ISN', 'CO_ISN' );
     MigrarStringKeyNull( 'CO_QUERY', QuitaTodos( Source.FieldByName( 'CO_QUERY' ).AsString ) );
     with TargetQuery do
     begin
          ParamByName( 'CO_NUMERO' ).AsInteger := iConcepto;
          ParamByName( 'CO_FORMULA' ).AsString := VerificaFormula( Source.FieldByName( 'CO_FORMULA' ).AsString, Format( 'Concepto %d: F�rmula Para Calcular ( CONCEPTO.CO_FORMULA )', [ iConcepto ] ), False );
          ParamByName( 'CO_X_ISPT' ).AsString := RevisaConcepto( Source.FieldByName( 'CO_X_ISPT' ).AsString, Format( 'Concepto %d: F�rmula Exento ISPT ( CONCEPTO.CO_X_ISPT )', [ iConcepto ] ), False );
          ParamByName( 'CO_IMP_CAL' ).AsString := RevisaConcepto( Source.FieldByName( 'CO_IMP_CAL' ).AsString, Format( 'Concepto %d: F�rmula ISPT Individual ( CONCEPTO.CO_IMP_CAL )', [ iConcepto ] ), False );
          ParamByName( 'CO_RECIBO' ).AsString := RevisaRecibo(Source.FieldByName( 'CO_RECIBO' ).AsString, Format( 'Concepto %d: F�rmula para Recibo ( CONCEPTO.CO_RECIBO )', [ iConcepto ] ), False );
          ParamByName( 'CO_DESCRIP' ).AsString := Source.FieldByName( 'CO_DESCRIP' ).AsString;
          ParamByName( 'CO_TIPO' ).AsInteger := Source.FieldByName( 'CO_TIPO' ).AsInteger;
     end;
end;

procedure TdmMigrarDatos.ConceptoAdditionalRows(Sender: TObject);
var
   sNumero: String;
begin
     inherited;
     with FConcepto do
     begin
          sNumero := FConcepto.Strings[ TablaMigrada.Counter - 1 ];
     end;
     MigrarStringKeyNull( 'CO_QUERY', K_STRING_NULO );
     with TargetQuery do
     begin
          ParamByName( 'CO_NUMERO' ).AsInteger := Str2Integer( Copy( sNumero, 4, 4 ) );
          ParamByName( 'CO_DESCRIP' ).AsString := Copy( sNumero, 9, ( Length( sNumero ) - 8 ) );
          ParamByName( 'CO_FORMULA' ).AsString := K_STRING_NULO;
          ParamByName( 'CO_X_ISPT' ).AsString := K_STRING_NULO;
          ParamByName( 'CO_IMP_CAL' ).AsString := K_STRING_NULO;
          ParamByName( 'CO_RECIBO' ).AsString := K_STRING_NULO;
          ParamByName( 'CO_TIPO' ).AsInteger := Ord( coAcumulado );
          ParamByName( 'CO_ACTIVO' ).AsString := K_GLOBAL_SI;
          ParamByName( 'CO_CALCULA' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_A_PTU' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_G_IMSS' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_G_ISPT' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_MENSUAL' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_IMPRIME' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_LISTADO' ).AsString := K_GLOBAL_NO;
          ParamByName( 'CO_ISN' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmMigrarDatos.ContratoMoveExcludedFields(Sender: TObject);
begin
     with TargetQuery do
     begin
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_DIAS' ).AsInteger := Source.FieldByName( 'TB_NUMERO' ).AsInteger;
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
     end;
end;

procedure TdmMigrarDatos.CursoMoveExcludedFields(Sender: TObject);
var
   sValue: String;

function BuscaMaestro( const sCodigo: String ): Boolean;
begin
     with tqUno do
     begin
          Active := False;
          Params[ 0 ].AsString := sCodigo;
          Active := True;
          Result := not IsEmpty;
     end;
end;

begin
     with Source do
     begin
          sValue := FieldByName( 'MA_CODIGO' ).AsString;
          if not BuscaMaestro( sValue ) then
             sValue := K_STRING_NULO;
          MigrarStringKey( 'CU_CLASIFI', FieldByName( 'CU_CLASIFI' ).AsString );
     end;
     MigrarStringKeyNull( 'MA_CODIGO', sValue );
     with TargetQuery do
     begin
          ParamByName( 'CU_CODIGO' ).AsString := Trim( Source.FieldByName( 'CU_CURSO' ).AsString );
          ParamByName( 'CU_ACTIVO' ).AsString := zBoolToStr( not ( Source.FieldByName( 'CU_CANCEL' ).AsString = K_GLOBAL_SI ) );
          ParamByName( 'CU_NOMBRE' ).AsString := Limita( Source.FieldByName( 'CU_NOMBRE' ).AsString, 40 );
          ParamByName( 'CU_INGLES' ).AsString := Limita( Source.FieldByName( 'CU_NAME' ).AsString, 40 );
          ParamByName( 'CU_TEXTO1' ).AsString := Source.FieldByName( 'CU_TEXTO' ).AsString;
          ParamByName( 'CU_TEXTO2' ).AsString := Source.FieldByName( 'CU_TEXT' ).AsString;
          ParamByName( 'CU_COSTO1' ).AsFloat := Source.FieldByName( 'CU_COSTO1' ).AsFloat;
          ParamByName( 'CU_COSTO2' ).AsFloat := Source.FieldByName( 'CU_COSTO2' ).AsFloat;
          ParamByName( 'CU_HORAS' ).AsFloat := Source.FieldByName( 'CU_HORAS' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.CursoOpenDatasets(Sender: TObject);
begin
     TablaMigrada.PrepareTargetQuery( tqUno, 'select MA_CODIGO from MAESTRO where ( MA_CODIGO = :MA_CODIGO )' );
end;

procedure TdmMigrarDatos.CursoCloseDatasets(Sender: TObject);
begin
     inherited;
     TablaMigrada.CloseQuery( tqUno );
end;

procedure TdmMigrarDatos.EventoMoveExcludedFields(Sender: TObject);
var
   sCodigo, sAntig, sCampo, sFormula, sValor: String;
   iPos: Integer;

function TopaEntero( const iValor: Integer ): Integer;
begin
     Result := iMin( iMax( iValor, 0 ), 30000 );
end;

function ExtractFieldName( const sValue: String ): String;
var
   i: Integer;
begin
     i := Pos( '->', sValue );
     if ( i > 0 ) then
        Result := Copy( sValue, ( i + 2 ), ( Length( sValue ) - ( i + 2 ) + 1 ) )
     else
         Result := sValue;
end;

begin
     with Source do
     begin
          sValor := Trim( Source.FieldByName( 'EV_AUTOSAL' ).AsString );
          sCodigo := FieldByName( 'EV_CODIGO' ).AsString;
          MigrarFecha( 'EV_FEC_BSS', FieldByName( 'EV_FEC_BSS' ).AsDateTime );
          MigrarStringKeyNull( 'EV_QUERY', QuitaTodos( FieldByName( 'EV_QUERY' ).AsString ) );
          MigrarStringKeyNull( 'EV_CLASIFI', FieldByName( 'EV_CLASIFI' ).AsString );
          MigrarStringKeyNull( 'EV_PUESTO', FieldByName( 'EV_PUESTO' ).AsString );
          MigrarStringKeyNull( 'EV_CONTRAT', FieldByName( 'EV_CONTRAT' ).AsString );
          MigrarStringKeyNull( 'EV_KARDEX', FieldByName( 'EV_KARDEX' ).AsString );
          MigrarStringKeyNull( 'EV_MOT_BAJ', FieldByName( 'EV_MOT_BAJ' ).AsString );
          MigrarStringKeyNull( 'EV_OTRAS_1', FieldByName( 'EV_OTRAS_1' ).AsString );
          MigrarStringKeyNull( 'EV_OTRAS_2', FieldByName( 'EV_OTRAS_2' ).AsString );
          MigrarStringKeyNull( 'EV_OTRAS_3', FieldByName( 'EV_OTRAS_3' ).AsString );
          MigrarStringKeyNull( 'EV_OTRAS_4', FieldByName( 'EV_OTRAS_4' ).AsString );
          MigrarStringKeyNull( 'EV_OTRAS_5', FieldByName( 'EV_OTRAS_5' ).AsString );
          MigrarStringKeyNull( 'EV_TABLASS', FieldByName( 'EV_TABLASS' ).AsString );
          MigrarStringKeyNull( 'EV_TURNO', FieldByName( 'EV_HORARIO' ).AsString );
          MigrarStringKeyNull( 'EV_PATRON', FieldByName( 'EV_PATRON' ).AsString );
          MigrarIntegerKeyNull( 'EV_NOMYEAR', 0 );
          MigrarIntegerKeyNull( 'EV_NOMTIPO', Ord( tpSemanal ) ); // ( 1 ) //
          MigrarIntegerKeyNull( 'EV_NOMNUME', FieldByName( 'EV_PE_NUM' ).AsInteger );
          if EsVer260 then
          begin
               sAntig := zBoolToStr( FieldByName( 'EV_M_ANTIG' ).AsBoolean );
               sFormula := FieldByName( 'EV_FORMULA' ).AsString;
               iPos := Pos( ':=', sFormula );
               if ( iPos > 0 ) then
               begin
                    sCampo := ExtractFieldName( Copy( sFormula, 1, ( iPos - 1 ) ) );
                    sFormula := Copy( sFormula, ( iPos + 2 ), ( Length( sFormula ) - ( iPos + 2 ) + 1 ) );
               end
               else
                   sCampo := '';
          end
          else
          begin
               sAntig := K_GLOBAL_NO;
               sCampo := '';
               sFormula := '';
          end;
     end;
     MigrarBooleanos( 'EV_ACTIVO', 'EV_ACTIVO' );
     MigrarBooleanos( 'EV_ALTA', 'EV_ALTA' );
     MigrarBooleanos( 'EV_BAJA', 'EV_BAJA' );
     MigrarAsInteger( 'EV_PRIORID', 'EV_PRIORID' );
     MigrarNivelesNull( 'EV_NIVEL', 'EV_NIVELES' );
     with TargetQuery do
     begin
          ParamByName( 'EV_FILTRO' ).AsString := ChecaFormula( Source.FieldByName( 'EV_FILTRO' ).AsString, Format( 'Evento %s: Filtro ( EVENTO.EV_FILTRO )', [ sCodigo ] ), True  );
          ParamByName( 'EV_SALARIO' ).AsString := ChecaFormula( Source.FieldByName( 'EV_SALARIO' ).AsString, Format( 'Evento %s: F�rmula Salario ( EVENTO.EV_SALARIO )', [ sCodigo ] ), False );
          ParamByName( 'EV_INCLUYE' ).AsInteger := Ord( ieEmpleados ); // ( 0 ) //
          ParamByName( 'EV_ANT_INI' ).AsInteger := TopaEntero( Source.FieldByName( 'EV_ANT_INI' ).AsInteger );
          ParamByName( 'EV_ANT_FIN' ).AsInteger := TopaEntero( Source.FieldByName( 'EV_ANT_FIN' ).AsInteger );
          ParamByName( 'EV_AUTOSAL' ).AsString := SiNo( sValor );
          ParamByName( 'EV_TABULA' ).AsString := zBoolToStr( StrLleno( sValor ) );
          ParamByName( 'EV_CODIGO' ).AsString := sCodigo;
          ParamByName( 'EV_DESCRIP' ).AsString := Source.FieldByName( 'EV_DESCRIP' ).AsString;
          ParamByName( 'EV_M_ANTIG' ).AsString := sAntig;
          ParamByName( 'EV_CAMPO' ).AsString := sCampo;
          ParamByName( 'EV_FORMULA' ).AsString := ChecaFormula( sFormula, Format( 'Evento %s: F�rmula Adicional ( EVENTO.EV_FORMULA )', [ sCodigo ] ), False );
          ParamByName( 'EV_FEC_CON' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmMigrarDatos.FestivoMoveExcludedFields(Sender: TObject);
var
   sValor: String;
begin
     with Source do
     begin
          sValor := FieldByName( 'FE_MES_DIA' ).AsString;
          MigrarFecha( 'FE_CAMBIO', FieldByName( 'FE_CAMBIO' ).AsDateTime );
     end;
     with TargetQuery do
     begin
          ParamByName( 'TU_CODIGO' ).AsString := Source.FieldByName( 'TU_CODIGO' ).AsString;
          ParamByName( 'FE_MES' ).AsInteger := Str2Integer( Copy( sValor, 1, 2 ) );
          ParamByName( 'FE_DIA' ).AsInteger := Str2Integer( Copy( sValor, 4, 2 ) );
          ParamByName( 'FE_TIPO' ).AsInteger := BoolToInt( Source.FieldByName( 'FE_TIPO' ).AsString = 'I' );
          ParamByName( 'FE_DESCRIP' ).AsString := Source.FieldByName( 'FE_DESCRIP' ).AsString;
     end;
end;

procedure TdmMigrarDatos.HorarioMoveExcludedFields(Sender: TObject);
var
   iExtCom, iExtMin: Integer;
   sOutBrk, sInBrk: String;
begin
     MigrarBooleanos( 'HO_ADD_EAT', 'HO_ADD_EAT' );
     MigrarBooleanos( 'HO_CHK_EAT', 'HO_CHK_EAT' );
     MigrarBooleanos( 'HO_IGN_EAT', 'HO_IGN_EAT' );
     if EsVer260 then
     begin
          with Source do
          begin
               iExtCom := FieldByName( 'HO_EXT_COM' ).AsInteger;
               iExtMin := FieldByName( 'HO_EXT_MIN' ).AsInteger;
               sOutBrk := FieldByName( 'HO_OUT_BRK' ).AsString;
               sInBrk := FieldByName( 'HO_IN_BRK' ).AsString;
          end;
     end
     else
     begin
          iExtCom := 0;
          iExtMin := 0;
          sOutBrk := '';
          sInBrk := '';
     end;
     with TargetQuery do
     begin
          ParamByName( 'HO_TIPO' ).AsInteger := Ord( ConvierteHorario( Source.FieldByName( 'HO_TIPO' ).AsInteger ) );
          ParamByName( 'HO_CODIGO' ).AsString := Source.FieldByName( 'HO_CODIGO' ).AsString;
          ParamByName( 'HO_COMER' ).AsInteger := Source.FieldByName( 'HO_COMER' ).AsInteger;
          ParamByName( 'HO_DESCRIP' ).AsString := Source.FieldByName( 'HO_DESCRIP' ).AsString;
          ParamByName( 'HO_DOBLES' ).AsFloat := Source.FieldByName( 'HO_DOBLES' ).AsFloat;
          ParamByName( 'HO_EXT_FIJ' ).AsFloat := Source.FieldByName( 'HO_EXT_FIJ' ).AsFloat;
          ParamByName( 'HO_IN_EAT' ).AsString := Source.FieldByName( 'HO_IN_EAT' ).AsString;
          ParamByName( 'HO_IN_TARD' ).AsInteger := Source.FieldByName( 'HO_IN_TARD' ).AsInteger;
          ParamByName( 'HO_IN_TEMP' ).AsInteger := Source.FieldByName( 'HO_IN_TEMP' ).AsInteger;
          ParamByName( 'HO_INTIME' ).AsString := Source.FieldByName( 'HO_INTIME' ).AsString;
          ParamByName( 'HO_LASTOUT' ).AsString := Source.FieldByName( 'HO_LASTOUT' ).AsString;
          ParamByName( 'HO_MIN_EAT' ).AsInteger := Source.FieldByName( 'HO_MIN_EAT' ).AsInteger;
          ParamByName( 'HO_JORNADA' ).AsFloat := Source.FieldByName( 'HO_JORNADA' ).AsFloat;
          ParamByName( 'HO_OU_TARD' ).AsInteger := Source.FieldByName( 'HO_OU_TARD' ).AsInteger;
          ParamByName( 'HO_OU_TEMP' ).AsInteger := Source.FieldByName( 'HO_OU_TEMP' ).AsInteger;
          ParamByName( 'HO_OUT_EAT' ).AsString := Source.FieldByName( 'HO_OUT_EAT' ).AsString;
          ParamByName( 'HO_OUTTIME' ).AsString := Source.FieldByName( 'HO_OUTTIME' ).AsString;
          ParamByName( 'HO_EXT_COM' ).AsInteger := iExtCom;
          ParamByName( 'HO_EXT_MIN' ).AsInteger := iExtMin;
          ParamByName( 'HO_OUT_BRK' ).AsString := sOutBrk;
          ParamByName( 'HO_IN_BRK' ).AsString := sInBrk;
     end;
end;

procedure TdmMigrarDatos.IncidenMoveExcludedFields(Sender: TObject);
var
   iValor: Integer;
begin
     iValor := Source.FieldByName( 'TB_NUMERO' ).AsInteger;
     with TargetQuery do
     begin
          ParamByName( 'TB_INCAPA' ).AsInteger := iValor;
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_INCIDEN' ).AsInteger := Ord( ConvierteIncidencia( Trim( Source.FieldByName( 'TB_CODIGO' ).AsString ), iValor ) );
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
     end;
end;

procedure TdmMigrarDatos.KarCursoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     Accept := Dataset.FieldByName( 'KA_STATUS' ).AsBoolean;
end;

procedure TdmMigrarDatos.KarCursoMoveExcludedFields(Sender: TObject);
var
   dTomado: TDate;
begin
     with Source do
     begin
          dTomado := FieldByName( 'KA_FECHA' ).AsDateTime;
          if ( dTomado = NullDateTime ) then
             dTomado := FieldByName( 'KA_PROGRAM' ).AsDateTime;
          MigrarStringKey( 'CU_CODIGO', Trim( Source.FieldByName( 'CU_CURSO' ).AsString ) );
          MigrarStringKey( 'CB_PUESTO', FieldByName( 'PU_PUESTO' ).AsString );
          MigrarStringKey( 'CB_TURNO', FieldByName( 'CB_HORARIO' ).AsString );
          MigrarStringKey( 'CB_CLASIFI', FieldByName( 'CB_CLASIFI' ).AsString );
          MigrarStringKey( 'MA_CODIGO', FieldByName( 'MA_CODIGO' ).AsString );
     end;
     MigrarCodigoEmpleado;
     MigrarNiveles( 'CB_NIVEL', 'CB_NIVELES' );
     MigrarFecha( 'KC_FEC_TOM', dTomado );
     with TargetQuery do
     begin
          ParamByName( 'KC_EVALUA' ).AsFloat := Source.FieldByName( 'KA_EVALUA' ).AsFloat;
          ParamByName( 'KC_HORAS' ).AsFloat := Source.FieldByName( 'KA_HORAS' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.MonedaMoveExcludedFields(Sender: TObject);
begin
     with TargetQuery do
     begin
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_VALOR' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.Mot_BajaMoveExcludedFields(Sender: TObject);
begin
     with TargetQuery do
     begin
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_IMSS' ).AsFloat := rMax( 1, Source.FieldByName( 'TB_NUMERO' ).AsFloat );
     end;
end;

procedure TdmMigrarDatos.PRiesgoMoveExcludedFields(Sender: TObject);
var
   dValue: TDate;
   rValue: Extended;
begin
     dValue := FirstDayOfYear( Anio );
     rValue := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
     with tqUno do
     begin
          if ( rValue <= 0.00 ) and not IsEmpty then
             rValue := FieldByName( 'SS_P_RT' ).AsFloat;
     end;
     MigrarFecha( 'RT_FECHA', dValue );
     with TargetQuery do
     begin
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'RT_PRIMA' ).AsFloat := rValue;
     end;
end;

procedure TdmMigrarDatos.PRiesgoOpenDatasets(Sender: TObject);
begin
     TablaMigrada.PrepareSourceQuery( tqUno, 'select SS_P_RT from LEY_IMSS.DBF where ' +
                                             '( :Fecha >= SS_INICIAL ) and ' +
                                             '( :Fecha <= SS_FINAL )' );
     with tqUno do
     begin
          ParamByName( 'Fecha' ).AsDateTime := FirstDayOfYear( Anio );
          Active := True;
     end;
end;

procedure TdmMigrarDatos.PRiesgoCloseDatasets(Sender: TObject);
begin
     TablaMigrada.CloseQuery( tqUno );
end;

procedure TdmMigrarDatos.PuestoMoveExcludedFields(Sender: TObject);
begin
     MigrarStringKeyNull( 'PU_CLASIFI', Copy( Source.FieldByName( 'TB_LETRA' ).AsString, 1, 4 ) );
     with TargetQuery do
     begin
          ParamByName( 'PU_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'PU_DESCRIP' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'PU_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'PU_NUMERO' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
          ParamByName( 'PU_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'PU_DETALLE' ).AsString := K_STRING_NULO;
          { 2.4: Campo nuevo en versi�n de datos 380 }
          ParamByName( 'PU_ACTIVO' ).AsString := K_GLOBAL_SI;
          ParamByName( 'PU_CHECA' ).AsString := K_USAR_GLOBAL;
          ParamByName( 'PU_AUTOSAL' ).AsString := K_USAR_GLOBAL;
     end;
end;

procedure TdmMigrarDatos.QuerysMoveExcludedFields(Sender: TObject);

 procedure ValidaColabora( sFiltro: String );
 var
    iPos: Integer;
 begin
      sFiltro := UpperCase(sFiltro);
      iPos := Pos('->',sFiltro);
      if iPos>0 then
      begin
           if sFiltro[iPos-1]='A' then
           begin
                if iPos-2<=0 then EXIT; //Quiere decir que es inicio de la expresion
                if not(sFiltro[iPos-2] in ['A'..'Z'])then EXIT;
           end;
           sFiltro := Copy( sFiltro,iPos-9,8);
           if sFiltro<>'COLABORA' then
           begin
                Bitacora.AddWarning( 'CONDICIONES',
                                     'La CONDICION ' + Source.FieldByName( 'QU_CODIGO' ).AsString +
                                     CR_LF +
                                     'Contiene Expresiones NO Compatibles Con La Tabla COLABORA' +
                                     CR_LF +
                                     'Sugerencia: Usar Funciones Equivalentes' );
           end;
      end;
 end;

 var sFiltro : string;
begin
     MigrarUsuario( 'US_CODIGO', 'QU_USER' );
     MigrarAsInteger( 'QU_NIVEL', 'QU_NIVEL' );
     with TargetQuery do
     begin
          sFiltro := Source.FieldByName( 'QU_FILTRO' ).AsString;
          {Para Catalogo de Condiciones:
          -Que se valide si viene un -> y lo que esta antes de la flechita
          no es colabora, que se genere un warning.}
          ValidaColabora( sFiltro );
          sFiltro := ConvierteFormula( sFiltro, TRUE );
          {-Revisar los campos huerfanos, para encontrar campos anteponerles COLABORA,}
          ParamByName( 'QU_FILTRO' ).AsString := ChecaAnchoFormula(ConvierteCamposColabora(aEmpleados,'COLABORA',sFiltro),'F�rmula ( QUERYS.QU_FILTRO )');
          ParamByName( 'QU_CODIGO' ).AsString := Source.FieldByName( 'QU_CODIGO' ).AsString;
          ParamByName( 'QU_DESCRIP' ).AsString := Source.FieldByName( 'QU_DESCRIP' ).AsString;
     end;
end;

procedure TdmMigrarDatos.RPatronMoveExcludedFields(Sender: TObject);
var
   sValor: String;
begin
     with Source do
     begin
          sValor := FieldByName( 'TB_CODIGO' ).AsString
     end;
     with TargetQuery do
     begin
          ParamByName( 'TB_ELEMENT' ).AsString := 'PATRON # ' + sValor;
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_NUMREG' ).AsString := Copy( Source.FieldByName( 'TB_ELEMENT' ).AsString, 1, 11 );
          ParamByName( 'TB_MODULO' ).AsInteger := Str2Integer( Copy( Source.FieldByName( 'TB_LETRA' ).AsString, 1, 1 ) );
          ParamByName( 'TB_CODIGO' ).AsString := sValor;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
     end;
end;

procedure TdmMigrarDatos.SSocialMoveExcludedFields(Sender: TObject);
var
   sValor: String;
begin
     with Source do
     begin
          sValor := FieldByName( 'T2_ELEMENT' ).AsString;
     end;
     with TargetQuery do
     begin
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_DIAS_AD' ).AsInteger := 0;
          ParamByName( 'TB_PRIMAVA' ).AsFloat := ( StrToReal( Source.FieldByName( 'T2_LETRA' ).AsString ) / 10 );
          ParamByName( 'TB_DIAS_AG' ).AsFloat := ( Source.FieldByName( 'T2_NUMERO' ).AsFloat / 10 );
          ParamByName( 'TB_PAGO_7' ).AsString := zBoolToStr( Copy( sValor, 6, 1 ) <> K_GLOBAL_NO );
          ParamByName( 'TB_PRIMA_7' ).AsString := zBoolToStr( Copy( sValor, 1, 1 ) = K_GLOBAL_SI );
          ParamByName( 'TB_PRIMADO' ).AsFloat := StrToReal( Copy( sValor, 2, 4 ) ) / 10;
     end;
end;

procedure TdmMigrarDatos.TKardexMoveExcludedFields(Sender: TObject);
var
   sTipo: String;
begin
     with Source do
     begin
          sTipo := FieldByName( 'TB_CODIGO' ).AsString;
     end;
     with TargetQuery do
     begin
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_SISTEMA' ).AsString := ConvierteSistema( sTipo );
          ParamByName( 'TB_NIVEL' ).AsInteger := ConvierteNivelKardex( sTipo );
          ParamByName( 'TB_CODIGO' ).AsString := sTipo;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
          ParamByName( 'TB_NUMERO' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.TurnoMoveExcludedFields(Sender: TObject);
var
   sCodigo: String;
   iDias: Integer;
begin
     MigrarBooleanos( 'TU_FESTIVO', 'TU_FESTIVO' );
     with Source do
     begin
          sCodigo := FieldByName( 'TU_CODIGO' ).AsString;
          iDias := FieldByName( 'TU_DIAS' ).AsInteger;
          if ( sCodigo = TURNO_VACIO ) then
             TablaMigrada.AdditionalRows := 0;
          MigrarFecha( 'TU_RIT_INI', FieldByName( 'TU_RIT_INI' ).AsDateTime );
          MigrarStringKey( 'TU_HOR_1', FieldByName( 'TU_HOR_1' ).AsString );
          MigrarStringKey( 'TU_HOR_2', FieldByName( 'TU_HOR_2' ).AsString );
          MigrarStringKey( 'TU_HOR_3', FieldByName( 'TU_HOR_3' ).AsString );
          MigrarStringKey( 'TU_HOR_4', FieldByName( 'TU_HOR_4' ).AsString );
          MigrarStringKey( 'TU_HOR_5', FieldByName( 'TU_HOR_5' ).AsString );
          MigrarStringKey( 'TU_HOR_6', FieldByName( 'TU_HOR_6' ).AsString );
          MigrarStringKey( 'TU_HOR_7', FieldByName( 'TU_HOR_7' ).AsString );
     end;
     with TargetQuery do
     begin
          ParamByName( 'TU_CODIGO' ).AsString := sCodigo;
          ParamByName( 'TU_DESCRIP' ).AsString := Source.FieldByName( 'TU_DESCRIP' ).AsString;
          ParamByName( 'TU_HORARIO' ).AsInteger := Ord( ConvierteTurno( Source.FieldByName( 'TU_HORARIO' ).AsInteger ) );
          ParamByName( 'TU_TIP_1' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_1' ).AsInteger ) );
          ParamByName( 'TU_TIP_2' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_2' ).AsInteger ) );
          ParamByName( 'TU_TIP_3' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_3' ).AsInteger ) );
          ParamByName( 'TU_TIP_4' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_4' ).AsInteger ) );
          ParamByName( 'TU_TIP_5' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_5' ).AsInteger ) );
          ParamByName( 'TU_TIP_6' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_6' ).AsInteger ) );
          ParamByName( 'TU_TIP_7' ).AsInteger := Ord( ConvierteStatusAusencia( Source.FieldByName( 'TU_TIP_7' ).AsInteger ) );
          ParamByName( 'TU_DIAS' ).AsInteger := iDias;
          ParamByName( 'TU_DOBLES' ).AsFloat := Source.FieldByName( 'TU_DOBLES' ).AsFloat;
          ParamByName( 'TU_DOMINGO' ).AsFloat := Source.FieldByName( 'TU_DOMINGO' ).AsFloat;
          ParamByName( 'TU_JORNADA' ).AsFloat := Source.FieldByName( 'TU_JORNADA' ).AsFloat;
          ParamByName( 'TU_NOMINA' ).AsInteger := Source.FieldByName( 'TU_NOMINA' ).AsInteger;
          ParamByName( 'TU_RIT_PAT' ).AsString := Source.FieldByName( 'TU_RIT_PAT' ).AsString;
          ParamByName( 'TU_TIP_JOR' ).AsInteger := Source.FieldByName( 'TU_TIP_JOR' ).AsInteger;
          { 2.4: Campo nuevo en versi�n de datos 380 }
          ParamByName( 'TU_VACA_DE' ).AsInteger := 0;
          { 2.4: Campo nuevo en versi�n de datos 380 }
          ParamByName( 'TU_VACA_SA' ).AsInteger := 0;
          { 2.4: Campo nuevo en versi�n de datos 380 }
          with ParamByName( 'TU_VACA_HA' ) do
          begin
               if ( iDias = K_DIAS_SEMANA_HABILES ) then
                  AsFloat := K_DIAS_VACACION_HABIL
               else
                   AsFloat := K_DIAS_VACACION_DEF;
          end;
     end;
end;

procedure TdmMigrarDatos.TurnoAdditionalRows(Sender: TObject);
begin
     inherited;
     MigrarFecha( 'TU_RIT_INI', NullDateTime );
     MigrarStringKey( 'TU_HOR_1', K_STRING_NULO );
     MigrarStringKey( 'TU_HOR_2', K_STRING_NULO );
     MigrarStringKey( 'TU_HOR_3', K_STRING_NULO );
     MigrarStringKey( 'TU_HOR_4', K_STRING_NULO );
     MigrarStringKey( 'TU_HOR_5', K_STRING_NULO );
     MigrarStringKey( 'TU_HOR_6', K_STRING_NULO );
     MigrarStringKey( 'TU_HOR_7', K_STRING_NULO );
     with TargetQuery do
     begin
          ParamByName( 'TU_CODIGO' ).AsString := TURNO_VACIO;
          ParamByName( 'TU_DESCRIP' ).AsString := 'Turno Global';
          ParamByName( 'TU_FESTIVO' ).AsString := zBoolToStr( False );
          ParamByName( 'TU_HORARIO' ).AsInteger := Ord( ConvierteTurno( 0 ) );
          ParamByName( 'TU_TIP_1' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_TIP_2' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_TIP_3' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_TIP_4' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_TIP_5' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_TIP_6' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_TIP_7' ).AsInteger := Ord( auSabado );
          ParamByName( 'TU_DIAS' ).AsInteger := 0;
          ParamByName( 'TU_DOBLES' ).AsFloat := 0;
          ParamByName( 'TU_DOMINGO' ).AsFloat := 0;
          ParamByName( 'TU_JORNADA' ).AsFloat := 0;
          ParamByName( 'TU_NOMINA' ).AsInteger := 0;
          ParamByName( 'TU_RIT_PAT' ).AsString := K_STRING_NULO;
          ParamByName( 'TU_TIP_JOR' ).AsInteger := Ord( tjDesconocido );
          { 2.4: Campo nuevo en versi�n de datos 380 }
          ParamByName( 'TU_VACA_DE' ).AsInteger := 0;
          { 2.4: Campo nuevo en versi�n de datos 380 }
          ParamByName( 'TU_VACA_SA' ).AsInteger := 0;
          { 2.4: Campo nuevo en versi�n de datos 380 }
          ParamByName( 'TU_VACA_HA' ).AsFloat := 0;
     end;
end;

procedure TdmMigrarDatos.NominaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     Accept := ( Dataset.FieldByName( 'CB_CODIGO' ).AsString <> K_EMPLEADO_CERO );
end;

procedure TdmMigrarDatos.NominaMoveExcludedFields(Sender: TObject);
const
     K_PAGO_X_FUERA = 'POR FUERA';
var
   sValor: String;
   dPago: TDate;
   iUsrPago: Integer;
   rJornada: TDiasHoras;
begin
     with Source do
     begin
          sValor := FieldByName( 'NO_OBSERVA' ).AsString;
          MigrarStringKey( 'CB_TURNO', FieldByName( 'CB_HORARIO' ).AsString );
          MigrarStringKey( 'CB_PUESTO', FieldByName( 'CB_PUESTO' ).AsString );
          MigrarStringKey( 'CB_CLASIFI', FieldByName( 'CB_CLASIFI' ).AsString );
          MigrarStringKey( 'CB_PATRON', FieldByName( 'CB_PATRON' ).AsString );
          if EsVer260 then
          begin
               dPago := FieldByName( 'NO_FEC_PAG' ).AsDateTime;
               iUsrPago := Str2Integer( FieldByName( 'NO_USR_PAG' ).AsString );
          end
          else
          begin
               dPago := NullDateTime;
               iUsrPago := 0;
          end;
          rJornada := Source.FieldByName( 'NO_JORNADA' ).AsFloat;
          if ZetaCommonTools.PesosIguales( rJornada, 0 ) then
             rJornada := 1; { FIX para evitar que haya errores en los reportes }
     end;
     MigrarCodigoEmpleado;
     MigrarNiveles( 'CB_NIVEL', 'CB_NIVELES' );
     MigrarUsuario( 'US_CODIGO', 'NO_USER' );
     MigrarAsInteger( 'PE_TIPO', 'PE_TIPO' );
     MigrarAsInteger( 'PE_NUMERO', 'PE_NUMERO' );
     MigrarAsInteger( 'NO_FOLIO_1', 'NO_FOLIO_1' );
     MigrarAsInteger( 'NO_FOLIO_2', 'NO_FOLIO_2' );
     MigrarAsInteger( 'NO_FOLIO_3', 'NO_FOLIO_3' );
     MigrarAsInteger( 'NO_FOLIO_4', 'NO_FOLIO_4' );
     MigrarAsInteger( 'NO_FOLIO_5', 'NO_FOLIO_5' );
     MigrarAsInteger( 'NO_STATUS', 'NO_STATUS' );
     MigrarAsInteger( 'NO_USER_RJ', 'NO_USER_RJ' );
     with TargetQuery do
     begin
          ParamByName( 'PE_YEAR' ).AsInteger := Anio;
          ParamByName( 'NO_NETO' ).AsFloat := Source.FieldByName( 'NO_PERCEPC' ).AsFloat -
                                              Source.FieldByName( 'NO_DEDUCCI' ).AsFloat;
          ParamByName( 'NO_LIQUIDA' ).AsInteger := Ord( ConvierteLiquidacionNomina( sValor ) );
          ParamByName( 'NO_FUERA' ).AsString := zBoolToStr( Pos( K_PAGO_X_FUERA, sValor ) > 0 );
          ParamByName( 'CB_ZONA_GE' ).AsString := Source.FieldByName( 'CB_ZONA_GE' ).AsString;
          ParamByName( 'NO_OBSERVA' ).AsString := sValor;
          ParamByName( 'CB_SAL_INT' ).AsFloat := Source.FieldByName( 'CB_SAL_INT' ).AsFloat;
          ParamByName( 'CB_SALARIO' ).AsFloat := Source.FieldByName( 'CB_SALARIO' ).AsFloat;
          ParamByName( 'NO_DIAS' ).AsFloat := Source.FieldByName( 'NO_DIAS' ).AsFloat;
          ParamByName( 'NO_ADICION' ).AsFloat := Source.FieldByName( 'NO_ADICION' ).AsFloat;
          ParamByName( 'NO_DEDUCCI' ).AsFloat := Source.FieldByName( 'NO_DEDUCCI' ).AsFloat;
          ParamByName( 'NO_DES_TRA' ).AsFloat := Source.FieldByName( 'NO_DES_TRA' ).AsFloat;
          ParamByName( 'NO_DIAS_AG' ).AsFloat := Source.FieldByName( 'NO_DIAS_AG' ).AsFloat;
          ParamByName( 'NO_DIAS_VA' ).AsFloat := Source.FieldByName( 'NO_DIAS_VA' ).AsFloat;
          ParamByName( 'NO_DOBLES' ).AsFloat := Source.FieldByName( 'NO_DOBLES' ).AsFloat;
          ParamByName( 'NO_EXTRAS' ).AsFloat := Source.FieldByName( 'NO_EXTRAS' ).AsFloat;
          ParamByName( 'NO_FES_PAG' ).AsFloat := Source.FieldByName( 'NO_FES_PAG' ).AsFloat;
          ParamByName( 'NO_FES_TRA' ).AsFloat := Source.FieldByName( 'NO_FES_TRA' ).AsFloat;
          ParamByName( 'NO_HORA_CG' ).AsFloat := Source.FieldByName( 'NO_HORA_CG' ).AsFloat;
          ParamByName( 'NO_HORA_SG' ).AsFloat := Source.FieldByName( 'NO_HORA_SG' ).AsFloat;
          ParamByName( 'NO_HORA_PD' ).AsFloat := Source.FieldByName( 'NO_DIAS_PD' ).AsFloat;
          ParamByName( 'NO_HORAS' ).AsFloat := Source.FieldByName( 'NO_HORAS' ).AsFloat;
          ParamByName( 'NO_IMP_CAL' ).AsFloat := Source.FieldByName( 'NO_IMP_CAL' ).AsFloat;
          ParamByName( 'NO_JORNADA' ).AsFloat := rJornada;
          ParamByName( 'NO_PER_CAL' ).AsFloat := Source.FieldByName( 'NO_PER_CAL' ).AsFloat;
          ParamByName( 'NO_PER_MEN' ).AsFloat := Source.FieldByName( 'NO_PER_MEN' ).AsFloat;
          ParamByName( 'NO_PER_ISN' ).AsFloat := Source.FieldByName( 'NO_PER_ISN' ).AsFloat;
          ParamByName( 'NO_PERCEPC' ).AsFloat := Source.FieldByName( 'NO_PERCEPC' ).AsFloat;
          ParamByName( 'NO_TARDES' ).AsFloat := Source.FieldByName( 'NO_TARDES' ).AsFloat;
          ParamByName( 'NO_TRIPLES' ).AsFloat := Source.FieldByName( 'NO_TRIPLES' ).AsFloat;
          ParamByName( 'NO_TOT_PRE' ).AsFloat := Source.FieldByName( 'NO_TOT_PRE' ).AsFloat;
          ParamByName( 'NO_VAC_TRA' ).AsFloat := Source.FieldByName( 'NO_VAC_TRA' ).AsFloat;
          ParamByName( 'NO_X_CAL' ).AsFloat := Source.FieldByName( 'NO_X_CAL' ).AsFloat;
          ParamByName( 'NO_X_ISPT' ).AsFloat := Source.FieldByName( 'NO_X_ISPT' ).AsFloat;
          ParamByName( 'NO_X_MENS' ).AsFloat := Source.FieldByName( 'NO_X_MENS' ).AsFloat;
          ParamByName( 'NO_D_TURNO' ).AsInteger := Source.FieldByName( 'NO_D_TURNO' ).AsInteger;
          ParamByName( 'NO_DIAS_AJ' ).AsInteger := Source.FieldByName( 'NO_DIAS_AJ' ).AsInteger;
          ParamByName( 'NO_DIAS_AS' ).AsInteger := Source.FieldByName( 'NO_DIAS_AS' ).AsInteger;
          ParamByName( 'NO_DIAS_CG' ).AsInteger := Source.FieldByName( 'NO_DIAS_CG' ).AsInteger;
          ParamByName( 'NO_DIAS_EM' ).AsInteger := Source.FieldByName( 'NO_DIAS_EM' ).AsInteger;
          ParamByName( 'NO_DIAS_FI' ).AsInteger := Source.FieldByName( 'NO_DIAS_FI' ).AsInteger;
          ParamByName( 'NO_DIAS_FJ' ).AsInteger := Source.FieldByName( 'NO_DIAS_FJ' ).AsInteger;
          ParamByName( 'NO_DIAS_FV' ).AsInteger := Source.FieldByName( 'NO_DIAS_FV' ).AsInteger;
          ParamByName( 'NO_DIAS_IN' ).AsInteger := Source.FieldByName( 'NO_DIAS_IN' ).AsInteger;
          ParamByName( 'NO_DIAS_NT' ).AsInteger := Source.FieldByName( 'NO_DIAS_NT' ).AsInteger;
          ParamByName( 'NO_DIAS_OT' ).AsInteger := Source.FieldByName( 'NO_DIAS_OT' ).AsInteger;
          ParamByName( 'NO_DIAS_RE' ).AsInteger := Source.FieldByName( 'NO_DIAS_RE' ).AsInteger;
          ParamByName( 'NO_DIAS_SG' ).AsInteger := Source.FieldByName( 'NO_DIAS_SG' ).AsInteger;
          ParamByName( 'NO_DIAS_SS' ).AsInteger := Source.FieldByName( 'NO_DIAS_SS' ).AsInteger;
          ParamByName( 'NO_DIAS_SU' ).AsInteger := Source.FieldByName( 'NO_DIAS_SU' ).AsInteger;
          ParamByName( 'NO_FEC_PAG' ).AsDate := dPago;
          ParamByName( 'NO_USR_PAG' ).AsInteger := iUsrPago;
     end;
end;

procedure TdmMigrarDatos.NominaCloseDatasets(Sender: TObject);
const
     K_PAGO_X_FUERA = 'MIGRAR: POR FUERA';
     Q_SUMA_1 = 'update PERIODO P set '+
                'P.PE_NUM_EMP = ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = P.PE_YEAR ) and ( N.PE_TIPO = P.PE_TIPO ) and ( N.PE_NUMERO = P.PE_NUMERO ) ), '+
                'P.PE_TOT_PER = ( select SUM( NO_PERCEPC ) from NOMINA N where ( N.PE_YEAR = P.PE_YEAR ) and ( N.PE_TIPO = P.PE_TIPO ) and ( N.PE_NUMERO = P.PE_NUMERO ) ), '+
                'P.PE_TOT_DED = ( select SUM( NO_DEDUCCI ) from NOMINA N where ( N.PE_YEAR = P.PE_YEAR ) and ( N.PE_TIPO = P.PE_TIPO ) and ( N.PE_NUMERO = P.PE_NUMERO ) ), '+
                'P.PE_TOT_NET = ( select SUM( NO_NETO ) from NOMINA N where ( N.PE_YEAR = P.PE_YEAR ) and ( N.PE_TIPO = P.PE_TIPO ) and ( N.PE_NUMERO = P.PE_NUMERO ) ) '+
                'where ( P.PE_DESCRIP = ''%s'' ) and ( ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = P.PE_YEAR ) and ( N.PE_TIPO = P.PE_TIPO ) and ( N.PE_NUMERO = P.PE_NUMERO ) ) > 0 )';
     Q_SUMA_2 = 'update PERIODO P set '+
                'P.PE_NUM_EMP = 0, '+
                'P.PE_TOT_PER = 0, '+
                'P.PE_TOT_DED = 0, '+
                'P.PE_TOT_NET = 0 '+
                'where ( P.PE_DESCRIP = ''%s'' ) and ( ( select COUNT(*) from NOMINA N where ( N.PE_YEAR = P.PE_YEAR ) and ( N.PE_TIPO = P.PE_TIPO ) and ( N.PE_NUMERO = P.PE_NUMERO ) ) = 0 )';
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, Format( Q_SUMA_1, [ K_PAGO_X_FUERA ] ) );
          PrepareTargetQuery( tqDos, Format( Q_SUMA_2, [ K_PAGO_X_FUERA ] ) );
          with tqUno do
          begin
               ExecSQL;
          end;
          with tqDos do
          begin
               ExecSQL;
          end;
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.OtrasPerMoveExcludedFields(Sender: TObject);
const
     K_PORCENTAJE = 'P';
var
   rMonto, rTasa: Extended;
   eTipo: eTipoOtrasPer;
   sValor: String;
begin
     with Source do
     begin
          sValor := FieldByName( 'TB_LETRA' ).AsString;
          rTasa := 0;
          rMonto := FieldByName( 'TB_NUMERO' ).AsFloat;
          eTipo := toCantidad;
          if ( Copy( sValor, 2, 1 ) = K_PORCENTAJE ) then
          begin
               rTasa := rMonto / 100;
               rMonto := 0;
               eTipo := toPorcentaje;
          end;
     end;
     with TargetQuery do
     begin
          ParamByName( 'TB_NUMERO' ).AsInteger := 0;
          ParamByName( 'TB_TEXTO' ).AsString := K_STRING_NULO;
          ParamByName( 'TB_TIPO' ).AsInteger := Ord( eTipo );
          ParamByName( 'TB_MONTO' ).AsFloat := rMonto;
          ParamByName( 'TB_TASA' ).AsFloat := rTasa;
          ParamByName( 'TB_IMSS' ).AsInteger := Ord( ConvierteOtras( Copy( sValor, 1, 1 ) ) );
          ParamByName( 'TB_ISPT' ).AsInteger := BoolToInt( Copy( sValor, 4, 1 ) = K_GLOBAL_SI );
          ParamByName( 'TB_TOPE' ).AsFloat := StrToReal( Copy( sValor, 5, 5 ) ) / 100;
          ParamByName( 'TB_CODIGO' ).AsString := Source.FieldByName( 'TB_CODIGO' ).AsString;
          ParamByName( 'TB_ELEMENT' ).AsString := Source.FieldByName( 'TB_ELEMENT' ).AsString;
          ParamByName( 'TB_INGLES' ).AsString := Source.FieldByName( 'TB_INGLES' ).AsString;
     end;
end;

procedure TdmMigrarDatos.PeriodoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     Accept := ( Dataset.FieldByName( 'CB_CODIGO' ).AsString = K_EMPLEADO_CERO );
end;

procedure TdmMigrarDatos.PeriodoMoveExcludedFields(Sender: TObject);
var
   sValor: String;
   iValor, iDiasNt: Integer;
   rTotPer, rTotDed: Extended;
   dInicial, dFinal, dPago: TDate;
begin
     with Source do
     begin
          sValor := FieldByName( 'CB_NIVELES' ).AsString;
          iValor := Str2Integer( FieldByName( 'PE_NUMERO' ).AsString );
          iDiasNt := FieldByName( 'NO_DIAS_NT' ).AsInteger;
          rTotPer := FieldByName( 'NO_PERCEPC' ).AsFloat;
          rTotDed := FieldByName( 'NO_DEDUCCI' ).AsFloat;
          if EsVer260 then
          begin
               dInicial := STOD( Copy( sValor, 1, 8 ) );
               dFinal := STOD( Copy( sValor, 9, 8 ) );
               dPago := STOD( Copy( sValor, 17, 8 ) );
          end
          else
          begin
               dInicial := CTOD( Copy( sValor, 1, 8 ) );
               dFinal := CTOD( Copy( sValor, 9, 8 ) );
               dPago := CTOD( Copy( sValor, 17, 8 ) );
          end;
     end;
     MigrarAsInteger( 'PE_TIPO', 'PE_TIPO' );
     MigrarAsInteger( 'PE_STATUS', 'NO_STATUS' );
     MigrarUsuario( 'US_CODIGO', 'NO_USER' );
     MigrarFecha( 'PE_FEC_INI', dInicial );
     MigrarFecha( 'PE_FEC_FIN', dFinal );
{$ifdef QUINCENALES}
     MigrarFecha( 'PE_ASI_INI', dInicial );
     MigrarFecha( 'PE_ASI_FIN', dFinal );
{$endif}
     MigrarFecha( 'PE_FEC_PAG', dPago );
     MigrarFecha( 'PE_FEC_MOD', Source.FieldByName( 'NO_FECHA' ).AsDateTime );
     with TargetQuery do
     begin
          ParamByName( 'PE_YEAR' ).AsInteger := Anio;
          ParamByName( 'PE_NUMERO' ).AsInteger := iValor;
          ParamByName( 'PE_USO' ).AsInteger := Ord( ConvierteUsoPeriodo( iValor ) );
          ParamByName( 'PE_INC_BAJ' ).AsString := zBoolToStr( iValor >= 600 );
          ParamByName( 'PE_SOLO_EX' ).AsString := zBoolToStr( iValor >= 200 );
          ParamByName( 'PE_DIA_MES' ).AsInteger := 0;
          ParamByName( 'PE_POS_MES' ).AsInteger := 0;
          ParamByName( 'PE_PER_MES' ).AsFloat := 0;
          ParamByName( 'PE_PER_TOT' ).AsFloat := 0;
          ParamByName( 'PE_AHORRO' ).AsString := zBoolToStr( ( iDiasNt = 1 ) or ( iDiasNt = 2 ) );
          ParamByName( 'PE_PRESTAM' ).AsString := zBoolToStr( ( iDiasNt = 1 ) or ( iDiasNt = 3 ) );
          ParamByName( 'PE_TOT_PER' ).AsFloat := rTotPer;
          ParamByName( 'PE_TOT_DED' ).AsFloat := rTotDed;
          ParamByName( 'PE_TOT_NET' ).AsFloat := ( rTotPer - rTotDed );
          ParamByName( 'PE_DESCRIP' ).AsString := Source.FieldByName( 'NO_OBSERVA' ).AsString;
          ParamByName( 'PE_MES' ).AsInteger := Source.FieldByName( 'NO_DIAS_RE' ).AsInteger;
          ParamByName( 'PE_DIAS' ).AsInteger := Source.FieldByName( 'NO_DIAS_FI' ).AsInteger;
          ParamByName( 'PE_DIAS_AC' ).AsInteger := Source.FieldByName( 'NO_DIAS_FJ' ).AsInteger;
          ParamByName( 'PE_NUM_EMP' ).AsInteger := Source.FieldByName( 'CB_SALARIO' ).AsInteger;
     end;
end;

procedure TdmMigrarDatos.PeriodoCloseDatasets(Sender: TObject);
var
   aDiasMes, aPeriodosMes: array [ 1..12 ] of Integer;
   iMes, iNumero, iTotPeriodos, iDiasAC, iPosMes: Integer;
   eTipo: eTipoPeriodo;
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select PE_NUMERO, PE_MES, PE_DIAS from PERIODO where '+
                                     '( PE_YEAR = :Year ) and '+
                                     '( PE_TIPO = :Tipo ) and '+
                                     '( PE_MES >= 1 ) and '+
                                     '( PE_MES <= 12 ) and '+
                                     '( PE_NUMERO < 200 ) '+
                                     'order by PE_MES, PE_NUMERO' );
          PrepareTargetQuery( tqDos, 'update PERIODO set '+
                                     'PE_DIAS_AC = :PE_DIAS_AC, '+
                                     'PE_DIA_MES = :PE_DIA_MES, '+
                                     'PE_POS_MES = :PE_POS_MES, '+
                                     'PE_PER_MES = :PE_PER_MES, '+
                                     'PE_PER_TOT = :PE_PER_TOT where '+
                                     '( PE_YEAR = :Year ) and '+
                                     '( PE_TIPO = :Tipo ) and ' +
                                     '( PE_NUMERO = :Numero )' );
     end;
     for eTipo := Low( eTipoPeriodo ) to High( eTipoPeriodo ) do
     begin
          { Inicializar en ceros }
          iTotPeriodos := 0;
          for iMes := 1 to 12 do
          begin
               aDiasMes[ iMes ] := 0;
               aPeriodosMes[ iMes ] := 0;
          end;
          { Contar totales mensuales y anual }
          with tqUno do
          begin
               Active := False;
               ParamByName( 'Year' ).AsInteger := Anio;
               ParamByName( 'Tipo' ).AsInteger := Ord( eTipo );
               Active := True;
               if not IsEmpty then
               begin
                    First;
                    while not Eof do
                    begin
                         iMes := FieldByName( 'PE_MES' ).AsInteger;
                         aDiasMes[ iMes ] := aDiasMes[ iMes ] + FieldByName( 'PE_DIAS' ).AsInteger;
                         Inc( aPeriodosMes[ iMes ] );
                         Inc( iTotPeriodos );
                         Next;
                    end;
                    { Calcular y asignar acumulados y posiciones }
                    with tqDos do
                    begin
                         ParamByName( 'Year' ).AsInteger := Anio;
                         ParamByName( 'Tipo' ).AsInteger := Ord( eTipo );
                    end;
                    iMes := 0;
                    iDiasAC := 0;
                    iPosMes := 0;
                    EmpezarTransaccion;
                    try
                       First;
                       while not Eof do
                       begin
                            iNumero := FieldByName( 'PE_NUMERO' ).AsInteger;
                            if ( FieldByName( 'PE_MES' ).AsInteger <> iMes ) then
                            begin
                                 iMes := FieldByName( 'PE_MES' ).AsInteger;
                                 iDiasAC := 0;
                                 iPosMes := 0;
                            end;
                            iDiasAC := iDiasAC + FieldByName( 'PE_DIAS' ).AsInteger;
                            iPosMes := iPosMes + 1;
                            with tqDos do
                            begin
                                 ParamByName( 'PE_DIAS_AC' ).AsInteger := iDiasAC;
                                 ParamByName( 'PE_DIA_MES' ).AsInteger := aDiasMes[ iMes ];
                                 ParamByName( 'PE_POS_MES' ).AsInteger := iPosMes;
                                 ParamByName( 'PE_PER_MES' ).AsInteger := aPeriodosMes[ iMes ];
                                 ParamByName( 'PE_PER_TOT' ).AsInteger := iTotPeriodos;
                                 ParamByName( 'Numero' ).AsInteger := iNumero;
                                 ExecSQL;
                            end;
                            Next;
                       end;
                       TerminarTransaccion( True );
                    except
                          on Error: Exception do
                          begin
                               TerminarTransaccion( False );
                               TablaMigrada.HandleException( 'Rec�lculo de D�as Fall� Para Per�odos Tipo ' +
                                                              ZetaCommonLists.ObtieneElemento( lfTipoPeriodo, Ord( eTipo ) ),
                                                              Error );
                          end;
                    end;
               end;
               Active := False;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.PermisoFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     Accept := ( Dataset.FieldByName( 'IN_CLASE' ).AsString = K_ES_PERMISO )
end;

procedure TdmMigrarDatos.PermisoMoveExcludedFields(Sender: TObject);
begin
     MigrarCodigoEmpleado;
     with Source do
     begin
          MigrarFecha( 'PM_FEC_INI', FieldByName( 'IN_FEC_INI' ).AsDateTime );
          MigrarFecha( 'PM_FEC_FIN', FieldByName( 'IN_FEC_FIN' ).AsDateTime );
          MigrarFecha( 'PM_CAPTURA', FieldByName( 'CB_FEC_CAP' ).AsDateTime );
          MigrarStringKey( 'PM_TIPO', FieldByName( 'IN_TIPO' ).AsString );
     end;
     MigrarUsuario( 'US_CODIGO', 'CB_USER' );
     with TargetQuery do
     begin
          ParamByName( 'PM_CLASIFI' ).AsInteger := Ord( ConvierteClasePermiso( Source.FieldByName( 'IN_CARGO' ).AsString ) );
          ParamByName( 'PM_COMENTA' ).AsString := Limita( Source.FieldByName( 'IN_COMENTA' ).AsString, 30 );
          ParamByName( 'PM_DIAS' ).AsInteger := Source.FieldByName( 'IN_DIAS' ).AsInteger;
          ParamByName( 'PM_NUMERO' ).AsString := Source.FieldByName( 'IN_NUMERO' ).AsString;
     end;
end;

procedure TdmMigrarDatos.PrestaciMoveExcludedFields(Sender: TObject);
var
   sValor, sValue, sPago7, sPrima7: String;
   rPrima: Extended;

procedure BuscaSSocial( const sCodigo: String; var sPago7, sPrima7: String; var rPrima: Extended );
begin
     with tqUno do
     begin
          Active := False;
          Params[ 0 ].AsString := sCodigo;
          Active := True;
          if not IsEmpty then
          begin
               rPrima := FieldByName( 'TB_PRIMADO' ).AsFloat;
               sPago7 := FieldByName( 'TB_PAGO_7' ).AsString;
               sPrima7 := FieldByName( 'TB_PRIMA_7' ).AsString;
          end;
     end;
end;

begin
     with Source do
     begin
          sValor := Copy( FieldByName( 'TB_TABLA' ).AsString, 2, 1 );
          sValue := Source.FieldByName( 'TB_ELEMENT' ).AsString;
     end;
     MigrarAsInteger( 'PT_YEAR', 'TB_CODIGO' );
     rPrima := 0;
     sPago7 := K_STRING_NULO;
     sPrima7 := K_STRING_NULO;
     BuscaSSocial( sValor, sPago7, sPrima7, rPrima );
     with TargetQuery do
     begin
          ParamByName( 'TB_CODIGO' ).AsString := sValor;
          ParamByName( 'PT_PRIMAVA' ).AsFloat := ( StrToReal( Source.FieldByName( 'TB_LETRA' ).AsString ) / 10 );
          ParamByName( 'PT_PRIMADO' ).AsFloat := rPrima;
          ParamByName( 'PT_PAGO_7' ).AsString := sPago7;
          ParamByName( 'PT_PRIMA_7' ).AsString := sPrima7;
          ParamByName( 'PT_DIAS_VA' ).AsFloat := Source.FieldByName( 'TB_NUMERO' ).AsFloat;
          ParamByName( 'PT_DIAS_AG' ).AsFloat := StrToReal( Copy( sValue, 10, 5 ) );
          ParamByName( 'PT_DIAS_AD' ).AsFloat := StrToReal( Copy( sValue, 15, 4 ) );
          ParamByName( 'PT_FACTOR' ).AsFloat := StrToReal( Copy( sValue, 1, 6 ) );
     end;
end;

procedure TdmMigrarDatos.PrestaciOpenDatasets(Sender: TObject);
begin
     TablaMigrada.PrepareTargetQuery( tqUno, 'select TB_PRIMADO, TB_PAGO_7, TB_PRIMA_7 from SSOCIAL where (TB_CODIGO = :TB_CODIGO)' );
end;

procedure TdmMigrarDatos.PrestaciCloseDatasets(Sender: TObject);
begin
     TablaMigrada.CloseQuery( tqUno );
end;

procedure TdmMigrarDatos.VacacionMoveExcludedFields(Sender: TObject);
var
   sCargo: String;
   iYear, iNumero: Integer;
   rValor, rTotal: Extended;
begin
     with Source do
     begin
          sCargo := FieldByName( 'IN_CARGO' ).AsString;
          MigrarFecha( 'VA_FEC_INI', FieldByName( 'IN_FEC_INI' ).AsDateTime );
          MigrarFecha( 'VA_FEC_FIN', FieldByName( 'IN_FEC_FIN' ).AsDateTime );
          MigrarFecha( 'VA_CAPTURA', FieldByName( 'CB_FEC_CAP' ).AsDateTime );
          MigrarStringKey( 'CB_TABLASS', FieldByName( 'CB_TABLASS' ).AsString );
          if EsVer260 then
          begin
               iYear := Str2Integer( Copy( sCargo, 1, 4 ) );
               iNumero := Str2Integer( Copy( sCargo, 5, 3 ) );
               rValor := FieldByName( 'IN_TASA_IP' ).AsFloat;
          end
          else
          begin
               iYear := Str2Integer( Copy( sCargo, 1, 1 ) ) + 1990;
               iNumero := Str2Integer( Copy( sCargo, 2, 3 ) );
               rValor := StrToReal( Copy( sCargo, 5, 5 ) );
          end;
     end;
     MigrarCodigoEmpleado;
     MigrarUsuario( 'US_CODIGO', 'CB_USER' );
     with TargetQuery do
     begin
          ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvVacaciones );
          ParamByName( 'VA_PAGO' ).AsFloat := rValor;
          ParamByName( 'VA_S_PAGO' ).AsInteger := 0;
          ParamByName( 'VA_D_PAGO' ).AsInteger := 0;
          ParamByName( 'VA_D_GOZO' ).AsInteger := 0;
          ParamByName( 'VA_GOZO' ).AsFloat := Source.FieldByName( 'IN_DIAS' ).AsFloat;
          ParamByName( 'VA_NOMYEAR' ).AsInteger := iYear;
          ParamByName( 'VA_NOMTIPO' ).AsInteger := Ord( tpSemanal );
          ParamByName( 'VA_NOMNUME' ).AsInteger := iNumero;
          ParamByName( 'VA_YEAR' ).AsInteger := 0;
          ParamByName( 'VA_GLOBAL' ).AsString := K_GLOBAL_NO;
          ParamByName( 'VA_TASA_PR' ).AsFloat := Source.FieldByName( 'VA_TASA_PR' ).AsFloat;
          rValor := Source.FieldByName( 'VA_MONTO' ).AsFloat;
          rTotal := rValor;
          ParamByName( 'VA_MONTO' ).AsFloat := rValor;
          rValor := Source.FieldByName( 'VA_SEVEN' ).AsFloat;
          rTotal := rTotal + rValor;
          ParamByName( 'VA_SEVEN' ).AsFloat := rValor;
          rValor := Source.FieldByName( 'VA_PRIMA' ).AsFloat;
          rTotal := rTotal + rValor;
          ParamByName( 'VA_PRIMA' ).AsFloat := rValor;
          rValor := Source.FieldByName( 'VA_OTROS_M' ).AsFloat;
          rTotal := rTotal + rValor;
          ParamByName( 'VA_OTROS' ).AsFloat := rValor;
          ParamByName( 'VA_TOTAL' ).AsFloat := rTotal;
          ParamByName( 'VA_PERIODO' ).AsString := Source.FieldByName( 'VA_TRA_DEL' ).AsString +
                                                  ' A ' +
                                                  Source.FieldByName( 'VA_TRA_AL' ).AsString;
          ParamByName( 'VA_COMENTA' ).AsString := Limita( Source.FieldByName( 'IN_COMENTA' ).AsString, K_ANCHO_TITULO );
          ParamByName( 'CB_SALARIO' ).AsFloat := Source.FieldByName( 'CB_SALARIO' ).AsFloat;
     end;
end;

procedure TdmMigrarDatos.VacacionCloseDatasets(Sender: TObject);
var
   rSaldoPago, rSumaGozo, rDerecho: Extended;
   rDerechoGozo, rDerechoPago, rTotGozo, rTotPago: Currency;
   dPrimero, dCierre, dDerecho, dUltima, dInicio, dAntiguedad: TDate;
   iEmpleado: TNumEmp;
   eTipo: eTipoVacacion;
   lAntig: Boolean;
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select SUM( V.VA_PAGO ) as VA_PAGO, ' +
                                     'SUM( V.VA_GOZO ) as VA_GOZO, ' +
                                     'MIN( V.VA_FEC_INI ) as VA_FEC_INI, ' +
                                     'V.CB_CODIGO as CB_CODIGO, ' +
                                     'C.CB_V_PAGO as CB_V_PAGO, ' +
                                     'C.CB_V_GOZO as CB_V_GOZO, ' +
                                     'C.CB_DER_PAG as CB_DER_PAG, ' +
                                     'C.CB_DER_FEC as CB_DER_FEC ' +
                                     'from VACACION V, COLABORA C where ' +
                                     '( V.VA_TIPO = 1 ) and ' +
                                     '( C.CB_CODIGO = V.CB_CODIGO ) and ' +
                                     '( V.VA_FEC_INI >= ( select C2.CB_FEC_ANT from COLABORA C2 where ( C2.CB_CODIGO = V.CB_CODIGO ) ) ) ' +
                                     'group by V.CB_CODIGO, C.CB_V_PAGO, C.CB_V_GOZO, C.CB_DER_PAG, C.CB_DER_FEC' );
          PrepareTargetQuery( tqDos, 'insert into VACACION ' +
                                     '( CB_CODIGO, VA_FEC_INI, VA_TIPO, VA_COMENTA, VA_D_PAGO, VA_D_GOZO ) values ' +
                                     '(:CB_CODIGO,:VA_FEC_INI,:VA_TIPO,:VA_COMENTA,:VA_D_PAGO,:VA_D_GOZO )' );
          PrepareTargetQuery( tqTres, 'insert into VACACION ' +
                                      '( CB_CODIGO, VA_FEC_INI, VA_FEC_FIN, VA_TIPO, VA_COMENTA, VA_PAGO, VA_GOZO ) values ' +
                                      '(:CB_CODIGO,:VA_FEC_INI,:VA_FEC_FIN,:VA_TIPO,:VA_COMENTA,:VA_PAGO,:VA_GOZO )' );
          PrepareTargetQuery( tqCuatro, 'update COLABORA set CB_V_PAGO = :CB_V_PAGO ' +
                                        'where ( CB_CODIGO = :CB_CODIGO )' );
          PrepareTargetQuery( tqCinco, 'update COLABORA set CB_V_GOZO = :CB_V_GOZO ' +
                                       'where ( CB_CODIGO = :CB_CODIGO )' );
          PrepareTargetQuery( tqSeis, 'select V.VA_FEC_INI from VACACION V where ' +
                                      '( V.CB_CODIGO = :CB_CODIGO ) and ' +
                                      '( V.VA_TIPO = 1 ) and ' +
                                      '( V.VA_FEC_INI >= ( select C.CB_FEC_ANT from COLABORA C where ( C.CB_CODIGO = V.CB_CODIGO ) ) ) ' +
                                      'order by V.VA_FEC_INI' );
     end;
     with tqUno do
     begin
          Active := True;
          while not Eof do
          begin
               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               EmpezarTransaccion;
               try
                  dPrimero := FieldByName( 'VA_FEC_INI' ).AsDateTime;
	              rSaldoPago := ( FieldByName( 'CB_V_PAGO' ).AsFloat - FieldByName( 'VA_PAGO' ).AsFloat );
                  rSumaGozo := ( FieldByName( 'CB_V_GOZO' ).AsFloat - FieldByName( 'VA_GOZO' ).AsFloat );
	              rDerecho := FieldByName( 'CB_DER_PAG' ).AsFloat;
                  dDerecho := FieldByName( 'CB_DER_FEC' ).AsDateTime;
	              if ( rDerecho > 0 ) then	// Agrega registro de cierre //
	              begin
                       TablaMigrada.ClearParameters( tqSeis );
                       with tqSeis do
                       begin
                            Active := False;
		                    ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                            Active := True;
                            if IsEmpty then
                               dCierre := NullDateTime
                            else
                                dCierre := ( Fields[ 0 ].AsDateTime - 1 );
                            Active := False;
                       end;
                       if ( dCierre > NullDateTime ) then  { Se hacen 2 cierres: uno un dia antes de la primera vacacion }
                       begin                               { con los valores; otro en la fecha deseado pero con valores en 0 };
                            TablaMigrada.ClearParameters( tqDos );
                            if ( dDerecho < dCierre ) then { Ajusta el cierre al menor de la fecha de aniversario }
                               dCierre := dDerecho;        { y la fecha de la primera vacaci�n - 1 }
                            MigrarFechaQuery( tqDos, 'VA_FEC_INI', dCierre );
                            with tqDos do
                            begin
		                         ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
		                         ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvCierre );
		                         ParamByName( 'VA_COMENTA' ).AsString := K_MARCA;
		                         ParamByName( 'VA_D_PAGO' ).AsFloat := rDerecho;
		                         ParamByName( 'VA_D_GOZO' ).AsFloat := rDerecho;
                                 ExecSQL;
                            end;
                            if ( dDerecho <> dCierre ) then
                            begin
                                 TablaMigrada.ClearParameters( tqDos );
		                         MigrarFechaQuery( tqDos, 'VA_FEC_INI', dDerecho );
                                 with tqDos do
                                 begin
                                      ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                      ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvCierre );
                                      ParamByName( 'VA_COMENTA' ).AsString := K_MARCA;
                                      ParamByName( 'VA_D_PAGO' ).AsFloat := 0;
                                      ParamByName( 'VA_D_GOZO' ).AsFloat := 0;
                                      ExecSQL;
                                 end;
                            end;
                       end
                       else
                       begin    { Solo un movimiento de cierre }
                            TablaMigrada.ClearParameters( tqDos );
		                    MigrarFechaQuery( tqDos, 'VA_FEC_INI', dDerecho );
                            with tqDos do
                            begin
                                 ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                                 ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvCierre );
                                 ParamByName( 'VA_COMENTA' ).AsString := K_MARCA;
                                 ParamByName( 'VA_D_PAGO' ).AsFloat := rDerecho;
                                 ParamByName( 'VA_D_GOZO' ).AsFloat := rDerecho;
                                 ExecSQL;
                            end;
                       end;
                  end;
                  if ( rSaldoPago > 0.5 ) or ( rSumaGozo > 0.5 ) then
	              begin
                       TablaMigrada.ClearParameters( tqTres );
		               MigrarFechaQuery( tqTres, 'VA_FEC_INI', dPrimero - 1 );
		               MigrarFechaQuery( tqTres, 'VA_FEC_FIN', dPrimero );
                       with tqTres do
                       begin
                            ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
                            ParamByName( 'VA_TIPO' ).AsInteger := Ord( tvVacaciones );
                            ParamByName( 'VA_COMENTA' ).AsString := K_MARCA;
                            ParamByName( 'VA_PAGO' ).AsFloat := rMax( rSaldoPago, 0 );
                            ParamByName( 'VA_GOZO' ).AsFloat := rMax( rSumaGozo, 0 );
                            ExecSQL;
                       end;
                  end;
                  if ( rSaldoPago < 0 ) then
                  begin
                       rSaldoPago := FieldByName( 'VA_PAGO' ).AsFloat;
                       with tqCuatro do
                       begin
		                    ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
		                    ParamByName( 'CB_V_PAGO' ).AsFloat := rSaldoPago;
                            ExecSQL;
                       end;
                  end;
                  if ( rSumaGozo < 0 ) then
                  begin
                       rSumaGozo := FieldByName( 'VA_GOZO' ).AsFloat;
                       with tqCinco do
                       begin
		                    ParamByName( 'CB_CODIGO' ).AsInteger := iEmpleado;
		                    ParamByName( 'CB_V_GOZO' ).AsFloat := rSumaGozo;
                            ExecSQL;
                       end;
                  end;
                  TerminarTransaccion( True );
               except
                     on Error: Exception do
                     begin
                          TerminarTransaccion( False );
                          TablaMigrada.HandleException( 'Cierre De Vacaciones Fall� Para Empleado ' +
                                                        IntToStr( iEmpleado ) + CR_LF ,
                                                        Error );
                     end;
               end;
               Next;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqSeis );
          CloseQuery( tqCinco );
          CloseQuery( tqCuatro );
          CloseQuery( tqTres );
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
     { Cierre de Vacaciones }
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select CB_CODIGO, CB_FEC_ANT from COLABORA where ( CB_ACTIVO = ''S'' ) order by CB_CODIGO' );
          PrepareTargetQuery( tqDos, 'select VA_FEC_INI, VA_TIPO, VA_GOZO, VA_D_GOZO, VA_PAGO, VA_D_PAGO ' +
                                     'from VACACION where ( CB_CODIGO = :Empleado ) and ( VA_FEC_INI >= :Fecha ) ' +
                                     'order by VA_FEC_INI, VA_TIPO' );
          PrepareTargetQuery( tqTres, 'update VACACION set VA_S_GOZO = :SaldoGozo, VA_S_PAGO = :SaldoPago, VA_YEAR = :Year '+
                                      'where ( CB_CODIGO = :Empleado ) and ( VA_FEC_INI = :Fecha ) and ( VA_TIPO = :Tipo )' );
          PrepareTargetQuery( tqCuatro, 'execute procedure UPDATE_EMP_VACA( :Empleado, :Ultima, :Cierre, :TotGozo, :TotPago, :TotPrima, :DerGozo, :DerPago, :DerPrima )' );
     end;
     with tqUno do
     begin
          Active := True;
          while not Eof do
          begin
               iEmpleado := FieldByName( 'CB_CODIGO' ).AsInteger;
               dAntiguedad := FieldByName( 'CB_FEC_ANT' ).AsDateTime;
               dCierre := 0;
               dUltima := 0;
               rDerechoGozo := 0;
               rTotGozo := 0;
               rDerechoPago := 0;
               rTotPago := 0;
               with tqTres do
               begin
                    ParamByName( 'Empleado' ).AsInteger := iEmpleado;
               end;
               with tqDos do
               begin
                    Active := False;
                    ParamByName( 'Empleado' ).AsInteger := iEmpleado;
                    ParamByName( 'Fecha' ).AsDate := dAntiguedad;
                    Active := True;
                    while not Eof do
                    begin
                         eTipo  := eTipoVacacion( FieldByName( 'VA_TIPO' ).AsInteger );
                         dInicio := FieldByName( 'VA_FEC_INI' ).AsDateTime;
                         if ( eTipo = tvCierre ) then
                         begin
                              rDerechoGozo := rDerechoGozo + FieldByName( 'VA_D_GOZO' ).AsFloat;
                              rDerechoPago := rDerechoPago + FieldByName( 'VA_D_PAGO' ).AsFloat;
                              dCierre := dInicio;
                         end
                         else
                         begin
                              rTotGozo := rTotGozo + FieldByName( 'VA_GOZO' ).AsFloat;
                              rTotPago := rTotPago + FieldByName( 'VA_PAGO' ).AsFloat;
                              dUltima := dInicio;
                         end;
                         { Actualiza Saldo Corriente y Year de Vacaciones }
                         with tqTres do
                         begin
                              ParamByName( 'SaldoGozo' ).AsFloat := ( rDerechoGozo - rTotGozo );
                              ParamByName( 'SaldoPago' ).AsFloat := ( rDerechoPago - rTotPago );
                              ParamByName( 'Year' ).AsInteger := Trunc( ZetaTressCFGTools.YearsCompletos( dAntiguedad, dInicio, lAntig ) );
                              ParamByName( 'Tipo' ).AsInteger := Ord( eTipo );
                              ParamByName( 'Fecha' ).AsDate := dInicio;
                              ExecSQL;
                         end;
                         Next;
                    end;
                    { Actualiza Colabora }
                    with tqCuatro do
                    begin
                         ParamByName( 'Empleado' ).AsInteger := iEmpleado;
                         ParamByName( 'Ultima' ).AsDate := dUltima;
                         ParamByName( 'Cierre' ).AsDate := dCierre;
                         ParamByName( 'TotGozo' ).AsFloat := rTotGozo;
                         ParamByName( 'TotPago' ).AsFloat := rTotPago;
                         ParamByName( 'TotPrima' ).AsFloat := 0;
                         ParamByName( 'DerGozo' ).AsFloat := rDerechoGozo;
                         ParamByName( 'DerPago' ).AsFloat := rDerechoPago;
                         ParamByName( 'DerPrima' ).AsFloat := 0;
                         ExecSQL;
                    end;
                    Active := False;
               end;
               Next;
          end;
     end;
     with TablaMigrada do
     begin
          CloseQuery( tqCuatro );
          CloseQuery( tqTres );
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.Kar_FijaFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
     inherited;
     with DataSet do
     begin
          with FieldByName( 'CB_TIPO' ) do
          begin
               Accept := ( AsString = K_T_ALTA ) or ( AsString = K_T_CAMBIO );
          end;
          Accept := Accept and
                    StrLleno( FieldByName( 'CB_OTRAS_' + IntToStr( TablaMigrada.Counter ) ).AsString ) and
                    PuedeMigrarKardex( FieldByName( 'CB_FECHA' ).AsDateTime, LeeCodigoEmpleado );
     end;
end;

procedure TdmMigrarDatos.Kar_FijaMoveExcludedFields(Sender: TObject);
type
    TDatosFija = record
      Monto: TPesos;
      Gravado: TPesos;
      IMSS: eIMSSOtrasPer;
    end;
var
   iFolio: Integer;
   sCodigo, sTipo: String;
   dFecha: TDate;
   rSalario, rPreInt: TPesos;
   iEmpleado: TNumEmp;

function GetDatosFija( const dFecha: TDate; sCodigo: String; rSalario, rPreInt: TPesos ): TDatosFija;
begin
     with tqUno do
     begin
          Active := False;
          Params[ 0 ].AsString := sCodigo;
          Active := True;
          if IsEmpty then
          begin
               with Result do
               begin
                    Monto := 0;
                    IMSS := ioExento;
               end;
          end
          else
          begin
               with Result do
               begin
	            if ( eTipoOtrasPer( FieldByName( 'TB_TIPO' ).AsInteger ) = toCantidad ) then
                       Monto := FieldByName( 'TB_MONTO' ).AsFloat
                    else
                        Monto := rSalario * FieldByName( 'TB_TASA' ).AsFloat / 100;
                    IMSS := eIMSSOtrasPer( FieldByName( 'TB_IMSS' ).AsInteger );
               end;
          end;
     end;
     with Result do
     begin
          case IMSS of
               ioExento: Gravado := 0;
	       ioGravado: Gravado := Monto;
	       ioAsistencia: Gravado := rMax( ( Monto - rPreInt * 0.10 ), 0 );
	       ioDespensa:
               begin
                    with tqDos do
                    begin
                         Active := False;
                         Params[ 0 ].AsDateTime := dFecha;
                         Active := True;
                         if IsEmpty then
		            Gravado := 0
                         else
		             Gravado := rMax( ( Monto - Fields[ 0 ].AsFloat * 0.40 ), 0 );
                    end;
               end;
          end;
     end;
end;

begin
     inherited;
     iFolio := TablaMigrada.Counter;
     with Source do
     begin
          sCodigo := FieldByName( 'CB_OTRAS_' + IntToStr( iFolio ) ).AsString;
          sTipo := Trim( FieldByName( 'CB_TIPO' ).AsString );
          dFecha := FieldByName( 'CB_FECHA' ).AsDateTime;
          rSalario := FieldByName( 'CB_SALARIO' ).AsFloat;
          rPreInt := FieldByName( 'CB_PRE_INT' ).AsFloat;
     end;
     iEmpleado := LeeCodigoEmpleado;
     MigrarEmpleado( iEmpleado );
     MigrarFechaKardex( 'CB_FECHA', dFecha );
     MigrarStringKey( 'KF_CODIGO', sCodigo );
     MigrarStringKey( 'CB_TIPO', sTipo );
     with TargetQuery do
     begin
          ParamByName( 'KF_FOLIO' ).AsInteger := iFolio;
          with GetDatosFija( dFecha, sCodigo, rSalario, rPreInt ) do
          begin
               ParamByName( 'KF_MONTO' ).AsFloat := Monto;
               ParamByName( 'KF_GRAVADO' ).AsFloat := Gravado;
               ParamByName( 'KF_IMSS' ).AsInteger := Ord( IMSS );
          end;
     end;
end;

procedure TdmMigrarDatos.Kar_FijaOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'select TB_TIPO, TB_MONTO, TB_TASA, TB_IMSS from OTRASPER where ( TB_CODIGO = :TB_CODIGO )' );
          PrepareTargetQuery( tqDos,'select S.SM_ZONA_A from SAL_MIN S where ( S.SM_FEC_INI = ( select MAX( S2.SM_FEC_INI ) from SAL_MIN S2 where ( S2.SM_FEC_INI <= :SM_FEC_INI ) ) )' );
     end;
end;

procedure TdmMigrarDatos.Kar_FijaCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
          CloseQuery( tqDos );
     end;
end;

procedure TdmMigrarDatos.CafReglaMoveExcludedFields(Sender: TObject);
var
   iCodigo: Integer;
begin
     inherited;
     iCodigo := CodigoNumerico( Source.FieldByName( 'CL_CODIGO' ).AsString );
     with TargetQuery do
     begin
          ParamByName( 'CL_CODIGO' ).AsInteger := iCodigo;
          ParamByName( 'CL_LETRERO' ).AsString := Source.FieldByName( 'CL_LETRERO' ).AsString;
          ParamByName( 'CL_ACTIVO' ).AsString := zBoolToStr( Source.FieldByName( 'CL_ACTIVO' ).AsBoolean );
          ParamByName( 'CL_QUERY' ).AsString := Source.FieldByName( 'CL_QUERY' ).AsString;
          ParamByName( 'CL_TIPOS' ).AsString := Source.FieldByName( 'CL_TIPOS' ).AsString;
          ParamByName( 'CL_LIMITE' ).AsInteger := Source.FieldByName( 'CL_LIMITE' ).AsInteger;
          ParamByName( 'CL_FILTRO' ).AsString := ChecaFormula( Source.FieldByName( 'CL_FILTRO' ).AsString, Format( 'Regla %d: Filtro ( CAFREGLA.CL_FILTRO )', [ iCodigo ] ), True );
          ParamByName( 'CL_TOTAL' ).AsString := ChecaFormula( Source.FieldByName( 'CL_TOTAL' ).AsString, Format( 'Regla %d: Comidas Consumidas ( CAFREGLA.CL_TOTAL )', [ iCodigo ] ), False );
          ParamByName( 'CL_EXTRAS' ).AsString := ChecaFormula( Source.FieldByName( 'CL_EXTRAS' ).AsString, Format( 'Regla %d: Comidas Extras ( CAFREGLA.CL_EXTRAS )', [ iCodigo ] ), False );
     end;
end;

procedure TdmMigrarDatos.CafComeMoveExcludedFields(Sender: TObject);
begin
     inherited;
     with Source do
     begin
          MigrarFecha( 'CF_FECHA', FieldByName( 'CF_FECHA' ).AsDateTime );
     end;
     MigrarCodigoEmpleado;
     MigrarUsuario( 'US_CODIGO', 'CF_USER' );
     with TargetQuery do
     begin
          ParamByName( 'CF_HORA' ).AsString := Source.FieldByName( 'CF_HORA' ).AsString;
          ParamByName( 'CF_TIPO' ).AsString := Source.FieldByName( 'CF_TIPO' ).AsString;
          ParamByName( 'CF_COMIDAS' ).AsInteger := Source.FieldByName( 'CF_COMIDAS' ).AsInteger;
          ParamByName( 'CF_EXTRAS' ).AsInteger := Source.FieldByName( 'CF_EXTRAS' ).AsInteger;
          ParamByName( 'CF_RELOJ' ).AsString := Source.FieldByName( 'CF_RELOJ' ).AsString;
          ParamByName( 'CL_CODIGO' ).AsInteger := CodigoNumerico( Source.FieldByName( 'CF_REGLA' ).AsString );
          ParamByName( 'CF_REG_EXT' ).AsString := zBoolToStr( Source.FieldByName( 'CF_REG_EXT' ).AsBoolean );
     end;
end;

{ *************** Reporteadores ******************* }

function TdmMigrarDatos.AnalizaFormula( sFormula: String; const EmpleadoSTR: Boolean; var iAncho : integer ): String;
var
   lHP: Boolean;

  procedure TransformaAncho;
  const
       aCatalogos: array[ 1..{$ifdef ACS}31{$else}28{$endif} ] of String = ( 'MOT_BAJA',
                                                'ESTUDIOS',
                                                'TRANSPOR',
                                                'VIVE_EN',
                                                'VIVE_CON',
                                                'TRANSPOR',
                                                'EDOCIVIL',
                                                'EXTRA1',
                                                'EXTRA2',
                                                'EXTRA3',
                                                'EXTRA4',
                                                'NIVEL1',
                                                'NIVEL2',
                                                'NIVEL3',
                                                'NIVEL4',
                                                'NIVEL5',
                                                'NIVEL6',
                                                'NIVEL7',
                                                'NIVEL8',
                                                'NIVEL9',
                                                {$ifdef ACS}
                                                'NIVEL10',
                                                'NIVEL11',
                                                'NIVEL12',
                                                {$endif}
                                                'SSOCIAL',
                                                'RPATRON',
                                                'OTRASPER',
                                                'TKARDEX',
                                                'INCIDEN',
                                                'CONTRATO',
                                                'CLASIFI',
                                                'TPRESTA' );
  aCatalogos2: array[ 1..5 ] of String = ( 'TURNO.TU_DESCRIP',
                                           'QUERYS.QU_DESCRIP',
                                           'PUESTO.PU_DESCRIP',
                                           'HORARIO.HO_DESCRIP',
                                           'CONCEPTO.CO_DESCRIP' );
  var
     i: Integer;
  begin { TransformaAncho }
       for i := Low( aCatalogos ) to High( aCatalogos ) do
       begin
            if ( Trim( sFormula ) = aCatalogos[ i ] + '.TB_ELEMENT' ) then
            begin
                 iAncho := 25;
                 Exit;
            end;
       end;
       for  i := Low( aCatalogos2 ) to High( aCatalogos2 ) do
       begin
            if ( Trim( sFormula ) = aCatalogos2[ i ] ) then
            begin
                 iAncho := 25;
                 Exit;
            end;
       end;
  end; { TransformaAncho }

  function TransformaEmpleado( const sValor: String ): String;
  var
     i: Integer;
  begin { TransformaEmpleado }
       Result := sValor;
       for  i := Low( aTablas ) to High( aTablas ) do
       begin
            Result := StrTransAll( Result, aTablas[ i ] + 'CB_CODIGO=', 'STR( ' + aTablas[ i ] + 'CB_@@@CODIGO, 5 )=' );
            Result := StrTransAll( Result, aTablas[ i ] + 'CB_CODIGO =', 'STR( ' + aTablas[ i ] + 'CB_@@@CODIGO, 5 )=' );
            Result := StrTransAll( Result, aTablas[ i ] + 'CB_CODIGO$', 'STR( ' + aTablas[ i ] + 'CB_@@@CODIGO, 5 )$' );
            Result := StrTransAll( Result, aTablas[ i ] + 'CB_CODIGO $', 'STR( ' + aTablas[ i ] + 'CB_@@@CODIGO, 5 )$' );
       end;
       Result := StrTransAll( Result, 'CB_@@@CODIGO', 'CB_CODIGO' );
 end; { TransformaEmpleado }

 procedure Advertencia( const sBusca, sMensaje, sSugerencia: String );
 begin { Advertencia }
      if ( AnsiPos( sBusca, sFormula ) > 0 ) then
      begin
           Bitacora.AddWarning( FReporte,
                                'Advertencia: ' + sMensaje + CR_LF +
                                'Expresi�n:   ' + ZetaCommonTools.BorraCReturn( sFormula ) + CR_LF +
                                'Sugerencia:  ' + sSugerencia );
      end;
 end; { Advertencia }

 procedure AdvertenciaAncho;
 begin { AdvertenciaAncho }
      if ( Length( Result ) > K_ANCHO_MAXIMO ) then
      begin
           Bitacora.AddWarning( FReporte,
                                Format( 'Advertencia: La Expresi�n Tiene M�s De %d Letras', [ K_ANCHO_MAXIMO ] ) + CR_LF +
                                'Expresi�n:  ' + Result + CR_LF +
                                'Sugerencia:  Revisar Que La Expresi�n Sea Correcta' );
           Result := Copy( Result, 1, K_ANCHO_MAXIMO );
      end;
 end; { AdvertenciaAncho }

 procedure AdvertenciaCampo( const sCampo: String );
 begin { AdvertenciaCampo }
      Advertencia( sCampo,
                   'El Campo o F�rmula ' + sCampo + ' Ya No Es Compatible Con El Sistema',
                   'Sustituir Por Un Campo o Expresi�n V�lido' );
 end; { AdvertenciaCampo }

 function CrearPlantilla( const sValores: String ): Boolean;
 begin { CrearPlantilla }
      Result := ( AnsiPos( sValores, sFormula ) > 0 );
      if Result then
      begin
           Bitacora.AddWarningList( K_REPORTE_CON_FORMULAS_DESCONTINUADAS, FReporte, Format( '%s(), ', [ sValores ] ) );
      end;
 end; { CrearPlantilla }

 procedure AdvertenciaHP( const sCampo: String );
 begin { AdvertenciaHP }
      lHP := CrearPlantilla( sCampo );
 end; { AdvertenciaHP }

 procedure AdvertenciaMEMO( const sCampo: String );
 begin { AdvertenciaMEMO }
      CrearPlantilla( sCampo );
 end; { AdvertenciaMEMO }

 procedure AdvertenciaUsuarios;
 const
      K_MENSAJE = 'Cambio de Tipo de Datos ( %s a US_CODIGO )';
      K_SUGIERE = 'Revise Sintaxis, Especialmente En Filtros';
 begin { AdvertenciaUsuarios }
      Advertencia( 'AH_USER', Format( K_MENSAJE, [ 'AH_USER' ] ), K_SUGIERE );
      Advertencia( 'AU_USER', Format( K_MENSAJE, [ 'AU_USER' ] ), K_SUGIERE );
      Advertencia( 'CR_USER', Format( K_MENSAJE, [ 'CR_USER' ] ), K_SUGIERE );
      Advertencia( 'CR_USER', Format( K_MENSAJE, [ 'CR_USER' ] ), K_SUGIERE );
      Advertencia( 'CH_USER', Format( K_MENSAJE, [ 'CH_USER' ] ), K_SUGIERE );
      Advertencia( 'CB_USER', Format( K_MENSAJE, [ 'CB_USER' ] ), K_SUGIERE );
      Advertencia( 'LS_USER', Format( K_MENSAJE, [ 'LS_USER' ] ), K_SUGIERE );
      Advertencia( 'MO_USER', Format( K_MENSAJE, [ 'MO_USER' ] ), K_SUGIERE );
      Advertencia( 'ES_USER', Format( K_MENSAJE, [ 'ES_USER' ] ), K_SUGIERE );
 end; { AdvertenciaUsuarios }

 procedure AdvertenciaVacaciones;
 const
      K_MENSAJE = 'Ya No Existe Este Campo ( %s )';
      K_SUGIERE = 'Revise Sintaxis, ( Valor actual = 0.00 )';
 begin { AdvertenciaVacaciones }
      Advertencia( 'CB_V_ID_2', Format( K_MENSAJE, [ 'CB_V_ID_2' ] ), K_SUGIERE );
      Advertencia( 'CB_V_PA_0', Format( K_MENSAJE, [ 'CB_V_PA_0' ] ), K_SUGIERE );
      Advertencia( 'CB_V_PA_2', Format( K_MENSAJE, [ 'CB_V_PA_2' ] ), K_SUGIERE );
      Advertencia( 'CB_V_GO_0', Format( K_MENSAJE, [ 'CB_V_GO_0' ] ), K_SUGIERE );
      Advertencia( 'CB_V_GO_2', Format( K_MENSAJE, [ 'CB_V_GO_2' ] ), K_SUGIERE );
 end; { AdvertenciaVacaciones }

 procedure AdvertenciaType;
 begin { AdvertenciaType }
      Advertencia( 'TYPE',
                   'La Funci�n TYPE(), ya no es compatible con el Sistema. Se elimin� de la Expresi�n.',
                   'Revisar que el Reporte Funcione Correctamente.' );

      sFormula := StrTransAll( sFormula, 'TYPE', '' );
 end; { AdvertenciaType }

 procedure AdvertenciaEMP_VACA;
 begin { AdvertenciaEMP_VACA }
      if ( Pos( 'EMP_VACACION(8)=', Result ) > 0 ) or ( Pos( 'EMP_VACACION(8) =', Result ) > 0 ) then
         AdvertenciaCampo( 'EMP_VACACION(8)' );
 end; { AdvertenciaEMP_VACA }


 procedure AdvertenciaNumeroTotal;
 begin { AdvertenciaNumeroTotal }
      if ( FTipoSalida in [ Ord( tfASCIIDel ), Ord( tfASCIIFijo ) ] ) and FEncabezado then
      begin
           AdvertenciaCampo( 'NUMERO_TOTAL' );
      end
      else
      begin
           Advertencia( 'NUMERO_TOTAL',
                        'NUMERO_TOTAL() Esta Descontinuada ( Se Cambio Por La Funci�n LINEA() )',
                        'Revisar Que El Reporte Funcione Correctamente' );
           sFormula := StrTransAll( sFormula, 'NUMERO_TOTAL', 'LINEA' );
      end;
 end; { AdvertenciaNumeroTotal }

 procedure AdvertenciaDIASRANGO;
 var
    sCampo: String;
 begin { AdvertenciaDIASRANGO }
      case FEntidad of
           enVacacion: sCampo := 'VACACION.VA_GOZO';
           enPermiso: sCampo := 'PERMISO.PM_DIAS';
      else
          sCampo := 'INCAPACI.IN_DIAS';
      end;
      Advertencia( 'DIAS_RANGO',
                   'Funci�n DIAS_RANGO(), Esta Descontinuada ( Se cambio por el campo ' + sCampo +' )',
                   'Revisar Que El Reporte Funcione Correctamente.' );
      Result := StrTransAll( Result, 'DIAS_RANGO()', sCampo );
      Result := StrTransAll( Result, 'DIAS_RANGO( )', sCampo );
      Result := StrTransAll( Result, 'DIAS_RANGO', sCampo );
 end; { AdvertenciaDIASRANGO }

 function MascaraNoCeros( const sTexto: String ): Boolean;
 begin { MascaraNoCeros }
      sMascaraNoCeros := '';
      Result := ( AnsiPos( 'PMOV(1,1)', sTexto ) > 0 ) or
                ( AnsiPos( 'PMOV(2,1)', sTexto ) > 0 );

      if Result then
         sMascaraNoCeros := '#,0;-#,0;#';
      Result := Result or
                ( AnsiPos ('PMOV(1,3)', sTexto ) > 0 ) or
                ( AnsiPos( 'PMOV(1,4)', sTexto ) > 0 ) or
                ( AnsiPos( 'PMOV(2,3)', sTexto ) > 0 ) or
                ( AnsiPos( 'PMOV(2,4)', sTexto ) > 0 );
 end; { MascaraNoCeros }

begin { AnalizaFormula }
     lHP := False;
     AdvertenciaHP( 'HPMEMO' );
     AdvertenciaHP( 'HPREV' );
     AdvertenciaHP( 'HPPAT' );
     AdvertenciaHP( 'HPGRIS' );
     AdvertenciaHP( 'HPSOMBRA' );
     AdvertenciaHP( 'HPGIRA' );
     AdvertenciaHP( 'HPCONFIGURA' );
     AdvertenciaHP( 'HPFONT' );
     AdvertenciaHP( 'HPCIRCULO' );
     AdvertenciaHP( 'HPRAYA' );
     AdvertenciaHP( 'HPL' );
     AdvertenciaHP( 'HPEJECT' );
     AdvertenciaHP( 'HPFOTO' );
     AdvertenciaHP( 'HPCAJAGRIS' );
     AdvertenciaHP( 'HPCAJADIBUJO' );
     AdvertenciaHP( 'HPCAJA' );
     if not lHP then
        AdvertenciaHP( 'HP' );
     AdvertenciaNumeroTotal;
     AdvertenciaMEMO( 'MEMOLINE' );
     Advertencia( 'IN_CARGO',
                  'El Campo IN_CARGO Esta Descontinuado ( Se Cambi� Por El Campo VA_CARGO )',
                  'PENDIENTE LA SUGERENCIA' );
     AdvertenciaVacaciones;
     AdvertenciaUsuarios;
     AdvertenciaType;
     Result := ConvierteFormula( sFormula, EmpleadoSTR );
     case FEntidad of
          enKardex: Result := ConvierteCamposColabora(aKardex,'KARDEX',Result);
          enNomina: Result := ConvierteCamposColabora(aNominas,'NOMINA',Result);
          enAusencia : Result := ConvierteCamposColabora(aAusencia,'AUSENCIA',Result);
          enKarCurso : Result := ConvierteCamposColabora(aKarCurso,'KARCURSO',Result);
     end;
     Result := ConvierteCamposColabora(aEmpleados,'COLABORA',Result);
     Result := TransformaResult( Result, FOffSet );
     sFormula := ' ' + UpperCase( Result );
     AdvertenciaDIASRANGO;
     AdvertenciaEMP_VACA;
     Advertencia( 'ESCALON',
                  'La Funci�n ESCALON() Est� Descontinuada',
                  'Dar La Alta De Redondeos En Las Tablas Num�ricas y' + CR_LF + 'Relacionar La Tabla Con Globales de Asistencia En Redondeo de Horas' );
     Advertencia( 'HORAS_EXENTAS',
                  'La Funci�n HORAS_EXENTAS() Est� Descontinuada',
                  'Cambiar por el campo NOMINA.NO_EXENTAS' );
     Advertencia( 'RELATED', 'La Funci�n RELATED() Est� Descontinuada', 'Usar Directamente El Nombre Del Campo Requerido' );
     Advertencia( 'CALC_CREDITO', 'La Funci�n CALC_CREDITO() Solamente se Puede Utilizar en Conceptos de N�mina', 'Usar otra Funci�n Equivalente' );
     Advertencia( 'ART86', 'La Funci�n ART86() Solamente se Puede Utilizar en Conceptos de N�mina', 'Usar otra Funci�n Equivalente' );
     Advertencia( 'ART86I', 'La Funci�n ART86I() Solamente se Puede Utilizar en Conceptos de N�mina', 'Usar otra Funci�n Equivalente' );
     Advertencia( 'CALC_ISPT', 'La Funci�n CALC_ISPT() Solamente se Puede Utilizar en Conceptos de N�mina', 'Usar otra Funci�n Equivalente' );
     Advertencia( 'ART80_SEPARA', 'La Funci�n ART80_SEPARA() Solamente se Puede Utilizar en Conceptos de N�mina', 'Usar otra Funci�n Equivalente' );

     //Advertencia( 'LIQUIDA', 'Se Encontr� La Palabra "LIQUIDA"', 'Revisar Que La F�rmula Sea Correcta' );
     CrearPlantilla( 'FOTO' );
     AdvertenciaCampo( 'CB_EDAD_HS' );
     AdvertenciaCampo( 'CB_EDAD_MS' );
     AdvertenciaCampo( 'EDAD_2_HIJ' );
     AdvertenciaCampo( 'CB_FAMILIA' );
     AdvertenciaCampo( 'CB_PARENT' );
     AdvertenciaCampo( 'LIMPIA_STR' );
     AdvertenciaCampo( 'AU_R_IN1' );
     AdvertenciaCampo( 'AU_R_OUT2' );
     AdvertenciaCampo( 'LE_USER' );
     AdvertenciaCampo( 'US_HASH' );
     AdvertenciaCampo( 'DISP_TIPOCO' );
     AdvertenciaCampo( 'CB_NIVELES' );
     AdvertenciaCampo( 'CLAS_PERM' );
     AdvertenciaCampo( 'TOT_INCIDE' );
     AdvertenciaCampo( 'DIAS_RANGO' );
     AdvertenciaCampo( 'CB_CONGELA' );
     AdvertenciaCampo( 'TYPE' );
     AdvertenciaCampo( 'VARAUX' );
     AdvertenciaCampo( 'SHOW_NAME' );
     AdvertenciaCampo( 'CB_CARGO' );
     AdvertenciaCampo( 'X:=' );
     Advertencia( 'SIN_DIA',
                  'La Funci�n SIN_DIA() Est� Descontinuada ( Se Cambi� Por FECHA_CORTA() )',
                  'Revisar Que El Reporte Funcione Correctamente' );
     case FEntidad of
          enPermiso: Result := StrTransAll( Result, 'IN_', 'PM_' );
          enVacacion:
          begin
               Result := StrTransAll( Result, 'IN_DIAS','VA_GOZO');
               Result := StrTransAll( Result, 'IN_', 'VA_' );
          end;
          enMovimien:
          begin
               FMascaraNoCeros := MascaraNoCeros(Result);
               Result := StrTransAll( Result, 'PMOV(1,1)', 'MOVIMIEN.CO_NUMERO' );
               Result := StrTransAll( Result, 'PMOV(1,2)', 'CONCEPTO.CO_DESCRIP' );
               Result := StrTransAll( Result, 'PMOV(1,3)', 'MOVIMIEN.MO_PERCEPC' );
               Result := StrTransAll( Result, 'PMOV(2,1)', 'MOVIMIEN.CO_NUMERO' );
               Result := StrTransAll( Result, 'PMOV(2,2)', 'CONCEPTO.CO_DESCRIP' );
               Result := StrTransAll( Result, 'PMOV(2,3)', 'MOVIMIEN.MO_DEDUCCI' );
          end;
          enMovimienBalanza:
          begin
               FMascaraNoCeros := MascaraNoCeros(Result);
               Result := StrTransAll( Result, 'PMOV(1,1)', 'TMPBALAN.TZ_NUM_PER' );
               Result := StrTransAll( Result, 'PMOV(1,2)', 'TMPBALAN.TZ_DES_PER' );
               Result := StrTransAll( Result, 'PMOV(1,3)', 'TMPBALAN.TZ_MON_PER' );
               Result := StrTransAll( Result, 'PMOV(1,4)', 'TMPBALAN.TZ_HOR_PER' );
               Result := StrTransAll( Result, 'PMOV(2,1)', 'TMPBALAN.TZ_NUM_DED' );
               Result := StrTransAll( Result, 'PMOV(2,2)', 'TMPBALAN.TZ_DES_DED' );
               Result := StrTransAll( Result, 'PMOV(2,3)', 'TMPBALAN.TZ_MON_DED' );
               Result := StrTransAll( Result, 'PMOV(2,4)', 'TMPBALAN.TZ_HOR_DED' );
          end;
          enPoliza:
          begin
               Result := StrTransAll( Result, 'CUENTA', 'POLIZA.TP_CUENTA' );
               Result := StrTransAll( Result, 'MONTO', 'POLIZA.TP_CARGO+POLIZA.TP_ABONO' );
               Result := StrTransAll( Result, 'TIPO', 'POLIZA.TP_CAR_ABO' );
               Result := StrTransAll( Result, 'COMENTARIO', 'POLIZA.TP_COMENTA' );
          end;
     end;
     AdvertenciaAncho;//Revisa que la expresion no exceda los 255 caracteres.
     TransformaAncho;
end; { AnalizaFormula }

procedure TdmMigrarDatos.ReportesOpenDatasets;
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'insert into REPORTE( ' +
                                     'RE_CODIGO, RE_NOMBRE, ' +
                                     'RE_TIPO, RE_TITULO, ' +
                                     'RE_ENTIDAD, RE_SOLOT, ' +
                                     'RE_CLASIFI, RE_FECHA, ' +
                                     'RE_REPORTE, RE_PRINTER, ' +
                                     'RE_PFILE, RE_ARCHIVO, ' +
                                     'US_CODIGO, RE_FILTRO, ' +
                                     'RE_NIVEL, QU_CODIGO ) values ( ' +
                                     ':RE_CODIGO, :RE_NOMBRE, ' +
                                     ':RE_TIPO, :RE_TITULO, ' +
                                     ':RE_ENTIDAD, :RE_SOLOT, ' +
                                     ':RE_CLASIFI, :RE_FECHA, ' +
                                     ':RE_REPORTE, :RE_PRINTER, ' +
                                     ':RE_PFILE, :RE_ARCHIVO, ' +
                                     ':US_CODIGO, :RE_FILTRO, ' +
                                     ':RE_NIVEL, :QU_CODIGO )' );
          PrepareTargetQuery( tqDos, 'select DI_TITULO, DI_TCORTO, DI_ANCHO, DI_MASCARA from DICCION ' +
                                     'where ( DI_CLASIFI = :Entidad ) AND ( DI_NOMBRE = :Campo )' );
          PrepareTargetQuery( tqTres, 'select MAX( RE_CODIGO ) + 1 from REPORTE' );
     end;
     FReporteOK := False;
     FReporte := '';
end;

procedure TdmMigrarDatos.RepRHCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqUno );
          CloseQuery( tqDos );
          CloseQuery( tqTres );
     end;
end;

procedure TdmMigrarDatos.Forma2CloseDatasets(Sender: TObject);
 const K_UPDATE = 'UPDATE REPORTE '+
                  'SET RE_ENTIDAD = 50 '+  //Cambia a Nomina
                  'where RE_ENTIDAD = 89 and RE_TIPO = 5 '+  
                  'AND ((SELECT COUNT(*) FROM CAMPOREP '+
                  'WHERE CAMPOREP.RE_CODIGO = REPORTE.RE_CODIGO '+
                  'AND CR_TIPO =0 AND CR_COLOR = 1) = 0)';
begin
     inherited;
     with TablaMigrada do
     begin
          {CV: 30-Dic-2001
          Este query es para aquellos reportes que se migraron con tabla principal de Balanza
          y que no contienen Datos en el Detalle del Reporte.}
          PrepareTargetQuery( tqCuatro, K_UPDATE );
          tqCuatro.ExecSQL;

          CloseQuery( tqCuatro );
     end;
     RepRHCloseDatasets(Sender);
end;

function TdmMigrarDatos.GetPosicion: Integer;
begin
     Inc( FPosicion );
     Result := FPosicion;
end;

function TdmMigrarDatos.GetInfo( sCampo: String; Entidad: TipoEntidad ): TInfo;
var
   iPos: Integer;
begin
     if ( sCampo = K_PRETTY ) then
     begin
          with Result do
          begin
               Titulo := 'Nombre';
               Ancho := 35;
               Mascara := '';
          end;
     end
     else
     begin
          iPos := Pos( '.', sCampo );
          if ( iPos > 0 ) then
             sCampo := Copy( sCampo, iPos + 1, MAXINT );
          with tqDos do
          begin
               Active := False;
               ParamAsInteger( 'Entidad', Ord( Entidad ), tqDos );
               ParamAsString( 'Campo', Copy( sCampo, 1, 10 ), tqDos );
               Active := True;
               if IsEmpty then
               begin
                    with Result do
                    begin
                         Titulo := VACIO;
                         Ancho := 0;
                         Mascara := VACIO;
                    end;
               end
               else
               begin
                    with Result do
                    begin
                         Titulo := FieldByName( 'DI_TCORTO' ).AsString;
                         Ancho := FieldByName( 'DI_ANCHO' ).AsInteger;
                         Mascara := FieldByName( 'DI_MASCARA' ).AsString;
                    end;
               end;
               Active := False;
          end;
     end;
end;

function TdmMigrarDatos.GetAncho( const iAncho: Integer; const sFormula: String; const Entidad: TipoEntidad ): Integer ;
const
     aCatalogos: array[ 1..28 ] of String = ( 'MOT_BAJA',
                                              'ESTUDIOS',
                                              'TRANSPOR',
                                              'VIVE_EN',
                                              'VIVE_CON',
                                              'TRANSPOR',
                                              'EDOCIVIL',
                                              'EXTRA1',
                                              'EXTRA2',
                                              'EXTRA3',
                                              'EXTRA4',
                                              'NIVEL1',
                                              'NIVEL2',
                                              'NIVEL3',
                                              'NIVEL4',
                                              'NIVEL5',
                                              'NIVEL6',
                                              'NIVEL7',
                                              'NIVEL8',
                                              'NIVEL9',
                                              'SSOCIAL',
                                              'RPATRON',
                                              'OTRASPER',
                                              'TKARDEX',
                                              'INCIDEN',
                                              'CONTRATO',
                                              'CLASIFI',
                                              'TPRESTA' );
     aCatalogos2: array[ 1..5 ] of String = ( 'TURNO.TU_DESCRIP',
                                              'QUERYS.QU_DESCRIP',
                                              'PUESTO.PU_DESCRIP',
                                              'HORARIO.HO_DESCRIP',
                                              'CONCEPTO.CO_DESCRIP' );
var
   i: Integer;
begin
     Result := 0;
     if ( iAncho = 0 ) then
     begin
          for  i := Low(aCatalogos) to High(aCatalogos) do
               if Trim(sFormula) = aCatalogos[i]+'.TB_ELEMENT' then
               begin
                    Result := 25;
                    Exit;
               end;
          if Result = 0 then
          begin
               for  i := Low(aCatalogos2) to High(aCatalogos2) do
               if Trim(sFormula) = aCatalogos2[i] then
               begin
                    Result := 25;
                    Exit;
               end;
          end;
          if Result = 0 then
          begin
               Result := GetInfo( sFormula, FEntidad ).Ancho;
               if ( Result = 0 ) then
                  Result := 10;
          end;
     end
     else
         Result := iAncho;
end;

procedure TdmMigrarDatos.FiltroFormas( const TipoFiltro: eTipoRango;
                                       const sCampo: String;
                                       sFiltro, sDescrip: String;
                                       const iEntidad: TipoEntidad;
                                       const iOper: Integer;
                                       const iTipo: eTipoGlobal;
                                       const Ancho: Integer );

  procedure AgregaUnCampo( const sDesc: String; const RangoActivo: eTipoRangoActivo );
  begin
       {iPosicion, iSubPos, iCalculado,
       iAncho, iTField, iShow, iAlinea, iColor, iOperacion: Integer;
       sFormula, sTitulo, sMascara, sRequiere,
       sDescripcion, sBold, sItalic, sSubraya, sStrike: String;
       TipoCampo: eTipoCampo;
       iEntidad: TipoEntidad }
       AgregaCampo( GetPosicion,
                    -1,
                    Ord( TipoFiltro ),
                    Ord( RangoActivo ),
                    Ord( iTipo ),
                    0,
                    0,
                    0,
                    iOper,
                    sCampo,
                    GetInfo( sCampo, iEntidad ).Titulo,
                    VACIO,
                    sFiltro,
                    sDesc,
                    VACIO,
                    VACIO,
                    VACIO,
                    VACIO,
                    tcFiltro,
                    iEntidad,
                    TRUE );
  end;

begin
     case TipoFiltro of
          rNinguno: AgregaUnCampo( sDescrip, raRango );
          rRangoEntidad, rRangoListas:
          begin
               if ( sFiltro = VACIO ) then
               begin
                    if ( Ancho > 0 ) then
                       AgregaUnCampo( sDescrip, raActivo )
                    else
                        AgregaUnCampo( sDescrip, raTodos );
               end
               else
               if ( sFiltro = K_GLOBAL_SI ) then
               begin
                    sFiltro := '';
                    AgregaUnCampo( sDescrip, raActivo );
               end
               else
                   AgregaUnCampo( sDescrip, raLista );
          end;
          rFechas: AgregaUnCampo( sDescrip, raRango );
          rBool:
          begin
               if ( sFiltro = K_GLOBAL_SI ) then
               begin
                    sFiltro := '0';
                    AgregaUnCampo( '0', raActivo )
               end
               else
               begin
                    sFiltro := '1';
                    AgregaUnCampo( '1', raActivo );
               end;
          end;
    end;
end;

procedure TdmMigrarDatos.AgregaCampo( const iPosicion, iSubPos, iCalculado,
                                            iAncho, iTField, iShow, iAlinea, iColor, iOperacion: Integer;
                                            sFormula, sTitulo, sMascara, sRequiere,
                                            sDescripcion, sBold, sItalic, sSubraya, sStrike: String;
                                      const TipoCampo: eTipoCampo;
                                      const iEntidad: TipoEntidad;
                                      const FDeDiccion : Boolean  );
var
   iAnchoDescrip: Integer;
begin
     iAnchoDescrip := iAncho;
     ParamAsInteger( 'RE_CODIGO', FCodigoReporte, TargetQuery );
     ParamAsInteger( 'CR_TIPO', Ord( TipoCampo ), TargetQuery );
     ParamAsInteger( 'CR_POSICIO', iPosicion, TargetQuery );
     ParamAsInteger( 'CR_SUBPOS', iSubPos, TargetQuery );
     ParamAsInteger( 'CR_TABLA', Ord( iEntidad ), TargetQuery );
     ParamAsString( 'CR_TITULO', Copy( sTitulo, 1, 30 ), TargetQuery );
     if FDeDiccion then
        ParamAsString( 'CR_FORMULA', Copy( sFormula, 1, K_ANCHO_MAXIMO ), TargetQuery )
     else
         ParamAsString( 'CR_FORMULA', Copy( AnalizaFormula( sFormula, FALSE, iAnchoDescrip ), 1, K_ANCHO_MAXIMO ), TargetQuery );
     ParamAsString( 'CR_REQUIER', Copy( sRequiere, 1, K_ANCHO_MAXIMO ), TargetQuery );
     ParamAsInteger( 'CR_CALC', iCalculado, TargetQuery );

     if StrVacio( sMascara ) and FMascaraNoCeros then
     begin
          if StrLleno(sMascaraNoCeros) then
             sMascara := sMascaraNoCeros
          else
              sMascara := '#,0.00;-#,0.00;#';
     end;
     ParamAsString( 'CR_MASCARA', Copy( sMascara, 1, 20 ), TargetQuery );
     if not ( TipoCampo in [ tcCampos, tcEncabezado, tcPieGrupo ] ) then
        iAnchoDescrip := iAncho;
     ParamAsInteger( 'CR_ANCHO', iAnchoDescrip, TargetQuery );
     ParamAsInteger( 'CR_OPER', iOperacion, TargetQuery );
     ParamAsInteger( 'CR_TFIELD', iTField, TargetQuery );
     ParamAsInteger( 'CR_SHOW', iShow, TargetQuery );
     ParamAsString( 'CR_DESCRIP', sDescripcion, TargetQuery );
     ParamAsString( 'CR_BOLD', sBold, TargetQuery );
     ParamAsString( 'CR_ITALIC', sItalic, TargetQuery );
     ParamAsString( 'CR_SUBRAYA', sSubraya, TargetQuery );
     ParamAsString( 'CR_STRIKE', sStrike, TargetQuery );
     ParamAsInteger( 'CR_ALINEA', iAlinea, TargetQuery );
     ParamAsInteger( 'CR_COLOR', iColor, TargetQuery );
     TargetQuery.ExecSQL;
end;

procedure TdmMigrarDatos.RepRHFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var
   lEsPoliza: Boolean;

function GetNumeroTabla( const iTabla: Integer ): TipoEntidad;
begin
      Result := enNinguno;
      FFiltroEmpleados := eTodos;
      FAgrupaEmpleado := False;
      case FBaseReporte of
           eREPORTES:
           begin
                case iTabla of
                     1: begin
                             Result := enEmpleado; {Empleados}
                             FFiltroEmpleados := eEmpleados;
                        end;
                     2: begin
                             Result := enEmpleado; {Bajas}
                             FFiltroEmpleados := eBajas;
                        end;
                     3: begin
                             Result := enEmpleado; {Emp/Bajas}
                             FFiltroEmpleados := eTodos;
                        end;
                     4: Result := enKardex;   {Kardex}
                     5: begin
                             Result := enKardex;   {Kardex X Empl.}
                             FAgrupaEmpleado := TRUE;
                        end;
                     //6: Conteo; {Nuevo en 2.60}
                end
           end;
           eREPCUR:
           begin
                case iTabla of
                     1: Result := enKarCurso; {Kardex de Cursos}
                     2: begin
                             Result := enEmpleado; {Empleados}
                             FFiltroEmpleados := eEmpleados;
                        end;
                     3: begin
                             Result := enEmpleado; {Bajas}
                             FFiltroEmpleados := eBajas;
                        end;
                     4: begin
                             Result := enEmpleado; {Empleados/Bajas}
                             FFiltroEmpleados := eTodos;
                        end;
                end
           end;
           eREPASIS, eESPECIAL:
           begin
                case iTabla of
                     1 : begin
                              Result := enNomina ;{N�mina Periodo Activo}
                         end;
                     2 : begin
                              Result := enEmpleado ;{Empleados}
                              FFiltroEmpleados := eEmpleados;
                         end;
                     3 : begin
                              Result := enEmpleado ;{Bajas}
                              FFiltroEmpleados := eBajas;
                         end;
                     4 : begin
                              Result := enEmpleado ;{Emp/Bajas}
                              FFiltroEmpleados := eTodos;
                         end;
                     5 : begin
                              Result := enNomina ;{Nomina Todo el A�o, Agrupado por Empleado}
                              FAgrupaEmpleado := TRUE;
                         end;
                     6 : Result := enPoliza ;{P�liza}
                     7 : Result := enPoliza ;{P�liza ASCII}
                     8 :
                     begin
                          Result := enAusencia ;{Calendario}
                     end;
                     9 : Result := enAhorro;
                     10 : Result := enPrestamo;
                end
           end;
           eREPTARJ:
           begin
                Result := enAusencia;
                Bitacora.AddWarning( K_ASISTENCIA, FReporte );
           end;
           eREPSAR:
           begin
                case iTabla of
                     //1 : DESAPARECE {SAR-01/SAAC-01}
                     //2 : DESAPARECE {SAR-02/SAAC-02}
                     //3 : DESAPARECE {SAR-04 (Act.)}
                     4 : Result := enIncapacidad ;{Incapacidades}
                     5 : Result := enVacacion ;{Vacaciones}
                     6 : Result := enPermiso ;{Permisos}
                     //7 : DESAPARECE {Historial SAR-02}
                     //8 : DESAPARECE{Auditor�a}
                     9 : Result := enLiq_IMSS ;{Total Mensual}
                     10: Result := enLiq_Emp ;{Empleados Mensual}
                     11 : Result := enLiq_Mov ;{Detalle Mensual}
                end
           end;
           eSUPERVISOR:
           begin
                case iTabla of
                     1: begin
                             Result := enEmpleado; {Empleados}
                             FFiltroEmpleados := eEmpleados;
                        end;
                     2: begin
                             Result := enEmpleado; {Bajas}
                             FFiltroEmpleados := eBajas;
                        end;
                     3: begin
                             Result := enEmpleado; {Empleados/Bajas}
                             FFiltroEmpleados := eTodos;
                        end;
                     4 : Result := enKardex;   {Kardex X Fecha.}
                     5 : begin
                              Result := enKardex;   {Kardex X Empleado.}
                              FAgrupaEmpleado := TRUE;
                         end;
                     6 : Result := enAusencia;
                     7 : Result := enNomina;
                     8 : Result := enIncapacidad;
                     9 : Result := enVacacion;
                     10 : Result := enPermiso;
                     {11 : BITACORA NO SE MIGRA )}
                     {12 : ASIGNA2, no migrar ( nuevo en 2.60 )}
                end;
           end;
      end;
end;

function GetPlantilla: String;
const
     kTemplateExt = '.QR2';
     kTemplateLand = 'DEFAULT LANDSCAPE';
     kTemplate = 'DEFAULT';
var
   sPapel: String;
begin
     with Source do
     begin
          sPapel := FieldByName( 'ES_PAPEL' ).AsString;
          if ( sPapel = VACIO ) or ( sPapel = '1' ) then
             Result := kTemplate + kTemplateExt
          else
              Result := kTemplateLand + kTemplateExt
     end;
end;

function GetSoloTotales: String;
var
   sNivel: String;
begin
     sNivel := Source.FieldByName( 'ES_NIVEL' ).AsString;
     Result := zBoolToStr( sNivel = '2' );
     //Result := zBoolToStr( ( sNivel <> VACIO ) and ( sNivel <> '1' ) );
end;

function GetTipoSalida: Integer;
begin
     case Source.FieldByName( 'ES_OUTPUT' ).AsInteger of
          3: Result := Ord( tfASCIIDel );
          4: Result := Ord( tfASCIIFijo )
     else
         Result := Ord( tfImpresora );
     end;
end;

procedure CamposFijos( const iPosicion, iSubPos, iCalculado: Integer;
                       sNombre, sTabla, sRequiere: String;
                       const Tipo: eTipoGlobal;
                       const Entidad: TipoEntidad;
                       const TipoCampo: eTipoCampo );
begin
     with GetInfo( sNombre, Entidad ) do
     begin
          if ZetaCommonTools.StrLleno( sTabla ) then
             sNombre := sTabla + PUNTO + sNombre;
          AgregaCampo( iPosicion,
                       iSubPos,
                       iCalculado,
                       Ancho,
                       Ord( Tipo ),
                       0,
                       0,
                       0,
                       Ord( ocNinguno ),
                       sNombre,
                       Titulo,
                       Mascara,
                       sRequiere,
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       TipoCampo,
                       FEntidad,
                       TRUE );
     end;
end;

procedure CamposFijosDesc( const iPosicion, iSubPos, iCalculado: Integer;
                           sNombre, sTabla, sRequiere: String;
                           const Tipo: eTipoGlobal;
                           const Entidad: TipoEntidad;
                           const TipoCampo: eTipoCampo );
begin
     with GetInfo( sNombre, Entidad ) do
     begin
          if ZetaCommonTools.StrLleno( sTabla ) then
             sNombre := sTabla + PUNTO + sNombre;
          AgregaCampo( iPosicion,
                       iSubPos,
                       iCalculado,
                       Ancho,
                       Ord( Tipo ),
                       0,
                       0,
                       0,
                       Ord( ocNinguno ),
                       sNombre,
                       ':',
                       Mascara,
                       sRequiere,
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       TipoCampo,
                       FEntidad,
                       TRUE );
     end;
end;

procedure OrdenListado;

procedure AgregaOrden( const sCampo: String; const Entidad: TipoEntidad );
begin
     AgregaCampo( GetPosicion,
                  -1,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  0,
                  sCampo,
                  GetInfo( sCampo, Entidad ).Titulo,
                  VACIO,
                  VACIO,
                  VACIO,
                  VACIO,
                  VACIO,
                  VACIO,
                  VACIO,
                  tcOrden,
                  Entidad,
                  TRUE );
end;

var
   iOrden: Integer;
begin
     iOrden := Str2Integer( Source.FieldByName( 'ES_POSICIO' ).AsString );
     if ( FEntidad <> enKardex ) then
     begin
          case iOrden of
               0, 1: AgregaOrden( 'CB_CODIGO', FEntidad );
               2:
               begin
                    AgregaOrden( 'COLABORA.CB_APE_PAT', enEmpleado );
                    AgregaOrden( 'COLABORA.CB_APE_MAT', enEmpleado );
                    AgregaOrden( 'COLABORA.CB_NOMBRES', enEmpleado );
               end;
          end;
     end
     else
     begin
          case iOrden of
               0, 1: AgregaOrden( 'KARDEX.CB_FECHA', FEntidad );
               2: AgregaOrden( 'KARDEX.CB_CODIGO', FEntidad );
               3: AgregaOrden( 'KARDEX.CB_TIPO', FEntidad );
               4:
               begin
                    AgregaOrden( 'COLABORA.CB_APE_PAT', enEmpleado );
                    AgregaOrden( 'COLABORA.CB_APE_MAT', enEmpleado );
                    AgregaOrden( 'COLABORA.CB_NOMBRES', enEmpleado );
               end;
          end;
     end;
end;

procedure GrupoListado;
var
   i, iPos, iLength: Integer;
   sGrupo, sCampo, sDescrip, sTitulo: String;
   iEntidad: TipoEntidad;

   function GetCampo: String;
   var
      sTablaGrupo: String;
   begin
        case FEntidad of
             enKardex: sTablaGrupo:= 'KARDEX';
             enKarCurso: sTablaGrupo:= 'KARCURSO';
             enNomina: sTablaGrupo:= 'NOMINA';
             enAusencia: sTablaGrupo:= 'AUSENCIA'
        else
            sTablaGrupo:= 'COLABORA';
        end;
        RESULT := VACIO;
        if ( i <= FNiveles ) then
        begin
             Result := 'CB_NIVEL' + IntToStr( i );
             sDescrip := 'NIVEL' + IntToStr( i ) + '.TB_ELEMENT';
             iEntidad := TipoEntidad( Ord( enNivel1 ) + i - 1 );
        end
        else
        begin
             if ( i = FNiveles + 1 ) then
             begin
                  Result := 'CB_PUESTO';
                  sDescrip := 'PUESTO.PU_DESCRIP';
                  iEntidad := enPuesto;
             end
             else if ( i = FNiveles + 2 ) then
             begin
                  Result := 'CB_CLASIFI';
                  sDescrip := 'CLASIFI.TB_ELEMENT';
                  iEntidad := enClasifi;
             end
             else if ( i = FNiveles + 3 ) then
             begin
                  Result := 'CB_TURNO';
                  sDescrip := 'TURNO.TU_DESCRIP';
                  iEntidad := enTurno;
             end
             else if ( i = FNiveles + 4 ) then
             begin
                  Result := 'CB_CONTRAT';
                  sDescrip := 'CONTRAT.TB_ELEMENT';
                  iEntidad := enContrato;
             end
             else if ( i = FNiveles + 5 ) then
             begin
                  Result := 'CB_CODIGO';
                  FAgrupaEmpleado := TRUE;
             end
             else if ( i = FNiveles + 6 ) then
                  Result := 'ESPECIAL';
        end;
        if ZetaCommonTools.StrLleno( Result ) then
           Result := sTablaGrupo + '.' + Result;
   end;

   procedure AgregaGrupo;
   begin
         AgregaCampo( iPos,
                      -1,                {SUBPOSICION}
                      BoolToInt( FALSE )  {SALTO DE PAG},
                      0,                 {ANCHO}
                      BoolToInt( TRUE ), {ENCABEZADO}
                      BoolToInt( TRUE ), {PIE DE GRUPO}
                      0,                 {ALINEACION}
                      0,                 {COLOR}
                      BoolToInt( TRUE ), {TOTALIZAR}
                      sCampo,            {FORMULA}
                      sTitulo,           {TITULO}
                      VACIO,             {MASCARA}
                      VACIO,             {REQUIERE}
                      VACIO,             {DESCIRPCION}
                      VACIO,             {BOLD}
                      VACIO,             {ITALIC}
                      VACIO,             {SUBRAYA}
                      VACIO,             {STRIKE}
                      tcGrupos,
                      FEntidad,
                      TRUE );        {TIPOCAMPO}
   end;

begin
     sGrupo := Source.FieldByName( 'ES_GRUPO' ).AsString;
     iLength := Length( sGrupo );
     sCampo := '';
     sTitulo := K_EMPRESA;
     FPosicion := -1;
     iPos := GetPosicion;
     AgregaGrupo;
     i := 1;
     while( i <= iLength ) do
     begin
          if ZetaCommonTools.StrVacio( sGrupo[ i ] ) then
             i := i + 1
          else
          begin
               sCampo := GetCampo;
               if ZetaCommonTools.StrLleno( sCampo ) and ( sCampo <> 'ESPECIAL' ) then
               begin
                    sTitulo := GetInfo( sCampo, FEntidad ).Titulo;
                    iPos := GetPosicion;
                    AgregaGrupo;
                    if ( sCampo <> 'CB_CODIGO' ) then
                    begin
                         CamposFijos( iPos, 0, -1, sCampo, VACIO, VACIO, tgTexto, FEntidad, tcEncabezado );
                         CamposFijosDesc( iPos, 1, -1, sDescrip, VACIO, VACIO, tgTexto, iEntidad, tcEncabezado );
                    end;
               end;
               i := i + 1;
          end;
     end;
     if FAgrupaEmpleado then
     begin
          sCampo := 'CB_CODIGO';
          sTitulo := GetInfo( sCampo, FEntidad ).Titulo;
          iPos := GetPosicion;
          AgregaGrupo;
          CamposFijos( iPos, 0, 0, 'CB_CODIGO', 'COLABORA', VACIO, tgNumero, FEntidad, tcEncabezado );
          //CamposFijosDesc( iPos, 1, 1, 'PRETTYNAME', 'COLABORA', 'COLABORA.CB_APE_PAT;COLABORA.CB_APE_MAT;COLABORA.CB_NOMBRES', tgTexto, enEmpleado, tcEncabezado );
          CamposFijosDesc( iPos, 1, -1, K_PRETTY, '', VACIO, tgTexto, enEmpleado, tcEncabezado );
     end;
end;

procedure FiltroListado;

procedure AgregaFiltro( const sCampo, sActivo: String );
begin
     AgregaCampo( GetPosicion,
                  -1,
                  Ord( rBool ),
                  Ord( raActivo ),
                  0,
                  0,
                  0,
                  0,
                  60,
                  sCampo,
                  GetInfo( sCampo, enEmpleado).Titulo,
                  VACIO,
                  sActivo,
                  K_GLOBAL_SI,
                  VACIO,
                  VACIO,
                  VACIO,
                  VACIO,
                  tcFiltro,
                  enEmpleado,
                  FALSE );
                  {A LO MEJOR LLEVA UN INC(POS)}
end;

begin
     case FFiltroEmpleados of
          eEmpleados: AgregaFiltro( 'CB_ACTIVO', '0' );
          eBajas: AgregaFiltro( 'CB_ACTIVO', '1' );
     end;

     case FEntidad of
          enKarCurso :
          begin
               FiltroFormas( rFechas, 'KARCURSO.KC_FEC_TOM',VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
               FiltroFormas( rRangoEntidad, 'KARCURSO.CB_PUESTO', VACIO, VACIO, FEntidad, Ord(enPuesto), tgTexto, 0 );
               FiltroFormas( rRangoEntidad, 'KARCURSO.CU_CODIGO', VACIO, VACIO, FEntidad, Ord(enCurso), tgTexto, 0 );
          end;
          enKardex :
          begin
               FiltroFormas( rFechas, 'KARDEX.CB_FECHA', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
               FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', VACIO, VACIO, FEntidad, Ord(enTKardex), tgTexto, 0 );
               FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
          end;
          enIncapacidad : FiltroFormas( rFechas, 'INCAPACI.IN_FEC_INI', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
          enPermiso : FiltroFormas( rFechas, 'PERMISO.PM_FEC_INI', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
          enVacacion : FiltroFormas( rFechas, 'VACACION.VA_FEC_INI', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
          enNomina :
          begin
               FiltroFormas( rRangoEntidad, 'NOMINA.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
               FiltroFormas( rRangoEntidad, 'NOMINA.PE_NUMERO', K_GLOBAL_SI, ARROBA_NUMERO, FEntidad, Ord(enPeriodo), tgNumero, 0 );
               FiltroFormas( rRangoEntidad, 'NOMINA.PE_YEAR', K_GLOBAL_SI, ARROBA_YEAR, FEntidad, -1, tgNumero, 0 );
               FiltroFormas( rRangoListas, 'NOMINA.PE_TIPO', K_GLOBAL_SI, ARROBA_TIPO, FEntidad, Ord(lfTipoPeriodo), tgNumero, 0 );
               //FiltroFormas( rNinguno, 'NOMINA.NO_NETO', '>0', VACIO, enNomina, 0, tgFloat );
          end;
          enAusencia : FiltroFormas( rFechas, 'AUSENCIA.AU_FECHA', VACIO, VACIO, enAusencia, 0, tgFecha, 0  );
          enLiq_IMSS :
          begin
               FiltroFormas( rRangoListas, 'LIQ_IMSS.LS_MONTH', K_GLOBAL_SI, ARROBA_IMSS_MES, FEntidad, Ord(lfMeses), tgNumero, 0 );
               FiltroFormas( rRangoEntidad, 'LIQ_IMSS.LS_YEAR', K_GLOBAL_SI, ARROBA_IMSS_YEAR, FEntidad, -1, tgNumero, 0 );
               FiltroFormas( rRangoListas, 'LIQ_IMSS.LS_TIPO', K_GLOBAL_SI, ARROBA_IMSS_TIPO, FEntidad, Ord(lfTipoLiqIMSS), tgNumero, 0 );
               FiltroFormas( rRangoEntidad, 'LIQ_IMSS.LS_PATRON', K_GLOBAL_SI, ARROBA_IMSS_PATRON, FEntidad, Ord(enRPatron), tgTexto, 0 );
          end;
          enLiq_Emp :
          begin
               FiltroFormas( rRangoListas, 'LIQ_EMP.LS_MONTH', K_GLOBAL_SI, ARROBA_IMSS_MES, FEntidad, Ord(lfMeses), tgNumero, 0 );
               FiltroFormas( rRangoEntidad,'LIQ_EMP.LS_YEAR', K_GLOBAL_SI, ARROBA_IMSS_YEAR, FEntidad, -1, tgNumero, 0 );
               FiltroFormas( rRangoListas,'LIQ_EMP.LS_TIPO', K_GLOBAL_SI, ARROBA_IMSS_TIPO, FEntidad, Ord(lfTipoLiqIMSS), tgNumero, 0 );
               FiltroFormas( rRangoEntidad,'LIQ_EMP.LS_PATRON', K_GLOBAL_SI, ARROBA_IMSS_PATRON, FEntidad, Ord(enRPatron), tgTexto, 0 );
          end;
          enLiq_Mov :
          begin
               FiltroFormas( rRangoListas, 'LIQ_MOV.LM_TIPO', '0', VACIO, FEntidad, Ord(lfTipoLiqMov), tgTexto, 0 );
               FiltroFormas( rRangoListas, 'LIQ_MOV.LS_MONTH', K_GLOBAL_SI, ARROBA_IMSS_MES, FEntidad, Ord(lfMeses), tgNumero, 0 );
               FiltroFormas( rRangoEntidad, 'LIQ_MOV.LS_YEAR', K_GLOBAL_SI, ARROBA_IMSS_YEAR, FEntidad, -1, tgNumero, 0 );
               FiltroFormas( rRangoListas, 'LIQ_MOV.LS_TIPO', K_GLOBAL_SI, VACIO, FEntidad, Ord(lfTipoLiqIMSS), tgNumero, 0 );
               FiltroFormas( rRangoEntidad, 'LIQ_MOV.LS_PATRON', K_GLOBAL_SI, ARROBA_IMSS_PATRON, FEntidad, Ord(enRPatron), tgTexto, 0 );
          end;
     end;
end;

procedure CamposListado;
var
   sPosicion, sFormula, sTitulo: String;
   TipoCampo: eTipoCampo;
   iPosicion, iSubPos, iCalculado: Integer;

function GetOperacion: eTipoOperacionCampo;
var
   sOper: String[ 1 ];
begin
     sOper := Source.FieldByName( 'ES_TOTAL' ).AsString;
     if ( sOper = ( 'N' ) ) then
        Result :=  ocNinguno
     else
         if ( sOper = ( 'S' ) ) then
            Result := ocSuma
         else
             if ( sOper = ( 'P' ) ) then
                Result := ocPromedio
             //else if ( sOper = ( 'T' ) ) then Result := ocSobreTotales {Esto todavia no es}
             else
                 Result :=  ocAutomatico
end;

procedure AgregaUnCampo;
var
   iAncho: Integer;
begin
     with Source do
     begin
          if ZetaCommonTools.StrVacio( sFormula ) then
             sFormula := FieldByName( 'ES_VALORES' ).AsString;
          if ZetaCommonTools.StrVacio( sTitulo ) then
             sTitulo := FieldByName( 'ES_HEADER' ).AsString;
          iAncho := GetAncho( FieldByName( 'ES_ANCHO' ).AsInteger, sFormula, FEntidad );
          if ( iAncho = 0 ) then
             iAncho := 10;
          AgregaCampo( iPosicion,
                       iSubPos,
                       iCalculado,
                       iAncho,
                       Ord( tgAutomatico ),
                       0,
                       0,
                       0,
                       Ord( GetOperacion ),
                       sFormula,
                       sTitulo,
                       VACIO,  //FieldByName( 'ES_PICTURE' ).AsString, QUEDA PENDIENTE
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       VACIO,
                       TipoCampo,
                       FEntidad,
                       FALSE );
     end;
end;

begin
     sFormula := VACIO;
     sTitulo := VACIO;
     iCalculado := -1;
     with Source do
     begin
          sPosicion := FieldByName( 'ES_POSICIO' ).AsString;
          iSubPos := -1;
          if ZetaCommonTools.StrLleno( sPosicion ) then
          begin
               if ( sPosicion[ 1 ] = '?' ) then {PARAMETRO}
               begin
                    iPosicion := GetPosicion;
                    TipoCampo := tcParametro;
               end
               else if ( sPosicion[ 1 ] = '=' ) then {DATO EN EL ENCABEZADO}
               begin
                    iPosicion := 0;
                    TipoCampo := tcEncabezado;
                    iSubPos := GetPosicion;
                    AgregaUnCampo;

                    iPosicion := 0;
                    TipoCampo := tcEncabezado;
                    iSubPos := GetPosicion;
                    iCalculado := K_RENGLON_NUEVO;
                    sTitulo := K_RENGLON_TITULO;
                    sFormula := K_RENGLON;

               end
               else if ( sPosicion[ 1 ] in  [ 'A'..'E' ] ) then {DATO EN EL ENCABEZADO DE UN ASCII}
               begin
                    FEncabezado := TRUE;
                    iPosicion := 0;

                    TipoCampo := tcEncabezado;
                    iSubPos := GetPosicion;

               end
               else if ( sPosicion[ 1 ] in  [ 'V'..'Z'] ) then {DATO EN EL PIE DE UN ASCII}
               begin
                    iPosicion := 0;

                    TipoCampo := tcPieGrupo;
                    iSubPos := GetPosicion;

               end
               else {CAMPO NORMAL}
               begin
                    iPosicion := GetPosicion;
                    TipoCampo := tcCampos;
               end;
          end
          else
          begin
               iPosicion := GetPosicion;
               TipoCampo := tcCampos;
          end;
          AgregaUnCampo;
     end;
end;

procedure AgregaCamposFijos( const iTabla: Integer; const sFijas: String );
begin
     FOffSet := 0;
     case FEntidad of
          enEmpleado, enKardex, enKarCurso, enPoliza, enAusencia, enAhorro, enPrestamo:
          begin
               if ( sFijas <> 'N' ) then
               begin
                    CamposFijos( GetPosicion, -1, 0, 'CB_CODIGO', 'COLABORA', VACIO, tgNumero, FEntidad, tcCampos );
                    //CamposFijos( GetPosicion, -1, 1, 'PRETTYNAME', 'COLABORA', 'COLABORA.CB_APE_PAT;COLABORA.CB_APE_MAT;COLABORA.CB_NOMBRES', tgTexto, enEmpleado, tcCampos );
                    CamposFijos( GetPosicion, -1, -1, K_PRETTY, '', VACIO, tgTexto, enEmpleado, tcCampos );
                    FOffSet := 2;
               end;
          end;
          enNomina:
          begin
               if ( iTabla = 5 ) then
               begin
                    CamposFijos( GetPosicion, -1, 0, 'PE_NUMERO', 'NOMINA', VACIO, tgNumero, FEntidad, tcCampos );
                    FOffSet := 1;
               end
               else
               if ( sFijas <> 'N' ) then
               begin
                    CamposFijos( GetPosicion, -1, 0, 'CB_CODIGO', 'COLABORA', VACIO, tgNumero, FEntidad, tcCampos );
                    //CamposFijos( GetPosicion, -1, 1, 'PRETTYNAME', 'COLABORA', 'COLABORA.CB_APE_PAT;COLABORA.CB_APE_MAT;COLABORA.CB_NOMBRES', tgTexto, enEmpleado, tcCampos );
                    CamposFijos( GetPosicion, -1, -1, K_PRETTY, '', VACIO, tgTexto, enEmpleado, tcCampos );
                    FOffSet := 2;
               end;
          end;
          enLiq_IMSS:
          begin
               CamposFijos( GetPosicion, -1, 0, 'LS_YEAR', 'LIQ_IMSS', VACIO, tgNumero, FEntidad, tcCampos );
               CamposFijos( GetPosicion, -1, 0, 'LS_MONTH', 'LIQ_IMSS', VACIO, tgNumero, FEntidad, tcCampos );
               CamposFijos( GetPosicion, -1, 0, 'LS_PATRON', 'LIQ_IMSS', VACIO, tgtexto, FEntidad, tcCampos );
               FOffSet := 3;
          end;
     end;
end;

 function ExisteCB_Codigo: Boolean;
 begin
      Result := FALSE;
      if FEntidad in  [ enEmpleado, enKardex, enKarCurso, enPoliza, enAusencia, enAhorro, enPrestamo, enNomina ] then
      begin
           with Source do
           begin
                Next;
                Result := Pos( 'CB_CODIGO', UpperCase( FieldByName( 'ES_VALORES' ).AsString ) ) > 0;
                Prior;
           end;
      end;
 end;

 function ReporteValido( const sReporte: String ): Boolean;
 const
      Arreglo: array[ 1..4 ] of String = ( 'DETA90A', 'DETA90B', 'IDENT90A', 'IDENT90B' );
 var
    i: Integer;
 begin
      Result := True;
      for  i := Low( Arreglo ) to High( Arreglo ) do
      begin
           Result := Result and ( sReporte <> Arreglo[ i ] );
      end;
 end;

var
   Es_Fijas,sQuery: String;
   iNumeroAncho: Integer;
begin //RepRHFilterRecord
     inherited;
     Accept := False; {Para Que Nunca se ejecute el ZetaMigrar.MoveExcluded }
     FEncabezado := FALSE;
     FMascaraNoCeros := FALSE;
     with Source do
     begin
          if ReporteValido( UpperCase( FieldByName( 'ES_CODIGO' ).AsString ) ) then
          begin
               FTipoSalida := GetTipoSalida;
               if ( FReporte = FieldByName( 'ES_CODIGO' ).AsString ) then
               begin
                    if FReporteOK then
                       CamposListado;
               end
               else
               begin
                    FReporte := FieldByName( 'ES_CODIGO' ).AsString;
                    FReporteOK := False;
                    lEsPoliza := ( FieldByName( 'ES_ANCHO' ).AsInteger = 6 );
                    if ZetaCommonTools.StrLleno( FieldByName( 'ES_TOKEN' ).AsString ) or lEsPoliza then
                    begin
                         if not lEsPoliza then
                            TablaMigrada.AgregaError( 'No Hay Encabezado Para El Reporte ' + FReporte );
                    end
                    else
                    begin
                         try
                            with tqTres do
                            begin
                                 Active := True;
                                 if IsEmpty then
                                    FCodigoReporte := 1
                                 else
                                     FCodigoReporte := Fields[ 0 ].AsInteger;
                                 Active := False;
                            end;
                            FEntidad := GetNumeroTabla( FieldByName( 'ES_ANCHO' ).AsInteger );
                            if ( FEntidad = enNinguno ) then
                               Bitacora.AddEvent( FReporte, 'No Puede Ser Migrado: No Es Compatible Con Esta Versi�n' )
                            else
                            begin
                                 tqUno.Active := False;
                                 sQuery := FieldByName( 'ES_QUERY' ).AsString;
                                 if ( Trim( sQuery ) = '*TODOS*' ) then
                                    sQuery := VACIO;
                                 ParamAsInteger( 'RE_CODIGO', FCodigoReporte, tqUno );
                                 ParamAsString( 'RE_NOMBRE', Limita( 'M_' + FNombreTabla  + FormatFloat('0000',FCodigoReporte) + ': ' + FieldByName( 'ES_CODIGO' ).AsString, 30 ), tqUno );
                                 ParamAsInteger( 'RE_TIPO', Ord( trListado ), tqUno );
                                 ParamAsString( 'RE_TITULO', Copy( FieldByName( 'ES_VALORES' ).AsString, 1, 50 ), tqUno );
                                 ParamAsInteger( 'RE_ENTIDAD', Ord( FEntidad ), tqUno );
                                 ParamAsString( 'RE_SOLOT', GetSoloTotales, tqUno );
                                 ParamAsInteger( 'RE_CLASIFI', Ord( FClasificacion ), tqUno );
                                 ParamAsDate( 'RE_FECHA', FieldByName( 'ES_FECHA' ).AsDateTime, tqUno );
                                 ParamAsString( 'RE_REPORTE', GetPlantilla, tqUno );
                                 ParamAsString( 'RE_PRINTER', GetImpresora, tqUno );
                                 ParamAsInteger( 'RE_PFILE',  FTipoSalida, tqUno );
                                 ParamAsString( 'RE_ARCHIVO', FieldByName( 'ES_ARCHIVO' ).AsString, tqUno );
                                 ParamAsInteger( 'US_CODIGO', 0, tqUno );
                                 ParamAsString( 'RE_FILTRO', AnalizaFormula( FieldByName( 'ES_PICTURE' ).AsString +
                                                                             FieldByName( 'ES_HEADER' ).AsString +
                                                                             Copy( FieldByName( 'ES_VALORES' ).AsString, 51, 100 ) +
                                                                             Copy( FieldByName( 'ES_VALORES' ).AsString, 181, 70 ), TRUE, iNumeroAncho ), tqUno );
                                 ParamAsInteger( 'RE_NIVEL', Str2Integer( Copy( FieldByName( 'ES_VALORES' ).AsString, 167, 1 ) ), tqUno );
                                 ParamAsString( 'QU_CODIGO', sQuery , tqUno );
                                 tqUno.ExecSQL;
                                 FPosicion := 0;
                                 GrupoListado;
                                 OrdenListado;
                                 FiltroListado;
                                 if ( FTipoSalida = Ord( tfASCIIFijo ) ) or ExisteCB_Codigo or ( FEntidad = enPoliza ) then
                                    Es_Fijas := 'N'
                                 else
                                     Es_Fijas := Trim( FieldByName( 'ES_FIJAS' ).AsString );
                                 AgregaCamposFijos( FieldByName( 'ES_ANCHO' ).AsInteger, Es_Fijas );
                                 FReporteOK := True;
                            end;
                         except
                               on Error: Exception do
                               begin
                                    TablaMigrada.HandleException( 'Error Al Agregar Encabezado Para El Reporte ' + FReporte , Error );
                               end;
                         end;
                    end;
               end;
          end;//if ReporteValido
     end;
end;

procedure TdmMigrarDatos.RepRHOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eREPORTES;
     FNombreTabla := 'ReporteRH';
//     FClasificacion := crEmpleados;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.RepTarjOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eREPTARJ;
     FNombreTabla := 'Checadas';
//     FClasificacion := crAsistencia;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.RepAsistOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eREPASIS;
     FNombreTabla := 'Asistencia';
//     FClasificacion := crAsistencia;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.RepCursosOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eREPCUR;
     FNombreTabla := 'Cursos';
//     FClasificacion := crEmpleados;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.RepSarOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eREPSAR;
     FNombreTabla := 'Imss';
//     FClasificacion := crPagosIMSS;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.EspecialOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eESPECIAL;
     FNombreTabla := 'Nominas';
//     FClasificacion := crNominas;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.RepSuperOpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eSUPERVISOR;
     FNombreTabla := 'Super';
//     FClasificacion := crSupervisor;
     ReportesOpenDataSets;
end;

procedure TdmMigrarDatos.Forma2OpenDatasets(Sender: TObject);
begin
     inherited;
     FBaseReporte := eFORMA;
     FNombreTabla := 'Formas';

     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'INSERT INTO REPORTE( ' +
                                      'RE_CODIGO, ' +
                                      'RE_NOMBRE, ' +
                                      'RE_TIPO, ' +
                                      'RE_TITULO, ' +
                                      'RE_ENTIDAD, ' +
                                      'RE_SOLOT, ' +
                                      'RE_CLASIFI, ' +
                                      'RE_FECHA, ' +
                                      'RE_REPORTE, ' +
                                      'RE_PRINTER, ' +
                                      'RE_PFILE, ' +
                                      'RE_ARCHIVO, ' +
                                      'US_CODIGO, ' +
                                      'RE_FILTRO, ' +
                                      'RE_NIVEL, ' +
                                      'QU_CODIGO, ' +
                                      'RE_ANCHO, ' +
                                      'RE_ALTO, ' +
                                      'RE_HOJA, ' +
                                      'RE_RENESPA, ' +
                                      'RE_COLESPA, ' +
                                      'RE_GENERAL, '+
                                      'RE_COLNUM, '+
                                      'RE_MAR_SUP '+
                                      ') VALUES (  ' +
                                      ':RE_CODIGO, ' +
                                      ':RE_NOMBRE, ' +
                                      ':RE_TIPO, ' +
                                      ':RE_TITULO, ' +
                                      ':RE_ENTIDAD, ' +
                                      ':RE_SOLOT, ' +
                                      ':RE_CLASIFI, ' +
                                      ':RE_FECHA, ' +
                                      ':RE_REPORTE, ' +
                                      ':RE_PRINTER, ' +
                                      ':RE_PFILE, ' +
                                      ':RE_ARCHIVO, ' +
                                      ':US_CODIGO, ' +
                                      ':RE_FILTRO, ' +
                                      ':RE_NIVEL, ' +
                                      ':QU_CODIGO, ' +
                                      ':RE_ANCHO, ' +
                                      ':RE_ALTO, ' +
                                      ':RE_HOJA, ' +
                                      ':RE_RENESPA, ' +
                                      ':RE_COLESPA, ' +
                                      ':RE_GENERAL, '+
                                      ':RE_COLNUM, '+
                                      ':RE_MAR_SUP )' ) ;

          PrepareTargetQuery( tqDos, 'select DI_TITULO, DI_TCORTO, DI_ANCHO, DI_MASCARA from DICCION ' +
                                     'where ( DI_CLASIFI = :Entidad ) AND ( DI_NOMBRE = :Campo )' );
          PrepareTargetQuery( tqTres, 'select MAX( RE_CODIGO ) + 1 from REPORTE' );
     end;
     FReporteOK := False;
     FReporte := '';
end;

procedure TdmMigrarDatos.Forma2FilterRecord(DataSet: TDataSet; var Accept: Boolean);

 function GetNumeroTabla( const sTabla: String ): TipoEntidad;
 begin
      FOffSet := 0;
      FMaster := enNinguno;
      if ( sTabla = '1' ) then
      begin
           Result := enKardex;
//           FClasificacion := crEmpleados;
      end
      else if ( sTabla = '2' ) then
      begin {Pendiente Importacion de Folios}
           FMaster := enNomina;
           case Str2Integer( Copy( Source.FieldByName( 'FO_DESCRIP').AsString, 1, 1 ) ) of
                2: Result := enMovimienBalanza;
                3: Result := enNomina
           else
               Result := enMovimien;
           end;
      end
      else if ( sTabla = '3' ) OR ( sTabla = 'Q' ) OR ( sTabla = 'N' ) then
      begin
           Result := enEmpleado; {Clasificacion Empleados}
//           if sTabla = 'N' then FClasificacion := crNominas
//           else FClasificacion := crEmpleados;
      end
      else if ( sTabla = '4' ) then
      begin
           Result := enVacacion;
//           FClasificacion := crEmpleados;
      end
      else if ( sTabla = '6' ) then
      begin
           FMaster := enNomina;
           Result := enMovimien; {NO_LIQUIDA = 'S'}
//           FClasificacion := crNominas;
      end
      else if ( sTabla = 'C' ) then
      begin
           FMaster := enNomina;
           Result := enAusencia; {+NOMINA}
//           FClasificacion := crNominas;
      end
      else if ( sTabla = 'D' ) then
      begin
           Result := enIncapacidad;
//           FClasificacion := crEmpleados;
      end
      else if ( sTabla = 'E' ) then
      begin
           Result := enConcepto;
//           FClasificacion := crNominas;
      end
      else if ( sTabla = 'H' ) then
      begin
           Result := enPermiso;
//           FClasificacion := crEmpleados;
      end
      else if ( sTabla = 'I' ) then
      begin
           Result := enKarCurso;
//           FClasificacion := crEmpleados;
      end
      else if ( sTabla = 'J' ) then
      begin
           FMaster := enEmpleado;
           Result := enKarCurso;
//           FClasificacion := crEmpleados;
      end
      else if ( sTabla = 'K' ) then
      begin
           Result := enLiq_Imss;
//           FClasificacion := crPagosIMSS;
      end
      else if ( sTabla = 'L' ) then
      begin
           FMaster := enLiq_Imss;
           Result := enLiq_Emp;
//           FClasificacion := crPagosIMSS;
      end
      else if ( sTabla = 'M' ) then
      begin
           FMaster := enLiq_Imss;
           Result := enLiq_Mov;
//           FClasificacion := crPagosIMSS;
      end
      else if ( sTabla = 'P' ) then
      begin
           Result := enLiq_Emp;
//           FClasificacion := crPagosIMSS;
      end
      else
          Result := enNinguno;
 end;

 procedure OrdenFormas( const sCampo: String; const iEntidad: TipoEntidad );
 begin
      AgregaCampo( GetPosicion,
                   -1,
                   0,
                   0,
                   0,
                   0,
                   0,
                   0,
                   0,
                   sCampo,
                   GetInfo( sCampo, iEntidad ).Titulo,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   tcOrden,
                   iEntidad,
                   FALSE );
 end;

 procedure FiltrosOrden;
 var
    sTabla: String;
 begin
      AgregaCampo( GetPosicion,
                   -1,
                   BoolToInt( TRUE ),
                   0,
                   BoolToInt( TRUE ),
                   BoolToInt( TRUE ),
                   0,
                   0,
                   BoolToInt( TRUE ),
                   K_EMPRESA,
                   K_EMPRESA,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   VACIO,
                   tcGrupos,
                   FEntidad,
                   TRUE );
      case FEntidad of
           enKardex:
           begin
                 if ( FReporte = 'A L T A' ) then
                 begin
                      FiltroFormas( rFechas, 'KARDEX.CB_FECHA', VACIO, VACIO, FEntidad, 0, tgFecha, 0  );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', K_T_ALTA, VACIO, FEntidad, Ord(enTKardex), tgTexto, 0 );
                      OrdenFormas( 'KARDEX.CB_FECHA', FEntidad );
                 end
                 else
                 if ( FReporte = 'B A J A' ) then
                 begin
                      FiltroFormas( rFechas, 'KARDEX.CB_FECHA_2', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', K_T_BAJA, VACIO, FEntidad, Ord(enTKardex), tgTexto, 0 );
                      OrdenFormas( 'KARDEX.CB_FECHA_2', FEntidad );
                 end
                 else
                 if ( FReporte = 'MODIFICA' ) then
                 begin
                      FiltroFormas( rFechas, 'KARDEX.CB_FECHA_2', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', K_T_CAMBIO, VACIO, FEntidad, Ord(enTKardex), tgTexto, 0 );
                      OrdenFormas( 'KARDEX.CB_FECHA_2', FEntidad );
                 end
                 else
                 if ( FReporte = 'RENOVACION' ) then
                 begin
                      FiltroFormas( rFechas, 'KARDEX.CB_FECHA', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', K_T_RENOVA, VACIO, FEntidad, Ord(enTKardex), tgTexto, 0 );
                      OrdenFormas( 'KARDEX.CB_FECHA', FEntidad );
                 end
                 else
                 if ( FReporte = 'RECONTRATA' ) then
                 begin
                      FiltroFormas( rFechas, 'KARDEX.CB_FECHA', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', K_T_BAJA,VACIO,  FEntidad, Ord(enTKardex), tgTexto, 0 );
                      FiltroFormas( rBool, 'KARDEX.CB_REINGRE', K_GLOBAL_SI, VACIO, FEntidad, 60, tgBooleano, 0 );
                      OrdenFormas( 'KARDEX.CB_FECHA', FEntidad );
                 end
                 else {LAS DEMAS FORMAS DE KARDEX}
                 begin
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_TIPO', VACIO, VACIO, FEntidad, Ord(enTKardex), tgTexto, 0 );
                      FiltroFormas( rFechas, 'KARDEX.CB_FECHA', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                      FiltroFormas( rRangoEntidad, 'KARDEX.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                      OrdenFormas( 'KARDEX.CB_FECHA', FEntidad );
                 end
           end;
           enMovimienBalanza, enMovimien, enNomina:
           begin {Pendiente Importacion de Folios}
                case FEntidad of
                     enMovimienBalanza:
                     begin
                          sTabla := 'TMPBALAN';
                          OrdenFormas( 'TMPBALAN.TZ_CODIGO', FEntidad );
                          if ( FMaster = enNomina ) then
                          begin
                               AgregaCampo( GetPosicion,
                                            -1,
                                            BoolToInt( TRUE ),
                                            0,
                                            BoolToInt( TRUE ),
                                            BoolToInt( TRUE ),
                                            0,
                                            0,
                                            BoolToInt( TRUE ),
                                            'TMPBALAN.CB_CODIGO',
                                            'N�mero',
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            tcGrupos,
                                            FEntidad,
                                            TRUE );
                          end;
                     end;
                     enMovimien:
                     begin
                          sTabla := 'MOVIMIEN';
                          if ( FMaster = enNomina ) then
                          begin
                               AgregaCampo( GetPosicion,
                                            -1,
                                            BoolToInt( TRUE ),
                                            0,
                                            BoolToInt( TRUE ),
                                            BoolToInt( TRUE ),
                                            0,
                                            0,
                                            BoolToInt( TRUE ),
                                            'MOVIMIEN.CB_CODIGO',
                                            'N�mero',
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            VACIO,
                                            tcGrupos,
                                            FEntidad,
                                            TRUE );
                          end;
                     end;
                     enNomina: sTabla := 'NOMINA';
                end;
                FiltroFormas( rRangoEntidad, sTabla + '.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, sTabla + '.PE_NUMERO', K_GLOBAL_SI, ARROBA_NUMERO, FEntidad, Ord(enPeriodo), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, sTabla + '.PE_YEAR', K_GLOBAL_SI, ARROBA_YEAR, FEntidad, -1, tgNumero, 0 );
                FiltroFormas( rRangoListas, sTabla + '.PE_TIPO', K_GLOBAL_SI, ARROBA_TIPO, FEntidad, Ord(lfTipoPeriodo), tgBooleano, 0 );
                OrdenFormas( sTabla + '.CB_CODIGO', FEntidad );
                if ( sTabla = '6' ) then
                begin
                     FiltroFormas( rNinguno, 'NOMINA.NO_LIQUIDA', '>0', VACIO, enNomina, 0, tgFloat, 0 );
                     OrdenFormas( 'MOVIMIEN.CO_NUMERO', FEntidad );
                     OrdenFormas( 'MOVIMIEN.MO_REFEREN', FEntidad );
                     {FALTA GRUPO PARA CB_CODIGO}
                end
                //else FiltroFormas( rNinguno, 'NOMINA.NO_NETO', '>0', VACIO, enNomina,0, tgFloat );
           end;
           enEmpleado: {Clasificacion Empleados}
           begin
                if FEsCarta then
                   FiltroFormas( rRangoEntidad, 'COLABORA.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 1 )
                else
                    FiltroFormas( rRangoEntidad, 'COLABORA.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                OrdenFormas( 'COLABORA.CB_CODIGO', FEntidad );
           end;
           enVacacion:
           begin
                FiltroFormas( rFechas, 'VACACION.VA_FECHA', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                FiltroFormas( rRangoEntidad, 'VACACION.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                OrdenFormas( 'VACACION.VA_FECHA', FEntidad );
           end;
           enAusencia: {+NOMINA}
           begin
                FiltroFormas( rRangoEntidad, 'AUSENCIA.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'AUSENCIA.PE_NUMERO', K_GLOBAL_SI, ARROBA_NUMERO, FEntidad, Ord(enPeriodo), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'AUSENCIA.PE_YEAR', K_GLOBAL_SI, ARROBA_YEAR, FEntidad, -1, tgNumero, 0 );
                FiltroFormas( rRAngoEntidad, 'AUSENCIA.PE_TIPO', K_GLOBAL_SI, ARROBA_TIPO, FEntidad, Ord(lfTipoPeriodo), tgNumero, 0 );
                FiltroFormas( rBool, 'COLABORA.CB_CHECA', K_GLOBAL_SI, VACIO, enEmpleado, 60, tgBooleano, 0 );
                OrdenFormas( 'AUSENCIA.CB_CODIGO', FEntidad );
                OrdenFormas( 'AUSENCIA.AU_FECHA', FEntidad );
                {FALRA GRUPO CB_CODIGO}
                if ( FMaster = enNomina ) then
                begin
                     AgregaCampo( GetPosicion,
                                  -1,
                                  BoolToInt( TRUE ),
                                  0,
                                  BoolToInt( TRUE ),
                                  BoolToInt( TRUE ),
                                  0,
                                  0,
                                  BoolToInt( TRUE ),
                                  'AUSENCIA.CB_CODIGO',
                                  'N�mero',
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  tcGrupos,
                                  FEntidad,
                                  TRUE );
                end;
           end;
           enIncapacidad:
           begin
                FiltroFormas( rFechas, 'INCAPACI.IN_FEC_INI', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                FiltroFormas( rRangoEntidad, 'INCAPACI.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                OrdenFormas( 'INCAPACI.IN_FEC_INI', FEntidad );
           end;
           enConcepto:
           begin
                FiltroFormas( rRangoEntidad, 'CONCEPTO.CO_NUMERO', VACIO, VACIO, FEntidad, Ord(enConcepto), tgNumero, 0 );
                FiltroFormas( rBool, 'CONCEPTO.CO_ACTIVO', K_GLOBAL_SI, VACIO, FEntidad, 60, tgBooleano, 0 );
                OrdenFormas( 'CONCEPTO.CO_NUMERO', FEntidad );
           end;
           enPermiso:
           begin
                FiltroFormas( rFechas, 'PERMISO.PM_FEC_INI', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                FiltroFormas( rRangoEntidad, 'PERMISO.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                OrdenFormas( 'PERMISO.PM_FEC_INI', FEntidad );
           end;
           enKarCurso:
           begin
                FiltroFormas( rFechas, 'KARCURSO.KC_FEC_TOM', VACIO, VACIO, FEntidad, 0, tgFecha, 0 );
                FiltroFormas( rRangoEntidad, 'KARCURSO.CB_CODIGO', VACIO, ARROBA_EMPLEADO, FEntidad, Ord(enEmpleado), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'KARCURSO.CU_CODIGO', VACIO, VACIO, FEntidad, Ord(enCurso), tgTexto, 0 );
                FiltroFormas( rRangoEntidad, 'KARCURSO.CB_PUESTO', VACIO, VACIO, FEntidad, Ord(enPuesto), tgTexto, 0 );
                if ( sTabla = 'J' ) then
                   OrdenFormas( 'KARCURSO.CB_CODIGO', FEntidad );
                OrdenFormas( 'KARCURSO.KC_FEC_TOM', FEntidad );
                if ( FMaster = enEmpleado ) then
                begin
                     AgregaCampo( GetPosicion,
                                  -1,
                                  BoolToInt( TRUE ),
                                  0,
                                  BoolToInt( TRUE ),
                                  BoolToInt( TRUE ),
                                  0,
                                  0,
                                  BoolToInt( TRUE ),
                                  'COLABORA.CB_CODIGO',
                                  'N�mero',
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  VACIO,
                                  tcGrupos,
                                  FEntidad,
                                  TRUE );
                end;
           end;
           enLiq_Imss:
           begin
                FiltroFormas( rRangoListas, 'LIQ_IMSS.LS_MONTH', K_GLOBAL_SI, ARROBA_IMSS_MES, FEntidad, Ord(lfMeses), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'LIQ_IMSS.LS_YEAR', K_GLOBAL_SI, ARROBA_IMSS_YEAR, FEntidad, -1, tgNumero, 0 );
                FiltroFormas( rRangoListas, 'LIQ_IMSS.LS_TIPO', K_GLOBAL_SI, ARROBA_IMSS_TIPO, FEntidad, Ord(lfTipoLiqIMSS), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'LIQ_IMSS.LS_PATRON', K_GLOBAL_SI, ARROBA_IMSS_PATRON, FEntidad, Ord(enRPatron), tgTexto, 0 );
                OrdenFormas( 'LIQ_IMSS.LS_MONTH', FEntidad );
           end;
           enLiq_Emp:
           begin
                FiltroFormas( rRangoListas, 'LIQ_EMP.LS_MONTH', K_GLOBAL_SI, ARROBA_IMSS_MES, FEntidad, Ord(lfMeses), tgNumero, 0 );
                FiltroFormas( rRangoEntidad,'LIQ_EMP.LS_YEAR', K_GLOBAL_SI, ARROBA_IMSS_YEAR, FEntidad, -1, tgNumero, 0 );
                FiltroFormas( rRangoListas,'LIQ_EMP.LS_TIPO', K_GLOBAL_SI, ARROBA_IMSS_TIPO, FEntidad, Ord(lfTipoLiqIMSS), tgNumero, 0 );
                FiltroFormas( rRangoEntidad,'LIQ_EMP.LS_PATRON', K_GLOBAL_SI, ARROBA_IMSS_PATRON, FEntidad, Ord(enRPatron), tgTexto, 0 );
                if ( sTabla = 'L' ) then
                   OrdenFormas( 'LIQ_EMP.LS_MONTH', FEntidad ); {GRUPO}
                OrdenFormas( 'LIQ_EMP.CB_CODIGO', FEntidad );
           end;
           enLiq_Mov:
           begin
                FiltroFormas( rRangoListas, 'LIQ_MOV.LM_TIPO', '0', VACIO, FEntidad, Ord(lfTipoLiqMov), tgTexto, 0 );
                FiltroFormas( rRangoListas, 'LIQ_MOV.LS_MONTH', K_GLOBAL_SI, ARROBA_IMSS_MES, FEntidad, Ord(lfMeses), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'LIQ_MOV.LS_YEAR', K_GLOBAL_SI, ARROBA_IMSS_YEAR, FEntidad, -1, tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'LIQ_MOV.LS_TIPO', K_GLOBAL_SI, VACIO, FEntidad, Ord(lfTipoLiqIMSS), tgNumero, 0 );
                FiltroFormas( rRangoEntidad, 'LIQ_MOV.LS_PATRON', K_GLOBAL_SI, ARROBA_IMSS_PATRON, FEntidad, Ord(enRPatron), tgTexto, 0 );
                OrdenFormas( 'LIQ_MOV.LS_MONTH', FEntidad ); {GRUPO}
                OrdenFormas( 'LIQ_MOV.CB_CODIGO', FEntidad );
                OrdenFormas( 'LIQ_MOV.LM_FECHA', FEntidad );
           end;
      end;
 end;

 procedure CamposFormas;
 var
    sMascara, sTitulo, sFormula, sCodigo: String;
    iBanda, iAlinea: Integer;

 function TransFormula( const sFormula: String ): String;

   function TransformaTexto( const sBusca, sTabla, sTexto: String; iPos: Integer ): String;
   begin
        Result := sTexto;
        while ( iPos > 0 ) do
        begin
             if ( iPos = 1 ) then
                Result := StrTransform( Result, sBusca, '@TABLA@.@CAMPO@')
             else
             begin
                  if ( Result[ iPos - 1 ] = '>' ) then
                     Result := StrTransform( Result, sBusca, '@CAMPO@')
                  else
                      Result := StrTransform( Result, sBusca, '@TABLA@.@CAMPO@')
             end;
             iPos := Pos( sBusca, Result );
        end;
        Result := StrTransAll( Result, '@CAMPO@', sBusca );
        Result := StrTransAll( Result, '@TABLA@', sTabla );
   end;

   function TransformaCampo( const Arreglo: array of String; const sTabla, sTexto: String ): String;
   var
      i, iPos: Integer;
   begin
        Result := sTexto;
        for i := Low(Arreglo) to High(Arreglo) do
        begin
             iPos := Pos(Arreglo[i],Result);
             Result := TransformaTexto(Arreglo[i],sTabla,Result, iPos);
        end;
   end;

   function TransformaCampoDias(const sTexto : string) : string;
   const
        NO_DIAS = 'NO_DIAS';
   var
      iPos: Integer;
   begin
        Result := sTexto;
        iPos := Pos( NO_DIAS, Result );
        if ( iPos > 0 ) and ( Result[ iPos + 7 ] <> '_' ) then
        begin
             Result := TransformaTexto( NO_DIAS, 'NOMINA', Result, iPos );
        end;
   end;

 begin { TransFormula }
      if ( Pos( '@', sFormula ) = 1 ) then
      begin
           Result := Copy( sFormula, 2, MAXINT );
           if iBanda in [ 2, 4 ] then
           begin
                case FMaster of
                     enEmpleado: Result := TransformaCampo( aEmpleados,
                                                            'COLABORA',
                                                            Result);
                     enNomina:
                     begin
                          Result := TransformaCampo( aNominas, 'NOMINA', Result );
                          Result := TransformaCampoDias( Result );
                     end;
                     enLiq_IMSS : Result := TransformaCampo( aLiqImss, 'LIQ_IMSS', Result );
                end;
           end;
      end
      else
          Result := EntreComillas( sFormula );
 end; { TransFormula }

 begin { CamposFormas }
      with Source do
      begin
           sCodigo := FieldByName( 'FO_INICIAL' ).AsString;
           iBanda := FieldByName( 'FO_POSICIO' ).AsInteger;
           sFormula := TransFormula( FieldByName( 'FO_FORMULA' ).AsString );
           sTitulo := FieldByName( 'FO_DESCRIP' ).AsString;
           if ZetaCommonTools.StrVacio( FieldByName( 'FO_ESTILO' ).AsString ) then
              iAlinea := 0
           else
               iAlinea := Ord( taRightJustify );
           if ZetaCommonTools.StrVacio( sTitulo ) then
              sTitulo := sFormula;
           AgregaCampo( GetPosicion,
                        FieldByName( 'FO_RENGLON' ).AsInteger,
                        -1,
                        FieldByName( 'FO_ANCHO' ).AsInteger,
                        Ord( tgAutomatico ),
                        FieldByName( 'FO_COLUMNA' ).AsInteger,
                        iAlinea,
                        iBanda - 2,
                        0,
                        sFormula,
                        sTitulo,
                        sMascara,
                        VACIO,
                        VACIO,
                        ZetaCommonTools.zBoolToStr( sCodigo = 'co_p_dblpr' ),
                        ZetaCommonTools.zBoolToStr( sCodigo = 'co_p_onita' ),
                        ZetaCommonTools.zBoolToStr( sCodigo = 'co_p_onund' ),
                        //zBoolToStr( ( sCodigo = 'co_p_pica' ) OR ( sCodigo = 'co_p_elite' ) OR ( sCodigo = 'co_p_compr' ) ),
                        K_GLOBAL_NO, //se pone en N para que a Nivel reporte se ponga el comprimido
                        tcCampos,
                        FEntidad,
                        FALSE );
      end;
 end; { CamposFormas }

 function GetNombrePlantilla( const sPlantilla: String ): String;
 const
      K_DOBLE_COMILLA = '"';
      K_QR_EXT = '.QR2';
 var
    i, j: Integer;
    sExt: String;
 begin
      Result := '';
      i := Pos( K_DOBLE_COMILLA, sPlantilla );
      if ( i > 0 ) then
      begin
           j := Pos( K_DOBLE_COMILLA, Copy( sPlantilla, ( i + 1 ), Length( sPlantilla ) ) );
           if ( j > 0 ) then
           begin
                Result := Trim( Copy( sPlantilla, i + 1, j - 1 ) );
                sExt := ExtractFileExt( Result );
                if ( sExt > '' ) then
                   Result := Copy( Result, 1, Pos( sExt, Result ) - 1 ) + K_QR_EXT
                else
                    Result := Result + K_QR_EXT;
                Bitacora.AddWarningList( K_CARTA_PLANTILLA, FReporte, Format( '%s -> %s, ', [ sPlantilla, Result ] ) );
           end;
      end;
 end;

var
   iRenglones, iEncabezado: Integer;
   sPlantilla, sImpresora: String;
begin //Forma2FilterRecord
     inherited;
     Accept := False; {Para Que Nunca se ejecute el ZetaMigrar.MoveExcluded }
     FMascaraNoCeros := FALSE;
     with Source do
     begin
          if ( FReporte = FieldByName( 'FO_CODIGO' ).AsString ) AND
             ( FTipoForma = FieldByName( 'FO_TIPO' ).AsString ) then
          begin
               if FReporteOK and not FEsCarta then
                  CamposFormas;
          end
          else
          begin
               FReporte := Trim(FieldByName( 'FO_CODIGO' ).AsString);
               FTipoForma := FieldByName( 'FO_TIPO' ).AsString;
               Next; //Para saber si no tiene columnas;
               if ( FReporte = FieldByName( 'FO_CODIGO' ).AsString ) AND
                  ( FTipoForma = FieldByName( 'FO_TIPO' ).AsString ) then
               begin
                    {Quiere decir que SI tiene mas columnas la Forma}
                    {Se tiene que Revisar que la Forma NO se una @CARTA}
                    FEsCarta := Pos( '@CARTA', UpperCase( FieldByName( 'FO_FORMULA' ).AsString ) ) > 0;
                    if FEsCarta then
                       sPlantilla := GetNombrePlantilla( FieldByName( 'FO_FORMULA' ).AsString );
                    Prior;//Si agrega el encabezado
                    FReporteOK := False;
                    if FEsCarta or ( FieldByName( 'FO_POSICIO' ).AsString  = '1' ) then
                    begin
                         try
                            with tqTres do
                            begin
                                 Active := True;
                                 if IsEmpty then
                                    FCodigoReporte := 1
                                 else
                                     FCodigoReporte := Fields[ 0 ].AsInteger;
                                 Active := False;
                            end;
                            FEntidad := GetNumeroTabla( FTipoForma );
                            if ( FEntidad = enNinguno ) then
                               Bitacora.AddEvent( FReporte, 'No Puede Ser Migrado: No es Compatible Con Esta Versi�n' )
                            else
                            begin
                                 sImpresora := Copy( FieldByName( 'FO_FORMULA' ).AsString, 101, 35 );
                                 if ZetaCommonTools.StrVacio( sImpresora ) then
                                    sImpresora := 'EPSON FX/LX/LQ';
                                 tqUno.Active := False;
                                 ParamAsInteger( 'RE_CODIGO' , FCodigoReporte, tqUno );
                                 ParamAsString( 'RE_NOMBRE', Limita( 'M_' + FNombreTabla + FormatFloat('0000',FCodigoReporte) + ' :' + FieldByName( 'FO_CODIGO' ).AsString, 30 )  , tqUno );
                                 ParamAsString( 'RE_TITULO', Copy( FieldByName( 'FO_MASCARA' ).AsString, 1, 50 ) , tqUno );
                                 ParamAsInteger( 'RE_ENTIDAD', Ord( FEntidad ), tqUno );
                                 ParamAsString( 'RE_SOLOT', VACIO, tqUno );
                                 ParamAsInteger( 'RE_CLASIFI', Ord( FClasificacion ), tqUno );
                                 ParamAsDate( 'RE_FECHA', Date, tqUno );
                                 ParamAsString( 'RE_PRINTER', sImpresora , tqUno );
                                 ParamAsInteger( 'RE_PFILE', Ord( tfImpresora ), tqUno );
                                 ParamAsString( 'RE_ARCHIVO', VACIO, tqUno );
                                 if FEsCarta then
                                 begin
                                      if ( sPlantilla = '' ) then
                                         raise Exception.Create( 'Funcion Carta' );
                                      ParamAsInteger( 'RE_TIPO', Ord( trForma ), tqUno );
                                      ParamAsString( 'RE_REPORTE', sPlantilla, tqUno );
                                 end
                                 else
                                 begin
                                      ParamAsInteger( 'RE_TIPO', Ord( trImpresionRapida ) , tqUno );
                                      ParamAsString( 'RE_REPORTE', FieldByName( 'FO_FINAL' ).AsString, tqUno );
                                 end;
                                 ParamAsInteger( 'US_CODIGO', 0 , tqUno );
                                 ParamAsString( 'RE_FILTRO', VACIO , tqUno );
                                 ParamAsInteger( 'RE_NIVEL', 0, tqUno );
                                 ParamAsString( 'QU_CODIGO', VACIO, tqUno );
                                 iRenglones := Str2Integer( Copy( FieldByName( 'FO_DESCRIP' ).AsString,22,3 ) );
                                 if ( iRenglones <= 0 ) then
                                    iRenglones := 66;
                                 ParamAsInteger( 'RE_ANCHO', iRenglones , tqUno ); { NUMERO DE LINEAS DE LA FORMA }
                                 ParamAsInteger( 'RE_ALTO', Str2Integer( Copy( FieldByName( 'FO_DESCRIP' ).AsString,25,3 ) ), tqUno ); {COMPESAR CADA X FORMAS}
                                 ParamAsInteger( 'RE_HOJA', Str2Integer( Copy( FieldByName( 'FO_DESCRIP' ).AsString,28,3 ) ), tqUno ); {COMPENSACION EN RENGLONES}
                                 iEncabezado := FieldByName( 'FO_RENGLON' ).AsInteger;
                                 if ( iEncabezado <= 0 ) then
                                    iEncabezado := iRenglones;
                                 ParamAsInteger( 'RE_RENESPA', iEncabezado, tqUno ); {RENGLON DONDE INICIA EL DETALLE}{LINEAS DE ENCABEZADO}
                                 ParamAsInteger( 'RE_COLESPA', FieldByName( 'FO_COLUMNA' ).AsInteger, tqUno ); {RENGLONES QUE MIDE EL DETALLE}
                                 ParamAsString( 'RE_GENERAL', FieldByName( 'FO_INICIAL' ).AsString, tqUno ); {CodigosImpresion};
                                 ParamAsInteger( 'RE_COLNUM', FieldByName( 'FO_ANCHO' ).AsInteger, tqUno ); {ESPACIO ENTRE RENGLONES}
                                 ParamAsInteger( 'RE_COLNUM', 64, tqUno );//Es el default para Lineas por pulgada - 6lineas
                                 ParamAsInteger( 'RE_MAR_SUP', 0, tqUno );//Es el default para caracteres por pulgada - 10cpi
                                 tqUno.ExecSQL;
                                 FPosicion := 0;
                                 FiltrosOrden;
                                 FReporteOK := True;
                            end;
                         except
                               on Error: Exception do
                               begin
                                    TablaMigrada.HandleException( 'Error Al Agregar Encabezado Para El Reporte ' + FReporte, Error );
                               end;
                         end;
                    end
                    else // IF FO_POSICION <> '1'
                    begin
                         Bitacora.AddEvent( FReporte, 'No Puede Ser Migrado: No Tiene Encabezado' )
                    end
               end;
          end
     end;
end;

procedure TdmMigrarDatos.PolizasOpenDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          PrepareTargetQuery( tqUno, 'insert into REPORTE ' +
                                     '( RE_CODIGO, RE_NOMBRE, RE_TIPO, RE_TITULO, RE_ENTIDAD, RE_CLASIFI ) values ' +
                                     '(:RE_CODIGO,:RE_NOMBRE,:RE_TIPO,:RE_TITULO,:RE_ENTIDAD,:RE_CLASIFI )' );
          PrepareTargetQuery( tqDos, 'select MAX( RE_CODIGO ) + 1 from REPORTE' );
     end;
     FReporteOK := False;
     FReporte := '';
end;

procedure TdmMigrarDatos.PolizasCloseDatasets(Sender: TObject);
begin
     inherited;
     with TablaMigrada do
     begin
          CloseQuery( tqDos );
          CloseQuery( tqUno );
     end;
end;

procedure TdmMigrarDatos.PolizasFilterRecord(DataSet: TDataSet; var Accept: Boolean);

function GetCriterio( const sCriterios: String; var iPos: Integer ): String;
const
     aCriterios: array[ 1..{$ifdef ACS}16{$else}13{$endif} ] of PChar = ( 'CB_NIVEL1',
                                             'CB_NIVEL2',
                                             'CB_NIVEL3',
                                             'CB_NIVEL4',
                                             'CB_NIVEL5',
                                             'CB_NIVEL6',
                                             'CB_NIVEL7',
                                             'CB_NIVEL8',
                                             'CB_NIVEL9',
                                             {$ifdef ACS}
                                             'CB_NIVEL10',
                                             'CB_NIVEL11',
                                             'CB_NIVEL12',
                                             {$endif}
                                             'CB_PUESTO',
                                             'CB_CLASIFI',
                                             'CB_TURNO',
                                             'CB_CONTRAT' );
var
   cValor: Char;
begin
     cValor := sCriterios[ iPos ];
     if ( cValor in [ '1'..{$ifdef ACS}'9'{$else}'9'{$endif} ] ) then
     begin
          Result := aCriterios[ iPos ];
          iPos := Ord( cValor ) - Ord( '0' );
     end
     else
     begin
          Result := '';
          iPos := 0;
     end;
end;

var
   i, iPos, iNumeroAncho: Integer;
   sReporte, sNombre, sValor, sToken, sOrden: String;
   Operacion: eTipoOperacionCampo;
begin
     inherited;
     Accept := False; {Para Que Nunca se ejecute el ZetaMigrar.MoveExcluded }
     FMascaraNoCeros := FALSE;
     with Source do
     begin
          sReporte := FieldByName( 'ES_CODIGO' ).AsString;
          sNombre := FieldByName( 'ES_VALORES' ).AsString;
          sToken := FieldByName( 'ES_TOKEN' ).AsString;
          sOrden := Trim( FieldByName( 'ES_GRUPO' ).AsString );
     end;
     if ( FReporte = sReporte ) then
     begin
          if FReporteOK then
          begin
               with Source do
               begin
                    if ( Trim( FieldByName( 'ES_TOTAL' ).AsString ) = 'A' ) then
                       Operacion := ocCuantos
                    else
                        Operacion := ocNinguno;
               end;
               FPosicion := FPosicion + 1;
               with TargetQuery do
               begin
                    ParamByName( 'RE_CODIGO' ).AsInteger := FCodigoReporte;
                    ParamByName( 'CR_POSICIO' ).AsInteger := FPosicion;
                    ParamByName( 'CR_SUBPOS' ).AsInteger := -1;
                    ParamByName( 'CR_TIPO' ).AsInteger := Ord( tcCampos );
                    ParamByName( 'CR_TITULO' ).AsString := Limita( Source.FieldByName( 'ES_HEADER' ).AsString, 30 );
                    ParamByName( 'CR_FORMULA' ).AsString := Limita( AnalizaFormula( Source.FieldByName( 'ES_VALORES' ).AsString, FALSE, iNumeroAncho ), K_ANCHO_MAXIMO );
                    ParamByName( 'CR_MASCARA' ).AsString := Limita( Source.FieldByName( 'ES_PICTURE' ).AsString, 20 );
                    ParamByName( 'CR_CALC' ).AsInteger := -1;
                    ParamByName( 'CR_OPER' ).AsInteger := Ord( Operacion );
                    ParamByName( 'CR_DESCRIP' ).AsString := Source.FieldByName( 'ES_POSICIO' ).AsString;
                    ExecSQL;
               end;
          end;
     end
     else
     begin
          FReporte := sReporte;
          FReporteOK := False;
          if ZetaCommonTools.StrLleno( sToken ) then
             Bitacora.AddEvent( FReporte, 'P�liza No Puede Ser Migrada: No Tiene Encabezado' )
          else
          begin
               try
                  with tqDos do
                  begin
                       Active := True;
                       if IsEmpty then
                          FCodigoReporte := 1
                       else
                           FCodigoReporte := Fields[ 0 ].AsInteger;
                       Active := False;
                  end;
                  with tqUno do
                  begin
                       Active := False;
                       ParamByName( 'RE_CODIGO' ).AsInteger := FCodigoReporte;
                       ParamByName( 'RE_NOMBRE' ).AsString := Limita( 'M_Poliza' + FormatFloat('0000',FCodigoReporte) + ':' + sNombre, 30 );
                       ParamByName( 'RE_TITULO' ).AsString := Limita( sValor, 100 );
                       ParamByName( 'RE_TIPO' ).AsInteger := Ord( trPoliza );
                       ParamByName( 'RE_ENTIDAD' ).AsInteger := Ord( enNomina );
                       ParamByName( 'RE_CLASIFI' ).AsInteger := Ord( FClasificacion );
                       ExecSQL;
                  end;
                  for i := 1 to Length( sOrden ) do
                  begin
                       iPos := i;
                       sValor := GetCriterio( sOrden, iPos );
                       if ( iPos > 0 ) then
                       begin
                            with TargetQuery do
                            begin
                                 ParamByName( 'RE_CODIGO' ).AsInteger := FCodigoReporte;
                                 ParamByName( 'CR_POSICIO' ).AsInteger := iPos;
                                 ParamByName( 'CR_SUBPOS' ).AsInteger := -1;
                                 ParamByName( 'CR_TIPO' ).AsInteger := Ord( tcOrden );
                                 ParamByName( 'CR_TITULO' ).AsString := 'Orden ' + IntToStr( iPos );
                                 ParamByName( 'CR_FORMULA' ).AsString := sValor;
                                 ParamByName( 'CR_MASCARA' ).AsString := '';
                                 ParamByName( 'CR_CALC' ).AsInteger := -1;
                                 ParamByName( 'CR_OPER' ).AsInteger := Ord( ocNinguno );
                                 ParamByName( 'CR_DESCRIP' ).AsString := '';
                                 ExecSQL;
                            end;
                       end;
                  end;
                  FPosicion := 0;
                  FReporteOK := True;
               except
                     on Error: Exception do
                     begin
                          TablaMigrada.HandleException( 'Error Al Agregar Encabezado Para La P�liza ' + FReporte, Error );
                     end;
               end;
          end;
     end;
end;

{
procedure TdmMigrarDatos.ActualizaConteo;
const
     Q_GET_GLOBALES_CONTEO = 'select GL_CODIGO, GL_FORMULA from GLOBAL where '+
                             '( GL_CODIGO between %d and %d ) or '+
                             '( GL_CODIGO between %d and %d ) '+
                             'order by GL_CODIGO';
     Q_UPDATE_DICCION = 'update DICCION set ' +
                        'DI_TITULO = ''%s'', '+
                        'DI_TCORTO = ''%s'','+
                        'DI_CLAVES = ''%s'', '+
                        'DI_NUMERO = %d '+
                        'where ( DI_CLASIFI = 124 ) and ( DI_NOMBRE = ''CT_NIVEL_%d'' )';
var
   aNiveles: array[ 1..9 ] of String;
   i, j: Integer;
   sDescrip: String;
   eConteo: eCamposConteo;

  function GetConteoDescrip( const eTipo: eCamposConteo ): String;
  begin
       case eTipo of
            coPuesto: Result := 'Puesto';
            coTurno: Result := 'Turno';
            coClasifi: Result := 'Clasificaci�n';
            coConfidencial: Result := 'Confidencialidad';
            coNivel1..coNivel9: Result := aNiveles[ Ord( eTipo ) - Ord( coNivel1 ) + 1 ];
       else
           Result := '';
       end;
  end;

  function GetConteoEntidad( const eTipo: eCamposConteo ): TipoEntidad;
  begin
       case eTipo of
          coPuesto: Result := enPuesto;
          coTurno: Result := enTurno;
          coClasifi: Result := enClasifi;
          coConfidencial: Result := enNivel0;
          coNivel1..coNivel9: Result := TipoEntidad( Ord( enNivel1 ) + Ord( eTipo ) - Ord( coNivel1 ) );
       else
           Result := enNinguno;
       end;
  end;

begin
     with Diccion do
     begin
          PrepareTargetQuery( tqUno, Format( Q_GET_GLOBALES_CONTEO, [ K_GLOBAL_NIVEL1,
                                                                      K_GLOBAL_NIVEL9,
                                                                      K_GLOBAL_CONTEO_NIVEL1,
                                                                      K_GLOBAL_CONTEO_NIVEL5 ] ) );
          i := 1;
          with tqUno do
          begin
               Active := TRUE;
               while not Eof and ( Fields[ 0 ].AsInteger <= K_GLOBAL_NIVEL9 ) do
               begin
                    aNiveles[ i ] := Fields[ 1 ].AsString;
                    Inc( i );
                    Next;
               end;
               i := 1;
               while not Eof and ( Fields[ 0 ].AsInteger > K_GLOBAL_NIVEL9 ) do
               begin
                    sDescrip := Fields[ 1 ].AsString;
                    if ZetaCommonTools.StrLleno( sDescrip ) then
                    begin
                         eConteo := eCamposConteo( StrToIntDef( sDescrip, 0 ) );
                         sDescrip := GetConteoDescrip( eConteo );
                         PrepareTargetQuery( tqDos, Format( Q_UPDATE_DICCION, [ sDescrip,
                                                                                sDescrip,
                                                                                UpperCase( sDescrip ),
                                                                                Ord( GetConteoEntidad( eConteo ) ),
                                                                                i ] ) );
                         tqDos.ExecSql;
                         CloseQuery( tqDos );
                         Inc( i );
                    end;
                    Next;
               end;
               for j := i  to  5 do
               begin
                    sDescrip := 'Criterio #' + IntToStr( j );
                    PrepareTargetQuery( tqDos, Format( Q_UPDATE_DICCION, [ sDescrip, sDescrip, UpperCase( sDescrip ), Ord( enNinguno ), j ] ) );
                    tqDos.ExecSql;
                    CloseQuery( tqDos );
               end;
          end;
          CloseQuery( tqUno );
     end;
end;
}

procedure TdmMigrarDatos.DiccionCloseDatasets(Sender: TObject);
begin
     inherited;
     spActualizaDiccion.ExecProc;
     { CV: 18-julio-2002 }
     { ya no se requiere la llamada,
       porque ya cambio el stored procedure de SP_REFRESH_DICCION
       el cual ya hace todo el trabajo }
     //ActualizaConteo; <-- ya se puede borrar.
end;

procedure TdmMigrarDatos.ConteoMoveExcludedFields(Sender: TObject);
begin
     inherited;
     with Source do
     begin
          MigrarFecha( 'CT_FECHA', FieldByName( 'CT_FECHA' ).AsDateTime );
     end;
     with TargetQuery do
     begin
          ParamByName( 'CT_NIVEL_1' ).AsString := Source.FieldByName( 'CT_NIVEL_1' ).AsString;
          ParamByName( 'CT_NIVEL_2' ).AsString := Source.FieldByName( 'CT_NIVEL_2' ).AsString;
          ParamByName( 'CT_NIVEL_3' ).AsString := Source.FieldByName( 'CT_NIVEL_3' ).AsString;
          ParamByName( 'CT_NIVEL_4' ).AsString := Source.FieldByName( 'CT_NIVEL_4' ).AsString;
          ParamByName( 'CT_NIVEL_5' ).AsString := Source.FieldByName( 'CT_NIVEL_5' ).AsString;
          ParamByName( 'CT_NUMERO1' ).AsFloat := Source.FieldByName( 'CT_NUMERO1' ).AsFloat;
          ParamByName( 'CT_NUMERO2' ).AsFloat := Source.FieldByName( 'CT_NUMERO2' ).AsFloat;
          ParamByName( 'CT_NUMERO3' ).AsFloat := Source.FieldByName( 'CT_NUMERO3' ).AsFloat;
          ParamByName( 'CT_TEXTO1' ).AsString := Source.FieldByName( 'CT_TEXTO1' ).AsString;
          ParamByName( 'CT_TEXTO2' ).AsString := Source.FieldByName( 'CT_TEXTO2' ).AsString;
          ParamByName( 'CT_REAL' ).AsFloat := Source.FieldByName( 'CT_REAL' ).AsFloat;
          ParamByName( 'CT_CUANTOS' ).AsInteger := Source.FieldByName( 'CT_CUANTOS' ).AsInteger;
     end;
end;

function TdmMigrarDatos.CierraBDReceptora: Boolean;
begin


     Result := False;
     try
        CloseDBSourceDatos;
        CloseDBTarget;


        Result := True;
     except
           on Error: Exception do
           begin
                Bitacora.HandleException( '� Error Al Cerrar Base De Datos !', Error );
           end;
     end;
end;


end.
