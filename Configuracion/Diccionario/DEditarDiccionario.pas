unit DEditarDiccionario;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, DBTables, ImgList, ActnList, StdCtrls,
     ZetaCommonLists,
     ZetaTipoEntidad;

const
     K_COLABORA_CODIGO = 'CB_CODIGO';
     K_COLABORA_NOMBRE = 'COLABORA.PRETTYNAME';
     K_EXPEDIENTE_CODIGO = 'EX_CODIGO';
     K_MATERIAL_CODIGO = 'MA_CODIGO';
     K_UNIDAD_CODIGO = 'UN_CODIGO';
     K_MATERIALGRUPO_CODIGO = 'GM_CODIGO';
     K_MONEDA_CODIGO = 'MO_CODIGO';
     K_OPERACIO_CODIGO = 'OP_CODIGO';
     K_TIPOPEDIMENTO_CODIGO = 'TP_CODIGO';

type
  TdmDiccionario = class(TDataModule)
    dbDiccionario: TDatabase;
    Entidades: TQuery;
    Q_General: TQuery;
    Q_Orden: TQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    function AbreQuery( const Texto: String ): TQuery;
    function GetScript(const iQuery: integer): String;
    procedure EjecutaQuery(const Texto: String);
    procedure PreparaQuery(Query: TQuery; const sTexto: String);
  public
    { Public declarations }
    function CargarTodosLosCampos: Boolean;
    function Conectar(const sAlias, sUsuario, sPassword: String): Boolean;
    procedure BorrarEntidad(const eEntidad: TipoEntidad);
    procedure CargarEntidades(Lista: TStrings; const Clasificacion: eClasifiReporte);
    procedure CargarInfoTabla;
    procedure DescargarEntidades(Lista: TStrings; const Clasificacion: eClasifiReporte);
    procedure DescargarInfoTabla;
    procedure Desconectar;
    procedure EditaEntidad( const EntidadTabla: TipoEntidad );
    procedure LeerAlias(Lista: TStrings);
    function SQLInfoTabla( const EntidadTabla: TipoEntidad ): string;

  end;

const
     K_CLASIFI = -1;
     Q_CAMPOS_ADD = 1;
     Q_CAMPOS_LEE = 2;
     Q_CAMPOS_CAMBIA = 3;
     Q_CAMPOS_DELETE = 4;
     Q_TABLA_NUEVA = 5;
     Q_TABLA_CAMBIA = 6;
     Q_TABLA_LEE = 7;
     Q_TABLA_DELETE = 8;
     Q_MAX_ORDEN = 9;
     Q_ENTIDADES_LEE = 10;
     Q_ENTIDADES_CAMBIA = 11;
     Q_TFIELDS = 12;
     Q_CAMPOS_ADD_SQL = 13;

var
   dmDiccionario: TdmDiccionario;

implementation

uses
    FInfoDiccionario,
    FEditaTabla,
    FEditaCampos,
    ZetaCommonTools,
    ZetaCommonClasses;

const
     K_CERO = 0;

var
   oInfoTabla: TinfoTabla;
   EditarTabla: TEditaTabla;

{$R *.DFM}

procedure TdmDiccionario.DataModuleCreate(Sender: TObject);
begin
     oInfoTabla := TInfoTabla.Create( Self );
     with Session do
     begin
          if not Active then
             Active := True;
          NetFileDir := ExtractFilePath( Application.ExeName );
          PrivateDir := ExtractFilePath( Application.ExeName );
     end;
end;

procedure TdmDiccionario.DataModuleDestroy(Sender: TObject);
begin
     FreeAndNil( oInfoTabla );
end;

procedure TdmDiccionario.LeerAlias( Lista: TStrings );
var
   OldConfigMode: TConfigMode;
begin
     with Session do
     begin
          OldConfigMode := ConfigMode;
          ConfigMode := [ cfmVirtual, cfmPersistent ];
          try
             GetAliasNames( Lista );
          finally
                 ConfigMode := OldConfigMode;
          end;
     end;
end;

function TdmDiccionario.GetScript( const iQuery: integer ): string;
begin
     case iQuery of
          Q_CAMPOS_ADD: Result:= 'insert into DICCION( DI_CLASIFI, DI_NOMBRE, DI_TITULO, DI_CALC, DI_ANCHO, DI_MASCARA, '+
                                                      'DI_TFIELD, DI_ORDEN, DI_REQUIER, DI_TRANGO, DI_NUMERO, DI_VALORAC, '+
                                                      'DI_RANGOAC, DI_CLAVES, DI_TCORTO, DI_CONFI ) '+
                                            'values ( :DI_CLASIFI, :DI_NOMBRE, :DI_TITULO, :DI_CALC, :DI_ANCHO, :DI_MASCARA, '+
                                                     ':DI_TFIELD, :DI_ORDEN, :DI_REQUIER, :DI_TRANGO, :DI_NUMERO, :DI_VALORAC, '+
                                                     ':DI_RANGOAC, :DI_CLAVES, :DI_TCORTO, :DI_CONFI )';
          Q_CAMPOS_LEE: Result := 'select DI_NOMBRE, DI_TITULO, DI_TCORTO, DI_CLAVES, DI_TFIELD, '+
                                  'DI_ANCHO, DI_MASCARA, DI_CONFI, DI_TRANGO, DI_NUMERO, DI_RANGOAC '+
                                  'from DICCION where ( DI_CLASIFI = %d ) ORDER BY DI_NOMBRE';
          Q_CAMPOS_CAMBIA: Result := 'update DICCION set DI_CLASIFI = %d, DI_NOMBRE = %s, DI_TITULO = %s, DI_CALC = %d, DI_ANCHO = %d, ' +
                                     'DI_MASCARA = %s, DI_TFIELD = %d, DI_ORDEN = %s, DI_REQUIER = %s, DI_TRANGO = %d, ' +
                                     'DI_NUMERO = %d, DI_VALORAC = %s, DI_RANGOAC = %d, DI_CLAVES = %s, DI_TCORTO = %s, DI_CONFI = %s '+
                                     'where ( DI_CLASIFI = %d ) And ( DI_NOMBRE = %s )';
          Q_CAMPOS_DELETE: Result := 'delete from DICCION where ( DI_CLASIFI = %d )';
          Q_TABLA_NUEVA: Result := 'insert into DICCION( DI_CLASIFI, '+
                                                        'DI_NOMBRE, '+
                                                        'DI_TITULO, '+
                                                        'DI_CALC, '+
                                                        'DI_ANCHO, '+
                                                        'DI_MASCARA, '+
                                                        'DI_TFIELD, '+
                                                        'DI_ORDEN, '+
                                                        'DI_REQUIER, '+
                                                        'DI_TRANGO, '+
                                                        'DI_NUMERO, '+
                                                        'DI_VALORAC, '+
                                                        'DI_RANGOAC, '+
                                                        'DI_CLAVES, '+
                                                        'DI_TCORTO, '+
                                                        'DI_CONFI ) values ( %d, %s, %s, %d, %d, %s, %d, %s, %s, %d, %d, %s, %d, %s, %s, %s )';
          Q_TABLA_CAMBIA: Result := 'update DICCION set DI_CLASIFI = %d, DI_NOMBRE = %s, DI_TITULO = %s, DI_CALC = %d, DI_ANCHO = %d, ' +
                                    'DI_MASCARA = %s, DI_ORDEN = %s, DI_REQUIER = %s, DI_TRANGO = %d, ' +
                                    'DI_NUMERO = %d, DI_VALORAC = %s, DI_RANGOAC = %d, DI_CLAVES = %s, DI_TCORTO = %s, DI_CONFI = %s '+
                                    'where ( DI_CLASIFI = %d ) And ( DI_CALC = %d )';
          Q_TABLA_LEE: Result := 'select DI_NOMBRE, DI_TITULO, DI_TCORTO, DI_CLAVES, DI_ANCHO, DI_TFIELD, DI_REQUIER '+
                                 'from DICCION where ( DI_CLASIFI = %d ) and ( DI_CALC = %d ) ORDER BY DI_NOMBRE';
          Q_TABLA_DELETE: Result := 'delete from DICCION where ( DI_CLASIFI = -1 ) and ( DI_CALC = %d )';
          Q_MAX_ORDEN: Result := 'select MAX( DI_TFIELD ) MAXIMO from DICCION where ( DI_CLASIFI = %d ) and ( DI_ANCHO = %d )';
          Q_ENTIDADES_LEE: Result := 'select DI_NOMBRE from DICCION where ( DI_ANCHO = %d ) And ( DI_CLASIFI = %d ) order by DI_TFIELD';
          Q_ENTIDADES_CAMBIA: Result := 'update DICCION set DI_TFIELD = :Orden where ( DI_ANCHO = %d ) and ( DI_NOMBRE = :Tabla ) and ( DI_CLASIFI = %d )';
          Q_TFIELDS: Result := 'select * from %s where ( 1 > 0 )';
          Q_CAMPOS_ADD_SQL: Result:= 'insert into DICCION( DI_CLASIFI, DI_NOMBRE, DI_TITULO, DI_CALC, DI_ANCHO, DI_MASCARA, '+
                                                      'DI_TFIELD, DI_ORDEN, DI_TRANGO, DI_NUMERO, DI_VALORAC, '+
                                                      'DI_RANGOAC, DI_CLAVES, DI_TCORTO, DI_CONFI ) '+
                                            'values ( %d, %s, %s, %d, %d, %s, '+
                                                    ' %d, %s, %d, %d, %s, '+
                                                    ' %d, %s, %s, %s );'




     else
         Result := VACIO;
     end;
end;

function TdmDiccionario.Conectar( const sAlias, sUsuario, sPassword: String ): Boolean;
begin
     try
        with dbDiccionario do
        begin
             Connected := False;
             AliasName := sAlias;
             with Params do
             begin
                  Values[ 'USER NAME' ] := sUsuario;
                  Values[ 'PASSWORD' ] := sPassword;
             end;
             Connected := True;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Desconectar;
                Application.HandleException( Error );
                Result := False;
           end;
     end;
end;

procedure TdmDiccionario.Desconectar;
begin
     with dbDiccionario do
     begin
          Connected := False;
     end;
end;

procedure TdmDiccionario.PreparaQuery( Query: TQuery; const sTexto: String );
begin
     with Query do
     begin
          if Active then
             Active := False;
          SQL.Clear;
          SQL.Text := sTexto;
          Prepare;
     end;
end;

procedure TdmDiccionario.EjecutaQuery(const Texto: String);
begin
     with Q_General do
     begin
          if Active then
             Active := False;
          SQL.Text := Texto;
          ExecSql;
     end;
end;

function TdmDiccionario.AbreQuery(const Texto: String): TQuery;
begin
     with Q_General do
     begin
          if Active then
             Active := False;
          SQL.Text := Texto;
          Open;
     end;
     Result := Q_General;
end;

procedure TdmDiccionario.CargarEntidades( Lista: TStrings; const Clasificacion: eClasifiReporte );
begin
     with Entidades do
     begin
          Active := FALSE;
          with SQL do
          begin
               Clear;
               Text := Format( GetScript( Q_ENTIDADES_LEE ), [ Ord( Clasificacion ) + 1, K_CLASIFI ] );
          end;
          Active := True;
          with Lista do
          begin
               BeginUpdate;
               try
                  while not EOF do
                  begin
                       Add( Fields[ 0 ].AsString );
                       Next;
                  end;
               finally
                      EndUpdate;
               end;
          end;
          Active := False;
     end;
end;

procedure TdmDiccionario.DescargarEntidades( Lista: TStrings; const Clasificacion: eClasifiReporte );
var
   i: Integer;
begin
     dbDiccionario.StartTransaction;
     try
        PreparaQuery( Q_Orden, Format( GetScript( Q_ENTIDADES_CAMBIA ), [ Ord( Clasificacion ) + 1, K_CLASIFI ] ) );
        with Lista do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  with Q_Orden do
                  begin
                       ParamByName( 'Orden' ).AsInteger := i + 1;
                       ParamByName( 'Tabla' ).AsString := Strings[ i ];
                       ExecSQL;
                  end;
             end;
        end;
        dbDiccionario.Commit;
     except
           On error : Exception do
           begin
                dbDiccionario.Rollback;
                Raise;
           end;
     end;
end;

procedure TdmDiccionario.BorrarEntidad( const eEntidad: TipoEntidad );
begin
     dbDiccionario.StartTransaction;
     try
        with Entidades do
        begin
             Active := False;
             with Sql do
             begin
                  Clear;
                  Text := Format( GetScript( Q_CAMPOS_DELETE ), [ Ord( eEntidad ) ] );
             end;
             ExecSQL;
             Active := False;
             with Sql do
             begin
                  Clear;
                  Text := Format( GetScript( Q_TABLA_DELETE ), [ Ord( eEntidad ) ] );
             end;
             ExecSQL;
             Active := False;
        end;
        dbDiccionario.Commit;
     except
           On error : Exception do
           begin
                dbDiccionario.Rollback;
                Raise;
           end;
     end;
end;

procedure TdmDiccionario.CargarInfoTabla;
begin
     AbreQuery( Format( GetScript( Q_TABLA_LEE ), [ K_CLASIFI, Ord( oInfoTabla.Entidad ) ] ) );
     with Q_General do
     begin
          if not IsEmpty then
          begin
               with oInfoTabla do
               begin
                    Nueva := False;
                    Tabla := FieldByName( 'DI_NOMBRE' ).AsString;
                    NombreCorto := FieldByName( 'DI_TCORTO' ).AsString;
                    Descripcion := FieldByName( 'DI_TITULO' ).AsString;
                    PalabrasClave := FieldByName( 'DI_CLAVES' ).AsString;
                    Clasificacion := eClasifiReporte( FieldByName( 'DI_ANCHO' ).AsInteger - 1 );
                    Orden := FieldByName( 'DI_TFIELD' ).AsInteger;
                    CargaRequeridos( FieldByName( 'DI_REQUIER' ).AsString );
               end;
          end;
          Active := False;
     end;
     AbreQuery( Format( GetScript( Q_CAMPOS_LEE ), [ Ord( oInfoTabla.Entidad ) ] ) );
     with Q_General do
     begin
          while not EOF do
          begin
               with oInfoTabla do
               begin
                    with AddCampo do
                    begin
                         Campo := FieldByName( 'DI_NOMBRE' ).AsString;
                         Descripcion := FieldByName( 'DI_TITULO' ).AsString;
                         NombreCorto := FieldByName( 'DI_TCORTO' ).AsString;
                         PalabrasClave := FieldByName( 'DI_CLAVES' ).AsString;
                         Tipo := eTipoGlobal( FieldByName( 'DI_TFIELD' ).AsInteger );
                         Ancho := FieldByName( 'DI_ANCHO' ).AsInteger;
                         Mascara := FieldByName( 'DI_MASCARA' ).AsString;
                         Confidencial := ZetaCommonTools.zStrToBool( FieldByName( 'DI_CONFI' ).AsString );
                         Rango := eTipoRango( FieldByName( 'DI_TRANGO' ).AsInteger );
                         Lookup := FieldByName( 'DI_NUMERO' ).AsInteger;
                         RangoActivo := eTipoRangoActivo( FieldByName( 'DI_RANGOAC' ).AsInteger );
                    end;
               end;
               Next;
          end;
          Active := False;
     end;
end;

procedure TdmDiccionario.DescargarInfoTabla;
var
   i, iOrden: Integer;
begin
     with oInfoTabla do
     begin
          dbDiccionario.StartTransaction;
          try
             if Nueva then
             begin
                  AbreQuery( Format( GetScript( Q_MAX_ORDEN ), [ K_CLASIFI, Ord( Clasificacion ) + 1 ] ) );
                  iOrden := Q_General.FieldByName( 'MAXIMO' ).AsInteger;
                  EjecutaQuery( Format( GetScript( Q_TABLA_NUEVA ), [ K_CLASIFI,                           //DI_CLASIFI
                                                                      EntreComillas( Tabla ) ,             //DI_NOMBRE
                                                                      EntreComillas( Descripcion ),        //DI_TITULO
                                                                      Ord( Entidad ),                      //DI_CALC
                                                                      Ord( Clasificacion ) + 1,            //DI_ANCHO
                                                                      EntreComillas( VACIO ),              //DI_MASCARA
                                                                      iOrden + 1,                          //DI_TFIELD
                                                                      EntreComillas( K_GLOBAL_NO ),        //DI_ORDEN
                                                                      EntreComillas( DescargaRequeridos ), //DI_REQUIER
                                                                      K_CERO,                              //DI_TRANGO
                                                                      K_CERO,                              //DI_NUMERO
                                                                      EntreComillas( VACIO ),              //DI_VALORAC
                                                                      K_CERO,                              //DI_RANGOAC
                                                                      EntreComillas( PalabrasClave ),      //DI_CLAVES
                                                                      EntreComillas( NombreCorto ),        //DI_TCORTO
                                                                      Entrecomillas( K_GLOBAL_NO ) ] ) );  //DI_CONFI
                  Nueva := FALSE;
             end
             else
             begin
                  EjecutaQuery( Format( GetScript( Q_TABLA_CAMBIA ), [ K_CLASIFI,
                                                                       EntreComillas( Tabla ) ,
                                                                       EntreComillas( Descripcion ),
                                                                       Ord( Entidad ),
                                                                       Ord( Clasificacion ) + 1,
                                                                       EntreComillas( VACIO ),
                                                                       EntreComillas( K_GLOBAL_NO ),
                                                                       EntreComillas( DescargaRequeridos ),
                                                                       K_CERO,
                                                                       K_CERO,
                                                                       EntreComillas( VACIO ),
                                                                       K_CERO,
                                                                       EntreComillas( PalabrasClave ),
                                                                       EntreComillas( NombreCorto ),
                                                                       Entrecomillas( K_GLOBAL_NO ),
                                                                       K_CLASIFI,
                                                                       Ord( Entidad ) ] ) );
             end;
             EjecutaQuery( Format( GetScript( Q_CAMPOS_DELETE ), [ Ord( Entidad ) ] ) );
             PreparaQuery( Q_General, GetScript( Q_CAMPOS_ADD ) );
             for i := 0 to ( Count - 1 ) do
             begin
                  with Q_General do
                  begin
                       with campo[ i ] do
                       begin
                            ParamByName( 'DI_CLASIFI' ).AsInteger := Ord( Entidad );
                            ParamByName( 'DI_NOMBRE' ).AsString := Campo;
                            ParamByName( 'DI_TITULO' ).AsString := Descripcion;
                            ParamByName( 'DI_CALC' ).AsInteger := K_CERO;
                            ParamByName( 'DI_ANCHO' ).AsInteger := Ancho;
                            ParamByName( 'DI_MASCARA' ).AsString := Mascara;
                            ParamByName( 'DI_TFIELD' ).AsInteger := Ord( Tipo );
                            ParamByName( 'DI_ORDEN' ).AsString := K_GLOBAL_SI;
                            ParamByName( 'DI_REQUIER' ).AsString := VACIO;
                            ParamByName( 'DI_TRANGO' ).AsInteger := Ord( Rango );
                            ParamByName( 'DI_NUMERO' ).AsInteger := Ord( Lookup );
                            ParamByName( 'DI_VALORAC' ).AsString := ValorAC;
                            ParamByName( 'DI_RANGOAC' ).AsInteger := Ord( RangoActivo );
                            ParamByName( 'DI_CLAVES' ).AsString := PalabrasClave;
                            ParamByName( 'DI_TCORTO' ).AsString := NombreCorto;
                            ParamByName( 'DI_CONFI' ).AsString := zBoolToStr( Confidencial );
                       end;
                       ExecSQL;
                  end;
             end;
             dbDiccionario.Commit;
          except
                On error : Exception do
                begin
                     dbDiccionario.Rollback;
                     Raise;
                end;
          end;
     end;
end;

function TdmDiccionario.SQLInfoTabla( const EntidadTabla: TipoEntidad ): string;
var
   i: Integer;
begin
     oInfoTabla.Cargar( EntidadTabla );
     with oInfoTabla do
     begin
          //Script de la TABLA
          Result := Format( GetScript( Q_TABLA_NUEVA ), [ K_CLASIFI,                           //DI_CLASIFI
                                                          EntreComillas( Tabla ) ,             //DI_NOMBRE
                                                          EntreComillas( Descripcion ),        //DI_TITULO
                                                          Ord( Entidad ),                      //DI_CALC
                                                          Ord( Clasificacion ) + 1,            //DI_ANCHO
                                                          EntreComillas( VACIO ),              //DI_MASCARA
                                                          Orden ,                              //DI_TFIELD
                                                          EntreComillas( K_GLOBAL_NO ),        //DI_ORDEN
                                                          EntreComillas( DescargaRequeridos ), //DI_REQUIER
                                                          K_CERO,                              //DI_TRANGO
                                                          K_CERO,                              //DI_NUMERO
                                                          EntreComillas( VACIO ),              //DI_VALORAC
                                                          K_CERO,                              //DI_RANGOAC
                                                          EntreComillas( PalabrasClave ),      //DI_CLAVES
                                                          EntreComillas( NombreCorto ),        //DI_TCORTO
                                                          Entrecomillas( K_GLOBAL_NO ) ] ) + ';' ;//DI_CONFI


          for i := 0 to ( Count - 1 ) do
          begin
               with campo[ i ] do
               begin
                    {
                         Q_CAMPOS_ADD_SQL: Result:= 'insert into DICCION( DI_CLASIFI, DI_NOMBRE, DI_TITULO, DI_CALC, DI_ANCHO, DI_MASCARA, '+
                                                      'DI_TFIELD, DI_ORDEN, DI_TRANGO, DI_NUMERO, DI_VALORAC, '+
                                                      'DI_RANGOAC, DI_CLAVES, DI_TCORTO, DI_CONFI ) '+
                                            'values ( %s, %s, %s, %s, %d, %s, '+
                                                    ' %s, %s, %d, %d, %s, '+
                                                    ' %d, %s, %s, %s )'
                                                                     }
                    Result := Result + CR_LF +
                              Format( GetScript( Q_CAMPOS_ADD_SQL ),
                                      [ Ord( Entidad ) , EntreComillas( Campo ), EntreComillas( Descripcion ), K_CERO, Ancho, EntreComillas( Mascara ),
                                        Ord( Tipo ) , EntreComillas( K_GLOBAL_SI ), Ord( Rango ), Ord( Lookup ), EntreComillas( ValorAC ),
                                        Ord( RangoActivo ) , EntreComillas( PalabrasClave ), EntreComillas( NombreCorto ), EntreComillas( zBoolToStr( Confidencial ) ) ] );
               end;
          end;
     end;
end;
procedure TdmDiccionario.EditaEntidad(const EntidadTabla: TipoEntidad);
begin
     oInfoTabla.Cargar( EntidadTabla );
     EditarTabla := TEditaTabla.Create( Self );
     try
        with EditarTabla do
        begin
             InfoTabla := oInfoTabla;
             ShowModal;
             if ( ModalResult = mrOk ) then
             begin
                  oInfoTabla.Descargar;
             end;
        end;
     finally
            FreeAndNil( EditarTabla );
     end;
end;

function TdmDiccionario.CargarTodosLosCampos: Boolean;
const
     K_USUARIO_CODIGO = 'US_CODIGO';
var
   i: Integer;
   sCampo: String;
begin
     Result := False;
     PreparaQuery( Q_General, Format( GetScript( Q_TFIELDS ), [ oInfoTabla.Tabla ] ) );
     with Q_General do
     begin
          Active := True;
          for i := 0 to ( FieldCount - 1 ) do
          begin
               sCampo := Fields[ i ].FieldName;
               with oInfoTabla do
               begin
                    if not ExisteCampo( sCampo ) then
                    begin
                         Result := True;
                         with AddCampo do
                         begin
                              Campo := sCampo;
                              Ancho := 0;
                              if ( sCampo = K_COLABORA_CODIGO ) then
                              begin
                                   {$IFDEF ADUANAS}
                                   Ancho := 6;
                                   Descripcion := 'Contribuyente';
                                   NombreCorto := 'Contribuyente';
                                   PalabrasClave := 'Contribuyente';
                                   {$ELSE}
                                   Descripcion := 'N�mero de Empleado';
                                   NombreCorto := 'N�mero';
                                   PalabrasClave := 'NUMERO EMPLEADO';
                                   {$ENDIF}
                              end
                              else if ( sCampo = K_USUARIO_CODIGO ) then
                              begin
                                       Descripcion := 'Usuario que Modific�';
                                       NombreCorto := 'Usuario';
                                       PalabrasClave := 'USUARIO MODIFICO';
                              end
                              else if (sCampo = K_MATERIAL_CODIGO) then
                              begin
                                   Ancho := 6;
                                   Descripcion := 'C�digo de material';
                                   NombreCorto := 'C�digo material';
                                   PalabrasClave := 'CODIGO MATERIAL';
                              end
                              else if (sCampo = K_UNIDAD_CODIGO) then
                              begin
                                   Ancho := 6;
                                   Descripcion := 'C�digo de unidad';
                                   NombreCorto := 'C�digo unidad';
                                   PalabrasClave := 'CODIGO UNIDAD';
                              end
                              else if (sCampo = K_MATERIALGRUPO_CODIGO) then
                              begin
                                   Ancho := 6;
                                   Descripcion := 'C�digo de grupo de material';
                                   NombreCorto := 'C�digo grupo material';
                                   PalabrasClave := 'CODIGO GRUPO MATERIAL';
                              end
                              else if (sCampo = K_MONEDA_CODIGO) then
                              begin
                                   Ancho := 6;
                                   Descripcion := 'C�digo de moneda';
                                   NombreCorto := 'C�digo moneda';
                                   PalabrasClave := 'CODIGO MONEDA';
                              end
                               else if (sCampo = K_OPERACIO_CODIGO) then
                              begin
                                   Ancho := 6;
                                   Descripcion := 'C�digo de operaci�n';
                                   NombreCorto := 'C�digo operaci�n';
                                   PalabrasClave := 'CODIGO OPERACION';
                              end
                              else if (sCampo = K_TIPOPEDIMENTO_CODIGO) then
                              begin
                                   Ancho := 6;
                                   Descripcion := 'C�digo de tipo de pedimento';
                                   NombreCorto := 'C�digo tipo pedimento';
                                   PalabrasClave := 'CODIGO TIPO PEDIMENTO';
                              end;
                              case Fields[ i ].DataType of
                                   ftString,ftMemo,ftBlob,ftFmtMemo: Tipo := tgTexto;
                                   ftSmallint, ftInteger, ftWord: Tipo := tgNumero;
                                   ftBoolean: Tipo := tgBooleano;
                                   ftFloat, ftCurrency, ftBCD, ftTime : Tipo := tgFloat;
                                   ftDate,ftDateTime: Tipo := tgFecha;
                              else
                                  Tipo := tgAutomatico;
                              end;
                              if (Ancho <> 0) then
                                 Ancho := FInfoDiccionario.GetDefAncho( Tipo );
                              Mascara := FInfoDiccionario.GetDefMascara( Tipo );
                              Rango := FInfoDiccionario.GetDefRango( Tipo );
                              RangoActivo := FInfoDiccionario.GetDefRangoActivo( Tipo );
                         end;
                    end;
               end;
          end;
          Active := False;
     end;
end;

end.
