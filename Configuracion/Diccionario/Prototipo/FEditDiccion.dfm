object EditDiccion: TEditDiccion
  Left = 37
  Top = 130
  Width = 988
  Height = 561
  Caption = 'EditDiccion'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 313
    Width = 980
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 316
    Width = 980
    Height = 211
    Align = alClient
    DataSource = dsDiccion
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'DI_CLASIFI'
        Title.Caption = 'Tab'
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_TFIELD'
        Title.Caption = 'Tipo'
        Width = 25
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_NOMBRE'
        Title.Caption = 'Nombre'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_TITULO'
        Width = 151
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_TFIELD'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_CLAVES'
        Width = 128
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_TCORTO'
        Width = 113
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_ANCHO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_MASCARA'
        Width = 82
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_TRANGO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_NUMERO'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DI_RANGOAC'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 980
    Height = 41
    Align = alTop
    TabOrder = 1
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 201
      Height = 25
      Caption = 'Esconder Panel Superior'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Edit1: TEdit
      Left = 216
      Top = 10
      Width = 225
      Height = 21
      TabOrder = 1
      Text = 'TRESS DATOS'
    end
    object Button2: TButton
      Left = 448
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Conectar BD'
      TabOrder = 2
      OnClick = Button2Click
    end
    object RadioGroup1: TRadioGroup
      Left = 544
      Top = 5
      Width = 265
      Height = 30
      Caption = 'Mostrar'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Solo Tablas'
        'Solo Campos')
      TabOrder = 3
      OnClick = RadioGroup1Click
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 41
    Width = 980
    Height = 272
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Agregar un Campo'
      object Label1: TLabel
        Left = 155
        Top = 66
        Width = 64
        Height = 13
        Caption = 'DI_NOMBRE'
        FocusControl = DI_NOMBRE
      end
      object Label2: TLabel
        Left = 163
        Top = 91
        Width = 56
        Height = 13
        Caption = 'DI_TITULO'
        FocusControl = DI_TITULO
      end
      object Label3: TLabel
        Left = 164
        Top = 165
        Width = 55
        Height = 13
        Caption = 'DI_ANCHO'
        FocusControl = DI_ANCHO
      end
      object Label4: TLabel
        Left = 150
        Top = 191
        Width = 69
        Height = 13
        Caption = 'DI_MASCARA'
        FocusControl = DI_MASCARA
      end
      object Label6: TLabel
        Left = 161
        Top = 113
        Width = 58
        Height = 13
        Caption = 'DI_CLAVES'
        FocusControl = DI_CLAVES
      end
      object Label7: TLabel
        Left = 157
        Top = 138
        Width = 62
        Height = 13
        Caption = 'DI_TCORTO'
        FocusControl = DI_TCORTO
      end
      object Label8: TLabel
        Left = 163
        Top = 40
        Width = 56
        Height = 13
        Caption = 'DI_CLASIFI'
        FocusControl = DI_CLASIFI2
      end
      object Label17: TLabel
        Left = 444
        Top = 44
        Width = 63
        Height = 13
        Caption = 'DI_TRANGO'
      end
      object Label18: TLabel
        Left = 442
        Top = 68
        Width = 65
        Height = 13
        Caption = 'DI_NUMERO'
        FocusControl = DI_NUMERO
      end
      object Label19: TLabel
        Left = 440
        Top = 92
        Width = 67
        Height = 13
        Caption = 'DI_VALORAC'
        FocusControl = DI_VALORAC
      end
      object Label20: TLabel
        Left = 437
        Top = 116
        Width = 70
        Height = 13
        Caption = 'DI_RANGOAC'
      end
      object Label5: TLabel
        Left = 728
        Top = 112
        Width = 103
        Height = 20
        Caption = 'Rango Abierto'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object DI_NOMBRE: TDBEdit
        Left = 231
        Top = 62
        Width = 134
        Height = 21
        CharCase = ecUpperCase
        DataField = 'DI_NOMBRE'
        DataSource = dsDiccion
        TabOrder = 0
      end
      object DI_TITULO: TDBEdit
        Left = 231
        Top = 87
        Width = 134
        Height = 21
        DataField = 'DI_TITULO'
        DataSource = dsDiccion
        TabOrder = 1
      end
      object DI_ANCHO: TDBEdit
        Left = 231
        Top = 161
        Width = 134
        Height = 21
        DataField = 'DI_ANCHO'
        DataSource = dsDiccion
        TabOrder = 2
      end
      object DI_MASCARA: TDBEdit
        Left = 231
        Top = 187
        Width = 134
        Height = 21
        DataField = 'DI_MASCARA'
        DataSource = dsDiccion
        TabOrder = 3
      end
      object DI_CLAVES: TDBEdit
        Left = 231
        Top = 109
        Width = 134
        Height = 21
        DataField = 'DI_CLAVES'
        DataSource = dsDiccion
        TabOrder = 4
      end
      object DI_TCORTO: TDBEdit
        Left = 231
        Top = 134
        Width = 134
        Height = 21
        DataField = 'DI_TCORTO'
        DataSource = dsDiccion
        TabOrder = 5
      end
      object DI_CLASIFI2: TDBEdit
        Left = 376
        Top = 36
        Width = 33
        Height = 21
        DataField = 'DI_CLASIFI'
        DataSource = dsDiccion
        TabOrder = 6
      end
      object DBNavigator1: TDBNavigator
        Left = 8
        Top = 3
        Width = 240
        Height = 25
        DataSource = dsDiccion
        TabOrder = 7
      end
      object DI_TIFIELD: TDBRadioGroup
        Left = 8
        Top = 28
        Width = 137
        Height = 100
        Caption = 'Tipo de Campo'
        DataField = 'DI_TFIELD'
        DataSource = dsDiccion
        Items.Strings = (
          'Booleano'
          'Con Decimales'
          'Entero'
          'Fecha'
          'Texto')
        TabOrder = 8
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4')
      end
      object DI_NUMERO: TDBEdit
        Left = 519
        Top = 64
        Width = 134
        Height = 21
        DataField = 'DI_NUMERO'
        DataSource = dsDiccion
        TabOrder = 9
      end
      object DI_VALORAC: TDBEdit
        Left = 519
        Top = 88
        Width = 134
        Height = 21
        DataField = 'DI_VALORAC'
        DataSource = dsDiccion
        TabOrder = 10
      end
      object DI_TRANGO: TComboBox
        Left = 519
        Top = 40
        Width = 134
        Height = 21
        ItemHeight = 13
        TabOrder = 11
        OnClick = DI_TRANGOClick
        Items.Strings = (
          'Ninguna'
          'Requisici�n'
          'Solicitud'
          'Candidato'
          'Entrevista'
          'Examen'
          'Cliente'
          'Condiciones'
          'Puesto'
          'AreaInteres'
          'Gasto'
          'TipoGasto'
          'TablaOpcion1'
          'TablaOpcion2'
          'TablaOpcion3'
          'TablaOpcion4'
          'TablaOpcion5'
          'Funciones'
          'Formula'
          'Cat�logo de Usuarios'
          'Datos B�sicos de Empresa'
          'Reportes'
          'Detalle de Reportes'
          'Bitacora'
          'Proceso'
          'Globales del Sistema'
          'Diccionario de Datos'
          'Referencias Laborales')
      end
      object TipoFiltro: TRadioGroup
        Left = 8
        Top = 132
        Width = 137
        Height = 105
        Caption = 'Tipo de Filtro?'
        Items.Strings = (
          'Lookup a otra Tabla'
          'Lista Fija'
          'Ninguna de las Dos')
        TabOrder = 12
      end
      object DI_RANGOAC: TComboBox
        Left = 519
        Top = 112
        Width = 134
        Height = 21
        ItemHeight = 13
        TabOrder = 13
        OnClick = DI_RANGOACClick
        Items.Strings = (
          'lfNinguna'
          'lfNivelUsuario'
          'lfTipoBitacora'
          'lfSexoDesc'
          'lfFuturo'
          'lfMotivoBaja'
          'lfTipoOtrasPer'
          'lfIMSSOtrasPer'
          'lfISPTOtrasPer'
          'lfIncidencias'
          'lfIncidenciaIncapacidad'
          'lfRPatronModulo'
          'lfTipoTurno'
          'lfTipoJornada'
          'lfTipoPeriodo'
          'lfUsoPeriodo'
          'lfStatusPeriodo'
          'lfTipoNomina'
          'lfTipoFestivo'
          'lfTipoHorario'
          'lfTipoReporte'
          'lfTipoRangoActivo'
          'lfOperacionConflicto'
          'lfStatusLectura'
          'lfClasifiReporte'
          'lfTipoCampo'
          'lfTipoOpera'
          'lfTipoOperacionCampo'
          'lfWorkStatus'
          'lfTipoLectura '
          'lfTipoCedula'
          'lfRangoFechas'
          'lfJustificacion'
          'lfIncluyeEvento'
          'lfTipoGlobal'
          'lfTipoConcepto'
          'lfMotivoFaltaDias'
          'lfMotivoFaltaHoras'
          'lfStatusAhorro'
          'lfStatusPrestamo'
          'lfStatusAusencia'
          'lfTipoDiaAusencia'
          'lfTipoChecadas'
          'lfDescripcionChecadas'
          'lfTipoPariente'
          'lfTipoInfonavit'
          'lfTipoVacacion'
          'lfStatusKardex'
          'lfMotivoIncapacidad'
          'lfFinIncapacidad'
          'lfTipoPermiso'
          'lfTipoLiqIMSS'
          'lfCampoCalendario'
          'lfTipoLiqMov'
          'lfCafeCalendario'
          'lfTipoAhorro'
          'lfHorasExtras'
          'lfMotTools'
          'lfBancaElectronica'
          'lfYears'
          'lfDescuentoTools'
          'lfMeses'
          'lfZonaGeografica'
          'lfSexo'
          'lfCodigoLiqMov'
          'lfDiasSemana'
          'lfMes13'
          'lfTipoRango'
          'lfLiqNomina'
          'lfAutorizaChecadas'
          'lfAltaAhorroPrestamo'
          'lfStatusEmpleado'
          'lfTipoFormato'
          'lfExtFormato'
          'lfFormatoFormas'
          'lfFormatoASCII'
          'lfEmailFrecuencia'
          'lfEmailFormato'
          'lfEmailNotificacion'
          'lfTipoCompanyName'
          'lfTipoCalculo'
          'lfCamposConteo'
          'lfHorasDias'
          'lfImportacion'
          'lfOperacionMontos '
          'lfTipoBanda'
          'lfPuertoSerial'
          'lfTipoGlobalAuto'
          'lfClaseBitacora'
          'lfProcesos'
          'lfRegla3x3'
          'lfTipoCompany'
          'lfPrioridad'
          'lfReqStatus'
          'lfMotivoVacante'
          'lfEstudios'
          'lfSolStatus'
          'lfEdoCivil'
          'lfEdoCivilDesc'
          'lfCanStatus'
          'lfEntStatus'
          'lfTipoExamen')
      end
      object DI_TRANGO2: TDBEdit
        Left = 656
        Top = 40
        Width = 57
        Height = 21
        DataField = 'DI_TRANGO'
        DataSource = dsDiccion
        Enabled = False
        TabOrder = 14
      end
      object DI_RANGOAC2: TDBEdit
        Left = 656
        Top = 112
        Width = 57
        Height = 21
        DataField = 'DI_RANGOAC'
        DataSource = dsDiccion
        Enabled = False
        TabOrder = 15
      end
      object DI_CLASIFI: TComboBox
        Left = 231
        Top = 36
        Width = 138
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 16
        OnClick = DI_CLASIFIClick
        Items.Strings = (
          'Ninguna'
          'Requisici�n'
          'Solicitud'
          'Candidato'
          'Entrevista'
          'Examen'
          'Cliente'
          'Condiciones'
          'Puesto'
          'AreaInteres'
          'Gasto'
          'TipoGasto'
          'TablaOpcion1'
          'TablaOpcion2'
          'TablaOpcion3'
          'TablaOpcion4'
          'TablaOpcion5'
          'Funciones'
          'Formula'
          'Cat�logo de Usuarios'
          'Datos B�sicos de Empresa'
          'Reportes'
          'Detalle de Reportes'
          'Bitacora'
          'Proceso'
          'Globales del Sistema'
          'Diccionario de Datos'
          'Referencias Laborales'
          ' ')
      end
      object DBNavigator2: TDBNavigator
        Left = 152
        Top = 212
        Width = 240
        Height = 25
        DataSource = dsDiccion
        TabOrder = 17
      end
      object BitBtn1: TBitBtn
        Left = 400
        Top = 212
        Width = 75
        Height = 25
        Caption = '&Agregar'
        TabOrder = 18
        OnClick = BitBtn1Click
      end
      object BitBtn2: TBitBtn
        Left = 480
        Top = 212
        Width = 75
        Height = 25
        Caption = '&Prior'
        TabOrder = 19
      end
      object BitBtn3: TBitBtn
        Left = 560
        Top = 212
        Width = 75
        Height = 25
        Caption = '&Next'
        TabOrder = 20
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Agregar Tablas'
      ImageIndex = 1
      object Label10: TLabel
        Left = 360
        Top = 42
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre Tabla'
        FocusControl = DBEdit2
      end
      object Label11: TLabel
        Left = 399
        Top = 67
        Width = 28
        Height = 13
        Alignment = taRightJustify
        Caption = 'T�tulo'
        FocusControl = DBEdit3
      end
      object Label12: TLabel
        Left = 351
        Top = 89
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Palabras Claves'
        FocusControl = DBEdit4
      end
      object Label14: TLabel
        Left = 368
        Top = 109
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci�n'
      end
      object Label15: TLabel
        Left = 387
        Top = 133
        Width = 40
        Height = 13
        Alignment = taRightJustify
        Caption = 'Posici�n'
        FocusControl = DBEdit7
      end
      object Label16: TLabel
        Left = 600
        Top = 104
        Width = 96
        Height = 13
        Caption = 'Clasificaci�n Default'
      end
      object BitBtn4: TBitBtn
        Left = 600
        Top = 40
        Width = 185
        Height = 25
        Caption = '&1.- Defaults'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        OnClick = BitBtn4Click
      end
      object LbTablas: TListBox
        Left = 16
        Top = 24
        Width = 321
        Height = 153
        ItemHeight = 13
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 431
        Top = 38
        Width = 134
        Height = 21
        CharCase = ecUpperCase
        DataField = 'DI_NOMBRE'
        DataSource = dsDiccion
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 431
        Top = 61
        Width = 134
        Height = 21
        DataField = 'DI_TITULO'
        DataSource = dsDiccion
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 431
        Top = 84
        Width = 134
        Height = 21
        CharCase = ecUpperCase
        DataField = 'DI_CLAVES'
        DataSource = dsDiccion
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 431
        Top = 129
        Width = 134
        Height = 21
        DataField = 'DI_TFIELD'
        DataSource = dsDiccion
        TabOrder = 4
      end
      object BitBtn5: TBitBtn
        Left = 600
        Top = 72
        Width = 185
        Height = 25
        Caption = '&2.- Grabar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnClick = BitBtn5Click
      end
      object DBNavigator3: TDBNavigator
        Left = 600
        Top = 8
        Width = 240
        Height = 25
        DataSource = dsDiccion
        TabOrder = 5
      end
      object BitBtn6: TBitBtn
        Left = 16
        Top = 192
        Width = 185
        Height = 49
        Caption = '3.- Mostrar Tablas por clasificaci�n y ordenadas'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        OnClick = BitBtn6Click
      end
      object cbClasifiDef: TComboBox
        Left = 600
        Top = 120
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 9
        Text = 'cbClasifiDef'
        Items.Strings = (
          'Ninguna'
          'Empleados'
          'Asistencia'
          'N�minas'
          'Pagos IMSS'
          'Consultas'
          'Cat�logos'
          'Tablas'
          'Supervisores'
          'Cafeter�a'
          'Migraci�n'
          'Cursos'
          'Labor'
          'Servicio M�dico')
      end
      object ComboBox1: TComboBox
        Left = 432
        Top = 105
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 10
        Text = 'cbClasifiDef'
        OnChange = ComboBox1Change
        Items.Strings = (
          'Ninguna'
          'Empleados'
          'Asistencia'
          'N�minas'
          'Pagos IMSS'
          'Consultas'
          'Cat�logos'
          'Tablas'
          'Supervisores'
          'Cafeter�a'
          'Migraci�n'
          'Cursos'
          'Labor'
          'Servicio M�dico')
      end
    end
  end
  object Database1: TDatabase
    DatabaseName = 'dbSeleccion'
    Params.Strings = (
      'USER NAME=SYSDBA')
    SessionName = 'Default'
    Left = 3
    Top = 34
  end
  object tblDiccion: TTable
    AfterPost = tblDiccionAfterPost
    OnNewRecord = tblDiccionNewRecord
    DatabaseName = 'dbSeleccion'
    IndexFieldNames = 'DI_CLASIFI;DI_NOMBRE'
    TableName = 'DICCION'
    Left = 36
    Top = 32
    object tblDiccionDI_CLASIFI: TSmallintField
      FieldName = 'DI_CLASIFI'
    end
    object tblDiccionDI_NOMBRE: TStringField
      FieldName = 'DI_NOMBRE'
      OnChange = tblDiccionDI_NOMBREChange
      Size = 10
    end
    object tblDiccionDI_TITULO: TStringField
      FieldName = 'DI_TITULO'
      OnChange = tblDiccionDI_TITULOChange
      Size = 30
    end
    object tblDiccionDI_CALC: TSmallintField
      FieldName = 'DI_CALC'
    end
    object tblDiccionDI_ANCHO: TSmallintField
      FieldName = 'DI_ANCHO'
    end
    object tblDiccionDI_MASCARA: TStringField
      FieldName = 'DI_MASCARA'
      Size = 30
    end
    object tblDiccionDI_TFIELD: TSmallintField
      FieldName = 'DI_TFIELD'
      OnChange = tblDiccionDI_TFIELDChange
    end
    object tblDiccionDI_ORDEN: TStringField
      FieldName = 'DI_ORDEN'
      FixedChar = True
      Size = 1
    end
    object tblDiccionDI_REQUIER: TStringField
      FieldName = 'DI_REQUIER'
      Size = 255
    end
    object tblDiccionDI_TRANGO: TSmallintField
      FieldName = 'DI_TRANGO'
    end
    object tblDiccionDI_NUMERO: TSmallintField
      FieldName = 'DI_NUMERO'
    end
    object tblDiccionDI_VALORAC: TStringField
      FieldName = 'DI_VALORAC'
      Size = 30
    end
    object tblDiccionDI_RANGOAC: TSmallintField
      FieldName = 'DI_RANGOAC'
    end
    object tblDiccionDI_CLAVES: TStringField
      FieldName = 'DI_CLAVES'
      Size = 30
    end
    object tblDiccionDI_TCORTO: TStringField
      FieldName = 'DI_TCORTO'
      Size = 30
    end
    object tblDiccionDI_CONFI: TStringField
      FieldName = 'DI_CONFI'
      FixedChar = True
      Size = 1
    end
  end
  object dsDiccion: TDataSource
    DataSet = tblDiccion
    OnDataChange = dsDiccionDataChange
    Left = 64
    Top = 32
  end
end
