inherited SQLServerDBLookup: TSQLServerDBLookup
  Left = 279
  Top = 316
  ActiveControl = Server
  Caption = 'Escoja Una Base De Datos De SQL Server'
  ClientHeight = 86
  ClientWidth = 329
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ServerLBL: TLabel [0]
    Left = 35
    Top = 9
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = '&Servidor:'
    FocusControl = Server
  end
  object BuscarServidor: TSpeedButton [1]
    Left = 287
    Top = 5
    Width = 23
    Height = 21
    Hint = 'Buscar Servidor de Microsoft SQL Server'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333FF33333333FF333993333333300033377F3333333777333993333333
      300033F77FFF3333377739999993333333333777777F3333333F399999933333
      33003777777333333377333993333333330033377F3333333377333993333333
      3333333773333333333F333333333333330033333333F33333773333333C3333
      330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
      993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
      333333333337733333FF3333333C333330003333333733333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = BuscarServidorClick
  end
  object Label1: TLabel [2]
    Left = 4
    Top = 32
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = '&Base de Datos:'
    FocusControl = Server
  end
  inherited PanelBotones: TPanel
    Top = 50
    Width = 329
    BevelOuter = bvNone
    inherited OK: TBitBtn
      Left = 161
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 246
    end
  end
  object Server: TEdit
    Left = 79
    Top = 5
    Width = 206
    Height = 21
    TabOrder = 1
    OnExit = ServerExit
  end
  object DBList: TComboBox
    Left = 79
    Top = 28
    Width = 205
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object Hdbc: THdbc
    Driver = 'SQL Server'
    UserName = 'sa'
    Attributes.Strings = (
      'Server=')
    ConnectionPooling = cpDefault
    Version = '5.06'
    Left = 52
    Top = 52
  end
  object OEQuery: TOEQuery
    hDbc = Hdbc
    hStmt.Target.Target = (
      'OE5.06'
      ''
      ''
      ''
      '')
    SQL.Strings = (
      'select DB_NAME(dbid) as DB_NAME'
      'from MASTER.DBO.SYSDATABASES where '
      '( DB_NAME( DBID ) <> '#39'master'#39' )'
      'order by DB_NAME')
    Params = <>
    Left = 20
    Top = 52
  end
  object OEAdministrator: TOEAdministrator
    DataSourceType = dsSystem
    Driver = 'SQL Server'
    Left = 83
    Top = 52
  end
end
