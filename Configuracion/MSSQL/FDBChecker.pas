unit FDBChecker;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, ExtCtrls, DB, DBTables,
     ODSI, OCL, OCI,
     FAutoServer,
     ZetaRegistryServer,
     ZBaseDlgModal;

type
  TDBChecker = class(TZetaDlgModal)
    ResultadoGB: TGroupBox;
    Resultados: TMemo;
    HdbcComparte: THdbc;
    HdbcEmpresa: THdbc;
    oeCompany: TOEQuery;
    oeVersion: TOEQuery;
    ckDigito: TCheckBox;
    oeDigito: TOEQuery;
    oeUpdDigito: TOEQuery;
    oeSysAdmin: TOEQuery;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HdbcBeforeConnect(Sender: TObject);
  private
    { Private declarations }
    FError: Boolean;
    FAutoServer: TAutoServer;
    FRefrescarShell: Boolean;
    function GetRegistry: TZetaRegistryServer;
    function ODBCOpenDB(Database: THdbc; const sNombre, sServerDatabase, sUserName, sPassword: String; const lComparte: Boolean = FALSE ): Boolean;
    function TestODBC: Boolean;
    procedure AgregaTexto(Lista: TStrings; sTexto: String);
    procedure WriteLog(const sMensaje: String);
    procedure WriteError(const sMensaje: String; Error: Exception);
    procedure CleanResultados;
    procedure SetDigitoEmpresa( const sEmpresa: String );
    procedure PreparaQuery( OECursor: TOEQuery; const sScript: String );
    function GetVersionODBC(Database: THdbc; const lComparte: Boolean ): String;
    function ActualizarDigitoEmpresa: Boolean;
  protected
    { Protected declarations }
    property Registry: TZetaRegistryServer read GetRegistry;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
    property RefrescarShell: Boolean read FRefrescarShell;
  end;

var
  DBChecker: TDBChecker;

implementation

uses DDBConfig,
     FAutoClasses,
     ZetaODBCTools,
     ZetaWinAPITools,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZGlobalTress,
     ZetaDialogo;

{$R *.DFM}

const
     SQL_COMPANYS = 'select CM_CODIGO, CM_NOMBRE, CM_ALIAS, CM_PASSWRD, CM_USRNAME, CM_DATOS, CM_DIGITO, CM_CONTROL, CM_NIVEL0 from COMPANY order by CM_NOMBRE';
     SQL_DIGITO_EMPRESA = 'select GL_FORMULA as DIGITO from GLOBAL where ( GL_CODIGO = %d )';
     SQL_IS_SYSADMIN = 'select IS_SRVROLEMEMBER (''sysadmin'', ''%s'' ) as IS_MEMBER';
     UPDATE_DIGITO = 'update COMPANY set CM_DIGITO = :CM_DIGITO where CM_CODIGO = :Codigo';
     aVersionScripts: array[ FALSE..TRUE ] of pChar = ( 'select GL_FORMULA as VERSION from GLOBAL where ( GL_CODIGO = %d )',
                                                        'select CL_TEXTO as VERSION from CLAVES where ( CL_NUMERO = %d )' );
     aVersionPosicion: array[ FALSE..TRUE ] of Integer = ( K_GLOBAL_VERSION_DATOS, K_POS_CLAVES_VERSION );

{ ******** TDBChecker ******** }

procedure TDBChecker.FormShow(Sender: TObject);
begin
     inherited;
     FRefrescarShell := FALSE;
     HelpContext := H50012_Probando_el_acceso;
end;

function TDBChecker.GetRegistry: TZetaRegistryServer;
begin
     Result := DDBConfig.dmDBConfig.Registry;
end;

procedure TDBChecker.CleanResultados;
begin
     with Resultados.Lines do
     begin
          BeginUpdate;
          Clear;
          EndUpdate;
     end;
end;
procedure TDBChecker.AgregaTexto( Lista: TStrings; sTexto: String );
var
   pValor, pStart: PChar;
   sValor: String;
begin
     with Lista do
     begin
          BeginUpdate;
          try
             pValor := Pointer( sTexto );
             if ( pValor <> nil ) then
             begin
                  while ( pValor^ <> #0 ) do
                  begin
                       pStart := pValor;
                       while not ( pValor^ in [ #0, #10, #13 ] ) do
                       begin
                            Inc( pValor );
                       end;
                       SetString( sValor, pStart, ( pValor - pStart ) );
                       Add( sValor );
                       if ( pValor^ = #13 ) then
                          Inc( pValor );
                       if ( pValor^ = #10 ) then
                          Inc( pValor );
                  end;
             end;
          finally
                 EndUpdate;
          end;
     end;
end;

procedure TDBChecker.WriteLog( const sMensaje: String );
begin
     with Resultados.Lines do
     begin
          BeginUpdate;
          Add( sMensaje );
          EndUpdate;
     end;
end;

procedure TDBChecker.WriteError( const sMensaje: String; Error: Exception );
begin
     FError := True;
     WriteLog( StringOfChar( '-', 40 ) );
     WriteLog( sMensaje );
     AgregaTexto( Resultados.Lines, ZetaCommonTools.GetExceptionInfo( Error ) );
end;

function TDBChecker.ActualizarDigitoEmpresa: Boolean;
begin
     Result := ckDigito.Checked;
end;

procedure TDBChecker.SetDigitoEmpresa( const sEmpresa: String );
var
   sDigito: String;
begin
     sDigito := VACIO;
     with oeDigito do
     begin
          Active := True;
          try
             if not IsEmpty then
                sDigito := FieldByName( 'DIGITO' ).AsString;
          finally
             Active := False;
          end;
     end;
     if strLleno( sDigito ) then
     begin
          with hdbcComparte do
          begin
               StartTransact;
               try
                  try
                     with oeUpdDigito do
                     begin
                          ParamByName( 'Codigo' ).AsString := sEmpresa;
                          ParamByName( 'CM_DIGITO' ).AsString := sDigito;
                          ExecSQL;
                     end;
                     Commit;
                     WriteLog( Format( 'Se Actualiz� D�gito En Gafetes -> %s', [ sDigito ] ) );
                     FRefrescarShell := TRUE;
                  except
                        on Error: Exception do
                        begin
                             Rollback;
                             WriteError( Format( 'ERROR Al Configurar D�gito de Empresa a %s', [ sEmpresa ] ), Error );
                        end;
                  end;
               finally
                  EndTransact;
               end;
          end;
     end;
end;

function TDBChecker.ODBCOpenDB(Database: THdbc; const sNombre, sServerDatabase, sUserName, sPassword: String; const lComparte: Boolean = FALSE ): Boolean;
var
   sServer, sDatabase: String;
   lSysAdmin: Boolean;
begin
     Result := False;
     ZetaServerTools.GetServerDatabase( sServerDatabase, sServer, sDatabase );
     try
        with Database do
        begin
             Connected := False;
             Driver := MSSQL_DRIVER;
             with Attributes do
             begin
                  Clear;
                  Add( Format( 'Server=%s', [ sServer ] ) );
                  Add( Format( 'Database=%s', [ sDatabase ] ) );
                  Add( Format( 'Language=%s', [ 'us_english' ] ) );
             end;
             UserName := sUserName;
             Password := sPassword;
             Connected := True;
             Result := Connected;
        end;
        WriteLog( 'OK por ODBC -> ' + sNombre + '  ' + GetVersionODBC( DataBase, lComparte ) );
     except
           on Error: Exception do
           begin
                WriteError( 'ERROR ODBC: Empresa ' + sNombre + ' ( ' + sServerDatabase + ' )', Error );
           end;
     end;
     if Result then
     begin
          try
             with oeSysAdmin do
             begin
                  Active := False;
                  Hdbc := Database;
                  PreparaQuery( oeSysAdmin, Format( SQL_IS_SYSADMIN, [ sUserName ] ) );
                  Active := True;
                  lSysAdmin := ( Fields[ 0 ].AsInteger = 1 );
                  Active := False;
             end;
             if not lSysAdmin then
             begin
                  WriteLog( '' );
                  WriteLog( Format( 'ERROR: Empresa %s ( %s )', [ sNombre, sServerDatabase ] ) );
                  WriteLog( Format( 'El Usuario "%s" No Es Miembro del SYSADMIN (Rol Administrativo de SQL Server)', [ sUserName ] ) );
                  WriteLog( '' );
             end;
          except
                on Error: Exception do
                begin
                     WriteError( Format( 'ERROR ODBC: Error Al Verificar Pertenencia De "%s" Al SYSADMIN', [ sUserName ] ), Error );
                end;
          end;
     end;
end;

function TDBChecker.GetVersionODBC( Database: THdbc; const lComparte: Boolean ): String;
const
     MENSAJE_VERSION = '( V. %s )';
begin
     try
        with oeVersion do
        begin
             Active := False;
             Hdbc := DataBase;
             with SQL do
             begin
                  Clear;
                  Add( Format( aVersionScripts[ lComparte ], [ aVersionPosicion[ lComparte ] ] ) );
             end;
             Active := True;
             if not IsEmpty then
                Result := Format( MENSAJE_VERSION, [ FieldByName( 'VERSION' ).AsString ] )
             else
                Result := Format( MENSAJE_VERSION, [ 'Indefinida' ] );
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Result := '...ERROR Al Leer Versi�n de Datos Por ODBC';
           end;
     end;
end;

procedure TDBChecker.PreparaQuery( OECursor: TOEQuery; const sScript: String );
begin
     with OECursor do
     begin
          Active := False;
          with SQL do
          begin
               Clear;
               Add( sScript );
          end;
     end;
end;

function TDBChecker.TestODBC: Boolean;
var
   lOk : Boolean;
begin
     WriteLog( '------- Probando ODBC ------- ' );
     with Registry do
     begin
          Result := ODBCOpenDB( hdbcComparte, 'Comparte', Database, UserName, Password, TRUE );
     end;
     if Result then
     begin
          try
             PreparaQuery( oeCompany, SQL_COMPANYS );
             oeCompany.Active := True;
             WriteLog( 'Lectura De Empresas Por ODBC Tuvo Exito' );
             if ActualizarDigitoEmpresa then
             begin
                  PreparaQuery( oeDigito, Format( SQL_DIGITO_EMPRESA, [ ZGlobalTress.K_GLOBAL_DIGITO_EMPRESA ] ) );
                  PreparaQuery( oeUpdDigito, UPDATE_DIGITO );
             end;
          except
                on Error: Exception do
                begin
                     WriteError( 'ERROR Al Leer Empresas Por ODBC', Error );
                     Result := False;
                end;
          end;
          if Result then
          begin
               with oeCompany do
               begin
                    while not Eof do
                    begin
                         lOk := ODBCOpenDB( hdbcEmpresa,
                                            FieldByName( 'CM_NOMBRE' ).AsString,
                                            FieldByName( 'CM_DATOS' ).AsString,
                                            FieldByName( 'CM_USRNAME' ).AsString,
                                            ZetaServerTools.Decrypt( FieldByName( 'CM_PASSWRD' ).AsString ) );
                         if lOk and ActualizarDigitoEmpresa and
                            strVacio( FieldByName( 'CM_DIGITO' ).AsString ) and
                            strVacio( FieldByName( 'CM_NIVEL0' ).AsString ) and
                            ( DDBConfig.dmDBConfig.GetTipoCompanyName( FieldByName( 'CM_CONTROL' ).AsString ) = tc3Datos ) then    // Solo Empresas de Datos de Tress
                            SetDigitoEmpresa( FieldByName( 'CM_CODIGO' ).AsString );    // Ya est�n abiertas las bases de datos de Comparte y Empresa
                         Result := lOk and Result;
                         Next;
                    end;
               end;
          end;
     end;
     if Result then
        WriteLog( '** Acceso A Bases De Datos Por ODBC Tuvo Exito **' );
end;

procedure TDBChecker.OKClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        CleanResultados;
        FError := False;
        TestODBC;
        if FError then
           ZetaDialogo.zError( '� Hay Problemas !', 'Hay Problemas En El Acceso A Las Bases De Datos', 0 )
        else
        begin
             ZetaDialogo.zInformation( '� Exito !', 'El Acceso A Las Bases De Datos Tuvo Exito', 0 );
             Cancelar.SetFocus;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TDBChecker.HdbcBeforeConnect(Sender: TObject);
const
     SQL_PRESERVE_CURSORS = 1204;
     SQL_PC_ON = 1;
begin
{$ifdef MSSQL}
     SQLSetConnectAttr(THdbc( Sender ).Handle, SQL_PRESERVE_CURSORS, Pointer(SQL_PC_ON), 0);
{$endif}
end;

end.
