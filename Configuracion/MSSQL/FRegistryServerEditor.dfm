inherited ServerRegistryEditor: TServerRegistryEditor
  Left = 463
  Top = 377
  Caption = 'Especificar Base De Datos de Microsoft SQL Server'
  ClientHeight = 144
  ClientWidth = 387
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object UserNameLBL: TLabel [0]
    Left = 49
    Top = 8
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = '&Usuario:'
    FocusControl = UserName
  end
  object PasswordLBL: TLabel [1]
    Left = 4
    Top = 29
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = '&Clave de Acceso:'
    FocusControl = Password
  end
  object PasswordCheckLBL: TLabel [2]
    Left = 24
    Top = 51
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'C&onfirmaci'#243'n:'
    FocusControl = PasswordCheck
  end
  object DatabaseLBL: TLabel [3]
    Left = 15
    Top = 73
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = '&Base de Datos:'
  end
  object BuscarDatabase: TSpeedButton [4]
    Left = 338
    Top = 69
    Width = 23
    Height = 21
    Hint = 'Buscar Base De Datos de Microsoft SQL Server'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333FF33333333FF333993333333300033377F3333333777333993333333
      300033F77FFF3333377739999993333333333777777F3333333F399999933333
      33003777777333333377333993333333330033377F3333333377333993333333
      3333333773333333333F333333333333330033333333F33333773333333C3333
      330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
      993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
      333333333337733333FF3333333C333330003333333733333777333333333333
      3000333333333333377733333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = BuscarDatabaseClick
  end
  object Database: TZetaTextBox [5]
    Left = 90
    Top = 71
    Width = 245
    Height = 18
    AutoSize = False
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object TipoLoginLBL: TLabel [6]
    Left = 35
    Top = 94
    Width = 53
    Height = 13
    Caption = 'Tipo de Login:'
  end
  inherited PanelBotones: TPanel
    Top = 116
    Width = 387
    Height = 28
    BevelOuter = bvNone
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 221
      Top = 2
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 306
      Top = 2
      OnClick = CancelarClick
    end
  end
  object UserName: TEdit
    Left = 90
    Top = 3
    Width = 123
    Height = 21
    TabOrder = 0
  end
  object Password: TEdit
    Left = 90
    Top = 25
    Width = 123
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
    OnChange = PasswordChange
  end
  object PasswordCheck: TEdit
    Left = 90
    Top = 47
    Width = 123
    Height = 21
    PasswordChar = '*'
    TabOrder = 2
  end
  object TipoLogin: TZetaKeyCombo
    Left = 90
    Top = 91
    Width = 272
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 3
    Text = 'Login a Tress'
    Items.Strings = (
      'Login a Tress'
      'Active Directory'
      'Active Directory con Opci'#243'n a Login Tress')
    ListaFija = lfNinguna
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
  end
end
