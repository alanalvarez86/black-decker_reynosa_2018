unit DReporteadorCliente;

interface

uses
  SysUtils, Classes,
  DCliente,
  DBasicoCliente,
  ReporteadorDD_TLB,
  DBClient,Variants,
  ZetaClientDataSet,
  ZetaTipoEntidad,
  ZetaCommonLists,
  ZetaCommonTools,
  ZetaCommonClasses
     ;

type

  IdmServerDiccionarioDisp = IdmServerReporteadorDDDisp;


  TdmReporteadorCliente = class(TDataModule)
  private
    { Private declarations }
    FParametros: TZetaParams;
    FServidor: IdmServerDiccionarioDisp;
    function GetServerDiccionario: IdmServerDiccionarioDisp;

  public
    { Public declarations }
  end;

var
  dmReporteadorCliente: TdmReporteadorCliente;

implementation

{$R *.dfm}

{ TdmReporteadorDD }

function TdmReporteadorCliente.GetServerDiccionario: IdmServerDiccionarioDisp;
begin
     Result := IdmServerDiccionarioDisp(dmCliente.CreaServidor( CLASS_dmServerReporteadorDD, FServidor) );
end;

end.
