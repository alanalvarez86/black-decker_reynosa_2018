unit DConfigurarEspeciales;
//Unidad que aplica solo para MSSQL CORPORATIVO

{$ifdef MSSQL}
//{$define USAR_ADO_SCRIPTS}
{$define EJECUTAR_SCRIPTS_SOLOS}
{$endif}
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, OCL, OCI, OVCL, ComCtrls, StdCtrls, Buttons, ExtCtrls,  DReportes,
     RegExpr,
  ADODB, DB;

type
  TdmConfigurarEspeciales = class(TDataModule)
    OESchema: TOESchema;
    Hdbc: THdbc;
    ADOConnection: TADOConnection;
    ADOCommand: TADOCommand;
    HstmtScript: THstmt;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure OESchemaProgress(Sender: TObject; Info: String);
  private
    { Private declarations }
    FTipo: Integer;
    FUserName: String;
    FPassword: String;
    FCompany: String;
    FDatabase: String;
    FAlias: String;
    FScriptFile : String;
    FServer : String;
    //FDatabaseName :String;
    FDatabaseComparte :String;
    FOK: Boolean;
    FdmMigracion : TdmReportes;


    function GetScriptFile: String;

    procedure SetScriptFile(const Value: String);

  protected
    { Protected declarations }
    {$ifdef USAR_ADO_SCRIPTS}
    function ConectaMSSQL_ADO( var mensaje : string ): Boolean;
    procedure DesconectaMSSQ_ADO;
    {$endif}
    function Ejecutar_Script(sScriptSQL: string;  var mensaje: string): boolean;
    function ProcesaScripts( sScriptSQL : string ): Boolean;


    function ConectaMSSQL: Boolean;
    function ConectaMSSQLComparte : Boolean;
    function ConectaMSSQLDatos : Boolean;
    function PreProcesarScript( lQuitarComentarios : boolean; lGenerarTmp : boolean;  var sBufferOut  : string) : boolean;

  public
    { Public declarations }
    property Tipo: Integer read FTipo write FTipo;
    property OK: Boolean read FOK;
    property Alias: String read FAlias write FAlias;
    property Database: String read FDatabase write FDatabase;
    property Company: String read FCompany write FCompany;
    property Password: String read FPassword write FPassword;
    property UserName: String read FUserName write FUserName;
    property Server: String read FServer write FServer;
//    property DatabaseName: String read FDatabaseName write FDatabaseName;
    property DatabaseComparte: String read FDatabaseComparte write FDatabaseComparte;
    property DmMigracion: TdmReportes read FdmMigracion write FdmMigracion;
    property ScriptFile: String read GetScriptFile write SetScriptFile;

    function Conectar : boolean;
    function ProcesaScript: Boolean;
    procedure Desconectar;
  end;

var
  dmConfigurarEspeciales: TdmConfigurarEspeciales;

implementation

{$R *.dfm}

uses DDBConfig,
     ZetaDialogo,
     ZetaTressCFGTools,
     ZetaServerTools,
     ZetaCommonTools,
     ZetaCommonClasses,
     FHelpContext;

{ TdmConfigurarEspeciales }



function TdmConfigurarEspeciales.ConectaMSSQL: Boolean;
begin
     try
        try
           with Hdbc do
           begin
                Connected := False;
                Datasource := FAlias;
                UserName := FUserName;
                Password := FPassword;
                BeforeConnect := dmMigracion.HdbcBeforeConnect;
                Connected := True;
           end;
           Result := True;
        except
              on Error: Exception do
              begin
                   Application.HandleException( Error );
                   Result := False;
              end;
        end;
     finally
     end;
end;

procedure TdmConfigurarEspeciales.Desconectar;
begin
{$ifdef USAR_ADO_SCRIPTS}
    DesconectaMSSQ_ADO;
{$else}
     hdbc.Connected := False;
{$endif}
end;

function TdmConfigurarEspeciales.GetScriptFile: String;
begin
   Result := FScriptFile;
end;

function TdmConfigurarEspeciales.PreProcesarScript( lQuitarComentarios : boolean; lGenerarTmp : boolean;  var sBufferOut  : string) : boolean;

function LoadFileBuffer(const FileName: TFileName): string;
var
 stream : TFileStream;
 sBuffer : string;
begin
   sBuffer := VACIO;
   stream := TFileStream.Create(FileName, fmOpenRead or fmShareDenyNone);
   with stream do
   try
      SetLength( sBuffer, Size);
      Read( PChar(sBuffer)^, Size);
   finally
          Free;
   end;
   Result := sBuffer;
end;

function SaveToFileBuffer(const FileName: TFileName; buffer : string): String;
var
   stringStream : TStringStream;
   fileStream : TFileStream;
begin
   stringStream := TStringStream.Create(buffer);
   fileStream := TFileStream.Create(FileName,fmCreate or fmOpenWrite);

   fileStream.CopyFrom( stringStream, stringStream.Size);
   freeAndNil(fileStream);
end;

var
   buffer, sServer, sDatabaseComparte : string;
   oQuitaComentaExpr : TRegExpr;

begin
     Result := False;
     sBufferOut := VACIO;
     if FileExists( ScriptFile )  then
     begin
          buffer := LoadFileBuffer( ScriptFile );
          ZetaServerTools.GetServerDatabase( dmDBConfig.Registry.Database, sServer, sDatabaseComparte );
          DmMigracion.TransformaScriptMSSQL( buffer, sServer, Self.Database, sDatabaseComparte);

          if ( lQuitarComentarios ) then
          begin
              oQuitaComentaExpr := TRegExpr.Create;
              try
                 oQuitaComentaExpr.Expression := '(/\*.*?(\*/))|(\s*\-\-[^\r\n]+)';
                 buffer := oQuitaComentaExpr.Replace( buffer, VACIO);
              finally
                     FreeAndNil ( oQuitaComentaExpr );
              end;
          end;

          sBufferOut := buffer;

          if ( lGenerarTmp ) then
             SaveToFileBuffer( ScriptFile+'.TMP', buffer );

     end
     else
     begin
          raise Exception.Create( 'No se pudo localizar el Archivo Script SQL' );
     end;
end;

function TdmConfigurarEspeciales.ProcesaScript: Boolean;
var
   dStart, dWait: TDateTime;
   sBufferOut : String;
begin



     with TdmReportes( dmMigracion ) do
     begin
          ConfiguraBaseDeDatosMsg( Format( 'Corriendo Script %s', [ ScriptFile ] ) );
          try
             {$ifdef USAR_ADO_SCRIPTS}
             PreProcesarScript( False, False, sBufferOut );
             ProcesaScripts( sBufferOut );
             {$else}
             {$ifndef EJECUTAR_SCRIPTS_SOLOS}
             PreProcesarScript( True, True, sBufferOut );
             OESchema.RunScript( ScriptFile+'.TMP' );
             {$else}
             PreProcesarScript( False, False, sBufferOut );
             ProcesaScripts( sBufferOut );
             {$endif}
             {$endif}
             sBufferOut := VACIO;
             Result := True;
          except
                on Error: Exception do
                begin
                     //Error.Message := 'Script: ' + ScriptFile + CR_LF + Error.Message;
                     ConfiguraBaseDeDatosError( '� Error Al Procesar Script !', Error );
                     Result := False;
                end;
          end;
          if ( Result ) then
             ConfiguraBaseDeDatosMsg( Format( 'Script %s Fu� Ejecutado', [ ScriptFile ] ) );
     end;
          { GA: 01/Ago/2002: Parece que es necesario dejar pasar unos segundos entre }
     { la ejecuci�n de cada script para permitirle al servidor actualizar su schema }
     dWait := EncodeTime( 0, 0, 3, 0 ); { 3 Segundos }
     dStart := Now;
     while True do
     begin
          if ( ( Now - dStart ) > dWait ) then
             Break;
     end;
     DeleteFile(ScriptFile+'.TMP');
end;

procedure TdmConfigurarEspeciales.SetScriptFile(const Value: String);
begin
  FScriptFile := Value;
end;

procedure TdmConfigurarEspeciales.DataModuleCreate(Sender: TObject);
begin
    //
end;

procedure TdmConfigurarEspeciales.DataModuleDestroy(Sender: TObject);
begin
//
end;

function TdmConfigurarEspeciales.ConectaMSSQLComparte: Boolean;
begin
    Company := 'COMPARTE';
    Alias :=  dmDBConfig.Registry.AliasComparte;
    Database := dmDBConfig.Registry.Database;
    UserName := dmDBConfig.Registry.UserName;
    Password := dmDBConfig.Registry.Password;

    Result := ConectaMSSQL;
    FOK := Result;
end;

function TdmConfigurarEspeciales.ConectaMSSQLDatos: Boolean;
{$ifdef USAR_ADO_SCRIPTS}
var
   sMensaje : string;
{$endif}
begin
    with dmDBConfig do
    begin
         Company := GetCompanyName;
         Alias := GetCompanyAlias;
         Database := GetCompanyDatabase;
         UserName := GetCompanyUserName;
         Password := GetCompanyPassword;
    end;

{$ifdef USAR_ADO_SCRIPTS}
    Result := ConectaMSSQL_ADO(sMensaje);
    if not Result then
       raise Exception.Create( 'No pudo conectarse nativamente  a la BD '+ Database  + ':' + sMensaje );
{$else}
    Result := ConectaMSSQL;
{$endif}
    FOK := Result;
end;

function TdmConfigurarEspeciales.Conectar: boolean;
begin
     if ( Tipo < 0 ) then
        Result := ConectaMSSQLComparte
     else
         Result := ConectaMSSQLDatos;
end;

procedure TdmConfigurarEspeciales.OESchemaProgress(Sender: TObject;
  Info: String);
begin
     inherited;
     TdmReportes( dmMigracion ).ConfiguraBaseDeDatosMsg( Info );
end;


{$ifdef USAR_ADO_SCRIPTS}
function TdmConfigurarEspeciales.ConectaMSSQL_ADO( var mensaje : string ) : boolean;
var
   sDatabaseName : string;
const
    K_ADO_PROVIDER_SQL2000= 'Provider=SQLOLEDB;';
    K_ADO_PROVIDER_SQL2000_1= 'Provider=SQLOLEDB.1;';
    K_ADO_PROVIDER_SQL2000_2= 'Provider=MSDASQL.1;';
    K_ADO_PROVIDER_SQL2005= 'Provider=SQLNCLI;';
    K_ADO_PROVIDER_SQL2005_1= 'Provider=SQLNCLI.1;';
    K_ADO_PROVIDER_SQL2008 = 'Provider=SQLNCLI10;';
    K_ADO_PROVIDER_SQL2008R2 = 'Provider=SQLNCLI10.1;';
    K_CONNECTION_STRING_FORMAT  = '%sPersist Security Info=True;User ID=%s;Password=%s;Initial Catalog=%s;Data Source=%s';

    function GetConnectionString( sProvider : string ) : string;
    begin
         ZetaServerTools.GetServerDatabase( FDatabase, FServer, sDatabaseName );
         Result := Format( K_CONNECTION_STRING_FORMAT, [K_ADO_PROVIDER_SQL2008, FUserName, FPassword, sDatabaseName, FServer] );
    end;

    function Conectar( sProvider : string ) : boolean;
    begin
        try
           ADOConnection.ConnectionString := GetConnectionString( sProvider ) ;
           ADOConnection.LoginPrompt := False;
           ADOConnection.Open;
           Result := ADOConnection.Connected;
        except
           on Error: Exception do
           begin
                mensaje := Error.Message + ' Provider: ' + sProvider ;
                Result := False;
           end;
        end;
    end;

begin

//1 Intenta SQL 2008R2
    Result :=  Conectar( K_ADO_PROVIDER_SQL2008R2);

    //1.1 Intenta SQL 2008
    if (not Result ) then
    begin
        mensaje := VACIO;
        Result :=  Conectar( K_ADO_PROVIDER_SQL2008 );
    end;

//2 Intenta SQL 2005
    if (not Result ) then
    begin
        mensaje := VACIO;
        Result :=  Conectar( K_ADO_PROVIDER_SQL2005 );
    end;
    if (not Result ) then

    begin
        mensaje := VACIO;
        Result :=  Conectar( K_ADO_PROVIDER_SQL2005_1 );
    end;


//2 Intenta SQL 2000
    if (not Result ) then
    begin
        mensaje := VACIO;
        Result :=  Conectar( K_ADO_PROVIDER_SQL2000 );
    end;
            //1.1 Intenta SQL 200 Con SQL2000
    if (not Result ) then
    begin
        mensaje := VACIO;
        Result :=  Conectar( K_ADO_PROVIDER_SQL2000_1 );
    end;

     //1.1 Intenta SQL 200 Con SQL2000   ODBC
    if (not Result ) then
    begin
        mensaje := VACIO;
        Result :=  Conectar( K_ADO_PROVIDER_SQL2000_2 );
    end;


end;


procedure TdmConfigurarEspeciales.DesconectaMSSQ_ADO;
begin
     try
        if ( ADOConnection.Connected ) then
           ADOConnection.Close;
     except
       on Error: Exception do
          begin
          end;
    end;

end;
{$endif}

function TdmConfigurarEspeciales.Ejecutar_Script(sScriptSQL: string;  var mensaje: string): boolean;
begin
    mensaje := VACIO;
    {$ifdef USAR_ADO_SCRIPTS}
    try
          ADOCommand.Connection := Self.ADOConnection;
          ADOCommand.CommandType := cmdUnknown;
          ADOCommand.CommandText := sScriptSQL;
          ADOCommand.Execute;
          Result := True;
    except
          on Error: Exception do
          begin
               mensaje := Error.Message;
               Result := False;
          end;
    end;
    {$else}
    try
          HstmtScript.hDbc := Hdbc;
          HstmtScript.SQL := sScriptSQL;
          HstmtScript.Execute;
          Result :=HstmtScript.Executed;
    except
          on Error: Exception do
          begin
               mensaje := Error.Message;
               Result := False;
          end;
    end;
    {$endif}
end;


function TdmConfigurarEspeciales.ProcesaScripts( sScriptSQL : string ) : Boolean;
var
   sMensaje : string;
   sNombreScript : string;
   oSepararGo : TRegExpr;
   oScriptPieces : TStringList;
   i : integer;
const
   K_AVANCE_FORMATO = '%d de %d : %s' ;
begin
    sMensaje := VACIO;
    sNombreScript := ExtractFileName(  Self.ScriptFile );
    oSepararGo := TRegExpr.Create;
    oSepararGo.ModifierI  := True;
    oSepararGo.ModifierM  := True;
    oSepararGo.ModifierS  := True;
    oSepararGo.ModifierX  := True;

    oSepararGo.Expression := '^\s*(GO)\s*$' ;
    oScriptPieces := TStringList.Create;
    oSepararGo.Split(sScriptSQL, oScriptPieces);
    Result := False;

    for i := 0 to oScriptPieces.Count-1 do
    begin
        if StrLleno( oScriptPieces[i] ) then
        begin
           TdmReportes( dmMigracion ).ItemName :=  Format( K_AVANCE_FORMATO, [i+1, oScriptPieces.Count, sNombreScript]);
           Result := Ejecutar_Script( oScriptPieces[i], sMensaje );
           if StrLleno( sMensaje ) then
           begin
               TdmReportes( dmMigracion ).ConfiguraBaseDeDatosError('Instalando: ' +oScriptPieces[i] , Exception.Create( sMensaje));
           end;
           TdmReportes( dmMigracion ).DoCallBack(  TdmReportes( dmMigracion ), Result);
        end;
    end;

    FreeAndNil( oSepararGo ) ;
end;

end.


