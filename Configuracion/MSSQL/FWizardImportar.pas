unit FWizardImportar;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Buttons, ExtCtrls, Db,
     FMigrar,
     ZetaWizard,
     ZetaDBTextBox, jpeg;

type
  TWizardImportar = class(TMigrar)
    OpenDialog: TOpenDialog;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    ArchivoSeek: TSpeedButton;
    ed_DataBase: TEdit;
    ed_User: TEdit;
    ed_pass: TEdit;
    GroupBox2: TGroupBox;
    cbBorrarTablas: TCheckBox;
    Label2: TLabel;
    lbBDSqlServer: TZetaTextBox;
    lbServidorSQLServer: TZetaTextBox;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    FMigrarComparte: Boolean;
    FMigrado: Boolean;
  public
    { Public declarations }
    property MigrarComparte: Boolean read FMigrarComparte write FMigrarComparte default False;
    property Migrado: Boolean read FMigrado;
  end;

var
  WizardImportar: TWizardImportar;

implementation

uses DMigrarDatosIB,
     DDBConfig,
     FHelpContext,
     ZetaTressCFGTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaClientTools;

{$R *.DFM}

procedure TWizardImportar.FormCreate(Sender: TObject);
begin
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Migrar.log' );
     dmMigracion := TdmMigrarDatosIB.Create( Self );
     inherited;
     HelpContext := H00017_Migrando_Datos_de_IB_a_MS_SQL;
end;

procedure TWizardImportar.FormDestroy(Sender: TObject);
begin
     dmMigracion.Free;
     inherited;
end;

procedure TWizardImportar.FormShow(Sender: TObject);
var
   sDatabase, sServer, sDB: String;
begin
     inherited;
     if FMigrarComparte then
     begin
          sDatabase := dmDBConfig.Registry.Database;
          Caption := 'Migrar Comparte De IB a MS SQL';
     end
     else
     begin
          sDatabase := dmDBConfig.GetCompanyDatabase;
          Caption := 'Migrar Empresa De IB a MS SQL';
     end;
     ZetaServerTools.GetServerDatabase( sDatabase, sServer, sDB );
     lbServidorSQLServer.Caption := sServer;
     lbBDSqlServer.Caption := sDB;
end;

procedure TWizardImportar.ArchivoSeekClick(Sender: TObject);
begin
     ed_DataBase.Text := ZetaClientTools.AbreDialogo( OpenDialog, ed_DataBase.Text, 'gdb' );
end;

procedure TWizardImportar.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);

  function Valida( const sDato, sMsg: String ): Boolean;
  begin
       if ZetaCommonTools.StrVacio( sDato ) then
       begin
            ZetaDialogo.zError( Caption, Format( 'Falt� Especificar Campo de %s ORIGEN', [ sMsg ] ), 0 );
            CanMove := False;
            Result := False;
       end
       else
           Result := True;
  end;

begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( DirectorioDOS ) then
          begin
               CanMove := Valida( ed_Database.Text, 'Base de Datos' ) and
                          Valida( ed_user.Text, 'Usuario' ) and
                          Valida( ed_Pass.Text, 'Password' );
          end;
     end;
end;

procedure TWizardImportar.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
begin
     FMigrado := False;
     StartLog;
     with TdmMigrarDatosIB( dmMigracion ) do
     begin
          IBDataBase := ed_Database.Text;
          IBUser := ed_User.Text;
          IBPassword := ed_Pass.Text;
          StartProcess;
          Importar( Self, FMigrarComparte, cbBorrarTablas.Checked );
          EndProcess;
          lOk := True;
          FMigrado := lOk;
     end;
     EndLog;
     inherited;
end;

procedure TWizardImportar.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     Self.Close;
     lOk := True;
end;

end.
