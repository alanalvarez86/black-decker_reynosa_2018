unit FWizardPresupuesto;

{ :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  :: Sistema:     Tress                                      ::
  :: Versi�n:     2.0                                        ::
  :: Lenguaje:    Pascal                                     ::
  :: Compilador:  Delphi v.5                                 ::
  :: Unidad:      FWizardPresupuesto.pas                     ::
  :: Descripci�n: Wizar de TressCFG para copiar informacion  ::
                  de alguna empresa de tipo DATOS a una      ::
                  nueva llamada Presupuestos (Seleccionada)  ::
  ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: }

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, Buttons, ExtCtrls, Db, DMigrar, ZWizardBasico,
     Mask, ZetaFecha, ZetaKeyCombo, jpeg, ZetaWizard,
     FMigrar,
     ZetaDBTextBox, ZetaNumero;

type
  TWizardPresupuesto = class(TWizardBasico)
    StatusBar: TStatusBar;
    Inicio: TTabSheet;
    OrigenTress: TTabSheet;
    Parametros: TTabSheet;
    Copiado: TTabSheet;
    Imagen: TImage;
    MensajeInicial: TMemo;
    grpEmpresaOrigen: TGroupBox;
    grpParametros: TGroupBox;
    lblAnoPresupuesto: TLabel;
    lblFechaEmpleadosActivos: TLabel;
    lblEmpleadosBaja: TLabel;
    PanelLog: TPanel;
    ProcesoGB: TGroupBox;
    AvanceProceso: TProgressBar;
    LogDisplay: TListBox;
    LogFileDialog: TSaveDialog;
    FechaEmpleadosActivos: TZetaFecha;
    FechaEmpleadosBaja: TZetaFecha;
    EmpresaFuente: TListBox;
    LogFileGB: TGroupBox;
    LogFileSeek: TSpeedButton;
    LogFileLBL: TLabel;
    LogFile: TEdit;
    LogFileWrite: TCheckBox;
    AnoPresupuesto: TZetaNumero;
    cbConservarConfNomina: TCheckBox;
    lbConservarConfigNomina: TLabel;
    chkNomMensual: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure LogFileSeekClick(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure AnoPresupuestoExit(Sender: TObject);
  private
    { Private declarations }
    function CallMeBack( const sMensaje: String ): Boolean;
    function GetYear(const dDAte : TDateTime) : Integer;
    function GetLogFileName: String;
    procedure SetLogFileName(const Value: String);
    procedure ShowStatus( const sMsg: String );
    function GetEmpresa: Integer;
    procedure SetFechas;

  public
    { Public declarations }
    property LogFileName: String read GetLogFileName write SetLogFileName;
  end;

var
  WizardPresupuesto: TWizardPresupuesto;

implementation


uses DDBConfig,
     FHelpContext,
     ZetaTressCFGTools,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaServerTools,
     ZetaDialogo,
     ZetaCommonLists,
     ZetaClientTools,
     DCompara,
     FEmpresas;

{$R *.dfm}

const
     K_ARCH_PRESUP = 'Presupuesto.log';

procedure TWizardPresupuesto.FormCreate(Sender: TObject);
begin
     dmCompara := TdmCompara.Create( Self ); //Data Module que se usara para los procesos del wizard.
     Wizard.Primero;
     HelpContext := H52000_Copiado_Base_Datos_para_Presupuestos;
end;

procedure TWizardPresupuesto.FormShow(Sender: TObject);
var
     oCursor: TCursor;

     procedure SetDefaultValues;
     var
          iEsteYear : Integer;

     begin
          iEsteYear := GetYear(Now);

          Wizard.Primero;
          AnoPresupuesto.Valor := iEsteYear + 1;
          SetFechas;
          LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( K_ARCH_PRESUP );
     end;

begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
          SetDefaultValues;
     finally
          Screen.Cursor := oCursor;
     end;
end;

procedure TWizardPresupuesto.FormDestroy(Sender: TObject);
begin
     if ( dmCompara.HdbcSource.Connected ) then
        dmCompara.DesconectarFuente;
        
     FreeAndNil( dmCompara );
     inherited;
end;

function TWizardPresupuesto.GetYear(const dDAte : TDateTime) : Integer;
var
     wYearActual, wMes, wDia : Word;
begin
     DecodeDate( dDAte, wYearActual, wMes, wDia );
     Result := Integer( wYearActual );
end;

procedure TWizardPresupuesto.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
    lOk := True;
    Close;
end;

function TWizardPresupuesto.GetEmpresa: Integer;
begin
     with EmpresaFuente do
     begin
          if ( ItemIndex < 0 ) then
             Result := -1
          else
              Result := Integer( Items.Objects[ ItemIndex ] );
     end;
end;

procedure TWizardPresupuesto.LogFileSeekClick(Sender: TObject);
begin
     inherited;
     with LogFileDialog do
     begin
          FileName := LogFileName;
          if Execute then
             LogFileName := FileName;
     end;
end;

function TWizardPresupuesto.GetLogFileName: String;
begin
     Result := LogFile.Text;
end;

procedure TWizardPresupuesto.SetLogFileName(const Value: String);
begin
     LogFile.Text := Value;
end;

procedure TWizardPresupuesto.ShowStatus(const sMsg: String);
begin
     StatusBar.SimpleText := sMsg;
end;

function TWizardPresupuesto.CallMeBack( const sMensaje: String ): Boolean;
begin
     ShowStatus( sMensaje );
     AvanceProceso.StepIt;
     Application.ProcessMessages;
     Result := not Wizard.Cancelado;
end;

procedure TWizardPresupuesto.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;


     function ValidaBitacora : Boolean;
     begin
          Result := not StrLleno( LogFile.Text );
     end;

begin
     inherited;
     if CanMove then
     begin
          with Wizard do
          begin
               if Adelante then
               begin
                    if EsPaginaActual( Inicio ) then
                    begin
                         with dmCompara do
                         begin
                              InitEmpresas( Self.EmpresaFuente.Items, True )
                         end;                         
                    end
                    else if EsPaginaActual( OrigenTress ) then
                    begin
                         if ( GetEmpresa < 0 ) then
                         begin
                              CanMove := False;
                              ZetaDialogo.zInformation( '� Atenci�n !', 'Es Necesario Seleccionar Una Empresa Fuente', 0 );
                              ActiveControl := EmpresaFuente;
                         end
                         else
                         begin
                              oCursor := Screen.Cursor;
                              Screen.Cursor := crHourglass;
                              try
                                 if not dmCompara.ConectarFuente( GetEmpresa ) then
                                 begin
                                      ActiveControl := EmpresaFuente;
                                      CanMove := False;
                                 end;
                              finally
                                      Screen.Cursor := oCursor;
                              end;
                         end;
                    end
                    else if EsPaginaActual( Parametros ) then
                    begin
                         if ( ValidaBitacora ) then
                         begin
                              CanMove := False;
                              ZetaDialogo.zInformation( '� Atenci�n !', 'Es Necesario Establecer un archivo de Bit�cora', 0 );
                              ActiveControl := LogFile;
                         end;
                    end;
               end;
          end;
     end;
end;

procedure TWizardPresupuesto.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
     FLog: TTransferLog;
begin
     // (JB) Ejecutar
     FLog := TTransferLog.Create;
     try
           with FLog do
           begin
                Start( LogFileName );
           end;
           ShowStatus( 'Preparaci�n Base Presupuesto' );
           with dmCompara do
           begin
                PreparacionPresupuestos( CallMeBack,
                                          FLog,
                                          AnoPresupuesto.ValorEntero,
                                          FechaEmpleadosActivos.Valor,
                                          FechaEmpleadosBaja.Valor,
                                          cbConservarConfNomina.Checked,
                                          chkNomMensual.Checked );
           end;
           ShowStatus( 'Preparaci�n Base Presupuesto Terminada' );
           lOk := True;
      finally
           with FLog do
           begin
                 Close;
                 Free;
           end;
      end;
      if ZetaDialogo.zConfirm( Caption, '� Proceso Terminado !' + CR_LF + '� Desea Ver La Bit�cora ?', 0, mbOk ) then
           ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( LogFileName ), ExtractFilePath( LogFileName ), SW_SHOWDEFAULT );
end;


procedure TWizardPresupuesto.AnoPresupuestoExit(Sender: TObject);
begin
     SetFechas;
end;

procedure TWizardPresupuesto.SetFechas;
begin
     if ( AnoPresupuesto.ValorEntero <= GetYear(Now) + 10 ) and ( AnoPresupuesto.ValorEntero >= GetYear(Now) -10 ) then
     begin
          FechaEmpleadosActivos.Valor := StrToDate( Format( '01/01/%d', [ AnoPresupuesto.ValorEntero  ] ) );
          FechaEmpleadosBaja.Valor := StrToDate( Format( '01/01/%d', [ AnoPresupuesto.ValorEntero - 1 ] ) );
     end
end;

end.
