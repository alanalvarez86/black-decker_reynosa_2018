unit FLicencias;

{$ifdef MSSQL}
{$define DEBUGSENTINEL}
{$define SENTINELVIRTUAL}
{$endif}

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Db, Grids, DBGrids, DBClient, ComCtrls,
     FAutoServer,
     FLicenciasAllocate,
     ZetaLicenseMgr,
     DZetaServerProvider,
     ZBaseDlgModal,
     ZetaDBGrid,
     ZetaDBTextBox,
     ZetaServerDataSet;

type
  TLicenseAllocation = class(TZetaDlgModal)
    DataSource: TDataSource;
    ZetaDBGrid: TZetaDBGrid;
    PanelSuperior: TPanel;
    StatusBar: TStatusBar;
    LicenciasGB: TGroupBox;
    LicenciaTotalLBL: TLabel;
    AsignadasLBL: TLabel;
    Asignadas: TZetaTextBox;
    LicenciaTotal: TZetaTextBox;
    DisponiblesLBL: TLabel;
    Disponibles: TZetaTextBox;
    Asignar: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AsignarClick(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    { Private declarations }
    FAutoServer: TAutoServer;
    FAllocated: Integer;
    FAllocateLicences: TFAllocateLicences;
    FManager: TLicenseMgr;
    oZetaProvider: TdmZetaServerProvider;
    FEnabled: Boolean;
    function GetAvailable: Integer;
    {$ifdef ANTES}
    function LicenciasEscribir: Boolean;
    {$endif}
    procedure LicenciasAsignar;
    procedure LicenciasLeerCallBack( const sMsg: String );
    procedure LicenciasLeerCallBackError( const sMsg: String; Error: Exception );
    procedure LicenciasLeer;
    procedure SetAllocated(const Value: Integer);
    {$ifdef VALIDA_EMPLEADOS}
    procedure EmpresasValidar;
    {$endif}
    procedure EmpresasLeer;
    procedure NoHaySentinela;
    procedure SetControls( const lEnabled: Boolean );
  protected
    { Protected declarations }
    property Allocated: Integer read FAllocated write SetAllocated;
    property Available: Integer read GetAvailable;
  public
    { Public declarations }
    property AutoServer: TAutoServer read FAutoServer write FAutoServer;
  end;

var
  LicenseAllocation: TLicenseAllocation;

implementation

uses DDBConfig,
     ZetaCommonClasses,
     ZetaSystemWorking,
     ZetaDialogo,
     {$ifdef SENTINELVIRTUAL}
     FSentinelRegistry,
     {$endif}     
     FAutoServerLoader;

const
     PANEL_INSTALACION = 0;
     PANEL_INTENTOS = 1;
     PANEL_MENSAJE = 2;

{$R *.DFM}

procedure TLicenseAllocation.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H00028_Asignacion_de_licencias_de_empleados;
     FAllocated := 0;
     FAllocateLicences := TFAllocateLicences.Create( Self );
     oZetaProvider := TdmZetaServerProvider.Create( Self );
     FManager := TLicenseMgr.Create( oZetaProvider );
     {$ifdef VALIDA_EMPLEADOS}
     Caption := 'Asignar Licencias de Empleados a Empresas';
     StatusBar.Visible := True;
     PanelSuperior.Visible := True;
     {$else}
     Caption := 'Cantidad De Empleados Por Empresa';
     StatusBar.Visible := False;
     PanelSuperior.Visible := False;
     {$endif}
     with OK do
     begin
          {$ifdef VALIDA_EMPLEADOS}
          Visible := True;
          {$else}
          Visible := False;
          {$endif}
     end;
     with Cancelar do
     begin
          {$ifdef VALIDA_EMPLEADOS}
          Kind := bkCancel;
          Caption := '&Cancelar';
          {$else}
          Kind := bkClose;
          Caption := '&Salir';
          {$endif}
     end;
end;

procedure TLicenseAllocation.NoHaySentinela;
begin
//US 1227: Cambiar etiqueta 'Sentinel' por 'Guardia" y en Mensajes de validacion que apliquen
{$ifdef MSSQL}
     ZetaDialogo.zError(  '� No Hay Guardia !', 'No hay un Guardia F�sico Conectado o Guardia Virtual Habilitado A Esta Computadora', 0);
{$else}
     ZetaDialogo.zError( '� No Hay Guardia !', 'No Hay Un Guardia Conectado A Esta Computadora', 0 );
{$endif}
end;

procedure TLicenseAllocation.FormShow(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with FManager do
        begin
             AutoServer := Self.AutoServer;
        end;
        { Empezar suponiendo que todo est� bien }
        SetControls( True );
        { Cargar Lista de Empresas }
        EmpresasLeer;
        {$ifdef VALIDA_EMPLEADOS}
        { Validar Empresas }
        EmpresasValidar;
        {$endif}
        { Leer los empleados asignados }
        if FEnabled then
           LicenciasLeer;
        { Asignar Empresas a Controles Visuales }
        Datasource.Dataset := dmDBConfig.cdsEmpresas;
        { Fijar Valores de Licencias }
        with AutoServer do
        begin
             if not Cargar then
             begin
             {$ifdef MSSQL}
               {$ifdef SENTINELVIRTUAL}
               FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
              {$endif}
                  TAutoServerLoader.LoadSentinelInfo( AutoServer );
                  if ( EsDemo ) then
                  begin
                      NoHaySentinela;
                      SetControls( False );
                  end;
             {$else}
                  NoHaySentinela;
                  SetControls( False );
             {$endif}
             end;
             {$ifdef ANTES}
             with StatusBar do
             begin
                  Panels[ PANEL_INSTALACION ].Text := Format( 'Instalaci�n # %d', [ Instalaciones ] );
                  with Panels[ PANEL_INTENTOS ] do
                  begin
                       if ( Intentos > 0 ) then
                          Text := Format( 'Intentos %d', [ Intentos ] )
                       else
                           Text := 'Intentos Agotados';
                  end;
             end;
             {$endif}
             Self.LicenciaTotal.Caption := Trim( Format( '%8.0n', [ Empleados / 1 ] ) );
        end;

        Allocated := FAllocated;
     finally
            Screen.Cursor := oCursor;
      {$ifdef SENTINELVIRTUAL}
               FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
              {$endif}
     end;
end;

procedure TLicenseAllocation.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FManager );
     FreeAndNil( oZetaProvider );
     FreeAndNil( FAllocateLicences );
     inherited;
end;

function TLicenseAllocation.GetAvailable: Integer;
begin
     Result := AutoServer.Empleados - FAllocated;
end;

procedure TLicenseAllocation.SetControls(const lEnabled: Boolean);
begin
     FEnabled := lEnabled;
     OK.Enabled := FEnabled;
     Asignar.Enabled := FEnabled;
     {$ifdef VALIDAR_EMPLEADOS}
     {$else}
     with ZetaDBGrid do
     begin
          with Columns[ GetFieldColumnIndex( 'CM_ASIGNED' ) ] do
          begin
               Visible := False;
          end;
     end;
     {$endif}
end;

procedure TLicenseAllocation.SetAllocated(const Value: Integer);
begin
     FAllocated := Value;
     Asignadas.Caption := Trim( Format( '%8.0n', [ FAllocated / 1 ] ) );
     with Self.Disponibles do
     begin
          if ( Available > 0 ) then
             Font.Color := clBlack
          else
              if ( Available = 0 ) then
                 Font.Color := clGreen
              else
                  Font.Color := clRed;
          Caption := Trim( Format( '%8.0n', [ Available / 1 ] ) );
     end;
end;

procedure TLicenseAllocation.EmpresasLeer;
begin
     with dmDBConfig do
     begin
          EmpresasConectar( {$ifdef VALIDA_EMPLEADOS}False{$else}True{$endif} );
     end;
end;

{$ifdef VALIDA_EMPLEADOS}
procedure TLicenseAllocation.EmpresasValidar;
var
   i, iPos: Integer;
   FLista: TStrings;
   sEmpresa, sCompany, sDatabase: String;
   lFound: Boolean;
begin
     {
     Es posible mejorar esto usando un cdsEmpresas.IndexFieldNames por CM_DATOS
     y barrer el dataset buscando dos registros contiguos con CM_DATOS iguales
     }
     FLista := TStringList.Create;
     try
        { Llenar TStrings }
        with cdsEmpresas do
        begin
             First;
             while not Eof do
             begin
                  FLista.Add( Format( '%s=%s', [ FieldByName( 'CM_CODIGO' ).AsString, UpperCase( FieldByName( 'CM_DATOS' ).AsString ) ] ) );
                  Next;
             end;
             { Validar que no haya valores repetidos en COMPANY.CM_DATOS }
             lFound := False;
             First;
             while not Eof and not lFound do
             begin
                  sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
                  sDatabase := UpperCase( FieldByName( 'CM_DATOS' ).AsString );
                  with FLista do
                  begin
                       iPos := IndexOfName( sEmpresa );
                       for i := 0 to ( Count - 1 ) do
                       begin
                            sCompany := Names[ i ];
                            if ( i <> iPos ) and ( sDatabase = Values[ sCompany ] ) then
                            begin
                                 lFound := True;
                                 Break;
                            end;
                       end;
                  end;
                  Next;
             end;
             if lFound then
             begin
                  SetControls( False );
                  ZetaDialogo.zError( '� Empresa Con Base De Datos Duplicada !', Format( 'La Empresa %s Usa La Misma Base De Datos Que La Empresa %s', [ sEmpresa, sCompany ] ), 0 );
             end
             else
                 SetControls( True );
        end;
     finally
            FreeAndNil( FLista );
     end;
end;
{$endif}

procedure TLicenseAllocation.LicenciasLeerCallBack( const sMsg: String );
begin
     ZSystemWorking.WriteMensaje( sMsg );
end;

procedure TLicenseAllocation.LicenciasLeerCallBackError( const sMsg: String; Error: Exception );
begin
     ZetaDialogo.zExcepcion( '� Error Al Leer Empleados Asignados!', sMsg, Error, 0 )
end;

procedure TLicenseAllocation.LicenciasLeer;
begin
     ZetaSystemWorking.InitAnimation( 'Leyendo Empresas' );
     try
        with dmDBConfig do
        begin
             LicenciasLeer( FManager, oZetaProvider, LicenciasLeerCallBack, LicenciasLeerCallBackError );
             FAllocated := 0;
             with cdsEmpresas do
             begin
                  First;
                  while not Eof do
                  begin
                       FAllocated := FAllocated + FieldByName( 'CM_ASIGNED' ).AsInteger;
                       Next;
                  end;
                  First;
             end;
        end;
     finally
            ZetaSystemWorking.EndAnimation;
     end;
end;

procedure TLicenseAllocation.LicenciasAsignar;
begin
     if FEnabled then
     begin
          with FAllocateLicences do
          begin
               with dmDBConfig.cdsEmpresas do
               begin
                    Empresa := FieldByName( 'CM_CODIGO' ).AsString;
                    RazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
                    Employees := FieldByName( 'CM_ACTIVOS' ).AsInteger;
                    Anterior := FieldByName( 'CM_ASIGNED' ).AsInteger;
               end;
               ShowModal;
               if ( ModalResult = mrOk ) then
               begin
                    try
                       with dmDBConfig.cdsEmpresas do
                       begin
                            Edit;
                            FieldByName( 'CM_ASIGNED' ).AsInteger := Licencias;
                            Post;
                       end;
                       Allocated := Allocated + ( Licencias - Anterior );
                    except
                          on Error: Exception do
                          begin
                               Application.HandleException( Error );
                          end;
                    end;
               end;
          end;
     end;
end;

{$ifdef ANTES}
function TLicenseAllocation.LicenciasEscribir: Boolean;
var
   sEmpresa, sRazonSocial: String;
{$ifdef ANTES}
   lDisminuyo: Boolean;
{$endif}
begin
{$ifdef ANTES}
     lDisminuyo := False;
{$endif}
     Result := True;
     try
        with dmDBConfig.cdsEmpresas do
        begin
             DisableControls;
             try
                First;
                while not Eof and Result do
                begin
                     sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
                     sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
                     try
                        StatusBar.Panels[ PANEL_MENSAJE ].Text := Format( 'Asignando Empleados A %s', [ sEmpresa ] );
                        Application.ProcessMessages;
                        with oZetaProvider do
                        begin
                             Activate;
                             EmpresaActiva := dmDBConfig.BuildEmpresa( dmDBConfig.cdsEmpresas );
                             with FManager do
                             begin
                                  Result := AsignadosSet( FieldByName( 'CM_ASIGNED' ).AsInteger );
                             end;
                             Deactivate;
                        end;
                        { Unicamente ejecutar la primera vez }
                        {$ifdef ANTES}
                        if not lDisminuyo then
                        begin
                             { Disminuye el # de Intentos }
                             AutoServer.DisminuirIntentos;
                             lDisminuyo := True;
                        end;
                        {$endif}
                     except
                           on Error: Exception do
                           begin
                                ZetaDialogo.zExcepcion( '� Error Al Asignar Empleados !', Format( 'No Fu� Posible Asignar Empleados A La Empresa %s: %s', [ sEmpresa, sRazonSocial ] ), Error, 0 );
                                Result := False;
                           end;
                     end;
                     Next;
                end;
             finally
                    EnableControls;
             end;
        end;
        StatusBar.Panels[ PANEL_MENSAJE ].Text := 'Asignaci�n De Empleados Terminada';
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
           end;
     end;
end;
{$endif}

procedure TLicenseAllocation.AsignarClick(Sender: TObject);
begin
     inherited;
     LicenciasAsignar;
end;

procedure TLicenseAllocation.ZetaDBGridDblClick(Sender: TObject);
begin
     inherited;
     {$ifdef VALIDA_EMPLEADOS}
     LicenciasAsignar;
     {$endif}
end;

procedure TLicenseAllocation.OKClick(Sender: TObject);
{$ifdef ANTES}
var
   oCursor: TCursor;
{$endif}
begin
     inherited;
{$ifdef ANTES}
     if ( Available < 0 ) then
        ZetaDialogo.zError( '� Total Excedido !', 'La Asignaci�n De Licencias Excede El Total Permitido', 0 )
     else
         if ( AutoServer.Intentos < 1 ) then
            ZetaDialogo.zError( '� Intentos Agotados !', 'Los Intentos Para Asignar Licencias Se Han Agotado', 0 )
         else
             if (Available = 0) or (( Available > 0 ) and ZetaDialogo.zConfirm( '� Hay Licencias Disponibles !', 'Todav�a Hay Licencias Disponibles' + CR_LF + '� Desea Continuar ?', 0, mbNo )) then
             begin
                  if AutoServer.Cargar then
                  begin
                       if AutoServer.EsDemo then
                          ZetaDialogo.zError( '� Modo DEMO !', 'No Es Posible Asignar Licencias En Modo DEMO', 0 )
                       else
                       begin
                            oCursor := Screen.Cursor;
                            Screen.Cursor := crHourglass;
                            try
                               if FEnabled and LicenciasEscribir then
                               begin
                                    ZetaDialogo.zInformation( 'Asignaci�n De Licencias', 'Las Licencias Fueron Asignadas Con Exito', 0 );
                                    { Esto cierra la pantalla }
                                    ModalResult := mrOk;
                               end;
                            finally
                                   Screen.Cursor := oCursor;
                            end;
                       end
                  end
                  else
                      NoHaySentinela;
             end;
{$endif}
end;

end.
