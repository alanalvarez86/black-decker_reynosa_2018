object dmSugeridas: TdmSugeridas
  OldCreateOrder = False
  Left = 310
  Top = 318
  Height = 480
  Width = 696
  object dbSugerido: TDatabase
    Connected = True
    DatabaseName = 'dbSugerido'
    DriverName = 'STANDARD'
    Params.Strings = (
      'PATH=D:\3Win_13\Datos\Fuentes\Sugeridos')
    SessionName = 'Default'
    Left = 32
    Top = 20
  end
  object tblNumerica: TTable
    DatabaseName = 'dbSugerido'
    FieldDefs = <
      item
        Name = 'NU_CODIGO'
        DataType = ftSmallint
      end
      item
        Name = 'NU_DESCRIP'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'NU_INGLES'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'NU_NUMERO'
        DataType = ftFloat
      end
      item
        Name = 'NU_TEXTO'
        DataType = ftString
        Size = 30
      end>
    StoreDefs = True
    TableName = 'NUMERICA.DB'
    UpdateMode = upWhereKeyOnly
    Left = 104
    Top = 20
    object tblNumericaNU_CODIGO: TSmallintField
      FieldName = 'NU_CODIGO'
    end
    object tblNumericaNU_DESCRIP: TStringField
      FieldName = 'NU_DESCRIP'
      Size = 30
    end
    object tblNumericaNU_INGLES: TStringField
      FieldName = 'NU_INGLES'
      Size = 30
    end
    object tblNumericaNU_NUMERO: TFloatField
      FieldName = 'NU_NUMERO'
    end
    object tblNumericaNU_TEXTO: TStringField
      FieldName = 'NU_TEXTO'
      Size = 30
    end
  end
  object dsNumerica: TDataSource
    DataSet = tblNumerica
    Left = 176
    Top = 20
  end
  object qryArt_80: TQuery
    CachedUpdates = True
    OnNewRecord = qryArt_80NewRecord
    DatabaseName = 'dbSugerido'
    DataSource = dsNumerica
    RequestLive = True
    SQL.Strings = (
      'select * from art_80'
      'where nu_codigo = :nu_codigo'
      'order by a80_li')
    UpdateMode = upWhereKeyOnly
    UpdateObject = updArt_80
    Left = 240
    Top = 20
    ParamData = <
      item
        DataType = ftSmallint
        Name = 'NU_CODIGO'
        ParamType = ptUnknown
      end>
    object qryArt_80NU_CODIGO: TSmallintField
      FieldName = 'NU_CODIGO'
      Origin = 'DBSUGERIDO."art_80.DB".NU_CODIGO'
      Visible = False
    end
    object qryArt_80A80_LI: TFloatField
      DisplayLabel = 'L�mite Inferior'
      FieldName = 'A80_LI'
      Origin = 'DBSUGERIDO."art_80.DB".A80_LI'
      currency = True
    end
    object qryArt_80A80_CUOTA: TFloatField
      DisplayLabel = 'Cuota Fija'
      FieldName = 'A80_CUOTA'
      Origin = 'DBSUGERIDO."art_80.DB".A80_CUOTA'
      currency = True
    end
    object qryArt_80A80_TASA: TFloatField
      DisplayLabel = 'Tasa'
      FieldName = 'A80_TASA'
      Origin = 'DBSUGERIDO."art_80.DB".A80_TASA'
      DisplayFormat = '0.00 %'
      EditFormat = '0.00 %'
    end
  end
  object updArt_80: TUpdateSQL
    ModifySQL.Strings = (
      'update art_80'
      'set'
      '  NU_CODIGO = :NU_CODIGO,'
      '  A80_LI = :A80_LI,'
      '  A80_CUOTA = :A80_CUOTA,'
      '  A80_TASA = :A80_TASA'
      'where'
      '  NU_CODIGO = :OLD_NU_CODIGO and'
      '  A80_LI = :OLD_A80_LI')
    InsertSQL.Strings = (
      'insert into art_80'
      '  (NU_CODIGO, A80_LI, A80_CUOTA, A80_TASA)'
      'values'
      '  (:NU_CODIGO, :A80_LI, :A80_CUOTA, :A80_TASA)')
    DeleteSQL.Strings = (
      'delete from art_80'
      'where'
      '  NU_CODIGO = :OLD_NU_CODIGO and'
      '  A80_LI = :OLD_A80_LI')
    Left = 240
    Top = 80
  end
end
