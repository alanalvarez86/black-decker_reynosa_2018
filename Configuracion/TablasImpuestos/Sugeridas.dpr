program Sugeridas;

uses
  Forms,
  Unit1 in 'Unit1.pas' {FormaPrincipal},
  FISPT in 'FISPT.pas' {FormaISPT},
  DSugeridas in 'DSugeridas.pas' {dmSugeridas: TDataModule};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TdmSugeridas, dmSugeridas);
  Application.CreateForm(TFormaPrincipal, FormaPrincipal);
  Application.CreateForm(TFormaISPT, FormaISPT);
  Application.Run;
end.
