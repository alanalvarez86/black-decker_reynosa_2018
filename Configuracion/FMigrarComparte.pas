unit FMigrarComparte;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, FileCtrl,
     Forms, Dialogs, StdCtrls, ComCtrls, Checklst, Buttons, ExtCtrls,
     FMigrar,
     ZetaWizard, jpeg;

type
  TMigrarComparte = class(TMigrar)
    TressDOS: TGroupBox;
    TressDOSPathSeek: TSpeedButton;
    TressDOSPathLBL: TLabel;
    TressDOSPath: TEdit;
    OpenDialog: TOpenDialog;
    Empresas: TTabSheet;
    EmpresasGB: TGroupBox;
    ListaEmpresas: TCheckListBox;
    Parametros: TTabSheet;
    InterbaseGB: TGroupBox;
    InterbaseSeek: TSpeedButton;
    InterbaseLBL: TLabel;
    DatosPathLBL: TLabel;
    DatosPathSeek: TSpeedButton;
    Interbase: TEdit;
    DatosPath: TEdit;
    MigrarUsuariosGrupos: TCheckBox;
    VersionDOS: TComboBox;
    VersionDOSLBL: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure InterbaseSeekClick(Sender: TObject);
    procedure DatosPathSeekClick(Sender: TObject);
  private
    { Private declarations }
    FMigrado: Boolean;
    function GetDataPath: String;
    function GetEsVer260: Boolean;
    function GetInterbaseFile: String;
    function GetMigrarUsuarios: Boolean;
    function GetVersionDosName: String;
    function LlenaEmpresas: Boolean;
  protected
    { Protected declarations }
    function GetSourceDatosPath: String; override;
  public
    { Public declarations }
    property Migrado: Boolean read FMigrado;
  end;

var
  MigrarComparte: TMigrarComparte;

implementation

uses ZetaCommonTools,
     ZetaTressCFGTools,
     ZetaDialogo,
     ZetaWinAPITools,
     FHelpContext,
     DMigrarComparte;

{$R *.DFM}

procedure TMigrarComparte.FormCreate(Sender: TObject);
begin
     FDOSPath := TressDOSPath;
     dmMigracion := TdmMigrarComparte.Create( Self );
     with dmMigracion.Registry do
     begin
          TargetAlias := AliasComparte;
          TargetUserName := UserName;
          TargetPassword := Password;
     end;
     VersionDOS.ItemIndex := 0;
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Comparte.log' );
     Interbase.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Datos\Datos.gdb' );
     DatosPath.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'Datos' );
     inherited;
     HelpContext := H00008_Migrar_comparte;
end;

procedure TMigrarComparte.FormDestroy(Sender: TObject);
begin
     inherited;
     dmMigracion.Free;
end;

function TMigrarComparte.GetSourceDatosPath: String;
begin
     Result := Self.SourceSharedPath;
end;

function TMigrarComparte.GetEsVer260: Boolean;
begin
     Result := ( VersionDOS.ItemIndex = 0 );
end;

function TMigrarComparte.GetVersionDosName: String;
begin
     with VersionDOS do
     begin
          Result := Items[ ItemIndex ];
     end;
end;

function TMigrarComparte.GetInterbaseFile: String;
begin
     Result := Interbase.Text;
end;

function TMigrarComparte.GetDataPath: String;
begin
     Result := ZetaCommonTools.VerificaDir( DatosPath.Text );
end;

function TMigrarComparte.GetMigrarUsuarios: Boolean;
begin
     Result := MigrarUsuariosGrupos.Checked;
end;

procedure TMigrarComparte.InterbaseSeekClick(Sender: TObject);
begin
     inherited;
     with Interbase do
     begin
          with OpenDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFilePath( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TMigrarComparte.DatosPathSeekClick(Sender: TObject);
var
   sPath: String;
begin
     inherited;
     with DatosPath do
     begin
          sPath := Text;
          if SelectDirectory( sPath, [ sdAllowCreate, sdPerformCreate, sdPrompt ], 0 ) then
             Text := sPath;
     end;
end;

function TMigrarComparte.LlenaEmpresas: Boolean;
var
   oCursor: TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        try
           with ListaEmpresas do
           begin
                dmMigracion.LlenaListaEmpresas( Items, SourceSharedPath );
                for i := 0 to ( Items.Count - 1 ) do
                    Checked[ i ] := True;
                ItemIndex := 0;
           end;
           Result := True;
        except
              on Error: Exception do
              begin
                   ShowError( 'Error Al Llenar Lista De Empresas', Error );
                   ActiveControl := FDOSPath;
                   Result := False;
              end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TMigrarComparte.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     if CanMove then
     begin
          if Wizard.Adelante then
          begin
               if Wizard.EsPaginaActual( DirectorioDOS ) then
               begin
                    CanMove := ValidaDbaseDriver and ValidaDirectorioDOS and LlenaEmpresas;
               end
               else
                   if Wizard.EsPaginaActual( Parametros ) then
                   begin
                        if not FileExists( GetInterbaseFile ) then
                        begin
                             ZetaDialogo.zWarning( Caption, 'Archivo de Base De Datos Modelo No Existe', 0, mbOK );
                             ActiveControl := Interbase;
                             CanMove := False;
                        end
                        else
                            if not DirectoryExists( GetDataPath ) then
                            begin
                                 ForceDirectories( GetDataPath );
                                 if not DirectoryExists( GetDataPath ) then
                                 begin
                                      ZetaDialogo.zWarning( Caption, 'Directorio De Bases De Datos No Existe', 0, mbOK );
                                      ActiveControl := DatosPath;
                                      CanMove := False;
                                 end;
                            end;
                   end;
          end;
     end;
end;

procedure TMigrarComparte.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   i: Integer;
begin
     FMigrado := False;
     with TdmMigrarComparte( dmMigracion ).CompanyList do
     begin
          Clear;
          with ListaEmpresas do
          begin
               for i := 0 to ( Items.Count - 1 ) do
               begin
                    if Checked[ i ] then
                       AddObject( Items.Strings[ i ], Items.Objects[ i ] );
               end;
          end;
     end;
     with FParamMsg do
     begin
          Clear;
          Add( Format( 'Versi�n de Tress DOS: %s', [ GetVersionDosName ] ) );
          Add( Format( 'Archivo Modelo:       %s', [ GetInterbaseFile ] ) );
          Add( Format( 'Directorio de Datos:  %s', [ GetDataPath ] ) );
          Add( Format( 'Migrar Usuarios:      %s', [ BoolToYesNo( GetMigrarUsuarios ) ] ) );
     end;
     StartLog;
     with TdmMigrarComparte( dmMigracion ) do
     begin
          EsVer260 := GetEsVer260;
          InterbaseFile := GetInterbaseFile;
          DataPath := GetDataPath;
          ComputerName := ZetaWinAPITools.GetComputerName;
          MigrarUsuarios := GetMigrarUsuarios;
     end;
     if MigrarDatos then
     begin
          lOk := True;
          FMigrado := lOk;
     end;
     EndLog;
     inherited;
end;

end.
