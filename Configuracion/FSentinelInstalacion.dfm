object SentinelInstall: TSentinelInstall
  Left = 289
  Top = 195
  ActiveControl = Clave
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Capturar Clave de Instalaci'#243'n'
  ClientHeight = 140
  ClientWidth = 397
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object EmpresaLBL: TLabel
    Left = 17
    Top = 30
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = '# Empresa:'
  end
  object ClaveLBL: TLabel
    Left = 41
    Top = 72
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cla&ve:'
    FocusControl = Clave
  end
  object SentinelLBL: TLabel
    Left = 20
    Top = 9
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sentinel #:'
  end
  object Sentinel: TZetaTextBox
    Left = 73
    Top = 7
    Width = 65
    Height = 18
    AutoSize = False
    Caption = 'Sentinel'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object Empresa: TZetaTextBox
    Left = 73
    Top = 28
    Width = 65
    Height = 17
    AutoSize = False
    Caption = 'Empresa'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object InstalacionLBL: TLabel
    Left = 7
    Top = 50
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = '# Instalaci'#243'n:'
  end
  object Instalacion: TZetaTextBox
    Left = 73
    Top = 48
    Width = 65
    Height = 17
    AutoSize = False
    Caption = 'Instalacion'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object OK: TBitBtn
    Left = 123
    Top = 101
    Width = 75
    Height = 25
    Hint = 'Grabar Sentinela'
    Caption = '&OK'
    Default = True
    TabOrder = 1
    OnClick = OKClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object Cancelar: TBitBtn
    Left = 216
    Top = 101
    Width = 75
    Height = 25
    Hint = 'Salir Sin Grabar Sentinela'
    Caption = '&Cancelar'
    TabOrder = 2
    Kind = bkCancel
  end
  object Clave: TEdit
    Left = 73
    Top = 68
    Width = 315
    Height = 21
    TabOrder = 0
  end
end
