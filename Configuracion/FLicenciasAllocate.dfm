inherited FAllocateLicences: TFAllocateLicences
  ActiveControl = Licenses
  Caption = 'Asignar Licencias A una Empresa'
  ClientHeight = 150
  ClientWidth = 400
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CompanyLBL: TLabel [0]
    Left = 51
    Top = 9
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa:'
  end
  object Company: TZetaTextBox [1]
    Left = 98
    Top = 7
    Width = 122
    Height = 17
    AutoSize = False
    Caption = 'Company'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object CompanyName: TZetaTextBox [2]
    Left = 98
    Top = 27
    Width = 290
    Height = 17
    AutoSize = False
    Caption = 'CompanyName'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object CompanyNameLBL: TLabel [3]
    Left = 29
    Top = 28
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Raz�n Social:'
  end
  object OldValueLBL: TLabel [4]
    Left = 3
    Top = 67
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Licencias Actuales:'
  end
  object OldValue: TZetaTextBox [5]
    Left = 98
    Top = 66
    Width = 122
    Height = 17
    AutoSize = False
    Caption = 'OldValue'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  object LicensesLBL: TLabel [6]
    Left = 7
    Top = 89
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Licencias Nuevas:'
  end
  object EmpleadosLBL: TLabel [7]
    Left = 40
    Top = 47
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleados:'
  end
  object Empleados: TZetaTextBox [8]
    Left = 98
    Top = 46
    Width = 122
    Height = 17
    AutoSize = False
    Caption = 'Empleados'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
  end
  inherited PanelBotones: TPanel
    Top = 114
    Width = 400
    BevelOuter = bvNone
    TabOrder = 1
    inherited OK: TBitBtn
      Left = 232
      ModalResult = 0
      OnClick = OKClick
    end
    inherited Cancelar: TBitBtn
      Left = 317
    end
  end
  object Licenses: TZetaNumero
    Left = 98
    Top = 85
    Width = 62
    Height = 21
    Mascara = mnDias
    TabOrder = 0
    Text = '0'
  end
end
