unit FClienteTRESS;

interface

uses Windows, Messages, SysUtils, Classes, Graphics,
     Controls, Forms, Dialogs, Db, DBClient,
     {$ifdef DOS_CAPAS}
     DServerSistema,
     {$else}
     Sistema_TLB,
     {$endif}
     {$ifndef VER130}
     Variants,
     {$endif}
     FAutoClasses,
     ZetaCommonClasses,
     ZetaCommonLists,
     ZetaClientDataSet,
     ZetaMigrar;


type
  eOperacion = ( eoCalculaPrenomina,eoCalcularSalarioIntegrado,eoRecalcularTarjetas );
  TClienteTRESS = class
  private
   {$ifdef DOS_CAPAS}
    FServerSistema: TdmServerSistema;
    {$else}
    FServerSistema: IdmServerSistemaDisp;
    function GetServerSistema: IdmServerSistemaDisp;
    {$endif}

 protected
    { Protected declarations }
    {$ifdef DOS_CAPAS}
    property ServerSistema: TdmServerSistema read FServerSistema;
    {$else}
    property ServerSistema: IdmServerSistemaDisp read GetServerSistema;
    {$endif}
  public
    { Public declarations }
     Constructor Create( EmpresaActiva : OleVariant;  sCM_CODIGO : string );
     Destructor  Destroy; override;
     procedure Init;
     procedure ReportaProcessDetail( const iFolio: Integer; Bitacora : TZetaMigrarLog );

end;
implementation


uses
     FAutoServer,
     ZetaCommonTools,
     ZGlobalTress,
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker,
     DZetaServerProvider,
     {$endif}
     DSistema,
     DCliente,
     DBasicoCliente,
     DDiccionario,
     DReportesCliente,
     DConsultas       
     ;

{ TClienteTRESS }
{$ifndef DOS_CAPAS}
function TClienteTRESS.GetServerSistema: IdmServerSistemaDisp;
begin
       Result := IdmServerSistemaDisp( dmCliente.CreaServidor( CLASS_dmServerSistema, FServerSistema ) );
end;
{$endif}





Constructor TClienteTRESS.Create( EmpresaActiva : OleVariant;  sCM_CODIGO : string );
begin
     ZetaCommonTools.Environment;
     {$ifdef DOS_CAPAS}
     DZetaServerProvider.InitAll;
     {$endif}
     dmCliente := TdmCliente.Create( nil );
     dmCliente.InitCompany;
     dmCliente.SetEmpresaActiva ( EmpresaActiva );
     dmCliente.FindCompany( sCM_CODIGO );
     dmCliente.EmpresaAbierta := True;
     dmSistema  := TdmSistema.Create( nil );
     dmDiccionario := TdmDiccionario.Create ( nil );
     dmReportesCliente := TdmReportesCliente.Create ( nil );
     dmConsultas := TdmConsultas.Create ( nil );
     {$ifdef DOS_CAPAS}
     FServerSistema := TdmServerSistema.Create( nil );
     {$endif}
end;

Destructor TClienteTRESS.Destroy;
begin
    {$ifdef DOS_CAPAS}
     FreeAndNil( FServerSistema );
    {$endif}
     FreeAndNil( dmCliente );
     FreeAndNil( dmSistema );
     FreeAndNil( dmDiccionario );
     FreeAndNil( dmReportesCliente );
     FreeAndNil( dmConsultas );
     {$ifdef DOS_CAPAS}
     ZetaSQLBroker.FreeAll(TRUE);
     DZetaServerProvider.FreeAll;
     {$endif}
end;


procedure TClienteTRESS.Init;
begin
     dmSistema.cdsUsuarios.Conectar;
     dmSistema.cdsUsuariosLookup.Conectar;


end;


procedure TClienteTRESS.ReportaProcessDetail( const iFolio: Integer ; Bitacora : TZetaMigrarLog);
const
     K_MENSAJE_BIT = 'Fecha Movimiento: %s - %s';
var
   sMensaje: String;
begin
     with dmConsultas do
     begin
          GetProcessLog( iFolio );
          with cdsLogDetail do
          begin
               if ( not IsEmpty ) then
               begin
                    First;
                    while ( not EOF ) do
                    begin
                         sMensaje := Format( K_MENSAJE_BIT, [ ZetaCommonTools.FechaCorta( FieldByName( 'BI_FEC_MOV' ).AsDateTime ),
                                                              FieldByName( 'BI_DATA' ).AsString ] );
                         case eTipoBitacora( FieldByName( 'BI_TIPO' ).AsInteger ) of
                              tbNormal: Bitacora.AgregaMensaje( sMensaje );
                              tbAdvertencia: Bitacora.AddWarning( 'Advertencia - ' + sMensaje, 'Importando Diccionario');
                              tbError, tbErrorGrave : Bitacora.AgregaError( sMensaje  );
                         end;
                         Next;
                    end;
               end;
          end;
     end;
end;


end.
