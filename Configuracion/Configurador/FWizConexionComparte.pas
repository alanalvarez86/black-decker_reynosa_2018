unit FWizConexionComparte;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxControls, cxGraphics, cxLookAndFeelPainters, cxLookAndFeels,
  dxCustomWizardControl, dxWizardControl, dxWizardControlForm, cxContainer, cxEdit, cxTextEdit, cxLabel, cxMaskEdit,
  cxDropDownEdit, cxGroupBox, cxMemo, cxRichEdit, cxCheckBox, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxCurrencyEdit, cxProgressBar,
   dxSkinsCore, TressMorado2013, ZetaServerTools, Vcl.ExtDlgs, cxShellBrowserDialog, ZetaRegistryServer, ShellApi,
  cxClasses;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TComparteConfig = class(TdxWizardControlForm)
    dxWixardConexionComparte: TdxWizardControl;
    IniConexion: TdxWizardControlPage;
    cxListServers: TcxComboBox;
    cxLabel1: TcxLabel;
    cxUser: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxPassword: TcxTextEdit;
    btnTestCnx: TcxButton;
    gbDatosNuevaBaseDatos: TcxGroupBox;
    cxNombreComparte: TcxTextEdit;
    cxLabel5: TcxLabel;
    cxUsarWF: TcxCheckBox;
    cxRutaDATA: TcxTextEdit;
    cxRuta: TcxLabel;
    cxPath: TcxButton;
    cxLabel4: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel9: TcxLabel;
    dxWizardControlPage1: TdxWizardControlPage;
    cxSize: TcxCurrencyEdit;
    cxMaxSize: TcxCurrencyEdit;
    cxIncrementos: TcxCurrencyEdit;
    txtmsjCrearDB: TcxMemo;
    cxProgress: TcxProgressBar;
    cxOpenDialog: TcxShellBrowserDialog;
    procedure dxWixardConexionComparteButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;var AHandled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure btnTestCnxClick(Sender: TObject);
    procedure cxUsarWFPropertiesChange(Sender: TObject);
    procedure cxPathClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxListServersExit(Sender: TObject);
    procedure cxUserExit(Sender: TObject);
    procedure cxPasswordExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FRegistry: TZetaRegistryServer;
    SC : TSQLConnection;
    function GetConnStr: widestring;
    function GetConnStrRegistry(Server, UserName, Password: String): widestring;
    function ValidaConexion(muestraMensaje: Boolean): Boolean;
    function PrepararBaseComparte(out mensaje: string): Boolean;
    property ConnStr : widestring read GetConnStr;
    function GetDefaultMSSQL: Boolean;
  public
    { Public declarations }
  end;

var
  ComparteConfig: TComparteConfig;

implementation

{$R *.dfm}
uses
    DB, ADODB, ActiveX, ComObj, AdoInt, OleDB, DDBConfig,ZetaCommonClasses,ZetaCommonTools,FConfigShell, ZetaWinAPITools, ZetaDialogo;

function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      10 );
end;

procedure ListAvailableSQLServers(Names : TStrings);
var
RSCon: ADORecordsetConstruction;
Rowset: IRowset;
SourcesRowset: ISourcesRowset;
SourcesRecordset: _Recordset;
SourcesName, SourcesType: TField;
begin
    OleCheck(CoCreateInstance(CLASS_Recordset, nil, CLSCTX_INPROC_SERVER or
    CLSCTX_LOCAL_SERVER, IUnknown, SourcesRecordset) );
    RSCon := SourcesRecordset as ADORecordsetConstruction;
    SourcesRowset := CreateComObject(ProgIDToClassID('SQLOLEDB Enumerator')) as ISourcesRowset;
    OleCheck(SourcesRowset.GetSourcesRowset(nil, IRowset, 0, nil, IUnknown(Rowset)));
    RSCon.Rowset := RowSet;
    with TADODataSet.Create(nil) do
    try
      Recordset := SourcesRecordset;
      SourcesName := FieldByName('SOURCES_NAME'); { do not localize }
      SourcesType := FieldByName('SOURCES_TYPE'); { do not localize }
      Names.BeginUpdate;
    try
        while not EOF do
        begin
            if (SourcesType.AsInteger = DBSOURCETYPE_DATASOURCE)
            and (SourcesName.AsString <>'') then
              Names.Add(SourcesName.AsString);
            Next;
        end;
        finally
          Names.EndUpdate;
        end;
    finally
      Free;
    end;
end;

procedure TComparteConfig.FormCreate(Sender: TObject);
begin
    HelpContext := 10;
    FRegistry := TZetaRegistryServer.Create;
end;

procedure TComparteConfig.FormDestroy(Sender: TObject);
begin
     FreeAndNil( FRegistry );
end;

procedure TComparteConfig.FormShow(Sender: TObject);
var
   isLocalServer: Boolean;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dxWizardControlPage1.Header.Title := VACIO;
        dxWizardControlPage1.Header.Description := VACIO;
        if FRegistry.IsKeyExists(SERVER_SERVER_DB) and FRegistry.IsKeyExists(SERVER_DATABASE) then
        begin
             isLocalServer := DDBConfig.dmDBConfig.IsLocalServer(GetConnStrRegistry(FRegistry.ServerDB, FRegistry.UserName, FRegistry.Password)) = 'local';
             cxRutaDATA.Enabled := not isLocalServer;
             cxPath.Enabled := isLocalServer;
        end
        else
        begin
             cxPath.Enabled := FALSE;
             cxRutaDATA.Enabled := TRUE;
        end;
        ListAvailableSQLServers(cxListServers.Properties.Items);

        with FRegistry do
        begin
             if not (IsKeyExists( SERVER_PATH_CONFIG)) OR (PathConfig = VACIO) then
             begin
                  PathConfig := VerificaDir( Installation ) + 'Configuraci�n';
             end;
        end;

        with FRegistry do
        begin
             cxUser.Text := UserName;
             cxPassword.Text := Password;
             cxListServers.Text := ServerDB;
             if cxListServers.Text = VACIO then
             begin
                  cxListServers.Text := ZetaWinAPITools.GetComputerName;
             end;
             GetDefaultMSSQL;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TComparteConfig.ValidaConexion(muestraMensaje: Boolean): Boolean;
var
   Mensaje:string;
   lOk: Boolean;
begin
    if StrLleno(cxListServers.Text) then
    begin
        if StrLleno(cxUser.Text) then
        begin
             if StrLleno(cxPassword.Text) then
             begin
                  Screen.Cursor := crSQLWait;
                  try
                      if dmDBConfig.ConectarServidor(ConnStr,Mensaje) then
                      begin
                           lOk := true;
                           if muestraMensaje then
                           begin
                                ZetaDialogo.ZInformation('�xito','Conexi�n exitosa.', 0 );
                                dmDBConfig.CerrarServer;
                           end;
                      end
                      else
                          begin
                          lOk := false;
                          ZetaDialogo.ZError('Error',Mensaje,0 );
                          end;
                  finally
                       Screen.Cursor := crDefault;
                  end;
             end
             else
             begin
                  ZetaDialogo.ZError('Error','El campo Clave no puede quedar vac�o',0 );
                  lOk := false;
             end;
        end
        else
        begin
            ZetaDialogo.ZError('Error','El campo Usuario no puede quedar vac�o', 0 );
            lOk := false;
        end;
    end
    else
    begin
         ZetaDialogo.ZError('Error','El campo Servidor no puede quedar vac�o', 0 );
         lOk := false;
    end;
    Result := lOk;
end;

procedure TComparteConfig.btnTestCnxClick(Sender: TObject);
begin
     ValidaConexion(true);
end;

procedure TComparteConfig.cxListServersExit(Sender: TObject);
begin
     GetDefaultMSSQL;
end;

function TComparteConfig.GetDefaultMSSQL: Boolean;
var
   Mensaje : String;
   isLocalServer: Boolean;
begin
     if StrLleno(cxListServers.Text) then
     begin
         if StrLleno(cxUser.Text) then
         begin
             if StrLleno(cxPassword.Text) then
             begin
                  SC.ServerName := cxListServers.Text;
                  if dmDBConfig.EsConexionValida(ConnStr,Mensaje) then
                  begin
                       with FRegistry do
                       begin
                            cxRutaDATA.Text :=  dmDBConfig.GetPathDefaultSQLServer(ConnStr);
                       end;

                      isLocalServer := DDBConfig.dmDBConfig.IsLocalServer(ConnStr) = 'local';
                      cxRutaDATA.Enabled := not isLocalServer;
                      cxPath.Enabled := isLocalServer;
                  end
                  else
                  begin
                       if FRegistry.IsKeyExists(SERVER_SERVER_DB) and FRegistry.IsKeyExists(SERVER_DATABASE) then
                       begin
                            isLocalServer := DDBConfig.dmDBConfig.IsLocalServer(GetConnStrRegistry(FRegistry.ServerDB, FRegistry.UserName, FRegistry.Password)) = 'local';
                            cxRutaDATA.Enabled := not isLocalServer;
                            cxPath.Enabled := isLocalServer;
                       end
                       else
                       begin
                            cxPath.Enabled := FALSE;
                            cxRutaDATA.Enabled := TRUE;
                       end;
                  end;
             end;
         end;
     end;
end;

procedure TComparteConfig.cxPasswordExit(Sender: TObject);
begin
    GetDefaultMSSQL;
end;

procedure TComparteConfig.cxPathClick(Sender: TObject);
begin
      cxOpenDialog.Path := ExtractFilePath( cxRutaDATA.Text + '\');
      if cxOpenDialog.Execute then
      begin
           cxRutaDATA.Text := cxOpenDialog.Path;
      end;
end;

procedure TComparteConfig.cxUsarWFPropertiesChange(Sender: TObject);
begin
     if cxUsarWF.Checked then
     begin
         cxSize.EditValue := cxSize.EditValue + 70;
         cxMaxSize.EditValue := cxMaxSize.EditValue + 100;
         cxIncrementos.EditValue := cxIncrementos.EditValue + 20;
     end
     else
     begin
         cxSize.EditValue := cxSize.EditValue - 70;
         cxMaxSize.EditValue := cxMaxSize.EditValue - 100;
         cxIncrementos.EditValue := cxIncrementos.EditValue - 20;
     end;
end;

procedure TComparteConfig.cxUserExit(Sender: TObject);
begin
    GetDefaultMSSQL;
end;

procedure TComparteConfig.dxWixardConexionComparteButtonClick(Sender: TObject; AKind: TdxWizardControlButtonKind;var AHandled: Boolean);
var
   mensaje : string;
   oCursor: TCursor;
   lOk : Boolean;
begin
     lOk := FALSE;
     if AKind = wcbkNext then
     begin
          if ValidaConexion(FALSE) then
          begin
              if cxNombreComparte.Text = VACIO  then
              begin
                   ZetaDialogo.ZError('Error','El campo base de datos no puede quedar vac�o.', 0);
                   AHandled := True;
              end
              else if DDBConfig.dmDBConfig.DataBaseExist( ConnStr, cxNombreComparte.Text) then
              begin
                   ZetaDialogo.ZError('Error','El nombre de la base de datos ya existe en el servidor seleccionado.', 0);
                   AHandled := True;
              end
              else if cxRutaDATA.Text = VACIO then
              begin
                    ZetaDialogo.ZError('Error','El campo ruta de base de datos no puede quedar vac�o.', 0);
                    AHandled := True;
              end;
          end
          else
          begin
               AHandled := True;
          end;
     end;
     if AKind = wcbkFinish then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
            //cxProgress.Properties.Marquee := True;
            cxProgress.Properties.ShowText := True;
            cxProgress.Properties.Text := 'Creando base de datos Comparte ...';
            dxWixardConexionComparte.Buttons.Back.Enabled := FALSE;
            dxWixardConexionComparte.Buttons.Finish.Enabled := FALSE;
            dxWixardConexionComparte.Buttons.Cancel.Enabled := FALSE;
            lOk := PrepararBaseComparte(mensaje);
            cxProgress.Properties.Text := 'Termino proceso';
            cxProgress.Position := 100;
            //cxProgress.Properties.Marquee := False;
          finally
             Screen.Cursor := oCursor;
          end;
          if lOk  then
          begin
             ZetaDialogo.ZInformation('�xito','La base de datos de COMPARTE fue creada exitosamente.', 0);
          end
          else
          begin
             ZetaDialogo.ZError('Error','La base de datos de COMPARTE termin� con errores. '+ mensaje, 0);
          end;

          if ZetaDialogo.zConfirm( 'Confirmaci�n', '� Desea ver la bit�cora del proceso ?', 0, mbOk ) then
          begin
              ExecuteFile( 'NOTEPAD.EXE', ExtractFileName( DDBConfig.dmDBConfig.Bitacora.FileName ), ExtractFilePath( DDBConfig.dmDBConfig.Bitacora.FileName ) );
          end;
     end;

     if ModalResult = mrCancel  then
     begin
          Close;
     end;
          
     if AKind = wcbkCancel  then
     begin
          Close;
     end;
end;

function TComparteConfig.PrepararBaseComparte(out mensaje: string): Boolean;
var
   lOk, lErrorEstructura, lErrorXML : Boolean;
begin
     lOk := FALSE;
     try
         SC.DatabaseName := 'master';
         lOk := DDBConfig.dmDBConfig.CrearComparte(ConnStr,cxNombreComparte.Text ,cxRutaDATA.Text, cxSize.EditValue, cxIncrementos.EditValue);
         if lOk then
         begin
             SC.DatabaseName := cxNombreComparte.Text;
             lOk :=DDBConfig.dmDBConfig.LeeScript(ConnStr, lErrorEstructura);

             if lOk then
             begin
                 DDBConfig.dmDBConfig.GetArrayEmpresa(SC.DatabaseName, SC.UserName, Encrypt(SC.Password), '', SC.DatabaseName);
                 lOk := DDBConfig.dmDBConfig.LeeXML(ConnStr, lErrorXML);
                 if not lErrorEstructura then
                 begin
                      try
                      begin
                           with FRegistry do
                           begin
                                ServerDB := cxListServers.Text;
                                UserName := cxUser.Text;
                                Password := cxPassword.Text;
                                DataBaseADO := cxNombreComparte.Text;
                                Database := cxListServers.Text + '.' + cxNombreComparte.Text;
                                PathMSSQL := cxRutaDATA.Text;
                                ModalResult := mrOk;
                           end;
                           end;
                          except
                          on e:exception do
                          begin
                               ZetaDialogo.ZError('Error','Hubo un error al tratar de registrar Comparte '+ e.Message,0);
                               ModalResult := mrCancel;
                          end;
                      end;
                 end
                 else
                 begin
                      ModalResult := mrCancel;
                 end;
             end
             else
             begin
                  ModalResult := mrCancel;
             end;
         end
         else
         begin
             ModalResult := mrCancel;
         end;
     Except on e:exception do
     begin
          ZetaDialogo.ZError('Error', mensaje, 0);     
          lOk := FALSE;
     end;
     end;
     
     Result := lOk;
end;       

function TComparteConfig.GetConnStr: widestring;
begin
    SC.ServerName := cxListServers.Text;
    SC.UserName := cxUser.Text;
    SC.Password := cxPassword.Text;

    Result := Format( 'Provider=%s;', [ FRegistry.SQLNCLIProvider ] );

    Result := Result + 'Data Source=' + Trim(SC.ServerName) + ';';
    if SC.DatabaseName <> '' then
      Result := Result + 'Initial Catalog=' + Trim(SC.DatabaseName) + ';';

    Result := Result + 'uid=' + Trim(SC.UserName) + ';';
    Result := Result + 'pwd=' + Trim(SC.Password) + ';';
    Result := Result + 'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80;Language=english';
end;

function TComparteConfig.GetConnStrRegistry(Server, UserName, Password: String): widestring;
begin
    SC.ServerName := Server;
    SC.DatabaseName := '';
    SC.UserName := UserName;
    SC.Password := Password;

    Result := Format( 'Provider=%s;', [ FRegistry.SQLNCLIProvider ] );

    Result := Result + 'Data Source=' + Trim(SC.ServerName) + ';';
    if SC.DatabaseName <> '' then
        Result := Result + 'Initial Catalog=' + Trim(SC.DatabaseName) + ';';

    Result := Result + 'uid=' + Trim(SC.UserName) + ';';
    Result := Result + 'pwd=' + Trim(SC.Password) + ';';
    Result := Result + 'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80;Language=english';
end;


end.
