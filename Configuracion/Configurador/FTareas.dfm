object FTarea: TFTarea
  Left = 406
  Top = 137
  Caption = 'FTarea'
  ClientHeight = 366
  ClientWidth = 545
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btnTareas: TButton
    Left = 288
    Top = 216
    Width = 75
    Height = 25
    Caption = 'btnTareas'
    TabOrder = 0
    OnClick = btnTareasClick
  end
  object txtNombrePC: TEdit
    Left = 328
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 1
    Text = 'txtNombrePC'
  end
  object txtNombreServicio: TEdit
    Left = 184
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 2
    Text = 'txtNombreServicio'
  end
  object opIniciar: TCheckBox
    Left = 104
    Top = 80
    Width = 97
    Height = 17
    Caption = 'opIniciar'
    TabOrder = 3
  end
end
