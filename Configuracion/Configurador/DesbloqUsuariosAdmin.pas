unit DesbloqUsuariosAdmin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, DDBConfig, Vcl.ComCtrls, dxSkinsCore, TressMorado2013;

type
  TDesbloqueaUsuariosAdmin = class(TForm)
    cxOK: TcxButton;
    cxCancelar: TcxButton;
    mensajeDesbloquea: TRichEdit;
    procedure cxOKClick(Sender: TObject);
    procedure cxCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DesbloqueaUsuariosAdmin: TDesbloqueaUsuariosAdmin;

implementation

{$R *.dfm}
uses FConfigShell;

procedure TDesbloqueaUsuariosAdmin.cxCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TDesbloqueaUsuariosAdmin.cxOKClick(Sender: TObject);
begin
     if not dmDBConfig.DesbloqueaAdministradores then
     begin
          ShowMessage('Hubo un problema al tratar de desbloquear a los Administradores');
     end;
end;

procedure TDesbloqueaUsuariosAdmin.FormCreate(Sender: TObject);
begin
     HelpContext := 9;
end;

end.

