unit FWizInstalacionServicio_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZetaEdit, ZetaDialogo, DB, ADODB, Mask, DBClient, ZetaClientDataSet,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxContainer, cxEdit,
  ZetaCXWizard, dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox,
  dxCustomWizardControl, dxWizardControl, cxRadioGroup, cxTextEdit, cxMemo,
  Vcl.Menus, ZetaHora, ZetaNumero, cxButtons, Vcl.ExtDlgs;

type
  TWizInstalacionServicio_DevEx = class(TcxBaseWizard)
    rdServCafeteria: TRadioButton;
    rdServCorreos: TRadioButton;
    rdServReportes: TRadioButton;
    Reportes: TdxWizardControlPage;
    Correos: TdxWizardControlPage;
    Cafeteria: TdxWizardControlPage;
    lblCafeteraServicio: TLabel;
    edCafeteraServicio: TEdit;
    cxBuscarServicioCafetera: TcxButton;
    edNombreInstancia: TEdit;
    lblCafeteraInstancia: TLabel;
    lblCafeteraPuerto: TLabel;
    edtPuerto: TZetaNumero;
    lblCafeteraValores: TLabel;
    lblCafeteraFormato: TLabel;
    edtHora: TZetaHora;
    lblCafeteraHora: TLabel;
    lblCorreosTitulo: TLabel;
    edServidorCorreos: TEdit;
    cxBuscarServicioCorreos: TcxButton;
    lblTituloServidorReportes: TLabel;
    edServidorReportes: TEdit;
    cxBuscarServicioReportes: TcxButton;
    edServidorReportesUsuario: TEdit;
    edServidorReportesClave: TEdit;
    lblTituloServidorReportesUsario: TLabel;
    lblTituloServidorReportesClave: TLabel;
    cxOpenDialog: TOpenTextFileDialog;
    edServidorCorreosClave: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edServidorCorreosUsuario: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure cxBuscarServicioReportesClick(Sender: TObject);
    procedure cxBuscarServicioCorreosClick(Sender: TObject);
    procedure cxBuscarServicioCafeteraClick(Sender: TObject);
    procedure edNombreInstanciaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    FTipoServicio : Integer;
    procedure ResetPaginas;
    procedure ChecarTipoServicio;
    procedure Escogerjecutable;
    function CafeteriaNombre : String;
    function InstalarServicio(ServiceName: String) : Boolean;
    procedure RegistrarServicio(Path: String; Port: Integer; Schedule: String; ServiceName: string; ServiceDescription: String);
    function ValidarUsuario(const UserName: string;const Domain: string; const PassWord: string): boolean;
  public
    { Public declarations }
  protected
   { Protected declarations }
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  end;

const
     K_SERV_CAFETERIA = 1;
     K_SERV_CORREOS   = 2;
     K_SERV_REPORTES  = 3;

     // Constantes de CafeteraServicio, Tress Caseta y Tress Cafeteria
     SERVICE_CAFETERIA_BASE_NAME = 'CafeteraServicio';
     SERVICE_CORREO_BASE_NAME = 'TressCorreosServicio';
     SERVICE_REPORTES_BASE_NAME  = 'TressReportesServicio';

     SERVICE_CAFETERIA_DISPLAYNAME = 'Servidor de Cafeter�a/Caseta';
     SERVICE_CORREO_DISPLAYNAME = 'TRESS Correos';
     SERVICE_REPORTES_DISPLAYNAME = 'TRESS Reportes';

     SERVICE_CAFETERIA_DESCRIPTION = 'Provee servicios de validaci�n de reglas para los clientes Tress Cafeter�a y Tress Caseta';
     SERVICE_CORREO_DESCRIPTION = 'Encargado de la generaci�n y env�o de correos electr�nicos que puedan ser enviados desde Sistema TRESS';
     SERVICE_REPORTES_DESCRIPTION = 'Genera los diferentes reportes que se tengan configurados dentro del Calendario de Sistema TRESS';

     K_CAFETERA_PUERTO    = 33100;            // Puerto TCP predeterminado del servidor CafeteraServicio

     K_ERROR_SERVICIO = 'La ruta donde se encuentra el servicio no es v�lida.';
     K_ERROR_USUARIO = 'Recuerde indicar el dominio y el usuario en el siguente formato: Dominio\Usuario.';
     K_ERROR_CREDENCIALES = 'El usuario o la clave son incorrectos, por favor revise las credenciales.';
     K_ERROR_PWD_VACIO = 'La clave no puede quedar vac�a.';
     K_ERROR_USR_VACIO = 'El usuario no puede quedar vac�o.';

var
  WizInstalacionServicio_DevEx: TWizInstalacionServicio_DevEx;

implementation

uses ZetaCommonTools, ZetaWinAPITools, ZetaCommonClasses, ZetaRegistryServer, Registry;

{$R *.dfm}

procedure TWizInstalacionServicio_DevEx.FormCreate(Sender: TObject);
var
   damage : Boolean;
begin
     inherited;
     FTipoServicio := K_SERV_CAFETERIA;
     edtPuerto.Valor := K_CAFETERA_PUERTO;
     ResetPaginas;

     rdServCorreos.Enabled := Not (ZetaWinAPITools.FindService(SERVICE_CORREO_BASE_NAME));
     rdServReportes.Enabled := Not (ZetaWinAPITools.FindService(SERVICE_REPORTES_BASE_NAME));
end;

function TWizInstalacionServicio_DevEx.ValidarUsuario(const UserName: string;const Domain: string; const PassWord: string): boolean;
var
   Retvar: boolean;
   LHandle: THandle;
begin
     RetVar := False;
     if StrLleno(Domain) and StrLleno(UserName) then
     begin
          Retvar := LogonUser(PChar(UserName), PChar(Domain), PChar(PassWord), LOGON32_LOGON_NETWORK, LOGON32_PROVIDER_DEFAULT, LHandle);
          if Retvar then
             CloseHandle(LHandle);
     end;

     Result := Retvar;
end;

procedure TWizInstalacionServicio_DevEx.CargaParametros;
var sNombre : String;
begin
     with Descripciones do
     begin
          Clear;
          case FTipoServicio of
               K_SERV_CAFETERIA :
               begin
                    AddString( 'Cafetera servicio', edCafeteraServicio.Text );
                    AddString( 'Nombre instancia', edNombreInstancia.Text );
                    AddString( 'Puerto TCP', edtPuerto.Text );
                    AddString( 'Hora de reinicio', edtHora.Text );
               end;
               K_SERV_CORREOS :
               begin
                    AddString( 'Servidor de correos', edServidorCorreos.Text );
                    AddString( 'Usuario', edServidorCorreosUsuario.Text );
               end;
               K_SERV_REPORTES :
               begin
                    AddString( 'Servidor de reportes email', edServidorReportes.Text );
                    AddString( 'Usuario', edServidorReportesUsuario.Text );
               end;
          end;
     end;

     inherited;
end;

function TWizInstalacionServicio_DevEx.EjecutarWizard: Boolean;
var
   sNombre : String;
begin
     inherited;
     case FTipoServicio of
          K_SERV_CAFETERIA : sNombre := CafeteriaNombre;
          K_SERV_CORREOS : sNombre := SERVICE_CORREO_BASE_NAME ;
          K_SERV_REPORTES : sNombre := SERVICE_REPORTES_BASE_NAME;
     end;

     Result := InstalarServicio(sNombre);
     if Not Result then
         ZetaDialogo.ZError(Self.Caption, 'Hubo un error al instalar el servicio ' + sNombre, 0 );
end;

procedure TWizInstalacionServicio_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if ( WizardControl.ActivePage = Parametros ) then
     begin
          case FTipoServicio of
               K_SERV_CAFETERIA : ActiveControl := rdServCafeteria;
               K_SERV_CORREOS : ActiveControl := rdServCorreos;
               K_SERV_REPORTES : ActiveControl := rdServReportes;
          end;
     end
     else
         if ( WizardControl.ActivePage = Cafeteria ) then
            ActiveControl := edCafeteraServicio
         else
             if ( WizardControl.ActivePage = Correos ) then
                ActiveControl := edServidorCorreos
             else
                 if ( WizardControl.ActivePage = Reportes ) then
                    ActiveControl := edServidorReportes;
end;

procedure TWizInstalacionServicio_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
procedure Split(Delimiter: Char; Str: string; ListOfStrings: TStrings) ;
begin
   ListOfStrings.Clear;
   ListOfStrings.Delimiter     := Delimiter;
   ListOfStrings.DelimitedText := Str;
end;
var
   sNombre : String;
   lCredenciales: TStringList;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if Wizard.EsPaginaActual( Parametros )then
          begin
               ResetPaginas;
               ChecarTipoServicio;
               case FTipoServicio of
                    K_SERV_CAFETERIA :
                    begin
                         Cafeteria.PageVisible := True;
                         Cafeteria.PageIndex := 1;
                    end;
                    K_SERV_CORREOS :
                    begin
                         Correos.PageVisible := True;
                         Correos.PageIndex := 1;
                    end;
                    K_SERV_REPORTES :
                    begin
                         Reportes.PageVisible := True;
                         Reportes.PageIndex := 1;
                    end;
               end;

               Ejecucion.PageIndex := 2;
          end
          else
          begin
               if Wizard.EsPaginaActual( Cafeteria )then
               begin
                    if Not (FileExists(edCafeteraServicio.Text)) then
                    begin
                         CanMove := Error( K_ERROR_SERVICIO, WizardControl.ActivePage );
                         ActiveControl := edCafeteraServicio;
                    end
                    else
                    begin
                         if not((edtPuerto.ValorEntero > 0) and (edtPuerto.ValorEntero < 65536)) then
                         begin
                              CanMove := Error( 'El puerto TCP no est� en el rango permitido.', WizardControl.ActivePage );
                              edtPuerto.SetFocus;
                         end
                         else
                         begin
                              sNombre := CafeteriaNombre;
                              if (ZetaWinAPITools.FindService(sNombre)) then
                              begin
                                   CanMove := Error('Instancia ' + sNombre + ' de CafeteraServicio ya se encuentra registrada.', WizardControl.ActivePage );
                                   edtPuerto.SetFocus;
                              end
                         end
                    end
               end
               else
               begin
                    if Wizard.EsPaginaActual( Correos )then
                    begin
                         if Not (FileExists(edServidorCorreos.Text)) then
                         begin
                              CanMove := Error( K_ERROR_SERVICIO, WizardControl.ActivePage );
                              ActiveControl := edServidorCorreos;
                         end
                         else
                         begin
                              if StrVacio(edServidorCorreosUsuario.Text) then
                              begin
                                   CanMove := Error( K_ERROR_USR_VACIO, WizardControl.ActivePage );
                                   ActiveControl := edServidorCorreosUsuario;
                              end
                              else
                              begin
                                   if StrVacio(edServidorCorreosClave.Text) then
                                   begin
                                        CanMove := Error( K_ERROR_PWD_VACIO, WizardControl.ActivePage );
                                        ActiveControl := edServidorCorreosClave;
                                   end
                                   else
                                   begin
                                        lCredenciales := TStringList.Create;
                                        try
                                           Split('\',edServidorCorreosUsuario.Text, lCredenciales);
                                           if (lCredenciales.Count > 1) then
                                           begin
                                                if NOT ValidarUsuario(lCredenciales[1], lCredenciales[0], edServidorCorreosClave.Text) or (lCredenciales.Count = 1) then
                                                begin
                                                     CanMove := Error( K_ERROR_CREDENCIALES , WizardControl.ActivePage );
                                                     ActiveControl := edServidorCorreosUsuario;
                                                end
                                           end
                                           else
                                           begin
                                                CanMove := Error( K_ERROR_USUARIO, WizardControl.ActivePage );
                                                ActiveControl := edServidorCorreosUsuario;
                                           end
                                        finally
                                                lCredenciales.Free;
                                        end
                                   end
                              end
                         end
                    end
                    else
                    begin
                          if Wizard.EsPaginaActual( Reportes )then
                          begin
                               if Not (FileExists(edServidorReportes.Text)) then
                               begin
                                    CanMove := Error( K_ERROR_SERVICIO, WizardControl.ActivePage );
                                    ActiveControl := edServidorReportes;
                               end
                               else
                               begin
                                    if StrVacio(edServidorReportesUsuario.Text) then
                                    begin
                                         CanMove := Error( K_ERROR_USR_VACIO, WizardControl.ActivePage );
                                         ActiveControl := edServidorReportesUsuario;
                                    end
                                    else
                                    begin
                                         if StrVacio(edServidorReportesClave.Text) then
                                         begin
                                              CanMove := Error( K_ERROR_PWD_VACIO , WizardControl.ActivePage );
                                              ActiveControl := edServidorReportesClave;
                                         end
                                         else
                                         begin
                                              lCredenciales := TStringList.Create;
                                              try
                                                 Split('\',edServidorReportesUsuario.Text, lCredenciales);
                                                 if (lCredenciales.Count > 1) then
                                                 begin
                                                      if NOT ValidarUsuario(lCredenciales[1], lCredenciales[0], edServidorReportesClave.Text) or (lCredenciales.Count = 1) then
                                                      begin
                                                           CanMove := Error( K_ERROR_CREDENCIALES, WizardControl.ActivePage );
                                                           ActiveControl := edServidorReportesUsuario;
                                                      end
                                                 end
                                                 else
                                                 begin
                                                      CanMove := Error( K_ERROR_USUARIO, WizardControl.ActivePage );
                                                      ActiveControl := edServidorReportesUsuario;
                                                 end
                                              finally
                                                     lCredenciales.Free;
                                              end
                                         end
                                    end
                               end
                          end
                    end
               end
          end
     end
end;

procedure TWizInstalacionServicio_DevEx.cxBuscarServicioCafeteraClick(Sender: TObject);
begin
     inherited;
     Escogerjecutable;
end;

procedure TWizInstalacionServicio_DevEx.cxBuscarServicioCorreosClick(Sender: TObject);
begin
     inherited;
     Escogerjecutable;
end;

procedure TWizInstalacionServicio_DevEx.cxBuscarServicioReportesClick(Sender: TObject);
begin
     inherited;
     Escogerjecutable;
end;

procedure TWizInstalacionServicio_DevEx.edNombreInstanciaKeyPress(Sender: TObject; var Key: Char);
begin
     inherited;
     if Key = ' ' then
        Key := #0;
end;

procedure TWizInstalacionServicio_DevEx.ResetPaginas;
begin
     Cafeteria.PageVisible := False;
     Correos.PageVisible := False;
     Reportes.PageVisible := False;
end;

procedure TWizInstalacionServicio_DevEx.ChecarTipoServicio;
begin
      if rdServCafeteria.Checked then
         FTipoServicio := K_SERV_CAFETERIA
      else if rdServCorreos.Checked then
           FTipoServicio := K_SERV_CORREOS
      else if rdServReportes.Checked then
           FTipoServicio := K_SERV_REPORTES;
end;

procedure TWizInstalacionServicio_DevEx.Escogerjecutable;
begin
     if cxOpenDialog.Execute then
     begin
           case FTipoServicio of
                K_SERV_CAFETERIA : edCafeteraServicio.Text := cxOpenDialog.FileName;
                K_SERV_CORREOS : edServidorCorreos.Text := cxOpenDialog.FileName;
                K_SERV_REPORTES :  edServidorReportes.Text := cxOpenDialog.FileName;
           end;
     end;
end;

function TWizInstalacionServicio_DevEx.CafeteriaNombre: String;
var
   sNombre : String;
begin
     sNombre := edNombreInstancia.Text;
     if StrLleno(sNombre) then
        sNombre := SERVICE_CAFETERIA_BASE_NAME + '_' + sNombre
     else
         sNombre := SERVICE_CAFETERIA_BASE_NAME;

     Result := sNombre;
end;

function TWizInstalacionServicio_DevEx.InstalarServicio(ServiceName: String): Boolean;
const
     K_INSTANCIA =' /INSTANCIA ';
var
   sUser, sPwd, DisplayName, ServicePath :String;
begin
     Result := False;
     case FTipoServicio of
          K_SERV_CAFETERIA :
          begin
               RegistrarServicio(edCafeteraServicio.Text, StrToInt(edtPuerto.Text), edtHora.Text, ServiceName, SERVICE_CAFETERIA_DESCRIPTION);
               DisplayName := Format('%s (%s)', [SERVICE_CAFETERIA_DISPLAYNAME, ServiceName]);
               ServicePath := '"' + edCafeteraServicio.Text + '"' + K_INSTANCIA + edNombreInstancia.Text;
          end;
          K_SERV_CORREOS :
          begin
               RegistrarServicio(edServidorCorreos.Text, 0, VACIO, ServiceName, SERVICE_CORREO_DESCRIPTION);
               DisplayName := Format('%s', [SERVICE_CORREO_DISPLAYNAME]);
               ServicePath := edServidorCorreos.Text;
               sUser := edServidorCorreosUsuario.Text;
               sPwd := edServidorCorreosClave.Text
          end;
          K_SERV_REPORTES :
          begin
               RegistrarServicio(edServidorReportes.Text, 0, VACIO, ServiceName, SERVICE_REPORTES_DESCRIPTION);
               DisplayName := Format('%s', [SERVICE_REPORTES_DISPLAYNAME]);
               ServicePath := edServidorReportes.Text;
               sUser := edServidorReportesUsuario.Text;
               sPwd := edServidorReportesClave.Text
          end;
     end;

     Result := (ZetaWinAPITools.AddService(ServiceName, DisplayName, ServicePath, VACIO, sUser, sPwd));
end;

procedure TWizInstalacionServicio_DevEx.RegistrarServicio(Path: String; Port: Integer; Schedule: String; ServiceName: string; ServiceDescription: String);
const
     P_PUERTO = 'PUERTO';
     P_HORA   = 'HORA';
var
   Reg: TRegistry;
   Hora: TDateTime;
begin
     if AnsiPos('32', ZetaWinAPITools.GetInfoFileDescription(Path)) > 0 then
        Reg := TRegistry.Create(KEY_READ or KEY_WRITE) //Escribe en Nodo de 32Bits
     else
         Reg := TRegistry.Create (KEY_ALL_ACCESS or KEY_WOW64_64KEY); //Escribe en nodo de 64Bits

     try
        Reg.RootKey := HKEY_LOCAL_MACHINE;
        //Agrega la descripcion para el servicio
        if Reg.OpenKey('\SYSTEM\CurrentControlSet\Services\' + ServiceName, True) then begin
           Reg.WriteInteger('DelayedAutostart', 1);
           Reg.WriteString('Description', ServiceDescription);
           Reg.CloseKey;
        end;

        if FTipoServicio = K_SERV_CAFETERIA then
        begin
             //Agregar el puerto del servicio al registry
             if Reg.OpenKey('\Software\Grupo Tress\' + ServiceName, True) then
             begin
                  Reg.WriteString(P_PUERTO, IntToStr(Port));

                  // Hora de reinicio de reglas
                  Hora := EncodeTime(StrToIntDef(Copy(Schedule, 1, 2), 0), StrToIntDef(Copy(Schedule, 3, 2), 0), 0, 0);
                  Reg.WriteDateTime(P_HORA, Hora);
                  Reg.CloseKey;
             end
        end;
     Finally
            FreeAndNil(Reg);
     end;
end;

end.
