object ComparteConfig: TComparteConfig
  Left = 402
  Top = 153
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Crear base de datos de COMPARTE'
  ClientHeight = 478
  ClientWidth = 668
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = True
  PopupMode = pmExplicit
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxWixardConexionComparte: TdxWizardControl
    Left = 0
    Top = 0
    Width = 668
    Height = 478
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    AutoSize = True
    Buttons.Back.Caption = '&Anterior'
    Buttons.Cancel.Caption = '&Cancelar'
    Buttons.CustomButtons.Buttons = <>
    Buttons.Finish.Caption = 'A&plicar'
    Buttons.Help.Caption = '&Ayuda'
    Buttons.Help.Visible = False
    Buttons.Next.Caption = 'C&ontinuar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Header.AssignedValues = [wchvDescriptionFont, wchvGlyphVisibility, wchvTitleFont]
    Header.DescriptionFont.Charset = DEFAULT_CHARSET
    Header.DescriptionFont.Color = clDefault
    Header.DescriptionFont.Height = -11
    Header.DescriptionFont.Name = 'Segoe UI'
    Header.DescriptionFont.Style = []
    Header.GlyphVisibility = wcevAlwaysVisible
    Header.TitleFont.Charset = DEFAULT_CHARSET
    Header.TitleFont.Color = clDefault
    Header.TitleFont.Height = -11
    Header.TitleFont.Name = 'Segoe UI'
    Header.TitleFont.Style = []
    InfoPanel.Font.Charset = DEFAULT_CHARSET
    InfoPanel.Font.Color = clDefault
    InfoPanel.Font.Height = -11
    InfoPanel.Font.Name = 'Segoe UI'
    InfoPanel.Font.Style = []
    LookAndFeel.Kind = lfOffice11
    OptionsAnimate.ResizeAnimation = wcraEnabled
    OptionsAnimate.TransitionEffect = wcteSlide
    OptionsViewStyleAero.EnableTitleAero = False
    OptionsViewStyleAero.Title.Font.Charset = DEFAULT_CHARSET
    OptionsViewStyleAero.Title.Font.Color = clDefault
    OptionsViewStyleAero.Title.Font.Height = -11
    OptionsViewStyleAero.Title.Font.Name = 'Segoe UI'
    OptionsViewStyleAero.Title.Font.Style = []
    ParentFont = False
    OnButtonClick = dxWixardConexionComparteButtonClick
    object IniConexion: TdxWizardControlPage
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Header.AssignedValues = [wchvDescriptionFont, wchvGlyph, wchvGlyphVisibility, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Segoe UI'
      Header.DescriptionFont.Style = []
      Header.GlyphVisibility = wcevAlwaysVisible
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Segoe UI'
      Header.TitleFont.Style = []
      Header.Description = 'Definir la conexi'#243'n y base de datos de COMPARTE.'
      Header.Title = 'Nueva base de datos de COMPARTE'
      object cxListServers: TcxComboBox
        Left = 144
        Top = 2
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        AutoSize = False
        TabOrder = 0
        OnExit = cxListServersExit
        Height = 23
        Width = 214
      end
      object cxLabel1: TcxLabel
        Left = 82
        Top = 4
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        AutoSize = False
        Caption = 'Servidor:'
        ParentColor = False
        Style.Color = clWhite
        Height = 17
        Width = 56
      end
      object cxUser: TcxTextEdit
        Left = 144
        Top = 30
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        AutoSize = False
        TabOrder = 2
        OnExit = cxUserExit
        Height = 20
        Width = 214
      end
      object cxLabel2: TcxLabel
        Left = 86
        Top = 30
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        AutoSize = False
        Caption = 'Usuario:'
        ParentColor = False
        Style.Color = clWhite
        Height = 17
        Width = 52
      end
      object cxLabel3: TcxLabel
        Left = 91
        Top = 58
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        AutoSize = False
        Caption = 'Clave:'
        ParentColor = False
        Style.Color = clWhite
        Height = 17
        Width = 43
      end
      object cxPassword: TcxTextEdit
        Left = 144
        Top = 59
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        AutoSize = False
        Properties.EchoMode = eemPassword
        Properties.PasswordChar = '*'
        TabOrder = 5
        OnExit = cxPasswordExit
        Height = 21
        Width = 214
      end
      object btnTestCnx: TcxButton
        Left = 364
        Top = 54
        Width = 133
        Height = 26
        Hint = 'Prueba la conexi'#243'n con los datos proporcionados'
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Probar Conexi'#243'n'
        OptionsImage.ImageIndex = 0
        OptionsImage.Images = FConfiguradorTRESS.ImageButtons
        OptionsImage.Margin = 1
        OptionsImage.Spacing = 6
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = btnTestCnxClick
      end
      object gbDatosNuevaBaseDatos: TcxGroupBox
        Left = 41
        Top = 86
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Caption = 'Datos'
        TabOrder = 7
        Height = 224
        Width = 520
        object cxNombreComparte: TcxTextEdit
          Left = 101
          Top = 25
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          TabOrder = 0
          Text = 'COMPARTE'
          Width = 214
        end
        object cxLabel5: TcxLabel
          Left = 44
          Top = 25
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Nombre:'
          ParentColor = False
          Style.Color = clWhite
        end
        object cxUsarWF: TcxCheckBox
          Left = 101
          Top = 52
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Usar m'#243'dulo de WorkFlow'
          ParentBackground = False
          ParentColor = False
          Properties.OnChange = cxUsarWFPropertiesChange
          Style.Color = clWhite
          TabOrder = 2
          Width = 172
        end
        object cxRutaDATA: TcxTextEdit
          Left = 153
          Top = 86
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          AutoSize = False
          StyleDisabled.TextColor = clWindowText
          TabOrder = 3
          Height = 21
          Width = 304
        end
        object cxRuta: TcxLabel
          Left = 15
          Top = 89
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Ruta de base de datos:'
          ParentColor = False
          Style.Color = clWhite
        end
        object cxPath: TcxButton
          Left = 460
          Top = 87
          Width = 26
          Height = 19
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = '...'
          TabOrder = 5
          OnClick = cxPathClick
        end
        object cxLabel4: TcxLabel
          Left = 60
          Top = 114
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Tama'#241'o Inicial:'
          ParentColor = False
          Style.Color = clWhite
        end
        object cxLabel7: TcxLabel
          Left = 66
          Top = 142
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Tama'#241'o Max:'
          ParentColor = False
          Style.Color = clWhite
        end
        object cxLabel9: TcxLabel
          Left = 73
          Top = 169
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          Caption = 'Incrementos:'
          ParentColor = False
          Style.Color = clWhite
        end
        object cxSize: TcxCurrencyEdit
          Left = 153
          Top = 113
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          EditValue = 80
          Enabled = False
          Properties.Alignment.Horz = taRightJustify
          Properties.DecimalPlaces = 0
          Properties.DisplayFormat = '0 MB'
          Properties.MaxValue = 9999.000000000000000000
          Properties.MinValue = 5.000000000000000000
          Properties.Nullable = False
          StyleDisabled.BorderColor = clBtnShadow
          StyleDisabled.Color = clWhite
          StyleDisabled.TextColor = clWindowText
          TabOrder = 9
          Width = 72
        end
        object cxMaxSize: TcxCurrencyEdit
          Left = 153
          Top = 140
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          EditValue = 300
          Enabled = False
          Properties.Alignment.Horz = taRightJustify
          Properties.DecimalPlaces = 0
          Properties.DisplayFormat = '0 MB'
          Properties.MaxValue = 9999.000000000000000000
          Properties.MinValue = 5.000000000000000000
          Properties.Nullable = False
          StyleDisabled.BorderColor = clBtnShadow
          StyleDisabled.Color = clWhite
          StyleDisabled.TextColor = clWindowText
          TabOrder = 10
          Width = 72
        end
        object cxIncrementos: TcxCurrencyEdit
          Left = 153
          Top = 167
          Margins.Left = 2
          Margins.Top = 2
          Margins.Right = 2
          Margins.Bottom = 2
          EditValue = 30
          Enabled = False
          Properties.Alignment.Horz = taRightJustify
          Properties.DecimalPlaces = 0
          Properties.DisplayFormat = '0 MB'
          Properties.MaxValue = 9999.000000000000000000
          Properties.MinValue = 5.000000000000000000
          Properties.Nullable = False
          StyleDisabled.BorderColor = clBtnShadow
          StyleDisabled.Color = clWhite
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 72
        end
      end
    end
    object dxWizardControlPage1: TdxWizardControlPage
      Margins.Left = 2
      Margins.Top = 2
      Margins.Right = 2
      Margins.Bottom = 2
      Header.AssignedValues = [wchvDescriptionFont, wchvTitleFont]
      Header.DescriptionFont.Charset = DEFAULT_CHARSET
      Header.DescriptionFont.Color = clDefault
      Header.DescriptionFont.Height = -11
      Header.DescriptionFont.Name = 'Segoe UI'
      Header.DescriptionFont.Style = []
      Header.TitleFont.Charset = DEFAULT_CHARSET
      Header.TitleFont.Color = clDefault
      Header.TitleFont.Height = -11
      Header.TitleFont.Name = 'Segoe UI'
      Header.TitleFont.Style = []
      object txtmsjCrearDB: TcxMemo
        Left = 102
        Top = 2
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Enabled = False
        Lines.Strings = (
          
            'Se proceder'#225' a crear la base de datos de COMPARTE, con la estruc' +
            'tura y datos '
          'iniciales.'
          ''
          'Presione el bot'#243'n Aplicar para iniciar.')
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -12
        Style.Font.Name = 'Segoe UI'
        Style.Font.Style = []
        Style.TransparentBorder = True
        Style.IsFontAssigned = True
        StyleDisabled.Color = clWhite
        StyleDisabled.TextColor = clWindowText
        TabOrder = 0
        Height = 90
        Width = 459
      end
      object cxProgress: TcxProgressBar
        Left = 102
        Top = 171
        Margins.Left = 2
        Margins.Top = 2
        Margins.Right = 2
        Margins.Bottom = 2
        Properties.AnimationSpeed = 20
        Properties.ShowTextStyle = cxtsText
        TabOrder = 1
        Width = 459
      end
    end
  end
  object cxOpenDialog: TcxShellBrowserDialog
    FolderLabelCaption = 'Configuracion'
    Options.TrackShellChanges = False
    ShowButtons = False
    Title = 'Buscar Folder Configuraci'#243'n'
    Left = 582
    Top = 284
  end
end
