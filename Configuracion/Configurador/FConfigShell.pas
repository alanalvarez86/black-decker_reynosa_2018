unit FConfigShell;

interface

uses
  CheckLst, DB, ZetaRegistryServer, FAutoServer, FAutoServerLoader, FSentinelRegistry, DZetaServerProvider, ZetaClientDataSet,
  WinSvc, FileCtrl, TressMorado2013, ComObj,
  // US #7328: Implementar Control de la ayuda en XE5
  // HtmlHelpViewer,
  FHelpManager, SysUtils,

  Forms, dxSkinsCore, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.Menus, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, cxDBData, dxSkinsdxBarPainter, cxClasses, Vcl.Dialogs, Vcl.ExtDlgs, Vcl.ImgList,
  Vcl.Controls, cxShellBrowserDialog, dxBar, dxSkinsForm, cxMemo, cxRichEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridCustomView, cxGrid, cxCheckListBox, cxCheckBox, cxImage, cxMaskEdit, cxDropDownEdit, Vcl.StdCtrls,
  cxButtons, cxTextEdit, cxGroupBox, cxLabel, dxGDIPlusClasses, Vcl.ExtCtrls, cxPC, System.Classes, dxBevel, ZetaDialogo, ZetaRegistryCliente,
  cxLocalization, dxBarBuiltInMenu, AbBase, AbBrowse, AbZBrows, AbZipper, ZcxWizardBasico, Vcl.Buttons;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TStringsCase = (slgbResumen, slgbConfigurar, slgbSentinel, slgbModulos, slgbServidor, slgbServicios, slgbDatos,slgbDesbloqueoAdministradores, slgbSalir, slgbUsuarios, slgFacilitadores );

  TFConfiguradorTRESS = class(TForm)
    dxSkinController1: TdxSkinController;
    dxBarManager1: TdxBarManager;
    lgbConfigurar: TdxBarLargeButton;
    lgbSentinel: TdxBarLargeButton;
    lgbModulos: TdxBarLargeButton;
    lgbServidor: TdxBarLargeButton;
    lgbServicios: TdxBarLargeButton;
    lgbDatos: TdxBarLargeButton;
    dxBarManager1Bar1: TdxBar;
    lgbSalir: TdxBarLargeButton;
    OpcionesPaginas: TcxPageControl;
    cxConfigurar: TcxTabSheet;
    cxSentinel: TcxTabSheet;
    cxModulos: TcxTabSheet;
    cxServidorProcesos: TcxTabSheet;
    cxServicios: TcxTabSheet;
    dxBevel1: TdxBevel;
    cxGBSentinel: TcxGroupBox;
    lblLicenciaSentinel: TcxLabel;
    cxArchivoLicencia: TcxTextEdit;
    lblServidor: TcxLabel;
    cxCompNam: TcxTextEdit;
    cxLicenciaSentinel: TcxButton;
    cxDatosSistema: TcxTabSheet;
    lblDatosServWindows: TcxLabel;
    cxServidorWin: TcxTextEdit;
    lblDatosProcesadores: TcxLabel;
    cxProcessors: TcxTextEdit;
    lblDatosVersion: TcxLabel;
    cxVersionSO: TcxTextEdit;
    lblDatosServidorBD: TcxLabel;
    cxServidorBD: TcxTextEdit;
    cxNETVersion: TcxTextEdit;
    lblDatosNetFramework: TcxLabel;
    cxUserLogged: TcxTextEdit;
    lblDatosUsuario: TcxLabel;
    cxMemoriaRAM: TcxTextEdit;
    lblDatosRAM: TcxLabel;
    cxMemoriaVirtual: TcxTextEdit;
    lblDatosMemVirtual: TcxLabel;
    cxMemoriaVirtualDisp: TcxTextEdit;
    lblDatosMemVirtualDisp: TcxLabel;
    pnlDatosSistema: TPanel;
    cxGridDatos: TcxGrid;
    cxGridDatosDBTableView1: TcxGridDBTableView;
    colDisco: TcxGridDBColumn;
    colTamanio: TcxGridDBColumn;
    colLibre: TcxGridDBColumn;
    colPorcOcupado: TcxGridDBColumn;
    cxGridDatosLevel1: TcxGridLevel;
    lblDatosRecursos: TcxLabel;
    cxRecursosLibres: TcxTextEdit;
    cxMemoriaRAMDisp: TcxTextEdit;
    lblDatosRAMDisp: TcxLabel;
    cxExportar: TcxButton;
    cxOpenDialog: TcxShellBrowserDialog;
    cxLargeImageMenu: TcxImageList;
    ImageButtons: TcxImageList;
    cxOpenDialogLic: TOpenTextFileDialog;
    cxTituloAutorizaVirtual: TcxLabel;
    cxBrowserServidor: TcxShellBrowserDialog;
    DataSourceDlls: TDataSource;
    DataSourceServicios: TDataSource;
    PnlGridServicios: TPanel;
    cxGridServicios: TcxGrid;
    cxGridServiciosDBTableView1: TcxGridDBTableView;
    colServicios: TcxGridDBColumn;
    colStatus: TcxGridDBColumn;
    colServiceName: TcxGridDBColumn;
    cxGridServiciosLevel1: TcxGridLevel;
    pnlBotones: TPanel;
    Panel1: TPanel;
    cxGridServidor: TcxGrid;
    cxGridServidorDBTableView1: TcxGridDBTableView;
    colLibreria: TcxGridDBColumn;
    colDescripcion: TcxGridDBColumn;
    colVersion: TcxGridDBColumn;
    colBuild: TcxGridDBColumn;
    colFecha: TcxGridDBColumn;
    cxGridServidorLevel1: TcxGridLevel;
    lblPaqueteTress20: TcxLabel;
    cxPathServidor: TcxTextEdit;
    btnBuscarDlls: TcxButton;
    PnlTConfigurar: TPanel;
    Panel4: TPanel;
    cxGBModulosAutoriza: TcxGroupBox;
    cxModulosAut: TcxCheckListBox;
    cxGBModulos: TcxGroupBox;
    cxModulosPrest: TcxCheckListBox;
    Panel5: TPanel;
    pnlServidorProcesos: TPanel;
    imgTitleServicios: TImage;
    imgTitleConfigurar: TImage;
    imgTltleModulos: TImage;
    imgTltleServidorProcesos: TImage;
    imgTitleDatosSistema: TImage;
    DSLogicalDrives: TDataSource;
    lgbResumen: TdxBarLargeButton;
    cxResumen: TcxTabSheet;
    PnlTResumen: TPanel;
    lblTResumen: TcxLabel;
    imgTitleResumen: TImage;
    PnlResumenConfig: TPanel;
    PnlResumenSentinel: TPanel;
    PnlResumenServicios: TPanel;
    PnlResumenServidor: TPanel;
    lblTResumenPlataforma: TcxLabel;
    lblTResumenCompInst: TcxLabel;
    lblTResumenVersion: TcxLabel;
    lblTResumenSentinelVersion: TcxLabel;
    lblTResumenSentVencimiento: TcxLabel;
    lblTResumenServiciosIni: TcxLabel;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    lblResumenVersion: TcxLabel;
    lblResumenSentPlataforma: TcxLabel;
    lblResumenSentVersion: TcxLabel;
    lblResumenSentVencimiento: TcxLabel;
    lblResumenServiciosInst: TcxLabel;
    lblResumenServiciosIni: TcxLabel;
    lblResumenCompInst: TcxLabel;
    cbResumensentEsDemo: TcxCheckBox;
    lblResumenSentEsDemo: TcxLabel;
    cxTituloSentinelAutorizar: TcxLabel;
    lblResumenSentServidor: TcxLabel;
    imgResumenConfigurar: TcxImage;
    lblResumenConfigurar: TcxLabel;
    lblResumenSentinel: TcxLabel;
    lblResumenServicios: TcxLabel;
    lblResumenServidorProcesos: TcxLabel;
    imgResumenSentinel: TcxImage;
    imgResumenServicios: TcxImage;
    imgResumenServidor: TcxImage;
    lblTResumenServiciosInst: TcxLabel;
    lblTConfigurar: TcxLabel;
    lblTModulos: TcxLabel;
    lblTServidorProcesos: TcxLabel;
    lblTServicios: TcxLabel;
    lblTDatosSistema: TcxLabel;
    txtResumenSentServidor: TcxLabel;
    btnReiniciarServicios: TcxButton;
    btnPararServicios: TcxButton;
    btnIniciarServicios: TcxButton;
    lblTResumenRutaServidor: TcxLabel;
    Panel3: TPanel;
    imgTitleSentinel: TImage;
    lblTSentinel: TcxLabel;
    btnRenovaAutorizacion: TcxButton;
    btnActualizaSentinel: TcxButton;
    btnRefreshSentinel: TcxButton;
    cxGBSentinelFisico: TcxGroupBox;
    lblSentNumero: TcxLabel;
    cxSN_NUMERO: TcxTextEdit;
    lblSentNumEmpresa: TcxLabel;
    cxSI_FOLIO: TcxTextEdit;
    lblSentPlataforma: TcxLabel;
    cxPlataforma: TcxTextEdit;
    lblSentBaseDatos: TcxLabel;
    cxBaseDatos: TcxTextEdit;
    lblVersion: TcxLabel;
    cxVersion: TcxTextEdit;
    lblEmpleados: TcxLabel;
    cxEmpleados: TcxTextEdit;
    lblUsuarios: TcxLabel;
    cxUsuarios: TcxTextEdit;
    lblVencimiento: TcxLabel;
    cxVencimiento: TcxTextEdit;
    lblInstalaciones: TcxLabel;
    cxInstalaciones: TcxTextEdit;
    lblEsDemo: TcxLabel;
    cxEsDemo: TcxCheckBox;
    lblTResumenConexion: TcxLabel;
    txtResumenConexion: TcxRichEdit;
    txtResumenRutaServidor: TcxRichEdit;
    gbArchivosConfig: TcxGroupBox;
    lblArchivoConfiguracion: TcxLabel;
    cxFolderConfiguracion: TcxTextEdit;
    cxBuscar: TcxButton;
    cxGBConfigCOMPARTE: TcxGroupBox;
    btnConfigConexion: TcxButton;
    cxConexionComparte: TcxTextEdit;
    lblUbicaBDCompartida: TcxLabel;
    lblConfUsuario: TcxLabel;
    cxUsuario: TcxTextEdit;
    btnCrearBD: TcxButton;
    GBCreacionArchivosBD: TcxGroupBox;
    lblUbicacionBD: TcxLabel;
    txtRutaArchivosBD: TcxTextEdit;
    btnRutaArchivosBaseDatos: TcxButton;
    cxGBVersionDB: TcxGroupBox;
    lblConfVersion: TcxLabel;
    cxVersionDatos: TcxTextEdit;
    cxGBConfigAcceso: TcxGroupBox;
    cxTipoLogin: TcxComboBox;
    lblConfTipoAcceso: TcxLabel;
    cxGBConfigWorkflow: TcxGroupBox;
    cxbtnPrepararWorkFlow: TcxButton;
    lblPrepararUsarWorkFlow: TcxLabel;
    lblSQLNativeClient: TcxLabel;
    txtSQLNativeClient: TcxTextEdit;
    btnBorrarSentinelVirtual: TcxButton;
    cxOpenDialogArchivosBD: TcxShellBrowserDialog;
    cxbtnActualizar: TcxButton;
    lblResumenDesc: TcxLabel;
    lblConfigurarDesc: TcxLabel;
    lblSentinelDesc: TcxLabel;
    lblModulosDesc: TcxLabel;
    lblServidorProcesosDesc: TcxLabel;
    lblServiciosDesc: TcxLabel;
    lblDatosSistemaDesc: TcxLabel;
    DevEx_cxLocalizer: TcxLocalizer;
    cxGrupoAcciones: TcxGroupBox;
    btnEscribirDisco: TRadioButton;
    btnEnviarGTI: TRadioButton;
    btnExportaReporte: TcxButton;
    AbZipper: TAbZipper;
    cxOpenDialogErrorLic: TcxShellBrowserDialog;
    cxAccionSugerida: TcxRichEdit;
    cxImgNoAut: TcxImage;
    cxImgAut: TcxImage;
    btnDesinstalarServicio: TcxButton;
    btnInstalarServicio: TcxButton;
    colServiceType: TcxGridDBColumn;
    colServiceRealName: TcxGridDBColumn;
    lgbUsuarios: TdxBarLargeButton;
    cxUsuariosSistema: TcxTabSheet;
    Panel6: TPanel;
    Image1: TImage;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxGroupBox1: TcxGroupBox;
    cxGridUsuariosSistema: TcxGrid;
    cxGridUsuariosSistemaDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    btnDesbloquearAdmin: TcxGroupBox;
    btnSacarUsuarios: TcxButton;
    DataSourceUsuariosSistema: TDataSource;
    btnAplicarFueraSistema: TcxButton;
    lgbFacilitadores: TdxBarLargeButton;
    cxFacilitadores: TcxTabSheet;
    cxLabel3: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxLabel5: TcxLabel;
    cxTextEdit3: TcxTextEdit;
    cxLabel6: TcxLabel;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    cxLabel8: TcxLabel;
    Panel7: TPanel;
    Image2: TImage;
    cxLabel14: TcxLabel;
    cxLabel15: TcxLabel;
    cxTextEdit12: TcxTextEdit;
    cxLabel16: TcxLabel;
    cxGroupBox2: TcxGroupBox;
    cxGroupBox5: TcxGroupBox;
    cxButton6: TcxButton;
    cxButton4: TcxButton;
    cxButton1: TcxButton;
    cxButton5: TcxButton;
    cxGroupBox6: TcxGroupBox;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Panel8: TPanel;
    Panel9: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel2: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    procedure lgbSalirClick(Sender: TObject);
    procedure lgbConfigurarClick(Sender: TObject);
    procedure lgbSentinelClick(Sender: TObject);
    procedure lgbModulosClick(Sender: TObject);
    procedure lgbServidorClick(Sender: TObject);
    procedure lgbServiciosClick(Sender: TObject);
    procedure lgbDatosClick(Sender: TObject);
    procedure lgbDesbloqueoAdministradoresClick(Sender: TObject);
    procedure cxBuscarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnConfigConexionClick(Sender: TObject);
    procedure btnCrearBDClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cxTipoLoginPropertiesChange(Sender: TObject);
    procedure cxLicenciaSentinelClick(Sender: TObject);
    procedure btnActualizaSentinelClick(Sender: TObject);
    procedure btnRenovaAutorizacionClick(Sender: TObject);
    procedure btnRefreshSentinelClick(Sender: TObject);
    procedure btnBuscarDllsClick(Sender: TObject);
    procedure btnReiniciarServiciosClick(Sender: TObject);
    procedure btnPararServiciosClick(Sender: TObject);
    procedure btnIniciarServiciosClick(Sender: TObject);
    procedure lgbResumenClick(Sender: TObject);
    procedure ButtonDown(Sender: TStringsCase);
    procedure btnBorrarSentinelVirtualClick(Sender: TObject);
    procedure cxPathServidorExit(Sender: TObject);
    procedure btnRutaArchivosBaseDatosClick(Sender: TObject);
    procedure cxFolderConfiguracionExit(Sender: TObject);
    procedure cxbtnPrepararWorkFlowClick(Sender: TObject);
    procedure cxbtnActualizarClick(Sender: TObject);
    procedure txtRutaArchivosBDExit(Sender: TObject);
    procedure cxExportarClick(Sender: TObject);
    procedure btnExportaReporteClick(Sender: TObject);
    procedure lgbUsuariosClick(Sender: TObject);
    procedure btnSacarUsuariosClick(Sender: TObject);
    procedure btnAplicarFueraSistemaClick(Sender: TObject);
    procedure btnInstalarServicioClick(Sender: TObject);
    procedure btnDesinstalarServicioClick(Sender: TObject);
    procedure cxGridServiciosDBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
    procedure btnUsuarioWindowsClick(Sender: TObject);
    procedure btnSharedFoldersClick(Sender: TObject);
    procedure btnComponentServicesClick(Sender: TObject);
    procedure btnServiciosWindowsClick(Sender: TObject);
    procedure lgbFacilitadoresClick(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure LinkLabel1LinkClick(Sender: TObject; const Link: string;
      LinkType: TSysLinkType);
    procedure cxLabel4Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure Label2Click(Sender: TObject);
    procedure Label3Click(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Image6Click(Sender: TObject);
    procedure Image5Click(Sender: TObject);
    procedure Image3Click(Sender: TObject);
    procedure Image4Click(Sender: TObject);
    private
    SC : TSQLConnection;
    function GetConnStr(Server, UserName, Password: String): widestring;
    procedure LoadDll;
    // US #7328: Implementar Control de la ayuda en XE5
    procedure ManejaExcepcion( Sender: TObject; Error: Exception );
    private
    { Private declarations }
    FRegistry: TZetaRegistryServer;
    FAutoServer: TAutoServer;
    FRegistrySent: TSentinelRegistryServer;
    FPath: String;
    procedure CambiarOpcion(Tab:TcxTabSheet );
    procedure CargaSentinelInfo;
    procedure CargaResumenSentinelInfo;
    //function BoolToFullStr(const lValue: Boolean): String;
    procedure CargaDatosSentinelVirtual;
    procedure CargaDatosResumenSentinelVirtual;
    procedure RefrescarSentinel;
    procedure RefrescarResumenSentinel;
    procedure MostrarListaAutorizados(Lista: TCheckListBox;ListaPrest:TCheckListBox);
    procedure CargarServicios;
    procedure CargarDatosSistema;
    procedure GetDLLVersions;
    procedure CargaResumen;
    procedure CargaPantalla;
    procedure RegistroInicial;
    procedure HabilitaBotonesMenu(lHabilita: Boolean);
    procedure InitHelpContext(iHelpContext : Integer);
    procedure HabilitaWorkflow;
    procedure CargarUsuariosSistema;
    function AbrirAplicacionesWindows(sRutaApp, sParametro : string) : boolean;
    // US #7328: Implementar Control de la ayuda en XE5
    protected
    FOldHelpEvent: THelpEvent;
    function HTMLHelpHook( Command: Word; Data: THelpEventData; var CallHelp: Boolean ): Boolean;
    function GetWMIString(wmiProperty, wmiClass: String): String;

    public
    { Public declarations }
  end;

var
  FConfiguradorTRESS: TFConfiguradorTRESS;
StringsCase: array [0..3] of string = (
    'lgbResumen',
    'lgbConfigurar',
    'lgbSentinel',
    'lgbModulos'
    );

implementation

uses
  Types, Windows, Graphics, ZetaCommonClasses, FWizConexionComparte, ConfigConexion, DDBConfig, ZetaWinAPITools,
  FSentinelWrite, ZetaLicenseMgr, DesbloqUsuariosAdmin, ZetaCommonTools, FConfigurarWorkFlow, ZetaServerTools,
  FWizActualizaComparte, MotorPatchUtils, FExportarDatosSistema, DBClient, FExportaDatosSistLic, FErrorGrabarArchivo3dt,
  FWizInstalacionServicio_DevEx, DesconectarUsuarios, ShellApi, System.SysConst, Messages, ActiveX, Variants;


{$R *.dfm}

procedure TFConfiguradorTRESS.FormCreate(Sender: TObject);
var
 l: DWORD;
begin
     ZetaRegistryCliente.InitClientRegistry;
     l := GetWindowLong(Self.Handle, GWL_STYLE);
     l := l and not (WS_MAXIMIZEBOX);
     l := SetWindowLong(Self.Handle, GWL_STYLE, l);

     FRegistry := TZetaRegistryServer.Create;
     DDBConfig.dmDBConfig := TdmDBConfig.Create(Self);
     FRegistrySent := TSentinelRegistryServer.Create( True );
     FAutoServerLoader.FCalculandoEmpleadosLoader := FALSE;
     FAutoServer := TAutoServer.Create;
     WindowState := wsNormal;

     // US #7328: Implementar Control de la ayuda en XE5
     with Application do
     begin
          OnException := ManejaExcepcion;
          FOldHelpEvent := OnHelp;
          OnHelp := HTMLHelpHook;
     end;
end;

// US #7328: Implementar Control de la ayuda en XE5
function TFConfiguradorTRESS.HTMLHelpHook(Command: Word; Data: THelpEventData; var CallHelp: Boolean): Boolean;
begin
    Result := FHelpManager.HtmlHelpHook( Application.HelpFile, Command, Data, CallHelp );
end;

// US #7328: Implementar Control de la ayuda en XE5
procedure TFConfiguradorTRESS.ManejaExcepcion(Sender: TObject; Error: Exception);
begin
     ZExcepcion( 'Error en ' + Application.Title, '� Se Encontr� un Error !', Error, 0 );
end;

procedure TFConfiguradorTRESS.HabilitaWorkflow;
var
   esWFPreparado : Boolean;
begin
    esWFPreparado := not DDBConfig.dmDBConfig.EsWorkflowPreparado;

    cxbtnPrepararWorkFlow.Enabled := esWFPreparado;
    if esWFPreparado then
    begin
        lblPrepararUsarWorkFlow.Caption := 'No preparado para reportes de WorkFlow:';
    end
    else
    begin
        lblPrepararUsarWorkFlow.Caption := 'Si preparado para reportes de WorkFlow:';
    end;
end;

procedure TFConfiguradorTRESS.Image3Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\lusrmgr.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.Image4Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\fsmgmt.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.Image5Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\comexp.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.Image6Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Services.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.InitHelpContext(iHelpContext : Integer);
begin
     HelpContext := iHelpContext;
end;

procedure TFConfiguradorTRESS.Label1Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Services.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.Label2Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\comexp.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.Label3Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\lusrmgr.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.Label4Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\fsmgmt.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.FormDestroy(Sender: TObject);
begin
     ZetaRegistryCliente.ClearClientRegistry;
     FRegistry.Free;
     DDBConfig.dmDBConfig.Free;
     FRegistrySent.Free;
     FAutoServer.Free;
end;

procedure TFConfiguradorTRESS.FormShow(Sender: TObject);
var
   Mensaje,sServer, sDataBase : String;
   EsTressCFG : Boolean;
begin
     with FRegistry do
     begin
        EsTressCFG := not ( IsKeyExists(SERVER_SERVER_DB)) AND IsKeyExists(SERVER_DATABASE) AND StrLleno(Database);
        if EsTressCFG and CanWrite then
        begin
             GetServerDatabase(Database, sServer, sDataBase);
             ServerDB := sServer;
             DataBaseADO := sDataBase;
        end;

        if not CanWrite then
        begin
            ZetaDialogo.ZError('Error', 'No se tienen permisos para registrar las variables del sistema (Registry), se debe ingresar con otro usuario.', 0);
            close();
        end
        else if not( IsKeyExists(SERVER_DATABASE)) OR (Database = VACIO)  then
        begin
             cxbtnPrepararWorkFlow.Enabled := FALSE;
             ZetaDialogo.ZWarning(Self.Caption, 'No existe configuraci�n inicial. '+#13#10+' '+#13#10+'Para realizar la configuraci�n de la base de datos de Comparte presione el bot�n OK.', 0, mbOK );
             RegistroInicial;
             OpcionesPaginas.ActivePage := cxConfigurar;
             ButtonDown(slgbConfigurar);
             HabilitaBotonesMenu(FALSE);
             CargaPantalla;
        end
        else if not ( IsKeyExists(SERVER_SERVER_DB)) OR (ServerDB = VACIO) then
        begin
             ZetaDialogo.ZError('Error','No fue posible acceder a la base de datos de Comparte. '+#13#10+' '+#13#10+'Para realizar la configuraci�n de la base de datos de Comparte presione el bot�n OK.', 0 );
             RegistroInicial;
             OpcionesPaginas.ActivePage := cxConfigurar;
             ButtonDown(slgbConfigurar);
             HabilitaBotonesMenu(FALSE);
             CargaPantalla;
             cxbtnPrepararWorkFlow.Enabled := FALSE;
        end
        else if not (dmDBConfig.EsConexionValida(GetConnStr(FRegistry.ServerDB, FRegistry.UserName, FRegistry.Password),Mensaje)) then
        begin
             ZetaDialogo.ZError('Error', 'No se pudo acceder a la BD de Comparte. '+#13#10+' '+#13#10+'Para realizar la configuraci�n de la base de datos de Comparte presione el bot�n OK.', 0 );
             RegistroInicial;
             OpcionesPaginas.ActivePage := cxConfigurar;
             ButtonDown(slgbConfigurar);
             HabilitaBotonesMenu(FALSE);
             CargaPantalla;
             cxbtnPrepararWorkFlow.Enabled := FALSE;
        end
        else
        begin
             DevEx_cxLocalizer.Active := True;
             DevEx_cxLocalizer.Locale := 2058;  // Cardinal para Espanol (Mexico)

            // ClientRegistry.EsClasica := FALSE;
            RegistroInicial;
            OpcionesPaginas.ActivePage := cxResumen;
            ButtonDown(slgbResumen);
            HabilitaBotonesMenu(TRUE);
            CargaPantalla;
        end;
     end;
end;

procedure TFConfiguradorTRESS.RegistroInicial;
begin
      with FRegistry do
      begin
          if not (IsKeyExists( SERVER_PATH_SERVER_CONFIG)) OR (PathServerConfig = VACIO) then
          begin
              PathServerConfig := Binarios;
          end;
          if not ( IsKeyExists(SERVER_PATH_CONFIG)) OR (PathConfig = VACIO) then
          begin
              PathConfig := VACIO;
              cxFolderConfiguracion.Text := PathConfig;
              PathConfig := Installation + 'Configuraci�n' ;
          end
          else
          begin
             cxFolderConfiguracion.Text := PathConfig;
          end;
      end;
end;
procedure TFConfiguradorTRESS.SpeedButton1Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Services.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.SpeedButton2Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\comexp.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.SpeedButton3Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\lusrmgr.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.txtRutaArchivosBDExit(Sender: TObject);
begin
     Fregistry.PathMSSQL := txtRutaArchivosBD.Text;
end;

procedure TFConfiguradorTRESS.lgbDesbloqueoAdministradoresClick(Sender: TObject);
begin
     ButtonDown(slgbDesbloqueoAdministradores);
     DesbloqUsuariosAdmin.DesbloqueaUsuariosAdmin := TDesbloqueaUsuariosAdmin.Create(Self);
     try
        DesbloqueaUsuariosAdmin.ShowModal;
        if DesbloqueaUsuariosAdmin.ModalResult = mrOk then
          cxConexionComparte.Text := FRegistry.Database;
     finally
     end;
end;

procedure TFConfiguradorTRESS.lgbFacilitadoresClick(Sender: TObject);
begin
      try
           //HelpContext := ;
           ButtonDown(slgFacilitadores);
           CambiarOpcion(cxFacilitadores );
           CargaPantalla;
      EXCEPT
      end;
end;

procedure TFConfiguradorTRESS.lgbConfigurarClick(Sender: TObject);
begin
     HelpContext := 3;
     ButtonDown(slgbConfigurar);
     CambiarOpcion(cxConfigurar);
     CargaPantalla;
end;

procedure TFConfiguradorTRESS.lgbDatosClick(Sender: TObject);
begin
     HelpContext := 8;
     ButtonDown(slgbDatos);
     CambiarOpcion(cxDatosSistema );
     CargarDatosSistema;
     CargaPantalla;
end;

procedure TFConfiguradorTRESS.HabilitaBotonesMenu(lHabilita: Boolean);
begin
     lgbResumen.Enabled := lHabilita;
     lgbSentinel.Enabled := lHabilita;
     lgbModulos.Enabled := lHabilita;
     lgbServidor.Enabled := lHabilita;
     lgbServicios.Enabled := lHabilita;
     lgbDatos.Enabled := lHabilita;
     lgbUsuarios.Enabled := lHabilita;
end;

procedure TFConfiguradorTRESS.CargarDatosSistema;
var
   SysInfo: TSystemInfo;
   MemoryInfo: TMemoryStatusEx;
   i: Word;
   useSpace : Double;
   Drives : TStringDynArray;
   cdsLogicalDrives : TZetaClientDataSet;
   version : string;
begin
     cdsLogicalDrives := TZetaClientDataSet.Create(Self);
     Windows.GetSystemInfo( SysInfo );
     FillChar(MemoryInfo, SizeOf(MemoryInfo), 0);
     MemoryInfo.dwLength := SizeOf(MemoryInfo);
     // check return code for errors
     Win32Check(GlobalMemoryStatusEx(MemoryInfo));
     //cxServidorWin.Text := GetWindowsVersionName( System.SysUtils.Win32MajorVersion, System.SysUtils.Win32MinorVersion );
     cxServidorWin.Text := GetWMIString('Caption','Win32_OperatingSystem');
     //cxVersionSO.Text := Format( '%d.%d Build %d %s', [ System.SysUtils.Win32MajorVersion, System.SysUtils.Win32MinorVersion, System.SysUtils.Win32BuildNumber, System.SysUtils.Win32CSDVersion ] );
     version := GetWMIString('Version','Win32_OperatingSystem');
     cxVersionSO.Text := Format( '%s Build %s', [ version.Substring(0,3) , version.Substring(4) ] );
     cxUserLogged.Text := ZetaWinApiTools.GetCurrentUserName;
     cxServidorBD.Text := 'Microsoft SQL Server ' + DDBConfig.dmDBConfig.GetServer;
     cxNETVersion.Text := GetNetFrameworkVersion;
     //cxProcessors.Text := GetArchitecture( SysInfo.wProcessorArchitecture, SysInfo.wProcessorLevel, SysInfo.wProcessorRevision );
     cxProcessors.Text := GetWMIString('Name','Win32_Processor');
     with MemoryInfo do
     begin
          cxRecursosLibres.Text := Format( '%f %s', [ ( 100 - dwMemoryLoad ) / 1, '%' ] );
          cxMemoriaRAM.Text := Format( '%f %s', [ BytesAMegas( ullTotalPhys ), 'MB' ] );
          cxMemoriaRAMDisp.Text := Format( '%f %s', [ BytesAMegas( ullAvailPhys ), 'MB'  ] );
          cxMemoriaVirtual.Text := Format( '%f %s', [ BytesAMegas( ullTotalVirtual ), 'MB'  ] );
          cxMemoriaVirtualDisp.Text := Format( '%f %s', [ BytesAMegas( ullAvailVirtual ), 'MB'  ] );
     end;

     with cdsLogicalDrives do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';

          AddStringField('colDisco', 50);
          AddStringField('colTamanio', 50);
          AddStringField('colLibre', 50);
          AddStringField('colPOrcOcupado', 50);
          CreateTempDataset;

          Open;
     end;
     {$ifdef TRESSCFG}
     Drives := ZetaWinApiTools.GetLogicalDrives;

     for i := 0 to Length(Drives)-1 do
     begin
         if DriveIsFixed(Drives[i]) then
         begin
         cdsLogicalDrives.Insert;
         cdsLogicalDrives.FieldByName('colDisco').AsString := Drives[i];

         cdsLogicalDrives.FieldByName('colTamanio').AsString := Format( '%f', [ ZetaWinApiTools.GetDiskSize(ord(Drives[i][1])-64)]);
         cdsLogicalDrives.FieldByName('colLibre').AsString := Format( '%f', [ ZetaWinApiTools.GetDiskFree(ord(Drives[i][1])-64)]);
         useSpace := ZetaWinApiTools.GetDiskSize(ord(Drives[i][1])-64) - ZetaWinApiTools.GetDiskFree(ord(Drives[i][1])-64);
         cdsLogicalDrives.FieldByName('colPorcOcupado').AsString := Format( '%f', [ useSpace]);
         cdsLogicalDrives.Post;
         end;
     end;

     DSLogicalDrives.DataSet := cdsLogicalDrives;
     cxGridDatosDBTableView1.DataController.DataSource := DSLogicalDrives;
     {$endif}
end;

procedure TFConfiguradorTRESS.CargaResumen;
var
i : Integer;
begin
        i := 0;
        with FRegistry do
        begin
        //Configurar
        lblResumenVersion.Caption := dmDBConfig.LoadVersion;
        txtResumenConexion.Clear;
        txtResumenConexion.Lines.Add(Database);
        //Sentinel
        RefrescarResumenSentinel;

        //Servicios
        CargarServicios;
        lblResumenServiciosInst.Caption := IntToStr(cxGridServiciosDBTableView1.DataController.RowCount);
        with DataSourceServicios.DataSet do
        begin
            First;
            while not Eof do
            begin
                 Active:= True;
                 if FieldByName('colStatus').AsString.Equals('Prendido') then
                 begin
                      i := i+1;
                 end;
                 Next;
            end;
        end;

        lblResumenServiciosIni.Caption := IntToStr( i );

        //Servidor de Procesos
        GetDLLVersions;
        lblResumenCompInst.Caption := IntToStr(cxGridServidorDBTableView1.DataController.RowCount);;
        txtResumenRutaServidor.Clear;
        txtResumenRutaServidor.Lines.Add(PathServerConfig);
        HabilitaWorkflow;
        end;
end;

procedure TFConfiguradorTRESS.CargaPantalla;
const
   system32_sqlncli =  'C:\Windows\System32\';
   SysWOW64_sqlncli =  'C:\Windows\SysWOW64\';
var
  sqlNativeClient: string;
  isLocalServer: Boolean;
  DetallesBD: TDetallesBD;
begin
      sqlNativeClient := VACIO;
      if OpcionesPaginas.ActivePage = cxResumen then
      begin
           InitHelpContext(2);
           CargaResumen;
      end
      else if OpcionesPaginas.ActivePage = cxConfigurar then
      begin
           InitHelpContext(3);
           cxVersionDatos.Enabled := FALSE;
           with FRegistry do
           begin
                cxFolderConfiguracion.Text := PathConfig;
                if (IsKeyExists( SERVER_SERVER_DB)) OR not(ServerDB = VACIO) then
                begin
                     isLocalServer := DDBConfig.dmDBConfig.IsLocalServer(GetConnStr(FRegistry.ServerDB, FRegistry.UserName, FRegistry.Password)) = 'local';
                     txtRutaArchivosBD.Enabled := not isLocalServer;
                     btnRutaArchivosBaseDatos.Enabled := isLocalServer;
                     cxVersionDatos.Text := dmDBConfig.LoadVersion;
                     cxTipoLogin.ItemIndex := TipoLogin;

                     try
                        DetallesBD := TDetallesBD.Create(nil, FRegistry.DataBase);
                        cxbtnActualizar.Enabled := DetallesBD.DBVersion < DetallesBD.DBMaxVersion;
                        FreeAndNil(DetallesBD);
                     except
                         on Error: Exception do
                         begin
                               ZetaDialogo.zInformation( 'Error al conectarse a la base de datos de Comparte.', 'Favor de verificar que se tenga el driver de MSSQL requerido.', 0 );
                               cxbtnActualizar.Enabled := False;
                         end;
                     end;
                end
                else
                begin
                     btnRutaArchivosBaseDatos.Enabled := FALSE;
                     txtRutaArchivosBD.Enabled := TRUE;
                end;
           end;
           with FRegistry do
           begin
                cxUsuario.Text := UserName;
                cxConexionComparte.Text := Database;

                if IsKeyExists(SERVER_PATH_MSSQL) then
                begin
                     txtRutaArchivosBD.Text := PathMSSQL;
                end
                else if IsKeyExists(SERVER_SERVER_DB) then
                begin
                     PathMSSQL := dmDBConfig.GetPathDefaultSQLServer(GetConnStr(FRegistry.ServerDB, FRegistry.UserName, FRegistry.Password));
                     txtRutaArchivosBD.Text := PathMSSQL;
                end
                else
                begin
                     txtRutaArchivosBD.Text := VACIO;
                end;
           end;
      end
      else if OpcionesPaginas.ActivePage = cxServidorProcesos then
      begin
           with FRegistry do
           begin
                if not (IsKeyExists( SERVER_PATH_SERVER_CONFIG)) OR (PathServerConfig = VACIO) then
                begin
                      PathServerConfig := Binarios;
                end;
                cxPathServidor.Text := PathServerConfig;
           end;
      end
      else if OpcionesPaginas.ActivePage = cxServicios then
      begin
           if cxGridServiciosDBTableView1.DataController.RowCount = 0 then
           begin
               btnIniciarServicios.Enabled := FALSE;
               btnPararServicios.Enabled := FALSE;
               btnReiniciarServicios.Enabled := FALSE;
           end;
      end
      else if OpcionesPaginas.ActivePage = cxDatosSistema then
      begin
           try
              if FileExists(system32_sqlncli + FRegistry.SQLNCLIProvider + '.dll') then
              begin
                   sqlNativeClient:= FRegistry.GetFileVersion(system32_sqlncli + FRegistry.SQLNCLIProvider+ '.dll');
              end
              else if FileExists(SysWOW64_sqlncli+ FRegistry.SQLNCLIProvider + '.dll') then
              begin
                    sqlNativeClient:=FRegistry.GetFileVersion(SysWOW64_sqlncli+ '.dll');
              end
              else
              begin
                   sqlNativeClient := 'No se encuentra versi�n';
              end;

                txtSQLNativeClient.Text := sqlNativeClient;
           except
                 sqlNativeClient := 'No se encuentra versi�n';

           end;
      end;
end;

procedure TFConfiguradorTRESS.lgbModulosClick(Sender: TObject);
begin
     HelpContext := 5;
     ButtonDown(slgbModulos);
     CargaSentinelInfo;
     CambiarOpcion(cxModulos);
end;

procedure TFConfiguradorTRESS.lgbResumenClick(Sender: TObject);
begin
    HelpContext := 2;
    ButtonDown(slgbResumen);
    CargaResumen;
    CambiarOpcion(cxResumen);
end;

procedure TFConfiguradorTRESS.ButtonDown(Sender: TStringsCase);
begin
  lgbResumen.Down    := (Sender = slgbResumen);
  lgbConfigurar.Down := (Sender = slgbConfigurar);
  lgbSentinel.Down   := (Sender = slgbSentinel);
  lgbModulos.Down    := (Sender = slgbModulos);
  lgbServidor.Down   := (Sender = slgbServidor);
  lgbServicios.Down  := (Sender = slgbServicios);
  lgbDatos.Down      := (Sender = slgbDatos);
  lgbSalir.Down      := (Sender = slgbSalir);
  lgbUsuarios.Down      := (Sender = slgbUsuarios);
  lgbFacilitadores.Down      := (Sender = slgFacilitadores);
end;

procedure TFConfiguradorTRESS.CargaSentinelInfo;
var
   oCursor: TCursor;
   ListModAut: TCheckListBox;
   ListModPrest: TCheckListBox;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
        FAutoServer.Cargar;
        TAutoServerLoader.LoadSentinelInfo( FAutoServer);
        with FAutoServer do
        begin
            {***(@am): Debido a la modificacion en el mecanismo de licencia, se requiere que al borrarse la licencia desde ConfiguradorTress
                    el sistema realmente cambie a modo DEMO, y muestra la configuracion de DEMO. Ya que anteriormente, dejaba en blanco las
                    la informacion del guardia.***}
            cxSN_NUMERO.Text := IntToStr( NumeroSerie );
            cxSI_FOLIO.Text := IntToStr( Empresa );
            cxEmpleados.Text := GetEmpleadosStr;
            cxVersion.Text := Version;
            cxVencimiento.Text := GetVencimientoStr;
            cxUsuarios.Text := IntToStr( Usuarios );
            cxEsDemo.Checked:= EsDemo;
            cxInstalaciones.Text := IntToStr( Instalaciones );
            cxPlataforma.Text := PlataformaAutoStr;
            cxBaseDatos.Text := GetSQLEngineStr;
            ListModAut := TCheckListBox.Create(Self);
            ListModPrest := TCheckListBox.Create(Self);
            ListModAut.Visible := false;
            ListModPrest.Visible:= false;
            ListModAut.Parent := Self;
            ListModPrest.Parent := Self;
            try
               GetModulos( ListModAut );
               GetPrestamos( ListModPrest );
               MostrarListaAutorizados(ListModAut,ListModPrest);
            finally
                   ListModPrest.Free;
                   ListModAut.Free;
            end;
        end;
     finally
            Screen.Cursor := oCursor;
            FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
     end;
end;

procedure TFConfiguradorTRESS.CargaResumenSentinelInfo;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FSentinelRegistry.FDesactivarConteoVirtual := TRUE;
        FAutoServer.Cargar;
        TAutoServerLoader.LoadSentinelInfo(FAutoServer);
        with FAutoServer do
        begin
            lblResumenSentPlataforma.Caption:= FAutoServer.PlataformaAutoStr;
            lblResumenSentVersion.Caption:= FAutoServer.Version;
            lblResumenSentVencimiento.Caption:= FAutoServer.GetVencimientoStr;
            cbResumensentEsDemo.Checked := FAutoServer.EsDemo;
        end;
     finally
            Screen.Cursor := oCursor;
            FSentinelRegistry.FDesactivarConteoVirtual := FALSE;
     end;
end;


procedure TFConfiguradorTRESS.MostrarListaAutorizados(Lista:TCheckListBox;ListaPrest:TCheckListBox);
var
  i: Integer;
begin
     cxModulosAut.Items.Clear;
     cxModulosPrest.Items.Clear;
     for i := 0 to Lista.Count - 1  do
     begin
          with cxModulosAut.Items.Add do
          begin
               Text := Lista.Items[i];
               Checked := Lista.Checked[i];
               Enabled := false;
          end;
     end;

     for i := 0 to ListaPrest.Count - 1  do
     begin
          with cxModulosPrest.Items.Add do
          begin
               Text := ListaPrest.Items[i];
               Checked := ListaPrest.Checked[i];
               Enabled := false;
          end;
     end;
end;

procedure TFConfiguradorTRESS.CargaDatosSentinelVirtual;
begin
      //if FSentinelRegistry.CheckComputerName then
      if FSentinelRegistry.CheckComputerInfo then
      begin
           with Self.cxTituloAutorizaVirtual do
           begin
                Caption := ' Autorizado';
                Left := 273;
                Style.TextColor := clGreen;
                cxImgAut.Visible := True;
                cxImgNoAut.Visible := False;
                cxImgAut.Visible := True;
                cxImgAut.Left := 247;
                cxGrupoAcciones.Visible := False;
                cxImgAut.Properties.ShowFocusRect := False;
           end;
      end
      else
      begin
           with Self.cxTituloAutorizaVirtual do 
           begin
                Caption := ' NO Autorizado';
                Left := 273;
                Style.TextColor := clRed;
                cxImgAut.Visible := False;
                cxImgNoAut.Visible := True;
                cxImgNoAut.Left := 247;
                cxImgNoAut.Properties.ShowFocusRect := False;
                // (@am): Solo debe mostrarse cuando este un archivo de licencia y este sea invalido
                cxGrupoAcciones.Visible := True;

           end;
      end;

      cxCompNam.Text := ZetaWinAPITools.GetComputerName;
      Self.cxArchivoLicencia.Text := FRegistrySent.ArchivoLicencia;
end;

procedure TFConfiguradorTRESS.CargaDatosResumenSentinelVirtual;
begin
      if StrLleno( FRegistrySent.ArchivoLicencia ) then
      begin
          cxTituloSentinelAutorizar.Visible := TRUE;
          //if FSentinelRegistry.CheckComputerName then
          if FSentinelRegistry.CheckComputerInfo then
          begin
               with Self.cxTituloSentinelAutorizar do
               begin
                    Caption := 'Autorizado';
                    Left := 194;
                    Style.TextColor := clGreen;
               end;
          end
          else
          begin
               with Self.cxTituloSentinelAutorizar do
               begin
                    Caption := 'NO Autorizado';
                    Left := 194;
                    Style.TextColor := clRed;
               end;
          end;
      end
      else
      begin
          cxTituloSentinelAutorizar.Visible := FALSE;
      end;

      txtResumenSentServidor.Caption := ZetaWinAPITools.GetComputerName;
end;

procedure TFConfiguradorTRESS.lgbSalirClick(Sender: TObject);
begin
     Close;
end;

procedure TFConfiguradorTRESS.lgbSentinelClick(Sender: TObject);
begin
     HelpContext := 4;
     ButtonDown(slgbSentinel);
     RefrescarSentinel;
     CambiarOpcion(cxSentinel);
end;

procedure TFConfiguradorTRESS.lgbServidorClick(Sender: TObject);
begin
      HelpContext := 6;
      ButtonDown(slgbServidor);
      CambiarOpcion(cxServidorProcesos);
      CargaPantalla;
      GetDLLVersions;
end;

procedure TFConfiguradorTRESS.btnAplicarFueraSistemaClick(Sender: TObject);
begin
     DesconectarUsuarios.DesconectaUsuarios := TDesconectaUsuarios.Create(Self);
     try
        DesconectaUsuarios.ShowModal;
        if DesconectaUsuarios.ModalResult = mrOk then
        begin
              dmDBConfig.DesconectarUsuarios;
              CargarUsuariosSistema;
              cxGridUsuariosSistema.Refresh;
        end;
     Except
     end;
end;

procedure TFConfiguradorTRESS.lgbUsuariosClick(Sender: TObject);
begin
      try
           HelpContext := 9;
           ButtonDown(slgbUsuarios);
           CambiarOpcion(cxUsuariosSistema );
           CargarUsuariosSistema;
           CargaPantalla;
      EXCEPT
      end;
end;

procedure TFConfiguradorTRESS.CargarUsuariosSistema;
var cdsUsuariosSistema : TZetaClientDataSet;
begin
     cdsUsuariosSistema := TZetaClientDataSet.Create(Self);
     with cdsUsuariosSistema do
     begin
          Filtered := False;
          Filter := '';
          IndexFieldNames := 'colUsuario';
          AddStringField('colUsuario', 50);
          AddStringField('colName', 50);
          AddStringField('colDesde', 50);
          CreateTempDataset;
          Open;
     end;
     dmDBConfig.CargarUsuarios(cdsUsuariosSistema);
     DataSourceUsuariosSistema.DataSet := cdsUsuariosSistema;
     cxGridUsuariosSistemaDBTableView1.DataController.DataSource := DataSourceUsuariosSistema;
end;

procedure TFConfiguradorTRESS.lgbServiciosClick(Sender: TObject);
begin
     HelpContext := 7;
     ButtonDown(slgbServicios);
     CambiarOpcion(cxServicios);
     CargarServicios;
     CargaPantalla;
end;

procedure TFConfiguradorTRESS.CargarServicios;
 var cdsServicios : TZetaClientDataSet;
begin
     cdsServicios := TZetaClientDataSet.Create(Self);
     with cdsServicios do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := 'colServicios';
          AddStringField('colServicios', 50);
          AddStringField('colStatus', 50);
          AddStringField('colServiceName', 100);
          AddStringField('colServiceRealName', 300);
          AddStringField('colServiceType', 50);
          CreateTempDataset;
          Open;
     end;
     ZetaWinAPITools.ServiceGetList('', SERVICE_WIN32, SERVICE_STATE_ALL,  TClientDataSet( cdsServicios ));
     DataSourceServicios.DataSet := cdsServicios;
     cxGridServiciosDBTableView1.DataController.DataSource := DataSourceServicios;
end;



procedure TFConfiguradorTRESS.btnIniciarServiciosClick(Sender: TObject);
var Index : Integer;
ServiceName: string;
oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
       Index := cxGridServiciosDBTableView1.DataController.GetSelectedRowIndex(0);
       ServiceName := cxGridServiciosDBTableView1.DataController.GetDisplayText(Index, 4);
       if Not ZetaWinAPITools.InitService(ServiceName) then
          ZetaDialogo.ZError(Self.Caption, 'Hubo un error al iniciar el servicio ' + ServiceName, 0 );

       CargarServicios;
       cxGridServiciosDBTableView1.DataController.FocusedRecordIndex := Index;
     finally
         Screen.Cursor := oCursor;
     end;

     CargarServicios;
end;

procedure TFConfiguradorTRESS.btnInstalarServicioClick(Sender: TObject);
begin
     inherited;
     ZcxWizardBasico.ShowWizard(TWizInstalacionServicio_DevEx);
     CargarServicios;
end;

procedure TFConfiguradorTRESS.btnPararServiciosClick(Sender: TObject);
var Index : Integer;
ServiceName: string;
oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
       Index := cxGridServiciosDBTableView1.DataController.GetSelectedRowIndex(0);
       ServiceName := cxGridServiciosDBTableView1.DataController.GetDisplayText(Index, 4);
       if not ZetaWinAPITools.StopService(ServiceName) then
          ZetaDialogo.ZError(Self.Caption, 'Hubo un error al detener el servicio ' + ServiceName, 0 );
       CargarServicios;
       cxGridServiciosDBTableView1.DataController.FocusedRecordIndex := Index;
     finally
         Screen.Cursor := oCursor;
     end;
     CargarServicios;
end;

procedure TFConfiguradorTRESS.btnReiniciarServiciosClick(Sender: TObject);
var Index : Integer;
ServiceName: string;
oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
       Index := cxGridServiciosDBTableView1.DataController.GetSelectedRowIndex(0);
       ServiceName := cxGridServiciosDBTableView1.DataController.GetDisplayText(Index, 4);
       if not ZetaWinAPITools.RestartService(ServiceName) then
          ZetaDialogo.ZError(Self.Caption, 'Hubo un error al reiniciar el servicio ' + ServiceName, 0 );

       CargarServicios;
       cxGridServiciosDBTableView1.ViewData.Rows[Index].Selected := True;
     finally
         Screen.Cursor := oCursor;
     end;
     CargarServicios;
end;

procedure TFConfiguradorTRESS.btnRefreshSentinelClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     btnRefreshSentinel.Enabled := FALSE;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        RefrescarSentinel;
     finally
            btnRefreshSentinel.Enabled := TRUE;
            Screen.Cursor := oCursor;
            Application.ProcessMessages;
     end;
end;

procedure TFConfiguradorTRESS.btnRenovaAutorizacionClick(Sender: TObject);
var
   oCursor: TCursor;
   {$ifdef DEBUGSENTINEL}
   oZetaProvider: TdmZetaServerProvider;
   oLicenseMgr: TLicenseMgr;
   {$endif}
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        {$ifdef DEBUGSENTINEL}
        try
           with FAutoServer do
           begin
                if Cargar then
                begin
                     oZetaProvider := TdmZetaServerProvider.Create( Self );
                     try
                        oLicenseMgr := TLicenseMgr.Create( oZetaProvider );
                        try
                           oZetaProvider.EmpresaActiva:= oZetaProvider.Comparte;
                           oLicenseMgr.EmpleadosGetGlobal;
                           OnSetData := oLicenseMgr.AutoSetData;
                           OnGetDataAdditional := oLicenseMgr.AutoOnGetDataAdditional;
                           try
                              EscribirAutorizacion;
                              ZetaDialogo.ZInformation('Autorizaci�n renovada','La autorizaci�n del sistema fu� renovada.',0 );
                              RefrescarSentinel;
                           finally
                                  OnSetData := nil;
                                  onGetDataAdditional := nil;
                           end;
                        finally
                               FreeAndNil( oLicenseMgr );
                        end;
                     finally
                            FreeAndNil( oZetaProvider );
                     end;
                end
     {$ifndef SENTINELVIRTUAL};{$else}
                else
                begin
                     FSentinelRegistry.FEmpleadosConteoVirtual := 0;
                     if FSentinelRegistry.ExisteSentinelVirtual then
                     begin
                         if TAutoServerLoader.CargarVirtualSentinelInfo( FAutoServer, True) then
                         begin
                              ZetaDialogo.ZInformation('Autorizaci�n renovada','La autorizaci�n del sistema fu� renovada',0 );
                              RefrescarSentinel;
                         end;
                     end;
                end;
     {$endif}
           end;
        except
              on Error: Exception do
              begin
                   //TODO: ManejaErrorSentinel( Error.Message );
              end;
        end;
        {$else}
        FAutoServer.Cargar;
        if DSentinelWrite.RenovarAutorizacion( ManejaErrorSentinel ) then
        begin
             ZetaDialogo.zInformation( 'Autorizaci�n renovada', 'La autorizaci�n del sistema fu� renovada', 0 );
             Refrescar;
        end;
        {$endif}
     finally
            Screen.Cursor := oCursor;
     end;

end;

procedure TFConfiguradorTRESS.btnRutaArchivosBaseDatosClick(Sender: TObject);
begin
      cxOpenDialogArchivosBD.Path := ExtractFilePath( FRegistry.PathMSSQL + '\' );
      if cxOpenDialogArchivosBD.Execute then
      begin
           txtRutaArchivosBD.Text := cxOpenDialogArchivosBD.Path;
           FRegistry.PathMSSQL := txtRutaArchivosBD.Text;
      end;
end;

procedure TFConfiguradorTRESS.btnSacarUsuariosClick(Sender: TObject);
begin
     ButtonDown(slgbDesbloqueoAdministradores);
     DesbloqUsuariosAdmin.DesbloqueaUsuariosAdmin := TDesbloqueaUsuariosAdmin.Create(Self);
     try
        DesbloqueaUsuariosAdmin.ShowModal;
        if DesbloqueaUsuariosAdmin.ModalResult = mrOk then
          cxConexionComparte.Text := FRegistry.Database;
     finally
     end;
end;


procedure TFConfiguradorTRESS.RefrescarSentinel;
begin
     CargaDatosSentinelVirtual;
     CargaSentinelInfo;
end;

procedure TFConfiguradorTRESS.RefrescarResumenSentinel;
begin
     CargaDatosResumenSentinelVirtual;
     CargaResumenSentinelInfo;
end;


procedure TFConfiguradorTRESS.CambiarOpcion(Tab:TcxTabSheet );
begin
     OpcionesPaginas.ActivePage := Tab;
end;

procedure TFConfiguradorTRESS.cxbtnActualizarClick(Sender: TObject);
begin
  ActualizaComparte := TActualizaComparte.Create(Self);
  ActualizaComparte.ShowModal;
  FreeAndNil(ActualizaComparte);
  CargaPantalla;
end;

procedure TFConfiguradorTRESS.cxbtnPrepararWorkFlowClick(Sender: TObject);
begin
     ConfigurarWorkflow := TConfigurarWorkflow.Create(Self);
     try
        ConfigurarWorkflow.ShowModal;
        if ConfigurarWorkflow.ModalResult = mrOk then
          HabilitaWorkflow;
     finally
     end;
end;

procedure TFConfiguradorTRESS.cxBuscarClick(Sender: TObject);
begin
      cxOpenDialog.Path := ExtractFilePath( FRegistry.PathConfig + '\');
      if cxOpenDialog.Execute then
      begin
           cxFolderConfiguracion.Text := cxOpenDialog.Path;
           FRegistry.PathConfig := cxFolderConfiguracion.Text;
      end;
end;


procedure TFConfiguradorTRESS.cxButton5Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Regedt32.Exe';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.cxButton6Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         //sRutaApp := 'C:\Windows\System32\secpol.msc';
         sRutaApp := 'C:\Windows\System32\eventvwr.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.btnComponentServicesClick(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\comexp.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.btnServiciosWindowsClick(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Services.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

function TFConfiguradorTRESS.AbrirAplicacionesWindows(sRutaApp,sParametro: string): boolean;
begin
      try
         Result := True;
         ShellExecute(handle,'open',PChar(sRutaApp), '','',SW_MAXIMIZE);
      Except
         Result := False;
      end;
end;

procedure TFConfiguradorTRESS.btnUsuarioWindowsClick(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\lusrmgr.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.btnSharedFoldersClick(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\fsmgmt.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.cxExportarClick(Sender: TObject);
var oExportarDatosSistema: TExportarDatosSistema;
begin
     oExportarDatosSistema := TExportarDatosSistema.Create( Self );
     with oExportarDatosSistema do
     begin
        FAutoServer.Cargar;
        AutoServer := FAutoServer;
        Show;
     end;
end;

procedure TFConfiguradorTRESS.cxFolderConfiguracionExit(Sender: TObject);
begin
     if StrLleno(cxFolderConfiguracion.Text) then
     begin
         FRegistry.PathConfig := cxFolderConfiguracion.Text;
     end
     else
     begin
          FRegistry.PathConfig := VACIO;
     end;
end;

procedure TFConfiguradorTRESS.cxGridServiciosDBTableView1FocusedRecordChanged(Sender: TcxCustomGridTableView; APrevFocusedRecord,AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     if AFocusedRecord <> nil then
     begin
          btnDesinstalarServicio.Enabled := (AFocusedRecord.Values[colServiceType.Index] = 'Cafeteria') or
          (AFocusedRecord.Values[colServiceType.Index] = 'TressReportes') or
          (AFocusedRecord.Values[colServiceType.Index] = 'TressCorreos');
     end;
end;

procedure TFConfiguradorTRESS.btnBorrarSentinelVirtualClick(Sender: TObject);
begin
     FRegistrySent.ArchivoLicencia := VACIO;
     {$ifdef DEBUGSENTINEL}
     FSentinelRegistry.BorrarSentinel;
     {$EndIf}
     RefrescarSentinel;
     cxGrupoAcciones.Visible := False; //(@am): Al borrar el sentinel debe desaparecer la seccion de envio del enrror
end;

procedure TFConfiguradorTRESS.btnBuscarDllsClick(Sender: TObject);
begin
      LoadDll;
end;

procedure TFConfiguradorTRESS.GetDLLVersions;
var
   oCursor: TCursor;
   sPath, sLibreria, sDescripcion, sVersion, sBuild, sFecha : String;
   cdsDlls : TZetaClientDataSet;
    procedure GetTressVersion( const sFileName: String);
    var
       sHint: String;
    begin
         if DirectoryExists(sPath) then
         begin
             sHint := ZetaWinAPITools.FormatDirectory( sPath ) + sFileName;
             if FileExists(sHint) then
             begin
                 sLibreria := sFileName;
                 sDescripcion := ZetaWinAPITools.GetInfoFileDescription( sHint);
                 sVersion := ZetaWinAPITools.GetInfoProductVersion( sHint);
                 sBuild := ZetaWinAPITools.GetInfoFileVersion( sHint );
                 sFecha := ZetaWinAPITools.GetInfoDateModified( sHint);
                 cdsDlls.Insert;
                 cdsDlls.FieldByName('colLibreria').AsString := sLibreria;
                 cdsDlls.FieldByName('colDescripcion').AsString := sDescripcion;
                 cdsDlls.FieldByName('colVersion').AsString := StringReplace(sVersion, '.0', '',[rfReplaceAll, rfIgnoreCase]);
                 cdsDlls.FieldByName('colBuild').AsString := sBuild;
                 cdsDlls.FieldByName('colFecha').AsString := sFecha;
                 cdsDlls.Post;
                 DataSourceDlls.DataSet := cdsDlls;
             end
             else
             begin
                  if not (DataSourceDlls.DataSet = NIL) then
                  begin
                       DataSourceDlls.DataSet := cdsDlls;
                  end;
             end;
         end
         else
         begin
             DataSourceDlls.DataSet := NIL;
         end;
    end;

begin
     cdsDlls := TZetaClientDataSet.Create(Self);
     with cdsDlls do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';

          AddStringField('colLibreria', 50);
          AddStringField('colDescripcion', 50);
          AddStringField('colVersion', 50);
          AddStringField('colBuild', 50);
          AddStringField('colFecha', 50);
          CreateTempDataset;
          Open;
     end;

     sPath := FRegistry.PathServerConfig;

     if ( sPath <> FPath ) then
     begin
          FPath := sPath;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             GetTressVersion( 'Asistencia.dll');
             GetTressVersion( 'Cafeteria.dll');
             GetTressVersion( 'CajaAhorro.dll');
             GetTressVersion( 'Catalogos.dll');
             GetTressVersion( 'Consultas.dll');
             GetTressVersion( 'Diccionario.dll');
             GetTressVersion( 'Global.dll');
             GetTressVersion( 'GlobalSeleccion.dll');
             GetTressVersion( 'GlobalVisitantes.dll');
             GetTressVersion( 'Imss.dll');
             GetTressVersion( 'Labor.dll');
             GetTressVersion( 'Login.dll');
             GetTressVersion( 'Nomina.dll');
             GetTressVersion( 'DAnualNomina.dll');
             GetTressVersion( 'DCalcNomina.dll');
             GetTressVersion( 'PortalTress.dll');
             GetTressVersion( 'Presupuestos.dll');
             GetTressVersion( 'Recursos.dll');
             GetTressVersion( 'Reportes.dll');
             GetTressVersion( 'Comparte.dll');
             GetTressVersion( 'Seleccion.dll');
             GetTressVersion( 'SelReportes.dll');
             GetTressVersion( 'ServicioMedico.dll');
             GetTressVersion( 'Sistema.dll');
             GetTressVersion( 'Super.dll');
             GetTressVersion( 'Tablas.dll');
             GetTressVersion( 'Visitantes.dll');
             GetTressVersion( 'VisReportes.dll');
             GetTressVersion( 'Sentinel3s.exe');
             GetTressVersion( 'WorkFlowCFG.Exe');
          finally
                 Screen.Cursor := oCursor;
          end;
     end;

    cxGridServidorDBTableView1.DataController.DataSource := DataSourceDlls;
end;

function TFConfiguradorTRESS.GetWMIString(wmiProperty, wmiClass: String): String;
var
  objWMI, colItems, colItem : OleVariant;
  oEnum : IEnumVARIANT;
  iValue : LongWord;
  wmiHost, root : String;

  function GetWMIObject(const objectName: String): IDispatch;
  var
    chEaten : Integer;
    BindCtx : IBindCtx;
    Moniker : IMoniker;
  begin
    OleCheck(CreateBindCtx(0, BindCtx));
    OleCheck(MkParseDisplayName(BindCtx, StringToOleStr(objectName), chEaten, Moniker));
    OleCheck(Moniker.BindToObject(BindCtx, nil, IDispatch, Result));
  end;

begin
  wmiHost := '.';
  root := 'root\CIMV2';
  objWMI  := GetWMIObject(Format('winmgmts:\\%s\%s',[wmiHost,root]));
  colItems := objWMI.ExecQuery(Format('SELECT * FROM %s',[wmiClass]),'WQL',0);
  oEnum := IUnknown(colItems._NewEnum) as IEnumVARIANT;
  while oEnum.Next(1, colItem, iValue) = 0 do
  begin
    Result := colItem.Properties_.Item(wmiProperty,0);
  end;
end;


procedure TFConfiguradorTRESS.cxLabel4Click(Sender: TObject);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Services.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.cxLicenciaSentinelClick(Sender: TObject);
begin
     if cxOpenDialogLic.Execute then
     begin
          Self.cxArchivoLicencia.Text   := cxOpenDialogLic.FileName;
          FRegistrySent.ArchivoLicencia := cxOpenDialogLic.FileName;
          CargaDatosSentinelVirtual;
     end;

end;

procedure TFConfiguradorTRESS.cxPathServidorExit(Sender: TObject);
begin
     if StrLleno(cxPathServidor.Text) then
     begin
          FRegistry.PathServerConfig := cxPathServidor.Text;
     end
     else
     begin
          cxPathServidor.Text := FRegistry.PathServerConfig;
     end;

     GetDLLVersions;
end;

procedure TFConfiguradorTRESS.LinkLabel1LinkClick(Sender: TObject;
  const Link: string; LinkType: TSysLinkType);
var
   sRutaApp: string;
begin
      try
         sRutaApp := VACIO;
         sRutaApp := 'C:\Windows\System32\Services.msc';
         AbrirAplicacionesWindows(sRutaApp, VACIO);
      Except
      end;
end;

procedure TFConfiguradorTRESS.LoadDll;
begin
      cxBrowserServidor.Path := ExtractFilePath( FRegistry.PathServerConfig + '\' );
      if cxBrowserServidor.Execute then
      begin
           cxPathServidor.Text := cxBrowserServidor.Path;
           FRegistry.PathServerConfig := cxPathServidor.Text;
      end;
      GetDLLVersions;
end;

procedure TFConfiguradorTRESS.cxTipoLoginPropertiesChange(Sender: TObject);
begin
     FRegistry.TipoLogin := cxTipoLogin.ItemIndex;
end;

procedure TFConfiguradorTRESS.btnActualizaSentinelClick(Sender: TObject);
begin
     with TSentinelWrite.Create( Self ) do
     begin
          try
             AutoServer := FAutoServer;
             ShowModal;
             if ( ModalResult = mrOK ) then
             begin
                  CargaDatosSentinelVirtual;
                  CargaSentinelInfo;
             end;
          finally
                 Free;
          end;
     end;
end;

procedure TFConfiguradorTRESS.btnConfigConexionClick(Sender: TObject);
begin
     ConfigurarConexion := TConfigurarConexion.Create(Self);
     try
        ConfigurarConexion.ShowModal;
        if ConfigurarConexion.ModalResult = mrOk then
        begin
            cxConexionComparte.Text := FRegistry.Database;
            HabilitaBotonesMenu(TRUE);
            CargaPantalla;
        end;
     finally
         ConfigurarConexion.Free;
     end;
end;

procedure TFConfiguradorTRESS.btnCrearBDClick(Sender: TObject);
begin
    if StrLleno(FRegistry.PathConfig) then
    begin
          if FileExists(VerificaDir(FRegistry.PathConfig) + 'Comparte\NuevaBD\Estructura\3ComparteDefaults.sql') then
          begin
              ComparteConfig := TComparteConfig.Create(Self);
              try
                 ComparteConfig.ShowModal;
                 if ComparteConfig.ModalResult = mrOk then
                 begin
                      HabilitaWorkflow;
                      cxConexionComparte.Text := FRegistry.Database;
                      HabilitaBotonesMenu(TRUE);
                      CargaPantalla;
                 end;
              finally
                 ComparteConfig.Free;
              end;
          end
          else
          begin
               ZetaDialogo.ZWarning(Self.Caption,'Es necesario configurar correctamente la ruta de los archivos de configuraci�n.',0, mbOk );
          end;
    end
    else
    begin
        ZetaDialogo.ZWarning(Self.Caption,'Es necesario configurar la ruta de los archivos de configuraci�n.', 0, mbOk );
    end

end;

procedure TFConfiguradorTRESS.btnDesinstalarServicioClick(Sender: TObject);
var 
   Index : Integer;
   ServiceName: string;
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        Index := cxGridServiciosDBTableView1.DataController.GetSelectedRowIndex(0);
        ServiceName := cxGridServiciosDBTableView1.DataController.GetDisplayText(Index, 4);
        if ZetaDialogo.ZConfirm(Caption, '� Desea desinstalar el servicio ' + ServiceName + '?', 0, mbNo ) then
        begin
             ZetaWinAPITools.StopService(ServiceName);
             if ZetaWinAPITools.RemoveService(ServiceName) then
             begin
                  CargarServicios;
                  ZetaDialogo.ZInformation(Caption, ServiceName + ' ha sido desinstalada correctamente.', 0);
             end
             else
                 ZetaDialogo.Zerror(Caption, 'No se pudo realizar la desinstalaci�n correctamente.', 0);
        end;
     finally
            Screen.Cursor := oCursor;
     end;
     CargarServicios;
end;

procedure TFConfiguradorTRESS.btnExportaReporteClick(Sender: TObject);
var
  oExportarDatosSistLic: TExportaDatosSistLic;
  sNombreZIP: String;
procedure ComprimirArchivo (sFileName: String);
var
  stDatos: TStringStream;
begin
    stDatos := TStringStream.Create;
    try
        stDatos.WriteString(oExportarDatosSistLic.Datos.Text);
        AbZipper.FileName := sFileName;
        AbZipper.AddFromStream(oExportarDatosSistLic.FileName, stDatos); //QUE ES FILENAME?
        AbZipper.CloseArchive;
    finally
        FreeAndNil (stDatos);
    end;
end;
function SeleccionaRuta: Boolean;
var sFileName: String;
begin
    sFileName := StrDef (ExtractFileName(oExportarDatosSistLic.FileName), 'ErrorLicencia.3dt');
    cxOpenDialogErrorLic.Path := ExtractFilePath( Application.ExeName );
    if not strLleno (ExtractFileExt(sFileName)) then
      sFileName := sFileName + '.3dt';
    if cxOpenDialogErrorLic.Execute then
      oExportarDatosSistLic.FileName := VerificaDir (cxOpenDialogErrorLic.Path)  + sFileName
    else
      oExportarDatosSistLic.FileName := '';
    Result :=  (Length(oExportarDatosSistLic.FileName) > 0);
end;
begin
     oExportarDatosSistLic := TExportaDatosSistLic.Create();
     with oExportarDatosSistLic do
     begin
        FAutoServer.Cargar;
        AutoServer := FAutoServer;
        try
           FileName := Format( '%sErrorLicencia.3dt', [ ExtractFilePath( Application.ExeName ) ] );
           ExportRepLic(FileName);
           if Length(Respuesta) >0 then
           begin
              ZetaDialogo.zInformation( 'Error','No ha sido posible exportar con �xito el reporte de error.', 0 );
           end
           else
           begin
               // Enviar archivo 3DT por medio del WebService
               if btnEnviarGTI.Checked then
               begin
                  //Crear nombre para el archivo ZIP
                  sNombreZIP := VerificaDir(ExtractFilePath(FileName)) + crearNombreArchivoZIP;
                  try
                      //Hacer el archivo ZIP
                      ComprimirArchivo(sNombreZIP);
                      //Envio del archivo
                      EnviarReporteLicencia(FileName, sNombreZIP);
                  finally
                      // Eliminar archivo ZIP
                      AbZipper.Free;
                      System.SysUtils.DeleteFile(sNombreZIP);
                  end;
                  if (Pos ('FINAL', UpperCase(Respuesta)) > 0) then
                  begin
                      //Notificar el envio al usuario
                      ZetaDialogo.zInformation( 'Reporte de Licencia','El reporte ha sido exportado con �xito', 0 );
                  end
                  else
                      ZetaDialogo.zInformation( 'Error','No ha sido posible realizar el env�o del reporte con �xito.', 0 );
               end
               else //Guardar Archivo localmente
               begin
                  if SeleccionaRuta then //Se selecciono una ruta para guardarlo
                  begin
                    if not IntentarGuardarArchivo then
                      FileName := FErrorGrabarArchivo3dt.EditarArchivo(FileName)
                    else
                      //Notificar el envio al usuario
                      ZetaDialogo.zInformation( 'Reporte de Licencia','El reporte ha sido exportado con �xito a:' + CR_LF + oExportarDatosSistLic.FileName , 0 );
                  end;
               end;
           end;

        finally
            FreeAndNil(oExportarDatosSistLic);
        end;
     end;
end;

function TFConfiguradorTRESS.GetConnStr(Server, UserName, Password: String): widestring;
begin
    SC.ServerName := Server;
    SC.DatabaseName := '';
    SC.UserName := UserName;
    SC.Password := Password;

    Result := Format( 'Provider=%s;', [ FRegistry.SQLNCLIProvider ] );
    Result := Result + 'Data Source=' + Trim(SC.ServerName) + ';';
    if SC.DatabaseName <> '' then
        Result := Result + 'Initial Catalog=' + Trim(SC.DatabaseName) + ';';

    Result := Result + 'uid=' + Trim(SC.UserName) + ';';
    Result := Result + 'pwd=' + Trim(SC.Password) + ';';

    Result := Result + 'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80;Language=english';
end;

end.
