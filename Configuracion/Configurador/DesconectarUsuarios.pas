unit DesconectarUsuarios;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, DDBConfig, Vcl.ComCtrls, dxSkinsCore, TressMorado2013;

type
  TDesconectaUsuarios = class(TForm)
    cxOK: TcxButton;
    cxCancelar: TcxButton;
    mensajeDesbloquea: TRichEdit;
    procedure cxOKClick(Sender: TObject);
    procedure cxCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DesconectaUsuarios: TDesconectaUsuarios;

implementation

{$R *.dfm}
uses FConfigShell;

procedure TDesconectaUsuarios.cxCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TDesconectaUsuarios.cxOKClick(Sender: TObject);
begin
     if not dmDBConfig.DesconectarUsuarios then
     begin
          ShowMessage('Hubo un problema al tratar de actualizar estatus de los usuarios dentro de sistema TRESS');
     end;
end;

procedure TDesconectaUsuarios.FormCreate(Sender: TObject);
begin
     //HelpContext := 9;
end;

end.

