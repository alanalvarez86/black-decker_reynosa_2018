unit FConfigurarWorkflow;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus,
  dxSkinsCore, TressMorado2013, Vcl.StdCtrls, cxButtons, Vcl.ComCtrls, DDBConfig, ZetaDialogo;

type
  TConfigurarWorkflow = class(TForm)
    mensajeDesbloquea: TRichEdit;
    btnPreparar: TcxButton;
    btbCancelar: TcxButton;
    procedure btnPrepararClick(Sender: TObject);
    procedure btbCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConfigurarWorkflow: TConfigurarWorkflow;

implementation

{$R *.dfm}

procedure TConfigurarWorkflow.btbCancelarClick(Sender: TObject);
begin
    Close;
end;

procedure TConfigurarWorkflow.btnPrepararClick(Sender: TObject);
begin
     if not dmDBConfig.PrepararWorkflow then
     begin
          ShowMessage('Hubo un problema al tratar de preparar workflow');
     end
     else
     begin
         ZetaDialogo.ZInformation(Self.Caption,'WorkFlow se ha configurado correctamente.',0 );
         ModalResult := mrOk;
     end;
end;

procedure TConfigurarWorkflow.FormCreate(Sender: TObject);
begin
     HelpContext := 13;
end;

end.
