unit FConfigSentinel;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsForm, cxTextEdit, cxLabel, cxMaskEdit, cxDropDownEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons,ZetaRegistryServer,
  dxSkinsCore, dxSkinsDefaultPainters, cxClasses;

type
  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TConfigurarConexion = class(TForm)
    cxListServers: TcxComboBox;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxUser: TcxTextEdit;
    cxPassword: TcxTextEdit;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxBaseDatos: TcxComboBox;
    dxSkinController1: TdxSkinController;
    cxOK: TcxButton;
    cxCancelar: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure cxOKClick(Sender: TObject);
    procedure cxCancelarClick(Sender: TObject);
    procedure cxBaseDatosPropertiesInitPopup(Sender: TObject);
  private
    { Private declarations }
    SC : TSQLConnection;
    FRegistry: TZetaRegistryServer;
    function GetConnStr: widestring;

    procedure DatabasesOnServer(Databases : TStrings);
    procedure CargarConexion;


    property ConnStr : widestring read GetConnStr;
  public
    { Public declarations }
  end;

var
  ConfigurarConexion: TConfigurarConexion;

implementation

{$R *.dfm}
uses
DB, ADODB, ActiveX, ComObj, AdoInt, OleDB, ConfigShell,ZetaCommonTools;

procedure ListAvailableSQLServers(Names : TStrings);
var
RSCon: ADORecordsetConstruction;
Rowset: IRowset;
SourcesRowset: ISourcesRowset;
SourcesRecordset: _Recordset;
SourcesName, SourcesType: TField;
begin
OleCheck(CoCreateInstance(CLASS_Recordset, nil, CLSCTX_INPROC_SERVER or
CLSCTX_LOCAL_SERVER, IUnknown, SourcesRecordset) );
RSCon := SourcesRecordset as ADORecordsetConstruction;
SourcesRowset := CreateComObject(ProgIDToClassID('SQLOLEDB Enumerator'))
as ISourcesRowset;
OleCheck(SourcesRowset.GetSourcesRowset(nil, IRowset, 0, nil,
IUnknown(Rowset)));
RSCon.Rowset := RowSet;
with TADODataSet.Create(nil) do try
Recordset := SourcesRecordset;
SourcesName := FieldByName('SOURCES_NAME'); { do not localize }
SourcesType := FieldByName('SOURCES_TYPE'); { do not localize }
Names.BeginUpdate;
try
while not EOF do begin
if (SourcesType.AsInteger = DBSOURCETYPE_DATASOURCE)
and
(SourcesName.AsString <>'')
then
Names.Add(SourcesName.AsString);
Next;
end;
finally
Names.EndUpdate;
end;
finally
Free;
end;
end;


procedure TConfigurarConexion.cxBaseDatosPropertiesInitPopup(Sender: TObject);
begin
     Screen.Cursor := crSQLWait;
     try
        DatabasesOnServer(cxBaseDatos.Properties.Items );
     finally
            Screen.Cursor := crDefault;
     end;
end;

procedure TConfigurarConexion.cxCancelarClick(Sender: TObject);
begin
   Close;
end;

procedure TConfigurarConexion.cxOKClick(Sender: TObject);
begin
     FRegistry := TZetaRegistryServer.Create;
     if StrLleno(cxUser.Text) then
     begin
          if StrLleno(cxPassword.Text) then
          begin
               if StrLleno(cxBaseDatos.Text) then
               begin
                     if StrLleno(cxBaseDatos.Text) then
                     begin
                           try
                              with FRegistry do
                              begin
                                   UserName := cxUser.Text ;
                                   Password := cxPassword.Text ;
                                   Database := cxListServers.Text+'.'+cxBaseDatos.SelText;
                                   ServerDB := cxListServers.Text;
                                   DataBaseADO := cxBaseDatos.Text;
                              end;
                           finally
                             FRegistry.Free;
                           end;
                     end
                     else
                     begin
                          MessageDlg('Capturar Servidor',mtWarning,[mbOk],0 );
                          ModalResult := mrNone;
                     end;
               end
               else
               begin
                    MessageDlg('Capturar Base de Datos',mtWarning,[mbOk],0 );
                    ModalResult := mrNone;
               end;
          end
          else
          begin
               MessageDlg('Capturar Clave',mtWarning,[mbOk],0 );
               ModalResult := mrNone;
          end;
     end
     else
     begin
          MessageDlg('Capturar Usuario',mtWarning,[mbOk],0 );
          ModalResult := mrNone;
     end;

end;



procedure TConfigurarConexion.DatabasesOnServer(Databases : TStrings);
var
  rs : _RecordSet;
begin
  Databases.Clear;
  with TAdoConnection.Create(nil) do
  try
    ConnectionString := ConnStr;
    LoginPrompt := False;
    try
      Open;
      rs := ConnectionObject.OpenSchema(adSchemaCatalogs, EmptyParam, EmptyParam);
      with rs do
      begin
        try
          Databases.BeginUpdate;
          while not Eof do
          begin
            Databases.Add(VarToStr(Fields['CATALOG_NAME'].Value));
            MoveNext;
          end;
        finally
          Databases.EndUpdate;
        end;
      end;
      Close;
    except
      on e:exception do
        MessageDlg(e.Message,mtError, [mbOK],0);
    end;
  finally
    Free;
  end;
end;


procedure TConfigurarConexion.FormShow(Sender: TObject);
begin
     ListAvailableSQLServers(cxListServers.Properties.Items);
     CargarConexion;
end;

procedure TConfigurarConexion.CargarConexion;
begin
   FRegistry := TZetaRegistryServer.Create;
     try
        with FRegistry do
        begin
             cxUser.Text := UserName;
             cxPassword.Text := Password;
             cxBaseDatos.Text := Database;
             cxListServers.Text := ServerDB;
        end;
     finally
       FRegistry.Free;
     end;
end;


function TConfigurarConexion.GetConnStr: widestring;
begin
    SC.ServerName := cxListServers.Text;
    if cxBaseDatos.ItemIndex <> -1 then
        SC.DatabaseName := cxBaseDatos.Properties.Items[cxBaseDatos.ItemIndex]
    else
        SC.DatabaseName := '';
    SC.UserName := cxUser.Text;
    SC.Password := cxPassword.Text;

    Result := 'Provider=SQLNCLI;';
    Result := Result + 'Data Source=' + Trim(SC.ServerName) + ';';
    if SC.DatabaseName <> '' then
      Result := Result + 'Initial Catalog=' + Trim(SC.DatabaseName) + ';';

    Result := Result + 'uid=' + Trim(SC.UserName) + ';';
    Result := Result + 'pwd=' + Trim(SC.Password) + ';';
    Result := Result + 'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80';
end;

end.
