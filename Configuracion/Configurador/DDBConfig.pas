unit DDBConfig;

interface

uses
  System.SysUtils, System.Classes,ZetaRegistryServer, Data.DB, Data.Win.ADODB, Forms, ZetaClientDataSet,DZetaServerProvider,
  Variants, TypInfo, DBClient, Dialogs, ShellApi, ZetaASCIIFile, ZetaDialogo, Controls,
  ZetaLicenseClasses,
  ZetaCommonLists,
  FAutoServer,
  ZetaServerDataSet,
  ZetaLicenseMgr,
  ZetaCommonClasses;

type
  eTipoConfig = ( eTablasXML  );

  TSQLConnection = record
    ServerName : widestring;
    DatabaseName : wideString;
    UserName : widestring;
    Password  : widestring;
  end;

  TCallBack = procedure( const sMsg: String; dProgreso: Double ) of object;
  TCallBackError = procedure( const sMsg: String; Error: Exception ) of object;
  TdmDBConfig = class(TDataModule)
    Connector: TADOConnection;
    AdoQuery: TADOQuery;
    cdsEmpresas: TServerDataSet;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEmpresasAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
    FSiATodos: Boolean;
    FContinuar: Boolean;
    FRegistry: TZetaRegistryServer;
    SC : TSQLConnection;
    oZetaProvider: TdmZetaServerProvider;
    FBitacora    : TAsciiLog;
    FAutoServer : TAutoServer;
    FGlobalLicense  : TGlobalLicenseValues;
    function GetConnStr: widestring;
    property ConnStr : widestring read GetConnStr;
    procedure SetTablaInfo( const eTipoTabla: eTipoConfig );
    procedure MaskNumerico(const Campo: String);
    procedure LogInit;    
    procedure LogEnd;        
  public
    { Public declarations }
    procedure EjecutaQuery(Query: string);
    procedure OpenQuery(Query: string);
    procedure ExecuteSQLScript(Query: TADOQuery);
    function CrearComparte(const ConnStr,NomBase,Ruta:string;const Size,Incrementos:Integer): Boolean;
    function LeeScript(const ConnStr: string; out lErrorEstructura: Boolean): Boolean;
    function LeeXML(const ConnStr: string; lErrorXML: Boolean): Boolean;
    procedure GrabaDataSet(Origen: TClientDataSet; Tabla: string; AdoQuery: TADOQuery);
    function GrabaComparte(oDelta: OleVariant; out ErrorCount: Integer): OleVariant; {$IFNDEF DOS_CAPAS} safecall; {$ENDIF}
    function ConectarseServer(const ConnStr: string; var MensajeError: string): Boolean;
    function ConectarServidor(const ConnStr: string; var MensajeError: string): Boolean;
    function EsConexionValida(const ConnStr: string; var MensajeError: string): Boolean;
    procedure CerrarServer;
    function DesbloqueaAdministradores: Boolean;
    function PrepararWorkflow: Boolean;
    function EsWorkflowPreparado: Boolean;
    function GetArrayEmpresa(sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, sCM_NIVEL0, sCM_CODIGO: string): OleVariant;
    function GetServer: string;
    function GetServerName: string;
    function IsLocalServer(const ConnStr: string): string;
    function LoadVersion: string;
    function GetPathDefaultSQLServer(const ConnStr: string): string;
    function DataBaseExist(const ConnStr: string; Database: string): Boolean;
    function ExecSQLScript(Query: TADOQuery; out Mensaje: string): Boolean;
    function ShowConfirmation: Boolean;
    function ExisteArchivo(Ruta, Archivo: string; out Mensaje: string): Boolean;
    function GetFileName(Path, FileName: string): string;
    function GrabaXML(Origen: TClientDataSet; Tabla: string; AdoQuery: TADOQuery; out Mensaje: string): Boolean;
    property ZetaProvider: TdmZetaServerProvider read oZetaProvider;
    property Bitacora    : TAsciiLog             read FBitacora;
    procedure EmpresasConectar( const lTodas: Boolean  ; const eTipo  : eTipoCompany = tc3Datos);
    function GetTerminales( const lTodas: Boolean ): string;
    procedure SetAutoServer( value : TAutoServer );
    property AutoServer : TAutoServer read FAutoServer;
    function UsuariosContar: Integer;
    function SeleccionLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError) : TDatosSeleccion;
    function VisitantesLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError ) : TDatosVisitantes;
    procedure LicenciasLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError );
    function BuildEmpresa(DataSet: TDataSet): OleVariant;
    function GetMotorPatchAvance(Parametros: TZetaParams): OleVariant;
    function GetBitacora(const iProceso: Integer): OleVariant;
    function CargarUsuarios(var cdsUsuariosSistema : TZetaClientDataSet): Boolean;
    function DesconectarUsuarios: Boolean;
  end;

var
  dmDBConfig: TdmDBConfig;

implementation

uses
  ZetaCommonTools, ZetaServerTools, ZetaClientTools, MotorPatchUtils;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

const
  K_RELLENO          = 10;
  K_QRY_CREACOMPARTE = 'CREATE DATABASE %0:s ' +
                       ' ON ' +
                       ' ( NAME = %0:s_dat, ' +
                       '   FILENAME = ''%1:s\%0:s_dat.mdf'',' +
                       '   SIZE = %2:d, ' +
                       '   FILEGROWTH = %3:d ) ' +
                       ' LOG ON ' +
                       ' ( NAME = ''%0:s_log'',' +
                       '  FILENAME = ''%1:s\%0:s_log.ldf'',' +
                       '  SIZE = %2:d,' +
                       '  FILEGROWTH = %3:d )' +
                       ' COLLATE SQL_Latin1_General_CP1_CI_AS';
  K_ANCHO_ULTIMA_NOMINA = 40;

  K_BITACORA_PROCESO = 'select BI_FECHA, BI_HORA, BI_TIPO, BI_NUMERO, BI_TEXTO, BI_PROC_ID, US_CODIGO, CB_CODIGO, BI_DATA, '+
                       'BI_CLASE, BI_FEC_MOV from BITACORA where ( BI_NUMERO = %d ) order by BI_TIPO, CB_CODIGO, BI_FEC_MOV';

{ TdmDBConfig }

function TdmDBConfig.ConectarseServer(const ConnStr: string;var MensajeError:string): Boolean;
begin
     with Connector do
     begin
          Close;
          ConnectionString := ConnStr;
          LoginPrompt := False;
          try
             Open;
             Result := True;
          Except on e:exception do
          begin
               MensajeError := e.Message;
               ConnectionString := GetConnStr;
               Result := False;
          end;

          end;
     end;
end;

procedure TdmDBConfig.CerrarServer;
begin
   Connector.Close;
end;

procedure TdmDBConfig.LogInit;
begin
     with FBitacora do
     begin
          if not Used then
          begin
               with Application do
               begin
                    Init( ZetaCommonTools.VerificaDir( ExtractFilePath( ExeName ) ) + Title + '.log' );
               end;
          end;
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Inicio %s ' + StringOfChar( '*', K_RELLENO ) );
     end;
end;

procedure TdmDBConfig.LogEnd;
begin
     with FBitacora do
     begin
          WriteTimeStamp( StringOfChar( '*', K_RELLENO ) + ' Final %s ' + StringOfChar( '*', K_RELLENO ) );
          Close;
     end;
end;

procedure TdmDBConfig.cdsEmpresasAfterOpen(DataSet: TDataSet);
begin
     inherited;
     MaskNumerico( 'CM_ACTIVOS' );
     MaskNumerico( 'CM_INACTIV' );
     MaskNumerico( 'CM_TOTAL' );
     MaskNumerico( 'CM_ASIGNED' );
end;

procedure TdmDBConfig.MaskNumerico( const Campo: String );
var
   oCampo: TField;
begin
     with dmDBConfig.cdsEmpresas do
     begin
          oCampo := FindField( Campo );
          if ( oCampo <> nil ) and ( oCampo is TNumericField ) then
          begin
               TNumericField( oCampo ).DisplayFormat := '#,0';
          end;
     end;
end;

function TdmDBConfig.ConectarServidor(const ConnStr: string;var MensajeError:string): Boolean;
begin
     with Connector do
     begin
          Close;
          ConnectionString := ConnStr;
          LoginPrompt := False;
          try
             Open;
             Result := True;
             Close;
          Except on e:exception do
          begin
               Result := False;
               ConnectionString := GetConnStr;
               MensajeError := 'Se ingresó Usuario o Contraseña incorrectos. ' + #13#10 + 'Verifique los datos.';
          end;

          end;
     end;
end;

function TdmDBConfig.EsConexionValida(const ConnStr: string;var MensajeError:string): Boolean;
begin
     with Connector do
     begin
          Close;
          ConnectionString := ConnStr;
          LoginPrompt := False;
          try
             Open;
             Result := True;
             Close;
          Except on e:exception do
          begin
               MensajeError := e.Message;
               ConnectionString := GetConnStr;
               Result := False;
          end;

          end;
     end;
end;

procedure TdmDBConfig.EjecutaQuery(Query: string);
begin
     with AdoQuery do
     begin
          Connection := Connector;
          SQL.Text := Query;
          ExecSQL;
     end;
end;

procedure TdmDBConfig.OpenQuery(Query: string);
begin
     with AdoQuery do
     begin
          Connection := Connector;
          SQL.Text := Query;
          Open;
     end;
end;

function TdmDBConfig.CrearComparte(const ConnStr,NomBase,Ruta:string;const Size,Incrementos:Integer): Boolean;
var
  Query, Mensaje:string;
  lOk : Boolean;
begin
     if ConectarseServer(ConnStr,Mensaje)then
     begin
          try
            Query := Format(K_QRY_CREACOMPARTE,[NomBase,Ruta,Size,Incrementos]);
            EjecutaQuery(Query);
            lOk := TRUE;
          Except on e:exception do
                begin
                     with FBitacora do
                     begin
                         LogInit;
                         WriteTexto( 'Hubo un error al tratar de conectarse a la base de datos. Error:' + #13#10 + e.Message );
                     end;
                     lOk := FALSE
                end;
          end;
     end
     else
     begin
         lOk := FALSE;
         with FBitacora do
         begin
             LogInit;
             WriteTexto( Mensaje);
         end;
     end;
  Result := lOk;
end;

procedure TdmDBConfig.ExecuteSQLScript(Query: TADOQuery);
var
  Script: TStringList;
  Sentences: string;
begin
  Script := TStringList.Create;
  Sentences := Query.SQL.Text;
  Script.Text := Sentences;
  Query.SQL.Clear;
  while Script.Count > 0 do
  try
    if ( UpperCase(Trim(Script[0])) = 'GO') or ((Script.Count = 0) ) then
    begin
      if Script.Count > 0 then
      begin
        Script.Delete(0);
      end;
      Query.ExecSQL;
      Query.SQL.Clear;
    end
    else
    begin
      Query.SQL.Add(Script[0]);
      Script.Delete(0);
    end;
  except
    on e:Exception do
      Application.MessageBox({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('Error al correr script. ' + e.ClassName + ': ' + e.Message), 'ExecuteSQLScript()', 0);
  end;
  Query.SQL.Text := Sentences;
 FreeAndNil(Script);
end;

function TdmDBConfig.ExecSQLScript(Query: TADOQuery; out Mensaje: string): Boolean;
var
  Script: TStringList;
  Sentences: string;
begin
  Mensaje := VACIO;
  Result := TRUE;
  Script := TStringList.Create;
  Sentences := Query.SQL.Text;
  Script.Text := Sentences;
  Query.SQL.Clear;
  while Script.Count > 0 do
  try
    if ( UpperCase(Trim(Script[0])) = 'GO') or ((Script.Count = 0) ) then
    begin
      if Script.Count > 0 then
      begin
        Script.Delete(0);
      end;
      Query.ExecSQL;
      Query.SQL.Clear;
    end
    else
    begin
      Query.SQL.Add(Script[0]);
      Script.Delete(0);
    end;
  except
    on e:Exception do
    begin
       Mensaje := {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}('Error al correr script. ' + e.ClassName + ': ' + e.Message);
       result:= FALSE;
       break;
    end;
  end;
  Query.SQL.Text := Sentences;
 FreeAndNil(Script);
end;

function TdmDBConfig.ShowConfirmation: Boolean;
begin
     Result := FSiATodos;
     if not FSiATodos then
     begin
         case ZetaDialogo.zYesToAll( 'Advertencia', '¡ Se encontraron errores ! ¿ Desea continuar ? ', 0, mbYes ) of
              mrYes: Result := True;
              mrNo: Result := False;
         else
             FSiATodos := True;
         end;
     end;
end;

function TdmDBConfig.ExisteArchivo(Ruta, Archivo: string; out Mensaje: string): Boolean;
begin
     Mensaje := VACIO;
     if not FileExists(Format('%s%s', [Ruta,Archivo])) then
     begin
          Mensaje := Format('No se encontro el archivo %s', [Archivo]);
          Result := FALSE;
     end
     else
     begin
         Result := TRUE;
     end;
end;

function TdmDBConfig.GetFileName(Path, FileName: string): string;
begin
     Result := Format('%s%s', [Path,FileName])
end;

function TdmDBConfig.LeeScript(const ConnStr: string; out lErrorEstructura: Boolean): Boolean;
var
  Mensaje, sRuta:string;
  lOk : Boolean;
begin
     FContinuar := TRUE;
     lOk := TRUE;
     lErrorEstructura := FALSE;

     sRuta := VerificaDir(FRegistry.PathConfig) + 'Comparte\NuevaBD\Estructura\';
     if ConectarseServer(ConnStr,Mensaje)then
     begin
           with FBitacora do
           begin
               LogInit;
               WriteTexto( 'Estructura' );
               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3ComparteDefaults.sql', Mensaje);
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3ComparteDefaults.sql'));
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3ComparteDefaults.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3ComparteDefaults.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3ComparteDomains.sql', Mensaje);
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3ComparteDomains.sql'));
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3ComparteDomains.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3ComparteDomains.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                        lErrorEstructura := TRUE;
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3ComparteTablas.sql', Mensaje);
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3ComparteTablas.sql'));
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3ComparteTablas.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3ComparteTablas.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                        lErrorEstructura := TRUE;
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3ComparteTriggers.sql', Mensaje);
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3ComparteTriggers.sql'));
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3ComparteTriggers.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3ComparteTriggers.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                        lErrorEstructura := TRUE;
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3ComparteProcedures.sql', Mensaje);               
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3ComparteProcedures.sql'));                                                                            
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3ComparteProcedures.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3ComparteProcedures.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                        lErrorEstructura := TRUE;
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3CompartePrinters.sql', Mensaje);
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3CompartePrinters.sql'));
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3CompartePrinters.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3CompartePrinters.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                        lErrorEstructura := TRUE;
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if FContinuar then
               begin
                   lOk := ExisteArchivo(sRuta, '3ComparteVersion.sql', Mensaje);
                   if lOk then
                   begin
                        AdoQuery.SQL.LoadFromFile(GetFileName(sRuta, '3ComparteVersion.sql'));
                        lOk := ExecSQLScript(AdoQuery, Mensaje);
                        if lOk then
                        begin
                            WriteTexto( 'Archivo de estructura: 3ComparteVersion.sql - Ok' );
                        end
                        else
                        begin
                            WriteTexto(Format('Archivo de estructura: 3ComparteVersion.sql - %s ', [Mensaje] ));
                            lErrorEstructura := TRUE;
                        end;
                   end
                   else
                   begin
                        WriteTexto( Mensaje );
                        lErrorEstructura := TRUE;
                   end;
                   if not lOk then
                   begin
                        FContinuar:= ShowConfirmation;
                   end;
               end;

               if  lErrorEstructura then
               begin
                   WriteTexto( 'La base de datos de Comparte terminó con errores. La aplicación no esta lista para ser utilizada.' );
                   if not FContinuar then
                   begin
                       lOk := FALSE;
                       LogEnd;
                   end;
               end
               else
               begin
                  WriteTexto( 'La base de datos se creo correctamente, la aplicación esta lista para ser utilizada.' );
               end;
           end;
     end;
     Result := lOk;
end;

function TdmDBConfig.GetArrayEmpresa(sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, sCM_NIVEL0,
  sCM_CODIGO: string): OleVariant;
// ZetaCommonClasses:
// POSICIONES DEL ARREGLO dmCliente.Empresa
// P_ALIAS = 0;
// P_DATABASE = 0;
// P_USER_NAME = 1;
// P_PASSWORD = 2;
// P_USUARIO = 3;
// P_NIVEL_0 = 4;
// P_CODIGO = 5;
// P_GRUPO = 6;
// P_APPID = 7;

begin
  Result := VarArrayOf([sCM_DATOS, sCM_USRNAME, sCM_PASSWRD, 0, sCM_NIVEL0, sCM_CODIGO, D_GRUPO_SIN_RESTRICCION, 0]);
end;


function TdmDBConfig.LeeXML(const ConnStr: string; lErrorXML: Boolean): Boolean;
var
   cdsXML : TZetaClientDataSet;
   sRuta, Mensaje : string;
   lOk : Boolean;
begin
     Mensaje := VACIO;
     lErrorXML := FALSE;
     cdsXML := TZetaClientDataSet.Create(Self);
     with cdsXML do
     begin
          Active:= False;
          Filtered := False;
          Filter := '';
          IndexFieldNames := '';
          with FBitacora do
          begin
              //Iniciales
              sRuta := VerificaDir(FRegistry.PathConfig) +'Comparte\NuevaBD\Iniciales\';
              WriteTexto( 'Iniciales' );   
              
              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'ACC_ARBOL.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'ACC_ARBOL.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información inicial de la tabla ACC_ARBOL - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'ACC_ARBOL', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo inicial: ACC_ARBOL.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información inicial de la tabla ACC_ARBOL - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'ACC_DER.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'ACC_DER.xml'));
                      Except on e:exception do
                      begin
                           WriteTexto(Format('No se pudo leer la información inicial de la tabla ACC_DER - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'ACC_DER', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo inicial: ACC_DER.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información inicial de la tabla ACC_DER - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              //Sugeridos
              sRuta := VerificaDir(FRegistry.PathConfig) + 'Comparte\NuevaBD\Sugeridos\';
              WriteTexto( 'Sugeridos' );

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'GLOBAL.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'GLOBAL.xml'));
                      Except on e:exception do
                      begin
                           WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla GLOBAL - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'GLOBAL', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: GLOBAL.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla GLOBAL - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'KSCREEN.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'KSCREEN.xml'));
                      Except on e:exception do
                      begin
                           WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla KSCREEN - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'KSCREEN', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: KSCREEN.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla KSCREEN - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'KBOTON.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'KBOTON.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla KBOTON - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'KBOTON', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: KBOTON.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla KBOTON - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'KESTILO.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'KESTILO.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla KESTILO - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'KESTILO', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: KESTILO.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla KESTILO - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'KSHOW.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'KSHOW.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla KSHOW - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'KSHOW', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: KSHOW.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla KSHOW - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'KSHOWINF.xml', Mensaje);
                 if lOk then
                 begin
                      try
                          cdsXML.LoadFromFile(GetFileName(sRuta, 'KSHOWINF.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla KSHOWINF - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                          lOk := FALSE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'KSHOWINF', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: KSHOWINF.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla KSHOWINF - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'ROL.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'ROL.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla ROL - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'ROL', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: ROL.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla ROL - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'USER_ROL.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'USER_ROL.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla USER_ROL - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'USER_ROL', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: USER_ROL.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla USER_ROL - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'WConexion.xml', Mensaje);
                 if lOk then
                 begin
                      try
                        cdsXML.LoadFromFile(GetFileName(sRuta, 'WConexion.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla WConexion - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'WConexion', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: WConexion.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla WConexion - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'WMODELO.xml', Mensaje);
                 if lOk then
                 begin
                      try
                         cdsXML.LoadFromFile(GetFileName(sRuta, 'WMODELO.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla WMODELO - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'WMODELO', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: WMODELO.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla WMODELO - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'WMOD_ROL.xml', Mensaje);
                 if lOk then
                 begin
                      try
                         cdsXML.LoadFromFile(GetFileName(sRuta, 'WMOD_ROL.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla WMOD_ROL - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'WMOD_ROL', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo Sugerido: WMOD_ROL.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla WMOD_ROL - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'WMODSTEP.xml', Mensaje);
                 if lOk then
                 begin
                      try
                          cdsXML.LoadFromFile(GetFileName(sRuta, 'WMODSTEP.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla WMODSTEP - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                          lOk := FALSE;
                      end;

                      end;
                      lOk := GrabaXML(cdsXML, 'WMODSTEP', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: WMODSTEP.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla WMODSTEP - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              //Reportes
              sRuta := VerificaDir(FRegistry.PathConfig) +'Comparte\NuevaBD\Sugeridos\';
              WriteTexto( 'Reportes' );

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'REPORTE.xml', Mensaje);
                 if lOk then
                 begin
                      try
                          cdsXML.LoadFromFile(GetFileName(sRuta, 'REPORTE.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla REPORTE - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'REPORTE', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: REPORTE.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla REPORTE - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

              if FContinuar then
              begin
                 lOk := ExisteArchivo(sRuta, 'CAMPOREP.xml', Mensaje);
                 if lOk then
                 begin
                      try
                         cdsXML.LoadFromFile(GetFileName(sRuta, 'CAMPOREP.xml'));
                      Except on e:exception do
                      begin
                          WriteTexto(Format('No se pudo leer la información de sugeridos para la tabla CAMPOREP - %s ', [Mensaje] ));
                          lOk := FALSE;
                          lErrorXML := TRUE;
                      end;
                      end;

                      lOk := GrabaXML(cdsXML, 'CAMPOREP', AdoQuery, Mensaje);
                      if lOk then
                      begin
                          WriteTexto( 'Archivo sugerido: CAMPOREP.xml - Ok' );
                      end
                      else
                      begin
                          WriteTexto(Format('No se pudo subir la información de sugeridos para la tabla CAMPOREP - %s ', [Mensaje] ));
                          lErrorXML := TRUE;
                      end;
                 end
                 else
                 begin
                      WriteTexto( Mensaje );
                      lErrorXML := TRUE;
                 end;
                 if not lOk then
                 begin
                      FContinuar:= ShowConfirmation;
                 end;
              end;

            //Diccionario
            sRuta := VerificaDir(FRegistry.PathConfig) + 'Comparte\NuevaBD\Diccionario\';
             WriteTexto( 'Diccionario' );
            if FContinuar then
            begin
               lOk := ExisteArchivo(sRuta, 'Diccion.xml', Mensaje);
               if lOk then
               begin
                    try
                       cdsXML.LoadFromFile(GetFileName(sRuta, 'Diccion.xml'));
                    Except on e:exception do
                    begin
                         WriteTexto(Format('No se pudo leer la información de Diccionario para la tabla Diccion - %s ', [Mensaje] ));
                         lOk := FALSE;
                         lErrorXML := TRUE;
                    end;
                    end;

                    lOk := GrabaXML(cdsXML, 'Diccion', AdoQuery, Mensaje);
                    if lOk then
                    begin
                        WriteTexto( 'Archivo de diccionario: Diccion.xml - Ok' );
                    end
                    else
                    begin
                        WriteTexto(Format('No se pudo subir la información de Diccionario para la tabla Diccion - %s ', [Mensaje] ));
                        lErrorXML := TRUE;
                    end;
               end
               else
               begin
                    WriteTexto( Mensaje );
                    lErrorXML := TRUE;
               end;
               if not lOk then
               begin
                    FContinuar:= ShowConfirmation;
               end;

               if  lErrorXML then
               begin
                    lOk := FALSE;
               end;
            end;
       end;
       Result := lOk;
     LogEnd;
  end;
end;

function TdmDBConfig.GrabaComparte(oDelta: OleVariant; out ErrorCount: Integer): OleVariant;
begin
     if VarIsNull(oDelta) then
     begin
        ErrorCount :=0;
        Result := Null;
     end;
     with oZetaProvider do
     begin
          EmpresaActiva := Comparte;
          SetTablaInfo( eTablasXML );
          Result := GrabaTabla( Comparte, oDelta, ErrorCount );
     end;
     //SetComplete;
end;

procedure TdmDBConfig.GrabaDataSet(Origen: TClientDataSet; Tabla: string; ADOQuery: TADOQuery);
var
  Campos: TStringList;
  I: Integer;
begin
  Campos := TStringList.Create;
  with Origen do try
    // Obtener los nombres de Campo
    for I := 0 to Fields.Count - 1 do
      Campos.Add(Fields[I].FieldName);

    // Formar un SQL
    ADOQuery.SQL.Clear;
    ADOQuery.SQL.Text := Format('insert into %s (%s)', [Tabla, Campos.CommaText]);
    for I := 0 to Campos.Count - 1 do
      Campos[I] := ':' + Campos[I];
    ADOQuery.SQL.Text := ADOQuery.SQL.Text + ' values (' + Campos.CommaText + ')';

    // Insertar registro a registro
    First;
    while not Eof do begin
      for I := 0 to Fields.Count - 1 do
        ADOQuery.Parameters[I].Value := Fields[I].Value;
      ADOQuery.ExecSQL;
      Next;
    end;
  except
    on e: Exception do
      Application.MessageBox({$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(e.ClassName + ': ' + e.Message), 'CopiaDataSet', 0);
  end;
end;

function TdmDBConfig.GrabaXML(Origen: TClientDataSet; Tabla: string; ADOQuery: TADOQuery; out Mensaje: string): Boolean;
var
  Campos: TStringList;
  I: Integer;
begin
  Mensaje := VACIO;
  Result := TRUE;
  Campos := TStringList.Create;
  with Origen do
  try
    // Obtener los nombres de Campo
    for I := 0 to Fields.Count - 1 do
      Campos.Add(Fields[I].FieldName);

    // Formar un SQL
    ADOQuery.SQL.Clear;
    ADOQuery.SQL.Text := Format('insert into %s (%s)', [Tabla, Campos.CommaText]);
    for I := 0 to Campos.Count - 1 do
      Campos[I] := ':' + Campos[I];
    ADOQuery.SQL.Text := ADOQuery.SQL.Text + ' values (' + Campos.CommaText + ')';

    // Insertar registro a registro
    First;
    while not Eof do begin
      for I := 0 to Fields.Count - 1 do
        ADOQuery.Parameters[I].Value := Fields[I].Value;
      ADOQuery.ExecSQL;
      Next;
    end;
  except
    on e: Exception do
    begin
      Mensaje:= {$ifdef TRESS_DELPHIXE5_UP}PWideChar{$else}PChar{$endif}(e.ClassName + ': ' + e.Message);
      Result := FALSE;
    end;
  end;
end;

procedure TdmDBConfig.SetTablaInfo( const eTipoTabla: eTipoConfig );
begin
     with oZetaProvider.TablaInfo do
     begin
     case eTipoTabla of
          eTablasXML: SetInfo( 'ACC_ARBOL',
          'AA_SOURCE, AX_NUMERO, AA_DESCRIP, AA_VERSION, AA_POSICIO,A_MODULO, AA_SCREEN, AA_INGLES',
          'AA_SOURCE, AX_NUMERO' );
          end;
     end;
end;

procedure TdmDBConfig.DataModuleCreate(Sender: TObject);
begin
     FRegistry := TZetaRegistryServer.Create;
     FBitacora := TAsciiLog.Create;
     //Connector := TADOConnection.Create(Self);
     oZetaProvider := DZetaServerProvider.GetZetaProvider( Self );
end;

procedure TdmDBConfig.DataModuleDestroy(Sender: TObject);
begin
     DZetaServerProvider.FreeZetaProvider( oZetaProvider );
     FreeAndNil( FBitacora );
     FreeAndNil( FRegistry );
end;

function TdmDBConfig.GetServer: string;
var
  Query,Mensaje:string;
{$IFDEF MSSQL}
const
  K_PRODUCT_VERSION = 'productversion';
  K_QRY_PROVIDER_SERVER_NAME = 'select SERVERPROPERTY(%s) as propiedad';
  {$ENDIF}
begin
     if ConectarseServer(GetConnStr,Mensaje)then
     begin
     try
        try
          Query := Format( K_QRY_PROVIDER_SERVER_NAME, [EntreComillas(K_PRODUCT_VERSION)]);
          OpenQuery(Query);
          Result := AdoQuery.FieldByName('propiedad').AsString;
          Except on e:exception do
          begin
               DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
          end;
         end;
      finally
        CerrarServer;
      end;
     end
     else
     begin
         Result := VACIO;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.GetServerName: string;
var
  Query:string;
{$IFDEF MSSQL}
const
  K_SERVER_NAME = 'SERVERNAME';
  K_QRY_PROVIDER_SERVER_NAME = 'select SERVERPROPERTY(%s) as ServerName';

  {$ENDIF}
begin
    try
      Query := Format( K_QRY_PROVIDER_SERVER_NAME, [EntreComillas(K_SERVER_NAME)]);
      OpenQuery(Query);
      Result := AdoQuery.FieldByName('ServerName').AsString;
      Except on e:exception do
      begin
           DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
      end;
     end;
end;

function TdmDBConfig.IsLocalServer(const ConnStr: string): string;
var
  Query,Mensaje:string;
{$IFDEF MSSQL}
const
  MACHINE_NAME = 'MachineName';
  LOCAL = 'local';
  REMOTE = 'Remote';
  K_QRY_PROVIDER_IS_LOCAL_SERVER = 'SELECT Case when HOST_NAME()=SERVERPROPERTY(%s) then %s else %s end as SERVER';

  {$ENDIF}
begin
     if ConectarseServer(ConnStr,Mensaje)then
     begin
     try
        try
          Query := Format( K_QRY_PROVIDER_IS_LOCAL_SERVER, [EntreComillas(MACHINE_NAME),EntreComillas(LOCAL), EntreComillas(REMOTE)]);
          OpenQuery(Query);
          Result := AdoQuery.FieldByName('SERVER').AsString;
          Except on e:exception do
          begin
               DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
          end;
         end;
      finally
        CerrarServer;
      end;
     end
     else
     begin
         Result := VACIO;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.DesbloqueaAdministradores: Boolean;
var
  Query,Mensaje:string;
{$IFDEF MSSQL}
const
  K_QRY_BLOQUEA_USUARIO = 'update USUARIO set US_BLOQUEA = %s where ( GR_CODIGO = %d )';

  {$ENDIF}

begin
     if ConectarseServer(GetConnStr,Mensaje)then
     begin
          try
            try
                Query := Format(K_QRY_BLOQUEA_USUARIO,[EntreComillas( ZetaCommonClasses.K_GLOBAL_NO ), D_GRUPO_SIN_RESTRICCION]);
                EjecutaQuery(Query);
                Result := true;
                Except on e:exception do
                begin
                     DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
                end;
            end;
          finally
            CerrarServer;
          end;
     end
     else
     begin
         Result := False;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.CargarUsuarios(var cdsUsuariosSistema : TZetaClientDataSet): Boolean;
var
    tqList : TZetaCursor;
    SQL: String;
    sCM_DATOS: String;
const
    K_QRY_CARGA_USUARIOS = 'Select US_CORTO as [Usuario] ,  US_NOMBRE as [Nombre], US_FEC_IN as [Desde] from USUARIO where US_DENTRO = %s';
begin
     sCM_DATOS := VACIO;
     try
        SQL :=  VACIO;

        SQL := Format(K_QRY_CARGA_USUARIOS,[EntreComillas( ZetaCommonClasses.K_GLOBAL_SI )]);

        oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;

        tqList := oZetaProvider.CreateQuery(SQL);

        with tqList do
        begin
              tqList.Active := true;
             while not Eof do
             begin
                  cdsUsuariosSistema.Append;
                  cdsUsuariosSistema.FieldByName( 'colUsuario' ).AsString := FieldByName( 'Usuario' ).AsString;
                  cdsUsuariosSistema.FieldByName( 'colName' ).AsString := FieldByName( 'Nombre' ).AsString;
                  cdsUsuariosSistema.FieldByName( 'colDesde' ).AsString := FieldByName( 'Desde' ).AsString;
                  cdsUsuariosSistema.Post;
                  Next;
             end;
             tqList.Active := False;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                cdsUsuariosSistema.EmptyDataset;
           end;
     end;
end;


function TdmDBConfig.DesconectarUsuarios: Boolean;
var
  Query,Mensaje:string;
const
  K_QRY_DESCONECTA_USUARIOS = 'declare @Fecha Fecha set @Fecha =  DATEADD(dd, DATEDIFF(dd, 0, getdate()), 0) UPDATE USUARIO set US_DENTRO = %s, US_FEC_OUT = @Fecha  where  US_DENTRO = %s ';
begin
     if ConectarseServer(GetConnStr,Mensaje)then
     begin
          try
            try
                Query := Format(K_QRY_DESCONECTA_USUARIOS,[EntreComillas( ZetaCommonClasses.K_GLOBAL_NO ) ,EntreComillas( ZetaCommonClasses.K_GLOBAL_SI )]);
                EjecutaQuery(Query);
                Result := true;
                Except on e:exception do
                begin
                     DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
                end;
            end;
          finally
            CerrarServer;
          end;
     end
     else
     begin
         Result := False;
         DatabaseError(Mensaje);
     end;
end;


function TdmDBConfig.EsWorkflowPreparado: Boolean;
var
  Query,Mensaje:string;
{$IFDEF MSSQL}
const
  K_XCOMPARTEY = 'XCOMPARTEY';
  K_QRY_EXISTE_XCOMPARTEY = 'select COUNT(*) as CUANTOS from Company where CM_CODIGO = %s';
  {$ENDIF}
begin
     if ConectarseServer(ConnStr,Mensaje)then
     begin
          try
            try
                Query := Format(K_QRY_EXISTE_XCOMPARTEY,[EntreComillas( K_XCOMPARTEY )]);
                OpenQuery(Query);
                Result := AdoQuery.FieldByName('CUANTOS').AsInteger > 0;
                Except on e:exception do
                begin
                     DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
                end;
            end;
          finally
            CerrarServer;
          end;
     end
     else
     begin
         Result := False;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.PrepararWorkflow: Boolean;
var
  Query,Mensaje:string;
{$IFDEF MSSQL}
const
  XCOMPARTEY = 'XCOMPARTEY';
  DESCRIPCION = 'Datos de Workflow';

  K_QRY_INSERTA_DB_INFO = 'INSERT INTO DB_INFO (DB_CODIGO, DB_DESCRIP, DB_CONTROL, DB_TIPO, DB_DATOS, DB_USRNAME, ' +
  'DB_PASSWRD, DB_CTRL_RL)' +
  'VALUES (%s, %s, %s, %d, %s, %s, %s, %s)';

  K_QRY_INSERTA_COMPANY = 'INSERT INTO Company (CM_CODIGO, CM_NOMBRE,  CM_PASSWRD, CM_USRNAME, CM_DATOS,  ' +
  'DB_CODIGO) ' +
  'VALUES (%s, %s,  %s, %s, %s, %s)';
  {$ENDIF}
begin
     if ConectarseServer(ConnStr,Mensaje)then
     begin
         try
              try
                  with FRegistry do
                  begin
                      Query := Format(K_QRY_INSERTA_DB_INFO,[EntreComillas( XCOMPARTEY ), EntreComillas( DESCRIPCION ),EntreComillas(ObtieneElemento(lfTipoCompany, ord(tcOtro))), ord(tcOtro),
                      EntreComillas(DataBase), EntreComillas(UserName), EntreComillas(ZetaServerTools.Encrypt (Password)), EntreComillas(ObtieneElemento(lfTipoCompany, ord(tcOtro)))]);
                  end;
                  EjecutaQuery(Query);
                  Result := true;
                  Except on e:exception do
                  begin
                       DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
                       Result := FALSE;
                  end;
               end;

               if Result then
               begin
                    try
                        with FRegistry do
                        begin
                            Query := Format(K_QRY_INSERTA_COMPANY,[EntreComillas( XCOMPARTEY ), EntreComillas( XCOMPARTEY ),
                            EntreComillas( Password ), EntreComillas(UserName), EntreComillas(DataBase),
                            EntreComillas( XCOMPARTEY )]);
                        end;
                        EjecutaQuery(Query);
                        Result := true;
                        Except on e:exception do
                        begin
                             DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
                             Result := FALSE;
                        end;
                    end;
               end;
         finally
           CerrarServer;
         end;
         end
         else
         begin
             Result := False;
             DatabaseError(Mensaje);
         end;
end;

function TdmDBConfig.LoadVersion  : String;
var
  Query, Mensaje:string;
const
  K_QRY_GET_VERSION = 'select CL_TEXTO from claves where CL_NUMERO=100';
begin
      if ConectarseServer(GetConnStr,Mensaje)then
      begin
          try
            try
                Query := (K_QRY_GET_VERSION);
                OpenQuery(Query);
                Result := AdoQuery.FieldByName('CL_TEXTO').AsString;
                Except on e:exception do
                begin
                     DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
                end;
            end;
          finally
             CerrarServer;
          end;
     end
     else
     begin
         Result := VACIO;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.GetPathDefaultSQLServer(const ConnStr: string): String;
var
  Mensaje:string;
const
  K_QRY_GET_PATH = 'SELECT SUBSTRING(physical_name, 1, CHARINDEX(N''master.mdf'', LOWER(physical_name)) - 1)  AS PATH FROM master.sys.master_files WHERE database_id = 1 AND file_id = 1';
begin
      if ConectarseServer(ConnStr,Mensaje)then
      begin
          try
            try
              OpenQuery(K_QRY_GET_PATH);
              Result := AdoQuery.FieldByName('PATH').AsString;
              Except on e:exception do
              begin
                   DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
              end;
             end;
        finally
          CerrarServer
    end;
     end
     else
     begin
         Result := VACIO;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.DataBaseExist(const ConnStr: string;Database: String): Boolean;
var
Query, Mensaje: String;
const
  K_QUERY = 'SELECT name FROM master.dbo.sysdatabases WHERE name = %s';
begin
      if ConectarseServer(ConnStr,Mensaje)then
      begin
    try
        try
          Query := Format( K_QUERY, [EntreComillas(database)]);
          OpenQuery(Query);
          Result := StrLleno( AdoQuery.FieldByName('name').AsString);
          Except on e:exception do
          begin
               DatabaseError('Hubo un error al intentar conectarse a la base de datos. Error:' + #13#10 + e.Message);
          end;
         end;
    finally
       CerrarServer;
    end;
     end
     else
     begin
         Result := False;
         DatabaseError(Mensaje);
     end;
end;

function TdmDBConfig.GetConnStr: widestring;
begin
     with FRegistry do
     begin
         SC.ServerName := ServerDB;
         SC.DatabaseName := DataBaseADO;
         SC.UserName := UserName;
         SC.Password := Password;
     end;

     Result := Format( 'Provider=%s;', [ FRegistry.SQLNCLIProvider ] );
     Result := Result + 'Data Source=' + Trim(SC.ServerName) + ';';
     if SC.DatabaseName <> '' then
        Result := Result + 'Initial Catalog=' + Trim(SC.DatabaseName) + ';';

     Result := Result + 'uid=' + Trim(SC.UserName) + ';';
     Result := Result + 'pwd=' + Trim(SC.Password) + ';';
     Result := Result + 'MultipleActiveResultSets=true;Pooling=true;DataTypeCompatibility=80;Language=english';
end;

procedure TdmDBConfig.EmpresasConectar( const lTodas: Boolean  ; const eTipo  : eTipoCompany = tc3Datos);
var tqList : TZetaCursor;
    SQL: String;
    sCM_DATOS: String;
begin
     sCM_DATOS := VACIO;
     { Primero llenar la lista de empresas }
     try
        SQL :=  'select CM_CODIGO, CM_NOMBRE, CM_ACTIVOS, CM_ALIAS, CM_USRNAME, CM_PASSWRD, CM_DATOS, CM_NIVEL0, CM_KCLASIFI, CM_CTRL_RL, CM_CHKSUM from COMPANY where ';
        if not lTodas then
        begin
             SQL := SQL + ' ( CM_NIVEL0 = '''' ) and ';
        end;

        SQL := SQL + Format( '( %s ) order by CM_CODIGO', [ ZetaServerTools.GetTipoCompany( Ord( eTipo ) ) ] );

        oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;

        tqList := oZetaProvider.CreateQuery(SQL);

        with tqList do
        begin
             Active := True;
             with cdsEmpresas do
             begin
                  InitTempDataSet;
                  AddStringField( 'CM_CODIGO', tqList.FieldByName( 'CM_CODIGO' ).DataSize );
                  AddStringField( 'CM_NOMBRE', tqList.FieldByName( 'CM_NOMBRE' ).DataSize );
                  AddStringField( 'CM_ALIAS', tqList.FieldByName( 'CM_ALIAS' ).DataSize );
                  AddStringField( 'CM_USRNAME', tqList.FieldByName( 'CM_USRNAME' ).DataSize );
                  AddStringField( 'CM_PASSWRD', tqList.FieldByName( 'CM_PASSWRD' ).DataSize );
                  AddStringField( 'CM_DATOS', tqList.FieldByName( 'CM_DATOS' ).DataSize );
                  AddStringField( 'CM_NIVEL0', tqList.FieldByName( 'CM_NIVEL0' ).DataSize );
                  AddStringField( 'CM_CTRL_RL', tqList.FieldByName( 'CM_CTRL_RL' ).DataSize );
                  AddStringField( 'CM_CHKSUM', tqList.FieldByName( 'CM_CHKSUM' ).DataSize );
                  AddIntegerField( 'CM_ASIGNED' );
                  AddIntegerField( 'CM_ACTIVOS' );
                  AddIntegerField( 'CM_INACTIV' );
                  AddIntegerField( 'CM_TOTAL' );
                  AddDateField( 'CM_ULT_ALT' );
                  AddDateField( 'CM_ULT_BAJ' );
                  AddDateField( 'CM_ULT_TAR' );
                  AddStringField( 'CM_ULT_NOM', K_ANCHO_ULTIMA_NOMINA );
                  AddIntegerField( 'PE_YEAR' );
                  AddIntegerField( 'PE_TIPO' );
                  AddIntegerField( 'PE_NUMERO' );
                  AddDateField( 'PE_FEC_INI' );
                  AddStringField( 'CM_CONNECT', 1 );
                  AddIntegerField( 'PC_SI_QTY' );
                  AddDateField( 'PC_SI_FEC' );
                  AddIntegerField( 'PC_AN_QTY' );
                  AddDateField( 'PC_AN_FEC' );
                  AddIntegerField( 'CM_KCLASIFI');
                  AddFloatField( 'PC_NOMLAVG');
                  AddFloatField( 'PC_NOM_AVG');

                  AddIntegerField( 'PC_FOA_QTY' );
                  AddDateField( 'PC_FOA_FEC' );
                  AddIntegerField( 'PC_FON_QTY' );
                  AddDateField( 'PC_FON_FEC' );
                  AddIntegerField( 'PC_FOC_QTY' );
                  AddDateField( 'PC_FOC_FEC' );
                  AddIntegerField( 'PC_LB_QTY' );
                  AddDateField( 'PC_LB_FEC' );
                  AddIntegerField( 'PC_EM_QTY' );
                  AddDateField( 'PC_EM_FEC' );
                  AddIntegerField( 'RE_CU_QTY' );
                  AddIntegerField( 'RE_CF_QTY' );
                  AddDateField( 'RE_CF_FEC' );
                  AddIntegerField( 'RE_RG_QTY' );
                  AddDateField( 'RE_RG_FEC' );
                  AddIntegerField( 'RE_SM_QTY' );
                  AddDateField( 'RE_SM_FEC' );
                  AddIntegerField( 'RE_PC_QTY' );
                  AddDateField( 'RE_PC_FEC' );
                  AddIntegerField( 'RE_EV_QTY' );
                  AddDateField( 'RE_EV_FEC' );

                  AddFloatField( 'DB_SIZE');
                  AddStringField( 'MS_SQL_VER', 256 );
                  AddStringField( 'MS_SQL_LEV', 256 );
                  AddStringField( 'MS_SQL_EDT', 256 );

                  AddMemoField('COMPANYXMLDATA', 32000);
                  AddMemoField('XMLINFOUSOTRESS', 32000);

                  CreateTempDataset;
                  LogChanges := False;
             end;
             while not Eof do
             begin
                  if Pos ('-'+FieldByName( 'CM_DATOS' ).AsString+'-', sCM_DATOS) = 0 then
                  begin
                      sCM_DATOS := sCM_DATOS + '-' + FieldByName( 'CM_DATOS' ).AsString + '-';
                      cdsEmpresas.Append;
                      cdsEmpresas.FieldByName( 'CM_CODIGO' ).AsString := FieldByName( 'CM_CODIGO' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_NOMBRE' ).AsString := FieldByName( 'CM_NOMBRE' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_ALIAS' ).AsString := FieldByName( 'CM_ALIAS' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_USRNAME' ).AsString := FieldByName( 'CM_USRNAME' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_PASSWRD' ).AsString := FieldByName( 'CM_PASSWRD' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_DATOS' ).AsString := FieldByName( 'CM_DATOS' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_NIVEL0' ).AsString := FieldByName( 'CM_NIVEL0' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_CTRL_RL' ).AsString := FieldByName( 'CM_CTRL_RL' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_CHKSUM' ).AsString := FieldByName( 'CM_CHKSUM' ).AsString;
                      cdsEmpresas.FieldByName( 'CM_CONNECT' ).AsString := K_GLOBAL_NO;
                      cdsEmpresas.FieldByName('CM_KCLASIFI').AsInteger:= FieldByName('CM_KCLASIFI').AsInteger;
                      cdsEmpresas.FieldByName( 'COMPANYXMLDATA' ).AsString := VACIO;
                      cdsEmpresas.FieldByName( 'DB_SIZE' ).AsFloat := 0;
                      cdsEmpresas.Post;
                  end;
                  Next;
             end;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                { Para que no haya nada que modificar }
                cdsEmpresas.EmptyDataset;
           end;
     end;
end;

function TdmDBConfig.GetTerminales( const lTodas: Boolean ): string;
var tqList : TZetaCursor;
    SQL: String;
begin
     try
        SQL :=  'select (select TE_CODIGO, TE_NOMBRE, TE_TEXTO,TE_MAC, TE_APP, TE_ACTIVO, TE_HUELLAS, TE_VERSION, TE_ID  from TERMINAL TERMINAL FOR XML AUTO,  TYPE, ROOT(''TERMINALES'')) as Resultado;';
        oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;

        tqList := oZetaProvider.CreateQuery(SQL);
        with tqList do
        begin
             Active := True;
             Result := FieldByName( 'Resultado' ).AsString ;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := VACIO;
           end;
     end;
end;

procedure TdmDBConfig.SetAutoServer(value: TAutoServer);
begin
     Self.FAutoServer := value;
     Self.FGlobalLicense.AutoServer := value;
end;

function TdmDBConfig.UsuariosContar: Integer;
var tqList : TZetaCursor;
begin
     try
        oZetaProvider.EmpresaActiva := oZetaProvider.Comparte;
        tqList := oZetaProvider.CreateQuery('select COUNT(*) as CUANTOS from USUARIO');
        with tqList do
        begin
             Active := True;
             Result := Fields[ 0 ].AsInteger;
             Active := False;
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                Result := 0;
           end;
     end;
end;

function TdmDBConfig.SeleccionLeer(Manager: TLicenseMgr;
  Provider: TdmZetaServerProvider; Respuesta: TCallBack;
  RespuestaError: TCallBackError) : TDatosSeleccion;
var
   FDatosSeleccion: TDatosSeleccion;
   sEmpresa, sRazonSocial, sBD : string;
   lOk : boolean;
begin

     with Result do
     begin
          BDCuantos := 0;
          RequisicionCuantos := 0;
          RequisicionUltimo := 0;
     end;

     with cdsEmpresas do
     begin
          First;
          {$ifdef VALIDA_EMPLEADOS}
          while not Eof and lOk do
          {$else}
          while not Eof do
          {$endif}
          begin
               sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
               sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
               sBD := FieldByName( 'CM_DATOS' ).AsString;

               lOk := True;

               try
                  Respuesta( Format( 'Recolectando dimensión de la Base de Datos %s', [ sEmpresa ] ), 0);
                  with Provider do
                  begin
                       Deactivate;     //Se agrega para descontaminar los accesos pasados
                       Activate;
                       EmpresaActiva := BuildEmpresa( dmDBConfig.cdsEmpresas );
                       with Manager do
                       begin
                            FDatosSeleccion := RegistrosSeleccion;
                       end;
                       Deactivate;
                  end;
               except
                     on Error: Exception do
                     begin
                          RespuestaError( Format( 'No Fué Posible Leer La BD de Seleccion de Personal %s: ' + CR_LF + '%s', [ sEmpresa, sRazonSocial ] ), Error );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                     with Result do
                     begin
                         BDCuantos := BDCuantos + 1;
                         RequisicionCuantos := RequisicionCuantos + FDatosSeleccion.RequisicionCuantos;
                         if ( FDatosSeleccion.RequisicionUltimo > RequisicionUltimo ) then
                            RequisicionUltimo := FDatosSeleccion.RequisicionUltimo;
                     end;
               end;
               Next;
          end;
          First;
     end;
end;

function TdmDBConfig.BuildEmpresa(DataSet : TDataSet):OleVariant;
{
Las posiciones de este arreglo se encuentran en ZetaCommonClasses:

P_ALIAS = 0;
P_DATABASE = 0;
P_USER_NAME = 1;
P_PASSWORD = 2;
P_USUARIO = 3;
P_NIVEL_0 = 4;
P_CODIGO = 5;
}
begin
     with DataSet do
     begin
          Result := VarArrayOf( [ FieldByName( 'CM_DATOS' ).AsString,
                                  FieldByName( 'CM_USRNAME' ).AsString,
                                  FieldByName( 'CM_PASSWRD' ).AsString,
                                  0,
                                  FieldByName( 'CM_NIVEL0' ).AsString,
                                  FieldByName( 'CM_CODIGO' ).AsString ] );
     end;
end;

function TdmDBConfig.VisitantesLeer(Manager: TLicenseMgr;
  Provider: TdmZetaServerProvider; Respuesta: TCallBack;
  RespuestaError: TCallBackError) : TDatosVisitantes;
var
   FDatosVisitantes: TDatosVisitantes;
   sEmpresa, sRazonSocial, sBD : string;
   lOk : boolean;
begin

     with Result do
     begin
          BDCuantos := 0;
          LibroCuantos := 0;
          LibroUltimo := 0;
     end;

     with cdsEmpresas do
     begin
          First;
          {$ifdef VALIDA_EMPLEADOS}
          while not Eof and lOk do
          {$else}
          while not Eof do
          {$endif}
          begin
               sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
               sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
               sBD := FieldByName( 'CM_DATOS' ).AsString;

               lOk := True;

               try
                  Respuesta( Format( 'Recolectando dimensión de la Base de Datos %s', [ sEmpresa ]), 0 );
                  with Provider do
                  begin
                       Deactivate;     //Se agrega para descontaminar los accesos pasados
                       Activate;
                       EmpresaActiva := BuildEmpresa( dmDBConfig.cdsEmpresas );
                       with Manager do
                       begin
                            FDatosVisitantes := RegistrosVisitantes;
                       end;
                       Deactivate;
                  end;
               except
                     on Error: Exception do
                     begin
                          RespuestaError( Format( 'No Fué Posible Leer La BD de Visitantes %s: ' + CR_LF + '%s', [ sEmpresa, sRazonSocial ] ), Error );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                     with Result do
                     begin
                         BDCuantos := BDCuantos + 1;
                         LibroCuantos := LibroCuantos + FDatosVisitantes.LibroCuantos;
                         if ( FDatosVisitantes.LibroUltimo > LibroUltimo ) then
                            LibroUltimo := FDatosVisitantes.LibroUltimo;
                     end;
               end
               else
               begin
               end;
               Next;
          end;
          First;
     end;
end;

procedure TdmDBConfig.LicenciasLeer( Manager: TLicenseMgr; Provider: TdmZetaServerProvider; Respuesta: TCallBack; RespuestaError: TCallBackError );
const
     K_MESES_3DT_HISTORIA = 3;
     K_PORCENTAJE_EMPRESAS = 60;
var
   lOk : Boolean;
   iEmpleados, iDesempleados, iAsignados: Integer;
   sEmpresa, sRazonSocial, sUltimaNomina, sCompanyXMLData, sBD : String;
   dUltimaAlta, dUltimaBaja, dUltimaTarjeta: TDate;
   fNominaDuracionAvgDias, fNominaDuracionAvgTotal, fDBSize : Double;
   FPeriodo: TDatosPeriodo;
   FProcesos: TDatosUltimosProcesos;
   FRegistros : TDatosUltimosRegistros;
   FVerSQLServer : TSQLServerVersion;
   dProgresoXEmpresa: Double;
   sCM_CTRL_RL: String;
   sXmlInfoUsoTress: String;

begin
     {$ifdef VALIDA_EMPLEADOS}
     lOk := True;
     {$endif}
     iAsignados := 0;
     iEmpleados := 0;
     iDesempleados := 0;
     dUltimaTarjeta := NullDateTime;
     dUltimaAlta := NullDateTime;
     dUltimaBaja := NullDateTime;
     fNominaDuracionAvgDias :=0.00;
     fNominaDuracionAvgTotal := 0.00;
     fDBSize := -1;
     sCompanyXMLData := VACIO;


     if cdsEmpresas.RecordCount > 0 then
        dProgresoXEmpresa := (K_PORCENTAJE_EMPRESAS/cdsEmpresas.RecordCount)
     else
        Respuesta( 'No hay compañías registradas', K_PORCENTAJE_EMPRESAS );

     with cdsEmpresas do
     begin
          First;
          {$ifdef VALIDA_EMPLEADOS}
          while not Eof and lOk do
          {$else}
          while not Eof do
          {$endif}
          begin
               sEmpresa := FieldByName( 'CM_CODIGO' ).AsString;
               sRazonSocial := FieldByName( 'CM_NOMBRE' ).AsString;
               sBD := FieldByName( 'CM_DATOS' ).AsString;
               sCM_CTRL_RL := FieldByName( 'CM_CTRL_RL' ).AsString;
               sXmlInfoUsoTress := VACIO;

               {$ifdef VALIDA_EMPLEADOS}
               {$else}
               lOk := True;
               {$endif}
               try
                  Respuesta( Format( 'Recolectando dimensión de la Base de Datos %s', [ sEmpresa ] ), dProgresoXEmpresa );
                  with Provider do
                  begin
                       Deactivate;     //Se agrega para descontaminar los accesos pasados
                       Activate;
                       EmpresaActiva := BuildEmpresa( dmDBConfig.cdsEmpresas );
                       with Manager do
                       begin
                            iAsignados := AsignadosGet;
                            iEmpleados := EmpleadosGet;
                            iDesempleados := DesempleadosGet;
                            dUltimaAlta := UltimaAlta;
                            dUltimaBaja := UltimaBaja;
                            dUltimaTarjeta := UltimaTarjeta;
                            FPeriodo := UltimaNomina;
                            fNominaDuracionAvgDias := DuracionNomina(K_MESES_3DT_HISTORIA);
                            fNominaDuracionAvgTotal := DuracionNomina(660);  //-55 years
                            fDBSize :=  GetDatabaseSize( sBD );
                            with FPeriodo do
                            begin
                                 sUltimaNomina := Format( '%s ( %s )', [ ZetaCommonTools.GetPeriodoInfo( Year, Numero, Tipo ), ZetaCommonTools.FechaCorta( Inicio ) ] );
                            end;
                            FProcesos := UltimosProcesos;
                            FRegistros := UltimosRegistros;
                            FVerSQLServer := GetSQLServerVersion;
                            sCompanyXMLData := GetCompanyXMLStr (sCM_CTRL_RL);
                            sXmlInfoUsoTress := GetInfoUsoTressStr (sCM_CTRL_RL);
                       end;
                       Deactivate;
                  end;
               except
                     on Error: Exception do
                     begin
                          RespuestaError( Format( 'No Fué Posible Leer Los Empleados Asignados A La Empresa %s: ' + CR_LF + '%s', [ sEmpresa, sRazonSocial ] ), Error );
                          lOk := False;
                     end;
               end;
               if lOk then
               begin
                    Edit;
                    FieldByName( 'CM_ACTIVOS' ).AsInteger := iEmpleados;
                    FieldByName( 'CM_INACTIV' ).AsInteger := iDesempleados;
                    FieldByName( 'CM_TOTAL' ).AsInteger := iEmpleados + iDesempleados;
                    FieldByName( 'CM_ASIGNED' ).AsInteger := iAsignados;
                    FieldByName( 'CM_ULT_ALT' ).AsDateTime := dUltimaAlta;
                    FieldByName( 'CM_ULT_BAJ' ).AsDateTime := dUltimaBaja;
                    FieldByName( 'CM_ULT_TAR' ).AsDateTime := dUltimaTarjeta;
                    FieldByName( 'CM_ULT_NOM' ).AsString := Copy( sUltimaNomina, 1, K_ANCHO_ULTIMA_NOMINA );
                    FieldByName( 'DB_SIZE' ).AsFloat := fDBSize;

                    with FVerSQLServer do
                    begin
                        FieldByName( 'MS_SQL_VER' ).AsString := Version;
                        FieldByName( 'MS_SQL_LEV' ).AsString := Nivel;
                        FieldByName( 'MS_SQL_EDT' ).AsString := Edicion;
                    end;

                    //Datos de Modulos
                    with FRegistros do
                    begin
                        FieldByName( 'RE_CU_QTY' ).AsInteger := CursosEntrenaCuantos;
                        FieldByName( 'RE_CF_QTY' ).AsInteger := CafeteriaCuantos;
                        FieldByName( 'RE_CF_FEC' ).AsDateTime:= CafeteriaUltimo;
                        FieldByName( 'RE_RG_QTY' ).AsInteger := ResguardoCuantos;
                        FieldByName( 'RE_RG_FEC' ).AsDateTime:= ResguardoUltimo;
                        FieldByName( 'RE_SM_QTY' ).AsInteger := SMedicosCuantos;
                        FieldByName( 'RE_SM_FEC' ).AsDateTime:= SMedicosUltimo;
                        FieldByName( 'RE_PC_QTY' ).AsInteger := PlanCarreraCuantos;
                        FieldByName( 'RE_PC_FEC' ).AsDateTime:= PlanCarreraUltimo;
                        FieldByName( 'RE_EV_QTY' ).AsInteger := EvaluacionCuantos;
                        FieldByName( 'RE_EV_FEC' ).AsDateTime:= EvaluacionUltimo;
                    end;

                    with FPeriodo do
                    begin
                         FieldByName( 'PE_YEAR' ).AsInteger := Year;
                         FieldByName( 'PE_TIPO' ).AsInteger := Ord( Tipo );
                         FieldByName( 'PE_NUMERO' ).AsInteger := Numero;
                         FieldByName( 'PE_FEC_INI' ).AsDateTime := Inicio;
                    end;
                    FieldByName( 'CM_CONNECT' ).AsString := K_GLOBAL_SI;

                    with FProcesos do
                    begin
                         FieldByName( 'PC_SI_QTY' ).AsInteger := CalculoIntegradosCuantos;
                         FieldByName( 'PC_SI_FEC' ).AsDateTime := CalculoIntegradosUltimo;
                         FieldByName( 'PC_AN_QTY' ).AsInteger := AfectarNominaCuantos;
                         FieldByName( 'PC_AN_FEC' ).AsDateTime := AfectarNominaUltimo;
                         FieldByName( 'PC_FOA_QTY' ).AsInteger := AjusteRetFonacotCuantos;
                         FieldByName( 'PC_FOA_FEC' ).AsDateTime:= AjusteRetFonacotUltimo;
                         FieldByName( 'PC_FON_QTY' ).AsInteger:= CalcularPagoFonacotCuantos;
                         FieldByName( 'PC_FON_FEC' ).AsDateTime:= CalcularPagoFonacotUltimo;
                         FieldByName( 'PC_LB_QTY' ).AsInteger := LabCalcularTiemposCuantos;
                         FieldByName( 'PC_LB_FEC' ).AsDateTime:= LabCalcularTiemposUltimo;
                         FieldByName( 'PC_FOC_QTY' ).AsInteger:= ConciliacionFonacotCuantos;
                         FieldByName( 'PC_FOC_FEC' ).AsDateTime:= ConciliacionFonacotUltimo;
                         FieldByName( 'PC_EM_QTY' ).AsInteger := ReportesEmailCuantos;
                         FieldByName( 'PC_EM_FEC' ).AsDateTime:= ReportesEmailUltimo;

                    end;

                    FieldByName( 'PC_NOMLAVG' ).AsFloat := fNominaDuracionAvgDias;
                    FieldByName( 'PC_NOM_AVG' ).AsFloat := fNominaDuracionAvgTotal;
                    FieldByName( 'COMPANYXMLDATA' ).AsString := sCompanyXMLData;
                    FieldByName( 'XMLINFOUSOTRESS' ).AsString := sXmlInfoUsoTress;
                    Post;
               end
               else
               begin
               end;
               Next;
          end;
          First;
     end;
end;

{* Regresar una tabla con datos de un proceso de actualización. @autor Ricardo Carrillo}
function TdmDBConfig.GetMotorPatchAvance(Parametros: TZetaParams): OleVariant;
var
   oQuery    : TZetaCursor;
   sAlias    : string;
   sServer   : string;
   sDatabase : string;
   sTop      : string;
   sWhere    : string;
   sSentencia: string;
begin
     Result := 0;
     oQuery := nil;
     with oZetaProvider, Parametros do
     try
        EmpresaActiva := Comparte;
        try
           sAlias := EmpresaActiva[P_ALIAS];
           ZetaServerTools.GetServerDatabase(sAlias, sServer, sDatabase);
           sSentencia := 'select %s PC_NUMERO, PC_MAXIMO, PC_PASO, PC_ERROR from PROCESO where %s';

           if ParamByName('DB_PROCESO').AsInteger = 0 then
           begin
                sTop := 'top 1';
                sWhere := 'PC_PARAMS = ' + Format(QuotedStr(MSG_CAMBIO_VERSION), [sDatabase, ParamByName('DB_VERSION').AsInteger, ParamByName('DB_APLICAVERSION').AsInteger]) + ' order by PC_FEC_INI + PC_HOR_INI desc';
           end
           else
           begin
                sTop := '';
                sWhere := 'PC_NUMERO = ' + ParamByName('DB_PROCESO').AsString;
           end;
           sSentencia := Format(sSentencia, [sTop, sWhere]);
           Result := OpenSQL(EmpresaActiva, sSentencia, True);
        except
              on e: Exception do
                raise Exception.Create( Format ('No es posible conectarse a: %s. Por favor, verifique su información. %s', [ParamByName('DB_DATOS').AsString, sLineBreak + e.Message]) );
        end;
     finally
            FreeAndNil(oQuery);
     end;
end;

{* Regresar una tabla con datos de bitácora de un proceso de actualización. @autor Ricardo Carrillo 2014-04-10}
function TdmDBConfig.GetBitacora(const iProceso: Integer): OleVariant;
begin
     with oZetaProvider do
     try
          EmpresaActiva := Comparte;
          Result := OpenSQL(EmpresaActiva, Format(K_BITACORA_PROCESO, [iProceso]), True);
     finally
            ;
     end;
end;


end.
