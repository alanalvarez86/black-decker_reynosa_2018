object ConfigurarConexion: TConfigurarConexion
  Left = 495
  Top = 338
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'Configurar conexi'#243'n'
  ClientHeight = 195
  ClientWidth = 391
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  DesignSize = (
    391
    195)
  PixelsPerInch = 96
  TextHeight = 13
  object cxListServers: TcxComboBox
    Left = 112
    Top = 19
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 0
    Width = 233
  end
  object cxLabel1: TcxLabel
    Left = 58
    Top = 20
    Caption = 'Servidor:'
    FocusControl = cxListServers
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxLabel2: TcxLabel
    Left = 62
    Top = 47
    Caption = 'Usuario:'
    FocusControl = cxUser
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxUser: TcxTextEdit
    Left = 112
    Top = 46
    ParentFont = False
    Properties.MaxLength = 100
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 1
    Width = 233
  end
  object cxPassword: TcxTextEdit
    Left = 112
    Top = 73
    ParentFont = False
    Properties.EchoMode = eemPassword
    Properties.MaxLength = 100
    Properties.PasswordChar = '*'
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 2
    OnExit = cxPasswordExit
    Width = 233
  end
  object cxLabel3: TcxLabel
    Left = 71
    Top = 74
    Caption = 'Clave:'
    FocusControl = cxPassword
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxLabel4: TcxLabel
    Left = 29
    Top = 101
    Caption = 'Base de Datos:'
    FocusControl = cxBaseDatos
    ParentColor = False
    ParentFont = False
    Style.Color = clWhite
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
  end
  object cxBaseDatos: TcxComboBox
    Left = 112
    Top = 100
    ParentFont = False
    Properties.OnInitPopup = cxBaseDatosPropertiesInitPopup
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 3
    Width = 233
  end
  object cxOK: TcxButton
    Left = 194
    Top = 161
    Width = 80
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&OK'
    ModalResult = 1
    OptionsImage.ImageIndex = 12
    OptionsImage.Images = FConfiguradorTRESS.ImageButtons
    OptionsImage.Margin = 1
    OptionsImage.Spacing = 16
    TabOrder = 8
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = cxOKClick
  end
  object cxCancelar: TcxButton
    Left = 280
    Top = 161
    Width = 86
    Height = 26
    Anchors = [akRight, akBottom]
    Caption = '&Cancelar'
    ModalResult = 2
    OptionsImage.ImageIndex = 14
    OptionsImage.Images = FConfiguradorTRESS.ImageButtons
    OptionsImage.Margin = 1
    TabOrder = 9
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    OnClick = cxCancelarClick
  end
end
