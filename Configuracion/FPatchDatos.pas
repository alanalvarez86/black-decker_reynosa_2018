unit FPatchDatos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, DbTables, Mask,
     FMigrar,
     ZetaWizard,
     ZetaNumero, jpeg;

type
  TPatchDatos = class(TMigrar)
    ArchivoGB: TGroupBox;
    ConfigFileLBL: TLabel;
    ConfigFileSeek: TSpeedButton;
    ConfigFile: TEdit;
    OpenDialog: TOpenDialog;
    Parametros: TTabSheet;
    ParametrosGB: TGroupBox;
    VersionLBL: TLabel;
    Version: TComboBox;
    PatchTriggers: TCheckBox;
    PatchSP: TCheckBox;
    PatchDB: TCheckBox;
    RangoScriptsGB: TGroupBox;
    ScriptInicialLBL: TLabel;
    ScriptFinalLBL: TLabel;
    ScriptInicial: TZetaNumero;
    ScriptFinal: TZetaNumero;
    ScriptsTodos: TRadioButton;
    ScriptsRango: TRadioButton;
    Label1: TLabel;
    VerActual: TEdit;
    UpDown: TUpDown;
    RecalcularKardex: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ConfigFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure PatchDBClick(Sender: TObject);
    procedure VersionChange(Sender: TObject);
  private
    { Private declarations }
    function GetConfigFile: String;
    function GetVersion: String;
    procedure SetConfigFile( const sValue: String );
  protected
    { Protected declarations }
    procedure SetControls; override;
  public
    { Public declarations }
    function Init: Boolean;
  end;

var
  PatchDatos: TPatchDatos;

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaTressCFGTools,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TPatchDatos.FormCreate(Sender: TObject);
begin
     Version.ItemIndex := 0;
     dmMigracion := TdmReportes.Create( Self );
     ScriptInicial.Valor := 1;
     ScriptFinal.Valor := 999;
     inherited;
     HelpContext := H00007_Actualizar_base_de_datos;
end;

procedure TPatchDatos.FormShow(Sender: TObject);
const
     {$ifdef INTERBASE}
     //K_PATCH_FILE = 'SP_%s';
     K_PATCH_FILE = '%sSP_%s';
     {$endif}
     {$ifdef MSSQL}
     //K_PATCH_FILE = 'SP_MSSQL_%s';
     K_PATCH_FILE = '%sSP_MSSQL_%s';
     {$endif}
var
   sTipo: String;
   sPath: String;
   lEnabled: Boolean;
begin
     inherited;
     case CompanyType of
          tcRecluta:
          begin
               lEnabled := False;
               sTipo := 'Seleccion';
               sPath := 'Seleccion\Patch\';
               Caption := 'Actualizar Base de Datos De Selecci�n';
          end;
          tcVisitas:
          begin
               lEnabled := False;
               sTipo := 'Visitantes';
               sPath := 'Visitas\Patch\';
               Caption := 'Actualizar Base de Datos De Visitantes';
          end;
     else
         begin
              lEnabled := True;
              sTipo := 'Datos';
              sPath := 'Fuentes\Patch\';
              Caption := 'Actualizar Base de Datos de una Empresa';
         end;
     end;
     with RecalcularKardex do
     begin
          Checked := False;
          Enabled := lEnabled;
          Visible := lEnabled;
     end;
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( Format( 'Patch%s.log', [ sTipo ] ) );
     //ConfigFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( Format( 'Fuentes\Patch\%s.db', [ Format( K_PATCH_FILE, [ sTipo ] ) ] ) );
     ConfigFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( Format( '%s.db', [ Format( K_PATCH_FILE, [ sPath, sTipo ] ) ] ) );
     SetControls;
end;

procedure TPatchDatos.FormDestroy(Sender: TObject);
begin
     TdmReportes( dmMigracion ).Free;
     inherited;
end;

function TPatchDatos.Init: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             CompanyType := Self.CompanyType;
             AbreBDReceptora;
             with Self.UpDown do
             begin
                  Position:= StrToIntDef( GetVerActual, 300 );
                  Enabled:= not Existe_VA;
             end;
             Self.VerActual.Enabled:= not Existe_VA;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Base De Datos Destino !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

function TPatchDatos.GetConfigFile: String;
begin
     Result := ConfigFile.Text;
end;

function TPatchDatos.GetVersion: String;
begin
     with Version do
     begin
          Result := Items.Strings[ ItemIndex ];
     end;
end;

procedure TPatchDatos.SetConfigFile( const sValue: String );
begin
     ConfigFile.Text := sValue
end;

procedure TPatchDatos.ConfigFileSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          FileName := GetConfigFile;
          InitialDir := ExtractFileDir( FileName );
          if Execute then
             SetConfigFile( FileName );
     end;
end;

procedure TPatchDatos.SetControls;
var
   lEnabled: Boolean;
begin
     lEnabled := PatchDB.Checked;
     RangoScriptsGB.Enabled := lEnabled;
     ScriptsTodos.Enabled := lEnabled;
     ScriptsRango.Enabled := lEnabled;
     lEnabled := lEnabled and ScriptsRango.Checked;
     ScriptInicialLBL.Enabled := lEnabled;
     ScriptInicial.Enabled := lEnabled;
     ScriptFinalLBL.Enabled := lEnabled;
     ScriptFinal.Enabled := lEnabled;
end;

procedure TPatchDatos.VersionChange(Sender: TObject);
var
   lEnabled: Boolean;
begin
     inherited;
     lEnabled := ( GetVersion = '385' );
     with RecalcularKardex do
     begin
          Enabled := lEnabled;
          if not lEnabled then
             Checked := False;
     end;
end;

procedure TPatchDatos.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if Wizard.EsPaginaActual( DirectorioDOS ) then
                  begin
                       if not FileExists( GetConfigFile ) then
                       begin
                            zWarning( Caption, 'Archivo de Scripts No Existe', 0, mbOK );
                            ActiveControl := ConfigFile;
                            CanMove := False;
                       end
                       else
                       begin
                            with TdmReportes( dmMigracion ) do
                            begin
                                 SourceSharedPath := ExtractFileDir( GetConfigFile );
                                 try
                                    if AbreArchivoParadox( ExtractFileName( GetConfigFile ) ) then
                                    begin
                                         if GetVersionList( Version.Items, ExtractFileName( GetConfigFile ) ) then
                                         begin
                                              Version.ItemIndex := 0;
                                              CanMove := True;
                                         end
                                         else
                                         begin
                                              zWarning( Caption, 'Archivo de Scripts Es Inv�lido', 0, mbOK );
                                              ActiveControl := ConfigFile;
                                              CanMove := False;
                                         end;
                                    end;
                                 except
                                       on Error: Exception do
                                       begin
                                            CanMove := False;
                                            ShowError( '� Error En Archivo de Scripts !', Error );
                                       end;
                                 end;
                            end;
                       end;
                  end;
                  if Wizard.EsPaginaActual( Parametros ) then
                  begin
                       if ( StrToIntDef( Self.VerActual.Text, 0 ) < 200 ) then
                       begin
                            zWarning( Caption, 'Error en N�mero de Versi�n, Valores Permitidos [ 200...999 ]', 0, mbOK );
                            ActiveControl := VerActual;
                            CanMove := False;
                       end
                  end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TPatchDatos.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);

function GetModoPatchEstructuras: TModoPatch;
begin
     with Result do
     begin
          Habilitado := PatchDB.Checked;
          if ScriptsTodos.Checked then
             Filtro := esTodos
          else
              if ScriptsRango.Checked then
                 Filtro := esRango
              else
                  Filtro := esLista;
          Inicial := ScriptInicial.ValorEntero;
          Final := ScriptFinal.ValorEntero;
          Lista := '';
     end;
end;

begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          InitCounter( GetPatchSteps );
          StartProcess;
          EnableStopOnError;
          lOk := ActualizarDatos( ExtractFileName( GetConfigFile ),
                                  Self.VerActual.Text,
                                  GetVersion,
                                  PatchTriggers.Checked,
                                  PatchSP.Checked,
                                  RecalcularKardex.Checked, False, False, 
                                  GetModoPatchEstructuras );
          EndProcess;
     end;
     EndLog;
     inherited;
end;

procedure TPatchDatos.PatchDBClick(Sender: TObject);
begin
     inherited;
     SetControls;
end;

end.
