unit FLicenciaUsuario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal, StdCtrls, ComCtrls, Buttons, ExtCtrls, ShellApi, Registry;

type
  TLicenciaUsuario = class(TZetaDlgModal)
    redLicencia: TRichEdit;
    SaveDialog: TSaveDialog;
    PanelUsuario: TPanel;
    gbusuario: TGroupBox;
    lblUsuario: TLabel;
    lblPlataforma: TLabel;
    lblFecha: TLabel;
    Usuariolbl: TLabel;
    Fechalbl: TLabel;
    Plataformalbl: TLabel;
    procedure OKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    function ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
    procedure GetInfoLicencia;
  public
    { Public declarations }
  end;
  TLicenciaRegistry = class(TObject)
  private
    { Private declarations }
    FRegistry: TRegIniFile;
    function GetFecha: string;
    function GetInstalacion: string;
    function GetPlataforma: String;
    function GetUsuario: String;
    function GetSeccion: string;
  protected
    { Protected declarations }
    property Registry: TRegIniFile read FRegistry;
  public
    { Public declarations }
    constructor Create; virtual;
    destructor Destroy; override;
    property Usuario: string read GetUsuario;
    property Plataforma: string read GetPlataforma;
    property Fecha: string read GetFecha;
    property Instalacion: string read GetInstalacion;
  end;


var
   LicenciaUsuario: TLicenciaUsuario;
   LicenciaRegistry: TLicenciaRegistry;

implementation

uses ZetaDialogo,
     ZetaCommonClasses;

{$R *.DFM}

const
     { ***** Contantes del Registry ***** }
     LICENCIA_REGISTRY_PATH   = 'Software\Grupo Tress\TressWin\Licencia';
     LICENCIA_USUARIO         = 'Usuario';
     LICENCIA_PLATAFORMA      = 'Plataforma';
     LICENCIA_FECHA           = 'Fecha';
     LICENCIA_INSTALACION     = 'Instalacion';
     LICENCIA_PROFESIONAL     = 'Profesional';
     LICENCIA_SERVIDOR        = 'Servidor';

     { ***** Cosntantes Generales ***** }
     K_NINGUNO                = '< Vacio >';

procedure TLicenciaUsuario.FormCreate(Sender: TObject);
begin
     inherited;
     LicenciaRegistry := TLicenciaRegistry.Create;
end;

procedure TLicenciaUsuario.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( LicenciaRegistry );
end;

procedure TLicenciaUsuario.FormShow(Sender: TObject);
begin
     inherited;
     GetInfoLicencia;
end;

procedure TLicenciaUsuario.OKClick(Sender: TObject);
var
   sArchivo: String;
begin
     inherited;
     with SaveDialog do
     begin
          if Execute then
          begin
               sArchivo := FileName;
               redLicencia.Lines.SaveToFile( sArchivo );
               if ZetaDialogo.ZConfirm( 'Informaci�n', 'La Licencia de Usuario se '+
                                     'Guard� en el Archivo:'+ CR_LF + CR_LF + '"'+
                                     sArchivo + '"' + CR_LF + CR_LF +
                                     '�Desea abrirlo? ', 0, mbOk ) then
               begin
                    ExecuteFile( ExtractFileName( sArchivo ), '', ExtractFilePath( sArchivo ) );
               end;
          end;
          ModalResult := mrNone;
     end;
end;

function TLicenciaUsuario.ExecuteFile( const FileName, Params, DefaultDir: String ): THandle;
var
   zFileName, zParams, zDir: array[ 0..79 ] of Char;
begin
     Result := ShellApi.ShellExecute( Application.MainForm.Handle,
                                      nil,
                                      StrPCopy( zFileName, FileName ),
                                      StrPCopy( zParams, Params ),
                                      StrPCopy( zDir, DefaultDir ),
                                      SW_SHOWDEFAULT );
end;

procedure TLicenciaUsuario.GetInfoLicencia;
begin
     with LicenciaRegistry do
     begin
          {$ifdef INTERBASE}
          if FRegistry.KeyExists( LICENCIA_SERVIDOR ) then
             Plataformalbl.Caption := Plataforma
          else
          begin
               lblPlataforma.Caption := 'Instalaci�n:';
               Plataformalbl.Caption := Instalacion;
          end;
          {$endif}
          {$ifdef MSSQL}
          Plataformalbl.Caption := Plataforma;
          {$endif}
          Usuariolbl.Caption := Usuario;
          Fechalbl.Caption := Fecha;
     end;
end;

{ TLicenciaRegistry }

constructor TLicenciaRegistry.Create;
begin
     FRegistry := TRegIniFile.Create;
     with FRegistry do
     begin
          RootKey := HKEY_LOCAL_MACHINE;
          OpenKey( LICENCIA_REGISTRY_PATH, TRUE );
          LazyWrite := False;
     end;
end;

destructor TLicenciaRegistry.Destroy;
begin
     inherited;
     FreeAndNil( FRegistry );
end;

function TLicenciaRegistry.GetFecha;
begin
     Result := FRegistry.ReadString( GetSeccion, LICENCIA_FECHA, K_NINGUNO );
end;

function TLicenciaRegistry.GetInstalacion;
begin
     Result := FRegistry.ReadString( GetSeccion, LICENCIA_INSTALACION, K_NINGUNO );
end;

function TLicenciaRegistry.GetPlataforma;
begin
     Result := FRegistry.ReadString( GetSeccion, LICENCIA_PLATAFORMA, K_NINGUNO );
end;

function TLicenciaRegistry.GetUsuario;
begin
     Result := FRegistry.ReadString( GetSeccion, LICENCIA_USUARIO, K_NINGUNO );
end;

function TLicenciaRegistry.GetSeccion: string;
begin
     {$ifdef INTERBASE}
     if ( FRegistry.KeyExists( LICENCIA_SERVIDOR ) ) then
          Result := LICENCIA_SERVIDOR
     else
          Result := LICENCIA_PROFESIONAL;
     {$endif}
     {$ifdef MSSQL}
     Result := LICENCIA_SERVIDOR;
     {$endif}

end;

end.
