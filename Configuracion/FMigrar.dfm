object Migrar: TMigrar
  Left = 224
  Top = 109
  AutoScroll = False
  Caption = 'Migración de Datos De DOS hacia SQL'
  ClientHeight = 269
  ClientWidth = 392
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Wizard: TZetaWizard
    Left = 0
    Top = 217
    Width = 392
    Height = 33
    Align = alBottom
    TabOrder = 0
    AlEjecutar = WizardAlEjecutar
    AlCancelar = WizardAlCancelar
    PageControl = PageControl
    Reejecutar = False
    object Anterior: TZetaWizardButton
      Left = 8
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Paso Anterior'
      Caption = '&Anterior'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Tipo = bwAnterior
      Wizard = Wizard
    end
    object Siguiente: TZetaWizardButton
      Left = 87
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Siguiente Paso'
      Caption = '&Siguiente'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333FF3333333333333003333
        3333333333773FF3333333333309003333333333337F773FF333333333099900
        33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
        99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
        33333333337F3F77333333333309003333333333337F77333333333333003333
        3333333333773333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Tipo = bwSiguiente
      Wizard = Wizard
    end
    object Migrar: TZetaWizardButton
      Left = 227
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Efectuar Migración'
      Caption = '&Migrar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333330000333333333333333333333333F33333333333
        00003333344333333333333333388F3333333333000033334224333333333333
        338338F3333333330000333422224333333333333833338F3333333300003342
        222224333333333383333338F3333333000034222A22224333333338F338F333
        8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
        33333338F83338F338F33333000033A33333A222433333338333338F338F3333
        0000333333333A222433333333333338F338F33300003333333333A222433333
        333333338F338F33000033333333333A222433333333333338F338F300003333
        33333333A222433333333333338F338F00003333333333333A22433333333333
        3338F38F000033333333333333A223333333333333338F830000333333333333
        333A333333333333333338330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      Tipo = bwEjecutar
      Wizard = Wizard
    end
    object Salir: TZetaWizardButton
      Left = 307
      Top = 4
      Width = 75
      Height = 25
      Hint = 'Abandonar Proceso'
      Caption = '&Cancelar'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Kind = bkCancel
      Tipo = bwCancelar
      Wizard = Wizard
    end
  end
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 392
    Height = 217
    ActivePage = Migracion
    Align = alClient
    TabOrder = 1
    object Inicio: TTabSheet
      Caption = 'Inicio'
      TabVisible = False
      object Imagen: TImage
        Left = 0
        Top = 0
        Width = 161
        Height = 207
        Align = alLeft
        Center = True
        Stretch = True
      end
      object MensajeInicial: TMemo
        Left = 161
        Top = 0
        Width = 223
        Height = 207
        Align = alClient
        Alignment = taCenter
        BorderStyle = bsNone
        Color = clBtnFace
        Lines.Strings = (
          ''
          'Este proceso transfiere la información'
          'de la base de datos de'
          'Tress DOS'
          'hacia la base de datos de'
          'Tress Windows'
          ''
          'La base de datos de Tress DOS'
          'usa archivos dBase III '
          ''
          'La base de datos Tress Windows'
          'usará el servidor de SQL escogido'
          'para esta instalación')
        TabOrder = 0
      end
    end
    object DirectorioDOS: TTabSheet
      Caption = 'DirectorioDOS'
      TabVisible = False
      object LogFileGB: TGroupBox
        Left = 0
        Top = 0
        Width = 384
        Height = 65
        Align = alTop
        Caption = ' Bitácora del Proceso '
        TabOrder = 0
        object LogFileSeek: TSpeedButton
          Left = 355
          Top = 36
          Width = 25
          Height = 25
          Hint = 'Buscar Directorio'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
            300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
            330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
            333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
            339977FF777777773377000BFB03333333337773FF733333333F333000333333
            3300333777333333337733333333333333003333333333333377333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = LogFileSeekClick
        end
        object LogFileLBL: TLabel
          Left = 61
          Top = 41
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Arc&hivo:'
          FocusControl = LogFile
        end
        object LogFile: TEdit
          Left = 104
          Top = 37
          Width = 246
          Height = 21
          TabOrder = 0
          Text = 'Bitacora.log'
        end
        object LogFileWrite: TCheckBox
          Left = 15
          Top = 16
          Width = 102
          Height = 17
          Alignment = taLeftJustify
          Caption = '&Guardar Bitácora:'
          Checked = True
          State = cbChecked
          TabOrder = 1
          OnClick = LogFileWriteClick
        end
      end
    end
    object Migracion: TTabSheet
      Caption = 'Migracion'
      TabVisible = False
      object PanelLog: TPanel
        Left = 0
        Top = 0
        Width = 384
        Height = 207
        Align = alClient
        TabOrder = 0
        object ProcesoGB: TGroupBox
          Left = 1
          Top = 1
          Width = 382
          Height = 37
          Align = alTop
          Caption = ' Avance del Proceso '
          TabOrder = 0
          object AvanceProceso: TProgressBar
            Left = 2
            Top = 13
            Width = 378
            Height = 22
            Align = alBottom
            Min = 0
            Max = 100
            Step = 1
            TabOrder = 0
          end
        end
        object LogDisplay: TListBox
          Left = 1
          Top = 38
          Width = 382
          Height = 168
          Align = alClient
          ExtendedSelect = False
          ItemHeight = 13
          TabOrder = 1
        end
      end
    end
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 250
    Width = 392
    Height = 19
    Panels = <
      item
        Width = 125
      end
      item
        Width = 125
      end
      item
        Width = 125
      end>
    SimplePanel = False
  end
  object LogFileDialog: TSaveDialog
    DefaultExt = 'log'
    Filter = 'Log Files|*.log|Text Files|*.txt|All Files|*.*'
    Left = 176
    Top = 283
  end
end
