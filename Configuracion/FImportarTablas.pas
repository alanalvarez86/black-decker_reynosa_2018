unit FImportarTablas;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, IniFiles,
     Forms, Dialogs, ComCtrls, StdCtrls, Buttons, ExtCtrls, DbTables,
     FMigrar,
     ZetaWizard,
     ZetaSmartLists, jpeg;

type
  eImportMode = ( imParadox, imAsciiCSV );
  TImportarTablas = class(TMigrar)
    OpenDialog: TOpenDialog;
    PorParadox: TTabSheet;
    ImportMode: TRadioGroup;
    PanelParadox: TPanel;
    ImportFileLBL: TLabel;
    ImportFile: TEdit;
    ImportFileSeek: TSpeedButton;
    ModoGB: TRadioGroup;
    PanelTabla: TPanel;
    PorAsciiCSV: TTabSheet;
    PanelArchivoASCII: TPanel;
    ArchivoASCIIlbl: TLabel;
    ArchivoASCIISeek: TSpeedButton;
    ArchivoASCII: TEdit;
    TablaDestinoGB: TGroupBox;
    TablaDestino: TListBox;
    PanelCampos: TPanel;
    ImportablesGB: TGroupBox;
    PanelBotoncitos: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    ZetaSmartLists: TZetaSmartLists;
    CamposGB: TGroupBox;
    Campos: TZetaSmartListBox;
    Importables: TZetaSmartListBox;
    Rechazar: TZetaSmartListsButton;
    Escoger: TZetaSmartListsButton;
    Arriba: TZetaSmartListsButton;
    Abajo: TZetaSmartListsButton;
    FormatoFechasLBL: TLabel;
    FormatoFechas: TComboBox;
    SoloValidar: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ImportFileSeekClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure WizardAfterMove(Sender: TObject);
    procedure WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
    procedure ArchivoASCIISeekClick(Sender: TObject);
  private
    { Private declarations }
    FTablas: TStrings;
    FCampos: TStrings;
    FImportables: TStrings;
    function CargaCampos: Boolean;
    function GetAsciiFile: String;
    function GetImportFile: String;
    function GetImportMode: eImportMode;
    function GetImportTableName: String;
    function GetImportTableIndex: Integer;
    function ImportFormatFileName: String;
    function Init: Boolean;
    procedure ImportFormatLoad(Lista: TStrings);
    procedure ImportFormatSave;
    procedure SetImportFile( const sValue: String );
    procedure SetAsciiFile( const sValue: String );
    procedure LoadFieldLists;
    procedure LoadImportFieldList;
  public
    { Public declarations }
  end;

var
  ImportarTablas: TImportarTablas;

implementation

uses ZetaMigrar,
     ZetaDialogo,
     ZetaTressCFGTools,
     ZetaCommonTools,
     FHelpContext,
     DReportes;

{$R *.DFM}

procedure TImportarTablas.FormCreate(Sender: TObject);
begin
     FTablas := TStringList.Create;
     FCampos := TStringList.Create;
     FImportables := TStringList.Create;
     LogFile.Text := ZetaTressCFGTools.SetFileNameDefaultPath( 'ImportarTablas.log' );
     SetImportFile( '' );
     SetAsciiFile( ZetaTressCFGTools.SetFileNameDefaultPath( 'Datos.csv' ) );
     dmMigracion := TdmReportes.Create( Self );
     inherited;
     HelpContext := H00022_Importando_tablas;
end;

procedure TImportarTablas.FormShow(Sender: TObject);
var
   eFormato: eFormatoFecha;
begin
     inherited;
     Init;
     with FormatoFechas do
     begin
          with Items do
          begin
               BeginUpdate;
               try
                  Clear;
                  for eFormato := Low( eFormatoFecha ) to High( eFormatoFecha ) do
                  begin
                       Add( aFormatoFecha[ eFormato ] );
                  end;
               finally
                      EndUpdate;
               end;
          end;
          ItemIndex := 0;
     end;
end;

procedure TImportarTablas.FormDestroy(Sender: TObject);
begin
     TdmReportes( dmMigracion ).Free;
     FreeAndNil( FImportables );
     FreeAndNil( FCampos );
     FreeAndNil( FTablas );
     inherited;
end;

function TImportarTablas.Init: Boolean;
var
   oCursor: TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        StartProcess;
        with TdmReportes( dmMigracion ) do
        begin
             GetDescripcionTablas( FTablas );
        end;
        with TablaDestino.Items do
        begin
             BeginUpdate;
             try
                Clear;
                for i := 0 to ( FTablas.Count - 1 ) do
                begin
                     Add( FTablas.Values[ FTablas.Names[ i ] ] );
                end;
             finally
                    EndUpdate;
             end;
        end;
        Result := True;
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Abrir Base De Datos Destino !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

function TImportarTablas.CargaCampos: Boolean;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TdmReportes( dmMigracion ) do
        begin
             GetDescripcionCampos( GetImportTableName, GetImportTableIndex, FCampos );
        end;
        Result := ( FCampos.Count > 0 );
     except
           on Error: Exception do
           begin
                ShowError( '� Error Al Leer Lista De Campos !', Error );
                Result := False;
           end;
     end;
     Screen.Cursor := oCursor;
end;

procedure TImportarTablas.LoadFieldLists;
var
   oCursor: TCursor;
   i, iPtr: Integer;
   sCampo: String;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        ImportFormatLoad( FImportables );
        with Campos.Items do
        begin
             BeginUpdate;
             try
                Clear;
                for i := 0 to ( FCampos.Count - 1 ) do
                begin
                     sCampo := FCampos.Names[ i ];
                     if ( FImportables.IndexOf( sCampo ) < 0 ) then
                        AddObject( FCampos.Values[ sCampo ], TObject( i ) );
                end;
             finally
                    EndUpdate;
             end;
        end;
        with Importables.Items do
        begin
             BeginUpdate;
             try
                Clear;
                for i := 0 to ( FImportables.Count - 1 ) do
                begin
                     sCampo := FImportables.Strings[ i ];
                     iPtr := FCampos.IndexOfName( sCampo );
                     if ( iPtr >= 0 ) then
                        AddObject( FCampos.Values[ sCampo ], TObject( iPtr ) );
                end;
             finally
                    EndUpdate;
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TImportarTablas.LoadImportFieldList;
var
   oCursor: TCursor;
   i: Integer;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        FImportables.Clear;
        with Importables.Items do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  FImportables.Add( FCampos.Names[ Integer( Objects[ i ] ) ] );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

function TImportarTablas.GetImportFile: String;
begin
     Result := ImportFile.Text;
end;

function TImportarTablas.GetAsciiFile: String;
begin
     Result := ArchivoASCII.Text;
end;

function TImportarTablas.GetImportTableName: String;
begin
     with TablaDestino do
     begin
          if ( ItemIndex >= 0 ) then
             Result := FTablas.Names[ ItemIndex ]
          else
              Result := '';
     end;
end;

function TImportarTablas.GetImportTableIndex: Integer;
begin
     with TablaDestino do
     begin
          if ( ItemIndex >= 0 ) then
             Result := Integer( FTablas.Objects[ ItemIndex ] )
          else
              Result := 0;
     end;
end;

function TImportarTablas.GetImportMode: eImportMode;
begin
     Result := eImportMode( ModoGB.ItemIndex );
end;

procedure TImportarTablas.SetImportFile( const sValue: String );
begin
     ImportFile.Text := sValue
end;

procedure TImportarTablas.SetAsciiFile( const sValue: String );
begin
     ArchivoASCII.Text := sValue;
end;

function TImportarTablas.ImportFormatFileName: String;
begin
     Result := ZetaTressCFGTools.SetFileNameDefaultPath( 'ImportarTablas.ini' );
end;

procedure TImportarTablas.ImportFormatLoad( Lista: TStrings );
var
   oIniFile: TIniFile;
begin
     oIniFile := TIniFile.Create( ImportFormatFileName );
     try
        with oIniFile do
        begin
             ReadSection( GetImportTableName, Lista );
        end;
     finally
            FreeAndNil( oIniFile );
     end;
end;

procedure TImportarTablas.ImportFormatSave;
var
   i: Integer;
   oIniFile: TIniFile;
   sSection: String;
begin
     sSection := GetImportTableName;
     oIniFile := TIniFile.Create( ImportFormatFileName );
     try
        with oIniFile do
        begin
             EraseSection( sSection );
             with FImportables do
             begin
                  for i := 0 to ( Count - 1 ) do
                  begin
                       WriteString( sSection, Strings[ i ], '' )
                  end;
             end;
        end;
     finally
            FreeAndNil( oIniFile );
     end;
end;

procedure TImportarTablas.ImportFileSeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          DefaultExt := 'db';
          Filter := 'Paradox (*.db)|*.db|Todos los Archivos (*.*)|*.*';
          Title := 'Escoja El Archivo A Importar';
          FileName := GetImportFile;
          InitialDir := ExtractFileDir( ImportFile.Text );
          if Execute then
             SetImportFile( FileName );
     end;
end;

procedure TImportarTablas.ArchivoASCIISeekClick(Sender: TObject);
begin
     inherited;
     with OpenDialog do
     begin
          DefaultExt := 'csv';
          Filter := 'ASCII - Separados Por Comas (*.csv)|*.csv|Todos los Archivos (*.*)|*.*';
          Title := 'Escoja El Archivo ASCII - CSV A Importar';
          FileName := GetAsciiFile;
          InitialDir := ExtractFileDir( GetAsciiFile );
          if Execute then
             SetAsciiFile( FileName );
     end;
end;

procedure TImportarTablas.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   oCursor: TCursor;
begin
     inherited;
     if CanMove then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             if Wizard.Adelante then
             begin
                  if Wizard.EsPaginaActual( DirectorioDOS ) then
                  begin
                       case GetImportMode of
                            imParadox:
                            begin
                                 if TablaDestino.ItemIndex < 0 then
                                 begin
                                      Zetadialogo.ZError( 'Importar Tablas', 'Debe Seleccionar La Tabla Destino', 0 );
                                      CanMove := False;
                                 end
                                 else
                                     iNewPage := PorParadox.PageIndex;
                            end;
                            imAsciiCSV:
                            begin
                                 if TablaDestino.ItemIndex < 0 then
                                 begin
                                      Zetadialogo.ZError( 'Importar Tablas', 'Debe Seleccionar La Tabla Destino', 0 );
                                      CanMove := False;
                                 end
                                 else
                                 begin
                                      if CargaCampos then
                                      begin
                                           LoadFieldLists;
                                           iNewPage := PorAsciiCSV.PageIndex;
                                      end
                                      else
                                          CanMove := False;
                                 end;
                            end;
                       end;
                  end
                  else
                      if Wizard.EsPaginaActual( PorParadox ) then
                      begin
                           if not FileExists( GetImportFile ) then
                           begin
                                ZetaDialogo.zWarning( Caption, 'Archivo A Importar No Existe', 0, mbOK );
                                ActiveControl := ImportFile;
                                CanMove := False;
                           end
                           else
                           begin
                                with TdmReportes( dmMigracion ) do
                                begin
                                     SourceSharedPath := ExtractFileDir( GetImportFile );
                                     try
                                        CanMove := AbreArchivoParadox( ExtractFileName( GetImportFile ) );
                                     except
                                           on Error: Exception do
                                           begin
                                                CanMove := False;
                                                ShowError( '� Error En Archivo A Importar !', Error );
                                           end;
                                     end;
                                end;
                                if CanMove then
                                   iNewPage := Migracion.PageIndex;
                           end;
                      end
                      else
                          if Wizard.EsPaginaActual( PorAsciiCSV ) then
                          begin
                               if FileExists( GetAsciiFile ) then
                               begin
                                    if ( Importables.Items.Count <= 0 ) then
                                    begin
                                         ZetaDialogo.zWarning( Caption, 'No Se Ha Escogido Ning�n Campo Por Importar', 0, mbOK );
                                         ActiveControl := Campos;
                                         CanMove := False;
                                    end;
                               end
                               else
                               begin
                                    ZetaDialogo.zWarning( Caption, 'Archivo ASCII A Importar No Existe', 0, mbOK );
                                    ActiveControl := ArchivoASCII;
                                    CanMove := False;
                               end;
                          end;
             end
             else
             begin
                  if Wizard.EsPaginaActual( PorParadox ) then
                     iNewPage := DirectorioDOS.PageIndex
                  else
                      if Wizard.EsPaginaActual( PorAsciiCSV ) then
                         iNewPage := DirectorioDOS.PageIndex
                      else
                          if Wizard.EsPaginaActual( Migracion ) then
                          begin
                               case GetImportMode of
                                    imParadox: iNewPage := PorParadox.PageIndex;
                                    imAsciiCSV: iNewPage := PorAsciiCSV.PageIndex;
                               end;
                          end;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TImportarTablas.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual( PorParadox ) then
     begin
          if ZetaCommonTools.StrVacio( GetImportFile ) then
             SetImportFile( ZetaTressCFGTools.SetFileNameDefaultPath( Format( '%s.DB', [ GetImportTableName ] ) ) );
     end;
end;

procedure TImportarTablas.WizardAlEjecutar(Sender: TObject; var lOk: Boolean);
var
   iTop: Integer;
begin
     StartLog;
     with TdmReportes( dmMigracion ) do
     begin
          case GetImportMode of
               imParadox: iTop := GetImportTableSteps;
               imAsciiCSV:
               begin
                    LoadImportFieldList;
                    with TdmReportes( dmMigracion ) do
                    begin
                         iTop := ImportarASCIIInit( GetAsciiFile );
                    end;
               end;
          else
              iTop := 0;
          end;
          if ( iTop > 0 ) then
          begin
               InitCounter( iTop );
               StartProcess;
               EnableStopOnError;
               case GetImportMode of
                    imParadox: lOk := ImportarTabla( GetImportTableName, TBatchMode( ImportMode.ItemIndex ) );
                    imAsciiCSV:
                    begin
                         ImportFormatSave;
                         lOk := ImportarASCII( SoloValidar.Checked, GetImportTableName, GetAsciiFile, FImportables, eFormatoFecha( FormatoFechas.ItemIndex ) );
                    end;
               end;
               StopCounter;
               EndProcess;
          end
          else
          begin
               lOk := False;
               ZetaDialogo.zError( Caption, 'No Hay Datos Por Importar', 0 );
          end;
     end;
     EndLog;
     inherited;
end;

end.
