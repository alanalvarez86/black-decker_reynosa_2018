program Actualizador;

uses
  Forms,
  FFixes in 'FFixes.pas' {Upgrader},
  FAyuda in 'FAyuda.pas' {Ayuda},
  ZBaseDlgModal in '..\..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Actualizador de Tress 1.3';
  Application.CreateForm(TUpgrader, Upgrader);
  Application.Run;
end.
