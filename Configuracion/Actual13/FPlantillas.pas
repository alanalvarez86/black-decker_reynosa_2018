unit FPlantillas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, QRDesign,
  Qrdctrls,
  QrExport,
  Qrprntr,
  QRDLDR,
  Qrctrls,
  qrdProcs,
  StdCtrls, Db, DBTables ;

type
  TPlantillas = class(TForm)
    FDesigner: TQRepDesigner;
    Reporte: TDesignQuickReport;
  private
    FDirectorio: string;
    FBackupDir: string;
    procedure SetDirectorio(const Value: string);
    procedure SetBackupDir(const Value: string);
    { Private declarations }
  public
    { Public declarations }
    property Directorio : string read FDirectorio write SetDirectorio;
    property BackupDir : string read FBackupDir write SetBackupDir;
    function CargaPlantilla( sArchivo : string ): string;
  end;

var
  Plantillas: TPlantillas;

implementation
uses ZetaCommonTools;

{$R *.DFM}

function TPlantillas.CargaPlantilla( sArchivo: string): string;
 var sFile, sFileBak : string;
begin
     Result := '';
     SysUtils.CreateDir(FDirectorio+FBackupDir);
     if ( Trim(sArchivo) > '' ) then
     begin
          if ExtractFileDir(sArchivo) = '' then
             sArchivo := FDirectorio + sArchivo;

          if ExtractFileExt(sArchivo) = '' then
             sArchivo := sArchivo + '.QR2';

          sFile := ExtractFileName(sArchivo);

          if FileExists(sArchivo) then
          begin
               qrdProcs.ClearReportForm( Self, TRUE );
               if FDesigner.LoadReport( sArchivo ) then
               begin
                    //Bitacora.Items.Add('Cargando Archivo: ' + sFile);
                    if NOT Reporte.Bands.HasDetail then
                    begin
                         sFileBak := FDirectorio+FBackupDir+sFile;
                         FDesigner.SaveReport(sFileBak);
                         with TQRDesignBand.Create( self ) do
                         begin
                              Parent := Reporte;
                              BandType := rbDetail;
                              Height := 10;
                         end;
                         FDesigner.SaveReport( sArchivo );
                         Result := 'La Plantilla ' +sFile+ ' fu� Modificada';
                    end;
               end
               else Result := 'Error al Cargar la Plantilla: ' + sFile;
          end
          {else
              Result := 'La Plantilla ' + sFile + ' No Existe';}
     end;
end;

procedure TPlantillas.SetBackupDir(const Value: string);
begin
     FBackupDir := VerificaDir(Value);
end;

procedure TPlantillas.SetDirectorio(const Value: string);
begin
     FDirectorio := VerificaDir(Value);
end;

end.
