unit DReportes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaClientDataSet, Db, DBClient,
  {$ifdef DOS_CAPAS}
     DServerSelReportes,
  {$else}
     SelReportes_TLB,
  {$endif}
  ZetaCommonLists,
  ZetaCommonClasses,
  ZetaTipoEntidad,
  DBaseReportes;

type
  TdmReportes = class(TdmBaseReportes)
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServerReportesBDE: TdmServerSelReportes;
    property ServerReportesBDE: TdmServerSelReportes read GetServerReportesBDE;
{$else}
    FServidorBDE: IdmServerSelReportesDisp;
    function GetServerReportesBDE: IdmServerSelReportesDisp;
    property ServerReportesBDE: IdmServerSelReportesDisp read GetServerReportesBDE;
{$endif}
  protected
    function GeneraSQL( var oSQLAgente: OleVariant; var ParamList: OleVariant;
                        var sError: WideString): OleVariant;override;
    function ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;override;
    function ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;override;
  public
    { Public declarations }
    function DirectorioPlantillas: string;override;
  end;

var
  dmReportes: TdmReportes;

implementation
uses DCliente,
     DGlobal,
     ZetaCommonTools,
     ZGlobalTress,
     ZAccesosTress;
{$R *.DFM}

{$ifdef DOS_CAPAS}
function TdmReportes.GetServerReportesBDE: TdmServerSelReportes;
begin
     Result := DCliente.dmCliente.ServerSelReportes;
end;
{$else}
function TdmReportes.GetServerReportesBDE: IdmServerSelReportesDisp;
begin
     Result := IdmServerSelReportesDisp( dmCliente.CreaServidor( CLASS_dmServerSelReportes, FServidorBDE ) );
end;
{$endif}

function TdmReportes.GeneraSQL( var oSQLAgente: OleVariant; var ParamList : OleVariant; var sError : WideString ) : OleVariant;
begin
     Result := ServerReportesBDE.GeneraSQL( dmCliente.Empresa,oSQLAgente, ParamList , sError  );
end;

function TdmReportes.ServidorEvaluaParam( var oSQLAgente : OleVariant; oParamList: OleVariant; var sError: Widestring ):Boolean;
begin
     Result := ServerReportesBDE.EvaluaParam( dmCliente.Empresa, oSQLAgente, oParamList, sError );
end;

function TdmReportes.ServidorPruebaFormula( Parametros : TZetaParams; var sTexto: widestring; const EntidadActiva: TipoEntidad; oParams: Olevariant ): Boolean;
begin
     //CV: 17-junio-2005
     //La implementacion de esta parte se realizara cuando cambie la llamada de DDiccionario.PruebaFormula
     Result := FALSE;
end;

function TdmReportes.DirectorioPlantillas: string;
begin
     Result := VerificaDir( Global.GetGlobalString( K_GLOBAL_DIR_PLANT ));
end;
end.
