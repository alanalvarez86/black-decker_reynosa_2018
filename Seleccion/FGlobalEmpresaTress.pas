unit FGlobalEmpresaTress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGlobal, StdCtrls, ZetaKeyLookup, Buttons, ExtCtrls;

type
  TGlobalEmpresaTress = class(TBaseGlobal)
    ZEmpresaTress: TZetaDBKeyLookup;
    lblEmpresa: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GlobalEmpresaTress: TGlobalEmpresaTress;

implementation

{$R *.DFM}

uses
    ZAccesosTress,
    ZGlobalTress,
    DSistema;

procedure TGlobalEmpresaTress.FormShow(Sender: TObject);
begin
     inherited;
     dmSistema.cdsEmpresasTress.Conectar;
end;

procedure TGlobalEmpresaTress.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos  := D_CAT_CONFI_GLOBALES;
     with dmSistema do
     begin
          with ZEmpresaTress do
          begin
               LookupDataSet := cdsEmpresasTress;
               Tag           := K_GLOBAL_EMPRESA_TRESS;
          end;
     end;

end;

end.
