unit FAltaWizardDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZetaCommonLists, StdCtrls, Buttons, ExtCtrls, jpeg;

type
  TAltaWizardDlg = class(TForm)
    lbMensaje: TLabel;
    Image1: TImage;
    Label2: TLabel;
    Panel1: TPanel;
    bOtra: TBitBtn;
    bEditar: TBitBtn;
    BitBtn3: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    FFolio: integer;
    FEsSolicitud: Boolean;
    { Private declarations }
  public
    { Public declarations }
    property Folio : integer read FFolio write FFolio;
    property EsSolicitud : Boolean read FEsSolicitud write FEsSolicitud;
  end;

var
  AltaWizardDlg: TAltaWizardDlg;

  function ShowDlgAltaWizard( const iFolio: integer;
                              const lEsSolicitud : Boolean ): eOperacionEnAlta;

implementation

{$R *.DFM}

{ TForm1 }



function ShowDlgAltaWizard( const iFolio: integer;
                              const lEsSolicitud : Boolean ): eOperacionEnAlta;
begin
     if ( AltaWizardDlg = nil ) then
          AltaWizardDlg := TAltaWizardDlg.Create( Application.MainForm ); // Se destruye al Salir del Programa //

     with AltaWizardDlg do
     begin
          Folio := iFolio;
          EsSolicitud := lEsSolicitud;
          ShowModal;
          case ModalResult of
               mrOk : Result := opOtraAlta;
               mrYes : Result := opEditar
               else Result := opTerminar
          end;
     end;
end;

procedure TAltaWizardDlg.FormShow(Sender: TObject);
 var sTexto : string;
begin
     if EsSolicitud then sTexto := 'Solicitud'
     else sTexto := 'Requisición';

     lbMensaje.Caption := 'La ' + sTexto +' Ha Sido Agregada con el Folio # ' + IntToStr(FFolio);
     bOtra.Caption := 'Otra ' + sTexto;
     bEditar.Caption := 'Editar ' + sTexto;
end;


end.
