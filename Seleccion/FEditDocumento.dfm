inherited EditDocumento: TEditDocumento
  Left = 361
  Top = 400
  Caption = 'Documento'
  ClientHeight = 184
  ClientWidth = 469
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 55
    Top = 78
    Width = 24
    Height = 13
    Caption = '&Tipo:'
  end
  object Label2: TLabel [1]
    Left = 40
    Top = 101
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Archivo:'
  end
  object SpeedButton1: TSpeedButton [2]
    Left = 438
    Top = 96
    Width = 23
    Height = 22
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      5555555555555555555555555555555555555555555555555555555555555555
      555555555555555555555555555555555555555FFFFFFFFFF555550000000000
      55555577777777775F55500B8B8B8B8B05555775F555555575F550F0B8B8B8B8
      B05557F75F555555575F50BF0B8B8B8B8B0557F575FFFFFFFF7F50FBF0000000
      000557F557777777777550BFBFBFBFB0555557F555555557F55550FBFBFBFBF0
      555557F555555FF7555550BFBFBF00055555575F555577755555550BFBF05555
      55555575FFF75555555555700007555555555557777555555555555555555555
      5555555555555555555555555555555555555555555555555555}
    NumGlyphs = 2
    OnClick = SpeedButton1Click
  end
  object SpeedButton4: TSpeedButton [3]
    Left = 365
    Top = 6
    Width = 102
    Height = 24
    Hint = 'Ver Documento'
    Caption = '&Ver Documento'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333FF3333333333333C0C333333333333F777F3333333333CC0F0C3
      333333333777377F33333333C30F0F0C333333337F737377F333333C00FFF0F0
      C33333F7773337377F333CC0FFFFFF0F0C3337773F33337377F3C30F0FFFFFF0
      F0C37F7373F33337377F00FFF0FFFFFF0F0C7733373F333373770FFFFF0FFFFF
      F0F073F33373F333373730FFFFF0FFFFFF03373F33373F333F73330FFFFF0FFF
      00333373F33373FF77333330FFFFF000333333373F333777333333330FFF0333
      3333333373FF7333333333333000333333333333377733333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
  end
  object Label5: TLabel [4]
    Left = 5
    Top = 122
    Width = 74
    Height = 13
    Caption = 'Observaciones:'
  end
  inherited PanelBotones: TPanel
    Top = 148
    Width = 469
    TabOrder = 6
    inherited OK: TBitBtn
      Left = 301
    end
    inherited Cancelar: TBitBtn
      Left = 386
    end
  end
  inherited PanelSuperior: TPanel
    Width = 469
    TabOrder = 0
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 469
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 143
    end
  end
  object Panel1: TPanel [8]
    Left = 0
    Top = 51
    Width = 469
    Height = 30
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 60
      Top = 8
      Width = 43
      Height = 13
      Caption = 'Solicitud:'
    end
    object SO_FOLIO: TZetaDBTextBox
      Left = 110
      Top = 6
      Width = 50
      Height = 17
      AutoSize = False
      Caption = 'SO_FOLIO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SO_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PRETTYNAME: TZetaTextBox
      Left = 163
      Top = 6
      Width = 300
      Height = 17
      AutoSize = False
      Caption = 'PRETTYNAME'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object DO_TIPO: TDBComboBox [9]
    Left = 81
    Top = 74
    Width = 108
    Height = 21
    DataField = 'DO_TIPO'
    DataSource = DataSource
    ItemHeight = 13
    Items.Strings = (
      'CURRIC'
      'CURSO'
      'HUELLA'
      'FOTO'
      'ACTA'
      'AUDIO'
      'VOZ')
    TabOrder = 3
    OnChange = DO_TIPOChange
  end
  object EArchivo: TEdit [10]
    Left = 81
    Top = 97
    Width = 352
    Height = 21
    TabOrder = 4
    Text = 'EArchivo'
    OnChange = EArchivoChange
  end
  object DO_OBSERVA: TDBEdit [11]
    Left = 81
    Top = 119
    Width = 380
    Height = 21
    DataField = 'DO_OBSERVA'
    DataSource = DataSource
    MaxLength = 30
    TabOrder = 5
  end
  inherited DataSource: TDataSource
    Left = 304
    Top = 57
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Documentos (*.DOC,*.TXT,*.RTF,*.XLS,*.PDF,*.HTM,*.HTML)|*.doc;*.' +
      'txt;*.RTF;*.XLS;*.PDF|Imagenes (*.BMP;*.CMS;*.GIF;*.JPG;*.PCX;*.' +
      'PNG;*.SCM;*.TIF;*.WAV;*.MID;*.RMI;*.AVI;*.MOV)|*.BMP;*.CMS;*.GIF' +
      ';*.JPG;*.PCX;*.PNG;*.SCM;*.TIF;*.WAV;*.MID;*.RMI;*.AVI;*.MOV|Aud' +
      'io/Video (*.WAV,*.MDI,*.MP3,*.WMA,*.CDA,*.MPG,*.AVI,*.MOV)|*.WAV' +
      ';*.MDI;*.MP3;*.WMA;*.CDA;*.MPG;*.AVI;*.MOV|Todos los Archivos (*' +
      '.*)|*.*'
    Left = 8
    Top = 88
  end
end
