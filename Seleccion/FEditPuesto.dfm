inherited EditPuesto: TEditPuesto
  Left = 305
  Top = 447
  Caption = 'Puesto'
  ClientHeight = 359
  ClientWidth = 425
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel [0]
    Left = 104
    Top = 16
    Width = 3
    Height = 13
  end
  inherited PanelBotones: TPanel
    Top = 323
    Width = 425
    TabOrder = 4
    inherited OK: TBitBtn
      Left = 257
    end
    inherited Cancelar: TBitBtn
      Left = 342
    end
  end
  inherited PanelSuperior: TPanel
    Width = 425
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 425
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 99
    end
  end
  object PanelCodigo: TPanel [4]
    Left = 0
    Top = 51
    Width = 425
    Height = 50
    Align = alTop
    TabOrder = 2
    object PU_CODIGOlbl: TLabel
      Left = 54
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C�d&igo:'
      FocusControl = PU_CODIGO
    end
    object PU_DESCRIPlbl: TLabel
      Left = 30
      Top = 29
      Width = 59
      Height = 13
      Caption = 'De&scripci�n:'
      FocusControl = PU_DESCRIP
    end
    object PU_CODIGO: TZetaDBEdit
      Left = 91
      Top = 4
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 0
      Text = 'SECRE'
      ConfirmEdit = True
      DataField = 'PU_CODIGO'
      DataSource = DataSource
    end
    object PU_DESCRIP: TDBEdit
      Left = 91
      Top = 25
      Width = 281
      Height = 21
      DataField = 'PU_DESCRIP'
      DataSource = DataSource
      TabOrder = 1
    end
  end
  object PageControl: TPageControl [5]
    Left = 0
    Top = 101
    Width = 425
    Height = 222
    ActivePage = Perfil
    Align = alClient
    TabOrder = 3
    object Perfil: TTabSheet
      Caption = 'Perfil'
      object PU_AREAlbl: TLabel
        Left = 11
        Top = 135
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'A&rea de Inter�s:'
        FocusControl = PU_AREA
      end
      object PU_SEXOlbl: TLabel
        Left = 59
        Top = 63
        Width = 27
        Height = 13
        Alignment = taRightJustify
        Caption = 'Se&xo:'
        FocusControl = PU_SEXO
      end
      object PU_ESTUDIOlbl: TLabel
        Left = 27
        Top = 87
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Escolari&dad:'
        FocusControl = PU_ESTUDIO
      end
      object PU_DOMINA1lbl: TLabel
        Left = 14
        Top = 111
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dominio I&ngl�s:'
        FocusControl = PU_DOMINA1
      end
      object QU_CODIGOlbl: TLabel
        Left = 19
        Top = 159
        Width = 67
        Height = 13
        Alignment = taRightJustify
        Caption = 'O&tros criterios:'
        FocusControl = QU_CODIGO
      end
      object PU_AREA: TZetaDBKeyLookup
        Left = 88
        Top = 131
        Width = 329
        Height = 21
        LookupDataset = dmSeleccion.cdsAreaInteres
        TabOrder = 4
        TabStop = True
        WidthLlave = 80
        DataField = 'PU_AREA'
        DataSource = DataSource
      end
      object PU_SEXO: TZetaDBKeyCombo
        Left = 88
        Top = 59
        Width = 120
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        Items.Strings = (
          '<Cualquiera>'
          'Masculino'
          'Femenino')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'PU_SEXO'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object EdadGB: TGroupBox
        Left = 32
        Top = 2
        Width = 199
        Height = 49
        Caption = ' Edad '
        TabOrder = 0
        object PU_EDADMINlbl: TLabel
          Left = 24
          Top = 20
          Width = 28
          Height = 13
          Alignment = taRightJustify
          Caption = '&Entre:'
          FocusControl = PU_EDADMIN
        end
        object EdadAniosLBL: TLabel
          Left = 162
          Top = 20
          Width = 24
          Height = 13
          Caption = 'A�os'
        end
        object PU_EDADMAXLBL: TLabel
          Left = 103
          Top = 20
          Width = 5
          Height = 13
          Caption = '&y'
          FocusControl = PU_EDADMAX
        end
        object PU_EDADMIN: TZetaDBNumero
          Left = 55
          Top = 16
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'PU_EDADMIN'
          DataSource = DataSource
        end
        object PU_EDADMAX: TZetaDBNumero
          Left = 114
          Top = 16
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          DataField = 'PU_EDADMAX'
          DataSource = DataSource
        end
      end
      object PU_ESTUDIO: TZetaDBKeyCombo
        Left = 88
        Top = 83
        Width = 120
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        Items.Strings = (
          '<Cualquiera>'
          'Primaria'
          'Secundaria'
          'Bachillerato'
          'T�cnico'
          'Profesional'
          'Maestr�a'
          'Doctorado')
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'PU_ESTUDIO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object PU_DOMINA1: TZetaDBNumero
        Left = 88
        Top = 107
        Width = 45
        Height = 21
        Mascara = mnTasa
        TabOrder = 3
        Text = '0.0 %'
        DataField = 'PU_DOMINA1'
        DataSource = DataSource
      end
      object QU_CODIGO: TZetaDBKeyLookup
        Left = 88
        Top = 155
        Width = 329
        Height = 21
        LookupDataset = dmSeleccion.cdsCondiciones
        TabOrder = 5
        TabStop = True
        WidthLlave = 80
        DataField = 'QU_CODIGO'
        DataSource = DataSource
      end
    end
    object Habilidades: TTabSheet
      Caption = 'Habilidades Requeridas'
      ImageIndex = 1
      object PU_HABILID: TDBMemo
        Left = 0
        Top = 0
        Width = 417
        Height = 194
        Align = alClient
        DataField = 'PU_HABILID'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Actividades: TTabSheet
      Caption = 'Actividades a Desarrollar'
      ImageIndex = 2
      object PU_ACTIVID: TDBMemo
        Left = 0
        Top = 0
        Width = 417
        Height = 194
        Align = alClient
        DataField = 'PU_ACTIVID'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Oferta: TTabSheet
      Caption = 'Oferta'
      ImageIndex = 3
      object PU_OFERTA: TDBMemo
        Left = 0
        Top = 81
        Width = 417
        Height = 113
        Align = alClient
        DataField = 'PU_OFERTA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 417
        Height = 81
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label13: TLabel
          Left = 3
          Top = 10
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Sueldo Base:'
        end
        object Label14: TLabel
          Left = 7
          Top = 34
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vacaciones:'
        end
        object Label30: TLabel
          Left = 16
          Top = 58
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aguinaldo:'
        end
        object Label31: TLabel
          Left = 231
          Top = 10
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bono(s):'
        end
        object Label17: TLabel
          Left = 241
          Top = 34
          Width = 29
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vales:'
        end
        object PU_SUELDO: TDBEdit
          Left = 68
          Top = 6
          Width = 140
          Height = 21
          DataField = 'PU_SUELDO'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 0
        end
        object PU_VACACI: TDBEdit
          Left = 68
          Top = 30
          Width = 140
          Height = 21
          DataField = 'PU_VACACI'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 1
        end
        object PU_BONO: TDBEdit
          Left = 273
          Top = 6
          Width = 140
          Height = 21
          DataField = 'PU_BONO'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 3
        end
        object PU_VALES: TDBEdit
          Left = 273
          Top = 30
          Width = 140
          Height = 21
          DataField = 'PU_VALES'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 4
        end
        object PU_AGUINAL: TDBEdit
          Left = 68
          Top = 54
          Width = 140
          Height = 21
          DataField = 'PU_AGUINAL'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 2
        end
      end
    end
    object Adicionales: TTabSheet
      Caption = 'Adicionales'
      ImageIndex = 4
      object PU_INGLESlbl: TLabel
        Left = 25
        Top = 62
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nom&bre en Ingl�s:'
        FocusControl = PU_INGLES
      end
      object PU_TEXTOlbl: TLabel
        Left = 41
        Top = 90
        Width = 70
        Height = 13
        Alignment = taRightJustify
        Caption = 'Te&xto General:'
        FocusControl = PU_TEXTO
      end
      object PU_NUMEROlbl: TLabel
        Left = 31
        Top = 118
        Width = 80
        Height = 13
        Alignment = taRightJustify
        Caption = 'N�&mero General:'
        FocusControl = PU_NUMERO
      end
      object PU_INGLES: TDBEdit
        Left = 114
        Top = 58
        Width = 277
        Height = 21
        DataField = 'PU_INGLES'
        DataSource = DataSource
        TabOrder = 0
      end
      object PU_TEXTO: TDBEdit
        Left = 114
        Top = 86
        Width = 277
        Height = 21
        DataField = 'PU_TEXTO'
        DataSource = DataSource
        TabOrder = 1
      end
      object PU_NUMERO: TZetaDBNumero
        Left = 114
        Top = 114
        Width = 121
        Height = 21
        Mascara = mnNumeroGlobal
        TabOrder = 2
        Text = '0.00'
        DataField = 'PU_NUMERO'
        DataSource = DataSource
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 384
    Top = 5
  end
end
