unit FEditSolicitud;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Mask, ComCtrls, ExtDlgs, ImgList, ToolWin, TDMULTIP, Grids, DBGrids, Db,
     ExtCtrls, DBCtrls, Buttons, StdCtrls,
     ZBaseEdicion,
     ZetaDBTextBox,
     ZetaFecha,
     ZetaKeyCombo,
     ZetaNumero,
     ZetaKeyLookup,
     ZetaDBGrid, ZetaSmartLists;

type
  TEditSolicitud = class(TBaseEdicion)
    PanelFolio: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    SO_FOLIO: TZetaTextBox;
    PRETTYNAME: TZetaTextBox;
    PageControl: TPageControl;
    Generales: TTabSheet;
    LblApePat: TLabel;
    LblApeMat: TLabel;
    LblNombres: TLabel;
    LblFecNac: TLabel;
    LblSexo: TLabel;
    LblEdoCivil: TLabel;
    SO_APE_PAT: TDBEdit;
    SO_APE_MAT: TDBEdit;
    SO_NOMBRES: TDBEdit;
    SO_FEC_NAC: TZetaDBFecha;
    ZEdad: TZetaTextBox;
    SO_EDO_CIV: TZetaDBKeyCombo;
    FotoSwitch: TSpeedButton;
    FOTOGRAFIA: TPDBMultiImage;
    BarraFotografia: TToolBar;
    tbLeerFoto: TToolButton;
    tbGrabarFoto: TToolButton;
    tbLeerTwain: TToolButton;
    tbSelectTwain: TToolButton;
    ImageListPrende: TImageList;
    OpenPictureDialog: TOpenPictureDialog;
    SavePictureDialog: TSavePictureDialog;
    ImageListApaga: TImageList;
    Solicitud: TTabSheet;
    LblFecSolicitud: TLabel;
    LblPuestoSolic: TLabel;
    GBAreas: TGroupBox;
    SO_AREA_1: TZetaDBKeyLookup;
    SO_AREA_2: TZetaDBKeyLookup;
    SO_AREA_3: TZetaDBKeyLookup;
    SO_FEC_INI: TZetaDBFecha;
    SO_PUE_SOL: TDBEdit;
    Curriculum: TTabSheet;
    SO_OBSERVA: TDBMemo;
    Examenes: TTabSheet;
    Panel2: TPanel;
    btnAgregarExamen: TSpeedButton;
    btnBorrarExamen: TSpeedButton;
    btnModificarExamen: TSpeedButton;
    GridExamenes: TZetaDBGrid;
    dsExamenes: TDataSource;
    Candidaturas: TTabSheet;
    ZetaDBGrid1: TZetaDBGrid;
    dsCandidaturas: TDataSource;
    SO_STATUS: TZetaDBKeyCombo;
    SO_SEXO: TZetaDBKeyCombo;
    Adicionales: TTabSheet;
    Requisitos: TTabSheet;
    SO_REQUI_1: TDBCheckBox;
    SO_REQUI_1lbl: TLabel;
    SO_REQUI_2lbl: TLabel;
    SO_REQUI_2: TDBCheckBox;
    SO_REQUI_3lbl: TLabel;
    SO_REQUI_3: TDBCheckBox;
    SO_REQUI_4: TDBCheckBox;
    SO_REQUI_4lbl: TLabel;
    SO_REQUI_5lbl: TLabel;
    SO_REQUI_6lbl: TLabel;
    SO_REQUI_7lbl: TLabel;
    SO_REQUI_8lbl: TLabel;
    SO_REQUI_8: TDBCheckBox;
    SO_REQUI_7: TDBCheckBox;
    SO_REQUI_6: TDBCheckBox;
    SO_REQUI_5: TDBCheckBox;
    SO_REQUI_9lbl: TLabel;
    SO_REQUI_9: TDBCheckBox;
    AdicionalScrollBox: TScrollBox;
    dsGridSolicitud: TDataSource;
    Referencias: TTabSheet;
    PanelReferencia: TPanel;
    BtnReferencias: TSpeedButton;
    GridReferencias: TZetaDBGrid;
    dsRefLaboral: TDataSource;
    Documentos: TTabSheet;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    dsDocumentos: TDataSource;
    DBGrid1: TDBGrid;
    bDocumento: TSpeedButton;
    Direccion: TTabSheet;
    Label6: TLabel;
    SO_PAIS: TDBEdit;
    Label7: TLabel;
    SO_CURP: TDBEdit;
    Label8: TLabel;
    SO_RFC: TDBEdit;
    Label9: TLabel;
    SO_CALLE: TDBEdit;
    SO_COLONIA: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    SO_CODPOST: TDBEdit;
    SO_CIUDAD: TDBEdit;
    Label12: TLabel;
    lblEstado: TLabel;
    SO_ESTADO: TZetaDBKeyLookup;
    SO_NACION: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    SO_TEL: TDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    SO_EMAIL: TDBEdit;
    Educacion: TTabSheet;
    LblEscuela: TLabel;
    SO_ESTUDIO: TZetaDBKeyCombo;
    LblIngles: TLabel;
    SO_INGLES: TZetaDBNumero;
    Label2: TLabel;
    SO_IDIOMA2: TDBEdit;
    LblIdioma: TLabel;
    LblDominio: TLabel;
    SO_DOMINA2: TZetaDBNumero;
    Label4: TLabel;
    Label20: TLabel;
    SO_ESCUELA: TDBEdit;
    ZYear: TZetaNumero;
    Label21: TLabel;
    ZMonth: TZetaNumero;
    Label22: TLabel;
    lblTransporte: TLabel;
    SO_MED_TRA: TZetaDBKeyLookup;
    DocEntregadaGb: TGroupBox;
    OtrosRequisitosGb: TGroupBox;
    Label5: TLabel;
    Label13: TLabel;
    SO_NUM_EXT: TDBEdit;
    SO_NUM_INT: TDBEdit;
    procedure tbLeerFotoClick(Sender: TObject);
    procedure tbGrabarFotoClick(Sender: TObject);
    procedure tbLeerTwainClick(Sender: TObject);
    procedure tbSelectTwainClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FotoSwitchClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure btnAgregarExamenClick(Sender: TObject);
    procedure btnModificarExamenClick(Sender: TObject);
    procedure btnBorrarExamenClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dsGridSolicitudDataChange(Sender: TObject; Field: TField);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure dsGridReqCandidatosDataChange(Sender: TObject;
      Field: TField);
    procedure BtnReferenciasClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure bDocumentoClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure tbBorrarFotoClick(Sender: TObject);
    procedure ZYearExit(Sender: TObject);
    procedure ZMonthExit(Sender: TObject);
  private
    { Private declarations }
    FTieneTwain: Boolean;
    FRevisaTwain: Boolean;
    FConectando: Boolean;
    rMonthActual: Real;
    rYearActual: Real;
    procedure SetAdicionales;
    procedure SetRequisitos;
    procedure SetReferencias;
    procedure IniciaResidencia;
    procedure CambiaFechaResidencia;
  protected
    { Protected declarations }
    function PuedeAgregar(var sMensaje: String): Boolean; override;
    function PuedeBorrar(var sMensaje: String): Boolean; override;
    procedure Connect; override;
    procedure Disconnect; override;
    procedure HabilitaControles; override;
    procedure EscribirCambios; override;{OP: 06/06/08}    
  public
    { Public declarations }
  end;

const
     K_ACCESO_EXAMEN = 'No Tiene Permiso Para %s Examenes';
     K_ACCESO_DOCUMENTO = 'No Tiene Permiso Para %s Documentos';
var
  EditSolicitud: TEditSolicitud;

implementation

uses DSeleccion,
     DGlobal,
     ZGlobalTress,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientTools,
     ZetaDialogo,
     ZetaAdicionalMgr,
     ZAccesosMgr,
     ZAccesosTress,
     FEditDocumento,
     FToolsSeleccion,
     JPEG,
     DLL95V1,
     FToolsFoto;

{$R *.DFM}

procedure TEditSolicitud.FormCreate(Sender: TObject);
var
   eSex: eSexo;
   eEdo: eEdoCivil;
begin
     FirstControl := SO_APE_PAT;
     FRevisaTwain := True;
     SO_ESTUDIO.ListaFija := lfEstudios;
     SO_EDO_CIV.ListaFija := lfEdoCivil;
     with SO_SEXO.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for eSex := Low( eSexo ) to High( eSexo ) do
             begin
                  Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfSexo, Ord( eSex ) ), ZetaCommonLists.ObtieneElemento( lfSexoDesc, Ord( eSex ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     with SO_EDO_CIV.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             for eEdo := Low( eEdoCivil ) to High( eEdoCivil ) do
             begin
                  Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfEdoCivil, Ord( eEdo ) ), ZetaCommonLists.ObtieneElemento( lfEdoCivilDesc, Ord( eEdo ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     inherited;
//     SetAdicionales;
     IndexDerechos := D_SOLICITUD_DATOS;
     HelpContext := H00073_SOLICITUDES;
     with dmSeleccion do
     begin
          SO_AREA_1.LookupDataset := cdsAreaInteres;
          SO_AREA_2.LookupDataset := cdsAreaInteres;
          SO_AREA_3.LookupDataset := cdsAreaInteres;
          SO_ESTADO.LookupDataset := cdsEstados;
          SO_MED_TRA.LookupDataset := cdsTransporte;
     end;
end;

procedure TEditSolicitud.FormShow(Sender: TObject);
begin
     SetAdicionales;
     SetRequisitos;
     SetReferencias;
     PageControl.ActivePage := Generales;
     inherited;
     PanelFolio.Visible := not Inserting;
{     if Referencias.TabVisible then
        Referencias.TabVisible := not Inserting; }
end;

procedure TEditSolicitud.Connect;
begin
     FConectando := TRUE;
     try
        with dmSeleccion do
        begin
             // El conectar de Tablas Adicionales se hace en m�todo SetAdicionales()

             DbNavigator.Visible := RequiereNavegador;
             if RequiereNavegador then
             begin
                  dsGridSolicitud.DataSet := GetcdsSolicitudes;
                  DbNavigator.DataSource := dsGridSolicitud;
             end;
             cdsAreaInteres.Conectar;
             cdsPuestos.Conectar;
             DataSource.DataSet:= cdsEditSolicita;
             dsExamenes.DataSet:= cdsExamenes;
             dsCandidaturas.DataSet:= cdsSolCandidatos;
             dsRefLaboral.DataSet := cdsRefLaboral;
             dsDocumentos.DataSet := cdsDocumentos;
             if SetControlesEmpresaTress( lblEstado, SO_ESTADO ) then
                cdsEstados.Conectar;
             if SetControlesEmpresaTress( lblTransporte, SO_MED_TRA ) then
                cdsTransporte.Conectar;

             if Inserting then
                FirstControl := SO_APE_PAT
             else
                FirstControl := SO_STATUS;
        end;
     finally
            FConectando := FALSE;
     end;
     IniciaResidencia;
end;

procedure TEditSolicitud.Disconnect;
begin
     DataSource.DataSet:= nil;
     dsExamenes.DataSet:= nil;
     dsCandidaturas.DataSet:= nil;
     DbNavigator.DataSource := nil;
     dsGridSolicitud.DataSet := nil;
end;

function TEditSolicitud.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Agregar En Esta Pantalla';
end;

function TEditSolicitud.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := False;
     sMensaje := 'No Se Puede Borrar En Esta Pantalla';
end;

procedure TEditSolicitud.HabilitaControles;
begin
     Examenes.TabVisible := not Inserting;
     Candidaturas.TabVisible := ZAccesosMgr.Revisa( D_SOLICITUD_VER_CANDIDATOS ) and ( not Inserting );
     inherited;
end;

{OP:23.Abr.08 Modificaci�n}
procedure TEditSolicitud.SetRequisitos;
var
   lHay: Boolean;
   DocumentoCoord: TCoordenadas;
   OtrosCoord: TCoordenadas;

   procedure SetRequisito( const iGlobal, iGlobalTipo: Word; Control: TDBCheckBox; Etiqueta: TLabel );
   const
        K_ALTURA_OTROS     = 15;
   var
      sEtiqueta: String;
      lOk: Boolean;
      TipoRequisito: eRequisitoTipo;
   begin
        TipoRequisito := eRequisitoTipo( Global.GetGlobalInteger( iGlobalTipo ) );
        sEtiqueta := Global.GetGlobalString( iGlobal );

        case TipoRequisito of
             rtDocumento:
             begin
                  lOk := FToolsSeleccion.AcomodaControles( DocumentoCoord, sEtiqueta, DocEntregadaGb, Etiqueta, Control );
                  OtrosRequisitosGb.Top  := OtrosRequisitosGb.Top + K_ALTURA_OTROS;
             end;
             rtOtro: lOk := FToolsSeleccion.AcomodaControles( OtrosCoord, sEtiqueta, OtrosRequisitosGb, Etiqueta, Control );
        else
            lOk := False;
        end;

        if lOk then
           lHay := True;
   end;

begin
     with DocumentoCoord do
     begin
          TopEtiq := 20;
          TopContr := 20;
          LeftEtiq := 30;
          LeftContr := 10;
          Height := 41;
     end;

     with OtrosCoord do
     begin
          TopEtiq := 20;
          TopContr := 20;
          LeftEtiq := 30;
          LeftContr := 10;
          Height    := 41;
     end;

     DocEntregadaGb.Top := 0;
     OtrosRequisitosGb.Top := 46;
     DocEntregadaGb.Height := DocumentoCoord.Height;
     OtrosRequisitosGb.Height := OtrosCoord.Height;

     lHay := False;
     SetRequisito( K_GLOBAL_REQUISITO1, K_GLOBAL_TPO_REQUISITO1, SO_REQUI_1, SO_REQUI_1lbl );
     SetRequisito( K_GLOBAL_REQUISITO2, K_GLOBAL_TPO_REQUISITO2, SO_REQUI_2, SO_REQUI_2lbl );
     SetRequisito( K_GLOBAL_REQUISITO3, K_GLOBAL_TPO_REQUISITO3, SO_REQUI_3, SO_REQUI_3lbl );
     SetRequisito( K_GLOBAL_REQUISITO4, K_GLOBAL_TPO_REQUISITO4, SO_REQUI_4, SO_REQUI_4lbl );
     SetRequisito( K_GLOBAL_REQUISITO5, K_GLOBAL_TPO_REQUISITO5, SO_REQUI_5, SO_REQUI_5lbl );
     SetRequisito( K_GLOBAL_REQUISITO6, K_GLOBAL_TPO_REQUISITO6, SO_REQUI_6, SO_REQUI_6lbl );
     SetRequisito( K_GLOBAL_REQUISITO7, K_GLOBAL_TPO_REQUISITO7, SO_REQUI_7, SO_REQUI_7lbl );
     SetRequisito( K_GLOBAL_REQUISITO8, K_GLOBAL_TPO_REQUISITO8, SO_REQUI_8, SO_REQUI_8lbl );
     SetRequisito( K_GLOBAL_REQUISITO9, K_GLOBAL_TPO_REQUISITO9, SO_REQUI_9, SO_REQUI_9lbl );
     { Prende / Apaga el TabSheet }
     Requisitos.TabVisible := lHay;
end;

procedure TEditSolicitud.SetReferencias;
var
   i: Integer;
begin
     Referencias.TabVisible := ( Global.GetGlobalInteger( K_GLOBAL_REF_LABORAL_QTY ) > 0 );
     if Referencias.TabVisible then
        with GridReferencias do
             for i := 1 to K_GLOBAL_REF_LABORAL_MAX do
             begin
                  Columns[i].Visible := strLleno( Global.NombreRefLab( i ) );
                  if Columns[i].Visible then
                     Columns[i].Title.Caption := Global.NombreRefLab( i );
                                  end;
end;

procedure TEditSolicitud.SetAdicionales;
var
   FCampos: TListaCampos;

   procedure LimpiaAdicionales;
   var
      i : Integer;
   begin
        for i := AdicionalScrollBox.ControlCount - 1 downto 0 do
            AdicionalScrollBox.Controls[i].Free;
   end;

   procedure DrawAdicionales;
   var
      i, iTop, iMiddle, iHeight, iWidth: Integer;
   begin
        iTop := 2;
        iWidth := AdicionalScrollBox.Width;
        with FCampos do
        begin
             for i := 0 to ( Count - 1 ) do
             begin
                  iMiddle := Trunc( iWidth / 3 );
                  with Campo[ i ] do
                  begin
                       if Capturar then
                       begin
                            with TStaticText.Create( Self ) do
                            begin
                                 Name := Format( '%s_LBL', [ Campo ] );
                                 Parent := AdicionalScrollBox;
                                 Alignment := taRightJustify;
                                 Height := 13;
                                 Top := iTop + 3;
                                 Caption := Letrero + ':';
                                 Left := iMiddle - 2 - Width;
                            end;
                            case Tipo of
                                 cdtTexto:
                                 begin
                                      with TDBEdit.Create( Forma ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           //Width := iWidth - Left - 2;
                                           Width := 300;
                                           Enabled := True;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtNumero:
                                 begin
                                      with TZetaDBNumero.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Mascara := mnNumeroGlobal;
                                           Enabled := True;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtFecha:
                                 begin
                                      with TZetaDBFecha.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Enabled := True;
                                           Opcional := TRUE;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtBooleano:
                                 begin
                                      with TDBCheckBox.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Caption := '';
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height + 2;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           AllowGrayed := False;
                                           Enabled := True;
                                           ValueChecked := K_GLOBAL_SI;
                                           ValueUnchecked := K_GLOBAL_NO;
                                           Datasource := Self.Datasource;
                                      end;
                                 end;
                                 cdtLookup:
                                 begin
                                      with TZetaDBKeyLookup.Create( Self ) do
                                      begin
                                           Name := Format( '%s', [ Campo ] );
                                           DataField := Campo;
                                           Parent := AdicionalScrollBox;;
                                           iHeight := Height;
                                           Top := iTop;
                                           Left := iMiddle + 2;
                                           Enabled := True;
                                           Datasource := Self.Datasource;
                                           LookupDataSet := dmSeleccion.GetcdsAdicional( Global );
                                           LookupDataSet.Conectar;
                                           Opcional := TRUE;
                                      end;
                                 end;
                            else
                                iHeight := 0;
                            end;
                            iTop := iTop + iHeight + 1;
                       end;
                  end;
             end;
        end;
   end; { DrawAdicionales }

begin { SetAdicionales }
     if dmSeleccion.CambioAdicionales then            // Al iniciar el programa esta bandera debe iniciar en TRUE
     begin
          LimpiaAdicionales;
          FCampos := TListaCampos.Create;
          try
             dmSeleccion.LlenaAdicionales( FCampos );
             with Adicionales do
             begin
                  TabVisible := ( FCampos.Count > 0 );
                  if TabVisible then
                     DrawAdicionales;
             end;
          finally
                 FCampos.Free;
          end;
     end;
end;

procedure TEditSolicitud.DataSourceDataChange(Sender: TObject; Field: TField);
var
   dNacimiento : TDateTime;
   rValor: Double;
begin
     inherited;
     // Edad
     rValor := 0;
     with dmSeleccion.cdsEditSolicita do
     begin
          SO_FOLIO.Caption := FieldByName('SO_FOLIO').AsString;
          PRETTYNAME.Caption := FieldByName('PRETTYNAME').AsString;
          if StrVacio(PRETTYNAME.Caption) then
             PRETTYNAME.Caption := FieldByName('SO_APE_PAT').AsString + ' ' +
                                   FieldByName('SO_APE_MAT').AsString + ', ' +
                                   FieldByName('SO_NOMBRES').AsString;

          if ( Field = nil ) or ( Field.FieldName = 'SO_FEC_NAC' ) then
          begin
               dNacimiento := FieldByName( 'SO_FEC_NAC' ).AsDateTime;
               if ( dNacimiento > 0 ) then
                  rValor := Trunc( Now - dNacimiento );
               ZEdad.Caption := TiempoDias( rValor, etYear );
          end;
     end;
end;

procedure TEditSolicitud.dsGridSolicitudDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if NOT FConectando then
     begin
          with dmSeleccion.cdsEditSolicita do
               if ( Field = nil ) and ( not ( State in [ dsEdit, dsInsert ] ) ) then
               begin
                    Application.ProcessMessages;
                    Refrescar;
                    Application.ProcessMessages;
               end;
     end;
end;

procedure TEditSolicitud.dsGridReqCandidatosDataChange(Sender: TObject; Field: TField);
begin
     inherited;
end;

{ Botones de Control de Fotograf�a }

procedure TEditSolicitud.tbLeerFotoClick(Sender: TObject);
begin
     inherited;
     if OpenPictureDialog.Execute then
     begin
          with dmSeleccion.cdsEditSolicita do
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
          FOTOGRAFIA.LoadFromFile( OpenPictureDialog.FileName );
     end;
end;

procedure TEditSolicitud.tbGrabarFotoClick(Sender: TObject);
begin
     inherited;
     SavePictureDialog.FilterIndex := 0;
     SavePictureDialog.FileName := '';
     if SavePictureDialog.Execute then
     begin
          with FOTOGRAFIA do
          begin
               case SavePictureDialog.FilterIndex of
                    1,2:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'JPG';
                         SaveToFileAsJPG(SavePictureDialog.FileName);
                    end;
                    3:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'BMP';
                         SaveToFileAsBMP(SavePictureDialog.FileName);
                    end;
                    4:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'GIF';
                         SaveToFileAsGIF(SavePictureDialog.FileName);
                    end;
                    5:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'TIF';
                         SaveToFileAsTIF(SavePictureDialog.FileName);
                    end;
                    6:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PCX';
                         SaveToFileAsPCX(SavePictureDialog.FileName);
                    end;
                    7:
                    begin
                         if SavePictureDialog.DefaultExt ='' then SavePictureDialog.DefaultExt := 'PNG';
                         SaveToFileAsPNG(SavePictureDialog.FileName);
                    end;
               end;
          end;
     end;
end;

procedure TEditSolicitud.tbLeerTwainClick(Sender: TObject);
begin
     if FRevisaTwain then
     begin
          FRevisaTwain:= False;
          FTieneTwain:= HasTwain( Handle );
     end;
     if FTieneTwain then
     begin
         try
            with dmSeleccion.cdsEditSolicita do
                 if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                    Edit;
            with FOTOGRAFIA do
            begin
                 UseTwainWindow:= True;
                 ScanImage( Handle );
                 Refresh;
            end;
         except
            ZError('','No se pudo obtener la fotograf�a - Error en Dispositivo Twain',0);
         end;
     end;
end;

procedure TEditSolicitud.tbSelectTwainClick(Sender: TObject);
begin
     inherited;
     try
        FOTOGRAFIA.SelectScanner(Handle);
     except
        ZError('','No se pudo Seleccionar - Error en Dispositivo Twain',0);
     end;
end;

procedure TEditSolicitud.FotoSwitchClick(Sender: TObject);
begin
     with FOTOGRAFIA do
     begin
          Visible := FotoSwitch.Down;
          tbLeerFoto.Enabled := Visible;
          tbGrabarFoto.Enabled := Visible;
          tbLeerTwain.Enabled := Visible;
          tbSelectTwain.Enabled := Visible;
     end;
end;

{ Grid de Examenes }

procedure TEditSolicitud.btnAgregarExamenClick(Sender: TObject);
begin
     if ( ZAccesosMgr.CheckDerecho( D_SOLICITUD_EXAMENES, K_DERECHO_ALTA ) ) then
        dmSeleccion.cdsExamenes.Agregar
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_EXAMEN, [ 'Agregar' ] ), 0 );
end;

procedure TEditSolicitud.btnModificarExamenClick(Sender: TObject);
begin
     if NoData( dsExamenes ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Examenes Para Modificar', 0 )
     else
        if ( ZAccesosMgr.CheckDerecho( D_SOLICITUD_EXAMENES, K_DERECHO_CAMBIO ) ) then
           dmSeleccion.cdsExamenes.Modificar
        else
           ZetaDialogo.zInformation( Caption, Format( K_ACCESO_EXAMEN, [ 'Modificar' ] ), 0 );
end;

procedure TEditSolicitud.btnBorrarExamenClick(Sender: TObject);
begin
     if NoData( dsExamenes ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Examenes Para Borrar', 0 )
     else
        if ( ZAccesosMgr.CheckDerecho( D_SOLICITUD_EXAMENES, K_DERECHO_BAJA ) ) then
           dmSeleccion.cdsExamenes.Borrar
        else
           ZetaDialogo.zInformation( Caption, Format( K_ACCESO_EXAMEN, [ 'Borrar' ] ), 0 );
end;

{ Grid de Referencias }

procedure TEditSolicitud.BtnReferenciasClick(Sender: TObject);
begin
     if NoData( dsRefLaboral ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Referencias Para Modificar', 0 )
     else
        if ( ZAccesosMgr.CheckDerecho( D_SOLICITUD_DATOS, K_DERECHO_CAMBIO ) ) then
           dmSeleccion.cdsRefLaboral.Modificar
        else
           ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Modificar la Solicitud', 0 ); 
end;

procedure TEditSolicitud.OKClick(Sender: TObject);
var
   lInsercion: Boolean;
begin
     inherited;
     lInsercion := Inserting;
     inherited;
     with dmSeleccion do
          if ( cdsEditSolicita.ChangeCount = 0 ) then    // Se Aplicaron los cambios
          begin
               if lInsercion then
               begin
                    cdsEditSolicita.Append;
                    AgregaRefLaborales;
                    PageControl.ActivePage := Generales;
                    HabilitaControles;
               end;
          end;
end;

procedure TEditSolicitud.CancelarClick(Sender: TObject);
var
   lInsercion: Boolean;
begin
     lInsercion := Inserting;
     inherited;
     if lInsercion then
        Close;
end;

procedure TEditSolicitud.PageControlChange(Sender: TObject);
begin
     inherited;
     with PageControl do
          if ( ActivePage = Adicionales ) then
             self.HelpContext := H00079_Solicitud_Adicionales
          else if ( ActivePage = Requisitos ) then
             self.HelpContext := H00078_Solicitud_Requisitos
          else if ( ActivePage = Referencias ) then
             self.HelpContext := H00083_Solicitud_Referencias
          else
             self.HelpContext := H00073_Solicitudes;
end;

procedure TEditSolicitud.bDocumentoClick(Sender: TObject);
begin
     inherited;
     dmSeleccion.AbreDocumento;
end;

procedure TEditSolicitud.SpeedButton1Click(Sender: TObject);
begin
     inherited;
     if ( ZAccesosMgr.CheckDerecho( D_SOLICITUD_DODUMENTO, K_DERECHO_ALTA ) ) then
        dmSeleccion.cdsDocumentos.Agregar
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_DOCUMENTO, [ 'Agregar' ] ), 0 );

end;

procedure TEditSolicitud.SpeedButton2Click(Sender: TObject);
begin
     inherited;
     if NoData( dsDocumentos ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Documentos Para Borrar', 0 )
     else
        if ( ZAccesosMgr.CheckDerecho( D_SOLICITUD_DODUMENTO, K_DERECHO_BAJA ) ) then
           dmSeleccion.cdsDocumentos.Borrar
        else
           ZetaDialogo.zInformation( Caption, Format( K_ACCESO_DOCUMENTO, [ 'Borrar' ] ), 0 );

end;

procedure TEditSolicitud.tbBorrarFotoClick(Sender: TObject);
begin
     inherited;
     with dmSeleccion.cdsEditSolicita do
     begin
          if ( not ( State in [ dsEdit, dsInsert ] ) ) then
             Edit;
          TBlobField(FieldByName('SO_FOTO')).Clear;
     end;
end;

procedure TEditSolicitud.CambiaFechaResidencia;
begin
     with dmSeleccion.cdsEditSolicita do
     begin
          if not Editing then
             Edit;
          FieldByName( 'SO_FEC_RES' ).AsDateTime:= ZetaClientTools.FechaResidencia( ZYear.Valor, ZMonth.Valor );
     end;
end;

procedure TEditSolicitud.IniciaResidencia;
begin
     SetYearsMonths( DataSource.DataSet.FieldByName( 'SO_FEC_RES' ).AsDateTime,
                     rYearActual, rMonthActual );
     ZYear.Valor  := rYearActual;
     ZMonth.Valor := rMonthActual;
end;

procedure TEditSolicitud.ZYearExit(Sender: TObject);
begin
     if ( rYearActual <> ZYear.Valor ) then
     begin
          CambiaFechaResidencia;
          rYearActual := ZYear.Valor;
     end;
end;

procedure TEditSolicitud.ZMonthExit(Sender: TObject);
begin
     if ( rMonthActual <> ZMonth.Valor ) then
     begin
          if ZMonth.Valor >= 12 then
          begin
             ZError( VACIO, 'Meses Tiene Que Ser Menor a 12', 0);
             ActiveControl:= ZMonth;
          end
          else
          begin
               CambiaFechaResidencia;
               rMonthActual := ZMonth.Valor;
          end;
     end;
end;

{OP: 05/06/08}
procedure TEditSolicitud.EscribirCambios;
begin
     if ( strLleno( dmSeleccion.cdsEditSolicita.FieldByName('SO_FOTO').AsString ) ) then
     begin
          {OP: 05/06/08}
           with FOTOGRAFIA do
           begin
                CutToClipboard;
                UpdateAsJPG:=True;
                PastefromClipboard;
           end;
        try  //acl1
           if( FOTOGRAFIA.Picture.Bitmap.Width > 300 ) then
               FToolsFoto.ResizeImageBestFit( FOTOGRAFIA, 300, 300 );
        except
              on Error: Exception do
              begin
                   ZError( self.Caption,'No se pudo ajustar el tama�o de la fotograf�a. Se guardar� con su tama�o original'+CR_LF+Error.Message, 0);
              end;
        end;
     end;
     inherited EscribirCambios;
end;

end.


