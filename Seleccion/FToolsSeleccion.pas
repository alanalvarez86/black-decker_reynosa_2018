unit FToolsSeleccion;

interface

uses Controls, Dialogs, SysUtils, StdCtrls, DBCtrls, {ZetaCommonLists,} ZetaCommonClasses, ZetaCommonTools;

type
  TCoordenadas = record
    LeftEtiq: Integer;
    TopEtiq: Integer;
    LeftContr: Integer;
    TopContr: Integer;
    Height: Integer;
end;

function AcomodaControles( var GrupoCoord: TCoordenadas; const sEtiqueta: String; Grupo: TGroupBox; Etiqueta: TLabel; Control: TDBCheckBox ): Boolean;

implementation
{OP:24.Abr.08}
function AcomodaControles( var GrupoCoord: TCoordenadas; const sEtiqueta: String; Grupo: TGroupBox; Etiqueta: TLabel; Control: TDBCheckBox ): Boolean;
const
     K_ALTURA_ADICIONAL = 15;
var
   lOk: Boolean;
begin
     lOk := ZetaCommonTools.StrLleno( sEtiqueta );
     Control.Visible := lOk;
     if Etiqueta <> nil then
     begin
          with Etiqueta do
          begin
               Caption := Format( '%s', [ sEtiqueta ] );
               Visible := lOk;
          end;
     end
     else
          Control.Caption := Format( '%s', [ sEtiqueta ] );

     if lOk then
     begin
          if Etiqueta <> nil then
          begin
               Etiqueta.Parent:= Grupo;
               Etiqueta.Top   := GrupoCoord.TopEtiq;
               Etiqueta.Left  := GrupoCoord.LeftEtiq;
               GrupoCoord.TopEtiq  := GrupoCoord.TopEtiq + K_ALTURA_ADICIONAL;
          end;

          Control.Parent := Grupo;
          Control.Top    := GrupoCoord.TopContr;
          Control.Left   := GrupoCoord.LeftContr;
          GrupoCoord.TopContr := GrupoCoord.TopContr + K_ALTURA_ADICIONAL;

          Grupo.Height   := GrupoCoord.Height;
          GrupoCoord.Height   := GrupoCoord.Height + K_ALTURA_ADICIONAL;
     end;
     Result := lOk;
end;

end.
