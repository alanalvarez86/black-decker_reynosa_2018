inherited EditEntrevista: TEditEntrevista
  Left = 303
  Top = 158
  Caption = 'Entrevista'
  ClientHeight = 388
  ClientWidth = 498
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel [0]
    Left = 18
    Top = 186
    Width = 65
    Height = 13
    Caption = 'Entrevistador:'
  end
  object Label5: TLabel [1]
    Left = 50
    Top = 162
    Width = 33
    Height = 13
    Caption = 'Fecha:'
  end
  object Label6: TLabel [2]
    Left = 35
    Top = 210
    Width = 48
    Height = 13
    Caption = 'Resumen:'
  end
  object Label7: TLabel [3]
    Left = 47
    Top = 239
    Width = 36
    Height = 13
    Caption = 'Detalle:'
  end
  object Label8: TLabel [4]
    Left = 2
    Top = 326
    Width = 81
    Height = 13
    Caption = 'Recomendaci�n:'
  end
  object Label9: TLabel [5]
    Left = 58
    Top = 136
    Width = 25
    Height = 13
    Caption = 'Folio:'
  end
  object ZetaDBTextBox1: TZetaDBTextBox [6]
    Left = 88
    Top = 132
    Width = 115
    Height = 21
    AutoSize = False
    Caption = 'ZetaDBTextBox1'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'ER_FOLIO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 352
    Width = 498
    inherited OK: TBitBtn
      Left = 330
    end
    inherited Cancelar: TBitBtn
      Left = 415
    end
  end
  inherited PanelSuperior: TPanel
    Width = 498
  end
  inherited PanelIdentifica: TPanel
    Width = 498
    inherited ValorActivo2: TPanel
      Width = 172
    end
  end
  object Panel1: TPanel [10]
    Left = 0
    Top = 51
    Width = 498
    Height = 30
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 21
      Top = 7
      Width = 58
      Height = 13
      Caption = 'Requisici�n:'
    end
    object eRequisicion: TZetaTextBox
      Left = 87
      Top = 5
      Width = 390
      Height = 19
      AutoSize = False
      Caption = '#1234: 1 Vacante de SECRETARIA EJECUTIVA'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object Panel2: TPanel [11]
    Left = 0
    Top = 81
    Width = 498
    Height = 49
    Align = alTop
    TabOrder = 4
    object eNombreCandidato: TZetaTextBox
      Left = 87
      Top = 5
      Width = 390
      Height = 19
      AutoSize = False
      Caption = '#101: Perez Martinez, Juan'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object ePerfil: TZetaTextBox
      Left = 87
      Top = 25
      Width = 390
      Height = 19
      AutoSize = False
      Caption = 'Masculino, 34 a�os, Secundaria, 60% Ingl�s'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object Label3: TLabel
      Left = 55
      Top = 28
      Width = 26
      Height = 13
      Caption = 'Perfil:'
    end
    object Label2: TLabel
      Left = 30
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Candidato:'
    end
  end
  object ER_FECHA: TZetaDBFecha [12]
    Left = 88
    Top = 157
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 5
    Text = '05/Oct/01'
    Valor = 37169
    DataField = 'ER_FECHA'
    DataSource = DataSource
  end
  object ER_NOMBRE: TDBEdit [13]
    Left = 88
    Top = 182
    Width = 350
    Height = 21
    DataField = 'ER_NOMBRE'
    DataSource = DataSource
    TabOrder = 6
  end
  object ER_RESUMEN: TDBEdit [14]
    Left = 88
    Top = 206
    Width = 350
    Height = 21
    DataField = 'ER_RESUMEN'
    DataSource = DataSource
    TabOrder = 7
  end
  object ER_DETALLE: TDBMemo [15]
    Left = 88
    Top = 230
    Width = 350
    Height = 89
    DataField = 'ER_DETALLE'
    DataSource = DataSource
    TabOrder = 8
  end
  object ER_RESULT: TZetaDBKeyCombo [16]
    Left = 88
    Top = 322
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 9
    ListaFija = lfEntStatus
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'ER_RESULT'
    DataSource = DataSource
    LlaveNumerica = True
  end
  inherited DataSource: TDataSource
    Left = 240
    Top = 9
  end
end
