inherited EditSolicitud: TEditSolicitud
  Left = 374
  Top = 214
  Caption = 'Solicitud de Empleo'
  ClientHeight = 423
  ClientWidth = 560
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 387
    Width = 560
    inherited OK: TBitBtn
      Left = 392
    end
    inherited Cancelar: TBitBtn
      Left = 477
    end
  end
  inherited PanelSuperior: TPanel
    Width = 560
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      DataSource = nil
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 560
    inherited ValorActivo2: TPanel
      Width = 234
    end
  end
  object PanelFolio: TPanel [3]
    Left = 0
    Top = 51
    Width = 560
    Height = 34
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 14
      Top = 8
      Width = 25
      Height = 13
      Caption = 'Folio:'
    end
    object Label3: TLabel
      Left = 419
      Top = 8
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object SO_FOLIO: TZetaTextBox
      Left = 42
      Top = 6
      Width = 65
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'SO_FOLIO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object PRETTYNAME: TZetaTextBox
      Left = 114
      Top = 6
      Width = 295
      Height = 17
      AutoSize = False
      Caption = 'PRETTYNAME'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
    object SO_STATUS: TZetaDBKeyCombo
      Left = 456
      Top = 5
      Width = 100
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      ListaFija = lfSolStatus
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      DataField = 'SO_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 85
    Width = 560
    Height = 277
    ActivePage = Generales
    Align = alClient
    MultiLine = True
    TabOrder = 4
    OnChange = PageControlChange
    object Generales: TTabSheet
      Caption = 'Generales'
      object LblApePat: TLabel
        Left = 21
        Top = 8
        Width = 80
        Height = 13
        Caption = 'Apellido Paterno:'
      end
      object LblApeMat: TLabel
        Left = 19
        Top = 31
        Width = 82
        Height = 13
        Caption = 'Apellido Materno:'
      end
      object LblNombres: TLabel
        Left = 56
        Top = 54
        Width = 45
        Height = 13
        Caption = 'Nombres:'
      end
      object LblFecNac: TLabel
        Left = 12
        Top = 77
        Width = 89
        Height = 13
        Caption = 'Fecha Nacimiento:'
      end
      object LblSexo: TLabel
        Left = 74
        Top = 124
        Width = 27
        Height = 13
        Caption = 'Sexo:'
      end
      object LblEdoCivil: TLabel
        Left = 43
        Top = 147
        Width = 58
        Height = 13
        Caption = 'Estado Civil:'
      end
      object ZEdad: TZetaTextBox
        Left = 233
        Top = 75
        Width = 92
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object FotoSwitch: TSpeedButton
        Left = 343
        Top = 6
        Width = 73
        Height = 32
        Hint = 'Mostrar Foto'
        AllowAllUp = True
        GroupIndex = 1
        Down = True
        Caption = 'Mostrar Foto'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000010000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          033333FFFF77777773F330000077777770333777773FFFFFF733077777000000
          03337F3F3F777777733F0797A770003333007F737337773F3377077777778803
          30807F333333337FF73707888887880007707F3FFFF333777F37070000878807
          07807F777733337F7F3707888887880808807F333333337F7F37077777778800
          08807F333FFF337773F7088800088803308073FF777FFF733737300008000033
          33003777737777333377333080333333333333F7373333333333300803333333
          33333773733333333333088033333333333373F7F33333333333308033333333
          3333373733333333333333033333333333333373333333333333}
        Layout = blGlyphTop
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        Spacing = -1
        Visible = False
        OnClick = FotoSwitchClick
      end
      object Label6: TLabel
        Left = 15
        Top = 101
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lugar Nacimiento:'
      end
      object Label7: TLabel
        Left = 56
        Top = 193
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'C.U.R.P.:'
      end
      object Label8: TLabel
        Left = 71
        Top = 170
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'R.F.C:'
      end
      object SO_APE_PAT: TDBEdit
        Left = 105
        Top = 4
        Width = 220
        Height = 21
        DataField = 'SO_APE_PAT'
        DataSource = DataSource
        TabOrder = 0
      end
      object SO_APE_MAT: TDBEdit
        Left = 105
        Top = 27
        Width = 220
        Height = 21
        DataField = 'SO_APE_MAT'
        DataSource = DataSource
        TabOrder = 1
      end
      object SO_NOMBRES: TDBEdit
        Left = 105
        Top = 50
        Width = 220
        Height = 21
        DataField = 'SO_NOMBRES'
        DataSource = DataSource
        TabOrder = 2
      end
      object SO_FEC_NAC: TZetaDBFecha
        Left = 105
        Top = 73
        Width = 125
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 3
        Text = '18/Dec/97'
        Valor = 35782.000000000000000000
        DataField = 'SO_FEC_NAC'
        DataSource = DataSource
      end
      object SO_EDO_CIV: TZetaDBKeyCombo
        Left = 105
        Top = 143
        Width = 125
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'SO_EDO_CIV'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object FOTOGRAFIA: TPDBMultiImage
        Left = 343
        Top = 46
        Width = 195
        Height = 147
        Hint = 'Foto del Empleado'
        ImageReadRes = lAutoMatic
        ImageWriteRes = sAutoMatic
        DataEngine = deBDE
        JPegSaveQuality = 75
        JPegSaveSmooth = 0
        PNGInterLaced = True
        ImageDither = True
        UpdateAsJPG = True
        UpdateAsBMP = False
        UpdateAsGIF = False
        UpdateAsPCX = False
        UpdateAsPNG = False
        UpdateAsTIF = False
        Ctl3D = True
        DataField = 'SO_FOTO'
        DataSource = DataSource
        ParentCtl3D = False
        RichTools = False
        ReadOnly = True
        StretchRatio = True
        TabOrder = 9
        TextLeft = 0
        TextTop = 0
        TextRotate = 0
        TextTransParent = False
        TifSaveCompress = sNONE
        UseTwainWindow = False
      end
      object BarraFotografia: TToolBar
        Left = 408
        Top = 200
        Width = 130
        Height = 29
        Align = alNone
        Caption = 'BarraFotografia'
        DisabledImages = ImageListApaga
        Flat = True
        Images = ImageListPrende
        Indent = 6
        TabOrder = 10
        object tbLeerFoto: TToolButton
          Left = 6
          Top = 0
          Hint = 'Leer Fotograf'#237'a de Disco'
          Caption = 'tbLeerFoto'
          ImageIndex = 0
          ParentShowHint = False
          ShowHint = True
          OnClick = tbLeerFotoClick
        end
        object tbGrabarFoto: TToolButton
          Left = 29
          Top = 0
          Hint = 'Grabar Fotograf'#237'a a Disco'
          Caption = 'tbGrabarFoto'
          ImageIndex = 1
          ParentShowHint = False
          ShowHint = True
          OnClick = tbGrabarFotoClick
        end
        object tbBorrarFoto: TToolButton
          Left = 52
          Top = 0
          Caption = 'tbBorrarFoto'
          ImageIndex = 4
          OnClick = tbBorrarFotoClick
        end
        object tbLeerTwain: TToolButton
          Left = 75
          Top = 0
          Hint = 'Leer Dispositivo Twain'
          Caption = 'tbLeerTwain'
          ImageIndex = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = tbLeerTwainClick
        end
        object tbSelectTwain: TToolButton
          Left = 98
          Top = 0
          Hint = 'Seleccionar Dispositivo Twain'
          Caption = 'tbSelectTwain'
          ImageIndex = 3
          ParentShowHint = False
          ShowHint = True
          OnClick = tbSelectTwainClick
        end
      end
      object SO_SEXO: TZetaDBKeyCombo
        Left = 105
        Top = 120
        Width = 125
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 5
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'SO_SEXO'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object SO_PAIS: TDBEdit
        Left = 105
        Top = 97
        Width = 220
        Height = 21
        DataField = 'SO_PAIS'
        DataSource = DataSource
        TabOrder = 4
      end
      object SO_CURP: TDBEdit
        Left = 105
        Top = 189
        Width = 190
        Height = 21
        CharCase = ecUpperCase
        DataField = 'SO_CURP'
        DataSource = DataSource
        TabOrder = 8
      end
      object SO_RFC: TDBEdit
        Left = 105
        Top = 166
        Width = 121
        Height = 21
        CharCase = ecUpperCase
        DataField = 'SO_RFC'
        DataSource = DataSource
        TabOrder = 7
      end
    end
    object Direccion: TTabSheet
      Caption = 'Domicilio'
      ImageIndex = 9
      object Label9: TLabel
        Left = 140
        Top = 13
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'Calle:'
      end
      object Label10: TLabel
        Left = 128
        Top = 60
        Width = 38
        Height = 13
        Alignment = taRightJustify
        Caption = 'Colonia:'
      end
      object Label11: TLabel
        Left = 98
        Top = 104
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo Postal:'
      end
      object Label12: TLabel
        Left = 129
        Top = 82
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ciudad:'
      end
      object lblEstado: TLabel
        Left = 130
        Top = 126
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Estado:'
      end
      object Label15: TLabel
        Left = 141
        Top = 148
        Width = 25
        Height = 13
        Alignment = taRightJustify
        Caption = 'Pa'#237's:'
      end
      object Label16: TLabel
        Left = 57
        Top = 170
        Width = 109
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tiempo de Residencia:'
      end
      object Label17: TLabel
        Left = 121
        Top = 193
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tel'#233'fono:'
      end
      object Label18: TLabel
        Left = 76
        Top = 215
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Correo Electr'#243'nico:'
      end
      object Label21: TLabel
        Left = 219
        Top = 171
        Width = 24
        Height = 13
        Caption = 'A'#241'os'
      end
      object Label22: TLabel
        Left = 315
        Top = 171
        Width = 31
        Height = 13
        Caption = 'Meses'
      end
      object lblTransporte: TLabel
        Left = 112
        Top = 241
        Width = 54
        Height = 13
        Alignment = taRightJustify
        Caption = 'Transporte:'
      end
      object Label5: TLabel
        Left = 118
        Top = 34
        Width = 48
        Height = 13
        Caption = '# Exterior:'
      end
      object Label13: TLabel
        Left = 254
        Top = 34
        Width = 45
        Height = 13
        Caption = '# Interior:'
      end
      object SO_CALLE: TDBEdit
        Left = 170
        Top = 9
        Width = 310
        Height = 21
        DataField = 'SO_CALLE'
        DataSource = DataSource
        TabOrder = 0
      end
      object SO_COLONIA: TDBEdit
        Left = 170
        Top = 56
        Width = 220
        Height = 21
        DataField = 'SO_COLONIA'
        DataSource = DataSource
        TabOrder = 3
      end
      object SO_CODPOST: TDBEdit
        Left = 170
        Top = 100
        Width = 80
        Height = 21
        DataField = 'SO_CODPOST'
        DataSource = DataSource
        MaxLength = 8
        TabOrder = 5
      end
      object SO_CIUDAD: TDBEdit
        Left = 170
        Top = 78
        Width = 220
        Height = 21
        DataField = 'SO_CIUDAD'
        DataSource = DataSource
        TabOrder = 4
      end
      object SO_ESTADO: TZetaDBKeyLookup
        Left = 170
        Top = 122
        Width = 310
        Height = 21
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'SO_ESTADO'
        DataSource = DataSource
      end
      object SO_NACION: TDBEdit
        Left = 170
        Top = 144
        Width = 220
        Height = 21
        DataField = 'SO_NACION'
        DataSource = DataSource
        TabOrder = 7
      end
      object SO_TEL: TDBEdit
        Left = 170
        Top = 189
        Width = 220
        Height = 21
        DataField = 'SO_TEL'
        DataSource = DataSource
        TabOrder = 10
      end
      object SO_EMAIL: TDBEdit
        Left = 170
        Top = 211
        Width = 220
        Height = 21
        DataField = 'SO_EMAIL'
        DataSource = DataSource
        TabOrder = 11
      end
      object ZYear: TZetaNumero
        Left = 170
        Top = 167
        Width = 45
        Height = 21
        Mascara = mnDias
        TabOrder = 8
        Text = '0'
        OnExit = ZYearExit
      end
      object ZMonth: TZetaNumero
        Left = 251
        Top = 167
        Width = 45
        Height = 21
        Mascara = mnDiasFraccion
        TabOrder = 9
        Text = '0.00'
        OnExit = ZMonthExit
      end
      object SO_MED_TRA: TZetaDBKeyLookup
        Left = 170
        Top = 233
        Width = 310
        Height = 21
        TabOrder = 12
        TabStop = True
        WidthLlave = 60
        DataField = 'SO_MED_TRA'
        DataSource = DataSource
      end
      object SO_NUM_EXT: TDBEdit
        Left = 170
        Top = 31
        Width = 80
        Height = 21
        DataField = 'SO_NUM_EXT'
        DataSource = DataSource
        TabOrder = 1
      end
      object SO_NUM_INT: TDBEdit
        Left = 301
        Top = 31
        Width = 80
        Height = 21
        DataField = 'SO_NUM_INT'
        DataSource = DataSource
        TabOrder = 2
      end
    end
    object Educacion: TTabSheet
      Caption = 'Educaci'#243'n'
      ImageIndex = 10
      object LblEscuela: TLabel
        Left = 142
        Top = 31
        Width = 58
        Height = 13
        Caption = 'Escolaridad:'
      end
      object LblIngles: TLabel
        Left = 128
        Top = 79
        Width = 72
        Height = 13
        Caption = 'Dominio Ingl'#233's:'
      end
      object Label2: TLabel
        Left = 245
        Top = 79
        Width = 8
        Height = 13
        Caption = '%'
      end
      object LblIdioma: TLabel
        Left = 143
        Top = 103
        Width = 57
        Height = 13
        Caption = 'Otro Idioma:'
      end
      object LblDominio: TLabel
        Left = 159
        Top = 127
        Width = 41
        Height = 13
        Caption = 'Dominio:'
      end
      object Label4: TLabel
        Left = 245
        Top = 127
        Width = 8
        Height = 13
        Caption = '%'
      end
      object Label20: TLabel
        Left = 32
        Top = 55
        Width = 168
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nombre de la Instituci'#243'n Educativa:'
      end
      object SO_ESTUDIO: TZetaDBKeyCombo
        Left = 204
        Top = 27
        Width = 125
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 0
        ListaFija = lfNinguna
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'SO_ESTUDIO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object SO_INGLES: TZetaDBNumero
        Left = 204
        Top = 75
        Width = 37
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 2
        DataField = 'SO_INGLES'
        DataSource = DataSource
      end
      object SO_IDIOMA2: TDBEdit
        Left = 204
        Top = 99
        Width = 125
        Height = 21
        DataField = 'SO_IDIOMA2'
        DataSource = DataSource
        TabOrder = 3
      end
      object SO_DOMINA2: TZetaDBNumero
        Left = 204
        Top = 123
        Width = 37
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 4
        DataField = 'SO_DOMINA2'
        DataSource = DataSource
      end
      object SO_ESCUELA: TDBEdit
        Left = 204
        Top = 51
        Width = 220
        Height = 21
        DataField = 'SO_ESCUELA'
        DataSource = DataSource
        TabOrder = 1
      end
    end
    object Solicitud: TTabSheet
      Caption = 'Solicitud'
      ImageIndex = 1
      object LblFecSolicitud: TLabel
        Left = 28
        Top = 8
        Width = 91
        Height = 13
        Caption = 'Fecha de Solicitud:'
      end
      object LblPuestoSolic: TLabel
        Left = 34
        Top = 31
        Width = 85
        Height = 13
        Caption = 'Puesto Solicitado:'
      end
      object GBAreas: TGroupBox
        Left = 23
        Top = 60
        Width = 466
        Height = 117
        Caption = 'Areas de Inter'#233's'
        TabOrder = 2
        object SO_AREA_1: TZetaDBKeyLookup
          Left = 20
          Top = 20
          Width = 430
          Height = 21
          LookupDataset = dmSeleccion.cdsAreaInteres
          TabOrder = 0
          TabStop = True
          WidthLlave = 80
          DataField = 'SO_AREA_1'
          DataSource = DataSource
        end
        object SO_AREA_2: TZetaDBKeyLookup
          Left = 20
          Top = 48
          Width = 430
          Height = 21
          LookupDataset = dmSeleccion.cdsAreaInteres
          TabOrder = 1
          TabStop = True
          WidthLlave = 80
          DataField = 'SO_AREA_2'
          DataSource = DataSource
        end
        object SO_AREA_3: TZetaDBKeyLookup
          Left = 20
          Top = 78
          Width = 430
          Height = 21
          LookupDataset = dmSeleccion.cdsAreaInteres
          TabOrder = 2
          TabStop = True
          WidthLlave = 80
          DataField = 'SO_AREA_3'
          DataSource = DataSource
        end
      end
      object SO_FEC_INI: TZetaDBFecha
        Left = 124
        Top = 4
        Width = 125
        Height = 22
        Cursor = crArrow
        Opcional = True
        TabOrder = 0
        Text = '18/Dec/97'
        Valor = 35782.000000000000000000
        DataField = 'SO_FEC_INI'
        DataSource = DataSource
      end
      object SO_PUE_SOL: TDBEdit
        Left = 124
        Top = 27
        Width = 322
        Height = 21
        DataField = 'SO_PUE_SOL'
        DataSource = DataSource
        TabOrder = 1
      end
    end
    object Adicionales: TTabSheet
      Caption = 'Adicionales'
      ImageIndex = 5
      object AdicionalScrollBox: TScrollBox
        Left = 0
        Top = 0
        Width = 554
        Height = 233
        HorzScrollBar.Visible = False
        Align = alClient
        BorderStyle = bsNone
        TabOrder = 0
      end
    end
    object Requisitos: TTabSheet
      Caption = 'Requisitos'
      ImageIndex = 6
      object SO_REQUI_1lbl: TLabel
        Left = 467
        Top = 16
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_1lbl'
      end
      object SO_REQUI_2lbl: TLabel
        Left = 467
        Top = 39
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_2lbl'
      end
      object SO_REQUI_3lbl: TLabel
        Left = 467
        Top = 62
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_3lbl'
      end
      object SO_REQUI_4lbl: TLabel
        Left = 467
        Top = 86
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_4lbl'
      end
      object SO_REQUI_5lbl: TLabel
        Left = 467
        Top = 110
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_5lbl'
      end
      object SO_REQUI_6lbl: TLabel
        Left = 467
        Top = 133
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_6lbl'
      end
      object SO_REQUI_7lbl: TLabel
        Left = 467
        Top = 156
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_7lbl'
      end
      object SO_REQUI_8lbl: TLabel
        Left = 467
        Top = 180
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_8lbl'
      end
      object SO_REQUI_9lbl: TLabel
        Left = 467
        Top = 203
        Width = 77
        Height = 13
        Caption = 'SO_REQUI_9lbl'
      end
      object SO_REQUI_1: TDBCheckBox
        Left = 448
        Top = 14
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_1'
        DataSource = DataSource
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_2: TDBCheckBox
        Left = 448
        Top = 37
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_2'
        DataSource = DataSource
        TabOrder = 1
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_3: TDBCheckBox
        Left = 448
        Top = 60
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_3'
        DataSource = DataSource
        TabOrder = 2
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_4: TDBCheckBox
        Left = 448
        Top = 84
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_4'
        DataSource = DataSource
        TabOrder = 3
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_8: TDBCheckBox
        Left = 448
        Top = 178
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_8'
        DataSource = DataSource
        TabOrder = 7
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_7: TDBCheckBox
        Left = 448
        Top = 154
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_7'
        DataSource = DataSource
        TabOrder = 6
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_6: TDBCheckBox
        Left = 448
        Top = 131
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_6'
        DataSource = DataSource
        TabOrder = 5
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_5: TDBCheckBox
        Left = 448
        Top = 108
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_5'
        DataSource = DataSource
        TabOrder = 4
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object SO_REQUI_9: TDBCheckBox
        Left = 448
        Top = 201
        Width = 16
        Height = 17
        Alignment = taLeftJustify
        DataField = 'SO_REQUI_9'
        DataSource = DataSource
        TabOrder = 8
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object DocEntregadaGb: TGroupBox
        Left = 160
        Top = 0
        Width = 233
        Height = 33
        Caption = 'Documentaci'#243'n entregada'
        TabOrder = 9
      end
      object OtrosRequisitosGb: TGroupBox
        Left = 160
        Top = 46
        Width = 233
        Height = 33
        Caption = 'Otros requisitos'
        TabOrder = 10
      end
    end
    object Referencias: TTabSheet
      Caption = 'Referencias'
      ImageIndex = 7
      object PanelReferencia: TPanel
        Left = 0
        Top = 0
        Width = 552
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BtnReferencias: TSpeedButton
          Left = 4
          Top = 3
          Width = 135
          Height = 24
          Hint = 'Modificar Referencia Seleccionada'
          Caption = 'Modificar Referencia'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnReferenciasClick
        end
      end
      object GridReferencias: TZetaDBGrid
        Left = 0
        Top = 30
        Width = 552
        Height = 201
        Align = alClient
        DataSource = dsRefLaboral
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = BtnReferenciasClick
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'RL_FOLIO'
            Title.Alignment = taCenter
            Title.Caption = '#'
            Width = 25
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO1'
            Title.Caption = 'Campo1'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO2'
            Title.Caption = 'Campo2'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO3'
            Title.Caption = 'Campo3'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO4'
            Title.Caption = 'Campo4'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO5'
            Title.Caption = 'Campo5'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO6'
            Title.Caption = 'Campo6'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO7'
            Title.Caption = 'Campo7'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO8'
            Title.Caption = 'Campo8'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO9'
            Title.Caption = 'Campo9'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO10'
            Title.Caption = 'Campo10'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO11'
            Title.Caption = 'Campo11'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO12'
            Title.Caption = 'Campo12'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO13'
            Title.Caption = 'Campo13'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO14'
            Title.Caption = 'Campo14'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO15'
            Title.Caption = 'Campo15'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO16'
            Title.Caption = 'Campo16'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO17'
            Title.Caption = 'Campo17'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO18'
            Title.Caption = 'Campo18'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO19'
            Title.Caption = 'Campo19'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RL_CAMPO20'
            Title.Caption = 'Campo20'
            Width = 100
            Visible = True
          end>
      end
    end
    object Examenes: TTabSheet
      Caption = 'Ex'#225'menes'
      ImageIndex = 3
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 552
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object btnAgregarExamen: TSpeedButton
          Left = 4
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Agregar Examen'
          Caption = 'Agregar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnAgregarExamenClick
        end
        object btnBorrarExamen: TSpeedButton
          Left = 86
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Borrar Examen'
          Caption = 'Borrar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnBorrarExamenClick
        end
        object btnModificarExamen: TSpeedButton
          Left = 168
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Modificar Examen'
          Caption = 'Modificar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = btnModificarExamenClick
        end
      end
      object GridExamenes: TZetaDBGrid
        Left = 0
        Top = 30
        Width = 552
        Height = 201
        Align = alClient
        DataSource = dsExamenes
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = btnModificarExamenClick
        Columns = <
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'EX_FOLIO'
            Title.Alignment = taCenter
            Title.Caption = '#'
            Width = 25
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'EX_FECHA'
            Title.Caption = 'Fecha'
            Width = 75
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'EX_TIPO'
            Title.Caption = 'Tipo'
            Width = 85
            Visible = True
          end
          item
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'EX_RESUMEN'
            Title.Caption = 'Resumen'
            Width = 150
            Visible = True
          end
          item
            Alignment = taCenter
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'EX_APROBO'
            Title.Alignment = taCenter
            Title.Caption = 'Aprob'#243
            Width = 40
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'EX_EVALUA1'
            Title.Alignment = taCenter
            Title.Caption = 'Evaluaci'#243'n'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'EX_APLICO'
            Title.Caption = 'Evaluador'
            Width = 125
            Visible = True
          end>
      end
    end
    object Documentos: TTabSheet
      Caption = 'Documentos'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 552
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object SpeedButton1: TSpeedButton
          Left = 8
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Agregar Documento'
          Caption = 'Agregar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton1Click
        end
        object SpeedButton2: TSpeedButton
          Left = 90
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Borrar Documento'
          Caption = 'Borrar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = SpeedButton2Click
        end
        object bDocumento: TSpeedButton
          Left = 171
          Top = 3
          Width = 102
          Height = 24
          Hint = 'Ver Documento'
          Caption = '&Ver Documento'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333FF3333333333333C0C333333333333F777F3333333333CC0F0C3
            333333333777377F33333333C30F0F0C333333337F737377F333333C00FFF0F0
            C33333F7773337377F333CC0FFFFFF0F0C3337773F33337377F3C30F0FFFFFF0
            F0C37F7373F33337377F00FFF0FFFFFF0F0C7733373F333373770FFFFF0FFFFF
            F0F073F33373F333373730FFFFF0FFFFFF03373F33373F333F73330FFFFF0FFF
            00333373F33373FF77333330FFFFF000333333373F333777333333330FFF0333
            3333333373FF7333333333333000333333333333377733333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bDocumentoClick
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 30
        Width = 552
        Height = 201
        Align = alClient
        DataSource = dsDocumentos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = bDocumentoClick
        Columns = <
          item
            Expanded = False
            FieldName = 'DO_TIPO'
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DO_OBSERVA'
            ReadOnly = True
            Title.Caption = 'Observaciones'
            Width = 245
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DO_TIPO_DOC'
            ReadOnly = True
            Title.Caption = 'Tipo'
            Width = 182
            Visible = True
          end>
      end
    end
    object Candidaturas: TTabSheet
      Caption = 'Candidaturas'
      ImageIndex = 4
      object ZetaDBGrid1: TZetaDBGrid
        Left = 0
        Top = 0
        Width = 554
        Height = 233
        Align = alClient
        DataSource = dsCandidaturas
        Options = [dgTitles, dgColumnResize, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Alignment = taCenter
            ButtonStyle = cbsNone
            Expanded = False
            FieldName = 'RQ_FOLIO'
            Title.Alignment = taCenter
            Title.Caption = 'Folio'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RQ_FEC_INI'
            Title.Caption = 'Fecha'
            Width = 80
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PU_DESCRIP'
            Title.Caption = 'Puesto'
            Width = 235
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'RQ_VACANTE'
            Title.Alignment = taCenter
            Title.Caption = 'Vacantes'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_STATUS'
            Title.Caption = 'Status'
            Width = 90
            Visible = True
          end>
      end
    end
    object Curriculum: TTabSheet
      Caption = 'Notas'
      ImageIndex = 2
      object SO_OBSERVA: TDBMemo
        Left = 0
        Top = 0
        Width = 554
        Height = 233
        Align = alClient
        DataField = 'SO_OBSERVA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 9
  end
  object ImageListPrende: TImageList
    Top = 314
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000808080008080
      8000808080008080800080808000000000008080800000000000000000000000
      FF000000FF000000FF0000000000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF0000FFFF000000000000FFFF00FFFFFF000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000000000FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF00000000000000FFFF00FFFFFF0000FFFF000000000000FFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF00000000000000FFFF00FFFFFF0000FFFF00FFFFFF000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      000000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFF000000000000000000000000000000000000000000000000
      0000C0C0C0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B000000
      00007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      00007B7B7B007B7B7B007B7B7B0000FFFF0000FFFF007B7B7B007B7B7B007B7B
      7B007B7B7B0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBDBD000000
      0000BDBDBD00BDBDBD0000000000000000000000000000000000000000000000
      000000000000000000007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600000000000000000000000000000000000000
      00000000000000000000C6C6C60000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BDBDBD00BDBD
      BD00BDBDBD00BDBDBD000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000000000000000000000
      0000848484000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B000000FF007B7B
      7B0000FF00007B7B7B007B7B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00BDBDBD00BDBDBD00000000000000
      00000000000000000000BDBDBD000000000000000000BD000000BD000000BD00
      0000BD00000000000000C6C6C60000000000000000000000000000000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF00000000000000000000000000FFFF
      FF00000000007B7B7B00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B7B7B00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD007B7B7B00BDBDBD00BDBDBD00000000000000
      0000000000007B7B7B007B7B7B000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF0000000000C6C6C60000000000000000000000000000000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000007B7B7B00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000000000007B7B7B00000000000000
      00000000000000000000BDBDBD007B7B7B00BDBDBD00BDBDBD00000000007B7B
      7B00000000007B7B7B00BDBDBD000000000000000000BD000000FFFFFF008484
      8400848484000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF000000
      0000FFFFFF0000000000000000000000000000000000FFFFFF0000000000FFFF
      FF000000000000FFFF0000FFFF000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000BDBDBD0000000000FF000000FF000000FF00
      00000000FF00FF000000FF00000000000000000000007B7B7B00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD007B7B7B00BDBDBD00BDBDBD0000000000BDBD
      BD0000000000BDBDBD00BDBDBD000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000C6C6C6000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000FFFF0000FFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      FF000000FF000000FF000000000000000000000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00BDBDBD00BDBDBD00000000000000
      000000000000BDBDBD00BDBDBD000000000000000000BD000000FFFFFF008484
      840084848400FFFFFF008484840000000000000000008484840084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF000000000000000000FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00000000000000
      00000000000000000000FFFFFF0000000000FFFFFF00000000000000FF000000
      FF000000FF000000FF000000FF000000000000000000BDBDBD00BDBDBD00BDBD
      BD00000000000000000000000000BDBDBD00BDBDBD00BDBDBD00000000000000
      00000000000000000000BDBDBD000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      000000000000BDBDBD0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000FFFFFF008484
      840084848400FFFFFF008484840000000000000000008484840084848400FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000000000BDBDBD00FFFFFF0000000000FFFFFF000000000000FF
      FF000000000000000000000000000000000000000000FFFFFF00000000000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000BDBDBD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BD0000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      FF0000FFFF0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF000000000000000000000000000000
      FF000000FF000000FF000000000000000000000000000000000000000000BDBD
      BD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD000000BD000000BD000000BD000000BD00
      0000BD000000000000000000000000000000000000000000000000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FFFF0000FFFF00000000000000000000000000FFFFFF0000000000BDBD
      BD00FFFFFF0000000000FFFFFF000000000000000000000000007B7B7B000000
      FF000000FF000000FF00000000000000000000000000BDBDBD00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000C6C6C600BD00
      0000BD000000C6C6C600BD000000BD000000C6C6C600BD000000BD000000C6C6
      C600BD0000000000000000000000000000000000000000FFFF0000FFFF000000
      000000000000000000000000000000FFFF0000FFFF0000000000000000000000
      00000000000000FFFF0000FFFF000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000FF000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD000000BD000000BD00
      0000BD000000BD000000BD000000BD000000BD000000BD000000BD000000BD00
      0000BD00000000000000000000000000000000FFFF0000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      000000000000000000000000000000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF0000000000000162000000000000
      FFE3000000000000FE63000000000000FC03000000000000F803000000000000
      F003000000000000F003000000000000E003000000000000C003000000000000
      80030000000000000003000000000000000300000000000001E3000000000000
      83F7000000000000C7F7000000000000FF7EFF00FC07F8F89001FF008003F8F8
      C003FF000007F870E003FF00003CF800E003000000188000E003000000008000
      E00300000000800000010000000080018000002300008003E007000100188003
      E00F0000803C8003E00F0023E3FF8007E027006387FF8007C07300C30FFF8007
      9E7901078FFF80077EFE03FFDFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object OpenPictureDialog: TOpenPictureDialog
    DefaultExt = 'JPG'
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Title = 'Leer Fotograf'#237'a de Disco'
    Top = 282
  end
  object SavePictureDialog: TSavePictureDialog
    Filter = 
      'JPEG Image File (*.jpg)|*.jpg|JPEG Image File (*.jpeg)|*.jpeg|Bi' +
      'tmaps (*.bmp)|*.bmp|GIF Image File (*.gif)|*.gif|TIF Image File ' +
      '(*.tif)|*.tif|PCX Image File (*.pcx)|*.pcx|PNG Image File (*.png' +
      ')|*.png'
    FilterIndex = 0
    Title = 'Grabar Fotograf'#237'a a Disco'
    Top = 348
  end
  object ImageListApaga: TImageList
    Top = 244
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00000000000000000000000000000000007B7B7B00FFFFFF00000000000000
      00000000000000000000FFFFFF007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B0000000000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B007B7B7B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B7B7B007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B7B7B007B7B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B007B7B7B00FFFFFF007B7B
      7B0000000000000000007B7B7B007B7B7B00000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B007B7B7B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B7B7B007B7B7B007B7B7B00FFFFFF0000000000FFFF
      FF0000000000FFFFFF007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B000000000000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF000000000000000000000000000000000000000000000000000000
      00007B7B7B00FFFFFF00000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00FFFFFF007B7B7B000000
      00007B7B7B0000000000000000007B7B7B007B7B7B007B7B7B0000000000FFFF
      FF0000000000000000007B7B7B007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF007B7B7B00FFFFFF0000000000000000007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000007B7B7B00FFFF
      FF00FFFFFF007B7B7B00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF007B7B7B007B7B7B00000000007B7B7B007B7B7B007B7B7B000000
      00007B7B7B00FFFFFF0000000000000000007B7B7B00FFFFFF00000000000000
      0000000000000000000000000000000000007B7B7B00FFFFFF00000000000000
      00000000000000000000000000007B7B7B007B7B7B00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000007B7B7B007B7B
      7B007B7B7B00FFFFFF00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFFFF007B7B
      7B00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF007B7B7B00FFFFFF00FFFFFF00FFFFFF007B7B7B00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B7B7B007B7B7B00FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B00000000000000000000000000000000007B7B7B00FFFF
      FF007B7B7B00FFFFFF00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B007B7B7B007B7B7B007B7B
      7B00FFFFFF007B7B7B007B7B7B007B7B7B007B7B7B00000000007B7B7B000000
      00007B7B7B007B7B7B007B7B7B00FFFFFF007B7B7B00FFFFFF007B7B7B007B7B
      7B00000000007B7B7B007B7B7B00000000007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000007B7B7B00FFFF
      FF007B7B7B00FFFFFF00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B007B7B7B007B7B
      7B00FFFFFF0000000000FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFF
      FF007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF007B7B7B00FFFFFF007B7B
      7B007B7B7B007B7B7B00FFFFFF00000000007B7B7B00FFFFFF00000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF0000000000000000007B7B7B007B7B
      7B007B7B7B0000000000FFFFFF007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF007B7B7B007B7B7B00000000007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B000000000000000000000000007B7B7B00FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B00000000007B7B7B00000000007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B00FFFFFF007B7B7B0000000000FFFFFF00FFFF
      FF007B7B7B007B7B7B007B7B7B00FFFFFF00FFFFFF00FFFFFF007B7B7B000000
      0000000000007B7B7B00000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF0000000000FFFFFF00FFFFFF007B7B7B00FFFFFF00000000007B7B
      7B00FFFFFF000000000000000000000000007B7B7B00FFFFFF0000000000FFFF
      FF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00000000007B7B7B007B7B7B007B7B
      7B007B7B7B00000000007B7B7B007B7B7B007B7B7B007B7B7B00000000000000
      000000000000000000007B7B7B007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF007B7B7B007B7B7B00000000007B7B7B00FFFFFF007B7B7B007B7B
      7B00FFFFFF00FFFFFF0000000000000000007B7B7B00FFFFFF007B7B7B007B7B
      7B00000000007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00000000007B7B
      7B007B7B7B007B7B7B00FFFFFF00000000000000000000000000FFFFFF007B7B
      7B00000000007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B7B7B007B7B7B00000000007B7B
      7B007B7B7B00FFFFFF00FFFFFF00000000007B7B7B00FFFFFF0000000000FFFF
      FF00FFFFFF007B7B7B00FFFFFF00000000007B7B7B0000000000000000007B7B
      7B007B7B7B007B7B7B00FFFFFF0000000000000000007B7B7B007B7B7B000000
      00007B7B7B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00FFFFFF00000000000000
      00007B7B7B007B7B7B00FFFFFF00FFFFFF007B7B7B00FFFFFF007B7B7B007B7B
      7B00000000007B7B7B00FFFFFF007B7B7B0000000000FFFFFF007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000007B7B7B0000000000FFFFFF007B7B
      7B00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007B7B7B007B7B7B000000
      00000000000000000000000000007B7B7B007B7B7B0000000000000000000000
      0000000000007B7B7B007B7B7B00000000007B7B7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B7B7B007B7B7B00000000007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B00000000000000000000000000000000007B7B7B00000000007B7B
      7B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B0000000000000000000000
      00000000000000000000000000007B7B7B000000000000000000000000000000
      00000000000000000000000000007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00CF3CFF00C00500008001FF0C82030000
      C003FF0028060000E7F38000162C0000E48300003FC20000E1133F3E21C20000
      8420240003C20000005009003FC200008480210138C40000E1070280401A0000
      E4272400843C0000E1030821CBFF0000E021216197FF0000C030088347FF0000
      9E790107AFFF00007EFE03FFDFFF000000000000000000000000000000000000
      000000000000}
  end
  object dsExamenes: TDataSource
    Left = 440
    Top = 24
  end
  object dsCandidaturas: TDataSource
    Left = 456
    Top = 40
  end
  object dsGridSolicitud: TDataSource
    OnDataChange = dsGridSolicitudDataChange
    Left = 472
    Top = 56
  end
  object dsRefLaboral: TDataSource
    Left = 488
    Top = 72
  end
  object dsDocumentos: TDataSource
    Left = 504
    Top = 88
  end
end
