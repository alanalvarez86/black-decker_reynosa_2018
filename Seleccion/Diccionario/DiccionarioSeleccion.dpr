program DiccionarioSeleccion;

uses
  Forms,
  FEditarDiccionario in '..\..\Configuracion\Diccionario\FEditarDiccionario.pas' {EditarDiccionario},
  DEditarDiccionario in '..\..\Configuracion\Diccionario\DEditarDiccionario.pas' {dmDiccionario: TDataModule},
  ZReportConst in '..\..\Reportes\ZReportConst.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TEditarDiccionario, EditarDiccionario);
  Application.Run;
end.
