unit FEditRequisicion;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaKeyLookup,
  ZetaKeyCombo, ZetaDBTextBox, Mask, ZetaNumero, ComCtrls, ZetaFecha,
  Grids, DBGrids, ZetaDBGrid, ZetaCommonClasses, ZetaSmartLists;

type
  TEditRequisicion = class(TBaseEdicion)
    PanelHeader: TPanel;
    dsCandidatos: TDataSource;
    dsGastos: TDataSource;
    PageControl: TPageControl;
    Generales: TTabSheet;
    Label18: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    PresupuestoLbl: TLabel;
    RQ_TURNO: TDBEdit;
    RQ_MOTIVO: TZetaDBKeyCombo;
    RQ_CLIENTE: TZetaDBKeyLookup;
    RQ_GTOPRES: TZetaDBNumero;
    Proceso: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    RQ_FEC_FIN: TZetaDBTextBox;
    RQ_FEC_INI: TZetaDBFecha;
    RQ_FEC_PRO: TZetaDBFecha;
    Perfil: TTabSheet;
    GBEdad: TGroupBox;
    Label8: TLabel;
    Label11: TLabel;
    Label10: TLabel;
    Label29: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label15: TLabel;
    RQ_EDADMIN: TZetaDBNumero;
    RQ_EDADMAX: TZetaDBNumero;
    RQ_SEXO: TZetaDBKeyCombo;
    RQ_EST_MIN: TZetaDBKeyCombo;
    RQ_INGLES: TZetaDBNumero;
    LblIngles: TLabel;
    RQ_AREA: TZetaDBKeyLookup;
    QU_CODIGO: TZetaDBKeyLookup;
    Candidatos: TTabSheet;
    Panel3: TPanel;
    BtnAgregarCandidato: TSpeedButton;
    BtnBorrarCandidato: TSpeedButton;
    BtnModificarCandidato: TSpeedButton;
    BtnVerSolicitud: TBitBtn;
    GridCandidatos: TZetaDBGrid;
    Puesto: TTabSheet;
    GroupBox3: TGroupBox;
    PU_HABILID: TMemo;
    GroupBox4: TGroupBox;
    PU_ACTIVID: TMemo;
    Oferta: TTabSheet;
    RQ_OFERTA: TDBMemo;
    Observaciones: TTabSheet;
    RQ_OBSERVA: TDBMemo;
    Anuncio: TTabSheet;
    RQ_ANUNCIO: TDBMemo;
    Gastos: TTabSheet;
    Panel2: TPanel;
    Label26: TLabel;
    RQ_GTOPRES2: TZetaDBTextBox;
    Label27: TLabel;
    BtnAgregarGasto: TSpeedButton;
    BtnBorrarGasto: TSpeedButton;
    BtnModificarGasto: TSpeedButton;
    GridGastos: TZetaDBGrid;
    eGastoTotal: TZetaTextBox;
    Panel1: TPanel;
    BRedactarAnuncio: TBitBtn;
    Label1: TLabel;
    eMailContacto: TEdit;
    BtnEnviar: TSpeedButton;
    Panel4: TPanel;
    Label13: TLabel;
    RQ_SUELDO: TDBEdit;
    RQ_VACACI: TDBEdit;
    Label14: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    RQ_BONO: TDBEdit;
    RQ_VALES: TDBEdit;
    Label17: TLabel;
    RQ_AGUINAL: TDBEdit;
    PanelFolio: TPanel;
    LblFolio: TLabel;
    RQ_FOLIO: TZetaDBTextBox;
    PanelOtros: TPanel;
    Label2: TLabel;
    RQ_PUESTO: TZetaDBKeyLookup;
    RQ_STATUS: TZetaDBKeyCombo;
    RQ_PRIORID: TZetaDBKeyCombo;
    Label4: TLabel;
    RQ_CONTRAT: TZetaTextBox;
    Label16: TLabel;
    RQ_CANDIDAT: TZetaTextBox;
    Label28: TLabel;
    RQ_VACANTE: TZetaDBNumero;
    Label5: TLabel;
    Label3: TLabel;
    lblContratos: TLabel;
    RQ_TIPO_CO: TZetaDBKeyLookup;
    ReemplazaLbl: TLabel;{OP:11.Abr.08}
    RQ_REEMPLA: TDBEdit;{OP:11.Abr.08}
    GBAutorizo: TGroupBox;{OP:14.Abr.08}
    Label12: TLabel;{OP:14.Abr.08}
    Label32: TLabel;{OP:14.Abr.08}
    RQ_AUTORIZ: TDBEdit;{OP:14.Abr.08}
    RQ_PUES_AU: TDBEdit;{OP:14.Abr.08}
    Label19: TLabel;{OP:14.Abr.08}
    RQ_HABILID: TDBMemo;{OP:14.Abr.08}
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtnAgregarCandidatoClick(Sender: TObject);
    procedure BtnBorrarCandidatoClick(Sender: TObject);
    procedure BtnModificarCandidatoClick(Sender: TObject);
    procedure BtnAgregarGastoClick(Sender: TObject);
    procedure BtnBorrarGastoClick(Sender: TObject);
    procedure BtnModificarGastoClick(Sender: TObject);
    procedure RQ_PUESTOValidLookup(Sender: TObject);
    procedure BtnVerSolicitudClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BRedactarAnuncioClick(Sender: TObject);
    procedure BtnEnviarClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure RQ_MOTIVOChange(Sender: TObject);
    procedure RQ_CLIENTEValidLookup(Sender: TObject);
  private
    FParametros: TZetaParams;
    FCalculando : Boolean;
    procedure CalculaTotalesRequisicion(const lActualizaStatus : Boolean = TRUE);
    procedure CalculaGastoTotal;
    procedure RedactaAnuncio;
    procedure VerificaReemplazo;{OP:11.Abr.08}
    procedure ActivaAutorizo;{OP:14.Abr.08}
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure DisConnect; override;
    procedure HabilitaControles; override;
    procedure EscribirCambios;override;
  public
    { Public declarations }
  end;

const
     K_ACCESO_CANDIDATO = 'No Tiene Permiso Para %s Candidatos';
     K_ACCESO_GASTOS = 'No Tiene Permiso Para %s Gastos';
     K_ALTURA_HEADER = 80;
var
  EditRequisicion: TEditRequisicion;

implementation

uses dSeleccion,
     DGlobal,
     ZGlobalTress,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZetaDialogo;

{$R *.DFM}

procedure TEditRequisicion.FormCreate(Sender: TObject);
var
   eSex: eSexo;
begin
     FParametros := TZetaParams.Create;
     FirstControl := RQ_PUESTO;
     with RQ_SEXO.Lista do
     begin
          BeginUpdate;
          try
             Clear;
             Add( Format( '%s=%s', [ 'I', '<Cualquiera>' ] ) );
             for eSex := Low( eSexo ) to High( eSexo ) do
             begin
                  Add( Format( '%s=%s', [ ZetaCommonLists.ObtieneElemento( lfSexo, Ord( eSex ) ), ZetaCommonLists.ObtieneElemento( lfSexoDesc, Ord( eSex ) ) ] ) );
             end;
          finally
                 EndUpdate;
          end;
     end;
     RQ_TIPO_CO.LookupDataset := dmSeleccion.cdsContratos;
     inherited;
     IndexDerechos := D_REQUISICION_DATOS;
     HelpContext := H00051_Requisicion_General;
end;

procedure TEditRequisicion.FormDestroy(Sender: TObject);
begin
     inherited;
     FreeAndNil( FParametros );
end;

procedure TEditRequisicion.FormShow(Sender: TObject);
begin
     PageControl.ActivePage := Generales;
     eMailContacto.Text := Global.GetGlobalString(K_GLOBAL_EMAIL_ANUNCIO);
     inherited;
     PanelFolio.Visible := ( NOT Inserting );
     if PanelFolio.Visible then
        PanelHeader.Height := K_ALTURA_HEADER
     else
        PanelHeader.Height := K_ALTURA_HEADER - PanelFolio.Height;


     RQ_GTOPRES.Visible := ZAccesosMgr.Revisa( D_REQUISICION_PRESUPUESTO );{OP:11.Abr.08}
     PresupuestoLbl.Visible := RQ_GTOPRES.Visible;{OP:21.Abr.08}
     ActivaAutorizo;{OP:14.Abr.08}
end;

procedure TEditRequisicion.Connect;
begin
     with dmSeleccion do
     begin
          RQ_AREA.LookUpDataSet := cdsAreaInteres;
          RQ_PUESTO.LookUpDataSet := cdsPuestos;
          RQ_CLIENTE.LookUpDataSet := cdsCliente;
          QU_CODIGO.LookUpDataSet := cdsCondiciones;
          cdsAreaInteres.Conectar;
          cdsTipoGasto.Conectar;
          cdsPuestos.Conectar;
          cdsCliente.Conectar;
          cdsCondiciones.Conectar;
          DataSource.DataSet:= cdsEditRequiere;
          dsGastos.DataSet:= cdsReqGastos;
          dsCandidatos.DataSet:= cdsReqCandidatos;

          {Temporal, porque el ComboBox queda en blanco}
          if StrVacio(cdsEditRequiere.FieldByName('RQ_SEXO').AsString) then
             RQ_SEXO.ItemIndex := 0;
          if SetControlesEmpresaTress( lblContratos, RQ_TIPO_CO ) then
             cdsContratos.Conectar;
     end;
     CalculaGastoTotal;
     CalculaTotalesRequisicion(FALSE);
     RQ_PUESTOValidLookup( self );
     VerificaReemplazo;{OP:11.Abr.08}     
end;

procedure TEditRequisicion.DisConnect;
begin
     DataSource.DataSet:= NIL;
     dsGastos.DataSet:= NIL;
     dsCandidatos.DataSet:= NIL;
end;

procedure TEditRequisicion.HabilitaControles;
begin
     inherited;
     Candidatos.TabVisible := ZAccesosMgr.Revisa( D_CANDIDATOS ) and ( NOT Inserting );
     Gastos.TabVisible := ZAccesosMgr.Revisa( D_REQUISICION_GASTOS ) and ( NOT Inserting );
     Oferta.TabVisible := ZAccesosMgr.Revisa( D_REQUISICION_OFERTA ) and ( NOT Inserting );{OP:11.Abr.08}
     Anuncio.TabVisible := ZAccesosMgr.Revisa( D_REQUISICION_ANUNCIO ) and ( NOT Inserting );{OP:11.Abr.08}
     //BtnObtenerCandidatos.Enabled := NOT inserting;
end;

procedure TEditRequisicion.RQ_PUESTOValidLookup(Sender: TObject);
begin
     with RQ_PUESTO do
     begin
          if strLleno( Llave ) then
          begin
               PU_HABILID.Lines.Text := LookupDataSet.FieldByName( 'PU_HABILID' ).AsString;
               PU_ACTIVID.Lines.Text := LookupDataSet.FieldByName( 'PU_ACTIVID' ).AsString;

               with DataSource.Dataset do
               begin
                    if State <> dsBrowse then
                    begin
                         FieldByName('RQ_EDADMIN').AsInteger := LookupDataSet.FieldByName( 'PU_EDADMIN' ).AsInteger;
                         FieldByName('RQ_EDADMAX').AsInteger := LookupDataSet.FieldByName( 'PU_EDADMAX').AsInteger;
                         if StrVacio(LookupDataSet.FieldByName( 'PU_SEXO' ).AsString) then
                            RQ_SEXO.ItemIndex := 0
                         else
                            FieldByName('RQ_SEXO').AsString := LookupDataSet.FieldByName( 'PU_SEXO' ).AsString;
                         FieldByName('RQ_ESTUDIO').AsInteger := LookupDataSet.FieldByName( 'PU_ESTUDIO').AsInteger;
                         FieldByName('RQ_INGLES').AsInteger := LookupDataSet.FieldByName( 'PU_DOMINA1').AsInteger;
                         FieldByName('RQ_AREA').AsString := LookupDataSet.FieldByName( 'PU_AREA' ).AsString;
                         FieldByName('QU_CODIGO').AsString := LookupDataSet.FieldByName( 'QU_CODIGO' ).AsString;
                         FieldByName('RQ_SUELDO').AsString := LookupDataSet.FieldByName('PU_SUELDO').AsString;
                         FieldByName('RQ_VACACI').AsString := LookupDataSet.FieldByName('PU_VACACI').AsString;
                         FieldByName('RQ_AGUINAL').AsString := LookupDataSet.FieldByName('PU_AGUINAL').AsString;
                         FieldByName('RQ_BONO').AsString := LookupDataSet.FieldByName('PU_BONO').AsString;
                         FieldByName('RQ_VALES').AsString := LookupDataSet.FieldByName('PU_VALES').AsString;
                         FieldByName('RQ_OFERTA').AsString := LookupDataSet.FieldByName('PU_OFERTA').AsString;
                    end;
               end;
          end
          else
          begin
               PU_HABILID.Lines.Text := VACIO;
               PU_ACTIVID.Lines.Text := VACIO;
          end;
     end;
end;

procedure TEditRequisicion.EscribirCambios;
var
   lInsercion: Boolean;
begin
     lInsercion := Inserting;
     inherited;
     with dmSeleccion.cdsEditRequiere do
          if ( ChangeCount = 0 ) then    // Se Aplicaron los cambios
          begin
               if lInsercion then
               begin
                    RQ_PUESTO.SetLlaveDescripcion( VACIO, VACIO ); // Para que Reinicie FoldKey
                    {Temporal, porque el ComboBox queda en blanco}
                    RQ_SEXO.ItemIndex := 0;
                    Append;
                    PageControl.ActivePage := Generales;
                    HabilitaControles;
               end;
          end;
end;

procedure TEditRequisicion.CancelarClick(Sender: TObject);
var
   lInsercion: Boolean;
begin
     lInsercion := Inserting;
     inherited;
     if lInsercion then
        Close;
end;

procedure TEditRequisicion.PageControlChange(Sender: TObject);
begin
     inherited;
     with PageControl do
          if ( ActivePage = Candidatos ) then
             self.HelpContext := H00053_Requisicion_Candidatos
          else if ( ActivePage = Gastos ) then
             self.HelpContext := H00055_Requisicion_Gastos
          else if ( ActivePage = Perfil ) then
             self.HelpContext := H00052_Requisicion_Perfil
          else if ( ActivePage = Generales ) then
             self.HelpContext := H00051_Requisicion_General
          else
             self.HelpContext := H00054_Requisicion_Otros_datos;
end;

{ Grid de Candidatos }

procedure TEditRequisicion.BtnAgregarCandidatoClick(Sender: TObject);
begin
     if ( ZAccesosMgr.CheckDerecho( D_CANDIDATOS, K_DERECHO_ALTA ) ) then
     begin
          dmSeleccion.cdsReqCandidatos.Agregar;
          CalculaTotalesRequisicion;
     end
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_CANDIDATO, [ 'Agregar' ] ), 0 );
end;

procedure TEditRequisicion.BtnBorrarCandidatoClick(Sender: TObject);
 function GetSolicitante : string;
 begin
     with dmSeleccion.cdsReqCandidatos do
          Result := FieldByName('SO_FOLIO').AsString + ': ' +
                    FieldByName('PRETTYNAME').AsString;

 end;
begin
     if NoData( dsCandidatos ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Candidatos Para Borrar', 0 )
     else if ( ZAccesosMgr.CheckDerecho( D_CANDIDATOS, K_DERECHO_BAJA ) ) then
     begin
          if ZetaDialogo.ZConfirm( Caption, '�Desea Borrar el Candidato:  ' + CR_LF +
                                             GetSolicitante +'  ?', 0, mbNo) then
          with dmSeleccion.cdsReqCandidatos do
          begin
               begin
                    Borrar;
                    Enviar;
                    CalculaTotalesRequisicion;
               end;
          end;
     end
     else
         ZetaDialogo.zInformation( Caption, Format( K_ACCESO_CANDIDATO, [ 'Borrar' ] ), 0 );
end;

procedure TEditRequisicion.BtnModificarCandidatoClick(Sender: TObject);
begin
     if NoData( dsCandidatos ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Candidatos Para Modificar', 0 )
     else if ( ZAccesosMgr.CheckDerecho( D_CANDIDATOS, K_DERECHO_CAMBIO ) ) then
     begin
          dmSeleccion.cdsReqCandidatos.Modificar;
          CalculaTotalesRequisicion;
     end
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_CANDIDATO, [ 'Modificar' ] ), 0 );
end;

procedure TEditRequisicion.BtnVerSolicitudClick(Sender: TObject);
 var oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        if NoData( dsCandidatos ) then
           ZetaDialogo.zInformation( Caption, 'No Hay Candidato Seleccionado', 0 )
        else if ( ZAccesosMgr.Revisa( D_SOLICITUD_DATOS ) ) then
        begin
             with dmSeleccion do
                  ModificaSolicitud( cdsReqCandidatos.FieldByName( 'SO_FOLIO' ).AsInteger, TRUE );
        end
        else
           ZetaDialogo.zInformation( Caption, 'No Tiene Permiso Para Ver Solicitudes', 0 );
     finally
            Screen.Cursor := oCursor;
     end;
end;


{ Grid de Gastos }

procedure TEditRequisicion.BtnAgregarGastoClick(Sender: TObject);
begin
     if ( ZAccesosMgr.CheckDerecho( D_REQUISICION_GASTOS, K_DERECHO_ALTA ) ) then
     begin
          dmSeleccion.cdsReqGastos.Agregar;
          CalculaGastoTotal;
     end
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_GASTOS, [ 'Agregar' ] ), 0 );
end;

procedure TEditRequisicion.BtnBorrarGastoClick(Sender: TObject);
begin
     if NoData( dsGastos ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Gastos Para Borrar', 0 )
     else if ( ZAccesosMgr.CheckDerecho( D_REQUISICION_GASTOS, K_DERECHO_BAJA ) ) then
     begin
          dmSeleccion.cdsReqGastos.Borrar;
          CalculaGastoTotal;
     end
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_GASTOS, [ 'Borrar' ] ), 0 );
end;

procedure TEditRequisicion.BtnModificarGastoClick(Sender: TObject);
begin
     if NoData( dsGastos ) then
        ZetaDialogo.zInformation( Caption, 'No Hay Gastos Para Modificar', 0 )
     else if ( ZAccesosMgr.CheckDerecho( D_REQUISICION_GASTOS, K_DERECHO_CAMBIO ) ) then
     begin
          dmSeleccion.cdsReqGastos.Modificar;
          CalculaGastoTotal;
     end
     else
        ZetaDialogo.zInformation( Caption, Format( K_ACCESO_GASTOS, [ 'Modificar' ] ), 0 );
end;

procedure TEditRequisicion.CalculaGastoTotal;
 var rGasto : Double;
begin
     with dmSeleccion do
     begin
          rGasto := CalculaGastoReal;
          eGastoTotal.Caption :=  FormatFloat( '#,0.00', rGasto);
          if rGasto > cdsEditRequiere.FieldByName('RQ_GTOPRES').AsFloat then
             eGastoTotal.Font.Color := clRed
          else
              eGastoTotal.Font.Color := clNavy;
     end;
end;

procedure TEditRequisicion.CalculaTotalesRequisicion(const lActualizaStatus : Boolean );
  var iCandidatos, iContratados, iSeleccionados, iVacantes : integer;
begin
     FCalculando :=  TRUE;
     try
        with dmSeleccion do
        begin
             SetTotalesRequisicion (iCandidatos, iContratados, iSeleccionados);
             RQ_CANDIDAT.Caption := IntToStr(iCandidatos);
             //RQ_SELECCI.Caption := IntToStr(iSeleccionados);
             RQ_CONTRAT.Caption := IntToStr(iContratados);
             if lActualizaStatus then
                with cdsEditRequiere do
                begin
                     iVacantes := FieldByName('RQ_VACANTE').AsInteger;
                     if (iContratados >= iVacantes ) AND
                        (FieldByName('RQ_STATUS').AsInteger = Ord(stPendiente)) then
                     begin
                          if State = dsBrowse then Edit;
                          FieldByName('RQ_STATUS').AsInteger := Ord(stCubierta);
                          ZInformation(Caption, Format( 'La Requisici�n #%s fue Completada con esta Contrataci�n.' +CR_LF+
                                                        'Ha Sido Actualizada a Status CUBIERTA.', [FieldByName('RQ_FOLIO').AsString]), 0 );
                     end
                     else if ( iContratados< iVacantes ) AND
                             ( FieldByName('RQ_STATUS').AsInteger = Ord(stCubierta)) then
                     begin
                          if State = dsBrowse then Edit;
                          FieldByName('RQ_STATUS').AsInteger := Ord(stPendiente);
                          ZInformation(Caption, Format( 'La Requisici�n #%s Ten�a Status CUBIERTA.' +CR_LF+
                                                        'El Cambio Efectuado la Regres� a Status PENDIENTE.', [FieldByName('RQ_FOLIO').AsString]), 0 );

                     end;
                end;
        end;
     finally
            FCalculando :=  FALSE;
     end;
end;

{ Anuncio por Email }

procedure TEditRequisicion.RedactaAnuncio;
  function GetTexto( const sField, sTexto : string ) : string;
  begin
       Result := '';
       with dmSeleccion.cdsEditRequiere.FieldByName(sField) do
            if StrLleno(AsString) then
               Result := sTexto +' ' + AsString + CR_LF;
  end;

  function GetTexto1( const sField, sTexto, sFinal : string ) : string;
  begin
       Result := '';
       with dmSeleccion.cdsEditRequiere.FieldByName(sField) do
            if AsInteger <> 0 then
               Result := Format( sTexto, [AsInteger] ) + sFinal + CR_LF;
  end;

  var iVacantes : integer;
      sPersona, sEdad, sEscolaridad, 
      sAnuncio, sHabilidades, sPrestaciones : string;
begin
     with dmSeleccion.cdsEditRequiere do
     begin
          iVacantes := FieldByName('RQ_VACANTE').AsInteger;

          sPersona := '';
          sEdad :='';

          if iVacantes > 1 then
             sPersona := 's';


          if FieldByName('RQ_SEXO').AsString = 'M' then
             sPersona := sPersona + ' del Sexo Masculino'
          else if FieldByName('RQ_SEXO').AsString = 'F' then
               sPersona := sPersona + ' del Sexo Femenino'
          else if iVacantes > 0 then
               sPersona := sPersona + ' Ambos Sexos';

          if (FieldByName('RQ_EDADMIN').AsInteger > 0) AND
             (FieldByName('RQ_EDADMAX').AsInteger > 0) then
             sEdad := Format( 'Edad: entre %d y %d a�os', [FieldByName('RQ_EDADMIN').AsInteger,FieldByName('RQ_EDADMAX').AsInteger] )
          else if (FieldByName('RQ_EDADMIN').AsInteger > 0) then
               sEdad := Format( 'Edad M�nima de %d a�os', [FieldByName('RQ_EDADMIN').AsInteger ] )
          else if (FieldByName('RQ_EDADMAX').AsInteger > 0) then
               sEdad := Format( 'Edad Maxima de %d a�os', [FieldByName('RQ_EDADMAX').AsInteger ] );

          if StrLleno(sEdad) then sEdad := sEdad + CR_LF;

          if eEstudios(FieldByName('RQ_ESTUDIO').AsInteger) <> esNinguno then
             sEscolaridad := 'Escolaridad M�nima: ' + ObtieneElemento(lfEstudios, FieldByName('RQ_ESTUDIO').AsInteger)+ CR_LF;

          sHabilidades := dmSeleccion.cdsPuestos.FieldByName('PU_HABILID').AsString;
          if StrLLeno(sHabilidades) then
             sHabilidades := 'Habilidades Requeridas: ' + CR_LF + sHabilidades+ CR_LF;

          sPrestaciones := GetTexto('RQ_SUELDO','Sueldo Base:');
          sPrestaciones := sPrestaciones + GetTexto('RQ_VACACI','Vacaciones:');
          sPrestaciones := sPrestaciones + GetTexto('RQ_AGUINAL','Aguinaldo:');
          sPrestaciones := sPrestaciones + GetTexto('RQ_BONO','Bono(s):');
          sPrestaciones := sPrestaciones + GetTexto('RQ_VALES','Vales:');
          sPrestaciones := sPrestaciones + GetTexto('RQ_OFERTA','');

          if StrLleno( sPrestaciones ) then
             sPrestaciones := 'Prestaciones:' + CR_LF+ sPrestaciones ;

          sAnuncio := Format( '%s SOLICITA' + CR_LF +
                              '%d Persona%s '+ CR_LF + 'para su PUESTO de %s' + CR_LF +
                              '%s' +
                              CR_LF +'Requisitos:'+CR_LF+'%s%s%s' + CR_LF+
                              '%s' +
                              '%s' +
                              'Interesados Comunicarse al tel�fono %s ' + CR_LF +
                              'con %s de Lunes a Viernes en horas h�biles.',

                              [ StrDef( GetTexto( 'CL_DESCRIP', 'La Empresa' ), 'Se' ),
                                iVacantes, sPersona,
                                FieldByName('PU_DESCRIP').AsString,
                                GetTexto('RQ_TURNO','en el Turno' ),
                                sEdad,
                                sEscolaridad,
                                StrDef( GetTexto1('RQ_INGLES','Ingl�s: %d', '%')+CR_LF,''),
                                StrDef( sHabilidades + CR_LF, '' ),
                                StrDef( sPrestaciones + CR_LF, '' ),
                                Global.GetGlobalString(K_GLOBAL_TEL_EMPRESA),
                                Global.GetGlobalString(K_GLOBAL_REPRESENTANTE)] );

          if State = dsBrowse then Edit;
          FieldByName('RQ_ANUNCIO').AsString := sAnuncio;
          Rq_Anuncio.SetFocus;
     end;
end;

{OP:11.Abr.08}
{Verifica si el motivo es 'Reemplazo', de serlo, activa el campo para
ingresar el nombre del personal que se reemplaza.}
procedure TEditRequisicion.VerificaReemplazo;
var
   lEnabled : Boolean;
begin
     lEnabled := eMotivoVacante( RQ_MOTIVO.Valor ) = mvReemplazo;

     RQ_REEMPLA.Enabled := lEnabled;
     ReemplazaLbl.Enabled := lEnabled;
end;

procedure TEditRequisicion.BRedactarAnuncioClick(Sender: TObject);
begin
     inherited;
     RedactaAnuncio;
end;

procedure TEditRequisicion.BtnEnviarClick(Sender: TObject);
begin
     inherited;
     if ZetaDialogo.ZErrorConfirm( Caption, '� Desea Enviar el Anuncio por Correo Electr�nico a' +CR_LF+
                                            'la direcci�n: '+ eMailContacto.Text + ' ?', 0, mbNo ) then
     begin
          dmSeleccion.EnviarAnuncio(RQ_ANUNCIO.Text, eMailContacto.Text);
          ZetaDialogo.ZInformation(Caption, 'Un Email con el Anuncio ha Sido Enviado a ' + CR_LF + eMailContacto.Text, 0);
     end;
end;

procedure TEditRequisicion.RQ_MOTIVOChange(Sender: TObject);
begin
     inherited;
     VerificaReemplazo();{OP:11.Abr.08}
end;

{OP:21.Abr.08}
{Al ser invocado, si el nombre y puesto de quien autoriza est� tiene info.
deshabilita ambos controles.}
procedure TEditRequisicion.ActivaAutorizo;
var
   lAutorNomLleno, lAutorPueLleno: Boolean;
   oControl: TWinControl;
begin
     with dmSeleccion.cdsCliente do
     begin
          lAutorNomLleno := StrLleno( FieldByName( 'CL_NOM_AU' ).AsString );
          lAutorPueLleno := StrLleno( FieldByName( 'CL_PUES_AU' ).AsString );
     end;

//     oControl := Self.ActiveControl;

     if ( RQ_GTOPRES.Visible ) then
        oControl := RQ_GTOPRES
     else
     begin
          if ( NOT lAutorNomLleno ) then
             oControl := RQ_AUTORIZ
          else if ( NOT lAutorPueLleno ) then
              oControl := RQ_PUES_AU
          else
              oControl := RQ_TIPO_CO;
     end;

     RQ_AUTORIZ.Enabled := NOT ( lAutorNomLleno );
     RQ_PUES_AU.Enabled := NOT ( lAutorPueLleno );

     Self.ActiveControl := oControl;

end;

{OP:14.Abr.08}
procedure TEditRequisicion.RQ_CLIENTEValidLookup(Sender: TObject);
begin
     inherited;
     ActivaAutorizo;
end;

end.
