unit FEditTablas;

interface

uses ZBaseTablas;

type
  TEditAreaInteres = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditTipoGasto = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditAdicional1 = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditAdicional2 = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditAdicional3 = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditAdicional4 = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TEditAdicional5 = class( TEditLookup )
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

var
  EditAreaInteres: TEditAreaInteres;
  EditTipoGasto: TEditTipoGasto;
  EditAdicional1: TEditAdicional1;
  EditAdicional2: TEditAdicional2;
  EditAdicional3: TEditAdicional3;
  EditAdicional4: TEditAdicional4;
  EditAdicional5: TEditAdicional5;

implementation

uses DSeleccion,
     ZAccesosTress,
     ZetaCommonClasses;

{ ****** TEditAreaInteres ******* }

procedure TEditAreaInteres.AfterCreate;
begin
     inherited AfterCreate;
     Caption := 'Areas de Inter�s';
     LookupDataset := dmSeleccion.cdsAreaInteres;
     HelpContext := H00070_Catalogos_generales;
     IndexDerechos := D_CAT_AREAS_INTERES;
end;

{ ****** TEditTipoGasto ******* }

procedure TEditTipoGasto.AfterCreate;
begin
     inherited AfterCreate;
     Caption := 'Tipos de Gasto';
     LookupDataset := dmSeleccion.cdsTipoGasto;
     HelpContext := H00070_Catalogos_generales;
     IndexDerechos := D_CAT_TIPO_GASTOS;
end;

{ ****** TEditAdicional1 ******* }

procedure TEditAdicional1.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional1;
     HelpContext := H00079_Tablas_Adicionales;
     IndexDerechos := D_CAT_ADICION_TABLA1;
end;

{ ****** TEditAdicional2 ******* }

procedure TEditAdicional2.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional2;
     HelpContext := H00079_Tablas_Adicionales;
     IndexDerechos := D_CAT_ADICION_TABLA2;
end;

{ ****** TEditAdicional3 ******* }

procedure TEditAdicional3.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional3;
     HelpContext := H00079_Tablas_Adicionales;
     IndexDerechos := D_CAT_ADICION_TABLA3;
end;

{ ****** TEditAdicional4 ******* }

procedure TEditAdicional4.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional4;
     HelpContext := H00079_Tablas_Adicionales;
     IndexDerechos := D_CAT_ADICION_TABLA4;
end;

{ ****** TEditAdicional5 ******* }

procedure TEditAdicional5.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional5;
     HelpContext := H00079_Tablas_Adicionales;
     IndexDerechos := D_CAT_ADICION_TABLA5;
end;

end.
