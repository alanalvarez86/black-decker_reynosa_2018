inherited EditGasto: TEditGasto
  Left = 350
  Top = 189
  Caption = 'Gasto de Reclutamiento'
  ClientHeight = 307
  ClientWidth = 430
  PixelsPerInch = 96
  TextHeight = 13
  object RQ_FOLIOlbl: TLabel [0]
    Left = 24
    Top = 51
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Requisición:'
  end
  object RQ_FOLIO: TZetaDBTextBox [1]
    Left = 84
    Top = 49
    Width = 61
    Height = 19
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'RQ_FOLIO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'RQ_FOLIO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object RG_FOLIOlbl: TLabel [2]
    Left = 41
    Top = 72
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Gasto #:'
  end
  object RG_FOLIO: TZetaDBTextBox [3]
    Left = 84
    Top = 70
    Width = 61
    Height = 19
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'RG_FOLIO'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'RG_FOLIO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object RG_FECHAlbl: TLabel [4]
    Left = 49
    Top = 95
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = '&Fecha:'
    FocusControl = RG_FECHA
  end
  object RG_MONTOlbl: TLabel [5]
    Left = 49
    Top = 166
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mo&nto:'
    FocusControl = RG_MONTO
  end
  object RG_DESCRIPlbl: TLabel [6]
    Left = 23
    Top = 142
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'D&escripción:'
    FocusControl = RG_DESCRIP
  end
  object RG_TIPOlbl: TLabel [7]
    Left = 58
    Top = 120
    Width = 24
    Height = 13
    Caption = '&Tipo:'
    FocusControl = RG_TIPO
  end
  object RG_COMENTAlbl: TLabel [8]
    Left = 8
    Top = 187
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Obser&vaciones:'
    FocusControl = RG_COMENTA
  end
  inherited PanelBotones: TPanel
    Top = 271
    Width = 430
    inherited OK: TBitBtn
      Left = 262
    end
    inherited Cancelar: TBitBtn
      Left = 347
    end
  end
  inherited PanelSuperior: TPanel
    Width = 430
  end
  inherited PanelIdentifica: TPanel
    Width = 430
    inherited ValorActivo2: TPanel
      Width = 104
    end
  end
  object RG_FECHA: TZetaDBFecha [12]
    Left = 84
    Top = 91
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 3
    Text = '28/Sep/01'
    Valor = 37162
    DataField = 'RG_FECHA'
    DataSource = DataSource
  end
  object RG_DESCRIP: TDBEdit [13]
    Left = 84
    Top = 139
    Width = 337
    Height = 21
    DataField = 'RG_DESCRIP'
    DataSource = DataSource
    TabOrder = 5
  end
  object RG_MONTO: TZetaDBNumero [14]
    Left = 84
    Top = 162
    Width = 114
    Height = 21
    Mascara = mnPesos
    TabOrder = 6
    Text = '0.00'
    DataField = 'RG_MONTO'
    DataSource = DataSource
  end
  object RG_TIPO: TZetaDBKeyLookup [15]
    Left = 84
    Top = 116
    Width = 338
    Height = 21
    LookupDataset = dmSeleccion.cdsTipoGasto
    TabOrder = 4
    TabStop = True
    WidthLlave = 60
    DataField = 'RG_TIPO'
    DataSource = DataSource
  end
  object RG_COMENTA: TDBMemo [16]
    Left = 84
    Top = 185
    Width = 337
    Height = 83
    DataField = 'RG_COMENTA'
    DataSource = DataSource
    ScrollBars = ssVertical
    TabOrder = 7
  end
  inherited DataSource: TDataSource
    Left = 188
    Top = 9
  end
end
