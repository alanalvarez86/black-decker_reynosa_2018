unit FTablas;

interface

uses ZBaseTablasConsulta;

type
  TCatTipoGastos = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatAreaInteres = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatAdicional1 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatAdicional2 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatAdicional3 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatAdicional4 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;
  TCatAdicional5 = class(TTablaLookup)
  protected
    {Protected declarations}
    procedure AfterCreate; override;
  end;

implementation

uses DSeleccion,
     ZetaCommonClasses;

{ ***** TCatAreaInteres **** }

procedure TCatAreaInteres.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAreaInteres;
     HelpContext := H00070_Catalogos_generales;
end;

{ ****** TCatTipoGastos ****** }

procedure TCatTipoGastos.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsTipoGasto;
     HelpContext := H00070_Catalogos_generales;
end;

{ ***** TCatAdicional1 **** }

procedure TCatAdicional1.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional1;
     HelpContext := H00079_Tablas_Adicionales;
end;

{ ***** TCatAdicional2 **** }

procedure TCatAdicional2.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional2;
     HelpContext := H00079_Tablas_Adicionales;
end;

{ ***** TCatAdicional3 **** }

procedure TCatAdicional3.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional3;
     HelpContext := H00079_Tablas_Adicionales;
end;

{ ***** TCatAdicional4 **** }

procedure TCatAdicional4.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional4;
     HelpContext := H00079_Tablas_Adicionales;
end;

{ ***** TCatAdicional5 **** }

procedure TCatAdicional5.AfterCreate;
begin
     inherited AfterCreate;
     LookupDataset := dmSeleccion.cdsAdicional5;
     HelpContext := H00079_Tablas_Adicionales;
end;

end.
