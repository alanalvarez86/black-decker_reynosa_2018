<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
	<center>
		<table cellSpacing="0" cellPadding="0" width="95%" bgColor="#004080" border="1">
			<tr>
				<td>
					<font face="book antiqua" size="48" color="#FF8000">
						<b>S</b>
					</font>	
					<font face="book antiqua" size="32" color="#FFFFFF">
						<b>elecci<xsl:text>&#243;</xsl:text>n de </b>
					</font>
					<font face="book antiqua" size="48" color="#FF8000">
						<b>P</b>
					</font>
					<font face="book antiqua" size="32" color="#FFFFFF">
						<b>ersonal</b>
					</font>	
				</td>					
			</tr>
			<tr>
				<table border="0" width="70%">
				<br></br>
					<xsl:for-each select="//ROWS/ROW">
						<xsl:variable name="Par"><xsl:value-of select="@PAR"/></xsl:variable>
					<xsl:if test="$Par=1">
						<tr>
							<td width="75%" bgcolor="#FFFFFF">							
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#004080" size="2">							
									<xsl:element name="a">
										<xsl:attribute name="href">Requisicion.asp?Folio=<xsl:value-of select="@RQ_FOLIO"/></xsl:attribute>									
									<xsl:value-of select="@PU_DESCRIP"/></xsl:element>
								</font>
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">
									<xsl:text> </xsl:text><b>-<xsl:value-of select="@CL_NOMBRE"/>-</b>
								</font>
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">																									
										<xsl:text>  </xsl:text><xsl:value-of select="@RQ_ANUNCIO"/></font></td>
						</tr>						
					</xsl:if>			
					<xsl:if test="$Par=0">
						<tr>
							<td width="75%" bgcolor="#FFCFB9">							
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#004080" size="2">							
									<xsl:element name="a">
										<xsl:attribute name="href">Requisicion.asp?Folio=<xsl:value-of select="@RQ_FOLIO"/></xsl:attribute>									
									<xsl:value-of select="@PU_DESCRIP"/></xsl:element>
								</font>		
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">
									<xsl:text> </xsl:text><b>-<xsl:value-of select="@CL_NOMBRE"/>-</b>
								</font>
								<font face="Verdana, Arial, Helvetica, sans-serif" color="#000000" size="1">																									
										<xsl:text>  </xsl:text><xsl:value-of select="@RQ_ANUNCIO"/></font></td>
						</tr>					
					</xsl:if>									
					</xsl:for-each>
				</table>
			</tr>
		</table>		
	</center>
</xsl:template>
</xsl:stylesheet>