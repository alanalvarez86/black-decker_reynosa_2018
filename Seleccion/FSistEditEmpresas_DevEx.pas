unit FSistEditEmpresas_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseEditEmpresas, Db, StdCtrls, ZetaEdit, Mask, DBCtrls, ExtCtrls,
  Buttons, ZetaSmartLists, ZetaKeyLookup;

type
  TSistEditEmpresas_DevEx = class(TSistBaseEditEmpresas)
    Label3: TLabel;
    DB_CODIGO: TZetaDBKeyLookup;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations } 
    procedure Connect;override;
  end;

var
  SistEditEmpresas_DevEx: TSistEditEmpresas_DevEx;

implementation

uses DSistema;

{$R *.DFM}

procedure TSistEditEmpresas_DevEx.FormCreate(Sender: TObject);
begin
  inherited;

     with dmSistema do
     begin
          DB_CODIGO.LookupDataset := cdsSistBaseDatosLookUp;
     end;
end;

procedure TSistEditEmpresas_DevEx.Connect;
begin
     with dmSistema do
     begin
          // cdsEmpresasLookup.Conectar;
          cdsSistBaseDatosLookup.Conectar;
     end;
     inherited;
end;

end.
