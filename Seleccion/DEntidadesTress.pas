unit DEntidadesTress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DEntidades, ZetaTipoEntidad, ZetaEntidad;

type
  TdmEntidadesTress = class(TdmEntidades)
  private
    { Private declarations }
  protected
    procedure CreaEntidades; override;
  public
    { Public declarations }
  end;

implementation

uses ZetaCommonLists;

{$R *.DFM}

{ TdmEntidadesTress }

procedure TdmEntidadesTress.CreaEntidades;
begin
     with AddEntidad( enRequisicion, 'REQUIERE', 'RQ_FOLIO' ) do
     begin
          AddRelacion( enAreaInteres,'RQ_AREA');
          AddRelacion( enPuesto,'RQ_PUESTO');
          AddRelacion( enCondiciones,'QU_CODIGO');
          AddRelacion( enCliente,'RQ_CLIENTE');
     end;
     with AddEntidad( enSolicitud, 'SOLICITA', 'SO_FOLIO' ) do
     begin
          AddRelacion( enAreaInteres,'SO_AREA_1');
          AddRelacion( enTablaOpcion1,'SO_G_TAB_1');
          AddRelacion( enTablaOpcion2,'SO_G_TAB_2');
          AddRelacion( enTablaOpcion3,'SO_G_TAB_3');
          AddRelacion( enTablaOpcion4,'SO_G_TAB_4');
          AddRelacion( enTablaOpcion5,'SO_G_TAB_5');
     end;

     with AddEntidad( enCandidato, 'CANDIDAT', 'SO_FOLIO,RQ_FOLIO' ) do
     begin
          AddRelacion( enRequisicion,'RQ_FOLIO');
          AddRelacion( enSolicitud,'SO_FOLIO');
     end;

     with AddEntidad( enEntrevista, 'ENTREVIS' , 'SO_FOLIO, RQ_FOLIO, ER_FOLIO' ) do
     begin
          AddRelacion( enCandidato, 'SO_FOLIO,RQ_FOLIO')
     end;
     with AddEntidad( enExamen, 'EXAMEN', 'SO_FOLIO, EX_FOLIO' ) do
     begin
          AddRelacion( enSolicitud, 'SO_FOLIO' );
     end;
     with AddEntidad( enDocumentos, 'DOCUMENT', 'SO_FOLIO,DO_TIPO' ) do
     begin
          AddRelacion( enSolicitud, 'SO_FOLIO' );
     end;
     with AddEntidad( enPuesto, 'PUESTO', 'PU_CODIGO' ) do
     begin
          AddRelacion(enCondiciones, 'QU_CODIGO');
          AddRelacion(enAreaInteres, 'PU_AREA' );
     end;
     with AddEntidad( enGasto, 'REQGASTO', 'RQ_FOLIO, RG_FOLIO' ) do
     begin
          AddRelacion(enRequisicion, 'RQ_FOLIO');
          AddRelacion(enTipoGasto, 'RG_TIPO');
     end;
     with AddEntidad( enReporte,'REPORTE','RE_CODIGO') do
     begin
          AddRelacion( enCondiciones,'QU_CODIGO');
     end;
     with AddEntidad( enCampoRep,'CAMPOREP','RE_CODIGO,CR_TIPO,CR_POSICIO') do
     begin
          AddRelacion( enReporte,'RE_CODIGO');
     end;

     with AddEntidad( enMisReportes, 'MISREPOR', 'RE_CODIGO,US_CODIGO' ) do
     begin
          AddRelacion( enReporte,'RE_CODIGO');
     end;

     with AddEntidad( enBitacora,'BITACORA','') do
     begin
          AddRelacion( enProceso,'BI_NUMERO');
     end;

     with AddEntidad( enReferenciasLaborales, 'REF_LAB', 'SO_FOLIO,RL_FOLIO' ) do
     begin
          AddRelacion( enSolicitud, 'SO_FOLIO' );
     end;

     with AddEntidad( enSuscrip,'SUSCRIP','RE_CODIGO,US_CODIGO') do
     begin
          AddRelacion( enReporte,'RE_CODIGO');
          AddRelacion( enUsuarios,'US_CODIGO');
     end;


     AddEntidad( enCliente, 'CLIENTE', 'CL_CODIGO' ) ;
     AddEntidad( enCondiciones, 'QUERYS', 'QU_CODIGO' );
     AddEntidad( enAreaInteres, 'AINTERES' , 'TB_CODIGO' );
     AddEntidad( enTipoGasto, 'TGASTO', 'TB_CODIGO' );
     AddEntidad( enGlobal,'GLOBAL','GL_CODIGO' );
     AddEntidad( enDiccion,'DICCION','DI_CLASIFI,DI_NOMBRE');
     AddEntidad( enProceso,'PROCESO','PC_NUMERO');
     AddEntidad( enTablaOpcion1, 'TABLA01', 'TB_CODIGO' );
     AddEntidad( enTablaOpcion2, 'TABLA02', 'TB_CODIGO' );
     AddEntidad( enTablaOpcion3, 'TABLA03', 'TB_CODIGO' );
     AddEntidad( enTablaOpcion4, 'TABLA04', 'TB_CODIGO' );
     AddEntidad( enTablaOpcion5, 'TABLA05', 'TB_CODIGO' );
end;
end.
