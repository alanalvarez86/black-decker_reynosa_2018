unit FSistUsuarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FSistBaseUsuarios, Db, StdCtrls, Buttons, Grids, DBGrids, ZetaDBGrid,
  ZetaCommonClasses, ExtCtrls;

type
  TSistUsuarios = class(TSistBaseUsuarios)
  private
    { Private declarations }
  protected
    function PuedeModificar( var sMensaje: String ): Boolean; override;
  public
    { Public declarations }
  end;

var
  SistUsuarios: TSistUsuarios;

implementation

uses ZAccesosMgr, ZAccesosTress, DCliente, DSistema;

{$R *.DFM}

{ TSistUsuarios }

function TSistUsuarios.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmSistema.PuedeModificarUsuarioPropio( DataSource.Dataset );
          if (not Result) then
          begin
               sMensaje := 'No Tiene Permiso Para Modificar Registros';
          end;
     end;
end;

end.
