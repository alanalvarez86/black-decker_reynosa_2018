inherited EditRequisicion: TEditRequisicion
  Left = 442
  Top = 186
  Width = 620
  Height = 444
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSizeable
  Caption = 'Requisici'#243'n'
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 374
    Width = 612
    inherited OK: TBitBtn
      Left = 444
    end
    inherited Cancelar: TBitBtn
      Left = 529
    end
  end
  inherited PanelSuperior: TPanel
    Width = 612
    inherited AgregarBtn: TSpeedButton
      Visible = False
    end
    inherited BorrarBtn: TSpeedButton
      Visible = False
    end
    inherited ModificarBtn: TSpeedButton
      Visible = False
    end
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 612
    inherited ValorActivo2: TPanel
      Width = 286
    end
  end
  object PanelHeader: TPanel [3]
    Left = 0
    Top = 51
    Width = 612
    Height = 80
    Align = alTop
    TabOrder = 3
    object PanelFolio: TPanel
      Left = 1
      Top = 1
      Width = 610
      Height = 25
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LblFolio: TLabel
        Left = 32
        Top = 8
        Width = 25
        Height = 13
        Caption = 'Folio:'
      end
      object RQ_FOLIO: TZetaDBTextBox
        Left = 60
        Top = 6
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'RQ_FOLIO'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'RQ_FOLIO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object PanelOtros: TPanel
      Left = 1
      Top = 26
      Width = 610
      Height = 51
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 21
        Top = 6
        Width = 36
        Height = 13
        Caption = 'Puesto:'
      end
      object Label4: TLabel
        Left = 457
        Top = 29
        Width = 44
        Height = 13
        Caption = 'Prioridad:'
      end
      object RQ_CONTRAT: TZetaTextBox
        Left = 283
        Top = 26
        Width = 30
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = '0'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label16: TLabel
        Left = 220
        Top = 30
        Width = 60
        Height = 13
        Caption = 'Contratados:'
      end
      object RQ_CANDIDAT: TZetaTextBox
        Left = 166
        Top = 26
        Width = 30
        Height = 21
        Alignment = taRightJustify
        AutoSize = False
        Caption = '0'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
      end
      object Label28: TLabel
        Left = 107
        Top = 30
        Width = 56
        Height = 13
        Caption = 'Candidatos:'
      end
      object Label5: TLabel
        Left = 9
        Top = 30
        Width = 48
        Height = 13
        Caption = 'Vacantes:'
      end
      object Label3: TLabel
        Left = 467
        Top = 5
        Width = 33
        Height = 13
        Caption = 'Status:'
      end
      object RQ_PUESTO: TZetaDBKeyLookup
        Left = 60
        Top = 2
        Width = 373
        Height = 21
        LookupDataset = dmSeleccion.cdsPuestos
        TabOrder = 0
        TabStop = True
        WidthLlave = 80
        OnValidLookup = RQ_PUESTOValidLookup
        DataField = 'RQ_PUESTO'
        DataSource = DataSource
      end
      object RQ_STATUS: TZetaDBKeyCombo
        Left = 504
        Top = 2
        Width = 100
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 2
        ListaFija = lfReqStatus
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_STATUS'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_PRIORID: TZetaDBKeyCombo
        Left = 504
        Top = 26
        Width = 100
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 3
        ListaFija = lfPrioridad
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_PRIORID'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_VACANTE: TZetaDBNumero
        Left = 60
        Top = 26
        Width = 30
        Height = 21
        Mascara = mnEmpleado
        TabOrder = 1
        DataField = 'RQ_VACANTE'
        DataSource = DataSource
      end
    end
  end
  object PageControl: TPageControl [4]
    Left = 0
    Top = 131
    Width = 612
    Height = 243
    ActivePage = Candidatos
    Align = alClient
    TabOrder = 4
    OnChange = PageControlChange
    object Generales: TTabSheet
      Caption = 'Generales'
      object Label18: TLabel
        Left = 49
        Top = 21
        Width = 35
        Height = 13
        Caption = 'Motivo:'
      end
      object Label23: TLabel
        Left = 53
        Top = 46
        Width = 31
        Height = 13
        Caption = 'Turno:'
      end
      object Label24: TLabel
        Left = 49
        Top = 70
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object PresupuestoLbl: TLabel
        Left = 22
        Top = 95
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Presupuesto:'
        WordWrap = True
      end
      object lblContratos: TLabel
        Left = 2
        Top = 186
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo de Contrato:'
      end
      object ReemplazaLbl: TLabel
        Left = 254
        Top = 21
        Width = 65
        Height = 13
        Caption = 'Reemplaza a:'
      end
      object RQ_TURNO: TDBEdit
        Left = 89
        Top = 42
        Width = 300
        Height = 21
        DataField = 'RQ_TURNO'
        DataSource = DataSource
        TabOrder = 2
      end
      object RQ_MOTIVO: TZetaDBKeyCombo
        Left = 89
        Top = 17
        Width = 154
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 0
        OnChange = RQ_MOTIVOChange
        ListaFija = lfMotivoVacante
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_MOTIVO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_CLIENTE: TZetaDBKeyLookup
        Left = 89
        Top = 67
        Width = 301
        Height = 21
        TabOrder = 3
        TabStop = True
        WidthLlave = 80
        OnValidLookup = RQ_CLIENTEValidLookup
        DataField = 'RQ_CLIENTE'
        DataSource = DataSource
      end
      object RQ_GTOPRES: TZetaDBNumero
        Left = 89
        Top = 92
        Width = 80
        Height = 21
        Mascara = mnPesos
        TabOrder = 4
        Text = '0.00'
        DataField = 'RQ_GTOPRES'
        DataSource = DataSource
      end
      object Proceso: TGroupBox
        Left = 409
        Top = 44
        Width = 190
        Height = 116
        Caption = 'Proceso de Selecci'#243'n'
        TabOrder = 7
        object Label20: TLabel
          Left = 29
          Top = 26
          Width = 28
          Height = 13
          Caption = 'Inici'#243':'
        end
        object Label21: TLabel
          Left = 13
          Top = 56
          Width = 44
          Height = 13
          Caption = 'Promesa:'
        end
        object Label22: TLabel
          Left = 16
          Top = 87
          Width = 41
          Height = 13
          Caption = 'Termin'#243':'
        end
        object RQ_FEC_FIN: TZetaDBTextBox
          Left = 62
          Top = 82
          Width = 99
          Height = 21
          AutoSize = False
          Caption = 'RQ_FEC_FIN'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RQ_FEC_FIN'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object RQ_FEC_INI: TZetaDBFecha
          Left = 62
          Top = 23
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '04/Ago/01'
          Valor = 37107.000000000000000000
          DataField = 'RQ_FEC_INI'
          DataSource = DataSource
        end
        object RQ_FEC_PRO: TZetaDBFecha
          Left = 62
          Top = 50
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '04/Ago/01'
          Valor = 37107.000000000000000000
          DataField = 'RQ_FEC_PRO'
          DataSource = DataSource
        end
      end
      object RQ_TIPO_CO: TZetaDBKeyLookup
        Left = 89
        Top = 183
        Width = 301
        Height = 21
        TabOrder = 6
        TabStop = True
        WidthLlave = 80
        DataField = 'RQ_TIPO_CO'
        DataSource = DataSource
      end
      object RQ_REEMPLA: TDBEdit
        Left = 324
        Top = 17
        Width = 274
        Height = 21
        DataField = 'RQ_REEMPLA'
        DataSource = DataSource
        TabOrder = 1
      end
      object GBAutorizo: TGroupBox
        Left = 8
        Top = 113
        Width = 393
        Height = 65
        Caption = 'Autoriz'#243
        TabOrder = 5
        object Label12: TLabel
          Left = 36
          Top = 16
          Width = 40
          Height = 13
          Caption = 'Nombre:'
        end
        object Label32: TLabel
          Left = 40
          Top = 40
          Width = 36
          Height = 13
          Caption = 'Puesto:'
        end
        object RQ_AUTORIZ: TDBEdit
          Left = 80
          Top = 12
          Width = 300
          Height = 21
          DataField = 'RQ_AUTORIZ'
          DataSource = DataSource
          Enabled = False
          TabOrder = 0
        end
        object RQ_PUES_AU: TDBEdit
          Left = 80
          Top = 37
          Width = 300
          Height = 21
          DataField = 'RQ_PUES_AU'
          DataSource = DataSource
          Enabled = False
          TabOrder = 1
        end
      end
    end
    object Perfil: TTabSheet
      Caption = 'Perfil'
      ImageIndex = 1
      object Label29: TLabel
        Left = 165
        Top = 48
        Width = 27
        Height = 13
        Caption = 'Se&xo:'
        FocusControl = RQ_SEXO
      end
      object Label7: TLabel
        Left = 106
        Top = 71
        Width = 86
        Height = 13
        Caption = 'Estudios &M'#237'nimos:'
        FocusControl = RQ_EST_MIN
      end
      object Label6: TLabel
        Left = 117
        Top = 120
        Width = 75
        Height = 13
        Caption = 'A&rea de Inter'#233's:'
        FocusControl = RQ_AREA
      end
      object Label15: TLabel
        Left = 125
        Top = 146
        Width = 67
        Height = 13
        Caption = 'O&tros criterios:'
        FocusControl = QU_CODIGO
      end
      object LblIngles: TLabel
        Left = 120
        Top = 95
        Width = 72
        Height = 13
        Caption = 'Dominio I&ngl'#233's:'
        FocusControl = RQ_INGLES
      end
      object Label19: TLabel
        Left = 106
        Top = 170
        Width = 86
        Height = 13
        Caption = 'Otras &Habilidades:'
        FocusControl = RQ_HABILID
      end
      object GBEdad: TGroupBox
        Left = 149
        Top = 0
        Width = 201
        Height = 41
        Caption = ' Edad '
        TabOrder = 0
        object Label8: TLabel
          Left = 16
          Top = 16
          Width = 25
          Height = 13
          Caption = '&Entre'
          FocusControl = RQ_EDADMIN
        end
        object Label11: TLabel
          Left = 166
          Top = 16
          Width = 23
          Height = 13
          Caption = 'a'#241'os'
        end
        object Label10: TLabel
          Left = 102
          Top = 16
          Width = 5
          Height = 13
          Caption = 'y'
        end
        object RQ_EDADMIN: TZetaDBNumero
          Left = 48
          Top = 12
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 0
          Text = '0'
          DataField = 'RQ_EDADMIN'
          DataSource = DataSource
        end
        object RQ_EDADMAX: TZetaDBNumero
          Left = 117
          Top = 12
          Width = 41
          Height = 21
          Mascara = mnDias
          TabOrder = 1
          Text = '0'
          DataField = 'RQ_EDADMAX'
          DataSource = DataSource
        end
      end
      object RQ_SEXO: TZetaDBKeyCombo
        Left = 197
        Top = 44
        Width = 154
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 1
        ListaFija = lfSexoDesc
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_SEXO'
        DataSource = DataSource
        LlaveNumerica = False
      end
      object RQ_EST_MIN: TZetaDBKeyCombo
        Left = 197
        Top = 67
        Width = 154
        Height = 21
        AutoComplete = False
        Style = csDropDownList
        DropDownCount = 10
        ItemHeight = 13
        TabOrder = 2
        ListaFija = lfEstudios
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        DataField = 'RQ_ESTUDIO'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object RQ_INGLES: TZetaDBNumero
        Left = 197
        Top = 91
        Width = 41
        Height = 21
        Mascara = mnTasa
        TabOrder = 3
        Text = '0.0 %'
        DataField = 'RQ_INGLES'
        DataSource = DataSource
      end
      object RQ_AREA: TZetaDBKeyLookup
        Left = 197
        Top = 116
        Width = 300
        Height = 21
        TabOrder = 4
        TabStop = True
        WidthLlave = 80
        DataField = 'RQ_AREA'
        DataSource = DataSource
      end
      object QU_CODIGO: TZetaDBKeyLookup
        Left = 197
        Top = 140
        Width = 300
        Height = 21
        TabOrder = 5
        TabStop = True
        WidthLlave = 80
        DataField = 'QU_CODIGO'
        DataSource = DataSource
      end
      object RQ_HABILID: TDBMemo
        Left = 197
        Top = 164
        Width = 297
        Height = 49
        DataField = 'RQ_HABILID'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 6
      end
    end
    object Candidatos: TTabSheet
      Caption = 'Candidatos'
      ImageIndex = 2
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 604
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object BtnAgregarCandidato: TSpeedButton
          Left = 10
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Agregar Candidato'
          Caption = 'Agregar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnAgregarCandidatoClick
        end
        object BtnBorrarCandidato: TSpeedButton
          Left = 92
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Borrar Candidato'
          Caption = 'Borrar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnBorrarCandidatoClick
        end
        object BtnModificarCandidato: TSpeedButton
          Left = 174
          Top = 3
          Width = 80
          Height = 24
          Hint = 'Modificar Candidato'
          Caption = 'Modificar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnModificarCandidatoClick
        end
        object BtnVerSolicitud: TBitBtn
          Left = 475
          Top = 3
          Width = 120
          Height = 24
          Caption = 'Ver Solicitud'
          TabOrder = 0
          OnClick = BtnVerSolicitudClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            5555555555FFFFFFFFFF5555500000000005555557777777777F55550BFBFBFB
            FB0555557F555555557F55500FBFBFBFBF0555577F555555557F550B0BFBFBFB
            FB05557F7F555555557F500F0FBFBFBFBF05577F7F555555557F0B0B0BFBFBFB
            FB057F7F7F555555557F0F0F0FBFBFBFBF057F7F7FFFFFFFFF750B0B00000000
            00557F7F7777777777550F0FB0FBFB0F05557F7FF75FFF7575550B0007000070
            55557F777577775755550FB0FBFB0F0555557FF75FFF75755555000700007055
            5555777577775755555550FBFB0555555555575FFF7555555555570000755555
            5555557777555555555555555555555555555555555555555555}
          NumGlyphs = 2
        end
      end
      object GridCandidatos: TZetaDBGrid
        Left = 0
        Top = 30
        Width = 604
        Height = 185
        Align = alClient
        DataSource = dsCandidatos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = BtnModificarCandidatoClick
        Columns = <
          item
            Expanded = False
            FieldName = 'SO_FOLIO'
            Title.Caption = 'Folio'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PRETTYNAME'
            Title.Caption = 'Nombre'
            Width = 210
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SO_SEXO'
            Title.Alignment = taCenter
            Title.Caption = 'Sexo'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SO_FEC_NAC'
            Title.Caption = 'Edad'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SO_ESTUDIO'
            Title.Caption = 'Escolaridad'
            Width = 75
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'SO_INGLES'
            Title.Alignment = taCenter
            Title.Caption = 'Ingl'#233's'
            Width = 35
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CD_STATUS'
            Title.Caption = 'Status'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SO_STATUS'
            Title.Caption = 'Solicitante'
            Width = 75
            Visible = True
          end>
      end
    end
    object Puesto: TTabSheet
      Caption = 'Descripci'#243'n del Puesto'
      ImageIndex = 3
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 302
        Height = 215
        Align = alLeft
        Caption = 'Habilidades Requeridas'
        TabOrder = 0
        object PU_HABILID: TMemo
          Left = 2
          Top = 15
          Width = 298
          Height = 198
          Align = alClient
          Color = clBtnFace
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
      object GroupBox4: TGroupBox
        Left = 302
        Top = 0
        Width = 302
        Height = 215
        Align = alClient
        Caption = 'Actividades a Realizar'
        TabOrder = 1
        object PU_ACTIVID: TMemo
          Left = 2
          Top = 15
          Width = 298
          Height = 198
          Align = alClient
          Color = clBtnFace
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 0
        end
      end
    end
    object Oferta: TTabSheet
      Caption = 'Oferta'
      ImageIndex = 4
      object RQ_OFERTA: TDBMemo
        Left = 0
        Top = 57
        Width = 604
        Height = 158
        Align = alClient
        DataField = 'RQ_OFERTA'
        DataSource = DataSource
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        MaxLength = 255
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 604
        Height = 57
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label13: TLabel
          Left = 1
          Top = 10
          Width = 63
          Height = 13
          Alignment = taRightJustify
          Caption = 'Sueldo Base:'
        end
        object Label14: TLabel
          Left = 5
          Top = 34
          Width = 59
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vacaciones:'
        end
        object Label30: TLabel
          Left = 218
          Top = 10
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Aguinaldo:'
        end
        object Label31: TLabel
          Left = 229
          Top = 34
          Width = 39
          Height = 13
          Alignment = taRightJustify
          Caption = 'Bono(s):'
        end
        object Label17: TLabel
          Left = 425
          Top = 10
          Width = 29
          Height = 13
          Alignment = taRightJustify
          Caption = 'Vales:'
        end
        object RQ_SUELDO: TDBEdit
          Left = 66
          Top = 6
          Width = 140
          Height = 21
          DataField = 'RQ_SUELDO'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 0
        end
        object RQ_VACACI: TDBEdit
          Left = 66
          Top = 30
          Width = 140
          Height = 21
          DataField = 'RQ_VACACI'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 1
        end
        object RQ_BONO: TDBEdit
          Left = 271
          Top = 30
          Width = 140
          Height = 21
          DataField = 'RQ_BONO'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 3
        end
        object RQ_VALES: TDBEdit
          Left = 458
          Top = 6
          Width = 140
          Height = 21
          DataField = 'RQ_VALES'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 4
        end
        object RQ_AGUINAL: TDBEdit
          Left = 271
          Top = 6
          Width = 140
          Height = 21
          DataField = 'RQ_AGUINAL'
          DataSource = DataSource
          MaxLength = 30
          TabOrder = 2
        end
      end
    end
    object Observaciones: TTabSheet
      Caption = 'Observaciones'
      ImageIndex = 5
      object RQ_OBSERVA: TDBMemo
        Left = 0
        Top = 0
        Width = 604
        Height = 215
        Align = alClient
        DataField = 'RQ_OBSERVA'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Anuncio: TTabSheet
      Caption = 'Anuncio'
      ImageIndex = 6
      object RQ_ANUNCIO: TDBMemo
        Left = 0
        Top = 30
        Width = 604
        Height = 185
        Align = alClient
        DataField = 'RQ_ANUNCIO'
        DataSource = DataSource
        ScrollBars = ssVertical
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 604
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 177
          Top = 7
          Width = 85
          Height = 13
          Caption = 'Enviar Anuncio A:'
        end
        object BtnEnviar: TSpeedButton
          Left = 575
          Top = 4
          Width = 23
          Height = 22
          Hint = 'Enviar Anuncio por Email'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            3333333333333333333333333333333333333FFFFFFFFFFFFFFF000000000000
            000077777777777777770FFFFFFFFFFFFFF07F3333FFF33333370FFFF777FFFF
            FFF07F333777333333370FFFFFFFFFFFFFF07F3333FFFFFF33370FFFF777777F
            FFF07F33377777733FF70FFFFFFFFFFF99907F3FFF33333377770F777FFFFFFF
            9CA07F77733333337F370FFFFFFFFFFF9A907FFFFFFFFFFF7FF7000000000000
            0000777777777777777733333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333333333333333
            3333333333333333333333333333333333333333333333333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnEnviarClick
        end
        object BRedactarAnuncio: TBitBtn
          Left = 6
          Top = 2
          Width = 129
          Height = 25
          Hint = 'Redactar Autom'#225'ticamente el Anuncio'
          Caption = 'Redactar Anuncio'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BRedactarAnuncioClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
        end
        object eMailContacto: TEdit
          Left = 267
          Top = 4
          Width = 300
          Height = 21
          TabOrder = 1
          Text = 'eMailContacto'
        end
      end
    end
    object Gastos: TTabSheet
      Caption = 'Gastos'
      ImageIndex = 7
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 604
        Height = 30
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label26: TLabel
          Left = 312
          Top = 6
          Width = 62
          Height = 13
          Caption = 'Presupuesto:'
        end
        object RQ_GTOPRES2: TZetaDBTextBox
          Left = 380
          Top = 4
          Width = 65
          Height = 19
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'RQ_GTOPRES2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'RQ_GTOPRES'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label27: TLabel
          Left = 463
          Top = 6
          Width = 56
          Height = 13
          Caption = 'Gasto Real:'
        end
        object BtnAgregarGasto: TSpeedButton
          Left = 13
          Top = 2
          Width = 80
          Height = 24
          Hint = 'Agregar Gasto'
          Caption = 'Agregar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
            333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
            0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333337F33333337F333330FFFFFFFF07333337F33333337F333330FFFFFFFF
            07333FF7F33333337FFFBBB0FFFFFFFF0BB37777F3333333777F3BB0FFFFFFFF
            0BBB3777F3333FFF77773330FFFF000003333337F333777773333330FFFF0FF0
            33333337F3337F37F3333330FFFF0F0B33333337F3337F77FF333330FFFF003B
            B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
            3BB33773333773333773B333333B3333333B7333333733333337}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnAgregarGastoClick
        end
        object BtnBorrarGasto: TSpeedButton
          Left = 95
          Top = 2
          Width = 80
          Height = 24
          Hint = 'Borrar Gasto'
          Caption = 'Borrar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
            55555FFFFFFF5F55FFF5777777757559995777777775755777F7555555555550
            305555555555FF57F7F555555550055BB0555555555775F777F55555550FB000
            005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
            B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
            B05555577F555557F7F5550E0BFBFB00B055557575F55577F7F550EEE0BFB0B0
            B05557FF575F5757F7F5000EEE0BFBF0B055777FF575FFF7F7F50000EEE00000
            B0557777FF577777F7F500000E055550805577777F7555575755500000555555
            05555777775555557F5555000555555505555577755555557555}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnBorrarGastoClick
        end
        object BtnModificarGasto: TSpeedButton
          Left = 177
          Top = 2
          Width = 80
          Height = 24
          Hint = 'Modificar Gasto'
          Caption = 'Modificar'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            0400000000000001000000000000000000001000000010000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
            000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
            00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
            F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
            0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
            FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
            FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
            0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
            00333377737FFFFF773333303300000003333337337777777333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BtnModificarGastoClick
        end
        object eGastoTotal: TZetaTextBox
          Left = 524
          Top = 4
          Width = 65
          Height = 19
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'eGastoTotal'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
      end
      object GridGastos: TZetaDBGrid
        Left = 0
        Top = 30
        Width = 604
        Height = 185
        Align = alClient
        DataSource = dsGastos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = BtnModificarGastoClick
        Columns = <
          item
            Expanded = False
            FieldName = 'RG_FOLIO'
            Title.Caption = 'Folio'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RG_TIPO'
            Title.Caption = 'Tipo'
            Width = 95
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RG_FECHA'
            Title.Caption = 'Fecha'
            Width = 75
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RG_DESCRIP'
            Title.Caption = 'Descripci'#243'n'
            Width = 250
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RG_MONTO'
            Title.Caption = 'Monto'
            Width = 85
            Visible = True
          end>
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 433
    Top = 71
  end
  object dsCandidatos: TDataSource
    Left = 440
    Top = 80
  end
  object dsGastos: TDataSource
    Left = 448
    Top = 88
  end
end
