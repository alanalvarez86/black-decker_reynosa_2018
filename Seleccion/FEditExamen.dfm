inherited EditExamen: TEditExamen
  Left = 229
  Top = 116
  Caption = 'Resultados De Un Examen'
  ClientHeight = 414
  ClientWidth = 467
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [0]
    Left = 42
    Top = 113
    Width = 33
    Height = 13
    Caption = '&Fecha:'
  end
  object Label4: TLabel [1]
    Left = 51
    Top = 136
    Width = 24
    Height = 13
    Caption = '&Tipo:'
  end
  object Label5: TLabel [2]
    Left = 12
    Top = 159
    Width = 63
    Height = 13
    Caption = '&Aplicado Por:'
  end
  object Label6: TLabel [3]
    Left = 27
    Top = 185
    Width = 48
    Height = 13
    Caption = '&Resumen:'
  end
  object Label11: TLabel [4]
    Left = 1
    Top = 207
    Width = 74
    Height = 13
    Caption = 'Obser&vaciones:'
  end
  inherited PanelBotones: TPanel
    Top = 378
    Width = 467
    inherited OK: TBitBtn
      Left = 299
    end
    inherited Cancelar: TBitBtn
      Left = 384
    end
  end
  inherited PanelSuperior: TPanel
    Width = 467
  end
  inherited PanelIdentifica: TPanel
    Width = 467
    inherited ValorActivo2: TPanel
      Width = 141
    end
  end
  object Panel1: TPanel [8]
    Left = 0
    Top = 51
    Width = 467
    Height = 53
    Align = alTop
    TabOrder = 3
    object Label1: TLabel
      Left = 60
      Top = 8
      Width = 43
      Height = 13
      Caption = 'Solicitud:'
    end
    object Label2: TLabel
      Left = 62
      Top = 29
      Width = 41
      Height = 13
      Caption = 'Examen:'
    end
    object SO_FOLIO: TZetaDBTextBox
      Left = 110
      Top = 6
      Width = 50
      Height = 17
      AutoSize = False
      Caption = 'SO_FOLIO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'SO_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object EX_FOLIO: TZetaDBTextBox
      Left = 110
      Top = 27
      Width = 50
      Height = 17
      AutoSize = False
      Caption = 'EX_FOLIO'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
      DataField = 'EX_FOLIO'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object PRETTYNAME: TZetaTextBox
      Left = 163
      Top = 6
      Width = 300
      Height = 17
      AutoSize = False
      Caption = 'PRETTYNAME'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ShowAccelChar = False
      Brush.Color = clBtnFace
      Border = True
    end
  end
  object EX_FECHA: TZetaDBFecha [9]
    Left = 77
    Top = 109
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 4
    Text = '01/Jul/01'
    Valor = 37073
    DataField = 'EX_FECHA'
    DataSource = DataSource
  end
  object EX_TIPO: TZetaDBKeyCombo [10]
    Left = 77
    Top = 133
    Width = 115
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 5
    ListaFija = lfTipoExamen
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    DataField = 'EX_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object EX_APLICO: TDBEdit [11]
    Left = 77
    Top = 156
    Width = 386
    Height = 21
    DataField = 'EX_APLICO'
    DataSource = DataSource
    TabOrder = 6
  end
  object EX_RESUMEN: TDBEdit [12]
    Left = 77
    Top = 180
    Width = 386
    Height = 21
    DataField = 'EX_RESUMEN'
    DataSource = DataSource
    TabOrder = 7
  end
  object GroupBox1: TGroupBox [13]
    Left = 78
    Top = 327
    Width = 221
    Height = 49
    Caption = ' Evaluaciones '
    TabOrder = 10
    object Label8: TLabel
      Left = 8
      Top = 22
      Width = 9
      Height = 13
      Caption = '1:'
    end
    object Label9: TLabel
      Left = 78
      Top = 22
      Width = 9
      Height = 13
      Caption = '2:'
    end
    object Label10: TLabel
      Left = 148
      Top = 22
      Width = 9
      Height = 13
      Caption = '3:'
    end
    object EX_EVALUA1: TZetaDBNumero
      Left = 24
      Top = 18
      Width = 45
      Height = 21
      Mascara = mnHoras
      TabOrder = 0
      Text = '0.00'
      DataField = 'EX_EVALUA1'
      DataSource = DataSource
    end
    object EX_EVALUA2: TZetaDBNumero
      Left = 94
      Top = 18
      Width = 45
      Height = 21
      Mascara = mnHoras
      TabOrder = 1
      Text = '0.00'
      DataField = 'EX_EVALUA2'
      DataSource = DataSource
    end
    object EX_EVALUA3: TZetaDBNumero
      Left = 164
      Top = 18
      Width = 45
      Height = 21
      Mascara = mnHoras
      TabOrder = 2
      Text = '0.00'
      DataField = 'EX_EVALUA3'
      DataSource = DataSource
    end
  end
  object EX_OBSERVA: TDBMemo [14]
    Left = 77
    Top = 203
    Width = 387
    Height = 107
    DataField = 'EX_OBSERVA'
    DataSource = DataSource
    ScrollBars = ssVertical
    TabOrder = 8
  end
  object EX_APROBO: TDBCheckBox [15]
    Left = 35
    Top = 311
    Width = 55
    Height = 17
    Alignment = taLeftJustify
    Caption = 'Aprob�:'
    DataField = 'EX_APROBO'
    DataSource = DataSource
    TabOrder = 9
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  inherited DataSource: TDataSource
    Left = 304
    Top = 105
  end
end
