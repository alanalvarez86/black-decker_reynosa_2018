inherited WizDepuraSeleccion: TWizDepuraSeleccion
  Left = 210
  Top = 123
  ActiveControl = nil
  Caption = 'Depuraci�n de Requisiciones y Solicitudes'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    BeforeMove = WizardBeforeMove
  end
  inherited PageControl: TPageControl
    inherited Parametros: TTabSheet
      object RequiereGB: TGroupBox
        Left = 73
        Top = 15
        Width = 257
        Height = 94
        TabOrder = 1
        object FechaLBL: TLabel
          Left = 27
          Top = 34
          Width = 60
          Height = 13
          Caption = '&Anteriores A:'
          FocusControl = FechaReq
        end
        object Label3: TLabel
          Left = 54
          Top = 59
          Width = 33
          Height = 13
          Caption = '&Status:'
        end
        object FechaReq: TZetaFecha
          Left = 90
          Top = 30
          Width = 140
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '30/Jun/00'
          Valor = 36707
        end
        object cbStatus: TComboBox
          Left = 90
          Top = 55
          Width = 140
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
        end
      end
      object SolicitaGB: TGroupBox
        Left = 73
        Top = 127
        Width = 257
        Height = 94
        TabOrder = 3
        object Label1: TLabel
          Left = 27
          Top = 34
          Width = 60
          Height = 13
          Caption = '&Anteriores A:'
          FocusControl = FechaSol
        end
        object SolStatusLBL: TLabel
          Left = 54
          Top = 62
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = '&Status:'
          FocusControl = SolStatus
        end
        object FechaSol: TZetaFecha
          Left = 90
          Top = 30
          Width = 140
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '30/Jun/00'
          Valor = 36707
        end
        object SolStatus: TComboBox
          Left = 90
          Top = 58
          Width = 140
          Height = 21
          TabStop = False
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 1
        end
      end
      object CBRequiere: TCheckBox
        Tag = 1
        Left = 77
        Top = 14
        Width = 170
        Height = 17
        Caption = 'Borrar Datos De &Requisiciones'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CBRequiereClick
      end
      object CBSolicita: TCheckBox
        Left = 77
        Top = 126
        Width = 156
        Height = 17
        Caption = 'Borrar Datos de &Solicitudes'
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CBRequiereClick
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Lines.Strings = (
          'Este proceso Borra las Requisiciones y Solicitudes que '
          'tengan fecha anterior a la que se indique y que '
          'correspondan con el Status especificado'
          ''
          'Presione el bot�n '#39'Ejecutar'#39' para iniciar el Proceso.')
      end
    end
  end
end
