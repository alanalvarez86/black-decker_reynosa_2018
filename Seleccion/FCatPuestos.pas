unit FCatPuestos;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid;

type
  TCatPuestos = class(TBaseConsulta)
    ZetaDBGrid1: TZetaDBGrid;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
    procedure DoLookup; override;
  end;

var
  CatPuestos: TCatPuestos;

implementation

uses DSeleccion,
     ZetaBuscador,
     ZetaCommonClasses;

{$R *.DFM}

procedure TCatPuestos.FormCreate(Sender: TObject);
begin
     inherited;
     CanLookup := True;
     HelpContext:= H00071_Puestos;
end;

procedure TCatPuestos.Connect;
begin
     with dmSeleccion do
     begin
          cdsAreaInteres.Conectar;
          cdsPuestos.Conectar;
          DataSource.DataSet:= cdsPuestos;
     end;
end;

procedure TCatPuestos.Refresh;
begin
     dmSeleccion.cdsPuestos.Refrescar;
end;

procedure TCatPuestos.Agregar;
begin
     inherited;
     dmSeleccion.cdsPuestos.Agregar;
end;

procedure TCatPuestos.Borrar;
begin
     inherited;
     dmSeleccion.cdsPuestos.Borrar;
end;

procedure TCatPuestos.Modificar;
begin
     inherited;
     dmSeleccion.cdsPuestos.Modificar;
end;

procedure TCatPuestos.DoLookup;
begin
     inherited;
     ZetaBuscador.BuscarCodigo( 'C�digo', 'Puestos', 'PU_CODIGO', dmSeleccion.cdsPuestos );
end;

end.
