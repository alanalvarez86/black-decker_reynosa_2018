unit FEditCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Mask, ZetaEdit,
  ZetaNumero, ComCtrls, ZetaSmartLists;

type
  TEditCliente = class(TBaseEdicion)
    PanelCodigo: TPanel;
    CL_CODIGO: TZetaDBEdit;
    CL_NOMBRE: TDBEdit;
    CL_CODIGOLbl: TLabel;
    CL_NOMBRELbl: TLabel;
    PageControl: TPageControl;
    Generales: TTabSheet;
    CL_DESCRIPLbl: TLabel;
    CL_DESCRIP: TDBEdit;
    CL_INDUSTRLbl: TLabel;
    CL_INDUSTR: TDBEdit;
    CL_EMPLEADLbl: TLabel;
    CL_EMPLEAD: TZetaDBNumero;
    CL_DIRECCLbl: TLabel;
    CL_DIRECC: TDBEdit;
    CL_TELLbl: TLabel;
    CL_TEL: TDBEdit;
    CL_FAXLbl: TLabel;
    CL_FAX: TDBEdit;
    Contactos: TTabSheet;
    Notas: TTabSheet;
    CL_OBSERVA: TDBMemo;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    CL_NOM_C1: TDBEdit;
    CL_PUES_C1: TDBEdit;
    CL_MAIL_C1: TDBEdit;
    CL_DESC_C1: TDBEdit;
    Label4: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    CL_NOM_C2: TDBEdit;
    CL_PUES_C2: TDBEdit;
    CL_MAIL_C2: TDBEdit;
    CL_DESC_C2: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    GroupBox3: TGroupBox;{OP:14.Abr.08}
    CL_NOM_AULbl: TLabel;{OP:14.Abr.08}
    CL_PUES_AULbl: TLabel;{OP:14.Abr.08}
    CL_NOM_AU: TDBEdit;{OP:14.Abr.08}
    CL_PUES_AU: TDBEdit;{OP:14.Abr.08}
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditCliente: TEditCliente;

implementation

uses dSeleccion, ZAccesosTress,ZetaCommonClasses;

{$R *.DFM}

procedure TEditCliente.FormCreate(Sender: TObject);
begin
     FirstControl := CL_CODIGO;
     inherited;
     IndexDerechos := D_CAT_CLIENTES;
     HelpContext := H00080_Clientes;
     PageControl.ActivePage := Generales;
end;

procedure TEditCliente.Connect;
begin
     Datasource.Dataset := dmSeleccion.cdsCliente;
end;

end.
