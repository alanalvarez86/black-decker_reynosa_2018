unit DSeleccion;

interface

{$DEFINE MULTIPLES_ENTIDADES}

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBClient,filectrl, ShellAPI, StdCtrls,
{$ifdef DOS_CAPAS}
     DServerSeleccion,
{$else}
     Seleccion_TLB,
{$endif}
{$ifndef VER130}
     Variants,
{$endif}
     ZetaClientDataSet, ZetaTipoEntidad, ZetaCommonLists, ZetaCommonClasses,
     ZetaAdicionalMgr,     
     ZBaseThreads, Psock, NMsmtp;

type
  TSeleccionThread = class( TBaseThread )
  private
    { Private declarations }
{$ifdef DOS_CAPAS}
    function GetServidor: TdmServerSeleccion;
    property Servidor: TdmServerSeleccion read GetServidor;
{$else}
    function GetServidor: IdmServerSeleccionDisp;
    property Servidor: IdmServerSeleccionDisp read GetServidor;
{$endif}
  protected
    { Protected declarations }
    function Procesar: OleVariant; override;
  end;

  TdmSeleccion = class(TDataModule)
    cdsCondiciones: TZetaLookupDataSet;
    cdsPuestos: TZetaLookupDataSet;
    cdsTipoGasto: TZetaLookupDataSet;
    cdsCliente: TZetaLookupDataSet;
    cdsAreaInteres: TZetaLookupDataSet;
    cdsRequiere: TZetaClientDataSet;
    cdsReqCandidatos: TZetaClientDataSet;
    cdsExamenes: TZetaClientDataSet;
    cdsReqGastos: TZetaClientDataSet;
    cdsSolicita: TZetaClientDataSet;
    cdsSolCandidatos: TZetaClientDataSet;
    cdsEditRequiere: TZetaClientDataSet;
    cdsEditSolicita: TZetaClientDataSet;
    cdsEntrevista: TZetaClientDataSet;
    cdsAdicional1: TZetaLookupDataSet;
    cdsAdicional2: TZetaLookupDataSet;
    cdsAdicional3: TZetaLookupDataSet;
    cdsAdicional4: TZetaLookupDataSet;
    cdsAdicional5: TZetaLookupDataSet;
    cdsRefLaboral: TZetaClientDataSet;
    Email: TNMSMTP;
    cdsLookUpSolicita: TZetaLookupDataSet;
    cdsLookupRequiere: TZetaLookupDataSet;
    cdsDocumentos: TZetaClientDataSet;
    cdsEditDocumentos: TZetaClientDataSet;
    cdsEstados: TZetaLookupDataSet;
    cdsContratos: TZetaLookupDataSet;
    cdsTransporte: TZetaLookupDataSet;
    procedure cdsTablasAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAlAdquirirDatos(Sender: TObject);
    procedure cdsCondicionesAlAdquirirDatos(Sender: TObject);
    procedure cdsPuestosAlCrearCampos(Sender: TObject);
    procedure cdsEditRequiereAlAdquirirDatos(Sender: TObject);
    procedure cdsEditSolicitaAlAdquirirDatos(Sender: TObject);
    procedure cdsSolicitaAlAgregar(Sender: TObject);
    procedure cdsEditSolicitaAlModificar(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure cdsSolCandidatosAlCrearCampos(Sender: TObject);
    procedure cdsSolicitaAlModificar(Sender: TObject);
    procedure cdsEditSolicitaNewRecord(DataSet: TDataSet);
    procedure cdsExamenesAlBorrar(Sender: TObject);
    procedure cdsExamenesAlModificar(Sender: TObject);
    procedure cdsExamenesNewRecord(DataSet: TDataSet);
    procedure cdsPuestosAlAgregar(Sender: TObject);
    procedure cdsPuestosAlBorrar(Sender: TObject);
    procedure GetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTablaAlModificar(Sender: TObject);
    procedure cdsTablasAlEnviarDatos(Sender: TObject);
    procedure cdsCondicionesAlAgregar(Sender: TObject);
    procedure cdsCondicionesAlBorrar(Sender: TObject);
    procedure cdsEditSolicitaAlEnviarDatos(Sender: TObject);
    procedure cdsReqGastosAlAgregar(Sender: TObject);
    procedure cdsReqGastosAlBorrar(Sender: TObject);
    procedure cdsReqGastosAlModificar(Sender: TObject);
    procedure cdsRequiereAlAgregar(Sender: TObject);
    procedure cdsRequiereAlModificar(Sender: TObject);
    procedure cdsRequiereAlCrearCampos(Sender: TObject);
    procedure cdsEditRequiereAlEnviarDatos(Sender: TObject);
    procedure cdsEditRequiereAlModificar(Sender: TObject);
    procedure cdsEditRequiereNewRecord(DataSet: TDataSet);
    procedure cdsEntrevistaAlAdquirirDatos(Sender: TObject);
    procedure cdsEntrevistaAlCrearCampos(Sender: TObject);
    procedure cdsReqCandidatosAlModificar(Sender: TObject);
    procedure cdsSolicitaAlCrearCampos(Sender: TObject);
    procedure cdsEntrevistaAlModificar(Sender: TObject);
    procedure cdsReqCandidatosAlCrearCampos(Sender: TObject);
    procedure cdsReqGastosAlCrearCampos(Sender: TObject);
    procedure cdsReqGastosNewRecord(DataSet: TDataSet);
    procedure cdsEntrevistaNewRecord(DataSet: TDataSet);
    procedure cdsEntrevistaAlAgregar(Sender: TObject);
    {$ifdef VER130}
    procedure cdsReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSolicitaReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEditDocumentosReconcileError(DataSet: TClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    {$else}
    procedure cdsReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsSolicitaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
    procedure cdsEditDocumentosReconcileError(DataSet: TCustomClientDataSet;E: EReconcileError; UpdateKind: TUpdateKind;var Action: TReconcileAction);
    {$endif}
    procedure cdsEditSolicitaAfterCancel(DataSet: TDataSet);
    procedure cdsSolicitaAfterDelete(DataSet: TDataSet);
    procedure cdsReqCandidatosAlAgregar(Sender: TObject);
    procedure cdsReqCandidatosAlBorrar(Sender: TObject);
    procedure cdsReqCandidatosAlEnviarDatos(Sender: TObject);
    procedure cdsRequiereAfterDelete(DataSet: TDataSet);
    procedure cdsExamenesAlCrearCampos(Sender: TObject);
    procedure cdsEditRequiereAlCrearCampos(Sender: TObject);
    procedure cdsReqGastosAlEnviarDatos(Sender: TObject);
    procedure cdsEntrevistaAlEnviarDatos(Sender: TObject);
    procedure cdsEntrevistaAlBorrar(Sender: TObject);
    procedure cdsEditSolicitaBeforePost(DataSet: TDataSet);
    procedure cdsEditRequiereBeforePost(DataSet: TDataSet);
    procedure cdsRefLaboralAlModificar(Sender: TObject);
    procedure cdsDocumentosAlBorrar(Sender: TObject);
    procedure cdsDocumentosAlModificar(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure cdsEntrevistaBeforePost(DataSet: TDataSet);
    procedure EmailFailure(Sender: TObject);
//    procedure cdsRefLaboralAlEnviarDatos(Sender: TObject);
    procedure cdsEditSolicitaAlAgregar(Sender: TObject);
    procedure cdsDocumentosAlCrearCampos(Sender: TObject);
    procedure cdsDocumentosCalcFields(DataSet: TDataSet);
    procedure cdsClienteBeforePost(DataSet: TDataSet);
    procedure cdsExamenesBeforePost(DataSet: TDataSet);
    procedure cdsPuestosBeforePost(DataSet: TDataSet);
    procedure cdsEditRequiereAlAgregar(Sender: TObject);
    procedure cdsTablasAfterDelete(DataSet: TDataSet);
    procedure cdsDocumentosAlAgregar(Sender: TObject);
    procedure cdsEditDocumentosAlAdquirirDatos(Sender: TObject);
    procedure cdsEditDocumentosBeforePost(DataSet: TDataSet);
    procedure cdsEditDocumentosNewRecord(DataSet: TDataSet);
    procedure cdsEditDocumentosAlModificar(Sender: TObject);
    procedure cdsEditDocumentosAlEnviarDatos(Sender: TObject);
    procedure cdsEditSolicitaAlCrearCampos(Sender: TObject);
    procedure cdsEstadosAlAdquirirDatos(Sender: TObject);
    procedure cdsContratosAlAdquirirDatos(Sender: TObject);
    procedure cdsEstadosGetRights(Sender: TZetaClientDataSet;
      const iRight: Integer; var lHasRights: Boolean);
    procedure cdsTransporteAlAdquirirDatos(Sender: TObject);
  private
    { Private declarations }
    lEditandoReq, FRequiereNavegador, FCambioAdicionales, FReemplazaDoc: Boolean;
    FFolioEntrevista, FFolioSolicitud, FFolioRequisicion : integer;
    FFolioGasto: integer;
    FOperacion: eOperacionConflicto;
    FListaProcesos: TProcessList;
{$ifdef DOS_CAPAS}
    function GetServerSeleccion: TdmServerSeleccion;
    property ServerSeleccion: TdmServerSeleccion read GetServerSeleccion;
{$else}
    FServidor: IdmServerSeleccionDisp;
    function GetServerSeleccion: IdmServerSeleccionDisp;
    property ServerSeleccion: IdmServerSeleccionDisp read GetServerSeleccion;
{$endif}
    function GetTipoDocumento: String;
    function GetFolioRequisicion: Integer;
    function GetFolioSolicitud: Integer;
    function GetCambioAdicionales: Boolean;
    function GetIndexRights( const iTabla: Integer ): Integer;
    function CorreThreadSeleccion(const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams): Boolean;
    function CheckAuto: Boolean;
    procedure ValidaMemoField(DataSet: TDataSet; const sField: string);
    procedure EdadGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure PrettyNameGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ShowEdicion(const iTabla: Integer);
    procedure GrabaReferencias(const iFolio: Integer);
    procedure LimpiaArchivosTemporales;
    procedure Nombre_NacChange(Sender: TField);
    procedure RQ_CLIENTEChange(Sender: TField);{OP:15.Abr.08}
  public
    { Public declarations }
    property FolioSolicitud : Integer read GetFolioSolicitud write FFolioSolicitud;
    property FolioRequisicion : Integer read GetFolioRequisicion write FFolioRequisicion;
    property EditandoReq: Boolean read lEditandoReq;
    property RequiereNavegador: Boolean read FRequiereNavegador;
    property CambioAdicionales: Boolean read GetCambioAdicionales;
    //function AgregaCandidatos(Parametros: TZetaParams;  var sMensaje: String): Boolean;
    function GetcdsSolicitudes: TZetaClientDataSet;
    function GetcdsAdicional( const iGlobal: word ): TZetaLookupDataSet;
    function CalculaGastoReal: TPesos ;
    function Depuracion(Parametros: TZetaParams): Boolean;
    function CargaDocumento(const sArchivo: string): Boolean;
    function AgregaUnCandidato: Boolean;
    function GetCampoAdicional( const iPosicion: integer ): string;
    function SetControlesEmpresaTress( lblLabel: TLabel; lblControl : TControl ): boolean;    
    procedure RefrescarRequiere(Parametros: TZetaParams);
    procedure RefrescarSolicita(Parametros: TZetaParams; const lBusqueda: Boolean = FALSE );
    procedure ModificaSolicitud(const iFolio: Integer; const lNavegar : Boolean);
    procedure SetLookupNames;
    procedure SetTotalesRequisicion( var iCandidatos, iContratados, iSeleccionados : integer);
    procedure GetLastFolioEntrevista;
    procedure GetLastFolioGasto;
    {$ifdef MULTIPLES_ENTIDADES}
    procedure NotifyDataChange(const Entidades: Array of TipoEntidad);
    {$else}
    procedure NotifyDataChange(const Entidades: ListaEntidades);
    {$endif}
    procedure EnviarAnuncio(const sAnuncio, eMailAnuncio: string);
    procedure AbreDocumento;
    procedure HandleProcessEnd(const iIndice, iValor: Integer);
    procedure AgregaRefLaborales;
    procedure CargaListaInteres(oLista, oListaCodigos: TStringList);
    procedure SetCambiosDocumentos;
    procedure LlenaAdicionales( FCampos: TListaCampos );
  end;

procedure ShowWizard( const eProceso: Procesos );

const
     K_AREA_INTERES = 0;
     K_CLIENTE = 1;
     K_TIPO_GASTO = 2;
     K_PUESTO = 3;
     K_CONDICIONES = 4;
     K_ADICIONAL_1 = 13;
     K_ADICIONAL_2 = 14;
     K_ADICIONAL_3 = 15;
     K_ADICIONAL_4 = 16;
     K_ADICIONAL_5 = 17;
var
  dmSeleccion: TdmSeleccion;

implementation

uses DCliente,
     DGlobal,
     DConsultas,
     FEditSolicitud,
     FEditRequisicion,
     FEditExamen,
     FEditPuesto,
     FEditTablas,
     FEditCatCondiciones,
     FEditGasto,
     FEditCandidato,
     FEditEntrevista,
     FBuscaSolicitud,
     FEditRefLaboral,
     FEditCliente,
     FEditDocumento,
     FWizDepuraSeleccion,
     FAltaRequisicion,
     FAltaSolicitud,
     FAltaWizardDlg,
     FTressShell,
     FValidaStatusSolicitud,
     FAutoClasses,
     ZReconcile,
     ZAccesosMgr,
     ZGlobalTress,
     ZAccesosTress,
     ZWizardBasico,
     ZetaWizardFeedBack,
     ZetaDialogo,
     ZetaMsgDlg,
     ZetaCommonTools,
     ZetaFilesTools,
     ZetaClientTools,
     ZetaTipoEntidadTools,
     ZBaseEdicion;

{$R *.DFM}

procedure ShowWizard( const eProceso: Procesos );
var
   WizardClass: TWizardBasicoClass;
begin
     WizardClass := nil;
     case eProceso of
          prDepuraSeleccion: WizardClass := TWizDepuraSeleccion;
     end;
     if ( WizardClass = nil ) then { Este IF debe ser Temporal }
        ZetaDialogo.zInformation( 'Estamos Trabajando...', 'Esta Opci�n No Ha Sido Implementada', 0 )
     else
        ZWizardBasico.ShowWizard( WizardClass );
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

{ ********** TSeleccionThread ********** }

{$ifdef DOS_CAPAS}
function TSeleccionThread.GetServidor: TdmServerSeleccion;
begin
     Result := DCliente.dmCliente.ServerSeleccion;
end;

{$else}
function TSeleccionThread.GetServidor: IdmServerSeleccionDisp;
begin
     Result := IdmServerSeleccionDisp( CreateServer( CLASS_dmServerSeleccion ) );
end;
{$endif}

function TSeleccionThread.Procesar: OleVariant;
begin
     case Proceso of
          prDepuraSeleccion: Result := Servidor.Depuracion( Empresa, Parametros.VarValues );
     end;
end;

{ ********** TdmSeleccion ********** }

procedure TdmSeleccion.DataModuleCreate(Sender: TObject);
begin
     lEditandoReq := FALSE;
     FFolioSolicitud := -1;
     FFolioRequisicion := -1;
     FCambioAdicionales := TRUE;
     cdsAreaInteres.Tag := K_AREA_INTERES;
     cdsCliente.Tag := K_CLIENTE;
     cdsPuestos.Tag := K_PUESTO;
     cdsCondiciones.Tag := K_CONDICIONES;
     cdsTipoGasto.Tag := K_TIPO_GASTO;
     cdsAdicional1.Tag := K_ADICIONAL_1;
     cdsAdicional2.Tag := K_ADICIONAL_2;
     cdsAdicional3.Tag := K_ADICIONAL_3;
     cdsAdicional4.Tag := K_ADICIONAL_4;
     cdsAdicional5.Tag := K_ADICIONAL_5;

     FListaProcesos := TProcessList.Create;
end;

procedure TdmSeleccion.DataModuleDestroy(Sender: TObject);
begin
     FListaProcesos.Free;
     LimpiaArchivosTemporales;
end;

{$ifdef DOS_CAPAS}
function TdmSeleccion.GetServerSeleccion: TdmServerSeleccion;
begin
     Result := DCliente.dmCliente.ServerSeleccion;
end;
{$else}
function TdmSeleccion.GetServerSeleccion: IdmServerSeleccionDisp;
begin
     Result := IdmServerSeleccionDisp( dmCliente.CreaServidor( CLASS_dmServerSeleccion, FServidor ) );
end;
{$endif}

function TdmSeleccion.CheckAuto: Boolean;
begin
     Result := Autorizacion.OkModulo( OkSeleccion, True );
     if not Result then
        ZInformation( 'Versi�n Demo', 'Operaci�n No Permitida En Versi�n Demo' , 0);
end;

{$ifdef VER130}
procedure TdmSeleccion.cdsReconcileError( DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$else}
procedure TdmSeleccion.cdsReconcileError( DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
begin
     Action := ZReconcile.HandleReconcileError(Dataset, UpdateKind, E);
end;
{$endif}

{$ifdef MULTIPLES_ENTIDADES}
procedure TdmSeleccion.NotifyDataChange(const Entidades: Array of TipoEntidad);
{$else}
procedure TdmSeleccion.NotifyDataChange(const Entidades: ListaEntidades);
{$endif}
begin
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enSolicitud , Entidades ) then
     {$else}
     if ( enSolicitud in Entidades ) then
     {$endif}
     begin
          cdsSolicita.SetDataChange;
     end;
     {$ifdef MULTIPLES_ENTIDADES}
     if Dentro( enRequisicion , Entidades ) then
     {$else}
     if ( enRequisicion in Entidades ) then
     {$endif}
     begin
          cdsRequiere.SetDataChange;
     end;
end;

procedure TdmSeleccion.SetLookupNames;
begin
     FCambioAdicionales := TRUE;
     cdsAdicional1.LookupName := Global.GetGlobalString(K_GLOBAL_TAB1);
     cdsAdicional2.LookupName := Global.GetGlobalString(K_GLOBAL_TAB2);
     cdsAdicional3.LookupName := Global.GetGlobalString(K_GLOBAL_TAB3);
     cdsAdicional4.LookupName := Global.GetGlobalString(K_GLOBAL_TAB4);
     cdsAdicional5.LookupName := Global.GetGlobalString(K_GLOBAL_TAB5);
end;

function TdmSeleccion.GetcdsSolicitudes: TZetaClientDataSet;
begin
     if lEditandoReq then
        Result := cdsReqCandidatos
     else
         Result := cdsSolicita;        // Aqu� se condicionar� si esta en Requisicion o en Solicitudes
end;

function TdmSeleccion.GetcdsAdicional( const iGlobal: word ): TZetaLookupDataSet;
begin
     Result := nil;
     case iGlobal of
          K_GLOBAL_TAB1 : Result := cdsAdicional1;
          K_GLOBAL_TAB2 : Result := cdsAdicional2;
          K_GLOBAL_TAB3 : Result := cdsAdicional3;
          K_GLOBAL_TAB4 : Result := cdsAdicional4;
          K_GLOBAL_TAB5 : Result := cdsAdicional5;
     end;
end;

function TdmSeleccion.GetCambioAdicionales: Boolean;
begin
     Result := FCambioAdicionales;
     FCambioAdicionales := FALSE;
end;

{ ********* Wizards *********** }

function TdmSeleccion.CorreThreadSeleccion( const eProceso: Procesos; const oLista: OleVariant; oParametros: TZetaParams ): Boolean;
begin
     with TSeleccionThread.Create( FListaProcesos ) do
     begin
          Proceso := eProceso;
          Employees := oLista;
          CargaParametros( oParametros );
          Resume;
     end;
     Result := True;
end;

procedure TdmSeleccion.HandleProcessEnd( const iIndice, iValor: Integer );

   procedure SetDataChange( const Proceso: Procesos );
   begin
        case Proceso of
             prDepuraSeleccion: TressShell.SetDataChange( [ enSolicitud, enRequisicion ] );
        end;
   end;

begin
     with TWizardFeedback.Create( Application ) do
     begin
          try
             with FListaProcesos do
             begin
                  try
                     with LockList do
                     begin
                          if ( iIndice >= 0 ) and ( iIndice < Count ) then
                             ProcessData.Assign( TProcessInfo( Items[ iIndice ] ) )
                          else
                              raise Exception.Create( 'Indice De Lista De Procesos Fuera De Rango ( ' + IntToStr( iIndice ) + ' )' );
                     end;
                  finally
                         UnlockList;
                  end;
             end;
             if ProcessOK then
                SetMensajes( '' );
             dmConsultas.LeerUnProceso( ShowProcessInfo( True ) );
             if ProcessOK then
                SetDataChange( ProcessData.Proceso );
          finally
             Free;
          end;
     end;
     TressShell.ReconectaMenu;
     Application.ProcessMessages;
end;

function TdmSeleccion.Depuracion(Parametros: TZetaParams): Boolean;
begin
     Result := CorreThreadSeleccion( prDepuraSeleccion, NULL, Parametros );
end;

{ ********* cdsTablas *********** }

procedure TdmSeleccion.GetRights(Sender: TZetaClientDataSet; const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := ZAccesosMgr.CheckDerecho( GetIndexRights( Sender.Tag ), iRight );
end;

procedure TdmSeleccion.ShowEdicion( const iTabla: Integer );
begin
     case iTabla of
          K_AREA_INTERES: ZBaseEdicion.ShowFormaEdicion( EditAreaInteres, TEditAreaInteres );
          K_CLIENTE: ZBaseEdicion.ShowFormaEdicion( EditCliente, TEditCliente );
          K_PUESTO: ZBaseEdicion.ShowFormaEdicion( EditPuesto, TEditPuesto );
          K_CONDICIONES: ZBaseEdicion.ShowFormaEdicion( EditCatCondiciones, TEditCatCondiciones );
          K_TIPO_GASTO: ZBaseEdicion.ShowFormaEdicion( EditTipoGasto, TEditTipoGasto );
          K_ADICIONAL_1: ZBaseEdicion.ShowFormaEdicion( EditAdicional1, TEditAdicional1 );
          K_ADICIONAL_2: ZBaseEdicion.ShowFormaEdicion( EditAdicional2, TEditAdicional2 );
          K_ADICIONAL_3: ZBaseEdicion.ShowFormaEdicion( EditAdicional3, TEditAdicional3 );
          K_ADICIONAL_4: ZBaseEdicion.ShowFormaEdicion( EditAdicional4, TEditAdicional4 );
          K_ADICIONAL_5: ZBaseEdicion.ShowFormaEdicion( EditAdicional5, TEditAdicional5 );
     end;
end;

function TdmSeleccion.GetIndexRights( const iTabla: Integer ): Integer;
begin
     case iTabla of
          K_AREA_INTERES: Result := D_CAT_AREAS_INTERES;
          K_CLIENTE: Result := D_CAT_CLIENTES;
          K_PUESTO: Result := D_CAT_PUESTOS;
          K_CONDICIONES: Result := D_CAT_GRALES_CONDICIONES;
          K_TIPO_GASTO: Result := D_CAT_TIPO_GASTOS;
          K_ADICIONAL_1: Result := D_CAT_ADICION_TABLA1;
          K_ADICIONAL_2: Result := D_CAT_ADICION_TABLA2;
          K_ADICIONAL_3: Result := D_CAT_ADICION_TABLA3;
          K_ADICIONAL_4: Result := D_CAT_ADICION_TABLA4;
          K_ADICIONAL_5: Result := D_CAT_ADICION_TABLA5;
     else
         Result := 0;
     end;
end;

procedure TdmSeleccion.cdsTablasAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerSeleccion.GetTabla( dmCliente.Empresa, Tag );
     end;
end;

procedure TdmSeleccion.cdsTablaAlModificar(Sender: TObject);
begin
     ShowEdicion( TClientDataSet( Sender ).Tag );
end;

procedure TdmSeleccion.cdsTablasAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( ChangeCount > 0 ) then
             Reconcile( ServerSeleccion.GrabaTabla( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;

procedure TdmSeleccion.cdsTablasAfterDelete(DataSet: TDataSet);
begin
     TZetaClientDataSet( DataSet ).Enviar;
end;

{ ********* cdsPuestos *********** }

procedure TdmSeleccion.cdsPuestosAlAdquirirDatos(Sender: TObject);
begin
     cdsPuestos.Data := ServerSeleccion.GetPuestos( dmCliente.Empresa );
end;

procedure TdmSeleccion.cdsPuestosAlCrearCampos(Sender: TObject);
begin
     cdsAreaInteres.Conectar;
     with cdsPuestos do
     begin
          CreateSimpleLookup( cdsAreaInteres, 'AI_DESCRIP', 'PU_AREA' );
     end;
end;

procedure TdmSeleccion.cdsPuestosAlAgregar(Sender: TObject);
begin
     cdsPuestos.Append;
     ZBaseEdicion.ShowFormaEdicion( EditPuesto, TEditPuesto );
end;

procedure TdmSeleccion.cdsPuestosAlBorrar(Sender: TObject);
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Puesto ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with cdsPuestos do
             begin
                  Delete;
                  Enviar;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmSeleccion.cdsPuestosBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField(cdsPuestos, 'PU_HABILID');
     ValidaMemoField(cdsPuestos, 'PU_ACTIVID');
end;

{ ********* cdsCondiciones *********** }

procedure TdmSeleccion.cdsCondicionesAlAdquirirDatos(Sender: TObject);
begin
     cdsCondiciones.Data := ServerSeleccion.GetCondiciones( dmCliente.Empresa );
end;

procedure TdmSeleccion.cdsCondicionesAlAgregar(Sender: TObject);
begin
     cdsCondiciones.Append;
     ZBaseEdicion.ShowFormaEdicion( EditCatCondiciones, TEditCatCondiciones );
end;

procedure TdmSeleccion.cdsCondicionesAlBorrar(Sender: TObject);
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Esta Condici�n ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with cdsCondiciones do
             begin
                  Delete;
                  Enviar;
             end;
          finally
                 Screen.Cursor := oCursor;
          end;
     end;
end;

{ ******* cdsRequiere ********* }

procedure TdmSeleccion.RefrescarRequiere(Parametros : TZetaParams);
begin
     cdsRequiere.Data := ServerSeleccion.GetRequiere( dmCliente.Empresa, Parametros.VarValues );
     cdsRequiere.ResetDataChange;
end;

procedure TdmSeleccion.cdsRequiereAlAgregar(Sender: TObject);
begin
     FFolioRequisicion := 0;
     with cdsEditRequiere do
     begin
          Refrescar;
          Agregar;
     end;
end;

procedure TdmSeleccion.cdsRequiereAlModificar(Sender: TObject);
begin
     FFolioRequisicion := -1;
     with cdsEditRequiere do
     begin
          Refrescar;
          Modificar;
     end;
end;

procedure TdmSeleccion.cdsRequiereAlCrearCampos(Sender: TObject);
begin
     cdsPuestos.Conectar;
     cdsCliente.Conectar;
     with cdsRequiere do
     begin
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'RQ_PUESTO' );
          CreateSimpleLookup( cdsCliente, 'CL_DESCRIP', 'RQ_CLIENTE' );
          ListaFija('RQ_PRIORID', lfPrioridad );
          ListaFija('RQ_STATUS', lfReqStatus );
          MaskFecha('RQ_FEC_INI');
          MaskFecha('RQ_FEC_PRO');
          MaskFecha('RQ_FEC_FIN');
          MaskNumerico('RQ_VACANTE', '#,0;-#,0;#');
          MaskNumerico('RQ_CANDIDA', '#,0;-#,0;#');
          MaskNumerico('RQ_CONTRAT', '#,0;-#,0;#');
     end;
end;

procedure TdmSeleccion.cdsRequiereAfterDelete(DataSet: TDataSet);
var
   ErrorCount : Integer;
begin
     ErrorCount := 0;
     with cdsRequiere do
     begin
          if ( ChangeCount > 0 ) then
             if Reconcile( ServerSeleccion.BorraRequisicion( dmCliente.Empresa, Delta, ErrorCount ) ) then
                TressShell.SetDataChange( [ enRequisicion ] );
     end;
end;

{ ******* cdsEditRequiere ********** }

function TdmSeleccion.GetFolioRequisicion: Integer;
begin
     Result := 0;
     with cdsRequiere do
     begin
          if ( FFolioRequisicion < 0 ) then
          begin
               if Active and not IsEmpty then
                  Result := FieldByName( 'RQ_FOLIO' ).AsInteger;
          end
          else Result := FFolioRequisicion;
     end;
     FFolioRequisicion := -1 ;
end;

procedure TdmSeleccion.cdsEditRequiereAlAdquirirDatos(Sender: TObject);
var
   Candidatos, Gastos: OleVariant;
begin
     cdsPuestos.Conectar;
     cdsEditRequiere.Data := ServerSeleccion.GetEditRequiere( dmCliente.Empresa,
                                                              FolioRequisicion,
                                                              Candidatos, Gastos );
     cdsReqCandidatos.Data := Candidatos;
     cdsReqGastos.Data := Gastos;
end;

procedure TdmSeleccion.cdsEditRequiereNewRecord(DataSet: TDataSet);
var
   dFechaInicial: TDate;
begin
     dFechaInicial := Trunc( Date );
     with cdsEditRequiere do
     begin
          FieldByName( 'RQ_FEC_INI' ).AsDateTime := dFechaInicial;
          FieldByName( 'RQ_FEC_PRO' ).AsDateTime := dFechaInicial;
          FieldByName( 'RQ_STATUS' ).AsInteger := Ord( stPendiente );
          FieldByName( 'RQ_PRIORID' ).AsInteger := Ord( prNormal );
          FieldByName( 'RQ_MOTIVO' ).AsInteger := Ord( mvRotacion );
          FieldByName( 'RQ_VACANTE' ).AsInteger := 1;
          FieldByName( 'RQ_SEXO' ).AsString := ' ';
          FieldByName( 'RQ_REEMPLA' ).AsString := ' ';{OP:11.Abr.08}
     end;
end;

procedure TdmSeleccion.cdsEditRequiereAlAgregar(Sender: TObject);
 var lNoMostrar, lCiclo : Boolean;
     iRequisicion : integer;
begin
     lEditandoReq := TRUE;
     lNoMostrar := FALSE;
     iRequisicion := 0;
     try
        with cdsEditRequiere do
        begin
             repeat
                   if NOT lNoMostrar then Append;
                   if lNoMostrar OR ZWizardBasico.ShowWizard( TAltaRequisicion ) then
                   begin
                        iRequisicion := iMax(iRequisicion,FolioRequisicion);
                        case ShowDlgAltaWizard( iRequisicion, FALSE ) of
                             opOtraAlta:
                             begin
                                  lCiclo := TRUE;
                                  lNoMostrar := FALSE;
                             end;
                             opEditar:
                             begin
                                  lCiclo := TRUE;
                                  lNoMostrar := TRUE;
                                  Modificar;
                             end
                             else
                             begin
                                  lCiclo := FALSE;
                                  lNoMostrar := FALSE;
                             end;
                        end;
                   end
                   else lCiclo := FALSE;

             until NOT lCiclo;
        end;
        TressShell.ReconectaFormaActiva;

     finally
        lEditandoReq := FALSE;
     end;
end;

procedure TdmSeleccion.cdsEditRequiereAlModificar(Sender: TObject);
begin
     lEditandoReq := TRUE;
     try
        ZBaseEdicion.ShowFormaEdicion( EditRequisicion, TEditRequisicion );
        TressShell.ReconectaFormaActiva;
     finally
        lEditandoReq := FALSE;
     end;
end;

procedure TdmSeleccion.cdsEditRequiereBeforePost(DataSet: TDataSet);
begin
     with cdsEditRequiere do
     begin
          if StrVacio(FieldByName('RQ_PUESTO').AsString) then
          begin
               FieldByName('RQ_PUESTO').FocusControl;
               DataBaseError('El Puesto no Puede Quedar Vac�o');
          end;
          if FieldByName('RQ_VACANTE').AsInteger = 0  then
          begin
               FieldByName('RQ_VACANTE').FocusControl;
               DataBaseError('El N�mero de Vacantes no Puede Ser Cero');
          end;
          if ( FieldByName( 'RQ_MOTIVO' ).AsString <> '3' ) then{OP:11.Abr.08}
             FieldByName( 'RQ_REEMPLA' ).AsString := ' ';

   end;
   ValidaMemoField(cdsEditRequiere,'RQ_OBSERVA');
   ValidaMemoField(cdsEditRequiere,'RQ_ANUNCIO');
end;

procedure TdmSeleccion.cdsEditRequiereAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iFolio: Integer;
begin
     if CheckAuto then
     begin
          ErrorCount := 0;
          with cdsEditRequiere do
          begin
               if ( State in [ dsEdit, dsInsert ] ) then
                  Post;
               if ( ChangeCount > 0 ) AND
                  cdsEditRequiere.Reconcile( ServerSeleccion.GrabaRequisicion( dmCliente.Empresa, DeltaNull, ErrorCount, iFolio ) ) then
               begin
                    TressShell.SetDataChange( [ enRequisicion, enSolicitud ] );
                    if ( iFolio > 0 ) then                     // Registro de Requisicion
                    begin
                         FFolioRequisicion := iFolio;            // Al refrescar cdsRequiere intenta posicionar este Folio
                         Edit;
                         FieldByName('RQ_FOLIO').AsInteger := FFolioRequisicion;
                         Post;
                         MergeChangeLog;
                    end;
               end;
          end;
     end;
end;

procedure TdmSeleccion.cdsEditRequiereAlCrearCampos(Sender: TObject);
begin
     cdsPuestos.Conectar;
     cdsCliente.Conectar;
     with cdsEditRequiere do
     begin
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'RQ_PUESTO' );
          CreateSimpleLookup( cdsCliente, 'CL_DESCRIP', 'RQ_CLIENTE' );
          FieldByName( 'RQ_CLIENTE' ).OnChange := RQ_CLIENTEChange;{OP:15.Abr.08}
     end;
end;

{OP:21.Abr.08}
procedure TdmSeleccion.RQ_CLIENTEChange(Sender: TField);
begin
     with cdsCliente do
     begin
          if( StrLleno( FieldByName( 'CL_CODIGO' ).AsString ) ) then
          begin
               if( StrLleno( FieldByName( 'CL_NOM_AU' ).AsString ) ) then
                   cdsEditRequiere.FieldByName( 'RQ_AUTORIZ' ).AsString := FieldByName( 'CL_NOM_AU' ).AsString;

               if( StrLleno( FieldByName( 'CL_PUES_AU' ).AsString ) ) then
                   cdsEditRequiere.FieldByName( 'RQ_PUES_AU' ).AsString := FieldByName( 'CL_PUES_AU' ).AsString;
          end;
     end;
end;

{ ********* cdsReqGastos ********* }

procedure TdmSeleccion.GetLastFolioGasto;
 var sIndex : string;
begin
     with cdsReqGastos do
     begin
          DisableControls;
          sIndex := IndexFieldNames;
          IndexFieldNames := 'RQ_FOLIO;RG_FOLIO';
          First;
          Last;
          FFolioGasto := FieldByName('RG_FOLIO').AsInteger+1;
          IndexFieldNames := sIndex;
          EnableControls;
     end;
end;

procedure TdmSeleccion.cdsReqGastosAlAgregar(Sender: TObject);
begin
     GetLastFolioGasto;
     cdsReqGastos.Append;
     ZBaseEdicion.ShowFormaEdicion( EditGasto, TEditGasto );
end;

procedure TdmSeleccion.cdsReqGastosAlBorrar(Sender: TObject);
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Gasto ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with cdsReqGastos do
             begin
                  Delete;
                  Enviar;
             end;
          finally
             Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmSeleccion.cdsReqGastosAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditGasto, TEditGasto );
end;

procedure TdmSeleccion.cdsReqGastosAlCrearCampos(Sender: TObject);
begin
     with cdsReqGastos do
     begin
          MaskPesos('RG_MONTO');
     end;
end;

procedure TdmSeleccion.cdsReqGastosNewRecord(DataSet: TDataSet);
begin
     with cdsReqGastos do
     begin
          FieldByName('RG_FOLIO').AsInteger := FFolioGasto;
          FieldByName('RQ_FOLIO').AsInteger := cdsEditRequiere.FieldByName( 'RQ_FOLIO' ).AsInteger; //GetFolioRequisicion;
          FieldByName('RG_FECHA').AsDateTime := Date;
     end;
end;

procedure TdmSeleccion.cdsReqGastosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsReqGastos do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( ChangeCount > 0 ) AND
             Reconcile( ServerSeleccion.GrabaGasto( dmCliente.Empresa, Delta,
                                                    cdsEditRequiere.FieldByName('RQ_FOLIO').AsInteger,
                                                    ErrorCount ) ) then
             TressShell.SetDataChange( [ enRequisicion ] );

     end;
end;

function TdmSeleccion.CalculaGastoReal: TPesos;
begin
     Result := 0;
     with cdsReqGastos do
     begin
          if Active then
          begin
               DisableControls;
               First;
               while NOT EOF do
               begin
                    Result := Result + FieldByName('RG_MONTO').AsFloat;
                    Next;
               end;
               EnableControls;
          end;
     end;
end;

{ ********** cdsSolicita *********** }

procedure TdmSeleccion.EdadGetText( Sender: TField; var Text: String; DisplayText: Boolean );
begin
     if DisplayText then
     begin
          if Assigned( Sender ) then
          begin
               if Sender.Dataset.IsEmpty then
                  Text := ''
               else
                   Text := Format( '%d', [ ZetaCommonTools.zYearsBetween( Sender.AsDateTime, Now ) ] );
          end
          else
              Text := '';
     end
     else
          Text := Sender.AsString;
end;

procedure TdmSeleccion.PrettyNameGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if Assigned( Sender ) then
          begin
               if Sender.Dataset.IsEmpty then
                  Text := ''
               else
                   Text := PropperCase( Sender.AsString );
          end
          else
              Text := '';
     end
     else
          Text := Sender.AsString;
end;

procedure TdmSeleccion.RefrescarSolicita(Parametros : TZetaParams; const lBusqueda: Boolean = FALSE );
var
   oCursor : TCursor;
   oClientDataSet : TZetaClientDataSet;
begin
     if lBusqueda then
        oClientDataSet := cdsLookupSolicita
     else
        oClientDataSet := cdsSolicita;
     oCursor := Screen.Cursor;
     try
        Screen.Cursor := crHourGlass;
        oClientDataSet.Data := ServerSeleccion.GetSolicitud( dmCliente.Empresa, Parametros.VarValues );
     finally
            oClientDataSet.ResetDataChange;
            Screen.Cursor := oCursor ;
     end;
end;

procedure TdmSeleccion.cdsSolicitaAlCrearCampos(Sender: TObject);
begin
     with Sender as TZetaClientDataSet do
     begin
          ListaFija('SO_STATUS', lfSolStatus );
          ListaFija( 'SO_ESTUDIO', lfEstudios );
          MaskTasa( 'SO_INGLES' );
          FieldByName( 'SO_FEC_NAC' ).OnGetText := EdadGetText;
          FieldByName( 'PRETTYNAME' ).OnGetText := PrettyNameGetText;
     end;
end;

procedure TdmSeleccion.cdsSolicitaAlAgregar(Sender: TObject);
begin
     FFolioSolicitud := 0;
     with cdsEditSolicita do
     begin
          Refrescar;
          Agregar;
     end;
end;

procedure TdmSeleccion.cdsSolicitaAlModificar(Sender: TObject);
begin
     FFolioSolicitud := -1;
     FRequiereNavegador := TRUE;
     with cdsEditSolicita do
     begin
          Refrescar;
          Modificar;
     end;
end;

procedure TdmSeleccion.cdsSolicitaAfterDelete(DataSet: TDataSet);
var
   ErrorCount : Integer;
   ErrorVar, ErrorData: OleVariant;
begin
     ErrorCount := 0;
     FOperacion := ocReportar;
     with cdsSolicita do
          if ChangeCount > 0 then
          begin
               Repeat
                     ErrorVar := ServerSeleccion.BorraSolicitud( dmCliente.Empresa, Delta, Ord( FOperacion ), ErrorCount, ErrorData );
                     cdsSolCandidatos.Data := ErrorData;     // Se usar� el cds que se usa cuando est� en edici�n de Solicitud
               Until ( Reconciliar( ErrorVar ) );
               TressShell.SetDataChange( [ enRequisicion, enSolicitud ] );
          end;
end;

{$ifdef VER130}
procedure TdmSeleccion.cdsSolicitaReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if ( Pos( 'STATUS_CANDIDATURAS', E.Message ) > 0 ) then     // Es error en Status de Candidaturas
     begin
          with DataSet do
               Opera := ShowDlgStatusCandidaturas( IntToStr( ValorCampo( FieldByName( 'SO_FOLIO' ) ) ),
                                                   ValorCampo( FieldByName( 'PRETTYNAME' ) ),
                                                   ZetaCommonLists.ObtieneElemento( lfSolStatus,
                                                   ValorCampo( FieldByName( 'SO_STATUS' ) ) ),
                                                   cdsSolCandidatos );
          if ( Opera = ocIgnorar ) then
          begin
               FOperacion:= Opera;
               Action := raAbort;
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$else}
procedure TdmSeleccion.cdsSolicitaReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   Opera : eOperacionConflicto;
begin
     Action := raCancel;
     if ( Pos( 'STATUS_CANDIDATURAS', E.Message ) > 0 ) then     // Es error en Status de Candidaturas
     begin
          with DataSet do
               Opera := ShowDlgStatusCandidaturas( IntToStr( ValorCampo( FieldByName( 'SO_FOLIO' ) ) ),
                                                   ValorCampo( FieldByName( 'PRETTYNAME' ) ),
                                                   ZetaCommonLists.ObtieneElemento( lfSolStatus,
                                                   ValorCampo( FieldByName( 'SO_STATUS' ) ) ),
                                                   cdsSolCandidatos );
          if ( Opera = ocIgnorar ) then
          begin
               FOperacion:= Opera;
               Action := raAbort;
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$endif}

{ ********  cdsEditSolicita ********** }

function TdmSeleccion.GetFolioSolicitud: Integer;
begin
     Result := 0;
     with GetcdsSolicitudes do
     begin
          if FFolioSolicitud < 0 then
          begin
               if Active and not IsEmpty then
                  Result := FieldByName( 'SO_FOLIO' ).AsInteger
          end
          else Result := FFolioSolicitud;
     end;
     FFolioSolicitud := -1 ;
end;

procedure TdmSeleccion.cdsEditSolicitaAlAdquirirDatos(Sender: TObject);
var
   Candidatos, Examenes, RefLaboral, Documentos: OleVariant;
   oCursor : TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourGlass;
     try
        cdsPuestos.Conectar;
        cdsEditSolicita.Data := ServerSeleccion.GetEditSolicitud( dmCliente.Empresa,
                                                                  FolioSolicitud,
                                                                  Candidatos,
                                                                  Examenes,
                                                                  RefLaboral,
                                                                  Documentos );
        cdsSolCandidatos.Data := Candidatos;
        cdsExamenes.Data := Examenes;
        cdsRefLaboral.Data := RefLaboral;
        cdsDocumentos.Data := Documentos;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmSeleccion.cdsEditSolicitaAlAgregar(Sender: TObject);
var lNoMostrar, lCiclo : Boolean;
    iSolicitud : integer;
begin
     lNoMostrar := FALSE;
     iSolicitud := 0;
     with cdsEditSolicita do
     begin
          repeat
                if NOT lNoMostrar then
                begin
                     if (RecordCount > 0) then //Esto es para que se limpien todos los datasets
                        Refrescar;             //a partir de la segunda vuelta
                     Append;
                     AgregaRefLaborales;
                     cdsRefLaboral.First;
                     with cdsEditDocumentos do
                     begin
                          Refrescar;
                          Append;
                     end;
                end;

                if lNoMostrar OR ZWizardBasico.ShowWizard( TAltaSolicitud ) then
                begin
                     iSolicitud := iMax(iSolicitud,FolioSolicitud);
                     case ShowDlgAltaWizard( iSolicitud, TRUE ) of
                          opOtraAlta:
                          begin
                               lCiclo := TRUE;
                               lNoMostrar := FALSE;
                          end;
                          opEditar:
                          begin
                               lCiclo := TRUE;
                               lNoMostrar := TRUE;
                               FRequiereNavegador := FALSE;
                               Modificar;
                          end
                          else
                          begin
                               lCiclo := FALSE;
                               lNoMostrar := FALSE;
                          end;
                     end;
                end
                else lCiclo := FALSE;
          until NOT lCiclo;
     end;
     TressShell.ReconectaFormaActiva;
end;

procedure TdmSeleccion.cdsEditSolicitaAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditSolicitud, TEditSolicitud );
     if not lEditandoReq then
        TressShell.ReconectaFormaActiva;
end;

procedure TdmSeleccion.cdsEditSolicitaAfterCancel(DataSet: TDataSet);
begin
     with cdsExamenes do
          if ( ChangeCount > 0 ) then
             CancelUpdates;
end;

procedure TdmSeleccion.cdsEditSolicitaBeforePost(DataSet: TDataSet);
begin
     with cdsEditSolicita do
     begin
          if StrVacio(FieldByName('SO_APE_PAT').AsString) then
          begin
               FieldByName('SO_APE_PAT').FocusControl;
               DataBaseError('El Apellido Paterno no Puede Quedar Vac�o');
          end;
          if StrVacio(FieldByName('SO_NOMBRES').AsString) then
          begin
               FieldByName('SO_NOMBRES').FocusControl;
               DataBaseError('El Nombre no Puede Quedar Vac�o');
          end;
          if FieldByName('SO_FEC_NAC').AsDateTime = NullDateTime then
          begin
               FieldByName('SO_FEC_NAC').FocusControl;
               DataBaseError('La Fecha de Nacimiento no Puede Quedar Vac�a');
          end;
     end;
     ValidaMemoField(cdsEditSolicita,'SO_OBSERVA');
end;

procedure TdmSeleccion.cdsEditSolicitaNewRecord(DataSet: TDataSet);
var
   i: Integer;
   dFechaInicial: TDate;
begin
     DFechaInicial := Trunc( Date );
     with cdsEditSolicita do
     begin
          FieldByName( 'SO_FEC_INI' ).AsDateTime := DFechaInicial;
          FieldByName( 'SO_FEC_NAC' ).AsDateTime := DFechaInicial;
          FieldByName( 'SO_SEXO' ).AsString := ZetaCommonLists.ObtieneElemento( lfSexo, Ord( esMasculino ) );
          FieldByName( 'SO_EDO_CIV' ).AsString := ZetaCommonLists.ObtieneElemento( lfEdoCivil, Ord( ecSoltero ) );
          FieldByName( 'SO_REQUI_1' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_2' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_3' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_4' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_5' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_6' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_7' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_8' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_REQUI_9' ).AsString := K_GLOBAL_NO;
          FieldByName( 'SO_FEC_RES' ).AsDateTime := DFechaInicial;
          for i := 1 to K_GLOBAL_LOG_MAX do
          begin
               FieldByName( Format( 'SO_G_LOG_%d', [ i ] ) ).AsString := K_GLOBAL_NO;
          end;
     end;
end;


procedure TdmSeleccion.cdsEditSolicitaAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iFolio: Integer;
begin
     ErrorCount := 0;
     with cdsEditSolicita do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( ChangeCount > 0 ) and Reconcile( ServerSeleccion.GrabaSolicitud( dmCliente.Empresa,
                                                Delta, ErrorCount, iFolio ) ) then
          begin
               TressShell.SetDataChange( [ enSolicitud ] );
               if ( iFolio > 0 ) then                     // Registro de Solicitud
               begin
                    GrabaReferencias( iFolio );
                    FFolioSolicitud := iFolio;              // Al refrescar cdsSolicita intenta posicionar este Folio
                    Edit;
                    FieldByName('SO_FOLIO').AsInteger := FFolioSolicitud;
                    FieldByName('PRETTYNAME').AsString := FieldByName('SO_APE_PAT').AsString + ' ' +
                                                          FieldByName('SO_APE_MAT').AsString + ', ' +
                                                          FieldByName('SO_NOMBRES').AsString;
                    Post;
                    MergeChangeLog;
               end;
          end;
     end;
end;

procedure TdmSeleccion.ModificaSolicitud( const iFolio: Integer; const lNavegar : Boolean);
var
   oCursor: TCursor;
begin
     FolioSolicitud := iFolio;
     with cdsEditSolicita do
     begin
          FRequiereNavegador := lNavegar;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             Refrescar;
          finally
             Screen.Cursor := oCursor;
          end;
          Modificar;
     end;
end;

{ ********** cdsExamenes ********** }

procedure TdmSeleccion.cdsExamenesAlCrearCampos(Sender: TObject);
begin
     cdsExamenes.ListaFija( 'EX_TIPO', lfTipoExamen );
end;

procedure TdmSeleccion.cdsExamenesNewRecord(DataSet: TDataSet);
var
   iFolio: Integer;
begin
     iFolio := 1;
     with cdsExamenes do
     begin
          with Aggregates.Items[0] do
               if ( not VarIsNull( Value ) ) then
                  iFolio := iFolio + Value;
          FieldByName( 'SO_FOLIO' ).AsInteger := cdsEditSolicita.FieldByName( 'SO_FOLIO' ).AsInteger;
          FieldByName( 'EX_FOLIO' ).AsInteger := iFolio;
          FieldByName( 'EX_FECHA' ).AsDateTime := Trunc( Date );
          FieldByName( 'EX_APROBO' ).AsString := K_GLOBAL_NO;
     end;
end;

procedure TdmSeleccion.cdsExamenesAlBorrar(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Examen ?', 0, mbNo ) then
        with cdsExamenes do
        begin
             Delete;
             Enviar;
        end;
end;

procedure TdmSeleccion.cdsExamenesAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditExamen, TEditExamen );
end;

procedure TdmSeleccion.cdsExamenesBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField(cdsExamenes,'EX_OBSERVA');
end;

{ ******** cdsRefLaboral ******** }

procedure TdmSeleccion.AgregaRefLaborales;
var
   i, iCuantos : Integer;
begin
     cdsRefLaboral.EmptyDataSet;
     iCuantos := Global.GetGlobalInteger( K_GLOBAL_REF_LABORAL_QTY );
     if ( iCuantos > 0 ) then
        for i := 1 to iCuantos do
            with cdsRefLaboral do
            begin
                 Append;
                 FieldByName( 'RL_FOLIO' ).AsInteger := i;
                 Post;
            end;
end;

procedure TdmSeleccion.cdsRefLaboralAlModificar(Sender: TObject);
begin
     try
        ZBaseEdicion.ShowFormaEdicion( EditRefLaboral, TEditRefLaboral );
     finally
            FreeAndNil( EditRefLaboral )
     end;
end;

{
procedure TdmSeleccion.cdsRefLaboralAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsRefLaboral do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( not ( cdsEditSolicita.State = dsInsert ) ) and ( ChangeCount > 0 ) then    // No se enviara inmediatamente en el Registro de Solicitudes
             Reconcile( ServerSeleccion.GrabaTabla( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
     end;
end;
}

procedure TdmSeleccion.GrabaReferencias( const iFolio: Integer );
begin
     with cdsRefLaboral do
     begin
          First;
          while not EOF do
          begin
               if ( not ( State in [ dsEdit, dsInsert ] ) ) then
                  Edit;
               FieldByName( 'SO_FOLIO' ).AsInteger := iFolio;
               Next;   // Se hace un Post Automatico
          end;
          Enviar;      // Despues de Asignar Folios envia las referencias
     end;
end;

{ ******** cdsSolCandidatos ******** }

procedure TdmSeleccion.cdsSolCandidatosAlCrearCampos(Sender: TObject);
begin
     cdsPuestos.Conectar;
     with cdsSolCandidatos do
     begin
          ListaFija('CD_STATUS', lfCanStatus );
          CreateSimpleLookup( cdsPuestos, 'PU_DESCRIP', 'RQ_PUESTO' );
     end;
end;

procedure TdmSeleccion.cdsReqCandidatosAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditCandidato, TEditCandidato );
end;

procedure TdmSeleccion.cdsReqCandidatosAlAgregar(Sender: TObject);
begin
     FBuscaSolicitud.AgregaCandidatosDialogo;
end;

procedure TdmSeleccion.cdsReqCandidatosAlBorrar(Sender: TObject);
begin
     cdsReqCandidatos.Delete;
end;

{ ******** cdsEntrevista ******** }

procedure TdmSeleccion.cdsEntrevistaAlAdquirirDatos(Sender: TObject);
begin
     cdsEntrevista.Data := ServerSeleccion.GetEntrevista( dmCliente.Empresa,
                                                          cdsReqCandidatos.FieldByName('SO_FOLIO').AsInteger,
                                                          cdsEditRequiere.FieldByName( 'RQ_FOLIO' ).AsInteger );
//                                                          GetFolioRequisicion );
end;

procedure TdmSeleccion.cdsEntrevistaAlCrearCampos(Sender: TObject);
begin
     with cdsEntrevista do
     begin
          ListaFija( 'ER_RESULT', lfEntStatus );
          MaskFecha( 'ER_FECHA' );
     end;
end;

procedure TdmSeleccion.cdsEntrevistaAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditEntrevista, TEditEntrevista );
end;

procedure TdmSeleccion.cdsEntrevistaAlBorrar(Sender: TObject);
var
   oCursor: TCursor;
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Esta Entrevista ?', 0, mbNo ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             with cdsEntrevista do
             begin
                  Delete;
                  Enviar;
             end;
          finally
             Screen.Cursor := oCursor;
          end;
     end;
end;

procedure TdmSeleccion.GetLastFolioEntrevista;
var
   sIndex : string;
begin
     with cdsEntrevista do
     begin
          sIndex := IndexFieldNames;
          //CV: Se le quito el campo RQ_FOLIO, porque ya no va a ser parte de la llave primaria.
          IndexFieldNames := 'SO_FOLIO;ER_FOLIO';       
          Last;
          FFolioEntrevista := FieldByName('ER_FOLIO').AsInteger+1;
          IndexFieldNames := sIndex;
     end;
end;

procedure TdmSeleccion.cdsEntrevistaAlAgregar(Sender: TObject);
begin
     GetLastFolioEntrevista;
     cdsEntrevista.Append;
     ZBaseEdicion.ShowFormaEdicion( EditEntrevista, TEditEntrevista );
end;

procedure TdmSeleccion.cdsEntrevistaNewRecord(DataSet: TDataSet);
begin
     with cdsEntrevista do
     begin
          FieldByName('RQ_FOLIO').AsInteger := cdsEditRequiere.FieldByName( 'RQ_FOLIO' ).AsInteger;  //GetFolioRequisicion;
          FieldByName('SO_FOLIO').AsInteger := cdsReqCandidatos.FieldByName('SO_FOLIO').AsInteger;
          FieldByName('ER_FOLIO' ).AsInteger := FFolioEntrevista;
          FieldByName('ER_FECHA').AsDateTime := Date;
     end;
end;

procedure TdmSeleccion.cdsEntrevistaBeforePost(DataSet: TDataSet);
begin
     with cdsEntrevista do
     begin
          if StrVacio(FieldByName('ER_NOMBRE').AsString) then
          begin
               FieldByName('ER_NOMBRE').FocusControl;
               DataBaseError('El Nombre del Entrevistador no Puede Quedar Vac�o');
          end;
   end;
   ValidaMemoField(cdsEntrevista,'ER_DETALLE');
end;

procedure TdmSeleccion.cdsEntrevistaAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with cdsEntrevista do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               Reconcile( ServerSeleccion.GrabaTabla( dmCliente.Empresa, Tag, Delta, ErrorCount ) );
               {with cdsReqCandidatos do
                    if eCanStatus(FieldByName('CD_STATUS').AsInteger) = csPorRevisar then
                    begin
                         lEnviar := State = dsBrowse;
                         if lEnviar then Edit;
                         FieldByName('CD_STATUS').AsInteger := Ord(csSeleccionado);
                         if lEnviar then Enviar;
                    end;
               }
          end;
     end;
end;

{ ******** cdsReqCandidatos ******** }

procedure TdmSeleccion.cdsReqCandidatosAlCrearCampos(Sender: TObject);
begin
     with cdsReqCandidatos do
     begin
          ListaFija('SO_ESTUDIO', lfEstudios );
          ListaFija('SO_STATUS', lfSolStatus );
          ListaFija('CD_STATUS', lfCanStatus );
          MaskFecha('SO_FEC_NAC');
          FieldByName( 'SO_FEC_NAC' ).OnGetText := EdadGetText;
          FieldByName( 'PRETTYNAME' ).OnGetText := PrettyNameGetText;
     end;
end;

procedure TdmSeleccion.SetTotalesRequisicion( var iCandidatos, iContratados, iSeleccionados : integer);
 var
    iSolicitud : integer;
begin
     with cdsReqCandidatos do
     begin
          DisableControls;
          iCandidatos := RecordCount;
          iSeleccionados := 0;
          iContratados := 0;

          iSolicitud := FieldByName('SO_FOLIO').AsInteger;

          First;

          while NOT EOF do
          begin
               case eCanStatus( FieldByName('CD_STATUS').AsInteger ) of
                    csSeleccionado,csPorContratar: Inc(iSeleccionados);
                    csContratado:
                    begin
                         Inc(iContratados);
                         Inc(iSeleccionados);
                    end;
               end;
               Next;
          end;
          Locate('SO_FOLIO',iSolicitud,[]);
          EnableControls;
     end;
end;

procedure TdmSeleccion.cdsReqCandidatosAlEnviarDatos(Sender: TObject);
var
   ErrorCount, iSolStatus: Integer;
   StatusSolicitud: eSolStatus;
begin
     with cdsReqCandidatos do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          StatusSolicitud := eSolStatus( FieldByName( 'SO_STATUS' ).AsInteger );
          if ( ChangeCount > 0 ) AND
             ( Reconcile( ServerSeleccion.GrabaCandidato( dmCliente.Empresa, Delta,
                          cdsEditRequiere.FieldByName('RQ_FOLIO').AsInteger, iSolStatus, ErrorCount ) ) ) then{OP:18.Abr.08}
          begin
             TressShell.SetDataChange( [ enRequisicion ] );
             if ( StatusSolicitud <> eSolStatus( iSolStatus ) ) then{OP:18.Abr.08}
             begin
                  Edit;
                  FieldByName('SO_STATUS').AsInteger := iSolStatus;
                  Post;
                  MergeChangeLog;
             end;
          end;
     end;
end;

function TdmSeleccion.AgregaUnCandidato: Boolean;
begin
     with cdsLookupSolicita do
     begin
          Result := not cdsReqCandidatos.Locate( 'SO_FOLIO', FieldByName( 'SO_FOLIO' ).AsString, [] );
          if Result then
          begin
               cdsReqCandidatos.Append;
               cdsReqCandidatos.FieldByName( 'RQ_FOLIO' ).AsInteger := cdsEditRequiere.FieldByName( 'RQ_FOLIO' ).AsInteger;
               cdsReqCandidatos.FieldByName( 'SO_FOLIO' ).AsInteger := FieldByName( 'SO_FOLIO' ).AsInteger;
               cdsReqCandidatos.FieldByName( 'PRETTYNAME' ).AsString := FieldByName( 'PRETTYNAME' ).AsString;
               cdsReqCandidatos.FieldByName( 'SO_SEXO' ).AsString := FieldByName( 'SO_SEXO' ).AsString;
               cdsReqCandidatos.FieldByName( 'SO_ESTUDIO' ).AsInteger := FieldByName( 'SO_ESTUDIO' ).AsInteger;
               cdsReqCandidatos.FieldByName( 'SO_FEC_NAC' ).AsDateTime := FieldByName( 'SO_FEC_NAC' ).AsDateTime;
               cdsReqCandidatos.FieldByName( 'SO_STATUS' ).AsInteger := FieldByName( 'SO_STATUS' ).AsInteger;
               cdsReqCandidatos.FieldByName( 'SO_INGLES' ).AsInteger := FieldByName( 'SO_INGLES' ).AsInteger;
               cdsReqCandidatos.Post;
          end;
     end;
end;

{function TdmSeleccion.AgregaCandidatos(Parametros: TZetaParams; var sMensaje: String): Boolean;
var
   iExistentes, iAgregados : Integer;
begin
     with cdsReqCandidatos do
     begin
          DisableControls;
          Filter := 'CD_STATUS <> 0';
          Filtered := TRUE;
          iExistentes := RecordCount;
          Filtered := FALSE;
          Data := ServerSeleccion.ObtenerCandidatos(dmCliente.Empresa, Parametros.VarValues);

          iAgregados := RecordCount - iExistentes;
          Result := iAgregados > 0;
          if Result then
          begin
               sMensaje := Format( 'Se Agregaron [%d] Candidatos', [ iAgregados ] );
               if ( iExistentes > 0 ) then
                  sMensaje := sMensaje + CR_LF + Format( 'Ya exist�an [%d] Candidatos', [ iExistentes ] );
          end
          else
              sMensaje := 'NO se tienen Solicitantes que cumplan con ' + CR_LF + 'el Perfil de la Requisici�n';
          EnableControls;
     end;
end;}

{ ******** EMail ******** }

procedure TdmSeleccion.EnviarAnuncio( const sAnuncio, eMailAnuncio: string);
var
   Puerto:Integer;
begin
     with Email do
     begin
          try
             if Not Connected then
             begin
                  with Global do
                  begin
                       Host := GetGlobalString(K_GLOBAL_EMAIL_HOST);
                       Puerto := GetGlobalInteger(K_GLOBAL_EMAIL_PORT);
                       if Puerto > 0 then
                          Port := Puerto;
                       if StrVacio(Host) then
                          ZetaDialogo.ZError('Servidor de Correos', 'El Servidor de Correos no Ha Sido Especificado',0);
                       UserID := StrDef(GetGlobalString(K_GLOBAL_EMAIL_USERID),'SelectoraDePersonal');
                       Password := GetGlobalString( K_GLOBAL_EMAIL_PSWD );
                       PostMessage.FromAddress := StrDef(GetGlobalString(K_GLOBAL_EMAIL_FROMADRESS),'SelectoraDePersonal@dominio.com');
                       PostMessage.FromName := StrDef(GetGlobalString(K_GLOBAL_EMAIL_FROMNAME),'Selecci�n y Reclutamiento de Personal' );
                  end;
                  Connect;
             end;
             if Connected then
             begin
                  SubType := mtPlain;
                  with PostMessage do
                  begin
                       ToAddress.Clear;
                       Body.Clear;
                       Attachments.Clear;
                       Subject := 'Anuncio';

                       ToAddress.CommaText := StrTransAll(eMailAnuncio, ';', ',' );
                       Body.Text :=  sAnuncio ;
                  end;
                  //PostMessage.Date := DateToStr(Date);
                  PostMessage.Date := VACIO;
                  SendMail;
             end
             else
                 ZetaDialogo.ZError('Servidor de Correos','El Servidor %s de Email no se Pudo Conectar'+Host, 0);
          finally
                 Disconnect;
          end;
     end;
end;

procedure TdmSeleccion.EmailFailure(Sender: TObject);
begin
     ZetaDialogo.ZError('Servidor de Correos','El Servidor %s de Email no se Pudo Conectar'+Global.GetGlobalString(K_GLOBAL_EMAIL_HOST), 0);
end;

{ cdsDocumentos }

procedure TdmSeleccion.cdsDocumentosAlBorrar(Sender: TObject);
begin
     if ZetaDialogo.zConfirm( '� Atenci�n !', '� Desea Borrar Este Documento ?', 0, mbNo ) then
        with cdsDocumentos do
        begin
             Delete;
             Enviar;
        end;
end;

procedure TdmSeleccion.cdsDocumentosAlCrearCampos(Sender: TObject);
begin
     with cdsDocumentos do
     begin
          CreateCalculated( 'DO_TIPO_DOC', ftString, 40 );
     end;
end;

procedure TdmSeleccion.AbreDocumento;
var
   sArchivo : string;
begin
     with cdsEditDocumentos do
     begin
          Refrescar;
          with TBlobField( FieldByName( 'DO_BLOB' ) ) do
          begin
               if NOT IsNull then
               begin
                    sArchivo := GetTempFile( K_PREFIJO_TEMP, FieldByName( 'DO_EXT' ).AsString );
                    if StrLleno( sArchivo ) then
                    begin
                         SaveTofile( sArchivo );
                         ExecuteFile( sArchivo, '', '', SW_SHOWDEFAULT );
                    end;
               end
               else
                  ZetaDialogo.ZInformation( 'Solicitud de Empleo', 'El Documento no Contiene Informaci�n', 0 );
          end;
     end;
end;

function TdmSeleccion.GetTipoDocumento: string;
 var
    aInfo: TSHFileInfo;
    oFile : TFileStream;
    sArchivo : PChar;
begin
     Result := 'Documento Vac�o';
     with dmSeleccion.cdsDocumentos do
     begin
{          with TBlobField(FieldByName('DO_BLOB')) do
          begin
               if NOT IsNull then
               begin }
                    sArchivo := PChar(GetTempFile( K_PREFIJO_TEMP, FieldByName('DO_EXT').AsString ) );
                    oFile := TFileStream.Create( sArchivo,fmCreate);
                    try
                       if SHGetFileInfo(sArchivo,0,aInfo,Sizeof(aInfo),SHGFI_TYPENAME)<>0 then
                       begin
                            Result := StrPas(aInfo.szTypeName);
                            if StrVacio(Result) then
                               Result := 'Archivo de ' + FieldByName('DO_EXT').AsString;
                       end
                       else
                           Result := 'Archivo Indefinido'
                    finally
                           oFile.Free;
                           DeleteFile(sArchivo);
                    end;
{               end;
          end; }
     end;
end;

procedure TdmSeleccion.cdsDocumentosCalcFields(DataSet: TDataSet);
begin
     with cdsDocumentos do
     begin
          FieldByName('DO_TIPO_DOC').AsString := GetTipoDocumento;
     end;
end;

procedure TdmSeleccion.cdsDocumentosAlAgregar(Sender: TObject);
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        cdsDocumentos.Append;
        try
           with cdsEditDocumentos do
           begin
                if ( not Active ) then        // Si no ha sido usado
                   Refrescar;
                EmptyDataSet;
                Agregar;
           end;
        finally
           cdsDocumentos.MergeChangeLog;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TdmSeleccion.cdsDocumentosAlModificar(Sender: TObject);
begin
     cdsDocumentos.Edit;
     try
        with cdsEditDocumentos do
        begin
             Refrescar;
             Modificar;
        end;
     finally
        cdsDocumentos.MergeChangeLog;
     end;
end;

procedure TdmSeleccion.SetCambiosDocumentos;
begin
     with cdsDocumentos do
     begin
          if ( State in [dsEdit, dsInsert] ) then      // Si no se invoc� Append o Edit antes no hace nada, para evitar dar informaci�n falsa
          begin
               FieldByName( 'SO_FOLIO' ).AsInteger := cdsEditDocumentos.FieldByName( 'SO_FOLIO' ).AsInteger;
               FieldByName( 'DO_TIPO' ).AsString := cdsEditDocumentos.FieldByName( 'DO_TIPO' ).AsString;
               FieldByName( 'DO_EXT' ).AsString := cdsEditDocumentos.FieldByName( 'DO_EXT' ).AsString;
               FieldByName( 'DO_OBSERVA' ).AsString := cdsEditDocumentos.FieldByName( 'DO_OBSERVA' ).AsString;
               Post;
          end;
     end;
end;

{ cdsEditDocumentos }

procedure TdmSeleccion.cdsEditDocumentosAlAdquirirDatos(Sender: TObject);
begin
     with cdsDocumentos do
          if Active then
             cdsEditDocumentos.Data := ServerSeleccion.GetDocumento( dmCliente.Empresa,
                                                                     FieldByName('SO_FOLIO').AsInteger,
                                                                     FieldByName('DO_TIPO').AsString )
          else
             cdsEditDocumentos.Data := ServerSeleccion.GetDocumento( dmCliente.Empresa, 0, '' );
end;

procedure TdmSeleccion.cdsEditDocumentosAlModificar(Sender: TObject);
begin
     ZBaseEdicion.ShowFormaEdicion( EditDocumento, TEditDocumento );
end;

procedure TdmSeleccion.cdsEditDocumentosBeforePost(DataSet: TDataSet);
begin
     with cdsEditDocumentos do
     begin
          if StrVacio(FieldByName('DO_TIPO').AsString) then
          begin
               FieldByName('DO_TIPO').FocusControl;
               raise EDataBaseError.Create('El Tipo de Documento no Puede Quedar Vac�o');
          end;
     end;
end;

procedure TdmSeleccion.cdsEditDocumentosNewRecord(DataSet: TDataSet);
begin
     with cdsEditDocumentos do
     begin
          FieldByName('SO_FOLIO').AsInteger := cdsEditSolicita.FieldByName('SO_FOLIO').AsInteger;
          FieldByName('DO_TIPO').AsString := 'CURRIC';
     end;
end;

procedure TdmSeleccion.cdsEditDocumentosAlEnviarDatos(Sender: TObject);
var
   ErrorCount: Integer;
begin
     with TZetaClientDataSet( Sender ) do
     begin
          if ( State in [ dsEdit, dsInsert ] ) then
             Post;
          if ( ChangeCount > 0 ) then
          begin
               FReemplazaDoc := FALSE;
               if ( not Reconciliar( ServerSeleccion.GrabaDocumento( dmCliente.Empresa, FReemplazaDoc, Delta, ErrorCount ) ) ) and
                  FReemplazaDoc then
                  Reconciliar( ServerSeleccion.GrabaDocumento( dmCliente.Empresa, FReemplazaDoc, Delta, ErrorCount ) );
          end;
     end;
end;

{$ifdef VER130}
procedure TdmSeleccion.cdsEditDocumentosReconcileError(DataSet: TClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sTipo : string;
begin
     if PK_Violation( E ) then
     begin
          if ZetaDialogo.ZErrorConfirm( 'Error al Agregar Documento',
                                        Format( 'Ya existe un Documento Tipo %s.' + CR_LF +
                                                '� Desea Sustituirlo ?', [cdsEditDocumentos.FieldByName('DO_TIPO').AsString]), 0, mbNo ) then
          begin
               sTipo := ValorCampo( DataSet.FieldByName( 'DO_TIPO' ) );
               with cdsDocumentos do
               begin
                    Delete;                               // Se borra el Actual
                    if Locate( 'DO_TIPO' , sTipo, [] ) then
                       Edit;                              // Se va a Reemplazar
               end;
               FReemplazaDoc := TRUE;
               Action := raCorrect;
{               with cdsEditDocumentos do
               begin
                    FieldBlob := TBlobField(FieldByName('DO_BLOB')).Value;
                    sTipo := FieldByName('DO_TIPO').AsString;
                    sExt := FieldByName('DO_EXT').AsString;
                    //Delete;
                    if cdsDocumentos.Locate('DO_TIPO',sTipo,[]) then
                    begin
                         Refrescar;     // Refresca cdsEditDocumentos
                         Edit;
                         TBlobField(FieldByName('DO_BLOB')).AsString := FieldBlob;
                         FieldByName('DO_EXT').AsString := sExt;
                         Post;
                         Action := raCorrect;
                         //Enviar;
                    end;
               end; }
          end
          else
          begin
               //cdsDocumentos.Cancel;      // Cancela el Append o Edit
               Action := raAbort;
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$else}
procedure TdmSeleccion.cdsEditDocumentosReconcileError(DataSet: TCustomClientDataSet; E: EReconcileError; UpdateKind: TUpdateKind; var Action: TReconcileAction);
var
   sTipo : string;
begin
     if PK_Violation( E ) then
     begin
          if ZetaDialogo.ZErrorConfirm( 'Error al Agregar Documento',
                                        Format( 'Ya existe un Documento Tipo %s.' + CR_LF +
                                                '� Desea Sustituirlo ?', [cdsEditDocumentos.FieldByName('DO_TIPO').AsString]), 0, mbNo ) then
          begin
               sTipo := ValorCampo( DataSet.FieldByName( 'DO_TIPO' ) );
               with cdsDocumentos do
               begin
                    Delete;                               // Se borra el Actual
                    if Locate( 'DO_TIPO' , sTipo, [] ) then
                       Edit;                              // Se va a Reemplazar
               end;
               FReemplazaDoc := TRUE;
               Action := raCorrect;
{               with cdsEditDocumentos do
               begin
                    FieldBlob := TBlobField(FieldByName('DO_BLOB')).Value;
                    sTipo := FieldByName('DO_TIPO').AsString;
                    sExt := FieldByName('DO_EXT').AsString;
                    //Delete;
                    if cdsDocumentos.Locate('DO_TIPO',sTipo,[]) then
                    begin
                         Refrescar;     // Refresca cdsEditDocumentos
                         Edit;
                         TBlobField(FieldByName('DO_BLOB')).AsString := FieldBlob;
                         FieldByName('DO_EXT').AsString := sExt;
                         Post;
                         Action := raCorrect;
                         //Enviar;
                    end;
               end; }
          end
          else
          begin
               //cdsDocumentos.Cancel;      // Cancela el Append o Edit
               Action := raAbort;
          end;
     end
     else
         MessageDlg( GetErrorDescription( E ), mtError, [ mbOK ], 0 );
end;
{$endif}

procedure TdmSeleccion.ValidaMemoField( DataSet : TDataSet; const sField : string );
begin
     with DataSet.FieldByName(sField) do
          if StrVacio(AsString) then
             AsString := ' ';
end;

procedure TdmSeleccion.cdsClienteBeforePost(DataSet: TDataSet);
begin
     ValidaMemoField(DataSet,'CL_OBSERVA');
     with DataSet do
     begin
          if StrVacio( FieldByName( 'CL_CODIGO' ).AsString ) then
             DatabaseError( 'El C�digo Del Cliente No Puede Quedar Vac�o' );
          if StrVacio( FieldByName( 'CL_NOMBRE' ).AsString ) then
             DatabaseError( 'El Nombre Del Cliente No Puede Quedar Vac�o' );
     end;
end;

function TdmSeleccion.CargaDocumento(const sArchivo : string): Boolean;
var
   FieldBlob : TBlobField;
   Archivo : TMemoryStream;
begin
     Result := FALSE;
     with cdsEditDocumentos do
     begin
          if StrLleno(sArchivo) AND
             FieldByName('DO_BLOB').IsBlob then
          begin
               if FileExists(sArchivo) then
               begin
                    if State = dsBrowse then Edit;
                    FieldByName('DO_EXT').AsString := UpperCase(Copy(ExtractFileExt(sArchivo),2,255));
                    FieldBlob := TBlobField(FieldByName('DO_BLOB'));

                    Archivo := TMemoryStream.Create;
                    Archivo.LoadFromFile(sArchivo);

                    FieldBlob.LoadFromStream(Archivo) ;
                    Post;
                    Result := TRUE;
               end
               else
                   ZetaDialogo.ZError('Agregando Documento', Format('El Archivo %s no Existe ', [sArchivo]), 0 );
          end;
     end;
end;

procedure TdmSeleccion.CargaListaInteres(oLista, oListaCodigos: TStringList);
var
   sOldIndex: String;
begin
     with cdsAreaInteres do
     begin
          Conectar;
          sOldIndex := IndexFieldNames;
          try
             IndexFieldNames := 'TB_ELEMENT';
             First;
             oListaCodigos.Clear;
             with oLista do
             begin
                  BeginUpdate;
                  Clear;
                  while not EOF do
                  begin
                       Add( FieldByName( 'TB_ELEMENT' ).AsString );
                       oListaCodigos.Add( FieldByName( 'TB_CODIGO' ).AsString );
                       Next;
                  end;
                  EndUpdate;
             end;
          finally
             IndexFieldNames := sOldIndex;
          end;
     end;
end;

procedure TdmSeleccion.LimpiaArchivosTemporales;
var
   aDir : Array[0..255] of char;
   FFile: TSearchRec;
begin
     //Se borran los archivos temporales creados por la Vista de Documentos de Solicitudes
     GetTempPath(255, adir);
     if ( FindFirst( aDir + K_PREFIJO_TEMP + '*.*', faArchive, FFile ) = 0 ) then
     begin
          DeleteFile( aDir + Ffile.Name );
          while ( FindNext( FFile ) = 0 ) do
                DeleteFile( aDir + Ffile.Name );
     end;
     FindClose( FFile );
end;

procedure TdmSeleccion.cdsEditSolicitaAlCrearCampos(Sender: TObject);
begin
     with cdsEditSolicita do
     begin
          FieldByName( 'SO_RFC' ).EditMask:= 'LLLL-999999-aaa;0';
          FieldByName( 'SO_CURP' ).EditMask:= 'LLLL-999999-L-LL-LLL-A-9;0';
          FieldByName( 'SO_APE_PAT' ).onChange:= Nombre_NacChange;
          FieldByName( 'SO_APE_MAT' ).onChange:= Nombre_NacChange;
          FieldByName( 'SO_NOMBRES' ).onChange:= Nombre_NacChange;
          FieldByName( 'SO_FEC_NAC' ).onChange:= Nombre_NacChange;
     end;
end;

procedure TdmSeleccion.Nombre_NacChange(Sender: TField);
begin
     with cdsEditSolicita do
     begin
          FieldByName( 'SO_RFC' ).AsString := ZetaCommonTools.CalcRFC( FieldByName( 'SO_APE_PAT' ).AsString,
                                                                       FieldByName( 'SO_APE_MAT' ).AsString,
                                                                       FieldByName( 'SO_NOMBRES' ).AsString,
                                                                       FieldByName( 'SO_FEC_NAC' ).AsDateTime );
     end;
end;

procedure TdmSeleccion.cdsEstadosAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerSeleccion.GetTabla( dmCliente.EmpresaTress, Tag );
     end;
end;

procedure TdmSeleccion.cdsContratosAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerSeleccion.GetTabla( dmCliente.EmpresaTress, Tag );
     end;
end;

function TdmSeleccion.GetCampoAdicional( const iPosicion: integer ): string;
begin
     Result := ZetaCommonTools.PadLCar( IntToStr( iPosicion ), 2, '_' ); //StrRight( Format( '_%d', [ iPosicion ] ), 2 );
end;

procedure TdmSeleccion.LlenaAdicionales( FCampos: TListaCampos );
var
   i, iGlobal, iEquiv, iValor: integer;
   eTipo: eCampoTipo;
begin
     Global.InitListaUsados;                  // Inicia Lista de Campos Usados
     with FCampos do
     begin
          with Global do
          begin
               for i := 0 to ( Global.ListaUsados.Count - 1 ) do
               begin
                    iGlobal := StrToIntDef( Global.ListaUsados.Strings[ i ], 0 );
                    eTipo := ZGlobalTress.GetTipoGlobalAdicional( iGlobal, iEquiv, iValor );
                    case eTipo of
                         cdtTexto:    Add( eTipo, GetGlobalString( iGlobal ), '', Format( 'SO_G_TEX%s', [ GetCampoAdicional( iValor ) ] ), '', 0, iGlobal, 0, True );
                         cdtNumero:   Add( eTipo, GetGlobalString( iGlobal ), '', Format( 'SO_G_NUM%s', [ GetCampoAdicional( iValor ) ] ), '', 0, iGlobal, 0, True );
                         cdtFecha:    Add( eTipo, GetGlobalString( iGlobal ), '', Format( 'SO_G_FEC%s', [ GetCampoAdicional( iValor ) ] ), '', 0, iGlobal, 0, True );
                         cdtBooleano: Add( eTipo, GetGlobalString( iGlobal ), '', Format( 'SO_G_LOG%s', [ GetCampoAdicional( iValor ) ] ), '', 0, iGlobal, 0, True );
                         cdtLookup:   Add( eTipo, GetGlobalString( iGlobal ), '', Format( 'SO_G_TAB%s', [ GetCampoAdicional( iValor ) ] ), '', 0, iGlobal, 0, True );
                    end;
               end;
          end;
     end;
end;

procedure TdmSeleccion.cdsEstadosGetRights(Sender: TZetaClientDataSet;
          const iRight: Integer; var lHasRights: Boolean);
begin
     lHasRights := FALSE;
end;

function TdmSeleccion.SetControlesEmpresaTress( lblLabel: TLabel; lblControl : TControl ): boolean;
begin
     Result := strLleno( Global.GetGlobalString( K_GLOBAL_EMPRESA_TRESS ) );
     lblLabel.Enabled := Result;
     lblControl.Enabled := Result;
end;

procedure TdmSeleccion.cdsTransporteAlAdquirirDatos(Sender: TObject);
begin
     with TZetaClientDataSet( Sender ) do
     begin
          Data := ServerSeleccion.GetTabla( dmCliente.EmpresaTress, Tag );
     end;
end;

end.
