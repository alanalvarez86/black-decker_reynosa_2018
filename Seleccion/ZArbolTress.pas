unit ZArbolTress;

interface

uses ComCtrls, Dialogs, SysUtils, DB, ZArbolTools;

function  GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );

implementation

uses ZetaDespConsulta,
     ZetaCommonTools,
     ZAccesosMgr,
     ZAccesosTress,
     ZGlobalTress,
     DGlobal,
     DSistema,
     DCliente;

const
     K_ARCHIVO                          = 750;
     K_ARCH_REQUISICION                 = 751;
     K_ARCH_SOLICITUD                   = 752;
     K_ARCH_REGISTRO                    = 753;
     K_CONSULTAS                        = 754;
     K_REPORTES                         = 755;
     K_BITACORA                         = 756;
     K_CATALOGOS                        = 757;
     K_CAT_CLIENTES                     = 758;
     K_CAT_PUESTOS                      = 759;
     K_CAT_TIPO_GASTOS                  = 760;
     K_CAT_AREA_INTERES                 = 761;
     K_TAB_ADICIONALES                  = 762;
     K_CAT_ADICION_TABLA1               = 763;
     K_CAT_ADICION_TABLA2               = 764;
     K_CAT_ADICION_TABLA3               = 765;
     K_CAT_ADICION_TABLA4               = 766;
     K_CAT_ADICION_TABLA5               = 767;
     K_SISTEMA                          = 768;
     K_SIST_DATOS_USUARIOS              = 769;
     K_SIST_DATOS_GRUPOS                = 770;
     K_CONS_SQL                         = 771;
     K_CAT_GRALES_CONDICIONES           = 772;
     K_SIST_DATOS_EMPRESAS              = 773;
     K_SIST_DATOS_IMPRESORAS            = 774;
     K_CAT_CONFIGURACION                = 775;
     K_CAT_CONFI_GLOBALES               = 776;
     K_CAT_CONFI_DICCIONARIO            = 777;
     K_ARCH_PROCESO                     = 778;

function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma   := TRUE;
     Result.Caption   := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma   := FALSE;
     Result.Caption   := sCaption;
     Result.IndexDerechos  := iDerechos;
end;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     with GLobal do
     begin
          case iNodo of
               K_ARCHIVO                 : Result := Folder( 'Archivo', D_ARCHIVO );
               K_ARCH_REQUISICION        : Result := Forma( 'Requisiciones', efcGridRequisicion );
               K_ARCH_SOLICITUD          : Result := Forma( 'Solicitudes', efcGridSolicitud );
               K_ARCH_REGISTRO           : Result := Forma( 'Registro', efcRegArchivo );
               K_ARCH_PROCESO            : Result := Forma( 'Procesos', efcProcArchivo );
               K_CONSULTAS               : Result := Folder( 'Consultas', D_CONSULTAS );
               K_REPORTES                : Result := Forma( 'Reportes', efcReportes );
               K_BITACORA                : Result := Forma( 'Bitácora', efcBitacora );
               K_CONS_SQL                : Result := Forma( 'SQL', efcQueryGral );
               K_CATALOGOS               : Result := Folder( 'Catálogos', D_CATALOGOS );
               K_CAT_PUESTOS             : Result := Forma( 'Puestos', efcCatPuestos );
               K_CAT_CLIENTES            : Result := Forma( 'Clientes', efcCatClientes );
               K_CAT_TIPO_GASTOS         : Result := Forma( 'Tipos de Gasto', efcCatTipoGastos );
               K_CAT_AREA_INTERES        : Result := Forma( 'Areas de Interés', efcCatAreaInteres );
               K_CAT_GRALES_CONDICIONES  : Result := Forma( 'Condiciones', efcCatCondiciones );
//               K_TAB_ADICIONALES         : Result := Folder( 'Adicionales', iNodo );
               K_CAT_ADICION_TABLA1      : Result := Forma( Global.AdTabla( 1 ), efcCatAdicional1 );
               K_CAT_ADICION_TABLA2      : Result := Forma( Global.AdTabla( 2 ), efcCatAdicional2 );
               K_CAT_ADICION_TABLA3      : Result := Forma( Global.AdTabla( 3 ), efcCatAdicional3 );
               K_CAT_ADICION_TABLA4      : Result := Forma( Global.AdTabla( 4 ), efcCatAdicional4 );
               K_CAT_ADICION_TABLA5      : Result := Forma( Global.AdTabla( 5 ), efcCatAdicional5 );
//               K_CAT_CONFIGURACION       : Result := Folder( 'Configuración', iNodo );
               K_CAT_CONFI_GLOBALES      : Result := Forma( 'Globales de Empresa', efcSistGlobales );
               K_CAT_CONFI_DICCIONARIO   : Result := Forma( 'Diccionario de Datos', efcDiccion );
               K_SISTEMA                 : Result := Folder( 'Sistema', D_SISTEMA );
               K_SIST_DATOS_EMPRESAS     : Result := Forma( 'Empresas', efcSistEmpresas );
               K_SIST_DATOS_USUARIOS     : Result := Forma( 'Usuarios', efcSistUsuarios );
               K_SIST_DATOS_GRUPOS       : Result := Forma( 'Grupos de Usuarios', efcSistGrupos );
               K_SIST_DATOS_IMPRESORAS   : Result := Forma( 'Impresoras', efcImpresoras );
            else
                Result := Folder( '', 0 );
            end;
     end;
end;

procedure CreaArbolDefault( oArbolMgr: TArbolMgr );
var
   i : Integer;

   procedure NodoNivel( const iNivel, iNodo: Integer );
   begin
        oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
   end;

begin
    NodoNivel( 0, K_ARCHIVO );
      NodoNivel( 1, K_ARCH_REQUISICION );
      NodoNivel( 1, K_ARCH_SOLICITUD );
      NodoNivel( 1, K_ARCH_REGISTRO );
      NodoNivel( 1, K_ARCH_PROCESO );
    NodoNivel( 0, K_CONSULTAS );
      NodoNivel( 1, K_REPORTES );
      NodoNivel( 1, K_BITACORA );
      NodoNivel( 1, K_CONS_SQL );
    NodoNivel( 0, K_CATALOGOS );
      NodoNivel( 1, K_CAT_PUESTOS );
      NodoNivel( 1, K_CAT_AREA_INTERES );
      NodoNivel( 1, K_CAT_CLIENTES );
      NodoNivel( 1, K_CAT_TIPO_GASTOS );
      NodoNivel( 1, K_CAT_GRALES_CONDICIONES );
      for i := 1 to K_GLOBAL_TAB_MAX do
          if StrLleno( Global.AdTabla( i ) ) then
             NodoNivel( 1, K_TAB_ADICIONALES + i );
    NodoNivel( 0, K_SISTEMA );
      NodoNivel( 1, K_SIST_DATOS_EMPRESAS );
      NodoNivel( 1, K_SIST_DATOS_GRUPOS );
      NodoNivel( 1, K_SIST_DATOS_USUARIOS );
      NodoNivel( 1, K_SIST_DATOS_IMPRESORAS );
      NodoNivel( 1, K_CAT_CONFI_GLOBALES );
      NodoNivel( 1, K_CAT_CONFI_DICCIONARIO );
end;

procedure CreaArbolUsuario( oArbolMgr: TArbolMgr; const sFileName: String );
begin
     with oArbolMgr do
     begin
//          Configuracion := TRUE;    // No valida derechos de acceso
          oArbolMgr.NumDefault := K_ARCH_REQUISICION ;
          CreaArbolDefault( oArbolMgr );
          Arbol.FullExpand;
     end;
end;

end.
