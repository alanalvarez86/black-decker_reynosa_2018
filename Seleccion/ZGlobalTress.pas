unit ZGlobalTress;

interface

uses ZetaCommonLists,
     ZetaCommonClasses;


{ TEMPORAL. Esto va en otra unidad }
{$INCLUDE ..\Seleccion\GlobalesSeleccion.inc}

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
function GlobalToPos( const iCodigo : Integer ) : Integer;
function PosToGlobal( const iPos : Integer ) : Integer;
function GetDescripcionGlobal( const iCodigo : integer ): string;
function GetTipoGlobalAdicional( const iCodigo : Integer; var iEquiv, iValor: integer ) : eCampoTipo;

implementation

{ Permite compactar el arreglo de Globales }
{ Los globales arriba de 2000, les resta un offset }
function GlobalToPos( const iCodigo : Integer ) : Integer;
begin
     if ( iCodigo <= K_NUM_GLOBALES ) then
        Result := iCodigo
     else
        Result := iCodigo - K_BASE_DEFAULTS + K_NUM_GLOBALES;
end;

function PosToGlobal( const iPos : Integer ) : Integer;
begin
     if ( iPos <= K_NUM_GLOBALES ) then
        Result := iPos
     else
        Result := iPos + K_BASE_DEFAULTS - K_NUM_GLOBALES;
end;

function GetTipoGlobal( const iCodigo : Integer ) : eTipoGlobal;
begin
     // Si se tienen Globales que no sean de Texto deber�n condicionarse para regresar otros
     // tipos Globales: tgBooleano, tgFloat, tgNumero, tgFecha, etc.
     if ( iCodigo in [ K_GLOBAL_NUM_GLOBAL1..K_GLOBAL_NUM_GLOBAL3 ] ) then
        Result := tgFloat
     else if ( iCodigo = K_GLOBAL_REF_LABORAL_QTY ) or ( iCodigo in [ K_GLOBAL_TPO_REQUISITO1..K_GLOBAL_TPO_REQUISITO9 ] )
     or ( iCodigo = K_GLOBAL_EMAIL_PORT )  or ( iCodigo = K_GLOBAL_EMAIL_AUTH )  then

        Result := tgNumero
     else
        Result := tgTexto;
end;

function GetDescripcionGlobal( const iCodigo : integer ): string;
begin
     case iCodigo of
          K_GLOBAL_RAZON_EMPRESA : Result := 'Raz�n Social de Empresa';
          K_GLOBAL_CALLE_EMPRESA : Result := 'Calle de Empresa';
          K_GLOBAL_COLONIA_EMPRESA : Result := 'Colonia de Empresa';
          K_GLOBAL_CIUDAD_EMPRESA : Result := 'Ciudad de Empresa';
          K_GLOBAL_ENTIDAD_EMPRESA : Result := 'Entidad de Empresa';
          K_GLOBAL_CP_EMPRESA : Result := 'C�digo Postal Empresa';
          K_GLOBAL_TEL_EMPRESA : Result := 'Tel�fono de Empresa';
          K_GLOBAL_RFC_EMPRESA : Result := 'R.F.C. de Empresa';
          K_GLOBAL_INFONAVIT_EMPRESA : Result := '# de INFONAVIT de Empresa';
          K_GLOBAL_REPRESENTANTE : Result := 'Representante Legal';
          K_GLOBAL_TEXT_GLOBAL1 : Result := 'Texto Global #1';
          K_GLOBAL_TEXT_GLOBAL2 : Result := 'Texto Global #2';
          K_GLOBAL_TEXT_GLOBAL3 : Result := 'Texto Global #3';
          K_GLOBAL_NUM_GLOBAL1 : Result := 'N�mero Global #1';
          K_GLOBAL_NUM_GLOBAL2 : Result := 'N�mero Global #2';
          K_GLOBAL_NUM_GLOBAL3 : Result := 'N�mero Global #3';
          K_GLOBAL_TEXTO1      : Result := 'Nombre de Texto #1';
          K_GLOBAL_TEXTO2      : Result := 'Nombre de Texto #2';
          K_GLOBAL_TEXTO3      : Result := 'Nombre de Texto #3';
          K_GLOBAL_TEXTO4      : Result := 'Nombre de Texto #4';
          K_GLOBAL_TEXTO5      : Result := 'Nombre de Texto #5';
          K_GLOBAL_TEXTO6      : Result := 'Nombre de Texto #6';
          K_GLOBAL_TEXTO7      : Result := 'Nombre de Texto #7';
          K_GLOBAL_TEXTO8      : Result := 'Nombre de Texto #8';
          K_GLOBAL_TEXTO9      : Result := 'Nombre de Texto #9';
          K_GLOBAL_TEXTO10     : Result := 'Nombre de Texto #10';
          K_GLOBAL_TEXTO11     : Result := 'Nombre de Texto #11';
          K_GLOBAL_TEXTO12     : Result := 'Nombre de Texto #12';
          K_GLOBAL_TEXTO13     : Result := 'Nombre de Texto #13';
          K_GLOBAL_TEXTO14     : Result := 'Nombre de Texto #14';
          K_GLOBAL_TEXTO15     : Result := 'Nombre de Texto #15';
          K_GLOBAL_TEXTO16     : Result := 'Nombre de Texto #16';
          K_GLOBAL_TEXTO17     : Result := 'Nombre de Texto #17';
          K_GLOBAL_TEXTO18     : Result := 'Nombre de Texto #18';
          K_GLOBAL_TEXTO19     : Result := 'Nombre de Texto #19';
          K_GLOBAL_TEXTO20     : Result := 'Nombre de Texto #20';
          K_GLOBAL_NUM1        : Result := 'Nombre de N�mero #1';
          K_GLOBAL_NUM2        : Result := 'Nombre de N�mero #2';
          K_GLOBAL_NUM3        : Result := 'Nombre de N�mero #3';
          K_GLOBAL_NUM4        : Result := 'Nombre de N�mero #4';
          K_GLOBAL_NUM5        : Result := 'Nombre de N�mero #5';
          K_GLOBAL_NUM6        : Result := 'Nombre de N�mero #6';
          K_GLOBAL_NUM7        : Result := 'Nombre de N�mero #7';
          K_GLOBAL_NUM8        : Result := 'Nombre de N�mero #8';
          K_GLOBAL_NUM9        : Result := 'Nombre de N�mero #9';
          K_GLOBAL_NUM10       : Result := 'Nombre de N�mero #10';
          K_GLOBAL_LOG1        : Result := 'Nombre de L�gico #1';
          K_GLOBAL_LOG2        : Result := 'Nombre de L�gico #2';
          K_GLOBAL_LOG3        : Result := 'Nombre de L�gico #3';
          K_GLOBAL_LOG4        : Result := 'Nombre de L�gico #4';
          K_GLOBAL_LOG5        : Result := 'Nombre de L�gico #5';
          K_GLOBAL_LOG6        : Result := 'Nombre de L�gico #6';
          K_GLOBAL_LOG7        : Result := 'Nombre de L�gico #7';
          K_GLOBAL_LOG8        : Result := 'Nombre de L�gico #8';
          K_GLOBAL_LOG9        : Result := 'Nombre de L�gico #9';
          K_GLOBAL_LOG10       : Result := 'Nombre de L�gico #10';
          K_GLOBAL_FECHA1      : Result := 'Nombre de Fecha #1';
          K_GLOBAL_FECHA2      : Result := 'Nombre de Fecha #2';
          K_GLOBAL_FECHA3      : Result := 'Nombre de Fecha #3';
          K_GLOBAL_FECHA4      : Result := 'Nombre de Fecha #4';
          K_GLOBAL_FECHA5      : Result := 'Nombre de Fecha #5';
          K_GLOBAL_TAB1        : Result := 'Nombre de Tabla #1';
          K_GLOBAL_TAB2        : Result := 'Nombre de Tabla #2';
          K_GLOBAL_TAB3        : Result := 'Nombre de Tabla #3';
          K_GLOBAL_TAB4        : Result := 'Nombre de Tabla #4';
          K_GLOBAL_TAB5        : Result := 'Nombre de Tabla #5';
          K_GLOBAL_POSICION_ADIC : Result := 'Adicionales a Capturar';
          K_GLOBAL_REQUISITO1 : Result := 'Requisito #1';
          K_GLOBAL_REQUISITO2 : Result := 'Requisito #2';
          K_GLOBAL_REQUISITO3 : Result := 'Requisito #3';
          K_GLOBAL_REQUISITO4 : Result := 'Requisito #4';
          K_GLOBAL_REQUISITO5 : Result := 'Requisito #5';
          K_GLOBAL_REQUISITO6 : Result := 'Requisito #6';
          K_GLOBAL_REQUISITO7 : Result := 'Requisito #7';
          K_GLOBAL_REQUISITO8 : Result := 'Requisito #8';
          K_GLOBAL_REQUISITO9 : Result := 'Requisito #9';
          K_GLOBAL_DIR_PLANT  : Result := 'Directorio Plantillas';
          K_GLOBAL_CB_TEXTO1  : Result := 'Campo en Tress de Texto #1';
          K_GLOBAL_CB_TEXTO2  : Result := 'Campo en Tress de Texto #2';
          K_GLOBAL_CB_TEXTO3  : Result := 'Campo en Tress de Texto #3';
          K_GLOBAL_CB_TEXTO4  : Result := 'Campo en Tress de Texto #4';
          K_GLOBAL_CB_TEXTO5  : Result := 'Campo en Tress de Texto #5';
          K_GLOBAL_CB_TEXTO6  : Result := 'Campo en Tress de Texto #6';
          K_GLOBAL_CB_TEXTO7  : Result := 'Campo en Tress de Texto #7';
          K_GLOBAL_CB_TEXTO8  : Result := 'Campo en Tress de Texto #8';
          K_GLOBAL_CB_TEXTO9  : Result := 'Campo en Tress de Texto #9';
          K_GLOBAL_CB_TEXTO10 : Result := 'Campo en Tress de Texto #10';
          K_GLOBAL_CB_TEXTO11 : Result := 'Campo en Tress de Texto #11';
          K_GLOBAL_CB_TEXTO12 : Result := 'Campo en Tress de Texto #12';
          K_GLOBAL_CB_TEXTO13 : Result := 'Campo en Tress de Texto #13';
          K_GLOBAL_CB_TEXTO14 : Result := 'Campo en Tress de Texto #14';
          K_GLOBAL_CB_TEXTO15 : Result := 'Campo en Tress de Texto #15';
          K_GLOBAL_CB_TEXTO16 : Result := 'Campo en Tress de Texto #16';
          K_GLOBAL_CB_TEXTO17 : Result := 'Campo en Tress de Texto #17';
          K_GLOBAL_CB_TEXTO18 : Result := 'Campo en Tress de Texto #18';
          K_GLOBAL_CB_TEXTO19 : Result := 'Campo en Tress de Texto #19';
          K_GLOBAL_CB_TEXTO20 : Result := 'Campo en Tress de Texto #20';
          K_GLOBAL_CB_NUM1  : Result := 'Campo en Tress de N�mero #1';
          K_GLOBAL_CB_NUM2  : Result := 'Campo en Tress de N�mero #2';
          K_GLOBAL_CB_NUM3  : Result := 'Campo en Tress de N�mero #3';
          K_GLOBAL_CB_NUM4  : Result := 'Campo en Tress de N�mero #4';
          K_GLOBAL_CB_NUM5  : Result := 'Campo en Tress de N�mero #5';
          K_GLOBAL_CB_NUM6  : Result := 'Campo en Tress de N�mero #6';
          K_GLOBAL_CB_NUM7  : Result := 'Campo en Tress de N�mero #7';
          K_GLOBAL_CB_NUM8  : Result := 'Campo en Tress de N�mero #8';
          K_GLOBAL_CB_NUM9  : Result := 'Campo en Tress de N�mero #9';
          K_GLOBAL_CB_NUM10 : Result := 'Campo en Tress de N�mero #10';

          K_GLOBAL_CB_LOG1  : Result := 'Campo en Tress de L�gico #1';
          K_GLOBAL_CB_LOG2  : Result := 'Campo en Tress de L�gico #2';
          K_GLOBAL_CB_LOG3  : Result := 'Campo en Tress de L�gico #3';
          K_GLOBAL_CB_LOG4  : Result := 'Campo en Tress de L�gico #4';
          K_GLOBAL_CB_LOG5  : Result := 'Campo en Tress de L�gico #5';
          K_GLOBAL_CB_LOG6  : Result := 'Campo en Tress de L�gico #6';
          K_GLOBAL_CB_LOG7  : Result := 'Campo en Tress de L�gico #7';
          K_GLOBAL_CB_LOG8  : Result := 'Campo en Tress de L�gico #8';
          K_GLOBAL_CB_LOG9  : Result := 'Campo en Tress de L�gico #9';
          K_GLOBAL_CB_LOG10 : Result := 'Campo en Tress de L�gico #10';
          K_GLOBAL_CB_FECHA1 : Result := 'Campo en Tress de Fecha #1';
          K_GLOBAL_CB_FECHA2 : Result := 'Campo en Tress de Fecha #2';
          K_GLOBAL_CB_FECHA3 : Result := 'Campo en Tress de Fecha #3';
          K_GLOBAL_CB_FECHA4 : Result := 'Campo en Tress de Fecha #4';
          K_GLOBAL_CB_FECHA5 : Result := 'Campo en Tress de Fecha #5';
          K_GLOBAL_CB_TAB1 : Result := 'Campo en Tress de Tabla #1';
          K_GLOBAL_CB_TAB2 : Result := 'Campo en Tress de Tabla #2';
          K_GLOBAL_CB_TAB3 : Result := 'Campo en Tress de Tabla #3';
          K_GLOBAL_CB_TAB4 : Result := 'Campo en Tress de Tabla #4';
          K_GLOBAL_CB_TAB5 : Result := 'Campo en Tress de Tabla #5';
          K_GLOBAL_VERSION_DATOS : Result := 'Version Datos';
          K_GLOBAL_REF_LABORAL1  : Result := 'Nombre Referencia Laboral 1';
          K_GLOBAL_REF_LABORAL2  : Result := 'Nombre Referencia Laboral 2';
          K_GLOBAL_REF_LABORAL3  : Result := 'Nombre Referencia Laboral 3';
          K_GLOBAL_REF_LABORAL4  : Result := 'Nombre Referencia Laboral 4';
          K_GLOBAL_REF_LABORAL5  : Result := 'Nombre Referencia Laboral 5';
          K_GLOBAL_REF_LABORAL6  : Result := 'Nombre Referencia Laboral 6';
          K_GLOBAL_REF_LABORAL7  : Result := 'Nombre Referencia Laboral 7';
          K_GLOBAL_REF_LABORAL8  : Result := 'Nombre Referencia Laboral 8';
          K_GLOBAL_REF_LABORAL9  : Result := 'Nombre Referencia Laboral 9';
          K_GLOBAL_REF_LABORAL10  : Result := 'Nombre Referencia Laboral 10';
          K_GLOBAL_EMAIL_HOST       : Result := 'Servidor de Correos';
          K_GLOBAL_EMAIL_PORT       : Result := 'Puerto';
          K_GLOBAL_EMAIL_USERID     : Result := 'ID. Usuario';
          K_GLOBAL_EMAIL_PSWD       : Result := 'Clave Correo';
          K_GLOBAL_EMAIL_FROMADRESS : Result := 'Direcci�n del Email';
          K_GLOBAL_EMAIL_FROMNAME   : Result := 'Descripci�n del Email';
          K_GLOBAL_EMAIL_MSGERROR   : Result := 'Direcci�n para Errores';
          K_GLOBAL_EMAIL_ANUNCIO : Result := 'Direcci�n para Anuncio';
          K_GLOBAL_GRABA_BITACORA : Result := 'Movimientos que se registran en Bit�cora';
          K_GLOBAL_EMPRESA_TRESS  : Result := 'Empresa de Tress para Obtener Cat�logos';
          K_GLOBAL_TPO_REQUISITO1     : Result := 'Tipo Requisito #1';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO2     : Result := 'Tipo Requisito #2';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO3     : Result := 'Tipo Requisito #3';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO4     : Result := 'Tipo Requisito #4';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO5     : Result := 'Tipo Requisito #5';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO6     : Result := 'Tipo Requisito #6';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO7     : Result := 'Tipo Requisito #7';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO8     : Result := 'Tipo Requisito #8';{OP:16.Abr.08}
          K_GLOBAL_TPO_REQUISITO9     : Result := 'Tipo Requisito #9';{OP:16.Abr.08}
          K_GLOBAL_REF_LABORAL11  : Result := 'Nombre Referencia Laboral 11';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL12  : Result := 'Nombre Referencia Laboral 12';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL13  : Result := 'Nombre Referencia Laboral 13';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL14  : Result := 'Nombre Referencia Laboral 14';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL15  : Result := 'Nombre Referencia Laboral 15';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL16  : Result := 'Nombre Referencia Laboral 16';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL17  : Result := 'Nombre Referencia Laboral 17';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL18  : Result := 'Nombre Referencia Laboral 18';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL19  : Result := 'Nombre Referencia Laboral 19';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORAL20  : Result := 'Nombre Referencia Laboral 20';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV1: Result := 'Nombre Referencia Laboral 1, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV2: Result := 'Nombre Referencia Laboral 2, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV3: Result := 'Nombre Referencia Laboral 3, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV4: Result := 'Nombre Referencia Laboral 4, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV5: Result := 'Nombre Referencia Laboral 5, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV6: Result := 'Nombre Referencia Laboral 6, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV7: Result := 'Nombre Referencia Laboral 7, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV8: Result := 'Nombre Referencia Laboral 8, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV9: Result := 'Nombre Referencia Laboral 9, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV10: Result := 'Nombre Referencia Laboral 10, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV11: Result := 'Nombre Referencia Laboral 11, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV12: Result := 'Nombre Referencia Laboral 12, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV13: Result := 'Nombre Referencia Laboral 13, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV14: Result := 'Nombre Referencia Laboral 14, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV15: Result := 'Nombre Referencia Laboral 15, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV16: Result := 'Nombre Referencia Laboral 16, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV17: Result := 'Nombre Referencia Laboral 17, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV18: Result := 'Nombre Referencia Laboral 18, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV19: Result := 'Nombre Referencia Laboral 19, Resultados Verificaci�n';{OP:18.Abr.08}
          K_GLOBAL_REF_LABORALRV20: Result := 'Nombre Referencia Laboral 20, Resultados Verificaci�n';{OP:18.Abr.08}
     else
         Result := 'Descripci�n No Disponible';
     end;
end;

function GetTipoGlobalAdicional( const iCodigo : Integer; var iEquiv, iValor: integer ) : eCampoTipo;
begin
     case iCodigo of
          K_GLOBAL_TEXTO1:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO1;
               iValor := K_INDICE_1;
          end;
          K_GLOBAL_TEXTO2:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO2;
               iValor := K_INDICE_2;
          end;
          K_GLOBAL_TEXTO3:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO3;
               iValor := K_INDICE_3;
          end;
          K_GLOBAL_TEXTO4:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO4;
               iValor := K_INDICE_4;
          end;
          K_GLOBAL_TEXTO5:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO5;
               iValor := K_INDICE_5;
          end;
          K_GLOBAL_TEXTO6:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO6;
               iValor := K_INDICE_6;
          end;
          K_GLOBAL_TEXTO7:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO7;
               iValor := K_INDICE_7;
          end;
          K_GLOBAL_TEXTO8:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO8;
               iValor := K_INDICE_8;
          end;
          K_GLOBAL_TEXTO9:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO9;
               iValor := K_INDICE_9;
          end;
          K_GLOBAL_TEXTO10:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO10;
               iValor := K_INDICE_10;
          end;
          K_GLOBAL_TEXTO11:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO11;
               iValor := K_INDICE_11;
          end;
          K_GLOBAL_TEXTO12:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO12;
               iValor := K_INDICE_12;
          end;
          K_GLOBAL_TEXTO13:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO13;
               iValor := K_INDICE_13;
          end;
          K_GLOBAL_TEXTO14:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO14;
               iValor := K_INDICE_14;
          end;
          K_GLOBAL_TEXTO15:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO15;
               iValor := K_INDICE_15;
          end;
          K_GLOBAL_TEXTO16:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO16;
               iValor := K_INDICE_16;
          end;
          K_GLOBAL_TEXTO17:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO17;
               iValor := K_INDICE_17;
          end;
          K_GLOBAL_TEXTO18:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO18;
               iValor := K_INDICE_18;
          end;
          K_GLOBAL_TEXTO19:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO19;
               iValor := K_INDICE_19;
          end;
          K_GLOBAL_TEXTO20:
          begin
               Result := cdtTexto;
               iEquiv := K_GLOBAL_CB_TEXTO20;
               iValor := K_INDICE_20;
          end;
          K_GLOBAL_NUM1:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM1;
               iValor := K_INDICE_1;
          end;
          K_GLOBAL_NUM2:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM2;
               iValor := K_INDICE_2;
          end;
          K_GLOBAL_NUM3:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM3;
               iValor := K_INDICE_3;
          end;
          K_GLOBAL_NUM4:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM4;
               iValor := K_INDICE_4;
          end;
          K_GLOBAL_NUM5:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM5;
               iValor := K_INDICE_5;
          end;
          K_GLOBAL_NUM6:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM6;
               iValor := K_INDICE_6;
          end;
          K_GLOBAL_NUM7:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM7;
               iValor := K_INDICE_7;
          end;
          K_GLOBAL_NUM8:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM8;
               iValor := K_INDICE_8;
          end;
          K_GLOBAL_NUM9:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM9;
               iValor := K_INDICE_9;
          end;
          K_GLOBAL_NUM10:
          begin
               Result := cdtNumero;
               iEquiv := K_GLOBAL_CB_NUM10;
               iValor := K_INDICE_10;
          end;
          K_GLOBAL_LOG1:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG1;
               iValor := K_INDICE_1;
          end;
          K_GLOBAL_LOG2:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG2;
               iValor := K_INDICE_2;
          end;
          K_GLOBAL_LOG3:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG3;
               iValor := K_INDICE_3;
          end;
          K_GLOBAL_LOG4:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG4;
               iValor := K_INDICE_4;
          end;
          K_GLOBAL_LOG5:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG5;
               iValor := K_INDICE_5;
          end;
          K_GLOBAL_LOG6:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG6;
               iValor := K_INDICE_6;
          end;
          K_GLOBAL_LOG7:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG7;
               iValor := K_INDICE_7;
          end;
          K_GLOBAL_LOG8:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG8;
               iValor := K_INDICE_8;
          end;
          K_GLOBAL_LOG9:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG9;
               iValor := K_INDICE_9;
          end;
          K_GLOBAL_LOG10:
          begin
               Result := cdtBooleano;
               iEquiv := K_GLOBAL_CB_LOG10;
               iValor := K_INDICE_10;
          end;
          K_GLOBAL_FECHA1:
          begin
               Result := cdtFecha;
               iEquiv := K_GLOBAL_CB_FECHA1;
               iValor := K_INDICE_1;
          end;
          K_GLOBAL_FECHA2:
          begin
               Result := cdtFecha;
               iEquiv := K_GLOBAL_CB_FECHA2;
               iValor := K_INDICE_2;
          end;
          K_GLOBAL_FECHA3:
          begin
               Result := cdtFecha;
               iEquiv := K_GLOBAL_CB_FECHA3;
               iValor := K_INDICE_3;
          end;
          K_GLOBAL_FECHA4:
          begin
               Result := cdtFecha;
               iEquiv := K_GLOBAL_CB_FECHA4;
               iValor := K_INDICE_4;
          end;
          K_GLOBAL_FECHA5:
          begin
               Result := cdtFecha;
               iEquiv := K_GLOBAL_CB_FECHA5;
               iValor := K_INDICE_5;
          end;
          K_GLOBAL_TAB1:
          begin
               Result := cdtLookup;
               iEquiv := K_GLOBAL_CB_TAB1;
               iValor := K_INDICE_1;
          end;
          K_GLOBAL_TAB2:
          begin
               Result := cdtLookup;
               iEquiv := K_GLOBAL_CB_TAB2;
               iValor := K_INDICE_2;
          end;
          K_GLOBAL_TAB3:
          begin
               Result := cdtLookup;
               iEquiv := K_GLOBAL_CB_TAB3;
               iValor := K_INDICE_3;
          end;
          K_GLOBAL_TAB4:
          begin
               Result := cdtLookup;
               iEquiv := K_GLOBAL_CB_TAB4;
               iValor := K_INDICE_4;
          end;
          K_GLOBAL_TAB5:
          begin
               Result := cdtLookup;
               iEquiv := K_GLOBAL_CB_TAB5;
               iValor := K_INDICE_5;
          end;
     else
         Result := cdtNinguno;
         iEquiv := K_INDICE_0;
         iValor := K_INDICE_0;
     end;
end;

end.
