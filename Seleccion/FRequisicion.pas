unit FRequisicion;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, ExtCtrls, Grids, DBGrids,
     ZBaseConsulta,
     ZetaDBGrid, StdCtrls, ZetaKeyCombo,
     ZetaCommonClasses, Mask, ZetaNumero, ZetaKeyLookup, Buttons;

type
  TRequisicion = class(TBaseConsulta)
    ZetaDBGrid: TZetaDBGrid;
    PanelFiltro: TPanel;
    AreaLBL: TLabel;
    CriterioLbl: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdadMinimaLBL: TLabel;
    EdadMaximaLBL: TLabel;
    Refrescar: TBitBtn;
    Cliente: TZetaKeyLookup;
    Puesto: TZetaKeyLookup;
    cbStatus: TComboBox;
    cbPrioridad: TComboBox;
    FolioInicial: TZetaNumero;
    FolioFinal: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure RefrescarClick(Sender: TObject);
  private
    { Private declarations }
    FParametros: TZetaParams;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations}
  end;

var
  Requisicion: TRequisicion;

implementation

uses DSeleccion,
     ZetaBuscador,
     ZetaCommonLists,
     ZAccesosMgr,
     ZAccesosTress,
     DSistema;

{$R *.DFM}

procedure TRequisicion.FormCreate(Sender: TObject);
  procedure FillCombo( Control: TComboBox; Tipo: ListasFijas; const sTodos: String );
  begin
       with Control do
       begin
            ZetaCommonLists.LlenaLista( Tipo, Items );
            Items.Insert( 0, Format( '< %s >', [ sTodos ] ) );
            ItemIndex := 1;
       end;
  end;
begin
     inherited;
     FParametros:= TZetaParams.Create;
     //CanLookup := True;
     HelpContext:= H00077_Requisicion_Personal;
     FillCombo( cbStatus, lfReqStatus, 'Todos' );
     FillCombo( cbPrioridad, lfPrioridad, 'Todas' );
     cbPrioridad.ItemIndex := 0;
     dmSeleccion.cdsRequiere.SetDataChange;
     
     Cliente.LookupDataset := dmSeleccion.cdsCliente;
     Puesto.LookupDataset := dmSeleccion.cdsPuestos;
end;

procedure TRequisicion.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmSeleccion do
     begin
          cdsCliente.Conectar;
          cdsAreaInteres.Conectar;
          cdsPuestos.Conectar;
          cdsCondiciones.Conectar;
          DataSource.DataSet:= cdsRequiere;
          if ( cdsRequiere.HayQueRefrescar ) then
             Refresh;
     end;
end;

procedure TRequisicion.Refresh;
var
   iFolio: Integer;
begin
     with FParametros do
     begin
          AddInteger( 'Status', CBStatus.ItemIndex - 1 );
          AddInteger( 'Prioridad', CBPrioridad.ItemIndex - 1 );
          AddInteger( 'FolioInicial', FolioInicial.ValorEntero );
          AddInteger( 'FolioFinal', FolioFinal.ValorEntero );
          AddString( 'Cliente', Cliente.Llave );
          AddString( 'Puesto', Puesto.Llave );
     end;
     with dmSeleccion do
     begin
          iFolio := FolioRequisicion;                   // Si hubo registro tiene el n�mero de folio, si no se posiciona sobre el actual del grid
          RefrescarRequiere( FParametros );
          with cdsRequiere do
               if ( iFolio > 0 ) and ( not Locate( 'RQ_FOLIO', iFolio, [ ] ) ) then
                  First;
     end;
end;

function TRequisicion.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := ZAccesosMgr.Revisa( D_REGISTRO_REQUISICION );
     if not Result then
        sMensaje := 'No Tiene Permiso Para Agregar Registros';
end;

procedure TRequisicion.Agregar;
begin
     inherited;
     dmSeleccion.cdsRequiere.Agregar;
end;

procedure TRequisicion.Borrar;
begin
     inherited;
     dmSeleccion.cdsRequiere.Borrar;
end;

procedure TRequisicion.Modificar;
begin
     inherited;
     dmSeleccion.cdsRequiere.Modificar;
end;

procedure TRequisicion.RefrescarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
     ZetaDBGrid.SetFocus;
end;

end.


