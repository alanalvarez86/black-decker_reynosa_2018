unit ZetaDespConsulta;

interface

uses Forms, Controls, Sysutils, Dialogs,
     ZBaseConsulta;

type
  eFormaConsulta = ( efcNinguna,
                     efcGridRequisicion,
                     efcGridSolicitud,
                     efcRegArchivo,
                     efcProcArchivo,
                     efcReportes,
                     efcBitacora,
                     efcCatClientes,
                     efcCatPuestos,
                     efcCatTipoGastos,
                     efcCatAreaInteres,
                     efcCatAdicional1,
                     efcCatAdicional2,
                     efcCatAdicional3,
                     efcCatAdicional4,
                     efcCatAdicional5,
                     efcSistGlobales,
                     efcSistUsuarios,
                     efcSistGrupos,
                     efcQueryGral,
                     efcCatCondiciones,
                     efcSistEmpresas,
                     efcImpresoras,
                     efcDiccion,
                     efcSistProcesos );

  TPropConsulta = record
    IndexDerechos: Integer;
    ClaseForma: TBaseConsultaClass;
  end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;

implementation

uses ZAccesosMgr,
     ZAccesosTress,
     FReportes,
     FQueryGral,
     FSistEmpresas,
     FSistGrupos,
     FSistUsuarios,
     FCatCondiciones,
     FGlobales,
     FRequisicion,
     FGridSolicitud,
     FSistBitacora,
     FSistProcesos,
     FImpresoras,
     FCatPuestos,
     FRegArchivo,
     FProcArchivo,
     FTablas,
     FCatClientes,
     FDiccion;

function Consulta( const iDerechos: Integer; const Forma: TBaseConsultaClass ): TPropConsulta;
begin
     with Result do
     begin
          IndexDerechos := iDerechos;
          ClaseForma := Forma;
     end;
end;

function GetPropConsulta( const Forma: eFormaConsulta ): TPropConsulta;
begin
     case Forma of
          efcGridRequisicion         : Result := Consulta( D_REQUISICION_DATOS, TRequisicion );
          efcGridSolicitud           : Result := Consulta( D_SOLICITUD_DATOS, TGridSolicitud );
          efcRegArchivo              : Result := Consulta( D_REGISTRO, TRegArchivo );
          efcProcArchivo             : Result := Consulta( D_PROCESO, TProcArchivo );
          efcReportes                : Result := Consulta( D_CONS_REPORTES, TReportes );
          efcBitacora                : Result := Consulta( D_CONS_BITACORA, TSistBitacora );
          efcSistProcesos            : Result := Consulta( D_CONS_BITACORA, TSistProcesos );
          efcCatClientes             : Result := Consulta( D_CAT_CLIENTES, TCatClientes );
          efcCatPuestos              : Result := Consulta( D_CAT_PUESTOS, TCatPuestos );
          efcCatTipoGastos           : Result := Consulta( D_CAT_TIPO_GASTOS, TCatTipoGastos );
          efcCatAreaInteres          : Result := Consulta( D_CAT_AREAS_INTERES, TCatAreaInteres );
          efcCatAdicional1           : Result := Consulta( D_CAT_ADICION_TABLA1, TCatAdicional1 );
          efcCatAdicional2           : Result := Consulta( D_CAT_ADICION_TABLA2, TCatAdicional2 );
          efcCatAdicional3           : Result := Consulta( D_CAT_ADICION_TABLA3, TCatAdicional3 );
          efcCatAdicional4           : Result := Consulta( D_CAT_ADICION_TABLA4, TCatAdicional4 );
          efcCatAdicional5           : Result := Consulta( D_CAT_ADICION_TABLA5, TCatAdicional5 );
          efcSistGlobales            : Result := Consulta( D_CAT_CONFI_GLOBALES, TSistGlobales );
          efcSistUsuarios            : Result := Consulta( D_SIST_DATOS_USUARIOS, TSistUsuarios );
          efcSistGrupos              : Result := Consulta( D_SIST_DATOS_GRUPOS, TSistGrupos );
          efcSistEmpresas            : Result := Consulta( D_SIST_DATOS_EMPRESAS, TSistEmpresas );
          efcQueryGral               : Result := Consulta( D_CONS_SQL, TQueryGral );
          efcCatCondiciones          : Result := Consulta( D_CAT_GRALES_CONDICIONES, TCatCondiciones );
          efcImpresoras              : Result := Consulta( D_SIST_DATOS_IMPRESORAS, TImpresoras );
          efcDiccion                 : Result := Consulta( D_CAT_CONFI_DICCIONARIO, TDiccion );
     else
         Result := Consulta( 0, nil );
     end;
end;

end.
