unit ZConstruyeFormula;

interface
uses Classes,
     Forms,
     Controls,
     ZetaTipoEntidad,
     ZetaCommonLists;

function GetFormulaConst( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
function GetFormulaUnlimited( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
function GetFormulaConstruye( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): Boolean;
function GetFormulaConstruyeParams( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const oParams : TStrings ): Boolean;

implementation
uses FConstruyeFormula;

function GetFormulaConstruye( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo : eTipoEvaluador ): Boolean;
begin
     if ( ConstruyeFormula = nil ) then
        ConstruyeFormula:= TConstruyeFormula.Create( Application );
     with ConstruyeFormula do
     begin
          Formula := sFormula;
          PosCursor := oPosCursor;
          EntidadActiva := oEntidad;
          TipoEvaluador := eTipo;
          ShowModal;
          Result := ( ModalResult = mrOk );
          if Result then
             sFormula := Formula;
     end;
end;

function GetFormula( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const lTruncar: Boolean ): String;
var
   oConstruyeFormula: TConstruyeFormula;
begin
     oConstruyeFormula := TConstruyeFormula.Create( Application.MainForm );
     with oConstruyeFormula do
     begin
          try
             Truncar := lTruncar;
             Formula := sFormula;
             EntidadActiva := oEntidad;
             PosCursor := oPosCursor;
             TipoEvaluador := eTipo;
             ShowModal;
             if ( ModalResult = mrOK ) then
                Result := Formula
             else
                 Result := sFormula;
          finally
                 Free;
          end;
     end;
end;

function GetFormulaConst( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
begin
     Result := GetFormula( oEntidad, sFormula, oPosCursor, eTipo, True );
end;

function GetFormulaUnlimited( const oEntidad: TipoEntidad; const sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador ): String;
begin
     Result := GetFormula( oEntidad, sFormula, oPosCursor, eTipo, False );
end;

function GetFormulaConstruyeParams( const oEntidad: TipoEntidad; var sFormula: String; oPosCursor: Integer; const eTipo: eTipoEvaluador; const oParams : TStrings ): Boolean;
begin
     ConstruyeFormula:= TConstruyeFormula.Create( Application );
     try
        with ConstruyeFormula do
        begin
             Formula := sFormula;
             PosCursor := oPosCursor;
             EntidadActiva := oEntidad;
             TipoEvaluador := eTipo;
             Params := oParams;
             ShowModal;
             Result := ( ModalResult = mrOk );
             if Result then
                sFormula := Formula;
        end;
    finally
           ConstruyeFormula.Free;
    end;
end;

end.
