inherited TressShell: TTressShell
  Left = 190
  Top = 144
  Width = 579
  Height = 390
  Caption = 'Selección de Personal'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ArbolSplitter: TSplitter
    Height = 325
  end
  inherited StatusBar: TStatusBar
    Top = 325
    Width = 571
  end
  inherited PanelArbol: TPanel
    Height = 325
    inherited Arbol: TTreeView
      Height = 293
    end
  end
  inherited PanelConsulta: TPanel
    Width = 384
    Height = 325
    inherited ClientAreaPG: TPageControl
      Width = 384
      Height = 295
    end
    inherited PanelToolBar: TPanel
      Width = 384
      inherited CerrarVentana: TZetaSpeedButton
        Left = 354
      end
    end
  end
  inherited ActionList: TActionList
    inherited _A_CatalogoUsuarios: TAction
      Enabled = True
      OnExecute = _A_CatalogoUsuariosExecute
    end
    object _R_Requisicion: TAction
      Category = 'MenuRegistro'
      Caption = '&Requisición'
      Hint = 'Registro de Requisiciones'
      OnExecute = _R_RequisicionExecute
    end
    object _R_Solicitud: TAction
      Category = 'MenuRegistro'
      Caption = '&Solicitud'
      Hint = 'Registro de Solicitantes'
      OnExecute = _R_SolicitudExecute
    end
    object _P_Depurar: TAction
      Category = 'MenuProceso'
      Caption = '&Depuración'
      Hint = 'Depuración de Información'
      OnExecute = _P_DepurarExecute
    end
  end
  inherited ShellMenu: TMainMenu
    inherited Registro: TMenuItem
      object Requisicin1: TMenuItem
        Action = _R_Requisicion
      end
      object Solicitud1: TMenuItem
        Action = _R_Solicitud
      end
    end
    inherited Procesos: TMenuItem
      object Depuracin1: TMenuItem
        Action = _P_Depurar
      end
    end
  end
end
