inherited PreguntaPassWord: TPreguntaPassWord
  Left = 446
  Top = 246
  Caption = 'Configuraci'#243'n  de Cafeter'#237'a'
  ClientHeight = 87
  ClientWidth = 227
  Color = clWhite
  OldCreateOrder = True
  OnShow = FormShow
  ExplicitWidth = 233
  ExplicitHeight = 115
  PixelsPerInch = 96
  TextHeight = 13
  object LblCred: TLabel [0]
    Left = 24
    Top = 15
    Width = 54
    Height = 23
    Caption = 'Clave:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Arial Black'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 51
    Width = 227
    TabOrder = 1
    ExplicitTop = 61
    ExplicitWidth = 227
    inherited OK_DevEx: TcxButton
      Left = 61
      Top = 5
      ExplicitLeft = 61
      ExplicitTop = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 142
      Top = 5
      ExplicitLeft = 142
      ExplicitTop = 5
    end
  end
  object ePassWord: TZetaEdit [2]
    Left = 84
    Top = 10
    Width = 121
    Height = 31
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial Black'
    Font.Style = []
    HideSelection = False
    ParentFont = False
    PasswordChar = '*'
    TabOrder = 0
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 4784120
  end
end
