unit FConfirmaComida;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, Buttons,
  ExtCtrls, ZetaEdit, OleCtrls, {SFEStandardLib_TLB,} dxSkinsCore, SFE,
  TressMorado2013, cxStyles, cxClasses, cxLookAndFeels, dxSkinsForm,
  ZBaseDlgModal, cxGraphics, cxLookAndFeelPainters, Vcl.Menus, Vcl.ImgList,
  cxButtons;

type
  TConfirmaComida = class(TZetaDlgModal_DevEx)
    LbCred: TLabel;
    LblTipo: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    lbSegundo: TLabel;
    Checada: TButton;
    eCredencial: TZetaEdit;
    eEmpleado: TZetaEdit;
    TimerSegundos: TTimer; // BIOMETRICO
    TimerSensorBiometrico: TTimer; // BIOMETRICO
    procedure ChecadaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TimerSegundosTimer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    // BIOMETRICO
    procedure TimerSensorBiometricoTimer(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FCredencial,FEmpleado : string;
    FTiempo : integer;
    FDrivers: boolean;
    //SFEStandard1: TSFEStandard; // BIOMETRICO
    procedure SetCredencial(const Value: string);
    procedure SetEmpleado(const Value: string);
    // BIOMETRICO
    procedure DetieneLector;
    procedure enableButtons(bEnable : Boolean);
    procedure switchWorking(bWorking : Boolean);
    function CaptureFinger() : Integer;
    function LeeArchivo:Boolean;
    procedure InicializaSensorBiometrico;
    procedure InicializaBiometrico;
    procedure ConfirmaComida( iUserId : Integer );
    { Private declarations }
  public
    property Credencial : string read FCredencial write SetCredencial;
    property Empleado : string read FEmpleado write SetEmpleado;
  end;

const
     K_DEFAULT_TIMEOUT = 5;
var
  ConfirmaComida: TConfirmaComida;
  gTemplate         : array[0..1403] of Integer; // BIOMETRICO
  gImageBytes       : array[0..65535] of Byte;
  addrOfImageBytes  : PByte;
  addrOfTemplate    : PByte;
  gWorking          : Boolean;

implementation

uses ZetaDialogo, DCafeCliente, ZetaCommonLists, ZetaCommonClasses;

{$R *.DFM}

procedure TConfirmaComida.FormShow(Sender: TObject);
var
   lBiometrico : Boolean; // BIOMETRICO
begin
     inherited;
     Checada.Width := 0;
     FTiempo := K_DEFAULT_TIMEOUT;
     lbSegundo.Caption := IntToStr( K_DEFAULT_TIMEOUT );
     TimerSegundos.Enabled := TRUE;
     with eCredencial do
     begin
          SetFocus;
          SelectAll;
     end;

     // BIOMETRICO
     lBiometrico := ( dmCafeCliente.TipoGafete = egBiometrico );
     if ( lBiometrico ) then
     begin
          TimerSensorBiometrico.Enabled := lBiometrico;
          InicializaBiometrico;
          Label1.Caption := 'Coloque su HUELLA para confirmar.';
     end;

     lbSegundo.Font.Color := RGB (59,175,74);
end;

procedure TConfirmaComida.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     TimerSegundos.Enabled := FALSE;
     // BIOMETRICO
     if ( dmCafeCliente.TipoGafete = egBiometrico ) then
        DetieneLector;
end;

procedure TConfirmaComida.FormCreate(Sender: TObject);
begin
     inherited;
     FDrivers := LoadDLL;
end;

procedure TConfirmaComida.ChecadaClick(Sender: TObject);
begin
     inherited;
     if ( FCredencial = eCredencial.Text ) then
        OK_DevEx.Click
     else
        Cancelar_DevEx.Click;
end;

procedure TConfirmaComida.SetCredencial(const Value: string);
begin
     FCredencial := Value;
     ECredencial.Text := Value;
end;

procedure TConfirmaComida.SetEmpleado(const Value: string);
begin
     FEmpleado := Value;
     EEmpleado.Text := Value;
end;


procedure TConfirmaComida.TimerSegundosTimer(Sender: TObject);
begin
     inherited;

     // BIOMETRICO
     if ( FTiempo > 0 ) then
          Dec( FTiempo )
     else
     begin
          DetieneLector;
          ConfirmaComida(0);
     end;
     lbSegundo.Caption := IntToStr(FTiempo);
end;

// BIOMETRICO
procedure TConfirmaComida.TimerSensorBiometricoTimer(Sender: TObject);

     function InicializaControl : Boolean;
     begin
          Result := False;
          try
             {if ( SFEStandard1 = nil ) then
             begin
                 SFEStandard1 := TSFEStandard.Create(self);
                 SFEStandard1.TabStop := False;
             end;}
            Result := True;
          except
                on E : Exception do
                   Application.HandleException( E );
                     //RechazaChecada( 'Driver de dispositivo biométrico no detectado. ' );
          end;
     end;

begin
     TimerSensorBiometrico.Enabled := False;

     if ( dmCafeCliente.TipoGafete = egBiometrico ) then
     begin
          if ( ( InicializaControl ) and ( LeeArchivo ) ) then
             InicializaSensorBiometrico;
     end;
end;

// BIOMETRICO
procedure TConfirmaComida.DetieneLector;
begin
     if ( FDrivers ) then
        SFE.Close();
     enableButtons(False);
     switchWorking(False);
end;

// BIOMETRICO
procedure TConfirmaComida.switchWorking(bWorking : Boolean);
begin
     gWorking := bWorking;
end;

// BIOMETRICO
procedure TConfirmaComida.enableButtons(bEnable : Boolean);
begin
     switchWorking(Not bEnable);
end;

// BIOMETRICO
function TConfirmaComida.LeeArchivo: Boolean;
var
   strDbFileName : String;
   iArchivoId, nRet : Integer;
begin
     try
        FDrivers := LoadDLL;
        strDbFileName :=  dmCafeCliente.ArchivoHuellas;

        if not FileExists( strDbFileName ) then
        begin
             iArchivoId := FileCreate( strDbFileName );
             FileClose( iArchivoId );
        end;

        //if ( SFEStandard1 <> nil ) then
           {$ifdef ANTES}
           nRet := SFE.Open(strDbFileName, 4, 0)
           {$else}
           if ( FDrivers ) then
              nRet := SFE.Open( PChar( strDbFileName ), K_TIPO_BIOMETRICO, 0);
           {$endif}
        //else
            //nRet := 1;
        if nRet = 0 then
        begin
             enableButtons(True);
             //SFEStandard1.DotNET();
             Result := True;
        end
        else
        begin
             Raise Exception.Create( 'No hay dispositivo biométrico!' );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                switchWorking(False);
                Result := False;
           end;
     end;
end;

// BIOMETRICO
procedure TConfirmaComida.InicializaSensorBiometrico;
var
   nRet, userId, fingerNum: Integer;

     procedure ObtieneLectura;
     label
          stopWorking;
     begin
          if ( LeeArchivo ) then
          begin
               try
                  switchWorking(True);
                  userId := 0;
                  nRet := CaptureFinger; //Devuelve algun emnsaje de lectura o error.

                  if nRet <> 1 then
                     goto stopWorking;
                  if ( FDrivers ) then
                     nRet := SFE.Identify(userId, fingerNum); //Obtiene el Identificador de la huella y el dedo

                  if nRet < 0 then
                       userId := 0
                  else
                      goto stopWorking;
stopWorking:
                  switchWorking(False);
               except
                     on Error: Exception do
                     begin
                          Application.HandleException( Error );
                          switchWorking(False);
                     end;
               end;
          end
     end;

begin
     ObtieneLectura;
     if ( userId > 0 ) then
          ConfirmaComida( userId );
     DetieneLector;
end;

// BIOMETRICO
function TConfirmaComida.CaptureFinger: Integer;
var
  nRet, nCapArea, nMaxCapArea, nResult : Integer;
begin
     nResult := 1;
     nMaxCapArea := 0;

     while True do
     begin
          try
             Application.ProcessMessages();
             if Not gWorking then
             begin
                  nResult := 0;
                  break;
             end;
             Application.ProcessMessages();
             if ( FDrivers ) then
                nRet := SFE.Capture();

             if nRet < 0 then
             begin
                  nResult := 0;
                  break;
             end;
             Application.ProcessMessages();
             if ( FDrivers ) then
                nCapArea := SFE.IsFinger();

             if nCapArea >= 0 then
             begin
                  if nCapArea < nMaxCapArea + 2 then break;
                  if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                  if nCapArea > 45 then break;
             end;
          except
                Result := 0;
                Exit;
          end;
     end;
     Application.ProcessMessages();
     if nResult = 1 then
     begin
          if ( FDrivers ) then
             nRet := SFE.GetImage(addrOfImageBytes);

          if nRet < 0 then
          begin
               nResult := 0;
          end
     end;
     CaptureFinger := nResult;
     Application.ProcessMessages();
end;

// BIOMETRICO
procedure TConfirmaComida.InicializaBiometrico;
begin
     //Inicializa Biometrico
     enableButtons(True);
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
end;

// BIOMETRICO
procedure TConfirmaComida.ConfirmaComida(iUserId: Integer);
begin
     if ( IntToStr(iUserId) = eCredencial.Text ) then
          OK_DevEx.Click
     else
          Cancelar_DevEx.Click;
end;

// BIOMETRICO
procedure TConfirmaComida.OKClick(Sender: TObject);
begin
     gWorking := False;
     inherited;
end;

// BIOMETRICO
procedure TConfirmaComida.CancelarClick(Sender: TObject);
begin
     gWorking := False;
     inherited;
end;

end.
