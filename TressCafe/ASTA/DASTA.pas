unit DASTA;

{------------------------------------------------------------------------------------------------
 Unidad para importar los datos de formato propietario ASTA a formato XML de Midas
 Este DLL se incluye como un recurso dentro del ejecutable de Tress Cafeter�a.

 Instrucciones para integrarlo al exe de Tress Cafeter�a:
 1. Compilar y generar el DLL con sus funciones exportadas correspondientes
 2. Compilar el archivo AstaDLL.rc (existe un archivo AstaDLL.bat que tiene las instrucciones)
 3. Una vez generado el archivo de recurso AstaDLL.RES, compilar Tress Cafeter�a y ejecutar de
    manera normal, todo es transparente para el usuario.
 ------------------------------------------------------------------------------------------------}

interface

uses
  SysUtils, Classes, DB, AstaDrv2, DBClient, AstaClientDataset;

type
  TdmASTA = class(TDataModule)
    adCalendario: TAstaDataSet;
    adCalendarioHORA: TStringField;
    adCalendarioACCION: TSmallintField;
    adCalendarioCONTADOR: TStringField;
    adCalendarioSEMANA: TSmallintField;
    adCalendarioSYNC: TStringField;
    cdsADO: TClientDataSet;
    adIdInvita: TAstaDataSet;
    adContador: TAstaDataSet;
    adContadorHORA: TStringField;
    adHuellas: TAstaDataSet;
  private
    { Private declarations }
    function EsBinarioASTA(sArchivo: AnsiString): Boolean;
    function ImportarBinario(sArchivo: AnsiString; dsOrigen: TAstaDataSet; Ordenar: string = ''): Boolean;
  public
    { Public declarations }
    function ImportarCalendario(sArchivo: AnsiString): Boolean;
    function ImportarInvitadores(sArchivo: AnsiString): Boolean;
    function ImportarHuellas(sArchivo: AnsiString): Boolean;
    function ImportarContador(sArchivo: AnsiString): Boolean;
  end;

var
  dmASTA: TdmASTA;

implementation

uses
  Dialogs, CafeteraUtils;

{$R *.dfm}

{ TdmASTA }

function TdmASTA.EsBinarioASTA(sArchivo: AnsiString): Boolean;
var
  Stream: TStream;
  Firma: Integer;
begin
  Result := False;
  Firma  := 0;

  Stream := TMemoryStream.Create;
  try
    TMemoryStream(Stream).LoadFromFile(sArchivo);
    Stream.ReadBuffer(Firma, SizeOf(Integer));
    Result := Firma = Asta3Signature
  except
    on e: Exception do
      MessageDlg('[ASTA.DLL] Error al abrir datos: ' + ExceptionAsString(e), mtError, [mbOK], 0)
  end;
  FreeAndNil(Stream);
end;

function TdmASTA.ImportarCalendario(sArchivo: AnsiString): Boolean;
begin
  Result := False;
  if EsBinarioASTA(sArchivo) then
    Result := ImportarBinario(sArchivo, adCalendario, 'HORA');
end;

function TdmASTA.ImportarInvitadores(sArchivo: AnsiString): Boolean;
begin
  Result := False;
  if EsBinarioASTA(sArchivo) then
    Result := ImportarBinario(sArchivo, adIdInvita);
end;

function TdmASTA.ImportarHuellas(sArchivo: AnsiString): Boolean;
begin
  Result := False;
  if EsBinarioASTA(sArchivo) then
    Result := ImportarBinario(sArchivo, adHuellas, 'HORA');
end;

function TdmASTA.ImportarContador(sArchivo: AnsiString): Boolean;
begin
  Result := False;
  if EsBinarioASTA(sArchivo) then
    Result := ImportarBinario(sArchivo, adContador, 'HORA');
end;

function TdmASTA.ImportarBinario(sArchivo: AnsiString; dsOrigen: TAstaDataSet; Ordenar: string): Boolean;
var
  Bak: string;

begin
  Result := False;
  try
    dsOrigen.LoadFromFileWithFields(sArchivo);
    dsOrigen.Open;
    if Ordenar <> '' then
      dsOrigen.SortDataSetByFieldName(Ordenar, TRUE); 
    CloneDataSet(dsOrigen, cdsADO);

    // Renombrar el archivo por si acaso
    Bak := ChangeFileExt(sArchivo, '.bak');
    DeleteFile(Bak);
    RenameFile(sArchivo, Bak);

    // Guardar con el nuevo formato XML de Midas
    cdsADO.SaveToFile(sArchivo, dfXMLUTF8);
    Result := True;
  except
    on e: Exception do
      MessageDlg(Format('[ASTA.DLL] Error al importar datos de %s: %s', [sArchivo, ExceptionAsString(e)]), mtError, [mbOK], 0)
  end;
end;

end.
