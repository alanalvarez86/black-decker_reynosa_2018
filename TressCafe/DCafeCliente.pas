unit DCafeCliente;

interface

uses
  BTMemoryModule, DBaseCafeCliente, CafeteraWSDL, ZetaCommonClasses, ZetaCommonLists, VaComm, VaPrst, ZetaWinAPITools, Dialogs,
  Forms, InvokeRegistry, DB, ExtCtrls, DBClient, Classes, Rio, SOAPHTTPClient, Types,
  ZetaClientDataSet, SysUtils;

{$DEFINE MULT_TIPO_COMIDA}

type

  TFuncionAsta = function (Archivo: PAnsiChar): Boolean; stdcall;

  TInfoPuerto = record
    PortNum: Integer;
    BaudRate: TVaBaudrate;
    DataBits: TVaDataBits;
    Parity: TVaParity;
    StopBits: TVaStopBits;
    DTRControl: TVaControlDTR;
    FlowControl: Integer; // TVaFlowControl;
    RTSControl: TVaControlRTS;
    EventChar: AnsiChar;
  end;

  PInfoPuerto = ^TInfoPuerto;

  TAsciiLecturas = class(TObject)
  private
    { Private declarations }
    FBuffer  : TextFile;
    FFileName: String;
    FUsed    : Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    property Used: Boolean read FUsed;
    procedure Init(const sFileName: String);
    procedure WriteTexto(const sTexto: String);
    procedure Close;
  end;

  TdmCafeCliente = class(TdmBaseCafeCliente)
    dsEmpleado: TDataSource;
    dsComidas: TDataSource;
    adCalendario: TClientDataSet;
    dsCalendario: TDataSource;
    adCalendarioAnterior: TClientDataSet;
    adCalendarioHORA: TStringField;
    adCalendarioACCION: TSmallintField;
    adCalendarioCONTADOR: TStringField;
    adCalendarioSEMANA: TSmallintField;
    adCalendarioTmp: TClientDataSet;
    adCalendarioSYNC: TStringField;
    cdsListado: TClientDataSet;
    dsDetalle: TDataSource;
    dsTotales: TDataSource;
    cdsIdInvita: TClientDataSet;
    adIdInvita: TClientDataSet;
    dsFotoEmp: TDataSource;
    StringField1: TStringField;
    SmallintField1: TSmallintField;
    StringField2: TStringField;
    SmallintField2: TSmallintField;
    StringField3: TStringField;
    adContador: TClientDataSet;
    dsContador: TDataSource;
    adContadorHORA: TStringField;
    cdsHuellas: TClientDataSet; // BIOMETRICO
    dsHuellas: TDataSource;
    cdsReiniciaHuellas: TClientDataSet;
    cdsReportaHuellas: TClientDataSet;
    dsReiniciaHuellas: TDataSource;
    dsReportaHuellas: TDataSource;
    adCalendarioTmpSYNC: TStringField; // BIOMETRICO
    dsConfiguracion: TDataSource;
    cdsConfiguracion: TClientDataSet;
    dsCalend: TDataSource;
    cdsActualizarConfig: TClientDataSet;
    cdsImportarConfig: TClientDataSet;
    procedure qryComidasCF_FECHAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure qryComidasCF_HORAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure adCalendarioHORASetText(Sender: TField; const Text: String);
    procedure adCalendarioHORAValidate(Sender: TField);
    procedure adCalendarioHORAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure adCalendarioACCIONGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure adCalendarioCONTADORSetText(Sender: TField; const Text: String); { OP: 22.Mayo.08 }
    procedure adCalendarioCONTADORGetText(Sender: TField; var Text: String; DisplayText: Boolean); { OP: 22.Mayo.08 }
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure CodedStream(SOAPMsg: TSOAPDataSet);
    procedure CodedMessage(SOAPMsg: TSOAPDataSet);
    procedure adCalendarioSEMANAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure adCalendarioNewRecord(DataSet: TDataSet);
    procedure adCalendarioSYNCGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure adCalendarioSYNCSetText(Sender: TField; const Text: String);
    procedure ConnTimerTimer(Sender: TObject);
  private
    { Private declarations }
    FLastGafete: String;
    FTipoComida, FLastTipo, FLastCantidad: Integer;
    FMostrarComidas, FMostrarSigComida, FUsaTeclado, FPreguntaTipo, FPreguntaQty, FMostrarFoto: Boolean;
    FProhibirAcceso, FValidarEmpleado, FCancelaOperacion: Boolean;
    FLastHora, FHoraSincroniza, FHoraSincronizaLastTime: TDateTime;
    FTipoGafete      : eTipoGafete;
    FSemanaCalendario: eDiasCafeteria; { OP: 23.Mayo.08 }
    FPrimeraVez      : Boolean;
    FSync            : Boolean;

    DLLCode            : Pointer;
    DLLCodeSize        : Integer;
    mp_MemoryModule    : PBTMemoryModule;
    ImportarCalendario : TFuncionAsta;
    ImportarInvitadores: TFuncionAsta;
    ImportarHuellas    : TFuncionAsta;
    ImportarContador   : TFuncionAsta;

    function ChecadaToStr(const sGafete, sTipoComida: String; const iComidas: Integer; const dValue: TDateTime): String;
    function GetFechaHora(const lCancelacion: Boolean): TDateTime;
    function GetFileIdInvita: String;
    function GetFileHuellas: String; // BIOMETRICO
    function ExisteIdInvitador(const sGafete: String): Boolean;
    procedure SetTipoComida(const Value: Integer);
    procedure SetProhibirAcceso(const Value: Boolean);

    procedure EjecutaAccionCalendario(const TipoAccion: eCafeCalendario; const lContador: Boolean); { OP: 22.Mayo.08 }
    procedure EjecutaAccionSincronizacion;
    procedure GetFotoEmpleado;
    function ObtenDiaSemana: eDiasCafeteria; { OP: 23.Mayo.08 }
    function ValidaDiaSemana(DiaCal, DiaAct: eDiasCafeteria): Boolean; { OP: 23.Mayo.08 }
    function DiaAnterior(const Dia: eDiasCafeteria): eDiasCafeteria; { OP: 23.Mayo.08 }
    procedure SetSemanaCalendario(const Dia: eDiasCafeteria); { OP: 23.Mayo.08 }
    function GetArchivoHuellas: String; // BIOMETRICO
    procedure LiberaDS(DS: TDataSet);
    procedure AsignaDS(oDS: TDataSource; XML: String);
    function StringToPAnsiChar(stringVar: string): PAnsiChar;
  public
    { Public declarations }
    // property MostrarFoto : Boolean write SetMostrarFoto;
    property MostrarFoto      : Boolean read FMostrarFoto write FMostrarFoto;
    property MostrarComidas   : Boolean read FMostrarComidas write FMostrarComidas;
    property MostrarSigComida   : Boolean read FMostrarSigComida write FMostrarSigComida;
    property TipoComida       : Integer read FTipoComida write SetTipoComida;
    property ProhibirAcceso   : Boolean read FProhibirAcceso write SetProhibirAcceso;
    property ValidarEmpleado  : Boolean read FValidarEmpleado write FValidarEmpleado;
    property LastGafete       : String read FLastGafete write FLastGafete;
    property LastHora         : TDateTime read FLastHora write FLastHora;
    property LastTipo         : Integer read FLastTipo write FLastTipo;
    property LastCantidad     : Integer read FLastCantidad write FLastCantidad;
    property CancelaOperacion : Boolean read FCancelaOperacion write FCancelaOperacion;
    property UsaTeclado       : Boolean read FUsaTeclado write FUsaTeclado;
    property PreguntaTipo     : Boolean read FPreguntaTipo write FPreguntaTipo;
    property PreguntaQty      : Boolean read FPreguntaQty write FPreguntaQty;
    property TipoGafete       : eTipoGafete read FTipoGafete write FTipoGafete;
    property SemanaCalendario : eDiasCafeteria read FSemanaCalendario write SetSemanaCalendario; { OP: 23.Mayo }
    property HoraSincroniza   : TDateTime read FHoraSincroniza write FHoraSincroniza;
    property HoraSincronizaLastTime : TDateTime read FHoraSincronizaLastTime write FHoraSincronizaLastTime;

    procedure RevisaModulo;
    procedure RevisaInvitadores;

    function ExisteEmpleado(var sGafete: String; const sTipoComida: String; const iComidas: Integer): Boolean;
    function SincronizarCafeteria(var sEstacion: String): Boolean; //US13921
    procedure ActualizarCafeteria(var sEstacion: String); //US13921
    function BuscaCafeteria(var sEstacion: String): Boolean; //US14639
    function VerificarSincronizacion: Boolean;
    function HayComidas: Boolean;
    function ValidacionOK(var sLetrero: String): Boolean;
    function GetConfiguracion(const lEntrada, lIguales: Boolean): String;
    function GetInfoPuerto(const iPuerto: Integer; const sConfiguracion: String): TInfoPuerto;
    function SetInfoPuerto(const InfoPuerto: TInfoPuerto): String;
    function GetFolderLecturas(const sArchivo: String): String;
    function EsGafeteInvitador(const sGafete: String): Boolean;
    function CompletaGafete(const sGafete: String): String;
    function DesCompletaGafete(const sGafete: String): String;
    {$IFDEF BOSE}
    procedure ScanContador(const sHora: String);
    procedure InicializaContador(const sArchivo: String);
    {$ENDIF}
    procedure SetSocket(const sDireccion: String; const iPuerto: Integer);
    procedure ScanCalendario(const sHora: String);
    function GetHoraSincroniza : String;
    procedure AgregaChecadaFueraLinea(const sGafete, sTipoComida: String; const iComidas: Integer);
    procedure AgregaChecadaContador(const iContador: Integer; const sMensaje: String);

    procedure ImprimeListado(const Reloj, TipoComidas: String; const FechaInicial, FechaFinal: TDateTime;
      const SoloTotales: Boolean);
    procedure SetCalendarioInicial(const sHora: String);
    procedure Init; override;
    procedure InicializaCalendario(const sArchivo: String);
    procedure SiguienteComida;
    procedure ImportaChecadas(const sArchivo: String);
    procedure QuitaFiltro; { OP: 23.Mayo.08 }
    procedure RevisaLecturas;
    procedure SincronizaHuellas; // BIOMETRICO
    property PrimeraVez: Boolean read FPrimeraVez write FPrimeraVez; // BIOMETRICO
    property Sync: Boolean read FSync write FSync; // BIOMETRICO
    procedure ReiniciaHuellas;                     // BIOMETRICO
    procedure ReportaHuella(sLista: String);       // BIOMETRICO
    property ArchivoHuellas: String read GetArchivoHuellas; // BIOMETRICO
    function ImportarConfiguracion( Parametros: TZetaParams ): Boolean;
  end;

var
  dmCafeCliente: TdmCafeCliente;
  FAsciiFile   : TAsciiLecturas;

implementation

uses
  FCafeteria, FConfigura, FBitacoraErrores, ZetaSystemWorking, ZetaCommonTools, ZetaClientTools, ZetaServerTools,
  ZetaRegistryCafe, ZetaDialogo, ZetaMessages, CafeteraConsts, CafeteraUtils, MaskUtils, Variants, Windows, DateUtils;

{$R *.DFM}
{$R AstaDLL.RES}

const
  K_FILE_ID_INVITADORES = 'INVITA.DAT';
  K_FILE_HUELLAS        = 'HUELLAS.DAT'; // BIOMETRICO


  { *********** TAsciiLecturas ********* }

procedure TAsciiLecturas.Init(const sFileName: String);
begin
  try
    AssignFile(FBuffer, sFileName);
    if FileExists(sFileName) then
      Append(FBuffer)
    else
      ReWrite(FBuffer);
    FUsed := True;
  except
    on Error: Exception do begin
      FUsed := False;
      CloseFile(FBuffer);
      raise;
    end;
  end;
  FFileName := sFileName;
end;

procedure TAsciiLecturas.WriteTexto(const sTexto: String);
begin
  Writeln(FBuffer, sTexto);
end;

procedure TAsciiLecturas.Close;
begin
  if Used then begin
    CloseFile(FBuffer);
    FUsed := False;
  end;
end;

{ TdmCafeCliente }

procedure TdmCafeCliente.DataModuleCreate(Sender: TObject);
var
  rs: TResourceStream;
begin
  inherited;
  FProhibirAcceso   := False;
  FValidarEmpleado  := True;
  FCancelaOperacion := False;
  FSync             := False;
  FAsciiFile        := nil;

  ImportarCalendario  := nil;
  ImportarInvitadores := nil;
  ImportarHuellas     := nil;
  ImportarContador    := nil;

  // Cargar DLL desde recurso a memoria
  rs := TResourceStream.Create(HInstance, 'AstaDLLRes', RT_RCDATA);
  try
    rs.Position := 0;
    DLLCodeSize := rs.Size;
    DLLCode := GetMemory(DLLCodeSize);
    rs.Read(DLLCode^, DLLCodeSize);

    try
      mp_MemoryModule := BTMemoryLoadLibary(DLLCode, DLLCodeSize);
      if Assigned(mp_MemoryModule) then begin
        @ImportarCalendario  := BTMemoryGetProcAddress(mp_MemoryModule, 'ImportarCalendario');
        @ImportarInvitadores := BTMemoryGetProcAddress(mp_MemoryModule, 'ImportarInvitadores');
        @ImportarHuellas     := BTMemoryGetProcAddress(mp_MemoryModule, 'ImportarHuellas');
        @ImportarContador    := BTMemoryGetProcAddress(mp_MemoryModule, 'ImportarContador');
      end;
    except
      on e: Exception do begin
        Showmessage('Error cargando ASTA.DLL: ' + BTMemoryGetLastError);
      end;
    end;

  except
    on e: Exception do begin
      Showmessage('Error cargando ASTA.DLL: ' + ExceptionAsString(e));
    end;
  end;
  rs.Free;
end;

procedure TdmCafeCliente.DataModuleDestroy(Sender: TObject);
begin
  LiberaDS(dsComidas.DataSet);
  LiberaDS(dsDetalle.DataSet);
  LiberaDS(dsTotales.DataSet);
  LiberaDS(dsFotoEmp.DataSet);
  LiberaDs(dsConfiguracion.DataSet);
  LiberaDS(dsCalend.DataSet);

  FreeAndNil(FAsciiFile);

  // Liberar maquinaria del DLL
  if Assigned(mp_MemoryModule) then begin
    BTMemoryFreeLibrary(mp_MemoryModule);
    mp_MemoryModule := nil;
  end;

  if Assigned(DLLCode) then begin
    FreeMemory(DLLCode);
    DLLCode := nil;
  end;

  inherited;
end;

procedure TdmCafeCliente.SetTipoComida(const Value: Integer);
begin
  if (FTipoComida <> Value) then
  begin
    FTipoComida                     := Value;
    FormaCafe.lblTipoComida.Caption := CafeRegistry.TextoTipoComida(FTipoComida);
    SiguienteComida;
  end;
end;

procedure TdmCafeCliente.SetProhibirAcceso(const Value: Boolean);
begin
  if (FProhibirAcceso <> Value) then
  begin
    FProhibirAcceso := Value;
    FormaCafe.ShowProhibicion(FProhibirAcceso);
  end;
end;

{ Conexion }

procedure TdmCafeCliente.SetSocket(const sDireccion: String; const iPuerto: Integer);
begin
  ZetaSystemWorking.InitAnimation('Conectando Cafeter�a..');
  Direccion := sDireccion;
  Puerto    := iPuerto;
  Interval  := CafeRegistry.Intervalo;
  Active    := True;
  ZetaSystemWorking.EndAnimation;
end;

{ adCalendario }

procedure TdmCafeCliente.adCalendarioHORAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if DisplayText then begin
    if Sender.DataSet.IsEmpty then
      Text := ''
    else
      Text := FormatMaskText('99:99;0', Sender.AsString);
  end else
    Text := Sender.AsString;
end;

procedure TdmCafeCliente.adCalendarioHORASetText(Sender: TField; const Text: String);
begin
  Sender.AsString := ConvierteHora(Text);
end;

procedure TdmCafeCliente.adCalendarioHORAValidate(Sender: TField);
var
  sHora: String;
begin
  sHora := Sender.AsString;
  if not ZetaCommonTools.ValidaHora(sHora, '48') then
    raise ERangeError.Create('Hora "' + Copy(sHora, 1, 2) + ':' + Copy(sHora, 3, 2) + '" Inv�lida');
end;

{ OP: 22.Mayo.08 }
procedure TdmCafeCliente.adCalendarioCONTADORGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if DisplayText then begin
    if Sender.DataSet.IsEmpty then
      Text := ''
    else
      Text := BoolAsSiNo(zStrToBool(Sender.AsString));
  end else
    Text := Sender.AsString;
end;

{ OP: 22.Mayo.08 }
procedure TdmCafeCliente.adCalendarioCONTADORSetText(Sender: TField; const Text: String);
begin
  Sender.AsString := zBoolToStr(zStrToBool(UpperCase(Text)));
end;

procedure TdmCafeCliente.adCalendarioACCIONGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if DisplayText then
  begin
    if Sender.DataSet.IsEmpty then
      Text := ''
    else
    begin
      if Sender.AsInteger <= 8 then
        Text := ZetaRegistryCafe.CafeRegistry.TextoTipoComida(Sender.AsInteger + 1) { OP:21.Mayo.08 }
      else
      begin
        Text := ObtieneElemento(lfCafeCalendario, Sender.AsInteger)
      end;
    end;
  end
  else
    Text := Sender.AsString;
end;

{ qryComidas }

procedure TdmCafeCliente.qryComidasCF_FECHAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.AsDateTime = 0 then
    Text := ''
  else
    Text := FormatDateTime('dddd d/mmm', Sender.AsDateTime);
end;

procedure TdmCafeCliente.qryComidasCF_HORAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if StrVacio(Sender.AsString) then
    Text := ''
  else
    Text := FormatMaskText('99:99;0', Sender.AsString);
end;

{ Metodos }

function TdmCafeCliente.EsGafeteInvitador(const sGafete: String): Boolean;
begin
  case TipoGafete of
    egTress:
      Result := Copy(sGafete, 2, 2) = K_PREFIJO_INVITADOR;
    else
      Result := ExisteIdInvitador(sGafete);
  end;
end;

function TdmCafeCliente.GetFileIdInvita: String;
begin
  Result := ZetaMessages.SetFileNameDefaultPath(K_FILE_ID_INVITADORES);
end;

// BIOMETRICO
function TdmCafeCliente.GetFileHuellas: String;
begin
  Result := ZetaMessages.SetFileNameDefaultPath(K_FILE_HUELLAS);
end;

function TdmCafeCliente.ExisteIdInvitador(const sGafete: String): Boolean;
var
  sArchivo: String;
begin
  Result := False;

  try
        with adIdInvita do begin
          if (not Active) then // Si no est� activo intenta leerlo del disco
          begin
            sArchivo := GetFileIdInvita;
            if FileExists(sArchivo) then begin
              try
                Active := False;
                if Assigned(ImportarInvitadores) then
                begin
                  ImportarInvitadores(StringToPAnsiChar(sArchivo));
                end;

                LoadFromFile(sArchivo);
                Active := True;
              except
                on Error: Exception do begin
                  FormaCafe.WarningMessage(Format('No se Puede Leer Archivo de Invitadores %s: %s', [sArchivo, Error.Message]));
                end;
              end;
            end;
          end;
          if Active then
          begin
            // BIOMETRICO
            if (FTipoGafete = egBiometrico) then
              Result := Locate('ID_NUMERO;IV_TIPO', VarArrayOf([sGafete, 2]), [])
            else
              Result := Locate('ID_NUMERO;IV_TIPO', VarArrayOf([sGafete, 1]), []);
          end;
        end;
  except
    on Error: Exception do begin
      Result := FALSE;
    end;
  end;
end;

function TdmCafeCliente.GetFechaHora(const lCancelacion: Boolean): TDateTime;
begin
  if lCancelacion then
    Result := FLastHora + (1 / K_24HORAS)
  else
    Result := Now;
end;

function TdmCafeCliente.ExisteEmpleado(var sGafete: String; const sTipoComida: String; const iComidas: Integer): Boolean;
var
  dValue: TDateTime;
begin
  dValue := GetFechaHora((iComidas < 0));
  AsciiServer.Write(ChecadaToStr(sGafete, sTipoComida, iComidas, dValue));
  with cdsEmpleado do begin
    with Params do begin
      ParamByName('Gafete').AsString      := sGafete;
      ParamByName('FechaHora').AsDateTime := dValue;
      ParamByName('NumComidas').AsInteger := iComidas;
      ParamByName('TipoComida').AsString  := sTipoComida;
      ParamByName('Letrero').AsString     := Estacion;
      ParamByName('Comidas').Value        := zBoolToStr(FMostrarComidas);
      ParamByName('TipoGafete').AsInteger := Ord(FTipoGafete);
      ParamByName('Foto').Value           := zBoolToStr(FMostrarFoto);
    end;

    // RefireSQL; // Extinto por obsoleto
    SOAPMsg := CreateSOAPMsg(K_AUTORIZAR_CHECADA_COMIDA, ParamsToXML(Params));
    if not SendSOAPMsg(SOAPMsg, Params, cdsEmpleado) then begin
      cdsEmpleado.Open;
      cdsEmpleado.EmptyDataSet;
    end;
    FreeAndNil(SOAPMsg);

    Result := not IsEmpty;
    if (Result) then
    begin
         FLastGafete   := sGafete;
         FLastHora     := dValue;
         FLastTipo     := StrToInt(sTipoComida);
         FLastCantidad := iComidas;
         GetFotoEmpleado;
    end;
    sGafete := Params.ParamByName('Gafete').AsString;
  end;
end;

//US13921
procedure TdmCafeCliente.ActualizarCafeteria(var sEstacion: String);
begin
     try
        with cdsActualizarConfig do
        begin
             with Params do begin
                  ParamByName('CF_NOMBRE').AsString := sEstacion;
                  ParamByName('DI_TIPO').AsInteger := Ord( dpCafeteria );
             end;

             SOAPMsg := CreateSOAPMsg(K_ACTUALIZA_CONFIG, ParamsToXML(Params));
             if not SendSOAPMsg(SOAPMsg, Params, cdsActualizarConfig) then begin
                cdsActualizarConfig.Open;
                cdsActualizarConfig.EmptyDataSet;
             end;
             //Result := Params.ParamByName('STATUS').AsBoolean;
        end;
     finally
            FreeAndNil(SOAPMsg);
     end;
end;

//US14639
function TdmCafeCliente.BuscaCafeteria(var sEstacion: string): Boolean;
begin
     Result := False;
     if dmCafeCliente.Active then
     begin
          try
             with cdsActualizarConfig do
             begin
                  with Params do
                  begin
                       ParamByName('CF_NOMBRE').AsString := sEstacion;
                       ParamByName('DI_TIPO').AsInteger := Ord( dpCafeteria );
                  end;

                  SOAPMsg := CreateSOAPMsg(K_VERIFICAR_CONFIG, ParamsToXML(Params));
                  if SendSOAPMsg(SOAPMsg, Params, cdsActualizarConfig) then
                     Result := Params.ParamByName('STATUS').AsBoolean;
             end;
          finally
                 FreeAndNil(SOAPMsg);
          end;
     end;
end;

//14854
function TdmCafeCliente.VerificarSincronizacion: Boolean;
var
   sDetalle : AnsiString;
   aDetalle, aTotales : TByteDynArray;

function Converter(P: TByteDynArray): string;
var
   Buffer: AnsiString;
begin
     SetLength(Buffer, Length(P) );
     System.Move(P[0], Buffer[1], Length(P));
     Result := string (  Buffer ) ;
end;
begin
     Result := False;
     if dmCafeCliente.Active then
     begin
          with cdsConfiguracion do
          begin
               with Params do
               begin
                    ParamByName('CF_NOMBRE').AsString := CafeRegistry.Identificacion;
                    ParamByName('DI_TIPO').AsInteger := Ord( dpCafeteria );
               end;

               try
                  // Enviar peticion al servidor SOAP
                  SOAPMsg := CreateSOAPMsg(K_VERIFICAR_SINC, ParamsToXML(Params));
                  SendSOAPMsgXML_DetalleTotales(aDetalle, aTotales, SOAPMsg, Params, cdsImportarConfig);

                  {$ifdef ANTES}
                  sDetalle := Decrypt ( Converter( aDetalle) ) ;
                  {$else}
                  sDetalle := Converter( aDetalle );
                  {$endif}

                  Result := zStrToBool (sDetalle);
               finally
                      FreeAndNil(SOAPMsg);
               end;
          end;
     end;
end;

//US13921
function TdmCafeCliente.SincronizarCafeteria(var sEstacion: string): Boolean;
var
   sDetalle, sTotales : AnsiString;
   aDetalle, aTotales : TByteDynArray;

function Converter(P: TByteDynArray): string;
var
   Buffer: AnsiString;
begin
     SetLength(Buffer, Length(P) );
     System.Move(P[0], Buffer[1], Length(P));
     Result := string (  Buffer ) ;
end;

begin
     Result := False;
     if dmCafeCliente.Active then
     begin
          with cdsConfiguracion do
          begin
               with Params do
               begin
                    ParamByName('CF_NOMBRE').AsString := sEstacion;
                    ParamByName('DI_TIPO').AsInteger := Ord( dpCafeteria );
               end;

               // Enviar peticion al servidor SOAP
               SOAPMsg := CreateSOAPMsg(K_SINCRONIZA_CONFIG, ParamsToXML(Params));
               SendSOAPMsgXML_DetalleTotales(aDetalle, aTotales, SOAPMsg, Params, cdsConfiguracion);

               {$ifdef ANTES}
               sDetalle := Decrypt ( Converter( aDetalle) ) ;
               sTotales := Decrypt ( Converter( aTotales) ) ;
               {$else}
               sDetalle := Utf8ToAnsi(Converter( aDetalle ));
               sTotales := Converter( aTotales );
               {$endif}

               AsignaDS(dsConfiguracion, sDetalle);
               AsignaDS(dsCalend, sTotales);

               FreeAndNil(SOAPMsg);

               with dsConfiguracion.DataSet do
               begin
                    Result := not IsEmpty;
                    if (Result) then
                    begin
                         with ZetaRegistryCafe.CafeRegistry do
                         begin
                              if CanWrite then
                                 ConfiguracionEnviada := True;
                              TipoComida1 := FieldByName('CF_TCOM_1').AsString;
                              TipoComida2 := FieldByName('CF_TCOM_2').AsString;
                              TipoComida3 := FieldByName('CF_TCOM_3').AsString;
                              TipoComida4 := FieldByName('CF_TCOM_4').AsString;
                              TipoComida5 := FieldByName('CF_TCOM_5').AsString;
                              TipoComida6 := FieldByName('CF_TCOM_6').AsString;
                              TipoComida7 := FieldByName('CF_TCOM_7').AsString;
                              TipoComida8 := FieldByName('CF_TCOM_8').AsString;
                              TipoComida9 := FieldByName('CF_TCOM_9').AsString;
                              TipoComida := FieldByName('CF_TIP_COM').AsInteger;
                              Velocidad := FieldByName('CF_VELOCI').AsInteger;
                              TiempoEspera := FieldByName('CF_T_ESP').AsInteger;
                              ChecadaSimultanea := eChecadaSimultanea(FieldByName('CF_CHECK_S').AsInteger);
                              DefaultEmpresa := FieldByName('CF_DEF_COM').AsString;
                              DefaultCredencial := FieldByName('CF_DEF_CRE').AsString;
                              Banner := FieldByName('CF_BANNER').AsString;
                              GafeteAdmon := FieldByName('CF_GAFETE').AsString;
                              ClaveAdmon := FieldByName('CF_CLAVE').AsString;
                              MostrarFoto := zStrToBool(FieldByName('CF_FOTO').AsString);
                              MostrarComidas := zStrToBool(FieldByName('CF_COM').AsString);
                              UsaTeclado := zStrToBool(FieldByName('CF_TECLADO').AsString);
                              MostrarSigComida := zStrToBool(FieldByName('CF_SIG_COM').AsString);
                              PreguntaTipo := zStrToBool(FieldByName('CF_PRE_TIP').AsString);
                              PreguntaQty := zStrToBool(FieldByName('CF_PRE_QTY').AsString);
                              ConcesionImprimir := zStrToBool(FieldByName('CF_A_PRINT').AsString);
                              ConcesionCancelar := zStrToBool(FieldByName('CF_A_CANC').AsString);
                              ReiniciaMediaNoche  := zStrToBool(FieldByName('CF_REINICI').AsString);
                         end;

                         adCalendario.Open;
                         adCalendario.Edit; //Calendario Edit
                         adCalendario.EmptyDataSet; //Limpia Calendario
                         if Not dsCalend.DataSet.IsEmpty then
                         begin
                              while not dsCalend.DataSet.EOF do
                              begin
                                   adCalendario.Append;
                                   adCalendario.FieldByName('HORA').AsString := dsCalend.DataSet.FieldByName('HORA').AsString;
                                   adCalendario.FieldByName('ACCION').AsInteger := dsCalend.DataSet.FieldByName('ACCION').AsInteger;
                                   adCalendario.FieldByName('CONTADOR').AsString := dsCalend.DataSet.FieldByName('CONTADOR').AsString;
                                   adCalendario.FieldByName('SEMANA').AsInteger := dsCalend.DataSet.FieldByName('SEMANA').AsInteger;
                                   adCalendario.FieldByName('SYNC').AsString := dsCalend.DataSet.FieldByName('SYNC').AsString;
                                   dsCalend.DataSet.Next;
                              end;
                         end;

                        if FieldByName('CF_CAMBIOS').AsString = K_GLOBAL_SI then
                            ActualizarCafeteria(sEstacion);
                    end;
               end;
          end;
     end
end;


function TdmCafeCliente.ImportarConfiguracion(Parametros: TZetaParams ): Boolean;
var
   sDetalle : AnsiString;
   aDetalle, aTotales : TByteDynArray;

function Converter(P: TByteDynArray): string;
var
   Buffer: AnsiString;
begin
     SetLength(Buffer, Length(P) );
     System.Move(P[0], Buffer[1], Length(P));
     Result := string (  Buffer ) ;
end;
begin
     if dmCafeCliente.Active then
     begin
          with cdsImportarConfig do
          begin
               with Params do
               begin
                    ParamByName('CF_NOMBRE').AsString := Parametros.ParamByName('Estacion').AsString;
                    ParamByName('DI_TIPO').AsInteger := Ord( dpCafeteria );
                    ParamByName('Detalle').AsString :=  ParamsToXML(Parametros);
                    ParamByName('Totales').AsString := DataSetToXML( adCalendario );
               end;

               try
                  // Enviar peticion al servidor SOAP
                  SOAPMsg := CreateSOAPMsg(K_IMPORTAR_CONFIG, ParamsToXML(Params));
                  SendSOAPMsgXML_DetalleTotales(aDetalle, aTotales, SOAPMsg, Params, cdsImportarConfig);

                  {$ifdef ANTES}
                  sDetalle := Decrypt ( Converter( aDetalle) ) ;
                  {$else}
                  sDetalle := Converter( aDetalle );
                  {$endif}

                  CafeRegistry.ConfiguracionEnviada := zStrToBool (sDetalle);
                  CafeRegistry.Identificacion := Parametros.ParamByName('Estacion').AsString;
               finally
                      FreeAndNil(SOAPMsg);
               end;

               Result := CafeRegistry.ConfiguracionEnviada;
          end;
     end;
end;

procedure TdmCafeCliente.GetFotoEmpleado;
begin
  with cdsEmpleado.Params do
    if Assigned(FindParam('Foto')) then
      AsignaDS(dsFotoEmp, ParamByName('Foto').AsString);
end;

function TdmCafeCliente.HayComidas: Boolean;
var
  sComidas: String;
begin
  Result := FMostrarComidas;
  if (Result) then
  begin
    sComidas := cdsEmpleado.Params.ParamByName('Comidas').AsString;
    Result   := (sComidas > '');
    if (Result) then
    begin
      try
        AsignaDS(dsComidas, sComidas);
      except
        Result := False;
        Exit;
      end;
      dsComidas.DataSet.FieldByName('CF_FECHA').OnGetText := qryComidasCF_FECHAGetText;
      dsComidas.DataSet.FieldByName('CF_HORA').OnGetText  := qryComidasCF_HORAGetText;
      Result                                              := not dsComidas.DataSet.IsEmpty;
    end;
  end;
end;

function TdmCafeCliente.ValidacionOK(var sLetrero: String): Boolean;
begin
  if FCancelaOperacion then begin
    Result := False;
    {$IFDEF MULT_TIPO_COMIDA}
    sLetrero := StrDef(sLetrero, 'Operaci�n Cancelada');
    {$ELSE}
    sLetrero := 'Operaci�n Cancelada';
    {$ENDIF}
  end else begin
    if dmCafeCliente.Active then begin
      with cdsEmpleado.Params do begin
        Result   := ParamByName('Status').AsInteger = 0;
        sLetrero := ParamByName('Letrero').AsString;
        if ( sLetrero = Estacion )  then
        begin
          sLetrero := 'No fue posible procesar el consumo en el servidor';
          Result := FALSE;
        end;

      end;
    end else begin
      Result   := True;
      sLetrero := CafeRegistry.MensajeOk;
    end;
  end;
  FCancelaOperacion := False;
end;

function TdmCafeCliente.ChecadaToStr(const sGafete, sTipoComida: String; const iComidas: Integer; const dValue: TDateTime): String;

  function GetGafete: String;
  var
    sTemp: String;
  begin
    if (FTipoGafete = egTress) then
    begin
      Result := sGafete;
      if (Length(Result) = 6) then
        Result := '0' + Result;
      sTemp    := Copy(Result, 2, Length(Result) - 1); // Credencial sin incluir D�gito de Empresa
      if EsGafeteInvitador(Result) then
        Result := Result[1] + PadR(sTemp, 10)
        // Para evaluar prefijo 'IV' - NOTA: El �ltimo Caracter ya no va a ser la letra de la credencial, esto puede afectar si se comienza a utilizar esta letra para los invitadores
      else
      begin
        if strLleno (Result) then
          Result := Result[1] + PadLCar(sTemp, 10, '0')
      end;
    end else if (FTipoGafete = egProximidad) then begin
      Result := TOKEN_PROXIMIDAD + PadLCar(sGafete, K_MAX_LEN_PROXIMIDAD, '0');
    end else if (FTipoGafete = egBiometrico) then begin
      Result := TOKEN_BIOMETRICO + PadLCar(sGafete, K_MAX_LEN_PROXIMIDAD, '0'); // BIOMETRICO
    end else
      Result := Space(11);
  end;

begin
  Result := Estacion + // ZetaCommonTools.PadRCar( FEstacion, 10, '0' ) +
    '#' + GetGafete + FormatDateTime('mmddhhnn', dValue) + ZetaCommonTools.PadL(IntToStr(iComidas), 2) + sTipoComida;
  // + GetGafeteProximidad;
end;

{ MV(23/julio/2004): Procedimiento que se tenia en la forma de Cafeter�a }
{ y se paso a DCafeCliente para que se tenga la funcionalidad para checadas fuera de linea }
function TdmCafeCliente.CompletaGafete(const sGafete: String): String;
const
  K_GAFETE_ANCHO_MINIMO = 5;
var
  sGafeteEmpleado: String;
begin
  sGafeteEmpleado := sGafete;
  Result          := sGafeteEmpleado;
  with CafeRegistry do begin
    case TipoGafete of
      egTress: begin
          if (Length(sGafeteEmpleado) < K_GAFETE_ANCHO_MINIMO) then begin
            sGafeteEmpleado := ZetaCommonTools.PadLCar(sGafeteEmpleado, K_GAFETE_ANCHO_MINIMO, '0');
          end;
          Result := Trim(DefaultEmpresa) + sGafeteEmpleado + Trim(DefaultCredencial);
        end;
    end;
  end;
end;

procedure TdmCafeCliente.ConnTimerTimer(Sender: TObject);
begin
  inherited;
  if ServerRunning then
   RevisaModulo;
end;

{ DesCompletaGafete: Se usa solamente para cuando se tiene un Default de Credencial y Default de Empresa. Ya que corta el 1er y ultimo
  caracter del string del gafete }
function TdmCafeCliente.DesCompletaGafete(const sGafete: String): String;
begin
  Result := sGafete;
  with CafeRegistry do begin
    if StrLleno(DefaultCredencial) or StrLleno(DefaultEmpresa) then begin
      case TipoGafete of
        egTress: begin
            if StrLleno(DefaultCredencial) then begin
              Result := CortaUltimo(Result);
            end;
            if StrLleno(DefaultEmpresa) then begin
              Result := Copy(Result, 2, (Length(Result)));
            end;
          end;
      end;
    end;
  end;
end;

procedure TdmCafeCliente.AgregaChecadaFueraLinea(const sGafete, sTipoComida: String; const iComidas: Integer);
var
  dValue                   : TDateTime;
  sChecada, sGafeteEmpleado: String;
begin
  dValue := GetFechaHora((iComidas < 0));
  if (FAsciiFile = nil) then
    FAsciiFile := TAsciiLecturas.Create;
  with FAsciiFile do begin
    try
      Init(CafeRegistry.Archivo);
    except
      on Error: Exception do begin
        DataBaseError('Error al Abrir Archivo de Lecturas ' + CafeRegistry.Archivo);
      end;
    end;
    sGafeteEmpleado := CompletaGafete(sGafete);
    sChecada        := ChecadaToStr(sGafeteEmpleado, sTipoComida, iComidas, dValue);
    WriteTexto(sChecada);
    Close;
    { GA: Guarda checada en bit�coras }
    AsciiServer.Write(sChecada);
    // Guarda Datos de Checada Anterior
    FLastGafete   := sGafeteEmpleado;
    FLastHora     := dValue;
    FLastTipo     := StrToInt(sTipoComida);
    FLastCantidad := iComidas;
  end;
end;

procedure TdmCafeCliente.AgregaChecadaContador(const iContador: Integer; const sMensaje: String);
var
  sChecada, sHora, sFecha: String;
begin
  sHora  := FormatDateTime('hhnn', Now);
  sFecha := FormatDateTime('YYYYMMDD', Date);
  if (FAsciiFile = nil) then
    FAsciiFile := TAsciiLecturas.Create;
  with FAsciiFile do begin
    try
      Init(CafeRegistry.Archivo);
    except
      on Error: Exception do begin
        DataBaseError('Error al Abrir Archivo de Lecturas ' + CafeRegistry.Archivo);
      end;
    end;
    sChecada := '|' + Copy(CafeRegistry.Identificacion, 1, 4) + PadLCar(IntToStr(iContador), 10, '0') + sFecha + sHora + sMensaje;
    WriteTexto(sChecada);
    Close;
  end;
end;

procedure TdmCafeCliente.Init;
begin
  inherited;
end;

procedure TdmCafeCliente.InicializaCalendario(const sArchivo: String); { OP: 22.Mayo.08 }
  procedure TransfiereInfoCalendario;
  begin
    with adCalendarioAnterior do try
      LoadFromFile(sArchivo);
      SortClientDataSet(adCalendarioAnterior, 'HORA', [ixDescending]);
      // SortDataSetByFieldName('HORA', TRUE );
      First;
    except
      on Error: Exception do
        FormaCafe.WarningMessage(Format('No se Puede Leer Archivo de Calendario Anterior %s: %s', [sArchivo, Error.Message]));
    end;

    with adCalendario do try
      Close;
      if not Assigned(Fields.FindField('CONTADOR')) then begin
        FieldDefs.Add('CONTADOR', ftString, 1);
      end;
      if not Assigned(Fields.FindField('SEMANA')) then begin
        FieldDefs.Add('SEMANA', ftSmallInt);
      end;
      if not Assigned(Fields.FindField('SYNC')) then begin
        FieldDefs.Add('SYNC', ftString, 1);
      end;
      FieldDefs.Update;
      Open;
      while not adCalendarioAnterior.EOF do begin
        Append;
        FieldByName('HORA').AsString     := adCalendarioAnterior.FieldByName('HORA').AsString;
        FieldByName('ACCION').AsInteger  := adCalendarioAnterior.FieldByName('ACCION').AsInteger;
        FieldByName('CONTADOR').AsString := 'N';
        FieldByName('SYNC').AsString     := 'N';
        FieldByName('SEMANA').AsInteger  := 0;
        Post;
        adCalendarioAnterior.Next;
      end;
      SortClientDataSet(adCalendario, 'HORA', [ixDescending]);
      // SortDataSetByFieldName('HORA', TRUE );
      SaveToFile(sArchivo, dfXMLUTF8)
    except
      on e: Exception do
        DataBaseError(Format('Error procesando Archivo de Calendario %s: %s', [sArchivo, e.Message]));
    end;
  end;

  procedure LeerArchivo;
  begin
    try
      { OP: adCalendarioTmp nos ayuda a validar que el archivo tenga el formato actual,
        de lo contrario, entra a m�todo para actualizar el archivo al nuevo formato.
        En el caso que el formato sea correcto, continua y adCalendario toma los datos. }
      if Assigned(ImportarCalendario) then begin
        ImportarCalendario(StringToPAnsiChar(sArchivo));
      end else
      adCalendarioTmp.LoadFromFile(sArchivo);
      with adCalendario do begin
        LoadFromFile(sArchivo);
        SortClientDataSet(adCalendario, 'HORA', [ixDescending]);
        // SortDataSetByFieldName('HORA', TRUE );
        if not Assigned(Fields.FindField('CONTADOR')) or not Assigned(Fields.FindField('SEMANA')) or not Assigned(Fields.FindField('SYNC')) then
          TransfiereInfoCalendario;
      end;
    except
      on Error: Exception do
        FormaCafe.WarningMessage(Format('No se Puede Leer Archivo de Calendario %s: %s', [sArchivo, Error.Message]));
    end;
  end;

begin
  with adCalendario do begin
    if FileExists(sArchivo) then
      LeerArchivo
    else begin
      SortClientDataSet(adCalendario, 'HORA', [ixDescending]);
      // SortDataSetByFieldName('HORA', TRUE );
    end;

  end;
end;

procedure TdmCafeCliente.SiguienteComida;
const K_FILTRO_SEMANA = 'SEMANA = %d';
var edcDiaSemana: eDiasCafeteria;
    tPruebas, tHora1, tHora1TEMP, tHora2: TDateTime;
    iIndex, iPrimerComida: Integer;
    bpanelDer3SigComidaVISIBLE: Boolean;
    adCalendarioTEMP: TClientDataSet;
begin
    adCalendarioTEMP := TClientDataSet.Create(Self);
    adCalendarioTEMP.Data := adCalendario.Data;

    if FMostrarSigComida then
    begin
      // CafeRegistry.
      edcDiaSemana := ObtenDiaSemana;

      // Filtrar primero por d�a
      with adCalendarioTEMP do
      begin
        Filtered := FALSE;
        open;
        if ( IndexName <> 'SEMANAHORA_IDX' )  then
        begin
              if ( IndexDefs.IndexOf('SEMANAHORA_IDX') < 0  )  then
              begin
                    with IndexDefs.AddIndexDef do
                    begin
                        Name := 'SEMANAHORA_IDX' ;
                        Fields := 'HORA;SEMANA';
                        Options := [ixCaseInsensitive];
                    end;
              end;
        end;
        IndexName := 'SEMANAHORA_IDX';
        First;
      end;

      // Escribir datos en pantalla cuando hay registros de calendario.
      bpanelDer3SigComidaVISIBLE := FALSE;

      if adCalendarioTEMP.RecordCount > 0 then
        tHora1TEMP := StrToTime (FormatMaskText('99:99;0', adCalendarioTEMP.FieldByName('HORA').AsString));

      for iIndex := 0 to adCalendarioTEMP.RecordCount-1 do
      begin
          tHora1 := StrToTime (FormatMaskText('99:99;0', adCalendarioTEMP.FieldByName('HORA').AsString));
          tHora2 := StrToTime (FormatDateTime( 'hh:mm', now ));

          if (tHora2 < tHora1) and
              (
                (adCalendarioTEMP.FieldByName('SEMANA').AsInteger = ord (edcDiaSemana))
                or
                (adCalendarioTEMP.FieldByName('SEMANA').AsInteger = 0)
              ) and
              (adCalendarioTEMP.FieldByName('ACCION').AsInteger < 9)
              then
          begin
            if (tHora1TEMP < tHora1) AND (bpanelDer3SigComidaVISIBLE) then
            begin
              tHora1TEMP := tHora1;
              break;
            end;

            FormaCafe.lblSigComida.Caption := CafeRegistry.TextoTipoComida( adCalendarioTEMP.FieldByName('ACCION').AsInteger+1);
            FormaCafe.lblHrSigComida.Caption := 'a las ' + FormatDateTime( 'hh:nn AM/PM', tHora1 );

            bpanelDer3SigComidaVISIBLE := TRUE;
            tHora1TEMP := tHora1;
          end;

          adCalendarioTEMP.Next;
      end;

      // Si no se ha mostrado siguiente comida, mostrar primer comida
      // dentro de las siguientes 24 hrs.
      if not bpanelDer3SigComidaVISIBLE then
      begin
          with adCalendarioTEMP do
          begin
            Filtered := FALSE;
            open;
            if ( IndexName <> 'SEMANAHORA_IDX' )  then
            begin
                  if ( IndexDefs.IndexOf('SEMANAHORA_IDX') < 0  )  then
                  begin
                        with IndexDefs.AddIndexDef do
                        begin
                            Name := 'SEMANAHORA_IDX' ;
                            Fields := 'HORA;SEMANA';
                            Options := [ixCaseInsensitive];
                        end;
                  end;
            end;
            IndexName := 'SEMANAHORA_IDX';
            First;
          end;

          if adCalendarioTEMP.RecordCount > 0 then
            tHora1TEMP := StrToTime (FormatMaskText('99:99;0', adCalendarioTEMP.FieldByName('HORA').AsString));

          for iIndex := 0 to adCalendarioTEMP.RecordCount-1 do
          begin
              // Asignar primero a tHora2 la primer hora de comida del d�a siguiente
              tHora1 := StrToTime (FormatMaskText('99:99;0', adCalendarioTEMP.FieldByName('HORA').AsString));
              tHora2 := StrToTime (FormatDateTime( 'hh:mm', now ));

              if (tHora2 > tHora1)
              and
                 (((adCalendarioTEMP.FieldByName('SEMANA').AsInteger - ord(edcDiaSemana)) = 1)
                  or ((adCalendarioTEMP.FieldByName('SEMANA').AsInteger - ord(edcDiaSemana)) = -6)
                  or
                    (adCalendarioTEMP.FieldByName('SEMANA').AsInteger =  0))
              and
                (adCalendarioTEMP.FieldByName('ACCION').AsInteger < 9)
              then
              begin
                if (tHora1TEMP < tHora1) AND (bpanelDer3SigComidaVISIBLE) then
                begin
                  tHora1TEMP := tHora1;
                  break;
                end;

                FormaCafe.lblSigComida.Caption := CafeRegistry.TextoTipoComida( adCalendarioTEMP.FieldByName('ACCION').AsInteger+1);
                FormaCafe.lblHrSigComida.Caption := 'a las ' + FormatDateTime( 'hh:nn AM/PM', tHora1);

                bpanelDer3SigComidaVISIBLE := TRUE;
                tHora1TEMP := tHora1;
              end;
              adCalendarioTEMP.Next;
          end
      end;

      FormaCafe.panelDer3SigComida.Top := 202;
      FormaCafe.panelDer3SigComida.Visible := bpanelDer3SigComidaVISIBLE;
    end
    else
      FormaCafe.panelDer3SigComida.Visible := FALSE;

    // Liberar objeto adCalendarioTEMP.
    FreeAndNil (adCalendarioTEMP);
end;

{$IFDEF BOSE}

procedure TdmCafeCliente.InicializaContador(const sArchivo: String);
begin
  with adContador do try
    if FileExists(sArchivo) then begin
      if Assigned(ImportarContador) then begin
        ImportarContador(sArchivo);
      end else
      LoadFromFile(sArchivo);
    end;
    SortClientDataSet(adContador, 'HORA', [ixDescending]);
    // SortDataSetByFieldName('HORA', TRUE );
  except
    FormaCafe.WarningMessage(Format('No se Puede Leer Archivo de Contador %s: %s', [sArchivo, Error.Message]));
  end;
end;

procedure TdmCafeCliente.ScanContador(const sHora: String);
var
  sHoraContador: String;
begin
  with adContador do begin
    if Active and (not IsEmpty) then begin
      SortClientDataSet(adContador, 'HORA', []);
      // SortDataSetByFieldName('HORA', FALSE );
      First;
      sHoraContador := FieldByName('HORA').AsString;
      while (not EOF) and (aMinutos(sHoraContador) <= aMinutos(sHora)) do begin
        if (sHoraContador = sHora) then begin
          // Reiniciar Contador
          FormaCafe.ReiniciaConsumo(0);
          Break;
        end;
        Next;
        sHoraContador := FieldByName('HORA').AsString;
      end;
    end;
  end;
end;
{$ENDIF}

procedure TdmCafeCliente.EjecutaAccionCalendario(const TipoAccion: eCafeCalendario; const lContador: Boolean); { OP: 22.Mayo.08 }
begin
  case TipoAccion of
    cfComida1 .. cfComida9:
    begin
        TipoComida := Ord(TipoAccion) + 1;
        if lContador then
          FormaCafe.ReiniciaConsumo(0);
    end;
    cfNormal:
    begin
        ProhibirAcceso := False;
        with CafeRegistry do
          SetSocket(Servidor, Puerto);

        FormaCafe.paseSuGafete;
        {if lContador then
          FormaCafe.ReiniciaConsumo(0);}
    end;
    cfProhibe:
    begin
        ProhibirAcceso := True;
        // DesconectaSocket;
        with CafeRegistry do
          SetSocket(Servidor, Puerto);

        if lContador then
          FormaCafe.ReiniciaConsumo(0);
    end;
    cfValida:
    begin
        FValidarEmpleado := True;
        with CafeRegistry do
          SetSocket(Servidor, Puerto);

        if lContador then
          FormaCafe.ReiniciaConsumo(0);
    end;
    cfNoValida:
    begin
        FValidarEmpleado := False;
        Direccion := '';
        Puerto    := 0;
        Active    := FALSE;

        if lContador then
          FormaCafe.ReiniciaConsumo(0);
    end;
  end;
end;

procedure TdmCafeCliente.EjecutaAccionSincronizacion;
begin
     FSync       := True;
     FPrimeraVez := True;
end;

function TdmCafeCliente.GetHoraSincroniza : String;
var sFecha : String;
begin
     if CafeRegistry.TipoSincronizacion = K_FRECUENCIA_MINUTOS then
     begin
          DateTimeToString(sFecha, 'yyyy', HoraSincroniza);
          if sFecha = '1899' then
          begin
               HoraSincroniza := Now;
               HoraSincronizaLastTime := Now;
               Result := FormatDateTime('hhMM', HoraSincroniza);
          end;

          Result := FormatDateTime('hhMM', HoraSincroniza );
     end
     else if CafeRegistry.TipoSincronizacion = K_FRECUENCIA_DIARIA then
     begin
          Result := CafeRegistry.Sincronizacion;
     end;
end;

procedure TdmCafeCliente.ScanCalendario(const sHora: String);
var
   sHoraCal, sHoraSincroniza : String;
   DiaSemanaCal, DiaSemana: eDiasCafeteria; { OP: 23.Mayo.08 }
   adCalendarioTEMP: TClientDataSet;
   iHoraA, iHoraB : Integer;
begin
     //US13921 - Sincronizacion de Cafeteria
     sHoraSincroniza := GetHoraSincroniza;
     if sHoraSincroniza <> VACIO then
     begin
          iHoraA := StrToInt(sHora);
          iHoraB := StrToInt(sHoraSincroniza);
          if (iHoraA >= iHoraB) or ( MinutesBetween(Now, HoraSincronizaLastTime) > CafeRegistry.FrecuenciaMinutos ) then
          begin
               if StrLleno(CafeRegistry.Identificacion)  then
               begin
                    if VerificarSincronizacion then
                    begin
                         FormaCafe.WizardConfiguracion;
                         FormaCafe.ActualizaParametros;
                         FormaCafe.LeeParametros;
                    end;

                    if CafeRegistry.TipoSincronizacion = K_FRECUENCIA_MINUTOS then
                    begin
                         HoraSincronizaLastTime := Now;
                         HoraSincroniza := IncMinute( Now, CafeRegistry.FrecuenciaMinutos);
                    end;
               end;
          end;
     end;

     adCalendarioTEMP := TClientDataSet.Create(Self);
     adCalendarioTEMP.Data := adCalendario.Data;

     with adCalendarioTEMP do
     begin
          if Active and (not IsEmpty) then
          begin
               Filtered := FALSE;
               open;
               if ( adCalendarioTEMP.IndexName <> 'SEMANAHORA_IDX' )  then
               begin
                    if ( IndexDefs.IndexOf('SEMANAHORA_IDX') < 0  )  then
                    begin
                          with IndexDefs.AddIndexDef do
                          begin
                               Name := 'SEMANAHORA_IDX' ;
                               Fields := 'HORA;SEMANA';
                               Options := [ixCaseInsensitive];
                          end;
                    end;
               end;
               adCalendarioTEMP.IndexName := 'SEMANAHORA_IDX';
               First;

               sHoraCal     := FieldByName('HORA').AsString;
               DiaSemanaCal := eDiasCafeteria(FieldByName('SEMANA').AsInteger);

               // Obtiene el d�a de la semana actual   ( dcTodaSemana, dcLunes, dcMartes, dcMiercoles, dcJueves, dcViernes, dcSabado, dcDomingo );
               DiaSemana := ObtenDiaSemana;

               adCalendarioTEMP.First;
               while (not EOF)  do
               begin
                    if (aMinutos(sHoraCal) <= aMinutos(sHora)) then
                    begin
                         if (sHoraCal = sHora) and (ValidaDiaSemana(DiaSemanaCal, DiaSemana)) then
                         begin
                              EjecutaAccionCalendario(eCafeCalendario(FieldByName('ACCION').AsInteger), zStrToBool(FieldByName('CONTADOR').AsString));
                         end;
                    end;

                    // DES SP: 13301 EP III - Recarga de huellas
                    // if (sHoraCal = sHora) and (zStrToBool(FieldByName('SYNC').AsString)) then
                    if (sHoraCal = sHora) and (zStrToBool(FieldByName('SYNC').AsString)) and
                       (sHoraCal <> CafeRegistry.RegenerarArchivoHuellasHora) then
                    begin
                         EjecutaAccionSincronizacion;
                    end;

                    adCalendarioTEMP.Next;
                    sHoraCal     := FieldByName('HORA').AsString;
                    DiaSemanaCal := eDiasCafeteria(FieldByName('SEMANA').AsInteger);
               end;
          end;

          // DES SP: 13301 EP III - Recarga de huellas
          // Independiente al calendario, se revisa la configuraci�n para reiniciar consumos y para regenerar
          // el archivo de huellas.
          if (ZetaRegistryCafe.CafeRegistry.ReiniciaMediaNoche) and (aMinutos(sHora) = 0) then
             FormaCafe.ReiniciaConsumo(0);

          // DES SP: 13301 EP III - Recarga de huellas
          if CafeRegistry.RegenerarArchivoHuellasHora <> VACIO then
          begin
               // if (aMinutos(sHora) = aMinutos(CafeRegistry.RegenerarArchivoHuellasHora)) then
               if (sHora = CafeRegistry.RegenerarArchivoHuellasHora) then
               begin
                    // Hace que la cafeteria recargue TODAS las huellas nuevamente la siguiente vez que se active el sensor.
                    if ( TipoGafete = egBiometrico) then
                    begin
                         FormaCafe.DetieneLector;
                         SysUtils.DeleteFile (CafeRegistry.FileTemplates);

                         // Correcci�n de Bug #12036: NELLCOR: No se recargan las huellas automaticamente en cafeteria.
                         // EjecutaAccionSincronizacion; // ?
                         with dmCafeCliente do
                         begin
                              PrimeraVez := True;
                              ReiniciaHuellas;
                         end;

                         FormaCafe.TimerSensorBiometrico.Enabled := TRUE;
                    end;
               end;
          end;
     end;

     // Liberar adCalendarioTEMP.
     FreeAndNil (adCalendarioTEMP);
end;

procedure TdmCafeCliente.SetCalendarioInicial(const sHora: String);
var
  iComida                               : Integer;
  lProhibe, lValidar                    : Boolean;
  lLastComida, lLastProhibe, lLastValida: Boolean;
  DiaSemana                             : eDiasCafeteria; { OP: 23.Mayo.08 }

  procedure EvaluaAccion(const TipoAccion: eCafeCalendario; const lRepetidos: Boolean = True);
  begin
    case TipoAccion of
      cfComida1 .. cfComida9: begin
          if lRepetidos or (not lLastComida) then
            iComida   := Ord(TipoAccion) + 1;
          lLastComida := True;
        end;
      cfNormal .. cfProhibe: begin
          if lRepetidos or (not lLastProhibe) then
            lProhibe   := (TipoAccion = cfProhibe);
          lLastProhibe := True;
        end;
      cfValida .. cfNoValida: begin
          if lRepetidos or (not lLastValida) then
            lValidar  := (TipoAccion = cfValida);
          lLastValida := True;
        end;
    end;
  end;

begin
  iComida          := TipoComida;
  FProhibirAcceso  := False; // Defaults al Iniciar el Programa
  FValidarEmpleado := True;
  lProhibe         := ProhibirAcceso;
  lValidar         := ValidarEmpleado;
  with adCalendario do
    if Active and (not IsEmpty) then
    begin
      DiaSemana := ObtenDiaSemana; { OP: 23.Mayo.08 }

      if ( adCalendario.IndexName <> 'SEMANAHORA_IDX' )  then
      begin
            if ( IndexDefs.IndexOf('SEMANAHORA_IDX') < 0  )  then
            begin
                  with IndexDefs.AddIndexDef do
                  begin
                      Name := 'SEMANAHORA_IDX' ;
                      Fields := 'HORA;SEMANA';
                      Options := [ixCaseInsensitive];
                  end;
            end;
      end;
      adCalendario.IndexName := 'SEMANAHORA_IDX';

      // SortDataSetByFieldName('HORA', FALSE );
      // Revisi�n de los defaults que quedan el dia anterior
      // EvaluaAccion( eCafeCalendario( FieldByName( 'ACCION' ).AsInteger ) );
      lLastComida  := False;
      lLastProhibe := False;
      lLastValida  := False;
      Last;

      while (not BOF) do
      begin
        if (aMinutos(FieldByName('HORA').AsString) > aMinutos(sHora)) then
        begin
          if (ValidaDiaSemana(eDiasCafeteria(FieldByName('SEMANA').AsInteger), DiaAnterior((DiaSemana)))) then
            EvaluaAccion(eCafeCalendario(FieldByName('ACCION').AsInteger), False);
        end;
        Prior;
      end;

      // Revisi�n de los cambios generados en el transcurso del dia
      First;
      while (not EOF) do
      begin
        if (aMinutos(FieldByName('HORA').AsString) <= aMinutos(sHora)) then
        begin
          if ValidaDiaSemana(eDiasCafeteria(FieldByName('SEMANA').AsInteger), DiaSemana) then
            EvaluaAccion(eCafeCalendario(FieldByName('ACCION').AsInteger));
        end;
        Next;
      end;
    end;

  if (iComida <> TipoComida) then
    EjecutaAccionCalendario(eCafeCalendario(iComida - 1), zStrToBool(adCalendario.FieldByName('CONTADOR').AsString));
  { OP: 22.Mayo.08 }
  if (lProhibe <> ProhibirAcceso) then
    EjecutaAccionCalendario(cfProhibe, zStrToBool(adCalendario.FieldByName('CONTADOR').AsString));
  if (lValidar <> ValidarEmpleado) then
    EjecutaAccionCalendario(cfNoValida, zStrToBool(adCalendario.FieldByName('CONTADOR').AsString));
  if (not lProhibe) and (lValidar) then
    EjecutaAccionCalendario(cfNormal, zStrToBool(adCalendario.FieldByName('CONTADOR').AsString));
  // Aqu� se conecta inicialmente el Servidor
end;

procedure TdmCafeCliente.RevisaInvitadores;
var
  sLetrero, sDataSet: String;
begin
  if (FTipoGafete in [egProximidad, egBiometrico]) then // Si no es de proximidad no hace nada
  begin
    with cdsIdInvita do begin
      // Enviar peticion al servidor SOAP
      SOAPMsg := CreateSOAPMsg(K_LEE_LISTA_INVITADORES, ParamsToXML(Params));
      SendSOAPMsg(SOAPMsg, Params, cdsIdInvita);
      FreeAndNil(SOAPMsg);

      sLetrero := Params.ParamByName('Letrero').AsString;
      sDataSet := Params.ParamByName('IdInvita').AsString;
    end;

    adIdInvita.XMLData := sDataSet;

    if StrVacio(sLetrero) then // Si llega vacio no se detectaron problemas
    begin
      try
        adIdInvita.SaveToFile(GetFileIdInvita, dfXMLUTF8);
      except
        on Error: Exception do begin
          sLetrero := 'No se Grab� Archivo de Invitadores: ' + Error.Message;
        end;
      end;
    end;
    if StrLleno(sLetrero) then
      FormaCafe.WarningMessage(sLetrero);
  end;
end;

procedure TdmCafeCliente.RevisaLecturas;
var
  sArchivo: String;
  Lecturas: TStringList;
begin
  sArchivo := CafeRegistry.Archivo;
  if FileExists(sArchivo) then begin
    Lecturas := TStringList.Create;
    try
      try
        Lecturas.LoadFromFile(sArchivo);

        // Enviar peticion al servidor SOAP
        SOAPMsg := CreateSOAPMsg(K_CAFETERA_FUERA_DE_LINEA, '', Lecturas.Text);
        if SendSOAPMsg(SOAPMsg) then
          CodedStream(SOAPMsg);
        FreeAndNil(SOAPMsg);
      except
        DataBaseError('Error al Abrir Archivo de Lecturas ' + sArchivo);
        raise;
      end;
    finally
      Lecturas.free;
      SysUtils.DeleteFile(sArchivo);
    end;
  end;
end;

procedure TdmCafeCliente.ImportaChecadas(const sArchivo: String);
var
  Archivo: TStringList;
begin
  if FileExists(sArchivo) then begin
    Archivo := TStringList.Create;
    try
      try
        Archivo.LoadFromFile(sArchivo);
      except
        ZetaDialogo.Zerror('Importar Checadas', Format('Error al Abrir Archivo de Lecturas "%s"', [sArchivo]), 0);
      end;
      if (Archivo.Count <> 0) then begin

        // Enviar peticion al servidor SOAP
        SOAPMsg := CreateSOAPMsg(K_CAFETERA_FUERA_DE_LINEA, '', Archivo.Text);
        if SendSOAPMsg(SOAPMsg) then
          CodedStream(SOAPMsg);
        FreeAndNil(SOAPMsg);

        if ZetaDialogo.ZConfirm('Importaci�n de Checadas',
          Format('El Archivo  %s  fue Importado.' + CR_LF + 'Para Saber si Hubo Errores Revise la Bit�cora.' + CR_LF +
              '� Desea Revisarla ?', [ExtractFileName(sArchivo)]), 0, mbNo) then begin
          ShowBitacoraErrores(GetArchivoCafeTemporal(True, GetFolderLecturas(CafeRegistry.Archivo)));
        end;
      end;
    finally
      Archivo.free;
    end;
  end else
    ZetaDialogo.Zerror('Importaci�n de Checadas', Format('El Archivo %s no Existe', [sArchivo]), 0);

end;

procedure TdmCafeCliente.RevisaModulo;
begin
  // Enviar peticion al servidor SOAP
  SOAPMsg := CreateSOAPMsg(K_CAFETERA_DEMO);
  if SendSOAPMsg(SOAPMsg) then
    CodedMessage(SOAPMsg);
  FreeAndNil(SOAPMsg);
end;

procedure TdmCafeCliente.ImprimeListado(const Reloj, TipoComidas: String; const FechaInicial, FechaFinal: TDateTime;
  const SoloTotales: Boolean);
var
  sDetalle, sTotales : AnsiString;
  aDetalle, aTotales : TByteDynArray;

  function Converter(P: TByteDynArray): string;
var
  Buffer: AnsiString;
begin
  SetLength(Buffer, Length(P) );
  System.Move(P[0], Buffer[1], Length(P));
  Result := string (  Buffer ) ;
end;


begin

  try
        with cdsListado do begin
          with Params do begin
            ParamByName('FechaInicial').AsDate  := FechaInicial;
            ParamByName('FechaFinal').AsDate    := FechaFinal;
            ParamByName('TipoComidas').AsString := TipoComidas;
            ParamByName('Reloj').AsString       := Reloj;
            ParamByName('Detalle').AsString     := zBoolToStr(SoloTotales);
          end;

          // Enviar peticion al servidor SOAP
          SOAPMsg := CreateSOAPMsg(K_LEE_CHECADAS_CAFETERIA, ParamsToXML(Params));

          SendSOAPMsgXML_DetalleTotales(aDetalle, aTotales, SOAPMsg, Params, cdsListado);

          {$ifdef ANTES}
          sDetalle := Decrypt ( Converter( aDetalle) ) ;
          sTotales := Decrypt ( Converter( aTotales) ) ;
          {$else}
          sDetalle := Converter( aDetalle);
          sTotales := Converter( aTotales);
          {$endif}

          AsignaDS(dsDetalle, sDetalle);
          AsignaDS(dsTotales, sTotales);

          FreeAndNil(SOAPMsg);

        end;
  except
        on e: Exception do begin
              ZError('Imprime Listado' , 'No fue posible extraer la informaci�n de consumos', 0);
        end;
  end;



end;

function TdmCafeCliente.GetFolderLecturas(const sArchivo: String): String;
var
  iPos, iLast: Integer;
  sTemp      : String;
begin
  Result := VACIO;
  if StrLleno(sArchivo) then begin
    iLast := 0;
    sTemp := sArchivo;
    Repeat
      iPos  := Pos('\', sTemp);
      sTemp := Copy(sTemp, iPos + 1, MAXINT);
      if iPos > 0 then
        iLast := iLast + iPos;
    Until (iPos = 0);
    if (iLast > 0) then
      Result := Copy(sArchivo, 1, (iLast - 1));
  end;
end;

procedure TdmCafeCliente.CodedStream(SOAPMsg: TSOAPDataSet);
const
  K_SEPARADOR = ' *************************** ';
var
  FLista    : TStrings;
  sSeparador: String;
begin
  if (SOAPMsg.MsgID = K_ERROR_REPLY) then // Transferencia de Errores
  begin
    FLista := TStringList.Create;
    with FLista do
      try
        Text       := SOAPMsg.DataSet;
        sSeparador := CR_LF + K_SEPARADOR + FormatDateTime('ddd dd/mmm/yyyy hh:nn:ss am/pm', Now) + K_SEPARADOR;
        FLista.Insert(0, sSeparador);
        GuardaCafeTemporal(FLista, GetFolderLecturas(CafeRegistry.Archivo));
      finally
        free;
      end;
  end;
end;

{ Comunicaci�n con Puertos Seriales }

function TdmCafeCliente.GetConfiguracion(const lEntrada, lIguales: Boolean): String;
begin
  if lEntrada or lIguales then
    Result := CafeRegistry.ComEntrada
  else
    Result := CafeRegistry.ComSalida;
end;

function TdmCafeCliente.GetInfoPuerto(const iPuerto: Integer; const sConfiguracion: String): TInfoPuerto;
var
  oLista: TStringList;

  function GetValorLista(const indice: Integer): Integer;
  begin
    if (oLista.Count - 1 < indice) then
      Result := 0
    else
      Result := StrToIntDef(oLista[indice], 0);
  end;

begin
  oLista := TStringList.Create;
  try
    oLista.CommaText := sConfiguracion;
    with Result do begin
      PortNum     := iPuerto;
      BaudRate    := TVaBaudrate(GetValorLista(0)+1);
      DataBits    := TVaDataBits(GetValorLista(1));
      Parity      := TVaParity(GetValorLista(2));
      StopBits    := TVaStopBits(GetValorLista(3));
      DTRControl  := TVaControlDTR(GetValorLista(4));
      FlowControl := GetValorLista(5); // TVaFlowControl(GetValorLista(5));
      RTSControl  := TVaControlRTS(GetValorLista(6));
      EventChar   := AnsiChar(GetValorLista(7));
    end;
  finally
    oLista.free;
  end;
end;

function TdmCafeCliente.SetInfoPuerto(const InfoPuerto: TInfoPuerto): String;
begin
  Result := VACIO;
  with InfoPuerto do begin
    Result := ConcatString(Result, IntToStr(Ord(BaudRate)-1), ',');
    Result := ConcatString(Result, IntToStr(Ord(DataBits)), ',');
    Result := ConcatString(Result, IntToStr(Ord(Parity)), ',');
    Result := ConcatString(Result, IntToStr(Ord(StopBits)), ',');
    Result := ConcatString(Result, IntToStr(Ord(DTRControl)), ',');
    Result := ConcatString(Result, IntToStr(FlowControl), ',');

    Result := ConcatString(Result, IntToStr(Ord(RTSControl)), ',');
    Result := ConcatString(Result, IntToStr(Ord(EventChar)), ',');
  end;
end;

procedure TdmCafeCliente.CodedMessage(SOAPMsg: TSOAPDataSet);
begin
  if (SOAPMsg.MsgID = K_CAFETERA_DEMO) then
    FormaCafe.CheckDemo(SOAPMsg.DataSet);
end;

{ OP: 23.Mayo.08 }
procedure TdmCafeCliente.adCalendarioSEMANAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if not(Sender.DataSet.IsEmpty) then
    Text := ObtieneElemento(lfDiasCafeteria, Sender.AsInteger)
  else
    Text := '';
end;

{ OP: 23.Mayo.08 }
procedure TdmCafeCliente.adCalendarioNewRecord(DataSet: TDataSet);
begin
  DataSet.FieldByName('SEMANA').AsInteger  := Ord(SemanaCalendario);
  DataSet.FieldByName('CONTADOR').AsString := K_GLOBAL_NO;
  DataSet.FieldByName('SYNC').AsString     := K_GLOBAL_NO;
end;

{ OP: 23.Mayo.08 }
{eDiasCafeteria = ( dcTodaSemana, dcLunes, dcMartes, dcMiercoles, dcJueves, dcViernes, dcSabado, dcDomingo );}
function TdmCafeCliente.ObtenDiaSemana: eDiasCafeteria;
begin
  case DayOfWeek(Now) of
    1: Result := dcDomingo;
    2: Result := dcLunes;
    3: Result := dcMartes;
    4: Result := dcMiercoles;
    5: Result := dcJueves;
    6: Result := dcViernes;
    7: Result := dcSabado;
    else
      Result := dcTodaSemana;
  end;
end;

{ OP: 23.Mayo.08 }
function TdmCafeCliente.DiaAnterior(const Dia: eDiasCafeteria): eDiasCafeteria;
begin
  Result := Dia;
  if (Result = dcLunes) then
    Result := dcDomingo
  else
    Dec(Result);
end;

{ OP: 23.Mayo.08 }
function TdmCafeCliente.ValidaDiaSemana(DiaCal, DiaAct: eDiasCafeteria): Boolean;
begin
  Result := (DiaCal = DiaAct) or (DiaCal = dcTodaSemana);
end;

{ OP: 23.Mayo.08 }
procedure TdmCafeCliente.SetSemanaCalendario(const Dia: eDiasCafeteria);
const
  K_FILTRO_SEMANA = 'SEMANA = %d';
begin
  FSemanaCalendario := Dia;
  with adCalendario do begin
    Filter   := Format(K_FILTRO_SEMANA, [Ord(Dia)]);
    Filtered := True;
    First;
  end;

end;

{ OP: 23.Mayo.08 }
procedure TdmCafeCliente.QuitaFiltro;
begin
  with adCalendario do begin
    Filter   := VACIO;
    Filtered := False;
  end;
end;

// BIOMETRICO
procedure TdmCafeCliente.SincronizaHuellas;
const
  K_BIO_FILE = 'Biometric.tem';
var
  sDataSet, sLetrero: AnsiString;
  aDetalle, aTotales : TByteDynArray;

  function Converter(P: TByteDynArray): string;
var
  Buffer: AnsiString;
begin
  SetLength(Buffer, Length(P) );
  System.Move(P[0], Buffer[1], Length(P));
  Result := string (  Buffer ) ;
end;

begin
  with cdsHuellas do begin
    with Params do begin
      ParamByName('Huellas').AsString  := VACIO;
      ParamByName('Letrero').AsString  := VACIO;
      ParamByName('Computer').AsString := ZetaWinAPITools.GetComputerName;
      ParamByName('idCafeteria').AsString := CafeRegistry.Identificacion;
      ParamByName('TipoEstacion').AsInteger := Ord( dpCafeteria );
    end;

    // Enviar peticion al servidor SOAP
{
    try
      SOAPMsg := CreateSOAPMsg(K_OBTENER_HUELLAS, ParamsToXML(Params));
      SendSOAPMsg(SOAPMsg, Params);
      FreeAndNil(SOAPMsg);
    except
      on Error: Exception do begin
          sLetrero := 'No se obtuvieron huellas: ' + Error.Message + CR_LF + sLetrero;

      end;
    end;
    }
    sLetrero := Params.ParamByName('Letrero').AsString;
    sDataSet := Params.ParamByName('Huellas').AsString;


          SOAPMsg := CreateSOAPMsg(K_OBTENER_HUELLAS, ParamsToXML(Params));

          SendSOAPMsgXML_DetalleTotales(aDetalle, aTotales, SOAPMsg, Params, cdsListado);

          {$ifdef ANTES}
          sDataSet := Decrypt ( Converter( aDetalle) ) ;
          sLetrero := Decrypt ( Converter( aTotales) ) ;
          {$else}
          sDataSet :=  Converter( aDetalle);
          sLetrero :=  Converter( aTotales);
          {$endif}

          //AsignaDS(dsDetalle, sDetalle);
         // AsignaDS(dsTotales, sTotales);


  end;
  if StrVacio(sLetrero) then // Si llega vacio no se detectaron problemas
  begin
    try
      XMLToDataSet(sDataSet, cdsHuellas);
      if SysUtils.FileExists( GetFileHuellas ) then
         SysUtils.Deletefile( GetFileHuellas );
      cdsHuellas.SaveToFile(GetFileHuellas, dfXMLUTF8);
    except
      on Error: Exception do begin
        sLetrero := 'No se gener� archivo de huellas: ' + Error.Message + CR_LF + sLetrero;
      end;
    end;
  end;

  if StrLleno(sLetrero) then
    raise Exception.Create(sLetrero);
end;

// BIOMETRICO
procedure TdmCafeCliente.ReiniciaHuellas;
begin
     with cdsReiniciaHuellas do
     begin
          Params.ParamByName('Computer').AsString := ZetaWinAPITools.GetComputerName;

          // Enviar peticion al servidor SOAP
          SOAPMsg := CreateSOAPMsg(K_BORRAR_RELACION_HUELLAS, ParamsToXML(Params));
          SendSOAPMsg(SOAPMsg);
          FreeAndNil(SOAPMsg);
     end;
end;

// BIOMETRICO
procedure TdmCafeCliente.ReportaHuella(sLista: String);
begin
  with cdsReportaHuellas do begin
    Params.ParamByName('Computer').AsString := ZetaWinAPITools.GetComputerName;
    Params.ParamByName('Lista').AsString    := sLista;

    // Enviar peticion al servidor SOAP
    SOAPMsg := CreateSOAPMsg(K_REPORTAR_HUELLAS, ParamsToXML(Params));
    SendSOAPMsg(SOAPMsg);
    FreeAndNil(SOAPMsg);
  end;
end;

// BIOMETRICO
function TdmCafeCliente.GetArchivoHuellas: String;
begin
  Result := CafeRegistry.FileTemplates;
end;

procedure TdmCafeCliente.LiberaDS(DS: TDataSet);
var
  oDS: TDataSet;
begin
  oDS := DS;
  FreeAndNil(oDS);
end;

procedure TdmCafeCliente.AsignaDS(oDS: TDataSource; XML: String);
begin
  LiberaDS(oDS.DataSet);
  oDS.DataSet := nil;
  oDS.DataSet := XMLToDataSet(XML);
end;

procedure TdmCafeCliente.adCalendarioSYNCGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if DisplayText then begin
    if Sender.DataSet.IsEmpty then
      Text := ''
    else
      Text := BoolAsSiNo(zStrToBool(Sender.AsString));
  end else
    Text := Sender.AsString;
end;

procedure TdmCafeCliente.adCalendarioSYNCSetText(Sender: TField; const Text: String);
begin
  Sender.AsString := zBoolToStr(zStrToBool(UpperCase(Text)));
end;

// Fuente: http://stackoverflow.com/questions/283759/convert-string-to-pansichar-in-delphi-2009
function TdmCafeCliente.StringToPAnsiChar(stringVar : string) : PAnsiChar;
Var
  AnsString : AnsiString;
  InternalError : Boolean;
begin
  InternalError := false;
  Result := '';
  try
    if stringVar <> '' Then
    begin
       AnsString := AnsiString(StringVar);
       Result := PAnsiChar(PAnsiString(AnsString));
    end;
  Except
    InternalError := true;
  end;
  if InternalError or (String(Result) <> stringVar) then
  begin
    Raise Exception.Create('Conversion from string to PAnsiChar failed!');
  end;
end;
end.
