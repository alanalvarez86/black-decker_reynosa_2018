inherited ConfirmaComida: TConfirmaComida
  Left = 0
  Top = 0
  Caption = 'Datos de la Comida'
  ClientHeight = 212
  ClientWidth = 477
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 483
  ExplicitHeight = 241
  PixelsPerInch = 96
  TextHeight = 13
  object LbCred: TLabel [0]
    Left = 13
    Top = 11
    Width = 113
    Height = 29
    Caption = 'Credencial:'
    Color = clBtnFace
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object LblTipo: TLabel [1]
    Left = 66
    Top = 104
    Width = 298
    Height = 29
    Caption = #191'Desea registrar otra comida?'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel [2]
    Left = 47
    Top = 133
    Width = 335
    Height = 29
    Caption = 'Deslice su GAFETE para confirmar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbSegundo: TLabel [3]
    AlignWithMargins = True
    Left = 428
    Top = 80
    Width = 41
    Height = 83
    AutoSize = False
    Caption = '5'
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -80
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel [4]
    Left = 18
    Top = 54
    Width = 108
    Height = 29
    Caption = 'Empleado:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 176
    Width = 477
    TabOrder = 3
    ExplicitTop = 169
    ExplicitWidth = 477
    inherited OK_DevEx: TcxButton
      Left = 306
      ParentFont = False
      OnClick = OKClick
      ExplicitLeft = 306
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 387
      ParentFont = False
      OnClick = CancelarClick
      ExplicitLeft = 387
    end
  end
  object Checada: TButton [6]
    Left = 322
    Top = 20
    Width = 100
    Height = 25
    Caption = 'Checada'
    Default = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = ChecadaClick
  end
  object eCredencial: TZetaEdit [7]
    Left = 132
    Top = 8
    Width = 163
    Height = 35
    CharCase = ecUpperCase
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    Text = 'ECREDENCIAL'
  end
  object eEmpleado: TZetaEdit [8]
    Left = 132
    Top = 51
    Width = 314
    Height = 35
    Ctl3D = False
    Enabled = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Calibri'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 10223776
  end
  object TimerSegundos: TTimer
    Enabled = False
    OnTimer = TimerSegundosTimer
    Left = 27
    Top = 131
  end
  object TimerSensorBiometrico: TTimer
    Interval = 1
    OnTimer = TimerSensorBiometricoTimer
    Left = 80
    Top = 147
  end
end
