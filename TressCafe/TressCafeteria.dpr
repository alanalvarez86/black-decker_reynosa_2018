program TressCafeteria;

{$IF CompilerVersion > 20}
  {$IFOPT D-}{$WEAKLINKRTTI ON}{$ENDIF}
  {$RTTI EXPLICIT METHODS([]) PROPERTIES([]) FIELDS([])}
{$IFEND}

uses
  MidasLib,
  Forms,
  FCafeteria in 'FCafeteria.pas' {FormaCafe},
  DCafeCliente in 'DCafeCliente.pas' {dmCafeCliente: TDataModule},
  ZBaseDlgModal in '..\Tools\ZBaseDlgModal.pas' {ZetaDlgModal},
  FConfigura in 'FConfigura.pas' {FormaConfigura},
  FPreguntaComida in 'FPreguntaComida.pas' {PreguntaComida},
  DBaseCafeCliente in '..\ServerCafe\Cliente\DBaseCafeCliente.pas' {dmBaseCafeCliente: TDataModule},
  CafeteraWSDL in '..\ServerCafe\Cliente\CafeteraWSDL.pas',
  GraphicField in '..\ServerCafe\Cliente\GraphicField.pas',
  CafeteraConsts in '..\ServerCafe\Cliente\CafeteraConsts.pas',
  CafeteraUtils in '..\ServerCafe\CafeteraUtils.pas',
  BTMemoryModule in 'BTMemoryModule.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Cliente de Cafeter�a';
  Application.HelpFile := 'TressCafeteria.chm';
  Application.CreateForm(TFormaCafe, FormaCafe);
  Application.Run;
end.
