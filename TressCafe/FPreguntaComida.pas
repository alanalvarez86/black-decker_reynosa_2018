unit FPreguntaComida;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, StdCtrls, Mask, ZetaNumero, ZetaDBTextBox, Buttons,
  ExtCtrls, ZetaEdit, OleCtrls, ZBaseDlgModal, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dximctrl, Vcl.ImgList, cxButtons, SFE;

type
  TPreguntaComida = class(TZetaDlgModal_DevEx)
    LblCred: TLabel;
    LblTipo: TLabel;
    LblCantidad: TLabel;
    COMIDA: TZetaNumero;
    QTY: TZetaNumero;
    CREDENCIAL: TZetaEdit;
    Panel1: TPanel;
    Panel2: TPanel;
    TimerSensorBiometrico: TTimer;
    ilbTipoComida: TdxImageListBox;
    TimerPregunta: TTimer; // BIOMETRICO
    procedure FormShow(Sender: TObject);
    procedure Panel1Enter(Sender: TObject);
    procedure Panel2Enter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OKClick(Sender: TObject); // BIOMETRICO
    procedure CancelarClick(Sender: TObject); // BIOMETRICO
    procedure TimerSensorBiometricoTimer(Sender: TObject); // BIOMETRICO
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TimerPreguntaTimer(Sender: TObject); // BIOMETRICO
  private
    FValidando,FInvitador: Boolean;
    FCredencial : string;
    FCantidad : integer;
    FDrivers: boolean;
    //SFEStandard1: TSFEStandard; // BIOMETRICO
    function ValidaCaptura: Boolean;
    procedure SetControlActivo;
    procedure LlenaListaTipoComida;{OP: 21.Mayo.08}
    function CaptureFinger: Integer; // BIOMETRICO
    procedure DetieneLector; // BIOMETRICO
    procedure enableButtons(bEnable: Boolean); // BIOMETRICO
    procedure InicializaBiometrico; // BIOMETRICO
    procedure InicializaSensorBiometrico; // BIOMETRICO
    function LeeArchivo: Boolean; // BIOMETRICO
    procedure switchWorking(bWorking: Boolean); // BIOMETRICO
    { Private declarations }
  public
    property Invitador : Boolean read FInvitador write FInvitador;
    property Validando: Boolean read FValidando write FValidando;
    procedure EscribirCambios;
    procedure CancelarComida;
  end;

var
  PreguntaComida: TPreguntaComida;
  gTemplate         : array[0..1403] of Integer; // BIOMETRICO
  gImageBytes       : array[0..65535] of Byte;
  addrOfImageBytes  : Pbyte;
  addrOfTemplate    : PByte;
  gWorking          : Boolean;
  

implementation

uses DCafeCliente,
     ZetaDialogo,
     ZetaRegistryCafe,
     ZetaCommonLists,
     ZetaCommonClasses;

{$R *.DFM}

procedure TPreguntaComida.FormShow(Sender: TObject);
var 
   lBiometrico : Boolean; // BIOMETRICO
begin
     inherited;
     with dmCafeCliente do
     begin
          COMIDA.Enabled := UsaTeclado and PreguntaTipo;
          QTY.Enabled := UsaTeclado and PreguntaQty or FInvitador;
          CREDENCIAL.Enabled := ( not UsaTeclado );
     end;
     FCantidad := 1;
     SetControlActivo;

     // BIOMETRICO
     lBiometrico := ( dmCafeCliente.TipoGafete = egBiometrico );

     if ( lBiometrico ) and (FInvitador) then
     begin
          TimerSensorBiometrico.Enabled := lBiometrico;
          InicializaBiometrico;
     end;

     TimerPregunta.Enabled := TRUE;
end;

procedure TPreguntaComida.EscribirCambios;
begin
     FValidando := TRUE;
     try
        OK_DevEx.SetFocus;
        if ValidaCaptura then
           OK_DevEx.Click;
     finally
        FValidando := FALSE;
     end;
end;

procedure TPreguntaComida.CancelarComida;
begin
     Cancelar_DevEx.SetFocus;
     Cancelar_DevEx.Click;
end;

function TPreguntaComida.ValidaCaptura: Boolean;
begin
     Result := TRUE;
     if (Comida.ValorEntero <= 0) OR (Comida.ValorEntero > 9) then
     begin
          Result := FALSE;
          Comida.SetFocus;
          ZError(Caption, 'Los Tipos de Comida Deben Ser Entre 1 y 9',0);
     end;

     if (QTY.ValorEntero <= 0) then
     begin
          Result := FALSE;
          QTY.SetFocus;
          ZError(Caption, 'La Cantidad Debe Ser Mayor a Cero',0);
     end;
end;

procedure TPreguntaComida.Panel1Enter(Sender: TObject);
begin
     inherited;
     EscribirCambios;
end;

procedure TPreguntaComida.Panel2Enter(Sender: TObject);
begin
     inherited;
     if ( not dmCafeCliente.UsaTeclado ) then
     begin
          CREDENCIAL.SetFocus;
          if ( FCredencial = Credencial.Text ) then
          begin
               TimerPregunta.Enabled := FALSE;
               Inc(FCantidad);
               QTY.Valor := FCantidad;
               TimerPregunta.Enabled := TRUE;
          end
          else
              EscribirCambios;
     end
     else
         Cancelar_DevEx.SetFocus;
end;

procedure TPreguntaComida.SetControlActivo;
begin
     OK_DevEx.SetFocus;
     if CREDENCIAL.Enabled then
     begin
          CREDENCIAL.Setfocus;
          FCredencial := CREDENCIAL.Text;
     end
     else if COMIDA.Enabled then
        COMIDA.SetFocus
     else
        QTY.SetFocus;
end;

procedure TPreguntaComida.FormCreate(Sender: TObject);
begin
     inherited;
     LlenaListaTipoComida;{OP: 21.Mayo.08}
     FDrivers := LoadDLL;
end;

{OP: 21.Mayo.08}
procedure TPreguntaComida.LlenaListaTipoComida;
const
   K_OFFSET_IMAGE = 3;
var
   iContador : Integer;
begin
   for iContador := 1 to 9 do
   begin
        {with listTipoComida.Items do
        begin
             Add( Format('%d = %s', [ iContador, ZetaRegistryCafe.CafeRegistry.TextoTipoComida( iContador ) ] ) );
        end;}
        ilbTipoComida.InsertItem(iContador, ZetaRegistryCafe.CafeRegistry.TextoTipoComida( iContador ), iContador + K_OFFSET_IMAGE);
   end;
end;

// BIOMETRICO
procedure TPreguntaComida.OKClick(Sender: TObject);
begin
     gWorking := False;
     inherited;
end;

// BIOMETRICO
procedure TPreguntaComida.CancelarClick(Sender: TObject);
begin
     gWorking := False;
     inherited;
end;

// BIOMETRICO
procedure TPreguntaComida.TimerPreguntaTimer(Sender: TObject);
begin
     {if ( CafeRegistry.TipoGafete = egBiometrico ) then // BIOMETRICO
          DetieneLector;}

     {if ( PreguntaComida <> nil ) then
     begin}
          {with PreguntaComida do
          begin}
               Validando := TRUE;
               try
                  if Visible and Invitador and ( QTY.Valor > 0 ) and
                     ( not dmCafeCliente.UsaTeclado ) then           // Si no usa teclado s� graba
                     // PreguntaComida.EscribirCambios
                     EscribirCambios
                  else
                      // PreguntaComida.CancelarComida
                      CancelarComida
               finally
                  Validando := FALSE;
               end;
          // end;
     // end;

     {if ( CafeRegistry.TipoGafete = egBiometrico ) then // BIOMETRICO
          TimerSensorBiometrico.Enabled := True;}

     TimerPregunta.Enabled := FALSE; // ??
     // Close; // ??
end;

procedure TPreguntaComida.TimerSensorBiometricoTimer(Sender: TObject);

     function InicializaControl : Boolean;
     begin
          Result := False;
          try
             {if ( SFEStandard1 = nil ) then
             begin
                 SFEStandard1 := TSFEStandard.Create(self);
                 SFEStandard1.TabStop := False;
             end;}
            Result := True;
          except
                on E : Exception do
                   Application.HandleException( E );
                     //RechazaChecada( 'Driver de dispositivo biom�trico no detectado. ' );
          end;
     end;

begin
     TimerSensorBiometrico.Enabled := False;

     if ( dmCafeCliente.TipoGafete = egBiometrico ) then
     begin
          if ( ( InicializaControl ) and ( LeeArchivo ) ) then
             InicializaSensorBiometrico;
     end;
end;

procedure TPreguntaComida.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     inherited;
     // BIOMETRICO
     if ( dmCafeCliente.TipoGafete = egBiometrico ) then
        DetieneLector;

     TimerPregunta.Enabled := FALSE;
     TimerSensorBiometrico.Enabled := FALSE;
end;

// BIOMETRICO
procedure TPreguntaComida.DetieneLector;
begin
     if ( FDrivers ) then
        SFE.Close();
     enableButtons(False);
     switchWorking(False);
end;

// BIOMETRICO
procedure TPreguntaComida.switchWorking(bWorking : Boolean);
begin
     gWorking := bWorking;
end;

// BIOMETRICO
procedure TPreguntaComida.enableButtons(bEnable : Boolean);
begin
     switchWorking(Not bEnable);
end;

// BIOMETRICO
function TPreguntaComida.LeeArchivo: Boolean;
var
   strDbFileName : String;
   iArchivoId, nRet : Integer;
begin
     try
        FDrivers := LoadDLL;
        strDbFileName :=  dmCafeCliente.ArchivoHuellas;

        if not FileExists( strDbFileName ) then
        begin
             iArchivoId := FileCreate( strDbFileName );
             FileClose( iArchivoId );
        end;

        //if ( SFEStandard1 <> nil ) then
           {$ifdef ANTES}
           nRet := SFE.Open( strDbFileName, 4, 0 )
           {$else}
           if ( FDrivers ) then
              nRet := SFE.Open( PChar( strDbFileName ), K_TIPO_BIOMETRICO, 0 );
           {$endif}
        //else
            //nRet := 1;
        if nRet = 0 then
        begin
             EnableButtons(True);
             Result := True;
        end
        else
        begin
             Raise Exception.Create( 'No hay dispositivo biom�trico!' );
        end;
     except
           on Error: Exception do
           begin
                Application.HandleException( Error );
                switchWorking(False);
                Result := False;
           end;
     end;
end;

// BIOMETRICO
procedure TPreguntaComida.InicializaSensorBiometrico;
var
   nRet, userId, fingerNum: Integer;

     procedure ObtieneLectura;
     label
          stopWorking;
     begin
          if ( LeeArchivo ) then
          begin
               try
                  switchWorking(True);
                  userId := 0;
                  nRet := CaptureFinger; //Devuelve alg�n mensaje de lectura o error.

                  if nRet <> 1 then
                     goto stopWorking;

                  if ( FDrivers ) then
                     nRet := SFE.Identify(userId, fingerNum); //Obtiene el Identificador de la huella y el dedo

                  if nRet < 0 then
                       userId := 0
                  else
                      goto stopWorking;
stopWorking:
                  switchWorking(False);
               except
                     on Error: Exception do
                     begin
                          Application.HandleException( Error );
                          switchWorking(False);
                     end;
               end;
          end
     end;

begin
     ObtieneLectura;
     // Cambios por funcionalidad de checada adicional al lector biom�trico.
     if ( ( userId > 0 ) and ( IntToStr( userId ) = Credencial.Text ) ) then
     begin
          if Invitador and (not dmCafeCliente.UsaTeclado) then
          begin
               Panel2Enter(Self);
               TimerSensorBiometrico.Enabled := TRUE;
          end
          else
          begin
               TimerPregunta.Enabled := FALSE;
               TimerSensorBiometrico.Enabled := FALSE;
               Panel1Enter(Self);
          end;
     end
     else
     begin
          // Panel1Enter(Self);
          // DetieneLector;
     end;
     // DetieneLector;
     // ----- -----
end;

// BIOMETRICO
function TPreguntaComida.CaptureFinger: Integer;
var
  nRet, nCapArea, nMaxCapArea, nResult : Integer;
begin
     nResult := 1;
     nMaxCapArea := 0;

     while True do
     begin
          try
             Application.ProcessMessages();
             if Not gWorking then
             begin
                  nResult := 0;
                  break;
             end;
             Application.ProcessMessages();
             if ( FDrivers ) then
                nRet := SFE.Capture();

             if nRet < 0 then
             begin
                  nResult := 0;
                  break;
             end;
             Application.ProcessMessages();
             if ( FDrivers ) then
                nCapArea := SFE.IsFinger();

             if nCapArea >= 0 then
             begin
                  if nCapArea < nMaxCapArea + 2 then break;
                  if nCapArea > nMaxCapArea then nMaxCapArea := nCapArea;
                  if nCapArea > 45 then break;
             end;
          except
                Result := 0;
                Exit;
          end;
     end;

     Application.ProcessMessages();
     if nResult = 1 then
     begin
          if ( FDrivers ) then
             nRet := SFE.GetImage(addrOfImageBytes);

          if nRet < 0 then
          begin
               nResult := 0;
          end
     end;
     CaptureFinger := nResult;
     Application.ProcessMessages();
end;

// BIOMETRICO
procedure TPreguntaComida.InicializaBiometrico;
begin
     //Inicializa Biometrico
     enableButtons(True);
     addrOfImageBytes := addr( gImageBytes );
     addrOfTemplate := addr( gTemplate );
end;


end.
