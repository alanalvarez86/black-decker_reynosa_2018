inherited ParametrosImpresion: TParametrosImpresion
  Left = 286
  Top = 242
  Caption = 'Par'#225'metros de Impresi'#243'n'
  ClientHeight = 246
  ClientWidth = 413
  OldCreateOrder = True
  OnCreate = FormCreate
  ExplicitWidth = 419
  ExplicitHeight = 275
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 57
    Top = 13
    Width = 38
    Height = 13
    Caption = 'Imprimir:'
  end
  object lbFechaInicial: TLabel [1]
    Left = 65
    Top = 38
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Inicial:'
  end
  object lbFechaFinal: TLabel [2]
    Left = 70
    Top = 63
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Final:'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object ComidasLBL: TLabel [3]
    Left = 52
    Top = 88
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Comidas:'
  end
  inherited PanelBotones: TPanel
    Top = 210
    Width = 413
    TabOrder = 4
    ExplicitTop = 210
    ExplicitWidth = 364
    inherited OK_DevEx: TcxButton
      Left = 247
      OnClick = OKClick
      ExplicitLeft = 198
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 328
      ExplicitLeft = 279
    end
  end
  object EFechaInicial: TZetaFecha [5]
    Left = 100
    Top = 34
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '15/Dec/97'
    Valor = 35779.000000000000000000
  end
  object EFechaFinal: TZetaFecha [6]
    Left = 100
    Top = 59
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '09/Dec/97'
    Valor = 35773.000000000000000000
  end
  object ListaTipos_DevEx: TcxCheckListBox [7]
    Left = 99
    Top = 88
    Width = 306
    Height = 114
    Columns = 2
    Items = <
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end>
    TabOrder = 3
    OnClick = ListaTiposClick
  end
  object CBTipoImpresion_DevEx: TcxComboBox [8]
    Left = 99
    Top = 8
    Properties.DropDownListStyle = lsFixedList
    Properties.Items.Strings = (
      'Detalle de Consumos'
      'Totales de Consumos')
    TabOrder = 0
    Width = 145
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 8913080
  end
end
