unit FImportaChecadas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseDlgModal_DevEx, Buttons, StdCtrls, ExtCtrls, ZBaseDlgModal, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters, Vcl.Menus, cxControls, cxContainer,
  cxEdit, cxTextEdit, Vcl.ImgList, cxButtons;

type
  TImportaChecadas = class(TZetaDlgModal_DevEx)
    LblArchivo: TLabel;
    OpenDialog: TOpenDialog;
    ArchivoSeek_DevEx: TcxButton;
    EditArchivo: TcxTextEdit;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
  private
    procedure ImportaChecadas;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ImportaChecadas: TImportaChecadas;

implementation
uses
    ZetaDialogo,
    ZetaCommonTools,
    DCafeCliente;

{$R *.DFM}
{
Archivos Temporales (Mient.00*)|Mient.00*|Archivo Perpetuo (Mient.mie)|Mient.mie|Archivos de Datos (*.dat)|*.dat|Archivos de Texto (*.txt)|*.txt|Todos (*.*)|*.*
}


procedure TImportaChecadas.ArchivoSeekClick(Sender: TObject);
 var
    sArchivo : string;
begin
     inherited;
     sArchivo := EditArchivo.Text;
     with OpenDialog do
     begin
          FileName := ExtractFileName( sArchivo );
          InitialDir := ExtractFilePath( sArchivo );

          if Execute then
             EditArchivo.Text := FileName;
     end;
end;

procedure TImportaChecadas.ImportaChecadas;
 var
    sArchivo : string;
begin
     sArchivo := EditArchivo.Text;
     if StrLleno( sArchivo ) then
     begin
          dmCafeCliente.ImportaChecadas( sArchivo );
     end
     else
         ZetaDialogo.ZError( Caption, 'El Nombre del Archivo no Puede Quedar Vac�o',0 );
end;

procedure TImportaChecadas.OKClick(Sender: TObject);
begin
     inherited;
     ImportaChecadas;
end;

end.


