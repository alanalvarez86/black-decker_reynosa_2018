inherited ImportaChecadas: TImportaChecadas
  Left = 179
  Top = 221
  Caption = 'Importaci'#243'n de Checadas'
  ClientHeight = 89
  ClientWidth = 437
  OldCreateOrder = True
  ExplicitWidth = 443
  ExplicitHeight = 118
  PixelsPerInch = 96
  TextHeight = 13
  object LblArchivo: TLabel [0]
    Left = 8
    Top = 20
    Width = 98
    Height = 13
    Caption = 'Archivo de Lecturas:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited PanelBotones: TPanel
    Top = 53
    Width = 437
    TabOrder = 2
    ExplicitTop = 53
    ExplicitWidth = 437
    inherited OK_DevEx: TcxButton
      Left = 270
      Top = 5
      OnClick = OKClick
      ExplicitLeft = 270
      ExplicitTop = 5
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 351
      Top = 5
      ExplicitLeft = 351
      ExplicitTop = 5
    end
  end
  object ArchivoSeek_DevEx: TcxButton [2]
    Left = 404
    Top = 16
    Width = 21
    Height = 21
    Hint = 'Indicar Archivo de Lecturas Fuera de L'#237'nea'
    OptionsImage.Glyph.Data = {
      DA050000424DDA05000000000000360000002800000013000000130000000100
      200000000000A40500000000000000000000000000000000000000B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFDDF3
      FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3FAFFDDF3
      FAFFDDF3FAFF14B8EBFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF10B7EAFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF3FC4EEFF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF3FC4EEFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFF6AD1
      F0FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF6AD1F0FFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFFFCFCFCFF92DDF5FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF96DFF5FFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC9EDF9FFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFE8F6FAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF53CA
      EFFF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCE
      F0FF5FCEF0FF5FCEF0FF5FCEF0FF5FCEF0FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFDDF3FAFFFCFC
      FCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFCFCFFFCFC
      FCFFFCFCFCFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FFDDF3FAFFFCFCFCFFFCFCFCFFFCFCFCFFBDEAF7FF7ED7F2FF7ED7
      F2FF7ED7F2FF7ED7F2FF7ED7F2FF7ED7F2FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FFC1EBF8FFDDF3FAFFDDF3FAFFDDF3
      FAFF6ED2F1FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    OnClick = ArchivoSeekClick
  end
  object EditArchivo: TcxTextEdit [3]
    Left = 112
    Top = 16
    AutoSize = False
    ParentFont = False
    Properties.ReadOnly = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Century Gothic'
    Style.Font.Style = []
    Style.IsFontAssigned = True
    TabOrder = 0
    Height = 21
    Width = 286
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  object OpenDialog: TOpenDialog
    DefaultExt = 'dat'
    Filter = 
      'Archivos Temporales (Mient.00*)|Mient.00*|Archivo Perpetuo (Mien' +
      't.mie)|Mient.mie|Archivos de Datos (*.dat)|*.dat|Archivos de Tex' +
      'to (*.txt)|*.txt|Todos (*.*)|*.*'
    Options = [ofHideReadOnly]
    Title = 'Seleccione el Archivo'
    Left = 54
    Top = 42
  end
end
