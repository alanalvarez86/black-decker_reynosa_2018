unit FWizConfigurarCafeteria_DevEx;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, ZetaCXWizard, dxGDIPlusClasses, cxImage,
  cxLabel, cxGroupBox, dxCustomWizardControl, dxWizardControl, Vcl.Menus,
  Vcl.ExtCtrls, Vcl.StdCtrls, cxTextEdit, cxButtons,
  ZetaKeyLookup_DevEx, cxProgressBar, cxDropDownEdit,
  ZetaCXStateComboBox, cxMaskEdit, cxSpinEdit, Vcl.Mask,
  ZetaFecha, cxCheckBox, TressMorado2013, ZcxBaseWizard, ZcxWizardBasico, ShellAPI,ZetaCommonClasses, FConfigura;

type
  TWizConfigurarCafeteria_DevEx = class(TcxBaseWizard)
    lblEstacion: TLabel;
    Estacion: TcxTextEdit;
    verConfig: TcxButton;
    rgOpciones: TRadioGroup;
    pAdvertencia: TPanel;
    lCaption: TLabel;
    lblPregunta: TLabel;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
    procedure verConfigClick(Sender: TObject);
    procedure rgOpcionesClick(Sender: TObject);
    procedure WizardAlCancelar(Sender: TObject; var lOk: Boolean);
  private
    { Private declarations }
    procedure ResetElementos;
    procedure IniciarElementos(var sEstacion : String);
  public
    { Public declarations }
  protected
    { Protected declarations }
    procedure CargaParametros; override;
    function EjecutarWizard: Boolean; override;
  end;


const
  K_ADVERTENCIA = 'El identificador %s tiene una configuraci�n existente en Sistema TRESS. Se tiene la opci�n de utilizar el identificador actual ' +
                'y reflejar la configuraci�n que se tiene en Sistema TRESS en el equipo local. La otra opci�n es indicar un nuevo identificador en ' +
                'la estaci�n, lo cual reflejar� la configuraci�n local en Sistema TRESS.';
  K_ERROR = 'El identificador "%s" ya existe en Sistema TRESS, ' +  #13#10 + ' especifique un identificador distinto para poder continuar.';
  K_TOP_LABEL = 211;
  K_OPCION_COPIAR = 0;
  K_OPCION_CREAR = 1;

var
  WizConfigurarCafeteria_DevEx: TWizConfigurarCafeteria_DevEx;

implementation

uses DCafeCliente, ZetaRegistryCafe, ZetaCommonTools, FCafeteria, CafeteraConsts, ZetaDialogo, ZetaHelpCafe;

{$R *.dfm}

procedure TWizConfigurarCafeteria_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var
   sEstacion : String;
   lEstado : Boolean;
   iItem : Integer;
begin
     inherited;
     sEstacion := Estacion.Text;
     if CanMove then
     begin
          if Wizard.Adelante and Wizard.EsPaginaActual(Parametros) then
          begin
               if StrVacio(sEstacion) then
                  CanMove := Error('La Estaci�n esta vacia', Estacion)
               else
               begin
                    lEstado := dmCafeCliente.BuscaCafeteria(sEstacion);
                    if rgOpciones.Visible then
                    begin
                         iItem := rgOpciones.ItemIndex;
                         if ( ( lEstado and ( Not rgOpciones.Visible ) ))  then
                         begin
                              CanMove := False;
                              IniciarElementos(sEstacion);
                              end
                         else
                         begin
                              if ( lEstado and  ( iItem = K_OPCION_CREAR) ) then
                              begin
                                   CanMove := False;
                                   ZetaDialogo.ZError( Caption, Format(K_ERROR, [ sEstacion ]), 0);
                              end
                         end;
                    end
                    else
                        if lEstado then
                        begin
                             CanMove := False;
                             ZetaDialogo.ZError( Caption, Format(K_ERROR, [ sEstacion ]), 0 );
                        end;
               end;
          end
     end;
end;

procedure TWizConfigurarCafeteria_DevEx.IniciarElementos(var sEstacion: string);
begin
     pAdvertencia.Visible := True;
     rgOpciones.Visible := True;
     lblPregunta.Visible := True;
     Estacion.Enabled := False;
     lCaption.Caption := Format(K_ADVERTENCIA, [sEstacion]);
     ActiveControl := rgOpciones;
     Estacion.Top := K_TOP_LABEL - 3;
     lblEstacion.Top := K_TOP_LABEL;
end;

procedure TWizConfigurarCafeteria_DevEx.ResetElementos;
begin
     pAdvertencia.Visible := False;
     rgOpciones.Visible := False;
     ParametrosControl := Estacion;
     Estacion.Enabled := True;
     Estacion.Top := K_TOP_LABEL - 103;
     lblEstacion.Top := K_TOP_LABEL - 100;
     lblPregunta.Visible := False;
end;

procedure TWizConfigurarCafeteria_DevEx.rgOpcionesClick(Sender: TObject);
begin
     inherited;
     Estacion.Enabled := (rgOpciones.ItemIndex = K_OPCION_CREAR);
     if rgOpciones.ItemIndex = K_OPCION_COPIAR then
        Estacion.Text := CafeRegistry.Identificacion
     else
         Estacion.Text := VACIO;
end;

procedure TWizConfigurarCafeteria_DevEx.FormCreate(Sender: TObject);
var sEstacion : String;
begin
     inherited;
     Self.HelpContext := H00032_Configuracion_Cliente_Wizard;
     sEstacion := CafeRegistry.Identificacion;
     Estacion.Text := sEstacion;
     ResetElementos;
     if dmCafeCliente.BuscaCafeteria(sEstacion) then
        IniciarElementos(sEstacion);
end;

procedure TWizConfigurarCafeteria_DevEx.CargaParametros;
begin
     with Descripciones do
     begin
          AddString('Estaci�n', Estacion.Text);
          with CafeRegistry do
          begin
               if StrLleno(Calendario) then
                  AddString('Ubicaci�n de calendario', Calendario);
          end;
     end;

     with ParameterList do
     begin
          AddString('Estacion', Estacion.Text);
          with CafeRegistry do
          begin
               AddInteger('TipoComida', TipoComida);
               AddBoolean('ReiniciaMediaNoche', ReiniciaMediaNoche);
               AddInteger('ChecadaSimultanea', ord(ChecadaSimultanea));
               AddInteger('TiempoEspera', TiempoEspera);

               AddString('DefaultEmpresa', DefaultEmpresa);
               AddString('DefaultCredencial', DefaultCredencial);
               AddString('Banner', Banner);
               AddInteger('Velocidad', Velocidad);

               AddBoolean('UsaTeclado', UsaTeclado);
               AddBoolean('PreguntaTipo', PreguntaTipo);
               AddBoolean('PreguntaQty', PreguntaQty);
               AddBoolean('MostrarFoto', MostrarFoto);
               AddBoolean('MostrarComida', MostrarComidas);
               AddBoolean('MostrarSigComida', MostrarSigComida);

               if StrLleno(Calendario) then
                  AddString('Calendario', Calendario);

               {$IFDEF BOSE}
               AddString('ArchivoContador', ArchivoContador);
               {$ENDIF}

               AddString('TipoComida1', TipoComida1);
               AddString('TipoComida2', TipoComida2);
               AddString('TipoComida3', TipoComida3);
               AddString('TipoComida4', TipoComida4);
               AddString('TipoComida5', TipoComida5);
               AddString('TipoComida6', TipoComida6);
               AddString('TipoComida7', TipoComida7);
               AddString('TipoComida8', TipoComida8);
               AddString('TipoComida9', TipoComida9);

               AddString('GafeteAdmon', GafeteAdmon);
               AddString('ClaveAdmon', ClaveAdmon);
               AddBoolean('ConcesionImprimir', ConcesionImprimir);
               AddBoolean('ConcesionCancelar', ConcesionCancelar);
          end;
     end;
     inherited;
end;

function TWizConfigurarCafeteria_DevEx.EjecutarWizard: Boolean;
begin
     inherited;
     Result := False;
     if ( ( rgOpciones.Visible ) and ( rgOpciones.ItemIndex = K_OPCION_COPIAR )) then
     begin
          FormaConfigura.SincronizaConfiguracion(Estacion.Text);
          FormaCafe.ActualizaParametros;
          if FormaCafe.LblConectado.Caption = ESTADO_CONEXION[K_CAFETERA_EN_LINEA_TEXT] then
             Result := True
     end
     else
         Result := dmCafeCliente.ImportarConfiguracion(ParameterList);

     if Result then
     begin
          FormaCafe.ShowTipoConexion(K_CAFETERA_EN_LINEA_TEXT);
     end
     else
     begin
          ZetaDialogo.zError( Self.Caption, 'Error al Configurar Cafeter�a', 0 );
          FormaCafe.ShowTipoConexion(K_CAFETERA_PENDIENTE_TEXT);
     end;
end;

procedure TWizConfigurarCafeteria_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizConfigurarCafeteria_DevEx.verConfigClick(Sender: TObject);
var
   FBuffer: TextFile;
   sFile : string;
begin
     inherited;
     sFile := ExtractFilePath( Application.ExeName ) + 'CONFIG_' +  Estacion.Text + '.txt';
     AssignFile(FBuffer, sFile);
     Rewrite(FBuffer);
     with CafeRegistry do
     begin
          WriteLn(FBuffer, 'Tipo de Comida: ' + IntToStr(TipoComida));
          WriteLn(FBuffer, 'Reiniciar Consumos: ' + zBoolToStr (ReiniciaMediaNoche));
          WriteLn(FBuffer, 'Checadas Simultaneas: ' + aSimultaneas[ChecadaSimultanea]);
          WriteLn(FBuffer, 'Limpiar Pantalla en: ' + IntToStr (TiempoEspera));
          WriteLn(FBuffer, 'Empresa: ' + DefaultEmpresa);
          WriteLn(FBuffer, 'Credencial: ' + DefaultCredencial);
          WriteLn(FBuffer, 'Letrero: ' + Banner);
          WriteLn(FBuffer, 'Velocidad de Letrero: ' + IntToStr ( Velocidad ));
          WriteLn(FBuffer, 'Usa Teclado: ' + zBoolToStr (UsaTeclado));
          WriteLn(FBuffer, 'Preguntar Tipo de Comida: ' + zBoolToStr (PreguntaTipo));
          WriteLn(FBuffer, 'Preguntar Cantidad: ' + zBoolToStr (PreguntaQty));
          WriteLn(FBuffer, 'Mostrar Foto: ' + zBoolToStr (MostrarFoto));
          WriteLn(FBuffer, 'Mostrar Comida: ' + zBoolToStr (MostrarComidas));
          WriteLn(FBuffer, 'Mostrar siguiente Comida: ' + zBoolToStr (MostrarSigComida));
          WriteLn(FBuffer, 'Tipo de Comida #1: ' + TipoComida1);
          WriteLn(FBuffer, 'Tipo de Comida #2: ' + TipoComida2);
          WriteLn(FBuffer, 'Tipo de Comida #3: ' + TipoComida3);
          WriteLn(FBuffer, 'Tipo de Comida #4: ' + TipoComida4);
          WriteLn(FBuffer, 'Tipo de Comida #5: ' + TipoComida5);
          WriteLn(FBuffer, 'Tipo de Comida #6: ' + TipoComida6);
          WriteLn(FBuffer, 'Tipo de Comida #7: ' + TipoComida7);
          WriteLn(FBuffer, 'Tipo de Comida #8: ' + TipoComida8);
          WriteLn(FBuffer, 'Tipo de Comida #9: ' + TipoComida9);
          WriteLn(FBuffer, 'Gafete: ' + GafeteAdmon);
          WriteLn(FBuffer, 'Puede Imprimir: ' + zBoolToStr (ConcesionImprimir));
          WriteLn(FBuffer, 'Puede Cancelar Comidas: ' + zBoolToStr (ConcesionCancelar));
     end;
     CloseFile(FBuffer);

     ShellExecute(Handle, 'open', PChar(sFile), nil, nil, SW_SHOWNORMAL);
end;

procedure TWizConfigurarCafeteria_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if (Wizard.EsPaginaActual(Parametros)) then
     begin
          parametrosControl := Estacion;
     end;
end;

procedure TWizConfigurarCafeteria_DevEx.WizardAlCancelar(Sender: TObject; var lOk: Boolean);
begin
     inherited;
     FormaCafe.ShowTipoConexion(K_CAFETERA_PENDIENTE_TEXT);
end;

end.
