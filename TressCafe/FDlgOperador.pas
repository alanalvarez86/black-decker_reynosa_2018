unit FDlgOperador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, ComCtrls, ToolWin, dxBar, cxClasses;

type
  TDlgOperador = class(TForm)
    dxBarManager: TdxBarManager;
    dxBarManagerBar: TdxBar;
    Imprimir_DevEx: TdxBarLargeButton;
    Configurar_DevEx: TdxBarLargeButton;
    Consumos_DevEx: TdxBarLargeButton;
    Cancelar_DevEx: TdxBarLargeButton;
    ImportarChecadas_DevEx: TdxBarLargeButton;
    Salir_DevEx: TdxBarLargeButton;
    procedure ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    FOperacion: Integer;
  protected
    { Protected declarations }
    procedure KeyPress( var Key: Char ); override; { TWinControl }
  public
    { Public declarations }
    property Operacion : Integer read FOperacion;
  end;

var
  DlgOperador: TDlgOperador;

implementation

uses ZetaDialogo, ZetaRegistryCafe, ZetaHelpCafe;

{$R *.DFM}

procedure TDlgOperador.FormCreate(Sender: TObject);
begin
     HelpContext := H00008_Activando_el_menu_Operador;
end;

procedure TDlgOperador.FormShow(Sender: TObject);
begin
     FOperacion := 0;
end;

procedure TDlgOperador.ButtonClick(Sender: TObject);
begin
     FOperacion := TWinControl( Sender ).Tag;
     self.Close;
end;

procedure TDlgOperador.KeyPress( var Key: Char );
begin
     inherited KeyPress( Key );
     if ( Key = Chr( VK_ESCAPE ) ) then
        ButtonClick( Salir_DevEx );
end;

end.
