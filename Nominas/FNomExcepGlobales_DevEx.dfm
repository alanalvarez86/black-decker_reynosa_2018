inherited NomExcepGlobales_DevEx: TNomExcepGlobales_DevEx
  Left = 270
  Top = 134
  Caption = 'Excepciones Globales'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ZetaDBGrid: TZetaCXGrid
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object CO_NUMERO: TcxGridDBColumn
        Caption = 'Concepto'
        DataBinding.FieldName = 'CO_NUMERO'
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 75
        Options.Grouping = False
        Width = 75
      end
      object CO_DESCRIP: TcxGridDBColumn
        Caption = 'Descripci'#243'n'
        DataBinding.FieldName = 'CO_DESCRIP'
        MinWidth = 100
        Options.Grouping = False
        Width = 100
      end
      object CO_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'CO_TIPO'
        MinWidth = 70
        Width = 70
      end
      object MG_FIJO: TcxGridDBColumn
        Caption = 'Fijo'
        DataBinding.FieldName = 'MG_FIJO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
      object MG_MONTO: TcxGridDBColumn
        Caption = 'Monto'
        DataBinding.FieldName = 'MG_MONTO'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        MinWidth = 90
        Options.Grouping = False
        Width = 90
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Modific'#243
        DataBinding.FieldName = 'US_CODIGO'
        MinWidth = 70
        Options.Grouping = False
        Width = 70
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end