unit FNomExcepMontos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, ExtCtrls, StdCtrls, Buttons,
     ZBaseConsulta,
     ZetaDBGrid, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
  Mask, ZetaFecha;
type
  TNomExcepMontos_DevEx = class(TBaseGridLectura_DevEx)
    Panel1: TPanel;
    BtnModificarTodos: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure BtnModificarTodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function PuedeModificarTodos( var sMensaje: String ): Boolean;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomExcepMontos_DevEx: TNomExcepMontos_DevEx;

implementation

uses DNomina,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo, DCliente, FTressShell;

{$R *.DFM}

{ TNomExcepMontos }

procedure TNomExcepMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stPeriodo;
     HelpContext := H30322_Montos;
end;

procedure TNomExcepMontos_DevEx.Connect;
begin
     with dmNomina do
     begin
          cdsExcepMontos.Conectar;
          DataSource.DataSet := cdsExcepMontos;
     end;
end;

procedure TNomExcepMontos_DevEx.Refresh;
begin
     with dmNomina do
     begin
          cdsExcepMontos.Refrescar;
     end;
end;

function TNomExcepMontos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar', True );
     end;
     if Result then
     begin
          if not ( ZetaDbGridDBTableView.DataController.DataSet.Eof ) then
          begin
               Result := dmNomina.ValidaStatusEmpleado( ZetaDbGridDBTableView.DataController.DataSet.FieldByName('CB_CODIGO').Value , sMensaje );
          end;
     end;
end;

function TNomExcepMontos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar', True );
     end;
     if Result then
     begin
          if not ( ZetaDbGridDBTableView.DataController.DataSet.Eof ) then
          begin
               Result := dmNomina.ValidaStatusEmpleado( ZetaDbGridDBTableView.DataController.DataSet.FieldByName('CB_CODIGO').Value , sMensaje );
          end;
     end;
end;

function TNomExcepMontos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
     if Result then
     begin
          if not ( ZetaDbGridDBTableView.DataController.DataSet.Eof ) then
          begin
               Result := dmNomina.ValidaStatusEmpleado( ZetaDbGridDBTableView.DataController.DataSet.FieldByName('CB_CODIGO').Value , sMensaje );
          end;
     end;
end;

function TNomExcepMontos_DevEx.PuedeModificarTodos(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
end;

procedure TNomExcepMontos_DevEx.Agregar;
begin
     dmNomina.cdsExcepMontos.Agregar;
end;

procedure TNomExcepMontos_DevEx.Borrar;
begin
     dmNomina.cdsExcepMontos.Borrar;
end;

procedure TNomExcepMontos_DevEx.Modificar;
begin
     dmNomina.cdsExcepMontos.Modificar;
end;

procedure TNomExcepMontos_DevEx.BtnModificarTodosClick(Sender: TObject);
var
   sMensaje: String;
begin
     if PuedeModificarTodos( sMensaje ) then
        dmNomina.EditarGridMontos( False )
     else
        zInformation( Caption, sMensaje, 0 );
end;

procedure TNomExcepMontos_DevEx.FormShow(Sender: TObject);
begin
   CreaColumaSumatoria(ZetaDbGridDBTableView.Columns[0],0,'', SkCount );
  inherited;
    ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := true;
  ZetaDBGridDBTableView.OptionsView.GroupByBox := true;

end;

end.
