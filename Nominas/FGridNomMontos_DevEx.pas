unit FGridNomMontos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, Grids, DBGrids, DBCtrls, Buttons, ExtCtrls, StdCtrls,
     ZetaDBGrid, ZetaSmartLists, ZBaseGridEdicion_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  ImgList, cxNavigator, cxDBNavigator, cxButtons, cxLabel, cxBarEditItem;
type
  TGridNomMontos_DevEx = class(TBaseGridEdicion_DevEx)
    CbOperacion: TdxBarCombo;
    cxBarEditItem1: TcxBarEditItem;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure BuscaEmpleado;
    procedure BuscaConcepto;
  protected
    { Protected declarations }
    procedure Buscar; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  GridNomMontos_DevEx: TGridNomMontos_DevEx;

implementation

uses DNomina,
     DCliente,
     DCatalogos,
     ZetaCommonClasses,
     ZetaBuscaEmpleado_DevEx;

{$R *.DFM}

{ TGridNomMontos }

procedure TGridNomMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33222_Excepciones_montos_todos;
     cbOperacion.ItemIndex := dmNomina.OperacionConflicto; // Default Anterior
end;

procedure TGridNomMontos_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmNomina do
     begin
          cdsExcepMontos.Refrescar;
          DataSource.DataSet:= cdsExcepMontos;
     end;
end;

procedure TGridNomMontos_DevEx.Buscar;
begin
     with ZetaDBGrid do
     begin
          if ( SelectedField.FieldName = 'CB_CODIGO' ) then
             BuscaEmpleado
          else
              if ( SelectedField.FieldName = 'CO_NUMERO' ) then
                 BuscaConcepto;
     end;
end;

procedure TGridNomMontos_DevEx.BuscaEmpleado;
var
   sKey, sDescription: String;
begin
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo( '', sKey, sDescription ) then
     begin
          with dmNomina.cdsExcepMontos do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CB_CODIGO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridNomMontos_DevEx.BuscaConcepto;
var
   sKey, sDescription: String;
begin
     if dmCatalogos.cdsConceptos.Search_DevEx( 'CO_NUMERO < 1000', sKey, sDescription ) then
     begin
          with dmNomina.cdsExcepMontos do
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               FieldByName( 'CO_NUMERO' ).AsString := sKey;
          end;
     end;
end;

procedure TGridNomMontos_DevEx.EscribirCambios;
begin
     dmNomina.OperacionConflicto := cbOperacion.ItemIndex;
     inherited EscribirCambios;
end;


end.
