inherited LiquidacionGlobal: TLiquidacionGlobal
  Left = 255
  Top = 221
  Caption = 'Liquidaci'#243'n Global de Finiquitos'
  ClientHeight = 326
  ClientWidth = 439
  PixelsPerInch = 96
  TextHeight = 13
  inherited Wizard: TZetaWizard
    Top = 290
    Width = 439
    inherited Anterior: TZetaWizardButton
      Enabled = False
    end
    inherited Salir: TZetaWizardButton
      Left = 349
    end
    inherited Ejecutar: TZetaWizardButton
      Left = 266
    end
  end
  inherited PageControl: TPageControl
    Width = 439
    Height = 290
    ActivePage = Parametros
    inherited Parametros: TTabSheet
      object Label1: TLabel
        Left = 11
        Top = 53
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 80
        Width = 431
        Height = 183
        Caption = ' N'#243'mina Activa '
        TabOrder = 0
        object iNumeroNomina: TZetaTextBox
          Left = 65
          Top = 28
          Width = 65
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object iMesNomina: TZetaTextBox
          Left = 65
          Top = 52
          Width = 65
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sDescripcion: TZetaTextBox
          Left = 139
          Top = 28
          Width = 255
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FechaInicial: TZetaTextBox
          Left = 65
          Top = 76
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object FechaFinal: TZetaTextBox
          Left = 65
          Top = 100
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object sStatusNomina: TZetaTextBox
          Left = 65
          Top = 124
          Width = 200
          Height = 17
          AutoSize = False
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
        end
        object Label4: TLabel
          Left = 27
          Top = 126
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object Label2: TLabel
          Left = 48
          Top = 102
          Width = 12
          Height = 13
          Alignment = taRightJustify
          Caption = 'Al:'
        end
        object Label3: TLabel
          Left = 37
          Top = 54
          Width = 23
          Height = 13
          Alignment = taRightJustify
          Caption = 'Mes:'
        end
        object PeriodoNumeroLBL: TLabel
          Left = 20
          Top = 30
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero:'
        end
        object Label5: TLabel
          Left = 41
          Top = 78
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'Del:'
        end
      end
      object GRTipo: TRadioGroup
        Left = 0
        Top = 0
        Width = 431
        Height = 41
        Align = alTop
        Caption = 'Tipo'
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Liquidaci'#243'n'
          'Indemnizaci'#243'n')
        TabOrder = 1
        OnClick = GRTipoClick
      end
      object Observaciones: TZetaEdit
        Left = 91
        Top = 49
        Width = 321
        Height = 21
        TabOrder = 2
        Text = 'Observaciones'
      end
    end
    object TabSheet1: TTabSheet [1]
      Caption = 'TabSheet1'
      ImageIndex = 4
      object GroupBox3: TGroupBox
        Left = 96
        Top = 12
        Width = 257
        Height = 201
        Caption = ' '#191' Si ya existe la Simulaci'#243'n ? '
        TabOrder = 0
        object GrSimAplicadas: TRadioGroup
          Left = 2
          Top = 101
          Width = 253
          Height = 80
          Align = alTop
          Caption = ' Para las simulaciones Aplicadas '
          ItemIndex = 0
          Items.Strings = (
            'Dejar la Anterior'
            'Encimar Nueva')
          TabOrder = 0
        end
        object GrSimAprobadas: TRadioGroup
          Left = 2
          Top = 19
          Width = 253
          Height = 82
          Align = alTop
          Caption = ' Para las simulaciones Aprobadas '
          ItemIndex = 0
          Items.Strings = (
            'Dejar la Anterior'
            'Encimar Nueva')
          TabOrder = 1
        end
        object Panel1: TPanel
          Left = 2
          Top = 15
          Width = 253
          Height = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
        end
      end
      object GroupBox4: TGroupBox
        Left = 96
        Top = 218
        Width = 257
        Height = 47
        Caption = '                                         '
        TabOrder = 1
        object IncluirBajas: TCheckBox
          Left = 14
          Top = 0
          Width = 114
          Height = 17
          Caption = 'Incluir Bajas desde: '
          TabOrder = 0
          OnClick = IncluirBajasClick
        end
        object FechaBaja: TZetaFecha
          Left = 34
          Top = 19
          Width = 115
          Height = 22
          Cursor = crArrow
          Enabled = False
          TabOrder = 1
          Text = '12/mar/09'
          Valor = 39884.000000000000000000
        end
      end
    end
    inherited FiltrosCondiciones: TTabSheet
      inherited FiltrosGB: TGroupBox
        Left = 10
        inherited Seleccionar: TBitBtn
          Visible = True
        end
      end
    end
    inherited Ejecucion: TTabSheet
      inherited Advertencia: TMemo
        Width = 431
        Lines.Strings = (
          'Advertencia'
          
            'Se va registrar la liquidaci'#243'n global de para los empleados indi' +
            'cados'
          'Presione '#39'Ejecutar'#39' para iniciar el proceso')
      end
      inherited ProgressPanel: TPanel
        Top = 212
        Width = 431
        inherited ProgressBar: TProgressBar
          Left = 32
        end
      end
    end
  end
end
