unit FFaltaRepetida_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ZetaDBTextBox, ZetaCommonLists, Mask, ZetaNumero,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxButtons, dxGDIPlusClasses;

type

  TFaltaRepetida_DevEx = class(TForm)
    Mensaje: TLabel;
    Image1: TImage;
    IgnoreBtn: TcxButton;
    ReplaceBtn: TcxButton;
    SumarBtn: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    EmpleadoLb: TLabel;
    TipoLb: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    AnteriorNu: TZetaNumero;
    NuevoNu: TZetaNumero;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

    function ShowDlgFaltaRepetida( const iEmpleado, iMotivo: Integer;
             const sDiaHora: String; const Nuevo, Anterior: Currency ): eOperacionConflicto;

var
  FaltaRepetida_DevEx: TFaltaRepetida_DevEx;

implementation

uses ZetaCommonClasses;

{$R *.DFM}

procedure TFaltaRepetida_DevEx.FormCreate(Sender: TObject);
begin
     HelpContext:= 0;
end;

function ShowDlgFaltaRepetida( const iEmpleado, iMotivo: Integer;
             const sDiaHora: String; const Nuevo, Anterior: Currency ): eOperacionConflicto;
begin
     if ( FaltaRepetida_DevEx = nil ) then
     begin
          FaltaRepetida_DevEx := TFaltaRepetida_DevEx.Create( Application.MainForm ); // Se destruye al Salir del Programa //
     end;

     with FaltaRepetida_DevEx do
     begin
          EmpleadoLb.Caption:= IntToStr( iEmpleado );
          if ( sDiaHora = K_TIPO_HORA ) then
             TipoLb.Caption:= ZetaCommonLists.ObtieneElemento( lfMotivoFaltaHoras, iMotivo )
          else
             TipoLb.Caption:= ZetaCommonLists.ObtieneElemento( lfMotivoFaltaDias, iMotivo );
          AnteriorNu.Valor:=  Anterior;
          NuevoNu.Valor:= Nuevo;
          ShowModal;
          case ModalResult of
               mrOk:  Result:= ocSustituir;
               mrYes: Result:= ocSumar;
          else Result:= ocIgnorar;
          end;
     end;
end;

procedure TFaltaRepetida_DevEx.FormShow(Sender: TObject);
begin
     IgnoreBtn.SetFocus;
end;

end.
