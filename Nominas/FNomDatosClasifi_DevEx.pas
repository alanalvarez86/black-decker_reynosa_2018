unit FNomDatosClasifi_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, StdCtrls, ExtCtrls, ComCtrls, DBCtrls,
     ZBaseConsulta, ZetaDBTextBox, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, dxSkinscxPCPainter,
     cxPCdxBarPopupMenu, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, cxPC, cxContainer, cxEdit, cxGroupBox, Menus,
     cxButtons, dxBarBuiltInMenu;

type
  TNomDatosClasifi_DevEx = class(TBaseConsulta)
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cb_turno: TZetaDBTextBox;
    cb_puesto: TZetaDBTextBox;
    cb_clasifi: TZetaDBTextBox;
    Turno: TZetaDBTextBox;
    Puesto: TZetaDBTextBox;
    Clasifi: TZetaDBTextBox;
    Label4: TLabel;
    cb_patron: TZetaDBTextBox;
    Patron: TZetaDBTextBox;
    Label6: TLabel;
    NO_OBSERVA: TZetaDBTextBox;
    Label5: TLabel;
    CB_ZONA_GE: TZetaDBTextBox;
    Label12: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label33: TLabel;
    NO_FUERA: TDBCheckBox;
    Label7: TLabel;
    NO_LIQUIDA: TZetaDBTextBox;
    TabSheet3: TcxTabSheet;
    GroupBox1: TcxGroupBox;
    Label8: TLabel;
    NO_FOLIO_1: TZetaDBTextBox;
    Label9: TLabel;
    NO_FOLIO_2: TZetaDBTextBox;
    Label10: TLabel;
    NO_FOLIO_3: TZetaDBTextBox;
    NO_FOLIO_4: TZetaDBTextBox;
    Label11: TLabel;
    Label13: TLabel;
    NO_FOLIO_5: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label14: TLabel;
    NO_FEC_PAG: TZetaDBTextBox;
    Label15: TLabel;
    NO_USR_PAG: TZetaDBTextBox;
    NO_STATUS: TZetaDBTextBox;
    ZNIVEL1: TZetaTextBox;
    ZNIVEL2: TZetaTextBox;
    ZNIVEL3: TZetaTextBox;
    ZNIVEL4: TZetaTextBox;
    ZNIVEL5: TZetaTextBox;
    ZNIVEL6: TZetaTextBox;
    ZNIVEL7: TZetaTextBox;
    ZNIVEL8: TZetaTextBox;
    ZNIVEL9: TZetaTextBox;
    bbMostrarCalendario: TcxButton;
    lblZNivel0: TLabel;
    ZNIVEL0: TZetaTextBox;
    CodNIVEL0: TZetaTextBox;
    ChkDepositoBanca: TCheckBox;
    Label16: TLabel;
    CB_NIVEL10lbl: TLabel;
    ZNIVEL10: TZetaTextBox;
    CB_NIVEL11lbl: TLabel;
    ZNIVEL11: TZetaTextBox;
    ZNIVEL12: TZetaTextBox;
    CB_NIVEL12lbl: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    CB_BAN_ELE: TZetaDBTextBox;
    CB_CTA_GAS: TZetaDBTextBox;
    CB_CTA_VAL: TZetaDBTextBox;
    Label21: TLabel;
    NO_PAGO: TZetaDBTextBox;
    SSocial: TZetaDBTextBox;
    cb_tablass: TZetaDBTextBox;
    Label22: TLabel;
    Label23: TLabel;
    NO_TIMBRO: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendarioClick(Sender: TObject);
  private
    procedure SetCamposNivel;
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomDatosClasifi_DevEx: TNomDatosClasifi_DevEx;

implementation

uses DNomina,
     DTablas,
     DGlobal,
     DCatalogos,
     DSistema,
     DCliente,
     ZGlobalTress,
     ZetaClientTools,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses;

{$R *.DFM}

{ ************** TNomDatosClasifi ************** }

procedure TNomDatosClasifi_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     ZNIVEL10.Visible := True;
     ZNIVEL11.Visible := True;
     ZNIVEL12.Visible := True;     
     {$endif}
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     HelpContext := H30315_Clasificacion;
     SetCamposNivel;
end;

procedure TNomDatosClasifi_DevEx.Connect;
begin
     with dmTablas do
     begin
          if ZNIVEL1.Visible then cdsNivel1.Conectar;
          if ZNIVEL2.Visible then cdsNivel2.Conectar;
          if ZNIVEL3.Visible then cdsNivel3.Conectar;
          if ZNIVEL4.Visible then cdsNivel4.Conectar;
          if ZNIVEL5.Visible then cdsNivel5.Conectar;
          if ZNIVEL6.Visible then cdsNivel6.Conectar;
          if ZNIVEL7.Visible then cdsNivel7.Conectar;
          if ZNIVEL8.Visible then cdsNivel8.Conectar;
          if ZNIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}{OP: 5.Ago.08}
          if ZNIVEL10.Visible then cdsNivel10.Conectar;
          if ZNIVEL11.Visible then cdsNivel11.Conectar;
          if ZNIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsSSocial.Conectar;
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
     end;
     with dmSistema do
     begin
          ZNivel0.Visible := HayNivel0;
          CodNIVEL0.Visible := ZNivel0.Visible;
          lblZNivel0.Visible := ZNivel0.Visible;
          if( ZNivel0.Visible )then
          begin
               cdsNivel0.Conectar;
          end;
          cdsUsuarios.Conectar;
     end;
     with dmNomina do
     begin
          cdsDatosClasifi.Conectar;
          DataSource.DataSet := cdsDatosClasifi;
     end;
end;

procedure TNomDatosClasifi_DevEx.Refresh;
begin
     dmNomina.cdsDatosClasifi.Refrescar;
end;

function TNomDatosClasifi_DevEx.PuedeAgregar( var sMensaje: String ): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          if not NoHayDatos then
          begin
               sMensaje := 'Este Empleado Ya Tiene N�mina En Este Per�odo';
               Result := False;
          end
          else
              if dmCliente.NominaYaAfectada then
              begin
                   sMensaje := 'No Se Puede Agregar A Una N�mina Ya Afectada';
                   Result := False;
              end;
     end;
end;

procedure TNomDatosClasifi_DevEx.Agregar;
begin
     dmNomina.cdsDatosClasifi.Agregar;
end;

function TNomDatosClasifi_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          if dmCliente.NominaYaAfectada then
          begin
               sMensaje := 'No Se Puede Borrar Una N�mina Ya Afectada';
               Result := False;
          end;
     end;
end;

procedure TNomDatosClasifi_DevEx.Borrar;
begin
     dmNomina.cdsDatosClasifi.Borrar;
end;

function TNomDatosClasifi_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          if dmCliente.NominaYaAfectada then
          begin
               sMensaje := 'No Se Puede Modificar Una N�mina Ya Afectada';
               Result := False;
          end;
     end;
end;

procedure TNomDatosClasifi_DevEx.Modificar;
begin
     dmNomina.cdsDatosClasifi.Modificar;
end;

procedure TNomDatosClasifi_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
var
   sNivel0: String;
begin
     inherited;
     with dmNomina.cdsDatosClasifi do
     begin
          if ZNIVEL1.Visible then ZNIVEL1.Caption:= dmTablas.cdsNivel1.GetDescripcion( FieldByName( 'CB_NIVEL1' ).AsString );
          if ZNIVEL2.Visible then ZNIVEL2.Caption:= dmTablas.cdsNivel2.GetDescripcion( FieldByName( 'CB_NIVEL2' ).AsString );
          if ZNIVEL3.Visible then ZNIVEL3.Caption:= dmTablas.cdsNivel3.GetDescripcion( FieldByName( 'CB_NIVEL3' ).AsString );
          if ZNIVEL4.Visible then ZNIVEL4.Caption:= dmTablas.cdsNivel4.GetDescripcion( FieldByName( 'CB_NIVEL4' ).AsString );
          if ZNIVEL5.Visible then ZNIVEL5.Caption:= dmTablas.cdsNivel5.GetDescripcion( FieldByName( 'CB_NIVEL5' ).AsString );
          if ZNIVEL6.Visible then ZNIVEL6.Caption:= dmTablas.cdsNivel6.GetDescripcion( FieldByName( 'CB_NIVEL6' ).AsString );
          if ZNIVEL7.Visible then ZNIVEL7.Caption:= dmTablas.cdsNivel7.GetDescripcion( FieldByName( 'CB_NIVEL7' ).AsString );
          if ZNIVEL8.Visible then ZNIVEL8.Caption:= dmTablas.cdsNivel8.GetDescripcion( FieldByName( 'CB_NIVEL8' ).AsString );
          if ZNIVEL9.Visible then ZNIVEL9.Caption:= dmTablas.cdsNivel9.GetDescripcion( FieldByName( 'CB_NIVEL9' ).AsString );
          {$ifdef ACS}{OP: 5.Ago.08}
          if ZNIVEL10.Visible then ZNIVEL10.Caption:= dmTablas.cdsNivel10.GetDescripcion( FieldByName( 'CB_NIVEL10' ).AsString );
          if ZNIVEL11.Visible then ZNIVEL11.Caption:= dmTablas.cdsNivel11.GetDescripcion( FieldByName( 'CB_NIVEL11' ).AsString );
          if ZNIVEL12.Visible then ZNIVEL12.Caption:= dmTablas.cdsNivel12.GetDescripcion( FieldByName( 'CB_NIVEL12' ).AsString );
          {$endif}
          if ZNIVEL0.Visible then
          begin
               sNivel0 := FieldByName('CB_NIVEL0').AsString;
               CodNIVEL0.Caption := sNivel0;
               ZNIVEL0.Caption:= dmSistema.cdsNivel0.GetDescripcion( sNivel0 );
          end;
          bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
          ChkDepositoBanca.Checked := ZetaCommonTools.StrLleno( FieldByName( 'CB_BAN_ELE' ).AsString );
     end;
end;

procedure TNomDatosClasifi_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, ZNIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, ZNIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, ZNIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, ZNIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, ZNIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, ZNIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, ZNIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, ZNIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, ZNIVEL9 );
     {$ifdef ACS}{OP: 5.Ago.08}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, ZNIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, ZNIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, ZNIVEL12 );
     {$endif}
end;

procedure TNomDatosClasifi_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

end.
