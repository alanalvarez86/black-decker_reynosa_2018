inherited CosteoTransferencias: TCosteoTransferencias
  Left = 170
  Top = 277
  Caption = 'Transferencias'
  ClientHeight = 505
  ClientWidth = 979
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 979
    inherited Slider: TSplitter
      Left = 257
    end
    inherited ValorActivo1: TPanel
      Width = 241
    end
    inherited ValorActivo2: TPanel
      Left = 260
      Width = 719
    end
  end
  object GridAutorizaciones: TZetaDBGrid [1]
    Left = 0
    Top = 249
    Width = 979
    Height = 256
    Align = alClient
    DataSource = DataSource
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'AU_FECHA'
        Title.Caption = 'Fecha'
        Width = 70
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CB_CODIGO'
        Title.Caption = 'N'#250'mero'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRETTYNAME'
        Title.Caption = 'Nombre Completo'
        Width = 192
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CC_ORIGEN'
        Title.Caption = 'Origen'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CC_CODIGO'
        Title.Caption = 'Destino'
        Width = 55
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'TR_HORAS'
        Title.Caption = 'Horas'
        Width = 55
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'ASIS_HORAS'
        Title.Caption = 'Asistencia'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TR_TIPO'
        Title.Caption = 'Tipo'
        Width = 61
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TR_MOTIVO'
        Title.Caption = 'Motivo'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TR_STATUS'
        Title.Caption = 'Status'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Caption = 'Transfiere'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TR_APRUEBA'
        Title.Caption = 'Recibe'
        Width = 120
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TR_GLOBAL'
        Title.Caption = #191'Global?'
        Width = 50
        Visible = True
      end>
  end
  object pnFiltros: TPanel [2]
    Left = 0
    Top = 19
    Width = 979
    Height = 230
    Align = alTop
    TabOrder = 1
    object lblFecIni: TLabel
      Left = 50
      Top = 13
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha inicial:'
    end
    object lblFecFin: TLabel
      Left = 57
      Top = 37
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Fecha final:'
    end
    object Label1: TLabel
      Left = 24
      Top = 61
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transferido desde:'
    end
    object lblMaestro: TLabel
      Left = 47
      Top = 85
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transferido a:'
    end
    object Label2: TLabel
      Left = 77
      Top = 109
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Motivo:'
    end
    object Label3: TLabel
      Left = 88
      Top = 139
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo:'
    end
    object Label4: TLabel
      Left = 72
      Top = 174
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Captura:'
    end
    object zFechaIni: TZetaFecha
      Left = 115
      Top = 8
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 0
      Text = '27/Jan/04'
      Valor = 38013.000000000000000000
    end
    object zFechaFin: TZetaFecha
      Left = 115
      Top = 32
      Width = 115
      Height = 22
      Cursor = crArrow
      TabOrder = 1
      Text = '27/Jan/04'
      Valor = 38013.000000000000000000
    end
    object CCOrigen: TZetaKeyLookup
      Left = 115
      Top = 57
      Width = 350
      Height = 21
      EditarSoloActivos = True
      TabOrder = 2
      TabStop = True
      WidthLlave = 70
    end
    object CCDestino: TZetaKeyLookup
      Left = 115
      Top = 81
      Width = 350
      Height = 21
      EditarSoloActivos = True
      TabOrder = 3
      TabStop = True
      WidthLlave = 70
    end
    object btnFiltrar: TBitBtn
      Left = 478
      Top = 60
      Width = 97
      Height = 46
      Caption = '&Filtrar'
      TabOrder = 8
      OnClick = btnFiltrarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777777777777
        777777777777777770000070000077777000E00BFBFB07777777E0BFBF000777
        7700E0FBFBFBF0777700E0BFBF0000007777E0FBFBFBFBFB0799E0BF00000000
        7799000BFB077777777777700077777777007777777777777700777777777777
        7777777777777777700077777777777770007777777777777777}
    end
    object TR_MOTIVO: TZetaKeyLookup
      Left = 115
      Top = 105
      Width = 350
      Height = 21
      TabOrder = 4
      TabStop = True
      WidthLlave = 70
    end
    object rgTipo: TRadioGroup
      Left = 115
      Top = 129
      Width = 350
      Height = 33
      Columns = 3
      Items.Strings = (
        'Todos'
        'Ordinarias'
        'Extras')
      TabOrder = 5
    end
    object cbSoloNegativos: TCheckBox
      Left = 115
      Top = 204
      Width = 246
      Height = 17
      Caption = 'Mostrar Transferencias mayores a la Asistencia'
      TabOrder = 7
    end
    object rgGlobales: TRadioGroup
      Left = 115
      Top = 164
      Width = 350
      Height = 33
      Columns = 3
      Items.Strings = (
        'Todas'
        'Individuales'
        'Globales')
      TabOrder = 6
    end
  end
  inherited DataSource: TDataSource
    Left = 696
    Top = 5
  end
end
