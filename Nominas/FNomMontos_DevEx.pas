unit FNomMontos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, ComCtrls, DBCtrls, ZetaDBTextBox,
     ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
     cxGridDBTableView, Menus, ActnList, ImgList, cxGridLevel, cxClasses,
     cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons, cxContainer, cxGroupBox,
     cxPCdxBarPopupMenu, cxPC, ExtCtrls;

type
  TNomMontos_DevEx = class(TBaseGridLectura_DevEx)
    PageControl1: TcxPageControl;
    dsMovimiento: TDataSource;
    TabSheet1: TcxTabSheet;
    TabSheet3: TcxTabSheet;
    Splitter1: TSplitter;
    GroupBox1: TcxGroupBox;
    Label6: TLabel;
    NO_PER_MEN: TZetaDBTextBox;
    Label9: TLabel;
    NO_PER_CAL: TZetaDBTextBox;
    Label4: TLabel;
    NO_TOT_PRE: TZetaDBTextBox;
    GroupBox2: TcxGroupBox;
    Label5: TLabel;
    NO_X_ISPT: TZetaDBTextBox;
    Label10: TLabel;
    NO_X_CAL: TZetaDBTextBox;
    Label7: TLabel;
    NO_X_MENS: TZetaDBTextBox;
    Label8: TLabel;
    NO_IMP_CAL: TZetaDBTextBox;
    GroupBox3: TcxGroupBox;
    Label13: TLabel;
    CB_SALARIO: TZetaDBTextBox;
    Label19: TLabel;
    CB_SAL_INT: TZetaDBTextBox;
    TabSheet2: TcxTabSheet;
    Label33: TLabel;
    NO_STATUS: TZetaDBTextBox;
    US_CODIGO: TZetaDBTextBox;
    Label12: TLabel;
    Label11: TLabel;
    NO_OBSERVA: TZetaDBTextBox;
    GroupBox4: TcxGroupBox;
    Label1: TLabel;
    NO_PERCEPC: TZetaDBTextBox;
    Label2: TLabel;
    NO_DEDUCCI: TZetaDBTextBox;
    NO_NETO: TZetaDBTextBox;
    Label3: TLabel;
    Label20: TLabel;
    NO_FUERA: TDBCheckBox;
    Label21: TLabel;
    NO_LIQUIDA: TZetaDBTextBox;
    Calcular: TcxButton;
    TabHoras: TcxTabSheet;
    TabDias: TcxTabSheet;
    Label17: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    NO_DIAS: TZetaDBTextBox;
    NO_DIAS_AS: TZetaDBTextBox;
    NO_DIAS_NT: TZetaDBTextBox;
    NO_DIAS_SU: TZetaDBTextBox;
    Label14: TLabel;
    Label23: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    NO_DIAS_SG: TZetaDBTextBox;
    NO_DIAS_CG: TZetaDBTextBox;
    NO_DIAS_OT: TZetaDBTextBox;
    Label29: TLabel;
    Label30: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    NO_DIAS_RE: TZetaDBTextBox;
    NO_DIAS_SS: TZetaDBTextBox;
    NO_DIAS_EM: TZetaDBTextBox;
    Label15: TLabel;
    Label16: TLabel;
    Label38: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    NO_DIAS_FJ: TZetaDBTextBox;
    NO_DIAS_FV: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    NO_DIAS_AG: TZetaDBTextBox;
    Label34: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label35: TLabel;
    NO_Hora_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label36: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label37: TLabel;
    Label39: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label40: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label41: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label42: TLabel;
    Label43: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label44: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label45: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label46: TLabel;
    Label47: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    BtnModificarTodos: TcxButton;
    CalcularAVENT: TcxButton;
    Label48: TLabel;
    NO_PER_ISN: TZetaDBTextBox;
    co_numero: TcxGridDBColumn;
    DESCRIPCION: TcxGridDBColumn;
    MO_PERCEPC: TcxGridDBColumn;
    MO_DEDUCCI: TcxGridDBColumn;
    CO_TIPO: TcxGridDBColumn;
    MO_ACTIVO: TcxGridDBColumn;
    US_CODIGO_GRID: TcxGridDBColumn;
    MO_REFEREN: TcxGridDBColumn;
    MO_X_ISPT: TcxGridDBColumn;
    MO_PER_CAL: TcxGridDBColumn;
    mo_imp_cal: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure CalcularClick(Sender: TObject);
    procedure BtnModificarTodosClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure HabilitaControl;    
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function NoHayDatos: Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomMontos_DevEx: TNomMontos_DevEx;

implementation

uses DNomina,
     DCatalogos,
     DCliente,
     DSistema,
     DProcesos,
     DGlobal,
     FTressShell,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaDialogo,
     ZAccesosMgr,
     ZGlobalTress,
     ZAccesosTress,
     ZetaSystemWorking;

{$R *.DFM}

{ TNomMontos }

procedure TNomMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     HelpContext:= H30313_Montos_nomina;
end;

procedure TNomMontos_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     Calcular.Enabled := CheckDerecho( D_NOM_PROC_CALCULAR, K_DERECHO_CONSULTA );
     //Calcular.Enabled := RevisaCualquiera( D_NOM_PROC_CALCULAR );
     HabilitaControl;
end;

procedure TNomMontos_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          //cdsMontos.Conectar;
          cdsMovMontos.Conectar;
          DataSource.DataSet:= cdsMontos;
          dsMovimiento.DataSet:= cdsMovMontos;
     end;
end;

procedure TNomMontos_DevEx.Refresh;
begin
     with dmNomina do
     begin
          //cdsMontos.Refrescar;
          cdsMovMontos.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNomMontos_DevEx.CalcularClick(Sender: TObject);
var
   oCursor: TCursor;
   bAfectadoEmp: Boolean;
   sMensaje: String;
begin
     sMensaje := VACIO;
     bAfectadoEmp := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     if ( bAfectadoEmp ) then
     begin
          inherited;
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          try
             InitAnimation( 'Calculando N�mina' );
             dmCatalogos.cdsConceptos.Refrescar;
             dmProcesos.CalcularNominaEmpleado;
          finally
                 EndAnimation;
                 TressShell.ReconectaMenu;
                 Screen.Cursor := oCursor;
          end;
     end
     else
     begin
          sMensaje := '� Se Encontr� Un Error !' + CR_LF + 'La N�mina ya fu� Afectada.' + CR_LF + 'No se puede volver a Calcular';
          zError( 'Error en Tress', sMensaje, 0 );
     end;
     {PENDIENTE tambi�n c�mo hacerle para que el checkbox NO_FUERA
     { sea ReadOnly sin cambiar la propiedad como en 1.3.}
end;

function TNomMontos_DevEx.NoHayDatos: Boolean;
begin
     Result := NoData( dsMovimiento );
end;

function TNomMontos_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.BajaNominaAnterior( dmCliente.cdsEmpleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.TipoNominaDiferente( dmCliente.cdsEmpleado , sMensaje );
     end;
end;

procedure TNomMontos_DevEx.Agregar;
begin
     dmNomina.cdsMovMontos.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TNomMontos_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
end;

procedure TNomMontos_DevEx.Borrar;
begin
     dmNomina.cdsMovMontos.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TNomMontos_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result := inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.BajaNominaAnterior( dmCliente.cdsEmpleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.TipoNominaDiferente( dmCliente.cdsEmpleado , sMensaje );
     end;
end;

procedure TNomMontos_DevEx.Modificar;
begin
     dmNomina.cdsMovMontos.Modificar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNomMontos_DevEx.BtnModificarTodosClick(Sender: TObject);
var
   sMensaje: String;
begin
     if NoHayDatos then
         zInformation( Caption, 'No Hay Datos Para Modificar', 0 )
     else
     begin
          if PuedeModificar( sMensaje ) then
          begin
               //ZetaDBGrid DataSource := nil;
               ZetaDBGridDBTableView.DataController.DataSource := nil;
               try
                  dmNomina.EditarGridMovMontos;
               finally
                  //ZetaDBGrid.DataSource := dsMovimiento;
                  ZetaDBGridDBTableView.DataController.DataSource := dsMovimiento;
               end;
          end
          else
             zInformation( Caption, sMensaje, 0 );
     end;
end;

procedure TNomMontos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     { No se debe llamar inherited para que no se tome otro Dataset
     { como referencia para el m�todo NoData }
     //ImgNoRecord.Visible:= NoHayDatos;   //DevEx(@am): No aplica para la Nueva imagen.
end;

procedure TNomMontos_DevEx.HabilitaControl;
begin
     Global.Conectar;
     Calcular.Visible := not Global.GetGlobalBooleano( K_GLOBAL_AVENT );
     CalcularAVENT.Visible := not Calcular.Visible;
end;

end.
