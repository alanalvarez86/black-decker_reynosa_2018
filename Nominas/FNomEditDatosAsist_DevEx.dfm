inherited NomEditDatosAsist_DevEx: TNomEditDatosAsist_DevEx
  Left = 412
  Top = 197
  Caption = 'Pre-N'#243'mina'
  ClientHeight = 382
  ClientWidth = 514
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 346
    Width = 514
    inherited OK_DevEx: TcxButton
      Left = 350
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 429
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 514
    inherited ValorActivo2: TPanel
      Width = 188
      inherited textoValorActivo2: TLabel
        Width = 182
      end
    end
  end
  object PageControl: TcxPageControl [3]
    Left = 0
    Top = 50
    Width = 514
    Height = 296
    Align = alClient
    TabOrder = 3
    Properties.ActivePage = TabSheet3
    Properties.CustomButtons.Buttons = <>
    ClientRectBottom = 294
    ClientRectLeft = 2
    ClientRectRight = 512
    ClientRectTop = 28
    object HorasTab: TcxTabSheet
      Caption = 'Horas'
      object GroupBox1: TcxGroupBox
        Left = 16
        Top = 1
        TabOrder = 0
        Height = 72
        Width = 457
        object Label20: TLabel
          Left = 56
          Top = 18
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fecha:'
        end
        object AU_FECHA: TZetaDBTextBox
          Left = 97
          Top = 16
          Width = 128
          Height = 17
          AutoSize = False
          Caption = 'AU_FECHA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'au_fecha'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label6: TLabel
          Left = 46
          Top = 36
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Posici'#243'n:'
        end
        object AU_POSICIO: TZetaDBTextBox
          Left = 97
          Top = 34
          Width = 24
          Height = 17
          AutoSize = False
          Caption = 'AU_POSICIO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_POSICIO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object US_CODIGO: TZetaDBTextBox
          Left = 281
          Top = 17
          Width = 155
          Height = 17
          AutoSize = False
          Caption = 'US_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_CODIGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label22: TLabel
          Left = 233
          Top = 19
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modific'#243':'
        end
      end
      object GroupBox2: TcxGroupBox
        Left = 16
        Top = 73
        TabOrder = 1
        Height = 128
        Width = 457
        object Label17: TLabel
          Left = 193
          Top = 22
          Width = 80
          Height = 13
          Alignment = taRightJustify
          Caption = 'Permiso c/&Goce:'
          FocusControl = AU_PER_CG
        end
        object Label18: TLabel
          Left = 194
          Top = 43
          Width = 79
          Height = 13
          Alignment = taRightJustify
          Caption = '&Permiso s/Goce:'
          FocusControl = AU_PER_SG
        end
        object Label19: TLabel
          Left = 171
          Top = 65
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = 'Descan&so Trabajado:'
          FocusControl = AU_DES_TRA
        end
        object Label16: TLabel
          Left = 238
          Top = 86
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tard&es:'
          FocusControl = AU_TARDES
        end
        object AU_EXTRAS: TZetaDBTextBox
          Left = 96
          Top = 105
          Width = 40
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_EXTRAS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_EXTRAS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label5: TLabel
          Left = 56
          Top = 86
          Width = 34
          Height = 13
          Alignment = taRightJustify
          Caption = '&Triples:'
          FocusControl = AU_TRIPLES
        end
        object Label14: TLabel
          Left = 58
          Top = 107
          Width = 32
          Height = 13
          Alignment = taRightJustify
          Caption = 'Extras:'
        end
        object Label13: TLabel
          Left = 49
          Top = 43
          Width = 41
          Height = 13
          Alignment = taRightJustify
          Caption = '&A Pagar:'
          FocusControl = AU_HORAS
        end
        object Label15: TLabel
          Left = 54
          Top = 65
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = '&Dobles:'
          FocusControl = AU_DOBLES
        end
        object Label7: TLabel
          Left = 40
          Top = 22
          Width = 50
          Height = 13
          Alignment = taRightJustify
          Caption = 'Ordinarias:'
        end
        object AU_HORASCK: TZetaDBTextBox
          Left = 96
          Top = 20
          Width = 40
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'AU_HORASCK'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'AU_HORASCK'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object AU_PER_CG: TZetaDBNumero
          Left = 280
          Top = 18
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 3
          Text = '0.00'
          DataField = 'AU_PER_CG'
          DataSource = DataSource
        end
        object AU_PER_SG: TZetaDBNumero
          Left = 280
          Top = 39
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 4
          Text = '0.00'
          DataField = 'AU_PER_SG'
          DataSource = DataSource
        end
        object AU_DES_TRA: TZetaDBNumero
          Left = 280
          Top = 61
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 5
          Text = '0.00'
          DataField = 'AU_DES_TRA'
          DataSource = DataSource
        end
        object AU_TARDES: TZetaDBNumero
          Left = 280
          Top = 82
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 6
          Text = '0.00'
          DataField = 'AU_TARDES'
          DataSource = DataSource
        end
        object AU_TRIPLES: TZetaDBNumero
          Left = 96
          Top = 82
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 2
          Text = '0.00'
          DataField = 'AU_TRIPLES'
          DataSource = DataSource
        end
        object AU_HORAS: TZetaDBNumero
          Left = 96
          Top = 39
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 0
          Text = '0.00'
          DataField = 'AU_HORAS'
          DataSource = DataSource
        end
        object AU_DOBLES: TZetaDBNumero
          Left = 96
          Top = 61
          Width = 40
          Height = 21
          Mascara = mnHoras
          TabOrder = 1
          Text = '0.00'
          DataField = 'AU_DOBLES'
          DataSource = DataSource
        end
      end
    end
    object TabSheet1: TcxTabSheet
      Caption = 'Clasificaci'#243'n'
      ImageIndex = 0
      object Label10: TLabel
        Left = 66
        Top = 103
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'T&urno:'
        FocusControl = CB_TURNO
      end
      object Label11: TLabel
        Left = 61
        Top = 125
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = '&Puesto:'
        FocusControl = CB_PUESTO
      end
      object Label12: TLabel
        Left = 35
        Top = 147
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = '&Clasificaci'#243'n:'
        FocusControl = CB_CLASIFI
      end
      object Label1: TLabel
        Left = 76
        Top = 15
        Width = 21
        Height = 13
        Alignment = taRightJustify
        Caption = '&D'#237'a:'
        FocusControl = AU_STATUS
      end
      object Label2: TLabel
        Left = 73
        Top = 37
        Width = 24
        Height = 13
        Alignment = taRightJustify
        Caption = '&Tipo:'
        FocusControl = AU_TIPODIA
      end
      object Label3: TLabel
        Left = 45
        Top = 59
        Width = 52
        Height = 13
        Alignment = taRightJustify
        Caption = '&Incidencia:'
        FocusControl = AU_TIPO
      end
      object Label4: TLabel
        Left = 60
        Top = 81
        Width = 37
        Height = 13
        Alignment = taRightJustify
        Caption = '&Horario:'
        FocusControl = HO_CODIGO
      end
      object bbMostrarCalendario: TcxButton
        Left = 401
        Top = 99
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        OnClick = bbMostrarCalendarioClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
        OptionsImage.Margin = 1
      end
      object CB_TURNO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 99
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsTurnos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_TURNO'
        DataSource = DataSource
      end
      object CB_PUESTO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 121
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsPuestos
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_PUESTO'
        DataSource = DataSource
      end
      object CB_CLASIFI: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 143
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsClasifi
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_CLASIFI'
        DataSource = DataSource
      end
      object AU_STATUS: TZetaDBKeyCombo
        Left = 100
        Top = 11
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 0
        ListaFija = lfStatusAusencia
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'AU_STATUS'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object AU_TIPODIA: TZetaDBKeyCombo
        Left = 100
        Top = 33
        Width = 145
        Height = 21
        AutoComplete = False
        BevelKind = bkFlat
        Style = csDropDownList
        Ctl3D = False
        ItemHeight = 13
        ParentCtl3D = False
        TabOrder = 1
        ListaFija = lfTipoDiaAusencia
        ListaVariable = lvPuesto
        Offset = 0
        Opcional = False
        EsconderVacios = False
        DataField = 'AU_TIPODIA'
        DataSource = DataSource
        LlaveNumerica = True
      end
      object AU_TIPO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 55
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsIncidencias
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'AU_TIPO'
        DataSource = DataSource
      end
      object HO_CODIGO: TZetaDBKeyLookup_DevEx
        Left = 100
        Top = 77
        Width = 300
        Height = 21
        LookupDataset = dmCatalogos.cdsHorarios
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'HO_CODIGO'
        DataSource = DataSource
      end
    end
    object TabSheet3: TcxTabSheet
      Caption = 'Niveles'
      object CB_NIVEL1Lbl: TLabel
        Left = 162
        Top = 4
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1'
      end
      object CB_NIVEL2Lbl: TLabel
        Left = 162
        Top = 26
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2'
      end
      object CB_NIVEL3Lbl: TLabel
        Left = 162
        Top = 48
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3'
      end
      object CB_NIVEL4Lbl: TLabel
        Left = 162
        Top = 70
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4'
      end
      object CB_NIVEL5Lbl: TLabel
        Left = 162
        Top = 92
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5'
      end
      object CB_NIVEL6Lbl: TLabel
        Left = 162
        Top = 114
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6'
      end
      object CB_NIVEL7Lbl: TLabel
        Left = 162
        Top = 136
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7'
      end
      object CB_NIVEL8Lbl: TLabel
        Left = 162
        Top = 158
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8'
      end
      object CB_NIVEL9Lbl: TLabel
        Left = 162
        Top = 180
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9'
      end
      object CB_NIVEL10Lbl: TLabel
        Left = 156
        Top = 202
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10'
        Visible = False
      end
      object CB_NIVEL11Lbl: TLabel
        Left = 156
        Top = 225
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11'
        Visible = False
      end
      object CB_NIVEL12Lbl: TLabel
        Left = 156
        Top = 249
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12'
        Visible = False
      end
      object CB_NIVEL1: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 0
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 22
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 44
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 66
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 88
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 110
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 132
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 154
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 176
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 199
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 222
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup_DevEx
        Left = 199
        Top = 245
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 404
    Top = 9
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_AgregarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_BorrarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
