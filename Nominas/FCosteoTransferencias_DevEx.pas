unit FCosteoTransferencias_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Db, ExtCtrls,  Mask, ZetaFecha, ZetaCommonClasses,
  ZBaseAsisConsulta_DevEx, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, TressMorado2013,
  cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, Menus, ActnList,
  ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid, cxTextEdit,
  Buttons, dxSkinscxPCPainter, ZetaKeyLookup_DevEx;

type
  TCosteoTransferencias_DevEx = class(TBaseAsisConsulta_DevEx)
    pnFiltros: TPanel;
    lblFecIni: TLabel;
    lblFecFin: TLabel;
    Label1: TLabel;
    lblMaestro: TLabel;
    zFechaIni: TZetaFecha;
    zFechaFin: TZetaFecha;
    CCOrigen: TZetaKeyLookup_DevEx;
    CCDestino: TZetaKeyLookup_DevEx;
    btnFiltrar: TBitBtn;
    Label2: TLabel;
    TR_MOTIVO: TZetaKeyLookup_DevEx;
    rgTipo: TRadioGroup;
    Label3: TLabel;
    cbSoloNegativos: TCheckBox;
    rgGlobales: TRadioGroup;
    Label4: TLabel;
    AU_FECHA: TcxGridDBColumn;
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    CC_ORIGEN: TcxGridDBColumn;
    CC_CODIGO: TcxGridDBColumn;
    TR_HORAS: TcxGridDBColumn;
    ASIS_HORAS: TcxGridDBColumn;
    TR_TIPO: TcxGridDBColumn;
    GROID_TR_MOTIVO: TcxGridDBColumn;
    TR_STATUS: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
    TR_APRUEBA: TcxGridDBColumn;
    TR_GLOBAL: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CosteoTransferencias_DevEx: TCosteoTransferencias_DevEx;

implementation

uses dAsistencia, dSistema, dGlobal, ZetaCommonLists, dTablas, dCliente,
     ZAccesosMgr, ZAccesosTress, ZGlobalTress, ZetaClientDataSet,
     ZBaseGridLectura_DevEx;

{$R *.DFM}

procedure TCosteoTransferencias_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Transferencias_Tress;
     zFechaIni.Valor:= dmCliente.GetDatosPeriodoActivo.Inicio;
     zFechaFin.Valor:= dmCliente.GetDatosPeriodoActivo.Fin;
     rgTipo.ItemIndex := 0;
     rgGlobales.ItemIndex := 0;


end;

procedure TCosteoTransferencias_DevEx.Connect;
 var
    oTransferencia: TZetaLookupDataset;
begin
     dmSistema.cdsUsuarios.Conectar;
     oTransferencia := dmTablas.GetDataSetTransferencia;
     oTransferencia.Conectar;
     dmTablas.cdsMotivoTransfer.Conectar;

     CCOrigen.LookupDataset := dmTablas.GetDataSetTransferencia;
     CCDestino.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;

     with dmAsistencia do
     begin
          with cdsCosteoTransferencias do
          begin
               if not Active or HayQueRefrescar then
                  DoRefresh;
          end;
          DataSource.DataSet:= cdsCosteoTransferencias;
     end;
end;

procedure TCosteoTransferencias_DevEx.Refresh;
begin
     if dmAsistencia.ParametrosTransferencias = NIL then
        dmAsistencia.ParametrosTransferencias := TZetaParams.Create(NIL);
     with dmAsistencia do
     begin
          with ParametrosTransferencias do
          begin
               AddDate( 'FechaIni', zFechaIni.Valor );
               AddDate( 'FechaFin', zFechaFin.Valor );
               AddString( 'CCOrigen', CCOrigen.Llave );
               AddString( 'CCDestino', CCDestino.Llave );
               AddString( 'Motivo', TR_MOTIVO.Llave );
               AddInteger( 'Tipo', rgTipo.ItemIndex - 1 );
               AddBoolean( 'SoloNegativos', cbSoloNegativos.Checked );
               AddInteger( 'Globales', rgGlobales.ItemIndex - 1 );
               AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
               AddInteger( 'Status', -1 ); // Por lo pronto no se filtra por Status
               AddBoolean( 'ModuloSupervisores', FALSE );
          end;
         ObtieneTransferencias( ParametrosTransferencias );
     end;

     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCosteoTransferencias_DevEx.Agregar;
begin
     dmAsistencia.cdsCosteoTransferencias.Agregar;
end;

procedure TCosteoTransferencias_DevEx.Borrar;
begin
     dmAsistencia.cdsCosteoTransferencias.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCosteoTransferencias_DevEx.Modificar;
begin
     dmAsistencia.cdsCosteoTransferencias.Modificar;
end;

procedure TCosteoTransferencias_DevEx.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

procedure TCosteoTransferencias_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
     //agrupar
     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := True;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := True;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('AU_FECHA'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();

end;

end.
