unit FGridAprobarFiniquitos_DevEx;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseGridEdicion_DevEx, DB, Grids, DBGrids, ZetaDBGrid, DBCtrls,
  ZetaSmartLists, ExtCtrls, StdCtrls,ZetaMessages, ImgList,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters,
  cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
  cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit, cxGroupBox;

type
  TGridAprobarFiniquitos_DevEx = class(TBaseGridEdicion_DevEx)
    ImageGrid: TImageList;
    GroupBox1: TcxGroupBox;
    ApagarTodos: TcxButton;
    PrenderTodos: TcxButton;
    procedure ZetaDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure ZetaDBGridCellClick(Column: TColumn);
    procedure ApagarTodosClick(Sender: TObject);
    procedure PrenderTodosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
  private
    { Private declarations }
    //procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  public
    { Public declarations }
  protected
   procedure Connect; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure EscribirCambios;override;
  end;

var
  GridAprobarFiniquitos_DevEx: TGridAprobarFiniquitos_DevEx;

implementation

uses dRecursos,
     DNomina,
     dCliente,
     dCatalogos,
     dTablas,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaCommonLists,
     FAutoClasses;

{$R *.dfm}


procedure TGridAprobarFiniquitos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := HHHHH_Aprobar_Finiquitos_Global;
end;

procedure TGridAprobarFiniquitos_DevEx.Connect;
begin
     inherited;
     with dmNomina do
     begin
          DataSource.DataSet := cdsGridAprobados;
          cdsGridAprobados.Conectar;
     end;
end;

procedure TGridAprobarFiniquitos_DevEx.Agregar;
begin
     inherited;

end;

procedure TGridAprobarFiniquitos_DevEx.Borrar;
begin
  inherited;

end;



procedure TGridAprobarFiniquitos_DevEx.ZetaDBGridCellClick(Column: TColumn);
var
   Valor: Integer;
begin
     inherited;
     with Column, DataSource.DataSet do
     begin
          if ( FieldName = 'NO_APROBA' ) then
          begin
               if not ( State in [ dsEdit, dsInsert ] ) then
                  Edit;
               Valor := FieldByName( FieldName ).AsInteger;
               if Valor = Ord(ssfSinAprobar) then
                  FieldByName( FieldName ).AsInteger := Ord(ssfAprobada)
               else
                  FieldByName( FieldName ).AsInteger := Ord(ssfSinAprobar);
               ZetaDBGrid.SelectedField := nil;
          end;
     end;
end;


procedure TGridAprobarFiniquitos_DevEx.ZetaDBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);

     function GetPosicion: Integer;
     begin
          Result := Rect.Right - Rect.Left;
          if ( Result > ImageGrid.Width ) then
             Result := Rect.Left + Round( ( Result - ImageGrid.Width ) / 2 ) + 1
          else
             Result := Rect.Left;
     end;

begin
       if ( not DataSource.DataSet.IsEmpty ) and ( Column.FieldName = 'NO_APROBA' ) and
          (  DataSource.DataSet.FieldByName( 'NO_APROBA' ).AsInteger >= 0 ) then
          ImageGrid.Draw( ZetaDBGrid.Canvas, GetPosicion, Rect.top + 1, Column.Field.AsInteger  )
       else
          ZetaDBGrid.DefaultDrawColumnCell( Rect, DataCol, Column, State );
end;


procedure TGridAprobarFiniquitos_DevEx.ApagarTodosClick(Sender: TObject);
begin
     with DataSource.DataSet do
     begin
          First;
          DisableControls;
          try
             while not Eof do
             begin
                  Edit;
                  FieldByName('NO_APROBA').AsInteger := Ord(ssfSinAprobar);
                  Next;
             end;
          Finally
                 EnableControls;
          end;
          Edit;
          HabilitaControles;
     end;
end;

procedure TGridAprobarFiniquitos_DevEx.PrenderTodosClick(Sender: TObject);
begin
     inherited;
      with DataSource.DataSet do
     begin
          First;
          DisableControls;
          try
             while not Eof do
             begin
                  Edit;
                  FieldByName('NO_APROBA').AsInteger := Ord(ssfAprobada );
                  Next;
             end;
          Finally
                 EnableControls;
          end;
          Edit;
          HabilitaControles;
     end;
end;

procedure TGridAprobarFiniquitos_DevEx.EscribirCambios;
begin
     inherited;

end;



procedure TGridAprobarFiniquitos_DevEx.ZetaDBGridTitleClick(Column: TColumn);
begin
     if (Column.FieldName <> 'NO_APROBA' )then
        ZetaDBGrid.OrdenarPor(Column);  
end;

end.
