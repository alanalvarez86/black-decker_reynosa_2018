unit FTipoExcepcion_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  cxListBox, cxButtons, ImgList;

type
  TTipoExcepcion_DevEx = class(TForm)
    BotonOk: TcxButton;
    ListAuto: TcxListBox;
    Cancelar: TcxButton;
    cxImageList24_PanelBotones: TcxImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ListAutoDblClick(Sender: TObject);
  private
    { Private declarations }
    function RenglonDia_Hor( const sTipo_Dia: String; const iTipo: Integer ): Integer;
    function RenglonExcepcion( const Indice: Integer ): Integer;
  public
    { Public declarations }
  end;

function ShowTipoExcepcion( const sDia_Hor: String; const iTipo: Integer;
                            var sNewDia_Hor: String; var iNewTipo: Integer ): Boolean;

const
     BRINCO_DIAS = 12;

var
  TipoExcepcion_DevEx: TTipoExcepcion_DevEx;

implementation

uses ZetaCommonLists, ZetaCommonClasses;

{$R *.DFM}

function ShowTipoExcepcion( const sDia_Hor: String; const iTipo: Integer;
                            var sNewDia_Hor: String; var iNewTipo: Integer ): Boolean;

begin
     if ( TipoExcepcion_DevEx = nil ) then
         TipoExcepcion_DevEx:= TTipoExcepcion_DevEx.Create( Application.MainForm );
     with TipoExcepcion_DevEx do
     begin
          ListAuto.ItemIndex:= RenglonDia_Hor( sDia_Hor, iTipo );
          ShowModal;

          if ModalResult = mrOk then
          begin
               with ListAuto do
               begin
                    if ( ItemIndex < BRINCO_DIAS ) then
                       sNewDia_Hor:= 'H'
                    else
                       sNewDia_Hor:= 'D';
                    iNewTipo:= RenglonExcepcion( ItemIndex );
               end;
               Result:= ( ( sNewDia_Hor <> sDia_Hor ) or ( iNewTipo <> iTipo ) );
          end
          else
               Result:= False;
     end;
end;

procedure TTipoExcepcion_DevEx.FormCreate(Sender: TObject);
begin
     with ListAuto do
     begin
          Clear;
          Items.Add( 'O : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhOrdinarias ) ) );
          Items.Add( 'E : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhExtras ) ) );
          Items.Add( 'R : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhRetardo ) ) );
          Items.Add( 'L : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhAdicionales ) ) );
          Items.Add( 'D : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhDomingo ) ) );
          Items.Add( '2 : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhDobles ) ) );
          Items.Add( '3 : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhTriples ) ) );
          Items.Add( 'S : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhFestivo ) ) );
          Items.Add( 'T : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhDescanso ) ) );
          Items.Add( 'U : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhVacaciones ) ) );
          Items.Add( 'X : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhConGoce ) ) );
          Items.Add( 'Y : ' + ObtieneElemento(lfMotivoFaltaHoras, Ord( mfhSinGoce ) ) );
          Items.Add( 'F : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdInjustificada ) ) );
          Items.Add( 'V : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdVacaciones ) ) );
          Items.Add( 'G : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdAguinaldo ) ) );
          Items.Add( 'N : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdNoTrabajados ) ) );
          Items.Add( 'A : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdAsistencia ) ) );
          Items.Add( 'Q : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdRetardo ) ) );
          Items.Add( 'M : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdIMSS ) ) );
          Items.Add( 'W : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdEM ) ) );
          Items.Add( 'H : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdAjuste ) ) );
          Items.Add( 'P : ' + ObtieneElemento(lfMotivoFaltaDias, Ord( mfdPrimaVacacional ) ) );
     end;
end;

procedure TTipoExcepcion_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = Chr(VK_ESCAPE) ) then
        ModalResult:= mrCancel
     else if ( Key = Chr(VK_RETURN) ) and ( ActiveControl is TListBox ) then
        ModalResult:= mrOk
     else
         with ListAuto do
         begin
              case Key of
                   'O', 'o' : ItemIndex:= 0;
                   'E', 'e' : ItemIndex:= 1;
                   'R', 'r' : ItemIndex:= 2;
                   'L', 'l' : ItemIndex:= 3;
                   'D', 'd' : ItemIndex:= 4;
                   '2'      : ItemIndex:= 5;
                   '3'      : ItemIndex:= 6;
                   'S', 's' : ItemIndex:= 7;
                   'T', 't' : ItemIndex:= 8;
                   'U', 'u' : ItemIndex:= 9;
                   'X', 'x' : ItemIndex:= 10;
                   'Y', 'y' : ItemIndex:= 11;
                   'F', 'f' : ItemIndex:= 12;
                   'V', 'v' : ItemIndex:= 13;
                   'G', 'g' : ItemIndex:= 14;
                   'N', 'n' : ItemIndex:= 15;
                   'A', 'a' : ItemIndex:= 16;
                   'Q', 'q' : ItemIndex:= 17;
                   'M', 'm' : ItemIndex:= 18;
                   'W', 'w' : ItemIndex:= 19;
                   'H', 'h' : ItemIndex:= 20;
              end;
         end;
end;

procedure TTipoExcepcion_DevEx.ListAutoDblClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

function TTipoExcepcion_DevEx.RenglonDia_Hor( const sTipo_Dia: String; const iTipo: Integer ): Integer;
begin
     Result:= 0;
     if ( sTipo_Dia = 'H' ) then
     begin
        case eMotivoFaltaHoras( iTipo ) of
             mfhOrdinarias : Result:= 0;
             mfhExtras : Result:= 1;
             mfhRetardo : Result:= 2;
             mfhAdicionales : Result:= 3;
             mfhDomingo : Result:= 4;
             mfhDobles : Result:= 5;
             mfhTriples : Result:= 6;
             mfhFestivo : Result:= 7;
             mfhDescanso : Result:= 8;
             mfhVacaciones : Result:= 9;
             mfhConGoce : Result:= 10;
             mfhSinGoce : Result:= 11;
        end;
     end
     else
     begin
         case eMotivoFaltaDias( iTipo ) of
              mfdInjustificada : Result:= 12;
              mfdVacaciones : Result:= 13;
              mfdAguinaldo : Result:= 14;
              mfdNoTrabajados : Result:= 15;
              mfdAsistencia : Result:= 16;
              mfdRetardo : Result:= 17;
              mfdIMSS : Result:= 18;
              mfdEM : Result:= 19;
              mfdAjuste : Result:= 20;
              mfdPrimaVacacional: Result := 21;
         end;
     end;
end;

function TTipoExcepcion_DevEx.RenglonExcepcion( const Indice: Integer ): Integer;
begin
     Result:= 0;
     case Indice of
          0 : Result:= Ord( mfhOrdinarias );
          1 : Result:= Ord( mfhExtras );
          2 : Result:= Ord( mfhRetardo );
          3 : Result:= Ord( mfhAdicionales );
          4 : Result:= Ord( mfhDomingo );
          5 : Result:= Ord( mfhDobles );
          6 : Result:= Ord( mfhTriples );
          7 : Result:= Ord( mfhFestivo );
          8 : Result:= Ord( mfhDescanso );
          9 : Result:= Ord( mfhVacaciones );
          10 : Result:= Ord( mfhConGoce );
          11 : Result:= Ord( mfhSinGoce );
          12 : Result:= Ord( mfdInjustificada );
          13 : Result:= Ord( mfdVacaciones );
          14 : Result:= Ord( mfdAguinaldo );
          15 : Result:= Ord( mfdNoTrabajados );
          16 : Result:= Ord( mfdAsistencia );
          17 : Result:= Ord( mfdRetardo );
          18 : Result:= Ord( mfdIMSS );
          19 : Result:= Ord( mfdEM );
          20 : Result:= Ord( mfdAjuste );
          21 : Result:= Ord( mfdPrimaVacacional );
     end;
end;

end.
