inherited NomEditDatosMontos_DevEx: TNomEditDatosMontos_DevEx
  Caption = 'Excepciones de Montos'
  ClientHeight = 317
  ClientWidth = 406
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 56
    Top = 59
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = '&Concepto:'
    FocusControl = CO_NUMERO
  end
  object Label6: TLabel [1]
    Left = 61
    Top = 259
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  object US_CODIGO: TZetaDBTextBox [2]
    Left = 106
    Top = 257
    Width = 159
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object PercepcionLbl: TLabel [3]
    Left = 48
    Top = 81
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = '&Percepci'#243'n:'
    FocusControl = MO_PERCEPC
  end
  object DeduccionLbl: TLabel [4]
    Left = 50
    Top = 102
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = '&Deducci'#243'n:'
    FocusControl = MO_DEDUCCI
  end
  object ReferenciaLbl: TLabel [5]
    Left = 3
    Top = 123
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = '&Referencia Pr'#233'stamo:'
    FocusControl = MO_REFEREN
  end
  inherited PanelBotones: TPanel
    Top = 281
    Width = 406
    inherited OK_DevEx: TcxButton
      Left = 242
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 321
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 406
    TabOrder = 8
    inherited Splitter: TSplitter
      Left = 217
    end
    inherited ValorActivo1: TPanel
      Width = 217
      inherited textoValorActivo1: TLabel
        Width = 211
      end
    end
    inherited ValorActivo2: TPanel
      Left = 220
      Width = 186
      inherited textoValorActivo2: TLabel
        Width = 180
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    TabOrder = 12
  end
  object GroupBox1: TcxGroupBox [9]
    Left = 14
    Top = 161
    Caption = ' Impuestos '
    TabOrder = 6
    Height = 92
    Width = 251
    object ExentaLbl: TLabel
      Left = 26
      Top = 18
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Parte E&xenta:'
      FocusControl = MO_X_ISPT
    end
    object MO_IMP_CALLbl: TLabel
      Left = 15
      Top = 60
      Width = 75
      Height = 13
      Alignment = taRightJustify
      Caption = 'I&SPT Individual:'
      FocusControl = MO_IMP_CAL
    end
    object MO_PER_CAL: TDBCheckBox
      Left = 40
      Top = 37
      Width = 66
      Height = 17
      Alignment = taLeftJustify
      Caption = '&Individual:'
      DataField = 'MO_PER_CAL'
      DataSource = DataSource
      TabOrder = 1
      ValueChecked = 'S'
      ValueUnchecked = 'N'
      OnClick = MO_PER_CALClick
    end
    object MO_X_ISPT: TZetaDBNumero
      Left = 93
      Top = 14
      Width = 92
      Height = 21
      Mascara = mnPesos
      TabOrder = 0
      Text = '0.00'
      DataField = 'MO_X_ISPT'
      DataSource = DataSource
    end
    object MO_IMP_CAL: TZetaDBNumero
      Left = 93
      Top = 56
      Width = 92
      Height = 21
      Mascara = mnPesos
      TabOrder = 2
      Text = '0.00'
      DataField = 'MO_IMP_CAL'
      DataSource = DataSource
    end
  end
  object MO_PERCEPC: TZetaDBNumero [10]
    Left = 108
    Top = 77
    Width = 92
    Height = 21
    Mascara = mnPesos
    TabOrder = 2
    Text = '0.00'
    DataField = 'MO_PERCEPC'
    DataSource = DataSource
  end
  object MO_DEDUCCI: TZetaDBNumero [11]
    Left = 108
    Top = 98
    Width = 92
    Height = 21
    Mascara = mnPesos
    TabOrder = 3
    Text = '0.00'
    DataField = 'MO_DEDUCCI'
    DataSource = DataSource
  end
  object MO_REFEREN: TDBEdit [12]
    Left = 108
    Top = 119
    Width = 92
    Height = 21
    DataField = 'MO_REFEREN'
    DataSource = DataSource
    TabOrder = 4
  end
  object MO_ACTIVO: TDBCheckBox [13]
    Left = 71
    Top = 143
    Width = 50
    Height = 17
    Alignment = taLeftJustify
    Caption = '&Activo:'
    DataField = 'MO_ACTIVO'
    DataSource = DataSource
    TabOrder = 5
    ValueChecked = 'S'
    ValueUnchecked = 'N'
    OnClick = MO_ACTIVOClick
  end
  object CO_NUMERO: TZetaDBKeyLookup_DevEx [14]
    Left = 108
    Top = 56
    Width = 285
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    LookupDataset = dmCatalogos.cdsConceptos
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 1
    TabStop = True
    WidthLlave = 60
    DataField = 'CO_NUMERO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 256
    Top = 97
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
    inherited dxBarControlContainerItem_DBNavigator: TdxBarControlContainerItem
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
