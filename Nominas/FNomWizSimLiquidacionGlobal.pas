unit FNomWizSimLiquidacionGlobal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseWizardFiltro, ComCtrls, ZetaKeyLookup, ZetaEdit, StdCtrls,
  Buttons, ZetaWizard, ExtCtrls, ZetaDBTextBox, Mask, ZetaFecha;

type
  TLiquidacionGlobal = class(TBaseWizardFiltro)
    TabSheet1: TTabSheet;
    GroupBox3: TGroupBox;
    GrSimAplicadas: TRadioGroup;
    GrSimAprobadas: TRadioGroup;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    iNumeroNomina: TZetaTextBox;
    iMesNomina: TZetaTextBox;
    sDescripcion: TZetaTextBox;
    FechaInicial: TZetaTextBox;
    FechaFinal: TZetaTextBox;
    sStatusNomina: TZetaTextBox;
    Label4: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PeriodoNumeroLBL: TLabel;
    Label5: TLabel;
    GroupBox4: TGroupBox;
    IncluirBajas: TCheckBox;
    FechaBaja: TZetaFecha;
    GRTipo: TRadioGroup;
    Label1: TLabel;
    Observaciones: TZetaEdit;
    procedure FormShow(Sender: TObject);
    procedure IncluirBajasClick(Sender: TObject);
    procedure GRTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

    { Private declarations }
  public
    { Public declarations }
  protected
    procedure CargaListaVerificacion;override;
    procedure CargaParametros;override;
    function EjecutarWizard: Boolean;override;
    function Verificar: Boolean;override;
  end;

var
  LiquidacionGlobal: TLiquidacionGlobal;

implementation

uses DProcesos,
     DCliente,
     ZetaCommonLists,
     FEmpleadoGridSelect,
     ZetaCommonClasses,
     ZetaCommonTools,
     DGlobal,
     ZBaseSelectGrid,
     ZGlobalTress;

{$R *.dfm}

procedure TLiquidacionGlobal.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= HHHHH_Liquidacion_global_Finiquitos;
     GrTipo.ItemIndex := 0;
     Observaciones.MaxLength := ZetaCommonClasses.K_ANCHO_DESCRIPCION;
     Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];
end;

procedure TLiquidacionGlobal.CargaListaVerificacion;
begin
     dmProcesos.LiquidacionGlobalLista( ParameterList );
end;

function TLiquidacionGlobal.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid.GridSelect( dmProcesos.cdsDataset, TEmpleadoGridSelect );
end;

function TLiquidacionGlobal.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.LiquidacionGlobal(ParameterList);
end;

procedure TLiquidacionGlobal.CargaParametros;
var
   Fecha:TDate;
begin
     Fecha := NullDateTime;

     with Descripciones do
     begin
           AddString( 'Tipo Liquidación', GRTipo.Items.Strings[GRTipo.ItemIndex] );
          AddString( 'Observaciones', Observaciones.Text );
          AddString( 'Encimar Aprobadas', zBoolToStr(GrSimAprobadas.ItemIndex = 1) );
          AddString( 'Encimar Aplicadas', zBoolToStr(GrSimAplicadas.ItemIndex = 1) );
     end;

     inherited CargaParametros;

     with ParameterList do
     begin
          AddInteger( 'TipoLiquidacion', GRTipo.ItemIndex + 1 );
          AddString( 'Observaciones', Observaciones.Text );
          AddString( 'Simulacion', K_GLOBAL_SI );
          AddString('EncimarAprobada',zBoolToStr(GrSimAprobadas.ItemIndex = 1));
          AddString('EncimarAplicada',zBoolToStr(GrSimAplicadas.ItemIndex = 1));
          if IncluirBajas.Checked then
             Fecha := FechaBaja.Valor;
          AddDate ('FechaBaja',Fecha);
     end;
end;

procedure TLiquidacionGlobal.FormShow(Sender: TObject);
begin
     inherited;
     GrSimAprobadas.Visible := Global.GetGlobalBooleano ( K_GLOBAL_SIM_FINIQ_APROBACION );
      with dmCliente do
     begin
          sDescripcion.Caption := GetPeriodoCampoDescripcion;
          SetPeriodoNumero(Global.GetGlobalInteger(K_GLOBAL_SIMULACION_FINIQUITOS ) );
          RefrescaPeriodo;
          with GetDatosPeriodoActivo do
          begin
               iNumeroNomina.Caption := IntToStr( Numero );
               iMesNomina.Caption := ObtieneElemento( lfMeses, ( Mes - 1  ) );
               sStatusNomina.Caption := ZetaCommonTools.GetDescripcionStatusPeriodo( Status, StatusTimbrado );
               FechaInicial.Caption := FormatDateTime( LongDateFormat, Inicio );
               FechaFinal.Caption := FormatDateTime( LongDateFormat, Fin );
          end;
          Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];
     end;
end;

procedure TLiquidacionGlobal.IncluirBajasClick(Sender: TObject);
begin
     inherited;
      FechaBaja.Enabled := IncluirBajas.Checked;
end;

procedure TLiquidacionGlobal.GRTipoClick(Sender: TObject);
begin
     inherited;
     Observaciones.Text := GRTipo.Items.Strings[GRTipo.ItemIndex];
end;

end.
