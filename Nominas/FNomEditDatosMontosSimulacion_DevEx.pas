unit FNomEditDatosMontosSimulacion_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FNomEditDatosMontos_DevEx, DB, DBCtrls, StdCtrls, Mask,
  ZetaNumero, ExtCtrls, ZetaDBTextBox, ZetaKeyLookup_DevEx,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
  dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, cxControls, cxContainer, cxEdit,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxGroupBox, cxNavigator, cxDBNavigator, cxButtons;

type
  TNomEditDatosMontosSimulacion_DevEx = class(TNomEditDatosMontos_DevEx)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  NomEditDatosMontosSimulacion_DevEx: TNomEditDatosMontosSimulacion_DevEx;

implementation

uses dCatalogos, dSistema, dNomina,ZAccesosTress,
     ZetaCommonLists;

{$R *.dfm}

procedure TNomEditDatosMontosSimulacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_EMP_SIM_FINIQUITOS_EMPLEADO;
     HelpContext := 0; // PENDIENTE
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stNinguno;
end;

procedure TNomEditDatosMontosSimulacion_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          cdsSimulaMontos.Conectar;
          DataSource.DataSet:= cdsSimulaMontos;
     end;
end;

end.
