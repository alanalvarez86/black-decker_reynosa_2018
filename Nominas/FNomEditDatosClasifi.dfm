inherited NomEditDatosClasifi: TNomEditDatosClasifi
  Left = 238
  Top = 369
  Caption = 'Clasificaci'#243'n'
  ClientHeight = 379
  ClientWidth = 576
  OnCloseQuery = nil
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 343
    Width = 576
    inherited OK: TBitBtn
      Left = 408
    end
    inherited Cancelar: TBitBtn
      Left = 493
    end
  end
  inherited PanelSuperior: TPanel
    Width = 576
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 576
    inherited ValorActivo2: TPanel
      Width = 250
      inherited textoValorActivo2: TLabel
        Width = 244
      end
    end
  end
  object PageControl: TPageControl [3]
    Left = 0
    Top = 51
    Width = 576
    Height = 292
    ActivePage = ClasificaTab
    Align = alClient
    TabOrder = 3
    object ClasificaTab: TTabSheet
      Caption = 'Clasificaci'#243'n'
      object Panel2: TPanel
        Left = 0
        Top = 57
        Width = 568
        Height = 207
        Align = alClient
        TabOrder = 1
        object Label5: TLabel
          Left = 64
          Top = 10
          Width = 83
          Height = 13
          Alignment = taRightJustify
          Caption = 'Zona Geogr'#225'fica:'
        end
        object Label10: TLabel
          Left = 116
          Top = 31
          Width = 31
          Height = 13
          Alignment = taRightJustify
          Caption = 'Turno:'
        end
        object Label11: TLabel
          Left = 111
          Top = 53
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Puesto:'
        end
        object Label12: TLabel
          Left = 85
          Top = 74
          Width = 62
          Height = 13
          Alignment = taRightJustify
          Caption = 'Clasificaci'#243'n:'
        end
        object Label2: TLabel
          Left = 79
          Top = 96
          Width = 68
          Height = 13
          Alignment = taRightJustify
          Caption = 'Reg. Patronal:'
        end
        object bbMostrarCalendario: TSpeedButton
          Left = 454
          Top = 26
          Width = 23
          Height = 22
          Hint = 'Mostrar Calendario del Turno'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
            003337777777777777F330FFFFFFFFFFF03337F3333FFF3337F330FFFF000FFF
            F03337F33377733337F330FFFFF0FFFFF03337F33337F33337F330FFFF00FFFF
            F03337F33377F33337F330FFFFF0FFFFF03337F33337333337F330FFFFFFFFFF
            F03337FFF3F3F3F3F7F33000F0F0F0F0F0333777F7F7F7F7F7F330F0F000F070
            F03337F7F777F777F7F330F0F0F0F070F03337F7F7373777F7F330F0FF0FF0F0
            F03337F733733737F7F330FFFFFFFF00003337F33333337777F330FFFFFFFF0F
            F03337FFFFFFFF7F373330999999990F033337777777777F733330FFFFFFFF00
            333337FFFFFFFF77333330000000000333333777777777733333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = bbMostrarCalendarioClick
        end
        object Label14: TLabel
          Left = 65
          Top = 161
          Width = 82
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#233'todo de Pago:'
        end
        object CB_ZONA_GE: TDBComboBox
          Left = 154
          Top = 5
          Width = 145
          Height = 21
          DataField = 'CB_ZONA_GE'
          DataSource = DataSource
          ItemHeight = 13
          Items.Strings = (
            'A'
            'B'
            'C')
          TabOrder = 0
        end
        object CB_TURNO: TZetaDBKeyLookup
          Left = 154
          Top = 27
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsTurnos
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 1
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_TURNO'
          DataSource = DataSource
        end
        object CB_PUESTO: TZetaDBKeyLookup
          Left = 154
          Top = 49
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsPuestos
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_PUESTO'
          DataSource = DataSource
        end
        object CB_CLASIFI: TZetaDBKeyLookup
          Left = 154
          Top = 70
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsClasifi
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 3
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_CLASIFI'
          DataSource = DataSource
        end
        object CB_PATRON: TZetaDBKeyLookup
          Left = 154
          Top = 92
          Width = 300
          Height = 21
          LookupDataset = dmCatalogos.cdsRPatron
          Opcional = False
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 4
          TabStop = True
          WidthLlave = 60
          DataField = 'CB_PATRON'
          DataSource = DataSource
        end
        object NO_FUERA: TDBCheckBox
          Left = 70
          Top = 115
          Width = 97
          Height = 18
          Alignment = taLeftJustify
          Caption = 'Pago por Fuera:'
          DataField = 'NO_FUERA'
          DataSource = DataSource
          TabOrder = 5
          ValueChecked = 'S'
          ValueUnchecked = 'N'
        end
        object ChkDepositoBanca: TCheckBox
          Left = 11
          Top = 137
          Width = 156
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Dep'#243'sito Banca Electr'#243'nica:'
          TabOrder = 6
          OnClick = ChkDepositoBancaClick
        end
        object NO_PAGO: TZetaDBKeyCombo
          Left = 154
          Top = 157
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 7
          ListaFija = lfMetodoPagoSAT
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
          DataField = 'NO_PAGO'
          DataSource = DataSource
          LlaveNumerica = True
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 568
        Height = 57
        Align = alTop
        TabOrder = 0
        object Label4: TLabel
          Left = 15
          Top = 30
          Width = 74
          Height = 13
          Alignment = taRightJustify
          Caption = 'Observaciones:'
        end
        object US_CODIGO: TZetaDBTextBox
          Left = 380
          Top = 8
          Width = 177
          Height = 17
          AutoSize = False
          Caption = 'US_CODIGO'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'US_CODIGO'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label22: TLabel
          Left = 332
          Top = 10
          Width = 43
          Height = 13
          Alignment = taRightJustify
          Caption = 'Modific'#243':'
        end
        object Label33: TLabel
          Left = 56
          Top = 10
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = 'Status:'
        end
        object Label7: TLabel
          Left = 370
          Top = 31
          Width = 81
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo Liquidaci'#243'n:'
        end
        object NO_STATUS: TZetaDBTextBox
          Left = 93
          Top = 8
          Width = 145
          Height = 17
          AutoSize = False
          Caption = 'NO_STATUS'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_STATUS'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_LIQUIDA: TZetaDBTextBox
          Left = 456
          Top = 29
          Width = 101
          Height = 17
          AutoSize = False
          Caption = 'NO_LIQUIDA'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_LIQUIDA'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_OBSERVA: TDBEdit
          Left = 92
          Top = 27
          Width = 253
          Height = 21
          DataField = 'NO_OBSERVA'
          DataSource = DataSource
          TabOrder = 0
        end
      end
    end
    object NivelesTab: TTabSheet
      Caption = 'Niveles'
      object CB_NIVEL9Lbl: TLabel
        Left = 227
        Top = 180
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9'
      end
      object CB_NIVEL8Lbl: TLabel
        Left = 227
        Top = 158
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8'
      end
      object CB_NIVEL7Lbl: TLabel
        Left = 227
        Top = 136
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7'
      end
      object CB_NIVEL6Lbl: TLabel
        Left = 227
        Top = 114
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6'
      end
      object CB_NIVEL5Lbl: TLabel
        Left = 227
        Top = 92
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5'
      end
      object CB_NIVEL4Lbl: TLabel
        Left = 227
        Top = 70
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4'
      end
      object CB_NIVEL3Lbl: TLabel
        Left = 227
        Top = 48
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3'
      end
      object CB_NIVEL2Lbl: TLabel
        Left = 227
        Top = 26
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2'
      end
      object CB_NIVEL1Lbl: TLabel
        Left = 227
        Top = 4
        Width = 30
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1'
      end
      object CB_NIVEL10Lbl: TLabel
        Left = 221
        Top = 202
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10'
        Visible = False
      end
      object CB_NIVEL11Lbl: TLabel
        Left = 221
        Top = 224
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11'
        Visible = False
      end
      object CB_NIVEL12Lbl: TLabel
        Left = 221
        Top = 246
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12'
        Visible = False
      end
      object CB_NIVEL1: TZetaDBKeyLookup
        Left = 266
        Top = 0
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel1
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 0
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL1'
        DataSource = DataSource
      end
      object CB_NIVEL3: TZetaDBKeyLookup
        Left = 266
        Top = 44
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel3
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 2
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL3'
        DataSource = DataSource
      end
      object CB_NIVEL4: TZetaDBKeyLookup
        Left = 266
        Top = 66
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel4
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 3
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL4'
        DataSource = DataSource
      end
      object CB_NIVEL5: TZetaDBKeyLookup
        Left = 266
        Top = 88
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel5
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 4
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL5'
        DataSource = DataSource
      end
      object CB_NIVEL6: TZetaDBKeyLookup
        Left = 266
        Top = 110
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel6
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 5
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL6'
        DataSource = DataSource
      end
      object CB_NIVEL7: TZetaDBKeyLookup
        Left = 266
        Top = 132
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel7
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 6
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL7'
        DataSource = DataSource
      end
      object CB_NIVEL8: TZetaDBKeyLookup
        Left = 266
        Top = 154
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel8
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 7
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL8'
        DataSource = DataSource
      end
      object CB_NIVEL9: TZetaDBKeyLookup
        Left = 266
        Top = 176
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel9
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 8
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL9'
        DataSource = DataSource
      end
      object CB_NIVEL2: TZetaDBKeyLookup
        Left = 266
        Top = 22
        Width = 300
        Height = 21
        LookupDataset = dmTablas.cdsNivel2
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 1
        TabStop = True
        WidthLlave = 60
        DataField = 'CB_NIVEL2'
        DataSource = DataSource
      end
      object CB_NIVEL10: TZetaDBKeyLookup
        Left = 266
        Top = 198
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 9
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL10'
      end
      object CB_NIVEL11: TZetaDBKeyLookup
        Left = 266
        Top = 220
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 10
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL11'
      end
      object CB_NIVEL12: TZetaDBKeyLookup
        Left = 266
        Top = 242
        Width = 300
        Height = 21
        Opcional = False
        EditarSoloActivos = False
        IgnorarConfidencialidad = False
        TabOrder = 11
        TabStop = True
        Visible = False
        WidthLlave = 60
        DataField = 'CB_NIVEL12'
      end
    end
    object PagosFolios: TTabSheet
      Caption = 'Pagos y Folios'
      object GroupBox1: TGroupBox
        Left = 40
        Top = 104
        Width = 225
        Height = 97
        Caption = 'Folios'
        TabOrder = 1
        object Label1: TLabel
          Left = 17
          Top = 20
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '1:'
        end
        object Label3: TLabel
          Left = 17
          Top = 42
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '2:'
        end
        object Label6: TLabel
          Left = 17
          Top = 65
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '3:'
        end
        object Label8: TLabel
          Left = 121
          Top = 20
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '4:'
        end
        object Label9: TLabel
          Left = 121
          Top = 42
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '5:'
        end
        object NO_FOLIO_1: TZetaDBNumero
          Left = 31
          Top = 16
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 0
          DataField = 'NO_FOLIO_1'
          DataSource = DataSource
        end
        object NO_FOLIO_2: TZetaDBNumero
          Left = 31
          Top = 38
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 1
          DataField = 'NO_FOLIO_2'
          DataSource = DataSource
        end
        object NO_FOLIO_3: TZetaDBNumero
          Left = 31
          Top = 61
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 2
          DataField = 'NO_FOLIO_3'
          DataSource = DataSource
        end
        object NO_FOLIO_4: TZetaDBNumero
          Left = 135
          Top = 16
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 3
          DataField = 'NO_FOLIO_4'
          DataSource = DataSource
        end
        object NO_FOLIO_5: TZetaDBNumero
          Left = 135
          Top = 38
          Width = 73
          Height = 21
          Mascara = mnEmpleado
          TabOrder = 4
          DataField = 'NO_FOLIO_5'
          DataSource = DataSource
        end
      end
      object GroupBox2: TGroupBox
        Left = 40
        Top = 5
        Width = 393
        Height = 97
        Caption = 'Pagos'
        TabOrder = 0
        object Label13: TLabel
          Left = 26
          Top = 51
          Width = 39
          Height = 13
          Caption = 'Usuario:'
        end
        object NO_USR_PAG: TZetaDBTextBox
          Left = 70
          Top = 49
          Width = 160
          Height = 17
          AutoSize = False
          Caption = 'NO_USR_PAG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_USR_PAG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object CBPagado: TCheckBox
          Left = 7
          Top = 28
          Width = 58
          Height = 17
          Caption = 'Pagado'
          TabOrder = 0
          OnClick = CBPagadoClick
        end
        object NO_FEC_PAG: TZetaDBFecha
          Left = 70
          Top = 25
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '22/feb/99'
          Valor = 36213.000000000000000000
          DataField = 'NO_FEC_PAG'
          DataSource = DataSource
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 1
  end
end
