unit FNomEditDatosClasifi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseEdicion, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, ZetaFecha,
  ZetaNumero, ZetaKeyCombo, Mask, ZetaDBTextBox, ZetaKeyLookup, ComCtrls,
  ZetaSmartLists;

type
  TNomEditDatosClasifi = class(TBaseEdicion)
    PageControl: TPageControl;
    ClasificaTab: TTabSheet;
    Panel2: TPanel;
    Label5: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    CB_ZONA_GE: TDBComboBox;
    CB_TURNO: TZetaDBKeyLookup;
    CB_PUESTO: TZetaDBKeyLookup;
    CB_CLASIFI: TZetaDBKeyLookup;
    CB_PATRON: TZetaDBKeyLookup;
    NO_FUERA: TDBCheckBox;
    Panel1: TPanel;
    Label4: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label22: TLabel;
    Label33: TLabel;
    Label7: TLabel;
    NO_OBSERVA: TDBEdit;
    NivelesTab: TTabSheet;
    CB_NIVEL9Lbl: TLabel;
    CB_NIVEL8Lbl: TLabel;
    CB_NIVEL7Lbl: TLabel;
    CB_NIVEL6Lbl: TLabel;
    CB_NIVEL5Lbl: TLabel;
    CB_NIVEL4Lbl: TLabel;
    CB_NIVEL3Lbl: TLabel;
    CB_NIVEL2Lbl: TLabel;
    CB_NIVEL1Lbl: TLabel;
    CB_NIVEL1: TZetaDBKeyLookup;
    CB_NIVEL4: TZetaDBKeyLookup;
    CB_NIVEL5: TZetaDBKeyLookup;
    CB_NIVEL6: TZetaDBKeyLookup;
    CB_NIVEL7: TZetaDBKeyLookup;
    CB_NIVEL8: TZetaDBKeyLookup;
    CB_NIVEL9: TZetaDBKeyLookup;
    PagosFolios: TTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    NO_FOLIO_1: TZetaDBNumero;
    NO_FOLIO_2: TZetaDBNumero;
    NO_FOLIO_3: TZetaDBNumero;
    NO_FOLIO_4: TZetaDBNumero;
    NO_FOLIO_5: TZetaDBNumero;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    CBPagado: TCheckBox;
    NO_FEC_PAG: TZetaDBFecha;
    CB_NIVEL2: TZetaDBKeyLookup;
    CB_NIVEL3: TZetaDBKeyLookup;
    NO_USR_PAG: TZetaDBTextBox;
    NO_STATUS: TZetaDBTextBox;
    NO_LIQUIDA: TZetaDBTextBox;
    bbMostrarCalendario: TSpeedButton;
    ChkDepositoBanca: TCheckBox;
    CB_NIVEL10Lbl: TLabel;
    CB_NIVEL10: TZetaDBKeyLookup;
    CB_NIVEL11Lbl: TLabel;
    CB_NIVEL11: TZetaDBKeyLookup;
    CB_NIVEL12Lbl: TLabel;
    CB_NIVEL12: TZetaDBKeyLookup;
    Label14: TLabel;
    NO_PAGO: TZetaDBKeyCombo;
    procedure FormCreate(Sender: TObject);
    procedure CBPagadoClick(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure ChkDepositoBancaClick(Sender: TObject);
    procedure CancelarClick(Sender: TObject);
  private
    FConnect : Boolean;
    procedure PonerModoEdicion;
    procedure SetCamposNivel;
    procedure SetControlsCheckBoxes;
  protected
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  NomEditDatosClasifi: TNomEditDatosClasifi;

implementation

uses DTablas,
     DCatalogos,
     DGlobal,
     DSistema,
     DNomina,
     ZetaClientTools,
     ZetaCommonTools,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZGlobalTress,
     ZAccesosTress,
     DCliente;

{$R *.DFM}

{ TNomEditDatosClasifi }

procedure TNomEditDatosClasifi.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := DataSource;
     CB_NIVEL11.DataSource := DataSource;
     CB_NIVEL12.DataSource := DataSource;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     {$endif}
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     HelpContext:= H30315_Clasificacion;
     IndexDerechos := D_NOM_DATOS_CLASIFI;
     SetCamposNivel;

     CB_TURNO.LookupDataset := dmCatalogos.cdsTurnos;
     CB_PUESTO.LookupDataset := dmCatalogos.cdsPuestos;
     CB_CLASIFI.LookupDataset := dmCatalogos.cdsClasifi;
     CB_PATRON.LookupDataset := dmCatalogos.cdsRPatron;
     CB_NIVEL1.LookupDataset := dmTablas.cdsNivel1;
     CB_NIVEL3.LookupDataset := dmTablas.cdsNivel3;
     CB_NIVEL4.LookupDataset := dmTablas.cdsNivel4;
     CB_NIVEL5.LookupDataset := dmTablas.cdsNivel5;
     CB_NIVEL6.LookupDataset := dmTablas.cdsNivel6;
     CB_NIVEL7.LookupDataset := dmTablas.cdsNivel7;
     CB_NIVEL8.LookupDataset := dmTablas.cdsNivel8;
     CB_NIVEL9.LookupDataset := dmTablas.cdsNivel9;
     CB_NIVEL2.LookupDataset := dmTablas.cdsNivel2;
     {$ifdef ACS}{OP: 5.Ago.08}
     CB_NIVEL10.LookupDataset := dmTablas.cdsNivel10;
     CB_NIVEL11.LookupDataset := dmTablas.cdsNivel11;
     CB_NIVEL12.LookupDataset := dmTablas.cdsNivel12;
     {$endif}
end;

procedure TNomEditDatosClasifi.Connect;
begin
     with dmTablas do
     begin
          if CB_NIVEL1.Visible then cdsNivel1.Conectar;
          if CB_NIVEL2.Visible then cdsNivel2.Conectar;
          if CB_NIVEL3.Visible then cdsNivel3.Conectar;
          if CB_NIVEL4.Visible then cdsNivel4.Conectar;
          if CB_NIVEL5.Visible then cdsNivel5.Conectar;
          if CB_NIVEL6.Visible then cdsNivel6.Conectar;
          if CB_NIVEL7.Visible then cdsNivel7.Conectar;
          if CB_NIVEL8.Visible then cdsNivel8.Conectar;
          if CB_NIVEL9.Visible then cdsNivel9.Conectar;
          {$ifdef ACS}{OP: 5.Ago.08}
          if CB_NIVEL10.Visible then cdsNivel10.Conectar;
          if CB_NIVEL11.Visible then cdsNivel11.Conectar;
          if CB_NIVEL12.Visible then cdsNivel12.Conectar;
          {$endif}
     end;
     with dmCatalogos do
     begin
          cdsClasifi.Conectar;
          cdsPuestos.Conectar;
          cdsTurnos.Conectar;
          cdsRPatron.Conectar;
     end;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          cdsDatosClasifi.Conectar;
          DataSource.DataSet:= cdsDatosClasifi;
     end;
     SetControlsCheckBoxes;
end;

procedure TNomEditDatosClasifi.SetControlsCheckBoxes;
begin
     FConnect := TRUE;
     CBPagado.Checked := ( NO_FEC_PAG.Valor > 0 );
     with ChkDepositoBanca do
     begin
          Checked := ZetaCommonTools.StrLleno( dmNomina.cdsDatosClasifi.FieldbyName( 'CB_BAN_ELE' ).AsString );
          Enabled := ( ChkDepositoBanca.checked ) or ZetaCommonTools.StrLleno( dmCliente.cdsEmpleado.FieldbyName( 'CB_BAN_ELE' ).AsString );
     end;
     FConnect := FALSE;
end;

procedure TNomEditDatosClasifi.PonerModoEdicion;
begin
     with dmNomina.cdsDatosClasifi do
     begin
          if NOT FConnect AND ( State = dsBrowse ) then
             Edit;
     end;
end;

procedure TNomEditDatosClasifi.CBPagadoClick(Sender: TObject);
begin
     inherited;
     PonerModoEdicion;
     NO_FEC_PAG.Enabled := CBPagado.Checked;
     if ( NO_FEC_PAG.Enabled ) AND ( NO_FEC_PAG.Valor = 0 ) then
     begin
          NO_FEC_PAG.Valor := Date;
     end;
end;

procedure TNomEditDatosClasifi.OKClick(Sender: TObject);
begin
     if NOT CBPagado.Checked then NO_FEC_PAG.Valor := 0;
     inherited;
end;

procedure TNomEditDatosClasifi.CancelarClick(Sender: TObject);
begin
     inherited;
     SetControlsCheckBoxes;
end;

procedure TNomEditDatosClasifi.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, CB_NIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, CB_NIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, CB_NIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, CB_NIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, CB_NIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, CB_NIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, CB_NIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, CB_NIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, CB_NIVEL9 );
     {$ifdef ACS}{OP: 5.Ago.08}    
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, CB_NIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, CB_NIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, CB_NIVEL12 );
     {$endif}
end;

procedure TNomEditDatosClasifi.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Llave );
end;

procedure TNomEditDatosClasifi.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmNomina.cdsDatosClasifi do
          begin
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
          end;
     end;
end;

procedure TNomEditDatosClasifi.ChkDepositoBancaClick(Sender: TObject);
var
   sBanca: String;
begin
     inherited;
     if NOT FConnect then
     begin
          PonerModoEdicion;
          if ChkDepositoBanca.Checked then
          begin
               sBanca := dmCliente.cdsEmpleado.FieldbyName('CB_BAN_ELE').AsString
          end
          else
          begin
               sBanca := VACIO;
          end;
          dmNomina.cdsDatosClasifi.FieldByName( 'CB_BAN_ELE' ).AsString := sBanca;

     end;
end;

end.


