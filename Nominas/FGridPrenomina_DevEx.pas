unit FGridPrenomina_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseGridEdicion_DevEx, Db, Grids, DBGrids, ZetaDBGrid, DBCtrls,
  ExtCtrls, StdCtrls, ZetaDBTextBox, ComCtrls, ZetaKeyLookup_DevEx, ZetaMessages,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls,
  dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses, ImgList, cxNavigator,
  cxDBNavigator, cxButtons, dxSkinscxPCPainter, cxPCdxBarPopupMenu, cxPC;

type
  TGridPrenomina_DevEx = class(TBaseGridEdicion_DevEx)
    PageControl1: TcxPageControl;
    TabSheet1: TcxTabSheet;
    Label17: TLabel;
    NO_DIAS: TZetaDBTextBox;
    Label18: TLabel;
    NO_DIAS_AS: TZetaDBTextBox;
    Label19: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    Label20: TLabel;
    NO_DIAS_FJ: TZetaDBTextBox;
    Label21: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    Label22: TLabel;
    NO_DIAS_NT: TZetaDBTextBox;
    Label23: TLabel;
    NO_DIAS_SG: TZetaDBTextBox;
    Label24: TLabel;
    NO_DIAS_SU: TZetaDBTextBox;
    Label32: TLabel;
    NO_DIAS_AG: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    Label31: TLabel;
    NO_DIAS_RE: TZetaDBTextBox;
    Label30: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    Label29: TLabel;
    NO_DIAS_OT: TZetaDBTextBox;
    Label28: TLabel;
    NO_DIAS_CG: TZetaDBTextBox;
    Label26: TLabel;
    NO_DIAS_EM: TZetaDBTextBox;
    Label27: TLabel;
    NO_DIAS_SS: TZetaDBTextBox;
    Label25: TLabel;
    TabSheet2: TcxTabSheet;
    Label4: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label10: TLabel;
    NO_HORA_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label11: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label5: TLabel;
    Label6: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label12: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label13: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label7: TLabel;
    Label8: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label14: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label15: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label9: TLabel;
    Label36: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    TabSheet4: TcxTabSheet;
    Label1: TLabel;
    CB_TURNO: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    NO_D_TURNO: TZetaDBTextBox;
    NO_JORNADA: TZetaDBTextBox;
    TURNO: TZetaDBTextBox;
    TabSheet3: TcxTabSheet;
    Label34: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label35: TLabel;
    NO_USER_RJ: TZetaDBTextBox;
    NO_OBSERVA: TZetaDBTextBox;
    Label16: TLabel;
    Label33: TLabel;
    STATUSNOM: TZetaDBTextBox;
    PanelEmpleado: TPanel;
    EmpleadoLbl: TLabel;
    CB_CODIGO: TZetaKeyLookup_DevEx;
    dsNomina: TDataSource;
    BBModificar: TcxButton;
    ImgNoRecord: TImage;
    bbMostrarCalendario: TcxButton;
    Label37: TLabel;
    NO_DIAS_SI: TZetaDBTextBox;
    Label38: TLabel;
    NO_HORASNT: TZetaDBTextBox;
    Label39: TLabel;
    NO_HORAPDT: TZetaDBTextBox;
    Label40: TLabel;
    NO_DIAS_FV: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure ZetaDBGridDblClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure CB_CODIGOValidKey(Sender: TObject);
    procedure ZetaDBGridTitleClick(Column: TColumn);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure dsNominaDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
    function NoHayDatos: Boolean;
    procedure CambioEmpleado;
    procedure SetControlesActivos(const lEnabled: Boolean);
   // procedure WMExaminar(var Message: TMessage); message WM_EXAMINAR;
  protected
    function PuedeAgregar: Boolean; override;
    function PuedeBorrar: Boolean; override;
    function PosicionaSiguienteColumna: Integer; override;
    procedure SetCycleControl; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure CancelarCambios; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  GridPrenomina_DevEx: TGridPrenomina_DevEx;

implementation

uses DNomina, DCliente, DCatalogos, ZetaCommonLists, ZetaCommonClasses, ZetaCommonTools,
     FNomEditDatosAsist_DevEx, ZBaseEdicion_DevEx;

{$R *.DFM}

procedure TGridPrenomina_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo2 := stPeriodo;
     HelpContext := H22121_edicion_prenomina;
     PrimerColumna := 2;
     PermiteAltas := FALSE;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

procedure TGridPrenomina_DevEx.Connect;
var
   Pos : TBookMark;
begin
     with dmNomina do
     begin
          cdsDatosAsist.Conectar;
          DataSource.DataSet := cdsMovDatosAsist;
          dsNomina.DataSet := cdsDatosAsist;
          Pos:= cdsMovDatosAsist.GetBookMark;
          cdsMovDatosAsist.First;
          if ( Pos <> nil ) then
          begin
               cdsMovDatosAsist.GotoBookMark( Pos );
               cdsMovDatosAsist.FreeBookMark( Pos );
          end;
     end;
     with dmCliente do
     begin
          dmNomina.EmpleadoGrid := Empleado;
          CB_CODIGO.SetLlaveDescripcion( IntToStr( Empleado ), GetDatosEmpleadoActivo.Nombre );  // Evita que se Haga el DoLookup del Control
     end;
end;

function TGridPrenomina_DevEx.PuedeAgregar: Boolean;
begin
     Result := FALSE;
end;

function TGridPrenomina_DevEx.PuedeBorrar: Boolean;
begin
     Result := FALSE;
end;

procedure TGridPrenomina_DevEx.EscribirCambios;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmNomina.cdsDatosAsist.Enviar;       // Se Env�a el Padre
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TGridPrenomina_DevEx.CancelarCambios;
begin
     inherited;
     with dmNomina.cdsDatosAsist do
          if State in [ dsEdit, dsInsert ] then
             Cancel;
end;

procedure TGridPrenomina_DevEx.HabilitaControles;
begin
     inherited;
     CB_CODIGO.Enabled := not self.Editing;
end;

procedure TGridPrenomina_DevEx.CambioEmpleado;
var
   oCursor: TCursor;
begin
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        dmNomina.cdsDatosAsist.Refrescar;
     finally
        Screen.Cursor := oCursor;
     end;
end;

procedure TGridPrenomina_DevEx.OK_DevExClick(Sender: TObject);
begin
     EscribirCambios;
     DataSourceStateChange( self );
     if CB_CODIGO.Enabled then
        CB_CODIGO.SetFocus;
end;

procedure TGridPrenomina_DevEx.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     if CB_CODIGO.Enabled then
        CB_CODIGO.SetFocus;
end;

procedure TGridPrenomina_DevEx.BBModificarClick(Sender: TObject);
begin
     inherited;
     ZBaseEdicion_DevEx.ShowFormaEdicion( NomEditDatosAsist_DevEx, TNomEditDatosAsist_DevEx );
end;

procedure TGridPrenomina_DevEx.ZetaDBGridDblClick(Sender: TObject);
begin
     inherited;
     BBModificarClick( Sender );
end;

procedure TGridPrenomina_DevEx.CB_CODIGOValidKey(Sender: TObject);
begin
     inherited;
     with dmNomina do
     begin
          if ( CB_CODIGO.Valor <> EmpleadoGrid ) then
          begin
               EmpleadoGrid:= CB_CODIGO.Valor;
               CambioEmpleado;
               SetControlesActivos( not NoHayDatos );
               if NoHayDatos then
                  CB_CODIGO.SetFocus
               else
               begin
                    ZetaDBGrid.SetFocus;
                    SeleccionaPrimerColumna;
               end;
          end;
     end;
end;

function TGridPrenomina_DevEx.NoHayDatos: Boolean;
begin
     with Datasource do
     begin
          if ( Dataset = nil ) then
             Result := False
          else
             Result := Dataset.IsEmpty;
     end;
end;

procedure TGridPrenomina_DevEx.SetControlesActivos( const lEnabled: Boolean );
begin
     ImgNoRecord.Visible := not lEnabled;
     ZetaDBGrid.Enabled  := lEnabled;
     BBModificar.Enabled := lEnabled;
end;

function TGridPrenomina_DevEx.PosicionaSiguienteColumna: Integer;
const
     K_COLUMNA_ORDINARIAS = 2;
begin
     with ZetaDBGrid.SelectedField do
     begin
          if ( FieldName = 'AU_TRIPLES' ) or ( FieldName = 'AU_PER_CG' ) or
             ( FieldName = 'AU_PER_SG' ) or ( FieldName = 'AU_DES_TRA' ) or
             ( FieldName = 'AU_TARDES' ) or ( FieldName = 'US_CODIGO' ) then
             Result := K_COLUMNA_ORDINARIAS
          else
             Result := inherited PosicionaSiguienteColumna;
     end
end;

procedure TGridPrenomina_DevEx.SetCycleControl;
begin
     inherited;
     if not OK_DevEx.Enabled then
        CB_CODIGO.SetFocus;
end;

procedure TGridPrenomina_DevEx.ZetaDBGridTitleClick(Column: TColumn);
begin
     { No debe hacer nada }
end;

{
procedure TGridPrenomina.WMExaminar(var Message: TMessage);
begin
     inherited;
     if ( TExaminador(  Message.LParam ) = exEnter ) then
     begin
          if ( ZetaDBGrid.SelectedIndex = PrimerColumna ) and
             ( dmNomina.cdsMovDatosAsist.EOF ) then
          begin
               if OK.Enabled then
                  OK.SetFocus;
               if not OK.Enabled then
                  CB_CODIGO.SetFocus;
          end
     end;
end;
}

procedure TGridPrenomina_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TGridPrenomina_DevEx.dsNominaDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     if ( Field = nil ) or ( Field.FieldName = 'CB_TURNO' ) then
     begin
          with dmNomina.cdsDatosAsist do
               bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
     end;
end;

end.


