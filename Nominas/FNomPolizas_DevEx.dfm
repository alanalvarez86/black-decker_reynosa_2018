inherited NomPolizas_DevEx: TNomPolizas_DevEx
  Left = 108
  Top = 358
  Caption = 'P'#243'lizas'
  ClientHeight = 285
  ClientWidth = 804
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelIdentifica: TPanel
    Width = 804
    inherited ValorActivo2: TPanel
      Width = 545
      inherited textoValorActivo2: TLabel
        Width = 539
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 804
    Height = 266
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      PopUpMenu = PopupMenu1
      DataController.DataSource = DataSource
      object PT_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'PT_CODIGO'
        MinWidth = 75
        Options.Grouping = False
        Width = 75
      end
      object PT_NOMBRE: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PT_NOMBRE'
        MinWidth = 100
        Options.Grouping = False
        Width = 100
      end
      object PH_FECHA: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'PH_FECHA'
        MinWidth = 75
        Options.Grouping = False
        Width = 75
      end
      object PH_HORA: TcxGridDBColumn
        Caption = 'Hora'
        DataBinding.FieldName = 'PH_HORA'
        MinWidth = 75
        Options.Grouping = False
      end
      object US_DESCRIP: TcxGridDBColumn
        Caption = 'Usuario'
        DataBinding.FieldName = 'US_DESCRIP'
        MinWidth = 75
        Options.Grouping = False
      end
      object PH_VECES: TcxGridDBColumn
        Caption = 'Veces'
        DataBinding.FieldName = 'PH_VECES'
        MinWidth = 75
        Options.Grouping = False
      end
      object POL_MOVS: TcxGridDBColumn
        Caption = 'Asientos'
        DataBinding.FieldName = 'POL_MOVS'
        MinWidth = 75
        Options.Grouping = False
      end
      object PH_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'PH_STATUS'
        MinWidth = 75
      end
      object PH_REPORTE: TcxGridDBColumn
        Caption = 'Formato'
        DataBinding.FieldName = 'PH_REPORTE'
        MinWidth = 75
        Options.Grouping = False
      end
    end
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
end