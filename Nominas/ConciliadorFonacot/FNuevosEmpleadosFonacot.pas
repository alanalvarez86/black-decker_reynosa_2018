unit FNuevosEmpleadosFonacot;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsulta, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  ZBaseConsultaFonacot, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxContainer, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxLabel,
  ZetaKeyLookup_DevEx, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  cxTextEdit, Vcl.Buttons, cxButtons;

type
  TNuevosEmpleadosFonacot = class(TBaseConsultaFonacot)
    Posicion: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    NomTress: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    NFonacot: TcxGridDBColumn;
    Status: TcxGridDBColumn;
    btnBuscarEmp_DevEx: TcxButton;
    Reubicado: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure NEmpleadoCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;var ADone: Boolean);
    procedure ZetaDBGridDBTableViewFocusedItemChanged( Sender: TcxCustomGridTableView; APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem);
    procedure btnBuscarEmp_DevExClick(Sender: TObject);
    procedure ZetaDBGridDBTableViewCustomDrawCell(
      Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);

  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
  end;

var
  NuevosEmpleadosFonacot: TNuevosEmpleadosFonacot;

implementation

{$R *.dfm}

uses
    ZetaCommonClasses, DConcilia, ZetaBuscaEmpleado_DevEx,
    DCliente;


procedure TNuevosEmpleadosFonacot.btnBuscarEmp_DevExClick(Sender: TObject);
var
   sKey,sNombre:string;
begin
     inherited;
     if ZetaBuscaEmpleado_DevEx.BuscaEmpleadoDialogo(VACIO,sKey,sNombre)then
     begin
          with dmConcilia.cdsEmpleaFonacot do
          begin
               Edit;
               FieldByName('NEmpleado').AsInteger := StrToInt(sKey);
               FieldByName('NomTress').AsString := sNombre;
               Post;
          end;
     end;
end;

procedure TNuevosEmpleadosFonacot.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsEmpleaFonacot;
end;

procedure TNuevosEmpleadosFonacot.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_NUEVOS_EMPLEADOS;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n empleado nuevo en Fonacot'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';
end;

procedure TNuevosEmpleadosFonacot.FormShow(Sender: TObject);
begin
     inherited;
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('Posicion'), K_SIN_TIPO , '', skCount);
     ZetaDBGridDBTableView.ApplyBestFit();
end;


procedure TNuevosEmpleadosFonacot.NEmpleadoCustomDrawCell( Sender: TcxCustomGridTableView; ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
     inherited;
     if AViewInfo.GridRecord.Focused and AViewInfo.Item.Focused then
     begin
           with btnBuscarEmp_DevEx do
           begin
                Left := AViewInfo.RealBounds.Left + AViewInfo.RealBounds.Width - Width;
                Top := AViewInfo.RealBounds.Top + ZetaDBGrid.Top;
                Visible := True;
           end;
     end;
end;

procedure TNuevosEmpleadosFonacot.ZetaDBGridDBTableViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var iTermina: String;
begin
     inherited;
     iTermina := AViewInfo.GridRecord.DisplayTexts[Status.Index];

     if iTermina = 'Baja' then
        ACanvas.Font.Color := clRed;
end;

procedure TNuevosEmpleadosFonacot.ZetaDBGridDBTableViewFocusedItemChanged( Sender: TcxCustomGridTableView; APrevFocusedItem, AFocusedItem: TcxCustomGridTableItem );
begin
     inherited;
     if ( AFocusedItem <> nil ) then
     begin
          if ( AFocusedItem.Name <> 'NEmpleado' ) and ( btnBuscarEmp_DevEx.Visible ) then
            btnBuscarEmp_DevEx.Visible:= False;
     end;
end;

end.
