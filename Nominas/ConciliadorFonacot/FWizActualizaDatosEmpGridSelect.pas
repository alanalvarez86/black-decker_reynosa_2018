unit FWizActualizaDatosEmpGridSelect;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBasicoSelectGrid_DevEx, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Vcl.Menus, dxSkinsCore, cxControls, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.ImgList, cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid,
  Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, DBClient, TressMorado2013, cxTextEdit;

type
  TWizActualizaDatosEmpGridSelect = class(TBasicoGridSelect_DevEx)
    NomCompleto: TcxGridDBColumn;
    procedure Cancelar_DevExClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FModalidad : TModalResult;
  public
    { Public declarations }
    property Modalidad : TModalResult read FModalidad;
  end;

var
   WizActualizaDatosEmpGridSelect: TWizActualizaDatosEmpGridSelect;

implementation

{$R *.dfm}

procedure TWizActualizaDatosEmpGridSelect.Cancelar_DevExClick(Sender: TObject);
begin
     inherited;
     TClientDataSet(DataSource.DataSet ).CancelUpdates;
     FModalidad := mrCancel;
end;

procedure TWizActualizaDatosEmpGridSelect.FormShow(Sender: TObject);
const
     K_ANCHO_FORMA = 1285;
begin
     inherited;
     Width := K_ANCHO_FORMA;

     with DataSet do
     begin
          MaskPesos( 'CRetencion' );
          MaskPesos( 'TressRetencion' );
          MaskPesos( 'DiferenciaCredito' );
     end;

     NomCompleto.Options.HorzSizing := TRUE;
     NomCompleto.MinWidth := 150;
end;

procedure TWizActualizaDatosEmpGridSelect.OK_DevExClick(Sender: TObject);
begin
  inherited;
     FModalidad := mrOk;
end;

end.
