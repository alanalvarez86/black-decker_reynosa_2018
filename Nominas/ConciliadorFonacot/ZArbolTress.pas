unit ZArbolTress;

interface

uses SysUtils, ComCtrls, Dialogs, DB,Controls,
     ZArbolTools, ZNavBarTools, cxtreeview, ZetaCommonTools;

function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr ; Arbolitos: array of TcxTreeView );
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );

implementation

uses
     DCliente,
     ZetaDespConsulta, ZetaTipoEntidad, ZAccesosMgr;

const
     //******** CONCILIACION **********
     K_CONCILIACION                      = 1;      //rama de empleados
     K_RESUMEN_EMPLEADOS_CON_CREDITO     = 10;
     K_NUEVOS_EMPLEADOS_FONACOT          = 20;
     K_CREDITOS_NUEVOS                   = 30;
     K_CREDITOS_DIFERENCIAS              = 40;
     K_CREDITOS_ACTIVOS_NO_CEDULA        = 50;
     K_CREDITOS_SALDADOS_ACTIVOS         = 60;
     K_ERRORES_CEDULA                    = 70;

     //******** CONFRONTA  **********
     K_CONFRONTA                         = 69;   //rama de nomina
     K_CEDULA_PAGO                       = 90;

     K_FULL_ARBOL_TRESS                 =9999;


     
function Forma( const sCaption: String; const eTipo: eFormaConsulta ): TNodoInfo;
begin
     Result.EsForma := TRUE;
     Result.Caption := sCaption;
     Result.TipoForma := eTipo;
end;

function Folder( const sCaption: String; const iDerechos: Integer ): TNodoInfo;
begin
     Result.EsForma := FALSE;
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;
//DevEx: Funcion para definir Grupos del NavBar
function Grupo( const sCaption: String; const iDerechos: Integer ): TGrupoInfo;
begin
     Result.Caption := sCaption;
     Result.IndexDerechos := iDerechos;
end;


function GetNodoInfo( const iNodo: Integer ): TNodoInfo;
begin
     case iNodo of
          //******** CONCILIACION **********}
              K_RESUMEN_EMPLEADOS_CON_CREDITO    : Result := Forma( 'Resumen de empleados con cr�dito', efcResumenEmpleadosConCredito );
              K_NUEVOS_EMPLEADOS_FONACOT         : Result := Forma( 'Nuevos empleados con Fonacot', efcNuevosEmpleadosFonacot );
              K_CREDITOS_NUEVOS                  : Result := Forma( 'Cr�ditos nuevos', efcCreditosNuevos );
              K_CREDITOS_DIFERENCIAS             : Result := Forma( 'Cr�ditos con diferencias', efcCreditosDiferencias );
              K_CREDITOS_ACTIVOS_NO_CEDULA       : Result := Forma( 'Cr�ditos activos que no est�n en c�dula', efcCreditosActivosNoCedula );
              K_CREDITOS_SALDADOS_ACTIVOS        : Result := Forma( 'Cr�ditos saldados en Tress pero activos en c�dula Fonacot', efcCreditosSaldadosActivos );
              K_ERRORES_CEDULA                   : Result := Forma( 'Errores en la c�dula', efcErroresCedula );
           //******** CONFRONTA  **********
              K_CEDULA_PAGO                      : Result := Forma( 'C�dula de pago', efcCedulaPago );
            else
           Result := Folder( '', 0 );
     end;
end;


//DevEx
function GetGrupoInfo( const iNodo: Integer ): TGrupoInfo;
begin
 case iNodo of

          K_CONCILIACION       : Result := Grupo( 'Conciliaci�n', iNodo );
          K_CONFRONTA          : Result := Grupo( 'Confronta', iNodo );
 end;
end;



 procedure CreaArbolitoDefault( oArbolMgr: TArbolMgr ; iAccesoGlobal: Integer );
var
   i, iNiveles: Integer;

function NodoNivel( const iNivel, iNodo: Integer ): Boolean;
begin
     Result := oArbolMgr.NodoNivel( iNivel, GetNodoInfo( iNodo ), iNodo );
end;

procedure AgregaNodoTabAdic( const oEntidad : TipoEntidad; iNodo: Integer );
begin
  //**   if strLleno( dmSistema.GetNameTabAdic( oEntidad ) ) then

          NodoNivel(1,iNodo);
end;

begin  // CreaArbolDefault
    case iAccesoGlobal of
     //******** CONCILIACION **********
     K_CONCILIACION :
     begin
          NodoNivel( 0, K_RESUMEN_EMPLEADOS_CON_CREDITO);
          NodoNivel( 0, K_NUEVOS_EMPLEADOS_FONACOT);
          NodoNivel( 0, K_CREDITOS_NUEVOS);
          NodoNivel( 0, K_CREDITOS_DIFERENCIAS);
          NodoNivel( 0, K_CREDITOS_ACTIVOS_NO_CEDULA);
          NodoNivel( 0, K_CREDITOS_SALDADOS_ACTIVOS);
          NodoNivel( 0, K_ERRORES_CEDULA);

     end;
     //******** CONFRONTA  **********
     K_CONFRONTA :
     begin
          NodoNivel( 0, K_CEDULA_PAGO);
     end;



end;
end;


procedure CreaNavBarDefault ( oNavBarMgr: TNavBarMgr; Arbolitos: array of TcxTreeView);
function NodoNivel( iNodo, iGroupIndex: Integer): Boolean;
begin
     Result := oNavBarMgr.NodoNivel( GetGrupoInfo( iNodo ), iGroupIndex, iNodo );
end;
procedure CreaArbolito( const K_GLOBAL:Integer; Arbolito: TcxTreeView);
var
   oArbolMgr: TArbolMgr;
begin
     with Arbolito do
     begin
          Parent := oNavBarMgr.NavBar.Groups[oNavBarMgr.UltimoGrupo.Index].Control;
          Align := alClient;
     end;
     oArbolMgr := TArbolMgr.Create( Arbolito );
     with oArbolMgr do
     begin
          try
             with Arbolito.Items do
             begin
                  BeginUpdate;
                  Clear;
                  try
                     with oArbolMgr do
                     begin
                          CreaArbolitoDefault( oArbolMgr, K_GLOBAL  );
                          Arbol_Devex.FullCollapse;
                     end;
                  finally
                         EndUpdate;
                  end;
             end;
          finally
                 Free;
          end;
     end;
end;
begin
     {***OJO***}
     {El index del grupo comienza desde 1 porque el 0 es para el grupo del Arbol del Usuario}
     // ************ Presupuestos *****************

     begin
        if( NodoNivel( K_CONCILIACION, 0 )) then
        begin
          CreaArbolito ( K_CONCILIACION, Arbolitos[0]);
        end;
        if( NodoNivel( K_CONFRONTA, 1 )) then
        begin
          CreaArbolito ( K_CONFRONTA, Arbolitos[1]);
        end;

     end;
end;



end.
