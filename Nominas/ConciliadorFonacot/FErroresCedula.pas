unit FErroresCedula;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsultaFonacot, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls, ZBaseConsulta,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  dxSkinsCore, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, cxDBData, cxContainer, Vcl.Menus,
  System.Actions, Vcl.ActnList, Vcl.ImgList, cxLabel, ZetaKeyLookup_DevEx,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid;

type
  TErroresCedula = class(TBaseConsultaFonacot)
    Renglon: TcxGridDBColumn;
    N_FONACOT: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    NOM_COMPLETO: TcxGridDBColumn;
    N_CREDITO: TcxGridDBColumn;
    N_EMPLEADO: TcxGridDBColumn;
    RET_MENS: TcxGridDBColumn;
    PLAZO: TcxGridDBColumn;
    MESES: TcxGridDBColumn;
    NUM_ERROR: TcxGridDBColumn;
    DESC_ERROR: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  protected
     procedure Connect; override;
  public
    { Public declarations }
  end;

var
  ErroresCedula: TErroresCedula;

implementation

{$R *.dfm}

uses
    DConcilia, ZetaCommonClasses, ZetaDialogo, DCliente;


procedure TErroresCedula.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsCedulas;
end;

procedure TErroresCedula.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_ERRORES_CEDULA;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n error en la c�dula'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

end;

procedure TErroresCedula.FormShow(Sender: TObject);
begin
     inherited;
     ZetaDBGridDBTableView.ApplyBestFit();
     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('Renglon'), K_SIN_TIPO , '', skCount );
end;


end.
