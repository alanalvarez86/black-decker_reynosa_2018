unit FCreditosSaldadosActivos;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsulta, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  ZBaseConsultaFonacot, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxContainer, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxLabel,
  ZetaKeyLookup_DevEx, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  ZetaCXGrid, cxCalendar, TressMorado2013;

type
  TCreditosSaldadosActivos = class(TBaseConsultaFonacot)
    Posicion: TcxGridDBColumn;
    NFonacot: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    StatusEmp: TcxGridDBColumn;
    NCredito: TcxGridDBColumn;
    FechaInicial: TcxGridDBColumn;
    Status: TcxGridDBColumn;
    MPrestado: TcxGridDBColumn;
    Saldo: TcxGridDBColumn;
    RMensual: TcxGridDBColumn;
    Pagos: TcxGridDBColumn;
    TotalPrestamo: TcxGridDBColumn;
    SaldoAjuste: TcxGridDBColumn;
    Observa: TcxGridDBColumn;
    Reubicado: TcxGridDBColumn;
    PlazoTress: TcxGridDBColumn;
    TressCedula: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure StatusCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure CambiarPropiedadesTressColumnGrid(var ACanvas: TcxCanvas);
    procedure CambiarPropiedadesCedulaColumnGrid(var ACanvas: TcxCanvas);
    procedure FechaInicialCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure MPrestadoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure SaldoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure RMensualCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure PagosCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ReubicadoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure ObservaCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure PlazoTressCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TotalPrestamoCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure TressCedulaCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    { Private declarations }
  protected
     procedure Connect; override;


  public
    { Public declarations }
  end;

var
  CreditosSaldadosActivos: TCreditosSaldadosActivos;

implementation

{$R *.dfm}

uses
    ZetaCommonClasses, DConcilia, DCliente;

procedure TCreditosSaldadosActivos.Connect;
begin
     inherited;
     DataSource.DataSet := dmConcilia.cdsCreditosNoActivos;

end;

procedure TCreditosSaldadosActivos.FechaInicialCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesTressColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_CREDITOS_SALDADOS_ACTIVOS;
     if ( dmCliente.ConciliacionTerminada ) then
        ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No se encontr� ning�n cr�dito saldado que si est� activo en la c�dula'
     else
         ZetaDBGridDBTableView.OptionsView.NoDataToDisplayInfoText := 'No hay datos calculados';

end;

procedure TCreditosSaldadosActivos.FormShow(Sender: TObject);
begin
    inherited;
    CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('Posicion'), K_SIN_TIPO , '', skCount);
    ZetaDBGridDBTableView.ApplyBestFit();

     ZetaDBGridDBTableView.OptionsCustomize.ColumnGrouping := TRUE;
     ZetaDBGridDBTableView.OptionsView.GroupByBox := TRUE; //Muestra la caja de agrupamiento
     Posicion.Options.Grouping:= FALSE;
     NFonacot.Options.Grouping:= FALSE;
     NEmpleado.Options.Grouping:= FALSE;
     NomCompleto.Options.Grouping:= FALSE;
     StatusEmp.Options.Grouping:= FALSE;
     NCredito.Options.Grouping:= FALSE;
     FechaInicial.Options.Grouping:= FALSE;
     Status.Options.Grouping:= FALSE;
     MPrestado.Options.Grouping:= FALSE;
     Saldo.Options.Grouping:= FALSE;
     RMensual.Options.Grouping:= FALSE;
     Pagos.Options.Grouping:= FALSE;
     TotalPrestamo.Options.Grouping:= FALSE;
     SaldoAjuste.Options.Grouping:= FALSE;
     Observa.Options.Grouping:= FALSE;
     PlazoTress.Options.Grouping:= FALSE;
     TressCedula.Options.Grouping:= FALSE;
     Reubicado.Options.Grouping:= TRUE;
end;


procedure TCreditosSaldadosActivos.MPrestadoCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesTressColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.ObservaCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesCedulaColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.PagosCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesCedulaColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.PlazoTressCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
   CambiarPropiedadesTressColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.ReubicadoCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesCedulaColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.RMensualCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesCedulaColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.SaldoCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesTressColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.StatusCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesTressColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.TotalPrestamoCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  CambiarPropiedadesCedulaColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.TressCedulaCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
    CambiarPropiedadesCedulaColumnGrid(ACanvas);
end;

procedure TCreditosSaldadosActivos.CambiarPropiedadesTressColumnGrid (var ACanvas: TcxCanvas);
begin
     with ACanvas do
     begin
          Brush.Color := RGB(168,219,114);
     end;
end;

procedure TCreditosSaldadosActivos.CambiarPropiedadesCedulaColumnGrid (var ACanvas: TcxCanvas);
begin
     with ACanvas do
     begin
          Brush.Color := clSkyBlue;
     end;
end;

end.
