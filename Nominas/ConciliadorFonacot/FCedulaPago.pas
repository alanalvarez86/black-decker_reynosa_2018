unit FCedulaPago;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZBaseConsulta, Data.DB, Vcl.StdCtrls, Vcl.ExtCtrls,
  ZBaseConsultaFonacot, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData,
  cxContainer, Vcl.Menus, System.Actions, Vcl.ActnList, Vcl.ImgList, cxLabel,
  ZetaKeyLookup_DevEx, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, ZetaCXGrid,
  ZetaDBTextBox, cxTextEdit, cxMaskEdit, cxDropDownEdit,
  cxButtons, Vcl.Mask, ZetaNumero, ZetaCommonClasses;

type
  TCedulaPago = class(TBaseConsultaFonacot)
    NFonacot: TcxGridDBColumn;
    NEmpleado: TcxGridDBColumn;
    NomCompleto: TcxGridDBColumn;
    StatusEmp: TcxGridDBColumn;
    RFC: TcxGridDBColumn;
    NCredito: TcxGridDBColumn;
    RetCedula: TcxGridDBColumn;
    RetTress: TcxGridDBColumn;
    Incidencia: TcxGridDBColumn;
    FecInicio: TcxGridDBColumn;
    FecFin: TcxGridDBColumn;
    SaldoAjus: TcxGridDBColumn;
    Observa: TcxGridDBColumn;
    cboFiltros: TcxComboBox;
    Panel10: TPanel;
    ztxtRetMensual: TZetaTextBox;
    ztxtRetTress: TZetaTextBox;
    ztxtEmpleados: TZetaTextBox;
    ztxtCreditos: TZetaTextBox;
    ztxtBajas: TZetaTextBox;
    ztxtIncapacidades: TZetaTextBox;
    Label14: TcxLabel;
    Label15: TcxLabel;
    Label16: TcxLabel;
    Label17: TcxLabel;
    Label18: TcxLabel;
    Label19: TcxLabel;
    ztxtAltas: TZetaTextBox;
    Label21: TcxLabel;
    Label22: TcxLabel;
    ztxtStatus: TZetaTextBox;
    ztxtNumCredCed: TZetaTextBox;
    Label23: TcxLabel;
    cxLabel1: TcxLabel;
    Label11: TLabel;
    nMontos: TZetaNumero;
    BtnFiltro1: TcxButton;
    DiferenciaCredito: TcxGridDBColumn;
    ztxtAReportar: TZetaTextBox;
    cxLabel2: TcxLabel;
    procedure FormCreate(Sender: TObject);
    procedure cboFiltrosPropertiesChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RetCedulaCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure RetTressCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure nMontosExit(Sender: TObject);
    procedure BtnFiltro1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostrarTotalesConfronta;
    procedure HabilitarDiferencias( lHabilitar: Boolean );
    function GetDiferencia( sFiltroDiferencia: String ): String;
    procedure CambiarPropiedadesTextoGrid(var ACanvas: TcxCanvas);
    procedure ObtenerValoresComparar( var dCRetencion, dTressRetencion : TPesos; var AViewInfo: TcxGridTableDataCellViewInfo );
  protected
     procedure Connect; override;

  public
    { Public declarations }
  end;

var
  CedulaPago: TCedulaPago;

implementation

{$R *.dfm}

uses
    Dconcilia, ZetaCommonTools, ZetaCommonLists,
    DCliente;

procedure TCedulaPago.Connect;
begin
     DataSource.DataSet := dmConcilia.cdsConfrontacion;
     dmCliente.SetValorRutaCedula( K_TIPO_CONFRONTA );
     dmCliente.SetValorRazonSocial( K_TIPO_CONFRONTA );

end;

procedure TCedulaPago.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_CONCIL_FONACOT_CEDULA_PAGO;
     if ( dmCliente.ConfrontaTerminada ) then
        MostrarTotalesConfronta;
     HabilitarDiferencias( FALSE );
end;

procedure TCedulaPago.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     PanelIdentifica.Visible := True;
     ValorActivo1.Visible := dmCliente.ConfrontaTerminada;
     ValorActivo2.Visible := dmCliente.ConfrontaTerminada;
     PanelIdentifica.Visible := True;

     if ( StrVacio( textoValorActivo2.Caption ) ) then
     begin
          ValorActivo2.Visible := False;
          ValorActivo1.Align := alClient;
     end;

     textoValorActivo1.Hint := textoValorActivo1.Caption;
     textoValorActivo2.Hint := 'Raz�n Social: ' + dmCliente.ValorRazonSocial
                + ': ' + dmCliente.cdsRSocial.GetDescripcion( dmCliente.ValorRazonSocial );

     CreaColumaSumatoria( ZetaDBGridDBTableView.GetColumnByFieldName('NFonacot'), K_SIN_TIPO , '', skCount );
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TCedulaPago.cboFiltrosPropertiesChange(Sender: TObject);
const
     K_FLITRO_DIFERENCIA = 'RetCedula <> RetTress';
var
   sFiltro :string;
   lHabilitar: Boolean;
begin
     inherited;
     lHabilitar := FALSE;
     case cboFiltros.ItemIndex of
          0: sFiltro := VACIO;
          1: begin
                  sFiltro := GetDiferencia( K_FLITRO_DIFERENCIA );
                  lHabilitar := TRUE;
             end;
          2: sFiltro := Format( 'Incidencia = %s',[ EntreComillas('B') ] );
          3: sFiltro := Format( 'Incidencia = %s',[ EntreComillas('I') ] );
          4: sFiltro := Format( 'Incidencia = %s',[ EntreComillas('A') ] );
          5: sFiltro := Format( 'StatusPrestamo = %d',[ Ord(spSaldado) ] );
     end;

     HabilitarDiferencias( lHabilitar );

     with DataSource.DataSet do
     begin
          Filtered := FALSE;
          Filter := sFiltro;
          Filtered := StrLleno(sFiltro);
     end;
end;

procedure TCedulaPago.MostrarTotalesConfronta;
begin
     if ( dmConcilia.cdsTotales.Active ) then
     begin
           with dmConcilia.cdsTotales do
           begin
                //Sumatoria de Cedula ..
                ztxtRetMensual.Caption  := FormatFloat( '#,0.00', dmConcilia.TotalCedula );
                ztxtRetTress.Caption  :=  FormatFloat( '#,0.00',FieldByName('FT_NOMINA').AsFloat + FieldByName('FT_AJUSTE').AsFloat );
                ztxtEmpleados.Caption  := IntToStr ( FieldByName('FT_EMPLEAD').AsInteger );
                ztxtCreditos.Caption  := IntToStr ( FieldByName('FT_CUANTOS').AsInteger );
                ztxtBajas.Caption  := IntToStr ( FieldByName('FT_BAJAS').AsInteger );
                ztxtIncapacidades.Caption  := IntToStr ( FieldByName('FT_INCAPA').AsInteger );
                ztxtAltas.Caption  := IntToStr( dmConcilia.TotalAltas );
                ztxtStatus.Caption  := dmConcilia.GetStatusCalculoFonacot;
                ztxtNumCredCed.Caption := IntToStr( dmConcilia.TotalCredCed);
                ztxtAReportar.Caption :=  FormatFloat( '#,0.00', ( ( ( FieldByName('FT_NOMINA').AsFloat + FieldByName('FT_AJUSTE').AsFloat ) * 100 ) / dmConcilia.TotalCedula ) ) + '%';
           end;
     end;

end;

procedure TCedulaPago.nMontosExit(Sender: TObject);
begin
     inherited;
     cboFiltrosPropertiesChange( Self );
end;

procedure TCedulaPago.HabilitarDiferencias( lHabilitar: Boolean );
begin
     Label11.Enabled := lHabilitar;
     nMontos.Enabled := lHabilitar;
     BtnFiltro1.Enabled := lHabilitar;
end;

function TCedulaPago.GetDiferencia( sFiltroDiferencia: String ): String;
begin
     if StrLleno( sFiltroDiferencia ) then
     sFiltroDiferencia := Parentesis( sFiltroDiferencia );
     sFiltroDiferencia := ConcatFiltros( sFiltroDiferencia, Parentesis( 'DIFERENCIA>' +  FloatToStr(nMontos.Valor) ) );
     Result := sFiltroDiferencia;
end;

procedure TCedulaPago.BtnFiltro1Click(Sender: TObject);
begin
     inherited;
     with ZetaDBGridDBTableView do
     begin
          if Visible and Enabled then
             SetFocus;
     end;
end;

procedure TCedulaPago.CambiarPropiedadesTextoGrid(var ACanvas: TcxCanvas);
begin
     with ACanvas do
     begin
          Font.Style := [fsBold];
          Font.Color := clRed;
     end;
end;

procedure TCedulaPago.ObtenerValoresComparar(var dCRetencion, dTressRetencion : TPesos; var AViewInfo: TcxGridTableDataCellViewInfo);
begin
     dCRetencion := AViewInfo.GridRecord.Values[RetCedula.Index];
     dTressRetencion := AViewInfo.GridRecord.Values[RetTress.Index];
end;

procedure TCedulaPago.RetCedulaCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   dCRetencion: TPesos;
   dTressRetencion: TPesos;
begin
     ObtenerValoresComparar( dCRetencion, dTressRetencion, AViewInfo);
     if dCRetencion <> dTressRetencion then
        CambiarPropiedadesTextoGrid( ACanvas );
end;

procedure TCedulaPago.RetTressCustomDrawCell(Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
          AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
   dCRetencion: TPesos;
   dTressRetencion: TPesos;
begin
     ObtenerValoresComparar( dCRetencion, dTressRetencion, AViewInfo);
     if dCRetencion <> dTressRetencion then
        CambiarPropiedadesTextoGrid( ACanvas );
end;

end.
