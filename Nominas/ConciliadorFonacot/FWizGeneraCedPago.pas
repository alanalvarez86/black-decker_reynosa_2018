unit FWizGeneraCedPago;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, cxContainer, cxEdit, cxProgressBar, Vcl.ExtCtrls, ZetaCXWizard,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, Vcl.Menus, Vcl.StdCtrls, cxTextEdit, cxButtons;

type
  TWizGeneraCedPago = class(TcxBaseWizard)
    PanelProgressBar: TPanel;
    ProgressBar: TcxProgressBar;
    gbArchivoCedula: TcxGroupBox;
    cxLabel1: TcxLabel;
    btnGuardaCedula: TcxButton;
    txtPathGuardaCedula: TcxTextEdit;
    lBisiestos: TCheckBox;
    DialogoCedula: TOpenDialog;
    procedure btnGuardaCedulaClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    procedure lBisiestosClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure txtPathGuardaCedulaPropertiesEditValueChanged(Sender: TObject);

  private
    { Private declarations }
    function GetMes:string;
  protected
    function EjecutarWizard: Boolean;override;
    procedure CargaParametros;override;
  public
    { Public declarations }
  end;

var
  WizGeneraCedPago: TWizGeneraCedPago;

implementation

{$R *.dfm}


uses
    ZetaCommonTools, ZetaDialogo, FTressShell, ZetaCommonClasses,
    ZetaClientTools, ZetaCommonLists, DConcilia, DCliente;


procedure TWizGeneraCedPago.btnGuardaCedulaClick(Sender: TObject);
begin
     inherited;
     txtPathGuardaCedula.Text := ZetaClientTools.AbreDialogo( DialogoCedula, txtPathGuardaCedula.Text, 'log' );
end;

function TWizGeneraCedPago.EjecutarWizard: Boolean;
begin
     TressShell.SetProgressbar( Self.ProgressBar);
     with dmConcilia do
     begin
          begin
               GetcdsAuxiliar(lBisiestos.Checked);
               TressShell.ConciliarStep;
               ZInformation('Generar c�dula de pago', CrearAsciiCedula( cdsAuxiliar,txtPathGuardaCedula.Text ),0);
          end;
     end;
     TressShell.ConciliarEnd;

end;



procedure TWizGeneraCedPago.FormCreate(Sender: TObject);
begin
     inherited;
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;

     HelpContext := H_CONCIL_FONACOT_GENERAR_CEDULA;

     with dmConcilia, Application do
     begin
          txtPathGuardaCedula.Text  := ZetaCommonTools.VerificaDir( ExtractFilePath( dmCliente.CedulaUltimaConfronta ) )
                                  + Format('Cedula_%s_Pago.csv',[ GetMes ]);
     end;
end;

procedure TWizGeneraCedPago.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Al aplicar este proceso se generar� la c�dula de pago con los par�metros indicados.';

end;

procedure TWizGeneraCedPago.lBisiestosClick(Sender: TObject);
begin
     inherited;
     dmConcilia.EnMascaraFechas( lBisiestos.Checked );

end;

procedure TWizGeneraCedPago.txtPathGuardaCedulaPropertiesEditValueChanged(
  Sender: TObject);
begin
     inherited;
     if ( Length( txtPathGuardaCedula.Text) < 68 )  then
        txtPathGuardaCedula.Properties.Alignment.Horz := taLeftJustify
     else
         txtPathGuardaCedula.Properties.Alignment.Horz := taRightJustify;

end;

procedure TWizGeneraCedPago.WizardBeforeMove(Sender: TObject;
  var iNewPage: Integer; var CanMove: Boolean);
begin
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( StrVacio( txtPathGuardaCedula.Text ) ) then
               begin
                     ZetaDialogo.ZError( 'Error al generar c�dula', 'El archivo de c�dula de Fonacot est� vac�o', 0 );
                     CanMove := False;
               end
               else if FileExists(txtPathGuardaCedula.Text) then
               begin
                    CanMove := ZConfirm('Generar c�dula Fonacot','El archivo de la c�dula de Fonacot ya existe �Desea reemplazarlo?',0,mbYes);
               end;
          end;
     end;
end;

procedure TWizGeneraCedPago.CargaParametros;
begin
     with ParameterList do
     begin
          AddString( 'ArchivoCedula', txtPathGuardaCedula.Text );
          AddBoolean( 'ConsiderarBisiestos', lBisiestos.Checked );
     end;

     with Descripciones do
     begin
          AddString( 'Guardar c�dula en', txtPathGuardaCedula.Text );
          AddString( 'Considerar a�os bisiestos', BoolToSiNo( lBisiestos.Checked ) );
     end;
     inherited;
end;

function TWizGeneraCedPago.GetMes: string;
begin
     Result := ObtieneElemento( lfMeses, dmCliente.MesConfronta - 1 );
end;


end.
