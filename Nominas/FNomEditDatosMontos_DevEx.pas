unit FNomEditDatosMontos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, Db, ExtCtrls, DBCtrls, StdCtrls, Mask,
     {$ifndef VER130}
     Variants,
     {$endif}
     ZetaDBTextBox,
     ZetaNumero, ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics,
     cxLookAndFeels, cxLookAndFeelPainters, Menus, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     cxControls, dxSkinsdxBarPainter, dxBarExtItems, dxBar, cxClasses,
     ImgList, cxNavigator, cxDBNavigator, cxButtons, cxContainer, cxEdit,
     ZetaKeyLookup_DevEx, cxGroupBox;

type
  TNomEditDatosMontos_DevEx = class(TBaseEdicion_DevEx)
    Label1: TLabel;
    GroupBox1: TcxGroupBox;
    ExentaLbl: TLabel;
    MO_IMP_CALLbl: TLabel;
    MO_PER_CAL: TDBCheckBox;
    MO_X_ISPT: TZetaDBNumero;
    MO_IMP_CAL: TZetaDBNumero;
    Label6: TLabel;
    US_CODIGO: TZetaDBTextBox;
    PercepcionLbl: TLabel;
    MO_PERCEPC: TZetaDBNumero;
    DeduccionLbl: TLabel;
    MO_DEDUCCI: TZetaDBNumero;
    ReferenciaLbl: TLabel;
    MO_REFEREN: TDBEdit;
    MO_ACTIVO: TDBCheckBox;
    CO_NUMERO: TZetaDBKeyLookup_DevEx;
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure MO_ACTIVOClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MO_PER_CALClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    procedure AsignaValoresPercepDeduc;
    procedure EvaluaTipoConcepto;
    procedure ShowPercepDeduc( const PercepState, DeducState: Boolean );
    procedure ShowControls( const lState: Boolean );
    procedure ShowImpuestos( const lState: Boolean);
    procedure ShowIndividual( const lState: Boolean );
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure EscribirCambios; override;
  public
    { Public declarations }
  end;

var
  NomEditDatosMontos_DevEx: TNomEditDatosMontos_DevEx;

implementation

uses DCatalogos,
     DNomina,
     DSistema,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaClientDataSet,
     ZAccesosTress;

{$R *.DFM}

{ TNomEditDatosMontos }

procedure TNomEditDatosMontos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     IndexDerechos := D_NOM_DATOS_MONTOS;
     HelpContext := H33131_Edicion_montos;
     FirstControl := CO_NUMERO;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     CO_NUMERO.LookupDataset := dmCatalogos.cdsConceptos;
end;

procedure TNomEditDatosMontos_DevEx.Connect;
begin
     dmCatalogos.cdsConceptos.Conectar;
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          cdsMovMontos.Conectar;
          DataSource.DataSet:= cdsMovMontos;
     end;
end;

procedure TNomEditDatosMontos_DevEx.EvaluaTipoConcepto;
var
   iTipo: eTipoConcepto;
begin
     with DataSource.DataSet do
     begin
          if ( FieldByName( 'CO_TIPO' ) <> nil ) then
          begin
               iTipo := eTipoConcepto( FieldByName( 'CO_TIPO' ).AsInteger );
               if ( iTipo in [ coDeduccion, coObligacion ] ) then
                  ShowPercepDeduc( False, True )
               else
                   ShowPercepDeduc( True, False );
          end;
     end;
end;

procedure TNomEditDatosMontos_DevEx.ShowControls( const lState: Boolean );
begin
     if lState then
        EvaluaTipoConcepto
     else
         ShowPercepDeduc( lState, lState );
     ShowImpuestos( lState );
     ReferenciaLBL.Enabled := lState;
     MO_REFEREN.Enabled := lState;
end;

procedure TNomEditDatosMontos_DevEx.ShowImpuestos( const lState: Boolean );
begin
     ExentaLbl.Enabled := lState;
     MO_X_ISPT.Enabled := lState;
     MO_PER_CAL.Enabled :=lState;
     if lState then
        ShowIndividual( MO_PER_CAL.Checked )
     else
         ShowIndividual( lState );
end;

procedure TNomEditDatosMontos_DevEx.ShowIndividual( const lState: Boolean );
begin
     MO_IMP_CAL.Enabled := lState;
     MO_IMP_CALLbl.Enabled := lState;
end;

procedure TNomEditDatosMontos_DevEx.ShowPercepDeduc( const PercepState, DeducState: Boolean );
var
   oControl: TWinControl;
   lCambiarFocus: Boolean;
begin
     oControl := nil;
     lCambiarFocus := False;
     if ( ActiveControl = MO_PERCEPC ) and ( not PercepState ) then
     begin
          oControl := MO_DEDUCCI;
          lCambiarFocus := True;
     end;
     if ( ActiveControl = MO_DEDUCCI ) and ( PercepState ) then
     begin
          oControl := MO_PERCEPC;
          lCambiarFocus := True;
     end;
     PercepcionLbl.Enabled := PercepState;
     MO_PERCEPC.Enabled := PercepState;
     DeduccionLbl.Enabled := DeducState;
     MO_DEDUCCI.Enabled := DeducState;
     if lCambiarFocus then
        oControl.SetFocus;
end;

procedure TNomEditDatosMontos_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     if ( DataSource.DataSet.State = dsBrowse ) then
     begin
          ShowControls( MO_ACTIVO.Checked );
          { PENDIENTE Carlos : El ShowImpuestos sale sobrando pues lo hace el ShowControls }
          ShowImpuestos( MO_ACTIVO.Checked );
          if MO_ACTIVO.Checked then
             EvaluaTipoConcepto
          else
              ShowPercepDeduc( MO_ACTIVO.Checked, MO_ACTIVO.Checked );
     end;
     if ( Field <> nil ) and ( Field.FieldName = 'CO_NUMERO' ) then
     begin
          EvaluaTipoConcepto;
          AsignaValoresPercepDeduc;
     end;
end;

procedure TNomEditDatosMontos_DevEx.AsignaValoresPercepDeduc;
begin
     with DataSource.DataSet do
     begin
          if MO_PERCEPC.Enabled then
             FieldByName( 'MO_DEDUCCI' ).AsFloat := 0
          else
              FieldByName( 'MO_PERCEPC' ).AsFloat := 0
     end;
end;

procedure TNomEditDatosMontos_DevEx.MO_ACTIVOClick(Sender: TObject);
begin
     inherited;
     ShowControls( MO_ACTIVO.Checked );
end;

procedure TNomEditDatosMontos_DevEx.MO_PER_CALClick(Sender: TObject);
begin
     inherited;
     ShowIndividual( MO_PER_CAL.Checked );
end;

procedure TNomEditDatosMontos_DevEx.EscribirCambios;
begin
     dmNomina.OperacionConflicto := Ord( ocReportar );
     inherited EscribirCambios;
end;

procedure TNomEditDatosMontos_DevEx.OK_DevExClick(Sender: TObject);
var
   iEmpleado, iConcepto: Integer;
   sReferencia: String;
begin
     with dmNomina do
     begin
          ConflictoMontos:= FALSE;

          inherited;

          if ConflictoMontos then
          begin
               with TZetaClientDataSet( Datasource.DataSet ) do
               begin
                    iEmpleado:= FieldByName( 'CB_CODIGO' ).AsInteger;
                    iConcepto:= FieldByName( 'CO_NUMERO' ).AsInteger;
                    sReferencia:= FieldByName( 'MO_REFEREN' ).AsString;
                    Refrescar;
                    Locate( 'CB_CODIGO;CO_NUMERO;MO_REFEREN',VarArrayOf([ iEmpleado, iConcepto, sReferencia]),[]);
               end;
          end;
     end;
end;

end.


