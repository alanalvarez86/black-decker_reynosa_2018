inherited EditCosteoTransferencias_DevEx: TEditCosteoTransferencias_DevEx
  Left = 312
  Top = 237
  Caption = 'Transferencias'
  ClientHeight = 424
  ClientWidth = 497
  PixelsPerInch = 96
  TextHeight = 13
  object TipoAutLbl: TLabel [0]
    Left = 126
    Top = 219
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object HorasLbl: TLabel [1]
    Left = 119
    Top = 144
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Horas:'
    FocusControl = TR_HORAS
  end
  object MotivoLbl: TLabel [2]
    Left = 115
    Top = 169
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Motivo:'
    FocusControl = TR_MOTIVO
  end
  object EmpleadoLbl: TLabel [3]
    Left = 100
    Top = 69
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label3: TLabel [4]
    Left = 100
    Top = 245
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Transfiere:'
    FocusControl = TR_MOTIVO
  end
  object US_DESCRIP: TZetaDBTextBox [5]
    Left = 152
    Top = 241
    Width = 198
    Height = 21
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lbDestino: TLabel [6]
    Left = 77
    Top = 119
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'CC a Transferir:'
    FocusControl = CC_CODIGO
  end
  object Label5: TLabel [7]
    Left = 117
    Top = 45
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
    FocusControl = CB_CODIGO
  end
  object CC_ORIGEN: TZetaDBTextBox [8]
    Left = 152
    Top = 90
    Width = 60
    Height = 21
    AutoSize = False
    Caption = 'CC_ORIGEN'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CC_ORIGEN'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lbOrigen: TLabel [9]
    Left = 99
    Top = 94
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'CC Origen:'
    FocusControl = CC_CODIGO
  end
  object CC_DESCRIP: TZetaDBTextBox [10]
    Left = 216
    Top = 90
    Width = 236
    Height = 21
    AutoSize = False
    Caption = 'CC_DESCRIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CC_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object TR_FECHA: TZetaDBTextBox [11]
    Left = 352
    Top = 241
    Width = 100
    Height = 21
    AutoSize = False
    Caption = 'TR_FECHA'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'TR_FECHA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [12]
    Left = 76
    Top = 194
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
  end
  object Label1: TLabel [13]
    Left = 117
    Top = 268
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Global:'
    FocusControl = TR_MOTIVO
  end
  inherited PanelBotones: TPanel
    Top = 388
    Width = 497
    TabOrder = 11
    inherited OK_DevEx: TcxButton
      Left = 333
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 412
      Cancel = True
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 497
    TabOrder = 0
    inherited ValorActivo2: TPanel
      Width = 171
      inherited textoValorActivo2: TLabel
        Width = 165
      end
    end
  end
  inherited DevEx_cxDBNavigatorEdicion: TcxDBNavigator
    Left = 0
    Top = 8
    TabOrder = 15
  end
  object CB_CODIGO: TZetaDBKeyLookup_DevEx [17]
    Left = 152
    Top = 65
    Width = 302
    Height = 21
    LookupDataset = dmCliente.cdsEmpleadoLookUp
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 2
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object TR_MOTIVO: TZetaDBKeyLookup_DevEx [18]
    Left = 152
    Top = 165
    Width = 302
    Height = 21
    LookupDataset = dmTablas.cdsMotivoTransfer
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 5
    TabStop = True
    WidthLlave = 60
    DataField = 'TR_MOTIVO'
    DataSource = DataSource
  end
  object TR_HORAS: TZetaDBNumero [19]
    Left = 152
    Top = 140
    Width = 60
    Height = 21
    Mascara = mnHoras
    TabOrder = 4
    Text = '0.00'
    DataField = 'TR_HORAS'
    DataSource = DataSource
  end
  object CC_CODIGO: TZetaDBKeyLookup_DevEx [20]
    Left = 152
    Top = 115
    Width = 302
    Height = 21
    LookupDataset = dmTablas.cdsNivel1
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 60
    DataField = 'CC_CODIGO'
    DataSource = DataSource
  end
  object AU_FECHA: TZetaDBFecha [21]
    Left = 152
    Top = 40
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 1
    Text = '04/mar/11'
    Valor = 40606.000000000000000000
    DataField = 'AU_FECHA'
    DataSource = DataSource
  end
  object TR_TIPO: TZetaDBKeyCombo [22]
    Left = 152
    Top = 215
    Width = 198
    Height = 21
    AutoComplete = False
    BevelKind = bkFlat
    Style = csDropDownList
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 7
    ListaFija = lfTipoHoraTransferenciaCosteo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TR_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TR_TEXTO: TZetaDBEdit [23]
    Left = 152
    Top = 190
    Width = 300
    Height = 21
    TabOrder = 6
    Text = 'TR_TEXTO'
    DataField = 'TR_TEXTO'
    DataSource = DataSource
  end
  object TR_GLOBAL: TDBCheckBox [24]
    Left = 152
    Top = 266
    Width = 15
    Height = 17
    DataField = 'TR_GLOBAL'
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 8
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object GroupBox1: TcxGroupBox [25]
    Left = 0
    Top = 288
    Align = alBottom
    Caption = ' Aprobaci'#243'n: '
    TabOrder = 9
    Height = 100
    Width = 497
    object Label4: TLabel
      Left = 93
      Top = 19
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
      Transparent = True
    end
    object TR_FEC_APR: TZetaDBTextBox
      Left = 328
      Top = 63
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'TR_FEC_APR'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TR_FEC_APR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label6: TLabel
      Left = 65
      Top = 43
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Comentarios:'
      Transparent = True
    end
    object Label7: TLabel
      Left = 89
      Top = 67
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'Recibe:'
      FocusControl = TR_MOTIVO
      Transparent = True
    end
    object ZetaDBTextBox1: TZetaDBTextBox
      Left = 128
      Top = 63
      Width = 198
      Height = 21
      AutoSize = False
      Caption = 'ZetaDBTextBox1'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_APRUEBA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TR_STATUS: TZetaDBKeyCombo
      Left = 128
      Top = 15
      Width = 198
      Height = 21
      AutoComplete = False
      BevelKind = bkFlat
      Style = csDropDownList
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 0
      ListaFija = lfStatusTransCosteo
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'TR_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object TR_TXT_APR: TZetaDBEdit
      Left = 128
      Top = 39
      Width = 300
      Height = 21
      TabOrder = 1
      Text = 'TR_TXT_APR'
      DataField = 'TR_TXT_APR'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 420
    Top = 1
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
    DesignInfo = 8388776
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    Left = 392
    Top = 8
    DockControlHeights = (
      0
      0
      31
      0)
    inherited dxBarButton_ExportarBtn: TdxBarButton
      Visible = ivNever
    end
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
    DesignInfo = 524648
  end
end
