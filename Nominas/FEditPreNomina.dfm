inherited EditPrenomina: TEditPrenomina
  Left = 202
  Top = 201
  Caption = 'Pre-N�mina'
  ClientHeight = 348
  ClientWidth = 539
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 312
    Width = 539
    inherited OK: TBitBtn
      Left = 371
    end
    inherited Cancelar: TBitBtn
      Left = 456
    end
    object BBModificar: TBitBtn
      Left = 24
      Top = 6
      Width = 121
      Height = 25
      Caption = '&Modificar Rengl�n'
      TabOrder = 2
      OnClick = BBModificarClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000010000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        00003333330FFFFFFFF000300000FF0F00F0E00BFBFB0FFFFFF0E0BFBF000FFF
        F0F0E0FBFBFBF0F00FF0E0BFBF00000B0FF0E0FBFBFBFBF0FFF0E0BF0000000F
        FFF0000BFB00B0FF00F03330000B0FFFFFF0333330B0FFFF000033330B0FF00F
        0FF03330B00FFFFF0F033309030FFFFF00333330330000000333}
    end
  end
  inherited PanelSuperior: TPanel
    Width = 539
    inherited DBNavigator: TDBNavigator
      Visible = False
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 539
    inherited ValorActivo2: TPanel
      Width = 213
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 51
    Width = 539
    Height = 110
    ActivePage = TabSheet1
    Align = alTop
    TabOrder = 3
    TabPosition = tpBottom
    object TabSheet1: TTabSheet
      Caption = 'D�as'
      object Label17: TLabel
        Left = 27
        Top = 4
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarios:'
      end
      object NO_DIAS: TZetaDBTextBox
        Left = 80
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 26
        Top = 22
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_DIAS_AS: TZetaDBTextBox
        Left = 80
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label19: TLabel
        Left = 382
        Top = 4
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas Injustificadas:'
      end
      object NO_DIAS_FI: TZetaDBTextBox
        Left = 484
        Top = 2
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label20: TLabel
        Left = 387
        Top = 22
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas Justificadas:'
      end
      object NO_DIAS_FJ: TZetaDBTextBox
        Left = 484
        Top = 20
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 155
        Top = 4
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incapacidad:'
      end
      object NO_DIAS_IN: TZetaDBTextBox
        Left = 224
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_IN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_IN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 4
        Top = 40
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'No Trabajados:'
      end
      object NO_DIAS_NT: TZetaDBTextBox
        Left = 80
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_NT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_NT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 130
        Top = 22
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso Sin Goce:'
      end
      object NO_DIAS_SG: TZetaDBTextBox
        Left = 224
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 18
        Top = 58
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Suspensi�n:'
      end
      object NO_DIAS_SU: TZetaDBTextBox
        Left = 80
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SU'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SU'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 387
        Top = 58
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aguinaldo a Pagar:'
      end
      object NO_DIAS_AG: TZetaDBTextBox
        Left = 484
        Top = 56
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_VA: TZetaDBTextBox
        Left = 484
        Top = 38
        Width = 35
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_VA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_VA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 378
        Top = 40
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones a Pagar:'
      end
      object NO_DIAS_RE: TZetaDBTextBox
        Left = 326
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_RE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_RE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label30: TLabel
        Left = 274
        Top = 22
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retardos:'
      end
      object NO_DIAS_AJ: TZetaDBTextBox
        Left = 326
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label29: TLabel
        Left = 288
        Top = 4
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ajuste:'
      end
      object NO_DIAS_OT: TZetaDBTextBox
        Left = 224
        Top = 55
        Width = 38
        Height = 18
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_OT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_OT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label28: TLabel
        Left = 145
        Top = 58
        Width = 73
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros Permisos:'
      end
      object NO_DIAS_CG: TZetaDBTextBox
        Left = 224
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label26: TLabel
        Left = 287
        Top = 58
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'E Y M:'
      end
      object NO_DIAS_EM: TZetaDBTextBox
        Left = 326
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_EM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_EM'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label27: TLabel
        Left = 127
        Top = 40
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso Con Goce:'
      end
      object NO_DIAS_SS: TZetaDBTextBox
        Left = 326
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label25: TLabel
        Left = 290
        Top = 40
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'IMSS:'
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Horas'
      object Label4: TLabel
        Left = 7
        Top = 6
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
      end
      object NO_HORAS: TZetaDBTextBox
        Left = 61
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 108
        Top = 42
        Width = 78
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima Dominical:'
      end
      object NO_Hora_PD: TZetaDBTextBox
        Left = 191
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_Hora_PD'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_Hora_PD'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DES_TRA: TZetaDBTextBox
        Left = 346
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 238
        Top = 6
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descanso Trabajado:'
      end
      object NO_EXTRAS: TZetaDBTextBox
        Left = 61
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_EXTRAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 25
        Top = 24
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
      end
      object Label6: TLabel
        Left = 21
        Top = 42
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
      end
      object NO_TRIPLES: TZetaDBTextBox
        Left = 61
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TRIPLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TRIPLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 252
        Top = 24
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo Trabajado:'
      end
      object NO_FES_TRA: TZetaDBTextBox
        Left = 346
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_VAC_TRA: TZetaDBTextBox
        Left = 346
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_VAC_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_VAC_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label13: TLabel
        Left = 241
        Top = 42
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaci�n Trabajada:'
      end
      object NO_DOBLES: TZetaDBTextBox
        Left = 61
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DOBLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DOBLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 21
        Top = 60
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
      end
      object Label8: TLabel
        Left = 150
        Top = 6
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tardes:'
      end
      object NO_TARDES: TZetaDBTextBox
        Left = 191
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TARDES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TARDES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label14: TLabel
        Left = 393
        Top = 6
        Width = 91
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso Con Goce:'
      end
      object NO_HORA_CG: TZetaDBTextBox
        Left = 490
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_HORA_SG: TZetaDBTextBox
        Left = 490
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label15: TLabel
        Left = 397
        Top = 24
        Width = 87
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso Sin Goce:'
      end
      object NO_ADICION: TZetaDBTextBox
        Left = 191
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_ADICION'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_ADICION'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 129
        Top = 24
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Adicionales:'
      end
      object Label36: TLabel
        Left = 407
        Top = 42
        Width = 77
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo Pagado:'
      end
      object NO_FES_PAG: TZetaDBTextBox
        Left = 490
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_PAG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_PAG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Turno'
      object Label1: TLabel
        Left = 52
        Top = 4
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object CB_TURNO: TZetaDBTextBox
        Left = 86
        Top = 2
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 57
        Top = 23
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D�as:'
      end
      object Label3: TLabel
        Left = 42
        Top = 43
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada:'
      end
      object NO_D_TURNO: TZetaDBTextBox
        Left = 86
        Top = 21
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_D_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_D_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_JORNADA: TZetaDBTextBox
        Left = 86
        Top = 41
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_JORNADA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_JORNADA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object TURNO: TZetaDBTextBox
        Left = 153
        Top = 2
        Width = 217
        Height = 17
        AutoSize = False
        Caption = 'TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object bbMostrarCalendario: TSpeedButton
        Left = 372
        Top = 0
        Width = 23
        Height = 22
        Hint = 'Mostrar Calendario del Turno'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
          003337777777777777F330FFFFFFFFFFF03337F3333FFF3337F330FFFF000FFF
          F03337F33377733337F330FFFFF0FFFFF03337F33337F33337F330FFFF00FFFF
          F03337F33377F33337F330FFFFF0FFFFF03337F33337333337F330FFFFFFFFFF
          F03337FFF3F3F3F3F7F33000F0F0F0F0F0333777F7F7F7F7F7F330F0F000F070
          F03337F7F777F777F7F330F0F0F0F070F03337F7F7373777F7F330F0FF0FF0F0
          F03337F733733737F7F330FFFFFFFF00003337F33333337777F330FFFFFFFF0F
          F03337FFFFFFFF7F373330999999990F033337777777777F733330FFFFFFFF00
          333337FFFFFFFF77333330000000000333333777777777733333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = bbMostrarCalendarioClick
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Generales'
      object Label34: TLabel
        Left = 34
        Top = 44
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific�:'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 81
        Top = 42
        Width = 138
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label35: TLabel
        Left = 26
        Top = 62
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_USER_RJ: TZetaDBTextBox
        Left = 81
        Top = 60
        Width = 138
        Height = 17
        AutoSize = False
        Caption = 'NO_USER_RJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_USER_RJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_OBSERVA: TZetaDBTextBox
        Left = 82
        Top = 24
        Width = 279
        Height = 17
        AutoSize = False
        Caption = 'NO_OBSERVA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_OBSERVA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label16: TLabel
        Left = 4
        Top = 26
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
      object Label33: TLabel
        Left = 45
        Top = 8
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object STATUSNOM: TZetaDBTextBox
        Left = 82
        Top = 6
        Width = 137
        Height = 17
        AutoSize = False
        Caption = 'STATUSNOM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
  end
  object DBGrid: TDBGrid [4]
    Left = 0
    Top = 161
    Width = 539
    Height = 151
    Align = alClient
    Ctl3D = True
    DataSource = dsAusencia
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGridDblClick
    Columns = <
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'au_posicio'
        ReadOnly = True
        Title.Alignment = taRightJustify
        Title.Caption = '#'
        Width = 20
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'AU_FECHA'
        ReadOnly = True
        Title.Caption = 'Fecha'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_HORAS'
        Title.Alignment = taRightJustify
        Title.Caption = 'Ordinarias'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_DOBLES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Dobles'
        Width = 39
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_TRIPLES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Triples'
        Width = 36
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_PER_CG'
        Title.Alignment = taRightJustify
        Title.Caption = 'Permiso CG'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_PER_SG'
        Title.Alignment = taRightJustify
        Title.Caption = 'Permiso SG'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_DES_TRA'
        Title.Alignment = taRightJustify
        Title.Caption = 'Des. Tra.'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AU_TARDES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Tardes'
        Width = 43
        Visible = True
      end
      item
        Alignment = taRightJustify
        Expanded = False
        FieldName = 'US_CODIGO'
        Title.Alignment = taRightJustify
        Title.Caption = 'Modific�'
        Width = 48
        Visible = True
      end>
  end
  inherited DataSource: TDataSource
    Left = 572
    Top = 81
  end
  object dsAusencia: TDataSource
    Left = 549
    Top = 223
  end
end
