unit FNomDiasHoras_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, Db, StdCtrls, ExtCtrls, ComCtrls,
     ZetaDBTextBox, ZBaseGridLectura_DevEx, cxGraphics, cxControls,
     cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
     TressMorado2013, dxSkinsDefaultPainters,
     dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
     cxEdit, cxNavigator, cxDBData, cxTextEdit, cxGridCustomTableView,
     cxGridTableView, cxGridDBTableView, Menus, ActnList, ImgList,
     cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ZetaCXGrid, cxButtons,
     cxPCdxBarPopupMenu, cxPC;

type
  TNomDiasHoras_DevEx = class(TBaseGridLectura_DevEx)
    Splitter1: TSplitter;
    dsFaltas: TDataSource;
    PageControl1: TcxPageControl;
    TabSheet4: TcxTabSheet;
    Label1: TLabel;
    CB_TURNO: TZetaDBTextBox;
    Turno: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    NO_D_TURNO: TZetaDBTextBox;
    NO_JORNADA: TZetaDBTextBox;
    TabSheet1: TcxTabSheet;
    Label17: TLabel;
    NO_DIAS: TZetaDBTextBox;
    Label18: TLabel;
    NO_DIAS_AS: TZetaDBTextBox;
    Label19: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    Label20: TLabel;
    NO_DIAS_FJ: TZetaDBTextBox;
    Label21: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    Label22: TLabel;
    NO_DIAS_NT: TZetaDBTextBox;
    Label23: TLabel;
    NO_DIAS_SG: TZetaDBTextBox;
    Label24: TLabel;
    NO_DIAS_SU: TZetaDBTextBox;
    Label32: TLabel;
    NO_DIAS_AG: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    Label31: TLabel;
    NO_DIAS_RE: TZetaDBTextBox;
    Label30: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    Label29: TLabel;
    NO_DIAS_OT: TZetaDBTextBox;
    Label28: TLabel;
    NO_DIAS_CG: TZetaDBTextBox;
    Label26: TLabel;
    NO_DIAS_EM: TZetaDBTextBox;
    Label27: TLabel;
    NO_DIAS_SS: TZetaDBTextBox;
    Label25: TLabel;
    TabSheet2: TcxTabSheet;
    Label4: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label10: TLabel;
    NO_HORA_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label11: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label5: TLabel;
    Label6: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label12: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label13: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label7: TLabel;
    Label8: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label14: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label15: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label9: TLabel;
    Label36: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    TabSheet3: TcxTabSheet;
    Label34: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label35: TLabel;
    NO_USER_RJ: TZetaDBTextBox;
    Label33: TLabel;
    NO_STATUS: TZetaDBTextBox;
    NO_OBSERVA: TZetaDBTextBox;
    Label16: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    NO_DIAS_FV: TZetaDBTextBox;
    bbMostrarCalendario: TcxButton;
    Label39: TLabel;
    NO_DIAS_SI: TZetaDBTextBox;
    Label40: TLabel;
    NO_HORASNT: TZetaDBTextBox;
    Label41: TLabel;
    NO_HORAPDT: TZetaDBTextBox;
    FA_FEC_INI: TcxGridDBColumn;
    FA_MOTIVO: TcxGridDBColumn;
    FA_DIAS: TcxGridDBColumn;
    FA_HORAS: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    function NoHayDatos: Boolean; override;
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar;override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  NomDiasHoras_DevEx: TNomDiasHoras_DevEx;

implementation

uses DNomina, DCliente, DSistema,
     ZetaCommonLists, ZetaCommonClasses, ZetaCommonTools, dCatalogos;

{$R *.DFM}

{ TNomDiasHoras }

procedure TNomDiasHoras_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext:= H30312_Dias_horas;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
end;

procedure TNomDiasHoras_DevEx.Connect;
begin
     dmSistema.cdsUsuarios.Conectar;
     with dmNomina do
     begin
          {
          cdsDatosNomina.Conectar;
          }
          cdsMovDiasHoras.Conectar;
          DataSource.DataSet := cdsDatosNomina;
          dsFaltas.DataSet := cdsMovDiasHoras
     end;
end;

procedure TNomDiasHoras_DevEx.Refresh;
begin
     with dmNomina do
     begin
          {
          cdsDatosNomina.Refrescar;
          }
          cdsMovDiasHoras.Refrescar;
     end;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TNomDiasHoras_DevEx.NoHayDatos: Boolean;
begin
     Result := NoData( dsFaltas );
end;

procedure TNomDiasHoras_DevEx.Agregar;
begin
     dmNomina.cdsMovDiasHoras.Agregar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNomDiasHoras_DevEx.Borrar;
begin
     dmNomina.cdsMovDiasHoras.Borrar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

procedure TNomDiasHoras_DevEx.Modificar;
begin
     dmNomina.cdsMovDiasHoras.Modificar;
     ZetaDBGridDBTableView.ApplyBestFit();
end;

function TNomDiasHoras_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.BajaNominaAnterior( dmCliente.cdsEmpleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.TipoNominaDiferente( dmCliente.cdsEmpleado , sMensaje );
     end;
end;

function TNomDiasHoras_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
end;

function TNomDiasHoras_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar', True );
     end;
     if Result then
     begin
          Result := dmNomina.ValidaStatusEmpleado( dmCliente.Empleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.BajaNominaAnterior( dmCliente.cdsEmpleado , sMensaje );
     end;
     if Result then
     begin
          Result := not dmNomina.TipoNominaDiferente( dmCliente.cdsEmpleado , sMensaje );
     end;
end;

procedure TNomDiasHoras_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     { No se debe llamar inherited para que no se tome otro Dataset
     { como referencia para el m�todo NoData }
     //ImgNoRecord.Visible:= NoHayDatos; //DevEx(@am): No es necesario en la Nueva Imagen.
     with Datasource do
     begin
          if ( Dataset <> nil ) then
          begin
               with DataSet do
                    bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
          end;
     end;
end;

procedure TNomDiasHoras_DevEx.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TNomDiasHoras_DevEx.FormShow(Sender: TObject);
begin
     ApplyMinWidth;
     inherited;
end;

end.
