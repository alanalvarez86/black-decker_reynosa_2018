unit FCosteoTransferencias;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseAsisConsulta, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons, Db,
  ExtCtrls, ZBaseConsulta, ZetaKeyLookup, Mask, ZetaFecha, ZetaCommonClasses;

type
  TCosteoTransferencias = class(TBaseAsisConsulta)
    GridAutorizaciones: TZetaDBGrid;
    pnFiltros: TPanel;
    lblFecIni: TLabel;
    lblFecFin: TLabel;
    Label1: TLabel;
    lblMaestro: TLabel;
    zFechaIni: TZetaFecha;
    zFechaFin: TZetaFecha;
    CCOrigen: TZetaKeyLookup;
    CCDestino: TZetaKeyLookup;
    btnFiltrar: TBitBtn;
    Label2: TLabel;
    TR_MOTIVO: TZetaKeyLookup;
    rgTipo: TRadioGroup;
    Label3: TLabel;
    cbSoloNegativos: TCheckBox;
    rgGlobales: TRadioGroup;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btnFiltrarClick(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
    procedure Refresh; override;
    procedure Agregar; override;
    procedure Borrar; override;
    procedure Modificar; override;
  public
    { Public declarations }
  end;

var
  CosteoTransferencias: TCosteoTransferencias;

implementation

uses dAsistencia, dSistema, dGlobal, ZetaCommonLists, dTablas, dCliente, ZBaseEdicion,
     ZAccesosMgr, ZAccesosTress, ZGlobalTress,ZetaClientDataSet;

{$R *.DFM}

procedure TCosteoTransferencias.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H_Transferencias_Tress;
     zFechaIni.Valor:= dmCliente.GetDatosPeriodoActivo.Inicio;
     zFechaFin.Valor:= dmCliente.GetDatosPeriodoActivo.Fin;
     rgTipo.ItemIndex := 0;
     rgGlobales.ItemIndex := 0;


end;

procedure TCosteoTransferencias.Connect;
 var
    oTransferencia: TZetaLookupDataset;
begin
     dmSistema.cdsUsuarios.Conectar;
     oTransferencia := dmTablas.GetDataSetTransferencia;
     oTransferencia.Conectar;
     dmTablas.cdsMotivoTransfer.Conectar;

     CCOrigen.LookupDataset := dmTablas.GetDataSetTransferencia;
     CCDestino.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;

     with dmAsistencia do
     begin
          with cdsCosteoTransferencias do
          begin
               if not Active or HayQueRefrescar then
                  DoRefresh;
          end;
          DataSource.DataSet:= cdsCosteoTransferencias;
     end;
end;

procedure TCosteoTransferencias.Refresh;
begin
     if dmAsistencia.ParametrosTransferencias = NIL then
        dmAsistencia.ParametrosTransferencias := TZetaParams.Create(NIL);
     with dmAsistencia do
     begin
          with ParametrosTransferencias do
          begin
               AddDate( 'FechaIni', zFechaIni.Valor );
               AddDate( 'FechaFin', zFechaFin.Valor );
               AddString( 'CCOrigen', CCOrigen.Llave );
               AddString( 'CCDestino', CCDestino.Llave );
               AddString( 'Motivo', TR_MOTIVO.Llave );
               AddInteger( 'Tipo', rgTipo.ItemIndex - 1 );
               AddBoolean( 'SoloNegativos', cbSoloNegativos.Checked );
               AddInteger( 'Globales', rgGlobales.ItemIndex - 1 );
               AddBoolean( 'PuedeCambiarTarjeta', dmCliente.PuedeModificarTarjetasAnteriores );
               AddInteger( 'Status', -1 ); // Por lo pronto no se filtra por Status
               AddBoolean( 'ModuloSupervisores', FALSE );
          end;
         ObtieneTransferencias( ParametrosTransferencias );
     end;

end;

procedure TCosteoTransferencias.Agregar;
begin
     dmAsistencia.cdsCosteoTransferencias.Agregar;
end;

procedure TCosteoTransferencias.Borrar;
begin
     dmAsistencia.cdsCosteoTransferencias.Borrar;
end;

procedure TCosteoTransferencias.Modificar;
begin
     dmAsistencia.cdsCosteoTransferencias.Modificar;
end;

procedure TCosteoTransferencias.btnFiltrarClick(Sender: TObject);
begin
     inherited;
     DoRefresh;
end;

end.
