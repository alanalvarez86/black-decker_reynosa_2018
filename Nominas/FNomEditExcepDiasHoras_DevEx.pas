unit FNomEditExcepDiasHoras_DevEx;                                            

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Mask, ExtCtrls, DBCtrls, Db, Buttons,
     ZetaNumero, ZetaFecha, ZetaKeyCombo, 
     {$ifndef VER130}Variants,{$endif}
     ZetaSmartLists, ZBaseEdicion_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore,
  TressMorado2013, dxSkinsDefaultPainters, cxControls, dxSkinsdxBarPainter,
  ZetaKeyLookup_DevEx, dxBarExtItems, dxBar, cxClasses, ImgList,
  cxNavigator, cxDBNavigator, cxButtons;

type
  TNomEditExcepDiasHoras_DevEx = class(TBaseEdicion_DevEx)
    FA_DIA_HOR: TDBRadioGroup;
    GBDias: TGroupBox;
    LMotDias: TLabel;
    LFecha: TLabel;
    LDias: TLabel;
    FA_MOTIVO1: TZetaDBKeyCombo;
    FA_FEC_INI: TZetaDBFecha;
    FA_DIAS: TZetaDBNumero;
    GBHoras: TGroupBox;
    LMotHoras: TLabel;
    LHoras: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    FA_MOTIVO2: TZetaDBKeyCombo;
    FA_HORAS: TZetaDBNumero;
    HorasMin: TZetaNumero;
    Panel1: TPanel;
    CB_CODIGO: TZetaDBKeyLookup_DevEx;
    EmpleadoLbl: TLabel;
    Label3: TLabel;
    FA_FEC_INI2: TZetaDBFecha;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
    procedure HorasMinExit(Sender: TObject);
    procedure HorasMinEnter(Sender: TObject);
    procedure FA_HORASChange(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FA_DIA_HORChange(Sender: TObject);
    procedure FA_MOTIVO1Change(Sender: TObject);
    procedure OKClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
  private
    { Private declarations }
    FHorasMinutos: Double;
    function EsFaltaDias: Boolean;
    procedure ShowControls;
  protected
    { Protected declarations }
    function PuedeAgregar( var sMensaje: String ): Boolean; override;
    function PuedeBorrar( var sMensaje: String ): Boolean; override;
    function PuedeModificar( var sMensaje: String ): Boolean; override;
    procedure Connect; override;
    procedure EscribirCambios; override;
    procedure HabilitaControles; override;
  public
    { Public declarations }
  end;

var
  NomEditExcepDiasHoras_DevEx: TNomEditExcepDiasHoras_DevEx;

implementation

uses DNomina,
     DCliente,
     DRecursos,
     ZetaCommonLists,
     ZetaCommonTools,
     ZAccesosTress,
     ZetaCommonClasses;

{$R *.DFM}

{ TNomEditExcepDiasHoras }

procedure TNomEditExcepDiasHoras_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stPeriodo;
     IndexDerechos := D_NOM_EXCEP_DIAS_HORAS;
     HelpContext := H33121_Dias_horas;
     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
end;

procedure TNomEditExcepDiasHoras_DevEx.Connect;
begin
     dmCliente.cdsEmpleadoLookUp.Conectar;
     with dmNomina do
     begin
          cdsExcepciones.Conectar;
          DataSource.DataSet:= cdsExcepciones;
     end;
end;

function TNomEditExcepDiasHoras_DevEx.PuedeAgregar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeAgregar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Agregar' );
     end;
end;

function TNomEditExcepDiasHoras_DevEx.PuedeBorrar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeBorrar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Borrar' );
     end;
end;

function TNomEditExcepDiasHoras_DevEx.PuedeModificar(var sMensaje: String): Boolean;
begin
     Result:= inherited PuedeModificar( sMensaje );
     if Result then
     begin
          Result := dmNomina.ValidaAfectada( sMensaje, True, 'Modificar' );
     end;
end;

function TNomEditExcepDiasHoras_DevEx.EsFaltaDias: Boolean;
begin
     Result := ( eMotivoFaltaDias( FA_MOTIVO1.ItemIndex ) in [ mfdAsistencia, mfdIMSS, mfdEM ] );
end;

procedure TNomEditExcepDiasHoras_DevEx.HabilitaControles;
begin
     CB_CODIGO.Enabled := Inserting;
     if CB_CODIGO.Enabled then
        FirstControl := CB_CODIGO
     else
         FirstControl := FA_DIA_HOR;
     inherited;
end;

procedure TNomEditExcepDiasHoras_DevEx.ShowControls;
var
   lDays, lHours: Boolean;
begin
     inherited;
     lDays := ( FA_DIA_HOR.ItemIndex = 0 );
     lHours := not lDays;
     GBDias.Enabled := lDays;
     LDias.Enabled := lDays;
     LMotDias.Enabled := lDays;
     LFecha.Enabled := lDays;
     FA_MOTIVO1.Enabled := lDays;
     FA_FEC_INI.Enabled := lDays and not EsFaltaDias;
     FA_DIAS.Enabled := lDays;
     GBHoras.Enabled := lHours;
     LMotHoras.Enabled := lHours;
     LHoras.Enabled := lHours;
     FA_MOTIVO2.Enabled := lHours;
     FA_HORAS.Enabled := lHours;
     FA_FEC_INI2.Enabled := lHours;
     Application.ProcessMessages;
end;

procedure TNomEditExcepDiasHoras_DevEx.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     inherited;
     ShowControls;
end;

procedure TNomEditExcepDiasHoras_DevEx.HorasMinExit(Sender: TObject);
begin
     inherited;
     if ( FHorasMinutos <> HorasMin.Valor ) then
     begin
          with dmNomina.cdsExcepciones do
          begin
               if ( State = dsBrowse ) then
                  Edit;
          end;
          FA_HORAS.Valor := ( HorasMin.Valor / 60 );
     end;
end;

procedure TNomEditExcepDiasHoras_DevEx.HorasMinEnter(Sender: TObject);
begin
     inherited;
     FHorasMinutos := HorasMin.Valor;
end;

procedure TNomEditExcepDiasHoras_DevEx.FA_HORASChange(Sender: TObject);
begin
     inherited;
     HorasMin.Valor := Round( FA_HORAS.Valor * 60 );
end;

procedure TNomEditExcepDiasHoras_DevEx.FA_DIA_HORChange(Sender: TObject);
begin
     inherited;
     ShowControls;
end;

procedure TNomEditExcepDiasHoras_DevEx.FA_MOTIVO1Change(Sender: TObject);
begin
     inherited;
     ShowControls;
end;

procedure TNomEditExcepDiasHoras_DevEx.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if ( Key = Chr( VK_RETURN ) ) and ( ActiveControl = FA_MOTIVO1 ) then
     begin
          Key := #0;
          if EsFaltaDias then
              FA_DIAS.SetFocus
          else
          begin
               with FA_FEC_INI do
               begin
                    Enabled := True;
                    SetFocus;
               end;
          end;
     end
     else
         inherited;
end;

procedure TNomEditExcepDiasHoras_DevEx.EscribirCambios;
begin
     dmNomina.OperacionConflicto := Ord( ocReportar );
     inherited EscribirCambios;
end;

procedure TNomEditExcepDiasHoras_DevEx.OKClick(Sender: TObject);
var
   iEmpleado, iMotivo: Integer;
   sDiaHora: String;
   dFecha: TDate;
begin
     with dmNomina do
     begin
          ConflictoFaltas:= FALSE;

          inherited;

          if ConflictoFaltas then
             with cdsExcepciones do
             begin
                  iEmpleado:= FieldByName( 'CB_CODIGO' ).AsInteger;
                  iMotivo:= FieldByName( 'FA_MOTIVO' ).AsInteger;
                  sDiaHora:= FieldByName( 'FA_DIA_HOR' ).AsString;
                  dFecha:= FieldByName( 'FA_FEC_INI' ).AsDateTime;
                  Refrescar;
                  Locate( 'CB_CODIGO;FA_FEC_INI;FA_DIA_HOR;FA_MOTIVO',VarArrayOf([ iEmpleado,
                          FechaAsStr( dFecha ), sDiaHora, iMotivo ] ), [] );
             end;
     end;
end;

procedure TNomEditExcepDiasHoras_DevEx.OK_DevExClick(Sender: TObject);
var
   iEmpleado, iMotivo: Integer;
   sDiaHora: String;
   dFecha: TDate;
begin
     with dmNomina do
     begin
          ConflictoFaltas:= FALSE;

          inherited;

          if ConflictoFaltas then
             with cdsExcepciones do
             begin
                  iEmpleado:= FieldByName( 'CB_CODIGO' ).AsInteger;
                  iMotivo:= FieldByName( 'FA_MOTIVO' ).AsInteger;
                  sDiaHora:= FieldByName( 'FA_DIA_HOR' ).AsString;
                  dFecha:= FieldByName( 'FA_FEC_INI' ).AsDateTime;
                  Refrescar;
                  Locate( 'CB_CODIGO;FA_FEC_INI;FA_DIA_HOR;FA_MOTIVO',VarArrayOf([ iEmpleado,
                          FechaAsStr( dFecha ), sDiaHora, iMotivo ] ), [] );
             end;
     end;

end;

end.



