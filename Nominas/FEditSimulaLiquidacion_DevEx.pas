unit FEditSimulaLiquidacion_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, FEditLiquidacion_DevEx, DB, ZetaFecha, ZetaEdit, ExtCtrls, DBCtrls,
  ZetaDBTextBox, StdCtrls, Mask, ZetaNumero, ComCtrls,
  cxGraphics, cxLookAndFeels, cxLookAndFeelPainters,
  Menus, dxSkinsCore, TressMorado2013,
  dxSkinsDefaultPainters, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
  cxControls, cxContainer, cxEdit, ImgList, cxGroupBox, cxPC, cxButtons;

type
  TEditSimulaLiquidacion_DevEx = class(TEditLiquidacion_DevEx)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure ConectaLiquidacion; override;
  public
    { Public declarations }
  end;

var
  EditSimulaLiquidacion_DevEx: TEditSimulaLiquidacion_DevEx;

implementation

uses dNomina,
     ZetaCommonClasses;

{$R *.dfm}

procedure TEditSimulaLiquidacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := 0; // PENDIENTE
end;

procedure TEditSimulaLiquidacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ValorActivo2.Caption := VACIO; // No mostrar periodo
end;

procedure TEditSimulaLiquidacion_DevEx.ConectaLiquidacion;
begin
     DataSource.DataSet := dmNomina.cdsSimulaLiquidacion;
end;

end.
