object MontoRepetido_DevEx: TMontoRepetido_DevEx
  Left = 48
  Top = 215
  ActiveControl = IgnoreBtn
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Advertencia'
  ClientHeight = 207
  ClientWidth = 347
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Mensaje: TLabel
    Left = 77
    Top = 10
    Width = 213
    Height = 17
    Caption = #161' Ya hay una Excepci'#243'n de Monto !'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Image1: TImage
    Left = 24
    Top = 16
    Width = 32
    Height = 32
    AutoSize = True
    Picture.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
      000000200806000000737A7AF4000000017352474200AECE1CE9000000046741
      4D410000B18F0BFC6105000000097048597300000EBE00000EBE01EA42B1C000
      00017D494441545847D597314EC3401045A7A0A0E008141C83220507A1E02848
      1C8502098490E303A44C91920350A430CE0528CDB7F98CD6CBD85EDBBB2BF1A5
      27599E6FAF13F96913F9F7697672C6C3BCA99FE5A2DECABE2EE4FDF82A973C9D
      2F75290F7880863CF2749E546F72854FFEE53C40F359C835C7E983059FDCC5C9
      9EE3B4C1421B6F6105DFC22D6BE982850EFEC20EC78F9D9CB31A3F552177C6A2
      7D0AB9673D6E5AEDF01557E6A22E78399368E96937455C2D2DED38D2B8B396A8
      5AE2867FB4E348E3CF411C2D7123533B8E3556278A96B891A91DC71AAB03D669
      39A61D2B1AABD3B154CB29ED58D3589D8EA55A4E69C79AC6EA38CCD3D2D2CE87
      558DD57199A5252EB076BB1EAC6AAC8E479896280EEE762EAC6BAC8E4F909628
      8EED766B19D73268B75BCB9096C1BBDD2FB8510FAB6331A4E5CCDD6ED13BE0D0
      D732443B1F5EAAB13A63F4B4C48949ED7CDA8776B13A13FC68898320ED52D069
      8983D21F6403FFAAE4B4951773988743F7E712079BAA949BDC24FD091F16916F
      EF8AE332454EAC190000000049454E44AE426082}
    Stretch = True
  end
  object Label1: TLabel
    Left = 111
    Top = 31
    Width = 50
    Height = 13
    Caption = 'Empleado:'
  end
  object Label2: TLabel
    Left = 112
    Top = 46
    Width = 49
    Height = 13
    Caption = 'Concepto:'
  end
  object EmpleadoLb: TLabel
    Left = 167
    Top = 31
    Width = 59
    Height = 13
    Caption = 'EmpleadoLb'
  end
  object ConceptoLb: TLabel
    Left = 167
    Top = 46
    Width = 58
    Height = 13
    Caption = 'ConceptoLb'
  end
  object Label3: TLabel
    Left = 104
    Top = 86
    Width = 51
    Height = 13
    Caption = 'Anterior -->'
  end
  object Label4: TLabel
    Left = 108
    Top = 108
    Width = 47
    Height = 13
    Caption = 'Nuevo -->'
  end
  object Label5: TLabel
    Left = 105
    Top = 133
    Width = 129
    Height = 16
    Caption = #191' Que Desea Hacer ?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 106
    Top = 62
    Width = 55
    Height = 13
    Caption = 'Referencia:'
  end
  object ReferenciaLb: TLabel
    Left = 167
    Top = 62
    Width = 64
    Height = 13
    Caption = 'ReferenciaLb'
  end
  object lblMensaje: TLabel
    Left = 16
    Top = 190
    Width = 3
    Height = 13
  end
  object IgnoreBtn: TcxButton
    Left = 235
    Top = 162
    Width = 100
    Height = 26
    Hint = 'Dejar Anterior y cancelar cambios'
    Cancel = True
    Caption = 'Dejar &Anterior'
    ModalResult = 2
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000004858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF98A1E2FFF6F7FDFFB7BEEBFF4B5BCDFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFB1B8
      E9FFF6F7FDFFACB3E8FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
      FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF9BA4E3FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
      CDFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
      FFFFFFFFFFFFA6AEE6FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
      EEFF4B5BCDFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
      FFFFAFB6E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
      FFFFC3C8EEFF4B5BCDFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
      E9FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
      FFFFFFFFFFFFC3C8EEFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8
      EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5BCDFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFF
      FFFFFFFFFFFFAFB6E9FFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8EEFF4B5B
      CDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFF
      FFFFAFB6E9FF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFFFFFFC3C8
      EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4B5BCDFFC3C8EEFFFFFFFFFFFFFFFFFFFFFFFFFFAFB6
      E9FF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFFFFFFFFFF
      FFFFC3C8EEFF4B5BCDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFFA9B0E7FFFFFFFFFFFFFFFFFFFFFFFFFFAFB6E9FF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFFFFFFFFFF
      FFFFFFFFFFFFB7BEEBFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFFE2E5F7FFFFFFFFFFFFFFFFFFAFB6E9FF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFFAFB6E9FFFFFF
      FFFFFFFFFFFFF6F7FDFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF8792DEFFDFE2F6FFA1A9E5FF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF9BA4
      E3FFDFE2F6FF98A1E2FF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858
      CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF4858CCFF}
    OptionsImage.Margin = 1
  end
  object ReplaceBtn: TcxButton
    Left = 13
    Top = 162
    Width = 95
    Height = 26
    Hint = 'Sustituir una Excepci'#243'n de monto'
    Caption = '&Sustituir'
    ModalResult = 1
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      200000000000000900000000000000000000000000000000000000B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFAFE7F8FF3CC4EEFF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CFF1FFFFFFFFFFFFFFFFFFCBEF
      FBFF5CCEF1FF04B3E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF78D6F3FFFFFFFFFFFFFFFFFFE7F8
      FDFF70D4F3FF08B4EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEFFAFEFF80D9F4FF10B7
      EAFF00B2E9FF0CB6EAFF38C3EEFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF38C3EEFF18B9EBFF00B2E9FF00B2
      E9FF97E0F6FFE3F7FDFFC7EEFAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF5CCEF1FFBFEC
      F9FFFBFEFFFFFFFFFFFFFFFFFFFF5CCEF1FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF80D9F4FFEBF9FDFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFE7F8FDFF0CB6EAFF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF20BCECFFF7FDFEFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83DAF4FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF83DAF4FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FDFEFF24BDECFF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF0CB6EAFFE7F8
      FDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFABE6F8FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF60CF
      F1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3EDFAFF34C2EDFF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FFCBEFFBFFFFFFFFFFDFF5FCFF58CDF1FF00B2E9FF00B2E9FF10B7EAFF0CB6
      EAFF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF3CC4EEFF7CD7F4FF08B4EAFF00B2E9FF04B3E9FF70D4F3FFEFFAFEFF74D5
      F3FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF50CAF0FFDBF4FCFFFFFFFFFFFFFFFFFF68D1
      F2FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF04B3E9FFB7E9F9FFFFFFFFFFFFFFFFFFCBEFFBFF3CC4EEFF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF60CFF1FFCBEFFBFF5CCEF1FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2
      E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF00B2E9FF}
    OptionsImage.Margin = 1
  end
  object SumarBtn: TcxButton
    Left = 124
    Top = 162
    Width = 95
    Height = 26
    Hint = 'Sumar a otra Excepci'#243'n de monto'
    Caption = 'S&umar'
    ModalResult = 6
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OptionsImage.Glyph.Data = {
      36090000424D3609000000000000360000002800000018000000180000000100
      20000000000000090000000000000000000000000000000000003399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFFB2D9FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCCE5
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFFB2D9FFFFFFFFFFFFF9FCFFFFE5F2
      FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFB9DC
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF73B9FFFFFCFDFFFFFCFDFFFF6DB6
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF90C7FFFFFFFFFFFFF5FA
      FFFF53A9FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFFACD5FFFFFFFF
      FFFFE5F2FFFF46A3FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF369BFFFFC5E2
      FFFFFFFFFFFFCFE7FFFF399CFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3D9E
      FFFFDCEDFFFFFFFFFFFFB5DAFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF49A4FFFFECF5FFFFFFFFFFFF99CCFFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFFB2D9FFFFFFFFFFFFDFEFFFFF399CFFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFFA5D2FFFFFFFFFFFFDFEFFFFF46A3FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF99CC
      FFFFFFFFFFFFECF5FFFF4DA6FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF8DC6FFFFFFFF
      FFFFECF5FFFF53A9FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF86C3FFFFFFFFFFFFF5FA
      FFFF60AFFFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF73B9FFFFFCFDFFFFF5FAFFFF63B1
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFFB2D9FFFFFFFFFFFFF5FAFFFFE5F2
      FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFFE5F2FFFF60AF
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFFB2D9FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF66B3
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399
      FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF3399FFFF}
    OptionsImage.Margin = 1
  end
  object AnteriorNu: TZetaNumero
    Left = 160
    Top = 82
    Width = 89
    Height = 21
    Color = clBtnFace
    Mascara = mnPesos
    TabOrder = 0
    Text = '0.00'
  end
  object NuevoNu: TZetaNumero
    Left = 160
    Top = 104
    Width = 89
    Height = 21
    Color = clBtnFace
    Mascara = mnPesos
    TabOrder = 1
    Text = '0.00'
  end
end
