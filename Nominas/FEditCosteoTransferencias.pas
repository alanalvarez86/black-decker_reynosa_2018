unit FEditCosteoTransferencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ZBaseEdicion, StdCtrls, ZetaKeyCombo, ZetaFecha, Mask,
  ZetaNumero, ZetaKeyLookup, ZetaDBTextBox, DB, ExtCtrls, DBCtrls,
  ZetaSmartLists, Buttons, ZetaEdit;

type
  TEditCosteoTransferencias = class(TBaseEdicion)
    TipoAutLbl: TLabel;
    HorasLbl: TLabel;
    MotivoLbl: TLabel;
    EmpleadoLbl: TLabel;
    Label3: TLabel;
    US_DESCRIP: TZetaDBTextBox;
    lbDestino: TLabel;
    Label5: TLabel;
    CC_ORIGEN: TZetaDBTextBox;
    lbOrigen: TLabel;
    CC_DESCRIP: TZetaDBTextBox;
    CB_CODIGO: TZetaDBKeyLookup;
    TR_MOTIVO: TZetaDBKeyLookup;
    TR_HORAS: TZetaDBNumero;
    CC_CODIGO: TZetaDBKeyLookup;
    AU_FECHA: TZetaDBFecha;
    TR_TIPO: TZetaDBKeyCombo;
    TR_FECHA: TZetaDBTextBox;
    TR_TEXTO: TZetaDBEdit;
    Label2: TLabel;
    TR_GLOBAL: TDBCheckBox;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    TR_FEC_APR: TZetaDBTextBox;
    Label6: TLabel;
    TR_STATUS: TZetaDBKeyCombo;
    TR_TXT_APR: TZetaDBEdit;
    Label7: TLabel;
    ZetaDBTextBox1: TZetaDBTextBox;
    procedure FormCreate(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect;override;
  public
    { Public declarations }
  end;

var
  EditCosteoTransferencias: TEditCosteoTransferencias;

implementation

uses ZAccesosTress,
     ZetaCommonClasses,
     ZGlobalTress,
     DGlobal,
     DCliente,
     DTablas,
     DAsistencia,
     DBaseCliente;

{$R *.dfm}

procedure TEditCosteoTransferencias.FormCreate(Sender: TObject);
var
   sNivel : string;
begin
     inherited;
     IndexDerechos := D_COSTEO_TRANSFERENCIAS;
     FirstControl := AU_FECHA;
     HelpContext := H_Edit_Transferencias_Tress;

     CB_CODIGO.LookupDataset := dmCliente.cdsEmpleadoLookUp;
     CC_CODIGO.LookupDataset := dmTablas.GetDataSetTransferencia;
     TR_MOTIVO.LookupDataset := dmTablas.cdsMotivoTransfer;

     with Global do
     begin
          sNivel := Global.NombreCosteo;
          lbOrigen.Caption :=  sNivel + ' Origen:';
          lbDestino.Caption := sNivel + ' Destino:';
     end;
end;

procedure TEditCosteoTransferencias.Connect;
begin
      dmTablas.GetDataSetTransferencia.Conectar;
      dmTablas.cdsMotivoTransfer.Conectar;
      Datasource.Dataset := dmAsistencia.cdsCosteoTransferencias;
end;

procedure TEditCosteoTransferencias.DataSourceDataChange(Sender: TObject; Field: TField);
begin
     //inherited;
     //En el caso de esta pantalla, no quiero que se cierre si no hay datos en el dataset.
     //Este dataset puede estar vacio debido a los filtros
end;

end.
