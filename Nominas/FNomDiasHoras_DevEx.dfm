inherited NomDiasHoras_DevEx: TNomDiasHoras_DevEx
  Left = 264
  Top = 536
  Caption = 'D'#237'as / Horas'
  ClientHeight = 280
  ClientWidth = 745
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 0
    Top = 0
    Width = 745
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  inherited PanelIdentifica: TPanel
    Top = 3
    Width = 745
    inherited ValorActivo2: TPanel
      Width = 486
      inherited textoValorActivo2: TLabel
        Width = 480
      end
    end
  end
  object PageControl1: TcxPageControl [2]
    Left = 0
    Top = 22
    Width = 745
    Height = 114
    Align = alTop
    TabOrder = 2
    Properties.ActivePage = TabSheet2
    Properties.CustomButtons.Buttons = <>
    Properties.TabPosition = tpBottom
    ClientRectBottom = 86
    ClientRectLeft = 2
    ClientRectRight = 743
    ClientRectTop = 2
    object TabSheet1: TcxTabSheet
      Caption = 'D'#237'as'
      object Label17: TLabel
        Left = 27
        Top = 4
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarios:'
      end
      object NO_DIAS: TZetaDBTextBox
        Left = 80
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label18: TLabel
        Left = 26
        Top = 22
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_DIAS_AS: TZetaDBTextBox
        Left = 80
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label19: TLabel
        Left = 393
        Top = 4
        Width = 94
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas injustificadas:'
      end
      object NO_DIAS_FI: TZetaDBTextBox
        Left = 491
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label20: TLabel
        Left = 401
        Top = 22
        Width = 86
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas justificadas:'
      end
      object NO_DIAS_FJ: TZetaDBTextBox
        Left = 491
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 157
        Top = 4
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Incapacidad:'
      end
      object NO_DIAS_IN: TZetaDBTextBox
        Left = 222
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_IN'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_IN'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label22: TLabel
        Left = 8
        Top = 40
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajados:'
      end
      object NO_DIAS_NT: TZetaDBTextBox
        Left = 80
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_NT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_NT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label23: TLabel
        Left = 136
        Top = 22
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object NO_DIAS_SG: TZetaDBTextBox
        Left = 222
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label24: TLabel
        Left = 19
        Top = 58
        Width = 58
        Height = 13
        Alignment = taRightJustify
        Caption = 'Suspensi'#243'n:'
      end
      object NO_DIAS_SU: TZetaDBTextBox
        Left = 80
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SU'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SU'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label32: TLabel
        Left = 550
        Top = 4
        Width = 89
        Height = 13
        Alignment = taRightJustify
        Caption = 'Aguinaldo a pagar:'
      end
      object NO_DIAS_AG: TZetaDBTextBox
        Left = 643
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DIAS_VA: TZetaDBTextBox
        Left = 491
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_VA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_VA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label31: TLabel
        Left = 389
        Top = 58
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones a pagar:'
      end
      object NO_DIAS_RE: TZetaDBTextBox
        Left = 327
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_RE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_RE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label30: TLabel
        Left = 277
        Top = 22
        Width = 46
        Height = 13
        Alignment = taRightJustify
        Caption = 'Retardos:'
      end
      object NO_DIAS_AJ: TZetaDBTextBox
        Left = 327
        Top = 2
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_AJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_AJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label29: TLabel
        Left = 291
        Top = 4
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ajuste:'
      end
      object NO_DIAS_OT: TZetaDBTextBox
        Left = 222
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_OT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_OT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label28: TLabel
        Left = 147
        Top = 58
        Width = 72
        Height = 13
        Alignment = taRightJustify
        Caption = 'Otros permisos:'
      end
      object NO_DIAS_CG: TZetaDBTextBox
        Left = 222
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label26: TLabel
        Left = 291
        Top = 58
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'E Y M:'
      end
      object NO_DIAS_EM: TZetaDBTextBox
        Left = 327
        Top = 56
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_EM'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_EM'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label27: TLabel
        Left = 131
        Top = 40
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_DIAS_SS: TZetaDBTextBox
        Left = 327
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label25: TLabel
        Left = 294
        Top = 40
        Width = 29
        Height = 13
        Alignment = taRightJustify
        Caption = 'IMSS:'
      end
      object Label38: TLabel
        Left = 380
        Top = 40
        Width = 107
        Height = 13
        Alignment = taRightJustify
        Caption = 'Faltas por vacaciones:'
      end
      object NO_DIAS_FV: TZetaDBTextBox
        Left = 491
        Top = 38
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_FV'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_FV'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label39: TLabel
        Left = 535
        Top = 22
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Subsidio incapacidad:'
      end
      object NO_DIAS_SI: TZetaDBTextBox
        Left = 643
        Top = 20
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DIAS_SI'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DIAS_SI'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = 'Horas'
      object Label4: TLabel
        Left = 26
        Top = 6
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Ordinarias:'
      end
      object NO_HORAS: TZetaDBTextBox
        Left = 80
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label10: TLabel
        Left = 153
        Top = 42
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical:'
      end
      object NO_HORA_PD: TZetaDBTextBox
        Left = 234
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_PD'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_PD'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_DES_TRA: TZetaDBTextBox
        Left = 416
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label11: TLabel
        Left = 312
        Top = 24
        Width = 98
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descanso trabajado:'
      end
      object NO_EXTRAS: TZetaDBTextBox
        Left = 80
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_EXTRAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_EXTRAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 44
        Top = 24
        Width = 32
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extras:'
      end
      object Label6: TLabel
        Left = 40
        Top = 42
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Dobles:'
      end
      object NO_TRIPLES: TZetaDBTextBox
        Left = 80
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TRIPLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TRIPLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 326
        Top = 42
        Width = 84
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo trabajado:'
      end
      object NO_FES_TRA: TZetaDBTextBox
        Left = 416
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_VAC_TRA: TZetaDBTextBox
        Left = 416
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_VAC_TRA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_VAC_TRA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label13: TLabel
        Left = 299
        Top = 60
        Width = 111
        Height = 13
        Alignment = taRightJustify
        Caption = 'Vacaciones trabajadas:'
      end
      object NO_DOBLES: TZetaDBTextBox
        Left = 80
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_DOBLES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_DOBLES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label7: TLabel
        Left = 40
        Top = 60
        Width = 34
        Height = 13
        Alignment = taRightJustify
        Caption = 'Triples:'
      end
      object Label8: TLabel
        Left = 193
        Top = 6
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tardes:'
      end
      object NO_TARDES: TZetaDBTextBox
        Left = 234
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_TARDES'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_TARDES'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label14: TLabel
        Left = 478
        Top = 6
        Width = 88
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso con goce:'
      end
      object NO_HORA_CG: TZetaDBTextBox
        Left = 572
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_CG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_CG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_HORA_SG: TZetaDBTextBox
        Left = 572
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORA_SG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORA_SG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label15: TLabel
        Left = 483
        Top = 24
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Permiso sin goce:'
      end
      object NO_ADICION: TZetaDBTextBox
        Left = 416
        Top = 4
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_ADICION'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_ADICION'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label9: TLabel
        Left = 353
        Top = 6
        Width = 57
        Height = 13
        Alignment = taRightJustify
        Caption = 'Adicionales:'
      end
      object Label36: TLabel
        Left = 490
        Top = 42
        Width = 76
        Height = 13
        Alignment = taRightJustify
        Caption = 'Festivo pagado:'
      end
      object NO_FES_PAG: TZetaDBTextBox
        Left = 572
        Top = 40
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_FES_PAG'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_FES_PAG'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label40: TLabel
        Left = 160
        Top = 24
        Width = 69
        Height = 13
        Alignment = taRightJustify
        Caption = 'No trabajadas:'
      end
      object NO_HORASNT: TZetaDBTextBox
        Left = 234
        Top = 22
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORASNT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORASNT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label41: TLabel
        Left = 130
        Top = 60
        Width = 99
        Height = 13
        Alignment = taRightJustify
        Caption = 'Prima dominical total:'
      end
      object NO_HORAPDT: TZetaDBTextBox
        Left = 234
        Top = 58
        Width = 38
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_HORAPDT'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_HORAPDT'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
    end
    object TabSheet4: TcxTabSheet
      Caption = 'Turno'
      object Label1: TLabel
        Left = 47
        Top = 4
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'C'#243'digo:'
      end
      object CB_TURNO: TZetaDBTextBox
        Left = 86
        Top = 2
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'CB_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Turno: TZetaDBTextBox
        Left = 86
        Top = 21
        Width = 217
        Height = 17
        AutoSize = False
        Caption = 'Turno'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Turno'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label2: TLabel
        Left = 57
        Top = 42
        Width = 26
        Height = 13
        Alignment = taRightJustify
        Caption = 'D'#237'as:'
      end
      object Label3: TLabel
        Left = 42
        Top = 61
        Width = 41
        Height = 13
        Alignment = taRightJustify
        Caption = 'Jornada:'
      end
      object NO_D_TURNO: TZetaDBTextBox
        Left = 86
        Top = 40
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_D_TURNO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_D_TURNO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_JORNADA: TZetaDBTextBox
        Left = 86
        Top = 59
        Width = 65
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = 'NO_JORNADA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_JORNADA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label37: TLabel
        Left = 24
        Top = 23
        Width = 59
        Height = 13
        Alignment = taRightJustify
        Caption = 'Descripci'#243'n:'
      end
      object bbMostrarCalendario: TcxButton
        Left = 307
        Top = 19
        Width = 21
        Height = 21
        Hint = 'Mostrar Calendario del Turno'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = bbMostrarCalendarioClick
        OptionsImage.Glyph.Data = {
          DA050000424DDA05000000000000360000002800000013000000130000000100
          200000000000A405000000000000000000000000000000000000D8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFF0DECDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E8DDFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5
          BFFFECD5BFFFF5EADFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5
          BFFFFAF4EFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB5
          8FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFAF7FFECD5BFFFECD5BFFFF5EA
          DFFFEEDAC7FFECD5BFFFEEDAC7FFF5EADFFFECD5BFFFECD5BFFFFAF4EFFFFFFF
          FFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF6
          F1FFDDB58FFFDDB58FFFEEDAC7FFE1BE9DFFDDB58FFFE1BE9DFFEEDAC7FFDDB5
          8FFFDDB58FFFF6ECE3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFFAF4EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA
          7FFFDDB58FFFECD5BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFDFBF9FFF0DFCFFFF0DF
          CFFFF8EFE7FFF2E3D5FFF0DFCFFFF2E3D5FFF8EFE7FFF0DFCFFFF0DFCFFFFBF7
          F3FFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4
          EFFFFAF4EFFFD8AA7FFFD8AA7FFFECD5BFFFDDB58FFFD8AA7FFFDDB58FFFECD5
          BFFFD8AA7FFFD8AA7FFFF5EADFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFFAF4EFFFFBF7F3FFE2BF9FFFE2BF9FFFF0DFCFFFE5C7
          ABFFE2BF9FFFE5C7ABFFF0DFCFFFE2BF9FFFE2BF9FFFF8EFE7FFFFFFFFFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFFAF4EFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFFAF4EFFFFFFFFFFFE5C7ABFFE1BE9DFFDFBA97FFFBF6F1FFFFFFFFFFFFFF
          FFFFDEB793FFE2BF9FFFE2BF9FFFFFFFFFFFFFFFFFFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFF3E4D7FFFFFFFFFFE2BF9FFFFAF4EFFFECD5
          BFFFFAF4EFFFFFFFFFFFFFFFFFFFE2BF9FFFFFFFFFFFE2BF9FFFFFFFFFFFF8EF
          E7FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD9AB81FFE1BE
          9DFFDAAF87FFF2E3D5FFE5C7ABFFE1BD9BFFE2BF9FFFE2BF9FFFDEB691FFF8F0
          E9FFDBB189FFE1BE9DFFD9AD83FFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA
          7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFFD8AA7FFF}
      end
    end
    object TabSheet3: TcxTabSheet
      Caption = 'Generales'
      object Label34: TLabel
        Left = 383
        Top = 8
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 430
        Top = 6
        Width = 179
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label35: TLabel
        Left = 375
        Top = 26
        Width = 51
        Height = 13
        Alignment = taRightJustify
        Caption = 'Asistencia:'
      end
      object NO_USER_RJ: TZetaDBTextBox
        Left = 430
        Top = 24
        Width = 179
        Height = 17
        AutoSize = False
        Caption = 'NO_USER_RJ'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_USER_RJ'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label33: TLabel
        Left = 45
        Top = 8
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object NO_STATUS: TZetaDBTextBox
        Left = 82
        Top = 6
        Width = 137
        Height = 17
        AutoSize = False
        Caption = 'NO_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_OBSERVA: TZetaDBTextBox
        Left = 82
        Top = 24
        Width = 279
        Height = 17
        AutoSize = False
        Caption = 'NO_OBSERVA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_OBSERVA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label16: TLabel
        Left = 4
        Top = 26
        Width = 74
        Height = 13
        Caption = 'Observaciones:'
      end
    end
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Top = 136
    Width = 745
    Height = 144
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      DataController.DataSource = dsFaltas
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsData.DeletingConfirmation = True
      OptionsSelection.CellSelect = False
      OptionsSelection.HideSelection = True
      OptionsView.Indicator = True
      object FA_FEC_INI: TcxGridDBColumn
        Caption = 'Fecha'
        DataBinding.FieldName = 'FA_FEC_INI'
        Width = 64
      end
      object FA_MOTIVO: TcxGridDBColumn
        Caption = 'Motivo'
        DataBinding.FieldName = 'FA_MOTIVO'
        HeaderAlignmentHorz = taCenter
        Width = 64
      end
      object FA_DIAS: TcxGridDBColumn
        Caption = 'D'#237'as'
        DataBinding.FieldName = 'FA_DIAS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 64
      end
      object FA_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'FA_HORAS'
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taRightJustify
        Properties.ReadOnly = False
        HeaderAlignmentHorz = taRightJustify
        Width = 64
      end
    end
  end
  inherited DataSource: TDataSource
    OnUpdateData = FormCreate
    Left = 380
    Top = 160
  end
  inherited cxImage16: TcxImageList
    FormatVersion = 1
  end
  object dsFaltas: TDataSource
    Left = 344
    Top = 168
  end
end
