inherited NomEditExcepGlobales_DevEx: TNomEditExcepGlobales_DevEx
  Caption = 'Excepciones Globales'
  ClientHeight = 210
  ClientWidth = 389
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [0]
    Left = 25
    Top = 60
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = '&Concepto:'
    FocusControl = CO_NUMERO
  end
  object UsarLbl: TLabel [1]
    Left = 49
    Top = 95
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = '&Usar:'
    FocusControl = Usar
  end
  object MG_MONTOLbl: TLabel [2]
    Left = 41
    Top = 125
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = '&Monto:'
    FocusControl = MG_MONTO
  end
  object Label6: TLabel [3]
    Left = 31
    Top = 149
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modific'#243':'
  end
  object US_CODIGO: TZetaDBTextBox [4]
    Left = 78
    Top = 147
    Width = 71
    Height = 17
    AutoSize = False
    Caption = 'US_CODIGO'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_CODIGO'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  inherited PanelBotones: TPanel
    Top = 174
    Width = 389
    inherited OK_DevEx: TcxButton
      Left = 224
      Top = 4
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 303
      Top = 4
    end
  end
  inherited PanelIdentifica: TPanel
    Width = 389
    inherited Splitter: TSplitter
      Left = 193
    end
    inherited ValorActivo1: TPanel
      Width = 193
      inherited textoValorActivo1: TLabel
        Width = 187
      end
    end
    inherited ValorActivo2: TPanel
      Left = 196
      Width = 193
      inherited textoValorActivo2: TLabel
        Width = 187
      end
    end
  end
  object CO_NUMERO: TZetaDBKeyLookup_DevEx [8]
    Left = 78
    Top = 56
    Width = 290
    Height = 21
    Filtro = 'CO_NUMERO < 1000'
    LookupDataset = dmCatalogos.cdsConceptos
    Opcional = False
    EditarSoloActivos = False
    IgnorarConfidencialidad = False
    TabOrder = 3
    TabStop = True
    WidthLlave = 50
    DataField = 'CO_NUMERO'
    DataSource = DataSource
  end
  object Usar: TDBRadioGroup [9]
    Left = 78
    Top = 82
    Width = 290
    Height = 33
    Columns = 2
    DataField = 'MG_FIJO'
    DataSource = DataSource
    Items.Strings = (
      'Monto Fijo'
      'F'#243'rmula')
    TabOrder = 4
    Values.Strings = (
      'S'
      'N')
    OnClick = UsarClick
  end
  object MG_MONTO: TZetaDBNumero [10]
    Left = 78
    Top = 121
    Width = 121
    Height = 21
    Mascara = mnPesos
    TabOrder = 5
    Text = '0.00'
    DataField = 'MG_MONTO'
    DataSource = DataSource
  end
  inherited DataSource: TDataSource
    Left = 308
    Top = 57
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
  inherited DevEx_BarManagerEdicion: TdxBarManager
    DockControlHeights = (
      0
      0
      28
      0)
  end
  inherited cxImageList16Edicion: TcxImageList
    FormatVersion = 1
  end
end
