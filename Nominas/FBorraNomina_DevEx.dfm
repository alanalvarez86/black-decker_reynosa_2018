inherited BorraNominaActiva_DevEx: TBorraNominaActiva_DevEx
  ActiveControl = Borrar
  Caption = 'Advertencia'
  ClientHeight = 207
  ClientWidth = 344
  OldCreateOrder = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage [0]
    Left = 8
    Top = 48
    Width = 32
    Height = 32
    AutoSize = True
    Picture.Data = {
      0B546478504E47496D61676589504E470D0A1A0A0000000D4948445200000020
      000000200806000000737A7AF4000000017352474200AECE1CE9000000046741
      4D410000B18F0BFC6105000000097048597300000EC000000EC0016AD6890900
      00017D494441545847D597314EC3401045A7A0A0E008141C83220507A1E02848
      1C8502098490E303A44C91920350A430CE0528CDB7F98CD6CBD85EDBBB2BF1A5
      27599E6FAF13F96913F9F7697672C6C3BCA99FE5A2DECABE2EE4FDF82A973C9D
      2F75290F7880863CF2749E546F72854FFEE53C40F359C835C7E983059FDCC5C9
      9EE3B4C1421B6F6105DFC22D6BE982850EFEC20EC78F9D9CB31A3F552177C6A2
      7D0AB9673D6E5AEDF01557E6A22E78399368E96937455C2D2DED38D2B8B396A8
      5AE2867FB4E348E3CF411C2D7123533B8E3556278A96B891A91DC71AAB03D669
      39A61D2B1AABD3B154CB29ED58D3589D8EA55A4E69C79AC6EA38CCD3D2D2CE87
      558DD57199A5252EB076BB1EAC6AAC8E479896280EEE762EAC6BAC8E4F909628
      8EED766B19D73268B75BCB9096C1BBDD2FB8510FAB6331A4E5CCDD6ED13BE0D0
      D732443B1F5EAAB13A63F4B4C48949ED7CDA8776B13A13FC68898320ED52D069
      8983D21F6403FFAAE4B4951773988743F7E712079BAA949BDC24FD091F16916F
      EF8AE332454EAC190000000049454E44AE426082}
    Transparent = True
  end
  object Mensaje: TLabel [1]
    Left = 48
    Top = 8
    Width = 281
    Height = 77
    Alignment = taCenter
    AutoSize = False
    Caption = 'Mensaje'
  end
  inherited PanelBotones: TPanel
    Top = 171
    Width = 344
    TabOrder = 1
    inherited OK_DevEx: TcxButton
      Left = 180
      Top = 5
      OnClick = OK_DevExClick
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 259
      Top = 5
      Cancel = True
    end
  end
  object Borrar: TcxRadioGroup [3]
    Left = 85
    Top = 93
    Caption = ' '#191' Que Desea Hacer ? '
    Properties.Items = <
      item
        Caption = 'Borrar Registros de Empleados'
        Value = '0'
      end
      item
        Caption = 'Borrar La Definici'#243'n del Per'#237'odo'
        Value = '1'
      end>
    ItemIndex = 0
    TabOrder = 0
    OnClick = BorrarClick
    Height = 65
    Width = 190
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
