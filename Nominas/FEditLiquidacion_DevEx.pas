unit FEditLiquidacion_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, ZetaEdit, DBCtrls, StdCtrls, Mask, Db, ComCtrls,
     ZBaseDlgModal_DevEx, ZetaDBTextBox, ZetaNumero,
     ZetaFecha, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters, Menus,
     cxGroupBox, cxPC, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, dxSkinscxPCPainter, cxPCdxBarPopupMenu,
     cxControls, cxContainer, cxEdit, ImgList, cxButtons;

type
  TEditLiquidacion_DevEx = class(TZetaDlgModal_DevEx)
    PanelIdentifica: TPanel;
    Splitter: TSplitter;
    ValorActivo1: TPanel;
    ValorActivo2: TPanel;
    DataSource: TDataSource;
    PageControl: TcxPageControl;
    tsLiquidacion: TcxTabSheet;
    Label6: TLabel;
    GBHoras: TcxGroupBox;
    Label13: TLabel;
    Label15: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    NO_HORAS: TZetaDBNumero;
    NO_DOBLES: TZetaDBNumero;
    NO_TRIPLES: TZetaDBNumero;
    NO_TARDES: TZetaDBNumero;
    NO_ADICION: TZetaDBNumero;
    NO_HORA_PD: TZetaDBNumero;
    GBVacaciones: TcxGroupBox;
    Label3: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    lblIgual: TLabel;
    Label9: TLabel;
    Label14: TLabel;
    VA_ANTERIORES: TZetaDBTextBox;
    VA_ACTUAL: TZetaDBTextBox;
    VA_FORMULA: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBNumero;
    GBAguinaldo: TcxGroupBox;
    Label4: TLabel;
    Label8: TLabel;
    AG_FORMULA: TZetaDBTextBox;
    NO_DIAS_AG: TZetaDBNumero;
    NO_LIQUIDA: TDBRadioGroup;
    NO_OBSERVA: TZetaDBEdit;
    tsClasificacion: TcxTabSheet;
    tsArea: TcxTabSheet;
    CB_NIVEL1lbl: TLabel;
    CB_NIVEL2lbl: TLabel;
    CB_NIVEL3lbl: TLabel;
    CB_NIVEL4lbl: TLabel;
    CB_NIVEL5lbl: TLabel;
    CB_NIVEL6lbl: TLabel;
    CB_NIVEL7lbl: TLabel;
    CB_NIVEL8lbl: TLabel;
    CB_NIVEL9lbl: TLabel;
    ZNIVEL1: TZetaTextBox;
    ZNIVEL2: TZetaTextBox;
    ZNIVEL3: TZetaTextBox;
    ZNIVEL4: TZetaTextBox;
    ZNIVEL5: TZetaTextBox;
    ZNIVEL6: TZetaTextBox;
    ZNIVEL7: TZetaTextBox;
    ZNIVEL8: TZetaTextBox;
    ZNIVEL9: TZetaTextBox;
    CB_NIVEL5: TZetaDBTextBox;
    CB_NIVEL2: TZetaDBTextBox;
    CB_NIVEL3: TZetaDBTextBox;
    CB_NIVEL4: TZetaDBTextBox;
    CB_NIVEL1: TZetaDBTextBox;
    CB_NIVEL6: TZetaDBTextBox;
    CB_NIVEL7: TZetaDBTextBox;
    CB_NIVEL8: TZetaDBTextBox;
    CB_NIVEL9: TZetaDBTextBox;
    CB_FEC_INGlbl: TLabel;
    CB_PUESTOlbl: TLabel;
    CB_HORARIOlbl: TLabel;
    CB_FEC_ING: TZetaDBTextBox;
    Label18: TLabel;
    CB_FEC_ANT: TZetaDBTextBox;
    ZAntiguedad: TZetaTextBox;
    CB_PUESTO: TZetaDBTextBox;
    CB_TURNO: TZetaDBTextBox;
    LBaja: TLabel;
    CB_FEC_BAJ: TZetaDBTextBox;
    dsEmpleado: TDataSource;
    TU_DESCRIP: TZetaTextBox;
    PU_DESCRIP: TZetaTextBox;
    FechaBaja: TZetaFecha;
    Label16: TLabel;
    NO_ANT_AG: TZetaDBTextBox;
    lblDiasPagados: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    NO_DIAS_IN: TZetaDBTextBox;
    Label17: TLabel;
    NO_DIAS_CU: TZetaDBTextBox;
    Label19: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    NO_DIAS_CO: TZetaDBTextBox;
    Label25: TLabel;
    NO_DIAS_PV: TZetaDBNumero;
    {$ifdef ACS}
    CB_NIVEL10lbl: TLabel;{OP: 4.Ago.08}
    CB_NIVEL10: TZetaDBTextBox;{OP: 4.Ago.08}
    ZNIVEL10: TZetaTextBox;{OP: 4.Ago.08}
    CB_NIVEL11lbl: TLabel;{OP: 4.Ago.08}
    CB_NIVEL11: TZetaDBTextBox;{OP: 4.Ago.08}
    ZNIVEL11: TZetaTextBox;{OP: 4.Ago.08}
    CB_NIVEL12lbl: TLabel;{OP: 4.Ago.08}
    CB_NIVEL12: TZetaDBTextBox;{OP: 4.Ago.08}     
    ZNIVEL12: TZetaTextBox;{OP: 4.Ago.08}
    {$endif}
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure NO_LIQUIDAClick(Sender: TObject);
    procedure OK_DevExClick(Sender: TObject);
    procedure FechaBajaValidDate(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaAntig;
    procedure SetModo( const iModo: integer );
    procedure SetControlesLectura(const lHabilitar:Boolean);
    procedure RemoverClose(const lHabilitar:Boolean);
  protected
    { Protected declarations }
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override; { TWinControl }
    procedure ConectaLiquidacion; virtual;
    procedure Connect;
    procedure SetCamposNivel;
  public
    { Public declarations }
  end;

var
  EditLiquidacion_DevEx: TEditLiquidacion_DevEx;

implementation

uses DNomina,
     DCliente,
     dTablas,
     dCatalogos,
     DGlobal,
     ZetaDialogo,
     ZetaCommonClasses,
     ZetaCommonTools,
     ZetaClientTools,
     ZetaClientDataSet,
     ZetaCommonLists,
     ZToolsPE;

{$R *.DFM}

procedure TEditLiquidacion_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     {$ifdef ACS}
     CB_NIVEL10.DataSource := dsEmpleado;
     CB_NIVEL11.DataSource := dsEmpleado;
     CB_NIVEL12.DataSource := dsEmpleado;
     CB_NIVEL10.Visible := True;
     CB_NIVEL11.Visible := True;
     CB_NIVEL12.Visible := True;
     CB_NIVEL10lbl.Visible := True;
     CB_NIVEL11lbl.Visible := True;
     CB_NIVEL12lbl.Visible := True;
     ZNIVEL10.Visible := True;
     ZNIVEL11.Visible := True;
     ZNIVEL12.Visible := True;
     {$endif}
     HelpContext := H30335_Liquidacion;
     SetCamposNivel;
     SetModo( mrNone );
     VA_FORMULA.Visible:= False; //Ver Defecto 1598: Ya nose calcula de esta forma
     lblIgual.Visible:= False;
end;

procedure TEditLiquidacion_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     with dmCliente do
     begin
          ValorActivo1.Caption := '   ' + GetEmpleadoDescripcion;
          ValorActivo2.Caption := GetPeriodoDescripcion + '   ';

     end;
     ConectaLiquidacion;
     with dmNomina do
     begin
          with FechaBaja do
          begin
               Valor := FechaBajaLiquidacion;
               Enabled := dmCliente.GetDatosEmpleadoActivo.Activo;
          end;
     end;
     Connect;
     PageControl.ActivePage := tsLiquidacion;
     if FechaBaja.Enabled then
        FechaBaja.SetFocus
     else
         if (NO_HORAS.Enabled) and (Global.OK_ProyectoEspecial(peRecontrataciones)) then
            NO_HORAS.SetFocus;
     with NO_ANT_AG do
     begin
          Visible := ( DataSource.DataSet.FieldByName( 'NO_ASK_AG' ).AsBoolean = True );
          lblDiasPagados.Visible := Visible;
     end;
     if Global.OK_ProyectoEspecial(peRecontrataciones)then
     			SetControlesLectura(not dmNomina.AplicandoBaja);
end;

procedure TEditLiquidacion_DevEx.ConectaLiquidacion;
begin
     DataSource.DataSet := dmNomina.cdsLiquidacion;
end;

procedure TEditLiquidacion_DevEx.Connect;
begin
     dsEmpleado.DataSet := dmCliente.cdsEmpleado;
     with dmTablas do
     begin
          with dmCliente.cdsEmpleado do
          begin
               if ZNIVEL1.Visible then
               begin
                    cdsNivel1.Conectar;
                    ZNIVEL1.Caption:= cdsNivel1.GetDescripcion( FieldByName( 'CB_NIVEL1' ).AsString );
               end;
               if ZNIVEL2.Visible then
               begin
                    cdsNivel2.Conectar;
                    ZNIVEL2.Caption:= cdsNivel2.GetDescripcion( FieldByName( 'CB_NIVEL2' ).AsString );
               end;
               if ZNIVEL3.Visible then
               begin
                    cdsNivel3.Conectar;
                    ZNIVEL3.Caption:= cdsNivel3.GetDescripcion( FieldByName( 'CB_NIVEL3' ).AsString );
               end;
               if ZNIVEL4.Visible then
               begin
                    cdsNivel4.Conectar;
                    ZNIVEL4.Caption:= cdsNivel4.GetDescripcion( FieldByName( 'CB_NIVEL4' ).AsString );
               end;
               if ZNIVEL5.Visible then
               begin
                    cdsNivel5.Conectar;
                    ZNIVEL5.Caption:= cdsNivel5.GetDescripcion( FieldByName( 'CB_NIVEL5' ).AsString );
               end;
               if ZNIVEL6.Visible then
               begin
                    cdsNivel6.Conectar;
                    ZNIVEL6.Caption:= cdsNivel6.GetDescripcion( FieldByName( 'CB_NIVEL6' ).AsString );
               end;
               if ZNIVEL7.Visible then
               begin
                    cdsNivel7.Conectar;
                    ZNIVEL7.Caption:= cdsNivel7.GetDescripcion( FieldByName( 'CB_NIVEL7' ).AsString );
               end;
               if ZNIVEL8.Visible then
               begin
                    cdsNivel8.Conectar;
                    ZNIVEL8.Caption:= cdsNivel8.GetDescripcion( FieldByName( 'CB_NIVEL8' ).AsString );
               end;
               if ZNIVEL9.Visible then
               begin
                    cdsNivel9.Conectar;
                    ZNIVEL9.Caption:= cdsNivel9.GetDescripcion( FieldByName( 'CB_NIVEL9' ).AsString );
               end;
               {$ifdef ACS}
               if ZNIVEL10.Visible then{OP: 4.Ago.08}
               begin
                    cdsNivel10.Conectar;
                    ZNIVEL10.Caption:= cdsNivel10.GetDescripcion( FieldByName( 'CB_NIVEL10' ).AsString );
               end;
               if ZNIVEL11.Visible then{OP: 4.Ago.08}
               begin
                    cdsNivel11.Conectar;
                    ZNIVEL11.Caption:= cdsNivel11.GetDescripcion( FieldByName( 'CB_NIVEL11' ).AsString );
               end;
               if ZNIVEL12.Visible then{OP: 4.Ago.08}
               begin
                    cdsNivel12.Conectar;
                    ZNIVEL12.Caption:= cdsNivel12.GetDescripcion( FieldByName( 'CB_NIVEL12' ).AsString );
               end;
               {$endif}
               //ZAntiguedad.Caption := TiempoDias( FieldByName( 'CB_FEC_BAJ' ).AsDateTime - FieldbyName('CB_FEC_ANT').AsDateTime + 1, etDias );
               //ZAntiguedad.Caption := TiempoDias( dmNomina.FechaBajaLiquidacion - FieldbyName('CB_FEC_ANT').AsDateTime + 1, etDias );
               CalculaAntig;
               dmCatalogos.cdsPuestos.Conectar;
               PU_DESCRIP.Caption:= dmCatalogos.cdsPuestos.GetDescripcion( FieldByName( 'CB_PUESTO' ).AsString );
               dmCatalogos.cdsTurnos.Conectar;
               TU_DESCRIP.Caption:= dmCatalogos.cdsTurnos.GetDescripcion( FieldByName( 'CB_TURNO' ).AsString );
          end;
     end;
end;

procedure TEditLiquidacion_DevEx.CalculaAntig;
begin
     ZAntiguedad.Caption := TiempoDias( dmNomina.FechaBajaLiquidacion - dmCliente.cdsEmpleado.FieldbyName('CB_FEC_ANT').AsDateTime + 1, etDias );
end;

procedure TEditLiquidacion_DevEx.NO_LIQUIDAClick(Sender: TObject);
begin
     inherited;
     NO_OBSERVA.Text := ZetaCommonLists.ObtieneElemento( lfLiqNomina, NO_LIQUIDA.ItemIndex + 1 );
end;

procedure TEditLiquidacion_DevEx.SetCamposNivel;
var
   iNiveles : Integer;
begin
     iNiveles := Global.NumNiveles;
     SetCampoNivel( 1, iNiveles, CB_NIVEL1lbl, ZNIVEL1 );
     SetCampoNivel( 2, iNiveles, CB_NIVEL2lbl, ZNIVEL2 );
     SetCampoNivel( 3, iNiveles, CB_NIVEL3lbl, ZNIVEL3 );
     SetCampoNivel( 4, iNiveles, CB_NIVEL4lbl, ZNIVEL4 );
     SetCampoNivel( 5, iNiveles, CB_NIVEL5lbl, ZNIVEL5 );
     SetCampoNivel( 6, iNiveles, CB_NIVEL6lbl, ZNIVEL6 );
     SetCampoNivel( 7, iNiveles, CB_NIVEL7lbl, ZNIVEL7 );
     SetCampoNivel( 8, iNiveles, CB_NIVEL8lbl, ZNIVEL8 );
     SetCampoNivel( 9, iNiveles, CB_NIVEL9lbl, ZNIVEL9 );
     {$ifdef ACS}{OP: 5.Ago.08}
     SetCampoNivel( 10, iNiveles, CB_NIVEL10lbl, ZNIVEL10 );
     SetCampoNivel( 11, iNiveles, CB_NIVEL11lbl, ZNIVEL11 );
     SetCampoNivel( 12, iNiveles, CB_NIVEL12lbl, ZNIVEL12 );
     {$endif}
     CB_NIVEL1.Visible  := ZNIVEL1.Visible;
     CB_NIVEL2.Visible  := ZNIVEL2.Visible;
     CB_NIVEL3.Visible  := ZNIVEL3.Visible;
     CB_NIVEL4.Visible  := ZNIVEL4.Visible;
     CB_NIVEL5.Visible  := ZNIVEL5.Visible;
     CB_NIVEL6.Visible  := ZNIVEL6.Visible;
     CB_NIVEL7.Visible  := ZNIVEL7.Visible;
     CB_NIVEL8.Visible  := ZNIVEL8.Visible;
     CB_NIVEL9.Visible  := ZNIVEL9.Visible;
     {$ifdef ACS}{OP: 5.Ago.08}
     CB_NIVEL10.Visible  := ZNIVEL10.Visible;
     CB_NIVEL11.Visible  := ZNIVEL11.Visible;
     CB_NIVEL12.Visible  := ZNIVEL12.Visible;
     {$endif}
end;

procedure TEditLiquidacion_DevEx.SetModo(const iModo: integer);
begin
     ModalResult := iModo;
end;

procedure TEditLiquidacion_DevEx.FechaBajaValidDate(Sender: TObject);
begin
     inherited;
     with dmNomina do
     begin
          if ( FechaBajaLiquidacion <> FechaBaja.Valor ) then
          begin
               FechaBajaLiquidacion := FechaBaja.Valor;
               TZetaClientDataSet( DataSource.DataSet ).Refrescar;
               CalculaAntig;
          end;
     end;
end;

procedure TEditLiquidacion_DevEx.OK_DevExClick(Sender: TObject);
var
   oCursor: TCursor;
begin
     inherited;
     oCursor := Screen.Cursor;
     Screen.Cursor := crHourglass;
     try
        with TZetaClientDataSet( DataSource.DataSet ) do
        begin
             if ( FieldByName( 'NO_ASK_AG' ).AsBoolean ) then
             begin
                  Case ( ZetaDialogo.ZSiNoCancel( 'Aviso', Format( 'Ya se ha Pagado Aguinaldo a Este Empleado en el A�o %d', [ ZetaCommonTools.TheYear( FieldByName( 'CB_FEC_BAJ' ).AsDateTime ) ] ) + CR_LF +
                                                                   '� Desea Pagar Aguinaldo en la Liquidaci�n ?', 0, mbCancel ) ) of
                       mrYes:
                       begin
                            Enviar;
                            SetModo( mrOk );
                       end;
                       mrNo:
                       begin
                            if ( State = dsBrowse ) then
                               Edit;
                            FieldByName( 'NO_DIAS_AG' ).AsFloat := 0;
                            Enviar;
                            SetModo( mrOK );
                       end;
                       mrCancel:
                       begin
                            SetModo( mrNone );
                       end;
                  end;
             end
             else
             begin
                  Enviar;
                  SetModo( mrOK );
             end;
        end;
     finally
            Screen.Cursor := oCursor;
     end;
end;

procedure TEditLiquidacion_DevEx.SetControlesLectura(const lHabilitar:Boolean);
begin
     FechaBaja.Enabled := lHabilitar;
     NO_LIQUIDA.Enabled := lHabilitar;
     NO_DIAS_VA.Enabled := lHabilitar;
     NO_DIAS_PV.Enabled := lHabilitar;
     NO_DIAS_AG.Enabled := lHabilitar;
     NO_HORAS.Enabled := lHabilitar;
     NO_DOBLES.Enabled := lHabilitar;
     NO_TRIPLES.Enabled := lHabilitar;
     NO_TARDES.Enabled := lHabilitar;
     NO_ADICION.Enabled := lHabilitar;
     NO_HORA_PD.Enabled := lHabilitar;
     Cancelar_DevEx.Enabled := lHabilitar;
     RemoverClose(lHabilitar);
end;

procedure TEditLiquidacion_DevEx.RemoverClose(const lHabilitar:Boolean);
const
   MnuCommand = SC_CLOSE;
 var
   SysMenu : HMenu;
 begin
   SysMenu := GetSystemMenu(Handle, FALSE) ;
   if not lHabilitar then
       DeleteMenu(SysMenu, MnuCommand, MF_BYCOMMAND);
end;

procedure TEditLiquidacion_DevEx.KeyDown( var Key: Word; Shift: TShiftState );
begin
     inherited KeyDown( Key, Shift );
     if Global.OK_ProyectoEspecial(peRecontrataciones)then
     begin
          if ( Key <> 0 ) then
          begin
               if ( ssAlt in Shift ) then { ALT }
               begin
                    case Key of
                         VK_F4:    { ALT + F4 }
                         begin
                              if dmNomina.AplicandoBaja then
                                 Key := 0;
                         end;
                    end;
               end;
          end;
     end;
end;

end.
