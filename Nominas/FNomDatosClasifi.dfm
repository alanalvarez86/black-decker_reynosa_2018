inherited NomDatosClasifi: TNomDatosClasifi
  Left = 334
  Top = 254
  Caption = 'N'#243'mina: Clasificaci'#243'n'
  ClientHeight = 336
  ClientWidth = 586
  PixelsPerInch = 96
  TextHeight = 13
  object Label18: TLabel [0]
    Left = 196
    Top = 155
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nivel9:'
  end
  object ZetaTextBox2: TZetaTextBox [1]
    Left = 237
    Top = 153
    Width = 340
    Height = 17
    AutoSize = False
    Caption = 'ZNIVEL9'
    ShowAccelChar = False
    Brush.Color = clSilver
    Border = False
  end
  inherited PanelIdentifica: TPanel
    Width = 586
    inherited Slider: TSplitter
      Left = 381
    end
    inherited ValorActivo1: TPanel
      Width = 365
      inherited textoValorActivo1: TLabel
        Width = 359
      end
    end
    inherited ValorActivo2: TPanel
      Left = 384
      Width = 202
      inherited textoValorActivo2: TLabel
        Width = 196
      end
    end
  end
  object PageControl1: TPageControl [3]
    Left = 0
    Top = 19
    Width = 586
    Height = 317
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    TabPosition = tpBottom
    object TabSheet1: TTabSheet
      Caption = 'Clasificaci'#243'n'
      object Label1: TLabel
        Left = 70
        Top = 119
        Width = 31
        Height = 13
        Alignment = taRightJustify
        Caption = 'Turno:'
      end
      object Label2: TLabel
        Left = 65
        Top = 138
        Width = 36
        Height = 13
        Alignment = taRightJustify
        Caption = 'Puesto:'
      end
      object Label3: TLabel
        Left = 39
        Top = 158
        Width = 62
        Height = 13
        Alignment = taRightJustify
        Caption = 'Clasificaci'#243'n:'
      end
      object cb_turno: TZetaDBTextBox
        Left = 105
        Top = 117
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'cb_turno'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'cb_turno'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object cb_puesto: TZetaDBTextBox
        Left = 105
        Top = 136
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'cb_puesto'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'cb_puesto'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object cb_clasifi: TZetaDBTextBox
        Left = 105
        Top = 156
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'cb_clasifi'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'cb_clasifi'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Turno: TZetaDBTextBox
        Left = 178
        Top = 117
        Width = 247
        Height = 17
        AutoSize = False
        Caption = 'Turno'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Turno'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Puesto: TZetaDBTextBox
        Left = 178
        Top = 136
        Width = 247
        Height = 17
        AutoSize = False
        Caption = 'Puesto'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Puesto'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Clasifi: TZetaDBTextBox
        Left = 178
        Top = 156
        Width = 247
        Height = 17
        AutoSize = False
        Caption = 'Clasifi'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Clasifi'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label4: TLabel
        Left = 33
        Top = 177
        Width = 68
        Height = 13
        Alignment = taRightJustify
        Caption = 'Reg. Patronal:'
      end
      object cb_patron: TZetaDBTextBox
        Left = 105
        Top = 175
        Width = 65
        Height = 17
        AutoSize = False
        Caption = 'cb_patron'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'cb_patron'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Patron: TZetaDBTextBox
        Left = 178
        Top = 175
        Width = 247
        Height = 17
        AutoSize = False
        Caption = 'Patron'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'Patron'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label6: TLabel
        Left = 27
        Top = 20
        Width = 74
        Height = 13
        Alignment = taRightJustify
        Caption = 'Observaciones:'
      end
      object NO_OBSERVA: TZetaDBTextBox
        Left = 105
        Top = 18
        Width = 320
        Height = 17
        AutoSize = False
        Caption = 'NO_OBSERVA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_OBSERVA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label5: TLabel
        Left = 18
        Top = 100
        Width = 83
        Height = 13
        Alignment = taRightJustify
        Caption = 'Zona Geogr'#225'fica:'
      end
      object CB_ZONA_GE: TZetaDBTextBox
        Left = 105
        Top = 98
        Width = 28
        Height = 17
        AutoSize = False
        Caption = 'CB_ZONA_GE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_ZONA_GE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label12: TLabel
        Left = 58
        Top = 40
        Width = 43
        Height = 13
        Alignment = taRightJustify
        Caption = 'Modific'#243':'
      end
      object US_CODIGO: TZetaDBTextBox
        Left = 105
        Top = 37
        Width = 160
        Height = 17
        AutoSize = False
        Caption = 'US_CODIGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'US_CODIGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label33: TLabel
        Left = 68
        Top = 2
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Status:'
      end
      object Label7: TLabel
        Left = 250
        Top = 2
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tipo Liquidaci'#243'n:'
      end
      object NO_LIQUIDA: TZetaDBTextBox
        Left = 336
        Top = 0
        Width = 89
        Height = 17
        AutoSize = False
        Caption = 'NO_LIQUIDA'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_LIQUIDA'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_STATUS: TZetaDBTextBox
        Left = 105
        Top = 0
        Width = 89
        Height = 17
        AutoSize = False
        Caption = 'NO_STATUS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_STATUS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object bbMostrarCalendario: TSpeedButton
        Left = 428
        Top = 114
        Width = 23
        Height = 22
        Hint = 'Mostrar Calendario del Turno'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
          003337777777777777F330FFFFFFFFFFF03337F3333FFF3337F330FFFF000FFF
          F03337F33377733337F330FFFFF0FFFFF03337F33337F33337F330FFFF00FFFF
          F03337F33377F33337F330FFFFF0FFFFF03337F33337333337F330FFFFFFFFFF
          F03337FFF3F3F3F3F7F33000F0F0F0F0F0333777F7F7F7F7F7F330F0F000F070
          F03337F7F777F777F7F330F0F0F0F070F03337F7F7373777F7F330F0FF0FF0F0
          F03337F733733737F7F330FFFFFFFF00003337F33333337777F330FFFFFFFF0F
          F03337FFFFFFFF7F373330999999990F033337777777777F733330FFFFFFFF00
          333337FFFFFFFF77333330000000000333333777777777733333}
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        OnClick = bbMostrarCalendarioClick
      end
      object lblZNivel0: TLabel
        Left = 20
        Top = 196
        Width = 81
        Height = 13
        Alignment = taRightJustify
        Caption = 'Confidencialidad:'
      end
      object ZNIVEL0: TZetaTextBox
        Left = 178
        Top = 194
        Width = 247
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object CodNIVEL0: TZetaTextBox
        Left = 105
        Top = 194
        Width = 65
        Height = 17
        AutoSize = False
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object Label16: TLabel
        Left = 257
        Top = 56
        Width = 150
        Height = 13
        Caption = 'Dep'#243'sito en Banca Electr'#243'nica:'
      end
      object Label17: TLabel
        Left = 11
        Top = 216
        Width = 90
        Height = 13
        Alignment = taRightJustify
        Caption = 'Banca Electr'#243'nica:'
      end
      object Label19: TLabel
        Left = 6
        Top = 234
        Width = 95
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tarjeta de Gasolina:'
      end
      object Label20: TLabel
        Left = -1
        Top = 252
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Tarjeta de Despensa:'
      end
      object CB_BAN_ELE: TZetaDBTextBox
        Left = 105
        Top = 214
        Width = 194
        Height = 17
        AutoSize = False
        Caption = 'CB_BAN_ELE'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_BAN_ELE'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_CTA_GAS: TZetaDBTextBox
        Left = 105
        Top = 233
        Width = 194
        Height = 17
        AutoSize = False
        Caption = 'CB_CTA_GAS'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_CTA_GAS'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object CB_CTA_VAL: TZetaDBTextBox
        Left = 105
        Top = 252
        Width = 194
        Height = 17
        AutoSize = False
        Caption = 'CB_CTA_VAL'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'CB_CTA_VAL'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object Label21: TLabel
        Left = 19
        Top = 81
        Width = 82
        Height = 13
        Alignment = taRightJustify
        Caption = 'M'#233'todo de Pago:'
      end
      object NO_PAGO: TZetaDBTextBox
        Left = 105
        Top = 79
        Width = 194
        Height = 17
        AutoSize = False
        Caption = 'NO_PAGO'
        ShowAccelChar = False
        Brush.Color = clBtnFace
        Border = True
        DataField = 'NO_PAGO'
        DataSource = DataSource
        FormatFloat = '%14.2n'
        FormatCurrency = '%m'
      end
      object NO_FUERA: TDBCheckBox
        Left = 329
        Top = 37
        Width = 97
        Height = 17
        Alignment = taLeftJustify
        Caption = 'Pago por Fuera:'
        DataField = 'NO_FUERA'
        DataSource = DataSource
        ReadOnly = True
        TabOrder = 0
        ValueChecked = 'S'
        ValueUnchecked = 'N'
      end
      object ChkDepositoBanca: TCheckBox
        Left = 408
        Top = 54
        Width = 18
        Height = 17
        Alignment = taLeftJustify
        Color = clBtnFace
        Enabled = False
        ParentColor = False
        State = cbGrayed
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Niveles'
      object CB_NIVEL1lbl: TLabel
        Left = 196
        Top = 2
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel1:'
      end
      object CB_NIVEL2lbl: TLabel
        Left = 196
        Top = 21
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel2:'
      end
      object CB_NIVEL3lbl: TLabel
        Left = 196
        Top = 40
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel3:'
      end
      object CB_NIVEL4lbl: TLabel
        Left = 196
        Top = 59
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel4:'
      end
      object CB_NIVEL5lbl: TLabel
        Left = 196
        Top = 79
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel5:'
      end
      object CB_NIVEL6lbl: TLabel
        Left = 196
        Top = 98
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel6:'
      end
      object CB_NIVEL7lbl: TLabel
        Left = 196
        Top = 117
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel7:'
      end
      object CB_NIVEL8lbl: TLabel
        Left = 196
        Top = 136
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel8:'
      end
      object CB_NIVEL9lbl: TLabel
        Left = 196
        Top = 155
        Width = 33
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel9:'
      end
      object ZNIVEL1: TZetaTextBox
        Left = 237
        Top = 0
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL1'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL2: TZetaTextBox
        Left = 237
        Top = 19
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL2'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL3: TZetaTextBox
        Left = 237
        Top = 38
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL3'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL4: TZetaTextBox
        Left = 237
        Top = 57
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL4'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL5: TZetaTextBox
        Left = 237
        Top = 77
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL5'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL6: TZetaTextBox
        Left = 237
        Top = 96
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL6'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL7: TZetaTextBox
        Left = 237
        Top = 115
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL7'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL8: TZetaTextBox
        Left = 237
        Top = 134
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL8'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL9: TZetaTextBox
        Left = 237
        Top = 153
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL9'
        ShowAccelChar = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL10lbl: TLabel
        Left = 190
        Top = 174
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel10:'
        Visible = False
      end
      object ZNIVEL10: TZetaTextBox
        Left = 237
        Top = 172
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL10'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL11lbl: TLabel
        Left = 190
        Top = 193
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel11:'
        Visible = False
      end
      object ZNIVEL11: TZetaTextBox
        Left = 237
        Top = 191
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL11'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object ZNIVEL12: TZetaTextBox
        Left = 237
        Top = 211
        Width = 340
        Height = 17
        AutoSize = False
        Caption = 'ZNIVEL12'
        ShowAccelChar = False
        Visible = False
        Brush.Color = clSilver
        Border = False
      end
      object CB_NIVEL12lbl: TLabel
        Left = 190
        Top = 213
        Width = 39
        Height = 13
        Alignment = taRightJustify
        Caption = 'Nivel12:'
        Visible = False
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Pagos y Folios'
      object GroupBox1: TGroupBox
        Left = 40
        Top = 81
        Width = 236
        Height = 144
        Caption = ' Folios '
        TabOrder = 0
        object Label8: TLabel
          Left = 45
          Top = 15
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '1:'
        end
        object NO_FOLIO_1: TZetaDBTextBox
          Left = 58
          Top = 13
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'NO_FOLIO_1'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FOLIO_1'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label9: TLabel
          Left = 45
          Top = 35
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '2:'
        end
        object NO_FOLIO_2: TZetaDBTextBox
          Left = 58
          Top = 33
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'NO_FOLIO_2'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FOLIO_2'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label10: TLabel
          Left = 45
          Top = 55
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '3:'
        end
        object NO_FOLIO_3: TZetaDBTextBox
          Left = 58
          Top = 53
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'NO_FOLIO_3'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FOLIO_3'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object NO_FOLIO_4: TZetaDBTextBox
          Left = 58
          Top = 73
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'NO_FOLIO_4'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FOLIO_4'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label11: TLabel
          Left = 45
          Top = 75
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '4:'
        end
        object Label13: TLabel
          Left = 45
          Top = 95
          Width = 9
          Height = 13
          Alignment = taRightJustify
          Caption = '5:'
        end
        object NO_FOLIO_5: TZetaDBTextBox
          Left = 58
          Top = 93
          Width = 65
          Height = 17
          AutoSize = False
          Caption = 'NO_FOLIO_5'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FOLIO_5'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
      object GroupBox2: TGroupBox
        Left = 40
        Top = 5
        Width = 441
        Height = 73
        Caption = ' Pagos '
        TabOrder = 1
        object Label14: TLabel
          Left = 23
          Top = 18
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Pagado:'
        end
        object NO_FEC_PAG: TZetaDBTextBox
          Left = 67
          Top = 16
          Width = 78
          Height = 17
          AutoSize = False
          Caption = 'NO_FEC_PAG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_FEC_PAG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
        object Label15: TLabel
          Left = 23
          Top = 42
          Width = 40
          Height = 13
          Alignment = taRightJustify
          Caption = 'Captur'#243':'
        end
        object NO_USR_PAG: TZetaDBTextBox
          Left = 67
          Top = 40
          Width = 310
          Height = 17
          AutoSize = False
          Caption = 'NO_USR_PAG'
          ShowAccelChar = False
          Brush.Color = clBtnFace
          Border = True
          DataField = 'NO_USR_PAG'
          DataSource = DataSource
          FormatFloat = '%14.2n'
          FormatCurrency = '%m'
        end
      end
    end
  end
  inherited DataSource: TDataSource
    Left = 483
    Top = 36
  end
end
