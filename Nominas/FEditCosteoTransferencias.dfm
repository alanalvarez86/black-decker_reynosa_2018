inherited EditCosteoTransferencias: TEditCosteoTransferencias
  Left = 314
  Top = 237
  Caption = 'Transferencias'
  ClientHeight = 411
  ClientWidth = 480
  PixelsPerInch = 96
  TextHeight = 13
  object TipoAutLbl: TLabel [0]
    Left = 102
    Top = 219
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo:'
  end
  object HorasLbl: TLabel [1]
    Left = 95
    Top = 144
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Horas:'
    FocusControl = TR_HORAS
  end
  object MotivoLbl: TLabel [2]
    Left = 91
    Top = 169
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Motivo:'
    FocusControl = TR_MOTIVO
  end
  object EmpleadoLbl: TLabel [3]
    Left = 76
    Top = 69
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empleado:'
    FocusControl = CB_CODIGO
  end
  object Label3: TLabel [4]
    Left = 76
    Top = 245
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Transfiere:'
    FocusControl = TR_MOTIVO
  end
  object US_DESCRIP: TZetaDBTextBox [5]
    Left = 128
    Top = 241
    Width = 198
    Height = 21
    AutoSize = False
    Caption = 'US_DESCRIP'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'US_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lbDestino: TLabel [6]
    Left = 53
    Top = 119
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'CC a Transferir:'
    FocusControl = CC_CODIGO
  end
  object Label5: TLabel [7]
    Left = 93
    Top = 45
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fecha:'
    FocusControl = CB_CODIGO
  end
  object CC_ORIGEN: TZetaDBTextBox [8]
    Left = 128
    Top = 90
    Width = 60
    Height = 21
    AutoSize = False
    Caption = 'CC_ORIGEN'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CC_ORIGEN'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object lbOrigen: TLabel [9]
    Left = 75
    Top = 94
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'CC Origen:'
    FocusControl = CC_CODIGO
  end
  object CC_DESCRIP: TZetaDBTextBox [10]
    Left = 192
    Top = 90
    Width = 232
    Height = 21
    AutoSize = False
    Caption = 'CC_DESCRIP'
    ShowAccelChar = False
    Brush.Color = clBtnFace
    Border = True
    DataField = 'CC_DESCRIP'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object TR_FECHA: TZetaDBTextBox [11]
    Left = 328
    Top = 241
    Width = 100
    Height = 21
    AutoSize = False
    Caption = 'TR_FECHA'
    ShowAccelChar = False
    Layout = tlCenter
    Brush.Color = clBtnFace
    Border = True
    DataField = 'TR_FECHA'
    DataSource = DataSource
    FormatFloat = '%14.2n'
    FormatCurrency = '%m'
  end
  object Label2: TLabel [12]
    Left = 52
    Top = 194
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observaciones:'
  end
  object Label1: TLabel [13]
    Left = 93
    Top = 268
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Global:'
    FocusControl = TR_MOTIVO
  end
  inherited PanelBotones: TPanel
    Top = 375
    Width = 480
    TabOrder = 11
    inherited OK: TBitBtn
      Left = 312
    end
    inherited Cancelar: TBitBtn
      Left = 397
    end
  end
  inherited PanelSuperior: TPanel
    Width = 480
    TabOrder = 0
  end
  inherited PanelIdentifica: TPanel
    Width = 480
    TabOrder = 1
    inherited ValorActivo2: TPanel
      Width = 154
    end
  end
  object CB_CODIGO: TZetaDBKeyLookup [17]
    Left = 128
    Top = 65
    Width = 300
    Height = 21
    LookupDataset = dmCliente.cdsEmpleadoLookUp
    TabOrder = 3
    TabStop = True
    WidthLlave = 80
    DataField = 'CB_CODIGO'
    DataSource = DataSource
  end
  object TR_MOTIVO: TZetaDBKeyLookup [18]
    Left = 128
    Top = 165
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsMotivoTransfer
    TabOrder = 6
    TabStop = True
    WidthLlave = 60
    DataField = 'TR_MOTIVO'
    DataSource = DataSource
  end
  object TR_HORAS: TZetaDBNumero [19]
    Left = 128
    Top = 140
    Width = 60
    Height = 21
    Mascara = mnHoras
    TabOrder = 5
    Text = '0.00'
    DataField = 'TR_HORAS'
    DataSource = DataSource
  end
  object CC_CODIGO: TZetaDBKeyLookup [20]
    Left = 128
    Top = 115
    Width = 300
    Height = 21
    LookupDataset = dmTablas.cdsNivel1
    TabOrder = 4
    TabStop = True
    WidthLlave = 60
    DataField = 'CC_CODIGO'
    DataSource = DataSource
  end
  object AU_FECHA: TZetaDBFecha [21]
    Left = 128
    Top = 40
    Width = 115
    Height = 22
    Cursor = crArrow
    TabOrder = 2
    Text = '04/Mar/11'
    Valor = 40606.000000000000000000
    DataField = 'AU_FECHA'
    DataSource = DataSource
  end
  object TR_TIPO: TZetaDBKeyCombo [22]
    Left = 128
    Top = 215
    Width = 198
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 8
    ListaFija = lfTipoHoraTransferenciaCosteo
    ListaVariable = lvPuesto
    Offset = 0
    Opcional = False
    EsconderVacios = False
    DataField = 'TR_TIPO'
    DataSource = DataSource
    LlaveNumerica = True
  end
  object TR_TEXTO: TZetaDBEdit [23]
    Left = 128
    Top = 190
    Width = 300
    Height = 21
    TabOrder = 7
    Text = 'TR_TEXTO'
    DataField = 'TR_TEXTO'
    DataSource = DataSource
  end
  object TR_GLOBAL: TDBCheckBox [24]
    Left = 128
    Top = 266
    Width = 15
    Height = 17
    DataField = 'TR_GLOBAL'
    DataSource = DataSource
    ReadOnly = True
    TabOrder = 9
    ValueChecked = 'S'
    ValueUnchecked = 'N'
  end
  object GroupBox1: TGroupBox [25]
    Left = 0
    Top = 283
    Width = 480
    Height = 92
    Align = alBottom
    Caption = ' Aprobaci'#243'n: '
    TabOrder = 10
    object Label4: TLabel
      Left = 93
      Top = 19
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status:'
    end
    object TR_FEC_APR: TZetaDBTextBox
      Left = 328
      Top = 63
      Width = 100
      Height = 21
      AutoSize = False
      Caption = 'TR_FEC_APR'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'TR_FEC_APR'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object Label6: TLabel
      Left = 65
      Top = 43
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Comentarios:'
    end
    object Label7: TLabel
      Left = 89
      Top = 67
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'Recibe:'
      FocusControl = TR_MOTIVO
    end
    object ZetaDBTextBox1: TZetaDBTextBox
      Left = 128
      Top = 63
      Width = 198
      Height = 21
      AutoSize = False
      Caption = 'ZetaDBTextBox1'
      ShowAccelChar = False
      Layout = tlCenter
      Brush.Color = clBtnFace
      Border = True
      DataField = 'US_APRUEBA'
      DataSource = DataSource
      FormatFloat = '%14.2n'
      FormatCurrency = '%m'
    end
    object TR_STATUS: TZetaDBKeyCombo
      Left = 128
      Top = 15
      Width = 198
      Height = 21
      AutoComplete = False
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 0
      ListaFija = lfStatusTransCosteo
      ListaVariable = lvPuesto
      Offset = 0
      Opcional = False
      EsconderVacios = False
      DataField = 'TR_STATUS'
      DataSource = DataSource
      LlaveNumerica = True
    end
    object TR_TXT_APR: TZetaDBEdit
      Left = 128
      Top = 39
      Width = 300
      Height = 21
      TabOrder = 1
      Text = 'TR_TXT_APR'
      DataField = 'TR_TXT_APR'
      DataSource = DataSource
    end
  end
  inherited DataSource: TDataSource
    Left = 65532
    Top = 49
  end
end
