unit FPagoRecibos_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls,
     Forms, Dialogs, StdCtrls, ExtCtrls, 
     ZBaseDlgModal_DevEx, cxGraphics, cxLookAndFeels,
     cxLookAndFeelPainters, Menus, dxSkinsCore, 
     TressMorado2013, dxSkinsDefaultPainters, ImgList, cxButtons, cxControls,
     cxContainer, cxEdit, cxGroupBox, cxRadioGroup;

type
  TPagoRecibos_DevEx = class(TZetaDlgModal_DevEx)
    RGMarcar: TcxRadioGroup;
    ValorActivo1: TPanel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PagoRecibos_DevEx: TPagoRecibos_DevEx;

implementation

uses DCliente,
     ZetaCommonClasses, dGlobal;

{$R *.DFM}

procedure TPagoRecibos_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H30336_Pago_de_recibos;
end;

procedure TPagoRecibos_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     ValorActivo1.Caption := dmCliente.GetPeriodoDescripcion;
     ActiveControl := RGMarcar;
end;

end.
