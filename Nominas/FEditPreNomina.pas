unit FEditPreNomina;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, ComCtrls,
     Dialogs, Db, ExtCtrls, DBCtrls, Buttons, StdCtrls, Grids, DBGrids,
     ZBaseEdicion,
     ZetaDBTextBox;

type
  TEditPrenomina = class(TBaseEdicion)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label17: TLabel;
    NO_DIAS: TZetaDBTextBox;
    Label18: TLabel;
    NO_DIAS_AS: TZetaDBTextBox;
    Label19: TLabel;
    NO_DIAS_FI: TZetaDBTextBox;
    Label20: TLabel;
    NO_DIAS_FJ: TZetaDBTextBox;
    Label21: TLabel;
    NO_DIAS_IN: TZetaDBTextBox;
    Label22: TLabel;
    NO_DIAS_NT: TZetaDBTextBox;
    Label23: TLabel;
    NO_DIAS_SG: TZetaDBTextBox;
    Label24: TLabel;
    NO_DIAS_SU: TZetaDBTextBox;
    Label32: TLabel;
    NO_DIAS_AG: TZetaDBTextBox;
    NO_DIAS_VA: TZetaDBTextBox;
    Label31: TLabel;
    NO_DIAS_RE: TZetaDBTextBox;
    Label30: TLabel;
    NO_DIAS_AJ: TZetaDBTextBox;
    Label29: TLabel;
    NO_DIAS_OT: TZetaDBTextBox;
    Label28: TLabel;
    NO_DIAS_CG: TZetaDBTextBox;
    Label26: TLabel;
    NO_DIAS_EM: TZetaDBTextBox;
    Label27: TLabel;
    NO_DIAS_SS: TZetaDBTextBox;
    Label25: TLabel;
    TabSheet2: TTabSheet;
    Label4: TLabel;
    NO_HORAS: TZetaDBTextBox;
    Label10: TLabel;
    NO_Hora_PD: TZetaDBTextBox;
    NO_DES_TRA: TZetaDBTextBox;
    Label11: TLabel;
    NO_EXTRAS: TZetaDBTextBox;
    Label5: TLabel;
    Label6: TLabel;
    NO_TRIPLES: TZetaDBTextBox;
    Label12: TLabel;
    NO_FES_TRA: TZetaDBTextBox;
    NO_VAC_TRA: TZetaDBTextBox;
    Label13: TLabel;
    NO_DOBLES: TZetaDBTextBox;
    Label7: TLabel;
    Label8: TLabel;
    NO_TARDES: TZetaDBTextBox;
    Label14: TLabel;
    NO_HORA_CG: TZetaDBTextBox;
    NO_HORA_SG: TZetaDBTextBox;
    Label15: TLabel;
    NO_ADICION: TZetaDBTextBox;
    Label9: TLabel;
    Label36: TLabel;
    NO_FES_PAG: TZetaDBTextBox;
    TabSheet4: TTabSheet;
    Label1: TLabel;
    CB_TURNO: TZetaDBTextBox;
    Label2: TLabel;
    Label3: TLabel;
    NO_D_TURNO: TZetaDBTextBox;
    NO_JORNADA: TZetaDBTextBox;
    TabSheet3: TTabSheet;
    Label34: TLabel;
    US_CODIGO: TZetaDBTextBox;
    Label35: TLabel;
    NO_USER_RJ: TZetaDBTextBox;
    NO_OBSERVA: TZetaDBTextBox;
    Label16: TLabel;
    Label33: TLabel;
    DBGrid: TDBGrid;
    BBModificar: TBitBtn;
    dsAusencia: TDataSource;
    STATUSNOM: TZetaDBTextBox;
    TURNO: TZetaDBTextBox;
    bbMostrarCalendario: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BBModificarClick(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure bbMostrarCalendarioClick(Sender: TObject);
    procedure DataSourceDataChange(Sender: TObject; Field: TField);
  private
    { Private declarations }
  protected
    { Protected declarations }
    procedure Connect; override;
  public
    { Public declarations }
  end;

var
  EditPrenomina: TEditPrenomina;

implementation

uses DNomina,
     DCatalogos,
     ZetaCommonLists,
     ZetaCommonClasses,
     ZetaCommonTools,
     FNomEditDatosAsist;

{$R *.DFM}

procedure TEditPrenomina.FormCreate(Sender: TObject);
begin
     inherited;
     TipoValorActivo1 := stEmpleado;
     TipoValorActivo2 := stPeriodo;
     HelpContext := H22121_edicion_prenomina;
end;

procedure TEditPrenomina.FormShow(Sender: TObject);
begin
     inherited;
     DBGrid.SelectedIndex := 0;
end;

procedure TEditPrenomina.BBModificarClick(Sender: TObject);
begin
     inherited;
     ZBaseEdicion.ShowFormaEdicion( NomEditDatosAsist, TNomEditDatosAsist );
end;

procedure TEditPrenomina.DBGridDblClick(Sender: TObject);
begin
     inherited;
     BBModificarClick( Sender );
end;

procedure TEditPrenomina.Connect;
begin
     with dmNomina do
     begin
          cdsDatosAsist.Conectar;
          DataSource.DataSet := cdsDatosAsist;
          dsAusencia.DataSet := cdsMovDatosAsist;
     end;
end;

procedure TEditPrenomina.bbMostrarCalendarioClick(Sender: TObject);
begin
     inherited;
     dmCatalogos.ShowCalendarioRitmo( CB_TURNO.Caption );
end;

procedure TEditPrenomina.DataSourceDataChange(Sender: TObject;Field: TField);
begin
     inherited;
     with Datasource do
     begin
          if ( Dataset <> nil ) then
          begin
               with DataSet do
                    bbMostrarCalendario.Enabled := ZetaCommonTools.StrLleno( FieldByName( 'CB_TURNO' ).AsString );
          end;
     end;
end;

end.
