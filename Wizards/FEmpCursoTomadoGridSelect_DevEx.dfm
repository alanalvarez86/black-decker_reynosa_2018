inherited EmpCursoTomadoGridSelect_DevEx: TEmpCursoTomadoGridSelect_DevEx
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientHeight = 255
  ClientWidth = 604
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 219
    Width = 604
    inherited OK_DevEx: TcxButton
      Left = 438
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 516
    end
  end
  inherited PanelSuperior: TPanel
    Width = 604
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 604
    Height = 184
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'CB_CODIGO'
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre'
        DataBinding.FieldName = 'PRETTYNAME'
      end
      object CU_CODIGO: TcxGridDBColumn
        Caption = 'Curso'
        DataBinding.FieldName = 'CU_CODIGO'
      end
      object KC_FEC_TOM: TcxGridDBColumn
        Caption = 'Inicio'
        DataBinding.FieldName = 'KC_FEC_TOM'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.DisplayFormat = 'dd/mmm/yyyy'
      end
      object KC_HORAS: TcxGridDBColumn
        Caption = 'Duraci'#243'n'
        DataBinding.FieldName = 'KC_HORAS'
      end
      object KC_EVALUA: TcxGridDBColumn
        Caption = 'Evaluaci'#243'n'
        DataBinding.FieldName = 'KC_EVALUA'
      end
      object KC_REVISIO: TcxGridDBColumn
        Caption = 'Revisi'#243'n'
        DataBinding.FieldName = 'KC_REVISIO'
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
