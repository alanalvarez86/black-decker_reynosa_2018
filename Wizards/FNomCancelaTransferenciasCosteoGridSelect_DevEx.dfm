inherited NomCancelaTransferenciasCosteoGridSelect_DevEx: TNomCancelaTransferenciasCosteoGridSelect_DevEx
  Left = 311
  Top = 268
  VertScrollBar.Range = 0
  AutoScroll = False
  Caption = 'Seleccione Los Empleados Deseados'
  ClientHeight = 267
  ClientWidth = 885
  PixelsPerInch = 96
  TextHeight = 13
  inherited PanelBotones: TPanel
    Top = 231
    Width = 885
    inherited OK_DevEx: TcxButton
      Left = 721
    end
    inherited Cancelar_DevEx: TcxButton
      Left = 800
      Cancel = True
    end
  end
  inherited PanelSuperior: TPanel
    Width = 885
  end
  inherited ZetaDBGrid: TZetaCXGrid
    Width = 885
    Height = 196
    inherited ZetaDBGridDBTableView: TcxGridDBTableView
      OptionsBehavior.FocusCellOnTab = True
      OptionsData.CancelOnExit = True
      OptionsSelection.CellSelect = False
      OptionsSelection.MultiSelect = True
      OptionsView.Indicator = True
      object CB_CODIGO: TcxGridDBColumn
        Caption = 'Empleado'
        DataBinding.FieldName = 'CB_CODIGO'
        Width = 57
      end
      object PRETTYNAME: TcxGridDBColumn
        Caption = 'Nombre Completo'
        DataBinding.FieldName = 'PRETTYNAME'
        Width = 128
      end
      object CC_ORIGEN: TcxGridDBColumn
        Caption = 'Origen'
        DataBinding.FieldName = 'CC_ORIGEN'
        Width = 60
      end
      object CC_DESTINO: TcxGridDBColumn
        Caption = 'Destino'
        DataBinding.FieldName = 'CC_DESTINO'
        Width = 60
      end
      object TR_HORAS: TcxGridDBColumn
        Caption = 'Horas'
        DataBinding.FieldName = 'TR_HORAS'
        Width = 50
      end
      object TR_TIPO: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'TR_TIPO'
      end
      object TR_MOTIVO: TcxGridDBColumn
        Caption = 'Motivo'
        DataBinding.FieldName = 'TR_MOTIVO'
        MinWidth = 60
        Width = 60
      end
      object TR_STATUS: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'TR_STATUS'
      end
      object US_CODIGO: TcxGridDBColumn
        Caption = 'Transfiri'#243
        DataBinding.FieldName = 'US_CODIGO'
        Width = 70
      end
      object TR_APRUEBA: TcxGridDBColumn
        Caption = 'Recibi'#243
        DataBinding.FieldName = 'TR_APRUEBA'
        Width = 65
      end
      object TR_GLOBAL: TcxGridDBColumn
        Caption = #191'Global?'
        DataBinding.FieldName = 'TR_GLOBAL'
        Width = 47
      end
      object TR_TEXTO: TcxGridDBColumn
        Caption = 'Observaciones'
        DataBinding.FieldName = 'TR_TEXTO'
        Width = 100
      end
      object TR_TXT_APR: TcxGridDBColumn
        Caption = 'Comentarios'
        DataBinding.FieldName = 'TR_TXT_APR'
        Width = 100
      end
    end
  end
  inherited cxImageList24_PanelBotones: TcxImageList
    FormatVersion = 1
  end
end
