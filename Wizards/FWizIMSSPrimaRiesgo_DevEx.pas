unit FWizIMSSPrimaRiesgo_DevEx;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, ComCtrls, ExtCtrls, Mask,
     ZetaFecha, ZetaNumero, ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
     cxLookAndFeelPainters, dxSkinsCore, TressMorado2013,
     dxSkinsDefaultPainters, cxContainer, cxEdit, ZetaCXWizard,
     dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
     dxWizardControl, Menus, ZetaKeyLookup_DevEx, cxButtons, cxCheckBox;

type
  TWizIMSSPrimaRiesgo_DevEx = class(TcxBaseWizard)
    Label1: TLabel;
    Year: TZetaNumero;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    FechaPrima: TZetaFecha;
    Archivo: TEdit;
    ArchivoLBL: TLabel;
    ArchivoSeek: TcxButton;
    RegistroPatronal: TZetaKeyLookup_DevEx;
    OpenDialog: TOpenDialog;
    Prima: TZetaNumero;
    lAcreditaSTPS: TcxCheckBox;
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure YearExit(Sender: TObject);
    procedure RegistroPatronalValidKey(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;
      var CanMove: Boolean);
    //DevEx Agregado para enfocar un control
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure GetFechaPrima;
    procedure GetPrimaRiesgo;
    function EsAcreditadoSTPS: Boolean;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }
  end;

var
  WizIMSSPrimaRiesgo_DevEx: TWizIMSSPrimaRiesgo_DevEx;

implementation

uses ZetaMessages,
     ZetaCommonClasses,
     ZetaDialogo,
     DCatalogos,
     DCliente,
     DIMSS,
     DProcesos,
     ZetaCommonTools;

{$R *.DFM}

procedure TWizIMSSPrimaRiesgo_DevEx.FormCreate(Sender: TObject);
begin
     Archivo.Text := ZetaMessages.SetFileNameDefaultPath( 'P_RIESGO.TXT' );
     inherited;
     ParametrosControl := RegistroPatronal;
     HelpContext := H40445_Calcular_prima_riesgo;
     RegistroPatronal.LookupDataset := dmCatalogos.cdsRPatron;

     //DevEx
     Parametros.PageIndex := 0;
     Ejecucion.PageIndex := 1;
end;

procedure TWizIMSSPrimaRiesgo_DevEx.FormShow(Sender: TObject);
begin
     ParametrosControl := nil;
     inherited;
     with dmCatalogos do
     begin
          cdsRPatron.Conectar;
     end;
     with dmCliente do
     begin
          Year.Valor := IMSSYear - 1;
          RegistroPatronal.Llave := IMSSPatron;
     end;
     GetPrimaRiesgo;
     GetFechaPrima;
     lAcreditaSTPS.Checked:= EsAcreditadoSTPS;

     Advertencia.Caption :=
        'Al ejecutar el proceso se genera un archivo con la nueva prima de riesgo con los par�metros del c�lculo y registra la nueva prima en el Registro Patronal.';
end;

procedure TWizIMSSPrimaRiesgo_DevEx.GetFechaPrima;
begin
     FechaPrima.Valor := EncodeDate( Year.ValorEntero + 1, 03, 01 );
end;

procedure TWizIMSSPrimaRiesgo_DevEx.GetPrimaRiesgo;
var
   sPatron: String;
begin
     sPatron := RegistroPatronal.Llave;
     if ( sPatron <> '' ) then
        Prima.Valor := dmImss.GetPrimaRiesgo( Year.ValorEntero, sPatron );
end;

procedure TWizIMSSPrimaRiesgo_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     inherited;
     with Archivo do
     begin
          with OpenDialog do
          begin
               FileName := ExtractFileName( Text );
               InitialDir := ExtractFileDir( Text );
               if Execute then
                  Text := FileName;
          end;
     end;
end;

procedure TWizIMSSPrimaRiesgo_DevEx.YearExit(Sender: TObject);
begin
     inherited;
     GetPrimaRiesgo;
     GetFechaPrima;
end;

procedure TWizIMSSPrimaRiesgo_DevEx.RegistroPatronalValidKey(Sender: TObject);
begin
     inherited;
     GetPrimaRiesgo;
     lAcreditaSTPS.Checked:= EsAcreditadoSTPS;
end;

procedure TWizIMSSPrimaRiesgo_DevEx.WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
begin
     ParametrosControl := nil;
     inherited;
     with Wizard do
     begin
          if CanMove and Adelante and EsPaginaActual( Parametros ) then
          begin
               if ( RegistroPatronal.Llave = '' ) then
               begin
                    ZetaDialogo.zError( Self.Caption, '� Registro Patronal no puede quedar vac�o !', 0 );
                    ActiveControl := RegistroPatronal;
                    CanMove := False;
               end
               else
                   if ( Archivo.Text = '' ) then
                   begin
                        ZetaDialogo.zError( Self.Caption, '� Nombre de archivo no puede quedar vac�o !', 0 );
                        ActiveControl := Archivo;
                        CanMove := False;
                   end;
          end;
     end;
end;

procedure TWizIMSSPrimaRiesgo_DevEx.CargaParametros;
begin
     inherited CargaParametros;
     with ParameterList do
     begin
          AddString( 'Patron', RegistroPatronal.Llave );
          AddString( 'Archivo', Archivo.Text );
          AddInteger( 'Year', Year.ValorEntero );
          AddFloat( 'Prima', Prima.Valor );
          AddDate( 'FechaPrima', FechaPrima.Valor );
          AddBoolean( 'AcreditaSTPS', lAcreditaSTPS.Checked );
     end;

     //DevEx
     with Descripciones do
     begin
          AddInteger( 'A�o', Year.ValorEntero );
          AddString( 'Registro Patronal', RegistroPatronal.Llave + ': ' + RegistroPatronal.Descripcion );
          AddBoolean( 'Acreditaci�n STPS', lAcreditaSTPS.Checked );
          AddFloat( 'Prima de riesgo anterior', Prima.Valor );
          AddDate( 'Fecha nueva', FechaPrima.Valor );
          AddString( 'Archivo de rastreo', Archivo.Text );
     end;
end;

function TWizIMSSPrimaRiesgo_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.CalcularPrimaRiesgo( ParameterList );
end;

{Funci�n que indica si el patr�n cuenta con Acreditaci�n por la secretar�a de trabajo y previsi�n social
  Regresa campo: RPATRON.TB_STYPS}
function TWizIMSSPrimaRiesgo_DevEx.EsAcreditadoSTPS: Boolean;
begin
     Result:= StrLleno(RegistroPatronal.Llave) and (dmCatalogos.cdsRPatron.Active)
              and zStrToBool(dmCatalogos.cdsRPatron.FieldByName('TB_STYPS').AsString);
end;


procedure TWizIMSSPrimaRiesgo_DevEx.WizardAfterMove(Sender: TObject);
begin
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := RegistroPatronal
     else
         ParametrosControl := nil;
     inherited;
end;


end.


