unit FWizEmpRenumera_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ZetaEdit, StdCtrls, ExtCtrls, ZetaKeyCombo, Mask,
  ZetaNumero, FWizEmpBaseFiltro,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore,  TressMorado2013,
  dxSkinsDefaultPainters, cxContainer, cxEdit, Menus, ZetaCXWizard,
  cxRadioGroup, cxTextEdit, cxMemo, ZetaKeyLookup_DevEx, cxButtons,
  dxGDIPlusClasses, cxImage, cxLabel, cxGroupBox, dxCustomWizardControl,
  dxWizardControl, cxCheckBox;

type
  TWizEmpRenumeraEmpleados_DevEx = class(TWizEmpBaseFiltro)
    Label5: TLabel;
    ArchivoSeek1: TcxButton;
    Label6: TLabel;
    Label7: TLabel;
    OpenDialog: TOpenDialog;
    gbArchivo: TcxGroupBox;
    ArchivoSeek: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    edArchivo: TEdit;
    Formato: TZetaKeyCombo;
    gbDelta: TcxGroupBox;
    lblIncremento: TLabel;
    edIncremento: TZetaNumero;
    ckBajasIncremento: TcxCheckBox;
    rbDelta: TcxRadioButton;
    rbTexto: TcxRadioButton;
    lblOperacion: TLabel;
    cbOperacion: TZetaKeyCombo;
    gbKardex: TcxGroupBox;
    RegGenerales: TZetaKeyLookup_DevEx;
    LblGenerales: TLabel;
    ckbKardex: TcxCheckBox;
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer;var CanMove: Boolean);
    procedure ArchivoSeekClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ckbKardexClick(Sender: TObject);
    
  private
    { Private declarations }
    function VerificaListaAscii: boolean;
    function MuestraListaCorrecta: boolean;
    procedure SetControles( const lAsciiEnabled: boolean );
    procedure HabilitaDeshabilita( gbGrupo: TcxGroupBox; lAccion: boolean );
  protected
    { Private declarations }
    function EjecutarWizard: Boolean; override;
    function Verificar: Boolean; override;
    procedure CargaParametros;override;
    procedure CargaListaVerificacion; override;
  public
    { Public declarations }
  end;

const
     FILTRO_LOOKUP = 'TB_SISTEMA=%s';

var
   WizEmpRenumeraEmpleados_DevEx: TWizEmpRenumeraEmpleados_DevEx;

implementation

uses
    DProcesos,
    ZBaseSelectGrid_DevEx,
    ZBaseGridShow_DevEx,
    ZetaClientTools,
    ZetaCommonTools,
    ZetaCommonClasses,
    ZetaCommonLists,
    ZetaDialogo,
    ZAsciiTools,
    ZGlobalTress,
    DTablas,
    FEmpRenumeraGridSelect_DevEx,
    FEmpRenumeraGridShow_DevEx, ZcxWizardBasico;

{$R *.DFM}

{ TRenumeraEmpleados }

procedure TWizEmpRenumeraEmpleados_DevEx.FormCreate(Sender: TObject);
begin
     edArchivo.Tag := K_GLOBAL_DEF_EMP_RENUMERA;
     Formato.ItemIndex := Ord( faASCIIFijo );
     cbOperacion.ItemIndex := Ord( oiSumar );
     HelpContext := H11620_Renumerar_empleados;
     inherited;
     with RegGenerales do
     begin
          LookupDataset := dmTablas.cdsMovKardex;
          Filtro := Format( FILTRO_LOOKUP, [ EntreComillas( K_GLOBAL_NO ) ] );
     end;

     //DevEx
     Parametros.PageIndex := 0;
     FiltrosCondiciones.PageIndex := 1;
     Ejecucion.PageIndex := 2;
end;

procedure TWizEmpRenumeraEmpleados_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     rbTexto.Checked := True;
     SetControles( rbTexto.Checked );
     dmTablas.cdsMovKardex.Conectar;
     ckbKardex.Checked := FALSE;
     Advertencia.Caption := 'Al ejecutar el proceso se cambiar�n los c�digos que ' +
         'identifican a los empleados que se enlistaron, este proceso generar� ' +
         'movimiento de kardex si se marc� de esta esta manera.';
end;

procedure TWizEmpRenumeraEmpleados_DevEx.RadioClick(Sender: TObject);
begin
     inherited;
     SetControles( rbTexto.Checked );
end;

procedure TWizEmpRenumeraEmpleados_DevEx.CargaParametros;
begin
     inherited;
     
     with ParameterList do
     begin
          { Parametros en el Caso de un Archivo de texto }
          AddBoolean( 'ArchivoAscii', rbTexto.Checked );
          AddString( 'Archivo', edArchivo.Text );
          AddInteger( 'Formato', Formato.Valor );

          { Parametros en el caso de un Incremento del Valor original }
          AddInteger( 'Cantidad', edIncremento.ValorEntero );
          AddInteger( 'Operacion', cbOperacion.Valor );
          AddBoolean( 'IncluirBajas', ckBajasIncremento.Checked );

          {Parametros de Kardex}
          AddBoolean('IncluirKardex',ckbKardex.Checked);
          AddString( 'KardexGeneral', RegGenerales.Llave );
     end;

     with Descripciones do
     begin
          //Clear;   //DevEx
          if rbTexto.Checked then
          begin
               AddString( 'Tipo de renumeraci�n', 'Archivo ASCII' );
               AddString( 'Archivo', edArchivo.Text );
               AddString( 'Formato ASCII', ObtieneElemento( lfFormatoASCII, Formato.Valor ) );
          end
          else
          begin
               AddString( 'Tipo de renumeraci�n', 'Incremento al c�digo actual' );
               AddInteger( 'Cantidad', edIncremento.ValorEntero );
               AddString( 'Operaci�n', ObtieneElemento( lfOperacionIncremento, cbOperacion.Valor ) );
               if ( ckBajasIncremento.Checked ) then
                  AddString( 'Inclu�r Bajas', ckBajasIncremento.Properties.ValueChecked )
               else
                   AddString( 'Inclu�r Bajas', ckBajasIncremento.Properties.valueUnchecked );
          end;
          if ( ckbKardex.Checked )then
          begin
             AddString('Inclu�r mov. de kardex',  ckbKardex.Properties.ValueChecked  );
             AddString('Tipo de registro general', RegGenerales.Llave + ': ' + RegGenerales.Descripcion);
          end;

     end;
end;

procedure TWizEmpRenumeraEmpleados_DevEx.CargaListaVerificacion;
begin
     dmProcesos.RenumeraEmpleadosGetLista( ParameterList );
end;

function TWizEmpRenumeraEmpleados_DevEx.Verificar: Boolean;
begin
     Result := ZBaseSelectGrid_DevEx.GridSelect( dmProcesos.cdsDataset, TEmpRenumeraGridSelect_DevEx );
end;

function TWizEmpRenumeraEmpleados_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.RenumeraEmpleados( ParameterList, Verificacion );
end;

procedure TWizEmpRenumeraEmpleados_DevEx.WizardBeforeMove( Sender: TObject; var iNewPage: Integer; var CanMove: Boolean );
begin
     inherited;
     with Wizard do
     begin
          if Adelante then
          begin
               if EsPaginaActual( Parametros ) then
               begin
                    if rbTexto.Checked then
                       CanMove := VerificaListaAscii and MuestraListaCorrecta
                    else
                    begin
                         if ( edIncremento.ValorEntero = 0 ) then
                            CanMove := Error( 'No se puede dejar la cantidad en cero', edIncremento )
                         else
                         begin
                              if ckbKardex.Checked and StrVacio( RegGenerales.Llave ) then
                                 CanMove := Error( 'Falta especificar el tipo de registro general', RegGenerales );
                         end;
                    end;
               end;
          end;
     end;
end;

function TWizEmpRenumeraEmpleados_DevEx.VerificaListaAscii: boolean;
begin
     CargaParametros;
     with dmProcesos do
     begin
          RenumeraVerificaListaASCII( ParameterList );
          Result := ( ErrorCount = 0 );
          if Not Result then
          begin
               ZAsciiTools.FiltraASCII( cdsDataSet, True );
               Result := ZBaseGridShow_DevEx.GridShow( cdsDataset, TEmpRenumeraGridShow_DevEx );
               ZAsciiTools.FiltraASCII( cdsDataSet, False );
          end;
     end;
end;

function TWizEmpRenumeraEmpleados_DevEx.MuestraListaCorrecta: boolean;
begin
     with dmProcesos do
     begin
          if ( ErrorCount > 0 ) then
             ZAsciitools.ClearErroresASCII( cdsDataset );
          Result := ZBaseSelectGrid_DevEx.GridSelect( cdsDataset, TEmpRenumeraGridSelect_DevEx, False );
          Verificacion := Result;
     end;
end;

procedure TWizEmpRenumeraEmpleados_DevEx.ArchivoSeekClick(Sender: TObject);
begin
     edArchivo.Text := ZetaClientTools.AbreDialogo( OpenDialog, edArchivo.Text, 'dat' );
end;

procedure TWizEmpRenumeraEmpleados_DevEx.SetControles( const lAsciiEnabled: boolean );
begin
     HabilitaDeshabilita( gbArchivo, lAsciiEnabled );
     HabilitaDeshabilita( gbDelta, Not lAsciiEnabled );
     FiltrosCondiciones.Enabled := Not lAsciiEnabled;
     if ( lAsciiEnabled ) then
        edArchivo.SetFocus
     else
         edIncremento.SetFocus;
end;

procedure TWizEmpRenumeraEmpleados_DevEx.HabilitaDeshabilita(gbGrupo: TcxGroupBox; lAccion: boolean);
var
   i: integer;
begin
     with gbGrupo do
     begin
          for i := 0 to ( ControlCount - 1 ) do
          begin
               Controls[ i ].Enabled := lAccion;
          end;
     end;
end;

procedure TWizEmpRenumeraEmpleados_DevEx.ckbKardexClick(Sender: TObject);
begin
     inherited;
     HabilitaDeshabilita( gbKardex, ckbKardex.Checked );
end;


end.
