unit FEmpEventosGridSelect;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZBaseSelectGrid, Db, Grids, DBGrids, ZetaDBGrid, StdCtrls, Buttons,
  ExtCtrls, dbClient;

type
  eOpEvento = ( eoNinguno, eoSalario, eoPuesto, eoTurno, eoContrato, eoTablaPrestaciones,
                eoReingreso, eoBaja, eoNiveles, eoKardexGeneral, eoAsignaCampo, eoTipoNomina );
  ListaOperaciones = Set of eOpEvento;
  TDatosOperacion = record
    FieldCheck : String;
    ExisteCampo : Boolean;
    MostrarColumnas : Boolean;
  end;

  TEmpEventosGridSelect = class(TBaseGridSelect)
    procedure FormShow(Sender: TObject);
    procedure ImprimirClick(Sender: TObject);
  private
    FOperaciones : Array [ eOpEvento ] of TDatosOperacion;
    procedure AgregaColumnasGrid;
    procedure SetListaOperaciones;
    procedure MostrarCampoTexto(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TV_SALARIOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TV_NOMTIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure TV_TIPONOMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    function HayCambios(oField: TField): Boolean;
  public
  end;

var
  EmpEventosGridSelect: TEmpEventosGridSelect;

implementation

uses dGlobal,
     ZetaCommonLists,
     ZetaCommonTools,
     ZetaCommonClasses,
     ZetaDialogo,
     FBaseReportes,
     ZBasicoSelectGrid,
     ZGlobalTress;

{$R *.DFM}

procedure TEmpEventosGridSelect.FormShow(Sender: TObject);
begin
     SetListaOperaciones;
     AgregaColumnasGrid;  // No Habr� problema con siempre Agregar Columnas por que se esta haciendo un free de la forma al terminar
     inherited;
end;

procedure TEmpEventosGridSelect.SetListaOperaciones;
var
   Pos : TBookMark;
   eOpera: eOPEvento;
   lMostrarTodas, lCheckCambios : Boolean;

   procedure InitOperacion( const opera: eOpEvento; const sCampo: String );
   begin
        with FOperaciones[ opera ] do
        begin
             FieldCheck := sCampo;
             ExisteCampo := ( DataSet.FindField( sCampo ) <> nil );
        end;
   end;

begin
     InitOperacion( eoSalario, 'TV_CAMBSAL' );
     InitOperacion( eoPuesto, 'TV_CAMBPUE' );
     InitOperacion( eoTurno, 'TV_CAMBTUR' );
     InitOperacion( eoContrato, 'TV_CAMBCON' );
     InitOperacion( eoTablaPrestaciones, 'TV_CAMBTSS' );
     InitOperacion( eoReingreso, 'TV_ALTA' );
     InitOperacion( eoBaja, 'TV_BAJA' );
     InitOperacion( eoNiveles, 'TV_CAMBNIV' );
     InitOperacion( eoKardexGeneral, 'TV_KARDEX' );
     InitOperacion( eoAsignaCampo, 'TV_CAMPO' );
     InitOperacion( eoTipoNomina, 'TV_CAMBNOM' );
     //eoTipoNomina, TV_CAMTIP
     with DataSet do
     begin
          DisableControls;
          try
             Pos:= GetBookMark;
             try
                First;
                while not EOF do
                begin
                     lMostrarTodas := TRUE;                     // Esta Bandera permite determinar si ya no ocupa seguir revisando el DataSet por que se agregar�n todas las columnas
                     for eOpera := eoSalario to eoTipoNomina do
                     begin
                          with FOperaciones[ eOpera ] do
                          begin
                               if ExisteCampo then
                               begin
                                    if not MostrarColumnas then
                                    begin
                                         if eOpera in [ eoKardexGeneral, eoAsignaCampo ] then
                                            lCheckCambios := strLleno( FieldByName( FieldCheck ).AsString )
                                         else
                                            lCheckCambios := zStrToBool( FieldByName( FieldCheck ).AsString );
                                         MostrarColumnas := lCheckCambios;
                                         if lMostrarTodas then
                                            lMostrarTodas := MostrarColumnas;
                                    end;
                               end;
                          end;
                     end;
                     if lMostrarTodas then
                        Break;
                     Next;
                end;
             finally
                if ( Pos <> nil ) then
                begin
                     GotoBookMark( Pos );
                     FreeBookMark( Pos );
                end;
             end;
          finally
             EnableControls;
          end;
     end;
end;

procedure TEmpEventosGridSelect.AgregaColumnasGrid;
var
   i : Integer;
   sNiv : String;

   procedure AgregarColumna( const sCampo: String; const iAncho: Integer; const sTitulo: String;
             const eOpera: eOpEvento );
   begin
        if FOperaciones[ eOpera ].MostrarColumnas then
        begin
             with ZetaDBGrid.Columns.Add do
             begin
                  FieldName:= sCampo;
                  Width := iAncho;
                  Title.Caption:= sTitulo;
             end;
             if eOpera in [ eoPuesto, eoSalario, eoTablaPrestaciones, eoTurno, eoContrato, eoNiveles ] then
                with DataSet.FieldByName( sCampo ) do
                begin
                     Tag := Ord( eOpera );
                     onGetText := MostrarCampoTexto;
                end;
        end;
   end;

begin
     AgregarColumna( 'TV_PUESTO', 43, 'Puesto', eoPuesto );
     AgregarColumna( 'TV_CLASIFI', 40, 'Clasif.', eoPuesto );
     AgregarColumna( 'TV_AUTOSAL', 25, 'Tab', eoSalario );
     AgregarColumna( 'TV_SALARIO', 65, 'Salario', eoSalario );
     for i := 1 to 5 do
         AgregarColumna( 'TV_OTRAS_' + IntToStr( i ), 40, 'Fija #' + IntToStr( i ), eoSalario );
     AgregarColumna( 'TV_TABLASS', 54, 'Tabla Ley', eoTablaPrestaciones );
     AgregarColumna( 'TV_KARDEX', 45, 'Kardex', eoKardexGeneral );
     AgregarColumna( 'TV_MONTO', 50, 'Monto', eoKardexGeneral );
     AgregarColumna( 'TV_TURNO', 42, 'Turno', eoTurno );
     // (JB) Se agrega columna a grid en caso de que se haya modificado el tipo de nomina.
     if FOperaciones[ eoTipoNomina ].MostrarColumnas then
     begin
          AgregarColumna( 'TV_TIPONOM', 65, 'Tipo N�mina.', eoTipoNomina );
          DataSet.FieldByName( 'TV_TIPONOM' ).onGetText := TV_TIPONOMGetText;
          DataSet.FieldByName( 'TV_TIPONOM' ).Tag := Ord( eoTipoNomina );
     end;
     AgregarColumna( 'TV_CONTRAT', 48, 'Contrato', eoContrato );
     AgregarColumna( 'TV_FEC_CON', 75, 'Renovar al', eoContrato );
     for i := 1 to K_GLOBAL_NIVEL_MAX do
     begin
          sNiv := Global.NombreNivel( i );
          if ( StrLleno( sNiv ) ) then
             AgregarColumna( 'TV_NIVEL' + IntToStr( i ), 60, sNiv, eoNiveles );
     end;
     AgregarColumna( 'TV_BAJA', 30, 'Baja', eoBaja );
     AgregarColumna( 'TV_MOT_BAJ', 60, 'Motivo Baja', eoBaja );
     AgregarColumna( 'TV_FEC_BSS', 75, 'Baja Imss', eoBaja );
     AgregarColumna( 'TV_NOMYEAR', 30, 'A�o', eoBaja );
     AgregarColumna( 'TV_NOMTIPO', 65, 'Tipo', eoBaja );
     AgregarColumna( 'TV_NOMNUME', 50, '# N�mina', eoBaja );
     AgregarColumna( 'TV_ALTA', 30, 'Alta', eoReingreso );
     if Global.GetGlobalBooleano( K_GLOBAL_RH_VALIDA_ORGANIGRAMA ) then
        AgregarColumna( 'TV_PLAZA', 50, 'Plaza', eoReingreso );
     AgregarColumna( 'TV_PATRON', 35, 'Patr�n', eoReingreso );
     AgregarColumna( 'TV_FEC_ANT', 75, 'Antig. desde', eoReingreso );
     AgregarColumna( 'TV_CAMPO', 80, 'Campo', eoAsignaCampo );
     AgregarColumna( 'TV_VALOR', 150, 'Valor', eoAsignaCampo );
     with DataSet do
     begin
          if FOperaciones[ eoSalario ].MostrarColumnas then
             DataSet.FieldByName( 'TV_SALARIO' ).onGetText := TV_SALARIOGetText;     // El MaskPesos no funciona por que debe validarse con MostrarCampoTexto
          if FOperaciones[ eoKardexGeneral ].MostrarColumnas then
             MaskPesos( 'TV_MONTO' );
          if FOperaciones[ eoBaja ].MostrarColumnas then
          begin
               MaskFecha( 'TV_FEC_BSS' );
               with DataSet.FieldByName( 'TV_NOMTIPO' ) do
               begin
                    Tag := Ord( eoBaja );
                    onGetText := TV_NOMTIPOGetText;
               end;
          end;
     end;
     DataSet.FieldByName( 'PRETTYNAME' ).DisplayWidth := 0;
end;

procedure TEmpEventosGridSelect.MostrarCampoTexto(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     if DisplayText then
     begin
          if HayCambios( Sender ) then
             Text := Sender.AsString
          else
             Text := VACIO;
     end
     else
          Text := Sender.AsString;
end;

procedure TEmpEventosGridSelect.TV_SALARIOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     MostrarCampoTexto( Sender, Text, DisplayText );
     if strLleno( Text ) then
        Text := FormatFloat( '#,0.00', Sender.AsFloat );
end;

procedure TEmpEventosGridSelect.TV_NOMTIPOGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     MostrarCampoTexto( Sender, Text, DisplayText );
     if strLleno( Text ) then
        Text := ObtieneElemento( lfTipoNomina, Sender.AsInteger );
end;

procedure TEmpEventosGridSelect.TV_TIPONOMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
     MostrarCampoTexto( Sender, Text, DisplayText );
     if strLleno( Text ) then
        Text := ObtieneElemento( lfTipoPeriodo, Sender.AsInteger );
end;

procedure TEmpEventosGridSelect.ImprimirClick(Sender: TObject);
var
   oCursor: TCursor;
   FDataReporte: TClientDataSet;
   i : Integer;

   procedure EditaReporteDataSet;
   var
      j : Integer;
   begin
        with FDataReporte do
        begin
             First;
             while not EOF do
             begin
                  Edit;
                  for j := 0 to FieldCount - 1 do
                      if ( eOpEvento( Fields[j].Tag ) in [ eoPuesto, eoSalario, eoTablaPrestaciones, eoTurno, eoContrato, eoNiveles ] ) and
                         ( not HayCambios( Fields[j] ) ) then
                         Fields[j].Clear;
                  Post;
                  Next;
             end;
        end;
   end;

begin
     if ZetaDialogo.zConfirm( 'Imprimir...', '� Desea Imprimir La Lista Mostrada ?', 0, mbYes ) then
     begin
          oCursor := Screen.Cursor;
          Screen.Cursor := crHourglass;
          FDataReporte := TClientDataSet.Create( self );
          try
             try
                with FDataReporte do
                begin
                     IndexFieldNames := DataSet.IndexFieldNames;  //'CB_CODIGO;TV_FECHA';
                     Data := DataSet.Data;
                     LogChanges := FALSE;
                     with self.DataSet do
                     begin
                          for i := 0 to FieldCount - 1 do
                              FDataReporte.Fields[i].Tag := Fields[i].Tag;
                     end;
                end;
                EditaReporteDataSet;
             finally
                Screen.Cursor := oCursor;
             end;
             with DataSet do
             begin
                  DisableControls;
                  ZetaDBGrid.DataSource.DataSet := FDataReporte;
                  try
                     FBaseReportes.ImprimirGridParams( ZetaDBGrid, ZetaDBGrid.DataSource.DataSet, Caption, 'IM', ParametrosGrid);

                  finally
                     ZetaDBGrid.DataSource.DataSet := self.DataSet;
                     EnableControls;
                  end;
             end;
          finally
             FreeAndNil( FDataReporte );
          end;
     end;
end;

function TEmpEventosGridSelect.HayCambios( oField: TField ): Boolean;
begin
     Result := zStrToBool( oField.DataSet.FieldByName( FOperaciones[ eOpEvento( oField.Tag ) ].FieldCheck ).AsString );
end;

end.
