unit FWizSistNumeroTarjeta_DevEx;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, ZetaWizard, ExtCtrls,
  ZcxBaseWizard, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, ZetaCXWizard, dxCustomWizardControl,
  dxWizardControl, dxSkinsCore, cxContainer, cxEdit, ZetaDBTextBox,
  cxGroupBox, cxLabel, dxGDIPlusClasses, cxImage, FWizSistBaseFiltro,
  Menus, ZetaKeyLookup_DevEx, cxButtons, ZetaEdit, cxRadioGroup,
  cxTextEdit, cxMemo, TressMorado2013, dxSkinsDefaultPainters;

type
  TWizSistNumeroTarjeta_DevEx =  class(TWizSistBaseFiltro)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
  public
    { Public declarations }
  end;

var
  WizSistNumeroTarjeta_DevEx: TWizSistNumeroTarjeta_DevEx;

implementation

{$R *.DFM}

uses DProcesos,
     DCliente,
     ZetaCommonClasses, ZcxWizardBasico;

procedure TWizSistNumeroTarjeta_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H11625_Actualizar_numero_de_tarjeta;

     //WizardControl.ActivePage := Ejecucion;
end;

function TWizSistNumeroTarjeta_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.NumeroTarjeta;
end;

procedure TWizSistNumeroTarjeta_DevEx.FormShow(Sender: TObject);
begin
     inherited;
     Advertencia.Caption := 'Se realizar� la Actualizaci�n del N�mero de Tarjeta para cada Empleado de la lista de Empresas que se tienen en la Base de Datos Comparte.';
     //Wizard.Saltar( Ejecucion.PageIndex );
     WizardControl.ActivePage := Ejecucion; 
     Anterior.Enabled := False;
     //ProgressBar.Visible := FALSE;
end;

end.
