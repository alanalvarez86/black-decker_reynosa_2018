inherited WizEmpVacaciones_DevEx: TWizEmpVacaciones_DevEx
  Left = 571
  Top = 227
  Caption = 'Vacaciones Globales'
  ClientHeight = 611
  ClientWidth = 462
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited WizardControl: TdxWizardControl
    Width = 462
    Height = 611
    inherited Parametros: TdxWizardControlPage
      Header.Description = 
        'El proceso registrar'#225' vacaciones a un grupo de empleados en una ' +
        'misma fecha.'
      Header.Title = 'Vacaciones globales'
      object RBProceso: TRadioGroup
        Left = 128
        Top = 156
        Width = 201
        Height = 105
        Caption = ' Tipo de Proceso '
        ItemIndex = 0
        Items.Strings = (
          'Agregar &Vacaciones'
          'Saldar Vacaciones &No Tomadas')
        TabOrder = 0
        OnClick = RBProcesoClick
      end
    end
    inherited Ejecucion: TdxWizardControlPage
      inherited GrupoParametros: TcxGroupBox
        Height = 376
        Width = 440
      end
      inherited cxGroupBox1: TcxGroupBox
        Width = 440
        inherited Advertencia: TcxLabel
          Style.IsFontAssigned = True
          Width = 372
          AnchorY = 51
        end
      end
    end
    inherited FiltrosCondiciones: TdxWizardControlPage
      inherited sCondicionLBl: TLabel
        Left = 22
        Top = 225
      end
      inherited sFiltroLBL: TLabel
        Left = 47
        Top = 248
      end
      inherited Seleccionar: TcxButton
        Left = 150
        Top = 342
        Visible = True
      end
      inherited ECondicion: TZetaKeyLookup_DevEx
        Left = 74
        Top = 222
      end
      inherited sFiltro: TcxMemo
        Left = 74
        Top = 248
        Style.IsFontAssigned = True
      end
      inherited GBRango: TGroupBox
        Left = 74
        Top = 92
      end
      inherited bAjusteISR: TcxButton
        Left = 388
        Top = 249
      end
    end
    object ParamVacaciones: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los campos para indicar los par'#225'metros para agregar vac' +
        'aciones.'
      Header.Title = 'Par'#225'metros para vacaciones'
      object GBDiasPrima: TGroupBox
        Left = 0
        Top = 300
        Width = 440
        Height = 62
        Align = alTop
        Caption = ' D'#237'as prima vacacional '
        TabOrder = 3
        object lbSaldarPrima: TLabel
          Left = 70
          Top = 38
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fijos:'
          FocusControl = PrimaVaca
          Transparent = True
        end
        object lSaldarPrima: TCheckBox
          Left = 6
          Top = 14
          Width = 104
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Saldar a la Fec&ha:'
          TabOrder = 0
          OnClick = lSaldarPrimaClick
        end
        object PrimaVaca: TZetaNumero
          Left = 97
          Top = 33
          Width = 67
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 1
          Text = '0.00'
        end
      end
      object GBParametros: TGroupBox
        Left = 0
        Top = 0
        Width = 440
        Height = 153
        Align = alTop
        Caption = ' Vacaciones '
        TabOrder = 0
        object Label1: TLabel
          Left = 17
          Top = 18
          Width = 76
          Height = 13
          Caption = 'Fecha de &Inicio:'
          FocusControl = FechaInicial
          Transparent = True
        end
        object Label2: TLabel
          Left = 2
          Top = 42
          Width = 91
          Height = 13
          Caption = 'Fecha de &Regreso:'
          FocusControl = FechaFinal
          Transparent = True
        end
        object Label11: TLabel
          Left = 54
          Top = 66
          Width = 39
          Height = 13
          Caption = '&Periodo:'
          FocusControl = sPeriodo
          Transparent = True
        end
        object Label12: TLabel
          Left = 19
          Top = 86
          Width = 74
          Height = 13
          Caption = 'O&bservaciones:'
          Transparent = True
        end
        object FechaInicial: TZetaFecha
          Left = 97
          Top = 14
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '14/ene/98'
          Valor = 35809.000000000000000000
        end
        object FechaFinal: TZetaFecha
          Left = 97
          Top = 38
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 1
          Text = '14/ene/98'
          Valor = 35809.000000000000000000
        end
        object sPeriodo: TEdit
          Left = 97
          Top = 62
          Width = 315
          Height = 21
          TabOrder = 2
        end
        object sObservacion: TcxMemo
          Left = 97
          Top = 84
          Lines.Strings = (
            '')
          Properties.ScrollBars = ssVertical
          TabOrder = 3
          Height = 62
          Width = 315
        end
      end
      object GBNominaVaca: TGroupBox
        Left = 0
        Top = 362
        Width = 440
        Height = 110
        Align = alTop
        Caption = ' N'#243'mina a Pagar '
        TabOrder = 4
        object Label8: TLabel
          Left = 69
          Top = 42
          Width = 24
          Height = 13
          Caption = '&Tipo:'
          FocusControl = TipoNomina
          Transparent = True
        end
        object Label9: TLabel
          Left = 53
          Top = 67
          Width = 40
          Height = 13
          Caption = '&N'#250'mero:'
          FocusControl = NumeroNomina
          Transparent = True
        end
        object Label10: TLabel
          Left = 72
          Top = 18
          Width = 22
          Height = 13
          Caption = 'A'#241'&o:'
          FocusControl = YearNomina
          Transparent = True
        end
        object TipoNomina: TZetaKeyCombo
          Left = 97
          Top = 38
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          OnChange = YearNominaChange
          ListaFija = lfTipoNomina
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
        end
        object NumeroNomina: TZetaKeyLookup_DevEx
          Left = 97
          Top = 62
          Width = 315
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          OnValidKey = NumeroNominaValidKey
        end
        object YearNomina: TSpinEdit
          Left = 97
          Top = 14
          Width = 65
          Height = 22
          MaxValue = 3000
          MinValue = 1900
          TabOrder = 0
          Value = 1900
          OnChange = YearNominaChange
        end
        object FiltroTipoNomina: TCheckBox
          Left = 97
          Top = 88
          Width = 248
          Height = 17
          Caption = 'Incluir solo empleados de este tipo de n'#243'mina'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
      end
      object GBDiasPagar: TGroupBox
        Left = 0
        Top = 215
        Width = 440
        Height = 85
        Align = alTop
        Caption = ' D'#237'as a Pagar '
        TabOrder = 2
        object lbSaldarPagar: TLabel
          Left = 70
          Top = 38
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = 'Fi&jos:'
          FocusControl = Pagados
          Transparent = True
        end
        object Label7: TLabel
          Left = 33
          Top = 62
          Width = 61
          Height = 13
          Alignment = taRightJustify
          Caption = 'Otros Pa&gos:'
          FocusControl = Otros
          Transparent = True
        end
        object lSaldarPagados: TCheckBox
          Left = 6
          Top = 14
          Width = 104
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Sal&dar a la Fecha:'
          TabOrder = 0
          OnClick = lSaldarPagadosClick
        end
        object Otros: TZetaNumero
          Left = 97
          Top = 58
          Width = 67
          Height = 21
          Mascara = mnPesos
          TabOrder = 2
          Text = '0.00'
        end
        object Pagados: TZetaNumero
          Left = 97
          Top = 33
          Width = 67
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 1
          Text = '0.00'
        end
      end
      object GBDiasGozados: TGroupBox
        Left = 0
        Top = 153
        Width = 440
        Height = 62
        Align = alTop
        Caption = ' D'#237'as Gozados '
        TabOrder = 1
        object lbSaldarGozar: TLabel
          Left = 70
          Top = 38
          Width = 24
          Height = 13
          Alignment = taRightJustify
          Caption = '&Fijos:'
          FocusControl = Gozados
          Transparent = True
        end
        object lSaldarGozados: TCheckBox
          Left = 6
          Top = 14
          Width = 104
          Height = 17
          Alignment = taLeftJustify
          Caption = 'Sa&ldar a la Fecha: '
          TabOrder = 0
          OnClick = lSaldarGozadosClick
        end
        object Gozados: TZetaNumero
          Left = 97
          Top = 33
          Width = 67
          Height = 21
          Mascara = mnDiasFraccion
          TabOrder = 1
          Text = '0.00'
        end
      end
    end
    object ParamSaldar: TdxWizardControlPage
      DoubleBuffered = False
      Header.Description = 
        'Complete los campos para indicar los par'#225'metros para saldar vaca' +
        'ciones no tomadas.'
      Header.Title = 'Par'#225'metros para saldar vacaciones'
      object GBPeriodoSaldadas: TGroupBox
        Left = 0
        Top = 234
        Width = 441
        Height = 127
        Caption = ' N'#243'mina a Pagar '
        TabOrder = 1
        object Label15: TLabel
          Left = 69
          Top = 53
          Width = 24
          Height = 13
          Caption = '&Tipo:'
          FocusControl = TipoSaldadas
          Transparent = True
        end
        object Label16: TLabel
          Left = 53
          Top = 78
          Width = 40
          Height = 13
          Caption = '&N'#250'mero:'
          FocusControl = NumeroSaldadas
          Transparent = True
        end
        object Label17: TLabel
          Left = 72
          Top = 28
          Width = 22
          Height = 13
          Caption = 'A'#241'&o:'
          FocusControl = YearSaldadas
          Transparent = True
        end
        object TipoSaldadas: TZetaKeyCombo
          Left = 97
          Top = 49
          Width = 145
          Height = 21
          AutoComplete = False
          BevelKind = bkFlat
          Style = csDropDownList
          Ctl3D = False
          ItemHeight = 13
          ParentCtl3D = False
          TabOrder = 1
          OnChange = YearSaldadasChange
          ListaFija = lfTipoNomina
          ListaVariable = lvPuesto
          Offset = 0
          Opcional = False
          EsconderVacios = True
        end
        object NumeroSaldadas: TZetaKeyLookup_DevEx
          Left = 97
          Top = 73
          Width = 315
          Height = 21
          EditarSoloActivos = False
          IgnorarConfidencialidad = False
          TabOrder = 2
          TabStop = True
          WidthLlave = 60
          OnValidKey = NumeroSaldadasValidKey
        end
        object YearSaldadas: TSpinEdit
          Left = 97
          Top = 24
          Width = 65
          Height = 22
          MaxValue = 3000
          MinValue = 1900
          TabOrder = 0
          Value = 1900
          OnChange = YearSaldadasChange
        end
        object SaldoFiltroTipoNomina: TCheckBox
          Left = 97
          Top = 99
          Width = 313
          Height = 17
          Caption = 'Incluir solo empleados de este tipo de n'#243'mina'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
      end
      object GBVacacionesSaldadas: TGroupBox
        Left = 0
        Top = 72
        Width = 441
        Height = 161
        Caption = ' Saldar Vacaciones '
        TabOrder = 0
        object Label14: TLabel
          Left = 19
          Top = 73
          Width = 74
          Height = 13
          Caption = 'O&bservaciones:'
          Transparent = True
        end
        object Label3: TLabel
          Left = 38
          Top = 28
          Width = 55
          Height = 13
          Caption = '&Referencia:'
          FocusControl = FechaReferencia
          Transparent = True
        end
        object Label13: TLabel
          Left = 32
          Top = 52
          Width = 61
          Height = 13
          Caption = '&Vencimiento:'
          FocusControl = VencimientoMonths
          Transparent = True
        end
        object lblAniosMeses: TLabel
          Left = 165
          Top = 52
          Width = 65
          Height = 13
          Caption = 'lblA'#241'osMeses'
        end
        object FechaReferencia: TZetaFecha
          Left = 97
          Top = 24
          Width = 115
          Height = 22
          Cursor = crArrow
          TabOrder = 0
          Text = '14/ene/98'
          Valor = 35809.000000000000000000
        end
        object VencimientoMonths: TSpinEdit
          Left = 97
          Top = 48
          Width = 65
          Height = 22
          MaxValue = 108
          MinValue = 1
          TabOrder = 1
          Value = 1
          OnChange = ChangeVencimientoMonths
        end
        object ObservacionesSaldadas: TcxMemo
          Left = 97
          Top = 73
          Lines.Strings = (
            '')
          Properties.ScrollBars = ssVertical
          TabOrder = 2
          Height = 80
          Width = 315
        end
      end
    end
  end
  inherited Wizard: TZetaCXWizard
    BeforeMove = WizardBeforeMove
    Top = 480
  end
end
