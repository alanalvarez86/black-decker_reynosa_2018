unit FEmpCancelarPermisosGlobalesGridSelect_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, DB, StdCtrls, ExtCtrls,
  ZBaseSelectGrid_DevEx, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Menus, dxSkinsCore, 
  TressMorado2013, dxSkinsDefaultPainters, cxControls, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, cxNavigator, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ImgList, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, ZetaCXGrid, cxButtons, cxCalendar;

type
  TEmpCancelarPermisosGlobalesGridSelect_DevEx = class(TBaseGridSelect_DevEx)
    CB_CODIGO: TcxGridDBColumn;
    PRETTYNAME: TcxGridDBColumn;
    PM_FEC_INI: TcxGridDBColumn;
    PM_DIAS: TcxGridDBColumn;
    PM_CLASIFI: TcxGridDBColumn;
    PM_TIPO: TcxGridDBColumn;
    PM_NUMERO: TcxGridDBColumn;
    PM_CAPTURA: TcxGridDBColumn;
    US_CODIGO: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmpCancelarPermisosGlobalesGridSelect_DevEx: TEmpCancelarPermisosGlobalesGridSelect_DevEx;

implementation

{$R *.DFM}

end.
