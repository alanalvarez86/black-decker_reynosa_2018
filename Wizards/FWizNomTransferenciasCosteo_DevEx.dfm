�
 TWIZNOMTRANSFERENCIASCOSTEO_DEVEX 0}  TPF0�!TWizNomTransferenciasCosteo_DevEx WizNomTransferenciasCosteo_DevExLeft�Top� CaptionTransferencias de Empleados porClientHeight�ClientWidth�PixelsPerInch`
TextHeight �TdxWizardControlWizardControlWidth�Height�Header.AssignedValues	wchvGlyph Header.Glyph.Data
�  �PNG

   IHDR           szz�   sRGB ���   gAMA  ���a   	pHYs  �  ���rI  *IDATXG�V-lA>�@T �
�@T H@ �f��A�@ H*%���%�
d�����(���5AT�@T��@ N���y;7{�׆������7�o��\c�$=s[�����۹y���{R�ߛ���G���V/&�y����=����4:k���n໤�����+p���� )�f��9��F�^������v���� �g����H�P��7��>�H*a<)L�;�Af�%juͅ87?��E�����1m�_�$EsEo��������!�
�"�}1~�i�^�
ǼE�k���{�"�{�ڛ�������"�� �A�?mL�oWi,:�ϕNi�r4\�p�d�NyF���%�`B�Yt_K��y���܎�ķNn�r�|���#%S'D��pQՠ�e����;��$��^>���O�#��2;�YJ	�M6!�����gv�a�7�t7�{#�*ұ�X��[ЗNcZA��l�V�dNjD�n� P��݄�񐎟T$r�4m�P����p�)�m�^��{�+N ���S=���Օ���4�wY6i\�`3���[��X\� ^΂�i~$�yɦVIe:��T���K3ӈ 8߼�2�Ҩ������}T��u�;��� �AsE�R>��p�EG�Q��,u�e��	µ�����(S���L2$�̰<*r:�B�w,���T�e�=Rn�J f]Flk��#�����D\�*2��R��si����<�XNX.3��u ݋�Ҩ��o�y,�!e8�*2_('D{�����Zeq,`��?��=�7�    IEND�B`� �TdxWizardControlPage
ParametrosHeader.Descriptiong Este proceso permite enviar un grupo de empleados de su nivel de costeo original a otro temporalmente.Header.TitleTransferencias TcxGroupBox	GroupBox2LeftTop-Caption Datos de la Transferencia TabOrder Transparent	Height� Width� TLabelLabel5Left1TopWidth?Height	AlignmenttaRightJustifyCaptionFecha Inicial:Transparent	  TLabelHorasLblLeftQTop5WidthHeight	AlignmenttaRightJustifyCaptionHoras:FocusControlTR_HORASTransparent	  TLabel	lbDestinoLeft'TopNWidthIHeight	AlignmenttaRightJustifyCaptionCC a Transferir:FocusControl
CC_DESTINOTransparent	  TLabel	MotivoLblLeftMTopgWidth#Height	AlignmenttaRightJustifyCaptionMotivo:FocusControl	TR_MOTIVOTransparent	  TLabelLabel6Left&Top� WidthJHeight	AlignmenttaRightJustifyCaptionObservaciones:Transparent	  TLabel
TipoAutLblLeftXTop� WidthHeight	AlignmenttaRightJustifyCaptionTipo:Transparent	  
TZetaFechaAU_FECHALeftrTopWidthsHeightCursorcrArrowTabOrder Text	04/mar/11Valor      ��@  TZetaNumeroTR_HORASLeftrTop1Width)HeightMascaramnHorasTabOrderText0.00  TZetaKeyLookup_DevEx
CC_DESTINOLeftrTopJWidth,HeightLookupDatasetdmTablas.cdsNivel1EditarSoloActivosIgnorarConfidencialidadTabOrderTabStop	
WidthLlave<  TZetaKeyLookup_DevEx	TR_MOTIVOLeftrTopcWidth,HeightLookupDatasetdmTablas.cdsMotivoTransferEditarSoloActivosIgnorarConfidencialidadTabOrderTabStop	
WidthLlave<  	TZetaEditTR_TEXTOLeftrTop|Width*HeightTabOrder  TZetaKeyComboTR_TIPOLeftrTop� Width� HeightAutoComplete	BevelKindbkFlatStylecsDropDownListCtl3D
ItemHeightParentCtl3DTabOrder	ListaFijalfTipoHoraTransferenciaCosteoListaVariablelvPuestoOffset OpcionalEsconderVacios  TcxCheckBoxcbDuplicadosLeftoTop� CaptionGValidar que no exista transferencia anterior a mismo(a) Centro de CostoState
cbsCheckedTabOrderTransparent	Widthg    �TdxWizardControlPage	Ejecucion �TcxGroupBoxGrupoParametrosHeight� Width�  �TcxGroupBoxcxGroupBox1Width� �TcxLabelAdvertenciaStyle.IsFontAssigned	Width�AnchorY3    �TdxWizardControlPageFiltrosCondiciones �TLabelsCondicionLBlLeft9Top�   �TLabel
sFiltroLBLLeftRTop�   �TLabel
lbOriginalLeft!TopWidthIHeight	AlignmenttaRightJustifyCaptionCC a Transferir:FocusControlCC_OriginalTransparent	  �	TcxButtonSeleccionarLeft� Top!TabOrderVisible	  �TZetaKeyLookup_DevEx
ECondicionLeftmTop�   �TcxMemosFiltroLeftmTop� Style.IsFontAssigned	  �	TGroupBoxGBRangoLeftmTop  �	TcxButton
bAjusteISRLeft�Top�   TZetaKeyLookup_DevExCC_OriginalLeftmTopWidthRHeightLookupDatasetdmTablas.cdsNivel1EditarSoloActivosIgnorarConfidencialidadTabOrderTabStop	
WidthLlaveR    �TZetaCXWizardWizard
BeforeMoveWizardBeforeMoveLeft�Top   