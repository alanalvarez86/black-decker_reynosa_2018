unit FWizSistCopiarConfiguracionCafeteria_DevEx;

interface

uses
  Windows, Messages, SysUtils,
{$ifndef VER130}
  Variants,
{$endif}
  Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, Buttons, ZetaKeyCombo,
  ExtCtrls, ZetaWizard, FWizSistBaseFiltro, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, Menus,
  ZetaCXWizard, ZetaEdit, cxRadioGroup, cxTextEdit, cxMemo,
  ZetaKeyLookup_DevEx, cxButtons, dxGDIPlusClasses, cxImage, cxLabel,
  cxGroupBox, dxCustomWizardControl, dxWizardControl, dxSkinsCore,
  TressMorado2013, ZcxBaseWizard, Vcl.CheckLst, cxCheckListBox;

type
  TWizSistCopiarConfiguracionCafeteria_DevEx = class(TcxBaseWizard)
    gpConfiguracionDe: TGroupBox;
    gpConfiguracionA: TGroupBox;
    rdTodas: TRadioButton;
    rdNuevas: TRadioButton;
    rdLista: TRadioButton;
    Label1: TLabel;
    Estacion: TComboBox;
    Informacion: TdxWizardControlPage;
    gpInformacionCopiar: TGroupBox;
    rdTodos: TRadioButton;
    rdCategoria: TRadioButton;
    listaEstaciones: TcxCheckListBox;
    Categorias: TcxCheckListBox;
    procedure FormCreate(Sender: TObject);
    procedure rdListaClick(Sender: TObject);
    procedure rdTodasClick(Sender: TObject);
    procedure rdNuevasClick(Sender: TObject);
    procedure WizardBeforeMove(Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
    procedure rdCategoriaClick(Sender: TObject);
    procedure rdTodosClick(Sender: TObject);
    procedure WizardAfterMove(Sender: TObject);
  private
    { Private declarations }
    procedure CargarEstaciones;
    procedure CargarEstacionesConfiguradas;
    function ObtenerListado (Listado: TcxCheckListBox ) : String;
  protected
    { Protected declarations }
    function EjecutarWizard: Boolean; override;
    procedure CargaParametros; override;
  public
    { Public declarations }

  end;

var
  WizSistCopiarConfiguracionCafeteria_DevEx: TWizSistCopiarConfiguracionCafeteria_DevEx;

implementation

{$R *.DFM}

uses DProcesos,
     DSistema,
     ZetaDialogo,
     ZetaCommonTools,
     ZetaCommonClasses;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.FormCreate(Sender: TObject);
begin
     inherited;
     HelpContext := H33620_Copia_Configuracion;
     CargarEstaciones;
     CargarEstacionesConfiguradas;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.rdCategoriaClick(Sender: TObject);
begin
     inherited;
     Categorias.Enabled := rdCategoria.Checked;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.rdListaClick(Sender: TObject);
begin
     inherited;
     listaEstaciones.Enabled := rdLista.Checked
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.rdNuevasClick(Sender: TObject);
begin
     inherited;
     listaEstaciones.Enabled := Not rdNuevas.Checked;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.rdTodasClick(Sender: TObject);
begin
     inherited;
     listaEstaciones.Enabled := Not rdTodas.Checked;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.rdTodosClick(Sender: TObject);
begin
     inherited;
     Categorias.Enabled := Not rdTodos.Checked;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.WizardAfterMove(Sender: TObject);
begin
     inherited;
     if Wizard.EsPaginaActual(Parametros) then
        ParametrosControl := Estacion;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.WizardBeforeMove( Sender: TObject; var iNewPage: Integer; var CanMove: Boolean);
var sEstaciones, sCategoria : String;
begin
     inherited;
     if Wizard.Adelante then
     begin
          if ( WizardControl.ActivePage = Parametros ) then
          begin
               if Estacion.ItemIndex < 0 then
                  CanMove := Error( 'Debe escoger una estación para copiar la configuración', Estacion )
               else
               begin
                    if rdLista.Checked then
                    begin
                         sEstaciones := ObtenerListado(listaEstaciones);
                         if StrVacio(sEstaciones) then
                            CanMove := Error( 'Debe escoger al menos 1 estación para copiar la configuración', listaEstaciones )
                         else
                             if Pos(Estacion.Items[Estacion.ItemIndex], sEstaciones) > 0 then
                                CanMove := Error( 'La estación fuente no puede ser igual a la estación a copiar', listaEstaciones );
                    end
               end
          end
          else if ( WizardControl.ActivePage = Informacion ) then
          begin
               if rdCategoria.Checked then
               begin
                    sCategoria := ObtenerListado(Categorias);
                    if StrVacio(sCategoria) then
                       CanMove := Error( 'Debe escoger al menos una categoría', Categorias );
               end
          end
     end
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.CargaParametros;
var
   sHacia, sCategoria : string;
begin
     inherited CargaParametros;

     if rdTodas.Checked then
        sHacia := 'Todas las cafeterías'
     else if rdNuevas.Checked then
          sHacia := 'Cafeterías registradas sin configuración'
     else
         sHacia := ObtenerListado( listaEstaciones);

     if rdTodos.Checked then
        sCategoria := 'Todas las categorías'
     else
         sCategoria := ObtenerListado( Categorias);

     with ParameterList do
     begin
           AddString( 'De', Estacion.Items[Estacion.ItemIndex] );
           AddString( 'Hacia', sHacia );
           AddString( 'Categorias', sCategoria );
     end;

     with Descripciones do
     begin
           AddString( 'Copiar configuración de', Estacion.Items[Estacion.ItemIndex] );
           AddString( 'Copiar configuración hacia', sHacia );
           AddString( 'Copiar datos en', sCategoria );
     end;
end;

function TWizSistCopiarConfiguracionCafeteria_DevEx.EjecutarWizard: Boolean;
begin
     Result := dmProcesos.ImportarCafeteria( ParameterList );
end;

function TWizSistCopiarConfiguracionCafeteria_DevEx.ObtenerListado(Listado : TcxCheckListBox) : String;
var
   i: integer;
begin
     with Listado do
     for i := 0 to Items.Count - 1 do
         if Items.Items[i].Checked then
            Result := Result + Items.Items[i].Text + ', ';

     delete(Result,length(Result) -1, 2);
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.CargarEstaciones;
var
   i: integer;
   lstEstacionesCafe : TStrings;
begin
     lstEstacionesCafe := TStringList.Create;
     try
        dmSistema.CargaEstacionesCafeteria( lstEstacionesCafe );

        with listaEstaciones do
        begin
             Items.BeginUpdate;
             try
                Clear;
                with lstEstacionesCafe do
                begin
                     for i := 0 to ( Count - 1 ) do
                         Items.Add.Text := lstEstacionesCafe[i];
                end;
            finally
                   Items.EndUpdate;
            end;
       end;

     finally
            lstEstacionesCafe.Free;
     end;
end;

procedure TWizSistCopiarConfiguracionCafeteria_DevEx.CargarEstacionesConfiguradas;
var
   i: integer;
   lstEstacionesCafe : TStrings;
begin
     lstEstacionesCafe := TStringList.Create;
     try
        dmSistema.CargaEstacionesConfiguradas ( lstEstacionesCafe );

        with Estacion do
        begin
             Items.BeginUpdate;
             try
                Clear;
                with lstEstacionesCafe do
                begin
                     for i := 0 to ( Count - 1 ) do
                         Items.Add( lstEstacionesCafe[i]);
                end;
            finally
                   Items.EndUpdate;
            end;
       end;

     finally
            lstEstacionesCafe.Free;
     end;
end;

end.

